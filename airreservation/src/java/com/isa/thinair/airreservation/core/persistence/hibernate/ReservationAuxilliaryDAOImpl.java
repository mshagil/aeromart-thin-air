/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatStatusTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationDTO;
import com.isa.thinair.airreservation.api.dto.PNLADLDestinationSupportDTO;
import com.isa.thinair.airreservation.api.dto.PassengerCheckinDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.PnlAdlDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.autocheckin.CheckinPassengersInfoDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.cesar.FlightLegPaxTypeCountTO;
import com.isa.thinair.airreservation.api.dto.cesar.PaxTypeCountTO;
import com.isa.thinair.airreservation.api.dto.eticket.PaxEticketTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.CabinInventoryDTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightLoadInfoDTO;
import com.isa.thinair.airreservation.api.dto.flightLoad.FlightSegmentInventoryDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestRecordDTO;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestRouteDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealNotificationDetailsDTO;
import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.model.AirportPassengerMessageTxHsitory;
import com.isa.thinair.airreservation.api.model.FlightLoad;
import com.isa.thinair.airreservation.api.model.PalCalHistory;
import com.isa.thinair.airreservation.api.model.PnlAdlHistory;
import com.isa.thinair.airreservation.api.model.PnlPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegAutoCheckin;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.PalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airreservation.core.bl.pnl.PNLUtil;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * ReservationAuxilliaryDAOImp is the business DAO hibernate implementation
 * 
 * @author Isuru
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationAuxilliaryDAO"
 */
public class ReservationAuxilliaryDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationAuxilliaryDAO {

	/* Holds the logger instance */

	private static Log log = LogFactory.getLog(ReservationAuxilliaryDAOImpl.class);

	final String CurrentPNRSegmentArrivalTime = "CurrArrivalTime";

	final String CurrentPNRSegmentDepartureTime = "CurrDeptTime";

	final String CurrentPNRSegmentDestination = "CurrDestCode";

	final String CurrentPNRSegmentDepartingStation = "CurrDepartingCode";

	private static Double maxConnectionTime = null;
	private static Double minConnectionTime = null;

	private final String REQ_SUCCESS = "SUCCESS";

	private final String INITIAL_ATTEMPT = "IA";
	private final String RETRIED_BOTH = "RC";
	private final String RETRIED_SEAT = "RS";
	// private List<AirportMessageDisplayDTO> airportsDisplayMessageList;

	/**
	 * Get flight manifest details
	 * 
	 * @param flightId
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<FlightManifestRecordDTO> getFlightManifest(FlightManifestOptionsDTO manifestOptions) {

		DataSource ds = ReservationModuleUtils.getDataSource(manifestOptions.getDataSourceMode());
		JdbcTemplate templete = new JdbcTemplate(ds);

		final Map<Integer, Integer> mapPnrPaxSegIds = new HashMap<Integer, Integer>();
		boolean isSortOnd = false;

		String strSortOption;
		String cabinClassFilter = "";
		String passportFilter = "";
		String docsFilter = "";
		String flightManifestDTOSQL1 = "";
		String flightManifestDTOSQL2 = "";

		if (manifestOptions.getSortOption().equalsIgnoreCase("BookingDt")) {
			strSortOption = "r.booking_timestamp";
		} else if (manifestOptions.getSortOption().equalsIgnoreCase("FName")) {
			strSortOption = "pax.upper_first_name,pax.upper_last_name";
		} else if (manifestOptions.getSortOption().equalsIgnoreCase("LName")) {
			strSortOption = "pax.upper_last_name";
		} else if (manifestOptions.getSortOption().equalsIgnoreCase("OnD")) {
			strSortOption = "fs.segment_code";
			isSortOnd = true;
		} else if (manifestOptions.getSortOption().equalsIgnoreCase("Agency")) {
			strSortOption = "Agency";
		} else if (manifestOptions.getSortOption().equalsIgnoreCase("ConnectionTime")) {
			strSortOption = "CONNECTION_TIME";
		} else {
			strSortOption = "ps.pnr";
		}

		if (manifestOptions.isFilterByCabinClass()) {

			if ("All".equalsIgnoreCase(manifestOptions.getCabinClass())) {
				if (manifestOptions.getSortOption().equalsIgnoreCase("BookingDt")) {
					strSortOption = "cabin_class_name, logical_cabin_class_name, r.booking_timestamp";
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("FName")) {
					strSortOption = "cabin_class_name, logical_cabin_class_name, pax.upper_first_name,pax.upper_last_name";
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("LName")) {
					strSortOption = "cabin_class_name, logical_cabin_class_name, pax.upper_last_name";
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("OnD")) {
					strSortOption = "cabin_class_name, logical_cabin_class_name, fs.segment_code";
					isSortOnd = true;
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("Agency")) {
					strSortOption = "cabin_class_name, logical_cabin_class_name, Agency";
				} else {
					strSortOption = "cabin_class_name, logical_cabin_class_name, ps.pnr";
				}
			} else {
				cabinClassFilter = "AND bc.cabin_class_code ='" + manifestOptions.getCabinClass() + "' ";
			}
		} else if (manifestOptions.isFilterByLogicalCabinClass()) {
			if (("All".equalsIgnoreCase(manifestOptions.getCabinClass()))
					|| ("All".equalsIgnoreCase(manifestOptions.getLogicalCabinClass()))) {

				if (manifestOptions.getSortOption().equalsIgnoreCase("BookingDt")) {
					strSortOption = "logical_cabin_class_code, r.booking_timestamp";
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("FName")) {
					strSortOption = "logical_cabin_class_code, pax.upper_first_name,pax.upper_last_name";
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("LName")) {
					strSortOption = "logical_cabin_class_code, pax.upper_last_name";
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("OnD")) {
					strSortOption = "logical_cabin_class_code, fs.segment_code";
					isSortOnd = true;
				} else if (manifestOptions.getSortOption().equalsIgnoreCase("Agency")) {
					strSortOption = "logical_cabin_class_code, Agency";
				} else {
					strSortOption = "logical_cabin_class_code, ps.pnr";
				}
				if (!"All".equalsIgnoreCase(manifestOptions.getCabinClass())
						&& "All".equalsIgnoreCase(manifestOptions.getLogicalCabinClass())) {
					cabinClassFilter = "AND bc.cabin_class_code ='" + manifestOptions.getCabinClass() + "' ";
				}
			} else {
				cabinClassFilter = "AND bc.logical_cabin_class_code ='" + manifestOptions.getLogicalCabinClass() + "' ";
			}
		}

		if ((!BeanUtils.nullHandler(manifestOptions.getPassportStatus()).equals("") && !ReservationInternalConstants.PassportStatus.ANY
				.equalsIgnoreCase(manifestOptions.getPassportStatus()))
				|| (!BeanUtils.nullHandler(manifestOptions.getPassportStatus()).equals("") && !ReservationInternalConstants.DOCSInfoStatus.ANY
						.equalsIgnoreCase(manifestOptions.getDocsStatus()))) {

			flightManifestDTOSQL1 = "SELECT * FROM (";
			flightManifestDTOSQL2 = "";

			if (!ReservationInternalConstants.PassportStatus.ANY.equalsIgnoreCase(manifestOptions.getPassportStatus())
					&& ReservationInternalConstants.DOCSInfoStatus.ANY.equalsIgnoreCase(manifestOptions.getDocsStatus())) {

				if (ReservationInternalConstants.PassportStatus.EXISTING.equalsIgnoreCase(manifestOptions.getPassportStatus())) {
					passportFilter = " ) WHERE pnumber IS NOT NULL ";
				} else if (ReservationInternalConstants.PassportStatus.MISSING.equalsIgnoreCase(manifestOptions
						.getPassportStatus())) {
					passportFilter = ") WHERE pnumber IS NULL ";
				}

			} else if (ReservationInternalConstants.PassportStatus.ANY.equalsIgnoreCase(manifestOptions.getPassportStatus())
					&& !ReservationInternalConstants.DOCSInfoStatus.ANY.equalsIgnoreCase(manifestOptions.getDocsStatus())) {

				if (ReservationInternalConstants.DOCSInfoStatus.EXISTING.equalsIgnoreCase(manifestOptions.getDocsStatus())) {
					docsFilter = " ) WHERE (pnumber IS NOT NULL AND foidExpiry IS NOT NULL AND foidIssuedCntry IS NOT NULL"
							+ " AND title IS NOT NULL AND firstName IS NOT NULL AND lastName IS NOT NULL AND dob IS NOT NULL AND nationality IS NOT NULL"
							+ flightManifestDTOSQL2;
				} else if (ReservationInternalConstants.DOCSInfoStatus.MISSING.equalsIgnoreCase(manifestOptions.getDocsStatus())) {
					docsFilter = " ) WHERE (pnumber IS NULL OR foidExpiry IS NULL OR foidIssuedCntry IS NULL"
							+ " OR title IS NULL OR firstName IS NULL OR lastName IS NULL OR dob IS NULL OR nationality IS NULL"
							+ flightManifestDTOSQL2;
				}
			} else if (!ReservationInternalConstants.PassportStatus.ANY.equalsIgnoreCase(manifestOptions.getPassportStatus())
					&& !ReservationInternalConstants.DOCSInfoStatus.ANY.equalsIgnoreCase(manifestOptions.getDocsStatus())) {

				if (ReservationInternalConstants.PassportStatus.EXISTING.equalsIgnoreCase(manifestOptions.getPassportStatus())) {
					passportFilter = " ) WHERE pnumber IS NOT NULL AND ";
				} else if (ReservationInternalConstants.PassportStatus.MISSING.equalsIgnoreCase(manifestOptions
						.getPassportStatus())) {
					passportFilter = ") WHERE pnumber IS NULL AND ";
				}

				if (ReservationInternalConstants.DOCSInfoStatus.EXISTING.equalsIgnoreCase(manifestOptions.getDocsStatus())) {
					docsFilter = "(pnumber IS NOT NULL AND foidExpiry IS NOT NULL AND foidIssuedCntry IS NOT NULL"
							+ " AND title IS NOT NULL AND firstName IS NOT NULL AND lastName IS NOT NULL AND dob IS NOT NULL AND nationality IS NOT NULL"
							+ flightManifestDTOSQL2;
				} else if (ReservationInternalConstants.DOCSInfoStatus.MISSING.equalsIgnoreCase(manifestOptions.getDocsStatus())) {
					docsFilter = "(pnumber IS NULL OR foidExpiry IS NULL OR foidIssuedCntry IS NULL"
							+ " OR title IS NULL OR firstName IS NULL OR lastName IS NULL OR dob IS NULL OR nationality IS NULL"
							+ flightManifestDTOSQL2;
				}

			}

		}

		/*
		 * The sub query to return infant's parent passenger's BC type. Since infant's don't have an BC allocated
		 * specifically they will slip into the Flight Manifest on an OPENRT reservation otherwise.
		 */
		String adultBCTypeSubQuery = "(SELECT DISTINCT(SUB_BC.BC_TYPE) FROM T_PNR_PAX_FARE_SEGMENT SUB_PPFS , T_BOOKING_CLASS SUB_BC, "
				+ "T_PNR_PAX_FARE SUB_PPF , T_PNR_SEGMENT SUB_PS "
				+ "WHERE SUB_PPF.PNR_PAX_ID = PAX.adult_id "
				+ "AND SUB_PPFS.PPF_ID = SUB_PPF.PPF_ID "
				+ "AND SUB_PPFS.BOOKING_CODE = SUB_BC.BOOKING_CODE "
				+ "AND SUB_PS.FLT_SEG_ID = FS.FLT_SEG_ID "
				+ "AND SUB_PS.PNR_SEG_ID = SUB_PPFS.PNR_SEG_ID AND SUB_PS.STATUS <> 'CNX')";

		// Fill DTO
		String flightManifestDTOSQL = "SELECT paxaddn.passport_number AS pnumber, pax.PNR_PAX_ID, ps.pnr,"
				+ "pax.upper_first_name as firstName, pax.upper_last_name as lastName, " + "nvl(pax.title,'') as title, ";

		if (AppSysParamsUtil.isShowPaxNamesInUpperCaseOnFltManifest()) {
			flightManifestDTOSQL += "upper(pax.first_name) || ' ' || upper(pax.last_name) as PassengerName,";
		} else {
			flightManifestDTOSQL += "pax.first_name || ' ' || pax.last_name as PassengerName,";
		}

		flightManifestDTOSQL += "pax.pax_type_code, pax.ADULT_ID, pax.DATE_OF_BIRTH as dob, ps.pnr_seg_id,"
				// + "nvl(pax.ssr_code,'')  as SSR, "
				// + "nvl(pax.ssr_text,'')  as SSR_INFO, "
				+ "to_char(r.booking_timestamp,'dd/mm/yyyy HH24:mi:ss') as bktimestamp, "
				+ "fs.segment_code,  r.status as ResStatus, ppfs.PAX_STATUS as PAXStatus, "
				+ "nvl(a.agent_name,nvl(ch.description,'')) as Agency, ppfs.booking_code,bc.bc_type, "
				+ "CASE WHEN pax.pax_type_code='IN' THEN f_get_infant_cabin_class_code(pax.PNR_PAX_ID,ps.pnr_seg_id,'CC') "
				+ " ELSE bc.cabin_class_code END AS cabin_class_code,"
				+ "CASE WHEN pax.pax_type_code='IN' THEN (SELECT cc.description FROM t_cabin_class cc "
				+ " WHERE cc.cabin_class_code=f_get_infant_cabin_class_code(pax.PNR_PAX_ID,ps.pnr_seg_id,'CC')) "
				+ " ELSE (SELECT cc.description FROM t_cabin_class cc WHERE cc.cabin_class_code=bc.cabin_class_code) END AS cabin_class_name,"
				+ "CASE WHEN pax.pax_type_code='IN' THEN f_get_infant_cabin_class_code(pax.PNR_PAX_ID,ps.pnr_seg_id,'LCC') "
				+ " ELSE bc.logical_cabin_class_code END AS logical_cabin_class_code,"
				+ "CASE WHEN pax.pax_type_code='IN' THEN (SELECT f_convert_message(msg.message_content) FROM t_i18n_message msg, t_logical_cabin_class lcc "
				+ " WHERE lcc.logical_cabin_class_code  =f_get_infant_cabin_class_code(pax.PNR_PAX_ID,ps.pnr_seg_id,'LCC') AND lcc.description_i18n_message_key=msg.i18n_message_key "
				+ " AND msg.message_locale ='en' ) "
				+ " ELSE (SELECT f_convert_message(msg.message_content) FROM t_i18n_message msg, t_logical_cabin_class lcc "
				+ " WHERE lcc.logical_cabin_class_code  =bc.logical_cabin_class_code AND lcc.description_i18n_message_key=msg.i18n_message_key "
				+ " AND msg.message_locale ='en') END AS logical_cabin_class_name,"
				+ "sum(nvl(pt.amount,0)) as Balance, ap.param_value as Currency,"
				+ getSeatCodeSQL(manifestOptions.getFlightId())
				+ getMealCodeSQL(manifestOptions.getFlightId())
				+ getBaggageCodeSQL(manifestOptions.getFlightId())
				+ " paxAddn.PASSPORT_NUMBER as foidNumber, paxCateg.pax_category_code as paxCategory, nationality.DESCRIPTION as nationality, "
				+ " paxAddn.PASSPORT_EXPIRY as foidExpiry, paxAddn.PASSPORT_ISSUED_CNTRY as foidIssuedCntry,"
				+ " (SELECT pet.e_ticket_number||'/'||ppfst.coupon_number||'-'||ppfst.ext_e_ticket_number||'/'||ppfst.ext_coupon_number "
				+ " FROM T_PNR_PAX_FARE_SEG_E_TICKET ppfst, "
				+ " T_PAX_E_TICKET pet "
				+ "  WHERE pax.pnr_pax_id                 = pet.pnr_pax_id "
				+ " AND pet.pax_e_ticket_id                = ppfst.pax_e_ticket_id "
				+ " AND ppfst.pnr_pax_fare_seg_e_ticket_id =  "
				+ " (SELECT MAX(ppfst.pnr_pax_fare_seg_e_ticket_id)  "
				+ "    FROM t_pnr_pax_fare_seg_e_ticket ppfst ,  "
				+ "  T_PNR_PAX_FARE_SEGMENT ppfs             ,  "
				+ "  T_PAX_E_TICKET pet  "
				+ "  WHERE pax.pnr_pax_id  = pet.pnr_pax_id  "
				+ " AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id  "
				+ " AND ppfst.ppfs_id       = ppfs.ppfs_id  "
				+ " AND ps.pnr_seg_id       = ppfs.pnr_seg_id  "
				+ " GROUP BY ppfs.ppfs_id  "
				+ "  )  "
				+ " ) AS eticket ,   "

				// Connection time retrieval
				+ " ABS(NVL( (SELECT ( CASE "
				+ " WHEN (SEG.OND_GROUP_ID = PS.OND_GROUP_ID  "
				+ " AND SEG.SEGMENT_SEQ    = (PS.SEGMENT_SEQ - 1)) "
				+ " THEN ((select EST_TIME_DEPARTURE_LOCAL from t_flight_segment tmp_fs where tmp_fs.flt_seg_id = fs.flt_seg_id)       - FLTSEG.EST_TIME_ARRIVAL_LOCAL)"
				+ " WHEN (SEG.OND_GROUP_ID = PS.OND_GROUP_ID "
				+ " AND SEG.SEGMENT_SEQ    = (PS.SEGMENT_SEQ + 1)) "
				+ " THEN (FLTSEG.EST_TIME_DEPARTURE_LOCAL         - (select EST_TIME_ARRIVAL_LOCAL from t_flight_segment tmp_fs where tmp_fs.flt_seg_id = fs.flt_seg_id) ) "
				+ " ELSE 0 END) AS con_time "
				+ " FROM T_FLIGHT_SEGMENT FLTSEG, T_PNR_SEGMENT SEG "
				+ " WHERE "
				+ " FLTSEG.FLT_SEG_ID=SEG.FLT_SEG_ID  "

				// Make sure first flights from return reservations are ignored.
				+ " AND ( (SEG.OND_GROUP_ID=PS.OND_GROUP_ID "
				+ " AND SEG.SEGMENT_SEQ = "
				+ " (PS.SEGMENT_SEQ - 1)) OR (SEG.OND_GROUP_ID=PS.OND_GROUP_ID "
				+ " AND SEG.SEGMENT_SEQ       = (PS.SEGMENT_SEQ + 1)) )"
				+ " AND SEG.STATUS <> 'CNX' "
				+ " AND SEG.PNR = PS.PNR"
				+ " AND rownum = 1 "
				+ " ) , 0 )) AS CONNECTION_TIME, "
				+ " (SELECT rpc.no FROM t_res_pcd rpc WHERE rpc.pnr_pax_id = pax.pnr_pax_id and rownum =1 ) as cc_digits ,"
				+ "release_timestamp,ac.expire_on, r.originator_pnr "
				+ "FROM T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, T_PNR_PASSENGER pax, T_PNR_PAX_ADDITIONAL_INFO paxAddn, "
				+ "T_RESERVATION r, T_PNR_PAX_FARE_SEGMENT ppfs, T_BOOKING_CLASS bc, "
				+ "T_PAX_TRANSACTION pt, T_AGENT a, T_APP_PARAMETER ap, T_SALES_CHANNEL ch, "
				+ "T_PNR_PAX_FARE ppf, "
				+ "T_PNR_PAX_CATEGORY paxCateg, T_NATIONALITY nationality,T_AUTO_CANCELLATION ac  "
				+ "WHERE ps.flt_seg_id = fs.flt_seg_id "
				+ "AND ps.pnr = r.pnr "
				+ "AND ps.pnr = pax.pnr "
				+ "AND pax.PNR_PAX_ID=paxAddn.PNR_PAX_ID "
				+ "AND ps.pnr_seg_id = ppfs.pnr_seg_id "
				+ "AND ppfs.booking_code = bc.booking_code (+) "
				+ cabinClassFilter
				+ "AND ppfs.ppf_id = ppf.ppf_id "
				+ "AND ppf.pnr_pax_id = pax.pnr_pax_id "
				+ "AND fs.flight_id = "
				+ manifestOptions.getFlightId()
				+ " "
				+ "AND ps.status <> '"
				+ ReservationInternalConstants.ReservationSegmentStatus.CANCEL
				+ "' "
				+ "AND pt.pnr_pax_id (+) = pax.pnr_pax_id "
				+ "AND r.owner_agent_code = a.agent_code (+) "
				+ "AND r.owner_channel_code = ch.sales_channel_code (+) "
				+ "AND (BC.bc_type is null OR BC.bc_type <> '"
				+ BookingClass.BookingClassType.OPEN_RETURN
				+ "') "

				/*
				 * Unfortunately I couldn't find a way to run this query and evaluate it twice without duplicating the
				 * subquery. If there's some way that I've missed this should be changed to remove query duplication.
				 * TODO: Thihara
				 */
				+ "AND ( "
				+ adultBCTypeSubQuery
				+ "IS NULL OR "
				+ adultBCTypeSubQuery
				+ " <> '"
				+ BookingClass.BookingClassType.OPEN_RETURN
				+ "')"

				/** JIRA :AARESAA-3317(Lalanthi) */
				+ "AND ap.param_key = '"
				+ SystemParamKeys.BASE_CURRENCY
				+ "' "
				+ " AND pax.PAX_CATEGORY_CODE=paxCateg.pax_category_code (+) "
				// + "AND pax.pax_cat_foid_id = paxCategF.pax_cat_foid_id (+) "
				// + "AND paxCategF.pax_category_code = paxCateg.pax_category_code (+) "
				+ "AND pax.nationality_code = nationality.nationality_code (+) "
				+ "AND ps.auto_cancellation_id = ac.auto_cancellation_id (+) "
				+ getShowBookingsFilteredByAgentQuery(manifestOptions.isPrivilegedToShowAllBookings(), manifestOptions
						.getUserPrincipal().getAgentCode())
				+ "GROUP BY pax.PNR_PAX_ID,ps.pnr, "
				+ "pax.title,"
				+ "pax.first_name, pax.last_name, "
				+ "pax.upper_first_name, pax.upper_last_name, "
				+ "pax.pax_type_code, pax.ADULT_ID,pax.DATE_OF_BIRTH, "
				+ "PS.SEGMENT_SEQ, PPFS.PNR_SEG_ID, FS.EST_TIME_DEPARTURE_LOCAL,PS.OND_GROUP_ID,"
				// + "nvl(pax.ssr_code,''), "
				// + "nvl(pax.ssr_text,''), "
				+ "ps.pnr_seg_id, "

				+ "fs.segment_code, fs.flt_seg_id, r.status , ppfs.PAX_STATUS, "
				+ "nvl(a.agent_name,nvl(ch.description,''))  , ppfs.booking_code, bc.bc_type, bc.cabin_class_code, bc.logical_cabin_class_code, "
				+ "ap.param_value, paxAddn.PASSPORT_NUMBER, paxAddn.PASSPORT_EXPIRY, paxAddn.PASSPORT_ISSUED_CNTRY, "
				+ "paxCateg.pax_category_code,nationality.DESCRIPTION, r.booking_timestamp, pax.release_timestamp,ac.expire_on, r.originator_pnr "
				+ "ORDER BY " + strSortOption + flightManifestDTOSQL2;

		flightManifestDTOSQL = flightManifestDTOSQL1 + flightManifestDTOSQL + passportFilter + docsFilter;

		final FlightManifestOptionsDTO finalManifestOptions = manifestOptions;

		int filteringOption;

		if (finalManifestOptions.isCheckInwardConnections()) {
			if (finalManifestOptions.isCheckOutwardConnections()) {

				if (!"".equals(finalManifestOptions.getInwardBoardingAirport())) {
					filteringOption = 1;

				} else if (!"".equals(finalManifestOptions.getInwardConnectionOrigin())
						|| !"".equals(finalManifestOptions.getInwardFlightNo())) {
					filteringOption = 2;

				} else {

					filteringOption = 3;

				}

				if (!"".equals(finalManifestOptions.getOutwardBoardingAirport())) {
					filteringOption = filteringOption * 10 + 1;

				} else if (!"".equals(finalManifestOptions.getOutwardConnectionDestination())
						|| !"".equals(finalManifestOptions.getOutwardFlightNo())) {
					filteringOption = filteringOption * 10 + 2;

				} else {

					filteringOption = filteringOption * 10 + 3;
				}

			} else {

				if (!"".equals(finalManifestOptions.getInwardBoardingAirport())) {
					filteringOption = 1;

				} else if (!"".equals(finalManifestOptions.getInwardConnectionOrigin())
						|| !"".equals(finalManifestOptions.getInwardFlightNo())) {
					filteringOption = 2;

				} else {

					filteringOption = 3;
				}

			}

		} else {

			if (finalManifestOptions.isCheckOutwardConnections()) {

				if (!"".equals(finalManifestOptions.getOutwardBoardingAirport())) {
					filteringOption = 4;
				} else if (!"".equals(finalManifestOptions.getOutwardConnectionDestination())
						|| !"".equals(finalManifestOptions.getOutwardFlightNo())) {
					filteringOption = 5;
				} else {

					filteringOption = 6;
				}
			} else {
				filteringOption = 7;
			}
		}

		final int finalFliteringOption = filteringOption;
		final boolean searchTriggerdByDifferentCarrier = manifestOptions.isSearchTriggerdByDifferentCarrier();

		ArrayList<FlightManifestRecordDTO> flightManifestDTOs = (ArrayList<FlightManifestRecordDTO>) templete.query(
				flightManifestDTOSQL, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<FlightManifestRecordDTO> flightManifestDTOs = new ArrayList<FlightManifestRecordDTO>();
						String passengerName;
						String paxStatus;
						String bcType;
						String cabinClassCode;
						String paxTitle;
						String paxType;
						String paxName;
						String segmentOrigin;
						String segmentDestination;
						FlightManifestRecordDTO flightManifestDTO;
						FlightManifestRouteDTO manifestDestinationDTO;
						FlightManifestRouteDTO manifestOriginDTO;

						while (rs.next()) {
							segmentOrigin = rs.getString("segment_code").substring(0, 3);
							segmentDestination = rs.getString("segment_code")
									.substring(rs.getString("segment_code").length() - 3);

							switch (finalFliteringOption) {

							case 1:
								if (finalManifestOptions.getInwardBoardingAirport().equals(segmentOrigin)) {
									manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);

									if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
										flightManifestDTO = new FlightManifestRecordDTO();
										flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
										flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

									} else {
										continue;
									}

								} else {
									continue;
								}
								break;
							case 2:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {

									flightManifestDTO = new FlightManifestRecordDTO();
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {
									continue;
								}
								break;
							case 3:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								flightManifestDTO = new FlightManifestRecordDTO();

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {
									flightManifestDTO.setOrigin(segmentOrigin);
								}
								break;
							case 4:
								if (finalManifestOptions.getOutwardBoardingAirport().equals(segmentDestination)) {
									manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);

									if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
										flightManifestDTO = new FlightManifestRecordDTO();
										flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
										flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO
												.isPossibleMissConnection());

									} else {
										continue;
									}

								} else {
									continue;
								}
								break;
							case 5:

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO = new FlightManifestRecordDTO();
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									continue;
								}
								break;
							case 6:
								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);
								flightManifestDTO = new FlightManifestRecordDTO();

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									flightManifestDTO.setDestination(segmentDestination);
								}
								break;
							case 11:
								if (finalManifestOptions.getInwardBoardingAirport().equals(segmentOrigin)) {
									manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);

									if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
										flightManifestDTO = new FlightManifestRecordDTO();
										flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
										flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

									} else {
										continue;
									}

								} else {
									continue;
								}

								if (finalManifestOptions.getOutwardBoardingAirport().equals(segmentDestination)) {
									manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);
									if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
										flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
										flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO
												.isPossibleMissConnection());

									} else {
										continue;
									}

								} else {
									continue;
								}
								break;
							case 12:
								if (finalManifestOptions.getInwardBoardingAirport().equals(segmentOrigin)) {
									manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);

									if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
										flightManifestDTO = new FlightManifestRecordDTO();
										flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
										flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

									} else {
										continue;
									}

								} else {
									continue;
								}

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									continue;
								}
								break;
							case 13:
								if (finalManifestOptions.getInwardBoardingAirport().equals(segmentOrigin)) {
									manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);

									if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
										flightManifestDTO = new FlightManifestRecordDTO();
										flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
										flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

									} else {
										continue;
									}

								} else {
									continue;
								}

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									flightManifestDTO.setDestination(segmentDestination);
								}
								break;
							case 21:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO = new FlightManifestRecordDTO();
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {
									continue;
								}

								if (finalManifestOptions.getOutwardBoardingAirport().equals(segmentDestination)) {
									manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);
									if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
										flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
										flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO
												.isPossibleMissConnection());

									} else {
										continue;
									}

								} else {
									continue;
								}
								break;
							case 22:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO = new FlightManifestRecordDTO();
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {
									continue;
								}

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									continue;
								}
								break;
							case 23:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO = new FlightManifestRecordDTO();
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {
									continue;
								}

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									flightManifestDTO.setDestination(segmentDestination);
								}
								break;
							case 31:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								flightManifestDTO = new FlightManifestRecordDTO();
								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {

									flightManifestDTO.setOrigin(segmentOrigin);
								}

								if (finalManifestOptions.getOutwardBoardingAirport().equals(segmentDestination)) {
									manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
											rs.getString("segment_code"), finalManifestOptions);

									if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
										flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
										flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO
												.isPossibleMissConnection());

									} else {
										continue;
									}

								} else {
									continue;
								}
								break;
							case 32:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								flightManifestDTO = new FlightManifestRecordDTO();

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());

								} else {

									flightManifestDTO.setOrigin(segmentOrigin);
								}

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {

									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());

								} else {
									continue;
								}
								break;
							case 33:
								manifestOriginDTO = getLatestInboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								flightManifestDTO = new FlightManifestRecordDTO();

								if (manifestOriginDTO != null && manifestOriginDTO.getOrigin() != null) {
									flightManifestDTO.setPossibleMisConnection(manifestOriginDTO.isPossibleMissConnection());
									flightManifestDTO.setOrigin(manifestOriginDTO.getOrigin());
									flightManifestDTO.setInboundConnectionTime(manifestOriginDTO.getInboundConnectionTime());
								} else {

									flightManifestDTO.setOrigin(segmentOrigin);
								}

								manifestDestinationDTO = getEarliestOutboundConnectionDestination(rs.getString("pnr"),
										rs.getString("segment_code"), finalManifestOptions);

								if (manifestDestinationDTO != null && manifestDestinationDTO.getDestination() != null) {
									flightManifestDTO.setDestination(manifestDestinationDTO.getDestination());
									flightManifestDTO.setPossibleMisConnection(manifestDestinationDTO.isPossibleMissConnection());
									flightManifestDTO.setOutboundConnectionTime(manifestDestinationDTO
											.getOutboundConnectionTime());

								} else {
									flightManifestDTO.setDestination(segmentDestination);
								}
								break;
							case 7:
							default:
								flightManifestDTO = new FlightManifestRecordDTO();
								break;

							}

							if (flightManifestDTO.getOrigin() == null) {
								flightManifestDTO.setOrigin(segmentOrigin);
							}

							if (flightManifestDTO.getDestination() == null) {
								flightManifestDTO.setDestination(segmentDestination);
							}

							flightManifestDTO.setConnectionTime(rs.getDouble("connection_time"));
							flightManifestDTO.setSegmentCode(rs.getString("segment_code"));
							flightManifestDTO.setAgency(rs.getString("Agency"));
							flightManifestDTO.setBalance(rs.getBigDecimal("Balance"));
							bcType = rs.getString("bc_type");
							cabinClassCode = rs.getString("cabin_class_code");

							if (rs.getString("booking_code") != null) {
								if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
									String logicalCCCode = rs.getString("logical_cabin_class_code");
									if (bcType != null && bcType.equals(BookingClass.BookingClassType.STANDBY)) {
										// Standby bookings are differentiated by *
										// in booking code
										flightManifestDTO.setBookingClass(rs.getString("booking_code") + "/" + logicalCCCode
												+ "/" + cabinClassCode + "*");
									} else if (bcType != null && bcType.equals(BookingClass.BookingClassType.OPEN_RETURN)) {
										// Open Return segments are differentiated
										// by # in booking code
										flightManifestDTO.setBookingClass(rs.getString("booking_code") + "/" + logicalCCCode
												+ "/" + cabinClassCode + "#");
									} else {
										flightManifestDTO.setBookingClass(rs.getString("booking_code") + "/" + logicalCCCode
												+ "/" + cabinClassCode);
									}
								} else {
									if (bcType != null && bcType.equals(BookingClass.BookingClassType.STANDBY)) {
										// Standby bookings are differentiated by *
										// in booking code
										flightManifestDTO.setBookingClass(rs.getString("booking_code") + "/" + cabinClassCode
												+ "*");
									} else if (bcType != null && bcType.equals(BookingClass.BookingClassType.OPEN_RETURN)) {
										// Open Return segments are differentiated
										// by # in booking code
										flightManifestDTO.setBookingClass(rs.getString("booking_code") + "/" + cabinClassCode
												+ "#");
									} else {
										flightManifestDTO.setBookingClass(rs.getString("booking_code") + "/" + cabinClassCode);
									}
								}
							} else {
								flightManifestDTO.setBookingClass("");
							}

							flightManifestDTO.setCabinClassCode(cabinClassCode);
							flightManifestDTO.setCabinClassName(rs.getString("cabin_class_name"));

							flightManifestDTO.setLogicalCCCode(rs.getString("logical_cabin_class_code"));
							flightManifestDTO.setLogicalCCName(rs.getString("logical_cabin_class_name"));

							flightManifestDTO.setCurrency(rs.getString("Currency"));

							// Build passenger name
							paxType = BeanUtils.nullHandler(rs.getString("pax_type_code"));
							paxTitle = BeanUtils.nullHandler(rs.getString("title"));
							paxName = BeanUtils.nullHandler(rs.getString("PassengerName"));
							flightManifestDTO.setPassengerType(paxType);
							flightManifestDTO.setPassengerTitle(paxTitle);

							if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
								if (!paxTitle.equals("")) {
									passengerName = paxTitle + ". " + paxName;
								} else {
									passengerName = paxName;
								}
							} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
								passengerName = "CHD " + paxName;
							} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
								String strTitle = "";
								if (!paxTitle.isEmpty()) {
									strTitle = paxTitle + ". ";
								}
								passengerName = "INF " + strTitle + paxName;
							} else {
								throw new CommonsDataAccessException("airreservations.arg.unidentifiedPaxTypeDetected");
							}

							flightManifestDTO.setPassengerName(passengerName);

							paxStatus = (rs.getString("PAXStatus") == null ? "" : rs.getString("PAXStatus"));

							// Find correct description
							// 'C' - CNF
							// 'F' - Flown
							// 'N' - NOSHO
							// 'R' - NOREC
							// 'G' - GOSHO
							// 'O' - OFFLK

							if (paxStatus.equals("C")) {
								paxStatus = "CNF";
							} else if (paxStatus.equals("F")) {
								paxStatus = "FLOWN";
							} else if (paxStatus.equals("N")) {
								paxStatus = "NOSHO";
							} else if (paxStatus.equals("R")) {
								paxStatus = "NOREC";
							} else if (paxStatus.equals("G")) {
								paxStatus = "GOSHO";
							} else if (paxStatus.equals("O")) {
								paxStatus = "OFFLK";
							}

							flightManifestDTO.setPaxStatus(paxStatus);
							flightManifestDTO.setPnr(rs.getString("pnr"));

							if (searchTriggerdByDifferentCarrier) {
								flightManifestDTO.setOriginatorPnr(rs.getString("pnr"));
							} else {
								String originatorPnr = rs.getString("originator_pnr");
								if (originatorPnr != null && !originatorPnr.isEmpty()) {
									flightManifestDTO.setOriginatorPnr(originatorPnr);
								} else {
									flightManifestDTO.setOriginatorPnr("-1");
								}
							}

							Integer pnrPaxID = new Integer(rs.getInt("PNR_PAX_ID"));
							Integer pnrSegID = new Integer(rs.getInt("PNR_SEG_ID"));
							flightManifestDTO.setPnrPaxId(pnrPaxID);
							if (rs.getObject("ADULT_ID") != null) {
								flightManifestDTO.setAdultPassengerID(new Integer(rs.getInt("ADULT_ID")));
							}

							flightManifestDTO.setReservationStatus(rs.getString("ResStatus"));
							flightManifestDTO.setReleaseTimeStamp(rs.getString("release_timestamp"));

							// if FOID have expiry date & issued place split it (This is for old data)
							String foid = rs.getString("foidNumber");
							Date exDt = rs.getDate("foidExpiry");

							String foidNo = "";
							String foidExpiry = "";
							String foidPlace = "";

							if (exDt != null) {
								SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
								foidExpiry = sf.format(exDt);
							}

							foidPlace = rs.getString("foidIssuedCntry");

							String[] foidArr = BeanUtils.nullHandler(foid).split(
									ReservationInternalConstants.PassengerConst.FOID_SEPARATOR);

							if (foidArr.length == 3) {
								foidNo = foidArr[0];

								if ((foidExpiry == null || foidExpiry.equals("")) && foidArr[1] != null) {
									foidExpiry = foidArr[1];
								}

								if ((foidPlace == null || foidPlace.equals("")) && foidArr[2] != null) {
									foidPlace = foidArr[2];
								}

							} else if (foidArr.length == 2) {
								foidNo = foidArr[0];

								if ((foidExpiry == null || foidExpiry.equals("")) && foidArr[1] != null) {
									foidExpiry = foidArr[1];
								}

							} else if (foidArr.length == 1) {
								foidNo = foidArr[0];
							}

							if (foidNo != null) {
								flightManifestDTO.setFoidNumber(foidNo);
							} else {
								flightManifestDTO.setFoidNumber("");
							}

							if (foidExpiry != null) {
								flightManifestDTO.setFoidPsptExpiry(foidExpiry);
							} else {
								flightManifestDTO.setFoidPsptExpiry("");
							}

							if (foidPlace != null) {
								flightManifestDTO.setFoidPsptIssuedPlace(foidPlace);
							} else {
								flightManifestDTO.setFoidPsptIssuedPlace("");
							}

							flightManifestDTO.setPaxCategory(rs.getString("paxCategory"));
							flightManifestDTO.setNationality(rs.getString("nationality"));
							String eticket = rs.getString("eticket");
							if (eticket != null) {
								String[] values = eticket.split("-");
								if (values[1].length() != 1) {
									flightManifestDTO.setEticket(values[1]);
								} else {
									flightManifestDTO.setEticket(values[0]);
								}
							} else {
								flightManifestDTO.setEticket("");
							}
							flightManifestDTO.setCc4Digit(rs.getString("cc_digits"));

							flightManifestDTO.setCredit(AppSysParamsUtil.getBaseCurrency());

							GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
							if (globalConfig.getBizParam(SystemParamKeys.SHOW_SEAT_MAP).equalsIgnoreCase("Y")) {
								flightManifestDTO.setSeatCode(rs.getString("SEAT_CODE"));
							}

							// Show meal code only if the meal parameter is on
							String showMeal = globalConfig.getBizParam(SystemParamKeys.SHOW_MEAL);
							if (showMeal.equalsIgnoreCase("Y")) {
								// flightManifestDTO.setMealCode(rs.getString("MEAL_CODE")!=null
								// ?rs.getString("MEAL_CODE").replace(',', '\n'):rs.getString("MEAL_CODE"));
								String mealString = null;
								if (rs.getString("MEAL_CODE") != null) {
									if (AppSysParamsUtil.isMultipleMealSelectionEnabled()) {
										StringBuffer mealStringBuf = new StringBuffer();
										String meals[] = rs.getString("MEAL_CODE").split(",");

										Map<String, Integer> mealsWithCount = new HashMap<String, Integer>();

										for (String meal : meals) {
											if (mealsWithCount.get(meal) == null) {
												mealsWithCount.put(meal, 1);
											} else {
												int previousMealCount = mealsWithCount.get(meal);
												mealsWithCount.put(meal, ++previousMealCount);
											}
										}

										int i = 1;
										for (String meal : mealsWithCount.keySet()) {
											if (i == mealsWithCount.size()) {
												mealStringBuf.append(mealsWithCount.get(meal) + "x" + meal);
											} else {
												mealStringBuf.append(mealsWithCount.get(meal) + "x" + meal + "\n");
											}
											++i;
										}
										mealString = mealStringBuf.toString();
									} else {
										mealString = rs.getString("MEAL_CODE").replace(',', '\n');
									}
								}

								flightManifestDTO.setMealCode(mealString);
							}

							String showBaggage = globalConfig.getBizParam(SystemParamKeys.SHOW_BAGGAGE);
							if (showBaggage.equalsIgnoreCase("Y")) {
								if (rs.getString("BAGGAGE_CODE") != null) {
									String baggageValues[] = rs.getString("BAGGAGE_CODE").split("<>");
									flightManifestDTO.setBaggageCode(baggageValues[0]);
									flightManifestDTO.setBaggageWeight(new Double(baggageValues[1]));
								} else {
									flightManifestDTO.setBaggageCode("");
									flightManifestDTO.setBaggageWeight(0);
								}

							}

							flightManifestDTO.setBookingDate(rs.getString("bktimestamp"));

							/**
							 * JIRA : AARESAA - 2675 --> Include a new column to show Hala services booked by each
							 * passenger .
							 */
							flightManifestDTO.setHalaServices(getHalaServices(pnrPaxID, pnrSegID));

							String autoCancelExpireTime = null;
							if (rs.getString("expire_on") != null) {
								autoCancelExpireTime = rs.getString("expire_on");
							}
							flightManifestDTO.setAutoCancelExpireTime(autoCancelExpireTime);

							flightManifestDTO.setConnectionTime(rs.getDouble("connection_time"));

							mapPnrPaxSegIds.put(pnrPaxID, pnrSegID);

							// Add the DTO to Collection
							flightManifestDTOs.add(flightManifestDTO);
						}
						/**
						 * Added by Lalanthi 25/09/2009 - To incorporate new DB change for SSR/HALA
						 */
						appendSSRInfoToFlightManifest(flightManifestDTOs, mapPnrPaxSegIds);

						return flightManifestDTOs;
					}
				});

		// To sort with Segment Code
		if (isSortOnd) {
			Collections.sort(flightManifestDTOs);
		}

		if (manifestOptions.getSortOption().equalsIgnoreCase("ConnectionTime")) {
			Collections.sort(flightManifestDTOs, new Comparator<FlightManifestRecordDTO>() {
				@Override
				public int compare(FlightManifestRecordDTO o1, FlightManifestRecordDTO o2) {
					if (o1.getMinimumConnectionTime() > o2.getMinimumConnectionTime()) {
						return 1;
					} else if (o1.getMinimumConnectionTime() < o2.getMinimumConnectionTime()) {
						return -1;
					}
					return 0;
				}
			});
		}

		return flightManifestDTOs;
	}

	@Override
	public String getFlightPassengerSummary(FlightManifestOptionsDTO manifestOptions) {

		DataSource ds = ReservationModuleUtils.getDataSource(manifestOptions.getDataSourceMode());
		JdbcTemplate template = new JdbcTemplate(ds);

		String dateFrom = null;
		try{
			dateFrom = CalendarUtil.convertDate(manifestOptions.getFlightDate(),CalendarUtil.PATTERN1,CalendarUtil.PATTERN12);
		} catch (Exception e){
			log.error("Error while generating getFlightPassengerSummary " + e.getMessage());
		}
		String flightNumber = manifestOptions.getFlightNo();
		StringBuilder sb = new StringBuilder();
		sb.append("select count(passenger.pnr_pax_id) as NUMBER_OF_PASSENGERS,trim(f_convert_message(messages.message_content)) as BUNDLE_NAME " +
				"from t_reservation reserv " +
				"inner join t_pnr_passenger passenger on reserv.pnr = passenger.pnr " +
				"inner join t_pnr_segment pnrSeg on pnrSeg.pnr = reserv.pnr " +
				"inner join T_FLIGHT_SEGMENT flightSeg on flightSeg.flt_seg_id = pnrSeg.flt_seg_id " +
				"inner join t_flight flight on flight.FLIGHT_ID = flightSeg.FLIGHT_ID " +
				"inner join t_bundled_fare_period bundledFarePeriod on pnrSeg.bundled_fare_period_id = bundledFarePeriod.BUNDLED_FARE_PERIOD_ID " +
				"inner join t_bundled_fare bundledFare on bundledFare.bundled_fare_id = bundledFarePeriod.bundled_fare_id " +
				"inner join t_bundled_fare_category bundledCategory on bundledFare.bundled_fare_category_id = bundledCategory.bundled_fare_category_id "
				+ "inner join  t_i18n_message messages on messages.I18N_MESSAGE_KEY = ('BUNDLE_CATEGORY_NAME' || bundledCategory.bundled_fare_category_id) ");
		sb.append("and pnrSeg.status <> 'CNX'  and passenger.pax_type_code <> 'IN' ");
		sb.append("AND (flightSeg.est_time_departure_local between TO_DATE('" + dateFrom + " 00:00:00', 'dd-mon-yyyy HH24:mi:ss') " +
				"AND TO_DATE('" + dateFrom + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss'))");
		if (flightNumber != null && !flightNumber.isEmpty()) {
			sb.append("and flight.flight_number = '" + flightNumber + "' ");
		}
		sb.append("group by f_convert_message(messages.message_content)");

		return template.query(sb.toString(), new ResultSetExtractor<String>() {
			@Override
			public String extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				boolean isEmpty = !resultSet.isBeforeFirst();
				if (isEmpty) {
					return "No passengers with bundled meals";
				}
				StringBuilder passengersSummary = new StringBuilder("");
				StringJoiner sj = new StringJoiner(",");
				while (resultSet.next()) {
					String bundledName = resultSet.getString("BUNDLE_NAME");
					String number = resultSet.getString("NUMBER_OF_PASSENGERS");
					passengersSummary.append(bundledName + ": " + number + " passengers,");
				}
				return passengersSummary.toString().replaceAll(",$", "");
			}
		});
	}

	private String getSeatCodeSQL(int flightId) {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		String showSeat = globalConfig.getBizParam(SystemParamKeys.SHOW_SEAT_MAP);
		if (showSeat.equalsIgnoreCase("Y")) {
			String sql = " F_CONCATENATE_LIST(CURSOR(SELECT (modelseat.seat_code) AS SEAT_CODE  FROM sm_t_pnr_pax_seg_am_seat paxseat  ,"
					+ "    sm_t_flight_am_seat amseat , sm_t_am_seat_charge seatcharge, sm_t_aircraft_model_seats modelseat, T_FLIGHT_SEGMENT SEG, "
					+ "    T_PNR_SEGMENT PNS"
					+ "    WHERE pax.pnr_pax_id   = paxseat.pnr_pax_id  AND paxseat.flight_am_seat_id = amseat.flight_am_seat_id "
					+ "        AND amseat.ams_charge_id = seatcharge.ams_charge_id "
					+ "        AND seatcharge.am_seat_id = modelseat.am_seat_id "
					+ "        AND paxseat.pnr_seg_id =   PNS.pnr_seg_id"
					+ "        AND PNS.status =  'CNF' "
					+ "        AND paxseat.status = 'RES'"
					+ " 	   AND PNS.flt_seg_id = SEG.flt_seg_id "
					+ "  	   AND SEG.flight_id  =" + flightId + " ) ,',')  AS SEAT_CODE , ";

			return sql;
		} else
			return "";
	}

	/**
	 * Appends SQL part related to Meal code, only if the ShowMeal parameter is set to 'Y'
	 * 
	 * @param flightId
	 * @return sql string
	 */

	private String getMealCodeSQL(int flightId) {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		String showMeal = globalConfig.getBizParam(SystemParamKeys.SHOW_MEAL);
		if (showMeal.equalsIgnoreCase("Y")) {
			String sql = " F_CONCATENATE_LIST(CURSOR(SELECT (MEAL_CODE) AS MEAL_CODE  FROM ML_T_MEAL M  ,"
					+ "    ML_T_PNR_PAX_SEG_Meal PSM, T_PNR_SEGMENT PNS , T_FLIGHT_SEGMENT SEG "
					+ "    WHERE M.meal_id  = PSM.meal_id  AND PSM.status     ='RES' "
					+ "    AND PSM.pnr_seg_id = PNS.pnr_seg_id  AND PNS.flt_seg_id =SEG.flt_seg_id " + "  AND SEG.flight_id  ="
					+ flightId + "  AND PSM.pnr_pax_id = pax.pnr_pax_id ) ,',')  AS MEAL_CODE , ";

			return sql;
		} else {
			return "";
		}
	}

	/**
	 * Appends SQL part related to Baggage code, only if the ShowBaggage parameter is set to 'Y'
	 * 
	 * @param flightId
	 * @return sql string
	 */
	private String getBaggageCodeSQL(int flightId) {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		String showBaggage = globalConfig.getBizParam(SystemParamKeys.SHOW_BAGGAGE);
		if (showBaggage.equalsIgnoreCase("Y")) {
			String sql = "(SELECT (nvl(B.BAGGAGE_NAME,'') || '<>' || nvl(B.BAGGAGE_WEIGHT,'0')) as BAGGAGE_CODE "
					+ "           FROM  BG_T_BAGGAGE B,BG_T_PNR_PAX_SEG_BAGGAGE PSB,T_PNR_SEGMENT PNS,T_FLIGHT_SEGMENT SEG "
					+ "           WHERE  B.baggage_id = PSB.baggage_id " + "           AND PSB.status='RES' "
					+ "           AND PSB.pnr_pax_id =pax.pnr_pax_id " + "           AND PSB.pnr_seg_id= PNS.pnr_seg_id "
					+ "           AND PNS.flt_seg_id=SEG.flt_seg_id and PNS.status = 'CNF' and SEG.flight_id=" + flightId
					+ "           AND rownum = 1 " + "   ) as BAGGAGE_CODE,";
			return sql;
		} else {
			return "";
		}
	}

	/**
	 * This method will append SSR Info to pnr passenger appeared in FlightManifest.
	 * 
	 * @author lalanthi - 25/09/2009 (To Incorporate HALA/SSR DB changes)
	 * @param flightManifestDTOs
	 * @param mapPnrPaxSegIds
	 *            JIRA :AARESAA-2675 (Issue 4)
	 */
	private void appendSSRInfoToFlightManifest(Collection<FlightManifestRecordDTO> flightManifestDTOs,
			Map<Integer, Integer> mapPnrPaxSegIds) {
		if (mapPnrPaxSegIds != null && mapPnrPaxSegIds.size() > 0) {
			Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxSSRMap = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
					.getPNRPaxSSRs(mapPnrPaxSegIds, false, false, false);
			for (FlightManifestRecordDTO flightManifestDTO : flightManifestDTOs) {
				Integer pnrPaxID = flightManifestDTO.getPnrPaxId();
				String ssrCode = flightManifestDTO.getSsr();
				String ssrText = flightManifestDTO.getSsrText();
				Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxSSRMap.get(pnrPaxID);
				String[] ssrData = PNLUtil.getPaxSSRData(paxSSRDetailDTOs, null);

				if (ssrCode == null || "".equals(ssrCode)) {
					ssrCode = ssrData[0];
					ssrText = ssrData[1];
				} else {
					ssrText += ssrData[0] + " " + ssrData[1];
				}
				flightManifestDTO.setSsr(ssrCode);
				flightManifestDTO.setSsrText(ssrText);
			}
		}
	}

	/**
	 * Get origin station id of the given pnr, departure station and flight id
	 */
	@SuppressWarnings("rawtypes")
	private String
			getPNROriginStationForFlight(String pnr, String segmentCode, int flightId, boolean confirmedReservationSegment) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String departureStation = segmentCode.substring(0, 3);
		String defaultOriginStationId = departureStation;
		String originStationId = "";

		Double maxConnectTime = null;

		Double minConnectTime = null;

		Double minMaxConnectionTime[] = getMinMaxConnectionTimeFromHub(departureStation);

		if (minMaxConnectionTime != null) {
			maxConnectTime = minMaxConnectionTime[1];
			minConnectTime = minMaxConnectionTime[0];
		} else {
			maxConnectTime = getMaximumConnectionTime();
			minConnectTime = getMinimumConnectionTime();
		}

		final double doubleMinConnectTime = (minConnectTime).doubleValue();

		// Get HashMap containing CurrentPNRSegmentArrivalTime,
		// CurrentPNRSegmentDestination and CurrentPNRSegmentDepartingStation
		HashMap currentPNRSegmentRecord = getCurrentPNRSegment(pnr, departureStation, flightId, confirmedReservationSegment);

		if (currentPNRSegmentRecord == null || currentPNRSegmentRecord.isEmpty()) {
			return "";
		}

		HashMap consideringPNRSegmentRecord = currentPNRSegmentRecord;

		while (!consideringPNRSegmentRecord.isEmpty()) {

			// Build OutBound DTO with PNR segment details
			// InBound arrival station should not be equal to the current
			// segments departing station
			String pnrSegSQL = "  select s.SEGMENT_CODE "
					+ "         , to_char(s.est_time_arrival_local, 'DD-MON-YYYY HH24:MI:SS') ARRIVAL_LOCAL"
					+ "         , substr(s.segment_code,length(s.segment_code) - 2,3) destination "
					+ "         , substr(s.segment_code,1,3) departingStation " + "         , ( TO_DATE('"
					+ consideringPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime) + "'"
					+ "                 ,'DD-MON-YYYY HH24:MI:SS')" + "              - s.est_time_arrival_local ) connectTime  "
					+ "         , to_char(s.est_time_departure_local, 'DD-MON-YYYY HH24:MI:SS') DEPARTURE_LOCAL "
					+ "    from v_ext_pnr_segment s  " + "   where s.pnr = '" + pnr + "' " + "     and s.segment_code like '%/"
					+ consideringPNRSegmentRecord.get(CurrentPNRSegmentDepartingStation) + "' " + "     and s.pnr_status = '";

			if (confirmedReservationSegment) {
				pnrSegSQL += ReservationInternalConstants.ReservationStatus.CONFIRMED;
			} else {
				pnrSegSQL += ReservationInternalConstants.ReservationStatus.CANCEL;
			}

			pnrSegSQL += "' " + "     and substr(s.segment_code,1,3) <> '"
					+ currentPNRSegmentRecord.get(CurrentPNRSegmentDestination) + "' "
					+ "   order by s.est_time_departure_local desc ";
				
			// Use est_time_departure_local since it's the most accurate option
			// Nili 8:57 PM 2/12/2010. Please don't use seqment seq since it's not currently consistent. Although with
			// this implementation
			// It's just a matter of Oracle's NUMBER vs TIMESTAMP.

			final double doubleMaxConnectTime = (maxConnectTime).doubleValue();
			// Build inbound flight details
			HashMap inboundDetailsMap = (HashMap) templete.query(pnrSegSQL, new ResultSetExtractor() {

				@SuppressWarnings("unchecked")
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap inboundDetailsMap = new HashMap();

					while (rs.next()) {
						if ((rs.getDouble("connectTime") > 0) && (rs.getDouble("connectTime") <= doubleMaxConnectTime)
								&& (rs.getDouble("connectTime") >= doubleMinConnectTime)) {
							/**
							 * Bussiness Rule: Consider only earliest departing flights within maximum connection time
							 */

							// Build the next considering PNR Segment
							// details
							HashMap<String, String> consideringPNRSegmentRecord = new HashMap<String, String>();
							consideringPNRSegmentRecord.put(CurrentPNRSegmentArrivalTime, rs.getString("ARRIVAL_LOCAL"));
							consideringPNRSegmentRecord.put(CurrentPNRSegmentDestination, rs.getString("destination"));
							consideringPNRSegmentRecord.put(CurrentPNRSegmentDepartingStation, rs.getString("departingStation"));
							consideringPNRSegmentRecord.put(CurrentPNRSegmentDepartureTime, rs.getString("DEPARTURE_LOCAL"));

							// Prepair output details
							inboundDetailsMap.put("OriginStationId", rs.getString("SEGMENT_CODE").substring(0, 3));
							inboundDetailsMap.put("ConsideringPNRSegmentRecord", consideringPNRSegmentRecord);

							return inboundDetailsMap;
						}
					}
					return inboundDetailsMap;
				}
			});
			// Save the the last origin
			if (!inboundDetailsMap.isEmpty()) {
				originStationId = (String) inboundDetailsMap.get("OriginStationId");
				// Get the next considering PNR Segment record
				consideringPNRSegmentRecord = (HashMap) inboundDetailsMap.get("ConsideringPNRSegmentRecord");
			} else {
				consideringPNRSegmentRecord = new HashMap();
			}
		}

		return (originStationId.equals("") ? defaultOriginStationId : originStationId);
	}

	/**
	 * Retrieves all Hala services booked per pnr, per pax
	 * 
	 * @param pnr_pax_id
	 * @param pnr_seg_id
	 * @return String - hala services booked. JIRA : AARESAA - 2675
	 */
	private String getHalaServices(int pnr_pax_id, int pnr_seg_id) {
		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		String showHala = globalConfig.getBizParam(SystemParamKeys.SHOW_AIRPORT_SERVICES);
		if (showHala.equalsIgnoreCase("Y")) {
			StringBuffer halaServices = new StringBuffer();
			String halaSQL = "SELECT chg.ssr_charge_code as HALASERVICES "
					+ "FROM t_pnr_pax_fare fare,t_pnr_pax_fare_segment seg,t_pnr_pax_segment_ssr ssr, t_ssr_info info , t_ssr_charge chg "
					+ "WHERE fare.pnr_pax_id IN (" + pnr_pax_id + ") " + "     AND seg.pnr_seg_id IN (" + pnr_seg_id + ") "
					+ "    AND fare.ppf_id = seg.ppf_id " + "    AND info.ssr_id = ssr.ssr_id "
					+ "    AND seg.ppfs_id = ssr.ppfs_id " + "    AND chg.ssr_charge_id = ssr.context_id "
					+ "ORDER BY PNR_PAX_ID ";
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate template = new JdbcTemplate(ds);
			halaServices = (StringBuffer) template.query(halaSQL, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					StringBuffer halaServices = new StringBuffer();
					while (rs.next()) {
						halaServices.append(rs.getString("HALASERVICES"));
						halaServices.append(" ");
					}
					return halaServices;
				}
			});

			return halaServices.toString();
		} else {
			return "";
		}
	}

	/* ##################################################################### */
	@Override
	public InboundConnectionDTO getInBoundConnectionList(String pnr, int id, String departureStation, int flightId) {
		return getInBoundConnectionList(pnr, true, departureStation, flightId);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public InboundConnectionDTO getInBoundConnectionList(String pnr, boolean confirmedReservationSegment,
			String departureStation, int flightId) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Double maxConnectTime = null;
		Double minConnectTime = null;

		Double minMaxConnectionTime[] = getMinMaxConnectionTimeFromHub(departureStation);

		if (minMaxConnectionTime != null) {
			maxConnectTime = minMaxConnectionTime[1] * -1;
			minConnectTime = minMaxConnectionTime[0] * -1;
		} else {
			maxConnectTime = getMaximumConnectionTime() * -1;
			minConnectTime = getMinimumConnectionTime() * -1;
		}

		final double doubleMinConnectTime = (minConnectTime).doubleValue();

		String currentPNRSegmentSQL = "select * from "
				+ " (select to_char(s.EST_TIME_DEPARTURE_LOCAL, 'DD-MON-YYYY HH24:MI:SS') DEPARTURE_LOCAL"
				+ "         , substr(s.segment_code,length(s.segment_code) - 2,3) destination " + "   from V_PNR_SEGMENT s  "
				+ "  where s.pnr = '" + pnr + "' " + "   and s.segment_code like '" + departureStation + "/%' "
				+ "   and s.pnr_status = '";

		if (confirmedReservationSegment) {
			currentPNRSegmentSQL += ReservationInternalConstants.ReservationStatus.CONFIRMED;
		} else {
			currentPNRSegmentSQL += ReservationInternalConstants.ReservationStatus.CANCEL;
		}

		currentPNRSegmentSQL += "' " + "   and s.flight_id =" + flightId + "" + "  order by SEGMENT_SEQ )"
				+ " where rownum = 1  ";

		HashMap currentPNRSegmentRecord = (HashMap) templete.query(currentPNRSegmentSQL, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				HashMap<String, String> currentPNRSegmentRecord = new HashMap<String, String>();
				if (rs.next()) {
					currentPNRSegmentRecord.put(CurrentPNRSegmentDepartureTime, rs.getString("DEPARTURE_LOCAL"));
					currentPNRSegmentRecord.put(CurrentPNRSegmentDestination, rs.getString("destination"));
				}
				return currentPNRSegmentRecord;
			}
		});

		if (currentPNRSegmentRecord == null || currentPNRSegmentRecord.isEmpty()) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT * ");
		sb.append(" FROM ");
		sb.append("   (SELECT f.FLIGHT_NUMBER, ");
		sb.append("     fs.SEGMENT_CODE, ");
		sb.append("     fs.EST_TIME_DEPARTURE_LOCAL , ");
		sb.append("     ppfs.BOOKING_CODE, ");
		sb.append("     bc.CABIN_CLASS_CODE                          AS CABIN_CLASS_CODE, ");
		sb.append("     TO_CHAR(fs.EST_TIME_ARRIVAL_LOCAL ,'HH24MI') AS arrivalTime, ");
		sb.append("     fs.EST_TIME_ARRIVAL_LOCAL , ");
		sb.append("     (fs.EST_TIME_ARRIVAL_LOCAL        - TO_DATE('");
		sb.append( currentPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime));
		sb.append("     ' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime, ");
		sb.append("     (TRUNC(fs.EST_TIME_ARRIVAL_LOCAL) - TRUNC(fs.EST_TIME_DEPARTURE_LOCAL) ) AS DATEDIFF ");
		sb.append("   FROM t_pnr_segment ps, ");
		sb.append("     t_flight_segment fs, ");
		sb.append("     t_flight f, ");
		sb.append("     T_PNR_PAX_FARE_SEGMENT ppfs, ");
		sb.append("     t_booking_class bc ");
		sb.append("   WHERE ps.pnr      = '");
		sb.append(pnr);
		sb.append("'   AND ps.flt_seg_id = fs.flt_seg_id ");
		sb.append("   AND fs.flight_id  = f.flight_id ");
		sb.append("   AND ps.pnr_seg_id =ppfs.pnr_seg_id ");
		sb.append("   AND fs.segment_code LIKE '%/");
		sb.append(departureStation);
		sb.append("'   AND bc.booking_code(+)   =ppfs.booking_code ");
		
		if (!AppSysParamsUtil.isSendInwardSurfaceSegmentInPNLADL()) {
			sb.append("   AND f.operation_type_id !=");
			sb.append(AirScheduleCustomConstants.OperationTypes.BUS_SERVICE);
		}
			
		if (confirmedReservationSegment) {
			sb.append("   AND ps.status            = '");
			sb.append(ReservationInternalConstants.ReservationStatus.CONFIRMED );
			sb.append("'");
		}
		
		sb.append("   UNION ALL ");
		sb.append("   SELECT TXFS.FLIGHT_NUMBER, ");
		sb.append("     TXFS.SEGMENT_CODE, ");
		sb.append("     TXFS.EST_TIME_DEPARTURE_LOCAL , ");
		sb.append("     NULL BOOKING_CODE, ");
		sb.append("     TXPS.CABIN_CLASS_CODE, ");
		sb.append("     TO_CHAR(TXFS.EST_TIME_ARRIVAL_LOCAL ,'HH24MI') AS arrivalTime, ");
		sb.append("     TXFS.EST_TIME_ARRIVAL_LOCAL , ");
		sb.append("     (TXFS.EST_TIME_ARRIVAL_LOCAL        - TO_DATE('");
		sb.append(currentPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime));
		sb.append("' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime, ");
		sb.append("     (TRUNC(TXFS.EST_TIME_ARRIVAL_LOCAL) - TRUNC(TXFS.EST_TIME_DEPARTURE_LOCAL) ) AS DATEDIFF ");
		sb.append("   FROM T_EXT_FLIGHT_SEGMENT TXFS, ");
		sb.append("     T_EXT_PNR_SEGMENT TXPS ");
		sb.append("   WHERE TXFS.EXT_FLT_SEG_ID= TXPS.EXT_FLT_SEG_ID ");
		sb.append("   AND TXPS.pnr             = '");
		sb.append(pnr);
		sb.append("'   AND TXFS.segment_code LIKE '%/");
		sb.append(departureStation);
		sb.append("'   AND TXPS.status = '");
		sb.append(ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED);
		sb.append("' ");
		
		if (AppSysParamsUtil.isSendIbObConnectionsForOtherAirlineSegments()) {	
			sb.append("   UNION ALL ");
			sb.append("   SELECT s.FLIGHT_NUMBER , ");
			sb.append("     CONCAT(CONCAT(s.ORIGIN, '/'), s.DESTINATION) SEGMENT_CODE, ");
			sb.append("     s.EST_TIME_DEPARTURE_LOCAL , ");
			sb.append("     s.BOOKING_CODE, ");
			sb.append("     ''                                          AS CABIN_CLASS_CODE , ");
			sb.append("     TO_CHAR(s.EST_TIME_ARRIVAL_LOCAL ,'HH24MI') AS arrivalTime, ");
			sb.append("     s.EST_TIME_ARRIVAL_LOCAL, ");
			sb.append("     (s.EST_TIME_ARRIVAL_LOCAL        - TO_DATE('");
			sb.append(currentPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime));
			sb.append("' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime, ");
			sb.append("     (TRUNC(s.EST_TIME_ARRIVAL_LOCAL) - TRUNC(s.EST_TIME_DEPARTURE_LOCAL) ) AS DATEDIFF ");
			sb.append("   FROM T_PNR_OTHER_AIRLINE_SEGMENT s ");
			sb.append("   WHERE s.pnr = '");
			sb.append(pnr);
			sb.append("'   AND concat(concat(s.ORIGIN, '/'), s.DESTINATION) LIKE '%/");
			sb.append(departureStation);
			sb.append("'   AND s.status = '");
			sb.append(ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED);
			sb.append("' ");
		}
	
		sb.append("   ) ");
		sb.append(" ORDER BY EST_TIME_ARRIVAL_LOCAL DESC ");


		final double negativeMaxConnectTime = (maxConnectTime).doubleValue();
		InboundConnectionDTO inboundConnectionDTO = (InboundConnectionDTO) templete.query(sb.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				InboundConnectionDTO inboundConnectionDTO = new InboundConnectionDTO();
				while (rs.next()) {
					if ((rs.getDouble("connectTime") < 0) && (rs.getDouble("connectTime") >= negativeMaxConnectTime)
							&& (rs.getDouble("connectTime") <= doubleMinConnectTime)) {
						/**
						 * Bussiness Rule: Consider only latest arriving flights within maximum connection time
						 */
						inboundConnectionDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
						String booking_code = rs.getString("BOOKING_CODE");

							if(booking_code != null && !booking_code.isEmpty()){
								if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
									if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
										inboundConnectionDTO.setFareClass(rs.getString("CABIN_CLASS_CODE"));
									} else {
										inboundConnectionDTO.setFareClass(String.valueOf(booking_code.charAt(0)));
									}
								} else {
									inboundConnectionDTO.setFareClass(rs.getString("CABIN_CLASS_CODE"));
								}
							}else{
								inboundConnectionDTO.setFareClass("Y");
							}
						
						// Get the day number of the month in String
						// format of "99"
						Date localDepartureDate = rs.getDate("EST_TIME_DEPARTURE_LOCAL");
						Calendar localDepartureCalendar = Calendar.getInstance();
						localDepartureCalendar.setTime(localDepartureDate);

						if (localDepartureCalendar.get(Calendar.DAY_OF_MONTH) < 10) {
							inboundConnectionDTO.setDate("0" + String.valueOf(localDepartureCalendar.get(Calendar.DAY_OF_MONTH)));
						} else {
							inboundConnectionDTO.setDate(String.valueOf(localDepartureCalendar.get(Calendar.DAY_OF_MONTH)));
						}

						inboundConnectionDTO.setDepartureStation(rs.getString("SEGMENT_CODE").substring(0, 3));
						inboundConnectionDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
						
						inboundConnectionDTO.setDateDifference(rs.getInt("dateDiff"));
						
						SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
						
						Date arrivalDate = rs.getDate("EST_TIME_ARRIVAL_LOCAL");
						String arrivalTime = rs.getString("arrivalTime");
						
						if(arrivalTime != null && !arrivalTime.isEmpty()){
							inboundConnectionDTO.setArrivalTime(arrivalTime);
						}
						return inboundConnectionDTO;
					}
				}
				return null;
			}
		});

		return inboundConnectionDTO;

	}

	/**
	 * Get current segment departure time and the destination airport
	 * 
	 * Business Rule: In case there are two PNR Segments starting with the same departure station and having the same
	 * flight id, the bussiness rule is to consider minimum PNR Segment Sequenced record.
	 * 
	 * @param pnr
	 * @param departureStation
	 * @param flightId
	 * @return HashMap containing CurrentPNRSegmentArrivalTime, CurrentPNRSegmentDestination and
	 *         CurrentPNRSegmentDepartingStation
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, String> getCurrentPNRSegment(String pnr, String departureStation, int flightId,
			boolean confirmedReservationSegment) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		/*
		 * Following SQL changed to consider confirmed reservations for additions and cancelled reservations for
		 * deletions
		 * 
		 * String currentPNRSegmentSQL = "select * from " + " (select to_char(s.EST_TIME_ARRIVAL_ZULU, 'DD-MON-YYYY
		 * HH24:MI:SS') ARRIVAL_ZULU" + " , substr(s.segment_code,length(s.segment_code) - 2,3) destination " + " ,
		 * substr(s.segment_code,1,3) departingStation " + " , to_char(s.EST_TIME_DEPARTURE_ZULU, 'DD-MON-YYYY
		 * HH24:MI:SS') DEPARTURE_ZULU " + " from V_PNR_SEGMENT s " + " where s.pnr = '" + pnr + "' " +
		 * " and s.segment_code like '" + departureStation + "/%' " + " and s.pnr_status = '" +
		 * ReservationInternalConstants.ReservationStatus.CONFIRMED + "' " + " and s.flight_id =" + flightId + "" + "
		 * order by SEGMENT_SEQ )" + " where rownum = 1 ";
		 */

		String currentPNRSegmentSQL = "select * from "
				+ " (select to_char(s.EST_TIME_ARRIVAL_LOCAL, 'DD-MON-YYYY HH24:MI:SS') ARRIVAL_LOCAL"
				+ "         , substr(s.segment_code,length(s.segment_code) - 2,3) destination "
				+ "         , substr(s.segment_code,1,3) departingStation "
				+ "         , to_char(s.EST_TIME_DEPARTURE_LOCAL, 'DD-MON-YYYY HH24:MI:SS') DEPARTURE_LOCAL "
				+ "   from V_PNR_SEGMENT s  " + "  where s.pnr = '" + pnr + "' " + "   and s.segment_code like '"
				+ departureStation + "/%' " + "   and s.pnr_status = '";
		if (confirmedReservationSegment) {
			currentPNRSegmentSQL += ReservationInternalConstants.ReservationStatus.CONFIRMED;
		} else {
			currentPNRSegmentSQL += ReservationInternalConstants.ReservationStatus.CANCEL;
		}
		currentPNRSegmentSQL += "' " + "   and s.flight_id =" + flightId + "" + "  order by SEGMENT_SEQ )"
				+ " where rownum = 1  ";

		HashMap<String, String> currentPNRSegmentRecord = (HashMap<String, String>) templete.query(currentPNRSegmentSQL,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						HashMap<String, String> currentPNRSegmentRecord = new HashMap<String, String>();
						if (rs.next()) {
							currentPNRSegmentRecord.put(CurrentPNRSegmentArrivalTime, rs.getString("ARRIVAL_LOCAL"));
							currentPNRSegmentRecord.put(CurrentPNRSegmentDestination, rs.getString("destination"));
							currentPNRSegmentRecord.put(CurrentPNRSegmentDepartingStation, rs.getString("departingStation"));
							currentPNRSegmentRecord.put(CurrentPNRSegmentDepartureTime, rs.getString("DEPARTURE_LOCAL"));
						}
						return currentPNRSegmentRecord;
					}
				});
		return currentPNRSegmentRecord;
	}

	// pnl bl calls this
	@Override
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId) {
		return getAllOutBoundSequences(pnr, paxId, departureStation, flightId, true, null);
	}

	// adl bl calls this
	@Override
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId,
			boolean confirmedReservationSegment) {
		return getAllOutBoundSequences(pnr, paxId, departureStation, flightId, confirmedReservationSegment, null);
	}

	/**
	 * This method returns the destination of the earliest connection for the specified pnr, flight id and departure
	 * station. If the destination is also specified, method will return the same destination if a match found, else
	 * will return null. If a maxConnection time is specified, it will take into account
	 * 
	 * @param pnr
	 * @param flightId
	 * @param departureStation
	 * @param arrivalStation
	 * @param flightNo
	 * @param confirmedReservationSegment
	 * @param maxConnectTime
	 * @return
	 * @throws DataA
	 */
	public FlightManifestRouteDTO getEarliestOutboundConnectionDestination(String pnr, String currentSegmentCode,
			FlightManifestOptionsDTO options) throws DataAccessException {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final FlightManifestRouteDTO manifestRouteDTO = new FlightManifestRouteDTO();

		String destOfCurrentSegment = currentSegmentCode.substring(currentSegmentCode.length() - 3);
		String originOfCurrentSegment = currentSegmentCode.substring(0, 3);

		Double minConnectTime = null;
		Double maxConnectTime = null;
		// hub defined minimum connection time
		Double minMaxConnectionTime[] = getMinMaxConnectionTimeFromHub(destOfCurrentSegment);

		if (minMaxConnectionTime != null) {
			minConnectTime = minMaxConnectionTime[0];
			maxConnectTime = minMaxConnectionTime[1];
		} else {
			minConnectTime = getMinimumConnectionTime();
			maxConnectTime = getMaximumConnectionTime();
		}

		final double doubleMinConnectTime = (minConnectTime).doubleValue();

		HashMap<String, String> currentPNRSegmentRecord = getCurrentPNRSegment(pnr, originOfCurrentSegment,
				options.getFlightId(), options.isConfirmedReservation());

		String outBoundDest = null;
		if (currentPNRSegmentRecord != null && !currentPNRSegmentRecord.isEmpty()) {

			String sql = " SELECT * FROM (SELECT SUBSTR(s.segment_code,LENGTH(s.segment_code) - 2,3) destination, "
					+ "(s.EST_TIME_DEPARTURE_LOCAL - TO_DATE('" + currentPNRSegmentRecord.get(CurrentPNRSegmentArrivalTime)
					+ "' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime FROM v_ext_pnr_segment s WHERE s.pnr = '" + pnr + "' "
					+ " AND (s.EST_TIME_DEPARTURE_LOCAL - TO_DATE('" + currentPNRSegmentRecord.get(CurrentPNRSegmentArrivalTime)
					+ "' ,'DD-MON-YYYY HH24:MI:SS') ) > 0 AND ";
			if (AppSysParamsUtil.isCityBasedAvailabilityEnabled()) {
				try {
					List<String> sameCityAirports = ReservationModuleUtils.getAirportBD().getSameCityAirports(
							destOfCurrentSegment);
					int count = 0;
					for (String airport : sameCityAirports) {
						if (!"".equals(options.getOutwardConnectionDestination())) {
							sql += (count == 0 ? "(" : " or ") + " s.segment_code LIKE '" + airport + "/%"
									+ options.getOutwardConnectionDestination() + "' ";
						} else {
							sql += (count == 0 ? "(" : " or ") + " s.segment_code LIKE '" + airport + "/%'";
						}
						count++;
					}
					if (count != 0) {
						sql += ") AND ";
					}
				} catch (Exception exception) {
					log.error("Error occured in retrieving same city airports", exception);
					// FIXME: Can we improve on this? Wanted to throw an module exception here.
					throw new DataAccessException("Error occured in retrieving same city airports") {
						private static final long serialVersionUID = 1L;
					};
				}
			} else {
				if (!"".equals(options.getOutwardConnectionDestination())) {
					sql += " s.segment_code LIKE '" + destOfCurrentSegment + "/%" + options.getOutwardConnectionDestination()
							+ "' AND";
				} else {
					sql += " s.segment_code LIKE '" + destOfCurrentSegment + "/%' AND";
				}
			}

			sql += " SUBSTR(s.segment_code,LENGTH(s.segment_code) - 2,3) <> '" + originOfCurrentSegment + "' ";

			if (options.isConfirmedReservation()) {
				sql += "AND s.pnr_status = '" + ReservationInternalConstants.ReservationStatus.CONFIRMED + "' ";
			}

			if (!"".equals(options.getOutwardFlightNo())) {
				sql += "AND s.flight_number = '" + options.getOutwardFlightNo() + "' ";
			}

			sql += "ORDER BY s.EST_TIME_DEPARTURE_LOCAL ) WHERE ROWNUM = 1";

			// Returns only one record if exists

			final double doubleMaxConnectTime;

			if (!"".equals(options.getMaxInwardConnectionTime())) {
				//doubleMaxConnectTime = convertStrTimeToDate(options.getMaxOutwardConnectionTime());
				doubleMaxConnectTime = maxConnectTime;
			} else {
				doubleMaxConnectTime = -1;
			}

			// Check if null pointer occurs
			outBoundDest = (String) templete.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					String destination = null;
					if (rs.next()) {

						// if ((rs.getDouble("connectTime") > 0) && (rs.getDouble("connectTime") >=
						// doubleMinConnectTime)) {
						if ((rs.getDouble("connectTime") > 0)) {
							if (doubleMaxConnectTime > 0) {
								if (rs.getDouble("connectTime") <= doubleMaxConnectTime) {
									destination = rs.getString("destination");
									manifestRouteDTO.setOutboundConnectionTime(rs.getDouble("connectTime"));
								}
							} else {
								manifestRouteDTO.setOutboundConnectionTime(rs.getDouble("connectTime"));
								destination = rs.getString("destination");
							}

							manifestRouteDTO.setDestination(destination);

							// if minimum connection time is less than actual connection time, there could
							// be a possible misconnection
							if (rs.getDouble("connectTime") < doubleMinConnectTime) {
								manifestRouteDTO.setPossibleMissConnection(true);
							}
						}
					}
					return destination;
				}
			});

		}

		return manifestRouteDTO;

	}

	public FlightManifestRouteDTO getLatestInboundConnectionDestination(String pnr, String currentSegmentCode,
			FlightManifestOptionsDTO options) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final FlightManifestRouteDTO manifestRouteDTO = new FlightManifestRouteDTO();

		String destOfCurrentSegment = currentSegmentCode.substring(currentSegmentCode.length() - 3);
		String originOfCurrentSegment = currentSegmentCode.substring(0, 3);

		Double minConnectTime = null;
		// hub defined minimum connection time
		Double minMaxConnectionTime[] = getMinMaxConnectionTimeFromHub(destOfCurrentSegment);

		if (minMaxConnectionTime != null) {
			minConnectTime = minMaxConnectionTime[0];
		} else {
			minConnectTime = getMinimumConnectionTime();
		}

		final double doubleMinConnectTime = (minConnectTime).doubleValue();

		HashMap<String, String> currentPNRSegmentRecord = getCurrentPNRSegment(pnr, originOfCurrentSegment,
				options.getFlightId(), options.isConfirmedReservation());

		String inBoundOrgin = null;
		if (currentPNRSegmentRecord != null && !currentPNRSegmentRecord.isEmpty()) {

			String sql = " SELECT * FROM (SELECT  SUBSTR(s.segment_code,1,3) origin, (TO_DATE('"
					+ currentPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime)
					+ "' ,'DD-MON-YYYY HH24:MI:SS') - s.EST_TIME_ARRIVAL_LOCAL) connectTime FROM v_ext_pnr_segment s "
					+ "WHERE s.pnr = '" + pnr + "' AND ";

			if (AppSysParamsUtil.isCityBasedAvailabilityEnabled()) {
				try {
					List<String> sameCityAirports = ReservationModuleUtils.getAirportBD().getSameCityAirports(
							originOfCurrentSegment);
					int count = 0;
					for (String airport : sameCityAirports) {
						if (!"".equals(options.getInwardConnectionOrigin())) {
							sql += (count == 0 ? "(" : " or ") + " s.segment_code LIKE '" + options.getInwardConnectionOrigin()
									+ "%" + airport + "' ";
						} else {
							sql += (count == 0 ? "(" : " or ") + " s.segment_code LIKE '%/" + airport + "' ";
						}
						count++;
					}
					if (count != 0) {
						sql += " ) AND ";
					}
				} catch (Exception exception) {
					log.error("Error occured in retrieving same city airports", exception);
					// FIXME: Can we improve on this? Wanted to throw an module exception here.
					throw new DataAccessException("Error occured in retrieving same city airports") {
						private static final long serialVersionUID = 1L;
					};
				}
			} else {
				if (!"".equals(options.getInwardConnectionOrigin())) {
					sql += " s.segment_code LIKE '" + options.getInwardConnectionOrigin() + "%" + originOfCurrentSegment
							+ "' AND";
				} else {
					sql += " s.segment_code LIKE '%/" + originOfCurrentSegment + "' AND";
				}
			}
			sql += " SUBSTR(s.segment_code,1,3) <> '" + destOfCurrentSegment + "' ";

			if (options.isConfirmedReservation()) {
				sql += "AND s.pnr_status = '" + ReservationInternalConstants.ReservationStatus.CONFIRMED + "' ";
			}

			if (!"".equals(options.getInwardFlightNo())) {
				sql += "AND s.flight_number = '" + options.getInwardFlightNo() + "' ";
			}

			sql += " and s.pnr_seg_id in "
					+ "(select pnr_seg_id from t_pnr_segment "
					+ "where pnr ='" + pnr + "' and ond_group_id in "
							+ "(select ond_group_id from t_pnr_segment "
							+ "where flt_seg_id in "
							+ "(select flt_seg_id from t_flight_segment where flight_id ='"+options.getFlightId()+"')))";
			
			sql += "ORDER BY s.EST_TIME_ARRIVAL_LOCAL ) WHERE ROWNUM = 1";

			// Returns only one record if exists

			final double doubleMaxConnectTime;

			if (!"".equals(options.getMaxInwardConnectionTime())) {
				doubleMaxConnectTime = convertStrTimeToDate(options.getMaxInwardConnectionTime());
			} else {
				doubleMaxConnectTime = -1;
			}

			// Check if null pointer occurs
			inBoundOrgin = (String) templete.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					String origin = null;
					if (rs.next()) {
						// if ((rs.getDouble("connectTime") > 0) && (rs.getDouble("connectTime") >=
						// doubleMinConnectTime)) {
						if (rs.getDouble("connectTime") > 0) {
							if (doubleMaxConnectTime > 0) {
								if (rs.getDouble("connectTime") <= doubleMaxConnectTime) {
									origin = rs.getString("origin");
									manifestRouteDTO.setInboundConnectionTime(rs.getDouble("connectTime"));
								}
							} else {
								origin = rs.getString("origin");
								manifestRouteDTO.setInboundConnectionTime(rs.getDouble("connectTime"));
							}

							manifestRouteDTO.setOrigin(origin);

							// if minimum connection time is less than actual connection time, there could
							// be a possible misconnection
							if (rs.getDouble("connectTime") < doubleMinConnectTime) {
								manifestRouteDTO.setPossibleMissConnection(true);
							}
						}
					}
					return origin;
				}
			});

		}

		return manifestRouteDTO;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Collection<OnWardConnectionDTO> getAllOutBoundSequences(String pnr, int paxId, String departureStation, int flightId,
			boolean confirmedReservationSegment, String maximumConnectionTime) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Double maxConnectTime = null;
		Double minConnectTime = null;

		HashMap<String, String> currentPNRSegmentRecord = getCurrentPNRSegment(pnr, departureStation, flightId,
				confirmedReservationSegment);

		if (currentPNRSegmentRecord == null || currentPNRSegmentRecord.isEmpty()) {
			return null;
		}

		HashMap<String, String> consideringPNRSegmentRecord = currentPNRSegmentRecord;

		// pnladl method
		if (maximumConnectionTime == null) {
			Double minMaxConnectionTime[] = getMinMaxConnectionTimeFromHub(consideringPNRSegmentRecord
					.get(CurrentPNRSegmentDestination));
			if (minMaxConnectionTime != null) {
				maxConnectTime = minMaxConnectionTime[1];
				minConnectTime = minMaxConnectionTime[0];
			} else {
				maxConnectTime = getMaximumConnectionTime();
				minConnectTime = getMinimumConnectionTime();
			}

		}// manifest method
		else {
			if (maxConnectTime == null) {
				maxConnectTime = getMaximumConnectionTimeForManifest(maximumConnectionTime);
			}

		}

		final double doubleMinConnectTime = (minConnectTime).doubleValue();

		Collection<OnWardConnectionDTO> onWardConnectionDTOs = new ArrayList<OnWardConnectionDTO>();

		while (!consideringPNRSegmentRecord.isEmpty()) {
			
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT distinct * ");
			sb.append(" FROM ");
			sb.append("   (SELECT f.FLIGHT_NUMBER, ");
			sb.append("     fs.SEGMENT_CODE, ");
			sb.append("     fs.EST_TIME_DEPARTURE_LOCAL , ");
			sb.append("     ppfs.BOOKING_CODE, ");
			sb.append("     bc.CABIN_CLASS_CODE                                                   AS CABIN_CLASS_CODE, ");
			sb.append("     (TRUNC(fs.EST_TIME_ARRIVAL_LOCAL) - TRUNC(fs.EST_TIME_DEPARTURE_LOCAL) ) AS dateDifference, ");
			sb.append("     TO_CHAR(fs.EST_TIME_ARRIVAL_LOCAL ,'HH24MI')                            AS arrivalTime, ");
			sb.append("     TO_CHAR(fs.EST_TIME_DEPARTURE_LOCAL ,'HH24MI')                          AS departureTime , ");
			sb.append("     TO_CHAR(fs.EST_TIME_ARRIVAL_LOCAL, 'DD-MON-YYYY HH24:MI:SS') ARRIVAL_LOCAL , ");
			sb.append("     SUBSTR(fs.segment_code,LENGTH(fs.segment_code) - 2,3) destination , ");
			sb.append("     SUBSTR(fs.segment_code,1,3) departingStation , ");
			sb.append("     (fs.EST_TIME_DEPARTURE_LOCAL - TO_DATE('");
			sb.append(consideringPNRSegmentRecord.get(CurrentPNRSegmentArrivalTime));
			sb.append(" ' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime ");
			sb.append("   FROM ");
			sb.append("     t_pnr_segment ps, ");
			sb.append("     t_flight_segment fs, ");
			sb.append("     t_flight f, ");
			sb.append("     T_PNR_PAX_FARE_SEGMENT ppfs, ");
			sb.append("     t_booking_class bc ");
			sb.append("   WHERE ps.pnr          = '");
			sb.append(pnr);
			sb.append("'");
			sb.append("   AND ps.flt_seg_id    = fs.flt_seg_id(+) ");
			sb.append("   AND fs.flight_id     = f.flight_id(+) ");
			sb.append("   AND fs.segment_code LIKE '");
			sb.append(consideringPNRSegmentRecord.get(CurrentPNRSegmentDestination));
			sb.append("/%' ");
			sb.append("   AND ps.pnr_seg_id         = ppfs.PNR_SEG_ID ");
			sb.append("   and bc.booking_code(+)=ppfs.booking_code ");

			if (!AppSysParamsUtil.isSendOnwardSurfaceSegmentInPNLADL()) {
				sb.append("   AND f.operation_type_id !=");
				sb.append(AirScheduleCustomConstants.OperationTypes.BUS_SERVICE);
			}

			if (confirmedReservationSegment) {
				sb.append("   AND ps.status         = '");
				sb.append(ReservationInternalConstants.ReservationStatus.CONFIRMED);
				sb.append("'");
			}

			sb.append("   union all ");
			sb.append("   SELECT  txfs.FLIGHT_NUMBER, ");
			sb.append("     txfs.SEGMENT_CODE, ");
			sb.append("     txfs.EST_TIME_DEPARTURE_LOCAL , ");
			sb.append("     null BOOKING_CODE, ");
			sb.append("     txps.CABIN_CLASS_CODE                                                    AS CABIN_CLASS_CODE, ");
			sb.append("     (TRUNC(txfs.EST_TIME_ARRIVAL_LOCAL) - TRUNC(txfs.EST_TIME_DEPARTURE_LOCAL) ) AS dateDifference, ");
			sb.append("     TO_CHAR(txfs.EST_TIME_ARRIVAL_LOCAL ,'HH24MI')                            AS arrivalTime, ");
			sb.append("     TO_CHAR(txfs.EST_TIME_DEPARTURE_LOCAL ,'HH24MI')                          AS departureTime , ");
			sb.append("     TO_CHAR(txfs.EST_TIME_ARRIVAL_LOCAL, 'DD-MON-YYYY HH24:MI:SS') ARRIVAL_LOCAL , ");
			sb.append("     SUBSTR(txfs.segment_code,LENGTH(txfs.segment_code) - 2,3) destination , ");
			sb.append("     SUBSTR(txfs.segment_code,1,3) departingStation , ");
			sb.append("     (txfs.EST_TIME_DEPARTURE_LOCAL - TO_DATE('");
			sb.append(consideringPNRSegmentRecord.get(CurrentPNRSegmentArrivalTime));
			sb.append("' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime ");
			sb.append(" FROM T_EXT_FLIGHT_SEGMENT TXFS, T_EXT_PNR_SEGMENT TXPS ");
			sb.append(" WHERE TXFS.EXT_FLT_SEG_ID= TXPS.EXT_FLT_SEG_ID ");
			sb.append(" and TXPS.pnr = '");
			sb.append(pnr);
			sb.append("'");
			sb.append(" AND TXFS.segment_code LIKE '");
			sb.append(consideringPNRSegmentRecord.get(CurrentPNRSegmentDestination));
			sb.append("/%' ");
			sb.append(" AND TXPS.status = '");
			sb.append(ReservationInternalConstants.ReservationStatus.CONFIRMED);
			sb.append("'");

			if (AppSysParamsUtil.isSendIbObConnectionsForOtherAirlineSegments()) {
				sb.append("   UNION ALL ");
				sb.append("   SELECT s.FLIGHT_NUMBER , ");
				sb.append("     CONCAT(CONCAT(s.ORIGIN, '/'), s.DESTINATION) SEGMENT_CODE, ");
				sb.append("     s.EST_TIME_DEPARTURE_LOCAL , ");
				sb.append("     s.BOOKING_CODE, ");
				sb.append("     ''                                                                     AS CABIN_CLASS_CODE , ");
				sb.append("     (TRUNC(s.EST_TIME_ARRIVAL_LOCAL) - TRUNC(s.EST_TIME_DEPARTURE_LOCAL) ) AS dateDifference, ");
				sb.append("     TO_CHAR(s.EST_TIME_ARRIVAL_LOCAL ,'HH24MI')                            AS arrivalTime, ");
				sb.append("     TO_CHAR(s.EST_TIME_DEPARTURE_LOCAL ,'HH24MI')                          AS departureTime, ");
				sb.append("     TO_CHAR(s.EST_TIME_ARRIVAL_LOCAL, 'DD-MON-YYYY HH24:MI:SS') ARRIVAL_LOCAL ,  ");
				sb.append("     s.DESTINATION destination,  ");
				sb.append("     s.ORIGIN departingStation,  ");
				sb.append("     (s.EST_TIME_DEPARTURE_LOCAL - TO_DATE('");
				sb.append(consideringPNRSegmentRecord.get(CurrentPNRSegmentArrivalTime));
				sb.append("' ,'DD-MON-YYYY HH24:MI:SS') ) connectTime ");
				sb.append("   FROM T_PNR_OTHER_AIRLINE_SEGMENT s ");
				sb.append("   WHERE s.pnr = '");
				sb.append(pnr);
				sb.append("'");
				sb.append("   AND s.ORIGIN LIKE '");
				sb.append(consideringPNRSegmentRecord.get(CurrentPNRSegmentDestination));
				sb.append("'");
				sb.append("   AND s.status = '");
				sb.append(ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED);
				sb.append("'");
			}

			sb.append("   ) ");
			sb.append(" ORDER BY ARRIVAL_LOCAL DESC ");
			
			
			final double doubleMaxConnectTime = (maxConnectTime).doubleValue();
			// Build outbound flight details
			HashMap outboundDetailsMap = (HashMap) templete.query(sb.toString(), new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap outboundDetailsMap = new HashMap();

					while (rs.next()) {
						if ((rs.getDouble("connectTime") > 0) && (rs.getDouble("connectTime") <= doubleMaxConnectTime)
								&& (rs.getDouble("connectTime") >= doubleMinConnectTime)) {
							/**
							 * Bussiness Rule: Consider only earliest departing flights within maximum connection time
							 */

							// Build the next considering PNR Segment
							// details
							HashMap<String, String> consideringPNRSegmentRecord = new HashMap<String, String>();
							consideringPNRSegmentRecord.put(CurrentPNRSegmentArrivalTime, rs.getString("ARRIVAL_LOCAL"));
							consideringPNRSegmentRecord.put(CurrentPNRSegmentDestination, rs.getString("destination"));
							consideringPNRSegmentRecord.put(CurrentPNRSegmentDepartingStation, rs.getString("departingStation"));

							OnWardConnectionDTO onWardConnectionDTO = new OnWardConnectionDTO();

							onWardConnectionDTO.setFlight(rs.getString("FLIGHT_NUMBER"));
							onWardConnectionDTO.setBoardingpoint(rs.getString("departingStation"));
							
							onWardConnectionDTO.setDateDifference(rs.getInt("dateDifference"));
							onWardConnectionDTO.setArrivalTime(rs.getString("arrivalTime"));
							
							onWardConnectionDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));

							String booking_code = rs.getString("BOOKING_CODE");
							if(booking_code != null && !booking_code.isEmpty()){
								if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
									if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
										onWardConnectionDTO.setFareclass(rs.getString("CABIN_CLASS_CODE"));
									} else {
										onWardConnectionDTO.setFareclass(String.valueOf(booking_code.charAt(0)));
									}
								} else {
									onWardConnectionDTO.setFareclass(rs.getString("CABIN_CLASS_CODE"));
								}
							}else{
								onWardConnectionDTO.setFareclass("Y");
							}

							// Get the day number of the month in String
							// format of "99"
							Date localDepartureDate = rs.getDate("EST_TIME_DEPARTURE_LOCAL");
							Calendar localDepartureCalendar = Calendar.getInstance();
							localDepartureCalendar.setTime(localDepartureDate);
							onWardConnectionDTO.setDay(localDepartureCalendar.get(Calendar.DAY_OF_MONTH));

							onWardConnectionDTO.setDepartureDate(localDepartureDate);

                            onWardConnectionDTO.setArrivalpoint(rs.getString("destination"));
							onWardConnectionDTO.setReservationstatus("");
							
							String departureTime = rs.getString("departureTime");
							onWardConnectionDTO.setDeparturetime(departureTime);

							// Prepair output details
							outboundDetailsMap.put("OnWardConnectionDTO", onWardConnectionDTO);
							outboundDetailsMap.put("ConsideringPNRSegmentRecord", consideringPNRSegmentRecord);

							return outboundDetailsMap;
						}
					}
					return outboundDetailsMap;
				}
			});
			// Collect outbound flights
			if (!outboundDetailsMap.isEmpty()) {
				onWardConnectionDTOs.add((OnWardConnectionDTO) outboundDetailsMap.get("OnWardConnectionDTO"));
				// Get the next considering PNR Segment record
				consideringPNRSegmentRecord = (HashMap) outboundDetailsMap.get("ConsideringPNRSegmentRecord");
			} else {
				consideringPNRSegmentRecord = new HashMap();
			}
		}

		return onWardConnectionDTOs;

	}

	public boolean isFoundADL(int flightId, String departureAirportCode, Collection<AirportPassengerMessageTxHsitory> historyList) {
		// Collection historyList=getPnlAdlHistory();
		Iterator<AirportPassengerMessageTxHsitory> it1 = historyList.iterator();
		boolean foundADL = false;

		while (it1.hasNext()) {
			AirportPassengerMessageTxHsitory hdto = it1.next();
			String pnloradl = hdto.getMessageType();
			int flightIdFromDB = hdto.getFlightNumber();
			// Timestamp timeStampFromDB = hdto.getTransmissionTimeStamp();
			// long timefromDB = timeStampFromDB.getTime();
			// previousTimestamp=timefromDB;
			if (pnloradl.equalsIgnoreCase("ADL") & hdto.getAirportCode().equalsIgnoreCase(departureAirportCode)
					& flightIdFromDB == flightId & hdto.getTransmissionStatus().equalsIgnoreCase("Y")) {
				foundADL = true;
			}
		}
		return foundADL;
	}

	/* ##################################################################### */

	public Timestamp getLatestADLTimeStamp(int flightID, String departureAirportCode, Collection<AirportPassengerMessageTxHsitory> historyList) {

		// Collection historyList=getPnlAdlHistory();
		long previousTimestamp = 0;

		Iterator<AirportPassengerMessageTxHsitory> it1 = historyList.iterator();

		while (it1.hasNext()) {
			AirportPassengerMessageTxHsitory hdto = it1.next();
			String pnloradl = hdto.getMessageType();
			int flightIdFromDB = hdto.getFlightNumber();
			Timestamp timeStampFromDB = hdto.getTransmissionTimeStamp();
			long timefromDB = timeStampFromDB.getTime();
			// previousTimestamp=timefromDB;
			if (pnloradl.equalsIgnoreCase("ADL") & flightIdFromDB == flightID
					& hdto.getAirportCode().equalsIgnoreCase(departureAirportCode)
					& hdto.getTransmissionStatus().equalsIgnoreCase("Y")) {
				if (timefromDB > previousTimestamp) {
					previousTimestamp = timefromDB;
				}
			}

		}
		return new Timestamp(previousTimestamp);
	}

	/* ##################################################################### */

	public Timestamp getLatestPNLTimeStamp(int flightID, String departureAirportCode, Collection<AirportPassengerMessageTxHsitory> historyList) {

		// Collection historyList=getPnlAdlHistory();
		long previousTimestamp = 0;

		Iterator<AirportPassengerMessageTxHsitory> it1 = historyList.iterator();

		while (it1.hasNext()) {
			AirportPassengerMessageTxHsitory hdto = it1.next();
			String pnloradl = hdto.getMessageType();
			int flightIdFromDB = hdto.getFlightNumber();
			Timestamp timeStampFromDB = hdto.getTransmissionTimeStamp();

			long timefromDB = timeStampFromDB.getTime();
			// previousTimestamp=timefromDB;
			if (pnloradl.equalsIgnoreCase("PNL") & hdto.getAirportCode().equalsIgnoreCase(departureAirportCode)
					& flightIdFromDB == flightID & hdto.getTransmissionStatus().equalsIgnoreCase("Y")) {
				if (timefromDB > previousTimestamp) {
					previousTimestamp = timefromDB;

				}

			}
		}

		return new Timestamp(previousTimestamp);

	}

	/* ##################################################################### */

	/**
	 * {@inheritDoc} Modified by Lalanthi - JIRA : AARESAA-2675(Issue 4)- 29/09/2009
	 */
	@Override
	public Collection<PNLADLDestinationDTO> getADL(int flightId, String departureAirportCode, Timestamp scheduledTimeStamp,
			String currentCarrierCode) {

		String sqlBookingType = getBookingTypes();

		Object[] adlParams = { new Integer(flightId), departureAirportCode, scheduledTimeStamp };

		String adlQuery = " SELECT distinct vpnr.pnr_status as vp, vseg.pnr_seg_id, "
				+ "     tr.status  as ress,vseg.pnl_stat as ps,BOOKING_CODE, bc_type, "
				+ "     CABIN_CLASS_CODE, LOGICAL_CABIN_CLASS_CODE, SEGMENT_SEQ, "
				+ "     substr(SEGMENT_CODE,length(SEGMENT_CODE)-2,length(SEGMENT_CODE)) as scode, "
				+ "     pas.FIRST_NAME as fn , pas.LAST_NAME as ln, pas.TITLE as ti, "
				+ "     pas.PNR as pnrn, pas.PNR_PAX_ID as id, pas.start_timestamp, " + "     pas.pax_type_code as paxTypeCode,"
				+ "		pas.DATE_OF_BIRTH as dob, pas.NATIONALITY_CODE as nationality,	"
				+ "     paxAddn.PASSPORT_NUMBER AS foid_number, " + "     paxAddn.PASSPORT_EXPIRY AS foid_expiry, "
				+ "     paxaddn.nationalid_no AS NIC, f.flight_type AS FLIGHT_TYPE ,"
				+ "     paxAddn.PASSPORT_ISSUED_CNTRY AS foid_issued_cntry, "
				+ "     pc.ssr_code AS foid_ssr_code, pet.e_ticket_number AS ETKT , ppfst.coupon_number AS coupon_no , "
				+ "     (SELECT rpc.no FROM t_res_pcd rpc WHERE rpc.pnr_pax_id = pas.pnr_pax_id and rownum =1 ) as cc_digits, "
				+ "     vseg.group_id, vpnr.CS_FLIGHT_NUMBER AS CS_FLIGHT_NO, vpnr.CS_BOOKING_CLASS AS CS_BOOKING_CODE, "
				+ "     VPNR.EST_TIME_DEPARTURE_LOCAL AS CS_DEPARTURE_TIME, SEGMENT_CODE AS cs_segment_code,  "
				+ "		PPFST.EXT_E_TICKET_NUMBER AS EX_ETKT, PPFST.EXT_COUPON_NUMBER AS EX_COUPON_NO, "
				+ "		TR.EXTERNAL_REC_LOCATOR AS EX_PNRN  " + " FROM T_Reservation tr, v_pnr_pax_segments vseg, "
				+ "     V_PNR_SEGMENT vpnr, T_Pnr_passenger pas , T_PNR_PAX_ADDITIONAL_INFO paxAddn, "
				+ "		T_PAX_INFO_CONFIG pc  ,  T_PAX_E_TICKET pet , T_PNR_PAX_FARE_SEG_E_TICKET ppfst , T_FLIGHT f "
				+ " WHERE   pas.pnr 				= tr.pnr " + "     AND vseg.pnr 				= tr.pnr " + "     AND vpnr.pnr 				= tr.pnr "
				+ "     AND pas.PNR 				= tr.pnr " + "     AND pas.PNR_PAX_ID 			= paxAddn.PNR_PAX_ID "
				+ "		AND pc.PAX_TYPE_CODE    	= pas.PAX_TYPE_CODE " + "		AND pc.PAX_CATEGORY_CODE 	= pas.PAX_CATEGORY_CODE "
				+ " 	AND pc.FIELD_NAME       	= '"
				+ BasePaxConfigDTO.FIELD_PASSPORT
				+ "' "
				+ "     AND vpnr.SEGMENT_STATUS in ('OPN','CLS') "
				+ "     AND ((tr.status = 'CNF' and vseg.pnl_stat = 'N' and vpnr.pnr_status != 'CNX') "
				+ "     OR (tr.status = 'CNF' and vseg.pnl_stat = 'C' and vpnr.pnr_status != 'CNX') "
				+ "     OR (vpnr.pnr_status = 'CNX' and vseg.pnl_stat = 'P') "
				+ "     OR (vpnr.pnr_status = 'CNX' and vseg.pnl_stat = 'C') "
				+ "     OR (vpnr.pnr_status = 'CNX' and vseg.pnl_stat = 'D')) "
				+ "     AND BOOKING_CODE is not null "
				+ sqlBookingType
				+ "     AND vpnr.FLIGHT_NUMBER LIKE '"
				+ currentCarrierCode
				+ "%'"
				+ "     AND vpnr.pnr_seg_id = vseg.pnr_seg_id "
				+ "     AND pas.pnr_pax_id = vseg.pnr_pax_id "
				+ "     AND FLIGHT_ID = ? "
				+ "     AND f.flight_id =  vpnr.FLIGHT_ID "
				+ "     AND substr(SEGMENT_CODE,0,3) = ?  "
				+ "     AND pas.start_timestamp <= ? "
				+ "     AND pas.PNR_PAX_ID           = pet.pnr_pax_id AND pet.pax_e_ticket_id      = ppfst.pax_e_ticket_id AND vseg.ppfs_id = ppfst.ppfs_id "
				+ "     AND ppfst.pnr_pax_fare_seg_e_ticket_id = "
				+ "        (SELECT MAX(ppfst.pnr_pax_fare_seg_e_ticket_id)  "
				+ "          FROM T_PNR_PAX_FARE_SEG_E_TICKET ppfst  "
				+ "           WHERE vseg.ppfs_id = ppfst.ppfs_id  "
				+ "           GROUP BY ppfst.ppfs_id ) " + "     ORDER BY pas.PNR_PAX_ID ";
		// AARESAA-6982 : Commenting above code due to this JIRA's issue as it removing CNX entries of ADL

		DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final ArrayList<PNLADLDestinationSupportDTO> listFromDB1 = new ArrayList<PNLADLDestinationSupportDTO>();
		final Collection<Integer> paxIdList = new ArrayList<Integer>();
		final ArrayList<Integer> adlPaxIdList = new ArrayList<Integer>();// JIRA : AARESAA-2675(Issue 4) Lalanthi
		final Map<Integer, Integer> mapPnrPaxSegIds = new HashMap<Integer, Integer>();

		final Collection<String> pnrList = new ArrayList<String>();
		templete.query(adlQuery, adlParams,

		new ResultSetExtractor() {

			@SuppressWarnings("unused")
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					String foidNumber = null;
					String foidExpiry = "";
					String foidPlace = "";
					String foidSSRCode = null;
					String newSSRText = "";
					String dateOfBirth = "";
					boolean isNICSentInPNLADL = false;

					ReservationPaxDetailsDTO rpax = new ReservationPaxDetailsDTO();
					PNLADLDestinationSupportDTO dto = new PNLADLDestinationSupportDTO();

					String bookingCodeType = rs.getString("bc_type");
					String pnlStatus = rs.getString("ps");
					String reservationStatus = rs.getString("ress");
					String cabinClassCode = rs.getString("CABIN_CLASS_CODE");
					String logicalCabinClassCode = rs.getString("LOGICAL_CABIN_CLASS_CODE");
					String bookingCode = rs.getString("BOOKING_CODE");
					String departure = rs.getString("scode");
					String firstName = rs.getString("fn");
					String lastName = rs.getString("ln");
					String pnr = rs.getString("pnrn");
					String title = rs.getString("ti");
					Integer pnrPaxId = new Integer(rs.getInt("id"));
					Integer pnrSegId = new Integer(rs.getInt("pnr_seg_id"));
					String pnrStat = rs.getString("vp");
					String paxType = rs.getString("paxTypeCode");
					String eticket = rs.getString("ETKT");
					int couponNo = rs.getInt("coupon_no");
					String ccDigits = rs.getString("cc_digits");
					String nationalIDNo = rs.getString("NIC") != null ? rs.getString("NIC") : "";
					String flightType = rs.getString("FLIGHT_TYPE") != null ? rs.getString("FLIGHT_TYPE") : "";

					String groupId = rs.getString("group_id");
					/** //JIRA : AARESAA-2675(Issue 4) Lalanthi - to incorporate new DB structure for SSR */
					adlPaxIdList.add(pnrPaxId);
					mapPnrPaxSegIds.put(pnrPaxId, pnrSegId);

					// MARKETING/CODESHARE FLIGHT INFORMATION
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");
					rpax.setCsBookingClass(csBookingClass);
					rpax.setCsFlightNo(csFlightNo);
					rpax.setCsDepatureDateTime(rs.getTimestamp("CS_DEPARTURE_TIME"));
					rpax.setCsOrginDestination(rs.getString("cs_segment_code"));

					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						String extEticket = rs.getString("EX_ETKT");
						int extCouponNo = rs.getInt("EX_COUPON_NO");
						if (extEticket != null && !"".equals(extEticket)) {
							eticket = extEticket;
						}
						couponNo = rs.getInt("EX_COUPON_NO");
						rpax.setExternalPnr(rs.getString("EX_PNRN"));
						rpax.setCodeShareFlight(true);
					}
					

					Date dob = rs.getDate("dob");
					if (dob != null) {
						SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
						dateOfBirth = sf.format(dob);
					}
					Integer nationalityId = rs.getInt("nationality");
					String nationality = "";
					if (nationalityId != null) {
						try {
							Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(nationalityId);
							if (nat != null && nat.getIsoCode() != null) {
								nationality = nat.getIsoCode();
							}
						} catch (ModuleException mEx) {
							// purposely catching exception
						}
					}

					// TODO Need to seperate WaitList and Standby Passengers in the future
					if (BookingClass.BookingClassType.STANDBY.equals(bookingCodeType)) {
						// rpax.setWaitingListPax(true); //TODO: Enable when mahan branch is merging with waitlisitng
						// facilities
						rpax.setIDPassenger(true);
					}

					// FOID details (eg: passport number, national id) to appear in PNL
					String foidStr = rs.getString("foid_number");
					Date exDt = rs.getDate("foid_expiry");

					if (exDt != null) {
						SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
						foidExpiry = sf.format(exDt);
					}

					foidPlace = rs.getString("foid_issued_cntry");

					if (foidStr != null) {
						// handle PSPT string to separate PSPT#, expiry date & Issued place
						String[] foidArr = foidStr.split(ReservationInternalConstants.PassengerConst.FOID_SEPARATOR);

						if (foidArr.length == 3) {
							foidNumber = foidArr[0];

							if ((foidExpiry == null || foidExpiry.equals("")) && foidArr[1] != null) {
								foidExpiry = foidArr[1];
							}

							if ((foidPlace == null || foidPlace.equals("")) && foidArr[2] != null) {
								foidPlace = foidArr[2];
							}

						} else if (foidArr.length == 2) {
							foidNumber = foidArr[0];

							if ((foidExpiry == null || foidExpiry.equals("")) && foidArr[1] != null) {
								foidExpiry = foidArr[1];
							}

							if ((foidPlace == null || foidPlace.equals("")) && foidArr[2] != null) {
								foidPlace = foidArr[2];
							}

						} else if (foidArr.length == 1) {
							foidNumber = foidArr[0];
						}
					}

					if (("DOM").equals(flightType)) {
						if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
							if(nationalIDNo != null && !nationalIDNo.isEmpty()){
								foidNumber = nationalIDNo;
								isNICSentInPNLADL = true;
							}
						}
					} else {
						if ((foidNumber == null || "".equals(foidNumber)) && !"".equals(nationalIDNo)) {
							foidNumber = nationalIDNo;
							isNICSentInPNLADL = false;
						}
					}

					if (foidNumber != null) {
						rpax.setFoidNumber(foidNumber);
					} else {
						rpax.setFoidNumber("");
					}

					if (foidExpiry != null) {
						rpax.setFoidExpiry(foidExpiry);
					} else {
						rpax.setFoidExpiry("");
					}

					if (foidPlace != null) {
						rpax.setFoidPlace(foidPlace);
					} else {
						rpax.setFoidPlace("");
					}

					foidSSRCode = rs.getString("foid_ssr_code");

					Collection<PaxSSRDetailDTO> ssrDetails = new ArrayList<PaxSSRDetailDTO>();
					newSSRText = "";
					String ssrCode = null;
					String ssrText = null;

					if ((foidNumber != null) && (!foidNumber.trim().equals(""))) {
						PaxSSRDetailDTO foidSSRDetails = new PaxSSRDetailDTO();
						if (foidSSRCode == null) {
							foidSSRCode = "";
						}
						if (ssrText == null) {
							ssrText = "";
						}
						if ((ssrCode != null) && (!ssrCode.trim().equals(""))) {
							if (ssrCode.trim().equalsIgnoreCase(foidSSRCode.trim())) {
								newSSRText = foidNumber.trim();
							} else {
								newSSRText = foidSSRCode + " " + foidNumber.trim();
							}
							if (ssrText.trim().equals("")) {
								ssrText = newSSRText;
							} else {
								ssrText = ssrText + " " + newSSRText;
							}
						} else if (!foidSSRCode.trim().equals("")) {
							ssrCode = foidSSRCode.trim();
							ssrText = foidNumber.trim();
						}
						foidSSRDetails.setSsrCode(ssrCode);
						foidSSRDetails.setSsrText(ssrText);
						foidSSRDetails.setNICSentInPNLADL(isNICSentInPNLADL);
						foidSSRDetails.setPnrPaxId(pnrPaxId);
						ssrDetails.add(foidSSRDetails);
					}

					if (eticket != null && (!eticket.trim().equals(""))) {
						PaxSSRDetailDTO eticketSSRDetails = new PaxSSRDetailDTO();
						// if (AppSysParamsUtil.isPNLEticketEnabled()) {
						// ssrCode = CommonsConstants.PNLticketType.TKNE;
						// } else {
						// ssrCode = CommonsConstants.PNLticketType.TKNA;
						// }
						ssrCode = CommonsConstants.PNLticketType.TKNE;
						eticketSSRDetails.setSsrCode(ssrCode);
						eticketSSRDetails.setSsrText(eticket + "/" + couponNo);
						eticketSSRDetails.setPnrPaxId(pnrPaxId);
						ssrDetails.add(eticketSSRDetails);
					}

					rpax.setPnrStatus(pnrStat);

					if (firstName == null) {
						firstName = "";
					}
					if (lastName == null) {
						lastName = "";
					}

					if ((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.INFANT))) {

					} else {
						paxIdList.add(pnrPaxId);
					}

					String upperfirstname = BeanUtils.removeExtraChars(firstName.toUpperCase());
					String upperlastname = BeanUtils.removeExtraChars(lastName.toUpperCase());

					firstName = BeanUtils.removeSpaces(upperfirstname);
					lastName = BeanUtils.removeSpaces(upperlastname);
					if (firstName.equalsIgnoreCase("TBA") || lastName.equalsIgnoreCase("TBA")) {
						continue;
					}

					rpax.setFirstName(firstName);
					rpax.setLastName(lastName);
					rpax.setPnr(pnr);

					rpax.setDob(dateOfBirth);
					rpax.setNationalityIsoCode(nationality != null ? nationality : "");

					if (ccDigits != null) {
						rpax.setCcDigits(BeanUtils.getLast4Digits(ccDigits));
					}
					if (title == null || title.equalsIgnoreCase("")) {
						title = "Mr";
					}

					rpax.setGender(PnlAdlUtil.findGender(title));
					// set passenger type
					if (((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.ADULT)))) {
						rpax.setPaxType(ReservationInternalConstants.PassengerType.ADULT);
					} else if ((paxType != null)
							&& (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.CHILD))) {
						// title = "CHD";
						rpax.setPaxType(ReservationInternalConstants.PassengerType.CHILD);
					} else if ((paxType != null)
							&& (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.INFANT))) {
						title = "CHD";
						rpax.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
					}

					rpax.setTitle(title.toUpperCase());
					rpax.setPnlStatType(pnlStatus);
					rpax.setStatus(reservationStatus);
					rpax.setPnrPaxId(pnrPaxId);
					rpax.setPnrSegId(pnrSegId);
					rpax.setSsrDetails(ssrDetails);
					// rpax.setSsrCode(ssrCode);
					// rpax.setSsrText(ssrText);
					rpax.setGroupId(groupId);

					// rpax.set
					if (cabinClassCode == null) {
						cabinClassCode = "Y";
					}
					if (logicalCabinClassCode == null) {
						logicalCabinClassCode = "Y";
					}

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							dto.setBookingCode(logicalCabinClassCode);
						} else {
							dto.setBookingCode(bookingCode);
						}
					} else {
						dto.setBookingCode(cabinClassCode);
					}

					dto.setDestinationStatusCode(departure);
					dto.setPassenger(rpax);

					if ((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.INFANT))) {

					} else {
						listFromDB1.add(dto);
					}
				}

				return listFromDB1;
			}
		});

		/** JIRA:AARESAA-2675 (Issue 4) - Lalanthi Date 30/09/2009 */
		appendADLSSRInfo(listFromDB1, mapPnrPaxSegIds);

		// if passengers pax id list will be not null but zero
		if (paxIdList.size() == 0) {

			PNLADLDestinationDTO dtotransfer = new PNLADLDestinationDTO();
			dtotransfer.setBookingCode("Y");
			dtotransfer.setDestinationAirportCode(departureAirportCode);
			Collection<ReservationPaxDetailsDTO> col = new ArrayList<ReservationPaxDetailsDTO>();
			dtotransfer.setPassenger(col);
			ArrayList<PNLADLDestinationDTO> paxIdEmptyList = new ArrayList<PNLADLDestinationDTO>();

			paxIdEmptyList.add(dtotransfer);

			return paxIdEmptyList;
		}

		Map<Integer, BigDecimal> sortedMap = getReservationTnxDAO().getPNRPaxBalances(paxIdList);

		Collection<PNLADLDestinationSupportDTO> refinedDatabaseList = new ArrayList<PNLADLDestinationSupportDTO>();

		for (int l = 0; l < listFromDB1.size(); l++) {

			PNLADLDestinationSupportDTO sdto = (listFromDB1).get(l);
			BigDecimal credit = sortedMap.get(sdto.getPassenger().getPnrPaxId());
			double creditvalue = 0;

			if (credit == null) {
				refinedDatabaseList.add(sdto);
			} else {
				creditvalue = credit.doubleValue();

				if (creditvalue <= 0) {
					refinedDatabaseList.add(sdto);
				} else {
					if (!pnrList.contains(sdto.getPassenger().getPnr())) {// finding
						pnrList.add(sdto.getPassenger().getPnr());
					}

				}
			}
		}

		// Collection finalRefinedList=new ArrayList();
		for (int i = 0; i < pnrList.size(); i++) {
			String pnr = ((ArrayList<String>) pnrList).get(i);
			for (int j = 0; j < refinedDatabaseList.size(); j++) {
				PNLADLDestinationSupportDTO sdto = ((ArrayList<PNLADLDestinationSupportDTO>) refinedDatabaseList).get(j);

				if (sdto.getPassenger().getPnr().equalsIgnoreCase(pnr)) {

					refinedDatabaseList.remove(sdto);
					j--;

				}

			}

		}

		Set<String> logicalCCs = new HashSet<String>();
		Set<String> destinations = new HashSet<String>();
		ArrayList<PNLADLDestinationDTO> transfer = new ArrayList<PNLADLDestinationDTO>();

		for (int i = 0; i < refinedDatabaseList.size(); i++) {
			PNLADLDestinationSupportDTO dto11 = ((ArrayList<PNLADLDestinationSupportDTO>) refinedDatabaseList).get(i);
			ArrayList<ReservationPaxDetailsDTO> passengerList = new ArrayList<ReservationPaxDetailsDTO>();

			for (int j = 0; j < refinedDatabaseList.size(); j++) {
				PNLADLDestinationSupportDTO dto22 = ((ArrayList<PNLADLDestinationSupportDTO>) refinedDatabaseList).get(j);
				if (dto11.getBookingCode().toUpperCase().charAt(0) == dto22.getBookingCode().toUpperCase().charAt(0)
						& dto11.getDestinationStatusCode().equalsIgnoreCase(dto22.getDestinationStatusCode())) {
					ReservationPaxDetailsDTO pax11 = dto11.getPassenger();
					ReservationPaxDetailsDTO pax22 = dto22.getPassenger();

					if (!passengerList.contains(pax11)) {
						passengerList.add(pax11);
						// passengerList.add(pax22);
					}
					if (!passengerList.contains(pax22)) {
						// passengerList.add(pax11);
						passengerList.add(pax22);
					}

				}
			}

			PNLADLDestinationDTO tmp = new PNLADLDestinationDTO();
			tmp.setBookingCode(dto11.getBookingCode());
			tmp.setDestinationAirportCode(dto11.getDestinationStatusCode());
			tmp.setPassenger(passengerList);
			logicalCCs.add(String.valueOf(dto11.getBookingCode().charAt(0)));
			destinations.add(dto11.getDestinationStatusCode());

			if (!transfer.contains(tmp) & !found(transfer, tmp)) {
				transfer.add(tmp);
			}

		}
		// Generating Fare class wise pnl for empty/ non-booked logical cabin classes
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			this.injectNonBookedFareClassDetails(logicalCCs, destinations, transfer, flightId);
		}

		return transfer;// destinationdtos;

	}

	@Override
	public List<PassengerInformation> getPNLPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimeStamp, String carrierCode) {

		String sqlBookingType = getBookingTypes();
		Object[] adlParams = { new Integer(flightId), departureAirportCode,
				scheduledTimeStamp };
		final List<Integer> pnrPaxIds = new ArrayList<Integer>();
		// TODO remove unnecassry fields from the query
		String adlQuery = "SELECT DISTINCT vpnr.pnr_status AS vp                                             , "
				+ "                vseg.pnr_seg_id                                                                , "
				+ "                tr.status     AS ress                                                          , "
				+ "                vseg.pnl_stat AS ps                                                            , "
				+ "                vseg.booking_code                                                                   , "
				+ "                vseg.bc_type                                                                        , "
				+ "                vseg.cabin_class_code                                                               , "
				+ "                vseg.logical_cabin_class_code                                                       , "
				+ "                vpnr.SEGMENT_SEQ                                                                    , "
				+ "                SUBSTR(vpnr.SEGMENT_CODE,LENGTH(vpnr.SEGMENT_CODE)-2,LENGTH(vpnr.SEGMENT_CODE)) AS destination   , "
				+ "                pas.FIRST_NAME        AS first_name ,  "
				+ "                pas.last_name          AS last_name ,  "
				+ "                pas.title                  AS title ,  "
				+ "                pas.PNR                                                          AS pnr        , "
				+ "                pas.PNR_PAX_ID                                                   AS pnr_pax_id , "
				+ "                pas.start_timestamp                                                            , "
				+ "                pas.pax_type_code             AS pax_type                                                  , "
				+ "                pas.DATE_OF_BIRTH             AS dob                                                       , "
				+ "                pas.NATIONALITY_CODE          AS nationality                                               , "
				+ "                paxAddn.PASSPORT_NUMBER       AS foid_number                                               , "
				+ "                paxAddn.PASSPORT_EXPIRY       AS foid_expiry                                               , "
				+ "                paxAddn.PASSPORT_ISSUED_CNTRY AS foid_issued_country                                         , "
				+ "                pc.ssr_code                   AS foid_ssr_code                                             , "
				+ "                paxAddn.PLACE_OF_BIRTH          AS place_of_birth                                             , "
				+ "                paxAddn.TRAVEL_DOC_TYPE         AS travel_doc_type                                            , "
				+ "                paxAddn.VISA_DOC_NUMBER         AS visa_doc_number                                            , "
				+ "                paxAddn.VISA_DOC_PLACE_OF_ISSUE AS visa_doc_place_of_issue                                    , "
				+ "                paxAddn.VISA_DOC_ISSUE_DATE     AS visa_doc_issue_date                                        , "
				+ "                paxAddn.VISA_APPLICABLE_COUNTRY AS visa_applicable_country                                    , "
				+ "                DECODE(paxAddn.VISA_DOC_NUMBER, NULL, NULL, 'DOCO') doco_ssr_code                             , "
				+ "                pet.e_ticket_number           AS eticket_no                                                , "
				+ "                ppfst.coupon_number           AS coupon_no                                                 , "
				+ "                (SELECT rpc.no " + "                FROM    t_res_pcd rpc "
				+ "                WHERE   rpc.pnr_pax_id = pas.pnr_pax_id " + "                AND     rownum         =1 "
				+ "                ) " + "                AS cc_digits, " + "                vseg.group_id, "
				+ "                vpnr.CS_FLIGHT_NUMBER AS CS_FLIGHT_NO, vpnr.CS_BOOKING_CLASS AS CS_BOOKING_CODE, "
				+ "                VPNR.EST_TIME_DEPARTURE_LOCAL AS CS_DEPARTURE_TIME, vpnr.SEGMENT_CODE AS cs_segment_code,  "
				+ "				   PPFST.EXT_E_TICKET_NUMBER AS EX_ETKT, PPFST.EXT_COUPON_NUMBER AS EX_COUPON_NO, "
				+ "				   TR.EXTERNAL_REC_LOCATOR AS EX_PNRN,  "
				+ "				   f.flight_type ,   paxaddn.nationalid_no AS NIC  "
				+ "FROM            T_Reservation tr                 , "
				+ "                v_pnr_pax_segments vseg          , "
				+ "                V_PNR_SEGMENT vpnr               , "
				+ "                T_Pnr_passenger pas              , "
				+ "                T_PNR_PAX_ADDITIONAL_INFO paxAddn, "
				+ "                T_PAX_INFO_CONFIG pc             , "
				+ "                T_PAX_E_TICKET pet               , "
				+ "                T_PNR_PAX_FARE_SEG_E_TICKET ppfst, "
				+ "				   T_FLIGHT f 						"
				+ "WHERE           pas.pnr              = tr.pnr "
				+ "AND             vseg.pnr             = tr.pnr "
				+ "AND             vpnr.pnr             = tr.pnr "
				+ "AND             pas.PNR              = tr.pnr "
				+ "AND             pas.PNR_PAX_ID       = paxAddn.PNR_PAX_ID "
				+ "AND             pc.PAX_TYPE_CODE     = pas.PAX_TYPE_CODE "
				+ "AND             pc.PAX_CATEGORY_CODE = pas.PAX_CATEGORY_CODE "
				+ "AND             pc.FIELD_NAME        = 'passportNo' "
				+ "AND             vpnr.SEGMENT_STATUS IN ('OPN', "
				+ "                                        'CLS') "
				+ "AND             vseg.BOOKING_CODE IS NOT NULL "
				+ sqlBookingType
				+ "AND             vpnr.FLIGHT_NUMBER LIKE '"
				+ carrierCode
				+ "%' "
				+ "AND             vpnr.pnr_seg_id       = vseg.pnr_seg_id "
				+ "AND             pas.pnr_pax_id        = vseg.pnr_pax_id "
				+ "AND             vpnr.FLIGHT_ID             = ? "
				+ "AND 			   vpnr.FLIGHT_ID        = f.FLIGHT_ID "
				+ "AND             SUBSTR(vpnr.SEGMENT_CODE,0,3)           = ? "
				+ "AND             pas.start_timestamp               <= ? "
				+ "AND             pas.PNR_PAX_ID                     = pet.pnr_pax_id "
				+ "AND             pet.pax_e_ticket_id                = ppfst.pax_e_ticket_id "
				+ "AND             vseg.ppfs_id                       = ppfst.ppfs_id "
				+ "AND             ppfst.pnr_pax_fare_seg_e_ticket_id = "
				+ "                (SELECT  MAX(ppfst.pnr_pax_fare_seg_e_ticket_id) "
				+ "                FROM     T_PNR_PAX_FARE_SEG_E_TICKET ppfst "
				+ "                WHERE    vseg.ppfs_id = ppfst.ppfs_id " + "                GROUP BY ppfst.ppfs_id "
				+ "                ) " + "ORDER BY        pas.PNR_PAX_ID";

		DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<PassengerInformation> passengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> finalPassengerInformation = new ArrayList<PassengerInformation>();

		templete.query(adlQuery, adlParams,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String foidNumber = rs.getString("foid_number");
					String segStatus = rs.getString("vp");
					String flightType = rs.getString("FLIGHT_TYPE");
					String nationalIDNo = rs.getString("NIC");
					boolean isNICSentInPNLADL = false;
					// TODO temporary fix untill we move PNL implementation also to msgbroker
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						continue;
					}
					PassengerInformation passenger = new PassengerInformation();
					passenger.setAdlAction(PNLConstants.AdlActions.A.toString());
					passenger.setPnrPaxId(rs.getInt("pnr_pax_id"));
					passenger.setPnrSegId(rs.getInt("pnr_seg_id"));
					passenger.setPnr(rs.getString("pnr"));
					passenger.setFirstName(rs.getString("first_name") != null ? rs.getString("first_name").toUpperCase() : "");
					passenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getFirstName())));
					passenger.setLastName(rs.getString("last_name") != null ? rs.getString("last_name").toUpperCase() : "");
					passenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getLastName())));
					if (passenger.getFirstName().equalsIgnoreCase("TBA") || passenger.getLastName().equalsIgnoreCase("TBA")) {
						continue;
					}
					passenger.setTitle(rs.getString("title") != null ? rs.getString("title").toUpperCase() : "");
					passenger.setGender(PnlAdlUtil.findGender(passenger.getTitle()));
					passenger.setEticketNumber(rs.getString("eticket_no"));
					passenger.setCoupon(rs.getInt("coupon_no"));
					passenger.setDob(rs.getDate("dob"));

					if (("DOM").equals(flightType)) {
						if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
							if(nationalIDNo != null && !nationalIDNo.isEmpty()){
								foidNumber = nationalIDNo;
								isNICSentInPNLADL = true;
							}
						}
					} else {
						if ((foidNumber == null || "".equals(foidNumber)) && !"".equals(nationalIDNo)) {
							foidNumber = nationalIDNo;
							isNICSentInPNLADL = false;
						}
					}
					passenger.setFoidNumber(foidNumber);
					passenger.setNICSentInPNLADL(isNICSentInPNLADL);

					passenger.setFoidExpiry(rs.getDate("foid_expiry"));
					passenger.setFoidIssuedCountry(rs.getString("foid_issued_country"));
					passenger.setFoidSsrCode(rs.getString("foid_ssr_code"));
					passenger.setPlaceOfBirth(rs.getString("place_of_birth"));
					passenger.setTravelDocumentType(rs.getString("travel_doc_type"));
					passenger.setVisaDocNumber(rs.getString("visa_doc_number"));
					passenger.setVisaApplicableCountry(rs.getString("visa_applicable_country"));
					passenger.setVisaDocIssueDate(rs.getDate("visa_doc_issue_date"));
					passenger.setVisaDocPlaceOfIssue(rs.getString("visa_doc_place_of_issue"));

					// MARKETING/CODESHARE FLIGHT INFORMATION
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					passenger.setCsBookingClass(csBookingClass);
					passenger.setCsFlightNo(csFlightNo);
					passenger.setCsDepatureDateTime(rs.getTimestamp("CS_DEPARTURE_TIME"));
					passenger.setCsOrginDestination(rs.getString("cs_segment_code"));
					
					
					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						passenger.setExternalPnr(rs.getString("EX_PNRN"));
						passenger.setCodeShareFlight(true);
						if (rs.getString("EX_ETKT") != null) {
							passenger.setEticketNumber(rs.getString("EX_ETKT"));
						}
						if (rs.getInt("EX_COUPON_NO") > 0) {
							passenger.setCoupon(rs.getInt("EX_COUPON_NO"));
						}
					}
					
					

					String ccDigits = rs.getString("cc_digits");
					if (ccDigits != null) {
						passenger.setCcDigits(BeanUtils.getLast4Digits(ccDigits));
					}
					passenger.setBcType(rs.getString("bc_type"));
					passenger.setGroupId(rs.getString("group_id"));
					passenger.setPaxType(rs.getString("pax_type"));

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							passenger.setClassOfService(rs.getString("logical_cabin_class_code"));
						} else {
							passenger.setClassOfService(rs.getString("booking_code"));
						}
					} else {
						passenger.setClassOfService(rs.getString("cabin_class_code"));
					}
					passenger.setRbdClassOfService(String.valueOf(passenger.getClassOfService().charAt(0)));
					passenger.setDestinationAirportCode(rs.getString("destination"));
					Integer nationalityId = rs.getInt("nationality");
					if (nationalityId != null) {
						passenger.setNationality(PnlAdlUtil.loadPassengerNationality(nationalityId));

					}
					String bcType = rs.getString("bc_type");
					if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
						passenger.setStandByPassenger(true);
					}
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						passenger.setWaitListedPassenger(true);
					}
					passenger.setPnrStatus(segStatus);
					passenger.setPnlStatus(rs.getString("ps"));
					/*
					 * if ((passenger.getPaxType() != null) &&
					 * (passenger.getPaxType().trim().equalsIgnoreCase(ReservationInternalConstants
					 * .PassengerType.CHILD))) { passenger.setTitle("CHD"); }
					 */
					pnrPaxIds.add(passenger.getPnrPaxId());
					passengerInformation.add(passenger);
				}

				return passengerInformation;
			}
		});

		if (pnrPaxIds.isEmpty()) {
			return passengerInformation;
		}

		return filterBalToPayPassengers(pnrPaxIds, passengerInformation);
	}

	
	@Override
	public List<PassengerInformation> getADLPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimeStamp, String carrierCode) {

		String sqlBookingType = getBookingTypes();
		Object[] adlParams = { PNLConstants.PnlPassengerStates.NOT_UPDATED, new Integer(flightId), departureAirportCode,
				scheduledTimeStamp };
		final List<Integer> pnrPaxIds = new ArrayList<Integer>();
		// TODO remove unnecassry fields from the query
		String adlQuery = "SELECT DISTINCT vpnr.pnr_status AS vp                                             , "
				+ "                vseg.pnr_seg_id                                                                , "
				+ "                tr.status     AS ress                                                          , "
				+ "                vseg.pnl_stat AS ps                                                            , "
				+ "                vseg.booking_code                                                                   , "
				+ "                vseg.bc_type                                                                        , "
				+ "                vseg.cabin_class_code                                                               , "
				+ "                vseg.logical_cabin_class_code                                                       , "
				+ "                vpnr.SEGMENT_SEQ                                                                    , "
				+ "                SUBSTR(vpnr.SEGMENT_CODE,LENGTH(vpnr.SEGMENT_CODE)-2,LENGTH(vpnr.SEGMENT_CODE)) AS destination   , "
				+ "                decode(adl.pnl_stat, 'D', adl.FIRST_NAME, pas.FIRST_NAME)        AS first_name ,  "
				+ "                decode(adl.pnl_stat, 'D', adl.last_name, pas.last_name)          AS last_name ,  "
				+ "                decode(adl.pnl_stat, 'D', adl.title, pas.title)                  AS title ,  "
				+ "                adl.PNR                                                          AS pnr        , "
				+ "                adl.pnl_pax_id                                                         AS pnl_pax_id        , "
				+ "    			   adl.pnl_stat as adl_action,"
				+ "                pas.PNR_PAX_ID                                                   AS pnr_pax_id , "
				+ "                pas.start_timestamp                                                            , "
				+ "                pas.pax_type_code             AS pax_type                                                  , "
				+ "                pas.DATE_OF_BIRTH             AS dob                                                       , "
				+ "                pas.NATIONALITY_CODE          AS nationality                                               , "
				+ "                paxAddn.PASSPORT_NUMBER       AS foid_number                                               , "
				+ "                paxAddn.PASSPORT_EXPIRY       AS foid_expiry                                               , "
				+ "                paxAddn.PASSPORT_ISSUED_CNTRY AS foid_issued_country                                         , "
				+ "                pc.ssr_code                   AS foid_ssr_code                                             , "
				+ "                paxAddn.PLACE_OF_BIRTH          AS place_of_birth                                             , "
				+ "                paxAddn.TRAVEL_DOC_TYPE         AS travel_doc_type                                            , "
				+ "                paxAddn.VISA_DOC_NUMBER         AS visa_doc_number                                            , "
				+ "                paxAddn.VISA_DOC_PLACE_OF_ISSUE AS visa_doc_place_of_issue                                    , "
				+ "                paxAddn.VISA_DOC_ISSUE_DATE     AS visa_doc_issue_date                                        , "
				+ "                paxAddn.VISA_APPLICABLE_COUNTRY AS visa_applicable_country                                    , "
				+ "                DECODE(paxAddn.VISA_DOC_NUMBER, NULL, NULL, 'DOCO') doco_ssr_code                             , "
				+ "                pet.e_ticket_number           AS eticket_no                                                , "
				+ "                ppfst.coupon_number           AS coupon_no                                                 , "
				+ "                (SELECT rpc.no " + "                FROM    t_res_pcd rpc "
				+ "                WHERE   rpc.pnr_pax_id = pas.pnr_pax_id " + "                AND     rownum         =1 "
				+ "                ) " + "                AS cc_digits, " + "                vseg.group_id, "
				+ "                vpnr.CS_FLIGHT_NUMBER AS CS_FLIGHT_NO, vpnr.CS_BOOKING_CLASS AS CS_BOOKING_CODE, "
				+ "                VPNR.EST_TIME_DEPARTURE_LOCAL AS CS_DEPARTURE_TIME, vpnr.SEGMENT_CODE AS cs_segment_code,  "
				+ "				   PPFST.EXT_E_TICKET_NUMBER AS EX_ETKT, PPFST.EXT_COUPON_NUMBER AS EX_COUPON_NO, "
				+ "				   TR.EXTERNAL_REC_LOCATOR AS EX_PNRN,  "
				+ "				   f.flight_type ,   paxaddn.nationalid_no AS NIC  "
				+ "FROM            T_Reservation tr                 , "
				+ "                v_pnr_pax_segments vseg          , "
				+ "                V_PNR_SEGMENT vpnr               , "
				+ "                T_Pnr_passenger pas              , "
				+ "                T_PNR_PAX_ADDITIONAL_INFO paxAddn, "
				+ "                T_PAX_INFO_CONFIG pc             , "
				+ "                T_PAX_E_TICKET pet               , "
				+ "                T_PNR_PAX_FARE_SEG_E_TICKET ppfst, "
				+ "				   T_FLIGHT f 						, "
				+ "				  t_pnl_passenger adl "
				+ "WHERE           pas.pnr              = tr.pnr "
				+ "AND             vseg.pnr             = tr.pnr "
				+ "AND             vpnr.pnr             = tr.pnr "
				+ "AND             pas.PNR              = tr.pnr "
				+ "AND             pas.PNR_PAX_ID       = paxAddn.PNR_PAX_ID "
				+ "AND adl.pnr_pax_id = pas.pnr_pax_id "
				+ "AND adl.pnr_seg_id = vpnr.pnr_seg_id "
				+ "AND adl.status = ? "
				+ "AND             pc.PAX_TYPE_CODE     = pas.PAX_TYPE_CODE "
				+ "AND             pc.PAX_CATEGORY_CODE = pas.PAX_CATEGORY_CODE "
				+ "AND             pc.FIELD_NAME        = 'passportNo' "
				+ "AND             vpnr.SEGMENT_STATUS IN ('OPN', 'CLS', 'CNX') "
				+ "AND             vseg.BOOKING_CODE IS NOT NULL "
				+ sqlBookingType
				+ "AND             vpnr.FLIGHT_NUMBER LIKE '"
				+ carrierCode
				+ "%' "
				+ "AND             vpnr.pnr_seg_id       = vseg.pnr_seg_id "
				+ "AND             pas.pnr_pax_id        = vseg.pnr_pax_id "
				+ "AND             vpnr.FLIGHT_ID             = ? "
				+ "AND 			   vpnr.FLIGHT_ID        = f.FLIGHT_ID "
				// +"AND            pas.pnr_pax_id IN (" + Util.buildIntegerInClauseContent(adlPassengerPnr.keySet()) +
				// ") "
				+ "AND             SUBSTR(vpnr.SEGMENT_CODE,0,3)           = ? "
				+ "AND             pas.start_timestamp               <= ? "
				+ "AND             pas.PNR_PAX_ID                     = pet.pnr_pax_id "
				+ "AND             pet.pax_e_ticket_id                = ppfst.pax_e_ticket_id "
				+ "AND             vseg.ppfs_id                       = ppfst.ppfs_id "
				+ "AND             ppfst.pnr_pax_fare_seg_e_ticket_id = "
				+ "                (SELECT  MAX(ppfst.pnr_pax_fare_seg_e_ticket_id) "
				+ "                FROM     T_PNR_PAX_FARE_SEG_E_TICKET ppfst "
				+ "                WHERE    vseg.ppfs_id = ppfst.ppfs_id " + "                GROUP BY ppfst.ppfs_id "
				+ "                ) " + "ORDER BY        pas.PNR_PAX_ID";

		DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<PassengerInformation> passengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> finalPassengerInformation = new ArrayList<PassengerInformation>();

		templete.query(adlQuery, adlParams,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String foidNumber = rs.getString("foid_number");
					String segStatus = rs.getString("vp");
					String flightType = rs.getString("FLIGHT_TYPE");
					String nationalIDNo = rs.getString("NIC");
					boolean isNICSentInPNLADL = false;
					// TODO temporary fix untill we move PNL implementation also to msgbroker

					PassengerInformation passenger = new PassengerInformation();
					passenger.setPnlPaxId(rs.getInt("pnl_pax_id"));
					passenger.setPnrPaxId(rs.getInt("pnr_pax_id"));
					passenger.setPnrSegId(rs.getInt("pnr_seg_id"));
					passenger.setPnr(rs.getString("pnr"));
					passenger.setAdlAction(rs.getString("adl_action"));
					passenger.setFirstName(rs.getString("first_name") != null ? rs.getString("first_name").toUpperCase() : "");
					passenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getFirstName())));
					passenger.setLastName(rs.getString("last_name") != null ? rs.getString("last_name").toUpperCase() : "");
					passenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getLastName())));
					if (passenger.getFirstName().equalsIgnoreCase("TBA") || passenger.getLastName().equalsIgnoreCase("TBA")) {
						continue;
					}
					passenger.setTitle(rs.getString("title") != null ? rs.getString("title").toUpperCase() : "");
					passenger.setGender(PnlAdlUtil.findGender(passenger.getTitle()));
					passenger.setEticketNumber(rs.getString("eticket_no"));
					passenger.setCoupon(rs.getInt("coupon_no"));
					passenger.setDob(rs.getDate("dob"));

					if (("DOM").equals(flightType)) {
						if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
							if(nationalIDNo != null && !nationalIDNo.isEmpty()){
								foidNumber = nationalIDNo;
								isNICSentInPNLADL = true;
							}
						}
					} else {
						if ((foidNumber == null || "".equals(foidNumber)) && !"".equals(nationalIDNo)) {
							foidNumber = nationalIDNo;
							isNICSentInPNLADL = false;
						}
					}
					passenger.setFoidNumber(foidNumber);
					passenger.setNICSentInPNLADL(isNICSentInPNLADL);

					passenger.setFoidExpiry(rs.getDate("foid_expiry"));
					passenger.setFoidIssuedCountry(rs.getString("foid_issued_country"));
					passenger.setFoidSsrCode(rs.getString("foid_ssr_code"));
					passenger.setPlaceOfBirth(rs.getString("place_of_birth"));
					passenger.setTravelDocumentType(rs.getString("travel_doc_type"));
					passenger.setVisaDocNumber(rs.getString("visa_doc_number"));
					passenger.setVisaApplicableCountry(rs.getString("visa_applicable_country"));
					passenger.setVisaDocIssueDate(rs.getDate("visa_doc_issue_date"));
					passenger.setVisaDocPlaceOfIssue(rs.getString("visa_doc_place_of_issue"));

					// MARKETING/CODESHARE FLIGHT INFORMATION
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					passenger.setCsBookingClass(csBookingClass);
					passenger.setCsFlightNo(csFlightNo);
					passenger.setCsDepatureDateTime(rs.getTimestamp("CS_DEPARTURE_TIME"));
					passenger.setCsOrginDestination(rs.getString("cs_segment_code"));
					
					
					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						passenger.setExternalPnr(rs.getString("EX_PNRN"));
						passenger.setCodeShareFlight(true);
						if (rs.getString("EX_ETKT") != null) {
							passenger.setEticketNumber(rs.getString("EX_ETKT"));
						}
						if (rs.getInt("EX_COUPON_NO") > 0) {
							passenger.setCoupon(rs.getInt("EX_COUPON_NO"));
						}
					}

					String ccDigits = rs.getString("cc_digits");
					if (ccDigits != null) {
						passenger.setCcDigits(BeanUtils.getLast4Digits(ccDigits));
					}
					passenger.setBcType(rs.getString("bc_type"));
					passenger.setGroupId(rs.getString("group_id"));
					passenger.setPaxType(rs.getString("pax_type"));

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							passenger.setClassOfService(rs.getString("logical_cabin_class_code"));
						} else {
							passenger.setClassOfService(rs.getString("booking_code"));
						}
					} else {
						passenger.setClassOfService(rs.getString("cabin_class_code"));
					}
					passenger.setRbdClassOfService(String.valueOf(passenger.getClassOfService().charAt(0)));
					passenger.setDestinationAirportCode(rs.getString("destination"));
					Integer nationalityId = rs.getInt("nationality");
					if (nationalityId != null) {
						passenger.setNationality(PnlAdlUtil.loadPassengerNationality(nationalityId));

					}
					String bcType = rs.getString("bc_type");
					if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
						passenger.setStandByPassenger(true);
					}
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						passenger.setWaitListedPassenger(true);
					}
					passenger.setPnrStatus(segStatus);
					passenger.setPnlStatus(rs.getString("ps"));
					/*
					 * if ((passenger.getPaxType() != null) &&
					 * (passenger.getPaxType().trim().equalsIgnoreCase(ReservationInternalConstants
					 * .PassengerType.CHILD))) { passenger.setTitle("CHD"); }
					 */
					pnrPaxIds.add(passenger.getPnrPaxId());
					passengerInformation.add(passenger);
				}

				return passengerInformation;
			}
		});

		if (pnrPaxIds.isEmpty()) {
			return passengerInformation;
		}

		filterAddDeleteForSameReservation(passengerInformation);

		return filterBalToPayPassengers(pnrPaxIds, passengerInformation);
	}

	private void filterAddDeleteForSameReservation(List<PassengerInformation> passengerInformation) {

		Set<PassengerInformation> removePaxInfo = new HashSet<>();
		OUTER: for (PassengerInformation addedPassenger : passengerInformation) {
			for (PassengerInformation deletedPassenger : passengerInformation) {
				if (PNLConstants.AdlActions.D.toString().equals(deletedPassenger.getAdlAction())
						&& PNLConstants.AdlActions.A.toString().equals(addedPassenger.getAdlAction())
						&& deletedPassenger.getPnlPaxId() > addedPassenger.getPnlPaxId() && addedPassenger.getPnrPaxId().equals(deletedPassenger.getPnrPaxId())
						&& addedPassenger.getPnr().equals(deletedPassenger.getPnr())
						&& addedPassenger.getPnrSegId().equals(deletedPassenger.getPnrSegId())) {
					removePaxInfo.add(deletedPassenger);
					removePaxInfo.add(addedPassenger);
					continue OUTER;
				}
			}

			if (addedPassenger.getPnrStatus() != null
					&& addedPassenger.getPnrStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
					&& PNLConstants.AdlActions.A.toString().equals(addedPassenger.getAdlAction())) {
				removePaxInfo.add(addedPassenger);
			}
		}

		passengerInformation.removeAll(removePaxInfo);

	}

	@Override
	public List<PassengerInformation> getDeletedADLPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimeStamp, String carrierCode) {

		Object[] adlParams = { new Integer(flightId), new Integer(flightId), PNLConstants.PnlPassengerStates.NOT_UPDATED, departureAirportCode,
				scheduledTimeStamp };
		final List<Integer> pnrPaxIds = new ArrayList<Integer>();
		
		
	StringBuilder adlSql = new StringBuilder();	
	
	
	adlSql.append(" SELECT ADL.PNL_PAX_ID            AS PNL_PAX_ID , ");
	adlSql.append("  ADL.PNR_PAX_ID                 AS PNR_PAX_ID , ");
	adlSql.append("  ADL.PNR_SEG_ID                 AS PNR_SEG_ID , ");
	adlSql.append("  ADL.PNR                        AS PNR , ");
	adlSql.append("  TR.EXTERNAL_REC_LOCATOR        AS EXT_PNR , ");
	adlSql.append("  PS.STATUS                      AS SEGSTATUS , ");
	adlSql.append("  SUBSTR(FS.SEGMENT_CODE,LENGTH(FS.SEGMENT_CODE)-2,LENGTH(FS.SEGMENT_CODE)) AS DESTINATION , ");
	adlSql.append("  ADL.FIRST_NAME                 AS FIRST_NAME , ");
	adlSql.append("  ADL.LAST_NAME                  AS LAST_NAME , ");
	adlSql.append("  ADL.TITLE                      AS TITLE , ");
	adlSql.append("  ADL.PAX_TYPE_CODE              AS PAX_TYPE , ");
	adlSql.append("  ADL.PNL_STAT                   AS ADL_ACTION , ");
	adlSql.append("  ADL.BC_TYPE                    AS BC_TYPE , ");
	adlSql.append("  ADL.BOOKING_CODE               AS BOOKING_CODE , ");
	adlSql.append("  ADL.CABIN_CLASS_CODE           AS CABIN_CLASS_CODE , ");
	adlSql.append("  ADL.LOGICAL_CABIN_CLASS_CODE   AS LOGICAL_CABIN_CLASS_CODE, ");
	adlSql.append("  ppfs.GROUP_ID                  AS GROUP_ID, ");
	adlSql.append("  ps.CS_FLIGHT_NUMBER            AS CS_FLIGHT_NO, ");
	adlSql.append("  ps.CS_BOOKING_CLASS            AS CS_BOOKING_CODE ");
	adlSql.append("FROM T_PNL_PASSENGER ADL, ");
	adlSql.append("  T_RESERVATION TR, ");
	adlSql.append("  T_PNR_SEGMENT PS, ");
	adlSql.append("  T_FLIGHT_SEGMENT FS, ");	
	adlSql.append("  t_pnr_pax_fare_segment ppfs ");	
	
	adlSql.append("WHERE ADL.PNR                     = TR.PNR ");
	adlSql.append("AND tr.pnr                      = ps.pnr ");	
	adlSql.append("AND ADL.PNR_SEG_ID              = PS.PNR_SEG_ID ");	
	adlSql.append("AND ppfs.pnr_seg_id             =ps.pnr_seg_id ");
	adlSql.append("AND PS.FLT_SEG_ID               = FS.FLT_SEG_ID ");
	adlSql.append("AND (FS.FLIGHT_ID                = ? ");
	adlSql.append("OR ADL.FLT_SEG_ID IN (SELECT fs.flt_seg_id FROM t_flight_segment fs WHERE fs.flight_id = ? )) ");
	adlSql.append("AND ADL.STATUS                  = ? ");
	adlSql.append("AND SUBSTR(FS.SEGMENT_CODE,1,3) = ? ");
	adlSql.append("AND TR.BOOKING_TIMESTAMP         <= ? ");

	DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<PassengerInformation> passengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> finalPassengerInformation = new ArrayList<PassengerInformation>();

		templete.query(adlSql.toString(), adlParams,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					PassengerInformation passenger = new PassengerInformation();
					passenger.setPnlPaxId(rs.getInt("PNL_PAX_ID"));
					passenger.setPnrPaxId(rs.getInt("PNR_PAX_ID"));
					passenger.setPnrSegId(rs.getInt("PNR_SEG_ID"));
					passenger.setPnr(rs.getString("PNR"));
					passenger.setAdlAction(rs.getString("ADL_ACTION"));
					passenger.setFirstName(rs.getString("FIRST_NAME") != null ? rs.getString("FIRST_NAME").toUpperCase() : "");
					passenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getFirstName())));
					passenger.setLastName(rs.getString("LAST_NAME") != null ? rs.getString("LAST_NAME").toUpperCase() : "");
					passenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getLastName())));
					if (passenger.getFirstName().equalsIgnoreCase("TBA") || passenger.getLastName().equalsIgnoreCase("TBA")) {
						continue;
					}
					passenger.setTitle(rs.getString("TITLE") != null ? rs.getString("TITLE").toUpperCase() : "");
					passenger.setGender(PnlAdlUtil.findGender(passenger.getTitle()));
					passenger.setPaxType(rs.getString("PAX_TYPE"));
					passenger.setDestinationAirportCode(rs.getString("DESTINATION"));
					passenger.setExternalPnr(rs.getString("EXT_PNR"));
				
					String bcType = rs.getString("BC_TYPE");
					passenger.setBcType(bcType);

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							passenger.setClassOfService(rs.getString("LOGICAL_CABIN_CLASS_CODE"));
						} else {
							passenger.setClassOfService(rs.getString("BOOKING_CODE"));
						}
					} else {
						passenger.setClassOfService(rs.getString("CABIN_CLASS_CODE"));
					}
					if (passenger.getClassOfService() != null) {
						passenger.setRbdClassOfService(String.valueOf(passenger.getClassOfService().charAt(0)));
					}
					if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
						passenger.setStandByPassenger(true);
					}
					
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					passenger.setCsBookingClass(csBookingClass);
					passenger.setCsFlightNo(csFlightNo);
					passenger.setGroupId(rs.getString("GROUP_ID"));
										
					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						passenger.setCodeShareFlight(true);
					}

					String segStatus = rs.getString("SEGSTATUS");

					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						passenger.setWaitListedPassenger(true);
					}
					passenger.setPnrStatus(segStatus);
					pnrPaxIds.add(passenger.getPnrPaxId());
					passengerInformation.add(passenger);
				}

				return passengerInformation;
			}
		});

		if (pnrPaxIds.isEmpty()) {
			return passengerInformation;
		}

		filterAddDeleteForSameReservation(passengerInformation);
		filterOutAddRecords(passengerInformation);
		return filterBalToPayPassengers(pnrPaxIds, passengerInformation);
	}

	private void filterOutAddRecords(List<PassengerInformation> passengerInformation) {
		Set<PassengerInformation> removePaxInfo = new HashSet<>();
		for (PassengerInformation addedPassenger : passengerInformation) {
			if (PNLConstants.AdlActions.A.toString().equals(addedPassenger.getAdlAction())) {
				removePaxInfo.add(addedPassenger);
			}
		}
		passengerInformation.removeAll(removePaxInfo);

	}

	private List<PassengerInformation> filterBalToPayPassengers(List<Integer> pnrPaxIds,
			List<PassengerInformation> passengerInformation) {
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();

		Map<Integer, BigDecimal> sortedMap = getReservationTnxDAO().getPNRPaxBalances(pnrPaxIds);
		for (PassengerInformation paxInfo : passengerInformation) {
			BigDecimal credit = sortedMap.get(paxInfo.getPnrPaxId());
			double creditvalue = 0;

			if (credit == null) {
				redefinedPassengerInformation.add(paxInfo);
			} else {
				creditvalue = credit.doubleValue();
				if (creditvalue <= 0) {
					redefinedPassengerInformation.add(paxInfo);
				}
			}
		}

		return redefinedPassengerInformation;
	}

	/**
	 * Sets SSR info for each DTO.
	 * 
	 * @param adlPAXDTOs
	 * @param adlPaxIdList
	 * @author lalanthi - Date : 29/09/2009 JIRA : AARESAA-2675 (Issue 4)
	 */
	private void appendADLSSRInfo(Collection<PNLADLDestinationSupportDTO> pnlAdlPAXDTOs, Map<Integer, Integer> mapPnrPaxSegIds) {
		if (pnlAdlPAXDTOs != null && pnlAdlPAXDTOs.size() > 0) {
			// Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxSSRMap = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
			// .getPNRPaxSSRs(mapPnrPaxSegIds, true, false, true);
			for (PNLADLDestinationSupportDTO pnlAdlPAXDTO : pnlAdlPAXDTOs) {
				// Integer pnrPaxID = pnlAdlPAXDTO.getPassenger().getPnrPaxId();
				String ssrCode = pnlAdlPAXDTO.getPassenger().getSsrCode();
				String ssrText = pnlAdlPAXDTO.getPassenger().getSsrText();

				// Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxSSRMap.get(pnrPaxID);
				// pnlAdlPAXDTO.getPassenger().setSsrDetails(paxSSRDetailDTOs);
				pnlAdlPAXDTO.getPassenger().setSsrCode(ssrCode);
				pnlAdlPAXDTO.getPassenger().setSsrText(ssrText);
			}
		}
	}

	/* ##################################################################### */

	@Override
	public Collection<AirportPassengerMessageTxHsitory> getPnlAdlHistory() {
		final Collection<AirportPassengerMessageTxHsitory> col = new ArrayList<AirportPassengerMessageTxHsitory>();

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		templete.query("select * from T_PNLADL_TX_HISTORY", new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					AirportPassengerMessageTxHsitory tx = new AirportPassengerMessageTxHsitory();
					tx.setFlightNumber(rs.getInt("FLIGHT_ID"));
					tx.setSitaAddress(rs.getString("SITA_ADDRESS"));
					tx.setEmail(rs.getString("EMAIL_ID"));
					tx.setMessageType(rs.getString("MESSAGE_TYPE"));
					tx.setTransmissionTimeStamp(rs.getTimestamp("TRANSMISSION_TIMESTAMP"));
					tx.setTransmissionStatus(rs.getString("TRANSMISSION_STATUS"));
					tx.setAirportCode(rs.getString("AIRPORT_CODE"));
					col.add(tx);

				}
				return col;
			}
		});

		return col;
	}

	/* ##################################################################### */

	@Override
	public Collection<AirportPassengerMessageTxHsitory> getPnlAdlHistory(String flightNumber, Date localDate, String airport) {
		final Collection<AirportPassengerMessageTxHsitory> col = new ArrayList<AirportPassengerMessageTxHsitory>();
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { localDate };

		String sql = "select DISTINCT h.ID, h.AIRPORT_CODE, h.EMAIL_ID, h.ERROR_DESCRIPTION, h.FLIGHT_ID,"
				+ " h.MESSAGE_TYPE, h.SENT_METHOD, h.SITA_ADDRESS, h.TRANSMISSION_STATUS, h.TRANSMISSION_TIMESTAMP,"
				+ " h.USER_ID, f.flight_number from T_PNLADL_TX_HISTORY h, T_FLIGHT f, T_Flight_segment fs "
				+ " where h.FLIGHT_ID=f.FLIGHT_ID " + " and trunc(fs.EST_TIME_DEPARTURE_LOCAL)=? and "
				+ " f.flight_id=fs.flight_id and " + " h.AIRPORT_CODE=substr(fs.segment_code,1,3) " + " and h.AIRPORT_CODE= '"
				+ airport + "'" + " and f.FLIGHT_NUMBER= '" + flightNumber + "' "
				+ " order by message_type desc,TRANSMISSION_TIMESTAMP ";
		// h.SENT_MESSAGE not included
		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				// boolean pnlFound = false;
				// boolean haveRecords = false;
				while (rs.next()) {
					// haveRecords = true;
					AirportPassengerMessageTxHsitory tx = new AirportPassengerMessageTxHsitory();
					tx.setFlightNumber(rs.getInt("FLIGHT_ID"));
					tx.setSitaAddress(rs.getString("SITA_ADDRESS"));
					tx.setMessageType(rs.getString("MESSAGE_TYPE"));
					tx.setEmail(rs.getString("EMAIL_ID"));
					// if (rs.getString("MESSAGE_TYPE").equals(PNLConstants.MessageTypes.PNL)) {
					// pnlFound = true;
					// }
					tx.setTransmissionTimeStamp(rs.getTimestamp("TRANSMISSION_TIMESTAMP"));
					tx.setTransmissionStatus(rs.getString("TRANSMISSION_STATUS"));
					tx.setAirportCode(rs.getString("AIRPORT_CODE"));
					col.add(tx);

				}
				/**
				 * NAEEM(NA): 21SEP2010 Jira ID: AARESAA-4807 To Display ADLs if PNLs are not delivered
				 ***/
				// if(!pnlFound&&haveRecords){
				// log.warn("!!!!!!!!!! ISSUE: PNL Record Not Written, but having ADL Records when getting data in Manual UI!!!!!!!!!!!!!!!!! :"
				// + col.size());
				// col.clear();
				// }
				return col;
			}
		});

		return col;

	}

	/* ##################################################################### */

	// private int getDates(int year, int month) {
	//
	// if (month == 0 | month == 2 | month == 4 | month == 6 | month == 7 | month == 9 | month == 11) {
	// return 31;
	// } else if (month == 3 | month == 5 | month == 8 | month == 10) {
	// return 30;
	// }
	//
	// else if (month == 1) {
	//
	// if (year % 4 == 0) {
	// return 29;
	// }
	//
	// else {
	//
	// return 28;
	// }
	//
	// }
	// return -1;
	// }

	/* ##################################################################### */

	private ReservationTnxDAO getReservationTnxDAO() {
		ReservationTnxDAO reservationTnxDAO = null;
		reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		return reservationTnxDAO;
	}

	private Collection<String> getAllflightDestinations(int fid) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		String sql = "select substr(segment_code,5,length(segment_code)) as segcode  from t_flight_segment where flight_id=?";

		final Collection<String> col = new ArrayList<String>();
		Object params[] = { new Integer(fid) };

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					// querying the passenger list and puttinmg to the
					// ReservationPaxDetailsDTo
					String segment = rs.getString("segcode");

					if (segment.length() == 3) {
						col.add(rs.getString("segcode"));
					}

				}
				return null;
			}
		});

		return col;
	}

	@Override
	public Collection<String> getAllflightSegments(int fid) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		String sql = "select segment_code as segcode  from t_flight_segment where flight_id=?";

		final Collection<String> col = new ArrayList<String>();
		Object params[] = { new Integer(fid) };

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					col.add(rs.getString("segcode"));
				}
				return null;
			}
		});

		return col;
	}

	@Override
	public Collection<PNLADLDestinationDTO> getPNL(int flightId, String departureAirportCode, String currentCarrierCode) {

		String sqlBookingType = getBookingTypes();
		String sql = "";

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);

		// for schedules service quiery will enter here.

		sql = "SELECT distinct BOOKING_CODE, CABIN_CLASS_CODE, LOGICAL_CABIN_CLASS_CODE, SEGMENT_SEQ, bc_type, "
				+ "     vseg.pnr_seg_id, vpnr.pnr_status as seg_status,"
				+ "     substr(SEGMENT_CODE,length(SEGMENT_CODE)-2,length(SEGMENT_CODE)) as scode, "
				+ "     pas.FIRST_NAME as fn, pas.LAST_NAME as ln, "
				+ "     pas.TITLE as ti, pas.PNR as pnrn, pas.PNR_PAX_ID as id, "
				+ "     pas.pax_type_code as paxTypeCode, "
				+ "		pas.DATE_OF_BIRTH as dob, pas.NATIONALITY_CODE as nationality,	"
				+ "     paxAddn.PASSPORT_NUMBER AS foid_number, "
				+ "     paxAddn.PASSPORT_EXPIRY AS foid_expiry, "
				+ "     paxAddn.PASSPORT_ISSUED_CNTRY AS foid_issued_cntry, "
				+ "     decode(paxAddn.PASSPORT_NUMBER, null, null, pc.SSR_CODE) foid_ssr_code, "
				+ "     decode(paxaddn.NATIONALID_NO , NULL, NULL, pc.SSR_CODE) nic_ssr_code,"
				+ "     paxAddn.PLACE_OF_BIRTH AS place_of_birth   ,"
				+ "     paxAddn.TRAVEL_DOC_TYPE AS travel_doc_type  ,"
				+ "     paxAddn.VISA_DOC_NUMBER AS visa_doc_number  ,"
				+ "     paxAddn.VISA_DOC_PLACE_OF_ISSUE AS visa_doc_place_of_issue,"
				+ "     paxAddn.VISA_DOC_ISSUE_DATE AS visa_doc_issue_date,"
				+ "     paxAddn.VISA_APPLICABLE_COUNTRY AS visa_applicable_country,"
				+ "     DECODE(paxAddn.VISA_DOC_NUMBER, NULL, NULL, 'DOCO') doco_ssr_code, "
				+ " 	pet.e_ticket_number AS ETKT , "
				+ "     ppfst.coupon_number as coupon_no , "
				+ "     (SELECT rpc.no FROM t_res_pcd rpc WHERE rpc.pnr_pax_id = pas.pnr_pax_id and rownum =1 ) as cc_digits, "
				+ "     vpnr.CS_FLIGHT_NUMBER AS CS_FLIGHT_NO, vpnr.CS_BOOKING_CLASS AS CS_BOOKING_CODE, "
				+ "     VPNR.EST_TIME_DEPARTURE_LOCAL AS CS_DEPARTURE_TIME, SEGMENT_CODE AS cs_segment_code,  "
				+ "		PPFST.EXT_E_TICKET_NUMBER AS EX_ETKT, PPFST.EXT_COUPON_NUMBER AS EX_COUPON_NO,"
				+ "		TR.EXTERNAL_REC_LOCATOR AS EX_PNRN,  paxaddn.nationalid_no AS NIC, f.flight_type AS FLIGHT_TYPE  "
				+ " FROM T_Reservation tr, v_pnr_pax_segments vseg, "
				+ "     V_PNR_SEGMENT vpnr, T_Pnr_passenger pas , T_PNR_PAX_ADDITIONAL_INFO paxAddn, T_PAX_INFO_CONFIG pc , T_PAX_E_TICKET pet , T_PNR_PAX_FARE_SEG_E_TICKET ppfst, T_FLIGHT f  "
				+ " WHERE   pas.pnr = tr.pnr " + "     and vseg.pnr = tr.pnr " + "     and vpnr.pnr = tr.pnr "
				+ "     and pas.PNR_PAX_ID = paxAddn.PNR_PAX_ID " + "		AND pc.PAX_TYPE_CODE    	= pas.PAX_TYPE_CODE "
				+ "		AND pc.PAX_CATEGORY_CODE 	= pas.PAX_CATEGORY_CODE " + " 	AND pc.FIELD_NAME       	= '"
				+ BasePaxConfigDTO.FIELD_PASSPORT
				+ "' "
				+ "     and tr.status = 'CNF' "
				+ "     and vpnr.SEGMENT_STATUS = 'OPN' "
				+ "     and BOOKING_CODE is not null "
				+ sqlBookingType
				+ "     and vpnr.FLIGHT_NUMBER LIKE '"
				+ currentCarrierCode
				+ "%'"
				+ "     and vpnr.pnr_status != 'CNX' "
				+ "     and pas.pnr_pax_id = vseg.pnr_pax_id "
				+ "     and  vpnr.pnr_seg_id = vseg.pnr_seg_id "
				+ "     and pas.PNR_PAX_ID       = pet.pnr_pax_id AND vseg.ppfs_id             = ppfst.ppfs_id AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id "
				+ " AND ppfst.pnr_pax_fare_seg_e_ticket_id = "
				+ " (SELECT MAX(ppfst.pnr_pax_fare_seg_e_ticket_id) "
				+ "    FROM T_PNR_PAX_FARE_SEG_E_TICKET ppfst "
				+ "     WHERE vseg.ppfs_id = ppfst.ppfs_id "
				+ "     GROUP BY ppfst.ppfs_id )"
				+ "     and vpnr.FLIGHT_ID = ?  "
				+ " AND f.flight_id =  vpnr.FLIGHT_ID"
				+ "     and substr(SEGMENT_CODE,0,3) = ? " + " ORDER BY pas.PNR_PAX_ID ";

		Object params[] = { new Integer(flightId), departureAirportCode };

		final Collection<PNLADLDestinationSupportDTO> listFromDB = new ArrayList<PNLADLDestinationSupportDTO>();
		final Collection<Integer> paxIdList = new ArrayList<Integer>();
		final Collection<String> pnrList = new ArrayList<String>();
		final ArrayList<String> dep = new ArrayList<String>();
		final Map<Integer, Integer> mapPnrPaxSegIds = new HashMap<Integer, Integer>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@SuppressWarnings("unused")
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					String foidNumber = null;
					String foidExpiry = "";
					String foidPlace = "";
					String foidSSRCode = null;
					String nicSSRCode = null;
					String newSSRText = "";
					String newVisaSSRText = "";
					String dateOfBirth = "";
					String visaDocNumber = null;
					String placeOfBirth = "";
					String travelDocumentType = "";
					String visaDocPlaceOfIssue = "";
					String visaDocIssueDate = "";
					String visaApplicableCountry = "";
					String docoSSRCode = null;
					boolean isNICSentInPNLADL = false;

					// querying the passenger list and puttinmg to the
					// ReservationPaxDetailsDTo
					ReservationPaxDetailsDTO rpax = new ReservationPaxDetailsDTO();
					PNLADLDestinationSupportDTO dto = new PNLADLDestinationSupportDTO();

					String bookingCodeType = rs.getString("bc_type");
					String cabinClassCode = rs.getString("CABIN_CLASS_CODE");
					String logicalCabinClassCode = rs.getString("LOGICAL_CABIN_CLASS_CODE");
					String bookingCode = rs.getString("BOOKING_CODE");
					// String segmentSeq = rs.getString("SEGMENT_SEQ");

					String departure = rs.getString("scode");
					dep.add(departure);
					String firstName = rs.getString("fn");
					String lastName = rs.getString("ln");
					String pnr = rs.getString("pnrn");
					String title = rs.getString("ti");
					Integer pnrSegId = new Integer(rs.getInt("pnr_seg_id"));

					Date dob = rs.getDate("dob");
					if (dob != null) {
						SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
						dateOfBirth = sf.format(dob);
					}

					Integer nationalityId = rs.getInt("nationality");
					String nationality = "";
					if (nationalityId != null) {
						try {
							Nationality nat = ReservationModuleUtils.getCommonMasterBD().getNationality(nationalityId);
							if (nat != null && nat.getIsoCode() != null) {
								nationality = nat.getIsoCode();
							}
						} catch (ModuleException mEx) {
							// purposely catching exception
						}
					}

					if (firstName == null) {
						firstName = "";
					}
					if (lastName == null) {
						lastName = "";
					}

					Integer pnrPaxId = new Integer(rs.getInt("id"));
					String paxType = rs.getString("paxTypeCode");

					// TODO Need to categorize wait list and ID passengers
					if (BookingClass.BookingClassType.STANDBY.equals(bookingCodeType)) {
						// rpax.setWaitingListPax(true); //TODO: To be enabled when merging Mahan branch including
						// Waitlisting facility
						rpax.setIDPassenger(true);
					}
					String segStatus = rs.getString("seg_status");
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						rpax.setWaitingListPax(true);
					}
					String ssrCode = null;

					String ssrText = null;
					String visaSsrCode = null;
					String visaSsrText = null;

					String eticket = rs.getString("ETKT");
					int couponNo = rs.getInt("coupon_no");
					String ccLastDigits = rs.getString("cc_digits");
					// FOID details (eg: passport number, national id) to appear in PNL
					String foidStr = rs.getString("foid_number");
					Date exDt = rs.getDate("foid_expiry");

					if (exDt != null) {
						SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
						foidExpiry = sf.format(exDt);
					}
					foidPlace = rs.getString("foid_issued_cntry");

					String nationalIDNo = rs.getString("NIC") != null ? rs.getString("NIC") : "";
					String flightType = rs.getString("FLIGHT_TYPE") != null ? rs.getString("FLIGHT_TYPE") : "";
					foidSSRCode = rs.getString("foid_ssr_code");
					nicSSRCode = rs.getString("nic_ssr_code");

					if (foidStr != null) {
						// handle PSPT string to separate PSPT#, expiry date & Issued place (This is for old data)
						String[] foidArr = foidStr.split(ReservationInternalConstants.PassengerConst.FOID_SEPARATOR);

						if (foidArr.length == 3) {
							foidNumber = foidArr[0];

							if ((foidExpiry == null || foidExpiry.equals("")) && foidArr[1] != null) {
								foidExpiry = foidArr[1];
							}

							if ((foidPlace == null || foidPlace.equals("")) && foidArr[2] != null) {
								foidPlace = foidArr[2];
							}

						} else if (foidArr.length == 2) {
							foidNumber = foidArr[0];

							if ((foidExpiry == null || foidExpiry.equals("")) && foidArr[1] != null) {
								foidExpiry = foidArr[1];
							}

						} else if (foidArr.length == 1) {
							foidNumber = foidArr[0];
						}
					}
					if (("DOM").equals(flightType)) {
						if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
							if(nationalIDNo != null && !nationalIDNo.isEmpty()){
								foidNumber = nationalIDNo;
								foidSSRCode = nicSSRCode;
								isNICSentInPNLADL = true;
							}
						}
					} else {
						if ((foidNumber == null || "".equals(foidNumber)) && !"".equals(nationalIDNo)) {
							foidNumber = nationalIDNo;
							foidSSRCode = nicSSRCode;
							isNICSentInPNLADL = false;
						}
					}

					if (foidNumber != null) {
						rpax.setFoidNumber(foidNumber);
					} else {
						rpax.setFoidNumber("");
					}

					if (foidExpiry != null) {
						rpax.setFoidExpiry(foidExpiry);
					} else {
						rpax.setFoidExpiry("");
					}

					if (foidPlace != null) {
						rpax.setFoidPlace(foidPlace);
					} else {
						rpax.setFoidPlace("");
					}

					foidSSRCode = rs.getString("foid_ssr_code");

					// DOCO details (eg: visa doc number, place of birth) to appear in PNL
					placeOfBirth = rs.getString("place_of_birth");
					travelDocumentType = rs.getString("travel_doc_type");
					visaDocNumber = rs.getString("visa_doc_number");
					visaDocPlaceOfIssue = rs.getString("visa_doc_place_of_issue");
					visaApplicableCountry = rs.getString("visa_applicable_country");
					Date issueDt = rs.getDate("visa_doc_issue_date");
					if (issueDt != null) {
						SimpleDateFormat sf = new SimpleDateFormat("ddMMMyy");
						visaDocIssueDate = sf.format(issueDt);
					}
					docoSSRCode = rs.getString("doco_ssr_code");

					if (placeOfBirth != null) {
						rpax.setPlaceOfBirth(placeOfBirth);
					} else {
						rpax.setPlaceOfBirth("");
					}

					if (travelDocumentType != null) {
						rpax.setTravelDocumentType(travelDocumentType);
					} else {
						rpax.setTravelDocumentType("");
					}

					if (visaDocNumber != null) {
						rpax.setVisaDocNumber(visaDocNumber);
					} else {
						rpax.setVisaDocNumber("");
					}

					if (visaDocPlaceOfIssue != null) {
						rpax.setVisaDocPlaceOfIssue(visaDocPlaceOfIssue);
					} else {
						rpax.setVisaDocPlaceOfIssue("");
					}

					if (visaApplicableCountry != null) {
						rpax.setVisaApplicableCountry(visaApplicableCountry);
					} else {
						rpax.setVisaApplicableCountry("");
					}

					rpax.setVisaDocIssueDate(visaDocIssueDate);

					// MARKETING/CODESHARE FLIGHT INFORMATION
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					rpax.setCsBookingClass(csBookingClass);
					rpax.setCsFlightNo(csFlightNo);
					rpax.setCsDepatureDateTime(rs.getTimestamp("CS_DEPARTURE_TIME"));
					rpax.setCsOrginDestination(rs.getString("cs_segment_code"));

					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						String extEticket = rs.getString("EX_ETKT");
						int extCouponNo = rs.getInt("EX_COUPON_NO");
						if (extEticket != null && !"".equals(extEticket)) {
							eticket = rs.getString("EX_ETKT");
						}
						couponNo = rs.getInt("EX_COUPON_NO");
						rpax.setExternalPnr(rs.getString("EX_PNRN"));
						rpax.setCodeShareFlight(true);
					}

					Collection<PaxSSRDetailDTO> ssrDetails = new ArrayList<PaxSSRDetailDTO>();
					newSSRText = "";
					newVisaSSRText = "";

					if ((foidNumber != null) && (!foidNumber.trim().equals(""))) {
						PaxSSRDetailDTO foidSSRDetails = new PaxSSRDetailDTO();
						if (foidSSRCode == null) {
							foidSSRCode = "";
						}
						if (ssrText == null) {
							ssrText = "";
						}
						if ((ssrCode != null) && (!ssrCode.trim().equals(""))) {
							if (ssrCode.trim().equalsIgnoreCase(foidSSRCode.trim())) {
								newSSRText = foidNumber.trim();
							} else {
								newSSRText = foidSSRCode + " " + foidNumber.trim();
							}
							if (ssrText.trim().equals("")) {
								ssrText = newSSRText;
							} else {
								ssrText = ssrText + " " + newSSRText;
							}
						} else if (!foidSSRCode.trim().equals("")) {
							ssrCode = foidSSRCode.trim();
							ssrText = foidNumber.trim();
						}
						foidSSRDetails.setSsrCode(ssrCode);
						foidSSRDetails.setSsrText(ssrText);
						foidSSRDetails.setPnrPaxId(pnrPaxId);
						foidSSRDetails.setNICSentInPNLADL(isNICSentInPNLADL);
						ssrDetails.add(foidSSRDetails);
					}

					if ((visaDocNumber != null) && (!visaDocNumber.trim().equals(""))) {
						PaxSSRDetailDTO docoSSRDetails = new PaxSSRDetailDTO();
						if (docoSSRCode == null) {
							docoSSRCode = "";
						}
						if (visaSsrText == null) {
							visaSsrText = "";
						}

						if ((visaSsrCode != null) && (!visaSsrCode.trim().equals(""))) {
							if (visaSsrCode.trim().equalsIgnoreCase(docoSSRCode.trim())) {
								newVisaSSRText = visaDocNumber.trim();
							} else {
								newVisaSSRText = docoSSRCode + " " + visaDocNumber.trim();
							}
							if (visaSsrText.trim().equals("")) {
								visaSsrText = newVisaSSRText;
							} else {
								visaSsrText = visaSsrText + " " + newVisaSSRText;
							}
						} else if (!docoSSRCode.trim().equals("")) {
							visaSsrCode = docoSSRCode.trim();
							visaSsrText = visaDocNumber.trim();
						}
						docoSSRDetails.setSsrCode(visaSsrCode);
						docoSSRDetails.setSsrText(visaSsrText);
						docoSSRDetails.setPnrPaxId(pnrPaxId);
						ssrDetails.add(docoSSRDetails);
					}

					if (eticket != null && (!eticket.trim().equals(""))) {
						PaxSSRDetailDTO eticketSSRDetails = new PaxSSRDetailDTO();
						if (AppSysParamsUtil.isPNLEticketEnabled()) {
							ssrCode = CommonsConstants.PNLticketType.TKNE;
						} else {
							ssrCode = CommonsConstants.PNLticketType.TKNA;
						}
						eticketSSRDetails.setSsrCode(ssrCode);
						eticketSSRDetails.setSsrText(eticket + "/" + couponNo);
						eticketSSRDetails.setPnrPaxId(pnrPaxId);
						ssrDetails.add(eticketSSRDetails);
					}

					mapPnrPaxSegIds.put(pnrPaxId, pnrSegId);
					paxIdList.add(pnrPaxId);

					rpax.setPnlStatType("N");

					// / Object[] par = { "P", new Integer(pnrPaxId) };
					// templete.update(sqlUpdate,par);

					String upperfirstname = BeanUtils.removeExtraChars(firstName.toUpperCase());
					String upperlastname = BeanUtils.removeExtraChars(lastName.toUpperCase());

					firstName = BeanUtils.removeSpaces(upperfirstname);
					lastName = BeanUtils.removeSpaces(upperlastname);
					if (firstName.equalsIgnoreCase("TBA") || lastName.equalsIgnoreCase("TBA")) {
						continue;
					}

					rpax.setFirstName(firstName);
					rpax.setLastName(lastName);
					rpax.setPnr(pnr);
					rpax.setPnrPaxId(pnrPaxId);
					rpax.setPnrSegId(pnrSegId);
					rpax.setCcDigits(BeanUtils.getLast4Digits(ccLastDigits));
					rpax.setDob(dateOfBirth);
					rpax.setNationalityIsoCode(nationality != null ? nationality : "");

					if (title == null || title.equalsIgnoreCase("")) {
						title = "Mr";
					}

					rpax.setGender(PnlAdlUtil.findGender(title));

					if ((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.ADULT))) {
						rpax.setPaxType(PassengerType.ADULT);
					}

					if ((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.CHILD))) {
						// title = "CHD";
						rpax.setPaxType(PassengerType.CHILD);
					}

					if ((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.INFANT))) {
						title = "CHD";
						rpax.setPaxType(PassengerType.INFANT);
					}

					title = BeanUtils.removeExtraChars(title.toUpperCase());

					rpax.setTitle(title.toUpperCase());
					rpax.setSsrDetails(ssrDetails);
					// rpax.setSsrCode(ssrCode);
					// rpax.setSsrText(ssrText);

					if (cabinClassCode == null || cabinClassCode.equals("")) {
						cabinClassCode = "Y";
					}
					if (logicalCabinClassCode == null || logicalCabinClassCode.equals("")) {
						logicalCabinClassCode = "Y";
					}

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							dto.setBookingCode(bookingCode);
							dto.setCabinClassCode(logicalCabinClassCode);
						} else {
							dto.setCabinClassCode(cabinClassCode);
							dto.setBookingCode(bookingCode);
						}
					} else {
						dto.setCabinClassCode(cabinClassCode);
						dto.setBookingCode(bookingCode);
					}

					dto.setDestinationStatusCode(departure);
					dto.setPassenger(rpax);

					if ((paxType != null) && (paxType.trim().equalsIgnoreCase(ReservationInternalConstants.PassengerType.INFANT))) {

					} else {
						listFromDB.add(dto);
					}

				}
				return null;
			}
		});

		// returns Integer and Double

		PNLADLDestinationDTO dtotransfer = new PNLADLDestinationDTO();
		dtotransfer.setBookingCode("Y");
		dtotransfer.setCabinClassCode("Y");
		dtotransfer.setDestinationAirportCode(departureAirportCode);
		Collection<ReservationPaxDetailsDTO> col = new ArrayList<ReservationPaxDetailsDTO>();
		dtotransfer.setPassenger(col);
		ArrayList<PNLADLDestinationDTO> paxIdEmptyList = new ArrayList<PNLADLDestinationDTO>();
		paxIdEmptyList.add(dtotransfer);

		if (paxIdList.size() == 0) {
			ArrayList<PNLADLDestinationDTO> retlist = new ArrayList<PNLADLDestinationDTO>();
			ArrayList<String> alist = (ArrayList<String>) getAllflightDestinations(flightId);
			for (int i = 0; i < alist.size(); i++) {

				PNLADLDestinationDTO tmp = new PNLADLDestinationDTO();
				tmp.setBookingCode("Y");
				tmp.setCabinClassCode("Y");
				tmp.setDestinationAirportCode(alist.get(i));
				tmp.setPassenger(new ArrayList<ReservationPaxDetailsDTO>());
				retlist.add(tmp);
			}
			return retlist;

		}

		Map<Integer, BigDecimal> sortedMap = getReservationTnxDAO().getPNRPaxBalances(paxIdList);
		Collection<PNLADLDestinationSupportDTO> refinedDatabaseList = new ArrayList<PNLADLDestinationSupportDTO>();

		for (int l = 0; l < listFromDB.size(); l++) {
			PNLADLDestinationSupportDTO sdto = ((ArrayList<PNLADLDestinationSupportDTO>) listFromDB).get(l);
			BigDecimal credit = sortedMap.get(sdto.getPassenger().getPnrPaxId());
			double creditvalue = 0;

			if (credit == null) {
				refinedDatabaseList.add(sdto);
			} else {

				creditvalue = credit.doubleValue();
				if (creditvalue <= 0) {
					refinedDatabaseList.add(sdto);
				} else {

					if (!pnrList.contains(sdto.getPassenger().getPnr())) {

						// finding the pnrs to be removed by checking the credit
						pnrList.add(sdto.getPassenger().getPnr());
					}

				}

			}
		}
		// Collection finalRefinedList=new ArrayList();
		for (int i = 0; i < pnrList.size(); i++) {
			String pnr = ((ArrayList<String>) pnrList).get(i);
			for (int j = 0; j < refinedDatabaseList.size(); j++) {
				PNLADLDestinationSupportDTO sdto = ((ArrayList<PNLADLDestinationSupportDTO>) refinedDatabaseList).get(j);
				if (sdto.getPassenger().getPnr().equalsIgnoreCase(pnr)) {
					refinedDatabaseList.remove(sdto);
					j--;
				}
			}

		}

		ArrayList<PNLADLDestinationDTO> transfer = new ArrayList<PNLADLDestinationDTO>();
		// passenger list will be grOuped by arrival

		if (refinedDatabaseList.size() == 0) {
			PNLADLDestinationDTO tmp = new PNLADLDestinationDTO();
			tmp.setBookingCode("Y");
			tmp.setCabinClassCode("Y");
			PNLADLDestinationSupportDTO pnladlDestinationSupportDTO = ((ArrayList<PNLADLDestinationSupportDTO>) listFromDB)
					.get(0);
			String destinationAirport = pnladlDestinationSupportDTO != null ? pnladlDestinationSupportDTO
					.getDestinationStatusCode() : departureAirportCode;
			tmp.setDestinationAirportCode(destinationAirport);
			tmp.setPassenger(new ArrayList<ReservationPaxDetailsDTO>());
			ArrayList<PNLADLDestinationDTO> retlist = new ArrayList<PNLADLDestinationDTO>();
			retlist.add(tmp);

			return retlist;

		}
		Set<String> fareClasses = new HashSet<String>();
		Set<String> destinations = new HashSet<String>();
		for (int i = 0; i < refinedDatabaseList.size(); i++) {
			PNLADLDestinationSupportDTO dto11 = ((ArrayList<PNLADLDestinationSupportDTO>) refinedDatabaseList).get(i);

			ArrayList<ReservationPaxDetailsDTO> passengerList = new ArrayList<ReservationPaxDetailsDTO>();
			// grouping the passengers destination wise

			for (int j = 0; j < refinedDatabaseList.size(); j++) {
				PNLADLDestinationSupportDTO dto22 = ((ArrayList<PNLADLDestinationSupportDTO>) refinedDatabaseList).get(j);

				if (isSameFareClass(dto11, dto22)
						&& dto11.getDestinationStatusCode().equalsIgnoreCase(dto22.getDestinationStatusCode())) {
					ReservationPaxDetailsDTO pax11 = dto11.getPassenger();
					ReservationPaxDetailsDTO pax22 = dto22.getPassenger();

					if (!passengerList.contains(pax11)) {
						passengerList.add(pax11);
						// passengerList.add(pax22);
					}

					if (!passengerList.contains(pax22)) {
						// passengerList.add(pax11);
						passengerList.add(pax22);
					}

				}
			}

			PNLADLDestinationDTO tmp = new PNLADLDestinationDTO();
			tmp.setCabinClassCode(dto11.getCabinClassCode());
			tmp.setBookingCode(dto11.getBookingCode());
			tmp.setDestinationAirportCode(dto11.getDestinationStatusCode());
			tmp.setPassenger(passengerList);
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				fareClasses.add(String.valueOf(dto11.getCabinClassCode().charAt(0)));
			} else {
				fareClasses.add(String.valueOf(dto11.getBookingCode().charAt(0)));
			}

			destinations.add(dto11.getDestinationStatusCode());

			if (!transfer.contains(tmp) & !found(transfer, tmp)) {
				transfer.add(tmp);
			}

		}

		// Generating Fare class wise pnl for empty/ non-booked logical cabin classes
		if (AppSysParamsUtil.isRBDPNLADLEnabled() && !AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			this.injectNonBookedFareClassDetails(fareClasses, destinations, transfer, flightId);
		}

		injectSSRInfoForPNLs(transfer, mapPnrPaxSegIds);

		return transfer;

	}

	private static boolean isSameFareClass(PNLADLDestinationDTO groupDTO, PNLADLDestinationDTO elementDTO) {
		String groupFareClass = groupDTO.getCabinClassCode();
		String elementFareClass = elementDTO.getCabinClassCode();
		if (AppSysParamsUtil.isRBDPNLADLEnabled() && !AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			groupFareClass = groupDTO.getBookingCode();
			elementFareClass = elementDTO.getBookingCode();
		}
		if (groupFareClass != null && elementFareClass != null
				&& groupFareClass.toUpperCase().charAt(0) == elementFareClass.toUpperCase().charAt(0)) {
			return true;
		}
		return false;

	}

	private boolean isSameFareClass(PNLADLDestinationSupportDTO groupDTO, PNLADLDestinationSupportDTO elementDTO) {
		String groupFareClass = groupDTO.getCabinClassCode();
		String elementFareClass = elementDTO.getCabinClassCode();
		if (AppSysParamsUtil.isRBDPNLADLEnabled() && !AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			groupFareClass = groupDTO.getBookingCode();
			elementFareClass = elementDTO.getBookingCode();
		}
		if (groupFareClass != null && elementFareClass != null
				&& groupFareClass.toUpperCase().charAt(0) == elementFareClass.toUpperCase().charAt(0)) {
			return true;
		}
		return false;

	}

	private void injectNonBookedFareClassDetails(Set<String> fareClasses, Set<String> destinations,
			ArrayList<PNLADLDestinationDTO> transfer, Integer flightId) {
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			try {
				for (String des : destinations) {
					for (String allocatedFareClass : getAllocatedFareClassesForFlight(flightId)) {
						if (!fareClasses.contains(String.valueOf(allocatedFareClass.charAt(0)))) {
							PNLADLDestinationDTO tmp = new PNLADLDestinationDTO();
							tmp.setBookingCode(allocatedFareClass);
							tmp.setDestinationAirportCode(des);
							tmp.setCabinClassCode(allocatedFareClass);
							tmp.setPassenger(new ArrayList<ReservationPaxDetailsDTO>());

							if (!transfer.contains(tmp) & !found(transfer, tmp)) {
								transfer.add(tmp);
							}
						}
					}
				}
			} catch (Exception e) {
				// purposely catching the Exception
				e.printStackTrace();
			}
		}
	}

	private List<String> getAllocatedFareClassesForFlight(Integer flightId) {

		List<String> allocatedFareClasses = new ArrayList<String>();
		LinkedHashMap<String, List<String>> fareClassDetails = ReservationModuleUtils.getFlightInventoryBD()
				.getAllocatedBookingClassesForCabinClasses(flightId);
		for (String cabinClass : fareClassDetails.keySet()) {
			for (String fareClass : fareClassDetails.get(cabinClass)) {
				allocatedFareClasses.add(fareClass);
			}
		}
		return allocatedFareClasses;
	}

	private void injectSSRInfoForPNLs(ArrayList<PNLADLDestinationDTO> transfer, Map<Integer, Integer> mapPnrPaxSegIds) {
		// Map<Integer, Collection<PaxSSRDetailDTO>> paxSSRMap =
		// ReservationDAOUtils.DAOInstance.RESERVATION_SSR.getPNRPaxSSRs(
		// mapPnrPaxSegIds, true, false, true);

		for (PNLADLDestinationDTO pnlDTO : transfer) {
			Collection<ReservationPaxDetailsDTO> paxDTOs = pnlDTO.getPassenger();

			for (ReservationPaxDetailsDTO reservationPaxDetailsDTO : paxDTOs) {
				ReservationPaxDetailsDTO paxDetailsDTO = reservationPaxDetailsDTO;

				// Integer paxId = paxDetailsDTO.getPnrPaxId();
				String ssrCode = paxDetailsDTO.getSsrCode();
				String ssrText = paxDetailsDTO.getSsrText();

				// Collection<PaxSSRDetailDTO> ssrDTOs = paxSSRMap.get(paxId);
				// paxDetailsDTO.setSsrDetails(ssrDTOs);

				paxDetailsDTO.setSsrCode(ssrCode);
				paxDetailsDTO.setSsrText(ssrText);
			}

		}
	}

	private static boolean found(ArrayList<PNLADLDestinationDTO> transfer, PNLADLDestinationDTO dto) {

		for (int i = 0; i < transfer.size(); i++) {
			PNLADLDestinationDTO d = transfer.get(i);
			if (d.getDestinationAirportCode().equalsIgnoreCase(dto.getDestinationAirportCode()) && isSameFareClass(d, dto)) {
				return true;
			}
		}
		return false;
	}

	/* ##################################################################### */

	/* ##################################################################### */

	/**
	 * DAOImpl Method to get Active SITA Addresses for airport code and carrier code
	 */
	@Override
	public Map<String, List<String>> getActiveSITAAddresses(String airportCode, String flightNumber, String ond, String carrierCode, DCS_PAX_MSG_GROUP_TYPE msgType) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final Map<String, List<String>> sitaAddressList = new HashMap<String, List<String>>();
		final List<String> mconnectList = new ArrayList<String>();
		final List<String> sitaTexList = new ArrayList<String>();
		final List<String> airincAddressList = new ArrayList<String>();

		String sql = " SELECT DISTINCT sita.AIRPORT_SITA_ID, "
		+  " sita.SITA_ADDRESS, "
		+  " sita.EMAIL_ID, "
		+  " sita.SITA_DELIVERY_METHOD "
		+  " FROM T_SITA_ADDRESS sita "
		+  " LEFT OUTER JOIN T_SITA_ADDRESS_OND ond "
		+  " ON sita.airport_sita_id = ond.airport_sita_id, "
		+  " T_SITA_ADDRESS_CARRIER carrier "
		+  " WHERE sita.AIRPORT_CODE         = ? "
		+  " AND sita.status                 ='ACT' "
		+  " AND carrier.carrier_code        = ? "
		+  " AND (ond.airport_sita_id IS NULL OR ond.ond_code      = ? ) "
		+  " AND sita.airport_sita_id        = carrier.airport_sita_id "

		+ " INTERSECT "

		+  " SELECT DISTINCT sita.AIRPORT_SITA_ID, "
		+  " sita.SITA_ADDRESS, "
		+  " sita.EMAIL_ID, "
		+  " sita.SITA_DELIVERY_METHOD "
		+  " FROM T_SITA_ADDRESS sita "
		+  " LEFT OUTER JOIN T_SITA_ADDRESS_FLIGHT_NUM flightNum "
		+  " ON sita.airport_sita_id = flightNum.airport_sita_id, " 
		+  " T_SITA_ADDRESS_CARRIER carrier "
		+  " WHERE sita.AIRPORT_CODE         = ? "
		+  " AND sita.status                 ='ACT' "
		+  " AND carrier.carrier_code        = ? "
		+  " AND ( flightNum.airport_sita_id IS NULL OR flightNum.flight_number      = ? ) "
		+  " AND sita.airport_sita_id        = carrier.airport_sita_id ";

		Object[] params = { airportCode, carrierCode, ond, airportCode, carrierCode, flightNumber };

		if (DCS_PAX_MSG_GROUP_TYPE.PNL_ADL.equals(msgType)) {
			sql = sql + " and sita.SEND_PNL_ADL = 'Y' ";
		} else if (DCS_PAX_MSG_GROUP_TYPE.PAL_CAL.equals(msgType)) {
			sql = sql + " and sita.SEND_PAL_CAL = 'Y' ";
		}

		templete.query(sql, params,

		new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					
					String sitaAddress = BeanUtils.nullHandler(rs.getString("SITA_ADDRESS"));
					String deliveryMethod = BeanUtils.nullHandler(rs.getString("SITA_DELIVERY_METHOD"));
					
					if (!StringUtil.isNullOrEmpty(sitaAddress) && SITAAddress.SITA_MESSAGE_VIA_SITATEX.equals(deliveryMethod)
							&& !sitaTexList.contains(sitaAddress)) {
						sitaTexList.add(sitaAddress);
					} else if (SITAAddress.SITA_MESSAGE_VIA_ARINC.equals(deliveryMethod)) {

						if (!StringUtil.isNullOrEmpty(sitaAddress) && !airincAddressList.contains(sitaAddress)) {
							airincAddressList.add(sitaAddress);
						}

						String emailIDs = BeanUtils.nullHandler(rs.getString("EMAIL_ID"));
						if (emailIDs.length() > 0) {
							String[] array = emailIDs.split(",");
							for (String element2 : array) {
								String element = BeanUtils.nullHandler(element2);

								if (element.length() > 0) {
									airincAddressList.add(element);
								}
							}
						}

					} else {

						if (!StringUtil.isNullOrEmpty(sitaAddress) && !mconnectList.contains(sitaAddress)) {
							mconnectList.add(sitaAddress);
						}
						String emailIDs = BeanUtils.nullHandler(rs.getString("EMAIL_ID"));

						if (emailIDs.length() > 0) {
							String[] array = emailIDs.split(",");
							for (String element2 : array) {
								String element = BeanUtils.nullHandler(element2);

								if (element.length() > 0) {
									mconnectList.add(element);
								}
							}
						}

					}	
				}
				sitaAddressList.put(SITAAddress.SITA_MESSAGE_VIA_SITATEX, sitaTexList);
				sitaAddressList.put(SITAAddress.SITA_MESSAGE_VIA_MCONNECT, mconnectList);
				sitaAddressList.put(SITAAddress.SITA_MESSAGE_VIA_ARINC, airincAddressList);
				return sitaAddressList;
			}
		});

		return sitaAddressList;
	}

	@Override
	public String getFlightNumber(int flightID) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { new Integer(flightID) };
		String sql = "select FLIGHT_NUMBER from T_FLIGHT where FLIGHT_ID=?";
		final ArrayList<String> list = new ArrayList<String>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String flightNumber = rs.getString("FLIGHT_NUMBER");

					list.add(flightNumber);

				}
				return null;
			}
		});

		String flightNumber = list.get(0);
		return flightNumber;

	}
	
	@Override
	public Map<String, String> getFlightLegs(int flightID) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { new Integer(flightID) };
		String sql = "select LEG_NUMBER, ORIGIN, DESTINATION from T_FLIGHT_LEG where FLIGHT_ID=?";
		final Map<String, String> legNumberWiseOndMap = new HashMap<String, String>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String legNumber = rs.getString("LEG_NUMBER");
					String origin = rs.getString("ORIGIN");
					String destination = rs.getString("DESTINATION");
					String ond = origin + "/" + destination;
					
					legNumberWiseOndMap.put(legNumber, ond);
				}
				return null;
			}
		});

		return legNumberWiseOndMap;

	}

	@Override
	public Date getFlightLocalDate(int flightId, String depatureAirport) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { new Integer(flightId), depatureAirport + "%" };
		String sql = "select EST_TIME_DEPARTURE_LOCAL from T_FLIGHT_SEGMENT where FLIGHT_ID=? AND segment_code like ?";
		final ArrayList<Date> list = new ArrayList<Date>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					Date fd = rs.getDate("EST_TIME_DEPARTURE_LOCAL");
					list.add(fd);

				}
				return null;
			}
		});

		return list.get(0);

	}

	@Override
	public Date getFlightZuluDate(int flightId, String depatureAirport) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { new Integer(flightId), depatureAirport + "%" };
		String sql = "select EST_TIME_DEPARTURE_ZULU from T_FLIGHT_SEGMENT where FLIGHT_ID=? AND segment_code like ?";
		final ArrayList<Date> list = new ArrayList<Date>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					Date fd = rs.getDate("EST_TIME_DEPARTURE_ZULU");
					list.add(fd);

				}
				return null;
			}
		});

		return list.get(0);

	}

	/**
	 * will return the flight id for, for manual pnl generation
	 */
	@Override
	public int getFlightID(String flightNumber, String departureStation, Date dateOfflight) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String dte = dateFormat.format(dateOfflight);

		DataSource ds = ReservationModuleUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { flightNumber, departureStation, dte, PNLConstants.FlightStatus.FLIGHT_CANCELED,
				PNLConstants.FightSegmentValidity.VALID_SEGMENT };

		// String sql = "select FLIGHT_ID from T_FLIGHT where FLIGHT_NUMBER=?
		// and ORIGIN=? and trunc(DEPARTURE_DATE)=?";

		String sql = " select f.FLIGHT_ID " + " from T_FLIGHT f, T_FLIGHT_SEGMENT fs " + " where "
				+ " f.flight_id = fs.flight_id and " + " f.flight_number = ? " + " and substr(fs.segment_code,1,3)=? "
				+ " and trunc(fs.EST_TIME_DEPARTURE_LOCAL)=? " + " and f.status != ? and fs.segment_valid_flag = ? "
				+ " and f.cs_oc_flight_number IS NULL AND f.cs_oc_carrier_code IS NULL";

		final ArrayList<Integer> list = new ArrayList<Integer>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					int fid = rs.getInt("FLIGHT_ID");
					list.add(new Integer(fid));

				}
				return null;
			}
		});

		Integer flightId = null;
		if (list.size() > 0) {
			flightId = list.get(0);
		} else {
			flightId = new Integer(-1);
		}
		return flightId.intValue();

	}

	/**
	 * will return the flight id for, for manual pnl generation
	 */
	@Override
	public PnlDataContext getFlightInfo(String flightNumber, String departureStation, Date dateOfflight) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String dte = dateFormat.format(dateOfflight);

		DataSource ds = ReservationModuleUtils.getDatasource();

		PnlDataContext dataContext = new PnlDataContext();
		dataContext.setFlightId(-1);
		
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { flightNumber, departureStation, dte, PNLConstants.FlightStatus.FLIGHT_CANCELED,
				PNLConstants.FightSegmentValidity.VALID_SEGMENT };

		// String sql = "select FLIGHT_ID from T_FLIGHT where FLIGHT_NUMBER=?
		// and ORIGIN=? and trunc(DEPARTURE_DATE)=?";

		String sql = " select f.FLIGHT_ID, f.ORIGIN,f.DESTINATION " + " from T_FLIGHT f, T_FLIGHT_SEGMENT fs " + " where "
				+ " f.flight_id = fs.flight_id and " + " f.flight_number = ? " + " and substr(fs.segment_code,1,3)=? "
				+ " and trunc(fs.EST_TIME_DEPARTURE_LOCAL)=? " + " and f.status != ? and fs.segment_valid_flag = ? "
				+ " and f.cs_oc_flight_number IS NULL AND f.cs_oc_carrier_code IS NULL";

		final ArrayList<Integer> list = new ArrayList<Integer>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					int fid = rs.getInt("FLIGHT_ID");
					dataContext.setFlightId(fid);
					dataContext.setFlightOriginAirportCode(rs.getString("ORIGIN"));
					dataContext.setFlightDestinationAirportCode(rs.getString("DESTINATION"));

				}
				return dataContext;
			}
		});

		return dataContext;

	}
	
	@Override
	public String getSavedPnlAdl(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException {
		final List<String> pnlADLMessages = new ArrayList<String>();
		boolean hasNoSita = false;
		StringBuilder strQuery = new StringBuilder();
		if (timestamp == null || sitaAddress == null || departureStation == null || messageType == null
				|| transmissionStatus == null) {
			return null;
		}
		if (sitaAddress.indexOf('@') != -1) {
			hasNoSita = true;
		}

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		strQuery.append("select SENT_MESSAGE from T_PNLADL_TX_HISTORY ");
		strQuery.append("where FLIGHT_ID =  " + flightID);
		strQuery.append(" and TRANSMISSION_TIMESTAMP = to_date('");
		strQuery.append(simpleDateFormat.format(timestamp));
		strQuery.append("','DD/MM/YYYY HH24:MI:SS') ");
		if (hasNoSita) {
			strQuery.append(" and EMAIL_ID = '");
		} else {
			strQuery.append(" and SITA_ADDRESS = '");
		}
		strQuery.append(sitaAddress.trim() + "' " + " and AIRPORT_CODE = '");
		strQuery.append(departureStation.trim() + "' " + " and MESSAGE_TYPE = '");
		strQuery.append(messageType.toUpperCase().trim() + "' ");
		strQuery.append(" and TRANSMISSION_STATUS = '");
		strQuery.append(transmissionStatus.toUpperCase().trim() + "' ");

		// String sql = "select SENT_MESSAGE from T_PNLADL_TX_HISTORY "
		// + "where FLIGHT_ID =  " + flightID
		// + " and TRANSMISSION_TIMESTAMP = to_date('"
		// + simpleDateFormat.format(timestamp)
		// + "','DD/MM/YYYY HH24:MI:SS') " + " and SITA_ADDRESS = '"
		// + sitaAddress.trim() + "' " + " and AIRPORT_CODE = '"
		// + departureStation.trim() + "' " + " and MESSAGE_TYPE = '"
		// + messageType.toUpperCase().trim() + "' "
		// + " and TRANSMISSION_STATUS = '"
		// + transmissionStatus.toUpperCase().trim() + "' ";

		templete.query(strQuery.toString(),

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Blob blobMSG = null;
				while (rs.next()) {
					blobMSG = rs.getBlob("SENT_MESSAGE");
					if (blobMSG != null) {
						pnlADLMessages.add(new String(blobMSG.getBytes(1, (int) blobMSG.length())));
						return pnlADLMessages;
					}
				}
				return pnlADLMessages;
			}
		});
		// templete.queryForList(queryString);
		// templete.setMaxRows(10)

		if (pnlADLMessages != null && !pnlADLMessages.isEmpty()) {
			return pnlADLMessages.get(0);
		}

		return null;

	}

	/**
	 * see ReservationAuxilliaryDAO:getFailuresWithin(int numOfMinutes)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Integer, PnlAdlDTO> getFailuresWithin(int mins, DCS_PAX_MSG_GROUP_TYPE msgType) {

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(new Date());
		calendar.set(GregorianCalendar.MINUTE, calendar.get(GregorianCalendar.MINUTE) - mins);

		Object[] params = { calendar.getTime() };
		
		String sql = getFailuresWithinQuery(msgType);
		
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);		

		Object col = template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				HashMap<Integer, PnlAdlDTO> failedPnlMap = new HashMap<Integer, PnlAdlDTO>();

				while (rs.next()) {

					PnlAdlDTO historyDTO = new PnlAdlDTO();
					historyDTO.setHisID(new Integer(rs.getInt("ID")));
					historyDTO.setFlightId(new Integer(rs.getInt("flight_id")));
					historyDTO.setAirportCode(rs.getString("airport_code"));
					historyDTO.setFlightNum(rs.getString("flight_number"));
					historyDTO.setMessageType(rs.getString("message_type"));
					historyDTO.setFlightDepartureDateZulu(rs.getTimestamp("est_time_departure_zulu"));
					historyDTO.setFlightDepartureDateLocal(rs.getTimestamp("est_time_departure_local"));

					historyDTO.setSitaAddress(rs.getString("sita_address"));
					historyDTO.setTransmissionTimeStamp(rs.getTimestamp("transmission_timestamp"));

					failedPnlMap.put(historyDTO.getHisID(), historyDTO);
				}

				return failedPnlMap;
			}

		});

		return (HashMap<Integer, PnlAdlDTO>) col;
	}
	
	private String getFailuresWithinQuery(DCS_PAX_MSG_GROUP_TYPE msgType){
		String sql ="";
		if(DCS_PAX_MSG_GROUP_TYPE.PNL_ADL.equals(msgType)){
			sql += " SELECT  his.ID as ID, "
					+ " flt.flight_id, flt.flight_number, "
					+ " seg.est_time_departure_zulu, seg.est_time_departure_local, his.message_type, " 
					+ " his.transmission_status, "
					+ " his.sita_address, " 
					+ " his.airport_code, " 
					+ " his.transmission_timestamp" 
					+ " FROM  "
					+ " t_flight flt, t_pnladl_tx_history his, t_flight_segment seg " 
					+ " WHERE "
					+ " flt.flight_id = his.flight_id and " 
					+ " flt.flight_id = seg.flight_id and   " 
					+ " flt.status != 'CNX' and   "
					+ " his.transmission_timestamp > ? and " 
					+ " his.transmission_status='N' " 
					+ " ORDER BY flt.flight_id, his.sita_address asc ";
				
		}else if(DCS_PAX_MSG_GROUP_TYPE.PAL_CAL.equals(msgType)){
			
			sql += " SELECT  his.pal_cal_tx_id as ID, "
					+ " flt.flight_id, flt.flight_number, "
					+ " seg.est_time_departure_zulu, seg.est_time_departure_local, his.message_type, " 
					+ " his.transmission_status, "
					+ " his.sita_address, " 
					+ " his.airport_code, " 
					+ " his.transmission_timestamp" 
					+ " FROM  "
					+ " t_flight flt, t_pal_cal_tx_history his, t_flight_segment seg " 
					+ " WHERE "
					+ " flt.flight_id = his.flight_id and " 
					+ " flt.flight_id = seg.flight_id and   " 
					+ " flt.status != 'CNX' and   "
					+ " his.transmission_timestamp > ? and " 
					+ " his.transmission_status='N' " 
					+ " ORDER BY flt.flight_id, his.sita_address asc ";
		}
		return sql;
	}

	/**
	 * @see com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO#getNumberOfAttempts(java.lang.Integer,
	 *      java.lang.String)
	 */
	@Override
	public int getNumberOfFailedAttempts(Integer flightId, String sitaAddress, String msgType) {
		
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { sitaAddress, flightId, msgType };

		String sql = getNumberOfFailedAttemptsQuery(msgType);
		
		Object count = template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));
				}
				return count;
			}

		});

		return ((Integer) (count)).intValue();
	}
	
	private String getNumberOfFailedAttemptsQuery(String msgType){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String sql = "";
		
		if(PNLConstants.MessageTypes.PNL.equals(msgType) || PNLConstants.MessageTypes.ADL.equals(msgType)){
			sql += " SELECT  count(flt.flight_id) " 
					+ " FROM  " 
					+ " t_flight flt, t_pnladl_tx_history his " 
					+ " WHERE "
					+ " flt.flight_id = his.flight_id and " 
					+ " flt.status != 'CNX'  and " 
					+ " trunc(his.transmission_timestamp) = '"+ dateFormat.format(new Date()) + "'" 
					+ " and " + " his.transmission_status='N' and "
					+ " his.sita_address = ? and " 
					+ " flt.flight_id = ? and " 
					+ " his.message_type = ? "
					+ " ORDER BY flt.flight_id, his.sita_address asc ";
		}else if(PNLConstants.MessageTypes.PAL.equals(msgType) || PNLConstants.MessageTypes.CAL.equals(msgType)){
			sql += " SELECT  count(flt.flight_id) " 
					 + " FROM  " 
					 + " t_flight flt, t_pal_cal_tx_history his " 
					 + " WHERE "					 				
					 + " flt.flight_id = his.flight_id and " 
					 + " flt.status != 'CNX'  and " 
					 + " trunc(his.transmission_timestamp) = '" + dateFormat.format(new Date()) + "'" 
					 + " and "  + " his.transmission_status='N' and "					 				
					 + " his.sita_address = ? and " 
					 + " flt.flight_id = ? and " 
					 + " his.message_type = ? "					 
					 + " ORDER BY flt.flight_id, his.sita_address asc ";
		}
		return  sql;
	}

	/**
	 * @see com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO#hasSuccessfulTxn(java.lang.Integer,
	 *      java.lang.String, java.lang.String)
	 */
	@Override
	public int getNumberOfSuccessAtempts(Integer flightId, String sitaAddress, String msgType, Timestamp failedTimeStamp) {
		
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { sitaAddress, flightId, msgType };

		String sql = getNumberOfSuccessAtemptsQuery(msgType, failedTimeStamp);

		Object count = template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));
				}
				return count;
			}

		});

		return ((Integer) (count)).intValue();
	}
	
	private String getNumberOfSuccessAtemptsQuery(String msgType, Timestamp failedTimeStamp){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String sql = "";
		
		if(PNLConstants.MessageTypes.PNL.equals(msgType) || PNLConstants.MessageTypes.ADL.equals(msgType)){
			sql += " SELECT  count(flt.flight_id) " 
					+ " FROM  " 
					+ " t_flight flt, t_pnladl_tx_history his " 
					+ " WHERE "
										
					+ " flt.flight_id = his.flight_id and " 
					+ " flt.status != 'CNX'  and " 
					+ " his.transmission_timestamp > to_date("										
					+ CalendarUtil.formatForSQL_toDate(failedTimeStamp) 
					+ ") and" 
					+ " his.transmission_status='Y' and "
										
					+ " his.sita_address = ? and " 
					+ " flt.flight_id = ? and " 
					+ " his.message_type = ? "
										
					+ " ORDER BY flt.flight_id, his.sita_address asc ";
		}else if(PNLConstants.MessageTypes.PAL.equals(msgType) || PNLConstants.MessageTypes.CAL.equals(msgType)){
			sql += " SELECT  count(flt.flight_id) " 
					+ " FROM  " 
					+ " t_flight flt, t_pal_cal_tx_history his " 
					+ " WHERE "
										
					+ " flt.flight_id = his.flight_id and " 
					+ " flt.status != 'CNX'  and " 
					+ " his.transmission_timestamp > to_date("										
					+ CalendarUtil.formatForSQL_toDate(failedTimeStamp) 
					+ ") and" 
					+ " his.transmission_status='Y' and "
										
					+ " his.sita_address = ? and " 
					+ " flt.flight_id = ? and " 
					+ " his.message_type = ? "
										
					+ " ORDER BY flt.flight_id, his.sita_address asc ";
		}
		return  sql;
	}

	/**
	 * Returns a List of String pnrPaxFareSegIds'
	 * 
	 * @param flightSegId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, String> getPnrPaxFareSegIds(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds) {

		String sql = "";
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String inStrPnrSegIds = PlatformUtiltiies.constructINStringForInts(pnrSegIds);
		String inStrPnrPaxIds = PlatformUtiltiies.constructINStringForInts(pnrPaxIds);

		sql = "  SELECT paxseg.ppfs_id, paxfare.pnr_pax_id,paxseg.pnr_seg_id   FROM t_pnr_pax_fare_segment paxseg, "
				+ " t_pnr_pax_fare paxfare " + " where paxfare.ppf_id = paxseg.ppf_id " + " and  paxfare.pnr_pax_id in ("
				+ inStrPnrPaxIds + ")" + " and paxseg.pnr_seg_id in (" + inStrPnrSegIds + ")";
		Map<Integer, String> mapPpfsIdTopnrpaxsegID = (Map<Integer, String>) template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				// List pnrSegIds = new ArrayList();
				Map<Integer, String> mapPpfsIdTopnrpaxsegID = new HashMap<Integer, String>();

				while (rs.next()) {
					String pnrPaxId = rs.getString("pnr_pax_id");
					String pnrSegId = rs.getString("pnr_seg_id");
					String uniqueId = pnrPaxId + "|" + pnrSegId;
					mapPpfsIdTopnrpaxsegID.put(new Integer(rs.getInt("ppfs_id")), uniqueId);
				}
				return mapPpfsIdTopnrpaxsegID;
			}

		});

		return mapPpfsIdTopnrpaxsegID;

	}

	/**
	 * 
	 * @param flightNumber
	 * @param flightDepZulu
	 * @param airportCode
	 * @return
	 */
	@Override
	public boolean hasPnlHistory(String flightNumber, Date flightDepZulu, String airportCode) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String depZulu = dateFormat.format(flightDepZulu);

		String sql = " SELECT count(a.flight_id) " + " FROM t_flight_segment a , t_flight b, t_pnladl_tx_history his "
				+ " where " + " a.flight_id = his.flight_id(+) and a.flight_id = b.flight_id" + " and "
				+ " trunc(a.est_time_departure_zulu) ='" + depZulu + "'" + " and b.status != 'CNX' "
				+ " and trim(flight_number) = '" + flightNumber.trim() + "'" + " and substr(a.segment_code,1,3)='" + airportCode
				+ "'" + " and his.message_type='" + PNLConstants.MessageTypes.PNL + "' " + " and his.SENT_METHOD='"
				+ PNLConstants.SendingMethod.SCHEDSERVICE + "'";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object intObj = template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}

		if (numOfRecs >= 1) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 
	 * @param flightNumber
	 * @param flightDepZulu
	 * @param airportCode
	 * @return
	 */
	@Override
	public boolean hasAnyPnlHistory(String flightNumber, Date flightDepZulu, String airportCode) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String depZulu = dateFormat.format(flightDepZulu);

		String sql = " SELECT count(a.flight_id) " + " FROM t_flight_segment a , t_flight b, t_pnladl_tx_history his "
				+ " where " + " a.flight_id = his.flight_id(+) and a.flight_id = b.flight_id" + " and "
				+ " trunc(a.est_time_departure_zulu) ='" + depZulu + "'" + " and b.status != 'CNX' "
				+ " and trim(flight_number) = '" + flightNumber.trim() + "'" + " and substr(a.segment_code,1,3)='" + airportCode
				+ "'" + " and his.message_type='" + PNLConstants.MessageTypes.PNL + "' ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object intObj = template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}

		if (numOfRecs >= 1) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @see ReservationAuxilliaryDAO savePnlAdlHistory
	 */
	@Override
	public void savePnlAdlHistory(PnlAdlHistory pnlAdlHistory) {

		hibernateSaveOrUpdate(pnlAdlHistory);

	}

	/**
	 * will get the pax countin the pnl
	 * 
	 * @param flightId
	 * @param airportCode
	 * @return
	 */
	@Override
	public Object[] getPaxCountInPnlADL(Integer flightId, String airportCode) {

		String sql = " select cnt.cabin_class_code, cnt.logical_cabin_class_code, cnt.booking_code, cnt.pax_count, cnt.last_group_id from t_pnladl_pax_count cnt "
				+ " where cnt.his_id IN (select his.id from t_pnladl_tx_history his where "
				+ " his.flight_id = ? and his.airport_code = ? and (his.transmission_status = ? or his.transmission_status = ?) "
				+ " and his.sent_method is not null) order by cnt.ID ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object params[] = { flightId, airportCode, PNLConstants.SITAMsgTransmission.SUCCESS,
				PNLConstants.SITAMsgTransmission.PARTIALLY };

		Object[] intObj = (Object[]) template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();
				String lastGroupCode = new String();
				while (rs.next()) {

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							ccPaxMap.put(rs.getString("logical_cabin_class_code"), new Integer(rs.getInt("pax_count")));
						} else {
							ccPaxMap.put(rs.getString("booking_code"), new Integer(rs.getInt("pax_count")));
						}
					} else {
						ccPaxMap.put(rs.getString("cabin_class_code"), new Integer(rs.getInt("pax_count")));
					}

					lastGroupCode = rs.getString("last_group_id");
				}

				return new Object[] { ccPaxMap, lastGroupCode };
			}

		});

		if (intObj == null) {
			throw new CommonsDataAccessException(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL);
		}

		return (intObj);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getClassOfServicesForFlight(int flightId) {

		String sql = "select c.cabin_class_code from t_flight t, t_aircraft_model m, t_aircraft_cc_capacity c  where t.flight_id=?"
				+ " and t.model_number=m.model_number " + " and m.model_number=c.model_number";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object params[] = { new Integer(flightId) };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> coss = new ArrayList<String>();
				while (rs.next()) {
					coss.add(rs.getString("cabin_class_code"));

				}

				return coss;
			}

		});

		return ((Collection<String>) intObj);

	}

	/**
	 * 
	 * @returns this.maxConnectionTime
	 */
	private Double getMaximumConnectionTime() {

		if (ReservationAuxilliaryDAOImpl.maxConnectionTime == null) {

			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);
			String maxConnectTimeSQL = "select (TO_DATE(TO_CHAR(MOD(TO_NUMBER(SUBSTR(PARAM_VALUE,1,2)),24)) || "
					+ "SUBSTR(PARAM_VALUE,3,3),'HH24:MI') "
					+ " - TO_DATE('00:00','HH24:MI') + floor(TO_NUMBER(SUBSTR(PARAM_VALUE,1,2))/24) ) ConnectTime "
					+ " from T_APP_PARAMETER " + "where PARAM_KEY = '" + SystemParamKeys.MAX_TRANSIT_TIME + "'";

			ReservationAuxilliaryDAOImpl.maxConnectionTime = (Double) templete.query(maxConnectTimeSQL, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs.next()) {
						return new Double(rs.getDouble("ConnectTime"));
					}
					return null;
				}
			});
		}

		return ReservationAuxilliaryDAOImpl.maxConnectionTime;
	}

	private Double[] getMinMaxConnectionTimeFromHub(String departureStation) {
		// [min,max]
		Double[] minMaxConnectionTimes = null;

		String[] minMaxTransitDurations = null;
		try {
			minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(departureStation, null, null);
		} catch (ModuleRuntimeException ex) {
			if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(ex.getExceptionCode())) {
				return null;
			}
		}

		String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
		String[] maxTransitHoursMin = minMaxTransitDurations[1].split(":");

		Double minConnectTimeInMinsInHours = (Double.valueOf(minTransitHoursMin[1]) / 60);
		Double minConnectTimeInHours = Double.valueOf(minTransitHoursMin[0]);
		Double maxConnectTimeInMinsInHours = (Double.valueOf(maxTransitHoursMin[1]) / 60);
		Double maxConnectTimeInHours = Double.valueOf(maxTransitHoursMin[0]);

		Double minConnectTime = (minConnectTimeInHours + minConnectTimeInMinsInHours) / 24;
		Double maxConnectTime = (maxConnectTimeInHours + maxConnectTimeInMinsInHours) / 24;

		minMaxConnectionTimes = new Double[] { minConnectTime, maxConnectTime };

		return minMaxConnectionTimes;
	}

	/**
	 * Convert HH:MI to days
	 * 
	 * @param time
	 * @return
	 */
	private Double convertStrTimeToDate(String time) {
		String[] hoursAndMins = time.split(":");

		Double minsInHours = (Double.valueOf(hoursAndMins[1]) / 60);

		return ((Double.valueOf(hoursAndMins[0]) + minsInHours) / 24);
	}

	/**
	 * 
	 * @returns this.minConnectionTime
	 */
	private Double getMinimumConnectionTime() {
		if (ReservationAuxilliaryDAOImpl.minConnectionTime == null) {
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);
			// Get minimum connect time in days
			String minConnectTimeSQL = "select (TO_DATE(TO_CHAR(MOD(TO_NUMBER(SUBSTR(PARAM_VALUE,1,2)),24)) || "
					+ "SUBSTR(PARAM_VALUE,3,3),'HH24:MI') "
					+ " - TO_DATE('00:00','HH24:MI') + floor(TO_NUMBER(SUBSTR(PARAM_VALUE,1,2))/24) ) ConnectTime "
					+ " from T_APP_PARAMETER " + "where PARAM_KEY = '" + SystemParamKeys.MIN_TRANSIT_TIME + "'";

			ReservationAuxilliaryDAOImpl.minConnectionTime = (Double) templete.query(minConnectTimeSQL, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs.next()) {
						return new Double(rs.getDouble("ConnectTime"));
					}
					return null;
				}
			});
		}
		return ReservationAuxilliaryDAOImpl.minConnectionTime;
	}

	/**
	 * 
	 * @param maximumConnectionTime
	 * @return
	 */
	private Double getMaximumConnectionTimeForManifest(String maximumConnectionTime) {

		Double maxConnectTimeM = null;
		if (maxConnectTimeM == null) {
			DataSource ds = ReservationModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);

			maximumConnectionTime = maximumConnectionTime.trim();

			String maxConnectTimeSQL = "select (TO_DATE(TO_CHAR(MOD(TO_NUMBER(SUBSTR('" + maximumConnectionTime
					+ "',1,2)),24)) || " + "SUBSTR('" + maximumConnectionTime + "',3,3),'HH24:MI') "
					+ " - TO_DATE('00:00','HH24:MI') + floor(TO_NUMBER(SUBSTR('" + maximumConnectionTime
					+ "',1,2))/24) ) ConnectTime " + " from dual ";

			maxConnectTimeM = (Double) templete.query(maxConnectTimeSQL, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs.next()) {
						return new Double(rs.getDouble("ConnectTime"));
					}
					log.error("!!!CHECK ME: MAXIMUM CONNECTION TIME NOT FOUND/NULL ");
					return null;
				}
			});
		}

		if (maxConnectTimeM == null) {
			log.error("!!!CHECK ME: MAXIMUM CONNECTION TIME NOT FOUND/NULL ");
		}
		return maxConnectTimeM;
	}

	/**
	 * savePnlPassengers(Collection pnlPassengers)
	 */
	@Override
	public void savePnlPassengers(Collection<PnlPassenger> pnlPassengers) {
		hibernateSaveOrUpdateAll(pnlPassengers);
	}

	/**
	 * 
	 * @param pnrSegId
	 * @param pnr
	 * @return
	 */
	@Override
	public List<PnlPassenger> getPnlPassegers(Integer pnrSegId, String status, boolean excludeTBAPax) {
		String hqlSkipTBA = "";
		if (excludeTBAPax) {
			hqlSkipTBA = " and REPLACE(pnlPax.firstName, ' ', '') != 'TBA' and REPLACE(pnlPax.firstName, ' ', '') != 'TBA'";
		}
		String hql = "select pnlPax from PnlPassenger as pnlPax " + " where pnlPax.pnrSegId = '" + pnrSegId + "' "
				+ " and pnlPax.status= '" + status + "'" + hqlSkipTBA;

		return find(hql, PnlPassenger.class);
	}

	@Override
	public List<PnlPassenger> getPnlPassegers(Set<String> pnrs, List<String> adlActions, String status) {
		String hql = "select pnlPax from PnlPassenger as pnlPax " + " where pnlPax.pnr in ("
				+ Util.buildStringInClauseContent(pnrs) + ") ";
		if (status != null) {
			hql += " and pnlPax.status= '" + status + "'";
		}
		hql += " and pnlPax.pnlStatus in (" + Util.buildStringInClauseContent(adlActions) + ")";

		return find(hql, PnlPassenger.class);
	}

	@Override
	public List<PnlPassenger> getPnlPassegers(Collection<Integer> pnrSegIds) {
		String hql = "select pnlPax from PnlPassenger as pnlPax " + " where pnlPax.status= 'N' and pnlPax.pnrSegId in( "
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ")";

		return find(hql, PnlPassenger.class);
	}

	@Override
	public void updatePnlPassegerStatusToSent(Collection<Integer> pnlPaxIds) {
			String sql = "";
			sql = " update T_PNL_PASSENGER set status = 'Y' where  pnl_pax_id in ("
					+ Util.buildIntegerInClauseContent(pnlPaxIds) + ") and status = 'N' ";
			JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			jt.update(sql);
	}
	
	@Override
	public List<PnlPassenger> getPnlPassegersFromPnlPaxIds(Collection<Integer> pnlPaxIds) {
		String hql = "select pnlPax from PnlPassenger as pnlPax " + " where pnlPax.status= 'N' and pnlPax.pnlPassengerId in( "
				+ Util.buildIntegerInClauseContent(pnlPaxIds) + ")";

		return find(hql, PnlPassenger.class);
	}

	@Override
	public List<PnlPassenger> getPnlPassegers(Collection<Integer> pnrSegIds, Collection<Integer> pnrPaxIds, String status) {
		String hql = "select pnlPax from PnlPassenger as pnlPax " + " where pnlPax.pnrSegId in ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ")" + " and pnlPax.pnrPaxId in ("
				+ Util.buildIntegerInClauseContent(pnrPaxIds) + ")" + " and pnlPax.status = '" + status + "'";

		return find(hql, PnlPassenger.class);
	}

	@Override
	public FlightLegPaxTypeCountTO getFlightLegPaxTypeCount(String flightNumber, String origin, String destination,
			Date departureDateZulu) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		// Changed as per CEASAR requirement - AARESAA-8330
		String segmentCode = "%" + origin + "%" + destination + "%";
		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String fltDate = df.format(departureDateZulu);
		String fltDateStart = fltDate + " 00:00:00";
		String fltDateEnd = fltDate + " 23:59:59";

		StringBuilder sqlBuilder = new StringBuilder();
		if (AppSysParamsUtil.isStandByPaxExcluded()) {
			sqlBuilder.append("SELECT COUNT(PP.PNR_PAX_ID) AS TOTALCOUNT,  PT.GENDER,  PP.PAX_TYPE_CODE,  PP.STATUS, ");
			sqlBuilder.append(" SUM(NVL(BAGGAGE_WEIGHT,0))BAGGAGE_WEIGHT ");
			sqlBuilder.append(" FROM T_PNR_PASSENGER PP JOIN T_PNR_SEGMENT PS ON  PS.PNR = PP.PNR ");
			sqlBuilder.append(" JOIN T_FLIGHT_SEGMENT FS ON PS.FLT_SEG_ID=FS.FLT_SEG_ID ");
			sqlBuilder.append(" JOIN T_FLIGHT F ON FS.FLIGHT_ID=F.FLIGHT_ID ");
			sqlBuilder.append(" LEFT JOIN T_PAX_TITLE PT ON PP.TITLE  =PT.TITLE_CODE ");
			sqlBuilder.append(
					" LEFT JOIN BG_T_PNR_PAX_SEG_BAGGAGE PSB  ON PSB.PNR_PAX_ID=PP.PNR_PAX_ID AND PSB.PNR_SEG_ID=PS.PNR_SEG_ID ");
			sqlBuilder.append(" LEFT JOIN BG_T_BAGGAGE B ON PSB.BAGGAGE_ID=B.BAGGAGE_ID ");
			sqlBuilder.append(" LEFT JOIN T_PNR_PAX_FARE PPF ON PPF.PNR_PAX_ID = PP.PNR_PAX_ID ");
			sqlBuilder.append(
					" LEFT JOIN T_PNR_PAX_FARE_SEGMENT PPFS ON PPFS.PNR_SEG_ID = PS.PNR_SEG_ID AND PPFS.PPF_ID = PPF.PPF_ID ");
			sqlBuilder.append(" LEFT JOIN T_BOOKING_CLASS TBC ON PPFS.BOOKING_CODE =TBC.BOOKING_CODE ");
			sqlBuilder.append(" WHERE FS.SEGMENT_CODE LIKE UPPER('");
			sqlBuilder.append(segmentCode);
			sqlBuilder.append("') AND FS.EST_TIME_DEPARTURE_ZULU between TO_DATE('");
			sqlBuilder.append(fltDateStart);
			sqlBuilder.append("', 'DD-MON-YYYY HH24:mi:ss' ) AND TO_DATE('");
			sqlBuilder.append(fltDateEnd);
			sqlBuilder.append("', 'DD-MON-YYYY HH24:mi:ss' ) AND FS.FLIGHT_ID=F.FLIGHT_ID ");
			sqlBuilder.append(" AND F.FLIGHT_NUMBER=UPPER('");
			sqlBuilder.append(flightNumber);
			sqlBuilder.append("') AND F.STATUS <> 'CNX' ");
			sqlBuilder.append(" AND   PS.STATUS ='CNF' ");
			sqlBuilder.append(" AND    (PSB.STATUS='RES' OR PSB.STATUS IS NULL) ");
			sqlBuilder.append(
					" AND (TBC.BC_TYPE <>'" + BookingClass.BookingClassType.STANDBY + "' OR  TBC.BC_TYPE IS NULL )");
			sqlBuilder.append(" GROUP BY PT.GENDER,PP.PAX_TYPE_CODE,PP.STATUS");
		} else {
			sqlBuilder.append("SELECT COUNT(PP.PNR_PAX_ID) AS TOTALCOUNT,  PT.GENDER,  PP.PAX_TYPE_CODE,  PP.STATUS, ");
			sqlBuilder.append(" SUM(NVL(BAGGAGE_WEIGHT,0))BAGGAGE_WEIGHT ");
			sqlBuilder.append(" FROM T_PNR_PASSENGER PP JOIN T_PNR_SEGMENT PS ON  PS.PNR = PP.PNR ");
			sqlBuilder.append(" JOIN T_FLIGHT_SEGMENT FS ON PS.FLT_SEG_ID=FS.FLT_SEG_ID ");
			sqlBuilder.append(" JOIN T_FLIGHT F ON FS.FLIGHT_ID=F.FLIGHT_ID ");
			sqlBuilder.append(" LEFT JOIN T_PAX_TITLE PT ON PP.TITLE  =PT.TITLE_CODE ");
			sqlBuilder.append(
					" LEFT JOIN BG_T_PNR_PAX_SEG_BAGGAGE PSB  ON PSB.PNR_PAX_ID=PP.PNR_PAX_ID AND PSB.PNR_SEG_ID=PS.PNR_SEG_ID ");
			sqlBuilder.append(" LEFT JOIN BG_T_BAGGAGE B ON PSB.BAGGAGE_ID=B.BAGGAGE_ID ");
			sqlBuilder.append(" WHERE FS.SEGMENT_CODE LIKE UPPER('");
			sqlBuilder.append(segmentCode);
			sqlBuilder.append("') AND FS.EST_TIME_DEPARTURE_ZULU between TO_DATE('");
			sqlBuilder.append(fltDateStart);
			sqlBuilder.append("', 'DD-MON-YYYY HH24:mi:ss' ) AND TO_DATE('");
			sqlBuilder.append(fltDateEnd);
			sqlBuilder.append("', 'DD-MON-YYYY HH24:mi:ss' ) AND FS.FLIGHT_ID=F.FLIGHT_ID ");
			sqlBuilder.append(" AND F.FLIGHT_NUMBER=UPPER('");
			sqlBuilder.append(flightNumber);
			sqlBuilder.append("') AND F.STATUS <> 'CNX' ");
			sqlBuilder.append(" AND   PS.STATUS ='CNF' ");
			sqlBuilder.append(" AND    (PSB.STATUS='RES' OR PSB.STATUS IS NULL) ");
			sqlBuilder.append(" GROUP BY PT.GENDER,PP.PAX_TYPE_CODE,PP.STATUS");
		}
		return (FlightLegPaxTypeCountTO) templete.query(sqlBuilder.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				FlightLegPaxTypeCountTO flightLegPaxTypeCountTO = new FlightLegPaxTypeCountTO();
				// PaxTypeCountTO cnfCountTO = new PaxTypeCountTO();
				// PaxTypeCountTO onhold = new PaxTypeCountTO();
				PaxTypeCountTO countTO = null;
				while (rs.next()) {
					int countValue = rs.getInt("totalCount");
					String strPaxType = rs.getString("pax_type_code");
					String strGender = rs.getString("gender");
					String bookingStatus = rs.getString("status");
					int baggageWight = rs.getInt("BAGGAGE_WEIGHT");

					if (bookingStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
						if (flightLegPaxTypeCountTO.getConfirmedPaxSummary() == null) {
							countTO = new PaxTypeCountTO();
						} else {
							countTO = flightLegPaxTypeCountTO.getConfirmedPaxSummary();
						}
					} else {
						if (flightLegPaxTypeCountTO.getOnholdPaxSummary() == null) {
							countTO = new PaxTypeCountTO();
						} else {
							countTO = flightLegPaxTypeCountTO.getOnholdPaxSummary();
						}
					}

					if (strPaxType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
						if (strGender != null && strGender.equals(ReservationInternalConstants.Gender.MALE)) {
							countTO.setAdultMaleCount(countTO.getAdultMaleCount() + countValue);
						} else if (strGender != null && strGender.equals(ReservationInternalConstants.Gender.FEMALE)) {
							countTO.setAdultFemaleCount(countTO.getAdultFemaleCount() + countValue);
						} else {
							countTO.setOthersCount(countTO.getOthersCount() + countValue);
						}
					} else if (strPaxType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
						countTO.setChildCount(countTO.getChildCount() + countValue);
					} else {
						countTO.setInfantCount(countTO.getInfantCount() + countValue);
					}
					countTO.setBaggageWeight(countTO.getBaggageWeight() + baggageWight );

					if (bookingStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
						flightLegPaxTypeCountTO.setConfirmedPaxSummary(countTO);
					} else {
						flightLegPaxTypeCountTO.setOnholdPaxSummary(countTO);
					}

				}

				return flightLegPaxTypeCountTO;
			}
		});

	}

	@Override
	public Map<String, List<Integer>> getInconsitentInsurances(Date startingFrom) {

		Timestamp timeStamp = new Timestamp(startingFrom.getTime());

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT r.pnr, ");
		sb.append("   a.ins_id ");
		sb.append(" FROM t_pnr_insurance a, ");
		sb.append("   t_reservation r ");
		sb.append(" WHERE a.pnr             =r.pnr ");
		sb.append(" AND r.booking_timestamp > to_date( ");
		sb.append(CalendarUtil.formatForSQL_toDate(timeStamp)).append(") ");
		sb.append(" AND r.status            ='CNF' ");
		sb.append(" AND ( (a.state          = '");
		sb.append(ReservationInternalConstants.INSURANCE_STATES.SC).append("'");
		sb.append(" AND a.policy_code      IS NULL ) ");
		sb.append(" OR a.state             = '");
		sb.append(ReservationInternalConstants.INSURANCE_STATES.FL).append("'");
		sb.append(" OR a.state             = '");
		sb.append(ReservationInternalConstants.INSURANCE_STATES.RQ).append("'");
		sb.append(" OR a.state             = '");
		sb.append(ReservationInternalConstants.INSURANCE_STATES.TO).append("')");

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		return (Map<String, List<Integer>>) templete.query(sb.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, List<Integer>> inconsistentData = new HashMap<String, List<Integer>>();

				while (rs.next()) {

					Integer insuranceId = rs.getInt("ins_id");
					String pnr = rs.getString("pnr");

					if (!inconsistentData.containsKey(pnr)) {
						inconsistentData.put(pnr, new ArrayList<Integer>());
					}
					inconsistentData.get(pnr).add(insuranceId);

				}
				return inconsistentData;
			}
		});

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Date[]> getFlightSegmentDepTimeZuluMap(Collection<Integer> pnrSegIds) {
		log.debug("getFlightSegmentDepTimeZulu called");

		String sql = " select flt_seg_id, est_time_departure_zulu, est_time_arrival_zulu "
				+ " from t_flight_segment where flt_seg_id in " + " (select flt_seg_id from t_pnr_Segment where pnr_seg_id in ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + "))";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		return (Map<Integer, Date[]>) templete.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Date[]> flightSegZuluMap = new HashMap<Integer, Date[]>();

				while (rs.next()) {

					Integer flightSegId = new Integer(rs.getInt("flt_seg_id"));

					Date flightSegmentDepartureZulu = rs.getTimestamp("est_time_departure_zulu");
					Date flightSegmentArrivalZulu = rs.getTimestamp("est_time_arrival_zulu");
					Date d[] = new Date[2];
					d[0] = flightSegmentDepartureZulu;
					d[1] = flightSegmentArrivalZulu;

					flightSegZuluMap.put(flightSegId, d);
				}
				log.debug("getFlightSegmentDepTimeZulu ended");
				return flightSegZuluMap;
			}
		});

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Date[]> getFlightSegmentDepTimeZuluMapForAddSegment(Collection<Integer> fltSegIds) {
		log.debug("getFlightSegmentDepTimeZulu called");

		String sql = " select flt_seg_id, est_time_departure_zulu, est_time_arrival_zulu "
				+ " from t_flight_segment where flt_seg_id in (" + Util.buildIntegerInClauseContent(fltSegIds) + ")";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		return (Map<Integer, Date[]>) templete.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Date[]> flightSegZuluMap = new HashMap<Integer, Date[]>();

				while (rs.next()) {

					Integer flightSegId = new Integer(rs.getInt("flt_seg_id"));

					Date flightSegmentDepartureZulu = rs.getTimestamp("est_time_departure_zulu");
					Date flightSegmentArrivalZulu = rs.getTimestamp("est_time_arrival_zulu");
					Date d[] = new Date[2];
					d[0] = flightSegmentDepartureZulu;
					d[1] = flightSegmentArrivalZulu;

					flightSegZuluMap.put(flightSegId, d);
				}
				log.debug("getFlightSegmentDepTimeZulu ended");
				return flightSegZuluMap;
			}
		});

	}

	/**
	 * Returns a List of String PnrSegmentCodes'
	 * 
	 * @param flightSegId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, String> getPnrSegmentCodes(Collection<Integer> pnrSegmentIds) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String sql = " SELECT ps.pnr_seg_id, fs.segment_code " + " FROM t_pnr_segment ps, t_flight_segment fs "
				+ " WHERE ps.flt_seg_id = fs.flt_seg_id " + " AND ps.pnr_seg_id in ("
				+ Util.constructINStringForInts(pnrSegmentIds) + ") " + " Order by ps.pnr_seg_id";

		Map<Integer, String> pnrSegIdList = null;
		pnrSegIdList = (Map<Integer, String>) template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> pnrSegIds = new HashMap<Integer, String>();

				while (rs.next()) {
					pnrSegIds.put(rs.getInt("pnr_seg_id"), rs.getString("segment_code"));
				}
				return pnrSegIds;
			}

		});

		return (pnrSegIdList);

	}

	/**
	 * Get flight manifest details
	 * 
	 * @param flightId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<PassengerCheckinDTO> getCheckinPassengers(int flightId, String airportCode, String checkinStatus,
			boolean standBy, boolean goShow) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final Map<Integer, Integer> mapPnrPaxSegIds = new HashMap<Integer, Integer>();
		// boolean isSortSegmentCode = false;

		String checkinPassengerDTOSQL = "SELECT ppfs.ppfs_id, pax.pnr_pax_id, ps.pnr, "
				+ "nvl(pax.title,'') as title, pax.first_name || ' ' || pax.last_name as PassengerName, "
				+ "pax.pax_type_code, ps.pnr_seg_id, "
				+ "fs.segment_code,  r.status as ResStatus, ppfs.PAX_STATUS as PAXStatus, "
				+ "ppfs.booking_code,bc.bc_type, bc.cabin_class_code, "
				+ "(SELECT DISTINCT TPP.first_name || ' ' || TPP.LAST_NAME "
				+ "FROM t_pnr_passenger TPP "
				+ "WHERE TPP.pnr = PS.pnr "
				+ "AND TPP.pax_type_code = 'IN' "
				+ "AND TPP.adult_id = pax.pnr_pax_id ) INFANT_PAX, "
				+ "(SELECT nvl(modelseat.seat_code,'')  as seat_code  "
				+ "         FROM "
				+ "         sm_t_pnr_pax_seg_am_seat paxseat , sm_t_flight_am_seat amseat , sm_t_am_seat_charge seatcharge, sm_t_aircraft_model_seats modelseat "
				+ "         WHERE  pax.pnr_pax_id   = paxseat.pnr_pax_id "
				+ "         AND paxseat.flight_am_seat_id = amseat.flight_am_seat_id "
				+ "         AND amseat.ams_charge_id = seatcharge.ams_charge_id "
				+ "         AND seatcharge.am_seat_id = modelseat.am_seat_id "
				+ "         AND paxseat.pnr_seg_id =   ps.pnr_seg_id " + "         AND paxseat.status = 'RES' "
				+ "     )as SEAT_CODE, " + "paxCateg.pax_category_code as paxCategory, " + "ppfs.checkin_status "
				+ "FROM T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, T_PNR_PASSENGER pax, "
				+ "T_RESERVATION r, T_PNR_PAX_FARE_SEGMENT ppfs, T_BOOKING_CLASS bc, " + "T_PNR_PAX_FARE ppf, "
				+ "T_PNR_PAX_CATEGORY paxCateg " + "WHERE ps.flt_seg_id = fs.flt_seg_id " + "AND ps.pnr = r.pnr "
				+ "AND ps.pnr = pax.pnr " + "AND ps.pnr_seg_id = ppfs.pnr_seg_id "
				+ "AND ppfs.booking_code = bc.booking_code (+) " + "AND ppfs.ppf_id = ppf.ppf_id "
				+ "AND ppf.pnr_pax_id = pax.pnr_pax_id " + "AND fs.flight_id =  " + flightId + " " + "AND fs.segment_code like '"
				+ airportCode + "%' " + "AND ps.status <> 'CNX' ";
		if (checkinStatus != null) {
			checkinPassengerDTOSQL += "AND ppfs.checkin_status = '" + checkinStatus + "' ";
		}
		if (standBy) {
			checkinPassengerDTOSQL += "AND bc.bc_type = '" + BookingClass.BookingClassType.STANDBY + "' ";
		}
		if (goShow) {
			checkinPassengerDTOSQL += "AND ppfs.PAX_STATUS = '" + ReservationInternalConstants.PaxFareSegmentTypes.GO_SHORE
					+ "' ";
		}
		checkinPassengerDTOSQL += "AND (BC.bc_type is null OR BC.bc_type <> 'CNX') " + "AND PAX.pax_type_code <> 'IN'  "
				+ "AND pax.PAX_CATEGORY_CODE = paxCateg.pax_category_code (+) " + "GROUP BY ppfs.ppfs_id, "
				+ "pax.PNR_PAX_ID,ps.pnr, " + "pax.title, " + "pax.first_name, pax.last_name, "
				+ "pax.pax_type_code, pax.ADULT_ID, " + "ps.pnr_seg_id, " + "fs.segment_code,  r.status , ppfs.PAX_STATUS, "
				+ "ppfs.booking_code, bc.bc_type, bc.cabin_class_code, " + "paxCateg.pax_category_code, r.booking_timestamp, "
				+ "ppfs.checkin_status " + "ORDER BY pnr";

		final int finalFlightID = flightId;
		ArrayList<PassengerCheckinDTO> passengerCheckinDTOs = (ArrayList<PassengerCheckinDTO>) templete.query(
				checkinPassengerDTOSQL, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<PassengerCheckinDTO> passengerCheckinDTOs = new ArrayList<PassengerCheckinDTO>();
						String passengerName;
						String paxTitle;
						String paxType;
						String paxName;
						PassengerCheckinDTO passengerCheckinDTO;

						while (rs.next()) {
							passengerCheckinDTO = new PassengerCheckinDTO();
							passengerCheckinDTO.setPnr(rs.getString("pnr"));
							paxType = BeanUtils.nullHandler(rs.getString("pax_type_code"));
							paxTitle = BeanUtils.nullHandler(rs.getString("title"));
							paxName = BeanUtils.nullHandler(rs.getString("PassengerName"));
							passengerCheckinDTO.setPassengerType(paxType);
							passengerCheckinDTO.setPassengerTitle(paxTitle);
							if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
								if (!paxTitle.equals("")) {
									passengerName = paxTitle + ". " + paxName;
								} else {
									passengerName = paxName;
								}
							} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
								passengerName = "CHD " + paxName;
							} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
								passengerName = "INF " + paxName;
							} else {
								throw new CommonsDataAccessException("airreservations.arg.unidentifiedPaxTypeDetected");
							}
							passengerCheckinDTO.setPassengerName(passengerName);
							passengerCheckinDTO.setOrigin(getPNROriginStationForFlight(rs.getString("pnr"),
									rs.getString("segment_code"), finalFlightID, true));
							passengerCheckinDTO.setSegmentCode(rs.getString("segment_code"));
							passengerCheckinDTO.setPaxCheckinStatus(rs.getString("checkin_status"));
							passengerCheckinDTO.setPassengerType(rs.getString("pax_type_code"));
							passengerCheckinDTO.setClassOfService(rs.getString("cabin_class_code"));
							passengerCheckinDTO.setBookingClass(rs.getString("booking_code"));
							passengerCheckinDTO.setBcType(rs.getString("bc_type"));
							passengerCheckinDTO.setSeatCode(rs.getString("seat_code"));
							passengerCheckinDTO.setPpfsStatus(rs.getString("PAXStatus"));
							passengerCheckinDTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
							passengerCheckinDTO.setPpfs_id(new Integer(rs.getInt("ppfs_id")));
							passengerCheckinDTO.setInfantPax(rs.getString("INFANT_PAX"));
							passengerCheckinDTO.setSsrText(getPassangersBookedSSR(finalFlightID,
									passengerCheckinDTO.getPnrPaxId()));
							passengerCheckinDTOs.add(passengerCheckinDTO);
						}
						appendSSRInfoToCheckinPassengers(passengerCheckinDTOs, mapPnrPaxSegIds);
						return passengerCheckinDTOs;
					}
				});
		return passengerCheckinDTOs;

	}

	private String getPassangersBookedSSR(Integer flightId, Integer pnrPaxId) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(ds);

		String selectedPaxSSR = "      SELECT distinct SI.SSR_CODE AS SSRCODE" + " FROM T_SSR_INFO SI, "
				+ "     T_PNR_PAX_SEGMENT_SSR ppss, " + "     T_PNR_PAX_FARE_SEGMENT ppfs, " + "     T_PNR_PAX_FARE ppf, "
				+ "     t_flight f, " + "     t_flight_segment ft, " + "     t_pnr_segment ps      "
				+ " where ppss.ssr_id = SI.ssr_id " + " and ppss.ppfs_id = ppfs.ppfs_id " + " and ppfs.ppf_id = ppf.ppf_id "
				+ " and ppfs.pnr_seg_id = ps.pnr_seg_id " + " and ps.flt_seg_id = ft.flt_seg_id "
				+ " and ft.flight_id = f.flight_id " + " and ppss.status = 'CNF' " + " and f.flight_id = " + flightId
				+ " and ppf.pnr_pax_id = " + pnrPaxId;

		String pnrPaxBookedSSRCVS = (String) jt.query(selectedPaxSSR, new ResultSetExtractor() {
			@Override
			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				String selectedPaxSSR = "";
				String selectedPaxSSrCVS = "";

				if (rs != null) {
					while (rs.next()) {
						selectedPaxSSR = BeanUtils.nullHandler(rs.getString("SSRCODE"));
						selectedPaxSSrCVS += selectedPaxSSR + ",";
					}
				}

				return (selectedPaxSSrCVS.equals("")) ? "" : selectedPaxSSrCVS.substring(0, selectedPaxSSrCVS.length() - 1);
			}
		});

		return pnrPaxBookedSSRCVS;
	}

	/**
	 * This method will append SSR Info to pnr passenger appeared in Checkin Passegners
	 * 
	 * @param passengerCheckinDTOs
	 * @param mapPnrPaxSegIds
	 */
	private void appendSSRInfoToCheckinPassengers(Collection<PassengerCheckinDTO> passengerCheckinDTOs,
			Map<Integer, Integer> mapPnrPaxSegIds) {
		if (mapPnrPaxSegIds != null && mapPnrPaxSegIds.size() > 0) {
			Map<Integer, Collection<PaxSSRDetailDTO>> pnrPaxSSRMap = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
					.getPNRPaxSSRs(mapPnrPaxSegIds, false, false, true);
			for (PassengerCheckinDTO passengerCheckinDTO : passengerCheckinDTOs) {
				Integer pnrPaxID = passengerCheckinDTO.getPnrPaxId();
				String ssrCode = passengerCheckinDTO.getSsr();
				String ssrText = passengerCheckinDTO.getSsrText();
				Collection<PaxSSRDetailDTO> paxSSRDetailDTOs = pnrPaxSSRMap.get(pnrPaxID);

				List<String> skippingSsrs = new ArrayList<String>();
				skippingSsrs.add(SSR.Code.HAJ);
				String[] ssrData = PNLUtil.getPaxSSRData(paxSSRDetailDTOs, skippingSsrs);

				if (ssrCode == null || "".equals(ssrCode)) {
					ssrCode = ssrData[0];
					ssrText = ssrData[1];
				} else {
					ssrText += ssrData[0] + " " + ssrData[1];
				}
				passengerCheckinDTO.setSsr(ssrCode);
				passengerCheckinDTO.setSsrText(ssrText);
			}
		}
	}

	/**
	 * This method will update the checkin status of the passenger
	 * 
	 * @param pnrPaxFareSegId
	 * @param checkinStatus
	 */
	@Override
	public void updatePaxCheckinStatus(ArrayList<Integer> pnrPaxFareSegId, String checkinStatus) {
		try {

			String hqlUpdate = "update ReservationPaxFareSegment set  checkinStatus=:checkinStatus where pnrPaxFareSegId in (:pnrPaxFareSegId)";

			getSession().createQuery(hqlUpdate).setParameter("checkinStatus", checkinStatus)
					.setParameterList("pnrPaxFareSegId", pnrPaxFareSegId).executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public String loadAirportMessagesTranslation(Integer airportMsgId, String selectedLang) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(dataSource);

		String sql = "  SELECT AMT.MSG_TEXT_OL TRANSLATED_MSG	" + "  FROM T_AIRPORT_MESSAGE_TRANSLATIONS AMT	"
				+ "	WHERE AMT.AIRPORT_MSG_ID = " + airportMsgId + "	AND AMT.LANGUAGE_CODE = '" + selectedLang + "'";

		String uniCodeMsg = (String) template.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String translatedAirportMsgObj = "";
				if (rs != null) {
					while (rs.next()) {
						translatedAirportMsgObj = rs.getString("TRANSLATED_MSG");
					}
				}
				return translatedAirportMsgObj;
			}
		});

		return uniCodeMsg;
	}

	@Override
	public String loadI18AirportMessagesTranslation(String messageKey, String selectedLang) {
		DataSource dataSource = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(dataSource);

		String sql = "SELECT MESSAGE_CONTENT FROM T_I18N_MESSAGE IM WHERE IM.I18N_MESSAGE_KEY = '" + messageKey
				+ "' AND IM.MESSAGE_LOCALE = '" + selectedLang + "'";

		String uniCodeMsg = (String) template.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String translatedAirportMsgObj = "";
				if (rs != null) {
					while (rs.next()) {
						translatedAirportMsgObj = rs.getString("MESSAGE_CONTENT");
					}
				}
				return translatedAirportMsgObj;
			}
		});

		return uniCodeMsg;
	}

	public void createMessagesList(List<AirportMessageDisplayDTO> apMsgList, AirportMessageDisplayDTO airportMessageDisplayDTO) {

		boolean msgAlreadyInList = false;
		for (AirportMessageDisplayDTO messageDisplayDTO : apMsgList) {
			if (messageDisplayDTO.getAirportMsgId().equals(airportMessageDisplayDTO.getAirportMsgId())) {
				msgAlreadyInList = true;
				return;
			}
		}

		if (!msgAlreadyInList) {
			apMsgList.add(airportMessageDisplayDTO);
		}
	}

	@Override
	public ArrayList<MealNotificationDetailsDTO> getFlightsForMealNotificaitionScheduling(int maxMealNotifyTime, Date date,
			List<String> hubs) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Timestamp dateLowerLimit = new Timestamp(date.getTime());
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 + (maxMealNotifyTime / 60)) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y" };
		final ArrayList<MealNotificationDetailsDTO> mealNotificationFlightList = new ArrayList<MealNotificationDetailsDTO>();

		String sql = null;
		if (hubs.size() == 0) {

			sql = " select distinct flight.FLIGHT_NUMBER,flightseg.FLIGHT_ID,flightSeg.EST_TIME_DEPARTURE_LOCAL,flightseg.EST_TIME_DEPARTURE_ZULU,substr(flightseg.segment_code,0,3) as ori,flightseg.FLT_SEG_ID from T_FLIGHT  flight,T_FLIGHT_SEGMENT"
					+ " flightseg  where flightseg.FLIGHT_ID=flight.FLIGHT_ID and flightseg.EST_TIME_DEPARTURE_ZULU<=? AND flightseg.EST_TIME_DEPARTURE_ZULU>? AND (flight.STATUS='ACT' OR flight.STATUS='CLS') and flightseg.SEGMENT_VALID_FLAG=? order by flightseg.flight_id,flightseg.EST_TIME_DEPARTURE_ZULU";
		} else {
			// Take only out bounds from enabled hubs
			sql = " select distinct flight.FLIGHT_NUMBER,flightseg.FLIGHT_ID,flightSeg.EST_TIME_DEPARTURE_LOCAL,flightseg.EST_TIME_DEPARTURE_ZULU,substr(flightseg.segment_code,0,3) as ori,flightseg.FLT_SEG_ID from T_FLIGHT  flight,T_FLIGHT_SEGMENT"
					+ " flightseg  where flightseg.FLIGHT_ID=flight.FLIGHT_ID and flightseg.EST_TIME_DEPARTURE_ZULU<=? AND flightseg.EST_TIME_DEPARTURE_ZULU>? AND (flight.STATUS='ACT' OR flight.STATUS='CLS') and flightseg.SEGMENT_VALID_FLAG=? AND flight.origin IN ("
					+ Util.buildStringInClauseContent(hubs) + ") order by flightseg.flight_id,flightseg.EST_TIME_DEPARTURE_ZULU";
		}

		templete.query(sql, params,

		new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {

					MealNotificationDetailsDTO mealNotificationDTO = new MealNotificationDetailsDTO();

					int flightID = rs.getInt("FLIGHT_ID");

					Timestamp timeStampzulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");

					Timestamp timeStampLocal = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");

					String departureStation = rs.getString("ori");

					int flightSegmentId = rs.getInt("FLT_SEG_ID");

					String flightNumber = rs.getString("FLIGHT_NUMBER");

					mealNotificationDTO.setDepartureStation(departureStation);
					mealNotificationDTO.setDepartureTimeZulu(timeStampzulu);
					mealNotificationDTO.setFlightId(flightID);
					mealNotificationDTO.setFlightSegId(flightSegmentId);
					mealNotificationDTO.setDeparturetimeLocal(timeStampLocal);
					mealNotificationDTO.setFlightNumber(flightNumber);

					mealNotificationFlightList.add(mealNotificationDTO);

				}

				return mealNotificationFlightList;
			}

		}

		);

		return mealNotificationFlightList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getMealNotificationTimings() {
		// Fetch from meal notification timing table
		Collection<String> colNotifyTimings = new ArrayList<String>();
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String sql = " SELECT MEAL_NOTIFY_TIMING_ID, AIRPORT_CODE, FLIGHT_NUMBER, "
				+ "NOTIFY_START, NOTIFY_GAP, LAST_NOTIFY, APPLY_TO_ALL, " + "ZULU_START_DATE, ZULU_END_DATE, STATUS "
				+ "FROM ML_T_MEAL_NOTIFY_TIMINGS A " + "WHERE " + "A.STATUS = 'ACT' ";
		// + "AND  A.ZULU_START_DATE < SYSDATE AND A.ZULU_END_DATE > SYSDATE "; // TODO: Additional flexibility
		// supported, if table will be updated with valid dates this can be uncomment and used

		colNotifyTimings = (Collection<String>) template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> colNotifyTimings = new ArrayList<String>();
				while (rs.next()) {
					colNotifyTimings.add(rs.getString("NOTIFY_START"));
				}
				return colNotifyTimings;
			}

		});

		return colNotifyTimings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<MealNotificationDetailsDTO> getMealNotificationDetails(boolean isLoadFlightLevel) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String sql = " SELECT MEAL_NOTIFY_TIMING_ID, AIRPORT_CODE, FLIGHT_NUMBER, "
				+ "NOTIFY_START, NOTIFY_GAP, LAST_NOTIFY, APPLY_TO_ALL, " + "ZULU_START_DATE, ZULU_END_DATE, STATUS "
				+ "FROM ML_T_MEAL_NOTIFY_TIMINGS A " + "WHERE  A.STATUS = 'ACT'";
		// + " AND A.ZULU_START_DATE < SYSDATE AND A.ZULU_END_DATE > SYSDATE "; // TODO: Additional flexibility
		// supported, if table will be updated with valid dates this can be uncomment and used
		if (isLoadFlightLevel) {
			sql += "AND A.FLIGHT_NUMBER IS NOT NULL";
		} else {
			sql += "AND A.FLIGHT_NUMBER IS NULL";
		}

		Collection<MealNotificationDetailsDTO> mealNotificationDetails = null;
		mealNotificationDetails = (Collection<MealNotificationDetailsDTO>) template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<MealNotificationDetailsDTO> mealNotificationDetails = new ArrayList<MealNotificationDetailsDTO>();
				while (rs.next()) {
					String notifyStartTime = rs.getString("NOTIFY_START");
					String lastNotifyTime = rs.getString("LAST_NOTIFY");
					MealNotificationDetailsDTO mealNotificationDetailsDTO = new MealNotificationDetailsDTO();
					mealNotificationDetailsDTO.setDepartureStation(rs.getString("AIRPORT_CODE"));
					mealNotificationDetailsDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
					mealNotificationDetailsDTO.setNotifyStartTime(Util.getTotalMinutes(notifyStartTime));
					mealNotificationDetailsDTO.setNotifyFrequency(rs.getInt("NOTIFY_GAP"));
					mealNotificationDetailsDTO.setLastNotification(Util.getTotalMinutes(lastNotifyTime));
					mealNotificationDetails.add(mealNotificationDetailsDTO);
				}
				return mealNotificationDetails;
			}

		});
		return mealNotificationDetails;

	}

	private String getBookingTypes() {
		String sqlBookingType;
		Collection<String> bcTypes = new ArrayList<String>();
		if (AppSysParamsUtil.isIncludeStandbyPaxInPNLAndADL()) {
			bcTypes.addAll(BookingClassUtil.getBcTypes(false));
			bcTypes.addAll(BookingClassUtil.getBcTypes(true));
			sqlBookingType = " and vseg.bc_type IN (" + Util.buildStringInClauseContent(bcTypes) + ") ";
		} else {
			bcTypes.addAll(BookingClassUtil.getBcTypes(false));
			sqlBookingType = " and vseg.bc_type IN (" + Util.buildStringInClauseContent(bcTypes) + ") ";
		}
		return sqlBookingType;
	}

	@Override
	public boolean hasFlownSegments(Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Check from T_PNR_PAX_FARE_SEGMENT - done via PFS processsing
		String strFlgSegmentSql = "  SELECT count(DISTINCT(ppfs.pnr_seg_id)) as flown_seg_count"
				+ " FROM t_pnr_pax_fare_segment ppfs  WHERE ppfs.pax_status   = ?  AND ppfs.pnr_seg_id IN ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ") ";

		Integer flownSegments = jt.queryForObject(strFlgSegmentSql,
				new Object[] { ReservationInternalConstants.PaxFareSegmentTypes.FLOWN }, Integer.class);

		if (flownSegments > 0) {
			return true;
		}

		// Check from T_PNR_PAX_FARE_SEGMENT - done via either PFS processsing or ETL processing
		strFlgSegmentSql = " SELECT count(DISTINCT(ppfs.pnr_seg_id)) as flown_seg_count "
				+ "  FROM t_pnr_pax_fare_segment ppfs, t_pnr_pax_fare_seg_e_ticket ppfset"
				+ "  WHERE ppfset.status   =  ? AND ppfset.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id IN ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ") ";

		flownSegments = jt.queryForObject(strFlgSegmentSql, new Object[] { EticketStatus.FLOWN.code() }, Integer.class);
		if (flownSegments > 0) {
			return true;
		}
		
		if(AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()){
			strFlgSegmentSql = " SELECT count(DISTINCT(ppfs.pnr_seg_id)) as flown_seg_count "
					+ "  FROM t_pnr_pax_fare_segment ppfs, t_pnr_pax_fare_seg_e_ticket ppfset"
					+ "  WHERE (ppfset.status   =  ? OR ppfset.status   =  ?)  AND ppfset.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id IN ("
					+ Util.buildIntegerInClauseContent(pnrSegIds) + ") ";
	
			flownSegments = jt.queryForObject(strFlgSegmentSql, new Object[] { EticketStatus.CHECKEDIN.code(),  EticketStatus.BOARDED.code()  }, Integer.class);
			if (flownSegments > 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Map<Integer, String> getFltSegWiseBkgClasses(Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String strFlgSegmentSql = "SELECT *   FROM  (SELECT ps.pnr_seg_id, ps.flt_seg_id, ppfs.booking_code ,"
				+ " rank() over (partition BY ps.flt_seg_id ORDER by ppfs.ppf_id ) rnk "
				+ " FROM t_pnr_segment ps,    t_pnr_pax_fare_segment ppfs    WHERE ps.pnr_seg_id IN ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ")  "
				+ " AND ppfs.pnr_seg_id    = ps.pnr_seg_id  AND ppfs.booking_code IS NOT NULL  "
				+ " AND ppfs.booking_code NOT LIKE 'OPENRT%'  )  WHERE rnk = 1  ";

		@SuppressWarnings("unchecked")
		Map<Integer, String> fltSegWiseBkgClass = (Map<Integer, String>) jt.query(strFlgSegmentSql, new Object[] {},
				new ResultSetExtractor() {
					Map<Integer, String> fltSegWiseBkgClass = new HashMap<Integer, String>();

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							Integer fltSegId = rs.getInt("flt_seg_id");
							String bookingCode = rs.getString("booking_code");
							if (bookingCode != null) {
								fltSegWiseBkgClass.put(fltSegId, bookingCode);
							}
						}
						return fltSegWiseBkgClass;
					}

				});
		return fltSegWiseBkgClass;
	}

	@Override
	public Map<Integer, String> getFltSegWiseCabinClasses(Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String strFlgSegmentSql = "SELECT *   FROM  (SELECT ps.pnr_seg_id, ps.flt_seg_id, ppfs.booking_code ,"
				+ " bc.cabin_class_code ,rank() over (partition BY ps.flt_seg_id ORDER by ppfs.ppf_id ) rnk "
				+ " FROM t_pnr_segment ps,    t_pnr_pax_fare_segment ppfs , t_booking_class bc     WHERE ps.pnr_seg_id IN ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ")  "
				+ " AND ppfs.pnr_seg_id    = ps.pnr_seg_id  AND ppfs.booking_code IS NOT NULL  "
				+ " AND bc.booking_code = ppfs.booking_code AND ppfs.booking_code NOT LIKE 'OPENRT%'  )  WHERE rnk = 1  ";

		@SuppressWarnings("unchecked")
		Map<Integer, String> fltSegWiseBkgClass = (Map<Integer, String>) jt.query(strFlgSegmentSql, new Object[] {},
				new ResultSetExtractor() {
					Map<Integer, String> fltSegWiseBkgClass = new HashMap<Integer, String>();

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							Integer fltSegId = rs.getInt("flt_seg_id");
							String cabinCode = rs.getString("cabin_class_code");
							if (cabinCode != null) {
								fltSegWiseBkgClass.put(fltSegId, cabinCode);
							}
						}
						return fltSegWiseBkgClass;
					}

				});
		return fltSegWiseBkgClass;
	}

	@Override
	public Map<Integer, Date> getFltSegWiseLastFQDates(List<Integer> flownPnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String strFlgSegmentSql = " SELECT pnr_seg_id, flt_seg_id, last_fq_date, ticket_valid_till, status"
				+ "   FROM t_pnr_segment  WHERE pnr_seg_id  IN (" + Util.buildIntegerInClauseContent(flownPnrSegIds) + ")  ";

		@SuppressWarnings("unchecked")
		Map<Integer, Date> fltSegWiseLastFQDates = (Map<Integer, Date>) jt.query(strFlgSegmentSql, new Object[] {},
				new ResultSetExtractor() {
					Map<Integer, Date> fltSegWiseLastFQDates = new HashMap<Integer, Date>();

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							Integer fltSegId = rs.getInt("flt_seg_id");
							Date latFQDate = rs.getTimestamp("last_fq_date");
							fltSegWiseLastFQDates.put(fltSegId, latFQDate);
						}
						return fltSegWiseLastFQDates;
					}

				});
		return fltSegWiseLastFQDates;
	}

	@Override
	public Map<Integer, Collection<Integer>> getPaxWiseFlownSegments(Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		// Check from T_PNR_PAX_FARE_SEGMENT - done via PFS processsing
		if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.PFS) {
			String strFlgSegmentSql = "  SELECT ppfs.pnr_seg_id, ppf.pnr_pax_id, ppfs.pax_status  "
					+ " FROM t_pnr_pax_fare_segment ppfs, t_pnr_pax_fare ppf   WHERE  "
					+ "  ppf.ppf_id = ppfs.ppf_id  AND ppfs.pnr_seg_id IN (" + Util.buildIntegerInClauseContent(pnrSegIds) + ") ";

			@SuppressWarnings("unchecked")
			Map<Integer, Collection<Integer>> paxFlownPnrSegMap = (Map<Integer, Collection<Integer>>) jt.query(strFlgSegmentSql,
					new Object[] {}, new ResultSetExtractor() {
						Map<Integer, Collection<Integer>> tmpPaxFlownPnrSegMap = new HashMap<Integer, Collection<Integer>>();

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							while (rs.next()) {
								Integer pnrPaxId = rs.getInt("pnr_pax_id");
								Integer pnrSegId = rs.getInt("pnr_seg_id");
								String paxStatus = rs.getString("pax_status");
								if (!tmpPaxFlownPnrSegMap.containsKey(pnrPaxId)) {
									tmpPaxFlownPnrSegMap.put(pnrPaxId, new ArrayList<Integer>());
								}
								if (ReservationInternalConstants.PaxFareSegmentTypes.FLOWN.equals(paxStatus)
										|| ReservationInternalConstants.PaxFareSegmentTypes.NO_REC.equals(paxStatus)) {
									tmpPaxFlownPnrSegMap.get(pnrPaxId).add(pnrSegId);
								}
							}
							return tmpPaxFlownPnrSegMap;
						}

					});
			return paxFlownPnrSegMap;
		} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET) {

			String strFlgSegmentSql = " SELECT ppfs.pnr_seg_id , ppf.pnr_pax_id, ppfset.status "
					+ " FROM t_pnr_pax_fare_segment ppfs,  t_pnr_pax_fare_seg_e_ticket ppfset, t_pnr_pax_fare ppf	"
					+ " WHERE ppf.ppf_id = ppfs.ppf_id AND " + " ppfset.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id IN ("
					+ Util.buildIntegerInClauseContent(pnrSegIds) + ") ";

			@SuppressWarnings("unchecked")
			Map<Integer, Collection<Integer>> paxFlownPnrSegMap = (Map<Integer, Collection<Integer>>) jt.query(strFlgSegmentSql,
					new Object[] {}, new ResultSetExtractor() {
						Map<Integer, Collection<Integer>> paxFlownPnrSegMap = new HashMap<Integer, Collection<Integer>>();

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							while (rs.next()) {
								Integer pnrPaxId = rs.getInt("pnr_pax_id");
								Integer pnrSegId = rs.getInt("pnr_seg_id");
								String paxStatus = rs.getString("status");
								if (!paxFlownPnrSegMap.containsKey(pnrPaxId)) {
									paxFlownPnrSegMap.put(pnrPaxId, new ArrayList<Integer>());
								}
								if (EticketStatus.FLOWN.code().equals(paxStatus)) {
									paxFlownPnrSegMap.get(pnrPaxId).add(pnrSegId);
								}
							}
							return paxFlownPnrSegMap;
						}

					});
			return paxFlownPnrSegMap;
		}
		return null;

	}

	private static String select = "SELECT F.FLIGHT_ID, F.FLIGHT_NUMBER, FL.FLIGHT_LEG_ID, FL.ORIGIN, FL.DESTINATION, F.DEPARTURE_DATE, FL.EST_TIME_DEPARTURE_ZULU, FL.EST_TIME_ARRIVAL_ZULU ";
	private static String select_notification_id_7 = ", 168 AS NOTIFICATION_ID ";
	private static String select_notification_id_6 = ", 144 AS NOTIFICATION_ID ";
	private static String select_notification_id_5 = ", 120 AS NOTIFICATION_ID ";
	private static String select_notification_id_4 = ", 96 AS NOTIFICATION_ID ";
	private static String select_notification_id_3 = ", 72 AS NOTIFICATION_ID ";
	private static String select_notification_id_2 = ", 48 AS NOTIFICATION_ID ";
	private static String select_notification_id_1 = ", 24 AS NOTIFICATION_ID ";
	private static String select_notification_id_75 = ", 18 AS NOTIFICATION_ID ";
	private static String select_notification_id_50 = ", 12 AS NOTIFICATION_ID ";
	private static String select_notification_id_25 = ", 6 AS NOTIFICATION_ID ";
	private static String from = "FROM T_FLIGHT F, T_FLIGHT_LEG FL ";
	private static String where_1 = "WHERE  F.STATUS in('ACT', 'CLS', 'CLR') AND (F.DEPARTURE_DATE BETWEEN ? AND ?) AND F.FLIGHT_ID = FL.FLIGHT_ID ";
	private static String where_2_7 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID) ";
	private static String where_2_6 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 144) ";
	private static String where_2_5 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 120) ";
	private static String where_2_4 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 96) ";
	private static String where_2_3 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 72) ";
	private static String where_2_2 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 48) ";
	private static String where_2_1 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 24) ";
	private static String where_2_75 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 18) ";
	private static String where_2_50 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 12) ";
	private static String where_2_25 = "AND NOT EXISTS (SELECT 1 FROM T_FLIGHT_LOAD FLO, T_FLIGHT_LEG_LOAD FLLO WHERE F.FLIGHT_ID = FLO.FLIGHT_ID AND FL.FLIGHT_LEG_ID  = FLLO.FLIGHT_LEG_ID AND FLO.NOTIFICATION_ID = 6) ";
	private static String union_all = "UNION ALL ";

	@Override
	public List<FlightLoad> getFlightLegLoadInfoScheduledForSending() {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		StringBuilder scheduledFlights = new StringBuilder();
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_25);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_25);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_50);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_50);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_75);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_75);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_1);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_1);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_2);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_2);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_3);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_3);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_4);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_4);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_5);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_5);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_6);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_6);
		scheduledFlights.append(union_all);
		scheduledFlights.append(select);
		scheduledFlights.append(select_notification_id_7);
		scheduledFlights.append(from);
		scheduledFlights.append(where_1);
		scheduledFlights.append(where_2_7);

		if (log.isDebugEnabled()) {
			log.debug("SQL :: " + scheduledFlights.toString());
		}

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("zulu"));
		Date now = cal.getTime();
		cal.add(Calendar.DATE, 7);
		Date seven = cal.getTime();
		cal.add(Calendar.DATE, -1);
		Date six = cal.getTime();
		cal.add(Calendar.DATE, -1);
		Date five = cal.getTime();
		cal.add(Calendar.DATE, -1);
		Date four = cal.getTime();
		cal.add(Calendar.DATE, -1);
		Date three = cal.getTime();
		cal.add(Calendar.DATE, -1);
		Date two = cal.getTime();
		cal.add(Calendar.DATE, -1);
		Date one = cal.getTime();
		cal.add(Calendar.HOUR, -6);
		Date threeQuarter = cal.getTime();
		cal.add(Calendar.HOUR, -6);
		Date half = cal.getTime();
		cal.add(Calendar.HOUR, -6);
		Date quarter = cal.getTime();

		Object[] objs = { now, quarter, quarter, half, half, threeQuarter, threeQuarter, one, one, two, two, three, three, four,
				four, five, five, six, six, seven };

		@SuppressWarnings("unchecked")
		List<ScheduledFlightInfo> scheduledFlightInfos = (List<ScheduledFlightInfo>) templete.query(scheduledFlights.toString(),
				objs, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<ScheduledFlightInfo> infos = new ArrayList<ScheduledFlightInfo>();
						while (rs.next()) {
							ScheduledFlightInfo info = new ScheduledFlightInfo(rs.getInt("FLIGHT_ID"),
									rs.getInt("FLIGHT_LEG_ID"), rs.getString("FLIGHT_NUMBER"), rs.getString("ORIGIN"), rs
											.getString("DESTINATION"), rs.getTime("EST_TIME_DEPARTURE_ZULU"), rs
											.getTime("EST_TIME_ARRIVAL_ZULU"), rs.getDate("DEPARTURE_DATE"), rs
											.getInt("NOTIFICATION_ID"));
							infos.add(info);
						}
						return infos;
					}

				});

		Map<Integer, FlightLoad> legLoadInformations = new HashMap<Integer, FlightLoad>();

		for (ScheduledFlightInfo scheduledFlightInfo : scheduledFlightInfos) {
			String segmentCode = "%" + scheduledFlightInfo.getOrigin() + "%" + scheduledFlightInfo.getDestination() + "%";
			FlightLegPaxTypeCountTO paxCount = getPaxCountForFlightLeg(scheduledFlightInfo.getFlightId(), segmentCode);
			double baggageLoad = getBaggageWeightForFlightLeg(scheduledFlightInfo.getFlightId(), segmentCode);

			int maleCount = (paxCount.getConfirmedPaxSummary() == null ? 0 : paxCount.getConfirmedPaxSummary()
					.getAdultMaleCount())
					+ (paxCount.getOnholdPaxSummary() == null ? 0 : paxCount.getOnholdPaxSummary().getAdultMaleCount());
			int femaleCount = (paxCount.getConfirmedPaxSummary() == null ? 0 : paxCount.getConfirmedPaxSummary()
					.getAdultFemaleCount())
					+ (paxCount.getOnholdPaxSummary() == null ? 0 : paxCount.getOnholdPaxSummary().getAdultFemaleCount());
			int childCount = (paxCount.getConfirmedPaxSummary() == null ? 0 : paxCount.getConfirmedPaxSummary().getChildCount())
					+ (paxCount.getOnholdPaxSummary() == null ? 0 : paxCount.getOnholdPaxSummary().getChildCount());
			int infantCount = (paxCount.getConfirmedPaxSummary() == null ? 0 : paxCount.getConfirmedPaxSummary().getInfantCount())
					+ (paxCount.getOnholdPaxSummary() == null ? 0 : paxCount.getOnholdPaxSummary().getInfantCount());

			FlightLoad dto = null;
			if (legLoadInformations.containsKey(scheduledFlightInfo.getFlightId())) {
				dto = legLoadInformations.get(scheduledFlightInfo.getFlightId());
			} else {
				dto = new FlightLoad(scheduledFlightInfo.getFlightId(), scheduledFlightInfo.getFlightNumber(),
						scheduledFlightInfo.getDepartureDate(), scheduledFlightInfo.getNotificationId());
			}
			dto.addLegLoadInformation(scheduledFlightInfo.getFlightLegId(), scheduledFlightInfo.getOrigin(),
					scheduledFlightInfo.getDestination(), scheduledFlightInfo.getLegDepartureTimeZulu(),
					scheduledFlightInfo.getLegArrivalTimeZulu(), maleCount, femaleCount, childCount, infantCount, baggageLoad);
			legLoadInformations.put(scheduledFlightInfo.getFlightId(), dto);

		}

		return new ArrayList<FlightLoad>(legLoadInformations.values());
	}

	private double getBaggageWeightForFlightLeg(int flightId, String segmentCode) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT SUM(BG.BAGGAGE_WEIGHT) AS TOTALSUM ");
		sqlBuilder.append("FROM T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT PS, BG_T_PNR_PAX_SEG_BAGGAGE PB, BG_T_BAGGAGE BG ");
		sqlBuilder.append("WHERE PS.FLT_SEG_ID = FS.FLT_SEG_ID AND ");
		sqlBuilder.append("FS.FLIGHT_ID = ");
		sqlBuilder.append(flightId);
		sqlBuilder.append(" AND FS.SEGMENT_CODE like UPPER('");
		sqlBuilder.append(segmentCode);
		sqlBuilder.append("') AND PS.STATUS ='CNF' ");
		sqlBuilder.append("AND PS.PNR_SEG_ID = PB.PNR_SEG_ID ");
		sqlBuilder.append("AND PB.BAGGAGE_ID = BG.BAGGAGE_ID ");

		Double baggageWeight = (Double) templete.query(sqlBuilder.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Double weight = 0d;
				while (rs.next()) {
					double sum = rs.getDouble("totalSum");
					weight += sum;
				}

				return weight;
			}
		});

		return baggageWeight;
	}

	private class ScheduledFlightInfo {
		private int flightId;
		private int flightLegId;
		private String flightNumber;
		private String origin;
		private String destination;
		private Date legDepartureTimeZulu;
		private Date legArrivalTimeZulu;
		private Date departureDate;
		private int notificationId;

		public ScheduledFlightInfo(int flightId, int flightLegId, String flightNumber, String origin, String destination,
				Date legDepartureTimeZulu, Date legArrivalTimeZulu, Date departureDate, int notificationId) {
			this.flightId = flightId;
			this.flightLegId = flightLegId;
			this.flightNumber = flightNumber;
			this.origin = origin;
			this.destination = destination;
			this.legDepartureTimeZulu = legDepartureTimeZulu;
			this.legArrivalTimeZulu = legArrivalTimeZulu;
			this.departureDate = departureDate;
			this.notificationId = notificationId;
		}

		public int getFlightId() {
			return flightId;
		}

		public int getFlightLegId() {
			return flightLegId;
		}

		public String getFlightNumber() {
			return flightNumber;
		}

		public String getOrigin() {
			return origin;
		}

		public String getDestination() {
			return destination;
		}

		public Date getLegDepartureTimeZulu() {
			return legDepartureTimeZulu;
		}

		public Date getLegArrivalTimeZulu() {
			return legArrivalTimeZulu;
		}

		public Date getDepartureDate() {
			return departureDate;
		}

		public int getNotificationId() {
			return notificationId;
		}

	}

	@Override
	public void saveFlightLoad(Collection<FlightLoad> flightLoads) {
		if (log.isDebugEnabled()) {
			log.debug("Saving following flight load details : " + flightLoads);
		}
		hibernateSaveOrUpdateAll(flightLoads);
		if (log.isDebugEnabled()) {
			log.debug("Saving flight load details completed.");
		}
	}

	private FlightLegPaxTypeCountTO getPaxCountForFlightLeg(int flightId, String segmentCode) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT COUNT(PP.PNR_PAX_ID) AS TOTALCOUNT, PT.GENDER, PP.PAX_TYPE_CODE, PP.STATUS ");
		sqlBuilder.append("FROM T_PNR_PASSENGER PP, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT PS,T_PAX_TITLE PT ");
		sqlBuilder.append("WHERE PS.FLT_SEG_ID=FS.FLT_SEG_ID AND ");
		sqlBuilder.append("FS.FLIGHT_ID=");
		sqlBuilder.append(flightId);
		sqlBuilder.append(" AND FS.SEGMENT_CODE like UPPER('");
		sqlBuilder.append(segmentCode);
		sqlBuilder.append("') AND PS.PNR=PP.PNR ");
		sqlBuilder.append("AND PP.TITLE=PT.TITLE_CODE(+) ");
		sqlBuilder.append("AND PS.STATUS ='CNF' ");
		sqlBuilder.append("GROUP BY PT.GENDER,PP.PAX_TYPE_CODE,PP.STATUS");

		FlightLegPaxTypeCountTO paxCounts = (FlightLegPaxTypeCountTO) templete.query(sqlBuilder.toString(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						FlightLegPaxTypeCountTO flightLegPaxTypeCountTO = new FlightLegPaxTypeCountTO();

						PaxTypeCountTO countTO = null;
						while (rs.next()) {
							int countValue = rs.getInt("totalCount");
							String strPaxType = rs.getString("pax_type_code");
							String strGender = rs.getString("gender");
							String bookingStatus = rs.getString("status");

							if (bookingStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
								if (flightLegPaxTypeCountTO.getConfirmedPaxSummary() == null) {
									countTO = new PaxTypeCountTO();
								} else {
									countTO = flightLegPaxTypeCountTO.getConfirmedPaxSummary();
								}
							} else {
								if (flightLegPaxTypeCountTO.getOnholdPaxSummary() == null) {
									countTO = new PaxTypeCountTO();
								} else {
									countTO = flightLegPaxTypeCountTO.getOnholdPaxSummary();
								}
							}

							if (strPaxType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
								if (strGender != null && strGender.equals(ReservationInternalConstants.Gender.MALE)) {
									countTO.setAdultMaleCount(countTO.getAdultMaleCount() + countValue);
								} else if (strGender != null && strGender.equals(ReservationInternalConstants.Gender.FEMALE)) {
									countTO.setAdultFemaleCount(countTO.getAdultFemaleCount() + countValue);
								} else {
									countTO.setOthersCount(countTO.getOthersCount() + countValue);
								}
							} else if (strPaxType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
								countTO.setChildCount(countTO.getChildCount() + countValue);
							} else {
								countTO.setInfantCount(countTO.getInfantCount() + countValue);
							}

							if (bookingStatus.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
								flightLegPaxTypeCountTO.setConfirmedPaxSummary(countTO);
							} else {
								flightLegPaxTypeCountTO.setOnholdPaxSummary(countTO);
							}

						}

						return flightLegPaxTypeCountTO;
					}
				});

		return paxCounts;
	}

	@Override
	public List<PaxEticketTO> getPaxEtickets(int flightId, String stationCode) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String eticketIdsQuery = " SELECT ppfset.pnr_pax_fare_seg_e_ticket_id, pp.pnr , pp.pnr_pax_id , pp.first_name , "
				+ " pp.last_name FROM t_pnr_pax_fare_segment ppfs , t_pnr_pax_fare ppf , t_pnr_pax_fare_seg_e_ticket ppfset, "
				+ " t_pnr_passenger pp,  t_pnr_segment ps, t_flight_segment fs , t_flight f "
				+ " WHERE ppfs.ppfs_id = ppfset.ppfs_id AND ppfs.ppf_id = ppf.ppf_id AND ppf.pnr_pax_id = pp.pnr_pax_id "
				+ " AND ( ppfs.pnr_seg_id = ps.pnr_seg_id ";
		if (AppSysParamsUtil.isAutoUpdateGroundSegStateWhenAirSegFlown()) {
			eticketIdsQuery += " OR (ps.ground_seg_id IS NOT NULL AND ppfs.pnr_seg_id = ps.ground_seg_id) ";
		}
		eticketIdsQuery += " ) AND ps.status <> '" + ReservationInternalConstants.ReservationSegmentStatus.CANCEL
				+ "' AND f.flight_id = ? AND SUBSTR( fs.segment_code, 0,3) = ? "
				+ " AND fs.flight_id = f.flight_id AND fs.flt_seg_id = ps.flt_seg_id ";

		@SuppressWarnings("unchecked")
		List<PaxEticketTO> paxEtickets = (List<PaxEticketTO>) jt.query(eticketIdsQuery, new Object[] { flightId, stationCode },
				new ResultSetExtractor() {
					List<PaxEticketTO> paxEtickets = new ArrayList<PaxEticketTO>();

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							PaxEticketTO paxEticket = new PaxEticketTO();
							paxEticket.setEticketId(rs.getInt("pnr_pax_fare_seg_e_ticket_id"));
							paxEticket.setName(rs.getString("first_name") + " " + rs.getString("last_name"));
							paxEticket.setPnr(rs.getString("pnr"));
							paxEticket.setPnrPaxId(rs.getInt("pnr_pax_id"));
							paxEtickets.add(paxEticket);
						}
						return paxEtickets;
					}

				});
		return paxEtickets;
	}
	
	@Override
	public List<PnlPassenger> getSegmentTransferredPnlPassegers(Set<String> pnrs, List<String> adlActions,
			Collection<Integer> pnrSegIds) {
		String hql = "select pnlPax from PnlPassenger as pnlPax " + " where pnlPax.pnr in ("
				+ Util.buildStringInClauseContent(pnrs) + ") " + " and pnlPax.status= 'N' and pnlPax.pnlStatus in( "
				+ Util.buildStringInClauseContent(adlActions) + ")" + " AND pnlPax.pnrSegId not in( "
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ")";

		return find(hql, PnlPassenger.class);
	}
	
	@Override
	public List<PnlPassenger> getNonSegmentTransferredPnlPassegers(Set<String> pnrs, List<String> adlActions,
			Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT pnlPax.* FROM " + " T_PNL_PASSENGER pnlPax, T_FLIGHT_SEGMENT fs, T_PNR_SEGMENT ps "
				+ " WHERE pnlPax.pnr          IN (" + Util.buildStringInClauseContent(pnrs) + ") "
				+ " AND pnlPax.status          = 'N' " + " AND pnlPax.pnl_Stat       IN("
				+ Util.buildStringInClauseContent(adlActions) + ") " + " AND ps.FLT_SEG_ID          = fs.FLT_SEG_ID "
				+ " AND pnlPax.PNR_SEG_ID      = ps.PNR_SEG_ID " + " AND pnlPax.PNR_SEG_ID NOT IN ("
				+ Util.buildIntegerInClauseContent(pnrSegIds) + ") " + " AND fs.FLT_SEG_ID         IN "
				+ " (SELECT fs.FLT_SEG_ID " + " FROM T_FLIGHT_SEGMENT fs, T_PNR_SEGMENT ps "
				+ " WHERE ps.FLT_SEG_ID = fs.FLT_SEG_ID AND ps.PNR_SEG_ID  IN(" + Util.buildIntegerInClauseContent(pnrSegIds)
				+ "))";

		@SuppressWarnings("unchecked")
		List<PnlPassenger> pnlPassengers = (List<PnlPassenger>) jt.query(sql, new ResultSetExtractor() {
			List<PnlPassenger> pnlPassengers = new ArrayList<PnlPassenger>();

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					PnlPassenger pax = new PnlPassenger();
					pax.setPnrPaxId(rs.getInt("pnr_pax_id"));
					pnlPassengers.add(pax);
				}
				return pnlPassengers;
			}

		});
		return pnlPassengers;
	}

	@Override
	public boolean hasPalSentHistory(String flightNumber, Date flightDepZulu, String airportCode) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String depZulu = dateFormat.format(flightDepZulu);

		String sql = " SELECT count(a.flight_id) " 
				+ " FROM "
				+ " t_flight_segment a , t_flight b, t_pal_cal_tx_history his "
				+ " where " 
				+ " a.flight_id = his.flight_id(+) and a.flight_id = b.flight_id" 
				+ " and trunc(a.est_time_departure_zulu) ='" + depZulu + "'" 
				+ " and b.status != 'CNX' "
				+ " and trim(flight_number) = '" + flightNumber.trim() + "'" 
				+ " and substr(a.segment_code,1,3)='" + airportCode+ "'" 
				+ " and his.message_type='" + PalConstants.MessageTypes.PAL + "' " 
				+ " and his.SENT_METHOD='"
				+ PalConstants.SendingMethod.SCHEDSERVICE + "'";

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Object intObj = jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));
				}
				return count;
			}
		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}
		return numOfRecs > 0;
	}

	@Override
	public void savePALCALTXHistory(PalCalHistory palCalHistory) {
		hibernateSaveOrUpdate(palCalHistory);		
	}

	@Override
	public List<PassengerInformation> getPALPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode) {
		String sqlBookingType = getBookingTypes();
		Object[] adlParams = { new Integer(flightId), departureAirportCode,	scheduledTimestamp };
		final List<Integer> pnrPaxIds = new ArrayList<Integer>();
		
		String adlQuery = "SELECT DISTINCT vpnr.pnr_status AS vp , "
						  +" vseg.pnr_seg_id , "
						  +" tr.status     AS ress , "
						  +" vseg.cal_stat AS ps , "
						  +" vseg.booking_code , "
						  +" vseg.bc_type , "
						  +" vseg.cabin_class_code , "
						  +" vseg.logical_cabin_class_code , "
						  +" vpnr.SEGMENT_SEQ , "
						  +" SUBSTR(vpnr.SEGMENT_CODE,LENGTH(vpnr.SEGMENT_CODE)-2,LENGTH(vpnr.SEGMENT_CODE)) AS destination , "
						 +"  pas.FIRST_NAME                                                                  AS first_name , "
						  +" pas.last_name                                                                   AS last_name , "
						  +" pas.title                                                                       AS title , "
						  +" pas.PNR                                                                         AS pnr , "
						  +" pas.PNR_PAX_ID                                                                  AS pnr_pax_id , "
						  +" pas.start_timestamp , "
						  +" pas.pax_type_code               AS pax_type , "
						  +" pas.DATE_OF_BIRTH               AS dob , "
						  +" pas.NATIONALITY_CODE            AS nationality , "
						  +" paxAddn.PASSPORT_NUMBER         AS foid_number , "
						  +" paxAddn.PASSPORT_EXPIRY         AS foid_expiry , "
						  +" paxAddn.PASSPORT_ISSUED_CNTRY   AS foid_issued_country , "
						  +" pc.ssr_code                     AS foid_ssr_code , "
						  +" paxAddn.PLACE_OF_BIRTH          AS place_of_birth , "
						  +" paxAddn.TRAVEL_DOC_TYPE         AS travel_doc_type , "
						  +" paxAddn.VISA_DOC_NUMBER         AS visa_doc_number , "
						  +" paxAddn.VISA_DOC_PLACE_OF_ISSUE AS visa_doc_place_of_issue , "
						  +" paxAddn.VISA_DOC_ISSUE_DATE     AS visa_doc_issue_date , "
						  +" paxAddn.VISA_APPLICABLE_COUNTRY AS visa_applicable_country , "
						  +" DECODE(paxAddn.VISA_DOC_NUMBER, NULL, NULL, 'DOCO') doco_ssr_code , "
						  +" pet.e_ticket_number AS eticket_no , "
						  +" ppfst.coupon_number AS coupon_no , "
						  +" (SELECT rpc.no "
						  +" FROM t_res_pcd rpc "
						  +" WHERE rpc.pnr_pax_id = pas.pnr_pax_id "
						  +" AND rownum           =1 "
						  +" ) AS cc_digits, "
						  +" vseg.group_id, "
						  +" vpnr.CS_FLIGHT_NUMBER         AS CS_FLIGHT_NO, "
						  +" vpnr.CS_BOOKING_CLASS         AS CS_BOOKING_CODE, "
						  +" VPNR.EST_TIME_DEPARTURE_LOCAL AS CS_DEPARTURE_TIME, "
						  +" vpnr.SEGMENT_CODE             AS cs_segment_code, "
						  +" PPFST.EXT_E_TICKET_NUMBER     AS EX_ETKT, "
						  +" PPFST.EXT_COUPON_NUMBER       AS EX_COUPON_NO, "
						  +" TR.EXTERNAL_REC_LOCATOR       AS EX_PNRN, "
						  +" f.flight_type , "
						  +" paxaddn.nationalid_no AS NIC "
//						  +" ppss.ssr_id           AS ssr "
						+" FROM T_Reservation tr , "
						  +" v_pnr_pax_segments vseg , "
						  +" V_PNR_SEGMENT vpnr , "
						  +" T_Pnr_passenger pas , "
						  +" T_PNR_PAX_ADDITIONAL_INFO paxAddn, "
						  +" T_PAX_INFO_CONFIG pc , "
						  +" T_PAX_E_TICKET pet , "
						  +" T_PNR_PAX_FARE_SEG_E_TICKET ppfst, "
						  +" T_FLIGHT f, "
						  +" t_pnr_pax_fare_segment ppfs, "
						  +" t_pnr_pax_segment_ssr ppss "
						+" WHERE pas.pnr            = tr.pnr "
						+" AND vseg.pnr             = tr.pnr "
						+" AND vpnr.pnr             = tr.pnr "
						+" AND pas.PNR              = tr.pnr "
						+" AND pas.PNR_PAX_ID       = paxAddn.PNR_PAX_ID "
						+" AND pc.PAX_TYPE_CODE     = pas.PAX_TYPE_CODE "
						+" AND pc.PAX_CATEGORY_CODE = pas.PAX_CATEGORY_CODE "
						+" AND pc.FIELD_NAME        = 'passportNo' "
						+" AND vpnr.SEGMENT_STATUS IN ('OPN', 'CLS') "
						+" AND vseg.BOOKING_CODE   IS NOT NULL "
						+ sqlBookingType
						+" AND vpnr.FLIGHT_NUMBER LIKE '"+carrierCode+"%' "
						+" AND vpnr.pnr_seg_id               = vseg.pnr_seg_id "
						+" AND pas.pnr_pax_id                = vseg.pnr_pax_id "
						+" AND vpnr.FLIGHT_ID                = ? "
						+" AND vpnr.FLIGHT_ID                = f.FLIGHT_ID "
						+" AND SUBSTR(vpnr.SEGMENT_CODE,0,3) = ? "
						+" AND pas.start_timestamp          <= ? "
						+" AND pas.PNR_PAX_ID                = pet.pnr_pax_id "
						+" AND pet.pax_e_ticket_id           = ppfst.pax_e_ticket_id "
						+" AND vseg.ppfs_id                  = ppfst.ppfs_id "
						+" AND vseg.pnr_seg_id               = ppfs.pnr_seg_id "
						+" AND ppfs.ppfs_id                  = ppss.ppfs_id "
	//					+" AND ppss.ssr_id                  IN "
	//					+"   (SELECT ssr_id FROM t_ssr_info ssr WHERE ssr.valid_for_palcal='Y') "
						+" AND vseg.ssr_code in (SELECT ssr.ssr_code FROM t_ssr_info ssr WHERE ssr.valid_for_palcal='Y') "
						+" AND ppfst.pnr_pax_fare_seg_e_ticket_id = "
						+"   (SELECT MAX(ppfst.pnr_pax_fare_seg_e_ticket_id) "
						+"   FROM T_PNR_PAX_FARE_SEG_E_TICKET ppfst "
						+"   WHERE vseg.ppfs_id = ppfst.ppfs_id "
						+"   GROUP BY ppfst.ppfs_id "
						+"   ) "
						+" ORDER BY pas.PNR_PAX_ID ";
		DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<PassengerInformation> passengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> finalPassengerInformation = new ArrayList<PassengerInformation>();

		templete.query(adlQuery, adlParams,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String foidNumber = rs.getString("foid_number");
					String segStatus = rs.getString("vp");
					String flightType = rs.getString("FLIGHT_TYPE");
					String nationalIDNo = rs.getString("NIC");
					boolean isNICSentInPNLADL = false;
					// TODO temporary fix untill we move PNL implementation also to msgbroker
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
						continue;
					}
					PassengerInformation passenger = new PassengerInformation();
					passenger.setAdlAction(PNLConstants.AdlActions.A.toString());
					passenger.setPnrPaxId(rs.getInt("pnr_pax_id"));
					passenger.setPnrSegId(rs.getInt("pnr_seg_id"));
					passenger.setPnr(rs.getString("pnr"));
					passenger.setFirstName(rs.getString("first_name") != null ? rs.getString("first_name").toUpperCase() : "");
					passenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getFirstName())));
					passenger.setLastName(rs.getString("last_name") != null ? rs.getString("last_name").toUpperCase() : "");
					passenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getLastName())));
					if (passenger.getFirstName().equalsIgnoreCase("TBA") || passenger.getLastName().equalsIgnoreCase("TBA")) {
						continue;
					}
					passenger.setTitle(rs.getString("title") != null ? rs.getString("title").toUpperCase() : "");
					passenger.setGender(PnlAdlUtil.findGender(passenger.getTitle()));
					passenger.setEticketNumber(rs.getString("eticket_no"));
					passenger.setCoupon(rs.getInt("coupon_no"));
					passenger.setDob(rs.getDate("dob"));

					if (("DOM").equals(flightType)) {
						if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
							if(nationalIDNo != null && !nationalIDNo.isEmpty()){
								foidNumber = nationalIDNo;
								isNICSentInPNLADL = true;
							}
						}
					} else {
						if ((foidNumber == null || "".equals(foidNumber)) && !"".equals(nationalIDNo)) {
							foidNumber = nationalIDNo;
							isNICSentInPNLADL = false;
						}
					}
					passenger.setFoidNumber(foidNumber);
					passenger.setNICSentInPNLADL(isNICSentInPNLADL);

					passenger.setFoidExpiry(rs.getDate("foid_expiry"));
					passenger.setFoidIssuedCountry(rs.getString("foid_issued_country"));
					passenger.setFoidSsrCode(rs.getString("foid_ssr_code"));
					passenger.setPlaceOfBirth(rs.getString("place_of_birth"));
					passenger.setTravelDocumentType(rs.getString("travel_doc_type"));
					passenger.setVisaDocNumber(rs.getString("visa_doc_number"));
					passenger.setVisaApplicableCountry(rs.getString("visa_applicable_country"));
					passenger.setVisaDocIssueDate(rs.getDate("visa_doc_issue_date"));
					passenger.setVisaDocPlaceOfIssue(rs.getString("visa_doc_place_of_issue"));

					// MARKETING/CODESHARE FLIGHT INFORMATION
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					passenger.setCsBookingClass(csBookingClass);
					passenger.setCsFlightNo(csFlightNo);
					passenger.setCsDepatureDateTime(rs.getTimestamp("CS_DEPARTURE_TIME"));
					passenger.setCsOrginDestination(rs.getString("cs_segment_code"));
					
					
					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						passenger.setExternalPnr(rs.getString("EX_PNRN"));
						passenger.setCodeShareFlight(true);
						if (rs.getString("EX_ETKT") != null) {
							passenger.setEticketNumber(rs.getString("EX_ETKT"));
						}
						if (rs.getInt("EX_COUPON_NO") > 0) {
							passenger.setCoupon(rs.getInt("EX_COUPON_NO"));
						}
					}
					
					

					String ccDigits = rs.getString("cc_digits");
					if (ccDigits != null) {
						passenger.setCcDigits(BeanUtils.getLast4Digits(ccDigits));
					}
					passenger.setBcType(rs.getString("bc_type"));
					passenger.setGroupId(rs.getString("group_id"));
					passenger.setPaxType(rs.getString("pax_type"));

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							passenger.setClassOfService(rs.getString("logical_cabin_class_code"));
						} else {
							passenger.setClassOfService(rs.getString("booking_code"));
						}
					} else {
						passenger.setClassOfService(rs.getString("cabin_class_code"));
					}
					passenger.setRbdClassOfService(String.valueOf(passenger.getClassOfService().charAt(0)));
					passenger.setDestinationAirportCode(rs.getString("destination"));
					Integer nationalityId = rs.getInt("nationality");
					if (nationalityId != null) {
						passenger.setNationality(PnlAdlUtil.loadPassengerNationality(nationalityId));

					}
					String bcType = rs.getString("bc_type");
					if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
						passenger.setStandByPassenger(true);
					}
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						passenger.setWaitListedPassenger(true);
					}
					passenger.setPnrStatus(segStatus);
					passenger.setPnlStatus(rs.getString("ps"));
					/*
					 * if ((passenger.getPaxType() != null) &&
					 * (passenger.getPaxType().trim().equalsIgnoreCase(ReservationInternalConstants
					 * .PassengerType.CHILD))) { passenger.setTitle("CHD"); }
					 */
					pnrPaxIds.add(passenger.getPnrPaxId());
					passengerInformation.add(passenger);
				}

				return passengerInformation;
			}
		});

		if (pnrPaxIds.isEmpty()) {
			return passengerInformation;
		}

		return filterBalToPayPassengers(pnrPaxIds, passengerInformation);

	}

	@Override
	public List<PassengerInformation> getCALPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimeStamp, String carrierCode) {


		String sqlBookingType = getBookingTypes();
		Object[] calParams = { PNLConstants.PnlPassengerStates.NOT_UPDATED, new Integer(flightId), departureAirportCode,
				scheduledTimeStamp };
		final List<Integer> pnrPaxIds = new ArrayList<Integer>();
		// TODO remove unnecassry fields from the query
		

		DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<PassengerInformation> passengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> finalPassengerInformation = new ArrayList<PassengerInformation>();
		
		String adlQuery = "SELECT DISTINCT vpnr.pnr_status AS vp                                             , "
				+ "                vseg.pnr_seg_id                                                                , "
				+ "                tr.status     AS ress                                                          , "
			//	+ "                vseg.pnl_stat AS ps                                                            , "
				+ "                vseg.cal_stat AS ps                                                            , "
				+ "                vseg.booking_code                                                                   , "
				+ "                vseg.bc_type                                                                        , "
				+ "                vseg.cabin_class_code                                                               , "
				+ "                vseg.logical_cabin_class_code                                                       , "
				+ "                vpnr.SEGMENT_SEQ                                                                    , "
				+ "                SUBSTR(vpnr.SEGMENT_CODE,LENGTH(vpnr.SEGMENT_CODE)-2,LENGTH(vpnr.SEGMENT_CODE)) AS destination   , "
				+ "                decode(msgPax.pal_stat, 'D', msgPax.FIRST_NAME, pas.FIRST_NAME)        AS first_name ,  "
				+ "                decode(msgPax.pal_stat, 'D', msgPax.last_name, pas.last_name)          AS last_name ,  "
				+ "                decode(msgPax.pal_stat, 'D', msgPax.title, pas.title)                  AS title ,  "
				+ "                msgPax.PNR                                                          AS pnr        , "
				+ "                msgPax.AIRPORT_MSG_PAX_ID               AS pnl_pax_id        , "
				+ "    			   msgPax.pal_stat as cal_action,"
				+ "                pas.PNR_PAX_ID                                                   AS pnr_pax_id , "
				+ "                pas.start_timestamp                                                            , "
				+ "                pas.pax_type_code             AS pax_type                                                  , "
				+ "                pas.DATE_OF_BIRTH             AS dob                                                       , "
				+ "                pas.NATIONALITY_CODE          AS nationality                                               , "
				+ "                paxAddn.PASSPORT_NUMBER       AS foid_number                                               , "
				+ "                paxAddn.PASSPORT_EXPIRY       AS foid_expiry                                               , "
				+ "                paxAddn.PASSPORT_ISSUED_CNTRY AS foid_issued_country                                         , "
				+ "                pc.ssr_code                   AS foid_ssr_code                                             , "
				+ "                paxAddn.PLACE_OF_BIRTH          AS place_of_birth                                             , "
				+ "                paxAddn.TRAVEL_DOC_TYPE         AS travel_doc_type                                            , "
				+ "                paxAddn.VISA_DOC_NUMBER         AS visa_doc_number                                            , "
				+ "                paxAddn.VISA_DOC_PLACE_OF_ISSUE AS visa_doc_place_of_issue                                    , "
				+ "                paxAddn.VISA_DOC_ISSUE_DATE     AS visa_doc_issue_date                                        , "
				+ "                paxAddn.VISA_APPLICABLE_COUNTRY AS visa_applicable_country                                    , "
				+ "                DECODE(paxAddn.VISA_DOC_NUMBER, NULL, NULL, 'DOCO') doco_ssr_code                             , "
				+ "                pet.e_ticket_number           AS eticket_no                                                , "
				+ "                ppfst.coupon_number           AS coupon_no                                                 , "
				+ "                (SELECT rpc.no " + "                FROM    t_res_pcd rpc "
				+ "                WHERE   rpc.pnr_pax_id = pas.pnr_pax_id " + "                AND     rownum         =1 "
				+ "                ) " + "                AS cc_digits, " + "                vseg.group_id, "
				+ "                vpnr.CS_FLIGHT_NUMBER AS CS_FLIGHT_NO, vpnr.CS_BOOKING_CLASS AS CS_BOOKING_CODE, "
				+ "                VPNR.EST_TIME_DEPARTURE_LOCAL AS CS_DEPARTURE_TIME, vpnr.SEGMENT_CODE AS cs_segment_code,  "
				+ "				   PPFST.EXT_E_TICKET_NUMBER AS EX_ETKT, PPFST.EXT_COUPON_NUMBER AS EX_COUPON_NO, "
				+ "				   TR.EXTERNAL_REC_LOCATOR AS EX_PNRN,  "
				+ "				   f.flight_type ,   paxaddn.nationalid_no AS NIC  "
				+ "FROM            T_Reservation tr                 , "
				+ "                v_pnr_pax_segments vseg          , "
				+ "                V_PNR_SEGMENT vpnr               , "
				+ "                T_Pnr_passenger pas              , "
				+ "                T_PNR_PAX_ADDITIONAL_INFO paxAddn, "
				+ "                T_PAX_INFO_CONFIG pc             , "
				+ "                T_PAX_E_TICKET pet               , "
				+ "                T_PNR_PAX_FARE_SEG_E_TICKET ppfst, "
				+ "				   T_FLIGHT f 						, "
				+ "				  t_airport_msg_pax msgPax "
				+ "WHERE           pas.pnr              = tr.pnr "
				+ "AND             vseg.pnr             = tr.pnr "
				+ "AND             vpnr.pnr             = tr.pnr "
				+ "AND             pas.PNR              = tr.pnr "
				+ "AND             pas.PNR_PAX_ID       = paxAddn.PNR_PAX_ID "
				+ "AND msgPax.pnr_pax_id = pas.pnr_pax_id "
				+ "AND msgPax.pnr_seg_id = vpnr.pnr_seg_id "
				+ "AND msgPax.status = ? "
				+ "AND             pc.PAX_TYPE_CODE     = pas.PAX_TYPE_CODE "
				+ "AND             pc.PAX_CATEGORY_CODE = pas.PAX_CATEGORY_CODE "
				+ "AND             pc.FIELD_NAME        = 'passportNo' "
				+ "AND             vpnr.SEGMENT_STATUS IN ('OPN', "
				+ "                                        'CLS') "
				+ "AND             vseg.BOOKING_CODE IS NOT NULL "
				+ sqlBookingType
				+ "AND             vpnr.FLIGHT_NUMBER LIKE '"
				+ carrierCode
				+ "%' "
				+ "AND             vpnr.pnr_seg_id       = vseg.pnr_seg_id "
				+ "AND             pas.pnr_pax_id        = vseg.pnr_pax_id "
				+ "AND             vpnr.FLIGHT_ID             = ? "
				+ "AND 			   vpnr.FLIGHT_ID        = f.FLIGHT_ID "
				+ "AND             SUBSTR(vpnr.SEGMENT_CODE,0,3)           = ? "
				+ "AND             pas.start_timestamp               <= ? "
				+ "AND             pas.PNR_PAX_ID                     = pet.pnr_pax_id "
				+ "AND             pet.pax_e_ticket_id                = ppfst.pax_e_ticket_id "
				+ "AND             vseg.ppfs_id                       = ppfst.ppfs_id "
				+ "AND             ppfst.pnr_pax_fare_seg_e_ticket_id = "
				+ "                (SELECT  MAX(ppfst.pnr_pax_fare_seg_e_ticket_id) "
				+ "                FROM     T_PNR_PAX_FARE_SEG_E_TICKET ppfst "
				+ "                WHERE    vseg.ppfs_id = ppfst.ppfs_id " + "                GROUP BY ppfst.ppfs_id "
				+ "                ) " + "ORDER BY        pas.PNR_PAX_ID";
		templete.query(adlQuery, calParams,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String foidNumber = rs.getString("foid_number");
					String segStatus = rs.getString("vp");
					String flightType = rs.getString("FLIGHT_TYPE");
					String nationalIDNo = rs.getString("NIC");
					boolean isNICSentInPNLADL = false;
					// TODO temporary fix untill we move PNL implementation also to msgbroker
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)
							&& rs.getString("cal_action").equals(PNLConstants.AdlActions.A.toString())) {
						continue;
					}
					PassengerInformation passenger = new PassengerInformation();
					passenger.setPnlPaxId(rs.getInt("pnl_pax_id"));
					passenger.setPnrPaxId(rs.getInt("pnr_pax_id"));
					passenger.setPnrSegId(rs.getInt("pnr_seg_id"));
					passenger.setPnr(rs.getString("pnr"));
					passenger.setAdlAction(rs.getString("cal_action"));
					passenger.setFirstName(rs.getString("first_name") != null ? rs.getString("first_name").toUpperCase() : "");
					passenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getFirstName())));
					passenger.setLastName(rs.getString("last_name") != null ? rs.getString("last_name").toUpperCase() : "");
					passenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getLastName())));
					if (passenger.getFirstName().equalsIgnoreCase("TBA") || passenger.getLastName().equalsIgnoreCase("TBA")) {
						continue;
					}
					passenger.setTitle(rs.getString("title") != null ? rs.getString("title").toUpperCase() : "");
					passenger.setGender(PnlAdlUtil.findGender(passenger.getTitle()));
					passenger.setEticketNumber(rs.getString("eticket_no"));
					passenger.setCoupon(rs.getInt("coupon_no"));
					passenger.setDob(rs.getDate("dob"));

					if (("DOM").equals(flightType)) {
						if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
							if(nationalIDNo != null && !nationalIDNo.isEmpty()){
								foidNumber = nationalIDNo;
								isNICSentInPNLADL = true;
							}
						}
					} else {
						if ((foidNumber == null || "".equals(foidNumber)) && !"".equals(nationalIDNo)) {
							foidNumber = nationalIDNo;
							isNICSentInPNLADL = false;
						}
					}
					passenger.setFoidNumber(foidNumber);
					passenger.setNICSentInPNLADL(isNICSentInPNLADL);

					passenger.setFoidExpiry(rs.getDate("foid_expiry"));
					passenger.setFoidIssuedCountry(rs.getString("foid_issued_country"));
					passenger.setFoidSsrCode(rs.getString("foid_ssr_code"));
					passenger.setPlaceOfBirth(rs.getString("place_of_birth"));
					passenger.setTravelDocumentType(rs.getString("travel_doc_type"));
					passenger.setVisaDocNumber(rs.getString("visa_doc_number"));
					passenger.setVisaApplicableCountry(rs.getString("visa_applicable_country"));
					passenger.setVisaDocIssueDate(rs.getDate("visa_doc_issue_date"));
					passenger.setVisaDocPlaceOfIssue(rs.getString("visa_doc_place_of_issue"));

					// MARKETING/CODESHARE FLIGHT INFORMATION
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					passenger.setCsBookingClass(csBookingClass);
					passenger.setCsFlightNo(csFlightNo);
					passenger.setCsDepatureDateTime(rs.getTimestamp("CS_DEPARTURE_TIME"));
					passenger.setCsOrginDestination(rs.getString("cs_segment_code"));
					
					
					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						passenger.setExternalPnr(rs.getString("EX_PNRN"));
						passenger.setCodeShareFlight(true);
						if (rs.getString("EX_ETKT") != null) {
							passenger.setEticketNumber(rs.getString("EX_ETKT"));
						}
						if (rs.getInt("EX_COUPON_NO") > 0) {
							passenger.setCoupon(rs.getInt("EX_COUPON_NO"));
						}
					}

					String ccDigits = rs.getString("cc_digits");
					if (ccDigits != null) {
						passenger.setCcDigits(BeanUtils.getLast4Digits(ccDigits));
					}
					passenger.setBcType(rs.getString("bc_type"));
					passenger.setGroupId(rs.getString("group_id"));
					passenger.setPaxType(rs.getString("pax_type"));

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							passenger.setClassOfService(rs.getString("logical_cabin_class_code"));
						} else {
							passenger.setClassOfService(rs.getString("booking_code"));
						}
					} else {
						passenger.setClassOfService(rs.getString("cabin_class_code"));
					}
					passenger.setRbdClassOfService(String.valueOf(passenger.getClassOfService().charAt(0)));
					passenger.setDestinationAirportCode(rs.getString("destination"));
					Integer nationalityId = rs.getInt("nationality");
					if (nationalityId != null) {
						passenger.setNationality(PnlAdlUtil.loadPassengerNationality(nationalityId));

					}
					String bcType = rs.getString("bc_type");
					if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
						passenger.setStandByPassenger(true);
					}
					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						passenger.setWaitListedPassenger(true);
					}
					passenger.setPnrStatus(segStatus);
					passenger.setPnlStatus(rs.getString("ps"));
			//		passenger.setPalStatus(rs.getString("cs"));
					/*
					 * if ((passenger.getPaxType() != null) &&
					 * (passenger.getPaxType().trim().equalsIgnoreCase(ReservationInternalConstants
					 * .PassengerType.CHILD))) { passenger.setTitle("CHD"); }
					 */
					pnrPaxIds.add(passenger.getPnrPaxId());
					passengerInformation.add(passenger);
				}

				return passengerInformation;
			}
		});

		if (pnrPaxIds.isEmpty()) {
			return passengerInformation;
		}

		return filterBalToPayPassengers(pnrPaxIds, passengerInformation);
	
	}
	
	@Override
	public List<PassengerInformation> getDeletedCALPassengerDetails(int flightId, String departureAirportCode,
			Timestamp scheduledTimestamp, String carrierCode) {
		


		Object[] adlParams = { new Integer(flightId), new Integer(flightId), PNLConstants.PnlPassengerStates.NOT_UPDATED, departureAirportCode,
				scheduledTimestamp };
		final List<Integer> pnrPaxIds = new ArrayList<Integer>();

		String adlQuery = " SELECT MSGPAX.AIRPORT_MSG_PAX_ID						           			 AS PNL_PAX_ID            , "
				+ "MSGPAX.PNR_PAX_ID  									                		 AS PNR_PAX_ID            , "
				+ "MSGPAX.PNR_SEG_ID  									                		 AS PNR_SEG_ID            , "
				+ "MSGPAX.PNR 		        										        	 AS PNR                   , "
				+ "TR.EXTERNAL_REC_LOCATOR 		        									 AS EXT_PNR               , "
				+ "PS.STATUS 								 					    	     AS SEGSTATUS             , "
				+ "SUBSTR(FS.SEGMENT_CODE,LENGTH(FS.SEGMENT_CODE)-2,LENGTH(FS.SEGMENT_CODE)) AS DESTINATION           , "
				+ "MSGPAX.FIRST_NAME                  											 AS FIRST_NAME			  , "
				+ "MSGPAX.LAST_NAME      													     AS LAST_NAME			  , "
				+ "MSGPAX.TITLE    											           		 AS TITLE				  , "
				+ "MSGPAX.PAX_TYPE_CODE											             AS PAX_TYPE			  , "
				+ "MSGPAX.PAL_STAT											                     AS CAL_ACTION 			  , "
				+ "MSGPAX.BC_TYPE											                     AS BC_TYPE 			  , "
				+ "MSGPAX.BOOKING_CODE										                     AS BOOKING_CODE		  , "
				+ "MSGPAX.CABIN_CLASS_CODE											             AS CABIN_CLASS_CODE	  , "
				+ "MSGPAX.LOGICAL_CABIN_CLASS_CODE											     AS LOGICAL_CABIN_CLASS_CODE, "
				+ "vpps.GROUP_ID                                                             AS GROUP_ID, "
				+ "vpnr.CS_FLIGHT_NUMBER 													 AS CS_FLIGHT_NO,"
				+ "vpnr.CS_BOOKING_CLASS 													 AS CS_BOOKING_CODE, "
				+ "vpps.PNL_STAT                                                             AS ps "
				+ "FROM t_airport_msg_pax MSGPAX, T_RESERVATION TR, T_PNR_SEGMENT PS, T_FLIGHT_SEGMENT FS, T_PNR_PASSENGER PP,V_PNR_PAX_SEGMENTS vpps,  V_PNR_SEGMENT vpnr WHERE	"
				+ "vpps.PNR = TR.PNR "
				+ "AND MSGPAX.PNR_SEG_ID = PS.PNR_SEG_ID AND MSGPAX.PNR = TR.PNR AND vpnr.pnr = TR.pnr AND  PP.PNR_PAX_ID = MSGPAX.PNR_PAX_ID 									"
				+ "AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND (FS.FLIGHT_ID = ?  										    "
				+ "OR MSGPAX.FLT_SEG_ID IN (SELECT fs.flt_seg_id FROM t_flight_segment fs WHERE fs.flight_id = ? ))        "
				+ "AND MSGPAX.PAL_STAT = 'D' AND MSGPAX.STATUS = ?																"
				+ "AND SUBSTR(FS.SEGMENT_CODE,0,3) = ? 	AND PP.START_TIMESTAMP <= ?										";

		DataSource ds = ReservationModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<PassengerInformation> passengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> redefinedPassengerInformation = new ArrayList<PassengerInformation>();
		List<PassengerInformation> finalPassengerInformation = new ArrayList<PassengerInformation>();

		templete.query(adlQuery, adlParams,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					PassengerInformation passenger = new PassengerInformation();
					passenger.setPnlPaxId(rs.getInt("PNL_PAX_ID"));
					passenger.setPnrPaxId(rs.getInt("PNR_PAX_ID"));
					passenger.setPnrSegId(rs.getInt("PNR_SEG_ID"));
					passenger.setPnr(rs.getString("PNR"));
					passenger.setAdlAction(AdlActions.D.toString());
					passenger.setFirstName(rs.getString("FIRST_NAME") != null ? rs.getString("FIRST_NAME").toUpperCase() : "");
					passenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getFirstName())));
					passenger.setLastName(rs.getString("LAST_NAME") != null ? rs.getString("LAST_NAME").toUpperCase() : "");
					passenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(passenger.getLastName())));
					if (passenger.getFirstName().equalsIgnoreCase("TBA") || passenger.getLastName().equalsIgnoreCase("TBA")) {
						continue;
					}
					passenger.setTitle(rs.getString("TITLE") != null ? rs.getString("TITLE").toUpperCase() : "");
					passenger.setGender(PnlAdlUtil.findGender(passenger.getTitle()));
					passenger.setPaxType(rs.getString("PAX_TYPE"));
					passenger.setDestinationAirportCode(rs.getString("DESTINATION"));
					passenger.setExternalPnr(rs.getString("EXT_PNR"));
				
					String bcType = rs.getString("BC_TYPE");
					passenger.setBcType(bcType);

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							passenger.setClassOfService(rs.getString("LOGICAL_CABIN_CLASS_CODE"));
						} else {
							passenger.setClassOfService(rs.getString("BOOKING_CODE"));
						}
					} else {
						passenger.setClassOfService(rs.getString("CABIN_CLASS_CODE"));
					}
					if (passenger.getClassOfService() != null) {
						passenger.setRbdClassOfService(String.valueOf(passenger.getClassOfService().charAt(0)));
					}
					if (BookingClass.BookingClassType.STANDBY.equals(bcType)) {
						passenger.setStandByPassenger(true);
					}
					
					String csFlightNo = rs.getString("CS_FLIGHT_NO");
					String csBookingClass = rs.getString("CS_BOOKING_CODE");

					passenger.setCsBookingClass(csBookingClass);
					passenger.setCsFlightNo(csFlightNo);
					passenger.setGroupId(rs.getString("GROUP_ID"));
										
					if (csFlightNo != null && !"".equals(csFlightNo.trim()) && csBookingClass != null
							&& !"".equals(csBookingClass.trim())) {
						passenger.setCodeShareFlight(true);
					}

					String segStatus = rs.getString("SEGSTATUS");

					if (segStatus != null && segStatus.equals(ReservationInternalConstants.ReservationSegmentStatus.WAIT_LISTING)) {
						passenger.setWaitListedPassenger(true);
					}
					passenger.setPnrStatus(segStatus);
					pnrPaxIds.add(passenger.getPnrPaxId());
					passenger.setPnlStatus(rs.getString("ps"));
					passengerInformation.add(passenger);
				}

				return passengerInformation;
			}
		});

		if (pnrPaxIds.isEmpty()) {
			return passengerInformation;
		}

		return filterBalToPayPassengers(pnrPaxIds, passengerInformation);

	}


	@Override
	public String getSavedPalCal(int flightID, Timestamp timestamp, String sitaAddress, String departureStation,
			String messageType, String transmissionStatus) throws ModuleException {

		final List<String> pnlADLMessages = new ArrayList<String>();
		boolean hasNoSita = false;
		StringBuilder strQuery = new StringBuilder();
		if (timestamp == null || sitaAddress == null || departureStation == null || messageType == null
				|| transmissionStatus == null) {
			return null;
		}
		if (sitaAddress.indexOf('@') != -1) {
			hasNoSita = true;
		}

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate jt = new JdbcTemplate(ds);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		Object params [] = {flightID,sitaAddress.trim(), departureStation.trim(),messageType.toUpperCase().trim(), transmissionStatus.toUpperCase().trim() };
		
		strQuery.append(" select SENT_MESSAGE from T_PAL_CAL_TX_HISTORY ");
		strQuery.append(" where FLIGHT_ID = ? ");
		strQuery.append(" and TRANSMISSION_TIMESTAMP = to_date('");
		strQuery.append(simpleDateFormat.format(timestamp));
		strQuery.append("','DD/MM/YYYY HH24:MI:SS') ");
		if (hasNoSita) {
			strQuery.append(" and EMAIL_ID = ?");
		} else {
			strQuery.append(" and SITA_ADDRESS = ?");
		}
		strQuery.append(" and AIRPORT_CODE = ?");
		strQuery.append(" and MESSAGE_TYPE = ?");
		strQuery.append(" and TRANSMISSION_STATUS = ?");

		jt.query(strQuery.toString(),params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Blob blobMSG = null;
				while (rs.next()) {
					blobMSG = rs.getBlob("SENT_MESSAGE");
					if (blobMSG != null) {
						pnlADLMessages.add(new String(blobMSG.getBytes(1, (int) blobMSG.length())));
						return pnlADLMessages;
					}
				}
				return pnlADLMessages;
			}
		});

		if (pnlADLMessages != null && !pnlADLMessages.isEmpty()) {
			return pnlADLMessages.get(0);
		}

		return null;

	}

	@Override
	public Collection<AirportPassengerMessageTxHsitory> getPalCalHistory(String flightNumber, Date localDate, String airport) {

		final Collection<AirportPassengerMessageTxHsitory> col = new ArrayList<AirportPassengerMessageTxHsitory>();
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { localDate, airport, flightNumber};

		String sql = "select DISTINCT h.pal_cal_tx_id, h.AIRPORT_CODE, h.EMAIL_ID, h.ERROR_DESCRIPTION, h.FLIGHT_ID,"
				+ " h.MESSAGE_TYPE, h.SENT_METHOD, h.SITA_ADDRESS, h.TRANSMISSION_STATUS, h.TRANSMISSION_TIMESTAMP,"
				+ " h.USER_ID, f.flight_number "
				+ " from "
				+ " T_PAL_CAL_TX_HISTORY h, "
				+ " T_FLIGHT f, T_Flight_segment fs "
				+ " where h.FLIGHT_ID=f.FLIGHT_ID "
				+ " and trunc(fs.EST_TIME_DEPARTURE_LOCAL)=? and "
				+ " f.flight_id=fs.flight_id and "
				+ " h.AIRPORT_CODE=substr(fs.segment_code,1,3) "
				+ " and h.AIRPORT_CODE= ?"
				+ " and f.FLIGHT_NUMBER= ? "
				+ " order by message_type desc,TRANSMISSION_TIMESTAMP ";
		
		templete.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				while (rs.next()) {
				
					AirportPassengerMessageTxHsitory tx = new AirportPassengerMessageTxHsitory();
					tx.setFlightNumber(rs.getInt("FLIGHT_ID"));
					tx.setSitaAddress(rs.getString("SITA_ADDRESS"));
					tx.setMessageType(rs.getString("MESSAGE_TYPE"));
					tx.setEmail(rs.getString("EMAIL_ID"));
					tx.setTransmissionTimeStamp(rs.getTimestamp("TRANSMISSION_TIMESTAMP"));
					tx.setTransmissionStatus(rs.getString("TRANSMISSION_STATUS"));
					tx.setAirportCode(rs.getString("AIRPORT_CODE"));
					col.add(tx);

				}
				return col;
			}
		});

		return col;

		
	}

	@Override
	public List<AirportMessagePassenger> getAirportMessagePassengers(Integer pnrPaxId, Integer pnrSegId, String palStatus) {
		String hql = "select pax from AirportMessagePassenger as pax "
				+ " where pax.pnrSegId =:pnrSegId and pax.pnrPaxId =:pnrPaxId and pax.palStatus =:palStatus ";

		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameter("pnrSegId", pnrSegId);
		hQuery.setParameter("pnrPaxId", pnrPaxId);
		hQuery.setParameter("palStatus", palStatus);
		List<AirportMessagePassenger> resultsList = hQuery.list();
		return resultsList;
	}
	
	@Override
	public List<AirportMessagePassenger> getAirportMessagePassengers(Integer pnrPaxId, Integer pnrSegId) {
		String hql = "select pax from AirportMessagePassenger as pax "
				+ " where pax.pnrSegId =:pnrSegId and pax.pnrPaxId =:pnrPaxId ";

		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameter("pnrSegId", pnrSegId);
		hQuery.setParameter("pnrPaxId", pnrPaxId);
		List<AirportMessagePassenger> resultsList = hQuery.list();
		return resultsList;
	}

	@Override
	public List<AirportMessagePassenger> getAirportMessagePassengers(String pnr) {
		String hql = "select pax from AirportMessagePassenger as pax "
				+ " where pax.pnr =:pnr and pax.status =:status ";

		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameter("pnr", pnr);
		hQuery.setParameter("status", PNLConstants.PAlCALSendingStatus.NOT_SEND);
		List<AirportMessagePassenger> resultsList = hQuery.list();
		return resultsList;
	}
	
	@Override
	public List<AirportMessagePassenger> getAirportMessagePassengersFromPnrSegIds(Collection<Integer> pnrSegIds) {

		String hql = "select pax from AirportMessagePassenger as pax "
				+ " where pax.status=:status and pax.pnrSegId in(:pnrSegIds)";

		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameterList("pnrSegIds", pnrSegIds);
		hQuery.setParameter("status", PNLConstants.PAlCALSendingStatus.NOT_SEND);
		List<AirportMessagePassenger> resultsList = hQuery.list();
		return resultsList;
	
	}

	@Override
	public void saveAirportMessagePassenger(Collection<AirportMessagePassenger> pnlPassengers) {
		hibernateSaveOrUpdateAll(pnlPassengers);

	}

	@Override
	public boolean hasAnyPALCALHistory(String flightNumber, Date flightDepZulu, String airportCode,
			boolean isSchdeularSentMsgHistoryOnly) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String depZulu = dateFormat.format(flightDepZulu);

		List<Object> args = new ArrayList<Object>();
		args.add(depZulu);
		args.add(flightNumber.trim());
		args.add(airportCode);

		String sql = " SELECT count(a.flight_id) " 
				+ " FROM t_flight_segment a , t_flight b, t_pal_cal_tx_history his "
				+ " where " + " a.flight_id = his.flight_id and a.flight_id = b.flight_id" 
				+ " and "
				+ " trunc(a.est_time_departure_zulu) =? " 
				+ " and b.status != 'CNX' " 
				+ " and trim(flight_number) = ? "
				+ " and substr(a.segment_code,1,3)=? ";

		if (isSchdeularSentMsgHistoryOnly) {
			sql = sql + " and his.SENT_METHOD= ? ";
			args.add(PNLConstants.SendingMethod.SCHEDSERVICE);

		}

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object intObj = template.query(sql, args.toArray(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int numOfRecs = 0;
		if (intObj != null) {
			numOfRecs = ((Integer) (intObj)).intValue();
		}

		if (numOfRecs >= 1) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public List<AirportMessagePassenger> getAirportMessagePassengers(Collection<Integer> pnlPaxIds) {
		String hql = "select pax from AirportMessagePassenger as pax " 
					+ " where pax.status= :status "
					+ " and pax.airportMsgPaxId in (:pnlPaxIds) ";

		Query hQuery = getSession().createQuery(hql);
		hQuery.setParameterList("pnlPaxIds", pnlPaxIds);
		hQuery.setParameter("status", PNLConstants.PAlCALSendingStatus.NOT_SEND);
		List<AirportMessagePassenger> resultsList = hQuery.list();
		return resultsList;
	}

	@Override
	public Object[] getPaxCountInPALCAL(Integer flightId, String airportCode) {


		String sql = "SELECT cnt.cabin_class_code,  cnt.logical_cabin_class_code,  "
				+ "cnt.booking_code,  cnt.pax_count,  cnt.last_group_id "
				+ "FROM T_PAL_CAL_PAX_COUNT cnt "
				+ "WHERE cnt.his_id =   ("
				+ "SELECT MAX(his.PAL_CAL_TX_ID)   "
				+ "FROM t_pal_cal_tx_history his   "
				+ "WHERE his.flight_id          = ? "
				+ "AND his.airport_code         = ? "
				+ "AND (his.transmission_status = ? "
				+ "OR his.transmission_status   = ?) "
				+ "AND his.sent_method   IS NOT NULL ) ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object params[] = { flightId, airportCode, PNLConstants.SITAMsgTransmission.SUCCESS,
				PNLConstants.SITAMsgTransmission.PARTIALLY };

		Object[] intObj = (Object[]) template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				HashMap<String, Integer> ccPaxMap = new HashMap<String, Integer>();
				String lastGroupCode = new String();
				while (rs.next()) {

					if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							ccPaxMap.put(rs.getString("logical_cabin_class_code"), new Integer(rs.getInt("pax_count")));
						} else {
							ccPaxMap.put(rs.getString("booking_code"), new Integer(rs.getInt("pax_count")));
						}
					} else {
						ccPaxMap.put(rs.getString("cabin_class_code"), new Integer(rs.getInt("pax_count")));
					}

					lastGroupCode = rs.getString("last_group_id");
				}

				return new Object[] { ccPaxMap, lastGroupCode };
			}

		});

		if (intObj == null) {
			throw new CommonsDataAccessException(PNLConstants.ERROR_CODES.PNL_COUNT_NOT_FOUND_FOR_ADL);
		}

		return (intObj);
	
	}
	
	private String getShowBookingsFilteredByAgentQuery(boolean isPrivillegedUser, String agentCode) {
		if (isPrivillegedUser) {
			return "";
		} else {
			return "AND r.owner_agent_code = '" + agentCode + "' ";
		}
	}
	
	@Override
	public Map<String, LinkedHashMap<String,String>> getFlightSeatConfigDetails(Integer flightId){
		
		Map<String, LinkedHashMap<String,String>> rawMap = new HashMap<String, LinkedHashMap<String,String>>();
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = {flightId};

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT IATA_AIRCRAFT_TYPE,CABIN_CLASS_CODE,SEAT_CAPACITY FROM T_AIRCRAFT_CC_CAPACITY");
		sqlBuilder.append(" JOIN T_AIRCRAFT_MODEL ON T_AIRCRAFT_MODEL.MODEL_NUMBER = T_AIRCRAFT_CC_CAPACITY.MODEL_NUMBER");
		sqlBuilder.append(" WHERE  T_AIRCRAFT_MODEL.MODEL_NUMBER IN ");
		sqlBuilder.append(" (SELECT T_FLIGHT.MODEL_NUMBER FROM T_FLIGHT JOIN T_AIRPORT");
		sqlBuilder.append(" ON T_FLIGHT.ORIGIN = T_AIRPORT.airport_code");
		sqlBuilder.append(" WHERE T_FLIGHT.flight_Id= ? and T_AIRPORT.pnl_cfg_enabled ='Y') ORDER BY SEAT_CAPACITY DESC");
		String sql = sqlBuilder.toString();
		
		templete.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					if (! rawMap.containsKey(rs.getString("IATA_AIRCRAFT_TYPE"))) {

						LinkedHashMap<String, String> capacityMap = new LinkedHashMap<String, String>();
						capacityMap.put(rs.getString("CABIN_CLASS_CODE"), rs.getString("SEAT_CAPACITY").toString());
						rawMap.put(rs.getString("IATA_AIRCRAFT_TYPE"), capacityMap);

					} else {

						rawMap.get(rs.getString("IATA_AIRCRAFT_TYPE")).put(rs.getString("CABIN_CLASS_CODE"),
								rs.getString("SEAT_CAPACITY"));
					}

				}
				return rawMap;
			}
		});

		return rawMap;
	}

	@Override
	public Map<Integer, Collection<Integer>> getPaxWiseBoardedCheckedInSegments(Collection<Integer> pnrSegIds) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

			String strFlgSegmentSql = " SELECT ppfs.pnr_seg_id , ppf.pnr_pax_id, ppfset.status "
					+ " FROM t_pnr_pax_fare_segment ppfs,  t_pnr_pax_fare_seg_e_ticket ppfset, t_pnr_pax_fare ppf	"
					+ " WHERE ppf.ppf_id = ppfs.ppf_id AND " + " ppfset.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id IN ("
					+ Util.buildIntegerInClauseContent(pnrSegIds) + ") ";

			@SuppressWarnings("unchecked")
			Map<Integer, Collection<Integer>> paxBoardedCheckedInPnrSegMap = (Map<Integer, Collection<Integer>>) jt.query(strFlgSegmentSql,
					new Object[] {}, new ResultSetExtractor() {
						Map<Integer, Collection<Integer>> paxBoardedCheckedInPnrSegMap = new HashMap<Integer, Collection<Integer>>();

						@Override
						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							while (rs.next()) {
								Integer pnrPaxId = rs.getInt("pnr_pax_id");
								Integer pnrSegId = rs.getInt("pnr_seg_id");
								String paxStatus = rs.getString("status");
								if (!paxBoardedCheckedInPnrSegMap.containsKey(pnrPaxId)) {
									paxBoardedCheckedInPnrSegMap.put(pnrPaxId, new ArrayList<Integer>());
								}
								if (EticketStatus.CHECKEDIN.code().equals(paxStatus) || EticketStatus.BOARDED.code().equals(paxStatus)) {
									paxBoardedCheckedInPnrSegMap.get(pnrPaxId).add(pnrSegId);
								}
							}
							return paxBoardedCheckedInPnrSegMap;
						}
					});
			return paxBoardedCheckedInPnrSegMap;

	}
	
	@Override
	public Collection<FlightLoadInfoDTO> getFlightLoadInfo(Date departureDateFromZulu, Date departureDateToZulu) {

		JdbcTemplate templete = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		final DateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		String flightDateFromZulu = df.format(departureDateFromZulu);
		String flightDateToZulu = df.format(departureDateToZulu);

		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT *FROM ");
		sqlBuilder.append("	(SELECT F.DEPARTURE_DATE, F.FLIGHT_NUMBER, F.FLIGHT_ID, F.MODEL_NUMBER, ");
		sqlBuilder.append(" NVL(BC.CABIN_CLASS_CODE,F_GET_INFANT_CABIN_CLASS_CODE(PP.PNR_PAX_ID,PS.PNR_SEG_ID,'CC')) CABIN_CLASS_CODE, ");
		sqlBuilder.append(" FS.SEGMENT_CODE, PP.PAX_TYPE_CODE ");
		sqlBuilder.append(" FROM T_PNR_SEGMENT PS ");
		sqlBuilder.append(" JOIN T_PNR_PASSENGER PP ON PS.PNR = PP.PNR ");
		sqlBuilder.append(" JOIN T_PNR_PAX_FARE PPF ON PP.PNR_PAX_ID = PPF.PNR_PAX_ID ");
		sqlBuilder.append(" JOIN T_PNR_PAX_FARE_SEGMENT PPFS ON PS.PNR_SEG_ID = PPFS.PNR_SEG_ID AND PPF.PPF_ID = PPFS.PPF_ID ");
		sqlBuilder.append(" JOIN T_FLIGHT_SEGMENT FS ON PS.FLT_SEG_ID = FS.FLT_SEG_ID ");
		sqlBuilder.append(" JOIN T_FLIGHT F ON FS.FLIGHT_ID = F.FLIGHT_ID ");
		sqlBuilder.append(" LEFT JOIN T_BOOKING_CLASS BC ON PPFS.BOOKING_CODE = BC.BOOKING_CODE ");
		sqlBuilder.append(" WHERE  FS.EST_TIME_DEPARTURE_ZULU  BETWEEN  TO_DATE('");
		sqlBuilder.append(flightDateFromZulu);
		sqlBuilder.append(" ','DD-MON-YYYY HH24:mi:ss' )  AND TO_DATE('");
		sqlBuilder.append(flightDateToZulu);
		sqlBuilder.append("', 'DD-MON-YYYY HH24:mi:ss' )");
		sqlBuilder.append(" AND F.STATUS = 'ACT' AND PS.STATUS = 'CNF' AND PP.STATUS = 'CNF' ");
		sqlBuilder.append(" AND (BC.BC_TYPE NOT IN ('OPENRT','STANDBY') OR BC.BC_TYPE IS NULL) ");
		sqlBuilder.append(" AND SUBSTR(F.FLIGHT_NUMBER,1,2) IN ");
		sqlBuilder.append(" (SELECT C.CARRIER_CODE  FROM T_CARRIER C ");
		sqlBuilder.append(" WHERE C.SERVICE_TYPE = 'AIRCRAFT') ");		   
		sqlBuilder.append(" ) PIVOT( COUNT(*) ");
		sqlBuilder.append(" FOR PAX_TYPE_CODE IN ('AD' TOTAL_ADULT_PAX,'CH' TOTAL_CHILD_PAX,'IN' TOTAL_INFANT_PAX)) ");

		return (Collection<FlightLoadInfoDTO>) templete.query(sqlBuilder.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, FlightLoadInfoDTO> flightLoadInfoMap = new HashMap<Integer, FlightLoadInfoDTO>();

				while (rs.next()) {
					FlightSegmentInventoryDTO flightSegmentInventory = new FlightSegmentInventoryDTO();
					CabinInventoryDTO cabinInventory = new CabinInventoryDTO();
					Map<String, CabinInventoryDTO> cabinInventoryMap = new HashMap<String, CabinInventoryDTO>();
					FlightLoadInfoDTO flightLoadInfo = new FlightLoadInfoDTO();

					String cabinClass = rs.getString("CABIN_CLASS_CODE");
					String aircraftModel = rs.getString("MODEL_NUMBER");
					String flightNumber = rs.getString("FLIGHT_NUMBER");
					Integer flightId = rs.getInt("FLIGHT_ID");
					Date depatureDate = rs.getTimestamp("DEPARTURE_DATE");

					flightSegmentInventory.setAdultCount(rs.getInt("TOTAL_ADULT_PAX"));
					flightSegmentInventory.setChildCount(rs.getInt("TOTAL_CHILD_PAX"));
					flightSegmentInventory.setInfantCount(rs.getInt("TOTAL_INFANT_PAX"));
					flightSegmentInventory.setTotalBookings(rs.getInt("TOTAL_ADULT_PAX") + rs.getInt("TOTAL_CHILD_PAX"));
					flightSegmentInventory.setSegmentCode(rs.getString("SEGMENT_CODE"));

					if (flightLoadInfoMap.containsKey(flightId)) {
						if (flightLoadInfoMap.get(flightId).getCabinInventoryMap().containsKey(cabinClass)) {
							flightLoadInfoMap.get(flightId).getCabinInventoryMap().get(cabinClass)
									.addFlightSegmentInventoryList(flightSegmentInventory);
						} else {
							cabinInventory.setCabinClassCode(cabinClass);
							cabinInventory.addFlightSegmentInventoryList(flightSegmentInventory);
							flightLoadInfoMap.get(flightId).getCabinInventoryMap().put(cabinClass, cabinInventory);
						}

					} else {
						cabinInventory.setCabinClassCode(cabinClass);
						cabinInventory.addFlightSegmentInventoryList(flightSegmentInventory);
						cabinInventoryMap.put(cabinClass, cabinInventory);

						flightLoadInfo.setFlightNumber(flightNumber);
						flightLoadInfo.setAircraftModel(aircraftModel);
						flightLoadInfo.setDepartureTimeZulu(depatureDate);
						flightLoadInfo.setCabinInventoryMap(cabinInventoryMap);

						flightLoadInfoMap.put(flightId, flightLoadInfo);
					}

				}
				return new ArrayList(flightLoadInfoMap.values());
			}
		});
	}
	
	@Override
	public void updateAutomaticCheckin(Collection<Integer> pnrPaxIds, Integer flightId, Integer noOfAttempts,
			String checkinStatus, String dcsResponseText) {
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " update T_PNR_PAX_SEG_AUTO_CHECKIN set NO_OF_ATTEMPTS =:noOfAttempts , DCS_CHECKIN_STATUS=:checkinStatus , "
				+ "DCS_RESPONSE_TEXT=:dcsResponseText   where  PNR_SEG_ID in (select PNR_SEG_ID from "
				+ "V_PNR_SEGMENT  pnrseg,T_FLIGHT_SEGMENT  fsegment  where fsegment.FLT_SEG_ID= pnrseg.FLT_SEG_ID  "
				+ " and fsegment.FLIGHT_ID=:flightId ) ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("dcsResponseText", dcsResponseText);
		params.put("flightId", flightId);
		params.put("noOfAttempts", noOfAttempts);
		params.put("checkinStatus", checkinStatus);
		if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
			pnrPaxIds = addInfantPnrPaxId(pnrPaxIds);
			sql += " and PNR_PAX_ID IN (:pnrPaxIds )";
			params.put("pnrPaxIds", pnrPaxIds.stream().collect(Collectors.toSet()));
		}
		template.update(sql, params);
	}

	private Collection<Integer> addInfantPnrPaxId(Collection<Integer> pnrPaxIds) {
		Collection<Integer> infantnrPaxIds = new ArrayList<Integer>();
		StringBuilder sqlBuilder = new StringBuilder();
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(ReservationModuleUtils.getDatasource());
		sqlBuilder.append(" select pp.PNR_PAX_ID ");
		sqlBuilder.append(" from T_PNR_PASSENGER pp ");
		sqlBuilder.append(" where pp.ADULT_ID IN (:pnrPaxIds ) ");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pnrPaxIds", pnrPaxIds);
		infantnrPaxIds = template.queryForList(sqlBuilder.toString(), params, Integer.class);
		pnrPaxIds.addAll(infantnrPaxIds);
		return pnrPaxIds;

	}

	private Collection<CheckinPassengersInfoDTO> mapCheckinPassengersInfoDTO(Map<String, Object> checkinPassengersInfoDTOMap,
			Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs) {
		CheckinPassengersInfoDTO checkinPassengersInfo = new CheckinPassengersInfoDTO();
		checkinPassengersInfo.setId(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("ID"))));
		checkinPassengersInfo.setFlightId(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("FLIGHT_ID"))));
		checkinPassengersInfo.setPnrPaxId(Integer.valueOf(String.valueOf(isIntegerNotNull(checkinPassengersInfoDTOMap
				.get("PNR_PAX_ID")))));
		checkinPassengersInfo.setAdultId(Integer.valueOf(String.valueOf(isIntegerNotNull(checkinPassengersInfoDTOMap
				.get("ADULT_ID")))));
		checkinPassengersInfo.setPaxType(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PAX_TYPE_CODE"))));
		checkinPassengersInfo.setCarrierInfoCode(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("CARRIERINFOCODE"))));
		checkinPassengersInfo.setCarrierInfoFlightnumber(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("CARRIERINFOFLIGHTNUMBER"))));
		checkinPassengersInfo.setDateOfDeparture(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("DATEOFDEPARTURE"))));
		checkinPassengersInfo.setDepartureLocationCode(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("DEPARTURELOCATIONCODE"))));
		checkinPassengersInfo.setFlightLegRPH(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("FLIGHTLEGRPH"))));
		checkinPassengersInfo.setFlightLegDepAirportLocationCode(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("FLTLEGDEPAIRPORTLOCATIONCODE"))));
		checkinPassengersInfo.setFlightLegAriAirportLocationCode(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("FLTLEGARIAIRPORTLOCATIONCODE"))));
		checkinPassengersInfo.setFlightLegOperatingAirlineCode(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("FLTLEGOPRTGAIRLINECODE"))));
		checkinPassengersInfo.setFlightLegOperatingAirlineFlightNumber(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("FLTLEGOPRTGAIRLINEFLTNUM"))));
		checkinPassengersInfo.setFlightInfoDateDeparture(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("FLIGHTINFODATEDEPARTURE"))));
		checkinPassengersInfo.setPaxSelectedSeat(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PAXSELECTEDSEAT"))));
		checkinPassengersInfo.setPaxType(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("PAXTYPE"))));
		checkinPassengersInfo
				.setPassportNumber(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("PASSPORTNUMBER"))));
		checkinPassengersInfo.setPassportIssuedCountry(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PASSPORTISSUEDCOUNTRY"))));
		checkinPassengersInfo.setPassportExpiryDate(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PASSPORTEXPIRYDATE"))));
		checkinPassengersInfo.setPassportFullName(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PASSPORTFULLNAME"))));
		checkinPassengersInfo.setVisaType(Integer.valueOf(String.valueOf(isIntegerNotNull(checkinPassengersInfoDTOMap
				.get("VISATYPE")))));
		checkinPassengersInfo.setVisaNumber(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("VISANUMBER"))));
		checkinPassengersInfo.setVisaIssuedCountry(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("VISAISSUEDCOUNTRY"))));
		checkinPassengersInfo
				.setVisaIssuedDate(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("VISAISSUEDDATE"))));
		checkinPassengersInfo.setDateOfBirth(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("DATEOFBIRTH"))));
		checkinPassengersInfo.setCountryOfResidence(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("COUNTRYOFRESIDENCE"))));
		checkinPassengersInfo.setPlaceOfBirth(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("PLACEOFBIRTH"))));
		checkinPassengersInfo.setDestinationCity(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("DESTINATIONCITY"))));
		checkinPassengersInfo.setNationality(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("NATIONALITY"))));
		checkinPassengersInfo.setTravelDocType(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("TRAVELDOCTYPE"))));
		checkinPassengersInfo.setStaffId(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("STAFFID"))));
		checkinPassengersInfo.setDateOfJoin(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("DATEOFJOIN"))));
		checkinPassengersInfo.setVisaApplicableCountry(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("VISAAPPLICABLECOUNTRY"))));
		checkinPassengersInfo.setGender(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("GENDER"))));
		checkinPassengersInfo.setPassengerGivenname(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PASSENGERGIVENNAME"))));
		checkinPassengersInfo.setPassengerSurname(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PASSENGERSURNAME"))));
		checkinPassengersInfo.setPassengerNametitle(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap
				.get("PASSENGERNAMETITLE"))));
		checkinPassengersInfo.setPreferredLanguage(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("PLANGUAGE"))));
		checkinPassengersInfo.setEmail(String.valueOf(isStringNotNull(checkinPassengersInfoDTOMap.get("EMAIL"))));
		checkinPassengersInfoDTOs.add(checkinPassengersInfo);
		return checkinPassengersInfoDTOs;
	}

	private Object isStringNotNull(Object data) {
		return data != null ? data : "";
	}

	private Object isIntegerNotNull(Object data) {
		return data != null ? data : "0";
	}


	@Override
	public List<ReservationPax> getReservationPaxSegAutoCheckin(Date departureDate, String flightNumber, String title,
			String firstName, String lastName, String pnr, String seatCode, String seatSelectionStatus) {
		Object[] params = { departureDate, flightNumber, firstName, lastName, pnr };
		JdbcTemplate templete = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("select pp.PNR_PAX_ID as pnrPaxId, pp.ADULT_ID as adultId , pp.PAX_TYPE_CODE as paxTypeCode ");
		sqlBuilder.append(" from t_flight f ");
		sqlBuilder.append("JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN v_PNR_SEGMENT ps on ps.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN T_PNR_PAX_SEG_AUTO_CHECKIN  ppsac  on ps.PNR_SEG_ID=ppsac.PNR_SEG_ID ");
		sqlBuilder.append("JOIN T_PNR_PASSENGER pp on pp.PNR_PAX_ID = ppsac.PNR_PAX_ID ");
		if (seatSelectionStatus.equalsIgnoreCase(REQ_SUCCESS)) {
			sqlBuilder.append("JOIN SM_T_FLIGHT_AM_SEAT  fas on fas.FLIGHT_AM_SEAT_ID=ppsac.FLIGHT_AM_SEAT_ID ");
			sqlBuilder.append("JOIN SM_T_AIRCRAFT_MODEL_SEATS ams on ams.AM_SEAT_ID=fas.AM_SEAT_ID ");
		}
		sqlBuilder.append("where f.DEPARTURE_DATE=to_timestamp(to_char( ? ,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') ");
		sqlBuilder.append("and f.FLIGHT_NUMBER=? ");
		sqlBuilder.append("and pp.FIRST_NAME=? ");
		sqlBuilder.append("and pp.LAST_NAME=? ");
		sqlBuilder.append("and pp.pnr=? ");
		if (title != null && title.length() > 0) {
			sqlBuilder.append("and pp.TITLE=? ");
			params = new Object[] { departureDate, flightNumber, firstName, lastName, pnr, title };
		}
		if (seatSelectionStatus.equalsIgnoreCase(REQ_SUCCESS)) {
			sqlBuilder.append("and ams.SEAT_CODE=? ");
			params = new Object[] { departureDate, flightNumber, firstName, lastName, pnr, seatCode };
		}
		if (seatSelectionStatus.equalsIgnoreCase(REQ_SUCCESS) && (title != null && title.length() > 0)) {
			params = new Object[] { departureDate, flightNumber, firstName, lastName, pnr, title, seatCode };
		}
		List<ReservationPax> reservationPaxs = new ArrayList<ReservationPax>();
		Collection<Map<String, Object>> rsList = templete.queryForList(sqlBuilder.toString(), params);
		rsList.stream().forEach(reservationPaxMap -> mapReservationPax(reservationPaxMap, reservationPaxs));
		return reservationPaxs;
	}
	
	private List<ReservationPax> getReservationInfantPaxSegAutoCheckin(Date departureDate, String flightNumber, String firstName,
			String lastName, String pnr, Integer adultId) {
		Object[] params = { departureDate, flightNumber, firstName, lastName, pnr };
		int[] types = { Types.DATE, Types.VARCHAR, Types.VARCHAR, Types.INTEGER };
		JdbcTemplate templete = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sqlBuilder = new StringBuilder();

		sqlBuilder.append("select pp.PNR_PAX_ID as pnrPaxId, pp.ADULT_ID as ADULTID , pp.PAX_TYPE_CODE as PAXTYPECODE");
		sqlBuilder.append(" from t_flight f ");
		sqlBuilder.append("JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN v_PNR_SEGMENT ps on ps.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN T_PNR_PAX_SEG_AUTO_CHECKIN  ppsac  on ps.PNR_SEG_ID=ppsac.PNR_SEG_ID ");
		sqlBuilder.append("JOIN T_PNR_PASSENGER pp on pp.PNR_PAX_ID = ppsac.PNR_PAX_ID ");
		sqlBuilder.append("where f.DEPARTURE_DATE=to_timestamp(to_char( ? ,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') ");
		sqlBuilder.append("and f.FLIGHT_NUMBER=? ");
		sqlBuilder.append("and pp.FIRST_NAME=? ");
		sqlBuilder.append("and pp.LAST_NAME=? ");
		sqlBuilder.append("and pp.pnr=? ");
		if (adultId != 0) {
			sqlBuilder.append("and pp.ADULT_ID=? ");
			params = new Object[] { departureDate, flightNumber, firstName, lastName, pnr, adultId };
		}
		List<ReservationPax> reservationPaxs = new ArrayList<ReservationPax>();
		Collection<Map<String, Object>> rsList = templete.queryForList(sqlBuilder.toString(), params);
		rsList.stream().forEach(reservationPaxMap -> mapReservationPax(reservationPaxMap, reservationPaxs));
		return reservationPaxs;
	}

	private List<ReservationPax> mapReservationPax(Map<String, Object> reservationPaxMap, List<ReservationPax> reservationPaxs) {
		ReservationPax reservationPax = null;
		if (reservationPaxMap != null && reservationPaxMap.size() > 0) {
			reservationPax = new ReservationPax();
			if (reservationPaxMap.get("PAXTYPECODE") != null
					&& ReservationInternalConstants.PassengerType.INFANT.equalsIgnoreCase(String.valueOf(reservationPaxMap
							.get("PAXTYPECODE")))) {
				if (reservationPaxMap.get("ADULTID") != null) {
					reservationPax.setPnrPaxId(Integer.valueOf(String.valueOf(reservationPaxMap.get("ADULTID"))));
					reservationPax.setPaxType(getPaxtype(Integer.valueOf(String.valueOf(reservationPaxMap.get("ADULTID")))));
				}
			} else {
				reservationPax.setPnrPaxId(Integer.valueOf(String.valueOf(reservationPaxMap.get("PNRPAXID"))));
				reservationPax.setPaxType(String.valueOf(reservationPaxMap.get("PAXTYPECODE")));
			}
			reservationPaxs.add(reservationPax);
		}
		return reservationPaxs;
	}

	@Override
	public PaxAutomaticCheckinTO getAutoCheckinPassengerDetails(Date departureDate, String flightNumber, String title,
			String firstName, String lastName, String pnr, String seatCode, String seatSelectionStatus) {
		PaxAutomaticCheckinTO automaticCheckinTO = null;
		ReservationPaxSegAutoCheckin  reservationPaxSegAutoCheckin;
		Query query = null;
		List<ReservationPax> reservationPaxs = getReservationPaxSegAutoCheckin(departureDate, flightNumber, title, firstName,
				lastName, pnr, seatCode, seatSelectionStatus);
		Collection pnrPaxIds = reservationPaxs.stream().map(s -> s.getPnrPaxId()).collect(Collectors.toList());
		String hqlFirstAttempt = "from ReservationPaxSegAutoCheckin ppsac where ppsac.pnrPaxId IN :pnrPaxIds  "
				+ "and  ppsac.dcsCheckinStatus= :dcsCheckinStatus ";
		query = getSession().createQuery(hqlFirstAttempt);
		query.setParameterList("pnrPaxIds", pnrPaxIds);
		query.setParameter("dcsCheckinStatus", INITIAL_ATTEMPT);
		query.setMaxResults(1);
		reservationPaxSegAutoCheckin = (ReservationPaxSegAutoCheckin)query.uniqueResult();
		if(reservationPaxSegAutoCheckin != null){
			return mapReservationPaxSegAutoCheckin(reservationPaxSegAutoCheckin);
		}
		String hqlSecondAttempt = "from ReservationPaxSegAutoCheckin ppsac where ppsac.pnrPaxId IN :pnrPaxIds "
				+ "and  ppsac.dcsCheckinStatus IN:dcsCheckinStatusList ";
		query = getSession().createQuery(hqlSecondAttempt);
		query.setParameterList("pnrPaxIds", pnrPaxIds);
		query.setParameterList("dcsCheckinStatusList", new ArrayList<String>(Arrays.asList(RETRIED_BOTH, RETRIED_SEAT)));
		query.setMaxResults(1);
		reservationPaxSegAutoCheckin = (ReservationPaxSegAutoCheckin)query.uniqueResult();
		if(reservationPaxSegAutoCheckin != null){
			return mapReservationPaxSegAutoCheckin(reservationPaxSegAutoCheckin);
		}
		return automaticCheckinTO;
	}

	private PaxAutomaticCheckinTO
			mapReservationPaxSegAutoCheckin(ReservationPaxSegAutoCheckin reservationPaxSegAutoCheckin) {
		PaxAutomaticCheckinTO paxAutoCheckin = new PaxAutomaticCheckinTO();
		paxAutoCheckin.setEmail(reservationPaxSegAutoCheckin.getEmail());
		paxAutoCheckin.setNoOfAttempts(reservationPaxSegAutoCheckin.getNoOfAttempts());
		paxAutoCheckin.setPnrPaxId(reservationPaxSegAutoCheckin.getPnrPaxId());
		return paxAutoCheckin;
	}

	@Override
	public Collection<CheckinPassengersInfoDTO> getPassengerDetailsforCheckin(Integer flightId, Integer pnrPaxId)
			throws ModuleException {

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Object params[] = { flightId };
		int[] types = { Types.INTEGER };
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT ps.pnr as id , f.FLIGHT_ID, pp.PNR_PAX_ID, pp.ADULT_ID, pp.PAX_TYPE_CODE, ");
		sqlBuilder.append("SUBSTR(f.flight_number,0,2) AS carrierInfocode,");
		sqlBuilder.append("SUBSTR(f.flight_number,3,LENGTH(f.flight_number)) AS carrierInfoFlightnumber ,");
		sqlBuilder.append("	f.DEPARTURE_DATE AS dateofdeparture,");
		sqlBuilder.append("f.DESTINATION as departureLocationcode,");
		sqlBuilder.append("fl.LEG_NUMBER as  flightLegRPH,");
		sqlBuilder.append("f.ORIGIN as fltLegDepAirportlocationcode ,");
		sqlBuilder.append("f.DESTINATION as fltLegAriAirportlocationCode ,");
		sqlBuilder.append("f.DEPARTURE_DATE AS flightInfoDatedeparture,");
		sqlBuilder.append("SUBSTR(f.flight_number,0,2) as fltLegOprtgAirlineCode,");
		sqlBuilder.append("SUBSTR(f.flight_number,3,LENGTH(f.flight_number)) as fltLegOprtgAirlineFltNum,");
		sqlBuilder.append("ams.SEAT_CODE as paxSelectedseat,");
		sqlBuilder.append("pp.PAX_TYPE_CODE as paxtype,");
		sqlBuilder.append("	ppai.PASSPORT_NUMBER as  passportnumber,");
		sqlBuilder.append("ppai.PASSPORT_ISSUED_CNTRY as passportIssuedCountry,");
		sqlBuilder.append("ppai.PASSPORT_EXPIRY as  passportExpiryDate,");
		sqlBuilder.append("CONCAT(pp.FIRST_NAME,(' '||pp.LAST_NAME)) as  passportFullName,");
		sqlBuilder.append("1 as  visaType,");
		sqlBuilder.append("ppai.VISA_DOC_NUMBER as  visaNumber,");
		sqlBuilder.append("ppai.VISA_DOC_PLACE_OF_ISSUE as  visaIssuedCountry,");
		sqlBuilder.append("ppai.VISA_DOC_ISSUE_DATE as visaIssuedDate,");
		sqlBuilder.append("pp.DATE_OF_BIRTH as dateOfBirth,");
		sqlBuilder.append("rc.C_COUNTRY_CODE as countryOfResidence ,");
		sqlBuilder.append("ppai.PLACE_OF_BIRTH as placeOfBirth,");
		sqlBuilder.append("ci.CITY_NAME as destinationCity ,");
		sqlBuilder.append("n.description as nationality,");
		sqlBuilder.append("ppai.TRAVEL_DOC_TYPE as travelDocType,");
		sqlBuilder.append("ppai.EMPLOYEE_ID as staffId,");
		sqlBuilder.append("ppai.DATE_OF_JOIN as  dateOfJoin,");
		sqlBuilder.append("ppai.VISA_APPLICABLE_COUNTRY as visaApplicableCountry,");
		sqlBuilder.append("CASE pp.TITLE WHEN 'MR' THEN 'M'  WHEN 'MS'  THEN 'F' WHEN 'MRS'  THEN 'F' ELSE 'O' END  AS gender,");
		sqlBuilder.append("pp.FIRST_NAME as  passengerGivenname,");
		sqlBuilder.append(" pp.LAST_NAME as  passengerSurname,");
		sqlBuilder.append("pp.TITLE as passengerNametitle,");
		sqlBuilder.append("rc.PREFERRED_LANG as planguage, ");
		sqlBuilder.append(" ppsac.email as email ");
		sqlBuilder.append("FROM t_FLIGHT f ");
		sqlBuilder.append("JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("join V_PNR_SEGMENT ps ON fs.FLT_SEG_ID = ps.FLT_SEG_ID  ");
		sqlBuilder.append("JOIN T_FLIGHT_LEG fl ON f.FLIGHT_ID =fl.FLIGHT_ID ");
		sqlBuilder.append("join T_PNR_PAX_SEG_AUTO_CHECKIN  ppsac on ps.PNR_SEG_ID= ppsac.PNR_SEG_ID ");
		sqlBuilder
				.append("JOIN SM_T_FLIGHT_AM_SEAT fas on  (fs.FLT_SEG_ID = fas.FLT_SEG_ID and ppsac.FLIGHT_AM_SEAT_ID = fas.FLIGHT_AM_SEAT_ID) ");
		sqlBuilder.append("join T_PNR_PASSENGER pp on f.FLIGHT_ID=ps.FLIGHT_ID and ppsac.PNR_PAX_ID=pp.PNR_PAX_ID ");
		sqlBuilder.append("join SM_T_AIRCRAFT_MODEL_SEATS ams on ams.AM_SEAT_ID = fas.AM_SEAT_ID ");
		sqlBuilder.append("JOIN T_PNR_PAX_ADDITIONAL_INFO ppai ON ppai.PNR_PAX_ID =pp.PNR_PAX_ID ");
		sqlBuilder.append("JOIN T_RESERVATION_CONTACT  rc ON pp.pnr=rc.PNR ");
		sqlBuilder.append("JOIN T_NATIONALITY n ON n.NATIONALITY_CODE= pp.NATIONALITY_CODE ");
		sqlBuilder.append("JOIN T_CITY ci ON ci.CITY_CODE=f.DESTINATION ");
		sqlBuilder.append("where f.FLIGHT_ID= ? ");
		if (pnrPaxId != 0) {
			sqlBuilder.append("and pp.PNR_PAX_ID= ? ");
			params = new Object[] { flightId, pnrPaxId };
			types = new int[] { Types.INTEGER, Types.INTEGER };
		}
		sqlBuilder.append("order  by ps.pnr");
		Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs = new ArrayList<CheckinPassengersInfoDTO>();
		Collection<Map<String, Object>> rsList = template.queryForList(sqlBuilder.toString(), params, types);
		rsList.stream()
				.forEach(
						checkinPassengersInfoDTOMap -> mapCheckinPassengersInfoDTO(checkinPassengersInfoDTOMap,
								checkinPassengersInfoDTOs));

		return removeForceConfirmedReservations(checkinPassengersInfoDTOs,
				getInfantPassengerDetailsforCheckin(flightId, pnrPaxId));
	}

	private Collection<CheckinPassengersInfoDTO> removeForceConfirmedReservations(
			Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs,
			Collection<CheckinPassengersInfoDTO> checkinInfantPassengersInfoDTOs) throws ModuleException {
		Collection<CheckinPassengersInfoDTO> cnfResrvationcheckinPassengersInfoDTOs = new ArrayList<CheckinPassengersInfoDTO>();

		Map<Integer, CheckinPassengersInfoDTO> checkinInfantPassengersInfoDTOMap = checkinInfantPassengersInfoDTOs.stream()
				.collect(
						Collectors.toMap(checkinInfantPassengersInfoDTO -> checkinInfantPassengersInfoDTO.getAdultId(),
								checkinInfantPassengersInfoDTO -> checkinInfantPassengersInfoDTO));
		for (CheckinPassengersInfoDTO checkinPassengersInfoDTO : checkinPassengersInfoDTOs) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(checkinPassengersInfoDTO.getId());
			pnrModesDTO.setLoadFares(false);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(false);
			Reservation reservation;
			try {
				reservation = ReservationProxy.getReservation(pnrModesDTO);
				if (reservation.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 0
						&& reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
					if (checkinInfantPassengersInfoDTOMap.get(checkinPassengersInfoDTO.getPnrPaxId()) != null) {
						checkinPassengersInfoDTO.setTravelWithInfant("Y");
						cnfResrvationcheckinPassengersInfoDTOs.add(checkinPassengersInfoDTO);
						checkinInfantPassengersInfoDTOMap.get(checkinPassengersInfoDTO.getPnrPaxId()).setPaxSelectedSeat(
								checkinPassengersInfoDTO.getPaxSelectedSeat());
						checkinInfantPassengersInfoDTOMap.get(checkinPassengersInfoDTO.getPnrPaxId()).setEmail(
								checkinPassengersInfoDTO.getEmail());
						cnfResrvationcheckinPassengersInfoDTOs.add(checkinInfantPassengersInfoDTOMap.get(checkinPassengersInfoDTO
								.getPnrPaxId()));
					} else {
						checkinPassengersInfoDTO.setTravelWithInfant("");
						cnfResrvationcheckinPassengersInfoDTOs.add(checkinPassengersInfoDTO);
					}
				}
			} catch (ModuleException e) {
				throw new ModuleException(e, "pnr.pax.balance.error");
			}

		}
		return cnfResrvationcheckinPassengersInfoDTOs;
	}
	
	@Override
	public Collection<CheckinPassengersInfoDTO> getInfantPassengerDetailsforCheckin(Integer flightId, Integer pnrPaxId)
			throws ModuleException {

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Object params[] = { flightId, ReservationInternalConstants.PassengerType.INFANT };
		int[] types = { Types.INTEGER, Types.VARCHAR };
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" SELECT ps.pnr as id , ");
		sqlBuilder.append(" f.FLIGHT_ID,");
		sqlBuilder.append(" pp.PNR_PAX_ID, ");
		sqlBuilder.append(" pp.ADULT_ID, ");
		sqlBuilder.append("SUBSTR(f.flight_number,0,2) AS carrierInfocode,");
		sqlBuilder.append("SUBSTR(f.flight_number,3,LENGTH(f.flight_number)) AS carrierInfoFlightnumber ,");
		sqlBuilder.append("	f.DEPARTURE_DATE AS dateofdeparture,");
		sqlBuilder.append("f.DESTINATION as departureLocationcode,");
		sqlBuilder.append("fl.LEG_NUMBER as  flightLegRPH,");
		sqlBuilder.append("f.ORIGIN as fltLegDepAirportlocationcode ,");
		sqlBuilder.append("f.DESTINATION as fltLegAriAirportlocationCode ,");
		sqlBuilder.append("f.DEPARTURE_DATE AS flightInfoDatedeparture,");
		sqlBuilder.append("SUBSTR(f.flight_number,0,2) as fltLegOprtgAirlineCode,");
		sqlBuilder.append("SUBSTR(f.flight_number,3,LENGTH(f.flight_number)) as fltLegOprtgAirlineFltNum,");
		sqlBuilder.append("pp.PAX_TYPE_CODE as paxtype,");
		sqlBuilder.append("	ppai.PASSPORT_NUMBER as  passportnumber,");
		sqlBuilder.append("ppai.PASSPORT_ISSUED_CNTRY as passportIssuedCountry,");
		sqlBuilder.append("ppai.PASSPORT_EXPIRY as  passportExpiryDate,");
		sqlBuilder.append("CONCAT(pp.FIRST_NAME,(' '||pp.LAST_NAME)) as  passportFullName,");
		sqlBuilder.append("1 as  visaType,");
		sqlBuilder.append("ppai.VISA_DOC_NUMBER as  visaNumber,");
		sqlBuilder.append("ppai.VISA_DOC_PLACE_OF_ISSUE as  visaIssuedCountry,");
		sqlBuilder.append("ppai.VISA_DOC_ISSUE_DATE as visaIssuedDate,");
		sqlBuilder.append("pp.DATE_OF_BIRTH as dateOfBirth,");
		sqlBuilder.append("rc.C_COUNTRY_CODE as countryOfResidence ,");
		sqlBuilder.append("ppai.PLACE_OF_BIRTH as placeOfBirth,");
		sqlBuilder.append("ci.CITY_NAME as destinationCity ,");
		sqlBuilder.append("n.description as nationality,");
		sqlBuilder.append("ppai.TRAVEL_DOC_TYPE as travelDocType,");
		sqlBuilder.append("ppai.EMPLOYEE_ID as staffId,");
		sqlBuilder.append("ppai.DATE_OF_JOIN as  dateOfJoin,");
		sqlBuilder.append("ppai.VISA_APPLICABLE_COUNTRY as visaApplicableCountry,");
		sqlBuilder.append("CASE pp.TITLE WHEN 'MR' THEN 'M'  WHEN 'MS'  THEN 'F' WHEN 'MRS'  THEN 'F' ELSE 'O' END  AS gender,");
		sqlBuilder.append("pp.FIRST_NAME as  passengerGivenname,");
		sqlBuilder.append(" pp.LAST_NAME as  passengerSurname,");
		sqlBuilder.append("pp.TITLE as passengerNametitle,");
		sqlBuilder.append("rc.PREFERRED_LANG as planguage ");
		sqlBuilder.append("FROM t_FLIGHT f ");
		sqlBuilder.append("JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN V_PNR_SEGMENT ps ON fs.FLT_SEG_ID = ps.FLT_SEG_ID  ");
		sqlBuilder.append("JOIN T_FLIGHT_LEG fl ON f.FLIGHT_ID =fl.FLIGHT_ID ");
		sqlBuilder.append("JOIN T_PNR_PASSENGER pp on pp.PNR=ps.PNR ");
		sqlBuilder.append(" LEFT JOIN T_PNR_PAX_ADDITIONAL_INFO ppai ON ppai.PNR_PAX_ID =pp.PNR_PAX_ID ");
		sqlBuilder.append(" LEFT JOIN T_RESERVATION_CONTACT  rc ON pp.PNR=rc.PNR ");
		sqlBuilder.append(" LEFT JOIN T_NATIONALITY n ON n.NATIONALITY_CODE= pp.NATIONALITY_CODE ");
		sqlBuilder.append(" LEFT JOIN T_CITY ci ON ci.CITY_CODE=f.DESTINATION ");
		sqlBuilder.append("where f.FLIGHT_ID= ? and pp.PAX_TYPE_CODE= ? ");
		if (pnrPaxId != 0) {
			sqlBuilder.append("and pp.ADULT_ID= ? ");
			params = new Object[] { flightId, ReservationInternalConstants.PassengerType.INFANT, pnrPaxId };
			types = new int[] { Types.INTEGER, Types.VARCHAR, Types.INTEGER };
		}
		sqlBuilder.append("order  by ps.pnr");
		Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs = new ArrayList<CheckinPassengersInfoDTO>();
		Collection<Map<String, Object>> rsList = template.queryForList(sqlBuilder.toString(), params, types);
		rsList.stream()
				.forEach(
						checkinPassengersInfoDTOMap -> mapCheckinPassengersInfoDTO(checkinPassengersInfoDTOMap,
								checkinPassengersInfoDTOs));
		return checkinPassengersInfoDTOs;
	}
	

	@Override
	public Collection<PaxAutomaticCheckinTO> getPaxWiseAutoCheckinInfo(Integer flightId) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE,
				ReservationConstants.SegmentStatus.CONFIRMED, flightId };
		Collection<PaxAutomaticCheckinTO> colAutomaticCheckins = new ArrayList<PaxAutomaticCheckinTO>();
		StringBuilder sqlQuery = new StringBuilder();

		sqlQuery.append("SELECT ppsac.amount,ppsac.automatic_checkin_template_id,ppsac.customer_id,ppsac.dcs_checkin_status, ");
		sqlQuery.append("ppsac.dcs_response_text,ppsac.email,ppsac.flight_am_seat_id,ppsac.no_of_attempts,ppsac.pnr_pax_id, ");
		sqlQuery.append("ppsac.ppac_id,ppsac.pnr_seg_id,ppsac.ppf_id,ppsac.request_timestamp,ppsac.sales_channel_code, ");
		sqlQuery.append("ppsacs.seat_type_preference,ppsac.status,ppsac.user_id, ps.PNR, ppsac.SIT_TOGETHER_PNR_PAX_ID ");
		sqlQuery.append("FROM T_FLIGHT f,T_FLIGHT_SEGMENT fs, T_PNR_SEGMENT ps, T_PNR_PASSENGER pp, ");
		sqlQuery.append("T_PNR_PAX_SEG_AUTO_CHECKIN ppsac, T_PNR_PAX_SEG_AUTO_CHKIN_SEAT ppsacs ");
		sqlQuery.append("WHERE fs.flight_id=f.FLIGHT_ID AND fs.FLT_SEG_ID = ps.FLT_SEG_ID ");
		sqlQuery.append("AND ps.PNR_SEG_ID= ppsac.PNR_SEG_ID AND  ppsac.PNR_PAX_ID = pp.PNR_PAX_ID ");
		sqlQuery.append("AND ppsacs.PPAC_ID (+)= ppsac.PPAC_ID AND pp.PAX_TYPE_CODE in ('AD','CH') ");
		sqlQuery.append("AND ppsac.STATUS= ? AND pp.STATUS = ? AND f.FLIGHT_ID= ? ");
		sqlQuery.append("ORDER by ppsac.REQUEST_TIMESTAMP, pp.PAX_SEQUENCE");

		Collection<Map<String, Object>> rsList = template.queryForList(sqlQuery.toString(), params);
		rsList.stream().forEach(pax -> collectPPSAC(pax, colAutomaticCheckins));
		return colAutomaticCheckins;
	}

	private Collection<PaxAutomaticCheckinTO> collectPPSAC(Map<String, Object> paxs,
			Collection<PaxAutomaticCheckinTO> colAutomaticCheckins) {
		PaxAutomaticCheckinTO paxAutoCheckin = new PaxAutomaticCheckinTO();
		paxAutoCheckin.setAmount((BigDecimal) (paxs.get("AMOUNT")));
		paxAutoCheckin.setAutoCheckinTemplateId(this.bigDecimalToInt(paxs.get("automatic_checkin_template_id")));
		paxAutoCheckin.setCustomerId(this.bigDecimalToInt(paxs.get("customer_id")));
		paxAutoCheckin.setDcsCheckinStatus((String) paxs.get("dcs_checkin_status"));
		paxAutoCheckin.setDcsResponseText((String) paxs.get("dcs_response_text"));
		paxAutoCheckin.setEmail((String) paxs.get("email"));
		paxAutoCheckin.setFlightAmSeatId(this.bigDecimalToInt(paxs.get("flight_am_seat_id")));
		paxAutoCheckin.setNoOfAttempts(this.bigDecimalToInt(paxs.get("no_of_attempts")));
		paxAutoCheckin.setPnrPaxId(this.bigDecimalToInt(paxs.get("pnr_pax_id")));
		paxAutoCheckin.setPnrPaxSegAutoCheckinId(this.bigDecimalToInt(paxs.get("ppac_id")));
		paxAutoCheckin.setPnrSegId(this.bigDecimalToInt(paxs.get("pnr_seg_id")));
		paxAutoCheckin.setPpfId(this.bigDecimalToInt(paxs.get("ppf_id")));
		paxAutoCheckin.setRequestTimeStamp((Date) paxs.get("request_timestamp"));
		paxAutoCheckin.setSalesChannelCode(this.bigDecimalToInt(paxs.get("sales_channel_code")));
		paxAutoCheckin.setSeatTypePreference((String) paxs.get("seat_type_preference"));
		paxAutoCheckin.setSitTogetherPnrPaxId(this.bigDecimalToInt(paxs.get("SIT_TOGETHER_PNR_PAX_ID")));
		paxAutoCheckin.setStatus((String) paxs.get("status"));
		paxAutoCheckin.setUserId((String) paxs.get("user_id"));
		paxAutoCheckin.setPnr((String) paxs.get("pnr"));
		colAutomaticCheckins.add(paxAutoCheckin);

		return colAutomaticCheckins;
	}

	@Override
	public Collection<FlightSeatStatusTO> getFlightAvailableSeatMap(int flightId, int blockedSeatRows) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightId, blockedSeatRows };

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT fas.FLIGHT_AM_SEAT_ID,ams.AM_SEAT_ID,ams.SEAT_LOCATION_TYPE,ams.COL_ID,");
		sqlQuery.append("ams.SEAT_CODE,ams.row_ID,fas.status FROM t_flight f, SM_T_AIRCRAFT_MODEL_SEATS ams,");
		sqlQuery.append("t_flight_segment fs, SM_T_FLIGHT_AM_SEAT fas where f.MODEL_NUMBER =ams.MODEL_NUMBER AND f.FLIGHT_ID = fs.FLIGHT_ID AND ");
		sqlQuery.append("fs.FLT_SEG_ID = fas.FLT_SEG_ID AND fas.AM_SEAT_ID = ams.AM_SEAT_ID AND ");
		sqlQuery.append("f.FLIGHT_ID= ? and ams.COL_ID > ? ORDER BY ams.COL_ID,ams.row_id");
		Collection<FlightSeatStatusTO> flightSeatsList = new ArrayList<FlightSeatStatusTO>();
		Collection<Map<String, Object>> rsList = template.queryForList(sqlQuery.toString(), params);
		rsList.stream().forEach(pax -> collectFlightSeat(pax, flightSeatsList));
		return flightSeatsList;
	}

	/**
	 * @param paxs
	 * @param fltTos
	 * @return
	 */
	private Collection<FlightSeatStatusTO> collectFlightSeat(Map<String, Object> paxs, Collection<FlightSeatStatusTO> fltTos) {
		FlightSeatStatusTO fltTo = new FlightSeatStatusTO();
		fltTo.setFlightAmSeatId(bigDecimalToInt(paxs.get("FLIGHT_AM_SEAT_ID")));
		fltTo.setSeatCode((String) paxs.get("SEAT_CODE"));
		fltTo.setSeatType((String) paxs.get("SEAT_LOCATION_TYPE"));
		fltTo.setColId(bigDecimalToInt(paxs.get("COL_ID")));
		fltTo.setRowGroupId(bigDecimalToInt(paxs.get("ROW_ID")));
		fltTo.setStatus((String) paxs.get("STATUS"));
		fltTos.add(fltTo);
		return fltTos;
	}

	/**
	 * @param fieldValue
	 * @return
	 */
	public int bigDecimalToInt(Object fieldValue) {
		int intVal = 0;
		if (fieldValue != null) {
			BigDecimal bDVal = (BigDecimal) fieldValue;
			intVal = bDVal.intValueExact();
		}
		return intVal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO#updatePaxAutoCheckinSeats(java.util
	 * .LinkedHashMap)
	 */
	@Override
	public void updatePaxAutoCheckinSeats(LinkedHashMap<Integer, Integer> blockedSeatIds) throws ModuleException {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		String updateAutoCheckin = "update t_pnr_pax_seg_auto_checkin set FLIGHT_AM_SEAT_ID = ? where PPAC_ID = ?";
		String updateFlightAmSeat = "update SM_T_FLIGHT_AM_SEAT set STATUS=? where FLIGHT_AM_SEAT_ID=?";

		PreparedStatement updatePPAC = null;
		PreparedStatement updateFAM = null;
		Connection con = null;
		try {
			con = ds.getConnection();
			updatePPAC = con.prepareStatement(updateAutoCheckin);
			updateFAM = con.prepareStatement(updateFlightAmSeat);

			for (Map.Entry<Integer, Integer> blockSeats : blockedSeatIds.entrySet()) {
				try {
					updatePPAC.setInt(1, blockSeats.getValue());
					updatePPAC.setInt(2, blockSeats.getKey());
					updatePPAC.executeUpdate();
					updateFAM.setString(1, AirinventoryCustomConstants.FlightSeatStatuses.RESERVED);
					updateFAM.setInt(2, blockSeats.getValue());
					updateFAM.executeUpdate();
				} catch (Exception e) {
					log.error("Message--" + e.getMessage());
					throw new ModuleException(e.getMessage());
				}
			}
		} catch (Exception e) {
			if (con != null) {
				try {
					log.error("Transaction is being rolled back for updatin Seat Map");
					con.rollback();
				} catch (SQLException excep) {
					log.error("Exception happend while transaction roll back" + e.getMessage());
					throw new ModuleException(excep.getMessage());
				}
			}
		} finally {
			try {
				if (updatePPAC != null) {
					updatePPAC.close();
				}
				if (updateFAM != null) {
					updateFAM.close();
				}
			} catch (SQLException e) {
				log.error("Exception happend while Commit the Auto checkin seat derive change" + e.getMessage());
				throw new ModuleException(e.getMessage());
			}
		}

	}

	@Override
	public void updateCreiditNoteforPassenger(Integer flightId) throws ModuleException {

		try {
			JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			Object params[] = { AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE,
					ReservationInternalConstants.CHAEGE_CODES.AUTOCHK, AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE,
					ReservationInternalConstants.CHAEGE_CODES.AUTOCHKR, ReservationPaxStatus.CONFIRMED,
					AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE, flightId };
			StringBuilder sqlBuilder = new StringBuilder();
			sqlBuilder.append("update  T_PNR_PAX_OND_CHARGES ");
			sqlBuilder.append("SET CHARGE_RATE_ID=(select  cr.CHARGE_RATE_ID from ");
			sqlBuilder.append("T_CHARGE_RATE cr ");
			sqlBuilder.append("where  cr.status= ? ");
			sqlBuilder.append("and cr.CHARGE_CODE= ?  )");
			sqlBuilder.append(" where PFT_ID in (select ppoc.PFT_ID  from ");
			sqlBuilder.append("t_FLIGHT f ");
			sqlBuilder.append("JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
			sqlBuilder.append("JOIN V_PNR_SEGMENT ps ON fs.FLT_SEG_ID = ps.FLT_SEG_ID ");
			sqlBuilder.append("JOIN T_PNR_PASSENGER pp ON pp.PNR = ps.PNR ");
			sqlBuilder.append("JOIN T_PNR_PAX_SEG_AUTO_CHECKIN ppsac ON pp.PNR_PAX_ID=ppsac.PNR_PAX_ID ");
			sqlBuilder.append("JOIN T_PNR_PAX_FARE ppf ON pp.PNR_PAX_ID=ppf.PNR_PAX_ID ");
			sqlBuilder.append("JOIN T_PNR_PAX_OND_CHARGES ppoc ON ppf.PPF_ID=ppoc.PPF_ID ");
			sqlBuilder.append("JOIN T_CHARGE_RATE cr ON ppoc.CHARGE_RATE_ID=cr.CHARGE_RATE_ID ");
			sqlBuilder.append("JOIN T_CHARGE ch ON ch.CHARGE_CODE=cr.CHARGE_CODE ");
			sqlBuilder.append("where  cr.status= ? ");
			sqlBuilder.append("and ppoc.CHARGE_RATE_ID is not null ");
			sqlBuilder.append("and cr.CHARGE_CODE= ? ");
			sqlBuilder.append("and ps.PNR_STATUS= ? ");
			sqlBuilder.append("and ppsac.STATUS= ? ");
			sqlBuilder.append("and f.FLIGHT_ID= ? ) ");
			int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.INTEGER };
			template.update(sqlBuilder.toString(), params, types);

		} catch (DataAccessException e) {
			throw new ModuleException(e.getMessage());
		}

	}

	@Override
	public String getAutoCheckinSeatSelectionStatus(Date departureDate, String flightNumber, String title, String firstName,
			String lastName, String pnr, String checkInSeat) {

		Object[] params = { departureDate, flightNumber, firstName, lastName, pnr };
		JdbcTemplate templete = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		StringBuilder sqlBuilder = new StringBuilder();
		String SEAT_SELECTION_SUCCESS = "SUCCESS";
		String SEAT_SELECTION_FAIL = "FAIL";
		sqlBuilder.append("select ppsac.PNR_PAX_ID as pnrPaxId ");
		sqlBuilder.append(" from t_flight f ");
		sqlBuilder.append("JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN v_PNR_SEGMENT ps on ps.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append("JOIN T_PNR_PAX_SEG_AUTO_CHECKIN  ppsac  on ps.PNR_SEG_ID=ppsac.PNR_SEG_ID ");
		sqlBuilder.append("JOIN T_PNR_PASSENGER pp on pp.PNR_PAX_ID = ppsac.PNR_PAX_ID ");
		sqlBuilder.append("JOIN SM_T_FLIGHT_AM_SEAT  fas on fas.FLIGHT_AM_SEAT_ID=ppsac.FLIGHT_AM_SEAT_ID ");
		sqlBuilder.append("JOIN SM_T_AIRCRAFT_MODEL_SEATS ams on ams.AM_SEAT_ID=fas.AM_SEAT_ID ");
		sqlBuilder.append("where f.DEPARTURE_DATE=to_timestamp(to_char( ? ,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') ");
		sqlBuilder.append("and f.FLIGHT_NUMBER=? ");
		sqlBuilder.append("and pp.FIRST_NAME=? ");
		sqlBuilder.append("and pp.LAST_NAME=? ");
		sqlBuilder.append("and pp.pnr=? ");
		sqlBuilder.append("and ( pp.TITLE=? or pp.TITLE is null ) ");
		sqlBuilder.append("and ams.SEAT_CODE=? ");
		params = new Object[] { departureDate, flightNumber, firstName, lastName, pnr, title, checkInSeat };
		List<ReservationPax> reservationPaxs = new ArrayList<ReservationPax>();
		Collection<Map<String, Object>> rsList = templete.queryForList(sqlBuilder.toString(), params);
		rsList.stream().forEach(reservationPaxMap -> mapReservationPax(reservationPaxMap, reservationPaxs));
		return reservationPaxs.size() > 0 ? SEAT_SELECTION_SUCCESS : SEAT_SELECTION_FAIL;

	}

	private String getPaxtype(Date departureDate, String flightNumber, String title, String firstName,
			String lastName, String pnr) {
		StringBuilder sqlBuilder = new StringBuilder();
		Object[] params = { departureDate, flightNumber, firstName, lastName, pnr, title };
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String paxTypeCode = "";
		sqlBuilder.append(" select pp.PAX_TYPE_CODE ");
		sqlBuilder.append(" from t_flight f ");
		sqlBuilder.append(" JOIN T_FLIGHT_SEGMENT fs ON fs.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append(" JOIN v_PNR_SEGMENT ps on ps.FLIGHT_ID=f.FLIGHT_ID ");
		sqlBuilder.append(" JOIN T_PNR_PAX_SEG_AUTO_CHECKIN  ppsac  on ps.PNR_SEG_ID=ppsac.PNR_SEG_ID ");
		sqlBuilder.append(" JOIN T_PNR_PASSENGER pp on pp.PNR_PAX_ID = ppsac.PNR_PAX_ID ");
		sqlBuilder.append(" where f.DEPARTURE_DATE=to_timestamp(to_char( ? ,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') ");
		sqlBuilder.append(" and f.FLIGHT_NUMBER=? ");
		sqlBuilder.append(" and pp.FIRST_NAME=? ");
		sqlBuilder.append(" and pp.LAST_NAME=?");
		sqlBuilder.append(" and pp.pnr=? ");
		sqlBuilder.append(" and (pp.TITLE= ? or pp.TITLE is null) ");
		paxTypeCode = (String) template.queryForObject(sqlBuilder.toString(), String.class, params);
		return paxTypeCode;
	}

	private String getPaxtype(Integer pnrPaxId) {
		StringBuilder sqlBuilder = new StringBuilder();
		Object[] params = { pnrPaxId };
		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String paxTypeCode = "";
		sqlBuilder.append(" select pp.PAX_TYPE_CODE ");
		sqlBuilder.append(" from T_PNR_PASSENGER pp ");
		sqlBuilder.append(" where pp.PNR_PAX_ID= ? ");
		paxTypeCode = (String) template.queryForObject(sqlBuilder.toString(), String.class, params);
		return paxTypeCode;
	}
	@Override
	public Integer getInfantPpsacId(Integer ppsacId){
		StringBuilder sqlBuilder = new StringBuilder();
		Integer infantPpsacId = 0;
		try {
			Object[] params = { ppsacId, AirinventoryCustomConstants.AutoCheckinConstants.ACTIVE };
			JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
			sqlBuilder.append(" SELECT inppsac.PPAC_ID ");
			sqlBuilder.append(" FROM T_PNR_PASSENGER pp ");
			sqlBuilder.append("join  T_PNR_PAX_SEG_AUTO_CHECKIN ppsac on pp.ADULT_ID=ppsac.PNR_PAX_ID ");
			sqlBuilder.append("join  T_PNR_PAX_SEG_AUTO_CHECKIN inppsac on inppsac.PNR_PAX_ID=pp.PNR_PAX_ID ");
			sqlBuilder.append("where ppsac.PPAC_ID = ? and ppsac.STATUS= ? ");
			infantPpsacId = (Integer) template.queryForObject(sqlBuilder.toString(), params, Integer.class);
		} catch (EmptyResultDataAccessException e) {
			return infantPpsacId;
		}
		return infantPpsacId;
	}

}