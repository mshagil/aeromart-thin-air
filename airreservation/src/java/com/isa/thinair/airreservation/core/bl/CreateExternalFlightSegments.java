package com.isa.thinair.airreservation.core.bl;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for persist the external flight segments before saving the reservation
 * 
 * @author malaka
 * @isa.module.command name="createExternalFlightSegments"
 */
public class CreateExternalFlightSegments extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CreateExternalFlightSegments.class);

	private SegmentBD segmentBD;

	public CreateExternalFlightSegments() {
		super();
		segmentBD = ReservationModuleUtils.getSegmentBD();
	}

	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);

		Set<ExternalPnrSegment> externalPnrSegments = reservation.getExternalReservationSegment();
		if (externalPnrSegments != null && !externalPnrSegments.isEmpty()) {
			for (ExternalPnrSegment ePnrSeg : externalPnrSegments) {
				ExternalFlightSegment externalFlightSegment = ePnrSeg.getExternalFlightSegment();
				externalFlightSegment = segmentBD.saveExternalFlightSegment(externalFlightSegment);
				ePnrSeg.setExternalFltSegId(externalFlightSegment.getExternalFlightSegId());
			}
		}
		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		log.debug("Exit execute");
		return response;
	}

}
