/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airproxy.api.model.reservation.core.OfflinePaymentInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationTemplateUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Payment gateway related business methods
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class PaymentGatewayBO {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(PaymentGatewayBO.class);

	/** Holds the tempory payment buffer size */
	private static final long TEMPORY_PAYMENT_BUFFER_SIZE = 20;

	/** Holds the ReservationPaymentDAO instance */
	private static ReservationPaymentDAO reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;

	/**
	 * Hide the constructor
	 */
	private PaymentGatewayBO() {

	}

	/**
	 * Validate Tempory Payment State Change
	 * 
	 * @param tempPaymentTnx
	 * @param destinationState
	 * @param validateDuplicatePaymentSucesses
	 * @param colUpdTempPaymentTnx
	 * @throws ModuleException
	 */
	private static void validateTemporyPaymentStateChange(TempPaymentTnx tempPaymentTnx, String destinationState,
			boolean validateDuplicatePaymentSucesses, Collection<TempPaymentTnx> colUpdTempPaymentTnx) throws ModuleException {
		if (ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE.equals(tempPaymentTnx.getStatus())) {
			if (ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_FAILURE.equals(destinationState)
					|| ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS.equals(destinationState)) {

			}
			// This case will not happen but accidently if an external client tried to force a reservation functionality
			// for a
			// Failed payment
			else {
				throw new ModuleException("airreservations.tempPayments.cannotCreatePNRForFailedPayments");
			}
			// In Some cases it was reported that RS and RF states coexist happily. This is due to Multiple Payment
			// Success Records initiated from the
			// front end so here we check for the PS status. Remember from the back end multiple PS states could occur.
		} else if (validateDuplicatePaymentSucesses
				&& ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS.equals(tempPaymentTnx.getStatus())
				&& ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS.equals(destinationState)) {
			throw new ModuleException("airreservations.tempPayments.duplicatePaymentSuccessRequest");
			// In Oct 8, 2010 (just after V2 migration) there were many cases reported in production where it ended up
			// with I->PS->PS->RS->PS->IS
			// or I->PS->PS->RS->PS->IF. This is happening from IBE.
		} else if (validateDuplicatePaymentSucesses
				&& ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS.equals(tempPaymentTnx.getStatus())
				&& ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS.equals(destinationState)) {
			throw new ModuleException("airreservations.tempPayments.duplicatePaymentSuccessRequest");
		} else {
			// Setting the new status
			tempPaymentTnx.setStatus(destinationState);
			tempPaymentTnx.setHistory(tempPaymentTnx.getHistory() + "->" + destinationState);
			colUpdTempPaymentTnx.add(tempPaymentTnx);
		}
	}

	/**
	 * Make tempory payment entry completed
	 * 
	 * @param colTnxIds
	 * @param status
	 * @param comments
	 * @param ccLastDigits
	 * @param validateDuplicatePaymentSucesses
	 * @param pnr TODO
	 * @param alias
	 * @throws ModuleException
	 */
	public static void updateTempPaymentEntries(Collection<Integer> colTnxIds, String status, String comments,
			String ccLastDigits, boolean validateDuplicatePaymentSucesses, String pnr, String alias) throws ModuleException {
		log.debug("Inside updateTempPaymentEntries");

		if (colTnxIds.size() > 0) {
			Collection<TempPaymentTnx> colTempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(colTnxIds);
			Collection<TempPaymentTnx> colUpdTempPaymentTnx = new ArrayList<TempPaymentTnx>();
			TempPaymentTnx tempPaymentTnx;

			for (TempPaymentTnx tempPaymentTnx2 : colTempPaymentTnx) {
				tempPaymentTnx = tempPaymentTnx2;

				if (comments != null) {
					if (tempPaymentTnx.getComments() != null) {
						comments = tempPaymentTnx.getComments() + comments;
					}
					tempPaymentTnx.setComments(comments);
				}
				if (ccLastDigits != null) {
					tempPaymentTnx.setLast4DigitsCC(ccLastDigits);
				}
				
				
				if (!StringUtil.isNullOrEmpty(alias)) {
					tempPaymentTnx.setAlias(alias);
				}
				
				if (pnr != null && (tempPaymentTnx.getPnr() == null || !tempPaymentTnx.getPnr().isEmpty())) {
					tempPaymentTnx.setPnr(pnr);
				}
				validateTemporyPaymentStateChange(tempPaymentTnx, status, validateDuplicatePaymentSucesses, colUpdTempPaymentTnx);
			}

			if (colUpdTempPaymentTnx.size() > 0) {
				reservationPaymentDAO.saveTempPaymentInfoAll(colUpdTempPaymentTnx);
			}
		}

		log.debug("Exit updateTempPaymentEntries");
	}
	
	/**
	 * For making temprory payment in case of Alias functionality
	 * 
	 * @param colTnxIds
	 * @param status
	 * @param comments
	 * @param ccLastDigits
	 * @param validateDuplicatePaymentSucesses
	 * @param pnr
	 * @param alias
	 * @param cardToBeSaved
	 * @throws ModuleException
	 */
	public static void updateTempPaymentEntries(Collection<Integer> colTnxIds, String status, String comments,
			String ccLastDigits, boolean validateDuplicatePaymentSucesses, String pnr, String alias, boolean saveCreditCard) throws ModuleException {
		log.debug("Inside updateTempPaymentEntries");

		if (colTnxIds.size() > 0) {
			Collection<TempPaymentTnx> colTempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(colTnxIds);
			Collection<TempPaymentTnx> colUpdTempPaymentTnx = new ArrayList<TempPaymentTnx>();
			TempPaymentTnx tempPaymentTnx;

			for (TempPaymentTnx paymentTnx : colTempPaymentTnx) {
				tempPaymentTnx = paymentTnx;

				if (comments != null) {
					if (tempPaymentTnx.getComments() != null) {
						comments = tempPaymentTnx.getComments() + comments;
					}
					tempPaymentTnx.setComments(comments);
				}

				if (ccLastDigits != null) {
					tempPaymentTnx.setLast4DigitsCC(ccLastDigits);
				}
				if (pnr != null && (tempPaymentTnx.getPnr() == null || !tempPaymentTnx.getPnr().isEmpty())) {
					tempPaymentTnx.setPnr(pnr);
				}
				if (saveCreditCard && !StringUtil.isNullOrEmpty(alias)) {
					tempPaymentTnx.setAlias(alias);
				}
				validateTemporyPaymentStateChange(tempPaymentTnx, status, validateDuplicatePaymentSucesses, colUpdTempPaymentTnx);
			}

			if (colUpdTempPaymentTnx.size() > 0) {
				reservationPaymentDAO.saveTempPaymentInfoAll(colUpdTempPaymentTnx);
			}
		}

		log.debug("Exit updateTempPaymentEntries");
	}

	/**
	 * Make tempory payment entry completed
	 * 
	 * @param colTnxIds
	 * @param status
	 * @param comments
	 * @param ccLastDigits
	 * @param validateDuplicatePaymentSucesses
	 * @param eDirhamFee
	 * @throws ModuleException
	 */
	public static void updateTempPaymentEntriesWithActualAmount(Collection<Integer> colTnxIds, String status, String comments,
			String ccLastDigits, boolean validateDuplicatePaymentSucesses, BigDecimal eDirhamFee) throws ModuleException {
		log.debug("Inside updateTempPaymentEntries");

		if (colTnxIds.size() > 0) {
			Collection<TempPaymentTnx> colTempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(colTnxIds);
			Collection<TempPaymentTnx> colUpdTempPaymentTnx = new ArrayList<TempPaymentTnx>();
			TempPaymentTnx tempPaymentTnx;

			for (TempPaymentTnx tempPaymentTnx2 : colTempPaymentTnx) {
				tempPaymentTnx = tempPaymentTnx2;

				if (comments != null) {
					if (tempPaymentTnx.getComments() != null) {
						comments = tempPaymentTnx.getComments() + comments;
					}
					tempPaymentTnx.setComments(comments);
				}

				if (ccLastDigits != null) {
					tempPaymentTnx.setLast4DigitsCC(ccLastDigits);
				}

				BigDecimal payAmount = AccelAeroCalculator.add(tempPaymentTnx.getAmount(), eDirhamFee);

				tempPaymentTnx.setAmount(payAmount);
				tempPaymentTnx.setPaymentCurrencyAmount(payAmount);

				validateTemporyPaymentStateChange(tempPaymentTnx, status, validateDuplicatePaymentSucesses, colUpdTempPaymentTnx);
			}

			if (colUpdTempPaymentTnx.size() > 0) {
				reservationPaymentDAO.saveTempPaymentInfoAll(colUpdTempPaymentTnx);
			}
		}

		log.debug("Exit updateTempPaymentEntries");
	}

	/**
	 * Get payment broker request time stamp
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public static Date getPaymentRequestTime(int paymentBrokerRefNo) throws ModuleException {

		TempPaymentTnx tempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(paymentBrokerRefNo);

		return tempPaymentTnx.getPaymentTimeStamp();
	}

	/**
	 * Load TempPaymentTnx
	 * 
	 * @param tnxId
	 * @return
	 * @throws ModuleException
	 */
	public static TempPaymentTnx loadTempPayment(int tnxId) throws ModuleException {
		return reservationPaymentDAO.getTempPaymentInfo(tnxId);
	}

	/**
	 * Get temp payment amount
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getTempPaymentAmount(int paymentBrokerRefNo) throws ModuleException {

		TempPaymentTnx tempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(paymentBrokerRefNo);

		return tempPaymentTnx.getAmount();
	}

	/**
	 * Update tempory payment entries states
	 * 
	 * @param colTnxIds
	 * @param status
	 * @throws ModuleException
	 */
	public static void updateTempPaymentEntriesForBulkRecords(Collection<Integer> colTnxIds, String status)
			throws ModuleException {
		log.debug("Inside updateTempPaymentEntriesForBulkRecords");
		Collection<Integer> colTmpTnxIds = new ArrayList<Integer>();
		Integer tptId;

		for (Integer integer : colTnxIds) {
			tptId = integer;
			colTmpTnxIds.add(tptId);

			if (colTmpTnxIds.size() == TEMPORY_PAYMENT_BUFFER_SIZE) {
				updateTempPaymentEntries(colTmpTnxIds, status, null, null, false, null, null);
				colTmpTnxIds = new ArrayList<Integer>();
			}
		}

		if (colTmpTnxIds.size() > 0) {
			updateTempPaymentEntries(colTmpTnxIds, status, null, null, false, null, null);
		}

		log.debug("Exit updateTempPaymentEntriesForBulkRecords");
	}

	/**
	 * Record tempory payment
	 * 
	 * @param reservationContactInfo
	 * @param colPaymentInfo
	 * @param credentialsDTO
	 * @param isCredit
	 * @param updateTemporyPaymentRef
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, CardPaymentInfo> recordTemporyPaymentEntry(ReservationContactInfo reservationContactInfo,
			Collection<PaymentInfo> colPaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit,
			boolean updateTemporyPaymentRef, boolean isOfflinePayment) throws ModuleException {
		log.debug("Inside recordTemporyPayment");

		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		Collection<CardPaymentInfo> colCardPaymentInfo = new ArrayList<CardPaymentInfo>();
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfo;

		while (itColPaymentInfo.hasNext()) {
			paymentInfo = itColPaymentInfo.next();

			// Card Payment
			if (paymentInfo instanceof CardPaymentInfo) {
				cardPaymentInfo = (CardPaymentInfo) paymentInfo;

				PaymentGatewayBO.cardPaymentInfoCompressor(cardPaymentInfo, colCardPaymentInfo);
			}
		}

		Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
		Map<Integer, CardPaymentInfo> mapTmpPaymentIdAndCardInfo = new HashMap<Integer, CardPaymentInfo>();
		Integer temporyPaymentId;
		while (itColCardPaymentInfo.hasNext()) {
			cardPaymentInfo = itColCardPaymentInfo.next();

			temporyPaymentId = recordTemporyPaymentEntry(reservationContactInfo, cardPaymentInfo, credentialsDTO, isCredit,
					isOfflinePayment);

			cardPaymentInfo.setTemporyPaymentId(temporyPaymentId.intValue());

			mapTmpPaymentIdAndCardInfo.put(temporyPaymentId, cardPaymentInfo);
		}

		// If any card payments exist setting the tempory payment id
		if (updateTemporyPaymentRef && colCardPaymentInfo.size() > 0) {
			// Capture the card payment
			itColCardPaymentInfo = colCardPaymentInfo.iterator();
			CardPaymentInfo cardPaymentInfoObj;

			while (itColCardPaymentInfo.hasNext()) {
				cardPaymentInfo = itColCardPaymentInfo.next();

				itColPaymentInfo = colPaymentInfo.iterator();
				while (itColPaymentInfo.hasNext()) {
					paymentInfo = itColPaymentInfo.next();

					// If its a card payment
					if (paymentInfo instanceof CardPaymentInfo) {
						cardPaymentInfoObj = (CardPaymentInfo) paymentInfo;

						// If match found
						if (CardPaymentInfo.compare(cardPaymentInfo, cardPaymentInfoObj)) {
							cardPaymentInfoObj.setTemporyPaymentId(cardPaymentInfo.getTemporyPaymentId());
						}
					}
				}
			}
		}

		log.debug("Exit recordTemporyPayment");
		return mapTmpPaymentIdAndCardInfo;
	}

	/**
	 * Get the TempPaymentTnx for giben id
	 * 
	 * @param tnxId
	 * @return
	 */
	public static TempPaymentTnx getTempPaymentTnx(int tnxId) {
		return reservationPaymentDAO.getTempPaymentInfo(tnxId);
	}

	/**
	 * Return the temporary payments for reservation
	 * 
	 * @param pnr
	 * @return
	 */
	public static Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr) {
		return reservationPaymentDAO.getTempPaymentsForReservation(pnr);
	}

	/**
	 * 
	 * @param pnr
	 * @param status
	 * @param productType
	 * @return
	 */
	public static Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr, String status, char productType) {
		return reservationPaymentDAO.getTempPaymentsForReservation(pnr, status, productType);
	}

	/**
	 * 
	 * @param reference
	 * @return
	 */
	public static boolean isExternalReferenceExist(String reference) {
		return reservationPaymentDAO.isPaymentReferenceExist(reference);
	}

	/**
	 * 
	 * @param reference
	 * @return
	 */
	public static Map<String, String> getAgentPayments(String reference) {
		return reservationPaymentDAO.getAgentPayments(reference);
	}

	/**
	 * 
	 * @param pnr
	 * @param productType
	 * @return
	 */
	public static Collection<TempPaymentTnx> getTempPaymentsForReservation(String pnr, char productType) {
		return reservationPaymentDAO.getTempPaymentsForReservation(pnr, productType);
	}

	/**
	 * Save the given TempPaymentTnx
	 * 
	 * @param tempPaymentTnx
	 */
	public static void saveTempPaymentTnx(TempPaymentTnx tempPaymentTnx) {
		reservationPaymentDAO.saveTempPaymentInfo(tempPaymentTnx);
	}

	/**
	 * Record Payment
	 * 
	 * @param reservationContactInfo
	 * @param cardPaymentInfo
	 * @param credentialsDTO
	 * @param isCredit
	 * @throws ModuleException
	 */
	private static Integer recordTemporyPaymentEntry(ReservationContactInfo reservationContactInfo,
			CardPaymentInfo cardPaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit, boolean isOfflinePayment)
			throws ModuleException {
		reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;

		// Inserting a record to T_TEMP_PAYMENT_TNX
		TempPaymentTnx tempPaymentTnx = new TempPaymentTnx();

		tempPaymentTnx.setTelephoneNo(BeanUtils.nullHandler(reservationContactInfo.getPhoneNo()));
		tempPaymentTnx.setMobileNo(BeanUtils.nullHandler(reservationContactInfo.getMobileNo()));
		tempPaymentTnx.setAmount(cardPaymentInfo.getTotalAmount());

		PayCurrencyDTO payCurrencyDTO = cardPaymentInfo.getPayCurrencyDTO();
		tempPaymentTnx.setPaymentCurrencyAmount(payCurrencyDTO.getTotalPayCurrencyAmount());
		tempPaymentTnx.setPaymentCurrencyCode(payCurrencyDTO.getPayCurrencyCode());

		tempPaymentTnx.setContactPerson(BeanUtils.nullHandler(reservationContactInfo.getFirstName()) + " "
				+ BeanUtils.nullHandler(reservationContactInfo.getLastName()));
		tempPaymentTnx.setCustomerId(credentialsDTO.getCustomerId());
		tempPaymentTnx.setEmailAddress(reservationContactInfo.getEmail());
		tempPaymentTnx.setUserId(credentialsDTO.getUserId());
		tempPaymentTnx.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		tempPaymentTnx.setPaymentTimeStamp(new Date());
		tempPaymentTnx.setLast4DigitsCC(BeanUtils.getLast4Digits(cardPaymentInfo.getNo()));
		tempPaymentTnx.setPnr(cardPaymentInfo.getPnr());
		tempPaymentTnx.setStatus(ReservationInternalConstants.TempPaymentTnxTypes.INITIATED);
		tempPaymentTnx.setHistory(tempPaymentTnx.getStatus());
		if (isOfflinePayment) {
			tempPaymentTnx.setIsOfflinePayment("Y");
		}

		// charith
		// To avoid payments without currency conversion
		if (!CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY)
				.equals(tempPaymentTnx.getPaymentCurrencyCode())
				&& tempPaymentTnx.getAmount().doubleValue() == tempPaymentTnx.getPaymentCurrencyAmount().doubleValue()) {
			throw new ModuleException("airreservations.temporyPayment.currencyConvertionFailed");
		}
		if (CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY)
				.equals(tempPaymentTnx.getPaymentCurrencyCode())
				&& tempPaymentTnx.getAmount().doubleValue() != tempPaymentTnx.getPaymentCurrencyAmount().doubleValue()) {
			throw new ModuleException("airreservations.temporyPayment.currencyConvertionFailed");
		}
		// Setting the IP Address if specified
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			tempPaymentTnx.setIpAddress(credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Is Credit
		if (isCredit) {
			tempPaymentTnx.setTnxType(ReservationInternalConstants.TnxTypes.CREDIT);
		}
		// Is Debit
		else {
			tempPaymentTnx.setTnxType(ReservationInternalConstants.TnxTypes.DEBIT);
		}

		reservationPaymentDAO.saveTempPaymentInfo(tempPaymentTnx);

		return tempPaymentTnx.getTnxId();
	}

	/**
	 * Audit failures
	 * 
	 * @param creditCardPayment
	 * @param operationType
	 * @param errorDescription
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private static void auditFailures(CreditCardPayment creditCardPayment, String operationType, String errorDescription,
			CredentialsDTO credentialsDTO) throws ModuleException {
		ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();

		reservationBD.auditCreditCardFailures(creditCardPayment.getPnr(), new BigDecimal(creditCardPayment.getAmount()),
				operationType, errorDescription, credentialsDTO.getTrackInfoDTO());
	}

	public static Object[] getRequestData(OfflinePaymentInfo offlinePaymentInfo, Integer temporyPaymentId,
			ReservationContactInfo contactInfo) throws ModuleException {
		return getRequestData(offlinePaymentInfo, temporyPaymentId, contactInfo, null);
	}

	public static Object[] getRequestData(OfflinePaymentInfo offlinePaymentInfo, Integer temporyPaymentId,
			ReservationContactInfo contactInfo, Date ohdReleaseDate) throws ModuleException {
		PaymentBrokerBD paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();
		String authorizationId = "";
		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();

		ipgRequestDTO.setDefaultCarrierName(AppSysParamsUtil.getDefaultCarrierCode());
		ipgRequestDTO.setIpgIdentificationParamsDTO(offlinePaymentInfo.getIpgIdentificationParamsDTO());
		ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_XBE);
		ipgRequestDTO.setPnr(contactInfo.getPnr());
		ipgRequestDTO.setAmount(offlinePaymentInfo.getPayCurrencyDTO().getTotalPayCurrencyAmount().toPlainString());

		ipgRequestDTO.setContactMobileNumber(contactInfo.getMobileNo());
		ipgRequestDTO.setEmail(contactInfo.getEmail());
		ipgRequestDTO.setContactFirstName(contactInfo.getFirstName());
		ipgRequestDTO.setContactLastName(contactInfo.getLastName());
		ipgRequestDTO.setApplicationTransactionId(temporyPaymentId);
		ipgRequestDTO.setPreferredLanguage(contactInfo.getPreferredLanguage());

		if (ohdReleaseDate != null) {
			ipgRequestDTO.setInvoiceExpirationTime(ohdReleaseDate);
		}

		IPGRequestResultsDTO resultDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);
		if (resultDTO.getPostDataMap() != null && !resultDTO.getPostDataMap().isEmpty()) {
			offlinePaymentInfo.setPayReference((String) resultDTO.getPostDataMap().get(PaymentConstants.REFFATOURATI));
		}
		DefaultServiceResponse sr = new DefaultServiceResponse(true);
		return new Object[] { sr.getResponseCode(), authorizationId, resultDTO.getBillNumber(),
				resultDTO.getBillNumberLabelKey() };
	}

	/**
	 * } Performs credit payment with blocking
	 * 
	 * @param cardPaymentInfo
	 * @param blockIds
	 * @param credentialsDTO
	 * @param pnr TODO
	 * @return
	 * @throws ModuleException
	 */
	public static Object[] creditPayment(CardPaymentInfo cardPaymentInfo, Collection<TempSegBcAlloc> blockIds,
			Collection<Integer> colTnxIds, CredentialsDTO credentialsDTO, List<CardDetailConfigDTO> cardDetailConfigData,
			TravelDTO travelData, String pnr) throws ModuleException {
		log.debug("Inside creditPayment");

		PaymentBrokerBD paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();

		// Create credit card payment
		CreditCardPayment creditCardPayment = TOAssembler.createCreditCardPayment(cardPaymentInfo, false, travelData);
		String operationType = ReservationTemplateUtils.AUDIT_PAYMENT_TEXT;
		String authorizationId = "";
		String userIPAddress = null;
		if (credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null) {
			userIPAddress = credentialsDTO.getTrackInfoDTO().getIpAddress();
		}
		// TODO add to assembler
		creditCardPayment.setUserIP(userIPAddress);
		// Send CC payment request and capture the response
		ServiceResponce serviceResponce = paymentBrokerBD.charge(creditCardPayment, creditCardPayment.getPnr(),
				creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode(), cardDetailConfigData);

		if (serviceResponce != null) {
			String ccLast4Digits = (String) serviceResponce.getResponseParam(PaymentConstants.PARAM_CC_LAST_DIGITS);
			if (!serviceResponce.isSuccess()) {
				cardPaymentInfo.setSuccess(false);
				// Updating the payment(s) information
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, ccLast4Digits, pnr, null);
				Object errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
				Object errorMsg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);

				// Audit the failurs
				auditFailures(creditCardPayment, operationType, errorCode + " " + errorMsg, credentialsDTO);

				ReservationBO.releaseBlockedSeats(blockIds);
				throw new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR,
						ReservationTemplateUtils.getStandardPaymentErrorMap(errorCode, errorMsg));
			} else {
				Object msg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID);

				authorizationId = BeanUtils.nullHandler(msg);
				// Updating the payment(s) information
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS, null, ccLast4Digits, pnr, null);
			}
		} else {
			cardPaymentInfo.setSuccess(false);
			// Updating the payment(s) information
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, null, null, null);
			// Audit the failurs
			auditFailures(creditCardPayment, operationType, ReservationTemplateUtils.NO_RESPONSE_TEXT, credentialsDTO);
			ReservationBO.releaseBlockedSeats(blockIds);
			throw new ModuleException("airreservations.arg.cc.noresponse");
		}

		log.debug("Exit creditPayment");
		return new Object[] { serviceResponce.getResponseCode(), operationType, authorizationId };

	}

	/**
	 * Performs credit payment
	 * 
	 * @param cardPaymentInfo
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Object[] creditPayment(CardPaymentInfo cardPaymentInfo, Collection<Integer> colTnxIds,
			CredentialsDTO credentialsDTO, List<CardDetailConfigDTO> cardDetailConfigData, TravelDTO travelData)
			throws ModuleException {
		log.debug("Inside creditPayment");
		PaymentBrokerBD paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();

		// Create credit card payment
		CreditCardPayment creditCardPayment = TOAssembler.createCreditCardPayment(cardPaymentInfo, false, travelData);
		String operationType = ReservationTemplateUtils.AUDIT_PAYMENT_TEXT;
		String authorizationId = "";
		String userIPAddress = null;
		if (credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null) {
			userIPAddress = credentialsDTO.getTrackInfoDTO().getIpAddress();
		}
		// TODO add to assembler
		creditCardPayment.setUserIP(userIPAddress);
		// Send CC payment request and capture the response
		ServiceResponce serviceResponce = paymentBrokerBD.charge(creditCardPayment, creditCardPayment.getPnr(),
				creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode(), cardDetailConfigData);

		if (serviceResponce != null) {
			String ccLast4Digits = (String) serviceResponce.getResponseParam(PaymentConstants.PARAM_CC_LAST_DIGITS);
			if (!serviceResponce.isSuccess()) {
				cardPaymentInfo.setSuccess(false);

				// Updating the payment(s) information
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, ccLast4Digits, null, null);
				Object errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
				Object errorMsg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);

				// Audit the failurs
				auditFailures(creditCardPayment, operationType, errorCode + " " + errorMsg, credentialsDTO);

				throw new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR,
						ReservationTemplateUtils.getStandardPaymentErrorMap(errorCode, errorMsg));
			} else {

				authorizationId = BeanUtils.nullHandler(cardPaymentInfo.getAuthorizationId());

				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS, null, ccLast4Digits, null, null);
			}
		} else {
			cardPaymentInfo.setSuccess(false);
			// Updating the payment(s) information
			ReservationModuleUtils.getReservationBD()
					.updateTempPaymentEntry(colTnxIds, ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE,
							null, null, null, null);
			// Audit the failurs
			auditFailures(creditCardPayment, operationType, ReservationTemplateUtils.NO_RESPONSE_TEXT, credentialsDTO);
			throw new ModuleException("airreservations.arg.cc.noresponse");
		}

		log.debug("Exit creditPayment");
		return new Object[] { serviceResponce.getResponseCode(), operationType, authorizationId };
	}

	/**
	 * Performs debit payment
	 * 
	 * @param cardPaymentInfo
	 * @param colTnxIds
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	public static Object[] debitPayment(CardPaymentInfo cardPaymentInfo, Collection<Integer> colTnxIds,
			CredentialsDTO credentialsDTO, boolean setPreviousCreditCardPayment) throws ModuleException {
		log.debug("Inside debitPayment");
		PaymentBrokerBD paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();

		// Create credit card payment
		CreditCardPayment creditCardPayment = TOAssembler.createCreditCardPayment(cardPaymentInfo, setPreviousCreditCardPayment,
				null);
		String operationType = ReservationTemplateUtils.AUDIT_REFUND_TEXT;
		String authorizationId = "";

		if (cardPaymentInfo.getOldCCRecordId() != -1 ) {
			reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
			// Getting the original Object
			CreditCardDetail creditCardDetail = reservationPaymentDAO.getCreditCardDetail(cardPaymentInfo.getOldCCRecordId());

			Collection<CreditCardDetail> colCreditCardDetail = reservationPaymentDAO.getCreditCardDetails(creditCardDetail
					.getPaymentBrokerRefNo());
			BigDecimal totalAmount = getTotalPreviousAmount(colCreditCardDetail, creditCardPayment);

			PreviousCreditCardPayment previousCCPayment = new PreviousCreditCardPayment();
			previousCCPayment.setNoFirstDigits(creditCardDetail.getNoFirstDigits());
			previousCCPayment.setNoLastDigits(creditCardDetail.getNoLastDigits());
			previousCCPayment.setName(creditCardDetail.getName());
			previousCCPayment.setEDate(creditCardDetail.getEDate());
			previousCCPayment.setSecurityCode(creditCardDetail.getSecurityCode());
			previousCCPayment.setPnr(creditCardDetail.getPnr());
			previousCCPayment.setPaymentBrokerRefNo(creditCardDetail.getPaymentBrokerRefNo());
			previousCCPayment.setTotalAmount(totalAmount);

			creditCardPayment.setPreviousCCPayment(previousCCPayment);
		} else if (cardPaymentInfo.isOfflinePayment()) {
			PreviousCreditCardPayment previousCCPayment = new PreviousCreditCardPayment();
			previousCCPayment.setPaymentBrokerRefNo(cardPaymentInfo.getPaymentBrokerRefNo());
			previousCCPayment.setPnr(cardPaymentInfo.getPnr());
			previousCCPayment.setTotalAmount(cardPaymentInfo.getTotalAmount());	
			creditCardPayment.setPreviousCCPayment(previousCCPayment);
		}

		// Send CC payment request and capture the response
		ServiceResponce serviceResponce = paymentBrokerBD.refund(creditCardPayment, creditCardPayment.getPnr(),
				creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode());

		if (serviceResponce != null) {
			if (!serviceResponce.isSuccess()) {
				cardPaymentInfo.setSuccess(false);
				// Updating the payment(s) information
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, null, null, null);
				Object errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
				Object errorMsg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);

				if (isNonExternalPaymentBrokerError(errorMsg)) {
					ModuleException moduleException = new ModuleException(errorMsg.toString());

					// Audit the failurs
					auditFailures(creditCardPayment, operationType, moduleException.getMessageString(), credentialsDTO);

					throw moduleException;
				} else {
					// Audit the failurs
					auditFailures(creditCardPayment, operationType, errorCode + " " + errorMsg, credentialsDTO);

					throw new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR,
							ReservationTemplateUtils.getStandardPaymentErrorMap(errorCode, errorMsg));
				}
			} else {
				Object objOperation = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);

				// Matching to the right operation type
				if (PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND.equals(BeanUtils.nullHandler(objOperation))) {
					operationType = ReservationTemplateUtils.AUDIT_REVERSE_TEXT;
				}

				Object objAuthorizationId = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID);

				authorizationId = BeanUtils.nullHandler(objAuthorizationId);
				// Updating the payment(s) information
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS, null, null, null, null);
			}
		} else {
			cardPaymentInfo.setSuccess(false);
			// Updating the payment(s) information
			ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
					ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, null, null, null, null);
			// Audit the failurs
			auditFailures(creditCardPayment, operationType, ReservationTemplateUtils.NO_RESPONSE_TEXT, credentialsDTO);
			throw new ModuleException("airreservations.arg.cc.noresponse");
		}

		log.debug("Exit debitPayment");
		return new Object[] { serviceResponce.getResponseCode(), operationType, authorizationId };
	}

	/**
	 * Returns the total previous amount
	 * 
	 * @param colCreditCardDetail
	 * @param creditCardPayment
	 * @return
	 * @throws ModuleException
	 */
	private static BigDecimal getTotalPreviousAmount(Collection<CreditCardDetail> colCreditCardDetail,
			CreditCardPayment creditCardPayment) throws ModuleException {

		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		ReservationTnxDAO reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		String requiredCurrencyCode = creditCardPayment.getCurrency();
		CreditCardDetail cardDetail;
		ReservationTnx reservationTnx;
		ReservationPaxExtTnx reservtionExtTnx;

		if (baseCurrencyCode.equals(requiredCurrencyCode)) {
			Iterator<CreditCardDetail> itColCreditCardDetail = colCreditCardDetail.iterator();
			while (itColCreditCardDetail.hasNext()) {
				cardDetail = itColCreditCardDetail.next();
				if (cardDetail.getTransactionId() != 0) {
					reservationTnx = reservationTnxDAO.getTransaction(cardDetail.getTransactionId());
					totalAmount = AccelAeroCalculator.add(totalAmount, reservationTnx.getAmount().abs());
				}

				if (cardDetail.getExtTransactionId() != 0) {
					reservtionExtTnx = reservationTnxDAO.getExternalTransaction(cardDetail.getExtTransactionId());
					totalAmount = AccelAeroCalculator.add(totalAmount, reservtionExtTnx.getAmount().abs());
				}
			}

		} else {
			// Collection<Integer> tnxIds = new ArrayList<Integer>();
			Iterator<CreditCardDetail> itColCreditCardDetail = colCreditCardDetail.iterator();
			while (itColCreditCardDetail.hasNext()) {

				cardDetail = itColCreditCardDetail.next();
				if (cardDetail.getTransactionId() != 0) {
					reservationTnx = reservationTnxDAO.getTransaction(cardDetail.getTransactionId());
					totalAmount = AccelAeroCalculator.add(totalAmount, reservationTnx.getPayCurrencyAmount().abs());
				}

				if (cardDetail.getExtTransactionId() != 0) {
					reservtionExtTnx = reservationTnxDAO.getExternalTransaction(cardDetail.getExtTransactionId());
					totalAmount = AccelAeroCalculator.add(totalAmount, reservtionExtTnx.getAmountPayCur().abs());
				}
			}
		}

		return totalAmount;
	}

	// private BigDecimal getBaseAmountFromTnx() {
	// ReservationTnxDAO reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	// return null;
	// }

	/**
	 * Performs reverse payment
	 * 
	 * @param cardPaymentInfo
	 * @param credentialsDTO
	 * @param triggeredExceptionCode
	 * @param colTnxIds
	 * @throws ModuleException
	 */
	private static void reversePayment(CardPaymentInfo cardPaymentInfo, CredentialsDTO credentialsDTO,
			String triggeredExceptionCode, Collection<Integer> colTnxIds) throws ModuleException {
		log.debug("Inside reversePayment");

		if (cardPaymentInfo.isSuccess()) {
			PaymentBrokerBD paymentBrokerBD = ReservationModuleUtils.getPaymentBrokerBD();

			// PreviousCreditCardPayment will set by passing true
			CreditCardPayment creditCardPayment = TOAssembler.createCreditCardPayment(cardPaymentInfo, true, null);

			// Send CC payment request and capture the response
			ServiceResponce serviceResponce = paymentBrokerBD.reverse(creditCardPayment, creditCardPayment.getPnr(),
					creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode());

			if (serviceResponce != null) {
				if (!serviceResponce.isSuccess()) {
					// Updating the payment(s) information
					ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
							ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE, null, null, null, null);

					Object errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
					Object errorMsg = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);

					if (isNonExternalPaymentBrokerError(errorMsg)) {
						ModuleException triggeredME = new ModuleException(triggeredExceptionCode);
						ModuleException reverseME = new ModuleException(errorMsg.toString());

						// Audit the failurs
						auditFailures(creditCardPayment, ReservationTemplateUtils.AUDIT_REVERSE_TEXT,
								triggeredME.getMessageString() + " / " + reverseME.getMessageString(), credentialsDTO);

						throw triggeredME;
					} else {
						// Audit the failurs
						auditFailures(creditCardPayment, ReservationTemplateUtils.AUDIT_REVERSE_TEXT, errorCode + " " + errorMsg,
								credentialsDTO);

						throw new ModuleException(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR,
								ReservationTemplateUtils.getStandardPaymentErrorMap(errorCode, errorMsg));
					}
				}
			} else {
				// Updating the payment(s) information
				ReservationModuleUtils.getReservationBD().updateTempPaymentEntry(colTnxIds,
						ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE, null, null, null, null);
				// Audit the failurs
				auditFailures(creditCardPayment, ReservationTemplateUtils.AUDIT_REVERSE_TEXT,
						ReservationTemplateUtils.NO_RESPONSE_TEXT, credentialsDTO);
				throw new ModuleException("airreservations.arg.cc.noresponse");
			}
		}

		log.debug("Exit reversePayment");
	}

	/**
	 * Is a non external payment broker error
	 * 
	 * @param errorMsg
	 * @return
	 */
	private static boolean isNonExternalPaymentBrokerError(Object errorMsg) {
		String strErrorMsg = BeanUtils.nullHandler(errorMsg);

		if (PaymentBrokerInternalConstants.MessageCodes.REVERSE_OPERATION_NOT_SUPPORTED.equals(strErrorMsg)
				|| PaymentBrokerInternalConstants.MessageCodes.DIFFERENT_PAYMENT_GATEWAY_TRANSACTION.equals(strErrorMsg)
				|| PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND.equals(strErrorMsg)
				|| PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR.equals(strErrorMsg)) {
			return true;
		}

		return false;
	}

	/**
	 * Perform the reverse payment
	 * 
	 * @param colPaymentInfo
	 * @param credentialsDTO
	 * @param triggeredExceptionCode
	 * @param colTnxIds
	 * @throws ModuleException
	 */
	public static void performReversePayment(Collection<PaymentInfo> colPaymentInfo, CredentialsDTO credentialsDTO,
			String triggeredExceptionCode, Collection<Integer> colTnxIds) throws ModuleException {
		log.debug("Inside performReversePayment");

		Collection<CardPaymentInfo> colCardPaymentInfo = new ArrayList<CardPaymentInfo>();
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfo;

		while (itColPaymentInfo.hasNext()) {
			paymentInfo = itColPaymentInfo.next();

			// Card Payment
			if (paymentInfo instanceof CardPaymentInfo) {
				cardPaymentInfo = (CardPaymentInfo) paymentInfo;

				PaymentGatewayBO.cardPaymentInfoCompressor(cardPaymentInfo, colCardPaymentInfo);
			}
		}

		Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
		while (itColCardPaymentInfo.hasNext()) {
			cardPaymentInfo = itColCardPaymentInfo.next();
			reversePayment(cardPaymentInfo, credentialsDTO, triggeredExceptionCode, colTnxIds);
		}

		log.debug("Exit performReversePayment");
	}

	/**
	 * Update object references
	 * 
	 * @param cardPaymentInfoR
	 * @param colPaymentInfo
	 * @param paymentBrokerRefNo
	 * @param operationType
	 */
	public static void updatePaymentBrokerReferences(CardPaymentInfo cardPaymentInfo, Collection<PaymentInfo> colPaymentInfo,
			int paymentBrokerRefNo, String operationType, String authorizationId) {
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfoObj;
		while (itColPaymentInfo.hasNext()) {
			paymentInfo = itColPaymentInfo.next();

			// If its a card payment
			if (paymentInfo instanceof CardPaymentInfo) {
				cardPaymentInfoObj = (CardPaymentInfo) paymentInfo;

				if (CardPaymentInfo.compare(cardPaymentInfo, cardPaymentInfoObj)) {
					cardPaymentInfoObj.setPaymentBrokerRefNo(paymentBrokerRefNo);
					cardPaymentInfoObj.setSuccess(true);
					cardPaymentInfoObj.setOperationType(operationType);
					cardPaymentInfoObj.setAuthorizationId(authorizationId);
				}
			}
		}
	}

	/**
	 * Update object references
	 * 
	 * @param mapTnxIds
	 * @param colPaymentInfo
	 * @throws ModuleException
	 */
	public static void updateTemporyPaymentReferences(Map<Integer, CardPaymentInfo> mapTnxIds,
			Collection<PaymentInfo> colPaymentInfo) throws ModuleException {
		Iterator<PaymentInfo> itColPaymentInfo = colPaymentInfo.iterator();
		Iterator<Integer> itMapTnxIds;
		PaymentInfo paymentInfo;
		CardPaymentInfo cardPaymentInfoObj;
		CardPaymentInfo cardPaymentInfo;
		Integer temporyPayRef;
		boolean updatedReferences = false;

		while (itColPaymentInfo.hasNext()) {
			paymentInfo = itColPaymentInfo.next();

			// If its a card payment
			if (paymentInfo instanceof CardPaymentInfo) {
				cardPaymentInfoObj = (CardPaymentInfo) paymentInfo;

				itMapTnxIds = mapTnxIds.keySet().iterator();
				while (itMapTnxIds.hasNext()) {
					temporyPayRef = itMapTnxIds.next();
					cardPaymentInfo = mapTnxIds.get(temporyPayRef);

					// Found the old credit card number
					if (CardPaymentInfo.compare(cardPaymentInfo, cardPaymentInfoObj)) {
						cardPaymentInfoObj.setTemporyPaymentId(temporyPayRef);
						cardPaymentInfoObj.setPaymentBrokerRefNo(cardPaymentInfo.getPaymentBrokerRefNo());

						if (cardPaymentInfo.isSuccess()) {
							cardPaymentInfoObj.setSuccess(true);
							updatedReferences = true;
						}
					}
				}
			}
		}

		if (!updatedReferences) {
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}
	}

	/**
	 * Compress card payment info objects
	 * 
	 * @param cardPaymentInfo
	 * @param colCardPaymentInfo
	 * @return
	 */
	public static Collection<CardPaymentInfo> cardPaymentInfoCompressor(CardPaymentInfo cardPaymentInfo,
			Collection<CardPaymentInfo> colCardPaymentInfo) {
		CardPaymentInfo cloneCardPaymentInfo = (CardPaymentInfo) cardPaymentInfo.clone();

		Iterator<CardPaymentInfo> itColCardPaymentInfo = colCardPaymentInfo.iterator();
		CardPaymentInfo cardPayInfo;
		PayCurrencyDTO payCurrencyDTO;
		PayCurrencyDTO clonePayCurrencyDTO;
		boolean newRecord = true;

		while (itColCardPaymentInfo.hasNext()) {
			cardPayInfo = itColCardPaymentInfo.next();

			if (CardPaymentInfo.compare(cardPayInfo, cloneCardPaymentInfo)) {
				newRecord = false;
				cardPayInfo.setTotalAmount(AccelAeroCalculator.add(cardPayInfo.getTotalAmount(),
						cloneCardPaymentInfo.getTotalAmount()));
				payCurrencyDTO = cardPayInfo.getPayCurrencyDTO();
				clonePayCurrencyDTO = cloneCardPaymentInfo.getPayCurrencyDTO();

				payCurrencyDTO.setTotalPayCurrencyAmount(AccelAeroCalculator.add(payCurrencyDTO.getTotalPayCurrencyAmount(),
						clonePayCurrencyDTO.getTotalPayCurrencyAmount()));

				break;
			}
		}

		// If its a newly created record
		if (newRecord) {
			colCardPaymentInfo.add(cloneCardPaymentInfo);
		}

		return colCardPaymentInfo;
	}

	/**
	 * This is specific method to handle multiple responses This is happening due to browser back button or multiple
	 * responses from the gateway We will initiate refund if there is successful payment when first payment response
	 * status is fail
	 * 
	 * @param transactionID
	 * @return
	 * @throws ModuleException
	 */
	public static boolean processMultiplePaymentResponses(int transactionID) throws ModuleException {
		boolean isProcess = false;
		TempPaymentTnx tempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(transactionID);
		if (tempPaymentTnx != null) {
			/* If first payment status is fail */
			if (ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE.equals(tempPaymentTnx.getStatus())
					&& ReservationInternalConstants.TempPaymentInconsistentStatusHistoryTypes.I_PF.equals(tempPaymentTnx
							.getHistory())) {
				/* Update payment status is PS */
				if (log.isDebugEnabled()) {
					log.debug("Updating payment status PNR" + tempPaymentTnx.getPnr());
					log.debug("Merchant ref:" + tempPaymentTnx.getTnxId());
				}
				Collection<Integer> colPaymentTnxIds = new ArrayList<Integer>();
				colPaymentTnxIds.add(transactionID);
				String status = ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_SUCCESS;
				tempPaymentTnx.setStatus(status);
				tempPaymentTnx.setComments(tempPaymentTnx.getComments() != null ? tempPaymentTnx.getComments() : ""
						+ "Moving I->PF->PS");
				tempPaymentTnx.setHistory(tempPaymentTnx.getHistory() + "->" + status);
				reservationPaymentDAO.saveTempPaymentInfo(tempPaymentTnx);
				isProcess = true;
			}
		}

		return isProcess;
	}

	/**
	 * Get temp payment currency amount
	 * 
	 * @param paymentBrokerRefNo
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getTempPaymentCurrencyAmount(int paymentBrokerRefNo) {
		TempPaymentTnx tempPaymentTnx = reservationPaymentDAO.getTempPaymentInfo(paymentBrokerRefNo);
		return tempPaymentTnx.getPaymentCurrencyAmount();
	}

	public static Integer recordTemporyPaymentEntryForOffline(ReservationContactInfo reservationContactInfo,
			OfflinePaymentInfo offlinePaymentInfo, CredentialsDTO credentialsDTO, boolean isCredit, boolean isOfflinePayment,
			String pnr) throws ModuleException {
		reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;

		// Inserting a record to T_TEMP_PAYMENT_TNX
		TempPaymentTnx tempPaymentTnx = new TempPaymentTnx();

		tempPaymentTnx.setTelephoneNo(BeanUtils.nullHandler(reservationContactInfo.getPhoneNo()));
		tempPaymentTnx.setMobileNo(BeanUtils.nullHandler(reservationContactInfo.getMobileNo()));
		tempPaymentTnx.setAmount(offlinePaymentInfo.getTotalAmount());

		PayCurrencyDTO payCurrencyDTO = offlinePaymentInfo.getPayCurrencyDTO();
		tempPaymentTnx.setPaymentCurrencyAmount(payCurrencyDTO.getTotalPayCurrencyAmount());
		tempPaymentTnx.setPaymentCurrencyCode(payCurrencyDTO.getPayCurrencyCode());

		tempPaymentTnx.setContactPerson(BeanUtils.nullHandler(reservationContactInfo.getFirstName()) + " "
				+ BeanUtils.nullHandler(reservationContactInfo.getLastName()));
		tempPaymentTnx.setCustomerId(credentialsDTO.getCustomerId());
		tempPaymentTnx.setEmailAddress(reservationContactInfo.getEmail());
		tempPaymentTnx.setUserId(credentialsDTO.getUserId());
		tempPaymentTnx.setSalesChannelCode(credentialsDTO.getSalesChannelCode());
		tempPaymentTnx.setPaymentTimeStamp(new Date());
		tempPaymentTnx.setPnr(pnr);
		tempPaymentTnx.setStatus(ReservationInternalConstants.TempPaymentTnxTypes.INITIATED);
		tempPaymentTnx.setHistory(tempPaymentTnx.getStatus());
		if (isOfflinePayment) {
			tempPaymentTnx.setIsOfflinePayment("Y");
		}

		// charith
		// To avoid payments without currency conversion
		if (!CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY)
				.equals(tempPaymentTnx.getPaymentCurrencyCode())
				&& tempPaymentTnx.getAmount().doubleValue() == tempPaymentTnx.getPaymentCurrencyAmount().doubleValue()) {
			throw new ModuleException("airreservations.temporyPayment.currencyConvertionFailed");
		}
		if (CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY)
				.equals(tempPaymentTnx.getPaymentCurrencyCode())
				&& tempPaymentTnx.getAmount().doubleValue() != tempPaymentTnx.getPaymentCurrencyAmount().doubleValue()) {
			throw new ModuleException("airreservations.temporyPayment.currencyConvertionFailed");
		}
		// Setting the IP Address if specified
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			tempPaymentTnx.setIpAddress(credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		// Is Credit
		if (isCredit) {
			tempPaymentTnx.setTnxType(ReservationInternalConstants.TnxTypes.CREDIT);
		}
		// Is Debit
		else {
			tempPaymentTnx.setTnxType(ReservationInternalConstants.TnxTypes.DEBIT);
		}

		reservationPaymentDAO.saveTempPaymentInfo(tempPaymentTnx);

		return tempPaymentTnx.getTnxId();
	}

	public static Collection<OfflinePaymentInfo> getUniqueOfflinePaymentObject(OfflinePaymentInfo offlinePaymentInfo,
			Collection<OfflinePaymentInfo> colOfflinePaymentInfo) {
		OfflinePaymentInfo cloneOfflinePaymentInfo = (OfflinePaymentInfo) offlinePaymentInfo.clone();

		Iterator<OfflinePaymentInfo> itOfflinePaymentInfo = colOfflinePaymentInfo.iterator();
		OfflinePaymentInfo offlinePayInfo;
		PayCurrencyDTO payCurrencyDTO;
		boolean newRecord = true;

		while (itOfflinePaymentInfo.hasNext()) {
			offlinePayInfo = itOfflinePaymentInfo.next();

			if (OfflinePaymentInfo.compare(offlinePayInfo, cloneOfflinePaymentInfo)) {
				newRecord = false;
				offlinePayInfo.setTotalAmount(AccelAeroCalculator.add(offlinePayInfo.getTotalAmount(),
						cloneOfflinePaymentInfo.getTotalAmount()));
				payCurrencyDTO = offlinePayInfo.getPayCurrencyDTO();
				
				payCurrencyDTO.setTotalPayCurrencyAmount(AccelAeroCalculator.multiplyDefaultScale(
						offlinePayInfo.getTotalAmount(), payCurrencyDTO.getPayCurrMultiplyingExchangeRate()));

				break;
			}
		}

		// If its a newly created record
		if (newRecord) {
			colOfflinePaymentInfo.add(cloneOfflinePaymentInfo);
		}

		return colOfflinePaymentInfo;
	}
}
