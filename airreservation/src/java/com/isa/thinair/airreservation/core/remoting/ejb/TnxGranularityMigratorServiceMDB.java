/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.remoting.ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.login.util.ForceLoginInvoker;

/**
 * @author Nilindra Fernando
 * @since Oct 27, 2010
 */
@MessageDriven(name = "MessagingServiceMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/tnxmigratorQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "4800") })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class TnxGranularityMigratorServiceMDB implements MessageListener {
	//private static final long serialVersionUID = -6583759698965963589L;

	public void onMessage(Message message) {

		try {
			//ForceLoginInvoker forceLoginInvoker = new ForceLoginInvoker();
			ForceLoginInvoker.defaultLogin();

			ObjectMessage objMessage = (ObjectMessage) message;
			String pnr = (String) objMessage.getObject();

			ReservationModuleUtils.getReservationBD().migrateTnxForGranularity(pnr);
		} catch (Throwable cdaex) {
		} finally {
			ForceLoginInvoker.close();
		}
	}

}
