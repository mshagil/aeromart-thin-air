package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargeRateDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.InsuranceSegExtChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.AutoCheckinExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.automaticCheckin.PaxAutomaticCheckinTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.CancellationUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to reprotect Exchanging segment ancillary
 * 
 * @author Dilan
 * 
 * @isa.module.command name="reprotectExchangedSegmentAncillary"
 */
public class ReprotectExchangedSegmentAncillary extends DefaultBaseCommand {

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {
		Collection<Integer> exchangedPnrSegIds = this.getParameter(CommandParamNames.EXCHANGE_PNR_SEGMENT_IDS, Collection.class);
		Map<Integer, IPayment> pnrPaxIdAndPayments = this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, Map.class);
		Collection<OndFareDTO> ondFareDTOs = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
		Collection<Integer> flownPnrSegIds = this.getParameter(CommandParamNames.FLOWN_PNR_SEGMENT_IDS, Collection.class);
		Map<Integer, List<ExternalChgDTO>> paxSeqWiseExtChgMap = this.getParameter(CommandParamNames.PAX_EXTERNAL_CHARGE_MAP,
				Map.class);

		String pnr = this.getParameter(CommandParamNames.PNR, String.class);
		Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges = new HashMap<Integer, Collection<ExternalChgDTO>>();
		Map<Integer, Map<Integer, Integer>> reprotedSegWiseSsrInvtry = new HashMap<Integer, Map<Integer, Integer>>();
		Map<Integer, List<ExternalChgDTO>> paxIdWiseExtChgMap = new HashMap<Integer, List<ExternalChgDTO>>();

		if (exchangedPnrSegIds != null && exchangedPnrSegIds.size() > 0) {
			if (pnrPaxIdAndPayments == null) {
				pnrPaxIdAndPayments = new HashMap<Integer, IPayment>();
			}

			Collection<Integer> effectiveExchangePnrSegIds = new ArrayList<Integer>();
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSeatingInfo(true);
			pnrModesDTO.setLoadMealInfo(true);
			pnrModesDTO.setLoadBaggageInfo(true);
			pnrModesDTO.setLoadAirportTransferInfo(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadAutoCheckinInfo(true);
			Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
			
			// Add Handling Fee on Modification flow to PaymentAssembler when payment assembler is composed at this
			// level [RequoteFlow:CancelSegment]
			addHandlingFeeForModification(reservation, pnrPaxIdAndPayments, paxSeqWiseExtChgMap);
			
			// For OHD modification insnurance will not be reprotected.
			Boolean isOHDBooking = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus());
			Map<Integer, String> fltSegCabinClass = new HashMap<Integer, String>();
			for (OndFareDTO ondFare : ondFareDTOs) {
				for (SegmentSeatDistsDTO segSeatDist : ondFare.getSegmentSeatDistsDTO()) {
					fltSegCabinClass.put(segSeatDist.getFlightSegId(), segSeatDist.getCabinClassCode());
				}
			}

			Map<Integer, Integer> pnrFlightSegIds = new HashMap<Integer, Integer>();
			for (ReservationSegment resSeg : reservation.getSegments()) {
				pnrFlightSegIds.put(resSeg.getPnrSegId(), resSeg.getFlightSegId());
				if (exchangedPnrSegIds.contains(resSeg.getPnrSegId())
						&& resSeg.getCabinClassCode().equals(fltSegCabinClass.get(resSeg.getFlightSegId()))) {
					effectiveExchangePnrSegIds.add(resSeg.getPnrSegId());
				}
			}

			if (flownPnrSegIds != null && flownPnrSegIds.size() > 0) {
				flownPnrSegIds = CancellationUtils.getAllPnrSegIdsForSameOndGroup(flownPnrSegIds, reservation);
				effectiveExchangePnrSegIds.removeAll(flownPnrSegIds);
			}
			boolean hasInsurance = false;
			boolean showTravelInsurance = AppSysParamsUtil.isShowTravelInsurance();

			if (reservation.getReservationInsurance() != null && !reservation.getReservationInsurance().isEmpty()) {
				for (ReservationInsurance resInsurance : reservation.getReservationInsurance()) {
					if (!ReservationInternalConstants.INSURANCE_STATES.OH.toString().equals(resInsurance.getState())) {
						hasInsurance = true;
					}
				}
			}

			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SEAT_MAP);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.MEAL);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BAGGAGE);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_SERVICE);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.FLEXI_CHARGES);
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
			if (!hasInsurance && showTravelInsurance) {
				colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
			}
			Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = ReservationBO
					.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, ChargeRateOperationType.MODIFY_ONLY);

			List<Integer> insChargeRateIds = new ArrayList<Integer>();
			ExternalChgDTO insCharge = null;
			if (!hasInsurance && showTravelInsurance) {
				insCharge = extExternalChgDTOMap.get(EXTERNAL_CHARGES.INSURANCE);
				for (ChargeRateDTO chgRate : insCharge.getChargeRates()) {
					insChargeRateIds.add(chgRate.getChgRateId());
				}
				insChargeRateIds.add(insCharge.getChgRateId());
			}

			for (ReservationPax pax : reservation.getPassengers()) {

				for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
					for (ReservationPaxFareSegment paxSegFare : paxFare.getPaxFareSegments()) {
						if (!effectiveExchangePnrSegIds.contains(paxSegFare.getPnrSegId())) {
							continue;
						}
						for (ReservationPaxSegmentSSR ssr : paxSegFare.getReservationPaxSegmentSSRs()) {
							SSRExternalChargeDTO extChg = null;
							if (ssr.getAirportCode() == null) {
								extChg = (SSRExternalChargeDTO) extExternalChgDTOMap.get(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
								extChg = (SSRExternalChargeDTO) extChg.clone();

								extChg.setChargeCode(EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString());
							} else {
								extChg = (SSRExternalChargeDTO) extExternalChgDTOMap.get(EXTERNAL_CHARGES.AIRPORT_SERVICE);
								extChg = (SSRExternalChargeDTO) extChg.clone();
								extChg.setChargeCode(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());
							}

							int fltSegId = pnrFlightSegIds.get(paxSegFare.getPnrSegId());
							extChg.setAmount(ssr.getChargeAmount());
							extChg.setAmountConsumedForPayment(true);
							extChg.setAirportCode(ssr.getAirportCode());
							extChg.setFlightRPH(fltSegId + "");
							extChg.setFlightSegId(fltSegId);
							// extChg.setSegmentSequece(paxExtChg.getSegmentSequence());
							extChg.setApplyOn(ssr.getApplyOn());
							extChg.setSSRText(ssr.getSSRText());
							extChg.setSSRId(ssr.getSSRId());

							if (!reprotedSegWiseSsrInvtry.containsKey(fltSegId)) {
								reprotedSegWiseSsrInvtry.put(fltSegId, new HashMap<Integer, Integer>());
							}
							if (!reprotedSegWiseSsrInvtry.get(fltSegId).containsKey(ssr.getSSRId())) {
								reprotedSegWiseSsrInvtry.get(fltSegId).put(ssr.getSSRId(), 0);
							}
							reprotedSegWiseSsrInvtry.get(fltSegId).put(ssr.getSSRId(),
									reprotedSegWiseSsrInvtry.get(fltSegId).get(ssr.getSSRId()) + 1);

							getPaxExtChgs(pax.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
						}

						if (!hasInsurance && !isOHDBooking) {
							for (ReservationPaxOndCharge charge : paxSegFare.getReservationPaxFare().getCharges()) {
								if (insChargeRateIds.contains(charge.getChargeRateId())) {
									InsuranceSegExtChgDTO extChg = new InsuranceSegExtChgDTO((ExternalChgDTO) insCharge.clone());
									int fltSegId = pnrFlightSegIds.get(paxSegFare.getPnrSegId());
									extChg.setAmount(charge.getEffectiveAmount());
									extChg.setAmountConsumedForPayment(true);
									extChg.setFlightSegId(fltSegId);
									extChg.setChargeCode(EXTERNAL_CHARGES.INSURANCE.toString());
									getPaxExtChgs(pax.getPnrPaxId(), reprotectedExternalCharges).add(extChg);

								}
							}
						}

						// Re-protect Flexi
						if (reservation.getPaxOndFlexibilities() != null && reservation.getPaxOndFlexibilities().size() > 0) {
							for (Integer paxFareId : reservation.getPaxOndFlexibilities().keySet()) {

								if (!paxFareId.equals(paxFare.getPnrPaxFareId())) {
									continue;
								}

								Collection<ReservationPaxOndFlexibilityDTO> collResPaxOndFlexibilityDTO = reservation
										.getPaxOndFlexibilities().get(paxFareId);
								if (collResPaxOndFlexibilityDTO != null && !collResPaxOndFlexibilityDTO.isEmpty()) {
									FlexiExternalChgDTO extChg = (FlexiExternalChgDTO) extExternalChgDTOMap
											.get(EXTERNAL_CHARGES.FLEXI_CHARGES);
									extChg = (FlexiExternalChgDTO) extChg.clone();

									BigDecimal flexiCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
									for (ReservationPaxOndCharge charge : paxSegFare.getReservationPaxFare().getCharges()) {
										if (extChg.getChgRateId().equals((charge.getChargeRateId()))) {
											flexiCharge = charge.getAmount();
										}
									}

									extChg.setAmount(flexiCharge);
									extChg.setChargeCode(EXTERNAL_CHARGES.FLEXI_CHARGES.toString());
									extChg.setFlightSegId(pnrFlightSegIds.get(paxSegFare.getSegment().getPnrSegId()));
									extChg.setReservationPaxOndFlexibilityDTOs(collResPaxOndFlexibilityDTO);
									extChg.setPnrSegId(paxSegFare.getSegment().getPnrSegId());

									getPaxExtChgs(pax.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
								}

							}
						}
					}

				}

				if (paxSeqWiseExtChgMap != null) {
					if (paxSeqWiseExtChgMap.containsKey(pax.getPaxSequence())
							&& paxSeqWiseExtChgMap.get(pax.getPaxSequence()) != null) {
						paxIdWiseExtChgMap.put(pax.getPnrPaxId(), paxSeqWiseExtChgMap.get(pax.getPaxSequence()));
					}
				}

			}

			for (PaxSeatTO seat : reservation.getSeats()) {
				if (!effectiveExchangePnrSegIds.contains(seat.getPnrSegId())) {
					continue;
				}
				SMExternalChgDTO extChg = (SMExternalChgDTO) extExternalChgDTOMap.get(EXTERNAL_CHARGES.SEAT_MAP);
				extChg = (SMExternalChgDTO) extChg.clone();
				extChg.setChargeCode(EXTERNAL_CHARGES.SEAT_MAP.toString());

				extChg.setFlightSegId(pnrFlightSegIds.get(seat.getPnrSegId()));
				extChg.setSeatNumber(seat.getSeatCode());
				extChg.setAmount(seat.getChgDTO().getAmount());
				getPaxExtChgs(seat.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
			}

			for (PaxMealTO meal : reservation.getMeals()) {
				if (!effectiveExchangePnrSegIds.contains(meal.getPnrSegId())) {
					continue;
				}
				MealExternalChgDTO extChg = (MealExternalChgDTO) extExternalChgDTOMap.get(EXTERNAL_CHARGES.MEAL);
				extChg = (MealExternalChgDTO) extChg.clone();
				extChg.setChargeCode(EXTERNAL_CHARGES.MEAL.toString());
				extChg.setFlightSegId(pnrFlightSegIds.get(meal.getPnrSegId()));
				extChg.setMealCode(meal.getMealCode());
				extChg.setAmount(meal.getChgDTO().getAmount());
				if (meal.getChgDTO() != null && meal.getChgDTO() != null) {
					MealExternalChgDTO mealExtChg = (MealExternalChgDTO) meal.getChgDTO();
					if (mealExtChg.getOfferedTemplateID() != null) {
						extChg.setOfferedTemplateID(mealExtChg.getOfferedTemplateID());
					}
				}
				getPaxExtChgs(meal.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
			}

			if (reservation.getAutoCheckins() != null && !reservation.getAutoCheckins().isEmpty()) {
				for (PaxAutomaticCheckinTO autoCheckin : reservation.getAutoCheckins()) {
					if (!effectiveExchangePnrSegIds.contains(autoCheckin.getPnrSegId())) {
						continue;
					}
					AutoCheckinExternalChgDTO extChg = (AutoCheckinExternalChgDTO) extExternalChgDTOMap
							.get(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
					extChg = (AutoCheckinExternalChgDTO) extChg.clone();
					extChg.setChargeCode(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN.toString());
					extChg.setFlightSegId(pnrFlightSegIds.get(autoCheckin.getPnrSegId()));
					extChg.setAutoCheckinCharge(autoCheckin.getChgDTO().getAmount());
					extChg.setAmount(autoCheckin.getChgDTO().getAmount());
					extChg.setSeatTypePreference(autoCheckin.getSeatTypePreference());
					extChg.setEmail(autoCheckin.getEmail());
					if (autoCheckin.getChgDTO() != null) {
						AutoCheckinExternalChgDTO autoCheckinExtChg = (AutoCheckinExternalChgDTO) autoCheckin.getChgDTO();
						if (autoCheckinExtChg.getAutoCheckinId() != null) {
							extChg.setAutoCheckinId(autoCheckinExtChg.getAutoCheckinId());
						}
					}
					getPaxExtChgs(autoCheckin.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
				}
			}

			if (reservation.getAirportTransfers() != null && !reservation.getAirportTransfers().isEmpty()) {
				for (PaxAirportTransferTO apt : reservation.getAirportTransfers()) {
					if (!effectiveExchangePnrSegIds.contains(apt.getPnrSegId())) {
						continue;
					}
					AirportTransferExternalChgDTO extChg = (AirportTransferExternalChgDTO) extExternalChgDTOMap
							.get(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
					extChg = (AirportTransferExternalChgDTO) extChg.clone();
					extChg.setChargeCode(EXTERNAL_CHARGES.AIRPORT_TRANSFER.toString());
					extChg.setPnrSegId(pnrFlightSegIds.get(apt.getPnrSegId()));
					extChg.setFlightSegId(pnrFlightSegIds.get(apt.getPnrSegId()));
					extChg.setAirportTransferId(apt.getAirportTransferId());
					extChg.setAirportCode(apt.getAirportCode());
					extChg.setTransferAddress(apt.getAddress());
					extChg.setTransferContact(apt.getContactNo());
					extChg.setPnrPaxFareId(apt.getPpfId());
					extChg.setPnrPaxId(apt.getPnrPaxId());
					extChg.setTransferType(apt.getPickupType());
					extChg.setTransferDate(apt.getRequestTimeStamp());
					extChg.setAmount(apt.getChargeAmount());
					getPaxExtChgs(apt.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
				}
			}

			Collection<String> baggageOndGroupIdsForFlownSegments = getBaggageOndGroupIdsForFlownSegments(
					reservation.getBaggages(), flownPnrSegIds);
			for (PaxBaggageTO baggage : reservation.getBaggages()) {
				if (!effectiveExchangePnrSegIds.contains(baggage.getPnrSegId())
						|| baggageOndGroupIdsForFlownSegments.contains(baggage.getBaggageOndGroupId())) {
					continue;
				}
				BaggageExternalChgDTO extChg = (BaggageExternalChgDTO) extExternalChgDTOMap.get(EXTERNAL_CHARGES.BAGGAGE);
				extChg = (BaggageExternalChgDTO) extChg.clone();
				extChg.setChargeCode(EXTERNAL_CHARGES.BAGGAGE.toString());
				extChg.setFlightSegId(pnrFlightSegIds.get(baggage.getPnrSegId()));
				extChg.setBaggageCode(baggage.getBaggageName());
				extChg.setBaggageId(baggage.getBaggageId());
				extChg.setOndBaggageGroupId(baggage.getBaggageOndGroupId());
				extChg.setOndBaggageChargeGroupId(baggage.getBaggageChrgId().toString());
				extChg.setAmount(baggage.getChgDTO().getAmount());
				getPaxExtChgs(baggage.getPnrPaxId(), reprotectedExternalCharges).add(extChg);
			}
			
			if (AppSysParamsUtil.shouldRemoveReprotectedBundledAnciForOHD() && isOHDBooking) {
				Set<ReservationSegment> confirmedReservationSegments = ReservationApiUtils.getConfirmedSegments(reservation
						.getSegments());
				if (confirmedReservationSegments != null && !confirmedReservationSegments.isEmpty()) {
					removeReprotectedBundledServices(confirmedReservationSegments, ondFareDTOs, reprotectedExternalCharges,
							effectiveExchangePnrSegIds);
				}
			}
			
			for (Integer pnrPaxId : reprotectedExternalCharges.keySet()) {
				Collection<ExternalChgDTO> extChgs = reprotectedExternalCharges.get(pnrPaxId);
				if (extChgs != null && extChgs.size() > 0) {
					if (!pnrPaxIdAndPayments.containsKey(pnrPaxId)) {
						pnrPaxIdAndPayments.put(pnrPaxId, new PaymentAssembler());
					}

					pnrPaxIdAndPayments.get(pnrPaxId).addExternalCharges(extChgs);
				}
			}

		} else {
			if (paxSeqWiseExtChgMap != null) {
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
				for (ReservationPax pax : reservation.getPassengers()) {
					if (paxSeqWiseExtChgMap.containsKey(pax.getPaxSequence())
							&& paxSeqWiseExtChgMap.get(pax.getPaxSequence()) != null) {
						paxIdWiseExtChgMap.put(pax.getPnrPaxId(), paxSeqWiseExtChgMap.get(pax.getPaxSequence()));
					}
				}
				
				// Add Handling Fee on Modification flow to PaymentAssembler when payment assembler is composed at this
				// level [RequoteFlow:CancelSegment Dry/Interline]
				addHandlingFeeForModification(reservation, pnrPaxIdAndPayments, paxSeqWiseExtChgMap);
			}
		}
		
		removeReprotectedFlexiCharge(reprotectedExternalCharges);

		Map<Integer, Collection<ExternalChgDTO>> allExtCharges = getAllNewPaxExternalCharges(reprotectedExternalCharges,
				paxIdWiseExtChgMap);

		Map<Integer, List<SSRExternalChargeDTO>> anciMap = ReservationDAOUtils.DAOInstance.RESERVATION_SSR
				.getPasssengerSSRDetails(pnr);

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		response.addResponceParam(CommandParamNames.NEW_AND_REPROTECT_EXT_CHARGES, allExtCharges);
		response.addResponceParam(CommandParamNames.EXCHG_REPROTECTED_EXTERNAL_CHARGES, reprotectedExternalCharges);
		response.addResponceParam(CommandParamNames.EXCHG_REPROTD_SSR_FLTSEG_INVENT, reprotedSegWiseSsrInvtry);
		response.addResponceParam(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, pnrPaxIdAndPayments);
		response.addResponceParam(CommandParamNames.SELECTED_ANCI_MAP, anciMap);
		if (this.getParameter(CommandParamNames.BOOKING_TYPE) != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		return response;
	}

	private Collection<String> getBaggageOndGroupIdsForFlownSegments(Collection<PaxBaggageTO> baggages,
			Collection<Integer> flownPnrSegIds) {
		Collection<String> baggageOndGroupIdsForFlownSegments = new HashSet<String>();
		if (baggages != null) {
			for (PaxBaggageTO paxBaggage : baggages) {
				if (flownPnrSegIds.contains(paxBaggage.getPnrSegId())) {
					baggageOndGroupIdsForFlownSegments.add(paxBaggage.getBaggageOndGroupId());
				}
			}
		}
		return baggageOndGroupIdsForFlownSegments;
	}

	private Collection<ExternalChgDTO> getPaxExtChgs(Integer pnrPaxId,
			Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges) {
		if (!reprotectedExternalCharges.containsKey(pnrPaxId)) {
			reprotectedExternalCharges.put(pnrPaxId, new ArrayList<ExternalChgDTO>());
		}
		return reprotectedExternalCharges.get(pnrPaxId);
	}

	private Map<Integer, Collection<ExternalChgDTO>> getAllNewPaxExternalCharges(
			Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Map<Integer, List<ExternalChgDTO>> paxIdWiseExtChgMap) {

		Map<Integer, Collection<ExternalChgDTO>> allExtCharges = new HashMap<>();
		if (reprotectedExternalCharges != null) {
			for (Entry<Integer, Collection<ExternalChgDTO>> extChgEntry : reprotectedExternalCharges.entrySet()) {
				Collection<ExternalChgDTO> lccExtChgList = extChgEntry.getValue();
				int pnrPaxId = extChgEntry.getKey();
				if (lccExtChgList != null && lccExtChgList.size() > 0) {
					if (!allExtCharges.containsKey(pnrPaxId)) {
						allExtCharges.put(pnrPaxId, new ArrayList<ExternalChgDTO>());
					}
					allExtCharges.get(pnrPaxId).addAll(lccExtChgList);
				}
			}
		}
		if (paxIdWiseExtChgMap != null) {
			for (Entry<Integer, List<ExternalChgDTO>> extChgEntry : paxIdWiseExtChgMap.entrySet()) {
				List<ExternalChgDTO> lccExtChgList = extChgEntry.getValue();
				int pnrPaxId = extChgEntry.getKey();
				if (lccExtChgList != null && lccExtChgList.size() > 0) {
					if (!allExtCharges.containsKey(pnrPaxId)) {
						allExtCharges.put(pnrPaxId, new ArrayList<ExternalChgDTO>());
					}
					allExtCharges.get(pnrPaxId).addAll(lccExtChgList);
				}
			}
		}

		return allExtCharges;
	}
	
	private void removeReprotectedFlexiCharge(Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges) {
		if (reprotectedExternalCharges != null && !reprotectedExternalCharges.isEmpty()) {

			for (Entry<Integer, Collection<ExternalChgDTO>> entry : reprotectedExternalCharges.entrySet()) {
				Collection<ExternalChgDTO> externalChgDTOs = entry.getValue();
				if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
					for (ExternalChgDTO externalChgDTO : externalChgDTOs) {
						if (externalChgDTO instanceof FlexiExternalChgDTO) {
							FlexiExternalChgDTO flexi = (FlexiExternalChgDTO) externalChgDTO;
							flexi.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());

						}
					}
				}
			}
		}
	}
	
	private Integer getBundleFareId(Set<ReservationSegment> reservationSegments, Integer exchangedPnrSegId) {
		Integer bundleFareId = -1;
		for (ReservationSegment resSegment : reservationSegments) {
			if (exchangedPnrSegId.equals(resSegment.getPnrSegId())) {
				bundleFareId = resSegment.getBundledFarePeriodId();
				break;
			}
		}
		return bundleFareId;
	}
	
	private Integer getBundleFareId(Set<ReservationSegment> reservationSegments, Integer exchangedPnrSegId, Collection<OndFareDTO> ondFareDTOs) {
		Integer bundleFareId = -1;
		Integer flightSegmentId = -1;
		
		for (ReservationSegment resSegment : reservationSegments) {
			if (exchangedPnrSegId.equals(resSegment.getPnrSegId())) {
				flightSegmentId = resSegment.getFlightSegId();
				break;
			}
		}
		
		if (flightSegmentId != -1) {
			for (OndFareDTO ondFareDTO :ondFareDTOs) {
				LinkedHashMap<Integer, FlightSegmentDTO> segmentMap = ondFareDTO.getSegmentsMap();
				for (Entry<Integer, FlightSegmentDTO> entry : segmentMap.entrySet()) {
					FlightSegmentDTO flightSegmentDTO = entry.getValue();
					if (flightSegmentId.equals(flightSegmentDTO.getSegmentId())){
						bundleFareId = ondFareDTO.getSelectedBundledFarePeriodId();
						break;
					}
				}
			}
		}
		return bundleFareId;
	}
	
	private Integer getFlightSegmentId(Set<ReservationSegment> reservationSegments, Integer exchangedPnrSegId) {
		Integer flightSegmentId = -1;
		for (ReservationSegment resSegment : reservationSegments) {
			if (exchangedPnrSegId.equals(resSegment.getPnrSegId())) {
				flightSegmentId = resSegment.getFlightSegId();
				break;
			}
		}
		return flightSegmentId;
	}
	
	private void removeReprotectedBundledServices(Set<ReservationSegment> confirmedReservationSegments,
			Collection<OndFareDTO> ondFareDTOs, Map<Integer, Collection<ExternalChgDTO>> reprotectedExternalCharges,
			Collection<Integer> exchangedPnrSegIds) {

		List<Integer> segIdsToRemoveReprotectedAnci = new ArrayList<Integer>();

		for (Integer exchangedPnrSegId : exchangedPnrSegIds) {
			Integer oldSegmentBundleFareId = getBundleFareId(confirmedReservationSegments, exchangedPnrSegId);
			Integer newSegmentBundleFareId = getBundleFareId(confirmedReservationSegments, exchangedPnrSegId, ondFareDTOs);

			if (ReservationApiUtils.shouldReprotectedAncillariesRemoved(oldSegmentBundleFareId, newSegmentBundleFareId)) {
				Integer flightSegmentId = getFlightSegmentId(confirmedReservationSegments, exchangedPnrSegId);
				segIdsToRemoveReprotectedAnci.add(flightSegmentId);
			}
		}

		if (reprotectedExternalCharges != null && !reprotectedExternalCharges.isEmpty()
				&& !segIdsToRemoveReprotectedAnci.isEmpty()) {
			for (Entry<Integer, Collection<ExternalChgDTO>> entry : reprotectedExternalCharges.entrySet()) {
				Collection<ExternalChgDTO> externalChgDTOs = entry.getValue();
				Collection<ExternalChgDTO> tempExternalChgDTOs = new ArrayList<ExternalChgDTO>();
				if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
					for (ExternalChgDTO externalChgDTO : externalChgDTOs) {
						if (!segIdsToRemoveReprotectedAnci.contains(externalChgDTO.getFlightSegId())) {
							tempExternalChgDTOs.add(externalChgDTO);
						}
					}
				}
				externalChgDTOs = tempExternalChgDTOs;
				reprotectedExternalCharges.put(entry.getKey(), tempExternalChgDTOs);
			}
		}
	}
	
	private void addHandlingFeeForModification(Reservation reservation, Map<Integer, IPayment> pnrPaxIdAndPayments,
			Map<Integer, List<ExternalChgDTO>> paxSeqWiseExtChgMap) throws ModuleException {
		if ((pnrPaxIdAndPayments == null || pnrPaxIdAndPayments.isEmpty()) && paxSeqWiseExtChgMap != null
				&& !paxSeqWiseExtChgMap.isEmpty()) {

			if (pnrPaxIdAndPayments == null) {
				pnrPaxIdAndPayments = new HashMap<Integer, IPayment>();
			}

			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

			ExternalChgDTO externalChgDTO = BeanUtils
					.getFirstElement(ReservationBO.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());

			for (ReservationPax pax : reservation.getPassengers()) {
				Integer pnrPaxId = pax.getPnrPaxId();
				if (paxSeqWiseExtChgMap.get(pax.getPaxSequence()) != null) {
					Collection<ExternalChgDTO> extChgs = paxSeqWiseExtChgMap.get(pax.getPaxSequence());
					Collection<ExternalChgDTO> filteredExtChgs = ReservationApiUtils
							.getFilteredHandlingFeeChargesForModification(extChgs, externalChgDTO);

					if (filteredExtChgs != null && filteredExtChgs.size() > 0) {

						if (!pnrPaxIdAndPayments.containsKey(pnrPaxId)) {
							pnrPaxIdAndPayments.put(pnrPaxId, new PaymentAssembler());
						}
						pnrPaxIdAndPayments.get(pnrPaxId).addExternalCharges(filteredExtChgs);
					}
				}
			}

		}
	}

}
