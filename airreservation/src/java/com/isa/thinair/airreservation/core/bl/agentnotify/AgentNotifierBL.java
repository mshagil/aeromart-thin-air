/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.agentnotify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.dto.AgentNotificationDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.auditor.api.model.AgentAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

/**
 * @author nafly
 * 
 */
public class AgentNotifierBL {

	private static Log log = LogFactory.getLog(AgentNotifierBL.class);

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	MessagingServiceBD messagingServiceBD = AirInventoryModuleUtils.getMessagingServiceBD();

	private static int maxNoOfAttempts = 1;

	public AgentNotifierBL() {
		maxNoOfAttempts = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.BANK_GUARANTEE_NOTIFICATION_ATTEMPTS));
	}

	/**
	 * Identify and send emails to the list of agents
	 * 
	 * @param ccAddress
	 * 
	 * @throws ModuleException
	 */
	public void sendAgentNotificationEmails(List<AgentNotificationDTO> notificationDTOs, UserPrincipal userPrincipal,
			String emailTemplate, Integer defaultMessageValidity, String ccAddress) throws ModuleException {
		List<MessageProfile> messageProfiles = new ArrayList<MessageProfile>();

		messageProfiles = preapreAgentNotificationEmailListToSend(notificationDTOs, userPrincipal, emailTemplate,
				defaultMessageValidity, ccAddress);

		if (messageProfiles != null && !messageProfiles.isEmpty()) {
			messagingServiceBD.sendMessages(messageProfiles);
		}
	}

	/**
	 * Compose the email list to send to the agent
	 * 
	 * @param notificationDTOs
	 * @param ccAddress
	 * @return
	 * @throws ModuleException
	 */
	private static List<MessageProfile> preapreAgentNotificationEmailListToSend(List<AgentNotificationDTO> notificationDTOs,
			UserPrincipal userPrincipal, String emailTemplate, int defaultMessageValidity, String ccAddress)
			throws ModuleException {
		ArrayList<MessageProfile> messageProfileList = new ArrayList<MessageProfile>();
		Topic topic = null;
		HashMap<Object, Object> map = null;
		MessageProfile profile = null;
		UserMessaging userMessaging = null;
		List<UserMessaging> userMessageList = new ArrayList<UserMessaging>();
		AgentAudit agentAudit = null;
		ArrayList<AgentAudit> agentAuditList = new ArrayList<AgentAudit>();

		for (AgentNotificationDTO notificationDTO : notificationDTOs) {
			map = new HashMap<Object, Object>();
			topic = new Topic();
			profile = new MessageProfile();
			userMessaging = new UserMessaging();
			userMessageList = new ArrayList<UserMessaging>();
			agentAuditList = new ArrayList<AgentAudit>();

			// Put the details to the map of the template
			map.put("notificationDTO", notificationDTO);

			// Set Audit records to topic object
			if (ReservationInternalConstants.BankGuranteeNotifyTemplates.BANK_GUARANTEE_EXPIRY_NOTIFICATION
					.equals(emailTemplate)) {
				agentAudit = createAgentAuditRecord(notificationDTO, AuditTemplateEnum.BANK_GUR_EXP_EMAIL, null, userPrincipal,
						null);
			} else {

				AuditTemplateEnum auditTemplateEnum = AuditTemplateEnum.CREDIT_LIMIT_UTILIZATION_EMAIL;
				String paymentCode = Agent.PAYMENT_MODE_ONACCOUNT;
				if (ReservationInternalConstants.BankGuranteeNotifyTemplates.CREDIT_LIMIT_UTILIZATION_NOTIFICATION
						.equals(emailTemplate)) {
					auditTemplateEnum = AuditTemplateEnum.CREDIT_LIMIT_UTILIZATION_EMAIL;
					paymentCode = Agent.PAYMENT_MODE_ONACCOUNT;
				} else if (ReservationInternalConstants.BankGuranteeNotifyTemplates.BSP_CREDIT_LIMIT_UTILIZATION_NOTIFICATION
						.equals(emailTemplate)) {
					auditTemplateEnum = AuditTemplateEnum.BSP_CREDIT_LIMIT_UTILIZATION_EMAIL;
					paymentCode = Agent.PAYMENT_MODE_BSP;

					// Set BSP related data
					String notifyDate = CalendarUtil.getFormattedDate(CalendarUtil.getCurrentSystemTimeInZulu(),
							CalendarUtil.PATTERN12);

					map.put("checkedPercentage", notificationDTO.getCheckedPercentage() * 100);
					map.put("notifyDate", notifyDate);
					map.put("baseCurrency", AppSysParamsUtil.getBaseCurrency());
				}

				agentAudit = createAgentAuditRecord(notificationDTO, auditTemplateEnum, paymentCode, userPrincipal, ccAddress);
			}

			if (agentAudit != null) {
				agentAudit.setDefaultMessageValidity(defaultMessageValidity);
				agentAuditList.add(agentAudit);
				topic.setAuditInfo(agentAuditList);
			}

			// Set map and topic name to the topic
			topic.setTopicParams(map);
			topic.setTopicName(emailTemplate);

			// Set message recipients details
			String toAddress = "";

			if (notificationDTO.getBillingEmail() != null && !notificationDTO.getBillingEmail().equals("")) {
				toAddress = notificationDTO.getBillingEmail();
			}

			if (ccAddress != null && !"".equals(ccAddress)) {
				toAddress += "," + ccAddress;
			}
			userMessaging.setToAddres(toAddress);
			userMessageList.add(userMessaging);

			// add the recipients and topic details to the profile
			profile.setUserMessagings(userMessageList);
			profile.setTopic(topic);

			messageProfileList.add(profile);
		}
		return messageProfileList;
	}

	/**
	 * Create Agent Audit Record
	 * 
	 * @param agentNotificationDTO
	 * @param paymentCode
	 * @param ccAddress
	 * @return
	 * @throws ModuleException
	 */
	private static AgentAudit createAgentAuditRecord(AgentNotificationDTO agentNotificationDTO,
			AuditTemplateEnum auditTemplateEnum, String paymentCode, UserPrincipal userPrincipal, String ccAddress)
			throws ModuleException {
		AgentAudit agentAudit = null;

		if (agentNotificationDTO != null) {
			agentAudit = new AgentAudit();

			agentAudit.setAgentCode(agentNotificationDTO.getAgentCode());
			agentAudit.setTemplateCode(auditTemplateEnum.getCode());
			agentAudit.setTimestamp(new Date());

			if (userPrincipal != null) {
				agentAudit.setUserId(userPrincipal.getUserId());
			}

			if (auditTemplateEnum.equals(AuditTemplateEnum.BANK_GUR_EXP_EMAIL)) {
				agentAudit.setDaysPriorToExpire(agentNotificationDTO.getNoOfDaysToExpire());
			} else if (auditTemplateEnum.equals(AuditTemplateEnum.CREDIT_LIMIT_UTILIZATION_EMAIL)
					|| auditTemplateEnum.equals(AuditTemplateEnum.BSP_CREDIT_LIMIT_UTILIZATION_EMAIL)) {
				agentAudit.setCreditLimitUtilization(agentNotificationDTO.getCheckedPercentage());
				agentAudit.setPaymentCode(paymentCode);

				if (auditTemplateEnum.equals(AuditTemplateEnum.CREDIT_LIMIT_UTILIZATION_EMAIL)) {
					agentAudit.addContentMap(AuditTemplateEnum.TemplateParams.BankGuaranteeExpireEmail.EMAIL_ADDRESS,
							agentNotificationDTO.getBillingEmail());
				} else if (auditTemplateEnum.equals(AuditTemplateEnum.BSP_CREDIT_LIMIT_UTILIZATION_EMAIL)) {
					agentAudit.addContentMap(AuditTemplateEnum.TemplateParams.BankGuaranteeExpireEmail.EMAIL_ADDRESS, ccAddress);
					agentAudit.addContentMap(AuditTemplateEnum.TemplateParams.CreditLimitUtilizationEmail.UTILIZATION,
							agentNotificationDTO.getUtilizationPercentage() + "%");
				}
			}

		}

		return agentAudit;
	}

	public static void sendTopupEmailNotification(String amount, String agentCode) throws ModuleException {
		Agent agentDetails = ReservationModuleUtils.getTravelAgentBD().getAgent(agentCode);
		String agentName = agentDetails.getAgentName();
		String agentBalance = agentDetails.getAgentSummary().getAvailableCredit().toString();
		String currencyCode = agentDetails.getCurrencyCode();
		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		List<String> agencyTopupAddress = AppSysParamsUtil.getAgentTopupEmailDestination();
		
    	List<UserMessaging> messages = new ArrayList<UserMessaging>();
    	for(String address: agencyTopupAddress) {
    		messages.add(new UserMessaging(address));
    	}
    	
		Topic topic = new Topic();
		HashMap<String, String> params = new HashMap<String, String>() {
			{
				put("amount", amount);
				put("currencyCode", currencyCode);
				put("agentCode", agentCode);
				put("agentName", agentName);
				put("agentBalance", agentBalance);
			}
		};

		topic.setTopicParams(params);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.EMAIL_NOTIFICAION_AGENT_TOPUP);

		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		messagingServiceBD.sendMessage(messageProfile);
	}

}
