package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementbuilders.NewLineElementBuilder;


public abstract class PnlAdlMessageBuilder extends BaseMessageBuilder{

	public PnlAdlMessageBuilder(String messgeBuilderType) {
		super(messgeBuilderType);
		
	}

	@Override
	protected void buildMessageHeaderReference() {
		headerElementBuilder();
		buildFlightNumberElementBuilder();
		buildSeatConfigurationElementBuilder();
		classCodeElementBuilder();
		destinationBulkElementBuilder();
		totalByDestinationFareBuilder();
		passengerModifierElementBuilder();
		nameElementBuilder();
		groupCodeElementBuilder();
		pnrElementBuilder();
		marketingFlightElementBuilder();
		inboundConnectionElementBuilder();
		onwardConnectionElementBuilder();
		waitListPassengerElementBuilder();
		standByPassengerElementBuilder();
		adultEticketElementBuilder();
		childElementBuilder();
		infantElementBuilder();
		docoElementBuilder();
		docsElementBuilder();
		seatElementBuilder();
		mealElementBuilder();
		ssrElementBuilder();
		creditCardElementBuilder();
		baggageElementBuilder();
		
	}
	
	@Override
	protected void setElementChain() {
		
		headerElementBuilder.setNextElementBuilder(flightNumberElementBuilder);

		flightNumberElementBuilder.setNextElementBuilder(seatConfigurationElementBuilder);
		flightNumberElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		seatConfigurationElementBuilder.setNextElementBuilder(classCodeElementBuilder);
		seatConfigurationElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		classCodeElementBuilder.setNextElementBuilder(destinationBulkElementBuilder);
		classCodeElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		destinationBulkElementBuilder.setNextElementBuilder(totalByDestinationElement);
		
		totalByDestinationElement.setNextElementBuilder(paxModifierElementBuilder);
		totalByDestinationElement.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		paxModifierElementBuilder.setNextElementBuilder(nameElementBuilder);
		paxModifierElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		nameElementBuilder.setNextElementBuilder(groupCodeElementBuilder);
		nameElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		nameElementBuilder.setLoopBackBuilder(paxModifierElementBuilder);
		
		groupCodeElementBuilder.setNextElementBuilder(pnrElementBuilder);
		groupCodeElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		pnrElementBuilder.setNextElementBuilder(marketingFlightElementBuilder);
		pnrElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		marketingFlightElementBuilder.setNextElementBuilder(inboundConnectionElementBuilder);
		marketingFlightElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		inboundConnectionElementBuilder.setNextElementBuilder(onwardConnectionElementBuilder);
		inboundConnectionElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		onwardConnectionElementBuilder.setNextElementBuilder(waitListPassengerElementBuilder);
		onwardConnectionElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		waitListPassengerElementBuilder.setNextElementBuilder(standByPassengerElementBuilder);
		waitListPassengerElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		standByPassengerElementBuilder.setNextElementBuilder(adultEticketElementBuilder);
		standByPassengerElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());

		adultEticketElementBuilder.setNextElementBuilder(childElementBuilder);
		adultEticketElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		childElementBuilder.setNextElementBuilder(infantElementBuilder);
		childElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		infantElementBuilder.setNextElementBuilder(docoElementBuilder);
		infantElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		docoElementBuilder.setNextElementBuilder(docsElementBuilder);
		docoElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		docsElementBuilder.setNextElementBuilder(seatElementBuilder);
		docsElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		seatElementBuilder.setNextElementBuilder(mealElementBuilder);
		seatElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		mealElementBuilder.setNextElementBuilder(ssrElementBuilder);
		mealElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		ssrElementBuilder.setNextElementBuilder(creditCardElementBuilder);
		ssrElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		creditCardElementBuilder.setNextElementBuilder(baggageElementBuilder);
		creditCardElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
		baggageElementBuilder.setConcatenationELementBuilder(new NewLineElementBuilder());
		
	}
}
