/* ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationLiteDTO;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRResultDTO;
import com.isa.thinair.airreservation.api.dto.ssr.ReservationSSRSearchDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.AnciTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationSSRDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * ReservationSSRDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @since 1.0
 * @isa.module.dao-impl dao-name="ReservationSSRDAO"
 */
public class ReservationSSRDAOImpl extends PlatformBaseHibernateDaoSupport implements ReservationSSRDAO {
	private static final Log log = LogFactory.getLog(ReservationSSRDAOImpl.class);
	private static final String dateTimeFormat = "dd MMM yyyy HH:mm";

	@Override
	@SuppressWarnings("unchecked")
	public Collection<PaxSSRDTO> getReservationSSR(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			String preferredLanguage) {

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT pp.pnr_pax_id,ps.pnr_seg_id , ppss.ppss_id, ppss.ppfs_id, ppss.ssr_id, ppss.ssr_text, ppss.ext_ref, ppss.context_id, ");
		sql.append(" ppss.charge_amount, ppss.status, ppss.pnl_adl_status, ppss.email_status, ");
		sql.append(" ppss.version, ppss.airport_code, ssi.ssr_code, ssi.ssr_desc, ssi.ssr_sub_cat_id, ssi.user_defined_ssr, ssi.VALID_FOR_PALCAL, msg.message_content as trntext, ppss.auto_cancellation_id ");
		sql.append(" FROM t_pnr_pax_segment_ssr ppss , t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, t_pnr_passenger pp, t_pnr_pax_fare PPF, t_ssr_info ssi ");
		sql.append(" LEFT OUTER JOIN t_i18n_message msg ON ssi.i18n_message_key = msg.i18n_message_key ");
		sql.append(" AND msg.message_locale = '" + preferredLanguage + "'");
		sql.append("WHERE  ppss.ppfs_id = ppfs.ppfs_id and ");
		sql.append("ppfs.pnr_seg_id = ps.pnr_seg_id and ppss.ssr_id = ssi.ssr_id and ");
		sql.append("ps.pnr_seg_id in(" + Util.constructINStringForInts(pnrSegIds) + ") and ");
		sql.append("pp.pnr_pax_id in(" + Util.constructINStringForInts(pnrPaxIds) + ") ");
		sql.append(" and PPF.pnr_pax_id=PP.pnr_pax_id AND PPF.ppf_id = PPFS.ppf_id ");
		sql.append(" AND (ppss.status <> 'CNX' OR (ppss.status='CNX' AND ssi.USER_DEFINED_SSR='Y')) ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		Collection<PaxSSRDTO> colSSR = (Collection<PaxSSRDTO>) template.query(sql.toString(), new SSRResultExtractorImpl());

		return colSSR;
	}

	@Deprecated
	@Override
	@SuppressWarnings("unchecked")
	public Collection<PaxSSRDTO> getReservationSSR(Collection<Integer> pnrPaxIds, boolean isAirportService) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT pp.pnr_pax_id,ps.pnr_seg_id , ppss.ppss_id, ppss.ppfs_id, ppss.ssr_id, ppss.ssr_text, ppss.ext_ref, ppss.context_id, ");
		sql.append(" ppss.charge_amount, ppss.status, ppss.pnl_adl_status, ppss.email_status, ");
		sql.append(" ppss.version, ppss.airport_code, ssi.ssr_code, ssi.ssr_desc, ssi.user_defined_ssr, ssi.VALID_FOR_PALCAL, ssi.ssr_sub_cat_id, 'null' as trntext, ppss.auto_cancellation_id ");
		sql.append(" FROM t_pnr_pax_segment_ssr ppss , t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, t_pnr_passenger pp, t_pnr_pax_fare PPF, t_ssr_info ssi ");
		sql.append("WHERE  ppss.ppfs_id = ppfs.ppfs_id and ");
		sql.append("ppfs.pnr_seg_id = ps.pnr_seg_id and ppss.ssr_id = ssi.ssr_id and ");
		sql.append("pp.pnr_pax_id in(" + Util.constructINStringForInts(pnrPaxIds) + ") ");
		sql.append(" and PPF.pnr_pax_id=PP.pnr_pax_id AND PPF.ppf_id = PPFS.ppf_id and ppss.status <> 'CNX'");
		if (isAirportService) {
			sql.append(" and ppss.airport_code is not null");
		} else {
			sql.append(" and ppss.airport_code is null");
		}

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Collection<PaxSSRDTO>) template.query(sql.toString(), new SSRResultExtractorImpl());
	}

	private class SSRResultExtractorImpl implements ResultSetExtractor {

		@Override
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

			Collection<PaxSSRDTO> colPaxSSR = new ArrayList<PaxSSRDTO>();

			while (rs.next()) {
				PaxSSRDTO paxssrTO = new PaxSSRDTO();
				paxssrTO.setPnrPaxSegmentSSRId(new Integer(rs.getString("ppss_id")));
				paxssrTO.setPnrPaxId(new Integer(rs.getInt("pnr_pax_id")));
				paxssrTO.setPnrSegId(new Integer(rs.getInt("pnr_seg_id")));
				paxssrTO.setSsrCode(rs.getString("ssr_code"));
				paxssrTO.setSsrText(rs.getString("ssr_text"));
				paxssrTO.setSsrDesc(rs.getString("ssr_desc"));
				paxssrTO.setExternalReference(rs.getString("ext_ref"));
				paxssrTO.setSsrSubCatID(new Integer(rs.getInt("ssr_sub_cat_id")));
				paxssrTO.setStatus(rs.getString("status"));
				paxssrTO.setSsrTranslatedText(rs.getString("trntext"));
				paxssrTO.setChargeAmount(rs.getDouble("charge_amount"));
				paxssrTO.setUserDefinedSSR("Y".equals(rs.getString("user_defined_ssr")));

				boolean includeInPalCal = false;
				if ("Y".equals(rs.getString("VALID_FOR_PALCAL"))) {
					includeInPalCal = true;
				}
				paxssrTO.setInclideInPalCAl(includeInPalCal);

				String airportCode = rs.getString("airport_code");
				if (airportCode != null && !airportCode.equalsIgnoreCase("null")) {
					paxssrTO.setAirportCode(airportCode);
				}

				if (AppSysParamsUtil.isAutoCancellationEnabled() && rs.getInt("auto_cancellation_id") != 0) {
					paxssrTO.setAutoCancellationId(rs.getInt("auto_cancellation_id"));
				}

				if (paxssrTO.getSsrTranslatedText() != null && !paxssrTO.getSsrTranslatedText().equalsIgnoreCase("null")) {
					paxssrTO.setSsrDesc(StringUtil.getUnicode(paxssrTO.getSsrTranslatedText()).replace('^', ','));
				}
				colPaxSSR.add(paxssrTO);
			}
			return colPaxSSR;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<ReservationSSRResultDTO> getReservationSSR(ReservationSSRSearchDTO reservationSSRSearchDTO) {
		StringBuilder sbSql = new StringBuilder(1500);

		String pnrNo = BeanUtils.nullHandler(reservationSSRSearchDTO.getPNRNo());
		Collection<Integer> pnrPaxIds = reservationSSRSearchDTO.getPNRPaxIds();
		Collection<Integer> pnrSegmnetIds = reservationSSRSearchDTO.getPNRSegmentIds();
		Collection<Integer> pnrPaxSegmnetSSRIds = reservationSSRSearchDTO.getpNRPaxSegmentSSRIds();
		Date fromDepartureDate = reservationSSRSearchDTO.getFromDepartureDate();
		Date toDepartureDate = reservationSSRSearchDTO.getToDepartureDate();
		boolean blnLoadCancelledSSRs = reservationSSRSearchDTO.isLoadCancelledSSRs();

		sbSql.append("SELECT res.pnr pnr,f.flight_number flightno,pax.title title,pax.first_name firstname,pax.last_name lastname,rCon.c_phone_no phoneno,rCon.c_mobile_no mobileno,");
		sbSql.append("fs.est_time_departure_local depdatelocal,fs.est_time_arrival_local arrdatelocal,ppss.apply_on applyon,ssr.ssr_desc ssrdesc,sscat.ssr_cat_id ssrcatid,sscat.ssr_sub_cat_id ssrsubcatid ");

		sbSql.append("FROM t_flight_segment fs, ");
		sbSql.append("t_flight f ,t_pnr_segment ps, ");
		sbSql.append("t_reservation res, ");
		sbSql.append("t_reservation_contact rCon, ");
		sbSql.append("t_pnr_passenger pax,");
		sbSql.append("t_pnr_pax_fare ppf,");
		sbSql.append("t_pnr_pax_fare_segment ppfs,");
		sbSql.append("t_pnr_pax_segment_ssr ppss,");
		sbSql.append("t_ssr_charge sc,t_ssr_info ssr,");
		sbSql.append("t_ssr_sub_category sscat ");

		sbSql.append("WHERE f.flight_id = fs.flight_id ");
		sbSql.append("AND ps.flt_seg_id = fs.flt_seg_id ");
		sbSql.append("AND res.pnr = ps.pnr ");
		sbSql.append("AND res.pnr = rCon.pnr ");
		sbSql.append("AND res.pnr = pax.pnr ");
		sbSql.append("AND pax.pnr_pax_id=ppf.pnr_pax_id ");
		sbSql.append("AND ppf.ppf_id=ppfs.ppf_id ");
		sbSql.append("AND ppfs.pnr_seg_id=ps.pnr_seg_id ");
		sbSql.append("AND ppfs.ppfs_id=ppss.ppfs_id ");
		sbSql.append("AND ppss.context_id=sc.ssr_charge_id ");
		sbSql.append("AND sc.ssr_id=ssr.ssr_id ");

		if (!blnLoadCancelledSSRs) {
			sbSql.append("AND ps.status<>'CNX' ");
		} else {
			sbSql.append("AND ps.status='CNX' ");
		}

		sbSql.append("AND ssr.ssr_sub_cat_id <> 0 ");
		sbSql.append("AND ssr.ssr_sub_cat_id=sscat.ssr_sub_cat_id ");

		if (fromDepartureDate != null && toDepartureDate != null) {
			sbSql.append("AND fs.est_time_departure_local between to_date("
					+ CalendarUtil.getSQLToDateInputString(fromDepartureDate) + ") AND to_date("
					+ CalendarUtil.getSQLToDateInputString(toDepartureDate) + ") ");
		} else if (fromDepartureDate != null) {
			sbSql.append("AND fs.est_time_departure_local >= to_date(" + CalendarUtil.getSQLToDateInputString(fromDepartureDate)
					+ ") ");
		} else if (toDepartureDate != null) {
			sbSql.append("AND fs.est_time_departure_local <= to_date(" + CalendarUtil.getSQLToDateInputString(toDepartureDate)
					+ ") ");
		}

		if (!pnrNo.isEmpty()) {
			sbSql.append("AND pax.pnr = '");
			sbSql.append(pnrNo);
			sbSql.append("' ");
		}

		if (pnrPaxIds != null) {
			sbSql.append(" AND pax.pnr_pax_id IN (" + Util.constructINStringForInts(pnrPaxIds) + ") ");
		}

		if (pnrSegmnetIds != null) {
			sbSql.append(" AND ps.pnr_seg_id IN (" + Util.constructINStringForInts(pnrSegmnetIds) + ") ");
		}

		if (pnrPaxSegmnetSSRIds != null && !pnrPaxSegmnetSSRIds.isEmpty()) {
			sbSql.append(" AND ppss.ppss_id IN (" + Util.constructINStringForInts(pnrPaxSegmnetSSRIds) + ") ");
		}

		sbSql.append("ORDER by ssr.ssr_sub_cat_id,fs.est_time_departure_local ");

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql = sbSql.toString();

		if (log.isDebugEnabled()) {
			log.debug(sql);
		}

		Collection<ReservationSSRResultDTO> ssrResultDTOs = (Collection<ReservationSSRResultDTO>) template.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<ReservationSSRResultDTO> ssrResultDTOs = new ArrayList<ReservationSSRResultDTO>();
						while (rs.next()) {
							ReservationSSRResultDTO ssrResultDTO = new ReservationSSRResultDTO();

							ssrResultDTO.setSsrCategoryId(rs.getInt("ssrcatid"));
							ssrResultDTO.setSsrSubCategoryId(rs.getInt("ssrsubcatid"));
							ssrResultDTO.setTimeStamp(CalendarUtil.formatDate(new Date(), dateTimeFormat));
							ssrResultDTO.setPnr(rs.getString("pnr"));
							ssrResultDTO.setFlightNo(rs.getString("flightno"));
							ssrResultDTO.setTitle(rs.getString("title"));
							ssrResultDTO.setFirstName(rs.getString("firstname"));
							ssrResultDTO.setLastName(rs.getString("lastname"));

							if (!"--".equals(rs.getString("phoneno"))) {
								ssrResultDTO.setContactNo(rs.getString("phoneno"));
							} else {
								ssrResultDTO.setContactNo(rs.getString("mobileno"));
							}

							Date serviceTime = null;

							if (ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE.equals(rs.getString("applyon"))) {
								serviceTime = rs.getTimestamp("depdatelocal");
							} else {
								serviceTime = rs.getTimestamp("arrdatelocal");
							}

							ssrResultDTO.setServiceTime(CalendarUtil.formatDate(serviceTime, dateTimeFormat));

							ssrResultDTO.setServiceDescription(rs.getString("ssrdesc"));

							ssrResultDTOs.add(ssrResultDTO);
						}
						return ssrResultDTOs;
					}
				});

		return ssrResultDTOs;
	}

	@Override
	/**
	 * Returns map of <pnrpaxId, Collection of SSRDetailDTO> 
	 *  ***Never use this for multi segment SSR, suitable for single segment used for manifest, PNL
	 */
	public Map<Integer, Collection<PaxSSRDetailDTO>> getPNRPaxSSRs(Map<Integer, Integer> mapPnrPaxSegIds, boolean isToSendPNL,
			boolean isToSendEmail, boolean isExcludeSegment) {

		if (mapPnrPaxSegIds == null | mapPnrPaxSegIds.size() == 0) {
			throw new CommonsDataAccessException("airreservations.arg.invalid.null");
		}

		return getPNRPaxSSRs(mapPnrPaxSegIds.keySet(), mapPnrPaxSegIds.values(), isToSendPNL, isToSendEmail, isExcludeSegment);

	}

	public Map<Integer, List<AncillaryDTO>> getPasssengerSSRDetails(Map<Integer, Integer> mapPnrPaxSegIds,  DCS_PAX_MSG_GROUP_TYPE msgType) {

		if (mapPnrPaxSegIds == null | mapPnrPaxSegIds.size() == 0) {
			throw new CommonsDataAccessException("airreservations.arg.invalid.null");
		}

		return getPassengerSSRDetails(mapPnrPaxSegIds.keySet(), mapPnrPaxSegIds.values(), msgType);

	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<PaxSSRDetailDTO>> getPNRPaxSSRs(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			boolean isToSendPNL, boolean isToSendEmail, boolean isExcludeSegment) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT fare.pnr_pax_id,seg.pnr_seg_id,info.ssr_code,info.ssr_desc,ssr.ssr_id,");
		sql.append("ssr.ssr_text,ssr.status,ssr.apply_on,info.ssr_sub_cat_id, cats.ssr_cat_id ");
		sql.append("FROM t_pnr_pax_fare fare,");
		sql.append("t_pnr_pax_fare_segment seg,");
		sql.append("t_pnr_pax_segment_ssr ssr, ");
		sql.append("t_ssr_info info, t_ssr_sub_category sub, t_ssr_category cats ");
		sql.append("WHERE fare.pnr_pax_id IN (" + Util.constructINStringForInts(pnrPaxIds) + ") ");
		if (!isExcludeSegment) {
			sql.append("AND seg.pnr_seg_id IN (" + Util.constructINStringForInts(pnrSegIds) + ") ");
		}
		sql.append("AND info.ssr_sub_cat_id = sub.ssr_sub_cat_id ");
		sql.append("AND sub.ssr_cat_id = cats.ssr_cat_id ");
		sql.append("AND fare.ppf_id = seg.ppf_id ");
		sql.append("AND info.ssr_id = ssr.ssr_id ");
		sql.append("AND seg.ppfs_id = ssr.ppfs_id ");
		sql.append("AND ssr.status='CNF' ");// JIRA: AARESAA-2675 - Lalanthi

		if (isToSendPNL) {
			sql.append("AND cats.send_in_pnl_adl = 'Y' ");
		}
		if (isToSendEmail) {
			sql.append("AND cats.send_in_email = 'Y' ");
		}
		sql.append("ORDER BY PNR_PAX_ID, info.ssr_code ");

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		final boolean setUniqueSSRPerPax = isExcludeSegment;

		Map<Integer, Collection<PaxSSRDetailDTO>> mapSSRDetailDTOs = (Map<Integer, Collection<PaxSSRDetailDTO>>) jt.query(
				sql.toString(), new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Collection<PaxSSRDetailDTO>> mapPaxSSRs = new HashMap<Integer, Collection<PaxSSRDetailDTO>>();

						if (rs != null) {
							Integer prePNRPaxId = null;
							String preSSRCode = null;
							Integer prePnrSegId = null;
							Collection<PaxSSRDetailDTO> ssrDetailDTOs = new ArrayList<PaxSSRDetailDTO>();

							Integer pnrPaxId = null;
							String ssrCode = null;
							Integer ssrCategoryId = null;
							Integer pnrSegId = null;
							while (rs.next()) {
								pnrPaxId = rs.getInt("pnr_pax_id");
								ssrCode = rs.getString("ssr_code");
								ssrCategoryId = rs.getInt("ssr_cat_id");
								pnrSegId = rs.getInt("pnr_seg_id");
								if (prePNRPaxId != null && !prePNRPaxId.equals(pnrPaxId)) {
									mapPaxSSRs.put(prePNRPaxId, ssrDetailDTOs); // Put the existing pax's ssrDetailDTOs
																				// to the map
									ssrDetailDTOs = new ArrayList<PaxSSRDetailDTO>();
									prePNRPaxId = null;
								}

								if (setUniqueSSRPerPax) { // FIXME Temporary fix till SSR per Segment enabled in
															// frontend
									if (prePNRPaxId != null && prePNRPaxId.equals(pnrPaxId) && preSSRCode != null
											&& preSSRCode.equals(ssrCode) && prePnrSegId != null && prePnrSegId.equals(pnrSegId)) {
										continue; // Take only unique SSR per pax (considering segment wise)
									}
								}

								PaxSSRDetailDTO segmentSSRDTO = new PaxSSRDetailDTO();

								segmentSSRDTO.setPnrPaxId(pnrPaxId);
								segmentSSRDTO.setSsrId(rs.getInt("ssr_id"));
								segmentSSRDTO.setSsrCode(ssrCode);
								segmentSSRDTO.setSsrText(rs.getString("ssr_text"));
								segmentSSRDTO.setStatus(rs.getString("status"));
								segmentSSRDTO.setSsrSubCatID(new Integer(rs.getInt("ssr_sub_cat_id")));
								segmentSSRDTO.setPNRSegId(new Integer(rs.getInt("pnr_seg_id")));
								segmentSSRDTO.setSsrDesc(rs.getString("ssr_desc"));
								segmentSSRDTO.setSsrCatId(ssrCategoryId);

								ssrDetailDTOs.add(segmentSSRDTO);
								prePNRPaxId = pnrPaxId;
								preSSRCode = ssrCode;
								prePnrSegId = pnrSegId;
							}

							if (prePNRPaxId != null) {
								mapPaxSSRs.put(prePNRPaxId, ssrDetailDTOs);
							}
						}
						return mapPaxSSRs;
					}
				});
		return mapSSRDetailDTOs;
	}

	public Map<Integer, List<AncillaryDTO>> getPassengerSSRDetails(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds, 
			DCS_PAX_MSG_GROUP_TYPE msgType) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String sql ;

		if (DCS_PAX_MSG_GROUP_TYPE.PAL_CAL_AC.equals(msgType)) {
			sql = getPassengerSSRDetailsForPalCal(pnrPaxIds, pnrSegIds,msgType);
		} else if (DCS_PAX_MSG_GROUP_TYPE.PAL_CAL_D.equals(msgType)) {
			sql = getPassengerSSRDetailsForPalCal(pnrPaxIds, pnrSegIds,msgType);
		} else {
			sql = getPassengerSSRDetailsForPnlAdl(pnrPaxIds, pnrSegIds);
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		@SuppressWarnings("unchecked")
		Map<Integer, List<AncillaryDTO>> mapSSRDetailDTOs = (Map<Integer, List<AncillaryDTO>>) jt.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, List<AncillaryDTO>> mapPaxSSRs = new HashMap<Integer, List<AncillaryDTO>>();

						if (rs != null) {

							while (rs.next()) {
								Integer pnrPaxId = rs.getInt("pnr_pax_id");
								if (mapPaxSSRs.get(pnrPaxId) == null) {
									List<AncillaryDTO> seats = new ArrayList<AncillaryDTO>();
									AncillaryDTO anci = new AncillaryDTO();
									anci.setAnciType(AnciTypes.SSR);
									anci.setCode(rs.getString("ssr_code"));
									anci.setDescription(rs.getString("ssr_text") != null
											? rs.getString("ssr_text").toUpperCase()
											: "");
									seats.add(anci);
									mapPaxSSRs.put(pnrPaxId, seats);

								} else {
									AncillaryDTO anci = new AncillaryDTO();
									anci.setAnciType(AnciTypes.SSR);
									anci.setCode(rs.getString("ssr_code"));
									anci.setDescription(rs.getString("ssr_text") != null
											? rs.getString("ssr_text").toUpperCase()
											: "");
									mapPaxSSRs.get(pnrPaxId).add(anci);

								}

							}

						}
						return mapPaxSSRs;
					}
				});
		return mapSSRDetailDTOs;
	}

	private String getPassengerSSRDetailsForPnlAdl(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT fare.pnr_pax_id,seg.pnr_seg_id,info.ssr_code,info.ssr_desc,ssr.ssr_id,");
		sql.append("ssr.ssr_text,ssr.status,ssr.apply_on,info.ssr_sub_cat_id, cats.ssr_cat_id ");
		sql.append("FROM t_pnr_pax_fare fare,");
		sql.append("t_pnr_pax_fare_segment seg,");
		sql.append("t_pnr_pax_segment_ssr ssr, ");
		sql.append("t_ssr_info info, t_ssr_sub_category sub, t_ssr_category cats ");
		sql.append("WHERE fare.pnr_pax_id IN (" + Util.constructINStringForInts(pnrPaxIds) + ") ");
		sql.append("AND seg.pnr_seg_id IN (" + Util.constructINStringForInts(pnrSegIds) + ") ");

		sql.append("AND info.ssr_sub_cat_id = sub.ssr_sub_cat_id ");
		sql.append("AND sub.ssr_cat_id = cats.ssr_cat_id ");
		sql.append("AND fare.ppf_id = seg.ppf_id ");
		sql.append("AND info.ssr_id = ssr.ssr_id ");
		sql.append("AND seg.ppfs_id = ssr.ppfs_id ");
		sql.append("AND ssr.status='CNF' ");
		sql.append("AND cats.send_in_pnl_adl = 'Y' ");
		
		sql.append("ORDER BY PNR_PAX_ID, info.ssr_code ");

		return sql.toString();
	}
	
	private String getPassengerSSRDetailsForPalCal(Collection<Integer> pnrPaxIds, Collection<Integer> pnrSegIds,
			DCS_PAX_MSG_GROUP_TYPE msgType) {
		StringBuilder sql = new StringBuilder();
		if (DCS_PAX_MSG_GROUP_TYPE.PAL_CAL_AC.equals(msgType)) {
			sql.append("SELECT fare.pnr_pax_id,seg.pnr_seg_id,info.ssr_code,info.ssr_desc,ssr.ssr_id,");
			sql.append("ssr.ssr_text,ssr.status,ssr.apply_on,info.ssr_sub_cat_id, cats.ssr_cat_id ");
			sql.append("FROM t_pnr_pax_fare fare,");
			sql.append("t_pnr_pax_fare_segment seg,");
			sql.append("t_pnr_pax_segment_ssr ssr, ");
			sql.append("t_ssr_info info, t_ssr_sub_category sub, t_ssr_category cats ");
			sql.append("WHERE fare.pnr_pax_id IN (" + Util.constructINStringForInts(pnrPaxIds) + ") ");
			sql.append("AND seg.pnr_seg_id IN (" + Util.constructINStringForInts(pnrSegIds) + ") ");

			sql.append("AND info.ssr_sub_cat_id = sub.ssr_sub_cat_id ");
			sql.append("AND sub.ssr_cat_id = cats.ssr_cat_id ");
			sql.append("AND fare.ppf_id = seg.ppf_id ");
			sql.append("AND info.ssr_id = ssr.ssr_id ");
			sql.append("AND seg.ppfs_id = ssr.ppfs_id ");
			sql.append("AND ssr.status='CNF' ");
			sql.append("AND cats.send_in_pnl_adl = 'Y' ");
			sql.append("AND info.VALID_FOR_PALCAL = 'Y' ");
			sql.append("ORDER BY PNR_PAX_ID, info.ssr_code ");

		} else {
			sql.append("SELECT fare.pnr_pax_id,seg.pnr_seg_id,info.ssr_code,info.ssr_desc,ssr.ssr_id,");
			sql.append("ssr.ssr_text,ssr.status,ssr.apply_on,info.ssr_sub_cat_id, cats.ssr_cat_id ");
			sql.append("FROM t_pnr_pax_fare fare,");
			sql.append("t_pnr_pax_fare_segment seg,");
			sql.append("t_pnr_pax_segment_ssr ssr, ");
			sql.append("t_ssr_info info, t_ssr_sub_category sub, t_ssr_category cats ");
			sql.append("WHERE fare.pnr_pax_id IN (" + Util.constructINStringForInts(pnrPaxIds) + ") ");
			sql.append("AND seg.pnr_seg_id IN (select PNR_SEG_ID from T_PNR_SEGMENT ");
			sql.append("WHERE PNR IN (select pnr from T_PNR_PASSENGER ");
			sql.append("WHERE PNR_PAX_ID IN (" + Util.constructINStringForInts(pnrPaxIds) + "))) ");
			sql.append("AND info.ssr_sub_cat_id = sub.ssr_sub_cat_id ");
			sql.append("AND sub.ssr_cat_id = cats.ssr_cat_id ");
			sql.append("AND fare.ppf_id = seg.ppf_id ");
			sql.append("AND info.ssr_id = ssr.ssr_id ");
			sql.append("AND seg.ppfs_id = ssr.ppfs_id ");
			sql.append("AND ssr.status ='CNX'  ");
			sql.append("AND cats.send_in_pnl_adl = 'Y' ");
			sql.append("AND info.VALID_FOR_PALCAL = 'Y' ");
			sql.append("ORDER BY PNR_PAX_ID, info.ssr_code ");
		}

		return sql.toString();
	}
	
	

	public boolean checkSSRChargeAssignedInReservation(int ssrId) throws CommonsDataAccessException {
		Object[] args = { ssrId };
		String sql = "SELECT count(*) FROM T_PNR_PAX_SEGMENT_SSR WHERE SSR_ID = ? ";
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		long count = jt.queryForObject(sql, args, Long.class);

		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<ReservationLiteDTO, List<PaxSSRDTO>> getExpiredSSRs(Collection<Integer> cancellationIds, String marketingCarrier) {
		boolean isDry = AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier) ? false : true;

		StringBuilder query = new StringBuilder();
		query.append("SELECT r.pnr, r.version, ppss.ssr_id, si.ssr_code, ppss.airport_code, pp.pax_sequence, ppss.ppss_id, ppss.auto_cancellation_id FROM t_reservation r, t_pnr_passenger pp,");
		query.append(" t_pnr_segment ps, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs, t_pnr_pax_segment_ssr ppss, t_ssr_info si");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id=ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id");
		query.append(" AND ppss.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id = ps.pnr_seg_id  AND ppss.ssr_id=si.ssr_id");
		query.append(" AND r.status =? AND pp.status =? AND ps.status =? AND ppss.status <> ? AND r.DUMMY_BOOKING = ? AND ");
		query.append(Util.getReplaceStringForIN("ppss.auto_cancellation_id", cancellationIds));
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.ORIGIN_CHANNEL_CODE ");
		if (isDry) {
			query.append("= ?");
		} else {
			query.append("<> ?");
		}

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL,
				String.valueOf(ReservationInternalConstants.DummyBooking.NO), marketingCarrier,
				ReservationInternalConstants.SalesChannel.LCC };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		return (Map<ReservationLiteDTO, List<PaxSSRDTO>>) template.query(query.toString(), args, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<ReservationLiteDTO, List<PaxSSRDTO>> pnrWiseSSRs = null;
				if (rs != null) {
					pnrWiseSSRs = new HashMap<ReservationLiteDTO, List<PaxSSRDTO>>();
					while (rs.next()) {
						String pnr = rs.getString("pnr");
						long version = rs.getLong("version");
						Integer autoCancellationId = rs.getInt("auto_cancellation_id");

						ReservationLiteDTO resDTO = new ReservationLiteDTO(pnr, version, autoCancellationId);

						if (!pnrWiseSSRs.containsKey(resDTO)) {
							pnrWiseSSRs.put(resDTO, new ArrayList<PaxSSRDTO>());
						}

						PaxSSRDTO paxSSRTO = new PaxSSRDTO();
						paxSSRTO.setSsrId(rs.getInt("ssr_id"));
						paxSSRTO.setSsrCode(rs.getString("ssr_code"));
						paxSSRTO.setAirportCode(rs.getString("airport_code"));
						paxSSRTO.setPaxSeq(rs.getInt("pax_sequence"));
						paxSSRTO.setPnrPaxSegmentSSRId(rs.getInt("ppss_id"));
						paxSSRTO.setUserDefinedSSR("Y".equals(rs.getString("user_defined_ssr")));
						pnrWiseSSRs.get(resDTO).add(paxSSRTO);

					}
				}
				return pnrWiseSSRs;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Integer> getExpiredLccSsrInfo(String marketingCarrier, Collection<Integer> cancellationIds) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT DISTINCT r.pnr, ppss.auto_cancellation_id FROM t_reservation r, t_pnr_passenger pp,");
		query.append(" t_pnr_segment ps, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs, t_pnr_pax_segment_ssr ppss, t_ssr_info si");
		query.append(" WHERE r.pnr = pp.pnr AND r.pnr = ps.pnr AND pp.pnr_pax_id=ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id");
		query.append(" AND ppss.ppfs_id = ppfs.ppfs_id AND ppfs.pnr_seg_id = ps.pnr_seg_id  AND ppss.ssr_id=si.ssr_id");
		query.append(" AND r.status =? AND pp.status =? AND ps.status =? AND ppss.status <> ? AND r.ORIGIN_CHANNEL_CODE = ? ");
		query.append(" AND r.ORIGIN_CARRIER_CODE = ? AND r.DUMMY_BOOKING = ? AND ppss.auto_cancellation_id IN (");
		query.append(BeanUtils.constructINStringForInts(cancellationIds) + ")");

		Object[] args = new Object[] { ReservationInternalConstants.ReservationStatus.CONFIRMED,
				ReservationInternalConstants.ReservationPaxStatus.CONFIRMED,
				ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED,
				ReservationInternalConstants.SegmentAncillaryStatus.CANCEL, ReservationInternalConstants.SalesChannel.LCC,
				marketingCarrier, String.valueOf(ReservationInternalConstants.DummyBooking.NO) };

		JdbcTemplate template = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		return (Map<String, Integer>) template.query(query.toString(), args, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> autoCnxPNRs = new HashMap<String, Integer>();
				if (rs != null) {
					while (rs.next()) {
						String pnr = BeanUtils.nullHandler(rs.getString("pnr"));
						Integer autoCnxId = rs.getInt("auto_cancellation_id");
						autoCnxPNRs.put(pnr, autoCnxId);
					}
				}

				return autoCnxPNRs;
			}
		});
	}

	public Map<Integer, List<SSRExternalChargeDTO>> getPasssengerSSRDetails(String pnr) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ps.segment_seq , fs.segment_code, pp.pnr_pax_id, ppss.* ");
		sql.append(" FROM t_pnr_pax_segment_ssr ppss, t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, ");
		sql.append(" 	t_flight_segment fs, t_pnr_pax_fare ppf, t_pnr_passenger pp ");
		sql.append(" WHERE pp.pnr       = '" + pnr + "' ");
		sql.append(" AND ppss.status    = 'CNF' ");
		sql.append(" AND ppss.ppfs_id   = ppfs.ppfs_id ");
		sql.append(" AND ps.pnr_seg_id = ppfs.pnr_seg_id ");
		sql.append(" AND ppfs.ppf_id    = ppf.ppf_id ");
		sql.append(" AND ppf.pnr_pax_id = pp.pnr_pax_id ");
		sql.append(" AND fs.flt_seg_id = ps.flt_seg_id ");

		@SuppressWarnings("unchecked")
		Map<Integer, List<SSRExternalChargeDTO>> mapSSRDetailDTOs = (Map<Integer, List<SSRExternalChargeDTO>>) jt.query(sql.toString(),
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, List<SSRExternalChargeDTO>> mapPaxSSRs = new HashMap<Integer, List<SSRExternalChargeDTO>>();

						if (rs != null) {

							while (rs.next()) {
								Integer pnrPaxId = rs.getInt("pnr_pax_id");
								if (mapPaxSSRs.get(pnrPaxId) == null) {
									List<SSRExternalChargeDTO> ssr = new ArrayList<SSRExternalChargeDTO>();
									SSRExternalChargeDTO anci = new SSRExternalChargeDTO();
									anci.setSSRId(rs.getInt("ssr_id"));
									anci.setSegmentSequece(rs.getInt("segment_seq"));
									anci.setSSRText(rs.getString("ssr_text") != null
											? rs.getString("ssr_text").toUpperCase()
											: "");
									anci.setSegmentCode(rs.getString("segment_code"));
									ssr.add(anci);
									mapPaxSSRs.put(pnrPaxId, ssr);

								} else {
									SSRExternalChargeDTO anci = new SSRExternalChargeDTO();
									anci.setSSRId(rs.getInt("ssr_id"));
									anci.setSegmentSequece(rs.getInt("segment_seq"));
									anci.setSSRText(rs.getString("ssr_text") != null
											? rs.getString("ssr_text").toUpperCase()
											: "");
									anci.setSegmentCode(rs.getString("segment_code"));
									mapPaxSSRs.get(pnrPaxId).add(anci);

								}

							}

						}
						return mapPaxSSRs;
					}
				});
		return mapSSRDetailDTOs;
	}

}
