package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * User action is only when cancel segment, this is applicable only when there is a balance due amount remain for PAX.
 * in-order to prevent this additional amount due on cancel segment flow. This command class will calculate relevant
 * minus charge adjustment for each individual PAX and apply respectively.
 * 
 * 
 * 
 * @author M.Rikaz
 * @since June 10,2014
 * 
 * @isa.module.command name="adjustAmountDue"
 */

public class AdjustAmountDue extends DefaultBaseCommand {
	private static Log log = LogFactory.getLog(AdjustAmountDue.class);
	private Reservation reservation = null;
	private Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap = null;

	@SuppressWarnings({ "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside Adjust Amount Due");

		if (AppSysParamsUtil.isSystemChargeAdjustmentForDueAmountEnabled()) {
			Collection<OndFareDTO> ondFareDTOs = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);
			paxWiseAdjustmentAmountMap = (Map<Integer, BigDecimal>) this
					.getParameter(CommandParamNames.PAX_ADJUSTMENT_AMOUNT_MAP);
			String pnr = (String) this.getParameter(CommandParamNames.PNR);
			reservation = loadReservation(pnr);

			this.validateParams(reservation, paxWiseAdjustmentAmountMap, ondFareDTOs);

			boolean isCNFReservation = ReservationStatus.CONFIRMED.equals(reservation.getStatus()) ? true : false;
			if (isCNFReservation && !isForcedConfirmedStatus(reservation.getPassengers()) ) {
				this.calculateBalanceDueAdjustment(ondFareDTOs);
			}

		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		log.debug("Exit Adjust Amount Due");

		return response;
	}

	private void calculateBalanceDueAdjustment(Collection<OndFareDTO> ondFareDTOs) throws ModuleException {

		if (ondFareDTOs != null && ondFareDTOs.size() > 0 && paxWiseAdjustmentAmountMap != null
				&& paxWiseAdjustmentAmountMap.size() > 0) {
			BigDecimal totalDueAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (Integer pnrPaxId : paxWiseAdjustmentAmountMap.keySet()) {
				totalDueAmount = AccelAeroCalculator.add(totalDueAmount, paxWiseAdjustmentAmountMap.get(pnrPaxId));
			}

			int totalPaxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
			int totalValidSectors = 1;

			// TODO:applying only for the first future segment, if same fare id duplicated then saving the
			// ReservationPaxOndCharge may difficult at addSegment command
			// for (OndFareDTO ondFareDTO : ondFareDTOs) {
			// if (!ondFareDTO.isFlown()) {
			// totalValidSectors++;
			// }
			// }

			BigDecimal[] ondWiseTotalDueAmount = AccelAeroCalculator.roundAndSplit(totalDueAmount, totalValidSectors);

			boolean hasAdult = false;
			boolean hasChild = false;
			for (ReservationPax pax : reservation.getPassengers()) {
				if (!hasAdult && PaxTypeTO.ADULT.equals(pax.getPaxType())) {
					hasAdult = true;
				}

				if (!hasChild && PaxTypeTO.CHILD.equals(pax.getPaxType())) {
					hasChild = true;
				}

				if (hasAdult && hasChild) {
					break;
				}
			}

			int ondCount = 0;
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				if (!ondFareDTO.isFlown()) {

					Map<Integer, BigDecimal> paxWiseDueAdjAmountMap = new HashMap<Integer, BigDecimal>();
					for (ReservationPax pax : reservation.getPassengers()) {
						Integer pnrPaxId = pax.getPnrPaxId();
						if (paxWiseAdjustmentAmountMap.get(pnrPaxId) != null
								&& paxWiseAdjustmentAmountMap.get(pnrPaxId).doubleValue() > 0) {

							BigDecimal paxTotalBalanceDue = paxWiseAdjustmentAmountMap.get(pnrPaxId);
							BigDecimal[] ondWisePaxDueAmount = AccelAeroCalculator.roundAndSplit(paxTotalBalanceDue,
									totalValidSectors);

							BigDecimal dueForThisOnd = ondWisePaxDueAmount[ondCount];

							paxWiseDueAdjAmountMap.put(pnrPaxId, dueForThisOnd.negate());

						}

					}

					ondFareDTO
							.injectBalanceDueAdjustment(paxWiseDueAdjAmountMap);
					break;
				}
			}

		}

	}

	private void validateParams(Reservation reservation, Map<Integer, BigDecimal> paxWiseAdjustmentAmountMap,
			Collection<OndFareDTO> ondFareDTOs) throws ModuleException {

		if (reservation == null || reservation.getPassengers() == null || ondFareDTOs == null
				|| paxWiseAdjustmentAmountMap == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

	}

	private Reservation loadReservation(String pnr) throws ModuleException {
		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}
	
	private boolean isForcedConfirmedStatus(Set<ReservationPax> passengers) {
		BigDecimal totalToPay = BigDecimal.ZERO;
		if (passengers != null && passengers.size() > 0) {
			for (ReservationPax pax : passengers) {
				if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
					if (pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
						totalToPay = AccelAeroCalculator.add(totalToPay, pax.getTotalAvailableBalance());
					}
				}
			}

			if (totalToPay.doubleValue() > 0) {
				return true;
			}
		}

		return false;
	}
}
