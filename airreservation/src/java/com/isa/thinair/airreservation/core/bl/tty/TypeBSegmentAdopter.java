package com.isa.thinair.airreservation.core.bl.tty;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.OtherAirlineSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;

public class TypeBSegmentAdopter {

	public static BookingSegmentDTO adoptResSegmentToTypeBCSSegment(ReservationSegmentDTO reservationSegmentDTO,
			String csOCCarrierCode, int paxCount) throws ModuleException {

		BookingSegmentDTO segment = new BookingSegmentDTO();
		adoptBasicSegment(reservationSegmentDTO, segment);
		segment.setNoofPax(paxCount);

		String bookingClass = null;
		if (reservationSegmentDTO.getFareTO() != null) {
			bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
		} else {
			bookingClass = reservationSegmentDTO.getCabinClassCode();
		}
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(csOCCarrierCode, bookingClass);
		segment.setCsBookingCode(gdsBookingClass);
		segment.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(csOCCarrierCode, gdsBookingClass));

		if (reservationSegmentDTO.getFlightNo() != null && reservationSegmentDTO.getFlightNo().length() > 2) {
			segment.setCsCarrierCode(reservationSegmentDTO.getFlightNo().substring(0, 2));
			segment.setCsFlightNumber(reservationSegmentDTO.getFlightNo().substring(2));
		}
		if (reservationSegmentDTO.getCsOcFlightNumber() != null && reservationSegmentDTO.getCsOcFlightNumber().length() > 2) {
			segment.setFlightNumber(reservationSegmentDTO.getCsOcFlightNumber().substring(2));
		}
		segment.setCarrierCode(reservationSegmentDTO.getCsOcCarrierCode());
		return segment;
	}

	public static BookingSegmentDTO adoptResSegmentToTypeBSegment(ReservationSegmentDTO reservationSegmentDTO,
			String carrierCode, int paxCount) throws ModuleException {

		BookingSegmentDTO segment = new BookingSegmentDTO();
		adoptBasicSegment(reservationSegmentDTO, segment);
		segment.setNoofPax(paxCount);

		String bookingClass = null;
		if (reservationSegmentDTO.getFareTO() != null) {
			bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
		} else {
			bookingClass = reservationSegmentDTO.getCabinClassCode();
		}
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(carrierCode, bookingClass);
		segment.setBookingCode(gdsBookingClass);

		if (reservationSegmentDTO.getFlightNo() != null && reservationSegmentDTO.getFlightNo().length() > 2) {
			segment.setCarrierCode(reservationSegmentDTO.getFlightNo().substring(0, 2));
			segment.setFlightNumber(reservationSegmentDTO.getFlightNo().substring(2));
		}
		return segment;
	}

	private static void adoptBasicSegment(ReservationSegmentDTO reservationSegmentDTO, BookingSegmentDTO segment) {
		segment.setDepartureStation(reservationSegmentDTO.getOrigin());
		segment.setDepartureDate(reservationSegmentDTO.getDepartureDate());
		segment.setDepartureTime(reservationSegmentDTO.getDepartureDate());
		segment.setDestinationStation(reservationSegmentDTO.getDestination());
		segment.setArrivalDate(reservationSegmentDTO.getArrivalDate());
		segment.setArrivalTime(reservationSegmentDTO.getArrivalDate());
		segment = TTYMessageCreatorUtil.setScheduleChanges(segment, reservationSegmentDTO);
	}

	public static BookingSegmentDTO adoptFlightSegmentToTypeBSegment(FlightSegmentDTO flightSegment,
			ReservationSegmentDTO reservationSegmentDTO, String carrierCode, int paxCount) throws ModuleException {

		BookingSegmentDTO segment = new BookingSegmentDTO();
		segment.setDepartureStation(flightSegment.getDepartureAirportCode());
		segment.setDepartureDate(flightSegment.getDepatureDate());
		segment.setDepartureTime(flightSegment.getDepartureDateTime());
		segment.setDestinationStation(flightSegment.getArrivalAirportCode());
		segment.setArrivalDate(flightSegment.getArrivalDateTime());
		segment.setArrivalTime(flightSegment.getArrivalDateTime());
		segment = TTYMessageCreatorUtil.setScheduleChanges(segment, reservationSegmentDTO);
		segment.setNoofPax(paxCount);

		String bookingClass = null;
		if (reservationSegmentDTO.getFareTO() != null) {
			bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
		} else {
			bookingClass = reservationSegmentDTO.getCabinClassCode();
		}
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(carrierCode, bookingClass);
		segment.setBookingCode(gdsBookingClass);

		if (flightSegment.getFlightNumber() != null && flightSegment.getFlightNumber().length() > 2) {
			segment.setCarrierCode(flightSegment.getFlightNumber().substring(0, 2));
			segment.setFlightNumber(flightSegment.getFlightNumber().substring(2));
		}
		return segment;
	}

	public static BookingSegmentDTO adoptFlightSegmentToTypeBCSSegment(FlightSegmentDTO flightSegment,
			ReservationSegmentDTO reservationSegmentDTO, String carrierCode, int paxCount) throws ModuleException {

		BookingSegmentDTO segment = new BookingSegmentDTO();
		segment.setDepartureStation(flightSegment.getDepartureAirportCode());
		segment.setDepartureDate(flightSegment.getDepatureDate());
		segment.setDepartureTime(flightSegment.getDepartureDateTime());
		segment.setDestinationStation(flightSegment.getArrivalAirportCode());
		segment.setArrivalDate(flightSegment.getArrivalDateTime());
		segment.setArrivalTime(flightSegment.getArrivalDateTime());
		segment = TTYMessageCreatorUtil.setScheduleChanges(segment, reservationSegmentDTO);
		segment.setNoofPax(paxCount);

		String bookingClass = null;
		if (reservationSegmentDTO.getFareTO() != null) {
			bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
		} else {
			bookingClass = reservationSegmentDTO.getCabinClassCode();
		}
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(carrierCode, bookingClass);
		segment.setCsBookingCode(gdsBookingClass);
		segment.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(carrierCode, gdsBookingClass));

		if (flightSegment.getFlightNumber() != null && flightSegment.getFlightNumber().length() > 2) {
			segment.setCsCarrierCode(flightSegment.getFlightNumber().substring(0, 2));
			segment.setCsFlightNumber(flightSegment.getFlightNumber().substring(2));
		}
		if (flightSegment.getCsOcFlightNumber() != null && flightSegment.getCsOcFlightNumber().length() > 2) {
			segment.setFlightNumber(flightSegment.getCsOcFlightNumber().substring(2));
		}
		segment.setCarrierCode(flightSegment.getCsOcCarrierCode());
		return segment;
	}
	
	public static BookingSegmentDTO adoptOtherAirlineSegToTypeBSegment(OtherAirlineSegment otherAirlineSegment,
			String carrierCode, int paxCount) throws ModuleException {

		BookingSegmentDTO segment = new BookingSegmentDTO();
		
		segment.setDepartureStation(otherAirlineSegment.getOrigin());
		segment.setDepartureDate(otherAirlineSegment.getDepartureDateTimeLocal());
		segment.setDepartureTime(otherAirlineSegment.getDepartureDateTimeLocal());
		segment.setDestinationStation(otherAirlineSegment.getDestination());
		segment.setArrivalDate(otherAirlineSegment.getArrivalDateTimeLocal());
		segment.setArrivalTime(otherAirlineSegment.getArrivalDateTimeLocal());
		
		int dayOffSet =  CalendarUtil.getTimeDifferenceInDays(otherAirlineSegment.getDepartureDate(),otherAirlineSegment.getArrivalDateTimeLocal());
		if (dayOffSet < 0) {
			segment.setDayOffSet(Math.abs(dayOffSet));
			segment.setDayOffSetSign("M");
			segment.setChangeInScheduleExist(true);
		} else if(dayOffSet > 0){
			segment.setDayOffSet(dayOffSet);
			segment.setChangeInScheduleExist(true);
		}

		segment.setNoofPax(paxCount);
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(carrierCode, otherAirlineSegment.getBookingCode());
		segment.setCsBookingCode(gdsBookingClass);
		segment.setCsCarrierCode(carrierCode);
		segment.setCsFlightNumber(otherAirlineSegment.getFlightNumber().substring(2));
		segment.setBookingCode(otherAirlineSegment.getBookingCode());
		segment.setCarrierCode(carrierCode);
		segment.setFlightNumber(otherAirlineSegment.getFlightNumber().substring(2));
		
		return segment;
	}
}
