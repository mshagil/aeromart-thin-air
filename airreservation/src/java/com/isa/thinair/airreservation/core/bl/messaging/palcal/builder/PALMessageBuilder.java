package com.isa.thinair.airreservation.core.bl.messaging.palcal.builder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.PnlAdlMessageResponse;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.DestinationFareContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.EndElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.context.MessageHeaderElementContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;

public class PALMessageBuilder extends PalCalMessageBuilder {

	private PnlAdlMessageResponse response;
	private PassengerCollection passengerCollection;
	private BaseDataContext context;

	private MessageHeaderElementContext mHContext;;
	private DestinationFareContext destinationFareContext;
	private Map<String, String> pnrWiseGroupCodes;

	public PALMessageBuilder() {
		super(MessageTypes.PAL.toString());
		pnrWiseGroupCodes = new HashMap<String, String>();
	}
	
	@Override
	public PnlAdlMessageResponse buildMessage(BaseDataContext context) {
		this.context = context;
		this.passengerCollection = context.getPassengerCollection();
		
		response = new PnlAdlMessageResponse();
		Integer partNumber = 0;
		buildMessageHeaderElements(partNumber);		
		buildMessageResponse();
		return response;
	}
	
	private void buildMessageResponse(){
		response.setMessagePartMap(messagePartMap);
		response.setFareClassWisePaxCount(this.passengerCollection.getFareClassWisePaxCount());
		response.setPnrPaxIdvsSegmentIds(this.passengerCollection.getPnrPaxIdvsSegmentIds());
		response.setPnrCollection(this.passengerCollection.getPnrCollection());
		response.setPnrPaxVsGroupCodes(destinationFareContext.getPnrPaxVsGroupCodes());
		response.setLastGroupCode(destinationFareContext.getLastGroupCode());
	}
	
	private void buildMessageHeaderElements(Integer partNumber) {
		partNumber++;
		prepareContext(partNumber);
		initiatingHeaderElement.buildElement(destinationFareContext);
		if(isRemainingPassengersIn(passengerCollection)){
			
			endElementBuilder().buildElement(createEndElementContext(partNumber,true));
			addMessagePart(partNumber, destinationFareContext.getMessageString().toString());
			clearMessageContainers(destinationFareContext);
			buildMessageHeaderElements(partNumber);
		}else{
			endElementBuilder().buildElement(createEndElementContext(partNumber,false));
			addMessagePart(partNumber, destinationFareContext.getMessageString().toString());
		}
	}
	
	private void prepareContext(Integer partNumber){
		if(destinationFareContext == null){
			destinationFareContext = createMessageHeaderElementContext(partNumber);
		}else{
			destinationFareContext.setPartNumber(partNumber);
			destinationFareContext.setPassengersPerPart(6);
			destinationFareContext.getMessageString().setLength(0);
			destinationFareContext.getCurrentMessageLine().setLength(0);
			destinationFareContext.setPartCountExceeded(false);
		}
	}
	
	private EndElementContext createEndElementContext(Integer partNumber,boolean isRemainingPassengers){
		EndElementContext endElementContext = new EndElementContext();
		endElementContext.setPartNumber(partNumber);
		endElementContext.setMessageString(destinationFareContext.getMessageString());
		endElementContext.setCurrentMessageLine(destinationFareContext.getCurrentMessageLine());
		endElementContext.setMoreElements(isRemainingPassengers);
		endElementContext.setMessageType(context.getMessageType());
		return endElementContext;
	}
	
	private DestinationFareContext createMessageHeaderElementContext(Integer partNumber) {
		DestinationFareContext destinationFareContext = new DestinationFareContext();
		destinationFareContext.setFlightNumber(context.getFlightNumber());
		destinationFareContext.setDepartureDate(context.getFlightLocalDate());
		destinationFareContext.setMessageType(MessageTypes.PAL.toString());
		destinationFareContext.setDepartureAirportCode(context.getDepartureAirportCode());
		destinationFareContext.setHeaderElement(MessageTypes.PAL.toString());
		destinationFareContext.setClassCodeElements(getClassCodeLementsMap());
		destinationFareContext.setDestinationFareElement(passengerCollection.getPnlAdlDestinationMap());
		destinationFareContext.setPnrWisePassengerCount(context.getPassengerCollection().getPnrWisePassengerCount());
		destinationFareContext.setPnrWisePassengerReductionCount(context.getPassengerCollection().getPnrWisePassengerReductionCount());
		destinationFareContext.setPnrWiseGroupCodes(pnrWiseGroupCodes);
		destinationFareContext.setGroupCodesList(context.getPassengerCollection().getGroupCodes());
		destinationFareContext.setFeaturePack(context.getFeaturePack());
		destinationFareContext.setPartNumber(partNumber);
		destinationFareContext.setPartNumbers(messagePartMap);
		destinationFareContext.setLastGroupCode(passengerCollection.getLastGroupCode());
		destinationFareContext.setPassengersPerPart(6);
		destinationFareContext.setPnrPaxVsGroupCodes(context.getPassengerCollection().getPnrPaxVsGroupCodes());
		destinationFareContext.setGroupCodeAvailabilityMap(new HashMap<String,Boolean>());
		return destinationFareContext;
	}

	private Map<String,List<String>> getClassCodeLementsMap(){
		Map<String,List<String>> classCodeElementsMap = null;
		if(context.getPassengerCollection() != null){
			classCodeElementsMap = context.getPassengerCollection().getRbdFareCabinMap();
		}
		return classCodeElementsMap;
	}
	
	@Override
	public void buildBridge() {
		// TODO Auto-generated method stub

	}
}
