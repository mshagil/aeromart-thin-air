/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityRules;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to brought forward credit This class introduced to brought forward credit. Normally credit carry forward and
 * credit brought forward linked together. To have a credit BF we must have a CF operation. Introduce this command
 * because we need to reverse credit CF operation on erroneous scenarios having multiple airlines. Credit CF might be
 * done in one transaction so we have to do the reversal in another transaction. This command class will use for that
 * purpose
 * 
 * 
 * @author charith
 * @isa.module.command name="broughtForwardCredit"
 */
public class BroughtForwardCredit extends DefaultBaseCommand {

	private ReservationCreditDAO reservationCreditDao;
	private ReservationTnxDAO reservationTnxDao;

	/**
	 * constructor of the BroughtForwardCredit command
	 */
	public BroughtForwardCredit() {
		reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/**
	 * execute method of the BroughtForwardCredit command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		PaxCreditDTO paxCreditDTO = (PaxCreditDTO) this.getParameter(CommandParamNames.PAX_CREDIT_TO);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Collection<ReservationAudit> reservationAudit = new ArrayList<ReservationAudit>();

		// checking params
		this.checkParams(paxCreditDTO, credentialsDTO);

		ReservationTnx creditTnx = TnxFactory.getCreditInstance(paxCreditDTO.getDebitPaxId(), paxCreditDTO.getBalance(),
				ReservationTnxNominalCode.CREDIT_BF.getCode(), credentialsDTO, CalendarUtil.getCurrentSystemTimeInZulu(), null,
				paxCreditDTO.getPayCarrier(), paxCreditDTO.getLccUniqueId(), false, true);
		creditTnx.setPayCurrencyAmount(paxCreditDTO.getBalance());
		creditTnx.setPayCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		reservationTnxDao.saveTransaction(creditTnx);

		// Note transaction break down is not recorded here. We don't need that here as we do not record it even in CF.

		ReservationCredit credit = reservationCreditDao.getReservationCredit(Long.valueOf(paxCreditDTO.getPaxCreditId()));
		// Refunded amount is passed from carry forward and will be set in to base carrier credit amount in
		// travellerCredit in LCC. So it is guaranteed that the charged amount is passed back again
		// with out any conversions
		credit.setBalance(AccelAeroCalculator.add(credit.getBalance(), paxCreditDTO.getBalance()));
		
		// Refund order
		Collection<String> colChargeGroupCodes = TnxGranularityRules.getRefundChargeGroupFulFillmentOrder();

		// adjust the payment charges collection in ReservationCredit
		TnxGranularityUtils.validateAndAdjustWithReservationCredit(credit, colChargeGroupCodes);
		
		reservationCreditDao.saveReservationCredit(credit);

		// construct audit
		reservationAudit.addAll(ReservationCoreUtils.composeAuditForCarryForwardCredit(paxCreditDTO));

		// constructing and return command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, reservationAudit);

		return response;
	}

	/**
	 * private method to validate parameters
	 * 
	 * @param carrier
	 *            carrier the credit is belongs to
	 * @param credentialsDTO
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(PaxCreditDTO paxCreditDTO, CredentialsDTO credentialsDTO) throws ModuleException {

		if (paxCreditDTO.getDebitPaxId() == null || paxCreditDTO.getPayCarrier() == null || paxCreditDTO.getBalance() == null
				|| credentialsDTO == null) // throw exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}

}
