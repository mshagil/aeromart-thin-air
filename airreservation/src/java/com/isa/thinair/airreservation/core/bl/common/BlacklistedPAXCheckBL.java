package com.isa.thinair.airreservation.core.bl.common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.model.BlacklistPAXRule;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * 
 * @author Rajiv Croos
 *
 */
public class BlacklistedPAXCheckBL {

	/**
	 * Apply all active blacklisting rules available on top of the given reservationTOs to filter out the blacklisted
	 * PAXs available.
	 * 
	 * @param reservationTOs
	 * @param modeFlag
	 *            : if mode flag true returns the with the first blacklisted PAX found else returns all the blacklisted
	 *            PAX found
	 * @return
	 */
	public static List<BlacklistPAX> checkForBlacklistedPAX(List<BlacklisPaxReservationTO> reservationTOs, boolean modeFlag) {
		//boolean blacklistedpaxfound = false;
		List<BlacklistPAX> blacklistPAXs = new ArrayList<BlacklistPAX>();
		BlacklistPAXCriteriaTO blacklistPAXCriteriaTO = new BlacklistPAXCriteriaTO();
		ListIterator<BlacklisPaxReservationTO> reservationTOIterator = reservationTOs.listIterator();
		RuleValidator ruleValidator = new RuleValidator();
		Set<BlacklistPAX> filteredBLPax = new HashSet<BlacklistPAX>();
		List<BlacklistPAXRule> blacklistPAXRules = ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO.getActiveBlacklistPAXRules();
		BlacklistPAX blPaxtmp = null;
		List<BlacklistPAX> matchingblacklistPAXs= new ArrayList<BlacklistPAX>();
		
		if(blacklistPAXRules!=null && !blacklistPAXRules.isEmpty()){
			while (reservationTOIterator.hasNext()) {
				BlacklisPaxReservationTO paxReservationTO = (BlacklisPaxReservationTO) reservationTOIterator.next();
				for (BlacklistPAXRule paxRule : blacklistPAXRules) {
					//if (!blacklistedpaxfound) {
						
						if(paxRule.getPaxFullNameChecked().equals("Y")){
							if(!StringUtil.isNullOrEmpty(paxReservationTO.getFullName())){
								blacklistPAXCriteriaTO.setPaxFullName(paxReservationTO.getFullName());
								ruleValidator.setFullName(true);
							}
						}

						if (paxRule.getDateOfBirthChecked().equals("Y")) {
							if ((paxReservationTO.getDateOfBirth() != null)) {
								blacklistPAXCriteriaTO.setDateOfBirth(paxReservationTO.getDateOfBirth());
								ruleValidator.setDob(true);
							}
						}

						if (paxRule.getPassportNoChecked().equals("Y")) {
							if (!StringUtil.isNullOrEmpty(paxReservationTO.getPassportNo())) {
								blacklistPAXCriteriaTO.setPassportNo(paxReservationTO.getPassportNo());
								ruleValidator.setPassportNo(true);
							}
						}

						if (paxRule.getNationalityChecked().equals("Y")) {
							//if (!StringUtil.isNullOrEmpty(paxReservationTO.getNationality())) {
							if (paxReservationTO.getNationalityCode()>0) {
								blacklistPAXCriteriaTO.setNationality(String.valueOf(paxReservationTO.getNationalityCode()));
								ruleValidator.setNationality(true);
							}
						}

						
						matchingblacklistPAXs = ReservationDAOUtils.DAOInstance.BLACKLIST_PAX_DAO
								.getBlacklistedPaXByRule(blacklistPAXCriteriaTO, ruleValidator.isFullName(),
										ruleValidator.isDob(), ruleValidator.isNationality(), ruleValidator.isPassportNo());

						
						if(!matchingblacklistPAXs.isEmpty()){
							//blPaxtmp = BlacklistPAXUtil.getMatchingBlacklistPax(matchingblacklistPAXs,paxReservationTO);
							blPaxtmp = matchingblacklistPAXs.get(0);
							blPaxtmp.setFullName(paxReservationTO.getFullName());
							
							if(blPaxtmp !=null){
								filteredBLPax.add(blPaxtmp);
								if (modeFlag){
									
									blacklistPAXs.addAll(filteredBLPax);
									return blacklistPAXs;
								}
							}
						}
						resetObjects(ruleValidator , blacklistPAXCriteriaTO);
						
						/*if (modeFlag && !matchingblacklistPAXs.isEmpty()) {
							blacklistedpaxfound = true;
							//filteredBLPax.add(BlacklistPAXUtil.BlacklisPaxReservationTOToBlacklistPAX(paxReservationTO));
							filteredBLPax.add(BlacklistPAXUtil.getMatchingBlacklistPax(matchingblacklistPAXs,paxReservationTO));							
							blacklistPAXs.addAll(filteredBLPax);
							return blacklistPAXs;
						}else if(!matchingblacklistPAXs.isEmpty()){
							//filteredBLPax.add(BlacklistPAXUtil.BlacklisPaxReservationTOToBlacklistPAX(paxReservationTO));
							filteredBLPax.add(BlacklistPAXUtil.getMatchingBlacklistPax(matchingblacklistPAXs,paxReservationTO));
						}*/
					//}
				}

			}
		}		

		blacklistPAXs.addAll(filteredBLPax);

		return blacklistPAXs;
	}

	private static void resetObjects(RuleValidator ruleValidator, BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) {
		ruleValidator.setFullName(false);
		ruleValidator.setNationality(false);
		ruleValidator.setDob(false);
		ruleValidator.setPassportNo(false);
		
		blacklistPAXCriteriaTO.setPaxFullName(null);
		blacklistPAXCriteriaTO.setNationality(null);
		blacklistPAXCriteriaTO.setDateOfBirth(null);
		blacklistPAXCriteriaTO.setPassportNo(null);
	}
}

class RuleValidator {

	boolean fname;

	boolean lname;
	
	boolean fullName;

	boolean nationality;

	boolean passportNo;

	boolean dob;

	public boolean isFname() {
		return fname;
	}

	public void setFname(boolean fname) {
		this.fname = fname;
	}

	public boolean isLname() {
		return lname;
	}

	public void setLname(boolean lname) {
		this.lname = lname;
	}

	public boolean isNationality() {
		return nationality;
	}

	public void setNationality(boolean nationality) {
		this.nationality = nationality;
	}

	public boolean isPassportNo() {
		return passportNo;
	}

	public void setPassportNo(boolean passportNo) {
		this.passportNo = passportNo;
	}

	public boolean isDob() {
		return dob;
	}

	public void setDob(boolean dob) {
		this.dob = dob;
	}

	public boolean isFullName() {
		return fullName;
	}

	public void setFullName(boolean fullName) {
		this.fullName = fullName;
	}	

}
