package com.isa.thinair.airreservation.core.bl.common;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.aig.InsurableFlightSegment;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

public class InsuranceHelper {

	public static String getAudit(List<InsuranceResponse> insuranceResponses) {

		StringBuilder audit = new StringBuilder();
		HashMap<String, BigDecimal> policyContainer = new HashMap<String, BigDecimal>();
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();

		if (insuranceResponses != null && !insuranceResponses.isEmpty()) {
			for (InsuranceResponse insuranceResponse : insuranceResponses) {
				String policyCode = insuranceResponse.getPolicyCode();
				if (insuranceResponse.isSuccess() && policyCode != null) {
					if (policyContainer.containsKey(policyCode)) {
						BigDecimal currentAmount = policyContainer.get(policyCode);
						BigDecimal updatedAmount = AccelAeroCalculator.add(currentAmount,
								insuranceResponse.getTotalPremiumAmount());
						policyContainer.put(policyCode, updatedAmount);

					} else {
						policyContainer.put(policyCode, insuranceResponse.getTotalPremiumAmount());
					}
				} else {
					audit.append(insuranceResponse.getErrorCode());
				}
			}

		}

		if (policyContainer != null && !policyContainer.isEmpty()) {
			for (String policy : policyContainer.keySet()) {
				BigDecimal policyTotal = policyContainer.get(policy);
				if (audit.length() > 0) {
					audit.append(", ");
				}
				audit.append(policy);
				audit.append("/");
				audit.append(policyTotal.toString());
				audit.append(baseCurrencyCode);
			}

		}

		if (audit.length() > 0) {
			return audit.toString();
		} else {
			return null;
		}

	}

	public static List<InsurableFlightSegment> createFlightSegList(Reservation reservation) {

		List<InsurableFlightSegment> flights = new ArrayList<InsurableFlightSegment>();

		Collection<ReservationSegmentDTO> ownSegments = reservation.getSegmentsView();
		for (ReservationSegmentDTO ownSegment : ownSegments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(ownSegment.getStatus())
					&& CalendarUtil.getCurrentSystemTimeInZulu().before(ownSegment.getZuluDepartureDate())) {
				InsurableFlightSegment insuranceFlgtSeg = new InsurableFlightSegment();

				insuranceFlgtSeg.setDepartureStationCode(ownSegment.getSegmentCode().substring(0, 3));
				insuranceFlgtSeg.setDepartureDateTimeLocal(ownSegment.getDepartureDate());
				insuranceFlgtSeg.setArrivalStationCode(ownSegment.getSegmentCode().substring(
						ownSegment.getSegmentCode().length() - 3, ownSegment.getSegmentCode().length()));
				insuranceFlgtSeg.setFlightNo(ownSegment.getFlightNo());
				insuranceFlgtSeg.setDepartureDateTimeZulu(ownSegment.getZuluDepartureDate());
				flights.add(insuranceFlgtSeg);
			}
		}

		Set<ExternalPnrSegment> externalSegments = reservation.getExternalReservationSegment();

		for (ExternalPnrSegment extSegment : externalSegments) {

			ExternalFlightSegment extFltSegment = extSegment.getExternalFlightSegment();
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(extSegment.getStatus())
					&& CalendarUtil.getCurrentSystemTimeInZulu().before(extFltSegment.getEstTimeDepatureZulu())) {
				InsurableFlightSegment insuranceFlgtSeg = new InsurableFlightSegment();
				insuranceFlgtSeg.setDepartureStationCode(extFltSegment.getSegmentCode().substring(0, 3));
				insuranceFlgtSeg.setDepartureDateTimeLocal(extFltSegment.getEstTimeDepatureLocal());
				insuranceFlgtSeg.setArrivalStationCode(extFltSegment.getSegmentCode().substring(
						extFltSegment.getSegmentCode().length() - 3, extFltSegment.getSegmentCode().length()));
				insuranceFlgtSeg.setFlightNo(extFltSegment.getFlightNumber());
				insuranceFlgtSeg.setDepartureDateTimeZulu(extFltSegment.getEstTimeDepatureZulu());
				flights.add(insuranceFlgtSeg);
			}
		}

		Collections.sort(flights);
		return flights;
	}
}
