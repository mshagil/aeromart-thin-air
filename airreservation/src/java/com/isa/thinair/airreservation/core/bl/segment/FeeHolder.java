package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airpricing.api.dto.CnxModPenServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxOndChargeDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.core.util.TOAssembler;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FeeHolder {

	private BigDecimal amount;
	private CnxModPenServiceTaxDTO taxDTO;
	private String taxDepositCountry;
	private String taxDepositState;

	public FeeHolder(BigDecimal amount) {
		this.amount = amount;
	}

	public FeeHolder(CnxModPenServiceTaxDTO taxDTO, String taxDepositCountry, String taxDepositState) {
		this.amount = taxDTO.getEffectiveCnxModPenChargeAmount();
		this.taxDTO = taxDTO;
		this.taxDepositCountry = taxDepositCountry;
		this.taxDepositState = taxDepositState;
	}

	public boolean hasFees() {
		return (taxDTO != null && taxDTO.getTotalTaxAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0);
	}

	public BigDecimal getTotalTaxAmount() {
		if (taxDTO != null) {
			return taxDTO.getTotalTaxAmount();
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public BigDecimal getTotal() {
		return AccelAeroCalculator.add(getTotalTaxAmount(), getChargeAmount());
	}

	public BigDecimal getChargeAmount() {
		return this.amount;
	}

	public Collection<ReservationPaxOndCharge> getOndChargesForFee(CredentialsDTO credentialsDTO, Integer tnxSeq,
			String chargeGroup) {

		if (taxDTO != null) {
			Collection<ReservationPaxOndCharge> chgList = new ArrayList<>();
			for (ServiceTaxDTO tax : taxDTO.getCnxModPenServiceTaxes()) {

				PaxOndChargeDTO paxOndChargeDTO = new PaxOndChargeDTO();
				String taxAppliedCategory =
						AppSysParamsUtil.getChargeGroupsToQuoteGSTOnNonTicketingRevenue() == null || !AppSysParamsUtil
								.getChargeGroupsToQuoteGSTOnNonTicketingRevenue().contains(chargeGroup) ?
								ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY :
								ReservationPaxOndCharge.TAX_APPLIED_FOR_NON_TICKETING_CATEGORY;

				paxOndChargeDTO.setTaxableAmount(tax.getTaxableAmount());
				paxOndChargeDTO.setNonTaxableAmount(tax.getNonTaxableAmount());
				paxOndChargeDTO.setTaxDepositCountryCode(taxDepositCountry);
				paxOndChargeDTO.setTaxDepositStateCode(taxDepositState);
				paxOndChargeDTO.setTaxAppliedCategory(taxAppliedCategory);

				ReservationPaxOndCharge ondCharge = TOAssembler.createReservationPaxOndCharge(tax.getAmount(), null,
						tax.getChargeRateId(), tax.getChargeGroupCode(), credentialsDTO, false, 0, null, null, paxOndChargeDTO,
						tnxSeq);

				chgList.add(ondCharge);
			}
			return chgList;
		}
		return null;
	}
	
	public Collection<ReservationPaxOndCharge> getOndChargesForModFee(CredentialsDTO credentialsDTO, Integer tnxSeq) {

		if (taxDTO != null) {
			Collection<ReservationPaxOndCharge> chgList = new ArrayList<>();
			for (ServiceTaxDTO tax : taxDTO.getCnxModPenServiceTaxes()) {

				PaxOndChargeDTO paxOndChargeDTO = new PaxOndChargeDTO();

				paxOndChargeDTO.setTaxableAmount(tax.getTaxableAmount());
				paxOndChargeDTO.setNonTaxableAmount(tax.getNonTaxableAmount());
				paxOndChargeDTO.setTaxDepositCountryCode(taxDepositCountry);
				paxOndChargeDTO.setTaxDepositStateCode(taxDepositState);
				paxOndChargeDTO.setTaxAppliedCategory(ReservationPaxOndCharge.TAX_APPLIED_FOR_TICKETING_CATEGORY);

				ReservationPaxOndCharge ondCharge = TOAssembler.createReservationPaxOndCharge(tax.getAmount(), null,
						tax.getChargeRateId(), tax.getChargeGroupCode(), credentialsDTO, false, 0, null, null, paxOndChargeDTO,
						tnxSeq);

				chgList.add(ondCharge);
			}
			return chgList;
		}
		return null;
	}

}
