package com.isa.thinair.airreservation.core.bl.transfer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.TransferSeatList;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.utils.AllocateEnum;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.TransferRollForwardFlightsSearchCriteria;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for roll forward transfers
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="rollForwardTranfers"
 */
public class RollForwardTranfers extends DefaultBaseCommand {

	public final static long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
	Log log = LogFactory.getLog(getClass());

	/** Holds the inventory delegate */
	// private FlightInventoryBD flightInventoryBD;

	/**
	 * constructor of the RollForwardTranfers command
	 */
	public RollForwardTranfers() {
		// flightInventoryBD = (FlightInventoryBD) ReservationModuleUtils.getFlightInventoryBD();
	}

	/**
	 * execute method of the RollForwardTranfers command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		TransferSeatDTO transferSeatDTO = (TransferSeatDTO) this.getParameter(CommandParamNames.TRANSFER_PAX_DTO);
		Boolean isAlert = (Boolean) this.getParameter(CommandParamNames.IS_ALEART);
		AllocateEnum overAlloc = (AllocateEnum) this.getParameter(CommandParamNames.OVER_ALLOC_ENUM);
		Integer overAllocSeats = (Integer) this.getParameter(CommandParamNames.OVER_ALLOC_SEATS);
		Date fromDate = (Date) this.getParameter(CommandParamNames.ROLLFORWARD_FROM_DATE);
		Date toDate = (Date) this.getParameter(CommandParamNames.ROLLFORWARD_TO_DATE);
		Frequency includeDays = (Frequency) this.getParameter(CommandParamNames.ROLLFORWARD_INCLUDE_FREQUENCY);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);

		Boolean includeClsFlights = (Boolean) this.getParameter(CommandParamNames.INCLUDE_CLS_FLIGHTS);
		// checking params
		this.checkParams(transferSeatDTO, isAlert, overAlloc, overAllocSeats, fromDate, toDate, includeDays, includeClsFlights,
				flightAlertDTO);

		FlightBD flightDelegate = ReservationModuleUtils.getFlightBD();

		// get source flights
		List sourceList = null;
		List targetList = null;

		TransferRollForwardFlightsSearchCriteria criteria = new TransferRollForwardFlightsSearchCriteria();
		Collection sourceFlights = transferSeatDTO.getSourceFltSegSeatDists();
		Iterator itSource = sourceFlights.iterator();
		if (sourceFlights.size() == 0) {
			throw new ModuleException("airreservations.rollforwardtransfers.sourcelist.error");
		}

		TransferSeatList seatList = (TransferSeatList) itSource.next();
		Flight source = flightDelegate.getFlight(seatList.getSourceFlightId());
		criteria.setFromDate(CalendarUtil.getStartTimeOfDate(fromDate));
		criteria.setToDate(CalendarUtil.getEndTimeOfDate(toDate));
		criteria.setFlight(source);
		criteria.setIncludeClsFlights(includeClsFlights.booleanValue());
		criteria.setFrequency(includeDays);
		criteria.setSource(true);

		sourceList = flightDelegate.getLikeFlights(criteria);
		if (sourceList.size() == 0) {
			throw new ModuleException("airreservations.rollforwardtransfers.sourcelist.error");
		}

		// get possible target flights
		TransferRollForwardFlightsSearchCriteria criteriaTarget = new TransferRollForwardFlightsSearchCriteria();
		Flight target = flightDelegate.getFlight(transferSeatDTO.getTargetFlightId());

		// calculate the Day Gap
		int daysDelta = CalendarUtil.daysUntil(source.getDepartureDate(), target.getDepartureDate());

		criteriaTarget.setFromDate(CalendarUtil.getStartTimeOfDate(CalendarUtil.getOfssetAddedTime(fromDate, daysDelta)));
		criteriaTarget.setToDate(CalendarUtil.getEndTimeOfDate(CalendarUtil.getOfssetAddedTime(toDate, daysDelta)));
		criteriaTarget.setFlight(target);
		criteria.setIncludeClsFlights(includeClsFlights.booleanValue());
		criteria.setSource(false);
		targetList = flightDelegate.getLikeFlights(criteriaTarget);
		if (targetList.size() == 0) {
			throw new ModuleException("airreservations.rollforwardtransfers.targetlist.error");
		}
		// match targets with source and prepare transferSeatDTO
		Date lastFlightDate = null;
		TransferSeatDTO transDTO = null;
		Collection<TransferSeatDTO> transDtoList = new ArrayList<TransferSeatDTO>();

		Iterator itSourceList = sourceList.iterator();
		while (itSourceList.hasNext()) {
			Flight sourceFlight = (Flight) itSourceList.next();
			// find matching target
			Flight targetFlight = this.findMatchingTarget(sourceFlight, targetList, lastFlightDate, daysDelta);

			if (targetFlight != null) {
				lastFlightDate = targetFlight.getDepartureDate();
				transDTO = this.copyTransferSeatDTO(transferSeatDTO, sourceFlight, targetFlight);
				transDtoList.add(transDTO);
			}

		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.setResponseCode(ResponseCodes.ROLLFORWARD_TRANSFERS_SUCCESSFULL);
		responce.addResponceParam(CommandParamNames.TRANSFER_PAX_DTO, transDtoList);
		responce.addResponceParam(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		responce.addResponceParam(CommandParamNames.IS_ROLLFORWARD, new Boolean(true));
		responce.addResponceParam(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		return responce;
	}

	/**
	 * private method to get the matching target flight (with day difference -1,0,+1)
	 * 
	 * @param sourceFlight
	 * @param targetList
	 */
	private Flight findMatchingTarget(Flight sourceFlight, List<Flight> targetList, Date lastFlightDate, int daysDelta) {
		Flight target = null;
		Iterator<Flight> itTargets = targetList.iterator();
		boolean noCheck = false;
		boolean dateCheck = false;
		if (lastFlightDate == null)
			noCheck = true;

		while (itTargets.hasNext()) {

			target = (Flight) itTargets.next();

			if (!noCheck)
				dateCheck = (target.getDepartureDate().compareTo(lastFlightDate) > 0);

			if (noCheck || dateCheck) {

				if (CalendarUtil.daysUntil(sourceFlight.getDepartureDate(), target.getDepartureDate()) == daysDelta) {

					lastFlightDate = target.getDepartureDate();
					break;
				} else {
					target = null;
				}
			} else {
				target = null;
			}
		}

		return target;
	}

	@SuppressWarnings("unchecked")
	private TransferSeatDTO copyTransferSeatDTO(TransferSeatDTO originalTransferSeatDTO, Flight sourceFlight, Flight target) {
		TransferSeatDTO newTransferSeatDTO = null;
		// get the segments for the targetflight
		Collection<FlightSegement> segments = target.getFlightSegements();
		Iterator<FlightSegement> segmentsIter = segments.iterator();
		while (segmentsIter.hasNext()) {
			FlightSegement flightSegment = (FlightSegement) segmentsIter.next();
			// check if originalTrnsferSeatDTO's target segment code is equal to target flights flight-segements
			// segmentCode
			// and the target flights' flight-segments valid flag should be true;
			// if it is equal put the targe flight segments flight seg id to the new transferDTO target flight seg id.
			if (originalTransferSeatDTO.getTargetSegCode().equals(flightSegment.getSegmentCode())
					&& (flightSegment.getValidFlag() == true)) {
				newTransferSeatDTO = new TransferSeatDTO();
				newTransferSeatDTO.setSkipStandbyBookings(originalTransferSeatDTO.isSkipStandbyBookings());
				newTransferSeatDTO.setLogicalCCCode(originalTransferSeatDTO.getLogicalCCCode());
				newTransferSeatDTO.setTargetFlightId(target.getFlightId().intValue());
				newTransferSeatDTO.setTargetSegCode(originalTransferSeatDTO.getTargetSegCode());
				newTransferSeatDTO.setTargetFlightSegId(flightSegment.getFltSegId().intValue());

				// iterate throught the originalTransferSeatDTO's souruceFlightSegSeatDist -- the transfer seat list.
				Iterator<TransferSeatList> sourceSeatsIter = originalTransferSeatDTO.getSourceFltSegSeatDists().iterator();
				while (sourceSeatsIter.hasNext()) {
					TransferSeatList originalTransferSeatList = (TransferSeatList) sourceSeatsIter.next();
					if (originalTransferSeatList != null) {
						// the source flight id should be from the target flight
						// get the target flights' flight segements and
						// put the approrpriate flight seg id to the new transfer seat list's seg id
						Collection<FlightSegement> sourceSegments = sourceFlight.getFlightSegements();
						Iterator<FlightSegement> sourceSegmentsIter = sourceSegments.iterator();
						while (sourceSegmentsIter.hasNext()) {
							FlightSegement sourceSegment = (FlightSegement) sourceSegmentsIter.next();
							if (originalTransferSeatList.getSourceSegmentCode().equals(sourceSegment.getSegmentCode())) {
								TransferSeatList newTransferSeatList = new TransferSeatList();
								newTransferSeatList.setSourceSegmentId(sourceSegment.getFltSegId().intValue());
								newTransferSeatList.setSourceFlightId(sourceFlight.getFlightId().intValue());

								// copy the original transfer seat list to the new transfer seat list.
								newTransferSeatList.setSourceSegmentCode(originalTransferSeatList.getSourceSegmentCode());
								newTransferSeatList.setConfirmedInfantCount(originalTransferSeatList.getConfirmedInfantCount());
								newTransferSeatList.setTransferSeatCount(originalTransferSeatList.getTransferSeatCount());
								newTransferSeatList.setTransferAll(originalTransferSeatList.isTransferAll());

								if (newTransferSeatDTO.getSourceFltSegSeatDists() != null) {
									newTransferSeatDTO.getSourceFltSegSeatDists().add(newTransferSeatList);
								} else {
									Collection<TransferSeatList> c = new ArrayList<TransferSeatList>();
									c.add(newTransferSeatList);
									newTransferSeatDTO.setSourceFltSegSeatDists(c);
								}
								break;
							}
						}
					}
				}
				break;
			}
		}
		return newTransferSeatDTO;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param transferSeatDTO
	 * @param isAlert
	 * @param overAlloc
	 * @param overAllocSeats
	 * @param fromDate
	 * @param toDate
	 * @param excludeDays
	 * @throws ModuleException
	 */
	private void checkParams(TransferSeatDTO transferSeatDTO, Boolean isAlert, AllocateEnum overAlloc, Integer overAllocSeats,
			Date fromDate, Date toDate, Frequency includeDays, Boolean includeClsFlights, FlightAlertDTO flightAlertDTO)
			throws ModuleException {

		if (transferSeatDTO == null)// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");

		if (isAlert == null)
			isAlert = new Boolean(true);

		if (includeClsFlights == null)
			includeClsFlights = new Boolean(true);

		if (overAlloc == null)
			overAlloc = AllocateEnum.AUTO_FIT;

		if (overAlloc.equals(AllocateEnum.OVER_LIMIT) && overAllocSeats == null)// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");

		if (fromDate == null)
			throw new ModuleException("airreservations.arg.invalid.null");

		if (toDate == null)
			throw new ModuleException("airreservations.arg.invalid.null");

		if (includeDays == null)
			throw new ModuleException("airreservations.arg.invalid.null");
		if (flightAlertDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}
}
