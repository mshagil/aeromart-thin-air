package com.isa.thinair.airreservation.core.util;

/**
 * @author eric
 * 
 */
public class IATAETicketGenerator {

	public final static int NO_OF_COUPONS_PER_TICKET = 4;
	public final static int SERIAL_NO_MAX_LENGTH = 8;

	@SuppressWarnings("unused")
	private enum formCodeEnum {
		ETicket(21), PAPER_TICKET(22);

		private final int code;

		private formCodeEnum(int code) {
			this.code = code;
		}

		public int getCode() {
			return code;
		}
	}

	/**
	 * @param airlineCode
	 * @param formCode
	 * @param serialNo
	 * @return
	 */
	public static String getIATAETicketNumber(String airlineCode, String formCode, String serialNo) {

		StringBuilder sb = new StringBuilder();
		sb.append(airlineCode);// append airline code - 3 digits
		sb.append(formCode);// append the type of document + source of issue. - 2 digits
		sb.append(formatSerialNumber(serialNo)); // append the serial number.- 8 digits
		return sb.toString();

	}

	/**
	 * @param serialNo
	 * @return
	 */
	private static String formatSerialNumber(String serialNo) {
		while (serialNo.length() < SERIAL_NO_MAX_LENGTH) {
			serialNo = "0" + serialNo;
		}
		return serialNo;
	}

	/**
	 * @param segementCount
	 * @return
	 */
	public static int getNoOfTickets(int segementCount) {
		int noOfEtickets = segementCount / NO_OF_COUPONS_PER_TICKET;
		if (segementCount % NO_OF_COUPONS_PER_TICKET > 0) {
			noOfEtickets++;
		}
		return noOfEtickets;
	}

	/**
	 * @param airlineCode
	 * @param couponNo
	 * @param serialNo
	 * @return the check digit , refer 1720a - Attachment D
	 */
	@SuppressWarnings("unused")
	private static long calculateModuler7CheckDigit(int airlineCode, int noOfCoupon, long serialNo) {

		return (Long.valueOf(String.valueOf(airlineCode) + String.valueOf(noOfCoupon) + String.valueOf(serialNo)) % 7);

	}

}
