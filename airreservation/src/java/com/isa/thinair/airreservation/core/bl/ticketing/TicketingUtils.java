package com.isa.thinair.airreservation.core.bl.ticketing;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaymentModeDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegEtktTransition;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;

/**
 * @author Nilindra Fernando
 * 
 */
public class TicketingUtils {

	public static String getRecieptNoForPayment(String paymentMode) {
		Map<String, PaymentModeDTO> mapPaymentModes = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getPaymentModes();
		String receiptNumber = null;
		PaymentModeDTO paymentModeDTO = null;

		if (paymentMode.equalsIgnoreCase(paymentMode)) {
			paymentModeDTO = mapPaymentModes.get(paymentMode);

			if (paymentMode != null) {
				if (paymentModeDTO.isGenerateReceipt) {
					if (paymentModeDTO.isGenerateUniqueRecieptforMode) {
						receiptNumber = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO
								.getNextUniqueRecieptNumber(paymentMode);
					} else {
						receiptNumber = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getNextCommonRecieptNumber();
					}
				}
			}
		}
		return receiptNumber;
	}

	/**
	 * @param trackInfoDTO
	 * @return
	 */
	public static CreateTicketInfoDTO createTicketingInfoDto(TrackInfoDTO trackInfoDTO) {

		CreateTicketInfoDTO tktInfoDto = new CreateTicketInfoDTO();
		if (trackInfoDTO != null) {
			tktInfoDto.setAppIndicator(trackInfoDTO.getAppIndicator());
			tktInfoDto.setPaymentAgent(trackInfoDTO.getPaymentAgent());
		}

		return tktInfoDto;
	}

	/**
	 * @param currentPaxCoupon
	 * @param newCouponStatus
	 * @param passengerCouponAudit
	 * @return
	 */
	public static ReservationAudit createPassengerCouponUpdateAudit(LccClientPassengerEticketInfoTO currentPaxCoupon,
			String newCouponStatus, String newPaxStatus, PassengerTicketCouponAuditDTO passengerCouponAudit) {

		ReservationAudit reservationAudit = new ReservationAudit();

		reservationAudit.setPnr(passengerCouponAudit.getPnr());
		reservationAudit.setModificationType(AuditTemplateEnum.UPDATED_ETICKET.getCode());
		reservationAudit.setUserNote(passengerCouponAudit.getRemark());
		// contentMap
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PNR_PAX_ID,
				passengerCouponAudit.getPnrPaxId());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.ETICKET_NUMBER,
				currentPaxCoupon.getPaxETicketNo() + " / " + currentPaxCoupon.getCouponNo());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.FLIGHT_NUMBER,
				currentPaxCoupon.getFlightNo());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.SEGMENT_CODE,
				currentPaxCoupon.getSegmentCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.FLIGHT_DEPARTURE_DATE,
				BeanUtils.parseDateFormat(currentPaxCoupon.getDepartureDate(), "E, dd MMM yyyy HH:mm:ss"));
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.STATUS, newCouponStatus);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PREVIOUS_STATUS,
				currentPaxCoupon.getPaxETicketStatus());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PAX_STATUS, newPaxStatus);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PREVIOUS_PAX_STATUS,
				currentPaxCoupon.getPaxStatus());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.UpdatedETicket.PASSENGER_NAME,
				passengerCouponAudit.getPassengerName());

		return reservationAudit;

	}

	public static ReservationPaxFareSegEtktTransition recordEtktCouponStatusTransition(PassengerCouponUpdateRQ paxCouponUpdateRQ,
			String oldCouponStatus, String newCouponStatus) {

		if (!oldCouponStatus.equals(newCouponStatus)) {
			ReservationPaxFareSegEtktTransition resPaxFareSegEtktTransition = new ReservationPaxFareSegEtktTransition();
			resPaxFareSegEtktTransition.setPaxFareSegmentEticketId(paxCouponUpdateRQ.getEticketId());
			resPaxFareSegEtktTransition.setFromStatus(oldCouponStatus);
			resPaxFareSegEtktTransition.setToStatus(newCouponStatus);
			//TODO change this
			List<String> finalEticketStatuses = Arrays.asList(EticketStatus.FLOWN.code(), EticketStatus.EXCHANGED.code(),
					EticketStatus.VOID.code(), EticketStatus.PRINT_EXCHANGED.code(), EticketStatus.CLOSED.code(),
					EticketStatus.BOARDED.code());
			if (finalEticketStatuses.contains(oldCouponStatus)) {
				resPaxFareSegEtktTransition.setInconsistentStatus(true);
			} else {
				resPaxFareSegEtktTransition.setInconsistentStatus(false);
			}
			return resPaxFareSegEtktTransition;
		}
		return null;
	}
}
