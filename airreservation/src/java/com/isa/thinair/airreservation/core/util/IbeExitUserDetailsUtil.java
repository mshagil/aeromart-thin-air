package com.isa.thinair.airreservation.core.util;

import com.isa.thinair.airreservation.api.dto.IbeExitUserDetailsDTO;
import com.isa.thinair.airreservation.api.model.IbeUserExitDetails;

public class IbeExitUserDetailsUtil {

	public static IbeUserExitDetails convertTo(IbeExitUserDetailsDTO ibeExitDetails) {

		IbeUserExitDetails ibeUserExitDetails = new IbeUserExitDetails();
		ibeUserExitDetails.setIbeExitDetailsId(ibeExitDetails.getIbeExitDetailsId());
		ibeUserExitDetails.setSearchFlightType(ibeExitDetails.getSearchFlightType());
		ibeUserExitDetails.setPaxEmail(ibeExitDetails.getPaxEmail());
		ibeUserExitDetails.setPaxFlightSearchFrom(ibeExitDetails.getPaxFlightSearchFrom());
		ibeUserExitDetails.setPaxFlightSearchTo(ibeExitDetails.getPaxFlightSearchTo());
		ibeUserExitDetails.setPaxFlihtSearchDepartureDate(ibeExitDetails.getPaxFlihtSearchDepartureDate());
		ibeUserExitDetails.setPaxFlightSearchArrivalDate(ibeExitDetails.getPaxFlightSearchArrivalDate());
		ibeUserExitDetails.setPaxFlightSearchNoOfInfants(ibeExitDetails.getPaxFlightSearchNoOfInfants());
		ibeUserExitDetails.setPaxFlightSearchNoAdults(ibeExitDetails.getPaxFlightSearchNoAdults());
		ibeUserExitDetails.setPaxFlightSearchNoChildren(ibeExitDetails.getPaxFlightSearchNoChildren());
		ibeUserExitDetails.setExitStep(ibeExitDetails.getExitStep());
		ibeUserExitDetails.setSearchTime(ibeExitDetails.getSearchTime());
		ibeUserExitDetails.setIsClickEmailLink(ibeExitDetails.isClickedExitDetailEmailLink());
		ibeUserExitDetails.setTitle(ibeExitDetails.getTitle());
		ibeUserExitDetails.setFirstName(ibeExitDetails.getFirstName());
		ibeUserExitDetails.setLastName(ibeExitDetails.getLastName());
		ibeUserExitDetails.setNationality(ibeExitDetails.getNationality());
		ibeUserExitDetails.setCountry(ibeExitDetails.getCountry());
		ibeUserExitDetails.setLanguage(ibeExitDetails.getLanguage());
		ibeUserExitDetails.setMobileNumber(ibeExitDetails.getMobileNumber());
		ibeUserExitDetails.setTravelMobileNumber(ibeExitDetails.getTravelMobileNumber());
		
		return ibeUserExitDetails;

	}
	
	public static IbeExitUserDetailsDTO convertTo(IbeUserExitDetails ibeExitDetails) {

		IbeExitUserDetailsDTO exitDetailsDto = new IbeExitUserDetailsDTO();
		exitDetailsDto.setIbeExitDetailsId(ibeExitDetails.getIbeExitDetailsId());
		exitDetailsDto.setSearchFlightType(ibeExitDetails.getSearchFlightType());
		exitDetailsDto.setPaxEmail(ibeExitDetails.getPaxEmail());
		exitDetailsDto.setPaxFlightSearchFrom(ibeExitDetails.getPaxFlightSearchFrom());
		exitDetailsDto.setPaxFlightSearchTo(ibeExitDetails.getPaxFlightSearchTo());
		exitDetailsDto.setPaxFlihtSearchDepartureDate(ibeExitDetails.getPaxFlihtSearchDepartureDate());
		exitDetailsDto.setPaxFlightSearchArrivalDate(ibeExitDetails.getPaxFlightSearchArrivalDate());
		exitDetailsDto.setPaxFlightSearchNoOfInfants(ibeExitDetails.getPaxFlightSearchNoOfInfants());
		exitDetailsDto.setPaxFlightSearchNoAdults(ibeExitDetails.getPaxFlightSearchNoAdults());
		exitDetailsDto.setPaxFlightSearchNoChildren(ibeExitDetails.getPaxFlightSearchNoChildren());
		exitDetailsDto.setExitStep(ibeExitDetails.getExitStep());
		exitDetailsDto.setSearchTime(ibeExitDetails.getSearchTime());
		exitDetailsDto.setClickedExitDetailEmailLink(ibeExitDetails.getIsClickEmailLink());
		exitDetailsDto.setTitle(ibeExitDetails.getTitle());
		exitDetailsDto.setFirstName(ibeExitDetails.getFirstName());
		exitDetailsDto.setLastName(ibeExitDetails.getLastName());
		exitDetailsDto.setNationality(ibeExitDetails.getNationality());
		exitDetailsDto.setCountry(ibeExitDetails.getCountry());
		exitDetailsDto.setLanguage(ibeExitDetails.getLanguage());
		exitDetailsDto.setMobileNumber(ibeExitDetails.getMobileNumber());
		exitDetailsDto.setTravelMobileNumber(ibeExitDetails.getTravelMobileNumber());

		return exitDetailsDto;
	}

}
