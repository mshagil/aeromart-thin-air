package com.isa.thinair.airreservation.core.persistence.hibernate.external;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentSageDTO;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.airreservation.core.persistence.dao.CreditSalesTransferDAOImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * SageSalesDAOImpl is the business DAO hibernate implementation
 * 
 * 
 * @isa.module.dao-impl dao-name="SageSystemDAO"
 */
public class SageSalesDAOImpl extends CreditSalesTransferDAOImpl {
	
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SageSalesDAOImpl.class);
	
	/*
	 * Returns total of transferred CreditCard sales from external system
	 * 
	 * @Param fromDate
	 * @Param toDate
	 * @Param AccontingSystemTemplate
	 */
	public BigDecimal getTransferedCreditCardSalesTotal(Date fromDate, Date toDate, AccontingSystemTemplate template) {
		BigDecimal totAmount = new BigDecimal(-1);
		DataSource ds = template.getExternalDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fromDate, toDate };
		String sql = "SELECT SUM(amount) as amt FROM INT_T_CREDITCARD_SALES WHERE DATE_OF_SALE BETWEEN ?  and ? ";
		try {
			totAmount = (BigDecimal) templete.query(sql, params, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					BigDecimal amount = null;
					while (rs.next()) {
						amount = rs.getBigDecimal("amt");
					}
					return amount;

				}
			});
		} catch (Exception ex) {
			log.error("External DataBase error:" + ex);
			throw new CommonsDataAccessException(ex, "exdb.getconnection.retrieval.failed", AirreservationConstants.MODULE_NAME);
		}
		return totAmount;
	}
	
	/*
	 * Inserting CreditCard Sales data to external system
	 * 
	 * @Param Collection<CreditCardPaymentSageDTO>
	 * @Param AccontingSystemTemplate
	 * 
	 * @throws ClassNotFoundException, SQLException, Exception
	 */	
	public void insertCreditCardSalesToExternal(Collection<CreditCardPaymentSageDTO> col, AccontingSystemTemplate template) throws ClassNotFoundException, SQLException, Exception {
		log.info("##################### Going to Insert CreditCard Sales To Sage XDB");
		Connection connection = null;
		
		try {
			connection = template.getExternalConnection();
			connection.setAutoCommit(false);
			Iterator<CreditCardPaymentSageDTO> it = col.iterator();
			// common sql query
			CreditCardPaymentSageDTO dto = null;
			String sql = "insert into  INT_T_CREDITCARD_SALES (DATE_OF_SALE,ACCOUNT_CODE,CARD_TYPE_ID,AUTH_CODE,AMOUNT,CURRENCY,PNR,TRANSFER_STATUS)  values (?,?,?,?,?,?,?,?)";

			PreparedStatement stmt = connection.prepareStatement(sql);

			String sqlq = "delete from INT_T_CREDITCARD_SALES where DATE_OF_SALE=? ";
			PreparedStatement stmtq = connection.prepareStatement(sqlq);
			int i = 0;
			String cardType = null;
			
			while (it.hasNext()) {
				dto = it.next();
				if (i == 0) {
					stmtq.setDate(1, new java.sql.Date(dto.getTransactionDate().getTime()));
					stmtq.executeUpdate();
				}
				
				cardType = dto.getCardType();
				stmt.setDate(1, new java.sql.Date(dto.getTransactionDate().getTime()));
				stmt.setString(2, dto.getAccountCode());
				if (cardType != null && cardType.trim().trim().length() > 6) {
					cardType = cardType.substring(0, 6);
				}
				stmt.setString(3, cardType);
				stmt.setString(4, dto.getAuthorizedCode());
				stmt.setBigDecimal(5, dto.getAmount());
				stmt.setString(6, dto.getCurrency());
				stmt.setString(7, dto.getPnr());
				stmt.setInt(8, 0);
				if (log.isDebugEnabled()) {
					log.debug(dto.getTransactionDate().getTime() + " | " + dto.getAccountCode() + " | "+dto.getCardType()+ " | "+ dto.getAuthorizedCode() + " | "+ dto.getAmount() + " | " + dto.getCurrency() + " | "+ dto.getPnr());
				}
				stmt.executeUpdate();
				i++;
			}

		} catch (Exception exception) {
			log.error("###### Error Inserting credit card Sales to Sage XDB", exception);
			connection.rollback();
			if (exception instanceof SQLException || exception instanceof ClassNotFoundException) {
				throw exception;
			} else {
				throw new CommonsDataAccessException(exception, "transfer.creditcardsales.failed");
			}
		}

		try {
			log.debug("########## Committing Credit Card Sales to Sage XDB");
			connection.commit();

		} catch (Exception e) {
			log.error("###### Error Committing credit card Sales to Sage XDB", e);
			throw new CommonsDataAccessException(e, "transfer.creditcardsales.failed");
		} finally {
			// connection.setAutoCommit(prevAutoCommitStatus);
			connection.close();
		}
		log.info("############ Successfuly Inserted CreditCard Sales To Sage XDB");

	}
	
}
