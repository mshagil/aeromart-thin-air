package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author eric
 * 
 * @isa.module.command name="createRequoteAudit"
 * 
 */
public class CreateRequoteAudit extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CreateRequoteAudit.class);

	/** Construct the CreateRequoteAudit */
	public CreateRequoteAudit() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		Boolean isCancelOperation = (Boolean) this.getParameter(CommandParamNames.IS_CANCEL_OND_OPERATION);
		Boolean isModifyOperation = (Boolean) this.getParameter(CommandParamNames.IS_MODIFY_OND_OPERATION);
		Boolean isAddOndOperation = (Boolean) this.getParameter(CommandParamNames.IS_ADD_OND_OPERATION);
		Boolean isCNXMODChargeUpdateAudit = (Boolean) this.getParameter(CommandParamNames.IS_CNX_MOD_FEE_CHANGE_AUDIT_NEED);

		CustomChargesTO customChargesTO = (CustomChargesTO) this.getParameter(CommandParamNames.CUSTOM_CHARGES);

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		FlownAssitUnit flownAsstUnit = getParameter(CommandParamNames.FLOWN_ASSIT_UNIT, FlownAssitUnit.class);
		String pnr = (String) getParameter(CommandParamNames.PNR);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Collection<ReservationAudit> colRequoteReservationAudit = new ArrayList<ReservationAudit>();
		if (this.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION) != null) {
			colRequoteReservationAudit = (Collection<ReservationAudit>) this
					.getParameter(CommandParamNames.RESERVATION_REQUOTE_AUDIT_COLLECTION);
		}

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		if (flownAsstUnit != null && flownAsstUnit.getAuditDetails() != null && pnr != null
				&& flownAsstUnit.getAuditDetails().length() > 0) {
			ReservationAudit reservationAudit = new ReservationAudit();
			reservationAudit.setPnr(pnr);
			reservationAudit.addContentMap("penaltyCharges:", flownAsstUnit.getAuditDetails().toString());
			reservationAudit.setModificationType(AuditTemplateEnum.RES_PEN_CHARGES.getCode());
			colReservationAudit.add(reservationAudit);
		}

		if (isModifyOperation) {
			if (customChargesTO.getCustomAdultMCharge() != null || customChargesTO.getCustomChildMCharge() != null) {
				ReservationAudit reservavtionAudit = new ReservationAudit();
				reservavtionAudit.setPnr(pnr);
				
				String adultCnxCharge = overrideAuditRecordByAppendingDefaultCNXCharge(customChargesTO.getCustomAdultMCharge(),
						customChargesTO.getDefaultCustomAdultCharge());
				String childCnxCharge = overrideAuditRecordByAppendingDefaultCNXCharge(customChargesTO.getCustomChildMCharge(),
						customChargesTO.getDefaultCustomChildCharge());
				String childInfantCharge = overrideAuditRecordByAppendingDefaultCNXCharge(customChargesTO.getCustomInfantMCharge(),
						customChargesTO.getDefaultCustomInfantCharge());
				
				reservavtionAudit.addContentMap("adultModCharge", adultCnxCharge);
				reservavtionAudit.addContentMap("childModCharge", childCnxCharge);
				reservavtionAudit.addContentMap("infantModCharge", childInfantCharge);
				
				reservavtionAudit.setModificationType(AuditTemplateEnum.OVERRIDE_MOD_CHARGE.getCode());
				colReservationAudit.add(reservavtionAudit);
			}
			colReservationAudit.addAll(this.getReservationAuditList(colRequoteReservationAudit,
					AuditTemplateEnum.MODIFIED_SEGMENT));

		} else if (isCancelOperation) {
			if (customChargesTO.getCustomAdultCCharge() != null || customChargesTO.getCustomChildCCharge() != null) {
				ReservationAudit reservavtionAudit = new ReservationAudit();
				reservavtionAudit.setPnr(pnr);
				
				String adultCnxCharge = overrideAuditRecordByAppendingDefaultCNXCharge(customChargesTO.getCustomAdultCCharge(),
						customChargesTO.getDefaultCustomAdultCharge());
				String childCnxCharge = overrideAuditRecordByAppendingDefaultCNXCharge(customChargesTO.getCustomChildCCharge(),
						customChargesTO.getDefaultCustomChildCharge());
				String childInfantCharge = overrideAuditRecordByAppendingDefaultCNXCharge(customChargesTO.getCustomInfantCCharge(),
						customChargesTO.getDefaultCustomInfantCharge());
				
				reservavtionAudit.addContentMap("adultCnxCharge",adultCnxCharge);
				reservavtionAudit.addContentMap("childCnxCharge", childCnxCharge);
				reservavtionAudit.addContentMap("infantCnxCharge", childInfantCharge);
				
				reservavtionAudit.setModificationType(AuditTemplateEnum.OVERRIDE_CNX_CHARGE.getCode());
				colReservationAudit.add(reservavtionAudit);
			}
			colReservationAudit.addAll(this.getReservationAuditList(colRequoteReservationAudit,
					AuditTemplateEnum.CANCELED_SEGMENT));

		} else if (isAddOndOperation) {
			colReservationAudit.addAll(this.getReservationAuditList(colRequoteReservationAudit,
					AuditTemplateEnum.ADDED_NEW_SEGMENT));

		}
		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;

	}

	/**
	 * @param colRequoteReservationAudit
	 * @param auditType
	 * @return
	 */
	private Collection<ReservationAudit> getReservationAuditList(Collection<ReservationAudit> colRequoteReservationAudit,
			AuditTemplateEnum auditType) {

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
		for (ReservationAudit resAudit : colRequoteReservationAudit) {
			if (resAudit.getModificationType() != null && resAudit.getModificationType().equals(auditType.getCode())) {
				colReservationAudit.add(resAudit);
			}
		}
		return colReservationAudit;
	}
	
	private String overrideAuditRecordByAppendingDefaultCNXCharge(BigDecimal cancellationCharge,
			BigDecimal defaultCancellationCharge) {

		if (cancellationCharge != null) {
			if (cancellationCharge.equals(defaultCancellationCharge) || defaultCancellationCharge == null) {
				return cancellationCharge.toString();
			} else {
				StringBuffer adultModCharge = new StringBuffer(cancellationCharge.toString());
				adultModCharge.append("[").append(defaultCancellationCharge.toString()).append("]");
				return adultModCharge.toString();
			}
		} else {
			return "-";
		}
	}
}
