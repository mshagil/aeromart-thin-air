package com.isa.thinair.airreservation.core.bl.tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class OperationPayTnx {
	private Date tnxDate;
	private List<ReservationTnx> payments = null;
	private BigDecimal totalPayment = null;
	private Long key;
	private boolean isAllDummyPayments = false;

	public OperationPayTnx(Date tnxDate, Long key) {
		this.tnxDate = tnxDate;
		this.key = key;
		this.payments = new ArrayList<ReservationTnx>();
		totalPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
	}

	public void addPaymentTnx(ReservationTnx paymentTnx) {
		if (payments.size() == 0) {
			isAllDummyPayments = isDummyPayment(paymentTnx);
		} else if (isAllDummyPayments && !isDummyPayment(paymentTnx)) {
			isAllDummyPayments = false;
		}
		payments.add(paymentTnx);
		totalPayment = AccelAeroCalculator.add(totalPayment, paymentTnx.getAmount().negate());
	}

	private boolean isDummyPayment(ReservationTnx paymentTnx) {
		return ("Y".equals(paymentTnx.getDummyPayment())
				&& paymentTnx.getAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0);
	}

	public boolean isZeroPayment() {
		return (!isDummyTnx() && totalPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0);
	}

	public boolean isDummyTnx() {
		return isAllDummyPayments;
	}

	public List<ReservationTnx> getPayments() {
		return payments;
	}

	public Date getTnxDate() {
		return tnxDate;
	}

	public ReservationTnx getTotalPaymentTnx() {
		ReservationTnx tnx = new ReservationTnx();
		tnx.setAmount(totalPayment.negate());
		return tnx;
	}

	public BigDecimal getTotalPayment() {
		return totalPayment;
	}

	public OperationPayTnx clone() {
		return clone(null, null);
	}

	public OperationPayTnx clone(Date tnxDate, Long key) {
		OperationPayTnx tnx = new OperationPayTnx(tnxDate == null ? this.getTnxDate() : tnxDate,
				key == null ? this.getKey() : key);
		for (ReservationTnx payment : payments) {
			tnx.addPaymentTnx(payment);
		}
		return tnx;
	}

	public OperationPayTnx diffWith(OperationPayTnx paymentTnx, Date tnxDate) {
		OperationPayTnx tnx = new OperationPayTnx(tnxDate, this.getKey());
		ReservationTnx payTnx = new ReservationTnx();
		if (getTotalPayment().compareTo(paymentTnx.getTotalPayment()) > 0) {
			payTnx.setAmount(AccelAeroCalculator.subtract(getTotalPayment(), paymentTnx.getTotalPayment()));
		} else {
			payTnx.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		payTnx.setAmount(payTnx.getAmount().negate());
		tnx.addPaymentTnx(payTnx);
		return tnx;
	}

	@Override
	public String toString() {
		return "OperationPayTnx [tnxDate=" + tnxDate + ", totalPayment=" + totalPayment + "]";
	}

	public Long getKey() {
		return key;
	}

}
