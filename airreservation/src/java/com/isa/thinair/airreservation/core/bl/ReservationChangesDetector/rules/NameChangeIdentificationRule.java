package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.RuleExecuterDatacontext;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dto.RuleResponseDTO;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.rules.base.ResModifyIdentificationRule;
import com.isa.thinair.commons.api.exception.ModuleException;

public class NameChangeIdentificationRule extends ResModifyIdentificationRule<RuleExecuterDatacontext> {

	public NameChangeIdentificationRule(String ruleRef) {
		super(ruleRef);
	}

	@Override
	public RuleResponseDTO executeRule(RuleExecuterDatacontext dataContext) throws ModuleException {

		RuleResponseDTO response = getResponseDTO();
		List<Integer> affectedPaxId = new ArrayList<>();

		String existingName;
		String updatedName;
		for (ReservationPax existingPax : dataContext.getExisitingReservation().getPassengers()) {
			existingName = existingPax.getFirstName() + existingPax.getLastName();
			for (ReservationPax updatedPax : dataContext.getUpdatedReservation().getPassengers()) {
				if (existingPax.getPaxSequence().equals(updatedPax.getPaxSequence())) {
					updatedName = updatedPax.getFirstName() + updatedPax.getLastName();
					if (!existingName.equalsIgnoreCase(updatedName)) {
						affectedPaxId.add(existingPax.getPnrPaxId());
					}
					break;
				}
			}
		}

		updatePaxWiseAffectedSegmentListToAllGivenPax(response, getUncancelledSegIds(dataContext.getUpdatedReservation()),
				getUncancelledSegIds(dataContext.getUpdatedReservation()), affectedPaxId);
		return response;
	}

}
