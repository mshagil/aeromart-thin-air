/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules;

import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.commonrules.base.BaseRule;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.CCodeElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.NameElementRuleContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.builder.elementrules.datacontext.RulesDataContext;

/**
 * @author udithad
 *
 */
public class ClassCodeElementRule extends BaseRule<CCodeElementRuleContext> {

	public static final int MAXIMUM_CHARACTERS_PER_LINE = 64;

	@Override
	public boolean validateRule(CCodeElementRuleContext context) {
		boolean isValied = false;
		
		if(context.isRbdEnabled()){
			isValied = true;
		}
		return isValied;
	}
}
