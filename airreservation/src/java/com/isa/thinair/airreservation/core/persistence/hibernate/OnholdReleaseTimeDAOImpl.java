package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.persistence.dao.OnholdReleaseTimeDAO;
import com.isa.thinair.commons.api.dto.ConfigOnholdTimeLimitsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * 
 * @author asiri
 * 
 * @isa.module.dao-impl dao-name="OnholdReleaseTimeDAO"
 */

public class OnholdReleaseTimeDAOImpl extends PlatformBaseHibernateDaoSupport implements OnholdReleaseTimeDAO {

	private static Log log = LogFactory.getLog(OnholdReleaseTimeDAOImpl.class);

	@Override
	public void saveOrUpdateOnholdReleaseTimeConfig(OnholdReleaseTime onholdReleaseTime) throws ModuleException {
		log.info("save or update OnholdReleaseTime.....");
		hibernateSaveOrUpdate(onholdReleaseTime);
	}

	@Override
	public void deleteOnholdReleaseTimeConfig(int releaseTimeId) throws ModuleException {
		log.info("delete OnholdReleaseTime");
		OnholdReleaseTime onholdRelTimeObj = getOnholdReleaseTime(releaseTimeId);
		delete(onholdRelTimeObj);
	}

	@Override
	public OnholdReleaseTime getOnholdReleaseTime(int releaseTimeId) throws ModuleException {
		log.info("get OnholdReleaseTime.....");
		return (OnholdReleaseTime) get(OnholdReleaseTime.class, releaseTimeId);
	}

	@Override
	public Page getOnholdReleaseTimes(ConfigOnholdTimeLimitsSearchDTO configOnholdTimeLimitsSearchDTO, int start, int recSize)
			throws ModuleException {

		String[] searchCriteria = getSearchCriteria(configOnholdTimeLimitsSearchDTO, start, recSize);
		int totalresultCount = getOnholdReleaseTimeConfigCount(searchCriteria[1]);
		Collection<OnHoldReleaseTimeDTO> onholdReleaseTimes = getOnholdReleaseTimeConfigs(searchCriteria[0]);
		return new Page(totalresultCount, start, start + recSize, onholdReleaseTimes);

	}

	private String[] getSearchCriteria(ConfigOnholdTimeLimitsSearchDTO configOnholdTimeLimitsSearchDTO, int start, int recSize) {

		String[] searchCritria = new String[2];

		StringBuilder sbResultSearch = new StringBuilder(
				"SELECT * from (SELECT DISTINCT rownum seq,x.* from (SELECT DISTINCT ort.ONHOLD_RELEASE_TIME_ID, ort.AGENT_CODE, ort.IS_DOMESTIC , ort.CABIN_CLASS_CODE, ort.OND_CODE,ort.BOOKING_CODE,ort.MODULE_CODE,"
						+ "ort.RELEASE_TIME, ort.REL_TIME_WRT, ort.RANKING, ort.START_CUTOVER , ort.END_CUTOVER , ort.VERSION, ort.FARE_RULE_ID ");

		StringBuilder sbResultsCount = new StringBuilder(
				"SELECT count(ONHOLD_RELEASE_TIME_ID) as REC_COUNT from (SELECT DISTINCT ort.ONHOLD_RELEASE_TIME_ID ");

		StringBuilder filterCriteria = new StringBuilder("FROM T_ONHOLD_RELEASE_TIME ort WHERE 1 = 1");

		if (configOnholdTimeLimitsSearchDTO.getAgentCode() != null
				&& configOnholdTimeLimitsSearchDTO.getAgentCode().trim().length() > 0) {
			filterCriteria.append(" AND ort.AGENT_CODE = '" + configOnholdTimeLimitsSearchDTO.getAgentCode() + "'");
		}

		if (configOnholdTimeLimitsSearchDTO.getBookingClass() != null
				&& configOnholdTimeLimitsSearchDTO.getBookingClass().trim().length() > 0) {
			filterCriteria.append(" AND ort.BOOKING_CODE = '" + configOnholdTimeLimitsSearchDTO.getBookingClass() + "'");
		}

		if (configOnholdTimeLimitsSearchDTO.getOrigin() != null
				&& configOnholdTimeLimitsSearchDTO.getOrigin().trim().length() > 0
				&& configOnholdTimeLimitsSearchDTO.getDestination() != null
				&& configOnholdTimeLimitsSearchDTO.getDestination().trim().length() > 0) {

			filterCriteria.append(" AND ort.OND_CODE = '" + configOnholdTimeLimitsSearchDTO.getOrigin() + "/"
					+ configOnholdTimeLimitsSearchDTO.getDestination() + "'");
		}

		if (configOnholdTimeLimitsSearchDTO.getFlightType() != null
				&& configOnholdTimeLimitsSearchDTO.getFlightType().trim().length() > 0) {

			if ("DOM".equals(configOnholdTimeLimitsSearchDTO.getFlightType())) {
				filterCriteria.append(" AND ort.IS_DOMESTIC = 'Y'");
			} else {
				filterCriteria.append(" AND ort.IS_DOMESTIC = 'N'");
			}
		}

		if (configOnholdTimeLimitsSearchDTO.getSalesChannel() != null
				&& configOnholdTimeLimitsSearchDTO.getSalesChannel().trim().length() > 0) {
			filterCriteria.append(" AND ort.MODULE_CODE = '" + configOnholdTimeLimitsSearchDTO.getSalesChannel() + "'");
		}

		if (configOnholdTimeLimitsSearchDTO.getRanking() > 0 && configOnholdTimeLimitsSearchDTO.getRanking() <= 5) {
			filterCriteria.append(" AND ort.RANKING = '" + configOnholdTimeLimitsSearchDTO.getRanking() + "'");
		}

		// filterCriteria.append(" order by ort.ONHOLD_RELEASE_TIME_ID)");

		sbResultSearch.append(filterCriteria);
		sbResultsCount.append(filterCriteria);

		sbResultSearch.append(" order by ort.ONHOLD_RELEASE_TIME_ID) x)");
		sbResultsCount.append(" order by ort.ONHOLD_RELEASE_TIME_ID)");

		sbResultSearch.append(" WHERE seq BETWEEN " + start + " AND " + (start + recSize));

		searchCritria[0] = sbResultSearch.toString();
		searchCritria[1] = sbResultsCount.toString();
		return searchCritria;

	}

	private int getOnholdReleaseTimeConfigCount(String searchCountCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Integer resultCount = (Integer) jdbcTemplate.query(searchCountCriteria.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int resultCount = 0;
				while (rs.next()) {
					resultCount = rs.getInt("REC_COUNT");

				}
				return resultCount;
			}
		});
		return resultCount;
	}

	private Collection<OnHoldReleaseTimeDTO> getOnholdReleaseTimeConfigs(String searchCriteria) {

		JdbcTemplate jbdcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Collection<OnHoldReleaseTimeDTO> colAgentRequests = (Collection<OnHoldReleaseTimeDTO>) jbdcTemplate.query(
				searchCriteria.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<OnHoldReleaseTimeDTO> colOnholdReleaseTimes = new ArrayList<OnHoldReleaseTimeDTO>();
						while (rs.next()) {
							OnHoldReleaseTimeDTO onholdReleaseTimeDTO = new OnHoldReleaseTimeDTO();

							onholdReleaseTimeDTO.setOndCode(rs.getString("OND_CODE"));
							onholdReleaseTimeDTO.setReleaseTimeId(rs.getInt("ONHOLD_RELEASE_TIME_ID"));
							onholdReleaseTimeDTO.setAgentCode(rs.getString("AGENT_CODE"));
							onholdReleaseTimeDTO.setBookingClass(rs.getString("BOOKING_CODE"));
							onholdReleaseTimeDTO.setCabinClass(rs.getString("CABIN_CLASS_CODE"));

							if (rs.getString("IS_DOMESTIC").charAt(0) == 'Y') {
								onholdReleaseTimeDTO.setDomestic(true);
							} else {
								onholdReleaseTimeDTO.setDomestic(false);
							}
							onholdReleaseTimeDTO.setEndCutoverTime(rs.getLong("END_CUTOVER"));
							onholdReleaseTimeDTO.setStartCutoverTime(rs.getLong("START_CUTOVER"));
							onholdReleaseTimeDTO.setModuleCode(rs.getString("MODULE_CODE"));
							onholdReleaseTimeDTO.setReleaseTime(rs.getLong("RELEASE_TIME"));
							onholdReleaseTimeDTO.setRank(rs.getInt("RANKING"));
							onholdReleaseTimeDTO.setVersion(rs.getLong("VERSION"));
							onholdReleaseTimeDTO.setReleaseTimeWithRelativeTo(rs.getString("REL_TIME_WRT"));
							onholdReleaseTimeDTO.setFareRuleId(rs.getInt("FARE_RULE_ID"));
							colOnholdReleaseTimes.add(onholdReleaseTimeDTO);
						}
						return colOnholdReleaseTimes;
					}
				});
		return colAgentRequests;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean isOhdRelCfgLegal(OnholdReleaseTime onholdRelTime) {
		StringBuilder sbResultSearch = new StringBuilder(" SELECT COUNT(OHD.ONHOLD_RELEASE_TIME_ID) "
				+ " FROM T_ONHOLD_RELEASE_TIME OHD WHERE OHD.FARE_RULE_ID = ? "
				+ " AND (? BETWEEN OHD.END_CUTOVER AND OHD.START_CUTOVER "
				+ " OR ? BETWEEN OHD.END_CUTOVER AND OHD.START_CUTOVER) ");
		if (onholdRelTime.getRelTimeId() > 0) {
			sbResultSearch.append(" AND OHD.ONHOLD_RELEASE_TIME_ID <> " + onholdRelTime.getRelTimeId() + " ");
		}

		Object[] args;
		if (!onholdRelTime.getModuleCode().equals("ANY")) {
			sbResultSearch.append(" AND (OHD.MODULE_CODE = 'ANY' OR OHD.MODULE_CODE = ? ) ");

			args = new Object[] { onholdRelTime.getFareRuleId(), onholdRelTime.getStartCutover(), onholdRelTime.getEndCutover(),
					onholdRelTime.getModuleCode() };
		} else {
			args = new Object[] { onholdRelTime.getFareRuleId(), onholdRelTime.getStartCutover(), onholdRelTime.getEndCutover() };
		}

		JdbcTemplate jbdcTemplate = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Object intObj = jbdcTemplate.query(sbResultSearch.toString(), args, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return false;
		}
		return true;
	}

}
