package com.isa.thinair.airreservation.core.bl.revacc;

import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.segment.AgentCommissionRemovalBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for removal of agent commission
 * @author rimaz
 * @isa.module.command name="removeAgentCommission"
 * 
 * */
public class RemoveAgentCommission extends DefaultBaseCommand {

	private static final Log log = LogFactory.getLog(RemoveAgentCommission.class);

	@Override
	public ServiceResponce execute() throws ModuleException {

		log.debug("Inside removeAgentCommission");

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Set<Integer> commissionRemovalPaxIds = (Set<Integer>) this.getParameter(CommandParamNames.REMOVE_AGENT_COM_PNR_PAX_IDS);
		Boolean removeAgentCommission = getParameter(CommandParamNames.REMOVE_AGENT_COMMISSION, Boolean.FALSE, Boolean.class);

		if (isRemoveCommission(removeAgentCommission, commissionRemovalPaxIds, reservation)) {

			Iterator<ReservationPax> reservationPaxIt = reservation.getPassengers().iterator();
			ReservationPax reservationPax = null;

			while (reservationPaxIt.hasNext()) {
				reservationPax = reservationPaxIt.next();
				Integer currentPaxId = reservationPax.getPnrPaxId();

				if (commissionRemovalPaxIds.contains(currentPaxId)) {
					removeGrantedAgentCommissions(reservationPax, credentialsDTO);
				}
			}
		}

		log.debug("Leaving removeAgentCommission");

		// Saving the reservation
		ReservationProxy.saveReservation(reservation);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);
		return response;
	}

	private void removeGrantedAgentCommissions(ReservationPax reservationPax, CredentialsDTO credentialsDTO)
			throws ModuleException {

		if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax)) {
			Set<ReservationPaxFare> resPaxFares = reservationPax.getPnrPaxFares();
			for (ReservationPaxFare resPaxFare : resPaxFares) {
				for (ReservationPaxOndCharge resPaxOndCharge : resPaxFare.getCharges()) {
					if (resPaxOndCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
						AgentCommissionRemovalBO agentCommissionBO = new AgentCommissionRemovalBO(resPaxOndCharge, credentialsDTO);
						agentCommissionBO.execute();
					}
				}
			}
		}
	}

	private boolean isRemoveCommission(boolean removeCommissionFE, Set<Integer> commissionRemovalPaxIds, Reservation reservation)
			throws ModuleException {

		boolean removeCommission = false;
		if (reservation == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		if (AppSysParamsUtil.isAgentCommmissionEnabled() && removeCommissionFE && commissionRemovalPaxIds != null
				&& !commissionRemovalPaxIds.isEmpty()) {
			removeCommission = true;
		}
		return removeCommission;
	}
}
