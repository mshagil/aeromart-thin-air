/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl;

import com.isa.thinair.commons.core.framework.Command;

/**
 * Macro command definitions for air reservation module
 * 
 * @author Nilindra Fernando
 * @isa.module.command.macro-def
 */
public final class MacroCommandDef {

	private MacroCommandDef() {
	}

	/**
	 * @isa.module.command.macro-command name="splitReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="2" inner-command="splitReservation"
	 * @isa.module.command.macro-map order="3" inner-command="handlePayments"
	 * @isa.module.command.macro-map order="4" inner-command="recordModification"
	 * @isa.module.command.macro-map order="5" inner-command="constructResponse"
	 */
	private Command splitReservationMacro;

	/**
	 * @isa.module.command.macro-command name="updateReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="2" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="3" inner-command="recordModification"
	 */
	private Command updateReservationMacro;

	/**
	 * @isa.module.command.macro-command name="updateRContactInfoMacro"
	 * @isa.module.command.macro-map order="1" inner-command="updateContactInfo"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command updateRContactInfoMacro;

	/**
	 * @isa.module.command.macro-command name="cancelReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="2" inner-command="cancelReservation"
	 * @isa.module.command.macro-map order="3" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="4" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="5" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="6" inner-command="handlePayments"
	 * @isa.module.command.macro-map order="7" inner-command="recordModification"

	 */
	private Command cancelReservationMacro;

	/**
	 * @isa.module.command.macro-command name="removePassengerMacro"
	 * @isa.module.command.macro-map order="1" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="2" inner-command="splitReservation"
	 * @isa.module.command.macro-map order="3" inner-command="cancelReservation"
	 * @isa.module.command.macro-map order="4" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="5" inner-command="cMForRPassenger"
	 * @isa.module.command.macro-map order="6" inner-command="revisePromotion"
	 * @isa.module.command.macro-map order="7" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="8" inner-command="handlePayments"
	 * @isa.module.command.macro-map order="9" inner-command="recordModification"
	 */
	private Command removePassengerMacro;

	/**
	 * @isa.module.command.macro-command name="cancelSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="2" inner-command="cancelSegment"
	 * @isa.module.command.macro-map order="3" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="4" inner-command="revisePromotion"
	 * @isa.module.command.macro-map order="5" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="6" inner-command="handlePayments"
	 * @isa.module.command.macro-map order="7" inner-command="recordModification"
	 * @isa.module.command.macro-map order="8" inner-command="constructResponse"
	 */
	private Command cancelSegmentMacro;

	/**
	 * @isa.module.command.macro-command name="changeSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="4" inner-command="cancelSegment"
	 * @isa.module.command.macro-map order="5" inner-command="makeSegment"
	 * @isa.module.command.macro-map order="6" inner-command="addSegment"
	 * @isa.module.command.macro-map order="7" inner-command="retainFlexibilities"
	 * @isa.module.command.macro-map order="8" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="9" inner-command="revenueModification"
	 * @isa.module.command.macro-map order="10" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="11" inner-command="cMForChgSegment"
	 * @isa.module.command.macro-map order="12" inner-command="revisePromotion"
	 * @isa.module.command.macro-map order="13" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="14" inner-command="recordModification"
	 * @isa.module.command.macro-map order="15" inner-command="constructResponse"
	 */
	private Command changeSegmentMacro;

	/**
	 * @isa.module.command.macro-command name="requoteSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="assembleRequoteInput"
	 * @isa.module.command.macro-map order="2" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="3" inner-command="fareAdjustment"
	 * @isa.module.command.macro-map order="4" inner-command="adjustAmountDue"
	 * @isa.module.command.macro-map order="5" inner-command="reprotectExchangedSegmentAncillary" *
	 * @isa.module.command.macro-map order="6" inner-command="calculateNameChangeCharge"
	 * @isa.module.command.macro-map order="7" inner-command="calculateEffectiveTaxForServices"
	 * @isa.module.command.macro-map order="8" inner-command="makePayment"
	 * @isa.module.command.macro-map order="9" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="10" inner-command="cancelSegment"
	 * @isa.module.command.macro-map order="11" inner-command="makeSegment"
	 * @isa.module.command.macro-map order="12" inner-command="addSegment"
	 * @isa.module.command.macro-map order="13" inner-command="retainFlexibilities"
	 * @isa.module.command.macro-map order="14" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="15" inner-command="transactionSegmentAssembler"
	 * @isa.module.command.macro-map order="16" inner-command="revenueModification"
	 * @isa.module.command.macro-map order="17" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="18" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="19" inner-command="cMForChgSegment"
	 * @isa.module.command.macro-map order="20" inner-command="createRequoteAudit"
	 * @isa.module.command.macro-map order="21" inner-command="revisePromotion"
	 * @isa.module.command.macro-map order="22" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="23" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="24" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="25" inner-command="recordModification"
	 * @isa.module.command.macro-map order="26" inner-command="constructResponse"
	 */
	private Command requoteSegmentMacro;

	/**
	 * @isa.module.command.macro-command name="addSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="removeInsurance"
	 * @isa.module.command.macro-map order="4" inner-command="makeSegment"
	 * @isa.module.command.macro-map order="5" inner-command="addSegment"
	 * @isa.module.command.macro-map order="6" inner-command="retainFlexibilities"
	 * @isa.module.command.macro-map order="7" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="8" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="9" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="10" inner-command="recordModification"
	 */
	private Command addSegmentMacro;

	/**
	 * @isa.module.command.macro-command name="addInfantMacro"
	 * @isa.module.command.macro-map order="1" inner-command="makePayment"
	 * @isa.module.command.macro-map order="2" inner-command="makeInfant"
	 * @isa.module.command.macro-map order="3" inner-command="transactionSegmentAssembler"
	 * @isa.module.command.macro-map order="4" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="5" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="6" inner-command="recordModification"
	 */
	private Command addInfantMacro;

	/**
	 * @isa.module.command.macro-command name="adjustCreditMacro"
	 * @isa.module.command.macro-map order="1" inner-command="recordCreditAdjust"
	 * @isa.module.command.macro-map order="2" inner-command="cMForAdjustments"
	 * @isa.module.command.macro-map order="3" inner-command="handlePayments"
	 * @isa.module.command.macro-map order="4" inner-command="recordModification"
	 */
	private Command adjustCreditMacro;

	/**
	 * @isa.module.command.macro-command name="updateReservationPaymentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="updatePayment"
	 * @isa.module.command.macro-map order="2" inner-command="confirmPendingAgentCommissions"
	 * @isa.module.command.macro-map order="3" inner-command="confirmReservation"
	 * @isa.module.command.macro-map order="4" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="5" inner-command="transactionSegmentAssembler"
	 * @isa.module.command.macro-map order="6" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="7" inner-command="cMForUpdatePayment"
	 * @isa.module.command.macro-map order="8" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="9" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="10" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="11" inner-command="recordModification"
	 */
	private Command updateReservationPaymentMacro;

	/**
	 * @isa.module.command.macro-command name="updateReservationSplitMacro"
	 * @isa.module.command.macro-map order="1" inner-command="updatePayment"
	 * @isa.module.command.macro-map order="2" inner-command="confirmReservation"
	 * @isa.module.command.macro-map order="3" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="4" inner-command="cMForUpdatePayment"
	 * @isa.module.command.macro-map order="5" inner-command="removeAutoCancellation"
	 * @isa.module.command.macro-map order="6" inner-command="recordModification"
	 * @isa.module.command.macro-map order="7" inner-command="captureOnholdPassengers"
	 * @isa.module.command.macro-map order="8" inner-command="splitReservation"
	 * @isa.module.command.macro-map order="9" inner-command="recordModification"
	 * @isa.module.command.macro-map order="10" inner-command="constructResponse"
	 */
	private Command updateReservationSplitMacro;

	/**
	 * @isa.module.command.macro-command name="createOnholdReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="makeReservation"
	 * @isa.module.command.macro-map order="4" inner-command="grantAgentCommission"
	 * @isa.module.command.macro-map order="5" inner-command="createReservation"
	 * @isa.module.command.macro-map order="6" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="7" inner-command="adjustBlockSeats"
	 * @isa.module.command.macro-map order="8" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="9" inner-command="adjustPromotionCredit"
	 * @isa.module.command.macro-map order="10" inner-command="cMForCreateOnHoldReservation"
	 * @isa.module.command.macro-map order="11" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="12" inner-command="recordModification"
	 * @isa.module.command.macro-map order="13" inner-command="constructResponse"
	 * 
	 */
	private Command createOnholdReservationMacro;

	/**
	 * @isa.module.command.macro-command name="createPayCarrierFulfillment"
	 * @isa.module.command.macro-map order="1" inner-command="createOrUpdatePayCarrierRes"
	 * 
	 */
	private Command createPayCarrierFulfillment;

	/**
	 * @isa.module.command.macro-command name="createCCReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="makeReservation"
	 * @isa.module.command.macro-map order="4" inner-command="grantAgentCommission"
	 * @isa.module.command.macro-map order="5" inner-command="createReservation"
	 * @isa.module.command.macro-map order="6" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="7" inner-command="adjustBlockSeats"
	 * @isa.module.command.macro-map order="8" inner-command="transactionSegmentAssembler"
	 * @isa.module.command.macro-map order="9" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="10" inner-command="adjustPromotionCredit"
	 * @isa.module.command.macro-map order="11" inner-command="cMForCreateConfirmReservation"
	 * @isa.module.command.macro-map order="12" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="13" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="14" inner-command="recordModification"
	 * @isa.module.command.macro-map order="15" inner-command="issueTicket"
	 * @isa.module.command.macro-map order="16" inner-command="constructResponse"
	 */
	private Command createCCReservationMacro;

	/**
	 * @isa.module.command.macro-command name="createGDSReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="makeReservation"
	 * @isa.module.command.macro-map order="4" inner-command="createExternalFlightSegments"
	 * @isa.module.command.macro-map order="5" inner-command="createReservation"
	 * @isa.module.command.macro-map order="6" inner-command="adjustBlockSeats"
	 * @isa.module.command.macro-map order="7" inner-command="transactionSegmentAssembler"
	 * @isa.module.command.macro-map order="8" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="9" inner-command="adjustPromotionCredit"
	 * @isa.module.command.macro-map order="10" inner-command="cMForCreateConfirmReservation"
	 * @isa.module.command.macro-map order="11" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="12" inner-command="recordModification"
	 * @isa.module.command.macro-map order="13" inner-command="constructResponse"
	 */
	private Command createGDSReservationMacro;

	/**
	 * @isa.module.command.macro-command name="createWebReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="makeReservation"
	 * @isa.module.command.macro-map order="4" inner-command="createReservation"
	 * @isa.module.command.macro-map order="5" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="6" inner-command="adjustBlockSeats"
	 * @isa.module.command.macro-map order="7" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="8" inner-command="adjustPromotionCredit"
	 * @isa.module.command.macro-map order="9" inner-command="cMForCreateConfirmReservation"
	 * @isa.module.command.macro-map order="10" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="11" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="12" inner-command="recordModification"
	 * @isa.module.command.macro-map order="13" inner-command="constructResponse"
	 */
	private Command createWebReservationMacro;

	/**
	 * @isa.module.command.macro-command name="createGoShoreReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="updatePayment"
	 * @isa.module.command.macro-map order="3" inner-command="makeReservation"
	 * @isa.module.command.macro-map order="4" inner-command="createReservation"
	 * @isa.module.command.macro-map order="5" inner-command="adjustGoShoreSeats"
	 * @isa.module.command.macro-map order="6" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="7" inner-command="adjustPromotionCredit"
	 * @isa.module.command.macro-map order="8" inner-command="cMForCreateConfirmReservation"
	 * @isa.module.command.macro-map order="9" inner-command="taxInvoiceGeneration"
	 * @isa.module.command.macro-map order="10" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="11" inner-command="recordModification"
	 * @isa.module.command.macro-map order="12" inner-command="constructResponse"
	 */
	private Command createGoShoreReservationMacro;

	/**
	 * @isa.module.command.macro-command name="createSplitReservationMacro"
	 * @isa.module.command.macro-map order="1" inner-command="preProcessDiscount"
	 * @isa.module.command.macro-map order="2" inner-command="makePayment"
	 * @isa.module.command.macro-map order="3" inner-command="makeReservation"
	 * @isa.module.command.macro-map order="4" inner-command="createReservation"
	 * @isa.module.command.macro-map order="5" inner-command="adjustBlockSeats"
	 * @isa.module.command.macro-map order="6" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="7" inner-command="adjustPromotionCredit"
	 * @isa.module.command.macro-map order="8" inner-command="cMForCreateConfirmReservation"
	 * @isa.module.command.macro-map order="9" inner-command="recordModification"
	 * @isa.module.command.macro-map order="10" inner-command="captureOnholdPassengers"
	 * @isa.module.command.macro-map order="11" inner-command="splitReservation"
	 * @isa.module.command.macro-map order="12" inner-command="recordModification"
	 * @isa.module.command.macro-map order="13" inner-command="constructResponse"
	 */
	private Command createSplitReservationMacro;

	/**
	 * @isa.module.command.macro-command name="passengerRefundMacro"
	 * @isa.module.command.macro-map order="1" inner-command="reversePayment"
	 * @isa.module.command.macro-map order="2" inner-command="revenueRefund"
	 * @isa.module.command.macro-map order="3" inner-command="removeAgentCommission"
	 * @isa.module.command.macro-map order="4" inner-command="cMForPaxRefund"
	 * @isa.module.command.macro-map order="5" inner-command="recordModification"
	 */
	private Command passengerRefundMacro;

	/**
	 * @isa.module.command.macro-command name="transferOwnerShipMacro"
	 * @isa.module.command.macro-map order="1" inner-command="transferOwnerShip"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command transferOwnerShipMacro;

	/**
	 * @isa.module.command.macro-command name="reProtectReservationsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="transferSeats"
	 * @isa.module.command.macro-map order="2" inner-command="resModifyDetector"
	 */
	private Command reProtectReservationsMacro;

	/**
	 * @isa.module.command.macro-command name="rollForwardReservationsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="rollForwardTranfers"
	 * @isa.module.command.macro-map order="2" inner-command="transferSeats"
	 */
	private Command rollForwardReservationsMacro;

	/**
	 * @isa.module.command.macro-command name="reconcileReservationsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="reconcileReservation"
	 * @isa.module.command.macro-map order="2" inner-command="taxInvoiceGeneration"
	 */
	private Command reconcileReservationsMacro;

	/**
	 * @isa.module.command.macro-command name="alertReservationsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="alertReservations"
	 */
	private Command alertReservationsMacro;

	/**
	 * @isa.module.command.macro-command name="reconcileExtPayTransactionMacro"
	 * @isa.module.command.macro-map order="1" inner-command="reconExtPayTransaction"
	 */
	private Command reconcileExtPayTransactionMacro;

	/**
	 * @isa.module.command.macro-command name="reinstateCreditMacro"
	 * @isa.module.command.macro-map order="1" inner-command="recordCreditAdjust"
	 * @isa.module.command.macro-map order="2" inner-command="recordReinstatedCredit"
	 * @isa.module.command.macro-map order="3" inner-command="recordModification"
	 */
	private Command reinstateCreditMacro;

	/**
	 * @isa.module.command.macro-command name="extendCreditsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="extendCredits"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command extendCreditsMacro;

	/**
	 * @isa.module.command.macro-command name="changeExternalSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="changeExternalSegment"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command changeExternalSegmentMacro;
	
	/**
	 * @isa.module.command.macro-command name="changeOtherAirlineSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="changeOtherAirlineSegment"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command changeOtherAirlineSegmentMacro;

	/**
	 * @isa.module.command.macro-command name="addInsuranceMacro"
	 * @isa.module.command.macro-map order="1" inner-command="createInsurance"
	 */
	private Command addInsuranceMacro;

	/**
	 * @isa.module.command.macro-command name="confirmOpenReturnSegmentsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="makePayment"
	 * @isa.module.command.macro-map order="2" inner-command="confirmOpenReturnSegments"
	 * @isa.module.command.macro-map order="3" inner-command="createInsurance"
	 * @isa.module.command.macro-map order="4" inner-command="revenuePurchase"
	 * @isa.module.command.macro-map order="5" inner-command="resModifyDetector"
	 * @isa.module.command.macro-map order="6" inner-command="recordModification"
	 */
	private Command confirmOpenReturnSegmentsMacro;

	/**
	 * @isa.module.command.macro-command name="carryForwardPaxCreditMacro"
	 * @isa.module.command.macro-map order="1" inner-command="carryForwardCredit"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command carryForwardPaxCreditMacro;

	/**
	 * @isa.module.command.macro-command name="rollForwardOnholdBookingMacro"
	 * @isa.module.command.macro-map order="1" inner-command="rollForwardOnholdBooking"
	 * @isa.module.command.macro-map order="2" inner-command="recordModification"
	 */
	private Command rollForwardOnholdBooking;
	
	/**
	 * @isa.module.command.macro-command name="exchangeSegmentMacro"
	 * @isa.module.command.macro-map order="1" inner-command="prepareExchangeSegment"
	 * @isa.module.command.macro-map order="2" inner-command="exchangeSegment"
	 * @isa.module.command.macro-map order="3" inner-command="revenueModification"
	 * @isa.module.command.macro-map order="4" inner-command="recordModification"
	 */
	private Command exchangeSegmentMacro;
	
	/**
	 * @isa.module.command.macro-command name="pnlAdlDataGeneratorMacro"
	 * @isa.module.command.macro-map order="1" inner-command="pnlAdlMasterDataExtractorCommand"
	 * @isa.module.command.macro-map order="2" inner-command="pnlAdlAdditionalInfoExtractorCommand"
	 * @isa.module.command.macro-map order="3" inner-command="pnlAdlWebServiceCommand"
	 * @isa.module.command.macro-map order="4" inner-command="pnlAdlMessageBuilderCommand"
	 */
	private Command pnlAdlDataGeneratorMacro;
	
	/**
	 * @isa.module.command.macro-command name="palCalDataGeneratorMacro"
	 * @isa.module.command.macro-map order="1" inner-command="palCalMasterDataExtractorCommand"
	 * @isa.module.command.macro-map order="2" inner-command="palCalAdditionalInfoExtractorCommand"
	 * @isa.module.command.macro-map order="3" inner-command="palCalMessageBuilderCommand"
	 */
	private Command palCalDataGeneratorMacro;
	
	/**
	 * @isa.module.command.macro-command name="resChangeDetectorMacro"
	 * @isa.module.command.macro-map order="1" inner-command="ruleRegistry"
	 * @isa.module.command.macro-map order="2" inner-command="ruleExecuter"
	 * @isa.module.command.macro-map order="3" inner-command="observerActionExecuter"
	 */
	private Command resChangeDetectorMacro;
	
}
