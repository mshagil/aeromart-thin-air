/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 * 
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Version $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.automaticcheckin;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isa.thinair.airreservation.api.dto.autocheckin.Airport;
import com.isa.thinair.airreservation.api.dto.autocheckin.BookingInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.CheckinPassengersInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.CheckinPassengersInfoDTO;
import com.isa.thinair.airreservation.api.dto.autocheckin.DepartureInformation;
import com.isa.thinair.airreservation.api.dto.autocheckin.FlightBasicInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.FlightInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.FlightInfoDetail;
import com.isa.thinair.airreservation.api.dto.autocheckin.FlightLegInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.IdentificationSearchParameters;
import com.isa.thinair.airreservation.api.dto.autocheckin.PassengerBookingInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.PassengerInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.PassengerName;
import com.isa.thinair.airreservation.api.dto.autocheckin.PnrPaxInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.dto.autocheckin.ThroughCheckInDestination;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.ets.AutomaticCheckInReq;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

/**
 * This class includes all the business logic for AutomaticCheckin
 * 
 * @author Panchatcharam.s
 */
public class AutomaticCheckinBL {
	private Log log = LogFactory.getLog(AutomaticCheckinBL.class);

	private final String STATUS = "1";
	private final String TENENTCODE = "SITA";
	private GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

	public void sendAutomaticCheckinDetails(Integer pnrPaxId, Integer flightId, Integer noOfAttempts, String dcsCheckinStatus,
			String dcsResponseText) throws ModuleException {
		try {
			Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs = ReservationModuleUtils.getReservationAuxilliaryBD()
					.getPassengerDetailsforCheckin(flightId, pnrPaxId);
			if (log.isDebugEnabled()) {
				log.debug("No of passengers send to Bulk Checkin : " + checkinPassengersInfoDTOs.size());
			}
			if (checkinPassengersInfoDTOs.size() > 0) {
				Map<String, Collection<PnrPaxInfo>> checkinPassengerMap = getCheckinPassengerMap(checkinPassengersInfoDTOs);
				CheckinPassengersInfo checkinPassengersInfo = getCheckinPassengerRequest(checkinPassengersInfoDTOs,
						checkinPassengerMap);
				String response = getResponse(checkinPassengersInfo, AppSysParamsUtil.getBulkCheckInURLText());
				if (log.isDebugEnabled()) {
					log.debug("Bulk checkin Response : " + response);
				}
				if (checkinPassengersInfoDTOs != null && checkinPassengersInfoDTOs.size() > 0) {
					ReservationModuleUtils.getReservationAuxilliaryBD().updateAutomaticCheckin(
							getPnrPaxIds(checkinPassengersInfoDTOs), flightId, noOfAttempts, dcsCheckinStatus, dcsResponseText);
				}
			}

		} catch (Exception me) {
			throw new ModuleException(me, "error.automaticcheckin.details");
		}
	}

	private Collection<Integer> getPnrPaxIds(Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs) {
		return checkinPassengersInfoDTOs.stream().map(s -> s.getPnrPaxId()).collect(Collectors.toList());
	}

	private String getResponse(CheckinPassengersInfo checkinPassengersInfo, String URL) throws ModuleException {
		HttpClient client = new HttpClient();
		ObjectMapper mapperObj = new ObjectMapper();
		String jsonStr = "";
		try {
			jsonStr = mapperObj.writeValueAsString(checkinPassengersInfo);
		} catch (JsonProcessingException me) {
			log.equals(me);
		}
		log.info("Request URL : " + URL);
		log.info("Request Body : " + jsonStr);
		PostMethod method = new PostMethod(URL);
		try {
			method.setRequestEntity(new StringRequestEntity(jsonStr, "application/json", "UTF-8"));
		} catch (UnsupportedEncodingException me) {
			log.equals(me);
		}
		String responseString = null;
		try {
			if (log.isDebugEnabled()) {
				log.debug("Executing the sendAutomaticCheckinDetails Request for URL : " + URL);
			}
			int returnCode = client.executeMethod(method);

			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				if (log.isDebugEnabled()) {
					log.debug("The Post method is not implemented by this URI");
				}
				return null;
			} else {
				responseString = method.getResponseBodyAsString();
				if (log.isDebugEnabled()) {
					log.debug("HTTP Response : " + responseString);
				}
			}
		} catch (Exception e) {
			throw new ModuleException("exception.while.parsing.http.response");
		} finally {
			method.releaseConnection();
		}
		return responseString;
	}

	private CheckinPassengersInfo getCheckinPassengerRequest(Collection<CheckinPassengersInfoDTO> checkinPassengersInfoDTOs,
			Map<String, Collection<PnrPaxInfo>> checkinPassengerMap) throws ModuleException {
		CheckinPassengersInfo checkinPassengersInfo = null;
		try {
			CheckinPassengersInfoDTO checkinPassengersInfoDTO = checkinPassengersInfoDTOs.iterator().next();
			checkinPassengersInfo = new CheckinPassengersInfo();
			checkinPassengersInfo.setStatus(STATUS);
			checkinPassengersInfo.setTenentCode(TENENTCODE);
			IdentificationSearchParameters identificationSearchParameters = new IdentificationSearchParameters();
			mapFlightInfo(checkinPassengersInfoDTO, identificationSearchParameters);
			mapIdentificationSearchParam(checkinPassengerMap, checkinPassengersInfo, identificationSearchParameters);
			Collection<FlightInfoDetail> flightInfoDetails = mapFlightInfo(checkinPassengersInfoDTO);
			checkinPassengersInfo.setFlightInfoDetails(flightInfoDetails);
			ThroughCheckInDestination throughCheckInDestination = new ThroughCheckInDestination();
			throughCheckInDestination.setLocationCode("");
			checkinPassengersInfo.setThroughCheckInDestination(throughCheckInDestination);
			mapBookingInfo(checkinPassengersInfoDTO.getPreferredLanguage(), checkinPassengerMap, checkinPassengersInfo);
		} catch (Exception e) {
			throw new ModuleException("error.automaticcheckin.details");
		}

		return checkinPassengersInfo;
	}

	private void mapBookingInfo(String preferdLang, Map<String, Collection<PnrPaxInfo>> checkinPassengerMap,
			CheckinPassengersInfo checkinPassengersInfo) {
		Collection<BookingInfo> bookingInfos = new ArrayList<BookingInfo>();
		checkinPassengerMap.entrySet().forEach(checkinPassenger -> {
			BookingInfo bookingInfo = new BookingInfo();
			bookingInfo.setId(checkinPassenger.getKey());
			bookingInfo.setLanguage(preferdLang);
			bookingInfo.setPnrPaxInfo(checkinPassenger.getValue());
			bookingInfos.add(bookingInfo);
		});
		checkinPassengersInfo.setBookingInfo(bookingInfos);
	}

	private void mapIdentificationSearchParam(Map<String, Collection<PnrPaxInfo>> checkinPassengerMap,
			CheckinPassengersInfo checkinPassengersInfo, IdentificationSearchParameters identificationSearchParameters) {
		PassengerInfo passengerInfo = new PassengerInfo();
		Collection<PassengerBookingInfo> passengerBookingInfos = new ArrayList<PassengerBookingInfo>();
		checkinPassengerMap.keySet().forEach(id -> {
			PassengerBookingInfo bookingInfo = new PassengerBookingInfo();
			bookingInfo.setId(id);
			passengerBookingInfos.add(bookingInfo);
		});
		passengerInfo.setBookingInfo(passengerBookingInfos);
		identificationSearchParameters.setPassengerInfo(passengerInfo);
		checkinPassengersInfo.setIdentificationSearchParameters(identificationSearchParameters);
	}

	private void mapFlightInfo(CheckinPassengersInfoDTO checkinPassengersInfoDTO,
			IdentificationSearchParameters identificationSearchParameters) {
		FlightInfo flightInfo = new FlightInfo();
		FlightBasicInfo carrierInfo = new FlightBasicInfo();
		carrierInfo.setCode(checkinPassengersInfoDTO.getCarrierInfoCode());
		carrierInfo.setFlightNumber(checkinPassengersInfoDTO.getCarrierInfoFlightnumber());
		flightInfo.setCarrierInfo(carrierInfo);
		DepartureInformation departureInformation = new DepartureInformation();
		departureInformation.setDateOfDeparture(checkinPassengersInfoDTO.getFlightInfoDateDeparture());
		departureInformation.setLocationCode(checkinPassengersInfoDTO.getDateOfDeparture());
		flightInfo.setDepartureInformation(departureInformation);
		identificationSearchParameters.setFlightInfo(flightInfo);
	}

	private Collection<FlightInfoDetail> mapFlightInfo(CheckinPassengersInfoDTO checkinPassengersInfoDTO) {
		Collection<FlightInfoDetail> flightInfoDetails = null;
		flightInfoDetails = new ArrayList<FlightInfoDetail>();
		FlightInfoDetail flightInfoDetail = new FlightInfoDetail();
		FlightLegInfo flightLegInfo = new FlightLegInfo();
		flightLegInfo.setRPH(checkinPassengersInfoDTO.getFlightLegRPH());
		Airport departureAirport = new Airport();
		departureAirport.setLocationCode(checkinPassengersInfoDTO.getFlightLegDepAirportLocationCode());
		flightLegInfo.setDepartureAirport(departureAirport);
		Airport arrivalAirport = new Airport();
		arrivalAirport.setLocationCode(checkinPassengersInfoDTO.getFlightLegAriAirportLocationCode());
		flightLegInfo.setArrivalAirport(arrivalAirport);
		FlightBasicInfo basicInfo = new FlightBasicInfo();
		basicInfo.setCode(checkinPassengersInfoDTO.getCarrierInfoCode());
		basicInfo.setFlightNumber(checkinPassengersInfoDTO.getCarrierInfoFlightnumber());
		flightLegInfo.setOperatingAirline(basicInfo);
		flightLegInfo.setDepartureDate(checkinPassengersInfoDTO.getFlightInfoDateDeparture());
		flightInfoDetail.setFlightLegInfo(flightLegInfo);
		return flightInfoDetails;
	}

	private Map<String, Collection<PnrPaxInfo>> getCheckinPassengerMap(
			Collection<CheckinPassengersInfoDTO> bulkCheckinPassengersInfoDTOs) {
		Map<String, Collection<PnrPaxInfo>> bulkCheckinPassengersInfo = new TreeMap<String, Collection<PnrPaxInfo>>();

		bulkCheckinPassengersInfoDTOs.forEach(bulkCheckinPassengersInfoDTO -> {
			Collection<PnrPaxInfo> pnrPaxInfos = null;
			if (bulkCheckinPassengersInfo.get(bulkCheckinPassengersInfoDTO.getId()) == null) {
				pnrPaxInfos = new ArrayList<PnrPaxInfo>();
			} else {
				pnrPaxInfos = bulkCheckinPassengersInfo.get(bulkCheckinPassengersInfoDTO.getId());
			}
			PnrPaxInfo pnrPaxInfo = new PnrPaxInfo();
			pnrPaxInfo.setEmail(bulkCheckinPassengersInfoDTO.getEmail());
			pnrPaxInfo.setTravelWithInfant(bulkCheckinPassengersInfoDTO.getTravelWithInfant());
			pnrPaxInfo.setPaxType(bulkCheckinPassengersInfoDTO.getPaxType());
			pnrPaxInfo.setSelectedSeat(bulkCheckinPassengersInfoDTO.getPaxSelectedSeat());
			ReservationPaxAdditionalInfo reservationPaxAdditionalInfo = new ReservationPaxAdditionalInfo();
			mapPaxAdditionalInfo(bulkCheckinPassengersInfoDTO, reservationPaxAdditionalInfo);
			pnrPaxInfo.setReservationPaxAdditionalInfo(reservationPaxAdditionalInfo);
			PassengerName passengerName = new PassengerName();
			mapPassengerName(bulkCheckinPassengersInfoDTO, pnrPaxInfos, pnrPaxInfo, passengerName);
			bulkCheckinPassengersInfo.put(bulkCheckinPassengersInfoDTO.getId(), pnrPaxInfos);
		});

		return bulkCheckinPassengersInfo;
	}

	private void mapPassengerName(CheckinPassengersInfoDTO bulkCheckinPassengersInfoDTO, Collection<PnrPaxInfo> pnrPaxInfos,
			PnrPaxInfo pnrPaxInfo, PassengerName passengerName) {
		Collection<String> givenName = new ArrayList<String>();
		givenName.add(bulkCheckinPassengersInfoDTO.getPassengerGivenname());
		passengerName.setGivenName(givenName);
		passengerName.setSurname(bulkCheckinPassengersInfoDTO.getPassengerSurname());
		passengerName.setNameTitle(bulkCheckinPassengersInfoDTO.getPassengerNametitle());
		pnrPaxInfo.setPassengerName(passengerName);
		pnrPaxInfos.add(pnrPaxInfo);
	}

	private void mapPaxAdditionalInfo(CheckinPassengersInfoDTO bulkCheckinPassengersInfoDTO,
			ReservationPaxAdditionalInfo reservationPaxAdditionalInfo) {
		reservationPaxAdditionalInfo.setPassportNumber(bulkCheckinPassengersInfoDTO.getPassportNumber());
		reservationPaxAdditionalInfo.setPassportIssuedCountry(bulkCheckinPassengersInfoDTO.getPassportIssuedCountry());
		reservationPaxAdditionalInfo.setPassportExpiryDate(bulkCheckinPassengersInfoDTO.getPassportExpiryDate());
		reservationPaxAdditionalInfo.setPassportFullName(bulkCheckinPassengersInfoDTO.getPassportFullName());
		reservationPaxAdditionalInfo.setVisaType(Integer.valueOf(bulkCheckinPassengersInfoDTO.getVisaType()));
		reservationPaxAdditionalInfo.setVisaNumber(bulkCheckinPassengersInfoDTO.getVisaNumber());
		reservationPaxAdditionalInfo.setVisaIssuedCountry(bulkCheckinPassengersInfoDTO.getVisaIssuedCountry());
		reservationPaxAdditionalInfo.setVisaIssuedDate(bulkCheckinPassengersInfoDTO.getVisaIssuedDate());
		reservationPaxAdditionalInfo.setDateOfBirth(bulkCheckinPassengersInfoDTO.getDateOfBirth());
		reservationPaxAdditionalInfo.setCountryOfResidence(bulkCheckinPassengersInfoDTO.getCountryOfResidence());
		reservationPaxAdditionalInfo.setPlaceOfBirth(bulkCheckinPassengersInfoDTO.getPlaceOfBirth());
		reservationPaxAdditionalInfo.setDestinationCity(bulkCheckinPassengersInfoDTO.getDestinationCity());
		reservationPaxAdditionalInfo.setNationality(bulkCheckinPassengersInfoDTO.getNationality());
		reservationPaxAdditionalInfo.setTravelDocType(bulkCheckinPassengersInfoDTO.getTravelDocType());
		reservationPaxAdditionalInfo.setStaffId(bulkCheckinPassengersInfoDTO.getStaffId());
		reservationPaxAdditionalInfo.setDateOfJoin(bulkCheckinPassengersInfoDTO.getDateOfJoin());
		reservationPaxAdditionalInfo.setVisaApplicableCountry(bulkCheckinPassengersInfoDTO.getVisaApplicableCountry());
		reservationPaxAdditionalInfo.setGender(bulkCheckinPassengersInfoDTO.getGender());
	}

	public void emailAutoCheckin(AutomaticCheckInReq request, String emailAddress, String template) throws ModuleException {
		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		try {
			HashMap<String, Object> dataMap = getTemplateParams(request, emailAddress);
			// User Message
			UserMessaging userMessaging = new UserMessaging();
			userMessaging.setFirstName(String.valueOf(dataMap.get("firstName")));
			userMessaging.setLastName(String.valueOf(dataMap.get("lastName")));
			userMessaging.setToAddres(String.valueOf(dataMap.get("emailAddress")));

			List<UserMessaging> messages = new ArrayList<UserMessaging>();
			messages.add(userMessaging);
			dataMap.put("subject", template);
			// Topic
			Topic topic = new Topic();
			topic.setTopicParams(dataMap);
			topic.setTopicName(template);
			topic.setLocale((Locale) dataMap.get("locale"));
			topic.setAttachMessageBody(false);

			// User Profile
			MessageProfile messageProfile = new MessageProfile();
			messageProfile.setUserMessagings(messages);
			messageProfile.setTopic(topic);

			// Send the message
			messagingServiceBD.sendMessage(messageProfile);
		} catch (Exception me) {
			throw new ModuleException(me, "error.sending.email.automaticcheckin");
		}
	}

	private HashMap<String, Object> getTemplateParams(AutomaticCheckInReq request, String emailAddress) throws ModuleException,
			ParseException {
		HashMap<String, Object> templateParamMap = null;
		if (request != null) {

			templateParamMap = new HashMap<String, Object>();
			templateParamMap.put("onlineCheckinUrl", getOnlineCheckinURL(request));
			templateParamMap.put("pnr", request.getPnr());
			templateParamMap.put("passengerName", request.getFirstName() + " " + request.getLastName());
			templateParamMap.put("airportName", globalConfig.getAirportInfo(request.getDepartureAirportCode(), true)[0]);
			templateParamMap.put("firstName", request.getFirstName());
			templateParamMap.put("lastName", request.getLastName());
			templateParamMap.put("emailAddress", emailAddress);

		}

		return templateParamMap;
	}

	private String getOnlineCheckinURL(AutomaticCheckInReq request) throws ParseException {
		StringBuilder onlineCheckinUrl = new StringBuilder();
		onlineCheckinUrl.append(AppSysParamsUtil.getOnlineCheckInURLText());
		onlineCheckinUrl.append("?pnr=");
		onlineCheckinUrl.append(request.getPnr());
		onlineCheckinUrl.append("&amp;depDate=");
		SimpleDateFormat urlFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		onlineCheckinUrl.append(urlFormat.format(inputFormat.parse(request.getDepartureDate())));
		onlineCheckinUrl.append("&amp;depAirport=");
		onlineCheckinUrl.append(request.getDepartureAirportCode());
		return onlineCheckinUrl.toString();
	}

}