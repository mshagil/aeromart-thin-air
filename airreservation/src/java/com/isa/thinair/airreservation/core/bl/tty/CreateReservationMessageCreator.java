package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.OSIAddressDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIEmailDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIPhoneNoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class CreateReservationMessageCreator extends TypeBReservationMessageCreator {
	
	public CreateReservationMessageCreator() {
		segmentsComposingStrategy = new SegmentsComposerForCreation();
		passengerNamesComposingStrategy = new PassengerNamesCommonComposer();
	}

	@Override
	public List<OSIDTO> addOSIDetails(Reservation reservation, TypeBRequestDTO typeBRequestDTO) {
		ReservationContactInfo reservationContactInfo = reservation.getContactInfo();
		List<OSIDTO> osiDTOs = new ArrayList<OSIDTO>();

		if (reservationContactInfo.getPhoneNo() != null) {
			OSIPhoneNoDTO phoneNoDTO = new OSIPhoneNoDTO();
			phoneNoDTO.setPhoneNo(reservationContactInfo.getPhoneNo());
			phoneNoDTO.setOsiValue(reservationContactInfo.getPhoneNo());
			phoneNoDTO.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_PHONE_TRAVEL_AGENT.getCode());
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				phoneNoDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				phoneNoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			osiDTOs.add(phoneNoDTO);
		}

		if (reservationContactInfo.getCity() != null || reservationContactInfo.getStreetAddress1() != null || reservationContactInfo.getStreetAddress2() != null) {
			OSIAddressDTO addressDTO = new OSIAddressDTO();
			addressDTO.setOsiValue(reservationContactInfo.getCity());
			addressDTO.setCity(reservationContactInfo.getCity());
			addressDTO.setAddressLineOne(reservationContactInfo.getStreetAddress1());
			addressDTO.setAddressLineTwo(reservationContactInfo.getStreetAddress2());
			addressDTO.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_ADDRESS.getCode());
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				addressDTO.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				addressDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			osiDTOs.add(addressDTO);
		}
		
		if (reservationContactInfo.getMobileNo() != null) {
			OSIPhoneNoDTO phoneDto = new OSIPhoneNoDTO();
			phoneDto.setPhoneNo(reservationContactInfo.getMobileNo());
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				phoneDto.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				phoneDto.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			phoneDto.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_PHONE_MOBILE.getCode());
			osiDTOs.add(phoneDto);
		}
		if (reservationContactInfo.getEmail() != null) {
			OSIEmailDTO emailDto = new OSIEmailDTO();
			String rawEmail = reservationContactInfo.getEmail();
			String email1 = rawEmail.replace("@", "//");
			String email = email1.replace("_", "..");
			emailDto.setEmail(email);
			if (typeBRequestDTO.getCsOCCarrierCode() != null) {
				emailDto.setCarrierCode(typeBRequestDTO.getCsOCCarrierCode());
			} else {
				emailDto.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			}
			emailDto.setCodeOSI(GDSExternalCodes.OSICodes.CONTACT_EMAIL.getCode());
			osiDTOs.add(emailDto);
		}
		return osiDTOs;
	}
	
	@Override
	public List<SSRDTO> addSsrDetails(List<SSRDTO> ssrDTOs, TypeBRequestDTO typeBRequestDTO, Reservation reservation)
			throws ModuleException {

		ssrDTOs.addAll(TTYMessageCreatorUtil.composeSsrDocsInfo(getAdditionalInfoChangedPaxList(reservation), reservation,
				typeBRequestDTO));
		ssrDTOs.addAll(TTYMessageCreatorUtil.composeSsrDocoInfo(getAdditionalInfoChangedPaxList(reservation), reservation,
				typeBRequestDTO));
		ssrDTOs.addAll(TTYMessageCreatorUtil.composeSsrDetails(reservation, typeBRequestDTO));
		return ssrDTOs;
	}
	
	/**
	 * @param srcReservation
	 * @param clnReservation
	 * @return
	 */
	private static Map<Integer, ReservationPaxAdditionalInfo> getAdditionalInfoChangedPaxList(Reservation srcReservation)
			throws ModuleException {

		Map<Integer, ReservationPaxAdditionalInfo> paxIdsMap = new HashMap<Integer, ReservationPaxAdditionalInfo>();
		ReservationPax reservationPaxSrv;

		for (Iterator<ReservationPax> itSrcReservationPax = srcReservation.getPassengers().iterator(); itSrcReservationPax
				.hasNext();) {
			reservationPaxSrv = itSrcReservationPax.next();
			ReservationPaxAdditionalInfo srcPaxAdditionalInfo = reservationPaxSrv.getPaxAdditionalInfo();

			if (srcPaxAdditionalInfo.getPassportNo() != null && srcPaxAdditionalInfo.getPassportExpiry() != null
					&& srcPaxAdditionalInfo.getPassportIssuedCntry() != null) {
				paxIdsMap.put(reservationPaxSrv.getPnrPaxId(), srcPaxAdditionalInfo);
			}
		}
		return paxIdsMap;
	}
}
