package com.isa.thinair.airreservation.core.bl.external.focus;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.airreservation.api.dto.external.CreditCardPaymentFocusDTO;
import com.isa.thinair.airreservation.api.model.CreditCardSalesFocus;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.external.AccontingSystemTemplate;
import com.isa.thinair.airreservation.core.bl.external.AccountingSystem;
import com.isa.thinair.airreservation.core.bl.external.LatestFirstSortComparatorCCSales;
import com.isa.thinair.airreservation.core.persistence.dao.FocusSystemDAO;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AccountingSystemFocusImpl extends AccontingSystemTemplate implements AccountingSystem {
	
	private static final Log log = LogFactory.getLog(AccountingSystemFocusImpl.class);

	/*
	 * Transfers credit card sales to Focus accounting System
	 * 
	 * @Param date
	 * @throws ModuleException
	 */
	@Override
	public void transferCreditSales(Date date) throws ModuleException {
		log.info("########### Generating and Transferring CC Sales for Focus:" + date);
		FocusSystemDAO salesDAO = ReservationDAOUtils.DAOInstance.focusSystemDAO;
		Collection<CreditCardPaymentFocusDTO> colDTOs = null;
		Collection<CreditCardSalesFocus> creditCardSalesPOJOs = null;
		try {
			salesDAO.clearCreditCardSalesTable(date);
			colDTOs = salesDAO.getCreditCardPayments(date);
		} catch (CommonsDataAccessException dae) {
			log.error("######### Error in clearing or retriving cc sales Focus:" + date);
			throw new CommonsDataAccessException(dae, "airreservations.auxilliary.getcreditsales",
					AirreservationConstants.MODULE_NAME);
		}

		if (colDTOs != null) {
			try {
				log.debug("######## Going to insert cc sales to internal Focus:" + date);
				creditCardSalesPOJOs = salesDAO.insertCreditCardSalesToInternal(colDTOs);
				log.debug("######## After inserting cc sales to internal Focus:" + date);
			} catch (CommonsDataAccessException dae) {
				log.error("############ Failed when inserting CC sales to internal Focus" + date);
				throw new CommonsDataAccessException(dae, "airreservations.auxilliary.transferinternalcreditsys",
						AirreservationConstants.MODULE_NAME);
			}

			String transferToExternal = ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer();
			if (transferToExternal == null || !(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM
					.equalsIgnoreCase(transferToExternal))) {
				throw new ModuleException("airreservation.exdb.notconfigured");
			}

			try {
				log.debug("###### Before inserting CC sales to XDB Focus: " + date);
				salesDAO.insertCreditCardSalesToExternal(colDTOs, (AccontingSystemTemplate)this);
				log.debug("###### After inserting CC sales to XDB Focus: " + date);
			} catch (SQLException me) {
				log.error("Failed when Transferring to XDB :", me);
				if (me.getErrorCode() == 2627)
					throw new ModuleException(me, "reservationauxilliary.externaltable.integrityviolation",
							AirreservationConstants.MODULE_NAME);
				else
					throw new ModuleException(me, "reservationauxilliary.externaltable.databaseunavailable",
							AirreservationConstants.MODULE_NAME);

			} catch (ClassNotFoundException nfe) {
				log.error("Transfer Credit Sales/insertCreditCardSalestoExternal Failed Focus...");
				throw new ModuleException(nfe, "reservationauxilliary.externaltable.integrityviolation",
						AirreservationConstants.MODULE_NAME);
			} catch (Exception e) {
				log.error("Transfering to Interface Tables Failed Focus", e);
				throw new ModuleException(e, "transfer.creditcardsales.failed", AirreservationConstants.MODULE_NAME);

			}

			try {
				log.debug("###### Before Updating Status in CC sales Focus: " + date);
				salesDAO.updateInternalCreditTable(creditCardSalesPOJOs);
				log.debug("###### After Updating Status in CC sales Focus: " + date);
			} catch (CommonsDataAccessException dae) {
				log.error("Transfer Credit Sales/update External Failed Focus..");
				throw new CommonsDataAccessException(dae, "airreservations.auxilliary.transferinternalcreditsys",
						AirreservationConstants.MODULE_NAME);

			}
		}
	}
	
	/*
	 * CreditCard sales details
	 * 
	 * @Param cards
	 * @Param fromDate
	 * @Param toDate
	 * @throws ModuleException
	 * 
	 * @Return CreditCardSalesStatusDTO
	 */
	@Override
	@SuppressWarnings("unchecked")
	public CreditCardSalesStatusDTO getCreditCardSalesDetails(Integer cards[], Date fromDate, Date toDate) throws ModuleException {

		FocusSystemDAO salesDAO = ReservationDAOUtils.DAOInstance.focusSystemDAO;
		CreditCardSalesStatusDTO creditCardSalesstatusDTO = new CreditCardSalesStatusDTO();
		creditCardSalesstatusDTO.setCreditSales(getCreditCardSalesHistory(fromDate, toDate));

		creditCardSalesstatusDTO.setTotAmtGenerated(salesDAO.getGeneratedCreditCardSalesTotal(fromDate, toDate));
		if (hasEXDB()) {
			creditCardSalesstatusDTO.setHasEXDB(true);
			creditCardSalesstatusDTO.setTotAmtTransfered(salesDAO.getTransferedCreditCardSalesTotal(fromDate, toDate, (AccontingSystemTemplate)this));
		} else {
			creditCardSalesstatusDTO.setHasEXDB(false);
		}

		return creditCardSalesstatusDTO;
	}
	
	/*
	 * CreditCard sales history
	 * 
	 * @Param fromDate
	 * @Param toDate
	 * 
	 * @Return ArrayList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ArrayList getCreditCardSalesHistory(Date fromDate, Date toDate) {
		FocusSystemDAO salesDAO = ReservationDAOUtils.DAOInstance.focusSystemDAO;
		// will return 0-a list of date objects 1-a list of dto's
		Object[] o = salesDAO.getCreditCardSalesHistory(fromDate, toDate);
		// hold the startdate
		GregorianCalendar calendarDate = new GregorianCalendar();
		calendarDate.setTime(fromDate);
		// hold the enddate
		GregorianCalendar calendarDateEnd = new GregorianCalendar();
		calendarDateEnd.setTime(toDate);

		// get the objects from the object array
		ArrayList dtoList = (ArrayList) o[0];
		ArrayList dateList = (ArrayList) o[1];

		// for all the dates..i.e from start to end, see if retrieved from the database
		// if not create your own dto, add it to the dto list and send it to f.e.
		while (calendarDate.before(calendarDateEnd) || calendarDate.equals(calendarDateEnd)) {

			if (!(dateList.contains(calendarDate.getTime()))) {
				CCSalesHistoryDTO historyDTO = new CCSalesHistoryDTO();
				historyDTO.setDateOfsale(calendarDate.getTime());
				historyDTO.setCardType("Not Generated");
				historyDTO.setTotalDailySales(AccelAeroCalculator.getDefaultBigDecimalZero());
				historyDTO.setTransferStatus("N");
				dtoList.add(historyDTO);
			}

			calendarDate.add(GregorianCalendar.DATE, +1);
		}
		// sort by date
		Collections.sort(dtoList, new LatestFirstSortComparatorCCSales());
		return dtoList;

	}	
	
	/*
	 * checks if transfer to external system is configured or not ?
	 */
	private boolean hasEXDB() {

		if (ReservationModuleUtils.getAirReservationConfig().getTransferToSqlServer().equalsIgnoreCase(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void transferTopUps(Date date) throws ModuleException {
		// TODO Auto-generated method stub

	}


}
