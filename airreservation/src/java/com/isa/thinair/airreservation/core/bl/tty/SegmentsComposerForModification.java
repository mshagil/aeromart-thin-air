package com.isa.thinair.airreservation.core.bl.tty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class SegmentsComposerForModification implements SegmentsComposingStrategy {
	
	@Override
	public List<BookingSegmentDTO> composeSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BookingSegmentDTO> composeCSSegments(Reservation reservation, TypeBRequestDTO typeBRequestDTO) throws ModuleException {
		
		int paxCount = TTYMessageCreatorUtil.getPaxCount(reservation, typeBRequestDTO);
		Map<Integer, ReservationSegmentDTO> exchangedSegmentDTOs = new HashMap<Integer, ReservationSegmentDTO>();
		if (typeBRequestDTO.getExchangedPnrSegIds() != null && !typeBRequestDTO.getExchangedPnrSegIds().isEmpty()) {
			for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
				if (typeBRequestDTO.getExchangedPnrSegIds().contains(reservationSegmentDTO.getPnrSegId())) {
					exchangedSegmentDTOs.put(reservationSegmentDTO.getFlightSegId(), reservationSegmentDTO);
				}
			}
		}

		List<BookingSegmentDTO> bookingSegmentDTOs = new ArrayList<BookingSegmentDTO>();
		BookingSegmentDTO segment;
		boolean actionRequired = false;

		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		GDSStatusTO gdsStatusTO = globalConfig.getActiveGdsMap().values().stream().filter(new Predicate<GDSStatusTO>() {
			public boolean test(GDSStatusTO gdsStatusTO) {
				return gdsStatusTO.getCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode());
			}
		}).findFirst().get();

		for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
			if (!ReservationInternalConstants.ReservationSegmentSubStatus.EXCHANGED.equalsIgnoreCase(reservationSegmentDTO
					.getSubStatus())) {
				if (reservationSegmentDTO.getCsOcCarrierCode() != null
						&& reservationSegmentDTO.getCsOcCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode())) {
					
					segment = TypeBSegmentAdopter.adoptResSegmentToTypeBCSSegment(reservationSegmentDTO, typeBRequestDTO.getCsOCCarrierCode(), paxCount);
					segment.setPrimeFlight(gdsStatusTO.getPrimeFlightEnabled());

					if ((typeBRequestDTO.getCnxPnrSegIds() != null && typeBRequestDTO.getCnxPnrSegIds().contains(
							reservationSegmentDTO.getPnrSegId()))
							|| (typeBRequestDTO.getCnxInversePnrSegIds() != null && typeBRequestDTO.getCnxInversePnrSegIds()
									.contains(reservationSegmentDTO.getPnrSegId()))) {
						if (gdsStatusTO.getPrimeFlightEnabled()) {
							segment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CANCEL.getCode());
						} else {
							segment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode());
						}
						actionRequired = true;
					} else if (typeBRequestDTO.getNewlyAddedFlightSegIds() != null
							&& typeBRequestDTO.getNewlyAddedFlightSegIds().contains(reservationSegmentDTO.getFlightSegId())) {
						if (!reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CANCEL)) {
							if (gdsStatusTO.getPrimeFlightEnabled()) {
								segment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.SOLD.getCode());
							} else {
								segment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode());
							}
							actionRequired = true;
						}
					} else {
						if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
							if (!exchangedSegmentDTOs.isEmpty()
									&& exchangedSegmentDTOs.keySet().contains(reservationSegmentDTO.getFlightSegId())) {
								String bookingClass = null;
								if (reservationSegmentDTO.getFareTO() != null) {
									bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
								} else {
									bookingClass = reservationSegmentDTO.getCabinClassCode();
								}
								String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(typeBRequestDTO.getCsOCCarrierCode(),
										bookingClass);								
								ReservationSegmentDTO oldResSegment = exchangedSegmentDTOs.get(reservationSegmentDTO
										.getFlightSegId());
								String oldBc = oldResSegment.getFareTO().getBookingClassCode();
								String oldGdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(
										typeBRequestDTO.getCsOCCarrierCode(), oldBc);
								if (oldGdsBookingClass != null && gdsBookingClass != null
										&& !oldGdsBookingClass.equals(gdsBookingClass)) {
									if (gdsStatusTO.getPrimeFlightEnabled()) {
										segment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.SOLD.getCode());
									} else {
										segment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode());
									}
									addExchangedSegment(typeBRequestDTO, bookingSegmentDTOs, segment, oldResSegment);
									actionRequired = true;
								} else {
									segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
								}
							} else {
								segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
							}
						}
					}
					if (segment.getAdviceOrStatusCode() != null) {
						bookingSegmentDTOs.add(segment);
					}
				} else if (TTYMessageUtil.getOALSegmentsSendingEligibility(typeBRequestDTO.getCsOCCarrierCode())) {

					segment = TypeBSegmentAdopter.adoptResSegmentToTypeBSegment(reservationSegmentDTO, typeBRequestDTO.getCsOCCarrierCode(), paxCount);
					if ((typeBRequestDTO.getCnxPnrSegIds() != null && typeBRequestDTO.getCnxPnrSegIds().contains(
							reservationSegmentDTO.getPnrSegId()))
							|| (typeBRequestDTO.getCnxInversePnrSegIds() != null && typeBRequestDTO.getCnxInversePnrSegIds()
									.contains(reservationSegmentDTO.getPnrSegId()))) {
						segment.setAdviceOrStatusCode(GDSExternalCodes.AdviceCode.CANCELLED.getCode());
						actionRequired = true;
					} else if (typeBRequestDTO.getNewlyAddedFlightSegIds() != null
							&& typeBRequestDTO.getNewlyAddedFlightSegIds().contains(reservationSegmentDTO.getFlightSegId())) {
						if (!reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CANCEL)) {
							segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
							actionRequired = true;
						}
					} else {
						if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
							if (!exchangedSegmentDTOs.isEmpty()
									&& exchangedSegmentDTOs.keySet().contains(reservationSegmentDTO.getFlightSegId())) {
								String bookingClass = null;
								if (reservationSegmentDTO.getFareTO() != null) {
									bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
								} else {
									bookingClass = reservationSegmentDTO.getCabinClassCode();
								}
								String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(typeBRequestDTO.getCsOCCarrierCode(),
										bookingClass);
								ReservationSegmentDTO oldResSegment = exchangedSegmentDTOs.get(reservationSegmentDTO
										.getFlightSegId());
								String oldBc = oldResSegment.getFareTO().getBookingClassCode();
								String oldGdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(
										typeBRequestDTO.getCsOCCarrierCode(), oldBc);
								if (oldGdsBookingClass != null && gdsBookingClass != null
										&& !oldGdsBookingClass.equals(gdsBookingClass)) {
									segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
									addExchangedOALSegment(typeBRequestDTO, bookingSegmentDTOs, segment, oldResSegment);
									actionRequired = true;
								} else {
									segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
								}
							} else {
								segment.setAdviceOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
							}
						}
					}
					if (segment.getAdviceOrStatusCode() != null) {
						bookingSegmentDTOs.add(segment);
					}					
				}
			}
		}
		if (actionRequired) {
			return bookingSegmentDTOs;
		} else {
			return new ArrayList<BookingSegmentDTO>();
		}
	}

	private static void addExchangedSegment(TypeBRequestDTO typeBRequestDTO, List<BookingSegmentDTO> bookingSegmentDTOs,
			BookingSegmentDTO segment, ReservationSegmentDTO oldResSegment) throws ModuleException {
		BookingSegmentDTO exSegment = new BookingSegmentDTO();
		exSegment.setDepartureStation(segment.getDepartureStation());
		exSegment.setDepartureDate(segment.getDepartureDate());
		exSegment.setDepartureTime(segment.getDepartureTime());
		exSegment.setDestinationStation(segment.getDestinationStation());
		exSegment.setArrivalDate(segment.getArrivalDate());
		exSegment.setArrivalTime(segment.getArrivalTime());
		exSegment.setCarrierCode(segment.getCarrierCode());
		exSegment.setCsCarrierCode(segment.getCsCarrierCode());
		exSegment.setCsFlightNumber(segment.getCsFlightNumber());
		exSegment.setFlightNumber(segment.getFlightNumber());
		exSegment.setNoofPax(segment.getNoofPax());

		GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();
		GDSStatusTO gdsStatusTO = globalConfig.getActiveGdsMap().values().stream().filter(new Predicate<GDSStatusTO>() {
			public boolean test(GDSStatusTO gdsStatusTO) {
				return gdsStatusTO.getCarrierCode().equals(typeBRequestDTO.getCsOCCarrierCode());
			}
		}).findFirst().get();

		String bookingClass = null;
		if (oldResSegment.getFareTO() != null) {
			bookingClass = oldResSegment.getFareTO().getBookingClassCode();
		} else {
			bookingClass = oldResSegment.getCabinClassCode();
		}
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(typeBRequestDTO.getCsOCCarrierCode(), bookingClass);
		exSegment.setCsBookingCode(gdsBookingClass);
		exSegment
				.setBookingCode(TTYMessageUtil.getExternalCsBCForGdsMappedBC(typeBRequestDTO.getCsOCCarrierCode(), gdsBookingClass));

		exSegment.setPrimeFlight(gdsStatusTO.getPrimeFlightEnabled());
		if (gdsStatusTO.getPrimeFlightEnabled()) {
			exSegment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CANCEL.getCode());
		} else {
			exSegment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode());
		}

		bookingSegmentDTOs.add(exSegment);
	}
	
	private static void addExchangedOALSegment(TypeBRequestDTO typeBRequestDTO, List<BookingSegmentDTO> bookingSegmentDTOs,
			BookingSegmentDTO segment, ReservationSegmentDTO oldResSegment) throws ModuleException {
		BookingSegmentDTO exSegment = new BookingSegmentDTO();
		exSegment.setDepartureStation(segment.getDepartureStation());
		exSegment.setDepartureDate(segment.getDepartureDate());
		exSegment.setDepartureTime(segment.getDepartureTime());
		exSegment.setDestinationStation(segment.getDestinationStation());
		exSegment.setArrivalDate(segment.getArrivalDate());
		exSegment.setArrivalTime(segment.getArrivalTime());
		exSegment.setCarrierCode(segment.getCarrierCode());
		exSegment.setFlightNumber(segment.getFlightNumber());
		exSegment.setNoofPax(segment.getNoofPax());

		String bookingClass = null;
		if (oldResSegment.getFareTO() != null) {
			bookingClass = oldResSegment.getFareTO().getBookingClassCode();
		} else {
			bookingClass = oldResSegment.getCabinClassCode();
		}
		String gdsBookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(typeBRequestDTO.getCsOCCarrierCode(), bookingClass);
		exSegment.setBookingCode(gdsBookingClass);
		exSegment.setAdviceOrStatusCode(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode());
		bookingSegmentDTOs.add(exSegment);
	}
}
