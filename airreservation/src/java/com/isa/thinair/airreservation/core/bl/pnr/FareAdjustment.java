package com.isa.thinair.airreservation.core.bl.pnr;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.DepAPFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for handling fare adjustment related operations on Fare. Fare Adjustment is calculated when fare is defined
 * in local currency and ROE is Changed at this stage if a +/- credit generated then it should be ignored by the system.
 * In order to handle such cases following class is required.
 * 
 * @author M.Rikaz
 * 
 * @isa.module.command name="fareAdjustment"
 */

public class FareAdjustment extends DefaultBaseCommand {
	private static Log log = LogFactory.getLog(FareAdjustment.class);
	private Reservation reservation = null;
	private Map<String, String> requoteSegmentMap = null;
	Map<String, Map<Integer, BigDecimal>> prevFareAdjMapByPaxType = null;

	@SuppressWarnings({ "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside Fare Adjestment");

		Collection<OndFareDTO> ondFareDTOs = this.getParameter(CommandParamNames.OND_FARE_DTOS, Collection.class);

		String pnr = (String) this.getParameter(CommandParamNames.PNR);

		reservation = loadReservation(pnr);

		requoteSegmentMap = (Map<String, String>) this.getParameter(CommandParamNames.REQUOTE_SEGMENT_MAP);

		Boolean isModifyOperation = (Boolean) this.getParameter(CommandParamNames.IS_MODIFY_OND_OPERATION);

		if (isModifyOperation == null) {
			isModifyOperation = new Boolean(false);
		}

		boolean isFareAdjustmentEnabledForModification = AppSysParamsUtil.isFareAdjustmentEnabledForModification();

		boolean isCNFReservation = ReservationStatus.CONFIRMED.equals(reservation.getStatus()) ? true : false;

		boolean isFareAdjustmentValid = false;

		if (isFareAdjustmentEnabledForModification && isCNFReservation && isModifyOperation) {

			this.validateParams(reservation, requoteSegmentMap, ondFareDTOs);

			if (isSameFlightModification(ondFareDTOs) && validateCurrencyCode(ondFareDTOs)) {

				prevFareAdjMapByPaxType = new HashMap<String, Map<Integer, BigDecimal>>();
				extractPrevFareAdjustmentMap();

				isFareAdjustmentValid = true;
				this.calculateFareAdjestments(ondFareDTOs);
			}

		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.FARE_ADJUSTMENT_VALID, isFareAdjustmentValid);
		if(this.getParameter(CommandParamNames.BOOKING_TYPE) != null){
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, this.getParameter(CommandParamNames.BOOKING_TYPE));
		}
		log.debug("Exit Fare Adjestment");
		return response;
	}

	private void calculateFareAdjestments(Collection<OndFareDTO> ondFareDTOs) throws ModuleException {

		if (reservation != null && reservation.getSegmentsView() != null && reservation.getSegmentsView().size() > 0
				&& requoteSegmentMap != null && requoteSegmentMap.size() > 0 && reservation.getPassengers() != null
				&& reservation.getPassengers().size() > 0 && ondFareDTOs != null && ondFareDTOs.size() > 0) {
			boolean hasAdult = false;
			boolean hasChild = false;
			boolean hasInfant = false;
			for (ReservationPax pax : reservation.getPassengers()) {
				if (!hasAdult && PaxTypeTO.ADULT.equals(pax.getPaxType())) {
					hasAdult = true;
				}

				if (!hasChild && PaxTypeTO.CHILD.equals(pax.getPaxType())) {
					hasChild = true;
				}

				if (!hasInfant && PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					hasInfant = true;
				}
				if (hasAdult && hasChild && hasInfant) {
					break;
				}
			}

			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				BigDecimal adultFareDiff = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal childFareDiff = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal infantFareDiff = AccelAeroCalculator.getDefaultBigDecimalZero();

				for (Integer fltSegId : ondFareDTO.getSegmentsMap().keySet()) {
					FlightSegmentDTO flightSegmentDTO = ondFareDTO.getFlightSegmentDTO(new Integer(fltSegId));

					Fare oldFare = getFltSegmentOldFareById(fltSegId);

					if (flightSegmentDTO != null && oldFare != null) {

						FareSummaryDTO fareSummaryDTO = ondFareDTO.getFareSummaryDTO();

						if (oldFare.getLocalCurrencyCode() != null
								&& oldFare.getLocalCurrencyCode().equals(fareSummaryDTO.getCurrenyCode())) {

							double oldAdultFareInLocal = oldFare.getFareAmountInLocal();
							double oldChildFareInLocal = oldFare.getChildFareInLocal();
							double oldInfantFareInLocal = oldFare.getInfantFareInLocal();

							DepAPFareDTO depAPFareDTO = fareSummaryDTO.getDepartureAPFare();

							double oldAdultFareInBase = calculatePercentage(oldFare.getFareAmount(),
									fareSummaryDTO.getFarePercentage(), true);

							double oldChildFareInBase = 0;
							double oldInfantFareInBase = 0;

							if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(oldFare.getInfantFareType())) {
								oldInfantFareInBase = calculatePercentage(oldFare.getInfantFare(),
										fareSummaryDTO.getFarePercentage(), true);

								oldInfantFareInLocal = calculatePercentage(oldFare.getInfantFareInLocal(),
										fareSummaryDTO.getFarePercentage(), false);

							} else {
								oldInfantFareInBase = calculatePercentage(oldAdultFareInBase,
										new BigDecimal(fareSummaryDTO.getInfantFare()).intValue(), true);

								oldInfantFareInLocal = calculatePercentage(oldAdultFareInLocal,
										new BigDecimal(fareSummaryDTO.getInfantFare()).intValue(), false);

							}

							if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(oldFare.getChildFareType())) {
								oldChildFareInBase = calculatePercentage(oldFare.getChildFare(),
										fareSummaryDTO.getFarePercentage(), true);

								oldChildFareInLocal = calculatePercentage(oldFare.getChildFareInLocal(),
										fareSummaryDTO.getFarePercentage(), false);
							} else {
								oldChildFareInBase = calculatePercentage(oldAdultFareInBase,
										new BigDecimal(fareSummaryDTO.getChildFare()).intValue(), true);

								oldChildFareInLocal = calculatePercentage(oldAdultFareInLocal,
										new BigDecimal(fareSummaryDTO.getChildFare()).intValue(), false);
							}

							double newChildFareInBase = 0;
							double newInfantFareInBase = 0;

							if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fareSummaryDTO.getChildFareType())) {
								newChildFareInBase = fareSummaryDTO.getChildFare();
							} else {
								newChildFareInBase = calculatePercentage(fareSummaryDTO.getAdultFare(), new BigDecimal(
										fareSummaryDTO.getChildFare()).intValue(), true);
							}

							if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(fareSummaryDTO.getInfantFareType())) {
								newInfantFareInBase = fareSummaryDTO.getInfantFare();
							} else {

								newInfantFareInBase = calculatePercentage(fareSummaryDTO.getAdultFare(), new BigDecimal(
										fareSummaryDTO.getInfantFare()).intValue(), true);
							}

							if (depAPFareDTO != null) {
								if (oldAdultFareInLocal == depAPFareDTO.getAdultFare()
										&& oldAdultFareInBase != fareSummaryDTO.getAdultFare()) {

									adultFareDiff = AccelAeroCalculator.subtract(
											AccelAeroCalculator.parseBigDecimal(oldAdultFareInBase),
											AccelAeroCalculator.parseBigDecimal(fareSummaryDTO.getAdultFare()));

								}

								if (oldChildFareInLocal == depAPFareDTO.getChildFare()
										&& oldChildFareInBase != newChildFareInBase) {

									childFareDiff = AccelAeroCalculator.subtract(
											AccelAeroCalculator.parseBigDecimal(oldChildFareInBase),
											AccelAeroCalculator.parseBigDecimal(newChildFareInBase));

								}

								if (oldInfantFareInLocal == depAPFareDTO.getInfantFare()
										&& oldInfantFareInBase != newInfantFareInBase) {
									infantFareDiff = AccelAeroCalculator.subtract(
											AccelAeroCalculator.parseBigDecimal(oldInfantFareInBase),
											AccelAeroCalculator.parseBigDecimal(newInfantFareInBase));

								}
							}

						}

						Map<String, BigDecimal> fareDiffByPaxTypeMap = new HashMap<String, BigDecimal>();

						adultFareDiff = AccelAeroCalculator.add(adultFareDiff,
								getAppliedFareAdjustments(PaxTypeTO.ADULT, oldFare.getFareId()));
						childFareDiff = AccelAeroCalculator.add(childFareDiff,
								getAppliedFareAdjustments(PaxTypeTO.CHILD, oldFare.getFareId()));
						infantFareDiff = AccelAeroCalculator.add(infantFareDiff,
								getAppliedFareAdjustments(PaxTypeTO.INFANT, oldFare.getFareId()));

						fareDiffByPaxTypeMap.put(PaxTypeTO.ADULT, adultFareDiff);
						fareDiffByPaxTypeMap.put(PaxTypeTO.CHILD, childFareDiff);
						fareDiffByPaxTypeMap.put(PaxTypeTO.INFANT, infantFareDiff);

						ondFareDTO.injectFareAdjustment(fareDiffByPaxTypeMap, hasAdult, hasChild, hasInfant);

					}

				}

			}

		}

	}

	private Fare getFltSegmentOldFareById(Integer fltSegId) throws ModuleException {
		Fare oldFare = null;
		if (reservation != null && reservation.getSegmentsView() != null && reservation.getSegmentsView().size() > 0
				&& requoteSegmentMap != null && requoteSegmentMap.size() > 0) {

			String oldPnrSegId = requoteSegmentMap.get(fltSegId + "");
			Integer oldSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(oldPnrSegId);
			int oldFareId = 0;
			Collection<ReservationSegmentDTO> segmentsView = reservation.getSegmentsView();

			if (!StringUtil.isNullOrEmpty(oldPnrSegId)) {
				for (ReservationSegmentDTO resSegmentDTO : segmentsView) {
					if (oldSegId.equals(resSegmentDTO.getPnrSegId())) {
						oldFareId = resSegmentDTO.getFareTO().getFareId();
						break;
					}
				}

				if (oldFareId > 0) {
					FareDTO oldFareDTO = ReservationModuleUtils.getFareBD().getFare(oldFareId);
					oldFare = oldFareDTO.getFare();
				}
			}

		}

		return oldFare;
	}

	private double calculatePercentage(double input, int percentage, boolean roundUpAmount) {

		double amount = (new Double(AccelAeroCalculator.calculatePercentage(AccelAeroCalculator.parseBigDecimal(input),
				percentage, RoundingMode.DOWN).toString())).doubleValue();

		if (roundUpAmount && AppSysParamsUtil.isRoundupChildAndInfantFaresWhenCalculatingFromAdultFare()) {
			amount = new Double(AccelAeroCalculator.parseBigDecimal(amount).setScale(0, RoundingMode.UP).toString())
					.doubleValue();
		}

		return amount;

	}

	private boolean isSameFlightModification(Collection<OndFareDTO> ondFareDTOs) {
		boolean isSameFlightModification = false;
		if (reservation != null && reservation.getSegmentsView() != null && reservation.getSegmentsView().size() > 0
				&& requoteSegmentMap != null && requoteSegmentMap.size() > 0 && ondFareDTOs != null && ondFareDTOs.size() > 0) {
			int i = 0;
			if (isSegmentExistInCurrentPNR(reservation.getSegmentsView(), ondFareDTOs)) {
				for (OndFareDTO ondFareDTO : ondFareDTOs) {
					i = i + ondFareDTO.getSegmentsMap().size();
					for (Integer fltSegId : ondFareDTO.getSegmentsMap().keySet()) {
						FlightSegmentDTO flightSegmentDTO = ondFareDTO.getFlightSegmentDTO(new Integer(fltSegId));

						String oldPnrSegId = requoteSegmentMap.get(fltSegId + "");
						Integer oldSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(oldPnrSegId);

						Collection<ReservationSegmentDTO> segmentsView = reservation.getSegmentsView();

						if (!StringUtil.isNullOrEmpty(oldPnrSegId)) {
							for (ReservationSegmentDTO resSegmentDTO : segmentsView) {
								if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSegmentDTO
										.getStatus()) && oldSegId.equals(resSegmentDTO.getPnrSegId())) {

									boolean isSameRoute = false;

									if (flightSegmentDTO.getSegmentCode().equals(resSegmentDTO.getSegmentCode())) {
										isSameRoute = true;
										isSameFlightModification = true;
									}

									if (!isSameRoute) {
										return false;
									}

									break;
								}
							}

						}

					}
				}
			} else {
				return false;
			}

			if (i != getTotalCNFSegmentCount(reservation.getSegmentsView())) {
				return false;
			}

		}

		return isSameFlightModification;
	}

	/**
	 * if this function returns false then we must skip fare adjustment calculation
	 * 
	 * */
	private boolean
			isSegmentExistInCurrentPNR(Collection<ReservationSegmentDTO> segmentsView, Collection<OndFareDTO> ondFareDTOs) {
		for (ReservationSegmentDTO resSegmentDTO : segmentsView) {
			if (!isSegmentFound(resSegmentDTO.getSegmentCode(), ondFareDTOs)) {
				return false;
			}
		}
		return true;
	}

	private boolean isSegmentFound(String segmentCode, Collection<OndFareDTO> ondFareDTOs) {
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			for (Integer fltSegId : ondFareDTO.getSegmentsMap().keySet()) {
				FlightSegmentDTO flightSegmentDTO = ondFareDTO.getFlightSegmentDTO(new Integer(fltSegId));

				if (flightSegmentDTO.getSegmentCode().equals(segmentCode)) {
					return true;
				}
			}
		}
		return false;
	}

	private int getTotalCNFSegmentCount(Collection<ReservationSegmentDTO> segmentsView) {
		int i = 0;
		if (segmentsView != null && segmentsView.size() > 0) {
			for (ReservationSegmentDTO resSegmentDTO : segmentsView) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSegmentDTO.getStatus())) {
					i++;
				}
			}
		}
		return i;
	}

	private boolean validateCurrencyCode(Collection<OndFareDTO> ondFareDTOs) throws ModuleException {
		boolean isValidCurrency = false;
		ArrayList<String> newFareCurrencyCodeList = new ArrayList<String>();
		ArrayList<String> paxPaymentCurrencyCodeList = new ArrayList<String>();

		if (reservation.getPassengers() != null && reservation.getPassengers().size() > 0) {
			Iterator<ReservationPax> passengersItr = reservation.getPassengers().iterator();
			while (passengersItr.hasNext()) {
				ReservationPax reservationPax = passengersItr.next();

				if (reservationPax.getPaxPaymentTnxView() == null) {
					throw new ModuleException("airreservations.arg.invalid.null");
				}

				if (reservationPax.getPaxPaymentTnxView().size() > 0) {
					for (ReservationTnx reservationTnx : reservationPax.getPaxPaymentTnxView()) {
						String payCurrencyCode = reservationTnx.getPayCurrencyCode();

						if (!StringUtil.isNullOrEmpty(payCurrencyCode)) {
							if (!paxPaymentCurrencyCodeList.contains(payCurrencyCode)
									&& reservationTnx.getAmount().doubleValue() != 0) {
								paxPaymentCurrencyCodeList.add(payCurrencyCode);
							}

						}

					}
				}

			}

			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				String ondFareCurrencyCode = ondFareDTO.getFareSummaryDTO().getCurrenyCode();
				if (!StringUtil.isNullOrEmpty(ondFareCurrencyCode)) {
					if (!newFareCurrencyCodeList.contains(ondFareCurrencyCode)) {
						newFareCurrencyCodeList.add(ondFareCurrencyCode);
					}
				}

			}

			if (newFareCurrencyCodeList.size() == 1 && paxPaymentCurrencyCodeList.size() == 1
					&& newFareCurrencyCodeList.containsAll(paxPaymentCurrencyCodeList)
					&& !AppSysParamsUtil.getBaseCurrency().equals(paxPaymentCurrencyCodeList.get(0))) {

				isValidCurrency = true;
			}

		}

		return isValidCurrency;
	}

	private void
			validateParams(Reservation reservation, Map<String, String> requoteSegmentMap, Collection<OndFareDTO> ondFareDTOs)
					throws ModuleException {

		if (reservation == null || reservation.getSegmentsView() == null || reservation.getPassengers() == null
				|| ondFareDTOs == null || requoteSegmentMap == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

	}

	private Reservation loadReservation(String pnr) throws ModuleException {
		if (StringUtil.isNullOrEmpty(pnr)) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);

		return ReservationProxy.getReservation(pnrModesDTO);
	}

	private void extractPrevFareAdjustmentMap() throws ModuleException {
		for (ReservationPax pax : reservation.getPassengers()) {

			if (prevFareAdjMapByPaxType.get(pax.getPaxType()) == null) {
				Map<Integer, BigDecimal> fareAdjByFaresMap = new HashMap<Integer, BigDecimal>();
				for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {

					boolean isCnfSegment = true;

					for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
						if (!ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(paxFareSeg.getSegment()
								.getStatus())) {
							isCnfSegment = false;
							break;
						}
					}

					if (isCnfSegment) {
						Integer fareId = paxFare.getFareId();
						BigDecimal fareAdjustment = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (ReservationPaxOndCharge paxCharge : paxFare.getCharges()) {
							if (ReservationInternalConstants.ChargeGroup.FAR.equals(paxCharge.getChargeGroupCode())
									&& paxCharge.getAdjustment() != null && paxCharge.getAdjustment().doubleValue() != 0) {

								fareAdjustment = AccelAeroCalculator.add(fareAdjustment, paxCharge.getAdjustment());
							}
						}
						fareAdjByFaresMap.put(fareId, fareAdjustment);
					}

				}

				prevFareAdjMapByPaxType.put(pax.getPaxType(), fareAdjByFaresMap);

			}

		}
	}

	private BigDecimal getAppliedFareAdjustments(String paxType, Integer fareId) {
		if (prevFareAdjMapByPaxType != null && prevFareAdjMapByPaxType.size() > 0 && !StringUtil.isNullOrEmpty(paxType)) {
			Map<Integer, BigDecimal> fareAdjByFaresMap = prevFareAdjMapByPaxType.get(paxType);

			if (fareAdjByFaresMap != null && fareAdjByFaresMap.size() > 0 && fareAdjByFaresMap.get(fareId) != null) {
				return fareAdjByFaresMap.get(fareId);
			}
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();
	}
}
