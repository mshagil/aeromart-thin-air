package com.isa.thinair.airreservation.core.audit;

public class PNLADLHistoryAuditConstants {
	
	public static final String FLIGHT_ID = "flight id";
	
	public static final String SITA_ADDRESS = "sita address";
	
	public static final String DEPARTURE_STATION = "departure station"; 
	
	public static final String MESSAGE_TYPE = "message type"; 
	
	public static final String TRANSMISSION_STATUS = "transmission status";
	
}
