package com.isa.thinair.airreservation.core.bl.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.PromotionAttribute;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.constants.PromoTemplateParam;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.promotion.api.utils.PromotionsInternalConstants;
import com.isa.thinair.promotion.api.utils.PromotionsUtils;

/**
 * @author Nilindra Fernando
 * @since Oct 23, 2012
 */
public class PromotionBL {

	private static PromotionAttribute composePromotionAttribute(String pnr, String promotionType, String attribute, String value) {
		PromotionAttribute promotionAttribute = new PromotionAttribute();
		promotionAttribute.setPnr(pnr);
		promotionAttribute.setPromotionType(promotionType);
		promotionAttribute.setAttribute(attribute);
		promotionAttribute.setValue(value);

		return promotionAttribute;
	}

	public static void saveAuditNextSeatPromotionRequest(String pnr, String status, Map<String, String> contentMapForAudit,
			CredentialsDTO credentialsDTO) throws ModuleException {

		Collection<PromotionAttribute> colPromotionAttribute = new ArrayList<PromotionAttribute>();
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);
		reservationAudit.setModificationType(AuditTemplateEnum.PROMOTION_NEXT_SEAT_FREE_REQUEST_LEVEL.getCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.APPROVAL_STATUS,
				status);
		colPromotionAttribute.add(composePromotionAttribute(pnr,
				AuditTemplateEnum.PROMOTION_NEXT_SEAT_FREE_REQUEST_LEVEL.getCode(),
				AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.APPROVAL_STATUS, status));

		if (PromotionsInternalConstants.PromotionRequestStatus.APPROVED.getStatusCode().equals(status)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.ADDED_SEATS,
					contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.ADDED_SEATS));
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REMOVED_SEATS,
					contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REMOVED_SEATS));
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.PNR,
					contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.PNR));
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.NUMBEROFSEATS,
					contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.NUMBEROFSEATS));

			colPromotionAttribute.add(composePromotionAttribute(pnr,
					AuditTemplateEnum.PROMOTION_NEXT_SEAT_FREE_REQUEST_LEVEL.getCode(),
					AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.NUMBEROFSEATS,
					contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.NUMBEROFSEATS)));

			StringBuilder sb = new StringBuilder();
			sb.append("Next Seat Free request approved:");
			sb.append(" Added Seats - ");
			sb.append((String) contentMapForAudit
					.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.ADDED_SEATS));

			if (!contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REMOVED_SEATS)
					.isEmpty()) {
				sb.append(" Removed Seats - ");
				sb.append(contentMapForAudit
						.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REMOVED_SEATS));
			}

			sb.append(" for ");
			sb.append(contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.PAX_NAME));
			sb.append(" on ");
			sb.append(contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.SEG_CODE));

			reservationAudit.setUserNote(sb.toString());
		} else {
			colPromotionAttribute.add(composePromotionAttribute(pnr,
					AuditTemplateEnum.PROMOTION_NEXT_SEAT_FREE_REQUEST_LEVEL.getCode(),
					AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REFUND_AMOUNT,
					contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionNextSeatFreeApproveRequest.REFUND_AMOUNT)));
		}

		if (colPromotionAttribute.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveOrUpdatePromotionForReservation(colPromotionAttribute);
		}

		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		auditorBD.audit(reservationAudit, reservationAudit.getContentMap());

	}

	public static void saveAuditFlexiDatePromotionRequest(String pnr, String status, Map<String, Object> contentMapForAudit,
			CredentialsDTO credentialsDTO) throws ModuleException {

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);
		reservationAudit.setModificationType(AuditTemplateEnum.PROMOTION_FLEXI_DATE_REQUEST_LEVEL.getCode());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.APPROVAL_STATUS, status);

		Collection<PromotionAttribute> colPromotionAttribute = new ArrayList<PromotionAttribute>();
		colPromotionAttribute.add(composePromotionAttribute(pnr, AuditTemplateEnum.PROMOTION_FLEXI_DATE_REQUEST_LEVEL.getCode(),
				AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.APPROVAL_STATUS, status));

		if ((PromotionsInternalConstants.PromotionRequestStatus.APPROVED).getStatusCode().equals(status)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.ORIGINAL_FLIGHT_ID,
					((Integer) contentMapForAudit
							.get(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.ORIGINAL_FLIGHT_ID))
							.toString());

			reservationAudit.addContentMap(
					AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.ORIGINAL_DEPARTURE_DATE,
					((Date) contentMapForAudit
							.get(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.ORIGINAL_DEPARTURE_DATE))
							.toString());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.PNR,
					(String) contentMapForAudit.get(AuditTemplateEnum.TemplateParams.PromotionFlexiDateApproveRequest.PNR));

		}

		if (colPromotionAttribute.size() > 0) {
			ReservationDAOUtils.DAOInstance.RESERVATION_DAO.saveOrUpdatePromotionForReservation(colPromotionAttribute);
		}

		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
	}

	/**
	 * Audits related to promotion subcription and unsubscription
	 * 
	 * @param pnr
	 * @param status
	 * @param contentMapForAudit
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	public static void saveAuditPromotionRequestSubcriptions(PromotionType promotionType, String pnr, String status,
			Map<String, Object> contentMapForAudit, CredentialsDTO credentialsDTO,
			Map<Integer, List<String>> segCodesDirectionWise) throws ModuleException {

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.createReservationAudit(reservationAudit, credentialsDTO, false);

		if (promotionType == PromotionType.PROMOTION_FLEXI_DATE) {
			reservationAudit.setModificationType(AuditTemplateEnum.PROMOTION_FLEXI_DATE_SUBSCRIPTION.getCode());

			String direction = contentMapForAudit.get((AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.DIRECTION))
					.toString();
			String ondCode = null;
			if (direction.equals("outbound")) {
				ondCode = PromotionsUtils.getOndCode(segCodesDirectionWise.get(PromotionsInternalConstants.DIRECTION_OUTBOUND));
			} else if (direction.equals("inbound")) {
				ondCode = PromotionsUtils.getOndCode(segCodesDirectionWise.get(PromotionsInternalConstants.DIRECTION_INBOUND));
			}
			String lowerBound = contentMapForAudit
					.get(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.LOWER_BOUND).toString();
			String upperBound = contentMapForAudit
					.get(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.UPPER_BOUND).toString();
			String userNote = PromoTemplateParam.ChargeAndReward
					.getFlexiDateRegistrationUserNote(lowerBound, upperBound, ondCode);

			reservationAudit.setUserNote(userNote);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.DIRECTION, direction);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.REQUEST_STATUS, status);
			reservationAudit
					.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.LOWER_BOUND, lowerBound);
			reservationAudit
					.addContentMap(AuditTemplateEnum.TemplateParams.PromotionFlexiDateSubsciption.UPPER_BOUND, upperBound);

		}

		reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
	}
}
