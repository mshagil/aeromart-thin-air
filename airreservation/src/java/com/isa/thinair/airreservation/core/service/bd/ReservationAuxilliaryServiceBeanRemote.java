package com.isa.thinair.airreservation.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;

@Remote
public interface ReservationAuxilliaryServiceBeanRemote extends ReservationAuxilliaryBD {

}
