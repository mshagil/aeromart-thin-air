package com.isa.thinair.airreservation.core.bl.tty;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class NameChangeMessageCreator extends TypeBReservationMessageCreator {
	
	public NameChangeMessageCreator() {
		segmentsComposingStrategy = new SegmentsCommonComposer();
		passengerNamesComposingStrategy = new PassengerNamesComposerForNameChange();
	}
}
