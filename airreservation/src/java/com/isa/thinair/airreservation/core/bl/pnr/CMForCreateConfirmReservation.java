/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.SMUtil;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.common.AirportTransferAdaptor;
import com.isa.thinair.airreservation.core.bl.common.AutomaticCheckinAdaptor;
import com.isa.thinair.airreservation.core.bl.common.BaggageAdaptor;
import com.isa.thinair.airreservation.core.bl.common.InsuranceHelper;
import com.isa.thinair.airreservation.core.bl.common.MealAdapter;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;

/**
 * Command for creating a modification object for the call center reservation
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="cMForCreateConfirmReservation"
 */
public class CMForCreateConfirmReservation extends DefaultBaseCommand {

	/** Holds the logger instance ye */
	private static Log log = LogFactory.getLog(CMForCreateConfirmReservation.class);

	/**
	 * Execute method of the cMForCreateConfirmReservation command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);

		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Map pnrPaxIdAndPayments = (Map) this.getParameter(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS);
		Map flightSeatIdsAndPaxTypes = (Map) this.getParameter(CommandParamNames.FLIGHT_SEAT_IDS_AND_PAX_TYPES);
		List<InsuranceResponse> insuranceResponses = (List<InsuranceResponse>) this.getParameter(CommandParamNames.INSURANCE_RES);
		List<Integer> openRTFltSegIds = (List<Integer>) this.getParameter(CommandParamNames.OPENRT_SEGMENT_IDS);
		Collection[] flightMealInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_MEAL_IDS);
		Collection[] airportTransferInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_AIRPORT_TRANSFER_IDS);
		String bookingType = (String) this.getParameter(CommandParamNames.BOOKING_TYPE);
		Collection[] flightBaggageInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_BAGGAGE_IDS);
		Collection[] flightAutoCheckinInfo = (Collection[]) this.getParameter(CommandParamNames.FLIGHT_AUTO_CHECKIN_IDS);
		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);
		Collection<FlexiExternalChgDTO> flexiCharges = (Collection<FlexiExternalChgDTO>) this
				.getParameter(CommandParamNames.FLEXI_INFO);

		Set<FlightSegmentDTO> flightSegmentDTOs = new HashSet<FlightSegmentDTO>();

		if (this.getParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET) != null) {
			flightSegmentDTOs = (Set<FlightSegmentDTO>) this.getParameter(CommandParamNames.FLIGHT_SEGMENT_DTO_SET);
		}

		String fareDiscountAudit = (String) this.getParameter(CommandParamNames.FARE_DISCOUNT_AUDIT);

		// Checking params
		this.validateParams(reservation, pnrPaxIdAndPayments, credentialsDTO);

		// Get the seat information
		String seatCodes = getSeatCodesForAudit(flightSeatIdsAndPaxTypes);

		// Get the seat information
		Collection[] colResInfo = { reservation.getPassengers(), reservation.getSegmentsView() };

		String mealCodes = null;
		if (flightMealInfo != null && flightMealInfo.length > 0) {
			mealCodes = MealAdapter.recordPaxWiseAuditHistoryForModifyMeals(reservation.getPnr(), flightMealInfo[0],
					credentialsDTO, null, colResInfo);
		}

		String baggageNames = null;
		if (flightBaggageInfo != null && flightBaggageInfo.length > 0) {
			baggageNames = BaggageAdaptor.recordPaxWiseAuditHistoryForModifyBaggages(reservation.getPnr(), flightBaggageInfo[0],
					credentialsDTO, null, colResInfo);
		}

		String airportTransfers = null;
		if (airportTransferInfo != null && airportTransferInfo.length > 0) {
			airportTransfers = AirportTransferAdaptor.recordPaxWiseAuditHistoryForModifyAirportTransfers(reservation.getPnr(),
					airportTransferInfo[0], credentialsDTO, null, colResInfo);
		}

		String autoCheckins = null;
		if (flightAutoCheckinInfo != null && flightAutoCheckinInfo.length > 0) {
			autoCheckins = AutomaticCheckinAdaptor.recordPaxWiseAuditHistoryForModifyAutoCheckin(flightAutoCheckinInfo[0]);
		}

		// Get flexi information
		String flexiInfo = null;
		if (flexiCharges != null && flexiCharges.size() > 0) {
			flexiInfo = ReservationApiUtils.getFlexiInfoForAudit(flexiCharges, reservation, null);
		}

		String insuranceAudit = InsuranceHelper.getAudit(insuranceResponses);
		String segmentInformation = ReservationApiUtils
				.getSegmentInformationForReservation(new ArrayList<FlightSegmentDTO>(flightSegmentDTOs), openRTFltSegIds);

		// Compose Modification object
		Collection colReservationAudit = this.composeAudit(reservation, reservation.getUserNote(), pnrPaxIdAndPayments,
				credentialsDTO, seatCodes, insuranceAudit, mealCodes, baggageNames, flexiInfo, segmentInformation,
				fareDiscountAudit, bookingType, airportTransfers, autoCheckins);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}



	/**
	 * Returns the seat codes for audit
	 * 
	 * @param flightSeatIdsAndPaxTypes
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String getSeatCodesForAudit(Map flightSeatIdsAndPaxTypes) {
		String seatCodes = null;

		if (flightSeatIdsAndPaxTypes != null && flightSeatIdsAndPaxTypes.keySet().size() > 0) {
			Collection<String> seatCodesValues = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
					.getSeatCodes(flightSeatIdsAndPaxTypes.keySet());

			if (seatCodesValues != null && seatCodesValues.size() > 0) {
				seatCodes = SMUtil.getSeatAuditString(seatCodesValues);
			}
		}

		return seatCodes;
	}

	/**
	 * Returns the Meal codes for audit
	 * 
	 * @param flightSeatIdsAndPaxTypes
	 * @return
	 */
	// private String getMealCodesForAudit(Collection<Integer> flightMealIds) {
	// String mealCodes = null;
	//
	// if (flightMealIds != null && flightMealIds.size() > 0) {
	// Collection<String> mealCodesValues = ReservationDAOUtils.DAOInstance.MEAL_DAO.getMealCodes(flightMealIds);
	//
	// if (mealCodesValues != null && mealCodesValues.size() > 0) {
	// mealCodes = MealUtil.getMealAuditString(mealCodesValues);
	// }
	// }
	//
	// return mealCodes;
	// }

	/**
	 * Compose the audit object
	 * 
	 * @param reservation
	 * @param userNote
	 * @param pnrPaxIdAndPayments
	 * @param credentialsDTO
	 * @param autoCheckins
	 * @param airportTransfers
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Collection composeAudit(Reservation reservation, String userNote, Map pnrPaxIdAndPayments,
			CredentialsDTO credentialsDTO, String seatCodes, String insuranceStr, String strMeals, String strBaggages,
			String flexiInfo, String segmentInfo, String fareDiscountInfo, String bookingType, String strAirportTransfers,
			String strAutoCheckins)
			throws ModuleException {

		Collection colReservationAudit = new ArrayList();

		// Get the passenger payment information
		String audit = ReservationCoreUtils.getPassengerPaymentInfo(pnrPaxIdAndPayments, reservation);

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(reservation.getPnr());
		reservationAudit.setModificationType(AuditTemplateEnum.ADDED_CONFIRMED_RESERVATION.getCode());
		reservationAudit.setUserNote(userNote);
		reservationAudit.setUserNoteType(reservation.getUserNoteType());

		// Set reservation audit information
		this.setReservationAuditInformation(reservation, reservationAudit);

		// Setting the passenger information
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.PASSENGER_DETAILS, audit);

		// Setting booking information
		if (bookingType != null && bookingType.equals("OVERBOOK")) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.BOOKING_TYPE,
					"OverBooking");
		}

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.IP_ADDDRESS,
					credentialsDTO.getTrackInfoDTO().getIpAddress());
		}

		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.ORIGIN_CARRIER,
					credentialsDTO.getTrackInfoDTO().getCarrierCode());
		}

		if (reservation.getOriginCountryOfCall() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.ORIGIN_COUNTRY_OF_CALL,
					reservation.getOriginCountryOfCall());
		}

		if (StringUtils.isNotEmpty(segmentInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.FLIGHT_SEGMENT_DETAILS,
					segmentInfo);
		}

		if (seatCodes != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Seats.SEATS, seatCodes);
		}

		if (insuranceStr != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Insurance.INSURANCE, insuranceStr);
		}
		if (strMeals != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Meals.MEALS, strMeals);
		}

		if (strAirportTransfers != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AirportTransfers.AIRPORTTRANSFERS,
					strAirportTransfers);
		}

		if (strAutoCheckins != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AutomaticCheckins.AUTOMATICCHECKINS, strAutoCheckins);
		}

		if (strBaggages != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Baggages.BAGGAGES, strBaggages);
		}

		if (flexiInfo != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.Flexibilities.FLEXIBILITIES, flexiInfo);
		}

		if (fareDiscountInfo != null && !"".equals(fareDiscountInfo)) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.FareDiscount.FARE_DISCOUNT_INFO, fareDiscountInfo);
		}

		String itineraryFareMask = reservation.getItineraryFareMaskFlag();

		if (itineraryFareMask != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ItineraryFareMask.ITINERARY_FARE_MAKS,
					itineraryFareMask);
		}

		colReservationAudit.add(reservationAudit);
		return colReservationAudit;
	}

	/**
	 * Set Reservation audit information
	 * 
	 * @param reservation
	 * @param reservationAudit
	 * @throws ModuleException
	 */
	private void setReservationAuditInformation(Reservation reservation, ReservationAudit reservationAudit)
			throws ModuleException {
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.FIRST_NAME,
				reservation.getContactInfo().getFirstName());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.LAST_NAME,
				reservation.getContactInfo().getLastName());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.STREET_ADDRESS_1,
				reservation.getContactInfo().getStreetAddress1());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.STREET_ADDRESS_2,
				reservation.getContactInfo().getStreetAddress2());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.CITY,
				reservation.getContactInfo().getCity());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.STATE,
				reservation.getContactInfo().getState());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.COUNTRY,
				reservation.getContactInfo().getCountryCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.PHONE_NUMBER,
				reservation.getContactInfo().getPhoneNo());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.MOBILE_NUMBER,
				reservation.getContactInfo().getMobileNo());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.EMAIL,
				reservation.getContactInfo().getEmail());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.FAX,
				reservation.getContactInfo().getFax());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.NATIONALITY_CODE,
				reservation.getContactInfo().getNationalityCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddedConfirmedReservation.TOTAL_CHARGES,
				reservation.getTotalChargeAmount().toString());
	}

	/**
	 * Validate Parameters
	 * 
	 * @param reservation
	 * @param pnrPaxIdAndPayments
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Reservation reservation, Map pnrPaxIdAndPayments, CredentialsDTO credentialsDTO)
			throws ModuleException {
		log.debug("Inside validateParams");

		if (reservation == null || reservation.getContactInfo() == null || reservation.getSegments() == null
				|| reservation.getSegments().size() == 0 || reservation.getPassengers() == null
				|| reservation.getPassengers().size() == 0 || pnrPaxIdAndPayments == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

}
