package com.isa.thinair.airreservation.core.bl.revacc.breakdown.migrator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;

/**
 * @author Nilindra Fernando
 * @since Oct 27, 2010
 */
public class TnxGranularityMigratorViaMDB extends PlatformBaseServiceDelegate {
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/tnxmigratorQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void sendMessageViaMDB(String pnr) {
		try {
			sendMessage(pnr, ReservationModuleUtils.getAirReservationConfig().getJndiProperties(), DESTINATION_JNDI_NAME,
					CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}

}
