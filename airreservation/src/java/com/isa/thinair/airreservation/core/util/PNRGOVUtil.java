package com.isa.thinair.airreservation.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPassengerDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVPaymentDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVResInfoDTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVSegmentDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.PNRGOVReservationLog;
import com.isa.thinair.airreservation.api.model.PNRGOVTxHistoryLog;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class PNRGOVUtil {
	private static Log log = LogFactory.getLog(PNRGOVUtil.class);

	private final static String SUCCESS = "SUCCESS";
	private final static String SUBMITTED = "SUBMITTED";
	private final static String ERROR = "ERROR";

	public static List<PNRGOVDTO> composePNRGOVDataDTOsForFlightSegment(Integer flightSegmentID, String contryCode,
			String airportCode, int timePeriod, String inboundOutbound) throws ModuleException {
		log.debug("Compose PNRGOV data for :" + flightSegmentID);
		List<PNRGOVDTO> pnrGovDataList = new ArrayList<PNRGOVDTO>();
		List<PNRGOVResInfoDTO> reInfoListForFlightSegment;
		reInfoListForFlightSegment = getResInfoListForFlightSegment(flightSegmentID);
		for (PNRGOVResInfoDTO resInfoDto : reInfoListForFlightSegment) {
			String pnr = resInfoDto.getPnr();
			PNRGOVDTO pnrGovDto = new PNRGOVDTO();
			pnrGovDto.setReservationInfo(resInfoDto);
			List<PNRGOVSegmentDTO> reservationSegments = getSegmentDataForPNR(pnr);
			pnrGovDto.setContactInfo(getReservationContactInfo(pnr));
			List<PNRGOVPassengerDTO> reservationPassengers = getPassengerDataForPNR(pnr);
			if (reservationPassengers.isEmpty()) {
				continue;
			}
			populateAncillaryDetailsForSegments(reservationPassengers, reservationSegments);
			retrieveAndPopulatePassengerEticketInfo(pnr, reservationPassengers);
			retrieveAndPopulatePassengerPaymentInfo(reservationPassengers);
			pnrGovDto.setSegments(reservationSegments);
			pnrGovDto.setPassengers(reservationPassengers);
			String validationStatus = checkMandatoryFieldsExists(pnrGovDto);
			if (!validationStatus.equals(SUCCESS)) {
				logPnrGovReservationData(pnrGovDto, flightSegmentID, contryCode, airportCode, inboundOutbound, timePeriod, ERROR,
						validationStatus);
			} else {
				pnrGovDataList.add(pnrGovDto);
			}

		}
		return pnrGovDataList;
	}

	private static List<PNRGOVResInfoDTO> getResInfoListForFlightSegment(Integer flightSegmentID) throws ModuleException {
		List<PNRGOVResInfoDTO> resInfoList;
		ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
		resInfoList = reservationBD.getResInfoListForFlightSegment(flightSegmentID);

		return resInfoList;
	}

	public static FlightSegement getFlightSegmentForFlightSegID(Integer flightSegmentID) throws ModuleException {
		FlightSegement flightSegment = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegmentID);
		return flightSegment;
	}

	public static Flight getFlightForFlightID(Integer flightId) throws ModuleException {
		Flight flight = ReservationModuleUtils.getFlightBD().getFlight(flightId);
		return flight;
	}

	public static String getDateOfPreparation(Date currentDate) {
		String date = CalendarUtil.getDateInFormattedString("yyMMdd", currentDate);
		return date;
	}

	public static String getTimeOfPreparation(Date currentDate) {
		String date = CalendarUtil.getDateInFormattedString("HHmm", currentDate);
		return date;
	}

	public static String getCarrierCode() {
		return ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
	}

	public static String getHubAirport() {
		return ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
	}

	private static List<PNRGOVSegmentDTO> getSegmentDataForPNR(String pnr) {
		List<PNRGOVSegmentDTO> pnrSegmentDtoList;
		pnrSegmentDtoList = ReservationModuleUtils.getReservationBD().getSegmentsForPNR(pnr, getCarrierCode());
		return removeGroundSegments(pnrSegmentDtoList);
	}

	private static ReservationContactInfo getReservationContactInfo(String pnr) {
		return ReservationDAOUtils.DAOInstance.RESERVATION_DAO.getContactInfo(pnr);
	}

	private static List<PNRGOVPassengerDTO> getPassengerDataForPNR(String pnr) {
		List<PNRGOVPassengerDTO> reservationPassengers;
		reservationPassengers = ReservationModuleUtils.getPassengerBD().getPassengersForPNR(pnr);
		assignInfantsToAdults(reservationPassengers);
		return reservationPassengers;
	}

	private static void assignInfantsToAdults(List<PNRGOVPassengerDTO> reservationPassengers) {
		List<PNRGOVPassengerDTO> infants = new ArrayList<PNRGOVPassengerDTO>();
		for (PNRGOVPassengerDTO passenger : reservationPassengers) {
			if (passenger.getPassengerType().equals(PaxTypeTO.INFANT)) {
				infants.add(passenger);
			}
		}
		for (PNRGOVPassengerDTO infant : infants) {
			int adultId = infant.getAdultID();
			for (PNRGOVPassengerDTO passenger : reservationPassengers) {
				int pnrPaxId = passenger.getPnrPaxId();
				if (adultId == pnrPaxId) {
					passenger.addIntant(infant);
					break;
				}
			}
		}
	}

	private static void retrieveAndPopulatePassengerEticketInfo(String pnr, List<PNRGOVPassengerDTO> reservationPassengers)
			throws ModuleException {
		Collection<EticketTO> eTicketsForPnr = ReservationDAOUtils.DAOInstance.ETicketDAO.getReservationETickets(pnr);
		Map<Integer, Set<String>> paxWiseEticketMap = getPaxWiseETickets(eTicketsForPnr);
		populatePassengerEticketInfo(paxWiseEticketMap, reservationPassengers);
	}

	private static Map<Integer, Set<String>> getPaxWiseETickets(Collection<EticketTO> eTicketsForPnr) {
		Map<Integer, Set<String>> paxWiseEticketMap = new HashMap<Integer, Set<String>>();

		if (eTicketsForPnr != null) {
			for (EticketTO eTicket : eTicketsForPnr) {
				if (!eTicket.getTicketStatus().equals("CNX")) {
					int pnrPaxId = eTicket.getPnrPaxId();
					String eticketID = eTicket.getEticketNumber();
					Set<String> existingETickets = paxWiseEticketMap.get(pnrPaxId);
					if (existingETickets != null) {
						existingETickets.add(eticketID);
					} else {
						existingETickets = new HashSet<String>();
						existingETickets.add(eticketID);
						paxWiseEticketMap.put(pnrPaxId, existingETickets);
					}
				}
			}
		}

		return paxWiseEticketMap;
	}

	private static void populatePassengerEticketInfo(Map<Integer, Set<String>> paxWiseEticketMap,
			List<PNRGOVPassengerDTO> reservationPassengers) {
		for (PNRGOVPassengerDTO passenger : reservationPassengers) {
			int pnrPaxId = passenger.getPnrPaxId();
			Set<String> passengerEtickets = paxWiseEticketMap.get(pnrPaxId);
			if (passengerEtickets != null) {
				passenger.setPassengerEticket(passengerEtickets);
			}
		}
	}

	private static void retrieveAndPopulatePassengerPaymentInfo(List<PNRGOVPassengerDTO> reservationPassengers)
			throws ModuleException {
		for (PNRGOVPassengerDTO passenger : reservationPassengers) {
			Integer pnrPaxID = passenger.getPnrPaxId();
			List<PNRGOVPaymentDTO> allPayments = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO.getPaxPaymentDTOs(String
					.valueOf(pnrPaxID));
			if (allPayments != null && !allPayments.isEmpty()) {
				PNRGOVPaymentDTO paymentSummary = new PNRGOVPaymentDTO();
				PNRGOVPaymentDTO paidTransaction = getPaymentTransaction(allPayments);
				paymentSummary.setPaymentAmount(getTotalPaymentAmount(allPayments));
				paymentSummary.setPaymentCurrency(ReservationModuleUtils.getGlobalConfig().getBizParam(
						SystemParamKeys.BASE_CURRENCY));
				if (paidTransaction != null) {
					paymentSummary.setNominalCode(paidTransaction.getNominalCode());
					paymentSummary.setTnxDate(paidTransaction.getTnxDate());
				}

				passenger.setPassengerPayment(paymentSummary);
			}
		}

	}

	private static BigDecimal getTotalPaymentAmount(List<PNRGOVPaymentDTO> allPayments) throws ModuleException {

		BigDecimal totalPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (PNRGOVPaymentDTO payment : allPayments) {
			totalPaymentAmount = AccelAeroCalculator.add(totalPaymentAmount, payment.getPaymentAmount());
		}
		BigDecimal paymentAmount = totalPaymentAmount.negate();
		return paymentAmount;
	}

	private static PNRGOVPaymentDTO getPaymentTransaction(List<PNRGOVPaymentDTO> allPayments) {
		for (PNRGOVPaymentDTO payment : allPayments) {
			if (payment.getTnxType().equals(ReservationInternalConstants.TnxTypes.CREDIT)) {
				return payment;
			}
		}

		return null;
	}

	private static void populateAncillaryDetailsForSegments(List<PNRGOVPassengerDTO> passengers, List<PNRGOVSegmentDTO> segments) {
		Collection<Integer> pnrPaxIds = new HashSet<Integer>();
		Collection<Integer> pnrSegIds = new HashSet<Integer>();

		for (PNRGOVPassengerDTO passenger : passengers) {
			pnrPaxIds.add(passenger.getPnrPaxId());
		}

		for (PNRGOVSegmentDTO segment : segments) {
			pnrSegIds.add(segment.getPnrSegId());
		}

		Collection<PaxSeatTO> bookedSeats = ReservationDAOUtils.DAOInstance.SEAT_MAP_DAO
				.getReservationSeats(pnrPaxIds, pnrSegIds);

		// Collection<PaxMealTO> bookedMeals = ReservationDAOUtils.DAOInstance.MEAL_DAO.getReservationMeals(pnrPaxIds,
		// pnrSegIds);

		for (PNRGOVSegmentDTO segment : segments) {
			Integer pnrSegID = segment.getPnrSegId();
			Map<Integer, PaxSeatTO> bookedSeat = new HashMap<Integer, PaxSeatTO>();
			Collection<PaxSeatTO> segmentSeats = new HashSet<PaxSeatTO>();
			// Collection<PaxMealTO> segmentMeals = new HashSet<PaxMealTO>();
			for (PaxSeatTO seat : bookedSeats) {
				if (pnrSegID.equals(seat.getPnrSegId())) {
					segmentSeats.add(seat);
				}
			}
			for (PaxSeatTO seat : segmentSeats) {
				Integer pnrPaxId = seat.getPnrPaxId();
				int paxReference = getPassengerReference(pnrPaxId, passengers);
				if (paxReference != 0) {
					bookedSeat.put(paxReference, seat);
				}
			}
			if (bookedSeat != null && !bookedSeat.isEmpty()) {
				segment.setBookedSeat(bookedSeat);
			}
		}
	}

	private static int getPassengerReference(Integer pnrPaxId, List<PNRGOVPassengerDTO> passengers) {
		for (PNRGOVPassengerDTO passenger : passengers) {
			if (pnrPaxId.equals(passenger.getPnrPaxId())) {
				return passenger.getPaxReference();
			}
		}
		return 0;
	}

	private static String checkMandatoryFieldsExists(PNRGOVDTO pnrGovDto) {
		List<PNRGOVPassengerDTO> passengers = pnrGovDto.getPassengers();
		for (PNRGOVPassengerDTO passenger : passengers) {
			if ((passenger.getPassengerType().equals(PaxTypeTO.ADULT) || passenger.getPassengerType().equals(PaxTypeTO.CHILD))) {
				if (passenger.getLastName() == null && passenger.getLastName().equals("")) {
					return "Passenger Last Name Empty : " + pnrGovDto.getPnr() + " for pnr pax id : " + passenger.getPnrPaxId();
				}
				PNRGOVPaymentDTO passengerPayment = passenger.getPassengerPayment();
				if (passengerPayment.getFormOfPaymentIdentification() == null
						&& passengerPayment.getFormOfPaymentIdentification().equals("")) {
					return "Form of Payment is Empty for Passenger : " + passenger.getPnrPaxId() + " PNR : " + pnrGovDto.getPnr();
				}
				if (passengerPayment.getPaymentAmount() == null) {
					return "Payment Amount Empty for passenger : " + passenger.getPnrPaxId() + " PNR : " + pnrGovDto.getPnr();

				}
			}

			if (passenger.getPassengerEticket().size() == 0) {
				return "Passenger E ticket doesn't exists : " + passenger.getPnrPaxId() + " PNR : " + pnrGovDto.getPnr();
			}
		}
		return SUCCESS;
	}

	public static void logPnrGovMessageData(int fltSegId, Date transmissionTimeStamp, String countryCode, String airportCode,
			String inboundOutbound, int timePeriod, UserPrincipal userPrincipal, String transmissionStatus, String description)
			throws ModuleException {
		boolean isOutbound = false;
		if (inboundOutbound.equals("OUTBOUND")) {
			isOutbound = true;
		} else {
			isOutbound = false;
		}
		boolean hasAlreadyLogged = ReservationDAOUtils.DAOInstance.PNRGOV_DAO.hasPnrGovMessageSent(fltSegId, countryCode,
				airportCode, isOutbound, timePeriod);
		if (hasAlreadyLogged) {
			PNRGOVTxHistoryLog alreadyLoggedEntry = ReservationDAOUtils.DAOInstance.PNRGOV_DAO.getAlreadyLoggedEntry(fltSegId,
					countryCode, airportCode, inboundOutbound, timePeriod);
			if (alreadyLoggedEntry != null) {
				alreadyLoggedEntry.setTransmissionTimeStamp(transmissionTimeStamp);
				alreadyLoggedEntry.setUserId(userPrincipal.getUserId());
				alreadyLoggedEntry.setStatus(transmissionStatus);
				alreadyLoggedEntry.setDescription(description);
				ReservationDAOUtils.DAOInstance.PNRGOV_DAO.saveOrUpdatePnrGovMessageHistoryLog(alreadyLoggedEntry);
			}
		} else {
			PNRGOVTxHistoryLog txHistory = new PNRGOVTxHistoryLog();
			txHistory.setFltSegID(fltSegId);
			txHistory.setTransmissionTimeStamp(transmissionTimeStamp);
			txHistory.setCountryCode(countryCode);
			txHistory.setAirportCode(airportCode);
			txHistory.setInboundOutbound(inboundOutbound);
			txHistory.setTimePeriod(timePeriod);
			txHistory.setUserId(userPrincipal.getUserId());
			txHistory.setStatus(transmissionStatus);
			txHistory.setDescription(description);
			ReservationDAOUtils.DAOInstance.PNRGOV_DAO.saveOrUpdatePnrGovMessageHistoryLog(txHistory);
		}

	}

	public static void logPnrGovReservationData(PNRGOVDTO pnrGovDto, int fltSegId, String countryCode, String airportCode,
			String inboundOutbound, int timePeriod, String status, String description) {
		PNRGOVReservationLog log = new PNRGOVReservationLog();
		log.setFltSegId(fltSegId);
		log.setCountryCode(countryCode);
		log.setAirportCode(airportCode);
		log.setInboundOutbound(inboundOutbound);
		log.setTimePeriod(timePeriod);
		log.setDescription(description);
		log.setPnr(pnrGovDto.getPnr());
		log.setStatus(status);
		ReservationDAOUtils.DAOInstance.PNRGOV_DAO.saveOrUpdatePnrGovReservationLog(log);
	}

	public static void logPnrGovReservationDataLog(List<PNRGOVDTO> pnrGovDtoList, Date transmissionTimeStamp,
			int flightSegmentID, String countryCode, String airportCode, int timePeriod, String inboundOutbound,
			String transmissionStatus, String description) {
		List<PNRGOVReservationLog> resLogList = new ArrayList<PNRGOVReservationLog>();
		for (PNRGOVDTO dto : pnrGovDtoList) {
			PNRGOVReservationLog log = new PNRGOVReservationLog();
			log.setFltSegId(flightSegmentID);
			log.setCountryCode(countryCode);
			log.setAirportCode(airportCode);
			log.setInboundOutbound(inboundOutbound);
			log.setTimePeriod(timePeriod);
			log.setDescription(description);
			log.setPnr(dto.getReservationInfo().getPnr());
			log.setStatus(transmissionStatus);
			resLogList.add(log);
		}
		ReservationDAOUtils.DAOInstance.PNRGOV_DAO.saveOrUpdatePnrGovReservationLogList(resLogList);
	}
	
	private static List<PNRGOVSegmentDTO> removeGroundSegments(List<PNRGOVSegmentDTO> pnrSegmentDtoList) {
		List<PNRGOVSegmentDTO> groundSegmentLinks = new ArrayList<PNRGOVSegmentDTO>();
		List<PNRGOVSegmentDTO> groundSegmentList = new ArrayList<PNRGOVSegmentDTO>();
		for (PNRGOVSegmentDTO tmpSegment : pnrSegmentDtoList) {
			if (tmpSegment.getGroundSegId() != null && !tmpSegment.getGroundSegId().equals("")) {
				groundSegmentLinks.add(tmpSegment);
			}
		}

		for (PNRGOVSegmentDTO groundSegmentLink : groundSegmentLinks) {
			for (PNRGOVSegmentDTO segmentDetail : pnrSegmentDtoList) {
				if (groundSegmentLink.getGroundSegId().equals(segmentDetail.getPnrSegId().toString())) {
					groundSegmentList.add(segmentDetail);
				}
			}
		}
		
		for (PNRGOVSegmentDTO groundSegment : groundSegmentList) {
			pnrSegmentDtoList.remove(groundSegment);
		}

		return pnrSegmentDtoList;
	}
}
