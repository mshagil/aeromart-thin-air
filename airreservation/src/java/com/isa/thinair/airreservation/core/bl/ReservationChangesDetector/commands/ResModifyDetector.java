package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.commands;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ResModifyDtectorDataContext;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @isa.module.command name="resModifyDetector"
 * 
 */

public class ResModifyDetector extends DefaultBaseCommand {

	@Override
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse serviceResponce = new DefaultServiceResponse();

		if (AppSysParamsUtil.isEnablePalCalMessage()) {

			List<ResModifyDtectorDataContext> modifyDtectorDataContextList = resolveCommandParameters();
			for (ResModifyDtectorDataContext modifyDtectorDataContext : modifyDtectorDataContextList) {
				Command command = (Command) ReservationModuleUtils.getBean(CommandNames.RES_CHANGE_DETECTOR_MACRO);
				command.setParameter(CommandParamNames.RES_MODIFY_DETECT_DATA_CONTEXT, modifyDtectorDataContext);
				serviceResponce = (DefaultServiceResponse)command.execute();
			}

		} 
		
		serviceResponce.setResponseCode((String) this.getParameter(CommandParamNames.TRANSFER_SEAT_MSG));
		serviceResponce.addResponceParam(ResponseCodes.TRANSFER_SEAT_NUMBER, this.getParameter(ResponseCodes.TRANSFER_SEAT_NUMBER));

		return serviceResponce;
	}

	private List<ResModifyDtectorDataContext> resolveCommandParameters() throws ModuleException {

		List<ResModifyDtectorDataContext> dataContextList = new ArrayList<ResModifyDtectorDataContext>();
		if (this.getParameter(CommandParamNames.MODIFY_DETECTOR_RES_LIST_BEFORE_MODIFY) != null) {

			List<Reservation> existResList = (List) this.getParameter(CommandParamNames.MODIFY_DETECTOR_RES_LIST_BEFORE_MODIFY);
			for (Reservation reservation : existResList) {
				ResModifyDtectorDataContext dataContext = new ResModifyDtectorDataContext();
				dataContext.setExisitingReservation(reservation);
				dataContext.setUpdatedReservation(getUpdatedReservation(reservation.getPnr()));
				dataContextList.add(dataContext);
			}

		} else {
			Reservation existRes = (Reservation) this.getParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY);
			String pnr = (String) this.getParameter(CommandParamNames.PNR);
			if (pnr != null && existRes != null && !pnr.equals(existRes.getPnr())) {
				pnr = existRes.getPnr();
			}
			Reservation updated = getUpdatedReservation(pnr);

			ResModifyDtectorDataContext dataContext = new ResModifyDtectorDataContext();
			dataContext.setExisitingReservation(existRes);
			dataContext.setUpdatedReservation(updated);
			dataContextList.add(dataContext);
		}

		return dataContextList;
	}

	private Reservation getUpdatedReservation(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDto = new LCCClientPnrModesDTO();
		pnrModesDto.setPnr(pnr);
		pnrModesDto.setLoadFares(true);
		pnrModesDto.setLoadSegView(true);
		pnrModesDto.setLoadSSRInfo(true);

		return ReservationProxy.getReservation(pnrModesDto);

	}

}
