/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.util.List;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;

/**
 * @author udithad
 *
 */
public class WaitListInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	private ReservationDAO reservationDAO;

	public WaitListInformation() {
		reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
	}

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> passengerInforamtions) {
		for (PassengerInformation information : passengerInforamtions) {
			if (information.isWaitListedPassenger()) {
				ReservationWLPriority reservationWaitListPriority = getReservationWaitListPriorityBy(information
						.getPnrPaxId());
				if (reservationWaitListPriority != null
						&& reservationWaitListPriority.getPriority() != null) {
					information.setWaitListedInfo(reservationWaitListPriority
							.getPriority().toString());
				}
			}
		}
	}

	private ReservationWLPriority getReservationWaitListPriorityBy(
			Integer passengerId) {
		return this.reservationWaitListPriority.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformations) {
		for (PassengerInformation passengerInformation : passengerInformations) {
			if (passengerInformation.isWaitListedPassenger()) {
				ReservationWLPriority reservationWaitListPriority = reservationDAO
						.getWaitListedReservationPriority(
								passengerInformation.getPnr(),
								passengerInformation.getPnrSegId());
				this.reservationWaitListPriority.put(
						passengerInformation.getPnrPaxId(),
						reservationWaitListPriority);
			}
		}
	}

}
