/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.TransferSeatBcCount;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.AutoCancellationSchDTO;
import com.isa.thinair.airreservation.api.dto.EffectedFlightSegmentDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.FlightReservationSummaryDTO;
import com.isa.thinair.airreservation.api.dto.OpenReturnOndTO;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentTransferDTO;
import com.isa.thinair.airreservation.api.dto.pfs.PnrPaxFareSegWisePaxTO;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVSegmentDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotification;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.model.ReservationSegmentTransfer;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * ReservationSegmentDAO is the business DAO interface for the reservation service segment APIs
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface ReservationSegmentDAO {
	/**
	 * Returns a reservation segment
	 * 
	 * @param pnrSegId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public ReservationSegment getReservationSegment(int pnrSegId) throws CommonsDataAccessException;

	/**
	 * Saves reservation segments
	 * 
	 * @param segments
	 */
	public void saveReservationSegments(Collection<ReservationSegment> segments);

	/**
	 * Saves reservation segments
	 * 
	 * @param segments
	 */
	public void saveReservationSegment(ReservationSegment reservationSegment);

	/**
	 * Return reservation segments
	 * 
	 * @param pnr
	 * @return
	 */
	public Collection<ReservationSegment> getPnrSegments(String pnr);

	/**
	 * Return reservations
	 * 
	 * @param flightSegId
	 * @return
	 */
	public Collection<Reservation> getPnrsForSegment(int flightSegId);

	/**
	 * Return reservation segment all information
	 * 
	 * @param pnr
	 * @return
	 */
	public Collection<ReservationSegmentDTO> getPnrSegmentsView(String pnr);

	/**
	 * method to get BC codes with Availability
	 * 
	 * @return HashMap
	 */
	public HashMap<String, TransferSeatBcCount> getBCCodesWithAvailability(int pnrSegId, boolean considerCancelledPax);

	/**
	 * Returns the no.of confirmed and On-Hold PNRs for each segment of a flight
	 * 
	 * @param flightIds
	 * @return
	 */
	public Collection<FlightReservationSummaryDTO> getFlightReservationsSummary(Collection<Integer> flightIds);

	/**
	 * Get a FlightSegmentDTO passing the id.
	 * 
	 * @param flightSegmentId
	 * @return
	 */
	public FlightSegmentDTO getFlightSegment(int flightSegmentId);

	/**
	 * Update reservation alerts
	 * 
	 * @param alertFlag
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 */
	public void updateReservationAlerts(int alertFlag, Collection<Integer> flightSegmentIDs, Collection<Integer> pnrSegIDs,
			String status);

	/**
	 * Updates the ReservationSegments seatReprotectStatus flag.
	 * 
	 * @param seatReprotectStatus
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 * @param status
	 */
	public void updateReservationSegmentSeatReprotectStatus(int seatReprotectStatus, Collection<Integer> flightSegmentIDs,
			Collection<Integer> pnrSegIDs, String status);

	/**
	 * Updates the ReservationSegments mealReprotectStatus flag.
	 * 
	 * @param seatReprotectStatus
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 * @param status
	 */
	public void updateReservationSegmentMealReprotectStatus(int seatReprotectStatus, Collection<Integer> flightSegmentIDs,
			Collection<Integer> pnrSegIDs, String status);

	/**
	 * Return reservation segments
	 * 
	 * @param flightSegmentIDs
	 * @param pnrSegIDs
	 * @param status
	 * @param loadReservation
	 * @param loadFares
	 * @return
	 */
	public Collection<ReservationSegment> getReservationSegments(Collection<Integer> flightSegmentIDs,
			Collection<Integer> pnrSegIDs, String status, boolean loadReservation, boolean loadFares);

	/**
	 * Return confirmed reservation ONDs
	 * 
	 * @param colFlightSegIds
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Map<String, Map<Integer, EffectedFlightSegmentDTO>> getConfirmedPnrOnds(Collection<Integer> colFlightSegIds)
			throws CommonsDataAccessException;

	/**
	 * Update passenger fare segments
	 * 
	 * @param colPpfsId
	 * @param status
	 */
	public void updatePaxFareSegments(Collection<Integer> colPpfsId, String status);

	/**
	 * Return segment information
	 * 
	 * @param colPnrSegmentIds
	 * @return
	 */
	public Collection<ReservationSegmentDTO> getSegmentInformation(Collection<Integer> colPnrSegmentIds);

	/**
	 * Returns a collection of FlightSegmentDTO
	 * 
	 * @param flightSegId
	 * @param pnr TODO
	 * @param excludePrintExchangedStatus TODO
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<FlightReconcileDTO> getPnrSegmentsForReconcilation(Collection<Integer> flightSegId, String pnr, boolean excludePrintExchangedStatus) throws CommonsDataAccessException;

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Integer getFlightSegmentId(String flightNumber, Date departureDate, String fromSegmentCode, String toSegmentCode)
			throws CommonsDataAccessException;

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<Integer> getFlightSegmentIds(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws CommonsDataAccessException;
	
	
	public FlightReconcileDTO getFlightSegmentReconcileDTO(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws CommonsDataAccessException;
	/**
	 * Return reconcile PAX fare segments
	 * 
	 * @param pnrSegIds
	 * @return
	 */
	public Collection<PnrPaxFareSegWisePaxTO> getReconcilePaxFareSegments(Collection<Integer> pnrSegIds);

	/**
	 * Returns the reservation segments to Transfer
	 * 
	 * @param flightSegId
	 * @return
	 */
	public Collection<Reservation> getReservationSegmentsForTransfer(int flightSegId, boolean isInterline,
			Collection<String> colPnr);

	public Collection<Reservation> getReservationsForTransfer(int flightSegId, boolean isInterline, String logicalCabinClass,
			Collection<String> colPnr);

	/**
	 * Returns segment codes for flight segment ids
	 * 
	 * @param colOrderedFlgSegIds
	 * @return
	 */
	public Map<Integer, String> getSegmentCodes(Collection<Integer> colFlgSegIds);

	/**
	 * Return reservation segment information
	 * 
	 * @param colFlightLegends
	 * @param executionDate
	 * @param startOverMins
	 * @param cutOverMins
	 * @param colSegStates
	 * @param colStates
	 * @return
	 */
	public Map<String, Collection<ReservationSegmentDTO>>
			getSegmentInformation(Collection<String> colFlightLegends, Date executionDate, int startOverMins, int cutOverMins,
					Collection<String> colSegStates, Collection<String> colStates);

	/**
	 * Returns the reservation segment transfer object by the pnr segment id
	 * 
	 * @param pnrSegId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public ReservationSegmentTransfer getReservationSegmentTransferByPnrSegId(int pnrSegId) throws CommonsDataAccessException;

	/**
	 * Saves the reservation segment transfers information
	 * 
	 * @param colReservationSegmentTransfer
	 */
	public void saveReservationSegmentsTransfer(Collection<ReservationSegmentTransfer> colReservationSegmentTransfer);

	/**
	 * Returns the transfer segments
	 * 
	 * @param colPnrSegIds
	 * @return
	 */
	public Map<Integer, ReservationSegmentTransfer> getReservationTransferSegments(Collection<Integer> colPnrSegIds);

	/**
	 * Returns the reservation transfer segments
	 * 
	 * @param states
	 * @param executionDate
	 * @return
	 */
	public Collection<ReservationSegmentTransferDTO>
			getReservationTransferSegments(Collection<String> states, Date executionDate);

	/**
	 * Returns Open Return Segments
	 * 
	 * @param executionDate
	 * @return
	 */
	public Collection<OpenReturnOndTO> getOpenReturnSegments(Date executionDate);

	/**
	 * Returns external segment information for the given pnr information
	 * 
	 * @param pnrs
	 * @return
	 */
	public Map<String, Collection<ReservationExternalSegmentTO>> getExternalSegmentInformation(Collection<String> pnrs);

	public List<ExternalPnrSegment> getExternalSegmentInformationByPnr(String pnr);

	/**
	 * save external flight segment. Note. This DTO can be included in the reservation module as external flight
	 * segments has nothing to do with flight schedules
	 * 
	 * @param externalFlightSegment
	 */
	public ExternalFlightSegment saveExternalFlightSegment(ExternalFlightSegment externalFlightSegment);

	/**
	 * Retrieve external flight segment for a given carrier and externalFlightSegRef
	 * 
	 * @param carrierCode
	 * @param extFlightSegRef
	 * @return
	 */
	public ExternalFlightSegment getExternalFlightSegment(String carrierCode, Integer extFlightSegRef);

	/**
	 * Retrieve external flight segment for a given id
	 * 
	 * @param externalFlightSegId
	 * @return
	 */
	public ExternalFlightSegment getExternalFlightSegment(Integer externalFlightSegId);

	public void updatePaxFareSegmentsForCheckin(Collection<Integer> colPpfsId);

	public void updateStatusReservationSegmentNotifyEvent(ReservationSegmentNotificationEvent notificationEvent);

	public List<ResSegmentEticketInfoDTO> getEticketInfo(List<String> lccUniqueIDs, List<Integer> txnIDs);

	/**
	 * Returns the correct flight segment id
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Integer getFlightSegmentIdBySegmentCode(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode, String segmentCode) throws CommonsDataAccessException;

	public void updateBaggageOndGrpIds(Map<Integer, String> pnrSegIdWiseOndBaggageGrpId);

	public void saveReservationSegmentNotifications(List<ReservationSegmentNotification> notifications);
	
	public Collection<Integer> getFlightSegmentIds(String pnr, boolean isCNFOnly) throws CommonsDataAccessException;

	/**
	 * Return own reservation expired segment(s) information based on auto cancellation flag
	 * 
	 * @param cancellationIds
	 * @param marketingCarrier
	 * @return
	 */
	public List<AutoCancellationSchDTO> getExpiredSegments(Collection<Integer> cancellationIds, String marketingCarrier);

	/**
	 * Returns marketing perspective lcc reservation(s) expired segments based on auto cancellation flag
	 * 
	 * @param marketingCarrier
	 * @param cancellationIds
	 * 
	 * @return
	 */
	public Map<String, Integer> getExpiredLccSegmentsInfo(String marketingCarrier, Collection<Integer> cancellationIds);

	/**
	 * Return existing modification charges
	 * 
	 * @param pnrSegIds
	 * @return
	 */
	public List<ReservationPaxDetailsDTO> getExistingModificationChargesDue(List<Integer> pnrSegIds);
	
	public List<PNRGOVSegmentDTO> getReservationSegmentsForPNR(String pnr , String carrierCode);
	
	public List<Integer> getAffectedPnrs(Collection<Integer> flightSegmentIDs, String status);

	/**
	 * Returns respective parent pnr id for a given parent_ppid of a infant
	 * 
	 * @param parentPPID
	 * 
	 * @return
	 */
	public String getParentPnr(Integer parentPPID) throws CommonsDataAccessException;

	public Map<String, Integer> getSegmentBundledFarePeriodIds(String pnr) throws CommonsDataAccessException;
	
	public Collection<Integer> getConnectedGroundSegmentPPFSID(Collection<Integer> colPpfsId) throws CommonsDataAccessException;

	public Integer getReservationSegmentId(String pnr, String departureAirport, String arrivalAirport) throws  CommonsDataAccessException;
	
	public Integer getFlightSegmentIdForSegmentTransfer(String flightNumber, Date departureDate, String fromSegmentCode,
			String toSegmentCode) throws CommonsDataAccessException;
	
	public Reservation loadReservationByPnrPaxId(Integer pnrPaxId) throws CommonsDataAccessException;
}