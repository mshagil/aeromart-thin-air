package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.AirportMessagePassenger;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.actionTemplate.base.BaseObseverActionTemplate;
import com.isa.thinair.airreservation.core.bl.ReservationChangesDetector.dataContext.ObserverActionDataContext;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ChangeAirportMessagePaxTemplate extends BaseObseverActionTemplate<ObserverActionDataContext> {

	public ChangeAirportMessagePaxTemplate() {
		super();
	}

	@Override
	public void executeTemplateAction(ObserverActionDataContext dataContext) throws ModuleException {

		Map<Integer, Set<Integer>> chgMap = dataContext.getChgMap();

		setRemoveAllSSROnUpdate(dataContext.getRemoveAllSSROnUpdate());
		// del MAP remove with existing data
		checkForExist(chgMap, PNLConstants.AdlActions.C, dataContext.getNewReservation().getPnr());

		List<AirportMessagePassenger> paxList = createUnexistPax(chgMap, dataContext.getNewReservation(),
				PNLConstants.AdlActions.C.toString());

		paxList.addAll(alreadyExistpaxListToUpdate);
		if (!paxList.isEmpty()) {
			auxilliaryDAO.saveAirportMessagePassenger(paxList);
		}
	}

}
