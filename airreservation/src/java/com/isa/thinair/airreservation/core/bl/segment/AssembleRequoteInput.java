package com.isa.thinair.airreservation.core.bl.segment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.RevenueDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.AdjustFareSurchargeTaxBO;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to compose the requote related parameters
 * 
 * @author Dilan
 * 
 * @isa.module.command name="assembleRequoteInput"
 */
public class AssembleRequoteInput extends DefaultBaseCommand {
	private static Log log = LogFactory.getLog(AssembleRequoteInput.class);

	@Override
	public ServiceResponce execute() throws ModuleException {
		log.debug("enter AssembleRequoteInput");
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		RequoteModifyRQ requoteModifyRQ = getParameter(CommandParamNames.REQUOTE_MODIFY_RQ, RequoteModifyRQ.class);
		CredentialsDTO credentialsDTO = getParameter(CommandParamNames.CREDENTIALS_DTO, CredentialsDTO.class);
		TrackInfoDTO trackInfoDTO = getParameter(CommandParamNames.RESERVATIN_TRACINFO, TrackInfoDTO.class);
		AutoCancellationInfo autoCancellationInfo = getParameter(CommandParamNames.AUTO_CANCELLATION_INFO,
				AutoCancellationInfo.class);

		Reservation prevReservation = getParameter(CommandParamNames.MODIFY_DETECTOR_RES_BEFORE_MODIFY, Reservation.class);
		RequoteAssembler asm = new RequoteAssembler(requoteModifyRQ, credentialsDTO);
		asm.setTrackInfo(trackInfoDTO);

		// Adjust Tax value applied
		AdjustFareSurchargeTaxBO adjustFareSurchargeTaxBO = new AdjustFareSurchargeTaxBO(asm.getOndFares(),
				requoteModifyRQ.getPnr(), asm.getReservation(), asm.isUserModification(), asm.getCnxPnrSegmentIds());
		adjustFareSurchargeTaxBO.adjustFareSurchargeTax(ChargeCodes.JN_FARE_SURCHARGE);

		response.addResponceParam(CommandParamNames.PNR, requoteModifyRQ.getPnr());
		response.addResponceParam(CommandParamNames.PNR_SEGMENT_IDS, asm.getCnxPnrSegmentIds());
		response.addResponceParam(CommandParamNames.EXCHANGE_PNR_SEGMENT_IDS, asm.getExchangedPnrSegIds());
		response.addResponceParam(CommandParamNames.FLOWN_PNR_SEGMENT_IDS, asm.getFlownPnrSegIds());
		response.addResponceParam(CommandParamNames.REQUOTE_MODIFY_RQ, true);
		response.addResponceParam(CommandParamNames.FLOWN_ASSIT_UNIT, asm.getFlownAssitUnit());
		response.addResponceParam(CommandParamNames.UPDATE_FLOWN_VALIDITY, asm.updateFlownTicketValidity());
		response.addResponceParam(CommandParamNames.TICKET_VALID_TILL_DATE, asm.getTicketValidTill(asm.isOpenReturn()));
		response.addResponceParam(CommandParamNames.TICKET_VALID_FLT_SEG_IDS, asm.getValidityApplicableFltSegIds());

		response.addResponceParam(CommandParamNames.IS_CANCEL_CHG_OPERATION, asm.isUserCancellation());
		response.addResponceParam(CommandParamNames.IS_MODIFY_CHG_OPERATION, asm.isUserModification());

		if (requoteModifyRQ.isApplyModificationCharge()) {
			response.addResponceParam(CommandParamNames.APPLY_CANCEL_CHARGE, false);
			response.addResponceParam(CommandParamNames.APPLY_MODIFY_CHARGE, true);
		} else {
			response.addResponceParam(CommandParamNames.APPLY_CANCEL_CHARGE, asm.isUserCancellation());
			response.addResponceParam(CommandParamNames.APPLY_MODIFY_CHARGE, asm.applyModificationCharge(prevReservation));
		}

		response.addResponceParam(CommandParamNames.IS_MODIFY_OND_OPERATION,
				asm.isUserModification() || asm.isSameFlightModification());
		response.addResponceParam(CommandParamNames.IS_CANCEL_OND_OPERATION, asm.isUserCancellation());
		response.addResponceParam(CommandParamNames.IS_ADD_OND_OPERATION, asm.isAddOndModification());

		response.addResponceParam(CommandParamNames.CUSTOM_CHARGES, asm.getCustomCharges());
		response.addResponceParam(CommandParamNames.IS_PAX_CANCEL, new Boolean(true));
		response.addResponceParam(CommandParamNames.PAYMENT_INFO,
				ReservationBO.getPaymentInfo(requoteModifyRQ.getAllPaymentAssemblers(), requoteModifyRQ.getPnr()));
		response.addResponceParam(CommandParamNames.VERSION, new Long(requoteModifyRQ.getVersion()));
		response.addResponceParam(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
		response.addResponceParam(CommandParamNames.OND_FARE_DTOS, asm.getOndFares());
		response.addResponceParam(CommandParamNames.FARE_DISCOUNT_DTO, asm.getFareDiscountInfo());
		response.addResponceParam(CommandParamNames.FARE_DISCOUNT_INFO, asm.getFareDiscountDTOInfo());
		response.addResponceParam(CommandParamNames.FARE_DISCOUNT_CODE, asm.getFareDiscountCode());
		response.addResponceParam(CommandParamNames.USER_NOTES, asm.getUserNotes());
		response.addResponceParam(CommandParamNames.USER_NOTE_TYPE, asm.getUserNoteType());
		response.addResponceParam(CommandParamNames.ENDORSEMENT_NOTES, asm.getEndorsementNotes());
		response.addResponceParam(CommandParamNames.SEG_OBJECTS_MAP, asm.getSegmentObjectsMap());
		response.addResponceParam(CommandParamNames.RESERVATION_SEGMENTS, asm.getReservationSegments());
		response.addResponceParam(CommandParamNames.RESERVATION_EXTERNAL_SEGMENTS, asm.getExternalSegments());
		updateSegmentSequenceForPaxSSRs(asm);
		response.addResponceParam(CommandParamNames.PNR_PAX_IDS_AND_PAYMENTS, asm.getPassengerPaymentsMap());
		response.addResponceParam(CommandParamNames.IS_CNX_FLOWN_SEGS, new Boolean(true));
		response.addResponceParam(CommandParamNames.RESERVATION_CONTACT_INFO, asm.getReservation().getContactInfo());
		response.addResponceParam(CommandParamNames.TRIGGER_BLOCK_SEATS, asm.isTriggerBlockSeats());
		response.addResponceParam(CommandParamNames.BLOCK_KEY_IDS, asm.getBlockSeats());
		response.addResponceParam(CommandParamNames.TRIGGER_PNR_FORCE_CONFIRMED,
				Boolean.valueOf(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED
						.equals(requoteModifyRQ.getPaymentType())));
		response.addResponceParam(CommandParamNames.PNR_PAYMENT_TYPES, requoteModifyRQ.getPaymentType());
		response.addResponceParam(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP, requoteModifyRQ.getReleaseTimeStampZulu());
		response.addResponceParam(CommandParamNames.ORDERED_NEW_FLIGHT_SEGMENT_IDS, asm.getNewFlightSegmentIds());
		response.addResponceParam(CommandParamNames.LAST_MODIFICATION_TIMESTAMP, requoteModifyRQ.getLastModificationTimestamp());
		response.addResponceParam(CommandParamNames.LAST_CURRENCY_CODE, requoteModifyRQ.getLastCurrencyCode());
		response.addResponceParam(CommandParamNames.IS_FLEXI_SELECTED, requoteModifyRQ.isFlexiExists());
		response.addResponceParam(CommandParamNames.OTHER_CARRIER_CREDIT_USED, false);
		response.addResponceParam(CommandParamNames.INSURANCE_INFO, requoteModifyRQ.getInsurances());
		response.addResponceParam(CommandParamNames.IS_CAPTURE_PAYMENT, Boolean.TRUE);
		response.addResponceParam(CommandParamNames.HAS_EXT_CARRIER_PAYMENTS, Boolean.valueOf(false));
		response.addResponceParam(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY, asm.isEnableTransactionGranularity());
		response.addResponceParam(CommandParamNames.IS_OPEN_RETURN_CONF_SEG_MODIFICATION, false); // FIXME
		response.addResponceParam(CommandParamNames.PASSENGER_REVENUE_MAP, new HashMap<Integer, RevenueDTO>());

		if (requoteModifyRQ.getNameChangedPaxMap() != null && requoteModifyRQ.getNameChangedPaxMap().size() > 0) {
			response.addResponceParam(CommandParamNames.NAME_CHANGE_PAX_MAP, requoteModifyRQ.getNameChangedPaxMap());
			response.addResponceParam(CommandParamNames.IS_FROM_NAME_CHANGE, true);
			response.addResponceParam(CommandParamNames.IS_TBA_NAME_CHANGE, false);
			response.addResponceParam(CommandParamNames.CHECK_DUPLICATE_NAMES, requoteModifyRQ.isDuplicateNameCheck());
			response.addResponceParam(CommandParamNames.IS_DUMMY_RESERVATION, false);
			response.addResponceParam(CommandParamNames.APPLY_NAME_CHANGE_CHARGE, false);
			response.addResponceParam(CommandParamNames.IS_MODIFY_OND_OPERATION, true);
		} else {
			response.addResponceParam(CommandParamNames.IS_FROM_NAME_CHANGE, false);
		}

		response.addResponceParam(CommandParamNames.CC_FRAUD_ENABLE,
				AppSysParamsUtil.isFraudCheckEnabled(credentialsDTO.getSalesChannelCode()));
		response.addResponceParam(CommandParamNames.RESERVATIN_TRACINFO, trackInfoDTO);
		response.addResponceParam(CommandParamNames.RESERVATION, asm.getReservation());
		response.addResponceParam(CommandParamNames.RESERVATION_TYPE, "UPDATE_RESERVATION");
		response.addResponceParam(CommandParamNames.IS_REQUOTE, true);
		response.addResponceParam(CommandParamNames.BC_NON_CHANGED_FLT_SEG_IDS, asm.getBCNonChangedExchangedFlightSegIds());
		response.addResponceParam(CommandParamNames.REQUOTE_SEGMENT_MAP, requoteModifyRQ.getRequoteSegmentMap());
		response.addResponceParam(CommandParamNames.IS_ACTUAL_PAYMENT, requoteModifyRQ.isActualPayment());
		response.addResponceParam(CommandParamNames.IS_NO_BALANCE_TO_PAY, requoteModifyRQ.isNoBalanceToPay());
		response.addResponceParam(CommandParamNames.PAX_TAXING_EXT_CHARGE_MAP, requoteModifyRQ.getPaxExtChgMap());
		response.addResponceParam(CommandParamNames.GROUND_SEG_BY_AIR_SEG_ID_MAP, requoteModifyRQ.getGroundFltSegByFltSegId());
		response.addResponceParam(CommandParamNames.PAX_ADJUSTMENT_AMOUNT_MAP, requoteModifyRQ.getPaxWiseAdjustmentAmountMap());
		response.addResponceParam(CommandParamNames.REASON_FOR_ALLOW_BLACKLISTED_PAX, requoteModifyRQ.getReasonForAllowBlPax());
		
		if (asm.isUserCancellation() || requoteModifyRQ.isNoBalanceToPay()) {
			// should pass only handling fee charges
			response.addResponceParam(CommandParamNames.PAX_EXTERNAL_CHARGE_MAP,
					convertExternalChgMap(requoteModifyRQ.getPaxExtChgMap()));
		}
		LoyaltyPaymentInfo loyaltyPaymentInfo = requoteModifyRQ.getDefaultCarrierLoyaltyPaymentInfo();

		if (loyaltyPaymentInfo != null) {
			Map<Integer, Map<String, BigDecimal>> lmsPaxProductRedeemedAmount = loyaltyPaymentInfo
					.getPaxProductPaymentBreakdown();

			if (lmsPaxProductRedeemedAmount != null && lmsPaxProductRedeemedAmount.size() > 0) {
				Map<Integer, Map<String, BigDecimal>> lmsPnrPaxIdRedeemedAmounts = new HashMap<Integer, Map<String, BigDecimal>>();
				for (ReservationPax reservationPax : asm.getReservation().getPassengers()) {
					if (lmsPaxProductRedeemedAmount.containsKey(reservationPax.getPaxSequence())) {
						lmsPnrPaxIdRedeemedAmounts.put(reservationPax.getPnrPaxId(),
								lmsPaxProductRedeemedAmount.get(reservationPax.getPaxSequence()));
					}
				}
				response.addResponceParam(CommandParamNames.LOYALTY_PAX_PRODUCT_REDEEMED_AMOUNTS, lmsPnrPaxIdRedeemedAmounts);
			}
		}

		if (autoCancellationInfo == null && requoteModifyRQ.isAutoCancellationEnabled()) {
			autoCancellationInfo = getAutoCancellationInfo(asm, requoteModifyRQ);
			response.addResponceParam(CommandParamNames.AUTO_CANCELLATION_INFO, autoCancellationInfo);
		}
		if (requoteModifyRQ.getFareInfo() != null && requoteModifyRQ.getFareInfo().getBookingType() != null) {
			response.addResponceParam(CommandParamNames.BOOKING_TYPE, requoteModifyRQ.getFareInfo().getBookingType());
		}

		log.debug("exit AssembleRequoteInput");
		return response;
	}

	private void updateSegmentSequenceForPaxSSRs(RequoteAssembler asm) throws ModuleException {
		Collection<ReservationSegment> colResSeg = asm.getReservationSegments();
		Map<Integer, IPayment> paxPaymentMap = asm.getPassengerPaymentsMap();
		if (paxPaymentMap != null) {
			for (Entry<Integer, IPayment> paymentEntry : paxPaymentMap.entrySet()) {
				PaymentAssembler paymentAssembler = (PaymentAssembler) paymentEntry.getValue();
				if (paymentAssembler != null) {
					Collection<ExternalChgDTO> lstAirportServiceChg = paymentAssembler
							.getPerPaxExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
					Collection<ExternalChgDTO> lstInflightServiceChg = paymentAssembler
							.getPerPaxExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
					updateSegmentSequence(colResSeg, lstAirportServiceChg);
					updateSegmentSequence(colResSeg, lstInflightServiceChg);
				}
			}
		}
	}

	private void updateSegmentSequence(Collection<ReservationSegment> colResSeg, Collection<ExternalChgDTO> extChgList) {
		if (extChgList != null && colResSeg != null) {
			for (ExternalChgDTO externalChgDTO : extChgList) {
				SSRExternalChargeDTO ssrExtDTO = (SSRExternalChargeDTO) externalChgDTO;
				int flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrExtDTO.getFlightRPH());
				int segmentSeq = 0;
				for (ReservationSegment reservationSegment : colResSeg) {
					if (reservationSegment.getFlightSegId() == flightSegId) {
						segmentSeq = reservationSegment.getSegmentSeq();
						break;
					}
				}
				ssrExtDTO.setSegmentSequece(segmentSeq);
			}
		}
	}

	private AutoCancellationInfo getAutoCancellationInfo(RequoteAssembler asm, RequoteModifyRQ requoteModifyRQ)
			throws ModuleException {
		AutoCancellationInfo autoCnxInfo = null;
		if ((asm.isSameFlightModification() || asm.isUserModification() || asm.isAddOndModification())) {
			Collection<ReservationSegment> reservationSegments = asm.getReservationSegments();
			Reservation reservation = asm.getReservation();

			// Track modification flow
			Map<String, String> requoteSegMap = requoteModifyRQ.getRequoteSegmentMap();
			Collection<Integer> cancelledPnrSegIds = asm.getCnxPnrSegmentIds();
			Set<Integer> effectveFltSegId = new HashSet<Integer>();
			Collection<Integer> modifiedSegId = new ArrayList<Integer>();

			if (requoteSegMap != null && !requoteSegMap.isEmpty()) {
				Collection<Integer> exchangedPnrSegId = asm.getExchangedPnrSegIds();
				for (ReservationSegment reservationSegment : reservationSegments) {
					effectveFltSegId.add(reservationSegment.getFlightSegId());
					String resFltSegId = reservationSegment.getFlightSegId().toString();
					if (requoteSegMap.containsKey(resFltSegId) && requoteSegMap.get(resFltSegId) != null) {
						int flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(requoteSegMap.get(resFltSegId));
						if (exchangedPnrSegId == null || !exchangedPnrSegId.contains(flightSegId)) {
							reservationSegment.setModifiedFrom(flightSegId);
							modifiedSegId.add(flightSegId);
						}
					}
				}
			}

			if (cancelledPnrSegIds != null && !cancelledPnrSegIds.isEmpty()) {
				modifiedSegId.addAll(cancelledPnrSegIds);
			}

			Date firstDepatureFlightDateTimeInZulu = null;
			FlightSegmentDTO newFirstFltSeg = null;
			Collection<FlightSegmentDTO> fltSegDtos = ReservationModuleUtils.getFlightBD().getFlightSegments(effectveFltSegId);
			for (FlightSegmentDTO flightSegmentDTO : fltSegDtos) {
				if (newFirstFltSeg == null) {
					newFirstFltSeg = flightSegmentDTO;
				} else if (newFirstFltSeg.getDepartureDateTimeZulu().after(flightSegmentDTO.getDepartureDateTimeZulu())) {
					newFirstFltSeg = flightSegmentDTO;
				}
			}

			if (newFirstFltSeg != null) {
				firstDepatureFlightDateTimeInZulu = newFirstFltSeg.getDepartureDateTimeZulu();
			}

			if (requoteModifyRQ.getInsurances() != null && !requoteModifyRQ.getInsurances().isEmpty()) {
				// Consider existing non cancelled segments in calculating auto expire time
				Collection<ReservationSegmentDTO> existingResSegDTOs = reservation.getSegmentsView();
				for (ReservationSegmentDTO reservationSegmentDTO : existingResSegDTOs) {
					if (cancelledPnrSegIds == null || !cancelledPnrSegIds.contains(reservationSegmentDTO.getPnrSegId())) {
						if (firstDepatureFlightDateTimeInZulu == null) {
							firstDepatureFlightDateTimeInZulu = reservationSegmentDTO.getZuluDepartureDate();
						} else if (firstDepatureFlightDateTimeInZulu.after(reservationSegmentDTO.getZuluDepartureDate())) {
							firstDepatureFlightDateTimeInZulu = reservationSegmentDTO.getZuluDepartureDate();
						}
					}
				}
			}

			autoCnxInfo = ReservationModuleUtils.getReservationBD().getAutoCancellationInfo(reservation.getPnr(), reservation,
					firstDepatureFlightDateTimeInZulu, modifiedSegId, AutoCancellationInfo.AUTO_CNX_TYPE.SEG_EXP.toString(),
					requoteModifyRQ.isHasBufferTimeAutoCnxPrivilege(), false);

		}
		return autoCnxInfo;
	}
	
	private Map<Integer, List<ExternalChgDTO>> convertExternalChgMap(Map<Integer, List<LCCClientExternalChgDTO>> lccExtChgMap) {
		Map<Integer, List<ExternalChgDTO>> extChgMap = new HashMap<Integer, List<ExternalChgDTO>>();
		for (Entry<Integer, List<LCCClientExternalChgDTO>> extChgEntry : lccExtChgMap.entrySet()) {
			List<LCCClientExternalChgDTO> lccExtChgList = extChgEntry.getValue();
			if (lccExtChgList != null && lccExtChgList.size() > 0) {
				extChgMap.put(extChgEntry.getKey(), ReservationApiUtils.populateExternalCharges(lccExtChgList));
			}

		}
		return extChgMap;
	}
}
