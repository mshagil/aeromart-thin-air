/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.revacc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.aircustomer.api.service.LoyaltyCreditServiceBD;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.revacc.ReservationPaxPaymentMetaTO;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.ITnxPayment;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationCredit;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.model.TnxAgentPayment;
import com.isa.thinair.airreservation.api.model.TnxCardPayment;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.TnxLoyaltyPayment;
import com.isa.thinair.airreservation.api.model.TnxPayment;
import com.isa.thinair.airreservation.api.model.TnxVoucherPayment;
import com.isa.thinair.airreservation.api.model.TransactionSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.revacc.breakdown.TnxGranularityFactory;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationCreditDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationPaymentDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airreservation.core.utils.CommandNames;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard.FieldName;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.util.CreditCardUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.service.VoucherBD;

/**
 * commanad to adjust record payment
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="recordPayment"
 */
public class RecordPayment extends DefaultBaseCommand {
	// Dao's
	private ReservationCreditDAO reservationCreditDao;

	private ReservationTnxDAO reservationTnxDao;

	private ReservationPaymentDAO reservationPaymentDAO;

	private LoyaltyCreditServiceBD loyaltyCreditServiceBD;

	/**
	 * constructor of the RecordPayment command
	 */
	public RecordPayment() {

		// looking up daos
		reservationCreditDao = ReservationDAOUtils.DAOInstance.RESERVATION_CREDIT_DAO;
		reservationTnxDao = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
		reservationPaymentDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAYMENT_DAO;
		loyaltyCreditServiceBD = ReservationModuleUtils.getLoyaltyCreditBD();
	}

	/**
	 * execute method of the RecordPayment command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// getting command params
		String pnrPaxId = (String) this.getParameter(CommandParamNames.PNR_PAX_ID);
		Collection paymentsList = (Collection) this.getParameter(CommandParamNames.PAYMENT_LIST);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		String recieptNumber = (String) this.getParameter(CommandParamNames.RECIEPT_NUMBER);
		ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO = (ReservationPaxPaymentMetaTO) this
				.getParameter(CommandParamNames.RESERVATION_PAX_PAYMENT_META_TO);
		List<CardDetailConfigDTO> cardConfigData = (List<CardDetailConfigDTO>) this
				.getParameter(CommandParamNames.PAYMENT_CARD_CONFIG_DATA);
		boolean enableTransactionGranularity = (Boolean) this.getParameter(CommandParamNames.ENABLE_TRANSACTION_GRANULARITY);
		boolean isGoShowProcess = this.getParameter(CommandParamNames.IS_GOSHOW_PROCESS, Boolean.FALSE, Boolean.class);
		boolean isActualPayment = this.getParameter(CommandParamNames.IS_ACTUAL_PAYMENT, Boolean.FALSE, Boolean.class);
		boolean isFirstPayment = this.getParameter(CommandParamNames.IS_FIRST_PAYMENT, Boolean.FALSE, Boolean.class);
		Integer transactionSeq = (Integer) this.getParameter(CommandParamNames.TRANSACTION_SEQ);
		Map<String, BigDecimal> lmsProductRedeemedAmount = (Map<String, BigDecimal>) this
				.getParameter(CommandParamNames.LOYALTY_PRODUCT_REDEEMED_AMOUNTS);
		List<TransactionSegment> transactionSegments = (List<TransactionSegment>) this
				.getParameter(CommandParamNames.TRNX_SEGMENTS);

		// check params
		this.checkParams(pnrPaxId, paymentsList, credentialsDTO);

		Date currentTimestamp = (Date) this.getParameter(CommandParamNames.CURRENT_TIMESTAMP);
		// Have this till you handle all the paths this command can be called is covered
		if (currentTimestamp == null) {
			currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		}

		// Record LMS payment initially
		if (lmsProductRedeemedAmount != null && lmsProductRedeemedAmount.size() > 0) {
			Iterator itLmsPayments = paymentsList.iterator();
			while (itLmsPayments.hasNext()) {
				ITnxPayment payment = (ITnxPayment) itLmsPayments.next();
				if (payment.getPaymentType() == PaymentType.LOYALTY_PAYMENT.getTypeValue()) {
					ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
							ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode(), credentialsDTO, currentTimestamp, null,
							payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);
					ReservationPaxOndCharge tmpReservationPaxOndCharge = reservationPaxPaymentMetaTO
							.getColPerPaxWiseOndNewCharges().iterator().next();
					BigDecimal maxLMSPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (tmpReservationPaxOndCharge.getReservationPaxFare() != null) {
						maxLMSPayAmount = AccelAeroCalculator.add(
								tmpReservationPaxOndCharge.getReservationPaxFare().getTotalFare(),
								tmpReservationPaxOndCharge.getReservationPaxFare().getTotalSurCharge());
					} else {
						maxLMSPayAmount = tmpReservationPaxOndCharge.getAmount();
					}
					if (AccelAeroCalculator.subtract(maxLMSPayAmount, payment.getAmount()).abs()
							.compareTo(AccelAeroRounderPolicy.LMS_THREASHOLD_AMOUNT) <= 0) {
						creditTnx.setAmount(maxLMSPayAmount.negate());
					}
					TnxLoyaltyPayment loyaltyPayment = (TnxLoyaltyPayment) payment;
					creditTnx.setExternalReference(loyaltyPayment.getLoyaltyMemberAccountId());
					creditTnx.setRemarks("LMS Reward IDs " + Arrays.toString(loyaltyPayment.getRewardIDs()));
					this.saveLMSTnxAndCredit(creditTnx, pnrPaxId, loyaltyPayment, reservationPaxPaymentMetaTO, credentialsDTO,
							null, enableTransactionGranularity, transactionSeq, lmsProductRedeemedAmount);

				}
			}
		}

		Iterator itPayments = paymentsList.iterator();
		while (itPayments.hasNext()) {

			ITnxPayment payment = (ITnxPayment) itPayments.next();

			if (payment.getPaymentType() == PaymentType.CASH.getTypeValue()) {

				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CASH_PAYMENT.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, isActualPayment);
				TnxPayment cashPayment = (TnxPayment) payment;
				setPayReference(cashPayment.getPaymentReferanceTO(), creditTnx);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);

			} else if (payment.getPaymentType() == PaymentType.CARD_DINERS.getTypeValue()) {

				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_DINERS.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;

				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);

			} else if (payment.getPaymentType() == PaymentType.CARD_MASTER.getTypeValue()) {
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_MASTER.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);

				TnxCardPayment cardPayment = (TnxCardPayment) payment;

				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);

				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);

			} else if (payment.getPaymentType() == PaymentType.CARD_VISA.getTypeValue()) {

				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_VISA.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;
				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);
			} else if (payment.getPaymentType() == PaymentType.CARD_AMEX.getTypeValue()) {

				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_AMEX.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;
				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);
			} else if (payment.getPaymentType() == PaymentType.CARD_GENERIC.getTypeValue()) {

				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_GENERIC.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;
				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);
			} else if (payment.getPaymentType() == PaymentType.CARD_CMI.getTypeValue()) {

				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_CMI.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;
				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);
			} else if (payment.getPaymentType() == PaymentType.ON_ACCOUNT.getTypeValue()) {
				TnxAgentPayment agentPayment = (TnxAgentPayment) payment;
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.ONACCOUNT_PAYMENT.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), isGoShowProcess, true);
				creditTnx.setAgentCode(BeanUtils.nullHandler(agentPayment.getAgentCode()));

				String creditSharedAgent = ReservationModuleUtils.getTravelAgentBD().getCreditSharingAgentCode(
						creditTnx.getAgentCode());
				if (!creditTnx.getAgentCode().equals(creditSharedAgent)) {
					creditTnx.setCreditSharedAgentCode(creditSharedAgent);
				}

				if (agentPayment.getPaymentReferenceTO() != null) {
					String paymentRef = BeanUtils.nullHandler(agentPayment.getPaymentReferenceTO().getPaymentRef());
					Integer actPaymentMode = (agentPayment.getPaymentReferenceTO().getActualPaymentMode() != null
							&& agentPayment.getPaymentReferenceTO().getActualPaymentMode() != 0
									? agentPayment.getPaymentReferenceTO().getActualPaymentMode()
									: null);
					creditTnx.setExternalReference(paymentRef);
					creditTnx.setExternalReferenceType(actPaymentMode);

					if (agentPayment.getPaymentReferenceTO().getPaymentRefType() == PAYMENT_REF_TYPE.LOYALTY) {
						loyaltyCreditServiceBD.consumeLoyaltyCreditFromCustomer(agentPayment.getAmount(),
								agentPayment.getPaymentReferenceTO().getPaymentRef());
						// set sales channel code as web by force
						creditTnx.setSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_AGENT);
					}
				}

				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);

			} else if (payment.getPaymentType() == PaymentType.BSP.getTypeValue()) {
				TnxAgentPayment agentPayment = (TnxAgentPayment) payment;
				String paymentRef = "";
				if(agentPayment.getPaymentReferenceTO() != null){
					paymentRef = BeanUtils.nullHandler(agentPayment.getPaymentReferenceTO().getPaymentRef());
				}
				 
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.BSP_PAYMENT.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);

				creditTnx.setFirstPayment(isFirstPayment ? CommonsConstants.YES : CommonsConstants.NO);
				creditTnx.setSegmentMapped(isFirstPayment ? CommonsConstants.YES : CommonsConstants.NO);
				creditTnx.setAgentCode(BeanUtils.nullHandler(agentPayment.getAgentCode()));
				creditTnx.setExternalReference(paymentRef);

				// TODO:RW handle for BSP
				// String creditSharedAgent = ReservationModuleUtils.getTravelAgentBD().getCreditSharingAgentCode(
				// creditTnx.getAgentCode());
				// if (!creditTnx.getAgentCode().equals(creditSharedAgent)) {
				// creditTnx.setCreditSharedAgentCode(creditSharedAgent);
				// }
		
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);

				if (isFirstPayment) {
					this.saveTransactionSegments(creditTnx, transactionSegments);
				}

			} else if (payment.getPaymentType() == PaymentType.CREDIT_BF.getTypeValue()) {
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CREDIT_BF.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);
				TnxCreditPayment paymentBF = (TnxCreditPayment) payment;
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO,
						paymentBF.getExpiryDate(), enableTransactionGranularity, transactionSeq);
			} else if (payment.getPaymentType() == PaymentType.CREDIT.getTypeValue()) {
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount().abs(),
						ReservationTnxNominalCode.CREDIT_BF.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);
				TnxCreditPayment paymentBF = (TnxCreditPayment) payment;
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO,
						paymentBF.getExpiryDate(), enableTransactionGranularity, transactionSeq);
			} else if (payment.getPaymentType() == PaymentType.PAYPAL.getTypeValue()) {
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.PAYMENT_PAYPAL.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;
				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);
			} else if (payment.getPaymentType() == PaymentType.CARD_DEBIT.getTypeValue()) {
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.CARD_PAYMENT_GENERIC_DEBIT.getCode(), credentialsDTO, currentTimestamp,
						recieptNumber, payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);
				TnxCardPayment cardPayment = (TnxCardPayment) payment;
				setPayReference(cardPayment.getPaymentReferanceTO(), creditTnx);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				saveCardInfo(cardPayment, pnrPaxId, creditTnx.getTnxId().intValue(), cardConfigData);
			} else if (payment.getPaymentType() == PaymentType.VOUCHER.getTypeValue()) {
				TnxVoucherPayment voucherPayment = (TnxVoucherPayment) payment;
				ReservationTnx creditTnx = TnxFactory.getCreditInstance(pnrPaxId, payment.getAmount(),
						ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode(), credentialsDTO, currentTimestamp, recieptNumber,
						payment.getPaymentCarrier(), payment.getLccUniqueTnxId(), false, true);
				creditTnx.setExternalReference(voucherPayment.getVoucherDTO().getVoucherId());
				setPayReference(voucherPayment.getPaymentReferanceTO(), creditTnx);
				this.saveTnxAndCredit(creditTnx, pnrPaxId, payment, reservationPaxPaymentMetaTO, credentialsDTO, null,
						enableTransactionGranularity, transactionSeq);
				VoucherBD voucherDelegate = ReservationModuleUtils.getVoucherBD();
				if (payment.getPaymentCarrier().equals(AppSysParamsUtil.getCarrierCode())) {
					BigDecimal redeemAmount = getVoucherRedeemedAmount(voucherPayment, creditTnx);
					voucherDelegate.recordRedemption(voucherPayment.getVoucherDTO().getVoucherId(), creditTnx.getTnxId(),
							redeemAmount.negate().toString(), null);
				}

			}

		} // end while

		Command cmdBalanceCredit = (Command) ReservationModuleUtils.getBean(CommandNames.BALANCE_CREDIT);
		cmdBalanceCredit.setParameter(CommandParamNames.PNR_PAX_ID, pnrPaxId);
		responce = (DefaultServiceResponse) cmdBalanceCredit.execute();

		// constructing and return command response
		if (responce.isSuccess()) {
			return responce;
		} else {
			throw new ModuleException("airreservation.recordpayment.error");
		}

	}
	
	private BigDecimal getVoucherRedeemedAmount(TnxVoucherPayment voucherPayment, ReservationTnx creditTnx) {
		BigDecimal redeemAmount = AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.divide(voucherPayment
				.getPayCurrencyDTO().getTotalPayCurrencyAmount(), voucherPayment.getPayCurrencyDTO()
				.getPayCurrMultiplyingExchangeRate()));
		double CURRENCY_CONVERSION_DIFF = 0.02;
		if (redeemAmount.compareTo(creditTnx.getAmount().negate()) > 0
				&& AccelAeroCalculator.subtract(redeemAmount, creditTnx.getAmount().negate()).doubleValue() < CURRENCY_CONVERSION_DIFF) {
			redeemAmount = creditTnx.getAmount().negate();
		}
		return redeemAmount;
	}

	private void saveTransactionSegments(ReservationTnx creditTnx, List<TransactionSegment> transactionSegments) {
		if (transactionSegments != null && creditTnx != null) {
			for (TransactionSegment trnxSegment : transactionSegments) {
				TransactionSegment clonedSeg = trnxSegment.clone();
				clonedSeg.setTnxId(creditTnx.getTnxId());
				reservationTnxDao.saveTransactionSegment(clonedSeg);
			}
		}
	}

	private void setPayReference(PaymentReferenceTO paymentReferanceTO, ReservationTnx creditTnx) {
		if (paymentReferanceTO != null) {
			String paymentRef = BeanUtils.nullHandler(paymentReferanceTO.getPaymentRef());
			Integer paymentMode = (paymentReferanceTO.getActualPaymentMode() != null
					&& paymentReferanceTO.getActualPaymentMode() != 0 ? paymentReferanceTO.getActualPaymentMode() : null);
			creditTnx.setExternalReference(paymentRef);
			if (paymentMode != null) {
				creditTnx.setExternalReferenceType(paymentMode);
			}
		}
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param paymentsList
	 * @param schedule
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkParams(String pnrPaxId, Collection paymentsList, CredentialsDTO credentialsDTO) throws ModuleException {

		if (pnrPaxId == null || credentialsDTO == null)// throw exception
			throw new ModuleException("airreservations.arg.invalid.null");
	}

	/**
	 * Recording tnx , tnx breakdown , and corresponding dry records for payment types other than onAccount Dry records
	 * are added if the payment is done by Dry user.
	 * 
	 * @param creditTnx
	 * @param pnrPaxId
	 * @param iTnxPayment
	 * @param reservationPaxPaymentMetaTO
	 * @param credentialsDTO
	 * @param transactionSeq
	 * @throws ModuleException
	 */
	private void saveTnxAndCredit(ReservationTnx creditTnx, String pnrPaxId, ITnxPayment iTnxPayment,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO, Date expDate,
			boolean enableTransactionGranularity, Integer transactionSeq) throws ModuleException {

		creditTnx.setPayCurrencyAmount(iTnxPayment.getPayCurrencyDTO().getTotalPayCurrencyAmount().negate());
		creditTnx.setPayCurrencyCode(iTnxPayment.getPayCurrencyDTO().getPayCurrencyCode());

		reservationTnxDao.saveTransaction(creditTnx);
		ReservationCredit credit = CreditFactory.getInstance(creditTnx.getTnxId().intValue(), pnrPaxId,
				AccelAeroCalculator.getDefaultBigDecimalZero(), expDate);
		reservationCreditDao.saveReservationCredit(credit);
		TnxGranularityFactory.saveReservationPaxTnxBrkForCreditsAndPaymentsComponsatingNewCharges(pnrPaxId,
				ReservationTnxNominalCode.CREDIT_BF, creditTnx, iTnxPayment.getPayCurrencyDTO(), reservationPaxPaymentMetaTO,
				enableTransactionGranularity, transactionSeq);
	}

	private void saveLMSTnxAndCredit(ReservationTnx creditTnx, String pnrPaxId, ITnxPayment iTnxPayment,
			ReservationPaxPaymentMetaTO reservationPaxPaymentMetaTO, CredentialsDTO credentialsDTO, Date expDate,
			boolean enableTransactionGranularity, Integer transactionSeq, Map<String, BigDecimal> lmsProductRedeemedAmount)
			throws ModuleException {

		creditTnx.setPayCurrencyAmount(iTnxPayment.getPayCurrencyDTO().getTotalPayCurrencyAmount().negate());
		creditTnx.setPayCurrencyCode(iTnxPayment.getPayCurrencyDTO().getPayCurrencyCode());

		reservationTnxDao.saveTransaction(creditTnx);
		ReservationCredit credit = CreditFactory.getInstance(creditTnx.getTnxId().intValue(), pnrPaxId,
				AccelAeroCalculator.getDefaultBigDecimalZero(), expDate);
		reservationCreditDao.saveReservationCredit(credit);
		TnxGranularityFactory.saveReservationPaxTnxBrkForLMSPayments(pnrPaxId, ReservationTnxNominalCode.CREDIT_BF, creditTnx,
				iTnxPayment.getPayCurrencyDTO(), reservationPaxPaymentMetaTO, enableTransactionGranularity, transactionSeq,
				lmsProductRedeemedAmount);
	}

	/**
	 * Note: Remove recoding of card sensitive data in AccelAero
	 * 
	 * @param cardPayment
	 * @param pnrPaxId
	 * @param transactionId
	 */
	private void saveCardInfo(TnxCardPayment cardPayment, String pnrPaxId, int transactionId,
			List<CardDetailConfigDTO> cardConfigData) {

		CreditCardDetail cardDetail = new CreditCardDetail();
		Map<String, String> storeDataMap = CreditCardUtil.getDBStroreData(cardConfigData, cardPayment.getCardNumber(),
				cardPayment.getCardName(), cardPayment.getCardExpiryDate(), cardPayment.getSecCode());
		String storeCardNumber = storeDataMap.get(FieldName.CARDNUMBER.code());

		if (cardPayment.getCardNumber() != null) {
			cardDetail.setNo(storeCardNumber);
		}
		cardDetail.setAddress(cardPayment.getAddress());
		if (cardPayment.getCardExpiryDate() != null) {
			cardDetail.setEDate(storeDataMap.get(FieldName.EXPIRYDATE.code()));
		}

		if (cardPayment.getCardName() != null) {
			cardDetail.setName(storeDataMap.get(FieldName.CARDHOLDERNAME.code()));
		}
		cardDetail.setNoLastDigits(BeanUtils.getLast4Digits(storeCardNumber));
		cardDetail.setNoFirstDigits(BeanUtils.getFirst6Digits(storeCardNumber));
		cardDetail.setTransactionId(transactionId);
		cardDetail.setType(cardPayment.getPaymentType());
		cardDetail.setPnrPaxId(new Integer(pnrPaxId));
		if (cardPayment.getSecCode() != null)
			cardDetail.setSecurityCode(storeDataMap.get(FieldName.CVV.code()));
		cardDetail.setPnr(cardPayment.getPnr());
		cardDetail.setPaymentBrokerRefNo(cardPayment.getPaymentBrokerRefNumber());
		cardDetail.setAuthorizationId(cardPayment.getAuthorizationId());
		Collection<TempPaymentTnx> colTempPayment = reservationPaymentDAO
				.getTempPaymentDetails(cardPayment.getPaymentBrokerRefNumber());
		Iterator<TempPaymentTnx> itrTempPayment = colTempPayment.iterator();
		while (itrTempPayment.hasNext()) {
			TempPaymentTnx tempPaymentTnx = (TempPaymentTnx) itrTempPayment.next();
			cardDetail.setNoLastDigits(tempPaymentTnx.getLast4DigitsCC());
		}

		reservationPaymentDAO.saveCreditCardDetail(cardDetail);
	}
}
