package com.isa.thinair.airreservation.core.bl.ReservationChangesDetector;

public interface ResChangesDetectorConstants {

	public interface ObserverRef {

		public static final String AIRPORT_MESSAGE_NOTIFIER_OBSERVER = "AIRPORT_MESSAGE_NOTIFIER_OBSERVER";
		public static final String PALCAL_MESSAGE_NOTIFIER_OBSERVER = "PALCAL_MESSAGE_NOTIFIER_OBSERVER";
	}

	public interface RuleRef {

		public static final String CREATE_CNF_RESERVATION_RULE = "CREATE_CNF_RESERVATION_RULE";
		public static final String CONFIRM_OHD_RESERVATION_RULE = "CONFIRM_OHD_RESERVATION_RULE";

		public static final String NAME_CHANGE_RULE = "NAME_CHANGE_RULE";
		public static final String SEG_MODIFY_RULE = "SEG_MODIFY_RULE";
		public static final String SEG_CNX_RULE = "SEG_CNX_RULE";
		public static final String RES_CNX_RULE = "RES_CNX_RULE";
		public static final String SSR_UPDATE_RULE = "SSR_UPDATE_RULE";
		public static final String REMOVE_PAX_RULE = "REMOVE_PAX_RULE";
		public static final String BOOKING_CLASS_MOD_RULE = "BOOKING_CLASS_MOD_RULE";
		public static final String CABIN_CLASS_MOD_RULE = "CABIN_CLASS_MOD_RULE";
	}

	public interface ActionTemplateRef {

		public static final String ADD_ACTION = "ADD_ACTION";
		public static final String DEL_ACTION = "DEL_ACTION";
		public static final String DEL_ADD_ACTION = "DEL_ADD_ACTION";
		public static final String CHG_ACTION = "CHG_ACTION";
	}
}
