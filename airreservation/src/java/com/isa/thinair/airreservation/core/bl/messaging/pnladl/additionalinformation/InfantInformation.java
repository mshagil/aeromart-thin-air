/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.additionalinformation;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.PassengerAdditions;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author udithad
 *
 */
public class InfantInformation extends
		PassengerAdditions<List<PassengerInformation>> {

	private PassengerDAO passengerDAO;
	private ETicketDAO eTicketDAO;
	private Integer flightId;
	private PassengerInformation infPassenger;
	private ReservationPax infant;
	private Flight flight;
	private ReservationTnxDAO reservationTnxDAO;

	public InfantInformation(Integer flightId) {
		this.flightId = flightId;
		passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;
		eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	@Override
	public void populatePassengerInformation(
			List<PassengerInformation> detailsDTOs) throws ModuleException {
		for (PassengerInformation information : detailsDTOs) {
			Set<ReservationPax> infantsSet = getInfantsSetBy(information
					.getPnrPaxId());
			if (infantsSet != null && infantsSet.size() > 0) {
				information.setInfant(createInfantInformation(infantsSet,
						information.isCodeShareFlight()));
			}
		}
	}

	private PassengerInformation createInfantInformation(
			Set<ReservationPax> infants, boolean isCodeShareFlight)
			throws ModuleException {
		flight = ReservationModuleUtils.getFlightBD().getFlight(flightId);
		if(infants != null && infants.size()>0){
			infant = infants.iterator().next();
		}
		if(infant != null){
			infPassenger = new PassengerInformation();
			createPassengerPersonalInformation();
			createPassengerAdditionalInformation();
			if (infant.getPaxAdditionalInfo() != null) {

			}
			EticketTO eTicket = eTicketDAO.getPassengerETicketDetail(
					infant.getPnrPaxId(), flightId);
			if (eTicket != null) {
				String eticket = eTicket.getEticketNumber();
				int couponNo = eTicket.getCouponNo();
				if (isCodeShareFlight) {
					if (eTicket.getExternalEticketNumber() != null) {
						eticket = eTicket.getExternalEticketNumber();
					}

					if (eTicket.getExternalCouponNo() != null) {
						couponNo = eTicket.getExternalCouponNo();
					}
				}

				infPassenger.setEticketNumber(eticket);
				infPassenger.setCoupon(couponNo);
			}
		}
		return infPassenger;
	}

	private void createPassengerPersonalInformation() {
		infPassenger.setPnrPaxId(infant.getPnrPaxId());
		infPassenger.setTitle(infant.getTitle() != null ? infant.getTitle()
				: "");
		infPassenger.setFirstName(infant.getFirstName() != null ? infant
				.getFirstName().toUpperCase() : "");
		infPassenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils
				.removeSpaces(infPassenger.getFirstName())));
		infPassenger.setLastName(infant.getLastName() != null ? infant
				.getLastName().toUpperCase() : "");
		infPassenger.setLastName(BeanUtils.removeExtraChars(BeanUtils
				.removeSpaces(infPassenger.getLastName())));
		infPassenger.setNationality(PnlAdlUtil.loadPassengerNationality(infant
				.getNationalityCode()));
		infPassenger.setPaxType(infant.getPaxType());
		infPassenger.setDob(infant.getDateOfBirth());
	}

	private void createPassengerAdditionalInformation() {
		ReservationPaxAdditionalInfo addInfo = infant.getPaxAdditionalInfo();

		setFoidInformation(addInfo);

		if (addInfo.getPassportExpiry() != null) {
			infPassenger.setFoidExpiry(addInfo.getPassportExpiry());
		}
		if (addInfo.getPassportIssuedCntry() != null) {
			infPassenger.setFoidIssuedCountry(addInfo.getPassportIssuedCntry());
		}
		if (addInfo.getVisaDocNumber() != null) {
			infPassenger.setVisaDocNumber(addInfo.getVisaDocNumber());
		}
		if (addInfo.getPlaceOfBirth() != null) {
			infPassenger.setPlaceOfBirth(addInfo.getPlaceOfBirth());
		}
		if (addInfo.getTravelDocumentType() != null) {
			infPassenger.setTravelDocumentType(addInfo.getTravelDocumentType());
		}
		if (addInfo.getVisaDocIssueDate() != null) {
			infPassenger.setVisaDocIssueDate(addInfo.getVisaDocIssueDate());
		}
		if (addInfo.getVisaDocPlaceOfIssue() != null) {
			infPassenger.setVisaDocPlaceOfIssue(addInfo
					.getVisaDocPlaceOfIssue());
		}
		if (addInfo.getVisaApplicableCountry() != null) {
			infPassenger.setVisaApplicableCountry(addInfo
					.getVisaApplicableCountry());
		}
	}

	private void setFoidInformation(ReservationPaxAdditionalInfo addInfo) {
		String foidNumber = null;
		boolean isNICSentInPNLADL = false;
		if (("DOM").equals(flight.getFlightType())){
			if (AppSysParamsUtil
					.isRequestNationalIDForReservationsHavingDomesticSegments()) {
					if(addInfo.getNationalIDNo() != null) {
						foidNumber = addInfo.getNationalIDNo().trim();
						isNICSentInPNLADL = true;
					}
			}
		} else {
			foidNumber = addInfo.getPassportNo();
			if ((foidNumber == null || "".equals(foidNumber))
					&& addInfo.getNationalIDNo() != null) {
				foidNumber = addInfo.getNationalIDNo().trim();
				isNICSentInPNLADL = true;
			}
		}
		infPassenger.setFoidNumber(foidNumber);
		infPassenger.setNICSentInPNLADL(isNICSentInPNLADL);
	}

	private Set<ReservationPax> getInfantsSetBy(Integer passengerId) {
		return this.infantInformation.get(passengerId);
	}

	@Override
	public void getAncillaryInformation(
			List<PassengerInformation> passengerInformations) {
		for (PassengerInformation passengerInformation : passengerInformations) {
			ReservationPax reservationPax = passengerDAO.getPassenger(
					passengerInformation.getPnrPaxId(), false, false, true);
			if (reservationPax != null && reservationPax.getInfants() != null
					&& reservationPax.getInfants().size() > 0) {
				if(reservationPax.getReservation().isInfantPaymentRecordedWithInfant()){
					Set<ReservationPax> infants = removeBalanceToPayInfants(reservationPax.getInfants());
					if(!infants.isEmpty()){
						this.infantInformation.put(passengerInformation.getPnrPaxId(),
								reservationPax.getInfants());
					}
				}else{
					this.infantInformation.put(passengerInformation.getPnrPaxId(),
							reservationPax.getInfants());
				}
				
			}

		}

	}
	
	private Set<ReservationPax> removeBalanceToPayInfants(Set<ReservationPax> infants){
		
		Set<ReservationPax> filteredInfants = new HashSet<>();
		
		for(ReservationPax infant : infants){
			BigDecimal credit = reservationTnxDAO.getPNRPaxBalance(String.valueOf(infant.getPnrPaxId()));
			double creditValue = 0;
			if(credit == null){
				filteredInfants.add(infant);
			}else {
				creditValue = credit.doubleValue();
				if(creditValue <= 0){
					filteredInfants.add(infant);
				}
			}	
		}
		
		return filteredInfants;
		
	}

}
