/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TypeBRequestDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.bl.common.TypeBRequestComposer;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for updating a reservation contact information only
 * 
 * Business Rules: (1) Only updates PNR contact information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="updateContactInfo"
 */
public class UpdateContactInfo extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(UpdateContactInfo.class);

	/**
	 * Execute method of the UpdateContactInfo command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		// Getting command parameters
		String pnr = (String) this.getParameter(CommandParamNames.PNR);
		Long version = (Long) this.getParameter(CommandParamNames.VERSION);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		ReservationContactInfo clientContInfo = (ReservationContactInfo) this
				.getParameter(CommandParamNames.RESERVATION_CONTACT_INFO);
		Boolean isDummyReservation = (Boolean) this.getParameter(CommandParamNames.IS_DUMMY_RESERVATION);

		// Checking params
		this.validateParams(pnr, version, clientContInfo, isDummyReservation, credentialsDTO);

		// Get the server reservation
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		Reservation srvReservation = ReservationProxy.getReservation(pnrModesDTO);
		ReservationContactInfo srvContInfo = srvReservation.getContactInfo();

		clientContInfo.setPnr(srvReservation.getPnr());
		clientContInfo.setVersion(srvContInfo.getVersion());

		// skipping version check for dummy reservation
		if (!isDummyReservation && (srvReservation.getVersion() != version.longValue())) {
			throw new ModuleException("airreservations.arg.concurrentUpdate");
		} else {
			if (srvContInfo.getCustomerId() != null) {
				clientContInfo.setCustomerId(srvContInfo.getCustomerId());
			}
			srvReservation.setContactInfo(clientContInfo);
		}

		// Saves the reservation
		ReservationProxy.saveReservation(srvReservation);

		// Constructing and returning the command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.VERSION, Long.valueOf(srvReservation.getVersion()));

		if (!isDummyReservation) {
			// Get the reservation audit
			ReservationAudit reservationAudit = this.composeAudit(pnr, srvContInfo, clientContInfo, credentialsDTO);
			Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();
			colReservationAudit.add(reservationAudit);
			response.addResponceParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION, colReservationAudit);

			if (!(credentialsDTO != null && credentialsDTO.getTrackInfoDTO() != null
					&& credentialsDTO.getTrackInfoDTO().getAppIndicator() != null && credentialsDTO.getTrackInfoDTO()
					.getAppIndicator().equals(AppIndicatorEnum.APP_GDS))) {
				// send typeB message
				Integer gdsNotifyAction = 13;
				if (gdsNotifyAction.equals(Integer.valueOf(GDSInternalCodes.GDSNotifyAction.CHANGE_CONTACT_DETAILS.getCode()))) {
					TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
					typeBRequestDTO.setReservation(srvReservation);
					typeBRequestDTO.setOriginalContInfo(srvContInfo);
					typeBRequestDTO.setGdsNotifyAction(gdsNotifyAction);
					TypeBRequestComposer.sendTypeBRequest(typeBRequestDTO);
				}
			}
			Set<String> csOCCarrirs = ReservationCoreUtils.getCSOCCarrierCodesInConfirmedSegments(srvReservation);		
			if (csOCCarrirs != null) {
				// send Codeshare typeB message
				TypeBRequestDTO typeBRequestDTO = new TypeBRequestDTO();
				typeBRequestDTO.setReservation(srvReservation);
				typeBRequestDTO.setOriginalContInfo(srvContInfo);
				typeBRequestDTO.setGdsNotifyAction(GDSInternalCodes.GDSNotifyAction.CHANGE_CONTACT_DETAILS.getCode());
				TypeBRequestComposer.sendCSTypeBRequest(typeBRequestDTO, csOCCarrirs);
			}
		}
		log.debug("Exit execute");
		return response;
	}

	/**
	 * Compose ReservationModification object
	 * 
	 * @param pnr
	 * @param srvContInfo
	 * @param clientContInfo
	 * @param credentialsDTO
	 * @return
	 * @throws ModuleException
	 */
	private ReservationAudit composeAudit(String pnr, ReservationContactInfo srvContInfo, ReservationContactInfo clientContInfo,
			CredentialsDTO credentialsDTO) throws ModuleException {
		String changes = BeanUtils.compare(clientContInfo, srvContInfo);

		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setPnr(pnr);
		reservationAudit.setModificationType(AuditTemplateEnum.CHANGED_CONTACT_DETAILS.getCode());
		reservationAudit.setUserNote(null);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedContactDetails.CONTACT_DETAILS, changes);

		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedContactDetails.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		// Setting the origin carrier code
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ChangedContactDetails.ORIGIN_CARRIER, credentialsDTO
					.getTrackInfoDTO().getCarrierCode());
		}

		return reservationAudit;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param pnr
	 * @param version
	 * @param contactInfo
	 * @param isDummyReservation
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(String pnr, Long version, ReservationContactInfo contactInfo, Boolean isDummyReservation,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (pnr == null || version == null || version.longValue() == -1 || contactInfo == null || credentialsDTO == null
				|| isDummyReservation == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
