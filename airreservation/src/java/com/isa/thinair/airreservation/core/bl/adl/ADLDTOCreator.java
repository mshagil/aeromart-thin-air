package com.isa.thinair.airreservation.core.bl.adl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.InboundConnectionDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLDTO;
import com.isa.thinair.airreservation.api.dto.adl.ADLPassengerData;
import com.isa.thinair.airreservation.api.dto.adl.AncillaryDTO;
import com.isa.thinair.airreservation.api.dto.adl.FlightInformation;
import com.isa.thinair.airreservation.api.dto.adl.PassengerInformation;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationWLPriority;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.PNLConstants.AdlActions;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.PnlAdlUtil;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.PassengerDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class ADLDTOCreator {

	/** AuxDAO set in constructor * */
	private ReservationAuxilliaryDAO auxilliaryDAO = null;
	private PassengerDAO passengerDAO = null;
	private ETicketDAO eTicketDAO;
	private ReservationDAO reservationDAO;
	private String msgType = null;
	private static Log log = LogFactory.getLog(ADLDTOCreator.class);
	Set<Integer> uniquePnlPaxIds = new HashSet<Integer>();
	private ReservationTnxDAO reservationTnxDAO;

	public ADLDTOCreator() {
		this.auxilliaryDAO = ReservationDAOUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		this.passengerDAO = ReservationDAOUtils.DAOInstance.PASSENGER_DAO;
		this.eTicketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		this.reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		this.msgType = PNLConstants.ADLGeneration.ADL;
		this.reservationTnxDAO = ReservationDAOUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	public ADLDTO generateADLElements(int flightId, String depAirportCode) throws ModuleException {
		// There might be scenarios where flight get canceled and reprotected passengers to a new flight and
		// PNL/ADL jobs already scheduled for old flight. This is to check and validate flight id
		flightId = this.locateActualFlightId(flightId);

		String flightNumber = auxilliaryDAO.getFlightNumber(flightId);
		log.debug("###################### Retrieved Flight Num : " + flightNumber + " for flight id: " + flightId + " airport : "
				+ depAirportCode);

		// populate the ADLElements
		Date localDate = auxilliaryDAO.getFlightLocalDate(flightId, depAirportCode);

		ADLDTO adlElements = new ADLDTO();
		FlightInformation flightInfo = new FlightInformation();
		flightInfo.setFlightId(flightId);
		flightInfo.setFlightNumber(flightNumber);
		flightInfo.setFlightDate(localDate);
		flightInfo.setBoardingAirport(depAirportCode);
		adlElements.setFlightInfo(flightInfo);
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			adlElements.setClassCodes(ReservationModuleUtils.getFlightInventoryBD().getAllocatedBookingClassesForCabinClasses(
					flightId));
		}
		adlElements.setPassengerData(this.generatepassengerADLElements(flightId, depAirportCode, flightNumber));
		Object[] pnlInformation = auxilliaryDAO.getPaxCountInPnlADL(new Integer(flightId), depAirportCode);
		adlElements.setDestinationWisePaxCount((HashMap<String, Integer>) pnlInformation[0]);
		adlElements.setLastGroupCode((String) pnlInformation[1]);
		adlElements.setUniquePnlPaxIds(uniquePnlPaxIds);
		return adlElements;

	}

	private Integer locateActualFlightId(Integer flightId) throws ModuleException {
		FlightBD fightBD = ReservationModuleUtils.getFlightBD();
		Flight flight = fightBD.getFlight(flightId);
		if (flight != null && flight.getStatus().equals(FlightStatusEnum.CANCELLED.getCode())) {
			log.info(" #####################     FLIGHT CANCELLED AND LOADING NEW FLIGHT ID : " + flightId);
			Integer actFlightId = null;
			for (FlightSegement fs : (flight.getFlightSegements())) {
				actFlightId = fightBD.getFlightID(fs.getSegmentCode(), flight.getFlightNumber(), fs.getEstTimeDepatureZulu());
				if (actFlightId != null) {
					flightId = actFlightId;
					log.info(" #####################     NEW FLIGHT ID TO SEND ADL : " + flightId);
					break;
				} else {
					log.info(" #####################     UNABLE TO FIND NEW FLIGHT ID AND USING OLD ONE : " + flightId);
				}
			}
		}
		return flightId;
	}

	private Map<String, Map<String, ADLPassengerData>> generatepassengerADLElements(Integer flightId,
			String departureAirportCode, String flightNumber) throws ModuleException {

		String carrierCode = "";
		if (flightNumber != null && !flightNumber.equals("")) {
			carrierCode = flightNumber.substring(0, 2);
		} else {
			carrierCode = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		Timestamp currentTimeStamp = new Timestamp(new Date().getTime());
		List<PassengerInformation> adlPassengers = auxilliaryDAO.getADLPassengerDetails(flightId, departureAirportCode,
				currentTimeStamp, carrierCode);
		// This is for build removed pax and splitted pax DEL elements in ADL
		List<PassengerInformation> allDeletedAdlPassengers = auxilliaryDAO.getDeletedADLPassengerDetails(flightId,
				departureAirportCode, currentTimeStamp, carrierCode);
		if (adlPassengers.size() == 0) {
			if (allDeletedAdlPassengers.size() == 0) {
				throw new ModuleException("reservationauxilliary.adl.noreservations", "airreservations");
			} else {
				return this.populateOnlyDeletedPassengers(allDeletedAdlPassengers);
			}

		}
		Map<String, Map<String, ADLPassengerData>> passengerADLElements = this.generatePassengerADLElements(adlPassengers,
				flightId, departureAirportCode);

		if (allDeletedAdlPassengers.size() != 0) {
			return this.populateFinalPassengerADLElements(passengerADLElements, allDeletedAdlPassengers);
		}

		return passengerADLElements;
	}

	private void injectSeatDetails(PassengerInformation passenger, Map<Integer, List<AncillaryDTO>> passengerSeats) {
		if (passengerSeats.get(passenger.getPnrPaxId()) != null) {
			passenger.setSeats(passengerSeats.get(passenger.getPnrPaxId()));
		}
	}

	private void injectMealDetails(PassengerInformation passenger, Map<Integer, List<AncillaryDTO>> passengerMeals) {
		if (passengerMeals.get(passenger.getPnrPaxId()) != null) {
			passenger.setMeals(passengerMeals.get(passenger.getPnrPaxId()));
		}
	}

	private void injectBaggageDetails(PassengerInformation passenger, AncillaryDTO passengerBaggage) {
		if (passengerBaggage != null) {
			passenger.setBaggages(passengerBaggage);
		}
	}

	private void injectSsrDetails(PassengerInformation passenger, Map<Integer, List<AncillaryDTO>> passengerSsrs) {
		if (passengerSsrs.get(passenger.getPnrPaxId()) != null) {
			passenger.setSsrs(passengerSsrs.get(passenger.getPnrPaxId()));
		}
	}

	private void injectInfantDetails(PassengerInformation passenger, Integer flightId) throws ModuleException {
		ReservationPax rpa = passengerDAO.getPassenger(passenger.getPnrPaxId(), false, false, true);
		Flight flight = ReservationModuleUtils.getFlightBD().getFlight(flightId);
		Set<ReservationPax> infants;
		if(rpa.getReservation().isInfantPaymentRecordedWithInfant()){
			infants = removeBalanceToPayInfants(rpa.getInfants());
		}else{
			infants = rpa.getInfants();
		}
		PassengerInformation infPassenger = null;
		String foidNumber = "";
		boolean isNICSentInPNLADL = false;
		for (ReservationPax infant : infants) {
			infPassenger = new PassengerInformation();
			infPassenger.setPnrPaxId(infant.getPnrPaxId());
			infPassenger.setTitle(infant.getTitle() != null ? infant.getTitle() : "");
			infPassenger.setFirstName(infant.getFirstName() != null ? infant.getFirstName().toUpperCase() : "");
			infPassenger.setFirstName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(infPassenger.getFirstName())));
			infPassenger.setLastName(infant.getLastName() != null ? infant.getLastName().toUpperCase() : "");
			infPassenger.setLastName(BeanUtils.removeExtraChars(BeanUtils.removeSpaces(infPassenger.getLastName())));
			infPassenger.setNationality(PnlAdlUtil.loadPassengerNationality(infant.getNationalityCode()));
			infPassenger.setPaxType(infant.getPaxType());
			infPassenger.setDob(infant.getDateOfBirth());
			if (infant.getPaxAdditionalInfo() != null) {
				ReservationPaxAdditionalInfo addInfo = infant.getPaxAdditionalInfo();

				if (AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()) {
					if (("DOM").equals(flight.getFlightType()) && addInfo.getNationalIDNo() != null) {
						foidNumber = addInfo.getNationalIDNo().trim();
						isNICSentInPNLADL = true;
					}
				} else {
					foidNumber = addInfo.getPassportNo();
					if ((foidNumber == null || "".equals(foidNumber)) && addInfo.getNationalIDNo() != null) {
						foidNumber = addInfo.getNationalIDNo().trim();
						isNICSentInPNLADL = true;
					}
				}
				infPassenger.setFoidNumber(foidNumber);
				infPassenger.setNICSentInPNLADL(isNICSentInPNLADL);

				if (addInfo.getPassportExpiry() != null) {
					infPassenger.setFoidExpiry(addInfo.getPassportExpiry());
				}
				if (addInfo.getPassportIssuedCntry() != null) {
					infPassenger.setFoidIssuedCountry(addInfo.getPassportIssuedCntry());
				}
				if (addInfo.getVisaDocNumber() != null) {
					infPassenger.setVisaDocNumber(addInfo.getVisaDocNumber());
				}
				if (addInfo.getPlaceOfBirth() != null) {
					infPassenger.setPlaceOfBirth(addInfo.getPlaceOfBirth());
				}
				if (addInfo.getTravelDocumentType() != null) {
					infPassenger.setTravelDocumentType(addInfo.getTravelDocumentType());
				}
				if (addInfo.getVisaDocIssueDate() != null) {
					infPassenger.setVisaDocIssueDate(addInfo.getVisaDocIssueDate());
				}
				if (addInfo.getVisaDocPlaceOfIssue() != null) {
					infPassenger.setVisaDocPlaceOfIssue(addInfo.getVisaDocPlaceOfIssue());
				}
				if (addInfo.getVisaApplicableCountry() != null) {
					infPassenger.setVisaApplicableCountry(addInfo.getVisaApplicableCountry());
				}				
			}
			EticketTO eTicket = eTicketDAO.getPassengerETicketDetail(infant.getPnrPaxId(), flightId);
			if (eTicket != null) {				
				String eticket = eTicket.getEticketNumber();
				int couponNo = eTicket.getCouponNo();
				if(passenger.isCodeShareFlight()){
					if(eTicket.getExternalEticketNumber()!=null){
						eticket =eTicket.getExternalEticketNumber();	
					}				
					
					if(eTicket.getExternalCouponNo()!=null){
						couponNo = eTicket.getExternalCouponNo();	
					}					
				}			
				
				
				infPassenger.setEticketNumber(eticket);
				infPassenger.setCoupon(couponNo);
			}
		}
		passenger.setInfant(infPassenger);
	}
	
	private Set<ReservationPax> removeBalanceToPayInfants(Set<ReservationPax> infants){
		
		Set<ReservationPax> filteredInfants = new HashSet<>();
		
		for(ReservationPax infant : infants){
			BigDecimal credit = reservationTnxDAO.getPNRPaxBalance(String.valueOf(infant.getPnrPaxId()));
			double creditValue = 0;
			if(credit == null){
				filteredInfants.add(infant);
			}else {
				creditValue = credit.doubleValue();
				if(creditValue <= 0){
					filteredInfants.add(infant);
				}
			}	
		}
		
		return filteredInfants;
		
	}

	private void injectInboundDetails(PassengerInformation passenger, Integer flightId, String departureAirportCode)
			throws ModuleException {
		InboundConnectionDTO idt = auxilliaryDAO.getInBoundConnectionList(passenger.getPnr(), passenger.getPnrPaxId(),
				departureAirportCode, flightId);
		if (idt != null) {
			passenger.setInboundInfo(idt);
		}
	}

	private void injectOnwardConnectionDetails(PassengerInformation passenger, Integer flightId, String departureAirportCode)
			throws ModuleException {
		ArrayList<OnWardConnectionDTO> onwardConnections = (ArrayList<OnWardConnectionDTO>) auxilliaryDAO
				.getAllOutBoundSequences(passenger.getPnr(), passenger.getPnrPaxId(), departureAirportCode, flightId, passenger
						.getPnrStatus().equalsIgnoreCase("CNF")
						& (passenger.getPnlStatus().equalsIgnoreCase("N") | passenger.getPnlStatus().equalsIgnoreCase("C")));
		if (onwardConnections != null) {
			passenger.setOnwardconnectionlist(onwardConnections);
		}
	}

	private void injectWaitListedDetails(PassengerInformation passenger) throws ModuleException {
		ReservationWLPriority rwlp = reservationDAO.getWaitListedReservationPriority(passenger.getPnr(), passenger.getPnrSegId());
		if (rwlp != null && rwlp.getPriority() != null) {
			passenger.setWaitListedInfo(rwlp.getPriority().toString());
		}
	}

	private Map<String, Map<String, ADLPassengerData>> generatePassengerADLElements(List<PassengerInformation> adlPassengers,
			Integer flightId, String departureAirportCode) throws ModuleException {
		Map<String, Map<String, ADLPassengerData>> passengerADLElements = new HashMap<String, Map<String, ADLPassengerData>>();
		Map<Integer, List<AncillaryDTO>> passengerSeats = PnlAdlUtil.getPassengerSeatMapDetails(adlPassengers);
		Map<Integer, List<AncillaryDTO>> passengerMeals = PnlAdlUtil.getPassengerMealDetails(adlPassengers);
		Map<Integer, AncillaryDTO> passengerBaggages = PnlAdlUtil
				.getPassengerBaggagesDetails(adlPassengers, departureAirportCode);
		Map<Integer, List<AncillaryDTO>> passengerSsrs = PnlAdlUtil.getPassengerSSRDetails(adlPassengers, DCS_PAX_MSG_GROUP_TYPE.PNL_ADL);
		Set<String> fareClasses = new HashSet<String>();
		Set<String> destinations = new HashSet<String>();
		for (PassengerInformation adlPassenger : adlPassengers) {
			fareClasses.add(adlPassenger.getRbdClassOfService());
			destinations.add(adlPassenger.getDestinationAirportCode());
			uniquePnlPaxIds.add(adlPassenger.getPnlPaxId());
			// Injecting seat map details
			this.injectSeatDetails(adlPassenger, passengerSeats);
			// Injecting meal details
			this.injectMealDetails(adlPassenger, passengerMeals);
			// Injecting baggage details
			this.injectBaggageDetails(adlPassenger, passengerBaggages.get(adlPassenger.getPnrPaxId()));
			// Injecting SSR details
			this.injectSsrDetails(adlPassenger, passengerSsrs);
			// Injecting infants
			this.injectInfantDetails(adlPassenger, flightId);
			// Injecting inbound information
			this.injectInboundDetails(adlPassenger, flightId, departureAirportCode);
			// Inject wait listed details if any
			if (adlPassenger.isWaitListedPassenger()) {
				this.injectWaitListedDetails(adlPassenger);
			}
			// Injecting onward connection details
			this.injectOnwardConnectionDetails(adlPassenger, flightId, departureAirportCode);
			if (passengerADLElements.get(adlPassenger.getDestinationAirportCode()) == null) {
				Map<String, ADLPassengerData> destinationWiseADLPassengers = new HashMap<String, ADLPassengerData>();
				passengerADLElements.put(adlPassenger.getDestinationAirportCode(), destinationWiseADLPassengers);
				if (passengerADLElements.get(adlPassenger.getDestinationAirportCode()).get(adlPassenger.getRbdClassOfService()) == null) {
					ADLPassengerData adlPassengerData = new ADLPassengerData();
					passengerADLElements.get(adlPassenger.getDestinationAirportCode()).put(adlPassenger.getRbdClassOfService(),
							adlPassengerData);
					this.populateADLLists(adlPassengerData, adlPassenger);
				} else {
					ADLPassengerData adlPassengerData = passengerADLElements.get(adlPassenger.getDestinationAirportCode()).get(
							adlPassenger.getRbdClassOfService());
					this.populateADLLists(adlPassengerData, adlPassenger);
				}
			} else {
				if (passengerADLElements.get(adlPassenger.getDestinationAirportCode()).get(adlPassenger.getRbdClassOfService()) == null) {
					ADLPassengerData adlPassengerData = new ADLPassengerData();
					passengerADLElements.get(adlPassenger.getDestinationAirportCode()).put(adlPassenger.getRbdClassOfService(),
							adlPassengerData);
					this.populateADLLists(adlPassengerData, adlPassenger);
				} else {
					ADLPassengerData adlPassengerData = passengerADLElements.get(adlPassenger.getDestinationAirportCode()).get(
							adlPassenger.getRbdClassOfService());
					this.populateADLLists(adlPassengerData, adlPassenger);
				}
			}
		}

		// Generating Fare class wise pnl for empty/ non-booked logical cabin classes
		if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
			List<String> allocatedFareClasses = new ArrayList<String>();
			LinkedHashMap<String, List<String>> fareClassDetails = ReservationModuleUtils.getFlightInventoryBD()
					.getAllocatedBookingClassesForCabinClasses(flightId);
			for (String cabinClass : fareClassDetails.keySet()) {
				for (String fareClass : fareClassDetails.get(cabinClass)) {
					allocatedFareClasses.add(fareClass);
				}
			}
			for (String des : destinations) {
				for (String allocatedFareClass : allocatedFareClasses) {
					String rbdCos = String.valueOf(allocatedFareClass.charAt(0));
					if (!fareClasses.contains(rbdCos)) {
						if (passengerADLElements.get(des) == null) {
							Map<String, ADLPassengerData> destinationWiseADLPassengers = new HashMap<String, ADLPassengerData>();
							passengerADLElements.put(des, destinationWiseADLPassengers);
							if (passengerADLElements.get(des).get(rbdCos) == null) {
								ADLPassengerData adlPassengerData = new ADLPassengerData();
								passengerADLElements.get(des).put(rbdCos, adlPassengerData);
							}
						} else {
							if (passengerADLElements.get(des).get(rbdCos) == null) {
								ADLPassengerData adlPassengerData = new ADLPassengerData();
								passengerADLElements.get(des).put(rbdCos, adlPassengerData);
							}
						}
					}
				}
			}
		}
		return passengerADLElements;
	}

	private void populateADLLists(ADLPassengerData adlPassengerData, PassengerInformation adlPassenger) {
		switch (AdlActions.valueOf(adlPassenger.getAdlAction())) {
		// ADL Addition
		case A:
			adlPassengerData.addAddedPassengers(adlPassenger);
			break;
		// ADL Deletion
		case D:
			adlPassengerData.addDeletedPassengers(adlPassenger);
			break;
		// ADL Change
		case C:
			adlPassengerData.addChangedPassengers(adlPassenger);
			break;
		default:
			// This can not be possible
			break;
		}
	}

	private Map<String, Map<String, ADLPassengerData>> populateFinalPassengerADLElements(
			Map<String, Map<String, ADLPassengerData>> passengerADLElements, List<PassengerInformation> allDeletedAdlPassengers) {

		for (PassengerInformation removedPassenger : allDeletedAdlPassengers) {
			String destination = removedPassenger.getDestinationAirportCode();
			if (removedPassenger.getRbdClassOfService() != null) {
				String rbdCos = removedPassenger.getRbdClassOfService();
				boolean removedPaxExists = false;
				ADLPassengerData adlPassengerData = passengerADLElements.get(destination).get(rbdCos);
				List<PassengerInformation> existingDelPassengers = adlPassengerData.getDeletedPassengers();
				if (existingDelPassengers != null && !existingDelPassengers.isEmpty()) {
					for (PassengerInformation deletedPassenger : existingDelPassengers) {
						if (deletedPassenger.getPnlPaxId().equals(removedPassenger.getPnlPaxId())) {
							removedPaxExists = true;
							break;
						}
					}
				}

				if (!removedPaxExists) {
					adlPassengerData.addDeletedPassengers(removedPassenger);
					uniquePnlPaxIds.add(removedPassenger.getPnlPaxId());
				}
			}
		}

		return passengerADLElements;

	}

	private Map<String, Map<String, ADLPassengerData>> populateOnlyDeletedPassengers(
			List<PassengerInformation> allDeletedAdlPassengers) {
		Map<String, Map<String, ADLPassengerData>> passengerADLElements = new HashMap<String, Map<String, ADLPassengerData>>();
		for (PassengerInformation removedPassenger : allDeletedAdlPassengers) {
			if (passengerADLElements.get(removedPassenger.getDestinationAirportCode()) == null) {
				Map<String, ADLPassengerData> destinationWiseADLPassengers = new HashMap<String, ADLPassengerData>();
				passengerADLElements.put(removedPassenger.getDestinationAirportCode(), destinationWiseADLPassengers);
				if (passengerADLElements.get(removedPassenger.getDestinationAirportCode()).get(
						removedPassenger.getRbdClassOfService()) == null) {
					ADLPassengerData adlPassengerData = new ADLPassengerData();
					passengerADLElements.get(removedPassenger.getDestinationAirportCode()).put(
							removedPassenger.getRbdClassOfService(), adlPassengerData);
					this.populateADLLists(adlPassengerData, removedPassenger);
				} else {
					ADLPassengerData adlPassengerData = passengerADLElements.get(removedPassenger.getDestinationAirportCode())
							.get(removedPassenger.getRbdClassOfService());
					this.populateADLLists(adlPassengerData, removedPassenger);
				}
			} else {
				if (passengerADLElements.get(removedPassenger.getDestinationAirportCode()).get(
						removedPassenger.getRbdClassOfService()) == null) {
					ADLPassengerData adlPassengerData = new ADLPassengerData();
					passengerADLElements.get(removedPassenger.getDestinationAirportCode()).put(
							removedPassenger.getRbdClassOfService(), adlPassengerData);
					this.populateADLLists(adlPassengerData, removedPassenger);
				} else {
					ADLPassengerData adlPassengerData = passengerADLElements.get(removedPassenger.getDestinationAirportCode())
							.get(removedPassenger.getRbdClassOfService());
					this.populateADLLists(adlPassengerData, removedPassenger);
				}
			}
			uniquePnlPaxIds.add(removedPassenger.getPnlPaxId());
		}
		return passengerADLElements;
	}
}
