/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airreservation.core.bl.pnr;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.PaxDiscountInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxSegmentSSRDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airreservation.core.activators.ReservationCoreUtils;
import com.isa.thinair.airreservation.core.bl.segment.SegmentBucketBO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;

/**
 * Command for making a reservation This will create reservation required objects structure inorder to persist in the
 * database
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.command name="makeReservation"
 */
public class MakeReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(MakeReservation.class);

	/**
	 * Execute method of the MakeReservation command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		Reservation reservation = (Reservation) this.getParameter(CommandParamNames.RESERVATION);
		Date pnrZuluReleaseTimeStamp = (Date) this.getParameter(CommandParamNames.OVERRIDDEN_ZULU_RELEASE_TIMESTAMP);
		Collection colOndFareDTOs = (Collection) this.getParameter(CommandParamNames.OND_FARE_DTOS);
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Map segObjectsMap = (Map) this.getParameter(CommandParamNames.SEG_OBJECTS_MAP);

		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps = (Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>>) this
				.getParameter(CommandParamNames.PAX_SEGMENT_SSR_MAPS);

		DefaultServiceResponse output = (DefaultServiceResponse) this.getParameter(CommandParamNames.OUTPUT_SERVICE_RESPONSE);

		DiscountedFareDetails discFareDetailsDTO = this.getParameter(CommandParamNames.FARE_DISCOUNT_INFO,
				DiscountedFareDetails.class);

		ReservationDiscountDTO reservationDiscountDTO = (ReservationDiscountDTO) this
				.getParameter(CommandParamNames.RESERVATION_DISCOUNT_DTO);

		boolean isCreditPromotion = false;
		if (discFareDetailsDTO != null
				&& PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(discFareDetailsDTO.getDiscountAs())) {
			isCreditPromotion = true;
		}

		// Checking params
		this.validateParams(reservation, colOndFareDTOs, segObjectsMap, credentialsDTO);

		// build the reservation
		reservation = this.buildReservation(reservation, pnrZuluReleaseTimeStamp, colOndFareDTOs, segObjectsMap,
				paxSegmentSSRDTOMaps, credentialsDTO, isCreditPromotion, reservationDiscountDTO);

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(CommandParamNames.RESERVATION, reservation);
		response.addResponceParam(CommandParamNames.OUTPUT_SERVICE_RESPONSE, output);

		log.debug("Exit execute");
		return response;
	}

	/**
	 * Build the Reservation
	 * 
	 * @param reservation
	 * @param pnrZuluReleaseTimeStamp
	 * @param colOndFareDTOs
	 * @param segObjectsMap
	 * @param paxSegmentSSRDTOMaps
	 * @param isCreditPromotion
	 * @param reservationDiscountDTO
	 * @return
	 * @throws ModuleException
	 */
	public Reservation buildReservation(Reservation reservation, Date pnrZuluReleaseTimeStamp,
			Collection<OndFareDTO> colOndFareDTOs, Map<Integer, ReservationSegment> segObjectsMap,
			Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> paxSegmentSSRDTOMaps, CredentialsDTO credentialsDTO,
			boolean isCreditPromotion, ReservationDiscountDTO reservationDiscountDTO) throws ModuleException {

		// Holds seat allocation information
		SegmentBucketBO segmentBucket = new SegmentBucketBO(colOndFareDTOs);

		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		// For reservation
		int totalPaxAdultCount = 0;
		int totalPaxChildCount = 0;
		int totalPaxInfantCount = 0;

		// For an OND
		Map<Integer, ReservationPax> allPassengers = new HashMap<Integer, ReservationPax>();

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (pnrZuluReleaseTimeStamp != null) {
				// We are setting the zulu release time stamp finally. This was
				// sent in from the front-end
				reservationPax.setZuluReleaseTimeStamp(pnrZuluReleaseTimeStamp);
			}

			// Keep the passenger objects with the passenger sequence will be
			// used in later
			allPassengers.put(reservationPax.getPaxSequence(), reservationPax);
			PaxDiscountDetailTO paxDiscountDetailTO = null;
			String discountCode = null;
			double discountPercentage = 0;
			if (reservationDiscountDTO != null) {
				paxDiscountDetailTO = reservationDiscountDTO.getPaxDiscountDetail(reservationPax.getPaxSequence());
				discountCode = reservationDiscountDTO.getDiscountCode();
				if (reservationDiscountDTO.isDiscountExist() && ChargeCodes.FARE_DISCOUNT.equals(discountCode)) {
					discountPercentage = reservationDiscountDTO.getDiscountPercentage();
				}
			}

			if (ReservationApiUtils.isInfantType(reservationPax)) {
				totalPaxInfantCount = totalPaxInfantCount + 1;
				addOndsForInfant(reservationPax, colOndFareDTOs, segmentBucket, segObjectsMap, credentialsDTO, isCreditPromotion,
						paxDiscountDetailTO, discountCode, discountPercentage);
			} else if (ReservationApiUtils.isChildType(reservationPax)) {
				totalPaxChildCount = totalPaxChildCount + 1;
				addOndsForChild(reservationPax, colOndFareDTOs, segmentBucket, segObjectsMap, credentialsDTO, isCreditPromotion,
						paxDiscountDetailTO, discountCode, discountPercentage);
			} else if (ReservationApiUtils.isAdultType(reservationPax)) {
				totalPaxAdultCount = totalPaxAdultCount + 1;
				addOndsForAdult(reservationPax, colOndFareDTOs, segmentBucket, segObjectsMap, credentialsDTO, isCreditPromotion,
						paxDiscountDetailTO, discountCode, discountPercentage);
			}

			// Set reservation total charge amounts
			ReservationCoreUtils.updateTotals(reservation, reservationPax.getTotalChargeAmounts(), true);
		}

		Map<Integer, Map<Integer, Collection<PaxSegmentSSRDTO>>> updatePaxSegmentSSRDTOMaps = ReservationSSRUtil
				.updateSSRBookedSegmentSequence(colOndFareDTOs, reservation.getSegments(), paxSegmentSSRDTOMaps);

		// adds ssrs to paxs
		ReservationSSRUtil.addPaxSegmentSSRS(reservation, updatePaxSegmentSSRDTOMaps);

		// Set the number of passenger and infant counts
		reservation.setTotalPaxAdultCount(totalPaxAdultCount);
		reservation.setTotalPaxChildCount(totalPaxChildCount);
		reservation.setTotalPaxInfantCount(totalPaxInfantCount);

		// Set Promotion details
		if (reservationDiscountDTO != null && reservationDiscountDTO.getTotalDiscount().doubleValue() > 0) {
			if (reservationDiscountDTO.getPromotionId() != null) {
				reservation.setPromotionId(reservationDiscountDTO.getPromotionId());
				reservation.setPromotionPaxCount(reservationDiscountDTO.getPromoPaxCount());
			}
		}

		// Tracks the parent and infant mappings
		this.trackParentInfantMappings(reservation, allPassengers);

		// Track any invalid infant mappings
		this.trackInvalidInfantMappings(reservation);

		return reservation;
	}

	/**
	 * Track any invalid infant mapping
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private void trackInvalidInfantMappings(Reservation reservation) throws ModuleException {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax reservationPax;

		while (itReservationPax.hasNext()) {
			reservationPax = (ReservationPax) itReservationPax.next();

			int size = reservationPax.getInfants() == null ? 0 : reservationPax.getInfants().size();
			// If it's not an infant
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				if (ReservationApiUtils.isParentBeforeSave(reservationPax)) {
					if (size > 1) {
						throw new ModuleException("airreservations.arg.invalidInfantAssign");
					}
				} else {
					if (size > 0) {
						throw new ModuleException("airreservations.arg.invalidInfantAssign");
					}
				}
			} else {
				if (size > 0) {
					throw new ModuleException("airreservations.arg.invalidInfantAssign");
				}
			}
		}
	}

	/**
	 * Add the OND(s) for infant
	 * 
	 * @param reservationPax
	 * @param colOndFareDTOs
	 * @param segmentBucket
	 * @param segObjectsMap
	 * @param isCreditPromotion
	 * @param paxDiscountDetailTO
	 * @param discountCode
	 * @param discountPercentage
	 * @throws ModuleException
	 */
	private void addOndsForInfant(ReservationPax reservationPax, Collection<OndFareDTO> colOndFareDTOs,
			SegmentBucketBO segmentBucket, Map<Integer, ReservationSegment> segObjectsMap, CredentialsDTO credentialsDTO,
			boolean isCreditPromotion, PaxDiscountDetailTO paxDiscountDetailTO, String discountCode, double discountPercentage)
			throws ModuleException {
		Iterator<OndFareDTO> itOndFares = colOndFareDTOs.iterator();
		ReservationPaxFare reservationPaxFare;
		OndFareDTO ondFareDTO;

		while (itOndFares.hasNext()) {
			ondFareDTO = (OndFareDTO) itOndFares.next();
			reservationPaxFare = new ReservationPaxFare();

			Collection<DiscountChargeTO> discountChargeTOs = null;
			if (paxDiscountDetailTO != null) {
				discountChargeTOs = paxDiscountDetailTO.getDiscountChargeTOsByFltSegId(ondFareDTO.getFlightSegmentIds());
			}

			PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getPaxDiscountInfoDTO(isCreditPromotion, discountChargeTOs,
					ondFareDTO.getFareId(), discountCode, discountPercentage, ReservationInternalConstants.ChargeGroup.FAR,
					AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()));

			if (ReservationApiUtils.isFareExist(ondFareDTO.getInfantFare())) {
				reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));
				ReservationCoreUtils.captureReservationPaxOndCharge(
						AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()), new Integer(ondFareDTO.getFareId()),
						null, ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false,
						paxDiscInfo, null, null, 0);
			}

			// Updates reservation passenger fare charges
			this.setPerOndFareCharges(reservationPaxFare, ondFareDTO, PassengerType.INFANT, credentialsDTO,
					reservationPax.getPaxSequence(), isCreditPromotion, discountChargeTOs, discountCode);

			// Set passenger totals
			ReservationCoreUtils.updateTotals(reservationPax, reservationPaxFare.getTotalChargeAmounts(), true);

			// Updates reservation passenger fare segment and charges
			this.setPerOndFareSegments(reservationPaxFare, ondFareDTO, segmentBucket, segObjectsMap, false);

			// Set pnr pax fares
			reservationPax.addPnrPaxFare(reservationPaxFare);
		}
	}

	/**
	 * Add the OND(s)
	 * 
	 * @param reservationPax
	 * @param colOndFareDTOs
	 * @param segmentBucket
	 * @param segObjectsMap
	 * @param isCreditPromotion
	 * @param paxDiscountDetailTO
	 * @param discountCode
	 * @param discountPercentage
	 * @throws ModuleException
	 */
	private void addOndsForChild(ReservationPax reservationPax, Collection<OndFareDTO> colOndFareDTOs,
			SegmentBucketBO segmentBucket, Map<Integer, ReservationSegment> segObjectsMap, CredentialsDTO credentialsDTO,
			boolean isCreditPromotion, PaxDiscountDetailTO paxDiscountDetailTO, String discountCode, double discountPercentage)
			throws ModuleException {
		Iterator<OndFareDTO> itOndFares = colOndFareDTOs.iterator();
		ReservationPaxFare reservationPaxFare;
		OndFareDTO ondFareDTO;

		while (itOndFares.hasNext()) {
			ondFareDTO = (OndFareDTO) itOndFares.next();
			reservationPaxFare = new ReservationPaxFare();

			Collection<DiscountChargeTO> discountChargeTOs = null;
			if (paxDiscountDetailTO != null) {
				discountChargeTOs = paxDiscountDetailTO.getDiscountChargeTOsByFltSegId(ondFareDTO.getFlightSegmentIds());
			}

			PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getPaxDiscountInfoDTO(isCreditPromotion, discountChargeTOs,
					ondFareDTO.getFareId(), discountCode, discountPercentage, ReservationInternalConstants.ChargeGroup.FAR,
					AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()));

			// Set the fare Id
			if (ReservationApiUtils.isFareExist(ondFareDTO.getChildFare())) {
				reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));
				ReservationCoreUtils.captureReservationPaxOndCharge(
						AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()), new Integer(ondFareDTO.getFareId()), null,
						ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false, paxDiscInfo,
						null, null, 0);
			}

			// Updates reservation passenger fare charges
			this.setPerOndFareCharges(reservationPaxFare, ondFareDTO, PassengerType.CHILD, credentialsDTO,
					reservationPax.getPaxSequence(), isCreditPromotion, discountChargeTOs, discountCode);

			// Set passenger totals
			ReservationCoreUtils.updateTotals(reservationPax, reservationPaxFare.getTotalChargeAmounts(), true);

			// Updates reservation passenger fare segment and charges
			this.setPerOndFareSegments(reservationPaxFare, ondFareDTO, segmentBucket, segObjectsMap, true);

			// Set pnr pax fares
			reservationPax.addPnrPaxFare(reservationPaxFare);
		}
	}

	/**
	 * Add the OND(s)
	 * 
	 * @param reservationPax
	 * @param colOndFareDTOs
	 * @param segmentBucket
	 * @param segObjectsMap
	 * @param isCreditPromotion
	 * @param paxDiscountDetailTO
	 * @param discountCode
	 * @param discountPercentage
	 * @throws ModuleException
	 */
	private void addOndsForAdult(ReservationPax reservationPax, Collection<OndFareDTO> colOndFareDTOs,
			SegmentBucketBO segmentBucket, Map<Integer, ReservationSegment> segObjectsMap, CredentialsDTO credentialsDTO,
			boolean isCreditPromotion, PaxDiscountDetailTO paxDiscountDetailTO, String discountCode, double discountPercentage)
			throws ModuleException {
		Iterator<OndFareDTO> itOndFares = colOndFareDTOs.iterator();
		ReservationPaxFare reservationPaxFare;
		OndFareDTO ondFareDTO;

		while (itOndFares.hasNext()) {
			ondFareDTO = (OndFareDTO) itOndFares.next();
			reservationPaxFare = new ReservationPaxFare();

			Collection<DiscountChargeTO> discountChargeTOs = null;
			if (paxDiscountDetailTO != null) {
				discountChargeTOs = paxDiscountDetailTO.getDiscountChargeTOsByFltSegId(ondFareDTO.getFlightSegmentIds());
			}

			PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getPaxDiscountInfoDTO(isCreditPromotion, discountChargeTOs,
					ondFareDTO.getFareId(), discountCode, discountPercentage, ReservationInternalConstants.ChargeGroup.FAR,
					AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()));

			// Set the fare Id
			if (ReservationApiUtils.isFareExist(ondFareDTO.getAdultFare())) {
				reservationPaxFare.setFareId(new Integer(ondFareDTO.getFareId()));
				ReservationCoreUtils.captureReservationPaxOndCharge(
						AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()), new Integer(ondFareDTO.getFareId()), null,
						ReservationInternalConstants.ChargeGroup.FAR, reservationPaxFare, credentialsDTO, false, paxDiscInfo,
						null, null, 0);
			}

			// Updates reservation passenger fare charges
			this.setPerOndFareCharges(reservationPaxFare, ondFareDTO, PassengerType.ADULT, credentialsDTO,
					reservationPax.getPaxSequence(), isCreditPromotion, discountChargeTOs, discountCode);

			// Set passenger totals
			ReservationCoreUtils.updateTotals(reservationPax, reservationPaxFare.getTotalChargeAmounts(), true);

			// Updates reservation passenger fare segment and charges
			this.setPerOndFareSegments(reservationPaxFare, ondFareDTO, segmentBucket, segObjectsMap, true);

			// Set pnr pax fares
			reservationPax.addPnrPaxFare(reservationPaxFare);
		}
	}

	/**
	 * Correcting infant/parent mapping and assigning parent charges
	 * 
	 * @param reservation
	 * @param allPassengers
	 */
	private void trackParentInfantMappings(Reservation reservation, Map<Integer, ReservationPax> allPassengers) {
		Iterator<ReservationPax> itReservationPax = reservation.getPassengers().iterator();
		ReservationPax currentPax;
		ReservationPax mappingPax;
		Integer parentOrInfantId;
		PaymentAssembler paymentAssembler;

		while (itReservationPax.hasNext()) {
			currentPax = (ReservationPax) itReservationPax.next();

			// Carrying out the mapping
			// Parent
			if (ReservationApiUtils.isParentBeforeSave(currentPax)) {
				parentOrInfantId = currentPax.getIndexId();
				mappingPax = (ReservationPax) allPassengers.get(parentOrInfantId);

				currentPax.addInfants(mappingPax);
			}
			// Infant
			else if (ReservationApiUtils.isInfantType(currentPax)) {
				parentOrInfantId = currentPax.getIndexId();
				mappingPax = (ReservationPax) allPassengers.get(parentOrInfantId);

				currentPax.addParent(mappingPax);
			}

			// Setting up the total paid amount for each passenger
			paymentAssembler = (PaymentAssembler) currentPax.getPayment();

			// Only if the payment exist we are setting the payments
			if (paymentAssembler.isPaymentExist()) {
				currentPax.setTotalPaidAmount(paymentAssembler.getTotalPayAmount());
			} else {
				currentPax.setTotalPaidAmount(null);
			}
		}
	}

	/**
	 * Setting Fare Charges
	 * 
	 * @param reservationPaxFare
	 * @param ondFareDTO
	 * @param passengerType
	 * @param paxSequence
	 *            TODO
	 * @param isCreditPromotion
	 *            TODO
	 * @param discountChargeTOs
	 * @param discountCode
	 * @throws ModuleException
	 */
	private void setPerOndFareCharges(ReservationPaxFare reservationPaxFare, OndFareDTO ondFareDTO, String passengerType,
			CredentialsDTO credentialsDTO, Integer paxSequence, boolean isCreditPromotion,
			Collection<DiscountChargeTO> discountChargeTOs, String discountCode) throws ModuleException {

		// Setting charges information
		if (ondFareDTO.getAllCharges() != null) {
			Iterator<QuotedChargeDTO> itCharges = ondFareDTO.getAllCharges().iterator();
			QuotedChargeDTO quotedChargeDTO;

			while (itCharges.hasNext()) {
				quotedChargeDTO = (QuotedChargeDTO) itCharges.next();

				PaxDiscountInfoDTO paxDiscInfo = ReservationApiUtils.getPaxDiscountInfoDTO(isCreditPromotion, discountChargeTOs,
						quotedChargeDTO.getChargeRateId(), discountCode, 0, quotedChargeDTO.getChargeCode(),
						AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(passengerType)));

				if (PassengerType.ADULT.equals(passengerType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {

						ReservationCoreUtils.captureReservationPaxOndCharge(
								AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT)),
								null, new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
								reservationPaxFare, credentialsDTO, false, paxDiscInfo, null, null, 0);
					}
				} else if (PassengerType.CHILD.equals(passengerType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_CHILD_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_CHILD) {
						ReservationCoreUtils.captureReservationPaxOndCharge(
								AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD)),
								null, new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
								reservationPaxFare, credentialsDTO, false, paxDiscInfo, null, null, 0);
					}
				} else if (PassengerType.INFANT.equals(passengerType)) {
					if (quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_INFANT_ONLY
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ALL
							|| quotedChargeDTO.getApplicableTo() == Charge.APPLICABLE_TO_ADULT_INFANT) {
						ReservationCoreUtils.captureReservationPaxOndCharge(
								AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)),
								null, new Integer(quotedChargeDTO.getChargeRateId()), quotedChargeDTO.getChargeGroupCode(),
								reservationPaxFare, credentialsDTO, false, paxDiscInfo, null, null, 0);
					}
				}
				// This can not happen. Precautionary measure
				else {
					throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
				}
			}
		}
	}

	/**
	 * Set Fare Segments
	 * 
	 * @param reservationPaxFare
	 * @param pnrFareDTO
	 * @param segmentBucket
	 * @param segObjectsMap
	 * @param applyBookingClass
	 * @throws ModuleException
	 */
	private void setPerOndFareSegments(ReservationPaxFare reservationPaxFare, OndFareDTO pnrFareDTO,
			SegmentBucketBO segmentBucket, Map<Integer, ReservationSegment> segObjectsMap, boolean applyBookingClass)
			throws ModuleException {

		Iterator<SegmentSeatDistsDTO> itSegAva = pnrFareDTO.getSegmentSeatDistsDTO().iterator();

		SegmentSeatDistsDTO segmentSeatDistsDTO;
		ReservationPaxFareSegment reservationPaxFareSegment;

		while (itSegAva.hasNext()) {
			segmentSeatDistsDTO = (SegmentSeatDistsDTO) itSegAva.next();
			reservationPaxFareSegment = new ReservationPaxFareSegment();

			// Set the Reservation Segment for relationship tracking
			reservationPaxFareSegment
					.setSegment((ReservationSegment) segObjectsMap.get(new Integer(segmentSeatDistsDTO.getFlightSegId())));
			if (segmentSeatDistsDTO.getSeatDistribution() != null && segmentSeatDistsDTO.getSeatDistribution().size() > 1) {
				reservationPaxFareSegment.setBcNested(ReservationInternalConstants.BCNested.YES);
			} else {
				reservationPaxFareSegment.setBcNested(ReservationInternalConstants.BCNested.NO);
			}

			if (applyBookingClass) {
				// Releasing from the bucket
				reservationPaxFareSegment.setBookingCode(segmentBucket.getBookingCode(segmentSeatDistsDTO.getFlightSegId()));
			}

			reservationPaxFare.addPaxFareSegment(reservationPaxFareSegment);
		}
	}

	/**
	 * Validating the parameters
	 * 
	 * @param reservation
	 * @param colOndFareDTOs
	 * @param segObjectsMap
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void validateParams(Reservation reservation, Collection colOndFareDTOs, Map segObjectsMap,
			CredentialsDTO credentialsDTO) throws ModuleException {
		log.debug("Inside validateParams");

		if (reservation.getContactInfo() == null || reservation.getSegments() == null || reservation.getSegments().size() == 0
				|| reservation.getPassengers() == null || reservation.getPassengers().size() == 0 || colOndFareDTOs == null
				|| colOndFareDTOs.size() == 0 || segObjectsMap == null || credentialsDTO == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		log.debug("Exit validateParams");
	}

}
