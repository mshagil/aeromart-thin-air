/**
 * 
 */
package com.isa.thinair.airreservation.core.bl.messaging.pnladl.datafactory;

import com.isa.thinair.airreservation.api.dto.pnl.BasePassengerCollection;
import com.isa.thinair.airreservation.api.dto.pnl.PassengerCollection;
import com.isa.thinair.airreservation.api.utils.PNLConstants.MessageTypes;
import com.isa.thinair.airreservation.core.bl.adl.datastructure.AdlMasterDataStructure;
import com.isa.thinair.airreservation.core.bl.messaging.palcal.dataContext.PalDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.AdlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.BaseDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datacontext.PnlDataContext;
import com.isa.thinair.airreservation.core.bl.messaging.pnladl.datastructure.MessageDataStructure;
import com.isa.thinair.airreservation.core.bl.pal.dataStructure.masterinformation.CalMasterDataStructure;
import com.isa.thinair.airreservation.core.bl.pal.dataStructure.masterinformation.PalMasterDataStructure;
import com.isa.thinair.airreservation.core.bl.pnl.datastructure.masterinformation.PnlMasterDataStructure;

/**
 * @author udithad
 *
 */
public class PnlAdlMasterDataFactory<K extends BasePassengerCollection, T extends BaseDataContext> extends
		PnlAdlAbstractDataFactory<BasePassengerCollection, BaseDataContext> {

	@Override
	public MessageDataStructure createDataStructure(String messageType) {
		MessageDataStructure messageDataStructure = null;
		if (messageType.equals(MessageTypes.PNL)) {
			messageDataStructure = new PnlMasterDataStructure<PassengerCollection, PnlDataContext>();
		} else if (messageType.equals(MessageTypes.ADL)) {
			messageDataStructure = new AdlMasterDataStructure<PassengerCollection, AdlDataContext>();
		} else if (messageType.equals(MessageTypes.PAL)) {
			messageDataStructure = new PalMasterDataStructure<PassengerCollection, PalDataContext>();
		} else if (messageType.equals(MessageTypes.CAL)) {
			messageDataStructure = new CalMasterDataStructure<PassengerCollection, PalDataContext>();
		}
		return messageDataStructure;
	}

}
