/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.dto.FlightManifestDTO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestReservationAuxilliaryDAOImpl extends PlatformTestCase {
	
//	/**
//	 * 
//	 * @throws Exception .
//	 * @see junit.framework.TestCase#setUp()
//	 */
//	protected void setUp() throws Exception {
//		super.setUp();
//	}
//
//	/**
//	 * 
//	 * @throws Exception .
//	 * @see junit.framework.TestCase#tearDown()
//	 */
//	protected void tearDown() throws Exception {
//		super.tearDown();
//	}
//	
//	public void testGetFlightManifest()throws ModuleException{
//		ReservationAuxilliaryDAO reservationAuxilliaryDAO =  getReservationAuxilliaryDAO();
//		Collection flightManifestDTOs = reservationAuxilliaryDAO.getFlightManifest(30559);
//		System.out.println("-------FlightManifest---------");
//		for (Iterator iter = flightManifestDTOs.iterator(); iter.hasNext();) {
//			FlightManifestDTO flightManifestDTO = (FlightManifestDTO) iter.next();
//			System.out.println("flightManifestDTO.getAgency() : " + flightManifestDTO.getAgency());
//			System.out.println("flightManifestDTO.getBalance() : " + flightManifestDTO.getBalance());
//			System.out.println("flightManifestDTO.getBookingClass() : " + flightManifestDTO.getBookingClass());
//			System.out.println("flightManifestDTO.getCurrency() : " + flightManifestDTO.getCurrency());
//			System.out.println("flightManifestDTO.getOrigin() : " + flightManifestDTO.getOrigin());
//			System.out.println("flightManifestDTO.getPassengerName() : " + flightManifestDTO.getPassengerName());
//			System.out.println("flightManifestDTO.getPaxStatus() : " + flightManifestDTO.getPaxStatus());
//			System.out.println("flightManifestDTO.getPnrId() : " + flightManifestDTO.getPnrId());
//			System.out.println("flightManifestDTO.getReservationStatus() : " + flightManifestDTO.getReservationStatus());
//			System.out.println("flightManifestDTO.getSegmentCode() : " + flightManifestDTO.getSegmentCode());
//			System.out.println("flightManifestDTO.getSsr() : " + flightManifestDTO.getSsr());
//			
//			System.out.println("flightManifestDTO.getPassengerType() : " + flightManifestDTO.getPassengerType());
//			System.out.println("flightManifestDTO.getPassengerID() : " + flightManifestDTO.getPassengerID());
//			System.out.println("flightManifestDTO.getAdultPassengerID() : " + flightManifestDTO.getAdultPassengerID());
//		}		
//		//System.out.println("--- Origin of -----------" + "PNR: YMK2UA    Departure Station: SHJ/DOH    FlightID: 755 IS : " + originStationID);		
//	}
//	
////	public void testGetOrigin()throws ModuleException{
////		ReservationAuxilliaryDAO reservationAuxilliaryDAO =  getReservationAuxilliaryDAO();
////		String originStationID = reservationAuxilliaryDAO.getPNROriginStationForFlight("YMK2UA","SHJ/DOH", 755);
////		System.out.println("--- Origin of -----------" + "PNR: YMK2UA    Departure Station: SHJ/DOH    FlightID: 755 IS : " + originStationID);		
////	}
//	
////	public void testGetInBoundConnectionList()throws ModuleException{
////		ReservationAuxilliaryDAO reservationAuxilliaryDAO =  getReservationAuxilliaryDAO();
////		InboundConnectionDTO inboundConnectionDTO = reservationAuxilliaryDAO.getInBoundConnectionList("YMK2UA",0,"SHJ", 5352);
////		System.out.println("--- InboundConnectionDTO -----------" + "PNR: YMK2UA    Departure Station: SHJ    FlightID: 5352");
////		printInboundConnectionDTO( inboundConnectionDTO);
////	}
////	
////	private void printInboundConnectionDTO(InboundConnectionDTO inboundConnectionDTO){
////		System.out.println("Flight Number : " + inboundConnectionDTO.getFlightNumber());
////		System.out.println("FareClass : " + inboundConnectionDTO.getFareClass());
////		System.out.println("Date : " + inboundConnectionDTO.getDate());
////		System.out.println("DepartureStation : " + inboundConnectionDTO.getDepartureStation());
////	}
////	
////	public void testGetAllOutBoundSequences()throws ModuleException{
////		ReservationAuxilliaryDAO reservationAuxilliaryDAO =  getReservationAuxilliaryDAO();
////		Collection onWardConnectionDTOs = reservationAuxilliaryDAO.getAllOutBoundSequences("YMK2UA",0,"SHJ", 5352);
////		System.out.println("--- onWardConnectionDTOs -----------" + "PNR: YMK2UA    Departure Station: CMB    FlightID: 5352");
////		printOnWardConnectionDTOs( onWardConnectionDTOs);
////	}
////	
////	private void printOnWardConnectionDTOs(Collection onWardConnectionDTOs){
////		for (Iterator iterOnWardConnectionDTOs = onWardConnectionDTOs.iterator(); iterOnWardConnectionDTOs.hasNext();) {
////			OnWardConnectionDTO onWardConnectionDTO = (OnWardConnectionDTO) iterOnWardConnectionDTOs.next();
////			System.out.println("Flight Number : " + onWardConnectionDTO.getFlight());
////			System.out.println("FareClass : " + onWardConnectionDTO.getFareclass());
////			System.out.println("Date : " + onWardConnectionDTO.getDay());
////			System.out.println("Arrival Point : " + onWardConnectionDTO.getArrivalpoint());
////			System.out.println("Boardingpoint : " + onWardConnectionDTO.getBoardingpoint());
////			
//////			dto.setFlight(flightNumber);
//////			dto.setBoardingpoint(board);
//////			dto.setFareclass(fareClass);
//////			dto.setDay(dd);
//////			dto.setArrivalpoint(arrival);
////		}		
////	}
//	
//    /*public void testGetCreditCardPaymentsOld() throws Exception {
//        System.out.println("Start..................................");
//        
//        Date today = new Date();
//        Date date = new Date(today.getTime() - 1000 * 5);
//        SimpleDateFormat dateFormat = null;
//
//        dateFormat = new SimpleDateFormat("dd-MM-yy");
//
//        int daynumber = date.getDate();
//        int monthNumber = date.getMonth() + 1;
//        int yearNumber = date.getYear() + 1900;
//
//        Date fdate = dateFormat.parse(daynumber + "-" + monthNumber
//                + "-" + yearNumber);
//        
//        getReservationAuxilliaryDAO().getCreditCardPaymentsOld(fdate);
//        System.out.println("End....................................");
//    }
//    
//    public void testGetCashPaymentsOld() throws Exception {
//        Date today=new Date();
//        Date date=new Date(today.getTime()-1000*5);
//        SimpleDateFormat dateFormat = null;
//
//        
//        dateFormat = new SimpleDateFormat("dd-MM-yy");
//        
//        int daynumber=date.getDate();
//        int monthNumber=date.getMonth()+1;
//        int yearNumber=date.getYear()+1900;
//        
//        Date fdate= dateFormat.parse(daynumber+"-"+monthNumber+"-"+yearNumber);
//        
//        getReservationAuxilliaryDAO().getCashPaymentsOld(fdate);
//    }
//    */
//    
//    public void testGetCreditCardPayments() throws Exception {
//        System.out.println("Start..................................");
//        
//        Date today = new Date();
//        Date date = new Date(today.getTime() - 1000 * 5);
//        SimpleDateFormat dateFormat = null;
//
//        dateFormat = new SimpleDateFormat("dd-MM-yy");
//
//        int daynumber = date.getDate();
//        int monthNumber = date.getMonth() + 1;
//        int yearNumber = date.getYear() + 1900;
//
//        Date fdate = dateFormat.parse(25 + "-" + 5
//                + "-" + 2006);
//        
//        getReservationAuxilliaryDAO().getCreditCardPayments(fdate);
//        
//        System.out.println("End....................................");
//    }
//
//    public void testGetCashPayments() throws Exception {
//        Date today=new Date();
//        Date date=new Date(today.getTime()-1000*5);
//        SimpleDateFormat dateFormat = null;
//
//        
//        dateFormat = new SimpleDateFormat("dd-MM-yy");
//        
//        int daynumber=date.getDate();
//        int monthNumber=date.getMonth()+1;
//        int yearNumber=date.getYear()+1900;
//        
//        Date fdate= dateFormat.parse(25+"-"+5+"-"+2006);
//        
//        getReservationAuxilliaryDAO().getCashPayments(fdate);
//    }
//    
//    public void testGetManualCreditCardPayments() throws Exception {
//        Date today=new Date();
//        Date date=new Date(today.getTime()-1000*5);
//        SimpleDateFormat dateFormat = null;
//
//        
//        dateFormat = new SimpleDateFormat("dd-MM-yy");
//        
//        int daynumber=date.getDate();
//        int monthNumber=date.getMonth()+1;
//        int yearNumber=date.getYear()+1900;
//        
//        Date fdate= dateFormat.parse(25+"-"+5+"-"+2006);
//        
//        int cardTypeId = 102607;
//        getReservationAuxilliaryDAO().getManualCreditCardPayments(fdate, cardTypeId);
//    }
//    
//    public void testGetCashPaymentsPeragent() throws Exception {
//        Date today=new Date();
//        Date date=new Date(today.getTime()-1000*5);
//        SimpleDateFormat dateFormat = null;
//
//        
//        dateFormat = new SimpleDateFormat("dd-MM-yy");
//        
//        int daynumber=date.getDate();
//        int monthNumber=date.getMonth()+1;
//        int yearNumber=date.getYear()+1900;
//        
//        Date fdate= dateFormat.parse(25+"-"+5+"-"+2006);
//        
//        String agentCode = "SHJ001"; // SHJ53
//        getReservationAuxilliaryDAO().getCashPaymentsPeragent(fdate, agentCode);
//    }
//  
    public void testGetPNL() {
        //Collection col = getReservationAuxilliaryDAO().getPNL(327, "MLE", null, null, null);
        //Collection col = getReservationAuxilliaryDAO().getPNL(20, "MLE", null, null, null);
    }
    
    public void testGetADL() {
        GregorianCalendar g = new GregorianCalendar();
        g.set(2007, 07, 03);
        Timestamp ts = new Timestamp(g.getTimeInMillis());
        //Collection col = getReservationAuxilliaryDAO().getADL(20, "MLE", ts);
    }
    
    private ReservationAuxilliaryDAO getReservationAuxilliaryDAO() {
		return null;
        //return ReservationModuleUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;        
    }
}