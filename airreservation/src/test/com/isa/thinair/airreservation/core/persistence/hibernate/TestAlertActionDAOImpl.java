package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airreservation.api.model.AlertAction;
import com.isa.thinair.airreservation.core.persistence.dao.AlertActionDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestAlertActionDAOImpl extends PlatformTestCase {

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testSave()throws ModuleException{
		
		AlertAction action = new AlertAction();
		action.setActionCode("AAA");
		action.setActionNote("SDFSD");
		getDAO().saveAlertAction(action);
		assertNotNull("Not Saved", getDAO().getAlertAction("AAA"));
		
	}

	public void testLoadAndSave()throws ModuleException{
		
		AlertAction action = getDAO().getAlertAction("AAA");
		String oldval = action.getActionNote();
		action.setActionNote("SDFSD new");
		getDAO().saveAlertAction(action);
		assertNotNull("Not Saved", getDAO().getAlertAction("AAA"));
		assertNotSame(oldval, action.getActionNote());
	}

	
	public void testGetCollection() throws ModuleException{
		Collection c = getDAO().getAlertActions();
		assertTrue("no recs", c.size()>0);
	}
	
	public void testGetPagedData() throws ModuleException{
		
		List criteriaList = new ArrayList();
		ModuleCriterion criterion = new ModuleCriterion();
		criterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterion.setFieldName("actionCode");
		List values = new ArrayList();
		values.add("AAA");
		criterion.setValue(values);
		criteriaList.add(criterion);
		Page p  = getDAO().getPagedAlertActionData(criteriaList, 0, 100, null);
		assertTrue("no recs", p.getPageData().size()>0);
	}
	
	public void testDelete()throws ModuleException{
		
		getDAO().deleteAlertAction("AAA");
		assertNull("Not deleted", getDAO().getAlertAction("AAA"));
		
	}
	  
	
	
	private AlertActionDAO getDAO() {
		return null;
	        //return ReservationModuleUtils.DAOInstance.RESERVATION_ALERT_ACTION_DAO;        
	    }
	
	 
}
