/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airreservation.core.persistence.hibernate;

import java.util.Collection;

import com.isa.thinair.airreservation.api.dto.pnl.PNLMetaDataDTO;
import com.isa.thinair.airreservation.core.bl.ReservationAuxiliaryBL;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationAuxilliaryDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationTnxDAO;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Chamindap
 *
 */
public class TestReservationTransactionDAOImpl extends PlatformTestCase {

	ReservationTnxDAO reservationTnxDAO = null;	
	public static String RESERVATION_TNX_DAO = "ReservationTnxDAO";
	public static String modulename = "airreservation";
	
	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		
		//reservationTnxDAO = ReservationModuleUtils.DAOInstance.RESERVATION_TNX_DAO;
	}

	/*public void testGetPNRPAXPayments() throws Exception
	{
		Collection c = reservationTnxDAO.getPNRPaxPayments("47");
		Iterator iterator = c.iterator();
		while (iterator.hasNext()) {
			ReservationTnx element = (ReservationTnx) iterator.next();
			System.out.println(element.toString());
		}
	}
	*/
	
	private ReservationAuxilliaryDAO getReservationAuxilliaryDAO() {
		ReservationAuxilliaryDAO reservationAuxilliaryDAO = null;
		//reservationAuxilliaryDAO = ReservationModuleUtils.DAOInstance.RESERVATION_AUXILLIARY_DAO;
		return reservationAuxilliaryDAO;
	}
//	public void testPnl()  throws Exception
//	{
//		ReservationAuxiliaryBL bl=new  ReservationAuxiliaryBL();
//	Collection col=	getReservationAuxilliaryDAO().getPNL(142,"SHJ",null,null,"");
//	GeneratePnl p=new GeneratePnl();
//	
//	PNLMetaDataDTO pnlmetadatadto = new PNLMetaDataDTO();
//	pnlmetadatadto.setBoardingairport("SHJ");
//	pnlmetadatadto.setDay(String.valueOf(4));
//	String fnumber = getReservationAuxilliaryDAO()
//			.getFlightNumberForManualPNL(142);
//
//	
//	pnlmetadatadto.setFlight(fnumber);// method to get flightnumber
//	pnlmetadatadto.setMessageIdentifier("PNL");
//	pnlmetadatadto.setMonth("MAR");
//	pnlmetadatadto.setPartnumber("PART1");
//	pnlmetadatadto.setPart2(true);
//	//bl.generatePNL(col,pnlmetadatadto);
//	p.generatePNL(col,pnlmetadatadto);
//	
//	
//	System.out.println("Collection size is...."+col.size());
//	}
	
	
	
	/**
	 * 
	 * @throws Exception .
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

//	/**
//	 * Test method for 'com.isa.thinair.airreservation.core.persistence
//	 * .hibernate.ReservationTransactionDAOImpl.setUnusedCreditExpired()'
//	 */
//	public void testSetUnusedCreditExpired() {
//		
//
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airreservation.core.persistence
//	 * .hibernate.ReservationTransactionDAOImpl.saveTransaction(ReservationTnx)'
//	 */
//	public void testSaveTransaction() {
//		
//
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airreservation.core.persistence
//	 * .hibernate.ReservationTransactionDAOImpl.updateTransaction(ReservationTnx)'
//	 */
//	public void testUpdateTransaction() {
//		
//
//	}

	/**
	 * Test method for 'com.isa.thinair.airreservation.core.persistence
	 * .hibernate.ReservationTransactionDAOImpl.getCashPayments()'
	 */
//	public void testGetCashPayments() {
//		Collection cashPaymentDTO = null;
//
//		assertNotNull(reservationTnxDAO);
//		
//		cashPaymentDTO = reservationTnxDAO.getCashPayments();
//		
//		assertEquals(" Incorrect Size ", 2, cashPaymentDTO.size());
//
//		Iterator iter = cashPaymentDTO.iterator();
//		
//		while (iter.hasNext()) {
//			CashPaymentDTO element = (CashPaymentDTO) iter.next();
//			
//			if (element.getStaffId().equals("T2")) {
//				assertEquals(" Incorrect Staff id ", "T2", 
//						element.getStaffId());
//				assertEquals(" Incorrect date of sales ", 
//						new GregorianCalendar(2005, 9, 11)
//						.getTime(), element.getDateOfSale());
//				assertEquals(" Incorrect total sales amount ", 14, 
//						element.getTotalSales(), 1);				
//			} else {
//				assertEquals(" Incorrect Staff id ", "U001", 
//						element.getStaffId());
//				assertEquals(" Incorrect date of sales ", 
//						new GregorianCalendar(2005, 9, 11)
//						.getTime(), element.getDateOfSale());
//				assertEquals(" Incorrect total sales amount ", 7, 
//						element.getTotalSales(), 1);				
//			}			
//		}		
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airreservation.core.persistence
//	 * .hibernate.ReservationTransactionDAOImpl.getCreditCardPayments()'
//	 */
//	public void testGetCreditCardPayments() {
//		Collection creditPaymentDTO = null;
//
//		assertNotNull(reservationTnxDAO);
//		
//		creditPaymentDTO = reservationTnxDAO.getCreditCardPayments();
//		
//		
//		assertEquals(" Incorrect Size ", 2, creditPaymentDTO.size());
//
//		Iterator iter = creditPaymentDTO.iterator();
//		
//		while (iter.hasNext()) {
//			CreditCardPaymentDTO element = (CreditCardPaymentDTO) iter.next();
//			
//			if (element.getCardType().equals("VISA")) {
//				assertEquals(" Incorrect card type ", "VISA", 
//						element.getCardType());
//				assertEquals(" Incorrect date of sales ", 
//						new GregorianCalendar(2005, 9, 11)
//						.getTime(), element.getDateOfSale());
//				assertEquals(" Incorrect total sales amount ", 5, 
//						element.getTotalSales(), 1);	
//			} else {
//				assertEquals(" Incorrect card type ", "MASTER", 
//						element.getCardType());
//				assertEquals(" Incorrect date of sales ", 
//						new GregorianCalendar(2005, 9, 11)
//						.getTime(), element.getDateOfSale());
//				assertEquals(" Incorrect total sales amount ", 5, 
//						element.getTotalSales(), 1);	
//			}			
//		}
//	}
//
//	/**
//	 * Test method for 'com.isa.thinair.airreservation.core.persistence
//	 * .hibernate.ReservationTransactionDAOImpl.getAgentTotalSales(Date, Date)'
//	 */
//	public void testGetAgentTotalSales() {
//		Collection agentTotalSaledDTO = null;
//		Date startDate = null;
//		Date endDate = null;
//		
//		startDate = new GregorianCalendar(2005, 8, 9).getTime();		
//		endDate = new GregorianCalendar(2005, 9, 12).getTime();
//		
//		assertNotNull(reservationTnxDAO);
//		
//		agentTotalSaledDTO = reservationTnxDAO.getAgentTotalSales(startDate,
//				endDate);
//		assertEquals(" Incorrect Size ", 1, agentTotalSaledDTO.size());
//		
//		Iterator iter = agentTotalSaledDTO.iterator();
//		
//		while (iter.hasNext()) {
//			AgentTotalSaledDTO element = (AgentTotalSaledDTO) iter.next();
//			
//			if (element.getAgentCode().equals("15")) {
//				assertEquals(" Incorrect agent code ", "15", 
//						element.getAgentCode());		
//				assertEquals(" Incorrect total sales amount ", 74, 
//						element.getTotalSales(), 1);	
//			}		
//		}
//	}
}