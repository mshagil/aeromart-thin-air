<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns0="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/tkcres"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="ns0 ns1 xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/03.1/COMMON_03.1_AA_TO_EDI.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <IATA>

            <xsl:for-each select="ns1:AA_TKCRES">

                <xsl:call-template name="Header">
                    <xsl:with-param name="elements" select="ns0:header"/>
                </xsl:call-template>

                <TKCRES>
                    <xsl:for-each select="ns1:message">

                        <xsl:call-template name="MessageHeader">
                            <xsl:with-param name="elements" select="ns0:messageHeader"/>
                        </xsl:call-template>

                        <xsl:call-template name="MessageFunction">
                            <xsl:with-param name="elements" select="ns1:messageFunction"/>
                        </xsl:call-template>

                        <xsl:call-template name="OriginatorInformation">
                            <xsl:with-param name="elements" select="ns1:originatorInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="TicketingAgentInformation">
                            <xsl:with-param name="elements" select="ns1:ticketingAgentInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="ReservationControlInformation">
                            <xsl:with-param name="elements" select="ns1:reservationControlInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="MonetaryInformation">
                            <xsl:with-param name="elements" select="ns1:monetaryInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="PricingTicketingDetails">
                            <xsl:with-param name="elements" select="ns1:pricingTicketingDetails"/>
                        </xsl:call-template>

                        <xsl:call-template name="OriginDestinationInformation">
                            <xsl:with-param name="elements" select="ns1:originDestinationInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="FormOfPayment">
                            <xsl:with-param name="elements" select="ns1:formOfPayment"/>
                        </xsl:call-template>

                        <xsl:call-template name="AdditionalTourInformation">
                            <xsl:with-param name="elements" select="ns1:additionalTourInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="NumberOfUnit">
                            <xsl:with-param name="elements" select="ns1:numberOfUnits"/>
                        </xsl:call-template>

                        <xsl:call-template name="TaxDetails">
                            <xsl:with-param name="elements" select="ns1:taxDetails"/>
                        </xsl:call-template>

                        <xsl:call-template name="ErrorInformation">
                            <xsl:with-param name="elements" select="ns1:errorInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="TicketCoupon">
                            <xsl:with-param name="elements" select="ns1:ticketCoupon"/>
                        </xsl:call-template>

                        <xsl:call-template name="FlightInformation">
                            <xsl:with-param name="elements" select="ns1:flightInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="DocumentInformationDetails">
                            <xsl:with-param name="elements" select="ns1:documentInfo"/>
                        </xsl:call-template>

                        <xsl:if test="ns1:ticketNumberDetails">
                            <xsl:for-each select="ns1:ticketNumberDetails">
                                <GROUP_1>

                                    <xsl:call-template name="TicketNumberDetails">
                                        <xsl:with-param name="elements" select="."/>
                                    </xsl:call-template>

                                    <xsl:call-template name="OriginatorInformation">
                                        <xsl:with-param name="elements" select="ns0:originatorInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="ErrorInformation">
                                        <xsl:with-param name="elements" select="ns0:errorInformation"/>
                                    </xsl:call-template>

                                    <xsl:for-each select="ns0:ticketCoupon">
                                        <GROUP_2>
                                            <xsl:call-template name="TicketCoupon">
                                                <xsl:with-param name="elements" select="."/>
                                            </xsl:call-template>

                                            <xsl:call-template name="ErrorInformation">
                                                <xsl:with-param name="elements" select="ns0:errorInformation"/>
                                            </xsl:call-template>
                                        </GROUP_2>
                                    </xsl:for-each>

                                </GROUP_1>
                            </xsl:for-each>
                        </xsl:if>

                        <xsl:if test="ns1:travellers">
                            <xsl:for-each select="ns1:travellers">
                                <GROUP_3>

                                    <xsl:call-template name="TravellerInformation">
                                        <xsl:with-param name="elements" select="."/>
                                    </xsl:call-template>

                                    <xsl:call-template name="TicketingAgentInformation">
                                        <xsl:with-param name="elements" select="ns0:ticketingAgentInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="ReservationControlInformation">
                                        <xsl:with-param name="elements" select="ns0:reservationControlInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="MonetaryInformation">
                                        <xsl:with-param name="elements" select="ns0:monetaryInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="FormOfPayment">
                                        <xsl:with-param name="elements" select="ns0:formOfPayment"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="PricingTicketingDetails">
                                        <xsl:with-param name="elements" select="ns0:pricingTicketingDetails"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="OriginDestinationInformation">
                                        <xsl:with-param name="elements" select="ns0:originDestinationInfo"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="FrequentTravellerInformation">
                                        <xsl:with-param name="elements" select="ns0:frequentTravellerInfo"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="AdditionalTourInformation">
                                        <xsl:with-param name="elements" select="ns0:additionalTourInfo"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="OriginatorInformation">
                                        <xsl:with-param name="elements" select="ns0:originatorInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="NumberOfUnit">
                                        <xsl:with-param name="elements" select="ns0:numberOfUnits"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="TaxDetails">
                                        <xsl:with-param name="elements" select="ns0:taxDetails"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="ErrorInformation">
                                        <xsl:with-param name="elements" select="ns0:errorInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="DocumentInformationDetails">
                                        <xsl:with-param name="elements" select="ns0:documentInfo"/>
                                    </xsl:call-template>

                                    <xsl:for-each select="ns0:tickets">
                                        <GROUP_4>

                                            <xsl:call-template name="TicketNumberDetails">
                                                <xsl:with-param name="elements" select="."/>
                                            </xsl:call-template>

                                            <xsl:call-template name="ErrorInformation">
                                                <xsl:with-param name="elements" select="ns0:errorInformation"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="OriginatorInformation">
                                                <xsl:with-param name="elements" select="ns0:originatorInformation"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="DateAndTimeInformation">
                                                <xsl:with-param name="elements" select="ns0:dateAndTime"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="ConsumerReferenceInformation">
                                                <xsl:with-param name="elements" select="ns0:consumerRef"/>
                                            </xsl:call-template>


                                            <xsl:for-each select="ns0:ticketCoupon">
                                                <GROUP_5>

                                                    <xsl:call-template name="TicketCoupon">
                                                        <xsl:with-param name="elements" select="."/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="FlightInformation">
                                                        <xsl:with-param name="elements" select="ns0:flightInformation"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="ReservationControlInformation">
                                                        <xsl:with-param name="elements" select="ns0:reservationControlInfo"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="RelatedProductInformation">
                                                        <xsl:with-param name="elements" select="ns0:relatedProductInfo"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="PricingTicketingSubsequent">
                                                        <xsl:with-param name="elements" select="ns0:pricingTicketingSubsequent"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="ExcessBaggageInformation">
                                                        <xsl:with-param name="elements" select="ns0:excessBaggageInformation"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="FrequentTravellerInformation">
                                                        <xsl:with-param name="elements" select="ns0:frequentTravellerInformation"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="DateAndTimeInformation">
                                                        <xsl:with-param name="elements" select="ns0:dateAndTimeInformation"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="ErrorInformation">
                                                        <xsl:with-param name="elements" select="ns0:errorInformation"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="OriginatorInformation">
                                                        <xsl:with-param name="elements" select="ns0:originatorInformation"/>
                                                    </xsl:call-template>

                                                    <xsl:call-template name="PricingTicketingDetails">
                                                        <xsl:with-param name="elements" select="ns0:pricingTicketingDetails"/>
                                                    </xsl:call-template>

                                                </GROUP_5>
                                            </xsl:for-each>

                                        </GROUP_4>
                                    </xsl:for-each>

                                </GROUP_3>
                            </xsl:for-each>
                        </xsl:if>

                    </xsl:for-each>

                    <UNT>
                    </UNT>

                </TKCRES>
            </xsl:for-each>

            <UNZ>
            </UNZ>
        </IATA>
    </xsl:template>
</xsl:stylesheet>