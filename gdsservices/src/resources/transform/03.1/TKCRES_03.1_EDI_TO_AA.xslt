<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:common="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/tkcres"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/03.1/COMMON_03.1_EDI_TO_AA.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <xsl:for-each select="IATA">
            <ns1:AA_TKCRES>

                <xsl:call-template name="UNB">
                    <xsl:with-param name="element_name">common:header</xsl:with-param>
                </xsl:call-template>

                <xsl:for-each select="TKCRES">

                    <ns1:message>
                        <xsl:call-template name="UNH">
                            <xsl:with-param name="element_name">common:messageHeader</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="MSG">
                            <xsl:with-param name="element_name">ns1:messageFunction</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="ORG">
                            <xsl:with-param name="element_name">ns1:originatorInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="TAI">
                            <xsl:with-param name="element_name">ns1:ticketingAgentInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="RCI">
                            <xsl:with-param name="element_name">ns1:reservationControlInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="MON">
                            <xsl:with-param name="element_name">ns1:monetaryInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="PTK">
                            <xsl:with-param name="element_name">ns1:pricingTicketingDetails</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="ODI">
                            <xsl:with-param name="element_name">ns1:originDestinationInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="FOP">
                            <xsl:with-param name="element_name">ns1:formOfPayment</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="ATI">
                            <xsl:with-param name="element_name">ns1:additionalTourInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="EQN">
                            <xsl:with-param name="element_name">ns1:numberOfUnits</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="TXD">
                            <xsl:with-param name="element_name">ns1:taxDetails</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="ERC">
                            <xsl:with-param name="element_name">ns1:errorInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="CPN">
                            <xsl:with-param name="complex"/>
                            <xsl:with-param name="element_name">ns1:ticketCoupon</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="TVL">
                            <xsl:with-param name="element_name">ns1:flightInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="DID">
                        </xsl:call-template>

                        <xsl:call-template name="IFT">
                            <xsl:with-param name="element_name">ns1:text</xsl:with-param>
                        </xsl:call-template>

                        <xsl:for-each select="GROUP_1">
                            <ns1:ticketNumberDetails>

                                <xsl:call-template name="TKT">
                                    <xsl:with-param name="complex">true</xsl:with-param>
                                    <xsl:with-param name="element_name"/>
                                </xsl:call-template>

                                <xsl:call-template name="ORG">
                                    <xsl:with-param name="element_name">common:originatorInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ERC">
                                    <xsl:with-param name="element_name">common:errorInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:for-each select="GROUP_2">
                                    <common:ticketCoupon>

                                        <xsl:call-template name="CPN">
                                            <xsl:with-param name="complex">true</xsl:with-param>
                                            <xsl:with-param name="element_name"/>
                                        </xsl:call-template>

                                        <xsl:call-template name="ERC">
                                            <xsl:with-param name="element_name">common:errorInformation</xsl:with-param>
                                        </xsl:call-template>

                                    </common:ticketCoupon>
                                </xsl:for-each>

                                <xsl:call-template name="IFT">
                                    <xsl:with-param name="element_name">text</xsl:with-param>
                                </xsl:call-template>

                            </ns1:ticketNumberDetails>
                        </xsl:for-each>

                        <xsl:for-each select="GROUP_3">
                            <ns1:travellers>

                                <xsl:call-template name="TIF">
                                    <xsl:with-param name="complex">true</xsl:with-param>
                                    <xsl:with-param name="element_name"/>
                                </xsl:call-template>

                                <xsl:call-template name="TAI">
                                    <xsl:with-param name="element_name">common:ticketingAgentInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="RCI">
                                    <xsl:with-param name="element_name">common:reservationControlInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="MON">
                                    <xsl:with-param name="element_name">common:monetaryInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="FOP">
                                    <xsl:with-param name="element_name">common:formOfPayment</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="PTK">
                                    <xsl:with-param name="element_name">common:pricingTicketingDetails</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ODI">
                                    <xsl:with-param name="element_name">common:originDestinationInfo</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="FTI">
                                    <xsl:with-param name="element_name">common:frequentTravellerInfo</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ATI">
                                    <xsl:with-param name="element_name">common:additionalTourInfo</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ORG">
                                    <xsl:with-param name="element_name">common:originatorInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="EQN">
                                    <xsl:with-param name="element_name">common:numberOfUnits</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="TXD">
                                    <xsl:with-param name="element_name">common:taxDetails</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ERC">
                                    <xsl:with-param name="element_name">common:errorInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="DID">
                                </xsl:call-template>

                                <xsl:call-template name="IFT">
                                    <xsl:with-param name="element_name">common:text</xsl:with-param>
                                </xsl:call-template>

                                <xsl:for-each select="GROUP_4">
                                    <common:tickets>

                                        <xsl:call-template name="TKT">
                                            <xsl:with-param name="complex">true</xsl:with-param>
                                            <xsl:with-param name="element_name"/>
                                        </xsl:call-template>

                                        <xsl:call-template name="ERC">
                                            <xsl:with-param name="element_name">common:errorInformation</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="ORG">
                                            <xsl:with-param name="element_name">common:originatorInformation</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="DAT">
                                            <xsl:with-param name="element_name">common:dateAndTime</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="IFT">
                                            <xsl:with-param name="element_name">common:text</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="CRI">
                                            <xsl:with-param name="element_name">common:consumerRef</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:for-each select="GROUP_5">
                                            <common:ticketCoupon>

                                                <xsl:call-template name="CPN">
                                                    <xsl:with-param name="complex">true</xsl:with-param>
                                                    <xsl:with-param name="element_name"/>
                                                </xsl:call-template>

                                                <xsl:call-template name="TVL">
                                                    <xsl:with-param name="element_name">common:flightInfomation</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="RCI">
                                                    <xsl:with-param name="element_name">common:reservationControlInfo</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="RPI">
                                                    <xsl:with-param name="element_name">common:relatedProductInfo</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="PTS">
                                                    <xsl:with-param name="element_name">common:pricingTicketingSubsequent</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="EBD">
                                                    <xsl:with-param name="element_name">common:excessBaggageInformation</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="FTI">
                                                    <xsl:with-param name="element_name">common:frequentTravellerInformation</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="DAT">
                                                    <xsl:with-param name="element_name">common:dateAndTimeInformation</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="ERC">
                                                    <xsl:with-param name="element_name">common:errorInformation</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="ORG">
                                                    <xsl:with-param name="element_name">common:originatorInformation</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="IFT">
                                                    <xsl:with-param name="element_name">common:text</xsl:with-param>
                                                </xsl:call-template>

                                                <xsl:call-template name="PTK">
                                                    <xsl:with-param name="element_name">common:pricingTicketingDetails</xsl:with-param>
                                                </xsl:call-template>

                                            </common:ticketCoupon>
                                        </xsl:for-each>

                                    </common:tickets>
                                </xsl:for-each>

                            </ns1:travellers>
                        </xsl:for-each>

                    </ns1:message>
                </xsl:for-each>
            </ns1:AA_TKCRES>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>