/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of GDS Publish Messages
 * 
 * @author Lasantha Pambagoda
 * @hibernate.class table = "T_GDS_PUBLISH_MSG_TRACKING"
 */
public class GDSPublishMessage extends Persistent {

	private static final long serialVersionUID = 7960788176519223282L;

	public static final String MESSAGE_TYPE_ASM = "ASM";

	public static final String MESSAGE_TYPE_SSM = "SSM";

	public static final String MESSAGE_SUCCESS = "SUCCESS";

	public static final String MESSAGE_FAILED = "FAILED";

	public static final String GDS_INACTIVE = "GDSINAC";

	private Integer id;

	private Integer gds;

	private String messageType;

	private Integer messageTypeId;

	private String referenceKey;

	private String messageContent;

	private String status;

	private Date genDate;

	private Date retryDate;

	private String remarks;

	/**
	 * returns the id
	 * 
	 * @hibernate.id column = "PUBLISH_MSG_TRACK_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_GDS_PUBLISH_MSG_TRACKING"
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * return the gds id which message intended to send
	 * 
	 * @hibernate.property column = "GDS_ID"
	 */
	public Integer getGds() {
		return gds;
	}

	public void setGds(Integer gds) {
		this.gds = gds;
	}

	/**
	 * returns the message type (eg: SSM, ASM, ASC)
	 * 
	 * @hibernate.property column = "MESSAGE_TYPE"
	 */
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * return the message type id (eg: scheduleId, flightId, pnrId)
	 * 
	 * @hibernate.property column = "MESSAGE_TYPE_ID"
	 */
	public Integer getMessageTypeId() {
		return messageTypeId;
	}

	public void setMessageTypeId(Integer messageTypeId) {
		this.messageTypeId = messageTypeId;
	}

	/**
	 * returns the common reference key between GDS and Accel Airo
	 * 
	 * @hibernate.property column = "REFERENCE_KEY"
	 */
	public String getReferenceKey() {
		return referenceKey;
	}

	public void setReferenceKey(String referenceKey) {
		this.referenceKey = referenceKey;
	}

	/**
	 * returns the message content
	 * 
	 * @hibernate.property column = "MESSAGE_CONTENT"
	 */
	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	/**
	 * returns the status of the message
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the message initially generated date
	 * 
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getGenDate() {
		return genDate;
	}

	public void setGenDate(Date genDate) {
		this.genDate = genDate;
	}

	/**
	 * returns the retry date of the message
	 * 
	 * @hibernate.property column = "RETRY_DATE"
	 */
	public Date getRetryDate() {
		return retryDate;
	}

	public void setRetryDate(Date retryDate) {
		this.retryDate = retryDate;
	}

	/**
	 * returns the remarks of the message
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
