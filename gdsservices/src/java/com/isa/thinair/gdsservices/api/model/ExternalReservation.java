package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "t_ext_reservation"
 */
public class ExternalReservation implements Serializable {

	public interface ExternalSystem {
		String GDS = "GDS";
		String CODE_SHARE = "CS";
	}

	public interface LockStatus {
		char LOCKED = 'L';
		char OPEN = 'O';
	}

	private String pnr;
	private String externalSystem;
	private String externalRecordLocator;
	private Integer gdsId;
	private String priceSynced;
	private String reservationSynced;
	private Character lockStatus;

	/**
	 * @hibernate.id column = "pnr" generator-class = "assigned"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column="ext_system"
	 */
	public String getExternalSystem() {
		return externalSystem;
	}

	public void setExternalSystem(String externalSystem) {
		this.externalSystem = externalSystem;
	}

	/**
	 * @hibernate.property column = "ext_rec_locator"
	 */
	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	/**
	 * @hibernate.property column = "gds_id"
	 */
	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * @hibernate.property column = "price_synced"
	 */
	public String getPriceSynced() {
		return priceSynced;
	}

	public void setPriceSynced(String priceSynced) {
		this.priceSynced = priceSynced;
	}

	/**
	 * @hibernate.property column = "res_synced"
	 */
	public String getReservationSynced() {
		return reservationSynced;
	}

	public void setReservationSynced(String reservationSynced) {
		this.reservationSynced = reservationSynced;
	}

	/**
	 * @hibernate.property column = "lock_status"
	 */
	public Character getLockStatus() {
		return lockStatus;
	}

	public void setLockStatus(Character lockStatus) {
		this.lockStatus = lockStatus;
	}
}
