/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2008 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;

/**
 * Biz deligate to provide GDS publishing related apis
 * 
 * @author Lasantha Pambagoda
 * @isa.module.bd-intf id="gdsservices.gdspublishing"
 */
public interface GDSPublishingBD {

	public void publishMessage(Envelope envilope) throws ModuleException;

}
