package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

public class SSMASMSummaryAuditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String messageType;

	private Integer inMessegeID;

	private int totalUpdatedSchedules;

	private int totalUpdatedFlights;

	private boolean success;

	private String responseCode;

	private String updatedScheduleDetails;

	private String rawMessage;

	private String userID;

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Integer getInMessegeID() {
		return inMessegeID;
	}

	public void setInMessegeID(Integer inMessegeID) {
		this.inMessegeID = inMessegeID;
	}

	public int getTotalUpdatedSchedules() {
		return totalUpdatedSchedules;
	}

	public void setTotalUpdatedSchedules(int totalUpdatedSchedules) {
		this.totalUpdatedSchedules = totalUpdatedSchedules;
	}

	public int getTotalUpdatedFlights() {
		return totalUpdatedFlights;
	}

	public void setTotalUpdatedFlights(int totalUpdatedFlights) {
		this.totalUpdatedFlights = totalUpdatedFlights;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getUpdatedScheduleDetails() {
		return updatedScheduleDetails;
	}

	public void setUpdatedScheduleDetails(String updatedScheduleDetails) {
		this.updatedScheduleDetails = updatedScheduleDetails;
	}

	public String getRawMessage() {
		return rawMessage;
	}

	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("\n################################################################");
		sb.append("\n############# PRINT DETAILS SCHEDULE MESSAGE AUDIT #############");
		sb.append("\n################################################################");
		sb.append("\nMSGTYPE :" + messageType + " IN MSG ID:" + this.inMessegeID + " MSG CONTENT:" + this.rawMessage);
		sb.append("\nTotal Updated Schedules:" + totalUpdatedSchedules + " AdHoc Schedules: " + totalUpdatedFlights);

		sb.append("\nUpdated Schedule Details :\n" + updatedScheduleDetails);
		sb.append("\nMessage Process Status:" + success + " ResponseMessage:" + responseCode);
		sb.append("\n################################################################");

		return sb.toString();
	}

}
