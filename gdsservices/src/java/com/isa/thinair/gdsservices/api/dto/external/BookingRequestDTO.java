package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.net.InetAddress;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.isa.thinair.gdsservices.api.dto.typea.MessageError;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

public class BookingRequestDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = -3719337365430743297L;

	private static Log log = LogFactory.getLog(BookingRequestDTO.class);

	private static Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");

	private String uniqueAccelAeroRequestId;

	private String messageBrokerReferenceId;

	private GDSExternalCodes.GDS gds;

	private String requestIPInfo;

	/** Creates a new instance of SegmentSellMessageDTO */
	public BookingRequestDTO() {
		super();
		this.setUniqueAccelAeroRequestId(formatter.format(new Date()) + "-" + UUID.randomUUID());
		this.setOsiDTOs(new ArrayList<OSIDTO>());
		this.setSsrDTOs(new ArrayList<SSRDTO>());
		this.setProcessStatusHistory(new ArrayList<ProcessStatus>());
		this.setProcessStatus(GDSExternalCodes.ProcessStatus.NOT_PROCESSED);
		this.setRequestIPInfo(getIPInfo());
		this.setErrors(new ArrayList<>());
		this.setSendResponse(true);
	}

	private String messageIdentifier;
	private String originatorAddress;
	private String responderAddress;
	private RecordLocatorDTO originatorRecordLocator;
	private RecordLocatorDTO responderRecordLocator;
	private String communicationReference;
	private Date messageReceivedDateStamp;
	private List<ChangedNamesDTO> changedNameDTOs;
	private List<UnchangedNamesDTO> unChangedNameDTOs;
	private List<NameDTO> newNameDTOs;
	private List<BookingSegmentDTO> bookingSegmentDTOs;	
	private List<BookingSegmentDTO> otherAirlineSegmentDTOs;
	private List<SSRDTO> ssrDTOs;
	private List<OSIDTO> osiDTOs;
	private boolean groupBooking;
	private Collection<ProcessStatus> processStatusHistory;
	private ProcessStatus processStatus;
	private String responseMessage;
	private boolean noAdviceCodeExists;
	private boolean notSupportedSSRExists;
	private String responseMessageIdentifier;
	private Collection<TempSegBcAllocDTO> blockedSeats;
	private GDSInternalCodes.TypeBMessageCategory messageCategory;
	private boolean addBaggage;
	private boolean sendReservationImage;
	private List<String> errors;
	private boolean sendResponse;


	/**
	 * @return the processStatus
	 */
	public ProcessStatus getProcessStatus() {
		return processStatus;
	}

	/**
	 * @param processStatus
	 *            the processStatus to set
	 */
	public void setProcessStatus(GDSExternalCodes.ProcessStatus processStatus) {
		this.processStatus = processStatus;

		if (processStatus != null) {
			this.getProcessStatusHistory().add(processStatus);
		}
	}

	public String getCommunicationReference() {
		return communicationReference;
	}

	public void setCommunicationReference(String communicationReference) {
		this.communicationReference = communicationReference;
	}

	public String getMessageIdentifier() {
		return messageIdentifier;
	}

	public void setMessageIdentifier(String messageIdentifier) {
		this.messageIdentifier = messageIdentifier;
	}

	public Date getMessageReceivedDateStamp() {
		return messageReceivedDateStamp;
	}

	public void setMessageReceivedDateStamp(Date messageReceivedDateStamp) {
		this.messageReceivedDateStamp = messageReceivedDateStamp;
	}

	public String getOriginatorAddress() {
		return originatorAddress;
	}

	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	public String getResponderAddress() {
		return responderAddress;
	}

	public void setResponderAddress(String responderAddress) {
		this.responderAddress = responderAddress;
	}

	public RecordLocatorDTO getOriginatorRecordLocator() {
		return originatorRecordLocator;
	}

	public void setOriginatorRecordLocator(RecordLocatorDTO originatorRecordLocator) {
		this.originatorRecordLocator = originatorRecordLocator;
	}

	public RecordLocatorDTO getResponderRecordLocator() {
		return responderRecordLocator;
	}

	public void setResponderRecordLocator(RecordLocatorDTO responderRecordLocator) {
		this.responderRecordLocator = responderRecordLocator;
	}

	/**
	 * @return the groupBooking
	 */
	public boolean isGroupBooking() {
		return groupBooking;
	}

	/**
	 * @param groupBooking
	 *            the groupBooking to set
	 */
	public void setGroupBooking(boolean groupBooking) {
		this.groupBooking = groupBooking;
	}

	/**
	 * @return the uniqueAccelAeroRequestId
	 */
	public String getUniqueAccelAeroRequestId() {
		return uniqueAccelAeroRequestId;
	}

	/**
	 * @param uniqueAccelAeroRequestId
	 *            the uniqueAccelAeroRequestId to set
	 */
	private void setUniqueAccelAeroRequestId(String uniqueAccelAeroRequestId) {
		this.uniqueAccelAeroRequestId = uniqueAccelAeroRequestId;
	}

	/**
	 * @return the gds
	 */
	public GDSExternalCodes.GDS getGds() {
		return gds;
	}

	/**
	 * @param gds
	 *            the gds to set
	 */
	public void setGds(GDSExternalCodes.GDS gdsType) {
		this.gds = gdsType;
	}

	public MessageType getMessageType() {
		return GDSExternalCodes.MessageType.RESERVATION;
	}

	/**
	 * @return the ssrDTOs
	 */
	public List<SSRDTO> getSsrDTOs() {
		return ssrDTOs;
	}

	/**
	 * @param ssrDTOs
	 *            the ssrDTOs to set
	 */
	public void setSsrDTOs(List<SSRDTO> ssrDTOs) {
		this.ssrDTOs = ssrDTOs;
	}

	/**
	 * @return the osiDTOs
	 */
	public List<OSIDTO> getOsiDTOs() {
		return osiDTOs;
	}

	/**
	 * @param osiDTOs
	 *            the osiDTOs to set
	 */
	public void setOsiDTOs(List<OSIDTO> osiDTOs) {
		this.osiDTOs = osiDTOs;
	}

	/**
	 * @return the changedNameDTOs
	 */
	public List<ChangedNamesDTO> getChangedNameDTOs() {
		return changedNameDTOs;
	}

	/**
	 * @param changedNameDTOs
	 *            the changedNameDTOs to set
	 */
	public void setChangedNameDTOs(List<ChangedNamesDTO> changedNameDTOs) {
		this.changedNameDTOs = changedNameDTOs;
	}

	/**
	 * @return the unChangedNameDTOs
	 */
	public List<UnchangedNamesDTO> getUnChangedNameDTOs() {
		return unChangedNameDTOs;
	}

	/**
	 * @param unChangedNameDTOs
	 *            the unChangedNameDTOs to set
	 */
	public void setUnChangedNameDTOs(List<UnchangedNamesDTO> unChangedNameDTOs) {
		this.unChangedNameDTOs = unChangedNameDTOs;
	}

	/**
	 * @return the newNameDTOs
	 */
	public List<NameDTO> getNewNameDTOs() {
		return newNameDTOs;
	}

	/**
	 * @param newNameDTOs
	 *            the newNameDTOs to set
	 */
	public void setNewNameDTOs(List<NameDTO> newNameDTOs) {
		this.newNameDTOs = newNameDTOs;
	}

	/**
	 * @return the bookingSegmentDTOs
	 */
	public List<BookingSegmentDTO> getBookingSegmentDTOs() {
		return bookingSegmentDTOs;
	}

	/**
	 * @param bookingSegmentDTOs
	 *            the bookingSegmentDTOs to set
	 */
	public void setBookingSegmentDTOs(List<BookingSegmentDTO> bookingSegmentDTOs) {
		this.bookingSegmentDTOs = bookingSegmentDTOs;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage
	 *            the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @param processStatusHistory
	 *            the processStatusHistory to set
	 */
	private void setProcessStatusHistory(Collection<ProcessStatus> processStatusHistory) {
		this.processStatusHistory = processStatusHistory;
	}

	/**
	 * @return the processStatusHistory
	 */
	public Collection<ProcessStatus> getProcessStatusHistory() {
		return processStatusHistory;
	}

	/**
	 * @return the messageBrokerReferenceId
	 */
	public String getMessageBrokerReferenceId() {
		return messageBrokerReferenceId;
	}

	/**
	 * @param messageBrokerReferenceId
	 *            the messageBrokerReferenceId to set
	 */
	public void setMessageBrokerReferenceId(String messageBrokerReferenceId) {
		this.messageBrokerReferenceId = messageBrokerReferenceId;
	}

	/**
	 * @return the requestIPInfo
	 */
	public String getRequestIPInfo() {
		return requestIPInfo;
	}

	/**
	 * @param requestIPInfo
	 *            the requestIPInfo to set
	 */
	private void setRequestIPInfo(String requestIPInfo) {
		this.requestIPInfo = requestIPInfo;
	}

	/**
	 * Returns the IP Information
	 * 
	 * @return
	 */
	private static String getIPInfo() {
		StringBuilder ipInfo = new StringBuilder();

		try {
			InetAddress addr = InetAddress.getLocalHost();

			// Get hostname
			String hostname = addr.getHostName().toLowerCase();
			ipInfo.append(hostname);

			// Get IP Address
			byte[] ipAddr = addr.getAddress();

			// Convert to dot representation
			String ipAddress = "";
			for (int i = 0; i < ipAddr.length; i++) {
				if (i > 0) {
					ipAddress += ".";
				}
				ipAddress += ipAddr[i] & 0xFF;
			}
			ipInfo.append("[").append(ipAddress).append("]");
		} catch (Exception e) {
			log.error(e);
		}

		return ipInfo.toString();
	}

	public boolean isNoAdviceCodeExists() {
		return noAdviceCodeExists;
	}

	public void setNoAdviceCodeExists(boolean noAdviceCodeExists) {
		this.noAdviceCodeExists = noAdviceCodeExists;
	}

	public String getResponseMessageIdentifier() {
		return responseMessageIdentifier;
	}

	public void setResponseMessageIdentifier(String responseMessageIdentifier) {
		this.responseMessageIdentifier = responseMessageIdentifier;
	}

	/**
	 * @return the blockedSeats
	 */
	public Collection<TempSegBcAllocDTO> getBlockedSeats() {
		return blockedSeats;
	}

	/**
	 * @param blockedSeats the blockedSeats to set
	 */
	public void setBlockedSeats(Collection<TempSegBcAllocDTO> blockedSeats) {
		this.blockedSeats = blockedSeats;
	}

	public boolean isNotSupportedSSRExists() {
		return notSupportedSSRExists;
	}

	public void setNotSupportedSSRExists(boolean notSupportedSSRExists) {
		this.notSupportedSSRExists = notSupportedSSRExists;
	}

	public GDSInternalCodes.TypeBMessageCategory getMessageCategory() {
		return messageCategory;
	}

	public void setMessageCategory(GDSInternalCodes.TypeBMessageCategory messageCategory) {
		this.messageCategory = messageCategory;
	}

	public boolean isAddBaggage() {
		return addBaggage;
	}

	public void setAddBaggage(boolean addBaggage) {
		this.addBaggage = addBaggage;
	}

	public boolean isSendReservationImage() {
		return sendReservationImage;
	}

	public void setSendReservationImage(boolean sendReservationImage) {
		this.sendReservationImage = sendReservationImage;
	}

	public BookingRequestDTO clone() {
		BookingRequestDTO cloned = null;
		try {
			cloned = (BookingRequestDTO) super.clone();
		} catch (CloneNotSupportedException e) {
			log.error(e.getMessage(), e);
		}

		return cloned;
	}

	public List<BookingSegmentDTO> getOtherAirlineSegmentDTOs() {
		return otherAirlineSegmentDTOs;
	}

	public void setOtherAirlineSegmentDTOs(List<BookingSegmentDTO> otherAirlineSegmentDTOs) {
		this.otherAirlineSegmentDTOs = otherAirlineSegmentDTOs;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public boolean isSendResponse() {
		return sendResponse;
	}

	public void setSendResponse(boolean sendResponse) {
		this.sendResponse = sendResponse;
	}
}
