/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

/**
 * @author M.Rikaz
 * 
 */
public class SSMMessageProcessStatus implements Serializable {

	private static final long serialVersionUID = 6213438435959278397L;

	private Integer scheduleId;

	private boolean isLocalTimeMode;

	private String status;

	private boolean isSuccessfullyProcessed;

	private String flightNumber;

	private String originDestination;

	private String remarks;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public boolean isLocalTimeMode() {
		return isLocalTimeMode;
	}

	public void setLocalTimeMode(boolean isLocalTimeMode) {
		this.isLocalTimeMode = isLocalTimeMode;
	}

	public boolean isSuccessfullyProcessed() {
		return isSuccessfullyProcessed;
	}

	public void setSuccessfullyProcessed(boolean isSuccessfullyProcessed) {
		this.isSuccessfullyProcessed = isSuccessfullyProcessed;
	}

	public String getOriginDestination() {
		return originDestination;
	}

	public void setOriginDestination(String originDestination) {
		this.originDestination = originDestination;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(" Flight No:" + flightNumber + " Time Mode:" + (isLocalTimeMode ? " LT" : "UTC") + " Status:" + status
				+ " Processed:" + isSuccessfullyProcessed + (scheduleId != null ? " Schedule Id:" + scheduleId : ""));
		sb.append(" O&D:" + originDestination + (remarks != null ? " " + remarks : " "));

		return sb.toString();
	}

}
