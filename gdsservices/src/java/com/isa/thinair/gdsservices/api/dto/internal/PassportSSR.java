package com.isa.thinair.gdsservices.api.dto.internal;

public class PassportSSR extends SpecialServiceRequest {

	private static final long serialVersionUID = -1069494087735142544L;

	private String passportNumber;

	/**
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber
	 *            the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

}
