package com.isa.thinair.gdsservices.api.dto.typea.internal;

public class ReservationRq extends InternalBaseRq {
	public enum Event {
		CREATE,
		REMOVE_SEGMENT,
		REMOVE_PASSENGER,
		SPLIT_RESERVATION,
		CHANGE_PAX_DETAILS
	}

	private Event event;

	public ReservationRq(Event event) {
		this.event = event;
	}

	public Event getEvent() {
		return event;
	}
}
