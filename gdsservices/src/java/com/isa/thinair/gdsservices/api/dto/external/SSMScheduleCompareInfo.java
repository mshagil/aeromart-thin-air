/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Analyzed Information of compared SSM Sub-Action message with AccelAero Targeted schedules
 * 
 * @author M.Rikaz
 */
public class SSMScheduleCompareInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4572831973576536094L;

	// SSM RQ Frequency & schedule frequency are same
	boolean isSameDays;
	// SSM RQ Frequency is a sub set of schedule frequency
	boolean isSubSetDays;
	// period & days of operations same
	boolean exactMatch = false;
	// schedule needs to be split and get the exact matching one or sub-schedule
	boolean isSplitSchedule = false;
	// Treat as REV,Schedule period extended/ Days of Operation added
	boolean isPossibleAddDays = false;
	boolean isSubSchedule = false;
	boolean skipSchedule = false;

	Integer exactMatchScheduleId = null;
	Integer matchedScheduleId = null;
	// Schedule period extended/ Days of Operation added
	// Integer addFrequencyScheduleId = null;
	// common days of operation
	Frequency commonOperationDays = null;
	Date dateSplitStartDate = null;
	Date dateSplitStopDate = null;

	public boolean isSameDays() {
		return isSameDays;
	}

	public void setSameDays(boolean isSameDays) {
		this.isSameDays = isSameDays;
	}

	public boolean isSubSetDays() {
		return isSubSetDays;
	}

	public void setSubSetDays(boolean isSubSetDays) {
		this.isSubSetDays = isSubSetDays;
	}

	public boolean isExactMatch() {
		return exactMatch;
	}

	public void setExactMatch(boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	public boolean isSplitSchedule() {
		return isSplitSchedule;
	}

	public void setSplitSchedule(boolean isSplitSchedule) {
		this.isSplitSchedule = isSplitSchedule;
	}

	public boolean isPossibleAddDays() {
		return isPossibleAddDays;
	}

	public void setPossibleAddDays(boolean isPossibleAddDays) {
		this.isPossibleAddDays = isPossibleAddDays;
	}

	public boolean isSubSchedule() {
		return isSubSchedule;
	}

	public void setSubSchedule(boolean isSubSchedule) {
		this.isSubSchedule = isSubSchedule;
	}

	public Integer getMatchedScheduleId() {
		return matchedScheduleId;
	}

	public void setMatchedScheduleId(Integer matchedScheduleId) {
		this.matchedScheduleId = matchedScheduleId;
	}

	// public Integer getAddFrequencyScheduleId() {
	// return addFrequencyScheduleId;
	// }
	//
	// public void setAddFrequencyScheduleId(Integer addFrequencyScheduleId) {
	// this.addFrequencyScheduleId = addFrequencyScheduleId;
	// }

	public Date getDateSplitStartDate() {
		return dateSplitStartDate;
	}

	public Integer getExactMatchScheduleId() {
		return exactMatchScheduleId;
	}

	public void setExactMatchScheduleId(Integer exactMatchScheduleId) {
		this.exactMatchScheduleId = exactMatchScheduleId;
	}// public Frequency getOverlapFrequency() {
		// return commonOperationDays;
		// }
		//
		// public void setOverlapFrequency(Frequency overlapFrequency) {
		// this.commonOperationDays = overlapFrequency;
		// }

	public Frequency getCommonOperationDays() {
		return commonOperationDays;
	}

	public void setCommonOperationDays(Frequency commonOperationDays) {
		this.commonOperationDays = commonOperationDays;
	}

	public void setDateSplitStartDate(Date dateSplitStartDate) {
		this.dateSplitStartDate = dateSplitStartDate;
	}

	public Date getDateSplitStopDate() {
		return dateSplitStopDate;
	}

	public void setDateSplitStopDate(Date dateSplitStopDate) {
		this.dateSplitStopDate = dateSplitStopDate;
	}

	public boolean isSkipSchedule() {
		return skipSchedule;
	}

	public void setSkipSchedule(boolean skipSchedule) {
		this.skipSchedule = skipSchedule;
	}

	public boolean isScheduleToBeUpdate() {
		if ((exactMatch || isSubSchedule) && !isSplitSchedule && exactMatchScheduleId != null) {
			return true;
		}
		return false;
	}

	public boolean isScheduleToBeSplitted() {
		if (!exactMatch && isSplitSchedule && matchedScheduleId != null) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n######### Schedule Analyze Report ");
		sb.append("\nMatches Exactly :" + exactMatch + " Sub-Schedule:" + isSubSchedule + " Schedule ID:" + exactMatchScheduleId);
		sb.append("\nSplit Required :" + isSplitSchedule + " Source Schedule ID:" + matchedScheduleId + " Split From:"
				+ CalendarUtil.formatDate(dateSplitStartDate, CalendarUtil.PATTERN1) + " To:"
				+ CalendarUtil.formatDate(dateSplitStopDate, CalendarUtil.PATTERN1));
		sb.append("\nSame Frequency :" + isSameDays + " SSM Sub-Set Frequency:" + isSubSetDays);
		sb.append("\nPeriod Extend/ Add Days of Operation :" + isPossibleAddDays);

		if (commonOperationDays != null) {
			sb.append("\nCommon Operation Days Count:" + commonOperationDays.getDayCount(true) + " Frequency:"
					+ commonOperationDays);
		}

		return sb.toString();
	}

}
