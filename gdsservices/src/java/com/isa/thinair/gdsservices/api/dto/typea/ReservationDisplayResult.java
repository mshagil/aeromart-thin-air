package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airreservation.api.model.Reservation;

public class ReservationDisplayResult implements Serializable {

	private static final long serialVersionUID = -367743797393223517L;

	/**
	 * Reservation set fro sucsessfull display request
	 */
	private List<Reservation> reservations;

	/**
	 * Error caurse for unsusessfull display request
	 */
	private List appErrors;

	/**
	 * @return the reservations
	 */
	public List<Reservation> getReservations() {
		if (reservations == null) {
			reservations = new ArrayList<Reservation>();
		}
		return reservations;
	}

	/**
	 * @param reservations
	 *            the reservations to set
	 */
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public void addReservation(Reservation reservation) {
		this.getReservations().add(reservation);
	}

	/**
	 * @return the appErrors
	 */
	public List getAppErrors() {
		if (appErrors == null) {
			appErrors = new ArrayList();
		}
		return appErrors;
	}

	/**
	 * @param appErrors
	 *            the appErrors to set
	 */
	public void setAppErrors(List appErrors) {
		this.appErrors = appErrors;
	}

	public boolean isErrorResponse() {
		return !getAppErrors().isEmpty();
	}

}
