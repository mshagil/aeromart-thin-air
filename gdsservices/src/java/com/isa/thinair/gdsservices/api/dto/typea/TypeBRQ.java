package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;

public class TypeBRQ implements Serializable {

	public enum Channel {
		TYPE_A, TYPE_B, WEB_SERVICES
	}

	public enum RequestType {
		INVENTORY_ADJUSTMENT
	}

	private String typeBMessage;
	private RequestType requestType;
	private Channel channel;
	private BookingRequestDTO bookingRequestDTO;
	private boolean saveInOutMsg;
	private String wsServerUrl;
	private boolean msgCreationSuccess = true;

	public String getTypeBMessage() {
		return typeBMessage;
	}

	public void setTypeBMessage(String typeBMessage) {
		this.typeBMessage = typeBMessage;
	}

	public RequestType getRequestType() {
		return requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	/**
	 * @return the bookingRequestDTO
	 */
	public BookingRequestDTO getBookingRequestDTO() {
		return bookingRequestDTO;
	}

	/**
	 * @param bookingRequestDTO
	 *            the bookingRequestDTO to set
	 */
	public void setBookingRequestDTO(BookingRequestDTO bookingRequestDTO) {
		this.bookingRequestDTO = bookingRequestDTO;
	}

	public boolean isSaveInOutMsg() {
		return saveInOutMsg;
	}

	public void setSaveInOutMsg(boolean saveInOutMsg) {
		this.saveInOutMsg = saveInOutMsg;
	}

	public String getWsServerUrl() {
		return wsServerUrl;
	}

	public void setWsServerUrl(String wsServerUrl) {
		this.wsServerUrl = wsServerUrl;
	}

	public boolean isMsgCreationSuccess() {
		return msgCreationSuccess;
	}

	public void setMsgCreationSuccess(boolean msgCreationSuccess) {
		this.msgCreationSuccess = msgCreationSuccess;
	}
}
