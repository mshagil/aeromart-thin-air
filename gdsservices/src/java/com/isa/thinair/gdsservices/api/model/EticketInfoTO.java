package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * E-ticket information holder for GDS functionality.
 * 
 * @author sanjaya
 * 
 */
public class EticketInfoTO implements Serializable {

	private static final long serialVersionUID = -5530515638766785089L;

	private List<CouponInfoTO> couponList = new ArrayList<CouponInfoTO>();

	private String eTicketNumber;

	private String eTicketStatus;

	/**
	 * @return the couponList
	 */
	public List<CouponInfoTO> getCouponList() {
		return couponList;
	}

	/**
	 * @return the eticketNumber
	 */
	public String getEticketNumber() {
		return eTicketNumber;
	}

	/**
	 * @param eticketNumber
	 *            the eticketNumber to set
	 */
	public void setEticketNumber(String eticketNumber) {
		this.eTicketNumber = eticketNumber;
	}

	/**
	 * @return the eTicketStatus
	 */
	public String geteTicketStatus() {
		return eTicketStatus;
	}

	/**
	 * @param eTicketStatus
	 *            the eTicketStatus to set
	 */
	public void seteTicketStatus(String eTicketStatus) {
		this.eTicketStatus = eTicketStatus;
	}

	/**
	 * @return The number of coupons attached to this e-ticket.
	 */
	public int getNumberOfTickets() {
		if (this.couponList == null) {
			return 0;
		} else {
			return this.couponList.size();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eTicketNumber == null) ? 0 : eTicketNumber.hashCode());
		result = prime * result + ((eTicketStatus == null) ? 0 : eTicketStatus.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EticketInfoTO other = (EticketInfoTO) obj;
		if (eTicketNumber == null) {
			if (other.eTicketNumber != null)
				return false;
		} else if (!eTicketNumber.equals(other.eTicketNumber))
			return false;
		if (eTicketStatus == null) {
			if (other.eTicketStatus != null)
				return false;
		} else if (!eTicketStatus.equals(other.eTicketStatus))
			return false;
		return true;
	}

}
