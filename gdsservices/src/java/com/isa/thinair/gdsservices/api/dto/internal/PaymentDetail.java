package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.PaymentType;

public interface PaymentDetail {
	PaymentType getPaymentType();
}
