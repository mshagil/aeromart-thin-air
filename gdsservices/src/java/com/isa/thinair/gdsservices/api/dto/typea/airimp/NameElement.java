package com.isa.thinair.gdsservices.api.dto.typea.airimp;

import java.io.Serializable;

public class NameElement implements Serializable {

	private static final long serialVersionUID = 1L;

	private String lastName;
	private String firstName;
	private String designation;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
}