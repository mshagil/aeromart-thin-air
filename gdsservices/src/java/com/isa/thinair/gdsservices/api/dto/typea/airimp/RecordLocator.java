package com.isa.thinair.gdsservices.api.dto.typea.airimp;

import java.io.Serializable;

public class RecordLocator implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cityAirport;
	private String airline;

	public String getCityAirport() {
		return cityAirport;
	}

	public void setCityAirport(String cityAirport) {
		this.cityAirport = cityAirport;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}
}