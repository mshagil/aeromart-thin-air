package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InitBaseRs implements Serializable {

	private boolean success;
	private List<String> messages;

	public InitBaseRs() {
		messages = new ArrayList<String>();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void addMessage(String message) {
		messages.add(message);
	}
}
