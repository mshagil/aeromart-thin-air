package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;
import java.sql.Blob;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_GDS_RES_IMAGE"
 */
public class GdsReservationImage extends Persistent implements Serializable {

	private Long gdsReservationImageId;
	private String pnr;
	private Blob reservationImage;
	private String imageContext;

	public interface ImageContext {
		String RESERVATION = "R";
		String TICKET = "T";
		String TICKET_CONTROL = "C";
	}

	/**
	 * @hibernate.id column = "GDS_RES_IMAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_GDS_RES_IMAGE"
	 */
	public Long getGdsReservationImageId() {
		return gdsReservationImageId;
	}

	public void setGdsReservationImageId(Long gdsReservationImageId) {
		this.gdsReservationImageId = gdsReservationImageId;
	}

	/**
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column = "RES_IMAGE"
	 */
	public Blob getReservationImage() {
		return reservationImage;
	}

	public void setReservationImage(Blob reservationImage) {
		this.reservationImage = reservationImage;
	}

	/**
	 * @hibernate.property column = "IMAGE_CONTEXT"
	 */
	public String getImageContext() {
		return imageContext;
	}

	public void setImageContext(String imageContext) {
		this.imageContext = imageContext;
	}
}
