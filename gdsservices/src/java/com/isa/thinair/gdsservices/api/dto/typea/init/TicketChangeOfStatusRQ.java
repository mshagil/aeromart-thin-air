package com.isa.thinair.gdsservices.api.dto.typea.init;


import java.util.HashMap;
import java.util.Map;

public class TicketChangeOfStatusRQ extends InitBaseRq {

	private boolean overrideStatus;
	private Map<Integer, String> couponStatus;
	private boolean groundHandler;

	public TicketChangeOfStatusRQ() {
		couponStatus = new HashMap<Integer, String>();
	}

	public boolean isOverrideStatus() {
		return overrideStatus;
	}

	public void setOverrideStatus(boolean overrideStatus) {
		this.overrideStatus = overrideStatus;
	}

	public Map<Integer, String> getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(Map<Integer, String> couponStatus) {
		this.couponStatus = couponStatus;
	}

	public void addCouponStatus(Integer ppfset, String couponStatus) {
		this.couponStatus.put(ppfset, couponStatus);
	}

	public boolean isGroundHandler() {
		return groundHandler;
	}

	public void setGroundHandler(boolean groundHandler) {
		this.groundHandler = groundHandler;
	}
}
