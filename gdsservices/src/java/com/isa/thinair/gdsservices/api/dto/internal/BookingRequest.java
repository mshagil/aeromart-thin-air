package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;

public class BookingRequest {
	private BookingRequestDTO bookingRequestDTO;

	/**
	 * @return the bookingRequestDTO
	 */
	public BookingRequestDTO getBookingRequestDTO() {
		return bookingRequestDTO;
	}

	/**
	 * @param bookingRequestDTO
	 *            the bookingRequestDTO to set
	 */
	public void setBookingRequestDTO(BookingRequestDTO bookingRequestDTO) {
		this.bookingRequestDTO = bookingRequestDTO;
	}

}
