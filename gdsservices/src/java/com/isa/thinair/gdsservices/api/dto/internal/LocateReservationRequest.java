package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class LocateReservationRequest extends CreateReservationRequest {

	private static final long serialVersionUID = 1602170935717580977L;

	@Override
	public ReservationAction getActionType() {
		return ReservationAction.LOCATE_RESERVATION;
	}
}
