package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.SSRCodes;

public class EmergencyContactDetailSSR extends SpecialServiceRequest {
	private static final long serialVersionUID = -260055999304903066L;

	public static final String SSR_CODE = SSRCodes.PCTC.getCode();

	private ContactDetail contactDetail;

	private String passengerFirstName;
	private String passengerLastName;
	private String passengerTitle;

	public EmergencyContactDetailSSR() {
		contactDetail = new ContactDetail();
	}

	/**
	 * @return the contactDetail
	 */
	public ContactDetail getContactDetail() {
		return contactDetail;
	}

	/**
	 * @param contactDetail
	 *            the contactDetail to set
	 */
	public void setContactDetail(ContactDetail contactDetail) {
		this.contactDetail = contactDetail;
	}

	/**
	 * @return the passengerFirstName
	 */
	public String getPassengerFirstName() {
		return passengerFirstName;
	}

	/**
	 * @param passengerFirstName
	 *            the passengerFirstName to set
	 */
	public void setPassengerFirstName(String passengerFirstName) {
		this.passengerFirstName = passengerFirstName;
	}

	/**
	 * @return the passengerLastName
	 */
	public String getPassengerLastName() {
		return passengerLastName;
	}

	/**
	 * @param passengerLastName
	 *            the passengerLastName to set
	 */
	public void setPassengerLastName(String passengerLastName) {
		this.passengerLastName = passengerLastName;
	}

	/**
	 * @return the passengerTitle
	 */
	public String getPassengerTitle() {
		return passengerTitle;
	}

	/**
	 * @param passengerTitle
	 *            the passengerTitle to set
	 */
	public void setPassengerTitle(String passengerTitle) {
		this.passengerTitle = passengerTitle;
	}
}
