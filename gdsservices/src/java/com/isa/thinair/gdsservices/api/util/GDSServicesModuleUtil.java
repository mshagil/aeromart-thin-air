/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.util;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airproxy.api.service.AirproxyAncillaryBD;
import com.isa.thinair.airproxy.api.service.AirproxyPassengerBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;
import com.isa.thinair.gdsservices.api.service.GdsRequiredBD;
import com.isa.thinair.gdsservices.api.service.SchedulePublishBD;
import com.isa.thinair.gdsservices.core.config.TypeAConfig;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidationsImplConfig;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.msgbroker.api.service.AVSServiceBD;
import com.isa.thinair.msgbroker.api.service.MessageBrokerServiceBD;
import com.isa.thinair.msgbroker.api.service.SSMASMServiceBD;
import com.isa.thinair.gdsservices.api.service.TypeAServiceBD;
import com.isa.thinair.msgbroker.api.service.TypeBServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.security.auth.login.LoginException;

/**
 * Reservation Services module specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class GDSServicesModuleUtil {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(GDSServicesModuleUtil.class);

	private static ValidationsImplConfig validationsImplConfig;

	/**
	 * Returns transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getConfig(), "gdsservices",
				"dsservices.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getConfig(), "gdsservices",
				"dsservices.config.dependencymap.invalid");
	}

	/**
	 * Returns IModule instance for reservation services module
	 * 
	 * @return
	 */
	public static IModule getModule() {
		return LookupServiceFactory.getInstance().getModule(GdsservicesConstants.MODULE_NAME);
	}

	/**
	 * Return transparent gdsservices business delegate
	 * 
	 * @return
	 */
	public static GDSServicesBD getGDSServicesBD(boolean invokeCredentials) throws ModuleException{

		if (invokeCredentials) {
			try {
				ForceLoginInvoker.webserviceLogin(GDSServicesModuleUtil.getConfig().getEjbAccessUsername(),
						GDSServicesModuleUtil.getConfig().getEjbAccessPassword(), SalesChannelsUtil.SALES_CHANNEL_GDS);
			} catch (LoginException e) {
				throw new ModuleException(e, "module.invalid.user");
			}
		}

		return (GDSServicesBD) GDSServicesModuleUtil.lookupEJB3Service(GdsservicesConstants.MODULE_NAME,
				GDSServicesBD.SERVICE_NAME);

	}

	public static GdsRequiredBD getGdsRequiredBD() {

		return (GdsRequiredBD) GDSServicesModuleUtil.lookupEJB3Service(GdsservicesConstants.MODULE_NAME,
				GdsRequiredBD.SERVICE_NAME);

	}

	public static GDSNotifyBD getGDSNotifyBD() {
		return (GDSNotifyBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GDSNotifyBD.SERVICE_NAME);
	}


	/**
	 * Return transparent reservation query business delegate
	 * 
	 * @return
	 */
	public final static ReservationQueryBD getReservationQueryBD() {

		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}
	
	/**
	 * Gets the Location Service from Air Master
	 * 
	 * @return LocationBD the Location delegate
	 */
	public final static LocationBD getLocationServiceBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	/**
	 * Return transparent reservation business delegate
	 * 
	 * @return
	 */
	public final static ReservationBD getReservationBD() {

		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);

	}

	/**
	 * Return transparent flight inventory res business delegate
	 * 
	 * @return
	 */
	public final static FlightInventoryResBD getFlightInventoryResBD() {

		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);

	}

	/**
	 * Return transparent travel agent business delegate
	 * 
	 * @return
	 */
	public final static TravelAgentBD getTravelAgentBD() {

		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);

	}

	/**
	 * Return Gds business delegate
	 * 
	 * @return
	 */
	public final static GdsBD getGdsBD() {

		return (GdsBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, GdsBD.SERVICE_NAME);
	}

	/**
	 * Return transparent passenger business delegate
	 * 
	 * @return
	 */
	public final static PassengerBD getPassengerBD() {

		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);

	}

	/**
	 * Return transparent reservation segment delegate
	 * 
	 * @return
	 */
	public static SegmentBD getReservationSegmentBD() {

		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);

	}

	/**
	 * Return transparent airport business delegate
	 * 
	 * @return
	 */
	public final static AirportBD getAirportBD() {

		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);

	}

	/**
	 * Return transparent security business delegate
	 * 
	 * @return
	 */
	public final static SecurityBD getSecurityBD() {

		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);

	}

	/**
	 * Return transparent booking class business delegate
	 * 
	 * @return
	 */
	public final static BookingClassBD getBookingClassBD() {

		return (BookingClassBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, BookingClassBD.SERVICE_NAME);

	}

	/**
	 * Return flight inventory business delegate
	 * 
	 * @return
	 */
	public final static FlightInventoryBD getFlightInventoryBD() {

		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);

	}

	/**
	 * Return common master business delegate
	 * 
	 * @return
	 */
	public final static CommonMasterBD getCommonMasterBD() {

		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);

	}

	public final static ScheduleBD getScheduleBD() {

		return (ScheduleBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);

	}

	/**
	 * Returns the FlightBD business Delegate
	 * 
	 * @return
	 */
	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	/**
	 * Returns the AircraftBD business Delegate
	 * 
	 * @return
	 */
	public static AircraftBD getAircraftBD() {
		return (AircraftBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	/**
	 * Returns the MessagingServiceBD business Delegate
	 * 
	 * @return
	 */
	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	/**
	 * Returns SSMASMServiceBD business Delegate
	 * 
	 * @return
	 */
	public final static SSMASMServiceBD getSSMASMServiceBD() {

		SSMASMServiceBD ssmAsmMessageSender = null;
		try {
			ssmAsmMessageSender = (SSMASMServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME,
					SSMASMServiceBD.SERVICE_NAME);
			log.debug("SSMASMServiceBD() is successfully executed");
		} catch (Exception e) {
			log.error("Error locating SSMASMServiceBD" + e.getMessage());
		}
		return ssmAsmMessageSender;
	}

	public final static AVSServiceBD getAVSServiceBD() {

		AVSServiceBD avsMessageSender = null;
		try {
			avsMessageSender = (AVSServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, AVSServiceBD.SERVICE_NAME);
			log.debug("AVSServiceBD() is successfully executed");
		} catch (Exception e) {
			log.error("Error locating AVSServiceBD" + e.getMessage());
		}
		return avsMessageSender;
	}

	/**
	 * 
	 * @return
	 */
	public final static AirproxyAncillaryBD getAirproxyAncillaryBD() {
		return (AirproxyAncillaryBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyAncillaryBD.SERVICE_NAME);
	}

	/**
	 * 
	 * @return
	 */
	public final static AirproxyReservationQueryBD getAirproxyReservationQueryBD() {
		return (AirproxyReservationQueryBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyReservationQueryBD.SERVICE_NAME);
	}

	public final static AirproxyReservationQueryBD getAirproxySearchAndQuoteBD() {
		return (AirproxyReservationQueryBD) lookupEJB3Service(
				AirproxyConstants.MODULE_NAME, AirproxyReservationQueryBD.SERVICE_NAME);

	}

	/**
	 * 
	 * @return
	 */
	public final static AirproxyReservationBD getAirproxyReservationBD() {
		return (AirproxyReservationBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyReservationBD.SERVICE_NAME);
	}

	public final static TypeBServiceBD getTypeBServiceBD() {
		return (TypeBServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeBServiceBD.SERVICE_NAME);
	}

	public final static TypeAServiceBD getTypeAServiceBD() {
		return (TypeAServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeAServiceBD.SERVICE_NAME);
	}

	public final static MessageBrokerServiceBD getMessageBrokerServiceBD() {
		return (MessageBrokerServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, MessageBrokerServiceBD.SERVICE_NAME);
	}

	public final static AirproxyPassengerBD getAirproxyPassengerBD() {
		AirproxyPassengerBD airproxyPassenger = null;
		airproxyPassenger = (AirproxyPassengerBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyPassengerBD.SERVICE_NAME);
		return airproxyPassenger;
	}

	public static BaggageBusinessDelegate getBaggageBD() {
		return (BaggageBusinessDelegate) lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}

	/**
	 * Return the global confirguration
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	/**
	 * Returns ValidationsImplConfig configurations.
	 * 
	 * @return IConfig
	 */
	public static ValidationsImplConfig getValidationsImplConfig() {
		if (validationsImplConfig != null) {
			return validationsImplConfig;
		}

		validationsImplConfig = (ValidationsImplConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/gdsservices?id=validationsImplConfig");

		return validationsImplConfig;
	}

	public static TypeAConfig getTypeAConfig() {
		return (TypeAConfig) LookupServiceFactory.getInstance().getBean("isa:base://modules/gdsservices?id=typeAConfig");
	}

	public static GDSServicesModuleConfig getConfig() {
		GDSServicesModuleConfig config = (GDSServicesModuleConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/gdsservices?id=gdsservicesModuleConfig");
		return config;
	}
		
	public static SsrBD getSsrServiceBD() {
		return (SsrBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}
	
	public static AuditorBD getAuditorBD() {
		return (AuditorBD) lookupServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}
	
	public final static SchedulePublishBD getSchedulePublishBD() {
		return (SchedulePublishBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, SchedulePublishBD.SERVICE_NAME);
	}

}
