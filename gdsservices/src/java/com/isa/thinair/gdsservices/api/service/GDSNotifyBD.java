package com.isa.thinair.gdsservices.api.service;

import com.isa.thinair.airproxy.api.model.reservation.commons.DisplayTickerRqDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRS;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRs;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRs;

public interface GDSNotifyBD {

	public static final String SERVICE_NAME = "GDSNotifyService";

	TicketChangeOfStatusRS changeCouponStatus(TicketChangeOfStatusRQ ticketChangeOfStatusRQ) throws ModuleException ;

	TicketingEventRs onTicketIssue(TicketingEventRq ticketIssueRq) throws ModuleException;

	ReservationRs onReservationModification (ReservationRq reservationRq);

	void reservationLock(String pnr) throws ModuleException;
	
	void handleDisplayForModifiedReservation(DisplayTickerRqDTO displayTicketRq) throws ModuleException;
}
