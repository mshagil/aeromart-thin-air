package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AVSRequestDTO implements Serializable {

	private static final long serialVersionUID = 7306190649955322553L;
	private String timestamp = null;
	private String messageType = null;

	private List<AVSRequestSegmentDTO> avsSegmentDTOs = null;

	private String operatingCarrier;
	private String gdsId;

	/**
	 * Gets AVSSegmentDTOs colloection.
	 * 
	 * @return
	 */
	public List<AVSRequestSegmentDTO> getAVSSegmentDTOs() {
		return avsSegmentDTOs;
	}

	public void addAVSSegmentDTOs(AVSRequestSegmentDTO avsSegmentDTO) {
		if (avsSegmentDTOs == null)
			avsSegmentDTOs = new ArrayList<AVSRequestSegmentDTO>();

		avsSegmentDTOs.add(avsSegmentDTO);

	}

	/**
	 * Sets AVSSegmentDTOs colloection. Only 10 segments could be presented in the list.
	 * 
	 * @param segmentDTOs
	 */
	public void setAVSSegmentDTOs(List<AVSRequestSegmentDTO> avsSegmentDTOs) {
		this.avsSegmentDTOs = avsSegmentDTOs;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getGdsId() {
		return gdsId;
	}

	public void setGdsId(String gdsId) {
		this.gdsId = gdsId;
	}

}
