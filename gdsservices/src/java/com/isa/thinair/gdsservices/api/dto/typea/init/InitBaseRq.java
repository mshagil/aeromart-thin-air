package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.io.Serializable;

import com.isa.thinair.gdsservices.api.model.GdsEvent;

public class InitBaseRq implements Serializable {

	private GdsEvent.Request requestType;

	public GdsEvent.Request getRequestType() {
		return requestType;
	}

	public void setRequestType(GdsEvent.Request requestType) {
		this.requestType = requestType;
	}
}
