package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;

public class SSMGroupMessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String senderAddress = null;
	private String recipientAddress = null;
	private String timestamp = null;

	private boolean codeShareCarrier;

	private String timeMode;

	private String operatingCarrier;

	private String messageSequenceReference;

	private String creatorReference;

	private MessageType messageType;

	private String errorMessage;

	private SSMASMSUBMessegeDTO masterScheduleMsg;

	private List<SSMASMSUBMessegeDTO> ssmASMSUBMessegeList;

	private String supplementaryInfo;

	public enum EXPECTED_ACTION {
		NONE, UPDATE, SPLIT_UPDATE;
	}

	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @param senderAddress
	 *            the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	/**
	 * @return the recipientAddress
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}

	/**
	 * @param recipientAddress
	 *            the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getMessageSequenceReference() {
		return messageSequenceReference;
	}

	public void setMessageSequenceReference(String messageSequenceReference) {
		this.messageSequenceReference = messageSequenceReference;
	}

	public String getCreatorReference() {
		return creatorReference;
	}

	public void setCreatorReference(String creatorReference) {
		this.creatorReference = creatorReference;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<SSMASMSUBMessegeDTO> getSsmASMSUBMessegeList() {
		return ssmASMSUBMessegeList;
	}

	public void setSsmASMSUBMessegeList(List<SSMASMSUBMessegeDTO> ssmASMSUBMessegeList) {
		this.ssmASMSUBMessegeList = ssmASMSUBMessegeList;
	}

	public void addSsmASMSUBMessegeList(SSMASMSUBMessegeDTO ssmASMSUBMessegeDTO) {
		if (this.ssmASMSUBMessegeList == null)
			this.ssmASMSUBMessegeList = new ArrayList<SSMASMSUBMessegeDTO>();
		this.ssmASMSUBMessegeList.add(ssmASMSUBMessegeDTO);
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

	public SSMASMSUBMessegeDTO getMasterScheduleMsg() {
		return masterScheduleMsg;
	}

	public void setMasterScheduleMsg(SSMASMSUBMessegeDTO masterScheduleMsg) {
		this.masterScheduleMsg = masterScheduleMsg;
	}

	public EXPECTED_ACTION getExpectedOperationAction() {

		if (SSMASMSUBMessegeDTO.Action.CNL.equals(masterScheduleMsg.getAction()) && getSsmASMSUBMessegeList() != null
				&& getSsmASMSUBMessegeList().size() == 1) {
			return EXPECTED_ACTION.UPDATE;
		} else if (SSMASMSUBMessegeDTO.Action.CNL.equals(masterScheduleMsg.getAction()) && getSsmASMSUBMessegeList() != null
				&& getSsmASMSUBMessegeList().size() > 1) {
			return EXPECTED_ACTION.SPLIT_UPDATE;
		} else {
			return EXPECTED_ACTION.NONE;
		}
	}

}
