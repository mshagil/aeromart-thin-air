package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.util.List;

/**
 * @author Manoj Dhanushka
 */
public class StandardScheduleMessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String airlineDesignator;

	private String flightNumber;

	private String serviceType;

	private String aircraftType;

	private List<ItineraryVariationScheduleMessageDTO> itineraryVariations;

	private String bookingClasses;

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getAirlineDesignator() {
		if (airlineDesignator.length() == 2) {
			airlineDesignator += " ";
		}
		return airlineDesignator;
	}

	public void setAirlineDesignator(String airlineDesignator) {
		this.airlineDesignator = airlineDesignator;
	}

	public String getFlightNumber() {
		String fltNum = "    ";
		if (this.flightNumber != null) {
			fltNum = this.flightNumber.substring(2);
			if (fltNum.length() == 3) {
				fltNum = " " + fltNum;
			}
		}
		return fltNum;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(String bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public List<ItineraryVariationScheduleMessageDTO> getItineraryVariations() {
		return itineraryVariations;
	}

	public void setItineraryVariations(List<ItineraryVariationScheduleMessageDTO> itineraryVariations) {
		this.itineraryVariations = itineraryVariations;
	}
}
