package com.isa.thinair.gdsservices.api.dto.external;

public class OtherServiceInfoDTO extends OSIDTO {

	private static final long serialVersionUID = -6136065889918802226L;

	private String serviceInfo;

	private String namesString;

	/**
	 * @return the serviceInfo
	 */
	public String getServiceInfo() {
		return serviceInfo;
	}

	/**
	 * @param serviceInfo
	 *            the serviceInfo to set
	 */
	public void setServiceInfo(String serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	/**
	 * @return the namesString
	 */
	public String getNamesString() {
		return namesString;
	}

	/**
	 * @param namesString
	 *            the namesString to set
	 */
	public void setNamesString(String namesString) {
		this.namesString = namesString;
	}

}
