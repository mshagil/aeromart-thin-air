package com.isa.thinair.gdsservices.api.dto.internal;

public class RecordLocator extends GDSBase {
	private static final long serialVersionUID = -6271021630609592638L;

	private String bookingOffice;
	private String pnrReference;
	private String taOfficeCode;

	private String userID;

	private String closestCityAirportCode;

	private String carrierCRSCode;

	private String userType;

	private String countryCode;

	private String currencyCode;

	private String agentDutyCode;

	private String erspId;

	private String firstDepartureFrom;
	
	private String pointOfSales;

	/**
	 * @return the pointOfSales
	 */
	public String getPointOfSales() {
		return pointOfSales;
	}

	/**
	 * @param pointOfSales the pointOfSales to set
	 */
	public void setPointOfSales(String pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

	/**
	 * @return the bookingOffice
	 */
	public String getBookingOffice() {
		return bookingOffice;
	}

	/**
	 * @param bookingOffice
	 *            the bookingOffice to set
	 */
	public void setBookingOffice(String bookingOffice) {
		this.bookingOffice = bookingOffice;
	}

	/**
	 * @return the pnrReference
	 */
	public String getPnrReference() {
		return pnrReference;
	}

	/**
	 * @param pnrReference
	 *            the pnrReference to set
	 */
	public void setPnrReference(String pnrReference) {
		this.pnrReference = pnrReference;
	}

	/**
	 * @return the taOfficeCode
	 */
	public String getTaOfficeCode() {
		return taOfficeCode;
	}

	/**
	 * @param taOfficeCode
	 *            the taOfficeCode to set
	 */
	public void setTaOfficeCode(String taOfficeCode) {
		this.taOfficeCode = taOfficeCode;
	}

	/**
	 * @return the userID
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @return the closestCityAirportCode
	 */
	public String getClosestCityAirportCode() {
		return closestCityAirportCode;
	}

	/**
	 * @param closestCityAirportCode
	 *            the closestCityAirportCode to set
	 */
	public void setClosestCityAirportCode(String closestCityAirportCode) {
		this.closestCityAirportCode = closestCityAirportCode;
	}

	/**
	 * @return the carrierCRSCode
	 */
	public String getCarrierCRSCode() {
		return carrierCRSCode;
	}

	/**
	 * @param carrierCRSCode
	 *            the carrierCRSCode to set
	 */
	public void setCarrierCRSCode(String carrierCRSCode) {
		this.carrierCRSCode = carrierCRSCode;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the agentDutyCode
	 */
	public String getAgentDutyCode() {
		return agentDutyCode;
	}

	/**
	 * @param agentDutyCode
	 *            the agentDutyCode to set
	 */
	public void setAgentDutyCode(String agentDutyCode) {
		this.agentDutyCode = agentDutyCode;
	}

	/**
	 * @return the erspId
	 */
	public String getErspId() {
		return erspId;
	}

	/**
	 * @param erspId
	 *            the erspId to set
	 */
	public void setErspId(String erspId) {
		this.erspId = erspId;
	}

	/**
	 * @return the firstDepartureFrom
	 */
	public String getFirstDepartureFrom() {
		return firstDepartureFrom;
	}

	/**
	 * @param firstDepartureFrom
	 *            the firstDepartureFrom to set
	 */
	public void setFirstDepartureFrom(String firstDepartureFrom) {
		this.firstDepartureFrom = firstDepartureFrom;
	}

}
