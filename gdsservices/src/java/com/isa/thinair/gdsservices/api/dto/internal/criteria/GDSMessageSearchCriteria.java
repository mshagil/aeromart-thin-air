/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.dto.internal.criteria;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dhanushka
 */
public class GDSMessageSearchCriteria implements Serializable {

	private static final long serialVersionUID = 6685581828655624332L;

	private String messageType;

	private Date messageReceivedDateFrom;

	private Date messageReceivedDateTo;

	private String communicationReference;

	private String gds;

	private String processStatus;

	private String requestXML;

	private String responseXML;

	private String gdsPnr;

	private String messageBrokerReferenceId;

	/**
	 * @return the gdsPnr
	 */
	public String getGdsPnr() {
		return gdsPnr;
	}

	/**
	 * @param gdsPnr
	 *            the gdsPnr to set
	 */
	public void setGdsPnr(String gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	/**
	 * @return the messageBrokerReferenceId
	 */
	public String getMessageBrokerReferenceId() {
		return messageBrokerReferenceId;
	}

	/**
	 * @param messageBrokerReferenceId
	 *            the messageBrokerReferenceId to set
	 */
	public void setMessageBrokerReferenceId(String messageBrokerReferenceId) {
		this.messageBrokerReferenceId = messageBrokerReferenceId;
	}

	/**
	 * @return the communicationReference
	 */
	public String getCommunicationReference() {
		return communicationReference;
	}

	/**
	 * @param communicationReference
	 *            the communicationReference to set
	 */
	public void setCommunicationReference(String communicationReference) {
		this.communicationReference = communicationReference;
	}

	/**
	 * @return the messageReceivedDateFrom
	 */
	public Date getMessageReceivedDateFrom() {
		return messageReceivedDateFrom;
	}

	/**
	 * @param messageReceivedDateFrom
	 *            the messageReceivedDateFrom to set
	 */
	public void setMessageReceivedDateFrom(Date messageReceivedDateFrom) {
		this.messageReceivedDateFrom = messageReceivedDateFrom;
	}

	/**
	 * @return the messageReceivedDateTo
	 */
	public Date getMessageReceivedDateTo() {
		return messageReceivedDateTo;
	}

	/**
	 * @param messageReceivedDateTo
	 *            the messageReceivedDateTo to set
	 */
	public void setMessageReceivedDateTo(Date messageReceivedDateTo) {
		this.messageReceivedDateTo = messageReceivedDateTo;
	}

	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 *            the messageType to set
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return the processStatus
	 */
	public String getProcessStatus() {
		return processStatus;
	}

	/**
	 * @param processStatus
	 *            the processStatus to set
	 */
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * @return the requestXML
	 */
	public String getRequestXML() {
		return requestXML;
	}

	/**
	 * @param requestXML
	 *            the requestXML to set
	 */
	public void setRequestXML(String requestXML) {
		this.requestXML = requestXML;
	}

	/**
	 * @return the responseXML
	 */
	public String getResponseXML() {
		return responseXML;
	}

	/**
	 * @param responseXML
	 *            the responseXML to set
	 */
	public void setResponseXML(String responseXML) {
		this.responseXML = responseXML;
	}

	/**
	 * @return the gds
	 */
	public String getGds() {
		return gds;
	}

	/**
	 * @param gds
	 *            the gds to set
	 */
	public void setGds(String gds) {
		this.gds = gds;
	}
}
