package com.isa.thinair.gdsservices.api.model;

/**
 * Supported EDIFACT messages
 *
 * @author malaka
 */
public enum IATAOperation {

	ITAREQ {
		@Override
		public IATAOperation getResponse() {
			return ITARES;
		}
	}, ITARES {
		@Override
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	}, PAOREQ {
		@Override
		public IATAOperation getResponse() {
			return PAORES;
		}
	}, PAORES {
		@Override
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	}, TKCREQ {
		@Override
		public IATAOperation getResponse() {
			return TKCRES;
		}
	}, TKCRES {
		@Override
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	}, TKCUAC {
		@Override
		public IATAOperation getResponse() {
			return TKCRES;
		}
	}, CONTRL {
		@Override
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	}, HWPREQ {
		public IATAOperation getResponse() {
			return HWPRES;
		}
	}, HWPRES {
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	}, CLTREQ {
		public IATAOperation getResponse() {
			return CLTRES;
		}
	}, CLTRES {
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	}, TKTREQ {
		public IATAOperation getResponse() {
			return TKTRES;
		}
	}, TKTRES {
		public IATAOperation getResponse() {
			throw new UnsupportedOperationException();
		}
	};

	@Deprecated
	public static IATAOperation extractEdiOperationName(String ediMessage) {
		for (IATAOperation supportedOperation : IATAOperation.values()) {
			if (ediMessage.contains(supportedOperation.toString())) {
				return supportedOperation;
			}
		}

		throw new UnsupportedOperationException("An unsupported operation was found in the EDI message");
	}

	public abstract IATAOperation getResponse();

	public <T> Class<T> getAccelAeroType() {
		throw new UnsupportedOperationException("An unsupported operation by Accel-Aero was found in the EDI message");
	}
}