package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.util.List;
import java.util.Map;

public class TicketDisplayRq extends InitBaseRq {

	private int gdsId;
	private String pnr;
	private Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons;

	private boolean calculateFinancialInfo;

	public TicketDisplayRq() {
		calculateFinancialInfo = false;
	}

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<Integer, Map<String, List<Integer>>> getPaxTicketsCoupons() {
		return paxTicketsCoupons;
	}

	public void setPaxTicketsCoupons(Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons) {
		this.paxTicketsCoupons = paxTicketsCoupons;
	}

	public boolean isCalculateFinancialInfo() {
		return calculateFinancialInfo;
	}

	public void setCalculateFinancialInfo(boolean calculateFinancialInfo) {
		this.calculateFinancialInfo = calculateFinancialInfo;
	}
}
