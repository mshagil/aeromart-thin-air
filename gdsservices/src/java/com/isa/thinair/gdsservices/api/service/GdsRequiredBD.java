package com.isa.thinair.gdsservices.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.ReIssueTicketRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.ReIssueTicketRs;
import com.isa.thinair.gdsservices.api.dto.typea.init.RecallTicketCouponControlRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.RecallTicketCouponControlRs;

public interface GdsRequiredBD {

	String SERVICE_NAME = "GdsRequiredService";

	RecallTicketCouponControlRs recallTicketCouponControl(RecallTicketCouponControlRq rq);

	ReIssueTicketRs reIssueTicket(ReIssueTicketRq reIssueTicketRq) throws ModuleException;
}
