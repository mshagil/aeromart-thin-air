package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

import com.isa.thinair.gdsservices.api.util.GDSApiUtils;

public class NameDTO implements Serializable {

	private static final long serialVersionUID = 87523691763113253L;
	public static final int MIN_GROUP_BOOKING_SIZE = 10;

	private String paxTitle;
	private String lastName;
	private String firstName;
	private int groupMagnitude;
	private int familyID;
	private boolean isInfant;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaxTitle() {
		return paxTitle;
	}

	public void setPaxTitle(String paxTitle) {
		this.paxTitle = paxTitle;
	}

	/**
	 * @return the groupMagnitude
	 */
	public int getGroupMagnitude() {
		return groupMagnitude;
	}

	/**
	 * @param groupMagnitude
	 *            the groupMagnitude to set
	 */
	public void setGroupMagnitude(int groupMagnitude) {
		this.groupMagnitude = groupMagnitude;
	}

	public String getIATAName() {
		return GDSApiUtils.getIATAName(getLastName(), getFirstName(), getPaxTitle());
	}

	public int getFamilyID() {
		return familyID;
	}

	public void setFamilyID(int familyID) {
		this.familyID = familyID;
	}

	public boolean isInfant() {
		return isInfant;
	}

	public void setInfant(boolean isInfant) {
		this.isInfant = isInfant;
	}
}
