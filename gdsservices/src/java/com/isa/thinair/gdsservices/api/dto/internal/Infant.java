package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Date;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class Infant extends Passenger {
	private static final long serialVersionUID = 6054965167903489916L;
	private Adult parent;
	private Date infantDateofBirth;

	@Override
	public String getPaxType() {
		return ReservationInternalConstants.PassengerType.INFANT;
	}

	/**
	 * @return Returns the parent.
	 */
	public Adult getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            The parent to set.
	 */
	public void setParent(Adult parent) {
		this.parent = parent;
	}

	public Date getInfantDateofBirth() {
		return infantDateofBirth;
	}

	public void setInfantDateofBirth(Date infantDateofBirth) {
		this.infantDateofBirth = infantDateofBirth;
	}
}
