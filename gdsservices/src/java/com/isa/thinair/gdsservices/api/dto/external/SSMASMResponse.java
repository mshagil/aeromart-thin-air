package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class SSMASMResponse implements Serializable {

	private static final long serialVersionUID = -3719337365430743297L;

	private boolean isSuccess;

	private String responseMessage;

	private SSMASMMessegeDTO ssiMessegeDTO;

	private boolean isACKNACMessage = false;

	private boolean isScheduleAlreadyInSession;

	private boolean adHocFlightUpdate;

	private Collection<SSMMessageProcessStatus> scheduleProcessStatus;

	private int updatedScheduleCount = 0;

	private int updatedAdhocScheduleCount = 0;

	/** Creates a new instance of SegmentSellMessageDTO */
	public SSMASMResponse() {

	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public SSMASMMessegeDTO getSsiMessegeDTO() {
		return ssiMessegeDTO;
	}

	public void setSsiMessegeDTO(SSMASMMessegeDTO ssiMessegeDTO) {
		this.ssiMessegeDTO = ssiMessegeDTO;
	}

	public boolean isACKNACMessage() {
		return isACKNACMessage;
	}

	public void setACKNACMessage(boolean isACKNACMessage) {
		this.isACKNACMessage = isACKNACMessage;
	}

	public boolean isScheduleAlreadyInSession() {
		return isScheduleAlreadyInSession;
	}

	public void setScheduleAlreadyInSession(boolean isScheduleAlreadyInSession) {
		this.isScheduleAlreadyInSession = isScheduleAlreadyInSession;
	}

	public boolean isAdHocFlightUpdate() {
		return adHocFlightUpdate;
	}

	public void setAdHocFlightUpdate(boolean adHocFlightUpdate) {
		this.adHocFlightUpdate = adHocFlightUpdate;
	}

	public Collection<SSMMessageProcessStatus> getScheduleProcessStatus() {
		if (scheduleProcessStatus == null) {
			scheduleProcessStatus = new ArrayList<SSMMessageProcessStatus>();
		}
		return scheduleProcessStatus;
	}

	public void addScheduleProcessStatus(SSMMessageProcessStatus scheduleProcessStatus) {
		getScheduleProcessStatus().add(scheduleProcessStatus);
	}

	public int getUpdatedScheduleCount() {
		return updatedScheduleCount;
	}

	public void setUpdatedScheduleCount(int updatedScheduleCount) {
		this.updatedScheduleCount = updatedScheduleCount;
	}

	public int getUpdatedAdhocScheduleCount() {
		return updatedAdhocScheduleCount;
	}

	public void setUpdatedAdhocScheduleCount(int updatedAdhocScheduleCount) {
		this.updatedAdhocScheduleCount = updatedAdhocScheduleCount;
	}

}
