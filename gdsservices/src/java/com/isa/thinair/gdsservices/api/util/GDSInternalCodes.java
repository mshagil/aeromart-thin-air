package com.isa.thinair.gdsservices.api.util;

/**
 * @author Nilindra Fernando
 * @since 7/7/2008
 */
public class GDSInternalCodes {
	public enum ActionCode {
		ONHOLD_REPLY_REQUIRED("ONHOLD_REPLY_REQUIRED"), ONHOLD("ONHOLD"), CONFIRM("CONFIRM");

		private ActionCode(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum StatusCode {
		NO_ACTION("NOACTION"), CONFIRMED("CONFIRMED"), CONFIRMED_SCHEDULE_CHANGED("CONFIRMEDSCHEDULECHANGED"), EXISTS("EXISTS"), DUPLICATE(
				"DUPLICATE"), NOT_OPERATED_PROVIDED("NOTOPERATEDPROVIDED"), NOT_AVAILABLE("NOTAVAILABLE"), CANCELLED("CANCELLED"), ERROR(
				"ERROR");

		private StatusCode(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum ReservationAction {
		CREATE_BOOKING(1), ADD_SEGMENT(2), MODIFY_SEGMENT(3), CANCEL_SEGMENT(4), CHANGE_PAX_DETAILS(5), CHANGE_CONTACT_DETAILS(6), ADD_INFANT(
				7), CANCEL_RESERVATION(8), LOCATE_RESERVATION(9), SPLIT_RESERVATION(10), UPDATE_EXTERNAL_SEGMENTS(11), UPDATE_STATUS(
				12), CONFIRM_BOOKING(13), ADD_SSR(14), REMOVE_SSR(15), MAKE_PAYMENT(16), CHECK_OLD_SEGMENTS(17), REMOVE_PAX(18), ISSUE_ETICKET(
				19), SPLIT_RESERVATION_RESP(20), CREATE_BOOKING_RESP(21), RESERVATION_RESP(22), TRANSFER_SEGMENT(23), EXCHANGE_ETICKET(
				24), EXTEND_TIMELIMIT(25);

		private int id;

		ReservationAction(int id) {
			this.id = id;
		}

		public int getCode() {
			return id;
		}
	}
	
	public enum GDSNotifyAction {
		NO_ACTION(0), ONHOLD_RELEASE(1), ONHOLD_PAY_CONFIRM(2), NAME_CHANGE(3), CANCEL_RESERVATION(4), CANCEL_SEGMENT(5), ADD_SEGMENT(
				6), REMOVE_PAX(7), SPLIT_PAX(8), ADD_INFANT(9), MODIFY_SSR(10), CHANGE_PAX_DETAILS(11), EXTEND_ONHOLD(12), CHANGE_CONTACT_DETAILS(
				13), REMOVE_PAX_INFANT(14), REMOVE_INFANT(15), CHANGE_USER_NOTE(16), TRANSFER_SEGMENT(17), CABIN_CHANGED(18), CREATE_ONHOLD(
				19), TICKETING(20), MODIFY_SEGMENT(21), NO_TICKET_CHANGE(22);

		private int id;

		GDSNotifyAction(int id) {
			this.id = id;
		}

		public int getCode() {
			return id;
		}
		
		public static GDSNotifyAction resolveGDSNotifyActionType(Integer id){
			for (GDSNotifyAction gdsNotifyAction : GDSNotifyAction.values()) {
				if (gdsNotifyAction.getCode() == id) {
					return gdsNotifyAction;
				}
			}
			return NO_ACTION;
		}
	}

	public enum SegmentType {
		SINGLE_SEGMENT(1), ALTERNATE_SEGMENTS(2), INFO_SEGMENT(3);

		private int id;

		SegmentType(int id) {
			this.id = id;
		}

		public int getCode() {
			return id;
		}
	}

	public enum ValidateConstants {
		SUCCESS("V"), FAILURE("F"), INVALID("G");

		private ValidateConstants(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}
	}

	public enum ResponseMessageCode {
		CONFIRMED_CC_PAYMENT("gdsservices.response.message.confirmed.card.payment"),
		CONFIRMED_ON_HOLD("gdsservices.response.message.confirmed.onhold"),
		CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS("gdsservices.response.message.confirmed.onhold.nopaymentDetails"),
		CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS_WITH_SSRADTK("gdsservices.response.message.confirmed.onhold.nopaymentDetails.withSSRADTK"),
		CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS_IN_BUFFERTIME("gdsservices.response.message.confirmed.onhold.nopaymentDetails.buffertime"),
		CONFIRMED_TICKETING("gdsservices.response.message.confirmed.ticketing"),
		EXTEND_ON_HOLD("gdsservices.response.message.extend.timelimit"),
		CANCELED_ON_HOLD("gdsservices.response.message.canceled.onhold"),
		CONFIRMED_BSP_PAYMENT("gdsservices.response.message.confirmed.bsp.payment"),
		MODIFY_CC_PAYMENT("gdsservices.response.message.modify.card.payment"),
		MODIFY_BSP_PAYMENT("gdsservices.response.message.modify.bsp.payment"),
		MODIFY_FORCE_CONFIRM("gdsservices.response.message.modify.forceconfirm"),
		SSR_NOT_SUPPORTED("gdsservices.response.message.ssr.notsupported"),
		INVALID_DATE_RANGE("gdsservices.response.message.schedule.copy.invalid.daterange"),
		ALL_SEGMENTS_INVALID("gdsservices.response.message.allsegments.invalid"),
		INVALID_LEG_DURATION("gdsservices.response.message.schedule.invalidduration"),
		SCHEDULE_SCHEDULE_CONFLICT("gdsservices.response.message.schedule.schedule.overlapped"),
		SCHEDULE_FLIGHT_CONFLICT("gdsservices.response.message.schedule.flight.overlapped"),
		INVALID_OVERLAPPING_SEGMENT_LEG("gdsservices.response.message.schedule.overlapsegment.invalid"),
		FLIGHT_CREATION_UPDATION_FAILED("gdsservices.response.message.flight.updation.warning"),
		RESERVATIONS_FOUND_FOR_LEGCHANGED_FLIGHT("gdsservices.response.message.leg.change.not.allowed"),
		SCHEDULE_FLIGHT_ROUTE_NOT_DEFINED("gdsservices.response.message.leg.data.invalid");
		

		private ResponseMessageCode(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum PaymentType {
		CREDIT_CARD("CC"), BSP("BSP"), ON_ACCOUNT("AC");

		private PaymentType(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum GDSStatus {
		ACT("ACT"), INA("INA");

		private String status;

		private GDSStatus(String status) {
			this.status = status;
		}

		public String getCode() {
			return status;
		}
	}

	public enum TypeBMessageDirection {
		REQUEST, RESPONSE
	}

	public enum TypeBMessageCategory {
		SCHEDULE,
		RESERVATION,
		SYNC_IN, // sent by gds
		SYNC_OUT, // sent to gds
		CODESHARE_OUT // sent to codeshare partner
	}

	public enum TypeBSyncType {
		TYPE_1 (1), TYPE_2 (2);

		private int id;

		TypeBSyncType(int id) {
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public static TypeBSyncType getTypeBSyncType(int id) {
			for (TypeBSyncType typeBSyncType : TypeBSyncType.values()) {
				if (id == typeBSyncType.id) {
					return typeBSyncType;
				}
			}
			return null;
		}
	}
}
