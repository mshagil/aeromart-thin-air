package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

public class RecordLocatorDTO implements Serializable {

	private static final long serialVersionUID = -9076687884797730422L;

	private String bookingOffice;
	private String pnrReference;
	private String taOfficeCode;

	private String userID;

	private String closestCityAirportCode;

	private String carrierCRSCode;

	private String userType;

	private String countryCode;

	private String currencyCode;

	private String agentDutyCode;

	private String erspId;

	private String firstDepartureFrom;
	
    private String pointOfSales;

	public String getBookingOffice() {
		return this.bookingOffice;
	}

	public void setBookingOffice(String bookingOffice) {
		this.bookingOffice = bookingOffice;
	}

	public String getPnrReference() {
		return this.pnrReference;
	}

	public void setPnrReference(String pnrReference) {
		this.pnrReference = pnrReference;
	}

	public String getUserID() {
		return this.userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentDutyCode() {
		return agentDutyCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentDutyCode(String agentCode) {
		this.agentDutyCode = agentCode;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode
	 *            the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the erspId
	 */
	public String getErspId() {
		return erspId;
	}

	/**
	 * @param erspId
	 *            the erspId to set
	 */
	public void setErspId(String erspId) {
		this.erspId = erspId;
	}

	/**
	 * @return the pointOfSales
	 */
	public String getPointOfSales() {
		return pointOfSales;
	}

	/**
	 * @param pointOfSales the pointOfSales to set
	 */
	public void setPointOfSales(String pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the taOfficeCode
	 */
	public String getTaOfficeCode() {
		return taOfficeCode;
	}

	/**
	 * @param taOfficeCode
	 *            the taOfficeCode to set
	 */
	public void setTaOfficeCode(String taOfficeCode) {
		this.taOfficeCode = taOfficeCode;
	}

	/**
	 * @return the closestCityAirportCode
	 */
	public String getClosestCityAirportCode() {
		return closestCityAirportCode;
	}

	/**
	 * @param closestCityAirportCode
	 *            the closestCityAirportCode to set
	 */
	public void setClosestCityAirportCode(String closestCityAirportCode) {
		this.closestCityAirportCode = closestCityAirportCode;
	}

	/**
	 * @return the carrierCRSCode
	 */
	public String getCarrierCRSCode() {
		return carrierCRSCode;
	}

	/**
	 * @param carrierCRSCode
	 *            the carrierCRSCode to set
	 */
	public void setCarrierCRSCode(String carrierCRSCode) {
		this.carrierCRSCode = carrierCRSCode;
	}

	/**
	 * @return the firstDepartureFrom
	 */
	public String getFirstDepartureFrom() {
		return firstDepartureFrom;
	}

	/**
	 * @param firstDepartureFrom
	 *            the firstDepartureFrom to set
	 */
	public void setFirstDepartureFrom(String firstDepartureFrom) {
		this.firstDepartureFrom = firstDepartureFrom;
	}
}
