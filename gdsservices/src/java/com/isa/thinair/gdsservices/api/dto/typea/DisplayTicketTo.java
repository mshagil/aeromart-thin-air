package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;
import java.util.Date;

public class DisplayTicketTo implements Serializable {

	private String passengerName;
	private String passengerSurname;
	private String passengerType;

	private String pnr;
	private String externalRecLocator;
	private String gdsCarrierCode;

	private String ticketNumber;

	private String couponNumber;
	private String couponStatus;

	private Date flightDepartureDate;
	private String origin;
	private String destination;
	private String flightNumber;
	private String bookingDesignator;


	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public String getPassengerSurname() {
		return passengerSurname;
	}

	public void setPassengerSurname(String passengerSurname) {
		this.passengerSurname = passengerSurname;
	}

	public String getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getExternalRecLocator() {
		return externalRecLocator;
	}

	public void setExternalRecLocator(String externalRecLocator) {
		this.externalRecLocator = externalRecLocator;
	}

	public String getGdsCarrierCode() {
		return gdsCarrierCode;
	}

	public void setGdsCarrierCode(String gdsCarrierCode) {
		this.gdsCarrierCode = gdsCarrierCode;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public Date getFlightDepartureDate() {
		return flightDepartureDate;
	}

	public void setFlightDepartureDate(Date flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getBookingDesignator() {
		return bookingDesignator;
	}

	public void setBookingDesignator(String bookingDesignator) {
		this.bookingDesignator = bookingDesignator;
	}
}
