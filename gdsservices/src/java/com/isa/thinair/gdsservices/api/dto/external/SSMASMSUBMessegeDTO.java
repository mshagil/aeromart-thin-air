package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;

public class SSMASMSUBMessegeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum Action {
		NEW, CNL, RPL, ADM, EQT, FLT, REV, TIM, SKD, NAC, ACK
	};

	// TODO:use com.isa.thinair.commons.api.constants.DayOfWeek
	public enum DayOfWeek {
		MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6), SUNDAY(7);

		private final int dayNumber;

		DayOfWeek(int dayNumber) {
			this.dayNumber = dayNumber;
		}

		public int dayNumber() {
			return dayNumber;
		}
	}

	private String referenceNumber;

	private Action action;
	private boolean withdrawal = false;

	private String flightDesignator;
	private Date startDate;
	private Date endDate;
	private Collection<DayOfWeek> daysOfOperation;

	private String serviceType;
	private String aircraftType;
	private String passengerResBookingDesignator;

	private String senderAddress = null;
	private String recipientAddress = null;
	private String timestamp = null;

	private List<SSMASMFlightLegDTO> legs;

	private boolean codeShareCarrier;

	private String timeMode;

	private String operatingCarrier;

	private String messageSequenceReference;

	private String creatorReference;

	private MessageType messageType;

	private String externalFlightNo;

	private boolean validRequest = true;

	private String errorMessage;

	private Date newStartDate;
	private Date newEndDate;
	private Collection<DayOfWeek> newDaysOfOperation;

	private boolean ownSchedule;

	private String newFlightDesignator;

	private String subSupplementaryInfo;

	private List<String> marketingFlightNos;

	private String aircraftConfiguration;

	/** ------------- Action Information --------------- */
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public boolean isWithdrawal() {
		return withdrawal;
	}

	public void setWithdrawal(boolean withdrawal) {
		this.withdrawal = withdrawal;
	}

	/** ------------------ Flight Information ------------ */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	/** ------------------ Period and Frequency -------- */
	public Date getStartDate() {
		if (startDate != null) {
			startDate = CalendarUtil.getStartTimeOfDate(startDate);
		}
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		if (endDate != null) {
			endDate = CalendarUtil.getEndTimeOfDate(endDate);
		}
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Collection<DayOfWeek> getDaysOfOperation() {
		return daysOfOperation;
	}

	public void setDaysOfOperation(Collection<DayOfWeek> daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}

	public void addDayOfWeek(DayOfWeek day) {
		if (daysOfOperation == null)
			daysOfOperation = new ArrayList<DayOfWeek>();
		daysOfOperation.add(day);
	}

	/** ------------------ Equipment Information ------- */
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getPassengerResBookingDesignator() {
		return passengerResBookingDesignator;
	}

	public void setPassengerResBookingDesignator(String passengerResBookingDesignator) {
		this.passengerResBookingDesignator = passengerResBookingDesignator;
	}

	/** ------------------ Legs Information ------- */
	public Collection<SSMASMFlightLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<SSMASMFlightLegDTO> legs) {
		this.legs = legs;
	}

	public void addLeg(SSMASMFlightLegDTO ssiFlightLeg) {
		if (this.legs == null)
			this.legs = new ArrayList<SSMASMFlightLegDTO>();
		this.legs.add(ssiFlightLeg);
	}

	/** ------------------ Reference Information ------- */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @param senderAddress
	 *            the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	/**
	 * @return the recipientAddress
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}

	/**
	 * @param recipientAddress
	 *            the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getMessageSequenceReference() {
		return messageSequenceReference;
	}

	public void setMessageSequenceReference(String messageSequenceReference) {
		this.messageSequenceReference = messageSequenceReference;
	}

	public String getCreatorReference() {
		return creatorReference;
	}

	public void setCreatorReference(String creatorReference) {
		this.creatorReference = creatorReference;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getExternalFlightNo() {
		return externalFlightNo;
	}

	public void setExternalFlightNo(String externalFlightNo) {
		this.externalFlightNo = externalFlightNo;
	}

	public boolean isValidRequest() {
		return validRequest;
	}

	public void setValidRequest(boolean validRequest) {
		this.validRequest = validRequest;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getNewStartDate() {
		if (newStartDate != null) {
			newStartDate = CalendarUtil.getStartTimeOfDate(newStartDate);
		}
		return newStartDate;
	}

	public void setNewStartDate(Date newStartDate) {
		this.newStartDate = newStartDate;
	}

	public Date getNewEndDate() {
		if (newEndDate != null) {
			newEndDate = CalendarUtil.getEndTimeOfDate(newEndDate);
		}
		return newEndDate;
	}

	public void setNewEndDate(Date newEndDate) {
		this.newEndDate = newEndDate;
	}

	public Collection<DayOfWeek> getNewDaysOfOperation() {
		return newDaysOfOperation;
	}

	public void setNewDaysOfOperation(Collection<DayOfWeek> newDaysOfOperation) {
		this.newDaysOfOperation = newDaysOfOperation;
	}

	public boolean isOwnSchedule() {
		return ownSchedule;
	}

	public void setOwnSchedule(boolean ownSchedule) {
		this.ownSchedule = ownSchedule;
	}

	public String getNewFlightDesignator() {
		return newFlightDesignator;
	}

	public void setNewFlightDesignator(String newFlightDesignator) {
		this.newFlightDesignator = newFlightDesignator;
	}

	public String getSubSupplementaryInfo() {
		return subSupplementaryInfo;
	}

	public void setSubSupplementaryInfo(String subSupplementaryInfo) {
		this.subSupplementaryInfo = subSupplementaryInfo;
	}

	public List<String> getMarketingFlightNos() {
		return marketingFlightNos;
	}

	public void setMarketingFlightNos(List<String> marketingFlightNos) {
		this.marketingFlightNos = marketingFlightNos;
	}

	public void addMarketingFlightNos(String marketingFlightNos) {
		if (this.marketingFlightNos == null)
			this.marketingFlightNos = new ArrayList<String>();
		this.marketingFlightNos.add(marketingFlightNos);
	}

	public String getAircraftConfiguration() {
		return aircraftConfiguration;
	}

	public void setAircraftConfiguration(String aircraftConfiguration) {
		this.aircraftConfiguration = aircraftConfiguration;
	}

}
