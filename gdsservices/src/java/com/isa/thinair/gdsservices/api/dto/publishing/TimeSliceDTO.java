package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author janandith jayawardena
 * 
 */
public class TimeSliceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3617879434103691032L;
	private Date startDate;
	private Date endDate;

	private int dSTOffset;

	private String airportCode;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getdSTOffset() {
		return dSTOffset;
	}

	public void setdSTOffset(int dSTOffset) {
		this.dSTOffset = dSTOffset;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

}
