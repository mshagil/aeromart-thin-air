package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

/**
 * @author Manoj
 */
public class ExchangeEticketRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = 1L;

	private Collection<Passenger> passengers;

	private Collection<SpecialServiceRequest> commonSSRs;

	public ExchangeEticketRequest() {
		passengers = new ArrayList<Passenger>();
		commonSSRs = new ArrayList<SpecialServiceRequest>();
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.EXCHANGE_ETICKET;
	}

	/**
	 * @return the passengers
	 */
	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the commonSSRs
	 */
	public Collection<SpecialServiceRequest> getCommonSSRs() {
		return commonSSRs;
	}

	/**
	 * @param commonSSRs
	 *            the commonSSRs to set
	 */
	public void setCommonSSRs(Collection<SpecialServiceRequest> commonSSRs) {
		this.commonSSRs = commonSSRs;
	}
}
