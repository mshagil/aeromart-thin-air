package com.isa.thinair.gdsservices.api.dto.external;

public class SSRMinorDTO extends SSRDTO {
	private static final long serialVersionUID = 6954223418041672953L;

	private String firstName;

	private String lastName;

	private String title;

	/** Age in years. */
	private int minorAge;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the minorAge
	 */
	public int getMinorAge() {
		return minorAge;
	}

	/**
	 * @param minorAge
	 *            the minorAge to set
	 */
	public void setMinorAge(int minorAge) {
		this.minorAge = minorAge;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
