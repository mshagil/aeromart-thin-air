package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class CancelReservationRequest extends GDSReservationRequestBase{
	private static final long serialVersionUID = -6163100792335338121L;

	private Collection<Segment> segments;

	public CancelReservationRequest() {
		segments = new ArrayList<Segment>();
	}

	/**
	 * @return the segments
	 */
	public Collection<Segment> getSegments() {
		return segments;
	}

	/**
	 * @param segments
	 *            the segments to set
	 */
	public void setSegments(Collection<Segment> segments) {
		this.segments = segments;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.CANCEL_RESERVATION;
	}
}
