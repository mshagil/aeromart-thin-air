package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.util.Map;

import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;

public class ReIssueTicketRs extends InitBaseRs {

	private int gdsId;
	private Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions; // pax-sequence -> segment-sequence -> transition

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public Map<Integer, Map<Integer, TransitionTo<Coupon>>> getTransitions() {
		return transitions;
	}

	public void setTransitions(Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions) {
		this.transitions = transitions;
	}

}
