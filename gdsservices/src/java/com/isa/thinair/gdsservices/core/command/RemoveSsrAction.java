package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.RemoveSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class RemoveSsrAction {
	private static Log log = LogFactory.getLog(RemoveSsrAction.class);

	public static RemoveSsrRequest processRequest(RemoveSsrRequest removeSsrRequest) {
		try {

			// 1. Retrieve Reservation ---
			Reservation reservation = CommonUtil.loadReservation(removeSsrRequest.getGdsCredentialsDTO(), removeSsrRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(removeSsrRequest.getGdsCredentialsDTO(), reservation);

			String nameKey = null;
			Passenger passenger = null;
			Map<String, Passenger> passengerMap = CommonUtil.getPassengerMap(removeSsrRequest.getPassengers());

			Collection<ReservationPax> resPassengers = reservation.getPassengers();

			Map<Integer, SegmentSSRAssembler> paxSSRMap = new HashMap<Integer, SegmentSSRAssembler>();
			for (ReservationPax resPax : resPassengers) {
				nameKey = GDSApiUtils.getIATAName(resPax.getLastName(), resPax.getFirstName(), resPax.getTitle());
				passenger = passengerMap.get(nameKey);

				if (passenger != null) {
					deleteSsrInfo(resPax, passenger, reservation, paxSSRMap);
				} else if (passengerMap.size() == 0) {
					deleteSsrInfoForAllPax(resPax, removeSsrRequest.getCommonSSRs(), reservation, paxSSRMap);
				}
			}

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().modifySSRs(paxSSRMap,
					reservation.getPnr(), "", null, (int) reservation.getVersion(), true, EXTERNAL_CHARGES.INFLIGHT_SERVICES,
					CommonUtil.getTrackingInfo(removeSsrRequest.getGdsCredentialsDTO()), true);

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				removeSsrRequest.setSuccess(true);
			} else {
				removeSsrRequest.setSuccess(false);
				removeSsrRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				removeSsrRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
			}

		} catch (ModuleException e) {
			log.error(" RemoveSsrAction Failed for GDS PNR " + removeSsrRequest.getOriginatorRecordLocator().getPnrReference(), e);
			removeSsrRequest.setSuccess(false);
			removeSsrRequest.setResponseMessage(e.getMessageString());
			removeSsrRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" RemoveSsrAction Failed for GDS PNR " + removeSsrRequest.getOriginatorRecordLocator().getPnrReference(), e);
			removeSsrRequest.setSuccess(false);
			removeSsrRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			removeSsrRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return removeSsrRequest;

	}

	private static void deleteSsrInfoForAllPax(ReservationPax resPax, Collection<SpecialServiceRequest> commonSSRs,
			Reservation reservation, Map<Integer, SegmentSSRAssembler> paxSSRMap) {

		if (commonSSRs != null && commonSSRs.size() > 0) {
			for (SpecialServiceRequest ssr : commonSSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (ssdr.getSegment() != null) {
						for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								if (reservationPaxFareSegment.getReservationPaxSegmentSSRs() != null
										&& reservationPaxFareSegment.getReservationPaxSegmentSSRs().size() > 0) {
									for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
										if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
											if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
													&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
													&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr
															.getSegment().getDepartureDate())
													&& seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber())) {
												for (ReservationPaxSegmentSSR paxSegmentSSR : reservationPaxFareSegment
														.getReservationPaxSegmentSSRs()) {
													String SSrCode = SSRUtil.getSSRCode(paxSegmentSSR.getSSRId());
													if (SSrCode.equals(ssr.getCode())) {
														SegmentSSRAssembler ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
														if (ipaxSSR == null) {
															paxSSRMap.put(resPax.getPaxSequence(), new SegmentSSRAssembler());
															ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
														}
														ipaxSSR.addCanceledSegemntSSRCodeMap(
																paxSegmentSSR.getPnrPaxSegmentSSRId(), ssr.getCode());
														ipaxSSR.addCanceledPaxSegmentSSR(paxSegmentSSR.getPnrPaxSegmentSSRId());
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private static void deleteSsrInfo(ReservationPax resPax, Passenger passenger, Reservation reservation,
			Map<Integer, SegmentSSRAssembler> paxSSRMap) {

		Collection<SpecialServiceRequest> sSRs = passenger.getSsrCollection();

		if (passenger != null) {
			for (SpecialServiceRequest ssr : sSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (ssdr.getSegment() != null) {
						for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								if (reservationPaxFareSegment.getReservationPaxSegmentSSRs() != null
										&& reservationPaxFareSegment.getReservationPaxSegmentSSRs().size() > 0) {
									for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
										if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
											if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
													&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
													&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr
															.getSegment().getDepartureDate())
													&& seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber())) {
												for (ReservationPaxSegmentSSR paxSegmentSSR : reservationPaxFareSegment
														.getReservationPaxSegmentSSRs()) {
													String SSrCode = SSRUtil.getSSRCode(paxSegmentSSR.getSSRId());
													if (SSrCode.equals(ssr.getCode())) {
														SegmentSSRAssembler ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
														if (ipaxSSR == null) {
															paxSSRMap.put(resPax.getPaxSequence(), new SegmentSSRAssembler());
															ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
														}
														ipaxSSR.addCanceledSegemntSSRCodeMap(
																paxSegmentSSR.getPnrPaxSegmentSSRId(), ssr.getCode());
														ipaxSSR.addCanceledPaxSegmentSSR(paxSegmentSSR.getPnrPaxSegmentSSRId());
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

}
