package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class CreateResponseTransformer {
	private static final ReservationAction RESERVATION_ACTION = ReservationAction.CREATE_BOOKING;

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap, ReservationAction reservationAction) {

		CreateReservationRequest createRequest = (CreateReservationRequest) reservationResponseMap
				.get(reservationAction != null ? reservationAction : RESERVATION_ACTION);

		if (createRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			addResponderPNR(bookingRequestDTO, createRequest);
			bookingRequestDTO = setSegmentStatuses(bookingRequestDTO, createRequest);
			bookingRequestDTO = setScheduleChanges(bookingRequestDTO, createRequest);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, createRequest);
			if(reservationAction.equals(ReservationAction.LOCATE_RESERVATION)) {
				bookingRequestDTO.setResponseMessageIdentifier(GDSExternalCodes.MessageIdentifier.ACKNOWLEDGEMENT.getCode());
			} else {
				bookingRequestDTO.setResponseMessageIdentifier(GDSExternalCodes.MessageIdentifier.LINK_RECORD_LOCATOR.getCode());
			}
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, createRequest);
			bookingRequestDTO.setResponseMessageIdentifier(null);
		}
		bookingRequestDTO.getErrors().addAll(createRequest.getErrorCode());

		return bookingRequestDTO;
	}

	/**
	 * Adds responder record locater details to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param createRequest
	 */
	private static void addResponderPNR(BookingRequestDTO bookingRequestDTO, CreateReservationRequest createRequest) {
		RecordLocatorDTO responderRecordLocator = new RecordLocatorDTO();

		responderRecordLocator.setPnrReference(createRequest.getResponderRecordLocator().getPnrReference());
		bookingRequestDTO.setResponderRecordLocator(responderRecordLocator);

	}

	/**
	 * adds responder record locater details to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param createRequest
	 */
	private static BookingRequestDTO setSegmentStatuses(BookingRequestDTO bookingRequestDTO,
			CreateReservationRequest createRequest) {

		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<SegmentDetail> segmentDetails = createRequest.getNewSegmentDetails();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (SegmentDetail segmentDetail : segmentDetails) {
				Segment segment = segmentDetail.getSegment();
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(segment,
							bookingSegmentDTO.getActionOrStatusCode());
					bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
				}
			}
		}

		return bookingRequestDTO;
	}

	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {
		return getResponse(bookingRequestDTO, reservationResponseMap, RESERVATION_ACTION);
	}
	
	private static BookingRequestDTO setScheduleChanges (BookingRequestDTO bookingRequestDTO,
			CreateReservationRequest createRequest) {
		
		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<SegmentDetail> segmentDetails = createRequest.getNewSegmentDetails();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (SegmentDetail segmentDetail : segmentDetails) {
				Segment segment = segmentDetail.getSegment();
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					if (segmentDetail.isChangeInSceduleExists()) {
						bookingSegmentDTO.setChangeInScheduleExist(true);
						bookingSegmentDTO.setDepartureTime(segment.getDepartureTime());
						bookingSegmentDTO.setArrivalTime(segment.getArrivalTime());
						if (segment.getArrivalDayOffset() < 0) {
							bookingSegmentDTO.setDayOffSet(Math.abs(segment.getArrivalDayOffset()));
							bookingSegmentDTO.setDayOffSetSign("M");
						} else {
							bookingSegmentDTO.setDayOffSet(segment.getArrivalDayOffset());
						}
					}
				}
			}
		}
		
		return bookingRequestDTO;
		
	}
}
