package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.ReScheduleRequiredInfo;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO.Action;

public class SchedulePublishBL {

	private static final Log log = LogFactory.getLog(SchedulePublishBL.class);

	private static AirportBD getAirportBD() {
		return GDSServicesModuleUtil.getAirportBD();
	}

	public static void sendSSMForSchedulePeriodChanges(ReScheduleRequiredInfo reScheduleInfo) throws ModuleException {

		if (reScheduleInfo != null) {
			Map gdsStatusMap = reScheduleInfo.getGdsStatusMap();
			String airLineCode = reScheduleInfo.getAirLineCode();
			Map bcMap = reScheduleInfo.getBcMap();
			FlightSchedule updatedSchedule = reScheduleInfo.getUpdatedSchedule();
			FlightSchedule existingSchedule = reScheduleInfo.getExistingSchedule();
			Collection<SSMSplitFlightSchedule> existingScheduleColl = reScheduleInfo.getExistingSubSchedules();
			Collection<SSMSplitFlightSchedule> updatedScheduleColl = reScheduleInfo.getUpdatedSubSchedules();

			existingScheduleColl = SchedulePublishUtil.addSubSchedule(existingScheduleColl, existingSchedule);
			updatedScheduleColl = SchedulePublishUtil.addSubSchedule(updatedScheduleColl, updatedSchedule);

			List<SSMSplitFlightSchedule> updatedScheduleList = new ArrayList<SSMSplitFlightSchedule>(updatedScheduleColl);
			List<SSMSplitFlightSchedule> existingScheduleList = new ArrayList<SSMSplitFlightSchedule>(existingScheduleColl);
			SSMSplitFlightSchedule updatedSubSchedule = null;
			String supplementaryInfo = reScheduleInfo.getSupplementaryInfo();

			boolean isActioned = false;

			if (reScheduleInfo.containsAction(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME)
					&& isScheduleRemainSame(updatedScheduleList, existingScheduleList)) {
				SSMSplitFlightSchedule existingSubSchedule = null;
				if (updatedScheduleList.size() == 1) {
					updatedSubSchedule = updatedScheduleList.get(0);
					existingSubSchedule = existingScheduleList.get(0);
				}
				sendSSMRevisionBySubSchedules(gdsStatusMap, updatedSchedule, airLineCode, existingSchedule, updatedSubSchedule,
						existingSubSchedule, supplementaryInfo);
				isActioned = true;
			}

			if (reScheduleInfo.containsAction(GDSScheduleEventCollector.LEG_TIME_CHANGE)
					&& isScheduleRemainSame(updatedScheduleList, existingScheduleList)) {
				if (updatedScheduleList.size() == 1) {
					updatedSubSchedule = updatedScheduleList.get(0);
				}
				generateSSMTimeChange(gdsStatusMap, reScheduleInfo.getAircraftTypeMap(), reScheduleInfo.getServiceType(),
						updatedSchedule, airLineCode, reScheduleInfo.getPublishedGdsIds(), bcMap, updatedSubSchedule,
						supplementaryInfo);
				isActioned = true;
			}

			if (!isActioned) {
				sendSSMForReSchedules(reScheduleInfo);
			}
		}

	}

	@SuppressWarnings("rawtypes")
	public static void sendSSMForReSchedules(ReScheduleRequiredInfo reScheduleInfo) throws ModuleException {

		if (reScheduleInfo != null) {
			Map gdsStatusMap = reScheduleInfo.getGdsStatusMap();
			Map aircraftTypeMap = reScheduleInfo.getAircraftTypeMap();
			String serviceType = reScheduleInfo.getServiceType();
			String airLineCode = reScheduleInfo.getAirLineCode();
			Collection updatedList = reScheduleInfo.getPublishedGdsIds();
			Map bcMap = reScheduleInfo.getBcMap();
			FlightSchedule updatedSchedule = reScheduleInfo.getUpdatedSchedule();
			FlightSchedule existingSchedule = reScheduleInfo.getExistingSchedule();
			Collection<SSMSplitFlightSchedule> existingScheduleColl = reScheduleInfo.getExistingSubSchedules();
			Collection<SSMSplitFlightSchedule> updatedScheduleColl = reScheduleInfo.getUpdatedSubSchedules();
			boolean isCancelExistingSubSchedule = reScheduleInfo.isCancelExistingSubSchedule();
			String supplementaryInfo = reScheduleInfo.getSupplementaryInfo();

			existingScheduleColl = SchedulePublishUtil.addSubSchedule(existingScheduleColl, existingSchedule);
			updatedScheduleColl = SchedulePublishUtil.addSubSchedule(updatedScheduleColl, updatedSchedule);

			List<SSMSplitFlightSchedule> updatedScheduleList = new ArrayList<SSMSplitFlightSchedule>(updatedScheduleColl);
			List<SSMSplitFlightSchedule> existingScheduleList = new ArrayList<SSMSplitFlightSchedule>(existingScheduleColl);
			Collections.sort(updatedScheduleList);
			Collections.sort(existingScheduleList);

			List<SSMSplitFlightSchedule> cnxScheduleList = new ArrayList<SSMSplitFlightSchedule>();
			List<SSMSplitFlightSchedule> matchedScheduleList = new ArrayList<SSMSplitFlightSchedule>();

			// trimmed
			if (!isCancelExistingSubSchedule) {
				for (SSMSplitFlightSchedule existingScheduleTemp : existingScheduleList) {

					Collection<SSMSplitFlightSchedule> possibleMataches = getMatchingSchedule(updatedScheduleList,
							existingScheduleTemp, matchedScheduleList);

					if (possibleMataches == null || possibleMataches.isEmpty()) {
						cnxScheduleList.add(existingScheduleTemp);
						// CNX schedules
					} else {
						boolean scheduleMatched = false;
						for (SSMSplitFlightSchedule updatedScheduleTemp : possibleMataches) {
							addLocalTimeDetailsToFlightSchedule(existingSchedule, existingScheduleTemp);
							addLocalTimeDetailsToFlightSchedule(updatedSchedule, updatedScheduleTemp);

							boolean isLegLocalTimeChanged = LegUtil.isLegLocalTimeChanged(existingScheduleTemp,
									updatedScheduleTemp);

							if (isScheduleRevisedNew(updatedScheduleTemp, existingScheduleTemp, existingSchedule,
									updatedSchedule, isLegLocalTimeChanged)) {
								matchedScheduleList.add(updatedScheduleTemp);
								sendSSMRevisionBySubSchedules(gdsStatusMap, updatedSchedule, airLineCode, existingSchedule,
										updatedScheduleTemp, existingScheduleTemp, supplementaryInfo);
								scheduleMatched = true;
								break;
							} else if (isScheduleTimeChanged(updatedScheduleTemp, existingScheduleTemp, existingSchedule,
									updatedSchedule, isLegLocalTimeChanged)) {
								matchedScheduleList.add(updatedScheduleTemp);
								generateSSMTimeChange(gdsStatusMap, aircraftTypeMap, serviceType, updatedSchedule, airLineCode,
										updatedList, bcMap, updatedScheduleTemp, supplementaryInfo);
								scheduleMatched = true;
								break;
							} else if (isScheduleRemainSame(updatedScheduleTemp, existingScheduleTemp, existingSchedule,
									updatedSchedule, isLegLocalTimeChanged)) {
								matchedScheduleList.add(updatedScheduleTemp);
								scheduleMatched = true;
								break;
							}

						}

						if (!scheduleMatched) {
							cnxScheduleList.add(existingScheduleTemp);
						}

					}
				}
			} else if (AppSysParamsUtil.isSendSsmCnlMessageForReSchedule()) {
				cnxScheduleList.addAll(existingScheduleColl);
			}

			// sending SSM-CNL for cancelled schedules
			sendSSMForCnxSchedules(gdsStatusMap, aircraftTypeMap, serviceType, airLineCode, updatedList, bcMap, cnxScheduleList,
					existingSchedule, supplementaryInfo);

			// sending SSM-NEW for new schedules
			sendSSMForNewSchedules(gdsStatusMap, aircraftTypeMap, serviceType, airLineCode, updatedList, bcMap,
					updatedScheduleList, matchedScheduleList, updatedSchedule, supplementaryInfo);
		}

	}

	@SuppressWarnings("rawtypes")
	public static void publishSSMNew(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedule,
			String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule, String supplementaryInfo)
			throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.NEW, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, null, null, supplementaryInfo);
	}

	@SuppressWarnings("rawtypes")
	public static void pulishSSMCancel(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedule,
			String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule, String supplementaryInfo)
			throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.CNL, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, null, null, supplementaryInfo);

	}

	@SuppressWarnings("rawtypes")
	public static void generateSSMRevisionMsg(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedule,
			String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule,
			FlightSchedule existingSchedule, SSMSplitFlightSchedule existingSubSchedule, String supplementaryInfo)
			throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.REV, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, existingSchedule, existingSubSchedule, supplementaryInfo);

	}

	@SuppressWarnings("rawtypes")
	public static void generateSSMAircraftModelChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType,
			FlightSchedule schedule, String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule,
			String supplementaryInfo) throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.EQT, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, null, null, supplementaryInfo);

	}

	@SuppressWarnings("rawtypes")
	public static void generateSSMTimeChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedule,
			String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule, String supplementaryInfo)
			throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.TIM, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, null, null, supplementaryInfo);

	}

	@SuppressWarnings("rawtypes")
	public static void generateSSMReplacementMsg(Map gdsStatusMap, Map aircraftTypeMap, String serviceType,
			FlightSchedule schedule, String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule,
			String supplementaryInfo) throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.RPL, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, null, null, supplementaryInfo);

	}

	@SuppressWarnings("rawtypes")
	public static void generateSSMFlightNumberChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType,
			FlightSchedule schedule, String airLineCode, Collection updatedList, Map bcMap, SSMSplitFlightSchedule subSchedule,
			String supplementaryInfo, FlightSchedule existingSchedule) throws ModuleException {

		sendSSMMessageForGDS(SSIMessegeDTO.Action.FLT, gdsStatusMap, aircraftTypeMap, serviceType, schedule, airLineCode,
				updatedList, bcMap, subSchedule, existingSchedule, null, supplementaryInfo);

	}

	private static void addLocalTimeDetailsToFlightSchedule(FlightSchedule schedule, SSMSplitFlightSchedule subSchedule)
			throws ModuleException {
		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, subSchedule);
	}

	private static Collection<SSMSplitFlightSchedule> getMatchingSchedule(List<SSMSplitFlightSchedule> updatedScheduleList,
			SSMSplitFlightSchedule existingSchedule, List<SSMSplitFlightSchedule> matchedScheduleList) {

		Collection<SSMSplitFlightSchedule> possibleMataches = new ArrayList<SSMSplitFlightSchedule>();

		for (SSMSplitFlightSchedule updatedScheduleTemp : updatedScheduleList) {
			if (updatedScheduleTemp != null) {
				if (CalendarUtil.isSameDay(existingSchedule.getStartDate(), updatedScheduleTemp.getStartDate())
						|| CalendarUtil.isSameDay(existingSchedule.getStopDate(), updatedScheduleTemp.getStopDate())) {
					possibleMataches.add(updatedScheduleTemp);
				}
			}
		}
		return possibleMataches;
	}

	@SuppressWarnings("rawtypes")
	private static void sendSSMForNewSchedules(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, String airLineCode,
			Collection updatedList, Map bcMap, List<SSMSplitFlightSchedule> updatedScheduleList,
			List<SSMSplitFlightSchedule> matchedScheduleList, FlightSchedule updatedSchedule, String supplementaryInfo)
			throws ModuleException {
		if (!matchedScheduleList.isEmpty()) {
			updatedScheduleList.removeAll(matchedScheduleList);
			removeAlreadyMatchedSchedules(updatedScheduleList, matchedScheduleList);
		}

		if (!updatedScheduleList.isEmpty()) {
			for (SSMSplitFlightSchedule subSchedule : updatedScheduleList) {
				publishSSMNew(gdsStatusMap, aircraftTypeMap, serviceType, updatedSchedule, airLineCode, updatedList, bcMap,
						subSchedule, supplementaryInfo);
			}

		}

	}

	@SuppressWarnings("rawtypes")
	public static void sendSSMForCnxSchedules(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, String airLineCode,
			Collection updatedList, Map bcMap, List<SSMSplitFlightSchedule> cnxScheduleList, FlightSchedule existingSchedule,
			String supplementaryInfo) throws ModuleException {
		if (!cnxScheduleList.isEmpty()) {
			for (SSMSplitFlightSchedule subSchedule : cnxScheduleList) {
				pulishSSMCancel(gdsStatusMap, aircraftTypeMap, serviceType, existingSchedule, airLineCode, updatedList, bcMap,
						subSchedule, supplementaryInfo);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private static void sendSSMRevisionBySubSchedules(Map gdsStatusMap, FlightSchedule updatedSchedule, String airLineCode,
			FlightSchedule existingSchedule, SSMSplitFlightSchedule updatedSubSchedule,
			SSMSplitFlightSchedule existingSubSchedule, String supplementaryInfo) throws ModuleException {

		generateSSMRevisionMsg(gdsStatusMap, null, null, updatedSchedule, airLineCode, existingSchedule.getGdsIds(), null,
				updatedSubSchedule, existingSchedule, existingSubSchedule, supplementaryInfo);

	}

	private static void sendSSMMessageForGDS(SSIMessegeDTO.Action actionType, Map gdsStatusMap, Map aircraftTypeMap,
			String serviceType, FlightSchedule schedule, String airLineCode, Collection updatedList, Map bcMap,
			SSMSplitFlightSchedule subSchedule, FlightSchedule existingSchedule, SSMSplitFlightSchedule existingSubSchedule,
			String supplementaryInfo) throws ModuleException {
		SSIMessegeDTO newSSM = new SSIMessegeDTO();
		populateSSMMessageDTO(actionType, aircraftTypeMap, schedule, subSchedule, newSSM, existingSchedule, existingSubSchedule,
				supplementaryInfo);

		Iterator itGdsIds = updatedList.iterator();

		while (itGdsIds.hasNext()) {
			Integer gdsId = (Integer) itGdsIds.next();

			if (bcMap != null && bcMap.get(gdsId.toString()) != null) {
				SchedulePublishUtil.addRBDInfo(newSSM, (Collection) bcMap.get(gdsId.toString()));
			}
			GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

			GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(gdsId, XMLStreamer.compose(newSSM),
					newSSM.getReferenceNumber(), schedule.getScheduleId());

			if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
				sedSSM(gdsStatus.getGdsCode(), airLineCode, newSSM, pubMessage);
			} else {
				pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
				getGDSServiceDAO().saveGDSPublisingMessage(pubMessage);
			}
		}

	}

	private static void populateSSMMessageDTO(SSIMessegeDTO.Action actionType, Map aircraftTypeMap, FlightSchedule schedule,
			SSMSplitFlightSchedule subSchedule, SSIMessegeDTO newSSM, FlightSchedule existingSchedule,
			SSMSplitFlightSchedule existingSubSchedule, String supplementaryInfo) throws ModuleException {
		switch (actionType) {
		case NEW:
			SchedulePublishUtil.populateSSIMessageDTOforNEW(newSSM, schedule, null,
					(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);
			break;
		case CNL:
			SchedulePublishUtil.populateSSIMessageDTOforCNL(newSSM, schedule, null,
					(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);
			break;
		case TIM:
			SchedulePublishUtil.populateSSIMessageDTOforTIM(newSSM, schedule, null,
					(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);
			break;
		case EQT:
			SchedulePublishUtil.populateSSIMessageDTOforEQT(newSSM, schedule, null,
					(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);
			break;
		case REV:
			SchedulePublishUtil.populateSSIMessageDTOforREV(newSSM, schedule, existingSchedule, subSchedule, existingSubSchedule,
					supplementaryInfo);
			break;
		case RPL:
			SchedulePublishUtil.populateSSIMessageDTOforRPL(newSSM, schedule, null,
					(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);
			break;
		case FLT:
			SchedulePublishUtil.populateSSIMessageDTOforFLT(newSSM, schedule, null, existingSchedule,
					(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);
			break;

		default:
			break;
		}
	}

	private static void sedSSM(String gdsCode, String airLineCode, SSIMessegeDTO newSSM, GDSPublishMessage pubMessage) {

		try {
			GDSServicesModuleUtil.getSSMASMServiceBD().sendStandardScheduleMessage(gdsCode, airLineCode, newSSM);
			getGDSServiceDAO().saveGDSPublisingMessage(pubMessage);

		} catch (Exception e) {
			pubMessage.setRemarks("Message broker invocation failed");
			pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
			getGDSServiceDAO().saveGDSPublisingMessage(pubMessage);
		}
	}

	private static GDSServicesDAO getGDSServiceDAO() {
		return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	private static boolean isScheduleTimeChanged(SSMSplitFlightSchedule updated, SSMSplitFlightSchedule existing,
			FlightSchedule existingScheduleCopied, FlightSchedule updatedScheduleCopied, boolean isLegLocalTimeChanged)
			throws ModuleException {

		if ((CalendarUtil.isSameDay(updatedScheduleCopied.getStartDateLocal(), existingScheduleCopied.getStartDateLocal())
				&& CalendarUtil.isSameDay(updatedScheduleCopied.getStopDateLocal(), existingScheduleCopied.getStopDateLocal()) && FrequencyUtil
					.isEqualFrequency(updatedScheduleCopied.getFrequencyLocal(), existingScheduleCopied.getFrequencyLocal()))
				&& isLegLocalTimeChanged) {

			return true;
		}
		return false;

	}

	private static boolean isScheduleRevisedNew(SSMSplitFlightSchedule updatedSubSchedule,
			SSMSplitFlightSchedule existingSubSchedule, FlightSchedule existingScheduleCopied,
			FlightSchedule updatedScheduleCopied, boolean isLegLocalTimeChanged) throws ModuleException {

		if (!(CalendarUtil.isSameDay(updatedScheduleCopied.getStartDateLocal(), existingScheduleCopied.getStartDateLocal())
				&& CalendarUtil.isSameDay(updatedScheduleCopied.getStopDateLocal(), existingScheduleCopied.getStopDateLocal()) && FrequencyUtil
					.isEqualFrequency(updatedScheduleCopied.getFrequencyLocal(), existingScheduleCopied.getFrequencyLocal()))
				&& !isLegLocalTimeChanged) {

			return true;
		}
		return false;

	}

	private static boolean isScheduleRemainSame(SSMSplitFlightSchedule updated, SSMSplitFlightSchedule existing,
			FlightSchedule existingScheduleCopied, FlightSchedule updatedScheduleCopied, boolean isLegLocalTimeChanged)
			throws ModuleException {

		if (FrequencyUtil.isEqualFrequency(updatedScheduleCopied.getFrequencyLocal(), existingScheduleCopied.getFrequencyLocal())
				&& !isLegLocalTimeChanged) {
			return true;
		}
		return false;

	}

	@SuppressWarnings("rawtypes")
	public static void sendSSMForScheduleSplit(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, String airLineCode,
			Collection updatedList, Map bcMap, FlightSchedule updatedSchedule, FlightSchedule existingSchedule,
			Collection<SSMSplitFlightSchedule> existingScheduleColl, Collection<SSMSplitFlightSchedule> updatedScheduleColl,
			boolean isCancelExistingSubSchedule, List<SSMSplitFlightSchedule> cnxScheduleList,
			List<SSMSplitFlightSchedule> matchedSubScheduleList, List<SSMSplitFlightSchedule> matchedExistingSchedules,
			String supplementaryInfo) throws ModuleException {

		List<SSMSplitFlightSchedule> updatedScheduleList = new ArrayList<SSMSplitFlightSchedule>(updatedScheduleColl);
		List<SSMSplitFlightSchedule> existingScheduleList = new ArrayList<SSMSplitFlightSchedule>(existingScheduleColl);
		Collections.sort(updatedScheduleList);
		Collections.sort(existingScheduleList);

		// trimmed
		if (!isCancelExistingSubSchedule) {
			for (SSMSplitFlightSchedule existingScheduleTemp : existingScheduleList) {

				Collection<SSMSplitFlightSchedule> possibleMataches = getMatchingSchedule(updatedScheduleList,
						existingScheduleTemp, matchedSubScheduleList);

				if (possibleMataches != null && !possibleMataches.isEmpty()) {

					for (SSMSplitFlightSchedule updatedScheduleTemp : possibleMataches) {
						addLocalTimeDetailsToFlightSchedule(existingSchedule, existingScheduleTemp);
						addLocalTimeDetailsToFlightSchedule(updatedSchedule, updatedScheduleTemp);

						boolean isLegLocalTimeChanged = LegUtil.isLegLocalTimeChanged(existingScheduleTemp, updatedScheduleTemp);

						if (isScheduleTimeChanged(updatedScheduleTemp, existingScheduleTemp, existingSchedule, updatedSchedule,
								isLegLocalTimeChanged)) {
							matchedSubScheduleList.add(updatedScheduleTemp);
							generateSSMTimeChange(gdsStatusMap, aircraftTypeMap, serviceType, updatedSchedule, airLineCode,
									updatedList, bcMap, updatedScheduleTemp, supplementaryInfo);
							matchedExistingSchedules.add(existingScheduleTemp);
							break;
						} else if (isScheduleRemainSame(updatedScheduleTemp, existingScheduleTemp, existingSchedule,
								updatedSchedule, isLegLocalTimeChanged)) {
							matchedSubScheduleList.add(updatedScheduleTemp);
							matchedExistingSchedules.add(existingScheduleTemp);
							break;
						}

					}

				}
			}
		} else if (AppSysParamsUtil.isSendSsmCnlMessageForReSchedule()) {
			cnxScheduleList.addAll(existingScheduleColl);
		}

		if (existingScheduleColl != null && !existingScheduleColl.isEmpty() && matchedExistingSchedules != null
				&& !matchedExistingSchedules.isEmpty()) {
			existingScheduleColl.removeAll(matchedExistingSchedules);
			removeAlreadyMatchedSchedules(existingScheduleColl, matchedExistingSchedules);
		}

		// sending SSM-NEW for new schedules
		sendSSMForNewSchedules(gdsStatusMap, aircraftTypeMap, serviceType, airLineCode, updatedList, bcMap, updatedScheduleList,
				matchedSubScheduleList, updatedSchedule, supplementaryInfo);

	}

	private static boolean isScheduleRemainSame(List<SSMSplitFlightSchedule> updatedScheduleList,
			List<SSMSplitFlightSchedule> existingScheduleList) {

		if (updatedScheduleList.size() == existingScheduleList.size() && existingScheduleList.size() <= 1) {
			return true;
		}
		return false;
	}

	public static void removeAlreadyMatchedSchedules(Collection<SSMSplitFlightSchedule> updatedScheduleList,
			Collection<SSMSplitFlightSchedule> matchedScheduleList) {

		Iterator<SSMSplitFlightSchedule> updatedScheduleItr = updatedScheduleList.iterator();
		while (updatedScheduleItr.hasNext()) {
			SSMSplitFlightSchedule updatedScheduleTemp = updatedScheduleItr.next();
			if (isMatchFound(updatedScheduleTemp, matchedScheduleList)) {
				updatedScheduleItr.remove();
			}
		}
	}

	private static boolean isMatchFound(SSMSplitFlightSchedule updatedScheduleTemp,
			Collection<SSMSplitFlightSchedule> matchedScheduleList) {
		for (SSMSplitFlightSchedule matchedScheduleTemp : matchedScheduleList) {
			if (updatedScheduleTemp.getStartDate().compareTo(matchedScheduleTemp.getStartDate()) == 0
					&& updatedScheduleTemp.getStopDate().compareTo(matchedScheduleTemp.getStopDate()) == 0) {
				return true;
			}
		}
		return false;
	}

	public static void sendSsmSubMessagesForUpdate(ReScheduleRequiredInfo reScheduleInfo) throws ModuleException {
		if (reScheduleInfo.getActions() != null) {
			// multiple actions
			// CNL//NEW
			Map aircraftTypeMap = reScheduleInfo.getAircraftTypeMap();
			Map gdsStatusMap = reScheduleInfo.getGdsStatusMap();
			String airLineCode = reScheduleInfo.getAirLineCode();
			Collection updatedList = reScheduleInfo.getPublishedGdsIds();
			Map bcMap = reScheduleInfo.getBcMap();

			Map<Integer, SSIMessegeDTO> ssmMessegeDTOMap = new HashMap<>();
			if (reScheduleInfo.containsAction(GDSScheduleEventCollector.SPLIT_SCHEDULE)) {

				Collection<FlightSchedule> splitSchedules = reScheduleInfo.getSplitSchedules();
				if (splitSchedules.size() == 1) {
					for (FlightSchedule schedule : splitSchedules) {
						publishSSMNew(gdsStatusMap, aircraftTypeMap, null, schedule, airLineCode, updatedList, bcMap, null,
								reScheduleInfo.getSupplementaryInfo());
					}
				} else if (splitSchedules.size() > 1) {
					SSIMessegeDTO cnlMessegeDTO = new SSIMessegeDTO();
					populateSSMMessageDTO(SSIMessegeDTO.Action.CNL, aircraftTypeMap, reScheduleInfo.getExistingSchedule(), null,
							cnlMessegeDTO, null, null, null);
					ssmMessegeDTOMap.put(1, cnlMessegeDTO);

					int i = 2;
					for (FlightSchedule schedule : splitSchedules) {
						SSIMessegeDTO newMessegeDTO = new SSIMessegeDTO();
						populateSSMMessageDTO(SSIMessegeDTO.Action.NEW, aircraftTypeMap, schedule, null, newMessegeDTO, null,
								null, null);
						ssmMessegeDTOMap.put(i, newMessegeDTO);
						i++;

					}
				}

			} else if (reScheduleInfo.getActions().size() > 1) {

				if (reScheduleInfo.isCancelExistingSchedule()) {
					/**
					 * 
					 */
					SSIMessegeDTO cnlMessegeDTO = new SSIMessegeDTO();
					populateSSMMessageDTO(SSIMessegeDTO.Action.CNL, aircraftTypeMap, reScheduleInfo.getExistingSchedule(), null,
							cnlMessegeDTO, null, null, null);
					ssmMessegeDTOMap.put(new Integer(1), cnlMessegeDTO);

					SSIMessegeDTO newMessegeDTO = new SSIMessegeDTO();
					populateSSMMessageDTO(SSIMessegeDTO.Action.NEW, aircraftTypeMap, reScheduleInfo.getUpdatedSchedule(), null,
							newMessegeDTO, null, null, null);
					ssmMessegeDTOMap.put(new Integer(2), newMessegeDTO);
				} else {

					Collection<String> actions = reScheduleInfo.getActions();
					int i = 1;
					boolean isRPLGenerated = false;
					for (String action : actions) {

						Action actionEnum = null;
						if (GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED.equals(action)) {
							actionEnum = Action.FLT;
						} else if (!isRPLGenerated
								&& (GDSScheduleEventCollector.REPLACE_EXISTING_INFO.equals(action) || GDSScheduleEventCollector.LEG_ADDED_DELETED
										.equals(action))) {
							actionEnum = Action.RPL;
							isRPLGenerated = true;
						} else if (GDSScheduleEventCollector.MODEL_CHANGED.equals(action)
								&& !actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)) {
							actionEnum = Action.EQT;
						} else if (GDSScheduleEventCollector.LEG_TIME_CHANGE.equals(action)) {
							actionEnum = Action.TIM;
						} else if (GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME.equals(action)) {
							actionEnum = Action.REV;
						}

						if (actionEnum != null) {
							SSIMessegeDTO newMessegeDTO = new SSIMessegeDTO();
							populateSSMMessageDTO(actionEnum, aircraftTypeMap, reScheduleInfo.getUpdatedSchedule(), null,
									newMessegeDTO, reScheduleInfo.getExistingSchedule(), null,
									reScheduleInfo.getSupplementaryInfo());
							ssmMessegeDTOMap.put(i, newMessegeDTO);
							i++;
						}

					}

				}

			}

			if (ssmMessegeDTOMap != null && !ssmMessegeDTOMap.isEmpty()) {
				Iterator itGdsIds = updatedList.iterator();

				while (itGdsIds.hasNext()) {
					Integer gdsId = (Integer) itGdsIds.next();
					Collection<String> bookingClassList = null;
					if (bcMap != null && bcMap.get(gdsId.toString()) != null) {
						bookingClassList = (Collection) bcMap.get(gdsId.toString());
					}
					GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

					GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(gdsId,
							XMLStreamer.compose(ssmMessegeDTOMap), "REF", reScheduleInfo.getExistingSchedule().getScheduleId());

					if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
						GDSServicesModuleUtil.getSSMASMServiceBD().sendStandardScheduleSubMessages(gdsStatus.getGdsCode(),
								airLineCode, ssmMessegeDTOMap, bookingClassList);
					} else {
						pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
						getGDSServiceDAO().saveGDSPublisingMessage(pubMessage);
					}
				}
			}

		}
	}
	
	public static void genarateSSMMessageDTO(SSIMessegeDTO.Action actionType, Map aircraftTypeMap, FlightSchedule schedule,
			SSMSplitFlightSchedule subSchedule, SSIMessegeDTO newSSM, FlightSchedule existingSchedule,
			SSMSplitFlightSchedule existingSubSchedule, String supplementaryInfo) throws ModuleException{
		
		populateSSMMessageDTO(actionType, aircraftTypeMap, schedule, subSchedule, newSSM, existingSchedule, existingSubSchedule, supplementaryInfo);
		
	}
	
	public static void cancelOldScheduleAndPublishNewSchedule(ReScheduleRequiredInfo reScheduleInfo)
			throws ModuleException {
		if (reScheduleInfo != null) {

			Map aircraftTypeMap = reScheduleInfo.getAircraftTypeMap();
			Map gdsStatusMap = reScheduleInfo.getGdsStatusMap();
			String airLineCode = reScheduleInfo.getAirLineCode();
			Collection updatedList = reScheduleInfo.getPublishedGdsIds();
			Map bcMap = reScheduleInfo.getBcMap();

			pulishSSMCancel(gdsStatusMap, aircraftTypeMap, null, reScheduleInfo.getExistingSchedule(), airLineCode,
					updatedList, bcMap, null, null);

			publishSSMNew(gdsStatusMap, aircraftTypeMap, null, reScheduleInfo.getUpdatedSchedule(), airLineCode,
					updatedList, bcMap, null, null);

		}
	}
}
