package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.KeyGenerator;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.NumberOfUnits;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PrintTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(PrintTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	private LCCClientReservation reservation;
	private String ticketNumber;
	private GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;
	private List<TicketNumberDetails> couponTransitions = new ArrayList<TicketNumberDetails>();


	protected void handleMessage() {

		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		tktreqMessage = tktReq.getMessage();

		tktRes = new AATKTRES();
		tktresMessage = new TKTRESMessage();

		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));


		try {

			preHandleMessage();
			printTicket();
			postHandleMessage();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.PRINT);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.PRINT);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);
		}

		messageDTO.setResponse(tktRes);
	}

	private void preHandleMessage() throws GdsTypeAException {

		GDSServicesSupportJDBCDAO gdsServicesSupportDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;
		String pnr;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		ticketNumber = tktReq.getMessage().getTickets().get(0).getTicketNumber();

		pnr = gdsServicesSupportDao.getPnrFromExternalTicketNumber(ticketNumber);

		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
		 	if (reservation != null) {
				throw new ModuleException(TypeANotes.ErrorMessages.GENERIC_ERROR);
		    }
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

	}

	private void printTicket() throws GdsTypeAException {

		String settlementAuthCode = null;
		int couponCount;
		TravellerTicketingInformation printTicket;

		TravellerTicketingInformationWrapper travellerWrapper = resolvePassenger(gdsReservation, ticketNumber);
		TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();

		settlementAuthCode = KeyGenerator.generateSettlementAuthorizationCode();
		couponCount = 0;

		TKTRESMessage tktresMessage = tktRes.getMessage();

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.PRINT);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

		tktresMessage.setOriginatorInformation(travellerWrapper.getOriginatorInformation());
		tktresMessage.setTicketingAgentInformation(travellerWrapper.getTicketingAgentInformation());
		tktresMessage.getReservationControlInformation().addAll(gdsReservation.getReservationControlInformation());
		tktresMessage.getMonetaryInformation().addAll(travellerImage.getMonetaryInformation());
		tktresMessage.setPricingTicketingDetails(travellerImage.getPricingTicketingDetails());
		tktresMessage.getFormOfPayment().addAll(travellerImage.getFormOfPayment());
		tktresMessage.getTaxDetails().addAll(travellerImage.getTaxDetails());
		tktresMessage.getText().addAll(travellerImage.getText());

		printTicket = new TravellerTicketingInformation();
		printTicket.setTravellerSurname(travellerImage.getTravellerSurname());
		printTicket.setTravellerCount(travellerImage.getTravellerCount());
		printTicket.setTravellerQualifier(travellerImage.getTravellerQualifier());
		printTicket.getTravellers().addAll(travellerImage.getTravellers());

		TicketNumberDetails ticket;
		for (TicketNumberDetails ticketImage : travellerImage.getTickets()) {
			ticket = new TicketNumberDetails();
			ticket.setTicketNumber(ticketImage.getTicketNumber());
			ticket.setDocumentType(ticketImage.getDocumentType());

			TicketNumberDetails couponTransitionTicket = new TicketNumberDetails();
			couponTransitionTicket.setTicketNumber(ticketImage.getTicketNumber());
			couponTransitionTicket.setDocumentType(ticketImage.getDocumentType());
			couponTransitions.add(couponTransitionTicket);

			for (TicketCoupon coupon : ticketImage.getTicketCoupon()) {
				coupon.setSettlementAuthCode(settlementAuthCode);
				coupon.setStatus(TypeAConstants.CouponStatus.PRINTED);

				TicketCoupon couponTransitionCoupon = new TicketCoupon();
				couponTransitionCoupon.setCouponNumber(coupon.getCouponNumber());
				couponTransitionCoupon.setStatus(coupon.getStatus());
				couponTransitionCoupon.setSettlementAuthCode(coupon.getSettlementAuthCode());
				couponTransitionCoupon.setFlightInfomation(coupon.getFlightInfomation());
				couponTransitionTicket.getTicketCoupon().add(couponTransitionCoupon);
			}

			couponCount += ticketImage.getTicketCoupon().size();
			ticket.getTicketCoupon().addAll(ticketImage.getTicketCoupon());
		}

		NumberOfUnits numberOfUnits = new NumberOfUnits();
		numberOfUnits.setQualifier(TypeAConstants.NumberOfUnitsQualifier.COUPONS_COUNT);
		numberOfUnits.setQuantity(String.valueOf(couponCount));
        printTicket.setNumberOfUnits(numberOfUnits);

		List<TravellerTicketingInformation> travellerTicketingInfo = tktresMessage.getTravellerTicketingInformation();
		travellerTicketingInfo.add(printTicket);

		tktRes.setMessage(tktresMessage);
	}


	private void postHandleMessage() throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();
		ServiceResponce serviceResponce;

		TravellerTicketingInformationWrapper travellerWrapper = resolvePassenger(gdsReservation, ticketNumber);
		TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();

		travellerWrapper.setOriginatorInformation(tktreqMessage.getOriginatorInformation());

		GDSDTOUtil.mergeCouponTransitions(travellerWrapper.getCouponTransitions(), couponTransitions, tktreqMessage.getOriginatorInformation());

		GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);

		try {
			List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger.getMergedETicketCoupons(tktReq.getMessage().getTravellerTicketingInformation(), reservation);
			List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

			serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);

			if (!serviceResponce.isSuccess()) {
				throw new ModuleException(TypeANotes.ErrorMessages.GENERIC_ERROR);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

	}

	private TravellerTicketingInformationWrapper resolvePassenger(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                              String ticketNumber) {
		TravellerTicketingInformationWrapper travellerWrapper = null;

		l0:
		for (TravellerTicketingInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					travellerWrapper = traveller;
					break l0;
				}
			}
		}

		return travellerWrapper;
	}
}
