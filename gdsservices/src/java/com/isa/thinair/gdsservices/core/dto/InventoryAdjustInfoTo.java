package com.isa.thinair.gdsservices.core.dto;

import java.io.Serializable;

public class InventoryAdjustInfoTo implements Serializable {

	private static final String DATA_DELIM = "_";

	private int paxCount;
	private String origin;
	private String destination;

	public InventoryAdjustInfoTo() {
	}

	public InventoryAdjustInfoTo(String uniqueKey) {
		String[] data = uniqueKey.split(DATA_DELIM);
		paxCount = Integer.parseInt(data[0]);
		origin = data[1];
		destination = data[2];
	}

	public int getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getUniqueKey() {
		return paxCount + DATA_DELIM + origin + DATA_DELIM + destination;
	}

	public String getPartialUniqueKey() {
		return origin + DATA_DELIM + destination;
	}
}
