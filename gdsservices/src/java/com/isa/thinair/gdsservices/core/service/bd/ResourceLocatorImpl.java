package com.isa.thinair.gdsservices.core.service.bd;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.core.service.ResourceLocator;

public class ResourceLocatorImpl implements ResourceLocator {

	private static Log log = LogFactory.getLog(ResourceLocatorImpl.class);

	private static ResourceLocator instance;

	// < path , content >
	private Map<String, byte[]> resourceContents = new HashMap<String, byte[]>();

	public static ResourceLocator getInstance() {
		if (instance == null) {
			instance = new ResourceLocatorImpl();
		}
		return instance;
	}

	public byte[] getContent(String path) {
		if (!resourceContents.containsKey(path)) {
			loadContent(path);
		}

		return resourceContents.get(path);
	}


	private void loadContent(String path) {
		byte[] fileContent = null;
		InputStream inputStream = null;

		try {
			inputStream = getClass().getResourceAsStream(path);
			fileContent = IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			log.error("Error occurred while reading file ---- " + path, e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
				}
			}
		}
		resourceContents.put(path, fileContent);
	}
}
