package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.Date;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateHeaders extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		bookingRequestDTO = checkGDS(bookingRequestDTO, MessageUtil.getMessage("gdsservices.validators.unidentified.gds"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

		bookingRequestDTO = checkCommunicationReference(bookingRequestDTO,
				MessageUtil.getMessage("gdsservices.validators.unidentified.communication.ref"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

		bookingRequestDTO = checkMsgReceivedDateStamp(bookingRequestDTO,
				MessageUtil.getMessage("gdsservices.validators.unidentified.message.receive.date"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

		bookingRequestDTO = checkOriginatorAddress(bookingRequestDTO,
				MessageUtil.getMessage("gdsservices.validators.unidentified.originator.address"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

		bookingRequestDTO = checkOriginatorLocator(bookingRequestDTO,
				MessageUtil.getMessage("gdsservices.validators.unidentified.originator.recordlocator"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

		bookingRequestDTO = checkOriginatorLocatorPNR(bookingRequestDTO,
				MessageUtil.getMessage("gdsservices.validators.unidentified.originator.pnr"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

		//This is not madatory.
//		bookingRequestDTO = checkOriginatorLocatorAgentCode(bookingRequestDTO,
//				MessageUtil.getMessage("gdsservices.validators.unidentified.originator.agentcode"));

		if (this.getStatus() != GDSInternalCodes.ValidateConstants.SUCCESS) {
			return bookingRequestDTO;
		}

//		bookingRequestDTO = checkOriginatorLocatorUserId(bookingRequestDTO,
//				MessageUtil.getMessage("gdsservices.validators.unidentified.originator.userid"));

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkOriginatorLocatorUserId(BookingRequestDTO bookingRequestDTO, String message) {
		RecordLocatorDTO recordLocatorDTO = bookingRequestDTO.getOriginatorRecordLocator();
		String userId = GDSApiUtils.maskNull(recordLocatorDTO.getUserID());

		if (userId.length() > 0) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkOriginatorLocatorAgentCode(BookingRequestDTO bookingRequestDTO, String message) {
		RecordLocatorDTO recordLocatorDTO = bookingRequestDTO.getOriginatorRecordLocator();
		String agentCode = GDSApiUtils.maskNull(recordLocatorDTO.getTaOfficeCode());

		if (agentCode.length() > 0) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkOriginatorLocator(BookingRequestDTO bookingRequestDTO, String message) {
		RecordLocatorDTO recordLocatorDTO = bookingRequestDTO.getOriginatorRecordLocator();

		if (recordLocatorDTO != null) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkOriginatorLocatorPNR(BookingRequestDTO bookingRequestDTO, String message) {
		RecordLocatorDTO recordLocatorDTO = bookingRequestDTO.getOriginatorRecordLocator();
		String pnrReference = GDSApiUtils.maskNull(recordLocatorDTO.getPnrReference());

		if (pnrReference.length() > 0) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkOriginatorAddress(BookingRequestDTO bookingRequestDTO, String message) {
		String originatorAddress = GDSApiUtils.maskNull(bookingRequestDTO.getOriginatorAddress());

		if (originatorAddress.length() > 0) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkMsgReceivedDateStamp(BookingRequestDTO bookingRequestDTO, String message) {
		Date date = bookingRequestDTO.getMessageReceivedDateStamp();

		if (date != null) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkCommunicationReference(BookingRequestDTO bookingRequestDTO, String message) {
		String reference = GDSApiUtils.maskNull(bookingRequestDTO.getCommunicationReference());

		if (reference.length() > 0) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO checkGDS(BookingRequestDTO bookingRequestDTO, String message) {
		if (bookingRequestDTO.getGds() == null) {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		}
		return bookingRequestDTO;
	}
}
