package com.isa.thinair.gdsservices.core.typeA.marshallers;

import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.typeA.helpers.StringOutputStream;

public class Marshaller {

	private static Map<String, JAXBContext> mapJAXBContext = new HashMap<String, JAXBContext>();

	public static JAXBElement<?> ediToJava(String ediXML, IATAOperation ediOperation) throws JAXBException {

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		String jaxbContextPackage = typeAMessageConfig.getMessagePath().get(ediOperation.toString());

		JAXBContext jaxbContext = getJAXBContext(jaxbContextPackage);
		JAXBElement<?> jaxbObject = (JAXBElement<?>) jaxbContext.createUnmarshaller().unmarshal(new StringReader(ediXML));

		return jaxbObject;
	}

	public static String javaToEdi(JAXBElement<?> object, IATAOperation ediOperation) throws JAXBException {

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		String jaxbContextPackage = typeAMessageConfig.getMessagePath().get(ediOperation.toString());
		OutputStream output = new StringOutputStream();

		JAXBContext jaxbContext = getJAXBContext(jaxbContextPackage);
		jaxbContext.createMarshaller().marshal(object, output);

		String ediXML = output.toString();
		return ediXML;
	}

	public static JAXBContext getJAXBContext(String jaxbContextPackage) throws JAXBException {
		if (!mapJAXBContext.containsKey(jaxbContextPackage)) {
			mapJAXBContext.put(jaxbContextPackage, JAXBContext.newInstance(jaxbContextPackage));
		}

		return mapJAXBContext.get(jaxbContextPackage);
	}
}