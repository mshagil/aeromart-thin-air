package com.isa.thinair.gdsservices.core.typeb.validators;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;

public class ValidateReservationActions extends ValidatorBase {
	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		GDSInternalCodes.ValidateConstants status = GDSInternalCodes.ValidateConstants.SUCCESS;
		String pnr;
		ExternalReservation externalReservation;

		GDSTypeAServiceDAO gdsTypeAServiceDAO;

		if (bookingRequestDTO.getResponderRecordLocator() != null) {

			pnr = bookingRequestDTO.getResponderRecordLocator().getPnrReference();

			gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
			externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);

			if (externalReservation != null && externalReservation.getReservationSynced().equals(CommonsConstants.NO)) {
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, "ACTION NOT ALLOWED");
				status = GDSInternalCodes.ValidateConstants.FAILURE;
			}
		}

		this.setStatus(status);

		return bookingRequestDTO;
	}
}
