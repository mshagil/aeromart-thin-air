package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.msgbroker.core.bl.TypeBMessageConstants;

public class OSIExtractorUtil {
	/**
	 * sets extract SSR base attributes
	 * 
	 * @param osi
	 * @param osiDTO
	 * @return
	 */
	public static OtherServiceInfo addBaseAttributes(OtherServiceInfo osi, OSIDTO osiDTO) {
		osi.setCode(osiDTO.getCodeOSI());
		osi.setText(osiDTO.getOsiValue());
		osi.setFullElement(osiDTO.getFullElement());

		return osi;
	}

	/**
	 * processes external SSR details and extract information
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	public static Collection<OtherServiceInfo> getOSIs(BookingRequestDTO bookingRequestDTO) {
		Collection<OtherServiceInfo> osis = new ArrayList<OtherServiceInfo>();
		Collection<OSIDTO> osiDTOs = bookingRequestDTO.getOsiDTOs();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<OSIDTO> iterator = osiDTOs.iterator(); iterator.hasNext();) {
			OSIDTO osiDTO = (OSIDTO) iterator.next();

			if (carrierCode.equals(osiDTO.getCarrierCode())
					|| osiDTO.getCarrierCode().equals(TypeBMessageConstants.CommonCodes.LIEU_AIRLINE_DESIGNATOR)) {
				OtherServiceInfo osi = new OtherServiceInfo();
				osi = addBaseAttributes(osi, osiDTO);
				osis.add(osi);
			}
		}

		return osis;
	}
}
