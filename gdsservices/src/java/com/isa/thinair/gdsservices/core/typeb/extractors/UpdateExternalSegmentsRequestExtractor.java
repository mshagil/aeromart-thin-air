package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class UpdateExternalSegmentsRequestExtractor extends ValidatorBase {
	public static UpdateExternalSegmentsRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {

		UpdateExternalSegmentsRequest updateExSegmentRequest = new UpdateExternalSegmentsRequest();
		updateExSegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);

		updateExSegmentRequest = (UpdateExternalSegmentsRequest) RequestExtractorUtil.addBaseAttributes(updateExSegmentRequest,
				bookingRequestDTO);

		updateExSegmentRequest = addExternalSegments(updateExSegmentRequest, bookingRequestDTO);
		updateExSegmentRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return updateExSegmentRequest;
	}

	/**
	 * adds segments to the UpdateExternalSegmentsRequest
	 * 
	 * @param updateExSegmentRequest
	 * @param bookingRequestDTO
	 * @return
	 */
	private static UpdateExternalSegmentsRequest addExternalSegments(UpdateExternalSegmentsRequest updateExSegmentRequest,
			BookingRequestDTO bookingRequestDTO) {

		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getOtherAirlineSegmentDTOs();
		Collection<Segment> externalSegments = new ArrayList<Segment>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			BookingSegmentDTO segmentDTO = (BookingSegmentDTO) iterator.next();

			if (!carrierCode.equals(segmentDTO.getCarrierCode())) {
				String actionOrStatusCode = GDSApiUtils.maskNull(segmentDTO.getActionOrStatusCode());
				Segment segment = RequestExtractorUtil.getSegment(segmentDTO);

				if (actionOrStatusCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD_FREE.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD_IFNOT_HOLDING.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode())) {

					segment.setStatusCode(StatusCode.CONFIRMED);
					// other airline segments
					externalSegments.add(segment);
				} else if (actionOrStatusCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.AdviceCode.CANCELLED.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())
						|| actionOrStatusCode.equals(GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode())) {					
					segment.setStatusCode(StatusCode.CANCELLED);
					// other airline segments
					externalSegments.add(segment);
				}
			}
		}

		updateExSegmentRequest.setExternalSegments(externalSegments);

		return updateExSegmentRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		RequestExtractorUtil.setResponderRecordLocator(bookingRequestDTO);
		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// External segments could be there or not.
			// We should not restrict any external or only internal segments
			// Business assumption: We assume that bookings could be created either via G9 only or with G9 and other
			// airlines.
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}
}
