package com.isa.thinair.gdsservices.core.bl.dsu;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.AmadeusConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SFTPUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.gdsservices.api.dto.publishing.FlightScheduleLegDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.ItineraryVariationScheduleMessageDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.LegTimeSliceDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.ScheduleUpdateMessageDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.StandardScheduleMessageDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.TimeSliceDTO;
import com.isa.thinair.gdsservices.api.model.GDSSchedulePublishStatus;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.TopicConfig;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

/**
 * @author Manoj Dhanushka
 */
public class PublishDSU {

	// BDs
	private ScheduleBD scheduleBD;

	private BookingClassBD bookingClassBD;

	private GdsBD gdsBD;

	private AirportBD airportBD;

	private FlightBD flightBD;

	private AircraftBD aircraftBD;

	private MessagingServiceBD messagingServiceBD;

	int recordSerialNumber = 2;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	private static TemplateEngine engine = new TemplateEngine();

	// Daily Schedule Message Template
	public static final String DAILY_SCHEDULE_UPDATE = "daily_schedule_update";

	// Daily Schedule Message File name
	public static final String FILE_NAME = AppSysParamsUtil.getAmadeusCompatibilityMap().get(AmadeusConstants.DSU_FILE_NAME)
			.toString();

	private static GlobalConfig globalConfig = GDSServicesModuleUtil.getGlobalConfig();

	private static final Log log = LogFactory.getLog(PublishDSU.class);

	public PublishDSU() {
		// looking up BDs
		scheduleBD = GDSServicesModuleUtil.getScheduleBD();
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();
		gdsBD = GDSServicesModuleUtil.getGdsBD();
		airportBD = GDSServicesModuleUtil.getAirportBD();
		flightBD = GDSServicesModuleUtil.getFlightBD();
		aircraftBD = GDSServicesModuleUtil.getAircraftBD();
		messagingServiceBD = GDSServicesModuleUtil.getMessagingServiceBD();

		// looking up DAOs
		gdsServiceDao = (GDSServicesDAO) GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * get All flights and schedules if any flight or schedule has updated after lastPublishedTime Compose the message
	 */
	public void publishDailyScheduleUpdateMessages() throws ModuleException {

		Date lastPublishedTime = null;
		lastPublishedTime = gdsServiceDao.getLastGDSSchedulePublishTime();
		
		List<String> codeShareCarriers = new ArrayList<String>();
		//codeShareCarriers.add("ALL");
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.isCodeShareCarrier()) {
				codeShareCarriers.add(gdsStatusTO.getCarrierCode());
			}
		}

		if (lastPublishedTime == null) {
			// Set lastPublishedTime when using first time
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(2010, 0, 1);
			lastPublishedTime = gc.getTime();
		}
		for (String codeShareCarrier : codeShareCarriers) {
			Collection schedules = scheduleBD.getFlightSchedulesToGDSPublish(lastPublishedTime, codeShareCarrier);
			Collection flights = flightBD.getFlightsToGDSPublish(lastPublishedTime, codeShareCarrier);
			Collection aircrafts = aircraftBD.getActiveAircraftModels();
			HashMap<Integer, List<Flight>> flightsMap = new HashMap<Integer, List<Flight>>();
			List<Flight> adhocFlights = new ArrayList<Flight>();
			if (flights != null) {
				Iterator ite = flights.iterator();
				while (ite.hasNext()) {
					DisplayFlightDTO flt = (DisplayFlightDTO) ite.next();
					Flight flight = flt.getFlight();
					List<Flight> flightList;
					if (flight.getScheduleId() != null && flight.getScheduleId() > 0) {
						flightList = new ArrayList<Flight>();
						flightList.add(flight);
						if (flightsMap.get(flight.getScheduleId()) == null) {
							flightsMap.put(flight.getScheduleId(), flightList);
						} else {
							flightsMap.get(flight.getScheduleId()).addAll(flightList);
						}
					} else {
						adhocFlights.add(flight);
					}
				}
			}
	
			String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
	
			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForScheduleFlight();
	
			List<StandardScheduleMessageDTO> scheduleDTOs = null;
			StandardScheduleMessageDTO scheduleMessageDTO = null;
	
			int itineraryVariationIdentifier = 1;
			// int amadeusGdsId = (gdsBD.getGDSByCode((GDSExternalCodes.GDS.AMADEUS).getCode())).getGdsId();
			int gdsId = 1;
			Map<String, GDSStatusTO> gdsMap = globalConfig.getActiveGdsMap();
			for(GDSStatusTO gdsStatusTO : gdsMap.values()) {
				if (gdsStatusTO.getCarrierCode() != null && gdsStatusTO.getCarrierCode().equals(codeShareCarrier)) {
					gdsId = gdsStatusTO.getGdsId();
				} else if (codeShareCarrier.equals("ALL")) {
					gdsId = 1;
				}
			}
			Collection bcMap;
			int i = 0;
	
			Map<String, String> compatMap = AppSysParamsUtil.getAmadeusCompatibilityMap();
	
			if (compatMap != null && !compatMap.isEmpty()
					&& compatMap.get(AmadeusConstants.DEFAULT_BOOKING_CLASS_ENABLED).equals(CommonsConstants.YES)) {
				bcMap = new ArrayList<String>();
				bcMap.add(compatMap.get(AmadeusConstants.DEFAULT_BOOKING_CLASS));
			} else {
				bcMap = bookingClassBD.getSingleCharBookingClassesForGDS(gdsId);
			}
	
			if (schedules != null) {
				scheduleDTOs = new ArrayList();
				Iterator ite = schedules.iterator();
				while (ite.hasNext()) {
					i++;
					FlightScheduleInfoDTO sched = (FlightScheduleInfoDTO) ite.next();
					scheduleMessageDTO = new StandardScheduleMessageDTO();
					scheduleMessageDTO.setAirlineDesignator(defaultCarrierCode);
					scheduleMessageDTO.setFlightNumber(sched.getFlightNumber());
					scheduleMessageDTO.setServiceType(serviceType);
					scheduleMessageDTO.setBookingClasses(SchedulePublishUtil.getRBDInfo(bcMap));
					scheduleMessageDTO.setAircraftType(SchedulePublishUtil.getIATAAirCraftType(aircrafts, sched.getModelNumber()));
	
					List<ItineraryVariationScheduleMessageDTO> itineraryVariations = new ArrayList<ItineraryVariationScheduleMessageDTO>();
					ItineraryVariationScheduleMessageDTO itineraryMessageDTO;
					if (flightsMap.get(sched.getScheduleId()) == null) {
						itineraryMessageDTO = new ItineraryVariationScheduleMessageDTO();
						itineraryMessageDTO.setFromDateOfPeriod(sched.getStartDate());
						itineraryMessageDTO.setToDateOfPeriod(sched.getStopDate());
						itineraryMessageDTO.setItineraryVariationIdentifier(itineraryVariationIdentifier);
						itineraryVariations.add(composeScheduleLegDetails(itineraryMessageDTO, sched));
					} else {
						List<Flight> fltList = flightsMap.get(sched.getScheduleId());
						itineraryMessageDTO = new ItineraryVariationScheduleMessageDTO();
						itineraryMessageDTO.setFromDateOfPeriod(sched.getStartDate());
						itineraryMessageDTO.setToDateOfPeriod(sched.getStopDate());
						itineraryMessageDTO = composeScheduleLegDetails(itineraryMessageDTO, sched);
						List<ItineraryVariationScheduleMessageDTO> itineraryMessages = splitSchedule(itineraryMessageDTO, fltList);
						itineraryVariations.addAll(itineraryMessages);
					}
					scheduleMessageDTO.setItineraryVariations(itineraryVariations);
					scheduleDTOs.add(scheduleMessageDTO);
				}
			}
	
			if (adhocFlights != null) {
				Iterator ite = adhocFlights.iterator();
				while (ite.hasNext()) {
					i++;
					Flight flight = (Flight) ite.next();
					scheduleMessageDTO = new StandardScheduleMessageDTO();
					scheduleMessageDTO.setAirlineDesignator(defaultCarrierCode);
					scheduleMessageDTO.setFlightNumber(flight.getFlightNumber());
					scheduleMessageDTO.setServiceType(serviceType);
					scheduleMessageDTO.setBookingClasses(SchedulePublishUtil.getRBDInfo(bcMap));
					scheduleMessageDTO.setAircraftType(SchedulePublishUtil.getIATAAirCraftType(aircrafts, flight.getModelNumber()));
	
					List<ItineraryVariationScheduleMessageDTO> itineraryVariations = new ArrayList<ItineraryVariationScheduleMessageDTO>();
					ItineraryVariationScheduleMessageDTO itineraryMessageDTO = new ItineraryVariationScheduleMessageDTO();
					itineraryMessageDTO.setItineraryVariationIdentifier(itineraryVariationIdentifier);
					itineraryMessageDTO.setFromDateOfPeriod(flight.getDepartureDateLocal());
					itineraryMessageDTO.setToDateOfPeriod(flight.getDepartureDateLocal());
					itineraryVariations.add(composeLegDetails(itineraryMessageDTO, flight));
					scheduleMessageDTO.setItineraryVariations(itineraryVariations);
					scheduleDTOs.add(scheduleMessageDTO);
				}
			}
			if (scheduleDTOs != null && scheduleDTOs.size() > 0) {
				Collections.sort(scheduleDTOs, new StandardScheduleMessageDTOComparator());
				int itineraryIdentifier = 0;
				String flightNumber = "";
				for (StandardScheduleMessageDTO sMDTO : scheduleDTOs) {
					sMDTO = getDSTBasedSchedules(sMDTO);
					if (!(sMDTO.getFlightNumber().equals(flightNumber))) {
						itineraryIdentifier = 0;
					}
					for (ItineraryVariationScheduleMessageDTO message : sMDTO.getItineraryVariations()) {
						message.setItineraryVariationIdentifier(++itineraryIdentifier);
						for (FlightScheduleLegDTO leg : message.getLegs()) {
							leg.setRecordSerialNumber(++recordSerialNumber);
						}
					}
					flightNumber = sMDTO.getFlightNumber();
				}
	
				ScheduleUpdateMessageDTO scheduleUpdateMessageDTO = new ScheduleUpdateMessageDTO();
				scheduleUpdateMessageDTO.setTimeMode("L");
				scheduleUpdateMessageDTO.setAirlineDesignator(defaultCarrierCode);
				scheduleUpdateMessageDTO.setFromDateOfPeriodValidity(new Date(System.currentTimeMillis()));
				scheduleUpdateMessageDTO.setToDateOfPeriodValidity(null);
				scheduleUpdateMessageDTO.setCreationDateTime(new Date(System.currentTimeMillis()));
				scheduleUpdateMessageDTO.setSerialNumberCheckReference(recordSerialNumber);
				scheduleUpdateMessageDTO.setEndCode(ScheduleUpdateMessageDTO.END);
				scheduleUpdateMessageDTO.setRecordSerialNumber(++recordSerialNumber);
	
				// Setting the Paramters
				HashMap paramMap = new HashMap();
				paramMap.put("scheduleDTOs", scheduleDTOs);
				paramMap.put("SUMsg", scheduleUpdateMessageDTO);
	
				writeToTemplate(paramMap, gdsId);
			}
		}
	}

	private StandardScheduleMessageDTO getDSTBasedSchedules(StandardScheduleMessageDTO scheduleMsg) {
		List<ItineraryVariationScheduleMessageDTO> finalItenaries = new ArrayList<ItineraryVariationScheduleMessageDTO>();

		AirportBD airportBD = GDSServicesModuleUtil.getAirportBD();
		List<ItineraryVariationScheduleMessageDTO> itenaryList = scheduleMsg.getItineraryVariations();

		for (ItineraryVariationScheduleMessageDTO itenary : itenaryList) {
			List<FlightScheduleLegDTO> legs = itenary.getLegs();
			Map<String, LegTimeSliceDTO> airportWiseEffectiveTimes = new HashMap<String, LegTimeSliceDTO>();
			
			for (FlightScheduleLegDTO leg : legs) {
				
				String departure = leg.getDepartureStation();
				String arrival = leg.getArrivalStation();
				LegTimeSliceDTO arrivalLeg = airportWiseEffectiveTimes.get(arrival);
				LegTimeSliceDTO departureLeg = airportWiseEffectiveTimes.get(departure);

				if (arrivalLeg == null) {
					arrivalLeg = new LegTimeSliceDTO(arrival);
				}
				arrivalLeg.setArrivalTime(leg.getTimeOfArrivalZulu());
//				arrivalLeg.setDepartureDayOffset(leg.getLocalTimeVariationForArrivalSt());

				if (departureLeg == null) {
					departureLeg = new LegTimeSliceDTO(departure);
				}
				departureLeg.setDepartureTime(leg.getTimeOfDepartureZulu());
//				departureLeg.setDepartureDayOffset(leg.getLocalTimeVariationForDepatureSt());

				airportWiseEffectiveTimes.put(departure, departureLeg);
				airportWiseEffectiveTimes.put(arrival, arrivalLeg);			
			}

			List<TimeSliceDTO> airportTimeSlices = DSTUtil.getTimeSlices(airportWiseEffectiveTimes, itenary.getFromDateOfPeriod(),
					itenary.getToDateOfPeriod());

			List<ItineraryVariationScheduleMessageDTO> dstBasedItenaries = DSTUtil.getTimeSlicedItenaries(itenary,
					airportTimeSlices);

			for (ItineraryVariationScheduleMessageDTO itenarySplit : dstBasedItenaries) {
				finalItenaries.add(itenarySplit);
			}
		}

		if (finalItenaries.size() > 0) {
			scheduleMsg.setItineraryVariations(finalItenaries);
		}
		return scheduleMsg;
	}

	/**
	 * generate msg using template TODO: send the message to ftp location save the msg in GDSSchedulePublishStatus
	 */
	private void writeToTemplate(HashMap paramMap, int gdsId) {
	
		TopicConfig topicConfig = (TopicConfig) ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getTopicConfigurationMap().get(DAILY_SCHEDULE_UPDATE);

		String templateFileName = topicConfig.getTemplateFileName();
		StringWriter w = new StringWriter();
		engine.writeTemplate(paramMap, templateFileName, w);

		String dsu = w.toString();
		// Writing to log
		log.info(dsu);
		writeTextFile(dsu);
		sendMail(dsu);
		// ftpFile(dsu);

		GDSSchedulePublishStatus gdsSchedulePublishStatus = new GDSSchedulePublishStatus();
		gdsSchedulePublishStatus.setGdsId(gdsId);
		gdsSchedulePublishStatus.setPublishedTime(new Date(System.currentTimeMillis()));
		gdsSchedulePublishStatus.setStatus("SUCCESS");
		gdsSchedulePublishStatus.setMessage(DataConverterUtil.createBlob(SchedulePublishUtil.nullHandler(dsu)
				.getBytes()));
		gdsServiceDao.saveGDSSchedulePublishStatus(gdsSchedulePublishStatus);
		
	}

	private void sendMail(String dsu) {
		String FROM_ADDRESS = "reservations@isaaviation.ae";
		// TODO : Change to other app parameter
		String TO_ADDRESS = globalConfig.getBizParam(SystemParamKeys.ADMIN_EMAIL_ID);
		String SUBJECT = "Daily Schedule Update";
		try {
			SimpleEmailDTO emailDTO = new SimpleEmailDTO();
			emailDTO.setToEmailAddress(TO_ADDRESS);
			emailDTO.setSubject(SUBJECT);
			emailDTO.setContent(dsu);
			emailDTO.setFromAddress(FROM_ADDRESS);
			messagingServiceBD.sendSimpleTextEmail(emailDTO);
		} catch (Exception me) {
			System.out.print("Error sending email.");
		}
	}

	private void writeTextFile(String dsu) {

		String[] fileInfo = FILE_NAME.split("\\.");

		StringBuilder localFilePath = new StringBuilder();
		localFilePath.append(PlatformConstants.getAbsAttachmentsPath()).append(File.separatorChar);

		if (fileInfo != null && fileInfo.length == 2) {
			localFilePath.append(fileInfo[0]);
			localFilePath.append("_").append(CalendarUtil.getDateInFormattedString("yyyyMMddHHmmss", new Date()));
			localFilePath.append(".").append(fileInfo[1]);
		} else {
			localFilePath.append(FILE_NAME).append("_")
					.append(CalendarUtil.getDateInFormattedString("yyyyMMddHHmmss", new Date()));
		}

		File file = new File(localFilePath.toString());
		try {
			FileUtils.writeStringToFile(file, dsu);
			sftpFile(file);
		} catch (IOException e) {
			log.error("Error occurred in writing DSU file");
		}

	}

	private void sftpFile(File file) {
		// Code to SFTP
		String host = globalConfig.getAmadeusFtpServerName();
		String user = "";
		String password = "";
		int port = 22;
		String remotePath = "";
		if (globalConfig.getAmadeusFtpServerUserName() != null) {
			user = globalConfig.getAmadeusFtpServerUserName();
		}
		if (globalConfig.getAmadeusFtpServerPassword() != null) {
			password = globalConfig.getAmadeusFtpServerPassword();
		}
		if (globalConfig.getAmadeusFtpPort() != null) {
			port = globalConfig.getAmadeusFtpPort();
		}
		if (globalConfig.getAmadeusFtpRemotePath() != null) {
			remotePath = globalConfig.getAmadeusFtpRemotePath();
		}
		try {
			SFTPUtil sftpUtil = new SFTPUtil(host, port, user, password);
			sftpUtil.connectAndLogin();
			sftpUtil.uploadFile(file.getAbsolutePath(), remotePath + "/" + FILE_NAME);
			sftpUtil.closeAndLogout();
//			SFTPUtil.copyRemoteFile(host, user, password, port, remotePath, FILE_NAME, file);
		} catch (JSchException e) {
			log.error("Error in copying DSU file to sftp location.");
		} catch (SftpException e) {
			log.error("Error in copying DSU file to sftp location.");
		}
	}

	private void ftpFile(String dsu) {
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();
		String fileName = "dsu.txt";
		byte[] byteStream = dsu.getBytes();

		// Code to FTP
		serverProperty.setServerName(globalConfig.getAmadeusFtpServerName());
		if (globalConfig.getAmadeusFtpServerUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(globalConfig.getAmadeusFtpServerUserName());
		}
		if (globalConfig.getAmadeusFtpServerPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(globalConfig.getAmadeusFtpServerPassword());
		}
		ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);
	}

	private List<ItineraryVariationScheduleMessageDTO> splitSchedule(ItineraryVariationScheduleMessageDTO sched,
			List<Flight> flights) throws ModuleException {
		List<ItineraryVariationScheduleMessageDTO> itineraryMessages = new ArrayList<ItineraryVariationScheduleMessageDTO>();
		Collections.sort(flights, new FlightComparator());
		int i = 0;
		int lengh = flights.size();
		for (Flight flight : flights) {
			if (SchedulePublishUtil.isEqualStartTimeOfDate(sched.getFromDateOfPeriod(), flight.getDepartureDateLocal())) {
				ItineraryVariationScheduleMessageDTO part1 = new ItineraryVariationScheduleMessageDTO();
				part1.setFromDateOfPeriod(flight.getDepartureDateLocal());
				part1.setToDateOfPeriod(flight.getDepartureDateLocal());
				part1 = composeLegDetails(part1, flight);
				itineraryMessages.add(part1);
				sched.setFromDateOfPeriod(SchedulePublishUtil.getChangedDate(sched.getFromDateOfPeriod(), 1));
				if (i == lengh - 1) {
					itineraryMessages.add(sched);
					break;
				}
			} else if (SchedulePublishUtil.isEqualStartTimeOfDate(sched.getToDateOfPeriod(), flight.getDepartureDateLocal())) {
				sched.setToDateOfPeriod(SchedulePublishUtil.getChangedDate(sched.getToDateOfPeriod(), -1));
				itineraryMessages.add(sched);
				ItineraryVariationScheduleMessageDTO part2 = new ItineraryVariationScheduleMessageDTO();
				part2.setFromDateOfPeriod(flight.getDepartureDateLocal());
				part2.setToDateOfPeriod(flight.getDepartureDateLocal());
				part2 = composeLegDetails(part2, flight);
				itineraryMessages.add(part2);
				break;
			} else if (SchedulePublishUtil.isBetween(flight.getDepartureDateLocal(), sched.getFromDateOfPeriod(),
					sched.getToDateOfPeriod())) {
				ItineraryVariationScheduleMessageDTO part1 = new ItineraryVariationScheduleMessageDTO();
				part1.setFromDateOfPeriod(sched.getFromDateOfPeriod());
				part1.setToDateOfPeriod(SchedulePublishUtil.getChangedDate(flight.getDepartureDateLocal(), -1));
				part1.setLegs(cloneLegList(sched.getLegs()));
				itineraryMessages.add(part1);
				ItineraryVariationScheduleMessageDTO part2 = new ItineraryVariationScheduleMessageDTO();
				part2.setFromDateOfPeriod(flight.getDepartureDateLocal());
				part2.setToDateOfPeriod(flight.getDepartureDateLocal());
				part2 = composeLegDetails(part2, flight);
				itineraryMessages.add(part2);
				sched.setFromDateOfPeriod(SchedulePublishUtil.getChangedDate(flight.getDepartureDateLocal(), 1));
				if (i == lengh - 1) {
					itineraryMessages.add((ItineraryVariationScheduleMessageDTO) sched.clone());
					break;
				}
			}
			i++;
		}
		return itineraryMessages;
	}

	private ItineraryVariationScheduleMessageDTO composeScheduleLegDetails(
			ItineraryVariationScheduleMessageDTO itineraryMessageDTO, FlightScheduleInfoDTO sched) throws ModuleException {

		List<FlightScheduleLegDTO> legs = new ArrayList<FlightScheduleLegDTO>();
		FlightScheduleLegDTO flightScheduleLegDTO;
		Set<FlightScheduleLegInfoDTO> segmentLegs = sched.getFlightScheduleLegs();
		List<FlightScheduleLegInfoDTO> segLeg = new ArrayList<FlightScheduleLegInfoDTO>(segmentLegs);
		Collections.sort(segLeg, new FlightScheduleLegInfoDTOComparator());
		if (segLeg != null) {
			Iterator legIte = segLeg.iterator();

			while (legIte.hasNext()) {

				FlightScheduleLegInfoDTO leg = (FlightScheduleLegInfoDTO) legIte.next();
				flightScheduleLegDTO = new FlightScheduleLegDTO();
				flightScheduleLegDTO.setLegSequenceNumber(leg.getLegNumber());
				flightScheduleLegDTO.setDepartureStation(leg.getOrigin());
				flightScheduleLegDTO.setTimeOfDeparture(leg.getEstDepartureTimeLocal());
				flightScheduleLegDTO.setTimeOfDepartureZulu(leg.getEstDepartureTimeZulu());
				flightScheduleLegDTO.setPassengerTerminalForDepatureSt(SchedulePublishUtil.getTerminalCode(airportBD
						.getDefaultTerminal(leg.getOrigin())));
				flightScheduleLegDTO.setArrivalStation(leg.getDestination());
				flightScheduleLegDTO.setTimeOfArrival(leg.getEstArrivalTimeLocal());
				flightScheduleLegDTO.setTimeOfArrivalZulu(leg.getEstArrivalTimeZulu());
				flightScheduleLegDTO.setPassengerTerminalForArrivalSt(SchedulePublishUtil.getTerminalCode(airportBD
						.getDefaultTerminal(leg.getDestination())));
				flightScheduleLegDTO.setConnectionStatus(SchedulePublishUtil.getInternationalDomesticStatus(airportBD
						.compareCountryOfAirports(leg.getOrigin(), leg.getDestination())));
				flightScheduleLegDTO.setDaysOfOperation(FrequencyUtil.shiftFrequencyBy(sched.getFrequencyLocal(),
						leg.getEstDepartureDayOffsetLocal()));

				if (leg.getEstDepartureDayOffsetLocal() > 0) {
					GregorianCalendar calStart = new GregorianCalendar();
					calStart.setTime(sched.getStartDate());
					calStart.add(Calendar.DATE, leg.getEstDepartureDayOffsetLocal());
					flightScheduleLegDTO.setLegStartDate(calStart.getTime());

					GregorianCalendar calEnd = new GregorianCalendar();
					calEnd.setTime(sched.getStopDate());
					calEnd.add(Calendar.DATE, leg.getEstDepartureDayOffsetLocal());
					flightScheduleLegDTO.setLegEndDate(calEnd.getTime());
				} else {
					flightScheduleLegDTO.setLegStartDate(sched.getStartDate());
					flightScheduleLegDTO.setLegEndDate(sched.getStopDate());
				}
				flightScheduleLegDTO.setLocalTimeVariationForDepatureSt(SchedulePublishUtil.getGMTOffset(
						airportBD.getAirport(leg.getOrigin()), flightScheduleLegDTO.getLegStartDate()));
				flightScheduleLegDTO.setLocalTimeVariationForArrivalSt(SchedulePublishUtil.getGMTOffset(
						airportBD.getAirport(leg.getDestination()), flightScheduleLegDTO.getLegStartDate()));
				legs.add(flightScheduleLegDTO);

			}
			itineraryMessageDTO.setLegs(legs);
		}
		return itineraryMessageDTO;
	}

	private ItineraryVariationScheduleMessageDTO composeLegDetails(ItineraryVariationScheduleMessageDTO itineraryMessageDTO,
			Flight flight) throws ModuleException {

		List<FlightScheduleLegDTO> legs = new ArrayList<FlightScheduleLegDTO>();
		FlightScheduleLegDTO flightScheduleLegDTO;
		Set segLeg = flight.getFlightLegs();
		if (segLeg != null) {
			Iterator legIte = segLeg.iterator();
			while (legIte.hasNext()) {
				FlightLeg leg = (FlightLeg) legIte.next();
				flightScheduleLegDTO = new FlightScheduleLegDTO();
				flightScheduleLegDTO.setLegSequenceNumber(leg.getLegNumber());
				flightScheduleLegDTO.setDepartureStation(leg.getOrigin());
				flightScheduleLegDTO.setTimeOfDeparture(leg.getEstDepartureTimeLocal());
				flightScheduleLegDTO.setTimeOfDepartureZulu(leg.getEstDepartureTimeZulu());
				flightScheduleLegDTO.setPassengerTerminalForDepatureSt(SchedulePublishUtil.getTerminalCode(airportBD
						.getDefaultTerminal(leg.getOrigin())));
				flightScheduleLegDTO.setArrivalStation(leg.getDestination());
				flightScheduleLegDTO.setTimeOfArrival(leg.getEstArrivalTimeLocal());
				flightScheduleLegDTO.setTimeOfArrivalZulu(leg.getEstArrivalTimeZulu());
				flightScheduleLegDTO.setPassengerTerminalForArrivalSt(SchedulePublishUtil.getTerminalCode(airportBD
						.getDefaultTerminal(leg.getDestination())));
				flightScheduleLegDTO.setConnectionStatus(SchedulePublishUtil.getInternationalDomesticStatus(airportBD
						.compareCountryOfAirports(leg.getOrigin(), leg.getDestination())));
				flightScheduleLegDTO.setDaysOfOperation(flight.getDayNumber() + leg.getEstDepartureDayOffsetLocal());

				if (leg.getEstDepartureDayOffsetLocal() > 0) {
					GregorianCalendar calStart = new GregorianCalendar();
					calStart.setTime(flight.getDepartureDateLocal());
					calStart.add(Calendar.DATE, leg.getEstDepartureDayOffsetLocal());
					flightScheduleLegDTO.setLegStartDate(calStart.getTime());

					GregorianCalendar calEnd = new GregorianCalendar();
					calEnd.setTime(flight.getDepartureDateLocal());
					calEnd.add(Calendar.DATE, leg.getEstDepartureDayOffsetLocal());
					flightScheduleLegDTO.setLegEndDate(calEnd.getTime());
				} else {
					flightScheduleLegDTO.setLegStartDate(flight.getDepartureDateLocal());
					flightScheduleLegDTO.setLegEndDate(flight.getDepartureDateLocal());
				}
				flightScheduleLegDTO.setLocalTimeVariationForDepatureSt(SchedulePublishUtil.getGMTOffset(
						airportBD.getAirport(leg.getOrigin()), flightScheduleLegDTO.getLegEndDate()));
				flightScheduleLegDTO.setLocalTimeVariationForArrivalSt(SchedulePublishUtil.getGMTOffset(
						airportBD.getAirport(leg.getDestination()), flightScheduleLegDTO.getLegEndDate()));

				legs.add(flightScheduleLegDTO);
			}
			itineraryMessageDTO.setLegs(legs);
		}
		return itineraryMessageDTO;
	}

	private class FlightComparator implements Comparator<Flight> {
		public int compare(Flight flight1, Flight flight2) {
			return flight1.getFlightId().compareTo(flight2.getFlightId());
		}
	}

	private class FlightScheduleLegInfoDTOComparator implements Comparator<FlightScheduleLegInfoDTO> {
		public int compare(FlightScheduleLegInfoDTO leg1, FlightScheduleLegInfoDTO leg2) {
			return (new Integer(leg1.getLegNumber()).compareTo(new Integer(leg2.getLegNumber())));
		}
	}

	private class StandardScheduleMessageDTOComparator implements Comparator<StandardScheduleMessageDTO> {
		public int compare(StandardScheduleMessageDTO ssm1, StandardScheduleMessageDTO ssm2) {
			return (new Integer(ssm1.getFlightNumber()).compareTo(new Integer(ssm2.getFlightNumber())));
		}
	}

	public static List<FlightScheduleLegDTO> cloneLegList(List<FlightScheduleLegDTO> list) {
		List<FlightScheduleLegDTO> clone = new ArrayList<FlightScheduleLegDTO>(list.size());
		for (FlightScheduleLegDTO item : list)
			clone.add((FlightScheduleLegDTO) item.clone());
		return clone;
	}

}
