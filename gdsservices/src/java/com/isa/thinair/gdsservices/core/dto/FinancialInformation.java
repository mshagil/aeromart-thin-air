package com.isa.thinair.gdsservices.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FinancialInformation implements Serializable {

	public interface Constants {
		String EXTRA_MILEAGE_ALLOWANCE_ROUTING_POINT_ANY = "XXX";
        String NON_FLOWN_FARE_INCLUDE = "//";
        String NON_FLOWN_FARE_NOT_INCLUDE = "/-";
	}

	private Location origin;
	private List<Sector> sectors;
	private String currency;
	private BigDecimal totalAmount;
	private BigDecimal rateOfExchange;
	private List<TaxFeeCharge> passengerFacilityCharges;
	private List<TaxFeeCharge> usDomesticTaxes;

	public FinancialInformation() {
		sectors = new ArrayList<Sector>();
		passengerFacilityCharges = new ArrayList<TaxFeeCharge>();
		usDomesticTaxes = new ArrayList<TaxFeeCharge>();
	}

	public void populateSegmentOriginLocation() {
	 	Location lastLocation = origin;

		for (Sector sector : sectors) {
			for (Segment segment : sector.getSegments()) {
				segment.setOrigin(lastLocation);
				lastLocation = segment.getDestination();
			}
		}
	}

	public Location getOrigin() {
		return origin;
	}

	public void setOrigin(Location origin) {
		this.origin = origin;
	}

	public List<Sector> getSectors() {
		return sectors;
	}

	public void setSectors(List<Sector> sectors) {
		this.sectors = sectors;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getRateOfExchange() {
		return rateOfExchange;
	}

	public void setRateOfExchange(BigDecimal rateOfExchange) {
		this.rateOfExchange = rateOfExchange;
	}

	public List<TaxFeeCharge> getUsDomesticTaxes() {
		return usDomesticTaxes;
	}

	public void setUsDomesticTaxes(List<TaxFeeCharge> usDomesticTaxes) {
		this.usDomesticTaxes = usDomesticTaxes;
	}

	public List<TaxFeeCharge> getPassengerFacilityCharges() {
		return passengerFacilityCharges;
	}

	public void setPassengerFacilityCharges(List<TaxFeeCharge> passengerFacilityCharges) {
		this.passengerFacilityCharges = passengerFacilityCharges;
	}

	public static class Location {

		private String airportOrCityCode;
		private boolean extraMileage;
		private boolean intermediateTransfer;
		private boolean maxPermMileageDeduction;
		private boolean ticketedPointNotIncludedInMileage;

		public String getAirportOrCityCode() {
			return airportOrCityCode;
		}

		public void setAirportOrCityCode(String airportOrCityCode) {
			this.airportOrCityCode = airportOrCityCode;
		}

		public boolean isExtraMileage() {
			return extraMileage;
		}

		public void setExtraMileage(boolean extraMileage) {
			this.extraMileage = extraMileage;
		}

		public boolean isIntermediateTransfer() {
			return intermediateTransfer;
		}

		public void setIntermediateTransfer(boolean intermediateTransfer) {
			this.intermediateTransfer = intermediateTransfer;
		}

		public boolean isMaxPermMileageDeduction() {
			return maxPermMileageDeduction;
		}

		public void setMaxPermMileageDeduction(boolean maxPermMileageDeduction) {
			this.maxPermMileageDeduction = maxPermMileageDeduction;
		}

		public boolean isTicketedPointNotIncludedInMileage() {
			return ticketedPointNotIncludedInMileage;
		}

		public void setTicketedPointNotIncludedInMileage(boolean ticketedPointNotIncludedInMileage) {
			this.ticketedPointNotIncludedInMileage = ticketedPointNotIncludedInMileage;
		}
	}

	public static class Carrier {
		private boolean flown;
        private Boolean fareInclude;
		private String carrierCode;
		private BigDecimal stopoverTransferCharge;
		private BigDecimal surcharges;

        public boolean getFlown() {
            return flown;
        }

        public void setFlown(boolean flown) {
            this.flown = flown;
        }

        public Boolean getFareInclude() {
            return fareInclude;
        }

        public void setFareInclude(Boolean fareInclude) {
            this.fareInclude = fareInclude;
        }

        public String getCarrierCode() {
			return carrierCode;
		}

		public void setCarrierCode(String carrierCode) {
			this.carrierCode = carrierCode;
		}

		public BigDecimal getStopoverTransferCharge() {
			return stopoverTransferCharge;
		}

		public void setStopoverTransferCharge(BigDecimal stopoverTransferCharge) {
			this.stopoverTransferCharge = stopoverTransferCharge;
		}

		public BigDecimal getSurcharges() {
			return surcharges;
		}

		public void setSurcharges(BigDecimal surcharges) {
			this.surcharges = surcharges;
		}
	}

	public static class Sector {

		private List<Segment> segments;
		private List<QuotedFare> quotedFares;

		public Sector() {
			segments = new ArrayList<Segment>();
			quotedFares = new ArrayList<QuotedFare>();
		}

		public List<Segment> getSegments() {
			return segments;
		}

		public void setSegments(List<Segment> segments) {
			this.segments = segments;
		}

		public List<QuotedFare> getQuotedFares() {
			return quotedFares;
		}

		public void setQuotedFares(List<QuotedFare> quotedFares) {
			this.quotedFares = quotedFares;
		}
	}

	public static class Segment {

		// 'origin' is not in the fare text; will populate this for our convenience only
		private Location origin;
		private Carrier carrier;
		private Location destination;

		public Location getOrigin() {
			return origin;
		}

		public void setOrigin(Location origin) {
			this.origin = origin;
		}

		public Carrier getCarrier() {
			return carrier;
		}

		public void setCarrier(Carrier carrier) {
			this.carrier = carrier;
		}

		public Location getDestination() {
			return destination;
		}

		public void setDestination(Location destination) {
			this.destination = destination;
		}
	}

	public static class QuotedFare {

		private List<String> commonPointMinChecksAirports;
		private boolean extraMileageAllowance;
		private AirportPair higherClassDiffAirportPair;
		private List<AirportPair> higherIntermediatePointAirportPairs;
		private String lowestCombinationPrincipleAirport;
		private String mileageEqualizationAirport;
		private List<AirportPair> minimumsAirportPairs;
        private boolean mileagePrinciple;
		private Integer mileagePrinciplePercentage;
		private AirportPair oneWaySubJourneyAirportPair;
		private AirportPair returnSubJourneyAirportPair;
		private Integer additionalStopovers;
		private boolean surcharge;
		private AirportPair surchargeAirportPair;

		private BigDecimal amount;

		public List<String> getCommonPointMinChecksAirports() {
			return commonPointMinChecksAirports;
		}

		public void setCommonPointMinChecksAirports(List<String> commonPointMinChecksAirports) {
			this.commonPointMinChecksAirports = commonPointMinChecksAirports;
		}

		public boolean getExtraMileageAllowance() {
			return extraMileageAllowance;
		}

		public void setExtraMileageAllowance(boolean extraMileageAllowance) {
			this.extraMileageAllowance = extraMileageAllowance;
		}

		public AirportPair getHigherClassDiffAirportPair() {
			return higherClassDiffAirportPair;
		}

		public void setHigherClassDiffAirportPair(AirportPair higherClassDiffAirportPair) {
			this.higherClassDiffAirportPair = higherClassDiffAirportPair;
		}

		public List<AirportPair> getHigherIntermediatePointAirportPairs() {
			return higherIntermediatePointAirportPairs;
		}

		public void setHigherIntermediatePointAirportPairs(List<AirportPair> higherIntermediatePointAirportPairs) {
			this.higherIntermediatePointAirportPairs = higherIntermediatePointAirportPairs;
		}

		public String getLowestCombinationPrincipleAirport() {
			return lowestCombinationPrincipleAirport;
		}

		public void setLowestCombinationPrincipleAirport(String lowestCombinationPrincipleAirport) {
			this.lowestCombinationPrincipleAirport = lowestCombinationPrincipleAirport;
		}

		public String getMileageEqualizationAirport() {
			return mileageEqualizationAirport;
		}

		public void setMileageEqualizationAirport(String mileageEqualizationAirport) {
			this.mileageEqualizationAirport = mileageEqualizationAirport;
		}

		public List<AirportPair> getMinimumsAirportPairs() {
			return minimumsAirportPairs;
		}

		public void setMinimumsAirportPairs(List<AirportPair> minimumsAipAirportPairs) {
			this.minimumsAirportPairs = minimumsAipAirportPairs;
		}

        public boolean getMileagePrinciple() {
            return mileagePrinciple;
        }

        public void setMileagePrinciple(boolean mileagePrinciple) {
            this.mileagePrinciple = mileagePrinciple;
        }

		public Integer getMileagePrinciplePercentage() {
			return mileagePrinciplePercentage;
		}

		public void setMileagePrinciplePercentage(Integer mileagePrinciplePercentage) {
			this.mileagePrinciplePercentage = mileagePrinciplePercentage;
		}

		public AirportPair getOneWaySubJourneyAirportPair() {
			return oneWaySubJourneyAirportPair;
		}

		public void setOneWaySubJourneyAirportPair(AirportPair oneWaySubJourneyAirportPair) {
			this.oneWaySubJourneyAirportPair = oneWaySubJourneyAirportPair;
		}

		public AirportPair getReturnSubJourneyAirportPair() {
			return returnSubJourneyAirportPair;
		}

		public void setReturnSubJourneyAirportPair(AirportPair returnSubJourneyAirportPair) {
			this.returnSubJourneyAirportPair = returnSubJourneyAirportPair;
		}

		public Integer getAdditionalStopovers() {
			return additionalStopovers;
		}

		public void setAdditionalStopovers(Integer additionalStopovers) {
			this.additionalStopovers = additionalStopovers;
		}

		public boolean getSurcharge() {
			return surcharge;
		}

		public void setSurcharge(boolean surcharge) {
			this.surcharge = surcharge;
		}

		public AirportPair getSurchargeAirportPair() {
			return surchargeAirportPair;
		}

		public void setSurchargeAirportPair(AirportPair surchargeAirportPair) {
			this.surchargeAirportPair = surchargeAirportPair;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
	}

	public static class TaxFeeCharge {

		private String airportCode;
		private BigDecimal amount;

		public String getAirportCode() {
			return airportCode;
		}

		public void setAirportCode(String airportCode) {
			this.airportCode = airportCode;
		}

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
	}

	public static class AirportPair {
		private String airport1;
		private String airport2;
		private String classOfService;

		public String getAirport1() {
			return airport1;
		}

		public void setAirport1(String airport1) {
			this.airport1 = airport1;
		}

		public String getAirport2() {
			return airport2;
		}

		public void setAirport2(String airport2) {
			this.airport2 = airport2;
		}

		public String getClassOfService() {
			return classOfService;
		}

		public void setClassOfService(String classOfService) {
			this.classOfService = classOfService;
		}
	}

}
