package com.isa.thinair.gdsservices.core.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.core.io.ResourceLoader;

/**
 * IATA Type A related configs
 * 
 * @author malaka
 * 
 */
public class TypeAMessageConfig {

	private ResourceLoader resourceLoader;
	
	private Map<String, String> messagePath;

	private Map<String, String> messageVersoins;

	private String dataDirectEdiXmlUrl;

	private String syntaxIdentifier;

	private String syntaxVersion;

	private String remoteId;

	private String remoteCode;

	private String ownId;

	private String ownCode;

	private int maxValidityPeriod;

	private String locationCode;

	private String agentIdentificationCode;

	private String inHouseIdentificationCode;

	private String originatorAuthorityRequestCode;
	
	// < version , < msg-type , xslt-location > >
	private Map<String, Map<String, String>> transformersLocations;

	
	/**
	 * @return the messageVersoin for a given message type
	 */
	public String getMessageVersoins(String message) {
		String verAndRel = messageVersoins.get(message);
		return verAndRel.substring(0, 2);
	}

	public String getMessageRelease(String message) {
		String verAndRel = messageVersoins.get(message);
		return verAndRel.substring(3);
	}

	/**
	 * @param messageVersoins
	 *            the messageVersoins to set
	 */
	public void setMessageVersoins(Map<String, String> messageVersoins) {
		this.messageVersoins = messageVersoins;
	}

	/**
	 * @return the messagePath
	 */
	public Map<String, String> getMessagePath() {
		if (messagePath == null) {
			messagePath = new HashMap<String, String>(messageVersoins.size());
		}

		for (Entry<String, String> entry : messageVersoins.entrySet()) {
			String message = entry.getKey();
			String version = entry.getValue();
			String messagePackage = "iata.typea.v" + version.replace(".", "") + "." + message.toLowerCase();
			messagePath.put(message, messagePackage);
		}

		return messagePath;
	}

	/**
	 * @return the dataDirectEdiXmlUrl
	 */
	public String getDataDirectEdiXmlUrl() {
		return dataDirectEdiXmlUrl;
	}

	/**
	 * @param dataDirectEdiXmlUrl
	 *            the dataDirectEdiXmlUrl to set
	 */
	public void setDataDirectEdiXmlUrl(String dataDirectEdiXmlUrl) {
		this.dataDirectEdiXmlUrl = dataDirectEdiXmlUrl;
	}

	/**
	 * @return the syntaxIdentifier
	 */
	public String getSyntaxIdentifier() {
		return syntaxIdentifier;
	}

	/**
	 * @param syntaxIdentifier
	 *            the syntaxIdentifier to set
	 */
	public void setSyntaxIdentifier(String syntaxIdentifier) {
		this.syntaxIdentifier = syntaxIdentifier;
	}

	/**
	 * @return the remoteId
	 */
	public String getRemoteId() {
		return remoteId;
	}

	/**
	 * @param remoteId
	 *            the remoteId to set
	 */
	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	/**
	 * @return the remoteCode
	 */
	public String getRemoteCode() {
		return remoteCode;
	}

	/**
	 * @param remoteCode
	 *            the remoteCode to set
	 */
	public void setRemoteCode(String remoteCode) {
		this.remoteCode = remoteCode;
	}

	/**
	 * @return the ownId
	 */
	public String getOwnId() {
		return ownId;
	}

	/**
	 * @param ownId
	 *            the ownId to set
	 */
	public void setOwnId(String ownId) {
		this.ownId = ownId;
	}

	/**
	 * @return the ownCode
	 */
	public String getOwnCode() {
		return ownCode;
	}

	/**
	 * @param ownCode
	 *            the ownCode to set
	 */
	public void setOwnCode(String ownCode) {
		this.ownCode = ownCode;
	}

	/**
	 * @return the syntaxVersion
	 */
	public String getSyntaxVersion() {
		return syntaxVersion;
	}

	/**
	 * @param syntaxVersion
	 *            the syntaxVersion to set
	 */
	public void setSyntaxVersion(String syntaxVersion) {
		this.syntaxVersion = syntaxVersion;
	}

	/**
	 * @return the maxValidityPeriod
	 */
	public int getMaxValidityPeriod() {
		return maxValidityPeriod;
	}

	/**
	 * @param maxValidityPeriod
	 *            the maxValidityPeriod to set
	 */
	public void setMaxValidityPeriod(int maxValidityPeriod) {
		this.maxValidityPeriod = maxValidityPeriod;
	}

	/**
	 * @return the locationCode
	 */
	public String getLocationCode() {
		return locationCode;
	}

	/**
	 * @param locationCode
	 *            the locationCode to set
	 */
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	/**
	 * @return the agentIdentificationCode
	 */
	public String getAgentIdentificationCode() {
		return agentIdentificationCode;
	}

	/**
	 * @param agentIdentificationCode
	 *            the agentIdentificationCode to set
	 */
	public void setAgentIdentificationCode(String agentIdentificationCode) {
		this.agentIdentificationCode = agentIdentificationCode;
	}

	/**
	 * @return the inHouseIdentificationCode
	 */
	public String getInHouseIdentificationCode() {
		return inHouseIdentificationCode;
	}

	/**
	 * @param inHouseIdentificationCode
	 *            the inHouseIdentificationCode to set
	 */
	public void setInHouseIdentificationCode(String inHouseIdentificationCode) {
		this.inHouseIdentificationCode = inHouseIdentificationCode;
	}

	/**
	 * @return the originatorAuthorityRequestCode
	 */
	public String getOriginatorAuthorityRequestCode() {
		return originatorAuthorityRequestCode;
	}

	/**
	 * @param originatorAuthorityRequestCode
	 *            the originatorAuthorityRequestCode to set
	 */
	public void setOriginatorAuthorityRequestCode(String originatorAuthorityRequestCode) {
		this.originatorAuthorityRequestCode = originatorAuthorityRequestCode;
	}

	public Map<String, Map<String, String>> getTransformersLocations() {
		return transformersLocations;
	}

	public void setTransformersLocations(Map<String, Map<String, String>> transformersLocations) {
		this.transformersLocations = transformersLocations;
	}
}
