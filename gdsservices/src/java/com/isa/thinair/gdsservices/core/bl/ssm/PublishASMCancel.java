/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishASMCancel"
 */
public class PublishASMCancel extends DefaultBaseCommand {

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	/**
	 * constructor of the PublishASMCancel command
	 */
	public PublishASMCancel() {

		// looking up BDs
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		// lokking up DAOs
		gdsServiceDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * execute method of the PublishASMCancel command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		Flight flight = (Flight) this.getParameter(GDSSchedConstants.ParamNames.FLIGHT);
		Flight overlappingFlight = (Flight) this.getParameter(GDSSchedConstants.ParamNames.OVERLAPPING_FLIGHT);
		String serviceType = (String) this.getParameter(GDSSchedConstants.ParamNames.SERVICE_TYPE);
		String supplementaryInfo = (String) this.getParameter(GDSSchedConstants.ParamNames.SUPPLEMENTARY_INFO);

		Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		Map aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();
		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		if (flight != null) {
			invokeMessageBroker(flight, gdsStatusMap, serviceType, (String) aircraftTypeMap.get(flight.getModelNumber()),
					airLineCode, supplementaryInfo);
		}

		if (overlappingFlight != null) {

			invokeMessageBroker(overlappingFlight, gdsStatusMap, serviceType,
					(String) aircraftTypeMap.get(overlappingFlight.getModelNumber()), airLineCode, supplementaryInfo);
		}

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command responce
		return responce;
	}

	private void invokeMessageBroker(Flight flight, Map gdsStatsMap, String serviceType, String aircraftType, String airLineCode,
			String supplementaryInfo)
			throws ModuleException {

		Collection gdsIds = flight.getGdsIds();

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO asmDto = new ASMessegeDTO();

			// Action Information
			SchedulePublishUtil.populateASMforCNL(asmDto, flight, serviceType, aircraftType, supplementaryInfo);

			Map bcMap = bookingClassBD.getBookingClassCodes(gdsIds);

			Iterator itGdsIds = gdsIds.iterator();
			ArrayList activeGdses = new ArrayList();

			while (itGdsIds.hasNext()) {

				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(asmDto, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatsMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(asmDto),
						asmDto.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {

					activeGdses.add(gdsId);

					try {

						//ReservationToGDSMessageSenderBD gdsSender = CommonPublishUtil.getGDSSender();
						GDSServicesModuleUtil.getSSMASMServiceBD().sendAdHocScheduleMessage(gdsStatus.getGdsCode(), airLineCode, asmDto);
						gdsServiceDao.saveGDSPublisingMessage(pubMessage);

					} catch (Exception e) {

						pubMessage.setRemarks("Message broker invocation failed");
						pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
						gdsServiceDao.saveGDSPublisingMessage(pubMessage);
					}
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}

			// notifiy flight events to inventory to send corresponding avs messages
			if (activeGdses.size() > 0) {
				NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
				NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
				newFlightEnvent.setFlightEventCode(FlightEventCode.FLIGHT_CANCELLED);
				newFlightEnvent.addFlightId(flight.getFlightId());
				newFlightEnvent.setGdsIdsRemoved(activeGdses);
				notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);
				flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ);
			}
		}
	}
}
