/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServicesSupportJDBCDAO;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * GDS DAO specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class GDSServicesDAOUtils {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(GDSServicesDAOUtils.class);

	/**
	 * Returns the module delegate
	 * 
	 * @return
	 */
	private static IModule getModule() {
		return LookupServiceFactory.getInstance().getModule(GdsservicesConstants.MODULE_NAME);
	}

	/**
	 * Returns a DAO for gds services module
	 * 
	 * @param daoName
	 * @return
	 */
	private static Object getDAO(String daoName) {
		String daoFullName = "isa:base://modules/" + GdsservicesConstants.MODULE_NAME + "?id=" + daoName + "Proxy";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(daoFullName);
	}

	/** Denotes the the DAO names of GDS Services */
	private static interface DAONames {
		public static String GDSSERVICES_DAO = "GDSServicesDAO";
		public static String GDSTYPEASERVICES_DAO = "GDSTypeAServiceDAO";
		public static String GDSSERVICES_SUPPORT_JDBC_DAO = "GDSServicesSupportDAO";
		public static String GDS_TYPEA_SERVICES_SUPPORT_JDBC_DAO = "GDSTypeAServicesSupportDAO";
	}

	/** Denotes the the DAO instances of gds services */
	public static interface DAOInstance {
		/** Holds the GDSServicesDAO instance */
		public static GDSServicesDAO GDSSERVICES_DAO = (GDSServicesDAO) GDSServicesDAOUtils
				.getDAO(GDSServicesDAOUtils.DAONames.GDSSERVICES_DAO);

		public static GDSTypeAServiceDAO GDSTYPEASERVICES_DAO = (GDSTypeAServiceDAO) GDSServicesDAOUtils
				.getDAO(GDSServicesDAOUtils.DAONames.GDSTYPEASERVICES_DAO);

		public static GDSServicesSupportJDBCDAO GDSSERVICES_SUPPORT_JDBC_DAO = (GDSServicesSupportJDBCDAO) getModule()
				.getLocalBean(GDSServicesDAOUtils.DAONames.GDSSERVICES_SUPPORT_JDBC_DAO);

		public static GDSTypeAServicesSupportJDBCDAO GDS_TypeA_JDBC_DAO = (GDSTypeAServicesSupportJDBCDAO) getModule()
				.getLocalBean(GDSServicesDAOUtils.DAONames.GDS_TYPEA_SERVICES_SUPPORT_JDBC_DAO);
	}
}
