package com.isa.thinair.gdsservices.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.gdsservices.api.model.CouponInfoTO;
import com.isa.thinair.gdsservices.api.model.EticketInfoTO;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServicesSupportJDBCDAO;

public class GDSTypeAServicesSupportJDBCDAOImpl extends PlatformBaseJdbcDAOSupport implements GDSTypeAServicesSupportJDBCDAO {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<EticketInfoTO> getEticketListForPPFSIds(Collection<Integer> ppfsIds, Integer gdsChannelCode)
			throws ModuleException {

		if (ppfsIds == null || gdsChannelCode == null) {
			throw new ModuleException("gdsservices.framework.empty.argument");
		}

		StringBuilder eTicketSQL = new StringBuilder();
		eTicketSQL.append("SELECT res.pnr           AS pnr , ");
		eTicketSQL.append("  petkt.e_ticket_number  AS e_ticket_number, ");
		eTicketSQL.append("  petkt.e_ticket_status  AS e_ticket_status, ");
		eTicketSQL.append("  pax.pnr_pax_id         AS pnr_pax_id, ");
		eTicketSQL.append("  ppfsetkt.coupon_number AS coupon_number, ");
		eTicketSQL.append("  ppfsetkt.status        AS coupon_status ");
		eTicketSQL.append("FROM t_pnr_pax_fare_segment ppfs, ");
		eTicketSQL.append("  t_pax_e_ticket petkt, ");
		eTicketSQL.append("  t_pnr_pax_fare_seg_e_ticket ppfsetkt, ");
		eTicketSQL.append("  t_reservation res, ");
		eTicketSQL.append("  t_pnr_passenger pax, ");
		eTicketSQL.append("  t_pnr_pax_fare ppf ");
		eTicketSQL.append("WHERE res.pnr                = pax.pnr ");
		eTicketSQL.append("AND pax.pnr_pax_id           = ppf.pnr_pax_id ");
		eTicketSQL.append("AND ppf.ppf_id               = ppfs.ppf_id ");
		eTicketSQL.append("AND ppfs.ppfs_id             = ppfsetkt.ppfs_id ");
		eTicketSQL.append("AND ppfsetkt.pax_e_ticket_id = petkt.pax_e_ticket_id ");
		eTicketSQL.append("AND res.origin_channel_code  = " + gdsChannelCode + " ");
		eTicketSQL.append("AND ppfs.ppfs_id IN (" + Util.buildIntegerInClauseContent(ppfsIds) + ")");

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Collection<EticketInfoTO> collection = (Collection<EticketInfoTO>) template.query(eTicketSQL.toString(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<EticketInfoTO> eTicketList = new ArrayList<EticketInfoTO>();
						while (rs.next()) {

							CouponInfoTO coupon = new CouponInfoTO();
							coupon.setCouponStatus(rs.getString("coupon_status"));
							coupon.setSegmentSquence(rs.getInt("coupon_number"));

							String eTicketNumber = rs.getString("e_ticket_number");
							String eTicketStatus = rs.getString("e_ticket_status");

							boolean exisitingEticket = false;
							for (EticketInfoTO eTicket : eTicketList) {

								if (eTicket.getEticketNumber().equals(eTicketNumber)) {
									eTicket.getCouponList().add(coupon);
									exisitingEticket = true;
								}
							}

							if (!exisitingEticket) {
								EticketInfoTO newEticket = new EticketInfoTO();
								newEticket.setEticketNumber(eTicketNumber);
								newEticket.seteTicketStatus(eTicketStatus);
								newEticket.getCouponList().add(coupon);
								eTicketList.add(newEticket);
							}
						}
						return eTicketList;
					}
				});

		return collection;
	}

	private String getLoadReservationQueryContent() {
		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("select r.gds_id gds_id, pet.e_ticket_number e_ticket_number, ppfset.coupon_number coupon_number, ")
				.append("  ppfset.ext_e_ticket_number ext_e_ticket_number, ppfset.ext_coupon_number ext_coupon_number, ")
				.append("  ppfs.pax_status status, fs.flt_seg_id flt_seg_id, fs.est_time_departure_local time_departure, ")
				.append("  fs.est_time_arrival_local time_arrival, fs.segment_code segment_code, f.flight_number flight_number, ")
				.append("  ppfs.ppfs_id ppfs_id, ppfs.booking_code booking_code , pp.pnr pnr, pp.pax_sequence pax_sequence, ")
				.append("  pp.title, pp.first_name first_name, pp.last_name last_name, ps.segment_seq segment_seq ")
				.append("from t_pnr_pax_fare_segment ppfs, t_pnr_pax_fare ppf, t_pnr_passenger pp, t_reservation r, ")
				.append("  t_pnr_segment ps, t_flight_segment fs, t_flight f, t_pax_e_ticket pet, t_pnr_pax_fare_seg_e_ticket ppfset ")
				.append("where ppfs.ppf_id = ppf.ppf_id and ppf.pnr_pax_id = pp.pnr_pax_id and pp.pnr = r.pnr ")
				.append("  and ppfs.pnr_seg_id = ps.pnr_seg_id and ps.flt_seg_id = fs.flt_seg_id and fs.flight_id = f.flight_id ")
				.append("  and pp.pnr_pax_id = pet.pnr_pax_id and pet.pax_e_ticket_id = ppfset.pax_e_ticket_id and ppfs.ppfs_id = ppfset.ppfs_id ")
//				.append("  and r.gds_id is not null ")
		;

		return queryContent.toString();
	}

	private List<LCCClientReservation> getReservations(String queryContent) {
		List<LCCClientReservation> reservations;
		JdbcTemplate template = new JdbcTemplate(getDataSource());


		reservations = (List<LCCClientReservation>)template.query(queryContent, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<LCCClientReservation> resList = new ArrayList<LCCClientReservation>();
				Map<String, LCCClientReservation> reservationsMap = new HashMap<String, LCCClientReservation>();
				LCCClientReservation reservation = null;
				LCCClientReservationPax pax;
				LCCClientReservationSegment seg;
				String pnr;
				String flightRefNo;
				int paxSeq;
				int segSeq;

				Map<String, Map<Integer, LCCClientReservationPax>> paxMapping = new HashMap<String, Map<Integer, LCCClientReservationPax>>();
				Map<String, Map<Integer, LCCClientReservationSegment>> segMapping = new HashMap<String, Map<Integer, LCCClientReservationSegment>>();

				while (rs.next()) {

					pnr = rs.getString("pnr");
					if (!reservationsMap.containsKey(pnr)) {
						reservation = new LCCClientReservation();
						reservation.setPNR(pnr);
						reservation.setGdsId(rs.getInt("gds_id"));

						reservationsMap.put(pnr, reservation);
						resList.add(reservation);

						paxMapping.put(pnr, new HashMap<Integer, LCCClientReservationPax>());
						segMapping.put(pnr, new HashMap<Integer, LCCClientReservationSegment>());

					}
					reservation = reservationsMap.get(pnr);

					FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
					flightSegmentTO.setDepartureDateTime(rs.getTimestamp("time_departure"));
					flightSegmentTO.setArrivalDateTime(rs.getTimestamp("time_arrival"));
					flightSegmentTO.setSegmentCode(rs.getString("segment_code"));
					flightSegmentTO.setFlightNumber(rs.getString("flight_number"));
					flightSegmentTO.setBookingType(rs.getString("booking_code"));
					flightSegmentTO.setOperatingAirline(AppSysParamsUtil.extractCarrierCode(rs.getString("flight_number")));
					flightSegmentTO.setFlightSegId(rs.getInt("flt_seg_id"));
					flightRefNo = FlightRefNumberUtil.composeFlightRPH(flightSegmentTO);

					paxSeq = rs.getInt("pax_sequence");
					if (!paxMapping.get(pnr).containsKey(paxSeq)) {
						pax = new LCCClientReservationPax();
						pax.setTitle(rs.getString("title"));
						pax.setFirstName(rs.getString("first_name"));
						pax.setLastName(rs.getString("last_name"));

						reservation.addPassenger(pax);
						paxMapping.get(pnr).put(paxSeq, pax);
					}
					pax = paxMapping.get(pnr).get(paxSeq);

					List<LccClientPassengerEticketInfoTO> etickets = new ArrayList<LccClientPassengerEticketInfoTO>();
					LccClientPassengerEticketInfoTO eticketInfoTO = new LccClientPassengerEticketInfoTO();
					eticketInfoTO.setPpfsId(rs.getInt("ppfs_id"));
					eticketInfoTO.setPaxETicketNo(rs.getString("e_ticket_number"));
					eticketInfoTO.setExternalPaxETicketNo(rs.getString("ext_e_ticket_number"));
					eticketInfoTO.setCouponNo(rs.getInt("coupon_number"));
					eticketInfoTO.setExternalCouponNo(rs.getInt("ext_coupon_number"));
					eticketInfoTO.setPaxETicketStatus(rs.getString("status"));
					eticketInfoTO.setFlightSegmentRef(flightRefNo);
					etickets.add(eticketInfoTO);
					pax.seteTickets(etickets);

					segSeq = rs.getInt("segment_seq");
					if (!segMapping.get(pnr).containsKey(segSeq)) {
						seg = new LCCClientReservationSegment();
						seg.setFlightSegmentRefNumber(flightRefNo);
						seg.setDepartureDate(rs.getTimestamp("time_departure"));
						seg.setArrivalDate(rs.getTimestamp("time_arrival"));
						seg.setSegmentCode(rs.getString("segment_code"));
						seg.setFlightNo(rs.getString("flight_number"));

						FareTO fareTO = new FareTO();
						fareTO.setBookingClassCode(rs.getString("booking_code"));
						seg.setFareTO(fareTO);

						reservation.addSegment(seg);
						segMapping.get(pnr).put(segSeq, seg);
					}
				}

				return resList;
			}
		} );

		return reservations;

	}

	public List<LCCClientReservation> loadReservations(List<Integer> ppfsIds) throws ModuleException {
		List<LCCClientReservation> reservations;
		String queryContent = getLoadReservationQueryContent();
		queryContent += " and ppfs.ppfs_id in ( " + Util.buildIntegerInClauseContent(ppfsIds) + " ) ";

		return getReservations(queryContent);

	}

	public List<LCCClientReservation> loadReservations(Map<String, List<Integer>> ticketsAndCoupons) throws ModuleException {
		String queryContent = getLoadReservationQueryContent();

		StringBuilder subQueryContent = new StringBuilder();
		boolean isFirstRecord = true;

		if (!ticketsAndCoupons.isEmpty()) {
			subQueryContent.append(" and ( ");
		}

		for (Map.Entry<String, List<Integer>> entry : ticketsAndCoupons.entrySet()) {
            if (isFirstRecord) {
				isFirstRecord = false;
			} else {
				subQueryContent.append(" or ");
			}

			subQueryContent.append(" ( pet.e_ticket_number = '" + entry.getKey() + "' ");
			if (!entry.getValue().isEmpty()) {
				subQueryContent.append("  and  ppfset.coupon_number in ( " + Util.buildIntegerInClauseContent(entry.getValue()) + " ) ");
			}
			subQueryContent.append(" ) ");
		}

		if (!ticketsAndCoupons.isEmpty()) {
			subQueryContent.append(" ) ");
		}

		return getReservations(queryContent + subQueryContent.toString());
	}
}
