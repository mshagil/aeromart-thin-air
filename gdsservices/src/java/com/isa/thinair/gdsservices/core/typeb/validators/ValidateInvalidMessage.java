package com.isa.thinair.gdsservices.core.typeb.validators;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class ValidateInvalidMessage extends ValidatorBase {

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String message = MessageUtil.getMessage("gdsservices.validators.unidentifed.message.identifier");
		bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

		this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
		return bookingRequestDTO;
	}
}
