package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRs;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxETicket;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ADLNotifyer;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.internal.IssueEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class IssueEticketAction {

	private static Log log = LogFactory.getLog(IssueEticketAction.class);

	public static IssueEticketRequest processRequest(IssueEticketRequest issueEticketRequest) {

		try {
			Reservation reservation = CommonUtil.loadReservation(issueEticketRequest.getGdsCredentialsDTO(), issueEticketRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(issueEticketRequest.getGdsCredentialsDTO(), reservation);

			String nameKey = null;
			Passenger passenger = null;
			Map<String, Passenger> passengerMap = CommonUtil.getPassengerMap(issueEticketRequest.getPassengers());

			Map<Integer, Collection<EticketTO>> oldPaxfareSegmentEtickets = new HashMap<Integer, Collection<EticketTO>>();
			Collection<ReservationPaxFareSegmentETicket> modifiedPaxfareSegmentEtickets = new ArrayList<ReservationPaxFareSegmentETicket>();
			Set<Integer> unrecordedPaxList = ADLNotifyer.getUnrecordedPnrPaxIds(modifiedPaxfareSegmentEtickets, reservation);

			Collection<ReservationPax> resPassengers = reservation.getPassengers();
			ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
			for (ReservationPax resPax : resPassengers) {
				nameKey = GDSApiUtils.getIATAName(resPax.getLastName(), resPax.getFirstName(), resPax.getTitle());
				passenger = passengerMap.get(nameKey);
				Collection<EticketTO> eTicketList = clone(resPax.geteTickets());
				oldPaxfareSegmentEtickets.put(resPax.getPnrPaxId(), eTicketList);
				if (passenger != null) {
					eticketTOs.addAll(addEticketInfoForPax(resPax, passenger, issueEticketRequest, reservation));
				} else if (passengerMap.size() == 0) {
					eticketTOs.addAll(addEticketInfoForAllPax(resPax, issueEticketRequest.getCommonSSRs(), reservation));
				}
			}
			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(eticketTOs);

			for (EticketTO eticket : eticketTOs) {
				if (oldPaxfareSegmentEtickets.get(eticket.getPnrPaxId().intValue()) != null) {
					for (EticketTO oldEticket : oldPaxfareSegmentEtickets.get(eticket.getPnrPaxId().intValue())) {
						if (EticketStatus.OPEN.code().equals(oldEticket.getTicketStatus())
								&& oldEticket.getExternalEticketNumber() != null
								&& !oldEticket.getExternalEticketNumber().equals(eticket.getExternalEticketNumber())) {
							ReservationPaxFareSegmentETicket paxFareSeg = new ReservationPaxFareSegmentETicket();
							ReservationPaxETicket paxEticket = new ReservationPaxETicket();
							paxEticket.setEticketNumber(eticket.getExternalEticketNumber());
							paxEticket.setPnrPaxId(eticket.getPnrPaxId());
							paxEticket.seteTicketStatus(eticket.getTicketStatus());
							paxFareSeg.setReservationPaxETicket(paxEticket);
							paxFareSeg.setPnrPaxFareSegId(eticket.getPnrPaxFareSegId());
							if (!modifiedPaxfareSegmentEtickets.contains(paxFareSeg)) {
								modifiedPaxfareSegmentEtickets.add(paxFareSeg);
							}
						}
					}
				}
			}
			ADLNotifyer.recordExchangedETicketForCodeShareBookings(modifiedPaxfareSegmentEtickets, reservation, unrecordedPaxList);
			onTicketIssuance(reservation);

			issueEticketRequest.setSendResponse(false);

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				issueEticketRequest.setSuccess(true);
			} else {
				issueEticketRequest.setSuccess(false);
				issueEticketRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			}

		} catch (ModuleException e) {
			log.error(" IssueEticketAction Failed for GDS PNR "
					+ issueEticketRequest.getOriginatorRecordLocator().getPnrReference(), e);
			issueEticketRequest.setSuccess(false);
			issueEticketRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" IssueEticketAction Failed for GDS PNR "
					+ issueEticketRequest.getOriginatorRecordLocator().getPnrReference(), e);
			issueEticketRequest.setSuccess(false);
			issueEticketRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return issueEticketRequest;
	}

	private static ArrayList<EticketTO> addEticketInfoForPax(ReservationPax resPax, Passenger passenger,
			IssueEticketRequest issueEticketRequest, Reservation reservation) throws ModuleException {

		ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
		Collection<SpecialServiceRequest> sSRs = passenger.getSsrCollection();
		boolean isFoundTicketDetails;
		if (sSRs != null) {
			for (SpecialServiceRequest ssr : sSRs) {
				isFoundTicketDetails = false;
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssdr.getCode())) {
						if (ssdr.getSegment() != null) {
							for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
								for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare
										.getPaxFareSegments()) {
									for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
										if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()
												&& !isFoundTicketDetails) {
											if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
													&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
													&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr
															.getSegment().getDepartureDate())
													&& (seg.getFlightNo().substring(2)
															.equals(ssdr.getSegment().getFlightNumber()) || ("0" + seg
															.getFlightNo().substring(2)).equals(ssdr.getSegment()
															.getFlightNumber()))) {
												for (EticketTO eTicket : resPax.geteTickets()) {
													if (eTicket.getPnrPaxFareSegId().equals(
															reservationPaxFareSegment.getPnrPaxFareSegId())
															&& !eTicket.getTicketStatus().equals(EticketStatus.EXCHANGED.code())
															&& !eTicket.getTicketStatus().equals(EticketStatus.CLOSED.code())) {
														if (ssdr.getComment() != null && ssdr.getComment().length() > 0
																&& ssdr.getComment().contains("C")) {
															String[] values = ssdr.getComment().split("C");
															if (values.length == 2 && values[0].length() > 0
																	&& values[1].length() > 0) {
																eTicket.setExternalEticketNumber(values[0]);
																eTicket.setExternalCouponNo(Integer.parseInt(values[1]));
																eTicket.setExternalCouponStatus(EticketStatus.OPEN.code());
																eTicket.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.VALIDATING_CARRIER);
																eticketTOs.add(eTicket);
															}
														}
													}
												}
												isFoundTicketDetails = true;
											}
										}
									}
								}
							}
							if (!isFoundTicketDetails) {
								throw new ModuleException("gdsservices.extractors.invalid.eticket.segment");
							}
						}
					}
				}
			}
		}
		return eticketTOs;
	}


	private static ArrayList<EticketTO> addEticketInfoForAllPax(ReservationPax resPax,
			Collection<SpecialServiceRequest> commonSSRs, Reservation reservation) throws ModuleException {

		ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
		if (commonSSRs != null && commonSSRs.size() > 0) {
			for (SpecialServiceRequest ssr : commonSSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssdr.getCode()) && ssdr.getSegment() != null) {
						for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
									if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
										if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
												&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
												&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr.getSegment()
														.getDepartureDate())
												&& (seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber()) || ("0" + seg
														.getFlightNo().substring(2)).equals(ssdr.getSegment().getFlightNumber()))) {

											for (EticketTO eTicket : resPax.geteTickets()) {
												if (eTicket.getPnrPaxFareSegId().equals(
														reservationPaxFareSegment.getPnrPaxFareSegId())) {
													if (ssdr.getComment() != null && ssdr.getComment().length() > 0
															&& ssdr.getComment().contains("C")) {
														String[] values = ssdr.getComment().split("C");
														if (values.length == 2 && values[0].length() > 0
																&& values[1].length() > 0) {
															eTicket.setExternalEticketNumber(values[0]);
															eTicket.setExternalCouponNo(Integer.parseInt(values[1]));
															eTicket.setExternalCouponStatus(EticketStatus.OPEN.code());
															eTicket.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.OPERATING_CARRIER);
															eticketTOs.add(eTicket);
														}
													}
												}
											}
										} else {
											throw new ModuleException("gdsservices.extractors.invalid.eticket.segment");
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return eticketTOs;
	}

	private static void onTicketIssuance(Reservation reservation) throws ModuleException {
		GDSNotifyBD gdsNotifyBD;
		TicketingEventRq ticketIssueRq;
		Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons;
		Map<String, List<Integer>> ticketCoupons;

		gdsNotifyBD = ReservationModuleUtils.getGDSNotifyBD();

		paxTicketsCoupons = new HashMap<Integer, Map<String, List<Integer>>>();
		for (ReservationPax resPax : reservation.getPassengers()) {
			ticketCoupons = new HashMap<String, List<Integer>>();
			paxTicketsCoupons.put(resPax.getPaxSequence(), ticketCoupons);
			// if all external tickets/coupons received
			if (GDSServicesUtil.allGdsETicketsIssued(resPax)) {

				for (EticketTO eticketTO : resPax.geteTickets()) {

					if (!ticketCoupons.containsKey(eticketTO.getExternalEticketNumber())) {
						ticketCoupons.put(eticketTO.getExternalEticketNumber(), new ArrayList<Integer>());
					}

					ticketCoupons.get(eticketTO.getExternalEticketNumber()).add(eticketTO.getExternalCouponNo());
				}


			}
		}

		ticketIssueRq = new TicketingEventRq();
		ticketIssueRq.setGdsId(reservation.getGdsId());
		ticketIssueRq.setPnr(reservation.getPnr());
		ticketIssueRq.setPaxTicketsCoupons(paxTicketsCoupons);
		ticketIssueRq.setCalculateFinancialInfo(true);

		GDSServicesModuleConfig gdsServicesModuleConfig = GDSServicesModuleUtil.getConfig();

		if (gdsServicesModuleConfig != null
				&& (gdsServicesModuleConfig.getMatipClientHost() != null && gdsServicesModuleConfig.getMatipClientPort() != 0)) {
			TicketingEventRs rs = gdsNotifyBD.onTicketIssue(ticketIssueRq);

			if (rs.isSuccess()) {
				ExternalReservation externalReservation =
						GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceGet(ExternalReservation.class, reservation.getPnr());

				externalReservation.setPriceSynced(CommonsConstants.YES);

				GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(externalReservation);
			}
		} else if (!((GDSStatusTO) ReservationModuleUtils.getGlobalConfig().getActiveGdsMap().get(reservation.getGdsId().toString()))
				.isAaValidatingCarrier() && !ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())) {
			gdsNotifyBD.onTicketIssue(ticketIssueRq);
		}

	}

	private static Collection<EticketTO> clone(Collection<EticketTO> eticketList) {
		Collection<EticketTO> olderEticketList = new ArrayList<EticketTO>();
		for (EticketTO eticket : eticketList) {
			EticketTO eticketTO = new EticketTO();
			eticketTO.setExternalEticketNumber(eticket.getExternalEticketNumber());
			eticketTO.setExternalCouponNo(eticket.getExternalCouponNo());
			eticketTO.setPnrPaxId(eticket.getPnrPaxId());
			eticketTO.setPnrPaxFareSegId(eticket.getPnrPaxFareSegId());
			eticketTO.setTicketStatus(eticket.getTicketStatus());
			olderEticketList.add(eticketTO);
		}
		return olderEticketList;
	}
}
