package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CancelReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class CancelReservationResponseTransformer {
	
	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		CancelReservationRequest cancelReservationRequest = (CancelReservationRequest) reservationResponseMap
				.get(ReservationAction.CANCEL_RESERVATION);

		if (cancelReservationRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = setSegmentStatuses(bookingRequestDTO, cancelReservationRequest);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, cancelReservationRequest);
			bookingRequestDTO.setResponseMessageIdentifier(null);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, cancelReservationRequest);
			bookingRequestDTO.setResponseMessageIdentifier(null);
		}
		bookingRequestDTO.getErrors().addAll(cancelReservationRequest.getErrorCode());

		return bookingRequestDTO;
	}
	
	private static BookingRequestDTO setSegmentStatuses(BookingRequestDTO bookingRequestDTO,
			CancelReservationRequest cancelReservationRequest) {

		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<Segment> segments = cancelReservationRequest.getSegments();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (Segment segment : segments) {
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					String gdsActionCode = GDSApiUtils.maskNull(bookingSegmentDTO.getActionOrStatusCode());

					if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())) {

						String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(segment);

						bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
					}
				}
			}

		}

		return bookingRequestDTO;
	}

}
