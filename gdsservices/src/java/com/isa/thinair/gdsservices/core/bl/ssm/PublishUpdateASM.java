/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishUpdateASM"
 */
public class PublishUpdateASM extends DefaultBaseCommand {

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	/**
	 * constructor of the PublishUpdateASM command
	 */
	public PublishUpdateASM() {

		// looking up BDs
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		// lokking up DAOs
		gdsServiceDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * execute method of the PublishUpdateASM command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		GDSFlightEventCollector sGdsEvents = (GDSFlightEventCollector) this
				.getParameter(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR);

		Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		Map aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();

		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		Collection gdsIdsForRbd = new ArrayList();

		if (sGdsEvents.getExistingFlight().getGdsIds() != null)
			gdsIdsForRbd.addAll(sGdsEvents.getExistingFlight().getGdsIds());

		if (sGdsEvents.getExistingOverlapFlight() != null && sGdsEvents.getExistingOverlapFlight().getGdsIds() != null)
			gdsIdsForRbd.addAll(sGdsEvents.getExistingOverlapFlight().getGdsIds());

		Map bcMap = bookingClassBD.getBookingClassCodes(gdsIdsForRbd);

		// TODO: TimeMode should configurable - AARESAA-24335
		if (isEligibleToSendSubMessages(sGdsEvents.getActions())) {

			sendASMSubMessagesForUpdate(sGdsEvents.getActions(), gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
					sGdsEvents.getUpdatedFlight(), airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap,
					sGdsEvents.getSupplementaryInfo(), sGdsEvents.getExistingFlight());

			if (sGdsEvents.getExistingOverlapFlight() != null) {
				sendASMSubMessagesForUpdate(sGdsEvents.getActions(), gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
						sGdsEvents.getUpdatedOverlapFlight(), airLineCode, sGdsEvents.getExistingOverlapFlight().getGdsIds(),
						bcMap, sGdsEvents.getSupplementaryInfo(), sGdsEvents.getExistingOverlapFlight());
			}

		} else if (isCancelAndComposeNew(sGdsEvents.getActions())) {

			sendASMCancel(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getExistingFlight(),
					airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap, sGdsEvents.getSupplementaryInfo());
			sendASMNew(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedFlight(),
					airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap, sGdsEvents.getSupplementaryInfo());

		}else {
				if (sGdsEvents.containsAction(GDSFlightEventCollector.FLIGHT_NUMBER_CHANGED)
						|| sGdsEvents.containsAction(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME)) {
					// Change of Flight Identifier FLT
					sendASMFlightNumberChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedFlight(), airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap,
							sGdsEvents.getSupplementaryInfo(), sGdsEvents.getExistingFlight());

					if (sGdsEvents.getExistingOverlapFlight() != null
							&& sGdsEvents.containsAction(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME)) {

						// Change of Flight Identifier overlapping flights
						sendASMFlightNumberChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
								sGdsEvents.getUpdatedOverlapFlight(), airLineCode, sGdsEvents.getExistingOverlapFlight().getGdsIds(),
								bcMap, sGdsEvents.getSupplementaryInfo(), sGdsEvents.getExistingOverlapFlight());
					}
				}

				if (sGdsEvents.containsAction(GDSFlightEventCollector.MODEL_CHANGED)
						&& !sGdsEvents.containsAction(GDSFlightEventCollector.REPLACE_EXISTING_INFO)) {

					// cancel and create the existing and updated schedules
					sendASMModelChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedFlight(),
							airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap, sGdsEvents.getSupplementaryInfo());

					if (sGdsEvents.getUpdatedOverlapFlight() != null) {

						sendASMModelChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
								sGdsEvents.getUpdatedOverlapFlight(), airLineCode, sGdsEvents.getExistingOverlapFlight().getGdsIds(),
								bcMap, sGdsEvents.getSupplementaryInfo());
					}
				}

				if (sGdsEvents.containsAction(GDSFlightEventCollector.LEG_ADDED_DELETED)
						|| sGdsEvents.containsAction(GDSFlightEventCollector.REPLACE_EXISTING_INFO)) {

					sendASMLegsAddedDeleted(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedFlight(), airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap,
							sGdsEvents.getSupplementaryInfo());
					// TODO - notify inventory

					if (sGdsEvents.getUpdatedOverlapFlight() != null) {

						sendASMLegsAddedDeleted(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
								sGdsEvents.getUpdatedOverlapFlight(), airLineCode, sGdsEvents.getExistingOverlapFlight().getGdsIds(),
								bcMap, sGdsEvents.getSupplementaryInfo());
						// TODO - notify inventory
					}

				} else if (sGdsEvents.containsAction(GDSFlightEventCollector.LEG_TIME_CHANGE)) {

					sendASMLegsTimeChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedFlight(),
							airLineCode, sGdsEvents.getExistingFlight().getGdsIds(), bcMap, sGdsEvents.getSupplementaryInfo());

					if (sGdsEvents.getUpdatedOverlapFlight() != null) {

						sendASMLegsTimeChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
								sGdsEvents.getUpdatedOverlapFlight(), airLineCode, sGdsEvents.getExistingOverlapFlight().getGdsIds(),
								bcMap, sGdsEvents.getSupplementaryInfo());
					}

				}
		}
		
		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

	private void sedASM(String gdsCode, String airLineCode, ASMessegeDTO newASM, GDSPublishMessage pubMessage) {

		try {

			GDSServicesModuleUtil.getSSMASMServiceBD().sendAdHocScheduleMessage(gdsCode, airLineCode, newASM);

			gdsServiceDao.saveGDSPublisingMessage(pubMessage);

		} catch (Exception e) {

			pubMessage.setRemarks("Message broker invocation failed");
			pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);
		}
	}

	/*
	 * private void nofigyInventory(FlightEventCode eventcode, Integer flightId, Collection gdsIDs) throws
	 * ModuleException{
	 * 
	 * //notifiy flight events to inventory to send corresponding avs messages NotifyFlightEventsRQ notifyFlightEventsRQ
	 * = new NotifyFlightEventsRQ(); NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
	 * newFlightEnvent.setFlightEventCode(eventcode); newFlightEnvent.addFlightId(flightId);
	 * newFlightEnvent.setGdsIdsRemoved(gdsIDs); notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);
	 * flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ); }
	 */

	private void sendASMNew(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, Flight flight, String airLineCode,
			Collection gdsIds, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforNEW(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsIds.iterator();

			while (itGdsIds.hasNext()) {

				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					sedASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}
		}
	}

	private void sendASMCancel(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, Flight flight, String airLineCode,
			Collection gdsIds, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforCNL(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsIds.iterator();

			while (itGdsIds.hasNext()) {
				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					sedASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}
		}
	}

	private void sendASMModelChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, Flight flight, String airLineCode,
			Collection gdsIds, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforEQT(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsIds.iterator();

			while (itGdsIds.hasNext()) {
				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					sedASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}
		}
	}

	private void sendASMLegsAddedDeleted(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, Flight flight,
			String airLineCode, Collection gdsIds, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforRPL(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsIds.iterator();

			while (itGdsIds.hasNext()) {
				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					sedASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}
		}
	}

	private void sendASMLegsTimeChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, Flight flight,
			String airLineCode, Collection gdsIds, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforTIM(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsIds.iterator();

			while (itGdsIds.hasNext()) {
				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					sedASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}
		}
	}

	private void sendASMFlightNumberChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, Flight flight,
			String airLineCode, Collection gdsIds, Map bcMap, String supplementaryInfo, Flight existingFlight)
			throws ModuleException {

		if (gdsIds != null && gdsIds.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforFLT(newASM, flight, serviceType, existingFlight,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsIds.iterator();

			while (itGdsIds.hasNext()) {
				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					sedASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}
		}
	}

	private void sendASMSubMessagesForUpdate(Collection<String> actions, Map gdsStatusMap, Map aircraftTypeMap,
			String serviceType, Flight flight, String airLineCode, Collection updatedList, Map bcMap, String supplementaryInfo,
			Flight existingFlight) throws ModuleException {

		if (actions != null && actions.size() > 1) {
			// multiple actions
			// CNL//NEW
			Map<Integer, ASMessegeDTO> asmMessegeDTOMap = new HashMap<>();

			if (isCanceExistFlightAndReInstate(actions)) {

				ASMessegeDTO cancelASM = new ASMessegeDTO();
				SchedulePublishUtil.populateASMforCNL(cancelASM, existingFlight, serviceType,
						(String) aircraftTypeMap.get(existingFlight.getModelNumber()), supplementaryInfo);
				asmMessegeDTOMap.put(new Integer(1), cancelASM);

				ASMessegeDTO newASM = new ASMessegeDTO();
				SchedulePublishUtil.populateASMforNEW(newASM, flight, serviceType,
						(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);
				asmMessegeDTOMap.put(new Integer(2), newASM);

			} else {
				int i = 1;
				boolean sentFLT = false;
				boolean isRPLGenerated = false;
				for (String action : actions) {
					ASMessegeDTO newASM = null;
					if (!sentFLT
							&& (GDSFlightEventCollector.FLIGHT_NUMBER_CHANGED.equals(action) || GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME
									.equals(action))) {
						newASM = new ASMessegeDTO();
						SchedulePublishUtil.populateASMforFLT(newASM, flight, serviceType, existingFlight,
								(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);
						sentFLT = true;
					} else if (!isRPLGenerated
							&& (GDSFlightEventCollector.REPLACE_EXISTING_INFO.equals(action) || GDSFlightEventCollector.LEG_ADDED_DELETED
									.equals(action))) {
						newASM = new ASMessegeDTO();
						SchedulePublishUtil.populateASMforRPL(newASM, flight, serviceType,
								(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);
						isRPLGenerated = true;
					} else if (GDSFlightEventCollector.MODEL_CHANGED.equals(action)
							&& !actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)) {

						newASM = new ASMessegeDTO();
						SchedulePublishUtil.populateASMforEQT(newASM, flight, serviceType,
								(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);
					} else if (GDSFlightEventCollector.LEG_TIME_CHANGE.equals(action)) {
						newASM = new ASMessegeDTO();
						SchedulePublishUtil.populateASMforTIM(newASM, flight, serviceType,
								(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);
					}

					if (newASM != null) {
						asmMessegeDTOMap.put(i, newASM);
						i++;
					}

				}

			}

			if (asmMessegeDTOMap != null && !asmMessegeDTOMap.isEmpty()) {
				Iterator itGdsIds = updatedList.iterator();

				while (itGdsIds.hasNext()) {
					Integer gdsId = (Integer) itGdsIds.next();
					Collection<String> bookingClassList = null;
					if (bcMap != null && bcMap.get(gdsId.toString()) != null) {
						bookingClassList = (Collection) bcMap.get(gdsId.toString());
					}
					GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatusMap.get(gdsId.toString());

					GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId,
							XMLStreamer.compose(asmMessegeDTOMap), "REF", existingFlight.getFlightId());

					if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
						GDSServicesModuleUtil.getSSMASMServiceBD().sendAdHocScheduleSubMessages(gdsStatus.getGdsCode(),
								airLineCode, asmMessegeDTOMap, bookingClassList);

					} else {
						pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
						gdsServiceDao.saveGDSPublisingMessage(pubMessage);
					}
				}
			}

		}
	}

	private boolean isEligibleToSendSubMessages(Collection<String> actions) {
		boolean isSendSubMessage = false;
		if (actions != null && AppSysParamsUtil.isEnableSendSsmAsmSubMessages()) {

			if (actions.contains(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME)
					&& (actions.contains(GDSFlightEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSFlightEventCollector.REPLACE_EXISTING_INFO) || actions
								.contains(GDSFlightEventCollector.LEG_TIME_CHANGE))) {
				isSendSubMessage = true;
			} else if (actions.contains(GDSFlightEventCollector.FLIGHT_NUMBER_CHANGED)
					&& (actions.contains(GDSFlightEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSFlightEventCollector.REPLACE_EXISTING_INFO) || actions
								.contains(GDSFlightEventCollector.LEG_TIME_CHANGE))) {
				isSendSubMessage = true;
			} else if (actions.contains(GDSFlightEventCollector.REPLACE_EXISTING_INFO)
					&& (actions.contains(GDSFlightEventCollector.FLIGHT_NUMBER_CHANGED)
							|| actions.contains(GDSFlightEventCollector.LEG_TIME_CHANGE) || actions
								.contains(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME))) {
				isSendSubMessage = true;
			} else if (actions.contains(GDSFlightEventCollector.MODEL_CHANGED)
					&& (actions.contains(GDSFlightEventCollector.LEG_TIME_CHANGE) || actions
							.contains(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME))) {
				isSendSubMessage = true;
			}

		}

		return isSendSubMessage;
	}

	private boolean isCanceExistFlightAndReInstate(Collection<String> actions) {
		if (actions != null
				&& actions.contains(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME)
				&& (actions.contains(GDSFlightEventCollector.LEG_TIME_CHANGE) || actions
						.contains(GDSFlightEventCollector.LEG_ADDED_DELETED))) {

			return true;
		}

		return false;
	}

	private boolean isCancelAndComposeNew(Collection<String> actions) {

		boolean cancelAndComposeNew = false;
		if (actions != null) {
			if (actions.contains(GDSFlightEventCollector.FLIGHT_NUMBER_CHANGED)
					&& (actions.contains(GDSFlightEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSFlightEventCollector.REPLACE_EXISTING_INFO)
							|| actions.contains(GDSFlightEventCollector.LEG_TIME_CHANGE))) {
				cancelAndComposeNew = true;
			} else if (actions.contains(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME)
					&& (actions.contains(GDSFlightEventCollector.REPLACE_EXISTING_INFO)
							|| actions.contains(GDSFlightEventCollector.LEG_TIME_CHANGE)
							|| actions.contains(GDSFlightEventCollector.MODEL_CHANGED))) {
				cancelAndComposeNew = true;

			}
		}

		return cancelAndComposeNew;
	}

	
}
