package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getDate;
import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getValue;
import iata.typea.v031.tkcreq.IATA;
import iata.typea.v031.tkcreq.TKCREQ;
import iata.typea.v031.tkcreq.Type0029;
import iata.typea.v031.tkcreq.Type0051;
import iata.typea.v031.tkcreq.Type0065;
import iata.typea.v031.tkcreq.UNB;
import iata.typea.v031.tkcreq.UNH;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageIdentifier;
import com.isa.thinair.gdsservices.core.typeA.model.RDD;
import com.isa.thinair.gdsservices.core.typeA.transformers.EDIFACTMessageProcess;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATARequest;

public class Tkcreq extends IATARequest {

	private TKCREQ tkcreq = null;

	public Tkcreq(JAXBElement<?> jaxbRequest) {
		super(jaxbRequest);
		tkcreq = getMessage();
		String msg0102 = tkcreq.getMSG().getMSG01().getMSG0102();
		CodeSetEnum.CS1225 msgBranchCode = CodeSetEnum.CS1225.getEnum(msg0102);
		customMessageBranchHandler = getMsgBranchHandler(msgBranchCode);
	}

	@Override
	public Map<String, Object> extractSpecificCommandParams() throws ModuleException {
		return customMessageBranchHandler.extractSpecificCommandParams(tkcreq);
	}

	@Override
	public String getBeanFromElement() {
		return customMessageBranchHandler.getCommandName();
	}

	@Override
	public InterchangeHeader getInterchangeHeader() throws InteractiveEDIException {
		for (Object elements : ((IATA) jaxbRequest.getValue()).getUNBAndUNGAndTKCREQ()) {
			if (elements instanceof UNB) {

				UNB unb = (UNB) elements;

				String syntaxId = getValue(unb, RDD.M, 1, 1);

				String versionNo = getValue(unb, RDD.M, 1, 2);
				// sender information
				String senderId = getValue(unb, RDD.M, 2, 1);
				String senderInternalId = getValue(unb, RDD.M, 2, 3);
				InterchangeInfo sender = new InterchangeInfo(senderId, senderInternalId);

				// sender information
				String recipientId = getValue(unb, RDD.M, 3, 1);
				String recipientCode = getValue(unb, RDD.M, 3, 3);
				InterchangeInfo recipient = new InterchangeInfo(recipientId, recipientCode);

				// TODO validate time
				String datePart = getValue(unb, RDD.M, 4, 1);
				String timePart = getValue(unb, RDD.M, 4, 2);
				Date timeStamp = getDate(new BigDecimal(datePart), new BigDecimal(timePart));

				String interchangeControlRef = getValue(unb, RDD.C, 5);
				Type0029 associatioinCode = getValue(unb, RDD.C, 8);

				return new InterchangeHeader(syntaxId, versionNo, sender, recipient, timeStamp, interchangeControlRef,
						associatioinCode.toString());
			}

		}

		return null;
	}

	@Override
	public MessageHeader getMessageHeader() throws InteractiveEDIException {
		MessageHeader header = new MessageHeader();
		UNH unh = tkcreq.getUNH();
		String msgRefNo = getValue(unh, RDD.M, 1);
		header.setMessageReferenceNumber(msgRefNo);

		Type0065 messageType = getValue(unh, RDD.M, 2, 1);
		String version = getValue(unh, RDD.M, 2, 2);
		String relNo = getValue(unh, RDD.M, 2, 3);
		Type0051 controllingAgency = getValue(unh, RDD.M, 2, 4);

		MessageIdentifier identifier = new MessageIdentifier(messageType.toString(), version, relNo, controllingAgency.toString());
		header.setMessageIdentifier(identifier);
		String commonAccessReference = getValue(unh, RDD.C, 3);
		header.setCommonAccessReference(commonAccessReference);
		return header;
	}

	public TKCREQ getMessage() {
		for (Object object2 : ((IATA) jaxbRequest.getValue()).getUNBAndUNGAndTKCREQ()) {
			if (object2 instanceof TKCREQ) {
				return (TKCREQ) object2;
			}
		}
		return null;
	}

	public EDIFACTMessageProcess getMsgBranchHandler(CodeSetEnum.CS1225 messageStructure) {
		switch (messageStructure) {

		case Display:
			return new TicketDisplay();

		case ChangeStatus:
			return new ChangeStatus();

		default:
			throw new UnsupportedOperationException("Message branch is not found or invalide");
		}
	}

}
