package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.ChangedNamesDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ChangePaxDetailsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class ChangePaxDetailsRequestExtractor extends ValidatorBase {

	public static ChangePaxDetailsRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		ChangePaxDetailsRequest changePaxDetailsRequest = new ChangePaxDetailsRequest();

		changePaxDetailsRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		changePaxDetailsRequest = (ChangePaxDetailsRequest) RequestExtractorUtil.addBaseAttributes(changePaxDetailsRequest,
				bookingRequestDTO);

		Map<Passenger, Passenger> oldNewPassengerDetails = new HashMap<Passenger, Passenger>();
		Collection<ChangedNamesDTO> changedNameDTOs = bookingRequestDTO.getChangedNameDTOs();

		if (changedNameDTOs != null && changedNameDTOs.size() > 0) {
			NameDTO oldNameDTO;
			NameDTO newNameDTO;

			for (ChangedNamesDTO changedNamesDTO : changedNameDTOs) {
				int oldNamesSize = changedNamesDTO.getOldNameDTOs().size();

				for (int i = 0; i < oldNamesSize; i++) {
					oldNameDTO = changedNamesDTO.getOldNameDTOs().get(i);
					newNameDTO = changedNamesDTO.getNewNameDTOs().get(i);
					changePaxDetailsRequest.setNameChangeExists(true);
					oldNewPassengerDetails.put(RequestExtractorUtil.parsePassenger(oldNameDTO),
							RequestExtractorUtil.parsePassenger(newNameDTO));
				}
			}
		}
		
		String userNotes = "";
		for (SSRDTO ssr : bookingRequestDTO.getSsrDTOs()) {
			if (ssr instanceof SSRInfantDTO && ((SSRInfantDTO)ssr).isNameChangeRelated()) {
				changePaxDetailsRequest.setInfantNameChangeExists(true);
			} else if (ssr instanceof SSRDocsDTO) {
				SSRDocsDTO docsDto = (SSRDocsDTO) ssr;
				if (docsDto.getPnrNameDTOs() != null && docsDto.getPnrNameDTOs().size() > 0) {
					NameDTO nameDTO = docsDto.getPnrNameDTOs().get(0);
					if (nameDTO != null) {
						if (oldNewPassengerDetails.size() > 0) {
							Passenger passenger = oldNewPassengerDetails.get(RequestExtractorUtil.parsePassenger(nameDTO));
							if (passenger != null) {
								passenger.setDocNumber(docsDto.getDocNumber());
								passenger.setDocCountryCode(docsDto.getDocCountryCode());
								passenger.setDocExpiryDate(docsDto.getDocExpiryDate());
							} else {
								Passenger newPassenger = RequestExtractorUtil.parsePassenger(nameDTO);
								newPassenger.setDocNumber(docsDto.getDocNumber());
								newPassenger.setDocCountryCode(docsDto.getDocCountryCode());
								newPassenger.setDocExpiryDate(docsDto.getDocExpiryDate());
								oldNewPassengerDetails.put(RequestExtractorUtil.parsePassenger(nameDTO), newPassenger);
							}
						} else {
							Passenger newPassenger = RequestExtractorUtil.parsePassenger(nameDTO);
							newPassenger.setDocNumber(docsDto.getDocNumber());
							newPassenger.setDocCountryCode(docsDto.getDocCountryCode());
							newPassenger.setDocExpiryDate(docsDto.getDocExpiryDate());
							oldNewPassengerDetails.put(RequestExtractorUtil.parsePassenger(nameDTO), newPassenger);
						}
					}
				}
			} else if (ssr instanceof SSRDocoDTO) {
				SSRDocoDTO docoDto = (SSRDocoDTO) ssr;
				if (docoDto.getPnrNameDTOs() != null && docoDto.getPnrNameDTOs().size() > 0) {
					NameDTO nameDTO = docoDto.getPnrNameDTOs().get(0);
					if (nameDTO != null) {
						if (oldNewPassengerDetails.size() > 0) {
							Passenger passenger = oldNewPassengerDetails.get(RequestExtractorUtil.parsePassenger(nameDTO));
							if (passenger != null) {
								passenger.setTravelDocType(docoDto.getTravelDocType());
								passenger.setVisaApplicableCountry(docoDto.getVisaApplicableCountry());
								passenger.setVisaDocIssueDate(docoDto.getVisaDocIssueDate());
								passenger.setVisaDocNumber(docoDto.getVisaDocNumber());
								passenger.setVisaDocPlaceOfIssue(docoDto.getVisaDocPlaceOfIssue());
								passenger.setPlaceOfBirth(docoDto.getPlaceOfBirth());
							} else {
								Passenger newPassenger = RequestExtractorUtil.parsePassenger(nameDTO);
								newPassenger.setTravelDocType(docoDto.getTravelDocType());
								newPassenger.setVisaApplicableCountry(docoDto.getVisaApplicableCountry());
								newPassenger.setVisaDocIssueDate(docoDto.getVisaDocIssueDate());
								newPassenger.setVisaDocNumber(docoDto.getVisaDocNumber());
								newPassenger.setVisaDocPlaceOfIssue(docoDto.getVisaDocPlaceOfIssue());
								newPassenger.setPlaceOfBirth(docoDto.getPlaceOfBirth());
								oldNewPassengerDetails.put(RequestExtractorUtil.parsePassenger(nameDTO), newPassenger);
							}
						} else {
							Passenger newPassenger = RequestExtractorUtil.parsePassenger(nameDTO);
							newPassenger.setTravelDocType(docoDto.getTravelDocType());
							newPassenger.setVisaApplicableCountry(docoDto.getVisaApplicableCountry());
							newPassenger.setVisaDocIssueDate(docoDto.getVisaDocIssueDate());
							newPassenger.setVisaDocNumber(docoDto.getVisaDocNumber());
							newPassenger.setVisaDocPlaceOfIssue(docoDto.getVisaDocPlaceOfIssue());
							newPassenger.setPlaceOfBirth(docoDto.getPlaceOfBirth());
							oldNewPassengerDetails.put(RequestExtractorUtil.parsePassenger(nameDTO), newPassenger);
						}
					}
				}
			} else if (ssr instanceof SSROTHERSDTO) {
				SSROTHERSDTO notesDto = (SSROTHERSDTO) ssr;
				userNotes += notesDto.getSsrValue();				
			}
		}
		
		changePaxDetailsRequest.setOldNewPassengerDetails(oldNewPassengerDetails);
		changePaxDetailsRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());
		changePaxDetailsRequest.setUserNote(userNotes);
		return changePaxDetailsRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			boolean isChangedNameDTOExist = bookingRequestDTO.getChangedNameDTOs() != null ? bookingRequestDTO
					.getChangedNameDTOs().size() > 0 ? true : false : false;
			boolean isSizesMatches = checkOldNewNameSizes(bookingRequestDTO.getChangedNameDTOs());
			
			boolean isChangedDocsExist = false;
			boolean isChangedDocoExist = false;
			boolean isChangedUserNoteExist = false;
			for (SSRDTO ssr : bookingRequestDTO.getSsrDTOs()) {
				if (ssr instanceof SSRDocsDTO) {
					isChangedDocsExist = true;
				} else if (ssr instanceof SSRDocoDTO) {
					isChangedDocoExist = true;
				} else if (ssr instanceof SSROTHERSDTO) {
					isChangedUserNoteExist = true;
				}
			}

			if (isChangedNameDTOExist || isChangedDocsExist ||isChangedDocoExist) {
				if (isSizesMatches) {
					this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				} else {
					String message = MessageUtil.getMessage("gdsservices.extractors.changeNamesMismatches");
					bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
					this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				}
			} else if (isChangedUserNoteExist) {
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			} else {
				String message = MessageUtil.getMessage("gdsservices.extractors.emptyChangeNames");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			}

			return bookingRequestDTO;
		}
	}

	private boolean checkOldNewNameSizes(List<ChangedNamesDTO> changedNameDTOs) {
		for (ChangedNamesDTO changedNamesDTO : changedNameDTOs) {
			int oldNamesSize = changedNamesDTO.getOldNameDTOs().size();
			int newNamesSize = changedNamesDTO.getNewNameDTOs().size();

			if (oldNamesSize != newNamesSize) {
				return false;
			}
		}

		return true;
	}
}
