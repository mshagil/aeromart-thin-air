package com.isa.thinair.gdsservices.core.dto;

public class Values2To<M, N> {
	private M m;
	private N n;

	public M getM() {
		return m;
	}

	public void setM(M m) {
		this.m = m;
	}

	public N getN() {
		return n;
	}

	public void setN(N n) {
		this.n = n;
	}
}
