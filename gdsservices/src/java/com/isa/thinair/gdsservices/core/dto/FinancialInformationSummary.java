package com.isa.thinair.gdsservices.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FinancialInformationSummary implements Serializable {

	private BigDecimal totalFare;
	private BigDecimal totalTax;
	private BigDecimal totalSurcharge;

	public BigDecimal getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	public BigDecimal getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public BigDecimal getTotalSurcharge() {
		return totalSurcharge;
	}

	public void setTotalSurcharge(BigDecimal totalSurcharge) {
		this.totalSurcharge = totalSurcharge;
	}
}
