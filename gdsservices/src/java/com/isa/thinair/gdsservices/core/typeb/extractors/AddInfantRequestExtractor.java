package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.AuthorizationUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddInfantRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.BookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Infant;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class AddInfantRequestExtractor extends ValidatorBase {

	private static final Log log = LogFactory.getLog(AddInfantRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());
		boolean isOverrideCountryConfig = false;
		if (gdsCredentialsDTO != null
				&& AuthorizationUtil.hasPrivilege(gdsCredentialsDTO.getPrivileges(),
						PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_OVERRIDE_COUNTRY_PAX_CONFIG)) {
			isOverrideCountryConfig = true;
		}

		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
		Collection<Infant> infants = AddInfantRequestExtractor.getRequest(gdsCredentialsDTO, ssrData, bookingRequestDTO)
				.getInfants();
		String errorMessage = ValidatorUtils.validatePaxDetails(bookingRequestDTO, isOverrideCountryConfig, infants);
		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else if (!errorMessage.isEmpty()) {
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, errorMessage);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// The General Action codes are validated at the high level.
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	public static AddInfantRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {

		AddInfantRequest addInfantRequest = new AddInfantRequest();

		addInfantRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		addInfantRequest = (AddInfantRequest) RequestExtractorUtil.addBaseAttributes(addInfantRequest, bookingRequestDTO);

		addInfantRequest = addInfantsSSRs(addInfantRequest, bookingRequestDTO, ssrData,gdsCredentialsDTO);
		addInfantRequest = addPassengerSSRs(addInfantRequest, ssrData);
		addInfantRequest = addPaymentDetail(addInfantRequest, ssrData);
		addInfantRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return addInfantRequest;
	}

	private static AddInfantRequest addInfantsSSRs(AddInfantRequest addInfantRequest, BookingRequestDTO bookingRequestDTO,
			SSRData ssrData, GDSCredentialsDTO gdsCredentialsDTO) {

		if (bookingRequestDTO.getChangedNameDTOs() == null || bookingRequestDTO.getChangedNameDTOs().isEmpty()) {
			// Change name and add infant operations are not happening simultaneously
			try {
				BookingRequest bookingRequest = RequestExtractorUtil.getBookingRequest(bookingRequestDTO);
				String extPnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getOriginatorRecordLocator());

				Reservation reservation = CommonUtil.loadReservation(gdsCredentialsDTO, extPnrReference, null);

				Collection<SSRInfantDTO> ssrs = ssrData.getInfantSSRDTOMap().values();
				Collection<Infant> infants = addInfantRequest.getInfants();
				Collection<Passenger> passengers = RequestExtractorUtil.getPassengers(bookingRequest, ssrData);
				Collection<SSRDocsDTO> docsSSRDTOs = ssrData.getDocsSSRDTOs();

				for (SSRInfantDTO ssrInfant : ssrs) {
					if (!ssrInfant.isNameChangeRelated()) {
						Infant infant = RequestExtractorUtil.getInfant(ssrInfant);
						RequestExtractorUtil.extractDOCSinfo(infant, docsSSRDTOs);
						Adult parent = null;

						if (!"".equals(ssrInfant.getIATAGuardianName())) {
							parent = pickAdultAsParent(ssrInfant.getIATAGuardianName(), passengers);
						} else if (passengers.size() == 1) {
							parent = (Adult) passengers.iterator().next();
						}

						if (!checkInfantExistsInOriginalReservation(reservation, infant)) {
							parent.setInfant(infant);
							infant.setParent(parent);
							infants.add(infant);
						}
					}
				}
				addInfantRequest.setInfants(infants);
			} catch (ModuleException ex) {

			}
		}
		return addInfantRequest;
	}

	private static AddInfantRequest addPaymentDetail(AddInfantRequest addInfantRequest, SSRData ssrData) {
		addInfantRequest.setPaymentDetail(SSRExtractorUtil.getPaymentDetail(ssrData));

		return addInfantRequest;
	}

	private static AddInfantRequest addPassengerSSRs(AddInfantRequest addInfantRequest, SSRData ssrData) {

		Map<String, Infant> resInfant = getPaxMap(addInfantRequest.getInfants());
		Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();
		Collection<SSRDTO> ssrDTOs = ssrData.getSsrDTOs();

		// add SSRDetailDTOs to paxs
		for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
			SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();
			List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();

			// add SSRDetailDTOs to selected paxs
			if (nameDTOs != null && nameDTOs.size() > 0) {
				for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
					NameDTO nameDTO = (NameDTO) iterNameDTO.next();
					SpecialServiceRequest ssr = new SpecialServiceRequest();
					ssr = SSRExtractorUtil.addBaseSSRAttributes(ssr, ssrDetailDTO);
					String iataPaxName = nameDTO.getIATAName();

					Infant resPax = resInfant.get(iataPaxName);

					if (resPax != null) {
						resPax.getSsrCollection().add(ssr);
					} else {
						//No need of throwing this exception
						// String errMsg = "addPassengerSSRs: Cannot locate the pax: " + iataPaxName;
						// log.error(errMsg);
						// throw new RuntimeException(errMsg);
					}
				}
			} else {
				// add SSRDetailDTOs to all paxs
				for (Iterator<Infant> iterAllPax = addInfantRequest.getInfants().iterator(); iterAllPax.hasNext();) {
					Infant resPax = (Infant) iterAllPax.next();
					SpecialServiceRequest ssr = new SpecialServiceDetailRequest();

					ssr = SSRExtractorUtil.addBaseSSRAttributes(ssr, ssrDetailDTO);
					resPax.getSsrCollection().add(ssr);
				}
			}
		}

		// add SSRDTOs to all paxs
		for (SSRDTO ssrDTO : ssrDTOs) {
			for (Iterator<Infant> iterAllPax = addInfantRequest.getInfants().iterator(); iterAllPax.hasNext();) {
				Infant resPax = (Infant) iterAllPax.next();
				SpecialServiceRequest ssr = new SpecialServiceDetailRequest();

				ssr = SSRExtractorUtil.addBaseSSRAttributes(ssr, ssrDTO);
				resPax.getSsrCollection().add(ssr);
			}
		}
		return addInfantRequest;
	}

	/**
	 * returns passenger map
	 * 
	 * @param passengers
	 * @return
	 */
	private static Map<String, Infant> getPaxMap(Collection<Infant> infants) {
		Map<String, Infant> paxMap = new HashMap<String, Infant>();

		for (Iterator<Infant> iterator = infants.iterator(); iterator.hasNext();) {
			Infant infant = (Infant) iterator.next();
			String iataPaxName = infant.getIATAName();

			paxMap.put(iataPaxName, infant);
		}

		return paxMap;
	}

	/**
	 * picks a adult as a parent
	 * 
	 * @param iataGuardianName
	 * @param passengers
	 * @return
	 */
	private static Adult pickAdultAsParent(String iataGuardianName, Collection<Passenger> passengers) {

		for (Iterator<Passenger> iterator = passengers.iterator(); iterator.hasNext();) {
			Passenger pax = (Passenger) iterator.next();

			if (GDSExternalCodes.PassengerType.ADULT.getCode().equals(pax.getPaxType())) {
				Adult adult = (Adult) pax;

				// FIXME: CHANGE THE LOGIC
				if (adult.getInfant() == null && iataGuardianName.equals(adult.getIATAName())) {
					return adult;
				}
			}
		}

		return null;
	}
	
	private static boolean checkInfantExistsInOriginalReservation(Reservation reservation, Infant infant) {
		if (reservation != null) {
			Set<ReservationPax> reservationPax = reservation.getPassengers();
			String iATAPaxName = infant.getLastName() + infant.getFirstName() + infant.getTitle();
			
			for (ReservationPax pax : reservationPax) {
				if (pax.getPaxType().equals(GDSExternalCodes.PassengerType.INFANT.getCode())) {
					String paxIATAName = pax.getLastName()+pax.getFirstName()+pax.getTitle();
					if (iATAPaxName.equals(paxIATAName)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
}
