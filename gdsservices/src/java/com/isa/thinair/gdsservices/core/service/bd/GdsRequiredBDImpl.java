package com.isa.thinair.gdsservices.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.gdsservices.api.service.GdsRequiredBD;

@Remote
public interface GdsRequiredBDImpl extends GdsRequiredBD {
}
