package com.isa.thinair.gdsservices.core.bl.typeA.common;

import java.util.Set;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.MessageFunction;

public class ConstraintsValidator {

	private EdiMessageHandler messageHandler;

	public ConstraintsValidator(EdiMessageHandler messageHandler) {
		this.messageHandler = messageHandler;
	}

	public void validateMessageFunction(MessageFunction messageFunction) throws GdsTypeAException {

		String msgFunction = messageFunction.getMessageFunction();
		Set<String> supportedFunctions = messageHandler.getAllowedMessageFunctions();

		if (!supportedFunctions.contains(msgFunction)) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.MESSAGE_FUNCTION_NOT_SUPPORTED,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		}
	}

}
