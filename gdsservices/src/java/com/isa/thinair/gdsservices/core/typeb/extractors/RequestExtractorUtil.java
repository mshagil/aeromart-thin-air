package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRMinorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.BookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Child;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Infant;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SingleSegment;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ActionCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.util.MasterDataUtil;

public class RequestExtractorUtil {

	public static GDSReservationRequestBase addBaseAttributes(GDSReservationRequestBase requestBase,
			BookingRequestDTO bookingRequestDTO) {

		requestBase.setCommunicationReference(bookingRequestDTO.getCommunicationReference());
		requestBase.setGds(bookingRequestDTO.getGds());
		requestBase.setMessageReceivedDateStamp(bookingRequestDTO.getMessageReceivedDateStamp());
		requestBase.setOriginatorAddress(bookingRequestDTO.getOriginatorAddress());
		requestBase.setOriginatorRecordLocator(getOriginatorRecordLocator(bookingRequestDTO));
		requestBase.setOriginatorAddress(bookingRequestDTO.getResponderAddress());
		requestBase.setResponderRecordLocator(getResponderOriginatorRecordLocator(bookingRequestDTO));

		return requestBase;
	}

	/**
	 * gets record locator
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	private static RecordLocator getOriginatorRecordLocator(BookingRequestDTO bookingRequestDTO) {
		RecordLocator recLocator = new RecordLocator();
		RecordLocatorDTO oriRecLocator = bookingRequestDTO.getOriginatorRecordLocator();

		recLocator.setAgentDutyCode(GDSApiUtils.maskNull(oriRecLocator.getAgentDutyCode()));
		recLocator.setBookingOffice(GDSApiUtils.maskNull(oriRecLocator.getBookingOffice()));
		recLocator.setCarrierCRSCode(GDSApiUtils.maskNull(oriRecLocator.getCarrierCRSCode()));
		recLocator.setClosestCityAirportCode(GDSApiUtils.maskNull(oriRecLocator.getClosestCityAirportCode()));
		recLocator.setCountryCode(GDSApiUtils.maskNull(oriRecLocator.getCountryCode()));
		recLocator.setCurrencyCode(GDSApiUtils.maskNull(oriRecLocator.getCurrencyCode()));
		recLocator.setErspId(GDSApiUtils.maskNull(oriRecLocator.getErspId()));
		recLocator.setFirstDepartureFrom(GDSApiUtils.maskNull(oriRecLocator.getFirstDepartureFrom()));
		recLocator.setPnrReference(GDSApiUtils.maskNull(oriRecLocator.getPnrReference()));
		recLocator.setTaOfficeCode(GDSApiUtils.maskNull(oriRecLocator.getTaOfficeCode()));
		recLocator.setUserID(GDSApiUtils.maskNull(oriRecLocator.getUserID()));
		recLocator.setUserType(GDSApiUtils.maskNull(oriRecLocator.getUserType()));
		recLocator.setPointOfSales(GDSApiUtils.maskNull(oriRecLocator.getPointOfSales()));

		return recLocator;
	}

	private static RecordLocator getResponderOriginatorRecordLocator(BookingRequestDTO bookingRequestDTO) {
		RecordLocator recLocator = new RecordLocator();
		RecordLocatorDTO resRecLocator = bookingRequestDTO.getResponderRecordLocator();

		if (resRecLocator != null) {
			recLocator.setAgentDutyCode(GDSApiUtils.maskNull(resRecLocator.getAgentDutyCode()));
			recLocator.setBookingOffice(GDSApiUtils.maskNull(resRecLocator.getBookingOffice()));
			recLocator.setCarrierCRSCode(GDSApiUtils.maskNull(resRecLocator.getCarrierCRSCode()));
			recLocator.setClosestCityAirportCode(GDSApiUtils.maskNull(resRecLocator.getClosestCityAirportCode()));
			recLocator.setCountryCode(GDSApiUtils.maskNull(resRecLocator.getCountryCode()));
			recLocator.setCurrencyCode(GDSApiUtils.maskNull(resRecLocator.getCurrencyCode()));
			recLocator.setErspId(GDSApiUtils.maskNull(resRecLocator.getErspId()));
			recLocator.setFirstDepartureFrom(GDSApiUtils.maskNull(resRecLocator.getFirstDepartureFrom()));
			recLocator.setPnrReference(GDSApiUtils.maskNull(resRecLocator.getPnrReference()));
			recLocator.setTaOfficeCode(GDSApiUtils.maskNull(resRecLocator.getTaOfficeCode()));
			recLocator.setUserID(GDSApiUtils.maskNull(resRecLocator.getUserID()));
			recLocator.setUserType(GDSApiUtils.maskNull(resRecLocator.getUserType()));
			recLocator.setPointOfSales(GDSApiUtils.maskNull(resRecLocator.getPointOfSales()));
		}

		return recLocator;
	}

	/**
	 * extracts segment data from a BookingSegmentDTO
	 * 
	 * @param bookingSegDTO
	 * @return
	 */

	public static Segment getSegment(BookingSegmentDTO bookingSegDTO) {
		Segment segment = new Segment();

		int arrivalDayOffset = bookingSegDTO.getDayOffSet();

		arrivalDayOffset = arrivalDayOffset * ((bookingSegDTO.getDayOffSetSign() == "-") ? -1 : 1);

		segment.setDepartureStation(bookingSegDTO.getDepartureStation());
		segment.setDepartureDate(bookingSegDTO.getDepartureDate());
		segment.setDepartureTime(bookingSegDTO.getDepartureTime());
		segment.setDepartureDayOffset(0);
		segment.setArrivalStation(bookingSegDTO.getDestinationStation());
		segment.setArrivalDate(CalendarUtil.addDateVarience(bookingSegDTO.getDepartureDate(), arrivalDayOffset));
		segment.setArrivalTime(bookingSegDTO.getArrivalTime());
		segment.setArrivalDayOffset(arrivalDayOffset);

		segment.setBookingCode(bookingSegDTO.getBookingCode());
		segment.setFlightNumber(bookingSegDTO.getCarrierCode() + bookingSegDTO.getFlightNumber());
		if (bookingSegDTO.getCsCarrierCode() != null && bookingSegDTO.getCsFlightNumber() != null) {
			segment.setCodeShareFlightNo(bookingSegDTO.getCsCarrierCode() + bookingSegDTO.getCsFlightNumber());
		}
		if (bookingSegDTO.getCsBookingCode() != null) {
			segment.setCodeShareBc(bookingSegDTO.getCsBookingCode());
		}
		segment.setActionCode(getActionCode(bookingSegDTO.getActionOrStatusCode()));

		return segment;
	}

	/**
	 * copies NameDTO details to Passenger
	 * 
	 * @param nameDTO
	 * @return Passenger
	 * 
	 */
	public static Passenger getPassenger(Passenger pax, NameDTO nameDTO) {
		String title = nameDTO.getPaxTitle();
		String firstName = nameDTO.getFirstName();
		Collection<String> titles = MasterDataUtil.getTitles();

		if (title != null && !titles.contains(title)) {
			firstName += title;
			title = null;
		}

		pax.setTitle(title);
		pax.setFirstName(firstName);
		pax.setLastName(nameDTO.getLastName());
		pax.setFamilyID(nameDTO.getFamilyID());

		return pax;
	}

	/**
	 * extracts a child from the ssrChildDTO
	 * 
	 * @param nameDTO
	 * @return
	 */
	public static Child getChild(NameDTO nameDTO) {
		Child child = new Child();

		return (Child) getPassenger(child, nameDTO);

	}

	/**
	 * extracts a child from the ssrChildDTO
	 * 
	 * @param nameDTO
	 * @return
	 */
	public static Adult getAdult(NameDTO nameDTO) {
		Adult adult = new Adult();

		return (Adult) getPassenger(adult, nameDTO);
	}

	/**
	 * extracts a infant from the ssrInfantDTO
	 * 
	 * @param ssrInfantDTO
	 * @return
	 */
	public static Infant getInfant(SSRInfantDTO ssrInfantDTO) {
		Infant infant = new Infant();
		String title = ssrInfantDTO.getInfantTitle();
		Collection<String> titles = MasterDataUtil.getTitles();
		String firstName = ssrInfantDTO.getInfantFirstName();

		if (!titles.contains(title)) {
			firstName += title;
			title = null;
		}

		infant.setTitle(title);
		infant.setFirstName(ssrInfantDTO.getInfantFirstName());
		infant.setLastName(ssrInfantDTO.getInfantLastName());
		infant.setInfantDateofBirth(ssrInfantDTO.getInfantDateofBirth());

		return infant;
	}

	/**
	 * Extracts passengers from the bookingRequest
	 * 
	 * @param bookingRequest
	 * @param ssrData
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Passenger> getPassengers(BookingRequest bookingRequest, SSRData ssrData) {
		List<NameDTO> newNamesDTOs = bookingRequest.getBookingRequestDTO().getNewNameDTOs();
		Collection<Passenger> passengers = null;
		if (bookingRequest.getBookingRequestDTO().isGroupBooking()) {
			passengers = getGroupPassengers(newNamesDTOs, ssrData);
		} else {
			passengers = getPassengers(newNamesDTOs, ssrData);
		}

		return passengers;
	}

	/**
	 * Extracts passengers from the newNamesDTOs
	 * 
	 * @param bookingRequest
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Passenger> getPassengers(List<NameDTO> newNamesDTOs, SSRData ssrData) {
		Collection<Passenger> passengers = new ArrayList<Passenger>();

		if (newNamesDTOs != null) {
			Map<String, SSRChildDTO> chidSSRMap = ssrData.getChildSSRDTOMap();
			Map<String, SSRMinorDTO> minorSSRMap = ssrData.getMinorSSRDTOMap();
			Map<String, SSRInfantDTO> infantSSRMap = ssrData.getInfantSSRDTOMap();
			Collection<SSRDocsDTO> docsSSRDTOs = ssrData.getDocsSSRDTOs();
			Collection<SSRDocoDTO> docoSSRDTOs = ssrData.getDocoSSRDTOs();

			for (Iterator<NameDTO> iterator = newNamesDTOs.iterator(); iterator.hasNext();) {
				NameDTO nameDTO = (NameDTO) iterator.next();
				String iataName = nameDTO.getIATAName();
				Passenger pax = null;

				if (chidSSRMap.get(iataName) != null || minorSSRMap.get(iataName) != null) {
					pax = RequestExtractorUtil.getChild(nameDTO);
					if (chidSSRMap.get(iataName) != null) {
						pax.setDateOfBirth(chidSSRMap.get(iataName).getDateOfBirth());
					}
					extractDOCSinfo(pax, docsSSRDTOs);
					extractDOCOinfo(pax, docoSSRDTOs);
				} else if (newNamesDTOs.size() == 1 && minorSSRMap.get("") != null) {
					pax = RequestExtractorUtil.getChild(nameDTO);
				} else {
					SSRInfantDTO ssrInfantDTO = infantSSRMap.get(iataName);

					if (ssrInfantDTO != null && !iataName.equals(ssrInfantDTO.getIATAGuardianName())) {
						// infant
					} else {
						pax = RequestExtractorUtil.getAdult(nameDTO);
						extractDOCSinfo(pax, docsSSRDTOs);
						extractDOCOinfo(pax, docoSSRDTOs);
					}
				}

				passengers.add(pax);
			}
		}

		return passengers;
	}
	
	public static void extractDOCSinfo(Passenger pax, Collection<SSRDocsDTO> docsSSRDTOs) {
		for (SSRDocsDTO docsSSRDTO : docsSSRDTOs) {
			if (docsSSRDTO.getDocFamilyName() != null && docsSSRDTO.getDocFamilyName().equals(pax.getLastName())
					&& docsSSRDTO.getDocType() != null && docsSSRDTO.getDocType().equals("P")) {
				if (docsSSRDTO.getNoOfPax() == 1 && docsSSRDTO.getPnrNameDTOs() != null
						&& docsSSRDTO.getPnrNameDTOs().size() == 1
						&& docsSSRDTO.getPnrNameDTOs().get(0).getFirstName().equals(pax.getFirstName())) {
					pax.setDocNumber(docsSSRDTO.getDocNumber());
					pax.setDocExpiryDate(docsSSRDTO.getDocExpiryDate());
					pax.setDocCountryCode(docsSSRDTO.getDocCountryCode());
					pax.setDateOfBirth(docsSSRDTO.getPassengerDateOfBirth());
					pax.setNationalityCode(docsSSRDTO.getPassengerNationality());
				}
			} else if (docsSSRDTOs != null && docsSSRDTOs.size() == 1) {
				if (docsSSRDTO.getDocType() != null && docsSSRDTO.getDocType().equals("P")
						&& docsSSRDTO.getDocFamilyName() == null && docsSSRDTO.getDocGivenName() == null) {
					pax.setDocNumber(docsSSRDTO.getDocNumber());
					pax.setDocExpiryDate(docsSSRDTO.getDocExpiryDate());
					pax.setDocCountryCode(docsSSRDTO.getDocCountryCode());
					pax.setDateOfBirth(docsSSRDTO.getPassengerDateOfBirth());
					pax.setNationalityCode(docsSSRDTO.getPassengerNationality());
				}
			}
		}
	}
	
	public static void extractDOCOinfo(Passenger pax, Collection<SSRDocoDTO> docoSSRDTOs) {
		for (SSRDocoDTO docoSSRDTO : docoSSRDTOs) {
			if (docoSSRDTO.getTravelDocType() != null && docoSSRDTO.getTravelDocType().equals("V")) {
				if (docoSSRDTO.getNoOfPax() == 1 && docoSSRDTO.getPnrNameDTOs() != null
						&& docoSSRDTO.getPnrNameDTOs().size() == 1
						&& docoSSRDTO.getPnrNameDTOs().get(0).getFirstName().equals(pax.getFirstName())) {
					pax.setVisaApplicableCountry(docoSSRDTO.getVisaApplicableCountry());
					pax.setVisaDocIssueDate(docoSSRDTO.getVisaDocIssueDate());
					pax.setVisaDocNumber(docoSSRDTO.getVisaDocNumber());
					pax.setVisaDocPlaceOfIssue(docoSSRDTO.getVisaDocPlaceOfIssue());
					pax.setTravelDocType(docoSSRDTO.getTravelDocType());
					pax.setPlaceOfBirth(docoSSRDTO.getPlaceOfBirth());
				}
			} else if (docoSSRDTOs != null && docoSSRDTOs.size() == 1) {
				if (docoSSRDTO.getTravelDocType() != null && docoSSRDTO.getTravelDocType().equals("V")) {
					pax.setVisaApplicableCountry(docoSSRDTO.getVisaApplicableCountry());
					pax.setVisaDocIssueDate(docoSSRDTO.getVisaDocIssueDate());
					pax.setVisaDocNumber(docoSSRDTO.getVisaDocNumber());
					pax.setVisaDocPlaceOfIssue(docoSSRDTO.getVisaDocPlaceOfIssue());
					pax.setTravelDocType(docoSSRDTO.getTravelDocType());
					pax.setPlaceOfBirth(docoSSRDTO.getPlaceOfBirth());
				}
			}
		}
	}
	
	/**
	 * Extracts passengers from the newNamesDTOs
	 * 
	 * @param bookingRequest
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Passenger> getGroupPassengers(List<NameDTO> newNamesDTOs, SSRData ssrData) {
		Collection<Passenger> passengers = new ArrayList<Passenger>();

		if (newNamesDTOs != null) {
			
			Map<String, SSRChildDTO> chidSSRMap = ssrData.getChildSSRDTOMap();
			Map<String, SSRMinorDTO> minorSSRMap = ssrData.getMinorSSRDTOMap();
			Map<String, SSRInfantDTO> infantSSRMap = ssrData.getInfantSSRDTOMap();
			int paxCount = 0;
			NameDTO nameDTO = new NameDTO();
			nameDTO.setFirstName("T.B.A.");
			nameDTO.setLastName("T.B.A.");
			if (newNamesDTOs.size() > 0) {
				paxCount = newNamesDTOs.get(0).getGroupMagnitude();
			}

			for (int i=0; i < paxCount; i++) {
				//TODO: support for child and infants
				Passenger pax = RequestExtractorUtil.getAdult(nameDTO);
				passengers.add(pax);
			}
		}
		return passengers;
	}

	/**
	 * extracts contact details
	 * 
	 * @param osis
	 * @return
	 */
	public static ContactDetail getContactDetails(Collection<OtherServiceInfo> osis) {

		ContactDetail contactDetail = new ContactDetail();

		for (OtherServiceInfo osi : osis) {
			String osiText = GDSApiUtils.maskNull(osi.getText());

			if (GDSExternalCodes.OSICodes.CONTACT_PHONE_NATURE_UNKNOWN.getCode().equals(osi.getCode())
					|| GDSExternalCodes.OSICodes.CONTACT_PHONE_BUSINESS.getCode().equals(osi.getCode())
					|| GDSExternalCodes.OSICodes.CONTACT_PHONE_HOME.getCode().equals(osi.getCode())
					|| GDSExternalCodes.OSICodes.CONTACT_PHONE_TRAVEL_AGENT.getCode().equals(osi.getCode())) {
				if (contactDetail.getLandPhoneNumber() == null) {
					contactDetail.setLandPhoneNumber(osiText);
				}
			} else if (GDSExternalCodes.OSICodes.CONTACT_PHONE_MOBILE.getCode().equals(osi.getCode())) {
				if (contactDetail.getMobilePhoneNumber() == null) {
					contactDetail.setMobilePhoneNumber(osiText);
				}
			} else if (GDSExternalCodes.OSICodes.CONTACT_PHONE_FAX.getCode().equals(osi.getCode())) {
				if (contactDetail.getFaxNumber() == null) {
					contactDetail.setFaxNumber(osiText);
				}
			} else if (GDSExternalCodes.OSICodes.CONTACT_ADDRESS.getCode().equals(osi.getCode())) {
				if (contactDetail.getStreetAddress() == null || contactDetail.getStreetAddress().length() == 0) {
					contactDetail.setStreetAddress(osiText);
				}
			} else if (GDSExternalCodes.OSICodes.CONTACT_EMAIL.getCode().equals(osi.getCode())) {
				if (contactDetail.getEmailAddress() == null) {
					contactDetail.setEmailAddress(osiText);
				}
			}
		}

		return contactDetail;
	}

	/**
	 * extracts a SingleSegment from a BookingSegmentDTO
	 * 
	 * @param segmentDTO
	 * @return
	 */
	public static SegmentDetail getSegmentDetail(BookingSegmentDTO segmentDTO) {
		SingleSegment segmentDetail = new SingleSegment();
		Segment segment = RequestExtractorUtil.getSegment(segmentDTO);
		segmentDetail.setSegment(segment);

		return segmentDetail;
	}

	/**
	 * extracts a SingleSegment from a BookingSegmentDTO
	 * 
	 * @param segmentDTO
	 * @param alternateSegmentDTO
	 * @return
	 */
	public static SegmentDetail getSegmentDetail(BookingSegmentDTO segmentDTO, BookingSegmentDTO alternateSegmentDTO) {
		throw new RuntimeException("Alternate Segments not suppoted in phase 1");
	}

	/**
	 * extracts a Segment from a SegmentDTO
	 * 
	 * @param segmentDTO
	 * @return
	 */
	public static Segment getSegment(SegmentDTO segmentDTO) {
		Segment segment = new Segment();

		segment.setFlightNumber(segmentDTO.getFlightNumber());
		segment.setDepartureStation(segmentDTO.getDepartureStation());
		segment.setDepartureDate(segmentDTO.getDepartureDate());
		segment.setArrivalStation(segmentDTO.getDestinationStation());
		segment.setBookingCode(segmentDTO.getBookingCode());
		segment.setCodeShareFlightNo(segmentDTO.getCsFlightNumber());	
		segment.setCodeShareBc(segmentDTO.getCsBookingCode());

		return segment;
	}

	/**
	 * gets BookingRequest
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	public static BookingRequest getBookingRequest(BookingRequestDTO bookingRequestDTO) {
		BookingRequest bookingRequest = new BookingRequest();
		bookingRequest.setBookingRequestDTO(bookingRequestDTO);

		return bookingRequest;
	}

	/**
	 * Parse NameDTO object to Passenger Object
	 * 
	 * @param nameDTO
	 * @return
	 */
	public static Passenger parsePassenger(NameDTO nameDTO) {
		Passenger passenger = new Passenger();

		passenger.setTitle(GDSApiUtils.maskNull(nameDTO.getPaxTitle()));
		passenger.setFirstName(GDSApiUtils.maskNull(nameDTO.getFirstName()));
		passenger.setLastName(GDSApiUtils.maskNull(nameDTO.getLastName()));

		return passenger;
	}

	/**
	 * returns the e-ticket number
	 * 
	 * @param ssrData
	 * @return
	 */
	public static String getTicketNo(SSRData ssrData) {
		String ticketNo = null;
		SSRDTO eTicketSSRDTO = ssrData.getETicketSSRDTO();

		if (eTicketSSRDTO != null) {
			ticketNo = eTicketSSRDTO.getSsrValue();
		}

		return ticketNo;
	}

	public static ActionCode getActionCode(String actionOrStatusCode) {
		ActionCode actionCode = null;

		if (GDSExternalCodes.ActionCode.NEED.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.NEED_ALTERNATE.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode().equals(actionOrStatusCode)) {
			actionCode = GDSInternalCodes.ActionCode.ONHOLD_REPLY_REQUIRED;
		} else if (GDSExternalCodes.ActionCode.SOLD.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.SOLD_FREE.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.SOLD_IFNOT_HOLDING.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode().equals(actionOrStatusCode)) {
			actionCode = GDSInternalCodes.ActionCode.ONHOLD;
		}

		return actionCode;
	}
	
	public static void setResponderRecordLocator(BookingRequestDTO bookingRequestDTO) {
		RecordLocatorDTO recordLocatorDTO = bookingRequestDTO.getResponderRecordLocator();
		if (recordLocatorDTO == null) {
			String originatorPnrReference = bookingRequestDTO.getOriginatorRecordLocator().getPnrReference();
			String pnr = null;
			try {
				pnr  = GDSServicesModuleUtil.getReservationQueryBD().getPnrByExtRecordLocator(originatorPnrReference, null, false);
			} catch (Exception ex) {
				//Do nothing
			}
			if (pnr != null) {
				recordLocatorDTO = new RecordLocatorDTO();
				recordLocatorDTO.setPnrReference(pnr);
				bookingRequestDTO.setResponderRecordLocator(recordLocatorDTO);
			}			
		}
	}
	
	public static void processExternalSegments(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		UpdateExternalSegmentsRequest updateExSegmentsRequest = UpdateExternalSegmentsRequestExtractor.getRequest(
				gdsCredentialsDTO, bookingRequestDTO);

		if (updateExSegmentsRequest.getExternalSegments() != null && updateExSegmentsRequest.getExternalSegments().size() > 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS, updateExSegmentsRequest);
		}
	}
}
