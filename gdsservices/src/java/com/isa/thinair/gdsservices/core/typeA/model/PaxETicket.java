package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class PaxETicket implements Serializable {

	private static final long serialVersionUID = -3391838179809970225L;

	private String eticketNumber;

	private List<PaxETicketSegment> paxEticketSegments = null;

	/**
	 * @return the eticketNumber
	 */
	public String getEticketNumber() {
		return eticketNumber;
	}

	/**
	 * @param eticketNumber
	 *            the eticketNumber to set
	 */
	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	/**
	 * @return the paxEticketSegments
	 */
	public List<PaxETicketSegment> getPaxEticketSegments() {
		if (paxEticketSegments == null) {
			paxEticketSegments = new LinkedList<PaxETicketSegment>();
		}
		return paxEticketSegments;
	}

	/**
	 * @param paxEticketSegments
	 *            the paxEticketSegments to set
	 */
	public void setPaxEticketSegments(List<PaxETicketSegment> paxEticketSegments) {
		this.paxEticketSegments = paxEticketSegments;
	}

	public void addPaxEticketSegments(PaxETicketSegment eTicketSegment) {
		eTicketSegment.setEticket(this.getEticketNumber());
		getPaxEticketSegments().add(eTicketSegment);
	}

	public int getSegmentCount() {
		return getPaxEticketSegments().size();
	}

}
