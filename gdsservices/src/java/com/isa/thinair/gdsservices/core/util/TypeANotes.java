package com.isa.thinair.gdsservices.core.util;

public interface TypeANotes {
	interface ErrorMessages {
		String NO_MESSAGE = "gds.type-a.error.no-message";

		String GENERIC_ERROR = "gds.type-a.error.default";

		String TICKET_NUMBER_DOES_NOT_EXIST = "gds.type-a.ticket.ticket-number-does-not-exist";
		String ITINERARY_MISMATCH = "gds.type-a.ticket.itinerary-mismatch";
		String TICKET_ISSUE_PARTIAL = "gds.type-a.ticket.ticket-issue-partial";
		String RESERVATION_CANCEL_VOID = "gds.type-a.ticket.reservation.cancelled-or-voided";
		String PASSENGER_DOES_NOT_EXIST = "gds.type-a.ticket.reservation.passenger-does-not-exist";
	}
}
