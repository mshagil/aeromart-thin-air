package com.isa.thinair.gdsservices.core.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import ognl.Ognl;
import ognl.OgnlContext;
import ognl.OgnlException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.core.dto.EDIMessageMetaDataDTO;

public class TypeADataConvertorUtill {

	enum TypeAPaxType {
		A, C, IN
	}

	private static Log log = LogFactory.getLog(TypeADataConvertorUtill.class);

	public static String DATE_FORMAT = "ddMMyy";
	public static String TIME_FORMAT = "HHmm";
	public static String DATE_TIME_FORMAT = "ddMMyy:HHmm";

	public static Date getDate(String dateString) {
		if (dateString != null) {
			SimpleDateFormat dateformat = new SimpleDateFormat(DATE_FORMAT);
			try {
				return dateformat.parse(dateString);
			} catch (ParseException e) {
				log.error("invalide date format");
			}
		}
		return null;
	}

	public static Date getDate(String dateString, String timeString) {

		StringBuilder sb = new StringBuilder();
		if (dateString != null) {
			try {
				if (timeString != null) {
					SimpleDateFormat dateformat = new SimpleDateFormat(DATE_TIME_FORMAT);
					sb.append(dateString).append(":").append(timeString);
					return dateformat.parse(sb.toString());
				} else {
					return getDate(dateString);
				}
			} catch (ParseException e) {
				log.error("invalid date and time format");
			}
		}
		return null;
	}

	public static BigDecimal getTimeDurationString(Date start, Date end) {
		int diffInMin = CalendarUtil.getTimeDifferenceInMinutes(start, end);
		return BigDecimal.valueOf(diffInMin);
	}

	public static String getTimeDurationString(int diffInMin) {
		StringBuilder sb = new StringBuilder();
		int hours = (int) (diffInMin / 60);
		int mins = (int) (diffInMin % 60);
		sb.append((hours < 10 ? "0" + hours : hours)).append(mins < 10 ? "0" + mins : mins);
		return sb.toString();
	}

	public static String getDateString(Date date) {
		SimpleDateFormat dateformat = new SimpleDateFormat(DATE_FORMAT);
		return dateformat.format(date);
	}

	public static String getTimeString(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT);
		return dateFormat.format(date);
	}

	public static String setValue(String val, String defaultVal) {
		return val != null ? val : defaultVal;
	}

	public static Object setValue(Object val, Object defaultVal) {
		return val != null ? val : defaultVal;
	}

	// TODO need to verify that OGNL is thread-safe or not.
	public static Object getValue(Object obj, String exp) {
		try {
			if (obj != null) {
				Object expr = Ognl.parseExpression(exp);
				Object val;
				val = Ognl.getValue(expr, new OgnlContext(), obj);
				if (val != null) {
					return val;
				}
			}
			return null;
		} catch (OgnlException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	public static String convertPaxType(String paxType) throws ModuleException {
		if (paxType.equals(TypeAPaxType.A.toString())) {
			return PassengerType.ADULT;
		} else if (paxType.equals(TypeAPaxType.C.toString())) {
			return PassengerType.CHILD;
		} else if (paxType.equals(TypeAPaxType.IN.toString())) {
			return PassengerType.INFANT;
		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	public static String convertToPaxType(String paxType) throws ModuleException {
		if (paxType.equals(PassengerType.ADULT)) {
			return TypeAPaxType.A.toString();
		} else if (paxType.equals(PassengerType.CHILD)) {
			return TypeAPaxType.C.toString();
		} else if (paxType.equals(PassengerType.INFANT)) {
			return TypeAPaxType.IN.toString();
		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	public static EDIMessageMetaDataDTO extractEdiOperationName(byte[] ediRawXml) {

		EDIMessageMetaDataDTO ediMetaDataDTO = new EDIMessageMetaDataDTO();
		new String(ediRawXml);
		InputStream ediXmlStream = null;
		InputSource ediXmlSource = null;

		XPathFactory xpathFactory;
		XPath xPath;
		XPathExpression xPathExpression;

		String nodeName;
		String content;

		try {

			ediXmlStream = new ByteArrayInputStream(ediRawXml);
			ediXmlSource = new InputSource(ediXmlStream);

			DocumentBuilderFactory domFactory =
					DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true);
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(ediXmlSource);

			xpathFactory = XPathFactory.newInstance();
			xPath = xpathFactory.newXPath();
			xPathExpression = xPath.compile("//UNH/UNH02/*");

			NodeList metaDataElements = (NodeList) xPathExpression.evaluate(doc, XPathConstants.NODESET);

			for (int i = 0; i < metaDataElements.getLength(); i++) {
				nodeName = metaDataElements.item(i).getNodeName();
				content = metaDataElements.item(i).getTextContent();

				if (nodeName.equals("UNH0201")) {
					ediMetaDataDTO.setIataOperation(IATAOperation.valueOf(content));
				} else if (nodeName.equals("UNH0202")) {
					ediMetaDataDTO.setMajorVersion(content);
				} else if (nodeName.equals("UNH0203")) {
					ediMetaDataDTO.setMinorVersion(content);
				}
			}

			xPathExpression = xPath.compile("//UNH/UNH03");
			Node element = (Node)xPathExpression.evaluate(doc, XPathConstants.NODE);
			if(element != null && element.getTextContent() != null){
				ediMetaDataDTO.setMessageReference(element.getTextContent());
			}

		} catch (Exception e) {
			throw new UnsupportedOperationException("Cannot determine operation type");
		} finally {
			if (ediXmlStream != null) {
				try {
					ediXmlStream.close();
				} catch (Exception e) {
				}
			}
		}

		return ediMetaDataDTO;
	}
}
