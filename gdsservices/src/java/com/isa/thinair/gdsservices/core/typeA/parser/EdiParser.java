package com.isa.thinair.gdsservices.core.typeA.parser;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.gdsservices.core.dto.FinancialInformation;

public interface EdiParser {
	Map<TypeAParseConstants.Segment, List<?>> parseEdiMessage(String ediMessage, Set<TypeAParseConstants.Segment> segsToParse);

	FinancialInformation parseFinancialInformation(Map<TypeAParseConstants.Segment, String> rawFinancialInfo);

	Map<TypeAParseConstants.Segment, String> generateFinancialInformation(FinancialInformation financialInformation);

}
