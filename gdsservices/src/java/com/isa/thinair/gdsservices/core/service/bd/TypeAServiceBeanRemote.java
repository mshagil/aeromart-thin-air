package com.isa.thinair.gdsservices.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.gdsservices.api.service.TypeAServiceBD;

@Remote
public interface TypeAServiceBeanRemote extends TypeAServiceBD {

}
