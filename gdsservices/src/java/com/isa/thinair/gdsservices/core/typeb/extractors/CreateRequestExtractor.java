package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.AuthorizationUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.BookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CreditCardDetail;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Infant;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.PaymentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

public class CreateRequestExtractor extends ValidatorBase {
	private static final Log log = LogFactory.getLog(CreateRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		boolean success = true;
		boolean isOverrideCountryConfig = false;
		boolean isContactDetailsExists = ValidatorUtils.isContactDetailsExists(bookingRequestDTO.getOsiDTOs(),
				bookingRequestDTO.getGds());

		boolean isOwnSegmentsExists = checkIsOwnSegmentsExists(bookingRequestDTO);

		if (isOwnSegmentsExists) {
			boolean isOwnSegmentsStatusCodesExist = checkIsOwnSegmentsStatusCodesExists(bookingRequestDTO);

			int maxAdultAllowed = 20;

			if (maxAdultAllowed < getAdultCount(bookingRequestDTO)) {

				String message = MessageUtil.getMessage("gdsservices.extractors.invalidAdultCount");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
				success = false;
			}

			if (isOwnSegmentsStatusCodesExist) {
				String message = MessageUtil.getMessage("gdsservices.extractors.createPNRStatusCodesExist");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
				success = false;
			}

			if (success) {
				success = validatePayments(bookingRequestDTO);
			}

			if (success) {
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				if (gdsCredentialsDTO != null
						&& AuthorizationUtil.hasPrivilege(gdsCredentialsDTO.getPrivileges(),
								PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_OVERRIDE_COUNTRY_PAX_CONFIG)) {
					isOverrideCountryConfig = true;
				}

				SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
				BookingRequest bookingRequest = RequestExtractorUtil.getBookingRequest(bookingRequestDTO);
				Collection<Passenger> passengers = RequestExtractorUtil.getPassengers(bookingRequest, ssrData);
				String errorMessage = ValidatorUtils.validatePaxDetails(bookingRequestDTO, isOverrideCountryConfig, passengers);
				if (!errorMessage.isEmpty()) {
					bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, errorMessage);
					this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
					return bookingRequestDTO;
				}

				return bookingRequestDTO;
			} else {
				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			}
		} else {
			String message = "Valid " + AppSysParamsUtil.getDefaultCarrierCode() + " segments not found";
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		}

	}

	/**
	 * Checks whether is own segment's status codes exists
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	private boolean checkIsOwnSegmentsStatusCodesExists(BookingRequestDTO bookingRequestDTO) {
		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (BookingSegmentDTO bookingSegmentDTO : segmentDTOs) {
			if (carrierCode.equals(GDSApiUtils.maskNull(bookingSegmentDTO.getCarrierCode()))) {
				GDSExternalCodes.StatusCode statusCode = GDSExternalCodes.StatusCode.getValue(bookingSegmentDTO
						.getActionOrStatusCode());

				if (statusCode != null) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Checks whether is own segment exists or not
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	private boolean checkIsOwnSegmentsExists(BookingRequestDTO bookingRequestDTO) {
		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (BookingSegmentDTO bookingSegmentDTO : segmentDTOs) {
			if (carrierCode.equals(GDSApiUtils.maskNull(bookingSegmentDTO.getCarrierCode()))) {
				return true;
			}
		}

		return false;
	}

	public static void composeReservationRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			CreateReservationRequest createRequest) {
		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
		Collection<OtherServiceInfo> osis = OSIExtractorUtil.getOSIs(bookingRequestDTO);

		createRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		createRequest = (CreateReservationRequest) RequestExtractorUtil.addBaseAttributes(createRequest, bookingRequestDTO);

		BookingRequest bookingRequest = RequestExtractorUtil.getBookingRequest(bookingRequestDTO);
		createRequest = addSegments(createRequest, bookingRequest);
		createRequest = addOtherAirlineSegments(createRequest, bookingRequest);
		createRequest.setPassengers(RequestExtractorUtil.getPassengers(bookingRequest, ssrData));
		createRequest = addInfants(createRequest, ssrData);
		createRequest = addContactDetails(createRequest, osis);
		createRequest = addPassengerSSRs(createRequest, ssrData);
		createRequest.setOsis(osis);
		createRequest = addPaymentDetail(createRequest, ssrData);
		createRequest = addPaymentPaymentTimeLimit(createRequest, ssrData);
		createRequest = addBlockSeatDetails(createRequest, bookingRequestDTO.getBlockedSeats());
		createRequest = addUserNotes(createRequest, bookingRequest);
		createRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());
	}

	public static CreateReservationRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {
		CreateReservationRequest createReservationRequest = new CreateReservationRequest();
		composeReservationRequest(gdsCredentialsDTO, bookingRequestDTO, createReservationRequest);
		return createReservationRequest;
	}

	/**
	 * adds infants to the CreateReservationRequest
	 * 
	 * @param createRequest
	 * @param ssrData
	 * @return
	 */
	private static CreateReservationRequest addInfants(CreateReservationRequest createRequest, SSRData ssrData) {

		Collection<SSRInfantDTO> ssrs = ssrData.getInfantSSRDTOMap().values();
		Collection<Passenger> passengers = createRequest.getPassengers();
		Collection<SSRDocsDTO> docsSSRDTOs = ssrData.getDocsSSRDTOs();
		Collection<SSRDocoDTO> docoSSRDTOs = ssrData.getDocoSSRDTOs();
		
		for (Iterator<SSRInfantDTO> iterator = ssrs.iterator(); iterator.hasNext();) {
			SSRInfantDTO ssrInfantDTO = (SSRInfantDTO) iterator.next();

			Infant infant = RequestExtractorUtil.getInfant(ssrInfantDTO);
			RequestExtractorUtil.extractDOCSinfo(infant, docsSSRDTOs);
			RequestExtractorUtil.extractDOCOinfo(infant, docoSSRDTOs);
			Adult parent = null;
			
			if(infant.getInfantDateofBirth() == null){
				infant.setInfantDateofBirth(infant.getDateOfBirth());
			}
			
			if (!"".equals(ssrInfantDTO.getIATAGuardianName())) {
				parent = pickAdultAsParent(ssrInfantDTO.getIATAGuardianName(), passengers);
			} else if (passengers.size() == 1) {
				parent = (Adult) passengers.iterator().next();
			}

			parent.setInfant(infant);
			infant.setParent(parent);
			passengers.add(infant);
		}

		return createRequest;
	}

	/**
	 * adds segments to the CreateReservationRequest
	 * 
	 * @param createRequest
	 * @param bookingRequest
	 * @return
	 */
	private static CreateReservationRequest addSegments(CreateReservationRequest createRequest, BookingRequest bookingRequest) {

		BookingRequestDTO bookingRequestDTO = bookingRequest.getBookingRequestDTO();
		String msgIdentifier = bookingRequestDTO.getMessageIdentifier();
		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		List<Segment> externalSegments = new ArrayList<Segment>();
		List<SegmentDetail> internalNewSegments = new ArrayList<SegmentDetail>();
		List<BookingSegmentDTO> segmentsAdded = new ArrayList<BookingSegmentDTO>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			BookingSegmentDTO segmentDTO = (BookingSegmentDTO) iterator.next();

			if (carrierCode.equals(segmentDTO.getCarrierCode()) && !segmentsAdded.contains(segmentDTO)) {
				String actionOrStatusCode = segmentDTO.getActionOrStatusCode();
				SegmentDetail segmentDetail = null;

				segmentsAdded.add(segmentDTO);

				// need this segment. if not immediately preceding segment
				if (segmentDTO.getActionOrStatusCode().equals(GDSExternalCodes.ActionCode.NEED_ALTERNATE.getCode())) {
					BookingSegmentDTO alternateSegmentDTO = (BookingSegmentDTO) iterator.next();

					if (!segmentsAdded.contains(alternateSegmentDTO)) {
						segmentsAdded.add(alternateSegmentDTO);
						segmentDetail = RequestExtractorUtil.getSegmentDetail(segmentDTO, alternateSegmentDTO);
					} else {
						segmentDetail = RequestExtractorUtil.getSegmentDetail(segmentDTO);
					}
				} else {
					segmentDetail = RequestExtractorUtil.getSegmentDetail(segmentDTO);
				}

				internalNewSegments.add(segmentDetail);

			} else {
				// other airline segments
				Segment segment = RequestExtractorUtil.getSegment(segmentDTO);
				externalSegments.add(segment);
			}
		}

		createRequest.setExternalSegments(externalSegments);
		createRequest.setNewSegmentDetails(internalNewSegments);

		return createRequest;
	}
	
	private static CreateReservationRequest addOtherAirlineSegments(CreateReservationRequest createRequest, BookingRequest bookingRequest) {

		BookingRequestDTO bookingRequestDTO = bookingRequest.getBookingRequestDTO();
		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getOtherAirlineSegmentDTOs();
		List<Segment> externalSegments = new ArrayList<Segment>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			BookingSegmentDTO segmentDTO = (BookingSegmentDTO) iterator.next();

			if (!carrierCode.equals(segmentDTO.getCarrierCode())) {
				// other airline segments
				Segment segment = RequestExtractorUtil.getSegment(segmentDTO);
				externalSegments.add(segment);
			}
		}
		createRequest.setExternalSegments(externalSegments);
		return createRequest;
	}
	
	private static CreateReservationRequest addUserNotes(CreateReservationRequest createRequest, BookingRequest bookingRequest) {
		if (bookingRequest.getBookingRequestDTO().isGroupBooking()) {
			List<NameDTO> newNamesDTOs = bookingRequest.getBookingRequestDTO().getNewNameDTOs();
			if (newNamesDTOs != null && newNamesDTOs.size() > 0) {
				String groupName = newNamesDTOs.get(0).getFirstName();
				createRequest.setUserNotes("GroupName: " + groupName);
			}
		}
		return createRequest;
	}


	/**
	 * picks a adult as a parent
	 * 
	 * @param iataGuardianName
	 * @param passengers
	 * @return
	 */
	private static Adult pickAdultAsParent(String iataGuardianName, Collection<Passenger> passengers) {

		for (Iterator<Passenger> iterator = passengers.iterator(); iterator.hasNext();) {
			Passenger pax = (Passenger) iterator.next();

			if (GDSExternalCodes.PassengerType.ADULT.getCode().equals(pax.getPaxType())) {
				Adult adult = (Adult) pax;

				// FIXME: CHANGE THE LOGIC
				if (adult.getInfant() == null && iataGuardianName.equals(adult.getIATAName())) {
					return adult;
				}
			}
		}

		return null;
	}

	/**
	 * extracts contact details
	 * 
	 * @param createRequest
	 * @param osis
	 * @return
	 */
	private static CreateReservationRequest addContactDetails(CreateReservationRequest createRequest,
			Collection<OtherServiceInfo> osis) {
		ContactDetail contactDetail = RequestExtractorUtil.getContactDetails(osis);
		createRequest.setContactDetail(contactDetail);

		return createRequest;
	}
	
	/**
	 * set block seats
	 * 
	 * @param createRequest
	 * @param blockseats
	 * @return
	 */
	private static CreateReservationRequest addBlockSeatDetails(CreateReservationRequest createRequest,
			Collection<TempSegBcAllocDTO> blockedSeats) {
		if (blockedSeats != null && blockedSeats.size() > 0) {
			createRequest.setBlockedSeats(blockedSeats);
		}
		return createRequest;
	}

	private static CreateReservationRequest addPassengerSSRs(CreateReservationRequest createRequest, SSRData ssrData) {

		Map<String, Passenger> resPaxMap = getPaxMap(createRequest.getPassengers());
		Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();
		Collection<SSRDTO> ssrDTOs = ssrData.getSsrDTOs();

		// add SSRDetailDTOs to paxs
		for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
			SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();
			List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();

			// add SSRDetailDTOs to selected paxs
			if (nameDTOs != null && nameDTOs.size() > 0) {
				for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
					NameDTO nameDTO = (NameDTO) iterNameDTO.next();
					SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
					ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
					String iataPaxName = nameDTO.getIATAName();

					Passenger resPax = resPaxMap.get(iataPaxName);

					if (resPax != null) {
						resPax.getSsrCollection().add(ssr);
					} else {
						String errMsg = "addPassengerSSRs: Cannot locate the pax: " + iataPaxName;
						log.error(errMsg);
						throw new RuntimeException(errMsg);
					}
				}
			} else {
				// add SSRDetailDTOs to all paxs
				for (Iterator<Passenger> iterAllPax = createRequest.getPassengers().iterator(); iterAllPax.hasNext();) {
					Passenger resPax = (Passenger) iterAllPax.next();
					SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();

					ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
					resPax.getSsrCollection().add(ssr);
				}
			}
		}

		// add SSRDTOs to all paxs
		for (SSRDTO ssrDTO : ssrDTOs) {
			for (Iterator<Passenger> iterAllPax = createRequest.getPassengers().iterator(); iterAllPax.hasNext();) {
				Passenger resPax = (Passenger) iterAllPax.next();
				SpecialServiceRequest ssr = new SpecialServiceDetailRequest();

				ssr = SSRExtractorUtil.addBaseSSRAttributes(ssr, ssrDTO);
				resPax.getSsrCollection().add(ssr);
			}
		}
		return createRequest;
	}

	/**
	 * returns passenger map
	 * 
	 * @param passengers
	 * @return
	 */
	private static Map<String, Passenger> getPaxMap(Collection<Passenger> passengers) {
		Map<String, Passenger> paxMap = new HashMap<String, Passenger>();

		for (Iterator<Passenger> iterator = passengers.iterator(); iterator.hasNext();) {
			Passenger passenger = (Passenger) iterator.next();
			String iataPaxName = passenger.getIATAName();

			paxMap.put(iataPaxName, passenger);
		}

		return paxMap;
	}

	/**
	 * adds credit card details to the CreateReservationRequest
	 * 
	 * @param createRequest
	 * @param ssrData
	 * @return
	 */
	private static CreateReservationRequest addPaymentDetail(CreateReservationRequest createRequest, SSRData ssrData) {
		createRequest.setPaymentDetail(SSRExtractorUtil.getPaymentDetail(ssrData));

		return createRequest;
	}

	private static CreateReservationRequest addPaymentPaymentTimeLimit(CreateReservationRequest createRequest, SSRData ssrData) {
		createRequest.setPaymentTimeLimit(SSRExtractorUtil.getPaymentTimeLimit(ssrData));

		return createRequest;
	}

	/**
	 * validates payment details
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	private boolean validatePayments(BookingRequestDTO bookingRequestDTO) {
		boolean success = true;
		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
		PaymentDetail paymentDetail = SSRExtractorUtil.getPaymentDetail(ssrData);

		if (paymentDetail != null && paymentDetail instanceof CreditCardDetail) {
			CreditCardDetail cardDetail = (CreditCardDetail) paymentDetail;

			if (cardDetail.isEmpty()) {
				success = false;
				String message = MessageUtil.getMessage("gdsservices.extractors.empty.ccdetails");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			}
		}

		return success;
	}

	/**
	 * This method can be get the adult count
	 * 
	 * @param bookingRequestDTO
	 * @return adult count of the reservation
	 */
	private int getAdultCount(BookingRequestDTO bookingRequestDTO) {

		int childAdultCount = bookingRequestDTO.getNewNameDTOs().size();
		int childCount = 0;

		for (Iterator<SSRDTO> itr = bookingRequestDTO.getSsrDTOs().iterator(); itr.hasNext();) {

			SSRDTO ssrDTO = itr.next();

			if (ssrDTO.getCodeSSR().equals(GDSExternalCodes.SSRCodes.valueOf("CHILD").getCode())) {

				childCount++;

			}

		}

		return (childAdultCount - childCount);

	}
}
