package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAConcurrencyException;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.typeA.transformers.DTOsTransformerUtil;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

public class IssueTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(IssueTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	private String pnr;
	private LCCClientReservation reservation;
	private GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;


	protected void handleMessage() {

		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		tktreqMessage = tktReq.getMessage();

		tktRes =  new AATKTRES();
		tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);
		messageDTO.setResponse(tktRes);
		log.info("Inside handleMessage");


		try {

			preHandleMessage();
			issueTicket();
			postHandleMessage();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.ISSUE);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
			messageDTO.getErrors().add(e.getErrorMessageKey());

			log.error("Error Occurred ----", e);
		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.ISSUE);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);
		}
		log.info("Exit handleMessage");

	}

	private void preHandleMessage() throws GdsTypeAException {

		tktReq = (AATKTREQ) messageDTO.getRequest();

		pnr = DTOsTransformerUtil.getOwnPnr(tktReq.getMessage().getReservationControlInformation());

		try {
			GDSServicesModuleUtil.getGDSNotifyBD().reservationLock(pnr);
		} catch (ModuleException e) {
			throw new GdsTypeAConcurrencyException();
		}

		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
			if (reservation == null) {
				throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

		preValidation();
	}

	private void preValidation() throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();
		LCCClientReservationPax pax;
		LccClientPassengerEticketInfoTO coupon;
		String gdsCarrierCode;

		gdsCarrierCode = tktreqMessage.getOriginatorInformation().getSenderSystemDetails().getCompanyCode();
		Map<String, List<TicketNumberDetails>> paxTickets = new HashMap<String, List<TicketNumberDetails>>();

		for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
			if (traveller.getTravellers().size() != 1) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.TOO_MUCH_DATA, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			if (pax == null) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.PASSENGER_NAME_MISMATCH_WITH_PNR, TypeANotes.ErrorMessages.PASSENGER_DOES_NOT_EXIST);
			}

			for (TicketNumberDetails ticketNumberDetails : traveller.getTickets()) {
				for (TicketCoupon ticketCoupon : ticketNumberDetails.getTicketCoupon()) {
					coupon = GdsUtil.lccPaxEticket(ticketNumberDetails, ticketCoupon, pax.geteTickets());
					if (coupon != null && coupon.getExternalPaxETicketNo() != null) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.ALREADY_TICKETED, TypeANotes.ErrorMessages.NO_MESSAGE);
					}
				}
			}

			if (!paxTickets.containsKey(pax.getTravelerRefNumber())) {
				paxTickets.put(pax.getTravelerRefNumber(), new ArrayList<TicketNumberDetails>());
			}

			paxTickets.get(pax.getTravelerRefNumber()).addAll(traveller.getTickets());

		}

		if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL) ||
				reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.VOID)) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.RESERVATION_CANCEL_VOID);
		}



		TicketCoupon cpn;
		for (LCCClientReservationSegment segment : reservation.getSegments()) {
			if (!segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				for (List<TicketNumberDetails> tickets : paxTickets.values()) {
					cpn = GDSDTOUtil.resolveTicketCoupon(tickets, segment, gdsCarrierCode);

					if (cpn == null) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.TICKET_ISSUE_PARTIAL);
					}
				}
			}
		}

		checkItineraryCompatibility(reservation.getSegments(), paxTickets.values());

		/*
		LCCClientReservationSegment segment;
		for (List<TicketNumberDetails> tickets : paxTickets.values()) {
			for (TicketNumberDetails ticket : tickets) {
				for (TicketCoupon ticketCoupon : ticket.getTicketCoupon()) {

					segment = GDSDTOUtil.resolveReservationSegment(reservation.getSegments(), ticketCoupon, gdsCarrierCode);

					if (segment == null) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR,
								TypeANotes.ErrorMessages.ITINERARY_MISMATCH);
					}
				}
			}
		}
		*/

		/*
		for (LCCClientReservationPax passenger : reservation.getPassengers()) {
			if (! (passenger.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CANCEL) ||
					passenger.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED))) {
				if (!paxTickets.containsKey(passenger.getTravelerRefNumber())) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
				}
			}
		}
        */

	}

	protected void issueTicket() throws GdsTypeAException {

		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		handleDiscrepancies();

		tktresMessage = tktRes.getMessage();

		messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.ISSUE);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

	}

	private void postHandleMessage() throws GdsTypeAException {
		TravellerTicketingInformationWrapper travellerWrapper;

		TKTREQMessage tktreqMessage = tktReq.getMessage();

		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
			if (reservation == null) {
				throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		try {
			List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger.getMergedETicketCoupons(tktreqMessage.getTravellerTicketingInformation(), reservation);
			List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);
			if (!serviceResponce.isSuccess()) {
				throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR, TypeANotes.ErrorMessages.NO_MESSAGE);

		}

		for (TravellerTicketingInformation ticketTraveller : tktreqMessage.getTravellerTicketingInformation()) {
			travellerWrapper = GDSDTOUtil.resolvePassengerByTraveller(gdsReservation, ticketTraveller);

			if (travellerWrapper == null) {
				travellerWrapper = new TravellerTicketingInformationWrapper(ticketTraveller);
				travellerWrapper.setOriginatorInformation(tktreqMessage.getOriginatorInformation());
				travellerWrapper.setTicketingAgentInformation(tktreqMessage.getTicketingAgentInformation());

				gdsReservation.getTravellerInformation().add(travellerWrapper);

				GDSDTOUtil.mergeCouponTransitions(travellerWrapper.getCouponTransitions(), ticketTraveller.getTickets(),
						tktreqMessage.getOriginatorInformation());

			} else {
				throw new GdsTypeAException();
			}
		}
		gdsReservation.getReservationControlInformation().addAll(tktreqMessage.getReservationControlInformation());

		GdsInfoDaoHelper.saveGdsReservationTicketView(pnr, gdsReservation);

		validateIssuance();
		
		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);
		externalReservation.setPriceSynced(CommonsConstants.YES);
		gdsTypeAServiceDAO.gdsTypeAServiceSaveOrUpdate(externalReservation);
		
//		checkPassengerStatusForConfirmation();

	}

	private void checkPassengerStatusForConfirmation() throws GdsTypeAException {
		
		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);			
			for(LCCClientReservationPax pax : reservation.getPassengers()){
				if(! pax.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)){
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}
		
	}

	private void handleDiscrepancies() throws GdsTypeAException {
		TKTREQMessage tktreqMessage = tktReq.getMessage();
		LCCClientReservationPax pax;
		int pnrPaxId;

		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
		ticketsAssembler.setPnr(pnr);
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());

		Map<Integer, GDSPaxChargesTO> paxAdjustment = new HashMap<Integer, GDSPaxChargesTO>();
		GDSPaxChargesTO paxChargesTO;

		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
		gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
		gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
		gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());
		gdsChargeAdjustmentRQ.setGdsChargesByPax(paxAdjustment);


		for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
			pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			pnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());

			FinancialInformationAssembler financialInformationAssembler = new FinancialInformationAssembler();
			financialInformationAssembler.setFinancialInformation(traveller.getText());
			financialInformationAssembler.setTaxDetails(traveller.getTaxDetails());
			financialInformationAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
			financialInformationAssembler.calculateAmounts();

			if (!paxAdjustment.containsKey(pnrPaxId)) {
				paxChargesTO = new GDSPaxChargesTO();
				paxChargesTO.setPaxType(pax.getPaxType());
				paxChargesTO.setPnrPaxId(pnrPaxId);
				paxChargesTO.setFareAmount(BigDecimal.ZERO);
				paxChargesTO.setTaxAmount(BigDecimal.ZERO);
				paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
				paxAdjustment.put(pnrPaxId, paxChargesTO);
			}

			paxChargesTO = paxAdjustment.get(pnrPaxId);

			paxChargesTO.setFareAmount(paxChargesTO.getFareAmount().add(financialInformationAssembler.getFareAmount()));
			paxChargesTO.setTaxAmount(paxChargesTO.getTaxAmount().add(financialInformationAssembler.getTaxAmount()));
			paxChargesTO.setSurChargeAmount(paxChargesTO.getSurChargeAmount().add(financialInformationAssembler.getSurcharge()));

			GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, traveller, pax);

		}

		try {
			ticketsAssembler.setGdsChargeAdjustmentRQ(gdsChargeAdjustmentRQ);

			TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);


		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

	}


	private void validateIssuance() throws GdsTypeAException {
		TKTREQMessage tktreqMessage = tktReq.getMessage();
		LCCClientReservationPax pax;

		Map<String, Set<String>> issuedCoupons;

		for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
			pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			issuedCoupons = new HashMap<String, Set<String>>();
			for (LccClientPassengerEticketInfoTO eticketInfoTO : pax.geteTickets()) {
				if (!issuedCoupons.containsKey(eticketInfoTO.getExternalPaxETicketNo())) {
					issuedCoupons.put(eticketInfoTO.getExternalPaxETicketNo(), new HashSet<String>());
				}
				issuedCoupons.get(eticketInfoTO.getExternalPaxETicketNo()).add(String.valueOf(eticketInfoTO.getExternalCouponNo()));
			}


			for (TicketNumberDetails ticketNumberDetails : traveller.getTickets()) {
				for (TicketCoupon ticketCoupon : ticketNumberDetails.getTicketCoupon()) {
					if (ticketNumberDetails.getDocumentType().equals(TypeAConstants.DocumentType.TICKET) &&
							!(issuedCoupons.containsKey(ticketNumberDetails.getTicketNumber()) &&
									issuedCoupons.get(ticketNumberDetails.getTicketNumber()).contains(ticketCoupon.getCouponNumber()))) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR, TypeANotes.ErrorMessages.NO_MESSAGE);
					}
				}
			}
		}
	}

	private void checkItineraryCompatibility(Collection<LCCClientReservationSegment> segments, Collection<List<TicketNumberDetails>> tickets)
		throws GdsTypeAException {

		int confirmedSegCount = 0;
		int requestedCoupons = 0;

		for (LCCClientReservationSegment segment : segments) {
			if (!segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				confirmedSegCount++;
			}
		}

		for (List<TicketNumberDetails> conjTicket : tickets) {
			for (TicketNumberDetails ticket : conjTicket) {
				requestedCoupons += ticket.getTicketCoupon().size();
			}
		}

		if ((confirmedSegCount * tickets.size()) != requestedCoupons) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.ITINERARY_MISMATCH);
		}
	}
}
