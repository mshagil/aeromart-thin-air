package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Date;

import com.isa.thinair.gdsservices.api.dto.external.SSRCreditCardDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSREmergencyContactDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROSAGDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRPassportDTO;
import com.isa.thinair.gdsservices.api.dto.internal.BSPDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreditCardDetail;
import com.isa.thinair.gdsservices.api.dto.internal.EmergencyContactDetailSSR;
import com.isa.thinair.gdsservices.api.dto.internal.PassportSSR;
import com.isa.thinair.gdsservices.api.dto.internal.PaymentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.CardCompanyCode;

public class SSRExtractorUtil {
	/**
	 * extracts a EmergencyContactDetailSSR from a SSRContactDTO
	 * 
	 * @param ssrContactDTO
	 * @return
	 */
	public static EmergencyContactDetailSSR getEmergencyContactDetailSSR(SSREmergencyContactDetailDTO ssrContactDTO) {
		EmergencyContactDetailSSR emergencyContactSsr = new EmergencyContactDetailSSR();

		emergencyContactSsr = (EmergencyContactDetailSSR) addBaseSSRAttributes(emergencyContactSsr, ssrContactDTO);
		emergencyContactSsr.setPassengerFirstName(ssrContactDTO.getPassengerFirstName());
		emergencyContactSsr.setPassengerLastName(ssrContactDTO.getPassengerLastName());
		emergencyContactSsr.setPassengerTitle(ssrContactDTO.getPassengerTitle());
		emergencyContactSsr.getContactDetail().setFirstName(ssrContactDTO.getEmContactName());
		emergencyContactSsr.getContactDetail().setLandPhoneNumber(ssrContactDTO.getEmContactNumberwithCountry());

		return emergencyContactSsr;
	}

	/**
	 * extracts a PassportSSR from a SSRPassportDTO
	 * 
	 * @param passportSSRDTO
	 * @return
	 */
	private static PassportSSR getPassportSSR(SSRPassportDTO passportSSRDTO) {
		PassportSSR passportSSR = new PassportSSR();

		passportSSR = (PassportSSR) addBaseSSRAttributes(passportSSR, passportSSRDTO);
		passportSSR.setPassportNumber(passportSSRDTO.getPassengerPassportNo());

		return passportSSR;
	}

	/**
	 * adds base SSRDTO values to SpecialServiceRequest
	 * 
	 * @param ssr
	 * @param ssrDTO
	 * @return
	 */
	public static SpecialServiceRequest addBaseSSRAttributes(SpecialServiceRequest ssr, SSRDTO ssrDTO) {

		ssr.setCode(ssrDTO.getCodeSSR());
		ssr.setValue(ssrDTO.getSsrValue());
		ssr.setComment(ssrDTO.getFreeText());

		return ssr;
	}

	/**
	 * adds SSRDetailDTO values to SpecialServiceDetailRequest
	 * 
	 * @param ssr
	 * @param ssrDTO
	 * @return
	 */
	public static SpecialServiceDetailRequest addSSRDetailAttributes(SpecialServiceDetailRequest ssr, SSRDetailDTO ssrDetailDTO) {

		ssr.setCode(ssrDetailDTO.getCodeSSR());
		ssr.setValue(ssrDetailDTO.getSsrValue());
		ssr.setComment(ssrDetailDTO.getFreeText());
		if (ssrDetailDTO.getSegmentDTO() != null) {
			Segment segment = new Segment();
			segment.setDepartureStation(ssrDetailDTO.getSegmentDTO().getDepartureStation());
			segment.setArrivalStation(ssrDetailDTO.getSegmentDTO().getDestinationStation());
			segment.setDepartureDate(ssrDetailDTO.getSegmentDTO().getDepartureDate());
			segment.setFlightNumber(ssrDetailDTO.getSegmentDTO().getFlightNumber());
			segment.setBookingCode(ssrDetailDTO.getSegmentDTO().getBookingCode());
			segment.setCodeShareFlightNo(ssrDetailDTO.getSegmentDTO().getCsFlightNumber());
			segment.setCodeShareBc(ssrDetailDTO.getSegmentDTO().getCsBookingCode());
			ssr.setSegment(segment);
		}
		return ssr;
	}

	/**
	 * extracts a PaymentDetail from a SSRData
	 * 
	 * @param ssrData
	 * @return
	 */
	public static PaymentDetail getPaymentDetail(SSRData ssrData) {
		PaymentDetail paymentDetail = null;
		SSROSAGDTO ssrOSAGDTO = ssrData.getOSAGSSRDTO();

		if (ssrOSAGDTO != null) {
			BSPDetail bspDetail = new BSPDetail();

			bspDetail.setIATACode(ssrOSAGDTO.getIATACode());
			paymentDetail = bspDetail;
		} else {
			SSRCreditCardDetailDTO ssrCreditCardDetailDTO = ssrData.getCreditCardDetailSSRDTO();

			if (ssrCreditCardDetailDTO != null) {

				CreditCardDetail cardDetail = new CreditCardDetail();
				CardCompanyCode ccCode = CardCompanyCode.getValue(ssrCreditCardDetailDTO.getCardType());

				cardDetail.setCardNo(ssrCreditCardDetailDTO.getCardNo());
				cardDetail.setCardType(ccCode.getCode());
				cardDetail.setExpiryDate(ssrCreditCardDetailDTO.getExpiryYear() + ssrCreditCardDetailDTO.getExpiryMonth());
				cardDetail.setHolderName(ssrCreditCardDetailDTO.getHolderName());
				cardDetail.setSecureCode(ssrCreditCardDetailDTO.getPinNumber());

				paymentDetail = cardDetail;
			}
		}

		return paymentDetail;
	}

	public static Date getPaymentTimeLimit(SSRData ssrData) {
		Date date = null;
		if (ssrData.getTicketingTimeLimit() != null) {
			date = ssrData.getTicketingTimeLimit().getTicketingTime();
		}
		return date;
	}
}
