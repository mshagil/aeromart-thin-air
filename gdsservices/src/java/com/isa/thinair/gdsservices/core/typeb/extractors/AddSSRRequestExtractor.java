package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class AddSSRRequestExtractor extends ValidatorBase {
	private static final Log log = LogFactory.getLog(AddSSRRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// There aren't any AddSSRRequestExtractor specific validations.
			// The General Action codes are validated at the high level.
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	/**
	 * returns AddSsrRequest
	 * 
	 * @param gdsCredentialsDTO
	 * @param ssrData
	 * @param bookingRequestDTO
	 * @return
	 */
	public static AddSsrRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {
		AddSsrRequest addSSRRequest = new AddSsrRequest();

		addSSRRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		addSSRRequest = (AddSsrRequest) RequestExtractorUtil.addBaseAttributes(addSSRRequest, bookingRequestDTO);

		addSSRRequest = addPassengersAndSSRs(addSSRRequest, ssrData);
		addSSRRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return addSSRRequest;
	}

	/**
	 * adds Passengers and SSRs to the AddSsrRequest
	 * 
	 * @param addSSRRequest
	 * @param ssrData
	 * @return
	 */
	private static AddSsrRequest addPassengersAndSSRs(AddSsrRequest addSSRRequest, SSRData ssrData) {

		Map<String, Passenger> resPaxMap = new HashMap<String, Passenger>();
		Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();
		Collection<SSRDTO> ssrDTOs = ssrData.getSsrDTOs();

		// add SSRDetailDTOs to paxs
		for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
			SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();

			if (!(GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssrDetailDTO.getCodeSSR())
					|| GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode().equals(ssrDetailDTO.getCodeSSR()) || GDSExternalCodes.SSRCodes.EXTEND_TIMELIMIT
					.getCode().equals(ssrDetailDTO.getCodeSSR()))) {
				List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();
				String gdsActionCode = ssrDetailDTO.getActionOrStatusCode();

				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {
					// add SSRDetailDTOs to selected paxs
					if (nameDTOs != null && nameDTOs.size() > 0) {
						for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
							NameDTO nameDTO = (NameDTO) iterNameDTO.next();
							SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
							ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
							String iataPaxName = nameDTO.getIATAName();
							Passenger resPax = resPaxMap.get(iataPaxName);

							if (resPax == null) {
								resPax = new Passenger();
								resPax = RequestExtractorUtil.getPassenger(resPax, nameDTO);
								resPaxMap.put(iataPaxName, resPax);
							}

							resPax.getSsrCollection().add(ssr);
							addSSRRequest.getPassengers().add(resPax);
						}
					} else {
						// add to common SSRS
						SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
						ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
						addSSRRequest.getCommonSSRs().add(ssr);
					}
				}
			}
		}

		// add to common SSRS
		for (SSRDTO ssrDTO : ssrDTOs) {
			String gdsActionCode = ssrDTO.getActionOrStatusCode();

			if (gdsActionCode != null) {
				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {
					SpecialServiceRequest ssr = new SpecialServiceDetailRequest();

					ssr = SSRExtractorUtil.addBaseSSRAttributes(ssr, ssrDTO);
					addSSRRequest.getCommonSSRs().add(ssr);
				}
			}

		}

		return addSSRRequest;
	}
}
