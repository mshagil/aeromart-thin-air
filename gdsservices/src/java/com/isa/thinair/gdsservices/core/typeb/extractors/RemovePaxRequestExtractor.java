package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.RemovePaxRequest;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;

/**
 * @author Manoj Dhanushka
 */
public class RemovePaxRequestExtractor extends ValidatorBase {

	public static RemovePaxRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {
		RemovePaxRequest removePaxRequest = new RemovePaxRequest();
		removePaxRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		removePaxRequest = (RemovePaxRequest) RequestExtractorUtil.addBaseAttributes(removePaxRequest, bookingRequestDTO);

		removePaxRequest = setPassengers(removePaxRequest, bookingRequestDTO);
		removePaxRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());
		return removePaxRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		return null;
	}

	/**
	 * sets passengers to split
	 * 
	 * @param splitResRequest
	 * @param bookingRequestDTO
	 * @return
	 */
	private static RemovePaxRequest setPassengers(RemovePaxRequest removePaxRequest, BookingRequestDTO bookingRequestDTO) {

		Collection<Passenger> passengers = new ArrayList<Passenger>();
		List<NameDTO> namesDTOs = bookingRequestDTO.getNewNameDTOs();

		if (bookingRequestDTO.getUnChangedNameDTOs() != null && bookingRequestDTO.getUnChangedNameDTOs().size() > 0) {
			for (Iterator<NameDTO> iterator = namesDTOs.iterator(); iterator.hasNext();) {
				NameDTO nameDTO = (NameDTO) iterator.next();
				Passenger passenger = RequestExtractorUtil.getPassenger(new Passenger(), nameDTO);
	
				passengers.add(passenger);
			}
		}
		removePaxRequest.setPassengers(passengers);

		return removePaxRequest;
	}

}
