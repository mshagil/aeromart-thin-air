package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddInfantRequest;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.AddSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CancelSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ChangeContactDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ChangePaxDetailsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CheckOldSegmentsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ConfirmBookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ExchangeEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ExtendTimelimitRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.IssueEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.MakePaymentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.gdsservices.api.dto.internal.RemovePaxRequest;
import com.isa.thinair.gdsservices.api.dto.internal.RemoveSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;

public class ModifyReservationRequestExtractor extends ValidatorBase {

	public static LinkedHashMap<ReservationAction, GDSReservationRequestBase> getMap(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {

		LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();

		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
		Collection<OtherServiceInfo> osis = OSIExtractorUtil.getOSIs(bookingRequestDTO);

		processCancelSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		processAddInfants(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);
		processNewSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);
		processOldSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);
		
		addSSRs(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);
		removeSSRs(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);
		updateEtickets(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);
		extendTimelimit(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);
		
		processRemovePax(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		processPaxDetails(gdsCredentialsDTO, bookingRequestDTO, requestMap);
		processExternalSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);
		processContactDetails(gdsCredentialsDTO, bookingRequestDTO, osis, requestMap);
		addPaymentDetails(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);

		return requestMap;
	}

	/**
	 * extracts SSRs to add
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void addSSRs(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO, SSRData ssrData,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {

		AddSsrRequest addSsrRequest = AddSSRRequestExtractor.getRequest(gdsCredentialsDTO, ssrData, bookingRequestDTO);

		if (addSsrRequest.getPassengers().size() != 0 || addSsrRequest.getCommonSSRs().size() != 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.ADD_SSR, addSsrRequest);
		}
	}
	
	/**
	 * extracts extend Timelimit info
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void extendTimelimit(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			SSRData ssrData, LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {

		ExtendTimelimitRequest extendTimelimitRequest = ExtendTimelimitRequestExtractor.getRequest(gdsCredentialsDTO, ssrData,
				bookingRequestDTO);

		if (extendTimelimitRequest.getTimeLimit() != null) {
			requestMap.put(GDSInternalCodes.ReservationAction.EXTEND_TIMELIMIT, extendTimelimitRequest);
		}
	}
	
	/**
	 * extracts SSRs to add
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void updateEtickets(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO, SSRData ssrData,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		GDSStatusTO gdsStatus = null;
		for (Entry<String, GDSStatusTO> entry : gdsStatusMap.entrySet()) {
			if (entry.getValue().getGdsCode().equals(bookingRequestDTO.getGds().getCode())) {
				gdsStatus = entry.getValue();
			}
		}
		if (gdsStatus == null || (gdsStatus != null && !gdsStatus.isAaValidatingCarrier())) {
			IssueEticketRequest issueEticketRequest = IssueEticketRequestExtractor.getRequest(gdsCredentialsDTO, ssrData,
					bookingRequestDTO);
			if (issueEticketRequest.getPassengers().size() != 0 || issueEticketRequest.getCommonSSRs().size() != 0) {
				
				ConfirmBookingRequest confirmBookingRequest = ConfirmBookingRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
				if (confirmBookingRequest != null) {
					requestMap.put(GDSInternalCodes.ReservationAction.CONFIRM_BOOKING, confirmBookingRequest);
				}				
				requestMap.put(GDSInternalCodes.ReservationAction.ISSUE_ETICKET, issueEticketRequest);
			} else {
				ExchangeEticketRequest exchangeEticketRequest = ExchangeEticketRequestExtractor.getRequest(gdsCredentialsDTO, ssrData,
						bookingRequestDTO);
				if (exchangeEticketRequest.getPassengers().size() != 0 || exchangeEticketRequest.getCommonSSRs().size() != 0) {
					
					ConfirmBookingRequest confirmBookingRequest = ConfirmBookingRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
					if (confirmBookingRequest != null) {
						requestMap.put(GDSInternalCodes.ReservationAction.CONFIRM_BOOKING, confirmBookingRequest);
					}
					requestMap.put(GDSInternalCodes.ReservationAction.EXCHANGE_ETICKET, exchangeEticketRequest);
				}				
			}
		}
	}

	/**
	 * extracts SSRs to remove
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void removeSSRs(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO, SSRData ssrData,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {

		RemoveSsrRequest removeSsrRequest = RemoveSSRRequestExtractor.getRequest(gdsCredentialsDTO, ssrData, bookingRequestDTO);

		if (removeSsrRequest.getPassengers().size() != 0 || removeSsrRequest.getCommonSSRs().size() != 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.REMOVE_SSR, removeSsrRequest);
		}
	}

	/**
	 * Process contact details
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processContactDetails(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			Collection<OtherServiceInfo> osis, LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {

		ChangeContactDetailRequest changeContactRequest = ChangeContactDetailRequestExtractor.getRequest(gdsCredentialsDTO,
				bookingRequestDTO, osis);

		if (changeContactRequest.getContactDetail() != null && !changeContactRequest.getContactDetail().isEmpty()) {
			requestMap.put(GDSInternalCodes.ReservationAction.CHANGE_CONTACT_DETAILS, changeContactRequest);
		}
	}

	/**
	 * Process external segments
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processExternalSegments(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		UpdateExternalSegmentsRequest updateExSegmentsRequest = UpdateExternalSegmentsRequestExtractor.getRequest(
				gdsCredentialsDTO, bookingRequestDTO);

		if (updateExSegmentsRequest.getExternalSegments() != null && updateExSegmentsRequest.getExternalSegments().size() > 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS, updateExSegmentsRequest);
		}
	}

	/**
	 * Process Passenger Details
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processPaxDetails(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		ChangePaxDetailsRequest changePaxRequest = ChangePaxDetailsRequestExtractor.getRequest(gdsCredentialsDTO,
				bookingRequestDTO);

		if ((changePaxRequest.getOldNewPassengerDetails() != null && changePaxRequest.getOldNewPassengerDetails().size() > 0)
				|| !changePaxRequest.getUserNote().equals("")) {
			requestMap.put(GDSInternalCodes.ReservationAction.CHANGE_PAX_DETAILS, changePaxRequest);
		}
	}

	/**
	 * Process Passenger Details
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processAddInfants(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			SSRData ssrData, LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {

		AddInfantRequest addInfantRequest = AddInfantRequestExtractor.getRequest(gdsCredentialsDTO, ssrData, bookingRequestDTO);

		if (addInfantRequest.getInfants() != null && addInfantRequest.getInfants().size() > 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.ADD_INFANT, addInfantRequest);
		}
	}

	/**
	 * Process Cancel Segments
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processCancelSegments(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		CancelSegmentRequest cancelSegmentRequest = CancelSegmentRequestExtractor
				.getRequest(gdsCredentialsDTO, bookingRequestDTO);

		if (cancelSegmentRequest.getSegments() != null && cancelSegmentRequest.getSegments().size() > 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT, cancelSegmentRequest);
		}
	}

	/**
	 * Process New Segments
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processNewSegments(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		AddSegmentRequest addSegmentRequest = AddSegmentRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);

		if (addSegmentRequest.getSegmentDetails() != null && addSegmentRequest.getSegmentDetails().size() > 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.ADD_SEGMENT, addSegmentRequest);
		}
		addBlockSeatDetails(addSegmentRequest, bookingRequestDTO.getBlockedSeats());
	}

	private static AddSegmentRequest addBlockSeatDetails(AddSegmentRequest addSegmentRequest,
			Collection<TempSegBcAllocDTO> blockedSeats) {
		if (blockedSeats != null && blockedSeats.size() > 0) {
			addSegmentRequest.setBlockedSeats(blockedSeats);
		}
		return addSegmentRequest;
	}

	/**
	 * Process Remove Pax
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processRemovePax(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		RemovePaxRequest removePaxRequest = RemovePaxRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);

		if (removePaxRequest.getPassengers() != null && removePaxRequest.getPassengers().size() > 0) {
			requestMap.clear();
			requestMap.put(GDSInternalCodes.ReservationAction.REMOVE_PAX, removePaxRequest);
		}
	}

	/**
	 * Process Existing Segments
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void processOldSegments(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		CheckOldSegmentsRequest checkOldSegmentsRequest = CheckOldSegmentRequestExtractor.getRequest(gdsCredentialsDTO,
				bookingRequestDTO);

		if (checkOldSegmentsRequest.getSegments() != null && checkOldSegmentsRequest.getSegments().size() > 0) {
			requestMap.put(GDSInternalCodes.ReservationAction.CHECK_OLD_SEGMENTS, checkOldSegmentsRequest);
		}
	}

	/**
	 * Identify the validators
	 * 
	 * @param colValidatorBase
	 * @param bookingRequestDTO
	 * @param gdsCredentialsDTO
	 */
	private void identifyTheValidators(Collection<ValidatorBase> colValidatorBase, BookingRequestDTO bookingRequestDTO,
			GDSCredentialsDTO gdsCredentialsDTO) {
		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
		LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();
		Collection<OtherServiceInfo> osis = OSIExtractorUtil.getOSIs(bookingRequestDTO);

		processContactDetails(gdsCredentialsDTO, bookingRequestDTO, osis, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.CHANGE_CONTACT_DETAILS) != null) {
			colValidatorBase.add(new ChangeContactDetailRequestExtractor());
		}

		processNewSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.ADD_SEGMENT) != null) {
			colValidatorBase.add(new AddSegmentRequestExtractor());
		}

		processCancelSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT) != null) {
			colValidatorBase.add(new CancelSegmentRequestExtractor());
		}

		processPaxDetails(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.CHANGE_PAX_DETAILS) != null) {
			colValidatorBase.add(new ChangePaxDetailsRequestExtractor());
		}

		processExternalSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS) != null) {
			colValidatorBase.add(new UpdateExternalSegmentsRequestExtractor());
		}

		addPaymentDetails(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.MAKE_PAYMENT) != null) {
			colValidatorBase.add(new MakePaymentRequestExtractor());
		}

		addSSRs(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.ADD_SSR) != null) {
			colValidatorBase.add(new AddSSRRequestExtractor());
		}

		removeSSRs(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.REMOVE_SSR) != null) {
			colValidatorBase.add(new RemoveSSRRequestExtractor());
		}
		
		updateEtickets(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.ISSUE_ETICKET) != null) {
			colValidatorBase.add(new IssueEticketRequestExtractor());
		}
		
		processAddInfants(gdsCredentialsDTO, bookingRequestDTO, ssrData, requestMap);

		if (requestMap.get(GDSInternalCodes.ReservationAction.ADD_INFANT) != null) {
			colValidatorBase.add(new AddInfantRequestExtractor());
		}
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		Collection<ValidatorBase> colValidatorBase = new ArrayList<ValidatorBase>();
		// colValidatorBase.add(new ChangeSSRExtractor());
		identifyTheValidators(colValidatorBase, bookingRequestDTO, gdsCredentialsDTO);
		RequestExtractorUtil.setResponderRecordLocator(bookingRequestDTO);

		for (ValidatorBase validatorBase : colValidatorBase) {
			bookingRequestDTO = validatorBase.validate(bookingRequestDTO, gdsCredentialsDTO);

			if (validatorBase.getStatus() == GDSInternalCodes.ValidateConstants.INVALID
					|| validatorBase.getStatus() == GDSInternalCodes.ValidateConstants.FAILURE) {
				this.setStatus(validatorBase.getStatus());
				return bookingRequestDTO;
			}
		}

		this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		return bookingRequestDTO;
	}

	/**
	 * adds payment details to the SSRs to the requestMap
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @param requestMap
	 */
	private static void addPaymentDetails(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			SSRData ssrData, LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap) {
		MakePaymentRequest makePaymentRequest = MakePaymentRequestExtractor.getRequest(gdsCredentialsDTO, ssrData,
				bookingRequestDTO);

		if (makePaymentRequest.getPaymentDetail() != null) {
			requestMap.put(ReservationAction.MAKE_PAYMENT, makePaymentRequest);
		}

	}

}
