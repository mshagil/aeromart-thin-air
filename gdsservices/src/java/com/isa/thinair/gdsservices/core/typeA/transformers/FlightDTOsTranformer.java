package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.typea.common.FlightInformation;

public class FlightDTOsTranformer {

	private List<List<FlightInformation>> onds;
	private String gdsCarrierCode;
	private String actionCode;

	public FlightPriceRQ getFlightPriceRQ() throws GdsTypeAException, ModuleException {
		Date departureDate;
		Date arrivalDate;
		FlightPriceRQ availRQ = new FlightPriceRQ();
		BookingClassBD bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();
		String gdsBookingClass;
		String bookingClass = null;
		int travellerCount = 0;
		FlightInformation flightInfo;
		FlightSegmentTO flightSegmentTO;

		TravelPreferencesTO travelPref = availRQ.getTravelPreferences();


		String gdsUser = null;
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		for(GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode() != null && gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				gdsUser = gdsStatusTO.getUserId();
				break;
			}
		}
		if (gdsUser == null) {
			throw new ModuleException("module.credential.error");
		}
		User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(gdsUser);
		if (user == null) {
			throw new ModuleException("module.credential.error");
		}


		// segs
		OriginDestinationInformationTO ondInfo;
		for (List<FlightInformation> ond : onds) {
			ondInfo = availRQ.addNewOriginDestinationInformation();
			ondInfo.setOrigin(ond.get(0).getDepartureLocation().getLocationCode());
			ondInfo.setDestination(ond.get(ond.size() - 1).getArrivalLocation().getLocationCode());

			departureDate = ond.get(0).getDepartureDateTime().toGregorianCalendar().getTime();
			// arrivalDate = flightInformation.getArrivalDateTime().toGregorianCalendar().getTime();
			ondInfo.setPreferredDate(CalendarUtil.getStartTimeOfDate(departureDate));
			ondInfo.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(departureDate));
			ondInfo.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(departureDate));
			// ondInfo.setArrivalDateTimeStart(CalendarUtil.getStartTimeOfDate(arrivalDate));
			// ondInfo.setArrivalDateTimeEnd(CalendarUtil.getStartTimeOfDate(arrivalDate));


			List<OriginDestinationOptionTO> ondOptions = new ArrayList<OriginDestinationOptionTO>();
			OriginDestinationOptionTO ondOption = new OriginDestinationOptionTO();
			ondOptions.add(ondOption);
			ondInfo.setOrignDestinationOptions(ondOptions);

			for (FlightInformation flightInformation : ond) {

				if (flightInformation.getActionCode().equals(actionCode)) {

					gdsBookingClass = flightInformation.getFlightPreference().getBookingClass();
					bookingClass = GDSServicesUtil.getBookingCode(gdsCarrierCode, gdsBookingClass);

					travellerCount = Integer.parseInt(flightInformation.getTravellerCount());

					flightSegmentTO = CommonUtil.getBestMatchedFlightSegmentTo(flightInformation);

					if (flightSegmentTO == null) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.FLIGHT_NOT_ACTIVE);
					}

					ondOption.getFlightSegmentList().add(flightSegmentTO);

				}
			}

			ondInfo.setPreferredClassOfService(bookingClassBD.getCabinClassForBookingClass(bookingClass));
			ondInfo.setPreferredBookingType(BookingClass.BookingClassType.NORMAL);
			ondInfo.setPreferredBookingClass(bookingClass);

		}

		if (travelPref.getBookingClassCode() == null) {
			travelPref.setBookingClassCode(bookingClass);
		}

		TravelerInfoSummaryTO travelerInfo = availRQ.getTravelerInfoSummary();
		PassengerTypeQuantityTO adultsQuantity = travelerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(travellerCount);

		AvailPreferencesTO availPref = availRQ.getAvailPreferences();
		availPref.setAppIndicator(ApplicationEngine.WS);
		availPref.setQuoteFares(true);
		availPref.setSearchSystem(ProxyConstants.SYSTEM.AA);
		availPref.setTravelAgentCode(user.getAgentCode());


		return availRQ;
	}

	public List<List<FlightInformation>> getOnds() {
		return onds;
	}

	public void setOnds(List<List<FlightInformation>> onds) {
		this.onds = onds;
	}

	public String getGdsCarrierCode() {
		return gdsCarrierCode;
	}

	public void setGdsCarrierCode(String gdsCarrierCode) {
		this.gdsCarrierCode = gdsCarrierCode;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
}
