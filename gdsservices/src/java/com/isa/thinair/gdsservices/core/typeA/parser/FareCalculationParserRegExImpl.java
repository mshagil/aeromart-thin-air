package com.isa.thinair.gdsservices.core.typeA.parser;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.isa.thinair.gdsservices.core.dto.FinancialInformation;

public class FareCalculationParserRegExImpl implements FareCalculationParser {

	private static final FareCalcRegExGrammar grammar;

	static {
		FareCalculationGrammar regExFactory = new FareCalculationGrammarRegExTreeImpl();
		grammar = (FareCalcRegExGrammar) regExFactory.getGrammar();
	}


	public FinancialInformation parseFinancialInformation(String fareInformationText) {
		FinancialInformation financialInformation = new FinancialInformation();
		Pattern pattern;
		Matcher matcher;
		String matchingText;

		FareCalcParserUtil parserUtil = new FareCalcParserUtil();

		pattern = Pattern.compile(grammar.getFareCalcNonDetailed());
		matcher = pattern.matcher(fareInformationText);

		int currentGroupNumber;

		if (matcher.find()) {
			currentGroupNumber = 2;

			matchingText = matcher.group(currentGroupNumber);
			financialInformation.setOrigin(parserUtil.parseLocation(matchingText));

			currentGroupNumber += 8;

			matchingText = matcher.group(currentGroupNumber);
			financialInformation.setSectors(parserUtil.parseSectors(matchingText));

			currentGroupNumber += 45;

			financialInformation.setCurrency(matcher.group(currentGroupNumber));

			currentGroupNumber += 3;

			financialInformation.setTotalAmount(new BigDecimal(matcher.group(currentGroupNumber)));

			currentGroupNumber += 4;

			if (matcher.group(currentGroupNumber) != null) {
				financialInformation.setRateOfExchange(new BigDecimal(matcher.group(currentGroupNumber + 2)));
			}

			currentGroupNumber += 4;

			if(matcher.group(currentGroupNumber) != null) {
                financialInformation.setPassengerFacilityCharges(parserUtil.parseTaxFeeCharge(matcher.group(currentGroupNumber + 6)));
            }

			currentGroupNumber += 8;

			if(matcher.group(currentGroupNumber) != null) {
                financialInformation.setUsDomesticTaxes(parserUtil.parseTaxFeeCharge(matcher.group(currentGroupNumber + 6)));
            }
		}

		financialInformation.populateSegmentOriginLocation();

		return financialInformation;
	}

	public String buildFareInformationText(FinancialInformation financialInformation) {
		StringBuilder fareInformationText = new StringBuilder();
        FareCalcGeneratorUtil generatorUtil = new FareCalcGeneratorUtil();
		GeneratorResponse tempResp;

		tempResp = generatorUtil.generateLocation(financialInformation.getOrigin(), FareCalculationConstants.CharType.UNKNOWN);
        fareInformationText.append(tempResp.getContent());

		tempResp = generatorUtil.generateSectors(financialInformation.getSectors(), tempResp.getCurrentCharType());
		fareInformationText.append(tempResp.getContent());

		fareInformationText
				.append(financialInformation.getCurrency())
				.append(financialInformation.getTotalAmount())
				.append(FareCalculationConstants.ElementIdentity.END_OF_FARE_CALC);

		tempResp = new GeneratorResponse();
		tempResp.setCurrentCharType(FareCalculationConstants.CharType.ALPHABETIC);

		if (financialInformation.getRateOfExchange() != null) {
			fareInformationText
					.append(generatorUtil.getElementSeparator(tempResp.getCurrentCharType(), FareCalculationConstants.CharType.ALPHABETIC))
					.append(FareCalculationConstants.ElementIdentity.RATE_OF_EXCHANGE)
					.append(financialInformation.getRateOfExchange().toPlainString());

			tempResp = new GeneratorResponse();
			tempResp.setCurrentCharType(FareCalculationConstants.CharType.NUMERIC);
		}

		if (financialInformation.getPassengerFacilityCharges() != null && !financialInformation.getPassengerFacilityCharges().isEmpty()) {
			fareInformationText
					.append(generatorUtil.getElementSeparator(tempResp.getCurrentCharType(), FareCalculationConstants.CharType.ALPHABETIC))
					.append(FareCalculationConstants.ElementIdentity.PASSENGER_FACILITY_CHARGE) ;

			tempResp = generatorUtil.generateTaxFeeCharge(financialInformation.getPassengerFacilityCharges());
			fareInformationText.append(tempResp.getContent());
		}
		if (financialInformation.getUsDomesticTaxes() != null && !financialInformation.getUsDomesticTaxes().isEmpty()) {
			fareInformationText
					.append(generatorUtil.getElementSeparator(tempResp.getCurrentCharType(), FareCalculationConstants.CharType.ALPHABETIC))
					.append(FareCalculationConstants.ElementIdentity.US_DOMESTIC_FLIGHT_SEGMENT_TAX);

			tempResp = generatorUtil.generateTaxFeeCharge(financialInformation.getUsDomesticTaxes());
			fareInformationText.append(tempResp.getContent());
		}

		return fareInformationText.toString();
	}

	public interface FareCalculationConstants {
		interface RegEx {
			String ALPHABETIC = "[A-Z]";
			String INTEGER = "\\d+";
			String DECIMAL = "\\d+(?:\\.\\d+)?";
			String AIRPORT_OR_CITY_CODE = "[A-Z]{3}";
			String CURRENCY_CODE = "[A-Z]{3}";
			String CARRIER_CODE_2_CHARS = "[A-Z0-9]{2}";
			String CARRIER_CODE_3_CHARS = "\\.[A-Z0-9]{3}";

			String SPACE = "( )";
			String SPACE_IF_ALPHABETIC_PRECEDE = "((?<=[A-Z])( )|(?<=[ 0-9/\\-])()|^)";
			String SPACE_IF_NUMERIC_PRECEDE = "((?<=[ A-Z/\\-])()|(?<=[0-9])( )|^)";
			String SPACE_IF_ALPHABETIC_SUCCEED= "($|( )(?=[A-Z])|()(?=[ 0-9/\\-]))";
			String SPACE_IF_NUMERIC_SUCCEED= "($|()(?=[ A-Z/\\-])|( )(?=[0-9]))";

			String ELEMENT_SEPARATOR = " ";
			String NO_ELEMENT_SEPARATOR = "";
		}

        interface ElementIdentity {
	        String INTERMEDIATE_TRANSFER_POINT = "X/";
	        String EXTRA_MILEAGE_ALLOWANCE = "E/";
	        String MAX_PERMITTED_MILEAGE_DEDUCTION = "L/";
	        String TICKETED_POINT_NOT_INCLUDED_IN_MILEAGE = "T/";

	        String STOP_OVER_TRANSFER_CHARGE = "S";
	        String SURCHARGE = "Q";

	        String COMMON_POINT_MIN_CHECK = "P R";
	        String HIGHER_CLASS_DIFFERENTIAL = "D";
	        String LOWEST_COMBINATION_PRINCIPLE = "C";
	        String MILEAGE_EQUALIZATION = "B";
	        String MINIMUMS = "P";
	        String MILEAGE_PRINCIPLE = "M";
	        String ONE_WAY_SUB_JOURNEY_CHECK = "H";
	        String RETURN_SUB_JOURNEY_CHECK = "U";

	        String SIDE_TRIP_START = "\\\\";
	        String SIDE_TRIP_END = "\\\\";

	        String END_OF_FARE_CALC = "END";
	        String RATE_OF_EXCHANGE = "ROE";

	        String PASSENGER_FACILITY_CHARGE = "XF ";
	        String US_DOMESTIC_FLIGHT_SEGMENT_TAX = "ZP ";

        }

		interface ElementPatterns {
			String INTERMEDIATE_TRANSFER_POINT = String.format("(X/)");
			String EXTRA_MILEAGE_ALLOWANCE = String.format("(E/)");
			String MAX_PERMITTED_MILEAGE_DEDUCTION = String.format("(L/)");
			String TICKETED_POINT_NOT_INCLUDED_IN_MILEAGE = String.format("(T/)");

			String STOP_OVER_TRANSFER_CHARGE = String.format("(S(%1s))", RegEx.DECIMAL);
			String SURCHARGE = String.format("(Q(%1s))", RegEx.DECIMAL);

			String COMMON_POINT_MIN_CHECK = String.format("(%1sP R((/%2s)+)%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String EXTRA_MILEAGE_ALLOWANCE_ANY = String.format("(%1sE/(XXX)%2s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String HIGHER_CLASS_DIFFERENTIAL = String.format("(%1sD ((%2s){2})%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String HIGHER_INTERMEDIATE_POINT = String.format("((%1s((%2s)/)?(%3s){2})+%4s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.ALPHABETIC, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String LOWEST_COMBINATION_PRINCIPLE = String.format("(%1sC/(%2s)%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String MILEAGE_EQUALIZATION = String.format("(%1sB/(%2s)%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String MINIMUMS = String.format("(%1sP(( (%2s){2})*)%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String MILEAGE_PRINCIPLE = String.format("((%1s|(%2s%3s))M%4s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.SPACE_IF_NUMERIC_PRECEDE, RegEx.INTEGER, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String ONE_WAY_SUB_JOURNEY_CHECK = String.format("(%1sH ((%2s){2})%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String RETURN_SUB_JOURNEY_CHECK = String.format("(%1sU ((%2s){2})%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String STOP_OVER_TRANSFER_CHARGE_SUMMARY = String.format("(((%1s(%2s))?)S%3s)", RegEx.SPACE_IF_NUMERIC_PRECEDE, RegEx.INTEGER, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String SURCHARGE_FARE = String.format("(%1sQ( (%2s){2})?%3s)", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);
			String FARE_BASIS_CODE = String.format("(%1s[A-Z0-9]{0,15} )", RegEx.SPACE_IF_ALPHABETIC_PRECEDE);

			String SIDE_TRIP_START = "(\\(|\\*)";
			String SIDE_TRIP_END = "(\\)|\\*)";
			String UNFLOWN_SECTOR = "((/)(-|/))";

			String END_OF_FARE_CALC = "END";
			String RATE_OF_EXCHANGE = String.format("( ROE(%1s))", RegEx.DECIMAL);

			String PASSENGER_FACILITY_CHARGE = String.format("(%1sXF( )?((%2s%3s)+))", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.DECIMAL);
			String US_DOMESTIC_FLIGHT_SEGMENT_TAX = String.format("(%1sZP( )?((%2s(%3s%4s|%5s))+))", RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegEx.AIRPORT_OR_CITY_CODE, RegEx.DECIMAL, RegEx.SPACE_IF_NUMERIC_SUCCEED, RegEx.SPACE_IF_ALPHABETIC_SUCCEED);

			String FARE_QUOTE_AMOUNT = String.format("(%1s%2s%3s)", RegEx.SPACE_IF_NUMERIC_PRECEDE, RegEx.DECIMAL, RegEx.SPACE_IF_NUMERIC_SUCCEED);
			String NON_DETAILED_CAPTURE = "(.*?)";
		}

		interface CharType {
			byte UNKNOWN = 0;
			byte ALPHABETIC = 1;
			byte NUMERIC = 2;
			byte SYMBOL = 3;
		}

	}


	private class FareCalcParserUtil {

		public FinancialInformation.Location parseLocation(String text) {
			FinancialInformation.Location location = new FinancialInformation.Location();

			String regEx = grammar.getAirportOrLocationDetailed();
			Pattern pattern = Pattern.compile(regEx);
			Matcher matcher = pattern.matcher(text);

			int currentGroupNumber;
			int tempGroupNumber;

			if (matcher.find()) {

				currentGroupNumber = 1;

				tempGroupNumber = currentGroupNumber + 6;
				location.setIntermediateTransfer(matcher.group(tempGroupNumber) != null);

				tempGroupNumber = currentGroupNumber + 8;
				location.setExtraMileage(matcher.group(tempGroupNumber) != null);

				tempGroupNumber = currentGroupNumber + 10;
				location.setMaxPermMileageDeduction(matcher.group(tempGroupNumber) != null);

				tempGroupNumber = currentGroupNumber + 12;
				location.setTicketedPointNotIncludedInMileage(matcher.group(tempGroupNumber) != null);

				tempGroupNumber = currentGroupNumber + 14;
				location.setAirportOrCityCode(matcher.group(tempGroupNumber));
			}

			return location;
		}

		public List<FinancialInformation.Sector> parseSectors(String text) {
			List<FinancialInformation.Sector> sectors = new ArrayList<FinancialInformation.Sector>();
			FinancialInformation.Sector sector;

			String regExJourney = grammar.getJourneySequenceDetailed();
			Pattern patternJourney = Pattern.compile(regExJourney);
			Matcher matcherJourney = patternJourney.matcher(text);
            int journeyEnd;

			String regExFareQuote = grammar.getFareQuoteGroupDetailed();
			Pattern patternFareQuote = Pattern.compile(regExFareQuote);
			Matcher matcherFareQuote = patternFareQuote.matcher(text);

			while (matcherJourney.find()) {
                journeyEnd = matcherJourney.end();
				if (matcherFareQuote.find(journeyEnd)) {
					sector = new FinancialInformation.Sector();

					if (matcherJourney.group(1) != null && matcherFareQuote.group(1) != null) {
						sector.setSegments(parseSegments(matcherJourney.group(1)));
						sector.setQuotedFares(parseQuotedFare(matcherFareQuote.group(1)));
					}

					sectors.add(sector);
				}
			}

			return sectors;
		}

		public List<FinancialInformation.Segment> parseSegments(String text) {
			List<FinancialInformation.Segment> segments = new ArrayList<FinancialInformation.Segment>();
		    FinancialInformation.Segment segment;
            FinancialInformation.Carrier carrier;
            FinancialInformation.Location location;

            String regExSegment = grammar.getJourneyUnitDetailed();
            Pattern patternSegment = Pattern.compile(regExSegment);
            Matcher matcherSegment = patternSegment.matcher(text);

			int currentGroupNumber;
			int tempGroupNumber;

            while (matcherSegment.find()) {

	            segment = new FinancialInformation.Segment();

	            currentGroupNumber = 2;

	            carrier = new FinancialInformation.Carrier();
                if (matcherSegment.group(currentGroupNumber) != null) {
                    if (matcherSegment.group(currentGroupNumber).equals(FinancialInformation.Constants.NON_FLOWN_FARE_INCLUDE)) {
                        carrier.setFlown(false);
                        carrier.setFareInclude(true);
                    } else if (matcherSegment.group(currentGroupNumber).equals(FinancialInformation.Constants.NON_FLOWN_FARE_NOT_INCLUDE)) {
                        carrier.setFlown(false);
                        carrier.setFareInclude(false);
                    }
                }

	            currentGroupNumber += 5;

	            if (matcherSegment.group(currentGroupNumber) != null) {
		            carrier.setFlown(true);
	            }

	            currentGroupNumber += 2;

	            if (matcherSegment.group(currentGroupNumber) != null) {
	                tempGroupNumber = currentGroupNumber + 2;
                    carrier.setStopoverTransferCharge(new BigDecimal(matcherSegment.group(tempGroupNumber)));
                }

	            currentGroupNumber += 3;

	            if (matcherSegment.group(currentGroupNumber) != null) {
		            tempGroupNumber = currentGroupNumber + 2;
		            carrier.setSurcharges(new BigDecimal(matcherSegment.group(tempGroupNumber)));
                }

	            currentGroupNumber += 4;

	            if (matcherSegment.group(currentGroupNumber) != null) {
                    carrier.setCarrierCode(matcherSegment.group(currentGroupNumber));
                }

	            currentGroupNumber += 3;

	            location = new FinancialInformation.Location();

	            tempGroupNumber = currentGroupNumber + 6;
	            location.setIntermediateTransfer(matcherSegment.group(tempGroupNumber) != null);

	            tempGroupNumber = currentGroupNumber + 8;
	            location.setExtraMileage(matcherSegment.group(tempGroupNumber) != null);

	            tempGroupNumber = currentGroupNumber + 10;
	            location.setMaxPermMileageDeduction(matcherSegment.group(tempGroupNumber) != null);

	            tempGroupNumber = currentGroupNumber + 12;
	            location.setTicketedPointNotIncludedInMileage(matcherSegment.group(tempGroupNumber) != null);

	            currentGroupNumber += 14;
	            location.setAirportOrCityCode(matcherSegment.group(currentGroupNumber));

                segment.setCarrier(carrier);
                segment.setDestination(location);

                segments.add(segment);
            }

			return segments;
		}

		private List<FinancialInformation.QuotedFare> parseQuotedFare(String text) {
			List<FinancialInformation.QuotedFare> quotedFares = new ArrayList<FinancialInformation.QuotedFare>();
			FinancialInformation.QuotedFare quotedFare;

			String regExFareQuote = grammar.getFareQuoteUnitDetailed();
			Pattern patternFareQuote = Pattern.compile(regExFareQuote);
			Matcher matcherFareQuote = patternFareQuote.matcher(text);

			int currentGroupNumber;
			int tempGroupNumber;

			boolean fareAmountFound = false;

			while (matcherFareQuote.find()) {

				currentGroupNumber = 0;
				quotedFare = new FinancialInformation.QuotedFare();

				currentGroupNumber += 4;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 4;
					quotedFare.setCommonPointMinChecksAirports(parseAirports(matcherFareQuote.group(tempGroupNumber)));
				}

				currentGroupNumber += 10;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 4;
					quotedFare.setExtraMileageAllowance
							(matcherFareQuote.group(tempGroupNumber).equals(FinancialInformation.Constants.EXTRA_MILEAGE_ALLOWANCE_ROUTING_POINT_ANY));
				}

				currentGroupNumber += 8;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 5;
					quotedFare.setHigherClassDiffAirportPair(parseAirportPairs(matcherFareQuote.group(tempGroupNumber)).get(0));
				}

				currentGroupNumber += 10;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber;
					quotedFare.setHigherIntermediatePointAirportPairs(parseAirportPairs(matcherFareQuote.group(tempGroupNumber)));
				}

				currentGroupNumber += 12;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 5;
					quotedFare.setLowestCombinationPrincipleAirport(matcherFareQuote.group(tempGroupNumber));
				}

				currentGroupNumber += 9;

				if (matcherFareQuote.group(54) != null) {
					tempGroupNumber = currentGroupNumber + 5;
					quotedFare.setMileageEqualizationAirport(matcherFareQuote.group(tempGroupNumber));
				}

				currentGroupNumber += 9;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 5;
					quotedFare.setMinimumsAirportPairs(parseAirportPairs(matcherFareQuote.group(tempGroupNumber)));
				}

				currentGroupNumber += 11;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 5;
					quotedFare.setOneWaySubJourneyAirportPair(parseAirportPairs(matcherFareQuote.group(tempGroupNumber)).get(0));
				}

				currentGroupNumber += 10;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 5;
					quotedFare.setReturnSubJourneyAirportPair(parseAirportPairs(matcherFareQuote.group(tempGroupNumber)).get(0));
				}

				currentGroupNumber += 10;


				if (matcherFareQuote.group(currentGroupNumber) != null) {
					quotedFare.setSurcharge(true);
					tempGroupNumber = currentGroupNumber + 5;
					if (matcherFareQuote.group(tempGroupNumber) != null) {
						quotedFare.setSurchargeAirportPair(parseAirportPairs(matcherFareQuote.group(tempGroupNumber)).get(0));
					}
				}

				currentGroupNumber += 10;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					quotedFare.setMileagePrinciple(true);
					tempGroupNumber = currentGroupNumber + 2;
					if (matcherFareQuote.group(tempGroupNumber) != null && !matcherFareQuote.group(tempGroupNumber).isEmpty()) {
						quotedFare.setMileagePrinciplePercentage(new Integer(matcherFareQuote.group(tempGroupNumber).trim()));
					}
				}

				currentGroupNumber += 13;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					tempGroupNumber = currentGroupNumber + 7;
					quotedFare.setAdditionalStopovers(new Integer(matcherFareQuote.group(tempGroupNumber)));
				}

				currentGroupNumber += 11;

				if (matcherFareQuote.group(currentGroupNumber) != null) {
					quotedFare.setAmount(new BigDecimal(matcherFareQuote.group(currentGroupNumber).trim()));
					fareAmountFound = true;
				}

				quotedFares.add(quotedFare);

				if (fareAmountFound) {
					break;
				}
			}

			return quotedFares;
		}

		public List<FinancialInformation.TaxFeeCharge> parseTaxFeeCharge(String text) {
            List<FinancialInformation.TaxFeeCharge> taxFeeCharges = new ArrayList<FinancialInformation.TaxFeeCharge>();
            FinancialInformation.TaxFeeCharge taxFeeCharge;

            Pattern p = Pattern.compile(String.format("((%1s)((%2s)%3s|%4s))", FareCalculationConstants.RegEx.AIRPORT_OR_CITY_CODE, FareCalculationConstants.RegEx.DECIMAL, FareCalculationConstants.RegEx.SPACE_IF_NUMERIC_SUCCEED, FareCalculationConstants.RegEx.SPACE_IF_ALPHABETIC_SUCCEED));
            Matcher m = p.matcher(text);

            while (m.find()) {
                taxFeeCharge = new FinancialInformation.TaxFeeCharge();
                taxFeeCharge.setAirportCode(m.group(2));

	            if (m.group(4) != null) {
		            taxFeeCharge.setAmount(new BigDecimal(m.group(4)));
	            }
                taxFeeCharges.add(taxFeeCharge);
            }

			return taxFeeCharges;
		}

        private List<FinancialInformation.AirportPair> parseAirportPairs(String text) {
            List<FinancialInformation.AirportPair> airportPairs = new ArrayList<FinancialInformation.AirportPair>();
            FinancialInformation.AirportPair airportPair;

            Pattern p = Pattern.compile(String.format("((%1s)/)?(%2s)(%3s)", FareCalculationConstants.RegEx.ALPHABETIC, FareCalculationConstants.RegEx.AIRPORT_OR_CITY_CODE, FareCalculationConstants.RegEx.AIRPORT_OR_CITY_CODE));
            Matcher m = p.matcher(text);

            while (m.find()) {
                airportPair = new FinancialInformation.AirportPair();
                airportPair.setClassOfService(m.group(2));
                airportPair.setAirport1(m.group(3));
                airportPair.setAirport2(m.group(4));
                airportPairs.add(airportPair);
            }

            return airportPairs;
        }

        private List<String> parseAirports(String text) {
            List<String> airports = new ArrayList<String>();

            Pattern p = Pattern.compile(String.format("(%1s)", FareCalculationConstants.RegEx.AIRPORT_OR_CITY_CODE));
            Matcher m = p.matcher(text);

            while (m.find()) {
                airports.add(m.group(1));
            }

            return airports;
        }
	}

	private class GeneratorResponse {
		private String content;
		private byte currentCharType;

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public byte getCurrentCharType() {
			return currentCharType;
		}

		public void setCurrentCharType(byte currentCharType) {
			this.currentCharType = currentCharType;
		}
	}

    private class FareCalcGeneratorUtil {
        public GeneratorResponse generateLocation(FinancialInformation.Location location, byte currentCharType) {
	        GeneratorResponse resp = new GeneratorResponse();
            String val = "";

	        val += getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC);

            if (location.isIntermediateTransfer()) {
                val += FareCalculationConstants.ElementIdentity.INTERMEDIATE_TRANSFER_POINT;
            }
            if (location.isExtraMileage()) {
                val += FareCalculationConstants.ElementIdentity.EXTRA_MILEAGE_ALLOWANCE;
            }
            if (location.isMaxPermMileageDeduction()) {
                val += FareCalculationConstants.ElementIdentity.MAX_PERMITTED_MILEAGE_DEDUCTION;
            }
            if (location.isTicketedPointNotIncludedInMileage()) {
                val += FareCalculationConstants.ElementIdentity.TICKETED_POINT_NOT_INCLUDED_IN_MILEAGE;
            }
            val += location.getAirportOrCityCode();

	        resp.setContent(val);
	        resp.setCurrentCharType(FareCalculationConstants.CharType.ALPHABETIC);

            return resp;
        }

        public GeneratorResponse generateCarrier(FinancialInformation.Carrier carrier, byte currentCharType) {
	        GeneratorResponse resp = new GeneratorResponse();
            String val = "";

            if (!carrier.getFlown()) {
                if (carrier.getFareInclude() != null) {
	                val += getElementSeparator(currentCharType, FareCalculationConstants.CharType.SYMBOL);
	                if (carrier.getFareInclude()) {
                        val += FinancialInformation.Constants.NON_FLOWN_FARE_INCLUDE;
                    } else {
                        val += FinancialInformation.Constants.NON_FLOWN_FARE_NOT_INCLUDE;
                    }
	                resp.setCurrentCharType(FareCalculationConstants.CharType.SYMBOL);
                } else {
                    // TODO -- exception
                }
            } else {
	            val += getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC);
	            if (carrier.getStopoverTransferCharge() != null) {
                    val += FareCalculationConstants.ElementIdentity.STOP_OVER_TRANSFER_CHARGE + carrier.getStopoverTransferCharge().toPlainString();
                }
                if (carrier.getSurcharges() != null) {
                    val += FareCalculationConstants.ElementIdentity.SURCHARGE + carrier.getSurcharges().toPlainString();
                }
                val += carrier.getCarrierCode();
	            resp.setCurrentCharType(FareCalculationConstants.CharType.ALPHABETIC);
            }

	        resp.setContent(val);

            return resp;
        }

        public GeneratorResponse generateSectors(List<FinancialInformation.Sector> sectors, byte currentCharType) {
	        GeneratorResponse resp = new GeneratorResponse();
	        StringBuilder sectorsContent = new StringBuilder();
	        GeneratorResponse tempResp;

	        for (FinancialInformation.Sector sector : sectors) {
		        tempResp = generateSegments(sector.getSegments(), currentCharType);
		        sectorsContent.append(tempResp.getContent());

		        tempResp = generateQuotedFare(sector.getQuotedFares(), tempResp.getCurrentCharType());
		        sectorsContent.append(tempResp.getContent());

		        currentCharType = tempResp.getCurrentCharType();
	        }

	        resp.setContent(sectorsContent.toString());
	        resp.setCurrentCharType(currentCharType);

	        return resp;
        }

        public GeneratorResponse generateSegments(List<FinancialInformation.Segment> segments, byte currentCharType) {
            GeneratorResponse resp = new GeneratorResponse();
	        StringBuilder segmentsContent = new StringBuilder();
	        GeneratorResponse tempResp;

	        for (FinancialInformation.Segment segment : segments) {
		        tempResp = generateCarrier(segment.getCarrier(), currentCharType);
		        segmentsContent.append(tempResp.getContent());

		        tempResp = generateLocation(segment.getDestination(), tempResp.getCurrentCharType());
		        segmentsContent.append(tempResp.getContent());

		        currentCharType = tempResp.getCurrentCharType();
	        }

	        resp.setContent(segmentsContent.toString());
	        resp.setCurrentCharType(currentCharType);

            return resp;
        }

        public GeneratorResponse generateQuotedFare(List<FinancialInformation.QuotedFare> quotedFares, byte currentCharType) {
	        GeneratorResponse resp = new GeneratorResponse();
	        StringBuilder fareQuoteContent = new StringBuilder();

	        for (FinancialInformation.QuotedFare quotedFare : quotedFares) {

		        if (quotedFare.getCommonPointMinChecksAirports() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
					        .append(FareCalculationConstants.ElementIdentity.COMMON_POINT_MIN_CHECK)
						    .append(generateAirports(quotedFare.getCommonPointMinChecksAirports()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getExtraMileageAllowance()) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
					        .append(FareCalculationConstants.ElementIdentity.EXTRA_MILEAGE_ALLOWANCE)
							.append(FinancialInformation.Constants.EXTRA_MILEAGE_ALLOWANCE_ROUTING_POINT_ANY);
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getHigherClassDiffAirportPair() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
			                .append(FareCalculationConstants.ElementIdentity.HIGHER_CLASS_DIFFERENTIAL)
					        .append(generateAirportPair(quotedFare.getHigherClassDiffAirportPair()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getLowestCombinationPrincipleAirport() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
			                .append(FareCalculationConstants.ElementIdentity.LOWEST_COMBINATION_PRINCIPLE)
					        .append(generateAirport(quotedFare.getLowestCombinationPrincipleAirport()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getMileageEqualizationAirport() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
					        .append(FareCalculationConstants.ElementIdentity.MILEAGE_EQUALIZATION)
					        .append(generateAirport(quotedFare.getMileageEqualizationAirport()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getMinimumsAirportPairs() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
					        .append(FareCalculationConstants.ElementIdentity.MINIMUMS)
					        .append(generateAirportPairs(quotedFare.getMinimumsAirportPairs()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getMileagePrinciple()) {
			        if (quotedFare.getMileagePrinciplePercentage() != null) {
				        fareQuoteContent
						        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.NUMERIC))
				                .append(quotedFare.getMileagePrinciplePercentage());
			        } else {
				        fareQuoteContent
						        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC));
			        }
			        fareQuoteContent.append(FareCalculationConstants.ElementIdentity.MILEAGE_PRINCIPLE);
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getOneWaySubJourneyAirportPair() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
			                .append(FareCalculationConstants.ElementIdentity.ONE_WAY_SUB_JOURNEY_CHECK)
					        .append(generateAirportPair(quotedFare.getOneWaySubJourneyAirportPair()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getReturnSubJourneyAirportPair() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
			                .append(FareCalculationConstants.ElementIdentity.RETURN_SUB_JOURNEY_CHECK)
					        .append(generateAirportPair(quotedFare.getReturnSubJourneyAirportPair()));
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getAdditionalStopovers() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.NUMERIC))
			                .append(quotedFare.getAdditionalStopovers().toString())
					        .append(FareCalculationConstants.ElementIdentity.STOP_OVER_TRANSFER_CHARGE);
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getSurcharge()) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
					        .append(FareCalculationConstants.ElementIdentity.SURCHARGE);
			        if (quotedFare.getSurchargeAirportPair() != null) {
				        fareQuoteContent.append(generateAirportPair(quotedFare.getSurchargeAirportPair()));
			        }
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getHigherIntermediatePointAirportPairs() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.ALPHABETIC))
							.append(generateAirportPairs(quotedFare.getHigherIntermediatePointAirportPairs()).trim());
			        currentCharType = FareCalculationConstants.CharType.ALPHABETIC;
		        }
		        if (quotedFare.getAmount() != null) {
			        fareQuoteContent
					        .append(getElementSeparator(currentCharType, FareCalculationConstants.CharType.NUMERIC))
					        .append(quotedFare.getAmount().toPlainString());
			        currentCharType = FareCalculationConstants.CharType.NUMERIC;
		        }
	        }

	        resp.setContent(fareQuoteContent.toString());
	        resp.setCurrentCharType(currentCharType);

	        return resp;
        }

        public GeneratorResponse generateTaxFeeCharge(List<FinancialInformation.TaxFeeCharge> taxFeeCharges) {
	        GeneratorResponse resp = new GeneratorResponse();
            String val = "";
            for (FinancialInformation.TaxFeeCharge taxFeeCharge : taxFeeCharges) {
                val += taxFeeCharge.getAirportCode();

	            if (taxFeeCharge.getAmount() != null) {
		            val += taxFeeCharge.getAmount().toPlainString();
		            resp.setCurrentCharType(FareCalculationConstants.CharType.NUMERIC);
	            } else {
		            val += " ";
		            resp.setCurrentCharType(FareCalculationConstants.CharType.ALPHABETIC);
	            }
            }

	        resp.setContent(val.trim());

            return resp;
        }

        public String generateAirportPair(FinancialInformation.AirportPair airportPair) {
	        String val = " ";
	        if (airportPair.getClassOfService() != null) {
		        val += airportPair.getClassOfService() + "/";
	        }
            return val + airportPair.getAirport1() + airportPair.getAirport2();
        }

        public String generateAirportPairs(List<FinancialInformation.AirportPair> airportPairs) {
            String val = "";
            for (FinancialInformation.AirportPair airportPair : airportPairs) {
                val += generateAirportPair(airportPair);
            }

            return val;
        }

	    public String generateAirport(String airport) {
		    return "/" + airport;
	    }

        public String generateAirports(List<String> airports) {
            String val = "";
            for (String airport : airports) {
                val += generateAirport(airport);
            }
            return val;
        }

	    public String getElementSeparator(byte currentCharType, byte nextCharType) {
		    return (currentCharType == nextCharType ? FareCalculationConstants.RegEx.ELEMENT_SEPARATOR :
				    FareCalculationConstants.RegEx.NO_ELEMENT_SEPARATOR);
	    }
    }
}
