package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.GDSPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIAddressDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIEmailDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIPhoneNoDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.AdviceCode;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.StatusCode;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.typeb.transformers.ResponseTransformerUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class ValidatorUtils {

	public static OSIDTO composeOTHSMessage(String comment) {
		OSIDTO osiDTO = new OSIDTO();
		osiDTO.setCodeOSI(GDSExternalCodes.OSICodes.OTHERS.getCode());
		osiDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		osiDTO.setOsiValue(comment);

		return osiDTO;
	}

	private static BookingRequestDTO updateWithAdviceCode(BookingRequestDTO bookingRequestDTO, String code) {
		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		String messageIdentifier = bookingRequestDTO.getMessageIdentifier();

		if (bookingSegmentDTOs != null && bookingSegmentDTOs.size() > 0) {
			for (BookingSegmentDTO bookingSegmentDTO : bookingSegmentDTOs) {
				if (bookingSegmentDTO.getActionOrStatusCode().equals(StatusCode.HOLDS_CONFIRMED.getCode())
						&& !messageIdentifier.equals(GDSExternalCodes.MessageIdentifier.NEW_BOOKING.getCode())) {
					bookingSegmentDTO.setAdviceOrStatusCode(StatusCode.HOLDS_CONFIRMED.getCode());
				} else 	if (bookingSegmentDTO.getActionOrStatusCode().equals(StatusCode.CODESHARE_CONFIRMED.getCode())
						&& !messageIdentifier.equals(GDSExternalCodes.MessageIdentifier.NEW_BOOKING.getCode())) {
					bookingSegmentDTO.setAdviceOrStatusCode(StatusCode.CODESHARE_CONFIRMED.getCode());
				} else {
					bookingSegmentDTO.setAdviceOrStatusCode(code);
				}

			}
		}

		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			for (SSRDTO ssrdto : ssrDTOs) {
				ssrdto.setAdviceOrStatusCode(code);
			}
		}

		bookingRequestDTO.setNoAdviceCodeExists(true);

		return bookingRequestDTO;
	}

	public static String getSupportedActionOrStatus(String code) {
		GDSExternalCodes.ActionCode actionCode = GDSExternalCodes.ActionCode.getValue(code);

		if (actionCode != null) {
			// These Action codes are not currently supported
			if (actionCode == GDSExternalCodes.ActionCode.NEED_ALTERNATE || actionCode == GDSExternalCodes.ActionCode.WAITLIST
					|| actionCode == GDSExternalCodes.ActionCode.CANCEL_WAITLISTED) {
				return null;
			} else {
				return actionCode.getCode();
			}
		} else {
			GDSExternalCodes.StatusCode statusCode = GDSExternalCodes.StatusCode.getValue(code);

			if (statusCode != null) {
				return statusCode.getCode();
			} else {
				return null;
			}
		}
	}
	
	public static String getSupportedAdviceCode(String code) {
		GDSExternalCodes.AdviceCode adviceCode = GDSExternalCodes.AdviceCode.getValue(code);

		if (adviceCode != null) {
			if (adviceCode == GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED
					|| adviceCode == GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED) {
				return adviceCode.getCode();
			} else {
				return null;
			}
		}
		return null;
	}


	public static String getPNRReference(RecordLocatorDTO recordLocatorDTO) {
		String pnrReference = "";

		if (recordLocatorDTO != null) {
			pnrReference = GDSApiUtils.maskNull(recordLocatorDTO.getPnrReference());
		}

		return pnrReference;
	}

	public static boolean isContactDetailsExists(List<OSIDTO> osiDTOs, GDSExternalCodes.GDS gds) {
		if (osiDTOs != null && osiDTOs.size() > 0) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			for (Iterator<OSIDTO> iterator = osiDTOs.iterator(); iterator.hasNext();) {
				OSIDTO osiDTO = (OSIDTO) iterator.next();
				//This is client specific change requested by Amadeus
				if (gds.equals(GDSExternalCodes.GDS.AMADEUS) && osiDTO.getCarrierCode().equals("YY")) {
					osiDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				}
				if (carrierCode.equals(GDSApiUtils.maskNull(osiDTO.getCarrierCode())) && osiDTO instanceof OSIPhoneNoDTO) {
					OSIPhoneNoDTO osiPhoneNoDTO = (OSIPhoneNoDTO) osiDTO;
					if (GDSApiUtils.maskNull(osiPhoneNoDTO.getPhoneNo()).length() > 0) {
						return true;
					}
				} else if (carrierCode.equals(GDSApiUtils.maskNull(osiDTO.getCarrierCode())) && osiDTO instanceof OSIEmailDTO) {
					OSIEmailDTO osiEmailDTO = (OSIEmailDTO) osiDTO;
					if (GDSApiUtils.maskNull(osiEmailDTO.getEmail()).length() > 0) {
						return true;
					}
				}  else if (carrierCode.equals(GDSApiUtils.maskNull(osiDTO.getCarrierCode())) && osiDTO instanceof OSIAddressDTO) {
					OSIAddressDTO osiAddressDTO = (OSIAddressDTO) osiDTO;
					if (GDSApiUtils.maskNull(osiAddressDTO.getAddressLineOne()).length() > 0
							|| GDSApiUtils.maskNull(osiAddressDTO.getAddressLineTwo()).length() > 0
							|| GDSApiUtils.maskNull(osiAddressDTO.getCity()).length() > 0) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public static BookingRequestDTO composeGeneralFailure(BookingRequestDTO bookingRequestDTO, String message) {
		bookingRequestDTO.setResponseMessage(message);
		return bookingRequestDTO;
	}

	public static BookingRequestDTO composeNoActionTakenResponse(BookingRequestDTO bookingRequestDTO,
			GDSReservationRequestBase requestBase) {
		return composeNoActionTakenResponse(bookingRequestDTO, requestBase.getResponseMessage());
	}

	public static BookingRequestDTO composeNoActionTakenResponse(BookingRequestDTO bookingRequestDTO, String message) {
		bookingRequestDTO = addResponse(bookingRequestDTO, message);

		return updateWithAdviceCode(bookingRequestDTO, AdviceCode.NO_ACTION_TAKEN.getCode());
	}

	/**
	 * adds response message to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param requestBase
	 * @return
	 */
	public static BookingRequestDTO addResponse(BookingRequestDTO bookingRequestDTO, GDSReservationRequestBase requestBase) {

		return addResponse(bookingRequestDTO, requestBase.getResponseMessage());
	}

	/**
	 * adds response message to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param message
	 * @return
	 */
	public static BookingRequestDTO addResponse(BookingRequestDTO bookingRequestDTO, String responseMessage) {
		responseMessage = GDSApiUtils.maskNull(responseMessage);
		boolean isMessageAlreadyExists = false;

		for (SSRDTO ssr : bookingRequestDTO.getSsrDTOs()) {
			if (ssr instanceof SSROTHERSDTO && ssr.getSsrValue() != null && !ssr.getSsrValue().equals("")) {
				String existingMessage = ssr.getSsrValue();
				if (existingMessage.toLowerCase().contains(responseMessage.toLowerCase())) {
					isMessageAlreadyExists = true;
				}
			}
		}

		if (responseMessage.length() > 0 && !isMessageAlreadyExists) {
			if (responseMessage.split(":")[0].equals("SSRADTK")) {
				bookingRequestDTO.getSsrDTOs().add(ResponseTransformerUtil.getResponseSSRADTK(responseMessage.split(":")[1]));
			} else {
				bookingRequestDTO.getSsrDTOs().add(ResponseTransformerUtil.getResponseSSR(responseMessage));
			}
		}

		return bookingRequestDTO;
	}

	private static void populateCountryWisePAxConfig(List contactConfigList,
			Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs, String paxType, Map<String, Boolean> countryPaxConfigAD,
			Map<String, Boolean> countryPaxConfigCH, Map<String, Boolean> countryPaxConfigIN) {
		for (List<PaxCountryConfigDTO> paxCountryConfigs : countryWiseConfigs.values()) {
			for (PaxCountryConfigDTO paxCountryConfig : paxCountryConfigs) {
				String field = paxCountryConfig.getFieldName();
				if (ReservationInternalConstants.PassengerType.ADULT.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigAD);
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigCH);
				} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigIN);
				}
			}
		}
	}

	private static void fillPaxCountryConfigMap(PaxCountryConfigDTO paxCountryConfig, Map<String, Boolean> countryPaxConfig) {
		String fieldName = paxCountryConfig.getFieldName();
		if (countryPaxConfig.get(fieldName) == null) {
			countryPaxConfig.put(fieldName, paxCountryConfig.isMandatory());
		} else {
			Boolean currentValue = countryPaxConfig.get(fieldName);
			countryPaxConfig.put(fieldName, (currentValue || paxCountryConfig.isMandatory()));
		}
	}

	public static String validatePaxDetails(BookingRequestDTO bookingRequestDTO, boolean isOverrideCountryConfig,
			Collection passengers) {
		boolean globalValue;
		boolean passportNoEmpty = false;
		boolean visaNoEmpty = false;
		boolean visaDetailsEmpty = false;
		String message = "";
		String paxType = "";
		String passportNumber = "";
		String visaDocNumber = "";
		Date visaIssueDate = null;
		String visaIssuePlace = "";
		String visaApplicableCountry = "";
		Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs = null;
		Map<String, Boolean> countryPaxConfigAD = new HashMap<String, Boolean>();
		Map<String, Boolean> countryPaxConfigCH = new HashMap<String, Boolean>();
		Map<String, Boolean> countryPaxConfigIN = new HashMap<String, Boolean>();
		List<String> airportCodes = new ArrayList<String>();
		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		if (!isOverrideCountryConfig) {
			for (BookingSegmentDTO segmentDTO : segmentDTOs) {
				airportCodes.add(segmentDTO.getDepartureStation());
				airportCodes.add(segmentDTO.getDestinationStation());
			}
		}

		List paxConfigList = GDSServicesModuleUtil.getGlobalConfig().getpaxConfig(AppIndicatorEnum.APP_GDS.toString());
		if (airportCodes != null && airportCodes.size() > 0) {
			try {
				countryWiseConfigs = GDSServicesModuleUtil.getAirproxyPassengerBD().loadPaxCountryConfig(SYSTEM.AA.toString(),
						AppIndicatorEnum.APP_GDS.toString(), airportCodes, null);

			} catch (ModuleException e) {
				e.printStackTrace();
			}
			if (countryWiseConfigs != null && countryWiseConfigs.size() > 0) {
				populateCountryWisePAxConfig(paxConfigList, countryWiseConfigs, paxType, countryPaxConfigAD, countryPaxConfigCH,
						countryPaxConfigIN);
			}
		}

		for (Passenger pax : (Collection<Passenger>) passengers) {

			passportNumber = pax.getDocNumber();
			visaDocNumber = pax.getVisaDocNumber();
			visaIssueDate = pax.getVisaDocIssueDate();
			visaIssuePlace = pax.getVisaDocPlaceOfIssue();
			visaApplicableCountry = pax.getVisaApplicableCountry();
			
			for (GDSPaxConfigDTO gdsConfig : (List<GDSPaxConfigDTO>) paxConfigList) {
				paxType = pax.getPaxType();
				globalValue = false;
				if (gdsConfig.getPaxTypeCode().equals(paxType)) {
					if (paxType.equals(ReservationInternalConstants.PassengerType.ADULT)
							&& countryPaxConfigAD.get(gdsConfig.getFieldName()) != null) {
						globalValue = countryPaxConfigAD.get(gdsConfig.getFieldName());
					}
				}
				if (paxType.equals(ReservationInternalConstants.PassengerType.CHILD)
						&& countryPaxConfigCH.get(gdsConfig.getFieldName()) != null) {
					globalValue = countryPaxConfigCH.get(gdsConfig.getFieldName());
				}
				if (paxType.equals(ReservationInternalConstants.PassengerType.INFANT)
						&& countryPaxConfigIN.get(gdsConfig.getFieldName()) != null) {
					globalValue = countryPaxConfigIN.get(gdsConfig.getFieldName());
				}

				if (globalValue || gdsConfig.isGdsMandatory()) {
					if (gdsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)
							&& (passportNumber == null || passportNumber.equals(""))) {
						passportNoEmpty = true;
						message = MessageUtil.getMessage("gdsservices.extractors.emptyPassportNumber");
					}
				}
				
				if ((globalValue || gdsConfig.isGdsMandatory()) && (gdsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_NUMBER)
						&& (visaDocNumber == null || visaDocNumber.equals("")))) {
					visaNoEmpty = true;
					message = MessageUtil.getMessage("gdsservices.extractors.emptyVisaDocNumber");
				}
				
				if ((globalValue || gdsConfig.isGdsMandatory()) && (gdsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_APPLICABLE_COUNTRY)
						&& (visaApplicableCountry == null || visaApplicableCountry.equals("")))) {
					visaDetailsEmpty = true;
					message = MessageUtil.getMessage("gdsservices.extractors.emptyVisaApplicableCountry");
				}
				
				if ((globalValue || gdsConfig.isGdsMandatory()) && (gdsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_ISSUE_DATE)
						&& (visaIssueDate == null))) {
					visaDetailsEmpty = true;
					message = MessageUtil.getMessage("gdsservices.extractors.emptyVisaIssueDate");
				}
				
				if ((globalValue || gdsConfig.isGdsMandatory()) && (gdsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_PLACE_OF_ISSUE)
						&& (visaIssuePlace == null || visaIssuePlace.equals("")))) {
					visaDetailsEmpty = true;
					message = MessageUtil.getMessage("gdsservices.extractors.emptyVisaIssuePlace");
				}
			}
		}
		
		if(passportNoEmpty && visaNoEmpty){
			message = MessageUtil.getMessage("gdsservices.extractors.emptyPassportAndVisaDocNumber");
		}else if(passportNoEmpty && visaDetailsEmpty){
			message = MessageUtil.getMessage("gdsservices.extractors.emptyPassportAndVisaDetails");
		}
		
		return message;
	}
}
