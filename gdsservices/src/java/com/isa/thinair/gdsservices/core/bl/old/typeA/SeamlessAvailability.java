package com.isa.thinair.gdsservices.core.bl.old.typeA;

import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.common.GDSTypeATransactionStatusUtils;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="seamlessAvailability.old"
 * @author zsaimeh
 */
public class SeamlessAvailability extends DefaultBaseCommand {

	@Override
	public ServiceResponce execute() {

		// Getting command params
		Map<String, List<CabinClassAvailabilityDTO>> odiCcdtoMap = (Map<String, List<CabinClassAvailabilityDTO>>) this
				.getParameter(TypeACommandParamNames.CABIN_CLASS_AVAILABILITY_DTO_LIST);

		String gdsTnxRef = (String) this.getParameter(TypeACommandParamNames.COMMON_ACCESS_REFERENCE);
		GDSTypeATransactionStatusUtils.createOrUpdateTransactionStage(gdsTnxRef, IATAOperation.PAOREQ.name());

		ForceLoginInvoker.login("SYSTEM", "G91saps23#");
		FlightInventoryBD flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		for (List<CabinClassAvailabilityDTO> cabinClassAvailabilityDTOList : odiCcdtoMap.values()) {
			for (CabinClassAvailabilityDTO cabinClassAvailabilityDTO : cabinClassAvailabilityDTOList) {
				cabinClassAvailabilityDTO.setCabinClassAvailability(flightInventoryBD
						.getCabinClassAvailability(cabinClassAvailabilityDTO));
			}
		}

		// Constructing and return command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(TypeACommandParamNames.CABIN_CLASS_AVAILABILITY_DTO_LIST, odiCcdtoMap);
		return response;
	}
}