package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.LinkedHashMap;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateStatusRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class UpdateStatusResponseTransformer {
	private static final ReservationAction RESERVATION_ACTION = ReservationAction.UPDATE_STATUS;

	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {
		UpdateStatusRequest updateStatusRequest = (UpdateStatusRequest) reservationResponseMap.get(RESERVATION_ACTION);
		String responseMessage = GDSApiUtils.maskNull(updateStatusRequest.getResponseMessage());

		if (updateStatusRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_IGNORED);

			if (responseMessage.length() > 0) {
				bookingRequestDTO.getSsrDTOs().add(ResponseTransformerUtil.getResponseSSR(responseMessage));
			}
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO,
					updateStatusRequest.getResponseMessage());
		}

		return bookingRequestDTO;
	}

}
