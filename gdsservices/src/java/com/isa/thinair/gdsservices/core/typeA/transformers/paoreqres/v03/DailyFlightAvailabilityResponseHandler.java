package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import iata.typea.v031.paores.ERC;
import iata.typea.v031.paores.GROUP1;
import iata.typea.v031.paores.GROUP2;
import iata.typea.v031.paores.GROUP3;
import iata.typea.v031.paores.GROUP4;
import iata.typea.v031.paores.IATA;
import iata.typea.v031.paores.IFT;
import iata.typea.v031.paores.ObjectFactory;
import iata.typea.v031.paores.PAORES;
import iata.typea.v031.paores.PDI;
import iata.typea.v031.paores.TVL;
import iata.typea.v031.paores.UNB;
import iata.typea.v031.paores.UNH;
import iata.typea.v031.paores.UNT;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDto;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.util.RandomANGen;

public class DailyFlightAvailabilityResponseHandler {

	public static JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws InteractiveEDIException {

		PaoresFactory factory = PaoresFactory.getInstance();
		ObjectFactory objectFactory = factory.getObjectFactory();

		IATA iata = objectFactory.createIATA();

		InterchangeHeader iHeader = (InterchangeHeader) response.getResponseParam(TypeACommandParamNames.IC_HEADER);
		MessageHeader mHeader = (MessageHeader) response.getResponseParam(TypeACommandParamNames.MESSAGE_HEADER);
		String messageFuc = (String) response.getResponseParam(TypeACommandParamNames.MESSAGE_FUNCTION);
		List<TVLDto> tvlDtos = (List) response.getResponseParam(TypeACommandParamNames.TVL_DTOS);
		Set<ERCDetail> errorDetails = (Set) response.getResponseParam(TypeACommandParamNames.APPLICATION_ERROR);

		// unb
		String interchangeControllRef = new RandomANGen(10).nextString("0001");
		UNB unb = factory.createUNB(iHeader, interchangeControllRef);

		PAORES paores = objectFactory.createPAORES();

		// unh
		UNH unh = factory.createUNH(mHeader);
		paores.setUNH(unh);

		// msg
		paores.setMSG(factory.createMSG(messageFuc));

		// Group 1 for error applicable for all the tvl segments
		GROUP1 group1 = objectFactory.createGROUP1();
		List<Pair<ERC, IFT>> ercList = factory.createERCIFT(errorDetails);
		for (Pair<ERC, IFT> pair : ercList) {
			group1.setERC(pair.getLeft());
			group1.setIFT(pair.getRight());
		}
		paores.getGROUP1().add(group1);

		// Group 2
		GROUP2 group2 = objectFactory.createGROUP2();

		// adding empty ODI to complient with branching
		group2.setODI(factory.createODI());

		for (TVLDto tvlDto : tvlDtos) {
			GROUP3 group3 = objectFactory.createGROUP3();

			AvailableFlightDTO availableFlightDTO = tvlDto.getAvailabilityResult();

			if (!tvlDto.isErrorSegment()) {
				// handle direct flights
				Collection<AvailableIBOBFlightSegment> availableFlightSegments = availableFlightDTO
						.getAvailableOndFlights(OndSequence.OUT_BOUND);
				FlightInventoryBD flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();
				if (availableFlightSegments != null) {
					for (AvailableIBOBFlightSegment availableFlight : availableFlightSegments) {
						if (availableFlight.getSelectedLogicalCabinClass() != null && availableFlight.isDirectFlight()) {
							FlightSegmentDTO segmDto = ((AvailableFlightSegment) availableFlight).getFlightSegmentDTO();
							FlightSegWithCCDTO dto = flightInventoryBD.getFlightSegmentsWithCabinClass(segmDto.getSegmentId());

							String carrierCode = dto.getFlightNumber().substring(0, 2);
							String flightNumber = dto.getFlightNumber().substring(2);
							String from = dto.getOrigin();
							String to = dto.getDestination();
							Date depDate = dto.getDepartureDate();
							Date arrivalDate = dto.getArrivalDate();

							TVL tvl = factory.createTVL(depDate, arrivalDate, from, to, carrierCode, flightNumber);
							group3.setTVL(tvl);

							Map<String, String> cabinMap = new HashMap<String, String>();
							for (String cabinClass : dto.getCabinClassAvailability()) {
								cabinMap.put(cabinClass.split("-")[0], "A");
							}
							// pdi
							PDI pdi = factory.createPDI(cabinMap);
							group3.setPDI(pdi);
							group2.getGROUP3().add(group3);
						}
					}
				}
			} else {

				TVLDetail tvlDetail = tvlDto.getTvlDetail();
				TVL tvl = factory.createTVL(tvlDetail.getDepartureTimeStart(), tvlDetail.getArrivalTime(), tvlDetail.getFrom(),
						tvlDetail.getTo(), tvlDetail.getCarrierCode(), tvlDetail.getFlightNumber());
				group3.setTVL(tvl);

				// Group 4 adding ERC specific for TVL
				Set<ERCDetail> ercDetails = tvlDto.getErcDetails();
				List<Pair<ERC, IFT>> ercListL4 = factory.createERCIFT(ercDetails);
				GROUP4 group4 = objectFactory.createGROUP4();
				for (Pair<ERC, IFT> pair : ercListL4) {
					group4.setERC(pair.getLeft());
					group4.setIFT(pair.getRight());
				}
				group3.getGROUP4().add(group4);
			}

			paores.getGROUP2().add(group2);

			// unt
			UNT unt = factory.createUNT(mHeader);
			paores.setUNT(unt);

		}

		iata.getUNBAndUNGAndPAORES().add(unb);
		iata.getUNBAndUNGAndPAORES().add(paores);
		return objectFactory.createIATA(iata);
	}
}
