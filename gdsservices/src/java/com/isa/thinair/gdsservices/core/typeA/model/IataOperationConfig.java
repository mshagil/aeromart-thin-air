package com.isa.thinair.gdsservices.core.typeA.model;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATARequest;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATAResponse;
import com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03.Itareq;
import com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03.Itares;
import com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03.Paoreq;
import com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03.Paores;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03.Tkcreq;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03.Tkcres;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcuac.v03.Tkcuac;
import com.isa.thinair.gdsservices.core.util.GDSConstants;

public class IataOperationConfig {

	public static String getMessageHandlerCommand(IATAOperation iataOperation) {

		switch (iataOperation) {
		case ITAREQ:
			return Constants.CommandNames.INVENTORY_ADJUSTMENT;
		case HWPREQ:
			return Constants.CommandNames.HYBRID_WRAP_UP;
		case CLTREQ:
			return Constants.CommandNames.CLEAR_TERMINATE_TRANSACTION;
		case TKTREQ:
			return Constants.CommandNames.COMMON_TICKET_HANDLER;
		case TKCUAC:
			return Constants.CommandNames.UNSOLICITED_TICKET_CONTROL_HANDLER;
		case TKCREQ:
			return Constants.CommandNames.COMMON_TICKET_CONTROL_HANDLER;
		}

		throw new UnsupportedOperationException();
	}

	public static String getMessageInitiatorCommand(IATAOperation iataOperation) {
		switch (iataOperation) {
			case TKTREQ:
				return Constants.CommandNames.COMMON_TICKET_INITIATOR;
			case TKCREQ:
				return Constants.CommandNames.COMMON_TICKET_CONTROL_INITIATOR;
		}

		throw new UnsupportedOperationException();
	}

	public static String getTransformationFile(IATAOperation iataOperation, GDSConstants.MessageTransformationDirection direction, String version) {

		return getTransformationLocation(version) + iataOperation.name() + "_" + version + "_" + direction.name() + ".xslt";
	}

	public static String getTransformationLocation(String version) {
		return "/resources/transform/" + version + "/";
	}

	@Deprecated
	public static IATARequest getRequestHandler(IATAOperation iataOperation, JAXBElement<?> jaxbRequest) {
		switch (iataOperation) {
		case PAOREQ:
			return new Paoreq(jaxbRequest);
		case ITAREQ:
			return new Itareq(jaxbRequest);
		case TKCUAC:
			return new Tkcuac(jaxbRequest);
		case TKCREQ:
			return new Tkcreq(jaxbRequest);
		default:
			throw new UnsupportedOperationException("request converter not available");
		}
	}

	@Deprecated
	public static IATAResponse getResponseHandler(IATAOperation iataOperation, IATARequest request) {
		switch (iataOperation) {
		case PAORES:
			return new Paores(request);
		case ITARES:
			return new Itares(request);
		case TKCRES:
			return new Tkcres(request);
		default:
			throw new UnsupportedOperationException("response converter not available");
		}
	}
}
