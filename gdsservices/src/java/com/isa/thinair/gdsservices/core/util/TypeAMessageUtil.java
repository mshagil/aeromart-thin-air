package com.isa.thinair.gdsservices.core.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.model.GDSTransactionStatus;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GdsApplicationError;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.dto.InventoryAdjustInfoTo;
import com.isa.thinair.gdsservices.core.exception.ErrorPath;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.typea.common.OriginDestinationInformation;

public abstract class TypeAMessageUtil {

	private static final String DATE_FORMAT_1 = "ddMMyyyyHHmm";

	public static boolean containsResponseKey(String messageReference) {
		return messageReference.contains(TypeAConstants.DELIM_INITIATOR_RESPONDER_KEY);
	}

	public static void saveMessagingSession(EDIMessageDTO messageDTO) {

		GDSTypeAServiceDAO typeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		String messageRef = messageDTO.getRequestMetaDataDTO().getMessageReference();

		GDSTransactionStatus transactionStatus = typeAServiceDAO.getGDSTransactionStatusFromRef(messageRef);

		transactionStatus.setTxnData(GDSApiUtils.toBlob(messageDTO.getMessagingSession()));

		typeAServiceDAO.gdsTypeAServiceSaveOrUpdate(transactionStatus);

	}

	public static void typeAMessagePostProcess(EDIMessageDTO ediMessageDTO, boolean isDisplayForETS) {
		String commonAccessRef = ediMessageDTO.getRequestMetaDataDTO().getMessageReference();
		if (commonAccessRef == null) {
			commonAccessRef = KeyGenerator.generateRespCommonAccessRefKey();
			ediMessageDTO.getRequestMetaDataDTO().setMessageReference(commonAccessRef);

		} else {
			if (!isDisplayForETS && !containsResponseKey(commonAccessRef)) {
				commonAccessRef += TypeAConstants.DELIM_INITIATOR_RESPONDER_KEY + KeyGenerator.generateRespCommonAccessRefKey();
				ediMessageDTO.getRequestMetaDataDTO().setMessageReference(commonAccessRef);
			}
		}

		loadMessagingSession(ediMessageDTO);
	}

	private static void loadMessagingSession(EDIMessageDTO messageDTO) {
		GDSTypeAServiceDAO typeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		String messageRef = messageDTO.getRequestMetaDataDTO().getMessageReference();
		Map<String, Object> messageSession;

		GDSTransactionStatus transactionStatus = typeAServiceDAO.getGDSTransactionStatusFromRef(messageRef);

		if (transactionStatus == null) {
			messageSession = new HashMap<String, Object>();

			transactionStatus = new GDSTransactionStatus();
			transactionStatus.setTnxStage(messageDTO.getRequestMetaDataDTO().getIataOperation().name());
			transactionStatus.setGdsTransactionRef(messageRef);
			transactionStatus.setTxnStartTime(new Date());
			transactionStatus.setTxnData(GDSApiUtils.toBlob(messageSession));

			typeAServiceDAO.gdsTypeAServiceSaveOrUpdate(transactionStatus);

			messageDTO.setMessagingSession(messageSession);
		} else {
			messageDTO.setMessagingSession((Map<String, Object>)GDSApiUtils.toObject(transactionStatus.getTxnData()));
		}

		typeAServiceDAO.gdsTypeAServiceLock(transactionStatus, false);

	}

	public static String updateInterchangeControlRef(String prevInterchangeControlRef, String sequenceNo)
			throws ModuleException {
		String commonAccessRef;
		if (prevInterchangeControlRef == null || prevInterchangeControlRef.length() <=
				TypeAConstants.SEQUENCE_NUMBER_FIXED_LENGTH) {
			throw new ModuleException("Interchange control reference key's length less than the minimum");
		}

		commonAccessRef = prevInterchangeControlRef.substring(0, prevInterchangeControlRef.length() -
				TypeAConstants.SEQUENCE_NUMBER_FIXED_LENGTH);

		return KeyGenerator.getInterchangeControlRef(commonAccessRef, sequenceNo);
	}

	public static String getReservationTempIdentifier(OriginDestinationInformation originDestinationInfo) {
		String travellersCount = null;

		InventoryAdjustInfoTo inventoryAdjustInfo = new InventoryAdjustInfoTo();
		inventoryAdjustInfo.setOrigin(originDestinationInfo.getDepartureAirportCityCode());
		inventoryAdjustInfo.setDestination(originDestinationInfo.getArrivalAirportCityCode());

		if (!originDestinationInfo.getFlightInfomation().isEmpty()) {
			originDestinationInfo.getFlightInfomation().iterator().next().getTravellerCount();

			if (travellersCount != null) {
				inventoryAdjustInfo.setPaxCount(Integer.parseInt(travellersCount));
			}
		}

		return inventoryAdjustInfo.getUniqueKey();
	}

	public static String mapToApplicationErrorCode(GdsApplicationError error) {
		String appErrorCode = TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR;

		switch (error) {
		case UNEQUAL_NUMBER_IN_PARTY:
			appErrorCode = TypeAConstants.ApplicationErrorCode.UNEQUAL_NUMBER_IN_PARTY;
			break;
		case SYSTEM_ERROR:
			appErrorCode = TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR;
			break;
		}

		return appErrorCode;
	}


	public static ErrorPath getErrorPath(IATAOperation iataOperation, String messageFunction, String errorCode) {
		ErrorPath errorPath = null;

		if (errorCode.equals(TypeAConstants.ApplicationErrorCode.NOT_AVAILABLE)
				|| errorCode.equals(TypeAConstants.ApplicationErrorCode.RES_BOOKING_DESIGNATOR_INVALID_MISSING)
				|| errorCode.equals(TypeAConstants.ApplicationErrorCode.FLIGHT_NOT_ACTIVE)
				|| errorCode.equals(TypeAConstants.ApplicationErrorCode.AIRLINE_DESIGNATOR_INVALID)) {
			errorPath = new ErrorPath(TypeAConstants.MessageElement.ROOT,
							new ErrorPath(TypeAConstants.MessageElement.ODI, new ErrorPath(TypeAConstants.MessageElement.TVL)));
		}

		if (errorPath == null) {
		    errorPath = new ErrorPath(TypeAConstants.MessageElement.ROOT);
		}

		return errorPath;
	}


	public static List<String> splitText(String text, int maxLength) {
		List<String> splitContent = new ArrayList<String>();
		Matcher m = Pattern.compile(String.format(".{1,%1d}", maxLength)).matcher(text);
		while (m.find()){
			splitContent.add(m.group(0));
		}
		return splitContent;
	}

}
