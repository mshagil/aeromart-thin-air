package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum.CS1225.FlightTimetableResponse;
import iata.typea.v031.paores.GROUP2;
import iata.typea.v031.paores.GROUP3;
import iata.typea.v031.paores.IATA;
import iata.typea.v031.paores.PAORES;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.jfree.util.Log;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAResponseCode;

public class FlightTimetableResponseHandler {

	public static JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws InteractiveEDIException {
		PaoresFactory factory = PaoresFactory.getInstance();
		PAORES paores = factory.getObjectFactory().createPAORES();
		paores.setUNH(factory.createUNH(null));
		paores.setMSG(factory.createMSG(FlightTimetableResponse.getValue()));

		GROUP2 group2 = factory.getObjectFactory().createGROUP2();
		group2.setODI(factory.createODI());

		// TODO remove me and replace wiht TVLDto
		List<AvailableFlightDTO> availableFlights = (List<AvailableFlightDTO>) response
				.getResponseParam(TypeAResponseCode.AVAILABILITY_SEARCH_RESULT);

		for (AvailableFlightDTO availableFlightDTO : availableFlights) {

			for (AvailableIBOBFlightSegment flightSegment : availableFlightDTO.getAvailableOndFlights(OndSequence.OUT_BOUND)) {
				if (flightSegment.isDirectFlight()) {
					updateGroup(factory, group2, (AvailableFlightSegment) flightSegment);
				} else {
					for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) flightSegment)
							.getAvailableFlightSegments()) {
						updateGroup(factory, group2, availableFlightSegment);
					}
				}
			}
		}

		paores.getGROUP2().add(group2);
		paores.setUNT(factory.createUNT(null));

		IATA iata = factory.getObjectFactory().createIATA();
		iata.getUNBAndUNGAndPAORES().add(factory.createUNB(null, null));
		iata.getUNBAndUNGAndPAORES().add(paores);
		return factory.getObjectFactory().createIATA(iata);
	}

	private static void updateGroup(PaoresFactory factory, GROUP2 group2, AvailableFlightSegment availableFlightSegment)
			throws InteractiveEDIException {
		if (availableFlightSegment.getSelectedLogicalCabinClass() != null) {
			GROUP3 group3 = factory.getObjectFactory().createGROUP3();
			FlightSegmentDTO segmDto = availableFlightSegment.getFlightSegmentDTO();

			FlightSegWithCCDTO dto = GDSServicesModuleUtil.getFlightInventoryBD().getFlightSegmentsWithCabinClass(
					segmDto.getSegmentId());
			group3.setTVL(factory.createTVL(dto.getDepartureDate(), dto.getArrivalDate(), dto.getOrigin(), dto.getDestination(),
					dto.getFlightNumber().substring(0, 2), dto.getFlightNumber().substring(2)));
			Map<String, String> cabinMap = new HashMap<String, String>();
			for (String cabinClass : dto.getCabinClassAvailability()) {
				cabinMap.put(cabinClass.split("-")[0], "A");
			}
			group3.setPDI(factory.createPDI(cabinMap));
			RouteInfo routeInfo = getRouteInfo(segmDto.getFromAirport(), segmDto.getToAirport());
			group3.setAPD(factory.createAPD(dto.getModelNumber().substring(1), "0", dto.getDepartureDate(), dto.getArrivalDate(),
					"6", dto.getFrequency(), routeInfo));
			group2.getGROUP3().add(group3);
		}
	}

	private static RouteInfo getRouteInfo(String from, String to) {
		StringBuilder sb = new StringBuilder();
		sb.append(from).append("/").append(to);
		try {
			return GDSServicesModuleUtil.getCommonMasterBD().getRouteInfo(sb.toString());
		} catch (ModuleException e) {
			Log.error("Unable to retrieve routing info for  [" + sb.toString() + "]", e);
		}
		return null;
	}
}
