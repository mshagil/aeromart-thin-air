package com.isa.thinair.gdsservices.core.common;

import java.util.Date;

import com.isa.thinair.gdsservices.api.model.GDSTransactionStatus;

@Deprecated
public class GDSTypeATransactionStatusUtils {

	public static GDSTransactionStatus createOrUpdateTransactionStage(String tnxRefId, String stage) {

		GDSTransactionStatus gdsTransactionStatus = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
				.getGDSTransactionStatusFromRef(tnxRefId);
		if (gdsTransactionStatus == null) {
			gdsTransactionStatus = new GDSTransactionStatus(new Date(), tnxRefId, null, stage, null);
		}
		gdsTransactionStatus.setTnxStage(stage);
		GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(gdsTransactionStatus);

		return gdsTransactionStatus;
	}

}
