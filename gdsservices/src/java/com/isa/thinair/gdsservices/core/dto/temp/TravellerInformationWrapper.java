package com.isa.thinair.gdsservices.core.dto.temp;

import java.io.Serializable;

import com.isa.typea.common.TravellerInformation;

public class TravellerInformationWrapper
		implements Serializable {
	private TravellerInformation travellerInformation;

	public TravellerInformationWrapper(TravellerInformation travellerInformation) {
		this.travellerInformation = travellerInformation;
	}

	public TravellerInformation getTravellerInformation() {
		return travellerInformation;
	}

	public void setTravellerInformation(TravellerInformation travellerInformation) {
		this.travellerInformation = travellerInformation;
	}

}
