package com.isa.thinair.gdsservices.core.util;

import com.isa.typea.common.OriginatorInformation;

public interface TypeASpecificConstants {
	
	enum TypeASegment {
		ORIGINATOR_DETAILS ("ORG", OriginatorInformation.class);

		private String code;
		private Class clazz;

		private TypeASegment(String code, Class clazz) {
			this.code = code;
			this.clazz = clazz;
		}

		public String getCode() {
			return code;
		}

		public Class getClazz() {
			return clazz;
		}

		public static TypeASegment getTypeASegment(String code) {
			for (TypeASegment segment : values()) {
				if (segment.getCode().equals(code)) {
					return segment;
				}
			}
			return null;
		}
	}

}
