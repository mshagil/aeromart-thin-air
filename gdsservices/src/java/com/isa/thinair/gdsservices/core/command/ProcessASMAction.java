package com.isa.thinair.gdsservices.core.command;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.external.AdHocScheduleProcessStatus;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMAuditDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMFlightLegDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSummaryAuditDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMMessageProcessStatus;
import com.isa.thinair.gdsservices.api.dto.external.ScheduleProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.bl.audit.ProcSSMASMAudit;
import com.isa.thinair.platform.api.ServiceResponce;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProcessASMAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ProcessASMAction.class);
	private static String TIME_FORMAT = "HHmm";
	private static String LOCAL_TIME = "LT";
	private static String ZULU_TIME = "UTC";
	private SSMASMMessegeDTO ssIMessegeDTO = null;
	private boolean isEnableAttachDefaultAnciTemplate;
	private boolean isLocalTimeMode = false;
	private String userID = null;
	private Integer inMessegeID = null;
	private AirportBD airportBD;
	public static final String OVERLAP_SUPPLEMENTARY_INFO = "(SI)([ ])(OLAP)([ ])*([A-Z]{3}[\\/#A-Z]{4,30})?";
	private boolean isAdhocBulkUpdate = false;
	private boolean isDetailAuditEnabled = false;
	private boolean isAdHocScheduleUpdate = false;

	public ProcessASMAction(SSMASMMessegeDTO ssIMessegeDTO, boolean isEnableAttachDefaultAnciTemplate) {
		this.ssIMessegeDTO = ssIMessegeDTO;
		airportBD = AirSchedulesUtil.getAirportBD();
		this.isEnableAttachDefaultAnciTemplate = isEnableAttachDefaultAnciTemplate;
		this.isDetailAuditEnabled = AppSysParamsUtil.isDetailAuditEnabledForProcessingScheduleMessages();
	}

	public SSMASMResponse processASMMessageAndUpdteSchedules() throws ModuleException {
		SSMASMResponse ssiMResponse = new SSMASMResponse();

		if (isValidateSSMMessageReceived()) {
			applyDefaultTimeMode();
			this.userID = ssIMessegeDTO.getProcessingUserID();
			this.inMessegeID = ssIMessegeDTO.getInMessegeID();
			List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList = (List<SSMASMSUBMessegeDTO>) ssIMessegeDTO.getSsmASMSUBMessegeList();
			ssiMResponse = processASMSubMessages(ssmASMSubMsgsList);

			addDetailMessageProcessedAudit(ssiMResponse);

		} else {
			ssiMResponse.setSuccess(false);
			ssiMResponse.setResponseMessage("");
		}

		return ssiMResponse;
	}

	public SSMASMResponse processSSMAsAdhocMessage(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO,
			boolean isEnableAttachDefaultAnciTemplate, Integer inMessegeID, String userID, boolean isLocalTimeMode)
			throws ModuleException {
		this.isEnableAttachDefaultAnciTemplate = isEnableAttachDefaultAnciTemplate;
		this.userID = userID;
		this.inMessegeID = inMessegeID;
		this.isLocalTimeMode = isLocalTimeMode;
		this.isAdHocScheduleUpdate = true;

		return processASMSubMessage(ssmASMSubMsgsDTO);
	}

	public SSMASMResponse processSSMAsAdhocMessageBulkUpdate(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO,
			boolean isEnableAttachDefaultAnciTemplate, Integer inMessegeID, String userID, boolean isLocalTimeMode)
			throws ModuleException {
		this.isEnableAttachDefaultAnciTemplate = isEnableAttachDefaultAnciTemplate;
		this.userID = userID;
		this.inMessegeID = inMessegeID;
		this.isLocalTimeMode = isLocalTimeMode;
		this.isAdhocBulkUpdate = true;
		this.isAdHocScheduleUpdate = true;

		SSMASMResponse responseSummary = new SSMASMResponse();
		Collection<Integer> updatedFlightIds = new ArrayList<>();

		if (SSMASMSUBMessegeDTO.Action.NEW.equals(ssmASMSubMsgsDTO.getAction()) && isDelayProcessingCancelAction()) {
			Flight composeFlight = composeFlightInfo(ssmASMSubMsgsDTO);
			String segmentCode = getFlightSegmentCode(composeFlight);

			if (!StringUtil.isNullOrEmpty(segmentCode)) {
				Collection<Integer> flightIds = GDSServicesModuleUtil.getFlightBD().getPossibleFlightDesignatorChange(
						ssmASMSubMsgsDTO.getStartDate(), segmentCode, this.isLocalTimeMode, ssmASMSubMsgsDTO.getEndDate(), true);

				if (flightIds != null && !flightIds.isEmpty()) {
					for (Integer flightId : flightIds) {
						Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

						updateAdHocFlight(flight, ssmASMSubMsgsDTO, responseSummary, updatedFlightIds);

					}
					responseSummary.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
				}

			}

		} else if (!SSMASMSUBMessegeDTO.Action.NEW.equals(ssmASMSubMsgsDTO.getAction())) {
			Collection<Flight> flightColl = getAvailableFlights(ssmASMSubMsgsDTO, true);
			updateAdHocFlights(flightColl, ssmASMSubMsgsDTO, responseSummary);

		}
		return responseSummary;
	}

	private SSMASMResponse processASMSubMessages(List<SSMASMSUBMessegeDTO> asmSubMsgsList) throws ModuleException {
		SSMASMResponse ssiMResponse = new SSMASMResponse();
		if (asmSubMsgsList != null && asmSubMsgsList.size() > 0) {
			for (SSMASMSUBMessegeDTO ssmASMSubMsgsDTO : asmSubMsgsList) {
				ssiMResponse = processASMSubMessage(ssmASMSubMsgsDTO);

				if (!ssiMResponse.isSuccess()) {
					break;
				}
			}

		}

		return ssiMResponse;
	}

	private SSMASMResponse processASMSubMessage(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssiMResponse = new SSMASMResponse();

		GDSApiUtils.validateScheduleDate(ssmASMSubMsgsDTO, ssmASMSubMsgsDTO.getAction().toString());

		switch (ssmASMSubMsgsDTO.getAction()) {
		case NEW:
			ssiMResponse = this.processRequestForNEW(ssmASMSubMsgsDTO);

			break;

		case CNL:
			ssiMResponse = this.processRequestForCNL(ssmASMSubMsgsDTO);
			break;

		case RPL:
			ssiMResponse = this.processRequestForRPL(ssmASMSubMsgsDTO);
			break;

		case EQT:
			ssiMResponse = this.processRequestForEQT(ssmASMSubMsgsDTO);
			break;

		case TIM:
			ssiMResponse = this.processRequestForTIM(ssmASMSubMsgsDTO);
			break;

		case FLT:
			ssiMResponse = this.processRequestForFLT(ssmASMSubMsgsDTO);
			break;

		case ACK:
		case NAC:
		case SKD:
		case REV:
		case ADM:
			ssiMResponse.setResponseMessage("ACTION TYPE NOT SUPPORTED");
			break;

		}

		return ssiMResponse;
	}

	private SSMASMResponse processRequestForNEW(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			GDSApiUtils.validateSsmAsmRequest(ssmASMSubMsgsDTO);

			Flight flight = composeFlightInfo(ssmASMSubMsgsDTO);

			Flight flightWaitingCnl = getWaitingFlightDesignatorChangeRequest(flight);
			if (flightWaitingCnl != null) {
				removeFlightDelayedCancelMessage(flightWaitingCnl.getFlightId(), ssmASMSubMsgsDTO);
				ssimResponse = replaceFlightDesignatorInformation(ssmASMSubMsgsDTO, flightWaitingCnl.getFlightId());
			}

			if (!ssimResponse.isSuccess() && !this.isAdhocBulkUpdate) {
				Flight flightExist = retreiveMatchingFlight(ssmASMSubMsgsDTO);
				if (flightExist == null || flight.getOverlapingFlightId() != null) {
					ServiceResponce serviceResponce = GDSServicesModuleUtil.getFlightBD()
							.createFlight(GDSApiUtils.getFlightAllowActionDTO(), flight, this.isEnableAttachDefaultAnciTemplate);

					// ssimResponse.setSsiMessegeDTO(ssmASMSubMsgsDTO);

					Integer flightID = (Integer) serviceResponce.getResponseParam(CommandParamNames.FLIGHT_ID);

					auditSSMProc(ssmASMSubMsgsDTO, flightID, ProcSSMASMAudit.Operation.CREATE, serviceResponce.isSuccess(),
							serviceResponce.getResponseCode());

					AdHocScheduleProcessStatus adHocScheduleProcessStatus = null;
					if (this.isDetailAuditEnabled && flightID != null) {
						Flight flightNew = GDSServicesModuleUtil.getFlightBD().getFlight(flightID);
						adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flightNew);
					}

					if (serviceResponce != null && serviceResponce.isSuccess()) {
						updatedFlightIds.add(flightID);
						ssimResponse.setSuccess(true);
						ssimResponse.setResponseMessage(null);
						adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
					} else {
						// // unable to create flight schedule
						ssimResponse.setSuccess(false);
						log.info(" ERROR @ ProcessASMAction :: processRequestForNEW :" + serviceResponce.getResponseCode());
						ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
					}

				} else {
					boolean cnlIgnored = removeFlightDelayedCancelMessage(flightExist.getFlightId(), ssmASMSubMsgsDTO);
					log.info(" Flight Designator already defined on the same day :: expecting to update ");
					if (cnlIgnored) {
						// replace flight info;
						ssimResponse = replaceFlightInformation(flightExist.getFlightId(), flight, ssmASMSubMsgsDTO);

					} else {
						ssimResponse.setSuccess(false);
						ssimResponse.setResponseMessage("FLIGHT/DATE LIMITED TO ONE OCCURRENCE");
					}

				}
			}

		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: processRequestForNEW", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.CREATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private SSMASMResponse processRequestForCNL(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Integer flightId = null;
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			Collection<Flight> flightColl = getAvailableAdHocFlights(ssmASMSubMsgsDTO);

			if (flightColl != null && !flightColl.isEmpty()) {
				for (Flight flightDTO : flightColl) {
					flightId = flightDTO.getFlightId();
					AdHocScheduleProcessStatus adHocScheduleProcessStatus = null;
					if (this.isDetailAuditEnabled) {
						Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);
						adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);
					}

					if (flightId != null) {
						if (isDelayProcessingCancelAction()) {
							ssimResponse = delayProcessingFlightCNLAction(flightId, ssmASMSubMsgsDTO);
						} else {
							ssimResponse = cancelFlight(flightId, ssmASMSubMsgsDTO);
						}
						break;
					}

					if (this.isDetailAuditEnabled) {
						if (ssimResponse.isSuccess()) {
							adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
							updatedFlightIds.add(flightId);
						}
						ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);
					}

				}
			}
		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: processRequestForCNL", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.CANCEL, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private SSMASMResponse processRequestForEQT(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Integer flightId = null;
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			Collection<Flight> flightColl = getAvailableAdHocFlights(ssmASMSubMsgsDTO);
			if (flightColl != null && !flightColl.isEmpty()) {
				for (Flight flightDTO : flightColl) {
					flightId = flightDTO.getFlightId();

					if (flightId != null) {
						Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);
						String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
								ssmASMSubMsgsDTO.getAircraftConfiguration());
						flight.setModelNumber(modelNo);
						ServiceResponce serviceResponce = GDSServicesModuleUtil.getFlightBD().updateFlight(
								GDSApiUtils.getFlightAllowActionDTO(), flight, new FlightAlertDTO(), true, false, true,
								isEnableAttachDefaultAnciTemplate);

						auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
								serviceResponce.getResponseCode());

						AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);

						if (serviceResponce != null && serviceResponce.isSuccess()) {
							updatedFlightIds.add(flightId);
							ssimResponse.setSuccess(true);
							adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
						} else {
							ssimResponse.setSuccess(false);
							log.error("Erro @ processRequestForEQT " + serviceResponce.getResponseCode());
							ssimResponse
									.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
						}
						ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);
						break;
					}

				}
			}

		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: processRequestForEQT", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private SSMASMResponse processRequestForTIM(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Integer flightId = null;
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			GDSApiUtils.validateSsmAsmRequest(ssmASMSubMsgsDTO);
			Collection<Flight> flightColl = getAvailableAdHocFlights(ssmASMSubMsgsDTO);
			if (flightColl != null && !flightColl.isEmpty()) {
				for (Flight flightDTO : flightColl) {
					flightId = flightDTO.getFlightId();
					ServiceResponce serviceResponce = null;
					if (flightId != null) {

						Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

						Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();
						if (legs != null && !legs.isEmpty()) {
							Date flightDepDate = ssmASMSubMsgsDTO.getStartDate();
							for (SSMASMFlightLegDTO legDTO : legs) {
								String depAirport = legDTO.getDepatureAirport();
								String arrAirport = legDTO.getArrivalAirport();
								if (!StringUtil.isNullOrEmpty(depAirport) && !StringUtil.isNullOrEmpty(arrAirport)) {

									Set<FlightLeg> scheduleLegs = flight.getFlightLegs();
									if (scheduleLegs != null && !scheduleLegs.isEmpty()) {
										for (FlightLeg fScheLeg : scheduleLegs) {
											if (depAirport.equalsIgnoreCase(fScheLeg.getOrigin())
													&& arrAirport.equalsIgnoreCase(fScheLeg.getDestination())) {

												updateFlightLegInfo(flight, fScheLeg, legDTO, flightDepDate);

											}

										}
									}

								}

							}
						}

						validateFlightLegs(flight);

						serviceResponce = GDSServicesModuleUtil.getFlightBD().updateFlight(GDSApiUtils.getFlightAllowActionDTO(),
								flight, GDSApiUtils.generateFlightAlertDTO(false), false, AppSysParamsUtil.isAllowedModificationForPastFlight(), true,
								isEnableAttachDefaultAnciTemplate);

						auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
								serviceResponce.getResponseCode());

						AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);

						if (serviceResponce != null && serviceResponce.isSuccess()) {
							updatedFlightIds.add(flightId);
							ssimResponse.setSuccess(true);
							adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
						} else {
							ssimResponse.setSuccess(false);
							log.error("Error @ processRequestForTIM " + serviceResponce.getResponseCode());
							ssimResponse
									.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
						}
						ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);
						break;
					}

				}
			}
		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: processRequestForTIM", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());

		return ssimResponse;
	}

	private static String getAircraftModelNo(String aircraftType, String aircraftVersion) throws ModuleException {
		String modelNumber = GDSApiUtils.getActiveAircraftModelNumberByIataType(aircraftType, aircraftVersion);
		if (modelNumber == null) {
			throw new ModuleException("gdsservices.airschedules.logic.bl.aircraft.type.invalid");
		}
		return modelNumber;
	}

	private void updateFlightSegments(Flight flight, Set<FlightLeg> legset) {
		HashSet<FlightSegement> segset = new HashSet<FlightSegement>();
		for (int i = 1; i < legset.size() + 1; i++) {

			Iterator<FlightLeg> legIt = legset.iterator();
			while (legIt.hasNext()) {

				FlightLeg leg1 = legIt.next();
				if (leg1.getLegNumber() == i) {

					String segmentCode = leg1.getOrigin() + "/" + leg1.getDestination();

					// create the segment
					FlightSegement seg1 = new FlightSegement();
					seg1.setSegmentCode(segmentCode);
					segset.add(seg1);

					// creating following segments
					for (int j = i + 1; j < legset.size() + 1; j++) {

						Iterator<FlightLeg> restLegIt = legset.iterator();
						while (restLegIt.hasNext()) {

							FlightLeg leg2 = restLegIt.next();
							if (leg2.getLegNumber() == j) {

								segmentCode = segmentCode + "/" + leg2.getDestination();

								// create folllowing segment
								FlightSegement seg2 = new FlightSegement();
								seg2.setSegmentCode(segmentCode);
								segset.add(seg2);

								break;
							}
						}
					}
					break;
				}
			}
		}
		flight.setFlightSegements(segset);
	}

	private Collection<Flight> getAvailableAdHocFlights(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {

		Collection<Flight> flightColl = getAvailableFlights(ssmASMSubMsgsDTO, false);

		if (flightColl == null || flightColl.isEmpty()) {
			throw new ModuleException("gdsservices.airschedules.logic.bl.data.not.exist");
		}

		return flightColl;
	}

	private SSMASMResponse processRequestForRPL(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Integer flightId = null;
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			GDSApiUtils.validateSsmAsmRequest(ssmASMSubMsgsDTO);
			Collection<Flight> flightColl = getAvailableAdHocFlights(ssmASMSubMsgsDTO);
			if (flightColl != null && !flightColl.isEmpty()) {
				for (Flight flightDTO : flightColl) {
					flightId = flightDTO.getFlightId();
					ServiceResponce serviceResponce = null;
					if (flightId != null) {
						Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

						if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getAircraftType())) {
							String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
									ssmASMSubMsgsDTO.getAircraftConfiguration());
							flight.setModelNumber(modelNo);
						}

						if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getServiceType())) {
							int operationTypeId = GDSApiUtils
									.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
							flight.setOperationTypeId(operationTypeId);
						}

						Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();

						Set<FlightLeg> legSet = new HashSet<FlightLeg>();
						int n = 1;
						String origin = null;
						String destination = null;
						if (legs != null && !legs.isEmpty()) {
							Date flightDepDate = ssmASMSubMsgsDTO.getStartDate();
							for (SSMASMFlightLegDTO legDTO : legs) {

								FlightLeg fScheLeg = new FlightLeg();

								updateFlightLegInfo(flight, fScheLeg, legDTO, flightDepDate);

								if (origin == null) {
									origin = legDTO.getDepatureAirport();
								}
								destination = legDTO.getArrivalAirport();
								fScheLeg.setLegNumber(n);
								n++;
								legSet.add(fScheLeg);

							}
						}
						flight.setFlightLegs(legSet);
						updateFlightSegments(flight, legSet);

						setFlightType(origin, destination, flight);

						// set CodeShare details
						flight.setCodeShareMCFlights(GDSApiUtils.getCodeShareMCFlights(ssmASMSubMsgsDTO.getMarketingFlightNos()));

						serviceResponce = GDSServicesModuleUtil.getFlightBD().updateFlight(GDSApiUtils.getFlightAllowActionDTO(),
								flight, new FlightAlertDTO(), true, false, true, isEnableAttachDefaultAnciTemplate);

						// ssimResponse.setSsiMessegeDTO(ssmASMSubMsgsDTO);
						auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
								serviceResponce.getResponseCode());

						AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);

						if (serviceResponce != null && serviceResponce.isSuccess()) {
							updatedFlightIds.add(flightId);
							ssimResponse.setSuccess(true);
							adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
						} else {
							ssimResponse.setSuccess(false);
							log.error("Erro @ processRequestForRPL " + serviceResponce.getResponseCode());
							ssimResponse
									.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
						}
						ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);
						break;
					}

				}
			}
		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: processRequestForRPL", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private void setFlightType(String origin, String destination, Flight flight) {
		boolean isDomesticRouteAvailable = false;
		List<FlightTypeDTO> lstFltTypes = AppSysParamsUtil.availableFlightTypes();
		for (FlightTypeDTO fltType : lstFltTypes) {
			if ("DOM".equals(fltType.getFlightType()) && "Y".equals(fltType.getFlightTypeStatus())) {
				isDomesticRouteAvailable = true;
			}
		}
		if (!StringUtil.isNullOrEmpty(origin) && !StringUtil.isNullOrEmpty(destination) && isDomesticRouteAvailable) {
			boolean isDomesticRoute = FlightUtil.isDomesticRoute(origin, destination);
			if (isDomesticRoute) {
				flight.setFlightType(FlightUtil.FLIGHT_TYPE_DOM);
			} else {
				flight.setFlightType(FlightUtil.FLIGHT_TYPE_INT);
			}
		} else {
			flight.setFlightType(FlightUtil.FLIGHT_TYPE_INT);
		}

	}

	private int dateVariation(Date departueDate, int proceedByDate) {

		if (this.isAdHocScheduleUpdate) {
			return proceedByDate;
		} else if (proceedByDate != 0) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(departueDate);
			int depDateOfMonth = cal.get(Calendar.DAY_OF_MONTH);
			int maxDateOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			int minDateOfMonth = cal.getActualMinimum(Calendar.DAY_OF_MONTH);

			if (minDateOfMonth == depDateOfMonth && proceedByDate > depDateOfMonth) {
				return -1;
			} else if (maxDateOfMonth == depDateOfMonth && proceedByDate < depDateOfMonth) {
				return 1;
			} else if (proceedByDate > depDateOfMonth) {
				return 1;
			} else if (depDateOfMonth > proceedByDate) {
				return -1;
			}
		}
		return 0;
	}

	private void updateFlightLegInfo(Flight flight, FlightLeg fScheLeg, SSMASMFlightLegDTO legDTO, Date flightDepDate)
			throws ParseException {
		fScheLeg.setOrigin(legDTO.getDepatureAirport());
		fScheLeg.setDestination(legDTO.getArrivalAirport());
		String depaturTime = legDTO.getDepatureTime();
		String arrivalTime = legDTO.getArrivalTime();
		int arrivalOffset = dateVariation(flightDepDate, legDTO.getArrivalOffset());
		int departureOffset = dateVariation(flightDepDate, legDTO.getDepatureOffiset());
		if (this.isLocalTimeMode) {
			fScheLeg.setEstDepartureTimeLocal(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
			fScheLeg.setEstArrivalTimeLocal(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
			fScheLeg.setEstArrivalDayOffsetLocal(arrivalOffset);
			fScheLeg.setEstDepartureDayOffsetLocal(departureOffset);
			fScheLeg.setDuration(0);
			fScheLeg.setEstDepartureTimeZulu(null);
			fScheLeg.setEstArrivalTimeZulu(null);

			if (flight.getDepartureDateLocal() == null) {
				flight.setDepartureDateLocal(flightDepDate);
			}
			flight.setDepartureDate(null);

		} else {
			fScheLeg.setEstDepartureTimeZulu(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
			fScheLeg.setEstArrivalTimeZulu(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
			fScheLeg.setEstArrivalDayOffset(arrivalOffset);
			fScheLeg.setEstDepartureDayOffset(departureOffset);
			fScheLeg.setEstDepartureTimeLocal(null);
			fScheLeg.setEstArrivalTimeLocal(null);
			fScheLeg.setDuration(0);
			if (flight.getDepartureDate() == null) {
				flight.setDepartureDate(flightDepDate);
			}
			flight.setDepartureDateLocal(null);
		}
	}

	private SSMASMResponse processRequestForFLT(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Integer flightId = null;
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			Collection<Flight> flightColl = getAvailableAdHocFlights(ssmASMSubMsgsDTO);
			if (flightColl != null && !flightColl.isEmpty()) {
				for (Flight flightDTO : flightColl) {
					flightId = flightDTO.getFlightId();

					ServiceResponce serviceResponce = null;
					if (flightId != null) {

						Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

						if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getNewFlightDesignator())) {
							if (ssmASMSubMsgsDTO.isCodeShareCarrier() || !ssmASMSubMsgsDTO.isOwnSchedule()) {
								flight.setCsOCFlightNumber(ssmASMSubMsgsDTO.getNewFlightDesignator());
							} else {
								flight.setFlightNumber(ssmASMSubMsgsDTO.getNewFlightDesignator());
							}

							if (isFlightDepartureDateChanged(ssmASMSubMsgsDTO)) {

								Set<FlightLeg> legSet = new HashSet<FlightLeg>();
								if (this.isLocalTimeMode) {
									addLocalTimeDetailsToFlight(flight);
								}

								Collection<FlightLeg> legs = flight.getFlightLegs();
								if (legs != null && !legs.isEmpty()) {
									for (FlightLeg legDTO : legs) {

										FlightLeg fScheLeg = new FlightLeg();
										fScheLeg.setOrigin(legDTO.getOrigin());
										fScheLeg.setDestination(legDTO.getDestination());

										if (this.isLocalTimeMode) {
											fScheLeg.setEstDepartureTimeLocal(legDTO.getEstDepartureTimeLocal());
											fScheLeg.setEstArrivalTimeLocal(legDTO.getEstArrivalTimeLocal());
											fScheLeg.setEstArrivalDayOffsetLocal(legDTO.getEstArrivalDayOffsetLocal());
											fScheLeg.setEstDepartureDayOffsetLocal(legDTO.getEstDepartureDayOffsetLocal());
											fScheLeg.setDuration(0);
											fScheLeg.setEstDepartureTimeZulu(null);
											fScheLeg.setEstArrivalTimeZulu(null);
											flight.setDepartureDateLocal(ssmASMSubMsgsDTO.getNewStartDate());
											flight.setDepartureDate(null);

										} else {
											fScheLeg.setEstDepartureTimeZulu(legDTO.getEstDepartureTimeZulu());
											fScheLeg.setEstArrivalTimeZulu(legDTO.getEstArrivalTimeZulu());
											fScheLeg.setEstArrivalDayOffset(legDTO.getEstArrivalDayOffset());
											fScheLeg.setEstDepartureDayOffset(legDTO.getEstDepartureDayOffset());
											fScheLeg.setEstDepartureTimeLocal(null);
											fScheLeg.setEstArrivalTimeLocal(null);
											fScheLeg.setDuration(0);
											flight.setDepartureDate(ssmASMSubMsgsDTO.getNewStartDate());
											flight.setDepartureDateLocal(null);
										}
										fScheLeg.setLegNumber(legDTO.getLegNumber());
										legSet.add(fScheLeg);

									}
								}
								flight.setFlightLegs(legSet);
								updateFlightSegments(flight, legSet);
							}
						} else {
							throw new ModuleException("gdsservices.response.message.new.flight.no.required.flt");
						}
						serviceResponce = GDSServicesModuleUtil.getFlightBD().updateFlight(GDSApiUtils.getFlightAllowActionDTO(),
								flight, GDSApiUtils.generateFlightAlertDTO(false), true, false, true,
								isEnableAttachDefaultAnciTemplate);

						auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
								serviceResponce.getResponseCode());

						AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);

						if (serviceResponce != null && serviceResponce.isSuccess()) {
							updatedFlightIds.add(flightId);
							adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
							ssimResponse.setSuccess(true);
						} else {
							ssimResponse.setSuccess(false);
							log.error("Erro @ processRequestForFLT " + serviceResponce.getResponseCode());
							ssimResponse
									.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
						}
						ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);
						break;
					}

				}
			}

		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: processRequestForFLT", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private boolean isValidateSSMMessageReceived() {

		if (ssIMessegeDTO == null || ssIMessegeDTO.getSsmASMSUBMessegeList() == null
				|| ssIMessegeDTO.getSsmASMSUBMessegeList().isEmpty()) {
			return false;
		}
		return true;
	}

	private void applyDefaultTimeMode() {
		if (LOCAL_TIME.equals(ssIMessegeDTO.getTimeMode())) {
			this.isLocalTimeMode = true;
		}
	}

	private void validateFlightLegs(Flight flight) throws ModuleException {
		if (flight.getFlightLegs() != null && !flight.getFlightLegs().isEmpty()) {
			String timeMode = null;
			for (FlightLeg fScheLeg : flight.getFlightLegs()) {
				Date depTime = fScheLeg.getEstDepartureTimeLocal();
				Date arrTime = fScheLeg.getEstArrivalTimeLocal();

				Date depTimeZulu = fScheLeg.getEstDepartureTimeZulu();
				Date arrTimeZulu = fScheLeg.getEstArrivalTimeZulu();

				if (depTime != null && arrTime != null) {
					if (timeMode != null && ZULU_TIME.equals(timeMode)) {
						throw new ModuleException("gdsservices.airschedules.logic.bl.leg.data.conflict.with.exist.data");
					}
					timeMode = LOCAL_TIME;
				} else if (depTimeZulu != null && arrTimeZulu != null) {
					if (timeMode != null && LOCAL_TIME.equals(timeMode)) {
						throw new ModuleException("gdsservices.airschedules.logic.bl.leg.data.conflict.with.exist.data");
					}
					timeMode = ZULU_TIME;
				}

			}
		}
	}

	private void auditSSMProc(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, Integer scheduleId, ProcSSMASMAudit.Operation operation,
			boolean success, String responseCode) throws ModuleException {

		SSMASMAuditDTO ssmAuditDTO = new SSMASMAuditDTO();

		ssmAuditDTO.setUserID(this.userID);
		ssmAuditDTO.setInMessegeID(this.inMessegeID);
		ssmAuditDTO.setScheduleId(scheduleId);
		ssmAuditDTO.setSsmASMSubMsgsDTO(ssmASMSubMsgsDTO);
		ssmAuditDTO.setOperation(operation);
		ssmAuditDTO.setResponseCode(responseCode);
		ssmAuditDTO.setSuccess(success);
		ssmAuditDTO.setRawMessage(ssIMessegeDTO.getRawMessage());

		ProcSSMASMAudit.doAuditProcSSMASM(ssmAuditDTO);
	}

	private Integer getPosibleOverlapFlightId(Flight flight, String subSupplementaryInfo) {

		try {
			List<String> validSegmentCodes = new ArrayList<String>();
			boolean isCheckForOverlapFlight = false;
			if (!StringUtil.isNullOrEmpty(subSupplementaryInfo) && subSupplementaryInfo.matches(OVERLAP_SUPPLEMENTARY_INFO)) {
				String overlappingSegmentCode = null;
				Pattern pattern = Pattern.compile(OVERLAP_SUPPLEMENTARY_INFO);
				Matcher matcher = pattern.matcher(subSupplementaryInfo);
				if (matcher.find()) {
					overlappingSegmentCode = matcher.group(5);
				}
				if (!StringUtil.isNullOrEmpty(overlappingSegmentCode)) {
					validSegmentCodes = Arrays.asList(overlappingSegmentCode.split("#"));
				}
				isCheckForOverlapFlight = true;
			}

			Collection<Flight> possibleOverlappingFlights = (GDSServicesModuleUtil.getFlightBD()
					.getPossibleOverlappingFlights(flight));

			if (possibleOverlappingFlights != null && !possibleOverlappingFlights.isEmpty()) {
				Flight possibleOverlappingFlight = getOverlappingFlight(possibleOverlappingFlights,
						isCheckForOverlapFlight, flight);

				allowOverlapValidSegments(possibleOverlappingFlight, flight, validSegmentCodes);

				if (possibleOverlappingFlight != null && validOverlappingSegmentExist(flight)) {
					return possibleOverlappingFlight.getFlightId();
				}
			}

		} catch (Exception e) {
			log.error("error while retreiving getPosibleOverlapFlightId");
		}
		return null;
	}

	private void addLocalTimeDetailsToFlight(Flight flight) throws ModuleException {
		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
		flight = timeAdder.addLocalTimeDetailsToFlight(flight);
	}

	private void allowOverlapValidSegments(Flight possibleOverlappingFlight, Flight flight, List<String> validSegmentCodes) {
		if (possibleOverlappingFlight != null && possibleOverlappingFlight.getFlightSegements() != null && flight != null
				&& flight.getFlightSegements() != null) {
			for (FlightSegement fltSegement : flight.getFlightSegements()) {
				for (FlightSegement posbleFltSegement : possibleOverlappingFlight.getFlightSegements()) {
					if (posbleFltSegement.getSegmentCode().equals(fltSegement.getSegmentCode())) {
						fltSegement.setValidFlag(false);
					}

					if (validSegmentCodes != null && !validSegmentCodes.isEmpty()
							&& !validSegmentCodes.contains(fltSegement.getSegmentCode())) {
						fltSegement.setValidFlag(false);
					}
				}

			}
		}

	}

	private boolean validOverlappingSegmentExist(Flight flight) {
		for (FlightSegement fltSegement : flight.getFlightSegements()) {
			if (fltSegement.getValidFlag()) {
				return true;
			}
		}
		return false;

	}

	private Flight getOverlappingFlight(Collection<Flight> possibleOverlappingFlights, boolean isCheckForOverlapFlight,
			Flight flight) {
		if (isCheckForOverlapFlight || AppSysParamsUtil.isEnableOverlappingbySsmAsm()) {
			for (Flight overlappingFlight : possibleOverlappingFlights) {

				// to allow same route creation, and skip same schedule when adding days of
				// oepration
				if (overlappingFlight.getOverlapingFlightId() == null
						&& LegUtil.checkLegsAddedDeleted(overlappingFlight, flight)
						&& overlappingFlight.getModelNumber() != null
						&& overlappingFlight.getModelNumber().equals(flight.getModelNumber())) {
					return overlappingFlight;
				}

			}
		}
		return null;
	}

	private SSMASMResponse cancelFlight(Integer flightId, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		ServiceResponce serviceResponce = GDSServicesModuleUtil.getFlightBD().cancelFlight(flightId, true,
				GDSApiUtils.generateFlightAlertDTO(true));

		auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.CANCEL, serviceResponce.isSuccess(),
				serviceResponce.getResponseCode());

		// ssimResponse.setSsiMessegeDTO(ssmASMSubMsgsDTO);
		if (serviceResponce != null && serviceResponce.isSuccess()) {
			ssimResponse.setSuccess(true);
		} else {
			ssimResponse.setSuccess(false);
			log.error("Erro @ processRequestForCNL " + serviceResponce.getResponseCode());
			ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
		}

		return ssimResponse;
	}

	private SSMASMResponse delayProcessingFlightCNLAction(Integer flightId, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();

		try {
			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flightId);
			Date currentSystemTime = CalendarUtil.getCurrentZuluDateTime();
			Date holdTill = CalendarUtil.addSeconds(CalendarUtil.getCurrentZuluDateTime(),
					AppSysParamsUtil.getSsmAsmCnlActionProcessingHoldTimeInSeconds());

			GDSServicesModuleUtil.getFlightBD().updateDelayCancelFlight(flightIds, true, holdTill);

			auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, true,
					"ASM CNL Action delayed from " + CalendarUtil.formatForSQL(currentSystemTime, CalendarUtil.PATTERN7) + " to "
							+ CalendarUtil.formatForSQL(holdTill, CalendarUtil.PATTERN7));
			ssimResponse.setSuccess(true);

		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: delayProcessingFlightCNLAction", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, false,
					"Unable to delay the ASM CNL Action");
		}

		return ssimResponse;
	}

	private void ignoreFlightWaitingCancelMessage(Integer flightId, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		try {
			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flightId);
			GDSServicesModuleUtil.getFlightBD().updateDelayCancelFlight(flightIds, false, null);
			auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, true, "Flight CNL message ignored");
		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: ignoreFlightWaitingCancelMessage", e);
			auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, false,
					"Unable to delay the ASM CNL Action");
		}

	}

	private boolean removeFlightDelayedCancelMessage(Integer flightId, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException {
		Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);
		if (flight.isCancelMsgWaiting()) {
			ignoreFlightWaitingCancelMessage(flightId, ssmASMSubMsgsDTO);
			return true;
		}
		return false;
	}

	private boolean isDelayProcessingCancelAction() {
		if (AppSysParamsUtil.isEnableDelayProcessingSsmAsmCnlAction()
				&& AppSysParamsUtil.getSsmAsmCnlActionProcessingHoldTimeInSeconds() > 0) {
			return true;
		}
		return false;
	}

	private Collection<Flight> getAvailableFlights(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, boolean isAdHocFlightsOnly)
			throws ModuleException {

		Collection<Flight> flightColl = new ArrayList<Flight>();

		ScheduleSearchDTO scheduleSearchDTO = new ScheduleSearchDTO();
		scheduleSearchDTO.setStartDate(CalendarUtil.getStartTimeOfDate(ssmASMSubMsgsDTO.getStartDate()));
		if (ssmASMSubMsgsDTO.isCodeShareCarrier()) {
			scheduleSearchDTO.setCsOCFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
			scheduleSearchDTO.setCsOCCarrierCode(ssmASMSubMsgsDTO.getOperatingCarrier());
		} else {
			scheduleSearchDTO.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
		}

		if (!isAdHocFlightsOnly) {
			scheduleSearchDTO.setEndDate(CalendarUtil.getEndTimeOfDate(ssmASMSubMsgsDTO.getStartDate()));
			scheduleSearchDTO.setAdHocFlightsOnly(false);
		} else {
			scheduleSearchDTO.setEndDate(CalendarUtil.getEndTimeOfDate(ssmASMSubMsgsDTO.getEndDate()));
			scheduleSearchDTO.setAdHocFlightsOnly(true);
		}

		if (this.isLocalTimeMode) {
			scheduleSearchDTO.setLocalTime(true);
		}
		List<String> statusFilters = new ArrayList<String>();
		statusFilters.add(FlightStatusEnum.CREATED.toString());
		statusFilters.add(FlightStatusEnum.ACTIVE.toString());
		scheduleSearchDTO.setStatusFilter(statusFilters);
		flightColl = GDSServicesModuleUtil.getFlightBD().getAvailableAdhocFlights(scheduleSearchDTO);

		return flightColl;

	}

	private Flight retreiveMatchingFlight(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {

		Collection<Flight> flightColl = getAvailableFlights(ssmASMSubMsgsDTO, false);
		if (flightColl != null && !flightColl.isEmpty()) {
			for (Flight flight : flightColl) {
				if (!FlightStatusEnum.CANCELLED.toString().equalsIgnoreCase(flight.getStatus())) {
					return flight;
				}
			}
		}

		return null;
	}

	private SSMASMResponse replaceFlightInformation(Integer flightId, Flight composedFlight, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {

			if (flightId != null) {
				Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

				if (!flight.getFlightNumber().equalsIgnoreCase(composedFlight.getFlightNumber())) {
					flight.setFlightNumber(composedFlight.getFlightNumber());
				}

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getAircraftType())) {
					String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
							ssmASMSubMsgsDTO.getAircraftConfiguration());
					flight.setModelNumber(modelNo);
				}

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getServiceType())) {
					int operationTypeId = GDSApiUtils.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
					flight.setOperationTypeId(operationTypeId);
				}

				Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();

				Set<FlightLeg> legSet = new HashSet<FlightLeg>();
				int n = 1;
				String origin = null;
				String destination = null;
				if (legs != null && !legs.isEmpty()) {
					Date flightDepDate = ssmASMSubMsgsDTO.getStartDate();
					for (SSMASMFlightLegDTO legDTO : legs) {

						FlightLeg fScheLeg = new FlightLeg();

						updateFlightLegInfo(flight, fScheLeg, legDTO, flightDepDate);

						if (origin == null) {
							origin = legDTO.getDepatureAirport();
						}
						destination = legDTO.getArrivalAirport();
						fScheLeg.setLegNumber(n);
						n++;
						legSet.add(fScheLeg);

					}
					if (legSet != null && !legSet.isEmpty()) {
						flight.setFlightLegs(legSet);
						updateFlightSegments(flight, legSet);
					}
				}

				setFlightType(origin, destination, flight);

				flight.setCodeShareMCFlights(GDSApiUtils.getCodeShareMCFlights(ssmASMSubMsgsDTO.getMarketingFlightNos()));

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getFlightBD().updateFlight(
						GDSApiUtils.getFlightAllowActionDTO(), flight, GDSApiUtils.generateFlightAlertDTO(false), true, false, true,
						isEnableAttachDefaultAnciTemplate);

				auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());

				AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					updatedFlightIds.add(flightId);
					ssimResponse.setSuccess(true);
					adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
				} else {
					ssimResponse.setSuccess(false);
					log.error("Erro @ replaceFlightInformation " + serviceResponce.getResponseCode());
					ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
				}
				ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);
			}

		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: replaceFlightInformation", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private Flight getWaitingFlightDesignatorChangeRequest(Flight composedFlight) throws ModuleException {
		Date departureDate = null;

		if (isLocalTimeMode) {
			departureDate = composedFlight.getDepartureDateLocal();
		} else {
			departureDate = composedFlight.getDepartureDate();
		}

		String segmentCode = getFlightSegmentCode(composedFlight);

		Collection<Integer> flightIds = GDSServicesModuleUtil.getFlightBD().getPossibleFlightDesignatorChange(departureDate,
				segmentCode, isLocalTimeMode, null, false);

		if (flightIds != null && !flightIds.isEmpty()) {
			for (Integer flightId : flightIds) {
				Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

				boolean isLegTimeChanged = true;
				if (isLocalTimeMode) {
					addLocalTimeDetailsToFlight(flight);

					isLegTimeChanged = LegUtil.isLegLocalTimeChanged(flight, composedFlight);

				} else {
					isLegTimeChanged = LegUtil.isLegTimeChanged(flight, composedFlight);

				}

				if (!isLegTimeChanged && !LegUtil.checkLegsAddedDeleted(flight, composedFlight)) {
					if (!flight.getFlightNumber().equalsIgnoreCase(composedFlight.getFlightNumber())) {
						return flight;
					}
				}

			}
		}

		return null;
	}

	private SSMASMResponse replaceFlightDesignatorInformation(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, Integer flightId)
			throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		try {
			if (flightId != null) {
				Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

				if (!flight.getFlightNumber().equalsIgnoreCase(ssmASMSubMsgsDTO.getFlightDesignator())) {
					flight.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
				}

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getAircraftType())) {
					String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
							ssmASMSubMsgsDTO.getAircraftConfiguration());
					flight.setModelNumber(modelNo);
				}

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getServiceType())) {
					int operationTypeId = GDSApiUtils.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
					flight.setOperationTypeId(operationTypeId);
				}

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getFlightBD().updateFlight(
						GDSApiUtils.getFlightAllowActionDTO(), flight, GDSApiUtils.generateFlightAlertDTO(false), true, false, true,
						isEnableAttachDefaultAnciTemplate);

				auditSSMProc(ssmASMSubMsgsDTO, flightId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
						"Flight Designator change");
				AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);
				if (serviceResponce != null && serviceResponce.isSuccess()) {
					ssimResponse.setSuccess(true);
					adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
					updatedFlightIds.add(flightId);
				} else {
					ssimResponse.setSuccess(false);
					log.error("Erro @ replaceFlightDesignatorInformation " + serviceResponce.getResponseCode());
					ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
				}

				ssimResponse.addScheduleProcessStatus(adHocScheduleProcessStatus);

			}
		} catch (Exception e) {
			log.error(" Exception @ ProcessASMAction :: replaceFlightDesignatorInformation", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, "Flight Designator change");
		}
		ssimResponse.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		return ssimResponse;
	}

	private boolean isFlightDepartureDateChanged(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) {
		if (MessageType.ASM.equals(ssmASMSubMsgsDTO.getMessageType()) && ssmASMSubMsgsDTO.getNewStartDate() != null
				&& !CalendarUtil.isSameDay(ssmASMSubMsgsDTO.getStartDate(), ssmASMSubMsgsDTO.getNewStartDate())) {
			return true;
		}
		return false;
	}

	private Frequency getFrequencyOfDays(
			Collection<com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.DayOfWeek> daysOfOperation) {
		Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();

		if (daysOfOperation != null && !daysOfOperation.isEmpty()) {
			for (com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.DayOfWeek dayOfWeek : daysOfOperation) {
				dayList.add(CalendarUtil.getIATAStandardDay(dayOfWeek.dayNumber()));
			}
		}
		Frequency fqn = CalendarUtil.getFrequencyFromDays(dayList);
		return fqn;
	}

	private Flight composeFlightInfo(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {

		Flight flight = null;
		try {
			flight = new Flight();
			Date startDate = ssmASMSubMsgsDTO.getStartDate();

			String externalFlightNo = ssmASMSubMsgsDTO.getExternalFlightNo();

			if (!ssmASMSubMsgsDTO.isOwnSchedule() && !StringUtil.isNullOrEmpty(externalFlightNo)) {
				flight.setFlightNumber(externalFlightNo);
				flight.setCsOCCarrierCode(ssmASMSubMsgsDTO.getOperatingCarrier());
				flight.setCsOCFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());

			} else {
				flight.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
			}

			int operationTypeId = GDSApiUtils.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
			flight.setOperationTypeId(operationTypeId);

			if (this.isLocalTimeMode) {
				flight.setDepartureDateLocal(startDate);
			} else {
				flight.setDepartureDate(startDate);
			}

			Set<FlightLeg> legSet = new HashSet<FlightLeg>();

			Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();
			int n = 1;
			String origin = null;
			String destination = null;
			if (legs != null && !legs.isEmpty()) {
				for (SSMASMFlightLegDTO legDTO : legs) {

					FlightLeg fScheLeg = new FlightLeg();
					fScheLeg.setOrigin(legDTO.getDepatureAirport());
					fScheLeg.setDestination(legDTO.getArrivalAirport());
					String depaturTime = legDTO.getDepatureTime();
					String arrivalTime = legDTO.getArrivalTime();
					if (origin == null) {
						origin = legDTO.getDepatureAirport();
					}
					destination = legDTO.getArrivalAirport();

					int arrivalOffset = dateVariation(startDate, legDTO.getArrivalOffset());
					int departureOffset = dateVariation(startDate, legDTO.getDepatureOffiset());
					if (this.isLocalTimeMode) {
						fScheLeg.setEstDepartureTimeLocal(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
						fScheLeg.setEstArrivalTimeLocal(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
						fScheLeg.setEstArrivalDayOffsetLocal(arrivalOffset);
						fScheLeg.setEstDepartureDayOffsetLocal(departureOffset);

					} else {
						fScheLeg.setEstDepartureTimeZulu(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
						fScheLeg.setEstArrivalTimeZulu(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
						fScheLeg.setEstArrivalDayOffset(arrivalOffset);
						fScheLeg.setEstDepartureDayOffset(departureOffset);
					}
					fScheLeg.setLegNumber(n);
					n++;
					legSet.add(fScheLeg);

				}
			}
			flight.setFlightLegs(legSet);
			updateFlightSegments(flight, legSet);
			String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(), ssmASMSubMsgsDTO.getAircraftConfiguration());
			setFlightType(origin, destination, flight);
			flight.setModelNumber(modelNo);
			flight.setViaScheduleMessaging(true);

			// set CodeShare details
			flight.setCodeShareMCFlights(GDSApiUtils.getCodeShareMCFlights(ssmASMSubMsgsDTO.getMarketingFlightNos()));

			flight.setOverlapingFlightId(getPosibleOverlapFlightId(flight, ssmASMSubMsgsDTO.getSubSupplementaryInfo()));
		} catch (Exception e) {
			log.error("error while composeFlightInfo");
			throw new ModuleException("error.compose.flight.info");
		}

		return flight;
	}

	private String getFlightSegmentCode(Flight composedFlight) {
		String segmentCode = null;
		String validSegmentCode = null;
		if (composedFlight != null) {
			int segmentLength = 0;
			Set<FlightSegement> segSet = composedFlight.getFlightSegements();
			if (segSet != null) {
				Iterator<FlightSegement> segIte = segSet.iterator();
				while (segIte.hasNext()) {
					FlightSegement segment = (FlightSegement) segIte.next();
					segmentCode = segment.getSegmentCode();
					if (segmentCode.length() > segmentLength) {
						segmentLength = segmentCode.length();
						validSegmentCode = segmentCode;
					}
				}
			}
		}

		return validSegmentCode;
	}

	private void updateAdHocFlights(Collection<Flight> flightColl, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO,
			SSMASMResponse responseSummary) throws ModuleException {
		Collection<Integer> updatedFlightIds = new ArrayList<>();
		if (flightColl != null && !flightColl.isEmpty() && responseSummary != null) {
			for (Flight flight : flightColl) {
				updateAdHocFlight(flight, ssmASMSubMsgsDTO, responseSummary, updatedFlightIds);
			}
			responseSummary.setUpdatedAdhocScheduleCount(updatedFlightIds.size());
		}
	}

	private void updateAdHocFlight(Flight flight, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, SSMASMResponse responseSummary,
			Collection<Integer> updatedFlightIds) throws ModuleException {
		if (flight != null && responseSummary != null && updatedFlightIds != null) {

			Integer flightId = flight.getFlightId();
			Date departureStartDate = flight.getDepartureDate();
			if (flight.getScheduleId() != null) {
				log.info("Invalid AdHoc Flight loaded for bulk update Dep.Date:"
						+ CalendarUtil.formatDate(departureStartDate, CalendarUtil.PATTERN1) + " Flight No:"
						+ flight.getFlightNumber());
				return;
			}

			if (this.isLocalTimeMode) {
				addLocalTimeDetailsToFlight(flight);
				departureStartDate = flight.getDepartureDateLocal();
			}

			ssmASMSubMsgsDTO.setStartDate(CalendarUtil.getStartTimeOfDate(departureStartDate));
			ssmASMSubMsgsDTO.setEndDate(CalendarUtil.getEndTimeOfDate(departureStartDate));

			Frequency fqn = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());
			boolean freqMatched = CalendarUtil.isDateWithinSelectedDayOfOperation(ssmASMSubMsgsDTO.getStartDate(), fqn);

			if (freqMatched) {
				log.info("Updating AdHoc Flight Details Dep.L.Date:"
						+ CalendarUtil.formatDate(departureStartDate, CalendarUtil.PATTERN1) + " Flight No:"
						+ flight.getFlightNumber());
				SSMASMResponse ssiMResponse = processASMSubMessage(ssmASMSubMsgsDTO);
				AdHocScheduleProcessStatus adHocScheduleProcessStatus = adaptAdHocScheduleProcessStatus(flight);
				if (ssiMResponse.isSuccess()) {
					responseSummary.setSuccess(true);
					updatedFlightIds.add(flightId);
					adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
				}
				responseSummary.addScheduleProcessStatus(adHocScheduleProcessStatus);

			} else {
				log.info("AdHoc Flight doesn't fall within the selected operation days Dep.L.Date:"
						+ CalendarUtil.formatDate(departureStartDate, CalendarUtil.PATTERN1) + " Flight No:"
						+ flight.getFlightNumber());
				return;
			}

		}

	}

	private AdHocScheduleProcessStatus adaptAdHocScheduleProcessStatus(Flight flight) {
		AdHocScheduleProcessStatus adHocScheduleProcessStatus = new AdHocScheduleProcessStatus();
		adHocScheduleProcessStatus.setFlightNumber(flight.getFlightNumber());
		adHocScheduleProcessStatus.setFlightId(flight.getFlightId());
		Date departureStartDate = flight.getDepartureDate();
		if (this.isLocalTimeMode) {
			adHocScheduleProcessStatus.setLocalTimeMode(true);
			departureStartDate = flight.getDepartureDateLocal();
		}

		adHocScheduleProcessStatus.setDepartureDate(departureStartDate);
		adHocScheduleProcessStatus.setScheduleId(flight.getScheduleId());
		adHocScheduleProcessStatus.setStatus(flight.getStatus());
		String originDestination = flight.getOriginAptCode() + "/" + flight.getDestinationAptCode();
		adHocScheduleProcessStatus.setOriginDestination(originDestination);

		return adHocScheduleProcessStatus;
	}

	private void addDetailMessageProcessedAudit(SSMASMResponse ssiMResponse) {

		if (this.isDetailAuditEnabled) {
			try {
				SSMASMSummaryAuditDTO ssmAsmAudit = new SSMASMSummaryAuditDTO();
				ssmAsmAudit.setMessageType("ASM");
				if (this.ssIMessegeDTO != null) {
					ssmAsmAudit.setInMessegeID(this.ssIMessegeDTO.getInMessegeID());
					ssmAsmAudit.setRawMessage(this.ssIMessegeDTO.getRawMessage());

				}
				ssmAsmAudit.setUserID(ssIMessegeDTO.getProcessingUserID());
				ssmAsmAudit.setTotalUpdatedSchedules(ssiMResponse.getUpdatedScheduleCount());
				ssmAsmAudit.setTotalUpdatedFlights(ssiMResponse.getUpdatedAdhocScheduleCount());

				StringBuilder sb = new StringBuilder();

				Collection<SSMMessageProcessStatus> messageProcessStatus = ssiMResponse.getScheduleProcessStatus();
				if (messageProcessStatus != null && !messageProcessStatus.isEmpty()) {
					for (SSMMessageProcessStatus processStatus : messageProcessStatus) {
						if (processStatus instanceof AdHocScheduleProcessStatus) {
							sb.append("\n" + (AdHocScheduleProcessStatus) processStatus);
							sb.append("\n");

						} else if (processStatus instanceof ScheduleProcessStatus) {

							sb.append("\n" + (ScheduleProcessStatus) processStatus);
							sb.append("\n");
						}

					}

				}
				ssmAsmAudit.setSuccess(ssiMResponse.isSuccess());
				ssmAsmAudit.setResponseCode(ssiMResponse.getResponseMessage());
				ssmAsmAudit.setUpdatedScheduleDetails(sb.toString());

				ProcSSMASMAudit.addScheduleMessageProcessedSummary(ssmAsmAudit);

			} catch (Exception e) {
				log.error("ERROR @ addDetailMessageProcessedAudit" + e.getMessage());
			}
		}

	}
}
