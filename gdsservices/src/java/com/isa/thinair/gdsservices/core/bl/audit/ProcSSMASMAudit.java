package com.isa.thinair.gdsservices.core.bl.audit;

import java.util.Date;
import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.utils.ProcStdScheduleAuditBase;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMAuditDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSummaryAuditDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

public class ProcSSMASMAudit extends ProcStdScheduleAuditBase {
	private static Log log = LogFactory.getLog(ProcSSMASMAudit.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void doAuditProcSSMASM(SSMASMAuditDTO ssmAuditDTO) {

		try {
			if (ssmAuditDTO != null && ssmAuditDTO.getSsmASMSubMsgsDTO() != null) {
				String userID = ssmAuditDTO.getUserID();
				Integer inMessegeID = ssmAuditDTO.getInMessegeID();
				Integer scheduleId = ssmAuditDTO.getScheduleId();
				Operation operation = ssmAuditDTO.getOperation();
				String responseCode = ssmAuditDTO.getResponseCode();
				boolean success = ssmAuditDTO.isSuccess();
				String rawMessage = ssmAuditDTO.getRawMessage();

				Audit audit = new Audit();
				audit.setTimestamp(new Date());
				LinkedHashMap contents = new LinkedHashMap();

				if (userID != null) {
					audit.setUserId(userID);
				} else {
					audit.setUserId("");
				}

				String entityIDtxt = "";

				SSMASMSUBMessegeDTO ssmASMSubMsgsDTO = ssmAuditDTO.getSsmASMSubMsgsDTO();
				if (GDSExternalCodes.MessageType.SSM.equals(ssmASMSubMsgsDTO.getMessageType())) {
					audit.setTaskCode(String.valueOf(TasksUtil.PROCESS_SSM));
					entityIDtxt = "schedule id";
				} else if (GDSExternalCodes.MessageType.ASM.equals(ssmASMSubMsgsDTO.getMessageType())) {
					audit.setTaskCode(String.valueOf(TasksUtil.PROCESS_ASM));
					entityIDtxt = "flight id";
				}

				contents.put("action", nullHandler(ssmASMSubMsgsDTO.getAction()));
				contents.put("in messege id", nullHandler(inMessegeID));
				contents.put("flight designator", nullHandler(ssmASMSubMsgsDTO.getFlightDesignator()));
				contents.put("start date", nullHandler(formatDate(ssmASMSubMsgsDTO.getStartDate())));
				contents.put("end date", nullHandler(formatDate(ssmASMSubMsgsDTO.getEndDate())));
				contents.put(entityIDtxt, nullHandler(scheduleId));
				contents.put("subMessage", nullHandler(rawMessage));

				if (success) {
					contents.put("status", ProcStdScheduleAuditBase.nullHandler(getStatus(Status.SUCCESS, operation)));
					contents.put("remarks", nullHandler(responseCode));
				} else {
					contents.put("status", ProcStdScheduleAuditBase.nullHandler(getStatus(Status.FAILD, operation)));
					contents.put("remarks", nullHandler(responseCode));
				}

				GDSServicesModuleUtil.getAuditorBD().audit(audit, contents);
			}
		} catch (Exception e) {
			log.error("error while saving audit " + e.getMessage());
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addScheduleMessageProcessedSummary(SSMASMSummaryAuditDTO ssmAsmAudit) {

		try {
			if (ssmAsmAudit != null) {

				String userID = ssmAsmAudit.getUserID();
				Audit audit = new Audit();
				audit.setTimestamp(new Date());
				audit.setTaskCode(String.valueOf(TasksUtil.SCHEDULE_MSG_PROCESSING_DETAILS));

				LinkedHashMap contents = new LinkedHashMap();
				if (userID != null) {
					audit.setUserId(userID);
				} else {
					audit.setUserId("");
				}
				contents.put("type", nullHandler(ssmAsmAudit.getMessageType()));
				contents.put("messege id", nullHandler(ssmAsmAudit.getInMessegeID()));
				contents.put("total schedules", nullHandler(ssmAsmAudit.getTotalUpdatedSchedules()));
				contents.put("total flights", nullHandler(ssmAsmAudit.getTotalUpdatedFlights()));
				contents.put("schedule details", nullHandler(ssmAsmAudit.getUpdatedScheduleDetails()));
				contents.put("message", nullHandler(ssmAsmAudit.getRawMessage()));

				if (ssmAsmAudit.isSuccess()) {
					contents.put("status", Status.SUCCESS.toString());
					contents.put("remarks", "");
				} else {
					contents.put("status", Status.FAILD.toString());
					contents.put("remarks", nullHandler(ssmAsmAudit.getResponseCode()));
				}

				GDSServicesModuleUtil.getAuditorBD().audit(audit, contents);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("error while saving addScheduleMessageProcessedSummary " + e.getMessage());
		}
	}
}
