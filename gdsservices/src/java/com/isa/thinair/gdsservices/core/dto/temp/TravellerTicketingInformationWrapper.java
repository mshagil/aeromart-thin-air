package com.isa.thinair.gdsservices.core.dto.temp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TicketingAgentInformation;
import com.isa.typea.common.TravellerTicketingInformation;

public class TravellerTicketingInformationWrapper extends TravellerInformationWrapper
		implements Serializable {

	private TicketingAgentInformation ticketingAgentInformation;
	private OriginatorInformation originatorInformation;

	private List<TicketNumberDetails> couponTransitions;

	public TravellerTicketingInformationWrapper(TravellerTicketingInformation travellerTicketingInformation) {
		super(travellerTicketingInformation);
		couponTransitions = new ArrayList<TicketNumberDetails>();

	}

	public TravellerTicketingInformation getTravellerInformation() {
		return (TravellerTicketingInformation) super.getTravellerInformation();
	}

	public List<TicketNumberDetails> getCouponTransitions() {
		return couponTransitions;
	}

	public TicketingAgentInformation getTicketingAgentInformation() {
		return ticketingAgentInformation;
	}

	public void setTicketingAgentInformation(TicketingAgentInformation ticketingAgentInformation) {
		this.ticketingAgentInformation = ticketingAgentInformation;
	}

	public OriginatorInformation getOriginatorInformation() {
		return originatorInformation;
	}

	public void setOriginatorInformation(OriginatorInformation originatorInformation) {
		this.originatorInformation = originatorInformation;
	}
}
