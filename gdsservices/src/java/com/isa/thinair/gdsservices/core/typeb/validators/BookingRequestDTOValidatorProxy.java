package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageIdentifier;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.typeb.extractors.AdviceScheduleChangeRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.CancelSegmentRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.ConfirmBookingRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.CreateRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.CreateReservationResponseExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.LocateReservationRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.ModifyReservationRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.ReservationResponseExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.SplitReservationRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.SplitReservationResponseExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.UpdateExternalSegmentsRequestExtractor;
import com.isa.thinair.gdsservices.core.typeb.extractors.UpdateStatusRequestExtractor;

/**
 * @author Nilindra Fernando
 */
public class BookingRequestDTOValidatorProxy {

	public static BookingRequestDTO validate(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {
		Collection<ValidatorBase> colCommonValidators = getCommonValidators(gdsCredentialsDTO, bookingRequestDTO);
		Collection<ValidatorBase> colDerivedValidators = getDerivedValidators(gdsCredentialsDTO, bookingRequestDTO);

		Collection<ValidatorBase> colAllValidators = new ArrayList<ValidatorBase>();
		colAllValidators.addAll(colCommonValidators);
		colAllValidators.addAll(colDerivedValidators);

		return executor(bookingRequestDTO, gdsCredentialsDTO, colAllValidators);
	}

	private static Collection<ValidatorBase> getCommonValidators(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {
		return GDSServicesModuleUtil.getValidationsImplConfig().getCommonValidators();
	}

	private static Collection<ValidatorBase> getDerivedValidators(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {
		Collection<ValidatorBase> colValidatorBase = new ArrayList<ValidatorBase>();
		String messageIdentifier = bookingRequestDTO.getMessageIdentifier();

		if (messageIdentifier.equals(MessageIdentifier.LOCATE_RESERVATION.getCode())) {
			colValidatorBase.add(new LocateReservationRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.PDM.getCode())) {
			colValidatorBase.add(new LocateReservationRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.NEW_BOOKING.getCode())) {
			colValidatorBase.add(new CreateRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.CANCEL_BOOKING.getCode())) {
			colValidatorBase.add(new CancelSegmentRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.NEW_CONTINUATION.getCode())
				|| messageIdentifier.equals(MessageIdentifier.NEW_ARRIVAL.getCode())
				|| messageIdentifier.equals(MessageIdentifier.ARRIVAL_UNKNOWN.getCode())) {
			colValidatorBase.add(new UpdateExternalSegmentsRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.AMEND_BOOKING.getCode())) {
			colValidatorBase.add(new ModifyReservationRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.DEVIDE_BOOKING.getCode())) {
			colValidatorBase.add(new SplitReservationRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.STATUS_REPLY.getCode())) {
			colValidatorBase.add(new UpdateStatusRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.TICKETING.getCode())) {
			colValidatorBase.add(new ConfirmBookingRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.SCHEDULE_CHANGE.getCode())) {
			colValidatorBase.add(new AdviceScheduleChangeRequestExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.DIVIDE_RESP.getCode())) {
			colValidatorBase.add(new SplitReservationResponseExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.LINK_RECORD_LOCATOR.getCode())) {
			colValidatorBase.add(new CreateReservationResponseExtractor());
		} else if (messageIdentifier.equals(MessageIdentifier.RESERVATION_RESP.getCode())) {
			colValidatorBase.add(new ReservationResponseExtractor());
		} else {
			colValidatorBase.add(new ValidateInvalidMessage());
		}
		return colValidatorBase;
	}

	private static BookingRequestDTO executor(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO,
			Collection<ValidatorBase> colValidatorBase) {
		for (ValidatorBase validatorBase : colValidatorBase) {
			bookingRequestDTO = validatorBase.validate(bookingRequestDTO, gdsCredentialsDTO);

			if (validatorBase.getStatus() == GDSInternalCodes.ValidateConstants.INVALID) {
				bookingRequestDTO.setProcessStatus(GDSExternalCodes.ProcessStatus.GENERAL_FAILURE);
				return bookingRequestDTO;
			} else if (validatorBase.getStatus() == GDSInternalCodes.ValidateConstants.FAILURE) {
				bookingRequestDTO.setProcessStatus(GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE);
				return bookingRequestDTO;
			}
		}

		bookingRequestDTO.setProcessStatus(GDSExternalCodes.ProcessStatus.VALIDATE_SUCCESS);
		return bookingRequestDTO;
	}
}
