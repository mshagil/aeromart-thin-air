package com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03;

import iata.typea.v031.itareq.ITAREQ;

import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.core.typeA.transformers.EDIFACTMessageProcess;

public class InventoryAdjustment implements EDIFACTMessageProcess {

	@Override
	public Map<String, Object> extractSpecificCommandParams(Object message) {
		ITAREQ itareq = (ITAREQ) message;
		return InventoryAdjustmentReqestHandler.getOwnMessageParams(itareq);
	}

	@Override
	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) {
		return InventoryAdjustmentResponseHandler.constructEDIResponce(response);
	}

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return null;
	}

}
