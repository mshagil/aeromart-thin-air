package com.isa.thinair.gdsservices.core.bl.internal.ticket;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRs;

public interface TicketIssuanceHandler {

	TicketingEventRs handleMonetaryDiscrepancies(TicketingEventRq ticketIssueRq) throws ModuleException;

	TicketingEventRs handleMonetaryDiscrepancies(TicketsAssembler ticketsAssembler) throws ModuleException;


}
