package com.isa.thinair.gdsservices.core.bl.avs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.PubAvailStatusUpdateRQ;
import com.isa.thinair.airinventory.api.dto.PublishBCAvailMsgDTO;
import com.isa.thinair.airinventory.api.dto.PublishSegAvailMsgDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.AvsGenerationFlow;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventCode;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.PublishInventoryEventStatus;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.msgbroker.api.dto.AVSMessageDTO;
import com.isa.thinair.msgbroker.api.dto.AVSRbdDTO;
import com.isa.thinair.msgbroker.api.dto.AVSSegmentDTO;
import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSRbdEventCode;
import com.isa.thinair.msgbroker.api.util.AVSConstants.AVSSegmentEventCode;


public class PublishAvailability {

	private final Log log = LogFactory.getLog(getClass());

	FlightInventoryBD flightInventoryBD;

	private CommonMasterBD commonMasterBD;

	HashMap<Integer, String> gdsCodes;

	public PublishAvailability() {
		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();
		commonMasterBD = GDSServicesModuleUtil.getCommonMasterBD();

	}

	public void publishAvailabilityMessages() throws ModuleException {
		Object[] avsDTOs = prepareAVSDTOs();

		Map<Integer, List<AVSSegmentDTO>> avsDetailsMap = (Map<Integer, List<AVSSegmentDTO>>) avsDTOs[0];
		Map<Integer, List<PubAvailStatusUpdateRQ>> pubAvailStatusUpdateRQsMap = (Map<Integer, List<PubAvailStatusUpdateRQ>>) avsDTOs[1];

		if (avsDetailsMap != null) {
			gdsCodes = commonMasterBD.getGdsCodes();
			String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			for (Integer gdsId : avsDetailsMap.keySet()) {
				List<AVSSegmentDTO> avsSegmentDTOs = avsDetailsMap.get(gdsId);
				List<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQs = pubAvailStatusUpdateRQsMap.get(gdsId);

				int total = avsSegmentDTOs.size();
				int maxFlightsPerReq = 10;

				for (int i = 0; i < (total / maxFlightsPerReq) + 1; ++i) {
					if (i * maxFlightsPerReq >= total)
						break;
					List<AVSSegmentDTO> avsSegmentDTOsChunck = new ArrayList<AVSSegmentDTO>();
					List<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQsChunck = new ArrayList<PubAvailStatusUpdateRQ>();

					for (int j = i * maxFlightsPerReq; j < (i + 1) * maxFlightsPerReq; ++j) {
						avsSegmentDTOsChunck.add(avsSegmentDTOs.get(j));
						pubAvailStatusUpdateRQsChunck.add(pubAvailStatusUpdateRQs.get(j));
						if (j == total - 1)
							break;
					}

					AVSMessageDTO avsMessageDTO = new AVSMessageDTO();
					avsMessageDTO.setAVSSegmentDTOs(avsSegmentDTOsChunck);
					try {
						log.info("Start publishing AVS segments to message broker [GDSId=" + gdsId + ", Number of AVS Segs ="
								+ avsSegmentDTOsChunck.size() + "]");
						GDSServicesModuleUtil.getAVSServiceBD().sendAVSMessages(gdsCodes.get(gdsId), gdsId, airLineCode,
								avsMessageDTO);
						pubAvailStatusUpdate(pubAvailStatusUpdateRQsChunck, PublishInventoryEventStatus.EVENT_PUBLISHED);

					} catch (Exception ex) {
						log.error("Publishing Availability Messages to Message Broker Failed", ex);
						pubAvailStatusUpdate(pubAvailStatusUpdateRQsChunck, PublishInventoryEventStatus.EVENT_PUBLISH_FAILED);
					}
				}
			}
		}
	}

	private Object[] prepareAVSDTOs() throws ModuleException {
		Map<Integer, Collection<PublishSegAvailMsgDTO>> publishAvailMsgDTOsMap = flightInventoryBD.getAvailabilityInfoToPublish();
		Map<Integer, List<PubAvailStatusUpdateRQ>> pubAvailStatusUpdateRQsMap = null;

		Map<Integer, List<AVSSegmentDTO>> avsSegmentDTOsMap = null;
		Collection<AVSSegmentDTO> avsSegmentDTOs = null;
		Collection<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQs = null;

		if (publishAvailMsgDTOsMap != null && publishAvailMsgDTOsMap.size() > 0) {
			for (Integer gdsId : publishAvailMsgDTOsMap.keySet()) {
				List<String> ignoreBookingClassList = avsIgnoreBookingClassList(gdsId);
				Collection<PublishSegAvailMsgDTO> publishAvailMsgDTOs = publishAvailMsgDTOsMap.get(gdsId);

				for (PublishSegAvailMsgDTO publishSegAvailMsgDTO : publishAvailMsgDTOs) {
					if (avsSegmentDTOsMap == null) {
						avsSegmentDTOsMap = new HashMap<Integer, List<AVSSegmentDTO>>();
						pubAvailStatusUpdateRQsMap = new HashMap<Integer, List<PubAvailStatusUpdateRQ>>();
					}

					if (!avsSegmentDTOsMap.containsKey(gdsId)) {
						avsSegmentDTOsMap.put(gdsId, new ArrayList<AVSSegmentDTO>());
					}

					if (!pubAvailStatusUpdateRQsMap.containsKey(gdsId)) {
						pubAvailStatusUpdateRQsMap.put(gdsId, new ArrayList<PubAvailStatusUpdateRQ>());
					}

					avsSegmentDTOs = avsSegmentDTOsMap.get(gdsId);
					pubAvailStatusUpdateRQs = pubAvailStatusUpdateRQsMap.get(gdsId);

					AVSSegmentDTO avsSegmentDTO = new AVSSegmentDTO();
					avsSegmentDTOs.add(avsSegmentDTO);

					PubAvailStatusUpdateRQ pubAvailStatusUpdateRQ = new PubAvailStatusUpdateRQ();
					pubAvailStatusUpdateRQs.add(pubAvailStatusUpdateRQ);

					avsSegmentDTO.setFlightDesignator(publishSegAvailMsgDTO.getFlightNumber());
					avsSegmentDTO.setDepartureDate(publishSegAvailMsgDTO.getDepartureDateLocal());
					avsSegmentDTO.setDepartureStation(publishSegAvailMsgDTO.getOriginAirport());
					avsSegmentDTO.setDestinationStation(publishSegAvailMsgDTO.getDestinationAirport());
					avsSegmentDTO.setSeatsAvailable(publishSegAvailMsgDTO.getSeatsAvailable());

					if (publishSegAvailMsgDTO.getSegmentEventCode() != null) {
						avsSegmentDTO.setAVSSegmentEventCode(getAVSSegmentEventCode(publishSegAvailMsgDTO.getSegmentEventCode()));

						pubAvailStatusUpdateRQ.setPublishAvailabilityId(publishSegAvailMsgDTO.getPublishAvailabilityId());
					} else if (publishSegAvailMsgDTO.getBcAvailabilityDTOs() != null
							&& publishSegAvailMsgDTO.getBcAvailabilityDTOs().size() > 0) {
						Collection<AVSRbdDTO> avsRbdDTOs = new ArrayList<AVSRbdDTO>();
						RBDLP: for (PublishBCAvailMsgDTO publishBCAvailMsgDTO : publishSegAvailMsgDTO.getBcAvailabilityDTOs()) {
							if (ignoreBookingClassList != null && !ignoreBookingClassList.isEmpty()
									&& ignoreBookingClassList.contains(publishBCAvailMsgDTO.getBookingCode())) {
								continue RBDLP;
							}
							AVSRbdDTO avsRbdDTO = new AVSRbdDTO();
							avsRbdDTOs.add(avsRbdDTO);
							avsRbdDTO.setBookingCode(publishBCAvailMsgDTO.getBookingCode());
							avsRbdDTO.setAvsRbdEventCode(getAVSRbdEventCode(publishBCAvailMsgDTO.getBcEventCode()));
							avsRbdDTO.setNumericAvailability(publishBCAvailMsgDTO.getSeatsAvailable());
							avsRbdDTO.setBcSeatsAvailable(publishBCAvailMsgDTO.getBcSeatsAvailable());

							pubAvailStatusUpdateRQ.setPublishAvailabilityId(publishBCAvailMsgDTO.getPublishAvailabilityId());
						}
						avsSegmentDTO.setAvsRbdDTOs(avsRbdDTOs);
						if (avsRbdDTOs.isEmpty()) {
							avsSegmentDTOs.remove(avsSegmentDTO);
							pubAvailStatusUpdateRQs.remove(pubAvailStatusUpdateRQ);
						}
					} else {
						throw new ModuleException("gdsservices.publish.availability.invalid.data",
								"Invalid publish availability request");
					}
				}
			}
		}

		Object[] results = { avsSegmentDTOsMap, pubAvailStatusUpdateRQsMap };

		return results;
	}

	private void pubAvailStatusUpdate(Collection<PubAvailStatusUpdateRQ> pubAvailStatusUpdateRQs,
			PublishInventoryEventStatus publishInventoryEventStatus) throws ModuleException {

		if (pubAvailStatusUpdateRQs != null) {
			for (PubAvailStatusUpdateRQ pubAvailStatusUpdateRQ : pubAvailStatusUpdateRQs) {
				pubAvailStatusUpdateRQ.setPublishInventoryEventStatus(publishInventoryEventStatus);
			}
		}

		flightInventoryBD.publishAvailabiltyUpdateStatus(pubAvailStatusUpdateRQs);
	}

	private AVSSegmentEventCode getAVSSegmentEventCode(PublishInventoryEventCode inventoryEventCode) throws ModuleException {
		AVSSegmentEventCode avSegmentEventCode = null;

		switch (inventoryEventCode) {
//		case AVS_CREATE_FLIGHT:
//			avSegmentEventCode = AVSSegmentEventCode.AVS_CREATE_FLIGHT;
//			break;
//		case AVS_PUBLISH_FLIGHT:
//			avSegmentEventCode = AVSSegmentEventCode.AVS_PUBLISH_FLIGHT;
//			break;
		case AVS_UNPUBLISED_FLIGHT:
			avSegmentEventCode = AVSSegmentEventCode.AVS_UNPUBLISED_FLIGHT;
			break;
		case AVS_FLIGHT_CLOSED:
			avSegmentEventCode = AVSSegmentEventCode.AVS_FLIGHT_CLOSED;
			break;
		case AVS_CANCEL_FLIGHT:
			avSegmentEventCode = AVSSegmentEventCode.AVS_CANCEL_FLIGHT;
			break;
		case AVS_FLIGHT_REOPEN:
			avSegmentEventCode = AVSSegmentEventCode.AVS_FLIGHT_REOPEN;
			break;
			
		default:
			throw new ModuleException("gdsservices.publish.availability.invalid.segevent",
					"Invalid Publish Availability Segment Event");
		}

		return avSegmentEventCode;
	}

	private AVSRbdEventCode getAVSRbdEventCode(PublishInventoryEventCode inventoryEventCode) throws ModuleException {
		AVSRbdEventCode avsRbdEventCode;

		switch (inventoryEventCode) {
		case AVS_CREATE_FLIGHT:
			avsRbdEventCode = AVSRbdEventCode.AVS_CREATE_FLIGHT;
			break;
		case AVS_PUBLISH_FLIGHT:
			avsRbdEventCode = AVSRbdEventCode.AVS_PUBLISH_FLIGHT;
			break;
		case AVS_SEG_AVAILABILITY_CHANGE:
			avsRbdEventCode = AVSRbdEventCode.AVS_SEG_AVAILABILITY_CHANGE;
			break;
		case AVS_RBD_AVAILABILITY_CHANGE:
			avsRbdEventCode = AVSRbdEventCode.AVS_RBD_AVAILABILITY_CHANGE;
			break;
		case AVS_RBD_SEG_AVAILABILITY_CHANGE:
			avsRbdEventCode = AVSRbdEventCode.AVS_RBD_SEG_AVAILABILITY_CHANGE;
			break;
		case AVS_RBD_SEG_AVAILABILITY_RESET:
			avsRbdEventCode = AVSRbdEventCode.AVS_RBD_SEG_AVAILABILITY_RESET;
			break;
		case AVS_AVAIL_BELOW_TRESH:
			avsRbdEventCode = AVSRbdEventCode.AVS_AVAIL_BELOW_TRESH;
			break;
		case AVS_AVAIL_ABOVE_THRESHOLD:
			avsRbdEventCode = AVSRbdEventCode.AVS_AVAIL_ABOVE_THRESHOLD;
			break;
		case AVS_RBD_CLOSED:
		case AVS_INACTIVATE_BC:
			avsRbdEventCode = AVSRbdEventCode.AVS_RBD_CLOSED;
			break;
		case AVS_RBD_REOPEN:
		case AVS_ACTIVATE_BC:
			avsRbdEventCode = AVSRbdEventCode.AVS_RBD_REOPEN;
			break;
		case AVS_RBD_REQUEST:
			avsRbdEventCode = AVSRbdEventCode.AVS_RBD_REQUEST;
			break;
		default:
			throw new ModuleException("gdsservices.publish.availability.invalid.rbdevent",
					"Invalid Publish Availability RBD Event");
		}

		return avsRbdEventCode;
	}
	
	private static List<String> avsIgnoreBookingClassList(Integer gdsId) {		
		String gdsIdStr = gdsId.toString();
		GDSServicesModuleConfig gdsModuleconfig = GDSServicesModuleUtil.getConfig();
		Map<Integer, List<String>> avsSkipPublishBookingCodes = gdsModuleconfig.getAvsSkipPublishBookingCodes();

		if (avsSkipPublishBookingCodes != null && !avsSkipPublishBookingCodes.isEmpty()
				&& avsSkipPublishBookingCodes.get(gdsIdStr) != null && !avsSkipPublishBookingCodes.get(gdsIdStr).isEmpty()) {
			return avsSkipPublishBookingCodes.get(gdsIdStr);
		}
		return null;
	}

	private List<String> getInactiveGdsBookingClasses() {
		return AirInventoryModuleUtils.getBookingClassDAO().getInactiveGdsBookingClasse();

	}

}
