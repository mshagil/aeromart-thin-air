package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketDisplayRq;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageInitiator;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.dto.EDIMessageMetaDataDTO;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.typea.common.Header;
import com.isa.typea.common.Location;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.SystemDetails;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;

public class InitiateDisplayTicket extends EdiMessageInitiator {

	private static final Log log = LogFactory.getLog(InitiateDisplayTicket.class);

	private TicketDisplayRq ticketDisplayRq;

	@Override
	protected void prepareMessages() throws ModuleException {
		EDIMessageDTO ediMessageDTO ;

		ticketDisplayRq = (TicketDisplayRq)getParameter(TypeACommandParamNames.TICKET_DTO);
		Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons = ticketDisplayRq.getPaxTicketsCoupons();
		Map<String, List<Integer>> ticketsCoupons;

		for (Integer paxSeq : paxTicketsCoupons.keySet()) {
			ticketsCoupons = paxTicketsCoupons.get(paxSeq);

			for (String ticketNumber : ticketsCoupons.keySet()) {
				
				OriginatorInformation originatorInformation = GDSDTOUtil.createOriginatorInformation();
				ediMessageDTO = new EDIMessageDTO(TypeAConstants.MessageFlow.INITIATE);

				MessageHeader messageHeader = GDSDTOUtil.createInitMessageHeader(IATAOperation.TKTREQ, ticketDisplayRq.getGdsId(), null);
				Header header = GDSDTOUtil.createInitEdiHeader(ticketDisplayRq.getGdsId(), messageHeader, null);

				AATKTREQ tktReq = new AATKTREQ();

				tktReq.setHeader(header);
				
				TKTREQMessage tktreqMessage = new TKTREQMessage();
				tktreqMessage.setMessageHeader(messageHeader);

				MessageFunction messageFunction = new MessageFunction();
				messageFunction.setMessageFunction(TypeAConstants.MessageFunction.DISPLAY);
				tktreqMessage.setMessageFunction(messageFunction);
				
				Location location = new Location();
				location.setLocationCode(AppSysParamsUtil.getHubAirport());
				
				SystemDetails systemDetails = new SystemDetails();
				systemDetails.setCompanyCode(AppSysParamsUtil.getDefaultCarrierCode());
				systemDetails.setLocation(location);
				originatorInformation.setSenderSystemDetails(systemDetails);
				originatorInformation.setAgentCurrencyCode(AppSysParamsUtil.getBaseCurrency());
				
				//system ?
				Gds gds = GdsUtil.getGds(ticketDisplayRq.getGdsId());
				originatorInformation.setOriginatorAuthorityCode("SYSTEM");
			
				originatorInformation.setAgentId(AppSysParamsUtil.getGdsConnectivityAgent(gds.getGdsCode()));
				
				tktreqMessage.setOriginatorInformation(originatorInformation);
				
				List<TicketNumberDetails> tickets =  tktreqMessage.getTickets();
				TicketNumberDetails ticketNumberDetails = new TicketNumberDetails();
				ticketNumberDetails.setTicketNumber(ticketNumber);
				ticketNumberDetails.setDocumentType(TypeAConstants.DocumentType.TICKET);
				tickets.add(ticketNumberDetails);

				ediMessageDTO.setRequest(tktReq);

				EDIMessageMetaDataDTO metaDataDTO = new EDIMessageMetaDataDTO();
				metaDataDTO.setIataOperation(iataOperation);
				metaDataDTO.setMajorVersion(messageHeader.getMessageId().getVersion());
				metaDataDTO.setMinorVersion(messageHeader.getMessageId().getReleaseNumber());
				metaDataDTO.setMessageReference(messageHeader.getCommonAccessReference());
				ediMessageDTO.setRequestMetaDataDTO(metaDataDTO);

				ediMessageDTO.setReqAssociationCode(header.getInterchangeHeader().getAssociationCode());
				ediMessageDTO.setGdsCode(gds.getGdsCode());
				
				ediMessageDTOs.add(ediMessageDTO);
			}
		}

	}

	@Override
	protected void processResponse() {



	}
}
