package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getValue;
import iata.typea.v031.tkcreq.GROUP1;
import iata.typea.v031.tkcreq.RCI;
import iata.typea.v031.tkcreq.TKCREQ;
import iata.typea.v031.tkcreq.TKT;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.model.RDD;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * 
 * @author sanjaya
 */
public class TicketDisplayRequestHandler {

	public static Map<String, Object> extractCommandParams(TKCREQ tkcreq) throws InteractiveEDIException {

		try {

			Map<String, Object> params = new HashMap<String, Object>();

			String messageFunction = getValue(tkcreq.getMSG(), RDD.C, 1, 2);
			// add message function required to build the responce
			params.put(TypeACommandParamNames.MESSAGE_FUNCTION, messageFunction);

			// search BY_ETICKET
			if (tkcreq.getGROUP1() != null) {
				List<GROUP1> group1List = tkcreq.getGROUP1();
				GROUP1 firstGroup = PlatformUtiltiies.getFirstElement(group1List);
				if (firstGroup != null) {
					TKT tkt = firstGroup.getTKT();
					String eticketNumber = getValue(tkt, RDD.M, 1, 1);
					ITicketDisplayRequest searchReq = new SearchByEticket(eticketNumber);
					params.put(TypeACommandParamNames.TICKET_SEARCH, searchReq);
					params.put(TypeACommandParamNames.TICKET_DISPLAY_SEARCH_CRITERIA, TicketDisplayRequestCriteria.BY_ETICKET);
					return params;
				}
			}

			if (tkcreq.getRCI() != null) {
				RCI rci = tkcreq.getRCI();
			}

			return params;
		} catch (Exception e) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.UNSPECIFIED_ERROR, "unknown error occour");
		}

	}
}
