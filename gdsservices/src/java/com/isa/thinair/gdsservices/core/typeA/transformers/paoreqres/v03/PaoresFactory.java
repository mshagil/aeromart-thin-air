package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.setValue;
import iata.typea.v031.paores.APD;
import iata.typea.v031.paores.ERC;
import iata.typea.v031.paores.ERC.ERC01;
import iata.typea.v031.paores.IFT;
import iata.typea.v031.paores.IFT.IFT01;
import iata.typea.v031.paores.MSG;
import iata.typea.v031.paores.ODI;
import iata.typea.v031.paores.ObjectFactory;
import iata.typea.v031.paores.PDI;
import iata.typea.v031.paores.TVL;
import iata.typea.v031.paores.Type0029;
import iata.typea.v031.paores.Type0051;
import iata.typea.v031.paores.Type0065;
import iata.typea.v031.paores.UNB;
import iata.typea.v031.paores.UNH;
import iata.typea.v031.paores.UNT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil;
import com.isa.thinair.gdsservices.core.util.RandomANGen;
import com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill;

public class PaoresFactory {

	private static final PaoresFactory factory = new PaoresFactory();
	private static final ObjectFactory objectFactory = new ObjectFactory();

	private PaoresFactory() {
		super();
	}

	public static PaoresFactory getInstance() {
		return factory;
	}

	// TODO is there a way to have a common method for all the responses.
	public UNB createUNB(InterchangeHeader interchangeHeader, String interchangeControllRef) throws InteractiveEDIException {
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNB unb = objectFactory.createUNB();
		// syntax id and version

		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxIdentifier(), 1, 1);
		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxVersion(), 1, 2);

		InterchangeInfo replySender = interchangeHeader.getRecipient();
		InterchangeInfo recipient = interchangeHeader.getSender();

		// sender info
		setValue(objectFactory, unb, replySender.getInterchangeId(), 2, 1);
		setValue(objectFactory, unb, replySender.getInterchangeCode(), 2, 3);

		// recipient info
		setValue(objectFactory, unb, recipient.getInterchangeId(), 3, 1);
		setValue(objectFactory, unb, recipient.getInterchangeCode(), 3, 3);

		// This date and time need to be given in GMT
		String[] dateAsString = IATADataExtractionUtil.getDateAsString(new Date(), true);
		setValue(objectFactory, unb, dateAsString[0], 4, 1);
		setValue(objectFactory, unb, dateAsString[1], 4, 2);

		// interchange controll ref of sender

		setValue(objectFactory, unb, interchangeControllRef, 5);

		// recipient ref
		setValue(objectFactory, unb, interchangeHeader.getRecipientRefAssociationId(), 6, 1);
		setValue(objectFactory, unb, interchangeHeader.getRecipientRefSequenceNo(), 6, 2);

		// set association codeunb
		setValue(objectFactory, unb, Type0029.T, 8);

		return unb;
	}

	public UNH createUNH(MessageHeader messageHeader) throws InteractiveEDIException {
		IATAOperation messageType = messageHeader.getMessageIdentifier().getMessageType().getResponse();
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNH unh = objectFactory.createUNH();

		// Message ref number . Allways 1
		setValue(objectFactory, unh, String.valueOf(1), 1);

		// Message type and version
		setValue(objectFactory, unh, Type0065.valueOf(messageType.toString()), 2, 1);
		setValue(objectFactory, unh, typeAMessageConfig.getMessageVersoins(messageType.toString()), 2, 2);
		setValue(objectFactory, unh, typeAMessageConfig.getMessageRelease(messageType.toString()), 2, 3);
		setValue(objectFactory, unh, Type0051.IA, 2, 4);

		// common access reference
		String carRemote = messageHeader.getCommonAccessReference();
		String carOwn = new RandomANGen(14).nextString("");
		String car = carRemote.concat("/").concat(carOwn);
		setValue(objectFactory, unh, car, 3);

		return unh;
	}

	public MSG createMSG(String functionCode) throws InteractiveEDIException {
		MSG msg = objectFactory.createMSG();
		setValue(objectFactory, msg, functionCode, 1, 2);
		return msg;
	}

	public ODI createODI() {
		ODI odi = objectFactory.createODI();
		return odi;
	}

	public TVL createTVL(Date departureDate, Date arrivalDate, String from, String to, String carrier, String productId)
			throws InteractiveEDIException {
		return createTVL(departureDate, arrivalDate, from, to, carrier, productId, null, null);
	}

	public TVL createTVL(Date departureDate, Date arrivalDate, String from, String to, String carrier, String productId,
			String sequenceNumber, String lineItemNumber) throws InteractiveEDIException {

		TVL tvl = objectFactory.createTVL();

		// departure and arrival dates
		String[] depDateAsString = IATADataExtractionUtil.getDateAsString(departureDate, false);
		setValue(objectFactory, tvl, depDateAsString[0], 1, 1);
		setValue(objectFactory, tvl, new BigDecimal(depDateAsString[1]), 1, 2);

		String[] arrivalDateAsString = IATADataExtractionUtil.getDateAsString(arrivalDate, false);
		setValue(objectFactory, tvl, arrivalDateAsString[0], 1, 3);
		setValue(objectFactory, tvl, new BigDecimal(arrivalDateAsString[1]), 1, 4);

		// from to
		setValue(objectFactory, tvl, from, 2, 1);
		setValue(objectFactory, tvl, to, 3, 1);

		// carrier code
		setValue(objectFactory, tvl, carrier, 4, 1);

		// productId
		setValue(objectFactory, tvl, productId, 5, 1);

		// product Details
		setValue(objectFactory, tvl, productId, 5, 1);
		// sequenceNumber
		setValue(objectFactory, tvl, sequenceNumber, 6, 1);

		// lineItemNumber
		if (lineItemNumber != null && !lineItemNumber.isEmpty()) {
			setValue(objectFactory, tvl, new BigDecimal(lineItemNumber), 7);
		}
		return tvl;
	}

	/**
	 * @param cabinClassAvailability
	 *            Map<cabinClassCode, availabilityStatus>
	 * @return
	 * @throws InteractiveEDIException
	 */
	public PDI createPDI(Map<String, String> cabinClassAvailability) throws InteractiveEDIException {
		PDI pdi = objectFactory.createPDI();

		for (Entry<String, String> entry : cabinClassAvailability.entrySet()) {
			String cabinClass = entry.getKey();
			String availability = entry.getValue();

			PDI.PDI02 productDetails = objectFactory.createPDIPDI02();
			setValue(objectFactory, productDetails, cabinClass, 1);
			setValue(objectFactory, productDetails, availability, 2);
			pdi.getPDI02().add(productDetails);
		}

		return pdi;
	}

	public APD createAPD(String aircraftType, String noOfStops, Date start, Date end, String pecentage, String frequency,
			RouteInfo routeInfo) {
		APD apd = objectFactory.createAPD();
		APD.APD01 additionalProductDetails = objectFactory.createAPDAPD01();
		additionalProductDetails.setAPD0101(aircraftType);
		additionalProductDetails.setAPD0102(BigDecimal.valueOf(Integer.valueOf(noOfStops)));// TODO

		additionalProductDetails.setAPD0103(TypeADataConvertorUtill.getTimeDurationString(start, end));
		additionalProductDetails.setAPD0104(BigDecimal.valueOf(Integer.valueOf(pecentage))); // TODO what is this.
		if (frequency != null) {
			StringBuilder fr = new StringBuilder();
			fr.append((frequency.charAt(0) == '1') ? "1" : "");
			fr.append((frequency.charAt(1) == '1') ? "2" : "");
			fr.append((frequency.charAt(2) == '1') ? "3" : "");
			fr.append((frequency.charAt(3) == '1') ? "4" : "");
			fr.append((frequency.charAt(4) == '1') ? "5" : "");
			fr.append((frequency.charAt(5) == '1') ? "6" : "");
			fr.append((frequency.charAt(6) == '1') ? "7" : "");
			additionalProductDetails.setAPD0105(fr.toString());
		}
		apd.setAPD01(additionalProductDetails);
		APD.APD03 mileageTimeDetails = objectFactory.createAPDAPD03();
		if (routeInfo != null) {
			mileageTimeDetails.setAPD0301(String.valueOf(routeInfo.getDistance().intValue()));
			apd.setAPD03(mileageTimeDetails);
		}
		return apd;
	}

	/**
	 * @throws ModuleException
	 */
	public UNT createUNT(MessageHeader messageHeader) throws InteractiveEDIException {
		UNT unt = objectFactory.createUNT();
		setValue(objectFactory, unt, messageHeader.getMessageReferenceNumber(), 2);
		return unt;
	}

	public ObjectFactory getObjectFactory() {
		return objectFactory;
	}

	public List<Pair<ERC, IFT>> createERCIFT(Collection<ERCDetail> errorDetails) throws InteractiveEDIException {

		List<Pair<ERC, IFT>> ercIft = new ArrayList<Pair<ERC, IFT>>();
		if (errorDetails != null) {
			for (ERCDetail detail : errorDetails) {

				ERC erc = objectFactory.createERC();
				ERC01 erc01 = objectFactory.createERCERC01();
				setValue(objectFactory, erc01, detail.getErrorCode().getValue(), 1);
				erc.setERC01(erc01);

				IFT ift = objectFactory.createIFT();
				IFT01 ift01 = objectFactory.createIFTIFT01();
				setValue(objectFactory, ift01, detail.getErrorCode().getErrorText(), 1);
				ift.setIFT01(ift01);

				Pair<ERC, IFT> pair = Pair.of(erc, ift);
				ercIft.add(pair);

			}
		}
		return ercIft;
	}

}
