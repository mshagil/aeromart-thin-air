package com.isa.thinair.gdsservices.core.service;


public interface ResourceLocator {

	byte[] getContent(String path);

}
