package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRADTKDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.AdviceCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;

public class ResponseTransformerUtil {

	/**
	 * Returns a OSI for OTHS code
	 * 
	 * @param msg
	 * @return
	 */
	public static OSIDTO getResponseOSI(String msg) {
		OSIDTO osiDTO = new OSIDTO();

		osiDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		osiDTO.setCodeOSI(GDSExternalCodes.OSICodes.OTHERS.getCode());
		osiDTO.setOsiValue(msg);
		return osiDTO;
	}

	/**
	 * Returns a SSR for OTHS code
	 * 
	 * @param msg
	 * @return
	 */
	public static SSRDTO getResponseSSR(String msg) {
		SSRDTO ssrDTO = new SSROTHERSDTO();

		ssrDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		ssrDTO.setSsrValue(msg);

		return ssrDTO;
	}

	/**
	 * returns segment advice codes
	 * 
	 * @param segment
	 * @return
	 */
	public static String getSegmentStatus(Segment segment, String actionCode) {
		String adviceStatusCode = null;
		StatusCode status = segment.getStatusCode();

		if (status == null || status == StatusCode.NO_ACTION || status == StatusCode.ERROR) {
			adviceStatusCode = GDSExternalCodes.AdviceCode.NO_ACTION_TAKEN.getCode();
		} else if (status == StatusCode.CONFIRMED) {
			if (actionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())) {
				adviceStatusCode = GDSExternalCodes.AdviceCode.CONFIRMED.getCode();
			} else if (actionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode())) {
				adviceStatusCode = GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode();
			} else {
				adviceStatusCode = GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode();
			}
		} else if (status == StatusCode.CONFIRMED_SCHEDULE_CHANGED) {
//			if (actionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
//					|| actionCode.equals(GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode())) {
//				adviceStatusCode = GDSExternalCodes.AdviceCode.HOLD_NEED_SCHEDULE_CHANGED.getCode();
//			} else {
				adviceStatusCode = GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode();
//			}
		} else if (status == StatusCode.NOT_AVAILABLE) {
			adviceStatusCode = GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode();
		} else if (status == StatusCode.EXISTS) {
			if (actionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {
				adviceStatusCode = GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode();
			} else {
				adviceStatusCode = GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode();
			}
		} else if (status == StatusCode.DUPLICATE) {
			adviceStatusCode = GDSExternalCodes.AdviceCode.NO_ACTION_TAKEN.getCode();
		} else if (status == StatusCode.CANCELLED) {
			adviceStatusCode = GDSExternalCodes.AdviceCode.CANCELLED.getCode();
		}

		return adviceStatusCode;
	}

	/**
	 * returns segment advice codes
	 * 
	 * @param segment
	 * @return
	 */
	public static String getSegmentStatus(Segment segment) {
		return getSegmentStatus(segment, null);
	}

	/**
	 * Sets SSR advice codes to CONFIRMD
	 * 
	 * @param bookingRequestDTO
	 * @param b
	 */
	public static BookingRequestDTO setSSRStatus(BookingRequestDTO bookingRequestDTO) {

		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		for (Iterator<SSRDTO> iterator = ssrDTOs.iterator(); iterator.hasNext();) {
			SSRDTO ssrDTO = (SSRDTO) iterator.next();
			String gdsActionCode = ssrDTO.getActionOrStatusCode();
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			if (carrierCode.equals(ssrDTO.getCarrierCode()) && gdsActionCode != null) {
				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.SOLD.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode())) {
					ssrDTO.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
				} else if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())) {
					ssrDTO.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
				} else if (gdsActionCode.equals(GDSExternalCodes.ActionCode.EXCHANGE_RECOMMENDED.getCode())) {
					ssrDTO.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
				}
			}

		}

		return bookingRequestDTO;
	}

	/**
	 * Sets SSR advice codes to CONFIRMD
	 * 
	 * @param bookingRequestDTO
	 * @param b
	 */
	public static BookingRequestDTO setCancelledSSRStatus(BookingRequestDTO bookingRequestDTO) {

		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		for (Iterator<SSRDTO> iterator = ssrDTOs.iterator(); iterator.hasNext();) {
			SSRDTO ssrDTO = (SSRDTO) iterator.next();
			String gdsActionCode = ssrDTO.getActionOrStatusCode();
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			if (carrierCode.equals(ssrDTO.getCarrierCode())) {
				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())) {

					ssrDTO.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
				}
			}

		}

		return bookingRequestDTO;
	}
	
	/**
	 * Returns a SSR for ADTK code
	 * 
	 * @param msg
	 * @return
	 */
	public static SSRDTO getResponseSSRADTK(String msg) {
		SSRDTO ssrDTO = new SSRADTKDTO();

		ssrDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		ssrDTO.setSsrValue( "TO " + AppSysParamsUtil.getDefaultCarrierCode() + msg);

		return ssrDTO;
	}
}
