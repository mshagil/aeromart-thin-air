package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.typeA.transformers.DTOsTransformerUtil;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReValidateTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(ReValidateTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	private LCCClientReservation reservation;
	private String pnr;
	private GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;
	private List<TicketNumberDetails> couponTransitions = new ArrayList<TicketNumberDetails>();


	protected void handleMessage() {

		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		tktreqMessage = tktReq.getMessage();

		tktRes = new AATKTRES();
		tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);

		messageDTO.setResponse(tktRes);

		try {

			preHandleMessage();
			handleDiscrepancies();

			for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
				reValidateTicket(traveller);
				postReValidateTicket(traveller);
			}

			postHandleMessage();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REVALIDATE);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REVALIDATE);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		}
	}

	private void preHandleMessage() throws GdsTypeAException {

		pnr = DTOsTransformerUtil.getOwnPnr(tktReq.getMessage().getReservationControlInformation());

		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
			if (reservation == null) {
				throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);
		if (externalReservation.getReservationSynced().equals(CommonsConstants.NO)) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

		if (gdsReservation == null) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

	}

	private void handleDiscrepancies() throws GdsTypeAException {
		TKTREQMessage tktreqMessage = tktReq.getMessage();
		LCCClientReservationPax pax;
		int pnrPaxId;
		TravellerTicketingInformationWrapper travellerImage;
		TravellerTicketingInformation ticketTravellerImage;

		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
		ticketsAssembler.setPnr(pnr);
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		Map<Integer, GDSPaxChargesTO> paxAdjustment = new HashMap<Integer, GDSPaxChargesTO>();
		GDSPaxChargesTO paxChargesTO;

		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
		gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
		gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
		gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());
		gdsChargeAdjustmentRQ.setGdsChargesByPax(paxAdjustment);




		for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
			pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			pnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());

			travellerImage = GDSDTOUtil.resolveTraveller(traveller, traveller.getTravellers().get(0), gdsReservation);
			ticketTravellerImage = travellerImage.getTravellerInformation();

			FinancialInformationAssembler financialInformationAssembler = new FinancialInformationAssembler();
			financialInformationAssembler.setFinancialInformation(ticketTravellerImage.getText());
			financialInformationAssembler.setTaxDetails(ticketTravellerImage.getTaxDetails());
			financialInformationAssembler.setMonetaryInformation(ticketTravellerImage.getMonetaryInformation());
			financialInformationAssembler.calculateAmounts();

			if (!paxAdjustment.containsKey(pnrPaxId)) {
				paxChargesTO = new GDSPaxChargesTO();
				paxChargesTO.setPaxType(pax.getPaxType());
				paxChargesTO.setPnrPaxId(pnrPaxId);
				paxChargesTO.setFareAmount(BigDecimal.ZERO);
				paxChargesTO.setTaxAmount(BigDecimal.ZERO);
				paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
				paxAdjustment.put(pnrPaxId, paxChargesTO);

				GdsDtoTransformer.appendNilRecord(ticketsAssembler, pax);
			}

			paxChargesTO = paxAdjustment.get(pnrPaxId);

			paxChargesTO.setFareAmount(paxChargesTO.getFareAmount().add(financialInformationAssembler.getFareAmount()));
			paxChargesTO.setTaxAmount(paxChargesTO.getTaxAmount().add(financialInformationAssembler.getTaxAmount()));
			paxChargesTO.setSurChargeAmount(paxChargesTO.getSurChargeAmount().add(financialInformationAssembler.getSurcharge()));

		}

		try {
			ticketsAssembler.setGdsChargeAdjustmentRQ(gdsChargeAdjustmentRQ);
			TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);


		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

	}

	private void reValidateTicket(TravellerTicketingInformation traveller) throws GdsTypeAException {


		TravellerTicketingInformationWrapper travellerWrapper = GDSDTOUtil.resolvePassengerByTraveller(gdsReservation, traveller);
		TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();
		TicketNumberDetails ticketImage;
		TicketCoupon couponImage;

		TicketNumberDetails reValidatedTicket;
		TicketCoupon reValidatedCoupon;

		for (TicketNumberDetails ticketNumberDetails : traveller.getTickets()) {
			ticketImage = GDSDTOUtil.resolveTicket(travellerImage, ticketNumberDetails.getTicketNumber());

			reValidatedTicket = new TicketNumberDetails();
			reValidatedTicket.setTicketNumber(ticketImage.getTicketNumber());
			reValidatedTicket.setDocumentType(ticketImage.getDocumentType());

			for (TicketCoupon ticketCoupon : ticketNumberDetails.getTicketCoupon()) {

				couponImage = GDSDTOUtil.resolveCoupon(ticketImage, ticketCoupon.getCouponNumber());

				// before re-validation
				reValidatedCoupon = new TicketCoupon();
				reValidatedCoupon.setCouponNumber(couponImage.getCouponNumber());
				reValidatedCoupon.setStatus(TypeAConstants.CouponStatus.EXCHANGED);
				reValidatedCoupon.setFlightInfomation(couponImage.getFlightInfomation());
				reValidatedCoupon.setOriginatorInformation(travellerWrapper.getOriginatorInformation());
				reValidatedTicket.getTicketCoupon().add(reValidatedCoupon);

				// re-validation - update flight
				couponImage.setFlightInfomation(ticketCoupon.getFlightInfomation());

				// after re-validation
				reValidatedCoupon = new TicketCoupon();
				reValidatedCoupon.setCouponNumber(ticketCoupon.getCouponNumber());
				reValidatedCoupon.setStatus(TypeAConstants.CouponStatus.ORIGINAL_ISSUE);
				reValidatedCoupon.setFlightInfomation(ticketCoupon.getFlightInfomation());
				reValidatedCoupon.setOriginatorInformation(travellerWrapper.getOriginatorInformation());
				reValidatedTicket.getTicketCoupon().add(reValidatedCoupon);

			}

			couponTransitions.add(reValidatedTicket);

		}

	}

	private void postReValidateTicket(TravellerTicketingInformation traveller) throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();

		TravellerTicketingInformationWrapper travellerWrapper = GDSDTOUtil.resolvePassengerByTraveller(gdsReservation, traveller);
		TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();

		travellerWrapper.setOriginatorInformation(tktreqMessage.getOriginatorInformation());
		GDSDTOUtil.mergeCouponTransitions(travellerWrapper.getCouponTransitions(), couponTransitions, tktreqMessage.getOriginatorInformation());

		GdsInfoDaoHelper.saveGdsReservationTicketView(pnr, gdsReservation);

		LCCClientReservationPax lccPax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

		List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger
				.getMergedETicketCoupons(couponTransitions, lccPax);
		List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

		ServiceResponce serviceResponce;

		try {
			serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}
		if (!serviceResponce.isSuccess()) {
			throw new GdsTypeAException();
		}

	}


	private void postHandleMessage() throws GdsTypeAException {
		TKTRESMessage tktresMessage = tktRes.getMessage();

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REVALIDATE);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);
	}
}
