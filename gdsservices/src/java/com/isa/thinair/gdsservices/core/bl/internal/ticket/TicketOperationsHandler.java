package com.isa.thinair.gdsservices.core.bl.internal.ticket;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;

public abstract class TicketOperationsHandler {

	public static TicketOperationsHandler getTicketOperationsHandler(boolean isValidatingCarrier) {
		return isValidatingCarrier ? new ValidatingCarrierTicketOperationsHandler() :
				new OperatingCarrierTicketOperationsHandler();
	}

	public abstract void internalTicketReIssue(TicketingEventRq ticketingEventRq) throws ModuleException;
}
