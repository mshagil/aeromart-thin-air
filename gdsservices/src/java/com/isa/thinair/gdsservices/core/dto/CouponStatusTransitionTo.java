package com.isa.thinair.gdsservices.core.dto;

public class CouponStatusTransitionTo {

	public enum TransitionMode {
		RECEIVE,    // currentStatus* -> receivingStatus* -> newStatus -> sendingStatus
		SEND        // currentStatus* -> sendingStatus* -> receivingStatus -> newStatus
	}

	private TransitionMode transitionMode;
	private String currentStatus;
	private String receivingStatus;
	private String newStatus;
	private String sendingStatus;
	private boolean settlementAuthCodeRequired;
	private boolean invalidTransition;

	public CouponStatusTransitionTo(TransitionMode transitionMode) {
		this.transitionMode = transitionMode;
	}

	public TransitionMode getTransitionMode() {
		return transitionMode;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getReceivingStatus() {
		return receivingStatus;
	}

	public void setReceivingStatus(String receivingStatus) {
		this.receivingStatus = receivingStatus;
	}

	public String getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(String newStatus) {
		this.newStatus = newStatus;
	}

	public String getSendingStatus() {
		return sendingStatus;
	}

	public void setSendingStatus(String sendingStatus) {
		this.sendingStatus = sendingStatus;
	}

	public boolean isSettlementAuthCodeRequired() {
		return settlementAuthCodeRequired;
	}

	public void setSettlementAuthCodeRequired(boolean settlementAuthCodeRequired) {
		this.settlementAuthCodeRequired = settlementAuthCodeRequired;
	}

	public boolean isInvalidTransition() {
		return invalidTransition;
	}

	public void setInvalidTransition(boolean invalidTransition) {
		this.invalidTransition = invalidTransition;
	}
}
