package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateSSRActionCodes extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			for (SSRDTO ssrDTO : ssrDTOs) {
				if (carrierCode.equals(ssrDTO.getCarrierCode())) {
					String actionOrStatusCode = GDSApiUtils.maskNull(ssrDTO.getActionOrStatusCode());

					if (actionOrStatusCode.length() > 0) {
						String derivedActionOrStatusCode = ValidatorUtils.getSupportedActionOrStatus(ssrDTO
								.getActionOrStatusCode());

						if (derivedActionOrStatusCode == null) {
							String message = MessageUtil.getMessage("gdsservices.validators.unsupported.ssr.actionstatus.codes")
									+ " " + ssrDTO.getActionOrStatusCode();
							bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

							this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
							return bookingRequestDTO;
						}
					} else {
						// Nilindra
						// For SSR action codes can be empty
						// This could be for CC details, PSPT .etc
						// Tempory Fix till we identify
						// String message =
						// MessageUtil.getMessage("gdsservices.validators.empty.ssr.actionstatus.codes");
						// bookingRequestDTO = ValidatorUtils.composeGeneralFailure(bookingRequestDTO, message);

						// this.setStatus(GDSInternalCodes.ValidateConstants.INVALID);
						this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
						return bookingRequestDTO;
					}
				}
			}
		}

		this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		return bookingRequestDTO;
	}
}
