package com.isa.thinair.gdsservices.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.typeA.parser.EdiParser;
import com.isa.thinair.gdsservices.core.typeA.parser.EdiParserFactory;
import com.isa.thinair.gdsservices.core.typeA.parser.TypeAParseConstants;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsCalculator;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.InteractiveFreeText;
import com.isa.typea.common.MonetaryInformation;
import com.isa.typea.common.TaxDetails;

public class FinancialInformationAssembler implements Serializable {

	private FinancialInformation financialInformation;

	private List<TaxDetails> taxDetails;
	private List<MonetaryInformation> monetaryInformation;

	private BigDecimal totalFare;
	private BigDecimal totalTax;
	private BigDecimal totalSurcharge;

	private BigDecimal totalTaxInSettleCurrency;
	private BigDecimal totalFareInSettleCurrency;
	private BigDecimal totalSurchargeInSettleCurrency;
	
	private boolean isFareBackdownAvailable;

	// TODO migrate to this
	private FinancialInformationSummary financialInformationSummary;


	public void setFinancialInformation(List<InteractiveFreeText> texts) {
		EdiParser ediParser = EdiParserFactory.getEdiParser(null);
		Map<TypeAParseConstants.Segment, String> elements;

		StringBuilder fareText = new StringBuilder();
		for (InteractiveFreeText text : texts) {
			if (text.getInformationType().equals(TypeAConstants.InformationType.FARE_CALCULATION)) {
				for (String txt : text.getFreeText()) {
					fareText.append(txt);
				}
			}
		}

		elements = new HashMap<TypeAParseConstants.Segment, String>();
		elements.put(TypeAParseConstants.Segment.IFT_15, fareText.toString());

		this.financialInformation = ediParser.parseFinancialInformation(elements);
	}

	public void setTaxDetails(List<TaxDetails> taxDetails) {
		this.taxDetails = taxDetails;
	}

	public void setMonetaryInformation(List<MonetaryInformation> monetaryInformation) {
		this.monetaryInformation = monetaryInformation;
	}

	public void setFinancialInformation(FinancialInformation financialInformation) {
		this.financialInformation = financialInformation;
	}


	public void calculateAmounts() throws GdsTypeAException {
		String settleCurrency = null;

		if (monetaryInformation != null) {
			settleCurrency = GdsUtil.getSettleCurrency(monetaryInformation);
		}

		calculateAmounts(settleCurrency, GdsCalculator.RoundingType.UP);
	}

	private void calculateAmounts(String settleCurrency, GdsCalculator.RoundingType roundingType) throws GdsTypeAException {

		BigDecimal totalTax = BigDecimal.ZERO;
		BigDecimal totalFare = BigDecimal.ZERO;
		BigDecimal totalSurcharge = BigDecimal.ZERO;

		BigDecimal totalTaxInSettleCurrency;
		BigDecimal totalFareInSettleCurrency;
		BigDecimal totalSurchargeInSettleCurrency;

		FinancialInformation.Carrier carrier;
		boolean isSurcharge;
		MonetaryInformation monetaryInfo;

		BigDecimal rateOfExchange = financialInformation.getRateOfExchange();


		if (settleCurrency == null || settleCurrency.isEmpty()) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		for (FinancialInformation.Sector sector : financialInformation.getSectors()) {
			for (FinancialInformation.Segment segment : sector.getSegments()) {
				carrier = segment.getCarrier();

				if (carrier.getFlown()) {
					if (carrier.getStopoverTransferCharge() != null) {
						totalSurcharge = totalSurcharge.add(carrier.getStopoverTransferCharge());
					}
					if (carrier.getSurcharges() != null) {
						totalSurcharge = totalSurcharge.add(carrier.getSurcharges());
					}
				}
			}

			for (FinancialInformation.QuotedFare quotedFare : sector.getQuotedFares()) {
				isSurcharge =  quotedFare.getSurchargeAirportPair() != null || quotedFare.getAdditionalStopovers() != null;

				if (isSurcharge) {
					totalSurcharge = totalSurcharge.add(quotedFare.getAmount());
				} else {
					totalFare = totalFare.add(quotedFare.getAmount());
				}
			}
		}

		for(FinancialInformation.TaxFeeCharge taxFeeCharge : financialInformation.getPassengerFacilityCharges()) {
			totalSurcharge = totalSurcharge.add(taxFeeCharge.getAmount());
		}

		for(FinancialInformation.TaxFeeCharge taxFeeCharge : financialInformation.getUsDomesticTaxes()) {
			totalTax = totalTax.add(taxFeeCharge.getAmount());
		}

		if (taxDetails != null) {
			for (TaxDetails taxes : taxDetails) {
				for (TaxDetails.Tax tax : taxes.getTax()) {
					if (tax.getType() != null && getSurChargeCodes().contains(tax.getType())) {
						totalSurcharge = totalSurcharge.add(tax.getAmount());
					} else {
						totalTax = totalTax.add(tax.getAmount());
					}
				}
			}
		}

		if (rateOfExchange == null && !financialInformation.getCurrency().equals(settleCurrency)) {  //**
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		} else if (financialInformation.getCurrency().equals(settleCurrency)) {
			totalFareInSettleCurrency = totalFare;
			totalSurchargeInSettleCurrency = totalSurcharge;
			totalTaxInSettleCurrency = totalTax;
		} else {

			try {
				if ((monetaryInfo = GdsUtil.getMoneratoryAmount(monetaryInformation, TypeAConstants.MonetaryType.EQUIVALENT_FARE)) != null) {
					totalFareInSettleCurrency = new BigDecimal(monetaryInfo.getAmount());
				} else if ((monetaryInfo = GdsUtil.getMoneratoryAmount(monetaryInformation, TypeAConstants.MonetaryType.BASE_FARE)) != null) {
					if (monetaryInfo.getCurrencyCode().equals(settleCurrency)) {
						totalFareInSettleCurrency = new BigDecimal(monetaryInfo.getAmount());
					} else if (monetaryInfo.getCurrencyCode().equals(AppSysParamsUtil.getBaseCurrency())) {
						totalFareInSettleCurrency = GDSServicesUtil.convertFromBaseCurrency(settleCurrency, new BigDecimal(monetaryInfo.getAmount()));
					} else {
						totalFareInSettleCurrency = GDSServicesUtil.convertFromBaseCurrency(settleCurrency,
									GDSServicesUtil.convertToBaseCurrency(monetaryInfo.getCurrencyCode(), new BigDecimal(monetaryInfo.getAmount())));
					}
				} else {
					totalFareInSettleCurrency = totalFare.multiply(rateOfExchange);
				}
			} catch (ModuleException e) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY,
						TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			totalSurchargeInSettleCurrency = totalSurcharge;
			totalTaxInSettleCurrency = totalTax;

		}

		try {
			Currency currency = GDSServicesModuleUtil.getCommonMasterBD().getCurrency(settleCurrency);

			if (currency != null && currency.getFareRoundingUnits() != null && currency.getTaxesChargesRoundingUnits() != null) {
				totalFareInSettleCurrency = GdsCalculator.round(totalFareInSettleCurrency.toPlainString(), currency.getFareRoundingUnits(),
						roundingType);
				totalSurchargeInSettleCurrency = GdsCalculator.round(totalSurchargeInSettleCurrency.toPlainString(), currency.getTaxesChargesRoundingUnits(),
						roundingType);
				totalTaxInSettleCurrency = GdsCalculator.round(totalTaxInSettleCurrency.toPlainString(), currency.getTaxesChargesRoundingUnits(),
						roundingType);

			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		try {
			this.totalFareInSettleCurrency = totalFareInSettleCurrency;
			this.totalSurchargeInSettleCurrency = totalSurchargeInSettleCurrency;
			this.totalTaxInSettleCurrency = totalTaxInSettleCurrency;

			this.totalFare = GDSServicesUtil.convertToBaseCurrency(settleCurrency, totalFareInSettleCurrency);
			this.totalSurcharge = GDSServicesUtil.convertToBaseCurrency(settleCurrency, totalSurchargeInSettleCurrency);
			this.totalTax = GDSServicesUtil.convertToBaseCurrency(settleCurrency, totalTaxInSettleCurrency);


		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		}

	}


	public void constructElements(String settleCurrency, GdsCalculator.RoundingType roundingType, boolean refundGDSTax) throws GdsTypeAException {
   	    MonetaryInformation monetaryInfo;
		CommonMasterBD commonMasterBD = GDSServicesModuleUtil.getCommonMasterBD();
		Currency currency = null;
		try {
			currency = commonMasterBD.getCurrency(settleCurrency);
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		if(refundGDSTax){
			calculateAmounts(settleCurrency, roundingType);
			taxDetails = new ArrayList<TaxDetails>();
			monetaryInformation = new ArrayList<MonetaryInformation>();

		}else{ // This will refund fare amount only
			taxDetails = new ArrayList<TaxDetails>();
			calculateAmounts(settleCurrency, roundingType);
			monetaryInformation = new ArrayList<MonetaryInformation>();
		}
		monetaryInfo = new MonetaryInformation();
		monetaryInfo.setAmountTypeQualifier(TypeAConstants.MonetaryType.BASE_FARE);
		monetaryInfo.setAmount(totalFareInSettleCurrency.setScale(currency.getDecimalPlaces()).toPlainString());
		monetaryInfo.setCurrencyCode(settleCurrency);
		monetaryInformation.add(monetaryInfo);

		BigDecimal total = totalFareInSettleCurrency.add(totalSurchargeInSettleCurrency).add(totalTaxInSettleCurrency);
		monetaryInfo = new MonetaryInformation();
		monetaryInfo.setAmountTypeQualifier(TypeAConstants.MonetaryType.TICKET_TOTAL);
		monetaryInfo.setAmount(total.setScale(currency.getDecimalPlaces()).toPlainString());
		monetaryInfo.setCurrencyCode(settleCurrency);
		monetaryInformation.add(monetaryInfo);


	}
	
	
	public boolean isFareBreakdownInTKCUACAvailable() {

		boolean available = false;

		if ((financialInformation.getRateOfExchange() != null
				&& financialInformation.getRateOfExchange().compareTo(BigDecimal.ZERO) != 0)
				&& (financialInformation.getSectors() != null && financialInformation.getSectors().size() > 1)) {

			available = true;
		}
		setFareBackdownAvailable(available);
		return available;
	}

	private Collection<String> getSurChargeCodes() {
		Collection<String> surchargeCodes = new HashSet<String>();
		surchargeCodes.add("YQ");

		return surchargeCodes;
	}

	public FinancialInformation getFinancialInformation() {
		return financialInformation;
	}

	public List<TaxDetails> getTaxDetails() {
		return taxDetails;
	}

	public List<MonetaryInformation> getMonetaryInformation() {
		return monetaryInformation;
	}

	public BigDecimal getFareAmount() {
		return totalFare;
	}

	public BigDecimal getTaxAmount() {
	 	return totalTax;
	}

	public BigDecimal getSurcharge() {
		return totalSurcharge;
	}

	public boolean isFareBackdownAvailable() {
		return isFareBackdownAvailable;
	}

	public void setFareBackdownAvailable(boolean isFareBackdownAvailable) {
		this.isFareBackdownAvailable = isFareBackdownAvailable;
	}
}
