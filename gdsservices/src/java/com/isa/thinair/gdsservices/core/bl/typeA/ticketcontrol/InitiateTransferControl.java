package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageInitiator;

public class InitiateTransferControl extends EdiMessageInitiator {
	@Override
	protected void prepareMessages() throws ModuleException {

	}

	@Override
	protected void processResponse() {
	}
}
