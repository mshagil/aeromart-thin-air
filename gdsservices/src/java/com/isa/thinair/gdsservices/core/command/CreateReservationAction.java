package com.isa.thinair.gdsservices.core.command;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.AuthorizationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.util.AncillaryUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.BSPDetail;
import com.isa.thinair.gdsservices.api.dto.internal.Child;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CreditCardDetail;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Infant;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.PaymentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.dto.internal.SSRTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.PaymentType;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.SegmentType;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;

public class CreateReservationAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CreateReservationAction.class);

	public static CreateReservationRequest processRequest(CreateReservationRequest createReservationRequest) {
		try {
			String originatorPnr = getOriginatorPNR(createReservationRequest);
			Reservation reservation = loadReservation(originatorPnr, createReservationRequest.getGdsCredentialsDTO());

			if (reservation != null) {
				setReservationDetails(reservation, createReservationRequest);
			} else {
				createReservation(createReservationRequest);
			}
		} catch (Exception e) {
			addErrorResponse(createReservationRequest, e);
		}

		return createReservationRequest;

	}

	/**
	 * adds response to the createReservationRequest
	 * 
	 * @param serviceResponce
	 * @param createReservationRequest
	 * @param totalAmount
	 * @param onholdReleaseTime
	 * @param departureDate
	 * @throws ModuleException
	 */
	private static void addResponse(ServiceResponce serviceResponce, CreateReservationRequest createReservationRequest,
			Gds gds, BigDecimal totalAmount, Date onholdReleaseTime, String currency, Date departureDate) throws ModuleException {

		if (serviceResponce != null && serviceResponce.isSuccess()) {
			String strPNR = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);
			RecordLocator recordLocator = new RecordLocator();
			recordLocator.setPnrReference(strPNR);
			createReservationRequest.setResponderRecordLocator(recordLocator);
			createReservationRequest.setSuccess(true);
			setStatusCodeForChangedSchedules(createReservationRequest.getNewSegmentDetails());
			createReservationRequest = CommonUtil.addResponseMessage(createReservationRequest, totalAmount, onholdReleaseTime,
					currency, CalendarUtil.getCurrentSystemTimeInZulu(), gds.isAllowSendSSRADTK(), departureDate);

			String externalSystem = gds.getCodeShareCarrier() ? ExternalReservation.ExternalSystem.CODE_SHARE :
					ExternalReservation.ExternalSystem.GDS;

			ExternalReservation extReservation = new ExternalReservation();
			extReservation.setPnr(strPNR);
			extReservation.setExternalSystem(externalSystem);
			extReservation.setExternalRecordLocator(createReservationRequest.getOriginatorRecordLocator().getPnrReference());
			extReservation.setGdsId(gds.getGdsId());
			extReservation.setPriceSynced(CommonsConstants.NO);
			extReservation.setReservationSynced(CommonsConstants.YES);
			extReservation.setLockStatus(ExternalReservation.LockStatus.OPEN);
			GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(extReservation);

		} else {
			createReservationRequest.setSuccess(false);
			createReservationRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			createReservationRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
			CommonUtil.setSegmentStatusCodes(createReservationRequest.getNewSegmentDetails(), GDSInternalCodes.StatusCode.ERROR);
		}
	}

	/**
	 * adds passengers to the iReservation & returns amount to pay
	 * 
	 * @param iReservation
	 * @param passengers
	 * @param paxSeqIDMap
	 * @param collFares
	 * @param cardDetail
	 * @param travelAgent
	 * @return
	 * @throws ModuleException
	 */
	private static BigDecimal
			addAdults(IReservation iReservation, Collection<Passenger> passengers, Map<Passenger, Integer> paxSeqIDMap,
					Collection<OndFareDTO> collFares, CreditCardDetail cardDetail, Agent travelAgent) throws ModuleException {

		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<String, BigDecimal> paxFareMap = CommonUtil.getTotalPaxPayments(collFares);
		
		for (Passenger passenger : passengers) {
			Passenger pax = passenger;
			List<SSRTO> processedSSRs = CommonUtil
					.getSSRInfo(pax.getSsrCollection(), (ReservationAssembler) iReservation, collFares);
			BigDecimal amount = null;

			if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {
				Adult adt = (Adult) pax;
				amount = paxFareMap.get(PaxTypeTO.ADULT);
				if (adt.getInfant() != null) {
					addParent(iReservation, adt, amount, cardDetail, travelAgent, paxSeqIDMap, processedSSRs);
				} else {
					addSingle(iReservation, adt, amount, cardDetail, travelAgent, paxSeqIDMap, processedSSRs);
				}
			} else if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				amount = paxFareMap.get(PaxTypeTO.CHILD);
				addChild(iReservation, pax, amount, cardDetail, travelAgent, paxSeqIDMap, processedSSRs);

			}

			if (amount != null) {
				totalAmount = AccelAeroCalculator.add(totalAmount, amount);
			}
		}

		return totalAmount;
	}

	/**
	 * adds segment information to the IReservation
	 * 
	 * @param iReservation
	 * @param collFares
	 */
	private static void addSegmentInformation(IReservation iReservation, Collection<OndFareDTO> collFares, Collection<Segment> segments) {
		int intFareGroup = 1;
		int intSegSeq = 1;
		int returnOndGroup = 1;

		for (OndFareDTO ondFareDTO : collFares) {
			OndFareDTO ondFare = ondFareDTO;
			Set tempSegIds = ondFare.getSegmentsMap().keySet();
			String csFlightNumber = null;
			String csBookingClass = null;

			for (Iterator iterator = tempSegIds.iterator(); iterator.hasNext();) {
				FlightSegmentDTO element = ondFare.getSegmentsMap().get(iterator.next());
				
				for (Segment segment : segments){
					if (segment.getFlightNumber().equals(element.getFlightNumber())
							&& CalendarUtil.isEqualStartTimeOfDate(segment.getDepartureDate(), element.getDepartureDateTime())) {
						csFlightNumber = segment.getCodeShareFlightNo();
						csBookingClass = segment.getCodeShareBc();
					}
				}

				if (ondFare.isInBoundOnd()) {
					iReservation.addReturnSegment(intSegSeq, element.getSegmentId(), intFareGroup, null, returnOndGroup, null, null, csFlightNumber, csBookingClass);
				} else {
					iReservation.addOutgoingSegment(intSegSeq, element.getSegmentId(), intFareGroup, returnOndGroup, null, null, csFlightNumber, csBookingClass);
				}
				intSegSeq++;
			}
			intFareGroup++;
			if (!(ondFareDTO.getFareType() == FareTypes.RETURN_FARE || ondFareDTO.getFareType() == FareTypes.HALF_RETURN_FARE))
				++returnOndGroup;
				
		}

	}

	/**
	 * " loads travel agent
	 * 
	 * @param taIATACode
	 * @return
	 * @throws ModuleException
	 */
	private static Agent getTravelAgent(String taIATACode) throws ModuleException {
		Agent travelAgent = null;

		if (taIATACode == null || "".equals(taIATACode)) {
			throw new ModuleException("gdsservices.actions.agent.credential.empty");
		}

		travelAgent = GDSServicesModuleUtil.getTravelAgentBD().getAgentByIATANumber(taIATACode);

		if (travelAgent == null) {
			log.error("Invalid IATA code: " + travelAgent);
			throw new ModuleException("gdsservices.actions.agent.credential.invalid");
		}

		return travelAgent;
	}

	/**
	 * returns originator PNR
	 * 
	 * @param createReservationRequest
	 * @return
	 */
	private static String getOriginatorPNR(CreateReservationRequest createReservationRequest) {
		String pnr = "";

		if (createReservationRequest.getOriginatorRecordLocator() != null) {
			pnr = GDSApiUtils.maskNull(createReservationRequest.getOriginatorRecordLocator().getPnrReference());
		}

		return pnr;
	}

	/**
	 * returns External Pos
	 *
	 * @param createReservationRequest
	 * @return
	 */
	private static String getExternalPos(CreateReservationRequest createReservationRequest) {
		String externalPos = null;

		if (createReservationRequest.getOriginatorRecordLocator() != null) {
			externalPos = GDSApiUtils.maskNull(createReservationRequest.getOriginatorRecordLocator().getBookingOffice() + " "
					+ createReservationRequest.getOriginatorRecordLocator().getPointOfSales());
		}

		return externalPos;
	}

	/**
	 * returns segments
	 * 
	 * @param segmentDetails
	 * @return
	 */
	private static Collection<Segment> getSegments(Collection<SegmentDetail> segmentDetails) {
		Collection<Segment> segments = new ArrayList<Segment>();

		for (SegmentDetail segDetail : segmentDetails) {
			if (segDetail.getSegmentType() == SegmentType.SINGLE_SEGMENT) {
				segments.add(segDetail.getSegment());
			} else {
				throw new RuntimeException("gds.not.supported.next.phase");
			}
		}

		return segments;
	}

	/**
	 * returns passenger Map
	 * 
	 * @param passengers
	 * @return
	 */
	private static Map<Passenger, Integer> getPaxSeqIDMap(Collection<Passenger> passengers) {

		Map<Passenger, Integer> paxSeqIDMap = new HashMap<Passenger, Integer>();
		int i = 1;

		for (Passenger pasenger : passengers) {
			paxSeqIDMap.put(pasenger, Integer.valueOf(i));
			i++;
		}

		return paxSeqIDMap;
	}

	private static void checkOnholdReleaseTime(Date onholdReleaseTime) throws ModuleException {
		if (onholdReleaseTime == null) {
			throw new ModuleException("gdsservices.actions.reservation.cannot.onhold");
		}

	}

	/**
	 * sets reservation details
	 * 
	 * @param reservation
	 * @param createReservationRequest
	 */
	private static void setReservationDetails(Reservation reservation, CreateReservationRequest createReservationRequest) {
		CommonUtil.setSegmentStatusCodes(createReservationRequest.getNewSegmentDetails(), GDSInternalCodes.StatusCode.NO_ACTION);
		String strPNR = reservation.getPnr();
		RecordLocator recordLocator = new RecordLocator();
		recordLocator.setPnrReference(strPNR);
		createReservationRequest.setResponderRecordLocator(recordLocator);
		createReservationRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.actions.duplicate.orginator.pnr"));
		createReservationRequest.addErrorCode("gdsservices.actions.duplicate.orginator.pnr");
		createReservationRequest.setSuccess(false);

	}

	/**
	 * adds infants to the iReservation
	 * 
	 * @param reservation
	 * @param passengers
	 * @param paxSeqIDMap
	 * @throws ModuleException
	 */
	private static void
			addInfants(IReservation iReservation, Collection<Passenger> passengers, Map<Passenger, Integer> paxSeqIDMap,
					Collection<OndFareDTO> collFares, CreditCardDetail cardDetail, Agent travelAgent) throws ModuleException {

		Map<String, BigDecimal> paxFareMap = CommonUtil.getTotalPaxPayments(collFares);
		BigDecimal amount = paxFareMap.get(PaxTypeTO.INFANT);

		for (Passenger passenger : passengers) {
			Passenger pax = passenger;

			List<SSRTO> processedSSRs = CommonUtil.getSSRInfo(pax.getSsrCollection(), null, null);
			SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
			if (processedSSRs != null) {
				for (SSRTO ssrTo : processedSSRs) {
					Integer ssrId = SSRUtil.getSSRId(ssrTo.getFirstSSRCode());
					segmentSSRs.addPaxSegmentSSR(ssrTo.getSegSequence(), ssrId, ssrTo.getOtherSSRInfo());
				}
			}

			if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				Infant inf = (Infant) pax;

				IPayment ipaymentPassenger = new PaymentAssembler();
				if (cardDetail != null) {
					ipaymentPassenger.addCardPayment(Integer.parseInt(cardDetail.getCardType()), cardDetail.getExpiryDate(),
							cardDetail.getCardNo(), cardDetail.getHolderName(), "", cardDetail.getSecureCode(), amount,
							AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null, getIPGIdentificationParamsDTO(), null,
							null, null, null, null, null);
				} else if (travelAgent != null) {
					ipaymentPassenger.addAgentCreditPayment(travelAgent.getAgentCode(), amount, null, null, null, null, null,
							null, null);
				}

				PaxAdditionalInfoDTO paxAdditionalInfoDTO = getFilledPaxAdditionalInfoDTO(inf);
				iReservation.addInfant(inf.getFirstName(), inf.getLastName(), inf.getTitle(), inf.getInfantDateofBirth(),
						CommonUtil.getNationalityCode(inf.getNationalityCode()), paxSeqIDMap.get(inf),
						paxSeqIDMap.get(inf.getParent()), paxAdditionalInfoDTO, null, ipaymentPassenger, segmentSSRs, null, null, null, null,
						inf.getParent().getFamilyID(), null, null, null, null, null);
			}
		}
	}

	/**
	 * try loads a reservation using originator pnr#
	 * 
	 * @param originatorPnr
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private static Reservation loadReservation(String originatorPnr, GDSCredentialsDTO credentialsDTO) throws ModuleException {
		Reservation reservation = null;

		if (originatorPnr.length() > 0) {
			reservation = CommonUtil.loadReservation(credentialsDTO, originatorPnr, null);
		} else {
			throw new ModuleException("wsclient.pnrTransfer.invalid.flightsNotAvailable");
		}

		return reservation;
	}

	/**
	 * adds a child to the iReservation
	 * 
	 * @param travelAgent
	 * @param cardDetail
	 * @param amount
	 * @param pax
	 * @param processedSSRs
	 * @param paxSeqIDMap
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void addChild(IReservation iReservation, Passenger pax, BigDecimal amount, CreditCardDetail cardDetail,
			Agent travelAgent, Map<Passenger, Integer> paxSeqIDMap, List<SSRTO> processedSSRs) throws ModuleException {
		Child chl = (Child) pax;
		IPayment ipaymentPassenger = new PaymentAssembler();

		SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = getFilledPaxAdditionalInfoDTO(pax);
		if (processedSSRs != null) {
			for (SSRTO ssrTo : processedSSRs) {
				Integer ssrId = SSRUtil.getSSRId(ssrTo.getFirstSSRCode());
				segmentSSRs.addPaxSegmentSSR(ssrTo.getSegSequence(), ssrId, ssrTo.getOtherSSRInfo());
			}
		}

		if (cardDetail != null) {
			// FIXME for pay currency
			ipaymentPassenger.addCardPayment(Integer.parseInt(cardDetail.getCardType()), cardDetail.getExpiryDate(),
					cardDetail.getCardNo(), cardDetail.getHolderName(), "", cardDetail.getSecureCode(), amount,
					AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null, getIPGIdentificationParamsDTO(), null, null, null,
					null, null, null);
		} else if (travelAgent != null) {
			// FIXME for pay currency
			ipaymentPassenger.addAgentCreditPayment(travelAgent.getAgentCode(), amount, null, null, null, null, null, null, null);
		}

		iReservation.addChild(chl.getFirstName(), chl.getLastName(), chl.getTitle(), chl.getDateOfBirth(),
				CommonUtil.getNationalityCode(chl.getNationalityCode()), paxSeqIDMap.get(chl), paxAdditionalInfoDTO, null,
				ipaymentPassenger, segmentSSRs, null, null, null, null, chl.getFamilyID(), null, null, null, null, null);

	}

	/**
	 * adds a parent to the iReservation
	 * 
	 * @param iReservation
	 * @param adt
	 * @param amount
	 * @param cardDetail
	 * @param travelAgent
	 * @param paxSeqIDMap
	 * @param processedSSRs
	 * @throws ModuleException
	 */
	private static void addParent(IReservation iReservation, Adult adt, BigDecimal amount, CreditCardDetail cardDetail,
			Agent travelAgent, Map<Passenger, Integer> paxSeqIDMap, List<SSRTO> processedSSRs) throws ModuleException {
		IPayment ipaymentPassenger = new PaymentAssembler();

		SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
		if (processedSSRs != null) {
			for (SSRTO ssrTo : processedSSRs) {
				Integer ssrId = SSRUtil.getSSRId(ssrTo.getFirstSSRCode());
				segmentSSRs.addPaxSegmentSSR(ssrTo.getSegSequence(), ssrId, ssrTo.getOtherSSRInfo());
			}
		}

		// FIXME for pay currency
		if (cardDetail != null) {
			ipaymentPassenger.addCardPayment(Integer.parseInt(cardDetail.getCardType()), cardDetail.getExpiryDate(),
					cardDetail.getCardNo(), cardDetail.getHolderName(), "", cardDetail.getSecureCode(), amount,
					AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null, getIPGIdentificationParamsDTO(), null, null, null,
					null, null, null);
		} else if (travelAgent != null) {
			ipaymentPassenger.addAgentCreditPayment(travelAgent.getAgentCode(), amount, null, null, null, null, null, null, null);
		}

		PaxAdditionalInfoDTO paxAdditionalInfoDTO = getFilledPaxAdditionalInfoDTO(adt);
		iReservation.addParent(adt.getFirstName(), adt.getLastName(), adt.getTitle(), adt.getDateOfBirth(),
				CommonUtil.getNationalityCode(adt.getNationalityCode()), paxSeqIDMap.get(adt), paxSeqIDMap.get(adt.getInfant()),
				paxAdditionalInfoDTO, null, ipaymentPassenger, segmentSSRs, null, null, null, null, adt.getFamilyID(), null,
				null, null, null, null);

	}
	
	private static PaxAdditionalInfoDTO getFilledPaxAdditionalInfoDTO(Passenger pax) {
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
		paxAdditionalInfoDTO.setPassportNo(pax.getDocNumber());
		paxAdditionalInfoDTO.setPassportExpiry(pax.getDocExpiryDate());
		paxAdditionalInfoDTO.setPassportIssuedCntry(pax.getDocCountryCode());
		
		paxAdditionalInfoDTO.setTravelDocumentType(pax.getTravelDocType());
		paxAdditionalInfoDTO.setVisaApplicableCountry(pax.getVisaApplicableCountry());
		paxAdditionalInfoDTO.setVisaDocIssueDate(pax.getVisaDocIssueDate());
		paxAdditionalInfoDTO.setVisaDocNumber(pax.getVisaDocNumber());
		paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(pax.getVisaDocPlaceOfIssue());
		paxAdditionalInfoDTO.setPlaceOfBirth(pax.getPlaceOfBirth());
		
		return paxAdditionalInfoDTO;
	}

	/**
	 * adds a adult to the iReservation
	 * 
	 * @param iReservation
	 * @param adt
	 * @param amount
	 * @param cardDetail
	 * @param travelAgent
	 * @param paxSeqIDMap
	 * @param processedSSRs
	 * @throws ModuleException
	 */
	private static void addSingle(IReservation iReservation, Adult adt, BigDecimal amount, CreditCardDetail cardDetail,
			Agent travelAgent, Map<Passenger, Integer> paxSeqIDMap, List<SSRTO> processedSSRs) throws ModuleException {
		IPayment ipaymentPassenger = new PaymentAssembler();
		// FIXME for pay currency
		if (cardDetail != null) {
			ipaymentPassenger.addCardPayment(Integer.parseInt(cardDetail.getCardType()), cardDetail.getExpiryDate(),
					cardDetail.getCardNo(), cardDetail.getHolderName(), "", cardDetail.getSecureCode(), amount,
					AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null, getIPGIdentificationParamsDTO(), null, null, null,
					null, null, null);
		} else if (travelAgent != null) {
			ipaymentPassenger.addAgentCreditPayment(travelAgent.getAgentCode(), amount, null, null, null, null, null, null, null);
		}

		SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
		if (processedSSRs != null) {
			for (SSRTO ssrTo : processedSSRs) {
				Integer ssrId = SSRUtil.getSSRId(ssrTo.getFirstSSRCode());
				segmentSSRs.addPaxSegmentSSR(ssrTo.getSegSequence(), ssrId, ssrTo.getOtherSSRInfo());
			}
		}

		PaxAdditionalInfoDTO paxAdditionalInfoDTO = getFilledPaxAdditionalInfoDTO(adt);
		iReservation.addSingle(adt.getFirstName(), adt.getLastName(), adt.getTitle(), adt.getDateOfBirth(),
				CommonUtil.getNationalityCode(adt.getNationalityCode()), paxSeqIDMap.get(adt), paxAdditionalInfoDTO, null,
				ipaymentPassenger, segmentSSRs, null, null, null, null, adt.getFamilyID(), null, null, null, null, null);

	}

	private static void addOtherAirlineSegmentInformation(IReservation iReservation, Collection<Segment> externalSegments,
			Integer gdsId) throws ModuleException {
		if (TTYMessageUtil.getOALSegmentsRecordingEligibility(gdsId)) {
			if (externalSegments != null && !externalSegments.isEmpty()) {
				for (Segment segment : externalSegments) {
					Segment exSegment = segment;
					String flightNumber = exSegment.getFlightNumber();
					Integer flightSegId = null;
					if (exSegment.getCodeShareFlightNo() != null && exSegment.getSegmentCode() != null) {
						String[] values = exSegment.getSegmentCode().split("/");
						List<FlightSegement> flightSegement = (List<FlightSegement>) GDSServicesModuleUtil.getFlightBD()
								.getFlightSegmentsForLocalDate(values[0], values[1], exSegment.getCodeShareFlightNo(),
										exSegment.getDepartureDateTime(), true);

						if (flightSegement != null && flightSegement.size() > 0) {
							flightSegId = flightSegement.get(0).getFltSegId();
						}
					}

					iReservation.addOtherAirlineSegment(flightNumber, exSegment.getBookingCode(),
							exSegment.getCodeShareFlightNo(), exSegment.getCodeShareBc(), exSegment.getDepartureDateTime(),
							exSegment.getArrivalDateTime(),
							ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED, flightSegId,
							exSegment.getSegmentCode());
				}
			}
		}
	}
	
	private static void addExternalSegmentInformation(IReservation iReservation, Collection<Segment> externalSegments) {
		if (externalSegments != null && !externalSegments.isEmpty()) {
			int intSegSeq = 1;
			for (Segment segment : externalSegments) {
				Segment exSegment = segment;
				String flightNumber = exSegment.getFlightNumber();
				String externalCarrierCode = AppSysParamsUtil.extractCarrierCode(flightNumber);
				iReservation.addExternalPnrSegment(intSegSeq, externalCarrierCode, flightNumber, exSegment.getSegmentCode(), " ",
						exSegment.getDepartureDateTime(), exSegment.getArrivalDateTime());
				intSegSeq++;
			}
		}

	}

	private static void addAncillaries(IReservation iReservation) throws ModuleException {

		BookingClassBD bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();
		BookingClass bookingClass;

		ReservationAssembler reservationAssembler = (ReservationAssembler)iReservation;
		Reservation reservation = reservationAssembler.getDummyReservation();

		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
		FlightSegmentTO flightSegment;
		int flightSegId;

		Collection<OndFareDTO> fares = reservationAssembler.getInventoryFares();
		FlightSegmentDTO flightSegmentDTO;

		for (ReservationSegment pnrSegment : reservation.getSegments()) {
			flightSegment = new FlightSegmentTO();

			flightSegId = pnrSegment.getFlightSegId();

			for (OndFareDTO ondFare : fares) {
				if (ondFare.getSegmentsMap().containsKey(flightSegId)) {

					flightSegmentDTO = ondFare.getSegmentsMap().get(flightSegId);

					flightSegment.setFlightNumber(flightSegmentDTO.getFlightNumber());
					flightSegment.setDepartureDateTime(flightSegmentDTO.getDepartureDateTime());
					flightSegment.setDepartureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
					flightSegment.setArrivalDateTime(flightSegmentDTO.getArrivalDateTime());
					flightSegment.setArrivalDateTimeZulu(flightSegmentDTO.getArrivalDateTimeZulu());
					flightSegment.setSegmentCode(flightSegmentDTO.getSegmentCode());
					flightSegment.setFlightSegId(flightSegmentDTO.getSegmentId());
					flightSegment.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
					flightSegment.setOndSequence(ondFare.getOndSequence());
					flightSegment.setReturnFlag(false);
					flightSegment.setBookingClass(ondFare.getFareSummaryDTO().getBookingClassCode());

					bookingClass = bookingClassBD.getBookingClass(ondFare.getFareSummaryDTO().getBookingClassCode());

					flightSegment.setCabinClassCode(bookingClass.getCabinClassCode());
					flightSegment.setLogicalCabinClassCode(bookingClass.getLogicalCCCode());
					// flightSegment.setOndSequence(OndSequence.OUT_BOUND);
					flightSegment.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentDTO));
					flightSegment.setAirportCode(flightSegmentDTO.getDepartureAirportCode());

				}
			}

			FlightSegmentTO bagFlightSeg = new FlightSegmentTO();

			flightSegments.add(flightSegment);
		}


		List<Integer> adultChildList = new ArrayList<Integer>();
		for (ReservationPax pax : reservation.getPassengers()) {
			if (pax.getPaxType().equals(PaxTypeTO.ADULT) || pax.getPaxType().equals(PaxTypeTO.CHILD)) {
				adultChildList.add(pax.getPaxSequence());
			}

		}

		Map<Integer, List<ExternalChgDTO>> baggageExternalCharges = AncillaryUtil
				.getPaxwiseBaggageExternalCharges(flightSegments, adultChildList, reservation.getGdsId());


		if (baggageExternalCharges != null) {
			for (ReservationPax pax : reservation.getPassengers()) {
				if (baggageExternalCharges.containsKey(pax.getPaxSequence())) {
					pax.getPayment().addExternalCharges(baggageExternalCharges.get(pax.getPaxSequence()));
				}
			}
		}
	}

	/**
	 * gets fares for the segments
	 * 
	 * @param credentialsDTO
	 * @param is
	 * @param collection
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<OndFareDTO> getFares(Collection<Segment> segments, int[] totCnt, GDSCredentialsDTO credentialsDTO,
			Integer gdsId) throws ModuleException {
		Collection<OndFareDTO> collFares = GDSAASearchAssembler.getQuotedPrice(credentialsDTO, segments, totCnt, gdsId);
		return collFares;
	}

	/**
	 * adds on hold release time to the iReservation
	 * 
	 * @param iReservation
	 * @param paymentDetail
	 * @param createReservationRequest
	 * @param collFares
	 * @return
	 * @throws ModuleException
	 */
	private static Date addOnholdReleaseTime(IReservation iReservation, PaymentDetail paymentDetail,
			CreateReservationRequest createReservationRequest, Collection<OndFareDTO> collFares) throws ModuleException {
		Date onholdReleaseTime = null;

		if (paymentDetail == null) {
			onholdReleaseTime = CommonUtil.getGDSOnholdReleaseTime(createReservationRequest, collFares);
			checkOnholdReleaseTime(onholdReleaseTime);
			iReservation.setPnrZuluReleaseTimeStamp(onholdReleaseTime);
		}

		return onholdReleaseTime;
	}

	/**
	 * adds contact information to the IReservation
	 * 
	 * @param passengers
	 * @param reservation
	 * @param createReservationRequest
	 */
	private static void addContactInformation(IReservation iReservation, Collection<Passenger> passengers,
			CreateReservationRequest createReservationRequest) {
		ContactDetail firstPaxContactInfo = new ContactDetail();

		for (Passenger pax : passengers) {
			if (!pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				firstPaxContactInfo.setFirstName(pax.getFirstName());
				firstPaxContactInfo.setLastName(pax.getLastName());
				firstPaxContactInfo.setTitle(pax.getTitle());
				break;
			}
		}

		ReservationContactInfo reservationContactInfo = CommonUtil.createReservationContactInfo(
				createReservationRequest.getContactDetail(), firstPaxContactInfo);
		String posInfo = CommonUtil.getPOSInformation(createReservationRequest);

		iReservation.addContactInfo(CommonUtil.getAsUserNotes(createReservationRequest.getOsis(), posInfo),
				reservationContactInfo, null);
	}

	/**
	 * creates a reservation
	 * 
	 * @param iReservation
	 * @param cardDetail
	 * @param travelAgent
	 * @param collBlockID
	 * @return
	 * @throws ModuleException
	 */
	private static ServiceResponce callReservationServices(IReservation iReservation, CreditCardDetail cardDetail,
			Agent travelAgent, Collection collBlockID, GDSCredentialsDTO credentialsDTO) throws ModuleException {
		ServiceResponce serviceResponce;

		TrackInfoDTO trackInfoDTO = getTrackingInfo(credentialsDTO);
		if (cardDetail != null || travelAgent != null) {
			trackInfoDTO.setPaymentAgent(travelAgent.getAgentCode());
			serviceResponce = GDSServicesModuleUtil.getReservationBD().createCCReservation(iReservation, collBlockID,
					trackInfoDTO, false, true);
		} else {
			serviceResponce = GDSServicesModuleUtil.getReservationBD().createOnHoldReservation(iReservation, collBlockID, false,
					trackInfoDTO, true);
		}

		return serviceResponce;
	}

	/**
	 * creates a new reservation
	 * 
	 * @param createReservationRequest
	 * @throws ModuleException
	 */
	private static void createReservation(CreateReservationRequest createReservationRequest) throws ModuleException {
		String originatorPnr = getOriginatorPNR(createReservationRequest);
		PaymentDetail paymentDetail = createReservationRequest.getPaymentDetail();
		CreditCardDetail cardDetail = null;
		Agent travelAgent = null;
		Gds gds;

		if (paymentDetail != null) {
			if (paymentDetail.getPaymentType() == PaymentType.CREDIT_CARD) {
				cardDetail = (CreditCardDetail) paymentDetail;
			} else if (paymentDetail.getPaymentType() == PaymentType.BSP) {
				BSPDetail bspDetail = (BSPDetail) paymentDetail;
				travelAgent = getTravelAgent(bspDetail.getIATACode());
			}
		}

		Collection<SegmentDetail> newSegmentDetails = createReservationRequest.getNewSegmentDetails();
		Collection<Segment> externalSegments = createReservationRequest.getExternalSegments();
		Collection<Passenger> passengers = createReservationRequest.getPassengers();
		Collection collBlockID = null; //= CommonUtil.getSegBcAlloc(createReservationRequest.getBlockedSeats());

//		List<FlightSegement> suggestedFlightSegments = null;

		for (SegmentDetail segmentDetail : newSegmentDetails) {
			String segmentCode = segmentDetail.getSegment().getSegmentCode();
			Date departureDateLocal = segmentDetail.getSegment().getDepartureDateTime();
			Date arrivalDateLocal = segmentDetail.getSegment().getArrivalDateTime();
			String flightNum = segmentDetail.getSegment().getFlightNumber();
			int flightSegCount = GDSServicesModuleUtil.getFlightBD().getFlightSegmentCount(flightNum, departureDateLocal,
					arrivalDateLocal, segmentCode, createReservationRequest.getGdsCredentialsDTO().getAgentCode());

			if (flightSegCount == 0) {
				WildcardFlightSearchDto flightSearchDto = new WildcardFlightSearchDto();
				flightSearchDto.setDepartureDateExact(true);
				flightSearchDto.setArrivalDateExact(true);
				flightSearchDto.setFlightNumber(flightNum);
				flightSearchDto.setDepartureDate(departureDateLocal);
				flightSearchDto.setArrivalDate(arrivalDateLocal);
				flightSearchDto.setDepartureAirport(segmentDetail.getSegment().getDepartureStation());
				flightSearchDto.setArrivalAirport(segmentDetail.getSegment().getArrivalStation());
				WildcardFlightSearchRespDto resp = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(flightSearchDto);

				if (resp.getBestMatch() != null) {
					segmentDetail.getSegment().setFlightNumber(resp.getBestMatch().getFlightNumber());
					segmentDetail.setAlternateFlightNumber(true);

				} else {
					flightSearchDto = new WildcardFlightSearchDto();
					flightSearchDto.setDepartureDateExact(false);
					flightSearchDto.setFlightNumber(flightNum);
					flightSearchDto.setDepartureDate(departureDateLocal);
					flightSearchDto.setDepartureAirport(segmentDetail.getSegment().getDepartureStation());
					flightSearchDto.setArrivalAirport(segmentDetail.getSegment().getArrivalStation());
					resp = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(flightSearchDto);

					if (resp.getBestMatch() != null) {
						String departureTime = CalendarUtil.getSegmentTime(resp.getBestMatch().getDepartureDateTime());
						String arrivalTime = CalendarUtil.getSegmentTime(resp.getBestMatch().getArrivalDateTime());
						Date depTime = CalendarUtil.getSegmentTime(departureTime);
						Date arrTime = CalendarUtil.getSegmentTime(arrivalTime);

						segmentDetail.getSegment().setFlightNumber(resp.getBestMatch().getFlightNumber());
						segmentDetail.getSegment().setDepartureTime(depTime);
						segmentDetail.getSegment().setArrivalTime(arrTime);
						segmentDetail.getSegment().setArrivalDate(CalendarUtil.truncateTime(resp.getBestMatch().getArrivalDateTime()));
						segmentDetail.getSegment().setArrivalDayOffset(CalendarUtil.getTimeDifferenceInDays(resp.getBestMatch().getDepartureDateTime(),resp.getBestMatch().getArrivalDateTime()));
						segmentDetail.setChangeInSceduleExists(true);
					}
				}
			}
		}

		if (!AppSysParamsUtil.isChildOnlyBookingsAllowed()) {
			CommonUtil.checkChildOnlyBookingExistWhenCreating(passengers);
		}
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		int gdsId = getGDSId(createReservationRequest.getGds().getCode());
		Collection<OndFareDTO> collFares = getFares(getSegments(newSegmentDetails), createReservationRequest.getTotalPaxCounts(),
				createReservationRequest.getGdsCredentialsDTO(), gdsId);
		
		// Not allowed Onhold Restricted Booking Class for Reservation
		CommonUtil.checkOnholdRestrictedBookingClassAvailable(collFares);
		
		// 2. Block Seats ------------
		if (collBlockID == null || collBlockID.size() == 0) {
			collBlockID = GDSServicesModuleUtil.getReservationBD().blockSeats(collFares, null);
		}
		gds = GDSServicesModuleUtil.getGdsBD().getGds(gdsId);

		// 3. Create Reservation -----
		IReservation iReservation = new ReservationAssembler(collFares, null);
		iReservation.setExtRecordLocator(originatorPnr);
		iReservation.setGDSId(gdsId);
		String ohdCurrency = setCurrencyCode(createReservationRequest, collFares);
		iReservation.setLastCurrencyCode(ohdCurrency);
		iReservation.setModifiable(gds.getAirlineResModifiable() ?
				ReservationInternalConstants.Modifiable.YES : ReservationInternalConstants.Modifiable.NO);
		iReservation.setExternalPos(getExternalPos(createReservationRequest));

		// 3.1. Set OnHold ReleseTime -----
		Date onholdReleaseTime = addOnholdReleaseTime(iReservation, paymentDetail, createReservationRequest, collFares);

		// 3.4. Set Segment Information -----
		addSegmentInformation(iReservation, collFares, getSegments(newSegmentDetails));

		// 3.5. Set External Segment Information ---
		addOtherAirlineSegmentInformation(iReservation, externalSegments, gdsId);
		
		// 3.2. Set Passenger Information -----
		Map<Passenger, Integer> paxSeqIDMap = getPaxSeqIDMap(passengers);

		totalAmount = addAdults(iReservation, passengers, paxSeqIDMap, collFares, cardDetail, travelAgent);
		addInfants(iReservation, passengers, paxSeqIDMap, collFares, cardDetail, travelAgent);

		// 3.3. Set Customer Contact Information -----
		addContactInformation(iReservation, passengers, createReservationRequest);

		addAncillaries(iReservation);
				
		GDSCredentialsDTO gdsCredentialsDTO = createReservationRequest.getGdsCredentialsDTO();
		if (gdsCredentialsDTO != null
				&& AuthorizationUtil.hasPrivilege(gdsCredentialsDTO.getPrivileges(),
						PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP)) {
			iReservation.setSkipDuplicateCheck(true);
		}

		// 4. Do Reservation -----
		ServiceResponce serviceResponce = callReservationServices(iReservation, cardDetail, travelAgent, collBlockID,
				gdsCredentialsDTO);
		
		Date departureDate = ReleaseTimeUtil.getFirstFlightDepartureFlightSegment(collFares).getDepartureDateTimeZulu();
		
		addResponse(serviceResponce, createReservationRequest, gds, totalAmount, onholdReleaseTime, ohdCurrency, departureDate);
		

	}
	
	private static String setCurrencyCode(CreateReservationRequest createReservationRequest, Collection<OndFareDTO> collFares) {
		RecordLocator originRecordLocator = createReservationRequest.getOriginatorRecordLocator();
		List<String> currencies = CommonsServices.getGlobalConfig().getActiveCurrencyList();
		String currencyCode = AppSysParamsUtil.getBaseCurrency();
		try {
			/*
			 * if (originRecordLocator != null) { if (originRecordLocator.getCurrencyCode() != null &&
			 * currencies.contains(originRecordLocator.getCurrencyCode())) { return
			 * originRecordLocator.getCurrencyCode(); } else if (originRecordLocator.getCountryCode() != null) { Country
			 * country = GDSServicesModuleUtil.getLocationServiceBD().getCountry(originRecordLocator.getCountryCode());
			 * if ( country != null && currencies.contains(country.getCurrencyCode())) { return
			 * country.getCurrencyCode(); } else if (originRecordLocator.getBookingOffice() != null) { Station station =
			 * GDSServicesModuleUtil.getLocationServiceBD().getStation(originRecordLocator.getBookingOffice()); Country
			 * StaionCountry = GDSServicesModuleUtil.getLocationServiceBD().getCountry(station.getCountryCode()); if
			 * (StaionCountry != null && currencies.contains(StaionCountry.getCurrencyCode())) { return
			 * StaionCountry.getCurrencyCode(); } } } }
			 */
			String firstDepAirport = CommonUtil.getFirstFlightDepartureAirport(collFares);
			if (firstDepAirport != null) {
				String countryCode = GDSServicesModuleUtil.getAirportBD().getOwnCountryCodeForAirport(firstDepAirport);
				Country StaionCountry = GDSServicesModuleUtil.getLocationServiceBD().getCountry(countryCode);
				if (StaionCountry != null && currencies.contains(StaionCountry.getCurrencyCode())) {
					return StaionCountry.getCurrencyCode();
				} 
			}
		} catch (Exception ex) {
			//Do nothing
		}
		return currencyCode;
	}

	/**
	 * adds error response to the createReservationRequest
	 * 
	 * @param createReservationRequest
	 * @param e
	 */
	private static void addErrorResponse(CreateReservationRequest createReservationRequest, Exception e) {
		if (e instanceof ModuleException) {
			ModuleException me = (ModuleException) e;
			log.error(" CreateReservationAction Failed for GDS PNR "
					+ createReservationRequest.getOriginatorRecordLocator().getPnrReference(), e);
			CommonUtil.setSegmentStatusCodes(createReservationRequest.getNewSegmentDetails(), GDSInternalCodes.StatusCode.ERROR);
			createReservationRequest.setSuccess(false);
			if (createReservationRequest.getResponseMessage() == null || createReservationRequest.getResponseMessage() == "") {
				if (me.getModuleCode() != null) {
					createReservationRequest.setResponseMessage(me.getMessageString() + me.getModuleCode());
				} else {
					createReservationRequest.setResponseMessage(me.getMessageString());
				}
				createReservationRequest.addErrorCode(me.getExceptionCode());
			}
		} else {
			log.error(" CreateReservationAction Failed for GDS PNR "
					+ createReservationRequest.getOriginatorRecordLocator().getPnrReference(), e);
			CommonUtil.setSegmentStatusCodes(createReservationRequest.getNewSegmentDetails(), GDSInternalCodes.StatusCode.ERROR);
			createReservationRequest.setSuccess(false);
			createReservationRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			createReservationRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}
	}

	/**
	 * FIXME - Multi Currency Pay Gateway Temp solution
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private static IPGIdentificationParamsDTO getIPGIdentificationParamsDTO() throws ModuleException {

		String defaultPmtCurrency = AppSysParamsUtil.getDefaultPGCurrency();
		Integer ipgId = GDSServicesModuleUtil.getCommonMasterBD().getCurrency(defaultPmtCurrency).getDefaultXbePGId();
		return new IPGIdentificationParamsDTO(ipgId, AppSysParamsUtil.getBaseCurrency());
	}

	private static TrackInfoDTO getTrackingInfo(GDSCredentialsDTO credentialsDTO) {

		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_GDS);
		trackInfoDTO.setOriginUserId(credentialsDTO.getUserId());
		trackInfoDTO.setOriginAgentCode(credentialsDTO.getAgentCode());
		trackInfoDTO.setOriginChannelId(credentialsDTO.getSalesChannelCode());
		return trackInfoDTO;

	}
	
	private static Integer getGDSId(String gdsCode) {
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		for (Entry<String, GDSStatusTO> entry : gdsStatusMap.entrySet()) {
			if (entry.getValue().getGdsCode().equals(gdsCode)) {
				return new Integer(entry.getKey());
			}
		}
		return null;
	}
	
	private static void setStatusCodeForChangedSchedules (Collection<SegmentDetail> segmentCollection) {
		for (SegmentDetail segment : segmentCollection) {
			if (segment.isChangeInSceduleExists()) {
				segment.getSegment().setStatusCode(GDSInternalCodes.StatusCode.CONFIRMED_SCHEDULE_CHANGED);
			}
		}
	}

}
