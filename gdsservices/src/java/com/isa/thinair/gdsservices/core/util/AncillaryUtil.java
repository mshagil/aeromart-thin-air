package com.isa.thinair.gdsservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonLoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientLMSPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServicePenaltyExtChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.airportTransfer.AirportTransferExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

public class AncillaryUtil {
	private AncillaryUtil() {
	}

	public static Map<Integer, List<ExternalChgDTO>> getPaxwiseBaggageExternalCharges(
			List<FlightSegmentTO> flightSegments, List<Integer> adultChildList, int gdsId)
			throws ModuleException {

		BookingClass bookingClass;
		Gds gds;
		boolean baggageAvailable;
		Map<Integer, List<ExternalChgDTO>> externalChargesMapForBaggages = null;

		Map<String, String> cosMap = new HashMap<String, String>();
		Map<String, String> bcMap = new HashMap<String, String>();

		gds =  GDSServicesModuleUtil.getGdsBD().getGds(gdsId);

		for (FlightSegmentTO flightSegment : flightSegments) {

			bookingClass = GDSServicesModuleUtil.getBookingClassBD().getBookingClass(flightSegment.getBookingClass());

			bcMap.put(flightSegment.getFlightRefNumber(), flightSegment.getBookingClass());
			cosMap.put(flightSegment.getFlightRefNumber(), bookingClass.getCabinClassCode());
		}

		Map<String, Integer> baggageOndGrpIdWiseCount = new HashMap<String, Integer>();
		Map<Integer, Map<String, LCCBaggageDTO>> paxWiseBaggageMap = new HashMap<Integer, Map<String, LCCBaggageDTO>>();

		if (!flightSegments.isEmpty()) {
			LCCReservationBaggageSummaryTo ondBaggageSummaryTO = new LCCReservationBaggageSummaryTo();
			ondBaggageSummaryTO.setBookingClasses(bcMap);
			ondBaggageSummaryTO.setClassesOfService(cosMap);

//			if (!isAncillaryAvailable(flightSegments, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.BAGGAGE, ondBaggageSummaryTO)) {
//				throw new ModuleException("Baggage not available");
//			}

			baggageAvailable = isAncillaryAvailable(flightSegments, LCCAncillaryAvailabilityInDTO.LCCAncillaryType.BAGGAGE, ondBaggageSummaryTO);

			if (baggageAvailable) {
				Map<Integer, LCCBaggageDTO> ondDefaultBaggage = getDefaultBaggage(flightSegments, baggageOndGrpIdWiseCount,
						ondBaggageSummaryTO, gds);

				Map<String, LCCBaggageDTO> ondDefaultBaggageKeyConverted = new HashMap<String, LCCBaggageDTO>();
				for (Map.Entry<Integer, LCCBaggageDTO> ondDefaultBaggageEntry : ondDefaultBaggage.entrySet()) {
					ondDefaultBaggageKeyConverted.put(ondDefaultBaggageEntry.getKey().toString(), ondDefaultBaggageEntry.getValue());
				}

				for (Integer paxRph : adultChildList) {
					Map<String, LCCBaggageDTO> baggageMap = paxWiseBaggageMap.get(paxRph);
					if (baggageMap == null) {
						paxWiseBaggageMap.put(paxRph, ondDefaultBaggageKeyConverted);
					} else {
						for (Map.Entry<String, LCCBaggageDTO> entry : ondDefaultBaggageKeyConverted.entrySet()) {
							baggageMap.put(entry.getKey(), entry.getValue());
						}
					}

				}

				externalChargesMapForBaggages = getExternalChargesMapForBaggages(paxWiseBaggageMap);
			}
		}

		return externalChargesMapForBaggages;
	}

	private static Map<Integer, LCCBaggageDTO> getDefaultBaggage(List<FlightSegmentTO> segments,
			Map<String, Integer> baggageOndGrpIdWiseCount, LCCReservationBaggageSummaryTo baggageSummaryTo, Gds gds)
			throws ModuleException {

		User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(gds.getUserId());

		Map<Integer, List<FlightBaggageDTO>> baggages = null;
		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			BaggageRq baggageRq = new BaggageRq();
			baggageRq.setFlightSegmentTOs(segments);
			baggageRq.setBookingClasses(baggageSummaryTo.getBookingClasses());
			baggageRq.setClassesOfService(baggageSummaryTo.getClassesOfService());
			baggageRq.setLogicalCC(baggageSummaryTo.getLogicalCC());
			baggageRq.setCheckCutoverTime(true);
			baggageRq.setSalesChannel(ReservationInternalConstants.SalesChannel.GDS);
			baggageRq.setAgent(user.getAgentCode());
			baggageRq.setRequote(AppSysParamsUtil.isRequoteEnabled());
			baggageRq.setOwnReservation(true);

			baggages = GDSServicesModuleUtil.getBaggageBD().getBaggage(baggageRq);

			if (baggages != null && baggages.size() > 0) {
				for (Map.Entry<Integer, List<FlightBaggageDTO>> entry : baggages.entrySet()) {
					String ondGroupId = BeanUtils.getFirstElement(entry.getValue()).getOndGroupId();
					Integer count = baggageOndGrpIdWiseCount.get(ondGroupId);

					if (count == null) {
						baggageOndGrpIdWiseCount.put(ondGroupId, new Integer(1));
					} else {
						baggageOndGrpIdWiseCount.put(ondGroupId, ++count);
					}
				}
			}
		} else {
			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
			for (FlightSegmentTO fltSeg : segments) {
				flightSegIdWiseCos.put(fltSeg.getFlightSegId(), null);
			}
			baggages = GDSServicesModuleUtil.getBaggageBD().getBaggages(flightSegIdWiseCos, false, false);
		}

		Map<Integer, LCCBaggageDTO> defaultBaggage = new HashMap<Integer, LCCBaggageDTO>();

		for (Integer flightSegid : baggages.keySet()) {

			for (FlightBaggageDTO availBag : baggages.get(flightSegid)) {
				if (CommonsConstants.YES.equals(availBag.getDefaultBaggage())) {
					LCCBaggageDTO lccBaggageDTO = new LCCBaggageDTO();
					lccBaggageDTO.setBaggageCharge(availBag.getAmount());
					lccBaggageDTO.setBaggageName(availBag.getBaggageName());
					lccBaggageDTO.setOndBaggageChargeId(BeanUtils.nullHandler(availBag.getChargeId()));
					lccBaggageDTO.setOndBaggageGroupId(BeanUtils.nullHandler(availBag.getOndGroupId()));

					defaultBaggage.put(flightSegid, lccBaggageDTO);
				}
			}
		}

		return defaultBaggage;
	}

	public static Map<Integer, List<ExternalChgDTO>> getExternalChargesMapForBaggages(
			Map<Integer, Map<String, LCCBaggageDTO>> baggageDetailsMap) throws ModuleException {

		Map<Integer, List<ExternalChgDTO>> externalCharges = new HashMap<Integer, List<ExternalChgDTO>>();

		Collection<ReservationInternalConstants.EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE);

		ExternalChgDTO externalChgDTO = BeanUtils.getFirstElement(GDSServicesModuleUtil.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());


		for (Integer travelerRef : baggageDetailsMap.keySet()) {
			List<ExternalChgDTO> chargesList = new ArrayList<ExternalChgDTO>();
			Map<String, LCCBaggageDTO> baggagesMap = baggageDetailsMap.get(travelerRef);
			for (String flightRPH : baggagesMap.keySet()) {
				LCCBaggageDTO baggageDTO = baggagesMap.get(flightRPH);


				BaggageExternalChgDTO externalChargeTO = (BaggageExternalChgDTO)externalChgDTO.clone();

				externalChargeTO.setChargeCode(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE.toString());
				externalChargeTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
				externalChargeTO.setExternalChargesEnum(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE);
				externalChargeTO.setOndBaggageGroupId(baggageDTO.getOndBaggageGroupId());
				externalChargeTO.setOndBaggageChargeGroupId(baggageDTO.getOndBaggageChargeId());
				externalChargeTO.setAmount(baggageDTO.getBaggageCharge());
				externalChargeTO.setBaggageCharge(baggageDTO.getBaggageCharge());
				externalChargeTO.setBaggageCode(baggageDTO.getBaggageName());
				externalChargeTO.setFlightSegId(Integer.valueOf(flightRPH));

				chargesList.add(externalChargeTO);

			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}




	public static boolean isAncillaryAvailable(List<FlightSegmentTO> flightSegmentTOs, String anciType,
			LCCReservationBaggageSummaryTo baggageSummaryTO) throws ModuleException {

		boolean isAnciAvailable = false;

		LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityInDTO = new LCCAncillaryAvailabilityInDTO();
		lccAncillaryAvailabilityInDTO.setFlightDetails(flightSegmentTOs);
		lccAncillaryAvailabilityInDTO.addAncillaryType(anciType);
		lccAncillaryAvailabilityInDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		lccAncillaryAvailabilityInDTO.setModifyAncillary(false);
		lccAncillaryAvailabilityInDTO.setOnHold(false);
		lccAncillaryAvailabilityInDTO.setBaggageSummaryTo(baggageSummaryTO);
		lccAncillaryAvailabilityInDTO.setQueryingSystem(ProxyConstants.SYSTEM.AA);
		lccAncillaryAvailabilityInDTO.setDepartureSegmentCode(getDepartureSegmentCode(flightSegmentTOs));

		AnciAvailabilityRS anciAvailabilityRS = GDSServicesModuleUtil.getAirproxyAncillaryBD().getAncillaryAvailability(
				lccAncillaryAvailabilityInDTO, null);
		List<LCCAncillaryAvailabilityOutDTO> lccAncillaryAvailabilityOutDTOs = anciAvailabilityRS
				.getLccAncillaryAvailabilityOutDTOs();
		lccAncillaryAvailabilityOutDTOs = checkPrivilegesAndBufferTimes(lccAncillaryAvailabilityOutDTOs, anciType);

		isAnciAvailable = anciActiveInLeastOneSeg(lccAncillaryAvailabilityOutDTOs, anciType);

		return isAnciAvailable;
	}

	private static List<LCCAncillaryAvailabilityOutDTO> checkPrivilegesAndBufferTimes(
			List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				Date departureDate = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getDepartureDateTime();
				String airportCode = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getSegmentCode().split("/")[0];
				Date depatureDateZulu = null;
				try {
					depatureDateZulu = getZuluDateTime(departureDate, airportCode);
				} catch (ModuleException e) {
					depatureDateZulu = departureDate;
				}
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					ancillaryStatus.setAvailable(ancillaryStatus.isAvailable() && isEnableService(anciType, depatureDateZulu));

				}
			}
		}
		return availabilityOutDTO;
	}

	private static boolean isEnableService(String anciType, Date departDate) {

		long diffMils = 3600000;

		Calendar currentTime = Calendar.getInstance();

		if (anciType.equalsIgnoreCase(LCCAncillaryAvailabilityInDTO.LCCAncillaryType.MEALS)) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
		}
		if (departDate.getTime() - diffMils > currentTime.getTimeInMillis()) {
			return true;
		} else {
			return false;
		}

	}

	private static boolean anciActiveInLeastOneSeg(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					if ((ancillaryStatus.isAvailable())
							&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static Date getZuluDateTime(Date localDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(GDSServicesModuleUtil.getAirportBD());

		return helper.getZuluDateTime(airportCode, localDate);
	}


	public static String getDepartureSegmentCode(List<FlightSegmentTO> flightSegmentTOs) {
		if (flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
			Collections.sort(flightSegmentTOs);
			return flightSegmentTOs.iterator().next().getSegmentCode();
		}

		return null;
	}


	public static IPayment populatePerPaxPayment(LCCClientPaymentAssembler paymentAssembler,
			Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap) throws ModuleException {
		IPayment ipaymentPassenger = new PaymentAssembler();

		Collection<ExternalChgDTO> extChgList = new ArrayList<ExternalChgDTO>();

		if(extExternalChgDTOMap !=null){
			for (LCCClientExternalChgDTO paxExtChg : paymentAssembler.getPerPaxExternalCharges()) {

				ExternalChgDTO externalChgDTO = (ExternalChgDTO) extExternalChgDTOMap.get(paxExtChg.getExternalCharges());

				if (externalChgDTO != null) {
					externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
					externalChgDTO.setApplicableChargeRate(paxExtChg.getJourneyType());
					externalChgDTO.setAmountConsumedForPayment(paxExtChg.isAmountConsumedForPayment());
					externalChgDTO.setAmount(paxExtChg.getAmount());
					externalChgDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));

					if (externalChgDTO instanceof SMExternalChgDTO) {
						((SMExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg
								.getFlightRefNumber()));
						((SMExternalChgDTO) externalChgDTO).setSeatNumber(paxExtChg.getCode());
					} else if (externalChgDTO instanceof MealExternalChgDTO) {
						((MealExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg
								.getFlightRefNumber()));
						((MealExternalChgDTO) externalChgDTO).setMealCode(paxExtChg.getCode());
						((MealExternalChgDTO) externalChgDTO).setAnciOffer(paxExtChg.isAnciOffer());
						((MealExternalChgDTO) externalChgDTO).setOfferedTemplateID(paxExtChg.getOfferedAnciTemplateID());
					} else if (externalChgDTO instanceof BaggageExternalChgDTO) {
						((BaggageExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
						((BaggageExternalChgDTO) externalChgDTO).setBaggageCode(paxExtChg.getCode());
						((BaggageExternalChgDTO) externalChgDTO).setOndBaggageChargeGroupId(paxExtChg.getOndBaggageChargeGroupId());
						((BaggageExternalChgDTO) externalChgDTO).setOndBaggageGroupId(paxExtChg.getOndBaggageGroupId());
					} else if (externalChgDTO instanceof FlexiExternalChgDTO) {
						LCCClientFlexiExternalChgDTO paxFlexiChg = (LCCClientFlexiExternalChgDTO) paxExtChg;
						((FlexiExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxFlexiChg.getFlightRefNumber()));
						((FlexiExternalChgDTO) externalChgDTO).setChargeCode(paxFlexiChg.getCode());
						for (FlexiInfoTO flexibility : paxFlexiChg.getFlexiBilities()) {
							ReservationPaxOndFlexibilityDTO flexi = new ReservationPaxOndFlexibilityDTO();
							flexi.setAvailableCount(flexibility.getAvailableCount());
							flexi.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
							flexi.setUtilizedCount(0);
							flexi.setFlexiRateId(flexibility.getFlexiRateId());
							flexi.setFlexibilityTypeId(flexibility.getFlexibilityTypeId());
							((FlexiExternalChgDTO) externalChgDTO).addToReservationFlexibilitiesList(flexi);
						}
					} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
						((SSRExternalChargeDTO) externalChgDTO).setAirportCode(paxExtChg.getAirportCode());
						((SSRExternalChargeDTO) externalChgDTO).setFlightRPH(paxExtChg.getFlightRefNumber());
						((SSRExternalChargeDTO) externalChgDTO).setSegmentSequece(paxExtChg.getSegmentSequence());
						((SSRExternalChargeDTO) externalChgDTO).setApplyOn((paxExtChg.getApplyOn() == null)
								? ReservationPaxSegmentSSR.APPLY_ON_SEGMENT
								: paxExtChg.getApplyOn());
						((SSRExternalChargeDTO) externalChgDTO).setSSRText(paxExtChg.getUserNote());

						Integer ssrId = SSRUtil.getSSRId(paxExtChg.getCode());
						((SSRExternalChargeDTO) externalChgDTO).setSSRId(ssrId);
					} else if (externalChgDTO instanceof AirportTransferExternalChgDTO) {
						((AirportTransferExternalChgDTO) externalChgDTO).setFlightRPH(paxExtChg.getFlightRefNumber());
						((AirportTransferExternalChgDTO) externalChgDTO).setFlightSegId(FlightRefNumberUtil
								.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferDate(paxExtChg.getTransferDate());
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferType(paxExtChg.getTransferType());
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferAddress(paxExtChg.getTransferAddress());
						((AirportTransferExternalChgDTO) externalChgDTO).setTransferContact(paxExtChg.getTransferContact());
						((AirportTransferExternalChgDTO) externalChgDTO).setAirportTransferId(paxExtChg.getOfferedAnciTemplateID());
						((AirportTransferExternalChgDTO) externalChgDTO).setAirportCode(paxExtChg.getAirportCode());

					} else if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
						((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
					} else if (externalChgDTO instanceof ServicePenaltyExtChgDTO) {
						((ServicePenaltyExtChgDTO) externalChgDTO).setFlightRefNumber(paxExtChg.getFlightRefNumber());
					}
					extChgList.add(externalChgDTO);
				}
			}
		}
		ipaymentPassenger.addExternalCharges(extChgList);

		BigDecimal consumedExternalCharges = paymentAssembler.getTotalConsumedExternalCharges();
		for (LCCClientPaymentInfo payment : paymentAssembler.getPayments()) {

			BigDecimal totalWOExtCharges = BigDecimal.ZERO;
			if (payment.includeExternalCharges()) {
				totalWOExtCharges = AccelAeroCalculator.subtract(payment.getTotalAmount(), consumedExternalCharges);
			} else {
				totalWOExtCharges = payment.getTotalAmount();
			}

			Integer paymentTnxId = payment.getPaymentTnxReference() != null
					? Integer.valueOf(payment.getPaymentTnxReference())
					: null;

			if (payment instanceof CommonLoyaltyPaymentInfo) {
				CommonLoyaltyPaymentInfo payOpt = (CommonLoyaltyPaymentInfo) payment;
				ipaymentPassenger.addLoyaltyCreditPayment(payOpt.getAgentCode(), totalWOExtCharges, payOpt.getLoyaltyAccount(),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			} else if (payment instanceof CommonCreditCardPaymentInfo) {
				CommonCreditCardPaymentInfo payOpt = (CommonCreditCardPaymentInfo) payment;
				ipaymentPassenger.addCardPayment(payOpt.getType(), payOpt.geteDate(), payOpt.getNo(), payOpt.getName(),
						payOpt.getAddress(), payOpt.getSecurityCode(), totalWOExtCharges, payOpt.getAppIndicator(),
						payOpt.getTnxMode(), payOpt.getOldCCRecordId(), payOpt.getIpgIdentificationParamsDTO(),
						payOpt.getPayReference(), payOpt.getActualPaymentMethod(), payOpt.getCarrierCode(),
						payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), null, payOpt.getPaymentBrokerId(),
						payOpt.getUserInputDTO(), paymentTnxId);
			} else if (payment instanceof LCCClientCashPaymentInfo) {
				LCCClientCashPaymentInfo payOpt = (LCCClientCashPaymentInfo) payment;
				ipaymentPassenger.addCashPayment(totalWOExtCharges, payOpt.getPayReference(), payOpt.getActualPaymentMethod(),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);

			} else if (payment instanceof LCCClientOnAccountPaymentInfo) {
				LCCClientOnAccountPaymentInfo payOpt = (LCCClientOnAccountPaymentInfo) payment;
				ipaymentPassenger.addAgentCreditPayment(payOpt.getAgentCode(), totalWOExtCharges, payOpt.getPayReference(),
						payOpt.getActualPaymentMethod(), payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(),
						payOpt.getLccUniqueTnxId(), payOpt.getPaymentMethod(), paymentTnxId);

			} else if (payment instanceof LCCClientPaxCreditPaymentInfo) {
				LCCClientPaxCreditPaymentInfo payOpt = (LCCClientPaxCreditPaymentInfo) payment;
				// pass the carrier code and pnr to facilitate any carrier credit utilization
				ipaymentPassenger.addCreditPayment(totalWOExtCharges, String.valueOf(payOpt.getDebitPaxRefNo()),
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), payOpt.getPaxSequence(),
						payOpt.getExpiryDate(), payOpt.getResidingPnr(), null, null, paymentTnxId);
			} else if (payment instanceof LCCClientLMSPaymentInfo) {
				LCCClientLMSPaymentInfo payOpt = (LCCClientLMSPaymentInfo) payment;
				ipaymentPassenger.addLMSPayment(payOpt.getLoyaltyMemberAccountId(), payOpt.getRewardIDs(), totalWOExtCharges,
						payOpt.getCarrierCode(), payOpt.getPayCurrencyDTO(), payOpt.getLccUniqueTnxId(), paymentTnxId);
			}
		}

		return ipaymentPassenger;
	}
}
