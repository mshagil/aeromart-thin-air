package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Interchange information holder
 * 
 * @author malaka
 * 
 */
public class InterchangeHeader implements Serializable {

	private static final long serialVersionUID = 1286858838058051795L;

	private String syntaxIdentifier;

	private String versionNo;

	private InterchangeInfo sender;

	private InterchangeInfo recipient;

	private Date timestamp;

	private String recipientRef;

	private String associatioinCode;

	public InterchangeHeader(String syntaxIdentifier, String versionNo, InterchangeInfo sender, InterchangeInfo recipient,
			Date timestamp, String recipientRef, String associatioinCode) {
		super();
		this.syntaxIdentifier = syntaxIdentifier;
		this.versionNo = versionNo;
		this.sender = sender;
		this.recipient = recipient;
		this.timestamp = timestamp;
		this.recipientRef = recipientRef;
		this.associatioinCode = associatioinCode;
	}

	/**
	 * @return the syntaxIdentifier
	 */
	public String getSyntaxIdentifier() {
		return syntaxIdentifier;
	}

	/**
	 * @return the sender
	 */
	public InterchangeInfo getSender() {
		return sender;
	}

	/**
	 * @return the recipient
	 */
	public InterchangeInfo getRecipient() {
		return recipient;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @return the recipientRef
	 */
	public String getRecipientRef() {
		return recipientRef;
	}

	public String getRecipientRefAssociationId() {
		return recipientRef.substring(0, 10);
	}

	public String getRecipientRefSequenceNo() {
		return recipientRef.substring(10);
	}

	/**
	 * @return the versionNo
	 */
	public String getVersionNo() {
		return versionNo;
	}

	/**
	 * @return the associatioinCode
	 */
	public String getAssociatioinCode() {
		return associatioinCode;
	}
}
