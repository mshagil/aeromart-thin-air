package com.isa.thinair.gdsservices.core.util;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;

public class TypeACodesMapper {

	private TypeACodesMapper() {
	}

	public static String getCouponStatus(String aaCouponStatus) {
		String status = null;

		if (aaCouponStatus.equals(CommonsConstants.EticketStatus.FLOWN.code())) {
			status = TypeAConstants.CouponStatus.FLOWN;
		}

		return status;
	}

}
