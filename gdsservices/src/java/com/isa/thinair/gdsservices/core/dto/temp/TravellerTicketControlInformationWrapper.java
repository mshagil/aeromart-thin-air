package com.isa.thinair.gdsservices.core.dto.temp;

import java.io.Serializable;

import com.isa.typea.common.TravellerTicketControlInformation;

public class TravellerTicketControlInformationWrapper extends TravellerInformationWrapper
		implements Serializable {

	public TravellerTicketControlInformationWrapper(TravellerTicketControlInformation travellerInformation) {
		super(travellerInformation);
	}

	public TravellerTicketControlInformation getTravellerInformation() {
		return (TravellerTicketControlInformation) super.getTravellerInformation();
	}
}
