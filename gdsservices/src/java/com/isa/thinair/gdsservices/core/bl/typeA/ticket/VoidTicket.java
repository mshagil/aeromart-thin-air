package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.config.TypeAConfig;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.KeyGenerator;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.*;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VoidTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(VoidTicket.class);

	private String pnr;
	private LCCClientReservation reservation;
	private boolean isSingleTnx;
	private Map<Integer, GDSPaxChargesTO> aaChargesByPax;
	private GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;
	private List<TravellerTicketingInformationWrapper> voidingTravellers;

	AATKTREQ tktReq;
	AATKTRES tktRes;


	protected void handleMessage() {

		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		tktreqMessage = tktReq.getMessage();

		tktRes =  new AATKTRES();
		tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);
		messageDTO.setResponse(tktRes);


		try {

			preHandleMessage();
			handleVoidTickets();
			postHandleMessage();

			prepareResp();
			handleSingleTransaction();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.VOID);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.VOID);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		}


	}


	private void preHandleMessage() throws GdsTypeAException {
		tktReq = (AATKTREQ) messageDTO.getRequest();

		preValidate();

		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
			if (reservation == null) {
				throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);
		if (externalReservation.getReservationSynced().equals(CommonsConstants.NO)) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		resolveVoidingPassengers();

	}

	private void preValidate() throws GdsTypeAException {
		TKTREQMessage tktreqMessage = tktReq.getMessage();

		OriginatorInformation originatorInformation = tktReq.getMessage().getOriginatorInformation();
		String gdsCarrierCode = originatorInformation.getSenderSystemDetails().getCompanyCode();

		TypeAConfig typeAConfig = GDSServicesModuleUtil.getConfig().getTypeAConfig();
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		GDSExternalCodes.GDS gds = null;

		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				gds = GDSExternalCodes.GDS.valueOf(gdsStatusTO.getGdsCode());
			}
		}

		TypeAConfig.TypeAFunctionConfig typeAFunctionConfig = typeAConfig.getFunctions().get(gds);

		TypeAConfig.FunctionConfig functionConfig = typeAFunctionConfig.getMessageConfig().get(TypeAConfig.Category.TICKET).get(TypeAConfig.Function
				.VOID);

		boolean functionSupported = functionConfig.isEnabled();

		if (!functionSupported) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.MESSAGE_FUNCTION_NOT_SUPPORTED, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		List<String> voidingTickets = new ArrayList<String>();
		for (TicketNumberDetails ticket : tktreqMessage.getTickets()) {
			voidingTickets.add(ticket.getTicketNumber());
		}

		Map<String, Set<String>> pnrsToEticketNo = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.getExternalETNumbersMap(voidingTickets);

		if (pnrsToEticketNo.size() != 1) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.TOO_MUCH_DATA, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		pnr = pnrsToEticketNo.keySet().iterator().next();

	}

	private void resolveVoidingPassengers() throws GdsTypeAException{
		TKTREQMessage tktreqMessage = tktReq.getMessage();

		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;

		Map<String, Map<Integer, List<String>>> groupedTickets; // pnr -> pax-id -> tkt-no
		Map<Integer, List<String>> resTickets;
		List<String> paxTickets;
		boolean voidApplicable;

		LCCClientReservationPax passenger;

		List<TravellerTicketingInformationWrapper> travellersByConjunctions;
		TravellerTicketingInformationWrapper voidingTraveller;
		Map<Integer, Set<String>> conjunctiveTickets;


		List<String> voidingTickets = new ArrayList<String>();
		for (TicketNumberDetails ticket : tktreqMessage.getTickets()) {
			voidingTickets.add(ticket.getTicketNumber());
		}

		groupedTickets = gdsTypeAServiceDAO.groupTickets(voidingTickets);
		resTickets = groupedTickets.get(pnr);

		voidingTravellers = new ArrayList<TravellerTicketingInformationWrapper>();

		for (Integer pnrPaxId : resTickets.keySet()) {
			passenger = GDSDTOUtil.resolvePax(reservation, pnrPaxId);
			paxTickets = resTickets.get(pnrPaxId);

			travellersByConjunctions = GDSDTOUtil.resolvePassengers(gdsReservation, passenger);
			conjunctiveTickets = GDSDTOUtil.groupTicketsByConjunctions(travellersByConjunctions);

			for (Set<String> conjunctiveTicket : conjunctiveTickets.values()) {
				voidApplicable = false;

				for (String ticket : paxTickets) {
					if (conjunctiveTicket.contains(ticket)) {
						voidApplicable = true;
						break;
					}
				}

				if (voidApplicable) {
					if (!passenger.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CANCEL)) {
						capturePaxTicketIssueAmounts(passenger);
						isSingleTnx = true;
					}

					voidingTraveller = GDSDTOUtil.resolvePassengerByTicketNumber(gdsReservation, conjunctiveTicket.iterator().next());
				    voidingTravellers.add(voidingTraveller);
				}

			}
		}
		
		if (voidingTravellers.size() != reservation.getPassengers().size()) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NUMBER_IN_PARTY_INVALID,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		}
	}

	private void handleVoidTickets() throws GdsTypeAException {
		TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.REFUND);
		ticketsAssembler.setPnr(reservation.getPNR());
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		for (TravellerTicketingInformationWrapper travellerWrapper : voidingTravellers) {
			voidTickets(travellerWrapper, ticketsAssembler);
		}

		try {
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}
	}

	private void voidTickets(TravellerTicketingInformationWrapper travellerWrapper, TicketsAssembler ticketsAssembler) throws GdsTypeAException {

		BigDecimal totalRefundingAmount;
		TravellerTicketingInformation traveller = travellerWrapper.getTravellerInformation();
		LCCClientReservationPax pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

		// amount to be refunded - all conjunctive tickets
		FinancialInformationAssembler financialInformationAssembler = new FinancialInformationAssembler();
		financialInformationAssembler.setFinancialInformation(traveller.getText());
		financialInformationAssembler.setTaxDetails(traveller.getTaxDetails());
		financialInformationAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
		financialInformationAssembler.calculateAmounts();

		totalRefundingAmount = financialInformationAssembler.getFareAmount()
				.add(financialInformationAssembler.getTaxAmount())
				.add(financialInformationAssembler.getSurcharge());


		// adjustment
		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
		gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
		gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
		gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());

		GDSPaxChargesTO paxChargesTO = new GDSPaxChargesTO();
		paxChargesTO.setFareAmount(BigDecimal.ZERO);
		paxChargesTO.setTaxAmount(BigDecimal.ZERO);
		paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
		paxChargesTO.setPaxType(pax.getPaxType());
		paxChargesTO.setInactiveSegmentsApplicable(true);

		gdsChargeAdjustmentRQ.addGdsCharges(PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()), paxChargesTO);

		try {

			GDSServicesModuleUtil.getReservationBD().syncAATotalGroupChargesWithCarrier(gdsChargeAdjustmentRQ, null);
			// refund
			TicketsAssembler.PassengerAssembler passengerAssembler = GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, traveller, pax);
			passengerAssembler.setTotalAmount(totalRefundingAmount);

		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

		// model changes
		String settleAuthCode = KeyGenerator.generateSettlementAuthorizationCode();
		for (TicketNumberDetails ticket : traveller.getTickets()) {
			for (TicketCoupon coupon : ticket.getTicketCoupon()) {
				coupon.setStatus(TypeAConstants.CouponStatus.VOIDED);
				coupon.setSettlementAuthCode(settleAuthCode);
			}
		}


	}

	private void postHandleMessage() throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();
		LCCClientReservationPax pax;

		for (TravellerTicketingInformationWrapper travellerWrapper : voidingTravellers) {
			TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();
			List<TicketNumberDetails> transitions;

			if (travellerWrapper != null) {
				travellerWrapper.setOriginatorInformation(tktreqMessage.getOriginatorInformation());
				travellerWrapper.setTicketingAgentInformation(tktreqMessage.getTicketingAgentInformation());

				transitions = new ArrayList<TicketNumberDetails>();

				for (TicketNumberDetails ticket : travellerImage.getTickets()) {

					TicketNumberDetails ticketNumberDetails = new TicketNumberDetails();
					ticketNumberDetails.setTicketNumber(ticket.getTicketNumber());
					ticketNumberDetails.setDocumentType(ticket.getDocumentType());
					transitions.add(ticketNumberDetails);

					for (TicketCoupon coupon : ticket.getTicketCoupon()) {

						TicketCoupon ticketCoupon = new TicketCoupon();
						ticketCoupon.setStatus(coupon.getStatus());
						ticketCoupon.setCouponNumber(coupon.getCouponNumber());
						ticketCoupon.setSettlementAuthCode(coupon.getSettlementAuthCode());
						ticketCoupon.setOriginatorInformation(tktreqMessage.getOriginatorInformation());
						ticketCoupon.setFlightInfomation(coupon.getFlightInfomation());
						ticketCoupon.setRelatedProductInfo(coupon.getRelatedProductInfo());

						ticketNumberDetails.getTicketCoupon().add(ticketCoupon);
					}

				}

				GDSDTOUtil.mergeCouponTransitions(travellerWrapper.getCouponTransitions(), transitions,
						tktreqMessage.getOriginatorInformation());

			}


			GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);


			try {
				reservation = GDSServicesUtil.loadLccReservation(reservation.getPNR());
			} catch (ModuleException e) {
				throw new GdsTypeAException();
			}

			List<LccClientPassengerEticketInfoTO> lccCoupons;

			List<TicketNumberDetails> tickets = new ArrayList<TicketNumberDetails>();
			TicketNumberDetails ticket;
			TicketCoupon coupon;

			for (TicketNumberDetails t : travellerImage.getTickets()) {
				if (t.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.NEW)) {
					ticket = new TicketNumberDetails();
					ticket.setTicketNumber(t.getTicketNumber());
					ticket.setDocumentType(TypeAConstants.DocumentType.TICKET);

					for (TicketCoupon c : t.getTicketCoupon()) {
						coupon = new TicketCoupon();
						coupon.setCouponNumber(c.getCouponNumber());
						coupon.setStatus(c.getStatus());
						coupon.setFlightInfomation(c.getFlightInfomation());
						coupon.setSettlementAuthCode(c.getSettlementAuthCode());

						ticket.getTicketCoupon().add(coupon);
					}

					tickets.add(ticket);
				}
			}

			pax = GDSDTOUtil.resolveReservationPax(travellerImage, travellerImage.getTravellers().get(0), reservation);
			lccCoupons = ReservationDtoMerger.getMergedETicketCoupons(tickets, pax);


			List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

			try {
			 	ServiceResponce responce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);

				if (!responce.isSuccess()) {
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}
			} catch (ModuleException e) {
				throw new GdsTypeAException();
			}
		}


	}

	private void prepareResp() {

		TKTRESMessage tktresMessage = tktRes.getMessage();

		TravellerTicketingInformation traveller;

		TicketNumberDetails ticketNumberDetails;
		TicketCoupon ticketCoupon;

		MessageFunction messageFunction = tktReq.getMessage().getMessageFunction();
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

		for (TravellerTicketingInformationWrapper travellerWrapper : voidingTravellers) {
			traveller = travellerWrapper.getTravellerInformation();

			for (TicketNumberDetails ticket : traveller.getTickets()) {
				ticketNumberDetails = new TicketNumberDetails();
				ticketNumberDetails.setTicketNumber(ticket.getTicketNumber());
				ticketNumberDetails.setDocumentType(TypeAConstants.DocumentType.TICKET);
				for (TicketCoupon coupon : ticket.getTicketCoupon()) {
					ticketCoupon = new TicketCoupon();
					ticketCoupon.setCouponNumber(coupon.getCouponNumber());
					ticketCoupon.setStatus(coupon.getStatus());
					ticketCoupon.setSettlementAuthCode(coupon.getSettlementAuthCode());

					ticketNumberDetails.getTicketCoupon().add(ticketCoupon);
				}
				tktresMessage.getTickets().add(ticketNumberDetails);
			}

		}
	}

	
	
	private void handleSingleTransaction() throws GdsTypeAException {
		if (isSingleTnx && aaChargesByPax != null && !aaChargesByPax.isEmpty()) {
			// TODO:
			// validate all pax details present
			GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
			gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
			gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
			gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());

			gdsChargeAdjustmentRQ.setGdsChargesByPax(aaChargesByPax);
			try {
				GDSServicesModuleUtil.getReservationBD().syncAATotalGroupChargesWithCarrier(gdsChargeAdjustmentRQ, null);
			} catch (ModuleException e) {
				throw new GdsTypeAException();
			}
		}
	}
	
	private BigDecimal[] appliedGDSChargeGroupAdj(List<FeeTO> fees) {

		BigDecimal totalSurChargeAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalFareAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalTaxAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalOtherAdj = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (fees != null && !fees.isEmpty()) {
			for (FeeTO feeTO : fees) {
				if (ChargeGroup.ADJ.equals(feeTO.getFeeCode())) {
					if (AirinventoryCustomConstants.ChargeCodes.GDS_SUR_GROUP_CHARGE_ADJ.equals(feeTO.getChargeCode())) {
						totalSurChargeAdj = AccelAeroCalculator.add(totalSurChargeAdj, feeTO.getAmount());
					} else if (AirinventoryCustomConstants.ChargeCodes.GDS_FARE_GROUP_CHARGE_ADJ.equals(feeTO.getChargeCode())) {
						totalFareAdj = AccelAeroCalculator.add(totalFareAdj, feeTO.getAmount());
					} else if (AirinventoryCustomConstants.ChargeCodes.GDS_TAX_GROUP_CHARGE_ADJ.equals(feeTO.getChargeCode())) {
						totalTaxAdj = AccelAeroCalculator.add(totalTaxAdj, feeTO.getAmount());
					} else if (AirinventoryCustomConstants.ChargeCodes.GDS_OTHER_GROUP_CHARGE_ADJ.equals(feeTO.getChargeCode())) {
						totalOtherAdj = AccelAeroCalculator.add(totalOtherAdj, feeTO.getAmount());
					}

				}

			}
		}

		BigDecimal totalChargeAdj[] = { totalFareAdj, totalTaxAdj, totalSurChargeAdj, totalOtherAdj };

		return totalChargeAdj;
	}
	
	private void capturePaxTicketIssueAmounts(LCCClientReservationPax pax) {
		if (aaChargesByPax == null) {
			aaChargesByPax = new HashMap<Integer, GDSPaxChargesTO>();
		}

		BigDecimal[] totalAdj = appliedGDSChargeGroupAdj(pax.getFees());
		GDSPaxChargesTO paxChargesTO = new GDSPaxChargesTO();
		paxChargesTO.setFareAmount(AccelAeroCalculator.add(pax.getTotalFare(), totalAdj[0]));
		paxChargesTO.setTaxAmount(AccelAeroCalculator.add(pax.getTotalTaxCharge(), totalAdj[1]));
		paxChargesTO.setSurChargeAmount(AccelAeroCalculator.add(pax.getTotalSurCharge(), totalAdj[2]));

		paxChargesTO.setOtherAmount(AccelAeroCalculator.add(pax.getTotalFare(), pax.getTotalTaxCharge(), pax.getTotalSurCharge())
				.negate());
		paxChargesTO.setPaxType(pax.getPaxType());
		paxChargesTO.setInactiveSegmentsApplicable(true);

		aaChargesByPax.put(PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()), paxChargesTO);
	}
}
