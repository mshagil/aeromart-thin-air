/*
 * 
==============================================================================
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
=============================================================================== 
 */

package com.isa.thinair.gdsservices.core.command;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.CodeShareFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightSegmentStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.Child;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Infant;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.PaymentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.dto.internal.SSRTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SingleSegment;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.PaymentType;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ResponseMessageCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.SegmentType;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.msgbroker.api.dto.BookingSegmentDTO;
import com.isa.thinair.msgbroker.core.bl.TypeBMessageConstants;
import com.isa.typea.common.FlightInformation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Dhanushka
 */
public class CommonUtil {

	private static Log log = LogFactory.getLog(CommonUtil.class);

	static SimpleDateFormat dtfmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	static SimpleDateFormat datefmt = new SimpleDateFormat("dd-MMM-yyyy HHmm");

	public static void validateReservation(GDSCredentialsDTO gdsCredentialsDTO, Reservation reservation) throws ModuleException {
		checkReservationExistance(reservation);
		checkReservationOwner(gdsCredentialsDTO, reservation);
		checkCancelledReservation(reservation);
	}
	
	public static void validateReservationOwner(GDSCredentialsDTO gdsCredentialsDTO, Reservation reservation) throws ModuleException {
		checkReservationExistance(reservation);
		checkReservationOwner(gdsCredentialsDTO, reservation);
	}
	
	public static void validateReservationStatus(GDSCredentialsDTO gdsCredentialsDTO, Reservation reservation) throws ModuleException {
		checkReservationExistance(reservation);
		checkCancelledReservation(reservation);
	}
	
	private static void checkReservationExistance(Reservation reservation) throws ModuleException {
		if (reservation == null) {
			throw new ModuleException("gdsservices.actions.reservationDoesNotExist");
		}
	}

	/**
	 * Check whether child only bookings exist when creating a booking
	 * 
	 * @param passengers
	 *            this can be adult, child or infant
	 * @throws ModuleException
	 */
	public static void checkChildOnlyBookingExistWhenCreating(Collection<Passenger> passengers) throws ModuleException {
		if (isChildOnlyBookingExistWhenCreating(passengers)) {
			throw new ModuleException("gdsservices.actions.childOnlyBookings.notAllowed");
		}
	}

	/**
	 * Whether any one adult exist in the given passengers
	 * 
	 * @param passengers
	 * @return
	 */
	private static boolean isChildOnlyBookingExistWhenCreating(Collection<Passenger> passengers) {
		for (Passenger pax : passengers) {
			if (ReservationInternalConstants.PassengerType.ADULT.equals(pax.getPaxType())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Check whether child only bookings exist when spliting
	 * 
	 * @param pnrPaxIds
	 * @param reservation
	 * @throws ModuleException
	 */
	public static void checkChildOnlyBookingExistWhenSpliting(Collection<Integer> pnrPaxIds, Reservation reservation)
			throws ModuleException {
		if (isChildOnlyBookingExistWhenSpliting(pnrPaxIds, reservation, true)
				|| isChildOnlyBookingExistWhenSpliting(pnrPaxIds, reservation, false)) {
			throw new ModuleException("gdsservices.actions.childOnlyBookings.notAllowed");
		}
	}

	/**
	 * Is child only bookings exists when splitting
	 * 
	 * @param pnrPaxIds
	 * @param reservation
	 * @param isForTheOldPNR
	 * @return
	 */
	private static boolean isChildOnlyBookingExistWhenSpliting(Collection<Integer> pnrPaxIds, Reservation reservation,
			boolean isForTheOldPNR) {
		ReservationPax reservationPax;

		for (Iterator itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (ReservationApiUtils.isAdultType(reservationPax)) {
				if (isForTheOldPNR) {
					if (!pnrPaxIds.contains(reservationPax.getPnrPaxId())) {
						return false;
					}
				} else {
					if (pnrPaxIds.contains(reservationPax.getPnrPaxId())) {
						return false;
					}
				}

			}
		}

		return true;
	}

	/**
	 * Is child only bookings exists when splitting in new pnr
	 * 
	 * @param pnrPaxIds
	 * @param reservation
	 * @return
	 */
	private static boolean isChildOnlyBookingExistWhenSplitingInTheNewPNR(Collection<Integer> pnrPaxIds, Reservation reservation) {
		ReservationPax reservationPax;

		for (Iterator itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = (ReservationPax) itReservationPax.next();

			if (pnrPaxIds.contains(reservationPax.getPnrPaxId()) && ReservationApiUtils.isAdultType(reservationPax)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks whether the current Agent is the owner of the reservation
	 * 
	 * @param gdsCredentialsDTO
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void checkReservationOwner(GDSCredentialsDTO gdsCredentialsDTO, Reservation reservation)
			throws ModuleException {
		if (!isReservationOwner(gdsCredentialsDTO, reservation)) {
			throw new ModuleException("gdsservices.actions.reservation.localized");
		}
	}

	/**
	 * Checks if the current Agent is the owner of the reservation
	 * 
	 * @param gdsCredentialsDTO
	 * @param reservation
	 * @return boolean
	 * @throws ModuleException
	 */
	public static boolean isReservationOwner(GDSCredentialsDTO gdsCredentialsDTO, Reservation reservation) {

		String accessingUserID = gdsCredentialsDTO.getUserId();
		Integer gdsID = reservation.getGdsId();
		if (gdsID != null) {
			String reservationGDSUser = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap().get(gdsID.toString()).getUserId();
			if (reservation.getAdminInfo() != null && reservationGDSUser != null && accessingUserID.equals(reservationGDSUser)) {
				return true;
			}
		} else {
			List<String> csOcCarrierCodes = new ArrayList<String>(); //TODO: change this to hashset
			for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {
				if (reservationSegmentDTO.getCsOcCarrierCode() != null
						&& !AppSysParamsUtil.getDefaultCarrierCode().equals(reservationSegmentDTO.getCsOcCarrierCode())) {
					csOcCarrierCodes.add(reservationSegmentDTO.getCsOcCarrierCode());
				}
			}
			if (!csOcCarrierCodes.isEmpty()) {
				Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
				for (Entry<String, GDSStatusTO> entry : gdsStatusMap.entrySet()) {
					//TODO: need to check this for all csOcCarrierCodes
					if (entry.getValue().getCarrierCode().equals(csOcCarrierCodes.get(0))) {
						String reservationGDSUser = entry.getValue().getUserId();
						if (reservation.getAdminInfo() != null && reservationGDSUser != null
								&& accessingUserID.equals(reservationGDSUser)) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}
	
	
	private static String getGDSName(Integer gdsID) {
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		GDSStatusTO gdsStatus = gdsStatusMap.get(gdsID.toString());
		if (gdsStatus != null) {
			return gdsStatus.getGdsCode();
		}
		return null;
	}

	/**
	 * checks whether the reservation made using a credit card
	 * 
	 * @param reservation
	 * @param paymentDetail
	 * @throws ModuleException
	 */
	public static void validatePaymentOptions(Reservation reservation, PaymentDetail paymentDetail) throws ModuleException {

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			Collection<Integer> pnrPaxIds = getPNRPaxIds(reservation.getPassengers());
			Map pnrPaxIdAndColReservationTnx = GDSServicesModuleUtil.getPassengerBD().getPNRPaxPaymentsAndRefunds(pnrPaxIds);
			List nominalCodes = ReservationTnxNominalCode.getCreditCardNominalCodes();
			Iterator itColPayments = pnrPaxIdAndColReservationTnx.values().iterator();
			Collection colPayments = (Collection) itColPayments.next();

			boolean isCCPayment = checkPaymentIsNotACCPayment(colPayments, nominalCodes);

			if (isCCPayment && (paymentDetail == null || paymentDetail.getPaymentType() != PaymentType.CREDIT_CARD)) {
				throw new ModuleException("gdsservices.actions.creditcard.payment.required");
			} else if (!isCCPayment && (paymentDetail == null || paymentDetail.getPaymentType() != PaymentType.BSP)) {
				throw new ModuleException("gdsservices.actions.bsp.payment.required");
			}
		}
	}
	
	/**
	 * 
	 * @param reservation
	 * @param totalPaidAmount
	 * @throws ModuleException
	 */
	public static void validatePaymentOptionsForConfirmedReservations(Reservation reservation, BigDecimal totalPaidAmount,
			boolean nameChangeExists) throws ModuleException {
		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus()) && nameChangeExists) {
			if (totalPaidAmount.intValue() > 0 ) {
				throw new ModuleException("gdsservices.actions.bsp.payment.required");
			}
		}
	}

	/**
	 * Checks whether the reservation was canceled.
	 * 
	 * @param reservation
	 * @throws ModuleException
	 */
	private static void checkCancelledReservation(Reservation reservation) throws ModuleException {
		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			throw new ModuleException("gdsservices.response.message.invalidPnrCancel");
		}
	}

	private static boolean checkPaymentIsNotACCPayment(Collection colPayments, List nominalCodes) {
		ReservationTnx reservationTnx;

		for (Iterator itReservationTnx = colPayments.iterator(); itReservationTnx.hasNext();) {
			reservationTnx = (ReservationTnx) itReservationTnx.next();

			if (nominalCodes.contains(reservationTnx.getNominalCode())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get reservation
	 * 
	 * @param gdsCredentialsDTO
	 * @param originatorPnr
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation loadReservation(GDSCredentialsDTO gdsCredentialsDTO, String extRecordLocator, String localPnr)
			throws ModuleException {

		// FIXME Nili - We don't need to load all the information
		// Let's finalize and optimize this
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		if (extRecordLocator != null) {
			pnrModesDTO.setExtRecordLocatorId(extRecordLocator);
		} else {
			pnrModesDTO.setPnr(localPnr);
		}

		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadEtickets(true);

		return GDSServicesModuleUtil.getReservationBD().getReservation(pnrModesDTO, null);
	}

	public static Reservation loadReservationFromExternalLocator(String exLocator) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setExtRecordLocatorId(exLocator);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadLastUserNote(true);

		return GDSServicesModuleUtil.getReservationBD().getReservation(pnrModesDTO, null);
	}

	/**
	 * Get unique key for each Segment
	 * 
	 * @param flightNo
	 * @param segmentCode
	 * @param departureDate
	 * @return
	 * @throws ModuleException
	 */
	public static String getSegMapKey(String flightNo, String segmentCode, Date departureDate, String bookingClass) throws ModuleException {
		// Nili 10:51 AM 9/2/2008
		// Business Assumption:
		// For all cancellations the time will be not sent from GDS
		return GDSApiUtils.maskNull(flightNo).toUpperCase() + bookingClass + segmentCode + BeanUtils.parseDateFormat(departureDate, "yyMMdd");
	}
	
	public static String getSegMapKeyWithoutBC(String flightNo, String segmentCode, Date departureDate) throws ModuleException {
		return GDSApiUtils.maskNull(flightNo).toUpperCase() + segmentCode + BeanUtils.parseDateFormat(departureDate, "yyMMdd");
	}
	
	public static String getSegMapKeyWithoutYear(String flightNo, String segmentCode, Date departureDate, String bookingClass) throws ModuleException {
		return GDSApiUtils.maskNull(flightNo).toUpperCase() + bookingClass + segmentCode + BeanUtils.parseDateFormat(departureDate, "MMdd");
	}

	public static CreateReservationRequest getExistingReservation(Reservation reservation,
			CreateReservationRequest createReservationRequest) throws ModuleException {
		// Setting Segment Details ---
		Collection<SegmentDetail> segmentDetails = getSegments(reservation.getSegmentsView(),
				ReservationApiUtils.getBookingClasses(reservation));
		createReservationRequest.setNewSegmentDetails(segmentDetails);

		// Setting External Segment Details ---
		Collection<Segment> externalSegments = getExtSegments(reservation.getExternalReservationSegment());
		createReservationRequest.setExternalSegments(externalSegments);

		// Setting Contact Details ---
		ContactDetail contactDetail = getContactDetails(reservation.getContactInfo());
		createReservationRequest.setContactDetail(contactDetail);

		// Setting Passenger Details ---
		Collection<Passenger> passengers = getPassengers(reservation.getPassengers());
		createReservationRequest.setPassengers(passengers);

		// Setting Reservation PNR ---
		RecordLocator responderRecordLocator = new RecordLocator();
		responderRecordLocator.setPnrReference(reservation.getPnr());
		createReservationRequest.setResponderRecordLocator(responderRecordLocator);

		createReservationRequest.setSuccess(true);

		createReservationRequest = CommonUtil.addResponseMessage(createReservationRequest, reservation.getTotalPaidAmount(),
				reservation.getReleaseTimeStamps()[0], reservation.getLastCurrencyCode(), reservation.getLastModificationTimestamp(), false, null);

		return createReservationRequest;

	}

	private static ContactDetail getContactDetails(ReservationContactInfo contactInfo) {
		ContactDetail newContact = new ContactDetail();
		newContact.setCity(contactInfo.getCity());
		newContact.setCountry(contactInfo.getCountryCode());
		newContact.setEmailAddress(contactInfo.getEmail());
		newContact.setFaxNumber(contactInfo.getFax());
		newContact.setFirstName(contactInfo.getFirstName());
		newContact.setLandPhoneNumber(contactInfo.getPhoneNo());
		newContact.setLastName(contactInfo.getLastName());
		newContact.setMobilePhoneNumber(contactInfo.getMobileNo());
		newContact.setStreetAddress(contactInfo.getStreetAddress1() + ", " + contactInfo.getStreetAddress2());
		newContact.setTitle(contactInfo.getTitle());

		return newContact;
	}

	/**
	 * adds credit card payment response message as a SSR
	 * 
	 * @param createReservationRequest
	 * @param totalAmount
	 * @param onholdReleaseTime
	 * @param departureDate TODO
	 * @param allowSendSSRADTK TODO
	 * @return
	 */
	public static CreateReservationRequest addResponseMessage(CreateReservationRequest createReservationRequest,
			BigDecimal totalAmount, Date onholdReleaseTime, String ohdCurrency, Date ohdLastModification, boolean allowSendSSRADTK, Date departureDate) throws ModuleException {
		PaymentDetail paymentDetail = (PaymentDetail) createReservationRequest.getPaymentDetail();
		CurrencyExchangeRate currencyExchangeRate;
		String currency = AppSysParamsUtil.getBaseCurrency();		
		if (ohdCurrency != null && ohdCurrency.length() == 3 && !currency.equals(ohdCurrency) && ohdLastModification != null) {
			currencyExchangeRate = new ExchangeRateProxy(ohdLastModification).getCurrencyExchangeRate(ohdCurrency,
					ApplicationEngine.XBE);
			if (currencyExchangeRate != null) {
				currency = ohdCurrency;
				totalAmount = AccelAeroCalculator.multiplyDefaultScale(currencyExchangeRate.getMultiplyingExchangeRate(), totalAmount);
			}
		}
		String message = "";

		if (paymentDetail != null) {
			if (paymentDetail.getPaymentType() == PaymentType.CREDIT_CARD) {
				message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_CC_PAYMENT, currency,
						totalAmount);
			} else if (paymentDetail.getPaymentType() == PaymentType.BSP) {
				message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_BSP_PAYMENT, currency,
						totalAmount);
			}
		} else {
			if (GDSServicesModuleUtil.getConfig().isShowPaymentDetailsInTTYMessage()) {
				message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD, currency,
						totalAmount, onholdReleaseTime);
			} else {
				
				int gdsConfirmDurationInMillis = 0;
				if (ifStatusReplyRequired(createReservationRequest)) {
					gdsConfirmDurationInMillis = (int) AppSysParamsUtil.getGdsConfirmationOnholdDurationInMillis();
				} else {
					gdsConfirmDurationInMillis = (int) AppSysParamsUtil.getGdsTicktingOnholdDurationInMillis();
				}
				
				if (allowSendSSRADTK) {
					message = CommonUtil.getFormattedMessage(
							GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS_WITH_SSRADTK, onholdReleaseTime);
				} else if (departureDate != null && CalendarUtil.addMilliSeconds(onholdReleaseTime, gdsConfirmDurationInMillis).after(departureDate)) {
					message = CommonUtil.getFormattedMessage(
							GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS_IN_BUFFERTIME, onholdReleaseTime);
				} else {
					message = CommonUtil.getFormattedMessage(
							GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS, onholdReleaseTime);
				}
			}
			if (createReservationRequest.isNotSupportedSSRExists()) {
				message = message+MessageUtil.getMessage(GDSInternalCodes.ResponseMessageCode.SSR_NOT_SUPPORTED.getCode());
			}
		}

		createReservationRequest.setResponseMessage(message);

		return createReservationRequest;
	}

	/**
	 * adds credit card payment response message as a SSR
	 * 
	 * @param addSegmentRequest
	 * @param totalAmount
	 * @param onholdReleaseTime
	 * @return
	 */
	public static String getResponseMessage(PaymentDetail paymentDetail, BigDecimal totalAmount, Date onholdReleaseTime)
			throws ModuleException {
		String currency = AppSysParamsUtil.getBaseCurrency();
		String message = "";

		if (paymentDetail != null) {
			if (paymentDetail.getPaymentType() == PaymentType.CREDIT_CARD) {
				message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.MODIFY_CC_PAYMENT, currency,
						totalAmount);
			} else if (paymentDetail.getPaymentType() == PaymentType.BSP) {
				message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.MODIFY_BSP_PAYMENT, currency,
						totalAmount);
			}
		} else {
			message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.MODIFY_FORCE_CONFIRM, currency,
					totalAmount, onholdReleaseTime);
		}

		return message;
	}

	private static Collection<Segment> getExtSegments(Collection<ExternalPnrSegment> colReservationExternalSegment) {
		Collection<Segment> externalSegments = null;

		if (colReservationExternalSegment != null && !colReservationExternalSegment.isEmpty()) {
			externalSegments = new ArrayList<Segment>();

			for (ExternalPnrSegment extSeg : colReservationExternalSegment) {
				Segment segment = new Segment();

				ExternalFlightSegment externalFlightSegment = extSeg.getExternalFlightSegment();
				String[] segmentStations = externalFlightSegment.getSegmentCode().split("/");
				segment.setDepartureStation(segmentStations[0].trim().toUpperCase());
				segment.setArrivalStation(segmentStations[segmentStations.length - 1].trim().toUpperCase());

				segment.setDepartureDate(externalFlightSegment.getEstTimeDepatureLocal());
				segment.setArrivalDate(externalFlightSegment.getEstTimeArrivalLocal());

				segment.setDepartureTime(externalFlightSegment.getEstTimeDepatureLocal());
				segment.setArrivalTime(externalFlightSegment.getEstTimeArrivalLocal());

				// FIXME Day off set is hard coded
				segment.setDepartureDayOffset(0);
				// FIXME Day off set is hard coded
				segment.setArrivalDayOffset(0);

				segment.setFlightNumber(externalFlightSegment.getFlightNumber());
				//segment.setCodeShareFlightNo(externalFlightSegment.getCodeShareFlightNo());
				//segment.setCodeShareBc(externalFlightSegment.getCodeShareBc());

				if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(extSeg.getStatus())) {
					segment.setStatusCode(GDSInternalCodes.StatusCode.CONFIRMED);
				} else {
					segment.setStatusCode(GDSInternalCodes.StatusCode.CANCELLED);
				}

				externalSegments.add(segment);
			}
		}

		return externalSegments;
	}

	private static Collection<SegmentDetail> getSegments(Collection<ReservationSegmentDTO> segmentsView, Map map) {
		Collection<SegmentDetail> segments = new ArrayList<SegmentDetail>();

		for (ReservationSegmentDTO segmentView : segmentsView) {

			Segment seg = new Segment();

			String[] segmentStations = segmentView.getSegmentCode().split("/");
			seg.setDepartureStation(segmentStations[0].trim().toUpperCase());
			seg.setArrivalStation(segmentStations[segmentStations.length - 1].trim().toUpperCase());

			seg.setDepartureDate(CalendarUtil.truncateTime(segmentView.getDepartureDate()));
			seg.setArrivalDate(CalendarUtil.truncateTime(segmentView.getArrivalDate()));

			seg.setDepartureTime(CalendarUtil.getOnlyTime(segmentView.getDepartureDate()));
			seg.setArrivalTime(CalendarUtil.getOnlyTime(segmentView.getArrivalDate()));

			// FIXME Day off set is hard coded
			seg.setDepartureDayOffset(0);
			// FIXME Day off set is hard coded
			seg.setArrivalDayOffset(0);

			BookingClass bookingClass = (BookingClass) map.get(segmentView.getPnrSegId());
			seg.setBookingCode(bookingClass.getBookingCode());

			seg.setFlightNumber(segmentView.getFlightNo());
			seg.setCodeShareFlightNo(segmentView.getCodeShareFlightNo());
			seg.setCodeShareBc(segmentView.getCodeShareBc());

			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segmentView.getStatus())) {
				seg.setStatusCode(GDSInternalCodes.StatusCode.EXISTS);
			} else {
				seg.setStatusCode(GDSInternalCodes.StatusCode.CANCELLED);
			}

			// Assumption
			// Only single segments are supported
			SingleSegment singleSegment = new SingleSegment();
			singleSegment.setSegment(seg);

			segments.add(singleSegment);
		}

		return segments;
	}

	private static Collection<Passenger> getPassengers(Collection<ReservationPax> colReservationPax) {
		Collection<Passenger> passengers = null;

		if (colReservationPax != null && !colReservationPax.isEmpty()) {
			passengers = new ArrayList<Passenger>();

			for (ReservationPax pax : colReservationPax) {
				if (ReservationApiUtils.isAdultType(pax)) {
					Adult adult = new Adult();
					adult.setTitle(pax.getTitle());
					adult.setFirstName(pax.getFirstName());
					adult.setLastName(pax.getLastName());

					adult.setSsrCollection(getSpecialServiceRequest(pax));
					Collection<ReservationPax> infants = pax.getInfants();

					if (infants != null && !infants.isEmpty()) {
						for (ReservationPax infant : infants) {
							Infant newInfant = new Infant();
							newInfant.setTitle(infant.getTitle());
							newInfant.setFirstName(infant.getFirstName());
							newInfant.setLastName(infant.getLastName());

							newInfant.setSsrCollection(getSpecialServiceRequest(infant));

							newInfant.setParent(adult);
							adult.setInfant(newInfant);

							passengers.add(newInfant);
						}
					}

					passengers.add(adult);
				} else if (ReservationApiUtils.isChildType(pax)) {
					Child child = new Child();
					child.setTitle(pax.getTitle());
					child.setFirstName(pax.getFirstName());
					child.setLastName(pax.getLastName());

					child.setSsrCollection(getSpecialServiceRequest(pax));
					passengers.add(child);
				}
			}
		}
		return passengers;
	}
	
	public static List<CodeShareFlightDTO> createCodeShareFlightInfo(Collection<Segment> segments,  Collection<ReservationSegmentDTO> resSegments) {
		List<CodeShareFlightDTO> codeShareFlightDTOs = null;
		for (Segment segment : segments) {
			if (segment.getCodeShareFlightNo() != null) {
				CodeShareFlightDTO codeShareFlightDTO = new CodeShareFlightDTO();
				codeShareFlightDTO.setFlightNumber(segment.getFlightNumber());
				codeShareFlightDTO.setBookingClass(segment.getBookingCode());
				codeShareFlightDTO.setCodeShareFlightNumber(segment.getCodeShareFlightNo());
				codeShareFlightDTO.setCodeShareBookingClass(segment.getCodeShareBc());
				codeShareFlightDTO.setDepartureDate(segment.getDepartureDate());
				if (codeShareFlightDTOs != null) {
					codeShareFlightDTOs.add(codeShareFlightDTO);
				} else {
					codeShareFlightDTOs = new ArrayList<CodeShareFlightDTO>();
					codeShareFlightDTOs.add(codeShareFlightDTO);
				}
			}
		}
		for (ReservationSegmentDTO segment : resSegments) {
			if (segment.getCodeShareFlightNo() != null) {
				CodeShareFlightDTO codeShareFlightDTO = new CodeShareFlightDTO();
				codeShareFlightDTO.setFlightNumber(segment.getFlightNo());
				codeShareFlightDTO.setBookingClass(segment.getBookingClass());
				codeShareFlightDTO.setCodeShareFlightNumber(segment.getCodeShareFlightNo());
				codeShareFlightDTO.setCodeShareBookingClass(segment.getCodeShareBc());
				codeShareFlightDTO.setDepartureDate(segment.getDepartureDate());
				if (codeShareFlightDTOs != null) {
					codeShareFlightDTOs.add(codeShareFlightDTO);
				} else {
					codeShareFlightDTOs = new ArrayList<CodeShareFlightDTO>();
					codeShareFlightDTOs.add(codeShareFlightDTO);
				}
			}
		}
		return codeShareFlightDTOs;
	}

//	public static String[] getSSRInfo(Collection<SpecialServiceRequest> ssrCollection, ReservationAssembler rAssembler,
//			Collection<OndFareDTO> collFares) {
//		String[] ssrArr = new String[] { null, null, null };
//
//		if (ssrCollection != null && !ssrCollection.isEmpty()) {
//			String firstSSRCode = null;
//			StringBuilder otherSSRInfo = new StringBuilder();
//			Integer segSequence = null;
//
//			for (SpecialServiceRequest ssr : ssrCollection) {
//				// Need to skip group booking SSR codes
//				if (!TypeBMessageConstants.SSRCodes.GRPF.equals(ssr.getCode().trim())
//						&& !TypeBMessageConstants.SSRCodes.GRPS.equals(ssr.getCode().trim())) {
//					if (firstSSRCode == null || firstSSRCode.equals("")) {
//						firstSSRCode = ssr.getCode().trim();
//					} else {
//						otherSSRInfo.append(" ");
//					}
//					otherSSRInfo.append(ssr.getCode().trim() + " " + ssr.getValue().trim());
//
//					if (ssr instanceof SpecialServiceDetailRequest) {
//						SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
//						if (ssdr.getSegment() != null && rAssembler != null && collFares != null) {
//							for (OndFareDTO ondFareDTO : collFares) {
//								OndFareDTO ondFare = ondFareDTO;
//								Set tempSegIds = ondFare.getSegmentsMap().keySet();
//
//								for (Iterator iterator = tempSegIds.iterator(); iterator.hasNext();) {
//									FlightSegmentDTO element = ondFare.getSegmentsMap().get(iterator.next());
//									if (element.getArrivalAirportCode().equals(ssdr.getSegment().getArrivalStation())
//											&& element.getDepartureAirportCode().equals(ssdr.getSegment().getDepartureStation())
//											&& CalendarUtil.isEqualStartTimeOfDate(element.getDepatureDate(), ssdr.getSegment()
//													.getDepartureDate())
//											&& element.getFlightNumber().substring(2).equals(ssdr.getSegment().getFlightNumber())) {
//										for (ReservationSegment segment : rAssembler.getDummyReservation().getSegments()) {
//											if (segment.getFlightSegId().intValue() == element.getSegmentId().intValue()) {
//												segSequence = segment.getSegmentSeq();
//											}
//										}
//									}
//								}
//							}
//						}
//
//					}
//				}
//			}
//			ssrArr[0] = firstSSRCode;
//			if (ssrArr[0] != null) {
//				ssrArr[1] = otherSSRInfo.toString();
//				if (segSequence != null) {
//					ssrArr[2] = segSequence.toString();
//				}
//			}
//		}
//
//		return ssrArr;
//	}
	
	public static List<SSRTO> getSSRInfo(Collection<SpecialServiceRequest> ssrCollection, ReservationAssembler rAssembler,
			Collection<OndFareDTO> collFares) {

		List<SSRTO> paxSegmentSSRs = null;

		if (ssrCollection != null && !ssrCollection.isEmpty()) {
			paxSegmentSSRs = new ArrayList<SSRTO>();

			for (SpecialServiceRequest ssr : ssrCollection) {
				SSRTO ssrTo = new SSRTO();
				String ssrCode = null;
				StringBuilder otherSSRInfo = new StringBuilder();
				Integer segSequence = null;

				// Need to skip group booking SSR codes
				if (!TypeBMessageConstants.SSRCodes.GRPF.equals(ssr.getCode().trim())
						&& !TypeBMessageConstants.SSRCodes.GRPS.equals(ssr.getCode().trim())) {
					ssrCode = ssr.getCode().trim();
					otherSSRInfo.append(ssr.getCode().trim() + " " + ssr.getValue().trim());

					if (ssr instanceof SpecialServiceDetailRequest) {
						SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
						if (ssdr.getSegment() != null && rAssembler != null && collFares != null) {
							for (OndFareDTO ondFareDTO : collFares) {
								OndFareDTO ondFare = ondFareDTO;
								Set tempSegIds = ondFare.getSegmentsMap().keySet();

								for (Iterator iterator = tempSegIds.iterator(); iterator.hasNext();) {
									FlightSegmentDTO element = ondFare.getSegmentsMap().get(iterator.next());
									if (element.getArrivalAirportCode().equals(ssdr.getSegment().getArrivalStation())
											&& element.getDepartureAirportCode().equals(ssdr.getSegment().getDepartureStation())
											&& CalendarUtil.isEqualStartTimeOfDate(element.getDepatureDate(), ssdr.getSegment()
													.getDepartureDate())
											&& element.getFlightNumber().substring(2).equals(ssdr.getSegment().getFlightNumber())) {
										for (ReservationSegment segment : rAssembler.getDummyReservation().getSegments()) {
											if (segment.getFlightSegId().intValue() == element.getSegmentId().intValue()) {
												segSequence = segment.getSegmentSeq();
											}
										}
									}
								}
							}
						}

					}
				}
				ssrTo.setFirstSSRCode(ssrCode);
				if (ssrCode != null) {
					ssrTo.setOtherSSRInfo(otherSSRInfo.toString());
					if (segSequence != null) {
						ssrTo.setSegSequence(segSequence);
					}
				}
				paxSegmentSSRs.add(ssrTo);
			}
		}
		return paxSegmentSSRs;
	}

	public static String getAsUserNotes(Collection<OtherServiceInfo> osiCollection, String posInfo) {
		StringBuilder osiStr = new StringBuilder();

		if (osiCollection != null && !osiCollection.isEmpty()) {
			for (OtherServiceInfo osi : osiCollection) {
				osiStr.append("[" + GDSApiUtils.maskNull(osi.getCode()) + ":" + GDSApiUtils.maskNull(osi.getText()) + "]");
			}
		}

		return osiStr.toString() + posInfo;
	}

	private static Collection<SpecialServiceRequest> getSpecialServiceRequest(ReservationPax pax) {
		SpecialServiceRequest ssr = new SpecialServiceRequest();
		// FIXME multi sr support
		// ssr.setCode(pax.getSsrCode());
		// ssr.setValue(pax.getSsrRemarks());

		Collection<SpecialServiceRequest> ssrCollection = new ArrayList<SpecialServiceRequest>();
		ssrCollection.add(ssr);
		return ssrCollection;
	}

	public static Map<String, BigDecimal> getTotalPaxPayments(Collection<OndFareDTO> collFares) {
		Map<String, BigDecimal> fareMap = new HashMap<String, BigDecimal>();
		BigDecimal totalAdultAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalChildAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalInfantAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (OndFareDTO element : collFares) {
			BigDecimal[] taxArr = AccelAeroCalculator.parseBigDecimal(element.getTotalCharges(PricingConstants.ChargeGroups.TAX));
			BigDecimal[] surChgArr = AccelAeroCalculator.parseBigDecimal(element
					.getTotalCharges(PricingConstants.ChargeGroups.SURCHARGE));

			totalAdultAmount = totalAdultAmount.add(AccelAeroCalculator.parseBigDecimal(element.getAdultFare()));
			totalAdultAmount = totalAdultAmount.add(taxArr[0]);
			totalAdultAmount = totalAdultAmount.add(surChgArr[0]);

			totalChildAmount = totalChildAmount.add(AccelAeroCalculator.parseBigDecimal(element.getChildFare()));
			totalChildAmount = totalChildAmount.add(taxArr[2]);
			totalChildAmount = totalChildAmount.add(surChgArr[2]);

			totalInfantAmount = totalInfantAmount.add(AccelAeroCalculator.parseBigDecimal(element.getInfantFare()));
			totalInfantAmount = totalInfantAmount.add(taxArr[1]);
			totalInfantAmount = totalInfantAmount.add(surChgArr[1]);
		}

		fareMap.put(PaxTypeTO.ADULT, totalAdultAmount);
		fareMap.put(PaxTypeTO.CHILD, totalChildAmount);
		fareMap.put(PaxTypeTO.INFANT, totalInfantAmount);

		return fareMap;
	}

	public static ReservationContactInfo createReservationContactInfo(ContactDetail contact, ContactDetail firstPaxContactInfo) {
		ReservationContactInfo reservationContactInfo = new ReservationContactInfo();

		String title = GDSApiUtils.maskNull(contact.getTitle());
		if (title.length() > 0) {
			reservationContactInfo.setTitle(title);
		} else {
			reservationContactInfo.setTitle(firstPaxContactInfo.getTitle());
		}

		String firstName = GDSApiUtils.maskNull(contact.getFirstName());
		if (firstName.length() > 0) {
			reservationContactInfo.setFirstName(firstName);
		} else {
			reservationContactInfo.setFirstName(firstPaxContactInfo.getFirstName());
		}

		String lastName = GDSApiUtils.maskNull(contact.getLastName());
		if (lastName.length() > 0) {
			reservationContactInfo.setLastName(lastName);
		} else {
			reservationContactInfo.setLastName(firstPaxContactInfo.getLastName());
		}

		reservationContactInfo.setStreetAddress1(contact.getStreetAddress());
		reservationContactInfo.setCity(contact.getCity());
		reservationContactInfo.setCountryCode(contact.getCountry());
		reservationContactInfo.setMobileNo(contact.getMobilePhoneNumber());
		reservationContactInfo.setPhoneNo(contact.getLandPhoneNumber());
		reservationContactInfo.setFax(contact.getFaxNumber());
		reservationContactInfo.setEmail(contact.getEmailAddress());
		reservationContactInfo.setPreferredLanguage(Locale.ENGLISH.toString());

		return reservationContactInfo;
	}

	public static void setSegmentStatusCodes(Collection<SegmentDetail> segmentDetails, StatusCode statusCode) {
		for (SegmentDetail seg : segmentDetails) {
			seg.getSegment().setStatusCode(statusCode);
		}
	}

	private static String composeMessage(String label, String value) {
		value = GDSApiUtils.maskNull(value);

		if (value.length() > 0) {
			return "[" + label + ":" + value + "]";
		} else {
			return "";
		}
	}

	public static Collection getSegmentStatusCodes(CreateReservationRequest createReservationRequest) {
		Collection<StatusCode> segStatusList = new HashSet<StatusCode>();
		for (SegmentDetail seg : createReservationRequest.getNewSegmentDetails()) {
			segStatusList.add(seg.getSegment().getStatusCode());
		}
		return segStatusList;
	}

	/**
	 * create a agent information string
	 * 
	 * @param requestBase
	 * @return
	 */
	public static String getPOSInformation(GDSReservationRequestBase requestBase) {

		RecordLocator oriRecLocator = requestBase.getOriginatorRecordLocator();
		StringBuilder agentinfo = new StringBuilder();

		agentinfo.append(composeMessage("GDS", requestBase.getGds().toString()));
		agentinfo.append(composeMessage("AgentDutyCode", oriRecLocator.getAgentDutyCode()));
		agentinfo.append(composeMessage("BookingOffice", oriRecLocator.getBookingOffice()));
		agentinfo.append(composeMessage("CarrierCRSCode", oriRecLocator.getCarrierCRSCode()));
		agentinfo.append(composeMessage("ClosestCityAirportCode", oriRecLocator.getClosestCityAirportCode()));
		agentinfo.append(composeMessage("CountryCode", oriRecLocator.getCountryCode()));
		agentinfo.append(composeMessage("CurrencyCode", oriRecLocator.getCurrencyCode()));
		agentinfo.append(composeMessage("ERSPId", oriRecLocator.getErspId()));
		agentinfo.append(composeMessage("FirstDepartureFrom", oriRecLocator.getFirstDepartureFrom()));
		agentinfo.append(composeMessage("PnrReference", oriRecLocator.getPnrReference()));
		agentinfo.append(composeMessage("TAOfficeCode", oriRecLocator.getTaOfficeCode()));
		agentinfo.append(composeMessage("UserID", oriRecLocator.getUserID()));
		agentinfo.append(composeMessage("UserType", oriRecLocator.getUserType()));

		return agentinfo.toString();
	}

	public static Date getGDSOnholdReleaseTime(CreateReservationRequest createReservationRequest, Collection<OndFareDTO> collFares) throws ModuleException {

		Collection<FlightSegmentDTO> colflightSegmentDTOs = ReleaseTimeUtil.getFlightDepartureInfo(collFares);

		FlightSegmentDTO earliestSeg = ReleaseTimeUtil.getFirstFlightDepartureFlightSegment(collFares);

		Date ticketingTimeLimit = createReservationRequest.getPaymentTimeLimit();

		Date earliestDepartureDate = earliestSeg.getDepartureDateTimeZulu();
		Date releseTime = null;
		Date dtReleaseTime = null;

		boolean replyRequired = ifStatusReplyRequired(createReservationRequest);
		
		dtReleaseTime = ReleaseTimeUtil.getReleaseTimeStamp(colflightSegmentDTOs, createReservationRequest.getGdsCredentialsDTO()
				.getPrivileges(), false, null, AppIndicatorEnum.APP_XBE.toString());
		
		boolean isApplyGlobalOHDReleaseTime = false;
		for (GDSStatusTO gds : GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap().values()) {
			if (gds.getGdsCode().equals(createReservationRequest.getGds().getCode()) && gds.isApplyGlobalOHDTimeLimit()) {
				isApplyGlobalOHDReleaseTime = true;
			}
		}

		if (isApplyGlobalOHDReleaseTime) {
			releseTime = getReleaseTime(earliestDepartureDate, replyRequired);
		} else if (dtReleaseTime != null) {
			releseTime = dtReleaseTime;
		}

		if (releseTime != null && dtReleaseTime != null) {
			if (ticketingTimeLimit != null) {
				Date ticketingTimeLimitZulu = getZuluDateTime(ticketingTimeLimit, earliestSeg.getDepartureAirportCode());

				if (ticketingTimeLimitZulu.before(dtReleaseTime)) {
					return ticketingTimeLimitZulu;
				} else {
					log.error("tktl - " + ticketingTimeLimitZulu + " | release time - " + dtReleaseTime);
					throw new ModuleException("gdsservices.actions.reservation.cannot.onhold");
				}
			} else {
				if (releseTime.before(dtReleaseTime)) {
					return releseTime;
				} else {
					return dtReleaseTime;
				}
			}
		} else {
			return null;
		}
	}


	public static Date getZuluDateTime(Date localDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(GDSServicesModuleUtil.getAirportBD());

		return helper.getZuluDateTime(airportCode, localDate);
	}

	private static boolean ifStatusReplyRequired(CreateReservationRequest createReservationRequest) {

		for (SegmentDetail segDetail : createReservationRequest.getNewSegmentDetails()) {
			if (segDetail.getSegment().getActionCode() != null
					&& segDetail.getSegment().getActionCode().equals(GDSInternalCodes.ActionCode.ONHOLD_REPLY_REQUIRED)) {
				return true;
			}

			if (segDetail.getSegment().getStatusCode() != null
					&& segDetail.getSegment().getStatusCode().equals(GDSInternalCodes.StatusCode.CONFIRMED_SCHEDULE_CHANGED)) {
				return true;
			}

		}

		return false;
	}

	public static int getGDSExtendOnholdDiffInMinutes(Reservation reservation) throws ModuleException, ParseException {

		Date releaseDate = null;
		Date newReleaseDate = null;
		Date maxAllowedReleaseTimeZulu = null;
		Date earliestDepartureDate = null;
		int intDiff = 0;

		Collection<ReservationPax> colReservationPax = reservation.getPassengers();
		for (ReservationPax reservationPax : colReservationPax) {
			if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
				releaseDate = reservationPax.getZuluReleaseTimeStamp();
				break;
			}
		}

		Collection<FlightSegmentDTO> colFlightSegmentDTO = ReleaseTimeUtil.getConfirmedDepartureSegments(reservation
				.getSegmentsView());

	//	earliestDepartureDate = ReleaseTimeUtil.getFirstFlightDepartureTimeZulu(reservation.getSegmentsView());
	//  FIX-ME  no idea behind the usage of segment view  adding the already existing date here	
		earliestDepartureDate = reservation.getReleaseTimeStamps()[0];
		newReleaseDate = getExtendedReleaseTime(earliestDepartureDate);

		maxAllowedReleaseTimeZulu = ReleaseTimeUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTO);

		if (newReleaseDate.after(maxAllowedReleaseTimeZulu)) {
			newReleaseDate = maxAllowedReleaseTimeZulu;
		}

		intDiff = (int) Math.round(BeanUtils.getIdealReleaseDate(newReleaseDate, releaseDate) / (60 * 1000));

		return intDiff;

	}

	private static Date getExtendedReleaseTime(Date departureDateTimeZulu) {
		long gdsConfirmTicktingDurationInMillis = AppSysParamsUtil.getGdsTicktingOnholdDurationInMillis();

		Calendar calDepTime = Calendar.getInstance();
		calDepTime.add(Calendar.MILLISECOND, (int) gdsConfirmTicktingDurationInMillis);

		if (departureDateTimeZulu.before(calDepTime.getTime())) {
			return departureDateTimeZulu;
		} else {
			return calDepTime.getTime();
		}
	}

	private static Date getReleaseTime(Date departureDateTimeZulu, boolean statusReplyRequired) {

		long gdsConfirmDurationInMillis = 0;

		if (statusReplyRequired) {
			gdsConfirmDurationInMillis = AppSysParamsUtil.getGdsConfirmationOnholdDurationInMillis();
		} else {
			gdsConfirmDurationInMillis = AppSysParamsUtil.getGdsTicktingOnholdDurationInMillis();
		}

		Calendar calDepTime = Calendar.getInstance();
		calDepTime.add(Calendar.MILLISECOND, (int) gdsConfirmDurationInMillis);

		if (departureDateTimeZulu.before(calDepTime.getTime())) {
			return departureDateTimeZulu;
		} else {
			return calDepTime.getTime();
		}
	}

	public static Map<String, Passenger> getPassengerMap(Collection<Passenger> passengerDetails) {
		Map<String, Passenger> paxMap = new HashMap<String, Passenger>();

		for (Passenger pax : passengerDetails) {
			paxMap.put(pax.getIATAName(), pax);
		}

		return paxMap;
	}

	/**
	 * gets formated message
	 * 
	 * @param messageCode
	 * @param args
	 * @return
	 */
	public static String getFormattedMessage(ResponseMessageCode messageCode, Object... args) {
		String strMsg = MessageUtil.getMessage(messageCode.getCode());

		if (args != null && args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				strMsg = strMsg.replace(String.valueOf("#" + (i + 1)), formatToString(args[i]));
			}
		}

		return strMsg;
	}

	private static String formatToString(Object object) {
		if (object instanceof Date) {
			return datefmt.format((Date) object);
		} else {
			return object.toString();
		}

	}

	private static Collection<Integer> getPNRPaxIds(Set passengers) {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();

		for (Iterator itReservationPax = passengers.iterator(); itReservationPax.hasNext();) {
			ReservationPax reservationPax = (ReservationPax) itReservationPax.next();

			// Only loading Adults,Parents,Children. Infant's won't have any payment records
			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				pnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}

		return pnrPaxIds;
	}
	
	/**
	 * extracts segments
	 * 
	 * @param segmentDetails
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<Segment> getSegments(Collection<SegmentDetail> segmentDetails) throws ModuleException {
		Collection<Segment> segments = new ArrayList<Segment>();

		for (SegmentDetail segDetail : segmentDetails) {
			if (segDetail.getSegmentType() == SegmentType.SINGLE_SEGMENT) {
				segments.add(segDetail.getSegment());
			} else {
				throw new ModuleException("gds.not.supported.next.phase");
			}
		}

		return segments;
	}
	
	/**
	 * checks for duplicate segments in the reservation
	 * 
	 * @param segments
	 * @param reservation
	 * @throws ModuleException
	 */
	public static void checkForDuplicateSegments(Collection<Segment> segments, Reservation reservation) throws ModuleException {
		Set<String> segKeySet = new HashSet<String>();

		Collection<ReservationSegmentDTO> resSegments = reservation.getSegmentsView();
		for (ReservationSegmentDTO resSegment : resSegments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSegment.getStatus())) {
				segKeySet.add(CommonUtil.getSegMapKey(resSegment.getFlightNo(),resSegment.getSegmentCode(),
						resSegment.getDepartureDate(), resSegment.getFareTO().getBookingClassCode()));
			}
		}

		for (Segment segment : segments) {
			String key = CommonUtil.getSegMapKey(segment.getFlightNumber(),
					GDSApiUtils.getSegmentCode(segment.getDepartureStation(), segment.getArrivalStation()),
					segment.getDepartureDate(),segment.getBookingCode());

			if (segKeySet.contains(key)) {
				throw new ModuleException("gdsservices.actions.duplicate.segment");
			}
		}
	}
	
	public static List<Segment> getDistinctSegments(Collection<Segment> firstSegments, Collection<Segment> secondSegments,
			Integer gdsId) throws ModuleException {
		Set<String> segKeySet = new HashSet<String>();
		List<Segment> segList = new ArrayList<Segment>();

		for (Segment segment : firstSegments) {
			String bc = TTYMessageUtil.getActualBCForGdsMappedBC(gdsId, segment.getBookingCode());
			String key = CommonUtil.getSegMapKey(segment.getFlightNumber(),
					GDSApiUtils.getSegmentCode(segment.getDepartureStation(), segment.getArrivalStation()),
					segment.getDepartureDate(), bc);
			segKeySet.add(key);
		}

		for (Segment segment : secondSegments) {
			String bc = TTYMessageUtil.getActualBCForGdsMappedBC(gdsId, segment.getBookingCode());
			String key = CommonUtil.getSegMapKey(segment.getFlightNumber(),
					GDSApiUtils.getSegmentCode(segment.getDepartureStation(), segment.getArrivalStation()),
					segment.getDepartureDate(), bc);

			if (!segKeySet.contains(key)) {
				segList.add(segment);
			}
		}
		return segList;
	}
	
	/**
	 * @return
	 */
	public static TrackInfoDTO getTrackingInfo(GDSCredentialsDTO credentialsDTO) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_GDS);
		trackInfoDTO.setOriginUserId(credentialsDTO.getUserId());
		trackInfoDTO.setOriginAgentCode(credentialsDTO.getAgentCode());
		trackInfoDTO.setOriginChannelId(credentialsDTO.getSalesChannelCode());
		return trackInfoDTO;
	}
	
	/**
	 * Get Travel information summary details (Pax type and count details)
	 * 
	 * @param infoSummaryType
	 * @return
	 */
	public static TravelerInfoSummaryTO getTravelInfomationSummary(Reservation reservation, FlightPriceRQ flightPriceRQ)
			throws ModuleException {
		int adults = reservation.getTotalPaxAdultCount();
		int children = reservation.getTotalPaxChildCount();
		int infants = reservation.getTotalPaxInfantCount();

		if (adults == 0 && children == 0) {
			throw new ModuleException("Adult pax count cannot be zero");
		}

		if (infants > adults) {
			throw new ModuleException("Infants cannot be more than adults");
		}

		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(adults);

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(children);

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(infants);

		return traverlerInfo;
	}
	
	public static OriginDestinationOptionTO setFlightSegmentTo(List<ReservationSegmentDTO> segList) {
		OriginDestinationOptionTO originDestinationOptionTO = null;
		if (segList != null) {
			originDestinationOptionTO = new OriginDestinationOptionTO();
			FlightSegmentTO flightSegTO = null;
			for (ReservationSegmentDTO resResDto : segList) {
				flightSegTO = new FlightSegmentTO();
				flightSegTO.setFlightSegId(resResDto.getFlightSegId());
				flightSegTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(resResDto));
				originDestinationOptionTO.getFlightSegmentList().add(flightSegTO);
			}
		}
		return originDestinationOptionTO;
	}

	public static OriginDestinationOptionTO setFlightSegmentTo(Segment segment) throws ModuleException {
		OriginDestinationOptionTO originDestinationOptionTO = null;
		WildcardFlightSearchDto flightSearchDto;
		WildcardFlightSearchRespDto flightSearchRespDto;
		if (segment != null) {
			originDestinationOptionTO = new OriginDestinationOptionTO();

			flightSearchDto = new WildcardFlightSearchDto();
			flightSearchDto.setDepartureAirport(segment.getDepartureStation());
			flightSearchDto.setArrivalAirport(segment.getArrivalStation());
			flightSearchDto.setDepartureDate(segment.getDepartureDateTime());
			flightSearchDto.setDepartureDateExact(false);
			flightSearchDto.setFlightNumber(segment.getFlightNumber());
			flightSearchRespDto = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(flightSearchDto);

			if (flightSearchRespDto.getBestMatch() != null) {
				originDestinationOptionTO.getFlightSegmentList().add(flightSearchRespDto.getBestMatch());
			}

		}
		return originDestinationOptionTO;
	}

	public static OriginDestinationOptionTO getOriginDestOptionTo(FlightInformation flightInformation) throws ModuleException {
		OriginDestinationOptionTO originDestinationOptionTO = null;
		FlightSegmentTO flightSegmentTO = getBestMatchedFlightSegmentTo(flightInformation);
		if (flightSegmentTO != null) {
			originDestinationOptionTO = new OriginDestinationOptionTO();

		}
		return originDestinationOptionTO;
	}

	public static FlightSegmentTO getBestMatchedFlightSegmentTo(FlightInformation flightInformation) throws ModuleException {
		WildcardFlightSearchRespDto resp;


		WildcardFlightSearchDto wildcardFlightSearch = new WildcardFlightSearchDto();

		try {
			wildcardFlightSearch.setDepartureDate(flightInformation.getDepartureDateTime().toGregorianCalendar().getTime());

			if (flightInformation.getArrivalDateTime() != null) {
				wildcardFlightSearch.setArrivalDate(flightInformation.getArrivalDateTime().toGregorianCalendar().getTime());
			}

			wildcardFlightSearch.setFlightNumber(flightInformation.getCompanyInfo().getMarketingAirlineCode() +
					flightInformation.getFlightNumber());
			wildcardFlightSearch.setDepartureAirport(flightInformation.getDepartureLocation().getLocationCode());
			wildcardFlightSearch.setArrivalAirport(flightInformation.getArrivalLocation().getLocationCode());

			resp = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(wildcardFlightSearch);
		} catch (Exception e) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.MISSING, "");
		}

		return resp.getBestMatch();

	}

	public static List<ReservationSegmentDTO> getActualResSegments(String gdsCarrierCode, List<BookingSegmentDTO> bookingSegmentDTOs)
		throws GdsTypeAException {
		List<ReservationSegmentDTO> reservationSegmentDTOs = new ArrayList<ReservationSegmentDTO>();
		ReservationSegmentDTO resSeg;
		WildcardFlightSearchRespDto resp;
		WildcardFlightSearchDto wildcardFlightSearch;
		FlightSegmentTO flightSegmentTO;

		Calendar date = Calendar.getInstance();
		Calendar time = Calendar.getInstance();

		String bookingClass;


		for (BookingSegmentDTO bookingSegment : bookingSegmentDTOs) {
			wildcardFlightSearch = new WildcardFlightSearchDto();
			date.setTime(bookingSegment.getDepartureDate());
			time.setTime(bookingSegment.getDepartureTime());
			date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
			date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
			date.set(Calendar.SECOND, 0);
			date.set(Calendar.MILLISECOND, 0);
			wildcardFlightSearch.setDepartureDate(date.getTime());


			date.setTime(bookingSegment.getDepartureDate());
			time.setTime(bookingSegment.getArrivalTime());
			date.add(Calendar.DATE, bookingSegment.getDayOffSet());
			date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
			date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
			date.set(Calendar.SECOND, 0);
			date.set(Calendar.MILLISECOND, 0);
			wildcardFlightSearch.setArrivalDate(date.getTime());

			wildcardFlightSearch.setFlightNumber(bookingSegment.getCarrierCode() + bookingSegment.getFlightNumber());
			wildcardFlightSearch.setDepartureAirport(bookingSegment.getDepartureStation());
			wildcardFlightSearch.setArrivalAirport(bookingSegment.getDestinationStation());

			try {
				resp = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(wildcardFlightSearch);
			} catch (ModuleException e) {
				throw new GdsTypeAException();
			}

			if (resp.getBestMatch() != null) {
				flightSegmentTO = resp.getBestMatch();

				resSeg = new ReservationSegmentDTO();
				resSeg.setFlightSegId(flightSegmentTO.getFlightSegId());

				bookingClass = GDSServicesUtil.getBookingCode(gdsCarrierCode, bookingSegment.getBookingCode());
				resSeg.setBookingClass(bookingClass);

				reservationSegmentDTOs.add(resSeg);
			} else {
				throw new GdsTypeAException();
			}

		}

		return reservationSegmentDTOs;
	}

	/**
	 * 
	 * @param availPreferencesTO
	 */
	public static void setAvailPreference(AvailPreferencesTO availPreferencesTO, GDSCredentialsDTO principal) {
		// later can set to GDS agent currency
		availPreferencesTO.setPreferredCurrency(AppSysParamsUtil.getBaseCurrency());
		availPreferencesTO.setTravelAgentCode(principal.getAgentCode());
		availPreferencesTO.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
		// availPreferencesTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
		availPreferencesTO.setQuoteOndFlexi(OndSequence.IN_BOUND, false);
		availPreferencesTO.setQuoteOndFlexi(OndSequence.OUT_BOUND, false);
		availPreferencesTO.setQuoteFares(true);
		availPreferencesTO.setSearchSystem(SYSTEM.AA);
		availPreferencesTO.setAppIndicator(ApplicationEngine.WS);
	}
	
	/**
	 * 
	 * @param bookingClassSet
	 * @param travelPreferencesTO
	 * @throws ModuleException
	 */
	public static void setTravelPreference(Collection<String> bookingClassSet, TravelPreferencesTO travelPreferencesTO)
			throws ModuleException {

		String bookingClass = BeanUtils.getFirstElement(bookingClassSet);
		String bookingType = BookingClass.BookingClassType.NORMAL;
		if (bookingClass != null) {
			travelPreferencesTO.setBookingClassCode(bookingClass);
			// Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
			// if (standByBCList.contains(bookingClass)) {
			// bookingType = BookingClass.BookingClassType.STANDBY;
			// }
		}
		travelPreferencesTO.setBookingType(bookingType);
	}
	
	/**
	 * extract Segments that are not cancelling
	 * 
	 * @param reservation
	 * @param gdsSegList
	 * @return
	 */
	public static Collection<ReservationSegmentDTO> extractNonCancelingSegments(Reservation reservation, Collection<Segment> gdsSegList)
			throws ModuleException {
		Map<String, ReservationSegmentDTO> segmentMap = new HashMap<String, ReservationSegmentDTO>();
		Collection colReservationSegmentDTO = reservation.getSegmentsView();
		Segment cancellingSegment;
		String key;


		Collection<ReservationSegmentDTO> nonCancellingSegment = new ArrayList<>(colReservationSegmentDTO);
		Collection<ReservationSegmentDTO> identifiedSegments = new ArrayList<ReservationSegmentDTO>();
		for (Iterator<ReservationSegmentDTO> iter = colReservationSegmentDTO.iterator(); iter.hasNext();) {

			ReservationSegmentDTO reservationSegment = (ReservationSegmentDTO) iter.next();
			key = CommonUtil.getSegMapKeyWithoutYear(reservationSegment.getFlightNo(), reservationSegment.getSegmentCode(),
					reservationSegment.getDepartureDate(),reservationSegment.getFareTO().getBookingClassCode());

			segmentMap.put(key, reservationSegment);

		}


		for (Iterator iter = gdsSegList.iterator(); iter.hasNext();) {
			ReservationSegmentDTO temporarySegment = null;

			cancellingSegment = (Segment) iter.next();
			String bookingCode = TTYMessageUtil.getActualBCForGdsMappedBC(reservation.getGdsId(), cancellingSegment.getBookingCode());

			List<String> flightNumbers = SegmentUtil.getPrioratizedFlightNumbers(cancellingSegment.getFlightNumber());
			for (String flightNumber : flightNumbers) {
				key = CommonUtil.getSegMapKeyWithoutYear(flightNumber, cancellingSegment.getSegmentCode(),
						cancellingSegment.getDepartureDate(), bookingCode);

				if (segmentMap.containsKey(key)) {
					temporarySegment = segmentMap.get(key);
					break;
				}
			}

			if (temporarySegment != null) {
				identifiedSegments.add(temporarySegment);
			}
		}

		nonCancellingSegment.removeAll(identifiedSegments);

		return nonCancellingSegment;
	}
	
	/**
	 * extract PNR ids
	 * 
	 * @param reservation
	 * @param gdsSegList
	 * @return
	 */
	public static List<Integer> extractPnrSegIDs(Reservation reservation, Collection<Segment> gdsSegList)
			throws ModuleException {
		Map<String, ReservationSegmentDTO> segmentMap = new HashMap<String, ReservationSegmentDTO>();
		Collection colReservationSegmentDTO = reservation.getSegmentsView();
		ReservationSegmentDTO reservationSegmentDTO;
		String key;

		for (Iterator iter = colReservationSegmentDTO.iterator(); iter.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) iter.next();
			String bookingClass = reservationSegmentDTO.getFareTO().getBookingClassCode();
			if (reservationSegmentDTO.getCsOcCarrierCode() != null
					&& !AppSysParamsUtil.getDefaultCarrierCode().equals(reservationSegmentDTO.getCsOcCarrierCode())) {
				//If this check happens in MC, use mc mapped bc for comparision
				bookingClass = TTYMessageUtil.getGdsMappedBCForActualBC(reservationSegmentDTO.getCsOcCarrierCode(), bookingClass);
			}
			key = CommonUtil.getSegMapKey(reservationSegmentDTO.getFlightNo(), reservationSegmentDTO.getSegmentCode(),
					reservationSegmentDTO.getDepartureDate(), bookingClass);
			segmentMap.put(key, reservationSegmentDTO);
		}

		List<ReservationSegmentDTO> identifiedSegments = new ArrayList<ReservationSegmentDTO>();
		for (Iterator<Segment> iter = gdsSegList.iterator(); iter.hasNext();) {
			Segment segment = (Segment) iter.next();
			String bookingCode = null;
			if (reservation.getGdsId() == null && segment.getCodeShareBc() != null && segment.getCodeShareFlightNo() != null) {
				//If this check happens in MC, use mc mapped bc for comparision
				bookingCode = segment.getCodeShareBc();
			} else {
				bookingCode = TTYMessageUtil.getActualBCForGdsMappedBC(reservation.getGdsId(), segment.getBookingCode());
			}

			List<String> flightNumbers = SegmentUtil.getPrioratizedFlightNumbers(segment.getFlightNumber());
			reservationSegmentDTO = null;
			for (String flightNumber : flightNumbers) {
				key = CommonUtil.getSegMapKey(flightNumber, segment.getSegmentCode(), segment.getDepartureDate(), bookingCode);

				if (segmentMap.containsKey(key)) {
					reservationSegmentDTO = segmentMap.get(key);
					break;
				}
			}

			if (reservationSegmentDTO != null) {
				identifiedSegments.add(reservationSegmentDTO);
			}
		}

		List<Integer> pnrSegIds = new ArrayList<Integer>();
		Collections.sort(identifiedSegments);
		if (identifiedSegments != null && identifiedSegments.size() > 0) {
			for (ReservationSegmentDTO resSegmentDTO : identifiedSegments) {
				pnrSegIds.add(resSegmentDTO.getPnrSegId());
			}
		}

		return pnrSegIds;
	}

	public static Collection<Integer> extractPaxIDs(Reservation reservation, Collection<Passenger> passengers)
			throws ModuleException {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		Map<String, Integer> paxMap = new HashMap<String, Integer>();

		Collection colReservationPax = reservation.getPassengers();
		for (Iterator iter = colReservationPax.iterator(); iter.hasNext();) {
			ReservationPax reservationPax = (ReservationPax) iter.next();
			if (ReservationApiUtils.isAdultType(reservationPax) || ReservationApiUtils.isChildType(reservationPax)) {
				String key = GDSApiUtils.getIATAName(reservationPax.getLastName(), reservationPax.getFirstName(),
						reservationPax.getTitle());
				paxMap.put(key, reservationPax.getPnrPaxId());
			}
		}

		for (Iterator iter = passengers.iterator(); iter.hasNext();) {
			Passenger pax = (Passenger) iter.next();

			String key = GDSApiUtils.getIATAName(pax.getLastName(), pax.getFirstName(), pax.getTitle());
			Integer pnrPaxId = paxMap.get(key);

			if (pnrPaxId != null) {
				pnrPaxIds.add(pnrPaxId);
			}
		}

		return pnrPaxIds;
	}
	
	public static Collection<Integer> extractInfantPaxIDs(Reservation reservation, Collection<Passenger> passengers)
			throws ModuleException {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		Map<String, Integer> paxMap = new HashMap<String, Integer>();

		Collection colReservationPax = reservation.getPassengers();
		for (Iterator iter = colReservationPax.iterator(); iter.hasNext();) {
			ReservationPax reservationPax = (ReservationPax) iter.next();
			if (ReservationApiUtils.isInfantType(reservationPax)) {
				String key = GDSApiUtils.getIATAName(reservationPax.getLastName(), reservationPax.getFirstName(),
						reservationPax.getTitle());
				paxMap.put(key, reservationPax.getPnrPaxId());
			}
		}

		for (Iterator iter = passengers.iterator(); iter.hasNext();) {
			Passenger pax = (Passenger) iter.next();

			String key = GDSApiUtils.getIATAName(pax.getLastName(), pax.getFirstName(), pax.getTitle());
			Integer pnrPaxId = paxMap.get(key);

			if (pnrPaxId != null) {
				pnrPaxIds.add(pnrPaxId);
			}
		}

		return pnrPaxIds;
	}
	
	public static Collection<TempSegBcAlloc> getSegBcAlloc(Collection<TempSegBcAllocDTO> blockedSeats) {
		ArrayList<TempSegBcAlloc> allBlockedRecsList = null;
		if (blockedSeats != null && blockedSeats.size() > 0) {
			for (TempSegBcAllocDTO tempSegBcAllocDTO : blockedSeats) {
				if (allBlockedRecsList == null) {
					allBlockedRecsList = new ArrayList<TempSegBcAlloc>();
				}
				TempSegBcAlloc tempSegBcAlloc = new TempSegBcAlloc();
				tempSegBcAlloc.setBookingClassType(tempSegBcAllocDTO.getBookingClassType());
				tempSegBcAlloc.setDirection(tempSegBcAllocDTO.getDirection());
				tempSegBcAlloc.setFccaId(tempSegBcAllocDTO.getFccaId());
				tempSegBcAlloc.setFccsbaId(tempSegBcAllocDTO.getFccsbaId());
				tempSegBcAlloc.setId(tempSegBcAllocDTO.getId());
				tempSegBcAlloc.setNestRecordId(tempSegBcAllocDTO.getNestRecordId());
				tempSegBcAlloc.setPrevStatusChgAction(tempSegBcAllocDTO.getPrevStatusChgAction());
				tempSegBcAlloc.setSeatsBlocked(tempSegBcAllocDTO.getSeatsBlocked());
				tempSegBcAlloc.setSkipSegInvUpdate(tempSegBcAllocDTO.getSkipSegInvUpdate());
				tempSegBcAlloc.setStatus(tempSegBcAllocDTO.getStatus());
				tempSegBcAlloc.setTimestamp(tempSegBcAllocDTO.getTimestamp());
				tempSegBcAlloc.setUserId(tempSegBcAllocDTO.getUserId());
				tempSegBcAlloc.setVersion(tempSegBcAllocDTO.getVersion());
				allBlockedRecsList.add(tempSegBcAlloc);
			}
		}
		return allBlockedRecsList;
	}
	
	/**
	 * Returns the first flight segment's departure station
	 */
	public static String getFirstFlightDepartureAirport(Collection<OndFareDTO> ondFareDTOs) {
		Date firstDepZulu = null;
		String firstDepStation = null;
		Collection<FlightSegmentDTO> flightSegmentDTOs = null;
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			flightSegmentDTOs = ondFareDTO.getSegmentsMap().values();
			for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
				if (firstDepZulu == null || firstDepZulu.after(flightSegmentDTO.getDepartureDateTimeZulu())) {
					firstDepZulu = flightSegmentDTO.getDepartureDateTimeZulu();
					firstDepStation = flightSegmentDTO.getDepartureAirportCode();
				}
			}
		}
		return firstDepStation;
	}
	
	public static boolean checkAlreadyFlownSegmentsExists(Reservation reservation, Collection<Segment> cancellingSegments)
			throws ModuleException {
	
		Map<Integer, Collection<Integer>> flownSegmentIDMap = GDSServicesModuleUtil.getReservationBD().getPaxwiseFlownSegments(
				reservation);
		if (flownSegmentIDMap.isEmpty()) {
			return false;
		} else {
			Set<Integer> mapKeySet = flownSegmentIDMap.keySet();
			Set<Integer> flownSegmentIDs = new HashSet<Integer>();
			for (Integer paxID : mapKeySet) {
				Collection<Integer> segmentIDs = flownSegmentIDMap.get(paxID);
				flownSegmentIDs.addAll(segmentIDs);
			}
			Map<String, ReservationSegmentDTO> flownReservationSegmentMap = getFlownReservationSegmentsMap(reservation,
					flownSegmentIDs);
			for (Segment cancellingSegment : cancellingSegments) {
				String mapKey = CommonUtil.getSegMapKey(cancellingSegment.getFlightNumber(), cancellingSegment.getSegmentCode(),
						cancellingSegment.getDepartureDate(), cancellingSegment.getBookingCode());
				if (flownReservationSegmentMap.get(mapKey) != null) {
					return true;
				}
			}
		}

		return false;
	}
	
	private static Map<String, ReservationSegmentDTO> getFlownReservationSegmentsMap(Reservation reservation,
			Collection<Integer> segmentID) throws ModuleException {
		Collection<ReservationSegmentDTO> reservationSegments = reservation.getSegmentsView();
		Map<String, ReservationSegmentDTO> flownReservationSegments = new HashMap<String, ReservationSegmentDTO>();

		for (Integer flownSegID : segmentID) {
			for (ReservationSegmentDTO resSegment : reservationSegments) {
				if (flownSegID.equals(resSegment.getPnrSegId())) {
					String key = CommonUtil.getSegMapKey(resSegment.getFlightNo(), resSegment.getSegmentCode(),
							resSegment.getDepartureDate(), resSegment.getFareTO().getBookingClassCode());
					flownReservationSegments.put(key, resSegment);
					break;
				}
			}
		}
		return flownReservationSegments;
	}
	
	public static Integer getNationalityCode(String nationalityCodeStr) throws ModuleException {
		Integer nationalityCode = null;
		Nationality nationality = null;

		if (nationalityCodeStr != null) {
			nationality = GDSServicesModuleUtil.getCommonMasterBD().getNationality(nationalityCodeStr);
		}
		if (nationality != null) {
			nationalityCode = nationality.getNationalityCode();
		}
		return nationalityCode;
	}

	public static void checkOnholdRestrictedBookingClassAvailable(Collection<OndFareDTO> collFares) throws ModuleException {
		Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = null;
		Collection<SeatDistribution> seatDistributions = null;

		for (OndFareDTO ondFareDTO : collFares) {
			segmentSeatDistsDTOs = ondFareDTO.getSegmentSeatDistsDTO();
			for (SegmentSeatDistsDTO segmentSeatDistsDTO : segmentSeatDistsDTOs) {
				seatDistributions = segmentSeatDistsDTO.getSeatDistribution();
				Object[] seatDistributions1 =seatDistributions.toArray()	;
//				for (SeatDistribution seatDistribution : seatDistributions) {
				// for nesting check only highest rank class 
					if (((SeatDistribution) seatDistributions1[seatDistributions.size()-1]).isOnholdRestricted()) {
						throw new ModuleException("gdsservices.actions.OnholdRestricted.notAllowed");
					}
//				}

			}
		}
	}
}
