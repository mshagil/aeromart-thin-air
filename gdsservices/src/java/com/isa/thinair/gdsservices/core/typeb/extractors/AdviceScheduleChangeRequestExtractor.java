package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.AdviceScheduleChangeRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CancelReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CancelSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Manoj
 */
public class AdviceScheduleChangeRequestExtractor extends ValidatorBase {
	
	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		RequestExtractorUtil.setResponderRecordLocator(bookingRequestDTO);
		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}
	
	public static LinkedHashMap<ReservationAction, GDSReservationRequestBase> getMap(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {

		LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();
		addSegments(requestMap, bookingRequestDTO, gdsCredentialsDTO);
		RequestExtractorUtil.processExternalSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);
		return requestMap;
	}
	
	private static void addSegments(LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap,
			BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		BookingSegmentDTO segmentDTO;
		String gdsActionCode;
		List<Segment> sourceSegments = new ArrayList<Segment>();
		List<Segment> targetSegments = new ArrayList<Segment>();
		List<SegmentDetail> targetSegmentDetails = new ArrayList<SegmentDetail>();

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			segmentDTO = (BookingSegmentDTO) iterator.next();
			gdsActionCode = segmentDTO.getActionOrStatusCode();
			if (gdsActionCode.equals(GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode())) {
				Segment sourceSegment = RequestExtractorUtil.getSegment(segmentDTO);
				sourceSegments.add(sourceSegment);
			} else if (gdsActionCode.equals(GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode())) {
				Segment targetSegment = RequestExtractorUtil.getSegment(segmentDTO);
				targetSegments.add(targetSegment);
				SegmentDetail targetSegmentDetail = RequestExtractorUtil.getSegmentDetail(segmentDTO);
				targetSegmentDetails.add(targetSegmentDetail);
			}
		}
		if (!sourceSegments.isEmpty() && !targetSegments.isEmpty()) {
			AdviceScheduleChangeRequest adviceScheduleChangeRequest = new AdviceScheduleChangeRequest();
			adviceScheduleChangeRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
			adviceScheduleChangeRequest = (AdviceScheduleChangeRequest) RequestExtractorUtil.addBaseAttributes(
					adviceScheduleChangeRequest, bookingRequestDTO);
			adviceScheduleChangeRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());
			adviceScheduleChangeRequest.setSourceSegments(sourceSegments);
			adviceScheduleChangeRequest.setTargetSegments(targetSegments);
			requestMap.put(GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT, adviceScheduleChangeRequest);

		} else if (!sourceSegments.isEmpty() && targetSegments.isEmpty()) {
			List<BookingSegmentDTO> oalSegmentDTOs = bookingRequestDTO.getOtherAirlineSegmentDTOs();
			if (!oalSegmentDTOs.isEmpty()) {
				for (BookingSegmentDTO oalSegmentDTO : bookingRequestDTO.getOtherAirlineSegmentDTOs()) {
					gdsActionCode = oalSegmentDTO.getActionOrStatusCode();
					if (gdsActionCode.equals(GDSExternalCodes.AdviceCode.CONFIRMED_SCHEDULE_CHANGED.getCode())) {
						oalSegmentDTO.setActionOrStatusCode(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode());
						if (segmentDTOs.size() == 1) {
							CancelReservationRequest cancelReservationRequest = new CancelReservationRequest();
							cancelReservationRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
							cancelReservationRequest = (CancelReservationRequest) RequestExtractorUtil.addBaseAttributes(
									cancelReservationRequest, bookingRequestDTO);
							cancelReservationRequest.getSegments().addAll(sourceSegments);
							requestMap.put(GDSInternalCodes.ReservationAction.CANCEL_RESERVATION, cancelReservationRequest);
						} else {
							CancelSegmentRequest cancelSegmentRequest = new CancelSegmentRequest();

							cancelSegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
							cancelSegmentRequest = (CancelSegmentRequest) RequestExtractorUtil.addBaseAttributes(
									cancelSegmentRequest, bookingRequestDTO);
							cancelSegmentRequest.getSegments().addAll(sourceSegments);
							requestMap.put(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT, cancelSegmentRequest);
						}
						break;
					}
				}
			}
		} else if (sourceSegments.isEmpty() && !targetSegments.isEmpty()) {
			List<BookingSegmentDTO> oalSegmentDTOs = bookingRequestDTO.getOtherAirlineSegmentDTOs();
			if (!oalSegmentDTOs.isEmpty()) {
				for (BookingSegmentDTO oalSegmentDTO : bookingRequestDTO.getOtherAirlineSegmentDTOs()) {
					gdsActionCode = oalSegmentDTO.getActionOrStatusCode();
					if (gdsActionCode.equals(GDSExternalCodes.AdviceCode.UNABLE_NOT_OPERATED_PROVIDED.getCode())) {
						oalSegmentDTO.setActionOrStatusCode(GDSExternalCodes.ActionCode.CANCEL.getCode());
						AddSegmentRequest addSegmentRequest = new AddSegmentRequest();
						addSegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
						addSegmentRequest = (AddSegmentRequest) RequestExtractorUtil.addBaseAttributes(addSegmentRequest,
								bookingRequestDTO);
						addSegmentRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());
						Collection<SegmentDetail> segmentDetails = new ArrayList<SegmentDetail>();
						segmentDetails.addAll(targetSegmentDetails);
						addSegmentRequest.setSegmentDetails(segmentDetails);
						requestMap.put(GDSInternalCodes.ReservationAction.ADD_SEGMENT, addSegmentRequest);
						break;
					}
				}
			}
		}
	}
}
