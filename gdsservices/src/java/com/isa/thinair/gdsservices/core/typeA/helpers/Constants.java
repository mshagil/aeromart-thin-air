package com.isa.thinair.gdsservices.core.typeA.helpers;

public class Constants {
	public interface CommandNames {

		public static final String EDI_PROTOCOL_PROCESSOR = "ediProtocolProcessor";
		public static final String INVENTORY_ADJUSTMENT = "inventoryAdjustment";
		public static final String HYBRID_WRAP_UP = "hybridWrapUp";
		public static final String CLEAR_TERMINATE_TRANSACTION = "clearTerminateTransaction";
		public static final String COMMON_TICKET_HANDLER = "commonTicketHandler";
		public static final String COMMON_TICKET_CONTROL_HANDLER = "commonTicketControlHandler";
		public static final String SPECIFIC_FLIGHT_AVAILABILITY = "specificFlightAvailability";
		public static final String DAILY_FLIGHT_AVAILABILITY = "dailyFlightAvailability";
		public static final String SEAMLESS_AVAILABILITY = "seamlessAvailability";
		public static final String SPECIFIC_FLIGHT_SCHEDULE = "specificFlightSchedule";
		public static final String COMMON_TICKET_INITIATOR = "commonTicketInitiator";
		public static final String COMMON_TICKET_CONTROL_INITIATOR = "commonTicketControlInitiator";
		public static final String UNSOLICITED_TICKET_CONTROL_HANDLER = "unsolicitedTicketControlHandler";

		public static final String CREATE_PASSENGER_RECORD = "createPassengerRecord";
		public static final String MODIFY_PASSENGER_RECORD = "modifyPassengerRecord";

		public static final String TRANSFER_CONTROL = "transferControl";
		
		public static final String DISPLAY_TICKET = "displayTicket";

		public static final String COMMON_TICKET_CONTROL_NOTIFIER = "commonTicketControlNotifier";

	}

	public interface Separators {
		public static final String SEGMENT_SEPARATOR = "_";
	}

	public interface Common {
		public static final String messageTypeVersionNumber = "96";
		public static final String messageTypeReleaseNumber = "2";
		public static final String controllingAgencyCoded = "IA";
		public static final String syntaxIdentifier = "IATA";
		public static final String syntaxVersionNumber = "1";
		public static final String recipientIdentification = "DL";
		public static final String senderIdentification = "SD";
	}
}