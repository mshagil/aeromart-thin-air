package com.isa.thinair.gdsservices.core.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.ChangeContactDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class ChangeContactDetailsAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ChangeContactDetailsAction.class);

	public static ChangeContactDetailRequest processRequest(ChangeContactDetailRequest changeContactDetailRequest) {
		try {
			Reservation reservation = CommonUtil.loadReservation(changeContactDetailRequest.getGdsCredentialsDTO(),
					changeContactDetailRequest.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(changeContactDetailRequest.getGdsCredentialsDTO(), reservation);

			ReservationContactInfo serverContactInfo = reservation.getContactInfo();
			ContactDetail clientContactInfo = changeContactDetailRequest.getContactDetail();

			boolean isUpdatable = compareAndCopy(serverContactInfo, clientContactInfo);

			if (isUpdatable) {
				ServiceResponce serviceRes = GDSServicesModuleUtil.getReservationBD().updateContactInfo(reservation.getPnr(),
						reservation.getVersion(), serverContactInfo,
						CommonUtil.getTrackingInfo(changeContactDetailRequest.getGdsCredentialsDTO()));

				if (serviceRes != null && serviceRes.isSuccess()) {
					changeContactDetailRequest.setSuccess(true);
				} else {
					changeContactDetailRequest.setSuccess(false);
					changeContactDetailRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
					changeContactDetailRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
				}
			} else {
				changeContactDetailRequest.setSuccess(true);
			}
		} catch (ModuleException e) {
			log.error(" ChangeContactDetailsAction Failed for GDS PNR "
					+ changeContactDetailRequest.getOriginatorRecordLocator().getPnrReference(), e);
			changeContactDetailRequest.setSuccess(false);
			changeContactDetailRequest.setResponseMessage(e.getMessageString());
			changeContactDetailRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" ChangeContactDetailsAction Failed for GDS PNR "
					+ changeContactDetailRequest.getOriginatorRecordLocator().getPnrReference(), e);
			changeContactDetailRequest.setSuccess(false);
			changeContactDetailRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			changeContactDetailRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return changeContactDetailRequest;
	}

	private static Object[] compare(String client, String server) {
		client = GDSApiUtils.maskNull(client);
		if (client.length() > 0) {
			boolean status = GDSApiUtils.isEqual(client, server);

			if (!status) {
				return new Object[] { new Boolean(true), client };
			}
		}

		return new Object[] { new Boolean(false), null };
	}

	private static boolean compareAndCopy(ReservationContactInfo serverContactInfo, ContactDetail clientContactInfo) {
		boolean isUpdatable = false;

		Object[] arrayValues = compare(clientContactInfo.getTitle(), serverContactInfo.getTitle());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setTitle((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getFirstName(), serverContactInfo.getFirstName());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setFirstName((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getLastName(), serverContactInfo.getLastName());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setLastName((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getStreetAddress(), serverContactInfo.getStreetAddress1());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setStreetAddress1((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getLandPhoneNumber(), serverContactInfo.getPhoneNo());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setPhoneNo((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getMobilePhoneNumber(), serverContactInfo.getMobileNo());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setMobileNo((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getCity(), serverContactInfo.getCity());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setCity((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getFaxNumber(), serverContactInfo.getFax());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setFax((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getEmailAddress(), serverContactInfo.getEmail());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setEmail((String) arrayValues[1]);
			isUpdatable = true;
		}

		arrayValues = compare(clientContactInfo.getCountry(), serverContactInfo.getCountryCode());
		if ((Boolean) arrayValues[0]) {
			serverContactInfo.setCountryCode((String) arrayValues[1]);
			isUpdatable = true;
		}

		return isUpdatable;
	}
}
