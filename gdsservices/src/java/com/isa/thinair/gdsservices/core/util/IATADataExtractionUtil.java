package com.isa.thinair.gdsservices.core.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.core.typeA.model.RDD;

public class IATADataExtractionUtil {

	private static Log log = LogFactory.getLog(IATADataExtractionUtil.class);

	public static final String DATE_FORMAT = "ddMMyy";
	public static final String TIME_FORMAT = "HHmm";
	public static final String DATE_TIME_FORMAT = "ddMMyy:HHmm";

	private static final String DATE_NUMBER_FORMATTER = "%06d"; // 10112 --> 010112
	private static final String TIME_NUMBER_FORMATTER = "%04d"; // 1 --> 0001

	private enum GetterSetter {

		GET("get"), SET("set");

		private String methodSuffix;

		private GetterSetter(String methodSuffix) {
			this.methodSuffix = methodSuffix;
		}

		/**
		 * @return the methodSuffix
		 */
		public String getMethodSuffix() {
			return methodSuffix;
		}

	}

	public static <T> T getValue(Object element, RDD rdd, Integer... locatorPath) throws InteractiveEDIException {

		T returnVal;

		if (element == null) {
			return null;
		}

		Object invoke = element;
		for (int locator : locatorPath) {
			Class cls = invoke.getClass();
			Method method = getMethodFromPos(cls.getMethods(), GetterSetter.GET, locator);
			try {
				invoke = method.invoke(invoke);
			} catch (Exception e) {
				throw new InteractiveEDIException(CodeSetEnum.CS0085.UNSPECIFIED_ERROR, "invalide method arguments");
			}
		}

		// validate element Mandatory / Conditional
		rdd.validate(invoke);
		returnVal = (T) invoke;
		return returnVal;
	}

	/**
	 * set the value of subElement by reflections
	 * 
	 * @param factory
	 * @param element
	 * @param value
	 * @param locatorPath
	 * @throws ModuleException
	 */
	public static void setValue(Object factory, Object element, Object value, Integer... locatorPath)
			throws InteractiveEDIException {

		Object invoke = element;
		for (int locatorIndex = 0; locatorIndex < locatorPath.length; locatorIndex++) {
			int locator = locatorPath[locatorIndex];
			Class cls = invoke.getClass();
			Method getter = getMethodFromPos(cls.getMethods(), GetterSetter.GET, locator);
			Method setter = getMethodFromPos(cls.getMethods(), GetterSetter.SET, locator);
			try {

				if (locatorIndex < locatorPath.length - 1) {
					Object child;
					// call get method
					child = getter.invoke(invoke);
					if (child == null) { // we need to create and set that object
						child = createSubElement(factory, element, locatorIndex, String.valueOf(locator));
						setter.invoke(invoke, child);
					}
					invoke = child;
				} else {
					// call set method
					setter.invoke(invoke, value);
				}

			} catch (Exception e) {
				log.error("invalide value or locator path", e);
				throw new InteractiveEDIException(CodeSetEnum.CS0085.UNSPECIFIED_ERROR, "invalide value or locator path");
			}
		}

	}

	private static Object createSubElement(Object factory, Object element, int locatorIndex, String locator)
			throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		String subElementName = element.getClass().getSimpleName();
		for (int i = 1; i <= locatorIndex + 1; i++) {
			subElementName += subElementName;
		}

		locator = (locator.length() == 1) ? "0" + locator : locator;
		String methodName = "create" + subElementName + locator;
		Method factoryMethod = factory.getClass().getMethod(methodName);
		Object subElement = factoryMethod.invoke(factory);

		return subElement;
	}

	@Deprecated
	public static Date getDate(Object element, RDD rdd, Integer[] dateLocator, Integer[] timeLocator)
			throws InteractiveEDIException {

		Date dateTime = null;

		if (RDD.M.equals(rdd)) {
			BigDecimal date = getValue(element, RDD.M, dateLocator);
			if (timeLocator != null) {
				BigDecimal time = getValue(element, RDD.M, timeLocator);
				dateTime = getDate(date, time);
			} else {
				dateTime = getDate(date);
			}
		} else {
			BigDecimal date = getValue(element, RDD.C, dateLocator);
			BigDecimal time = getValue(element, RDD.C, timeLocator);

			if (date != null) {
				return getDate(date, time);
			}
		}

		return dateTime;

	}

	private static Date getDate(BigDecimal date) {
		if (date != null) {
			String dateStr = String.format(DATE_NUMBER_FORMATTER, date.intValueExact());
			SimpleDateFormat dateformat = new SimpleDateFormat(DATE_FORMAT);
			try {
				return dateformat.parse(dateStr);
			} catch (ParseException e) {
				log.error("invalide date format");
			}
		}
		return null;
	}

	public static Date getDate(BigDecimal date, BigDecimal time) {

		StringBuilder sb = new StringBuilder();
		if (date != null) {
			String dateStr = String.format(DATE_NUMBER_FORMATTER, date.intValueExact());
			try {
				if (time != null) {
					String timeStr = String.format(TIME_NUMBER_FORMATTER, time.intValueExact());

					SimpleDateFormat dateformat = new SimpleDateFormat(DATE_TIME_FORMAT);
					sb.append(dateStr).append(":").append(timeStr);
					return dateformat.parse(sb.toString());
				} else {
					return getDate(date);
				}
			} catch (ParseException e) {
				log.error("invalid date and time format");
			}
		}
		return null;
	}

	/**
	 * Convert the given Date to GMT string representation
	 * 
	 * @param date
	 * @param isGMT
	 *            if true gmt time will be given as a string
	 * @return
	 */
	public static String[] getDateAsString(Date date, boolean isGMT) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		if (isGMT) {
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		}
		String formatedDate = dateFormat.format(date);
		String[] dateParts = formatedDate.split(":");
		return dateParts;
	}

	private static Method getMethodFromPos(Method[] methods, GetterSetter getterSetter, int locator) {

		String suffix = String.valueOf(locator);

		suffix = (suffix.length() == 1) ? "0" + suffix : suffix;

		for (Method method : methods) {
			if (method.getName().startsWith(getterSetter.getMethodSuffix()) && method.getName().endsWith(suffix)) {
				return method;
			}
		}

		throw new IllegalArgumentException();
	}

	public static String getSplitedElement(String[] array, int index, String defaultVal) {
		if (index < array.length) {
			return array[index];
		} else {
			return defaultVal;
		}
	}

}
