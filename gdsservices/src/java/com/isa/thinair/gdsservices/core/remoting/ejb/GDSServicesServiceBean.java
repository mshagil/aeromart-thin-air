/*
 * 
==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
===============================================================================
 */
package com.isa.thinair.gdsservices.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.ETicketUpdateRequestDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.ServiceError;
import com.isa.thinair.commons.api.dto.ets.TicketCoupon;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayReq;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayRes;
import com.isa.thinair.commons.api.dto.ets.TicketNumberDetails;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.AVSResponse;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.dto.internal.criteria.GDSMessageSearchCriteria;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.EticketInfoTO;
import com.isa.thinair.gdsservices.api.model.GDSEdiMessages;
import com.isa.thinair.gdsservices.api.model.GDSMessage;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.avs.ProcessAVSRequest;
import com.isa.thinair.gdsservices.core.bl.avs.PublishAvailability;
import com.isa.thinair.gdsservices.core.bl.dsu.DSTUtil;
import com.isa.thinair.gdsservices.core.bl.dsu.PublishDSU;
import com.isa.thinair.gdsservices.core.bl.internal.reservation.SyncReservationTotalTicketPrice;
import com.isa.thinair.gdsservices.core.bl.ssm.ProcessSSMASM;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.bl.typeA.common.MessageErrorAdaptor;
import com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol.HandleDisplayTicket;
import com.isa.thinair.gdsservices.core.command.AuditAction;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.common.ReservationServicesProxy;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.service.bd.GDSServicesBDImpl;
import com.isa.thinair.gdsservices.core.service.bd.GDSServicesBDLocalImpl;
import com.isa.thinair.gdsservices.core.typeA.converters.Convert;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.gdsservices.core.typeA.parser.EdiParser;
import com.isa.thinair.gdsservices.core.typeA.parser.EdiParserFactory;
import com.isa.thinair.gdsservices.core.typeA.parser.TypeAParseConstants;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03.TkcReqMessagingUtil;
import com.isa.thinair.gdsservices.core.util.TypeAMessageUtil;
import com.isa.thinair.login.util.DatabaseUtil;
import com.isa.typea.common.Header;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.tkcreq.AATKCREQ;

/**
 * Session bean to provide GDS Services realted functionalities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
@Stateless
@RemoteBinding(jndiBinding = "GDSServicesService.remote")
@LocalBinding(jndiBinding = "GDSServicesService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class GDSServicesServiceBean extends PlatformBaseSessionBean implements GDSServicesBDImpl, GDSServicesBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(GDSServicesServiceBean.class);

	/**
	 * Process Request
	 * 
	 * @param bookingRequestDTO
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BookingRequestDTO processRequest(BookingRequestDTO bookingRequestDTO) throws ModuleException {
		GDSServicesBD gdsServicesBD = GDSServicesModuleUtil.getGDSServicesBD(false);

		try {
			gdsServicesBD.auditRequests(bookingRequestDTO, GDSExternalCodes.ProcessStatus.NOT_PROCESSED.getValue(), null);

			if (bookingRequestDTO.getBlockedSeats() != null && !bookingRequestDTO.getBlockedSeats().isEmpty()) {
				List<Integer> tempSegBcAllocIds = new ArrayList<>();
				for (TempSegBcAllocDTO tempSegBcAllocDTO : bookingRequestDTO.getBlockedSeats()) {
					tempSegBcAllocIds.add(tempSegBcAllocDTO.getId());
				}

				Collection<TempSegBcAlloc> tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(tempSegBcAllocIds);
				GDSServicesModuleUtil.getReservationBD().releaseBlockedSeats(tempSegBcAllocs);
			}

			GDSCredentialsDTO gdsCredentialsDTO = getGDSCredentials(bookingRequestDTO);
			ReservationServicesProxy reservationServicesProxy = new ReservationServicesProxy(this.sessionContext);
			bookingRequestDTO = reservationServicesProxy.processRequest(gdsCredentialsDTO, bookingRequestDTO);

			gdsServicesBD.auditRequests(bookingRequestDTO, bookingRequestDTO.getProcessStatus().getValue(),
					AuditAction.getActionDescription(bookingRequestDTO));
			return bookingRequestDTO;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processRequest ", ex);
			gdsServicesBD.auditRequests(bookingRequestDTO, GDSExternalCodes.ProcessStatus.ERROR_OCCURED.getValue(),
					ex.getMessageString());
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processRequest ", cdaex);
			gdsServicesBD.auditRequests(bookingRequestDTO, GDSExternalCodes.ProcessStatus.ERROR_OCCURED.getValue(),
					cdaex.getMessageString());
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Throwable e) {
			log.error(" ((o)) Throwable::processRequest ", e);
			gdsServicesBD.auditRequests(bookingRequestDTO, GDSExternalCodes.ProcessStatus.ERROR_OCCURED.getValue(),
					e.getMessage());
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getMessage());
		}
	}

	/**
	 * Processes external requests
	 * 
	 * @param reservationRequestMap
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map processRequests(Map reservationRequestMap) throws ModuleException {
		if (reservationRequestMap == null || reservationRequestMap.size() == 0) {
			throw new ModuleException("gdsservices.framework.emptyRequests");
		}

		GDSServicesBD gdsServicesBD = GDSServicesModuleUtil.getGDSServicesBD(false);

		try {
			Map responseMap = new HashMap();
			BookingRequestDTO bookingRequestDTO;
			String uniqueKey;

			for (Iterator itr = reservationRequestMap.keySet().iterator(); itr.hasNext();) {
				uniqueKey = (String) itr.next();
				bookingRequestDTO = (BookingRequestDTO) reservationRequestMap.get(uniqueKey);
				responseMap.put(uniqueKey, gdsServicesBD.processRequest(bookingRequestDTO));
			}

			return responseMap;
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processRequests ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processRequests ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			log.error(" ((o)) Exception::processRequests ", e);
			throw new ModuleException(e, e.getMessage());
		}
	}

	/**
	 * Audit Generic Request
	 * 
	 * @param bookingRequestDTO
	 * @param status
	 * @param processDesc
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void auditRequests(BookingRequestDTO bookingRequestDTO, String status, String processDesc) throws ModuleException {
		try {
			GDSCredentialsDTO gdsCredentialsDTO = getGDSCredentials(bookingRequestDTO);
			AuditAction.perform(bookingRequestDTO, status, processDesc, gdsCredentialsDTO);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::auditRequests ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::auditRequests ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get GDS Message
	 * 
	 * @param GDSMessage
	 *            ID
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public GDSMessage getGDSMessage(long messageId) throws ModuleException {
		try {
			return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.getGDSMessage(messageId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getGDSMessage ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Search GDS Messages
	 * 
	 * @param GDSMessageSearchCriteria
	 * @param startIndex
	 * @param noRecs
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page searchGDSMessages(GDSMessageSearchCriteria criteria, int startIndex, int noRecs) throws ModuleException {
		try {
			return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.searchGDSMessages(criteria, startIndex, noRecs);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::searchGDSMessages ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::searchGDSMessages ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Publish AVS
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void publishAvailabilityMessages() throws ModuleException {
		try {
			getPublishAvailabilityInstance().publishAvailabilityMessages();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::publishAVSMessages ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::searchGDSMessages ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Publish DSU
	 * 
	 * @throws ModuleException
	 */
	public void publishDailyScheduleUpdateMessages() throws ModuleException {
		try {
			getPublishDailyScheduleUpdateInstance().publishDailyScheduleUpdateMessages();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::publishDSUMessages ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::searchDSUMessages ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public void sendFlownSegmentStatues(Collection<Integer> ppfsIds) throws ModuleException {
		try {
			if (ppfsIds != null && ppfsIds.size() > 0) {
				// TODO = What is the correct Type A channel code?
				Collection<EticketInfoTO> relatedEtickets = GDSServicesDAOUtils.DAOInstance.GDS_TypeA_JDBC_DAO
						.getEticketListForPPFSIds(ppfsIds, 3);

				TkcReqMessagingUtil.sendTkcreqForFlownSegments(relatedEtickets);
			}

		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::sendFlownSegmentStatues ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::sendFlownSegmentStatues ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			log.error(" ((o)) CommonsDataAccessException::sendFlownSegmentStatues ", ex);
			throw new ModuleException(null, ex);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String processEdifactMesssage(String ediMessage, boolean isDisplayForETS) throws ModuleException {

		String ediResult = null;
		DefaultServiceResponse response;
		IATAOperation ediOperation = null;

		DefaultBaseCommand command;
		GDSEdiMessages gdsMessage = new GDSEdiMessages();
		EDIMessageDTO messageDTO = null;

		try {

			gdsMessage.setReqMessage(ediMessage.substring(0, ediMessage.length() > 3999 ? 3999 : ediMessage.length()));
			gdsMessage.setReqTimestamp(new Date());

			UserPrincipal userPrincipal = this.getUserPrincipal();
			messageDTO = new EDIMessageDTO(TypeAConstants.MessageFlow.RESPOND);
			messageDTO.setRawRequest(ediMessage);
			messageDTO.setRequestIp(userPrincipal.getIpAddress());
			Convert.processInboundMessage(messageDTO);

			TypeAMessageUtil.typeAMessagePostProcess(messageDTO, isDisplayForETS);
			gdsMessage.setGdsTransactionRef(messageDTO.getRequestMetaDataDTO().getMessageReference());
			

			command = (DefaultBaseCommand) GDSServicesModuleUtil.getModule().getLocalBean(
					Constants.CommandNames.EDI_PROTOCOL_PROCESSOR);
			command.setParameter(TypeACommandParamNames.EDI_REQUEST_DTO, messageDTO);
			response = (DefaultServiceResponse) command.execute();
			 log.info("\n ********Validated edi message" + new Date() + "\n" + messageDTO.getRawRequest());
			GDSExternalCodes.ProcessStatus processStatus = (GDSExternalCodes.ProcessStatus) response
					.getResponseParam(TypeACommandParamNames.EDI_PROCESS_STATUS);

			if (processStatus == GDSExternalCodes.ProcessStatus.VALIDATE_SUCCESS) {

				ediOperation = messageDTO.getRequestMetaDataDTO().getIataOperation(); 
				String commandName = IataOperationConfig.getMessageHandlerCommand(ediOperation);
				command = (DefaultBaseCommand) GDSServicesModuleUtil.getModule().getLocalBean(commandName);
				if (command != null) {

					HashMap<String, Object> paramMap = new HashMap<String, Object>();
					paramMap.put(TypeACommandParamNames.USER_PRINCIPLE, userPrincipal);
					paramMap.put(TypeACommandParamNames.EDI_REQUEST_DTO, messageDTO);
					command.setParameters(paramMap);
					response = (DefaultServiceResponse) command.execute();
					response.addResponceParams(paramMap);
					 log.info("\n ********Processed edi message: \n" + messageDTO.getRawRequest());
					if (messageDTO.isSendResponse()) {
						ediResult = Convert.toEdiMessage(messageDTO);
					}
					 log.info("\n ********Created edi responce: \n" + ediResult);
					TypeAMessageUtil.saveMessagingSession(messageDTO);

					if (messageDTO.isRollbackTransaction()) {
						sessionContext.setRollbackOnly();
					}
				}
			} else {
				CodeSetEnum.CS0085 cs0085;
				String respAssociationCode = messageDTO.getRespAssociationCode();
				if (respAssociationCode.equals(TypeAConstants.AssociationCode.INACTIVE)) {
					cs0085 = CodeSetEnum.CS0085.TOO_OLD;
				} else if (respAssociationCode.equals(TypeAConstants.AssociationCode.DUPLICATE)) {
					cs0085 = CodeSetEnum.CS0085.DUPLICATE_DETECTED;
				} else {
					cs0085 = CodeSetEnum.CS0085.UNSPECIFIED_ERROR;
				}

					throw new InteractiveEDIException(cs0085, "Message not processed successfully.");
			}

		} catch (InteractiveEDIException e) {
			MessageHeader messageHeader = null;
			Header header = null;

			EdiParser ediParser = EdiParserFactory.getEdiParser(null);
			Set<TypeAParseConstants.Segment> segs = new HashSet<TypeAParseConstants.Segment>();
			segs.add(TypeAParseConstants.Segment.UNB);
			segs.add(TypeAParseConstants.Segment.UNH);
			Map<TypeAParseConstants.Segment, List<?>> parsedTos = ediParser.parseEdiMessage(ediMessage, segs);
			if (!parsedTos.get(TypeAParseConstants.Segment.UNB).isEmpty()) {
				header = ((List<Header>) parsedTos.get(TypeAParseConstants.Segment.UNB)).get(0);
			}
			if (!parsedTos.get(TypeAParseConstants.Segment.UNH).isEmpty()) {
				messageHeader = ((List<MessageHeader>) parsedTos.get(TypeAParseConstants.Segment.UNH)).get(0);
			}

			ediResult = MessageErrorAdaptor.getErrorMessage(header, messageHeader, e);

			log.error("InteractiveEDIException occour [" + e.getDescription() + "]", e);

		} catch (ModuleException e) {
			log.error("ModuleException occour", e);
			MessageHeader messageHeader = null;
			Header header = null;

			EdiParser ediParser = EdiParserFactory.getEdiParser(null);
			Set<TypeAParseConstants.Segment> segs = new HashSet<TypeAParseConstants.Segment>();
			segs.add(TypeAParseConstants.Segment.UNB);
			segs.add(TypeAParseConstants.Segment.UNH);
			Map<TypeAParseConstants.Segment, List<?>> parsedTos = ediParser.parseEdiMessage(ediMessage, segs);
			if (!parsedTos.get(TypeAParseConstants.Segment.UNB).isEmpty()) {
				header = ((List<Header>) parsedTos.get(TypeAParseConstants.Segment.UNB)).get(0);
			}
			if (!parsedTos.get(TypeAParseConstants.Segment.UNH).isEmpty()) {
				messageHeader = ((List<MessageHeader>) parsedTos.get(TypeAParseConstants.Segment.UNH)).get(0);
			}

			InteractiveEDIException ediException = new InteractiveEDIException(CodeSetEnum.CS0085.INVALID_VALUE, e.getExceptionCode());
			ediResult = MessageErrorAdaptor.getErrorMessage(header, messageHeader, ediException);

		} catch (Exception e) {
			log.error("Exception occour", e);
		} finally {
			gdsMessage.setResMessage(ediResult);
			gdsMessage.setResTimestamp(new Date());
			gdsMessage.setErrors(messageDTO.getErrors().toString());
			GDSServicesModuleUtil.getGDSServicesBD(false).saveGdsEdiMessage(gdsMessage);
		}
		log.info("\n ********Sending edi responce: \n" + ediResult);
		return ediResult;
	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	private GDSCredentialsDTO getCallerCredentials() throws ModuleException {
		UserPrincipal userPrincipal = this.getUserPrincipal();

		if (userPrincipal == null || userPrincipal.getName() == null) {
			throw new ModuleException("module.credential.error");
		}

		GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
		gdsCredentialsDTO.setUserId(userPrincipal.getUserId());
		gdsCredentialsDTO.setAgentCode(userPrincipal.getAgentCode());
		gdsCredentialsDTO.setAgentStation(userPrincipal.getAgentStation());
		gdsCredentialsDTO.setSalesChannelCode(userPrincipal.getSalesChannel());
		gdsCredentialsDTO.setColUserDST(userPrincipal.getColUserDST());
		gdsCredentialsDTO.setDefaultCarrierCode(userPrincipal.getDefaultCarrierCode());
		gdsCredentialsDTO.setPrivileges(getPrivileges(userPrincipal.getUserId()));

		return gdsCredentialsDTO;
	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	private GDSCredentialsDTO getGDSCredentials(BookingRequestDTO bookingRequestDTO) throws ModuleException {

		String gdsUser = null;
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		for(GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getGdsCode().equals(bookingRequestDTO.getGds().getCode())) {
				gdsUser = gdsStatusTO.getUserId();
			}
		}
		if (gdsUser == null) {
			throw new ModuleException("module.credential.error");
		}
		User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(gdsUser);
		if (user == null) {
			throw new ModuleException("module.credential.error");
		}
		Agent agent = GDSServicesModuleUtil.getTravelAgentBD().getAgent(user.getAgentCode());
		if (agent == null) {
			throw new ModuleException("module.credential.error");
		}
		Collection<UserDST> colUserDST = DatabaseUtil.getAgentLocalTime(agent.getStationCode());
		GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
		gdsCredentialsDTO.setUserId(user.getUserId());
		gdsCredentialsDTO.setAgentCode(agent.getAgentCode());
		gdsCredentialsDTO.setAgentStation(agent.getStationCode());
		gdsCredentialsDTO.setSalesChannelCode(ReservationInternalConstants.SalesChannel.GDS);
		gdsCredentialsDTO.setColUserDST(colUserDST);
		gdsCredentialsDTO.setDefaultCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		gdsCredentialsDTO.setPrivileges(getPrivileges(user.getUserId()));

		return gdsCredentialsDTO;
	}

	/**
	 * Return an instance of {@link PublishAvailability}
	 * 
	 * @return
	 */
	private PublishAvailability getPublishAvailabilityInstance() {
		if (publishAvailability == null) {
			publishAvailability = new PublishAvailability();
		}
		return publishAvailability;
	}

	/**
	 * Return an instance of {@link PublishDSU}
	 * 
	 * @return
	 */
	private PublishDSU getPublishDailyScheduleUpdateInstance() {
		if (publishDSU == null) {
			publishDSU = new PublishDSU();
		}
		return publishDSU;
	}
	
	private ProcessSSMASM getProcessSSMInstance() {
		if (processSSM == null) {
			processSSM = new ProcessSSMASM();
		}
		return processSSM;
	}
	
	private ProcessAVSRequest getProcessAVSInstance() {
		if (processAVS == null) {
			processAVS = new ProcessAVSRequest();
		}
		return processAVS;
	}

	/**
	 * Retrieves user privileges
	 * 
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	private Set getPrivileges(String userId) throws ModuleException {
		User user = GDSServicesModuleUtil.getSecurityBD().getUser(userId);
		return user.getPrivitedgeIDs();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveGdsEdiMessage(GDSEdiMessages message) throws ModuleException {
		GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.saveGdsEdiMessage(message);
	}

	private PublishAvailability publishAvailability;

	private PublishDSU publishDSU;
	
	private ProcessSSMASM processSSM;
	
	private ProcessAVSRequest processAVS;	
	

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SSMASMResponse processSSMASMRequests(SSMASMMessegeDTO ssiMessegeDTO) throws ModuleException {
		try {
			if (ssiMessegeDTO == null) {
				throw new ModuleException("gdsservices.framework.emptyRequests");
			}
			return getProcessSSMInstance().processSSMASMRequests(null, ssiMessegeDTO);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processFlightScheduleRequests ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processFlightScheduleRequests ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Throwable e) {
			log.error(" ((o)) Throwable::processFlightScheduleRequests ", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getMessage());
		}
	}
	
	public AVSResponse processAVSRequests(AVSRequestDTO avsRequestDTO) throws ModuleException {
		try {
			if (avsRequestDTO == null || avsRequestDTO.getAVSSegmentDTOs().isEmpty()) {
				throw new ModuleException("gdsservices.framework.emptyRequests");
			}
			return getProcessAVSInstance().beginProcessAVS(avsRequestDTO);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processAVSRequests ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processAVSRequests ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Throwable e) {
			log.error(" ((o)) Throwable::processAVSRequests ", e);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getMessage());
		}
	}	
	
	
	public Collection<FlightSchedule> getDSTApplicableSubSchedules(FlightSchedule schedule) throws ModuleException {
		try {
			return DSTUtil.getDSTBasedSchedules(schedule);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::getDSTApplicableSubSchedules ", ex);
			// throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (Exception e) {
			log.error(" ((o)) Exception::getDSTApplicableSubSchedules ", e);
			//throw new ModuleException(e, e.getMessage());
		}
		return null;
	}

	public AeroMartUpdateCouponStatusRs updateCouponStatus(AeroMartUpdateCouponStatusRq request) throws ModuleException {


		Map<String, List<Integer>> ticketsAndCoupons = new HashMap<>();
		Map<String, Map<Integer, String>> ticketsCouponsAndStatus = new HashMap<>();

		List<Integer> coupons;
		Map<Integer, String> couponStatus;
		int couponNumber;

		for (TicketNumberDetails ticketNumberDetails : request.getTickets()) {
			if (!ticketsAndCoupons.containsKey(ticketNumberDetails.getTicketNumber())) {
				coupons = new ArrayList<>();
				couponStatus = new HashMap<>();

				ticketsAndCoupons.put(ticketNumberDetails.getTicketNumber(), coupons);
				ticketsCouponsAndStatus.put(ticketNumberDetails.getTicketNumber(), couponStatus);

			} else {
				coupons = ticketsAndCoupons.get(ticketNumberDetails.getTicketNumber());
				couponStatus = ticketsCouponsAndStatus.get(ticketNumberDetails.getTicketNumber());
			}

			for (TicketCoupon ticketCoupon : ticketNumberDetails.getTicketCoupon()) {
				couponNumber = Integer.parseInt(ticketCoupon.getCouponNumber());

				coupons.add(couponNumber);
				couponStatus.put(couponNumber, ticketCoupon.getStatus());
			}

		}


		List<LCCClientReservation> reservations = GDSServicesDAOUtils.DAOInstance.GDS_TypeA_JDBC_DAO.loadReservations(ticketsAndCoupons);

		ETicketUpdateRequestDTO eTicketUpdateRequestDTO;
		String requestCouponStatus;
		String internalCouponStatus = null;
		LCCClientReservationSegment segment;

		for (LCCClientReservation reservation : reservations) {
			for (LCCClientReservationPax passenger : reservation.getPassengers()) {
				for (LccClientPassengerEticketInfoTO eticket : passenger.geteTickets()) {

					requestCouponStatus = ticketsCouponsAndStatus.get(eticket.getPaxETicketNo()).get(eticket.getCouponNo());
					if (requestCouponStatus.equals(TypeAConstants.CouponStatus.ORIGINAL_ISSUE)) {
						internalCouponStatus = CommonsConstants.EticketStatus.OPEN.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.CHECKED_IN)) {
						internalCouponStatus = CommonsConstants.EticketStatus.CHECKEDIN.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.BOARDED)) {
						internalCouponStatus = CommonsConstants.EticketStatus.BOARDED.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.FLOWN)) {
						internalCouponStatus = CommonsConstants.EticketStatus.FLOWN.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.VOIDED)) {
						internalCouponStatus = CommonsConstants.EticketStatus.VOID.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.EXCHANGED)) {
						internalCouponStatus = CommonsConstants.EticketStatus.EXCHANGED.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.REFUNDED)) {
						internalCouponStatus = CommonsConstants.EticketStatus.REFUNDED.code();
					} else if (requestCouponStatus.equals(TypeAConstants.CouponStatus.SUSPENDED)) {
						internalCouponStatus = CommonsConstants.EticketStatus.SUSPENDED.code();
					}

					segment = null;
					for (LCCClientReservationSegment seg : reservation.getSegments()) {
						if (seg.getFlightSegmentRefNumber().equals(eticket.getFlightSegmentRef())) {
							segment	= seg;
						}
					}

					eTicketUpdateRequestDTO = new ETicketUpdateRequestDTO();
					eTicketUpdateRequestDTO.setFlightNo(segment.getFlightNo());
					eTicketUpdateRequestDTO.setDepartureDate(segment.getDepartureDate());
					eTicketUpdateRequestDTO.setSegmentCode(segment.getSegmentCode());
					eTicketUpdateRequestDTO.seteTicketNo(eticket.getPaxETicketNo());
					eTicketUpdateRequestDTO.setStatus(internalCouponStatus);
//					eTicketUpdateRequestDTO.setUnaccompaniedETicketNo(eTicketStatusUpdateRQ.getUnaccompaniedETicketNo());
//					eTicketUpdateRequestDTO.setUnaccompaniedStatus(convertStatus(eTicketStatusUpdateRQ.getUnaccompaniedStatus()));
					eTicketUpdateRequestDTO.setTitle(passenger.getTitle());
					eTicketUpdateRequestDTO.setFirstName(passenger.getFirstName());
					eTicketUpdateRequestDTO.setLastName(passenger.getLastName());
//					eTicketUpdateRequestDTO.setInfantFirstName(eTicketStatusUpdateRQ.getInfantFirstName());
//					eTicketUpdateRequestDTO.setInfantLsatName(eTicketStatusUpdateRQ.getInfantLastName());
					eTicketUpdateRequestDTO.setCouponNumber(String.valueOf(eticket.getCouponNo()));
//					eTicketUpdateRequestDTO.setUnaccompaniedCouponNo(eTicketStatusUpdateRQ.getUnaccompaniedCoupenNo());

					GDSServicesModuleUtil.getReservationBD().updateETicketStatus(eTicketUpdateRequestDTO);

				}
			}
		}


		AeroMartUpdateCouponStatusRs response = new AeroMartUpdateCouponStatusRs();
		response.setSuccess(true);

		return response;

	}

	public AeroMartUpdateExternalCouponStatusRs updateExternalCouponStatus(AeroMartUpdateExternalCouponStatusRq request)
			throws ModuleException {
		AeroMartUpdateExternalCouponStatusRs response = new AeroMartUpdateExternalCouponStatusRs();
		try {
			SyncReservationTotalTicketPrice totalPriceSync = new SyncReservationTotalTicketPrice(request);
			totalPriceSync.updateExternalCouponStatus();
			response.setSuccess(true);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateExternalCouponStatus ", ex);
			ServiceError serviceError = new ServiceError();
			serviceError.setCode(ex.getExceptionCode());
			serviceError.setMessage(ex.getMessage());
			response.setServiceError(serviceError);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
			
		} catch (Exception e) {
			log.error(" ((o)) Exception::updateExternalCouponStatus ", e);
			ServiceError serviceError = new ServiceError();			
			serviceError.setMessage(e.getMessage());
			response.setServiceError(serviceError);
			throw new ModuleException(e, e.getMessage());
		}

		return response;

	}
	
}