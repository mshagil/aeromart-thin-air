package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ExtendTimelimitRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.IssueEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

/**
 * @author Manoj Dhanushka
 */
public class ExtendTimelimitResponseTransformer {
	
	private static final ReservationAction RESERVATION_ACTION = ReservationAction.EXTEND_TIMELIMIT;

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		ExtendTimelimitRequest extendTimelimitRequest = (ExtendTimelimitRequest) reservationResponseMap.get(RESERVATION_ACTION);

		if (extendTimelimitRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS_IGNORE_NOTIFICATION);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, extendTimelimitRequest);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, extendTimelimitRequest);
		}
		bookingRequestDTO.getErrors().addAll(extendTimelimitRequest.getErrorCode());

		return bookingRequestDTO;
	}

}
