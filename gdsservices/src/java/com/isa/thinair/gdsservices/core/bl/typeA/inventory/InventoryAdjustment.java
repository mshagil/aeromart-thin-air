package com.isa.thinair.gdsservices.core.bl.typeA.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.GdsPublishFccSegBcInventoryTo;
import com.isa.thinair.airinventory.api.dto.TempSegBcAllocDto;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.exception.ErrorPath;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.typeA.transformers.FlightDTOsTranformer;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.TypeAMessageUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.OriginDestinationInformation;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.itareq.AAITAREQ;
import com.isa.typea.itareq.ITAREQMessage;
import com.isa.typea.itares.AAITARES;
import com.isa.typea.itares.ITARESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Dilan Anuruddha
 * @isa.module.command name="inventoryAdjustment"
 */
public class InventoryAdjustment extends EdiMessageHandler {

	private static Log log = LogFactory.getLog(InventoryAdjustment.class);

	private AAITAREQ itareq;
	private AAITARES itares;


	public void handleMessage() {

		ErrorInformation errorInformation;
		MessageFunction messageFunction;
		ErrorPath errorPath;
		ErrorPath childErrorPath;
		boolean errorAttached;

		List<OriginDestinationInformation> reqOriginDestInfo;
		List<OriginDestinationInformation> respOriginDestInfo;

		itareq = (AAITAREQ) messageDTO.getRequest();
		ITAREQMessage itareqMessage = itareq.getMessage();

		itares = new AAITARES();
		ITARESMessage itaresMessage = new ITARESMessage();
		itaresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(itareqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));

		itares.setHeader(GDSDTOUtil.createRespEdiHeader(itareq.getHeader(), itaresMessage.getMessageHeader(), messageDTO));
		itares.setMessage(itaresMessage);
		messageDTO.setResponse(itares);

		try {

			validateMessage();
			releaseBlockedSeats();
			blockSeats();

			itaresMessage.setMessageFunction(itareqMessage.getMessageFunction());

			reqOriginDestInfo = itareqMessage.getOriginDestinationInfo();
			respOriginDestInfo = itaresMessage.getOriginDestinationInfo();


			for (OriginDestinationInformation origDest : reqOriginDestInfo) {
				for (FlightInformation flightInfo : origDest.getFlightInfomation()) {
					if (flightInfo.getActionCode().equals(TypeAConstants.ActionCode.REPLY_REQUIRED)) {
						flightInfo.getFlightRemarks().add(TypeAConstants.SequenceNumber.E_TICKET_CANDIDATE);
						flightInfo.setActionCode(TypeAConstants.StatusCode.HOLDS_CONFIRMED);
					} else if (flightInfo.getActionCode().equals(TypeAConstants.ActionCode.CANCEL)) {
						flightInfo.setActionCode(TypeAConstants.StatusCode.CANCELLED);
					}
				}
				respOriginDestInfo.add(origDest);
			}
		} catch (GdsTypeAException e) {
			log.error("inventory adjustment ---- ", e);

			messageFunction = new MessageFunction();
			messageFunction.setServiceType(TypeAConstants.BusinessFunction.AIR_PROVIDER);
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.INVENTORY_ADJUSTMENT);
			itaresMessage.setMessageFunction(messageFunction);

			if (e.attachError()) {
				messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);

				errorInformation = new ErrorInformation();
				errorInformation.setApplicationErrorCode(e.getEdiErrorCode());

				errorPath = TypeAMessageUtil.getErrorPath(IATAOperation.ITARES, messageFunction.getMessageFunction(), e.getEdiErrorCode());

				errorAttached = false;
				if (errorPath.getMessageElement() == TypeAConstants.MessageElement.ROOT) {
					if (errorPath.getChildElement() != null) {
						childErrorPath = errorPath.getChildElement();
						if (childErrorPath.getMessageElement() == TypeAConstants.MessageElement.ODI) {
							childErrorPath = childErrorPath.getChildElement();
							if (childErrorPath.getMessageElement() == TypeAConstants.MessageElement.TVL) {

								reqOriginDestInfo = itareqMessage.getOriginDestinationInfo();
								respOriginDestInfo = itaresMessage.getOriginDestinationInfo();
								for (OriginDestinationInformation origDest : reqOriginDestInfo) {
									for (FlightInformation flightInfo : origDest.getFlightInfomation()) {
										if (flightInfo.getActionCode().equals(TypeAConstants.ActionCode.REPLY_REQUIRED)) {
											flightInfo.setActionCode(TypeAConstants.StatusCode.UNABLE);
											flightInfo.setErrorInformation(errorInformation);
										}
									}
									respOriginDestInfo.add(origDest);
								}

								errorAttached = true;
							}
						}

						if (!errorAttached) {
							itaresMessage.setErrorInformation(errorInformation);
						}
					} else {
						itaresMessage.setErrorInformation(errorInformation);
					}
				}
			}

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
//			messageDTO.getErrors().add(e.getErrorMessageKey());

		} catch (Exception e) {
			log.error("inventory adjustment ---- ", e);

			messageFunction = new MessageFunction();
			messageFunction.setServiceType(TypeAConstants.BusinessFunction.AIR_PROVIDER);
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.INVENTORY_ADJUSTMENT);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			itaresMessage.setMessageFunction(messageFunction);

			errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			itaresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

		}
	}

	private void validateMessage() throws GdsTypeAException {

		boolean erroneous = false;
		ErrorInformation errorInformation;
		ITAREQMessage itareqMessage = itareq.getMessage();

		constraintsValidator.validateMessageFunction(itareqMessage.getMessageFunction());

		for (OriginDestinationInformation originDestinationInfo : itareqMessage.getOriginDestinationInfo()) {
			for (FlightInformation flightInfo : originDestinationInfo.getFlightInfomation()) {
				if (flightInfo.getTravellerCount() != null && Integer.parseInt(flightInfo.getTravellerCount()) <= 0) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NUMBER_IN_PARTY_INVALID, TypeANotes.ErrorMessages.NO_MESSAGE);
				}

				if (!flightInfo.getCompanyInfo().getMarketingAirlineCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					errorInformation = new ErrorInformation();
					errorInformation.setSubjectQualifier(TypeAConstants.TextSubjectQualifier.CODED_FREE_TEXT);
					errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.AIRLINE_DESIGNATOR_INVALID);

					flightInfo.setErrorInformation(errorInformation);
					erroneous = true;
				}
			}
		}

		if (erroneous) {
			itares.getMessage().getOriginDestinationInfo().addAll(itareqMessage.getOriginDestinationInfo());

			GdsTypeAException exception = new GdsTypeAException();
			exception.setAttachError(false);
			throw exception;
		}

	}


	private void blockSeats() throws GdsTypeAException {

		Collection<TempSegBcAlloc> seatBlock = null;
		FlightPriceRQ flightPriceRQ = null;

		List<Integer> blockSeats;
		Map<String, Integer> tempSegBcAllocIds;
		List<TempSegBcAllocDto> blockedSeatsInfo;
		List<List<FlightInformation>> onds;
		List<FlightInformation> ond;
		Map<Integer, Integer> adjustedAllocations;
		List<Integer> blockSeatIds;
		FlightSegmentTO flightSegmentTO;
		int travellersCount;
		FlightDTOsTranformer flightDTOsTranformer;
		String gdsBookingClass;
		String bookingClass;
		String gdsCarrierCode ;
		FCCSegBCInventory fccSegBCInventory;
		String fccSegBcAllocKey;

		if (messageDTO.getMessagingSession().containsKey(TypeAConstants.MessageSessionKey.BLOCKED_SEATS)) {
			tempSegBcAllocIds = (Map<String, Integer>) messageDTO.getMessagingSession().get(TypeAConstants.MessageSessionKey.BLOCKED_SEATS);
		} else {
			tempSegBcAllocIds = new HashMap<String, Integer>();
			messageDTO.getMessagingSession().put(TypeAConstants.MessageSessionKey.BLOCKED_SEATS, tempSegBcAllocIds);
		}

		ITAREQMessage message = itareq.getMessage();
		OriginatorInformation originatorInfo = message.getOriginatorInformation();
		gdsCarrierCode = originatorInfo.getSenderSystemDetails().getCompanyCode();

		List<List<FlightInformation>> fliteredFlightInfo = filterFlightInformation(message, TypeAConstants.ActionCode.REPLY_REQUIRED);
		onds = new ArrayList<List<FlightInformation>>();
		adjustedAllocations = new HashMap<Integer, Integer>();
		for (List<FlightInformation> ondFlightInfo : fliteredFlightInfo) {
			ond = new ArrayList<FlightInformation>();
			if (!ondFlightInfo.isEmpty()) {
				for (FlightInformation flightInfo : ondFlightInfo) {
					try {
						flightSegmentTO = CommonUtil.getBestMatchedFlightSegmentTo(flightInfo);
					} catch (ModuleException e) {
						throw new GdsTypeAException();
					}
					if (flightSegmentTO != null) {
						gdsBookingClass = flightInfo.getFlightPreference().getBookingClass();
						bookingClass = GDSServicesUtil.getBookingCode(gdsCarrierCode, gdsBookingClass);

						fccSegBCInventory = GDSServicesModuleUtil.getFlightInventoryBD()
								.getFCCSegBCInventory(flightSegmentTO.getFlightSegId(), bookingClass);
						travellersCount = Integer.parseInt(flightInfo.getTravellerCount());

						if (fccSegBCInventory == null) {
							onInventoryUnavailable(flightSegmentTO, bookingClass, gdsCarrierCode);
							throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.RES_BOOKING_DESIGNATOR_INVALID_MISSING, TypeANotes.ErrorMessages.NO_MESSAGE);
						}

						fccSegBcAllocKey = String.valueOf(fccSegBCInventory.getFccsbInvId());
						if (tempSegBcAllocIds.containsKey(fccSegBcAllocKey)) {
							adjustedAllocations.put(tempSegBcAllocIds.get(fccSegBcAllocKey), travellersCount);
						} else {
							ond.add(flightInfo);
						}
					} else {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.FLIGHT_NOT_ACTIVE, TypeANotes.ErrorMessages.NO_MESSAGE);
					}
				}
				if (!ond.isEmpty()) {
					onds.add(ond);
				}
			}
		}

		if (!onds.isEmpty()) {
			try {
				flightDTOsTranformer = new FlightDTOsTranformer();
				flightDTOsTranformer.setOnds(onds);
				flightDTOsTranformer.setGdsCarrierCode(originatorInfo.getSenderSystemDetails().getCompanyCode());
				flightDTOsTranformer.setActionCode(TypeAConstants.ActionCode.REPLY_REQUIRED);
				flightPriceRQ = flightDTOsTranformer.getFlightPriceRQ();
			} catch (ModuleException e) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_PROCESS, TypeANotes.ErrorMessages.NO_MESSAGE);
			}
		}


		try {
			for (int seatBlockId : adjustedAllocations.keySet()) {
				blockSeatIds = new ArrayList<Integer>();
				blockSeatIds.add(seatBlockId);
				Collection<TempSegBcAlloc> allocs = GDSServicesModuleUtil.getFlightInventoryResBD()
						.adjustSeatsBlocked(blockSeatIds, adjustedAllocations.get(seatBlockId), 0);

				for (TempSegBcAlloc alloc : allocs) {
					fccSegBcAllocKey = String.valueOf(alloc.getFccsbaId());
					tempSegBcAllocIds.put(fccSegBcAllocKey, alloc.getId());

				}
			}

		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_PROCESS, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		FlightPriceRS flightPriceRS;
		if (flightPriceRQ != null && !flightPriceRQ.getOriginDestinationInformationList().isEmpty()) {

			try {
				flightPriceRS = GDSServicesModuleUtil.getAirproxyReservationQueryBD().quoteFlightPrice(flightPriceRQ, null);
			} catch (ModuleException e) {
				onInventoryUnavailable(onds, gdsCarrierCode);
				log.error("GDS inventory adjustment error ", e);
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NOT_AVAILABLE, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			PriceInfoTO priceInfo = flightPriceRS.getSelectedPriceFlightInfo();

			if (priceInfo != null) {
				FareSegChargeTO fareSegChargeTO = priceInfo.getFareSegChargeTO();

				try {
					seatBlock = (Collection<TempSegBcAlloc>) GDSServicesModuleUtil.getAirproxyReservationQueryBD().priviledgeUserBlockSeat(flightPriceRQ, fareSegChargeTO);

					for (TempSegBcAlloc alloc : seatBlock) {
						fccSegBcAllocKey = String.valueOf(alloc.getFccsbaId());
						tempSegBcAllocIds.put(fccSegBcAllocKey, alloc.getId());
					}

				} catch (ModuleException e) {
					onInventoryUnavailable(onds, gdsCarrierCode);
					log.error("GDS inventory adjustment error ", e);
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NOT_AVAILABLE, TypeANotes.ErrorMessages.NO_MESSAGE);
				}

			} else {
				onInventoryUnavailable(onds, gdsCarrierCode);
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NOT_AVAILABLE, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

		}
	}

	private void releaseBlockedSeats() throws GdsTypeAException {
		ReservationBD reservationBD = GDSServicesModuleUtil.getReservationBD();
		FlightSegmentTO flightSegmentTO;
		FCCSegBCInventory fccSegBCInventory;
		String gdsBookingClass;
		String bookingClass;
		String gdsCarrierCode ;
		String fccSegBcAllocKey;


		ITAREQMessage message = itareq.getMessage();
		OriginatorInformation originatorInfo = message.getOriginatorInformation();
		gdsCarrierCode = originatorInfo.getSenderSystemDetails().getCompanyCode();

		// TODO ---- consider interline
		Map<String, Integer> tempSegBcAllocIds = null;

		if (messageDTO.getMessagingSession().containsKey(TypeAConstants.MessageSessionKey.BLOCKED_SEATS)) {
			tempSegBcAllocIds = (Map<String, Integer>) messageDTO.getMessagingSession().get(TypeAConstants.MessageSessionKey.BLOCKED_SEATS);

			AAITAREQ itareq = (AAITAREQ) messageDTO.getRequest();
			List<List<FlightInformation>> flightInformation = filterFlightInformation(itareq.getMessage(), TypeAConstants.ActionCode.CANCEL);

			if (!flightInformation.isEmpty()) {
				List<Integer> tempSegBcAllocIdsToRelease = new ArrayList<Integer>();
				Map<Integer, Integer> tempSegBcAllocIdsToAdjust = new HashMap<>();
				for (List<FlightInformation> ond : flightInformation) {
					for (FlightInformation flightInfo : ond) {
						try {
							flightSegmentTO = CommonUtil.getBestMatchedFlightSegmentTo(flightInfo);
							gdsBookingClass = flightInfo.getFlightPreference().getBookingClass();
							bookingClass = GDSServicesUtil.getBookingCode(gdsCarrierCode, gdsBookingClass);

							fccSegBCInventory = GDSServicesModuleUtil.getFlightInventoryBD().getFCCSegBCInventory(flightSegmentTO.getFlightSegId(), bookingClass);
							fccSegBcAllocKey = String.valueOf(fccSegBCInventory.getFccsbInvId());

						} catch (ModuleException e) {
							throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.FLIGHT_NOT_ACTIVE, TypeANotes.ErrorMessages.NO_MESSAGE);
						}

						if (tempSegBcAllocIds.containsKey(fccSegBcAllocKey)) {

							List<Integer> ids = new ArrayList<>();
							ids.add(tempSegBcAllocIds.get(fccSegBcAllocKey));
							List<TempSegBcAlloc> tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(ids);

							if (tempSegBcAllocs == null || tempSegBcAllocs.isEmpty()) {
								throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
							}

							TempSegBcAlloc tempSegBcAlloc = tempSegBcAllocs.get(0);
							if (tempSegBcAlloc.getSeatsBlocked().compareTo(Integer.parseInt(flightInfo.getTravellerCount())) == 0) {
								tempSegBcAllocIdsToRelease.add(tempSegBcAllocIds.get(fccSegBcAllocKey));
							} else if (tempSegBcAlloc.getSeatsBlocked().compareTo(Integer.parseInt(flightInfo.getTravellerCount())) > 0) {
								tempSegBcAllocIdsToAdjust.put(tempSegBcAllocIds.get(fccSegBcAllocKey), - Integer.parseInt(flightInfo.getTravellerCount()));
							} else {
								throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
							}


						} else {
							throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
						}
					}
				}

				if (!tempSegBcAllocIdsToRelease.isEmpty()) {
					List<TempSegBcAlloc> tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(tempSegBcAllocIdsToRelease);

					try {
						reservationBD.releaseBlockedSeats(tempSegBcAllocs);
						for (TempSegBcAlloc tempSegBcAlloc : tempSegBcAllocs) {
							tempSegBcAllocIds.remove(String.valueOf(tempSegBcAlloc.getFccsbaId()));
						}
					} catch (ModuleException e) {
						throw new GdsTypeAException();
					}
				}

				if (!tempSegBcAllocIdsToAdjust.isEmpty()) {
					try {
						for (int seatBlockId : tempSegBcAllocIdsToAdjust.keySet()) {
							List<Integer> blockSeatIds = new ArrayList<Integer>();
							blockSeatIds.add(seatBlockId);
							Collection<TempSegBcAlloc> allocs = GDSServicesModuleUtil.getFlightInventoryResBD()
									.adjustSeatsBlocked(blockSeatIds, tempSegBcAllocIdsToAdjust.get(seatBlockId), 0);

							for (TempSegBcAlloc alloc : allocs) {
								fccSegBcAllocKey = String.valueOf(alloc.getFccsbaId());
								tempSegBcAllocIds.put(fccSegBcAllocKey, alloc.getId());

							}
						}
					} catch (ModuleException e) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
					}
				}

				List<TempSegBcAlloc> tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(tempSegBcAllocIdsToRelease);

				try {
					reservationBD.releaseBlockedSeats(tempSegBcAllocs);
				} catch (ModuleException e) {
					throw new GdsTypeAException();
				}
			}
		}

	}

	protected Set<String> getAllowedMessageFunctions() {
		Set<String> allowedMessageFunctions = new HashSet<String>();
		allowedMessageFunctions.add(TypeAConstants.MessageFunction.INVENTORY_ADJUSTMENT);

		return allowedMessageFunctions;
	}

	private List<List<FlightInformation>> filterFlightInformation(ITAREQMessage inventoryAdjustment, String actionCode) {
		List<List<FlightInformation>> onds = new ArrayList<List<FlightInformation>>();
		List<FlightInformation> ond;
		Set<String> navigatedAirports;
		boolean returnStarted = false;
		String prevSegmentBookingClass;

		for (OriginDestinationInformation originDestination : inventoryAdjustment.getOriginDestinationInfo()) {
			ond = new ArrayList<FlightInformation>();
			navigatedAirports = new HashSet<String>();
			for (FlightInformation flightInformation : originDestination.getFlightInfomation()) {
				if (flightInformation.getActionCode().equals(actionCode)) {
					if (navigatedAirports.isEmpty()) {
						navigatedAirports.add(flightInformation.getDepartureLocation().getLocationCode());
					}

					if (!navigatedAirports.contains(flightInformation.getArrivalLocation().getLocationCode())) {
						navigatedAirports.add(flightInformation.getArrivalLocation().getLocationCode());
					} else {
						if (!returnStarted) {
							onds.add(ond);
							ond = new ArrayList<FlightInformation>();
							returnStarted = true;
						}
					}

					ond.add(flightInformation);

				}
			}
			onds.add(ond);
		}

		return onds;
	}


	private void onInventoryUnavailable(FlightSegmentTO flightSegmentTO, String bookingClass, String gdsCarrierCode) {
		FCCSegInventory fccSegInventory;

		List<GdsPublishFccSegBcInventoryTo> gdsPublishFccSegBcInventoryTos = new ArrayList<>();
		GdsPublishFccSegBcInventoryTo gdsPublishFccSegBcInventoryTo;

		Integer gdsId = null;
		for (GDSStatusTO gds : GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap().values()) {
			if (gds.getCarrierCode() != null && gds.getCarrierCode().equals(gdsCarrierCode)) {
				gdsId = gds.getGdsId();
			}
		}

		try {
			BookingClass bc = GDSServicesModuleUtil.getBookingClassBD().getBookingClass(bookingClass);

			if (gdsId == null)
				return; // todo test this

			fccSegInventory = GDSServicesModuleUtil.getFlightInventoryBD().getFCCSegInventory(flightSegmentTO.getFlightSegId(), bc.getLogicalCCCode());

			if (fccSegInventory != null) {

				gdsPublishFccSegBcInventoryTo = new GdsPublishFccSegBcInventoryTo();

				gdsPublishFccSegBcInventoryTo.setFccsInvId(fccSegInventory.getFccsInvId());
				gdsPublishFccSegBcInventoryTo.setBookingClass(bookingClass);
				gdsPublishFccSegBcInventoryTo.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT.getCode());
				gdsPublishFccSegBcInventoryTo.setEventCode(AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_CLOSED.getCode());
				gdsPublishFccSegBcInventoryTo.setGdsId(gdsId);

				gdsPublishFccSegBcInventoryTos.add(gdsPublishFccSegBcInventoryTo);
			}

			if (!gdsPublishFccSegBcInventoryTos.isEmpty()) {
				GDSServicesModuleUtil.getFlightInventoryBD().publishGdsFccSegBcInventory(gdsPublishFccSegBcInventoryTos);
			}
		} catch (ModuleException e) {
			// do nothing
		}
	}

	private void onInventoryUnavailable(List<List<FlightInformation>> onds, String gdsCarrierCode) {
		List<FlightInformation> ond;
		FlightSegmentTO flightSegmentTO;
		String gdsBookingClass;
		String bookingClass;
		FCCSegBCInventory fccSegBCInventory;
		FCCSegInventory fccSegInventory;
		boolean isInvUnavailable;

		List<GdsPublishFccSegBcInventoryTo> gdsPublishFccSegBcInventoryTos = new ArrayList<>();
		GdsPublishFccSegBcInventoryTo gdsPublishFccSegBcInventoryTo;

		Integer gdsId = null;
		for (GDSStatusTO gds : GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap().values()) {
			if (gds.getCarrierCode() != null && gds.getCarrierCode().equals(gdsCarrierCode)) {
				gdsId = gds.getGdsId();
			}
		}

		if (gdsId == null) return; // todo test this

		for (List<FlightInformation> ondFlightInfo : onds) {
			ond = new ArrayList<FlightInformation>();
			if (!ondFlightInfo.isEmpty()) {
				for (FlightInformation flightInfo : ondFlightInfo) {
					isInvUnavailable = false;
					flightSegmentTO = null;
					try {
						flightSegmentTO = CommonUtil.getBestMatchedFlightSegmentTo(flightInfo);
					} catch (ModuleException e) {
						// do nothing
					}
					if (flightSegmentTO != null) {
						gdsBookingClass = flightInfo.getFlightPreference().getBookingClass();
						bookingClass = GDSServicesUtil.getBookingCode(gdsCarrierCode, gdsBookingClass);

						fccSegBCInventory = GDSServicesModuleUtil.getFlightInventoryBD().getFCCSegBCInventory(flightSegmentTO.getFlightSegId(),
								bookingClass);

						if (fccSegBCInventory.getStatus().equals(FCCSegBCInventory.Status.CLOSED) || fccSegBCInventory.getSeatsAvailable() <= 0) {
							isInvUnavailable = true;
						} else {
							fccSegInventory = GDSServicesModuleUtil.getFlightInventoryBD().getFCCSegInventory(flightSegmentTO.getFlightSegId(), fccSegBCInventory.getLogicalCCCode());

							if (fccSegInventory.getAvailableSeats() <= 0) {
								isInvUnavailable = true;
							}
						}


						if (isInvUnavailable) {
							gdsPublishFccSegBcInventoryTo = new GdsPublishFccSegBcInventoryTo();

							gdsPublishFccSegBcInventoryTo.setFccsbInvId(fccSegBCInventory.getFccsbInvId());
							gdsPublishFccSegBcInventoryTo.setFccsInvId(fccSegBCInventory.getfccsInvId());
							gdsPublishFccSegBcInventoryTo.setBookingClass(fccSegBCInventory.getBookingCode());
							gdsPublishFccSegBcInventoryTo.setEventStatus(AirinventoryCustomConstants.PublishInventoryEventStatus.NEW_EVENT.getCode());
							gdsPublishFccSegBcInventoryTo.setEventCode(AirinventoryCustomConstants.PublishInventoryEventCode.AVS_RBD_CLOSED.getCode());
							gdsPublishFccSegBcInventoryTo.setGdsId(gdsId);

							gdsPublishFccSegBcInventoryTos.add(gdsPublishFccSegBcInventoryTo);
						}

					}
				}
			}
		}

		if (!gdsPublishFccSegBcInventoryTos.isEmpty()) {
			GDSServicesModuleUtil.getFlightInventoryBD().publishGdsFccSegBcInventory(gdsPublishFccSegBcInventoryTos);
		}
	}

}
