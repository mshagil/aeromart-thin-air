package com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03;

import iata.typea.v031.itares.ERC;
import iata.typea.v031.itares.GROUP1;
import iata.typea.v031.itares.GROUP2;
import iata.typea.v031.itares.GROUP3;
import iata.typea.v031.itares.GROUP4;
import iata.typea.v031.itares.GROUP5;
import iata.typea.v031.itares.IFT;
import iata.typea.v031.itares.ITARES;
import iata.typea.v031.itares.ODI;
import iata.typea.v031.itares.ObjectFactory;
import iata.typea.v031.itares.RPI;
import iata.typea.v031.itares.TVL;
import iata.typea.v031.itares.Type0051;
import iata.typea.v031.itares.Type0065;
import iata.typea.v031.itares.UNB;
import iata.typea.v031.itares.UNH;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill;

public class ItaresFactory {

	private static final ItaresFactory factory = new ItaresFactory();
	private static final ObjectFactory objectFactory = new ObjectFactory();

	private ItaresFactory() {
		super();
	}

	public static ItaresFactory getInstance() {
		return factory;
	}

	public ObjectFactory getObjectfactory() {
		return objectFactory;
	}

	public ITARES createITARES() {
		ITARES itares = objectFactory.createITARES();
		itares.setUNH(createUNH());
		itares.setUNT(objectFactory.createUNT());
		return itares;
	}

	public UNB createUNB() {
		UNB unb = objectFactory.createUNB();
		UNB.UNB01 identifier = objectFactory.createUNBUNB01();
		identifier.setUNB0101(Constants.Common.syntaxIdentifier);
		identifier.setUNB0102(Constants.Common.syntaxVersionNumber);
		unb.setUNB01(identifier);

		UNB.UNB02 sender = objectFactory.createUNBUNB02();
		sender.setUNB0201(Constants.Common.senderIdentification);
		unb.setUNB02(sender);

		UNB.UNB03 recipient = objectFactory.createUNBUNB03();
		recipient.setUNB0301(Constants.Common.recipientIdentification);
		unb.setUNB03(recipient);

		// This date and time need to be given in GMT
		UNB.UNB04 dateTimeOfPreparation = objectFactory.createUNBUNB04();
		dateTimeOfPreparation.setUNB0401(BigDecimal.valueOf(new Date().getTime()).toString());
		dateTimeOfPreparation.setUNB0402(BigDecimal.valueOf((new Date().getTime())).toString());
		unb.setUNB04(dateTimeOfPreparation);

		unb.setUNB05("841F60");
		return unb;
	}

	public UNH createUNH() {
		UNH unh = objectFactory.createUNH();
		unh.setUNH01(String.valueOf(1));
		UNH.UNH02 identifier = objectFactory.createUNHUNH02();
		identifier.setUNH0201(Type0065.ITAREQ);
		identifier.setUNH0202(Constants.Common.messageTypeVersionNumber);
		identifier.setUNH0203(Constants.Common.messageTypeReleaseNumber);
		identifier.setUNH0204(Type0051.IA);
		unh.setUNH02(identifier);
		return unh;
	}

	public ITARES addERCIFT(ITARES itares, String ercCode, String iftmsg) {
		GROUP1 group1 = objectFactory.createGROUP1();

		if (ercCode != null) {
			ERC erc = objectFactory.createERC();
			ERC.ERC01 applicationErrorDetail = objectFactory.createERCERC01();
			applicationErrorDetail.setERC0101(ercCode);
			erc.setERC01(applicationErrorDetail);
			group1.setERC(erc);
		}

		if (iftmsg != null) {
			IFT ift = objectFactory.createIFT();
			IFT.IFT01 freeTextQualification = objectFactory.createIFTIFT01();
			freeTextQualification.setIFT0101("3");
			ift.setIFT01(freeTextQualification);
			ift.getIFT02().add(iftmsg);
			group1.setIFT(ift);
		}

		itares.getGROUP1().add(group1);
		return itares;
	}

	public GROUP2 addERCIFT(GROUP2 group2, String ercCode, String iftmsg) {
		GROUP3 group3 = objectFactory.createGROUP3();

		if (ercCode != null) {
			ERC erc = objectFactory.createERC();
			ERC.ERC01 applicationErrorDetail = objectFactory.createERCERC01();
			applicationErrorDetail.setERC0101(ercCode);
			erc.setERC01(applicationErrorDetail);
			group3.setERC(erc);
		}

		if (iftmsg != null) {
			IFT ift = objectFactory.createIFT();
			IFT.IFT01 freeTextQualification = objectFactory.createIFTIFT01();
			freeTextQualification.setIFT0101("3");
			ift.setIFT01(freeTextQualification);
			ift.getIFT02().add(iftmsg);
			group3.setIFT(ift);
		}

		group2.getGROUP3().add(group3);
		return group2;
	}

	public GROUP4 addERCIFT(GROUP4 group4, String ercCode, String iftmsg) {
		GROUP5 group5 = objectFactory.createGROUP5();

		if (ercCode != null) {
			ERC erc = objectFactory.createERC();
			ERC.ERC01 applicationErrorDetail = objectFactory.createERCERC01();
			applicationErrorDetail.setERC0101(ercCode);
			erc.setERC01(applicationErrorDetail);
			group5.setERC(erc);
		}

		if (iftmsg != null) {
			IFT ift = objectFactory.createIFT();
			IFT.IFT01 freeTextQualification = objectFactory.createIFTIFT01();
			freeTextQualification.setIFT0101("3");
			ift.setIFT01(freeTextQualification);
			ift.getIFT02().add(iftmsg);
			group5.setIFT(ift);
		}

		group4.getGROUP5().add(group5);
		return group4;
	}

	public ODI createODI(String... locations) {
		ODI odi = objectFactory.createODI();
		for (String pleace : locations) {
			odi.getODI01().add(pleace);
		}
		return odi;
	}

	public TVL createTVL(Date departureDate, Date arrivalDate, String from, String to, String carrier, String productId) {
		TVL tvl = objectFactory.createTVL();
		TVL.TVL01 dateTime = objectFactory.createTVLTVL01();
		dateTime.setTVL0101(TypeADataConvertorUtill.getDateString(departureDate));
		dateTime.setTVL0102(BigDecimal.valueOf(departureDate.getTime()));
		dateTime.setTVL0104(BigDecimal.valueOf(arrivalDate.getTime()));
		// both dates are in the same day
		if (CalendarUtil.isSameDay(departureDate, arrivalDate)) {
			dateTime.setTVL0103("");
		} else {
			dateTime.setTVL0103(TypeADataConvertorUtill.getDateString(arrivalDate));
		}
		tvl.setTVL01(dateTime);

		TVL.TVL02 locationFrom = objectFactory.createTVLTVL02();
		locationFrom.setTVL0201(from);
		TVL.TVL03 locationTo = objectFactory.createTVLTVL03();
		locationTo.setTVL0301(to);
		tvl.setTVL02(locationFrom);
		tvl.setTVL03(locationTo);

		TVL.TVL04 companyIdentification = objectFactory.createTVLTVL04();
		companyIdentification.setTVL0401(carrier);
		tvl.setTVL04(companyIdentification);

		TVL.TVL05 pDetails = objectFactory.createTVLTVL05();
		pDetails.setTVL0501(productId);
		tvl.setTVL05(pDetails);

		return tvl;
	}

	public RPI createRPI(int paxCount, String... statusCodes) {
		RPI rpi = objectFactory.createRPI();
		rpi.setRPI01(BigDecimal.valueOf(paxCount));
		rpi.getRPI02().addAll(Arrays.asList(statusCodes));
		return rpi;
	}

}
