package com.isa.thinair.gdsservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConfigurationException;

import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.TypeAConfig;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.DateUtil;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GdsTypeACodes;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerInformationWrapper;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketControlInformationWrapper;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.msgbroker.api.dto.BookingSegmentDTO;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.Header;
import com.isa.typea.common.InteractiveFreeText;
import com.isa.typea.common.InterchangeHeader;
import com.isa.typea.common.InterchangeInfo;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.common.MessageIdentifier;
import com.isa.typea.common.MonetaryInformation;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.TaxDetails;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.Traveller;
import com.isa.typea.common.TravellerInformation;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.common.TravellerTicketingInformation;

public abstract class GDSDTOUtil {
	private static Log log = LogFactory.getLog(GDSDTOUtil.class);

	public static Header createRespEdiHeader(Header reqEdiHeader, MessageHeader respMessageHeader, EDIMessageDTO messageDTO)
			 {

		String interchangeControlRef;

		InterchangeHeader reqInterchangeHeader = reqEdiHeader.getInterchangeHeader();
		Header respHeader = new Header();
		InterchangeHeader respInterchangeHeader = new InterchangeHeader();
		respInterchangeHeader.setSyntaxId(reqInterchangeHeader.getSyntaxId());
		respInterchangeHeader.setVersionNo(reqInterchangeHeader.getVersionNo());
		respInterchangeHeader.setReceiverInfo(reqInterchangeHeader.getSenderInfo());
		respInterchangeHeader.setSenderInfo(reqInterchangeHeader.getReceiverInfo());

		try {
			respInterchangeHeader.setTimestamp(DateUtil.parse(new Date()));
		} catch (DatatypeConfigurationException e) {
			log.error("Date Conversion Error", e);
		}

		if (reqEdiHeader.getInterchangeHeader().getRecipientRef() != null &&
				!reqEdiHeader.getInterchangeHeader().getRecipientRef().isEmpty()) {
			interchangeControlRef = KeyGenerator.getNextInterchangeControlRef(reqEdiHeader.getInterchangeHeader().getRecipientRef());
		} else {
			interchangeControlRef = KeyGenerator.getInterchangeControlRef(respMessageHeader.getCommonAccessReference(), respMessageHeader.getReferenceNo());
		}

		respInterchangeHeader.setInterchangeControlRef(interchangeControlRef);
		respInterchangeHeader.setRecipientRef(reqInterchangeHeader.getInterchangeControlRef());
		respInterchangeHeader.setAssociationCode(messageDTO.getRespAssociationCode());

		respHeader.setInterchangeHeader(respInterchangeHeader);
		return respHeader;
	}

	public static MessageHeader createRespMessageHeader(MessageHeader reqMessageHeader, String commonAccessRef,
	                                                    IATAOperation respType) {

		MessageHeader respMessageHeader = new MessageHeader();

		MessageIdentifier reqMsgId = reqMessageHeader.getMessageId();
		MessageIdentifier msgId = new MessageIdentifier();
		msgId.setControllingAgency(reqMsgId.getControllingAgency());
		msgId.setVersion(reqMsgId.getVersion());
		msgId.setReleaseNumber(reqMsgId.getReleaseNumber());
		msgId.setMessageType(respType.name());
		respMessageHeader.setMessageId(msgId);
		respMessageHeader.setReferenceNo(reqMessageHeader.getReferenceNo());
		respMessageHeader.setCommonAccessReference(commonAccessRef);

		return respMessageHeader;
	}

	public static OriginatorInformation createOriginatorInformation() {
		OriginatorInformation originatorInformation = new OriginatorInformation();

		return originatorInformation;
	}

	public static LCCClientReservationPax resolveReservationPax(TravellerInformation travellerInfo, Traveller traveller, LCCClientReservation reservation) {
		LCCClientReservationPax reservationPax = null;
		for (LCCClientReservationPax passenger : reservation.getPassengers()) {
			if (areEqualDespiteWhiteSpaces(traveller.getGivenName(), passenger.getFirstName()) &&
					areEqualDespiteWhiteSpaces(passenger.getLastName(),travellerInfo.getTravellerSurname())) {
				reservationPax = passenger;
				break;
			}
		}

		return reservationPax;
	}
	public static <T extends TravellerInformationWrapper> T resolveTraveller(TravellerInformation travellerInfo, Traveller traveller, GdsReservation<T> reservation) {
		TravellerInformation tvl;
		T val = null;
		for (T t : reservation.getTravellerInformation()) {
			tvl = t.getTravellerInformation();
			if (areEqualDespiteWhiteSpaces(tvl.getTravellers().get(0).getGivenName(), traveller.getGivenName()) &&
					areEqualDespiteWhiteSpaces(tvl.getTravellerSurname(), travellerInfo.getTravellerSurname())) {
				val = t;
				break;
			}
		}

		return val;
	}

	public static TicketCoupon resolveTicketCoupon(List<TicketNumberDetails> tickets, LCCClientReservationSegment segment, String gdsCarrierCode) {
		TicketCoupon ticketCoupon = null;
		FlightInformation flightInfo;
		String flightNumber;
		String bookingClass;
		String gdsBookingClass;

		bookingClass = segment.getFareTO().getBookingClassCode();
		gdsBookingClass = GDSServicesUtil.getGdsBookingCode(gdsCarrierCode, bookingClass);


		out:
		for (TicketNumberDetails ticket : tickets) {
			for (TicketCoupon coupon : ticket.getTicketCoupon()) {
				flightInfo = coupon.getFlightInfomation();
				flightNumber = flightInfo.getCompanyInfo().getMarketingAirlineCode() + flightInfo.getFlightNumber();
				if (flightNumber != null && flightNumber.equals(segment.getFlightNo()) &&
						flightInfo.getDepartureDateTime().toGregorianCalendar().getTime().equals(segment.getDepartureDate()) &&
						flightInfo.getDepartureLocation().getLocationCode().equals(GdsUtil.getDepartingAirport(segment.getSegmentCode())) &&
						flightInfo.getArrivalLocation().getLocationCode().equals(GdsUtil.getArrivalAirport(segment.getSegmentCode())) &&
						flightInfo.getFlightPreference().getBookingClass().equals(gdsBookingClass)) {
					ticketCoupon = coupon;
					break out;
				}
			}
		}

		return ticketCoupon;
	}

	public static LCCClientReservationSegment resolveReservationSegment(Collection<LCCClientReservationSegment> segments, TicketCoupon ticketCoupon, String gdsCarrierCode) {
		LCCClientReservationSegment segment = null;
		FlightInformation flightInfo;
		String flightNumber;
		String bookingClass;

		flightInfo = ticketCoupon.getFlightInfomation();
		flightNumber = flightInfo.getCompanyInfo().getMarketingAirlineCode() + flightInfo.getFlightNumber();

		bookingClass = GDSServicesUtil.getBookingCode(gdsCarrierCode, flightInfo.getFlightPreference().getBookingClass());

		for (LCCClientReservationSegment seg : segments) {
			if ((!seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) &&
					flightNumber != null && flightNumber.equals(seg.getFlightNo()) &&
					flightInfo.getDepartureDateTime().toGregorianCalendar().getTime().equals(seg.getDepartureDate()) &&
					flightInfo.getDepartureLocation().getLocationCode().equals(GdsUtil.getDepartingAirport(seg.getSegmentCode())) &&
					flightInfo.getArrivalLocation().getLocationCode().equals(GdsUtil.getArrivalAirport(seg.getSegmentCode())) &&
					bookingClass.equals(seg.getFareTO().getBookingClassCode())) {
				segment = seg;
				break;
			}
		}

		return segment;
	}

	public static boolean containsTicketCoupon(List<TicketNumberDetails> tickets, LccClientPassengerEticketInfoTO lccCoupon) {
		TicketCoupon ticketCoupon = null;
		FlightInformation flightInfo;
		String flightNumber;
		out:
		for (TicketNumberDetails ticket : tickets) {
			for (TicketCoupon coupon : ticket.getTicketCoupon()) {
				flightInfo = coupon.getFlightInfomation();
				flightNumber = flightInfo.getCompanyInfo().getMarketingAirlineCode() + flightInfo.getFlightNumber();
				if ( (flightNumber != null && flightNumber.equals(lccCoupon.getFlightNo()) &&
						flightInfo.getDepartureDateTime().toGregorianCalendar().getTime().equals(lccCoupon.getDepartureDate()) &&
						flightInfo.getDepartureLocation().getLocationCode().equals(GdsUtil.getDepartingAirport(lccCoupon.getSegmentCode())) &&
						flightInfo.getArrivalLocation().getLocationCode().equals(GdsUtil.getArrivalAirport(lccCoupon.getSegmentCode())))) {
					ticketCoupon = coupon;
					break out;
				}
			}
		}

		return (ticketCoupon != null) ;
	}

	public static Header createInitEdiHeader(int gdsId, MessageHeader initHeader, Map<String, Object> msgSession)
			throws ModuleException {

		InterchangeInfo sender;
		InterchangeInfo receiver;
		Gds gds;
		ServiceClient serviceClient;

		gds = GdsUtil.getGds(gdsId);
		serviceClient = GdsUtil.getGdsServiceClient(gdsId);

		Header header = new Header();
		InterchangeHeader interchangeHeader = new InterchangeHeader();
		interchangeHeader.setSyntaxId(gds.getTypeASyntaxId());
		interchangeHeader.setVersionNo(gds.getTypeASyntaxVersionNumber());

		sender = new InterchangeInfo();
		sender.setInterchangeId(gds.getTypeASenderId());
		sender.setInternalIdentificationCode(gds.getTypeASenderSubId());
		interchangeHeader.setSenderInfo(sender);

		receiver = new InterchangeInfo();
		receiver.setInterchangeId(serviceClient.getTypeAAddress());
		receiver.setInternalIdentificationCode(serviceClient.getTypeASubAddress());
		interchangeHeader.setReceiverInfo(receiver);

		try {
			interchangeHeader.setTimestamp(DateUtil.parse(new Date()));
		} catch (DatatypeConfigurationException e) {
			throw new ModuleException(e, "Error while parsing Date");
		}

		interchangeHeader.setInterchangeControlRef(KeyGenerator.getInterchangeControlRef(initHeader.getCommonAccessReference(),
				initHeader.getReferenceNo()));

//		TODO -- need to imple multiple messages in single session
//		if (msgSession != null) {
//
//		} else {
			interchangeHeader.setAssociationCode(TypeAConstants.AssociationCode.ONE_OFF);
//		}

		header.setInterchangeHeader(interchangeHeader);

		return header;
	}

	public static MessageHeader createInitMessageHeader(IATAOperation iataOperation, int gdsId, Map<String, Object> msgSession)
		throws ModuleException {

		Gds gds = GdsUtil.getGds(gdsId);
		MessageIdentifier messageIdentifier;

		MessageHeader messageHeader = new MessageHeader();

		messageIdentifier = new MessageIdentifier();

		TypeAConfig config = GDSServicesModuleUtil.getConfig().getTypeAConfig();
		Map<GDSExternalCodes.GDS, Map<IATAOperation, String>> versions = config.getVersions();

		messageIdentifier.setControllingAgency(TypeAConstants.TYPE_A_CONTROLLING_AGENCY);
		String[] msgVersion;

		if (versions.containsKey(GDSExternalCodes.GDS.valueOf(gds.getGdsCode())) &&
				versions.get(GDSExternalCodes.GDS.valueOf(gds.getGdsCode())).containsKey(iataOperation)) {
			msgVersion = versions.get(GDSExternalCodes.GDS.valueOf(gds.getGdsCode())).get(iataOperation)
					.split(Pattern.quote(TypeAConstants.DELIM_VERSION_NUMBERS));
		} else {
			msgVersion = gds.getTypeAVersion().split(Pattern.quote(TypeAConstants.DELIM_VERSION_NUMBERS));
		}

		messageIdentifier.setVersion(msgVersion[0]);
		messageIdentifier.setReleaseNumber(msgVersion[1]);

		messageIdentifier.setMessageType(iataOperation.name());
		messageHeader.setMessageId(messageIdentifier);

//		TODO -- need to imple multiple messages in single session
//		if (msgSession != null) {
			messageHeader.setCommonAccessReference(KeyGenerator.generateRespCommonAccessRefKey());
			messageHeader.setReferenceNo("1");
//		} else {
//		First Message of conversation
//		}

		return messageHeader;
	}

	public static List<TicketNumberDetails> filterTicketNumbers(List<TicketNumberDetails> allTickets,
	                                                               GdsTypeACodes.TicketDataIndicator ticketDataIndicator) {
		List<TicketNumberDetails> filteredTicketNumbers = new ArrayList<TicketNumberDetails>();
		String requiredDataIndicator = null;

		switch (ticketDataIndicator) {
		case OLD:
			requiredDataIndicator = TypeAConstants.TicketDataIndicator.OLD;
			break;
		case NEW:
			requiredDataIndicator = TypeAConstants.TicketDataIndicator.NEW;
			break;
		}

		for (TicketNumberDetails ticket : allTickets) {
			if (ticket.getDataIndicator().equals(requiredDataIndicator)) {
				filteredTicketNumbers.add(ticket);
			}
		}

		return filteredTicketNumbers;
	}

	public static String getFareText(Collection<InteractiveFreeText> freeTexts) {
		StringBuilder sb = new StringBuilder();

		for (InteractiveFreeText freeText : freeTexts) {
			if (freeText.getInformationType().equals(TypeAConstants.InformationType.FARE_CALCULATION)) {
				for (String text : freeText.getFreeText()) {
					sb.append(text);
				}
			}
		}

		return sb.toString();
	}


	public static TravellerTicketingInformationWrapper resolvePassengerByTicketNumber(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                                                  String ticketNumber) {
		TravellerTicketingInformationWrapper travellerWrapper = null;

		l0:
		for (TravellerTicketingInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					travellerWrapper = traveller;
					break l0;
				}
			}
		}

		return travellerWrapper;
	}

	public static TravellerTicketControlInformationWrapper resolveTicketControlPassengerByTicketNumber(GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation,
	                                                                                  String ticketNumber) {
		TravellerTicketControlInformationWrapper travellerWrapper = null;

		l0:
		for (TravellerTicketControlInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					travellerWrapper = traveller;
					break l0;
				}
			}
		}

		return travellerWrapper;
	}

	public static TravellerTicketingInformationWrapper resolvePassengerByTraveller(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                                                  TravellerTicketingInformation travellers) {
		TravellerTicketingInformationWrapper travellerWrapper = null;
		TravellerTicketingInformation travellerGroupImage;
		Traveller travellerImage;
		Traveller traveller = travellers.getTravellers().get(0);

		l1:
		for (TravellerTicketingInformationWrapper travellerGroupWrapper : gdsReservation.getTravellerInformation()) {
			travellerGroupImage = travellerGroupWrapper.getTravellerInformation();
			travellerImage = travellerGroupImage.getTravellers().get(0);

			if (areEqualDespiteWhiteSpaces(travellerGroupImage.getTravellerSurname(), travellers.getTravellerSurname()) &&
					(traveller.getGivenName() != null && travellerImage.getGivenName() != null &&
							areEqualDespiteWhiteSpaces(traveller.getGivenName(), travellerImage.getGivenName()))) {

				l2:
				for (TicketNumberDetails ticket : travellers.getTickets()) {
					for (TicketNumberDetails ticketImage : travellerGroupImage.getTickets()) {
						if (ticket.getTicketNumber().equals(ticketImage.getTicketNumber())) {
							break l2;
						}
					}

					continue l1;
				}

				travellerWrapper = travellerGroupWrapper;
				break;
			}
		}

		return travellerWrapper;
	}

	@Deprecated
	public static TravellerTicketingInformationWrapper resolvePassenger(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                                               LCCClientReservationPax pax) {
		TravellerTicketingInformationWrapper travellerWrapper = null;
		TravellerTicketingInformation travellerGroupImage;
		Traveller travellerImage;

		for (TravellerTicketingInformationWrapper travellerGroupWrapper : gdsReservation.getTravellerInformation()) {
			travellerGroupImage = travellerGroupWrapper.getTravellerInformation();
			travellerImage = travellerGroupImage.getTravellers().get(0);

			if (areEqualDespiteWhiteSpaces(pax.getLastName(), travellerGroupImage.getTravellerSurname()) &&
					(pax.getFirstName() != null && travellerImage.getGivenName() != null &&
							areEqualDespiteWhiteSpaces(pax.getFirstName(), travellerImage.getGivenName()))) {
				travellerWrapper = travellerGroupWrapper;
				break;
			}
		}

		return travellerWrapper;
	}


	public static List<TravellerTicketingInformationWrapper> resolvePassengers(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                                    LCCClientReservationPax pax) {
		List<TravellerTicketingInformationWrapper> applicableTravellerImages = new ArrayList<TravellerTicketingInformationWrapper>();
		TravellerTicketingInformation travellerGroupImage;
		Traveller travellerImage;

		for (TravellerTicketingInformationWrapper travellerGroupWrapper : gdsReservation.getTravellerInformation()) {
			travellerGroupImage = travellerGroupWrapper.getTravellerInformation();
			travellerImage = travellerGroupImage.getTravellers().get(0);

			if (areEqualDespiteWhiteSpaces(pax.getLastName(), travellerGroupImage.getTravellerSurname()) &&
					(pax.getFirstName() != null && travellerImage.getGivenName() != null &&
							areEqualDespiteWhiteSpaces(pax.getFirstName(), travellerImage.getGivenName()))) {
				applicableTravellerImages.add(travellerGroupWrapper);
			}
		}

		return applicableTravellerImages;
	}

	public static TicketNumberDetails resolveTicket(TravellerTicketingInformation traveller, String ticketNumber) {
		TicketNumberDetails ticketNumberDetails = null;

		for (TicketNumberDetails ticket : traveller.getTickets()) {
			if(ticketNumber.equals(ticket.getTicketNumber())) {
				ticketNumberDetails = ticket;
				break;
			}
		}

		return ticketNumberDetails;
	}

	public static TicketNumberDetails resolveTicket(TravellerTicketControlInformation traveller, String ticketNumber) {
		TicketNumberDetails ticketNumberDetails = null;

		for (TicketNumberDetails ticket : traveller.getTickets()) {
			if(ticketNumber.equals(ticket.getTicketNumber())) {
				ticketNumberDetails = ticket;
				break;
			}
		}

		return ticketNumberDetails;
	}

	public static TicketNumberDetails resolveTicket(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                                                  String ticketNumber) {
		TicketNumberDetails ticketNumberDetails = null;

		l0:
		for (TravellerTicketingInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					ticketNumberDetails = ticket;
					break l0;
				}
			}
		}

		return ticketNumberDetails;
	}

	public static TicketCoupon resolveCoupon(TicketNumberDetails ticket, String couponNumber) {
		TicketCoupon ticketCoupon = null;

		for (TicketCoupon coupon : ticket.getTicketCoupon()) {
			if (couponNumber.equals(coupon.getCouponNumber())) {
				ticketCoupon = coupon;
				break;
			}
		}

		return ticketCoupon;
	}

	public static TicketCoupon resolveCoupon(List<? extends TicketNumberDetails> tickets, String ticketNumber, Integer couponNumber) {
		TicketCoupon ticketCoupon = null;

		for (TicketNumberDetails ticket : tickets) {
			if(ticketNumber.equals(ticket.getTicketNumber())) {
				ticketCoupon = resolveCoupon(ticket, String.valueOf(couponNumber));
				break;
			}
		}


		return ticketCoupon;
	}

	public static void mergeCouponTransitions(List<TicketNumberDetails> couponTransitions,
	                                          List<TicketNumberDetails> newTransitions, OriginatorInformation originator) {

		TicketNumberDetails applicableTicket;
		TicketCoupon transition;

		for (TicketNumberDetails mergingTicket : newTransitions) {
			applicableTicket = null;
			for (TicketNumberDetails couponTransition : couponTransitions) {
				if (mergingTicket.getTicketNumber().equals(couponTransition.getTicketNumber())) {
					applicableTicket = couponTransition;
				}
			}

			if (applicableTicket == null) {
			 	applicableTicket = new TicketNumberDetails();
				applicableTicket.setTicketNumber(mergingTicket.getTicketNumber());
				applicableTicket.setDocumentType(mergingTicket.getDocumentType());
				couponTransitions.add(applicableTicket);
			}

			for (TicketCoupon coupon : mergingTicket.getTicketCoupon()) {
				transition = new TicketCoupon();
				transition.setCouponNumber(coupon.getCouponNumber());
				transition.setStatus(coupon.getStatus());
				transition.setSettlementAuthCode(coupon.getSettlementAuthCode());

				transition.setFlightInfomation(coupon.getFlightInfomation());
				transition.setDateAndTimeInformation(coupon.getDateAndTimeInformation());
				transition.setOriginatorInformation(originator);

				applicableTicket.getTicketCoupon().add(transition);
			}
		}
	}

	public static List<BookingSegmentDTO> getNewSegments(List<BookingSegmentDTO> allSegments) {
		List<BookingSegmentDTO> newResSegments = new ArrayList<BookingSegmentDTO>();
		GDSInternalCodes.ActionCode actionCode;

		for (BookingSegmentDTO segment : allSegments) {
			actionCode = getActionCode(segment.getActionOrStatusCode());
			if (GDSInternalCodes.ActionCode.ONHOLD_REPLY_REQUIRED == actionCode || GDSInternalCodes.ActionCode.ONHOLD == actionCode) {
				newResSegments.add(segment);
			}
		}

		return newResSegments;
	}


	public static GDSInternalCodes.ActionCode getActionCode(String actionOrStatusCode) {
		GDSInternalCodes.ActionCode actionCode = null;

		if (GDSExternalCodes.ActionCode.NEED.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.NEED_ALTERNATE.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode().equals(actionOrStatusCode)) {
			actionCode = GDSInternalCodes.ActionCode.ONHOLD_REPLY_REQUIRED;
		} else if (GDSExternalCodes.ActionCode.SOLD.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.SOLD_FREE.getCode().equals(actionOrStatusCode)
				|| GDSExternalCodes.ActionCode.SOLD_IFNOT_HOLDING.getCode().equals(actionOrStatusCode)) {
			actionCode = GDSInternalCodes.ActionCode.ONHOLD;
		}

		return actionCode;
	}

	public static List<MonetaryInformation> generateMonetoryInfo(List<MonetaryInformation> oldMonetaryInfo,
	                                                             List<MonetaryInformation> newMonetaryInfo,
	                                                             List<TaxDetails> taxDetails) throws GdsTypeAException {
		List<MonetaryInformation> monInfoList = new ArrayList<MonetaryInformation>();
		Map<String, MonetaryInformation> monetaryInfo = new HashMap<String, MonetaryInformation>();
		MonetaryInformation mon;
		BigDecimal baseAmount = null;
		BigDecimal equivalantAmount = null;
		BigDecimal totalAmount = null;
		BigDecimal additionalPaymentAmount = null;
		String additionalPaymentCurrency = null;
		BigDecimal taxTotal;
		BigDecimal oldTicketTotal;
		String oldTicketCurrency;

		for (MonetaryInformation monetaryInformation : newMonetaryInfo) {
			monetaryInfo.put(monetaryInformation.getAmountTypeQualifier(), monetaryInformation);

			if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.BASE_FARE)) {
				baseAmount = new BigDecimal(monetaryInformation.getAmount());
			} else if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.EQUIVALENT_FARE)) {
				equivalantAmount = new BigDecimal(monetaryInformation.getAmount());
			} else if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {

				if (monetaryInformation.getAmount().endsWith(TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX)) {
					additionalPaymentAmount = new BigDecimal(monetaryInformation.getAmount()
							.substring(0, monetaryInformation.getAmount().length() - TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX.length()));
					additionalPaymentCurrency =  monetaryInformation.getCurrencyCode();
				} else if (monetaryInformation.getAmount().equals(TypeAConstants.TypeAKeywords.NO_ADDITIONAL_COLLECTION)) {
					additionalPaymentAmount = null;
				} else {
					totalAmount = new BigDecimal(monetaryInformation.getAmount());
				}
			} else if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.ADDITIONAL_COLLECTION_TOTAL)) {
				additionalPaymentAmount = new BigDecimal(monetaryInformation.getAmount());
				additionalPaymentCurrency =  monetaryInformation.getCurrencyCode();
			}
		}


		if (monetaryInfo.containsKey(TypeAConstants.MonetaryType.BASE_FARE)) {
			monInfoList.add(monetaryInfo.get(TypeAConstants.MonetaryType.BASE_FARE));
		}

		if (monetaryInfo.containsKey(TypeAConstants.MonetaryType.EQUIVALENT_FARE)) {
			monInfoList.add(monetaryInfo.get(TypeAConstants.MonetaryType.EQUIVALENT_FARE));
		}

		if (totalAmount != null) {
			monInfoList.add(monetaryInfo.get(TypeAConstants.MonetaryType.TICKET_TOTAL));
		} else {

			oldTicketTotal = null;
			oldTicketCurrency = null;
			for (MonetaryInformation monetaryInformation : oldMonetaryInfo) {
				if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {
					oldTicketTotal = new BigDecimal(monetaryInformation.getAmount());
					oldTicketCurrency = monetaryInformation.getCurrencyCode();
				}
			}

			if (oldTicketTotal == null) {
				throw new GdsTypeAException();
			}

			mon = new MonetaryInformation();
			monInfoList.add(mon);

			mon.setAmountTypeQualifier(TypeAConstants.MonetaryType.TICKET_TOTAL);
			if (additionalPaymentAmount != null) {
				if (!additionalPaymentCurrency.equals(oldTicketCurrency)) {
					throw new GdsTypeAException();
				} else {
					mon.setAmount(oldTicketTotal.add(additionalPaymentAmount).toPlainString());
					mon.setCurrencyCode(additionalPaymentCurrency);
				}
			} else {
				taxTotal = BigDecimal.ZERO;
				for (TaxDetails tax : taxDetails) {
					for (TaxDetails.Tax t : tax.getTax()) {
						taxTotal = taxTotal.add(t.getAmount());
					}
				}

				if (equivalantAmount != null) {
					mon.setAmount(equivalantAmount.add(taxTotal).toPlainString());
					mon.setCurrencyCode(monetaryInfo.get(TypeAConstants.MonetaryType.EQUIVALENT_FARE).getCurrencyCode());

				} else if (baseAmount != null) {
					mon.setAmount(baseAmount.add(taxTotal).toPlainString());
					mon.setCurrencyCode(monetaryInfo.get(TypeAConstants.MonetaryType.BASE_FARE).getCurrencyCode());
				} else {
					throw new GdsTypeAException();
				}
			}


			for (String monType : monetaryInfo.keySet()) {
				if (!(monType.equals(TypeAConstants.MonetaryType.BASE_FARE) || monType.equals(TypeAConstants.MonetaryType.EQUIVALENT_FARE) ||
						monType.equals(TypeAConstants.MonetaryType.TICKET_TOTAL))) {
					monInfoList.add(monetaryInfo.get(monType));
				}
			}
		}

		return monInfoList;
	}


	public static TicketCoupon resolveTicketCoupon(List<TravellerTicketingInformationWrapper> travellerImages,
	                                               LCCClientReservationPax pax, LccClientPassengerEticketInfoTO coupon) {
		TicketCoupon ticketCoupon = null;
		TravellerTicketingInformation traveller;

		for (TravellerTicketingInformationWrapper travellerImage : travellerImages) {
			traveller = travellerImage.getTravellerInformation();
			if (areEqualDespiteWhiteSpaces(traveller.getTravellerSurname(), pax.getLastName()) &&
					areEqualDespiteWhiteSpaces(traveller.getTravellers().get(0).getGivenName(), pax.getFirstName())) {
				for (TicketNumberDetails tkt : traveller.getTickets()) {
					if (tkt.getTicketNumber().equals(coupon.getExternalPaxETicketNo())) {
						for (TicketCoupon cpn : tkt.getTicketCoupon()) {
							if (cpn.getCouponNumber().equals(String.valueOf(coupon.getExternalCouponNo()))) {
								ticketCoupon = cpn;
							}
						}
					}
				}
			}
		}

		return ticketCoupon;
	}

	public static TicketCoupon resolveTicketControlCoupon(List<TravellerTicketControlInformationWrapper> travellerImages,
	                                               LCCClientReservationPax pax, LccClientPassengerEticketInfoTO coupon) {
		TicketCoupon ticketCoupon = null;
		TravellerTicketControlInformation traveller;

		for (TravellerTicketControlInformationWrapper travellerImage : travellerImages) {
			traveller = travellerImage.getTravellerInformation();
			if (areEqualDespiteWhiteSpaces(traveller.getTravellerSurname(), pax.getLastName()) &&
					areEqualDespiteWhiteSpaces(traveller.getTravellers().get(0).getGivenName(), pax.getFirstName())) {
				for (TicketNumberDetails tkt : traveller.getTickets()) {
					if (tkt.getTicketNumber().equals(coupon.getExternalPaxETicketNo())) {
						for (TicketCoupon cpn : tkt.getTicketCoupon()) {
							if (cpn.getCouponNumber().equals(String.valueOf(coupon.getExternalCouponNo()))) {
								ticketCoupon = cpn;
							}
						}
					}
				}
			}
		}

		return ticketCoupon;
	}

	public static Map<Integer, Set<String>> groupTicketsByConjunctions(List<TravellerTicketingInformationWrapper> travellerWrappers) {
		int groupId = 0;
		TravellerTicketingInformation traveller;
		Map<Integer, Set<String>> conjunctions = new HashMap<Integer, Set<String>>();

		for (TravellerTicketingInformationWrapper travellerWrapper : travellerWrappers) {
			traveller = travellerWrapper.getTravellerInformation();

			groupId++;
			conjunctions.put(groupId, new HashSet<String>());

			for (TicketNumberDetails ticket : traveller.getTickets()) {
				conjunctions.get(groupId).add(ticket.getTicketNumber());
			}
		}

		return conjunctions;
	}

	public static LCCClientReservationPax resolvePax(LCCClientReservation reservation, int pnrPaxId) {
		int tempPnrPaxId;
		LCCClientReservationPax passenger = null;

		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			tempPnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());

			if (tempPnrPaxId == pnrPaxId) {
				passenger = pax;
				break;
			}
		}

		return passenger;
	}

	public static boolean areEqualDespiteWhiteSpaces(String value1, String value2) {
		return value1.replaceAll("\\s+", "").equalsIgnoreCase(value2.replaceAll("\\s+", ""));
	}

	public static void injectAmounts(GDSPaxChargesTO paxChargesTO, String gdsCode, List<MonetaryInformation> monetaryInformationList, List<TaxDetails> taxDetailsList)
		throws ModuleException {

		MonetaryInformation totalMon = null;
		MonetaryInformation equivalentMon = null;
		MonetaryInformation baseMon = null;

		for (MonetaryInformation monetaryInformation : monetaryInformationList) {
			if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {
				totalMon = monetaryInformation;
			} else if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.EQUIVALENT_FARE)) {
				equivalentMon = monetaryInformation;
			} else if (monetaryInformation.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.BASE_FARE)) {
				baseMon = monetaryInformation;
			}
		}

		BigDecimal fare = BigDecimal.ZERO;
		BigDecimal surcharge = BigDecimal.ZERO;
		BigDecimal tax = BigDecimal.ZERO;


		if (totalMon != null) {
			fare = GDSServicesUtil.convertToBaseCurrency(totalMon.getCurrencyCode(), new BigDecimal(totalMon.getAmount()));
		} else if (equivalentMon != null) {
			fare = GDSServicesUtil.convertToBaseCurrency(equivalentMon.getCurrencyCode(), new BigDecimal(equivalentMon.getAmount()));
		} else if (baseMon != null) {
			fare = GDSServicesUtil.convertToBaseCurrency(baseMon.getCurrencyCode(), new BigDecimal(baseMon.getAmount()));
		} else {
			throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		GDSExternalCodes.GDS gds = GDSExternalCodes.GDS.valueOf(gdsCode);
		Set<String> surchargeCodes = GDSServicesModuleUtil.getConfig().getSurchargeCodes().get(gds);

		if (taxDetailsList != null) {

		 	for (TaxDetails taxDetails : taxDetailsList) {
				for (TaxDetails.Tax singleTax : taxDetails.getTax()) {
					if (surchargeCodes.contains(singleTax.getType())) {
						surcharge = surcharge.add(singleTax.getAmount());
					} else {
						tax = tax.add(singleTax.getAmount());
					}
				}
			}

		}

		paxChargesTO.setFareAmount(fare.subtract(surcharge).subtract(tax));
		paxChargesTO.setSurChargeAmount(surcharge);
		paxChargesTO.setTaxAmount(tax);

	}

}
