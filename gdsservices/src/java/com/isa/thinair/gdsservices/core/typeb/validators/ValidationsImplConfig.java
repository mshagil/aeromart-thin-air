package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.List;

import com.isa.thinair.platform.api.IConfig;

public class ValidationsImplConfig implements IConfig {

	private List<ValidatorBase> commonValidators;

	/**
	 * @return the commonValidators
	 */
	public List<ValidatorBase> getCommonValidators() {
		return commonValidators;
	}

	/**
	 * @param commonValidators
	 *            the commonValidators to set
	 */
	public void setCommonValidators(List<ValidatorBase> commonValidators) {
		this.commonValidators = commonValidators;
	}
}
