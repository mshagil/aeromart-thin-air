package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.internal.BookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.IssueEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class IssueEticketRequestExtractor extends ValidatorBase {

	private static final Log log = LogFactory.getLog(IssueEticketRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	/**
	 * returns IssueEticketRequest
	 * 
	 * @param gdsCredentialsDTO
	 * @param ssrData
	 * @param bookingRequestDTO
	 * @return
	 */
	public static IssueEticketRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {
		IssueEticketRequest issueEticketRequest = new IssueEticketRequest();

		issueEticketRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		issueEticketRequest = (IssueEticketRequest) RequestExtractorUtil
				.addBaseAttributes(issueEticketRequest, bookingRequestDTO);

		issueEticketRequest = addPassengersAndSSRs(issueEticketRequest, ssrData, bookingRequestDTO, gdsCredentialsDTO);
		issueEticketRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return issueEticketRequest;
	}

	/**
	 * adds Passengers and SSRs to the AddSsrRequest
	 * 
	 * @param addSSRRequest
	 * @param ssrData
	 * @return
	 */
	private static IssueEticketRequest addPassengersAndSSRs(IssueEticketRequest issueEticketRequest, SSRData ssrData, BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		try {
			String extPnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getOriginatorRecordLocator());
	
			Reservation reservation = CommonUtil.loadReservation(gdsCredentialsDTO, extPnrReference, null);
	
			Map<String, Passenger> resPaxMap = new HashMap<String, Passenger>();
			Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();
	
			// add SSRDetailDTOs to paxs
			for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
				SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();
	
				if (GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssrDetailDTO.getCodeSSR())) {
					List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();
					String gdsActionCode = ssrDetailDTO.getActionOrStatusCode();
	
					if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {
	
						if (nameDTOs != null && nameDTOs.size() > 0) {
							for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
								NameDTO nameDTO = (NameDTO) iterNameDTO.next();
								SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
								ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
								String iataPaxName = nameDTO.getIATAName();
								if (ssr.getComment() != null && ssr.getComment().startsWith("INF")) {
									ssr.setComment(ssr.getComment().substring(3));
									boolean infantFound = false;
									for (ReservationPax reservationPax : reservation.getPassengers()) {
										String iataResPaxName = GDSApiUtils.getIATAName(reservationPax.getLastName(),
												reservationPax.getFirstName(), reservationPax.getTitle());
										if (iataPaxName.equals(iataResPaxName)) {
											for (ReservationPax infant : reservationPax.getInfants()) {
												iataPaxName = GDSApiUtils.getIATAName(infant.getLastName(),
														infant.getFirstName(), infant.getTitle());
												nameDTO.setFirstName(infant.getFirstName());
												nameDTO.setLastName(infant.getLastName());
												nameDTO.setPaxTitle(infant.getTitle());
												nameDTO.setFamilyID(infant.getFamilyID());
												infantFound = true;
												break;
											}
										}
										if (infantFound) {
											break;
										}
									}
								}

								Passenger resPax = resPaxMap.get(iataPaxName);
	
								if (resPax == null) {
									resPax = new Passenger();
									resPax = RequestExtractorUtil.getPassenger(resPax, nameDTO);
									resPaxMap.put(iataPaxName, resPax);
								}
	
								resPax.getSsrCollection().add(ssr);
								issueEticketRequest.getPassengers().add(resPax);
							}
						} else {
							// add to common SSRS
							SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
							ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
							issueEticketRequest.getCommonSSRs().add(ssr);
						}
					}
				}
			}
		} catch (ModuleException e) {
			
		}
		return issueEticketRequest;
	}
}
