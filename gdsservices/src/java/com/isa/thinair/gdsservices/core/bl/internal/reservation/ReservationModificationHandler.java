package com.isa.thinair.gdsservices.core.bl.internal.reservation;

import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRs;

public interface ReservationModificationHandler {

	ReservationRs splitResOrRemovePax(ReservationRq reservationRq);

	ReservationRs removeSegments(ReservationRq reservationRq);

	ReservationRs changePaxDetails(ReservationRq reservationRq);
}
