package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.RemovePaxRequest;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Manoj Dhanushka
 */
public class RemovePaxAction {

	private static Log log = LogFactory.getLog(RemovePaxAction.class);

	public static RemovePaxRequest processRequest(RemovePaxRequest removePaxRequest) {

		try {
			Reservation reservation = CommonUtil.loadReservation(removePaxRequest.getGdsCredentialsDTO(), null, removePaxRequest
					.getResponderRecordLocator().getPnrReference());

			CommonUtil.validateReservation(removePaxRequest.getGdsCredentialsDTO(), reservation);
			Collection<Integer> pnrPaxIds = CommonUtil.extractPaxIDs(reservation, removePaxRequest.getPassengers());
			Collection<Integer> infantPnrPaxIds = CommonUtil.extractInfantPaxIDs(reservation, removePaxRequest.getPassengers());
			
			if (infantPnrPaxIds.size() > 0) {
				pnrPaxIds.addAll(infantPnrPaxIds);
			}

			if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
				CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
				ServiceResponce serviceRes = AirproxyModuleUtils.getPassengerBD().removePassengers(reservation.getPnr(), null,
						pnrPaxIds, customChargesTO, new Long(reservation.getVersion()), CommonUtil.getTrackingInfo(removePaxRequest.getGdsCredentialsDTO()), null, null);

				if (serviceRes != null && serviceRes.isSuccess()) {
					String strNewPNR = (String) serviceRes.getResponseParam(CommandParamNames.PNR);
					removePaxRequest.setCancelledPnr(strNewPNR);
					removePaxRequest.setSuccess(true);
				} else {
					removePaxRequest.setSuccess(false);
					removePaxRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
					removePaxRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
				}

			} else {
				removePaxRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.actions.invalidPassengers"));
				removePaxRequest.setSuccess(false);
				removePaxRequest.addErrorCode("gdsservices.actions.invalidPassengers");
			}
		} catch (ModuleException e) {
			log.error(" RemovePaxAction Failed for GDS PNR " + removePaxRequest.getOriginatorRecordLocator().getPnrReference(), e);
			removePaxRequest.setSuccess(false);
			removePaxRequest.setResponseMessage(e.getMessageString());
			removePaxRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" RemovePaxAction Failed for GDS PNR " + removePaxRequest.getOriginatorRecordLocator().getPnrReference(), e);
			removePaxRequest.setSuccess(false);
			removePaxRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			removePaxRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return removePaxRequest;
	}

}
