package com.isa.thinair.gdsservices.core.bl.typeA;

public class DailyFlightAvailabilityTest extends BaseTestUtil {

	public static void main(String[] args) {
		DailyFlightAvailabilityTest availabilityTest = new DailyFlightAvailabilityTest();
		availabilityTest.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		StringBuffer message = new StringBuffer();
		message.append("UNA:+.? '");
		message.append("UNB+IATB:1+1PETS::PS+1AETH::HR+111024:1247+20440029390001++TR2+O'");
		message.append("UNH+1+PAOREQ:03:1:IA+09D0F10900284E'");
		message.append("MSG+:44'");
		message.append("ORG+AF:NIC'");
		message.append("ODI'");
		message.append("TVL+190810+SHJ+BOM+G9'");
		message.append("UNT+6+1'");
		return message.toString();
	}
}