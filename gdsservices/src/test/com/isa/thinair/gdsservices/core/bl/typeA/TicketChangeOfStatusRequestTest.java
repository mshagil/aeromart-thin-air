package com.isa.thinair.gdsservices.core.bl.typeA;

public class TicketChangeOfStatusRequestTest extends BaseTestUtil {

	public static void main(String args[]) {
		TicketChangeOfStatusRequestTest test = new TicketChangeOfStatusRequestTest();
		test.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		StringBuffer sb = new StringBuffer();

		sb.append("UNB+IATB:1+1PETS::PS+1AETH::HR+111024:1247+20440029390001++TR2+O'");
		sb.append("UNH+1+TKCREQ:03:1:IA+09D0F10900284E'");
		sb.append("MSG+:142'");
		sb.append("ORG+PS:HDQ+00000000:CRC+++A++SW'");
		sb.append("EQN+1:TD'");
		sb.append("TKT+1692409430037:T:1'");
		sb.append("CPN+1:B'");
		sb.append("UNT+7+1'");
		sb.append("UNZ+1+20440029390001'");

		return sb.toString();
	}
}
