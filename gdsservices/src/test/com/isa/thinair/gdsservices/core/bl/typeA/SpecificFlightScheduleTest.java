package com.isa.thinair.gdsservices.core.bl.typeA;

public class SpecificFlightScheduleTest extends BaseTestUtil {

	public static void main(String[] args) {
		SpecificFlightScheduleTest specificFlightScheduleTest = new SpecificFlightScheduleTest();
		specificFlightScheduleTest.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		String ediMessage = "UNA:+.? 'UNB+IATA:1+SR+DL+890701:0830+841F60'UNH+1+PAOREQ:96:2:IA'MSG+:50'ORG+1G:SWI+12345678'ODI'TVL+281210+++G9+568'UNT+6+1'";
		return ediMessage;
	}
}