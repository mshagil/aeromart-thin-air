package com.isa.thinair.gdsservices.api.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerTitle;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.PriceDetail;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.core.util.GDSTestCase;
import com.isa.thinair.gdsservices.core.util.TestLookupUtil;
import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class TestGDSServicesBD extends GDSTestCase {

	public void testProcessRequest() throws ModuleException {
//		System.setProperty("repository.home", "c:/isaconfig-test");
//		ModuleFramework.startup();
//		new ClientLoginModule().login("SYSTEM", "password",
//				"c:/isaconfig-test/client/config/client_jass_login.config", "JbossClientLogin");
//
//		gdsServicesBD = TestLookupUtil.getGDSServicesBD();

		//gdsServicesBD.processRequest(getNewBookingTestData());
		
	}
	
	
	private CreateReservationRequest getNewBookingTestData(){
		
		CreateReservationRequest reservationRequest = new CreateReservationRequest();
		
//		Collection<Segment> segmentCollection = new ArrayList<Segment>();
//		Segment seg = new Segment();
//		seg.setDepartureStation("SHJ");
//		seg.setArrivalStation("CMB");
//		Calendar cal = Calendar.getInstance();
//		//cal.add(Calendar.DATE, 3);
//		seg.setDepartureDateTime(cal.getTime());
//		segmentCollection.add(seg);
//		
//		Adult adt = new Adult();
//		adt.setFirstName("Dhanushka");
//		adt.setLastName("Ranatunga");
//		adt.setTitle(PassengerTitle.MR);
//		reservationRequest.getPassengers().add(adt);
//		
//		
//		ContactDetail contact = new ContactDetail();
//		contact.setTitle("Mr");
//		contact.setFirstName("Dhanu");
//		contact.setLastName("Rana");
//		contact.setStreetAddress("435/44, Nungamugoda");
//		contact.setCity("Kelaniya");
//		contact.setCountry("LK");
//		contact.setMobilePhoneNumber("094716808300");
//		contact.setLandPhoneNumber("094112917777");
//		contact.setFaxNumber("0941234567");
//		contact.setEmailAddress("dnranatunga@yahoo.com");
//		
//		PriceDetail priceDetail = new PriceDetail(); 
//		
//		reservationRequest.setSegmentDetails(segmentCollection);
//		reservationRequest.setContactDetail(contact);
//		reservationRequest.setPriceDetail(priceDetail);
//		
//		reservationRequest.addReservationAction(GDSCodes.ReservationAction.CREATE_BOOKING);
		
		return reservationRequest;
		
	}
	
	
	
}
