package com.isa.thinair.aircustomer.api.dto.lms;

import java.io.Serializable;
import java.util.Date;

public class LMSMemberDTO implements Serializable {

	private static final long serialVersionUID = 6611457277779695504L;

	private Integer lmsMemberId;

	private String ffid;

	private String firstName;

	private String middleName;

	private String lastName;

	private Date dateOfBirth;

	private String mobileNumber;

	private String phoneNumber;

	private Date regDate;

	private String enrollmentLocExtRef;

	private String passportNum;

	private String language;

	private String nationalityCode;

	private String residencyCode;

	private String headOFEmailId;

	private Integer sbInternalId;
	
	private String memberExternalId;

	private Integer genderTypeId;

	private String password;

	private String refferedEmail;
	
	private char confirmed;
	
	private String enrollingCarrier;

	private String enrollingChannelExtRef;

	private String enrollingChannelIntRef;


	/**
	 * @return the lmsMemberId
	 */
	public Integer getLmsMemberId() {
		return lmsMemberId;
	}

	/**
	 * @param lmsMemberId
	 *            the lmsMemberId to set
	 */
	public void setLmsMemberId(Integer lmsMemberId) {
		this.lmsMemberId = lmsMemberId;
	}

	/**
	 * @return the ffid
	 */
	public String getFfid() {
		return ffid;
	}

	/**
	 * @param ffid
	 *            the ffid to set
	 */
	public void setFfid(String ffid) {
		this.ffid = ffid;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName
	 *            the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the regDate
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * @param regDate
	 *            the regDate to set
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * @return the enrollmentLocExtRef
	 */
	public String getEnrollmentLocExtRef() {
		return enrollmentLocExtRef;
	}

	/**
	 * @param enrollmentLocExtRef
	 *            the enrollmentLocExtRef to set
	 */
	public void setEnrollmentLocExtRef(String enrollmentLocExtRef) {
		this.enrollmentLocExtRef = enrollmentLocExtRef;
	}

	/**
	 * @return the passportNum
	 */
	public String getPassportNum() {
		return passportNum;
	}

	/**
	 * @param passportNum
	 *            the passportNum to set
	 */
	public void setPassportNum(String passportNum) {
		this.passportNum = passportNum;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the nationalityCode
	 */
	public String getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode
	 *            the nationalityCode to set
	 */
	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return the residencyCode
	 */
	public String getResidencyCode() {
		return residencyCode;
	}

	/**
	 * @param residencyCode
	 *            the residencyCode to set
	 */
	public void setResidencyCode(String residencyCode) {
		this.residencyCode = residencyCode;
	}

	/**
	 * @return the headOFEmailId
	 */
	public String getHeadOFEmailId() {
		return headOFEmailId;
	}

	/**
	 * @param headOFEmailId
	 *            the headOFEmailId to set
	 */
	public void setHeadOFEmailId(String headOFEmailId) {
		this.headOFEmailId = headOFEmailId;
	}

	/**
	 * @return the sbInternalId
	 */
	public Integer getSbInternalId() {
		return sbInternalId;
	}

	/**
	 * @param sbInternalId
	 *            the sbInternalId to set
	 */
	public void setSbInternalId(Integer sbInternalId) {
		this.sbInternalId = sbInternalId;
	}

	/**
	 * @return the genderTypeId
	 */
	public Integer getGenderTypeId() {
		return genderTypeId;
	}

	/**
	 * @param genderTypeId
	 *            the genderTypeId to set
	 */
	public void setGenderTypeId(Integer genderTypeId) {
		this.genderTypeId = genderTypeId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the refferedEmail
	 */
	public String getRefferedEmail() {
		return refferedEmail;
	}

	/**
	 * @param refferedEmail
	 *            the refferedEmail to set
	 */
	public void setRefferedEmail(String refferedEmail) {
		this.refferedEmail = refferedEmail;
	}

	/**
	 * @return confirmed
	 * 				the confirmed to set
	 * 
	 */
	public char getConfirmed() {
		return confirmed;
	}

	/**
	 * @param confirmed
	 */
	public void setConfirmed(char confirmed) {
		this.confirmed = confirmed;
	}

	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}
	public String getEnrollingCarrier() {
		return enrollingCarrier;
	}

	public void setEnrollingCarrier(String enrollingCarrier) {
		this.enrollingCarrier = enrollingCarrier;
	}

	public String getEnrollingChannelExtRef() {
		return enrollingChannelExtRef;
	}

	public void setEnrollingChannelExtRef(String enrollingChannelExtRef) {
		this.enrollingChannelExtRef = enrollingChannelExtRef;
	}

	public String getEnrollingChannelIntRef() {
		return enrollingChannelIntRef;
	}

	public void setEnrollingChannelIntRef(String enrollingChannelIntRef) {
		this.enrollingChannelIntRef = enrollingChannelIntRef;
	}
}
