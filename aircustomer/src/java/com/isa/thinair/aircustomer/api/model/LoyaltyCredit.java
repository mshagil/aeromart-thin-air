/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 * @version $$Id$$
 */

package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_LOYALTY_CREDIT" LoyaltyCredit is the entity class to repesent a Loyalty Credit model
 * @author Chamindap
 * 
 */
public class LoyaltyCredit extends Persistent implements Serializable {

	private static final long serialVersionUID = 1874172480979149605L;

	private Integer loyaltyCreditId;

	private String loyaltyAccountNo;

	private BigDecimal creditEarned;

	private Date dateEarn;

	private Date dateExp;

	private BigDecimal creditBalance;

	public LoyaltyCredit() {

	}

	/**
	 * returns the loyaltyCreditId
	 * 
	 * @return Returns the loyaltyCreditId.
	 * 
	 * @hibernate.id column = "LOYALTY_CREDIT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_LOYALTY_CREDIT"
	 * 
	 * 
	 */
	public Integer getLoyaltyCreditId() {
		return loyaltyCreditId;
	}

	public void setLoyaltyCreditId(Integer id) {
		this.loyaltyCreditId = id;
	}

	/**
	 * @return Returns the loyaltyAccountNo.
	 * @hibernate.property column = "LOYALTY_ACCOUNT_NO"
	 */
	public String getLoyaltyAccountNo() {
		return loyaltyAccountNo;
	}

	/**
	 * @param loyaltyAccountNo
	 *            The loyaltyAccountNo to set.
	 */
	public void setLoyaltyAccountNo(String loyaltyAccountNo) {
		this.loyaltyAccountNo = loyaltyAccountNo;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CREDIT_EARNED"
	 */
	public BigDecimal getCreditEarned() {
		return creditEarned;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCreditEarned(BigDecimal creditEarned) {
		this.creditEarned = creditEarned;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CREDIT_BALANCE"
	 */
	public BigDecimal getCreditBalance() {
		return creditBalance;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCreditBalance(BigDecimal creditBalance) {
		this.creditBalance = creditBalance;
	}

	/**
	 * returns the dateOfBirth
	 * 
	 * @return Returns the dateOfBirth.
	 * @hibernate.property column = "DATE_EARN"
	 */
	public Date getDateEarn() {
		return dateEarn;
	}

	/**
	 * sets the dateEarn
	 * 
	 * @param dateEarn
	 *            The dateEarn to set.
	 */
	public void setDateEarn(Date dateEarn) {
		this.dateEarn = dateEarn;
	}

	/**
	 * returns the dateExp
	 * 
	 * @return Returns the dateExp.
	 * @hibernate.property column = "DATE_EXP"
	 */
	public Date getDateExp() {
		return dateExp;
	}

	/**
	 * sets the dateExp
	 * 
	 * @param dateExp
	 *            The dateExp to set.
	 */
	public void setDateExp(Date dateExp) {
		this.dateExp = dateExp;
	}

}