/**
 * 
 */
package com.isa.thinair.aircustomer.api.model;

import java.util.Date;

import com.isa.thinair.aircustomer.api.dto.external.AgentFrequentCustomerDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.util.Util;

/**
 * @hibernate.class table = "T_FREQUENT_CUSTOMER" FrequentCustomer is the entity class to represent a AgentFrequentCustomer model 
 * 							and this is different than Customer
 * @author suneth
 *
 */
public class AgentFrequentCustomer extends Persistent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2072859346315324601L;

	private Integer frequentCustomerId;
	
	private String email;

	private String title;

	private String firstName;

	private String lastName;
	
	private String passportNumber;
	
	private Date dateOfBirth;
	
	private Date passportExpiryDate;
	
	private String agentCode;
	
	private Integer nationalityCode;
	
	private String paxType;
	
	public AgentFrequentCustomer(){
		
	}
	
	/**
	 * Converts the Customer object to CustomerTO object
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public AgentFrequentCustomerDTO toAgentFrequentCustomerDTO() throws ModuleException {
		
		AgentFrequentCustomerDTO agentFrequentCustomerDTO = new AgentFrequentCustomerDTO();
		Util.copyProperties(agentFrequentCustomerDTO, this);
		return agentFrequentCustomerDTO;
		
	}

	/**
	 * @return the frequentCustomerId
	 * @hibernate.id column = "FREQUENT_CUSTOMER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FREQUENT_CUSTOMER"
	 */
	public Integer getFrequentCustomerId() {
		return frequentCustomerId;
	}

	/**
	 * @param frequentCustomerId the frequentCustomerId to set
	 */
	public void setFrequentCustomerId(Integer frequentCustomerId) {
		this.frequentCustomerId = frequentCustomerId;
	}

	/**
	 * @return the emailId
	 * @hibernate.property column = "EMAIL"
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the title
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 * @hibernate.property column = "FIRST_NAME"
	 * 
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 * @hibernate.property column = "LAST_NAME"
	 * 
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the passportNumber
	 * @hibernate.property column = "PASSPORT_NUMBER"
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @return the dateOfBirth
	 * @hibernate.property column = "DATE_OF_BIRTH"
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the passportExpiryDate
	 * @hibernate.property column = "PASSPORT_EXPIRY"
	 */
	public Date getPassportExpiryDate() {
		return passportExpiryDate;
	}

	/**
	 * @param passportExpiryDate the passportExpiryDate to set
	 */
	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	/**
	 * @return the agentCode
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the nationalityCode
	 * @hibernate.property column = "NATIONALITY_CODE"
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode the nationalityCode to set
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return the paxType
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}
	
}
