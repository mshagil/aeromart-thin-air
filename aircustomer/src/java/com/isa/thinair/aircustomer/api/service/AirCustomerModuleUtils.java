/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.api.service;

import com.isa.thinair.aircustomer.core.config.AirCustomerConfig;
import com.isa.thinair.aircustomer.core.persistence.dao.CustomerDAO;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;

/**
 * @author Chamindap
 */
public class AirCustomerModuleUtils {

	/**
	 * Returns the IModule reference.
	 * 
	 * @return the IModule reference.
	 */
	private static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("aircustomer");
	}

	/**
	 * Returns the CustomerDAO reference.
	 * 
	 * @return CustomerDAO reference.
	 */
	public static CustomerDAO getCustomerDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (CustomerDAO) lookupService.getBean("isa:base://modules/" + "aircustomer?id=customerDAOProxy");
	}

	/**
	 * Gets the dao
	 * 
	 * @param module
	 * @param daoModule
	 * @return
	 */
	public static Object getDAO(String daoModule) {
		String beanUri = null;

		beanUri = "isa:base://modules/aircustomer?id=" + daoModule + "Proxy";

		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(beanUri);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	/**
	 * Return transparent Crypto delegate
	 * 
	 * @return
	 */
	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) lookupEJB2ServiceBD(CryptoConstants.MODULE_NAME, CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	/**
	 * Return transparent Common Master delegate
	 * 
	 * @return
	 */
	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	/**
	 * Return transparent Location Service delegate
	 * 
	 * @return
	 */
	public static LocationBD getLocationServiceBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirCustomerConfig(), "aircustomer",
				"module.dependencymap.invalid");
	}

	/**
	 * Returns EJB2 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupEJB2ServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirCustomerConfig(),
				"aircustomer", "module.dependencymap.invalid");
	}

	public static AirCustomerConfig getAirCustomerConfig() {
		return (AirCustomerConfig) AirCustomerModuleUtils.getInstance().getModuleConfig();
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, LoyaltyManagementBD.SERVICE_NAME);
	}

	/**
	 * gets the auditor BD
	 * 
	 * @return
	 * @throws ModuleException 
	 */
	public static AuditorBD getAuditorBD() throws ModuleException {

		return (AuditorBD) lookupEJB2ServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);
		
	}
}