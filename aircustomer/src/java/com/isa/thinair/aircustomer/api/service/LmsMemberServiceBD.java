package com.isa.thinair.aircustomer.api.service;

import java.util.Collection;
import java.util.Locale;

import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author chethiya
 *
 */
public interface LmsMemberServiceBD {
	
	public static final String SERVICE_NAME = "LmsMemberService";
	
	/**
	 * @return
	 * @throws ModuleException
	 */
	public Collection<LmsMember> getLmsMembers() throws ModuleException;

	/**
	 * @param password
	 * @param emailId
	 * @throws ModuleException
	 */
	public void setPassword(String password, String emailId) throws ModuleException;
	
	/**
	 * @param emailId
	 * @return
	 * @throws ModuleException
	 */
	public LmsMember getLmsMember(String emailId) throws ModuleException;
	
	/**
	 * @param sbInternalId
	 * @return
	 * @throws ModuleException
	 */
	public LmsMember getLmsMemberBySBId(int sbInternalId) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @param emailId
	 * @param carrierCode
	 * @param locale TODO
	 * @throws ModuleException
	 */
	public void confirmMergeLmsMember(LmsMember lmsMember, String emailId, String carrierCode, Locale locale,
			boolean isServiceAppRQ) throws ModuleException;

	/**
	 * @param validationString
	 * @param emailId
	 * @return TODO
	 * @throws ModuleException
	 */
	
	
	public int mergeLmsMember(String validationString, String emailId) throws ModuleException;
	
	/**
	 * @param validationString
	 * @param emailId
	 * @return
	 * @throws ModuleException
	 */
	
	public int numberOfMerges(String validationString, String emailId) throws ModuleException;
	/**
	 * @param lmsMemberId
	 * @return
	 * @throws ModuleException
	 */
	
	public LmsMember getLmsMemberById(int lmsMemberId) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @throws ModuleException
	 */
	public void saveOrUpdate(LmsMember lmsMember) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @param carrierCode
	 * @param locale TODO
	 * @return
	 * @throws ModuleException
	 */
	public boolean register(LmsMember lmsMember, String carrierCode, Locale locale, boolean isServiceAppRQ)
			throws ModuleException;
	
	/**
	 * @param ffid
	 * @return
	 * @throws ModuleException
	 */
	public String getVerificationString(String ffid) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @param validationString
	 * @param carrierCode
	 * @param available TODO
	 * @param locale TODO
	 * @return
	 * @throws ModuleException
	 */
	public boolean resendLmsConfEmail(LmsMember lmsMember, String validationString, String carrierCode, boolean available,
			Locale locale, boolean isServiceAppRQ) throws ModuleException;
	
	/**
	 * @param ffid
	 * @param menuIntRef
	 * @return
	 * @throws ModuleException
	 */
	
	public String getCrossProtalLoginUrl(String ffid, String menuIntRef) throws ModuleException;

	/**
	 * @param lmsMember
	 * @param carrierCode
	 * @return
	 * @throws ModuleException
	 */
	public boolean sendLMSMergeSuccessEmail(LmsMember lmsMember, String carrierCode) throws ModuleException;

	public boolean syncLmsMember(String marketingAirLineCode, String memberFFID) throws ModuleException;

}
