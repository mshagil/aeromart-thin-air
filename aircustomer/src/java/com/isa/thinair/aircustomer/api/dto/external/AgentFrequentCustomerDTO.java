/**
 * 
 */
package com.isa.thinair.aircustomer.api.dto.external;

import java.io.Serializable;
import java.util.Date;

/**
 * @author suneth
 *
 */
public class AgentFrequentCustomerDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8709407769263194020L;

	private Integer frequentCustomerId;

	private String email;

	private String title;

	private String firstName;

	private String lastName;
	
	private String passportNumber;
	
	private Date dateOfBirth;
	
	private Date passportExpiryDate;
	
	private String agentCode;
	
	private Integer nationalityCode;

	private String paxType;
	
	/**
	 * @return the frequentCustomerId
	 */
	public Integer getFrequentCustomerId() {
		return frequentCustomerId;
	}

	/**
	 * @param frequentCustomerId the frequentCustomerId to set
	 */
	public void setFrequentCustomerId(Integer frequentCustomerId) {
		this.frequentCustomerId = frequentCustomerId;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the passportNumber
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * @param passportNumber the passportNumber to set
	 */
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the passportExpiryDate
	 */
	public Date getPassportExpiryDate() {
		return passportExpiryDate;
	}

	/**
	 * @param passportExpiryDate the passportExpiryDate to set
	 */
	public void setPassportExpiryDate(Date passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the nationalityCode
	 */
	public Integer getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode the nationalityCode to set
	 */
	public void setNationalityCode(Integer nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

}
