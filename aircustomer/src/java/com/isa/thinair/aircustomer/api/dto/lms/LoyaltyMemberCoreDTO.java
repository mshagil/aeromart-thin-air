package com.isa.thinair.aircustomer.api.dto.lms;

import java.io.Serializable;

/**
 * DTO to hold core data of a loyalty member
 * 
 * @author rumesh
 * 
 */
public class LoyaltyMemberCoreDTO extends LoyaltyBaseDTO implements Serializable {

	private static final long serialVersionUID = -7811885724415393282L;

	private int SBInternalMemberId;
	
	private String memberExternalId;

	private String memberNamePrefixString;

	private int memberNamePrefixInternalId;

	private String memberFirstName;

	private String memberLastName;

	private int memberBirthdateMonth;

	private int memberBirthdateDay;

	private int memberBirthdateYear;

	private String memberGenderString;
	
	private boolean eligibleForFamilyHead;

	private int memberGenderInternalId;

	public int getSBInternalMemberId() {
		return SBInternalMemberId;
	}

	public void setSBInternalMemberId(int sBInternalMemberId) {
		SBInternalMemberId = sBInternalMemberId;
	}
	

	public String getMemberExternalId() {
		return memberExternalId;
	}

	public void setMemberExternalId(String memberExternalId) {
		this.memberExternalId = memberExternalId;
	}

	public String getMemberNamePrefixString() {
		return memberNamePrefixString;
	}

	public void setMemberNamePrefixString(String memberNamePrefixString) {
		this.memberNamePrefixString = memberNamePrefixString;
	}

	public int getMemberNamePrefixInternalId() {
		return memberNamePrefixInternalId;
	}

	public void setMemberNamePrefixInternalId(int memberNamePrefixInternalId) {
		this.memberNamePrefixInternalId = memberNamePrefixInternalId;
	}

	public String getMemberFirstName() {
		return memberFirstName;
	}

	public void setMemberFirstName(String memberFirstName) {
		this.memberFirstName = memberFirstName;
	}

	public String getMemberLastName() {
		return memberLastName;
	}

	public void setMemberLastName(String memberLastName) {
		this.memberLastName = memberLastName;
	}

	public int getMemberBirthdateMonth() {
		return memberBirthdateMonth;
	}

	public void setMemberBirthdateMonth(int memberBirthdateMonth) {
		this.memberBirthdateMonth = memberBirthdateMonth;
	}

	public int getMemberBirthdateDay() {
		return memberBirthdateDay;
	}

	public void setMemberBirthdateDay(int memberBirthdateDay) {
		this.memberBirthdateDay = memberBirthdateDay;
	}

	public int getMemberBirthdateYear() {
		return memberBirthdateYear;
	}

	public void setMemberBirthdateYear(int memberBirthdateYear) {
		this.memberBirthdateYear = memberBirthdateYear;
	}

	public String getMemberGenderString() {
		return memberGenderString;
	}

	public void setMemberGenderString(String memberGenderString) {
		this.memberGenderString = memberGenderString;
	}

	public int getMemberGenderInternalId() {
		return memberGenderInternalId;
	}

	public void setMemberGenderInternalId(int memberGenderInternalId) {
		this.memberGenderInternalId = memberGenderInternalId;
	}

	public boolean isEligibleForFamilyHead() {
		return eligibleForFamilyHead;
	}

	public void setEligibleForFamilyHead(boolean eligibleForFamilyHead) {
		this.eligibleForFamilyHead = eligibleForFamilyHead;
	}
}
