/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

import com.isa.thinair.aircustomer.api.dto.external.CustomerQuestionaireTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Util;

/**
 * @hibernate.class table = "T_CUSTOMER_QUESTIONAIRE"
 * @author Chamindap
 */
public class CustomerQuestionaire /* extends Persistent */implements Serializable {

	private static final long serialVersionUID = -492447905355961310L;

	private int customerQuestionnairId;

	private Integer customerId;

	private Integer questionKey;

	private String questionAnswer;

	public CustomerQuestionaire() {

	}

	CustomerQuestionaire(CustomerQuestionaireTO customerQuestionaireTO) throws ModuleException {
		this();
		Util.copyProperties(this, customerQuestionaireTO);
	}

	CustomerQuestionaireTO toCustomerQuestionaireTO() throws ModuleException {
		CustomerQuestionaireTO customerQuestionaireTO = new CustomerQuestionaireTO();
		Util.copyProperties(customerQuestionaireTO, this);
		return customerQuestionaireTO;
	}

	/**
	 * returns the customer questionnair id.
	 * 
	 * @return Returns the customerQuestionnairId.
	 * @hibernate.id column = "CQ_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CUSTOMER_QUESTIONAIRE"
	 */
	public int getCustomerQuestionnairId() {
		return customerQuestionnairId;
	}

	/**
	 * @param customerQuestionnairId
	 *            The customerQuestionnairId to set.
	 */
	public void setCustomerQuestionnairId(int customerQuestionnairId) {
		this.customerQuestionnairId = customerQuestionnairId;
	}

	/**
	 * @return Returns the questionAnswer.
	 * @hibernate.property column = "QUES_ANS"
	 */
	public String getQuestionAnswer() {
		return questionAnswer;
	}

	/**
	 * @param questionAnswer
	 *            The questionAnswer to set.
	 */
	public void setQuestionAnswer(String questionAnswer) {
		this.questionAnswer = questionAnswer;
	}

	/**
	 * @return Returns the questionKey.
	 * @hibernate.property column = "QUES_KEY"
	 */
	public Integer getQuestionKey() {
		return questionKey;
	}

	/**
	 * @param questionKey
	 *            The questionKey to set.
	 */
	public void setQuestionKey(Integer questionKey) {
		this.questionKey = questionKey;
	}

	/**
	 * @return Returns the customerId.
	 * @hibernate.property column = "CUSTOMER_ID"
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            The customerId to set.
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
}