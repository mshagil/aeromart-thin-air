/**
 * 
 */
package com.isa.thinair.aircustomer.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "SM_T_SEAT_TYPE" SeatType is the model class for seat types available for seat preference
 * @author suneth
 * 
 */
public class SeatType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7185042275343859859L;
	
	private String seatType;
	
	private String description;
	
	private String status;
	
	public SeatType(){
		
	}

	/**
	 * @return the seatType
	 * @hibernate.id column = "SEAT_TYPE" generator-class = "assigned"
	 */
	public String getSeatType() {
		return seatType;
	}

	/**
	 * @param seatType the seatType to set
	 */
	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	/**
	 * @return the description
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
}
