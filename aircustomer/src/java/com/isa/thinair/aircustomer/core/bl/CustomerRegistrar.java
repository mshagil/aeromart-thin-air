/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.bl;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author chamindap
 * 
 */
public interface CustomerRegistrar {

	/**
	 * Method to register a customer.
	 * 
	 * @param customer
	 *            the customer.
	 * @param isServiceAppRQ
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public boolean register(Customer customer, String originCarrierCode, LoyaltyCustomerProfile loyaltyCustomerProfile,
			boolean isServiceAppRQ) throws ModuleException;

	/**
	 * Method to confirm a customer.
	 * 
	 * @param customerLoginId
	 *            the customerLoginId.
	 * @param key
	 *            the key.
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public boolean confirm(String customerLoginId, String key) throws ModuleException;

	/**
	 * Method to send the password to a customer.
	 * 
	 * @param customerLoginId
	 *            the customerLoginId.
	 * @param question
	 *            the question.
	 * @param answer
	 *            the answer.
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public boolean getForgotPassword(String loginId, String question, String answer, String originCarrierCode)
			throws ModuleException;

	/**
	 * Method to authenticate a customer.
	 * 
	 * @param loginId
	 * @param password
	 * @return
	 * @throws ModuleException
	 */
	public Customer authenticate(String loginId, String password) throws ModuleException;

	/**
	 * 
	 * @param customerId
	 * @return
	 * @throws ModuleException
	 */
	public Customer getCustomer(int customerId) throws ModuleException;

	/**
	 * 
	 * @param loginId
	 * @return
	 * @throws ModuleException
	 */
	public Customer getCustomer(String loginId) throws ModuleException;

	/**
	 * 
	 * @param socialCustomerId
	 * @return
	 * @throws ModuleException
	 */
	public SocialCustomer getSocialCustomer(String socialCustomerId, String socialSiteType) throws ModuleException;

	/**
	 * 
	 * @param customer
	 * @throws ModuleException
	 */
	public void saveOrUpdate(Customer customer) throws ModuleException;

	/**
	 * @param customerLoginId
	 * @throws ModuleException
	 */
	public void confirmLMSMemberStatus(String customerLoginId) throws ModuleException;

	/**
	 * @param partialEmail
	 * @param start TODO
	 * @param pageSize TODO
	 * @return
	 * @throws ModuleException
	 */
	public Page<Customer> getCustomerByPartialEmail(String partialEmail, int start, int pageSize) throws ModuleException;

	/**
	 * @param email
	 * @return
	 * @throws ModuleException
	 */
	public Page<Customer> getCustomerByEmail(String partialEmail) throws ModuleException;


	/**
	 * @param emailId
	 * @param originCarrierCode
	 * @throws ModuleException
	 */
	public void resendProfileDetailsMail(String emailId, String originCarrierCode) throws ModuleException;

	
	/**
	 * Method to send a link to reset the password to a customer.
	 * 
	 * @param customerLoginId
	 *            the customerLoginId.
	 * @param question
	 *            the question.
	 * @param answer
	 *            the answer.
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public boolean sendPasswordResetLink(String loginId, String question, String answer, String originCarrierCode, String baseUrl) throws ModuleException;

}