/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.persistence.dao;

import java.util.List;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredMeal;
import com.isa.thinair.aircustomer.api.model.CustomerPreferredSeat;
import com.isa.thinair.aircustomer.api.model.CustomerSession;
import com.isa.thinair.aircustomer.api.model.FamilyMember;
import com.isa.thinair.aircustomer.api.model.SeatType;
import com.isa.thinair.aircustomer.api.model.SocialCustomer;
import com.isa.thinair.commons.api.dto.Page;

/**
 * CustomerDAO interface
 * 
 * @author Chamindap
 * 
 */
public interface CustomerDAO {

	/**
	 * Save or update customer data.
	 * 
	 * @param customer
	 *            the customer
	 */
	public void saveOrUpdate(Customer customer);

	/**
	 * Get all customers.
	 * 
	 * @return a collection
	 */
	public List<Customer> getCustomers();

	/**
	 * Get a given customer.
	 * 
	 * @param customerId
	 * @return the customer
	 */
	public Customer getCustomer(int customerId);

	/**
	 * 
	 * @param loginId
	 * @return
	 */
	public Customer getCustomer(String loginId);

	/**
	 * 
	 * @param socialEmailId
	 * @return
	 */
	public SocialCustomer getSocialCustomer(String socialCustomerId, String socialSiteType);

	/**
	 * 
	 * @param loginId
	 * @param password
	 * @return
	 */
	public Customer authenticate(String loginId, String password);

	/**
	 * Returns the password for the user.
	 * 
	 * @param loginId
	 * @return
	 */
	public Customer getForgotPassword(String loginId, String question, String answer);

	public boolean isTokenExists(String token);

	public CustomerSession loadCustomerSession(String token);

	public void createCustomerSession(int customerId, String token);

	public Page<Customer> getCustomerByPartialEmail(String partialEmail, int start, int pageSize);

	public Page<Customer> getCustomerByEmail(String email);
	
	public List<SeatType> getPreferenceSeatTypes();
	
	public void setCustomerPreferredSeat(CustomerPreferredSeat customerPreferredSeat);
	
	public void setCustomerPreferredMeals(List<CustomerPreferredMeal> customerPreferredMealList);
	
	public String getCustomerPreferredSeatType(int customerId);
	
	public List<CustomerPreferredMealDTO> getCustomerPreferredMeals(int customerId);
	
	public void saveOrUpdate(FamilyMember familyMember);
	
	public FamilyMember getFamilyMember(String firstName , String lastName, int customerId);
	
	public FamilyMember getFamilyMember(int familyMemberId);
	
	public List<FamilyMember> getFamilyMembers(int customerId);
	
	public void removeFamilyMember(FamilyMember familyMember);
		
}