package com.isa.thinair.aircustomer.core.bl;

import java.util.List;
import java.util.Locale;

import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author chethiya
 *
 */
public interface LmsMemberBL {
	
	/**
	 * @return
	 * @throws ModuleException
	 */
	public List<LmsMember> getLmsMembers() throws ModuleException;

	/**
	 * @param password
	 * @param emailId
	 * @throws ModuleException
	 */
	public void setPassword(String password, String emailId) throws ModuleException;
	
	/**
	 * @param emailId
	 * @return
	 * @throws ModuleException
	 */
	public LmsMember getLmsMember(String emailId) throws ModuleException;
	
	/**
	 * @param sbInternalId
	 * @return
	 * @throws ModuleException
	 */
	public LmsMember getLmsMemberBySBId(int sbInternalId) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @param emailId
	 * @return TODO
	 * @throws ModuleException
	 */
	public int mergeLmsMember(String verificationString, String emailId) throws ModuleException;
	
	/**
	 * @param verificationString
	 * @param emailId
	 * @return
	 * @throws ModuleException
	 */
	public int numberOfMerges(String verificationString, String emailId) throws ModuleException;
	
	/**
	 * @param emailId
	 * @param carrierCode
	 * @param locale TODO
	 * @param verificationString
	 * @return
	 * @throws ModuleException
	 */
	public boolean confirmMergeLms(LmsMember lmsMember, String emailId, String carrierCode, Locale locale, boolean isServiceAppRQ)
			throws ModuleException;
	
	/**
	 * @param lmsMemberId
	 * @return
	 * @throws ModuleException
	 */
	public LmsMember getLmsMemberById(int lmsMemberId) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @return
	 * @throws ModuleException
	 */
	public boolean saveOrUpdate(LmsMember lmsMember) throws ModuleException;
	
	/**
	 * @param lmsMember
	 * @param carrierCode
	 * @param locale TODO
	 * @return
	 * @throws ModuleException
	 */
	public boolean register(LmsMember lmsMember, String carrierCode, Locale locale, boolean isServiceAppRQ)
			throws ModuleException;
	
	/**
	 * @param ffid
	 * @return
	 * @throws ModuleException
	 */
	public String getVerificationString(String ffid) throws ModuleException;

}
