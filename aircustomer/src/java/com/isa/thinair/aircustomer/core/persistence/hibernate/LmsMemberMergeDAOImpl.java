package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.isa.thinair.aircustomer.api.model.LmsMemberMerge;
import com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberMergeDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author chethiya
 *@isa.module.dao-impl dao-name="lmsMemberMergeDAO"
 */
public class LmsMemberMergeDAOImpl extends PlatformHibernateDaoSupport implements LmsMemberMergeDAO{

	
	
	/**
	 * (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberMergeDAO#getLmsMergeList()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<LmsMemberMerge> getLmsMergeList() {
		
		List<LmsMemberMerge> mergeList = null;
		mergeList = find("from LmsMemberMerge ",LmsMemberMerge.class);
		return mergeList;
	}

	/**
	 * (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberMergeDAO#getLmsMerge(java.lang.String)
	 */
	@Override
	public LmsMemberMerge getLmsMerge(String emailId) {
		LmsMemberMerge lmsMemberMerge = null;
		List<LmsMemberMerge> list = null;

		list = find("Select lmsMemberMerge " + "from LmsMemberMerge as lmsMemberMerge where upper(lmsMemberMerge.emailId)=?",
				emailId.toUpperCase(),LmsMemberMerge.class);

		if (list.size() != 0) {
			lmsMemberMerge = list.get(0);
		}

		return lmsMemberMerge;
	}

	/**
	 * (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberMergeDAO#getLmsMergeById(java.lang.String)
	 */
	@Override
	public LmsMemberMerge getLmsMergeById(String mergeId) {
		LmsMemberMerge lmsMemberMerge = null;
		List<LmsMemberMerge> list = null;

		list = find("Select lmsMemberMerge " + "from LmsMemberMerge as lmsMemberMerge where lmsMemberMerge.mergeId=?",
				mergeId,LmsMemberMerge.class);

		if (list.size() != 0) {
			lmsMemberMerge = list.get(0);
		}

		return lmsMemberMerge;
	}

	/**
	 * (non-Javadoc)
	 * @see com.isa.thinair.aircustomer.core.persistence.dao.LmsMemberMergeDAO#getVerificationString(java.lang.String)
	 */
	@Override
	public String getVerificationString(String emailId) {
		
		LmsMemberMerge lmsMemberMerge = null;
		lmsMemberMerge = getLmsMerge(emailId);
		if(lmsMemberMerge==null){
			return null;
		}
		return lmsMemberMerge.getVerificationString();
	}

	@Override
	public void saveOrUpdate(LmsMemberMerge lmsMemberMerge) {
		lmsMemberMerge.setEmailId(lmsMemberMerge.getEmailId().toUpperCase());
		super.hibernateSaveOrUpdate(lmsMemberMerge);
		
	}

	@Override
	public int deleteRecord(String verificationString, String emailId) {
		emailId = emailId.toUpperCase();
		String hql = "delete from LmsMemberMerge c where c.emailId=:emailId AND c.verificationString=:verificationString";
		Query query = getSession().createQuery(hql).setString("emailId", emailId)
				.setString("verificationString", verificationString);
		return query.executeUpdate();
		
	}
	
	@Override
	public int numberOfMerges(String verificationString, String emailId) {
		emailId = emailId.toUpperCase();
		String hql = "from LmsMemberMerge where emailId=:emailId AND verificationString=:verificationString";
		Query query = getSession().createQuery(hql).setString("emailId", emailId)
				.setString("verificationString", verificationString);
		List records = query.list();
		return records.size();
	}
	
	

}
