/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.persistence.hibernate;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCustomerDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CriteriaParserForHibernate;

/**
 * @author Chamindap
 * @isa.module.dao-impl dao-name="loyaltyCustomerDAO"
 */
public class LoyaltyCustomerDAOImpl extends PlatformHibernateDaoSupport implements LoyaltyCustomerDAO {

	/**
	 * Default constructure
	 */
	public LoyaltyCustomerDAOImpl() {
		super();
	}

	public void saveOrUpdate(LoyaltyCustomerProfile customer) {
		super.hibernateSaveOrUpdate(customer);
	}

	/**
	 * @see com.isa.thinair.customer.core.persistence.dao .CustomerDAO#removeCustomer(int)
	 */
	public void removeLoyaltyCustomer(String accountNo) {
		Object customer = load(LoyaltyCustomerProfile.class, accountNo);
		delete(customer);
	}

	/**
	 * Get all customers.
	 * 
	 * @return a collection
	 */
	public List<LoyaltyCustomerProfile> getLoyaltyCustomers() {
		return find("from LoyaltyCustomerProfile", LoyaltyCustomerProfile.class);
	}

	public String getLoyaltyAccountNo(Integer customerId) {
		List<String> list = null;
		String s = null;
		list = find(
				"Select customer.loyaltyAccountNo " + "from LoyaltyCustomerProfile as customer where customer.customerId=?",
				customerId, String.class);
		if (list.size() != 0) {
			s = (String) list.get(0);
		}
		return s;
	}

	/**
	 * Get a given customer.
	 * 
	 * @param customerId
	 * @return the customer
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomer(String accountNo) {
		return get(LoyaltyCustomerProfile.class, accountNo);
	}

	/**
	 * @see com.isa.thinair.aircustomer.core.persistence.dao .CustomerDAO#getCustomer(java.lang.String)
	 */
	public LoyaltyCustomerProfile getLoyaltyCustomerByCustomerId(int customerId) {
		LoyaltyCustomerProfile customer = null;
		List<LoyaltyCustomerProfile> list = null;

		list = find(
				"Select customer " + "from LoyaltyCustomerProfile as customer where customer.customerId=?", customerId,LoyaltyCustomerProfile.class);

		if (list.size() != 0) {
			customer = list.get(0);
		}

		return customer;
	}

	/**
	 * @see com.isa.thinair.aircustomer.core.persistence.dao .CustomerDAO#getCustomers(java.util.Set, int, int)
	 */
	@SuppressWarnings("unchecked")
	public Page getLoyaltyCustomers(List<ModuleCriterion> criteria, int startRec, int pageSize) {
		Collection<LoyaltyCustomerProfile> customers = null;
		Page page = null;
		int recSize;

		if (criteria != null) {
			customers = CriteriaParserForHibernate.parse(criteria, getSession().createCriteria(LoyaltyCustomerProfile.class))
					.list();
			recSize = customers.size();
			page = new Page(recSize, startRec, startRec + pageSize - 1, customers);
		}
		
		return page;
	}

	public LoyaltyCustomerProfile getLoyaltyCustomerByExample(LoyaltyCustomerProfile loyaltyCustomer) {

		LoyaltyCustomerProfile customer = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(loyaltyCustomer.getDateOfBirth());
		loyaltyCustomer.setMobile(getNumber(loyaltyCustomer.getMobile()));
		List<LoyaltyCustomerProfile> list = null;
		Object[] param = { loyaltyCustomer.getLoyaltyAccountNo().trim(), loyaltyCustomer.getEmail().trim().toLowerCase(),
				loyaltyCustomer.getCity().trim().toLowerCase(), loyaltyCustomer.getMobile().trim(),
				loyaltyCustomer.getCountryCode(), loyaltyCustomer.getNationalityCode(), cal.get(Calendar.DATE),
				cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR) };
		list = find(
				"Select c " + "from LoyaltyCustomerProfile as c where c.loyaltyAccountNo= ? " + "AND lower(email) = ? "
						+ "AND lower(c.city) = ? " + "AND replace(c.mobile, '-', '') = ? " + "AND c.countryCode= ? " + "AND c.nationalityCode= ? "
						+ "AND c.status = 'O' " + "AND day(c.dateOfBirth) = ?" + "AND month(c.dateOfBirth) = ?"
						+ "AND year(c.dateOfBirth) = ?", param, LoyaltyCustomerProfile.class);

		if (list.size() != 0) {
			customer = list.get(0);
		}
		return customer;
	}
	
	
	private String getNumber(String strNumber) {		
		if (strNumber != null) {
			strNumber = strNumber.replaceAll("-", "").replaceAll(" ", "");
		}
		return strNumber;
	}
	
}
