/**
 * 
 */
package com.isa.thinair.aircustomer.core.persistence.dao;

import com.isa.thinair.aircustomer.api.model.AgentFrequentCustomer;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author suneth
 *
 */
public interface AgentFrequentCustomerDAO {

	/**
	 * Save new Frequent Customer of update a existing one
	 * 
	 * @param frequentCustomer
	 */
	public void saveOrUpdate(AgentFrequentCustomer frequentCustomer);

	/**
	 * Search Frequent Customer List from First Name , Last Name , Email
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param agentCode
	 *            TODO
	 * @return FrequentCustomer List
	 */
	public Page searchFrequentCustomers(String firstName, String lastName, String email, String agentCode) throws ModuleException;

	/**
	 * Search Frequent Customer from agentFrequentCustomerID
	 * 
	 * @param agentFrequentCustomerId
	 * @return FrequentCustomer
	 */
	public AgentFrequentCustomer getFrequentCustomer(Integer agentFrequentCustomerId);

	/**
	 * Delete a Frequent Customer
	 * 
	 * @param frequentCustomer
	 */
	public void deleteFrequentCustomer(AgentFrequentCustomer frequentCustomer);
}
