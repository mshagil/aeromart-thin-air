/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aircustomer.core.bl;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.aircustomer.api.model.LoyaltyCustomerProfile;
import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.aircustomer.core.persistence.dao.LoyaltyCustomerDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author chamindap
 * 
 */
public class LoyaltyCustomerBLImpl implements LoyaltyCustomerBL {

	/** The CustomerDAO */
	private LoyaltyCustomerDAO customerDAO = null;

	/**
	 * The constructure.
	 */
	public LoyaltyCustomerBLImpl() {
		super();
	}

	/**
	 * Method to register a customer.
	 * 
	 * @param customer
	 *            the customer.
	 * @see com.isa.thinair.aircustomer.core.bl.CustomerRegistrar
	 *      #register(com.isa.thinair.aircustomer.api.model.Customer)
	 * @return the boolean value.
	 * @throws ModuleException
	 */
	public Hashtable<String, Hashtable<String, Integer>> saveAll(Collection<LoyaltyCustomerProfile> customers) throws ModuleException {

		Hashtable<String, Hashtable<String, Integer>> failedProfile = new Hashtable<String, Hashtable<String, Integer>>();
		Iterator<LoyaltyCustomerProfile> iter = customers.iterator();
		if (customerDAO == null) {
			customerDAO = (LoyaltyCustomerDAO) AirCustomerModuleUtils.getDAO("loyaltyCustomerDAO");
		}
		while (iter.hasNext()) {
			Integer errorCode = -1;
			LoyaltyCustomerProfile loyaltyCustomer = (LoyaltyCustomerProfile) iter.next();
			try {
				if (loyaltyCustomer.getMobile() != null && loyaltyCustomer.getMobile().length() > 0) {
					String[] mobileParts = StringUtils.split(loyaltyCustomer.getMobile(), "-");
					if (mobileParts.length != 3) {
						errorCode = 3;
						throw new CommonsDataAccessException("module.insert.error");
					}
				}
				customerDAO.saveOrUpdate(loyaltyCustomer);
			} catch (CommonsDataAccessException cdae) {
				if ("module.duplicate.key".equals(cdae.getExceptionCode()))
					errorCode = 1;
				else if ("module.insert.error".equals(cdae.getExceptionCode())
						|| "module.update.error".equals(cdae.getExceptionCode()))
					errorCode = 2;
				else if ("module.value.toolarge".equals(cdae.getExceptionCode()))
					errorCode = 3;
				else if ("module.integer.toolarge".equals(cdae.getExceptionCode()))
					errorCode = 4;
				else if ("module.saveconstraint.childerror".equals(cdae.getExceptionCode()))
					errorCode = 5;
				else if ("module.constraint.violated".equals(cdae.getExceptionCode()))
					errorCode = 6;

			} catch (Exception e) {
				errorCode = 0;
			}
			if (errorCode > 0) {
				Hashtable<String, Integer> failedField = new Hashtable<String, Integer>();

				if (errorCode == 1)
					failedField.put("accountNumber", 1);
				else {
					if (loyaltyCustomer.getLoyaltyAccountNo() == null || loyaltyCustomer.getLoyaltyAccountNo().length() == 0)
						failedField.put("accountNumber", 2);
					else if (loyaltyCustomer.getLoyaltyAccountNo().length() > 12)
						failedField.put("accountNumber", 3);

					if (loyaltyCustomer.getDateOfBirth() == null)
						failedField.put("dateOfBirth", 2);
					if (loyaltyCustomer.getCity() == null || loyaltyCustomer.getCity().length() == 0)
						failedField.put("city", 2);
					else if (loyaltyCustomer.getCity().length() > 50)
						failedField.put("city", 3);

					if (loyaltyCustomer.getCountryCode() == null || loyaltyCustomer.getCountryCode().length() == 0)
						failedField.put("countryCode", 2);
					else if (loyaltyCustomer.getCountryCode().length() > 2)
						failedField.put("countryCode", 3);
					else if (AirCustomerModuleUtils.getLocationServiceBD().getCountry(loyaltyCustomer.getCountryCode()) == null)
						failedField.put("countryCode", 5);

					if (loyaltyCustomer.getNationalityCode() == 0)
						failedField.put("nationalityCode", 2);
					else if (loyaltyCustomer.getNationalityCode() > 999)
						failedField.put("nationalityCode", 3);
					else if (AirCustomerModuleUtils.getCommonMasterBD().getNationality(loyaltyCustomer.getNationalityCode()) == null)
						failedField.put("nationalityCode", 5);

					if (loyaltyCustomer.getEmail() == null || loyaltyCustomer.getEmail().length() == 0)
						failedField.put("email", 2);
					else if (loyaltyCustomer.getEmail().length() > 100)
						failedField.put("email", 3);

					if (loyaltyCustomer.getMobile() == null || loyaltyCustomer.getMobile().length() == 0)
						failedField.put("mobile", 2);
					else if (loyaltyCustomer.getMobile().length() > 20)
						failedField.put("mobile", 3);
					else {
						String[] mobileParts = StringUtils.split(loyaltyCustomer.getMobile(), "-");
						if (mobileParts.length != 3)
							failedField.put("mobile", 7);
					}
					if (loyaltyCustomer.getCardType() == null || loyaltyCustomer.getCardType().length() == 0)
						failedField.put("cardType", 2);
					else if (loyaltyCustomer.getCardType().length() > 1)
						failedField.put("cardType", 3);
					else if (!"C".equals(loyaltyCustomer.getCardType()) && !"P".equals(loyaltyCustomer.getCardType()))
						failedField.put("cardType", 6);
					if (loyaltyCustomer.getStatus() == null || loyaltyCustomer.getStatus().length() == 0)
						failedField.put("status", 2);
					else if (loyaltyCustomer.getStatus().length() > 1)
						failedField.put("status", 3);
					else if (!"C".equals(loyaltyCustomer.getStatus()) && !"O".equals(loyaltyCustomer.getStatus()))
						failedField.put("status", 6);

				}
				failedProfile.put(loyaltyCustomer.getLoyaltyAccountNo(), failedField);
			}

		}
		return failedProfile;

	}
}