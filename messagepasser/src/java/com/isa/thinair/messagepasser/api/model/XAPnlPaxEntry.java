/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.messagepasser.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of PNL parsed information
 * 
 * @author Byorn
 * @since 1.0
 * @hibernate.class table = "T_XA_PNL_PARSED"
 */
public class XAPnlPaxEntry extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -546317974338360390L;

	/** Holds the pnl pax parsed id */
	private int pnlPaxId;

	/** Holds the pnl unique id */
	private Integer pnlId;

	/** Holds the received time stamp */
	private Date receivedDate;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the flight date */
	private Date flightDate;

	/** Holds the departure airport */
	private String departureAirport;

	/** Holds the pnr */
	private String pnr;

	/** Holds the passenger title */
	private String title;

	/** Holds the passenger first name */
	private String firstName;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the process status (not processed, processed, error) */
	private String processedStatus;

	/** Holds the cabin class */
	private String cabinClassCode;

	/** Holds the arrival airport */
	private String arrivalAirport;

	/** Holds the error description */
	private String errorDescription;

	/** Holds the external airline pnr number */
	private String xaPnr;

	/** Holds the infant first name */
	private String infantFirstName;

	/** Holds the infant last name */
	private String infantLastName;

	/** Holds the infant title */
	private String infantTitle;

	/** Holds the in-bound information */
	private String inBoundInfo;

	/** Holds the out-bound information */
	private String outBoundInfo;

	/** Holds the infant ssr code */
	private String infantSSRCode;

	/** Holds the infant ssr remarks */
	private String infantSSRRemarks;

	/** Holds the adult ssr code */
	private String adultSSRCode;

	/** Holds the adult ssr remarks */
	private String adultSSRRemarks;

	/** Holds the passenger type */
	private String paxType;

	/**
	 * @return Returns the arrivalAirport.
	 * @hibernate.property column = "DESTINATION_STATION"
	 */
	public String getArrivalAirport() {
		return arrivalAirport;
	}

	/**
	 * @param arrivalAirport
	 *            The arrivalAirport to set.
	 */
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the departureAirport.
	 * @hibernate.property column = "DEP_STN"
	 */
	public String getDepartureAirport() {
		return departureAirport;
	}

	/**
	 * @param departureAirport
	 *            The departureAirport to set.
	 */
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		if (errorDescription == null) {
			errorDescription = "";
		}
		if (this.errorDescription != null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
			if(this.errorDescription.length() > 100){
				this.errorDescription = this.errorDescription.trim().substring(0,99);
			}
		}
	}

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightDate.
	 * @hibernate.property column = "FLIGHT_DATE"
	 */
	public Date getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            The flightDate to set.
	 */
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the pnlId.
	 * @hibernate.property column = "PNL_ID"
	 */
	public Integer getPnlId() {
		return pnlId;
	}

	/**
	 * @param pnlId
	 *            The pnlId to set.
	 */
	public void setPnlId(Integer pnlId) {
		this.pnlId = pnlId;
	}

	/**
	 * @return Returns the pnlPaxId.
	 * @hibernate.id column = "PP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_XA_PNL"
	 */
	public int getPnlPaxId() {
		return pnlPaxId;
	}

	/**
	 * @param pnlPaxId
	 *            The pnlPaxId to set.
	 */
	public void setPnlPaxId(int pnlPaxId) {
		this.pnlPaxId = pnlPaxId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the processedStatus.
	 * @hibernate.property column = "PROC_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 *            The processedStatus to set.
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the receivedDate.
	 * @hibernate.property column = "RECIEVED_DATE"
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * @param receivedDate
	 *            The receivedDate to set.
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the infantFirstName.
	 * @hibernate.property column = "INFANT_FIRST_NAME"
	 */
	public String getInfantFirstName() {
		return infantFirstName;
	}

	/**
	 * @param infantFirstName
	 *            The infantFirstName to set.
	 */
	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	/**
	 * @return Returns the infantLastName.
	 * @hibernate.property column = "INFANT_LAST_NAME"
	 */
	public String getInfantLastName() {
		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return Returns the infantTitle.
	 * @hibernate.property column = "INFANT_TITLE"
	 */
	public String getInfantTitle() {
		return infantTitle;
	}

	/**
	 * @param infantTitle
	 *            The infantTitle to set.
	 */
	public void setInfantTitle(String infantTitle) {
		this.infantTitle = infantTitle;
	}

	/**
	 * @return Returns the xaPnr.
	 * @hibernate.property column = "XA_PNR"
	 */
	public String getXaPnr() {
		return xaPnr;
	}

	/**
	 * @param xaPnr
	 *            The xaPnr to set.
	 */
	public void setXaPnr(String xaPnr) {
		this.xaPnr = xaPnr;
	}

	/**
	 * @return Returns the inBoundInfo.
	 * @hibernate.property column = "IN_BOUND_INFO"
	 */
	public String getInBoundInfo() {
		return inBoundInfo;
	}

	/**
	 * @param inBoundInfo
	 *            The inBoundInfo to set.
	 */
	public void setInBoundInfo(String inBoundInfo) {
		this.inBoundInfo = inBoundInfo;
	}

	/**
	 * @return Returns the outBoundInfo.
	 * @hibernate.property column = "OUT_BOUND_INFO"
	 */
	public String getOutBoundInfo() {
		return outBoundInfo;
	}

	/**
	 * @param outBoundInfo
	 *            The outBoundInfo to set.
	 */
	public void setOutBoundInfo(String outBoundInfo) {
		this.outBoundInfo = outBoundInfo;
	}

	/**
	 * @return Returns the adultSSRCode.
	 * @hibernate.property column = "ADULT_SSR_CODE"
	 */
	public String getAdultSSRCode() {
		return adultSSRCode;
	}

	/**
	 * @param adultSSRCode
	 *            The adultSSRCode to set.
	 */
	public void setAdultSSRCode(String adultSSRCode) {
		this.adultSSRCode = adultSSRCode;
	}

	/**
	 * @return Returns the adultSSRRemarks.
	 * @hibernate.property column = "ADULT_SSR_REMARKS"
	 */
	public String getAdultSSRRemarks() {
		return adultSSRRemarks;
	}

	/**
	 * @param adultSSRRemarks
	 *            The adultSSRRemarks to set.
	 */
	public void setAdultSSRRemarks(String adultSSRRemarks) {
		this.adultSSRRemarks = adultSSRRemarks;
	}

	/**
	 * @return Returns the infantSSRCode.
	 * @hibernate.property column = "INFANT_SSR_CODE"
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRRemarks.
	 * @hibernate.property column = "INFANT_SSR_REMARKS"
	 */
	public String getInfantSSRRemarks() {
		return infantSSRRemarks;
	}

	/**
	 * @param infantSSRRemarks
	 *            The infantSSRRemarks to set.
	 */
	public void setInfantSSRRemarks(String infantSSRRemarks) {
		this.infantSSRRemarks = infantSSRRemarks;
	}
}
