/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates. All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.messagepasser.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of Pnl header information
 * 
 * @author Byorn
 * @since 1.0
 * @hibernate.class table = "T_XA_PNL"
 */
public class XAPnl extends Persistent {
	/** Holds the pnl parsed id */
	private int pnlId;

	/** Hold the date downloaded */
	private Date dateDownloaded;

	/** Hold the from address of Pnl */
	private String fromAddress;

	/** Hold the carrier code */
	private String carrierCode;

	/** Hold the xaPnlContent */
	private String xaPnlContent;

	/** Hold the fromAirport */
	private String fromAirport; //

	/** Hold the toAirport */
	private String toAirport;

	/** Holds the number of passengers */
	private int numberOfPassengers;

	/** Hold the proceed status */
	private String processedStatus;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the Departure Date */
	private Date departureDate;

	/** Holds the partNumber */
	private int partNumber;

	/** Holds the number of Attempts */
	private int numOfAttempts;

	/** Holds the xa flight number */
	private String xaFlightNumber;

	/**
	 * @return Returns the departureDate.
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the carrierCode.
	 * @hibernate.property column = "CARRIER_CODE"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            The carrierCode to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return Returns the dateDownloaded.
	 * @hibernate.property column = "DATE_OF_DOWNLOAD"
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the fromAddress.
	 * @hibernate.property column = "FROM_ADDRESS"
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return Returns the fromAirport.
	 * @hibernate.property column = "FROM_AIRPORT"
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the numberOfPassengers.
	 * @hibernate.property column = "NUM_OF_PAX"
	 */
	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	/**
	 * @param numberOfPassengers
	 *            The numberOfPassengers to set.
	 */
	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

	/**
	 * @return Returns the pnlId.
	 * @hibernate.id column = "PNL_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_XA_PNL_PARSED"
	 */
	public int getPnlId() {
		return pnlId;
	}

	/**
	 * @param pnlId
	 *            The pnlId to set.
	 */
	public void setPnlId(int pnlId) {
		this.pnlId = pnlId;
	}

	/**
	 * @return Returns the processedStatus.
	 * @hibernate.property column = "PROCESSED_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 *            The processedStatus to set.
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the toAirport.
	 * @hibernate.property column = "TO_AIRPORT"
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	/**
	 * @return Returns the xaPnlContent.
	 * @hibernate.property column = "PNL_CONTENT"
	 */
	public String getXaPnlContent() {
		return xaPnlContent;
	}

	/**
	 * @param xaPnlContent
	 *            The xaPnlContent to set.
	 */
	public void setXaPnlContent(String xaPnlContent) {
		this.xaPnlContent = xaPnlContent;
	}

	/**
	 * @return Returns the numOfAttempts.
	 * @hibernate.property column = "NUMBER_OF_ATTEMPS"
	 */
	public int getNumOfAttempts() {
		return numOfAttempts;
	}

	/**
	 * @param numOfAttempts
	 *            The numOfAttempts to set.
	 */
	public void setNumOfAttempts(int numOfAttempts) {
		this.numOfAttempts = numOfAttempts;
	}

	/**
	 * @return Returns the partNumber.
	 * @hibernate.property column = "PART_NUMBER"
	 */
	public int getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setPartNumber(int partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return Returns the xaFlightNumber.
	 * @hibernate.property column = "XA_FLIGHT_NUMBER"
	 */
	public String getXaFlightNumber() {
		return xaFlightNumber;
	}

	/**
	 * @param xaFlightNumber
	 *            The xaFlightNumber to set.
	 */
	public void setXaFlightNumber(String xaFlightNumber) {
		this.xaFlightNumber = xaFlightNumber;
	}

}
