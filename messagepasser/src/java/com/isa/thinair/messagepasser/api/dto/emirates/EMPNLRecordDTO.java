/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.dto.emirates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import com.isa.thinair.messagepasser.api.utils.ParserConstants;

/**
 * Holds the pnl atomic record specific data transfer information
 * 
 * @author Byorn
 * @since 1.0
 */
public class EMPNLRecordDTO {
	/** Hold the tour Id * */
	// private String tourId;

	/** Holds the passenger number */
	private String paxNumber;

	/** Holds the ticket number will contain .L/10257445 */
	// private String ticketnumber;

	/** Holds the inbound Details .I/G9608Y03ALY */
	private String inboundDetails;

	/** Holds the outbound Details .O/G9608Y03ALY */
	private String outboundDetails;

	/** Holds a collection of EMRecordDTO */
	private Collection records;

	/** Hold the collection of EMRecordRemarkDTO */
	/** NOTE: ONLY FOR THE PARSER * */
	/** SEE EMRecordDTO: It has its relavent remarks info */
	private Collection remarks;

	/**
	 * Holds the error Description
	 */
	private String errorDescription;

	/** Holds the destination airport, cccode, totnumberofpax */
	private String desAirportNumPaxCCCode;

	/** Holds the Cabin class code of the flight **/
	private String ccCode;

	/** Hold the Number of passengers Flown **/
	private String totNumberOfPax;

	/** Hold the destination airport **/
	private String destinationAirport;

	/**
	 * Add Records DTO
	 * 
	 * @param recordDTO
	 */
	public void addRecordDTO(EMRecordDTO recordDTO) {
		// If the first name is empty
		// This can be for a Goshore or a Noshore reservation
		if (recordDTO.getFirstNameWithTitle() == null) {
			this.addRecords(recordDTO);
		} else {
			StringTokenizer stringTokenizer = new StringTokenizer(recordDTO.getFirstNameWithTitle(), " ", false);

			if (stringTokenizer.countTokens() != 1) {
				int i = 1;
				while (stringTokenizer.hasMoreElements()) {
					String firstName = (String) stringTokenizer.nextElement();

					if (i == 1) {
						recordDTO.setFirstNameWithTitle(firstName);
						this.addRecords(recordDTO);
					} else {
						EMRecordDTO copyRecordDTO = (EMRecordDTO) recordDTO.clone();
						copyRecordDTO.setFirstNameWithTitle(firstName);
						this.addRecords(copyRecordDTO);
					}

					i++;
				}
			} else {
				this.addRecords(recordDTO);
			}
		}
	}

	/**
	 * Add Remarks
	 * 
	 * @param recordDTO
	 */
	public void addRemarkRecord(EMRecordRemarkDTO remarkDTO) {
		if (this.getRemarks() == null) {
			this.setRemarks(new ArrayList());
		}
		String ssrCode = remarkDTO.getSsrCode();
		if (okToADD(ssrCode)) {
			this.getRemarks().add(remarkDTO);
		}
	}

	private boolean okToADD(String ssrCode) {

		if (ssrCode.equals(ParserConstants.SSR_CODES.CODE_OKOB) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_WCHR)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_BSCT) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_BLND)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_DEAF) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_LANG)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_MAAS) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_WCHP)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_INAD) || ssrCode.equals(ParserConstants.SSR_CODES.CODE_VIP)
				|| ssrCode.equals(ParserConstants.SSR_CODES.CODE_TKNA)
				|| ssrCode.equals(ParserConstants.EMRECORD_REMARK_DTO.INFANT_CODE1)
				|| ssrCode.equals(ParserConstants.EMRECORD_REMARK_DTO.INFANT_CODE2)) {
			return true;

		} else {

			if (ssrCode.equals(ParserConstants._VIP_CASE)) {

				return true;
			}

			return false;
		}

	}

	/**
	 * Add Records
	 * 
	 * @param recordDTO
	 */
	private void addRecords(EMRecordDTO recordDTO) {
		if (this.getRecords() == null) {
			this.setRecords(new ArrayList());
		}

		this.getRecords().add(recordDTO);
	}

	public String getInboundDetails() {
		if (this.inboundDetails != null) {
			if (this.inboundDetails.startsWith(".I/")) {
				this.inboundDetails = this.inboundDetails.substring(3);
			}
		}
		return inboundDetails;
	}

	public void setInboundDetails(String inboundDetails) {
		this.inboundDetails = inboundDetails;
	}

	public String getOutboundDetails() {
		if (this.outboundDetails != null) {
			if (this.outboundDetails.startsWith(".O/")) {
				this.outboundDetails = this.outboundDetails.substring(3);
			}
		}
		return outboundDetails;

	}

	public void setOutboundDetails(String outboundDetails) {
		this.outboundDetails = outboundDetails;
	}

	// public String getTourId() {
	// return tourId;
	// }
	//
	// public void setTourId(String tourId) {
	// this.tourId = tourId;
	// }

	/**
	 * @return Returns the records.
	 */
	public Collection getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            The records to set.
	 */
	public void setRecords(Collection records) {
		this.records = records;
	}

	/**
	 * @return Returns the ticketnumber.
	 */
	// public String getTicketnumber() {
	// return ticketnumber;
	// }

	/**
	 * @param ticketnumber
	 *            The ticketnumber to set.
	 */
	// public void setTicketnumber(String ticketnumber) {
	// this.ticketnumber = ticketnumber;
	// }

	/**
	 * Returns the pnr number
	 * 
	 * @return
	 */
	// public String getXAPnrNumber() {
	// String pnr = null;
	// if (this.getTicketnumber() != null && this.getTicketnumber().length() > 0) {
	// pnr = this.getTicketnumber().substring(2);
	// return pnr.replaceAll("/", "");
	// }
	// else {
	// return null;
	// }
	//
	// }

	/**
	 * @return Returns the paxNumber.
	 */
	public String getPaxNumber() {
		return paxNumber;
	}

	/**
	 * @param paxNumber
	 *            The paxNumber to set.
	 */
	public void setPaxNumber(String paxNumber) {
		this.paxNumber = paxNumber;
	}

	/**
	 * @return Returns the errorDescription.
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the remarks.
	 */
	public Collection getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(Collection remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the desAirportNumPaxCCCode
	 */
	public String getDesAirportNumPaxCCCode() {
		return desAirportNumPaxCCCode;
	}

	/**
	 * @param desAirportNumPaxCCCode
	 *            the desAirportNumPaxCCCode to set
	 */
	public void setDesAirportNumPaxCCCode(String desAirportNumPaxCCCode) {
		this.desAirportNumPaxCCCode = desAirportNumPaxCCCode;
		this.destinationAirport = desAirportNumPaxCCCode.substring(1, 4);
		this.totNumberOfPax = desAirportNumPaxCCCode.substring(4, desAirportNumPaxCCCode.length() - 1);
		this.ccCode = desAirportNumPaxCCCode.substring(desAirportNumPaxCCCode.length() - 1);
	}

	/**
	 * @return the ccCode
	 */
	public String getCcCode() {
		return ccCode;
	}

	/**
	 * @return the totNumberOfPax
	 */
	public String getTotNumberOfPax() {
		return totNumberOfPax;
	}

	/**
	 * @return the destinationAirport
	 */
	public String getDestinationAirport() {
		return destinationAirport;
	}

}
