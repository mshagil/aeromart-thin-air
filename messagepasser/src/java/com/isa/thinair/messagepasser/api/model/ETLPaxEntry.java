package com.isa.thinair.messagepasser.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of ETL parsed information
 * 
 * @author Ishan
 * @since 1.0
 * @hibernate.class table = "T_ETL_PARSED"
 */
public class ETLPaxEntry extends Persistent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -546317974338360390L;

	/** Holds the etl pax parsed id */
	private int etlPaxId;

	/** Holds the etl unique id */
	private Integer etlId;

	/** Holds the received time stamp */
	private Date receivedDate;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the flight date */
	private Date flightDate;

	/** Holds the departure airport */
	private String departureAirport;

	/** Holds the pnr */
	private String pnr;

	/** Holds the passenger title */
	private String title;

	/** Holds the passenger first name */
	private String firstName;

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the process status (not processed, processed, error) */
	private String processedStatus;

	/** Holds the cabin class */
	private String cabinClassCode;

	/** Holds the arrival airport */
	private String arrivalAirport;

	/** Holds the error description */
	private String errorDescription;

	/** Holds the passenger type */
	private String paxType;

	private String eticketNumber;

	private Integer coupNumber;

	private String paxStatus;

	private String infEticketNumber;

	private Integer infCoupNumber;

	private int parentEpId;

	private String logicalCabinClassCode;

	private String bookingClassCode;

	private String externalRecordLocator;

	private String marketingFlightElement;

	private String codeShareFlightNo;

	private String codeShareBc;

	private Integer gdsId;

	private String externalEticketNumber;

	private Integer externalCouponNo;

	private String externalInfEticketNumber;

	private Integer externalInfCoupNumber;
	
	private char useAeroMartETS = 'N';

	/**
	 * @return Returns the arrivalAirport.
	 * @hibernate.property column = "DESTINATION_STATION"
	 */
	public String getArrivalAirport() {
		return arrivalAirport;
	}

	/**
	 * @param arrivalAirport
	 *            The arrivalAirport to set.
	 */
	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the departureAirport.
	 * @hibernate.property column = "DEP_STN"
	 */
	public String getDepartureAirport() {
		return departureAirport;
	}

	/**
	 * @param departureAirport
	 *            The departureAirport to set.
	 */
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	/**
	 * @return Returns the errorDescription.
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            The errorDescription to set.
	 */
	public void setErrorDescription(String errorDescription) {
		// if (errorDescription == null) {
		// errorDescription = "";
		// }
		//
		// if (this.errorDescription == null) {
		// this.errorDescription = errorDescription;
		// } else {
		// this.errorDescription = this.errorDescription + " " + errorDescription;
		// if (this.errorDescription.length() > 100) {
		// this.errorDescription = this.errorDescription.trim().substring(0, 99);
		// }
		// }
		this.errorDescription = errorDescription;
	}

	/**
	 * @return Returns the firstName.
	 * @hibernate.property column = "FIRST_NAME"
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the flightDate.
	 * @hibernate.property column = "FLIGHT_DATE"
	 */
	public Date getFlightDate() {
		return flightDate;
	}

	/**
	 * @param flightDate
	 *            The flightDate to set.
	 */
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	/**
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the lastName.
	 * @hibernate.property column = "LAST_NAME"
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the etlId.
	 * @hibernate.property column = "ETL_ID"
	 */
	public Integer getEtlId() {
		return etlId;
	}

	/**
	 * @param etlId
	 *            The etlId to set.
	 */
	public void setEtlId(Integer etlId) {
		this.etlId = etlId;
	}

	/**
	 * @return Returns the etlPaxId.
	 * @hibernate.id column = "EP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_ETL"
	 */
	public int getEtlPaxId() {
		return etlPaxId;
	}

	/**
	 * @param etlPaxId
	 *            The etlPaxId to set.
	 */
	public void setEtlPaxId(int etlPaxId) {
		this.etlPaxId = etlPaxId;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the processedStatus.
	 * @hibernate.property column = "PROC_STATUS"
	 */
	public String getProcessedStatus() {
		return processedStatus;
	}

	/**
	 * @param processedStatus
	 *            The processedStatus to set.
	 */
	public void setProcessedStatus(String processedStatus) {
		this.processedStatus = processedStatus;
	}

	/**
	 * @return Returns the receivedDate.
	 * @hibernate.property column = "RECIEVED_DATE"
	 */
	public Date getReceivedDate() {
		return receivedDate;
	}

	/**
	 * @param receivedDate
	 *            The receivedDate to set.
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * @return Returns the title.
	 * @hibernate.property column = "TITLE"
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the infantTitle.
	 * @hibernate.property column = "ET_NO"
	 */
	public String getEticketNumber() {
		return eticketNumber;
	}

	public void setEticketNumber(String eticketNumber) {
		this.eticketNumber = eticketNumber;
	}

	/**
	 * @return Returns the coupNumber.
	 * @hibernate.property column = "COUP_NO"
	 */
	public Integer getCoupNumber() {
		return coupNumber;
	}

	public void setCoupNumber(Integer coupNumber) {
		this.coupNumber = coupNumber;
	}

	/**
	 * @return Returns the paxStatus.
	 * @hibernate.property column = "PAX_STATUS"
	 */
	public String getPaxStatus() {
		return paxStatus;
	}

	public void setPaxStatus(String paxStatus) {
		this.paxStatus = paxStatus;
	}

	/**
	 * @return Returns the infantTitle.
	 * @hibernate.property column = "INF_ET_NO"
	 */
	public String getInfEticketNumber() {
		return infEticketNumber;
	}

	public void setInfEticketNumber(String infEticketNumber) {
		this.infEticketNumber = infEticketNumber;
	}

	/**
	 * @return Returns the coupNumber.
	 * @hibernate.property column = "INF_COUP_NO"
	 */
	public Integer getInfCoupNumber() {
		return infCoupNumber;
	}

	public void setInfCoupNumber(Integer infCoupNumber) {
		this.infCoupNumber = infCoupNumber;
	}

	/**
	 * @return Returns the etlPaxId.
	 * @hibernate.id column = "PARENT_EP_ID"
	 */
	public int getParentEpId() {
		return parentEpId;
	}

	public void setParentEpId(int parentEpId) {
		this.parentEpId = parentEpId;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	/**
	 * @return Returns the paxType.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return Returns the externalRecordLocator.
	 * @hibernate.property column = "EXTERNAL_REC_LOCATOR"
	 */
	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	/**
	 * @return Returns the marketingFlightElement.
	 * @hibernate.property column = "CS_FLIGHT_INFO"
	 */
	public String getMarketingFlightElement() {
		return marketingFlightElement;
	}

	public void setMarketingFlightElement(String marketingFlightElement) {
		this.marketingFlightElement = marketingFlightElement;
	}

	/**
	 * @return Returns the codeShareFlightNo.
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	/**
	 * @return Returns the codeShareBc.
	 * @hibernate.property column = "CS_BOOKING_CLASS"
	 */
	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}

	/**
	 * @return the gdsId
	 * @hibernate.property column = "GDS_ID"
	 */
	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * @return
	 * @hibernate.property column ="EXT_ET_NO"
	 */
	public String getExternalEticketNumber() {
		return externalEticketNumber;
	}

	/**
	 * @param externalEticketNumber
	 */
	public void setExternalEticketNumber(String externalEticketNumber) {
		this.externalEticketNumber = externalEticketNumber;
	}

	/**
	 * @return
	 * @hibernate.property column ="EXT_COUP_NO"
	 */
	public Integer getExternalCouponNo() {
		return externalCouponNo;
	}

	public void setExternalCouponNo(Integer externalCouponNo) {
		this.externalCouponNo = externalCouponNo;
	}

	/**
	 * @return
	 * @hibernate.property column ="EXT_INF_ET_NO"
	 */
	public String getExternalInfEticketNumber() {
		return externalInfEticketNumber;
	}

	public void setExternalInfEticketNumber(String externalInfEticketNumber) {
		this.externalInfEticketNumber = externalInfEticketNumber;
	}

	/**
	 * @return
	 * @hibernate.property column ="EXT_INF_COUP_NO"
	 */
	public Integer getExternalInfCoupNumber() {
		return externalInfCoupNumber;
	}

	public void setExternalInfCoupNumber(Integer externalInfCoupNumber) {
		this.externalInfCoupNumber = externalInfCoupNumber;
	}
	
	/**
	 * @return
	 * @hibernate.property column="USE_AA_ETS"
	 */
	public char getUseAeroMartETS() {
		return useAeroMartETS;
	}
	

	public void setUseAeroMartETS(char useAeroMartETS) {
		this.useAeroMartETS = useAeroMartETS;
	}


	public boolean isUseAeroMartETS() {

		return this.useAeroMartETS == 'Y' ? true : false;

	}

}
