/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.dto.emirates;

/**
 * Holds the PNL atomic record specific data transfer information
 * 
 * @author Byorn
 * @since 1.0
 */
public class EMRecordDTO {

	public static final String SSR_CODE_For_XAPNR = "TKNA";

	/** Holds the passenger last name */
	private String lastName;

	/** Holds the passenger first name with title */
	private String firstNameWithTitle;

	private String ssrCode;

	private String ssrText;

	private String infantName;

	private String infantLastName;

	private String infantSSRCode;

	private String infantSSRText;

	private String errorDescription;

	/** Holds the ticket number will contain .L/10257445 */
	private String ticketnumber;

	/** Hold the tour Id * */
	private String tourId;

	/**
	 * Add the first Name with title
	 * 
	 * @param fName
	 */
	public void addFName(String fName) {
		if (this.getFirstNameWithTitle() == null) {
			this.setFirstNameWithTitle(fName);
		} else {
			this.setFirstNameWithTitle(this.getFirstNameWithTitle() + " " + fName);
		}
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the title in order to parse
	 * 
	 * @return
	 */
	public String getParseTitle() {
		if (this.getFirstNameWithTitle() == null) {
			return this.getLastName();
		} else {
			return this.getFirstNameWithTitle();
		}
	}

	/**
	 * Clones the RecordDTO object
	 */
	public Object clone() {
		EMRecordDTO recordDTO = new EMRecordDTO();
		recordDTO.setFirstNameWithTitle(this.getFirstNameWithTitle());
		recordDTO.setLastName(this.getLastName());

		return recordDTO;
	}

	/**
	 * @return Returns the firstNameWithTitle.
	 */
	public String getFirstNameWithTitle() {
		return firstNameWithTitle;
	}

	/**
	 * @param firstNameWithTitle
	 *            The firstNameWithTitle to set.
	 */
	public void setFirstNameWithTitle(String firstNameWithTitle) {
		this.firstNameWithTitle = firstNameWithTitle;
	}

	/**
	 * @return Returns the infantName.
	 */
	public String getInfantName() {
		return infantName;
	}

	/**
	 * @param infantName
	 *            The infantName to set.
	 */
	public void setInfantName(String infantName) {
		this.infantName = infantName;
	}

	/**
	 * @return Returns the infantSSRCode.
	 */
	public String getInfantSSRCode() {
		return infantSSRCode;
	}

	/**
	 * @param infantSSRCode
	 *            The infantSSRCode to set.
	 */
	public void setInfantSSRCode(String infantSSRCode) {
		this.infantSSRCode = infantSSRCode;
	}

	/**
	 * @return Returns the infantSSRText.
	 */
	public String getInfantSSRText() {
		return infantSSRText;
	}

	/**
	 * @param infantSSRText
	 *            The infantSSRText to set.
	 */
	public void setInfantSSRText(String infantSSRText) {
		this.infantSSRText = infantSSRText;
	}

	/**
	 * @return Returns the ssrCode.
	 */
	public String getSsrCode() {
		if (this.ssrCode == null) {
			this.ssrCode = " ";
		}

		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return Returns the ssrText.
	 */
	public String getSsrText() {
		if (this.ssrText == null) {
			this.ssrText = "";
		}
		return this.ssrText;
	}

	/**
	 * @param ssrText
	 *            The ssrText to set.
	 */
	public void setSsrText(String ssrText) {

		this.ssrText = ssrText;
	}

	/**
	 * @return Returns the infantLastName.
	 */
	public String getInfantLastName() {

		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            The infantLastName to set.
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	public String getErrorDescription() {
		if (this.errorDescription == null) {
			this.errorDescription = "";
		}
		return this.errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		if (errorDescription == null) {
			errorDescription = "";
		}

		if (this.errorDescription == null) {
			this.errorDescription = errorDescription;
		} else {
			this.errorDescription = this.errorDescription + " " + errorDescription;
		}
	}

	/**
	 * @return the ticket number
	 */
	public String getTicketnumber() {
		return ticketnumber;
	}

	/**
	 * @param ticketnumber
	 *            the ticket number to set
	 */
	public void setTicketnumber(String ticketnumber) {
		this.ticketnumber = ticketnumber;
	}

	/**
	 * Returns the XA pnr number
	 * 
	 * @return
	 */
	public String getXAPnrNumber() {
		String pnr = null;
		if (this.getTicketnumber() != null && this.getTicketnumber().length() > 0) {
			pnr = this.getTicketnumber().substring(2);
			return pnr.replaceAll("/", "");
		} else {
			return null;
		}

	}

	/**
	 * @return the tourId
	 */
	public String getTourId() {
		return tourId;
	}

	/**
	 * @param tourId
	 *            the tourId to set
	 */
	public void setTourId(String tourId) {
		this.tourId = tourId;
	}

}
