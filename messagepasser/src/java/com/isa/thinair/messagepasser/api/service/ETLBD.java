package com.isa.thinair.messagepasser.api.service;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLProcessPaxResponseDTO;
import com.isa.thinair.messagepasser.api.model.CodeShareETL;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.platform.api.ServiceResponce;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Ishan
 */
public interface ETLBD {

	public static final String SERVICE_NAME = "ETLService";

	/**
	 * Reconcile ETL Reservations
	 * 
	 * @param etlId
	 * @param isConsiderFutureDateValidation TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reconcileETLReservations(int etlId, boolean isConsiderFutureDateValidation) throws ModuleException;

	/**
	 * Prcoess etl reconcilation
	 */
	public ServiceResponce processETLMessage() throws ModuleException;

	/**
	 * Returns ETL entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	public Page getPagedETLData(List criteria, int startIndex, int noRecs, List orderByFieldList) throws ModuleException;

	/**
	 * Return ETL entries
	 * 
	 * @param etlId
	 * @return
	 * @throws ModuleException
	 */

	public Collection getEtlParseEntries(int etlId) throws ModuleException;

	/**
	 * Return Passenger final sales record
	 * 
	 * @param etlID
	 * @return
	 * @throws ModuleException
	 */
	public ETL getETL(int etlID) throws ModuleException;

	public int getEtlParseEntryCount(int etlId, String processedStatus) throws ModuleException;

	public Integer parseDocument(Collection<String> msgNamesThatMayHavePartTwoThree, String strMsgName, MessageTypes messageType)
			throws ModuleException, ParseException;

	public Collection<String> getValidatedDocuments(String processPath, String errorPath, String partialDownloadPath,
			String messageType);

	public void downloadMessagesFromMailServer(MessageConfigDTO msgCofigDTO, String messageType) throws ModuleException;

	/**
	 * @param etlPaxEntry
	 * @param etl
	 * @return ETLProcessPaxResponseDTO
	 * @throws ModuleException
	 */
	public ETLProcessPaxResponseDTO processETLPaxEntry(ETLPaxEntry etlPaxEntry, ETL etl) throws ModuleException;

	public void saveETLParseEntry(ETLPaxEntry etlPaxEntry) throws ModuleException;

	public void deleteETLPaxEntry(int etlPaxId) throws ModuleException;

	public ETLPaxEntry getETLPaxEntry(int etlPaxId) throws ModuleException;

	public void saveETLEntry(ETL etl) throws ModuleException;

	public boolean hasETL(String flightNumber, Date departureDate, String airport, Collection<String> processedStatus);

	public void updateETLEntry(ETL etl);

	public void getEtlPrlFilesViaSitaTex(List<String> messages, String messageType) throws ModuleException;
	
	public void saveCodeShareETLEntry(CodeShareETL etl) throws ModuleException;
}
