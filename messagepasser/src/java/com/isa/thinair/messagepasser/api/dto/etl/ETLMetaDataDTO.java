/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.api.dto.etl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Holds Electronic Ticket List Meta data transfer information
 * 
 * @author Ishan
 * @since 1.0
 */
public class ETLMetaDataDTO {

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the departure day e.g. 02 */
	private String day;

	/** Holds the departure month e.g. DEC */
	private String month;

	/** Holds the from airport */
	private String boardingAirport;

	/** Holds the part number */
	private String partNumber;

	/** Holds the day and month e.g 02DEC */
	private String dayMonth;

	/** Hold the pnlContent */
	private String pnlContent;

	/** Hold the date downloaded */
	private Date dateDownloaded;

	/** Hold the from address of Pfs */
	private String fromAddress;

	/** Holds the real departure date and the timestamp **/
	private Date realDepartureDate;

	/** Holds the EMPNLRecordDTO **/
	private Collection passengerRecords;

	private Map tourIdTicketNumberMap = new HashMap();

	private String actualFlightNumber;

	/**
	 * Add passenger
	 * 
	 * @param passengerRecordDTO
	 */
	public void addPassengerRecords(ETLRecordDTO empnlRecordDTO) {
		if (this.getPassengerRecords() == null) {
			this.setPassengerRecords(new ArrayList());
		}

		this.getPassengerRecords().add(empnlRecordDTO);
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getCarrierCode() {
		String ccCode = null;

		if (!"".equals(this.flightNumber)) {
			ccCode = flightNumber.substring(0, 2);
		}
		return ccCode;
	}

	/**
	 * @return Returns the pnlContent.
	 */
	public String getPnlContent() {
		return pnlContent;
	}

	/**
	 * @param pnlContent
	 *            The pnlContent to set.
	 */
	public void setPnlContent(String pnlContent) {
		this.pnlContent = pnlContent;
	}

	/**
	 * @return Returns the dateDownloaded.
	 */
	public Date getDateDownloaded() {
		return dateDownloaded;
	}

	/**
	 * @param dateDownloaded
	 *            The dateDownloaded to set.
	 */
	public void setDateDownloaded(Date dateDownloaded) {
		this.dateDownloaded = dateDownloaded;
	}

	/**
	 * @return Returns the fromAddress.
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @param fromAddress
	 *            The fromAddress to set.
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	/**
	 * @return Returns the dayMonth.
	 */
	public String getDayMonth() {
		return dayMonth;
	}

	/**
	 * @param dayMonth
	 *            The dayMonth to set.
	 */
	public void setDayMonth(String dayMonth) {
		this.dayMonth = dayMonth;

		this.setMonth(dayMonth.substring(dayMonth.length() - 3, dayMonth.length()));
		this.setDay(dayMonth.substring(0, dayMonth.length() - 3));
	}

	/**
	 * Returns the departure date
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Date getDepartureDate() throws ModuleException {
		Calendar calToday = new GregorianCalendar();
		int year = calToday.get(Calendar.YEAR);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
		String tmpdate = year + "-" + this.getMonth() + "-" + this.getDay();

		Date dateToCheck = null;
		try {
			dateToCheck = dateFormat.parse(tmpdate);
			Date sysDate = new Date(System.currentTimeMillis());
			if (CalendarUtil.isGreaterThan(dateToCheck, sysDate)) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(sysDate);
				cal.add(Calendar.DATE, 1);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				return cal.getTime();
			}
			return dateToCheck;
		} catch (ParseException e1) {
			throw new ModuleException("airreservations.pnl.invalidMonthAndDay");
		}
	}

	/**
	 * @return Returns the boardingAirport.
	 */
	public String getBoardingAirport() {
		return boardingAirport;
	}

	/**
	 * @param boardingAirport
	 *            The boardingAirport to set.
	 */
	public void setBoardingAirport(String boardingAirport) {
		this.boardingAirport = boardingAirport;
	}

	/**
	 * @return Returns the day.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            The day to set.
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the partNumber.
	 */
	public String getPartNumber() {
		if (this.partNumber == null || this.partNumber.length() < 5) {
			return "";
		}
		return this.partNumber.substring(4);

	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return Returns the realDepartureDate.
	 */
	public Date getRealDepartureDate() {
		return realDepartureDate;
	}

	/**
	 * @param realDepartureDate
	 *            The realDepartureDate to set.
	 */
	public void setRealDepartureDate(Date realDepartureDate) {
		this.realDepartureDate = realDepartureDate;
	}

	// TODO
	/**
	 * @return Returns the destinationAirport.
	 */
	public String getDestinationAirport() {
		if (passengerRecords != null) {
			for (Iterator iterator = this.passengerRecords.iterator(); iterator.hasNext();) {
				ETLRecordDTO obj = (ETLRecordDTO) iterator.next();
				if (obj.getDestinationAirport() != null) {
					return obj.getDestinationAirport();
				}
			}
		}
		return null;
	}

	/**
	 * @return Returns the totNumberOfPax.
	 */
	public int getTotNumberOfPax() {
		int totPax = 0;
		if (passengerRecords != null) {
			for (Iterator iterator = this.passengerRecords.iterator(); iterator.hasNext();) {
				ETLRecordDTO obj = (ETLRecordDTO) iterator.next();
				totPax += Integer.parseInt(obj.getTotNumberOfPax());
			}
		}
		return totPax;
	}

	/**
	 * @return Returns the passengerRecords.
	 */
	public Collection getPassengerRecords() {
		return passengerRecords;
	}

	/**
	 * @param passengerRecords
	 *            The passengerRecords to set.
	 */
	public void setPassengerRecords(Collection passengerRecords) {
		this.passengerRecords = passengerRecords;
	}

	/**
	 * @return Returns the tourIdTicketNumberMap.
	 */
	public Map getTourIdTicketNumberMap() {
		return tourIdTicketNumberMap;
	}

	/**
	 * @param tourIdTicketNumberMap
	 *            The tourIdTicketNumberMap to set.
	 */
	public void setTourIdTicketNumberMap(Map tourIdTicketNumberMap) {
		this.tourIdTicketNumberMap = tourIdTicketNumberMap;
	}

	public String getActualFlightNumber() {
		return actualFlightNumber;
	}

	public void setActualFlightNumber(String actualFlightNumber) {
		this.actualFlightNumber = actualFlightNumber;
	}

	/**
	 * 
	 * @param tourId
	 * @return
	 */
	public String getXAPnrNumber(String tourId) {

		String tcktNumber = (String) getTourIdTicketNumberMap().get(tourId);
		if (tcktNumber == null) {
			return null;
		}
		String pnr = tcktNumber.substring(2);
		return pnr.replaceAll("/", "");
	}

	/**
	 * 
	 * @param tourId
	 * @param ticketNumber
	 */
	public void addTourIdTicketNumber(String tourId, String ticketNumber) {

		if (tourId != null && ticketNumber != null) {

			getTourIdTicketNumberMap().put(tourId, ticketNumber);

		}

	}
}
