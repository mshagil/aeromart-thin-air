package com.isa.thinair.messagepasser.api.utils;

public interface ParserConstants {

	public static final String MAPKEY_CONT = "CONT";

	public static final String CC_CODE = "Y";

	/**
	 * holds the valid ssr codes in our system
	 */
	public static interface SSR_CODES {

		public static final String CODE_TKNA = "TKNA";

		public static final String CODE_OKOB = "OKOB";

		public static final String CODE_WCHR = "WCHR";

		public static final String CODE_BSCT = "BSCT";

		public static final String CODE_BLND = "BLND";

		public static final String CODE_DEAF = "DEAF";

		public static final String CODE_LANG = "LANG";

		public static final String CODE_MAAS = "MAAS";

		public static final String CODE_WCHP = "WCHP";

		public static final String CODE_INAD = "INAD";

		public static final String CODE_VIP = "VIP";

		public static final String CODE_DOCS = "DOCS";

		public static final String CODE_XBAG = "XBAG";

		public static final String CODE_SEAT = "SEAT";

	}

	public static final String _VIP_CASE = "1VIP";

	public static interface EMRECORD_REMARK_DTO {

		public static final String INFANT_CODE1 = "INFT";

		public static final String INFANT_CODE2 = "INF";

		public static final String CHILD = "CHD";

	}

	public enum MessageTypes {
		ETL, PRL, SSIM
	}

	/** Denotes PRL process status types */
	public static interface PRLProcessStatus {
		/** Holds the process status */
		public static final String PROCESSED = "P";

		/** Holds the no processed status */
		public static final String NOT_PROCESSED = "N";

		/** Holds the error occured status */
		public static final String ERROR_OCCURED = "E";
	}

	/** Donotes Prl status types */
	public static interface PRLStatus {
		/** Holds the unparsed status - it is not already parsed */
		public static final String UN_PARSED = "U";

		/** Holds the un parsed with errors status */
		public static final String UN_PARSED_WITH_ERRORS = "E";

		/** Holds the parsed status - it is already parsed */
		public static final String PARSED = "P";

		/** Holds the reconcile status - it is reconciled with no errors */
		public static final String RECONCILE_SUCCESS = "R";

		/** Holds the reconcile status - Waiting for PFS process after parsed until ETL processed */
		public static final String WAITING_FOR_PRL_PROCESS = "W";
	}

	/** Denotes ETL process status types */
	public static interface ETLProcessStatus {
		/** Holds the process status */
		public static final String PROCESSED = "P";

		/** Holds the no processed status */
		public static final String NOT_PROCESSED = "N";

		/** Holds the error occured status */
		public static final String ERROR_OCCURED = "E";
	}
	
	
	/** Denotes SSIM process status types */
	public static interface SSIMProcessStatus {		
		/** Holds the process status */
		public static final String PARSED = "P";

		/** Holds the no processed status */
		public static final String COMPLETED = "C";

		/** Holds the error occured status */
		public static final String ERROR_OCCURED = "E";
	}
	
	/** Denotes SSIMParsedEntry  process status types */
	public static interface SSIMScheduleProcessStatus {		 
		/** Holds the process status */
		public static final String PARSED = "P";

		/** Holds the no processed status */
		public static final String SCHEDULE_CREATED = "C";

		/** Holds the error occured status */
		public static final String ERROR_OCCURED = "E";
	}
}
