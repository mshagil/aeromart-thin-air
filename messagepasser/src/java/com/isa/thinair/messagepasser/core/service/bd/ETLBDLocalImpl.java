package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.messagepasser.api.service.ETLBD;

/**
 * @author Ishan
 */
@Local
public interface ETLBDLocalImpl extends ETLBD {

}
