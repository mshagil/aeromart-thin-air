/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.messagepasser.api.service.ReconcileCsvBD;

@Remote
public interface ReconcileCsvBDImpl extends ReconcileCsvBD {

}
