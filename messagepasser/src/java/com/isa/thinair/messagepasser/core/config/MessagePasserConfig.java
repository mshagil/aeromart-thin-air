/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.messagepasser.core.config;

import java.util.Map;
import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * Holds Air Reservation related module Configurations
 * 
 * @author Nilindra Fernando, ISuru Nishan, Byorn John
 * @since 1.0
 * @isa.module.config-bean
 */
public class MessagePasserConfig extends DefaultModuleConfig {

	/* Mail Server Related */
	private String defaultMailServer;
	private String popMailServer;
	private String ttyMailServer;
	private String defaultMailServerUsername;
	private String defaultMailServerPassword;

	/* XA PNL Related */
	private String xaPnlProcessPath;
	private String xaPnlParsedPath;
	private String xaPnlErrorPath;
	private String xaPnlMailServerUsername;
	private String xaPnlMailServerPassword;
	private String xaFetchPnlFromMailServer;
	private String xaDeleteFromMailServer;
	private String emiratesDefaultAgentCode;
	private Properties emailXAPnlErrorTo;
	private String xaPnlRetryLimit;
	private Map xaFlightNumberMap;

	/* ETL Related configurations */
	private String etlProcessPath;
	private String etlParsedPath;
	private String etlErrorPath;
	private String etlPartialDownloadPath;
	private String etlMailServerUsername;
	private String etlMailServerPassword;
	private String ttyMailServerUserName;
	private String ttyMailServerPassword;
	private String etlFetchFromMailServer;
	private String etlDeleteFromMailServer;
	private Properties emailEtlErrorTo;

	/* PRL Related configurations */
	private String prlProcessPath;
	private String prlParsedPath;
	private String prlErrorPath;
	private String prlPartialDownloadPath;
	private String prlMailServerUsername;
	private String prlMailServerPassword;
	private String prlFetchFromMailServer;
	private String prlDeleteFromMailServer;
	private String prlPopMailServer;

	/* PFS XML Processing configurations */
	private String pfsJaxbXmlPkgPath;
	private String ftpFolder;
	private String ftpIP;
	private String ftpUser;
	private String ftpPassword;
	private String cipher;
	private String cipherPassword;
	private String pfsLocalPath;
	private String pfsFtpErrFrom;
	private Properties pfsFtpErrTo;

	private String ssimMailServerUsername;
	private String ssimMailServerPassword;
	private String ssimProcessPath;
	private String ssimParsedPath;
	private String ssimErrorPath;
	private String ssimPartialDownloadPath;
	private String ssimFetchFromMailServer;
	private String ssimDeleteFromMailServer;

	/**
	 * @return Returns the defaultMailServer.
	 */
	public String getDefaultMailServer() {
		return defaultMailServer;
	}

	/**
	 * @param defaultMailServer
	 *            The defaultMailServer to set.
	 */
	public void setDefaultMailServer(String defaultMailServer) {
		this.defaultMailServer = defaultMailServer;
	}

	/**
	 * @return Returns the defaultMailServerPassword.
	 */
	public String getDefaultMailServerPassword() {
		return defaultMailServerPassword;
	}

	/**
	 * @param defaultMailServerPassword
	 *            The defaultMailServerPassword to set.
	 */
	public void setDefaultMailServerPassword(String defaultMailServerPassword) {
		this.defaultMailServerPassword = defaultMailServerPassword;
	}

	/**
	 * @return Returns the defaultMailServerUsername.
	 */
	public String getDefaultMailServerUsername() {
		return defaultMailServerUsername;
	}

	/**
	 * @param defaultMailServerUsername
	 *            The defaultMailServerUsername to set.
	 */
	public void setDefaultMailServerUsername(String defaultMailServerUsername) {
		this.defaultMailServerUsername = defaultMailServerUsername;
	}

	/**
	 * @return Returns the emailXAPnlErrorTo.
	 */
	public Properties getEmailXAPnlErrorTo() {
		return emailXAPnlErrorTo;
	}

	/**
	 * @param emailXAPnlErrorTo
	 *            The emailXAPnlErrorTo to set.
	 */
	public void setEmailXAPnlErrorTo(Properties emailXAPnlErrorTo) {
		this.emailXAPnlErrorTo = emailXAPnlErrorTo;
	}

	/**
	 * @return Returns the emiratesDefaultAgentCode.
	 */
	public String getEmiratesDefaultAgentCode() {
		return emiratesDefaultAgentCode;
	}

	/**
	 * @param emiratesDefaultAgentCode
	 *            The emiratesDefaultAgentCode to set.
	 */
	public void setEmiratesDefaultAgentCode(String emiratesDefaultAgentCode) {
		this.emiratesDefaultAgentCode = emiratesDefaultAgentCode;
	}

	/**
	 * @return Returns the popMailServer.
	 */
	public String getPopMailServer() {
		return popMailServer;
	}

	/**
	 * @param popMailServer
	 *            The popMailServer to set.
	 */
	public void setPopMailServer(String popMailServer) {
		this.popMailServer = popMailServer;
	}

	/**
	 * @return Returns the xaDeleteFromMailServer.
	 */
	public String getXaDeleteFromMailServer() {
		return xaDeleteFromMailServer;
	}

	/**
	 * @param xaDeleteFromMailServer
	 *            The xaDeleteFromMailServer to set.
	 */
	public void setXaDeleteFromMailServer(String xaDeleteFromMailServer) {
		this.xaDeleteFromMailServer = xaDeleteFromMailServer;
	}

	/**
	 * @return Returns the xaFetchPnlFromMailServer.
	 */
	public String getXaFetchPnlFromMailServer() {
		return xaFetchPnlFromMailServer;
	}

	/**
	 * @param xaFetchPnlFromMailServer
	 *            The xaFetchPnlFromMailServer to set.
	 */
	public void setXaFetchPnlFromMailServer(String xaFetchPnlFromMailServer) {
		this.xaFetchPnlFromMailServer = xaFetchPnlFromMailServer;
	}

	/**
	 * @return Returns the xaPnlErrorPath.
	 */
	public String getXaPnlErrorPath() {
		return xaPnlErrorPath;
	}

	/**
	 * @param xaPnlErrorPath
	 *            The xaPnlErrorPath to set.
	 */
	public void setXaPnlErrorPath(String xaPnlErrorPath) {
		this.xaPnlErrorPath = xaPnlErrorPath;
	}

	/**
	 * @return Returns the xaPnlMailServerPassword.
	 */
	public String getXaPnlMailServerPassword() {
		return xaPnlMailServerPassword;
	}

	/**
	 * @param xaPnlMailServerPassword
	 *            The xaPnlMailServerPassword to set.
	 */
	public void setXaPnlMailServerPassword(String xaPnlMailServerPassword) {
		this.xaPnlMailServerPassword = xaPnlMailServerPassword;
	}

	/**
	 * @return Returns the xaPnlMailServerUsername.
	 */
	public String getXaPnlMailServerUsername() {
		return xaPnlMailServerUsername;
	}

	/**
	 * @param xaPnlMailServerUsername
	 *            The xaPnlMailServerUsername to set.
	 */
	public void setXaPnlMailServerUsername(String xaPnlMailServerUsername) {
		this.xaPnlMailServerUsername = xaPnlMailServerUsername;
	}

	/**
	 * @return Returns the xaPnlParsedPath.
	 */
	public String getXaPnlParsedPath() {
		return xaPnlParsedPath;
	}

	/**
	 * @param xaPnlParsedPath
	 *            The xaPnlParsedPath to set.
	 */
	public void setXaPnlParsedPath(String xaPnlParsedPath) {
		this.xaPnlParsedPath = xaPnlParsedPath;
	}

	/**
	 * @return Returns the xaPnlProcessPath.
	 */
	public String getXaPnlProcessPath() {
		return xaPnlProcessPath;
	}

	/**
	 * @param xaPnlProcessPath
	 *            The xaPnlProcessPath to set.
	 */
	public void setXaPnlProcessPath(String xaPnlProcessPath) {
		this.xaPnlProcessPath = xaPnlProcessPath;
	}

	/**
	 * @return Returns the xaPnlRetryLimit.
	 */
	public String getXaPnlRetryLimit() {
		return xaPnlRetryLimit;
	}

	/**
	 * @param xaPnlRetryLimit
	 *            The xaPnlRetryLimit to set.
	 */
	public void setXaPnlRetryLimit(String xaPnlRetryLimit) {
		this.xaPnlRetryLimit = xaPnlRetryLimit;
	}

	/**
	 * @return Returns the xaFlightNumberMap.
	 */
	public Map getXaFlightNumberMap() {
		return xaFlightNumberMap;
	}

	/**
	 * @param xaFlightNumberMap
	 *            The xaFlightNumberMap to set.
	 */
	public void setXaFlightNumberMap(Map xaFlightNumberMap) {
		this.xaFlightNumberMap = xaFlightNumberMap;
	}

	public String getEtlProcessPath() {
		return etlProcessPath;
	}

	public void setEtlProcessPath(String etlProcessPath) {
		this.etlProcessPath = etlProcessPath;
	}

	public String getEtlParsedPath() {
		return etlParsedPath;
	}

	public void setEtlParsedPath(String etlParsedPath) {
		this.etlParsedPath = etlParsedPath;
	}

	public String getEtlErrorPath() {
		return etlErrorPath;
	}

	public void setEtlErrorPath(String etlErrorPath) {
		this.etlErrorPath = etlErrorPath;
	}

	public String getEtlPartialDownloadPath() {
		return etlPartialDownloadPath;
	}

	public void setEtlPartialDownloadPath(String etlPartialDownloadPath) {
		this.etlPartialDownloadPath = etlPartialDownloadPath;
	}

	public String getEtlMailServerUsername() {
		return etlMailServerUsername;
	}

	public void setEtlMailServerUsername(String etlMailServerUsername) {
		this.etlMailServerUsername = etlMailServerUsername;
	}

	public String getEtlMailServerPassword() {
		return etlMailServerPassword;
	}

	public void setEtlMailServerPassword(String etlMailServerPassword) {
		this.etlMailServerPassword = etlMailServerPassword;
	}

	public String getEtlFetchFromMailServer() {
		return etlFetchFromMailServer;
	}

	public void setEtlFetchFromMailServer(String etlFetchFromMailServer) {
		this.etlFetchFromMailServer = etlFetchFromMailServer;
	}

	public String getEtlDeleteFromMailServer() {
		return etlDeleteFromMailServer;
	}

	public void setEtlDeleteFromMailServer(String etlDeleteFromMailServer) {
		this.etlDeleteFromMailServer = etlDeleteFromMailServer;
	}

	public Properties getEmailEtlErrorTo() {
		return emailEtlErrorTo;
	}

	public void setEmailEtlErrorTo(Properties emailEtlErrorTo) {
		this.emailEtlErrorTo = emailEtlErrorTo;
	}

	public String getPrlProcessPath() {
		return prlProcessPath;
	}

	public void setPrlProcessPath(String prlProcessPath) {
		this.prlProcessPath = prlProcessPath;
	}

	public String getPrlParsedPath() {
		return prlParsedPath;
	}

	public void setPrlParsedPath(String prlParsedPath) {
		this.prlParsedPath = prlParsedPath;
	}

	public String getPrlErrorPath() {
		return prlErrorPath;
	}

	public void setPrlErrorPath(String prlErrorPath) {
		this.prlErrorPath = prlErrorPath;
	}
	
	public String getPrlPartialDownloadPath() {
		return prlPartialDownloadPath;
	}

	public void setPrlPartialDownloadPath(String prlPartialDownloadPath) {
		this.prlPartialDownloadPath = prlPartialDownloadPath;
	}

	public String getPrlMailServerUsername() {
		return prlMailServerUsername;
	}

	public void setPrlMailServerUsername(String prlMailServerUsername) {
		this.prlMailServerUsername = prlMailServerUsername;
	}

	public String getPrlMailServerPassword() {
		return prlMailServerPassword;
	}

	public void setPrlMailServerPassword(String prlMailServerPassword) {
		this.prlMailServerPassword = prlMailServerPassword;
	}

	public String getPrlFetchFromMailServer() {
		return prlFetchFromMailServer;
	}

	public void setPrlFetchFromMailServer(String prlFetchFromMailServer) {
		this.prlFetchFromMailServer = prlFetchFromMailServer;
	}

	public String getPrlDeleteFromMailServer() {
		return prlDeleteFromMailServer;
	}

	public void setPrlDeleteFromMailServer(String prlDeleteFromMailServer) {
		this.prlDeleteFromMailServer = prlDeleteFromMailServer;
	}

	public String getPrlPopMailServer() {
		return prlPopMailServer;
	}

	public void setPrlPopMailServer(String prlPopMailServer) {
		this.prlPopMailServer = prlPopMailServer;
	}

	public String getPfsJaxbXmlPkgPath() {
		return pfsJaxbXmlPkgPath;
	}

	public void setPfsJaxbXmlPkgPath(String pfsJaxbXmlPkgPath) {
		this.pfsJaxbXmlPkgPath = pfsJaxbXmlPkgPath;
	}

	public String getFtpFolder() {
		return ftpFolder;
	}

	public void setFtpFolder(String ftpFolder) {
		this.ftpFolder = ftpFolder;
	}

	public String getFtpIP() {
		return ftpIP;
	}

	public void setFtpIP(String ftpIP) {
		this.ftpIP = ftpIP;
	}

	public String getFtpUser() {
		return ftpUser;
	}

	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public String getCipher() {
		return cipher;
	}

	public void setCipher(String cipher) {
		this.cipher = cipher;
	}

	public String getCipherPassword() {
		return cipherPassword;
	}

	public void setCipherPassword(String cipherPassword) {
		this.cipherPassword = cipherPassword;
	}

	public String getPfsLocalPath() {
		return pfsLocalPath;
	}

	public void setPfsLocalPath(String pfsLocalPath) {
		this.pfsLocalPath = pfsLocalPath;
	}

	public String getPfsFtpErrFrom() {
		return pfsFtpErrFrom;
	}

	public void setPfsFtpErrFrom(String pfsFtpErrFrom) {
		this.pfsFtpErrFrom = pfsFtpErrFrom;
	}

	/**
	 * @return the pfsFtpErrTo
	 */
	public Properties getPfsFtpErrTo() {
		return pfsFtpErrTo;
	}

	/**
	 * @param pfsFtpErrTo
	 *            the pfsFtpErrTo to set
	 */
	public void setPfsFtpErrTo(Properties pfsFtpErrTo) {
		this.pfsFtpErrTo = pfsFtpErrTo;
	}

	public String getTtyMailServer() {
		return ttyMailServer;
	}

	public void setTtyMailServer(String ttyMailServer) {
		this.ttyMailServer = ttyMailServer;
	}

	public String getTtyMailServerUserName() {
		return ttyMailServerUserName;
	}

	public void setTtyMailServerUserName(String ttyMailServerUserName) {
		this.ttyMailServerUserName = ttyMailServerUserName;
	}

	public String getTtyMailServerPassword() {
		return ttyMailServerPassword;
	}

	public void setTtyMailServerPassword(String ttyMailServerPassword) {
		this.ttyMailServerPassword = ttyMailServerPassword;
	}

	public String getSsimMailServerUsername() {
		return ssimMailServerUsername;
	}

	public void setSsimMailServerUsername(String ssimMailServerUsername) {
		this.ssimMailServerUsername = ssimMailServerUsername;
	}

	public String getSsimMailServerPassword() {
		return ssimMailServerPassword;
	}

	public void setSsimMailServerPassword(String ssimMailServerPassword) {
		this.ssimMailServerPassword = ssimMailServerPassword;
	}

	public String getSsimProcessPath() {
		return ssimProcessPath;
	}

	public void setSsimProcessPath(String ssimProcessPath) {
		this.ssimProcessPath = ssimProcessPath;
	}

	public String getSsimParsedPath() {
		return ssimParsedPath;
	}

	public void setSsimParsedPath(String ssimParsedPath) {
		this.ssimParsedPath = ssimParsedPath;
	}

	public String getSsimErrorPath() {
		return ssimErrorPath;
	}

	public void setSsimErrorPath(String ssimErrorPath) {
		this.ssimErrorPath = ssimErrorPath;
	}

	public String getSsimPartialDownloadPath() {
		return ssimPartialDownloadPath;
	}

	public void setSsimPartialDownloadPath(String ssimPartialDownloadPath) {
		this.ssimPartialDownloadPath = ssimPartialDownloadPath;
	}

	public String getSsimFetchFromMailServer() {
		return ssimFetchFromMailServer;
	}

	public void setSsimFetchFromMailServer(String ssimFetchFromMailServer) {
		this.ssimFetchFromMailServer = ssimFetchFromMailServer;
	}

	public String getSsimDeleteFromMailServer() {
		return ssimDeleteFromMailServer;
	}

	public void setSsimDeleteFromMailServer(String ssimDeleteFromMailServer) {
		this.ssimDeleteFromMailServer = ssimDeleteFromMailServer;
	}

}
