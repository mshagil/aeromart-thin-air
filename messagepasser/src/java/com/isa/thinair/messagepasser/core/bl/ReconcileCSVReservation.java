/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2011 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.messagepasser.core.bl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ExternalFareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.model.CSVBookingEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ReconcileCSVConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.CSVBookingDAO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 1). ReconcileCSVReservation process the rows in the "t_csv_booking_temp" table and Reconcile the booking 2).following
 * status were maintained in "t_csv_booking_temp" table in order to identify the status of the booking reconcile process
 * PROCESSSTATUS => N - Not Processed P - Successfully Created E - Error while Processing the PNR record
 * 
 * Note: required data should be provided in required format. in case of any data mismatch records not included in the
 * reconcile process.
 * 
 * */

/**
 * Class to Reconcile the Bookings from the CSV Data, from "t_csv_booking_temp" table
 * 
 * @author M.Rikaz
 */
public class ReconcileCSVReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReconcileCSVReservation.class);
	private static String CONTACT_INFO_SPLITTER = "\\|";

	private static String ADULT_PAX_TYPE = ReservationInternalConstants.PassengerType.ADULT;
	private static String CHILD_PAX_TYPE = ReservationInternalConstants.PassengerType.CHILD;
	private static String INFANT_PAX_TYPE = ReservationInternalConstants.PassengerType.INFANT;

	/** Hold the reservation bd instance **/
	private final ReservationBD reservationBD;

	/** Hold the segment bd instance **/
	private final SegmentBD segmentBD;

	/** Holds the reservation query bd instance */
	private final ReservationQueryBD reservationQueryBD;

	/** Holds the flight segment id */
	private Integer flightSegmentId;

	/** Holds the flight number */
	private String flightNumber;

	/** Holds the departure date */
	private Date departureDate;

	/** Holds the from airport */
	private String fromAirport;

	/** Holds the to airport */
	private String toAirport;

	/** Holds the carrier code */
	private final String carrierCode;

	/** Holds the segment code */
	private String segmentCode;

	/** Holds the csv booking dao instance */
	private final CSVBookingDAO csvBookingDAO;

	private String bookingClass;

	private String cabinClass;

	/** Holds the default agent code */
	private final Agent agent;

	private boolean executeBookingReconcile;

	private boolean executeDepDateFormatting;

	public void setExecuteDepDateFormatting(boolean executeDepDateFormatting) {
		this.executeDepDateFormatting = executeDepDateFormatting;
	}

	public void setExecuteBookingReconcile(boolean executeBookingReconcile) {
		this.executeBookingReconcile = executeBookingReconcile;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	/**
	 * Construct ReconcileReservation
	 * 
	 * @throws ModuleException
	 */
	public ReconcileCSVReservation() throws ModuleException {

		csvBookingDAO = MessagePasserDAOUtils.DAOInstance.CSV_BOOKING_DAO;

		segmentBD = MessagePasserModuleUtils.getSegmentBD();

		reservationBD = MessagePasserModuleUtils.getReservationBD();

		reservationQueryBD = MessagePasserModuleUtils.getReservationQueryBD();

		String agentCode = "";

		agent = MessagePasserModuleUtils.getTravelAgentBD().getAgent(agentCode);

		// assigned booking class should have enough number of inventory to process the data
		this.bookingClass = "ST";
		this.carrierCode = "W5";

	}

	/**
	 * Execute method of the ReconcileCSVReservation
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		// mahan departure date formatting
		if (executeDepDateFormatting) {
			updateDepartureDate();
		}

		if (executeBookingReconcile) {
			// load data
			Collection csvBookingPnrColl = csvBookingDAO.getUniquePnrByStatus(
					ReconcileCSVConstants.BookingProcessStatus.NOT_PROCESSED, 100);

			Iterator<String> csvBookingPnrItr = csvBookingPnrColl.iterator();

			while (csvBookingPnrItr.hasNext()) {
				String pnr = csvBookingPnrItr.next();
				// check for required field data in relevant formats
				// validateParams

				// to identify any inconsistent process-status in entries
				boolean groupPnrStatus = csvBookingDAO.isGroupPnrDataConsistent(pnr);

				Collection csvBookingEntryByPaxColl = csvBookingDAO.getBookingEntriesByPnr(
						ReconcileCSVConstants.BookingProcessStatus.NOT_PROCESSED, pnr);

				if (!groupPnrStatus) {
					this.updateCSVBookingEntries(null, ReconcileCSVConstants.BookingProcessStatus.ERROR_OCCURED,
							"PNR process status are inconsistent", csvBookingEntryByPaxColl, false);
				}

				if (groupPnrStatus && (csvBookingEntryByPaxColl != null) && (csvBookingEntryByPaxColl.size() > 0)) {
					this.prepareToCreateReservation(pnr, csvBookingEntryByPaxColl, null, false);
				}

			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		return response;
	}

	/**
	 * create resrevation for the grouped CSVBookingEntry collection
	 * 
	 * @param externalPNR
	 * @param colPaxCSVBookingEntry
	 * @param myNote
	 * @param reqSplitReservation
	 */
	private void prepareToCreateReservation(String externalPNR, Collection<CSVBookingEntry> colPaxCSVBookingEntry, String myNote,
			boolean reqSplitReservation) throws ModuleException {

		boolean splitReservation = false;

		try {

			Map<String, String> contactInfoMap = getContactsDetails(colPaxCSVBookingEntry);
			Collection<OndFareDTO> colOndFareDTO = null;
			Collection<Integer> flightSegmentIdCol = new ArrayList<Integer>();

			Collection<CSVBookingEntry> colPaxBookEtryTemp = new ArrayList<CSVBookingEntry>();

			colOndFareDTO = getAvailableFlightsSelectedFare(colPaxCSVBookingEntry, flightSegmentIdCol);

			Map<String, Collection<CSVBookingEntry>> paxEntrMap = pnrPaxCsvEntryMapBySegment(colPaxCSVBookingEntry);

			for (String seg : paxEntrMap.keySet()) {
				colPaxBookEtryTemp = paxEntrMap.get(seg);
				break;
			}

			BigDecimal totalAdultQuotaAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalChildQuotaAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalInfantQuotaAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (colOndFareDTO != null && colOndFareDTO.size() > 0) {

				Iterator itColOndFareDTO = colOndFareDTO.iterator();
				OndFareDTO ondFareDTO;

				double[] totalTaxAmounts;
				double[] totalSChgAmounts;

				while (itColOndFareDTO.hasNext()) {
					ondFareDTO = (OndFareDTO) itColOndFareDTO.next();

					// Checking the Adult Fare
					if (ReservationApiUtils.isFareExist(ondFareDTO.getAdultFare())) {
						totalAdultQuotaAmount = AccelAeroCalculator.add(totalAdultQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(ondFareDTO.getAdultFare()));
					}

					// Checking the Child Fare
					if (ReservationApiUtils.isFareExist(ondFareDTO.getChildFare())) {
						totalChildQuotaAmount = AccelAeroCalculator.add(totalChildQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(ondFareDTO.getChildFare()));
					}

					// Checking the Infant Fare
					if (ReservationApiUtils.isFareExist(ondFareDTO.getInfantFare())) {
						totalInfantQuotaAmount = AccelAeroCalculator.add(totalInfantQuotaAmount,
								AccelAeroCalculator.parseBigDecimal(ondFareDTO.getInfantFare()));
					}

					totalTaxAmounts = ondFareDTO.getTotalCharges(ChargeGroups.TAX);
					totalAdultQuotaAmount = AccelAeroCalculator.add(totalAdultQuotaAmount,
							AccelAeroCalculator.parseBigDecimal(totalTaxAmounts[0]));
					totalInfantQuotaAmount = AccelAeroCalculator.add(totalInfantQuotaAmount,
							AccelAeroCalculator.parseBigDecimal(totalTaxAmounts[1]));
					totalChildQuotaAmount = AccelAeroCalculator.add(totalChildQuotaAmount,
							AccelAeroCalculator.parseBigDecimal(totalTaxAmounts[2]));

					totalSChgAmounts = ondFareDTO.getTotalCharges(ChargeGroups.SURCHARGE);
					totalAdultQuotaAmount = AccelAeroCalculator.add(totalAdultQuotaAmount,
							AccelAeroCalculator.parseBigDecimal(totalSChgAmounts[0]));
					totalInfantQuotaAmount = AccelAeroCalculator.add(totalInfantQuotaAmount,
							AccelAeroCalculator.parseBigDecimal(totalSChgAmounts[1]),
							AccelAeroCalculator.parseBigDecimal(ondFareDTO.getTotalCharges(ChargeGroups.INFANT_SURCHARGE)[1]));
					totalChildQuotaAmount = AccelAeroCalculator.add(totalChildQuotaAmount,
							AccelAeroCalculator.parseBigDecimal(totalSChgAmounts[2]));
				}
			}

			// Create the iReservation with fare details.
			IReservation iReservation = new ReservationAssembler(colOndFareDTO, null);

			IPayment iPaymentForAdult = new PaymentAssembler();
			iPaymentForAdult.addCashPayment(totalAdultQuotaAmount, null, null, null, null, null, null);

			IPayment iPaymentForChild = new PaymentAssembler();
			iPaymentForChild.addCashPayment(totalChildQuotaAmount, null, null, null, null, null, null);

			IPayment iPaymentForParent = new PaymentAssembler();
			iPaymentForParent.addCashPayment(AccelAeroCalculator.add(totalAdultQuotaAmount, totalInfantQuotaAmount), null, null,
					null, null);

			// Add Passengers
			this.addPassengers(iReservation, colPaxBookEtryTemp, iPaymentForAdult, iPaymentForChild, iPaymentForParent);

			// Add Segment Information
			Iterator<Integer> flightSegmentIdItr = flightSegmentIdCol.iterator();

			int seq = 1;
			while (flightSegmentIdItr.hasNext()) {
				Integer flighSegId = flightSegmentIdItr.next();
				iReservation.addOutgoingSegment(seq, flighSegId.intValue(), new Integer(seq), null, null, null, null, null);
				seq++;
			}

			ReservationContactInfo contactInfo = new ReservationContactInfo();
			contactInfo.setTitle(contactInfoMap.get("TITLE"));
			contactInfo.setFirstName(contactInfoMap.get("F_NAME"));
			contactInfo.setLastName(contactInfoMap.get("L_NAME"));
			contactInfo.setMobileNo(contactInfoMap.get("MOBILE"));
			contactInfo.setPreferredLanguage(Locale.ENGLISH.toString());

			// Getting the created user note - can use this for credit card payment
			String userNote = this.createUserNote(externalPNR, null, null, myNote);

			iReservation.addContactInfo(userNote, contactInfo, null);
			iReservation.setPnr(externalPNR);

			// Creating a normal XBE reservation
			ServiceResponce serviceResponce = reservationBD.createWebReservation(iReservation, null, null);

			// createCCReservation
			String strPNR = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);

			// Update CSV Booking Entry
			this.updateCSVBookingEntries(strPNR, ReconcileCSVConstants.BookingProcessStatus.PROCESSED, null,
					colPaxCSVBookingEntry, reqSplitReservation);
		} catch (ModuleException me) {
			// If any error occured for this entry making it has error occured
			// Update CSV Booking Entry
			this.updateCSVBookingEntries(null, ReconcileCSVConstants.BookingProcessStatus.ERROR_OCCURED, me.getMessageString(),
					colPaxCSVBookingEntry, reqSplitReservation);

			if (me.getExceptionCode().equals("airreservations.csv.booking.pax.total.price.mismatch")) {
				// split the reservation
				splitReservation = true;
			}

		} catch (Exception e) {
			// If any error occured for this entry making it has error occured
			// Update CSV Booking Entry
			this.updateCSVBookingEntries(null, ReconcileCSVConstants.BookingProcessStatus.ERROR_OCCURED,
					ReconcileCSVReservation.ErrorDescription.CSV_BOOKING_APP_ERROR, colPaxCSVBookingEntry, reqSplitReservation);
		}

		if (splitReservation) {

			try {
				List<Collection<CSVBookingEntry>> resSplittedList = paxFareChargesMapByPaxId(segmentCode, colPaxCSVBookingEntry);

				String note = "Master booking from - " + externalPNR + " - TOTAL PRICE DISPUTE";
				Iterator<Collection<CSVBookingEntry>> splittedCsvCollecList = resSplittedList.iterator();
				while (splittedCsvCollecList.hasNext()) {
					Collection<CSVBookingEntry> spliitedPaxCsvPaxColl = splittedCsvCollecList.next();
					prepareToCreateReservation(null, spliitedPaxCsvPaxColl, note, true);
				}

			} catch (Exception e) {
				throw new ModuleException("airreservations.csv.booking.split.error");
			}

		}
	}

	/**
	 * Create the user note
	 * 
	 * @param externalPNR
	 * @param inBoundInfo
	 * @param outBoundInfo
	 * @return
	 */
	private String createUserNote(String externalPNR, String inBoundInfo, String outBoundInfo, String additionalInfo) {
		String userNote = "";
		inBoundInfo = BeanUtils.nullHandler(inBoundInfo);
		outBoundInfo = BeanUtils.nullHandler(outBoundInfo);

		userNote = "Reconciled reservation from CSV data :: ";

		if (externalPNR != null && externalPNR.equals("")) {
			userNote = userNote + " EXT-PNR (" + externalPNR + ") ";
		}

		// If in bound information exist
		if (!inBoundInfo.equals("")) {
			userNote = userNote + " IN BOUND(" + inBoundInfo + ") ";
		}

		// If out bound information exist
		if (!outBoundInfo.equals("")) {
			userNote = userNote + " OUT BOUND(" + outBoundInfo + ") ";
		}

		if (additionalInfo != null && !additionalInfo.equals("")) {
			userNote = userNote + " ADD INFO (" + additionalInfo + ")";
		}

		return userNote;
	}

	/**
	 * Update CSV Booking Entries Information
	 * 
	 * @param strPNR
	 * @param processStatus
	 * @param errorDesc
	 * @param colCsvBookingEntry
	 * @param splitReservation
	 */
	private void updateCSVBookingEntries(String strPNR, String processStatus, String errorDesc,
			Collection<CSVBookingEntry> colCsvBookingEntry, boolean splitReservation) {

		Iterator csvBookingEntryItr = colCsvBookingEntry.iterator();
		CSVBookingEntry csvBookingEntry;

		while (csvBookingEntryItr.hasNext()) {
			csvBookingEntry = (CSVBookingEntry) csvBookingEntryItr.next();

			if (strPNR != null) {
				csvBookingEntry.setAccelAeroPnr(strPNR);
			}

			if (splitReservation && ReconcileCSVConstants.BookingProcessStatus.PROCESSED.equals(processStatus)) {
				csvBookingEntry.setDescription("Original reservation splitted due to Total Amount Mis-match");
			} else if (!splitReservation && ReconcileCSVConstants.BookingProcessStatus.PROCESSED.equals(processStatus)) {
				csvBookingEntry.setDescription("Booking Confirmed");
			}
			csvBookingEntry.setProcessedStatus(processStatus);
			csvBookingEntry.setErrorDescription(errorDesc);

			csvBookingDAO.saveCSVBookingEntry(csvBookingEntry);
		}
	}

	/**
	 * Add Passengers
	 * 
	 * @param iReservation
	 * @param csvPaxBookingEntryColl
	 * @param iPaymentForAdult
	 * @param iPaymentForParent
	 * @param iPaymentForChild
	 * @throws ModuleException
	 */
	private void addPassengers(IReservation iReservation, Collection<CSVBookingEntry> csvPaxBookingEntryColl,
			IPayment iPaymentForAdult, IPayment iPaymentForChild, IPayment iPaymentForParent) throws ModuleException {
		Iterator csvPaxBookingEntryItr = csvPaxBookingEntryColl.iterator();
		CSVBookingEntry csvPaxBookEntry;
		String passengerType;
		int i = 1;

		while (csvPaxBookingEntryItr.hasNext()) {
			csvPaxBookEntry = (CSVBookingEntry) csvPaxBookingEntryItr.next();

			passengerType = getPassengerType(csvPaxBookEntry);

			SegmentSSRAssembler dummySSRs = new SegmentSSRAssembler();

			Map<String, String> paxNameMap = formatPaxNameInfo(csvPaxBookEntry.getTitle(), csvPaxBookEntry.getFirstName(),
					csvPaxBookEntry.getLastName(), true, true);
			PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();

			if (passengerType.equals(PassengerTypes.ADULT)) {

				CSVBookingEntry csvInfantBookEntry = getInfantRecord(csvPaxBookEntry.getPaxId(), csvPaxBookingEntryColl);

				if (csvInfantBookEntry != null) {
					// For Parent
					Map<String, String> infantNameMap = formatPaxNameInfo(csvInfantBookEntry.getTitle(),
							csvInfantBookEntry.getFirstName(), csvInfantBookEntry.getLastName(), true, true);

					iReservation.addParent(paxNameMap.get("F_NAME"), paxNameMap.get("L_NAME"), paxNameMap.get("TITLE"), null,
							null, i, i + 1, paxAdditionalInfoDTO, null, iPaymentForParent, dummySSRs, null, null,
							null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

					iReservation.addInfant(infantNameMap.get("F_NAME"), infantNameMap.get("L_NAME"), infantNameMap.get("TITLE"),
							csvInfantBookEntry.getDateOfBirth(), null, i + 1, i, paxAdditionalInfoDTO, null, null, dummySSRs,
							null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

					i = i + 2;
				} else {
					// For Adult
					iReservation.addSingle(paxNameMap.get("F_NAME"), paxNameMap.get("L_NAME"), paxNameMap.get("TITLE"), null,
							null, i, paxAdditionalInfoDTO, null, iPaymentForAdult, dummySSRs, null, null, null,
							null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

					i = i + 1;
				}

			} else if (passengerType.equals(PassengerTypes.CHILD)) {
				// For Child
				iReservation.addChild(paxNameMap.get("F_NAME"), paxNameMap.get("L_NAME"), paxNameMap.get("TITLE"), null, null, i,
						paxAdditionalInfoDTO, null, iPaymentForChild, dummySSRs, null, null, null, null,
						CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

				i = i + 1;
			}

		}
	}

	private int[] getPassengerCounts(Collection<CSVBookingEntry> collCSVPaxBookingEntry) throws ModuleException {
		Iterator<CSVBookingEntry> csvPaxBookingEntryItr = collCSVPaxBookingEntry.iterator();
		CSVBookingEntry csvBookingEntry;
		int totalNoOfAdults = 0;
		int totalNoOfChildren = 0;
		int totalNoOfInfants = 0;
		String passengerType;

		while (csvPaxBookingEntryItr.hasNext()) {
			csvBookingEntry = csvPaxBookingEntryItr.next();

			passengerType = csvBookingEntry.getPaxType();

			if (ADULT_PAX_TYPE.equals(passengerType)) {
				totalNoOfAdults = totalNoOfAdults + 1;

			} else if (CHILD_PAX_TYPE.equals(passengerType)) {
				totalNoOfChildren = totalNoOfChildren + 1;

			} else if (INFANT_PAX_TYPE.equals(passengerType)) {
				totalNoOfInfants = totalNoOfInfants + 1;
			}
		}
		int paxCountTotal = totalNoOfAdults + totalNoOfChildren + totalNoOfInfants;

		if (paxCountTotal == 0) {
			throw new ModuleException("airreservations.csv.booking.pax.count.invalid");
		}

		return new int[] { totalNoOfAdults, totalNoOfChildren, totalNoOfInfants };
	}

	/**
	 * Locate the passenger type
	 * 
	 * @param csvBookingEntry
	 * @return
	 * @throws ModuleException
	 */
	private static String getPassengerType(CSVBookingEntry csvBookingEntry) throws ModuleException {

		if (ADULT_PAX_TYPE.equals(csvBookingEntry.getPaxType())) {
			return PassengerTypes.ADULT;
		} else if (CHILD_PAX_TYPE.equals(csvBookingEntry.getPaxType())) {
			return PassengerTypes.CHILD;
		} else if (INFANT_PAX_TYPE.equals(csvBookingEntry.getPaxType())) {
			return PassengerTypes.INFANT;
		} else {
			throw new ModuleException("airreservations.xaPnl.invalidPassengerType");
		}
	}

	/** Donotes error descriptions */
	private static interface ErrorDescription {
		public static final String CSV_BOOKING_APP_ERROR = new ModuleException("airreservations.arg.appErrorWhileReconcile")
				.getMessageString();
	}

	/** Holds the external air line specific passenger types */
	private static interface PassengerTypes {
		/** Holds the adult type */
		public static final String ADULT = "A";
		/** Holds the child type */
		public static final String CHILD = "C";
		/** Holds the infant type */
		public static final String INFANT = "I";
	}

	/**
	 * Will validate records and derive flight segment information
	 * 
	 * @param colValues
	 * @throws ModuleException
	 */
	private void validateRecordsAndDeriveFlightSegmentInfo(Collection<CSVBookingEntry> colValues,
			Collection<Integer> flightSegmentIdCol) throws ModuleException {

		Collection colFlightNumbers = new HashSet();
		Collection colFlightDates = new HashSet();
		Collection colDepatureStations = new HashSet();
		Collection colArrivalStations = new HashSet();
		Collection colSegmentCodes = new HashSet();

		Collection parentIdMixCol = new HashSet(); // PARENTID_SEGMENT

		Iterator<CSVBookingEntry> itr = colValues.iterator();

		while (itr.hasNext()) {
			CSVBookingEntry csvBookingEntry = itr.next();

			// validation
			String paxType = csvBookingEntry.getPaxType();
			Integer parentPaxId = csvBookingEntry.getParentPaxId();
			String segmentCode = csvBookingEntry.getSegmentCode();
			String segmentStatus = csvBookingEntry.getSegmentStatus();
			String flightNo = csvBookingEntry.getFlightNo();

			String parentIdMix = null;

			if (StringUtil.isNullOrEmpty(segmentCode)) {
				throw new ModuleException("airreservations.csv.booking.seg.code.empty");
			}

			if (StringUtil.isNullOrEmpty(flightNo)) {
				throw new ModuleException("airreservations.csv.booking.flight.no.empty");
			}

			if (StringUtil.isNullOrEmpty(paxType)) {
				throw new ModuleException("airreservations.csv.booking.pax.type.empty");
			}

			if ((INFANT_PAX_TYPE.equals(paxType)) && (parentPaxId == null || parentPaxId < 1)) {
				throw new ModuleException("airreservations.csv.booking.parent.pax.id.empty");
			}

			if (INFANT_PAX_TYPE.equals(paxType)) {

				parentIdMix = parentPaxId + "$" + segmentCode + "$" + segmentStatus + "$" + flightNo;
				if (parentIdMixCol.size() > 0 && parentIdMixCol.contains(parentIdMix)) {
					throw new ModuleException("airreservations.csv.booking.parent.already.defined");
				}

				if (getParentRecord(parentPaxId, colValues) == null) {
					throw new ModuleException("airreservations.csv.booking.parent.record.invalid");
				}

				parentIdMixCol.add(parentIdMix);
			}

			colFlightNumbers.add(flightNo);

			colFlightDates.add(csvBookingEntry.getDepartureDateTimeZulu());

			colSegmentCodes.add(csvBookingEntry.getSegmentCode());

			String airports[] = getOnDAirports(segmentCode);

			if (airports.length < 2) {
				throw new ModuleException("airreservations.csv.booking.seg.code.empty");
			}

			colDepatureStations.add(airports[0]);

			colArrivalStations.add(airports[airports.length - 1]);

		}

		if (colFlightNumbers.size() > 1 || colFlightDates.size() > 1 || colDepatureStations.size() > 1
				|| colArrivalStations.size() > 1) {
			throw new ModuleException("airreservations.csv.booking.mul.seg.det.found");
		}

		flightNumber = (String) BeanUtils.getFirstElement(colFlightNumbers);
		departureDate = (Date) BeanUtils.getFirstElement(colFlightDates);
		fromAirport = (String) BeanUtils.getFirstElement(colDepatureStations);
		toAirport = (String) BeanUtils.getFirstElement(colArrivalStations);
		segmentCode = (String) BeanUtils.getFirstElement(colSegmentCodes);

		flightSegmentId = segmentBD.getFlightSegmentIdBySegmentCode(flightNumber, departureDate, fromAirport, toAirport,
				segmentCode);
		flightSegmentIdCol.add(flightSegmentId);
	}

	private String[] getOnDAirports(String segmentCode) throws ModuleException {

		String ondArray[] = new String[2];

		if (segmentCode == null) {
			throw new ModuleException("airreservations.csv.booking.seg.code.empty");
		}

		String allAirportsArray[] = segmentCode.split("\\/");
		ondArray[0] = allAirportsArray[0];
		ondArray[1] = allAirportsArray[allAirportsArray.length - 1];

		return ondArray;
	}

	private Map<String, String> getContactsDetails(Collection<CSVBookingEntry> colValues) throws ModuleException {
		Map<String, String> contactInfoMap = new HashMap<String, String>();

		List<String> validTitle = new ArrayList<String>();
		validTitle.add("MR");
		validTitle.add("MS");
		validTitle.add("CHILD");
		validTitle.add("MRS");
		validTitle.add("MSTR");
		validTitle.add("MISS");
		validTitle.add("DR");
		validTitle.add("CAPT");

		try {
			Iterator<CSVBookingEntry> csvBookingItr = colValues.iterator();

			while (csvBookingItr.hasNext()) {
				CSVBookingEntry csvBookingEntry = csvBookingItr.next();
				String contactPersonStr = csvBookingEntry.getContactPerson();
				String mobilePhone = formatPhoneNo(csvBookingEntry.getMobileNo());
				String landPhone = formatPhoneNo(csvBookingEntry.getLandPhoneNo());

				if (!StringUtil.isNullOrEmpty(contactPersonStr)) {
					String contactPersArray[] = contactPersonStr.split(CONTACT_INFO_SPLITTER);
					if (contactPersArray != null && contactPersArray.length > 2) {
						contactInfoMap.put("F_NAME", contactPersArray[0]);
						contactInfoMap.put("L_NAME", contactPersArray[1]);
						contactInfoMap.put("TITLE", contactPersArray[2].toUpperCase());
					} else if (contactPersArray != null && contactPersArray.length > 1) {
						contactInfoMap.put("F_NAME", contactPersArray[0]);
						if (validTitle.contains(contactPersArray[1].toUpperCase())) {
							contactInfoMap.put("L_NAME", contactPersArray[0]);
							contactInfoMap.put("TITLE", contactPersArray[1].toUpperCase());
						} else {
							contactInfoMap.put("L_NAME", contactPersArray[1]);
							contactInfoMap.put("TITLE", "MR");
						}
					} else if (contactPersArray != null && contactPersArray.length > 0) {
						contactInfoMap.put("F_NAME", contactPersArray[0]);
						contactInfoMap.put("L_NAME", contactPersArray[0]);
						contactInfoMap.put("TITLE", "MR");
					}
					contactInfoMap.put("MOBILE", mobilePhone);
					contactInfoMap.put("L_PHONE", landPhone);

					break;
				} else if (ADULT_PAX_TYPE.equals(csvBookingEntry.getPaxType())) {

					Map<String, String> paxNameMap = formatPaxNameInfo(csvBookingEntry.getTitle(),
							csvBookingEntry.getFirstName(), csvBookingEntry.getLastName(), true, true);

					contactInfoMap.put("F_NAME", paxNameMap.get("F_NAME"));
					contactInfoMap.put("L_NAME", paxNameMap.get("L_NAME"));
					contactInfoMap.put("TITLE", paxNameMap.get("TITLE"));

					contactInfoMap.put("MOBILE", mobilePhone);
					contactInfoMap.put("L_PHONE", landPhone);

					break;
				}

			}

		} catch (Exception e) {
			throw new ModuleException("airreservations.csv.booking.contact.per.error");
		}
		return contactInfoMap;
	}

	private CSVBookingEntry getInfantRecord(int paxId, Collection<CSVBookingEntry> paxCsvColl) {

		Iterator<CSVBookingEntry> paxCsvItr = paxCsvColl.iterator();

		while (paxCsvItr.hasNext()) {
			CSVBookingEntry csvBookingEntry = paxCsvItr.next();

			if (INFANT_PAX_TYPE.equals(csvBookingEntry.getPaxType()) && paxId == csvBookingEntry.getParentPaxId().intValue()) {
				return csvBookingEntry;
			}

		}

		return null;
	}

	private CSVBookingEntry getParentRecord(int parentPaxId, Collection<CSVBookingEntry> paxCsvColl) {

		Iterator<CSVBookingEntry> paxCsvItr = paxCsvColl.iterator();

		while (paxCsvItr.hasNext()) {
			CSVBookingEntry csvBookingEntry = paxCsvItr.next();

			if (ADULT_PAX_TYPE.equals(csvBookingEntry.getPaxType()) && parentPaxId == csvBookingEntry.getPaxId().intValue()) {
				return csvBookingEntry;
			}

		}

		return null;
	}

	// PAX_TYPE -(AD,CH,IN)
	// |_ _ _ FARE
	// |_ _ _ TAX
	// |_ _ _ SUR
	// |_ _ _ OTHER
	// |_ _ _ TOTAL
	private Map<String, Map<String, BigDecimal>> paxFareChargesMapBySegment(String segmentCode,
			Collection<CSVBookingEntry> paxCsvColl) throws ModuleException {
		Map<String, Map<String, BigDecimal>> paxFareChargesMap = new HashMap<String, Map<String, BigDecimal>>();

		try {
			Iterator<CSVBookingEntry> paxCsvItr = paxCsvColl.iterator();

			while (paxCsvItr.hasNext()) {
				CSVBookingEntry paxCsvBookingEntry = paxCsvItr.next();

				if (segmentCode != null && segmentCode.equals(paxCsvBookingEntry.getSegmentCode())) {
					// check pax total for same pax type
					// - if total price mismatch then split the reservation
					String paxType = paxCsvBookingEntry.getPaxType();
					BigDecimal fareAmount = paxCsvBookingEntry.getFareAmount();
					BigDecimal taxAmount = paxCsvBookingEntry.getTaxAmount();
					BigDecimal surCharges = paxCsvBookingEntry.getSurCharges();
					BigDecimal totalAmount = AccelAeroCalculator.add(fareAmount, taxAmount, surCharges);

					if (!paxFareChargesMap.containsKey(paxType)) {
						Map<String, BigDecimal> fareInfoMap = new HashMap<String, BigDecimal>();

						fareInfoMap.put("FARE", fareAmount);
						fareInfoMap.put("TAX", taxAmount);
						fareInfoMap.put("SUR", surCharges);
						fareInfoMap.put("TOTAL", totalAmount);

						paxFareChargesMap.put(paxType, fareInfoMap);
					} else {
						// check price mismatch
						if (paxFareChargesMap.containsKey(paxType)) {
							Map<String, BigDecimal> fareInfoMap = paxFareChargesMap.get(paxType);

							if (totalAmount.doubleValue() != fareInfoMap.get("TOTAL").doubleValue()) {
								// throws exception
								log.info("Total price mismatch in " + paxType);
								throw new ModuleException("airreservations.csv.booking.pax.total.price.mismatch");
							}

						}

					}

				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			throw new ModuleException("airreservations.csv.booking.fare.charges.error");
		}

		return paxFareChargesMap;
	}

	// only adult & child will be processed
	private List<Collection<CSVBookingEntry>>
			paxFareChargesMapByPaxId(String segmentCode, Collection<CSVBookingEntry> paxCsvColl) throws ModuleException {

		Iterator<CSVBookingEntry> paxCsvItr = paxCsvColl.iterator();
		List<Collection<CSVBookingEntry>> resSplittedList = new ArrayList<Collection<CSVBookingEntry>>();

		while (paxCsvItr.hasNext()) {
			CSVBookingEntry paxCsvBookingEntry = paxCsvItr.next();

			if (segmentCode != null && segmentCode.equals(paxCsvBookingEntry.getSegmentCode())
					&& !INFANT_PAX_TYPE.equals(paxCsvBookingEntry.getPaxType())) {

				Collection<CSVBookingEntry> groupedCsvCollection = new ArrayList<CSVBookingEntry>();

				CSVBookingEntry csvInfantBookEntry = null;
				if (ADULT_PAX_TYPE.equals(paxCsvBookingEntry.getPaxType())) {
					Integer paxId = paxCsvBookingEntry.getPaxId();
					csvInfantBookEntry = getInfantRecord(paxId, paxCsvColl);
				}

				groupedCsvCollection.add(paxCsvBookingEntry);

				if (csvInfantBookEntry != null) {
					groupedCsvCollection.add(csvInfantBookEntry);
				}

				resSplittedList.add(groupedCsvCollection);

			}

		}

		return resSplittedList;
	}

	/**
	 * Overwrites the OndFareDTO Collection Charges with PAX defined charges in the CSV
	 * 
	 * */
	private void injectWithBookingCharges(Collection<OndFareDTO> colOndFareDTO,
			Map<String, Map<String, BigDecimal>> paxFareChargesMap) {
		if (colOndFareDTO != null && colOndFareDTO.size() > 0) {

			Iterator itColOndFareDTO = colOndFareDTO.iterator();
			OndFareDTO ondFareDTO;

			BigDecimal adultTax = new BigDecimal(0);
			BigDecimal childTax = new BigDecimal(0);
			BigDecimal infantTax = new BigDecimal(0);

			BigDecimal adultSurCharges = new BigDecimal(0);
			BigDecimal childSurCharges = new BigDecimal(0);
			BigDecimal infantSurCharges = new BigDecimal(0);

			if (paxFareChargesMap.get(ADULT_PAX_TYPE) != null) {
				adultTax = paxFareChargesMap.get(ADULT_PAX_TYPE).get("TAX");
				adultSurCharges = paxFareChargesMap.get(ADULT_PAX_TYPE).get("SUR");
			}

			if (paxFareChargesMap.get(CHILD_PAX_TYPE) != null) {
				childTax = paxFareChargesMap.get(CHILD_PAX_TYPE).get("TAX");
				childSurCharges = paxFareChargesMap.get(CHILD_PAX_TYPE).get("SUR");
			}

			if (paxFareChargesMap.get(INFANT_PAX_TYPE) != null) {
				infantTax = paxFareChargesMap.get(INFANT_PAX_TYPE).get("TAX");
				infantSurCharges = paxFareChargesMap.get(INFANT_PAX_TYPE).get("SUR");
			}

			while (itColOndFareDTO.hasNext()) {
				ondFareDTO = (OndFareDTO) itColOndFareDTO.next();

				Collection<QuotedChargeDTO> chargesList = new ArrayList<QuotedChargeDTO>();

				// update SUR charges
				QuotedChargeDTO surChargeOveride = new QuotedChargeDTO();
				surChargeOveride.setChargeGroupCode("SUR");

				// surChargeOveride.setChargeCode("YX-55");
				// surChargeOveride.setChargeRateId(5657);

				surChargeOveride.setChargeCode("LP34");
				surChargeOveride.setChargeRateId(145);

				surChargeOveride.setApplicableTo(5);
				surChargeOveride.setChargeBasis("V");
				surChargeOveride.setChargeCategoryCode("OND");
				surChargeOveride.setChargeValueInPercentage(false);
				surChargeOveride.setChargeValuePercentage(100);
				surChargeOveride.setEffectiveChargeAmount(ADULT_PAX_TYPE, adultSurCharges.doubleValue());
				surChargeOveride.setEffectiveChargeAmount(CHILD_PAX_TYPE, childSurCharges.doubleValue());
				surChargeOveride.setEffectiveChargeAmount(INFANT_PAX_TYPE, infantSurCharges.doubleValue());

				// update TAX charges
				QuotedChargeDTO taxChargeOveride = new QuotedChargeDTO();
				taxChargeOveride.setChargeGroupCode("TAX");

				// taxChargeOveride.setChargeCode("SL");
				// taxChargeOveride.setChargeRateId(5248);

				taxChargeOveride.setChargeCode("LP35");
				taxChargeOveride.setChargeRateId(146);

				taxChargeOveride.setApplicableTo(5);
				taxChargeOveride.setChargeBasis("V");
				taxChargeOveride.setChargeCategoryCode("OND");
				taxChargeOveride.setChargeValueInPercentage(false);
				taxChargeOveride.setChargeValuePercentage(100);
				taxChargeOveride.setEffectiveChargeAmount(ADULT_PAX_TYPE, adultTax.doubleValue());
				taxChargeOveride.setEffectiveChargeAmount(CHILD_PAX_TYPE, childTax.doubleValue());
				taxChargeOveride.setEffectiveChargeAmount(INFANT_PAX_TYPE, infantTax.doubleValue());

				chargesList.add(surChargeOveride);
				chargesList.add(taxChargeOveride);

				ondFareDTO.setAllCharges(chargesList);

			}
		}

	}

	private Collection<OndFareDTO> getAvailableFlightsSelectedFare(Collection<CSVBookingEntry> colPaxCSVBookingEntry,
			Collection<Integer> flightSegmentIdCol) throws ModuleException {

		Collection<OndFareDTO> colOndFareDTORet = new ArrayList<OndFareDTO>();

		Map<String, Collection<CSVBookingEntry>> pnrPaxCsvEntryBySegMap = pnrPaxCsvEntryMapBySegment(colPaxCSVBookingEntry);

		for (String segCodeKey : pnrPaxCsvEntryBySegMap.keySet()) {

			Collection<CSVBookingEntry> csvPaxBookingEntryBySegmentCol = pnrPaxCsvEntryBySegMap.get(segCodeKey);

			this.validateRecordsAndDeriveFlightSegmentInfo(csvPaxBookingEntryBySegmentCol, flightSegmentIdCol);

			AvailableFlightSearchDTOBuilder availableFlightSearchDTO = new AvailableFlightSearchDTOBuilder();
			int[] paxCounts = this.getPassengerCounts(csvPaxBookingEntryBySegmentCol);
			// Map<String, String> contactInfoMap = getContactsDetails(csvPaxBookingEntryBySegmentCol);

			String cabinClassCode = "Y";
			// bookingClass = "N1";
			// bookingClass = "K";

			Map<String, Map<String, BigDecimal>> paxFareChargesMap = paxFareChargesMapBySegment(segmentCode,
					csvPaxBookingEntryBySegmentCol);

			BigDecimal adultTotalAmount = new BigDecimal(0);
			BigDecimal childTotalAmount = new BigDecimal(0);
			BigDecimal infantTotalAmount = new BigDecimal(0);

			if (paxFareChargesMap.get(ADULT_PAX_TYPE) != null) {
				adultTotalAmount = paxFareChargesMap.get(ADULT_PAX_TYPE).get("FARE");
			}

			if (paxFareChargesMap.get(CHILD_PAX_TYPE) != null) {
				childTotalAmount = paxFareChargesMap.get(CHILD_PAX_TYPE).get("FARE");
			}

			if (paxFareChargesMap.get(INFANT_PAX_TYPE) != null) {
				infantTotalAmount = paxFareChargesMap.get(INFANT_PAX_TYPE).get("FARE");
			}

			availableFlightSearchDTO.setAdultCount(paxCounts[0]);
			availableFlightSearchDTO.setChildCount(paxCounts[1]);
			availableFlightSearchDTO.setInfantCount(paxCounts[2]);
			availableFlightSearchDTO.setFromAirport(fromAirport);
			availableFlightSearchDTO.setToAirport(toAirport);
			availableFlightSearchDTO.setDepartureVariance(0);
			availableFlightSearchDTO.setArrivalVariance(0);
			availableFlightSearchDTO.setDepartureDate(departureDate);
			availableFlightSearchDTO.setReturnFlag(false);
			availableFlightSearchDTO.setOndCode(segmentCode);
			availableFlightSearchDTO.setBookingClassCode(bookingClass);
			// availableFlightSearchDTO.setAgentCode(agent.getAgentCode());

			Collection<Integer> colOutBoundSegments = new ArrayList<Integer>();
			colOutBoundSegments.add(flightSegmentId);
			availableFlightSearchDTO.setOutBoundFlights(colOutBoundSegments);

			availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
					.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY));

			// economy
			availableFlightSearchDTO.setCabinClassCode(cabinClassCode);

			ExternalFareSummaryDTO existFareSummaryDTO = null;

			existFareSummaryDTO = new ExternalFareSummaryDTO();
			existFareSummaryDTO.setBookingClassCode(bookingClass);
			existFareSummaryDTO.setAdultFareAmount(adultTotalAmount.doubleValue());
			existFareSummaryDTO.setChildFareAmount(childTotalAmount.doubleValue());
			existFareSummaryDTO.setInfantFareAmount(infantTotalAmount.doubleValue());

			availableFlightSearchDTO.getSearchDTO().setExternalFareSummaryDTO(existFareSummaryDTO);

			AvailableFlightDTO availableFlightDTO = reservationQueryBD.getAvailableFlightsWithAllFares(
					availableFlightSearchDTO.getSearchDTO(), null);

			Collection<OndFareDTO> ondFareDTOCol = null;

			SelectedFlightDTO selectedFlightDTO = availableFlightDTO.getSelectedFlight();

			if (selectedFlightDTO == null) {
				throw new ModuleException("airreservations.csv.booking.selec.flight.det.empty");
			}

			ondFareDTOCol = selectedFlightDTO.getOndFareDTOs(false);

			if (ondFareDTOCol == null || ondFareDTOCol.size() == 0) {
				throw new ModuleException("airreservations.csv.booking.fare.det.empty");
			}

			injectWithBookingCharges(ondFareDTOCol, paxFareChargesMap);
			colOndFareDTORet.addAll(ondFareDTOCol);

		}

		return colOndFareDTORet;
	}

	private Map<String, Collection<CSVBookingEntry>> pnrPaxCsvEntryMapBySegment(
			Collection<CSVBookingEntry> csvBookingEntryByPaxColl) {
		Map<String, Collection<CSVBookingEntry>> pnrPaxCsvEntryBySegMap = new HashMap<String, Collection<CSVBookingEntry>>();

		Iterator<CSVBookingEntry> csvBookingEntryByPaxItr = csvBookingEntryByPaxColl.iterator();

		while (csvBookingEntryByPaxItr.hasNext()) {
			CSVBookingEntry csvPaxBookingEntry = csvBookingEntryByPaxItr.next();

			String segCodeStr = csvPaxBookingEntry.getSegmentCode();
			Date depDate = csvPaxBookingEntry.getDepartureDateTimeZulu();
			String segStatus = csvPaxBookingEntry.getSegmentStatus();
			String flightNo = csvPaxBookingEntry.getFlightNo();

			String key = segCodeStr + "$" + depDate.toString() + "$" + segStatus + "$" + flightNo;

			if (pnrPaxCsvEntryBySegMap.get(key) != null) {
				pnrPaxCsvEntryBySegMap.get(key).add(csvPaxBookingEntry);
			} else {
				pnrPaxCsvEntryBySegMap.put(key, new ArrayList<CSVBookingEntry>());
				pnrPaxCsvEntryBySegMap.get(key).add(csvPaxBookingEntry);
			}

		}

		return pnrPaxCsvEntryBySegMap;
	}

	/**
	 * updates the Departure Date field when Departure date is not set,to execute this operation MA_DATE column should
	 * contain the text value
	 * */
	private void updateDepartureDate() throws ModuleException {

		// load data
		String processedStatus = ReconcileCSVConstants.BookingProcessStatus.PROCESS_DEPARTURE_DATE;

		Collection csvBookingPnrColl = csvBookingDAO.getUniquePnrByStatus(processedStatus, 500);

		Iterator<String> csvBookingPnrItr = csvBookingPnrColl.iterator();

		while (csvBookingPnrItr.hasNext()) {
			String pnr = csvBookingPnrItr.next();

			Collection csvBookingEntryByPaxColl = csvBookingDAO.getBookingEntriesByPnr(processedStatus, pnr);

			Iterator<CSVBookingEntry> csvBookingPnrPaxItr = csvBookingEntryByPaxColl.iterator();

			while (csvBookingPnrPaxItr.hasNext()) {

				CSVBookingEntry csvBookingEntry = csvBookingPnrPaxItr.next();

				Date depDate = csvBookingEntry.getDepartureDateTimeZulu();
				String depDateStr = csvBookingEntry.getDepDateTimeStr();

				if (depDate == null || depDate.equals("")) {
					// (!StringUtil.isNullOrEmpty(depDateStr))
					if (!StringUtil.isNullOrEmpty(depDateStr)) {
						try {
							depDate = formatDeparDateTime(depDateStr, true);
							csvBookingEntry.setDepartureDateTimeZulu(depDate);

							csvBookingEntry.setProcessedStatus(ReconcileCSVConstants.BookingProcessStatus.NOT_PROCESSED);
						} catch (Exception e) {
							// TODO: handle exception
							csvBookingEntry.setProcessedStatus(ReconcileCSVConstants.BookingProcessStatus.ERROR_OCCURED);
							csvBookingEntry.setErrorDescription("Error while formatting departure date string");
						}
					} else {
						csvBookingEntry.setProcessedStatus(ReconcileCSVConstants.BookingProcessStatus.ERROR_OCCURED);
						csvBookingEntry.setErrorDescription("Cannot process empty departure date");
					}

				} else {
					csvBookingEntry.setProcessedStatus(ReconcileCSVConstants.BookingProcessStatus.NOT_PROCESSED);
				}

				csvBookingDAO.saveCSVBookingEntry(csvBookingEntry);

			}

		}
	}

	/**
	 * used to format Departure Date when Departue not according to the expected format
	 * 
	 * @param dateStr
	 *            (Ex:MO24OCT 1055)
	 * @param curDateTime
	 *            (true => compares the formated date with current date time if it's current date is latest then
	 *            increament sets the year to next year)
	 * 
	 * @return formETicketNo
	 * */
	private Date formatDeparDateTime(String dateStr, boolean curDateTime) throws ModuleException {

		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(new Date());

		try {

			String dateArray[] = dateStr.split(" ");

			String pattern_1 = "[\\d]{3}"; // 255
			String pattern_2 = "[\\d]{4}"; // 2255
			String pattern_3 = "[a-zA-Z]{3}[\\d]{2}[a-zA-Z]{3}"; // MON24OCT
			String pattern_4 = "[a-zA-Z]{3}[\\d]{2}[a-zA-Z]{3}[\\d]{2}"; // MON24OCT11
			String pattern_5 = "[a-zA-Z]{2}[\\d]{2}[a-zA-Z]{3}"; // MO24OCT
			String pattern_6 = "[a-zA-Z]{2}[\\d]{2}[a-zA-Z]{3}[\\d]{2}"; // MO24OCT11

			String datePattern = "ddMMMyy HHmm";

			String datePart = "";
			String monthPart = "";
			String yearPart = "" + cal.get(Calendar.YEAR);
			String timepart = "0000";

			if (yearPart.length() > 3) {
				yearPart = yearPart.substring(2, 4);
			}

			StringBuffer sbDateStr = new StringBuffer();

			// Format Date
			if (dateArray != null && dateArray.length > 0 && !dateArray[0].trim().equals("")) {
				dateStr = dateArray[0];
				if (dateStr.matches(pattern_3)) {
					// MON24OCT
					datePart = dateStr.substring(3, 5);
					monthPart = dateStr.substring(5, 8);

					sbDateStr.append(datePart);
					sbDateStr.append(monthPart);
					sbDateStr.append(yearPart);

				} else if (dateStr.matches(pattern_4)) {
					// MON24OCT11
					datePart = dateStr.substring(3, 5);
					monthPart = dateStr.substring(5, 8);
					yearPart = dateStr.substring(8, 10);

					sbDateStr.append(datePart);
					sbDateStr.append(monthPart);
					sbDateStr.append(yearPart);

				} else if (dateStr.matches(pattern_5)) {
					// MO24OCT
					datePart = dateStr.substring(2, 4);
					monthPart = dateStr.substring(4, 7);

					sbDateStr.append(datePart);
					sbDateStr.append(monthPart);
					sbDateStr.append(yearPart);

				} else if (dateStr.matches(pattern_6)) {
					// MO24OCT11
					datePart = dateStr.substring(2, 4);
					monthPart = dateStr.substring(4, 7);
					yearPart = dateStr.substring(7, 9);

					sbDateStr.append(datePart);
					sbDateStr.append(monthPart);
					sbDateStr.append(yearPart);
				}
			}

			if (dateArray != null && dateArray.length == 2 && !dateArray[1].trim().equals("")) {

				if (dateArray[1].matches(pattern_1)) {
					// 255
					timepart = "0" + dateArray[1];

				} else if (dateArray[1].matches(pattern_2)) {
					// 2255
					timepart = dateArray[1];
				}
			}

			sbDateStr.append(" ");
			sbDateStr.append(timepart);

			Date date = null;
			SimpleDateFormat format = new SimpleDateFormat(datePattern);
			try {
				date = format.parse(sbDateStr.toString());
			} catch (ParseException e) {
				throw new ModuleException("airreservations.csv.booking.formt.dep.date.error");
			}

			cal.setTime(date);

			if (curDateTime) {
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
			}

		} catch (Exception e) {
			throw new ModuleException("airreservations.csv.booking.formt.dep.date.error");
		}

		return cal.getTime();
	}

	/**
	 * used to format E-Ticket No
	 * 
	 * @param eTicketNo
	 * 
	 * @return formETicketNo
	 * */
	private String formatEticketNo(String eTicketNo) {

		String formETicketNo = null;
		if (!StringUtil.isNullOrEmpty(eTicketNo)) {
			eTicketNo = eTicketNo.trim();
			if (eTicketNo.length() > 20) {
				formETicketNo = null;
			} else {
				formETicketNo = eTicketNo;
			}
		}
		return formETicketNo;
	}

	/**
	 * used to format Phone No
	 * 
	 * @param phoneNo
	 * 
	 * @return formPhoneNum
	 * */
	private String formatPhoneNo(String phoneNo) {
		String formPhoneNum = "";

		if (!StringUtil.isNullOrEmpty(phoneNo)) {
			phoneNo = phoneNo.trim();
			if (phoneNo.length() < 18) {
				formPhoneNum = phoneNo;
			}
		}

		return formPhoneNum;
	}

	/**
	 * format passenger name info,useful when first name/last name mandatory.
	 * 
	 * @param title
	 * @param fName
	 * @param lName
	 * @param fNameMandatory
	 * @param lNameMandatory
	 * 
	 * @return paxNameMap
	 * */
	private Map<String, String> formatPaxNameInfo(String title, String fName, String lName, boolean fNameMandatory,
			boolean lNameMandatory) {

		Map<String, String> paxNameMap = new HashMap<String, String>();

		if (StringUtil.isNullOrEmpty(title)) {
			title = "MR";
		}

		if (StringUtil.isNullOrEmpty(fName) && StringUtil.isNullOrEmpty(lName)) {
			fName = "T B A";
			lName = "T B A";
		} else if (StringUtil.isNullOrEmpty(fName) && !StringUtil.isNullOrEmpty(lName) && fNameMandatory) {
			fName = lName;
		} else if (!StringUtil.isNullOrEmpty(fName) && StringUtil.isNullOrEmpty(lName) && lNameMandatory) {
			lName = fName;
		} else if (StringUtil.isNullOrEmpty(fName) && !StringUtil.isNullOrEmpty(lName) && !fNameMandatory) {
			fName = "";
		} else if (!StringUtil.isNullOrEmpty(fName) && StringUtil.isNullOrEmpty(lName) && !lNameMandatory) {
			lName = "";
		}

		paxNameMap.put("TITLE", title.toUpperCase());
		paxNameMap.put("F_NAME", fName);
		paxNameMap.put("L_NAME", lName);

		return paxNameMap;
	}
}
