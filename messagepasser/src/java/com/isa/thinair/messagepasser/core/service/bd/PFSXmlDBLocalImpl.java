package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.messagepasser.api.service.PFSXmlBD;

@Local
public interface PFSXmlDBLocalImpl extends PFSXmlBD {

}
