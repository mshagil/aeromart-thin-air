package com.isa.thinair.messagepasser.core.remoting.ejb;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO;
import com.isa.thinair.messagepasser.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.messagepasser.core.bl.ETLPRLFileNamer;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.persistence.dao.PRLDAO;
import com.isa.thinair.messagepasser.core.service.bd.PRLBDImpl;
import com.isa.thinair.messagepasser.core.service.bd.PRLBDLocalImpl;
import com.isa.thinair.messagepasser.core.util.ParserUtil;
import com.isa.thinair.messagepasser.core.utils.CommandNames;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Session bean to provide PRL Processing Service related functionalities
 * 
 * @author Ishan
 */
@Stateless
@RemoteBinding(jndiBinding = "PRLService.remote")
@LocalBinding(jndiBinding = "PRLService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PRLServiceBean extends PlatformBaseSessionBean implements PRLBDImpl, PRLBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PRLServiceBean.class);

	/**
	 * Reconcile PRL Reservations
	 * 
	 * @param prlId
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reconcilePRLReservations(int prlId) throws ModuleException {
		try {
			Command command = (Command) MessagePasserModuleUtils.getBean(CommandNames.RECONCILE_P_R_L_RESERVATION);
			command.setParameter(CommandParamNames.PRL_ID, new Integer(prlId));
			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcilePRLReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcilePRLReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Process PRL message
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce processPRLMessage() throws ModuleException {

		// One Thread at a Time
		synchronized (this) {
			final String generalLogFileName = "PRL-Operation Failure " + new Date().toString();

			Throwable exceptionToLog = null;
			HashMap fileExceptionMap = new HashMap();

			log.info("######### PRL PARSING : INITIALIZING OPERATION  ##########");
			MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
			String prlProcessPath = messagePasserConfig.getPrlProcessPath();
			String prlParsedPath = messagePasserConfig.getPrlParsedPath();
			String prlErrorPath = messagePasserConfig.getPrlErrorPath();
			String prlPartialDownloadPath = messagePasserConfig.getPrlPartialDownloadPath();

			MessageConfigDTO msgCofigDTO = new MessageConfigDTO();
			msgCofigDTO.setPopMailServer(messagePasserConfig.getPrlPopMailServer());
			msgCofigDTO.setMailServerUsername(messagePasserConfig.getPrlMailServerUsername());
			msgCofigDTO.setMailServerPassword(messagePasserConfig.getPrlMailServerPassword());
			msgCofigDTO.setFetchFromMailServer(
					messagePasserConfig.getPrlFetchFromMailServer().equalsIgnoreCase("true") ? true : false);
			msgCofigDTO.setDeleteFromMailServer(
					messagePasserConfig.getPrlDeleteFromMailServer().equalsIgnoreCase("true") ? true : false);
			msgCofigDTO.setProcessPath(prlProcessPath);

			String messageType = MessageTypes.PRL.toString();
			Collection colMsgNames = null;
			try {
				// Downloading the PRL Messages
				ParserUtil.downloadMessagesFromMailServer(msgCofigDTO, messageType, new ETLPRLFileNamer(MessageTypes.PRL));

				colMsgNames = ParserUtil.getValidatedDocuments(prlProcessPath, prlErrorPath, prlPartialDownloadPath, messageType);

				Iterator itColMsgNames = colMsgNames.iterator();
				String strMsgName;
				Integer prlId = null;
				// parse every file one by one - insert to db and get the prl id
				// for processing

				while (itColMsgNames.hasNext()) {
					strMsgName = (String) itColMsgNames.next();

					// need to log the format error in a separate file.
					try {
						prlId = ParserUtil.parseDocument(colMsgNames, strMsgName, MessageTypes.PRL);

					} catch (ModuleException e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(),
								e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					} catch (Exception e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(),
								e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					}

					if (prlId != null) {

						// CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
						// Command command = (Command) MessagePasserModuleUtils
						// .getBean(CommandNames.RECONCILE_P_R_L_RESERVATION);
						// command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
						// command.setParameter(CommandParamNames.PRL_ID, new Integer(prlId));
						//
						// return command.execute();
					}
				}

			} catch (ModuleException ex) {
				this.sessionContext.setRollbackOnly();
				log.error(" ((o)) ModuleException::processPRLMessage ", ex);
				fileExceptionMap.put(generalLogFileName, exceptionToLog = ex);
				throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
			} catch (CommonsDataAccessException cdaex) {
				fileExceptionMap.put(generalLogFileName, exceptionToLog = cdaex);
				this.sessionContext.setRollbackOnly();
				log.error(" ((o)) CommonsDataAccessException::processPRLMessage ", cdaex);
				throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
			} finally {

				if (exceptionToLog != null) {
					// TODO need to have seperate error reporting mechanism for
					// PRL
					log.info("########################## PRL ERRORS EXIST STARTING TO LOG   ##########");
					Iterator iter = fileExceptionMap.keySet().iterator();
					XAPnlLogDTO logDTO;

					while (iter.hasNext()) {
						String fileName = (String) iter.next();
						ParserUtil.moveAndDeleteFiles(prlProcessPath, prlErrorPath, fileName);
						Throwable xceptionToLog = (Throwable) fileExceptionMap.get(fileName);

						logDTO = new XAPnlLogDTO();
						logDTO.setFileName(fileName);
						logDTO.setStackTraceElements(xceptionToLog.getStackTrace());

						if (xceptionToLog instanceof ModuleException) {
							ModuleException e = (ModuleException) xceptionToLog;
							logDTO.setExceptionDescription(e.getMessageString());
						} else {

							logDTO.setExceptionDescription(xceptionToLog.getLocalizedMessage());
						}
						//
						// XAPnlReconcilationLogger.createErrorLog(logDTO);
						// XAMailError.notifyError(logDTO);

					}
					log.info("########################## PRL ERRORS EXIST ... FINISHED  LOGING   ##########");
				}
				ParserUtil.moveAndDeleteAnyRemainingFiles(prlProcessPath, prlParsedPath, colMsgNames);
			}
			log.info("########################## PRL PARSING : OPERATION COMPLETED ##########");

			return new DefaultServiceResponse(true);
		}
	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	private CredentialsDTO getCallerCredentials(TrackInfoDTO trackInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal = this.getUserPrincipal();
		return ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
	}

	/**
	 * Returns PRL entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getPagedPRLData(List criteria, int startIndex, int noRecs, List orderByFieldList) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.PRL_DAO.getPagedPRLData(criteria, startIndex, noRecs,
					orderByFieldList);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPagedPRLData ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns prl parse entries
	 * 
	 * @param prlId
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<PRLPaxEntry> getPRLParseEntries(int prlId) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.PRL_DAO.getPRLParseEntries(prlId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPRLParseEntries ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns prl entry
	 * 
	 * @param prlID
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PRL getPRL(int prlID) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.PRL_DAO.getPRL(prlID);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPRL ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * Return prl parse entries count
	 * 
	 * @param prlId
	 * @param processedStatus
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getPRLParseEntryCount(int prlId, String processedStatus) throws ModuleException {
		try {
			PRLDAO prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;
			return prlDAO.getPRLParseEntriesCount(prlId, processedStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPrlParseEntryCount ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Delete prl entry
	 *
	 * @param prl
	 * @throws ModuleException
	 */
	@Override @TransactionAttribute(TransactionAttributeType.REQUIRED) public void deletePrlEntry(PRL prl)
			throws ModuleException {
		try {
			AuditorBD auditorBD = MessagingModuleUtils.getAuditorBD();
			auditorBD.deleteAuditsByID(prl.getPrlId());

			MessagePasserDAOUtils.DAOInstance.PRL_DAO.deletePrlEntry(prl.getPrlId());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::deletePRLEntry ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * Save PRL parse entry
	 * @param prlParsed
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void savePRLParseEntry(PRLPaxEntry prlParsed) throws ModuleException {
		try {
			PRLDAO prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;
			prlDAO.savePRLPaxEntry(prlParsed);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::savePrlParseEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Delete the prl parse entry and
	 * @param prl
	 * @param prlPaxId
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deletePRLParseEntry(PRL prl, int prlPaxId) throws ModuleException {
		try {
			PRLDAO prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;
			prlDAO.deletePrlPaxEntry(prlPaxId);

			int prlPaxCount = prlDAO.getPRLParseEntriesCount(prl.getPrlId(), null);

			// If all the PAX deleted for the PRL, then Set the PRL status to UNPARSED
			if (prlPaxCount == 0) {
				prl.setProcessedStatus(ParserConstants.PRLStatus.UN_PARSED);
				prlDAO.savePRLEntry(prl);
			} else if (prlDAO.getPRLParseEntriesCount(prl.getPrlId(), ParserConstants.PRLProcessStatus.PROCESSED) == prlPaxCount) {
				// If all the PAX are processed. then set the PRL status to Reconciled
				prl.setProcessedStatus(ParserConstants.PRLStatus.RECONCILE_SUCCESS);
				prlDAO.savePRLEntry(prl);
			}
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::deletePrlParseEntry ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

}
