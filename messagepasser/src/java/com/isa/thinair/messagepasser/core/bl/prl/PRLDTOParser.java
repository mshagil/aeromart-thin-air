package com.isa.thinair.messagepasser.core.bl.prl;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.messagepasser.api.dto.prl.PRLMetaDataDTO;
import com.isa.thinair.messagepasser.api.dto.prl.PRLPAXRecordDTO;
import com.isa.thinair.messagepasser.api.dto.prl.PRLPaxNameListDTO;
import com.isa.thinair.messagepasser.api.dto.prl.PRLRecordDTO;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.persistence.dao.PRLDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * The utility to parse a PNL document
 *
 * @author Ishan
 * @since 1.0 *
 */
public class PRLDTOParser {

	/**
	 * holds the key if tour id was null, which means a continuation to part 2
	 * is possible
	 */

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PRLDTOParser.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(CalendarUtil.PATTERN6);
	private static final SimpleDateFormat dateFormat1 = new SimpleDateFormat(CalendarUtil.PATTERN1);
	private static final String KILOS = "K";
	/**
	 * Returns PNL meta data transfer information
	 *
	 * @param f
	 * @return
	 * @throws ModuleException
	 */
	private static PRLMetaDataDTO getMetaDataDTO(File f) throws ModuleException {
		Throwable exception = null;
		String pnlContent = "";
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(f));
			StringBuffer parserFriendlyText = new StringBuffer();
			StringBuffer originalPNLText = new StringBuffer();
			boolean destinationFound = false;
			boolean prevWasDestinationFound = false;
			boolean isValidPnl = false;
			String continuationText = null;
			boolean isEmptyPRL = false;

			String strHolder = null;
			String fromAddress = "";
			boolean started = false;
			int noOfPax = 0;

			while ((strHolder = buffer.readLine()) != null) {

				// Capturing the from address
				if (strHolder.length() > 5 && strHolder.substring(0, 5).equalsIgnoreCase("From:")) {
					fromAddress = BeanUtils.nullHandler(strHolder.substring(5));
					int start = fromAddress.indexOf("<");
					int end = fromAddress.lastIndexOf(">");

					// This means Email Address is nilindra@abc.com
					if (start == -1 || end == -1) {
						fromAddress = BeanUtils.nullHandler(fromAddress);
						// This mean Email Address is Nilindra
						// <nilindra@abc.com>
					} else {
						fromAddress = BeanUtils.nullHandler(fromAddress.substring(start + 1, end));
					}
				}
				// Processing the PRL part
				else if (strHolder.trim().equalsIgnoreCase("PRL") || started) {
					isValidPnl = true;

					// for continuation from part 1 check the line
					if (prevWasDestinationFound) {
						prevWasDestinationFound = false;
						if (!isANormalPaxRecordLine(strHolder)) {
							continuationText = strHolder;
							continue;
						}

					}

					originalPNLText.append(strHolder + "\n");
					started = true;

					// If it's PNL label

					if (strHolder.equalsIgnoreCase("PRL")) {
						parserFriendlyText.append(strHolder + "\n");
						// Process down the PNL label
					} else if (strHolder.trim().length() > 2 && !isNumber(strHolder.trim().substring(0, 3))) {

						// FIXME: Temporary FIX
						if (strHolder.startsWith("RBD")) {
							continue;
						} else if (strHolder.startsWith("NIL")) {
							isEmptyPRL = true;
							continue;
						} else if (strHolder.charAt(0) == '-') {

							// Omit destination entries without passages
							noOfPax = Integer
									.parseInt(strHolder.trim().substring(4, strHolder.trim().length() - 1));
							if (noOfPax == 0) {
								continue;
							}

							if (!destinationFound) {
								parserFriendlyText.append(strHolder + "\n");
								prevWasDestinationFound = true;
								destinationFound = true;
							} else {
								parserFriendlyText.append(strHolder + "\n");
							}
						} else if (strHolder.charAt(3) == ' ') {
							parserFriendlyText.append(strHolder.substring(0, 7).replaceAll("/", "") + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".L/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);
							parserFriendlyText.append(" " + strHolder + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".R/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".L/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".C/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".I/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".O/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".RN/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".W/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".N/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".O2/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);

							parserFriendlyText.append(" " + strHolder + "\n");
						}

						else {
							// If it's the end of the prl making it as
							// process ended
							if (strHolder.indexOf("ENDPRL") != -1 || strHolder.indexOf("ENDPART") != -1) {
								started = false;

							}

							/**/
							if (isNumber(strHolder.substring(0, 1))) {
								if (isNumber(strHolder.substring(1, 2))) {
									strHolder = new StringBuffer(strHolder).insert(2, " ").toString();
									// parserFriendlyText.insert(2, "
									// ");
								} else {
									strHolder = new StringBuffer(strHolder).insert(1, " ").toString();
									// parserFriendlyText.insert(1, "
									// ");
								}

							}
							parserFriendlyText.append(strHolder + "\n");

							/**/

						}
					}

				}
			}
			buffer.close();
			if (!isValidPnl) {
				throw new ModuleException(new Exception("Invalid IATA Format"), "airreservations.pnl.cannotParsePNL");
			}
			String text = parserFriendlyText.toString();

			pnlContent = text.replaceAll("\n", "<br>");

			PRLMetaDataDTO metaDataDTO = null;
			if (!isEmptyPRL) {
				metaDataDTO = PRLParser.process(text);
				metaDataDTO.setFromAddress(fromAddress);
				metaDataDTO.setPnlContent(originalPNLText.toString());
				metaDataDTO.setTotNumberOfPax(noOfPax);

			}

			return metaDataDTO;

		} catch (IOException e) {
			exception = e;
			log.error(" ERROR ", e);
			throw new ModuleException(e, "airreservations.pnl.cannotLocatePNL");
		} catch (ModuleException e) {
			exception = e;
			log.error(" ERROR ", e);
			throw new ModuleException(e, "airreservations.pnl.cannotParsePNL");
		} finally {

			// if exception occured mail to relavent parties.
			if (exception != null) {

				XAPnlLogDTO pnlLogDTO = new XAPnlLogDTO();
				pnlLogDTO.setExceptionDescription(exception.getMessage());
				pnlLogDTO.setStackTraceElements(exception.getStackTrace());
				pnlLogDTO.setXaPnlContent(pnlContent);
				pnlLogDTO.setFileName(f.getName());
				// XAMailError.notifyError(pnlLogDTO);
			}

		}
	}

	private static boolean isANormalPaxRecordLine(String content) {
		if (isNumber(content.substring(0, 1))) {
			return true;

		}
		return false;
	}

	/**
	 * Find out whether it's a number or not
	 *
	 * @param str
	 * @return
	 */
	private static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * Insert to the prl entries
	 *
	 * @param msgNamesWithPartTwoOrThree
	 * @param strMsgName
	 * @return
	 * @throws ModuleException
	 */
	public static int insertPRLToDatabase(Collection msgNamesWithPartTwoOrThree, String strMsgName)
			throws ModuleException, ParseException {

		MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();

		PRLDAO prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;
		String prlProcessPath = messagePasserConfig.getPrlProcessPath();
		Collection colPaxNameListsDTO = getAllPaxNameListDTOs(prlProcessPath, strMsgName, msgNamesWithPartTwoOrThree);

		PRLPaxNameListDTO paxNameListsDTO;
		PRLPaxEntry prlPaxEntry;

		int prlId = 0;

		for (Iterator iter = colPaxNameListsDTO.iterator(); iter.hasNext();) {
			paxNameListsDTO = (PRLPaxNameListDTO) iter.next();
			prlPaxEntry = new PRLPaxEntry();

			prlPaxEntry.setCabinClassCode(paxNameListsDTO.getCabinClassCode());
			prlPaxEntry.setDepartureAirport(paxNameListsDTO.getDepartureAirportCode());
			prlPaxEntry.setTitle(paxNameListsDTO.getTitle());
			prlPaxEntry.setFirstName(paxNameListsDTO.getFirstName());
			prlPaxEntry.setLastName(paxNameListsDTO.getLastName());
			prlPaxEntry.setFlightNumber(paxNameListsDTO.getFlightNumber());
			prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.NOT_PROCESSED);
			prlPaxEntry.setFlightDate(paxNameListsDTO.getRealFlightDate());
			prlPaxEntry.setArrivalAirport(paxNameListsDTO.getArrivalAirportCode());
			prlPaxEntry.setPnr(paxNameListsDTO.getXAPnr().trim());
			prlPaxEntry.setReceivedDate(paxNameListsDTO.getDateDownloaded());
			prlPaxEntry.setPrlId(new Integer(paxNameListsDTO.getPrlId()));
			prlPaxEntry.setPaxType(paxNameListsDTO.getPaxType());
			prlPaxEntry.setErrorDescription(paxNameListsDTO.getErrorDescription());
			prlPaxEntry.setEticketNumber(paxNameListsDTO.getEticketNumber());
			prlPaxEntry.setCoupNumber(StringUtil.isNullOrEmpty(paxNameListsDTO.getCoupNumber()) ?
					0 :
					new Integer(paxNameListsDTO.getCoupNumber()));
			prlPaxEntry.setInfEticketNumber(paxNameListsDTO.getInfEticketNumber() != null ?
					paxNameListsDTO.getInfEticketNumber().substring(3) :
					null);
			prlPaxEntry.setInfCoupNumber(StringUtil.isNullOrEmpty(paxNameListsDTO.getInfCoupNumber()) ?
					0 :
					new Integer(paxNameListsDTO.getInfCoupNumber()));
			prlPaxEntry.setPaxStatus(paxNameListsDTO.getPaxStatus());
			prlPaxEntry.setPassportNumber(StringUtil.isNullOrEmpty(paxNameListsDTO.getPassportNumber()) ?
					"" :
					paxNameListsDTO.getPassportNumber());
			prlPaxEntry.setPassportExpiryDate(paxNameListsDTO.getPassportExpiryDate());
			prlPaxEntry.setCountryOfIssue(StringUtil.isNullOrEmpty(paxNameListsDTO.getCountryOfIssue()) ?
					"" :
					paxNameListsDTO.getCountryOfIssue());
			prlPaxEntry.setSeatNumber(
					StringUtil.isNullOrEmpty(paxNameListsDTO.getSeatNumber()) ? "" : paxNameListsDTO.getSeatNumber());
			prlPaxEntry.setWeightIndicator(StringUtil.isNullOrEmpty(paxNameListsDTO.getWeightIndicator()) ?
					KILOS :
					paxNameListsDTO.getWeightIndicator());
			prlPaxEntry.setNoOfCheckedBaggage(StringUtil.isNullOrEmpty(paxNameListsDTO.getNoOfCheckedBaggage()) ?
					0 :
					new Integer(paxNameListsDTO.getNoOfCheckedBaggage()));
			prlPaxEntry.setCheckedBaggageWeight(StringUtil.isNullOrEmpty(paxNameListsDTO.getCheckedBaggageWeight()) ?
					0 :
					new Integer(paxNameListsDTO.getCheckedBaggageWeight()));
			prlPaxEntry.setUncheckedBaggageWeight(
					StringUtil.isNullOrEmpty(paxNameListsDTO.getUncheckedBaggageWeight()) ?
							0 :
							new Integer(paxNameListsDTO.getUncheckedBaggageWeight()));
			prlDAO.savePRLPaxEntry(prlPaxEntry);

			prlId = prlPaxEntry.getPrlId().intValue();

		}

		return prlId;
	}

	/**
	 * A Temporary Method
	 *
	 * @TODO: Incorporate Singleton/Lazy Load Design Pattern
	 * @param ssrCode
	 */
	private static String checkToAppendTKNA(String ssrCode) {

		if (ssrCode == null || ssrCode.equals("") || ssrCode.equals(" ")) {
			return ParserConstants.SSR_CODES.CODE_TKNA;
		}

		if (ssrCode.equals(ParserConstants._VIP_CASE)) {
			return ParserConstants.SSR_CODES.CODE_VIP;
		}

		return ssrCode;

	}

	/**
	 * Returns passenger final sales data transfer information
	 *
	 * @param file
	 * @return
	 * @throws ModuleException
	 */
	private static Collection getPRLPaxNameListDTO(File file, Collection msgNamesHavingPartTwoThree, String path)
			throws ModuleException, ParseException {
		PassengerBD passengerBD = MessagePasserModuleUtils.getPassengerBD();
		Collection colPaxTitles = new ArrayList(passengerBD.getPassengerTitleMap().keySet());
		Collection<String> excludingPaxTypes = new ArrayList<>();
		excludingPaxTypes.add(PaxTypeTO.INFANT);
		Map<String, Collection<String>> paxTitleTypes = passengerBD.getPassengerTitleTypes(excludingPaxTypes);
		PRLPaxNameListDTO prlNameListDTO = null;

		PRLRecordDTO empnlRecordDTO = null;
		PRLPAXRecordDTO recordDTO = null;

		PRLMetaDataDTO emPnlMetaDataDTO = getMetaDataDTO(file);
		ArrayList colPaxNameListDTO = new ArrayList();
		if (emPnlMetaDataDTO != null) {
			emPnlMetaDataDTO.getPnlContent();

			// Get the flight error occured status
			boolean flightErrorOccured = isRealDepartureDateErrorOccured(emPnlMetaDataDTO);

			checkIfPRLAlreadyExists(emPnlMetaDataDTO);

			PRL prl = new PRL();
			prl.setCarrierCode(emPnlMetaDataDTO.getCarrierCode());
			prl.setFlightNumber(emPnlMetaDataDTO.getActualFlightNumber());
			prl.setFromAddress(emPnlMetaDataDTO.getFromAddress());
			prl.setNumberOfPassengers(emPnlMetaDataDTO.getTotNumberOfPax());
			prl.setToAirport(emPnlMetaDataDTO.getDestinationAirport());
			prl.setFromAirport(emPnlMetaDataDTO.getBoardingAirport());
			prl.setDateDownloaded(emPnlMetaDataDTO.getDateDownloaded());
			prl.setPrlContent(emPnlMetaDataDTO.getPnlContent());
			prl.setFromAddress(emPnlMetaDataDTO.getFromAddress());
			prl.setDepartureDate(emPnlMetaDataDTO.getRealDepartureDate());
			prl.setPartNumber(Integer.valueOf(emPnlMetaDataDTO.getPartNumber()).intValue());
			// prl.setNumOfAttempts(0);

			if (flightErrorOccured) {
				prl.setProcessedStatus(ParserConstants.PRLProcessStatus.ERROR_OCCURED);
			} else if (prl.getNumberOfPassengers() > 0) {
				prl.setProcessedStatus(ParserConstants.PRLStatus.PARSED);
			} else { // This means empty prl received
				prl.setProcessedStatus(ParserConstants.PRLStatus.UN_PARSED);
			}

			Iterator empnlRecordDTOIterator = emPnlMetaDataDTO.getPassengerRecords().iterator();

			while (empnlRecordDTOIterator.hasNext()) {

				String pnrNumber = "";

				prlNameListDTO = new PRLPaxNameListDTO();
				empnlRecordDTO = (PRLRecordDTO) empnlRecordDTOIterator.next();

				Iterator itRecordDTO = empnlRecordDTO.getRecords().iterator();
				while (itRecordDTO.hasNext()) {

					recordDTO = (PRLPAXRecordDTO) itRecordDTO.next();
					if (recordDTO.getLastName().equals("")) {
						recordDTO = (PRLPAXRecordDTO) itRecordDTO.next();
					}

					pnrNumber = getXAPnrNumber(emPnlMetaDataDTO, recordDTO, msgNamesHavingPartTwoThree, path);

					// Setting the PaxNameListsDTO information
					prlNameListDTO.setXAPnr(pnrNumber);

					prlNameListDTO.setTitle(getPassengerDerivedInfo(recordDTO.getParseTitle(), colPaxTitles, paxTitleTypes)[0]
							.toString());

					Object[] passengerInfo = getPassengerDerivedInfo(recordDTO.getFirstNameWithTitle(), colPaxTitles, paxTitleTypes);
					prlNameListDTO.setFirstName(passengerInfo[1].toString());
					prlNameListDTO.setPaxType(passengerInfo[2].toString());
					prlNameListDTO.setLastName(recordDTO.getLastName());

					String ssrCode = "";
					prlNameListDTO.setSsrCode(ssrCode = checkToAppendTKNA(recordDTO.getSsrCode()));
					if (ssrCode.equals(ParserConstants.SSR_CODES.CODE_TKNA)) {
						prlNameListDTO.setSsrRemarks(pnrNumber);
					} else {
						prlNameListDTO.setSsrRemarks(recordDTO.getSsrText() + " " + pnrNumber);
					}

					prlNameListDTO.setInboundInfo(empnlRecordDTO.getInboundDetails());
					prlNameListDTO.setOutboundInfo(empnlRecordDTO.getOutboundDetails());

					prlNameListDTO.setDay(emPnlMetaDataDTO.getDay());
					prlNameListDTO.setMonth(emPnlMetaDataDTO.getMonth());
					prlNameListDTO.setArrivalAirportCode(empnlRecordDTO.getDestinationAirport());
					prlNameListDTO.setDepartureAirportCode(emPnlMetaDataDTO.getBoardingAirport());
					prlNameListDTO.setFlightNumber(emPnlMetaDataDTO.getActualFlightNumber());
					prlNameListDTO.setDateDownloaded(emPnlMetaDataDTO.getDateDownloaded());
					prlNameListDTO.setRealFlightDate(emPnlMetaDataDTO.getRealDepartureDate());
					prlNameListDTO.setCabinClassCode(empnlRecordDTO.getCcCode());
					prlNameListDTO.setEticketNumber(recordDTO.getEticketNumber());
					prlNameListDTO.setCoupNumber(recordDTO.getCoupNumber());
					prlNameListDTO.setInfEticketNumber(recordDTO.getInfEticketNumber());
					prlNameListDTO.setInfCoupNumber(recordDTO.getInfCoupNumber());
					prlNameListDTO.setPaxStatus(recordDTO.getPaxStatus());
					prlNameListDTO.setPassportNumber(recordDTO.getPassportNumber());
					if (!StringUtil.isNullOrEmpty(recordDTO.getPassportExpiryDate())) {
						Date passportExpDate = dateFormat.parse(recordDTO.getPassportExpiryDate());
						String date = dateFormat1.format(passportExpDate);
						prlNameListDTO.setPassportExpiryDate(dateFormat1.parse(date));
					}
					prlNameListDTO.setCountryOfIssue(recordDTO.getCountryOfIssue());
					prlNameListDTO.setSeatNumber(recordDTO.getSeatNumber());
					prlNameListDTO.setWeightIndicator(recordDTO.getWeightIndicator());
					prlNameListDTO.setNoOfCheckedBaggage(recordDTO.getNoOfCheckedBaggage());
					prlNameListDTO.setCheckedBaggageWeight(recordDTO.getCheckedBaggageWeight());
					prlNameListDTO.setUncheckedBaggageWeight(recordDTO.getUncheckedBaggageWeight());
					colPaxNameListDTO.add(prlNameListDTO);
					if (itRecordDTO.hasNext()) {
						prlNameListDTO = new PRLPaxNameListDTO();
					}
				}
			}

			PRLDAO prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;
			prlDAO.savePRLEntry(prl);

			for (Iterator itColPaxNameListDTO = colPaxNameListDTO.iterator(); itColPaxNameListDTO.hasNext();) {
				prlNameListDTO = (PRLPaxNameListDTO) itColPaxNameListDTO.next();
				prlNameListDTO.setPrlId(prl.getPrlId());
			}
		}
		return colPaxNameListDTO;
	}

	/**
	 * Returns passenger derived information It is assumed that MR, MS, CHILD
	 * codes should be there for any lines. However there could be more codes
	 * too. The first priority will be given for the MR, MS and CHILD codes TODO
	 * Find ways to make it improve
	 *
	 * @param name
	 * @param colTitles
	 * @return
	 */
	private static Object[] getPassengerDerivedInfo(String name, Collection colTitles,
			Map<String, Collection<String>> paxTitleTypes) {
		String title = ReservationInternalConstants.PassengerTitle.MR;
		String paxType = ReservationInternalConstants.PassengerType.ADULT;
		String firstName = name;
		boolean matchFound = false;
		String dbTitle;

		if (name.endsWith("MR")) {
			title = ReservationInternalConstants.PassengerTitle.MR;
			firstName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("MS")) {
			title = ReservationInternalConstants.PassengerTitle.MS;
			firstName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("CHD")) {
			title = ReservationInternalConstants.PassengerTitle.CHILD;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			firstName = name.substring(0, name.length() - 3);
		} else {
			for (Iterator itColTitles = colTitles.iterator(); itColTitles.hasNext();) {
				dbTitle = (String) itColTitles.next();

				if (name.endsWith(dbTitle)) {
					matchFound = true;
					title = dbTitle;
					firstName = name.substring(0, name.length() - dbTitle.length());
					if (paxTitleTypes.get(title) != null) {
						paxType = paxTitleTypes.get(title).iterator().next();
					}
				}
			}
		}

		// This means there is no matching record
		if (!matchFound) {
			if (name.endsWith("MRS")) {
				title = ReservationInternalConstants.PassengerTitle.MS;
				firstName = name.substring(0, name.length() - 3);
			} else if (name.endsWith("MISS")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				firstName = name.substring(0, name.length() - 4);
			} else if (name.endsWith("MSTR")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				firstName = name.substring(0, name.length() - 4);
			}
		}

		return new Object[] { title, firstName, paxType };
	}

	public static void checkIfPRLAlreadyExists(PRLMetaDataDTO dataDTO) throws ModuleException {
		PRLDAO prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;

		if (prlDAO.hasAnEqualPRL(dataDTO.getActualFlightNumber(), dataDTO.getBoardingAirport(), dataDTO.getDepartureDate(),
				Integer.valueOf(dataDTO.getPartNumber()))) {
			throw new ModuleException("airreservations.xapnl.already.exists");
		}
	}

	/**
	 *
	 * @param metaDataDTO
	 * @param recordDTO
	 * @return
	 * @throws ModuleException
	 */
	private static String getXAPnrNumber(PRLMetaDataDTO metaDataDTO, PRLPAXRecordDTO recordDTO,
			Collection msgNamesHavingPartTwoOrThree, String path) throws ModuleException {

		boolean hasPartTwo = false;
		if (msgNamesHavingPartTwoOrThree != null && msgNamesHavingPartTwoOrThree.size() > 1) {
			hasPartTwo = true;
		}

		String xaPnr = recordDTO.getXAPnrNumber();
		if (xaPnr == null) {
			String tourID = recordDTO.getTourId();
			if (tourID == null && hasPartTwo) {
				xaPnr = checkXAPnrInOtherPartsTourIDs(metaDataDTO, ParserConstants.MAPKEY_CONT,
						msgNamesHavingPartTwoOrThree, path);
				return xaPnr;
			} else {
				xaPnr = metaDataDTO.getXAPnrNumber(tourID);
			}
		}
		if (xaPnr == null) {

			log.error("############## XA PNR Number Was Null Going to Search in partTwo or Three###########");
			log.error("########## Tour Id: " + recordDTO.getTourId() + " ###########");
			log.error("########## Flight Number :" + metaDataDTO.getActualFlightNumber());
			log.error("########## Boarding Airport :" + metaDataDTO.getBoardingAirport());
			log.error("########## Destination Airport :" + metaDataDTO.getDestinationAirport());
			log.error("########## Departure Date :" + metaDataDTO.getDepartureDate());
			if (hasPartTwo) {
				xaPnr = checkXAPnrInOtherPartsTourIDs(metaDataDTO, recordDTO.getTourId(), msgNamesHavingPartTwoOrThree,
						path);
			}
			// throw new ModuleException("airreservations.xaPnl.xaPnrNull");
		}
		if (xaPnr == null || xaPnr.equals("")) {
			xaPnr = " ";
			recordDTO.setErrorDescription("searching for tour id in next part caused error");
		}
		return xaPnr;
	}

	private static String checkXAPnrInOtherPartsTourIDs(PRLMetaDataDTO metaDataDTO, String tourId,
			Collection msgNamesHavingPartTwoOrThree, String path) throws ModuleException {

		if (msgNamesHavingPartTwoOrThree == null || msgNamesHavingPartTwoOrThree.size() <= 1) {
			return null;
		}

		try {
			Iterator iterMsgs = msgNamesHavingPartTwoOrThree.iterator();
			int currentPartNumber = Integer.valueOf(metaDataDTO.getPartNumber()).intValue();

			while (iterMsgs.hasNext()) {
				File f = new File(path, iterMsgs.next().toString());

				PRLMetaDataDTO dataDTO = PRLDTOParser.getMetaDataDTO(f);
				if (dataDTO != null) {

					if (dataDTO.getFlightNumber().equals(metaDataDTO.getFlightNumber())
							&& (currentPartNumber != Integer.valueOf(dataDTO.getPartNumber()).intValue()
									&& metaDataDTO.getBoardingAirport().equals(dataDTO.getBoardingAirport()) && metaDataDTO
									.getDepartureDate().equals(dataDTO.getDepartureDate()))) {

						return dataDTO.getXAPnrNumber(tourId);

					}
				}
			}
		} catch (Exception e) {
			throw new ModuleException("pnldtoparser.searchtourid.error");
		}

		return null;
	}

	/**
	 * Finds out if the real departure date error occured or not
	 *
	 * @param metaDataDTO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isRealDepartureDateErrorOccured(PRLMetaDataDTO metaDataDTO) throws ModuleException {
		boolean errorOccured = false;
		FlightBD flightBD = MessagePasserModuleUtils.getFlightBD();
		Date departureDate = metaDataDTO.getDepartureDate();
		FlightSegmentTO flightSegmentTO;
		WildcardFlightSearchDto flightSearchDto;
		WildcardFlightSearchRespDto respDto;

		// Checking for flight segment information
		try {

			flightSearchDto = new WildcardFlightSearchDto();
			flightSearchDto.setDepartureAirport(metaDataDTO.getBoardingAirport());
			flightSearchDto.setArrivalAirport(metaDataDTO.getDestinationAirport());
			flightSearchDto.setFlightNumber(metaDataDTO.getFlightNumber());
			flightSearchDto.setDepartureDate(departureDate);
			flightSearchDto.setDepartureDateExact(false);
			flightSearchDto.setNonCNXFlight(true);

			respDto = flightBD.searchFlightsWildcardBased(flightSearchDto);

			if (respDto.getBestMatch() != null && !respDto.isDepartureTimeAmbiguous()) {
				flightSegmentTO = respDto.getBestMatch();
				metaDataDTO.setActualFlightNumber(flightSegmentTO.getFlightNumber());
				metaDataDTO.setRealDepartureDate(flightSegmentTO.getDepartureDateTime());
			} else {
				metaDataDTO.setRealDepartureDate(departureDate);
				metaDataDTO.setActualFlightNumber(metaDataDTO.getFlightNumber());
				errorOccured = true;
			}

		} catch (ModuleException e) {
			metaDataDTO.setRealDepartureDate(departureDate);
			errorOccured = true;
		}

		return errorOccured;
	}

	/**
	 * Returns all passenger final sales DTO(s)
	 *
	 * @param path
	 * @param strMsgName
	 * @return
	 * @throws ModuleException
	 */
	private static Collection getAllPaxNameListDTOs(String path, String strMsgName,
			Collection msgNamesHavingPartTwoThree) throws ModuleException, ParseException {
		File fileMsgName = new File(path, strMsgName);
		return getPRLPaxNameListDTO(fileMsgName, msgNamesHavingPartTwoThree, path);
	}

}
