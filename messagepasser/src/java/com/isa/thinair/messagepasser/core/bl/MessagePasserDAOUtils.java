/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.bl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messagepasser.api.utils.MessagepasserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.CSVBookingDAO;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;
import com.isa.thinair.messagepasser.core.persistence.dao.PRLDAO;
import com.isa.thinair.messagepasser.core.persistence.dao.SSIMDAO;
import com.isa.thinair.messagepasser.core.persistence.dao.XAPnlDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * MessagePasserDAO specific utilities
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class MessagePasserDAOUtils {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(MessagePasserDAOUtils.class);

	/**
	 * Returns a DAO for air reservation module
	 * 
	 * @param daoName
	 * @return
	 */
	private static Object getDAO(String daoName) {
		String daoFullName = "isa:base://modules/" + MessagepasserConstants.MODULE_NAME + "?id=" + daoName + "Proxy";
		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(daoFullName);
	}

	/** Denotes the the DAO names of air reservation */
	private static interface DAONames {
		public static String XA_PNL_DAO = "XAPnlDAO";
		public static String CSV_BOOKING_DAO = "CSVBookingDAO";
		public static String ETL_DAO = "ETLDAO";
		public static String PRL_DAO = "PRLDAO";
		public static String SSIM_DAO = "SSIMDAO";
	}

	/** Denotes the the DAO instances of air reservation */
	public static interface DAOInstance {
		/** Holds the XAPnlDAO instance */
		public static XAPnlDAO XA_PNL_DAO = (XAPnlDAO) getDAO(MessagePasserDAOUtils.DAONames.XA_PNL_DAO);
		public static ETLDAO ETL_DAO = (ETLDAO) getDAO(MessagePasserDAOUtils.DAONames.ETL_DAO);
		public static PRLDAO PRL_DAO = (PRLDAO) getDAO(MessagePasserDAOUtils.DAONames.PRL_DAO);
		public static CSVBookingDAO CSV_BOOKING_DAO = (CSVBookingDAO) getDAO(MessagePasserDAOUtils.DAONames.CSV_BOOKING_DAO);
		public static SSIMDAO SSIM_DAO = (SSIMDAO) getDAO(MessagePasserDAOUtils.DAONames.SSIM_DAO);
	}
}
