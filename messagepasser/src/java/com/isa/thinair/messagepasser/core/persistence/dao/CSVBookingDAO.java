/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.messagepasser.api.model.CSVBookingEntry;

public interface CSVBookingDAO {

	public CSVBookingEntry getBookingById(int paxId);

	public void saveCSVBookingEntry(CSVBookingEntry csvBookingEntry);

	public Collection getBookingEntries(String processedStatus);

	public Collection getBookingEntriesByPnr(String processStatus, String pnr);

	public Collection getUniquePnrByStatus(String processedStatus, int recordCount);

	public boolean isGroupPnrDataConsistent(String pnr);

}
