package com.isa.thinair.messagepasser.core.persistence.hibernate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.messagepasser.api.model.SSIM;
import com.isa.thinair.messagepasser.api.model.SSIMParsedEntry;
import com.isa.thinair.messagepasser.core.persistence.dao.SSIMDAO;

/**
 * SSIMDAO is the business DAO hibernate implementation
 * 
 * @author Rikaz
 * @since 1.0
 * @isa.module.dao-impl dao-name="SSIMDAO"
 */
public class SSIMDAOImpl extends PlatformBaseHibernateDaoSupport implements SSIMDAO {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SSIMDAOImpl.class);

	@Override
	public void saveSSIMEntry(SSIM ssim) {
		// TODO Auto-generated method stub
		hibernateSaveOrUpdate(ssim);
	}

	@Override
	public void saveSSIMParsedEntry(SSIMParsedEntry ssimParsedEntry) {
		// TODO Auto-generated method stub
		hibernateSaveOrUpdate(ssimParsedEntry);
	}	

	@Override
	public void updateSSIMEntryStatus(SSIM ssim) {
		String hqlUpdate = "Update SSIM set processStatus = :status,errorDescription =:errorDesc  where ssimId = :ssimId";
		getSession().createQuery(hqlUpdate).setString("status", ssim.getProcessStatus())
				.setString("errorDesc", ssim.getErrorDescription()).setInteger("ssimId", ssim.getSsimId()).executeUpdate();

	}

	@Override
	public void updateSSIMParsedEntryStatus(SSIMParsedEntry ssimParsedEntry) {
		String hqlUpdate = "Update SSIMParsedEntry set scheduleStatus = :status,errorDescription =:errorDesc where ssimParsedId = :ssimId";
		getSession().createQuery(hqlUpdate).setString("status", ssimParsedEntry.getScheduleStatus())
				.setString("errorDesc", ssimParsedEntry.getErrorDescription())
				.setInteger("ssimId", ssimParsedEntry.getSsimParsedId()).executeUpdate();

	}

}