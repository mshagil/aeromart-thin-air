package com.isa.thinair.messagepasser.core.bl;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.core.util.ParserUtil;

/**
 * Mail Client
 *
 * @author M.Rikaz
 */
public class EmailRetriever {

	private static final Log log = LogFactory.getLog(EmailRetriever.class);

	private static final String MULTIPART_CONTENT_TYPE = "multipart";
	private static final String TEXT_HTML_CONTENT_TYPE = "text/html";
	private static final String TEXT_PLAIN_CONTENT_TYPE = "text/plain";

	/**
	 * Downloads new messages and saves attachments to disk if any.
	 * 
	 * @param host
	 * @param port
	 * @param userName
	 * @param password
	 */
	public static void getMessagesAndAttachments(String path, String host, String port, String userName, String password,
			boolean deleteMessage, String messageType) throws ModuleException {

		validateParams(path, host, userName, password, messageType);

		File messagesDir = new File(path);
		if (!messagesDir.exists()) {
			boolean status = messagesDir.mkdirs();
			// This could be due to some file creational problems
			if (!status) {
				throw new ModuleException("airreservations.pop3Client.mailFilesCreationError");
			}
		}

		Properties properties = new Properties();

		properties.put("mail.pop3.host", host);
		properties.put("mail.pop3.port", port);

		// SSL setting
		// properties.setProperty("mail.pop3.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		// properties.setProperty("mail.pop3.socketFactory.fallback", "false");
		// properties.setProperty("mail.pop3.socketFactory.port", String.valueOf(port));

		Session session  = Session.getInstance(properties);

		Folder folderInbox = null;
		Store store = null;
		try {

			// connects to the message store
			store = session.getStore("pop3");
			log.info(" ###################### Connecting to Mail Server - " + host + " user name: " + userName + ", password: "+ password);
			store.connect(userName, password);
			log.info(" ###################### Authentication Successful ");

			folderInbox = store.getFolder("INBOX");
			folderInbox.open(Folder.READ_WRITE);
			log.info(" ###################### Reading Messages ");
			Message[] arrayMessages = folderInbox.getMessages();
			log.info(" ###################### Total Inbox Messages " + arrayMessages.length);
			for (int i = 0; i < arrayMessages.length; i++) {
				Message message = arrayMessages[i];
				log.info(" ###################### Reading Message " + i);
				String contentType = message.getContentType();
				String messageContent = null;
				String fileName = ParserUtil.getFileName(new Date(), i, messageType);
				boolean msgAsAttachement = false;
				if (contentType.contains(MULTIPART_CONTENT_TYPE)) {
					// content may contain attachments
					Multipart multiPart = (Multipart) message.getContent();
					int numberOfParts = multiPart.getCount();
					for (int partCount = 0; partCount < numberOfParts; partCount++) {
						MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
						if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
							// this part is attachment
							part.saveFile(path + File.separator + fileName);
							msgAsAttachement = true;
						} else if (part.getContentType().contains(TEXT_PLAIN_CONTENT_TYPE)) {
							// this part may be the message content
							messageContent = part.getContent().toString();
						}
					}
				} else if (contentType.contains(TEXT_PLAIN_CONTENT_TYPE) || contentType.contains(TEXT_HTML_CONTENT_TYPE)) {
					Object content = message.getContent();
					if (content != null) {
						messageContent = content.toString();
					}
				}

				if (!msgAsAttachement) {
					storeContentToFile(path, fileName, messageContent, i, messageType);
				}

				if (deleteMessage) {
					log.info(" ###################### Delete Message " + i + " from the Mail box ");
					message.setFlag(Flags.Flag.DELETED, true);
				}
			}
			folderInbox.close(true);
			store.close();
		} catch (NoSuchProviderException ex) {
			log.error("###################### ERROR No provider for pop3.", ex);
			throw new ModuleException("airreservations.pop3Client.genericMailError");
		} catch (MessagingException ex) {
			log.error("###################### ERROR Could not connect to the message store", ex);
			throw new ModuleException("airreservations.pop3Client.genericMailError");
		} catch (IOException ex) {
			log.error("###################### ERROR IOException", ex);
			throw new ModuleException("airreservations.pop3Client.genericMailError");
		}
	}

	private static void storeContentToFile(String path, String fileName, String content, int message, String messageType)
			throws ModuleException {

		try {
			if (!StringUtil.isNullOrEmpty(content)) {
				File file = new File(path, fileName);
				FileUtils.writeStringToFile(file, content);
				log.info(" ###################### File Created " + file.getName());
			}
		} catch (IOException e) {
			log.error(" ###################### File Creation failed " + fileName);
		}

	}

	private static void validateParams(String path, String hostName, String userName, String password, String messageType)
			throws ModuleException {
		if (path == null || path.equals("") || hostName == null || hostName.equals("") || userName == null || password == null
				|| messageType == null || messageType.equals("")) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
	}
}