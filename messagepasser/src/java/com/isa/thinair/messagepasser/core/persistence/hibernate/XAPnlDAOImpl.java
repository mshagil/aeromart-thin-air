/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.messagepasser.api.model.XAPnl;
import com.isa.thinair.messagepasser.api.model.XAPnlPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.core.persistence.dao.XAPnlDAO;

/**
 * XAPnlDAO is the business DAO hibernate implementation
 * 
 * @author Byorn de silva
 * @since 1.0
 * @isa.module.dao-impl dao-name="XAPnlDAO"
 */
public class XAPnlDAOImpl extends PlatformBaseHibernateDaoSupport implements XAPnlDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(XAPnlDAOImpl.class);

	/**
	 * Returns the xa pnl
	 * 
	 * @param xaPnlId
	 * @return
	 */
	public XAPnl getXAPnlEntry(int xaPnlId) {
		return (XAPnl) get(XAPnl.class, new Integer(xaPnlId));
	}

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @param pnlContent
	 * @return
	 */
	public boolean hasAnEqualXAPnl(String flightNumber, String airportCode, Date departureDate, Integer partNumber) {

		String sql = "SELECT count(pnl_id)" + " FROM t_xa_pnl "
				+ " where flight_number=? and departure_date=? and  from_airport=? and part_number=? ";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, departureDate, airportCode, partNumber };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		}
		return false;

	}

	/**
	 * Save XAPnl Entry
	 * 
	 * @param XAPnl
	 */
	public void saveXAPnlEntry(XAPnl xaPnl) {
		log.debug("Inside saveXAPnlEntry");

		hibernateSaveOrUpdate(xaPnl);

		log.debug("Exit saveXAPnlEntry");
	}

	/**
	 * Save XAPnl Pax Entry
	 * 
	 * @param xaPnlPaxEntry
	 */
	public void saveXAPnlPaxEntry(XAPnlPaxEntry xaPnlPaxEntry) {
		log.debug("Inside saveXAPnlPaxEntry");

		hibernateSaveOrUpdate(xaPnlPaxEntry);

		log.debug("Exit saveXAPnlPaxEntry");
	}

	/**
	 * Save XAPnl Pax Entry
	 * 
	 * @param colXAPnlPaxEntry
	 */
	public void saveXAPnlPaxEntries(Collection colXAPnlPaxEntry) {
		log.debug("Inside saveXAPnlPaxEntries");

		hibernateSaveOrUpdateAll(colXAPnlPaxEntry);

		log.debug("Exit saveXAPnlPaxEntries");
	}

	/**
	 * Return XA PNL Pax entries
	 * 
	 * @param xaPnlId
	 * @param status
	 * @return
	 */
	public Collection getXAPnlPaxEntries(int xaPnlId, String status) {
		if (status == null) {
			String hql = " SELECT xaPnlPaxEntry FROM XAPnlPaxEntry AS xaPnlPaxEntry " + " WHERE xaPnlPaxEntry.pnlId = ? ";

			return find(hql, new Object[] { new Integer(xaPnlId) }, XAPnlPaxEntry.class);
		} else {
			String hql = " SELECT xaPnlPaxEntry FROM XAPnlPaxEntry AS xaPnlPaxEntry "
					+ " WHERE xaPnlPaxEntry.pnlId = ? AND xaPnlPaxEntry.processedStatus = ? ";

			return find(hql, new Object[] { new Integer(xaPnlId), status }, XAPnlPaxEntry.class);
		}
	}

	public int getCountOfErrorPax(Integer pnlId) {

		String sql = "SELECT count(pp_id) ";
		sql += " FROM t_xa_pnl_parsed ";
		sql += " WHERE pnl_id=? ";
		sql += " AND (pnr is null OR proc_status='N' OR proc_status='E')";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { pnlId };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		// Page page = new Page();
		return ((Integer) intObj).intValue();

	}

	public Collection getPaxNamesHavingError(Integer pnlId) {

		String sql = "SELECT title, first_name, last_name, pax_type_code, xa_pnr, infant_first_name, error_description ";
		sql += " FROM t_xa_pnl_parsed";
		sql += " WHERE pnl_id=? ";
		sql += " AND (pnr is null OR proc_status='N' OR proc_status='E')";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { pnlId };

		Collection paxDetails = (Collection) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection paxDetails = new ArrayList();

				while (rs.next()) {
					String paxDetail = "PNR NUMBER: " + rs.getString("xa_pnr") + " NAME : ["
							+ ReservationPax.getPassengerTypeDescription(rs.getString("pax_type_code")) + "] "
							+ rs.getString("title") + " " + rs.getString("first_name") + " " + rs.getString("last_name")
							+ " ERROR : " + rs.getString("error_description");
					paxDetails.add(paxDetail);

				}

				return paxDetails;
			}

		});

		return paxDetails;
	}

	public Collection getPaxNamesWithReservations(Integer pnlId) {

		String sql = "SELECT title, first_name, last_name, pax_type_code, xa_pnr, infant_first_name, error_description ";
		sql += " FROM t_xa_pnl_parsed ";
		sql += " WHERE pnl_id=? ";
		sql += " AND proc_status='P' ";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { pnlId };

		Collection paxDetails = (Collection) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection paxDetails = new ArrayList();

				while (rs.next()) {
					String paxDetail = "PNR NUMBER: " + rs.getString("xa_pnr") + " NAME : ["
							+ ReservationPax.getPassengerTypeDescription(rs.getString("pax_type_code")) + "] "
							+ rs.getString("title") + " " + rs.getString("first_name") + " " + rs.getString("last_name");

					paxDetails.add(paxDetail);
				}

				return paxDetails;
			}

		});

		return paxDetails;
	}

	public Collection getFailedXAPnlIds(int attemptLimit) {

		String sql = " SELECT distinct xapax.pnl_id " + " FROM t_xa_pnl xapnl, t_xa_pnl_parsed xapax " + " WHERE "
				+ " xapnl.pnl_id=xapax.pnl_id and " + " (xapax.proc_status = ? or xapax.proc_status = ? or xapax.pnr is null)"
				+ " and xapnl.number_of_attemps < ? " + " and xapnl.departure_date > ?";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Timestamp now = new Timestamp(new Date().getTime());

		Object params[] = { ReservationInternalConstants.XAPnlProcessStatus.NOT_PROCESSED,
				ReservationInternalConstants.XAPnlProcessStatus.ERROR_OCCURED, new Integer(attemptLimit), now };

		Collection xapnlIds = (Collection) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection xapnlIds = new ArrayList();

				while (rs.next()) {
					xapnlIds.add(new Integer(rs.getInt(1)));
				}
				return xapnlIds;

			}

		});

		return xapnlIds;
	}

	public Page getPagedXAPNLData(List criteria, int startIndex, int noRecs, List orderByFieldList) {
		return getPagedData(criteria, startIndex, noRecs, XAPnl.class, orderByFieldList);
	}

}