package com.isa.thinair.messagepasser.core.bl.etl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.dto.etl.ETLMetaDataDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLPAXRecordDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLPaxNameListDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLRecordDTO;
import com.isa.thinair.messagepasser.api.dto.etl.LogDTO;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.bl.MailError;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;
import com.isa.thinair.messagepasser.core.util.ParserUtil;

/**
 * The utility to parse a PNL document
 * 
 * @author Ishan
 * @since 1.0 *
 */
public class ETLDTOParser {

	/**
	 * holds the key if tour id was null, which means a continuation to part 2 is possible
	 */

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ETLDTOParser.class);

	/**
	 * Returns PNL meta data transfer information
	 * 
	 * @param f
	 * @return
	 * @throws ModuleException
	 */
	private static ETLMetaDataDTO getMetaDataDTO(File f) throws ModuleException {
		Throwable exception = null;
		String content = "";
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(f));
			StringBuffer parserFriendlyText = new StringBuffer();
			StringBuffer originalPNLText = new StringBuffer();
			boolean destinationFound = false;
			boolean prevWasDestinationFound = false;
			boolean isValidEtl = false;
			String continuationText = null;
			boolean isEmptyETL = false;

			String strHolder = null;
			String fromAddress = "";
			boolean started = false;
			boolean flightNumberPass = false;

			while ((strHolder = buffer.readLine()) != null) {

				// Capturing the from address
				if (strHolder.length() > 5 && strHolder.substring(0, 5).equalsIgnoreCase("From:")) {
					fromAddress = BeanUtils.nullHandler(strHolder.substring(5));
					int start = fromAddress.indexOf("<");
					int end = fromAddress.lastIndexOf(">");

					// This means Email Address is nilindra@abc.com
					if (start == -1 || end == -1) {
						fromAddress = BeanUtils.nullHandler(fromAddress);
						// This mean Email Address is Nilindra
						// <nilindra@abc.com>
					} else {
						fromAddress = BeanUtils.nullHandler(fromAddress.substring(start + 1, end));
					}
				}
				// Processing the ETL part
				else if (strHolder.trim().equalsIgnoreCase("ETL") || started) {
					isValidEtl = true;

					// for continuation from part 1 check the line
					if (prevWasDestinationFound) {
						prevWasDestinationFound = false;
						if (!isANormalPaxRecordLine(strHolder)) {
							continuationText = strHolder;
							continue;
						}

					}

					originalPNLText.append(strHolder + "\n");
					started = true;

					if (strHolder.equalsIgnoreCase("ETL")) {
						parserFriendlyText.append(strHolder + "\n");
						// Process down the PNL label
					} else if (strHolder.trim().length() > 2 && !isNumber(strHolder.trim().substring(0, 3))) {

						// FIXME: Temporary FIX
						if (strHolder.startsWith("RBD")) {
							continue;
						} else if (strHolder.startsWith("NIL")) {
							// isEmptyETL = true;
							// continue;
						} else if (strHolder.charAt(0) == '-') {

							// Omit destination entries without passages
							int noOfPax = 0;
							StringBuffer paxCount = new StringBuffer("");

							for (int i = 0; i < strHolder.length(); i++) {
								try {
									paxCount.append(String.valueOf(Integer.parseInt(String.valueOf(strHolder.charAt(4 + i)))));

								} catch (NumberFormatException e) {
									noOfPax = Integer.parseInt(paxCount.toString());
									break;
								}
							}

							if (noOfPax == 0) {
								continue;
							}

							if (!destinationFound) {
								parserFriendlyText.append(strHolder + "\n");
								prevWasDestinationFound = true;
								destinationFound = true;
							} else {
								parserFriendlyText.append(strHolder + "\n");
							}
						} else if (strHolder.charAt(3) == ' ') {
							parserFriendlyText.append(strHolder.substring(0, 7).replaceAll("/", "") + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".L/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);
							parserFriendlyText.append(" " + strHolder + "\n");
						} else if (strHolder.substring(0, 3).equalsIgnoreCase(".R/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".L/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".C/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".I/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".O/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".RN/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".W/")
								|| strHolder.substring(0, 3).equalsIgnoreCase(".N/")
								|| strHolder.substring(0, 4).equalsIgnoreCase(".O2/")) {
							parserFriendlyText.deleteCharAt(parserFriendlyText.length() - 1);

							parserFriendlyText.append(" " + strHolder + "\n");
						}

						else {
							// If it's the end of the etl making it as
							// process ended
							if (strHolder.indexOf("ENDETL") != -1 || strHolder.indexOf("ENDPART") != -1) {
								started = false;

							}

							if (flightNumberPass) {
								/**/
								if (isNumber(strHolder.substring(0, 1))) {
									if (isNumber(strHolder.substring(1, 2))) {
										strHolder = new StringBuffer(strHolder).insert(2, " ").toString();
										// parserFriendlyText.insert(2, "
										// ");
									} else {
										strHolder = new StringBuffer(strHolder).insert(1, " ").toString();
										// parserFriendlyText.insert(1, "
										// ");
									}

								}
							} else {
								if (checkCarrierCode(strHolder.substring(0, 2))) {
									flightNumberPass = true;
								}
							}
							parserFriendlyText.append(strHolder + "\n");

							/**/

						}
					}

				}
			}
			buffer.close();
			if (!isValidEtl) {
				throw new ModuleException(new Exception("Invalid IATA Format"), "messagepasser.etl.cannotParseETL");
			}
			String text = parserFriendlyText.toString();

			content = text.replaceAll("\n", "<br>");

			ETLMetaDataDTO metaDataDTO = null;
			if (!isEmptyETL) {
				metaDataDTO = ETLParser.process(text);
				metaDataDTO.setFromAddress(fromAddress);
				metaDataDTO.setPnlContent(originalPNLText.toString());

			}

			return metaDataDTO;

		} catch (IOException e) {
			exception = e;
			log.error(" ERROR ", e);
			throw new ModuleException(e, "messagepasser.etl.cannotLocateETL");
		} catch (ModuleException e) {
			exception = e;
			log.error(" ERROR ", e);
			throw new ModuleException(e, "messagepasser.etl.cannotParseETL");
		} finally {

			// if exception occured mail to relavent parties.
			if (exception != null) {

				LogDTO logDTO = new LogDTO();
				logDTO.setExceptionDescription(exception.getMessage());
				logDTO.setStackTraceElements(exception.getStackTrace());
				logDTO.setContent(content);
				logDTO.setFileName(f.getName());
				MailError.notifyError(logDTO);
			}

		}
	}

	private static boolean isANormalPaxRecordLine(String content) {
		if (isNumber(content.substring(0, 1))) {
			return true;

		}
		return false;
	}

	/**
	 * Find out whether it's a number or not
	 * 
	 * @param str
	 * @return
	 */
	private static boolean isNumber(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * Insert to the etl entries
	 * 
	 * @param colMsgNames
	 * @return
	 * @throws ModuleException
	 */
	public static int insertETLToDatabase(Collection<String> msgNamesWithPartTwoOrThree, String strMsgName)
			throws ModuleException {

		MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
		ETLDAO etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;
		String etlProcessPath = messagePasserConfig.getEtlProcessPath();
		Collection<ETLPaxNameListDTO> colPaxNameListsDTO = getAllPaxNameListDTOs(etlProcessPath, strMsgName,
				msgNamesWithPartTwoOrThree);

		ETLPaxNameListDTO paxNameListsDTO;
		ETLPaxEntry etlPaxEntry;

		int pnlId = 0;

		for (Iterator<ETLPaxNameListDTO> iter = colPaxNameListsDTO.iterator(); iter.hasNext();) {
			paxNameListsDTO = (ETLPaxNameListDTO) iter.next();
			if (paxNameListsDTO.getEticketNumber() == null) {
				pnlId = paxNameListsDTO.getEtlId();
				continue;
			}
			etlPaxEntry = new ETLPaxEntry();

			if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					etlPaxEntry.setLogicalCabinClassCode(paxNameListsDTO.getCabinClassCode());
				} else {
					etlPaxEntry.setBookingClassCode(paxNameListsDTO.getBookingClassCode());
				}
			} else {
				etlPaxEntry.setCabinClassCode(paxNameListsDTO.getCabinClassCode());
			}

			etlPaxEntry.setDepartureAirport(paxNameListsDTO.getDepartureAirportCode());
			etlPaxEntry.setTitle(paxNameListsDTO.getTitle());
			etlPaxEntry.setFirstName(paxNameListsDTO.getFirstName());
			etlPaxEntry.setLastName(paxNameListsDTO.getLastName());
			etlPaxEntry.setFlightNumber(paxNameListsDTO.getFlightNumber());
			etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
			etlPaxEntry.setFlightDate(paxNameListsDTO.getRealFlightDate());
			etlPaxEntry.setArrivalAirport(paxNameListsDTO.getArrivalAirportCode());
			etlPaxEntry.setReceivedDate(paxNameListsDTO.getDateDownloaded());
			etlPaxEntry.setEtlId(new Integer(paxNameListsDTO.getEtlId()));
			etlPaxEntry.setPaxType(paxNameListsDTO.getPaxType());
			etlPaxEntry.setErrorDescription(paxNameListsDTO.getErrorDescription());
			etlPaxEntry.setEticketNumber(paxNameListsDTO.getEticketNumber());
			etlPaxEntry.setMarketingFlightElement(paxNameListsDTO.getMarketingFlight());
			etlPaxEntry.setCoupNumber(new Integer(paxNameListsDTO.getCoupNumber()));
			etlPaxEntry.setExternalEticketNumber(paxNameListsDTO.getExternalEticketNumber());
			etlPaxEntry.setExternalCouponNo(paxNameListsDTO.getExternalCouponNo());
			etlPaxEntry.setExternalInfCoupNumber(paxNameListsDTO.getExternalInfCoupNumber());
			etlPaxEntry.setExternalInfEticketNumber(paxNameListsDTO.getExternalInfEticketNumber());
			etlPaxEntry.setExternalRecordLocator(paxNameListsDTO.getExternalRecordLocator());			
			etlPaxEntry.setCodeShareBc(paxNameListsDTO.getCodeShareBc());
			etlPaxEntry.setCodeShareFlightNo(paxNameListsDTO.getCodeShareFlightNo());
			etlPaxEntry.setGdsId(paxNameListsDTO.getGdsId());
			etlPaxEntry.setCabinClassCode(paxNameListsDTO.getCabinClassCode());
			
			char useAeroMartETS = (paxNameListsDTO.getUseAeroMartETS() != null && paxNameListsDTO.getUseAeroMartETS() == true) ? 'Y' : 'N';
			etlPaxEntry.setUseAeroMartETS(useAeroMartETS);
			
			String infEticketNo = paxNameListsDTO.getInfEticketNumber();
			if(!StringUtil.isNullOrEmpty(infEticketNo)){
				infEticketNo = infEticketNo.replace("INF", "");
			}
			
			etlPaxEntry.setInfEticketNumber(infEticketNo);
			etlPaxEntry.setInfCoupNumber(paxNameListsDTO.getInfCoupNumber() != null ? new Integer(paxNameListsDTO
					.getInfCoupNumber()) : null);
			etlPaxEntry.setPaxStatus(paxNameListsDTO.getPaxStatus());
			etlPaxEntry.setPnr(paxNameListsDTO.getPnr());
			etlDAO.saveETLPaxEntry(etlPaxEntry);

			pnlId = etlPaxEntry.getEtlId().intValue();
		}

		return pnlId;
	}

	/**
	 * A Temporary Method
	 * 
	 * @TODO: Incorporate Singleton/Lazy Load Design Pattern
	 * @param pnlPaxEntry
	 */
	private static String checkToAppendTKNA(String ssrCode) {

		if (ssrCode == null || ssrCode.equals("") || ssrCode.equals(" ")) {
			return ParserConstants.SSR_CODES.CODE_TKNA;
		}

		if (ssrCode.equals(ParserConstants._VIP_CASE)) {
			return ParserConstants.SSR_CODES.CODE_VIP;
		}

		return ssrCode;

	}
	
	private static Date getFlightDepZuluDate(String airportCode, Date flightDepLocalDate) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(MessagePasserModuleUtils.getAirportBD());
		return helper.getZuluDateTime(airportCode, flightDepLocalDate);
	}

	/**
	 * Returns passenger final sales data transfer information
	 * 
	 * @param file
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ETLPaxNameListDTO> getETLPaxNameListDTO(File file, String path) throws ModuleException {
		PassengerBD passengerBD = MessagePasserModuleUtils.getPassengerBD();
		Collection<String> colPaxTitles = new ArrayList<String>(passengerBD.getPassengerTitleMap().keySet());

		Collection<String> excludingPaxTypes = new ArrayList<String>();
		excludingPaxTypes.add(PaxTypeTO.INFANT);
		Map<String, Collection<String>> paxTitleTypes = passengerBD.getPassengerTitleTypes(excludingPaxTypes);
		ETLPaxNameListDTO etlNameListDTO = null;

		ETLRecordDTO empnlRecordDTO = null;
		ETLPAXRecordDTO recordDTO = null;
		Date flightDepZuluDate = null;

		ETLMetaDataDTO emPnlMetaDataDTO = getMetaDataDTO(file);
		ArrayList<ETLPaxNameListDTO> colPaxNameListDTO = new ArrayList<ETLPaxNameListDTO>();
		if (emPnlMetaDataDTO != null) {
			flightDepZuluDate = getFlightDepZuluDate(emPnlMetaDataDTO.getBoardingAirport(), emPnlMetaDataDTO.getDepartureDate());
			if ((flightDepZuluDate.compareTo(new Date()) < 0)
					|| ((flightDepZuluDate.compareTo(CalendarUtil.getEndTimeOfDate(new Date())) <= 0) && AppSysParamsUtil
							.isAllowProcessPfsBeforeFlightDeparture())) {
				emPnlMetaDataDTO.getPnlContent();

				Flight flight = getMarketingCarrierFlight(emPnlMetaDataDTO);
				Set<CodeShareMCFlight> codeShareMCFlights = getCodeShareMCFlights(emPnlMetaDataDTO);
				String flightNo = (flight != null) ? flight.getFlightNumber() : emPnlMetaDataDTO.getActualFlightNumber();
				emPnlMetaDataDTO.setActualFlightNumber(flightNo);
				// Get the flight error occured status
				boolean flightErrorOccured = isRealDepartureDateErrorOccured(emPnlMetaDataDTO);

				checkIfEtlAlreadyExists(emPnlMetaDataDTO);

				ETL etl = new ETL();
				etl.setCarrierCode(emPnlMetaDataDTO.getCarrierCode());
				etl.setFlightNumber(emPnlMetaDataDTO.getActualFlightNumber());
				etl.setFromAddress(emPnlMetaDataDTO.getFromAddress());
				etl.setNumberOfPassengers(emPnlMetaDataDTO.getTotNumberOfPax());
				etl.setToAirport(emPnlMetaDataDTO.getDestinationAirport());
				etl.setFromAirport(emPnlMetaDataDTO.getBoardingAirport());
				etl.setDateDownloaded(emPnlMetaDataDTO.getDateDownloaded());

				etl.setEtlContent(emPnlMetaDataDTO.getPnlContent());
				etl.setFromAddress(emPnlMetaDataDTO.getFromAddress());
				etl.setPartNumber(Integer.valueOf(emPnlMetaDataDTO.getPartNumber()).intValue());
				// etl.setNumOfAttempts(0);
				etl.setDepartureDate(emPnlMetaDataDTO.getRealDepartureDate());

				if (flightErrorOccured) {
					etl.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
				} else {
					etl.setProcessedStatus(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
				}

				if (emPnlMetaDataDTO.getPassengerRecords() != null) {
					Iterator empnlRecordDTOIterator = emPnlMetaDataDTO.getPassengerRecords().iterator();

					while (empnlRecordDTOIterator.hasNext()) {

						String pnrNumber = "";

						etlNameListDTO = new ETLPaxNameListDTO();
						empnlRecordDTO = (ETLRecordDTO) empnlRecordDTOIterator.next();

						// preapre the remarks dto..
						ParserUtil.prepareRemarks(empnlRecordDTO);

						if (empnlRecordDTO.getRecords() != null) {
							Iterator<ETLPAXRecordDTO> itRecordDTO = empnlRecordDTO.getRecords().iterator();
							while (itRecordDTO.hasNext()) {

								recordDTO = (ETLPAXRecordDTO) itRecordDTO.next();

								String pnr = recordDTO.getPnrNumber();
								// Check Constrains
								// With some etl records pnr is not coming in that case we are loading it
								if (StringUtil.isNullOrEmpty(pnr)) {
									pnr = MessagePasserModuleUtils.getReservationQueryBD().getPNRByEticketNo(
											recordDTO.getEticketNumber());
									etlNameListDTO.setPnr(pnr);
								}else{
									etlNameListDTO.setPnr(pnr);
								}
								
								etlNameListDTO.setArrivalAirportCode(empnlRecordDTO.getDestinationAirport());
								etlNameListDTO.setMarketingFlight(recordDTO.getMarketingFlight());
								etlNameListDTO.setEticketNumber(recordDTO.getEticketNumber());
								etlNameListDTO.setCoupNumber(recordDTO.getCoupNumber());
								etlNameListDTO.setInfEticketNumber(recordDTO.getInfEticketNumber());
								etlNameListDTO.setInfCoupNumber(recordDTO.getInfCoupNumber());
								
								if (AppSysParamsUtil.isRBDPNLADLEnabled()) {
									if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
										etlNameListDTO.setLogicalCabinClassCode(empnlRecordDTO.getCcCode());
									} else {
										etlNameListDTO.setBookingClassCode(empnlRecordDTO.getCcCode());
									}
								} else {
									etlNameListDTO.setCabinClassCode(empnlRecordDTO.getCcCode());
								}

								checkConstrains(flightErrorOccured, etlNameListDTO, etl, codeShareMCFlights);								
								
								
								// Setting the PaxNameListsDTO information
								// etlNameListDTO.setPnr(pnrNumber);

								etlNameListDTO.setTitle(getPassengerDerivedInfo(recordDTO.getParseTitle(), colPaxTitles,
										paxTitleTypes)[0].toString());

								Object[] passengerInfo = getPassengerDerivedInfo(recordDTO.getFirstNameWithTitle(), colPaxTitles,
										paxTitleTypes);
								etlNameListDTO.setFirstName(passengerInfo[1].toString());
								etlNameListDTO.setPaxType(passengerInfo[2].toString());
								etlNameListDTO.setLastName(recordDTO.getLastName());

								String ssrCode = "";
								etlNameListDTO.setSsrCode(ssrCode = checkToAppendTKNA(recordDTO.getSsrCode()));
								if (ssrCode.equals(ParserConstants.SSR_CODES.CODE_TKNA)) {
									etlNameListDTO.setSsrRemarks(pnrNumber);
								} else {
									etlNameListDTO.setSsrRemarks(recordDTO.getSsrText() + " " + pnrNumber);
								}

								// etlNameListDTO.setErrorDescription(etlNameListDTO.getErrorDescription() + " "
								// + recordDTO.getErrorDescription());
								etlNameListDTO.setInboundInfo(empnlRecordDTO.getInboundDetails());
								etlNameListDTO.setOutboundInfo(empnlRecordDTO.getOutboundDetails());
								etlNameListDTO.setDay(emPnlMetaDataDTO.getDay());
								etlNameListDTO.setMonth(emPnlMetaDataDTO.getMonth());								
								etlNameListDTO.setDepartureAirportCode(emPnlMetaDataDTO.getBoardingAirport());
								etlNameListDTO.setFlightNumber(emPnlMetaDataDTO.getActualFlightNumber());
								etlNameListDTO.setDateDownloaded(emPnlMetaDataDTO.getDateDownloaded());
								etlNameListDTO.setRealFlightDate(emPnlMetaDataDTO.getRealDepartureDate());								
								etlNameListDTO.setPaxStatus(recordDTO.getPaxStatus());
								etlNameListDTO.setLogicalCabinClassCode(empnlRecordDTO.getLogicalCabinClassCode());
								etlNameListDTO.setBookingClassCode(empnlRecordDTO.getBookingClassCode());

								colPaxNameListDTO.add(etlNameListDTO);
								if (itRecordDTO.hasNext()) {
									etlNameListDTO = new ETLPaxNameListDTO();
								}
							}
						}
					}
				} else {
					etlNameListDTO = new ETLPaxNameListDTO();
					colPaxNameListDTO.add(etlNameListDTO);
				}
				ETLDAO etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;
				/**
				 * 
				 */
				etlDAO.saveETLEntry(etl);

				for (Iterator<ETLPaxNameListDTO> itColPaxNameListDTO = colPaxNameListDTO.iterator(); itColPaxNameListDTO
						.hasNext();) {
					etlNameListDTO = (ETLPaxNameListDTO) itColPaxNameListDTO.next();
					etlNameListDTO.setEtlId(etl.getEtlId());
				}
			}
		}
		return colPaxNameListDTO;
	}

	/**
	 * Returns passenger derived information It is assumed that MR, MS, CHILD codes should be there for any lines.
	 * However there could be more codes too. The first priority will be given for the MR, MS and CHILD codes TODO Find
	 * ways to make it improve
	 * 
	 * @param name
	 * @param colTitles
	 * @return
	 */
	private static Object[] getPassengerDerivedInfo(String name, Collection<String> colTitles,
			Map<String, Collection<String>> paxTitleTypes) {
		String title = ReservationInternalConstants.PassengerTitle.MR;
		String paxType = ReservationInternalConstants.PassengerType.ADULT;
		String firstName = name;
		boolean matchFound = false;
		String dbTitle;

		// To fix first name blank etl records
		if (name == null) {
			return new Object[] { title, "", paxType };
		}
		if (name.endsWith("MR")) {
			title = ReservationInternalConstants.PassengerTitle.MR;
			firstName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("MS")) {
			title = ReservationInternalConstants.PassengerTitle.MS;
			firstName = name.substring(0, name.length() - 2);
		} else if (name.endsWith("CHD")) {
			title = ReservationInternalConstants.PassengerTitle.CHILD;
			paxType = ReservationInternalConstants.PassengerType.CHILD;
			firstName = name.substring(0, name.length() - 3);
		} else {
			for (Iterator<String> itColTitles = colTitles.iterator(); itColTitles.hasNext();) {
				dbTitle = (String) itColTitles.next();

				if (name.endsWith(dbTitle)) {
					matchFound = true;
					title = dbTitle;
					firstName = name.substring(0, name.length() - dbTitle.length());
					if (paxTitleTypes.get(title) != null) {
						paxType = paxTitleTypes.get(title).iterator().next();
					}
				}
			}
		}

		// This means there is no matching record
		if (!matchFound) {
			if (name.endsWith("MRS")) {
				title = ReservationInternalConstants.PassengerTitle.MS;
				firstName = name.substring(0, name.length() - 3);
			} else if (name.endsWith("MISS")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				firstName = name.substring(0, name.length() - 4);
			} else if (name.endsWith("MSTR")) {
				title = ReservationInternalConstants.PassengerTitle.CHILD;
				paxType = ReservationInternalConstants.PassengerType.CHILD;
				firstName = name.substring(0, name.length() - 4);
			}
		}

		return new Object[] { title, firstName, paxType };
	}

	public static void checkIfEtlAlreadyExists(ETLMetaDataDTO dataDTO) throws ModuleException {
		ETLDAO etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;

		if (etlDAO.hasAnEqualEtl(dataDTO.getActualFlightNumber(), dataDTO.getBoardingAirport(), dataDTO.getDepartureDate(),
				Integer.valueOf(dataDTO.getPartNumber()))) {
			throw new ModuleException("messagepasser.etl.already.exists");
		}
	}

	/**
	 * Finds out if the real departure date error occured or not
	 * 
	 * @param pfsMetaDataDTO
	 * @return
	 * @throws ModuleException
	 */
	private static boolean isRealDepartureDateErrorOccured(ETLMetaDataDTO metaDataDTO) throws ModuleException {
		boolean errorOccured = false;
		FlightBD flightBD = MessagePasserModuleUtils.getFlightBD();
		Date departureDate = metaDataDTO.getDepartureDate();
		FlightSegmentTO flightSegmentTO;
		WildcardFlightSearchDto flightSearchDto;
		WildcardFlightSearchRespDto respDto;

		// Checking for flight segment information
		try {
			flightSearchDto = new WildcardFlightSearchDto();
            flightSearchDto.setDepartureAirport(metaDataDTO.getBoardingAirport());
            flightSearchDto.setArrivalAirport(metaDataDTO.getDestinationAirport());
			flightSearchDto.setFlightNumber(metaDataDTO.getFlightNumber());
			flightSearchDto.setDepartureDate(departureDate);
			flightSearchDto.setDepartureDateExact(false);
			flightSearchDto.setNonCNXFlight(true);

			respDto = flightBD.searchFlightsWildcardBased(flightSearchDto);

			if (respDto.getBestMatch() != null && !respDto.isDepartureTimeAmbiguous()) {
				flightSegmentTO = respDto.getBestMatch();
				metaDataDTO.setActualFlightNumber(flightSegmentTO.getFlightNumber());
				metaDataDTO.setRealDepartureDate(flightSegmentTO.getDepartureDateTime());
			} else {
				metaDataDTO.setRealDepartureDate(departureDate);
				metaDataDTO.setActualFlightNumber(metaDataDTO.getFlightNumber());
				errorOccured = true;
			}

		} catch (ModuleException e) {
			metaDataDTO.setRealDepartureDate(departureDate);
			errorOccured = true;
		}

		return errorOccured;
	}

	/**
	 * Returns all passenger final sales DTO(s)
	 * 
	 * @param path
	 * @param strMsgName
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<ETLPaxNameListDTO> getAllPaxNameListDTOs(String path, String strMsgName,
			Collection<String> msgNamesHavingPartTwoThree) throws ModuleException {
		File fileMsgName = new File(path, strMsgName);
		return getETLPaxNameListDTO(fileMsgName, path);
	}

	/**
	 * Check ETL Constrains
	 * 
	 * @param flightErrorOccured
	 * @param etlPaxNameListDTO
	 * @param etl
	 * @param codeShareMCFlights TODO
	 * @throws ModuleException
	 */
	private static void checkConstrains(boolean flightErrorOccured, ETLPaxNameListDTO etlPaxNameListDTO, ETL etl, Set<CodeShareMCFlight> codeShareMCFlights)
			throws ModuleException {

		boolean pnrErrorOccured = false;

		// Checking the pnr number
		try {
			String pnr = etlPaxNameListDTO.getPnr();
			if (etlPaxNameListDTO != null && !StringUtil.isNullOrEmpty(pnr)) {
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				boolean isExternalPnr = false;				
				if (!StringUtil.isNullOrEmpty(etlPaxNameListDTO.getMarketingFlight())
						&& etlPaxNameListDTO.getMarketingFlight().length() > 10) {
					pnrModesDTO.setExtRecordLocatorId(pnr);
					pnrModesDTO.setLoadFares(true);
					pnrModesDTO.setLoadSegView(true);
					isExternalPnr = true;
				} else {
					pnrModesDTO.setPnr(pnr);
				}
				Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);
				if(reservation==null && !isExternalPnr){
					pnrModesDTO.setExtRecordLocatorId(pnr);
					pnrModesDTO.setLoadFares(true);
					reservation = ReservationProxy.getReservation(pnrModesDTO);
				}
				
				if(reservation!=null){
					etlPaxNameListDTO.setPnr(reservation.getPnr());	
				}

				if (reservation != null && ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())
						&& !StringUtil.isNullOrEmpty(reservation.getExternalRecordLocator())) {

					etlPaxNameListDTO.setExternalRecordLocator(reservation.getExternalRecordLocator());
					etlPaxNameListDTO.setPnr(reservation.getPnr());

					Collection<FlightReconcileDTO> colFlightReconcileDTO = null;
					FlightReconcileDTO flightReconcileDTO = null;
					Date depDate = etl.getDepartureDate();
					for (ReservationSegmentDTO segDTO : reservation.getSegmentsView()) {
						if (segDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
								&& segDTO.getOrigin().equals(etl.getFromAirport())
								&& segDTO.getDestination().equals(etl.getToAirport())
								&& segDTO.getFlightNo().equals(etl.getFlightNumber())) {
							depDate = segDTO.getDepartureDate();
						}
					}
					colFlightReconcileDTO = MessagePasserModuleUtils.getSegmentBD().getPnrSegmentsForReconcilation(
							etl.getFlightNumber(), depDate, etl.getFromAirport(), etlPaxNameListDTO.getArrivalAirportCode(),
							reservation.getPnr());
					Integer flighSegId = null;
					for (Iterator<FlightReconcileDTO> iterFlightReconcileDTO = colFlightReconcileDTO.iterator(); iterFlightReconcileDTO
							.hasNext();) {
						flightReconcileDTO = (FlightReconcileDTO) iterFlightReconcileDTO.next();
						flighSegId = flightReconcileDTO.getFlightSegId();
					}

					Set<ReservationSegment> segs = reservation.getSegments();
					ReservationSegment reservationSegment = null;
					for (Iterator<ReservationSegment> iterSegs = segs.iterator(); iterSegs.hasNext();) {
						reservationSegment = (ReservationSegment) iterSegs.next();
						if (reservationSegment.getFlightSegId().equals(flighSegId)
								&& !reservationSegment.getStatus().equals(
										ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
							break;
						}
					}

					if (reservationSegment != null) {
						
						String csFlightNo = null;
						GDSStatusTO gdsStatusTO= ReservationApiUtils.getGDSShareCarrierTO(reservation.getGdsId());
						for(CodeShareMCFlight codeShareMCFlight: codeShareMCFlights){
							if(codeShareMCFlight.getCsMCCarrierCode().equals(gdsStatusTO.getCarrierCode())){
								csFlightNo = codeShareMCFlight.getCsMCFlightNumber();
							}
						}

						String csBookingClass = reservationSegment.getCodeShareBc();
						String segmentCode = etl.getFromAirport()+"/"+etl.getToAirport();
						Date departuerDate = etl.getDepartureDate();

						if(flightReconcileDTO!=null){
							segmentCode = flightReconcileDTO.getSegementCode();
							departuerDate = flightReconcileDTO.getDepartureDateTimeLocal();
						}						
						etlPaxNameListDTO.setCodeShareBc(csBookingClass);
						etlPaxNameListDTO.setCodeShareFlightNo(csFlightNo);
						etlPaxNameListDTO.setCabinClassCode(reservationSegment.getCabinClassCode());
						etlPaxNameListDTO.setGdsId(reservation.getGdsId());
						etlPaxNameListDTO.setUseAeroMartETS(reservation.isETRecordedInAeroMartETS());

						if (StringUtil.isNullOrEmpty(etlPaxNameListDTO.getMarketingFlight())) {
							etlPaxNameListDTO.setMarketingFlight(ReservationApiUtils.getMarketingFlightData(csFlightNo,
									csBookingClass, segmentCode, departuerDate));
						}

						if (!StringUtil.isNullOrEmpty(etlPaxNameListDTO.getEticketNumber())
								&& !StringUtil.isNullOrEmpty(etlPaxNameListDTO.getCoupNumber())) {
							EticketTO eticketTO = MessagePasserModuleUtils.getReservationBD()
									.getEticketDetailsByExtETicketNumber(etlPaxNameListDTO.getEticketNumber(),
											new Integer(etlPaxNameListDTO.getCoupNumber()));
							if (eticketTO != null) {
								etlPaxNameListDTO.setEticketNumber(eticketTO.getEticketNumber());
								etlPaxNameListDTO.setCoupNumber(eticketTO.getCouponNo().toString());
								etlPaxNameListDTO.setExternalEticketNumber(eticketTO.getExternalEticketNumber());
								etlPaxNameListDTO.setExternalCouponNo(eticketTO.getExternalCouponNo());
							}

						}

						String infEticketNo = etlPaxNameListDTO.getInfEticketNumber();
						if (!StringUtil.isNullOrEmpty(etlPaxNameListDTO.getInfEticketNumber())
								&& !StringUtil.isNullOrEmpty(etlPaxNameListDTO.getInfCoupNumber())) {
							infEticketNo = infEticketNo.replace("INF", "");
							EticketTO eticketTO = MessagePasserModuleUtils.getReservationBD()
									.getEticketDetailsByExtETicketNumber(infEticketNo,
											new Integer(etlPaxNameListDTO.getInfCoupNumber()));
							if (eticketTO != null) {
								etlPaxNameListDTO.setInfEticketNumber(eticketTO.getEticketNumber());
								etlPaxNameListDTO.setInfCoupNumber(eticketTO.getCouponNo().toString());
								etlPaxNameListDTO.setExternalInfEticketNumber(eticketTO.getExternalEticketNumber());
								etlPaxNameListDTO.setExternalInfCoupNumber(eticketTO.getExternalCouponNo());
							}

						}

					}

				}
			}
		} catch (CommonsDataAccessException e) {
			pnrErrorOccured = true;
		}

		// Checking the flight validation
		if (!flightErrorOccured) {

			// Checking the pnr validation
			if (etl.getProcessedStatus() == null
					|| ReservationInternalConstants.PfsStatus.UN_PARSED.equals(etl.getProcessedStatus())) {

				if (!pnrErrorOccured) {
					etl.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED);
				} else {
					etl.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
				}
			} else {
				etl.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
			}

		} else {
			etl.setProcessedStatus(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS);
		}
	}

	private static boolean checkCarrierCode(String carrierCode) {
		ReservationDAO reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		Collection<String> colCarrierCodes = reservationDAO.getCarrierCodes();
		for (String string : colCarrierCodes) {
			if (string.equalsIgnoreCase(carrierCode)) {
				return true;
			}
		}
		return false;
	}

	private static Flight getMarketingCarrierFlight(ETLMetaDataDTO etlMetaDataDTO) throws ModuleException {
		FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
		searchCriteria.setCsOcFlightNumber(etlMetaDataDTO.getFlightNumber());
		searchCriteria.setEstDepartureDate(etlMetaDataDTO.getDepartureDate());
		searchCriteria.setOrigin(etlMetaDataDTO.getBoardingAirport());

		return ReservationModuleUtils.getFlightBD().getSpecificFlightDetail(searchCriteria);
	}
	
	private static Set<CodeShareMCFlight> getCodeShareMCFlights(ETLMetaDataDTO etlMetaDataDTO) throws ModuleException {

		FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
		searchCriteria.setFlightNumber(etlMetaDataDTO.getFlightNumber());
		searchCriteria.setEstDepartureDate(etlMetaDataDTO.getDepartureDate());
		searchCriteria.setOrigin(etlMetaDataDTO.getBoardingAirport());

		Flight flight =  ReservationModuleUtils.getFlightBD().getSpecificFlightDetail(searchCriteria);
		if(flight == null){
			return null;
		}
		return flight.getCodeShareMCFlights();
				
	}
}