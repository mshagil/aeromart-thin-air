package com.isa.thinair.messagepasser.core.persistence.dao;

import com.isa.thinair.messagepasser.api.model.SSIM;
import com.isa.thinair.messagepasser.api.model.SSIMParsedEntry;

public interface SSIMDAO {

	public void saveSSIMEntry(SSIM ssim);

	public void saveSSIMParsedEntry(SSIMParsedEntry ssimParsedEntry);

	public void updateSSIMEntryStatus(SSIM ssim);

	public void updateSSIMParsedEntryStatus(SSIMParsedEntry ssimParsedEntry);

}
