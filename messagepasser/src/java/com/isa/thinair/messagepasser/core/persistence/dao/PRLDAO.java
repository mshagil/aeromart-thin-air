package com.isa.thinair.messagepasser.core.persistence.dao;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface PRLDAO {

	/**
	 * Returns the PRL
	 * 
	 * @param prlId
	 * @return
	 */
	public PRL getPRLEntry(int prlId);

	/**
	 * Save PRL Entry
	 * 
	 * @param prl
	 */
	public void savePRLEntry(PRL prl);

	/**
	 * Save PRL Pax Entry
	 * 
	 * @param prlPaxEntry
	 */
	public void savePRLPaxEntry(PRLPaxEntry prlPaxEntry);

	/**
	 * Save PRL Pax Entries
	 * 
	 * @param colPRLPaxEntry
	 */
	public void savePRLPaxEntries(Collection colPRLPaxEntry);

	/**
	 * Return PRL Passenger entries
	 * 
	 * @param prlId
	 * @param status
	 * @return
	 */
	public Collection getPRLPaxEntries(int prlId, String status);

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @return
	 */
	public boolean hasAnEqualPRL(String flightNumber, String airportCode, Date departureDate, Integer partNumber);

	/**
	 * Returns Page object with PRL data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 */
	public Page getPagedPRLData(List criteria, int startIndex, int noRecs, List orderByFieldList);

	/**
	 * Return prl parse entries
	 * 
	 * @param prlId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection getPRLParseEntries(int prlId) throws CommonsDataAccessException;

	/**
	 * 
	 * @param prlID
	 * @return
	 */
	public PRL getPRL(int prlID);

	public int getPRLParseEntriesCount(int prlId, String processedStatus) throws CommonsDataAccessException;

	/**
	 * Delete the prl
	 *
	 * @param prlId
	 * @throws CommonsDataAccessException
	 */
	public void deletePrlEntry(int prlId) throws CommonsDataAccessException;

	/**
	 * Delete the Prl Pax Entry
	 *
	 * @param prlPaxId
	 * @throws CommonsDataAccessException
	 */
	public void deletePrlPaxEntry(int prlPaxId) throws CommonsDataAccessException;



}
