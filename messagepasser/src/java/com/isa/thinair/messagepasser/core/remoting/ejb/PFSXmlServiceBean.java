package com.isa.thinair.messagepasser.core.remoting.ejb;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.pfs.PfsMailError;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.PerfUtil;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.service.bd.PFSXmlBDImpl;
import com.isa.thinair.messagepasser.core.service.bd.PFSXmlDBLocalImpl;
import com.isa.thinair.messagepasser.core.util.ParserUtil;
import com.sita.api.AirlineType;
import com.sita.api.AirportType;
import com.sita.api.FlightType;
import com.sita.api.MSGType;
import com.sita.api.PassengerType;
import com.sita.api.PassengersType;
import com.sita.api.PaxData;
import com.sita.api.PaxnameType;
import com.sita.api.TicketType;

@Stateless
@RemoteBinding(jndiBinding = "PFSXmlService.remote")
@LocalBinding(jndiBinding = "PFSXmlService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Clustered
public class PFSXmlServiceBean extends PlatformBaseSessionBean implements PFSXmlBDImpl, PFSXmlDBLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PFSXmlServiceBean.class);

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@TransactionTimeout(1200)
	public void processPFSXml(Date day) throws ModuleException {
		Date startDateTime = new Date();
		log.info("######### PFS XML PARSING : INITIALIZING OPERATION  ##########");

		if (day == null) {
			day = new Date();
		}

		MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();

		try {
			String sitaPackageName = "com.sita.api";
			JAXBContext jc = JAXBContext.newInstance(sitaPackageName);
			Unmarshaller um = jc.createUnmarshaller();

			List<String> fileNames = ParserUtil.downloadPfsXmls(day);
			for (String fileName : fileNames) {

				if (log.isDebugEnabled()) {
					log.debug("######### PFS XML PARSING : File  ##########" + fileName);
				}				
				try {
					PaxData paxData = (PaxData) um.unmarshal(new java.io.FileInputStream(messagePasserConfig.getPfsLocalPath()
							+ fileName));
					List<AirlineType> airlines = paxData.getAirline();
					for (AirlineType airline : airlines) {// airline

						List<FlightType> flights = airline.getFlight();
						for (FlightType flight : flights) {// flight

							List<AirportType> cities = flight.getCity();
							for (AirportType city : cities) {// cities
								processXmlPfs(airline, flight, city, fileName);
							}
						}
					}
				} catch (Exception e){
					log.error("######### PFS XML PARSING : ERROR  ########## File Name : " + fileName, e);
					ParserUtil.sendEmailNotification(e, "XML PFS Processing ERROR : " + fileName);
				}								
			}
		} catch (Exception e) {
			log.error("######### PFS XML Processing : ERROR  ##########", e);
			ParserUtil.sendEmailNotification(e, "XML PFS Processing ERROR");
		}

		Date endDateTime = new Date();
		log.info("######### PFS XML PARSING : COMPLETING THE OPERATION  ["
				+ PerfUtil.getExecutionTime(startDateTime, endDateTime) + "] ##########");
	}

	private void processXmlPfs(AirlineType airline, FlightType flight, AirportType city, String fileName) {

		Throwable exception = null;
		Integer pfsId = null;
		boolean isPfsAlreadyExists = false;

		try {
			// populate PFS info
			Pfs pfs = ParserUtil.getPfs(airline, flight, city);
			pfs.setDateDownloaded(ParserUtil.pfsFileDate(fileName));
            
			isPfsAlreadyExists = MessagePasserModuleUtils.getReservationBD()
					.checkIfPfsAlreadyExists(pfs.getDepartureDate(),
							pfs.getFlightNumber(), pfs.getFromAirport());

			if(!isPfsAlreadyExists){				
			// save PFS
			pfsId = MessagePasserModuleUtils.getReservationBD().createPfs(pfs);

			PassengersType passengers = city.getPassengers();
			List<PaxnameType> paxnames = passengers.getPaxname();
			for (PaxnameType paxname : paxnames) {

				List<PassengerType> passengersList = paxname.getPassenger();
				for (PassengerType passengerType : passengersList) {
					
					PfsPaxEntry paxEntry = ParserUtil.getPfsPaxEntry(pfs, passengerType);
					paxEntry.setLastName(paxname.getSurname());
					paxEntry.setCabinClassCode(paxname.getClazz());
					paxEntry.setArrivalAirport(paxname.getDestination());
					// assume one pnr
					if (paxname.getPNR().size() > 0) {
						paxEntry.setPnr(paxname.getPNR().get(0).getRecordLocator());
					}

					boolean isGateNoShow = isGateNoShow(paxname);
					String paxStatus = "";
					if (BeanUtils.nullHandler(passengerType.getBoardingNumber()).length() == 0
							|| (paxname.getMSG() != null && isGateNoShow)) {
						paxStatus = ReservationInternalConstants.PfsPaxStatus.NO_SHORE;

					} else if (BeanUtils.nullHandler(passengerType.getURES()).equals("Y")) {// Go shore
						paxStatus = ReservationInternalConstants.PfsPaxStatus.GO_SHORE;

					} else if (BeanUtils.nullHandler(passengerType.getNREC()).equals("Y")) {// No
						paxStatus = ReservationInternalConstants.PfsPaxStatus.NO_REC;
						updatePnr(paxEntry, passengerType);

					} else if (BeanUtils.nullHandler(passengerType.getAccepted()).equals("Y")) { // Flown
						// paxStatus = ReservationInternalConstants.PfsPaxStatus.FLOWN;
						continue;
					} else if (BeanUtils.nullHandler(passengerType.getDeleted()).equals("Y")) {// Off load
						paxStatus = ReservationInternalConstants.PfsPaxStatus.OFF_LD;
						// Process this as noshow pax as earlier implemented
						// continue;// NOT implemented in current version (Head => 28/03/2012)
					} else {
						log.error(" UnSupported PfsPaxStatus, please improve...");
					}
					if (BeanUtils.nullHandler(paxStatus).length() > 0) {
						paxEntry.setEntryStatus(paxStatus);
						paxEntry.setPfsId(pfsId);
						MessagePasserModuleUtils.getReservationBD().savePfsParseEntry(paxEntry);
					}

				}

			}
			// process pfs
			MessagePasserModuleUtils.getReservationBD().reconcileReservations(pfsId, false, true, null,
					AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
			
			} else {
				log.info("######### DUPLICATE PFS RECIEVED ########## File Name : " + fileName);
				
				String notificationMailBody = "<table> <td> <tr> " + "Duplicate PFS recieved for" + " </tr> ";
				notificationMailBody += " <tr> " + "Flight Number : " + pfs.getFlightNumber() + " </tr> ";
				notificationMailBody += " <tr> " + "Departure date : " + pfs.getDepartureDate() + " </tr> ";
				notificationMailBody += " <tr> " + "Airport : " + pfs.getFromAirport() + " </tr> ";
				notificationMailBody += " <tr> " + "File : " + fileName + " </tr> </td> </table>";

				ParserUtil.sendEmailNotification("DUPLICATE PFS RECIEVED", notificationMailBody);
			}
			
		} catch (Exception e) {
			exception = e;
		} finally {
			try {
				PfsMailError.notifyError(pfsId, exception, fileName);
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

	private boolean isGateNoShow(PaxnameType paxname) {
		List<MSGType> msgTypes = paxname.getMSG();
		if(msgTypes !=null && !msgTypes.isEmpty()){
			for(MSGType messageType : msgTypes){							
				if(ReservationInternalConstants.PfsPaxNoShowIndicators.GATE_NO_SHOW
				.equalsIgnoreCase(messageType.getMsgText())){
					return true;
				}
			}
		}
		return false;
	}
	private void updatePnr(PfsPaxEntry paxEntry, PassengerType passenger)
			throws ModuleException {
		if (paxEntry.getPnr() == null) {
			List<Object> docaOrDOCOOrDOCS = passenger.getDOCAOrDOCOOrDOCS();
			
			for(Object obj : docaOrDOCOOrDOCS){
				
				if(obj instanceof TicketType){
					TicketType passengerTkt = (TicketType) obj;
					if(passengerTkt.getTicketNumber() !=null){
						String pnr = MessagePasserModuleUtils.getReservationQueryBD().getPNRByEticketNo(
								passengerTkt.getTicketNumber());
						paxEntry.setPnr(pnr);
					}
				}
			}
		}
	}

}
