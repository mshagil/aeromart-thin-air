/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */

package com.isa.thinair.scheduledservices.api.util;

/**
 * To keep track of Reservation Internal Constants
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public final class ScheduleServicesConstants {

	/** Denotes the the Message Types */
	public static interface MessageTypes {
		public static final String PNL = "PNL";

		public static final String ADL = "ADL";
	}

	/** Denotes the the PNLADL service responce Constants */
	public static interface PNLADLServiceResponceJobStatus {
		public static final String Transmited = "T";

		public static final String NotTransmited = "N";

		public static final String PartialyTransmited = "P";
	}

	/** Denotes the the PNLADL service responce Constants */
	public static interface SchedulerJobTypes {
		public static final String Periodic = "P";

		public static final String Fixed = "F";

		public static final String Dynamic = "D";
	}

	public static interface ScheduledReportNames {
		public static final String COMPANY_PAYMENT_SUMMARY = "companyPaymentSummery";
	}
}
