package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author indika
 */
public class ReleaseBlockedSeatsFromSeatMapJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ReleaseBlockedSeatsFromSeatMap.releaseBlockedSeatsFromSeatMap();
		} catch (Exception me) {
			throw new JobExecutionException(me);
		}
	}

}
