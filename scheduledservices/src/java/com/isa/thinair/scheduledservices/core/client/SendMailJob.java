package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class SendMailJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(SendMailJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			log.info("Executing Send Mail Job.............. " + new Date());
			CredentialInvokerUtil.invokeCredentials();
			MessagingServiceBD messagingBD = ScheduledservicesUtils.getMessagingServiceBD();
			List messageList = messagingBD.loadEmails();
			if ((messageList != null) && (messageList.size() > 0)) {
				messagingBD.sendMessages(messageList);
			}
			log.info("Finished Executing Send Mail Job. " + new Date());
		} catch (Exception e) {
			log.error("Error Occured while sending mails: " + e.getMessage(), e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
