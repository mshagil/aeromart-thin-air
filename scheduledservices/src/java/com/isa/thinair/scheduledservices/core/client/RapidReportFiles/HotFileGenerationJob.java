package com.isa.thinair.scheduledservices.core.client.RapidReportFiles;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.scheduledservices.core.client.BSPSalesJob;

public class HotFileGenerationJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(BSPSalesJob.class);

	private int salesFrequencyInDays;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			ReportFileGeneration.execute(salesFrequencyInDays, ReportFileTypes.FileTypes.HOT);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Ret File Generation job failed", e);
			}
			throw new JobExecutionException(e);
		}
	}

	public int getSalesFrequencyInDays() {
		return salesFrequencyInDays;
	}

	public void setSalesFrequencyInDays(int salesFrequencyInDays) {
		this.salesFrequencyInDays = salesFrequencyInDays;
	}

}
