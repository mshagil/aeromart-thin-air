package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.AvsGenerationFlow;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Quartz Job for calling Publish BC status updates
 */
public class PublishBCStatusJob extends QuartzJobBean {

	private final Log log = LogFactory.getLog(getClass());

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try {
			log.info("############# Starting PublishBCStatusJob ");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getGDServicesBD().publishAvailabilityMessages();
			log.info("############# Finished PublishBCStatusJob ");
		} catch (Exception ce) {
			log.error("############# Error PublishBCStatusJob ", ce);
			throw new JobExecutionException(ce);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
