/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.pnl.PNLTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.scheduledservices.api.util.ScheduledServicesUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

/**
 * THIS JOB WILL RETRIEVE FLIGHT INFORMATION DEPARTURE TIMES AND WILL SCHEDULE PNLS AND ADLS
 * 
 * @author Isuru / Byorn
 * 
 */
public class PNLADLTxSchedularJob extends QuartzJobBean {

	// Init Log
	private static Log log = LogFactory.getLog(PNLADLTxSchedularJob.class);

	// method when quarts triggers this job
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			addDebugLog("############################## SCHEDULING PNLS, ADLS AND FLIGHT CLOSURE JOB IS STARTED "
					+ CalendarUtil.getCurrentSystemTimeInZulu());

			CredentialInvokerUtil.invokeCredentials();
			ReservationAuxilliaryBD auxilliaryBD = ScheduledservicesUtils.getReservationAuxilliaryBD();

			Timestamp currentTime = new Timestamp((CalendarUtil.getCurrentSystemTimeInZulu()).getTime());
			addDebugLog("RETREIVING FLIGHT PNL ADL TRNSMSN TIMES" + currentTime);
			ArrayList<PNLTransMissionDetailsDTO> list = auxilliaryBD.getFlightForPnlAdlScheduling();
			addDebugLog("RETREIVING FLIGHT PNL ADL TRNSMSN TIMES SUCCESSFUL !!!" + currentTime);

			Collections.sort(list, new LatestFirstSortComparator());

			// populating configuration data.
			GlobalConfig globalConfig = ScheduledServicesUtil.getGlobalConfig();
			int flightClosureDepartureGapMins = AppSysParamsUtil.getFlightClosureGapInMins();
			int pnlDepartureGapMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.PNL_DEPARTURE_GAP));
			int lastADLDepartureGapMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.LAST_ADL));
			int adlRepeatIntervalMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.ADL_INTERVAL));
			// Configuration data for Auto check-in
			int autoCheckinSeatMapSchdMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.ACI_SEATMAP_GAP));
			int autoCheckinSendStatusSchdMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.ACI_SEND_STATUS_GAP));

			Integer effectivePNLDepGapInMins = null;

			String jobId = null;
			int newTime = 5;
			String previousDepartureStation = "";
			int previousFlightId = -1;

			for (int i = 0; i < list.size(); i++) {
				boolean isScheduledPnl = false;
				PNLTransMissionDetailsDTO dto = list.get(i);
				jobId = dto.getFlightNumber() + "/" + dto.getDepartureStation() + "/" + dto.getDepartureTimeZulu();
				String departureStation = dto.getDepartureStation();

				// Note: 7th June - For Merging G9 and 0Y No need to check the carrier coce.
				// PNL Should be sent to all the valid flights in the System.
				// Checking the carrier code again as blocked seats interlining is started with Maroc - 08/Jul
				if (!flightIsSystemAirLine(dto.getFlightNumber())) {
					continue;
				}

				int fid = dto.getFlightId();
				Timestamp zuluFlightDepartureTime = dto.getDepartureTimeZulu();

				// do not reschedule check!
				if (previousDepartureStation.equalsIgnoreCase(departureStation) && previousFlightId == fid) {
					continue;
				}

				previousDepartureStation = departureStation;
				previousFlightId = fid;

				Timestamp flightClosureTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime()
						- flightClosureDepartureGapMins * 60 * 1000);

				effectivePNLDepGapInMins = dto.getPnlDepartureGap() != 0 ? dto.getPnlDepartureGap() : pnlDepartureGapMins;

				Timestamp pnlScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - 60 * effectivePNLDepGapInMins
						* 1000);

				// Time stamp calculation data for Auto check-in
				Timestamp autoCheckinSeatMapSchdTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - 60
						* (effectivePNLDepGapInMins + autoCheckinSeatMapSchdMins) * 1000);
				Timestamp autoCheckinSendStatusSchdTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - 60
						* (effectivePNLDepGapInMins - autoCheckinSendStatusSchdMins) * 1000);

				// If PNL scheduled time is in the past, re-schedule if eligible
				if (pnlScheduledTimeStamp.getTime() < new Date().getTime()) {
					if (!pnlAlreadySent(dto.getFlightNumber(), dto.getDepartureStation(), dto.getDepartureTimeZulu())) {
						if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.PNL)) {
							addDebugLog("PNL WAS NOT SCHEDULED FOR " + jobId + " AND PNL SHOULD HAVE SENT AT :"
									+ pnlScheduledTimeStamp.toString());
							newTime = newTime + 2;
							// Adding 2 mins to avoid possible errors from simultaneous PNL sending
							pnlScheduledTimeStamp = new Timestamp(new Date().getTime() + 60 * newTime * 1000);
							addDebugLog("NEW PNL TIME FOR " + jobId + " IS : " + pnlScheduledTimeStamp.toString());
						} else {
							log.warn("PNL WAS ALREADY SCHEDULED AND WILL NOT SCHEDULE:" + jobId);
							continue;
						}
					} else {
						log.warn("PNL WAS ALREADY SENT AND WILL NOT SCHEDULE:" + jobId);
						log.warn("ADL WILL BE SCHEDULED:" + jobId);
						newTime = newTime + 2;
						// Adding 2 mins to avoid possible errors from simultaneous PNL sending
						pnlScheduledTimeStamp = new Timestamp(new Date().getTime() + 60 * newTime * 1000);
						scheduleADLJobs(dto, lastADLDepartureGapMins, adlRepeatIntervalMins, zuluFlightDepartureTime,
								pnlScheduledTimeStamp, currentTime, jobId, fid, flightClosureTimeStamp, departureStation);
						continue;
					}
				}

				if (pnlScheduledTimeStamp.getTime() > currentTime.getTime()) {
					JobDetail pnlJobDetail = new JobDetail();
					setJobData(pnlJobDetail, jobId, SSInternalConstants.JOB_TYPE.PNL, fid, departureStation,
							dto.getFlightNumber());
					pnlJobDetail.setJobClass(PNLSender.class);
					Date pnlstartTime = CommonUtil.getDateof(pnlScheduledTimeStamp);
					try {
						addDebugLog("JOB: " + jobId + "SCHEDULING PNL. FLIGHT NUMBER: " + dto.getFlightNumber() + " PNL TIME : "
								+ pnlstartTime);
						ScheduleManager.scheduleJob(pnlJobDetail, pnlstartTime, null, 0, 0);
					} catch (SchedulerException exception) {
						log.error("JOB: " + jobId + " DID NOT SCHEDULE PNL. POSSIBLLY PNL TIME ALREADY SCHEDULED. ", exception);
					} catch (Exception e) {
						log.error("JOB: " + jobId + " DID NOT SCHEDULE PNL. POSSIBLLY PNL TIME ALREADY SCHEDULED. ", e);
						pnlJobDetail.setRequestsRecovery(true);
					}
					isScheduledPnl = true;
				}

				if (isScheduledPnl) {
					scheduleADLJobs(dto, lastADLDepartureGapMins, adlRepeatIntervalMins, zuluFlightDepartureTime,
							pnlScheduledTimeStamp, currentTime, jobId, fid, flightClosureTimeStamp, departureStation);

					if (flightClosureTimeStamp.getTime() > currentTime.getTime()) {
						JobDetail flightCloseJobDetail = new JobDetail();
						setJobData(flightCloseJobDetail, jobId, SSInternalConstants.JOB_TYPE.FLIGHT_CLOSER, fid,
								departureStation, dto.getFlightNumber());
						flightCloseJobDetail.setJobClass(FlightClosurer.class);
						java.util.Date flightclosestartTime = CommonUtil.getDateof(flightClosureTimeStamp);
						addDebugLog("JOB: " + jobId + " SCHEDULING FLIGHT CLOSURE. CLOSURE TIME : " + flightclosestartTime
								+ ", FLIGHT DEP ZULU : " + dto.getDepartureTimeZulu());
						try {
							ScheduleManager.scheduleJob(flightCloseJobDetail, flightclosestartTime, null, 0, 0);
						} catch (SchedulerException exception) {
							log.error("JOB: " + jobId + " DID NOT SCHEDULE ! POSSIBLLY FLIGHT CLOSURE ALREADY SCHEDULED",
									exception);
						}
					} else {
						log.warn("JOB: " + "NOT SCHEDULING FLIGHT CLOSURE. CLOSURE TIME ALREADY PASS. CLOSURE TIME : "
								+ flightClosureTimeStamp + ", FLIGHT DEP ZULU : " + dto.getDepartureTimeZulu());
					}

					if (AppSysParamsUtil.isMarkFlightPnrFlownAutomatically()) {
						if (ScheduledservicesUtils.getAirportBD().getAirport(departureStation).hasAutomaticFlownProcessEnabled()) {
							if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.FLIGHT_PNR_FLOWN)) {
								JobDetail pnrFlownJob = new JobDetail();
								setJobData(pnrFlownJob, jobId, SSInternalConstants.JOB_TYPE.FLIGHT_PNR_FLOWN, fid,
										departureStation, dto.getFlightNumber());
								pnrFlownJob.setJobClass(FlightBookingFlownJob.class);
								java.util.Date flightMarkFlownDateTime = CommonUtil.getDateof(dto.getDepartureTimeZulu());
								addDebugLog("JOB: " + jobId + " SCHEDULING FLIGHT PNR FLOWN. FLOWN MARK TIME : "
										+ flightMarkFlownDateTime + ", FLIGHT DEP ZULU : " + dto.getDepartureTimeZulu());
								try {
									ScheduleManager.scheduleJob(pnrFlownJob, flightMarkFlownDateTime, null, 0, 0);
								} catch (SchedulerException exception) {
									log.error("JOB: " + jobId + " DID NOT SCHEDULE ! ", exception);
								}
							}
						} else {
							addDebugLog("AIRPORT : " + departureStation + " , NOT Auto Flown Enabled");
						}
					}
					// AutomaticCheckinDetailsSender Scheduler
					if (autoCheckinSeatMapSchdTimeStamp.getTime() > currentTime.getTime()) {
						JobDetail autoCheckinSeatMapDetail = new JobDetail();
						setJobData(autoCheckinSeatMapDetail, jobId, SSInternalConstants.JOB_TYPE.AUTO_CHECKIN_SEAT_MAP, fid,
								departureStation, dto.getFlightNumber());
						autoCheckinSeatMapDetail.setJobClass(AutoCheckinSeatMapJob.class);
						Date autoCheckinSeatMapstartTime = CommonUtil.getDateof(autoCheckinSeatMapSchdTimeStamp);
						try {
							addDebugLog("JOB: " + jobId + "Autocheckin Seat Service. FLIGHT NUMBER: " + dto.getFlightNumber()
									+ " PNL TIME : " + autoCheckinSeatMapstartTime);
							ScheduleManager.scheduleJob(autoCheckinSeatMapDetail, autoCheckinSeatMapstartTime, null, 0, 0);
						} catch (SchedulerException exception) {
							log.error(
									"JOB: "
											+ jobId
											+ " DID NOT SCHEDULE Autocheckin Seat Service. POSSIBLLY Autocheckin Seat Service TIME ALREADY SCHEDULED. ",
									exception);
						} catch (Exception e) {
							log.error(
									"JOB: "
											+ jobId
											+ " DID NOT SCHEDULE Autocheckin Seat Service. POSSIBLLY Autocheckin Seat Service TIME ALREADY SCHEDULED. ",
									e);
							autoCheckinSeatMapDetail.setRequestsRecovery(true);
						}
					}
					if (autoCheckinSendStatusSchdTimeStamp.getTime() > currentTime.getTime()) {

						JobDetail sendAutoChekinJobDetail = new JobDetail();
						setJobData(sendAutoChekinJobDetail, jobId, SSInternalConstants.JOB_TYPE.AUTOMATIC_CHECKIN_SEND_DETAILS,
								fid, departureStation, dto.getFlightNumber());
						sendAutoChekinJobDetail.setJobClass(AutomaticCheckinDetailsSender.class);
						Date autoCheckinSendStatusSchdTime = CommonUtil.getDateof(autoCheckinSendStatusSchdTimeStamp);
						try {
							addDebugLog("JOB: " + jobId + "autoCheckinSendStatusSchdTime . FLIGHT NUMBER: "
									+ dto.getFlightNumber() + " autoCheckinSendStatusSchd TIME : "
									+ autoCheckinSendStatusSchdTime);
							ScheduleManager.scheduleJob(sendAutoChekinJobDetail, autoCheckinSendStatusSchdTime, null, 0, 0);
						} catch (SchedulerException exception) {
							log.error(
									"JOB: "
											+ jobId
											+ " DID NOT SCHEDULE AutomaticCheckinDetailsSender. POSSIBLLY AutomaticCheckinDetailsSender TIME ALREADY SCHEDULED. ",
									exception);
						} catch (Exception e) {
							log.error(
									"JOB: "
											+ jobId
											+ " DID NOT SCHEDULE AutomaticCheckinDetailsSender. POSSIBLLY AutomaticCheckinDetailsSender TIME ALREADY SCHEDULED. ",
									e);
							sendAutoChekinJobDetail.setRequestsRecovery(true);
						}
					}
				}
				addDebugLog(" =================================================================================== ");
			}
			addDebugLog("#######COMPLETED SCHEDULING PNLS, ADLS AND FLIGHT CLOSURES WITHOUT ANY ERRORS##################");
		} catch (Exception e) {
			log.error("ERROR OCCURED IN SCHEDULING PNL, ADL AND FLIGHT CLOSURE JOBS", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void scheduleADLJobs(PNLTransMissionDetailsDTO dto, int lastADLDepartureGapMins, int adlRepeatIntervalMins,
			Timestamp zuluFlightDepartureTime, Timestamp pnlScheduledTimeStamp, Timestamp currentTime, String jobId, int fid,
			Timestamp flightClosureTimeStamp, String departureStation) {

		Integer effectiveLastADLDepGapInMins = dto.getLastAdlGap() > 0 ? dto.getLastAdlGap() : lastADLDepartureGapMins;
		Integer effectiveADLRepeatIntervalInMins = dto.getAdlRepeatInterval() != 0
				? dto.getAdlRepeatInterval()
				: adlRepeatIntervalMins;
		Timestamp lastADLScheduledTimestamp = new Timestamp(zuluFlightDepartureTime.getTime() - effectiveLastADLDepGapInMins * 60
				* 1000);
		int totalADLToSend = 0;
		Timestamp adlScheduledTimeStamp = null;
		// Number of ADLs to send
		if (AppSysParamsUtil.isMaintainSeperateADLTransmissionIntervalAfterCutOffTime()) {
			int adlRepeatIntervalMinsAfterCutoffTime = Integer.parseInt(ScheduledServicesUtil.getGlobalConfig().getBizParam(
					SystemParamKeys.ADL_INTERVAL_AFTER_CUTOFF_TIME));
			long flightCutoffTime = dto.getFlightCutoffTime();
			Date flightCutOff = new Date(dto.getDepartureTimeZulu().getTime() - flightCutoffTime);

			int adlCountBeforeCutOffTime = 0, adlCountAfterCutOffTime = 0;
			Integer effectiveADLRepeatIntervalAfterCutoffTimeInMins = dto.getAdlRepeatIntervalAfterCutoffTime() != 0 ? dto
					.getAdlRepeatIntervalAfterCutoffTime() : adlRepeatIntervalMinsAfterCutoffTime;

			if (effectiveADLRepeatIntervalAfterCutoffTimeInMins != 0) {
				adlCountBeforeCutOffTime = (int) Math.floor(((float) (flightCutOff.getTime() - pnlScheduledTimeStamp.getTime()))
						/ (effectiveADLRepeatIntervalInMins * 60 * 1000));
				adlCountAfterCutOffTime = (int) Math
						.ceil(((float) (lastADLScheduledTimestamp.getTime() - flightCutOff.getTime()))
								/ (effectiveADLRepeatIntervalAfterCutoffTimeInMins * 60 * 1000));
				if (adlCountBeforeCutOffTime < 0) {
					adlCountBeforeCutOffTime = 0;
				}
				if (adlCountAfterCutOffTime < 0) {
					adlCountAfterCutOffTime = 0;
				}
			} else {
				adlCountBeforeCutOffTime = (int) Math.ceil(((float) (lastADLScheduledTimestamp.getTime() - pnlScheduledTimeStamp
						.getTime())) / (effectiveADLRepeatIntervalInMins * 60 * 1000));

			}

			totalADLToSend = adlCountBeforeCutOffTime + adlCountAfterCutOffTime;
			for (int k = 1; k <= totalADLToSend; k++) {
				if (k <= adlCountBeforeCutOffTime) {
					adlScheduledTimeStamp = new Timestamp(pnlScheduledTimeStamp.getTime() + (k)
							* effectiveADLRepeatIntervalInMins * 60 * 1000);
				} else if (k == totalADLToSend - adlCountAfterCutOffTime + 1) {
					adlScheduledTimeStamp = new Timestamp(flightCutOff.getTime());
				} else if (k <= totalADLToSend) {
					adlScheduledTimeStamp = new Timestamp(adlScheduledTimeStamp.getTime()
							+ effectiveADLRepeatIntervalAfterCutoffTimeInMins * 60 * 1000);
				} else {
					adlScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - effectiveLastADLDepGapInMins * 60
							* 1000);
				}

				if (adlScheduledTimeStamp.getTime() > currentTime.getTime()) {
					scheduleADLJob(jobId, k, fid, departureStation, dto.getFlightNumber(), adlScheduledTimeStamp,
							SSInternalConstants.JOB_TYPE.ADL);
				}
			}
			if (adlScheduledTimeStamp != null && adlScheduledTimeStamp.getTime() < flightClosureTimeStamp.getTime()) {
				scheduleADLJob(jobId, totalADLToSend + 1, fid, departureStation, dto.getFlightNumber(), adlScheduledTimeStamp,
						SSInternalConstants.JOB_TYPE.ADL);
			}

		} else {
			totalADLToSend = (int) Math.ceil(((float) (lastADLScheduledTimestamp.getTime() - pnlScheduledTimeStamp.getTime()))
					/ (effectiveADLRepeatIntervalInMins * 60 * 1000));

			for (int k = 1; k <= totalADLToSend; k++) {
				if (k < totalADLToSend) {
					adlScheduledTimeStamp = new Timestamp(pnlScheduledTimeStamp.getTime() + (k)
							* effectiveADLRepeatIntervalInMins * 60 * 1000);
				} else {
					adlScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - effectiveLastADLDepGapInMins * 60
							* 1000);
				}

				if (adlScheduledTimeStamp.getTime() > flightClosureTimeStamp.getTime()) {
					log.warn("JOB : " + jobId + " ADL DELIVERY IS WITHIN FLIGHT CLOSURE TIME. ADL TIME :"
							+ adlScheduledTimeStamp.toString());
				}

				if (adlScheduledTimeStamp.getTime() > currentTime.getTime()) {
					scheduleADLJob(jobId, k, fid, departureStation, dto.getFlightNumber(), adlScheduledTimeStamp,
							SSInternalConstants.JOB_TYPE.ADL);
				}
			}
		}
	}

	private void setJobData(JobDetail jobDetail, String jobId, String jobGroup, int flightId, String departureStation,
			String flightNumber) {
		jobDetail.getJobDataMap().put(Job.PROP_JOB_ID, jobId);
		jobDetail.getJobDataMap().put(Job.PROP_JOB_GROUP, jobGroup);
		jobDetail.getJobDataMap().put(Job.PROP_FlightId, String.valueOf(flightId));
		jobDetail.getJobDataMap().put(Job.PROP_DepartureStation, departureStation);
		jobDetail.getJobDataMap().put(Job.PROP_FLIGHT_NUMBER, flightNumber);
	}

	/**
	 * check if pnl already sent / has a history
	 * 
	 * @param flightNumber
	 * @param departureStation
	 * @param departuretimeZulu
	 * @return
	 */
	private boolean pnlAlreadySent(String flightNumber, String departureStation, Timestamp departuretimeZulu) {
		try {
			return ScheduledservicesUtils.getReservationAuxilliaryBD().hasPnlSentHistory(flightNumber, departuretimeZulu,
					departureStation);
		} catch (Exception exception) {
			log.error(
					"Error when checking pnl already sent! This will not interupt operation " + exception.getLocalizedMessage(),
					exception);
		}
		return false;
	}

	private class LatestFirstSortComparator implements Comparator<PNLTransMissionDetailsDTO> {
		@Override
		public int compare(PNLTransMissionDetailsDTO first, PNLTransMissionDetailsDTO second) {
			PNLTransMissionDetailsDTO transMissionDetailsDTO = first;
			PNLTransMissionDetailsDTO compareTo = second;
			return (transMissionDetailsDTO.getDepartureTimeZulu() == null || compareTo.getDepartureTimeZulu() == null)
					? 0
					: transMissionDetailsDTO.getDepartureTimeZulu().compareTo(compareTo.getDepartureTimeZulu()) * -1;
		}
	}

	private boolean flightIsSystemAirLine(String flightNumber) {
		String[] carrierCodes = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CARRIERS_TO_SEND_PNL_ADL)
				.trim().split(",");
		for (String carrierCode : carrierCodes) {
			if (carrierCode.equals(AppSysParamsUtil.extractCarrierCode(flightNumber))) {
				return true;
			}
		}
		return false;
	}

	private void addDebugLog(String logMessage) {
		if (log.isDebugEnabled()) {
			log.debug(logMessage);
		}
	}

	private void scheduleADLJob(String jobId, int count, int fid, String departureStation, String flightNumber,
			Timestamp adlScheduleTimeStamp, String jobType) {
		JobDetail adlJobDetail = new JobDetail();
		setJobData(adlJobDetail, jobId, jobType + "/" + count, fid, departureStation, flightNumber);
		adlJobDetail.setJobClass(ADLSender.class);
		java.util.Date adlstartTime = CommonUtil.getDateof(adlScheduleTimeStamp);

		try {
			addDebugLog("JOB : " + jobId + " SCHEDULING ADL. ADL TIME : " + adlstartTime);
			ScheduleManager.scheduleJob(adlJobDetail, adlstartTime, null, 0, 0);
		} catch (SchedulerException exception) {
			log.error("JOB: " + jobId + " DID NOT SCHEDULE ! POSSIBLLY ADL-" + count + " TIME ALREADY SCHEDULED : ", exception);
		}
	}
}
