package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class FareUpdateOnExchangeRateChangeAutomation {
	private static final Log log = LogFactory.getLog(FareUpdateOnExchangeRateChangeAutomation.class);

	public static void execute() throws Exception {

		try {
			log.info("######### Fare and Charge Update on Exchange Rate Change Automation Job invoked ##########");

			CredentialInvokerUtil.invokeCredentials();

			log.info("######### Fare Update on Exchange Rate Change Automation Job started at " + new Date());
			ScheduledservicesUtils.getFareBD().fareUpdateOnExchangeRateChange();
			log.info("######### Fare Update on Exchange Rate Change Automation Job ended at " + new Date());

			log.info("######### Charge Update on Exchange Rate Change Automation Job started at " + new Date());
			ScheduledservicesUtils.getchargeBD().chargeUpdateOnExchangeRateChange();
			log.info("######### Charge Update on Exchange Rate Change Automation Job ended at " + new Date());

			log.info("######### ANCI Charge Update on Exchange Rate Change Automation Job started at " + new Date());
			ScheduledservicesUtils.getAnciChargeLocalCurrBD().baseCurrAmountUpdateOnLocalCurrExchangeRtUpdate();
			log.info("######### ANCI Charge Update on Exchange Rate Change Automation Job ended at " + new Date());

		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
