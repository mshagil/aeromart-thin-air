package com.isa.thinair.scheduledservices.core.client.ancillary;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class AncillaryNotificationRecoveryJob extends QuartzJobBean {

	Log log = LogFactory.getLog(AncillaryNotificationRecoveryJob.class);

	public void executeInternal(JobExecutionContext executionContext) throws JobExecutionException {

		if (AppSysParamsUtil.isAncillaryReminderEnable()) {
			try {
				CredentialInvokerUtil.invokeCredentials();
				ReservationBD reservationBD = ScheduledservicesUtils.getReservationBD();
				FlightBD flightBD = ScheduledservicesUtils.getFlightBD();
				Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();

				log.info("START - Recovery Schedule Ancillary Reminders ::: [CurrentTime = " + currentTimestamp + "]");
				List<AncillaryReminderDetailDTO> recoveredFlightSegments = reservationBD.getFlightSegmentsToScheduleReminder(
						currentTimestamp, "RECOVERY_SCHEDULER");
				log.info("Recovery Schedule Ancillary Reminders ::: " + "[No of flight Segments Fetched = "
						+ recoveredFlightSegments.size() + "]");

				int timeFraction = 5;
				Integer flightId;
				String flightNumber;
				Integer flightSegmentId;
				Integer flightSegNotifEventId;
				Integer airportCutOverStartTime;
				String originAirport;
				String onlineCheckin;
				Date departureTimeLocal;
				Date departureTimeZulu;
				Date expectedSendNotificationTime;

				for (AncillaryReminderDetailDTO anciDetailDTO : recoveredFlightSegments) {
					String jobId = "RECOVERY/" + anciDetailDTO.getFlightOrigin() + "/" + anciDetailDTO.getFlightNumber() + "/"
							+ anciDetailDTO.getFlightId() + "/" + anciDetailDTO.getFlightSegId() + "/"
							+ anciDetailDTO.getDepartureTimeZulu();

					flightId = anciDetailDTO.getFlightId();
					flightNumber = anciDetailDTO.getFlightNumber();
					flightSegmentId = anciDetailDTO.getFlightSegId();
					flightSegNotifEventId = anciDetailDTO.getFlightSegmentNotificationEventId();
					airportCutOverStartTime = anciDetailDTO.getAnciNotificationStartCutOverTime();
					originAirport = anciDetailDTO.getFlightOrigin();
					onlineCheckin = anciDetailDTO.getAirportOnlineCheckin();
					departureTimeLocal = anciDetailDTO.getDeparturetimeLocal();
					departureTimeZulu = anciDetailDTO.getDepartureTimeZulu();
					expectedSendNotificationTime = anciDetailDTO.getNotificationReminderTime();

					Date ancilaryRemainderRescheduleTime = expectedSendNotificationTime;

					if (ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.ANCILLARY_RECOVERY_NOTIFICATION)) {
						log.info("Recovery Schedule Ancillary Reminders ::: [Skipping Scheduling Recovery Job as Scheduled JOB_ID="
								+ jobId + "]");
						continue;
					}

					log.info("Recovery Schedule Ancillary Reminders ::: [Scheduling Recovery Job For JOB_ID=" + jobId + "]");
					log.info("Recovery Schedule Ancillary Reminders ::: [Update T_FLIGHT_SEG_NOTIFICATION Table Wtih Status 'RESCHEDULED' ");
					flightBD.updateFlightSegmentNotifyStatus(flightSegmentId, flightSegNotifEventId, "RESCHEDULED", "", 0,
							ReservationInternalConstants.NotificationType.ANCI_REMINDER);

					timeFraction = timeFraction + 2;
					ancilaryRemainderRescheduleTime = new Date(
							new Timestamp(new Date().getTime() + 60 * timeFraction * 1000).getTime());

					log.info("Recovery Schedule Ancillary Reminders ::: [New Schedule Recovery time = "
							+ ancilaryRemainderRescheduleTime + "]");

					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(ancilaryRemainderRescheduleTime);

					int anciReminderYear = calendar.get(Calendar.YEAR);
					int anciReminderMonth = calendar.get(Calendar.MONTH) + 1;
					int anciReminderDate = calendar.get(Calendar.DATE);
					int anciReminderHour = calendar.get(Calendar.HOUR);
					int anciReminderMintes = calendar.get(Calendar.MINUTE);
					int anciReminderSeconds = calendar.get(Calendar.SECOND);

					JobDetail notificationJobDetail = new JobDetail();
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_JOB_ID, jobId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_JOB_GROUP,
							SSInternalConstants.JOB_TYPE.ANCILLARY_RECOVERY_NOTIFICATION);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FlightId, flightId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_DepartureStation, originAirport);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FLIGHT_NUMBER, flightNumber);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FlightSegId, flightSegmentId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID,
							flightSegNotifEventId);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_DEPARTURE_DATE_LOCAL,
							departureTimeLocal);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_DEPARTURE_DATE_ZULU,
							departureTimeZulu);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_AIRPORT_ONLINE_CHECKIN,
							onlineCheckin);
					notificationJobDetail.getJobDataMap().put(AncillaryNotificationSender.PROP_ANCILLARY_CUT_OVER_TIME,
							airportCutOverStartTime);

					notificationJobDetail.setJobClass(AncillaryNotificationSender.class);

					java.util.Date notificationRecoveryTime = CommonUtil.getDateof(anciReminderSeconds, anciReminderMintes,
							anciReminderHour, anciReminderDate, anciReminderMonth, anciReminderYear);
					try {

						ScheduleManager.scheduleJob(notificationJobDetail, notificationRecoveryTime, null, 0, 0);
						log.info("Recovery Schedule Ancillary Reminders [Scheduled Recovery Job JOB_ID = " + jobId
								+ ", Notification Recovery Send Timestamp = " + notificationRecoveryTime + "]");
					} catch (SchedulerException se) {
						log.error("Recovery Schedule Ancillary Reminders [Scheduling Recovery Job FAILED for JOB_ID=" + jobId
								+ "]", se);
						// FIXME - handle exception
					} catch (Exception e) {
						log.error("Recovery Schedule Ancillary Reminders [Scheduling Recovery Job FAILED for JOB_ID=" + jobId
								+ "]", e);
						notificationJobDetail.setRequestsRecovery(true);
					}
				}
			} catch (Exception ex) {
				log.error("Recovery Schedule Ancillary Reminders failed", ex);
				throw new JobExecutionException(ex);
			} finally {
				CredentialInvokerUtil.close();
			}
		}
		log.info("END - Recovery Schedule Ancillary Reminders [CurrentTime = " + CalendarUtil.getCurrentSystemTimeInZulu() + "]");
	}

}
