/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.reporting.api.model.ReportMetaData;
import com.isa.thinair.reporting.api.model.SRSentItem;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * Job to generate scheduled reports.
 * 
 * @author Lasantha Pambagoda
 */
public class ScheduledReportGenerationBL {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ScheduledReportGenerationBL.class);

	public void generateReport(Integer scheduledReportId) throws ModuleException {

		try {

			// if scheduledReportId found then get the retrieve the corresponding record
			ScheduledReport scheduledReport = ScheduledservicesUtils.getScheduledReportBD().getScheduledReport(scheduledReportId);

			if (scheduledReport.getStatus().equals(ScheduledReport.STATUS_STARTED)
					&& scheduledReport.getStartDate().getTime() < System.currentTimeMillis()
					&& System.currentTimeMillis() < scheduledReport.getEndDate().getTime()) {

				ReportMetaData reporName = ScheduledservicesUtils.getScheduledReportBD().getReportMetaData(
						scheduledReport.getReportName());

				ReportGenerator generator = ScheduledservicesUtils.getReportGenerator(scheduledReport.getReportName());
				String fileName = generator.generateReport(scheduledReport);
				boolean isAttachmentReport = generator.isEmailAttachmentReport();

				if (fileName != null) {

					if (log.isInfoEnabled()) {
						log.info("Befor sending the generated report" + fileName + "is attachement contains : "
								+ isAttachmentReport);
					}

					SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy 'at' HH:mm");
					HashMap<Object, Object> paramMap = new HashMap<Object, Object>();
					paramMap.put("reportName", reporName.getDisplayName());
					paramMap.put("generatedTime", format.format(new Date()));
					paramMap.put("scheduledDate", format.format(scheduledReport.getCreatedTime()));
					paramMap.put("scheduledBy", scheduledReport.getScheduledBy());
					if (isAttachmentReport) {
						sendEmailMessage(scheduledReport.getSendReportTo(), fileName, paramMap);
						if (log.isInfoEnabled()) {
							log.info("after sending the generated report as an Attachment");
						}
					} else {
						if (AppSysParamsUtil.sendFileLinkWithDomainName()) {
							paramMap.put("fileName", AppSysParamsUtil.getReportURLPRefix() + fileName);
						} else {
							paramMap.put("fileName", fileName);
						}					
						paramMap.put("downLoadURL", AppSysParamsUtil.getReportURLPRefix() + fileName);
						sendEmailMessageWithLink(scheduledReport.getSendReportTo(), fileName, paramMap);
						if (log.isInfoEnabled()) {
							log.info("after sending the generated report as an Download URL");
						}
					}

					// save the sent item info
					SRSentItem sentItem = new SRSentItem();
					sentItem.setScheduledReportId(scheduledReportId);
					sentItem.setStatus(SRSentItem.STATUS_SENT);
					sentItem.setSentTime(new Date());
					sentItem.setNotes(reporName.getDisplayName() + "sent to " + scheduledReport.getSendReportTo());
					ScheduledservicesUtils.getScheduledReportBD().saveSRSentItem(sentItem);
				}

			} else {
				if (log.isInfoEnabled()) {
					log.info("Skipping the report generation" + scheduledReportId);
				}
			}

		} catch (Exception re) {

			// save the sent item info
			SRSentItem sentItem = new SRSentItem();
			sentItem.setScheduledReportId(scheduledReportId);
			sentItem.setStatus(SRSentItem.STATUS_FAILED);
			sentItem.setSentTime(new Date());
			String notes = "";
			if (re.getMessage() != null) {
				if (re.getMessage().length() > 500) { // DB column length
					notes = re.getMessage().substring(0, 500);
				} else {
					notes = re.getMessage();
				}
			}
			if (notes == null || notes.trim().equals("")) {
				notes = "Sending schedule report failed due to unknown exception";
			}
			sentItem.setNotes(notes);
			ScheduledservicesUtils.getScheduledReportBD().saveSRSentItem(sentItem);

			log.error("Error occured ", re);
			throw new ModuleException(re.getMessage());
		}
	}

	private void sendEmailMessageWithLink(String emailID, String fileLocationURL, HashMap<Object, Object> paramMap) {
		UserMessaging userMessaging = new UserMessaging();

		userMessaging.setLastName("Scheduled Reports");
		userMessaging.setToAddres(emailID);
		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(paramMap);
		topic.setTopicName("scheduled_report_confirmation_email_link");
		topic.setAttachMessageBody(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		ScheduledservicesUtils.getMessagingServiceBD().sendMessage(messageProfile);
	}

	private void sendEmailMessage(String emailID, String attachmentFileName, HashMap<Object, Object> paramMap) {

		// User Message
		UserMessaging userMessaging = new UserMessaging();

		userMessaging.setLastName("Scheduled Reports");
		userMessaging.setToAddres(emailID);
		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(paramMap);
		topic.setTopicName("scheduled_report_confirmation_email");
		topic.setAttachMessageBody(false);
		topic.addAttachmentFileName(attachmentFileName);
		// topic.setGetAttachmentsFromFtp(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		ScheduledservicesUtils.getMessagingServiceBD().sendMessage(messageProfile);
	}
}
