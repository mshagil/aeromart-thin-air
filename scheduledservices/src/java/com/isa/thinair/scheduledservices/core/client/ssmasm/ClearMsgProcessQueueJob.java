package com.isa.thinair.scheduledservices.core.client.ssmasm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ClearMsgProcessQueueJob extends QuartzJobBean {

	private static final Log log = LogFactory.getLog(ClearMsgProcessQueueJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExcContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ClearMsgPreocessQueue.execute();
		} catch (Exception e) {
			log.error("ClearMsgProcessQueueJob ", e);
			throw new JobExecutionException(e);
		}

	}
}
