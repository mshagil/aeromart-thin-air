package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class SSMASMPublisher {
	private static final Log log = LogFactory.getLog(SSMASMPublisher.class);

	public static void execute() throws ModuleException {
		log.info("######### SSM ASM Publisher Job invoked ##########");
		log.info("######### SSM ASM Publisher Job started at " + new Date());
		ScheduledservicesUtils.getSSMASMServiceBD().publishSSMASMMessages();
		log.info("######### SSM ASM Publisher Job ended at " + new Date());
	}
}
