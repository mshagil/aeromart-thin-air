package com.isa.thinair.scheduledservices.core.client.rak;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class PublishDailyRakInsuranceInfoJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(PublishRakInsuranceInfoJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
			log.info("###################### START	- Main Scheduler Job for Daily RAK Insurace Publication [CurrentTime = "
					+ currentTimestamp + "] ######################");
			schedulePublishingDailyDetails();
			log.info("###################### END 	- Main Scheduler Job for Daily RAK Insurace Publication [CurrentTime = "
					+ currentTimestamp + "] ######################");
		} catch (Exception e) {
			log.error("Main Scheduler Job for Daily RAK Insurace Publication Failed", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void schedulePublishingDailyDetails() throws JobExecutionException {

		try {

			log.info(" ---------------------------------------------------------------------------------------");
			log.info(" #####################     DAILY RAK INSURANCE SUB JOB SCHEDULING STARTED FOR ######################");
			log.info(" ---------------------------------------------------------------------------------------");

			GregorianCalendar calendar = new GregorianCalendar();
			Date notificationStartTime = calendar.getTime();
			calendar.add(Calendar.DATE, -1);
			String jobId = "RAKPUBLISH_DAILY" + "/" + notificationStartTime;

			JobDetail notificationJobDetail = new JobDetail();
			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_ID, jobId);
			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_JOB_GROUP,
					SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION);

			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_INSURANCE_PUBLISH_TYPE,
					InsurancePublisherDetailDTO.PUBLISH_FIRST);
			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFICATION_TYPE, "INS");
			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_COUNT, 1);
			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_NOTIFY_RESULT, "");
			notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_BOOKING_DATE, calendar.getTime());
			// notificationJobDetail.getJobDataMap().put(PublishRakInsuranceInfo.PROP_IS_RECOVERY,insurancePublisherDTO.isRecovery());
			notificationJobDetail.setJobClass(PublishDailyRakInsuranceInfo.class);

			try {

				if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.RAK_INSURANCE_PUBLICATION)) {
					ScheduleManager.scheduleJob(notificationJobDetail, notificationStartTime, null, 0, 0);
					log.info("Schedule Daily Insurance Publishing [Scheduled Job JOB_ID=" + jobId
							+ ", Notification Send Timestamp=" + notificationStartTime + "]");
				} else {
					log.info("Job is already scheduled. Skipping scheduling ... [JOB_ID=" + jobId + "]");
				}

			} catch (SchedulerException se) {
				log.error("Schedule Daily Insurance Publishing[Scheduling Job FAILED for JOB_ID=" + jobId + "]", se);
			} catch (Exception e) {
				log.error("Schedule Daily Insurance Publishing [Scheduling Job FAILED for JOB_ID=" + jobId + "]", e);
				notificationJobDetail.setRequestsRecovery(true);
			}
			log.info(" #####################     DAILY PUBLISH SUB JOB SCHEDULING ENDED    ######################");

		} catch (Exception ex) {
			log.error("Schedule Dialy Insurance Publishings failed", ex);
			throw new JobExecutionException(ex);
		}
	}

}
