package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class LmsProductFileGeneratorJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(LmsProductFileGeneratorJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if (AppSysParamsUtil.isLMSEnabled()) {
				LMSFileGenerator.generateProductFile();
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("LMS Product File Generate job failed", e);
			}
			throw new JobExecutionException(e);
		}
	}

}
