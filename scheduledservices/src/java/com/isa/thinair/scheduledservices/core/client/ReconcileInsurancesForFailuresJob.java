package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author byorn
 * 
 */
public class ReconcileInsurancesForFailuresJob extends QuartzJobBean {
	private int searchGapInMinutes;

	private static final Log log = LogFactory.getLog(ReconcileInsurancesForFailuresJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		if (AppSysParamsUtil.isShowTravelInsurance()) {
			try {

				CredentialInvokerUtil.invokeCredentials();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(new Date());
				gc.add(GregorianCalendar.MINUTE, -searchGapInMinutes);

				log.info("START- reconcile insurances for failures at " + gc.getTime());
				ScheduledservicesUtils.getReservationAuxilliaryBD().reconcileInsurancesForFailures(gc.getTime());
				log.info("START- reconcile insurances for failures");

			} catch (Exception e) {
				log.error("Error in reconcile insurances for failures ", e);
				throw new JobExecutionException(e);
			} finally {
				CredentialInvokerUtil.close();
			}
		}
	}

	/**
	 * @return the searchGapInMinutes
	 */
	public int getSearchGapInMinutes() {
		return searchGapInMinutes;
	}

	/**
	 * @param searchGapInMinutes
	 *            the searchGapInMinutes to set
	 */
	public void setSearchGapInMinutes(int searchGapInMinutes) {
		this.searchGapInMinutes = searchGapInMinutes;
	}

}
