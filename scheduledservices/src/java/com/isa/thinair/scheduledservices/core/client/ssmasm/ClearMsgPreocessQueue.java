package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class ClearMsgPreocessQueue {
	private static final Log log = LogFactory.getLog(ClearMsgPreocessQueue.class);

	public static void execute() throws ModuleException {
		log.info("######### Clear Msg Preocess Queue Job invoked ##########");
		log.info("######### Clear Msg Preocess Queue Job started at " + new Date());
		ScheduledservicesUtils.getMessageBrokerServiceBD().clearInMsgProcessingQueue();
		log.info("######### Clear Msg Preocess Queue Job ended at " + new Date());
	}
}
