package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.OnHoldBookingInfoDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * 
 * @author nafly
 * 
 */
public class OHDPaymentReminderJob extends QuartzJobBean {

	private Log log = LogFactory.getLog(OHDPaymentReminderJob.class);

	@Override
	public void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			log.info("#####################  OHD PNR PAYMENT REMINDER JOB STARTED ######################");
			CredentialInvokerUtil.invokeCredentials();

			if (AppSysParamsUtil.isOHDPaymentEamilReminderEnabled()) {
				int minutesToRemind = AppSysParamsUtil.getOHDPaymentReminderDuration();
				
				Date toDate = CalendarUtil.addMinutes(new Date(), minutesToRemind);

				List<OnHoldBookingInfoDTO> bookingInfoDTOs = ScheduledservicesUtils.getReservationBD()
						.getPnrInformationToSendPaymentReminders(toDate);
				if (bookingInfoDTOs != null) {
					if (log.isDebugEnabled()) {
						log.debug("Number of PNRs to Send OHD Payment Reminders : -" + bookingInfoDTOs.size());
					}

					boolean isGroupPnr = false;
					for (OnHoldBookingInfoDTO ohdBooking : bookingInfoDTOs) {
						if (ohdBooking != null) {
							isGroupPnr = ohdBooking.getGroupPnr() != null ? true : false;

							try {
								ScheduledservicesUtils.getReservationBD().sendIBEOnholdPaymentReminderEmail(ohdBooking.getPnr(),
										isGroupPnr, false);
							} catch (Exception e) {
								// Exception is not thrown to make sure one email sending failure does not affect other
								// PNRs
								log.error("Error in sending ohd payment reminder for PNR : " + ohdBooking.getPnr(), e);
							}
						}
					}
				}

			} else {
				if (log.isDebugEnabled()) {
					log.debug("OHD Payment reminder is disabled. Hence Job is not executed");
				}
			}
			log.info("#####################  OHD PNR PAYMENT REMINDER JOB FINISHED ######################");
		} catch (Exception me) {
			if (log.isErrorEnabled()) {
				log.error("Send OHD Payment Reminder failed", me);
			}
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
