package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author chanaka
 *
 */
public class UnblockVoucherJob extends QuartzJobBean{
	
	private static Log log = LogFactory.getLog(UnblockVoucherJob.class);
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if (AppSysParamsUtil.isVoucherEnabled()) {
				CredentialInvokerUtil.invokeCredentials();
				ScheduledservicesUtils.getVoucherBD().unblockVouchers();
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Voucher Unblock job failed", e);
			}
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
		
	}

}
