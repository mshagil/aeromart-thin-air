package com.isa.thinair.scheduledservices.core.client;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PromotionRequestsApproverJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(PromotionRequestsApproverJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

		try {
			log.info(" ---- promo approver start");
			CredentialInvokerUtil.invokeCredentials();

			ScheduledservicesUtils.getPromotionAdministrationBD().approveRejectPromotionRequests(
					PromotionType.PROMOTION_NEXT_SEAT_FREE, new ArrayList<HashMap<String, Object>>());
			log.info(" ---- promo approver end");
		} catch (Exception e) {
			log.error("PromotionRequestsApproverJob ", e);
			throw new JobExecutionException(e);
		}

	}
}