package com.isa.thinair.scheduledservices.core.client.ancillary;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.airreservation.api.dto.ancilaryreminder.AncillaryReminderDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class AncillaryNotificationSender implements Job {

	private Log log = LogFactory.getLog(AncillaryNotificationSender.class);

	public void execute(JobExecutionContext executionContext) throws JobExecutionException {
		String flightNumber = null;
		Integer flightID = null;
		String flightOriginAirport = null;
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			JobDetail jobDetail = executionContext.getJobDetail();
			AncillaryReminderDetailDTO reminderDetailDTO = new AncillaryReminderDetailDTO();

			String jobName = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
			String JobGroupName = jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);
			flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
			flightID = jobDetail.getJobDataMap().getInt(Job.PROP_FlightId);
			Integer flightSegmentId = jobDetail.getJobDataMap().getInt(Job.PROP_FlightSegId);
			Integer flightSegNotiEventId = jobDetail.getJobDataMap().getInt(Job.PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID);
			flightOriginAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);
			String airportOnlineCheckin = jobDetail.getJobDataMap().getString(Job.PROP_AIRPORT_ONLINE_CHECKIN);
			Integer anciStartCutOverTime = jobDetail.getJobDataMap().getInt(Job.PROP_ANCILLARY_CUT_OVER_TIME);
			Date flightDepartureDateTimeLocal = (Date) jobDetail.getJobDataMap().get(Job.PROP_DEPARTURE_DATE_LOCAL);
			Date flightDepartureDateTimeZulu = (Date) jobDetail.getJobDataMap().get(Job.PROP_DEPARTURE_DATE_ZULU);

			reminderDetailDTO.setJobName(jobName);
			reminderDetailDTO.setJobGroupName(JobGroupName);
			reminderDetailDTO.setFlightNumber(flightNumber);
			reminderDetailDTO.setFlightId(flightID);
			reminderDetailDTO.setFlightSegId(flightSegmentId);
			reminderDetailDTO.setFlightOrigin(flightOriginAirport);
			reminderDetailDTO.setFlightSegmentNotificationEventId(null);
			reminderDetailDTO.setDeparturetimeLocal(flightDepartureDateTimeLocal);
			reminderDetailDTO.setDepartureTimeZulu(flightDepartureDateTimeZulu);
			reminderDetailDTO.setAirportOnlineCheckin(airportOnlineCheckin);
			reminderDetailDTO.setAnciNotificationStartCutOverTime(anciStartCutOverTime);
			reminderDetailDTO.setFlightSegmentNotificationEventId(flightSegNotiEventId);
			
			log.info(" ---------------------------------------------------------------------------------------");
			log.info(" #####################     ANCILLARY NOTIFICATION SUB JOB STARTED ######################");
			log.info(" #####################     NOTIFICATION FOR FLIGHT NUMBER :     " + flightNumber);
			log.info(" #####################     NOTIFICATION FOR FLIGHT ID 	: 	  " + flightID);
			log.info(" #####################     NOTIFICATION FOR FLIGHT SEG NUMBER   :     " + flightSegmentId);
			log.info(" #####################     DEPATURE STATION 				:     " + flightOriginAirport);
			log.info(" ---------------------------------------------------------------------------------------");

			sendNotification(reminderDetailDTO);
			log.info("#################### ANCILLARY NOTIFICATION SUB JOB Completed  #############################");

		} catch (Exception exception) {
			log.error("###################  EXCEPTION ARAISED IN SUB JOB WHILE SENDING REMINDER FOR FLIGHT#########");
			log.error("###################  FLIGHT ID 						: 	" + flightID);
			log.error("###################  DEPATURE STATION 				:   " + flightOriginAirport);
			log.error("###################  FLIGHT NUMBER 					:   " + flightNumber);
			log.error("###################  EXCEPTION MESSAGE 				:   " + exception.getMessage());
			log.error("###################  EXCEPTION DETAILS 				:   ");
			log.error(exception);
			throw new JobExecutionException(exception);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	private void sendNotification(AncillaryReminderDetailDTO reminderDetailDTO) throws ModuleException {
		log.info(" #####################     Giving Call to AirReservation    ######################");
		ScheduledservicesUtils.getReservationBD().sendAncillaryReminderNotification(reminderDetailDTO);
		log.info(" #####################     AirReservation Process Completed ######################");
	}

}
