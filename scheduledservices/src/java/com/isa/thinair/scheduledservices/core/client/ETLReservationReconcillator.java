package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ETLReservationReconcillator {

	private static Log log = LogFactory.getLog(ETLReservationReconcillator.class);

	public static void reconcileETLReservations() throws Exception {
		try {
			log.info("Before Reconciling the ETL ");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getETLBD().processETLMessage();
			log.info("After Reconciling the ETL ");
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
