package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class OnlineCheckInReminderRecoveryJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(OnlineCheckInReminderRecoveryJob.class);

	@Override
	public void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		CredentialInvokerUtil.invokeCredentials();

		OnlineCheckInReminderRecovery onlineCheckInReminderRecovery = new OnlineCheckInReminderRecovery();

		try {
			onlineCheckInReminderRecovery.execute();
		} catch (Exception e) {
			log.error("Error in executing online checking reminder recovery job", e);
			throw new JobExecutionException(e);
		}

	}

}
