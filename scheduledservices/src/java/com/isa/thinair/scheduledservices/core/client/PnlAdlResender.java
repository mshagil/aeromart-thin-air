package com.isa.thinair.scheduledservices.core.client;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Byorn
 */
public class PnlAdlResender {

	public static void checkAndResend() throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationAuxilliaryBD().checkAndResendPNLADL();
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
