package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * XAPnlReservationReconcillator
 * 
 * @author byorn
 * @since 1.0
 */
public class XAPnlReservationReconcillator {

	private static Log log = LogFactory.getLog(XAPnlReservationReconcillator.class);

	public static void reconcileReservations() throws Exception {
		try {
			log.info("Before Reconciling the XA reservations");
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getXApnlBD().processXAPnlReconcilation();
			log.info("After Reconciling the XA reservations");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
