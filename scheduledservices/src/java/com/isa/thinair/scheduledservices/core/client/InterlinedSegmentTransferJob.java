/**
 * 
 */
package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public class InterlinedSegmentTransferJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			InterlinedSegmentTransfer.execute(null);
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}
}
