package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author byorn //for 'direct agents' agents without reporting_to_gsa //retrieves invoices with 'unsettled' from
 *         t_invoice, period - taken from app param //if the number of invoices unsettled => to app_param then disable
 *         the "direct" agent
 */
public class BlackListAgentsJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(BlackListAgentsJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			BlackListAgents.execute(new Date());
		} catch (Exception me) {
			throw new JobExecutionException(me);
		}
	}
}
