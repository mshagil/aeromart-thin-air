package com.isa.thinair.scheduledservices.core.client.agentnotify;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * Scheduler job to notify when BSP credit limit usage of agents reached predefined limit
 * 
 * @author rumesh
 * 
 */
public class BSPAgentNotifierJob extends QuartzJobBean {

	private Log log = LogFactory.getLog(BSPAgentNotifierJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			if (AppSysParamsUtil.isBSPCreditLimitAlertingEnabled()) {
				log.info("#####################  BSP CREDIT UTILIZATION NOTIFICATION JOB STARTED ######################");

				ScheduledservicesUtils.getTravelAgentFinanceBD().sendBSPCreditLimitUtilizationEmail();

				log.info("#####################  BSP CREDIT UTILIZATION NOTIFICATION JOB COMPLETED ######################");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("BSP credit utilization notifier failed", e);
			}
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

}
