/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

/**
 * @author isuru Module = Reservation Interface = ReservationAuxilliaryBD
 */
public class PNLSender implements Job {
	private Log log = LogFactory.getLog(PNLSender.class);

	/**
	 * JOB EXCECUTION CONTEXT
	 */
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();

			String flightId = jobDetail.getJobDataMap().getString(Job.PROP_FlightId);
			String depAirport = jobDetail.getJobDataMap().getString(Job.PROP_DepartureStation);

			String flightNumber = jobDetail.getJobDataMap().getString(Job.PROP_FLIGHT_NUMBER);
			
			log.info(" #####################     PNL JOB STARTED ######################");
			log.info(" ----------------------------------------------------------------");
			log.info(" #####################     PNL FOR FLIGHT ID : " + flightId);
			log.info(" #####################     DEPATURE STATION : " + depAirport);
			log.info(" #####################     FLIGHT NUMBER : " + flightNumber);
			log.info(" ----------------------------------------------------------------");
			sendPNL(Integer.parseInt(flightId), depAirport);

			log.info("#################### PNLSender Completed  #############################");
		} catch (Exception me) {
			log.error("send PNL failed", me);
			throw new JobExecutionException(me);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

	/**
	 * sendPNL Method
	 * 
	 * @param flightId
	 * @param departureStation
	 * @param mdto
	 * @return
	 * @throws ModuleException
	 * @throws Exception
	 */
	public void sendPNL(int flightId, String departureStation) throws ModuleException, Exception {
		ScheduledservicesUtils.getReservationAuxilliaryBD().sendPNL(flightId, departureStation);
	}

}
