package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class PFSXmlProcessor {

	private static Log log = LogFactory.getLog(PFSXmlProcessor.class);

	public static void processPFSXmls(Date day) throws Exception {
		try {
			log.info("Before processing the PFS xml ");
			CredentialInvokerUtil.invokeCredentials();

			// download and filter files
			ScheduledservicesUtils.getPfsXmlBD().processPFSXml(day);
			log.info("After processing the PFS xml ");
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
