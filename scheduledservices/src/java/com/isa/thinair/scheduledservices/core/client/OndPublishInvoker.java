package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.OndPublishToCachingBL;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * 
 * @author jpadukka
 * 
 */
public class OndPublishInvoker {

	private static final Log log = LogFactory.getLog(OndPublishInvoker.class);

	public void execute() throws ModuleException {

		log.info("#######################################################");
		log.info("########## Starting OnD pubish invoker #######");

		CredentialInvokerUtil.invokeCredentials();

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			log.info("########## OnD pubslishing via LCC... ######");
			Boolean result = invokeLccForOndPublishing();
			if (result != null && result) {
				log.info("########## OnD pubslishing via LCC successful ######");
			} else {
				log.info("########## OnD pubslishing via LCC failed ######");
			}
		} else {
			log.info("########## OnD pubslishing via Own airline... ######");
			publishOndForSystem();
		}

		log.info("########## Completed OnD pubish invoker ######");
		log.info("#######################################################");
	}

	/**
	 * Invokes lcc ws to publish ond combinations
	 * 
	 * @throws ModuleException
	 */
	private Boolean invokeLccForOndPublishing() throws ModuleException {
		try {

			return ScheduledservicesUtils.getLCCMasterDataPublisherBD().publishOndCombinationsViaLCC(
					AppSysParamsUtil.getCarrierCode());
		} catch (Exception e) {
			log.error("Failed while Publishing OnDs via LCC", e);
			throw new ModuleException(e, "lccclient.ondpublish.invocation.error");
		}
	}

	/**
	 * Own airline Ond pulishing
	 * 
	 * @throws ModuleException
	 */
	private void publishOndForSystem() throws ModuleException {
		
		try {
			OndPublishedTO publishedOnds = ScheduledservicesUtils.getCommonMasterBD().getOndListForSystem();

			if (publishedOnds != null && publishedOnds.getOrigins() != null && publishedOnds.getOrigins().size() > 0) {

				boolean cachingEnable = AppSysParamsUtil.isCachingEnableforOndPublished();
				if (cachingEnable) {
					OndPublishToCachingBL casheBL =  new OndPublishToCachingBL();
					casheBL.prepareListForCaching(publishedOnds);
				}

				ScheduledservicesUtils.getCommonMasterBD().publishOndListAsJavascript(publishedOnds);
			} else {
				log.info("No Onds for publishing for own airline");
			}
		} catch (Exception e) {
			log.error("Failed while Publishing OnDs for Own Airline", e);
			throw new ModuleException(e, "ondpublish.invocation.error");
		}
	}
}
