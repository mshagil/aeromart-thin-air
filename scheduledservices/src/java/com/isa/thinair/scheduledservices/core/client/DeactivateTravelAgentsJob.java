package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.api.exception.ModuleException;

public class DeactivateTravelAgentsJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		DeactivateTravelAgents deactivateTravelAgents = new DeactivateTravelAgents();
		try {
			deactivateTravelAgents.execute();
		} catch (ModuleException e) {
			throw new JobExecutionException(e);
		}
	}
}
