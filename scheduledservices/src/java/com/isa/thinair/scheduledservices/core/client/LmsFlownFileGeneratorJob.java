package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author rumesh
 * 
 */
public class LmsFlownFileGeneratorJob extends QuartzJobBean {
	private static Log log = LogFactory.getLog(LmsFlownFileGeneratorJob.class);

	private int salesFrequencyInDays = 1;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if (AppSysParamsUtil.isLMSEnabled()) {
				LMSFileGenerator.generateFlownFile(salesFrequencyInDays);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("LMS Flown File Generate job failed", e);
			}
			throw new JobExecutionException(e);
		}
	}

	public int getSalesFrequencyInDays() {
		return salesFrequencyInDays;
	}

	public void setSalesFrequencyInDays(int salesFrequencyInDays) {
		this.salesFrequencyInDays = salesFrequencyInDays;
	}

}
