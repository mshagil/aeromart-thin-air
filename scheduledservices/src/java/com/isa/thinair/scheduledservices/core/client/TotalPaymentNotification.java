package com.isa.thinair.scheduledservices.core.client;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class TotalPaymentNotification {
	private static final Log log = LogFactory.getLog(OnlineCheckInReminder.class);

	public void execute() throws JobExecutionException {

		try {

			// SchedulerConfig sc = (SchedulerConfig) SchedulerUtils.getInstance().getModuleConfig();
			Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
			log.debug("****************************************************************************************************************");
			log.debug("****************************************************************************************************************");
			log.debug("****************************************************************************************************************");
			log.info("START - Total payment due notification [CurrentTime = "
					+ new SimpleDateFormat("ddMMyy-HH:mm:ss").format(currentTimestamp) + "]");
			sendNotification();

		} catch (Exception ex) {
			log.error("Total payment due Reminders failed", ex);

		} finally {
			CredentialInvokerUtil.close();
		}

	}

	private void sendNotification() throws ModuleException {
		log.debug(" #####################     Giving Call to AirReservation    ######################");
		ScheduledservicesUtils.getReservationBD().sendTotalPaymentDueReminder();
		log.debug(" #####################     AirReservation Process Completed ######################");
	}

}
