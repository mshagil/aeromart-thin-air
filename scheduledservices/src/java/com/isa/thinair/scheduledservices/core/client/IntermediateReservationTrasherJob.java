package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class IntermediateReservationTrasherJob extends QuartzJobBean {

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {
			IntermediateReservationTrasher.clearTempBCAlloc();
		} catch (Exception ee) {
			throw new JobExecutionException(ee);
		}
	}

}
