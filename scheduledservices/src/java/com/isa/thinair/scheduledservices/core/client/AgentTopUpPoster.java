package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author lsandeepa
 */
public class AgentTopUpPoster {

	/**
	 * get the agent top up transactions frrom t_agent_transaction.
	 * save to T_AGENT_TOP_UP_TRANSFER in internal sys datablase.
	 * save list to external INT_T_AGENT_TOP_UP datablse.
	 * update status of T_AGENT_TOP_UP_TRANSFER if external was updated.
	 * 
	 * @throws ModuleException
	 */
	public static void transferAgentTopUp(Date date) throws Exception {
		
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationAuxilliaryBD().transferAgentTopUp(date);
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
