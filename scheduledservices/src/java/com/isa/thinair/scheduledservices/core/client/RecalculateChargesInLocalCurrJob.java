package com.isa.thinair.scheduledservices.core.client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Re-calculate charges with exchange rate change
 * 
 * @author Noshani Perera
 */
public class RecalculateChargesInLocalCurrJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(RecalculateChargesInLocalCurrJob.class);

	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			RecalculateChargesInLocalCurr.execute(null);
		} catch (Exception e) {
			log.error("Error in recalculateChargesInLocalCurr", e);
			throw new JobExecutionException(e);
		}
	}

}
