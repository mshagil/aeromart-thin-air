package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class ReleaseLockInMessageProcessingQueue {

	private static final Log log = LogFactory.getLog(ReleaseLockInMessageProcessingQueue.class);
	
	public static void executeJob() {

		log.info("######### Release Lock InMessage Processing Queue Job invoked ##########");
		log.info("######### Release Lock InMessage Processing Queue Job started at " + new Date());
		try {
		
			Collection<Integer> msgQueueIdList = retreiveAllLockedMsgQueues();
			
			if (msgQueueIdList != null && !msgQueueIdList.isEmpty()) {
				for (Integer lockedMsgQueueId : msgQueueIdList) {
					Integer nextMsgIdToProcess = retreiveNextMessageFromWaitingQueue(lockedMsgQueueId);
					if (nextMsgIdToProcess != null) {
						updateNextInMsgId(lockedMsgQueueId, nextMsgIdToProcess);
						removeMsgFromQueue(nextMsgIdToProcess, lockedMsgQueueId);
						callPostMessageMdbInvoker(nextMsgIdToProcess);

					}else{
						updateFlightForUnlockStatus(lockedMsgQueueId);
					}
				}
			}

		} catch (ModuleException ex) {
			log.error("Error : Release Lock InMessage Processing Queue Job" + ex);
		}

		log.info("######### End invokation : Release Lock InMessage Processing Queue Job ##########");
	}

	/**
	 * 
	 * @param nextMsgIdIntheQueue
	 * @throws ModuleException
	 */
	private static void callPostMessageMdbInvoker(Integer nextMsgIdIntheQueue) throws ModuleException {
		ScheduledservicesUtils.getMessageBrokerServiceBD().postMessageMdbInvoker(nextMsgIdIntheQueue);
	}
	
	private static Collection<Integer> retreiveAllLockedMsgQueues() throws ModuleException {
		return ScheduledservicesUtils.getMessageBrokerServiceBD().allLockedMsgQueues();
	}
	
	private static Integer retreiveNextMessageFromWaitingQueue(Integer lockedQueue) throws ModuleException {
		return ScheduledservicesUtils.getMessageBrokerServiceBD().getNextMessageIdFromWaitingQueue(lockedQueue);
	}
	
	
	private static void updateNextInMsgId(Integer lockedQueue, int inMsgID) throws ModuleException {
		ScheduledservicesUtils.getMessageBrokerServiceBD().updateNextInMsgId(lockedQueue, inMsgID);
	}

	private static void removeMsgFromQueue(Integer inMessageId, Integer lockedMsg) throws ModuleException {

		ScheduledservicesUtils.getMessageBrokerServiceBD().removeMsgFromQueue(inMessageId, lockedMsg);
		log.info("######### Removed message ##########" + inMessageId);

	}
	
	private static void updateFlightForUnlockStatus( Integer lockedMsg){
		String msgStatus = "U";
		ScheduledservicesUtils.getMessageBrokerServiceBD().releaseLockedScheduleFlight(msgStatus, lockedMsg);;
	}
	
	
}
