package com.isa.thinair.scheduledservices.core.client.lcc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

/**
 * The Class LCCAirportInfoPublisher.
 */
public class LCCMasterDataPublisher {

	/** The Constant log. */
	private static final Log log = LogFactory.getLog(LCCMasterDataPublisher.class);

	/**
	 * Execute.
	 * 
	 * @throws ModuleException
	 *             the module exception
	 */
	public static void execute() throws ModuleException {

		if (AppSysParamsUtil.isLCCConnectivityEnabled() && AppSysParamsUtil.isLCCDataSyncEnabled()) {
			log.info("#######################################################");
			log.info("########## Starting AA-LCC MasterData Publishing #######");

			ScheduledservicesUtils.getCommonMasterBD().publishMasterDataUpdates();

			log.info("########## Completed AA-LCC MasterData Publishing ######");
			log.info("#######################################################");
		}

	}

}
