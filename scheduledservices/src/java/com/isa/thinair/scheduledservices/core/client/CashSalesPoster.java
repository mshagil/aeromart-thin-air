/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author isuru
 */
public class CashSalesPoster {

	/**
	 * Method to check transfer cash sales.
	 * 
	 * @throws ModuleException
	 */
	public static void transferCashSales(Date date) throws Exception {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ScheduledservicesUtils.getReservationAuxilliaryBD().transferCashSales(date);
		} catch (Exception e) {
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
