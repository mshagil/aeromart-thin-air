package com.isa.thinair.scheduledservices.core.client.masterdata.exchangerates;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class ExchangeRateAutomation {
	private static final Log log = LogFactory.getLog(ExchangeRateAutomation.class);

	public static void execute() throws ModuleException {
		log.info("######### Exchange Rate Automation Job invoked ##########");
		log.info("######### Exchange Rate Automation Job started at " + new Date());
		ScheduledservicesUtils.getCommonMasterBD().updateExchangeRate();
		log.info("######### Exchange Rate Automation Job ended at " + new Date());
	}
}
