package com.isa.thinair.scheduledservices.core.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.airreservation.api.dto.pal.PALTransMissionDetailsDTO;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.scheduledservices.api.util.ScheduledServicesUtil;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CommonUtil;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduledservices.core.util.SSInternalConstants;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

public class PALCALTxSchedularJob extends QuartzJobBean {

	private static Log log = LogFactory.getLog(PALCALTxSchedularJob.class);

	GlobalConfig globalConfig;
	int flightClosureDepartureGapMins;
	int minimunPalDepartureGapMins;
	int lastCALDepartureGapMins;
	int minimumCalRepeatIntervalMins;

	@Override
	public void executeInternal(JobExecutionContext arg0) throws JobExecutionException {

		try {

			addDebugLog("############################## SCHEDULING PALS, CALS AND JOB IS STARTED "
					+ CalendarUtil.getCurrentSystemTimeInZulu());

			CredentialInvokerUtil.invokeCredentials();

			Timestamp currentTime = new Timestamp((CalendarUtil.getCurrentSystemTimeInZulu()).getTime());

			ArrayList<PALTransMissionDetailsDTO> palTranmitionDTOList = getPALTransmitionDTOList(currentTime);

			Collections.sort(palTranmitionDTOList, new LatestFirstSortComparator());

			setGlobalParameters();
			Integer effectivePALDepGapInMins = null;
			Integer effectiveCALRepeatIntervalInMins = null;
			Integer effectiveLastCALDepGapInMins = null;

			String jobId = null;
			int newTime = 5;
			String previousDepartureStation = "";
			int previousFlightId = -1;

			for (PALTransMissionDetailsDTO transmitionDTO : palTranmitionDTOList) {

				boolean isScheduledPal = false;

				jobId = compostJobId(transmitionDTO);

				String departureStation = transmitionDTO.getDepartureStation();

				if (!isCarrierAllowedToSendPalCal(transmitionDTO.getFlightNumber())) {
					continue;
				}

				int flightID = transmitionDTO.getFlightId();
				Timestamp zuluFlightDepartureTime = transmitionDTO.getDepartureTimeZulu();

				if (previousDepartureStation.equalsIgnoreCase(departureStation) && previousFlightId == flightID) {
					continue;
				}

				previousDepartureStation = departureStation;
				previousFlightId = flightID;

				Timestamp flightClosureTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime()
						- flightClosureDepartureGapMins * 60 * 1000);

				effectivePALDepGapInMins = transmitionDTO.getPalDepartureGap() != 0
						? transmitionDTO.getPalDepartureGap()
						: minimunPalDepartureGapMins;

				Timestamp palScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - 60 * effectivePALDepGapInMins
						* 1000);

				// If PAL scheduled time is in the past, re-schedule if eligible
				if (palScheduledTimeStamp.getTime() < new Date().getTime()) {

					if (!isPalAlreadySent(transmitionDTO.getFlightNumber(), transmitionDTO.getDepartureStation(),
							transmitionDTO.getDepartureTimeZulu())) {

						if (!ScheduleManager.jobWasScheduled(jobId, SSInternalConstants.JOB_TYPE.PAL)) {

							// Adding 2 mins to avoid possible errors from simultaneous PAL sending
							newTime = newTime + 2;
							palScheduledTimeStamp = getpalScheduledTimeStamp(jobId, newTime, palScheduledTimeStamp);

						} else {
							log.warn("PAL WAS ALREADY SCHEDULED AND WILL NOT SCHEDULE:" + jobId);
							continue;
						}
					} else {
						log.warn("PAL WAS ALREADY SENT AND WILL NOT SCHEDULE:" + jobId);
						continue;
					}
				}

				if (palScheduledTimeStamp.getTime() > currentTime.getTime()) {
					schedulePALJob(jobId, transmitionDTO, departureStation, flightID, palScheduledTimeStamp);
					isScheduledPal = true;
				}

				if (isScheduledPal) {
					effectiveLastCALDepGapInMins = geteffectiveLastCALDepGapInMins(transmitionDTO);

					effectiveCALRepeatIntervalInMins = geteffectiveCALRepeatIntervalInMins(transmitionDTO);

					Timestamp lastCALScheduledTimestamp = getlastCALScheduledTimestamp(effectiveLastCALDepGapInMins,
							zuluFlightDepartureTime);

					Timestamp calScheduledTimeStamp = null;

					if (AppSysParamsUtil.isMaintainSeperateCALTransmissionIntervalAfterCutOffTime()) {

						handleCALForDifferentTransmissionIntervals(currentTime, effectiveCALRepeatIntervalInMins,
								effectiveLastCALDepGapInMins, jobId, transmitionDTO, departureStation, flightID,
								zuluFlightDepartureTime, flightClosureTimeStamp, palScheduledTimeStamp,
								lastCALScheduledTimestamp, calScheduledTimeStamp);

					} else {
						handleCALForUniqueTransmissionInterval(currentTime, effectiveCALRepeatIntervalInMins,
								effectiveLastCALDepGapInMins, jobId, transmitionDTO, departureStation, flightID,
								zuluFlightDepartureTime, flightClosureTimeStamp, palScheduledTimeStamp, lastCALScheduledTimestamp);
					}
				}
				addDebugLog(" =================================================================================== ");
			}
			addDebugLog("#######COMPLETED SCHEDULING PALS, CALS WITHOUT ANY ERRORS##################");

		} catch (Exception e) {
			log.error("ERROR OCCURED IN SCHEDULING PAL, CAL JOBS", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}

	}

	private void handleCALForUniqueTransmissionInterval(Timestamp currentTime, Integer effectiveCALRepeatIntervalInMins,
			Integer effectiveLastCALDepGapInMins, String jobId, PALTransMissionDetailsDTO transmitionDTO,
			String departureStation, int flightID, Timestamp zuluFlightDepartureTime, Timestamp flightClosureTimeStamp,
			Timestamp palScheduledTimeStamp, Timestamp lastCALScheduledTimestamp) {
		int totalCALToSend;
		Timestamp calScheduledTimeStamp;
		totalCALToSend = (int) Math.ceil(((float) (lastCALScheduledTimestamp.getTime() - palScheduledTimeStamp.getTime()))
				/ (effectiveCALRepeatIntervalInMins * 60 * 1000));

		for (int k = 1; k <= totalCALToSend; k++) {

			calScheduledTimeStamp = getcalScheduledTimeStamp(effectiveCALRepeatIntervalInMins, effectiveLastCALDepGapInMins,
					zuluFlightDepartureTime, palScheduledTimeStamp, totalCALToSend, null, null, 0, 0, 0, k);

			if (calScheduledTimeStamp.getTime() > flightClosureTimeStamp.getTime()) {
				log.warn("JOB : " + jobId + " CAL DELIVERY IS WITHIN FLIGHT CLOSURE TIME. CAL TIME :"
						+ calScheduledTimeStamp.toString());
			}

			if (calScheduledTimeStamp.getTime() > currentTime.getTime()) {
				scheduleCALJob(jobId, k, flightID, departureStation, transmitionDTO.getFlightNumber(), calScheduledTimeStamp);
			}
		}
	}

	private void handleCALForDifferentTransmissionIntervals(Timestamp currentTime, Integer effectiveCALRepeatIntervalInMins,
			Integer effectiveLastCALDepGapInMins, String jobId, PALTransMissionDetailsDTO transmitionDTO,
			String departureStation, int flightID, Timestamp zuluFlightDepartureTime, Timestamp flightClosureTimeStamp,
			Timestamp palScheduledTimeStamp, Timestamp lastCALScheduledTimestamp, Timestamp calScheduledTimeStamp) {
		int totalCALToSend;
		int calRepeatIntervalMinsAfterCutoffTime = Integer.parseInt(globalConfig
				.getBizParam(SystemParamKeys.CAL_INTERVAL_AFTER_CUTOFF_TIME));

		long flightCutoffTime = transmitionDTO.getFlightCutoffTime();

		Date flightCutOff = new Date(transmitionDTO.getDepartureTimeZulu().getTime() - flightCutoffTime);

		Integer effectiveCALRepeatIntervalAfterCutoffTimeInMins = geteffectiveCALRepeatIntervalAfterCutoffTimeInMins(
				transmitionDTO, calRepeatIntervalMinsAfterCutoffTime);

		int calCountBeforeCutOffTime = getCalCountBeforeCutOffTime(effectiveCALRepeatIntervalAfterCutoffTimeInMins, flightCutOff,
				palScheduledTimeStamp, effectiveCALRepeatIntervalInMins, lastCALScheduledTimestamp);

		int calCountAfterCutOffTime = getCalCountAfterCutOffTime(effectiveCALRepeatIntervalAfterCutoffTimeInMins, flightCutOff,
				palScheduledTimeStamp, effectiveCALRepeatIntervalInMins, lastCALScheduledTimestamp);

		totalCALToSend = calCountBeforeCutOffTime + calCountAfterCutOffTime;
		for (int k = 1; k <= totalCALToSend; k++) {

			calScheduledTimeStamp = getcalScheduledTimeStamp(effectiveCALRepeatIntervalInMins, effectiveLastCALDepGapInMins,
					zuluFlightDepartureTime, palScheduledTimeStamp, totalCALToSend, calScheduledTimeStamp, flightCutOff,
					effectiveCALRepeatIntervalAfterCutoffTimeInMins, calCountBeforeCutOffTime, calCountAfterCutOffTime, k);

			if (calScheduledTimeStamp.getTime() > currentTime.getTime()) {
				scheduleCALJob(jobId, k, flightID, departureStation, transmitionDTO.getFlightNumber(), calScheduledTimeStamp);
			}
		}
		if (calScheduledTimeStamp != null && calScheduledTimeStamp.getTime() < flightClosureTimeStamp.getTime()) {
			scheduleCALJob(jobId, totalCALToSend + 1, flightID, departureStation, transmitionDTO.getFlightNumber(),
					calScheduledTimeStamp);
		}
	}

	private Timestamp getpalScheduledTimeStamp(String jobId, int newTime, Timestamp palScheduledTimeStamp) {
		addDebugLog("PAL WAS NOT SCHEDULED FOR " + jobId + " AND PAL SHOULD HAVE SENT AT :" + palScheduledTimeStamp.toString());

		palScheduledTimeStamp = new Timestamp(new Date().getTime() + 60 * newTime * 1000);

		addDebugLog("NEW PAL TIME FOR " + jobId + " IS : " + palScheduledTimeStamp.toString());
		return palScheduledTimeStamp;
	}

	private Timestamp getcalScheduledTimeStamp(Integer effectiveCALRepeatIntervalInMins, Integer effectiveLastCALDepGapInMins,
			Timestamp zuluFlightDepartureTime, Timestamp palScheduledTimeStamp, int totalCALToSend,
			Timestamp calScheduledTimeStamp, Date flightCutOff, Integer effectiveCALRepeatIntervalAfterCutoffTimeInMins,
			int calCountBeforeCutOffTime, int calCountAfterCutOffTime, int k) {

		if (AppSysParamsUtil.isMaintainSeperateCALTransmissionIntervalAfterCutOffTime()) {
			if (k <= calCountBeforeCutOffTime) {
				calScheduledTimeStamp = new Timestamp(palScheduledTimeStamp.getTime() + (k) * effectiveCALRepeatIntervalInMins
						* 60 * 1000);
			} else if (k == totalCALToSend - calCountAfterCutOffTime + 1) {
				calScheduledTimeStamp = new Timestamp(flightCutOff.getTime());
			} else if (k <= totalCALToSend) {
				calScheduledTimeStamp = new Timestamp(calScheduledTimeStamp.getTime()
						+ effectiveCALRepeatIntervalAfterCutoffTimeInMins * 60 * 1000);
			} else {
				calScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - effectiveLastCALDepGapInMins * 60
						* 1000);
			}
		} else {
			if (k < totalCALToSend) {
				calScheduledTimeStamp = new Timestamp(palScheduledTimeStamp.getTime() + (k) * effectiveCALRepeatIntervalInMins
						* 60 * 1000);
			} else {
				calScheduledTimeStamp = new Timestamp(zuluFlightDepartureTime.getTime() - effectiveLastCALDepGapInMins * 60
						* 1000);
			}
		}
		return calScheduledTimeStamp;
	}

	private Timestamp getlastCALScheduledTimestamp(Integer effectiveLastCALDepGapInMins, Timestamp zuluFlightDepartureTime) {
		Timestamp lastCALScheduledTimestamp = new Timestamp(zuluFlightDepartureTime.getTime() - effectiveLastCALDepGapInMins * 60
				* 1000);
		return lastCALScheduledTimestamp;
	}

	private Integer geteffectiveCALRepeatIntervalInMins(PALTransMissionDetailsDTO transmitionDTO) {
		Integer effectiveCALRepeatIntervalInMins;
		effectiveCALRepeatIntervalInMins = transmitionDTO.getCalRepeatInterval() != 0
				? transmitionDTO.getCalRepeatInterval()
				: minimumCalRepeatIntervalMins;
		return effectiveCALRepeatIntervalInMins;
	}

	private Integer geteffectiveLastCALDepGapInMins(PALTransMissionDetailsDTO transmitionDTO) {
		Integer effectiveLastCALDepGapInMins;
		effectiveLastCALDepGapInMins = transmitionDTO.getLastCalGap() > 0
				? transmitionDTO.getLastCalGap()
				: lastCALDepartureGapMins;
		return effectiveLastCALDepGapInMins;
	}

	private int getCalCountBeforeCutOffTime(int effectiveCALRepeatIntervalAfterCutoffTimeInMins, Date flightCutOff,
			Timestamp palScheduledTimeStamp, Integer effectiveCALRepeatIntervalInMins, Timestamp lastCALScheduledTimestamp) {
		int calCountBeforeCutOffTime = 0;

		if (effectiveCALRepeatIntervalAfterCutoffTimeInMins != 0) {
			calCountBeforeCutOffTime = (int) Math.floor(((float) (flightCutOff.getTime() - palScheduledTimeStamp.getTime()))
					/ (effectiveCALRepeatIntervalInMins * 60 * 1000));

			if (calCountBeforeCutOffTime < 0) {
				calCountBeforeCutOffTime = 0;
			}

		} else {
			calCountBeforeCutOffTime = (int) Math.ceil(((float) (lastCALScheduledTimestamp.getTime() - palScheduledTimeStamp
					.getTime())) / (effectiveCALRepeatIntervalInMins * 60 * 1000));

		}
		return calCountBeforeCutOffTime;
	}

	private int getCalCountAfterCutOffTime(int effectiveCALRepeatIntervalAfterCutoffTimeInMins, Date flightCutOff,
			Timestamp palScheduledTimeStamp, Integer effectiveCALRepeatIntervalInMins, Timestamp lastCALScheduledTimestamp) {

		int calCountAfterCutOffTime = 0;

		if (effectiveCALRepeatIntervalAfterCutoffTimeInMins != 0) {

			calCountAfterCutOffTime = (int) Math.ceil(((float) (lastCALScheduledTimestamp.getTime() - flightCutOff.getTime()))
					/ (effectiveCALRepeatIntervalAfterCutoffTimeInMins * 60 * 1000));

			if (calCountAfterCutOffTime < 0) {
				calCountAfterCutOffTime = 0;
			}
		}
		return calCountAfterCutOffTime;
	}

	private Integer geteffectiveCALRepeatIntervalAfterCutoffTimeInMins(PALTransMissionDetailsDTO transmitionDTO,
			int calRepeatIntervalMinsAfterCutoffTime) {
		Integer effectiveCALRepeatIntervalAfterCutoffTimeInMins = transmitionDTO.getCalRepeatIntervalAfterCutoffTime() != 0
				? transmitionDTO.getCalRepeatIntervalAfterCutoffTime()
				: calRepeatIntervalMinsAfterCutoffTime;
		return effectiveCALRepeatIntervalAfterCutoffTimeInMins;
	}

	private void schedulePALJob(String jobId, PALTransMissionDetailsDTO transmitionDTO, String departureStation, int flightID,
			Timestamp palScheduledTimeStamp) {
		JobDetail palJobDetail = new JobDetail();
		setJobData(palJobDetail, jobId, SSInternalConstants.JOB_TYPE.PAL, flightID, departureStation,
				transmitionDTO.getFlightNumber());
		palJobDetail.setJobClass(PALSender.class);
		Date palstartTime = CommonUtil.getDateof(palScheduledTimeStamp);
		try {
			addDebugLog("JOB: " + jobId + "SCHEDULING PAL. FLIGHT NUMBER: " + transmitionDTO.getFlightNumber() + " PAL TIME : "
					+ palstartTime);
			ScheduleManager.scheduleJob(palJobDetail, palstartTime, null, 0, 0);
		} catch (SchedulerException exception) {
			log.error("JOB: " + jobId + " DID NOT SCHEDULE PAL. POSSIBLLY PAL TIME ALREADY SCHEDULED. ", exception);
		} catch (Exception e) {
			log.error("JOB: " + jobId + " DID NOT SCHEDULE PAL. POSSIBLLY PAL TIME ALREADY SCHEDULED. ", e);
			palJobDetail.setRequestsRecovery(true);
		}
	}

	private String compostJobId(PALTransMissionDetailsDTO transmitionDTO) {

		String jobId = "PALCAL_" + transmitionDTO.getFlightNumber() + "/" + transmitionDTO.getDepartureStation() + "/"
				+ transmitionDTO.getDepartureTimeZulu();
		return jobId;
	}

	private void setGlobalParameters() {
		this.globalConfig = ScheduledServicesUtil.getGlobalConfig();
		this.flightClosureDepartureGapMins = AppSysParamsUtil.getFlightClosureGapInMins();
		this.minimunPalDepartureGapMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.PAL_DEPATURE_GAP));
		this.lastCALDepartureGapMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.LAST_CAL));
		this.minimumCalRepeatIntervalMins = Integer.parseInt(globalConfig.getBizParam(SystemParamKeys.CAL_INTERVAL));
	}

	private ArrayList<PALTransMissionDetailsDTO> getPALTransmitionDTOList(Timestamp currentTime) throws ModuleException {
		addDebugLog("RETREIVING FLIGHT PAL CAL TRNSMSN TIMES" + currentTime);

		ReservationAuxilliaryBD auxilliaryBD = ScheduledservicesUtils.getReservationAuxilliaryBD();
		ArrayList<PALTransMissionDetailsDTO> palTranmitionDTOList = auxilliaryBD.getFlightForPalCalScheduling();

		addDebugLog("RETREIVING FLIGHT PAL CAL TRNSMSN TIMES SUCCESSFUL !!!" + currentTime);
		return palTranmitionDTOList;
	}

	private void addDebugLog(String logMessage) {
		if (log.isDebugEnabled()) {
			log.debug(logMessage);
		}
	}

	private class LatestFirstSortComparator implements Comparator<PALTransMissionDetailsDTO> {
		@Override
		public int compare(PALTransMissionDetailsDTO first, PALTransMissionDetailsDTO second) {
			PALTransMissionDetailsDTO transMissionDetailsDTO = first;
			PALTransMissionDetailsDTO compareTo = second;
			return (transMissionDetailsDTO.getDepartureTimeZulu() == null || compareTo.getDepartureTimeZulu() == null)
					? 0
					: transMissionDetailsDTO.getDepartureTimeZulu().compareTo(compareTo.getDepartureTimeZulu()) * -1;
		}
	}

	private boolean isCarrierAllowedToSendPalCal(String flightNumber) {
		String[] carrierCodes = ReservationModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CARRIERS_TO_SEND_PAL_CAL)
				.trim().split(",");
		for (String carrierCode : carrierCodes) {
			if (carrierCode.equals(AppSysParamsUtil.extractCarrierCode(flightNumber))) {
				return true;
			}
		}
		return false;
	}

	private boolean isPalAlreadySent(String flightNumber, String departureStation, Timestamp departuretimeZulu) {
		try {
			return ScheduledservicesUtils.getReservationAuxilliaryBD().hasPalSentHistory(flightNumber, departuretimeZulu,
					departureStation);
		} catch (Exception exception) {
			log.error(
					"Error when checking pal already sent! This will not interupt operation " + exception.getLocalizedMessage(),
					exception);
		}
		return false;
	}

	private void scheduleCALJob(String jobId, int count, int fid, String departureStation, String flightNumber,
			Timestamp calScheduleTimeStamp) {
		JobDetail calJobDetail = new JobDetail();
		setJobData(calJobDetail, jobId, SSInternalConstants.JOB_TYPE.CAL + "/" + count, fid, departureStation, flightNumber);
		calJobDetail.setJobClass(CALSender.class);
		Date calstartTime = CommonUtil.getDateof(calScheduleTimeStamp);

		try {
			addDebugLog("JOB : " + jobId + " SCHEDULING CAL. CAL TIME : " + calstartTime);
			ScheduleManager.scheduleJob(calJobDetail, calstartTime, null, 0, 0);
		} catch (SchedulerException exception) {
			log.error("JOB: " + jobId + " DID NOT SCHEDULE ! POSSIBLLY CAL-" + count + " TIME ALREADY SCHEDULED : ", exception);
		}
	}

	private void setJobData(JobDetail jobDetail, String jobId, String jobGroup, int flightId, String departureStation,
			String flightNumber) {
		jobDetail.getJobDataMap().put(Job.PROP_JOB_ID, jobId);
		jobDetail.getJobDataMap().put(Job.PROP_JOB_GROUP, jobGroup);
		jobDetail.getJobDataMap().put(Job.PROP_FlightId, String.valueOf(flightId));
		jobDetail.getJobDataMap().put(Job.PROP_DepartureStation, departureStation);
		jobDetail.getJobDataMap().put(Job.PROP_FLIGHT_NUMBER, flightNumber);
	}

}
