/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduledservices.core.client;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionException;

import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public class InterlinedSegmentTransfer {
	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(InterlinedSegmentTransfer.class);

	/**
	 * Process the segment transfer
	 * 
	 * @param executionDate
	 * @throws ModuleException
	 */
	private static void processSegmentTransfer(Date executionDate) throws ModuleException {
		log.info("########## Transfer Initiated ##########");
		Collection interlinedAirLines = ScheduledservicesUtils.getGlobalConfig().getInterlinedAirlines();
		InterlinedAirLineTO interlinedAirLineTO;
		for (Iterator itr = interlinedAirLines.iterator(); itr.hasNext();) {
			interlinedAirLineTO = (InterlinedAirLineTO) itr.next();

			log.info("########## Started processing for " + interlinedAirLineTO.getCarrierCode() + " ######");
			ScheduledservicesUtils.getSegmentBD().processInterlinedSegments(executionDate, interlinedAirLineTO);
			log.info("########## Completed processing for " + interlinedAirLineTO.getCarrierCode() + " ######");
		}
		log.info("########## Transfer Completed ##########");
	}

	/**
	 * Notify segment transfer failures
	 * 
	 * @param executionDate
	 * @throws ModuleException
	 */
	private static void notifySegmentTransferFailures(Date executionDate) throws ModuleException {
		ScheduledservicesUtils.getSegmentBD().notifySegmentTransferFailures(executionDate);
	}

	/**
	 * Main execution
	 * 
	 * @param executionDate
	 * @return
	 * @throws ModuleException
	 */
	public static void execute(Date executionDate) throws Exception {
		
		try {
			log.info("#######################################################");
			log.info("########## Starting Code Share Segment Transfer #######");

			CredentialInvokerUtil.invokeCredentials();

			if (executionDate == null) {
				executionDate = new Date();
			}

			log.info("########## Started " + executionDate + " ######");

			processSegmentTransfer(executionDate);

			notifySegmentTransferFailures(new Date());

			log.info("########## Completed Code Share Segment Transfer ######");
			log.info("#######################################################");
		} catch (Exception e) {
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}
}
