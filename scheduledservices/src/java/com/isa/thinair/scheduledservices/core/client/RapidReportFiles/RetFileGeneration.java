package com.isa.thinair.scheduledservices.core.client.RapidReportFiles;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.invoicing.api.model.bsp.ReportFileTypes;
import com.isa.thinair.scheduledservices.core.client.BSPSales;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class RetFileGeneration {
	private static Log log = LogFactory.getLog(BSPSales.class);

	public static void execute(int salesFrequncyInDays, ReportFileTypes.FileTypes file) throws Exception {
		log.info("######### BSP Sales Job invoked ##########");
		try {
			CredentialInvokerUtil.invokeCredentials();

			Date date = new Date();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
			date = fmt.parse(fmt.format(date)); // Convert the date to yyyy/MM/dd 00:00

			// toDate = yyyy/MM/(dd-1) 11:59
			Date toDate = new Date(date.getTime() - TimeUnit.SECONDS.toMillis(1));

			// fromDate = (yyyy/MM/(dd-salesFrequncyInDays) 00:00)
			Date fromDate = new Date(date.getTime() - TimeUnit.DAYS.toMillis(salesFrequncyInDays));
			log.info("######### Ret file generation Job started for RET file generation at " + new Date() + "for sales Frequency"
					+ salesFrequncyInDays);
			ScheduledservicesUtils.getInvoicingBD().generateReportFile(fromDate, toDate, false, file);
			log.info("######### Ret file generation Job ended RET file generation at " + new Date());
		} catch (Exception e) {
			log.error(e);
			// CommonUtil.reportSheduleFailure(e, "BSP RET File creation failed!");
		}

		try {
			log.info("######### BSP Sales Job started RET file upload ##########");
			ScheduledservicesUtils.getInvoicingBD().uploadPendingRETFiles();
			log.info("######### BSP Sales Job ended for RET file upload ##########");

		} catch (Exception e) {
			log.error(e);
			// CommonUtil.reportSheduleFailure(e, "BSP Scheduler Failed!");
			throw e;
		} finally {
			CredentialInvokerUtil.close();
		}
		log.info("######### BSP Sales Job ended at " + new Date());
	}
}
