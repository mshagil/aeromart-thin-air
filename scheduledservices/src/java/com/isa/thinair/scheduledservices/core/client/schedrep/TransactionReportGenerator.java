package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;

public class TransactionReportGenerator extends BaseRowsetGenerator {

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		ResultSet resultSet = null;
		String reportOption = reportsSearchCriteria.getReportOption();
		Boolean isDetailReport = reportsSearchCriteria.isDetailReport();
		
		if (reportOption.equals("CC_TRAN") && isDetailReport) {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getPerDayCCFraudDetails(reportsSearchCriteria);
	
		} else if (reportOption.equals("CC_TOPUP") ) {

			resultSet = ScheduledservicesUtils.getDataExtractionBD().getPerDayCCFraudDetails(reportsSearchCriteria);

		}
		else if (reportOption.equals("CC_REFUND") && isDetailReport) {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getPerDayCCFraudDetails(reportsSearchCriteria);
		
		} else if (reportOption.equals("CC_TRAN")) {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getCCTransactionDetails(reportsSearchCriteria);
		
		} else if (reportOption.equals("CC_TRAN_DETAIL")) {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getPerDayCCFraudDetails(reportsSearchCriteria);
		
		} else if (reportOption.equals("CC_REFUND")) {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getCCTransactionRefundDetails(reportsSearchCriteria);
		
		} else if (reportOption.equals("CC_REFUND_DETAIL")) {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getPerDayCCFraudDetails(reportsSearchCriteria);
		
		} else {
			
			resultSet = ScheduledservicesUtils.getDataExtractionBD().getCCTransactionPendingRefundDetails(reportsSearchCriteria);
		}
		
		return resultSet;
	}

	@Override
	protected SimpleDateFormat getReportsSearchCriteriaDateFormat() {
		return new SimpleDateFormat("dd-MMM-yyyy");
	}

}
