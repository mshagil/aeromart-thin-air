package com.isa.thinair.scheduledservices.core.client.ssmasm;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;
import com.isa.thinair.scheduler.api.Job;

public class SendSSMRecapJob extends QuartzJobBean {
	
	private static final Log log = LogFactory.getLog(SendSSMRecapJob.class);

	@Override
	protected void executeInternal(JobExecutionContext jobExcContext) throws JobExecutionException {

		try {
			CredentialInvokerUtil.invokeCredentials();
			org.quartz.JobDetail jobDetail = jobExcContext.getJobDetail();
			Date scheduleStartDate = (Date)jobDetail.getJobDataMap().get(Job.PROP_GDS_SSMRECAP_SCHED_START_DATE);			
			Date scheduleEndDate = (Date)jobDetail.getJobDataMap().get(Job.PROP_GDS_SSMRECAP_SCHED_END_DATE);
			boolean isSelectedSchedules = jobDetail.getJobDataMap().getBoolean(Job.PROP_GDS_IS_SELECTED_SCHED);
			
			if (jobDetail.getJobDataMap().getBoolean(Job.PROP_EXTERNAL_SSMRECAP_SENDING)) {
				String externalEmailAddress = jobDetail.getJobDataMap().getString(Job.PROP_EXTERNAL_SSMRECAP_EMAIL);
				String externalSitaAddress = jobDetail.getJobDataMap().getString(Job.PROP_EXTERNAL_SSMRECAP_SITA);
				SendSSMRecapExternal.execute(scheduleStartDate, scheduleEndDate, isSelectedSchedules, externalEmailAddress,
						externalSitaAddress);
			} else {
				int gdsID = jobDetail.getJobDataMap().getInt(Job.PROP_GDS_ID);
				String emailAddress = jobDetail.getJobDataMap().getString(Job.PROP_GDS_SSMRECAP_EMAIL);
				SendSSMRecap.execute(gdsID, scheduleStartDate, scheduleEndDate, isSelectedSchedules, emailAddress);
			}
			
		} catch (Exception e) {
			log.error("SSMRecapJob ", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	
		
	}

}
