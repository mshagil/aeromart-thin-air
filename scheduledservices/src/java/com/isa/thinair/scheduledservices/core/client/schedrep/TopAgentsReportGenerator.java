package com.isa.thinair.scheduledservices.core.client.schedrep;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.scheduledservices.api.dto.ReportConfig;
import com.isa.thinair.scheduledservices.core.service.ScheduledservicesUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class TopAgentsReportGenerator extends BaseRowsetGenerator {

	private static Log log = LogFactory.getLog(TopAgentsReportGenerator.class);

	protected String generatedReportFileUniqueID;

	@Override
	public ResultSet getResultSet(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return ScheduledservicesUtils.getDataExtractionBD().getTopAgentsData(reportsSearchCriteria);
	}

	public String generateReport(ScheduledReport scheduledReport) throws ModuleException {

		String zipFileName = null;
		generatedReportFileUniqueID = generateUniqueID();
		// regenerate the reportsSearchCriteria from the parameter template
		XStream xstream = new XStream(new DomDriver());
		ReportsSearchCriteria reportsSearchCriteria = (ReportsSearchCriteria) xstream.fromXML(scheduledReport
				.getCriteriaTemplete());
		Map<String, Object> parameters = (Map<String, Object>) xstream.fromXML(scheduledReport.getParamTemplete());

		// inject the date range

		injectRecalulatedParams(reportsSearchCriteria, parameters, scheduledReport);
		
		SimpleDateFormat format = getReportsSearchCriteriaDateFormat();
		
		try {
			
			reportsSearchCriteria.setDateFrom(new Timestamp(format.parse(reportsSearchCriteria.getDateRangeFrom()).getTime()));
			reportsSearchCriteria.setDateTo(new Timestamp(format.parse(reportsSearchCriteria.getDateRangeTo()).getTime()));
			
		} catch (ParseException e) {
			log.debug("Date parse exception");
			e.printStackTrace();
			throw new ModuleException(e.getMessage());
		}
		

		// register the template
		String reportPath = PlatformConstants.getConfigRootAbsPath() + scheduledReport.getReportTempleteRelativePath();
		ScheduledservicesUtils.getReportingFrameworkBD().storeJasperTemplateRefs(scheduledReport.getReportName(), reportPath);

		ResultSet resultSet = getResultSet(reportsSearchCriteria);

		// generate the report and save it in the report store and email it
		if (resultSet != null) {

			byte[] reportBytes = this.createReport(scheduledReport, parameters, resultSet, new HashMap<Object, Object>());

			Collection<String> allDetailReports = new ArrayList<String>();
			String fileName = createFileName(scheduledReport);
			allDetailReports.add(fileName);
			
			FTPServerProperty serverProperty = new FTPServerProperty();
			FTPUtil ftpUtil = new FTPUtil();

			// Code to FTP
			serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
			if (ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName() == null) {
				serverProperty.setUserName("");
			} else {
				serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
			}
			serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
			if (ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword() == null) {
				serverProperty.setPassword("");
			} else {
				serverProperty.setPassword(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword());
			}
			ftpUtil.setPassiveMode(ScheduledservicesUtils.getGlobalConfig().isFtpEnablePassiveMode());

			if(scheduledReport.getReportName() != null && 
					(scheduledReport.getReportName().equalsIgnoreCase("NILTransactionsAgentsSummary")
							|| scheduledReport.getReportName().equalsIgnoreCase("transactionReport"))){
				ftpUtil.uploadAttachmentWithOutFTP(fileName, reportBytes);
			}
			else{
				ftpUtil.uploadAttachment(fileName, reportBytes, serverProperty);
			}			
			
			zipFileName = generateZipFile(allDetailReports, scheduledReport.getReportName());

			if (getReportConfig().getDeliveryMode() == ReportConfig.DeliveryMode.LINK) {
				copyReportsToReportsStore(zipFileName);
			}
		}
		return zipFileName;

	}

	protected String createFileName(ScheduledReport scheduledReport) {
		String fileExtension = ".pdf";
		if (HTML.equals(scheduledReport.getReportFormat())) {
			fileExtension = ".html";
		} else if (CSV.equals(scheduledReport.getReportFormat()) || CSV_AGENT.equals(scheduledReport.getReportFormat())) {
			fileExtension = ".csv";
		} else if (EXCEL.equals(scheduledReport.getReportFormat())) {
			fileExtension = ".xls";
		}
		return scheduledReport.getReportName() + "_" + generatedReportFileUniqueID + fileExtension;
	}

	private String generateZipFile(Collection<String> filesNames, String reportName) {

		String zipFileName = null;
		String attachmentName = null;
		try {
			attachmentName = reportName + generatedReportFileUniqueID + ".zip";
			zipFileName = PlatformConstants.getAbsAttachmentsPath() + "/" + attachmentName;
			FileOutputStream fos = new FileOutputStream(zipFileName);
			ZipOutputStream zos = new ZipOutputStream(fos);

			for (String fileName : filesNames) {
				fileName = PlatformConstants.getAbsAttachmentsPath() + "/" + fileName;
				addToZipFile(fileName, zos);
			}
			zos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			log.error("Error while creating zip file, fnf", e);
		} catch (IOException e) {
			log.error("Error while creating zip file, io", e);
		}

		uploadZipFileToFtpServer(attachmentName, zipFileName);

		return attachmentName;
	}

	private void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(fileName);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}
		file.delete();
		zos.closeEntry();
		fis.close();
	}

	private void uploadZipFileToFtpServer(String remote, String local) {
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		serverProperty.setServerName(ScheduledservicesUtils.getGlobalConfig().getFtpServerName());
		if (ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
		}
		serverProperty.setUserName(ScheduledservicesUtils.getGlobalConfig().getFtpServerUserName());
		if (ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(ScheduledservicesUtils.getGlobalConfig().getFtpServerPassword());
		}

		ftpUtil.setPassiveMode(ScheduledservicesUtils.getGlobalConfig().isFtpEnablePassiveMode());
		ftpUtil.uploadZipFile(remote, local, serverProperty);

	}

	public String getGeneratedReportFileUniqueID() {
		return generatedReportFileUniqueID;
	}

	public void setGeneratedReportFileUniqueID(String generatedReportFileUniqueID) {
		this.generatedReportFileUniqueID = generatedReportFileUniqueID;
	}

}