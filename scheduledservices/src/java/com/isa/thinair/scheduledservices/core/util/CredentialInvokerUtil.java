package com.isa.thinair.scheduledservices.core.util;

import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.scheduler.api.utils.SchedulerConstants;
import com.isa.thinair.scheduler.core.config.SchedulerConfig;

public class CredentialInvokerUtil {

	public static void invokeCredentials() {
		SchedulerConfig schedulerConfig = (SchedulerConfig) LookupServiceFactory.getInstance().getModuleConfig(
				SchedulerConstants.MODULE_NAME);
		String userName = schedulerConfig.getUsername();
		String password = schedulerConfig.getPassword();

		ForceLoginInvoker.login(userName, password);
	}
	
	
	public static void close() {
		ForceLoginInvoker.close();
	}

}
