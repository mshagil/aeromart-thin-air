package com.isa.thinair.scheduledservices.core.client.masterdata.exchangerates;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduledservices.core.util.CredentialInvokerUtil;

public class ExchangeRateAutomationJob extends QuartzJobBean {
	private static final Log log = LogFactory.getLog(ExchangeRateAutomationJob.class);

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			CredentialInvokerUtil.invokeCredentials();
			ExchangeRateAutomation.execute();
		} catch (Exception e) {
			log.error("ExchangeRateAutomationJob", e);
			throw new JobExecutionException(e);
		} finally {
			CredentialInvokerUtil.close();
		}
	}

}
