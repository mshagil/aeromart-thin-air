package com.isa.thinair.scheduledservices.core.client;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * 
 * @author jpadukka
 * 
 */
public class OndPublishInvokerJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		OndPublishInvoker ondPublishInvoker = new OndPublishInvoker();
		try {
			ondPublishInvoker.execute();
		} catch (Exception ex) {
			throw new JobExecutionException(ex);
		}
	}

}
