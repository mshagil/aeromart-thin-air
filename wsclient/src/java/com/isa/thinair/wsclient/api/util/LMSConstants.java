package com.isa.thinair.wsclient.api.util;

/**
 * Keep all constants related to LMS Integration
 * 
 * @author rumesh
 * 
 */
public final class LMSConstants {

	public static interface Module {
		public static final String CUSTOMER_FIELD = "CUSTOMER_FIELD";
		public static final String MEMBER = "MEMBER";
		public static final String MEMBER_ACTIVITY = "MEMBER_ACTIVITY";
		public static final String MEMBER_CONTACT = "MEMBER_CONTACT";
		public static final String MEMBER_SECURITY = "MEMBER_SECURITY";
		public static final String OFFER = "OFFER";
		public static final String PORTAL = "PORTAL";
		public static final String REFERRER = "REFERRER";
		public static final String REWARD = "REWARD";
		public static final String SUPPORT = "SUPPORT";
		public static final String MESSAGE = "MESSAGE";
	}

	public static interface WSReponse {
		public static final int RESPONSE_CODE_OK = 0;
		public static final int MEMBER_NOT_FOUND = 5000;
		public static final int DUPLICATE_PHONE_NUM = 315;
		public static final int DUPLICATE_ACCOUNT_ID = 318;
	}

	public static interface MemberCustomField {
		public static final String HEAD_OF_HOUSE = "HEAD_OF_HOUSE";
		public static final String NATIONALITY_CODE = "NATIONALITY_CODE";
		public static final String RESIDENCY_CODE = "RESIDENCY_CODE";
	}

	public static interface MemberContactField {
		public static final Integer VALID = 1;
		public static final Integer INVALID = 0;
	}

	public static enum MemberTitle {
		MR("MR", 1), MSTR("MSTR", 1), MS("MS", 2), MRS("MRS", 2), MISS("MISS", 2);

		private String title;
		private int titleId;

		private MemberTitle(String title, int titleId) {
			this.title = title;
			this.titleId = titleId;
		}

		public String getTitle() {
			return this.title;
		}

		public int getTitleId() {
			return this.titleId;
		}
	}

	public static interface MemberState {
		public static final int IS_LOYALTY_MEMBER = 1;
		public static final int MEMBER_SUSPENDED = 1;

	}

	public static enum LocationExternalReference {
		REDEEM_IBE("Web", 'W');

		private String locationReference;
		private char appIndicator;

		private LocationExternalReference(String locationReference, char appIndicator) {
			this.locationReference = locationReference;
			this.appIndicator = appIndicator;
		}

		public String getLocationReference() {
			return locationReference;
		}

		public char getAppIndicator() {
			return appIndicator;
		}

	}

	public static enum MenuExternalReference {
		EARN("EARN", "EARN"), REDEEM("REDEEM", "REDEEM"), FAQS("FAQS", "FAQS"), RETRO("RETRO", "RETRO");

		private String ExternalReference;
		private String internalReference;

		private MenuExternalReference(String locationReference, String appIndicator) {
			this.ExternalReference = locationReference;
			this.internalReference = appIndicator;
		}

		public String getExternalReference() {
			return ExternalReference;
		}

		public String getInternalReference() {
			return internalReference;
		}
	}
	
	public static enum LMS_SERVICE_PROVIDER {
		SMART_BUTTON, AERO_REWARD, GRAVTY;
	}

}
