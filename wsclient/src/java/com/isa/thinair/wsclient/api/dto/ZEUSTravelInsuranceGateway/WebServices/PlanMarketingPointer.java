/**
 * PlanMarketingPointer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class PlanMarketingPointer  implements java.io.Serializable {
    private int pointID;

    private java.lang.String pointDesc;

    public PlanMarketingPointer() {
    }

    public PlanMarketingPointer(
           int pointID,
           java.lang.String pointDesc) {
           this.pointID = pointID;
           this.pointDesc = pointDesc;
    }


    /**
     * Gets the pointID value for this PlanMarketingPointer.
     * 
     * @return pointID
     */
    public int getPointID() {
        return pointID;
    }


    /**
     * Sets the pointID value for this PlanMarketingPointer.
     * 
     * @param pointID
     */
    public void setPointID(int pointID) {
        this.pointID = pointID;
    }


    /**
     * Gets the pointDesc value for this PlanMarketingPointer.
     * 
     * @return pointDesc
     */
    public java.lang.String getPointDesc() {
        return pointDesc;
    }


    /**
     * Sets the pointDesc value for this PlanMarketingPointer.
     * 
     * @param pointDesc
     */
    public void setPointDesc(java.lang.String pointDesc) {
        this.pointDesc = pointDesc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PlanMarketingPointer)) return false;
        PlanMarketingPointer other = (PlanMarketingPointer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.pointID == other.getPointID() &&
            ((this.pointDesc==null && other.getPointDesc()==null) || 
             (this.pointDesc!=null &&
              this.pointDesc.equals(other.getPointDesc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getPointID();
        if (getPointDesc() != null) {
            _hashCode += getPointDesc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PlanMarketingPointer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanMarketingPointer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PointID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PointDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
