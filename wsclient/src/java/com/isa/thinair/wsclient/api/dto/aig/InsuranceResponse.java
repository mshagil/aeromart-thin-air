package com.isa.thinair.wsclient.api.dto.aig;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.InsuranceTypes;
import com.isa.thinair.wsclient.api.dto.InsuranceProductConfig;

public class InsuranceResponse implements Serializable {

	private static final long serialVersionUID = 2088361985036821131L;

	private boolean success;
	private String errorCode;
	private String errorMessage;
	private String messageId;

	private String quotedCurrencyCode;

	private BigDecimal totalPremiumAmount;
	private BigDecimal quotedTotalPremiumAmount;

	// derived information
	private BigDecimal amount;
	private BigDecimal quotedAmount;
	private BigDecimal taxAmount;
	private BigDecimal quotedTaxAmout;
	private BigDecimal netAmount;
	private BigDecimal quotedNetAmount;

	private String policyCode;

	private Map<InsuranceTypes, BigDecimal> insTypeCharges;
	private boolean isEuropianCountry;

	private String termNCondition;
	private String planInfoDescription;
	private String planNoConsideration;

	private String ssrFeeCode;
	private String planCode;
	private String planDesc;
	private String planCoverdInfoDescription;

	private InsuranceProductConfig insProductConfig;

	private String planDescription;

	private Integer insuranceRefId;
	
	private Map<String, String> insuranceExternalContent;

	public InsuranceResponse() {
	}

	public InsuranceResponse(boolean isSuccess, String errorCode, String errorMessage, String messageId,
			BigDecimal totalPremiumAmount, BigDecimal quotedTotalPremiumAmount, String quotedCurrencyCode) {
		this.success = isSuccess;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.messageId = messageId;
		this.totalPremiumAmount = totalPremiumAmount;
		this.quotedTotalPremiumAmount = quotedTotalPremiumAmount;
		this.quotedCurrencyCode = quotedCurrencyCode;
	}

	public InsuranceResponse(boolean isSuccess, String errorCode, String errorMessage, String messageId,
			BigDecimal totalPremiumAmount, BigDecimal quotedTotalPremiumAmount, String quotedCurrencyCode,
			boolean insuranceViewChange) {
		this.success = isSuccess;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.messageId = messageId;
		this.totalPremiumAmount = totalPremiumAmount;
		this.quotedTotalPremiumAmount = quotedTotalPremiumAmount;
		this.quotedCurrencyCode = quotedCurrencyCode;
		this.isEuropianCountry = insuranceViewChange;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public void setTotalPremiumAmount(BigDecimal totalPremiumAmount) {
		this.totalPremiumAmount = totalPremiumAmount;
	}

	public void setEuropianCountry(boolean isEuropianCountry) {
		this.isEuropianCountry = isEuropianCountry;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getMessageId() {
		return messageId;
	}

	public BigDecimal getTotalPremiumAmount() {
		return totalPremiumAmount;
	}

	/**
	 * @return the policyCode
	 */
	public String getPolicyCode() {
		return policyCode;
	}

	/**
	 * @param policyCode
	 *            the policyCode to set
	 */
	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public void setAdditionalErrorMessage(String errorMsg) {
		if (this.errorMessage != null) {
			this.errorMessage = errorMsg + "|" + this.errorMessage;
		} else {
			this.errorMessage = errorMsg;
		}
	}

	/**
	 * @return the quotedTotalPremiumAmount
	 */
	public BigDecimal getQuotedTotalPremiumAmount() {
		return quotedTotalPremiumAmount;
	}

	/**
	 * @param quotedTotalPremiumAmount
	 *            the quotedTotalPremiumAmount to set
	 */
	public void setQuotedTotalPremiumAmount(BigDecimal quotedTotalPremiumAmount) {
		this.quotedTotalPremiumAmount = quotedTotalPremiumAmount;
	}

	/**
	 * @return the quotedCurrencyCode
	 */
	public String getQuotedCurrencyCode() {
		return quotedCurrencyCode;
	}

	/**
	 * @param quotedCurrencyCode
	 *            the quotedCurrencyCode to set
	 */
	public void setQuotedCurrencyCode(String quotedCurrencyCode) {
		this.quotedCurrencyCode = quotedCurrencyCode;
	}

	public Map<InsuranceTypes, BigDecimal> getInsTypeCharges() {
		return insTypeCharges;
	}

	public void setInsTypeCharges(Map<InsuranceTypes, BigDecimal> insTypeCharges) {
		this.insTypeCharges = insTypeCharges;
	}

	public boolean isEuropianCountry() {
		return isEuropianCountry;
	}

	public void setIsEuropianCountry(boolean insuranceViewChange) {
		this.isEuropianCountry = insuranceViewChange;
	}

	public String getTermNCondition() {
		return termNCondition;
	}

	public void setTermNCondition(String termNCondition) {
		this.termNCondition = termNCondition;
	}

	public String getPlanInfoDescription() {
		return planInfoDescription;
	}

	public void setPlanInfoDescription(String planInfoDescription) {
		this.planInfoDescription = planInfoDescription;
	}

	public String getPlanNoConsideration() {
		return planNoConsideration;
	}

	public void setPlanNoConsideration(String palnNoConsideration) {
		this.planNoConsideration = palnNoConsideration;
	}

	public String getSsrFeeCode() {
		return ssrFeeCode;
	}

	public void setSsrFeeCode(String ssrFeeCode) {
		this.ssrFeeCode = ssrFeeCode;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public String getPlanCoverdInfoDescription() {
		return planCoverdInfoDescription;
	}

	public void setPlanCoverdInfoDescription(String planCoverdInfoDescription) {
		this.planCoverdInfoDescription = planCoverdInfoDescription;
	}

	public InsuranceProductConfig getInsProductConfig() {
		return insProductConfig;
	}

	public void setInsProductConfig(InsuranceProductConfig insProductConfig) {
		this.insProductConfig = insProductConfig;
	}

	public String getPlanDescription() {
		return planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getQuotedAmount() {
		return quotedAmount;
	}

	public void setQuotedAmount(BigDecimal quotedAmount) {
		this.quotedAmount = quotedAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getQuotedTaxAmout() {
		return quotedTaxAmout;
	}

	public void setQuotedTaxAmout(BigDecimal quotedTaxAmout) {
		this.quotedTaxAmout = quotedTaxAmout;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getQuotedNetAmount() {
		return quotedNetAmount;
	}

	public void setQuotedNetAmount(BigDecimal quotedNetAmount) {
		this.quotedNetAmount = quotedNetAmount;
	}

	public Integer getInsuranceRefId() {
		return insuranceRefId;
	}

	public void setInsuranceRefId(Integer insuranceRefId) {
		this.insuranceRefId = insuranceRefId;
	}

	public Map<String, String> getInsuranceExternalContent() {
		return insuranceExternalContent;
	}

	public void setInsuranceExternalContent(
			Map<String, String> insuranceExternalContent) {
		this.insuranceExternalContent = insuranceExternalContent;
	}

}
