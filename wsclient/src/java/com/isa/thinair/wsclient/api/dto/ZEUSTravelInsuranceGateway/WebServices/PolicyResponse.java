/**
 * PolicyResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class PolicyResponse  implements java.io.Serializable {
    private java.lang.String itineraryState;

    private java.lang.String itineraryID;

    private java.lang.String PNR;

    private java.lang.String errorCode;

    private java.lang.String errorMessage;

    private java.lang.String policyNo;

    private java.lang.String policyCancelDateTime;

    private java.lang.String policyRefundDateTime;

    public PolicyResponse() {
    }

    public PolicyResponse(
           java.lang.String itineraryState,
           java.lang.String itineraryID,
           java.lang.String PNR,
           java.lang.String errorCode,
           java.lang.String errorMessage,
           java.lang.String policyNo,
           java.lang.String policyCancelDateTime,
           java.lang.String policyRefundDateTime) {
           this.itineraryState = itineraryState;
           this.itineraryID = itineraryID;
           this.PNR = PNR;
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.policyNo = policyNo;
           this.policyCancelDateTime = policyCancelDateTime;
           this.policyRefundDateTime = policyRefundDateTime;
    }


    /**
     * Gets the itineraryState value for this PolicyResponse.
     * 
     * @return itineraryState
     */
    public java.lang.String getItineraryState() {
        return itineraryState;
    }


    /**
     * Sets the itineraryState value for this PolicyResponse.
     * 
     * @param itineraryState
     */
    public void setItineraryState(java.lang.String itineraryState) {
        this.itineraryState = itineraryState;
    }


    /**
     * Gets the itineraryID value for this PolicyResponse.
     * 
     * @return itineraryID
     */
    public java.lang.String getItineraryID() {
        return itineraryID;
    }


    /**
     * Sets the itineraryID value for this PolicyResponse.
     * 
     * @param itineraryID
     */
    public void setItineraryID(java.lang.String itineraryID) {
        this.itineraryID = itineraryID;
    }


    /**
     * Gets the PNR value for this PolicyResponse.
     * 
     * @return PNR
     */
    public java.lang.String getPNR() {
        return PNR;
    }


    /**
     * Sets the PNR value for this PolicyResponse.
     * 
     * @param PNR
     */
    public void setPNR(java.lang.String PNR) {
        this.PNR = PNR;
    }


    /**
     * Gets the errorCode value for this PolicyResponse.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this PolicyResponse.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this PolicyResponse.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this PolicyResponse.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the policyNo value for this PolicyResponse.
     * 
     * @return policyNo
     */
    public java.lang.String getPolicyNo() {
        return policyNo;
    }


    /**
     * Sets the policyNo value for this PolicyResponse.
     * 
     * @param policyNo
     */
    public void setPolicyNo(java.lang.String policyNo) {
        this.policyNo = policyNo;
    }


    /**
     * Gets the policyCancelDateTime value for this PolicyResponse.
     * 
     * @return policyCancelDateTime
     */
    public java.lang.String getPolicyCancelDateTime() {
        return policyCancelDateTime;
    }


    /**
     * Sets the policyCancelDateTime value for this PolicyResponse.
     * 
     * @param policyCancelDateTime
     */
    public void setPolicyCancelDateTime(java.lang.String policyCancelDateTime) {
        this.policyCancelDateTime = policyCancelDateTime;
    }


    /**
     * Gets the policyRefundDateTime value for this PolicyResponse.
     * 
     * @return policyRefundDateTime
     */
    public java.lang.String getPolicyRefundDateTime() {
        return policyRefundDateTime;
    }


    /**
     * Sets the policyRefundDateTime value for this PolicyResponse.
     * 
     * @param policyRefundDateTime
     */
    public void setPolicyRefundDateTime(java.lang.String policyRefundDateTime) {
        this.policyRefundDateTime = policyRefundDateTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PolicyResponse)) return false;
        PolicyResponse other = (PolicyResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.itineraryState==null && other.getItineraryState()==null) || 
             (this.itineraryState!=null &&
              this.itineraryState.equals(other.getItineraryState()))) &&
            ((this.itineraryID==null && other.getItineraryID()==null) || 
             (this.itineraryID!=null &&
              this.itineraryID.equals(other.getItineraryID()))) &&
            ((this.PNR==null && other.getPNR()==null) || 
             (this.PNR!=null &&
              this.PNR.equals(other.getPNR()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.policyNo==null && other.getPolicyNo()==null) || 
             (this.policyNo!=null &&
              this.policyNo.equals(other.getPolicyNo()))) &&
            ((this.policyCancelDateTime==null && other.getPolicyCancelDateTime()==null) || 
             (this.policyCancelDateTime!=null &&
              this.policyCancelDateTime.equals(other.getPolicyCancelDateTime()))) &&
            ((this.policyRefundDateTime==null && other.getPolicyRefundDateTime()==null) || 
             (this.policyRefundDateTime!=null &&
              this.policyRefundDateTime.equals(other.getPolicyRefundDateTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItineraryState() != null) {
            _hashCode += getItineraryState().hashCode();
        }
        if (getItineraryID() != null) {
            _hashCode += getItineraryID().hashCode();
        }
        if (getPNR() != null) {
            _hashCode += getPNR().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getPolicyNo() != null) {
            _hashCode += getPolicyNo().hashCode();
        }
        if (getPolicyCancelDateTime() != null) {
            _hashCode += getPolicyCancelDateTime().hashCode();
        }
        if (getPolicyRefundDateTime() != null) {
            _hashCode += getPolicyRefundDateTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PolicyResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itineraryID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ItineraryID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PNR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PNR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyCancelDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyCancelDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyRefundDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PolicyRefundDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
