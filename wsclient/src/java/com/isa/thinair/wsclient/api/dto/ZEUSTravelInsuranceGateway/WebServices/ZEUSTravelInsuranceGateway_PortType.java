/**
 * ZEUSTravelInsuranceGateway_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public interface ZEUSTravelInsuranceGateway_PortType extends java.rmi.Remote {
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlans getAvailablePlans(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansWithRiders getAvailablePlansWithRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.OutputResponseAvailablePlansOTA getAvailablePlansOTA(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestOTA genericRequestOTALite) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponse confirmPurchase(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest genericRequest) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ConfirmPurchaseResponseWithRiders confirmPurchaseWithRiders(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders genericRequest) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GetPolicyDetailsResponse getPolicyDetails(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, java.lang.String PNR) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse cancelPolicy(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader policyHeader) throws java.rmi.RemoteException;
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyResponse reissuePolicyCertificate(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication itineraryAuthentication, com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PolicyHeader policyHeader) throws java.rmi.RemoteException;
}
