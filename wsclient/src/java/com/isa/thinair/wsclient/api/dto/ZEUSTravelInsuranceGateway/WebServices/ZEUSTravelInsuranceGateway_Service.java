/**
 * ZEUSTravelInsuranceGateway_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public interface ZEUSTravelInsuranceGateway_Service extends javax.xml.rpc.Service {
    public java.lang.String getZEUSTravelInsuranceGatewayAddress();

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType getZEUSTravelInsuranceGateway() throws javax.xml.rpc.ServiceException;

    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ZEUSTravelInsuranceGateway_PortType getZEUSTravelInsuranceGateway(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
