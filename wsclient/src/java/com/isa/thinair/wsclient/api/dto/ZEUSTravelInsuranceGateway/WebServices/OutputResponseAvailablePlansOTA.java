/**
 * OutputResponseAvailablePlansOTA.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices;

public class OutputResponseAvailablePlansOTA  implements java.io.Serializable {
    private java.lang.String insuranceTitle;

    private java.lang.String insuranceDesc;

    private int totalPlansAvailable;

    private java.lang.String errorCode;

    private java.lang.String errorMessage;

    private com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTA[] availablePlans;

    public OutputResponseAvailablePlansOTA() {
    }

    public OutputResponseAvailablePlansOTA(
           java.lang.String insuranceTitle,
           java.lang.String insuranceDesc,
           int totalPlansAvailable,
           java.lang.String errorCode,
           java.lang.String errorMessage,
           com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTA[] availablePlans) {
           this.insuranceTitle = insuranceTitle;
           this.insuranceDesc = insuranceDesc;
           this.totalPlansAvailable = totalPlansAvailable;
           this.errorCode = errorCode;
           this.errorMessage = errorMessage;
           this.availablePlans = availablePlans;
    }


    /**
     * Gets the insuranceTitle value for this OutputResponseAvailablePlansOTA.
     * 
     * @return insuranceTitle
     */
    public java.lang.String getInsuranceTitle() {
        return insuranceTitle;
    }


    /**
     * Sets the insuranceTitle value for this OutputResponseAvailablePlansOTA.
     * 
     * @param insuranceTitle
     */
    public void setInsuranceTitle(java.lang.String insuranceTitle) {
        this.insuranceTitle = insuranceTitle;
    }


    /**
     * Gets the insuranceDesc value for this OutputResponseAvailablePlansOTA.
     * 
     * @return insuranceDesc
     */
    public java.lang.String getInsuranceDesc() {
        return insuranceDesc;
    }


    /**
     * Sets the insuranceDesc value for this OutputResponseAvailablePlansOTA.
     * 
     * @param insuranceDesc
     */
    public void setInsuranceDesc(java.lang.String insuranceDesc) {
        this.insuranceDesc = insuranceDesc;
    }


    /**
     * Gets the totalPlansAvailable value for this OutputResponseAvailablePlansOTA.
     * 
     * @return totalPlansAvailable
     */
    public int getTotalPlansAvailable() {
        return totalPlansAvailable;
    }


    /**
     * Sets the totalPlansAvailable value for this OutputResponseAvailablePlansOTA.
     * 
     * @param totalPlansAvailable
     */
    public void setTotalPlansAvailable(int totalPlansAvailable) {
        this.totalPlansAvailable = totalPlansAvailable;
    }


    /**
     * Gets the errorCode value for this OutputResponseAvailablePlansOTA.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this OutputResponseAvailablePlansOTA.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorMessage value for this OutputResponseAvailablePlansOTA.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this OutputResponseAvailablePlansOTA.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the availablePlans value for this OutputResponseAvailablePlansOTA.
     * 
     * @return availablePlans
     */
    public com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTA[] getAvailablePlans() {
        return availablePlans;
    }


    /**
     * Sets the availablePlans value for this OutputResponseAvailablePlansOTA.
     * 
     * @param availablePlans
     */
    public void setAvailablePlans(com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PlanOTA[] availablePlans) {
        this.availablePlans = availablePlans;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputResponseAvailablePlansOTA)) return false;
        OutputResponseAvailablePlansOTA other = (OutputResponseAvailablePlansOTA) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.insuranceTitle==null && other.getInsuranceTitle()==null) || 
             (this.insuranceTitle!=null &&
              this.insuranceTitle.equals(other.getInsuranceTitle()))) &&
            ((this.insuranceDesc==null && other.getInsuranceDesc()==null) || 
             (this.insuranceDesc!=null &&
              this.insuranceDesc.equals(other.getInsuranceDesc()))) &&
            this.totalPlansAvailable == other.getTotalPlansAvailable() &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.availablePlans==null && other.getAvailablePlans()==null) || 
             (this.availablePlans!=null &&
              java.util.Arrays.equals(this.availablePlans, other.getAvailablePlans())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInsuranceTitle() != null) {
            _hashCode += getInsuranceTitle().hashCode();
        }
        if (getInsuranceDesc() != null) {
            _hashCode += getInsuranceDesc().hashCode();
        }
        _hashCode += getTotalPlansAvailable();
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getAvailablePlans() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAvailablePlans());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAvailablePlans(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputResponseAvailablePlansOTA.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "OutputResponseAvailablePlansOTA"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insuranceTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InsuranceTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insuranceDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "InsuranceDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPlansAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "TotalPlansAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availablePlans");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailablePlans"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "PlanOTA"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ZEUSTravelInsuranceGateway/WebServices", "AvailablePlan"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
