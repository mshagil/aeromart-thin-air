package com.isa.thinair.wsclient.api.dto.lms;

import java.io.Serializable;
import java.math.BigDecimal;

public class RewardData implements Serializable {

	private static final long serialVersionUID = 7296049814981797654L;

	private String rewardTypeExtReference;

	private BigDecimal rewardAmount;

	public String getRewardTypeExtReference() {
		return rewardTypeExtReference;
	}

	public void setRewardTypeExtReference(String rewardTypeExtReference) {
		this.rewardTypeExtReference = rewardTypeExtReference;
	}

	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}

	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}

}
