package com.isa.thinair.wsclient.core.service.lms.impl.aeroreward;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.config.AeroRewardConfig;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto.Member;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto.MemberSearchByIdRequest;

public class LMSAeroRewardWebServiceInvoker {

	private static Log log = LogFactory.getLog(LMSAeroRewardWebServiceInvoker.class);

	public static final AeroRewardConfig aeroRewardConfig = WSClientModuleUtils.getModuleConfig().getLmsClientConfig()
			.getAeroRewardConfig();
	public static final String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_LMS;

	public static final String ERROR_CODE = "errorCode";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String COOKIE = "Cookie";

	public static final String RETRIEVE_MEMBER_BY_ID = "RETRIEVE_MEMBER_BY_ID";

	@SuppressWarnings({ "rawtypes"})
	public static AeroRewardRestServiceClient getServiceCleint(final String serviceCode) throws ModuleException {

		AeroRewardRestServiceClient stub;
		String url = getServiceUrl(serviceCode);

		switch (serviceCode) {

		case RETRIEVE_MEMBER_BY_ID:
			stub = new AeroRewardRestServiceClient<Member, MemberSearchByIdRequest>(Member.class, url);
			break;

		default:
			log.error("Loyalty service unsupported Lms service code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}

		return stub;
	}

	private static String getServiceUrl(String serviceCode) throws ModuleException {

		Properties serviceUrlConfig = aeroRewardConfig.getRewardApiServiceUrl();
		String property = (String) serviceUrlConfig.get(serviceCode);
		if (property == null) {
			log.error("Loyalty service unsupported Lms service code detected");
			throw new ModuleException("wsclient.loyalty.unsupported.service.invocation.error");
		}
		return property;
	}

}
