package com.isa.thinair.wsclient.core.service.lms.impl.gravty;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto.Member;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSCommonUtil;

public class GravtyDtoUtils {
	
	@SuppressWarnings("serial")
	public static Map<Integer, String> genderType = new HashMap<Integer, String>() {{		
		put(1,"male");
		put(2,"female");
		put(0,"other");
	}};

	public static LoyaltyPointDetailsDTO adaptToLoyaltyPointDetailsDTO(Member member) {

		LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = new LoyaltyPointDetailsDTO();
		loyaltyPointDetailsDTO.setMemberAccountId(member.getLoginName());
		loyaltyPointDetailsDTO.setMemberPointsAvailable(member.getLoyaltyPoints().getPointsAvailable());
		loyaltyPointDetailsDTO.setMemberPointsDeducted(member.getLoyaltyPoints().getPointsDeducted());
		loyaltyPointDetailsDTO.setMemberPointsEarned(member.getLoyaltyPoints().getPointsEarnedTotal());
		loyaltyPointDetailsDTO.setMemberPointsExpired(member.getLoyaltyPoints().getPointsExpired());
		loyaltyPointDetailsDTO.setMemberPointsLocked(member.getLoyaltyPoints().getPointsSystemBlocked());
		loyaltyPointDetailsDTO.setMemberPointsUsed(member.getLoyaltyPoints().getPointsBurnedTotal());
		return loyaltyPointDetailsDTO;

	}
	public static LoyaltyMemberCoreDTO populateLoyaltyMemberCoreDTO(String memberAccountId,
			MemberFetchDataResponse memberData) {
		return LMSCommonUtil.populateLoyaltyMemberCoreDTO(memberAccountId,memberData);
	}
	
	public static LoyaltyPointDetailsDTO populateLoyaltyPointDetailsDTO(String memberAccountId, MemberFetchDataResponse memberData) {

		return LMSCommonUtil.populateLoyaltyPointDetailsDTO(memberAccountId,memberData);
	}
	
	}
