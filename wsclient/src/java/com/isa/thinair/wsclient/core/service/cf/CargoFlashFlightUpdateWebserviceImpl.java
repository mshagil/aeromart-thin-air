package com.isa.thinair.wsclient.core.service.cf;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.Service;

import org.apache.axis.client.ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.datacontract.schemas._2004._07.FlightDetail;
import org.datacontract.schemas._2004._07.FlightSchedule;
import org.datacontract.schemas._2004._07.System_Collections_Generic.KeyValuePairOfstringServiceResponselLTIrbuF;
import org.tempuri.BasicHttpBinding_IUpdateScheduleStub;
import org.tempuri.IUpdateSchedule;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.FlightsToPublish;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.util.CommonUtil;

public class CargoFlashFlightUpdateWebserviceImpl implements DefaultWebserviceClient {

	private static final String RESPONSE_SUCCESS = "Success";
	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_CF_FLIGHT;
	
	private static final String CARGO_FLASH = "cargo-flash";
	private static Log log = LogFactory.getLog(CARGO_FLASH);

	private String serviceURL = PlatformUtiltiies.nullHandler(WSClientModuleUtils.getModuleConfig().getServiceURLsMap()
			.get(SERVICE_PROVIDER));

	@Override
	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	public boolean updateFlightDetails(List<FlightsToPublish> flightsToPublish) throws ModuleException {
		log.info("BEGIN:: updateFlightDetails(List<FlightsToPublish> flightsToPublish) [serviceProvider=" + SERVICE_PROVIDER
				+ "]");

		FlightSchedule request = null;
		KeyValuePairOfstringServiceResponselLTIrbuF response = null;

		try {

			request = generateUpdateRequest(flightsToPublish);

			if (request != null) {
				IUpdateSchedule service = getClientStub();
				response = service.updateFlightSchedule(request);

				if (log.isDebugEnabled()) {
					log.debug("Response Recived for updateFlightDetails(List<FlightsToPublish> flightsToPublish) [serviceProvider="
							+ response.getKey() + "]");
				}
				return RESPONSE_SUCCESS.equalsIgnoreCase(response.getKey());
			}
		} catch (Exception e) {
			log.error("ERROR IN :: updateFlightDetails(List<FlightsToPublish> flightsToPublish " + e);
			// Error is not thrown because this has to respond false and update the flight status
		} catch (Throwable te) {
			log.error("ERROR IN :: updateFlightDetails(List<FlightsToPublish> flightsToPublish " + te);
		} finally {

			CargoFlashAuditor.log(log, "CARGO_FLASH_FLIGHT", request, response);
			log.info("END:: updateFlightDetails(List<FlightsToPublish> flightsToPublish) [serviceProvider=" + SERVICE_PROVIDER
					+ "]");
		}

		return false;
	}

	public IUpdateSchedule getClientStub() throws ModuleException {
		try {
			URL wsdlURL = new URL(serviceURL);
			QName qname = new QName("http://tempuri.org/", "IUpdateSchedule");
			ServiceFactory serviceFactory = new ServiceFactory();
			Service uploadService = serviceFactory.createService(qname);

			return new BasicHttpBinding_IUpdateScheduleStub(wsdlURL, uploadService);
		} catch (Exception e) {
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");

		}
	}

	/**
	 * Convert FlightsToPublish collection to the request object of web service
	 * 
	 * @param cargoFlashSchedule
	 * @param flightsToPublish
	 * @throws ModuleException
	 */
	private FlightSchedule generateUpdateRequest(List<FlightsToPublish> flightsToPublish) throws ModuleException {
		FlightSchedule cargoFlashSchedule = new FlightSchedule();

		if (flightsToPublish != null) {

			List<FlightDetail> listOfLights = new ArrayList<FlightDetail>();
			Flight flight = null;
			FlightDetail cfFlightDetail = null;

			for (FlightsToPublish toPublish : flightsToPublish) {

				flight = (Flight) XMLStreamer.decompose(toPublish.getStrFlightDetails());
				String destination = null;
				String origin = null;

				if (flight != null) {
					for (FlightSegement segement : flight.getFlightSegements()) {
						cfFlightDetail = new FlightDetail();

						cfFlightDetail.setETA(CommonUtil.parseDateToCal(segement.getEstTimeArrivalLocal()));
						cfFlightDetail.setETD(CommonUtil.parseDateToCal(segement.getEstTimeDepatureLocal()));

						destination = CommonUtil.getOriginOrDestinationFromSegment(segement.getSegmentCode(), false);
						cfFlightDetail.setDestination(destination);

						origin = CommonUtil.getOriginOrDestinationFromSegment(segement.getSegmentCode(), true);
						cfFlightDetail.setOrigin(origin);

						cfFlightDetail.setFlightDate(cfFlightDetail.getETD());

						cfFlightDetail.setFlightNo(flight.getFlightNumber());

						cfFlightDetail.setFlightStatus(getFlightStatus(flight.getStatus()));
						listOfLights.add(cfFlightDetail);

					}

				}

			}

			if (!listOfLights.isEmpty()) {
				FlightDetail[] flightArray = new FlightDetail[listOfLights.size()];
				listOfLights.toArray(flightArray);
				cargoFlashSchedule.setFlightDetail(flightArray);
			}

		}

		return cargoFlashSchedule;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	private Boolean getFlightStatus(String status) {

		if (status.equals(FlightStatusEnum.CANCELLED.getCode())) {
			return Boolean.TRUE;
		} else if (status.equals(FlightStatusEnum.CLOSED)) {
			return Boolean.TRUE;
		} else if (status.equals(FlightStatusEnum.CLOSED_FOR_RESERVATION)) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

}
