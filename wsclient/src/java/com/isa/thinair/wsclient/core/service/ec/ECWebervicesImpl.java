package com.isa.thinair.wsclient.core.service.ec;

import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ArrayOfChoice1;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ArrayOfDocumentSnapIn;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ArrayOfJourney;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ArrayOfPassenger;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ArrayOfSegment;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.AuthenticationData;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.AuthorizationRequest;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ContextIdentifier;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.Header;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.ItineraryDetailsSnapIn;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.Journey;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.Message;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.NameValue;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.NameValueCollectionDocumentSnapIn;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.Passenger;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.PaymentResponse;
import ie.eurocommerce.v1_00.specifications.paymentengine.bridge.api.Segment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckRequestDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.DocumentSnapInDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.FraudStatus;
import com.isa.thinair.airreservation.api.dto.eurocommerce.ItineraryDocumentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.JourneyDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.NameValueDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.NameValueDocumentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.PassengerDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.SegmentCollectionDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.SegmentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.TransactionResultDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;

/**
 * Eurocommerce WS client
 * 
 * @author mekanayake
 * 
 */
public class ECWebervicesImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_EC;

	private static String STATUS = "Status";

	private static String MESSAGE = "Message";

	private static String REFERENCE = "Reference";

	private static String SCORE = "Score";

	private static String RESULT_CODE = "ResultCode";

	private static final String VERSION = "1.0.00";

	private static final String ACCOUNT_CODE = "AirArabia";

	private static final String PASSWORD = "]ag2M.hH9%HiCkn#JF;Sa*p+jRm";

	private static final String LOCATIO_CODE = "TA"; // TODO move out

	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	private static Log log = LogFactory.getLog(ECWebervicesImpl.class);

	@Override
	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	public CCFraudCheckResponseDTO validateCCPaymentForFraud(CCFraudCheckRequestDTO paymentRQ) throws ModuleException {
		// Credit card details should not be saved/logged
		// log.debug(String.format("calling validate CC paymetn for %s", paymentRQ));
		AuthorizationRequest req = prepareProcessPaymentRequest(paymentRQ);
		try {
			PaymentResponse res = (PaymentResponse) ExternalWebServiceInvoker.invokeService(SERVICE_PROVIDER,
					"processPaymentRequestVAS", true, new Param(req, req.getClass().getSuperclass()));
			CCFraudCheckResponseDTO paymentResponseDTO = null;
			if (res != null) {
				paymentResponseDTO = prepareCCFraudCheckResponseDTO(res);
				log.debug(String.format("The result from the Eurocommerce %s", paymentResponseDTO));
			}
			return paymentResponseDTO;
		} catch (ModuleException e) {
			// we cannot propagate the ModuleException to the caller because it will stop the booking process when the
			// fraud check is unavailable.
			// log this as external error in the T_TEMP_PAYMENT_TNX
			log.error("The cc fraud call fails", e);
			CCFraudCheckResponseDTO responseDTO = new CCFraudCheckResponseDTO();
			responseDTO.setStatus(FraudStatus.ERROR);
			String errorStr = e.toString();
			errorStr = (errorStr.length() >= 255 ? errorStr.substring(0, 255) : errorStr);
			responseDTO.setMessage(errorStr);
			return responseDTO;
		}
	}

	private static CCFraudCheckResponseDTO prepareCCFraudCheckResponseDTO(PaymentResponse response) {
		CCFraudCheckResponseDTO responseDTO = new CCFraudCheckResponseDTO();

		if (response.getExceptionsLog().getMessage().size() > 0) { // CC validation not successful.Errors present.
			StringBuffer sb = new StringBuffer();
			for (Message msg : response.getExceptionsLog().getMessage()) {
				sb.append(msg.getText());
			}
			String errorStr = sb.toString();
			errorStr = (errorStr.length() >= 255 ? errorStr.substring(0, 255) : errorStr);
			responseDTO.setStatus(FraudStatus.ERROR);
			responseDTO.setMessage(errorStr);
		} else {
			// populate the DocumentSnapIn from the response
			NameValueCollectionDocumentSnapIn nvDocSnapIn = (NameValueCollectionDocumentSnapIn) response.getSnapIns()
					.getDocumentSnapIn().get(0);
			for (NameValue nameValue : nvDocSnapIn.getItems().getTransitiveNameValueOrNameValue()) {
				if (nameValue.getName().equals(STATUS)) {
					responseDTO.setStatus(FraudStatus.getStatus(nameValue.getValue()));
				} else if (nameValue.getName().equals(MESSAGE)) {
					responseDTO.setMessage(nameValue.getValue());
				} else if (nameValue.getName().equals(REFERENCE)) {
					responseDTO.setRef(nameValue.getValue());
				} else if (nameValue.getName().equals(SCORE)) {
					responseDTO.setScore(Integer.parseInt(nameValue.getValue()));
				} else if (nameValue.getName().equals(RESULT_CODE)) {
					responseDTO.setResultCode(nameValue.getValue());
				}
			}
		}

		// adding the tnx result from the res.
		TransactionResultDTO tnxResults = new TransactionResultDTO();
		tnxResults.setTransactionMoniker(response.getTransactionResult().getTransactionMoniker());
		tnxResults.setResponseIndicator(response.getTransactionResult().getResponseIndicator().value());
		tnxResults.setMessage(response.getTransactionResult().getMessage());
		tnxResults.setResultType(response.getTransactionResult().getResultType().value());
		tnxResults.setProcessed(response.getTransactionResult().isProcessed());
		tnxResults.setResponseDate(response.getTransactionResult().getResponseDate());
		responseDTO.setResult(tnxResults);

		return responseDTO;
	}

	private static AuthorizationRequest prepareProcessPaymentRequest(CCFraudCheckRequestDTO fraudRQ) throws ModuleException {

		SimpleDateFormat dateformat = new SimpleDateFormat(DATE_FORMAT);

		AuthorizationRequest authRequest = new AuthorizationRequest();
		authRequest.setVersion(VERSION);
		authRequest.setGuid(UUID.randomUUID().toString());
		authRequest.setTimestamp(dateformat.format(Calendar.getInstance().getTime()));

		// setting the authentication data.
		AuthenticationData authenticationData = new AuthenticationData();
		authenticationData.setAccountCode(ACCOUNT_CODE);
		authenticationData.setAccountPassword(PASSWORD);
		authenticationData.setLocationCode(LOCATIO_CODE);
		authRequest.setAuthenticationData(authenticationData);

		authRequest.setPaymentMethodCode(fraudRQ.getPaymentmethodCode());
		authRequest.setAmount(AccelAeroCalculator.parseBigDecimal(fraudRQ.getAmount().doubleValue()));
		authRequest.setCurrencyCode(fraudRQ.getOriginalCurrencyCode());
		authRequest.setAccountNumber(fraudRQ.getAccountNumber());
		authRequest.setExpirationMonth(fraudRQ.getExpirationMonth());
		authRequest.setExpirationYear(fraudRQ.getExpirationYear());

		// setting the cotextIdentifier
		ContextIdentifier contextId = new ContextIdentifier();
		contextId.setExternalReference(fraudRQ.getExternalRef());
		contextId.setExternalId(fraudRQ.getExternalId());
		authRequest.setContextIdentifier(contextId);

		// populating the DocumentSnaps
		ArrayOfDocumentSnapIn documentSnaps = new ArrayOfDocumentSnapIn();
		for (DocumentSnapInDTO snapIn : fraudRQ.getDocumentSnapInDto()) {
			if (snapIn instanceof NameValueDocumentDTO) { // adding NameValueCollectionDocumentSnapIn
				NameValueDocumentDTO nameValueDocument = (NameValueDocumentDTO) snapIn;
				NameValueCollectionDocumentSnapIn documentSnapIn = new NameValueCollectionDocumentSnapIn();
				// add the name and description
				documentSnapIn.setName(nameValueDocument.getName());
				documentSnapIn.setDescription(nameValueDocument.getDiscription());

				// add the name value list
				ArrayOfChoice1 items = new ArrayOfChoice1();
				for (NameValueDTO nameValue : nameValueDocument.getIteamDTOs()) {
					NameValue nv = new NameValue();
					nv.setName(nameValue.getName());
					nv.setValue(nameValue.getValue());
					items.getTransitiveNameValueOrNameValue().add(nv);
				}
				documentSnapIn.setItems(items);
				documentSnaps.getDocumentSnapIn().add(documentSnapIn);

			} else if (snapIn instanceof ItineraryDocumentDTO) { // adding ItineraryDetailsSnapIn
				ItineraryDocumentDTO itineraryDocument = (ItineraryDocumentDTO) snapIn;
				ItineraryDetailsSnapIn documentSnapIn = new ItineraryDetailsSnapIn();

				// add the name and description
				documentSnapIn.setName(itineraryDocument.getName());
				documentSnapIn.setDescription(itineraryDocument.getDiscription());

				// add the header
				Header header = new Header();
				header.setPnr(itineraryDocument.getHeaderDTO().getPnr());
				header.setTicketLess(itineraryDocument.getHeaderDTO().isTicketLess());
				header.setIssueDate(dateformat.format(itineraryDocument.getHeaderDTO().getIssueDate()));// setting the
																										// current time
				header.setCurrencyCode(itineraryDocument.getHeaderDTO().getCurrencyCode());
				header.setAgencyName(itineraryDocument.getHeaderDTO().getAgencyName());
				documentSnapIn.setHeader(header);

				// add the passengers
				ArrayOfPassenger passengers = new ArrayOfPassenger();
				for (PassengerDTO pax : itineraryDocument.getPassengrDTOs()) {
					Passenger passenger = new Passenger();
					passenger.setTitle(pax.getTitle());
					passenger.setFirstName(pax.getFirstName());
					passenger.setLastName(pax.getLastName());
					passenger.setLoyaltyNumber(pax.getLoyaltyNumber());
					passenger.setType(getECPaxType(pax.getType()));
					passengers.getPassenger().add(passenger);
				}
				documentSnapIn.setPassengers(passengers);

				// add journeys
				ArrayOfJourney journeys = new ArrayOfJourney();
				for (JourneyDTO journeDto : itineraryDocument.getJourneyDTOs()) {
					SegmentCollectionDTO segmentCollection = journeDto.getSegmentCollectionDTO();
					Journey journe = new Journey();
					ArrayOfSegment segments = new ArrayOfSegment();
					for (SegmentDTO segmentDto : segmentCollection.getSegmentDTOs()) {
						Segment segment = new Segment();
						segment.setFareClass(segmentDto.getFareClass());
						segment.setDepartureAirport(segmentDto.getDepartureAirport());
						segment.setDepartureDateTime(dateformat.format(segmentDto.getDepartureDateTime()));
						segment.setArrivalAirport(segmentDto.getArrivalAirport());
						segment.setArrivalDateTime(dateformat.format(segmentDto.getArrivalDateTime()));
						segment.setCarrier(segmentDto.getCarrier());
						segment.setFlightNumber(segmentDto.getFlightNumber().substring(2));
						segment.setFareBasis(segmentDto.getFareBasis());
						segment.setDepartureAirportCountry(segmentDto.getDepartureAirportCountry());
						segment.setDepartureAirportTimeZone(getAirportTimeZone(segmentDto.getDepartureDateTimeZulu(),
								segmentDto.getDepartureDateTime()));
						segments.getSegment().add(segment);
					}
					journe.setSegments(segments);

					journeys.getJourney().add(journe);
				}
				documentSnapIn.setJourneys(journeys);
				documentSnaps.getDocumentSnapIn().add(documentSnapIn);
			}
			authRequest.setSnapIns(documentSnaps);
		}
		return authRequest;
	}

	private static String getECPaxType(String paxType) {
		if (paxType.equals("AD")) {
			return "adult";
		} else if (paxType.equals("CH")) {
			return "child";
		} else if (paxType.equals("IN")) {
			return "infant";
		}
		return null;
	}

	private static String getAirportTimeZone(Date zulu, Date airport) {
		long diff = Math.abs((airport.getTime() - zulu.getTime()) / (1000 * 60));
		StringBuffer sb = new StringBuffer();
		String min = String.valueOf((int) (diff % 60));
		min = (min.length() == 1) ? "0".concat(min) : min;
		String hours = String.valueOf((int) (diff / 60));
		hours = (hours.length() == 1) ? "0".concat(hours) : hours;
		sb.append((zulu.after(airport)) ? "-" : "+");
		sb.append(hours).append(":").append(min);
		return sb.toString();
	}
}
