package com.isa.thinair.wsclient.core.service.rm;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;

public class RMWebservicesClientUtil {

	public interface WSInventoryRMThresholdStatus {
		public static final String ABOVE_THRESHOLD = "ABOVE_TOL";
		public static final String BELOW_THRESHOLD = "BELOW_TOL";
	}

	public static String getRMAlertMessageType(String thresholdStatus, String optimizationStatus) {
		String messageType = null;
		if (!AirinventoryCustomConstants.InventoryRMOptimizationStatus.OPTIMIZED.equals(optimizationStatus)
				&& !AirinventoryCustomConstants.InventoryRMOptimizationStatus.OPTIMIZATION_PENDING.equals(optimizationStatus)) {
			if (AirinventoryCustomConstants.InventoryRMThresholdStatus.ABOVE_THRESHOLD.equals(thresholdStatus)) {
				messageType = WSInventoryRMThresholdStatus.ABOVE_THRESHOLD;
			} else if (AirinventoryCustomConstants.InventoryRMThresholdStatus.BELOW_THRESHOLD.equals(thresholdStatus)) {
				messageType = WSInventoryRMThresholdStatus.BELOW_THRESHOLD;
			}
		}
		return messageType;
	}

	/**
	 * TODO - configure
	 * 
	 * @return
	 */
	public static String getMessageProcessingStatus() {
		return "PENDING";
	}
}
