package com.isa.thinair.wsclient.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;
import com.isa.thinair.wsclient.api.util.LMSConstants.LMS_SERVICE_PROVIDER;

/**
 * LMS Web Service Client Configurations
 * 
 * @author rumesh
 * 
 */
public class LMSClientConfig extends DefaultModuleConfig {

	private LMS_SERVICE_PROVIDER defaultLmsProviderCode;

	private SmartButtonConfig smartButtonConfig;

	private AeroRewardConfig aeroRewardConfig;
	
	private GravtyConfig gravtyConfig;

	public LMS_SERVICE_PROVIDER getDefaultLmsProviderCode() {
		return defaultLmsProviderCode;
	}

	public void setDefaultLmsProviderCode(LMS_SERVICE_PROVIDER defaultLmsProviderCode) {
		this.defaultLmsProviderCode = defaultLmsProviderCode;
	}

	public SmartButtonConfig getSmartButtonConfig() {
		return smartButtonConfig;
	}

	public void setSmartButtonConfig(SmartButtonConfig smartButtonConfig) {
		this.smartButtonConfig = smartButtonConfig;
	}

	public AeroRewardConfig getAeroRewardConfig() {
		return aeroRewardConfig;
	}

	public void setAeroRewardConfig(AeroRewardConfig aeroRewardConfig) {
		this.aeroRewardConfig = aeroRewardConfig;
	}

	public GravtyConfig getGravtyConfig() {
		return gravtyConfig;
	}

	public void setGravtyConfig(GravtyConfig gravtyConfig) {
		this.gravtyConfig = gravtyConfig;
	}

	
}
