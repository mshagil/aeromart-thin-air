package com.isa.thinair.wsclient.core.service.blockspace;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.reporting.api.model.InventoryAllocationTO;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;
import com.isa.thinair.wsclient.core.service.blockspace.utilities.InventoryAllocationComposerUtil;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventoryDataRS;
import com.isaaviation.thinair.webservices.api.airinventory.AAFlightInventorySearchRQ;

/**
 * AA webservices client logic implementation.
 * 
 * @author Ruckman
 * 
 */
public class InventoryAllocationsClientImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_AA;

	private static Log log = LogFactory.getLog(InventoryAllocationsClientImpl.class);

	public String getServiceProvider(String carrierCode) {
		if (!PlatformUtiltiies.nullHandler(carrierCode).equals("")) {
			return SERVICE_PROVIDER + ExternalWebServiceInvoker.SERVICE_PROVIDER_SEPERATOR + carrierCode;
		}

		return SERVICE_PROVIDER;
	}

	/**
	 * @param InventoryAllocationTO
	 *            allocationTO
	 * @param Map
	 *            interLineAirlines
	 * @return list of InventoryAllocationDTO
	 * @throws ModuleException
	 */
	public List getFlightInventoryAllocations(InventoryAllocationTO allocationTO, Map interLineAirlines) throws ModuleException {

		log.debug("Begin getFlightInventoryAllocations");
		AAFlightInventoryDataRS dataRS = null;
		List allocations = null;
		List allAllocations = new ArrayList();
		Collection<InterlinedAirLineTO> allInterLineAirLines = interLineAirlines.values();

		AAFlightInventorySearchRQ flightInventorySearchRQ = InventoryAllocationComposerUtil.compose(allocationTO);

		for (InterlinedAirLineTO interLineTO : allInterLineAirLines) {

			log.debug("Invoking AV Search.. Service Provider : Carrier Code : " + interLineTO.getCarrierCode());
			// Set the sevice provider id and user id for looking different different webservice
			flightInventorySearchRQ.setUserId(interLineTO.getUserId());
			dataRS = new AAFlightInventoryDataRS();

			try {
				// Retreive the inventory allocation data from given marketing carrier
				dataRS = (AAFlightInventoryDataRS) ExternalWebServiceInvoker.invokeService(getServiceProvider(interLineTO
						.getCarrierCode()), "getFlightInventoryAllocations", true, new Param(flightInventorySearchRQ));
			} catch (ModuleException e) {
				log.error("Error in Invoking AV Search.. Service Provider :  Carrier Code : " + interLineTO.getCarrierCode(), e);
			}
			log.debug("AV Search is succesful with Service Provider : Carrier Code : " + interLineTO.getCarrierCode());
			// extract the summary from given marketing carrier
			allocations = InventoryAllocationComposerUtil.extract(dataRS, interLineTO.getCarrierCode());
			// Add the allocation to existing collection
			allAllocations.addAll(allocations);
		}

		return allAllocations;
	}
}
