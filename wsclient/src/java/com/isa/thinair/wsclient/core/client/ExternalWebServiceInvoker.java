package com.isa.thinair.wsclient.core.client;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.security.wss4j.WSS4JOutHandler;
import org.codehaus.xfire.transport.http.CommonsHttpMessageSender;

import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.acceaero.AAResWebServicesClient;
import com.isa.thinair.wsclient.api.dib.airarabiaClient;
import com.isa.thinair.wsclient.api.ebi.ServiceClient;
import com.isa.thinair.wsclient.api.ec.PaymentRequestProcessorClient;
import com.isa.thinair.wsclient.api.rm.AARMWebServicesClient;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.WSClientConfig;

/**
 * Service invocation facade for invoking external webservices.
 * 
 * @author Mohamed Nasly
 */
public class ExternalWebServiceInvoker {

	private static Log log = LogFactory.getLog(ExternalWebServiceInvoker.class);

	public static final String SERVICE_PROVIDER_EBI = "EBI";

	public static final String SERVICE_PROVIDER_RM = "RM";

	public static final String SERVICE_PROVIDER_AA = "AA";

	public static final String SERVICE_PROVIDER_DIB = "DIB"; // Dubai
																// Islamic
																// Bank

	public static final String SERVICE_PROVIDER_DB = "DB"; // Dubai
															// Bank

	public static final String SERVICE_PROVIDER_EP = "EP"; // Emirates
															// Post

	public static final String SERVICE_PROVIDER_EC = "EC"; // Eurocommerce
															// CC
															// fraud
															// check
	public static final String SERVICE_PROVIDER_ACE = "ACE";

	public static final String SERVICE_PROVIDER_NETCAST = "NETCAST";

	public static final String SERVICE_PROVIDER_CF = "CF"; // Cargo Flash

	public static final String SERVICE_PROVIDER_CF_FLIGHT = "CF_FLIGHT"; // Cargo Flash Flight

	public static final String SERVICE_PROVIDER_TUNE = "TUNE"; // Tune Insurance

	public static final String SERVICE_PROVIDER_LMS = "LMS";
	
	public static final String SERVICE_PROVIDER_TYPE_B = "TYPE_B";

	/** Holds the service provider seperator */
	public static final String SERVICE_PROVIDER_SEPERATOR = "_";

	private static WSClientConfig wsClientConfig = WSClientModuleUtils.getModuleConfig();

	private static Map interlinedAirlinesMap = CommonsServices.getGlobalConfig().getActiveInterlinedCarriersMap();

	private static final Map<String, String> passwords = new Hashtable<String, String>();

	/**
	 * Replacing the default HTTPS protocol for all <code>commons-httpclient</code> clients. This new protocol allows
	 * both trusted and un-trusted SSL certificates to be automatically used by <code>commons-httpclient</code> clients.
	 * The previous behavior was limited to accepting only trusted SSL certificates.
	 * 
	 * This code block should be moved to a higher level class as it will affect the runtime behavior of all
	 * <code>commons-httpclient</code> clients.
	 */
	// Commented this to solve a problem of overriding settings by Amadeus RQs and xbe
	/*
	 * static { SecureProtocolSocketFactory secureProtocolSocketFactory =
	 * AccelAeroSecureProtocolSocketFactory.getSocketFactory(); Protocol.registerProtocol("https", new Protocol("https",
	 * (ProtocolSocketFactory) secureProtocolSocketFactory, 443)); }
	 */

	/**
	 * Invoke the requested service.
	 * 
	 * @param serviceProviderID
	 * @param serviceMethodName
	 * @param params
	 * @return
	 */
	public static Object invokeService(String serviceProviderID, String serviceMethodName, boolean hasResponse, Param... params)
			throws ModuleException {
		Object service = getService(serviceProviderID);
		Object response = null;
		try {

			if (params.length == 0) {
				if (hasResponse) {
					response = service.getClass().getMethod(serviceMethodName);
				} else {
					service.getClass().getMethod(serviceMethodName);
				}
			} else {
				Class[] paramClasses = new Class[params.length];
				Object[] parmArray = new Object[params.length];
				for (int index = 0; index < params.length; index++) {
					parmArray[index] = params[index].getParam();
					paramClasses[index] = params[index].getParamClass();
				}
				if (hasResponse) {
					response = service.getClass().getMethod(serviceMethodName, paramClasses).invoke(service, parmArray);
				} else {
					service.getClass().getMethod(serviceMethodName, paramClasses).invoke(service, parmArray);
				}
			}
		} catch (NoSuchMethodException e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.invalid.args", "Invalid service method ["
					+ serviceMethodName + "]");
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException(e, "wsclient.serviceinvocation.failed", "External service invocation failure");
		}

		return response;
	}

	/**
	 * Obtain the requested service implementation.
	 * 
	 * @param serviceProviderID
	 * @return
	 */
	private static Object getService(String serviceProviderID) throws ModuleException {
		String serviceURL = PlatformUtiltiies.nullHandler(wsClientConfig.getServiceURLsMap().get(serviceProviderID));
		Object service = null;
		if (SERVICE_PROVIDER_EBI.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new ServiceClient().getEBGWebservices(serviceURL);
		} else if (SERVICE_PROVIDER_RM.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new AARMWebServicesClient().getAARMWebServicesHttpPort(serviceURL);
		} else if (serviceProviderID.indexOf(SERVICE_PROVIDER_AA) != -1 && !serviceURL.equals("")) {
			service = new AAResWebServicesClient().getAAResWebServicesHttpPort(serviceURL);
		} else if (SERVICE_PROVIDER_DIB.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new airarabiaClient().getairarabiaSoap(serviceURL);
		} else if (SERVICE_PROVIDER_DB.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new com.isa.thinair.wsclient.api.db.ServiceClient().getServiceSoap(serviceURL);
		} else if (SERVICE_PROVIDER_EP.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new com.isa.thinair.wsclient.api.ep.AirArabiaReconWebServiceClient()
					.getAirArabiaReconWebServiceSoap(serviceURL);
		} else if (SERVICE_PROVIDER_ACE.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new com.isa.thinair.wsclient.api.ace.ACORDServiceClient().getACORDServiceSoap(serviceURL);
		} else if (SERVICE_PROVIDER_NETCAST.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new com.isa.thinair.wsclient.api.netcast.NetcastWebServiceClient().getNetcastWebServicePort(serviceURL);
		} else if (SERVICE_PROVIDER_EC.equals(serviceProviderID) && !serviceURL.equals("")) {
			service = new PaymentRequestProcessorClient().getPaymentRequestProcessorSoap(serviceURL);
		} else {
			throw new ModuleException("wsclient.serviceinvocation.invalid.provider", "Invalid service provider ID ["
					+ serviceProviderID + "]");
		}

		// For logging request/response
		Client clientx = ((XFireProxy) Proxy.getInvocationHandler(service)).getClient();
		clientx.addInHandler(new org.codehaus.xfire.util.dom.DOMInHandler());
		clientx.addInHandler(new org.codehaus.xfire.util.LoggingHandler());
		clientx.addOutHandler(new org.codehaus.xfire.util.LoggingHandler());
		clientx.addOutHandler(new org.codehaus.xfire.util.dom.DOMOutHandler());

		if (wsClientConfig.getSecurityEnabledURLs().contains(serviceProviderID)) {
			if (serviceProviderID.indexOf(SERVICE_PROVIDER_AA) != -1) {
				String carrierCode = getCarrierCode(serviceProviderID);
				String userName = prepareCredentials(carrierCode);
				authenticate(clientx, carrierCode, userName);
			} else {
				throw new ModuleException("wsclient.serviceinvocation.invalid.authentication",
						"Invalid service provider authentication [" + serviceProviderID + "]");
			}
		}

		// Enabling the proxy for required clients
		if (wsClientConfig.getProxyEnabledURLs().contains(serviceProviderID)) {
			GlobalConfig globalconfig = WSClientModuleUtils.getGlobalConfig();
			if (globalconfig.getUseProxy()) {
				clientx.setProperty(CommonsHttpMessageSender.HTTP_PROXY_HOST, globalconfig.getHttpProxy());
				clientx.setProperty(CommonsHttpMessageSender.HTTP_PROXY_PORT, String.valueOf(globalconfig.getHttpPort()));
				clientx.setProperty(CommonsHttpMessageSender.DISABLE_PROXY_UTILS, "true");
			}
		}

		// Set customize timeout for required clients
		if (wsClientConfig.getCustomTimeOutSetUrls().keySet().contains(serviceProviderID)) {
			clientx.setProperty(CommonsHttpMessageSender.HTTP_TIMEOUT,
					wsClientConfig.getCustomTimeOutSetUrls().get(serviceProviderID));
		}

		return service;
	}

	/**
	 * Prepare the credentials
	 * 
	 * @param carrierCode
	 * @return
	 */
	private static String prepareCredentials(String carrierCode) {
		InterlinedAirLineTO interlinedAirLineTO = (InterlinedAirLineTO) interlinedAirlinesMap.get(carrierCode);
		passwords.put(interlinedAirLineTO.getUserId(), interlinedAirLineTO.getPassword());
		return interlinedAirLineTO.getUserId();
	}

	/**
	 * Authenticate
	 * 
	 * @param clientx
	 * @param carrierCode
	 * @param userName
	 */
	private static void authenticate(Client clientx, String carrierCode, String userName) {
		Properties properties = new Properties();

		properties.setProperty(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		properties.setProperty(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		properties.setProperty(WSHandlerConstants.USER, userName);
		properties.setProperty(WSHandlerConstants.PW_CALLBACK_CLASS, PasswordHandler.class.getName());
		clientx.addOutHandler(new WSS4JOutHandler(properties));
	}

	public static class PasswordHandler implements CallbackHandler {
		private final Log log = LogFactory.getLog(getClass());

		public PasswordHandler() {
		}

		public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
			if (log.isDebugEnabled())
				log.debug("BEGIN handle(Callback[])");
			WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
			String id = pc.getIdentifer();
			pc.setPassword(passwords.get(id));
			if (log.isDebugEnabled())
				log.debug("END handle(Callback[])");
		}
	}

	/**
	 * Returns the carrier code
	 * 
	 * @param completeSerProviderURL
	 * @return
	 */
	public static String getCarrierCode(String completeSerProviderURL) {
		return completeSerProviderURL.substring(
				completeSerProviderURL.indexOf(ExternalWebServiceInvoker.SERVICE_PROVIDER_SEPERATOR) + 1,
				completeSerProviderURL.length());
	}

	/**
	 * Obtain the requested serviceAgent.
	 * 
	 * @param serviceProviderID
	 * @return
	 */
	public static String getServiceAgent(String serviceProviderID) throws ModuleException {
		return PlatformUtiltiies.nullHandler(wsClientConfig.getServiceAgentsMap().get(serviceProviderID));

	}

}
