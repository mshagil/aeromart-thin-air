package com.isa.thinair.wsclient.core.service.tune;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.InsuranceExternalContenKey;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.wsclient.api.dto.InsuranceProductConfig;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.GenderType;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.IdentificationType;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequest;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.InputRequestWithRiders;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryAuthentication;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryContact;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryFlight;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeader;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryHeaderWithRiders;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.ItineraryPassenger;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.Plan;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseMain;
import com.isa.thinair.wsclient.api.dto.ZEUSTravelInsuranceGateway.WebServices.PurchaseRider;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsurableFlightSegment;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.TuneClientConfig;
import com.isa.thinair.wsclient.core.service.tune.TuneWebServiceImpl.TunePlanType;
import com.isa.thinair.wsclient.core.util.InsuranceUtil;

public class TuneUtil {

	private static final String DATE_FMT = "yyyy-MM-dd HH:mm:ss";

	// Quote Related
	public InputRequest prepareQuoteRequest(IInsuranceRequest iRequest) throws DatatypeConfigurationException, ModuleException {
		InputRequest tuneRequest = new InputRequest();

		ItineraryAuthentication authentication = getTuneAuthentication();
		tuneRequest.setAuthentication(authentication);

		ItineraryHeader header = getTuneItenaryHeader(iRequest);
		tuneRequest.setHeader(header);

		ItineraryContact contactDetails = getTuneContactDeatils(iRequest);
		tuneRequest.setContactDetails(contactDetails);

		ItineraryFlight[] flights = getTuneFlights(iRequest);
		tuneRequest.setFlights(flights);

		ItineraryPassenger[] passengers = getTunePAXs(iRequest);
		tuneRequest.setPassengers(passengers);

		return tuneRequest;
	}

	public void updateAvailableInsuranceQuotes(List<InsuranceResponse> avilableInsuranceQuotes, Plan[] avilablePlans,
			IInsuranceRequest iRequest, String errorCode, String errorMSG, boolean isRiderPlan) throws ModuleException {

		InsuranceRequestAssembler iReqAssembler = (InsuranceRequestAssembler) iRequest;
		int salesChannelCode = iReqAssembler.getInsureSegment().getSalesChanelCode();
		boolean isEurophianCountry = false;

		try {
			InsuranceCountryTO aigCountryTO = InsuranceUtil.getCountryTO(iReqAssembler.getInsureSegment().getFromAirportCode());
			isEurophianCountry = aigCountryTO.isEuropianCountry();
		} catch (ModuleException ex) {

		}

		ExchangeRateProxy exRateProxy = new ExchangeRateProxy(new Date());
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		BigDecimal exrate = exRateProxy.getExchangeRate(TuneWebServiceImpl.QUOTE_CURRENCY, baseCurrency);

		HashMap<String, InsuranceProductConfig> insProductConfigMap = InsuranceUtil.getInsuranceProductConfigs();

		for (Plan plan : avilablePlans) {
			InsuranceProductConfig insProductConfig = getInsuranceProductConfigForPlan(insProductConfigMap, plan, isRiderPlan,
					salesChannelCode);

			if (insProductConfig != null) {

				BigDecimal quotedTotalPremium = AccelAeroRounderPolicy.convertAndRound(
						new BigDecimal(plan.getTotalPremiumAmount()), exrate, null, null);

				InsuranceResponse insResponse = new InsuranceResponse();
				insResponse.setSuccess(true);
				insResponse.setErrorCode(errorCode);
				insResponse.setErrorMessage(errorMSG);

				insResponse.setInsProductConfig(insProductConfig);

				// Ideally this should be based on the actual currency code.
				insResponse.setQuotedCurrencyCode(baseCurrency);
				insResponse.setTotalPremiumAmount(quotedTotalPremium);
				insResponse.setQuotedTotalPremiumAmount(quotedTotalPremium);

				insResponse.setEuropianCountry(isEurophianCountry);

				insResponse.setSsrFeeCode(plan.getSSRFeeCode());
				insResponse.setPlanCode(plan.getPlanCode());
				
				Map<String, String> externalContentForDisplay = new HashMap<String, String>();
				externalContentForDisplay.put(InsuranceExternalContenKey.PLAN_COVERED_INFOMATION_DESCRIPTION.toString(), removeCDataTags(plan.getPlanContent()));   
//				if(!isRiderPlan){
//					externalContentForDisplay.put(InsuranceExternalContenKey.PLAN_COVERED_INFOMATION_DESCRIPTION.toString(), getMainPlanHTML(isEurophianCountry));   
//				}else {
//					externalContentForDisplay.put(InsuranceExternalContenKey.PLAN_COVERED_INFOMATION_DESCRIPTION.toString(), getRiderPlanHTML(isEurophianCountry));   
//					
//				}
				//swap the title and desc after communicating from insurance provider
				externalContentForDisplay.put(InsuranceExternalContenKey.PLAN_TITEL.toString(), plan.getPlanDesc());   
				externalContentForDisplay.put(InsuranceExternalContenKey.PLAN_DESC.toString(), removeCDataTags(plan.getPlanTitle()));   
				externalContentForDisplay.put(InsuranceExternalContenKey.PUSH_PLAN_SELECT_YES_DESCRIPTION.toString(), removeCDataTags(plan.getPlanYesDesc()));   
				externalContentForDisplay.put(InsuranceExternalContenKey.PLAN_TERMS_AND_CONDITIONS.toString(),getTnC());   
				if (!isRiderPlan && !plan.isIsDefaultPlan()) {
					externalContentForDisplay.put(InsuranceExternalContenKey.POPUP_YES_BUTTON_TEXT.toString(), removeCDataTags(plan.getPlanAdditionalInfoTitle()));   
					externalContentForDisplay.put(InsuranceExternalContenKey.POPUP_NO_BUTTON_TEXT.toString(), removeCDataTags(plan.getPlanAdditionalInfoDesc()));   
				}
				insResponse.setInsuranceExternalContent(externalContentForDisplay);

				BigDecimal qTotalTaxAmount = BigDecimal.ZERO;
				BigDecimal qTotalNetAmount = quotedTotalPremium;
				BigDecimal[] baseAmounts = null;

				baseAmounts = InsuranceUtil.getBaseCurrencyAmount(baseCurrency, new BigDecimal[] { quotedTotalPremium,
						qTotalNetAmount, qTotalTaxAmount });

				insResponse.setAmount((baseAmounts != null ? baseAmounts[0] : AccelAeroCalculator.getDefaultBigDecimalZero()));
				insResponse.setQuotedAmount(quotedTotalPremium);

				insResponse.setNetAmount(baseAmounts != null ? baseAmounts[1] : AccelAeroCalculator.getDefaultBigDecimalZero());
				insResponse.setQuotedNetAmount(qTotalNetAmount);

				insResponse.setTaxAmount(baseAmounts != null ? baseAmounts[2] : AccelAeroCalculator.getDefaultBigDecimalZero());
				insResponse.setQuotedTaxAmout(qTotalTaxAmount);

				avilableInsuranceQuotes.add(insResponse);
			}

		}
	}

	// SELL Related
	public InputRequest prepareSellPolicyRequest(IInsuranceRequest iRequest) throws ModuleException {
		InputRequest getTravelQuote = new InputRequest();

		ItineraryAuthentication authentication = getTuneAuthentication();
		getTravelQuote.setAuthentication(authentication);

		ItineraryHeader header = getTuneItenaryHeader(iRequest);
		getTravelQuote.setHeader(header);

		ItineraryContact contactDetails = getTuneContactDeatils(iRequest);
		getTravelQuote.setContactDetails(contactDetails);

		ItineraryFlight[] flights = getTuneFlights(iRequest);
		getTravelQuote.setFlights(flights);

		ItineraryPassenger[] passengers = getTunePAXs(iRequest);
		getTravelQuote.setPassengers(passengers);

		return getTravelQuote;
	}

	public InputRequestWithRiders prepareSellWithRiderPolicyRequest(List<IInsuranceRequest> iRequests) throws ModuleException {
		InputRequestWithRiders tuneRequest = new InputRequestWithRiders();
		IInsuranceRequest firstRequest = iRequests.get(0);

		ItineraryAuthentication authentication = getTuneAuthentication();
		tuneRequest.setAuthentication(authentication);

		ItineraryHeaderWithRiders header = getTuneItenaryHeaderwithRidePlan(iRequests);
		tuneRequest.setHeader(header);

		ItineraryContact contactDetails = getTuneContactDeatils(firstRequest);
		tuneRequest.setContactDetails(contactDetails);

		ItineraryFlight[] flights = getTuneFlights(firstRequest);
		tuneRequest.setFlights(flights);

		ItineraryPassenger[] passengers = getTunePAXs(firstRequest);
		tuneRequest.setPassengers(passengers);

		return tuneRequest;
	}

	public ItineraryHeaderWithRiders getTuneItenaryHeaderwithRidePlan(List<IInsuranceRequest> iRequests) throws ModuleException {
		ItineraryHeaderWithRiders header = new ItineraryHeaderWithRiders();
		IInsuranceRequest firstRequest = iRequests.get(0);
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) firstRequest;
		LCCInsuredContactInfoDTO contactInfo = insReqAssemebler.getLccInsuranceContactInfo() != null ? insReqAssemebler
				.getLccInsuranceContactInfo() : createContactInfoUnavailable(firstRequest);

		String channel = null;
		// We should have a channel set at this moment, hence no need to null check
		switch (insReqAssemebler.getInsureSegment().getSalesChanelCode()) {
		case SalesChannelsUtil.SALES_CHANNEL_AGENT:
			channel = "XBE";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_WEB:
			channel = "IBE";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
			channel = "WS";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_IOS:
			channel = "AOS";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
			channel = "AAD";
			break;
		default:
			channel = "XBE";
		}

		String policyNo = "";
		String itineraryID = "";
		String purchaseDate = "";
		String countryCode = contactInfo.getCountry();
		String cultureCode = contactInfo.getPrefLanguage() != null && !contactInfo.getPrefLanguage().isEmpty() ? contactInfo
				.getPrefLanguage().toUpperCase() : "EN";

		int totalAdults = getAdultInfantCount(firstRequest, false);
		int totalChild = 0;
		int totalInfants = getAdultInfantCount(firstRequest, true);

		String pnr = insReqAssemebler.getPnr() != null && !insReqAssemebler.getPnr().isEmpty() ? insReqAssemebler.getPnr() : "";

		header.setChannel(channel);
		header.setItineraryID(itineraryID);
		header.setPNR(pnr);
		header.setPolicyNo(policyNo);
		header.setPurchaseDate(purchaseDate);
		header.setCountryCode(countryCode);
		header.setCultureCode(cultureCode);
		header.setTotalAdults(totalAdults);
		header.setTotalChild(totalChild);
		header.setTotalInfants(totalInfants);

		PurchaseMain[] mainPuchesList = getTuneMainPruductForSell(iRequests);
		if (mainPuchesList != null && mainPuchesList[0] != null) {
			header.setPurchasesMains(mainPuchesList);
		}

		PurchaseRider[] riderPuchesList = getTuneRiderPruductForSell(iRequests);
		if (riderPuchesList != null && riderPuchesList[0] != null) {
			header.setPurchasesRiders(riderPuchesList);
		}

		return header;
	}

	public PurchaseMain[] getTuneMainPruductForSell(List<IInsuranceRequest> iRequests) throws ModuleException {
		PurchaseMain[] listTuneMain = new PurchaseMain[1];
		String currency = AppSysParamsUtil.getBaseCurrency();
		ExchangeRateProxy exRateProxy = new ExchangeRateProxy(new Date());
		BigDecimal exrate = exRateProxy.getExchangeRate(TuneWebServiceImpl.QUOTE_CURRENCY, currency);

		for (IInsuranceRequest iRequest : iRequests) {
			InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;

			if (!TunePlanType.RIDER.toString().equals(InsuranceUtil.getInsuranceProductCode(insReqAssemebler.getInsuranceId()))) {
				PurchaseMain main = new PurchaseMain();

				String SSRFeeCode = insReqAssemebler.getSsrFeeCode() != null && !insReqAssemebler.getSsrFeeCode().isEmpty()
						? insReqAssemebler.getSsrFeeCode()
						: "";
				double totalPremium = insReqAssemebler.getQuotedTotalPremiumAmount() != null ? insReqAssemebler
						.getQuotedTotalPremiumAmount().divide(exrate, 2).doubleValue() : BigDecimal.ZERO.longValue();

				main.setSSRFeeCode(SSRFeeCode);
				main.setTotalPremium(totalPremium);
				main.setCurrency(TuneWebServiceImpl.QUOTE_CURRENCY);
				listTuneMain[0] = main;
			}
		}

		return listTuneMain;
	}

	public PurchaseRider[] getTuneRiderPruductForSell(List<IInsuranceRequest> iRequests) throws ModuleException {
		PurchaseRider[] listTuneRider = new PurchaseRider[1];
		String currency = AppSysParamsUtil.getBaseCurrency();
		ExchangeRateProxy exRateProxy = new ExchangeRateProxy(new Date());
		BigDecimal exrate = exRateProxy.getExchangeRate(TuneWebServiceImpl.QUOTE_CURRENCY, currency);
		for (IInsuranceRequest iRequest : iRequests) {
			InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
			PurchaseRider rider = new PurchaseRider();
			if (TunePlanType.RIDER.toString().equals(InsuranceUtil.getInsuranceProductCode(insReqAssemebler.getInsuranceId()))) {
				String SSRFeeCode = insReqAssemebler.getSsrFeeCode() != null && !insReqAssemebler.getSsrFeeCode().isEmpty()
						? insReqAssemebler.getSsrFeeCode()
						: "";
				double totalPremium = insReqAssemebler.getQuotedTotalPremiumAmount() != null ? insReqAssemebler
						.getQuotedTotalPremiumAmount().divide(exrate, 2).doubleValue() : BigDecimal.ZERO.longValue();
				rider.setSSRFeeCode(SSRFeeCode);
				rider.setTotalPremium(totalPremium);
				rider.setCurrency(TuneWebServiceImpl.QUOTE_CURRENCY);
				listTuneRider[0] = rider;
			}
		}
		return listTuneRider;

	}

	// COMMON
	private ItineraryAuthentication getTuneAuthentication() {
		TuneClientConfig tuneClientConfig = WSClientModuleUtils.getModuleConfig().getTuneClientConfig();
		ItineraryAuthentication auththentiocation = new ItineraryAuthentication();
		auththentiocation.setUsername(tuneClientConfig.getUserName());
		auththentiocation.setPassword(tuneClientConfig.getPswd());
		return auththentiocation;
	}

	private ItineraryHeader getTuneItenaryHeader(IInsuranceRequest iRequest) throws ModuleException {
		ItineraryHeader header = new ItineraryHeader();
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
		LCCInsuredContactInfoDTO contactInfo = insReqAssemebler.getLccInsuranceContactInfo() != null ? insReqAssemebler
				.getLccInsuranceContactInfo() : createContactInfoUnavailable(iRequest);

		String channel = null;
		// We should have a channel set at this moment, hence no need to null check
		switch (insReqAssemebler.getInsureSegment().getSalesChanelCode()) {
		case SalesChannelsUtil.SALES_CHANNEL_AGENT:
			channel = "XBE";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_WEB:
			channel = "IBE";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
			channel = "WS";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_IOS:
			channel = "AOS";
			break;
		case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
			channel = "AAD";
			break;
		default:
			channel = "XBE";
		}

		String itineraryID = "";
		String PNR = insReqAssemebler.getPnr() != null && !insReqAssemebler.getPnr().isEmpty() ? insReqAssemebler.getPnr() : "";
		String policyNo = "";
		String purchaseDate = "";
		String SSRFeeCode = insReqAssemebler.getSsrFeeCode() != null && !insReqAssemebler.getSsrFeeCode().isEmpty()
				? insReqAssemebler.getSsrFeeCode()
				: "";
		String PlanCode = insReqAssemebler.getPlanCode() != null && !insReqAssemebler.getPlanCode().isEmpty() ? insReqAssemebler
				.getPlanCode() : "";
		String feeDescription = "";
		// String currency = insReqAssemebler.getCurrency() != null ? insReqAssemebler.getCurrency() : AppSysParamsUtil
		// .getBaseCurrency();
		String currency = AppSysParamsUtil.getBaseCurrency();
		ExchangeRateProxy exRateProxy = new ExchangeRateProxy(new Date());
		BigDecimal exrate = exRateProxy.getExchangeRate(TuneWebServiceImpl.QUOTE_CURRENCY, currency);
		double totalPremium = insReqAssemebler.getQuotedTotalPremiumAmount() != null ? insReqAssemebler
				.getQuotedTotalPremiumAmount().divide(exrate, 2).doubleValue() : BigDecimal.ZERO.longValue();
		String countryCode = contactInfo.getCountry();
		String cultureCode = contactInfo.getPrefLanguage() != null && !contactInfo.getPrefLanguage().isEmpty() ? contactInfo
				.getPrefLanguage().toUpperCase() : "EN";
		int totalAdults = getAdultInfantCount(iRequest, false);
		int totalChild = 0;
		int totalInfants = getAdultInfantCount(iRequest, true);

		header.setChannel(channel);
		header.setItineraryID(itineraryID);
		header.setPNR(PNR);
		header.setPolicyNo(policyNo);
		header.setPurchaseDate(purchaseDate);
		header.setSSRFeeCode(SSRFeeCode);
		header.setFeeDescription(feeDescription);
		header.setCurrency(TuneWebServiceImpl.QUOTE_CURRENCY);
		header.setTotalPremium(totalPremium);
		header.setCountryCode(countryCode);
		header.setCultureCode(cultureCode);
		header.setTotalAdults(totalAdults);
		header.setTotalChild(totalChild);
		header.setTotalInfants(totalInfants);

		return header;
	}

	private ItineraryContact getTuneContactDeatils(IInsuranceRequest iRequest) {
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
		LCCInsuredContactInfoDTO contactInfo = insReqAssemebler.getLccInsuranceContactInfo() != null ? insReqAssemebler
				.getLccInsuranceContactInfo() : createContactInfoUnavailable(iRequest);
		String contactPerson = contactInfo.getContactPerson();
		String address1 = contactInfo.getAddress1();
		String address2 = contactInfo.getAddress2();
		String address3 = contactInfo.getAddress3();
		String homePhoneNum = contactInfo.getHomePhoneNum();
		String mobilePhoneNum = contactInfo.getMobilePhoneNum();
		String otherPhoneNum = "";
		String postCode = contactInfo.getPostCode();
		String city = contactInfo.getCity();
		String state = contactInfo.getState();
		String country = contactInfo.getCountry();
		String emailAddress = contactInfo.getEmailAddress();

		ItineraryContact contactDetails = new ItineraryContact();
		contactDetails.setContactPerson(contactPerson);
		contactDetails.setAddress1(address1);
		contactDetails.setAddress2(address2);
		contactDetails.setAddress3(address3);
		contactDetails.setHomePhoneNum(homePhoneNum);
		contactDetails.setMobilePhoneNum(mobilePhoneNum);
		contactDetails.setOtherPhoneNum(otherPhoneNum);
		contactDetails.setPostCode(postCode);
		contactDetails.setCity(city);
		contactDetails.setState(state);
		contactDetails.setCountry(country);
		contactDetails.setEmailAddress(emailAddress);

		return contactDetails;

	}

	private ItineraryFlight[] getTuneFlights(IInsuranceRequest iRequest) {
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;

		List<ItineraryFlight> insurableFlights = new ArrayList<ItineraryFlight>();

		List<InsurableFlightSegment> insurableFlightSegments = insReqAssemebler.getFlightSegments();

		for (InsurableFlightSegment insurableFlightSegment : insurableFlightSegments) {

			ItineraryFlight itineraryFlight = new ItineraryFlight();
			itineraryFlight.setDepartStationCode(insurableFlightSegment.getDepartureStationCode());
			itineraryFlight.setDepartDateTime(formattedDate(insurableFlightSegment.getDepartureDateTimeLocal()));
			itineraryFlight.setDepartCountryCode(CommonsServices.getGlobalConfig().getAirportInfo(
					itineraryFlight.getDepartStationCode(), false)[3].toString());
			itineraryFlight.setArrivalStationCode(insurableFlightSegment.getArrivalStationCode());
			itineraryFlight.setArrivalCountryCode(CommonsServices.getGlobalConfig().getAirportInfo(
					itineraryFlight.getArrivalStationCode(), false)[3].toString());
			itineraryFlight.setDepartFlightNo(insurableFlightSegment.getFlightNo());
			itineraryFlight.setDepartAirlineCode(insurableFlightSegment.getFlightNo().substring(0, 2));
			insurableFlights.add(itineraryFlight);
		}

		return insurableFlights.toArray(new ItineraryFlight[insurableFlights.size()]);
	}

	private ItineraryPassenger[] getTunePAXs(IInsuranceRequest iRequest) throws ModuleException {
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
		Collection<InsurePassenger> passengers = insReqAssemebler.getPassengers();
		int insuredPaxCount = passengers.size();

		ArrayList<ItineraryPassenger> itineraryPassengers = new ArrayList<ItineraryPassenger>();
		Iterator<InsurePassenger> itPax = passengers.iterator();
		while (itPax.hasNext()) {
			InsurePassenger insuPax = itPax.next();
			String isInfant = insuPax.isInfant() ? "1" : "0";
			String firstName = insuPax.getFirstName();
			String lastName = insuPax.getLastName();
			GenderType gender = (insuPax.getTitle() != null && !insuPax.getTitle().isEmpty()) ? "MR".equalsIgnoreCase(insuPax
					.getTitle()) ? GenderType.Male : GenderType.Female : GenderType.Male;
			String DOB = insuPax.getDateOfBirth() != null ? formattedDate(insuPax.getDateOfBirth()) : "1979-01-01 00:00:00";
			String age = !insuPax.isInfant() ? getAge(insuPax) : "0";
			String identityType = IdentificationType._Passport;
			String identityNo = "99999999";
			boolean isQualified = false;
			String countryOfResidence = insuPax.getCountryOfAddress();

			// String nationality = insuPax.getNationality() != null ? AAServicesModuleUtils.getCommonMasterBD()
			// .getNationality(insuPax.getNationality()).getIsoCode() : countryOfResidence;
			String nationality = insuPax.getNationality() != null && !"".equals(insuPax.getNationality())
					? WSClientModuleUtils.getCommonServiceBD().getNationality(Integer.parseInt(insuPax.getNationality()))
							.getIsoCode()
					: countryOfResidence;
			String selectedPlanCode = insReqAssemebler.getPlanCode();
			String selectedSSRFeeCode = insReqAssemebler.getSsrFeeCode();
			// String currencyCode = insReqAssemebler.getCurrency() != null ? insReqAssemebler.getCurrency() :
			// AppSysParamsUtil
			// .getBaseCurrency();

			String currencyCode = AppSysParamsUtil.getBaseCurrency();

			ExchangeRateProxy exRateProxy = new ExchangeRateProxy(new Date());
			// BigDecimal exrate = exRateProxy.getExchangeRate(TuneWebServiceImpl.QUOTE_CURRENCY, currencyCode);
			BigDecimal exrate = exRateProxy.getExchangeRate(currencyCode, TuneWebServiceImpl.QUOTE_CURRENCY);

			// double passengerPremiumAmount = insReqAssemebler.getQuotedTotalPremiumAmount() != null ? insReqAssemebler
			// .getQuotedTotalPremiumAmount().divide(exrate).divide(new BigDecimal(insuredPaxCount), 2).doubleValue() :
			// 0.00;

			double passengerPremiumAmount = insReqAssemebler.getQuotedTotalPremiumAmount() != null ? AccelAeroRounderPolicy
					.convertAndRound(insReqAssemebler.getQuotedTotalPremiumAmount(), exrate, null, null)
					.divide(new BigDecimal(insuredPaxCount), 2).doubleValue() : 0.00;

			ItineraryPassenger pax = new ItineraryPassenger();
			pax.setIsInfant(isInfant);
			pax.setFirstName(firstName);
			pax.setLastName(lastName);
			pax.setGender(GenderType.Male);
			pax.setGender(gender);
			pax.setDOB(DOB);
			pax.setAge(age);
			pax.setIdentityType(IdentificationType.Passport);
			pax.setIdentityNo(identityNo);
			pax.setIsQualified(isQualified);
			pax.setNationality(nationality);
			pax.setCountryOfResidence(countryOfResidence);
			pax.setPassengerPremiumAmount(passengerPremiumAmount);
			pax.setSelectedSSRFeeCode(selectedSSRFeeCode);
			pax.setSelectedPlanCode(selectedPlanCode);
			pax.setCurrencyCode(TuneWebServiceImpl.QUOTE_CURRENCY);

			itineraryPassengers.add(pax);
		}
		return (ItineraryPassenger[]) itineraryPassengers.toArray(new ItineraryPassenger[itineraryPassengers.size()]);
	}

	private int getAdultInfantCount(IInsuranceRequest iRequest, boolean isInfantCount) {
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
		Collection<InsurePassenger> passengers = insReqAssemebler.getPassengers();
		Iterator<InsurePassenger> itPax = passengers.iterator();
		int adultInfantCount = 0;
		while (itPax.hasNext()) {
			InsurePassenger insuPax = itPax.next();
			if (insuPax.isInfant() == isInfantCount) {
				adultInfantCount++;
			}
		}
		return adultInfantCount;
	}

	private String formattedDate(Date date) {
		String formattedDate = "";
		SimpleDateFormat dateformatter = new SimpleDateFormat(DATE_FMT);
		formattedDate = dateformatter.format(date);
		return formattedDate;

	}

	private LCCInsuredContactInfoDTO createContactInfoUnavailable(IInsuranceRequest iRequest) {
		LCCInsuredContactInfoDTO contactInfo = new LCCInsuredContactInfoDTO();
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRequest;
		Collection<InsurePassenger> passengers = insReqAssemebler.getPassengers();

		Iterator<InsurePassenger> itPax = passengers.iterator();
		if (itPax.hasNext()) {
			InsurePassenger insuPax = itPax.next();
			contactInfo.setAddress1(insuPax.getAddressLn1());
			contactInfo.setAddress2(insuPax.getAddressLn2());
			contactInfo.setAddress3("");
			contactInfo.setContactPerson(insuPax.getFirstName() + "" + insuPax.getLastName());
			contactInfo.setCity(insuPax.getCity());
			contactInfo.setState("");
			contactInfo.setCountry(insuPax.getCountryOfAddress());
			contactInfo.setHomePhoneNum(insuPax.getHomePhoneNumber());
			contactInfo.setMobilePhoneNum("");
			contactInfo.setOtherPhoneNum("");
			contactInfo.setEmailAddress(insuPax.getEmail());
			contactInfo.setPrefLanguage("EN");

		}
		return contactInfo;
	}

	private InsuranceProductConfig getInsuranceProductConfigForPlan(HashMap<String, InsuranceProductConfig> insProductConfigMap,
			Plan plan, boolean isRiderPlan, int salesChannelsCode) {
		String configKey = TuneWebServiceImpl.SERVICE_PROVIDER + "_";
		if (isRiderPlan) {
			configKey = configKey + TunePlanType.RIDER.toString() + "_" + salesChannelsCode;
			return insProductConfigMap.get(configKey);
		} else if (plan.isIsDefaultPlan()) {
			configKey = configKey + TunePlanType.DEFAULT.toString() + "_" + salesChannelsCode;
			return insProductConfigMap.get(configKey);
		} else {
			configKey = configKey + TunePlanType.PUSH.toString() + "_" + salesChannelsCode;
			InsuranceProductConfig config = insProductConfigMap.get(configKey);

			if (config != null) {
				config.setPopup(true);
			}
			return config;
		}
	}

	private String getAge(InsurePassenger insuPax) {
		Date defaultAdultBirthday = new Date(79, 0, 01); // 1979-01-01
		Date birthday = insuPax.getDateOfBirth() != null ? insuPax.getDateOfBirth() : defaultAdultBirthday;
		long ageInMillis = new Date().getTime() - birthday.getTime();

		Date age = new Date(ageInMillis);

		return Integer.toString(age.getYear() - 70);
	}

	private String removeCDataTags(String description) {
		if (description != null && description.startsWith("<![CDATA[")) {
			description = description.replace("<![CDATA[", "");
			if (description.endsWith("]]>")) {
				description = description.replace("]]>", "");
			}
		}
		return description;

	}

	@Deprecated
	private String getMainPlanHTML(boolean isEropian) {
		if(!isEropian){
		return "            <table width=\"100%\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
				+ "               <tbody>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label id=\"lblInsuranceHD\" class=\"fntBold hdFontColor\">Travelling is fun and it's more fun when you travel with a peace of mind</label></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label>Travelling with peace of mind simply means your entire journey is protected with <font color=\"red\"><b><label id=\"insProductName\">TUNE PROTECT PLATINUM</label></b></font> travel insurance plan.</label></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label>For just <font color=\"red\"><b><label id=\"spnInsCost\">USD 19.60</label></b></font>, you will enjoy great coverage that includes:</label></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\" colspan=\"2\" valign=\"top\" width=\"100%\">"
				+ "                        <table class=\"insCoverInforTable\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">"
				+ "                           <tbody>"
				+ "                              <tr>"
				+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo1\"><font color=\"red\"><b>USD 50,000</b></font> for Accidental and Sickness Medical Reimbursement</label></td>"
				+ "                              </tr>"
				+ "                              <tr>"
				+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo2\"><font color=\"red\"><b>USD 20,000</b></font> for Emergency Medical Evacuation &amp; Repatriation</label></td>"
				+ "                              </tr>"
				+ "                              <tr>"
				+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo3\"><font color=\"red\"><b>USD 3,000</b></font> for Travel Cancellation or Curtailment</label></td>"
				+ "                              </tr>"
				+ "                           </tbody>"
				+ "                        </table>"
				+ "                     </td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label>and more...</label></td>"
				+ "                  </tr>"
				+ "   <tr>"
				+ "         <td class=\"pnlMid\" valign=\"top\">"
				+ "            <table width=\"650\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
				+ "               <tbody>"
				+ "                  <tr></tr>"
				+ "                  <tr>"
				+ "                     <td class=\"insuranceViewChange\">"
				+ "                        <table>"
				+ "                           <tbody>"
				+ "                              <tr>"
				+ "                                 <td class=\"alignLeft\" width=\"2%\"><input id=\"radAnciIns_Y\" name=\"chkIns\" checked=\"checked\" value=\"36.6\" type=\"checkbox\"></td>"
				+ "                                 <td class=\"alignLeft\" width=\"98%\"><label id=\"lblInsuranceYes\">Yes, I would like to purchase  Tune Protect Travel Protection by Air Arabia</label></td>"
				+ "                              </tr>" + "                           </tbody>"
				+ "                        </table>" + "                     </td>" + "                  </tr>"
				+ "                  <tr>" + "                     <td class=\"alignLeft\" colspan=\"2\">"
				+ "               </td></tr></tbody>" + "            </table>" + "         </td>" 
				+ "      </tr>"
				+ "		<tr><td class=\"rowGap\" colspan=\"2\"></td> </tr>"
				+ "		<tr><td class=\"pnlMid pnlGroupbottom\" colspan=\"3\"><img src=\"../images/spacer_no_cache.gif\"></td></tr>"
				+ "		<tr><td class=\"rowGap\" colspan=\"2\"></td></tr>"
				+ "</tbody>"
				+ "</table>";
		}else{
			return "            <table width=\"100%\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
					+ "               <tbody>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label id=\"lblInsuranceHD\" class=\"fntBold hdFontColor\">Travelling is fun and it's more fun when you travel with a peace of mind</label></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label>Travelling with peace of mind simply means your entire journey is protected with <font color=\"red\"><b><label id=\"insProductName\">TUNE PROTECT PLATINUM</label></b></font> travel insurance plan.</label></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label>For just <font color=\"red\"><b><label id=\"spnInsCost\">USD 19.60</label></b></font>, you will enjoy great coverage that includes:</label></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\" colspan=\"2\" valign=\"top\" width=\"100%\">"
					+ "                        <table class=\"insCoverInforTable\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">"
					+ "                           <tbody>"
					+ "                              <tr>"
					+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo1\"><font color=\"red\"><b>USD 50,000</b></font> for Accidental and Sickness Medical Reimbursement</label></td>"
					+ "                              </tr>"
					+ "                              <tr>"
					+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo2\"><font color=\"red\"><b>USD 20,000</b></font> for Emergency Medical Evacuation &amp; Repatriation</label></td>"
					+ "                              </tr>"
					+ "                              <tr>"
					+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo3\"><font color=\"red\"><b>USD 3,000</b></font> for Travel Cancellation or Curtailment</label></td>"
					+ "                              </tr>"
					+ "                           </tbody>"
					+ "                        </table>"
					+ "                     </td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label>and more...</label></td>"
					+ "                  </tr>"
					+ "   <tr>"
					+ "         <td class=\"pnlMid\" valign=\"top\">"
					+ "            <table width=\"650\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
					+ "         <tbody><tr></tr><tr><td class=\"insuranceViewChange\">"
					+"<table>"
					+"<tbody>"
					+"<tr>"
					+"<td class=\"alignLeft\" width=\"2%\"><input name=\"radAnciIns\" id=\"radAnciIns_Y\" autocomplete=\"off\" value=\"76\" type=\"radio\"></td>"
					+"<td class=\"alignLeft\" width=\"98%\"><label id=\"lblInsuranceYes\">Yes, I would like to purchase  Tune Protect Travel Protection by Air Arabia</label></td>"
					+"</tr>"
					+"</tbody>"
					+"</table>"
					+"</td>"
+""
+"<td class=\"insuranceViewChange\">"
+"<table>"
					+"<tbody>"
					+"<tr>"
					+"<td class=\"alignLeft\" width=\"2%\"><input name=\"radAnciIns\" id=\"radAnciIns_N\" autocomplete=\"off\" type=\"radio\"></td>"
					+"<td class=\"alignLeft\" width=\"98%\"><label id=\"lblInsuranceNo\">No Thanks, I don't need  Tune Protect Travel Protection by Air Arabia</label></td>"
					+"</tr>"
					+"</tbody>"
					+"</table>"
					+"</td>"
					+""
					+"</tr>"
					
+ "		<tr><td class=\"rowGap\" colspan=\"2\"></td> </tr>"
+ "		<tr><td class=\"pnlMid pnlGroupbottom\" colspan=\"3\"><img src=\"../images/spacer_no_cache.gif\"></td></tr>"
+ "		<tr><td class=\"rowGap\" colspan=\"2\"></td></tr>"
					+"</tbody>   "   
				    +"</table>";
		}
	}

	@Deprecated
	private String getRiderPlanHTML(boolean isEropian) {
		if(!isEropian){
		return "            <table width=\"100%\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
				+ "               <tbody>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label id=\"lblInsuranceHD\" class=\"fntBold hdFontColor\">By any Chance if you Missed your Flight</label></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label>Travelling with peace of mind simply means your entire journey is protected with <font color=\"red\"><b><label id=\"insProductName_Rider\">TUNE MISSED FLIGHT</label></b></font>  insurance plan.</label></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label>For just <font color=\"red\"><b><label id=\"spnInsCost_Rider\">USD 9.634</label></b></font>, you will enjoy great coverage that includes:</label></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\" colspan=\"2\" valign=\"top\" width=\"100%\">"
				+ "                        <table class=\"insCoverInforTable\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">"
				+ "                           <tbody>"
				+ "                              <tr>"
				+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo1\"><font color=\"red\"><b>Full Refund</b></font> for Connection Flight Missed</label></td>"
				+ "                              </tr>"
				+ "                           </tbody>"
				+ "                        </table>"
				+ "                     </td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
				+ "                  </tr>"
				+ "                  <tr>"
				+ "                     <td class=\"alignLeft\"><label>and more...</label></td>"
				+ "                  </tr>"
				+ "   <tr>"
				+ "         <td class=\"pnlMid\" valign=\"top\">"
				+ "            <table width=\"650\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
				+ "               <tbody>"
				+ "                  <tr></tr>"
				+ "                  <tr>"
				+ "                     <td class=\"insuranceViewChange\">"
				+ "                        <table>"
				+ "                           <tbody>"
				+ "                              <tr>"
				+ "                                 <td class=\"alignLeft\" width=\"2%\"><input id=\"radAnciIns_Y_Rider\" name=\"chkIns\" checked=\"checked\" value=\"36.6\" type=\"checkbox\"></td>"
				+ "                                 <td class=\"alignLeft\" width=\"98%\"><label id=\"lblInsuranceYes\">Yes, I would like to purchase  Missed Flight Protection by Air Arabia</label></td>"
				+ "                              </tr>" + "                           </tbody>"
				+ "                        </table>" + "                     </td>" + "                  </tr>"
				+ "                  <tr>" + "                     <td class=\"alignLeft\" colspan=\"2\">"
				+ "               </td></tr></tbody>" + "            </table>" + "         </td>" + "      </tr>"
				+ "		<tr>                     <td class=\"rowGap\" colspan=\"2\"></td>                  </tr>"
				+ "<tr>  <td class=\"pnlMid pnlGroupbottom\" colspan=\"3\"><img src=\"../images/spacer_no_cache.gif\"></td></tr>"
				+ "		<tr>                     <td class=\"rowGap\" colspan=\"2\"></td>                  </tr>"
				+ "               </tbody>" + "            </table>";
		}else {
			return "            <table width=\"100%\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
					+ "               <tbody>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label id=\"lblInsuranceHD\" class=\"fntBold hdFontColor\">By any Chance if you Missed your Flight</label></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label>Travelling with peace of mind simply means your entire journey is protected with <font color=\"red\"><b><label id=\"insProductName_Rider\">TUNE MISSED FLIGHT</label></b></font>  insurance plan.</label></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label>For just <font color=\"red\"><b><label id=\"spnInsCost_Rider\">USD 9.634</label></b></font>, you will enjoy great coverage that includes:</label></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\" colspan=\"2\" valign=\"top\" width=\"100%\">"
					+ "                        <table class=\"insCoverInforTable\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">"
					+ "                           <tbody>"
					+ "                              <tr>"
					+ "                                 <td class=\"withIcon\"><span> </span><label id=\"insCoverInfo1\"><font color=\"red\"><b>Full Refund</b></font> for Connection Flight Missed</label></td>"
					+ "                              </tr>"
					+ "                           </tbody>"
					+ "                        </table>"
					+ "                     </td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"rowGap\" colspan=\"2\"></td>"
					+ "                  </tr>"
					+ "                  <tr>"
					+ "                     <td class=\"alignLeft\"><label>and more...</label></td>"
					+ "                  </tr>"
					+ "   <tr>"
					+ "         <td class=\"pnlMid\" valign=\"top\">"
					+ "            <table width=\"650\" cellpadding=\"1\" cellspacing=\"1\" border=\"0\">"
					+ "               <tbody><tr></tr><tr><td class=\"insuranceViewChange\">"
					+"<table>"
					+"<tbody>"
					+"<tr>"
					+"<td class=\"alignLeft\" width=\"2%\"><input name=\"radAnciInsRider\" id=\"radAnciIns_Y_Rider\" autocomplete=\"off\" value=\"76\" type=\"radio\"></td>"
					+"<td class=\"alignLeft\" width=\"98%\"><label id=\"lblInsuranceYes\">Yes, I would like to purchase  Tune Protect Travel Protection by Air Arabia</label></td>"
					+"</tr>"
					+"</tbody>"
					+"</table>"
					+"</td>"
+""
+"<td class=\"insuranceViewChange\">"
+"<table>"
+"<tbody>"
+"<tr>"
+"<td class=\"alignLeft\" width=\"2%\"><input name=\"radAnciInsrider\" id=\"radAnciIns_N_Rider\" autocomplete=\"off\" type=\"radio\"></td>"
+"<td class=\"alignLeft\" width=\"98%\"><label id=\"lblInsuranceNo\">No Thanks, I don't need  Tune Protect Travel Protection by Air Arabia</label></td>"
+"</tr>"
+"</tbody>"
+"</table>"
+"</td>"
+""
+"</tr>"
+ "		<tr><td class=\"rowGap\" colspan=\"2\"></td> </tr>"
+ "		<tr><td class=\"pnlMid pnlGroupbottom\" colspan=\"3\"><img src=\"../images/spacer_no_cache.gif\"></td></tr>"
+ "		<tr><td class=\"rowGap\" colspan=\"2\"></td></tr>"
+"</tbody>   "   
+"</table>";			
		}
	}

	
	private String getTnC() {
		return "";
	}

}
