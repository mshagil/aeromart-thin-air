package com.isa.thinair.wsclient.core.service.lms.impl.gravty;

import java.time.LocalDateTime;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.GravtyConfig;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtySysDto.Credentials;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtySysDto.SystemAuthTokenResponse;

public class GravtyTokenHandler {

	private static Log log = LogFactory.getLog(GravtyTokenHandler.class);
	
	private static String authToken = null;

	private static LocalDateTime tokenExpireTime = LocalDateTime.now().minusHours(1);

	public static final GravtyConfig gravtyConfig = WSClientModuleUtils.getModuleConfig().getLmsClientConfig().getGravtyConfig();

	@SuppressWarnings("unchecked")
	private static GravtySysDto.SystemAuthTokenResponse generateNewSystemToken() throws ModuleException {
		GravtyRestServiceClient<SystemAuthTokenResponse, Credentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.SYS_LOGIN);

		GravtySysDto.Credentials request = new GravtySysDto.Credentials();
		String username = (String) gravtyConfig.getUsername();
		String password = (String) gravtyConfig.getPassword();

		request.setUsername(username);
		request.setPassword(password);

		GravtySysDto.SystemAuthTokenResponse sysAuthRes = serviceClent.post(request);
		return sysAuthRes;
	}

	public static String getAuthToken() throws ModuleException {
		if (isExpireToken()) {
			log.info("=============== Auth Token Expired =====================");
			return updateToken();
		}
		return authToken;
	}

	private static synchronized String updateToken() throws ModuleException {

		if (isExpireToken()) {
			log.info("=============== Auth Token Generation Started =====================");
			GravtySysDto.SystemAuthTokenResponse newAuth = generateNewSystemToken();
			Long expireTimeIncBuffer = Long.parseLong(newAuth.getExpires_in()) - gravtyConfig.getExpirationBuffer();
			authToken = newAuth.getToken();
			tokenExpireTime = LocalDateTime.now().plusSeconds(expireTimeIncBuffer);
			log.info("=============== Auth Token Generation Successs =====================");
		}
		return authToken;
	}

	private static boolean isExpireToken() {
		return LocalDateTime.now().isAfter(tokenExpireTime);
	}

}
