package com.isa.thinair.wsclient.core.service.lms.impl.aeroreward;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto.Member;

public class AeroRewardDtoUtils {

	public static LoyaltyPointDetailsDTO adaptToLoyaltyPointDetailsDTO(Member member) {

		LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = new LoyaltyPointDetailsDTO();
		loyaltyPointDetailsDTO.setMemberAccountId(member.getLoginName());
		loyaltyPointDetailsDTO.setMemberPointsAvailable(member.getLoyaltyPoints().getPointsAvailable());
		loyaltyPointDetailsDTO.setMemberPointsDeducted(member.getLoyaltyPoints().getPointsDeducted());
		loyaltyPointDetailsDTO.setMemberPointsEarned(member.getLoyaltyPoints().getPointsEarnedTotal());
		loyaltyPointDetailsDTO.setMemberPointsExpired(member.getLoyaltyPoints().getPointsExpired());
		loyaltyPointDetailsDTO.setMemberPointsLocked(member.getLoyaltyPoints().getPointsSystemBlocked());
		loyaltyPointDetailsDTO.setMemberPointsUsed(member.getLoyaltyPoints().getPointsBurnedTotal());
		return loyaltyPointDetailsDTO;

	}

}
