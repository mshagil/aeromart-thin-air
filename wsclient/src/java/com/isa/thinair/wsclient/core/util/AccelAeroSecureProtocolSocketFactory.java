package com.isa.thinair.wsclient.core.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * A SecureProtocolSocketFactory that uses JSSE to create sockets. This implementation uses an inner SSLSocketFactory
 * that allows both trusted and un-trusted SSL certificates to be used.
 * 
 * @author Zaki Saimeh
 */
public class AccelAeroSecureProtocolSocketFactory implements SecureProtocolSocketFactory {

	private final Log log = LogFactory.getLog(AccelAeroSecureProtocolSocketFactory.class);

	/**
	 * The factory singleton.
	 */
	private static final AccelAeroSecureProtocolSocketFactory factory = new AccelAeroSecureProtocolSocketFactory();

	/**
	 * Gets an singleton instance of the AccelAeroSecureProtocolSocketFactory.
	 * 
	 * @return a AccelAeroSecureProtocolSocketFactory
	 */
	public static AccelAeroSecureProtocolSocketFactory getSocketFactory() {
		return factory;
	}

	private SSLSocketFactory innerSSLSocketFactory;

	/**
	 * Constructor for AccelAeroSecureProtocolSocketFactory.
	 */
	public AccelAeroSecureProtocolSocketFactory() {
		super();

		// Create a trust manager that does not validate certificate chains
		// (effectively allowing un-trusted certificates to be automatically used)
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			innerSSLSocketFactory = sc.getSocketFactory();

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		} catch (Exception e) {
			log.error(e);
		}

	}

	/**
	 * @see SecureProtocolSocketFactory#createSocket(java.lang.String,int,java.net.InetAddress,int)
	 */
	public Socket createSocket(String host, int port, InetAddress clientHost, int clientPort) throws IOException,
			UnknownHostException {
		return innerSSLSocketFactory.createSocket(host, port, clientHost, clientPort);
	}

	/**
	 * @see SecureProtocolSocketFactory#createSocket(java.lang.String,int)
	 */
	public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
		return innerSSLSocketFactory.createSocket(host, port);
	}

	/**
	 * @see SecureProtocolSocketFactory#createSocket(java.net.Socket,java.lang.String,int,boolean)
	 */
	public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
		return ((SSLSocketFactory) innerSSLSocketFactory).createSocket(socket, host, port, autoClose);
	}

	/**
	 * Attempts to get a new socket connection to the given host within the given time limit. (The time limit is set in
	 * the passed HttpConnectionParams object)
	 * 
	 * @param host
	 *            the host name/IP
	 * @param port
	 *            the port on the host
	 * @param localAddress
	 *            the local host name/IP to bind the socket to
	 * @param localPort
	 *            the port on the local machine
	 * @param params
	 *            {@link HttpConnectionParams Http connection parameters}
	 * 
	 * @return Socket a new socket
	 * 
	 * @throws IOException
	 *             if an I/O error occurs while creating the socket
	 * @throws UnknownHostException
	 *             if the IP address of the host cannot be determined
	 */
	public Socket createSocket(String host, int port, InetAddress localAddress, int localPort, HttpConnectionParams params)
			throws IOException, UnknownHostException, ConnectTimeoutException {
		if (params == null) {
			throw new IllegalArgumentException("Parameters may not be null");
		}

		int timeout = params.getConnectionTimeout();
		if (timeout == 0) {
			return createSocket(host, port, localAddress, localPort);
		} else {
			Socket socket = createSocket(host, port, localAddress, localPort);
			socket.setSoTimeout(timeout);
			return socket;
		}
	}

	/**
	 * All instances of AccelAeroSecureProtocolSocketFactory are the same.
	 */
	public boolean equals(Object obj) {
		return ((obj != null) && obj.getClass().equals(AccelAeroSecureProtocolSocketFactory.class));
	}

	/**
	 * All instances of AccelAeroSecureProtocolSocketFactory have the same hash code.
	 */
	public int hashCode() {
		return AccelAeroSecureProtocolSocketFactory.class.hashCode();
	}

}