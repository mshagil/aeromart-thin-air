package com.isa.thinair.wsclient.core.service.cf;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.xml.namespace.QName;
import javax.xml.rpc.Service;

import org.apache.axis.client.ServiceFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.FlightLegLoad;
import com.isa.thinair.airreservation.api.model.FlightLoad;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import org.tempuri.FileUploadInfoSoap;
import org.tempuri.FileUploadInfoSoapStub;
import org.tempuri.FlightDetail;
import org.tempuri.FlightLegDetail;
import org.tempuri.FlightLoadInfo;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;

/**
 * Client implementation of web service for cargo flash to publish baggage load information.
 * 
 * @author vidula
 *
 */
public class CargoFlashWebservicesImpl {

	private static final String RESPONSE_SUCCESS = "Success";
	private static final String CARGO_FLASH = "cargo-flash";
	private static Log log = LogFactory.getLog(CARGO_FLASH);
	private String serviceURL = PlatformUtiltiies.nullHandler(WSClientModuleUtils.getModuleConfig().getServiceURLsMap().get(ExternalWebServiceInvoker.SERVICE_PROVIDER_CF));
	
	/**
	 * Web service method to publish load details for each flight.
	 * @param flightLoadInformationDTOs
	 * @return true if the WS call success.
	 * @throws ModuleException
	 */
	public boolean publishFlightLoadInfo(List<FlightLoad> flightLoadInformationDTOs) throws ModuleException {
		
		FlightLoadInfo request = null;
		String response = null;
		
		try {
			log.info("[CargoFlashWebservicesImpl::publishFlightLoadInfo()] Begin");
			if (log.isDebugEnabled()) {
				log.debug("[CargoFlashWebservicesImpl::publishFlightLoadInfo()] - " + flightLoadInformationDTOs);
			}
			FileUploadInfoSoap serviceStub = getClientStub();
			request = convert(flightLoadInformationDTOs);
			response = serviceStub.publishFlightLoadInfo(request);
			response = response.replaceAll("\\.", "");
			return RESPONSE_SUCCESS.equalsIgnoreCase(response);
		} catch (Exception e) {
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		} finally {
			CargoFlashAuditor.log(log, "CARGO_FLASH_LOAD", request, response);
		}
	}
	
	private FileUploadInfoSoap getClientStub() throws ModuleException {
		try {
			URL wsdlURL = new URL(serviceURL);
			QName qname = new QName("http://tempuri.org/", "FileUploadInfo");

			ServiceFactory serviceFactory = new ServiceFactory();
			Service paymentService = serviceFactory.createService(qname);
			return new FileUploadInfoSoapStub(wsdlURL, paymentService);

		} catch (Exception e) {
			throw new ModuleException(e, "wsclient.serviceinvocation.failed");
		}
	}
	
	private static FlightLoadInfo convert(List<FlightLoad> flightLoads) {
		if (flightLoads == null || flightLoads.size() == 0) {
			return new FlightLoadInfo(new FlightDetail[0]);
		}
		List<FlightDetail> detailList = new ArrayList<FlightDetail>();
		for (FlightLoad flightLoad : flightLoads) {
			detailList.add(convert(flightLoad));
		}
		return new FlightLoadInfo(detailList.toArray(new FlightDetail[flightLoads.size()]));
	}
	
	private static FlightDetail convert(FlightLoad flightLoad) {
		List<FlightLegDetail> legDetails = new ArrayList<FlightLegDetail>();
		for (FlightLegLoad legLoad : flightLoad.getFlightLegLoad()) {
			legDetails.add(convert(legLoad));
		}
		Calendar flightDateTime = Calendar.getInstance();
		flightDateTime.setTime(flightLoad.getDepartureDate());
		return new FlightDetail(flightDateTime, legDetails.toArray(new FlightLegDetail[legDetails.size()]),
				flightLoad.getFlightNo());
	}
	
	private static FlightLegDetail convert(FlightLegLoad legLoad) {
		Calendar arrivalZulu = Calendar.getInstance(TimeZone.getTimeZone("Zulu"));
		arrivalZulu.setTime(legLoad.getArrivalTimeZulu());
		Calendar departureZulu = Calendar.getInstance(TimeZone.getTimeZone("Zulu"));
		departureZulu.setTime(legLoad.getDepartureTimeZulu());
		return new FlightLegDetail(arrivalZulu, legLoad.getBaggageLoad(), legLoad.getChildCount(),
				departureZulu, legLoad.getDestination(), legLoad.getFemaleCount(), legLoad.getInfantCount(),
				legLoad.getMaleCount(), legLoad.getOrigin());
	}
}
