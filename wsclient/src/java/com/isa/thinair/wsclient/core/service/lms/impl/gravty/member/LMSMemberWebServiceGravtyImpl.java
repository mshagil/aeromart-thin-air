package com.isa.thinair.wsclient.core.service.lms.impl.gravty.member;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.core.service.LoyaltyExternalConfig;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.GravtyConfig;
import com.isa.thinair.wsclient.core.service.lms.base.member.LMSMemberWebService;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Country;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.EnrollmentChannel;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberByIDResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberData;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberRequestData;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Nationality;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.PasswordSet;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.PrefferedLanguage;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.userCredentials;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDtoUtils;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyRestServiceClient;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.LMSGravtyWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.lms.impl.smartbutton.LMSCommonUtil;

public class LMSMemberWebServiceGravtyImpl implements LMSMemberWebService {
	
	public static final GravtyConfig gravtyConfig = WSClientModuleUtils.getModuleConfig().getLmsClientConfig().getGravtyConfig();

	private static Log log = LogFactory.getLog(LMSMemberWebServiceGravtyImpl.class);
	private static Map<String, Integer> countryInternalIds = null;
	private static Map<String, String> natinalityInternalIds = null;
	private static List<String> enrollmentChannelInternalIds = null;
	private static List<String> prefferedLanguageList=null;
	public static final String AT = "@";
	public static final String DATE_OF_BIRTH_FORMAT = "yyyy-MM-dd";

	@Override
	public LoyaltyMemberStateDTO getMemberState(String memberAccountId, int internaMemberId) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String enrollMemberWithEmail(Customer customer) throws ModuleException {
		GravtyDto.MemberData memberData = createMember(customer);
		return memberData.getMember_id();
	}

	@Override
	public boolean addMemberAccountId(String WSSecurityToken, String memberAccountId, String memberExternalId,
			String idTypeExternalReference, String memberAccountIdToAdd) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public LoyaltyMemberCoreDTO getMemberCoreDetails(String memberAccountId) throws ModuleException {
		LoyaltyMemberCoreDTO loyaltyCoreDTO = null;
		GravtyDto.MemberFetchDataResponse memberDataFromGravty = getMemberFromEmail(memberAccountId);
		loyaltyCoreDTO = LMSCommonUtil.populateLoyaltyMemberCoreDTO(memberAccountId, memberDataFromGravty);
		return loyaltyCoreDTO;
	}

	@Override
	public LoyaltyPointDetailsDTO getMemberPointBalances(String memberAccountId, String memberExternalId) throws ModuleException {
		LoyaltyPointDetailsDTO loyaltyPointDTO = null;
		GravtyDto.MemberFetchDataResponse memberDataFromGravty;

		if (StringUtils.isEmpty(memberExternalId)) {
			memberDataFromGravty = getMemberFromEmail(memberAccountId);
		} else {
			memberDataFromGravty = getMemberByExternalRef(memberExternalId);
		}

		loyaltyPointDTO = LMSCommonUtil.populateLoyaltyPointDetailsDTO(memberAccountId, memberDataFromGravty);
		return loyaltyPointDTO;
	}
	
	@SuppressWarnings("unchecked")
	private MemberFetchDataResponse getMemberByExternalRef(String memberExternalId) throws ModuleException {
		
		GravtyDto.MemberFetchDataResponse member = null;

		GravtyRestServiceClient<MemberByIDResponse, String> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.FETCH_MEMBER);
		serviceClent.setUrl(serviceClent.getUrl() + memberExternalId);
		MemberByIDResponse response= serviceClent.get();
		
		if(response != null){
			member = response.getData();
		}

		return member;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  GravtyDto.MemberFetchDataResponse getMemberFromEmail(String memberAccountId) throws ModuleException {

		List<GravtyDto.MemberFetchDataResponse> memberList = null;
		GravtyDto.MemberFetchDataResponse member = null;

		if (memberAccountId != null) {
			@SuppressWarnings("unchecked")
			GravtyRestServiceClient<MemberFetchDataResponse, String> serviceClent = LMSGravtyWebServiceInvoker
					.getServiceCleint(LMSGravtyWebServiceInvoker.FETCH_MEMBER_BY_EMAIL);
			serviceClent.setUrl(serviceClent.getUrl() + memberAccountId);
			HttpResponse response = serviceClent.getHttpResponse();

			memberList = getMemberList(response);

			if (memberList != null && !memberList.isEmpty()) {
				member = memberList.get(0);
			}
		}

		return member;
	}

	@SuppressWarnings("unchecked")
	private List<MemberFetchDataResponse> getMemberList(HttpResponse response) throws ModuleException {
		Gson gson = new Gson();

		try {
			return (List<MemberFetchDataResponse>) gson.fromJson(EntityUtils.toString(response.getEntity()),
					new TypeToken<List<MemberFetchDataResponse>>() {
					}.getType());
		} catch (JsonSyntaxException e) {
			log.error(e.getMessage());
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		} catch (ParseException e) {
			log.error(e.getMessage());
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		} catch (IOException e) {
			log.error(e.getMessage());
			throw new ModuleException("wsclient.loyalty.remote.service.invocation.error", "reward.service.client");
		}

	}

	@Override
	public boolean saveMemberCustomFields(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException {
		//updateToken();
		GravtyRestServiceClient<PasswordSet, userCredentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.MEMBER_PASSWORD);
		GravtyDto.userCredentials request = new GravtyDto.userCredentials();

		request.setUsername(memberAccountId);
		request.setPassword(password);

		GravtyDto.PasswordSet sysreturns = serviceClent.post(request);
		return false;
	}

	@Override
	public boolean saveMemberEmailAddress(String memberAccountId, String emailAddress) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveMemberPhoneNumbers(String memberAccountId, String phoneNumber, String mobileNumber)
			throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveMemberPreferredCommunicationLanguage(String memberAccountId, String languageCode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveLoyaltySystemReferredByUser(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getCrossPortalLoginUrl(String memberAccountId, String password, String menuItemIntRef)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendMemberWelcomeMessage(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int saveMemberName(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateMember(Customer customer) throws ModuleException {
		MemberFetchDataResponse member = getMemberByExternalRef(customer.getLMSMemberDetails().getMemberExternalId());
		LmsMember lmsMember = customer.getLMSMemberDetails();
		com.isa.thinair.airmaster.api.model.Nationality nationality = WSClientModuleUtils.getCommonServiceBD().getNationality(customer.getNationalityCode());
		GravtyRestServiceClient<MemberData, MemberRequestData> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.UPDATE_MEMBER);
		serviceClent.setUrl(serviceClent.getUrl() + "/" + member.getMember_id()+"/");
		
		//MemberFetchDataResponse refferalMember = getMemberFromEmail(customer.getLMSMemberDetails().getRefferedEmail());

		GravtyDto.MemberRequestData request = new GravtyDto.MemberRequestData();

		
		GravtyDto.User user = new GravtyDto.User();
		user.setEmail(member.getUser().getEmail());
		user.setFirstName(lmsMember.getFirstName());
		user.setLastName(lmsMember.getLastName());

		request.setUser(user);
		request.setMember_name(lmsMember.getFirstName() + " " + lmsMember.getLastName());
		request.setSalutation(customer.getTitle());
		request.setDate_of_birth(getFormatDateOfBirth(lmsMember.getDateOfBirth()));
		request.setGender(GravtyDtoUtils.genderType.get(lmsMember.getGenderTypeId()));
		request.setMobile(lmsMember.getMobileNumber().replace("-", ""));
		request.setAddress_line1(customer.getAddressLine());
		request.setCity(customer.getCity());
		request.setZipcode(customer.getZipCode());
		request.setEnrolling_sponsor(member.getEnrolling_sponsor());
		request.setEnrollment_channel(getEnrollmentChannel(customer.getLMSMemberDetails().getEnrollingChannelExtRef()));
		request.setEnrollment_referrer(member.getEnrollment_referrer());
		request.setFamily_head(formatEmailForGravty(lmsMember.getHeadOFEmailId()));
		request.setNationality(nationality.getIcaoCode());
		request.setCountry(getCountryCode(customer.getCountryCode()));
		request.addCommunicationLanguage(getLanguageCode(customer.getLMSMemberDetails().getLanguage()));
		request.addAgentCode(customer.getLMSMemberDetails().getEnrollingChannelIntRef());
		request.setPassport_number(customer.getLMSMemberDetails().getPassportNum());
		
		if(log.isDebugEnabled()){
            log.debug(request.toString());
        }

		GravtyDto.MemberData memberData = serviceClent.put(request);
		
		if(log.isDebugEnabled()){
            log.debug(memberData.toString());
        }
	}
//TODO Remove this when Gravty Fix email case sensitive issue
	private String formatEmailForGravty(String email) {
		String formatEmail = null;
		if(!StringUtil.isNullOrEmpty(email)){
			String[] emailParts = email.split(AT);
			formatEmail = emailParts[0].toUpperCase()+AT+emailParts[1];
		}	
		return formatEmail;
	}

	@SuppressWarnings("unchecked")
	@Override
	public MemberRemoteLoginResponseDTO memberLogin(String userName, String password) throws ModuleException {
		GravtyRestServiceClient<MemberRemoteLoginResponseDTO, userCredentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.LOGIN_MEMBER);

		GravtyDto.userCredentials request = new GravtyDto.userCredentials();

		request.setUsername(userName);
		request.setPassword(password);

		MemberRemoteLoginResponseDTO sysreturns = serviceClent.post(request);

		return sysreturns;
	}


	@SuppressWarnings("unchecked")
	public MemberData createMember(Customer customer) throws ModuleException {
		GravtyRestServiceClient<MemberData, MemberRequestData> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.CREATE_MEMBER);

		LoyaltyExternalConfig externalConfig = (LoyaltyExternalConfig) PromotionModuleServiceLocator.getLoyaltyExternalConfig();
		int sponserId = externalConfig.getSponserId();

		com.isa.thinair.airmaster.api.model.Nationality nationality = WSClientModuleUtils.getCommonServiceBD().getNationality(customer.getNationalityCode());

		GravtyDto.MemberRequestData request = new GravtyDto.MemberRequestData();

		String referrelMemberId = getRefferelMemberExternalId(customer);

		GravtyDto.User user = new GravtyDto.User();
		user.setEmail(customer.getLMSMemberDetails().getFfid());
		user.setFirstName(customer.getLMSMemberDetails().getFirstName());
		user.setLastName(customer.getLMSMemberDetails().getLastName());

		request.setUser(user);
		request.setMember_name(customer.getLMSMemberDetails().getFirstName() + " " + customer.getLMSMemberDetails().getLastName());
		request.setSalutation(customer.getTitle().toUpperCase());
		request.setDate_of_birth(getFormatDateOfBirth(customer.getLMSMemberDetails().getDateOfBirth()));
		request.setGender(GravtyDtoUtils.genderType.get(customer.getLMSMemberDetails().getGenderTypeId()));
		request.setNationality(nationality.getIcaoCode());
		request.setMobile(customer.getMobile() != null ?customer.getMobile().replace("-", "") : null);
		request.setAddress_line1(customer.getAddressLine());
		request.setAddress_line2(null);
		request.setCity(customer.getCity());
		request.setRegion(null);
		request.setZipcode(customer.getZipCode());
		request.setEnrollment_channel(getEnrollmentChannel(customer.getLMSMemberDetails().getEnrollingChannelExtRef()));
		request.setEnrollment_referrer(referrelMemberId);
		request.setCountry(getCountryCode(customer.getCountryCode()));
		request.setFamily_head(formatEmailForGravty(customer.getLMSMemberDetails().getHeadOFEmailId()));
		request.setEnrolling_sponsor(sponserId);
		request.addCommunicationLanguage(getLanguageCode(customer.getLMSMemberDetails().getLanguage()));
		request.addAgentCode(customer.getLMSMemberDetails().getEnrollingChannelIntRef());
		request.setPassport_number(customer.getLMSMemberDetails().getPassportNum());

		if(log.isDebugEnabled()){
            log.debug(request.toString());
        }
		
		GravtyDto.MemberData memberData = serviceClent.post(request);
		
		if(log.isDebugEnabled()){
            log.debug(memberData.toString());
        }
		
		return memberData;
	}

	private String getRefferelMemberExternalId(Customer customer) throws ModuleException {
		String referrelMemberId = null;
		if (!StringUtils.isEmpty(customer.getLMSMemberDetails().getRefferedEmail())) {

			LmsMemberServiceBD lmsMemberServiceBD = (LmsMemberServiceBD) WSClientModuleUtils.getLmsMemberServiceBD();
			LmsMember refferelMemberLocal = lmsMemberServiceBD.getLmsMember(customer.getLMSMemberDetails().getRefferedEmail());

			if (refferelMemberLocal != null && !StringUtils.isEmpty(refferelMemberLocal.getMemberExternalId())) {
				referrelMemberId = refferelMemberLocal.getMemberExternalId();
			} else {
				MemberFetchDataResponse refferelMemberRemote = getMemberFromEmail(customer.getLMSMemberDetails()
						.getRefferedEmail());
				referrelMemberId = refferelMemberRemote.getMember_id();
			}
		}
		return referrelMemberId;
	}

	public String getLanguageCode(String languageCode) throws ModuleException {

		if (!StringUtil.isNullOrEmpty(languageCode)) {
			languageCode = languageCode.toUpperCase();
			if (!getPrefferedLanguages().contains(languageCode)) {
				languageCode = null;
			}
		}
		return languageCode;
	}
	
	private String getFormatDateOfBirth(Date date) {

		String dateOfBirth = null;

		if (date != null) {
			dateOfBirth = CalendarUtil.formatDate(date, DATE_OF_BIRTH_FORMAT);
		}

		return dateOfBirth;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Country> getCountriesList() throws ModuleException {
		GravtyRestServiceClient<Country[], userCredentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.COUNTRY);

		GravtyDto.Country[] sysreturns = serviceClent.get();
		List<Country> arrayList = new ArrayList<Country>(Arrays.asList(sysreturns));

		return arrayList;
	}

	@SuppressWarnings("unchecked")
	public List<Nationality> getNationalitiesList() throws ModuleException {
		GravtyRestServiceClient<Nationality[], userCredentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.NATIONALITY);

		GravtyDto.Nationality[] sysreturns = serviceClent.get();
		List<Nationality> arrayList = new ArrayList<Nationality>(Arrays.asList(sysreturns));

		return arrayList;
	}


	public Integer getCountryCode(String countryCode) throws ModuleException {
		if (countryCode != null) {

			if (countryInternalIds == null) {
				List<Country> countries = getCountriesList();
				countryInternalIds = new HashMap<String, Integer>();

				for (Country c : countries) {
					countryInternalIds.put(c.getIso_code(), c.getId());
				}

			}
			return countryInternalIds.get(countryCode);
		}
		return null;

	}

	public String getNationalityCode(String nationalityName) throws ModuleException {
		if (nationalityName != null) {
			if (natinalityInternalIds == null) {

				natinalityInternalIds = new HashMap<String, String>();
				List<Nationality> nationalitiesList = getNationalitiesList();
				for (Nationality n : nationalitiesList) {
					natinalityInternalIds.put(n.getName(), n.getCode());
				}
			}
			return natinalityInternalIds.get(nationalityName);
		}
		return null;
	}

	public String getEnrollmentChannel(String enrollmentChannelName) throws ModuleException {

		if (enrollmentChannelName != null) {
			if (enrollmentChannelInternalIds == null) {
				enrollmentChannelInternalIds = new ArrayList<String>();
				List<EnrollmentChannel> enrollmentChannel = getEnrollmentChannelsList();

				for (EnrollmentChannel e : enrollmentChannel) {
					enrollmentChannelInternalIds.add(e.getCode());
				}
			}

			if (enrollmentChannelInternalIds.contains(enrollmentChannelName)) {
				return enrollmentChannelName;
			}
		}
		return null;
	}
	
	public static List<String> getPrefferedLanguages() throws ModuleException {
		if(prefferedLanguageList == null){		
			List<PrefferedLanguage> prefferedLanguages = getPrefferedLanguagesList();
			prefferedLanguageList = new ArrayList<String>();
			for(PrefferedLanguage language : prefferedLanguages){
				prefferedLanguageList.add(language.getCode());
			}			
		}
		return prefferedLanguageList;
	}
	

	@SuppressWarnings("unchecked")
	public List<EnrollmentChannel> getEnrollmentChannelsList() throws ModuleException {
		GravtyRestServiceClient<EnrollmentChannel[], userCredentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.ENROLLMENT_CHANNEL);

		GravtyDto.EnrollmentChannel[] sysreturns = serviceClent.get();
		List<EnrollmentChannel> arrayList = new ArrayList<EnrollmentChannel>(Arrays.asList(sysreturns));

		return arrayList;
	}

	@SuppressWarnings("unchecked")
	public static List<PrefferedLanguage> getPrefferedLanguagesList() throws ModuleException {
		GravtyRestServiceClient<PrefferedLanguage[], userCredentials> serviceClent = LMSGravtyWebServiceInvoker
				.getServiceCleint(LMSGravtyWebServiceInvoker.PREFFERED_LANGUAGE);

		GravtyDto.PrefferedLanguage[] languagesList = serviceClent.get();
		List<PrefferedLanguage> arrayList = new ArrayList<PrefferedLanguage>(Arrays.asList(languagesList));

		return arrayList;
	}
	
}
