package com.isa.thinair.wsclient.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckRequestDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.rak.RakFlightInsuranceInfoDTO;
import com.isa.thinair.airreservation.api.model.FlightLoad;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airschedules.api.model.FlightsToPublish;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusTransactionInfoDTO;
import com.isa.thinair.paymentbroker.api.dto.PAYPALRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonResponse;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentFieldsResponse;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentIdRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtInquiryRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtPayRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtRefundRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtReversalRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtSettleRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtVerifyRequest;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_HomeResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundServiceResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SRTMRefundStatusResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCaptureRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRs;
import com.isa.thinair.paymentbroker.api.migs.MIGSRequest;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.reporting.api.model.InventoryAllocationTO;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardRequest;
import com.isa.thinair.wsclient.api.dto.lms.IssueRewardResponse;
import com.isa.thinair.wsclient.api.dto.netcast.NetCastRequest;
import com.isa.thinair.wsclient.core.client.BankWebServicesInvoker;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.ameria.AmeriaWebServiceImpl;
import com.isa.thinair.wsclient.core.service.bd.WSClientBDImpl;
import com.isa.thinair.wsclient.core.service.bd.WSClientBDLocalImpl;
import com.isa.thinair.wsclient.core.service.behpardakht.BehpardakhtWebServiceImpl;
import com.isa.thinair.wsclient.core.service.blockspace.BlockSpaceWebservicesClientImpl;
import com.isa.thinair.wsclient.core.service.blockspace.InventoryAllocationsClientImpl;
import com.isa.thinair.wsclient.core.service.cf.CargoFlashFlightUpdateWebserviceImpl;
import com.isa.thinair.wsclient.core.service.cf.CargoFlashWebservicesImpl;
import com.isa.thinair.wsclient.core.service.cp.CPWebservicesImpl;
import com.isa.thinair.wsclient.core.service.db.DBWebServicesImpl;
import com.isa.thinair.wsclient.core.service.dib.DIBWebServicesImpl;
import com.isa.thinair.wsclient.core.service.ebi.EBIWebervicesImpl;
import com.isa.thinair.wsclient.core.service.ec.ECWebervicesImpl;
import com.isa.thinair.wsclient.core.service.ep.EPWebServicesImpl;
import com.isa.thinair.wsclient.core.service.lms.base.LMSServiceFactory;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.common.LMSCommonServiceGravtyImpl;
import com.isa.thinair.wsclient.core.service.migs.MigsWebServicesImpl;
import com.isa.thinair.wsclient.core.service.netcast.NetCastWebServiceImpl;
import com.isa.thinair.wsclient.core.service.parsian.ParsianWebServicesImpl;
import com.isa.thinair.wsclient.core.service.payfort.PayfortWebServicesImpl;
import com.isa.thinair.wsclient.core.service.pp.PPWebServicesImpl;
import com.isa.thinair.wsclient.core.service.rak.RakBL;
import com.isa.thinair.wsclient.core.service.rak.RakProxy;
import com.isa.thinair.wsclient.core.service.rm.RMWebservicesClientImpl;
import com.isa.thinair.wsclient.core.service.saman.SamanWebServicesImpl;
import com.isa.thinair.wsclient.core.service.tap.TapGoSellWebServiceImpl;
import com.isa.thinair.wsclient.core.service.typeb.TypeBResWebServicesClientImpl;
import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.DoExpressCheckoutPaymentRequestType;
import com.paypal.soap.api.DoExpressCheckoutPaymentResponseType;
import com.paypal.soap.api.GetExpressCheckoutDetailsRequestType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseDetailsType;
import com.paypal.soap.api.GetExpressCheckoutDetailsResponseType;
import com.paypal.soap.api.GetTransactionDetailsRequestType;
import com.paypal.soap.api.GetTransactionDetailsResponseType;
import com.paypal.soap.api.RefundTransactionRequestType;
import com.paypal.soap.api.RefundTransactionResponseType;
import com.paypal.soap.api.SetExpressCheckoutRequestType;
import com.paypal.soap.api.SetExpressCheckoutResponseType;

/**
 * @author Nasly / byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "WSClientService.remote")
@LocalBinding(jndiBinding = "WSClientService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class WSClientServiceBean extends PlatformBaseSessionBean implements WSClientBDImpl, WSClientBDLocalImpl {

	private static final long serialVersionUID = 8244781239882651936L;

	private Log log = LogFactory.getLog(WSClientServiceBean.class);

	/**
	 * Requests EBI transaction statuses for transactions of a given PNR.
	 * 
	 * @param transactionStatusEnquiry
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public PNRExtTransactionsTO getEBITransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO) throws ModuleException {
		return new EBIWebervicesImpl().getTransactionStatus(pnrExtTransactionsTO);
	}

	/**
	 * Requests transaction statuses for transactions of a given PNR.
	 * 
	 * @param transactionStatusEnquiry
	 * @return
	 * @throws ModuleException
	 */
	public PNRExtTransactionsTO getPNRTransactionStatus(PNRExtTransactionsTO pnrExtTransactionsTO, String serviceProvider)
			throws ModuleException {
		if (ExternalWebServiceInvoker.SERVICE_PROVIDER_EBI.equals(serviceProvider))
			return new EBIWebervicesImpl().getTransactionStatus(pnrExtTransactionsTO);
		else if (ExternalWebServiceInvoker.SERVICE_PROVIDER_DIB.equals(serviceProvider))
			return new DIBWebServicesImpl().getTransactionStatus(pnrExtTransactionsTO);
		else if (ExternalWebServiceInvoker.SERVICE_PROVIDER_DB.equals(serviceProvider))
			return new DBWebServicesImpl().getTransactionStatus(pnrExtTransactionsTO);
		else if (ExternalWebServiceInvoker.SERVICE_PROVIDER_EP.equals(serviceProvider))
			return new EPWebServicesImpl().getTransactionStatus(pnrExtTransactionsTO);
		else
			return null;

	}

	/**
	 * Send AA transaction statuses for a given transaction period (day), to be reconciled at banks.
	 * 
	 * @param startTimestamp
	 *            Transactions start timestamp
	 * @param endTimestamp
	 *            Transactions end timestamp
	 * @throws ModuleException
	 * 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void requestDailyBankTnxReconcilation(Date startTimestamp, Date endTimestamp, String serviceProvider)
			throws ModuleException {
		new BankWebServicesInvoker().requestDailyTnxReconcilation(startTimestamp, endTimestamp, serviceProvider);
	}

	/**
	 * Publish Above Threshold/Below Threshold inventories to RM.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void publishRMInventoryAlerts() throws ModuleException {
		new RMWebservicesClientImpl().publishAlerts();
	}

	/**
	 * Transfer Reservation
	 * 
	 * @param reservation
	 * @param pnrSegIDs
	 * @param interlinedAirLineTO
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce transferReservation(Reservation reservation, Collection pnrSegIDs,
			InterlinedAirLineTO interlinedAirLineTO) throws ModuleException {
		return new BlockSpaceWebservicesClientImpl().transferReservation(reservation, pnrSegIDs, interlinedAirLineTO);
	}

	/**
	 * Get the inventory allocatyion data.
	 * 
	 * @throws ModuleException
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List getFlightInventoryAllocations(InventoryAllocationTO allocationTO, Map interLineAirlines) throws ModuleException {
		return new InventoryAllocationsClientImpl().getFlightInventoryAllocations(allocationTO, interLineAirlines);
	}

	public String sendNetCastSMSMessage(NetCastRequest request) throws ModuleException {
		log.debug("######## sendNetCastSMSMessage called");
		try {
			return new NetCastWebServiceImpl().sendNetCastSMSMessage(request);
		} catch (ModuleException ex) {
			throw ex;
		} catch (Exception e) {
			log.error("sendNetCastSMSMessage error", e);
			throw new ModuleException("wsclient.sms.general");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CCFraudCheckResponseDTO validateCCPaymentForFraud(CCFraudCheckRequestDTO paymentRQ) throws ModuleException {
		log.debug("validateCCPaymentForFraud called");
		try {
			return new ECWebervicesImpl().validateCCPaymentForFraud(paymentRQ);
		} catch (ModuleException ex) {
			throw ex;
		} catch (Exception e) {
			log.error("validateCCPaymentForFraud error", e);
			throw new ModuleException("wsclient.ec.general");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public InsuranceResponse quoteRAKInsurancePolicy(IInsuranceRequest iQuoteRequest) throws ModuleException {
		log.debug("######## quote RAK InsurancePolicy called");
		try {
			return RakBL.quoteInsurancePolicy(iQuoteRequest);
		} catch (ModuleException ex) {
			throw ex;
		} catch (Throwable e) {
			log.error("quote RAK InsurancePolicy error", e);
			throw new ModuleException("wsclient.aig.general");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public InsuranceResponse reSellRAKInsurancePolicy(IInsuranceRequest reSellReq) throws ModuleException {
		log.debug("######## sellInsurancePolicy called");
		try {
			return RakBL.sellInsurancePolicy(reSellReq);
		} catch (ModuleException ex) {
			throw ex;
		} catch (Exception e) {
			log.error("sellInsurancePolicy error", e);
			throw new ModuleException("wsclient.aig.general");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public InsuranceResponse sellRAKInsurancePolicy(IInsuranceRequest isellReq) throws ModuleException {
		log.debug("######## sellInsurancePolicy called");
		try {
			return RakBL.sellInsurancePolicy(isellReq);
		} catch (ModuleException ex) {
			throw ex;
		} catch (Exception e) {
			log.error("sellInsurancePolicy error", e);
			throw new ModuleException("wsclient.aig.general");
		}
	}

	@Override
	public String[] getInsurancePubStatus(String response) throws ModuleException {
		return RakProxy.getInsurancePubStatus(response);
	}

	@Override
	public String publishRakInsuranceData(RakFlightInsuranceInfoDTO rakFlightInsuranceInfoDTO) throws ModuleException {
		return RakProxy.publishInsuranceData(rakFlightInsuranceInfoDTO);
	}

	public CyberplusResponseDTO cancel(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling cancel CC payment for %s", cyberplusRequestDTO.getTransactionId()));
		try {
			return new CPWebservicesImpl().cancel(cyberplusRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public CyberplusResponseDTO modify(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling modify CC payment for %s", cyberplusRequestDTO.getTransactionId()));
		try {
			CyberplusResponseDTO res = new CPWebservicesImpl().modify(cyberplusRequestDTO);
			return res;
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public CyberplusTransactionInfoDTO getInfo(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling getInfo CC payment for %s", cyberplusRequestDTO.getTransactionId()));
		try {
			CyberplusTransactionInfoDTO res = new CPWebservicesImpl().getInfo(cyberplusRequestDTO);
			return res;
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public CyberplusTransactionInfoDTO refund(CyberplusRequestDTO cyberplusRequestDTO) throws ModuleException {
		log.debug(String.format("calling refund CC payment for %s", cyberplusRequestDTO.getTransactionId()));
		try {
			CyberplusTransactionInfoDTO res = new CPWebservicesImpl().refund(cyberplusRequestDTO);
			return res;
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public CyberplusTransactionInfoDTO create(CyberplusRequestDTO paymentInfo) throws ModuleException {
		log.debug(String.format("calling create CC payment for %s", paymentInfo.getTransactionId()));
		try {
			CyberplusTransactionInfoDTO res = new CPWebservicesImpl().create(paymentInfo);
			return res;
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	/*
	 * PAYPAL Transactions
	 */

	public DoDirectPaymentRequestType prepareDoDirectPaymentRequestType(PAYPALRequestDTO paypalRequestDTO) throws ModuleException {
		try {
			return new PPWebServicesImpl().prepareDoDirectPaymentRequestType(paypalRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public DoDirectPaymentResponseType doDirectPaymentWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			DoDirectPaymentRequestType pprequest) throws ModuleException {
		try {
			return new PPWebServicesImpl().doDirectPaymentWebServiceCall(paypalRequestDTO, pprequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public RefundTransactionRequestType prepareRefund(PAYPALRequestDTO paypalRequestDTO) throws ModuleException {
		try {
			return new PPWebServicesImpl().prepareRefund(paypalRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public RefundTransactionResponseType doRefundWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			RefundTransactionRequestType pprequest) throws ModuleException {
		try {
			return new PPWebServicesImpl().refund(paypalRequestDTO, pprequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public GetTransactionDetailsRequestType prepareDoGetTransactionDetailsCode(PAYPALRequestDTO paypalRequestDTO)
			throws ModuleException {
		try {
			return new PPWebServicesImpl().prepareDoGetTransactionDetailsCode(paypalRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public GetTransactionDetailsResponseType doGetTransactionDetailsCode(GetTransactionDetailsRequestType pprequest,
			PAYPALRequestDTO paypalRequestDTO) throws ModuleException {
		try {
			return new PPWebServicesImpl().doGetTransactionDetailsCode(pprequest, paypalRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public SetExpressCheckoutRequestType prepareSetExpressCheckoutRequestType(PAYPALRequestDTO paypalRequestDTO)
			throws ModuleException {
		try {
			return new PPWebServicesImpl().prepareSetExpressCheckoutRequestType(paypalRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public SetExpressCheckoutResponseType doSetExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			SetExpressCheckoutRequestType pprequest) throws ModuleException {
		try {
			return new PPWebServicesImpl().doSetExpressCheckoutWebServiceCall(paypalRequestDTO, pprequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public GetExpressCheckoutDetailsRequestType prepareGetExpressCheckoutDetailsRequestType(PAYPALRequestDTO paypalRequestDTO)
			throws ModuleException {
		try {
			return new PPWebServicesImpl().prepareGetExpressCheckoutDetailsRequestType(paypalRequestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public GetExpressCheckoutDetailsResponseType doGetExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			GetExpressCheckoutDetailsRequestType pprequest) throws ModuleException {
		try {
			return new PPWebServicesImpl().doGetExpressCheckoutWebServiceCall(paypalRequestDTO, pprequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public DoExpressCheckoutPaymentRequestType prepareDoExpressCheckoutPaymentRequestType(
			GetExpressCheckoutDetailsResponseDetailsType response) throws ModuleException {
		try {
			return new PPWebServicesImpl().prepareDoExpressCheckoutPaymentRequestType(response);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public DoExpressCheckoutPaymentResponseType doDoExpressCheckoutWebServiceCall(PAYPALRequestDTO paypalRequestDTO,
			DoExpressCheckoutPaymentRequestType pprequest) throws ModuleException {
		try {
			return new PPWebServicesImpl().doDoExpressCheckoutWebServiceCall(paypalRequestDTO, pprequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public SamanResponseDTO verifyTransaction(SamanRequestDTO verifyRequest) throws ModuleException {
		try {
			return new SamanWebServicesImpl().verifyTransaction(verifyRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public SamanResponseDTO reverseTransaction(SamanRequestDTO refundRequest) throws ModuleException {
		try {
			return new SamanWebServicesImpl().reverseTransaction(refundRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public boolean publishFlightLoadInformation(List<FlightLoad> flightLoadInformationDTOs) throws ModuleException {
		try {
			return new CargoFlashWebservicesImpl().publishFlightLoadInfo(flightLoadInformationDTOs);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	public String reverseTransactionSRTM(SamanRequestDTO refundRequest) throws ModuleException {
		try {
			return new SamanWebServicesImpl().reverseTransactionSRTM(refundRequest);

		} catch (ModuleException e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public SRTMRefundServiceResponseDTO reverseTransactionRegSRTM(SamanRequestDTO refundRequest, String transRefNum, Long tptID)
			throws ModuleException {
		try {
			return new SamanWebServicesImpl().reverseTransactionRegSRTM(refundRequest, transRefNum, tptID);

		} catch (ModuleException e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public SRTMRefundServiceResponseDTO reverseTransactionExcSRTM(SamanRequestDTO refundRequest, Long refundRegID)
			throws ModuleException {
		try {
			return new SamanWebServicesImpl().reverseTransactionExcSRTM(refundRequest, refundRegID);

		} catch (ModuleException e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}


	public SRTMRefundStatusResponseDTO getSRTMRefundStatus(SamanRequestDTO refundRequest) throws ModuleException {
		try {
			return new SamanWebServicesImpl().getSRTMRefundReportStatus(refundRequest);
		} catch (ModuleException e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public Pay_AT_StoreResponseDTO payAtStorePaymentTransaction(Pay_AT_StoreRequestDTO payRequest) throws ModuleException {
		try {
			return new PayfortWebServicesImpl().payAtStorePaymentTransaction(payRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public Pay_AT_HomeResponseDTO payAtHomePaymentTransaction(Pay_AT_HomeRequestDTO payRequest) throws ModuleException {
		try {
			return new PayfortWebServicesImpl().payAtHomePaymentTransaction(payRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public CheckOrderResponseDTO checkTransaction(CheckOrderDTO checkOrderDTO) throws ModuleException {
		try {
			return new PayfortWebServicesImpl().checkTransaction(checkOrderDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public CheckNotificationResponseDTO checkNotification(CheckNotificationDTO checkNotificationDTO) throws ModuleException {
		try {
			return new PayfortWebServicesImpl().checkNotification(checkNotificationDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public String postRefundRequest(MIGSRequest refundReuest) throws ModuleException {
		return new MigsWebServicesImpl().postRefundRequest(refundReuest);
	}

	public String verifyTransaction(MIGSRequest refundReuest) throws ModuleException {
		return new MigsWebServicesImpl().verifyTransaction(refundReuest);
	}

	@Override
	public boolean publishFlightChangeInformation(List<FlightsToPublish> flightsToPublish) throws ModuleException {
		try {
			return new CargoFlashFlightUpdateWebserviceImpl().updateFlightDetails(flightsToPublish);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TypeBRS processTypeBResMessage(TypeBRQ typebReq) throws ModuleException {
		try {
			return new TypeBResWebServicesClientImpl().processTypeBResMessage(typebReq);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyMemberStateDTO getLoyaltyMemberState(String memberAccountId) throws ModuleException {
		try {
			return  LMSServiceFactory.getLMSMemberWebService().getMemberState(memberAccountId, 0);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String enrollMemberWithEmail(Customer customer) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().enrollMemberWithEmail(customer);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean addMemberAccountId(String WSSecurityToken, String memberAccountId, String memberExternalId,
			String idTypeExternalReference, String memberAccountIdToAdd) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().addMemberAccountId(WSSecurityToken, memberAccountId, memberExternalId,
					idTypeExternalReference, memberAccountIdToAdd);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyMemberCoreDTO getLoyaltyMemberCoreDetails(String memberAccountId) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().getMemberCoreDetails(memberAccountId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public LoyaltyPointDetailsDTO getLoyaltyMemberPointBalances(String memberAccountId,  String memberExternalId) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().getMemberPointBalances(memberAccountId, memberExternalId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean saveMemberCustomFields(Customer customer) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveMemberCustomFields(customer);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean saveMemberEmailAddress(String memberAccountId, String emailAddress) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveMemberEmailAddress(memberAccountId, emailAddress);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveMemberHeadOfHouseHold(memberAccountId, headOfHosueHold);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().removeMemberHeadOfHouseHold(memberAccountId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean saveMemberPhoneNumbers(String memberAccountId, String phoneNumber, String mobileNumber) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveMemberPhoneNumbers(memberAccountId, phoneNumber, mobileNumber);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean saveMemberPreferredCommunicationLanguage(String memberAccountId, String languageCode) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveMemberPreferredCommunicationLanguage(memberAccountId, languageCode);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean saveLoyaltySystemReferredByUser(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveLoyaltySystemReferredByUser(memberAccountId, referredByMemberAccountId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().setMemberPassword(memberAccountId, password);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean sendMemberWelcomeMessage(String memberAccountId) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().sendMemberWelcomeMessage(memberAccountId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getCrossPortalLoginUrl(String memberAccountId, String password, String menuItemIntRef) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().getCrossPortalLoginUrl(memberAccountId, password, menuItemIntRef);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public IssueRewardResponse issueVariableRewards(IssueRewardRequest issueRewardRequest, String pnr, Map<String, Double> productPoints) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSRewardWebService().issueVariableRewards(issueRewardRequest, pnr, productPoints);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean redeemMemberRewards(String locationExtReference, String[] rewardIds, String memberAccountId)
			throws ModuleException {
		try {
			return LMSServiceFactory.getLMSRewardWebService().redeemMemberRewards(locationExtReference, rewardIds, memberAccountId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean cancelMemberRewards(String pnr, String[] rewardIds, String memberAccountId) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSRewardWebService().cancelMemberRewards(pnr, rewardIds, memberAccountId);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	public BehpardakhtResponse verifyRequest(BehpardakhtVerifyRequest verifyRequest) throws ModuleException {
		try {
			return new BehpardakhtWebServiceImpl().verifyRequest(verifyRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public BehpardakhtResponse payRequest(BehpardakhtPayRequest payRequest) throws ModuleException {
		try {
			return new BehpardakhtWebServiceImpl().payRequest(payRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public BehpardakhtResponse settleRequest(BehpardakhtSettleRequest settleRequest) throws ModuleException {
		try {
			return new BehpardakhtWebServiceImpl().settleRequest(settleRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public BehpardakhtResponse inquiryRequest(BehpardakhtInquiryRequest inquiryRequest) throws ModuleException {
		try {
			return new BehpardakhtWebServiceImpl().inquiryRequest(inquiryRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public BehpardakhtResponse reversalRequest(BehpardakhtReversalRequest reversalRequest) throws ModuleException {
		try {
			return new BehpardakhtWebServiceImpl().reversalRequest(reversalRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	public BehpardakhtResponse refundRequest(BehpardakhtRefundRequest refundRequest) throws ModuleException {
		try {
			return new BehpardakhtWebServiceImpl().refundRequest(refundRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public int saveMemberName(Customer customer) throws ModuleException {
		try {
			return LMSServiceFactory.getLMSMemberWebService().saveMemberName(customer);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}
	@Override
	public AmeriaBankCommonResponse getAmeriaBankPaymentId(AmeriaBankPaymentIdRequest ameriaBankPaymentIdRequest)
			throws ModuleException {
		try {
			return new AmeriaWebServiceImpl().getPaymentId(ameriaBankPaymentIdRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public AmeriaBankPaymentFieldsResponse getAmeriaBankPaymentFields(AmeriaBankCommonRequest ameriaBankPaymentFieldsRequest)
			throws ModuleException {
		try {
			return new AmeriaWebServiceImpl().getPaymentFields(ameriaBankPaymentFieldsRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public AmeriaBankCommonResponse refundAmeriaBankPayment(AmeriaBankCommonRequest refundAmeriaBankPaymentRequest)
			throws ModuleException {
		try {
			return new AmeriaWebServiceImpl().refundAmeriaBankPayment(refundAmeriaBankPaymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public AmeriaBankCommonResponse reverseAmeriaBankPayment(AmeriaBankCommonRequest refundAmeriaBankPaymentRequest)
			throws ModuleException {
		try {
			return new AmeriaWebServiceImpl().reverseAmeriaBankPayment(refundAmeriaBankPaymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}
	
	@Override
	public ParsianPaymentResponseDTO pinPaymentEnquiry(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException {
	
		try {
			return new ParsianWebServicesImpl().paymentEnquiry(pinPaymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public ParsianPaymentResponseDTO pinVoidPayment(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException {
		try {
			return new ParsianWebServicesImpl().pinVoidPayment(pinPaymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public ParsianPaymentResponseDTO pinReversal(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException {
		try {
			return new ParsianWebServicesImpl().pinReversal(pinPaymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public ParsianRefundResponseDTO doRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException {
		try {
			return new ParsianWebServicesImpl().doRefund(requestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public ParsianRefundResponseDTO approveRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException {
		try {
			return new ParsianWebServicesImpl().approveRefund(requestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public ParsianRefundResponseDTO cancleSuspendedRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException {
		try {
			return new ParsianWebServicesImpl().cancelRefund(requestDTO);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public ParsianPaymentResponseDTO pinPaymentRequest(ParsianPaymentRequestDTO pinPaymentRequest) throws ModuleException {

		try {
			return new ParsianWebServicesImpl().pinPaymentRequest(pinPaymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public TapPaymentRequestCallRs paymentRequest(TapPaymentRequestCallRq paymentRequest) throws ModuleException {
		try {
			return new TapGoSellWebServiceImpl().paymentRequest(paymentRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public TapCaptureRequestCallRs capturePayment(TapCaptureRequestCallRq captureRequest) throws ModuleException {
		try {
			return new TapGoSellWebServiceImpl().capturePayment(captureRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public TapGetOrderStatusRequestCallRs getOrderStatus(TapGetOrderStatusRequestCallRq getOrderRequest) throws ModuleException {
		try {
			return new TapGoSellWebServiceImpl().getOrderStatus(getOrderRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public TapRefundCallRs refund(TapRefundCallRq refundRequest) throws ModuleException {
		try {
			return new TapGoSellWebServiceImpl().refund(refundRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public TapRefundStatusCallRs getRefundStatus(TapRefundStatusCallRq refundStatusRequest) throws ModuleException {
		try {
			return new TapGoSellWebServiceImpl().getRefundStatus(refundStatusRequest);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException("wsclient.serviceinvocation.failed");
			}
		}
	}


	@Override
	public MemberRemoteLoginResponseDTO memberLogin(String userName, String password) throws ModuleException {

		try {
			return LMSServiceFactory.getLMSMemberWebService().memberLogin(userName,password);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public void updateMember(Customer customer) throws ModuleException {
		try {
			LMSServiceFactory.getLMSMemberWebService().updateMember(customer);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}

	@Override
	public String generateS3SignedUrl(String fileName, String mimeType) throws ModuleException {
		try {
			return new LMSCommonServiceGravtyImpl().generateS3SignedUrl(fileName, mimeType);
		} catch (Exception e) {
			if (e instanceof ModuleException) {
				throw (ModuleException) e;
			} else {
				throw new ModuleException(e, "wsclient.serviceinvocation.failed");
			}
		}
	}
}
