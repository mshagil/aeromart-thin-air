package com.isa.thinair.wsclient.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

public class TuneClientConfig extends DefaultModuleConfig {

	private String userName;
	private String pswd;
	private String url;
	private String method;
	private String connectionTimeOutInSeconds;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getConnectionTimeOutInSeconds() {
		return connectionTimeOutInSeconds;
	}

	public void setConnectionTimeOutInSeconds(String connectionTimeOutInSeconds) {
		this.connectionTimeOutInSeconds = connectionTimeOutInSeconds;
	}

}
