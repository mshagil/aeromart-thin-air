package com.isa.thinair.wsclient.core.util;

import java.io.OutputStream;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SOAPLoggingHandler implements SOAPHandler<SOAPMessageContext> {

	private static Log log = LogFactory.getLog(SOAPLoggingHandler.class);

	public SOAPLoggingHandler() {
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	@Override
	public void close(MessageContext mtcx) {
	}

	@Override
	public boolean handleFault(SOAPMessageContext mtcx) {

		logSOAPMessage(mtcx);
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext mtcx) {

		logSOAPMessage(mtcx);
		return true;
	}

	private void logSOAPMessage(SOAPMessageContext mtcx) {
		Boolean outboundProperty = (Boolean) mtcx.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue()) {
			log.debug("\nOutbound message:");
		} else {
			log.debug("\nInbound message:");
		}

		SOAPMessage message = mtcx.getMessage();
		StringOutputStream outputStream = new StringOutputStream();
		try {
			message.writeTo(outputStream);
			log.debug("\n" + outputStream.toString() + "\n");
		} catch (Exception e) {
			System.out.println("Exception in handler: " + e);
		}
	}

	class StringOutputStream extends OutputStream {

		private StringBuilder string = new StringBuilder();

		@Override
		public void write(int b) {
			this.string.append((char) b);
		}

		public String toString() {
			return this.string.toString();
		}
	}

}
