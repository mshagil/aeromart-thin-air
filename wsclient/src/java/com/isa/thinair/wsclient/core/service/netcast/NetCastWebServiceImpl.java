package com.isa.thinair.wsclient.core.service.netcast;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.api.dto.netcast.NetCastRequest;
import com.isa.thinair.wsclient.core.client.ExternalWebServiceInvoker;
import com.isa.thinair.wsclient.core.client.Param;
import com.isa.thinair.wsclient.core.service.DefaultWebserviceClient;

public class NetCastWebServiceImpl implements DefaultWebserviceClient {

	private static String SERVICE_PROVIDER = ExternalWebServiceInvoker.SERVICE_PROVIDER_NETCAST;

	@Override
	public String getServiceProvider(String carrierCode) {
		return SERVICE_PROVIDER;
	}

	public String sendNetCastSMSMessage(NetCastRequest request) throws ModuleException {
		String resultCode = (String) ExternalWebServiceInvoker.invokeService(getServiceProvider(null), "sENDSMS", true,
				new Param(request.getMobile()), new Param(request.getMessage()), new Param(request.getNetCastId()), new Param(
						request.getMask()));
		return resultCode;
	}
}
