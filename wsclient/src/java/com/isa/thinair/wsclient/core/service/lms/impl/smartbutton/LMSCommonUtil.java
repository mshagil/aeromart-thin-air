package com.isa.thinair.wsclient.core.service.lms.impl.smartbutton;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.wsclient.api.dto.lms.RewardData;
import com.isa.thinair.wsclient.api.lms.customfield.RecCustomFieldKeyValuePair;
import com.isa.thinair.wsclient.api.lms.customfield.ReqSaveMemberCustomFieldCollection;
import com.isa.thinair.wsclient.api.lms.member.FetchMemberCoreReturn;
import com.isa.thinair.wsclient.api.lms.member.FetchMemberStateReturn;
import com.isa.thinair.wsclient.api.lms.memberactivity.FetchMemberPointBalancesReturn;
import com.isa.thinair.wsclient.api.lms.referrer.ReqProcessReferredBy;
import com.isa.thinair.wsclient.api.lms.reward.ReqIssueVariableReward;
import com.isa.thinair.wsclient.api.lms.reward.ReqValidateVariableRewardAmount;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isa.thinair.wsclient.api.util.LMSConstants.LocationExternalReference;
import com.isa.thinair.wsclient.api.util.LMSConstants.MemberTitle;
import com.isa.thinair.wsclient.api.util.LMSConstants.MenuExternalReference;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;

/**
 * Utility class related to LMS API integration
 * 
 * @author rumesh
 * 
 */
public final class LMSCommonUtil {
	
	private static Log log = LogFactory.getLog(LMSCommonUtil.class);	

	public static ReqSaveMemberCustomFieldCollection populateMemberCustomFieldCollection(Customer customer) {

		Map<String, String> customFields = generateCustomFieldReq(customer);
		ReqSaveMemberCustomFieldCollection reqObj = null;
		if (customFields != null && !customFields.isEmpty()) {
			reqObj = new ReqSaveMemberCustomFieldCollection();
			reqObj.setSecurityToken(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWebServiceToken());
			reqObj.setCardNumber(customer.getCustomerLmsDetails().iterator().next().getFfid());

			RecCustomFieldKeyValuePair[] customFieldPairs = new RecCustomFieldKeyValuePair[customFields.size()];
			int counter = 0;
			for (Entry<String, String> customFieldEntry : customFields.entrySet()) {
				String fieldReference = customFieldEntry.getKey();
				String fieldValue = customFieldEntry.getValue();

				RecCustomFieldKeyValuePair keyValueObj = new RecCustomFieldKeyValuePair();
				keyValueObj.setCustomFieldExternalReference(fieldReference);
				keyValueObj.setCustomFieldValue(fieldValue);

				customFieldPairs[counter++] = keyValueObj;
			}

			reqObj.setCustomFieldPairs(customFieldPairs);

		}

		return reqObj;
	}

	public static int getLoyaltyMemberTitleId(String memberTitle) {
		int titleId = LMSConstants.MemberTitle.MR.getTitleId();

		MemberTitle titleEnumObj = LMSConstants.MemberTitle.valueOf(memberTitle);

		if (titleEnumObj != null) {
			titleId = titleEnumObj.getTitleId();
		}

		return titleId;
	}

	public static LoyaltyMemberStateDTO populateLoyaltyMemberStateDTO(String memberAccountId, FetchMemberStateReturn wsResponse) {
		LoyaltyMemberStateDTO loyaltyMemberStateDTO = new LoyaltyMemberStateDTO();

		if (wsResponse != null) {
			loyaltyMemberStateDTO.setMemberAccountId(memberAccountId);
			loyaltyMemberStateDTO.setReturnCode(wsResponse.getReturnCode());
			loyaltyMemberStateDTO
					.setLoyaltyMember(wsResponse.getMemberIsLoyaltyMember() == LMSConstants.MemberState.IS_LOYALTY_MEMBER);
			loyaltyMemberStateDTO
					.setMemberSuspended(wsResponse.getMemberIsSuspended() == LMSConstants.MemberState.MEMBER_SUSPENDED);
			loyaltyMemberStateDTO.setReturnCode(wsResponse.getReturnCode());
		}

		return loyaltyMemberStateDTO;
	}

	public static LoyaltyMemberCoreDTO populateLoyaltyMemberCoreDTO(String memberAccountId, FetchMemberCoreReturn wsResponse) {
		LoyaltyMemberCoreDTO loyaltyMemberCoreDTO = null;

		if (wsResponse != null) {
			loyaltyMemberCoreDTO = new LoyaltyMemberCoreDTO();
			loyaltyMemberCoreDTO.setMemberAccountId(memberAccountId);
			loyaltyMemberCoreDTO.setMemberFirstName(wsResponse.getMemberFirstName());
			loyaltyMemberCoreDTO.setMemberLastName(wsResponse.getMemberLastName());
			loyaltyMemberCoreDTO.setMemberGenderInternalId(wsResponse.getMemberGenderInternalId());
			loyaltyMemberCoreDTO.setMemberGenderString(wsResponse.getMemberGenderString());
			loyaltyMemberCoreDTO.setMemberBirthdateDay(wsResponse.getMemberBirthdateDay());
			loyaltyMemberCoreDTO.setMemberBirthdateMonth(wsResponse.getMemberBirthdateMonth());
			loyaltyMemberCoreDTO.setMemberBirthdateYear(wsResponse.getMemberBirthdateYear());
			loyaltyMemberCoreDTO.setMemberNamePrefixInternalId(wsResponse.getMemberNamePrefixInternalId());
			loyaltyMemberCoreDTO.setMemberNamePrefixString(wsResponse.getMemberNamePrefixString());
			loyaltyMemberCoreDTO.setSBInternalMemberId(wsResponse.getSBInternalMemberId());
			loyaltyMemberCoreDTO.setMemberExternalId(String.valueOf(wsResponse.getSBInternalMemberId()));
			loyaltyMemberCoreDTO.setReturnCode(wsResponse.getReturnCode());
			loyaltyMemberCoreDTO.setEligibleForFamilyHead(true);
		}

		return loyaltyMemberCoreDTO;
	}

	public static LoyaltyPointDetailsDTO populateLoyaltyPointDetailsDTO(String memberAccountId,
			FetchMemberPointBalancesReturn wsResponse) {
		LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = null;

		if (wsResponse != null) {
			loyaltyPointDetailsDTO = new LoyaltyPointDetailsDTO();
			loyaltyPointDetailsDTO.setMemberAccountId(memberAccountId);
			loyaltyPointDetailsDTO.setMemberPointsAvailable(getRoundedPointsValue(wsResponse.getMemberPointsAvailable()));
			loyaltyPointDetailsDTO.setMemberPointsDeducted(getRoundedPointsValue(wsResponse.getMemberPointsDeducted()));
			loyaltyPointDetailsDTO.setMemberPointsEarned(getRoundedPointsValue(wsResponse.getMemberPointsEarned()));
			loyaltyPointDetailsDTO.setMemberPointsExpired(getRoundedPointsValue(wsResponse.getMemberPointsExpired()));
			loyaltyPointDetailsDTO.setMemberPointsLocked(getRoundedPointsValue(wsResponse.getMemberPointsLocked()));
			loyaltyPointDetailsDTO.setMemberPointsUsed(getRoundedPointsValue(wsResponse.getMemberPointsUsed()));
			loyaltyPointDetailsDTO.setReturnCode(wsResponse.getReturnCode());
		}

		return loyaltyPointDetailsDTO;
	}

	public static ReqIssueVariableReward populateReqIssueVariableReward(RewardData rewardData) {
		ReqIssueVariableReward lmsReqObj = new ReqIssueVariableReward();
		lmsReqObj.setSecurityToken(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWebServiceToken());
		lmsReqObj.setRewardTypeExternalReference(rewardData.getRewardTypeExtReference());
		lmsReqObj.setDollarAmountToRedeem(rewardData.getRewardAmount().doubleValue());
		return lmsReqObj;
	}

	public static ReqValidateVariableRewardAmount populateReqValidateVariableRewardAmount(RewardData rewardData) {
		ReqValidateVariableRewardAmount lmsReqObj = new ReqValidateVariableRewardAmount();
		lmsReqObj.setCardNumber(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWebServiceToken());
		lmsReqObj.setRewardTypeExternalReference(rewardData.getRewardTypeExtReference());
		lmsReqObj.setDollarAmountToRedeem(rewardData.getRewardAmount().doubleValue());
		return lmsReqObj;
	}

	public static int[] converttoReponseRewardIds(Set<Integer> rewardIdSet) {

		int[] rewardIdArr = new int[rewardIdSet.size()];
		int counter = 0;
		for (Integer rewardId : rewardIdSet) {
			rewardIdArr[counter++] = rewardId;
		}

		return rewardIdArr;
	}

	public static String getLocationExternalReference(AppIndicatorEnum appIndicatorEnum) {
		String locationExtReference = null;

		for (LocationExternalReference locationEnum : LocationExternalReference.values()) {
			if (locationEnum.getAppIndicator() == appIndicatorEnum.getCode()) {
				return locationEnum.getLocationReference();
			}
		}

		return locationExtReference;
	}

	public static ReqProcessReferredBy populateReqProcessReferredBy(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException {
		ReqProcessReferredBy reqProcessReferredBy = new ReqProcessReferredBy();
		reqProcessReferredBy.setSecurityToken(LMSSmartButtonWebServiceInvoker.lmsClientConfig.getWebServiceToken());
		reqProcessReferredBy.setReferreeAccountId(memberAccountId);
		reqProcessReferredBy.setReferredByAccountId(referredByMemberAccountId);
		return reqProcessReferredBy;
	}

	public static String getMenuItemExternalReference(String menuInternalReference) {
		String menuItemExtReference = null;

		for (MenuExternalReference menuItemEnum : MenuExternalReference.values()) {
			if (menuItemEnum.getInternalReference().equals(menuItemExtReference)) {
				menuItemExtReference = menuItemEnum.getExternalReference();
				break;
			}
		}

		return menuInternalReference;
	}

	private static Map<String, String> generateCustomFieldReq(Customer customer) {
		Map<String, String> customFields = new HashMap<String, String>();

		
		LmsMember lmsMember = customer.getLMSMemberDetails();

		if (lmsMember != null) {
			Country country = null;
			if (!BeanUtils.nullHandler(customer.getCountryCode()).equals(lmsMember.getResidencyCode())) {
				try {
					country = WSClientModuleUtils.getCommonServiceBD().getCountryByName(customer.getCountryCode());
				} catch (Exception e) {
					log.error("LmsMemberConfirmationAction==>", e);
				}
			} else {
				try {
					country = WSClientModuleUtils.getCommonServiceBD().getCountryByName(lmsMember.getResidencyCode());
				} catch (Exception e) {
					log.error("LmsMemberConfirmationAction==>", e);
				}
			}

			customFields.put(LMSConstants.MemberCustomField.RESIDENCY_CODE, (country == null) ? "" : country.getIcaoCode());
		//	customFields.put(LMSConstants.MemberCustomField.HEAD_OF_HOUSE, BeanUtils.nullHandler(lmsMember.getHeadOFEmailId()));

			Nationality nationality = null;
			try {

				Integer nat = Integer.parseInt(lmsMember.getNationalityCode());
				nationality = WSClientModuleUtils.getCommonServiceBD().getNationality(nat);
			} catch (Exception e) {
				log.error("LmsMemberConfirmationAction==>", e);
			}
			customFields.put(LMSConstants.MemberCustomField.NATIONALITY_CODE,
					(nationality == null) ? "" : nationality.getIcaoCode());
		}
		

		return customFields;
	}

	private static double getRoundedPointsValue(double originalPointsValue) {
		BigDecimal val = new BigDecimal(originalPointsValue);
		val = val.setScale(0, RoundingMode.DOWN);
		return val.doubleValue();
	}

	public static LoyaltyMemberCoreDTO populateLoyaltyMemberCoreDTO(String memberAccountId, MemberFetchDataResponse memberData) {
		LoyaltyMemberCoreDTO loyaltyMemberCoreDTO = new LoyaltyMemberCoreDTO();
		  loyaltyMemberCoreDTO.setReturnCode(LMSConstants.WSReponse.MEMBER_NOT_FOUND);
		if (memberData != null) {
			loyaltyMemberCoreDTO = new LoyaltyMemberCoreDTO();
			loyaltyMemberCoreDTO.setMemberAccountId(memberAccountId);
			loyaltyMemberCoreDTO.setMemberFirstName(memberData.getUser().getFirstName());
			loyaltyMemberCoreDTO.setMemberLastName(memberData.getUser().getLastName());
			loyaltyMemberCoreDTO.setMemberNamePrefixInternalId(memberData.getId());
			loyaltyMemberCoreDTO.setMemberNamePrefixString(memberData.getSalutation());
			loyaltyMemberCoreDTO.setMemberExternalId(memberData.getMember_id());
			loyaltyMemberCoreDTO.setEligibleForFamilyHead(StringUtils.isEmpty(memberData.getFamily_head()));
			
			if (memberData.getDate_of_birth() != null) {

				String[] dateOfBirthArr = memberData.getDate_of_birth().split("-");
				loyaltyMemberCoreDTO.setMemberBirthdateDay(Integer.parseInt(dateOfBirthArr[2]));
				loyaltyMemberCoreDTO.setMemberBirthdateMonth(Integer.parseInt(dateOfBirthArr[1]));
				loyaltyMemberCoreDTO.setMemberBirthdateYear(Integer.parseInt(dateOfBirthArr[0]));
			}
			 loyaltyMemberCoreDTO.setReturnCode(LMSConstants.WSReponse.RESPONSE_CODE_OK);
		}
		return loyaltyMemberCoreDTO;
	}
	
	public static LoyaltyPointDetailsDTO populateLoyaltyPointDetailsDTO(String memberAccountId, MemberFetchDataResponse memberData) {

		LoyaltyPointDetailsDTO loyaltyPointDetailsDTO = new LoyaltyPointDetailsDTO();
		double pointBalance = 0;

		if (memberData != null && !memberData.getBalances().isEmpty()) {
			pointBalance = AccelAeroCalculator.parseBigDecimal(memberData.getBalances().get(0).getBalance()).doubleValue();
		}

		loyaltyPointDetailsDTO.setMemberAccountId(memberAccountId);
		loyaltyPointDetailsDTO.setMemberPointsAvailable(pointBalance);

		return loyaltyPointDetailsDTO;
	}
}
