package com.isa.thinair.wsclient.core.config;

import java.util.Map;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Byorn
 * 
 * @isa.module.config-bean
 */
public class AIGClientConfig extends DefaultModuleConfig {

	private String messageType;
	private String version;
	private String gdsCode;
	private String transactionTypeQuote;
	private String transactionTypeSell;
	private String iataCountryCode;
	private String agencyPCC;
	private String agencyCode;
	private String hoursToAddForExpirationDate;
	private String gdsProductCode;
	private String passengerDefaultBirthday;
	private String insuredPolicyHolderFlag;
	private String insuredFlag;
	private String planCodeOneWay;
	private String planCodeReturn;
	private String benefitCode;
	private String url;
	private String method;
	private String connectionTimeOutInSeconds;
	private String retryFlag;
	private Map aigCountryCodes;

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getGdsCode() {
		return gdsCode;
	}

	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	public String getTransactionTypeQuote() {
		return transactionTypeQuote;
	}

	public void setTransactionTypeQuote(String transactionTypeQuote) {
		this.transactionTypeQuote = transactionTypeQuote;
	}

	public String getTransactionTypeSell() {
		return transactionTypeSell;
	}

	public void setTransactionTypeSell(String transactionTypeSell) {
		this.transactionTypeSell = transactionTypeSell;
	}

	public String getIataCountryCode() {
		return iataCountryCode;
	}

	public void setIataCountryCode(String iataCountryCode) {
		this.iataCountryCode = iataCountryCode;
	}

	public String getAgencyPCC() {
		return agencyPCC;
	}

	public void setAgencyPCC(String agencyPCC) {
		this.agencyPCC = agencyPCC;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getHoursToAddForExpirationDate() {
		return hoursToAddForExpirationDate;
	}

	public void setHoursToAddForExpirationDate(String hoursToAddForExpirationDate) {
		this.hoursToAddForExpirationDate = hoursToAddForExpirationDate;
	}

	public String getGdsProductCode() {
		return gdsProductCode;
	}

	public void setGdsProductCode(String gdsProductCode) {
		this.gdsProductCode = gdsProductCode;
	}

	public String getPassengerDefaultBirthday() {
		return passengerDefaultBirthday;
	}

	public void setPassengerDefaultBirthday(String passengerDefaultBirthday) {
		this.passengerDefaultBirthday = passengerDefaultBirthday;
	}

	public String getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}

	public String getUrl() {
		return url;
	}

	public String getMethod() {
		return method;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getInsuredPolicyHolderFlag() {
		return insuredPolicyHolderFlag;
	}

	public void setInsuredPolicyHolderFlag(String insuredPolicyHolderFlag) {
		this.insuredPolicyHolderFlag = insuredPolicyHolderFlag;
	}

	public String getInsuredFlag() {
		return insuredFlag;
	}

	public void setInsuredFlag(String insuredFlag) {
		this.insuredFlag = insuredFlag;
	}

	public String getPlanCodeOneWay() {
		return planCodeOneWay;
	}

	public void setPlanCodeOneWay(String planCodeOneWay) {
		this.planCodeOneWay = planCodeOneWay;
	}

	public String getPlanCodeReturn() {
		return planCodeReturn;
	}

	public void setPlanCodeReturn(String planCodeReturn) {
		this.planCodeReturn = planCodeReturn;
	}

	public String getConnectionTimeOutInSeconds() {
		return connectionTimeOutInSeconds;
	}

	public void setConnectionTimeOutInSeconds(String connectionTimeOutInSeconds) {
		this.connectionTimeOutInSeconds = connectionTimeOutInSeconds;
	}

	/**
	 * @return the retryFlag
	 */
	public String getRetryFlag() {
		return retryFlag;
	}

	/**
	 * @param retryFlag
	 *            the retryFlag to set
	 */
	public void setRetryFlag(String retryFlag) {
		this.retryFlag = retryFlag;
	}

	/**
	 * @return the aigCountryCodes
	 */
	public Map getAigCountryCodes() {

		return aigCountryCodes;
	}

	/**
	 * @param aigCountryCodes
	 *            the aigCountryCodes to set
	 */
	public void setAigCountryCodes(Map aigCountryCodes) {
		this.aigCountryCodes = aigCountryCodes;
	}

}
