package com.isa.thinair.wsclient.core.service.parsian;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundResponseDTO;
import com.isa.thinair.wsclient.api.parsian.EShopService;
import com.isa.thinair.wsclient.api.parsian.EShopServiceSoap;
import com.isa.thinair.wsclient.api.parsian.refund.ArrayOfRefund;
import com.isa.thinair.wsclient.api.parsian.refund.PecRefundService;
import com.isa.thinair.wsclient.api.parsian.refund.PecRefundServiceSoap;
import com.isa.thinair.wsclient.api.parsian.refund.Refund;
import com.isa.thinair.wsclient.api.parsian.refund.RefundInfo;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.ParsianClientConfig;

public class ParsianWebServicesImpl {

	private static Log log = LogFactory.getLog(ParsianWebServicesImpl.class);

	ParsianClientConfig parsianClientConfig = WSClientModuleUtils.getModuleConfig().getParsianClientConfig();

	public ParsianPaymentResponseDTO pinPaymentRequest(ParsianPaymentRequestDTO pinPaymentRequestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::pinPayment()] Begin");

		ParsianPaymentResponseDTO response = new ParsianPaymentResponseDTO();
		try {
			EShopServiceSoap serviceStub = getClientStub();
			Holder<Long> authority = new Holder<Long>();
			Holder<Short> status = new Holder<Short>();
			// This is because the call will get failed sometimes when the values are not initialized.
			authority.value = (long) 0;
			status.value = (short) 0;
			serviceStub.pinPaymentRequestWithExtra(pinPaymentRequestDTO.getPin(),
					BigDecimal.valueOf(pinPaymentRequestDTO.getAmount()), pinPaymentRequestDTO.getOrderId(),
					pinPaymentRequestDTO.getCallbackUrl(), pinPaymentRequestDTO.getAdditionalData(), authority, status);
			response.setAuthority(authority.value);
			response.setStatus(status.value);

		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}

		log.debug("[ParsianWebServicesImpl::pinPayment()] End");
		return response;
	}

	public ParsianPaymentResponseDTO paymentEnquiry(ParsianPaymentRequestDTO parsianPaymentRequestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::pinPaymentEnquiry()] Begin");

		ParsianPaymentResponseDTO response = new ParsianPaymentResponseDTO();
		try {
			EShopServiceSoap serviceStub = getClientStub();
			Holder<Short> status = new Holder<Short>();
			Holder<Long> invoiceNumber = new Holder<Long>();
			invoiceNumber.value = (long) 0;
			status.value = (short) 0;
			serviceStub.paymentEnquiry(parsianPaymentRequestDTO.getPin(), parsianPaymentRequestDTO.getAuthority(), status,
					invoiceNumber);
			response.setStatus(status.value);
			response.setInvoiceNumber(invoiceNumber.value);
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}

		log.debug("[ParsianWebServicesImpl::pinPaymentEnquiry()] End");
		return response;
	}

	public ParsianPaymentResponseDTO pinVoidPayment(ParsianPaymentRequestDTO parsianPaymentRequestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::pinVoidPayment()] Begin");

		ParsianPaymentResponseDTO response = new ParsianPaymentResponseDTO();
		try {
			EShopServiceSoap serviceStub = getClientStub();
			Holder<Short> status = new Holder<Short>();
			status.value = (short) 0;
			serviceStub.pinVoidPayment(parsianPaymentRequestDTO.getPin(), parsianPaymentRequestDTO.getOrderId(),
					parsianPaymentRequestDTO.getOrderToVoid(), status);
			response.setStatus(status.value);
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}

		log.debug("[ParsianWebServicesImpl::pinVoidPayment()] End");
		return response;

	}

	public ParsianPaymentResponseDTO pinReversal(ParsianPaymentRequestDTO parsianPaymentRequestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::pinReversal()] Begin");

		ParsianPaymentResponseDTO response = new ParsianPaymentResponseDTO();
		try {
			EShopServiceSoap serviceStub = getClientStub();
			Holder<Short> status = new Holder<Short>();
			status.value = (short) 0;
			serviceStub.pinVoidPayment(parsianPaymentRequestDTO.getPin(), parsianPaymentRequestDTO.getOrderId(),
					parsianPaymentRequestDTO.getOrderToReversal(), status);
			response.setStatus(status.value);
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}

		log.debug("[ParsianWebServicesImpl::pinReversal()] End");
		return response;

	}

	public ParsianRefundResponseDTO doRefund(ParsianRefundRequestDTO requestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::doRefund()] Begin");
		ParsianRefundResponseDTO response = new ParsianRefundResponseDTO();
		try {
			RefundInfo refundInfo = null;
			PecRefundServiceSoap refundService = getPecRefundClientStub();
			refundInfo = refundService.doRefund(requestDTO.getReceiptNo(), requestDTO.getRefundId(), new Long(0),
					requestDTO.getAmount(), requestDTO.getRefundUserName(), requestDTO.getPassword());
			if (refundInfo != null && refundInfo.getRefunds().getRefund() != null) {
				prepareRefundResponse(refundInfo, response);
			} else {
				response.setHasPermission(false);
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}
		log.debug("[ParsianWebServicesImpl::doRefund()] Ends");
		return response;
	}

	public ParsianRefundResponseDTO approveRefund(ParsianRefundRequestDTO requestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::approveRefund()] Begin");
		ParsianRefundResponseDTO response = new ParsianRefundResponseDTO();
		try {
			RefundInfo refundInfo = null;
			PecRefundServiceSoap refundService = getPecRefundClientStub();
			refundInfo = refundService.approveRefund(requestDTO.getRefundId(), requestDTO.getReceiptNo(),
					requestDTO.getRefundUserName(), requestDTO.getPassword());
			if (refundInfo != null && refundInfo.getRefunds().getRefund() != null) {
				prepareRefundResponse(refundInfo, response);
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}
		log.debug("[ParsianWebServicesImpl::approveRefund()] Ends");
		return response;
	}

	public ParsianRefundResponseDTO cancelRefund(ParsianRefundRequestDTO requestDTO)
			throws com.isa.thinair.commons.api.exception.ModuleException {

		log.debug("[ParsianWebServicesImpl::cancelRefund()] Begin");
		ParsianRefundResponseDTO response = new ParsianRefundResponseDTO();
		try {
			RefundInfo refundInfo = null;
			PecRefundServiceSoap refundService = getPecRefundClientStub();
			refundInfo = refundService.cancleRefund(requestDTO.getRefundId(), requestDTO.getReceiptNo(),
					requestDTO.getRefundUserName(), requestDTO.getPassword());
			if (refundInfo != null && refundInfo.getRefunds().getRefund() != null) {
				prepareRefundResponse(refundInfo, response);
			}
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		} finally {
			removeProxy();
		}

		return response;
	}

	private EShopServiceSoap getClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {
			setUpProxyAndSSL();
			URL wsdlURL = new URL(parsianClientConfig.getWsdlUrl());
			QName qname = new QName("http://tempuri.org/", "EShopService");
			return new EShopService(wsdlURL, qname).getEShopServiceSoap();
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		}
	}

	private PecRefundServiceSoap getPecRefundClientStub() throws com.isa.thinair.commons.api.exception.ModuleException {
		try {
			setUpProxyAndSSL();
			URL wsdlURL = new URL(parsianClientConfig.getRefundWsdlUrl());
			QName qname = new QName("http://tempuri.org/", "PecRefundService");
			return new PecRefundService(wsdlURL, qname).getPecRefundServiceSoap();
		} catch (Exception ex) {
			log.error(ex);
			throw new ModuleException(ex, "wsclient.serviceinvocation.failed");
		}
	}

	private void removeProxy() {
		if (parsianClientConfig.isUseProxy()) {
			setProxy("", "");
		}
		if (parsianClientConfig.isUseSSL()) {
			setSSL("", "");
		}
	}

	private void setUpProxyAndSSL() {
		if (parsianClientConfig.isUseProxy()) {
			setProxy(parsianClientConfig.getHttpProxy(), parsianClientConfig.getHttpProxyPort());
		}
		if (parsianClientConfig.isUseSSL()) {
			setSSL(parsianClientConfig.getTrustStoreFile(), parsianClientConfig.getKeyStoreFile());
		}
	}

	private void setProxy(String host, String port) {
		System.setProperty("https.proxyHost", host);
		System.setProperty("https.proxyPort", port);
	}

	private void setSSL(String trustStore, String keyStore) {
		System.setProperty("javax.net.ssl.keyStore", keyStore);
		System.setProperty("javax.net.ssl.trustStore", trustStore);
	}

	private void prepareRefundResponse(RefundInfo refundInfo, ParsianRefundResponseDTO response) {

		ArrayOfRefund refundsArray = refundInfo.getRefunds();
		List<Refund> refunds = refundsArray.getRefund();
		if(refunds != null && !refunds.isEmpty()){
			Refund currentRefund = refunds.get(refunds.size()-1);
			response.setRefundStatusId(currentRefund.getRefundStatusId());
			response.setSuccess(currentRefund.isResult() == null ? false : currentRefund.isResult());
			response.setTransactionStatus(refundInfo.getTransaction().getStatus());
			response.setMessage((String) refundInfo.getMessage());
			response.setHasPermission(refundInfo.isHasPermission());
		}else{
			response.setSuccess(false);
		}
	}
}
