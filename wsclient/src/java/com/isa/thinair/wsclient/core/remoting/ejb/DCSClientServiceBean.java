package com.isa.thinair.wsclient.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSADLInfoRS;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRQ;
import com.isa.connectivity.profiles.dcs.v1.exposed.dto.DCSPNLInfoRS;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.wsclient.core.bl.PNLADLPublisherBL;
import com.isa.thinair.wsclient.core.service.bd.DCSClientBDLocal;
import com.isa.thinair.wsclient.core.service.bd.DCSClientBDRemote;

@Stateless
@RemoteBinding(jndiBinding = "DCSClientService.remote")
@LocalBinding(jndiBinding = "DCSClientService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Clustered
public class DCSClientServiceBean extends PlatformBaseSessionBean implements DCSClientBDRemote, DCSClientBDLocal {

	private Log log = LogFactory.getLog(DCSClientServiceBean.class);

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void ping(String text) throws ModuleException {
		PNLADLPublisherBL.ping(text);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DCSPNLInfoRS sendPNLToDCS(DCSPNLInfoRQ pnlInfo) throws ModuleException {
		return PNLADLPublisherBL.sendPNLToDCS(pnlInfo);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DCSADLInfoRS sendADLToDCS(DCSADLInfoRQ adlInfo) throws ModuleException {
		return PNLADLPublisherBL.sendADLToDCS(adlInfo);
	}

}
