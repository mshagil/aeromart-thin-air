package com.isa.thinair.wsclient.core.service.rak;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.model.ReservationInsurancePremium;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceCountryTO;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.service.WSClientModuleUtils;
import com.isa.thinair.wsclient.core.config.AIGClientConfig;
import com.isa.thinair.wsclient.core.dao.InsuranceDAO;
import com.isa.thinair.wsclient.core.dao.InsurancePremiumDAO;

/**
 * BL for RAK Insurance change
 * 
 * @author lalanthi
 * @since 03/08/2010
 */
public class RakBL {
	private static final Log log = LogFactory.getLog(RakBL.class);

	public enum RequestType {
		SELL, QUOTE, RESELL
	};

	private static String RAK_INSURANCE_CURRENCY_CODE = "USD";

	/**
	 * Prepares insurance quote
	 * 
	 * @throws ModuleException
	 * */
	public static InsuranceResponse quoteInsurancePolicy(IInsuranceRequest insuranceRequest) throws ModuleException {
		InsuranceResponse insResponse = null;
		if (log.isDebugEnabled()) {
			log.debug("Start quote RAK InsurancePolicy");
		}
		// Calls this method to check whether the origin airport is supported for insurance
		InsuranceCountryTO aigCountryTO = getCountryTO(((InsuranceRequestAssembler) insuranceRequest).getInsureSegment()
				.getFromAirportCode());
		BigDecimal quotedTotalPremium = RakBL.calculateTotalPremium(insuranceRequest);
		BigDecimal quotedTotalTaxAmount = new BigDecimal(0);
		BigDecimal quotedTotalNetAmount = quotedTotalPremium;
		BigDecimal[] baseCurrencyAmounts = null;

		if (quotedTotalPremium != null) {
			baseCurrencyAmounts = getBaseCurrencyAmount(RAK_INSURANCE_CURRENCY_CODE, new BigDecimal[] { quotedTotalPremium,
					quotedTotalNetAmount, quotedTotalTaxAmount });
			insResponse = new InsuranceResponse();
			insResponse.setSuccess(true);
			insResponse.setErrorCode("0");
			insResponse.setErrorMessage("OK");
			insResponse.setMessageId("");
			insResponse.setTotalPremiumAmount(baseCurrencyAmounts[0]);
			insResponse.setQuotedTotalPremiumAmount(quotedTotalPremium);
			insResponse.setQuotedCurrencyCode(RAK_INSURANCE_CURRENCY_CODE);
			insResponse.setEuropianCountry(aigCountryTO.isEuropianCountry());

			InsuranceRequestAssembler insReqAss = (InsuranceRequestAssembler) insuranceRequest;

			insResponse.setAmount(baseCurrencyAmounts[0]);
			insResponse.setQuotedAmount(quotedTotalPremium);

			insResponse.setNetAmount(baseCurrencyAmounts[1]);
			insResponse.setQuotedNetAmount(quotedTotalNetAmount);

			insResponse.setTaxAmount(baseCurrencyAmounts[2]);
			insResponse.setQuotedTaxAmout(quotedTotalTaxAmount);

		} else {
			insResponse = new InsuranceResponse(false, "Error", "NA", "", null, quotedTotalPremium, RAK_INSURANCE_CURRENCY_CODE,
					false);

			if (log.isDebugEnabled()) {
				log.debug("There is an error occurred while calculating total premium for RAK insurance");
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("End quote RAK Insurance Policy");
		}
		return insResponse;

	}

	/**
	 * Unlike the AIG sell policy, we are not sending the sell request to a remote service on the fly. Actual insurance
	 * sales data will be sent to the provider as a batch process (An XML report including all the insured pax details
	 * for a flight for a particular period). This method will create the necessary data(to be published to provider)
	 * and store it in the DB for later retrieval.
	 * 
	 * @param iRakRequest
	 * @return
	 * @throws ModuleException
	 */
	public static InsuranceResponse sellInsurancePolicy(IInsuranceRequest iRakRequest) throws ModuleException {
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRakRequest;
		BigDecimal totalPremiumAmount = insReqAssemebler.getTotalPremiumAmount();
		BigDecimal quotedTotalPremiumAmount = insReqAssemebler.getQuotedTotalPremiumAmount();

		InsuranceResponse insResponse = new InsuranceResponse(true, "0", "OK", "", totalPremiumAmount, quotedTotalPremiumAmount,
				RAK_INSURANCE_CURRENCY_CODE);
		insResponse.setPolicyCode(AppSysParamsUtil.getRakGroupPolicy() + " " + insReqAssemebler.getPnr()); // For the
																											// RAK, PNR
																											// is the
																											// policy
																											// code.

		return insResponse;
	}

	/**
	 * Calculates the insurance premium based on the travel duration and the number of passengers in the reservation
	 * 
	 * @param iRakRequest
	 * @return
	 */
	private static BigDecimal calculateTotalPremium(IInsuranceRequest iRakRequest) {
		InsuranceRequestAssembler insReqAssemebler = (InsuranceRequestAssembler) iRakRequest;

		Date depDate = insReqAssemebler.getInsureSegment().getDepartureDate();
		Date arriveDate = insReqAssemebler.getInsureSegment().getArrivalDate();
		boolean roundTrip = insReqAssemebler.getInsureSegment().isRoundTrip();
		int paxCount = insReqAssemebler.getPassengers().size();
		Map<Integer, ReservationInsurancePremium> insurancePremiumCol = InsurancePremiumDAO.getInsurancePremiumData();
		ReservationInsurancePremium insurancePremium = null;

		if (!roundTrip) {
			insurancePremium = insurancePremiumCol
					.get(new Integer(ReservationInsurancePremium.LESS_THAN_30_DAYS_ONE_WAY_PREMIUM));
			RAK_INSURANCE_CURRENCY_CODE = insurancePremium.getCurrency();
			return AccelAeroCalculator.multiply(new BigDecimal(insurancePremium.getAmount()), paxCount);
		} else {
			long dateDifferenceInMillis = arriveDate.getTime() - depDate.getTime();
			long diffrenceInDays = (dateDifferenceInMillis / (1000 * 60 * 60 * 24)) + 1;
			if (diffrenceInDays < 31) {
				insurancePremium = insurancePremiumCol.get(new Integer(
						ReservationInsurancePremium.LESS_THAN_30_DAYS_ROUND_TRIP_PREMIUM));
				RAK_INSURANCE_CURRENCY_CODE = insurancePremium.getCurrency();
				return AccelAeroCalculator.multiply(new BigDecimal(insurancePremium.getAmount()), paxCount);
			} else if (diffrenceInDays < 46) {
				insurancePremium = insurancePremiumCol.get(new Integer(
						ReservationInsurancePremium.BETWEEN_31_45_DAYS_ROUND_TRIP_PREMIUM));
				RAK_INSURANCE_CURRENCY_CODE = insurancePremium.getCurrency();
				return AccelAeroCalculator.multiply(new BigDecimal(insurancePremium.getAmount()), paxCount);
			} else if (diffrenceInDays < 61) {
				insurancePremium = insurancePremiumCol.get(new Integer(
						ReservationInsurancePremium.BETWEEN_46_60_DAYS_ROUND_TRIP_PREMIUM));
				RAK_INSURANCE_CURRENCY_CODE = insurancePremium.getCurrency();
				return AccelAeroCalculator.multiply(new BigDecimal(insurancePremium.getAmount()), paxCount);
			} else if (diffrenceInDays < 91) {
				insurancePremium = insurancePremiumCol.get(new Integer(
						ReservationInsurancePremium.BETWEEN_61_90_DAYS_ROUND_TRIP_PREMIUM));
				RAK_INSURANCE_CURRENCY_CODE = insurancePremium.getCurrency();
				return AccelAeroCalculator.multiply(new BigDecimal(insurancePremium.getAmount()), paxCount);

			} else {
				return null;
			}
		}

	}

	private static BigDecimal[] getBaseCurrencyAmount(String currencyCode, BigDecimal... values) throws ModuleException {
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal[] convertedValues = null;
		if (!baseCurrencyCode.equals(currencyCode) && values != null && values.length > 0) {
			BigDecimal exchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
					.getPurchaseExchangeRateAgainstBase(currencyCode);
			convertedValues = new BigDecimal[values.length];
			for (int i = 0; i < values.length; i++) {
				convertedValues[i] = AccelAeroRounderPolicy.convertAndRound(values[i], exchangeRate, null, null);
			}
		} else {
			convertedValues = values;
		}
		return convertedValues;
	}

	/**
	 * Returns AIG Country data for specified airport
	 * 
	 * @param originAirport
	 * @return
	 * @throws ModuleException
	 */
	private static InsuranceCountryTO getCountryTO(String originAirport) throws ModuleException {
		AIGClientConfig aigClientConfig = WSClientModuleUtils.getModuleConfig().getAigClientConfig();
		Map<String, InsuranceCountryTO> aigCountryTOMap = aigClientConfig.getAigCountryCodes();
		if (aigCountryTOMap == null) {
			aigCountryTOMap = new HashMap<String, InsuranceCountryTO>();
		}

		InsuranceCountryTO aigCountryTO = null;

		if (aigCountryTOMap.get(originAirport) == null) {
			aigCountryTO = InsuranceDAO.getCountryInformation(originAirport);
		} else {
			aigCountryTO = aigCountryTOMap.get(originAirport);
		}
		if (aigCountryTO != null) {
			aigCountryTOMap.put(originAirport, aigCountryTO);
			aigClientConfig.setAigCountryCodes(aigCountryTOMap);
		} else {
			throw new ModuleException("wsclient.aig.origin.notsupported");
		}
		return aigCountryTO;
	}

}
