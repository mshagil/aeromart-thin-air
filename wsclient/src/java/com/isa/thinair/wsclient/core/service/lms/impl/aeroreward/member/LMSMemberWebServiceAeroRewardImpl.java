package com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.member;

import java.util.List;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberStateDTO;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyPointDetailsDTO;
import com.isa.thinair.aircustomer.api.dto.lms.MemberRemoteLoginResponseDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.core.service.lms.base.member.LMSMemberWebService;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto.Member;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDto.MemberSearchByIdRequest;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardDtoUtils;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.AeroRewardRestServiceClient;
import com.isa.thinair.wsclient.core.service.lms.impl.aeroreward.LMSAeroRewardWebServiceInvoker;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.Country;
import com.isa.thinair.wsclient.core.service.lms.impl.gravty.GravtyDto.MemberFetchDataResponse;

public class LMSMemberWebServiceAeroRewardImpl implements LMSMemberWebService {

	@Override
	public LoyaltyMemberStateDTO getMemberState(String memberAccountId, int internaMemberId) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String enrollMemberWithEmail(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addMemberAccountId(String WSSecurityToken, String memberAccountId, String memberExternalId,
			String idTypeExternalReference, String memberAccountIdToAdd) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public LoyaltyMemberCoreDTO getMemberCoreDetails(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public LoyaltyPointDetailsDTO getMemberPointBalances(String memberAccountId,  String memberExternalId) throws ModuleException {
		AeroRewardRestServiceClient<Member, MemberSearchByIdRequest> serviceClent = LMSAeroRewardWebServiceInvoker
				.getServiceCleint(LMSAeroRewardWebServiceInvoker.RETRIEVE_MEMBER_BY_ID);

		AeroRewardDto.MemberSearchByIdRequest request = new AeroRewardDto.MemberSearchByIdRequest();
		request.setAccountID(memberAccountId);		
		
		AeroRewardDto.Member member = serviceClent.post(request);
		return AeroRewardDtoUtils.adaptToLoyaltyPointDetailsDTO(member);
	}

	@Override
	public boolean saveMemberCustomFields(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setMemberPassword(String memberAccountId, String password) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveMemberEmailAddress(String memberAccountId, String emailAddress) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveMemberPhoneNumbers(String memberAccountId, String phoneNumber, String mobileNumber) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveMemberPreferredCommunicationLanguage(String memberAccountId, String languageCode) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveLoyaltySystemReferredByUser(String memberAccountId, String referredByMemberAccountId)
			throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getCrossPortalLoginUrl(String memberAccountId, String password, String menuItemIntRef) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendMemberWelcomeMessage(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int saveMemberName(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean saveMemberHeadOfHouseHold(String memberAccountId, String headOfHosueHold) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeMemberHeadOfHouseHold(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public MemberRemoteLoginResponseDTO memberLogin(String userName,String password) throws ModuleException {
		return null;
	}

	@Override
	public List<Country> getCountriesList() throws ModuleException {
		return null;
	}

	@Override
	public void updateMember(Customer customer) throws ModuleException {
		// TODO Auto-generated method stub
	}

	@Override
	public MemberFetchDataResponse getMemberFromEmail(String memberAccountId) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	

}
