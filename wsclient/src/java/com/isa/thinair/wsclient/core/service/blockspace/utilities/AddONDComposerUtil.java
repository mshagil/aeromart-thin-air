package com.isa.thinair.wsclient.core.service.blockspace.utilities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;

import org.codehaus.xfire.util.UID;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ.AirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.WebservicesConstants;
import com.isa.thinair.wsclient.api.dto.BaseDTO;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * @author Nilindra Fernando
 * 
 */
public class AddONDComposerUtil {

	public static Object[] compose(OTAAirBookRS otaAirBookRS, OTAAirPriceRS otaAirPriceRS, BaseDTO baseDTO)
			throws ModuleException {

		OTAAirBookModifyRQ otaAirBookModRQ = new OTAAirBookModifyRQ();

		otaAirBookModRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		otaAirBookModRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_AirBookModify);
		otaAirBookModRQ.setSequenceNmbr(new BigInteger("1"));
		otaAirBookModRQ.setEchoToken(UID.generate());
		otaAirBookModRQ.setTimeStamp(CommonUtil.parse(new Date()));
		otaAirBookModRQ.setTransactionIdentifier(otaAirBookRS.getTransactionIdentifier());

		POSType pos = new POSType();
		otaAirBookModRQ.setPOS(pos);
		pos.getSource().add(DataComposerUtil.preparePOS(baseDTO));

		otaAirBookModRQ.setAirBookModifyRQ(new AirBookModifyRQ());
		otaAirBookModRQ.getAirBookModifyRQ().setModificationType(
				CommonUtil.getOTACodeValue(IOTACodeTables.ModificationType_MOD_ADD_OND));

		AirReservationType airReservation = null;
		for (Iterator i = otaAirBookRS.getSuccessAndWarningsAndAirReservation().iterator(); i.hasNext();) {
			Object o = i.next();
			if (o instanceof AirReservation) {
				airReservation = (AirReservationType) o;
			}
		}

		otaAirBookModRQ.getAirBookModifyRQ().getBookingReferenceID().add(airReservation.getBookingReferenceID().get(0));

		BigDecimal totalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (Object o : otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {
			if (o instanceof PricedItinerariesType) {
				for (PricedItineraryType itinsType : ((PricedItinerariesType) o).getPricedItinerary()) {
					otaAirBookModRQ.getAirBookModifyRQ().setAirItinerary(itinsType.getAirItinerary());
					totalPrice = AccelAeroCalculator.add(totalPrice, itinsType.getAirItineraryPricingInfo().getItinTotalFare()
							.getTotalFare().getAmount());
				}
			}
		}

		// Total outstanding payment
		otaAirBookModRQ.getAirBookModifyRQ().setFulfillment(new AirReservationType.Fulfillment());
		otaAirBookModRQ.getAirBookModifyRQ().getFulfillment()
				.setPaymentDetails(new AirReservationType.Fulfillment.PaymentDetails());

		DirectBillType directBill = new DirectBillType();
		directBill.setCompanyName(new CompanyName());
		directBill.getCompanyName().setCode(baseDTO.getOnAccountAgentCode());

		PaymentDetailType paymentDetail = null;
		otaAirBookModRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails().getPaymentDetail()
				.add(paymentDetail = new PaymentDetailType());
		paymentDetail.setDirectBill(directBill);
		paymentDetail.setPaymentAmount(new PaymentAmount());
		paymentDetail.getPaymentAmount().setAmount(totalPrice);

		AALoadDataOptionsType options = new AALoadDataOptionsType();
		options.setLoadFullFilment(true);
		options.setLoadAirItinery(true);
		options.setLoadPriceInfoTotals(true);
		options.setLoadFullFilment(true);
		options.setLoadTravelerInfo(true);

		AAAirBookModifyRQExt airBookModifyRQAAExt = new AAAirBookModifyRQExt();
		airBookModifyRQAAExt.setAALoadDataOptions(options);

		return new Object[] { otaAirBookModRQ, airBookModifyRQAAExt };
	}
}
