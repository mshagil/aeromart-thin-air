package com.isa.thinair.wsclient.client;


import java.util.Calendar;

import com.isa.thinair.airreservation.api.dto.eurocommerce.HeaderDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.ItineraryDocumentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.JourneyDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.NameValueDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.NameValueDocumentDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.PassengerDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckRequestDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.CCFraudCheckResponseDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.SegmentCollectionDTO;
import com.isa.thinair.airreservation.api.dto.eurocommerce.SegmentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.core.service.ec.ECWebervicesImpl;

public class TestECWebervicesImpl extends PlatformTestCase{

	@Override
	protected void setUp() throws Exception {
		System.setProperty("repository.home", "/usr/isa/isatestconfig");
		super.setUp();
	}
	
	public void testProcessPaymentResponseDTO(){
		WSClientBD wsClientBD = (WSClientBD) WebServicesModuleUtils.getWSClientBD();
		try {
			CCFraudCheckResponseDTO res = new ECWebervicesImpl().validateCCPaymentForFraud(getCCFraudCheckRequestDTO());
//			ProcessPaymentResponseDTO res = wsClientBD.validateCCPaymentForFraud(getApprovedProcessPaymentRequestDTO());
			assertNotNull(res);
			assertEquals(res.getStatus(), "Approved");
		} catch (ModuleException e) {
			e.printStackTrace();
		}
	}
	
	
	private CCFraudCheckRequestDTO getCCFraudCheckRequestDTO(){
		CCFraudCheckRequestDTO dto= new CCFraudCheckRequestDTO();
		
		dto.setExternalId("123456789101");
		dto.setExternalRef("123456789101");
		
		dto.setAmount(AccelAeroCalculator.parseBigDecimal(1.01));
		dto.setOriginalCurrencyCode("AED");
		dto.setAccountNumber("4324383499030519");
		dto.setExpirationMonth(9);
		dto.setExpirationYear(2011);
		
		NameValueDocumentDTO nameValueDoc1 =new NameValueDocumentDTO("Global","Global Values");
		nameValueDoc1.addIteamDTOs(new NameValueDTO("AccountHolderName","Smith Francis"));
		
		NameValueDocumentDTO nameValueDoc2 =new NameValueDocumentDTO("CC","Provide CVV Code");
		nameValueDoc2.addIteamDTOs(new NameValueDTO("VerificationCode",""));
		
		NameValueDocumentDTO nameValueDoc3 =new NameValueDocumentDTO("FraudCheck","Fraud Check Values");
		nameValueDoc3.addAllIteamDTOs(new NameValueDTO("Enabled","true"),new NameValueDTO("EmailAddress","accept@airarabiaweb.com"),
				new NameValueDTO("PhoneNumber","11111111"),new NameValueDTO("IPAddress","192.168.105.106"),new NameValueDTO("SessionID","y7eKWGvW3eDJoGSc0fUGjZD"));
		dto.addAllDocumentSnapInDto(nameValueDoc1,nameValueDoc2,nameValueDoc3);
		
		ItineraryDocumentDTO itineraryDocumentDTO = new ItineraryDocumentDTO();
		itineraryDocumentDTO.setName("ItineraryDetails");
		
		HeaderDTO header = new HeaderDTO("X8M3NW","MOTO",true,Calendar.getInstance().getTime(),"MAD");
		itineraryDocumentDTO.setHeaderDTO(header);
		
		PassengerDTO pax1 = new PassengerDTO("Mr", "adult", "adult surname", "loyaltyNumber", "type");
		PassengerDTO pax2 = new PassengerDTO("Mrs", "infant", "infant surname", "loyaltyNumber", "type");
		itineraryDocumentDTO.addAllPassengreDTO(pax1,pax2);
		
		JourneyDTO jdto1 = new JourneyDTO();
		JourneyDTO jdto2 = new JourneyDTO();
		
		Calendar cal1 = Calendar.getInstance();
		cal1.set(2010, 2, 1, 2, 10, 0);
		Calendar cal2 = Calendar.getInstance();
		cal2.set(2010, 2, 1, 4, 5, 0);

		SegmentCollectionDTO segCol1 = new SegmentCollectionDTO();
		SegmentDTO segmentDto1 = new SegmentDTO("Y","CMB", "SHJ",cal1.getTime(),cal2.getTime(),"G5","120","100","AU","+10:00");
		segCol1.addSegmentDTO(segmentDto1);
		
		
		Calendar cal3 = Calendar.getInstance();
		cal3.set(2010, 3, 3, 6, 0, 0);
		Calendar cal4 = Calendar.getInstance();
		cal4.set(2010, 3, 3, 5, 55, 0);
		
		SegmentCollectionDTO segCol2 = new SegmentCollectionDTO();
		SegmentDTO segmentDto2 = new SegmentDTO("Y", "SHJ","CMB",cal3.getTime(),cal4.getTime(),"G5","121","100","AU","+10:00");
		segCol2.addSegmentDTO(segmentDto2);
		jdto1.setSegmentCollectionDTO(segCol1);
		jdto2.setSegmentCollectionDTO(segCol2);
		itineraryDocumentDTO.addAllJourneyDTOs(jdto1,jdto2);
		dto.addDocumentSnapInDto(itineraryDocumentDTO);
		
		return dto;
	}
}
