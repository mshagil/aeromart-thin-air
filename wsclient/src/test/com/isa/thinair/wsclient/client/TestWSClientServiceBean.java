package com.isa.thinair.wsclient.client;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.core.remoting.ejb.WSClientServiceBean;

public class TestWSClientServiceBean {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Context context;
		try {
			ClientLoginModule clientLoginModule = new ClientLoginModule();
			clientLoginModule.login("SYSTEM", "password", "/usr/isa/aares/config-00/repository/modules/login/jass_client_login.config", null);
			
			Properties properties = new Properties();
			properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
			properties.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
			properties.put("java.naming.provider.url", "localhost:1099");
			
			context = new InitialContext(properties);
			WSClientBD remoteWSClientBD = (WSClientBD) context.lookup(WSClientServiceBean.SERVICE_NAME + ".remote");
			
			Calendar startTimestamp = new GregorianCalendar();
			startTimestamp.add(Calendar.DAY_OF_MONTH, 0);
			startTimestamp.set(Calendar.HOUR_OF_DAY, 0);
			startTimestamp.set(Calendar.MINUTE, 0);
			startTimestamp.set(Calendar.SECOND, 0);
			startTimestamp.set(Calendar.MILLISECOND, 0);
			
			Calendar endTimestmap = new GregorianCalendar();
			endTimestmap.add(Calendar.DAY_OF_MONTH, 0);
			endTimestmap.set(Calendar.HOUR_OF_DAY, 23);
			endTimestmap.set(Calendar.MINUTE, 59);
			endTimestmap.set(Calendar.SECOND, 59);
			endTimestmap.set(Calendar.MILLISECOND, 999);
			
			remoteWSClientBD.requestDailyBankTnxReconcilation(startTimestamp.getTime(), endTimestmap.getTime(), "EBI");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}