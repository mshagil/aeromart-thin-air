package com.isa.thinair.wsclient.client;

import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.ejb.CreateException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.wsclient.core.remoting.ejb.WSClientService;
import com.isa.thinair.wsclient.core.remoting.ejb.WSClientServiceHome;

public class TestRMWebservicesClient {

	/**
	 * @param args
	 * @throws NamingException 
	 * @throws CreateException 
	 * @throws RemoteException 
	 * @throws ModuleException 
	 */
	public static void main(String[] args) throws NamingException, RemoteException, CreateException, ModuleException {
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		hashtable.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		hashtable.put("java.naming.provider.url", "jnp://localhost:1099");
		InitialContext initialContext = new InitialContext(hashtable);
		
		WSClientServiceHome wsClientHome = (WSClientServiceHome) initialContext.lookup("ejb/WSClientService");
		WSClientService wsClientService = wsClientHome.create();
		
		wsClientService.publishRMInventoryAlerts();
	}

}
