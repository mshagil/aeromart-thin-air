package com.isa.thinair.aaservices.core.bl.paxcredit;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCreditSearchOptions;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxCreditSearchByPnrListRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxCreditSearchByPnrListRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxCreditSearchRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxCreditSearchRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AAPaxCreditBL {

	private static Log log = LogFactory.getLog(AAPaxCreditBL.class);

	public AAPaxCreditSearchRS getPaxCreditForOtherCarrier(AAPaxCreditSearchRQ aaPaxCreditSearchRQ) {

		AAPaxCreditSearchRS aaPaxCreditSearchRS = initAAPaxCreditSearchRS();
		Collection<ReservationPaxDTO> collection = new ArrayList<ReservationPaxDTO>();

		try {
			// pnr search
			PaxCreditSearchOptions paxCreditSearchOptions = aaPaxCreditSearchRQ.getPaxCreditSearchOptions();
			if (paxCreditSearchOptions.getCreditCardNo() == null || paxCreditSearchOptions.getCreditCardNo().isEmpty()) {
				ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
				reservationSearchDTO.setOwnerAgentCode(paxCreditSearchOptions.getAgentCode());
				reservationSearchDTO.setPnr(paxCreditSearchOptions.getPnr());
				reservationSearchDTO.setFirstName(paxCreditSearchOptions.getPersonName().getFirstName());
				reservationSearchDTO.setLastName(paxCreditSearchOptions.getPersonName().getSurName());
				reservationSearchDTO.setFromAirport(paxCreditSearchOptions.getFromAirport());
				reservationSearchDTO.setToAirport(paxCreditSearchOptions.getToAirport());
				reservationSearchDTO.setTelephoneNo(paxCreditSearchOptions.getPhoneNo());
				reservationSearchDTO.setDepartureDate(paxCreditSearchOptions.getDepatureDate());
				reservationSearchDTO.setReturnDate(paxCreditSearchOptions.getReturnDate());
				reservationSearchDTO.setFilterZeroCredits(paxCreditSearchOptions.isFilterZeroCredits());
				reservationSearchDTO.setFlightNo(paxCreditSearchOptions.getFlightNo());

				if (paxCreditSearchOptions.getOriginChannelIds() == null
						|| paxCreditSearchOptions.getOriginChannelIds().isEmpty()) {
					reservationSearchDTO.setOriginChannelIds(null);
				} else {
					reservationSearchDTO.setOriginChannelIds(paxCreditSearchOptions.getOriginChannelIds());
				}
				collection = AAServicesModuleUtils.getReservationQueryBD().getPnrPassengerSumCredit(reservationSearchDTO);

			} else { // CC search
				ReservationPaymentDTO reservationPaymentDTO = new ReservationPaymentDTO();
				reservationPaymentDTO.setOriginAgentCode(paxCreditSearchOptions.getAgentCode());
				reservationPaymentDTO.setCreditCardNo(paxCreditSearchOptions.getCreditCardNo());
				reservationPaymentDTO.setEDate(paxCreditSearchOptions.getCardExpiry());
				reservationPaymentDTO.setOriginChannelIds(paxCreditSearchOptions.getOriginChannelIds());

				collection = AAServicesModuleUtils.getReservationQueryBD()
						.getReservationsForCreditCardInfo(reservationPaymentDTO);
			}

			// transforming the results
			if (collection != null && !collection.isEmpty()) {
				for (ReservationPaxDTO reservationPaxDTO : collection) {
					aaPaxCreditSearchRS.getPaxCretidDetailInfo().add(
							PaxCreditUtil.populateReservationPassengerDetailInfo(reservationPaxDTO));
				}
			}

		} catch (ModuleException ex) {
			log.error("ERROR", ex);
			AAExceptionUtil.addAAErrrors(aaPaxCreditSearchRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaPaxCreditSearchRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaPaxCreditSearchRS.getResponseAttributes().setSuccess(successType);
		}

		return aaPaxCreditSearchRS;
	}

	private AAPaxCreditSearchRS initAAPaxCreditSearchRS() {
		AAPaxCreditSearchRS aaPaxCreditSearchRS = new AAPaxCreditSearchRS();
		aaPaxCreditSearchRS.setResponseAttributes(new AAResponseAttributes());
		return aaPaxCreditSearchRS;
	}

	public AAPaxCreditSearchByPnrListRS getPaxCreditForOtherCarrier(AAPaxCreditSearchByPnrListRQ aaPaxCreditSearchByPnrListRQ) {
		AAPaxCreditSearchByPnrListRS aaPaxCreditSearchByPnrListRS = initAAPaxCreditSearchByPnrListRS();
		Collection<PaxCreditDTO> collection = new ArrayList<PaxCreditDTO>();
		try {
			collection = AAServicesModuleUtils.getReservationQueryBD().getPaxCreditForPnrs(
					aaPaxCreditSearchByPnrListRQ.getPnrList());

			if (collection != null && !collection.isEmpty()) {
				aaPaxCreditSearchByPnrListRS.getCustomerCredit().addAll(PaxCreditUtil.populateCustomerCredit(collection));
			}

		} catch (ModuleException ex) {
			log.error("ERROR", ex);
			AAExceptionUtil.addAAErrrors(aaPaxCreditSearchByPnrListRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaPaxCreditSearchByPnrListRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaPaxCreditSearchByPnrListRS.getResponseAttributes().setSuccess(successType);
		}

		return aaPaxCreditSearchByPnrListRS;
	}

	private AAPaxCreditSearchByPnrListRS initAAPaxCreditSearchByPnrListRS() {
		AAPaxCreditSearchByPnrListRS aaPaxCreditSearchByPnrListRS = new AAPaxCreditSearchByPnrListRS();
		aaPaxCreditSearchByPnrListRS.setResponseAttributes(new AAResponseAttributes());
		return aaPaxCreditSearchByPnrListRS;
	}

}
