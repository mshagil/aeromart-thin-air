package com.isa.thinair.aaservices.core.bl.availability;

import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;

public class AAPaxCountAssembler implements IPaxCountAssembler {
	private int adultCount = 0;
	private int childCount = 0;
	private int infantCount = 0;

	public AAPaxCountAssembler(List<PassengerTypeQuantity> paxQtyList) {
		for (PassengerTypeQuantity paxQtyType : paxQtyList) {
			if (PassengerType.ADT == paxQtyType.getPassengerType()) {
				adultCount = paxQtyType.getQuantity();
			} else if (PassengerType.CHD == paxQtyType.getPassengerType()) {
				childCount = paxQtyType.getQuantity();
			} else if (PassengerType.INF == paxQtyType.getPassengerType()) {
				infantCount = paxQtyType.getQuantity();
			}
		}
	}

	@Override
	public int getInfantCount() {
		return infantCount;
	}

	@Override
	public int getChildCount() {
		return childCount;
	}

	@Override
	public int getAdultCount() {
		return adultCount;
	}

}
