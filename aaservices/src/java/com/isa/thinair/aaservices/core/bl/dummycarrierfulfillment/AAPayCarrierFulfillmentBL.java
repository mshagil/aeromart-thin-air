package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfillmentCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;

/**
 * This is responsible for for creating the payable records in the payment carrier In order to link the external
 * payments a dummy reservation is created in the Marketing carrier.If the reservation already exist in the MC that will
 * be used to link the external payments Created reservation's pnr will be same as the operating Carrier pnr and will
 * link to external segments and transactions.
 * 
 * With the dummy reservation dummy pax will also be created with suitable values If later the opCarrier booking get
 * modified to MC then the same dummy booking will be converted to represent the actual booking in that carrier
 * 
 * @author mekanayake
 * 
 */
public class AAPayCarrierFulfillmentBL {

	private static Log log = LogFactory.getLog(AAPayCarrierFulfillmentBL.class);

	private AAPayCarrierFulfillmentCode fulfillmentAction;

	private AAReservation aaAirReservation;

	private AAReservation newAAAirReservation;

	private AAPOS aaPos;

	private boolean isAutoRefund;

	private boolean fromNameChange;
	
	private String reasonForAllowBlPax;

	public AAPayCarrierFulfillmentBL(AAPayCarrierFulfullmentRQ aaPayCarrierFulfullmentRQ) {
		super();
		this.aaAirReservation = aaPayCarrierFulfullmentRQ.getAaAirReservation();
		this.newAAAirReservation = aaPayCarrierFulfullmentRQ.getNewAAAirReservation();
		this.fulfillmentAction = aaPayCarrierFulfullmentRQ.getFulfillmentAction();
		this.aaPos = aaPayCarrierFulfullmentRQ.getAaPos();
		this.isAutoRefund = aaPayCarrierFulfullmentRQ.isIsAutoRefund();
		this.fromNameChange = aaPayCarrierFulfullmentRQ.isFromNameChange();
		this.reasonForAllowBlPax = aaPayCarrierFulfullmentRQ.getReasonForAllowBlPax();
	}

	public AAPayCarrierFulfullmentRS execute() {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());
		
		try {

			checkConstraints();

			// build the external tnx to be added to the dummy res
			if (log.isDebugEnabled()) {
				log.debug("requesting pay carrier fulfilmetn " + fulfillmentAction);
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_1_CREATE_RESERVATION) {
				AAFulfillPayment aaFulfillPayment = new AAFulfillPayment(aaAirReservation, null, aaPos, isAutoRefund, reasonForAllowBlPax);
				carrierFulfillment = aaFulfillPayment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_2_ONHOLD_RESERVATION) {
				AAFulfillOHDReservation aaFulfillONDReservation = new AAFulfillOHDReservation(aaAirReservation, null, aaPos);
				carrierFulfillment = aaFulfillONDReservation.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_3_CANCEL_RESERVATION) {
				AAFulfillCancelReservation aaFulfillCancelRes = new AAFulfillCancelReservation(aaAirReservation, null, aaPos);
				carrierFulfillment = aaFulfillCancelRes.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_4_BALANCE_PAYMENT) {
				// reusing the AAFulfillPayment for balance payment as well
				AAFulfillPayment aaFulfillPayment = new AAFulfillPayment(aaAirReservation, null, aaPos, isAutoRefund, reasonForAllowBlPax);

				carrierFulfillment = aaFulfillPayment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_5_MODIFY_RESERVATION_CONTACT_INFO) {
				// reusing the AAFulfillPayment for balance payment as well
				AAFulfillModifyContact aaFulfillPayment = new AAFulfillModifyContact(aaAirReservation, null, aaPos);
				carrierFulfillment = aaFulfillPayment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_6_MODIFY_RESERVATION_PAX_INFO) {
				// reusing the AAFulfillPayment for balance payment as well
				AAFulfillModifyPaxInfo aaFulfillPayment = new AAFulfillModifyPaxInfo(aaAirReservation, null, aaPos);
				carrierFulfillment = aaFulfillPayment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_9_CANCEL_MODIFY_OND) {
				AAFulfillCancelOrModifySegment aaFulfillCancelSegment = new AAFulfillCancelOrModifySegment(aaAirReservation,
						newAAAirReservation, aaPos);
				aaFulfillCancelSegment.setFromNameChange(this.fromNameChange);
				carrierFulfillment = aaFulfillCancelSegment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_10_SPLIT_PASSENGER) {
				AAFulfillSplitReservation aaFulfillCancelSegment = new AAFulfillSplitReservation(aaAirReservation,
						newAAAirReservation, aaPos);
				aaFulfillCancelSegment.setSplitForRemovePax(false);
				carrierFulfillment = aaFulfillCancelSegment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_14_TRANSFER_OWNERSHIP) {
				AAFulfillTransferOwnership aaFulfillCancelSegment = new AAFulfillTransferOwnership(aaAirReservation,
						newAAAirReservation, aaPos);
				carrierFulfillment = aaFulfillCancelSegment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_16_ADD_INFANT) {
				AAFulfillAddInfant aaFulfillCancelSegment = new AAFulfillAddInfant(aaAirReservation, newAAAirReservation, aaPos);
				carrierFulfillment = aaFulfillCancelSegment.execute();
			}

			if (fulfillmentAction == AAPayCarrierFulfillmentCode.FULFILL_17_REMOVE_PAX) {
				AAFulfillSplitReservation aaFulfillCancelSegment = new AAFulfillSplitReservation(aaAirReservation,
						newAAAirReservation, aaPos);
				aaFulfillCancelSegment.setSplitForRemovePax(true);
				carrierFulfillment = aaFulfillCancelSegment.execute();
			}

			if (log.isDebugEnabled()) {
				log.debug("Pay carrier fulfilmetn succsessfull for" + fulfillmentAction);
			}

		} catch (Exception e) {
			log.error("pay carrier fulfillment (AAPayCarrierFulfullmentRQ) failed for [" + aaAirReservation.getGroupPnr() + "] "
					+ fulfillmentAction + " .", e);
			AAExceptionUtil.addAAErrrors(carrierFulfillment.getResponseAttributes().getErrors(), e);
		}

		return carrierFulfillment;
	}

	private void checkConstraints() throws WebservicesException {

		if (aaAirReservation == null || aaAirReservation.getAirReservation() == null || fulfillmentAction == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "requierd fields are not incluted");
		}

	}

}
