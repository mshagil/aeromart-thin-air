package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeReverse;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeReverseInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author M.Rikaz
 */
class AAChargeReverseBL {

	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private Reservation reservation;

	public AAAirBookModifyRQ getAaAirBookModifyRQ() {
		return aaAirBookModifyRQ;
	}

	public void setAaAirBookModifyRQ(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * Does group adjustments. Use this for single adjustments as well, unless there's a good reason to use the
	 * execute() method. It's kept there for reverse compatibility and reverting possibilities. Will be removed in a
	 * later merge
	 * 
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	void executeChargeReversal() throws WebservicesException, ModuleException {

		// Collection privilegeKeys = (Collection) AASessionManager.getInstance()
		// .getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		ChargeReverseInfo chargeReverseInfo = aaAirBookModifyRQ.getAaReservation().getAirReservation().getChargeReverseInfo();
		boolean isNoSHowCharge = chargeReverseInfo.isNoShowTaxReverse();
		List<ChargeReverse> chargeReverseList = chargeReverseInfo.getChargeReverseList();
		Collection<Integer> pnrPaxIds = new ArrayList<>();
		Collection<String> pnrPaxIdsStr = new ArrayList<>();
		if (chargeReverseList != null && !chargeReverseList.isEmpty()) {
			for (ChargeReverse chargeReverse : chargeReverseList) {
				pnrPaxIdsStr.add(chargeReverse.getTravelerRefNumber());

				if (chargeReverse.getTravelerRefNumberList() != null && !chargeReverse.getTravelerRefNumberList().isEmpty()) {
					for (String travelerRefNumber : chargeReverse.getTravelerRefNumberList()) {
						// pnrPaxs.add(new Integer(strPnrPax));
						pnrPaxIds.add(new Integer(getPnrPaxId(travelerRefNumber)));

					}
				}

			}
		}

		// List<Integer> pnrPaxFareIDs = new ArrayList<Integer>();
		//
		// // Common values taken from the first element of the list.
		// int chargeRateId = -1;
		// BigDecimal amount = chargeAdjustments.get(0).getAmount();
		// String userNote = chargeAdjustments.get(0).getUserNote();

		// Get the charge adjustment type object for the selected adjustment type id.
		// ChargeAdjustmentTypeDTO chargeAdjustmentType = ChargeAdjustmentTypeUtil
		// .getChargeAdjustmentTypeDTO(chargeAdjustments.get(0).getChargeAdjustmentTypeId());

		// // Get the associated charge rate id from the charge adjustment type.
		// if (chargeAdjustments.get(0).getChargeType().value().equals(ChargeType.REFUNDABLE.value())) {
		// AAReservationUtil.authorize(privilegeKeys, chargeAdjustmentType.getRefundablePrivilegeId());
		// chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, true);
		// } else if (chargeAdjustments.get(0).getChargeType().value().equals(ChargeType.NON_REFUNDABLE.value())) {
		// AAReservationUtil.authorize(privilegeKeys, chargeAdjustmentType.getNonRefundablePrivilegeId());
		// chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, false);
		// } else {
		// throw new WebservicesException(AAErrorCode.ERR_49_MAXICO_REQUIRED_INVALID_CHARGE_FOUND_FOR_ADJUSTMENT,
		// "Charge Type can not be empty");
		// }
		TrackInfoDTO trckInfo = new TrackInfoDTO();

		AAServicesModuleUtils.getPassengerBD().reverseRefundableCharges(reservation.getPnr(), pnrPaxIds, null, true, trckInfo);

	}

	private static Integer getPnrPaxId(String lccTravelerReferenceNumber) {
		int pnrPaxIdStartIndex = lccTravelerReferenceNumber.lastIndexOf("$");
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = lccTravelerReferenceNumber.substring(pnrPaxIdStartIndex);
			return new Integer(pnrPaxId);
		}
		return null;
	}

}
