package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerSelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryReleaseSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryReleaseSeatRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.platform.api.ServiceResponce;

public class AAAncillaryReleaseSeatBL {
	private static final Log log = LogFactory.getLog(AAAncillaryReleaseSeatBL.class);

	public AAAncillaryReleaseSeatRS execute(AAAncillaryReleaseSeatRQ releaseSeatRQ) {
		AAAncillaryReleaseSeatRS releaseSeatRS = new AAAncillaryReleaseSeatRS();
		releaseSeatRS.setResponseAttributes(new AAResponseAttributes());
		releaseSeatRS.getResponseAttributes().setSuccess(new AASuccess());
		try {
			SeatMapBD seatMapBD = AAServicesModuleUtils.getSeatMapBD();
			Set<Integer> flightAMSeatIds = new HashSet<Integer>();
			for (SegmentSeats segmentSeats : releaseSeatRQ.getSegmentSeats()) {
				FlightSegment flightSegment = segmentSeats.getFlightSegments();
				String flightSegmentId = flightSegment.getFlightRefNumber();

				for (PassengerSelectedSeat selectedSeat : segmentSeats.getPassengerSelectedSeat()) {
					String seatNumber = selectedSeat.getSeatNumbers();
					Integer flightAMSeatId = seatMapBD.getFlightAmSeatID(new Integer(flightSegmentId), seatNumber);
					flightAMSeatIds.add(flightAMSeatId);
				}
			}
			if (flightAMSeatIds.size() > 0) {
				if (log.isDebugEnabled()) {
					log.debug("Calling BD to update Seat status to VAC");
				}
				ServiceResponce serviceResponce = seatMapBD.releaseBlockedSeats(flightAMSeatIds);
				if (log.isDebugEnabled()) {
					log.debug("Seat status updation " + (serviceResponce.isSuccess() ? "Successful" : "Not Successful"));
				}
			}
		} catch (Exception e) {
			releaseSeatRS.getResponseAttributes().setSuccess(null);
		}

		return releaseSeatRS;
	}
}
