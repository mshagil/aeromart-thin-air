package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ClientPaymentAssembler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerRefPaymentMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AARequoteModifySegmentsRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AARequoteModifySegmentsRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;

public class AARequoteModifySegmentsBL {
	private static Log log = LogFactory.getLog(AARequoteModifySegmentsBL.class);

	private AARequoteModifySegmentsRQ aaRequoteModifySegmentsRQ;

	private TransactionalReservationProcess tnxResProcess;

	public AARequoteModifySegmentsBL(AARequoteModifySegmentsRQ aaRequoteModifySegmentsRQ, AASessionManager aaSessionManager) {
		this.aaRequoteModifySegmentsRQ = aaRequoteModifySegmentsRQ;
		String transactionId = PlatformUtiltiies.nullHandler((aaRequoteModifySegmentsRQ != null && aaRequoteModifySegmentsRQ
				.getHeaderInfo() != null) ? aaRequoteModifySegmentsRQ.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			this.tnxResProcess = (TransactionalReservationProcess) aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AARequoteModifySegmentsRS execute() {
		AARequoteModifySegmentsRS aaRequoteModifySegmentsRS = new AARequoteModifySegmentsRS();
		aaRequoteModifySegmentsRS.setResponseAttributes(new AAResponseAttributes());

		RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();

		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();
		if (aaRequoteModifySegmentsRQ.getSelectedFareTypes() != null
				&& aaRequoteModifySegmentsRQ.getSelectedFareTypes().size() > 0) {
			fareSegChargeTOs.addAll(tnxResProcess.getFSCSet(aaRequoteModifySegmentsRQ.getSelectedFareTypes()));

			PaxCountAssembler paxCountAssembler = new PaxCountAssembler(tnxResProcess.getPaxCountAssembler().getAdultCount(),
					tnxResProcess.getPaxCountAssembler().getChildCount(), tnxResProcess.getPaxCountAssembler().getInfantCount());

			String bookingType = null;
			if (fareSegChargeTOs != null) {
				bookingType = tnxResProcess.getBookingType(fareSegChargeTOs.get(0));
			}

			QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(fareSegChargeTOs, paxCountAssembler, null,
					bookingType);
			requoteModifyRQ.setFareInfo(fareInfo);
		}

		try {

			Reservation reservation = loadReservation(aaRequoteModifySegmentsRQ.getGroupPnr());
			compseRequoteModifyRQ(requoteModifyRQ, reservation);
			requoteModifyRQ.setReasonForAllowBlPax(aaRequoteModifySegmentsRQ.getReasonForAllowBlPax());
			ServiceResponce response = AAServicesModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ,
					null);

			if (response.isSuccess()) {
				Long version = (Long) response.getResponseParam(CommandParamNames.VERSION);
				if (version != null) {
					aaRequoteModifySegmentsRS.setVersion(version.toString());
				}
			}

		} catch (Exception exp) {
			log.error("requoteModifySegments(requoteModifyRQ) failed.", exp);
			AAExceptionUtil.addAAErrrors(aaRequoteModifySegmentsRS.getResponseAttributes().getErrors(), exp);
		}

		aaRequoteModifySegmentsRS.setGroupPnr(aaRequoteModifySegmentsRQ.getGroupPnr());
		aaRequoteModifySegmentsRS.getResponseAttributes().setSuccess(new AASuccess());

		return aaRequoteModifySegmentsRS;
	}

	private void compseRequoteModifyRQ(RequoteModifyRQ requoteModifyRQ, Reservation reservation) throws ModuleException {
		requoteModifyRQ.setGroupPnr(aaRequoteModifySegmentsRQ.getGroupPnr());
		requoteModifyRQ.setPnr(aaRequoteModifySegmentsRQ.getGroupPnr());
		requoteModifyRQ.setActualPayment(aaRequoteModifySegmentsRQ.isActualPayment());
		requoteModifyRQ.setAutoCancellationEnabled(aaRequoteModifySegmentsRQ.isAutoCancellationEnabled());
		requoteModifyRQ.setUserNotes(aaRequoteModifySegmentsRQ.getUserNotes());
		requoteModifyRQ.setUserNoteType(aaRequoteModifySegmentsRQ.getUserNoteType());
		requoteModifyRQ.setEndorsementNotes(aaRequoteModifySegmentsRQ.getEndorsementNotes());
		requoteModifyRQ.setHasBufferTimeAutoCnxPrivilege(aaRequoteModifySegmentsRQ.isHasBufferTimeAutoCnxPrivilege());
		requoteModifyRQ.setPaxExtChgMap(TransformerUtil.composePerPaxExternalChargeMap(aaRequoteModifySegmentsRQ
				.getPaxExtChgMap()));
		// requoteModifyRQ.setLccPassengerPayments(TransformerUtil.transformLccPaymentMap(aaRequoteModifySegmentsRQ
		// .getPassengerPaymentMap()));
		requoteModifyRQ.setLccPassengerPayments(transformLccPaxRefPaymentMap(
				aaRequoteModifySegmentsRQ.getPassengerRefPaymentMap(), requoteModifyRQ.getPaxExtChgMap()));
		
		IInsuranceRequest insuranceRequest = TransformerUtil.transformInsurance(aaRequoteModifySegmentsRQ.getInsuranceRequest(),	reservation);
	
		if(insuranceRequest != null){
			List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();
			insuranceRequests.add(insuranceRequest);
			requoteModifyRQ.setInsurances(insuranceRequests);
		}
		
	
		// TODO add insurance pax details later
		requoteModifyRQ.setLastCurrencyCode(aaRequoteModifySegmentsRQ.getLastCurrencyCode());
		requoteModifyRQ.setLastModificationTimestamp(aaRequoteModifySegmentsRQ.getLastModificationTimestamp());

		requoteModifyRQ.getRemovedSegmentIds().addAll(aaRequoteModifySegmentsRQ.getRemovedSegIds());

		requoteModifyRQ.setReservationStatus(aaRequoteModifySegmentsRQ.getReservationStatus());
		requoteModifyRQ.setVersion(Long.toString(reservation.getVersion()));

		requoteModifyRQ.setOldFareIdByFltSegIdMap(TransformerUtil.transformIntegerIntegerMap(aaRequoteModifySegmentsRQ
				.getOldFareIdFltSegIdMap()));
		requoteModifyRQ.setOndFareTypeByFareIdMap(TransformerUtil.transformIntegerStringMap(aaRequoteModifySegmentsRQ
				.getOndFareTypeFareIdMap()));
		requoteModifyRQ.setRequoteSegmentMap(TransformerUtil.transformStringStringMap(aaRequoteModifySegmentsRQ
				.getRequoteSegmentMap()));
		requoteModifyRQ.setGroundFltSegByFltSegId(TransformerUtil.transformStringStringMap(aaRequoteModifySegmentsRQ
				.getGroundFltSegByFltSegIdMap()));
		requoteModifyRQ.setPaxWiseAdjustmentAmountMap(TransformerUtil.transformIntegerDecimalMap(aaRequoteModifySegmentsRQ
				.getPaxWiseAdjustmentAmount()));
		requoteModifyRQ.setFlightRefWiseOndBaggageGroup(TransformerUtil.transformStringStringMap(aaRequoteModifySegmentsRQ
				.getFltSegOndBaggageGroup()));
		requoteModifyRQ.setNameChangedPaxMap(TransformerUtil.transformIntegerPaxNameDTO(aaRequoteModifySegmentsRQ
				.getNameChangedPaxMap()));
		requoteModifyRQ.setDuplicateNameCheck(aaRequoteModifySegmentsRQ.isSkipDuplicateNameCheck());

		// TODO should pass user action mode to get this
		requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);

		requoteModifyRQ.setLastCurrencyCode(aaRequoteModifySegmentsRQ.getLastCurrencyCode());
		requoteModifyRQ.setLastFareQuoteDate(aaRequoteModifySegmentsRQ.getLastFareQuotedDate());
		requoteModifyRQ.setFQWithinValidity(aaRequoteModifySegmentsRQ.isFqWithinValidity());

		// To Do add fare discount details

		requoteModifyRQ.setReleaseTimeStampZulu(aaRequoteModifySegmentsRQ.getReleaseTimeStampZulu());
		requoteModifyRQ.setExternalChargesMap(getExternalChargesMap(aaRequoteModifySegmentsRQ.getExternalCharge()));
		requoteModifyRQ.setApplyModificationCharge(aaRequoteModifySegmentsRQ.isCnxWithModCharge());
		CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(aaRequoteModifySegmentsRQ.getChargeOverride(),
				ReservationConstants.AlterationType.ALT_REQUOTE);
		requoteModifyRQ.setCustomCharges(customChargesTO);
	}

	private static Map<EXTERNAL_CHARGES, ExternalChgDTO> getExternalChargesMap(List<ExternalCharge> externalCharges)
			throws ModuleException {

		Set<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();
		if (externalCharges != null) {
			for (ExternalCharge externalCharge : externalCharges) {
				colEXTERNAL_CHARGES.add(TransformerUtil.transformExternalChargeType(externalCharge.getType()));
			}
		}

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		if (colEXTERNAL_CHARGES.size() > 0) {
			extExternalChgDTOMap = AAServicesModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
					ChargeRateOperationType.MODIFY_ONLY);
		}
		return extExternalChgDTOMap;
	}

	private static Map<String, LCCClientPaymentAssembler> transformLccPaxRefPaymentMap(
			List<PassengerRefPaymentMap> paymentMapList, Map<Integer, List<LCCClientExternalChgDTO>> paxExternalChargesMap) {
		Map<String, LCCClientPaymentAssembler> trnPaymentMap = new HashMap<String, LCCClientPaymentAssembler>();

		for (PassengerRefPaymentMap passengerPaymentMap : paymentMapList) {
			String paxRefNum = passengerPaymentMap.getKey();
			int paxSeq = PaxTypeUtils.getPaxSeq(paxRefNum);
			LCCClientPaymentAssembler lccAssembler = new LCCClientPaymentAssembler();

			ClientPaymentAssembler assembler = passengerPaymentMap.getValue();
			List<LCCClientExternalChgDTO> lccClientExternalChgDTOs = TransformerUtil.transformExcharges(assembler
					.getPerPaxExternalCharges());

			List<LCCClientExternalChgDTO> paxExternalCharges = paxExternalChargesMap.get(paxSeq);
			if (paxExternalCharges != null && lccClientExternalChgDTOs != null) {
				addAddtionalDetails(lccClientExternalChgDTOs, paxExternalCharges);
			}

			lccAssembler.getPerPaxExternalCharges().addAll(lccClientExternalChgDTOs);

			trnPaymentMap.put(passengerPaymentMap.getKey(), lccAssembler);

		}

		return trnPaymentMap;
	}

	private static void addAddtionalDetails(List<LCCClientExternalChgDTO> lccClientExternalChgDTOs,
			List<LCCClientExternalChgDTO> paxExternalCharges) {
		Iterator<LCCClientExternalChgDTO> lccClientExternalChgDTOIte = lccClientExternalChgDTOs.iterator();
		List<LCCClientExternalChgDTO> tmpLccClientExternalChgDTOs = new ArrayList<LCCClientExternalChgDTO>();
		while (lccClientExternalChgDTOIte.hasNext()) {
			LCCClientExternalChgDTO lccClientExternalChgDTO = lccClientExternalChgDTOIte.next();
			for (LCCClientExternalChgDTO paxExternalCharge : paxExternalCharges) {
				if (paxExternalCharge.getExternalCharges() == lccClientExternalChgDTO.getExternalCharges()
						&& paxExternalCharge.getFlightRefNumber().equals(lccClientExternalChgDTO.getFlightRefNumber())
						&& (paxExternalCharge.getCode() != null && paxExternalCharge.getCode().equals(
								lccClientExternalChgDTO.getCode()))) {
					if (paxExternalCharge.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)) {
						lccClientExternalChgDTOIte.remove();
						tmpLccClientExternalChgDTOs.add(paxExternalCharge);
					} else {
						lccClientExternalChgDTO.setAirportCode(paxExternalCharge.getAirportCode());
						lccClientExternalChgDTO.setApplyOn(paxExternalCharge.getApplyOn());
						lccClientExternalChgDTO.setUserNote(paxExternalCharge.getUserNote());
						lccClientExternalChgDTO.setOndBaggageChargeGroupId(paxExternalCharge.getOndBaggageChargeGroupId());
						lccClientExternalChgDTO.setOndBaggageGroupId(paxExternalCharge.getOndBaggageGroupId());
						lccClientExternalChgDTO.setAnciOffer(paxExternalCharge.isAnciOffer());
						lccClientExternalChgDTO.setOfferedAnciTemplateID(paxExternalCharge.getOfferedAnciTemplateID());
					}
					break;
				} else if (paxExternalCharge.getExternalCharges() == lccClientExternalChgDTO.getExternalCharges()
						&& paxExternalCharge.getFlightRefNumber().equals(lccClientExternalChgDTO.getFlightRefNumber())) {
					lccClientExternalChgDTO.setAirportCode(paxExternalCharge.getAirportCode());
					lccClientExternalChgDTO.setApplyOn(paxExternalCharge.getApplyOn());
					lccClientExternalChgDTO.setUserNote(paxExternalCharge.getUserNote());
					lccClientExternalChgDTO.setOndBaggageChargeGroupId(paxExternalCharge.getOndBaggageChargeGroupId());
					lccClientExternalChgDTO.setOndBaggageGroupId(paxExternalCharge.getOndBaggageGroupId());
					lccClientExternalChgDTO.setAnciOffer(paxExternalCharge.isAnciOffer());
					lccClientExternalChgDTO.setOfferedAnciTemplateID(paxExternalCharge.getOfferedAnciTemplateID());
				}
			}
		}	
		
		for(LCCClientExternalChgDTO paxExternalCharge : paxExternalCharges){
			if (paxExternalCharge.getExternalCharges().equals(EXTERNAL_CHARGES.HANDLING_CHARGE)) {
				tmpLccClientExternalChgDTOs.add(paxExternalCharge);
			}
		}

		if (tmpLccClientExternalChgDTOs.size() > 0) {
			lccClientExternalChgDTOs.addAll(tmpLccClientExternalChgDTOs);
		}

	}

	public static Reservation loadReservation(String pnr) throws ModuleException, WebservicesException {
		Reservation reservation = null;
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		return reservation;
	}
}
