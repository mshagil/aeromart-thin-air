package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TransferSegment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AATransferSegmentsRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AATransferSegmentsRS;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

public class AATransferSegmentsBL {

	private AATransferSegmentsRQ aaTransferSegmentsRQ;

	public void setTransferSegmentsRQ(AATransferSegmentsRQ aaTransferSegmentsRQ) {
		this.aaTransferSegmentsRQ = aaTransferSegmentsRQ;
	}

	/**
	 * Execute some logic based on the Request object and process and return the Response object
	 * 
	 * @return AATransferSegmentsRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public AATransferSegmentsRS execute() {

		AATransferSegmentsRS aaTransferSegmentsRS = new AATransferSegmentsRS();
		AAResponseAttributes aaResponseAttributes = new AAResponseAttributes();
		aaTransferSegmentsRS.setResponseAttributes(aaResponseAttributes);

		try {
			String pnr = aaTransferSegmentsRQ.getPnr();
			String reservationVersion = aaTransferSegmentsRQ.getReservationVersion();
			List<TransferSegment> aaTransferSegments = aaTransferSegmentsRQ.getTransferSegments();
			Map<Integer, Integer> pnrSegIdFlightSegIdMap = new HashMap<Integer, Integer>();
			Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();
			List<ExternalSegment> newExternalSegments = aaTransferSegmentsRQ.getNewExternalSegment();
			List<ExternalSegment> oldExternalSegments = aaTransferSegmentsRQ.getOldExternalSegment();
			List<Integer> reprotectFligtSegIds = aaTransferSegmentsRQ.getReProtectFltSegIds();
			String visibleIBEOnly = aaTransferSegmentsRQ.getIbeVisibleOnly();

			for (TransferSegment aaTransferSegment : aaTransferSegments) {
				String pnrSegId = aaTransferSegment.getPnrSegmentId();
				String flightSegId = aaTransferSegment.getTargetFlightSegmentId();

				pnrSegIdFlightSegIdMap.put(new Integer(pnrSegId), new Integer(flightSegId));
			}

			for (ExternalSegment externalSegment : newExternalSegments) {
				ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
				externalSegmentTO.setFlightNo(externalSegment.getFlightNumber());
				externalSegmentTO.setDepartureDate(externalSegment.getDepartureDate());
				externalSegmentTO.setDepartureDateZulu(externalSegment.getDepartureDateZulu());
				externalSegmentTO.setArrivalDate(externalSegment.getArrivalDate());
				externalSegmentTO.setArrivalDateZulu(externalSegment.getArrivalDateZulu());
				externalSegmentTO.setSegmentCode(externalSegment.getSegmentCode());
				externalSegmentTO.setCabinClassCode(externalSegment.getCabinClass());
				externalSegmentTO.setLogicalCabinClassCode(externalSegment.getLogicalCabinClass());
				// TODO set return flag and flight ref number and operating carrier
				externalSegmentTO.setOperatingAirline(AAUtils.getCarrierCode(externalSegment.getFlightNumber()));
				externalSegmentTO.setFlightRefNumber(String.valueOf(externalSegment.getFlightSegId()));
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);
				colExternalSegmentTO.add(externalSegmentTO);
			}

			for (ExternalSegment externalSegment : oldExternalSegments) {
				ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
				externalSegmentTO.setFlightNo(externalSegment.getFlightNumber());
				externalSegmentTO.setDepartureDate(externalSegment.getDepartureDate());
				externalSegmentTO.setDepartureDateZulu(externalSegment.getDepartureDateZulu());
				externalSegmentTO.setArrivalDate(externalSegment.getArrivalDate());
				externalSegmentTO.setArrivalDateZulu(externalSegment.getArrivalDateZulu());
				externalSegmentTO.setSegmentCode(externalSegment.getSegmentCode());
				externalSegmentTO.setCabinClassCode(externalSegment.getCabinClass());
				externalSegmentTO.setLogicalCabinClassCode(externalSegment.getLogicalCabinClass());
				// TODO set return flag and flight ref number and operating carrier
				externalSegmentTO.setOperatingAirline(AAUtils.getCarrierCode(externalSegment.getFlightNumber()));
				externalSegmentTO.setFlightRefNumber(String.valueOf(externalSegment.getFlightSegId()));
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
				colExternalSegmentTO.add(externalSegmentTO);
			}

			SegmentBD segmentBD = AAServicesModuleUtils.getSegmentBD();

			ServiceResponce serviceResponse = segmentBD.transferReservationSegment(pnr, pnrSegIdFlightSegIdMap, true, false,
					Long.parseLong(reservationVersion), false, reprotectFligtSegIds, visibleIBEOnly, aaTransferSegmentsRQ.getSalesChannelKey());

			if (colExternalSegmentTO.size() > 0) {
				Reservation tmpReservation = AAReservationUtil.getReservation(pnr, null, false, false, false, false, false,
						false, false);
				segmentBD.changeExternalSegments(pnr, colExternalSegmentTO, tmpReservation.getVersion(), null);
			}

			Collection<String> canceledSegmentRPHs = (Collection<String>) serviceResponse
					.getResponseParam(ResponseCodes.TRANSFERED_RESERVATION_SEGMENTS);

			aaTransferSegmentsRS.getCanceledSegmentRPHs().addAll(canceledSegmentRPHs);

			if (serviceResponse.isSuccess()) {
				aaResponseAttributes.setSuccess(new AASuccess());
			} else {
				throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Transfer segment failed");
			}
		} catch (Exception e) {
			AAExceptionUtil.addAAErrrors(aaTransferSegmentsRS.getResponseAttributes().getErrors(), e);
		}

		return aaTransferSegmentsRS;
	}
}
