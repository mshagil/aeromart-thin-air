package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedFlexi;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

class AAFlexiBL {

	private final AASelectedAncillaryRQ aaSelectedAncillaryRQ;

	private TransactionalReservationProcess tnxResProcess;

	AAFlexiBL(AASelectedAncillaryRQ aaSelectedAncillaryRQ, AASessionManager aaSessionManager) {
		this.aaSelectedAncillaryRQ = aaSelectedAncillaryRQ;
		String transactionId = PlatformUtiltiies.nullHandler((aaSelectedAncillaryRQ != null) ? aaSelectedAncillaryRQ
				.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			this.tnxResProcess = aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	List<FlexiQuotation> getSelectedFlexiDetails(List<SelectedFlexi> selectedFlexis) throws WebservicesException, ModuleException {

		List<FlexiQuotation> flexiQuotation = new ArrayList<FlexiQuotation>();

		Collection<Integer> flexiRuleRateIds = getFlexiRuleRateIds(selectedFlexis);

		Map<String, List<FlexiRuleDTO>> ondFlexiCharges = getOndFlexiRulesForReservation(flexiRuleRateIds);
		for (Entry<String, List<FlexiRuleDTO>> entry : ondFlexiCharges.entrySet()) {
			String ondCode = entry.getKey();

			for (FlexiRuleDTO flexiRuleDTO : entry.getValue()) {
				FlexiQuotation flexiCharge = new FlexiQuotation();
				flexiCharge.setFlexiRuleRateId(flexiRuleDTO.getFlexiRuleRateId());
				flexiCharge.setAdultCharge((flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT)));
				flexiCharge.setChildCharge((flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD)));
				flexiCharge.setInfantCharge((flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT)));
				flexiCharge.setOndCode(ondCode);
				flexiQuotation.add(flexiCharge);
			}

		}

		return flexiQuotation;
	}

	private Collection<Integer> getFlexiRuleRateIds(List<SelectedFlexi> selectedFlexis) {
		Collection<Integer> flexiRuleRateIds = new HashSet<Integer>();

		for (SelectedFlexi selectedFlexi : selectedFlexis) {
			flexiRuleRateIds.addAll(selectedFlexi.getFlexiRateIds());
		}
		return flexiRuleRateIds;
	}

	private Map<String, List<FlexiRuleDTO>> getOndFlexiRulesForReservation(Collection<Integer> flexiRuleRateIds)
			throws WebservicesException, ModuleException {

		if (tnxResProcess == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Transaction not found");
		}

		Map<String, List<FlexiRuleDTO>> allFlexiCharges = new HashMap<String, List<FlexiRuleDTO>>();
		if (tnxResProcess != null) {
			Collection<OndFareDTO> ondFareDTOListForReservation = tnxResProcess
					.getOndFareDTOListForReservation(aaSelectedAncillaryRQ.getSelectedFareTypes());
			for (OndFareDTO ondFareDTO : ondFareDTOListForReservation) {

				String ondCode = ondFareDTO.getOndCode();
				if (!allFlexiCharges.containsKey(ondCode)) {
					allFlexiCharges.put(ondCode, new ArrayList<FlexiRuleDTO>());
				}

				Collection<FlexiRuleDTO> flexiCharges = ondFareDTO.getAllFlexiCharges();
				if (flexiCharges != null) {
					for (FlexiRuleDTO flexiRuleDTO : flexiCharges) {
						for (Integer flexiRuleRateId : flexiRuleRateIds) {
							if (flexiRuleRateId.equals(flexiRuleDTO.getFlexiRuleRateId())) {
								allFlexiCharges.get(ondCode).add(flexiRuleDTO);
							}
						}
					}
				}
			}
		}
		return allFlexiCharges;
	}

}
