package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CreditDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * A utility class for creating the {@code CustomerFulfillment} for a given reservation.
 * 
 * CustomerFulfillment is the object which holds the payment made by each of the {@link ReservationPax} to the carrier.
 * The payment data for the CustomerFulfillment is always collected from the Marketing Carrier.
 * 
 * @author sanjaya
 * 
 */
public class AACustomerFulfillmentPopulator {

	/** The executing airline code */
	private static String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();

	/**
	 * @param reservation
	 *            The {@link Reservation} for the {@link CustomerFulfillment} to be created.
	 * @return The {@link CustomerFulfillment} for the given {@link Reservation}.
	 * 
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static List<TravelerFulfillment> populateCustomerFulfillment(Reservation reservation,
			Map<Integer, Collection<CreditInfoDTO>> paxCredits) throws ModuleException, WebservicesException {
		List<TravelerFulfillment> customerFulfillment = new ArrayList<TravelerFulfillment>();

		// we need to fill the CustomerFulfillment only if payment details in CarrierFulfillment has this carrier as a
		// payment carrier.
		if (isThisCarrierAPaymentCarrier(reservation)) {

			Map<Integer, ReservationPax> paxSeqReservationPaxMap = getPaxSeqReservationPaxMap(reservation);

			Map<Integer, Collection<ReservationTnx>> paxReservationTnxMap = new HashMap<Integer, Collection<ReservationTnx>>();
			Map<Integer, Collection<ReservationPaxExtTnx>> paxReservationExtTnxMap = new HashMap<Integer, Collection<ReservationPaxExtTnx>>();
			Collection<Integer> pnrPaxSequenceList = new ArrayList<Integer>();
			for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
				// Do not process fulfillments for Infants
				if (reservation.isInfantPaymentRecordedWithInfant() || !ReservationApiUtils.isInfantType(reservationPax)) {
					Collection<ReservationTnx> reservationTnxForPax = new ArrayList<ReservationTnx>();

					// we are getting a list of transactions for the passenger which has the pay carrier as this
					// carrier.
					for (ReservationTnx tnx : (Collection<ReservationTnx>) reservationPax.getPaxPaymentTnxView()) {
						if (thisAirlineCode.equals(tnx.getPaymentCarrier())) {
							reservationTnxForPax.add(tnx);
						}
					}
					paxReservationTnxMap.put(reservationPax.getPaxSequence(), reservationTnxForPax);
					pnrPaxSequenceList.add(reservationPax.getPaxSequence());

					if (reservation.isInfantPaymentRecordedWithInfant() || !ReservationApiUtils.isInfantType(reservationPax)) {
						paxReservationExtTnxMap.put(reservationPax.getPaxSequence(),
								reservationPax.getExternalPaxPaymentTnxView());
					}
				}
			}

			// creating the TravelerFulfillment for each of the pax
			for (Integer paxSeq : pnrPaxSequenceList) {
				Collection<ReservationTnx> paxTnxCol = paxReservationTnxMap.get(paxSeq);
				Collection<ReservationPaxExtTnx> paxExtTnxCol = paxReservationExtTnxMap.get(paxSeq);
				ReservationPax pax = paxSeqReservationPaxMap.get(paxSeq);
				TravelerFulfillment travelerFulfillment = populateTravellerFulfillment(paxTnxCol, paxExtTnxCol, pax,
						paxCredits.get(pax.getPnrPaxId()));
				customerFulfillment.add(travelerFulfillment);
			}
		} else if (!paxCredits.isEmpty()) {
			for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
				// Do not process fulfillments for Infants
				if (reservation.isInfantPaymentRecordedWithInfant() || !ReservationApiUtils.isInfantType(reservationPax)) {
					TravelerFulfillment travelerFulfillment = populateTravellerFulfillment(null, null, reservationPax,
							paxCredits.get(reservationPax.getPnrPaxId()));
					customerFulfillment.add(travelerFulfillment);
				}
			}
		}
		return customerFulfillment;
	}

	/**
	 * Populates the {@code TravelerFulfillment} for a give list of {@code ReservationTnx} and
	 * {@code ReservationPaxExtTnx}
	 * 
	 * @param reservationPaxTnxList
	 *            : The list of reservation pax tnx having the payment carrier as this carrier.
	 * @param reservationPaxExtTnxList
	 *            : The list of external carrier transactions for a given carrier.
	 * @param pax
	 *            : The reservation passenger.
	 * 
	 * @return The {@code TravelerFulfillment} for the given pax for the given set of tnxs and ext-tnxs.
	 * @throws WebservicesException
	 */
	private static TravelerFulfillment populateTravellerFulfillment(Collection<ReservationTnx> reservationPaxTnxList,
			Collection<ReservationPaxExtTnx> reservationPaxExtTnxList, ReservationPax pax,
			Collection<CreditInfoDTO> creditInfoDTOs) throws ModuleException, WebservicesException {

		// This is ok as our logic executes on non zero collections.
		if (reservationPaxTnxList == null) {
			reservationPaxTnxList = new ArrayList<ReservationTnx>();
		}
		// This is ok as our logic executes on non zero collections.
		if (reservationPaxExtTnxList == null) {
			reservationPaxExtTnxList = new ArrayList<ReservationPaxExtTnx>();
		}

		if (creditInfoDTOs == null) {
			creditInfoDTOs = new ArrayList<CreditInfoDTO>();
		}

		TravelerFulfillment travelerFulfillment = new TravelerFulfillment();
		travelerFulfillment.setTravelerRefNumber(AAReservationUtil.getTravelerRef(pax));

		if (reservationPaxTnxList.size() > 0) {
			// there are no external charges recorded in this airline for this reservation. Hence get the payment
			// details
			// of the ReservationTnx. This is executed when a reservation made by carrier C1 on itself is loaded by a
			// different carrier.
			// (Loading a normal reservation as a dry booking)
			for (ReservationTnx tnx : reservationPaxTnxList) {
				// captureCustomerPaymentDetails(amount, nominalCode, agentCode, txnDate, paymentDetailsList,
				// ccLast4Digits, authorizationId, paidCurrAmount, paidCurrency, agentName, ccdHolderName)
				captureCustomerPaymentDetails(tnx, travelerFulfillment.getPaymentDetails());
			}

		}

		if (reservationPaxExtTnxList.size() > 0) {
			// There are only external payments
			for (ReservationPaxExtTnx extTnx : reservationPaxExtTnxList) {
				captureCustomerPaymentDetails(extTnx, travelerFulfillment.getPaymentDetails());
			}
		}

		if (!creditInfoDTOs.isEmpty()) {
			travelerFulfillment.getCreditDetails().addAll(getCustomerCreditDetails(creditInfoDTOs));
		}

		return travelerFulfillment;
	}

	/**
	 * Determines if the executing carrier (this carrier) is a payment carrier for the given reservation.
	 * 
	 * The executing carrier becomes a payment carrier if either one of the following conditions are true. 1.) It has
	 * external payment records against the given reservation. 2.) It has pax tnx records with the payment carrier
	 * recorded as the same.
	 * 
	 * @param reservation
	 *            : The reservation.
	 * 
	 * @return: true if this carrier is a payment carrier. false otherwise.
	 */
	private static boolean isThisCarrierAPaymentCarrier(Reservation reservation) {
		boolean isThisCarrierAPaymentCarrier = false;
		for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
			for (ReservationTnx tnx : (Collection<ReservationTnx>) reservationPax.getPaxPaymentTnxView()) {
				if (tnx != null) {
					if (thisAirlineCode.equals(tnx.getPaymentCarrier())) {
						isThisCarrierAPaymentCarrier = true;
						break;
					}
				}
			}

			// or if there are any external payments in this carrier for the given reservation
			// it means that there are customer payments
			if (reservationPax.getExternalPaxPaymentTnxView().size() > 0) {
				isThisCarrierAPaymentCarrier = true;
			}

			// only one occurence of either of the above two
			if (isThisCarrierAPaymentCarrier) {
				break;
			}
		}
		return isThisCarrierAPaymentCarrier;
	}

	/**
	 * Captures payment details for a given tnx record.
	 * 
	 * @param reservationPaxTnx
	 * @param paymentDetailsList
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static void captureCustomerPaymentDetails(ReservationTnx reservationPaxTnx, List<PaymentDetails> paymentDetailsList)
			throws WebservicesException, ModuleException {
		if (reservationPaxTnx != null && paymentDetailsList != null) {

			// if the ReservationPaxExtTnx's LCC unique tnx key is in the paymentDetailsList we don't want to create a
			// new PaymentDetail
			// just add the amounts to the respective PaymentDetails
			boolean tnxMergedWithExisting = AAPaymentDetailsFactory.addTransactionToExisitingPaymentDetail(reservationPaxTnx,
					paymentDetailsList);

			// if there were no PaymentDetails object with the same unique ID, then we have to create a separate payment
			// object for this.
			if (!tnxMergedWithExisting) {
				CurrencyCodeGroup baseCurrencyCodeGroup = FindReservationUtil.getDefaultCurrencyCodeGroup();
				Integer nominalCode = reservationPaxTnx.getNominalCode();
				PaymentDetails paymentDetails = null;
				if (ReservationTnxNominalCode.getCashTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getCashPaymentDetails(reservationPaxTnx, baseCurrencyCodeGroup,
							thisAirlineCode);
				} else if (ReservationTnxNominalCode.getOnAccountTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getOnAccountPaymentDetails(reservationPaxTnx, baseCurrencyCodeGroup,
							thisAirlineCode);
				} else if (ReservationTnxNominalCode.getCreditCardNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getCreditCardPaymentDetails(reservationPaxTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else if (ReservationTnxNominalCode.getCreditTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getPaxCreditPaymentDetails(reservationPaxTnx, baseCurrencyCodeGroup,
							thisAirlineCode);
				} else if (ReservationTnxNominalCode.getBSPAccountTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getBSPAccountPaymentDetails(reservationPaxTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else if (ReservationTnxNominalCode.getLoyaltyNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getLoyaltyPaymentDetails(reservationPaxTnx, baseCurrencyCodeGroup,
							thisAirlineCode);
				} else if (ReservationTnxNominalCode.getVoucherNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getVoucherPaymentDetails(reservationPaxTnx, baseCurrencyCodeGroup,
							thisAirlineCode);
				} else {
					throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE,
							"Unrecognizable nominal code passed to AASearchReservationBL.capturePaymentDetails");
				}

				if (paymentDetails != null) {
					paymentDetailsList.add(paymentDetails);
				}
			}
		}
	}

	/**
	 * Captures the payment details for a given external tnx record.
	 * 
	 * @param reservationPaxExtTnx
	 * @param paymentDetailsList
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static void captureCustomerPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx,
			List<PaymentDetails> paymentDetailsList) throws WebservicesException, ModuleException {
		if (reservationPaxExtTnx != null && paymentDetailsList != null) {

			// if the ReservationPaxExtTnx's LCC unique tnx key is in the paymentDetailsList we don't want to create a
			// new PaymentDetail
			// just add the amounts to the respective PaymentDetails
			boolean extTnxMergedWithExisting = AAPaymentDetailsFactory.addExtTransactionToExisitingPaymentDetail(
					reservationPaxExtTnx, paymentDetailsList);

			// if there were no PaymentDetails object with the same unique ID, then we have to create a separate payment
			// object for this.
			if (!extTnxMergedWithExisting) {
				CurrencyCodeGroup baseCurrencyCodeGroup = FindReservationUtil.getDefaultCurrencyCodeGroup();
				Integer nominalCode = reservationPaxExtTnx.getNominalCode();
				PaymentDetails paymentDetails = null;
				if (ReservationTnxNominalCode.getCashTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getCashPaymentDetails(reservationPaxExtTnx, baseCurrencyCodeGroup,
							thisAirlineCode);
				} else if (ReservationTnxNominalCode.getOnAccountTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getOnAccountPaymentDetails(reservationPaxExtTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else if (ReservationTnxNominalCode.getCreditCardNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getCreditCardPaymentDetails(reservationPaxExtTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else if (ReservationTnxNominalCode.getCreditTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getPaxCreditPaymentDetails(reservationPaxExtTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else if (ReservationTnxNominalCode.getBSPAccountTypeNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getBSPAccountPaymentDetails(reservationPaxExtTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else if (ReservationTnxNominalCode.getVoucherNominalCodes().contains(nominalCode)) {
					paymentDetails = AAPaymentDetailsFactory.getVoucherPaymentDetails(reservationPaxExtTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
				} else {
					throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE,
							"Unrecognizable nominal code passed to AASearchReservationBL.capturePaymentDetails");
				}

				if (paymentDetails != null) {
					paymentDetailsList.add(paymentDetails);
				}
			}
		}
	}

	public static List<CreditDetails> getCustomerCreditDetails(Collection<CreditInfoDTO> creditInfoDTOs) {
		List<CreditDetails> creditDetailsList = new ArrayList<CreditDetails>();
		if (creditInfoDTOs != null) {
			for (CreditInfoDTO creditInfoDTO : creditInfoDTOs) {
				CreditDetails creditDetails = new CreditDetails();
				creditDetails.setAmount(creditInfoDTO.getAmount());
				creditDetails.setCreditId(creditInfoDTO.getCreditId());
				creditDetails.setExpireDate(creditInfoDTO.getExpireDate());
				creditDetails.setStatus(AAUtils.convertAACreditStatusToLCCCreditStatus(creditInfoDTO.getEnumStatus()));
				creditDetails.setTxnId(creditInfoDTO.getTxnId());
				creditDetails.setUserNote(creditInfoDTO.getUserNote());
				creditDetails.setCarrierCode(creditInfoDTO.getCarrierCode());
				creditDetails.setCurrencyCode(creditInfoDTO.getCurrencyCode());
				creditDetails.setLccUniqueTnx(creditInfoDTO.getLccUniqueTnxId());
				creditDetails.setNonRefundableCredit(creditInfoDTO.isNonRefundableCredit());
				creditDetailsList.add(creditDetails);
			}
		}
		return creditDetailsList;
	}

	/**
	 * Gets the Map of Pax with their sequence values as the key.
	 * 
	 * @param reservation
	 *            The {@code Reservation}
	 * 
	 * @return
	 */
	private static Map<Integer, ReservationPax> getPaxSeqReservationPaxMap(Reservation reservation) {
		Map<Integer, ReservationPax> paxSeqReservationPaxMap = new HashMap<Integer, ReservationPax>();

		for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
			paxSeqReservationPaxMap.put(reservationPax.getPaxSequence(), reservationPax);
		}

		return paxSeqReservationPaxMap;
	}

}
