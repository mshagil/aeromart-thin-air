package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExpectedBookingStates;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.CreditCardDetail;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.ExternalSegmentUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * TODO handle for modification
 * 
 * @author mekanayake
 * 
 */
public class AAExtCarrierFulfillmentUtil {

	private static Log log = LogFactory.getLog(AAExtCarrierFulfillmentUtil.class);

	/**
	 * Extract new external segments and update old ones
	 * 
	 * @param reservation
	 * @param aaairBookRQ
	 * @return new externalPnrSegments that need to be added
	 * @throws ModuleException
	 */
	public static Set<ExternalPnrSegment> syncExtSegments(Reservation reservation,
			List<OriginDestinationOption> originDestinationOptions) throws ModuleException {

		Map<Integer, Collection<ExternalPnrSegment>> perFlightExtPnrSegments = new HashMap<Integer, Collection<ExternalPnrSegment>>();
		Set<ExternalPnrSegment> externalPnrSegments = new HashSet<ExternalPnrSegment>();

		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();

		// for bus segments
		Collection<String> carrierCodes = AAServicesModuleUtils.getReservationBD().getCarrierCodesByAirline(
				AppSysParamsUtil.getDefaultAirlineIdentifierCode());

		Set<String> externalCarriers = new HashSet<String>();
		if (originDestinationOptions != null && !originDestinationOptions.isEmpty()) {

			SegmentBD segmentBD = AAServicesModuleUtils.getSegmentBD();

			for (OriginDestinationOption originDestinationOptionType : originDestinationOptions) {

				for (FlightSegment bookFlightSegment : originDestinationOptionType.getFlightSegment()) {

					if (!defaultCarrier.equals(bookFlightSegment.getOperatingAirline())
							&& !carrierCodes.contains(bookFlightSegment.getOperatingAirline())) {

						externalCarriers.add(bookFlightSegment.getOperatingAirline());

						Integer extFlightSegRef = FlightRefNumberUtil.getSegmentIdFromFlightRPH(bookFlightSegment
								.getFlightRefNumber());
						// String fltSeg = bookFlightSegment.getDepartureAirportCode() + "/" +
						// bookFlightSegment.getArrivalAirportCode();

						ExternalFlightSegment externalFlightSegment = segmentBD.getExternalFlightSegment(
								bookFlightSegment.getOperatingAirline(), extFlightSegRef);

						if (externalFlightSegment == null) {
							externalFlightSegment = new ExternalFlightSegment();
							if (log.isDebugEnabled()) {
								log.debug("Adding new ExternalFlightSegment FlightSegRef = [" + extFlightSegRef
										+ "] segmentCode = [" + bookFlightSegment.getSegmentCode() + "]");
							}
						}

						externalFlightSegment.setExternalFlightSegRef(extFlightSegRef);
						externalFlightSegment.setFlightNumber(bookFlightSegment.getFlightNumber());
						externalFlightSegment.setSegmentCode(bookFlightSegment.getSegmentCode());
						externalFlightSegment.setExternalCarrierCode(bookFlightSegment.getOperatingAirline());
						String flightStatus = (bookFlightSegment.getFlightStatus() == null ? "OPN" : bookFlightSegment
								.getFlightStatus());
						externalFlightSegment.setStatus(flightStatus);

						externalFlightSegment.setEstTimeArrivalLocal(bookFlightSegment.getArrivalDateTime());
						externalFlightSegment.setEstTimeDepatureLocal(bookFlightSegment.getDepatureDateTime());

						Date arrivalDateTimeZulu = (bookFlightSegment.getArrivalDateTimeZulu() != null ? bookFlightSegment
								.getArrivalDateTimeZulu() : bookFlightSegment.getArrivalDateTime());
						Date depatureDateTimeZulu = (bookFlightSegment.getDepatureDateTimeZulu() != null ? bookFlightSegment
								.getDepatureDateTimeZulu() : bookFlightSegment.getDepatureDateTime());
						externalFlightSegment.setEstTimeArrivalZulu(arrivalDateTimeZulu);
						externalFlightSegment.setEstTimeDepatureZulu(depatureDateTimeZulu);

						// save or update external flight segment
						externalFlightSegment = segmentBD.saveExternalFlightSegment(externalFlightSegment);

						Collection<ExternalPnrSegment> pnrSegments = perFlightExtPnrSegments.get(extFlightSegRef);
						if (pnrSegments == null || pnrSegments.isEmpty()) {
							pnrSegments = new ArrayList<ExternalPnrSegment>();
						}

						ExternalPnrSegment externalPnrSegment = new ExternalPnrSegment();
						externalPnrSegment.setExternalCarrierCode(bookFlightSegment.getOperatingAirline());
						if (bookFlightSegment.getBookingFlightRefNumber() != null) {
							externalPnrSegment
									.setExternalPnrSegRef(Integer.valueOf(bookFlightSegment.getBookingFlightRefNumber()));
						}

						externalPnrSegment.setCabinClassCode(bookFlightSegment.getCabinClassCode());
						externalPnrSegment.setLogicalCabinClassCode(bookFlightSegment.getLogicalCabinClassCode());
						externalPnrSegment.setSegmentSeq(bookFlightSegment.getSegmentSequence());
						externalPnrSegment.setStatus(bookFlightSegment.getStatus().value());
						externalPnrSegment.setExternalFltSegId(externalFlightSegment.getExternalFlightSegId());
						externalPnrSegment.setExternalFlightSegment(externalFlightSegment);

						externalPnrSegment.setMarketingAgentCode(bookFlightSegment.getMarketingAgentCode());
						externalPnrSegment.setMarketingCarrierCode(bookFlightSegment.getMarketingCarrierCode());
						externalPnrSegment.setMarketingStationCode(bookFlightSegment.getMarketingStationCode());
						externalPnrSegment.setStatusModifiedSalesChannelCode(bookFlightSegment.getStatusModifiedChannelCode());
						externalPnrSegment.setStatusModifiedDate(bookFlightSegment.getStatusModifiedDate());
						externalPnrSegment.setSubStatus(bookFlightSegment.getSubStatus());

						pnrSegments.add(externalPnrSegment);
						perFlightExtPnrSegments.put(extFlightSegRef, pnrSegments);
						externalPnrSegments.add(externalPnrSegment);
					}
				}
			}
		}

		if (reservation == null || reservation.getExternalReservationSegment() == null
				|| reservation.getExternalReservationSegment().isEmpty()) {
			// add all external pnr segments to the reservation
			return externalPnrSegments;
		} else {

			Set<ExternalPnrSegment> newExtSegments = new HashSet<ExternalPnrSegment>();

			Map<String, Collection<ExternalPnrSegment>> serverSegmentMap = ExternalSegmentUtil
					.getConfirmedReservationExternalSegmentInAMap(reservation.getExternalReservationSegment());

			for (ExternalPnrSegment externalPnrSegment : externalPnrSegments) {
				if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED
						.equals(externalPnrSegment.getStatus())) {
					if (!serverSegmentMap.containsKey(externalPnrSegment.getUniqueSegmentKey())) {
						newExtSegments.add(externalPnrSegment);
					}
				}
			}

			return newExtSegments;
		}
	}

	/**
	 * Return per pax sequence wise external reservation map
	 * 
	 * @param reservation
	 * @param newExtFulfillment
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, Collection<ReservationPaxExtTnx>> buildExternalPaymentFulfillment(Reservation reservation,
			List<TravelerFulfillment> newExtFulfillment) throws ModuleException {

		// Get the existing external pax tnx for a given reservation
		Map<Integer, Collection<ReservationPaxExtTnx>> existingPaxExtPayment = new HashMap<Integer, Collection<ReservationPaxExtTnx>>();
		if (reservation != null) {
			for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
				Collection<ReservationPaxExtTnx> existingExtTnxs = new ArrayList<ReservationPaxExtTnx>();
				for (ReservationPaxExtTnx paxExtTnx : (Collection<ReservationPaxExtTnx>) reservationPax
						.getExternalPaxPaymentTnxView()) {
					existingExtTnxs.add(paxExtTnx);
				}
				existingPaxExtPayment.put(reservationPax.getPaxSequence(), existingExtTnxs);
			}
		}

		Map<Integer, Collection<ReservationPaxExtTnx>> perPaxExternalPaymentTobeAdded = new HashMap<Integer, Collection<ReservationPaxExtTnx>>();
		if (newExtFulfillment != null && !newExtFulfillment.isEmpty()) {
			for (TravelerFulfillment newTravelerFulfillment : newExtFulfillment) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(newTravelerFulfillment.getTravelerRefNumber());

				Collection<ReservationPaxExtTnx> existingExtTnxs = existingPaxExtPayment.get(paxSeq);
				Collection<ReservationPaxExtTnx> newExtTnxs = new ArrayList<ReservationPaxExtTnx>();
				if (existingExtTnxs == null || existingExtTnxs.isEmpty()) {
					for (PaymentDetails paymentDetail : newTravelerFulfillment.getPaymentDetails()) {
						ReservationPaxExtTnx reservationPaxExtTnx = transform(paymentDetail, existingExtTnxs);
						newExtTnxs.add(reservationPaxExtTnx);
					}
					perPaxExternalPaymentTobeAdded.put(paxSeq, newExtTnxs);
				} else {
					for (PaymentDetails newPaymentDetail : newTravelerFulfillment.getPaymentDetails()) {
						// add only the new external tnx
						if (!addedExtTnx(newPaymentDetail, existingExtTnxs)) {
							ReservationPaxExtTnx reservationPaxExtTnx = transform(newPaymentDetail, existingExtTnxs);
							newExtTnxs.add(reservationPaxExtTnx);
						}
					}

					if (!newExtTnxs.isEmpty()) {
						perPaxExternalPaymentTobeAdded.put(paxSeq, newExtTnxs);
					}
				}
			}
		}
		return perPaxExternalPaymentTobeAdded;
	}

	public static AAAirBookRQ buildDummyRes(AAReservation aaReservation) {

		AirReservation airReservation = aaReservation.getAirReservation();

		AAAirBookRQ airBookRQ = new AAAirBookRQ();
		airBookRQ.setPnr(aaReservation.getGroupPnr());
		airBookRQ.setGroupPnr(aaReservation.getGroupPnr());
		airBookRQ.setAaPos(new AAPOS());
		airBookRQ.setFulfillment(new Fulfillment());
		airBookRQ.setExpectedBookingStates(ExpectedBookingStates.DUMMY);
		airBookRQ.setCapturePayments(false);
		airBookRQ.setAirItineraryPricingInfo(airReservation.getPriceInfo());
		airBookRQ.setTravelerInfo(convertTravelerRef(airReservation.getTravelerInfo()));
		airBookRQ.setContactInfo(airReservation.getContactInfo());
		airBookRQ.setAirItinerary(airReservation.getAirItinerary());

		return airBookRQ;
	}

	public static void updateReservationsStatus(Reservation reservation, AAReservation aaAirReservation) {
		if (aaAirReservation.getAirReservation().getStatus() != null
				&& !aaAirReservation.getAirReservation().getStatus().isEmpty()) {
			reservation.setStatus(aaAirReservation.getAirReservation().getStatus());
		}

		if (aaAirReservation.getAirReservation().getTravelerInfo() != null) {
			for (ReservationPax pax : (Collection<ReservationPax>) reservation.getPassengers()) {
				for (AirTraveler airTraveler : aaAirReservation.getAirReservation().getTravelerInfo().getAirTraveler()) {
					if (pax.getPaxSequence().equals(airTraveler.getTravelerSequence())) {
						pax.setStatus(airTraveler.getStatus());
						break; // exiting form inner loop
					}
				}
			}
		}
	}

	/**
	 * combine the own and external ond options
	 * 
	 * @param airReservation
	 * @return
	 */
	public static List<OriginDestinationOption> getOwnAndExternalOndOptions(AirReservation airReservation) {
		List<OriginDestinationOption> sum = new ArrayList<OriginDestinationOption>();

		List<OriginDestinationOption> ondOptions = airReservation.getAirItinerary().getOriginDestinationOption();
		List<OriginDestinationOption> extONDOption = airReservation.getExternalAirItinerary().getOriginDestinationOption();

		if (ondOptions != null) {
			sum.addAll(ondOptions);
		}

		if (extONDOption != null) {
			sum.addAll(extONDOption);
		}

		return sum;
	}

	/**
	 * convert the travelerRef to RPH. Eg : A1$12345667 --> A1
	 * 
	 * @param travelerRefNumber
	 * @return
	 */
	private static TravelerInfo convertTravelerRef(TravelerInfo travelerInfo) {

		for (AirTraveler traveler : travelerInfo.getAirTraveler()) {
			traveler.setTravelerRefNumber(PaxTypeUtils.getRPHFromTraverRef(traveler.getTravelerRefNumber()));
		}
		return travelerInfo;
	}

	/**
	 * check if the external tnx is already added
	 * 
	 * @param paymentDetail
	 * @param existingExtTnxs
	 * @return
	 */
	private static boolean addedExtTnx(PaymentDetails paymentDetail, Collection<ReservationPaxExtTnx> existingExtTnxs) {
		boolean found = false;
		for (ReservationPaxExtTnx paxExtTnx : existingExtTnxs) {
			if (paxExtTnx.getLccUniqueId().equals(paymentDetail.getUniqueTnxId())
					&& paxExtTnx.getExternalCarrierCode().equals(paymentDetail.getCarrierCode())) {
				found = true;
				break;
			}
		}

		return found;
	}

	private static ReservationPaxExtTnx transform(PaymentDetails paymentDetail, Collection<ReservationPaxExtTnx> existingExtTnxs)
			throws ModuleException {

		PaymentSummary paySummary = paymentDetail.getPaymentSummary();
		String tnxType = ReservationInternalConstants.TnxTypes.DEBIT;
		Integer nominalCode = Integer.valueOf(paymentDetail.getNorminalCode());
		if (ReservationTnxNominalCode.getPaymentTypeNominalCodes().contains(nominalCode)) {
			tnxType = ReservationInternalConstants.TnxTypes.CREDIT;
		}
		ReservationPaxExtTnx extTnx = new ReservationPaxExtTnx();
		extTnx.setExternalCarrierCode(paymentDetail.getCarrierCode());// OpCarrier code
		extTnx.setLccUniqueId(paymentDetail.getUniqueTnxId());
		extTnx.setNominalCode(nominalCode);
		extTnx.setTnxType(tnxType);
		extTnx.setAmount(paymentDetail.getPaymentCarrierBaseAmount().getPaymentCarrierBaseAmount().negate());
		extTnx.setAmountExtBase(paySummary.getPaymentAmount().negate());
		extTnx.setAmountPayCur(paymentDetail.getPaymentCurrencyAmount().getPaymentCurrencyAmount().negate());
		extTnx.setPayCurrencyCode(paymentDetail.getPaymentCurrencyAmount().getCurrencyCodeGroup().getCurrencyCode());
		extTnx.setDateTime(paySummary.getApplicableDateTime());
		extTnx.setSalesChannelCode(paymentDetail.getSalesChannelCode());
		extTnx.setAgentCode(paymentDetail.getAgentCode());
		extTnx.setUserId(paymentDetail.getUserId());
		extTnx.setRemarks(paymentDetail.getRemarks());
		// If bsp payment only
		if (ReservationTnxNominalCode.getBSPAccountTypeNominalCodes().contains(nominalCode)) {
			extTnx.setFirstPayment(paymentDetail.isFirstPayment() ? CommonsConstants.YES : CommonsConstants.NO);
		}

		if (paymentDetail.getPaymentCard() != null) {
			CommonCreditCardPayment cardPayment = paymentDetail.getPaymentCard();
			CreditCardDetail cardDetail = new CreditCardDetail();
			cardDetail.setAddress(cardPayment.getAddress());
			cardDetail.setNo(cardPayment.getNo());
			cardDetail.setName(cardPayment.getCardHolderName());
			cardDetail.setEDate(cardPayment.getEDate());
			cardDetail.setSecurityCode(cardPayment.getSecurityCode());
			cardDetail.setType(AAUtils.getCardType(cardPayment));
			if (cardPayment.getPaymentBrokerId() != null) {
				cardDetail.setPaymentBrokerRefNo(cardPayment.getPaymentBrokerId());
			}
			cardDetail.setNoFirstDigits(cardPayment.getFirst4Digits());
			cardDetail.setAuthorizationId(cardPayment.getAuthorizationId());
			extTnx.setCreditCardDetail(cardDetail);
		}

		if (paymentDetail.getVoucherPayment() != null && paymentDetail.getVoucherPayment().getVoucherID() != null) {
			extTnx.setExternalReference(paymentDetail.getVoucherPayment().getVoucherID());
		}

		// we need to link the original external payment tnx id with refunds
		if (!StringUtils.isEmpty(paymentDetail.getOriginalPaymentUID())) {
			ReservationPaxExtTnx selectedPayment = null;
			if (existingExtTnxs != null) {
				for (ReservationPaxExtTnx exTnx : existingExtTnxs) {
					if (exTnx.getExternalCarrierCode().equals(paymentDetail.getCarrierCode())
							&& exTnx.getLccUniqueId().equals(paymentDetail.getOriginalPaymentUID())) {
						selectedPayment = exTnx;
						break;
					}
				}
				extTnx.setOriginalExtPaymentTnxID(selectedPayment.getPaxExternalTnxId());
			}
		}

		return extTnx;
	}

	/**
	 * Injects the marketing credentials to the Flight Segment
	 * 
	 * @param ondOptionList
	 * @param trackInfoDTO
	 * @param userPrincipal
	 */
	public static void injectCredentialInfomationToFlightSegment(List<OriginDestinationOption> ondOptionList,
			TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal) {
		if (ondOptionList != null && trackInfoDTO != null) {
			for (OriginDestinationOption ondOption : ondOptionList) {
				if (ondOption != null && ondOption.getFlightSegment() != null) {
					for (FlightSegment flightSegment : ondOption.getFlightSegment()) {
						flightSegment.setMarketingAgentCode(trackInfoDTO.getMarketingAgentCode());
						flightSegment.setMarketingCarrierCode(trackInfoDTO.getCarrierCode());
						flightSegment.setMarketingStationCode(trackInfoDTO.getMarketingAgentStationCode());
						flightSegment.setStatusModifiedChannelCode(new Integer(userPrincipal.getSalesChannel()));
						flightSegment.setStatusModifiedDate(new Date());
					}
				}
			}
		}
	}

	public static ReservationAdminInfoTO buildReservationAdminInfoTO(AAReservation aaAirReservation) {
		ReservationAdminInfoTO adminInfoTO = new ReservationAdminInfoTO();
		AdminInfo adminInfo = aaAirReservation.getAirReservation().getAirReservationSummary().getAdminInfo();
		adminInfoTO.setOwnerChannelId(Integer.valueOf(adminInfo.getOwnerChannelID()));
		adminInfoTO.setOverrideOwnerChannelId(true);
		
		boolean isChannelWebIosOrAndroid = SalesChannelsUtil.isSalesChannelWebOrMobile(Integer.valueOf(adminInfo.getOriginChannelID()));
		
		if ((adminInfo.getOwnerAgentCode() != null && !adminInfo.getOwnerAgentCode().isEmpty())) {
			adminInfoTO.setOverrideOwnerAgentCode(true);
			adminInfoTO.setOwnerAgentCode(adminInfo.getOwnerAgentCode());
		}
		
		if ((adminInfo.getOriginAgentCode() != null && !adminInfo.getOriginAgentCode().isEmpty())) {
			adminInfoTO.setOriginAgentCode(adminInfo.getOriginAgentCode());
			adminInfoTO.setOverrideOriginAgentCode(true);

		} else if ((isChannelWebIosOrAndroid && (adminInfo.getOriginAgentCode() == null || adminInfo
				.getOriginAgentCode().isEmpty()))) {
			adminInfoTO.setOriginAgentCode(null);
			adminInfoTO.setOverrideOriginAgentCode(true);
			adminInfoTO.setOverrideOriginChannelId(true);
			adminInfoTO.setOriginChannelId(Integer.valueOf(adminInfo.getOriginChannelID()));
		}

		if ((adminInfo.getOriginUserID() != null && !adminInfo.getOriginUserID().isEmpty())) {
			adminInfoTO.setOriginUserId(adminInfo.getOriginUserID());
			adminInfoTO.setOverrideOriginUserId(true);
		} else if ((isChannelWebIosOrAndroid && (adminInfo.getOriginUserID() == null || adminInfo
				.getOriginUserID().isEmpty()))) {
			adminInfoTO.setOriginUserId(null);
			adminInfoTO.setOverrideOriginUserId(true);
			adminInfoTO.setOverrideOriginChannelId(true);
			adminInfoTO.setOriginChannelId(Integer.valueOf(adminInfo.getOriginChannelID()));
		}
		return adminInfoTO;
	}
	
	
	public static void updateDummyPNRPAXDetails(Reservation reservation, AAReservation newAAAirReservation,
			boolean isFromNameChange) {

		// update dummy PNR PAX details in case of name change request.
		if (isFromNameChange && reservation != null && reservation.getPassengers() != null
				&& !reservation.getPassengers().isEmpty() && newAAAirReservation.getAirReservation().getTravelerInfo() != null
				&& newAAAirReservation.getAirReservation().getTravelerInfo().getAirTraveler() != null
				&& !newAAAirReservation.getAirReservation().getTravelerInfo().getAirTraveler().isEmpty()) {

			Iterator<AirTraveler> airTravelerItr = newAAAirReservation.getAirReservation().getTravelerInfo().getAirTraveler()
					.iterator();
			while (airTravelerItr.hasNext()) {
				AirTraveler airTraveler = airTravelerItr.next();
				if (airTraveler != null) {
					for (ReservationPax reservationPax : reservation.getPassengers()) {
						if (airTraveler.getTravelerSequence().intValue() == reservationPax.getPaxSequence().intValue()
								&& airTraveler.getPersonName() != null) {
							if (!StringUtil.isNullOrEmpty(airTraveler.getPersonName().getTitle())) {
								reservationPax.setTitle(airTraveler.getPersonName().getTitle());
							}
							if (!StringUtil.isNullOrEmpty(airTraveler.getPersonName().getFirstName())) {
								reservationPax.setFirstName(airTraveler.getPersonName().getFirstName());
							}

							if (!StringUtil.isNullOrEmpty(airTraveler.getPersonName().getSurName())) {
								reservationPax.setLastName(airTraveler.getPersonName().getSurName());
							}

						}

					}
				}

			}
		}

	}
}
