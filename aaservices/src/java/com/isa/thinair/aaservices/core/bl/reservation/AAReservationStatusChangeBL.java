package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.log4j.Logger;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

class AAReservationStatusChangeBL {

	private static Logger logger = Logger.getLogger(AAReservationStatusChangeBL.class);

	static void execute(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ) throws Exception {

		try {
			String newReservationStatus = aaAirBookModifyRQ.getAaReservation().getAirReservation().getStatus();

			if (newReservationStatus == null
					|| !(ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(newReservationStatus) || ReservationInternalConstants.ReservationStatus.CANCEL
							.equals(newReservationStatus))) {
				throw new WebservicesException(AAErrorCode.ERR_57_MAXICO_REQUIRED_INVALID_RESERVATION_STATUS,
						"Reservation status is invalid: " + reservation.getPnr());
			}

			AAServicesModuleUtils.getReservationBD().updateReservatonStatus(reservation, newReservationStatus);

		} catch (Exception e) {
			logger.error("Error in AAReservatoinStatusChangeBL.execute", e);
			throw e;
		}
	}

}
