package com.isa.thinair.aaservices.core.bl.alerting;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AARemoveAlertsRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AARemoveAlertsRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * The alert remove logic.
 * 
 * @author sanjaya
 * 
 */
public class AARemoveAlertsBL {

	/* Logger */
	private static Log log = LogFactory.getLog(AARemoveAlertsBL.class);

	/* The request */
	private AARemoveAlertsRQ aARemoveAlertsRQ;

	/* New Alert removal response */
	private AARemoveAlertsRS aARemoveAlertsRS;

	/**
	 * @param aARemoveAlertsRQ
	 *            The request param to set.
	 */
	public void setRequestParams(AARemoveAlertsRQ aARemoveAlertsRQ) {
		this.aARemoveAlertsRQ = aARemoveAlertsRQ;
	}

	// Initializes the response variable.
	private void initializeAARemoveAlertsRS() {
		aARemoveAlertsRS = new AARemoveAlertsRS();
		aARemoveAlertsRS.setResponseAttributes(new AAResponseAttributes());
	}

	/**
	 * @return The alert removal response.
	 */
	public AARemoveAlertsRS excute() {

		initializeAARemoveAlertsRS();

		try {
			if (aARemoveAlertsRQ.getAlertIdList() != null) {
				AAServicesModuleUtils.getSegmentBD().clearAlerts(aARemoveAlertsRQ.getAlertIdList());
				return this.aARemoveAlertsRS;
			} else {
				throw new ModuleException("The alert ID list to be removed is null");
			}

		} catch (Exception ex) {
			log.error("Error occurred during removal of alerts with IDs :" + aARemoveAlertsRQ.getAlertIdList(), ex);
			AAExceptionUtil.addAAErrrors(aARemoveAlertsRS.getResponseAttributes().getErrors(), ex);

		} finally {
			int numOfErrors = aARemoveAlertsRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aARemoveAlertsRS.getResponseAttributes().setSuccess(successType);
		}

		return aARemoveAlertsRS;
	}
}
