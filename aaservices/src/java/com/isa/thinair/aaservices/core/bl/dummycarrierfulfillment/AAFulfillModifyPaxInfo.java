package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.UserNote;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Handle pax info modifications to the dummy reservations
 * 
 * @author malaka
 * 
 */
class AAFulfillModifyPaxInfo extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillModifyPaxInfo.class);

	AAFulfillModifyPaxInfo(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillModifyPaxInfo::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			if (reservation == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			TravelerInfo travelerInfo = aaAirReservation.getAirReservation().getTravelerInfo();
			UserNote lastUserNote = aaAirReservation.getAirReservation().getLastUserNote();
			String originCountryOfCall = aaAirReservation.getAirReservation().getOriginCountryOfCall();
			// update pax info
			AAUtils.updatePaxInfo(travelerInfo, reservation.getPassengers());

			// update user notes
			if (lastUserNote != null) {
				reservation.setUserNote(lastUserNote.getNoteText());
			}
			reservation.setOriginCountryOfCall(originCountryOfCall);
			ServiceResponce updateReservation = AAServicesModuleUtils.getReservationBD()
					.updateDummyReservation(reservation, null);
			Long version = (Long) updateReservation.getResponseParam(CommandParamNames.VERSION);

			if (updateReservation.isSuccess()) {

				if (log.isDebugEnabled()) {
					log.debug("AAFulfillModifyPaxInfo::end");
				}

				carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
				if(version != null){
					carrierFulfillment.setResVersion(version.toString());
				}
			}

		} catch (ModuleException e) {
			log.error("dummy reservation AAFulfillModifyPaxInfo failed", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {

		AirReservation airReservation = aaAirReservation.getAirReservation();

		if (airReservation.getTravelerInfo() == null || airReservation.getLastUserNote() == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}

	}

}
