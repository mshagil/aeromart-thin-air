package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingMealRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Meal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedMeal;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

class AAMealsBL implements TransactionalReservationProcessBL {

	private static final Log log = LogFactory.getLog(AAMealsBL.class);

	private TransactionalReservationProcess transactionalReservationProcess;

	private String pnr;

	List<FlightSegmentMeals> execute(BookingMealRequest mealRequest, List<BookingFlightSegment> bookingFlightSegments,
			String selectedLangauge, Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, AAPOS aapos)
			throws ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("execute method called");
		}

		List<FlightSegmentMeals> flightSegmentMeals = new ArrayList<FlightSegmentMeals>();

		Set<BookingFlightSegment> bkFltSegs = AncillaryUtil.getFlightSegmentSet(bookingFlightSegments,
				mealRequest.getFlightRefNumberRPHList());

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCOS = AncillaryUtil.getFlightSegmentIdWiseClassOfServices(bkFltSegs);

		Map<Integer, List<FlightMealDTO>> mealMap = null;

		int salesChannelCode = Integer.valueOf(AAUtils.getSalesChannelFromPOS(aapos));

		// TODO handle the scenario where the requested language is not available
		if (selectedLangauge != null && !selectedLangauge.trim().equals("") && !selectedLangauge.trim().equalsIgnoreCase("en")) {
			mealMap = AAServicesModuleUtils.getMealBD().getMealsWithTranslations(flightSegIdWiseCOS, selectedLangauge,
					flightSegIdWiseSelectedMeals, salesChannelCode);
		} else {
			mealMap = AAServicesModuleUtils.getMealBD().getMeals(flightSegIdWiseCOS, flightSegIdWiseSelectedMeals, false, null,
					salesChannelCode);
		}

		List<FlightSegmentMeals> flightSegmentMeal = AncillaryUtil.getFlightSegmentMeals(bkFltSegs, mealMap);

		setMealOffers(flightSegmentMeal, selectedLangauge, aapos.getMarketingBookingChannel(), flightSegIdWiseSelectedMeals, salesChannelCode);

		flightSegmentMeals.addAll(flightSegmentMeal);

		return flightSegmentMeals;
	}

	private void setMealOffers(List<FlightSegmentMeals> segmentMeals, String selectedLanguage, BookingSystems appEngine,
			Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, int salesChannelCode) throws ModuleException {

		Map<String, BundledFareDTO> ondBundledFares = AncillaryUtil.getSegmentCodeWiseBundledFares(
				transactionalReservationProcess, pnr, null);
		Map<String, Integer> ondBundledFareMealTemplates = AncillaryUtil.getOndBundledFareServiceTemplateIds(ondBundledFares,
				EXTERNAL_CHARGES.MEAL.toString());
		Map<String, Boolean> ondBundleFreeServiceMultiSelection = AncillaryUtil.getOndBundledFareServiceMultipleSelection(
				ondBundledFares, EXTERNAL_CHARGES.MEAL.toString());
		boolean isBundledFareSelected = AncillaryUtil.isAtleastOneBundledFareTemplateExists(transactionalReservationProcess, pnr,
				null);
		Collection<BookingFlightSegment> bookingFlightSegments = AncillaryUtil.getFlightSegmentSetFromMeal(segmentMeals);

		for (FlightSegmentMeals flightSegmentMeal : segmentMeals) {
			BookingFlightSegment bookingFlightSegment = flightSegmentMeal.getFlightSegment();
			if (isBundledFareSelected) {
				Boolean isAllowMultiSelect = null;
				if (ondBundleFreeServiceMultiSelection != null) {
					isAllowMultiSelect = ondBundleFreeServiceMultiSelection.get(bookingFlightSegment.getSegmentCode());
				}
				if (ondBundledFareMealTemplates != null
						&& ondBundledFareMealTemplates.containsKey(bookingFlightSegment.getSegmentCode())
						&& ondBundledFareMealTemplates.get(bookingFlightSegment.getSegmentCode()) != null) {

					Integer bundledFareTemplateId = ondBundledFareMealTemplates.get(bookingFlightSegment.getSegmentCode());

					List<FlightMealDTO> bundledFareMeals = AAServicesModuleUtils.getMealBD().getMealsByTemplate(
							bundledFareTemplateId,
							bookingFlightSegment.getCabinClassCode(),
							bookingFlightSegment.getLogicalCabinClassCode(),
							selectedLanguage,
							Integer.valueOf(bookingFlightSegment.getFlightRefNumber()),
							flightSegIdWiseSelectedMeals == null ? null : flightSegIdWiseSelectedMeals.get(Integer
									.valueOf(bookingFlightSegment.getFlightRefNumber())), salesChannelCode);

					if (!bundledFareMeals.isEmpty()) {
						flightSegmentMeal.getMeal().clear();
						flightSegmentMeal.getMeal().addAll(AncillaryUtil.getMealCollectionMealDTO(bundledFareMeals));
					}

					if (isAllowMultiSelect != null) {
						flightSegmentMeal.setMultipleMealSelectionEnabled(isAllowMultiSelect);
					}

				}
			} else if (appEngine != null && SalesChannelsUtil.isBookingSystemWebOrMobile(appEngine)) {
				setAnciOffers(flightSegmentMeal, bookingFlightSegments, selectedLanguage, flightSegIdWiseSelectedMeals, salesChannelCode);
			}
		}

	}

	/**
	 * Retrieves applicable anci offer criteria and set the offer details if any offers are available, replaces the
	 * selected meal prices with the offer prices.
	 * 
	 * @param segmentBaggages
	 *            Selected segment meal detail.
	 * @param selectedLanguage
	 *            Selected language.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	private void setAnciOffers(FlightSegmentMeals segmentMeal, Collection<BookingFlightSegment> bookingFlightSegments,
			String selectedLanguage, Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, int salesChannelCode) throws ModuleException {
		AnciOfferCriteriaSearchTO anciOfferSearchCriteria = AnciOfferUtil.createAnciOfferSearch(segmentMeal.getFlightSegment(),
				AnciOfferType.MEAL, bookingFlightSegments);

		AnciOfferCriteriaTO availableAnciOffer = AAServicesModuleUtils.getAnciOfferBD().getAvailableAnciOffer(
				anciOfferSearchCriteria);

		if (availableAnciOffer != null) {

			List<FlightMealDTO> offerMeals = AAServicesModuleUtils.getMealBD().getMealsByTemplate(
					availableAnciOffer.getTemplateID(),
					segmentMeal.getFlightSegment().getCabinClassCode(),
					segmentMeal.getFlightSegment().getLogicalCabinClassCode(),
					selectedLanguage,
					Integer.valueOf(segmentMeal.getFlightSegment().getFlightRefNumber()),
					flightSegIdWiseSelectedMeals == null ? null : flightSegIdWiseSelectedMeals.get(Integer.valueOf(segmentMeal
							.getFlightSegment().getFlightRefNumber())), salesChannelCode);

			if (!offerMeals.isEmpty()) {
				segmentMeal.getMeal().clear();
				segmentMeal.getMeal().addAll(AncillaryUtil.getMealCollectionMealDTO(offerMeals));
				segmentMeal.setIsAnciOffer(true);
				segmentMeal.setOfferName(availableAnciOffer.getNameTranslations().get(selectedLanguage));
				segmentMeal.setOfferDescription(availableAnciOffer.getDescriptionTranslations().get(selectedLanguage));
				segmentMeal.setOfferedTemplateID(availableAnciOffer.getTemplateID());
			}
		}
	}

	List<FlightSegmentMeals> getSelectedMealDetails(List<SelectedMeal> selectedMeals,
			Map<Integer, Set<String>> flightSegIdWiseSelectedMeals, boolean skipCutoverValidation, AAPOS aapos)
			throws ModuleException {

		List<FlightSegmentMeals> flightSegmentMealsList = new ArrayList<FlightSegmentMeals>();

		Map<Integer, ClassOfServiceDTO> flightSegmentIdWiseCos = getFlightSegmentIdWiseCos(selectedMeals);
		Map<Integer, Integer> flightSegWiseOfferTemplateIDs = getflightSegWiseOfferTemplateIDs(selectedMeals);

		int salesChannelCode = Integer.valueOf(AAUtils.getSalesChannelFromPOS(aapos));

		Map<Integer, List<FlightMealDTO>> mealMap = AAServicesModuleUtils.getMealBD().getMeals(flightSegmentIdWiseCos,
				flightSegIdWiseSelectedMeals, skipCutoverValidation, null, salesChannelCode);

		Map<Integer, List<FlightMealDTO>> segWiseOfferMeals = AAServicesModuleUtils.getMealBD().getSegWiseMealsByTemplates(
				flightSegWiseOfferTemplateIDs, salesChannelCode);

		for (Entry<Integer, List<FlightMealDTO>> segmentEntry : mealMap.entrySet()) {

			Integer segmentId = segmentEntry.getKey();
			List<FlightMealDTO> flightMealDTOs = segmentEntry.getValue();
			if (!segWiseOfferMeals.isEmpty() && segWiseOfferMeals.get(segmentId) != null
					&& !segWiseOfferMeals.get(segmentId).isEmpty()) {
				flightMealDTOs = segWiseOfferMeals.get(segmentId);
			}

			for (FlightMealDTO flightMealDTO : flightMealDTOs) {
				for (SelectedMeal selectedMeal : selectedMeals) {
					if (selectedMeal.getMealCode().contains(flightMealDTO.getMealCode())
							&& selectedMeal.getFlightSegment().getFlightRefNumber().equals(segmentId.toString())) {

						Meal meal = new Meal();
						meal.setMealCharge(flightMealDTO.getAmount());
						meal.setMealCode(flightMealDTO.getMealCode());

						FlightSegmentMeals flightSegmentMeals = new FlightSegmentMeals();
						flightSegmentMeals.setFlightSegment(selectedMeal.getFlightSegment());
						flightSegmentMeals.getMeal().add(meal);

						if (flightSegWiseOfferTemplateIDs.get(segmentId) != null) {
							flightSegmentMeals.setIsAnciOffer(true);
							flightSegmentMeals.setOfferedTemplateID(flightSegWiseOfferTemplateIDs.get(segmentId));
						}

						flightSegmentMealsList.add(flightSegmentMeals);
					}
				}
			}
		}

		return flightSegmentMealsList;

	}

	private Map<Integer, Integer> getflightSegWiseOfferTemplateIDs(List<SelectedMeal> selectedMeals) throws ModuleException {
		Map<Integer, Integer> segWiseTemplateIDs = new HashMap<Integer, Integer>();
		boolean isBundledFareSelected = false;

		Map<String, BundledFareDTO> segmentBundledFareMap = AncillaryUtil.getSegmentCodeWiseBundledFares(
				transactionalReservationProcess, pnr, null);
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (SelectedMeal selectedMeal : selectedMeals) {
				BookingFlightSegment bkgFlightSeg = selectedMeal.getFlightSegment();
				if (segmentBundledFareMap.containsKey(bkgFlightSeg.getSegmentCode())) {
					isBundledFareSelected = true;
					BundledFareDTO bundledFareDTO = segmentBundledFareMap.get(bkgFlightSeg.getSegmentCode());
					if (bundledFareDTO != null) {
						segWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
								bundledFareDTO.getApplicableServiceTemplateId(EXTERNAL_CHARGES.MEAL.toString()));
					}
				}
			}
		}

		// If bundled fare is not selected, then check for any applied anci offers
		if (!isBundledFareSelected) {
			for (SelectedMeal selectedMeal : selectedMeals) {
				BookingFlightSegment bkgFlightSeg = selectedMeal.getFlightSegment();
				segWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()), selectedMeal.getOfferedAnciTemplateID());
			}
		}

		return segWiseTemplateIDs;
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseCos(List<SelectedMeal> selectedMeals) {
		Map<Integer, ClassOfServiceDTO> flightSegmentIds = new HashMap<Integer, ClassOfServiceDTO>();

		for (SelectedMeal selectedMeal : selectedMeals) {
			BookingFlightSegment bkgFlightSeg = selectedMeal.getFlightSegment();
			ClassOfServiceDTO cosDto = new ClassOfServiceDTO(bkgFlightSeg.getCabinClassCode(),
					bkgFlightSeg.getLogicalCabinClassCode(), bkgFlightSeg.getSegmentCode());
			flightSegmentIds.put(new Integer(bkgFlightSeg.getFlightRefNumber()), cosDto);
		}

		return flightSegmentIds;
	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
