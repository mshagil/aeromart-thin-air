package com.isa.thinair.aaservices.core.bl.availability;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AvailPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InterlineAirlineFareInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationInformation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightAvailRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAProcessParams;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUniqueParam;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAModificationParamUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAAvailabilitySearchBL implements TransactionalReservationProcessBL {

	// Logic Logger
	private static final Log log = LogFactory.getLog(AAAvailabilitySearchBL.class);

	private AAFlightAvailRQ aaFlightAvailRQ;

	private AAFlightAvailRS aaFlightAvailRS = null;

	private AvailabilityDTOCreator availabilityDTOCreator = null;

	private TransactionalReservationProcess transactionalReservationProcess;

	public void setAaFlightAvailRQ(AAFlightAvailRQ aaFlightAvailRQ) {

		this.aaFlightAvailRQ = aaFlightAvailRQ;
		aaFlightAvailRS = new AAFlightAvailRS();
		aaFlightAvailRS.setResponseAttributes(new AAResponseAttributes());
		aaFlightAvailRS.setHeaderInfo(aaFlightAvailRQ.getHeaderInfo());
		aaFlightAvailRS.getResponseAttributes().setSuccess(new AASuccess());
	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public AAFlightAvailRS execute() {

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAAvaialabilitySearchBL called by LCConnect @ " + AppSysParamsUtil.getDefaultCarrierCode());
			}

			if (transactionalReservationProcess == null || transactionalReservationProcess.getTransactionIdentifier() == null) {
				log.error("transactionalReservationProcess is null");
				throw new WebservicesException(AAErrorCode.ERR_8_MAXICO_REQUIRED_TRANSACTION_INVALID_OR_EXPIRED,
						"no trasaction found for the transaction id "
								+ aaFlightAvailRQ.getHeaderInfo().getTransactionIdentifier());
			} else {
				if (log.isDebugEnabled()) {
					log.debug(transactionalReservationProcess.getTransactionIdentifier() + " - Availability search initiated");
				}
			}

			availabilityDTOCreator = new AvailabilityDTOCreator(aaFlightAvailRQ);
			availabilityDTOCreator.createDTO();

			AvailableFlightSearchDTO availableSearchDTO = availabilityDTOCreator.getAvailableSearchDTO();
			AAServiceAvailLogger.logSearch(availableSearchDTO, AASessionManager.getInstance().getCurrentUserPrincipal());
			AvailPreferences availPreferences = aaFlightAvailRQ.getAvailPreferences();
			if (availPreferences.isInterlineAvailabilityOnly()) {
				if (log.isDebugEnabled()) {
					log.debug(transactionalReservationProcess.getTransactionIdentifier() + " - Only interline fares allowed ");
				}
				availableSearchDTO.setSearchOnlyInterlineFlights(true);
			} else {
				if (log.isDebugEnabled()) {
					log.debug(transactionalReservationProcess.getTransactionIdentifier()
							+ " - Connectable airline segment fares allowed in the search");
				}
			}

			List<InterlineAirlineFareInfo> inerlineAirlineFareList = aaFlightAvailRQ.getInterlineAirlineFareInfo();
			Collection<String> externalBookingClasses = new ArrayList<String>();
			for (InterlineAirlineFareInfo interlineAirlineFare : inerlineAirlineFareList) {
				externalBookingClasses.add(interlineAirlineFare.getBookingClassCode());
			}
			if (log.isDebugEnabled()) {
				log.debug(transactionalReservationProcess.getTransactionIdentifier()
						+ " - External booking classes from lcconnect " + externalBookingClasses.toString());
			}
			availableSearchDTO.setExternalBookingClasses(externalBookingClasses);
			availableSearchDTO.setSearchInitiatedFromInterline(true);
			availableSearchDTO.setOwnSearch(false);
			AvailableFlightDTO availableResultDTO = AAServicesModuleUtils.getReservationQueryBD()
					.getAvailableFlightsWithAllFares(availableSearchDTO, null);
			populateAAFlightAvailRS(availableResultDTO);
			populateProcessParams();
			populateReservationProcessParams();

		} catch (Exception e) {
			log.error(transactionalReservationProcess.getTransactionIdentifier() + " - Error occured at availability search", e);
			aaFlightAvailRS = new AAFlightAvailRS();
			aaFlightAvailRS.setHeaderInfo(aaFlightAvailRQ.getHeaderInfo());
			aaFlightAvailRS.setResponseAttributes(new AAResponseAttributes());
			AAExceptionUtil.addAAErrrors(aaFlightAvailRS.getResponseAttributes().getErrors(), e);
		}

		if (log.isDebugEnabled()) {
			log.debug(transactionalReservationProcess.getTransactionIdentifier() + " - Availability search completed");
		}

		return aaFlightAvailRS;
	}

	private void populateProcessParams() {
		if (this.aaFlightAvailRQ.getAvailPreferences().isIncludeHalfReturn()) {
			if (AppSysParamsUtil.isAllowHalfReturnSegmentCombinability()) {
				AAProcessParams aaPP = new AAProcessParams();
				aaPP.getUniqueParams().add(AAUniqueParam.OWHRT_COMBINE_ALLOWED);
				this.aaFlightAvailRS.setProcessParams(aaPP);
			}
		}
	}

	private void populateReservationProcessParams() {
		AppIndicatorEnum appIndicatorEnum = AppIndicatorEnum.APP_XBE;
		if (aaFlightAvailRQ.getAvailPreferences().getAppIndicator().equals(ApplicationEngine.IBE.toString())) {
			appIndicatorEnum = AppIndicatorEnum.APP_IBE;
		} else if (aaFlightAvailRQ.getAvailPreferences().getAppIndicator().equals(ApplicationEngine.ANDROID.toString())) {
			appIndicatorEnum = AppIndicatorEnum.APP_ANDROID;
		} else if (aaFlightAvailRQ.getAvailPreferences().getAppIndicator().equals(ApplicationEngine.IOS.toString())) {
			appIndicatorEnum = AppIndicatorEnum.APP_IOS;
		}
		ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
		modificationParamRQInfo.setAppIndicator(appIndicatorEnum);
		modificationParamRQInfo.setIsRegisteredUser(false);
		List<String> reservationParams = AAModificationParamUtil.getModificationParams(modificationParamRQInfo, null, false, 0);

		if (reservationParams != null) {
			aaFlightAvailRS.getReservationParams().addAll(reservationParams);
		}
	}

	private void populateAAFlightAvailRS(AvailableFlightDTO availableResultDTO) throws ModuleException {

		// populating the departure flight
		OriginDestinationInformation depatureOndInfoRS = new OriginDestinationInformation();
		AvailabilityUtil.populateOriginDestinationInformation(depatureOndInfoRS, availabilityDTOCreator.getOriginLocation(),
				availabilityDTOCreator.getDestinationLocation(), availabilityDTOCreator.getDepatureDateTimeStart(),
				availabilityDTOCreator.getDepatureDateTimeEnd());
		aaFlightAvailRS.getOriginDestinationInformation().add(depatureOndInfoRS);
		aaFlightAvailRS.setIbeOnholdEnabled(availableResultDTO.isOnHoldEnabled());

		AvailableIBOBFlightSegment selectedMinFareOutBound = null;

		AvailableIBOBFlightSegment selectedMinSegFareOutBound = null;
		Collection<AvailableIBOBFlightSegment> selectedOutboundFlights = null;

		if (availableResultDTO.getSelectedFlight() != null
				&& availableResultDTO.getSelectedFlight().getSelectedOndFlights() != null
				&& !availableResultDTO.getSelectedFlight().getSelectedOndFlights().isEmpty()) {
			selectedMinFareOutBound = availableResultDTO.getSelectedFlight().getSelectedOndFlight(OndSequence.OUT_BOUND);

			if (!availableResultDTO.getSelectedFlight().getOndWiseMinimumSegFareFlights().isEmpty()) {

				selectedMinSegFareOutBound = availableResultDTO.getSelectedFlight()
						.getSelectedOndSegFlight(OndSequence.OUT_BOUND);
				selectedOutboundFlights = availableResultDTO.getSelectedFlight().getOndWiseSelectedDateAllSegFareFlights(
						OndSequence.OUT_BOUND);
			}

		}
		AvailabilityUtil.populateFlightSegments(depatureOndInfoRS,
				availableResultDTO.getAvailableOndFlights(OndSequence.OUT_BOUND), selectedMinFareOutBound,
				availabilityDTOCreator, false, false, false);

		if (selectedMinSegFareOutBound != null) {
			AvailabilityUtil.populateFlightSegments(depatureOndInfoRS, Arrays.asList(selectedMinSegFareOutBound),
					selectedMinSegFareOutBound, availabilityDTOCreator, false, true, false);
		}

		// populate the return flight
		if (availabilityDTOCreator.isReturnFlight()) {

			AvailableIBOBFlightSegment selectedMinFareInBound = null;
			AvailableIBOBFlightSegment selectedMinSegFareInBound = null;
			Collection<AvailableIBOBFlightSegment> selectedInboundFlights = null;

			if (availableResultDTO.getSelectedFlight() != null
					&& availableResultDTO.getSelectedFlight().getSelectedOndFlights() != null
					&& availableResultDTO.getSelectedFlight().getSelectedOndFlights().size() > 1) {
				selectedMinFareInBound = availableResultDTO.getSelectedFlight().getSelectedOndFlight(OndSequence.IN_BOUND);

				if (!availableResultDTO.getSelectedFlight().getOndWiseMinimumSegFareFlights().isEmpty()) {
					selectedMinSegFareInBound = availableResultDTO.getSelectedFlight().getSelectedOndSegFlight(
							OndSequence.IN_BOUND);
					selectedInboundFlights = availableResultDTO.getSelectedFlight().getOndWiseSelectedDateAllSegFareFlights(
							OndSequence.IN_BOUND);
				}
			}

			OriginDestinationInformation returnOndInfoRS = new OriginDestinationInformation();
			AvailabilityUtil.populateOriginDestinationInformation(returnOndInfoRS,
					availabilityDTOCreator.getDestinationLocation(), availabilityDTOCreator.getOriginLocation(),
					availabilityDTOCreator.getReturnDateTimeStart(), availabilityDTOCreator.getReturnDateTimeEnd());
			returnOndInfoRS.setReturnFlag(true);
			aaFlightAvailRS.getOriginDestinationInformation().add(returnOndInfoRS);
			AvailabilityUtil.populateFlightSegments(returnOndInfoRS,
					availableResultDTO.getAvailableOndFlights(OndSequence.IN_BOUND), selectedMinFareInBound,
					availabilityDTOCreator, false, false, false);

			if (selectedMinSegFareInBound != null) {
				AvailabilityUtil.populateFlightSegments(returnOndInfoRS, Arrays.asList(selectedMinSegFareInBound),
						selectedMinSegFareInBound, availabilityDTOCreator, false, true, false);
			}
		}
	}
}
