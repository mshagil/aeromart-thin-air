package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseContactConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BasePaxConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BasePaxCutOverAge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IBEContactConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IBEPaxConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.WSPaxConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.XBEContactConfig;
import com.isa.connectivity.profiles.maxico.v1.common.dto.XBEPaxConfig;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxContactConfigRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxContactConfigRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.WSPaxConfigDTO;
import com.isa.thinair.commons.api.dto.XBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;

/**
 * 
 * @author rumesh
 * 
 */
public class AALoadPaxContactConfigBL {
	private static Log log = LogFactory.getLog(AALoadPaxContactConfigBL.class);

	private AAPaxContactConfigRQ aaContactConfigRQ;

	public void setRequestParams(AAPaxContactConfigRQ aaContactConfigRQ) {
		this.aaContactConfigRQ = aaContactConfigRQ;
	}

	public AAPaxContactConfigRS excute() {

		AAPaxContactConfigRS aaContactConfigRS = new AAPaxContactConfigRS();
		aaContactConfigRS.setResponseAttributes(new AAResponseAttributes());

		try {
			PaxContactConfigDTO paxContactConfigDTO = AAServicesModuleUtils.getGlobalConfig().getPaxContactConfig(
					aaContactConfigRQ.getAppName());
			aaContactConfigRS.getContactConfig().addAll(populateContactConfig(paxContactConfigDTO.getContactConfigList()));
			aaContactConfigRS.getPaxConfig().addAll(populatePaxConfig(paxContactConfigDTO.getPaxConfigList()));
			aaContactConfigRS.getResponseAttributes().setSuccess(new AASuccess());

			int infanCutOverAge = AAServicesModuleUtils.getGlobalConfig().getPaxTypeMap().get(PaxTypeTO.INFANT)
					.getCutOffAgeInYears();
			int childCutOverAge = AAServicesModuleUtils.getGlobalConfig().getPaxTypeMap().get(PaxTypeTO.CHILD)
					.getCutOffAgeInYears();
			int adultCutOverAge = AAServicesModuleUtils.getGlobalConfig().getPaxTypeMap().get(PaxTypeTO.ADULT)
					.getCutOffAgeInYears();
			BasePaxCutOverAge basePaxCutOverAge = new BasePaxCutOverAge();
			basePaxCutOverAge.setInfantCutOverAge(infanCutOverAge);
			basePaxCutOverAge.setChildCutOverAge(childCutOverAge);
			basePaxCutOverAge.setAdultCutOverAge(adultCutOverAge);
			aaContactConfigRS.setPaxCutOverAge(basePaxCutOverAge);

		} catch (Exception e) {
			log.error("Load pax & contact config failed in AAServices	", e);
			AAExceptionUtil.addAAErrrors(aaContactConfigRS.getResponseAttributes().getErrors(), e);
		}
		return aaContactConfigRS;
	}

	private List<BasePaxConfig> populatePaxConfig(List paxConfigDtoList) {
		List<BasePaxConfig> paxConfigList = new ArrayList<BasePaxConfig>();

		for (Object obj : paxConfigDtoList) {
			if (obj instanceof XBEPaxConfigDTO) {
				XBEPaxConfigDTO xbePaxDto = (XBEPaxConfigDTO) obj;
				XBEPaxConfig xbePax = new XBEPaxConfig();
				xbePax.setPaxConfigId(xbePaxDto.getPaxConfigId());
				xbePax.setFieldName(xbePaxDto.getFieldName());
				xbePax.setPaxCatCode(xbePaxDto.getPaxCatCode());
				xbePax.setPaxTypeCode(xbePaxDto.getPaxTypeCode());
				xbePax.setSsrCode(xbePaxDto.getSsrCode());
				xbePax.setUniqueness(xbePaxDto.getUniqueness());
				xbePax.setUniquenessGroup(xbePaxDto.getUniquenessGroup());
				xbePax.setXbeVisibility(xbePaxDto.isXbeVisibility());
				xbePax.setXbeMandatory(xbePaxDto.isXbeMandatory());
				paxConfigList.add(xbePax);
			} else if (obj instanceof IBEPaxConfigDTO) {
				IBEPaxConfigDTO ibePaxDto = (IBEPaxConfigDTO) obj;
				IBEPaxConfig ibePax = new IBEPaxConfig();
				ibePax.setPaxConfigId(ibePaxDto.getPaxConfigId());
				ibePax.setFieldName(ibePaxDto.getFieldName());
				ibePax.setPaxCatCode(ibePaxDto.getPaxCatCode());
				ibePax.setPaxTypeCode(ibePaxDto.getPaxTypeCode());
				ibePax.setSsrCode(ibePaxDto.getSsrCode());
				ibePax.setUniqueness(ibePaxDto.getUniqueness());
				ibePax.setUniquenessGroup(ibePaxDto.getUniquenessGroup());
				ibePax.setIbeVisibility(ibePaxDto.isIbeVisibility());
				ibePax.setIbeMandatory(ibePaxDto.isIbeMandatory());
				paxConfigList.add(ibePax);
			} else if (obj instanceof WSPaxConfigDTO) {
				WSPaxConfigDTO wsPaxDto = (WSPaxConfigDTO) obj;
				WSPaxConfig wsPax = new WSPaxConfig();
				wsPax.setPaxConfigId(wsPaxDto.getPaxConfigId());
				wsPax.setFieldName(wsPaxDto.getFieldName());
				wsPax.setPaxCatCode(wsPaxDto.getPaxCatCode());
				wsPax.setPaxTypeCode(wsPaxDto.getPaxTypeCode());
				wsPax.setSsrCode(wsPaxDto.getSsrCode());
				wsPax.setUniqueness(wsPaxDto.getUniqueness());
				wsPax.setUniquenessGroup(wsPaxDto.getUniquenessGroup());
				wsPax.setWsVisibility(wsPaxDto.isWsVisibility());
				wsPax.setWsMandatory(wsPaxDto.isWsMandatory());
				paxConfigList.add(wsPax);
			}
		}

		return paxConfigList;
	}

	private List<BaseContactConfig> populateContactConfig(List contactConfigDtoList) {
		List<BaseContactConfig> contactConfigList = new ArrayList<BaseContactConfig>();

		for (Object obj : contactConfigDtoList) {
			if (obj instanceof XBEContactConfigDTO) {
				XBEContactConfigDTO xbeDTO = (XBEContactConfigDTO) obj;
				XBEContactConfig xbeConfig = new XBEContactConfig();
				xbeConfig.setFieldName(xbeDTO.getFieldName());
				xbeConfig.setGroupId(xbeDTO.getGroupId());
				xbeConfig.setXbeVisibility(xbeDTO.isXbeVisibility());
				xbeConfig.setMandatory(xbeDTO.isMandatory());
				xbeConfig.setValidationGroup(xbeDTO.getValidationGroup());
				contactConfigList.add(xbeConfig);
			} else if (obj instanceof IBEContactConfigDTO) {
				IBEContactConfigDTO ibeDTO = (IBEContactConfigDTO) obj;
				IBEContactConfig ibeConfig = new IBEContactConfig();
				ibeConfig.setFieldName(ibeDTO.getFieldName());
				ibeConfig.setGroupId(ibeDTO.getGroupId());
				ibeConfig.setIbeVisibility(ibeDTO.isIbeVisibility());
				ibeConfig.setMandatory(ibeDTO.isMandatory());
				ibeConfig.setValidationGroup(ibeDTO.getValidationGroup());
				contactConfigList.add(ibeConfig);
			}
		}

		return contactConfigList;
	}
}
