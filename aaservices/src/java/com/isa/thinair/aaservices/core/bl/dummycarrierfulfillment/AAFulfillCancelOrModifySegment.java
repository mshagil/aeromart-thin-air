package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.bl.reservation.AAModifyReservationUtil;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAExtCarrierFulfillmentUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Reconcile external Pnr segments of dummy reservation for add/modify/cancel segment
 * 
 * @author malaka
 * 
 */
class AAFulfillCancelOrModifySegment extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillCancelOrModifySegment.class);
	private boolean fromNameChange;

	AAFulfillCancelOrModifySegment(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillCancelOrModifySegment::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			if (reservation == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			// updating the reservation and pax status form the opCarrier
			AAExtCarrierFulfillmentUtil.updateReservationsStatus(reservation, aaAirReservation);

			OriginDestinationOptions cancelingOndOptions = aaAirReservation.getAirReservation().getAirItinerary();

			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(super.aaPos);
			UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();
			Collection<ExternalSegmentTO> colCNXExternalSegmentTO;

			if (cancelingOndOptions != null) {
				AAExtCarrierFulfillmentUtil.injectCredentialInfomationToFlightSegment(
						cancelingOndOptions.getOriginDestinationOption(), trackInfo, userPrincipal);
			}

			colCNXExternalSegmentTO = AAModifyReservationUtil.getOndWiseExternalFlightSegments(cancelingOndOptions,
					ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
			Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();
			if (log.isDebugEnabled()) {
				log.debug("CNX external pnr segment size = [" + colCNXExternalSegmentTO.size() + "]");
			}
			colExternalSegmentTO.addAll(colCNXExternalSegmentTO);

			if (newAAAirReservation != null) {
				OriginDestinationOptions confirmingOndOptions = newAAAirReservation.getAirReservation().getAirItinerary();

				AAExtCarrierFulfillmentUtil.injectCredentialInfomationToFlightSegment(
						confirmingOndOptions.getOriginDestinationOption(), trackInfo, userPrincipal);
				Collection<ExternalSegmentTO> colCNFExternalSegmentTO = AAModifyReservationUtil.getOndWiseExternalFlightSegments(
						confirmingOndOptions, ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);
				colCNXExternalSegmentTO = AAModifyReservationUtil.getOndWiseCanceledFlightSegments(confirmingOndOptions);

				if (log.isDebugEnabled()) {
					log.debug("newly added CNF external segment size = [" + colCNFExternalSegmentTO.size() + "]");
				}
				colExternalSegmentTO.addAll(colCNFExternalSegmentTO);
				colExternalSegmentTO.addAll(colCNXExternalSegmentTO);
			}
			
			// update dummy PNR PAX details in case of name change request.
			AAExtCarrierFulfillmentUtil.updateDummyPNRPAXDetails(reservation,newAAAirReservation, isFromNameChange());

			Long version = null;
			if (colExternalSegmentTO != null && colExternalSegmentTO.size() > 0) {
				ServiceResponce response = AAServicesModuleUtils.getSegmentBD().changeExternalSegmentsForDummyReservation(
						reservation, colExternalSegmentTO);
				version = (Long) response.getResponseParam(CommandParamNames.VERSION);
			} else {
				version = reservation.getVersion();
			}

			carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
			carrierFulfillment.setResVersion(version.toString());

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillCancelOrModifySegment::end");
			}

		} catch (ModuleException e) {
			log.error("fulfill payment fail", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = newAAAirReservation.getAirReservation();

		if ((airReservation.getAirItinerary() == null || airReservation.getAirItinerary().getOriginDestinationOption() == null || airReservation
				.getAirItinerary().getOriginDestinationOption().isEmpty())) {

			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}
	}

	public boolean isFromNameChange() {
		return fromNameChange;
	}

	public void setFromNameChange(boolean fromNameChange) {
		this.fromNameChange = fromNameChange;
	}
	
}
