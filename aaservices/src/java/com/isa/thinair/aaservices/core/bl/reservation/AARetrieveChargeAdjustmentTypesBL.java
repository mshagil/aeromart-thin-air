package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AARetrieveChargeAdjustmentTypesRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * aaservices business logic for retrieving the charge adjustment types.
 * 
 * @author sanjaya
 * 
 */
public class AARetrieveChargeAdjustmentTypesBL {

	/** Class Logger */
	private static final Log log = LogFactory.getLog(AARetrieveChargeAdjustmentTypesBL.class);

	/* New PNR Response */
	private AARetrieveChargeAdjustmentTypesRS response;

	// Initializes the response variable.
	private void initializeResponseObject() {
		response = new AARetrieveChargeAdjustmentTypesRS();
		response.setResponseAttributes(new AAResponseAttributes());
	}

	/**
	 * @return The charge adjustment types response.
	 */
	public AARetrieveChargeAdjustmentTypesRS excute() {

		initializeResponseObject();

		try {
			Collection<ChargeAdjustmentTypeDTO> chargeAdjustmentTypes = CommonsServices.getGlobalConfig()
					.getChargeAdjustmentTypes();

			for (ChargeAdjustmentTypeDTO adjustmentType : chargeAdjustmentTypes) {
				response.getChargeAdjustmentTypes().add(TransformerUtil.transform(adjustmentType));
			}
		} catch (Exception ex) {
			log.error("Error occurred while retrieving the charge adjustment types from airline : "
					+ AppSysParamsUtil.getDefaultCarrierCode());
			AAExceptionUtil.addAAErrrors(response.getResponseAttributes().getErrors(), ex);

		} finally {
			int numOfErrors = response.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			response.getResponseAttributes().setSuccess(successType);
		}
		return response;
	}
}
