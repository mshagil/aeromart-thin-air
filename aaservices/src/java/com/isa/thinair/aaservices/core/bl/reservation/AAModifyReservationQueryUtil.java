package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AlterationBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CnxModAddONDBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Discount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fee;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FeeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ResCnxModAddResBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Surcharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Tax;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCnxModAddResBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAPaymentDetailsFactory;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airproxy.api.dto.FareInfoTOV2;
import com.isa.thinair.airproxy.api.dto.PAXSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ResBalancesSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.ResModifyQueryV2Util;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.ChargeMetaTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.LccPaxPaymentTO;
import com.isa.thinair.airreservation.api.dto.decorator.ReservationMediator;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.ResModifyQueryV2UtilSummer;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * @author Nilindra Fernando
 * @since 7:48 PM 11/13/2009
 */
class AAModifyReservationQueryUtil {

	static AAAirBookRS getBalancesForCancelOND(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ)
			throws ModuleException, WebservicesException {

		Collection<Collection<Integer>> ondWisePnrSegIds = AAModifyReservationUtil.getOndWiseOwnPnrSegIds(reservation,
				aaAirBookModifyRQ.getAaReservation().getAirReservation().getAirItinerary());

		int ondCount = ondWisePnrSegIds.size();

		boolean isNewOndExists = false;
		Collection<Integer> newFltSegIds = null;
		if (aaAirBookModifyRQ.getNewAAReservation() != null) {
			isNewOndExists = AAModifyReservationUtil.isOndsExists(aaAirBookModifyRQ.getNewAAReservation().getAirReservation()
					.getAirItinerary());
			if (ondCount == 1) {
				newFltSegIds = AAModifyReservationUtil.getFltSegIds(aaAirBookModifyRQ.getNewAAReservation().getAirReservation()
						.getAirItinerary());
			}
		}

		String alterationType = isNewOndExists
				? ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE
				: ReservationConstants.AlterationType.ALT_CANCEL_OND;
		ResBalancesSummaryTOV2 holderResBalancesSummaryTOV2 = null;
		ResBalancesSummaryTOV2 resBalancesSummaryTOV2;

		// divide the override value among OND if it's the TOT
		ChargerOverride chargeOverride = aaAirBookModifyRQ.getChargeOverride();
		AAReservationUtil.recalculateChargeOverride(chargeOverride, ondCount);

		CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride, alterationType);

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, null);

		for (Collection<Integer> pnrSegIds : ondWisePnrSegIds) {

			resBalancesSummaryTOV2 = ResModifyQueryV2Util.getResBalancesForAlt(alterationType, reservation, pnrSegIds,
					newFltSegIds, null, false, customChargesTO, null, aaAirBookModifyRQ.isHasExternalPayments(), 0,
					quotedExtChgMap);

			if (holderResBalancesSummaryTOV2 == null) {
				holderResBalancesSummaryTOV2 = resBalancesSummaryTOV2;
			} else {
				holderResBalancesSummaryTOV2 = ResModifyQueryV2UtilSummer.sum(holderResBalancesSummaryTOV2,
						resBalancesSummaryTOV2);
			}
		}

		holderResBalancesSummaryTOV2 = ResModifyQueryV2Util.applyPaymentAwareness(alterationType, reservation,
				holderResBalancesSummaryTOV2);
		return composeAAAirBookRS(alterationType, holderResBalancesSummaryTOV2, reservation);
	}

	static AAAirBookRS getBalancesForCancelRes(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ)
			throws ModuleException {

		String alterationType = ReservationConstants.AlterationType.ALT_CANCEL_RES;

		int ondCount = ReservationUtil.getUniqueFareIds(reservation);

		// divide the override value among OND if it's the TOT
		ChargerOverride chargeOverride = aaAirBookModifyRQ.getChargeOverride();
		AAReservationUtil.recalculateChargeOverride(chargeOverride, ondCount);

		CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride, alterationType);

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, null);
		
		
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap = null;
		if (aaAirBookModifyRQ.getPerPaxExternalCharges() != null && !aaAirBookModifyRQ.getPerPaxExternalCharges().isEmpty()) {
			paxExtChargeMap = AAModifyReservationQueryUtil
					.composePerPaxExternalChargeMap(aaAirBookModifyRQ.getPerPaxExternalCharges());
		}

		ResBalancesSummaryTOV2 resBalancesSummaryTOV2 = ResModifyQueryV2Util.getResBalancesForAlt(alterationType, reservation,
				null, null, null, false, customChargesTO, paxExtChargeMap, aaAirBookModifyRQ.isHasExternalPayments(), 0,
				quotedExtChgMap);

		resBalancesSummaryTOV2 = ResModifyQueryV2Util.applyPaymentAwareness(alterationType, reservation, resBalancesSummaryTOV2);
		return composeAAAirBookRS(alterationType, resBalancesSummaryTOV2, reservation);
	}

	static AAAirBookRS getBalanceForAddInfant(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ)
			throws ModuleException {
		String alterationType = ReservationConstants.AlterationType.ALT_ADD_INF;
		int noOfInfants = 1;
		ReservationMediator rm = new ReservationMediator(reservation);
		Collection ondDTOs = rm.getOndFareDTOs(noOfInfants);
		UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();

		addInfant(reservation, aaAirBookModifyRQ.getAdPaxList());

		Collection<OndFareDTO> collFares = AAServicesModuleUtils.getFlightInventoryResBD().getInfantQuote(ondDTOs,
				userPrincipal.getAgentStation(), true, userPrincipal.getAgentCode());

		int ondCount = ReservationUtil.getUniqueFareIds(reservation);

		ChargerOverride chargeOverride = aaAirBookModifyRQ.getChargeOverride();
		AAReservationUtil.recalculateChargeOverride(chargeOverride, ondCount);

		CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride, alterationType);

		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap = AirproxyModuleUtils.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, null);
		
		ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = ServiceTaxConverterUtil.createServiceTaxQuoteForAddInfantRequest(
				collFares, reservation, userPrincipal, null, null);
		ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = ServiceTaxConverterUtil
				.adaptServiceTaxQuoteForTicketingRevenueRS(AirproxyModuleUtils.getChargeBD().quoteServiceTaxForTicketingRevenue(
						serviceTaxRQ));
		Map<Integer, List<LCCClientExternalChgDTO>> passengerExtChargeMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
		String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
		String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();
		List<ServiceTaxTO> infantServiceTaxs = null;
		if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null && !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
			infantServiceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT);
			ServiceTaxCalculatorUtil.addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs, taxDepositCountryCode,
					taxDepositStateCode, false);
			Integer adultSeqId = null;
			for (String adult : aaAirBookModifyRQ.getAdPaxList()) {
				if (adult != null) {
					adultSeqId = new Integer(adult);
					break;
				}
			}
			passengerExtChargeMap.put(adultSeqId, infantChgDTOs);
		}
		
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap = composePerPaxExternalChargeMap(
				aaAirBookModifyRQ.getPerPaxExternalCharges());
		
		if (paxExtChargeMap != null && !paxExtChargeMap.isEmpty()) {
			Integer adultSeqId = null;
			for (String adult : aaAirBookModifyRQ.getAdPaxList()) {
				if (adult != null) {
					adultSeqId = new Integer(adult);
					break;
				}
			}
			for (Integer paxSeq : paxExtChargeMap.keySet()) {
				if (passengerExtChargeMap.get(paxSeq) == null) {
					passengerExtChargeMap.put(adultSeqId, new ArrayList<>());
				}

				passengerExtChargeMap.get(adultSeqId).addAll(paxExtChargeMap.get(paxSeq));
			}
		}

		ResBalancesSummaryTOV2 resBalancesSummaryTOV2 = ResModifyQueryV2Util.getResBalancesForAlt(alterationType, reservation,
				null, null, collFares, false, customChargesTO, passengerExtChargeMap, aaAirBookModifyRQ.isHasExternalPayments(), 0,
				quotedExtChgMap);

		resBalancesSummaryTOV2.setInfantCharge(getInfantCharge(collFares, infantServiceTaxs));

		resBalancesSummaryTOV2 = ResModifyQueryV2Util.applyPaymentAwareness(alterationType, reservation, resBalancesSummaryTOV2);
		return composeAAAirBookRS(alterationType, resBalancesSummaryTOV2, reservation);
	}

	/*
	 * Add dummy infant entry to reservation
	 */
	private static void addInfant(Reservation reservation, List<String> adultIdList) {
		int infSeq = reservation.getPassengers().size();
		for (String adult : adultIdList) {
			for (ReservationPax pax : (Collection<ReservationPax>) reservation.getPassengers()) {
				if (adult != null && (new Integer(adult)).compareTo(pax.getPaxSequence()) == 0) {
					ReservationPax newInfant = new ReservationPax();
					newInfant.setPaxType(ReservationInternalConstants.PassengerType.INFANT);
					newInfant.addParent(pax);
					pax.addInfants(newInfant);

					reservation.addPassenger(newInfant);
					break;
				}
			}
		}
	}

	private static BigDecimal getInfantCharge(Collection<OndFareDTO> collFares, List<ServiceTaxTO> infantServiceTaxs) {
		BigDecimal infCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (collFares != null) {
			Iterator<OndFareDTO> ondFareDTOColForInfFQIt = collFares.iterator();

			double infantCharge = 0;
			OndFareDTO ondFareDTO;
			while (ondFareDTOColForInfFQIt.hasNext()) {
				ondFareDTO = ondFareDTOColForInfFQIt.next();
				infantCharge += ondFareDTO.getInfantFare() + ondFareDTO.getTotalCharges()[1];
			}
			if (infantServiceTaxs != null && !infantServiceTaxs.isEmpty()) {
				for (ServiceTaxTO serviceTax : infantServiceTaxs) {
					infantCharge += serviceTax.getAmount().doubleValue();
				}
			}

			infCharge = AccelAeroCalculator.parseBigDecimal(infantCharge);
		}

		return infCharge;
	}

	private static AAAirBookRS composeAAAirBookRS(String alterationType, ResBalancesSummaryTOV2 resBalancesSummaryTOV2,
			Reservation reservation) throws ModuleException {

		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());
		aaAirBookRS.setAaAirReservation(new AAReservation());
		aaAirBookRS.getAaAirReservation().setAirReservation(new AirReservation());

		ResCnxModAddResBalances resCnxModAddResBalances = prepareWSResAlterationBalances(alterationType, reservation,
				resBalancesSummaryTOV2);

		resCnxModAddResBalances.setInfantCharge(resBalancesSummaryTOV2.getInfantCharge());

		aaAirBookRS.getAaAirReservation().getAirReservation().setResCnxModAddResBalances(resCnxModAddResBalances);
		aaAirBookRS.getAaAirReservation().setGroupPnr(reservation.getOriginatorPnr());
		aaAirBookRS.getAaAirReservation().setPnr(reservation.getPnr());
		aaAirBookRS.getResponseAttributes().setSuccess(new AASuccess());

		return aaAirBookRS;
	}

	private static ResCnxModAddResBalances prepareWSResAlterationBalances(String alterationType, Reservation reservation,
			ResBalancesSummaryTOV2 resBalancesSummaryTOV2) throws ModuleException {

		ResCnxModAddResBalances resCnxModAddResBalances = new ResCnxModAddResBalances();
		AlterationBalances alterationBalances = composeAlterationBalances(alterationType,
				resBalancesSummaryTOV2.getSegmentSummaryTOV2(),
				PaxTypeUtils.composePassengerTypes(PassengerType.ADT, PassengerType.CHD, PassengerType.INF));
		resCnxModAddResBalances.setAlterationBalances(alterationBalances);

		Map<Integer, ReservationPax> passengerMap = ReservationUtil.getPassengerMap(reservation.getPassengers());
		// Set pax balances
		Collection<PAXSummaryTOV2> colPAXSummaryTOV2 = resBalancesSummaryTOV2.getPaxSummaryTOV2();

		for (PAXSummaryTOV2 paxSummaryTOV2 : colPAXSummaryTOV2) {
			TravelerCnxModAddResBalances travelerCnxModAddResBalances = new TravelerCnxModAddResBalances();

			ReservationPax parentReservationPax = passengerMap.get(paxSummaryTOV2.getPnrPaxId());
			travelerCnxModAddResBalances.setTravelerRefNumber(PaxTypeUtils.travelerReference(parentReservationPax));
			travelerCnxModAddResBalances.setTravelerName(ReservationApiUtils.getPassengerName(null,
					parentReservationPax.getTitle(), parentReservationPax.getFirstName(), parentReservationPax.getLastName(),
					false));

			if (ReservationApiUtils.isParentAfterSave(parentReservationPax)) {
				ReservationPax infant = (ReservationPax) BeanUtils.getFirstElement(parentReservationPax.getInfants());
				travelerCnxModAddResBalances.setTravelerInfantName(ReservationApiUtils.getPassengerName(null, infant.getTitle(),
						infant.getFirstName(), infant.getLastName(), false));
			} else {
				travelerCnxModAddResBalances.setTravelerInfantName("");
			}

			travelerCnxModAddResBalances.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(parentReservationPax
					.getPaxType()));

			AlterationBalances perPaxAlterationBalances = composeAlterationBalances(alterationType,
					paxSummaryTOV2.getSegmentSummaryTOV2(),
					PaxTypeUtils.composePassengerTypes(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxSummaryTOV2.getPaxType())));

			perPaxAlterationBalances.getPaymentSummary().addAll(composeCredits(paxSummaryTOV2.getColCredits()));
			perPaxAlterationBalances.getPaymentDetails().addAll(composeReservationTnxs(paxSummaryTOV2.getColPayments()));
			perPaxAlterationBalances.getCustomerPaymentDetails().addAll(
					composeLccPaxPayments(paxSummaryTOV2.getColLccPaxPayments()));

			perPaxAlterationBalances.setTotalAmountDue(paxSummaryTOV2.getTotalAmountDue());
			perPaxAlterationBalances.setTotalCreditAmount(paxSummaryTOV2.getTotalCreditAmount());
			perPaxAlterationBalances.setTotalCnxChargeForCurrentOperation(paxSummaryTOV2.getTotalCnxChargeForCurrentAlt());
			perPaxAlterationBalances.setTotalModChargeForCurrentOperation(paxSummaryTOV2.getTotalModChargeForCurrentAlt());
			perPaxAlterationBalances.setTotalPrice(paxSummaryTOV2.getTotalPrice());

			travelerCnxModAddResBalances.setAlterationBalances(perPaxAlterationBalances);

			resCnxModAddResBalances.getTravelerCnxModAddResBalances().add(travelerCnxModAddResBalances);
		}

		// Set totals
		alterationBalances.getPaymentSummary().addAll(composeCredits(resBalancesSummaryTOV2.getColCredits()));
		alterationBalances.getPaymentDetails().addAll(composeReservationTnxs(resBalancesSummaryTOV2.getColPayments()));
		alterationBalances.getCustomerPaymentDetails().addAll(
				composeLccPaxPayments(resBalancesSummaryTOV2.getColLccPaxPayments()));

		alterationBalances.setTotalAmountDue(resBalancesSummaryTOV2.getTotalAmountDue());
		alterationBalances.setTotalCreditAmount(resBalancesSummaryTOV2.getTotalCreditAmount());
		alterationBalances.setTotalCnxChargeForCurrentOperation(resBalancesSummaryTOV2.getTotalCnxChargeForCurrentAlt());
		alterationBalances.setTotalModChargeForCurrentOperation(resBalancesSummaryTOV2.getTotalModChargeForCurrentAlt());
		alterationBalances.setTotalPrice(resBalancesSummaryTOV2.getTotalPrice());

		return resCnxModAddResBalances;
	}

	private static Collection<PaymentDetails> composeReservationTnxs(Collection<ReservationTnx> colReservationTnxs)
			throws ModuleException {
		Collection<PaymentDetails> colPaymentDetails = new ArrayList<PaymentDetails>();

		if (colReservationTnxs != null && colReservationTnxs.size() > 0) {
			for (ReservationTnx reservationTnx : colReservationTnxs) {
				PaymentDetails paymentDetails = AAPaymentDetailsFactory.createPaymentDetails(reservationTnx, null, null, true);
				// TODO To make the payments based on the payment types
				colPaymentDetails.add(paymentDetails);
			}
		}

		return colPaymentDetails;
	}

	private static Collection<PaymentDetails> composeLccPaxPayments(Collection<LccPaxPaymentTO> colLccPaxPaymentTO)
			throws ModuleException {
		Collection<PaymentDetails> colPaymentDetails = new ArrayList<PaymentDetails>();

		if (colLccPaxPaymentTO != null && colLccPaxPaymentTO.size() > 0) {
			for (LccPaxPaymentTO lccPaxPaymentTO : colLccPaxPaymentTO) {
				PaymentDetails paymentDetails = AAPaymentDetailsFactory.createPaymentDetails(lccPaxPaymentTO.getLccResPaymentTO()
						.getTnxDate(), lccPaxPaymentTO.getBaseAmount(), null, null);
				// No need to set payment carrier reference as it is not valid for re-factored lcc transactions
				// TODO To make the payments based on the payment types
				paymentDetails.setCash(true);

				colPaymentDetails.add(paymentDetails);
			}
		}

		return colPaymentDetails;
	}

	private static Collection<PaymentSummary> composeCredits(Collection<ReservationTnx> colReservationTnx) throws ModuleException {
		Collection<PaymentSummary> colCredit = new ArrayList<PaymentSummary>();

		if (colReservationTnx != null) {
			for (ReservationTnx reservationTnx : colReservationTnx) {
				PaymentSummary credit = new PaymentSummary();
				credit.setApplicableDateTime(reservationTnx.getDateTime());
				credit.setPaymentAmount(reservationTnx.getAmount());
				credit.setCurrencyCodeGroup(FindReservationUtil.getDefaultCurrencyCodeGroup());

				colCredit.add(credit);
			}
		}

		return colCredit;
	}

	private static AlterationBalances composeAlterationBalances(String alterationType, SegmentSummaryTOV2 segmentSummaryTOV2,
			Collection<PassengerType> colPassengerType) throws ModuleException {
		AlterationBalances alterationBalances = new AlterationBalances();

		CnxModAddONDBalances updatingONDBalances = null;
		CnxModAddONDBalances newONDBalances = null;

		// Existing OND data
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
			updatingONDBalances = new CnxModAddONDBalances();

			updatingONDBalances.setCurrentAmounts(convert(segmentSummaryTOV2.getCurrentAmounts(), colPassengerType));
			updatingONDBalances.setNonRefundableAmounts(convert(segmentSummaryTOV2.getCurrentNonRefundableAmounts(),
					colPassengerType));
			updatingONDBalances.setRefundableAmounts(convert(segmentSummaryTOV2.getCurrentRefundableAmounts(), colPassengerType));
		}

		// New OND data
		boolean isCancelPNRExtChargesExist = false;
		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				&& AppSysParamsUtil.applyHandlingFeeOnModification() && segmentSummaryTOV2.getNewAmounts() != null
				&& segmentSummaryTOV2.getNewAmounts().getTotalSurcharge().doubleValue() > 0) {
			isCancelPNRExtChargesExist = true;
		}
		
		if (isCancelPNRExtChargesExist || ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			newONDBalances = new CnxModAddONDBalances();

			newONDBalances.setCurrentAmounts(convert(segmentSummaryTOV2.getNewAmounts(), colPassengerType));

			if (segmentSummaryTOV2.getNewNonRefundableAmounts() != null) {
				newONDBalances
						.setNonRefundableAmounts(convert(segmentSummaryTOV2.getNewNonRefundableAmounts(), colPassengerType));
			}

			if (segmentSummaryTOV2.getNewRefundableAmounts() != null) {
				newONDBalances.setRefundableAmounts(convert(segmentSummaryTOV2.getNewRefundableAmounts(), colPassengerType));
			}
		}

		if (ReservationConstants.AlterationType.ALT_CANCEL_RES.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE.equals(alterationType)) {
			alterationBalances.setCnxONDBalances(updatingONDBalances);
		}

		if (isCancelPNRExtChargesExist || ReservationConstants.AlterationType.ALT_ADD_OND.equals(alterationType)
				|| ReservationConstants.AlterationType.ALT_ADD_INF.equals(alterationType)) {
			alterationBalances.setAddONDBalances(newONDBalances);
		}

		alterationBalances.setCurrencyCodeGroup(FindReservationUtil.getDefaultCurrencyCodeGroup());

		return alterationBalances;
	}

	private static FareType convert(FareInfoTOV2 currentAmounts, Collection<PassengerType> colPassengerType) {
		FareType fareType = new FareType();
		fareType.setTotalFare(currentAmounts.getTotalFare());
		fareType.setTotalTaxes(currentAmounts.getTotalTax());
		fareType.setTotalSurcharges(currentAmounts.getTotalSurcharge());
		fareType.setTotalFees(AccelAeroCalculator.add(currentAmounts.getTotalAdjCharge(), currentAmounts.getTotalCanCharge(),
				currentAmounts.getTotalModCharge()));
		fareType.setTotalDiscount(currentAmounts.getTotalDiscount());
		fareType.setTotalPrice(AccelAeroCalculator.add(currentAmounts.getTotalFare(), currentAmounts.getTotalTax(),
				currentAmounts.getTotalSurcharge(), currentAmounts.getTotalAdjCharge(), currentAmounts.getTotalCanCharge(),
				currentAmounts.getTotalModCharge(), currentAmounts.getTotalDiscount()));

		CurrencyCodeGroup currencyCodeGroup = new CurrencyCodeGroup();
		// TODO Currency properties are hard coded
		currencyCodeGroup.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		currencyCodeGroup.setDecimalPlaces(2);
		fareType.setCurrencyCodeGroup(currencyCodeGroup);

		fareType.getBaseFare().addAll(composeFare(currentAmounts.getFare(), colPassengerType));
		fareType.getTaxes().addAll(composeTax(currentAmounts.getTaxes(), colPassengerType));
		fareType.getSurcharges().addAll(composeSurcharges(currentAmounts.getSurcharges(), colPassengerType));
		fareType.getFees().addAll(composeFees(currentAmounts.getCancellations(), colPassengerType));
		fareType.getFees().addAll(composeFees(currentAmounts.getModifications(), colPassengerType));
		fareType.getFees().addAll(composeFees(currentAmounts.getAdjustments(), colPassengerType));
		fareType.getDiscount().addAll(composeDiscount(currentAmounts.getDiscounts(), colPassengerType));

		return fareType;
	}

	private static Collection<Fee> composeFees(Collection<ChargeMetaTO> fees, Collection<PassengerType> colPassengerType) {
		Collection<Fee> colFees = new ArrayList<Fee>();

		for (ChargeMetaTO chargeMetaTO : fees) {
			Fee fee = new Fee();
			fee.setAmount(chargeMetaTO.getChargeAmount());
			fee.setApplicableDateTime(chargeMetaTO.getChargeDate());
			fee.setSegmentCode(BeanUtils.nullHandler(chargeMetaTO.getOndCode()));
			fee.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			fee.setFeeCode(BeanUtils.nullHandler(chargeMetaTO.getChargeGroupCode()));
			fee.setFeeName(BeanUtils.nullHandler(chargeMetaTO.getChargeDescription()));
			fee.getApplicablePassengerType().addAll(colPassengerType);

			if (chargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.ADJ)) {
				fee.setFeeType(FeeType.ADJ);
			} else if (chargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.CNX)) {
				fee.setFeeType(FeeType.CNX);
			} else if (chargeMetaTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.MOD)) {
				fee.setFeeType(FeeType.MOD);
			} else {
				// FIXME, throw exception (Unidentified Fee) or add fee type called OTH (Other) ?
			}

			colFees.add(fee);
		}

		return colFees;
	}

	private static Collection<Surcharge> composeSurcharges(Collection<ChargeMetaTO> surcharges,
			Collection<PassengerType> colPassengerType) {
		Collection<Surcharge> colSurcharges = new ArrayList<Surcharge>();

		for (ChargeMetaTO chargeMetaTO : surcharges) {
			Surcharge surcharge = new Surcharge();
			surcharge.setAmount(chargeMetaTO.getChargeAmount());
			surcharge.setApplicableDateTime(chargeMetaTO.getChargeDate());
			surcharge.setSegmentCode(BeanUtils.nullHandler(chargeMetaTO.getOndCode()));
			surcharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			surcharge.setSurchargeCode(BeanUtils.nullHandler(chargeMetaTO.getChargeCode()));
			surcharge.setSurchargeName(BeanUtils.nullHandler(chargeMetaTO.getChargeDescription()));
			surcharge.getApplicablePassengerType().addAll(colPassengerType);

			colSurcharges.add(surcharge);
		}

		return colSurcharges;
	}

	private static Collection<Tax> composeTax(Collection<ChargeMetaTO> taxes, Collection<PassengerType> colPassengerType) {
		Collection<Tax> colTaxes = new ArrayList<Tax>();

		for (ChargeMetaTO chargeMetaTO : taxes) {
			Tax tax = new Tax();
			tax.setAmount(chargeMetaTO.getChargeAmount());
			tax.setApplicableDateTime(chargeMetaTO.getChargeDate());
			tax.setSegmentCode(BeanUtils.nullHandler(chargeMetaTO.getOndCode()));
			tax.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			tax.setTaxCode(BeanUtils.nullHandler(chargeMetaTO.getChargeCode()));
			tax.setTaxName(BeanUtils.nullHandler(chargeMetaTO.getChargeDescription()));
			tax.getApplicablePassengerType().addAll(colPassengerType);

			colTaxes.add(tax);
		}

		return colTaxes;
	}

	private static Collection<Discount> composeDiscount(Collection<ChargeMetaTO> discounts,
			Collection<PassengerType> colPassengerType) {
		Collection<Discount> colDiscounts = new ArrayList<Discount>();
		for (ChargeMetaTO chargeMetaTO : discounts) {
			Discount discount = new Discount();
			discount.setAmount(chargeMetaTO.getChargeAmount());
			discount.setApplicableDateTime(chargeMetaTO.getChargeDate());
			discount.setSegmentCode(BeanUtils.nullHandler(chargeMetaTO.getOndCode()));
			discount.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			discount.setDiscountCode(BeanUtils.nullHandler(chargeMetaTO.getChargeCode()));
			discount.setDiscountName(BeanUtils.nullHandler(chargeMetaTO.getChargeDescription()));
			discount.getApplicablePassengerType().addAll(colPassengerType);
			colDiscounts.add(discount);
		}
		return colDiscounts;
	}

	private static Collection<BaseFare> composeFare(Collection<ChargeMetaTO> fare, Collection<PassengerType> colPassengerType) {
		Collection<BaseFare> colBaseFare = new ArrayList<BaseFare>();

		for (ChargeMetaTO chargeMetaTO : fare) {
			BaseFare baseFare = new BaseFare();
			baseFare.setAmount(chargeMetaTO.getChargeAmount());
			baseFare.setApplicableDateTime(chargeMetaTO.getChargeDate());
			baseFare.setSegmentCode(BeanUtils.nullHandler(chargeMetaTO.getOndCode()));
			baseFare.getCarrierCode().add(AppSysParamsUtil.getDefaultCarrierCode());
			baseFare.getApplicablePassengerType().addAll(colPassengerType);

			colBaseFare.add(baseFare);
		}

		return colBaseFare;
	}

	static AAAirBookRS getLastAllowedReleaseTime(Reservation reservation) throws WebservicesException {

		@SuppressWarnings("unchecked")
		Collection<FlightSegmentDTO> colFlightSegmentDTO = WebplatformUtil.getConfirmedDepartureSegments(reservation
				.getSegmentsView());
		Date maxAllowedReleaseTimeZulu = WebplatformUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTO);
		boolean isConfirmedExternalSegmentExist = checkConfirmedExternalSegments(reservation.getExternalReservationSegment());

		if (maxAllowedReleaseTimeZulu == null && !isConfirmedExternalSegmentExist) {// Not allowed to extend hold
			throw new WebservicesException(
					AAErrorCode.ERR_30_MAXICO_REQUIRED_INVALID_ONHOLD_DURATION_PLEASE_CONTACT_ADMINISTRATOR,
					"Onhold release time can not be empty");
		}

		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setAaAirReservation(new AAReservation());
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());
		aaAirBookRS.getAaAirReservation().setAirReservation(new AirReservation());
		aaAirBookRS.getAaAirReservation().getAirReservation().setPriceInfo(new PriceInfo());

		aaAirBookRS.getAaAirReservation().setPnr(reservation.getPnr());
		aaAirBookRS.getAaAirReservation().setGroupPnr(reservation.getOriginatorPnr());
		aaAirBookRS.getAaAirReservation().getAirReservation().getPriceInfo().setOnholdReleaseTimeZulu(maxAllowedReleaseTimeZulu);

		aaAirBookRS.getResponseAttributes().setSuccess(new AASuccess());

		return aaAirBookRS;
	}

	private static boolean checkConfirmedExternalSegments(Set<ExternalPnrSegment> existingExtsegments) {
		boolean isExternalSegmentExist = false;
		for (ExternalPnrSegment pnrSegment : existingExtsegments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(pnrSegment.getStatus())) {
				isExternalSegmentExist = true;
				break;
			}
		}
		return isExternalSegmentExist;
	}

	static AAAirBookRS getBalancesForAddOND(Reservation reservation, long version, AAAirBookModifyRQ aaAirBookModifyRQ,
			Collection<OndFareDTO> colOndFareDTOs) throws WebservicesException, ModuleException {

		if (reservation != null && reservation.getVersion() != version
				&& (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.NO)) {
			throw new WebservicesException(
					AAErrorCode.ERR_23_MAXICO_REQUIRED_CONCURRENT_UPDATE_OF_THE_RESERVATION_DETECTED_PLEASE_TRY_AGAIN, "");
		}

		String alterationType = ReservationConstants.AlterationType.ALT_ADD_OND;
		ResBalancesSummaryTOV2 resBalancesSummaryTOV2;
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap = composePerPaxExternalChargeMap(aaAirBookModifyRQ
				.getPerPaxExternalCharges());

		if (reservation != null) {
			// TODO Shouldn't this be changed to the new OND count rather than existing one? FIXME
			int ondCount = ReservationUtil.getUniqueFareIds(reservation);

			// divide the override value among OND if it's the TOT
			ChargerOverride chargeOverride = aaAirBookModifyRQ.getChargeOverride();
			AAReservationUtil.recalculateChargeOverride(chargeOverride, ondCount);

			CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride, alterationType);

			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
			Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedExtChgMap = AirproxyModuleUtils.getReservationBD()
					.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null);

			resBalancesSummaryTOV2 = ResModifyQueryV2Util.getResBalancesForAlt(alterationType, reservation, null, null,
					colOndFareDTOs, false, customChargesTO, paxExtChargeMap, aaAirBookModifyRQ.isHasExternalPayments(), 0,
					quotedExtChgMap);
			return composeAAAirBookRS(alterationType, resBalancesSummaryTOV2, reservation);
		} else {
			// TODO charge override implementation for fresh OND
			Collection<PAXSummaryTOV2> colPAXSummaryTOV2 = composePaxSummaryV2(aaAirBookModifyRQ.getAaReservation()
					.getAirReservation().getTravelerInfo());
			resBalancesSummaryTOV2 = ResModifyQueryV2Util.getBalancesForFreshAddOnd(alterationType, colPAXSummaryTOV2,
					colOndFareDTOs, paxExtChargeMap, 0);
			return composeAAAirBookRS(alterationType, resBalancesSummaryTOV2, aaAirBookModifyRQ);
		}
	}

	public static Map<Integer, List<LCCClientExternalChgDTO>> composePerPaxExternalChargeMap(
			List<PerPaxExternalCharges> ppExtChgList) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap = null;
		if (ppExtChgList != null) {
			paxExtChargeMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			for (PerPaxExternalCharges ppExtChgs : ppExtChgList) {
				paxExtChargeMap.put(ppExtChgs.getPaxSequence(), tranformExtCharges(ppExtChgs.getExternalCharges()));
			}
		}
		return paxExtChargeMap;
	}

	private static List<LCCClientExternalChgDTO> tranformExtCharges(List<ExternalCharge> externalCharges) {
		List<LCCClientExternalChgDTO> lccExtChgDTOList = new ArrayList<LCCClientExternalChgDTO>();
		for (ExternalCharge extChg : externalCharges) {
			LCCClientExternalChgDTO lccExtChg = new LCCClientExternalChgDTO();
			lccExtChg.setAmount(extChg.getAmount());
			lccExtChg.setCarrierCode(extChg.getCarrierCode());
			lccExtChg.setFlightRefNumber(extChg.getFlightRefNumber());
			lccExtChg.setSegmentCode(extChg.getSegmentCode());
			lccExtChg.setCode(extChg.getCode());
			lccExtChg.setExternalCharges(AAUtils.getExternalChargeType(extChg.getType()));
			lccExtChg.setAirportCode(extChg.getAirportCode());
			lccExtChg.setOperationMode(extChg.getOperationMode());
			lccExtChgDTOList.add(lccExtChg);
		}
		return lccExtChgDTOList;
	}

	private static AAAirBookRS composeAAAirBookRS(String alterationType, ResBalancesSummaryTOV2 resBalancesSummaryTOV2,
			AAAirBookModifyRQ aaAirBookModifyRQ) throws ModuleException {

		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());
		aaAirBookRS.setAaAirReservation(new AAReservation());
		aaAirBookRS.getAaAirReservation().setAirReservation(new AirReservation());

		ResCnxModAddResBalances resCnxModAddResBalances = prepareWSResAlterationBalances(alterationType, aaAirBookModifyRQ,
				resBalancesSummaryTOV2);

		aaAirBookRS.getAaAirReservation().getAirReservation().setResCnxModAddResBalances(resCnxModAddResBalances);
		aaAirBookRS.getAaAirReservation().setGroupPnr(aaAirBookModifyRQ.getAaReservation().getGroupPnr());
		aaAirBookRS.getAaAirReservation().setPnr(aaAirBookModifyRQ.getAaReservation().getPnr());
		aaAirBookRS.getResponseAttributes().setSuccess(new AASuccess());

		return aaAirBookRS;
	}

	private static ResCnxModAddResBalances prepareWSResAlterationBalances(String alterationType,
			AAAirBookModifyRQ aaAirBookModifyRQ, ResBalancesSummaryTOV2 resBalancesSummaryTOV2) throws ModuleException {

		ResCnxModAddResBalances resCnxModAddResBalances = new ResCnxModAddResBalances();
		AlterationBalances alterationBalances = composeAlterationBalances(alterationType,
				resBalancesSummaryTOV2.getSegmentSummaryTOV2(),
				PaxTypeUtils.composePassengerTypes(PassengerType.ADT, PassengerType.CHD, PassengerType.INF));
		resCnxModAddResBalances.setAlterationBalances(alterationBalances);

		Map<Integer, AirTraveler> passengerMap = getAirTravelers(aaAirBookModifyRQ);
		// Set pax balances
		Collection<PAXSummaryTOV2> colPAXSummaryTOV2 = resBalancesSummaryTOV2.getPaxSummaryTOV2();

		for (PAXSummaryTOV2 paxSummaryTOV2 : colPAXSummaryTOV2) {
			TravelerCnxModAddResBalances travelerCnxModAddResBalances = new TravelerCnxModAddResBalances();

			AirTraveler airTraveler = passengerMap.get(paxSummaryTOV2.getPnrPaxId());
			travelerCnxModAddResBalances.setTravelerRefNumber(airTraveler.getTravelerRefNumber());
			travelerCnxModAddResBalances.setTravelerName(ReservationApiUtils.getPassengerName(null, airTraveler.getPersonName()
					.getTitle(), airTraveler.getPersonName().getFirstName(), airTraveler.getPersonName().getSurName(), false));

			if (paxSummaryTOV2.getInfantId() != null) {
				AirTraveler infant = passengerMap.get(paxSummaryTOV2.getInfantId());
				travelerCnxModAddResBalances.setTravelerInfantName(ReservationApiUtils.getPassengerName(null, infant
						.getPersonName().getTitle(), infant.getPersonName().getFirstName(), infant.getPersonName().getSurName(),
						false));
			} else {
				travelerCnxModAddResBalances.setTravelerInfantName("");
			}

			travelerCnxModAddResBalances.setPassengerType(airTraveler.getPassengerType());

			AlterationBalances perPaxAlterationBalances = composeAlterationBalances(alterationType,
					paxSummaryTOV2.getSegmentSummaryTOV2(),
					PaxTypeUtils.composePassengerTypes(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxSummaryTOV2.getPaxType())));

			perPaxAlterationBalances.setTotalAmountDue(paxSummaryTOV2.getTotalAmountDue());
			perPaxAlterationBalances.setTotalCreditAmount(paxSummaryTOV2.getTotalCreditAmount());
			perPaxAlterationBalances.setTotalCnxChargeForCurrentOperation(paxSummaryTOV2.getTotalCnxChargeForCurrentAlt());
			perPaxAlterationBalances.setTotalModChargeForCurrentOperation(paxSummaryTOV2.getTotalModChargeForCurrentAlt());
			perPaxAlterationBalances.setTotalPrice(paxSummaryTOV2.getTotalPrice());

			travelerCnxModAddResBalances.setAlterationBalances(perPaxAlterationBalances);

			resCnxModAddResBalances.getTravelerCnxModAddResBalances().add(travelerCnxModAddResBalances);
		}

		alterationBalances.setTotalAmountDue(resBalancesSummaryTOV2.getTotalAmountDue());
		alterationBalances.setTotalCreditAmount(resBalancesSummaryTOV2.getTotalCreditAmount());
		alterationBalances.setTotalCnxChargeForCurrentOperation(resBalancesSummaryTOV2.getTotalCnxChargeForCurrentAlt());
		alterationBalances.setTotalModChargeForCurrentOperation(resBalancesSummaryTOV2.getTotalModChargeForCurrentAlt());
		alterationBalances.setTotalPrice(resBalancesSummaryTOV2.getTotalPrice());

		return resCnxModAddResBalances;
	}

	private static Map<Integer, AirTraveler> getAirTravelers(AAAirBookModifyRQ aaAirBookModifyRQ) {
		Map<Integer, AirTraveler> mapTraveler = new HashMap<Integer, AirTraveler>();

		for (AirTraveler airTraveler : aaAirBookModifyRQ.getAaReservation().getAirReservation().getTravelerInfo()
				.getAirTraveler()) {
			mapTraveler.put(airTraveler.getTravelerSequence(), airTraveler);
		}

		return mapTraveler;
	}

	private static Collection<PAXSummaryTOV2> composePaxSummaryV2(TravelerInfo travelerInfo) {
		Collection<PAXSummaryTOV2> colPAXSummaryTOV2 = new ArrayList<PAXSummaryTOV2>();

		for (AirTraveler airTraveler : travelerInfo.getAirTraveler()) {
			PAXSummaryTOV2 paxSummaryTOV2 = new PAXSummaryTOV2();
			paxSummaryTOV2.setPnrPaxId(airTraveler.getTravelerSequence());
			paxSummaryTOV2.setPaxName(ReservationApiUtils.getPassengerName("", airTraveler.getPersonName().getTitle(),
					airTraveler.getPersonName().getFirstName(), airTraveler.getPersonName().getSurName(), false));
			paxSummaryTOV2.setPaxType(PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(airTraveler.getPassengerType()));
			paxSummaryTOV2.setInfantId(airTraveler.getAccompaniedSequence());

			colPAXSummaryTOV2.add(paxSummaryTOV2);
		}

		return colPAXSummaryTOV2;
	}

	public static Map<Integer, List<ExternalChgDTO>> convertLCCClientDTOChgToAAChgDTO(
			Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges) throws ModuleException {

		Map<Integer, List<ExternalChgDTO>> paxCharges = new HashMap<>();

		if (paxExternalCharges != null && !paxExternalCharges.isEmpty()) {

			Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);

			Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = AirproxyModuleUtils.getReservationBD()
					.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null);

			if (extChgMap != null && !extChgMap.isEmpty()) {
				ExternalChgDTO externalChgDTO = BeanUtils.getFirstElement(extChgMap.values());
				if (externalChgDTO != null) {
					for (Integer paxSeq : paxExternalCharges.keySet()) {
						List<LCCClientExternalChgDTO> charges = paxExternalCharges.get(paxSeq);
						if (charges != null && !charges.isEmpty()) {
							for (LCCClientExternalChgDTO paxExtChg : charges) {

								externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
								externalChgDTO.setAmountConsumedForPayment(paxExtChg.isAmountConsumedForPayment());
								externalChgDTO.setAmount(paxExtChg.getAmount());
								externalChgDTO.setFlightSegId(
										FlightRefNumberUtil.getSegmentIdFromFlightRPH(paxExtChg.getFlightRefNumber()));

								if (EXTERNAL_CHARGES.HANDLING_CHARGE == externalChgDTO.getExternalChargesEnum()
										&& paxExtChg.getOperationMode() != null) {
									externalChgDTO.setOperationMode(paxExtChg.getOperationMode());
								}

								if (paxCharges.get(paxSeq) == null) {
									paxCharges.put(paxSeq, new ArrayList<>());
								}

								paxCharges.get(paxSeq).add(externalChgDTO);
							}
						}

					}
				}
			}
		}

		return paxCharges;

	}
	
	public static Map<Integer, List<ExternalChgDTO>>
			tranformPaxExternalChargeToExternalChgDTO(List<PerPaxExternalCharges> ppExtChgList) throws ModuleException {
		Map<Integer, List<ExternalChgDTO>> paxCharges = null;
		if (ppExtChgList != null && !ppExtChgList.isEmpty()) {
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap = composePerPaxExternalChargeMap(ppExtChgList);
			paxCharges = convertLCCClientDTOChgToAAChgDTO(paxExtChargeMap);
		}

		return paxCharges;
	}
}