/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2009 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aaservices.core.config.AAServicesConfig;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.aircustomer.api.utils.AircustomerConstants;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.service.TranslationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.service.AirproxyPassengerBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.promotion.api.service.AnciOfferBD;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.webplatform.core.util.LookupUtils;
import com.isa.thinair.webplatform.core.util.WebServicesDAO;
import com.isa.thinair.wsclient.api.service.InsuranceClientBD;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

/**
 * @author Nilindra Fernando
 */
public class AAServicesModuleUtils {
	private static Log log = LogFactory.getLog(AAServicesModuleUtils.class);

	/**
	 * Lookup an exernal module
	 * 
	 * @param moduleName
	 * @return IModule Module Implemenation
	 */
	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getWebServicesConfig(), "webservices",
				"webservices.config.dependencymap.invalid");
	}

	public static AAServicesConfig getWebServicesConfig() {
		return (AAServicesConfig) LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/aaservices?id=aaservicesModuleConfig");
	}

	/**
	 * Retrieves application's global configurations
	 * 
	 * @return GlobalConfig
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Return reservation query service delegate
	 * 
	 * @return
	 */
	public static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Return reservation service delegate
	 * 
	 * @return
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	/**
	 * Return reservation auxillary service delegate
	 * 
	 * @return
	 */
	public static ReservationAuxilliaryBD getReservationAuxillaryBD() {
		return (ReservationAuxilliaryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	/**
	 * Return passenger service delegate
	 * 
	 * @return
	 */
	public static PassengerBD getPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	/**
	 * Return flight service delegate
	 * 
	 * @return
	 */
	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static FareBD getFareBD() {
		return (FareBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static SegmentBD getSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);
	}

	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static DataExtractionBD getDataExtractionBD() {
		return (DataExtractionBD) lookupEJB3Service(ReportingConstants.MODULE_NAME, DataExtractionBD.SERVICE_NAME);
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}
	
	public static InsuranceClientBD getInsuranceClientBD() {
		return (InsuranceClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, InsuranceClientBD.SERVICE_NAME);
	}


	static WebServicesDAO getWebServiceDAO() {
		return LookupUtils.getWebServiceDAO();
	}

	public static AlertingBD getAlertingBD() {
		return (AlertingBD) lookupEJB2ServiceBD(AlertingConstants.MODULE_NAME, AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	/**
	 * Return transparent auditor delegate
	 * 
	 * @return
	 */
	public static AuditorBD getAuditorServiceBD() {
		return (AuditorBD) AAServicesModuleUtils.lookupEJB2ServiceBD(AuditorConstants.MODULE_NAME,
				AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	/**
	 * Returns EJB2 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupEJB2ServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getWebServicesConfig(),
				"aaservices", "aaservices.config.dependencymap.invalid");
	}

	/**
	 * Gets an SeatMap Service from Inventory
	 * 
	 * @return SeatMapBD the SeatMap delegate
	 */
	public final static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) AAServicesModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	public static SsrBD getSsrServiceBD() {
		if (log.isDebugEnabled()) {
			log.debug("Retrieving SsrBD : " + SsrBD.class);
		}
		return (SsrBD) AAServicesModuleUtils.lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}

	/**
	 * gets the meal biz delegate
	 * 
	 * @return MealBD
	 */
	public final static MealBD getMealBD() {
		if (log.isDebugEnabled()) {
			log.debug("Retrieving MealBD : " + MealBD.class);
		}
		return (MealBD) AAServicesModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
	}

	/**
	 * gets the baggage biz delegate
	 * 
	 * @return MealBD
	 */
	public final static BaggageBusinessDelegate getBaggageBD() {
		if (log.isDebugEnabled()) {
			log.debug("Retrieving BaggageBD : " + BaggageBusinessDelegate.class);
		}
		return (BaggageBusinessDelegate) AAServicesModuleUtils.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}

	/**
	 * Gets fare Rule Service from Air Pricing
	 * 
	 * @return FareRuleBD the Fare Rule delegate
	 */
	public final static FareRuleBD getFareRuleBD() {

		FareRuleBD fareRuleBD = null;
		try {
			fareRuleBD = (FareRuleBD) AAServicesModuleUtils.lookupEJB3Service(AirpricingConstants.MODULE_NAME,
					FareRuleBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating FareClassBD", e);
		}
		return fareRuleBD;
	}

	public final static AirproxyReservationQueryBD getAirproxySearchAndQuoteBD() {
		return (AirproxyReservationQueryBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
				AirproxyReservationQueryBD.SERVICE_NAME);
	}

	public final static AirproxyPassengerBD getAirProxyPassengerBD() {
		return (AirproxyPassengerBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyPassengerBD.SERVICE_NAME);
	}

	public final static AirproxyReservationBD getAirproxyReservationBD() {
		AirproxyReservationBD airproxyReservationBD = null;
		try {
			airproxyReservationBD = (AirproxyReservationBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyReservationQueryBD.SERVICE_NAME);
		} catch (Exception ex) {
			log.error("Error locating AirproxyReservationBD", ex);
		}
		return airproxyReservationBD;
	}

	public final static TranslationBD getTranslationBD() {
		return (TranslationBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, TranslationBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public static PromotionCriteriaAdminBD getPromotionCriteriaAdminBD() {
		return (PromotionCriteriaAdminBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, PromotionCriteriaAdminBD.SERVICE_NAME);
	}

	public static LocationBD getLocationServiceBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	public final static AnciOfferBD getAnciOfferBD() {
		return (AnciOfferBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, AnciOfferBD.SERVICE_NAME);
	}

	public static BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, BunldedFareBD.SERVICE_NAME);
	}

	public final static LmsMemberServiceBD getLmsMemberServiceBD() {
		return (LmsMemberServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, LmsMemberServiceBD.SERVICE_NAME);

	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, LoyaltyManagementBD.SERVICE_NAME);
	}
	

	public final static AirCustomerServiceBD getAirCustomerServiceBD() {
		return (AirCustomerServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, AirCustomerServiceBD.SERVICE_NAME);
	}
	public final static BlacklistPAXBD getBlacklistPAXBD(){
		return (BlacklistPAXBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, BlacklistPAXBD.SERVICE_NAME);
	} 
}