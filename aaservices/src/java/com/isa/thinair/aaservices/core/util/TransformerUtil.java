package com.isa.thinair.aaservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicablePromotionDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Baggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BundleFareDescriptionInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BundleFareTranslationInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustmentType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClientExternalChg;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClientPaymentAssembler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonAnciModifyAssembler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CustomChargeDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Eticket;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentInfoTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerDateMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerDecimalMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerPaxNameInfoMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerStringMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Meal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerPaymentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxExternalChargesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxNameInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCurrencyAmount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxExternalCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PromotionSelectionCriteria;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Seat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSegmentAncillary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringBundledFareTranslationMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringDecimalMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringDoubleMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TrackInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.UserNote;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.dto.seatavailability.FarePriceOnd;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

public class TransformerUtil {

	public static CommonAncillaryModifyAssembler transformAnciAssembler(CommonAnciModifyAssembler commonAnciModifyAssembler) {

		CommonAncillaryModifyAssembler assembler = new CommonAncillaryModifyAssembler();

		assembler.setPnr(commonAnciModifyAssembler.getPnr());
		assembler.setTargetSystem(SYSTEM.AA);
		assembler.setTemporyPaymentMap(transformTempPaymentMap(commonAnciModifyAssembler.getTempPaymentMap()));
		assembler.setTransactionIdentifier(commonAnciModifyAssembler.getTransactionIdentifier());
		assembler.setVersion(commonAnciModifyAssembler.getVersion());
		assembler.getPaxAddAncillaryMap().putAll(transformAnciMap(commonAnciModifyAssembler.getPaxAddAncillaryMap()));
		assembler.getPaxRemoveAncillaryMap().putAll(transformAnciMap(commonAnciModifyAssembler.getPaxRemoveAncillaryMap()));
		assembler.getPaxUpdateAncillaryMap().putAll(transformAnciMap(commonAnciModifyAssembler.getPaxUpdateAncillaryMap()));
		assembler.getPassengerPaymentMap().putAll(transformPaymentMap(commonAnciModifyAssembler.getPassengerPaymentMap()));
		assembler.setForceConfirm(commonAnciModifyAssembler.isForceConfirm());

		return assembler;
	}

	private static Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> transformAnciMap(List<AncillaryMap> anciMap) {
		Map<Integer, List<LCCSelectedSegmentAncillaryDTO>> transformedMap = new HashMap<Integer, List<LCCSelectedSegmentAncillaryDTO>>();

		for (AncillaryMap map : anciMap) {
			List<LCCSelectedSegmentAncillaryDTO> lccAncillaryDTOs = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
			List<SelectedSegmentAncillary> segmentAncillaries = map.getValue();

			for (SelectedSegmentAncillary segmentAnci : segmentAncillaries) {
				LCCSelectedSegmentAncillaryDTO ancillaryDTO = new LCCSelectedSegmentAncillaryDTO();

				ancillaryDTO.setAirSeatDTO((segmentAnci.getAirSeatDTO() != null)
						? transformSeat(segmentAnci.getAirSeatDTO())
						: null);
				ancillaryDTO.setCarrierCode(segmentAnci.getCarrierCode());
				ancillaryDTO.setFlightSegmentTO((segmentAnci.getFlightSegmentTO() != null) ? transformFlightSegment(segmentAnci
						.getFlightSegmentTO()) : null);
				ancillaryDTO.setExternalFlightSegments(composeExternalSegments(segmentAnci));
								
				List<InsuranceQuotation> insuraceQuotations = segmentAnci.getInsuranceQuotationDTOs();
				ancillaryDTO.setInsuranceQuotations((insuraceQuotations != null)
						? transformInsuranceQuotation(insuraceQuotations)
						: null);
				
				ancillaryDTO
						.setMealDTOs((segmentAnci.getMealDTOs() != null) ? tranformMealDTOs(segmentAnci.getMealDTOs()) : null);
				ancillaryDTO.setBaggageDTOs((segmentAnci.getBaggageDTOs() != null) ? tranformBaggageDTOs(segmentAnci
						.getBaggageDTOs()) : null);
				ancillaryDTO.setSpecialServiceRequestDTOs((segmentAnci.getSpecialServiceRequestDTOs() != null)
						? transformSSRDTOs(segmentAnci.getSpecialServiceRequestDTOs())
						: null);

				ancillaryDTO.setAirportServiceDTOs((segmentAnci.getAirportServiceRequestDTOs() != null)
						? transformAirportServiceDTOs(segmentAnci.getAirportServiceRequestDTOs())
						: null);

				ancillaryDTO.setTravelerRefNumber(segmentAnci.getTravelerRefNumber());

				lccAncillaryDTOs.add(ancillaryDTO);
			}

			transformedMap.put(map.getKey(), lccAncillaryDTOs);
		}

		return transformedMap;
	}

	private static List<FlightSegmentTO> composeExternalSegments(SelectedSegmentAncillary segmentAnci) {
		List<FlightSegmentTO> externalFlightSegments = new ArrayList<FlightSegmentTO>();

		if (segmentAnci.getExternalFlightSegmentTOs() != null && segmentAnci.getExternalFlightSegmentTOs().size() > 0) {
			for (FlightSegmentInfoTO flightSegmentInfoTO : segmentAnci.getExternalFlightSegmentTOs()) {
				externalFlightSegments.add(transformFlightSegment(flightSegmentInfoTO));
			}
		}

		return externalFlightSegments;
	}

	private static LCCAirSeatDTO transformSeat(Seat seat) {
		LCCAirSeatDTO airSeatDTO = new LCCAirSeatDTO();

		airSeatDTO.setBookedPassengerType(seat.getBookedPassengerType().toString());
		// airSeatDTO.setNotAllowedPassengerType(seat.getNotAllowedPassengerType())
		airSeatDTO.setSeatAvailability(seat.getSeatAvailability());
		airSeatDTO.setSeatCharge(seat.getSeatCharge());
		airSeatDTO.setSeatMessage(seat.getSeatMessage());
		airSeatDTO.setSeatNumber(seat.getSeatNumber());
		airSeatDTO.setSeatType((seat.getSeatType() != null) ? seat.getSeatType().toString() : null);
		airSeatDTO.setSeatLocationType(seat.getSeatLocationType());

		List<String> notAllowedTypes = new ArrayList<String>();
		for (PassengerType passengerType : seat.getNotAllowedPassengerType()) {
			switch (passengerType) {
			case ADT:
				notAllowedTypes.add(PaxTypeTO.ADULT);
				break;
			case CHD:
				notAllowedTypes.add(PaxTypeTO.CHILD);
				break;
			case PRT:
				notAllowedTypes.add(PaxTypeTO.PARENT);
				break;
			case INF:
				notAllowedTypes.add(PaxTypeTO.INFANT);
				break;
			}
		}

		airSeatDTO.setNotAllowedPassengerType(notAllowedTypes);

		return airSeatDTO;
	}

	private static FlightSegmentTO transformFlightSegment(FlightSegmentInfoTO flightSegmentInfoTO) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

		flightSegmentTO.setAdultCount(flightSegmentInfoTO.getAdultCount());
		flightSegmentTO.setArrivalDateTime(flightSegmentInfoTO.getArrivalDateTime());
		flightSegmentTO.setArrivalDateTimeZulu(flightSegmentInfoTO.getDepartureDateTime());
		flightSegmentTO.setArrivalTerminalName(flightSegmentTO.getArrivalTerminalName());
		flightSegmentTO.setCabinClassCode(flightSegmentInfoTO.getCabinClassCode());
		flightSegmentTO.setChildCount(flightSegmentInfoTO.getChildCount());
		flightSegmentTO.setDepartureDateTime(flightSegmentInfoTO.getDepartureDateTime());
		flightSegmentTO.setDepartureDateTimeZulu(flightSegmentInfoTO.getDepartureDateTimeZulu());
		if (flightSegmentInfoTO.getFlightId() != null) {
			flightSegmentTO.setFlightId(Integer.parseInt(flightSegmentInfoTO.getFlightId()));
		}
		flightSegmentTO.setFlightNumber(flightSegmentInfoTO.getFlightNumber());
		flightSegmentTO.setFlightRefNumber(flightSegmentInfoTO.getFlightRefNumber());
		if (flightSegmentInfoTO.getFlightSegId() != null) {
			flightSegmentTO.setFlightSegId(Integer.parseInt(flightSegmentInfoTO.getFlightSegId()));
		}
		flightSegmentTO.setInfantCount(flightSegmentTO.getInfantCount());
		flightSegmentTO.setOperatingAirline(flightSegmentInfoTO.getOperatingAirline());
		flightSegmentTO.setOperationType(flightSegmentInfoTO.getOperationType());
		// flightSegmentTO.setPassengerTypeQuantityList(flightSegmentInfoTO.get)
		flightSegmentTO.setReturnFlag(flightSegmentInfoTO.isReturnFlag());
		flightSegmentTO.setSegmentCode(flightSegmentInfoTO.getSegmentCode());
		flightSegmentTO.setSubStationShortName(flightSegmentInfoTO.getSubStationShortName());
		flightSegmentTO.setBaggageONDGroupId(flightSegmentInfoTO.getBaggageONDGroupId());
		flightSegmentTO.setPnrSegId(flightSegmentInfoTO.getBookingFlightRefNumber());

		return flightSegmentTO;
	}

	private static List<LCCInsuranceQuotationDTO> transformInsuranceQuotation(List<InsuranceQuotation> insuranceQuotations) {

		List<LCCInsuranceQuotationDTO> lccQuotationDTOs = new ArrayList<LCCInsuranceQuotationDTO>();

		for (InsuranceQuotation insuranceQuotation : insuranceQuotations) {
			LCCInsuranceQuotationDTO lccQuotationDTO = new LCCInsuranceQuotationDTO();
			lccQuotationDTO.setPolicyCode(insuranceQuotation.getPolicyCode());
			lccQuotationDTO.setQuotedCurrencyCode(insuranceQuotation.getQuotedCurrencyCode());
			lccQuotationDTO.setQuotedTotalPremiumAmount(insuranceQuotation.getQuotedTotalPremiumAmount());
			lccQuotationDTO.setOperatingAirline(insuranceQuotation.getOperatingAirline());
			lccQuotationDTO.setInsuranceRefNumber(Integer.toString(insuranceQuotation.getInsuranceRefNumber()));
			lccQuotationDTO.setSsrFeeCode(insuranceQuotation.getSsrFeeCode());
			lccQuotationDTO.setPlanCode(insuranceQuotation.getPlanCode());

			lccQuotationDTOs.add(lccQuotationDTO);
		}

		return lccQuotationDTOs;
	}

	private static List<LCCMealDTO> tranformMealDTOs(List<Meal> mealList) {
		List<LCCMealDTO> lccMealDTOs = new ArrayList<LCCMealDTO>();

		for (Meal meal : mealList) {
			LCCMealDTO lccMealDTO = new LCCMealDTO();

			lccMealDTO.setMealCode(meal.getMealCode());
			lccMealDTO.setMealDescription(meal.getMealDescription());
			lccMealDTO.setMealCharge(meal.getMealCharge());
			lccMealDTO.setMealName(meal.getMealName());
			lccMealDTO.setAvailableMeals(meal.getAvailableMeals());
			lccMealDTO.setSoldMeals(meal.getSoldMeals());
			lccMealDTO.setAllocatedMeals(meal.getAllocatedMeals());
			lccMealDTO.setMealImagePath(meal.getMealImageLink());
			lccMealDTO.setTranslatedMealDescription(meal.getTranslatedMealDescription());
			lccMealDTO.setTranslatedMealTitle(meal.getTranslatedMealTitle());
			lccMealDTO.setMealCategoryID(meal.getMealCategoryID());
			lccMealDTO.setMealCategoryCode(meal.getMealCategoryCode());
			lccMealDTO.setAnciOffer(meal.isIsAnciOffer());
			lccMealDTO.setOfferedTemplateID(meal.getOfferedAnciTemplateID());
			lccMealDTOs.add(lccMealDTO);
		}

		return lccMealDTOs;
	}

	private static List<LCCBaggageDTO> tranformBaggageDTOs(List<Baggage> baggageList) {
		List<LCCBaggageDTO> lccBaggageDTOs = new ArrayList<LCCBaggageDTO>();

		for (Baggage baggage : baggageList) {
			LCCBaggageDTO lccBaggageDTO = new LCCBaggageDTO();

			lccBaggageDTO.setBaggageDescription(baggage.getBaggageDescription());
			lccBaggageDTO.setBaggageCharge(baggage.getBaggageCharge());
			lccBaggageDTO.setBaggageName(baggage.getBaggageName());
			lccBaggageDTO.setOndBaggageChargeId(baggage.getOndChargeId());

			// lccBaggageDTO.setTranslatedBaggageDescription(baggage.getTranslatedBaggageDescription());
			// lccBaggageDTO.setTranslatedBaggageTitle(baggage.getTranslatedBaggageTitle());

			lccBaggageDTOs.add(lccBaggageDTO);
		}

		return lccBaggageDTOs;
	}

	private static List<LCCSpecialServiceRequestDTO> transformSSRDTOs(List<SpecialServiceRequest> ssrs) {
		List<LCCSpecialServiceRequestDTO> lccSSRList = new ArrayList<LCCSpecialServiceRequestDTO>();

		for (SpecialServiceRequest ssr : ssrs) {
			LCCSpecialServiceRequestDTO lccSSRDTO = new LCCSpecialServiceRequestDTO();

			lccSSRDTO.setSsrCode(ssr.getSsrCode());
			lccSSRDTO.setDescription(ssr.getDescription());
			lccSSRDTO.setServiceQuantity(ssr.getServiceQuantity());
			lccSSRDTO.setStatus(ssr.getStatus());
			lccSSRDTO.setCarrierCode(ssr.getCarrierCode());
			lccSSRDTO.setCharge(ssr.getCharge());
			lccSSRDTO.setText(ssr.getText());
			lccSSRDTO.setAvailableQty(ssr.getAvailableQty());

			lccSSRList.add(lccSSRDTO);
		}

		return lccSSRList;
	}

	private static List<LCCAirportServiceDTO> transformAirportServiceDTOs(List<AirportServiceRequest> apsReqList) {
		List<LCCAirportServiceDTO> airportServiceDTOs = new ArrayList<LCCAirportServiceDTO>();

		for (AirportServiceRequest airportServiceRequest : apsReqList) {
			LCCAirportServiceDTO airportServiceDTO = new LCCAirportServiceDTO();

			airportServiceDTO.setAirportCode(airportServiceRequest.getAirportCode());
			airportServiceDTO.setApplicabilityType(airportServiceRequest.getApplicabilityType());
			airportServiceDTO.setCarrierCode(airportServiceRequest.getCarrierCode());
			airportServiceDTO.setServiceCharge(airportServiceRequest.getServiceCharge());
			airportServiceDTO.setSsrCode(airportServiceRequest.getSsrCode());
			airportServiceDTO.setSsrDescription(airportServiceRequest.getDescription());
			airportServiceDTO.setApplyOn(airportServiceRequest.getApplyOn());

			airportServiceDTOs.add(airportServiceDTO);
		}

		return airportServiceDTOs;
	}

	public static Map<String, LCCClientPaymentAssembler> transformLccPaymentMap(List<PassengerPaymentMap> paymentMapList) {
		Map<String, LCCClientPaymentAssembler> trnPaymentMap = new HashMap<String, LCCClientPaymentAssembler>();

		for (PassengerPaymentMap passengerPaymentMap : paymentMapList) {
			LCCClientPaymentAssembler lccAssembler = new LCCClientPaymentAssembler();

			ClientPaymentAssembler assembler = passengerPaymentMap.getValue();
			lccAssembler.getPerPaxExternalCharges().addAll(transformExcharges(assembler.getPerPaxExternalCharges()));

			trnPaymentMap.put(passengerPaymentMap.getKey().toString(), lccAssembler);

		}

		return trnPaymentMap;
	}

	private static Map<Integer, LCCClientPaymentAssembler> transformPaymentMap(List<PassengerPaymentMap> paymentMapList) {
		Map<Integer, LCCClientPaymentAssembler> trnPaymentMap = new HashMap<Integer, LCCClientPaymentAssembler>();

		for (PassengerPaymentMap passengerPaymentMap : paymentMapList) {
			LCCClientPaymentAssembler lccAssembler = new LCCClientPaymentAssembler();

			ClientPaymentAssembler assembler = passengerPaymentMap.getValue();

			// lccAssembler.getPayments().addAll(transformAndCombineCashPayments(assembler.getPaymentsClientCash()));
			// lccAssembler.getPayments().addAll(transformAndCombineCCPayments(assembler.getPaymentsCCommonCreditCard()));
			// lccAssembler.getPayments().addAll(transformAndCombineOnAccPayaments(assembler.getPaymentsClientOnAccount()));
			// lccAssembler.getPayments().addAll(transformAndCombinePaxCrdpayments(assembler.getPaymentsClientPaxCredit()));
			// lccAssembler.setPayCurrencyDTO(transformPayCurrency(assembler.getPayCurrencyDTO()));
			lccAssembler.getPerPaxExternalCharges().addAll(transformExcharges(assembler.getPerPaxExternalCharges()));

			trnPaymentMap.put(passengerPaymentMap.getKey(), lccAssembler);

		}

		return trnPaymentMap;
	}

	/*
	 * private static List<CommonCreditCardPaymentInfo> transformAndCombineCCPayments(List<CommonCreditCardPayment>
	 * creditCardPayments){ List<CommonCreditCardPaymentInfo> creditCardPaymentInfos = new
	 * ArrayList<CommonCreditCardPaymentInfo>();
	 * 
	 * for(CommonCreditCardPayment payment : creditCardPayments){ CommonCreditCardPaymentInfo
	 * commonCreditCardPaymentInfo = new CommonCreditCardPaymentInfo();
	 * 
	 * ClientPaymentInfo commonInfo = payment.getClientPaymentInfo();
	 * 
	 * commonCreditCardPaymentInfo.setActualPaymentMethod(commonInfo.getActualPaymentMethod());
	 * commonCreditCardPaymentInfo.setAddress(payment.getAddress());
	 * commonCreditCardPaymentInfo.setAppIndicator((payment
	 * .getAppIndicator().equals(AppIndicatorEnum.APP_IBE.toString())
	 * )?AppIndicatorEnum.APP_IBE:AppIndicatorEnum.APP_XBE);
	 * commonCreditCardPaymentInfo.setAuthorizationId(payment.getAuthorizationId());
	 * commonCreditCardPaymentInfo.setCardHoldersName(payment.getCardHoldersName());
	 * commonCreditCardPaymentInfo.setCarrierCode(commonInfo.getCarrierCode());
	 * commonCreditCardPaymentInfo.seteDate(payment.getEDate());
	 * commonCreditCardPaymentInfo.setGroupPNR(payment.getGroupPNR());
	 * commonCreditCardPaymentInfo.setIncludeExternalCharges(commonInfo.isIncludeExternalCharges());
	 * commonCreditCardPaymentInfo.setLccPaxTnxId(commonInfo.getLccPaxTnxId());
	 * commonCreditCardPaymentInfo.setLccUniqueTnxId(commonInfo.getLccUniqueTnxId());
	 * commonCreditCardPaymentInfo.setName(payment.getName()); commonCreditCardPaymentInfo.setNo(payment.getNo());
	 * commonCreditCardPaymentInfo.setNoFirstDigits(payment.getNoFirstDigits());
	 * commonCreditCardPaymentInfo.setNoLastDigits(payment.getNoLastDigits());
	 * commonCreditCardPaymentInfo.setOldCCRecordId(payment.getOldCCRecordId());
	 * commonCreditCardPaymentInfo.setOriginalPayReference(commonInfo.getOriginalPayReference());
	 * commonCreditCardPaymentInfo.setPayCurrencyAmount(commonInfo.getPayCurrencyAmount());
	 * commonCreditCardPaymentInfo.setPayCurrencyDTO(transformPayCurrency(commonInfo.getPayCurrencyDTO()));
	 * commonCreditCardPaymentInfo.setPaymentBrokerId(payment.getPaymentBrokerId());
	 * commonCreditCardPaymentInfo.setPaymentSuccess(payment.isIsPaymentSuccess());
	 * commonCreditCardPaymentInfo.setPayReference(commonInfo.getPayReference());
	 * commonCreditCardPaymentInfo.setRecieptNumber(commonInfo.getRecieptNumber());
	 * commonCreditCardPaymentInfo.setRemarks(commonInfo.getRemarks());
	 * commonCreditCardPaymentInfo.setSecurityCode(payment.getSecurityCode());
	 * commonCreditCardPaymentInfo.setTemporyPaymentId(payment.getTemporyPaymentId());
	 * commonCreditCardPaymentInfo.setTnxMode
	 * ((payment.getTnxMode().intValue()==TnxModeEnum.MAIL_TP_ORDER.getCode())?TnxModeEnum
	 * .MAIL_TP_ORDER:TnxModeEnum.SECURE_3D); commonCreditCardPaymentInfo.setTotalAmount(commonInfo.getTotalAmount());
	 * commonCreditCardPaymentInfo.setTotalAmountCurrencyCode(commonInfo.getTotalAmountCurrencyCode());
	 * commonCreditCardPaymentInfo.setTxnDateTime(commonInfo.getTxnDateTime());
	 * commonCreditCardPaymentInfo.setType(payment.getType());
	 * 
	 * IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new
	 * IPGIdentificationParamsDTO(payment.getIpgIdentificationParamsDTO().getFullyQualifiedIPGConfigName());
	 * 
	 * ipgIdentificationParamsDTO.set_isIPGConfigurationExists(payment.getIpgIdentificationParamsDTO().
	 * isIPGConfigurationExists());
	 * ipgIdentificationParamsDTO.setIpgId(payment.getIpgIdentificationParamsDTO().getIpgId());
	 * 
	 * commonCreditCardPaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
	 * 
	 * creditCardPaymentInfos.add(commonCreditCardPaymentInfo);
	 * 
	 * } return creditCardPaymentInfos; }
	 * 
	 * private static List<LCCClientCashPaymentInfo> transformAndCombineCashPayments(List<ClientCashPaymentInfo>
	 * clientCashPayments){ List<LCCClientCashPaymentInfo> cashPaymentInfoList = new
	 * ArrayList<LCCClientCashPaymentInfo>();
	 * 
	 * for(ClientCashPaymentInfo payment : clientCashPayments){ LCCClientCashPaymentInfo lccClPaymentInfo = new
	 * LCCClientCashPaymentInfo();
	 * 
	 * ClientPaymentInfo commonInfo = payment.getClientPaymentInfo();
	 * 
	 * lccClPaymentInfo.setActualPaymentMethod(commonInfo.getActualPaymentMethod());
	 * lccClPaymentInfo.setCarrierCode(commonInfo.getCarrierCode());
	 * lccClPaymentInfo.setIncludeExternalCharges(commonInfo.isIncludeExternalCharges());
	 * lccClPaymentInfo.setLccPaxTnxId(commonInfo.getLccPaxTnxId());
	 * lccClPaymentInfo.setLccUniqueTnxId(commonInfo.getLccUniqueTnxId());
	 * lccClPaymentInfo.setOriginalPayReference(commonInfo.getOriginalPayReference());
	 * lccClPaymentInfo.setPayCurrencyAmount(commonInfo.getPayCurrencyAmount());
	 * lccClPaymentInfo.setPayCurrencyDTO(transformPayCurrency(commonInfo.getPayCurrencyDTO()));
	 * lccClPaymentInfo.setPayReference(commonInfo.getPayReference());
	 * lccClPaymentInfo.setRecieptNumber(commonInfo.getRecieptNumber());
	 * lccClPaymentInfo.setRemarks(commonInfo.getRemarks());
	 * lccClPaymentInfo.setTotalAmount(commonInfo.getTotalAmount());
	 * lccClPaymentInfo.setTotalAmountCurrencyCode(commonInfo.getTotalAmountCurrencyCode());
	 * lccClPaymentInfo.setTxnDateTime(commonInfo.getTxnDateTime());
	 * 
	 * cashPaymentInfoList.add(lccClPaymentInfo);
	 * 
	 * }
	 * 
	 * return cashPaymentInfoList; }
	 * 
	 * private static List<LCCClientOnAccountPaymentInfo>
	 * transformAndCombineOnAccPayaments(List<ClientOnAccountPaymentInfo> clientOnAccPayments){
	 * List<LCCClientOnAccountPaymentInfo> onAccPaymentList = new ArrayList<LCCClientOnAccountPaymentInfo>();
	 * 
	 * for(ClientOnAccountPaymentInfo payment : clientOnAccPayments){ LCCClientOnAccountPaymentInfo
	 * lccOnAccountPaymentInfo = new LCCClientOnAccountPaymentInfo();
	 * 
	 * ClientPaymentInfo commonInfo = payment.getClientPaymentInfo();
	 * 
	 * lccOnAccountPaymentInfo.setActualPaymentMethod(commonInfo.getActualPaymentMethod());
	 * lccOnAccountPaymentInfo.setAgentCode(payment.getAgentCode());
	 * lccOnAccountPaymentInfo.setAgentName(payment.getAgentName());
	 * lccOnAccountPaymentInfo.setCarrierCode(commonInfo.getCarrierCode());
	 * lccOnAccountPaymentInfo.setExternalAgentCode(payment.getExternalAgentCode());
	 * lccOnAccountPaymentInfo.setExternalCarrierCode(payment.getExternalCarrierCode());
	 * lccOnAccountPaymentInfo.setIncludeExternalCharges(commonInfo.isIncludeExternalCharges());
	 * lccOnAccountPaymentInfo.setLccPaxTnxId(commonInfo.getLccPaxTnxId());
	 * lccOnAccountPaymentInfo.setLccUniqueTnxId(commonInfo.getLccUniqueTnxId());
	 * lccOnAccountPaymentInfo.setOriginalPayReference(commonInfo.getOriginalPayReference());
	 * lccOnAccountPaymentInfo.setPayCurrencyAmount(commonInfo.getPayCurrencyAmount());
	 * lccOnAccountPaymentInfo.setPayCurrencyDTO(transformPayCurrency(commonInfo.getPayCurrencyDTO()));
	 * lccOnAccountPaymentInfo.setPayReference(commonInfo.getPayReference());
	 * lccOnAccountPaymentInfo.setRecieptNumber(commonInfo.getRecieptNumber());
	 * lccOnAccountPaymentInfo.setRemarks(commonInfo.getRemarks());
	 * lccOnAccountPaymentInfo.setTotalAmount(commonInfo.getTotalAmount());
	 * lccOnAccountPaymentInfo.setTotalAmountCurrencyCode(commonInfo.getTotalAmountCurrencyCode());
	 * lccOnAccountPaymentInfo.setTxnDateTime(commonInfo.getTxnDateTime());
	 * 
	 * onAccPaymentList.add(lccOnAccountPaymentInfo); }
	 * 
	 * return onAccPaymentList; }
	 * 
	 * private static List<LCCClientPaxCreditPaymentInfo>
	 * transformAndCombinePaxCrdpayments(List<ClientPaxCreditPaymentInfo> clientPaxCrdPayments){
	 * List<LCCClientPaxCreditPaymentInfo> paxCrdPaymentList = new ArrayList<LCCClientPaxCreditPaymentInfo>();
	 * 
	 * for(ClientPaxCreditPaymentInfo payment : clientPaxCrdPayments){ LCCClientPaxCreditPaymentInfo paxCrdPaymentInfo =
	 * new LCCClientPaxCreditPaymentInfo();
	 * 
	 * ClientPaymentInfo commonInfo = payment.getClientPaymentInfo();
	 * 
	 * paxCrdPaymentInfo.setActualPaymentMethod(commonInfo.getActualPaymentMethod());
	 * paxCrdPaymentInfo.setCarrierCode(commonInfo.getCarrierCode());
	 * paxCrdPaymentInfo.setDebitPaxRefNo(payment.getDebitPaxRefNo());
	 * paxCrdPaymentInfo.setDescription(payment.getDescription());
	 * paxCrdPaymentInfo.setIncludeExternalCharges(commonInfo.isIncludeExternalCharges());
	 * paxCrdPaymentInfo.setLccPaxTnxId(commonInfo.getLccPaxTnxId());
	 * paxCrdPaymentInfo.setLccUniqueTnxId(commonInfo.getLccUniqueTnxId());
	 * paxCrdPaymentInfo.setOriginalPayReference(commonInfo.getOriginalPayReference());
	 * paxCrdPaymentInfo.setPayCurrencyAmount(commonInfo.getPayCurrencyAmount());
	 * paxCrdPaymentInfo.setPayCurrencyDTO(transformPayCurrency(commonInfo.getPayCurrencyDTO()));
	 * paxCrdPaymentInfo.setPayReference(commonInfo.getPayReference()); paxCrdPaymentInfo.setPnr(payment.getPnr());
	 * paxCrdPaymentInfo.setRecieptNumber(commonInfo.getRecieptNumber());
	 * paxCrdPaymentInfo.setRemarks(commonInfo.getRemarks());
	 * paxCrdPaymentInfo.setTotalAmount(commonInfo.getTotalAmount());
	 * paxCrdPaymentInfo.setTotalAmountCurrencyCode(commonInfo.getTotalAmountCurrencyCode());
	 * paxCrdPaymentInfo.setTxnDateTime(commonInfo.getTxnDateTime());
	 * 
	 * paxCrdPaymentList.add(paxCrdPaymentInfo); }
	 * 
	 * return paxCrdPaymentList; }
	 * 
	 * private static PayCurrencyDTO transformPayCurrency(PayCurrency payCurrency){ PayCurrencyDTO payCurrencyDTO = new
	 * PayCurrencyDTO(payCurrency.getPayCurrencyCode(), payCurrency.getPayCurrMultiplyingExchangeRate());
	 * 
	 * payCurrencyDTO.setBoundaryValue(payCurrency.getBoundaryValue());
	 * payCurrencyDTO.setBreakPointValue(payCurrency.getBreakPointValue());
	 * payCurrencyDTO.setPayCurrencyExchangeRateId(payCurrency.getPayCurrencyExchangeRateId());
	 * payCurrencyDTO.setTotalPayCurrencyAmount(payCurrency.getTotalPayCurrencyAmount());
	 * 
	 * return payCurrencyDTO; }
	 */
	public static List<LCCClientExternalChgDTO> transformExcharges(List<ClientExternalChg> externalChargesList) {
		List<LCCClientExternalChgDTO> lccChgDTOs = new ArrayList<LCCClientExternalChgDTO>();

		for (ClientExternalChg externalChg : externalChargesList) {
			LCCClientExternalChgDTO lccChgDTO = new LCCClientExternalChgDTO();

			lccChgDTO.setAmount(externalChg.getAmount());
			lccChgDTO.setAmountConsumedForPayment(externalChg.isIsAmountConsumedForPayment());
			lccChgDTO.setCarrierCode(externalChg.getCarrierCode());
			lccChgDTO.setCode(externalChg.getCode());
			lccChgDTO.setFlightRefNumber(externalChg.getFlightRefNumber());
			lccChgDTO.setOperationMode(externalChg.getOperationMode());

			if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.CREDIT_CARD.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.CREDIT_CARD);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString())) {
				lccChgDTO.setAirportCode(externalChg.getAirportCode());
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.HANDLING_CHARGE.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.INSURANCE.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.INSURANCE);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.MEAL.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.MEAL);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.BAGGAGE.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.BAGGAGE);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.NO_SHORE.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.NO_SHORE);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.NON_REF_ADJ.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.NON_REF_ADJ);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.REF_ADJ.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.REF_ADJ);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.REFUND.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.REFUND);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.SEAT_MAP.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.SEAT_MAP);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.PROMOTION_REWARD.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.PROMOTION_REWARD);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.JN_ANCI.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.JN_ANCI);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.ANCI_PENALTY.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.ANCI_PENALTY);
			} else if (externalChg.getExternalCharges().equals(EXTERNAL_CHARGES.SERVICE_TAX.toString())) {
				lccChgDTO.setExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX);
			}

			lccChgDTOs.add(lccChgDTO);
		}

		return lccChgDTOs;
	}

	private static Map<Integer, CommonCreditCardPaymentInfo> transformTempPaymentMap(List<PaymentMap> paymentMapList) {
		Map<Integer, CommonCreditCardPaymentInfo> commonCCPaymentMap = new HashMap<Integer, CommonCreditCardPaymentInfo>();

		for (PaymentMap paymentMap : paymentMapList) {

			CommonCreditCardPayment payment = paymentMap.getValue();

			CommonCreditCardPaymentInfo commonCreditCardPaymentInfo = new CommonCreditCardPaymentInfo();

			// ClientPaymentInfo commonInfo = payment.getClientPaymentInfo();

			// commonCreditCardPaymentInfo.setActualPaymentMethod(commonInfo.getActualPaymentMethod());
			commonCreditCardPaymentInfo.setAddress(payment.getAddress());
			commonCreditCardPaymentInfo.setAppIndicator((SalesChannelsUtil.isAppIndicatorWebOrMobile(payment.getAppIndicator()))
					? AppIndicatorEnum.APP_IBE
					: AppIndicatorEnum.APP_XBE);
			commonCreditCardPaymentInfo.setAuthorizationId(payment.getAuthorizationId());
			commonCreditCardPaymentInfo.setCardHoldersName(payment.getCardHolderName());
			// commonCreditCardPaymentInfo.setCarrierCode(commonInfo.getCarrierCode());
			commonCreditCardPaymentInfo.seteDate(payment.getEDate());
			commonCreditCardPaymentInfo.setGroupPNR(payment.getGroupPNR());
			// commonCreditCardPaymentInfo.setIncludeExternalCharges(commonInfo.isIncludeExternalCharges());
			// commonCreditCardPaymentInfo.setLccPaxTnxId(commonInfo.getLccPaxTnxId());
			// commonCreditCardPaymentInfo.setLccUniqueTnxId(commonInfo.getLccUniqueTnxId());
			commonCreditCardPaymentInfo.setName(payment.getName());
			commonCreditCardPaymentInfo.setNo(payment.getNo());
			commonCreditCardPaymentInfo.setNoFirstDigits(payment.getFirst4Digits());
			commonCreditCardPaymentInfo.setNoLastDigits(payment.getLast4Digits());
			commonCreditCardPaymentInfo.setOldCCRecordId(payment.getOldCCRecordId());
			// commonCreditCardPaymentInfo.setOriginalPayReference(commonInfo.getOriginalPayReference());
			// commonCreditCardPaymentInfo.setPayCurrencyAmount(commonInfo.getPayCurrencyAmount());
			// commonCreditCardPaymentInfo.setPayCurrencyDTO(transformPayCurrency(commonInfo.getPayCurrencyDTO()));
			commonCreditCardPaymentInfo.setPaymentBrokerId(payment.getPaymentBrokerId());
			commonCreditCardPaymentInfo.setPaymentSuccess(payment.isIsPaymentSuccess());
			// commonCreditCardPaymentInfo.setPayReference(commonInfo.getPayReference());
			// commonCreditCardPaymentInfo.setRecieptNumber(commonInfo.getRecieptNumber());
			// commonCreditCardPaymentInfo.setRemarks(commonInfo.getRemarks());
			commonCreditCardPaymentInfo.setSecurityCode(payment.getSecurityCode());
			commonCreditCardPaymentInfo.setTemporyPaymentId(payment.getTemporyPaymentId());
			commonCreditCardPaymentInfo.setTnxMode((payment.getTnxMode().intValue() == TnxModeEnum.MAIL_TP_ORDER.getCode())
					? TnxModeEnum.MAIL_TP_ORDER
					: TnxModeEnum.SECURE_3D);
			// commonCreditCardPaymentInfo.setTotalAmount(commonInfo.getTotalAmount());
			// commonCreditCardPaymentInfo.setTotalAmountCurrencyCode(commonInfo.getTotalAmountCurrencyCode());
			// commonCreditCardPaymentInfo.setTxnDateTime(commonInfo.getTxnDateTime());
			// commonCreditCardPaymentInfo.setType(payment.getType());

			/*
			 * IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new
			 * IPGIdentificationParamsDTO(payment.getIpgIdentificationParamsDTO().getFullyQualifiedIPGConfigName());
			 * 
			 * ipgIdentificationParamsDTO.set_isIPGConfigurationExists(payment.getIpgIdentificationParamsDTO().
			 * isIPGConfigurationExists());
			 * ipgIdentificationParamsDTO.setIpgId(payment.getIpgIdentificationParamsDTO().getIpgId());
			 * 
			 * commonCreditCardPaymentInfo.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
			 */

			commonCCPaymentMap.put(paymentMap.getKey(), commonCreditCardPaymentInfo);
		}

		return commonCCPaymentMap;
	}

	public static TrackInfoDTO transformTrackInfo(TrackInfo trackInfo) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();

		trackInfoDTO.setCarrierCode(trackInfo.getCarrierCode());
		trackInfoDTO.setIpAddress(trackInfo.getIpAddress());
		trackInfoDTO.setMarketingAgentCode(trackInfo.getMarketingAgentCode());
		trackInfoDTO.setMarketingAgentStationCode(trackInfo.getMarketingAgentStationCode());
		trackInfoDTO.setMarketingUserId(trackInfo.getMarketingUserId());
		trackInfoDTO.setOriginIPNumber(trackInfo.getOriginIPNumber());
		trackInfoDTO.setSessionId(trackInfo.getSessionId());
		trackInfoDTO.setAppIndicator(trackInfo.getAppIndicator().toString());
		
		return trackInfoDTO;
	}

	/**
	 * This method only support pax credit.
	 * 
	 * @param tnxCreditPayments
	 * @return
	 */
	public static List<PaymentDetails> transform(Collection<TnxCreditPayment> tnxCreditPayments) {
		List<PaymentDetails> paymentDetailsList = new ArrayList<PaymentDetails>();
		for (TnxCreditPayment tnxCreditPayment : tnxCreditPayments) {
			PaymentDetails paymentDetails = new PaymentDetails();
			paymentDetails.setPaymentSummary(new PaymentSummary());
			paymentDetails.getPaymentSummary().setPaymentAmount(tnxCreditPayment.getAmount());
			paymentDetails.getPaymentSummary().setPaymentCarrier(tnxCreditPayment.getPaymentCarrier());
			paymentDetails.getPaymentSummary().setCurrencyCodeGroup(FindReservationUtil.getDefaultCurrencyCodeGroup());

			PayCurrencyDTO payCurrencyDTO = tnxCreditPayment.getPayCurrencyDTO();
			PaymentCurrencyAmount paymentCurrencyAmount = new PaymentCurrencyAmount();
			paymentCurrencyAmount.setPaymentCurrencyAmount(payCurrencyDTO.getTotalPayCurrencyAmount());
			CurrencyCodeGroup codeGroup = new CurrencyCodeGroup();
			codeGroup.setCurrencyCode(payCurrencyDTO.getPayCurrencyCode());
			paymentCurrencyAmount.setCurrencyCodeGroup(codeGroup);
			paymentCurrencyAmount.setPayCurrMultiplyingExchangeRate(payCurrencyDTO.getPayCurrMultiplyingExchangeRate());
			paymentCurrencyAmount.setBoundaryValue(payCurrencyDTO.getBoundaryValue());
			paymentCurrencyAmount.setBreakPointValue(payCurrencyDTO.getBreakPointValue());

			paymentDetails.setPaymentCurrencyAmount(paymentCurrencyAmount);
			paymentDetails.setNorminalCode(ReservationTnxNominalCode.CREDIT_BF.getCode());

			paymentDetails.setUniqueTnxId(tnxCreditPayment.getLccUniqueTnxId());
			TravelerCreditPayment travelerCreditPayment = new TravelerCreditPayment();
			travelerCreditPayment.setDebitTravelerRefNumber(tnxCreditPayment.getPnrPaxId());
			travelerCreditPayment.setCarrierBaseAmount(tnxCreditPayment.getAmount());
			travelerCreditPayment.setPaxCreditId(tnxCreditPayment.getPaxCreditId());
			travelerCreditPayment.setEDate(tnxCreditPayment.getExpiryDate());
			travelerCreditPayment.setBasePnr(tnxCreditPayment.getPnr());
			// Note : we dont need to set the utilizing pnr here because we need that for recording payment only
			paymentDetails.setTravelerCreditPayment(travelerCreditPayment);
			paymentDetailsList.add(paymentDetails);
		}
		return paymentDetailsList;
	}

	public static CustomChargesTO transformCustomCharges(ChargerOverride chargeOverride, String alterationType) {
		CustomChargesTO customChargesTO = null;

		if (chargeOverride != null) {

			PnrChargeDetailTO adultCustomChargeDetail = transform(chargeOverride.getAdultCustomChargeDetail(), alterationType);
			PnrChargeDetailTO childCustomChargeDetail = transform(chargeOverride.getChildCustomChargeDetail(), alterationType);
			PnrChargeDetailTO infantCustomChargeDetail = transform(chargeOverride.getInfantCustomChargeDetail(), alterationType);

			customChargesTO = new CustomChargesTO(chargeOverride.getCustomAdultChargeValue(),
					chargeOverride.getCustomChildChargeValue(), chargeOverride.getCustomInfantChargeValue(),
					chargeOverride.getCustomAdultChargeValue(), chargeOverride.getCustomChildChargeValue(),
					chargeOverride.getCustomInfantChargeValue(), adultCustomChargeDetail, childCustomChargeDetail,
					infantCustomChargeDetail, chargeOverride.isSendingAbosoluteCharge() == null
							? false
							: chargeOverride.isSendingAbosoluteCharge());

			customChargesTO.setCompareWithExistingModificationCharges(chargeOverride.isCompareWithExistingModificationCharges());
			customChargesTO.setExistingModificationChargeAdult(chargeOverride.getExistingModificationChargeAdult());
			customChargesTO.setExistingModificationChargeChild(chargeOverride.getExistingModificationChargeChild());
			customChargesTO.setExistingModificationChargeInfant(chargeOverride.getExistingModificationChargeInfant());
			customChargesTO.setDefaultCustomAdultCharge(chargeOverride.getExistingModificationChargeAdult());
			customChargesTO.setDefaultCustomChildCharge(chargeOverride.getExistingModificationChargeChild());
		}

		return customChargesTO;

	}

	private static PnrChargeDetailTO transform(CustomChargeDetail customChargeDetail, String alterationType) {

		if (customChargeDetail != null) {

			if (alterationType.equals(ReservationConstants.AlterationType.ALT_CANCEL_OND)
					|| alterationType.equals(ReservationConstants.AlterationType.ALT_CANCEL_RES)) {

				PnrChargeDetailTO pnrChargeDetailTO = new PnrChargeDetailTO();
				pnrChargeDetailTO.setCancellationChargeType(customChargeDetail.getChargeType());
				pnrChargeDetailTO.setMinCancellationAmount(customChargeDetail.getMinAmount());
				pnrChargeDetailTO.setMaximumCancellationAmount(customChargeDetail.getMaxAmount());
				pnrChargeDetailTO.setChargePercentage(customChargeDetail.getPercentage());

				return pnrChargeDetailTO;
			} else if (alterationType.equals(ReservationConstants.AlterationType.ALT_MODIFY_OND)
					|| alterationType.equals(ReservationConstants.AlterationType.ALT_CANCEL_OND_WITH_MODIFY_CHARGE)) {

				PnrChargeDetailTO pnrChargeDetailTO = new PnrChargeDetailTO();
				pnrChargeDetailTO.setModificationChargeType(customChargeDetail.getChargeType());
				pnrChargeDetailTO.setMinModificationAmount(customChargeDetail.getMinAmount());
				pnrChargeDetailTO.setMaximumModificationAmount(customChargeDetail.getMaxAmount());
				pnrChargeDetailTO.setChargePercentage(customChargeDetail.getPercentage());

				return pnrChargeDetailTO;
			} else if (alterationType.equals(ReservationConstants.AlterationType.ALT_REQUOTE)) {
				PnrChargeDetailTO pnrChargeDetailTO = new PnrChargeDetailTO();
				pnrChargeDetailTO.setCancellationChargeType(customChargeDetail.getChargeType());
				pnrChargeDetailTO.setMinCancellationAmount(customChargeDetail.getMinAmount());
				pnrChargeDetailTO.setMaximumCancellationAmount(customChargeDetail.getMaxAmount());

				pnrChargeDetailTO.setModificationChargeType(customChargeDetail.getChargeType());
				pnrChargeDetailTO.setMinModificationAmount(customChargeDetail.getMinAmount());
				pnrChargeDetailTO.setMaximumModificationAmount(customChargeDetail.getMaxAmount());

				pnrChargeDetailTO.setChargePercentage(customChargeDetail.getPercentage());

				return pnrChargeDetailTO;
			}
		}

		return null;
	}

	/**
	 * Transform the provided {@link ChargeAdjustmentTypeDTO} to {@link ChargeAdjustmentType} object.
	 * 
	 * @param adjustmentType
	 *            : The object to be transformed.
	 * 
	 * @return : The trasnformed object.
	 */
	public static ChargeAdjustmentType transform(ChargeAdjustmentTypeDTO adjustmentType) {
		ChargeAdjustmentType transformedAdjustmentType = new ChargeAdjustmentType();
		transformedAdjustmentType.setChargeAdjustmentTypeId(adjustmentType.getChargeAdjustmentTypeId());
		transformedAdjustmentType.setChargeAdjustmentTypeName(adjustmentType.getChargeAdjustmentTypeName());
		transformedAdjustmentType.setDescription(adjustmentType.getDescription());
		transformedAdjustmentType.setRefundableChargeCode(adjustmentType.getRefundableChargeCode());
		transformedAdjustmentType.setNonRefundableChargeCode(adjustmentType.getNonRefundableChargeCode());
		transformedAdjustmentType.setRefundablePrivilegeId(adjustmentType.getRefundablePrivilegeId());
		transformedAdjustmentType.setNonRefundablePrivilegeId(adjustmentType.getNonRefundablePrivilegeId());
		transformedAdjustmentType.setDefaultAdustment(adjustmentType.getDefaultAdjustment());
		transformedAdjustmentType.setDisplayOrder(adjustmentType.getDisplayOrder());
		transformedAdjustmentType.setStatus(adjustmentType.getStatus());
		return transformedAdjustmentType;
	}

	public static List<StringIntegerMap> convertMapToLCCList(Map<String, Integer> map) {
		List<StringIntegerMap> lccMap = new ArrayList<StringIntegerMap>();

		if (map != null && !map.isEmpty()) {
			for (Entry<String, Integer> mapEntry : map.entrySet()) {
				StringIntegerMap mapObj = new StringIntegerMap();
				mapObj.setKey(mapEntry.getKey());
				mapObj.setValue(mapEntry.getValue());
				lccMap.add(mapObj);
			}
		}

		return lccMap;
	}

	public static List<StringStringMap> convertStringStringMapToLCCList(Map<String, String> map) {
		List<StringStringMap> lccMap = new ArrayList<StringStringMap>();

		if (map != null && !map.isEmpty()) {
			for (Entry<String, String> mapEntry : map.entrySet()) {
				StringStringMap mapObj = new StringStringMap();
				mapObj.setKey(mapEntry.getKey());
				mapObj.setValue(mapEntry.getValue());
				lccMap.add(mapObj);
			}
		}

		return lccMap;
	}

	public static Map<Integer, Integer> transformIntegerIntegerMap(List<IntegerIntegerMap> integerIntegerList) {
		Map<Integer, Integer> integerIntegerMap = new HashMap<Integer, Integer>();
		for (IntegerIntegerMap integerIntegerObj : integerIntegerList) {
			integerIntegerMap.put(integerIntegerObj.getKey(), integerIntegerObj.getValue());
		}
		return integerIntegerMap;
	}

	public static Map<Integer, String> transformIntegerStringMap(List<IntegerStringMap> integerStringList) {
		Map<Integer, String> integerStringMap = new HashMap<Integer, String>();
		for (IntegerStringMap integerStringObj : integerStringList) {
			integerStringMap.put(integerStringObj.getKey(), integerStringObj.getValue());
		}
		return integerStringMap;
	}

	public static Map<String, String> transformStringStringMap(List<StringStringMap> stringStringList) {
		Map<String, String> stringStringMap = new HashMap<String, String>();
		for (StringStringMap stringStringObj : stringStringList) {
			stringStringMap.put(stringStringObj.getKey(), stringStringObj.getValue());
		}
		return stringStringMap;
	}

	public static Map<Integer, BigDecimal> transformIntegerDecimalMap(List<IntegerDecimalMap> integerDecimalList) {
		Map<Integer, BigDecimal> integerDecimalMap = new HashMap<Integer, BigDecimal>();
		for (IntegerDecimalMap integerDecimalObj : integerDecimalList) {
			integerDecimalMap.put(integerDecimalObj.getKey(), integerDecimalObj.getValue());
		}
		return integerDecimalMap;
	}

	public static Map<Integer, NameDTO> transformIntegerPaxNameDTO(List<IntegerPaxNameInfoMap> paxNameInfoMap) {
		Map<Integer, NameDTO> integerDecimalMap = new HashMap<Integer, NameDTO>();
		for (IntegerPaxNameInfoMap integerPaxNameObj : paxNameInfoMap) {
			NameDTO nameDTO = new NameDTO();
			PaxNameInfo paxNameInfo = integerPaxNameObj.getValue();
			nameDTO.setTitle(paxNameInfo.getTitle());
			nameDTO.setFirstname(paxNameInfo.getFirstName());
			nameDTO.setLastName(paxNameInfo.getLastName());
			nameDTO.setFFID(paxNameInfo.getFfid());
			nameDTO.setPnrPaxId(paxNameInfo.getPnrPaxId());
			nameDTO.setPaxReference(paxNameInfo.getPaxReference());
			integerDecimalMap.put(integerPaxNameObj.getKey(), nameDTO);
		}

		return integerDecimalMap;
	}
	

	public static Map<Integer, List<LCCClientExternalChgDTO>> composePerPaxExternalChargeMap(
			List<PerPaxExternalCharges> ppExtChgList) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChargeMap = null;
		if (ppExtChgList != null) {
			paxExtChargeMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
			for (PerPaxExternalCharges ppExtChgs : ppExtChgList) {
				paxExtChargeMap.put(ppExtChgs.getPaxSequence(), tranformExtCharges(ppExtChgs.getExternalCharges()));
			}
		}
		return paxExtChargeMap;
	}

	public static Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>>
			composePaxExternalChargeMap(List<PaxExternalChargesMap> ppExtChgList) {
		Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExtChargeMap = null;
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		if (ppExtChgList != null) {
			paxCarrierExtChargeMap = new HashMap<String, TreeMap<String, List<LCCClientExternalChgDTO>>>();
			for (PaxExternalChargesMap ppExtChgs : ppExtChgList) {
				TreeMap<String, List<LCCClientExternalChgDTO>> carrierExtCharges = new TreeMap<String, List<LCCClientExternalChgDTO>>();
				carrierExtCharges.put(carrierCode, tranformExtCharges(ppExtChgs.getExternalCharges()));
				paxCarrierExtChargeMap.put(ppExtChgs.getPaxKey(), carrierExtCharges);
			}
		}
		return paxCarrierExtChargeMap;
	}

	public static List<LCCClientExternalChgDTO> tranformExtCharges(List<ExternalCharge> externalCharges) {
		List<LCCClientExternalChgDTO> lccExtChgDTOList = new ArrayList<LCCClientExternalChgDTO>();
		for (ExternalCharge extChg : externalCharges) {

			if (extChg.getType().equals(ExternalChargeType.FLEXI_CHARGES)) {
				LCCClientFlexiExternalChgDTO externalChgDTO = new LCCClientFlexiExternalChgDTO();
				externalChgDTO.setAmount(extChg.getAmount());
				externalChgDTO.setAmountConsumedForPayment(false);
				externalChgDTO.setCarrierCode(extChg.getCarrierCode());
				externalChgDTO.setCode(extChg.getCode());
				externalChgDTO.setExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
				externalChgDTO.setFlightRefNumber(extChg.getFlightRefNumber());
				Collection<FlexiInfoTO> flexiInfoTOs = new ArrayList<FlexiInfoTO>();
				if (extChg.getAdditionalDetails() != null && extChg.getAdditionalDetails().size() > 0) {
					for (FlexiInfo flexibility : extChg.getAdditionalDetails()) {
						FlexiInfoTO flexiInfoTO = new FlexiInfoTO();
						flexiInfoTO.setAvailableCount(flexibility.getAvailableCount());
						flexiInfoTO.setFlexiRateId(flexibility.getFlexiRateId());
						flexiInfoTO.setFlexibilityTypeId(flexibility.getFlexibilityTypeId());
						flexiInfoTOs.add(flexiInfoTO);
					}
				}
				externalChgDTO.getFlexiBilities().addAll(flexiInfoTOs);
				externalChgDTO.setSegmentCode(extChg.getSegmentCode());
				lccExtChgDTOList.add(externalChgDTO);
			} else {
				LCCClientExternalChgDTO lccExtChg = new LCCClientExternalChgDTO();
				lccExtChg.setAmount(extChg.getAmount());
				lccExtChg.setCarrierCode(extChg.getCarrierCode());
				lccExtChg.setFlightRefNumber(extChg.getFlightRefNumber());
				lccExtChg.setSegmentCode(extChg.getSegmentCode());
				lccExtChg.setCode(extChg.getCode());
				lccExtChg.setExternalCharges(AAUtils.getExternalChargeType(extChg.getType()));
				lccExtChg.setAirportCode(extChg.getAirportCode());
				lccExtChg.setApplyOn(extChg.getApplyOn());
				lccExtChg.setOndBaggageChargeGroupId(extChg.getOndBaggageChargeGroupId());
				lccExtChg.setOndBaggageGroupId(extChg.getOndBaggageGroupId());
				lccExtChg.setAnciOffer(extChg.isIsAnciOffer());
				lccExtChg.setOfferedAnciTemplateID(extChg.getOfferedAnciTemplateID());
				if (extChg.getType().equals(ExternalChargeType.HANDLING_CHARGE)) {
					lccExtChg.setOperationMode(extChg.getOperationMode());
				}
				lccExtChgDTOList.add(lccExtChg);
			}
		}
		return lccExtChgDTOList;
	}

	public static IInsuranceRequest transformInsurance(InsuranceRequest lccInsuranceRequest, Reservation reservation) {
		IInsuranceRequest insurance = null;
		if (lccInsuranceRequest != null
				&& lccInsuranceRequest.getOperatingCarrierCode().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			insurance = new InsuranceRequestAssembler();
			ReservationBD reservationBD = AAServicesModuleUtils.getReservationBD();
			ReservationInsurance reservationInsurance = reservationBD.getReservationInsurance(new Integer(lccInsuranceRequest
					.getInsuranceRefNumber()));
			InsureSegment insSegment = new InsureSegment();
			insSegment.setFromAirportCode(reservationInsurance.getOrigin());
			insSegment.setToAirportCode(reservationInsurance.getDestination());
			insSegment.setDepartureDate(reservationInsurance.getDateOfTravel());
			insSegment.setArrivalDate(reservationInsurance.getDateOfReturn());
			insSegment.setRoundTrip(reservationInsurance.getRoundTrip().equals("Y") ? true : false);
			insSegment.setSalesChanelCode(reservationInsurance.getSalesChannel());

			if (reservation != null) {
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						InsurePassenger insurePassenger = new InsurePassenger();

						// adding AIG Pax info
						insurePassenger.setFirstName(pax.getFirstName());
						insurePassenger.setLastName(pax.getLastName());
						insurePassenger.setDateOfBirth(pax.getDateOfBirth());
						ReservationContactInfo contactInfo = reservation.getContactInfo();
						// Adding AIG Contact Info
						insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insurePassenger.setCity(contactInfo.getCity());
						insurePassenger.setEmail(contactInfo.getEmail());
						insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insurePassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

						insurance.addPassenger(insurePassenger);
					}
				}
			}

			insurance.addFlightSegment(insSegment);
			insurance.setInsuranceId(reservationInsurance.getInsuranceId());
			insurance.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedAmount());
			insurance.setSsrFeeCode(lccInsuranceRequest.getSsrFeeCode());
			insurance.setPlanCode(lccInsuranceRequest.getPlanCode());
		}

		return insurance;
	}

	public static PassengerType transformLCCPaxTypeCode(String paxType) {
		PassengerType passengerType = null;
		if (ReservationInternalConstants.PassengerType.ADULT.equals(paxType)) {
			passengerType = PassengerType.ADT;
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxType)) {
			passengerType = PassengerType.CHD;
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
			passengerType = PassengerType.INF;
		} else if (ReservationInternalConstants.PassengerType.PARENT.equals(paxType)) {
			passengerType = PassengerType.PRT;
		}
		return passengerType;
	}

	public static EXTERNAL_CHARGES transformExternalChargeType(ExternalChargeType lccExternalCharges) {
		if (lccExternalCharges == ExternalChargeType.CREDIT_CARD) {
			return EXTERNAL_CHARGES.CREDIT_CARD;
		} else if (lccExternalCharges == ExternalChargeType.HANDLING_CHARGE) {
			return EXTERNAL_CHARGES.HANDLING_CHARGE;
		} else if (lccExternalCharges == ExternalChargeType.SEAT_MAP) {
			return EXTERNAL_CHARGES.SEAT_MAP;
		} else if (lccExternalCharges == ExternalChargeType.MEAL) {
			return EXTERNAL_CHARGES.MEAL;
		} else if (lccExternalCharges == ExternalChargeType.INSURANCE) {
			return EXTERNAL_CHARGES.INSURANCE;
		} else if (lccExternalCharges == ExternalChargeType.INFLIGHT_SERVICES) {
			return EXTERNAL_CHARGES.INFLIGHT_SERVICES;
		} else if (lccExternalCharges == ExternalChargeType.FLEXI_CHARGES) {
			return EXTERNAL_CHARGES.FLEXI_CHARGES;
		} else if (lccExternalCharges == ExternalChargeType.AIRPORT_SERVICE) {
			return EXTERNAL_CHARGES.AIRPORT_SERVICE;
		} else if (lccExternalCharges == ExternalChargeType.PROMOTION_REWARD) {
			return EXTERNAL_CHARGES.PROMOTION_REWARD;
		} else if (lccExternalCharges == ExternalChargeType.ADDITONAL_SEAT_CHARGE) {
			return EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE;
		} else if (lccExternalCharges == ExternalChargeType.BAGGAGE) {
			return EXTERNAL_CHARGES.BAGGAGE;
		} else if (lccExternalCharges == ExternalChargeType.JN_OTHER) {
			return EXTERNAL_CHARGES.JN_OTHER;
		} else if (lccExternalCharges == ExternalChargeType.JN_ANCI) {
			return EXTERNAL_CHARGES.JN_ANCI;
		} else if (lccExternalCharges == ExternalChargeType.SERVICE_TAX) {
			return EXTERNAL_CHARGES.SERVICE_TAX;
		} else if (lccExternalCharges == ExternalChargeType.BSP_FEE) {
			return EXTERNAL_CHARGES.BSP_FEE;
		}

		return null;
	}

	public static PromoSelectionCriteria transformFromPromotionSelectionCriteria(
			PromotionSelectionCriteria aaPromoSelectionCriteria) {

		PromoSelectionCriteria promoSelectionCriteria = null;
		if (aaPromoSelectionCriteria != null) {
			promoSelectionCriteria = new PromoSelectionCriteria();
			promoSelectionCriteria.setPromoCode(aaPromoSelectionCriteria.getPromoCode());
			promoSelectionCriteria.setPromoType(aaPromoSelectionCriteria.getPromoType());
			promoSelectionCriteria.getOndList().addAll(aaPromoSelectionCriteria.getOndList());
			promoSelectionCriteria.getFlights().addAll(aaPromoSelectionCriteria.getFlights());
			promoSelectionCriteria.getFlightIds().addAll(aaPromoSelectionCriteria.getFlightIds());
			promoSelectionCriteria.getFlightSegIds().addAll(aaPromoSelectionCriteria.getFlightSegIds());
			promoSelectionCriteria.setFlightSegWiseLogicalCabinClass(transformIntegerStringMap(aaPromoSelectionCriteria
					.getFlightSegWiseLogicalCabinClass()));
			promoSelectionCriteria.setSalesChannel(aaPromoSelectionCriteria.getSalesChannel());
			promoSelectionCriteria.setReservationDate(aaPromoSelectionCriteria.getReservationDate());
			promoSelectionCriteria.setOndFlightDates(populateOndFlights(aaPromoSelectionCriteria.getOndFlightDates()));
			promoSelectionCriteria.setAgent(aaPromoSelectionCriteria.getAgent());
			promoSelectionCriteria.getCabinClasses().addAll(aaPromoSelectionCriteria.getCabinClasses());
			promoSelectionCriteria.getLogicalCabinClasses().addAll(aaPromoSelectionCriteria.getLogicalCabinClasses());
			promoSelectionCriteria.getBookingClasses().addAll(aaPromoSelectionCriteria.getBookingClasses());
			promoSelectionCriteria.setJourneyType(getJourneyType(aaPromoSelectionCriteria.getJourneyType()));
			promoSelectionCriteria.setPreferredLanguage(aaPromoSelectionCriteria.getPreferedLanguage());
			promoSelectionCriteria.setAdultCount(aaPromoSelectionCriteria.getAdultCount());
			promoSelectionCriteria.setChildCount(aaPromoSelectionCriteria.getChildCount());
			promoSelectionCriteria.setInfantCount(aaPromoSelectionCriteria.getInfantCount());
			promoSelectionCriteria.setBankIdNo(aaPromoSelectionCriteria.getBankIdNo());
		}
		return promoSelectionCriteria;
	}

	public static ApplicablePromotionDetails transformFromApplicablePromotionDetailsTO(
			ApplicablePromotionDetailsTO applicablePromotionDetailsTo) {

		ApplicablePromotionDetails aaApplicablePromotionDetails = null;
		if (applicablePromotionDetailsTo != null) {
			aaApplicablePromotionDetails = new ApplicablePromotionDetails();
			aaApplicablePromotionDetails.setPromoCriteriaId(applicablePromotionDetailsTo.getPromoCriteriaId());
			aaApplicablePromotionDetails.setPromoName(applicablePromotionDetailsTo.getPromoName());
			aaApplicablePromotionDetails.setDescription(applicablePromotionDetailsTo.getDescription());
			aaApplicablePromotionDetails.setPromoType(applicablePromotionDetailsTo.getPromoType());
			aaApplicablePromotionDetails.setPromoCode(applicablePromotionDetailsTo.getPromoCode());
			aaApplicablePromotionDetails.setSystemGenerated(applicablePromotionDetailsTo.isSystemGenerated());
			aaApplicablePromotionDetails.setDiscountType(applicablePromotionDetailsTo.getDiscountType());
			aaApplicablePromotionDetails.setDiscountValue(applicablePromotionDetailsTo.getDiscountValue());
			aaApplicablePromotionDetails.setApplyTo(applicablePromotionDetailsTo.getApplyTo());
			aaApplicablePromotionDetails.setDiscountAs(applicablePromotionDetailsTo.getDiscountAs());
			aaApplicablePromotionDetails.setApplicableAdultCount(applicablePromotionDetailsTo.getApplicableAdultCount());
			aaApplicablePromotionDetails.setApplicableChildCount(applicablePromotionDetailsTo.getApplicableChildCount());
			aaApplicablePromotionDetails.setApplicableChildCount(applicablePromotionDetailsTo.getApplicableChildCount());
			aaApplicablePromotionDetails.getApplicableAncillaries().addAll(
					applicablePromotionDetailsTo.getApplicableAncillaries());
			aaApplicablePromotionDetails.setAllowSamePaxOnly(applicablePromotionDetailsTo.isAllowSamePaxOnly());
			aaApplicablePromotionDetails.getApplicableBINs().addAll(applicablePromotionDetailsTo.getApplicableBINs());
			aaApplicablePromotionDetails.getApplicableOnds().addAll(applicablePromotionDetailsTo.getApplicableOnds());
			aaApplicablePromotionDetails.setOriginPnr(applicablePromotionDetailsTo.getOriginPnr());
			aaApplicablePromotionDetails.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		}
		return aaApplicablePromotionDetails;
	}

	public static List<Eticket> transform(Map<Integer, EticketDTO> paxEtickets, List<FlightSegmentTO> flightSegmentTOs)
			throws ModuleException {
		List<Eticket> lccPaxEtickets = new ArrayList<Eticket>();
		for (Integer lccSegSequence : paxEtickets.keySet()) {
			EticketDTO eticketDTO = paxEtickets.get(lccSegSequence);
			FlightSegmentTO flightSegmentTO = flightSegmentTOs.get(lccSegSequence - 1);
			Eticket eticket = new Eticket();
			eticket.setEticketNumber(eticketDTO.getEticketNo());
			eticket.setCouponNo(eticketDTO.getCouponNo());
			eticket.setSegmentSequence(flightSegmentTO.getSegmentSequence());

			Map<String, String> busAirCarrierCodes = AAServicesModuleUtils.getFlightBD().getBusAirCarrierCodes();
			String carrierCode = flightSegmentTO.getOperatingAirline();

			if (busAirCarrierCodes.containsKey(carrierCode)) {
				eticket.setAirlineCode(busAirCarrierCodes.get(carrierCode));
			} else {
				eticket.setAirlineCode(carrierCode);
			}
			lccPaxEtickets.add(eticket);
		}
		return lccPaxEtickets;
	}

	private static JourneyType getJourneyType(String journeyType) {

		if (journeyType.equals(JourneyType.SINGLE_SECTOR)) {
			return JourneyType.SINGLE_SECTOR;
		} else if (journeyType.equals(JourneyType.ROUNDTRIP)) {
			return JourneyType.ROUNDTRIP;
		}
		return null;
	}

	private static Map<Integer, Date> populateOndFlights(List<IntegerDateMap> lccOndFlight) {
		Map<Integer, Date> ondFlights = new HashMap<Integer, Date>();

		for (IntegerDateMap lccFlightObj : lccOndFlight) {
			ondFlights.put(lccFlightObj.getKey(), lccFlightObj.getValue());
		}

		return ondFlights;
	}

	public static List<StringDoubleMap> populateStringDoubleMap(Map<String, Double> stringDoubleMap) {
		List<StringDoubleMap> stringDoubleMaps = new ArrayList<StringDoubleMap>();

		if (stringDoubleMap != null && !stringDoubleMap.isEmpty()) {
			for (Entry<String, Double> stringDouble : stringDoubleMap.entrySet()) {
				StringDoubleMap stringDoubleObj = new StringDoubleMap();
				stringDoubleObj.setKey(stringDouble.getKey());
				stringDoubleObj.setValue(stringDouble.getValue());
				stringDoubleMaps.add(stringDoubleObj);
			}
		}

		return stringDoubleMaps;
	}

	public static List<StringDecimalMap> populateStringDecimalMap(Map<String, BigDecimal> stringDecimalMap) {
		List<StringDecimalMap> stringDecimalMaps = new ArrayList<StringDecimalMap>();

		if (stringDecimalMap != null && !stringDecimalMap.isEmpty()) {
			for (Entry<String, BigDecimal> stringDecimal : stringDecimalMap.entrySet()) {
				StringDecimalMap stringDecimalObj = new StringDecimalMap();
				stringDecimalObj.setKey(stringDecimal.getKey());
				stringDecimalObj.setValue(stringDecimal.getValue());
				stringDecimalMaps.add(stringDecimalObj);
			}
		}

		return stringDecimalMaps;
	}

	public static Map<String, BigDecimal> transformStringDecimalMap(List<StringDecimalMap> stringDecimalList) {
		Map<String, BigDecimal> stringDecimalMap = new HashMap<String, BigDecimal>();
		for (StringDecimalMap stringDecimalObj : stringDecimalList) {
			stringDecimalMap.put(stringDecimalObj.getKey(), stringDecimalObj.getValue());
		}
		return stringDecimalMap;
	}

	public static List<StringDecimalMap> transformFarePriceOndToStringDecimalMap(
			Map<FarePriceOnd, BigDecimal> farePriceOndDecimalMap) {
		List<StringDecimalMap> stringDecimalMaps = new ArrayList<StringDecimalMap>();

		if (farePriceOndDecimalMap != null && !farePriceOndDecimalMap.isEmpty()) {
			for (Entry<FarePriceOnd, BigDecimal> farePriceOndDecimal : farePriceOndDecimalMap.entrySet()) {
				StringDecimalMap stringDecimalObj = new StringDecimalMap();
				stringDecimalObj.setKey(farePriceOndDecimal.getKey().toString());
				stringDecimalObj.setValue(farePriceOndDecimal.getValue());
				stringDecimalMaps.add(stringDecimalObj);
			}
		}

		return stringDecimalMaps;
	}

	
	public static UserNoteTO transform(UserNote userNote) {

		UserNoteTO userNoteTo = new UserNoteTO();
		ArrayList<String> userNoteList = new ArrayList<String>();
		userNoteList.add(userNote.getNoteText());
		userNoteTo.setUserNotes(userNoteList);
		userNoteTo.setUserNoteType(userNote.getUserNoteType());
		userNoteTo.setPnr(userNote.getPnr());
		userNoteTo.setUserName(userNote.getUsername());
		userNoteTo.setSystemNote(userNote.getSystemText());
		userNoteTo.setAction(userNote.getAction());
		return userNoteTo;

	}

	public static TrackInfoDTO transform(TrackInfo trackInfo) {

		TrackInfoDTO trackInfoDto = new TrackInfoDTO();
		trackInfoDto.setCarrierCode(trackInfo.getCarrierCode());
		trackInfoDto.setIpAddress(trackInfo.getIpAddress());
		trackInfoDto.setMarketingAgentCode(trackInfo.getMarketingAgentCode());
		trackInfoDto.setMarketingAgentStationCode(trackInfo.getMarketingAgentStationCode());
		trackInfoDto.setMarketingUserId(trackInfo.getMarketingUserId());

		return trackInfoDto;

	}

	public static BundleFareDescriptionInfo transform(BundleFareDescriptionTemplateDTO descriptionTemplateDTO) {

		BundleFareDescriptionInfo targetDescriptionInfo = new BundleFareDescriptionInfo();

		if (descriptionTemplateDTO != null) {
			targetDescriptionInfo.setDescriptionTemplateId(descriptionTemplateDTO.getTemplateID());
			targetDescriptionInfo.setTemplateName(descriptionTemplateDTO.getTemplateName());
			targetDescriptionInfo.setDefaultPaidServicesContent(descriptionTemplateDTO.getDefaultPaidServicesContent());
			targetDescriptionInfo.setDefaultFreeServicesContent(descriptionTemplateDTO.getDefaultFreeServicesContent());

			List<StringBundledFareTranslationMap> targetTranslationTemplates = new ArrayList<StringBundledFareTranslationMap>();

			Map<String, BundleFareDescriptionTranslationTemplateDTO> sourceTranslations = descriptionTemplateDTO
					.getTranslationTemplates();
			if (sourceTranslations != null && !sourceTranslations.isEmpty()) {
				for (String language : sourceTranslations.keySet()) {
					BundleFareDescriptionTranslationTemplateDTO sourceTemplateDTO = sourceTranslations.get(language);
					BundleFareTranslationInfo targetTranslationInfo = new BundleFareTranslationInfo();
					targetTranslationInfo.setDescriptionTemplateId(sourceTemplateDTO.getTemplateID());
					targetTranslationInfo.setLanguageCode(sourceTemplateDTO.getLanguageCode());
					targetTranslationInfo.setPaidServicesContent(sourceTemplateDTO.getPaidServicesContent());
					targetTranslationInfo.setFreeServicesContent(sourceTemplateDTO.getFreeServicesContent());

					StringBundledFareTranslationMap translationMap = new StringBundledFareTranslationMap();
					translationMap.setKey(language);
					translationMap.setValue(targetTranslationInfo);
					targetTranslationTemplates.add(translationMap);

				}
				targetDescriptionInfo.getTranslationTemplates().addAll(targetTranslationTemplates);
			}
		}

		return targetDescriptionInfo;
	}

}
