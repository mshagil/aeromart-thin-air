package com.isa.thinair.aaservices.core.bl.availability;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Fare Rule Proxy to cache the already loaded fare rules so it won't be loaded again
 * 
 * @author Nilindra Fernando
 */
public class FareRuleProxy {

	private Map<Integer, FareRule> mapFareRule;

	public FareRuleProxy() {
		setMapFareRule(new HashMap<Integer, FareRule>());
	}

	public void refreshTheCache(Collection<OndFareDTO> colOndFareDTO) throws ModuleException {
		Collection<Integer> currentFareRuleIds = new HashSet<Integer>();
		for (OndFareDTO ondFareDTO : colOndFareDTO) {
			currentFareRuleIds.add(ondFareDTO.getFareSummaryDTO().getFareRuleID());
		}

		Collection<Integer> fareRuleIdsToLoad = new HashSet<Integer>();
		Collection<Integer> existingFareRuleIds = getMapFareRule().keySet();
		if (existingFareRuleIds.size() > 0) {
			for (Integer fareRuleId : currentFareRuleIds) {
				if (!existingFareRuleIds.contains(fareRuleId)) {
					fareRuleIdsToLoad.add(fareRuleId);
				}
			}
		} else {
			fareRuleIdsToLoad.addAll(currentFareRuleIds);
		}

		if (fareRuleIdsToLoad.size() > 0) {
			Map<Integer, FareRule> loadedFareRulesMap = AAServicesModuleUtils.getFareRuleBD().getFareRulesMap(fareRuleIdsToLoad);
			getMapFareRule().putAll(loadedFareRulesMap);
		}
	}

	public FareRule getFareRule(int fareRuleId) throws ModuleException {
		FareRule fareRule = getMapFareRule().get(fareRuleId);

		if (fareRule == null) {
			throw new ModuleException("airpricing.fare.rule.can.not.be.located");
		}

		return fareRule;

	}

	public FareRule loadFareRule(Integer fareRuleID) throws ModuleException {
		FareRule fareRule = AAServicesModuleUtils.getFareRuleBD().getFareRule(fareRuleID);
		if (fareRule == null) {
			throw new ModuleException("airpricing.fare.rule.can.not.be.located");
		}

		return fareRule;

	}

	/**
	 * @return the mapFareRule
	 */
	private Map<Integer, FareRule> getMapFareRule() {
		return mapFareRule;
	}

	/**
	 * @param mapFareRule
	 *            the mapFareRule to set
	 */
	private void setMapFareRule(Map<Integer, FareRule> mapFareRule) {
		this.mapFareRule = mapFareRule;
	}

}
