package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.isa.connectivity.profiles.maxico.v1.common.MaxicoCommonConstants;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAError;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Simplified Utility for preparing Error/Warning objects
 * 
 * We need to extract the AA Errors to a common place. Hope this utility will help us to do it easily. When we get some
 * free time :-)
 * 
 * @author Nilindra Fernando
 */
public class AAExceptionUtil {

	private static ResourceBundle aaErrorCodesMappings = null;
	private static String AA_ERROR_CODES_MAPPINGS_RESOURCE_BUNDLE = "resources/aa_codes/AAErrorCodesMapping";
	private static Object lock = new Object();

	/**
	 * Prepares a colleciton of ErrorType from WebservicesException
	 * 
	 * @param wse
	 *            WebservicesException
	 * @return ErrorType collection
	 */
	private static Collection<AAError> extractAAErrorsFromWSException(WebservicesException wse) {
		Collection<AAError> errors = new ArrayList<AAError>();

		if (wse.getCause() != null && wse.getCause() instanceof ModuleException) {
			ModuleException me = (ModuleException) wse.getCause();
			String details = (me.getExceptionDetails() != null) ? me.getExceptionDetails().toString() : null;
			AAErrorCode aaErrorCode = getMappedAAErrorCode(me);
			errors.add(composeErrorType(aaErrorCode, me.getMessageString(), details));
		}

		for (AAErrorCode aaErrorCode : wse.getAAErrorCodes().keySet()) {
			errors.add(composeErrorType(aaErrorCode, wse.getAAErrorCodes().get(aaErrorCode), null));
		}

		return errors;
	}

	private static AAError composeErrorType(AAErrorCode aaErrorCode, String description, String contextInfo) {
		AAError aaError = new AAError();
		aaError.setErrorCode(aaErrorCode);
		aaError.setDescription(description);

		if (contextInfo != null && contextInfo.length() > 0) {
			aaError.setDescription(description + " [" + contextInfo + "]");
		}

		return aaError;
	}

	/**
	 * Add ErrorType and/or Warning Type from Exception to the collections
	 * 
	 * @param errorsCollection
	 *            ErrorType objects are added to this collection
	 * @param warningsCollection
	 *            WarningType objects are added to this collection
	 * @param exception
	 *            Errors/Warnings are extracted from this exception
	 */
	public static void addAAErrrors(Collection<AAError> errorsCollection, Exception exception) {
		if (exception != null && exception instanceof WebservicesException) {
			if (errorsCollection != null) {
				Collection<AAError> errors = extractAAErrorsFromWSException((WebservicesException) exception);
				for (AAError error : errors) {
					errorsCollection.add(error);
				}
			}
		} else if (exception != null && exception instanceof ModuleException) {
			ModuleException me = (ModuleException) exception;
			String details = (me.getExceptionDetails() != null) ? me.getExceptionDetails().toString() : null;
			AAErrorCode aaErrorCode = getMappedAAErrorCode(me);
			errorsCollection.add(composeErrorType(aaErrorCode, me.getMessageString(), details));
		} else {
			errorsCollection.add(composeErrorType(AAErrorCode.ERR_13_MAXICO_REQUIRED_UNDETERMINED_CAUSE_PLEASE_REPORT,
					exception != null ? "Error StackTrace = " + getStackTrace(exception, 10) : "no exception details", null));
		}
	}

	private static AAErrorCode getMappedAAErrorCode(ModuleException me) {
		AAErrorCode aaErrorCode = AAErrorCode.ERR_13_MAXICO_REQUIRED_UNDETERMINED_CAUSE_PLEASE_REPORT;

		try {
			String strAAErrorCode = PlatformUtiltiies.nullHandler(getAACodesMapping().getString(me.getExceptionCode()));
			aaErrorCode = AAErrorCode.fromValue(strAAErrorCode);
		} catch (NullPointerException e) {
		} catch (MissingResourceException e) {
		} catch (ClassCastException e) {
		} catch (IllegalArgumentException e) {
		}

		return aaErrorCode;
	}

	/**
	 * Get the exception stack trace as a String
	 * 
	 * @param ex
	 * @return Stack Trace
	 */
	private static String getStackTrace(Exception ex, int lines) {
		StackTraceElement[] stackTrace = ex.getStackTrace();
		StringBuilder stackBuf = new StringBuilder();
		if (stackTrace != null) {
			for (int i = 0; i < lines; i++) {
				stackBuf.append(stackTrace[i].toString()).append(MaxicoCommonConstants.NEWLINE);
			}
		}
		return stackBuf.toString();
	}

	private static ResourceBundle getAACodesMapping() {
		if (aaErrorCodesMappings == null) {
			initializeData();
		}
		return aaErrorCodesMappings;
	}

	/**
	 * Initializes OTA codes tables and OTA-ModuleException mappings
	 */
	private static void initializeData() {
		synchronized (lock) {
			if (aaErrorCodesMappings == null) {
				Locale locale = new Locale("en");
				aaErrorCodesMappings = ResourceBundle.getBundle(AA_ERROR_CODES_MAPPINGS_RESOURCE_BUNDLE, locale);
			}
		}
	}
}
