package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Seat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeatMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSeat;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.FlightSeatStatuses;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

class AASeatMapBL implements TransactionalReservationProcessBL {

	private TransactionalReservationProcess transactionalReservationProcess;

	private String pnr;

	List<SegmentSeatMap> execute(List<BookingFlightSegment> bookingFlightSegments, String selectedLanguage,
			BookingSystems appEngine) throws ModuleException {

		Map<String, Integer> ondBundledFareSeatTemplates = AncillaryUtil.getOndBundledFareServiceTemplateIds(
				transactionalReservationProcess, pnr, null, EXTERNAL_CHARGES.SEAT_MAP.toString());
		boolean isBundledFareSelected = AncillaryUtil.isAtleastOneBundledFareTemplateExists(transactionalReservationProcess, pnr,
				null);

		Map<Integer, FlightSeatsDTO> seatMap = null;
		Map<FlightSegment, Map<Integer, FlightSeatsDTO>> flightSegmentSeatMap = new HashMap<FlightSegment, Map<Integer, FlightSeatsDTO>>();
		for (BookingFlightSegment flightSegment : bookingFlightSegments) {
			List<Integer> flightSegIds = AncillaryUtil.getFlightSegmentIds(flightSegment);
			seatMap = AAServicesModuleUtils.getSeatMapBD().getFlightSeats(flightSegIds, selectedLanguage, false);

			if (isBundledFareSelected) {
				if (ondBundledFareSeatTemplates != null
						&& ondBundledFareSeatTemplates.containsKey(flightSegment.getSegmentCode())
						&& ondBundledFareSeatTemplates.get(flightSegment.getSegmentCode()) != null) {

					Integer bundledFareTemplateId = ondBundledFareSeatTemplates.get(flightSegment.getSegmentCode());

					Collection<SeatDTO> offerSeats = AAServicesModuleUtils.getSeatMapBD().getFlightSeatsByChargeTemplate(
							bundledFareTemplateId);

					replaceAttachedSeatValuesWithOffer(seatMap, offerSeats, null, null, null);

				}
			} else {
				AnciOfferCriteriaTO availableOffer = null;

				if (appEngine != null && SalesChannelsUtil.isBookingSystemWebOrMobile(appEngine)) {
					AnciOfferCriteriaSearchTO anciOfferSearchCriteria = AnciOfferUtil.createAnciOfferSearch(flightSegment,
							AnciOfferType.SEAT, bookingFlightSegments);

					availableOffer = AAServicesModuleUtils.getAnciOfferBD().getAvailableAnciOffer(anciOfferSearchCriteria);
				}

				if (availableOffer != null) {
					Collection<SeatDTO> offerSeats = AAServicesModuleUtils.getSeatMapBD().getFlightSeatsByChargeTemplate(
							availableOffer.getTemplateID());

					replaceAttachedSeatValuesWithOffer(seatMap, offerSeats,
							availableOffer.getNameTranslations().get(selectedLanguage), availableOffer
									.getDescriptionTranslations().get(selectedLanguage), availableOffer.getTemplateID());

				}
			}

			flightSegmentSeatMap.put(flightSegment, AncillaryCommonUtil.rearrangeSeatRows(seatMap));

		}
		List<SegmentSeatMap> segmentSeatMaps = AncillaryUtil.transform(flightSegmentSeatMap);
		return segmentSeatMaps;
	}

	List<FlightSegmentSeats> getSelectedSeatDetails(List<SelectedSeat> selectedSeats) throws ModuleException {

		List<FlightSegmentSeats> flightSegmentSeatsList = new ArrayList<FlightSegmentSeats>();
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = getFlightSegmentIdsMap(selectedSeats);
		Map<Integer, Integer> flightSegIDWiseOfferTemplateIDs = getFlightSegWiseOfferTemplateIDs(selectedSeats);

		Map<Integer, FlightSeatsDTO> seatMap = AAServicesModuleUtils.getSeatMapBD().getFlightSeats(flightSegIdWiseCos, null);

		Map<Integer, Collection<SeatDTO>> segWiseOfferSeats = AAServicesModuleUtils.getSeatMapBD()
				.getSeatInfoForSeatChargeTemplates(flightSegIDWiseOfferTemplateIDs);

		for (Entry<Integer, FlightSeatsDTO> segmentEntry : seatMap.entrySet()) {

			Integer segmentId = segmentEntry.getKey();
			FlightSeatsDTO segmentSeats = segmentEntry.getValue();

			Collection<SeatDTO> offerSeats = segWiseOfferSeats.get(segmentId);

			for (SeatDTO seatDTO : (Collection<SeatDTO>) segmentSeats.getSeats()) {
				for (SelectedSeat selectedSeat : selectedSeats) {
					if (selectedSeat.getSeatNumber().contains(seatDTO.getSeatCode())
							&& selectedSeat.getFlightSegment().getFlightRefNumber().equals(segmentId.toString())) {
						Seat seat = new Seat();
						seat.setSeatCharge(seatDTO.getChargeAmount());
						seat.setSeatNumber(seatDTO.getSeatCode());

						if (offerSeats != null) {
							for (SeatDTO offerSeat : offerSeats) {
								if (seat.getSeatNumber().equalsIgnoreCase(offerSeat.getSeatCode())) {
									seat.setSeatCharge(offerSeat.getChargeAmount());
								}
							}
						}

						FlightSegmentSeats flightSegmentSeats = new FlightSegmentSeats();
						flightSegmentSeats.setFlightSegment(selectedSeat.getFlightSegment());
						flightSegmentSeats.getSeat().add(seat);

						flightSegmentSeatsList.add(flightSegmentSeats);
					}
				}
			}
		}
		return flightSegmentSeatsList;
	}

	/**
	 * Replaces selected seat prices with the offered seat prices. Sets the offer details to the selected seat details.
	 * 
	 * @param selectedSeatMap
	 *            Selected seat details.
	 * @param offerSeats
	 *            Offered seats.
	 * @param offerName
	 *            Name of the selected anci offer.
	 * @param offerDescription
	 *            Description of the selected anci offer.
	 * @param templateID
	 *            Seat charge template of the selected anci offer.
	 */
	private void replaceAttachedSeatValuesWithOffer(Map<Integer, FlightSeatsDTO> selectedSeatMap, Collection<SeatDTO> offerSeats,
			String offerName, String offerDescription, Integer templateID) {
		for (Entry<Integer, FlightSeatsDTO> segWiseSeats : selectedSeatMap.entrySet()) {

			FlightSeatsDTO selFlightSeatDTO = segWiseSeats.getValue();
			selFlightSeatDTO.setAnciOffer(true);
			selFlightSeatDTO.setAnciOfferName(offerName);
			selFlightSeatDTO.setAnciOfferDescription(offerDescription);
			selFlightSeatDTO.setOfferedTemplateID(templateID);

			Collection<SeatDTO> selectedSeats = selFlightSeatDTO.getSeats();

			for (SeatDTO selectedSeat : selectedSeats) {
				for (SeatDTO offeredSeat : offerSeats) {
					if (selectedSeat.getSeatCode().equals(offeredSeat.getSeatCode())) {
						selectedSeat.setChargeAmount(offeredSeat.getChargeAmount());
						if (FlightSeatStatuses.INACTIVE.equals(offeredSeat.getStatus())) {
							selectedSeat.setStatus(FlightSeatStatuses.RESERVED);
						}
					}
				}
			}
		}
	}

	private Map<Integer, Integer> getFlightSegWiseOfferTemplateIDs(List<SelectedSeat> selectedSeats) throws ModuleException {
		Map<Integer, Integer> segOfferTemplateIDs = new HashMap<Integer, Integer>();
		boolean isBundledFareSelected = false;

		Map<String, BundledFareDTO> segmentBundledFareMap = AncillaryUtil.getSegmentCodeWiseBundledFares(
				transactionalReservationProcess, pnr, null);
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (SelectedSeat selectedSeat : selectedSeats) {
				BookingFlightSegment bkgFlightSeg = selectedSeat.getFlightSegment();
				if (segmentBundledFareMap.containsKey(bkgFlightSeg.getSegmentCode())) {
					isBundledFareSelected = true;
					BundledFareDTO bundledFareDTO = segmentBundledFareMap.get(bkgFlightSeg.getSegmentCode());
					if (bundledFareDTO != null) {
						segOfferTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
								bundledFareDTO.getApplicableServiceTemplateId(EXTERNAL_CHARGES.SEAT_MAP.toString()));
					}
				}
			}
		}

		// If bundled fare is not selected, then check for any applied anci offers
		if (!isBundledFareSelected) {
			for (SelectedSeat selectedSeat : selectedSeats) {
				BookingFlightSegment bkgFlightSeg = selectedSeat.getFlightSegment();
				segOfferTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()), selectedSeat.getOfferedAnciTemplateID());
			}
		}

		return segOfferTemplateIDs;
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdsMap(List<SelectedSeat> selectedSeats) {
		Map<Integer, ClassOfServiceDTO> flightSegmentIds = new HashMap<Integer, ClassOfServiceDTO>();

		for (SelectedSeat selectedSeat : selectedSeats) {
			BookingFlightSegment bkgFlightSeg = selectedSeat.getFlightSegment();
			flightSegmentIds.put(
					new Integer(bkgFlightSeg.getFlightRefNumber()),
					new ClassOfServiceDTO(bkgFlightSeg.getCabinClassCode(), bkgFlightSeg.getLogicalCabinClassCode(), bkgFlightSeg
							.getSegmentCode()));
		}

		return flightSegmentIds;
	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
