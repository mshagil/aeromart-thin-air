/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aaservices.core.web;

import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author Nilindra Fernando
 */
public class InitServlet extends HttpServlet {

	private static final long serialVersionUID = 1659584818002289334L;

	private static Log log = LogFactory.getLog(InitServlet.class);

	@Override
	public void init() {
		/**
		 * Initializing the application framework Module configurations are picked from first available of the
		 * followings - location specified by repository.home envioronment variable - location sepecified at
		 * PlatformConstants.getConfigRootAbsPath()
		 */
		ModuleFramework.startup();
		scheduleTasks();
	}

	private void scheduleTasks() {
		try {
			AALocalQuartzScheduler localQuartzScheduler = new AALocalQuartzScheduler();
			localQuartzScheduler.scheduleAndStartScheduler();
		} catch (Exception e) {
			log.error("Starting Quartz scheduler failed", e);
			throw new RuntimeException(e);
		}
	}
}
