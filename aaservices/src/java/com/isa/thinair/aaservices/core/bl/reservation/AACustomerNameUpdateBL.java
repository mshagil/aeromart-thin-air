package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AACustomerNameUpdateRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AACustomerNameUpdateRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.utils.LmsMemberGenderEnum;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.LoyaltyNameValidator;

public class AACustomerNameUpdateBL {

	private static Log log = LogFactory.getLog(AACustomerNameUpdateBL.class);

	AACustomerNameUpdateRQ aaCustomerNameUpdateRQ = null;

	public AACustomerNameUpdateBL(AACustomerNameUpdateRQ aaCustomerNameUpdateRQ) {
		this.aaCustomerNameUpdateRQ = aaCustomerNameUpdateRQ;
	}

	public AACustomerNameUpdateRS execute() {

		String ffid = aaCustomerNameUpdateRQ.getMemberFFID();

		AACustomerNameUpdateRS aaCustomerNameUpdateRS = new AACustomerNameUpdateRS();

		ReservationBD reservationBD = AAServicesModuleUtils.getReservationBD();
		TrackInfoDTO trackInfoDTO = null;

		trackInfoDTO = AAUtils.getTrackInfo(aaCustomerNameUpdateRQ.getAaPos());

		try {
			AirCustomerServiceBD airCustomerServiceBD = AAServicesModuleUtils.getAirCustomerServiceBD();
			Customer customer = airCustomerServiceBD.getCustomer(ffid);
			Customer oldCustomer = populateOldCustomerForAudit(customer);
			if (customer != null) {
				customer.setFirstName(aaCustomerNameUpdateRQ.getFirstName());
				customer.setLastName(aaCustomerNameUpdateRQ.getLastName());
				customer.setTitle(aaCustomerNameUpdateRQ.getTitle());
				if (customer.getLMSMemberDetails() != null) {
					customer.getLMSMemberDetails().setFirstName(aaCustomerNameUpdateRQ.getFirstName());
					customer.getLMSMemberDetails().setLastName(aaCustomerNameUpdateRQ.getLastName());
					if (aaCustomerNameUpdateRQ.getTitle().equalsIgnoreCase("MR")) {
						customer.getLMSMemberDetails().setGenderTypeId(LmsMemberGenderEnum.MALE.getCode());
					} else if (aaCustomerNameUpdateRQ.getTitle().equalsIgnoreCase("MS")) {
						customer.getLMSMemberDetails().setGenderTypeId(LmsMemberGenderEnum.FEMALE.getCode());
					} else {
						customer.getLMSMemberDetails().setGenderTypeId(LmsMemberGenderEnum.UNDEFINED.getCode());
					}
				}

				airCustomerServiceBD.saveOrUpdateWithName(oldCustomer, customer, trackInfoDTO.getMarketingUserId());
			}

			if (!LoyaltyNameValidator.compareNames(customer.getFirstName(), customer.getLastName(), oldCustomer.getFirstName(),
					oldCustomer.getLastName())) {
				Collection<String> pnrList = reservationBD.removeFFIDFromUnflownPNRs(ffid, trackInfoDTO);
				aaCustomerNameUpdateRS.setResult(pnrList.size());
				AAResponseAttributes aaResponseAttributes = new AAResponseAttributes();
				aaResponseAttributes.setSuccess(new AASuccess());
				aaCustomerNameUpdateRS.setResponseAttributes(aaResponseAttributes);
				if (pnrList.isEmpty()) {
					log.info("No reservations updated");
				} else {
					for (String pnr : pnrList) {
						log.info("Removed ffid : " + ffid + " from PNR : " + pnr + ".");
					}
				}
			}

		} catch (ModuleException e) {
			log.error("Member name update failed.", e);
			AAExceptionUtil.addAAErrrors(aaCustomerNameUpdateRS.getResponseAttributes().getErrors(), e);
		}

		return aaCustomerNameUpdateRS;
	}

	private Customer populateOldCustomerForAudit(Customer oldCustomer) {
		Customer customer = new Customer();
		customer.setEmailId(oldCustomer.getEmailId());
		customer.setTitle(oldCustomer.getTitle());
		customer.setFirstName(oldCustomer.getFirstName());
		customer.setLastName(oldCustomer.getLastName());
		customer.setTelephone(oldCustomer.getTelephone());
		customer.setMobile(oldCustomer.getMobile());
		customer.setCountryCode(oldCustomer.getCountryCode());
		customer.setNationalityCode(oldCustomer.getNationalityCode());
		if (oldCustomer.getLMSMemberDetails() != null) {
			LmsMember lmsMember = new LmsMember();
			lmsMember.setDateOfBirth(oldCustomer.getLMSMemberDetails().getDateOfBirth());
			lmsMember.setPassportNum(oldCustomer.getLMSMemberDetails().getPassportNum());
			lmsMember.setLanguage(oldCustomer.getLMSMemberDetails().getLanguage());
			lmsMember.setFfid(oldCustomer.getLMSMemberDetails().getHeadOFEmailId());
			Set<LmsMember> set = new HashSet<LmsMember>();
			set.add(lmsMember);
			customer.setCustomerLmsDetails(set);
		}
		return customer;
	}

}
