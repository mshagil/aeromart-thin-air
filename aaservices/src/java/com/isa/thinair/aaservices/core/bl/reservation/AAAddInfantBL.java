package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.HeaderInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAExternalChargesUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServiceConstants;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.converters.ServiceTaxConverterUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.decorator.ReservationMediator;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;

public class AAAddInfantBL {
	private static Log log = LogFactory.getLog(AAAddInfantBL.class);

	private AAAirBookModifyRQ aaAirBookModifyRQ;
	private AAAirBookRS aaAirBookRS;

	private void initializeAAAirBookRS() {
		aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAAirBookRS execute() {

		Reservation reservation = null;
		Collection privilegeKeys = null;
		String pnr = null;
		String versionString = "";
		initializeAAAirBookRS();

		privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
		versionString = aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion();

		if (log.isDebugEnabled()) {
			log.debug("Retreived PNR " + pnr + " for add Infant operation");
		}

		long version = -1l;
		if (versionString != null) {
			version = Long.parseLong(versionString);
		}
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setRecordAudit(true);

		try {
			AAReservationUtil.authorize(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.RES_ADD_INFANT);
			reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

			if (reservation != null) {

				int noOfInfants = 1;// FIXME : Check whether this can be >1 in XBE
				// Get the travelers
				List<AirTraveler> travellers = aaAirBookModifyRQ.getAaReservation().getAirReservation().getTravelerInfo()
						.getAirTraveler();
				// for on-hold or confirmed booking, leave the same.
				Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE;
				// get the fares and assemble the passenger interface
				ReservationMediator reservationMediator = new ReservationMediator(reservation);
				// TODO: Get correct price quote for interline add infant
				UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();
				String stationCode = userPrincipal.getAgentStation();
				String agentCode = userPrincipal.getAgentCode();
				Collection inventoryFares = AAServicesModuleUtils.getFlightInventoryResBD().getInfantQuote(
						reservationMediator.getOndFareDTOs(noOfInfants), stationCode, true, agentCode);
				IPassenger iPassenger = null;
				// Check whether this reservation's segments are CNX, if so change the quoted charges to zero amount
				if (isAllSegmentsCancelledForCarrier(reservation)) {
					Collection<OndFareDTO> invFares = new ArrayList<OndFareDTO>(inventoryFares);
					inventoryFares = getInfantQuoteForCanceledReservations(invFares, reservation);
				}
				iPassenger = new PassengerAssembler(inventoryFares);

				Integer adultPaxSeq = null;
				Integer adultPaxPnrPaxID = null;
				Integer infantPaxSequence = null;
				AirTraveler infantTraveler = null;
				String foidNumber = null;
				Date foidExpiry = null;
				String foidIssuedCntry = null;
				PaxCategoryFoidTO paxCategoryFOIDTO = null;

				for (AirTraveler airTraveler : travellers) {
					if (airTraveler.isAccompaniedByInfant()) {
						adultPaxSeq = PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber());
					} else {
						infantPaxSequence = getInfantPaxSequence(airTraveler.getTravelerRefNumber());
						infantTraveler = airTraveler;
					}
				}
				for (ReservationPax reservationPax : reservation.getPassengers()) {
					if (adultPaxSeq != null && adultPaxSeq.compareTo(reservationPax.getPaxSequence()) == 0) {
						adultPaxPnrPaxID = reservationPax.getPnrPaxId();
						break;
					}
				}
				
				if (log.isDebugEnabled()) {
					log.debug("Adult pax sequence is : " + adultPaxSeq.intValue());
					log.debug("Infant pax sequence is : " + infantPaxSequence.intValue());
				}
				// Prepare FOID details
				if (infantTraveler.getPassengerType() != null) {
					paxCategoryFOIDTO = AAUtils.getPaxCategoryFOID(AAServiceConstants.DEFAULT_PAX_CATEGORY,
							infantTraveler.getPassengerType());
				}

				if (infantTraveler != null) {
					if (infantTraveler.getFormOfIdentification() != null) {
						foidNumber = infantTraveler.getFormOfIdentification().getFoidNumber();
					}
				}
				AAUtils.validateFOIDNumber(paxCategoryFOIDTO, foidNumber);

				// Gets the nationality code from nationality ISO code
				Integer intnationality = null;
				if (infantTraveler.getNationalityCode() != null) {
					int nationalityCode = AAServicesModuleUtils.getCommonMasterBD()
							.getNationality(infantTraveler.getNationalityCode()).getNationalityCode();
					intnationality = new Integer(nationalityCode);
				}

				if (infantTraveler.getFormOfIdentification() != null) {
					foidExpiry = infantTraveler.getFormOfIdentification().getPsptExpiry() != null ? infantTraveler
							.getFormOfIdentification().getPsptExpiry().toGregorianCalendar().getTime() : null;
					foidIssuedCntry = infantTraveler.getFormOfIdentification().getPsptIssuedCntry();
				}

				// Add infant to PassengerAssembler
				iPassenger.addInfant(infantTraveler.getPersonName().getFirstName(), infantTraveler.getPersonName().getSurName(),
						null, infantTraveler.getBirthDate() != null ? infantTraveler.getBirthDate().toGregorianCalendar()
								.getTime() : null, intnationality, infantPaxSequence, adultPaxSeq, foidNumber, foidExpiry,
						foidIssuedCntry, null, null, null, null, infantTraveler.getPassengerCategory(), null, null, null, "", "",
						"", "", null);
				// blank payment
				IPayment iPaymentPax = new PaymentAssembler();
				iPassenger.addPassengerPayments(adultPaxPnrPaxID, iPaymentPax);
				Map<String, Collection<ExternalChgDTO>> externalChargesMap = AAExternalChargesUtil.getPassengerWiseExternalCharges(
						aaAirBookModifyRQ.getAaReservation().getAirReservation().getPriceInfo(), ChargeRateOperationType.MODIFY_ONLY);
				
				ServiceTaxQuoteForTicketingRevenueRQ serviceTaxRQ = ServiceTaxConverterUtil.createServiceTaxQuoteForAddInfantRequest(
						inventoryFares, reservation, userPrincipal, null, null);
				ServiceTaxQuoteForTicketingRevenueRS serviceTaxRS = AirproxyModuleUtils.getChargeBD()
						.quoteServiceTaxForTicketingRevenue(serviceTaxRQ);
				if (serviceTaxRS.getPaxTypeWiseServiceTaxes() != null && !serviceTaxRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
					List<ServiceTaxDTO> infantServiceTaxs = serviceTaxRS.getPaxTypeWiseServiceTaxes().get(PaxTypeTO.INFANT);
					if (infantServiceTaxs != null && !infantServiceTaxs.isEmpty()) {
						if (infantServiceTaxs != null) {
							for (ServiceTaxDTO infantServiceTax : infantServiceTaxs) {
								ServiceTaxExtChgDTO serviceTaxExtChgDTO = ServiceTaxConverterUtil.externalChargeAdaptor(
										infantServiceTax, serviceTaxRS.getServiceTaxDepositeCountryCode(),
										serviceTaxRS.getServiceTaxDepositeStateCode());
								if (externalChargesMap.get(adultPaxSeq.toString()) != null) {
									externalChargesMap.get(adultPaxSeq.toString()).add(serviceTaxExtChgDTO);
								} else {
									Collection<ExternalChgDTO> externalCharges = new ArrayList<ExternalChgDTO>();
									externalCharges.add(serviceTaxExtChgDTO);
									externalChargesMap.put(adultPaxSeq.toString(), externalCharges);
								}
							}
						}
					}
				}
				
				iPaymentPax.addExternalCharges(externalChargesMap.get(adultPaxSeq.toString()));
				
				Map<Integer, List<ExternalChgDTO>> paxCharges = AAModifyReservationQueryUtil
						.tranformPaxExternalChargeToExternalChgDTO(aaAirBookModifyRQ.getPerPaxExternalCharges());
				if (paxCharges != null && !paxCharges.isEmpty()) {
					iPaymentPax.addExternalCharges(paxCharges.get(adultPaxSeq));
				}

				
				if (log.isDebugEnabled()) {
					log.debug("Calling block seats for add infant");
				}
				// Block the seats
				TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaAirBookModifyRQ.getAaPos());
				Collection blockKeyIds = AAServicesModuleUtils.getReservationBD().blockSeats(
						reservationMediator.getOndFareDTOs(noOfInfants), null);
				if (log.isDebugEnabled() && blockKeyIds != null) {
					log.debug("No of blocked seats : " + blockKeyIds.size());
				}

				AutoCancellationInfo autoCancellationInfo = null;
				if (aaAirBookModifyRQ.getAutoCancellationInfo() != null) {
					autoCancellationInfo = AAAutoCancellationBL.populateAutoCancellation(aaAirBookModifyRQ
							.getAutoCancellationInfo());
				}

				// add the infant.
				AAServicesModuleUtils.getPassengerBD().addInfant(pnr, iPassenger, blockKeyIds, intPaymentMode,
						reservation.getVersion(), null, null, false, false, false, autoCancellationInfo);
				aaAirBookRS = loadReservation(pnrModesDTO);
			}
		} catch (Exception ex) {
			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), ex);
		}
		return aaAirBookRS;
	}

	/**
	 * 
	 * @param inventoryFares
	 * @param reservation
	 * @return
	 */
	private Collection getInfantQuoteForCanceledReservations(Collection<OndFareDTO> inventoryFares, Reservation reservation) {
		for (OndFareDTO ondFareDTO : inventoryFares) {
			ondFareDTO.setAllCharges(new ArrayList());
			ondFareDTO.setFareType(0);

		}
		return inventoryFares;
	}

	private boolean isAllSegmentsCancelledForCarrier(Reservation reservation) {
		List<Boolean> segmentStatus = new ArrayList<Boolean>();
		int segCount = 0;
		for (ReservationPax passenger : reservation.getPassengers()) {
			for (ReservationPaxFare paxFare : passenger.getPnrPaxFares()) {
				for (ReservationPaxFareSegment paxFareSeg : paxFare.getPaxFareSegments()) {
					if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(paxFareSeg.getSegment().getStatus())) {
						segmentStatus.add(new Boolean(true));
					}
					segCount++; // Gets the total segments count
				}
			}

			break;// Checking one pax is enough as all are having the same segments
		}
		if (segmentStatus.size() >= segCount) {
			return true;
		} else {
			return false;
		}
	}

	private AAAirBookRS loadReservation(LCCClientPnrModesDTO pnrModesDTO) {
		AAReadRQ aaReadRQ = new AAReadRQ();
		aaReadRQ.setPnr(pnrModesDTO.getPnr());
		HeaderInfo headerInfo = new HeaderInfo();
		aaReadRQ.setHeaderInfo(headerInfo);

		if ((aaAirBookModifyRQ != null) && (aaAirBookModifyRQ.getLoadDataOptions() != null)) {
			aaAirBookModifyRQ.getLoadDataOptions().setLoadPTCPriceInfo(true);
		}

		// Set these booleans for interline reservations.
		// TODO : Need to verify and remove unwanted
		pnrModesDTO.setLoadLocalTimes(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);

		aaReadRQ.setLoadReservationOptions(AAReservationUtil.createLoadReservationOptions(pnrModesDTO));
		AASearchReservationByPnrBL aaSearchReservationBL = new AASearchReservationByPnrBL();
		aaSearchReservationBL.setRequest(aaReadRQ);

		return aaSearchReservationBL.execute();
	}

	/**
	 * Extract the infant sequence number from infant traveler ref number
	 * 
	 * @param travellerRefNum
	 * @return
	 */
	private Integer getInfantPaxSequence(String travellerRefNum) {
		if (travellerRefNum.indexOf("$") > 0) {
			return PaxTypeUtils.getPaxSeq(travellerRefNum);
		} else {
			String infSeq = travellerRefNum.substring(1, travellerRefNum.indexOf("/"));
			return new Integer(infSeq);
		}
	}

	public AAAirBookModifyRQ getAaAirBookModifyRQ() {
		return aaAirBookModifyRQ;
	}

	public void setAaAirBookModifyRQ(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

}
