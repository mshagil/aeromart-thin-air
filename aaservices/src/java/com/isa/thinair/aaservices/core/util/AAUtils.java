/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Address;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Country;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CreditStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FormOfIdentification;
import com.isa.connectivity.profiles.maxico.v1.common.dto.HeaderInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LMSPaymentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxProductAmounts;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCardType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCurrencyAmount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SystemType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Telephone;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO.status;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.LccReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.BaseContactConfigDTO;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.dto.WSPaxConfigDTO;
import com.isa.thinair.commons.api.dto.XBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.XMLDataTypeUtil;
import com.isa.thinair.lccclient.api.util.LCCUtils;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;

public class AAUtils {

	public static void extractReservationPayments(List<PaymentDetails> paymentDetailsList, IPayment reservationPayments)
			throws WebservicesException, ModuleException {

		for (PaymentDetails paymentDetail : paymentDetailsList) {
			BigDecimal paymentAmount = paymentDetail.getPaymentSummary().getPaymentAmount();

			Integer originalPaymentTnxId = (paymentDetail.getOriginalPaymentTnxRefNumber() != null ? new Integer(
					paymentDetail.getOriginalPaymentTnxRefNumber()) : null);

			PayCurrencyDTO payCurrencyDtoFromMC = getPayCurrencyDTO(paymentDetail.getPaymentCurrencyAmount());
			if (paymentDetail.getDirectBill() != null) {
				String agentCode = paymentDetail.getDirectBill().getAgentCode();
				reservationPayments.addAgentCreditPayment(agentCode, paymentAmount, paymentDetail.getPaymentRefNumber(), null,
						paymentDetail.getPaymentSummary().getPaymentCarrier(), payCurrencyDtoFromMC,
						paymentDetail.getUniqueTnxId(), paymentDetail.getPaymentSummary().getPaymentMethod(),
						originalPaymentTnxId);
			} else if (paymentDetail.isCash()) {
				reservationPayments.addCashPayment(paymentAmount, paymentDetail.getPaymentSummary().getPaymentCarrier(),
						payCurrencyDtoFromMC, paymentDetail.getUniqueTnxId(), originalPaymentTnxId);
			} else if (paymentDetail.getPaymentCard() != null) {
				CommonCreditCardPayment paymentCard = paymentDetail.getPaymentCard();
				AppIndicatorEnum appIndicatorEnum = SalesChannelsUtil.isAppIndicatorWebOrMobile(paymentCard.getAppIndicator())
						? AppIndicatorEnum.APP_IBE
						: AppIndicatorEnum.APP_XBE;
				TnxModeEnum tnxModeEnum = paymentCard.getTnxMode().intValue() == TnxModeEnum.MAIL_TP_ORDER.getCode()
						? TnxModeEnum.MAIL_TP_ORDER
						: TnxModeEnum.SECURE_3D;

				reservationPayments.addCardPayment(getCardType(paymentCard), paymentCard.getEDate(), paymentCard.getNo(),
						paymentCard.getName(), paymentCard.getAddress(), paymentCard.getSecurityCode(), paymentAmount,
						appIndicatorEnum,
						tnxModeEnum,
						paymentCard.getOldCCRecordId(),
						null,
						// Note : we can set ipgIdentificationParamsDTO as null
						// because there will not be any payment for this
						paymentDetail.getPaymentSummary().getPaymentCarrier(), payCurrencyDtoFromMC,
						paymentDetail.getUniqueTnxId(), paymentCard.getAuthorizationId(), paymentCard.getPaymentBrokerId(),
						originalPaymentTnxId);

			} else if (paymentDetail.getTravelerCreditPayment() != null) {

				TravelerCreditPayment travelerCreditPayment = paymentDetail.getTravelerCreditPayment();
				reservationPayments.addCreditPayment(paymentDetail.getPaymentSummary().getPaymentAmount(), null, paymentDetail
						.getPaymentSummary().getPaymentCarrier(), payCurrencyDtoFromMC, paymentDetail.getUniqueTnxId(), null,
						travelerCreditPayment.getEDate(), travelerCreditPayment.getBasePnr(), travelerCreditPayment
								.getPaxCreditId(), travelerCreditPayment.getUtilizingPnr(), originalPaymentTnxId);
			} else if (paymentDetail.getLmsPaymentInfo() != null) {
				LMSPaymentInfo lmsPaymentInfo = paymentDetail.getLmsPaymentInfo();
				String[] rewardIds = new String[lmsPaymentInfo.getRewardIds().size()];
				int i = 0;
				for (String e : lmsPaymentInfo.getRewardIds()) {
					rewardIds[i++] = e;
				}
				reservationPayments.addLMSPayment(lmsPaymentInfo.getMemberAccountId(), rewardIds,
						lmsPaymentInfo.getTotalPaymentAmount(), paymentDetail.getPaymentSummary().getPaymentCarrier(),
						payCurrencyDtoFromMC, paymentDetail.getUniqueTnxId(), originalPaymentTnxId);
			} else if (paymentDetail.getVoucherPayment() != null) {
				VoucherDTO voucherDTO = new VoucherDTO();
				voucherDTO.setVoucherId(paymentDetail.getVoucherPayment().getVoucherID());
				voucherDTO.setAmountInBase(paymentDetail.getVoucherPayment().getVoucherAmount().toString());
				voucherDTO.setRedeemdAmount(paymentDetail.getVoucherPayment().getVoucherRedeemAmount().toString());
				reservationPayments.addVoucherPayment(voucherDTO, paymentDetail.getPaymentSummary().getPaymentAmount(),
						paymentDetail.getPaymentSummary().getPaymentCarrier(), payCurrencyDtoFromMC,
						paymentDetail.getUniqueTnxId(), null);
			}
		}
	}
	
	
	private static PayCurrencyDTO getPayCurrencyDTO(PaymentCurrencyAmount paymentCurrencyAmount) {
		String payCurrencyCode = paymentCurrencyAmount.getCurrencyCodeGroup().getCurrencyCode();
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(payCurrencyCode,
				paymentCurrencyAmount.getPayCurrMultiplyingExchangeRate(), paymentCurrencyAmount.getBoundaryValue(),
				paymentCurrencyAmount.getBreakPointValue());
		return payCurrencyDTO;
	}

	/**
	 * Method to get the pax FOID details
	 * 
	 * @param paxCategoryCode
	 * @param passengerType
	 * @return
	 * @throws WebservicesException
	 */
	public static PaxCategoryFoidTO getPaxCategoryFOID(String paxCategoryCode, PassengerType passengerType)
			throws WebservicesException {
		PaxCategoryFoidTO paxCategoryFoidTo = null;
		boolean found = false;
		String paxTypeCode = null;

		paxTypeCode = PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(passengerType);

		if (paxTypeCode == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Passenger Type is Invalid");
		}

		List paxConfigList = AAServicesModuleUtils.getGlobalConfig().getpaxConfig("WS");

		for (Object obj : paxConfigList) {
			if (obj instanceof WSPaxConfigDTO) {
				WSPaxConfigDTO wsPaxConfig = (WSPaxConfigDTO) obj;
				if (!wsPaxConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
					continue;
				} else {
					if (wsPaxConfig.getPaxCatCode() != null && wsPaxConfig.getPaxTypeCode() != null && (paxCategoryCode != null)
							&& (paxTypeCode != null) && (wsPaxConfig.getPaxCatCode().trim().equals(paxCategoryCode))
							&& (wsPaxConfig.getPaxTypeCode().equals(paxTypeCode))) {
						paxCategoryFoidTo = new PaxCategoryFoidTO();
						String required = "N";
						if (wsPaxConfig.isWsVisibility() && wsPaxConfig.isWsMandatory()) {
							required = "Y";
						}
						paxCategoryFoidTo.setPaxCatFoidRequired(required);
						found = true;
						break;
					}
				}
			}
		}

		if (!found) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE,
					"Passenger Category FOID details not found");
		}

		return paxCategoryFoidTo;
	}

	/**
	 * Method to get the foidNumber
	 * 
	 * @param list
	 * @param rph
	 * @return
	 */
	static String getFOIDNumber(List<AirTraveler> list, String rph) {
		String foidNumber = null;
		if (list != null) {
			for (AirTraveler traveler : list) {
				if (rph.equals(traveler.getTravelerRefNumber()) && traveler.getFormOfIdentification() != null) {
					foidNumber = traveler.getFormOfIdentification().getFoidNumber();
					break;
				}
			}
		}
		return foidNumber;
	}

	/**
	 * Method to validate foid details.
	 * 
	 * @param paxCategoryFOIDTO
	 * @param foidNumber
	 * @return
	 */
	public static void validateFOIDNumber(PaxCategoryFoidTO paxCategoryFOIDTO, String foidNumber) throws WebservicesException {
		if ((paxCategoryFOIDTO.getPaxCatFoidRequired().equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE))
				&& ((foidNumber == null) || (foidNumber.trim().equals("")))) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "FOID Number required");
		}
	}

	/**
	 * Method to get the 2 letter iso code from the nationality code
	 * 
	 * @param nationalityCode
	 *            Integer representing the nationality in the DB
	 * @return 2 letter iso code
	 * @throws ModuleException
	 */
	public static String getNationalityIsoCode(Integer nationalityCode) throws ModuleException {
		String nationality = null;
		if (nationalityCode != null) {
			nationality = AAServicesModuleUtils.getCommonMasterBD().getNationality(nationalityCode).getIsoCode();
		}

		return nationality;
	}

	/**
	 * Method to get the nationality code from the 2 letter iso code
	 * 
	 * @param nationalityIsoCode
	 *            2 letter iso code
	 * @return Integer representing the nationality in the DB
	 * @throws ModuleException
	 */
	static Integer getNationalityCode(String nationalityIsoCode) throws ModuleException {
		Integer nationality = null;
		if (nationalityIsoCode != null) {
			nationality = AAServicesModuleUtils.getCommonMasterBD().getNationality(nationalityIsoCode).getNationalityCode();
		}

		return nationality;
	}

	/**
	 * Returns the payment card type
	 * 
	 * @param nominalCode
	 * @return
	 */
	static PaymentCardType getPaymentCardType(Integer nominalCode) {

		PaymentCardType paymentCardType = PaymentCardType.GENERIC;

		if (nominalCode != null) {
			if (LccReservationTnxNominalCode.getVisaTypeNominalCodes().contains(nominalCode)) {
				paymentCardType = PaymentCardType.VISA;
			} else if (LccReservationTnxNominalCode.getMasterTypeNominalCodes().contains(nominalCode)) {
				paymentCardType = PaymentCardType.MASTER;
			} else if (LccReservationTnxNominalCode.getAmexTypeNominalCodes().contains(nominalCode)) {
				paymentCardType = PaymentCardType.AMERICAN_EXPRESS;
			} else if (LccReservationTnxNominalCode.getDinersTypeNominalCodes().contains(nominalCode)) {
				paymentCardType = PaymentCardType.DINERS_CLUB;
			} else if (LccReservationTnxNominalCode.getCMITypeNominalCodes().contains(nominalCode)) {
				paymentCardType = PaymentCardType.CMI;
			} else if (LccReservationTnxNominalCode.getGenericTypeNominalCodes().contains(nominalCode)) {
				paymentCardType = PaymentCardType.GENERIC;
			}
		}

		return paymentCardType;
	}

	/**
	 * Populate HeaderInfo
	 * 
	 * @throws ModuleException
	 */
	public static HeaderInfo populateHeaderInfo(String transactionIdentified) {

		HeaderInfo headerInfo = new HeaderInfo();
		headerInfo.setTimeStamp(new Date());
		headerInfo.setTransactionIdentifier(transactionIdentified);

		return headerInfo;
	}

	public static TrackInfoDTO getTrackInfo(AAPOS aaPos) {

		String marketingIpAddress = BeanUtils.nullHandler(aaPos.getMarketingIPAddress());
		String marketingAirLineCode = BeanUtils.nullHandler(aaPos.getMarketingAirLineCode());

		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setIpAddress(marketingIpAddress);
		trackInfoDTO.setCarrierCode(marketingAirLineCode);

		trackInfoDTO.setMarketingUserId(BeanUtils.nullHandler(aaPos.getMarketingUserId()));
		trackInfoDTO.setMarketingAgentStationCode(BeanUtils.nullHandler(aaPos.getAirportCode()));
		trackInfoDTO.setMarketingAgentCode(BeanUtils.nullHandler(aaPos.getMarketingAgentCode()));
		
		String salesChannelFromPOS = getSalesChannelFromPOS(aaPos);
		
		if (StringUtils.isNotEmpty(salesChannelFromPOS)) {
			trackInfoDTO.setMarketingBookingChannel(Integer.valueOf(salesChannelFromPOS));
		}

		return trackInfoDTO;
	}

	public static String getSalesChannelFromPOS(AAPOS aapos) {
		Integer salesChannel = null;
		if (aapos.getMarketingBookingChannel() != null) {
			if (aapos.getMarketingBookingChannel().compareTo(BookingSystems.AARES_IBE) == 0) {
				salesChannel = SalesChannelsUtil.SALES_CHANNEL_WEB;
			} else if (aapos.getMarketingBookingChannel().compareTo(BookingSystems.AARES_XBE) == 0) {
				salesChannel = SalesChannelsUtil.SALES_CHANNEL_AGENT;
			} else if (aapos.getMarketingBookingChannel().compareTo(BookingSystems.AAHOLIDAYS_IBE) == 0) {
				salesChannel = SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS;
			} else if (aapos.getMarketingBookingChannel().compareTo(BookingSystems.AAHOLIDAYS_XBE) == 0) {
				salesChannel = SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS;
			} else if(aapos.getMarketingBookingChannel().compareTo(BookingSystems.AARES_IOS) == 0){
				salesChannel = SalesChannelsUtil.SALES_CHANNEL_IOS;
			} else if(aapos.getMarketingBookingChannel().compareTo(BookingSystems.AARES_ANDROID) == 0){
				salesChannel = SalesChannelsUtil.SALES_CHANNEL_ANDROID;
			}
		}
		return (salesChannel != null) ? salesChannel.toString() : null;
	}

	public static Map<Integer, ReservationPax> getReservationPaxSequence(Collection<ReservationPax> reservationPax) {
		Map<Integer, ReservationPax> sequenceReservationPax = new HashMap<Integer, ReservationPax>();
		for (ReservationPax pax : reservationPax) {
			sequenceReservationPax.put(pax.getPaxSequence(), pax);
		}
		return sequenceReservationPax;
	}

	static int getCardType(CommonCreditCardPayment paymentCard) throws ModuleException {
		switch (paymentCard.getCardType()) {
		case MASTER:
			return 1;
		case VISA:
			return 2;
		case AMERICAN_EXPRESS:
			return 3;
		case DINERS_CLUB:
			return 4;
		case GENERIC:
			return 5;
		case CMI:
			return 6;
		default:
			throw new ModuleException("Invalid card type detected. Please contact administrator...");
		}
	}

	/**
	 * Filter to keep only visible pax fields of the carrier
	 */
	public static void enforceCarrierConfigForPax(Set passengers, String appIndicator) {
		List paxConfigList = AAServicesModuleUtils.getGlobalConfig().getpaxConfig(appIndicator);
		if (passengers != null) {
			for (ReservationPax rPax : (Collection<ReservationPax>) passengers) {
				for (Object obj : paxConfigList) {
					if (obj instanceof XBEPaxConfigDTO) {
						XBEPaxConfigDTO xbeConfig = (XBEPaxConfigDTO) obj;
						if (!xbeConfig.isXbeVisibility() && xbeConfig.getPaxCatCode().equals(rPax.getPaxCategory())
								&& xbeConfig.getPaxTypeCode().equals(rPax.getPaxType())) {

							if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE)) {
								rPax.setTitle("");
								continue;
							} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_FNAME)) {
								rPax.setFirstName("");
								continue;
							} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_LNAME)) {
								rPax.setLastName("");
								continue;
							} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONALITY)) {
								rPax.setNationalityCode(0);
								continue;
							} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_DOB)) {
								rPax.setDateOfBirth(null);
								continue;
							} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVELWITH)) {
								rPax.setIndexId(0);
								continue;
							}

							ReservationPaxAdditionalInfo addnInfo = rPax.getPaxAdditionalInfo();
							if (addnInfo != null) {
								if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
									addnInfo.setPassportNo("");
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_EXPIRY)) {
									addnInfo.setPassportExpiry(null);
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_ISSUED_CNTRY)) {
									addnInfo.setPassportIssuedCntry("");
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PLACE_OF_BIRTH)) {
									addnInfo.setPlaceOfBirth("");
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVEL_DOC_TYPE)) {
									addnInfo.setTravelDocumentType("");
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_NUMBER)) {
									addnInfo.setVisaDocNumber("");
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_ISSUE_DATE)) {
									addnInfo.setVisaDocIssueDate(null);
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_PLACE_OF_ISSUE)) {
									addnInfo.setVisaDocPlaceOfIssue("");
									continue;
								} else if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_APPLICABLE_COUNTRY)) {
									addnInfo.setVisaApplicableCountry("");
									continue;
								}
							}
						}
					} else if (obj instanceof IBEPaxConfigDTO) {
						IBEPaxConfigDTO ibeConfig = (IBEPaxConfigDTO) obj;
						if (!ibeConfig.isIbeVisibility() && ibeConfig.getPaxCatCode().equals(rPax.getPaxCategory())
								&& ibeConfig.getPaxTypeCode().equals(rPax.getPaxType())) {

							if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE)) {
								rPax.setTitle("");
								continue;
							} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_FNAME)) {
								rPax.setFirstName("");
								continue;
							} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_LNAME)) {
								rPax.setLastName("");
								continue;
							} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONALITY)) {
								rPax.setNationalityCode(null);
								continue;
							} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_DOB)) {
								rPax.setDateOfBirth(null);
								continue;
							} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVELWITH)) {
								rPax.setIndexId(0);
								continue;
							}

							ReservationPaxAdditionalInfo addnInfo = rPax.getPaxAdditionalInfo();
							if (addnInfo != null) {
								if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
									addnInfo.setPassportNo("");
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_EXPIRY)) {
									addnInfo.setPassportExpiry(null);
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_ISSUED_CNTRY)) {
									addnInfo.setPassportIssuedCntry("");
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PLACE_OF_BIRTH)) {
									addnInfo.setPlaceOfBirth("");
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVEL_DOC_TYPE)) {
									addnInfo.setTravelDocumentType("");
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_NUMBER)) {
									addnInfo.setVisaDocNumber("");
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_ISSUE_DATE)) {
									addnInfo.setVisaDocIssueDate(null);
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_PLACE_OF_ISSUE)) {
									addnInfo.setVisaDocPlaceOfIssue("");
									continue;
								} else if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_APPLICABLE_COUNTRY)) {
									addnInfo.setVisaApplicableCountry("");
									continue;
								}
							}
						}
					} else if (obj instanceof WSPaxConfigDTO) {
						WSPaxConfigDTO wsConfig = (WSPaxConfigDTO) obj;
						if (!wsConfig.isWsVisibility() && wsConfig.getPaxCatCode().equals(rPax.getPaxCategory())
								&& wsConfig.getPaxTypeCode().equals(rPax.getPaxType())) {

							if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE)) {
								rPax.setTitle("");
								continue;
							} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_FNAME)) {
								rPax.setFirstName("");
								continue;
							} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_LNAME)) {
								rPax.setLastName("");
								continue;
							} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONALITY)) {
								rPax.setNationalityCode(0);
								continue;
							} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_DOB)) {
								rPax.setDateOfBirth(null);
								continue;
							} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVELWITH)) {
								rPax.setIndexId(0);
								continue;
							}

							ReservationPaxAdditionalInfo addnInfo = rPax.getPaxAdditionalInfo();
							if (addnInfo != null) {
								if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
									addnInfo.setPassportNo("");
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_EXPIRY)) {
									addnInfo.setPassportExpiry(null);
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_ISSUED_CNTRY)) {
									addnInfo.setPassportIssuedCntry("");
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PLACE_OF_BIRTH)) {
									addnInfo.setPlaceOfBirth("");
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVEL_DOC_TYPE)) {
									addnInfo.setTravelDocumentType("");
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_NUMBER)) {
									addnInfo.setVisaDocNumber("");
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_ISSUE_DATE)) {
									addnInfo.setVisaDocIssueDate(null);
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_DOC_PLACE_OF_ISSUE)) {
									addnInfo.setVisaDocPlaceOfIssue("");
									continue;
								} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_VISA_APPLICABLE_COUNTRY)) {
									addnInfo.setVisaApplicableCountry("");
									continue;
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Update the reservation contact info
	 * 
	 * @param contactInfo
	 * @param reservationContactInfo
	 * @throws ModuleException
	 */
	public static void updateReservationContactInfo(ContactInfo contactInfo, ReservationContactInfo reservationContactInfo)
			throws ModuleException {
		Address address = contactInfo.getAddress();
		String streetAddress1 = address.getAddressLine().size() > 0 ? address.getAddressLine().get(0) : "";
		String streetAddress2 = address.getAddressLine().size() > 1 ? address.getAddressLine().get(1) : "";
		Country country = address.getCountry();
		PersonName personName = contactInfo.getPersonName();
		Telephone fax = contactInfo.getFax();
		Telephone mobile = contactInfo.getMobile();
		Telephone telephone = contactInfo.getTelephone();
		String prefLang = (contactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : contactInfo
				.getPreferredLanguage();
		String nationalityIsoCode = contactInfo.getNationalityCode();

		reservationContactInfo.setTitle(BeanUtils.nullHandler(personName.getTitle()));
		reservationContactInfo.setFirstName(BeanUtils.nullHandler(personName.getFirstName()));
		reservationContactInfo.setLastName(BeanUtils.nullHandler(personName.getSurName()));
		reservationContactInfo.setCity(BeanUtils.nullHandler(address.getCityName()));
		reservationContactInfo.setCountryCode(BeanUtils.nullHandler(country.getCountryCode()));
		reservationContactInfo.setEmail(BeanUtils.nullHandler(contactInfo.getEmail()));
		reservationContactInfo.setMobileNo(BeanUtils.nullHandler(mobile.getAreaCode())
				+ BeanUtils.nullHandler(mobile.getPhoneNumber()));
		reservationContactInfo.setFax(BeanUtils.nullHandler(fax.getAreaCode()) + BeanUtils.nullHandler(fax.getPhoneNumber()));
		reservationContactInfo.setPhoneNo(BeanUtils.nullHandler(telephone.getAreaCode())
				+ BeanUtils.nullHandler(telephone.getPhoneNumber()));
		reservationContactInfo.setStreetAddress1(streetAddress1);
		reservationContactInfo.setStreetAddress2(streetAddress2);
		reservationContactInfo.setZipCode(contactInfo.getZipCode());
		reservationContactInfo.setPreferredLanguage(prefLang);

		if (nationalityIsoCode != null) {
			Integer nationalityCode = LCCUtils.getNationalityCode(nationalityIsoCode);
			if (nationalityCode != null) {
				reservationContactInfo.setNationalityCode(nationalityCode.toString());
			}
		}

		// Set Emergency contact
		PersonName emgnPerson = contactInfo.getEmgnPersonName();
		String emgnTitle = (emgnPerson != null) ? BeanUtils.nullHandler(emgnPerson.getTitle()) : "";
		String emgnFName = (emgnPerson != null) ? BeanUtils.nullHandler(emgnPerson.getFirstName()) : "";
		String emgnLName = (emgnPerson != null) ? BeanUtils.nullHandler(emgnPerson.getSurName()) : "";

		reservationContactInfo.setEmgnTitle(emgnTitle);
		reservationContactInfo.setEmgnFirstName(emgnFName);
		reservationContactInfo.setEmgnLastName(emgnLName);

		Telephone emgnPhone = contactInfo.getEmgnTelephone();
		String emgnPhoneStr = "";
		if (emgnPhone != null) {
			emgnPhoneStr = BeanUtils.nullHandler(emgnPhone.getAreaCode()) + BeanUtils.nullHandler(emgnPhone.getPhoneNumber());
		}
		
		reservationContactInfo.setTaxRegNo(BeanUtils.nullHandler(contactInfo.getTaxRegNo()));
		reservationContactInfo.setState(BeanUtils.nullHandler(address.getState()));

		reservationContactInfo.setEmgnPhoneNo(emgnPhoneStr);
		reservationContactInfo.setEmgnEmail(BeanUtils.nullHandler(contactInfo.getEmgnEmail()));
		reservationContactInfo.setSendPromoEmail(contactInfo.isSendPromoEmail());
	}

	public static void updatePaxInfo(TravelerInfo travelerInfo, Set<ReservationPax> reservationPaxs) throws ModuleException {

		Map<AirTraveler, ReservationPax> map = new HashMap<AirTraveler, ReservationPax>();
		Map<Integer, ReservationPax> mapAdultInfant = new HashMap<Integer, ReservationPax>();
		for (AirTraveler airTraveler : travelerInfo.getAirTraveler()) {
			for (ReservationPax reservationPax : reservationPaxs) {
				if (PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber()).compareTo(reservationPax.getPaxSequence()) == 0) {
					map.put(airTraveler, reservationPax);
					if (ReservationApiUtils.isInfantType(reservationPax)) {
						int adultSeq = PaxTypeUtils.getParentSeq(airTraveler.getTravelerRefNumber());
						mapAdultInfant.put(adultSeq, reservationPax);
					}
					break;
				}
			}
		}

		// Update ReservationPax with new info from AirTraveler
		for (Entry<AirTraveler, ReservationPax> entry : map.entrySet()) {
			AirTraveler airTraveler = entry.getKey();
			ReservationPax reservationPax = entry.getValue();

			PersonName personName = airTraveler.getPersonName();
			reservationPax.setTitle(BeanUtils.nullHandler(personName.getTitle()));
			reservationPax.setFirstName(BeanUtils.nullHandler(personName.getFirstName()));
			reservationPax.setLastName(BeanUtils.nullHandler(personName.getSurName()));

			Date dateOfBirth = airTraveler.getBirthDate() == null ? null : XMLDataTypeUtil.getDate(airTraveler.getBirthDate());
			reservationPax.setDateOfBirth(dateOfBirth);

			String nationalityIsoCode = airTraveler.getNationalityCode();
			reservationPax.setNationalityCode(AAUtils.getNationalityCode(nationalityIsoCode));

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber());
				if (reservationPax.getInfants() != null) {
					reservationPax.getInfants().clear(); // In case we have unwanted infants attached from before
				}

				ReservationPax attachedInfantPax = mapAdultInfant.get(paxSeq);

				if (attachedInfantPax != null) {
					attachedInfantPax.setIndexId(reservationPax.getPaxSequence());
				}
			}

			// Set additional info
			ReservationPaxAdditionalInfo addnInfo = reservationPax.getPaxAdditionalInfo();
			addnInfo.setFfid(airTraveler.getFfid());
			if (airTraveler.getFormOfIdentification() != null) {
				FormOfIdentification foid = airTraveler.getFormOfIdentification();
				addnInfo.setPassportNo(foid.getFoidNumber());
				addnInfo.setPassportExpiry((foid.getPsptExpiry() != null)
						? foid.getPsptExpiry().toGregorianCalendar().getTime()
						: null);
				addnInfo.setPassportIssuedCntry(foid.getPsptIssuedCntry());
				addnInfo.setPlaceOfBirth(foid.getPlaceOfBirth());
				addnInfo.setTravelDocumentType(foid.getTravelDocumentType());
				addnInfo.setVisaDocNumber(foid.getVisaDocNumber());
				addnInfo.setVisaDocIssueDate((foid.getVisaDocIssueDate() != null) ? foid.getVisaDocIssueDate()
						.toGregorianCalendar().getTime() : null);
				addnInfo.setVisaDocPlaceOfIssue(foid.getVisaDocPlaceOfIssue());
				addnInfo.setVisaApplicableCountry(foid.getVisaApplicableCountry());
				reservationPax.setPaxAdditionalInfo(addnInfo);
			}
		}
	}

	/**
	 * Filter to keep only visible contact info fields of the carrier
	 */
	public static void enforceCarrierConfigForContactInfo(ReservationContactInfo contactInfo, String appIndicator) {

		List contactConfigList = AAServicesModuleUtils.getGlobalConfig().getContactDetailsConfig(appIndicator);

		for (Object obj : contactConfigList) {
			if (obj instanceof XBEContactConfigDTO) {
				XBEContactConfigDTO xbeConfig = (XBEContactConfigDTO) obj;

				if (!xbeConfig.isXbeVisibility()) {
					if (xbeConfig.getGroupId() == BaseContactConfigDTO.GROUP_NORMAL_CONTACT) {
						if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_TITLE)) {
							contactInfo.setTitle("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_FIRST_NAME)) {
							contactInfo.setFirstName("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_LAST_NAME)) {
							contactInfo.setLastName("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_NATIONALITY)) {
							contactInfo.setNationalityCode("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_ADDRESS)) {
							setAddressToContact(contactInfo);
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_CITY)) {
							contactInfo.setCity("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_ZIP_CODE)) {
							contactInfo.setZipCode("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_COUNTRY)) {
							contactInfo.setCountryCode("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_MOBILE)) {
							contactInfo.setMobileNo("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_PHONE)) {
							contactInfo.setPhoneNo("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_FAX)) {
							contactInfo.setFax("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_EMAIL)) {
							contactInfo.setEmail("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_PREFERRED_LANG)) {
							contactInfo.setPreferredLanguage("");
						}
					} else if (xbeConfig.getGroupId() == BaseContactConfigDTO.GROUP_EMERGENCY_CONTACT) {
						if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_TITLE)) {
							contactInfo.setEmgnTitle("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_FIRST_NAME)) {
							contactInfo.setEmgnFirstName("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_LAST_NAME)) {
							contactInfo.setEmgnLastName("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_PHONE)) {
							contactInfo.setEmgnPhoneNo("");
						} else if (xbeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_EMAIL)) {
							contactInfo.setEmgnEmail("");
						}
					}
				}
			} else if (obj instanceof IBEContactConfigDTO) {
				IBEContactConfigDTO ibeConfig = (IBEContactConfigDTO) obj;

				if (!ibeConfig.isIbeVisibility()) {
					if (ibeConfig.getGroupId() == BaseContactConfigDTO.GROUP_NORMAL_CONTACT) {
						if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_TITLE)) {
							contactInfo.setTitle("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_FIRST_NAME)) {
							contactInfo.setFirstName("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_LAST_NAME)) {
							contactInfo.setLastName("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_NATIONALITY)) {
							contactInfo.setNationalityCode("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_ADDRESS)) {
							setAddressToContact(contactInfo);
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_CITY)) {
							contactInfo.setCity("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_ZIP_CODE)) {
							contactInfo.setZipCode("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_COUNTRY)) {
							contactInfo.setCountryCode("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_MOBILE)) {
							contactInfo.setMobileNo("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_PHONE)) {
							contactInfo.setPhoneNo("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_FAX)) {
							contactInfo.setFax("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_EMAIL)) {
							contactInfo.setEmail("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_PREFERRED_LANG)) {
							contactInfo.setPreferredLanguage("");
						}
					} else if (ibeConfig.getGroupId() == BaseContactConfigDTO.GROUP_EMERGENCY_CONTACT) {
						if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_TITLE)) {
							contactInfo.setEmgnTitle("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_FIRST_NAME)) {
							contactInfo.setEmgnFirstName("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_LAST_NAME)) {
							contactInfo.setEmgnLastName("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_PHONE)) {
							contactInfo.setEmgnPhoneNo("");
						} else if (ibeConfig.getFieldName().equals(BaseContactConfigDTO.FIELD_EMAIL)) {
							contactInfo.setEmgnEmail("");
						}
					}
				}
			}
		}
	}

	private static void setAddressToContact(ReservationContactInfo contactInfo) {
		if (ArrayUtils
				.contains(StringUtils.split(AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries(), ","),
						contactInfo.getCountryCode())) {
			contactInfo.setStreetAddress1(StringUtils.defaultString(contactInfo.getStreetAddress1()));
			contactInfo.setStreetAddress2(StringUtils.defaultString(contactInfo.getStreetAddress2()));
		} else {
			contactInfo.setStreetAddress1("");
			contactInfo.setStreetAddress2("");
		}
	}

	public static SystemType transformSystem(SYSTEM system) {
		SystemType systemType = SystemType.NONE;

		if (SYSTEM.AA == system) {
			systemType = SystemType.AA;
		} else if (SYSTEM.INT == system) {
			systemType = SystemType.INT;
		} else if (SYSTEM.ALL == system) {
			systemType = SystemType.ALL;
		} else if (SYSTEM.COND == system) {
			systemType = SystemType.ALL;
		}

		return systemType;
	}

	public static EXTERNAL_CHARGES getExternalChargeType(ExternalChargeType externalChargeType) {
		if (externalChargeType == ExternalChargeType.CREDIT_CARD) {
			return EXTERNAL_CHARGES.CREDIT_CARD;
		} else if (externalChargeType == ExternalChargeType.HANDLING_CHARGE) {
			return EXTERNAL_CHARGES.HANDLING_CHARGE;
		} else if (externalChargeType == ExternalChargeType.SEAT_MAP) {
			return EXTERNAL_CHARGES.SEAT_MAP;
		} else if (externalChargeType == ExternalChargeType.MEAL) {
			return EXTERNAL_CHARGES.MEAL;
		} else if (externalChargeType == ExternalChargeType.BAGGAGE) {
			return EXTERNAL_CHARGES.BAGGAGE;
		} else if (externalChargeType == ExternalChargeType.INSURANCE) {
			return EXTERNAL_CHARGES.INSURANCE;
		} else if (externalChargeType == ExternalChargeType.INFLIGHT_SERVICES) {
			return EXTERNAL_CHARGES.INFLIGHT_SERVICES;
		} else if (externalChargeType == ExternalChargeType.FLEXI_CHARGES) {
			return EXTERNAL_CHARGES.FLEXI_CHARGES;
		} else if (externalChargeType == ExternalChargeType.AIRPORT_SERVICE) {
			return EXTERNAL_CHARGES.AIRPORT_SERVICE;
		} else if (externalChargeType == ExternalChargeType.PROMOTION_REWARD) {
			return EXTERNAL_CHARGES.PROMOTION_REWARD;
		} else if (externalChargeType == ExternalChargeType.ADDITONAL_SEAT_CHARGE) {
			return EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE;
		} else if (externalChargeType == ExternalChargeType.JN_ANCI) {
			return EXTERNAL_CHARGES.JN_ANCI;
		} else if (externalChargeType == ExternalChargeType.JN_OTHER) {
			return EXTERNAL_CHARGES.JN_OTHER;
		} else if (externalChargeType == ExternalChargeType.ANCI_PENALTY) {
			return EXTERNAL_CHARGES.ANCI_PENALTY;
		} else if (externalChargeType == ExternalChargeType.BSP_FEE) {
			return EXTERNAL_CHARGES.BSP_FEE;
		}

		return null;
	}

	public static ExternalChargeType getExternalChargeType(EXTERNAL_CHARGES lccExternalCharges) {
		if (lccExternalCharges == EXTERNAL_CHARGES.CREDIT_CARD) {
			return ExternalChargeType.CREDIT_CARD;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.HANDLING_CHARGE) {
			return ExternalChargeType.HANDLING_CHARGE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.SEAT_MAP) {
			return ExternalChargeType.SEAT_MAP;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.MEAL) {
			return ExternalChargeType.MEAL;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.BAGGAGE) {
			return ExternalChargeType.BAGGAGE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.INSURANCE) {
			return ExternalChargeType.INSURANCE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
			return ExternalChargeType.INFLIGHT_SERVICES;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.FLEXI_CHARGES) {
			return ExternalChargeType.FLEXI_CHARGES;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
			return ExternalChargeType.AIRPORT_SERVICE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.PROMOTION_REWARD) {
			return ExternalChargeType.PROMOTION_REWARD;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE) {
			return ExternalChargeType.ADDITONAL_SEAT_CHARGE;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.JN_ANCI) {
			return ExternalChargeType.JN_ANCI;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.JN_OTHER) {
			return ExternalChargeType.JN_OTHER;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.ANCI_PENALTY) {
			return ExternalChargeType.ANCI_PENALTY;
		} else if (lccExternalCharges == EXTERNAL_CHARGES.BSP_FEE) {
			return ExternalChargeType.BSP_FEE;
		} 

		return null;
	}

	public static String getAppIndicator(String appEngine) {
		String appIndicator;

		if (ApplicationEngine.IBE.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_IBE.toString();
		} else if (ApplicationEngine.ANDROID.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_ANDROID.toString();
		} else if (ApplicationEngine.IOS.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_IOS.toString();
		} else if (ApplicationEngine.XBE.toString().equals(appEngine)) {
			appIndicator = AppIndicatorEnum.APP_XBE.toString();
		} else {
			appIndicator = AppIndicatorEnum.APP_WS.toString();
		}

		return appIndicator;
	}

	static CreditStatus convertAACreditStatusToLCCCreditStatus(status aaStatus) {
		if (aaStatus == status.AVAILABLE) {
			return CreditStatus.AVAILABLE;
		} else {
			return CreditStatus.EXPIRED;
		}
	}

	// TODO Review
	// TODO move to Common
	public static String getCarrierCode(String flightNumber) {
		if (flightNumber != null && flightNumber.length() > 2) {
			flightNumber = flightNumber.substring(0, 2);
		}
		return flightNumber;
	}

	public static boolean isMarketingCarrier(String marketingCarrier) {
		return AppSysParamsUtil.getDefaultCarrierCode().equals(marketingCarrier);
	}

	public static List<PaymentDetails> updatePaxTransactionIds(List<PaymentDetails> originalPaymentsList, Integer pnrPaxId) {

		for (PaymentDetails paymentDetail : originalPaymentsList) {
			if (StringUtils.isEmpty(paymentDetail.getOriginalPaymentUID())) {
				// we have a own booking loaded through lcc
				continue;
			} else {
				ReservationTnx paymentTnx = AAServicesModuleUtils.getPassengerBD().getLccTransaction(pnrPaxId.toString(),
						paymentDetail.getOriginalPaymentUID());
				paymentDetail.setOriginalPaymentTnxRefNumber(paymentTnx.getTnxId().toString());

			}

		}

		return originalPaymentsList;

	}

	public static List<ExternalChargeType> convertToExternalChargeTypes(Set<String> bundledFareFreeServiceNames) {
		List<ExternalChargeType> externalChargeTypes = new ArrayList<ExternalChargeType>();

		for (String freeServiceName : bundledFareFreeServiceNames) {
			if (freeServiceName.equals(EXTERNAL_CHARGES.SEAT_MAP.toString())) {
				externalChargeTypes.add(ExternalChargeType.SEAT_MAP);
			} else if (freeServiceName.equals(EXTERNAL_CHARGES.MEAL.toString())) {
				externalChargeTypes.add(ExternalChargeType.MEAL);
			} else if (freeServiceName.equals(EXTERNAL_CHARGES.BAGGAGE.toString())) {
				externalChargeTypes.add(ExternalChargeType.BAGGAGE);
			} else if (freeServiceName.equals(EXTERNAL_CHARGES.AIRPORT_SERVICE.toString())) {
				externalChargeTypes.add(ExternalChargeType.AIRPORT_SERVICE);
			} else if (freeServiceName.equals(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())) {
				externalChargeTypes.add(ExternalChargeType.FLEXI_CHARGES);
			}
		}

		return externalChargeTypes;
	}
	
	public static boolean isEnableServiceIBE(AncillaryType anciType, Date DepDateZulu, boolean isInEditMode) {
		long diffMils = 3600000;

		if(DepDateZulu == null)
			return true;
		
		long lngBufferTime = 0;
		Date currentTime = DateUtil.getCurrentZuluDateTime();

		if (anciType == AncillaryType.SEAT_MAP) {
			diffMils = AppSysParamsUtil.getIBESeatmapStopCutoverInMillis();

		} else if (anciType == AncillaryType.MEALS) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());

		} else if (anciType == AncillaryType.BAGGAGE) {
			diffMils = BaggageTimeWatcher.getBaggageCutOverTimeForIBEInMillis(isInEditMode);

		} else if (anciType == AncillaryType.SSR) {
			diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();

		} else if (anciType == AncillaryType.INSURANCE) {
			diffMils = lngBufferTime;
		}

		if (isInEditMode && ((DepDateZulu.getTime() - lngBufferTime) < currentTime.getTime())) {
			return false;
		} else if (DepDateZulu.getTime() - diffMils < currentTime.getTime()) {
			return false;
		} else {
			return true;
		}
	}

	public static LoyaltyPaymentInfo extractLoyaltyPaymentInfo(List<TravelerFulfillment> carrierFulfillment) {
		LoyaltyPaymentInfo loyaltyPaymentInfo = null;
		BigDecimal totalLMSPayAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (TravelerFulfillment travelerFulfillment : carrierFulfillment) {
			for (PaymentDetails paymentDetail : travelerFulfillment.getPaymentDetails()) {
				LMSPaymentInfo lmsPaymentInfo = paymentDetail.getLmsPaymentInfo();
				if (lmsPaymentInfo != null) {
					if (loyaltyPaymentInfo == null) {
						loyaltyPaymentInfo = new LoyaltyPaymentInfo();
						loyaltyPaymentInfo.setMemberAccountId(lmsPaymentInfo.getMemberAccountId());

						String[] rewardIds = new String[lmsPaymentInfo.getRewardIds().size()];
						int i = 0;
						for (String e : lmsPaymentInfo.getRewardIds()) {
							rewardIds[i++] = e;
						}

						loyaltyPaymentInfo.setLoyaltyRewardIds(rewardIds);

						Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts = new HashMap<Integer, Map<String, BigDecimal>>();

						for (PaxProductAmounts paxProductAmounts : lmsPaymentInfo.getPaxProductRedeemBreakdown()) {
							paxWiseProductAmounts.put(paxProductAmounts.getKey(),
									TransformerUtil.transformStringDecimalMap(paxProductAmounts.getValue()));
						}
						loyaltyPaymentInfo.setPaxProductPaymentBreakdown(paxWiseProductAmounts);
					}
					totalLMSPayAmount = AccelAeroCalculator.add(totalLMSPayAmount, lmsPaymentInfo.getTotalPaymentAmount());
				}
			}
		}

		if (loyaltyPaymentInfo != null) {
			loyaltyPaymentInfo.setTotalPayment(totalLMSPayAmount);
			return loyaltyPaymentInfo;
		} else {
			return null;
		}
	}
	
}