package com.isa.thinair.aaservices.core.bl.onhold;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.OhdParam;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAOnholdAvailabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAOnholdAvailabilityRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.dto.ResOnholdValidationDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author rumesh
 * 
 */

public class AAOnholdAvailabilityBL {
	private static final Log log = LogFactory.getLog(AAOnholdAvailabilityBL.class);

	private AAOnholdAvailabilityRQ aaOnholdAvailabilityRQ;

	public void setRequest(AAOnholdAvailabilityRQ aaOnholdAvailabilityRQ) {
		this.aaOnholdAvailabilityRQ = aaOnholdAvailabilityRQ;
	}

	public AAOnholdAvailabilityRS execute() {
		AAOnholdAvailabilityRS aaOnholdAvailabilityRS = new AAOnholdAvailabilityRS();
		aaOnholdAvailabilityRS.setResponseAttributes(new AAResponseAttributes());

		try {
			boolean allowOnhold = AAServicesModuleUtils.getReservationQueryBD().isReservationOnholdable(
					populateResOnholdValidationDTO(aaOnholdAvailabilityRQ.getOhdParam()));
			aaOnholdAvailabilityRS.getResponseAttributes().setSuccess(new AASuccess());
			aaOnholdAvailabilityRS.setOnholdAvailability(allowOnhold);
		} catch (ModuleException e) {
			log.error("Onhold availability check failed in AAServices", e);
			AAExceptionUtil.addAAErrrors(aaOnholdAvailabilityRS.getResponseAttributes().getErrors(), e);
		}

		return aaOnholdAvailabilityRS;
	}

	private ResOnholdValidationDTO populateResOnholdValidationDTO(OhdParam ohdParam) {
		ResOnholdValidationDTO resOnholdValidationDTO = new ResOnholdValidationDTO();
		resOnholdValidationDTO.setIpAddress(ohdParam.getIpAddress());
		resOnholdValidationDTO.setValidationType(ohdParam.getValidationType());
		resOnholdValidationDTO.setContactEmail(ohdParam.getContactEmail());
		resOnholdValidationDTO.setFlightSegIdList(ohdParam.getFlightSegIdList());
		resOnholdValidationDTO.setDepatureDateList(ohdParam.getDepartureDateList());
		resOnholdValidationDTO.setPaxList(populateResPaxList(ohdParam.getPaxList()));
		resOnholdValidationDTO.setSalesChannel(ohdParam.getSalesChannel());
		return resOnholdValidationDTO;
	}

	private List<ReservationPax> populateResPaxList(List<PersonName> personNames) {
		List<ReservationPax> paxList = new ArrayList<ReservationPax>();
		for (PersonName p : personNames) {
			ReservationPax rp = new ReservationPax();
			rp.setTitle(p.getTitle());
			rp.setFirstName(p.getFirstName());
			rp.setLastName(p.getSurName());
			paxList.add(rp);
		}
		return paxList;
	}
}
