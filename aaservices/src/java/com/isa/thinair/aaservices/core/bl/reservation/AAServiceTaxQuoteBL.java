package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AvailPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerBooleanMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModifyPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxTypeWiseServiceTaxesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxWiseExternalChargesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxWiseServiceTaxesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ServiceTax;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAServiceTaxQuoteForTktRevRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAServiceTaxQuoteForTktRevRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.SimplifiedChargeDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedFlightSegmentDTO;
import com.isa.thinair.airpricing.api.dto.SimplifiedPaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.utils.CommonUtil;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.FareConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class AAServiceTaxQuoteBL {
	
	private static Log log = LogFactory.getLog(AACreateReservationBL.class);

	private final AAServiceTaxQuoteForTktRevRQ aaServiceTaxQuoteForTktRevRQ;

	private TransactionalReservationProcess tnxResProcess;
	
	private boolean isCalculateOnlyForExternalCharges;

	public AAServiceTaxQuoteBL(AAServiceTaxQuoteForTktRevRQ aaServiceTaxQuoteForTktRevRQ, AASessionManager aaSessionManager) {
		this.aaServiceTaxQuoteForTktRevRQ = aaServiceTaxQuoteForTktRevRQ;
		String transactionId = PlatformUtiltiies.nullHandler((aaServiceTaxQuoteForTktRevRQ != null) ? aaServiceTaxQuoteForTktRevRQ.getHeaderInfo()
				.getTransactionIdentifier() : null);

		this.isCalculateOnlyForExternalCharges = aaServiceTaxQuoteForTktRevRQ != null
				? aaServiceTaxQuoteForTktRevRQ.isCalculateOnlyForExternalCharges() : false;
		
		if (!this.isCalculateOnlyForExternalCharges && transactionId.length() > 0 && aaSessionManager != null) {
			if (log.isDebugEnabled()) {
				log.debug("Current request session id " + aaServiceTaxQuoteForTktRevRQ.getAaPos().getUserSessionId());
			}
			this.tnxResProcess = aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AAServiceTaxQuoteForTktRevRS execute() {
		AAServiceTaxQuoteForTktRevRS aaServiceTaxQuoteForTktRevRS = new AAServiceTaxQuoteForTktRevRS();
		aaServiceTaxQuoteForTktRevRS.setResponseAttributes(new AAResponseAttributes());

		try {

			if (!isCalculateOnlyForExternalCharges)
				checkConstraints();

			if (log.isDebugEnabled()) {
				log.debug("Going to service tax quote in  " + AppSysParamsUtil.getDefaultCarrierCode()
						+ "for txnId : " + aaServiceTaxQuoteForTktRevRQ.getHeaderInfo().getTransactionIdentifier());
			}
			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTicketingRevenueRQ = new ServiceTaxQuoteForTicketingRevenueRQ();

			Map<Integer, Collection<OndFareDTO>> jouneyONDWiseFareONDs = new HashMap<Integer, Collection<OndFareDTO>>();
			if(!isCalculateOnlyForExternalCharges){
				Collection<OndFareDTO> collFares = tnxResProcess.getOndFareDTOListForReservation(
						aaServiceTaxQuoteForTktRevRQ.getSelectedFareTypes(), false);				
				
				if(collFares!=null && !collFares.isEmpty()){
					for (OndFareDTO ondFareDTO : collFares) {
						if (jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()) == null) {
							jouneyONDWiseFareONDs.put(ondFareDTO.getOndSequence(), new ArrayList<OndFareDTO>());
						}
						jouneyONDWiseFareONDs.get(ondFareDTO.getOndSequence()).add(ondFareDTO);
					}
				}
			}
			List<PassengerTypeQuantityTO> paxQtyList = null;
			if (aaServiceTaxQuoteForTktRevRQ.getPassengerTypeQuantity() != null && !aaServiceTaxQuoteForTktRevRQ.getPassengerTypeQuantity().isEmpty()) {
				paxQtyList = new ArrayList<PassengerTypeQuantityTO>();
				for (PassengerTypeQuantity paxTypeQty : aaServiceTaxQuoteForTktRevRQ.getPassengerTypeQuantity()) {
					PassengerTypeQuantityTO paxTypeQtyTO = new PassengerTypeQuantityTO();
					paxTypeQtyTO.setPassengerType(PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(paxTypeQty.getPassengerType()));
					paxTypeQtyTO.setQuantity(paxTypeQty.getQuantity());
					paxQtyList.add(paxTypeQtyTO);
				}
			}
			String appIndicator = null;
			if (aaServiceTaxQuoteForTktRevRQ.getAvailPreferences() != null
					&& aaServiceTaxQuoteForTktRevRQ.getAvailPreferences().getAppIndicator() != null) {
				appIndicator = aaServiceTaxQuoteForTktRevRQ.getAvailPreferences().getAppIndicator().toString();
			}
			
			if (paxQtyList != null) {
				IPaxCountAssembler paxAssm = new PaxCountAssembler(paxQtyList);
				FareTypeTO fareTypeTO = FareConvertUtil.getFareTypeTO(
						new ArrayList<Collection<OndFareDTO>>(jouneyONDWiseFareONDs.values()), paxAssm,
						aaServiceTaxQuoteForTktRevRQ.getAgentCode(), CommonUtil.getAppIndicator(appIndicator),
						populateBaseAvailRQ(aaServiceTaxQuoteForTktRevRQ.getAvailPreferences()));

				Map<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges = new HashMap<String, List<SimplifiedChargeDTO>>();
				List<SimplifiedChargeDTO> adultChargeDTOs = new ArrayList<SimplifiedChargeDTO>();
				List<SimplifiedChargeDTO> childChargeDTOs = new ArrayList<SimplifiedChargeDTO>();
				List<SimplifiedChargeDTO> infantChargeDTOs = new ArrayList<SimplifiedChargeDTO>();
				if (fareTypeTO != null) {
					for (PerPaxPriceInfoTO perPaxPriceInfo : fareTypeTO.getPerPaxPriceInfo()) {
						if (paxTypeWiseCharges.get(perPaxPriceInfo.getPassengerType()) == null) {
							for (BaseFareTO baseFareTO : perPaxPriceInfo.getPassengerPrice().getBaseFares()) {
								SimplifiedChargeDTO fareDTO = new SimplifiedChargeDTO();
								fareDTO.setAmount(baseFareTO.getAmount());
								fareDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.FAR);
								if (baseFareTO.getCarrierCode() != null && baseFareTO.getCarrierCode().size() > 0) {
									fareDTO.setCarrierCode(baseFareTO.getCarrierCode().get(0));
								}
								fareDTO.setLogicalCCCode(baseFareTO.getLogicalCCType());
								fareDTO.setOndSequence(baseFareTO.getOndSequence());
								fareDTO.setSegmentCode(baseFareTO.getSegmentCode());

								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
									adultChargeDTOs.add(fareDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
									childChargeDTOs.add(fareDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
									infantChargeDTOs.add(fareDTO);
								}
							}

							for (TaxTO taxTO : perPaxPriceInfo.getPassengerPrice().getTaxes()) {
								SimplifiedChargeDTO taxDTO = new SimplifiedChargeDTO();
								taxDTO.setAmount(taxTO.getAmount());
								taxDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.TAX);
								taxDTO.setCarrierCode(taxTO.getCarrierCode());
								taxDTO.setChargeCode(taxTO.getTaxCode());
								taxDTO.setOndSequence(taxTO.getOndSequence());
								taxDTO.setSegmentCode(taxTO.getSegmentCode());

								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
									adultChargeDTOs.add(taxDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
									childChargeDTOs.add(taxDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
									infantChargeDTOs.add(taxDTO);
								}
							}

							for (SurchargeTO surchargeTO : perPaxPriceInfo.getPassengerPrice().getSurcharges()) {
								SimplifiedChargeDTO surchargeDTO = new SimplifiedChargeDTO();
								surchargeDTO.setAmount(surchargeTO.getAmount());
								surchargeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
								surchargeDTO.setCarrierCode(surchargeTO.getCarrierCode());
								surchargeDTO.setChargeCode(surchargeTO.getSurchargeCode());
								surchargeDTO.setOndSequence(surchargeTO.getOndSequence());
								surchargeDTO.setSegmentCode(surchargeTO.getSegmentCode());

								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
									adultChargeDTOs.add(surchargeDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
									childChargeDTOs.add(surchargeDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
									infantChargeDTOs.add(surchargeDTO);
								}
							}

							for (FeeTO feeTO : perPaxPriceInfo.getPassengerPrice().getFees()) {
								SimplifiedChargeDTO feeDTO = new SimplifiedChargeDTO();
								feeDTO.setAmount(feeTO.getAmount());
								feeDTO.setChargeGroupCode(ReservationInternalConstants.ChargeGroup.SUR);
								feeDTO.setCarrierCode(feeTO.getCarrierCode());
								feeDTO.setChargeCode(feeTO.getFeeCode());
								feeDTO.setOndSequence(feeTO.getOndSequence());
								feeDTO.setSegmentCode(feeTO.getSegmentCode());

								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.ADULT)) {
									adultChargeDTOs.add(feeDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.CHILD)) {
									childChargeDTOs.add(feeDTO);
								}
								if (perPaxPriceInfo.getPassengerType().equals(PaxTypeTO.INFANT)) {
									infantChargeDTOs.add(feeDTO);
								}
							}
							if (!adultChargeDTOs.isEmpty()) {
								paxTypeWiseCharges.put(PaxTypeTO.ADULT, adultChargeDTOs);
							}
							if (!childChargeDTOs.isEmpty()) {
								paxTypeWiseCharges.put(PaxTypeTO.CHILD, childChargeDTOs);
							}
							if (!infantChargeDTOs.isEmpty()) {
								paxTypeWiseCharges.put(PaxTypeTO.INFANT, infantChargeDTOs);
							}
						}
					}
					serviceTaxQuoteForTicketingRevenueRQ.setPaxTypeWiseCharges(paxTypeWiseCharges);
				}

			}

			
			if (aaServiceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges() != null
					&& !aaServiceTaxQuoteForTktRevRQ.getPaxWiseExternalCharges().isEmpty()) {
				Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseCharges = new HashMap<SimplifiedPaxDTO, List<SimplifiedChargeDTO>>();
				for (PaxWiseExternalChargesMap paxWiseExternalChargeEntry : aaServiceTaxQuoteForTktRevRQ
						.getPaxWiseExternalCharges()) {
					if (paxWiseExternalChargeEntry.getValue() != null && !paxWiseExternalChargeEntry.getValue().isEmpty()) {
						List<SimplifiedChargeDTO> chargesList = new ArrayList<SimplifiedChargeDTO>();
						SimplifiedPaxDTO paxDTO = new SimplifiedPaxDTO();
						paxDTO.setPaxSequence(paxWiseExternalChargeEntry.getKey().getPaxSequence());
						paxDTO.setPaxType(paxWiseExternalChargeEntry.getKey().getPaxType());
						for (com.isa.connectivity.profiles.maxico.v1.common.dto.SimplifiedChargeDTO externalChgDTO : paxWiseExternalChargeEntry
								.getValue()) {
							if (externalChgDTO.getAmount() != null && externalChgDTO.getAmount().doubleValue() > 0) {
								SimplifiedChargeDTO chargeDTO = new SimplifiedChargeDTO();
								chargeDTO.setAmount(externalChgDTO.getAmount());
								chargeDTO.setChargeGroupCode(externalChgDTO.getChargeGroupCode());
								chargeDTO.setCarrierCode(externalChgDTO.getCarrierCode());
								chargeDTO.setChargeCode(externalChgDTO.getChargeCode());
								chargeDTO.setFlightRefNumber(externalChgDTO.getFlightRefNumber());
								chargeDTO.setSegmentCode(externalChgDTO.getSegmentCode());
								chargeDTO.setSegmentSequence(externalChgDTO.getSegmentSequence());
								chargesList.add(chargeDTO);
							}
						}
						if (!chargesList.isEmpty()) {
							paxWiseCharges.put(paxDTO, chargesList);
						}
					}
				}
				serviceTaxQuoteForTicketingRevenueRQ.setPaxWiseExternalCharges(paxWiseCharges);
			}
			
			List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs = new ArrayList<SimplifiedFlightSegmentDTO>();
			for (FlightSegment flightSegmentTO : aaServiceTaxQuoteForTktRevRQ.getFlightSegments()) {
				SimplifiedFlightSegmentDTO simplifiedFlightSegmentDTO = new SimplifiedFlightSegmentDTO();
				simplifiedFlightSegmentDTO.setDepartureDateTimeZulu(flightSegmentTO.getDepatureDateTimeZulu());
				simplifiedFlightSegmentDTO.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
				simplifiedFlightSegmentDTO.setLogicalCabinClassCode(flightSegmentTO.getLogicalCabinClassCode());
				simplifiedFlightSegmentDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				simplifiedFlightSegmentDTO.setOndSequence(flightSegmentTO.getOndSequence());
				simplifiedFlightSegmentDTO.setSegmentCode(flightSegmentTO.getSegmentCode());
				simplifiedFlightSegmentDTO.setSegmentSequence(flightSegmentTO.getSegmentSequence());
				simplifiedFlightSegmentDTO.setReturnFlag("Y".equals(flightSegmentTO.getReturnFlag()) ? true : false);
				simplifiedFlightSegmentDTO.setOperatingAirline(flightSegmentTO.getOperatingAirline());
				simplifiedFlightSegmentDTOs.add(simplifiedFlightSegmentDTO);
			}
			serviceTaxQuoteForTicketingRevenueRQ.setSimplifiedFlightSegmentDTOs(simplifiedFlightSegmentDTOs);
			serviceTaxQuoteForTicketingRevenueRQ.setPaxState(aaServiceTaxQuoteForTktRevRQ.getPaxState());
			serviceTaxQuoteForTicketingRevenueRQ.setPaxCountryCode(aaServiceTaxQuoteForTktRevRQ.getPaxCountryCode());
			serviceTaxQuoteForTicketingRevenueRQ.setPaxTaxRegistered(aaServiceTaxQuoteForTktRevRQ.isPaxTaxRegistered());
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxQuoteForTktRevRS = AAServicesModuleUtils.getChargeBD()
					.quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTicketingRevenueRQ);
			
			composeAAServiceTaxQuoteForTktRevRS(aaServiceTaxQuoteForTktRevRS, serviceTaxQuoteForTktRevRS);			
			
		} catch (Exception e) {
			log.error("book(AAAirBookRQ) failed.", e);e.printStackTrace();
			AAExceptionUtil.addAAErrrors(aaServiceTaxQuoteForTktRevRS.getResponseAttributes().getErrors(), e);
		}
		return aaServiceTaxQuoteForTktRevRS;
	}
	
	private void composeAAServiceTaxQuoteForTktRevRS(AAServiceTaxQuoteForTktRevRS aaServiceTaxQuoteForTktRevRS,
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxQuoteForTktRevRS) {
		if (serviceTaxQuoteForTktRevRS != null) {
			aaServiceTaxQuoteForTktRevRS.setServiceTaxDepositeCountryCode(serviceTaxQuoteForTktRevRS.getServiceTaxDepositeCountryCode());
			aaServiceTaxQuoteForTktRevRS.setServiceTaxDepositeStateCode(serviceTaxQuoteForTktRevRS.getServiceTaxDepositeStateCode());
			
			if (serviceTaxQuoteForTktRevRS.getPaxTypeWiseServiceTaxes() != null
					&& !serviceTaxQuoteForTktRevRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
				List<PaxTypeWiseServiceTaxesMap> paxTypeWiseServiceTaxes = new ArrayList<PaxTypeWiseServiceTaxesMap>();
				for (Entry<String, List<ServiceTaxDTO>> paxTypeWiseServiceTaxDTOs : serviceTaxQuoteForTktRevRS
						.getPaxTypeWiseServiceTaxes().entrySet()) {
					List<ServiceTax> serviceTaxes = new ArrayList<ServiceTax>();
					for (ServiceTaxDTO serviceTaxDTO : paxTypeWiseServiceTaxDTOs.getValue()) {
						ServiceTax serviceTax = new ServiceTax();
						serviceTax.setAmount(serviceTaxDTO.getAmount());
						serviceTax.setCarrierCode(serviceTaxDTO.getCarrierCode());
						serviceTax.setChargeCode(serviceTaxDTO.getChargeCode());
						serviceTax.setChargeGroupCode(serviceTaxDTO.getChargeGroupCode());
						serviceTax.setFlightRefNumber(serviceTaxDTO.getFlightRefNumber());
						serviceTax.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
						serviceTax.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
						serviceTaxes.add(serviceTax);
					}
					PaxTypeWiseServiceTaxesMap paxTypeWiseServiceTaxesMap = new PaxTypeWiseServiceTaxesMap();
					paxTypeWiseServiceTaxesMap.setKey(paxTypeWiseServiceTaxDTOs.getKey());
					paxTypeWiseServiceTaxesMap.getValue().addAll(serviceTaxes);
					paxTypeWiseServiceTaxes.add(paxTypeWiseServiceTaxesMap);
				}
				aaServiceTaxQuoteForTktRevRS.getPaxTypeWiseServiceTaxes().addAll(paxTypeWiseServiceTaxes);
			}
			
			if (serviceTaxQuoteForTktRevRS.getPaxWiseServiceTaxes() != null
					&& !serviceTaxQuoteForTktRevRS.getPaxWiseServiceTaxes().isEmpty()) {
				List<PaxWiseServiceTaxesMap> paxWiseServiceTaxes = new ArrayList<PaxWiseServiceTaxesMap>();
				for (Entry<Integer, List<ServiceTaxDTO>> paxWiseServiceTaxDTOs : serviceTaxQuoteForTktRevRS
						.getPaxWiseServiceTaxes().entrySet()) {
					List<ServiceTax> serviceTaxes = new ArrayList<ServiceTax>();
					for (ServiceTaxDTO serviceTaxDTO : paxWiseServiceTaxDTOs.getValue()) {
						ServiceTax serviceTax = new ServiceTax();
						serviceTax.setAmount(serviceTaxDTO.getAmount());
						serviceTax.setCarrierCode(serviceTaxDTO.getCarrierCode());
						serviceTax.setChargeCode(serviceTaxDTO.getChargeCode());
						serviceTax.setChargeGroupCode(serviceTaxDTO.getChargeGroupCode());
						serviceTax.setChargeRateId(serviceTaxDTO.getChargeRateId());
						serviceTax.setFlightRefNumber(serviceTaxDTO.getFlightRefNumber());
						serviceTax.setNonTaxableAmount(serviceTaxDTO.getNonTaxableAmount());
						serviceTax.setTaxableAmount(serviceTaxDTO.getTaxableAmount());
						serviceTaxes.add(serviceTax);
					}
					PaxWiseServiceTaxesMap paxWiseServiceTaxesMap = new PaxWiseServiceTaxesMap();
					paxWiseServiceTaxesMap.setKey(paxWiseServiceTaxDTOs.getKey());
					paxWiseServiceTaxesMap.getValue().addAll(serviceTaxes);
					paxWiseServiceTaxes.add(paxWiseServiceTaxesMap);
				}
				aaServiceTaxQuoteForTktRevRS.getPaxWiseServiceTaxes().addAll(paxWiseServiceTaxes);
			}
			aaServiceTaxQuoteForTktRevRS.getResponseAttributes().setSuccess(new AASuccess());
		}		
	}

	
	static BaseAvailRQ populateBaseAvailRQ(AvailPreferences availPref) {
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();

		// populate AvailPreferencesTO
		AvailPreferencesTO availTO = baseAvailRQ.getAvailPreferences();
		if (availPref != null) {
			availTO.setRestrictionLevel(availPref.getRestrictionLevel());
			availTO.setBookingPaxType(availPref.getBookingPaxType());
			availTO.setFareCategoryType(availPref.getFareCategoryType());
			availTO.setHalfReturnFareQuote(availPref.isIncludeHalfReturn());
			availTO.setStayOverTimeInMillis(availPref.getStayOverTimeInMillis());
			availTO.setInboundFareModified(availPref.isInboundOnd());
			availTO.setModifyBooking(availPref.isModifyBooking());
			availTO.setRequoteFlightSearch(availPref.isRequoteFlightSearch());
			availTO.setAllowOverrideSameOrHigherFareMod(availPref.isAllowOverrideSameOrHigherFareMod());

			if (availPref.getQuoteOndFlexi() != null) {
				for (IntegerBooleanMap mapObj : availPref.getQuoteOndFlexi()) {
					availTO.setQuoteOndFlexi(mapObj.getKey(), mapObj.isValue());
				}
			}

			availTO.setTravelAgentCode(availPref.getTravelAgentCode());

			if (availPref.getAppIndicator() == null) {
				availTO.setAppIndicator(ApplicationEngine.XBE);
			} else {
				availTO.setAppIndicator(ApplicationEngine.valueOf(availPref.getAppIndicator()));
			}

			availTO.setPromoCode(availPref.getPromoCode());
			availTO.setBankIdentificationNumber(availPref.getBankIdNumber());
			availTO.setPromoApplicable(availPref.isSingleCarrier());
			availTO.setBundledFareApplicable(availPref.isSingleCarrier());
			availTO.setPreserveOndOrder(availPref.isPreserveOndOrder());

			if (availPref.getModifyPref() != null) {
				ModifyPreferences modifyPref = availPref.getModifyPref();
				availTO.setFlightsPerOndRestriction(modifyPref.getFlightsPerOndRestriction());
				availTO.setOldFareAmount(modifyPref.getOldFareAmount());
				availTO.setOldFareID(modifyPref.getOldFareId());
				availTO.setOldFareType(modifyPref.getOldFareType());

				availTO.setInverseFareID(modifyPref.getInverseFareId());

				if (availPref.getModifyPref().isInboundModified() != null) {
					availTO.setInboundFareModified(availPref.getModifyPref().isInboundModified());
				}
			}
		}
		return baseAvailRQ;
	}
	
	private void checkConstraints() throws WebservicesException, ModuleException {

		if (tnxResProcess == null) {
			String transactionId = PlatformUtiltiies.nullHandler((aaServiceTaxQuoteForTktRevRQ != null)
					? aaServiceTaxQuoteForTktRevRQ.getHeaderInfo().getTransactionIdentifier()
					: null);
			log.error(" TransactionalReservationProcess is null for txnId : " + transactionId);
			throw new WebservicesException(AAErrorCode.ERR_8_MAXICO_REQUIRED_TRANSACTION_INVALID_OR_EXPIRED,
					"Transaction not found");
		}
		if (!tnxResProcess.hasFareType(aaServiceTaxQuoteForTktRevRQ.getSelectedFareTypes())) {
			log.error(" Saved fare type in TransactionalReservationProcess is not same in ServiceTax quote request");
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}
	}

}
