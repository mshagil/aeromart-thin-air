package com.isa.thinair.aaservices.core.bl.alerting;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertPage;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAlertRetrievalRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAlertRetrievalRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Retrieves the list of alerts for the given search criteria.
 * 
 * @author sanjaya
 * 
 */
public class AARetrieveAlertsBL {
	/* Logger */
	private static Log log = LogFactory.getLog(AARetrieveAlertsBL.class);

	/* The request */
	private AAAlertRetrievalRQ aaAlertRetrievalRQ;

	/* New Alert Response */
	private AAAlertRetrievalRS aaAlertRetrievalRS;

	/**
	 * @param aaAlertRetrievalRQ
	 *            The request param to set.
	 */
	public void setRequestParams(AAAlertRetrievalRQ aaAlertRetrievalRQ) {
		this.aaAlertRetrievalRQ = aaAlertRetrievalRQ;
	}

	// Initializes the response variable.
	private void initializeAARetrieveAlertsRS() {
		aaAlertRetrievalRS = new AAAlertRetrievalRS();
		aaAlertRetrievalRS.setResponseAttributes(new AAResponseAttributes());
	}

	/**
	 * @return The alert response.
	 */
	public AAAlertRetrievalRS excute() {

		initializeAARetrieveAlertsRS();

		try {

			if (aaAlertRetrievalRQ.getSearchCriteria() != null) {
				AlertDetailDTO searchCriteria = composeSearchCriteria(aaAlertRetrievalRQ.getSearchCriteria());
				Page alertResult = AAServicesModuleUtils.getAlertingBD().retrieveAlertsForSearchCriteria(searchCriteria);
				AlertPage alertPage = composeAlertPage(alertResult);
				if (alertPage != null) {
					aaAlertRetrievalRS.setPage(alertPage);
				}
				return this.aaAlertRetrievalRS;
			} else {
				// TODO handle error

			}

		} catch (Exception ex) {
			log.error("Error occurred during retrieval of alerts for given search critirea", ex);
			AAExceptionUtil.addAAErrrors(aaAlertRetrievalRS.getResponseAttributes().getErrors(), ex);

		} finally {
			int numOfErrors = aaAlertRetrievalRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaAlertRetrievalRS.getResponseAttributes().setSuccess(successType);
		}

		return aaAlertRetrievalRS;
	}

	private static AlertDetail convertAlertDetailDTO(AlertDetailDTO alertDetailDTO) {
		// TODO - in the original AlertBD the search criteria and the search result is saved in the same AlertDetailDTO
		// object
		// change this when possible. Using the same notion is the LCC data flow as well.
		AlertDetail alertSearchCriteria = new AlertDetail();

		if (alertDetailDTO.getAlertId() != null) {
			alertSearchCriteria.setAlertId(alertDetailDTO.getAlertId());
		}
		alertSearchCriteria.setAlertDate(alertDetailDTO.getAlertDate());
		alertSearchCriteria.setFlightNumber(alertDetailDTO.getFlightNumber());
		alertSearchCriteria.setDepDate(alertDetailDTO.getDepDate());
		alertSearchCriteria.setPnr(alertDetailDTO.getPnr());
		alertSearchCriteria.setContent(alertDetailDTO.getContent());
		alertSearchCriteria.setPrivilege(alertDetailDTO.getPrivilege());
		alertSearchCriteria.setFromDate(alertDetailDTO.getFromDate());
		alertSearchCriteria.setToDate(alertDetailDTO.getToDate());
		alertSearchCriteria.setTotalRecs(alertDetailDTO.getTotalRecs());
		alertSearchCriteria.setStartRec(alertDetailDTO.getStartRec());
		alertSearchCriteria.setEndRec(alertDetailDTO.getEndRec());
		alertSearchCriteria.setOriginAirport(alertDetailDTO.getOriginAirport());
		alertSearchCriteria.setAgentCode(alertDetailDTO.getAgentCode());
		alertSearchCriteria.setViewAnyAlert(alertDetailDTO.isViewAnyAlert());
		alertSearchCriteria.setViewOwnAlert(alertDetailDTO.isViewOwnAlert());
		alertSearchCriteria.setViewReptAgentAlert(alertDetailDTO.isViewReptAgentAlert());
		alertSearchCriteria.setOriginAirport(alertDetailDTO.getOriginAirport());
		alertSearchCriteria.setSearchAirline(AppSysParamsUtil.getDefaultCarrierCode());
		alertSearchCriteria.setOriginDepDate(alertDetailDTO.getOriginDepDate());
		alertSearchCriteria.setOriginatorPnr(alertDetailDTO.getPnr());
		alertSearchCriteria.setAnciReprotectStatus(alertDetailDTO.getAnciReprotectStatus());

		return alertSearchCriteria;
	}

	/**
	 * Creates the AlertPage object from {@link Page}
	 * 
	 * @param alertPage
	 *            The alert page to be converted
	 * 
	 * @return The converted {@link AlertPage} object.
	 */
	private static AlertPage composeAlertPage(Page page) {

		AlertPage alertPage = null;

		if (page != null) {
			int totalNumberOfRecords = page.getTotalNoOfRecords();
			int totalNumberOfRecordsInSystem = page.getTotalNoOfRecordsInSystem();
			int startPosition = page.getStartPosition();
			int endPosition = page.getEndPosition();
			List<AlertDetail> alertDetailList = new ArrayList<AlertDetail>();

			if (page.getPageData() != null) {
				for (Object alertDetailDTO : page.getPageData()) {
					if (alertDetailDTO != null) {
						if (alertDetailDTO instanceof AlertDetailDTO) {
							alertDetailList.add(convertAlertDetailDTO((AlertDetailDTO) alertDetailDTO));
						}
					}
				}
			}

			alertPage = new AlertPage();
			alertPage.setEndPosition(endPosition);
			alertPage.setStartPosition(startPosition);
			alertPage.setTotalNoOfRecords(totalNumberOfRecords);
			alertPage.setTotalNoOfRecordsInSystem(totalNumberOfRecordsInSystem);
			alertPage.getAlertData().addAll(alertDetailList);
		}

		return alertPage;
	}

	private static AlertDetailDTO composeSearchCriteria(AlertDetail alertDetail) {
		// TODO - in the original AlertBD the search criteria and the search result is saved in the same AlertDetailDTO
		// object
		// change this when possible. Using the same notion is the LCC data flow as well.
		AlertDetailDTO alertDetailDTO = null;
		if (alertDetail != null) {
			alertDetailDTO = new AlertDetailDTO();
			alertDetailDTO.setFlightNumber(alertDetail.getFlightNumber());
			alertDetailDTO.setDepDate(alertDetail.getDepDate());
			alertDetailDTO.setPrivilege(alertDetail.getPrivilege());
			alertDetailDTO.setFromDate(alertDetail.getFromDate());
			alertDetailDTO.setToDate(alertDetail.getToDate());
			alertDetailDTO.setStartRec(alertDetail.getStartRec());
			alertDetailDTO.setEndRec(alertDetail.getEndRec());
			alertDetailDTO.setOriginAirport(alertDetail.getOriginAirport());
			alertDetailDTO.setAgentCode(alertDetail.getAgentCode());
			alertDetailDTO.setViewAnyAlert(alertDetail.isViewAnyAlert());
			alertDetailDTO.setViewOwnAlert(alertDetail.isViewOwnAlert());
			alertDetailDTO.setViewReptAgentAlert(alertDetail.isViewReptAgentAlert());
			alertDetailDTO.setOriginAirport(alertDetail.getOriginAirport());
			alertDetailDTO.setSearchAirline(alertDetail.getSearchAirline());
			alertDetailDTO.setOriginDepDate(alertDetail.getOriginDepDate());
			alertDetailDTO.setAnciReprotectStatus(alertDetail.getAnciReprotectStatus());
		}
		return alertDetailDTO;
	}
}
