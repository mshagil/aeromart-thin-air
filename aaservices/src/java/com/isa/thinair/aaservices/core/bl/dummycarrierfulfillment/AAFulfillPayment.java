package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExtCarrierFulfillmentUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * This class responsible for creating the dummy reservation for payment in the pay carrier
 * 
 * @author malaka
 * 
 */
class AAFulfillPayment extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillPayment.class);

	private boolean isAutoRefund;
	private String reasonForAllowBlPax;

	AAFulfillPayment(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos, boolean isAutoRefund,String reasonForAllowBlPax) {
		super(aaAirReservation, newAAAirReservation, aaPos);
		this.isAutoRefund = isAutoRefund;
		this.reasonForAllowBlPax = reasonForAllowBlPax;
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAPayCarrierFulfullmentRS::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			pnrModesDTO.setLoadExternalPaxPayments(true);
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			List<OriginDestinationOption> ondOptions = AAExtCarrierFulfillmentUtil.getOwnAndExternalOndOptions(aaAirReservation
					.getAirReservation());

			if (reservation == null) {
				if (log.isDebugEnabled()) {
					log.debug("No dummy reservation exist . Creating a new dummy reservaion for pnr = ["
							+ aaAirReservation.getGroupPnr() + "]");
				}
				// No dummy reservations created in the payment carrier
				AAAirBookRQ buildDummyRes = AAExtCarrierFulfillmentUtil.buildDummyRes(aaAirReservation);
				Object[] transformedBookRQInfo = new AAServicesTransformUtil.BookingRequest(buildDummyRes, null).transform();
				IReservation iReservation = (IReservation) transformedBookRQInfo[0];
				Set<ExternalPnrSegment> syncExtSegments = AAExtCarrierFulfillmentUtil.syncExtSegments(null, ondOptions);
				iReservation.addExternalPnrSegments(syncExtSegments);

				iReservation.setOriginCountryOfCall(aaAirReservation.getAirReservation().getOriginCountryOfCall());
				ReservationAssembler rAssembler = (ReservationAssembler) iReservation;
				reservation = rAssembler.getDummyReservation();
				reservation.setDummyBooking(ReservationInternalConstants.DummyBooking.YES);
				reservation.setInfantPaymentSeparated(ReservationInternalConstants.InfantPaymentRecordedWithInfant.YES);
			} else {
				// We have the reservation already in the payment carrier. Need to sync the extPnrSegments
				if (log.isDebugEnabled()) {
					log.debug("Reservaton already exist in the payment carrier . pnr = [" + aaAirReservation.getGroupPnr() + "]");
				}
				Set<ExternalPnrSegment> newExtSegments = AAExtCarrierFulfillmentUtil.syncExtSegments(reservation, ondOptions);
				for (ExternalPnrSegment extPnrSegments : newExtSegments) {
					Set<ExternalPnrSegment> existingExtsegments = reservation.getExternalReservationSegment();
					boolean isExternalSegmentExist = false;
					for (ExternalPnrSegment pnrSegment : existingExtsegments) {
						if (pnrSegment.getUniqueSegmentKey().equals(extPnrSegments.getUniqueSegmentKey())) {
							isExternalSegmentExist = true;
							break;
						}
					}

					if (!isExternalSegmentExist) {
						reservation.addExternalReservationSegment(extPnrSegments);
					}
				}
			}

			// updating the reservation and pax status form the opCarrier
			AAExtCarrierFulfillmentUtil.updateReservationsStatus(reservation, aaAirReservation);

			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaPos);
			ReservationAdminInfoTO adminInfoTO = AAExtCarrierFulfillmentUtil.buildReservationAdminInfoTO(aaAirReservation);

			// build the Ext_Pax_Transactions
			List<TravelerFulfillment> extCarriersFulfillment = aaAirReservation.getAirReservation().getFulfillment()
					.getExternalCarriersFulfillments();
			Map<Integer, Collection<ReservationPaxExtTnx>> paxWiseExtTnx = AAExtCarrierFulfillmentUtil
					.buildExternalPaymentFulfillment(reservation, extCarriersFulfillment);

			ReservationBD reservationDelegate = AAServicesModuleUtils.getReservationBD();
			ServiceResponce responce = reservationDelegate.createPayCarrierfulfillReservation(reservation, trackInfo,
					adminInfoTO, paxWiseExtTnx, reasonForAllowBlPax);
			Long version = (Long) responce.getResponseParam(CommandParamNames.VERSION);

			/*
			 * If this is an dry auto refund we need to do the payment reversal as well. Normally this is done at the
			 * Calling carrier side before the LCC call is made for refund. However since the dry auto refund call is
			 * initiated in LCC itself I had to go with this approach.
			 */
			if (isAutoRefund) {
				PaymentAssembler paymentAssembler = new PaymentAssembler();
				for (Collection<ReservationPaxExtTnx> paxTxns : paxWiseExtTnx.values()) {
					for (ReservationPaxExtTnx extTxn : paxTxns) {
						paymentAssembler.addAgentCreditPayment(extTxn.getAgentCode(), extTxn.getAmountPayCur(),
								extTxn.getExternalCarrierCode(), null, extTxn.getLccUniqueId(), extTxn.getPaxExternalTnxId());
					}
				}

				reservationDelegate.refundPayment(trackInfo, paymentAssembler, reservation.getPnr(),
						reservation.getContactInfo(), new ArrayList<Integer>());
			}

			carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
			carrierFulfillment.setResVersion(version.toString());

			if (log.isDebugEnabled()) {
				log.debug("AAPayCarrierFulfullmentRS::end");
			}

		} catch (ModuleException e) {
			log.error("fulfill AAFulfillPayment fail", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = aaAirReservation.getAirReservation();

		if ((airReservation.getAirItinerary() == null || airReservation.getAirItinerary().getOriginDestinationOption() == null
				|| airReservation.getAirItinerary().getOriginDestinationOption().isEmpty() || airReservation.getFulfillment() == null)) {

			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");

		}

	}

}
