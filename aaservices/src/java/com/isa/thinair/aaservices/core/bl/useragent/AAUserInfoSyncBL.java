package com.isa.thinair.aaservices.core.bl.useragent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCAgent;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCRole;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCUser;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUserInfoSyncRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUserInfoSyncRS;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.SecurityUtil;
import com.isa.thinair.airsecurity.api.dto.external.RoleTO;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.RoleAccessibility;
import com.isa.thinair.airsecurity.api.model.RoleVisibility;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;

public class AAUserInfoSyncBL {
	private static Log log = LogFactory.getLog(AAUserInfoSyncBL.class);

	private static final String AA_SERVICE_CHANNEL = "AA";

	public AAUserInfoSyncRQ aaUserInfoSyncRQ;

	public AAUserInfoSyncBL(AAUserInfoSyncRQ aaUserInfoSyncRQ) {
		this.aaUserInfoSyncRQ = aaUserInfoSyncRQ;

	}

	public AAUserInfoSyncRS execute() {
		AAUserInfoSyncRS aaUserInfoSyncRS = new AAUserInfoSyncRS();
		aaUserInfoSyncRS.setResponseAttributes(new AAResponseAttributes());

		List<String> savedUserIds = new ArrayList<String>();
		List<String> savedRoleIds = new ArrayList<String>();
		List<String> savedAgentCodes = new ArrayList<String>();

		try {
			if (aaUserInfoSyncRQ.getRoleList().size() > 0) {
				syncRoles(aaUserInfoSyncRQ.getRoleList(), savedRoleIds);
				aaUserInfoSyncRS.getUpdatedRoleIds().addAll(savedRoleIds);
			}

			if (aaUserInfoSyncRQ.getAgentList().size() > 0) {
				syncAgents(aaUserInfoSyncRQ.getAgentList(), savedAgentCodes);
				aaUserInfoSyncRS.getUpdatedAgentCodes().addAll(savedAgentCodes);
			}

			if (aaUserInfoSyncRQ.getUserList().size() > 0) {
				syncUsers(aaUserInfoSyncRQ.getUserList(), savedUserIds);
				aaUserInfoSyncRS.getUpdatedUserIds().addAll(savedUserIds);
			}

			aaUserInfoSyncRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (ModuleException ex) {
			log.error("AAUserInfoSyncBL syncUserInfo failed.", ex);
			AAExceptionUtil.addAAErrrors(aaUserInfoSyncRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("AAUserInfoSyncBL syncUserInfo failed.", e);
			AAExceptionUtil.addAAErrrors(aaUserInfoSyncRS.getResponseAttributes().getErrors(), e);
		}

		return aaUserInfoSyncRS;
	}

	private void syncAgents(List<LCCAgent> agentList, List<String> savedAgentCodes) throws ModuleException {
		TravelAgentBD travelAgentBD = AAServicesModuleUtils.getTravelAgentBD();

		String airlineAgentType = AppSysParamsUtil.getCarrierAgent();

		for (LCCAgent lccAgent : aaUserInfoSyncRQ.getAgentList()) {
			Agent airAgent = new Agent();
			airAgent.setAgentCode(lccAgent.getAgentCode());
			if (lccAgent.getAgentTypeCode().equals(Util.AIRLINE_AGENT_TYPE)) {
				airAgent.setAgentTypeCode(airlineAgentType);
			} else {
				airAgent.setAgentTypeCode(lccAgent.getAgentTypeCode());
			}
			airAgent.setAgentName(lccAgent.getAgentName());
			airAgent.setStationCode(lccAgent.getStationCode());
			airAgent.setTerritoryCode(lccAgent.getTerritoryCode());
			airAgent.setTelephone(lccAgent.getTelephone());
			airAgent.setFax(lccAgent.getFax());
			airAgent.setEmailId(lccAgent.getEmailId());
			airAgent.setAgentIATANumber(lccAgent.getAgentIATANumber());
			airAgent.setStatus(lccAgent.getStatus());
			airAgent.setCreatedBy(lccAgent.getCreatedBy());
			airAgent.setCreatedDate(lccAgent.getCreatedDate());
			airAgent.setModifiedBy(lccAgent.getModifiedBy());
			airAgent.setModifiedDate(lccAgent.getModifiedDate());
			airAgent.setVersion(lccAgent.getVersion());
			airAgent.setReportingToGSA(lccAgent.getReportToGSA());
			airAgent.setBillingEmail(lccAgent.getBillingEmail());
			airAgent.setCurrencyCode(lccAgent.getCurrencyCode());
			airAgent.setNotes(lccAgent.getNotes());
			airAgent.setCurrencySpecifiedIn(lccAgent.getSpecifiedIn());
			airAgent.setAutoInvoice(lccAgent.getAutoInvoice());
			airAgent.setLanguageCode(lccAgent.getLanguageCode());
			airAgent.setOnHold(lccAgent.getOnhold());
			airAgent.setServiceChannel(lccAgent.getServiceChannelCode());
			airAgent.setCapturePaymentRef(lccAgent.getCapturePayRef());
			airAgent.setPrefferedRptFormat(lccAgent.getPrefRPTFormat());
			airAgent.setPayRefMandatory(lccAgent.getPayRefMandatory());
			airAgent.setPublishToWeb(lccAgent.getWebsiteVisibility());
			airAgent.setMobile(lccAgent.getMobile());
			airAgent.setEnableWSMultiCurrency(lccAgent.getShowPriceInSelCur());
			airAgent.setAirlineCode(lccAgent.getAirlineCode());
			airAgent.setCharterAgent(lccAgent.getCharterAgent());
			airAgent.setGsaCode(lccAgent.getGsaCode());
			airAgent.setAgentWiseFareMaskEnable(lccAgent.getFareMask());
			airAgent.setLccPublishStatus(Util.LCC_PUBLISHED);
			airAgent.setHandlingFeeChargeBasisCode(lccAgent.getHandlingChgBasis());
			airAgent.setHandlingFeeAppliesTo((int) lccAgent.getHandlingChgAppliesTo());
			airAgent.setHandlingFeeEnable(lccAgent.getEnableHandlingChg());
			airAgent.setAgentWiseTicketEnable(lccAgent.getEnableAgentWiseTicket());
			airAgent.setHasCreditLimit(lccAgent.getHasCreditLimit());
			airAgent.setHandlingFeeModificationApllieTo((int) lccAgent.getHandlingChgAppliesToMod());
			airAgent.setHandlingFeeModificationEnabled(lccAgent.getEnableHandlingChgForMod());

			if (lccAgent.getPaymentCodes() != null && lccAgent.getPaymentCodes().size() > 0) {
				Set<String> agentPaymentModes = new HashSet<String>(lccAgent.getPaymentCodes());
				airAgent.setPaymentMethod(agentPaymentModes);
			}

			Agent oldAgent = travelAgentBD.getAgent(airAgent.getAgentCode());
			if (oldAgent != null && oldAgent.getVersion() > airAgent.getVersion()) {
				// TO DO error
			} else {
				if (oldAgent != null) {
					airAgent.setVersion(oldAgent.getVersion());

					// TODO set existing fields

					Agent newAgent = travelAgentBD.saveAgent(airAgent);
					if (lccAgent.getVersion() != (newAgent.getVersion() + 1)) {
						travelAgentBD.updateAgentVersion(newAgent.getAgentCode(), newAgent.getVersion());
					}
				} else {
					airAgent.setVersion(-1);
					Agent newAgent = travelAgentBD.saveAgent(airAgent);
				}

				savedAgentCodes.add(airAgent.getAgentCode());
			}

		}

	}

	private void syncRoles(List<LCCRole> roleList, List<String> savedRoleIds) throws ModuleException {
		SecurityBD securityBD = AAServicesModuleUtils.getSecurityBD();

		String airlineAgentType = AppSysParamsUtil.getCarrierAgent();

		for (LCCRole lccRole : aaUserInfoSyncRQ.getRoleList()) {
			Role airRole = new Role();
			airRole.setRoleId(lccRole.getRoleId());
			airRole.setRoleName(lccRole.getRoleName());
			airRole.setRemarks(lccRole.getRemarks());
			airRole.setCreatedBy(lccRole.getCreatedBy());
			airRole.setCreatedDate(lccRole.getCreatedDate());
			airRole.setModifiedBy(lccRole.getModifiedBy());
			airRole.setModifiedDate(lccRole.getModifiedDate());
			airRole.setVersion(lccRole.getVersion());
			airRole.setStatus(lccRole.getStatus());
			airRole.setServiceChannel(lccRole.getServiceChannelCode());
			airRole.setAirlineCode(lccRole.getAirlineCode());
			airRole.setIncludeExcludeFlag(lccRole.getIncludeExclude());
			airRole.setLccPublishStatus(Util.LCC_PUBLISHED);

			if (lccRole.getStatus().equals(Role.STATUS_DELETE)) {
				securityBD.removeRole(lccRole.getRoleId(), lccRole.getServiceChannelCode());
				savedRoleIds.add(airRole.getRoleId());
				continue;
			}

			Set<RoleAccessibility> setAccessTypes = new HashSet<RoleAccessibility>();
			RoleAccessibility roleAccessibility = null;
			for (String assignableType : lccRole.getAssignableAgentTypeCodes()) {
				roleAccessibility = new RoleAccessibility();
				if (assignableType.equals(Util.AIRLINE_AGENT_TYPE)) {
					roleAccessibility.setAgentTypeCode(airlineAgentType);
				} else {
					roleAccessibility.setAgentTypeCode(assignableType);
				}
				setAccessTypes.add(roleAccessibility);
			}
			airRole.setAgentTypeAccessiblity(setAccessTypes);

			Set<RoleVisibility> setVisibilityTypes = new HashSet<RoleVisibility>();
			RoleVisibility roleVisibility = null;
			for (String visibleType : lccRole.getVisibleAgentTypeCodes()) {
				roleVisibility = new RoleVisibility();
				if (visibleType.equals(Util.AIRLINE_AGENT_TYPE)) {
					roleVisibility.setAgentTypeCode(airlineAgentType);
				} else {
					roleVisibility.setAgentTypeCode(visibleType);
				}
				setVisibilityTypes.add(roleVisibility);
			}
			airRole.setAgentTypeVisibilities(setVisibilityTypes);

			Set<Privilege> rolePriv = new HashSet<Privilege>();
			if (lccRole.getPrivilegeIds() != null && lccRole.getPrivilegeIds().size() > 0) {
				for (String privilegeId : lccRole.getPrivilegeIds()) {
					Privilege privilege = SecurityUtil.getPrivilege(privilegeId);
					rolePriv.add(privilege);
				}
				airRole.setPrivileges(rolePriv);
			}

			Role oldRole = securityBD.getRole(airRole.getRoleId());
			if (oldRole != null && oldRole.getVersion() > airRole.getVersion()) {
				// TO DO error
			} else {
				if (oldRole != null) {
					airRole.setVersion(oldRole.getVersion());
					securityBD.saveRole(airRole, null);
					if (lccRole.getVersion() != airRole.getVersion() + 1) {
						securityBD.updateRoleVersion(airRole.getRoleId(), airRole.getVersion());
					}
				} else {
					airRole.setVersion(-1);
					securityBD.saveRole(airRole, null);
				}

				savedRoleIds.add(airRole.getRoleId());
			}

		}

	}

	private void syncUsers(List<LCCUser> userList, List<String> savedUserIds) throws ModuleException {
		SecurityBD securityBD = AAServicesModuleUtils.getSecurityBD();
		for (LCCUser lccUser : aaUserInfoSyncRQ.getUserList()) {

			UserTO userTO = new UserTO();
			userTO.setUserId(lccUser.getUserId());
			userTO.setPassword(lccUser.getPassword());
			userTO.setDisplayName(lccUser.getDisplayName());
			userTO.setEmailId(lccUser.getEmailId());
			userTO.setStatus(lccUser.getStatus());
			userTO.setCreatedBy(lccUser.getCreatedBy());
			userTO.setCreatedDate(lccUser.getCreatedDate());
			userTO.setModifiedBy(lccUser.getModifiedBy());
			userTO.setModifiedDate(lccUser.getModifiedDate());
			userTO.setVersion(lccUser.getVersion());
			userTO.setAgentCode(lccUser.getAgentCode());
			userTO.setDefaultCarrierCode(lccUser.getDefaultCarrierCode());
			userTO.setAdminUserCreateStatus(lccUser.getAdminUserCreateStatus());
			userTO.setNormalUserCreateStatus(lccUser.getNormalUserCreateStatus());
			userTO.setServiceChannel(lccUser.getServiceChannelCode());
			userTO.setPasswordReseted(lccUser.getPasswordResetted());
			userTO.setAirlineCode(lccUser.getAirlineCode());
			userTO.setMyIDCarrierCode(lccUser.getMyIdCarrierCode());
			userTO.setLccPublishStatus(Util.LCC_PUBLISHED);
			userTO.setThemeCode("default");

			if (lccUser.getRoleIds() != null && lccUser.getRoleIds().size() > 0) {
				Set<RoleTO> userRoles = new HashSet<RoleTO>();
				for (String roleId : lccUser.getRoleIds()) {
					RoleTO roleTO = new RoleTO();
					roleTO.setRoleId(roleId);
					userRoles.add(roleTO);
				}
				userTO.setRoleTOs(userRoles);
			}

			if (lccUser.getCarrierCodes() != null && lccUser.getCarrierCodes().size() > 0) {
				Set<String> userCarriers = new HashSet<String>(lccUser.getCarrierCodes());
				userTO.setUserCarriers(userCarriers);
			}

			User oldUser = securityBD.getUserBasicDetails(lccUser.getUserId());
			if (oldUser != null && oldUser.getVersion() > lccUser.getVersion()) {
				// TO DO error
			} else {
				if (oldUser != null) {
					userTO.setVersion(oldUser.getVersion());
					securityBD.saveOrUpdateUser(userTO);
					if (lccUser.getVersion() != userTO.getVersion()) {
						securityBD.updateUserVersion(userTO.getUserId(), lccUser.getVersion());
					}
				} else {
					userTO.setVersion(-1);
					securityBD.saveOrUpdateUser(userTO);
				}

				savedUserIds.add(userTO.getUserId());
			}
		}
	}
}
