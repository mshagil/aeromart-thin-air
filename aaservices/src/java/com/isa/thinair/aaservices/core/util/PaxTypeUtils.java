package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;

public class PaxTypeUtils {

	// Variables are private since all RPH manipulation should be within these methods only
	private static final String $ = "$";
	private static final String CARRIER_SEPERATOR = "|";
	private static final String TRAVELLER_SEPERATOR = ",";
	public static String convertLCCPaxCodeToAAPaxCodes(PassengerType passengerType) {
		switch (passengerType) {
		case ADT:
			return PaxTypeTO.ADULT;
		case CHD:
			return PaxTypeTO.CHILD;
		case INF:
			return PaxTypeTO.INFANT;
		case PRT:
			return PaxTypeTO.PARENT;
		default:
			return null; // undefined pax type found, return null
		}
	}

	public static PassengerType convertAAPaxCodeToLCCPaxCodes(String applicablePaxType) {
		if (applicablePaxType.equals(PaxTypeTO.ADULT)) {
			return PassengerType.ADT;
		} else if (applicablePaxType.equals(PaxTypeTO.CHILD)) {
			return PassengerType.CHD;
		} else if (applicablePaxType.equals(PaxTypeTO.INFANT)) {
			return PassengerType.INF;
		} else if (applicablePaxType.equals(PaxTypeTO.PARENT)) {
			return PassengerType.PRT;
		}

		return null;
	}

	/**
	 * Returns encoded traveler reference information.
	 * 
	 * @param reservationPax
	 * @return
	 */
	public static String travelerReference(ReservationPax reservationPax) {
		// TODO Seems this is common for LCCClient / AAServices etc. Possibly let's refactor.
		String travelerReference = "";
		if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPax.getPaxType())) {
			travelerReference = "A" + reservationPax.getPaxSequence() + $ + reservationPax.getPnrPaxId();
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPax.getPaxType())) {
			travelerReference = "C" + reservationPax.getPaxSequence() + $ + reservationPax.getPnrPaxId();
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPax.getPaxType())) {
			travelerReference = "I" + reservationPax.getPaxSequence() + "/A" + reservationPax.getParent().getPaxSequence() + $
					+ reservationPax.getPnrPaxId();
		}
		return travelerReference;
	}

	public static Integer getPnrPaxId(String lccTravelerReferenceNumber) {
		int pnrPaxIdStartIndex = lccTravelerReferenceNumber.lastIndexOf($);
		pnrPaxIdStartIndex = pnrPaxIdStartIndex + 1;
		if (pnrPaxIdStartIndex > 0) {
			String pnrPaxId = lccTravelerReferenceNumber.substring(pnrPaxIdStartIndex);
			return new Integer(pnrPaxId);
		}
		return null;
	}

	/**
	 * method to extract carrier (AA services) traveler ref number from LCC traveler ref number
	 */
	private static String getCarrierTravelerRefFromLCCTravelerRef(String airlineCode, String lccTravelerRef) {
		String carrierTravelerRef = null;
		if (lccTravelerRef != null) {
			String[] travelRefNumbers = lccTravelerRef.split(TRAVELLER_SEPERATOR);
			for (String carrerRefProtion : travelRefNumbers) {
				if (carrerRefProtion.indexOf(airlineCode, 0) >= 0) {
					int indexOfAirlineCodeSeparator = carrerRefProtion.lastIndexOf(CARRIER_SEPERATOR);
					carrierTravelerRef = carrerRefProtion.substring(indexOfAirlineCodeSeparator + 1);
					carrierTravelerRef = carrierTravelerRef.trim();
					break;
				}
			}
		}
		return carrierTravelerRef;
	}

	public static Collection<PassengerType> composePassengerTypes(PassengerType... passengerType) {
		Collection<PassengerType> colPassengerTypes = new ArrayList<PassengerType>();

		for (int i = 0; i < passengerType.length; i++) {
			colPassengerTypes.add(passengerType[i]);
		}

		return colPassengerTypes;
	}

	public static Integer getPaxSeq(String travelerRefNumber) {
		StringBuilder sequence = new StringBuilder();
		String numbers = "0123456789";

		int indexOf$ = travelerRefNumber.indexOf($.charAt(0));

		for (int i = indexOf$ - 1; i >= 0; i--) {
			char charAt = travelerRefNumber.charAt(i);
			if (numbers.indexOf(charAt) == -1) {
				if (i > 2) {
					if (travelerRefNumber.charAt(i - 1) == '/') {
						sequence = new StringBuilder();
						i--;
						continue;
					}
				}
				break;
			} else {
				sequence.append(charAt);
			}
		}

		return Integer.valueOf(sequence.reverse().toString());
	}

	/**
	 * convert the travelerRef to RPH. If travelerRef is already in RPH return itself Eg : A1$12345667 --> A1 I1/A1 -->
	 * I1/A1
	 * 
	 * @param travelerRefNumber
	 * @return
	 */
	static String getRPHFromTraverRef(String travelerRefNumber) {
		int indexOf$ = travelerRefNumber.indexOf($);
		if (indexOf$ != -1) {
			return travelerRefNumber.substring(0, indexOf$);
		}

		return travelerRefNumber;
	}

	/**
	 * If the passed traveler ref number is not that of an infant then this method will return a (-1)
	 * 
	 * @param infantTravelerRefNumber
	 * @return
	 */
	// TODO : This method should be factored out for both AA & LCC
	public static Integer getParentSeq(String infantTravelerRefNumber) {

		StringBuilder sequence = new StringBuilder();
		String numbers = "0123456789";

		int indexOfFS = infantTravelerRefNumber.indexOf('/');
		if (indexOfFS == -1) {
			return indexOfFS;
		}

		for (int i = indexOfFS + 2; i <= infantTravelerRefNumber.length(); i++) {
			char charAt = infantTravelerRefNumber.charAt(i);
			if (numbers.indexOf(charAt) == -1) {
				break;
			} else {
				sequence.append(charAt);
			}
		}

		return Integer.valueOf(sequence.toString());
	}

	/**
	 * TEST METHODS
	 * 
	 * (Should be moved to proper JUnit class)
	 */
	public static void main(String[] args) {
		// Test getPnrPaxId(...) method
		System.out.println(getPnrPaxId("A1$123456"));

		String travelerRef = "3O|I3/A2$308393, G9|I3/A2$23302605";
		System.out.println("Old travelerRef = " + travelerRef);
		LCCClientReservationPax infLccPax = new LCCClientReservationPax();
		infLccPax.setParent(new LCCClientReservationPax());
		infLccPax.getParent().setPaxSequence(1); // Setting new parent pax sequence
		infLccPax.setPaxSequence(3);
		infLccPax.setTravelerRefNumber(travelerRef);
		System.out.println(getCarrierTravelerRefFromLCCTravelerRef("G9", travelerRef));
		System.out.println(getPnrPaxId(getCarrierTravelerRefFromLCCTravelerRef("G9", travelerRef)));
	}
}