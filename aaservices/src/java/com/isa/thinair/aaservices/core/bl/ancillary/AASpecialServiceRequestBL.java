package com.isa.thinair.aaservices.core.bl.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

class AASpecialServiceRequestBL {

	private static final Log log = LogFactory.getLog(AASpecialServiceRequestBL.class);

	private String carrierCode;
	private String salesChannel;
	private Map<Integer, Set<String>> flightSegIdWiseSelectedSSRs;
	private boolean modifyAnci;
	private boolean hasPrivModifySSRWithinBuffer = false;

	List<FlightSegmentSpecialServiceRequests> execute(List<BookingFlightSegment> flightSegments,
			String salesChannelCode, String selectedLanguage, boolean isModifyAnci) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("execute called.");
		}

		List<FlightSegmentSpecialServiceRequests> flightSegmentSpecialServiceRequests = new ArrayList<FlightSegmentSpecialServiceRequests>();

		SsrBD ssrDelegate = AAServicesModuleUtils.getSsrServiceBD();

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();

		for (BookingFlightSegment bookingFlightSegment : flightSegments) {
			ClassOfServiceDTO cosDto = new ClassOfServiceDTO(bookingFlightSegment.getCabinClassCode(),
					bookingFlightSegment.getLogicalCabinClassCode(), bookingFlightSegment.getSegmentCode());
			flightSegIdWiseCos.put(new Integer(bookingFlightSegment.getFlightRefNumber()), cosDto);
		}

		List<SpecialServiceRequestDTO> tempList = ssrDelegate.getActiveSSRsByFltSegIds(flightSegIdWiseCos,
				SSRCategory.ID_INFLIGHT_SERVICE.toString(), salesChannelCode,
				AppSysParamsUtil.isInventoryCheckForSSREnabled(), selectedLanguage, isModifyAnci, false,
				hasPrivModifySSRWithinBuffer, false);

		for (Entry<Integer, ClassOfServiceDTO> cosEntry : flightSegIdWiseCos.entrySet()) {
			if (tempList != null && tempList.size() > 0) {
				if (log.isDebugEnabled()) {
					log.debug("special service requests retrieved : " + tempList);
				}

				for (BookingFlightSegment flightSegment : flightSegments) {

					if (new Integer(flightSegment.getFlightRefNumber()).intValue() != cosEntry.getKey().intValue()) {
						continue;
					}

					FlightSegmentSpecialServiceRequests specialServiceRequests = new FlightSegmentSpecialServiceRequests();
					specialServiceRequests.setFlightSegment(flightSegment);

					Set<String> segmentsList = new HashSet<String>();
					String fltSegArr[] = flightSegment.getSegmentCode().split("/");

					if (fltSegArr.length == 2) {
						segmentsList.add(fltSegArr[0] + "/" + fltSegArr[1]);
					} else {
						for (int i = 0; i < fltSegArr.length - 2; i++) {
							String ori = fltSegArr[i + 0];
							String con = fltSegArr[i + 1];
							String des = fltSegArr[i + 2];

							segmentsList.add(ori + "/" + con);
							segmentsList.add(con + "/" + des);

						}
					}

					for (String segment : segmentsList) {
						String segArr[] = segment.split("/");
						String fltSegOrigin = segArr[0];
						String fltSegDestination = segArr[1];

						for (SpecialServiceRequestDTO specialServiceRequestDTO : tempList) {

							// If the SSRs with 0 available quantity or Inactive
							// status isn't added to reservation skip
							// those
							// SSRs.
							if ((specialServiceRequestDTO.getAvailableQty().intValue() == 0
									|| "N".equals(specialServiceRequestDTO.getStatus()))
									&& !isSSRAlreadyAdded(
											FlightRefNumberUtil
													.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()),
											specialServiceRequestDTO.getSsrCode())) {
								continue;
							}

							String ssrOrigin = specialServiceRequestDTO.getOndCode().split("/")[0];
							String ssrDestination = specialServiceRequestDTO.getOndCode().split("/")[1];

							if ((fltSegOrigin.equals(ssrOrigin) || fltSegDestination.equals(ssrDestination))) {
								specialServiceRequests.getSpecialServiceRequest()
										.add(populateSpecialServiceRequest(specialServiceRequestDTO));
							}
						}
					}

					flightSegmentSpecialServiceRequests.add(specialServiceRequests);
				}
			}
		}

		return flightSegmentSpecialServiceRequests;
	}

	public List<FlightSegmentSpecialServiceRequests> getSelectedInflightServices(
			List<SelectedSSR> selectedInflightServices, boolean skipeCutoverValidation) throws ModuleException {
		List<FlightSegmentSpecialServiceRequests> fltSegInflightList = new ArrayList<FlightSegmentSpecialServiceRequests>();

		if (selectedInflightServices != null) {

			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
			for (SelectedSSR selectedFlight : selectedInflightServices) {
				ClassOfServiceDTO cosDto = new ClassOfServiceDTO(selectedFlight.getFlightSegment().getCabinClassCode(),
						selectedFlight.getFlightSegment().getLogicalCabinClassCode(),
						selectedFlight.getFlightSegment().getSegmentCode());
				flightSegIdWiseCos.put(FlightRefNumberUtil
						.getSegmentIdFromFlightRPH(selectedFlight.getFlightSegment().getFlightRefNumber()), cosDto);
			}

			Map<Integer, List<SpecialServiceRequestDTO>> availableServiceMap = AAServicesModuleUtils.getSsrServiceBD()
					.getAvailableSSRs(flightSegIdWiseCos, salesChannel, false, modifyAnci, skipeCutoverValidation);

			for (Entry<Integer, List<SpecialServiceRequestDTO>> entry : availableServiceMap.entrySet()) {
				for (SelectedSSR selectedSSR : selectedInflightServices) {
					Integer flightSegId = FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(selectedSSR.getFlightSegment().getFlightRefNumber());
					if (flightSegId.intValue() == entry.getKey().intValue()) {
						FlightSegmentSpecialServiceRequests ssr = new FlightSegmentSpecialServiceRequests();
						ssr.setFlightSegment(selectedSSR.getFlightSegment());

						List<SpecialServiceRequestDTO> ssrList = entry.getValue();
						if (ssrList != null) {
							for (SpecialServiceRequestDTO specialServiceRequestDTO : ssrList) {
								ssr.getSpecialServiceRequest()
										.add(populateSpecialServiceRequest(specialServiceRequestDTO));
							}

						}

						fltSegInflightList.add(ssr);
					}
				}
			}
		}

		return fltSegInflightList;
	}

	private Map<Integer, String> getFlightIdCabinClassMap(List<BookingFlightSegment> flightSegments) {
		Map<Integer, String> fltCabinMap = new HashMap<Integer, String>();

		for (BookingFlightSegment bkgFltSeg : flightSegments) {
			fltCabinMap.put(new Integer(bkgFltSeg.getFlightRefNumber()), bkgFltSeg.getCabinClassCode());
		}

		return fltCabinMap;
	}

	private Map<Integer, String> getFltSegIdsMap(List<BookingFlightSegment> flightSegments) {
		Map<Integer, String> fltSegIdsMap = new HashMap<Integer, String>();
		for (BookingFlightSegment bkgFltSeg : flightSegments) {
			fltSegIdsMap.put(new Integer(bkgFltSeg.getFlightRefNumber()), bkgFltSeg.getSegmentCode());
		}
		return fltSegIdsMap;
	}

	private SpecialServiceRequest populateSpecialServiceRequest(SpecialServiceRequestDTO specialServiceRequestDTO) {
		SpecialServiceRequest specialServiceRequest = new SpecialServiceRequest();
		specialServiceRequest.setStatus("ACT");
		specialServiceRequest.setSsrCode(specialServiceRequestDTO.getSsrCode());
		specialServiceRequest.setSsrName(specialServiceRequestDTO.getSsrName());

		BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (specialServiceRequestDTO.getChargeAmount() != null
				&& !specialServiceRequestDTO.getChargeAmount().equals("")) {
			serviceCharge = new BigDecimal(specialServiceRequestDTO.getChargeAmount());
		}

		specialServiceRequest.setCharge(serviceCharge);
		specialServiceRequest.setCarrierCode(carrierCode);

		if (specialServiceRequestDTO.getDecriptionSelectedLanguage() == null || StringUtil
				.getUnicode(specialServiceRequestDTO.getDecriptionSelectedLanguage()).equalsIgnoreCase("null")) {
			specialServiceRequest.setDescription(specialServiceRequestDTO.getDescription());
		} else {
			specialServiceRequest.setDescription(
					StringUtil.getUnicode(specialServiceRequestDTO.getDecriptionSelectedLanguage()).replace('^', ','));
		}
		specialServiceRequest.setServiceQuantity("");
		specialServiceRequest.setAvailableQty(specialServiceRequestDTO.getAvailableQty());

		return specialServiceRequest;
	}

	private boolean isSSRAlreadyAdded(Integer flightSegId, String ssrCode) {

		if (flightSegIdWiseSelectedSSRs != null) {
			for (Entry<Integer, Set<String>> entry : flightSegIdWiseSelectedSSRs.entrySet()) {
				if (flightSegId != null && (flightSegId.intValue() == entry.getKey().intValue())
						&& entry.getValue() != null && entry.getValue().contains(ssrCode)) {
					return true;
				}
			}
		}

		return false;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}

	public Map<Integer, Set<String>> getFlightSegIdWiseSelectedSSRs() {
		return flightSegIdWiseSelectedSSRs;
	}

	public void setFlightSegIdWiseSelectedSSRs(Map<Integer, Set<String>> flightSegIdWiseSelectedSSRs) {
		this.flightSegIdWiseSelectedSSRs = flightSegIdWiseSelectedSSRs;
	}

	public boolean isModifyAnci() {
		return modifyAnci;
	}

	public void setModifyAnci(boolean modifyAnci) {
		this.modifyAnci = modifyAnci;
	}

	public void setHasPrivModifySSRWithinBuffer(boolean hasPrivModifySSRWithinBuffer) {
		this.hasPrivModifySSRWithinBuffer = hasPrivModifySSRWithinBuffer;
	}

}
