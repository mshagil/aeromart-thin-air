package com.isa.thinair.reporting.api.constants;

public enum ReportNamesEnum {

	COMPANY_PAYMENT("companyPayment");

	private String reportName;

	private ReportNamesEnum(String name) {
		reportName = name;
	}

	public String getReportName() {
		return reportName;
	}

	public static ReportNamesEnum getReportNameByString(String reportName) {
		if (reportName.equals(COMPANY_PAYMENT.getReportName())) {
			return COMPANY_PAYMENT;
		}
		return COMPANY_PAYMENT;
	}
}
