/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.reporting.api.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * @author Chamindap
 * 
 */
public class PaymentMode implements Serializable {

	private static final long serialVersionUID = 6639264894566510351L;

	String paymentCode;

	String paymentDescription;

	/**
	 * The constructor.
	 */
	public PaymentMode() {
	}

	/**
	 * @return Returns the paymentCode.
	 */
	public String getPaymentCode() {
		return paymentCode;
	}

	/**
	 * @param paymentCode
	 *            The paymentCode to set.
	 */
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}

	/**
	 * @return Returns the paymentDescription.
	 */
	public String getPaymentDescription() {
		return paymentDescription;
	}

	/**
	 * @param paymentDescription
	 *            The paymentDescription to set.
	 */
	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) return true;

		if (object == null || getClass() != object.getClass()) {
			return false;
		}

		PaymentMode that = (PaymentMode) object;

		return new EqualsBuilder()
				.append(paymentCode, that.paymentCode)
				.append(paymentDescription, that.paymentDescription)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(paymentCode)
				.append(paymentDescription)
				.toHashCode();
	}
}