package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

/**
 * @author harsha
 * 
 */
public class FlaAlertDTO implements Serializable {

	private static final long serialVersionUID = 3587624415563968231L;

	private Integer alertId;

	private String isActioned;

	private String alertType;

	private String alertDate;

	private String bkngDate;

	private String pnr;

	private String content;

	private String originAirport;

	private String passengerName;

	private String bkngMode;

	private String systemRemarks;

	private String bkngClass;

	private String mobileNo;

	private String phoneNo;

	public Integer getAlertId() {
		return alertId;
	}

	public String getAlertType() {
		return alertType;
	}

	public String getAlertDate() {
		return alertDate;
	}

	public String getBkngDate() {
		return bkngDate;
	}

	public String getPnr() {
		return pnr;
	}

	public String getContent() {
		return content;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public String getBkngMode() {
		return bkngMode;
	}

	public String getSystemRemarks() {
		return systemRemarks;
	}

	public String getBkngClass() {
		return bkngClass;
	}

	public void setAlertId(Integer alertId) {
		this.alertId = alertId;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public void setAlertDate(String alertDate) {
		this.alertDate = alertDate;
	}

	public void setBkngDate(String bkngDate) {
		this.bkngDate = bkngDate;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public void setBkngMode(String bkngMode) {
		this.bkngMode = bkngMode;
	}

	public void setSystemRemarks(String systemRemarks) {
		this.systemRemarks = systemRemarks;
	}

	public void setBkngClass(String bkngClass) {
		this.bkngClass = bkngClass;
	}

	public String getIsActioned() {
		return isActioned;
	}

	public void setIsActioned(String isActioned) {
		this.isActioned = isActioned;
	}

	public String getMobileNo() {
		if (this.mobileNo == null)
			return "";
		else if (!"".equals(this.mobileNo))
			return " / " + this.mobileNo;
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPhoneNo() {
		if (this.phoneNo == null)
			return "";
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

}
