package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

public class DistributionFlightInfoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4093199533742458722L;
	private String country;
	private String agents;
	private String cities;
	private String flights;
	private String countryCode;

	private Collection<BookingsTO> channelWiseBookings;
	private Map<String, BigDecimal> salesByChannel;

	public DistributionFlightInfoTO() {
		super();
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAgents() {
		return agents;
	}

	public void setAgents(String agents) {
		this.agents = agents;
	}

	public String getCities() {
		return cities;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	public String getFlights() {
		return flights;
	}

	public void setFlights(String flights) {
		this.flights = flights;
	}

	@JSON
	public Collection<BookingsTO> getChannelWiseBookings() {
		return channelWiseBookings;
	}

	public void setChannelWiseBookings(Collection<BookingsTO> channelWiseBookings) {
		this.channelWiseBookings = channelWiseBookings;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Map<String, BigDecimal> getSalesByChannel() {
		return salesByChannel;
	}

	public void setSalesByChannel(Map<String, BigDecimal> salesByChannel) {
		this.salesByChannel = salesByChannel;
	}

}
