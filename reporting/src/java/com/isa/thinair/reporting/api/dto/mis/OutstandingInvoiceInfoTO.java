package com.isa.thinair.reporting.api.dto.mis;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OutstandingInvoiceInfoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1269376774586602310L;

	private String invoceNumber;

	private Date invoiceDate;

	private BigDecimal invoiceAmount;

	private BigDecimal settledAmount;

	public String getInvoceNumber() {
		return invoceNumber;
	}

	public void setInvoceNumber(String invoceNumber) {
		this.invoceNumber = invoceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

}
