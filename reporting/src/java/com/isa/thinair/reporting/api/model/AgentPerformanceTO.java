package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class AgentPerformanceTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7386972364244260914L;
	private String agentName;
	private String agentCode;
	private String agentLocation;
	private String amount;
	private String address1;
	private String address2;
	private String city;
	private String country;
	private String telNo;
	private String email;
	private String contact;
	private String lastSale;
	private String yearlySales;

	private boolean bestPerfoming;
	private boolean lowPerforming;

	private String target;
	private Long actualSales;
	private String ytdSales;
	private String ytdSalesVariance;
	private String regionName;
	private String strActualSales;
	private BigDecimal salesValue;

	public AgentPerformanceTO() {
		super();
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentLocation() {
		return agentLocation;
	}

	public void setAgentLocation(String agentLocation) {
		this.agentLocation = agentLocation;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getLastSale() {
		return lastSale;
	}

	public void setLastSale(String lastSale) {
		this.lastSale = lastSale;
	}

	public boolean isBestPerfoming() {
		return bestPerfoming;
	}

	public void setBestPerfoming(boolean bestPerfoming) {
		this.bestPerfoming = bestPerfoming;
	}

	public boolean isLowPerforming() {
		return lowPerforming;
	}

	public void setLowPerforming(boolean lowPerforming) {
		this.lowPerforming = lowPerforming;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Long getActualSales() {
		return actualSales;
	}

	public void setActualSales(Long actualSales) {
		this.actualSales = actualSales;
	}

	public String getYtdSales() {
		return ytdSales;
	}

	public void setYtdSales(String ytdSales) {
		this.ytdSales = ytdSales;
	}

	public String getYtdSalesVariance() {
		return ytdSalesVariance;
	}

	public void setYtdSalesVariance(String ytdSalesVariance) {
		this.ytdSalesVariance = ytdSalesVariance;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getStrActualSales() {
		return strActualSales;
	}

	public void setStrActualSales(String strActualSales) {
		this.strActualSales = strActualSales;
	}

	public BigDecimal getSalesValue() {
		return salesValue;
	}

	public void setSalesValue(BigDecimal salesValue) {
		this.salesValue = salesValue;
	}

	public String getYearlySales() {
		return yearlySales;
	}

	public void setYearlySales(String yearlySales) {
		this.yearlySales = yearlySales;
	}

}
