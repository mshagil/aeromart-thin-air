package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

public class MISOutstandingInvoicesTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3067714299919062239L;
	private String agentName;
	private String invoiceNumber;
	private String invoiceDate;
	private String invoiceAmount;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
}
