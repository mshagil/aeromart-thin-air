package com.isa.thinair.reporting.api.model;

import java.io.Serializable;

public class FlaGraphBarBreakDwnDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7121452749312292027L;

	private String agentName;
	private String agentCode;
	private String pnr;
	private String status;
	private Integer totPaxCnt;
	private Integer totInfCnt;
	private String totCharges;
	private String totPayments;
	private String lastModDate;

	public String getAgentName() {
		return agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public String getPnr() {
		return pnr;
	}

	public String getTotCharges() {
		return totCharges;
	}

	public String getTotPayments() {
		return totPayments;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setTotCharges(String totCharges) {
		this.totCharges = totCharges;
	}

	public void setTotPayments(String totPayments) {
		this.totPayments = totPayments;
	}

	public String getLastModDate() {
		return lastModDate;
	}

	public void setLastModDate(String lastModDate) {
		this.lastModDate = lastModDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getTotPaxCnt() {
		return totPaxCnt;
	}

	public Integer getTotInfCnt() {
		return totInfCnt;
	}

	public void setTotPaxCnt(Integer totPaxCnt) {
		this.totPaxCnt = totPaxCnt;
	}

	public void setTotInfCnt(Integer totInfCnt) {
		this.totInfCnt = totInfCnt;
	}

}
