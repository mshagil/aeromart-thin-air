package com.isa.thinair.reporting.api.model;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Ruckman Colins S.
 * @since Feb 15, 2008 , 12:32:46 PM.
 */
public class InventoryAllocationTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6241332939384225197L;

	private String flightNumber;

	private Calendar depatureDate;

	private String cabinClass;

	private Calendar messageDate;

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public Calendar getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Calendar depatureDate) {
		this.depatureDate = depatureDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Calendar getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(Calendar messageDate) {
		this.messageDate = messageDate;
	}

}
