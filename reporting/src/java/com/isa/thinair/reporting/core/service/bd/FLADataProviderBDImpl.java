package com.isa.thinair.reporting.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.reporting.api.service.FLADataProviderBD;

/**
 * @author harsha
 */
@Remote
public interface FLADataProviderBDImpl extends FLADataProviderBD {

}