/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.reporting.core.persistent.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;
import javax.sql.RowSet;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.rowset.OracleCachedRowSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airreservation.api.dto.HalaServiceDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.PaymentMode;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reporting.core.persistent.util.ReportsStatementCreator;
import com.isa.thinair.reporting.core.persistent.util.StoredProcedureCall;
import com.isa.thinair.reporting.core.util.LookupUtils;

/**
 * Class to load report data
 * 
 * @author Vinothini
 */
public class ReportsDAO {

	private static final String SQL_PERFORMANCE_SALES_STAFF_DETAIL = "performanceOfSalesStaffDetail";
	private static final String SQL_CALLCENTRE_REFUND = "callCentreRefund";
	private static final String SQL_CUSTOMER_PROFILE_DETAIL = "customerProfileDetail";
	private static final String SQL_TOP_AGENTS = "topAgents";
	private static final String SQL_TOP_SEGMENTS = "topSegments";
	private static final String SQL_SALES_CHANNEL_DESC = "sale_channel_desc";

	/** Default Data Source to get the report data */
	private DataSource defaultDataSource;

	/** Reporting datasource for non live reports */
	private DataSource rptDataSource;

	/** properties to store queries for reports **/
	private Properties queryString;

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Gets the Schedule capacity Varience Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report data
	 */
	public RowSet getInboundOutboundFlightScheduleData(ReportsSearchCriteria reportsSearchCriteria) {
		try {
			// sets TRANSACTION READ ONLY, if the module config specifies
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
			map.put("toDate", reportsSearchCriteria.getDateRangeTo());
			map.put("flightNumber", reportsSearchCriteria.getFlightNumber());
			map.put("operationType", reportsSearchCriteria.getOperationType());
			map.put("origin", reportsSearchCriteria.getSectorFrom());
			map.put("destination", reportsSearchCriteria.getSectorTo());
			map.put("status", reportsSearchCriteria.getBuildStatus());

			try {
				String sql = new ReportsStatementCreator().getInbounOutboundScheduleQuery(reportsSearchCriteria);
				SqlParameter spParam[] = new SqlParameter[8];
				spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
				spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
				spParam[2] = new SqlParameter("flightNumber", Types.VARCHAR);
				spParam[3] = new SqlParameter("operationType", Types.VARCHAR);
				spParam[4] = new SqlParameter("origin", Types.VARCHAR);
				spParam[5] = new SqlParameter("destination", Types.VARCHAR);
				spParam[6] = new SqlParameter("status", Types.VARCHAR);

				spParam[7] = new SqlOutParameter("cursor", OracleTypes.CURSOR, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							rowSet.populate(rs);
							log.debug("RowSet populated");
						}
						return null;
					}
				});

				StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()),
						sql, spParam);
				spCall.callStoredProc(map);

			} catch (Exception e) {
				log.error("Exception in stored proc ", e);
			}
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	/**
	 * Gets the Schedule Capacity Varience Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getScheduleCapacityVarienceData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getScheduleCapacityVarienceQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Forward Booking Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report data
	 */
	public RowSet getViewSeatInventoryFareMovements(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getViewSeatInventoryFareMovements(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Flights Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getFlightDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getFlightDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the AVS seats Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAvsSeat(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAvsSeatQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Flight Schedules Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getScheduleDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getSceduleDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	/**
	 * Gets the Issued Voucher Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getIssuedVoucherDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getIssuedVoucherDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	/**
	 * Gets the Redeemed Voucher Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getRedeemedVoucherDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getRedeemedVoucherDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}


	/**
	 * Gets the GSA/Agent Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgetGSAData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentGSADataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Meals Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getMealsSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getMealsSummaryDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getPassengersMealSummary(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getPassengerMealSummary(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the External Payment Reconciliation Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getExtPmtReconcilSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getExtPmtReconcilSummaryDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the USer/Agent Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentUserData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentUserDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the USer Privileges Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getUserPrivilegeData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getUserPrivilegeDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Cancel Reservation Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCnxReservationDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCnxreservationQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Acquired Credit Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAcquiredCreditDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAcquiredCreditQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Available Credit Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAvailableCreditDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAvailableCreditQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * gets the performance of Sales Staff summary Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getPerformanceOfSalesStaffSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getPerformanceOfSalesStaffSummaryQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Performance of sales staff Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getPerformanceOfSalesStaffDetailData(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = queryString.getProperty(SQL_PERFORMANCE_SALES_STAFF_DETAIL);
		Collection paymentTypes = reportsSearchCriteria.getPaymentTypes();

		String sqlToAdd = "";
		Iterator iter = paymentTypes.iterator();
		if (paymentTypes != null) {
			iter = paymentTypes.iterator();
		}

		if (paymentTypes != null && paymentTypes.size() == 2) {
			sqlToAdd = " AND EXISTS (  SELECT 'X' "
					+ " FROM t_pax_transaction t1 "
					+ " WHERE t1.pnr_pax_id = p1.pnr_pax_id "
					+ " AND t1.nominal_code IN (19, 18, 15, 16, 28, 17, 6) "
					// + " AND (t1.PAYMENT_CARRIER_CODE is null or t1.PAYMENT_CARRIER_CODE = '"
					// + AppSysParamsUtil.getDefaultCarrierCode() + "') " // Remove this since only origin user should
					// check
					+ "AND ABS(t1.AMOUNT) > 0 " + "   UNION" + " SELECT 'X' " + " FROM t_pax_ext_carrier_transactions exttnx "
					+ " WHERE exttnx.pnr_pax_id = p1.pnr_pax_id "
					+ " AND exttnx.nominal_code IN (19, 18, 15, 16, 28, 17, 6) AND ABS(exttnx.AMOUNT) > 0 " + ")";

		} else {

			String paymentOption = null;
			while (iter.hasNext()) {

				paymentOption = (String) iter.next();

				// fully paid booking
				if ("1".equals(paymentOption)) {

					sqlToAdd = " AND EXISTS (select 'X' from t_pax_transaction t "
							+ " where r1.status='CNF' "
							// + " and (T.PAYMENT_CARRIER_CODE is null or T.PAYMENT_CARRIER_CODE = '"
							// + AppSysParamsUtil.getDefaultCarrierCode() + "') " // Remove this since only origin user
							// should check
							+ " and  p1.pnr_pax_id=t.pnr_pax_id and p1.pnr=r1.pnr group by p1.pnr having sum(t.amount)<=0"
							+ " UNION " + " SELECT 'X' " + " FROM t_pax_ext_carrier_transactions exttnx "
							+ " WHERE r1.status  ='CNF' " + " AND p1.pnr_pax_id=exttnx.pnr_pax_id "
							+ " AND p1.pnr       =r1.pnr " + " GROUP BY P1.PNR " + " HAVING SUM(exttnx.AMOUNT)<=0 " + ")";

				}

				// partially paid booking
				if ("2".equals(paymentOption)) {

					sqlToAdd = " AND EXISTS (select 'X' from t_pax_transaction t " + " where r1.status='CNF' "
					// + " and (T.PAYMENT_CARRIER_CODE is null or T.PAYMENT_CARRIER_CODE = '"
					// + AppSysParamsUtil.getDefaultCarrierCode() + "') " // Remove this since only origin user should
					// check
							+ " and p1.pnr_pax_id=t.pnr_pax_id and p1.pnr=r1.pnr group by p1.pnr having sum(t.amount)>0)";

				}

			}
		}

		String newSql = "";
		newSql = sql.replace("{0}", sqlToAdd);

		Object[] parameters = new Object[] { reportsSearchCriteria.getUserId(), reportsSearchCriteria.getAgentCode(),
				reportsSearchCriteria.getDateFrom(), reportsSearchCriteria.getDateTo() };
		int[] dataTypes = new int[] { Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP };
		return getReportData(newSql, parameters, dataTypes, reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets Mode of Payment Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCallCentreModeOfPaymentsData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getModeofPaymentQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Refund Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCallCentreRefundData(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = queryString.getProperty(SQL_CALLCENTRE_REFUND);
		Object[] parameters = new Object[] { reportsSearchCriteria.getDateFrom(), reportsSearchCriteria.getDateTo(),
				AppSysParamsUtil.getDefaultCarrierCode(), reportsSearchCriteria.getDateFrom(), reportsSearchCriteria.getDateTo() };
		int[] dataTypes = new int[] { Types.TIMESTAMP, Types.TIMESTAMP, Types.CHAR, Types.TIMESTAMP, Types.TIMESTAMP };
		return getReportData(sql, parameters, dataTypes, reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Travel History report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCustomerTravelHistoryDetailData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCustomerTravelHistoryDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Customer Profile Data Report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCustomerProfileSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		RowSet rowSet = null;
		if (reportsSearchCriteria.isByNationality()) {
			rowSet = getReportData(new ReportsStatementCreator().getCustomerProfileNationality(reportsSearchCriteria),
					reportsSearchCriteria.getDataSoureType());
		} else if (reportsSearchCriteria.isByCountryOfResidence()) {
			rowSet = getReportData(new ReportsStatementCreator().getCustomerProfileCountry(reportsSearchCriteria),
					reportsSearchCriteria.getDataSoureType());
		} else {
			rowSet = getReportData(new ReportsStatementCreator().getCustomerProfile(reportsSearchCriteria),
					reportsSearchCriteria.getDataSoureType());
		}
		return rowSet;
	}

	/**
	 * Gets the Web profile Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getWebProfilesData(ReportsSearchCriteria reportsSearchCriteria) {

		RowSet rowSet = null;
		rowSet = getReportData(new ReportsStatementCreator().getWebProfilesData(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
		return rowSet;
	}

	/**
	 * Gets the Customer Profile Detail Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCustomerProfileDetailData(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = queryString.getProperty(SQL_CUSTOMER_PROFILE_DETAIL);
		Object[] parameters = new Object[] { reportsSearchCriteria.getCustomerId(), reportsSearchCriteria.getCustomerId() };
		int[] dataTypes = new int[] { Types.VARCHAR, Types.VARCHAR };
		return getReportData(sql, parameters, dataTypes, reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Enplanement Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getEnplanementData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getEnplanementQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Top Agents Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getTopAgentsData(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = queryString.getProperty(SQL_TOP_AGENTS);
		String segmentLike = reportsSearchCriteria.getSectorFrom() + "%" + reportsSearchCriteria.getSectorTo();
		String flightTypeLike = "";
		if (reportsSearchCriteria.getFlightType() != null) {
			flightTypeLike = reportsSearchCriteria.getFlightType();
		} else {
			flightTypeLike = "%";
		}
		Object[] parameters = new Object[] { flightTypeLike, segmentLike,
				reportsSearchCriteria.getDateFrom(), reportsSearchCriteria.getDateTo(),
				new Integer(reportsSearchCriteria.getNoOfTops()) };
		int[] dataTypes = new int[] { Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.INTEGER };
		return getReportData(sql, parameters, dataTypes, reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Top Segment Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getTopSegmentsData(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = queryString.getProperty(SQL_TOP_SEGMENTS);
		sql = sql.replace("$param$1",
				new String(reportsSearchCriteria.getSalesChannels().toString().replace("[", "").replace("]", "")));
		Object[] parameters = new Object[] { reportsSearchCriteria.getDateFrom(), reportsSearchCriteria.getDateTo(),
				new Integer(reportsSearchCriteria.getNoOfTops()) };
		int[] dataTypes = new int[] { Types.TIMESTAMP, Types.TIMESTAMP, Types.INTEGER };
		return getReportData(sql, parameters, dataTypes, reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Segment Contribution By Agent Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getSegmentContributionByAgentsByPOSData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getPOSContributionData(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

		// sql = queryString.getProperty(SQL_SEGMENT_CONTRIBUTION_BY_AGENTS);
		// parameters = new Object[] {
		// reportsSearchCriteria.getSectorFrom(),
		// reportsSearchCriteria.getSectorTo(),
		// reportsSearchCriteria.getDateFrom(),
		// reportsSearchCriteria.getDateTo()};
		// dataTypes = new int[] {Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP,
		// Types.TIMESTAMP};
		// return getReportData(sql, parameters, dataTypes, reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCountryContributionPerFlightSegmentData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(
				new ReportsStatementCreator().getCountryContributionPerFlightSegmentData(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Passenger Contact Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getPaxContactDetailsData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getPaxContactDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Onhold Passengers Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getOnholdPassengersData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getOnholdPassengersQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the get Agent Charge Adjustments Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentChargeAdjustmentsData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getShowAgentChargeAdjustmentsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getIBEOnholdPassengersData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getIBEOnholdPassengersQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Reservation Break Down Summary Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getReservationBreakdownSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getReservationBreakdownSummaryQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Company Payment Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCompanyPaymentQuery(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCompanyPaymentQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Company Payment Report Data By Currency
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCompanyPaymentCurrencyQuery(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCompanyPaymentQueryByCurrency(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Company Payment By Pos Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCompanyPaymentPOSQuery(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCompanyPaymentPOSQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Resevation Break down Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getReservationBreakdownDetailData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getReservationBreakdownDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Customer Existing Credit Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCustomerExistingCreditData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCustomerExistingCreditDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Tax & Surcharge Data for the Given Search
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAirportTaxData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAirportTaxQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Fare Discounts Data for the Given Search
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getFareDiscountsData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getFareDiscountsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Agents Commision Data for the Given Search
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentCommisionData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentCommisionQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Reservation Break Down Tax Report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getReservationBreakdownTaxesData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getReservationBreakdownTaxesQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Outstanding balance Summary Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getOutstandingBalanceSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getOutstandingBalanceSummaryQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Outstanding Balance Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getOutstandingBalanceDetailData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getOutstandingBalanceDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Invoice Summary Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInvoiceSummaryData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInvoiceSummaryQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Invoice Summary Report Data By Currency
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInvoiceSummaryDataByCurrency(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInvoiceSummaryQueryByCurrency(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Agent Transaction Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentTransactionData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentTransactionDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the OND Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public void getONDData(ReportsSearchCriteria reportsSearchCriteria) {

		// sets TRANSACTION READ ONLY, if the module config specifies
		setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
		map.put("toDate", reportsSearchCriteria.getDateRangeTo());
		map.put("usrId", reportsSearchCriteria.getUserId());
		map.put("stations", reportsSearchCriteria.getStation());
		map.put("segments", reportsSearchCriteria.getSegment());
		map.put("bcs", reportsSearchCriteria.getBookingClass());
		map.put("agents", reportsSearchCriteria.getAgentCode());
		map.put("rptFormat", reportsSearchCriteria.getReqReportFormat());

		try {

			String sql = new ReportsStatementCreator().getONDDataQuery();
			SqlParameter spParam[] = new SqlParameter[8];
			spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
			spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
			spParam[2] = new SqlParameter("usrId", Types.VARCHAR);
			spParam[3] = new SqlParameter("stations", Types.VARCHAR);
			spParam[4] = new SqlParameter("segments", Types.VARCHAR);
			spParam[5] = new SqlParameter("bcs", Types.VARCHAR);
			spParam[6] = new SqlParameter("agents", Types.VARCHAR);
			spParam[7] = new SqlParameter("rptFormat", Types.VARCHAR);

			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()), sql,
					spParam);
			spCall.callStoredProc(map);

		} catch (Exception e) {
			log.error("Exception in revenue proc " + e.getMessage());
		}
		// return rowSetInb;

	}

	/**
	 * Gets the Row set for s Given Sql & Data Source
	 * 
	 * @param sql
	 *            the SQL
	 * @param dataSourceType
	 *            the Data Source
	 * @return RowSet for the Sql
	 */
	private RowSet getReportData(String sql, String dataSourceType) {
		try {
			// sets TRANSACTION READ ONLY, if the module config specifies
			setTransactionReadOnly(dataSourceType);
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
			log.debug("SQL is [" + sql + "]");
			jdbcTemplate.query(sql, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						rowSet.populate(rs);
						log.debug("RowSet populated");
					}
					return null;
				}
			});
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	private Map<String, ResultSet> getReportData(Map<String, String> sqls, String dataSourceType) {
		final Map<String, ResultSet> reportDataMap = new HashMap<>();

		try {
			setTransactionReadOnly(dataSourceType);
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));

			for (Map.Entry<String, String> entry : sqls.entrySet()) {

				log.debug("SQL is [" + entry.getValue() + "]");
				jdbcTemplate.query(entry.getValue(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							OracleCachedRowSet rowSet = new OracleCachedRowSet();
							rowSet.populate(rs);
							reportDataMap.put(entry.getKey(), rowSet);
							log.debug("RowSet populated");
						}
						return null;
					}
				});
			}

			return reportDataMap;
		} catch (Exception e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	/**
	 * 
	 * Gets pax count for PaxStatusReport
	 * 
	 * @param sql
	 * @param dataSourceType
	 * @return Map<String, String>
	 */
	private Map<String, Integer> getPaxCountsForPaxStatusReport(String sql, String dataSourceType) {
		setTransactionReadOnly(dataSourceType);
		final Map<String, Integer> paxCountsMap = new LinkedHashMap<String, Integer>();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
		log.debug("SQL is [" + sql + "]");
		jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {

					while (rs.next()) {
						String paxGender = rs.getString("GENDER");
						int genderCount = rs.getInt("GENDER_COUNT");
						paxCountsMap.put(paxGender, genderCount);
					}

				}
				return null;
			}
		});
		return paxCountsMap;
	}

	/**
	 * 
	 * Gets total pax count for BookingCountDetailsReport
	 * 
	 * @param sql
	 * @param dataSourceType
	 * @return
	 */
	private Integer getPaxTotalForBookingCountDetailsReport(String sql, String dataSourceType) {
		setTransactionReadOnly(dataSourceType);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
		log.debug("SQL is [" + sql + "]");

		Integer paxTotal = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer total = 0;
				if (rs != null) {
					while (rs.next()) {
						total = rs.getInt("TOTAL_COUNT");
					}
				}
				return total;
			}
		});

		return paxTotal;
	}

	/**
	 * Returns the summary of booked hala services used for scheduler notification.
	 * 
	 * @param sql
	 * @param dataSourceType
	 * @return Colletion of HalaServiceDTO's
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<HalaServiceDTO> getBookedHalaServices(String sql, String dataSourceType) {
		setTransactionReadOnly(dataSourceType);
		ArrayList<HalaServiceDTO> halaDTOs = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
		log.debug("SQL is [" + sql + "]");
		halaDTOs = (ArrayList) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<HalaServiceDTO> resultsList = new ArrayList<HalaServiceDTO>();
				if (rs != null) {
					String hubAirPort = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
					while (rs.next()) {
						HalaServiceDTO halaServiceDTO = new HalaServiceDTO();
						Timestamp ts = rs.getTimestamp("DEPLOCAL");
						String depLocal = CalendarUtil.getDateInFormattedString("dd-MMM-yy HH:mm",
								new java.util.Date(ts.getTime()));
						Timestamp ts2 = rs.getTimestamp("ARRVLOCAL");
						String arrvLocal = CalendarUtil.getDateInFormattedString("dd-MMM-yy HH:mm",
								new java.util.Date(ts2.getTime()));
						Timestamp ts3 = rs.getTimestamp("DEPZULU");
						Timestamp ts4 = rs.getTimestamp("ARRZULU");

						halaServiceDTO.setDepartureDate(new java.util.Date(ts.getTime()));
						halaServiceDTO.setStrDepLocal(depLocal);
						halaServiceDTO.setFlightNo(rs.getString("FLIGHTNO"));
						halaServiceDTO.setOrigin(rs.getString("AIRPORT"));
						halaServiceDTO.setDestination(rs.getString("DESTINATION"));
						halaServiceDTO.setArrivalDate(new java.util.Date(ts2.getTime()));
						halaServiceDTO.setStrArrivLocal(arrvLocal);
						halaServiceDTO.setDepartureZulu(new java.util.Date(ts3.getTime()));
						halaServiceDTO.setArrivalZulu(new java.util.Date(ts4.getTime()));
						halaServiceDTO.setEmail(rs.getString("email"));
						halaServiceDTO.setChargeCode(rs.getString("charge_code"));
						halaServiceDTO.setChargeDescription(rs.getString("charge_description"));
						halaServiceDTO.setNoOfPax(rs.getInt("ADS") + rs.getInt("CHS")); // Removed Infants
						halaServiceDTO.setSsrChargeId(rs.getInt("ssr_charge_id"));
						halaServiceDTO.setHubAirPort(hubAirPort);
						if (rs.getTimestamp("DEPTIME") != null) {
							Timestamp ts5 = rs.getTimestamp("DEPTIME");
							halaServiceDTO.setApplicableTime(CalendarUtil.getDateInFormattedString("dd-MMM-yy HH:mm",
									new java.util.Date(ts5.getTime())));
							halaServiceDTO.setApplyOn("D");
						}
						if (rs.getTimestamp("ARRIVALTIME") != null) {
							Timestamp ts5 = rs.getTimestamp("ARRIVALTIME");
							halaServiceDTO.setApplicableTime(CalendarUtil.getDateInFormattedString("dd-MMM-yy HH:mm",
									new java.util.Date(ts5.getTime())));
							halaServiceDTO.setApplyOn("A");
						}
						halaServiceDTO.setApplicableAirport(rs.getString("serviceairport"));

						resultsList.add(halaServiceDTO);
					}

					return resultsList;
				} else {
					return null;
				}
			}
		});
		return halaDTOs;
	}

	/**
	 * Returns the summary of booked hala services used for scheduler notification.
	 * 
	 * @param sql
	 * @param dataSourceType
	 * @return Colletion of HalaServiceDTO's
	 */
	@SuppressWarnings("unchecked")
	private ArrayList<HalaServiceDTO> getBookedHalaServicesDetails(String sql, String dataSourceType) {

		setTransactionReadOnly(dataSourceType);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
		log.debug("SQL is [" + sql + "]");
		ArrayList<HalaServiceDTO> halaDTOs = (ArrayList) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<HalaServiceDTO> resultsList = new ArrayList<HalaServiceDTO>();

				if (rs != null) {
					String hubAirPort = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
					while (rs.next()) {
						HalaServiceDTO halaServiceDTO = new HalaServiceDTO();

						halaServiceDTO.setPnr(rs.getString("pnr"));
						halaServiceDTO.setFlightNo(rs.getString("flightno"));
						halaServiceDTO.setFullName(rs.getString("fullname"));
						halaServiceDTO.setSegCode(rs.getString("seg_code"));
						halaServiceDTO.setOrigin(rs.getString("origin"));
						halaServiceDTO.setDestination(rs.getString("destination"));

						DateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");

						try {
							halaServiceDTO.setDepartureDate(formatter.parse(rs.getString("depdate")));
							halaServiceDTO.setArrivalDate(formatter.parse(rs.getString("arrivaldate")));
						} catch (ParseException e) {
							log.error("Exception in Date Parse : " + e.getMessage());
						}

						halaServiceDTO.setSsrCode(rs.getString("ssr_code"));
						halaServiceDTO.setSsrDescription(rs.getString("ssr_desc"));
						halaServiceDTO.setStation(rs.getString("airport"));
						halaServiceDTO.setHubAirPort(hubAirPort);
						halaServiceDTO.setFlightSegmentId(rs.getString("flt_segment"));
						halaServiceDTO.setSsrId(rs.getString("ssr_id"));
						halaServiceDTO.setSsrCategoryId(rs.getInt("SSR_CAT_ID"));
						halaServiceDTO.setPpssId(rs.getLong("PPSS_ID"));

						resultsList.add(halaServiceDTO);
					}
				}

				return resultsList;
			}
		});
		return halaDTOs;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<PaxSSRDTO> getBookedHalaServicePaxDetails(String sql, String dataSourceType) {
		ArrayList<PaxSSRDTO> serviceBookedPaxList = null;
		setTransactionReadOnly(dataSourceType);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
		log.debug("SQL is [" + sql + "]");
		serviceBookedPaxList = (ArrayList) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					ArrayList<PaxSSRDTO> halaPaxList = new ArrayList<PaxSSRDTO>();
					while (rs.next()) {
						PaxSSRDTO paxSSRDTO = new PaxSSRDTO();
						paxSSRDTO.setPassenger(rs.getString("FULLNAME"));
						paxSSRDTO.setPnr(rs.getString("PNR"));
						Timestamp ts = rs.getTimestamp("SERVICEBOOKEDTIME");
						paxSSRDTO.setBookedTime(CalendarUtil.getDateInFormattedString("dd-MMM-yy HH:mm", new Date(ts.getTime())));
						paxSSRDTO.setChargeAmount(rs.getDouble("amount"));
						paxSSRDTO.setContactNo(rs.getString("contactNo"));
						paxSSRDTO.setSsrText(rs.getString("description"));
						halaPaxList.add(paxSSRDTO);
					}
					return halaPaxList;
				} else {
					return null;
				}
			}
		});
		return serviceBookedPaxList;
	}

	/**
	 * Gets the Row set for a give SQL & paramerts set
	 * 
	 * @param sql
	 *            the SQL
	 * @param parameters
	 *            the input Parameter for the sql
	 * @param dataTypes
	 *            the input data types
	 * @param dataSourceType
	 *            the Data source
	 * @return RowSet the Rowset after executing the sql
	 */
	private RowSet getReportData(String sql, Object[] parameters, int[] dataTypes, String dataSourceType) {
		try {
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			// sets TRANSACTION READ ONLY, if the module config specifies
			setTransactionReadOnly(dataSourceType);

			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(dataSourceType));
			log.debug("SQL is [" + sql + "]");
			jdbcTemplate.query(sql, parameters, dataTypes, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						rowSet.populate(rs);
						log.debug("RowSet populated");
					}
					return null;
				}
			});
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	/**
	 * Gets the payment Modes
	 * 
	 * @return Collection the Collection of payment modes
	 */
	public Collection getPaymentsMode() {
		JdbcTemplate jt = new JdbcTemplate(getDataSource(null));

		// Nili 5:43 PM 11/20/2007 Refer THINAIR-861
		String sql = " SELECT NOMINAL_CODE, DESCRIPTION FROM T_PAX_TRNX_NOMINAL_CODE a, T_CREDITCARD_TYPE b "
				+ " WHERE a.NOMINAL_CODE IN ("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getReportsPaymentTypeNominalCodes())
				+ ") AND b.PAYMENT_NC(+) = a.NOMINAL_CODE " + " AND (b.VISIBILITY is null OR b.VISIBILITY = 1) ";

		Collection payments = (Collection) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				PaymentMode paymentMode;
				Collection<PaymentMode> paymentModes = new HashSet<>();

				if (rs != null) {
					while (rs.next()) {
						paymentMode = new PaymentMode();
						paymentMode.setPaymentCode(rs.getString(1));
						paymentMode.setPaymentDescription(rs.getString(2));
						paymentModes.add(paymentMode);
					}
				}

				return paymentModes;
			}
		});

		return payments;
	}

	public Collection getRoleForPrivileges(ReportsSearchCriteria reportsSearchCriteria) {
		JdbcTemplate jt = new JdbcTemplate(getDataSource(null));

		StringBuilder sb = new StringBuilder();
		sb.append(" select distinct rp.role_id ");
		sb.append(" from t_role_privilege rp ");
		sb.append(" where ( ");

		List<String> privilegeList = new ArrayList<String>(reportsSearchCriteria.getPrivileges());

		while (privilegeList.size() > 1000) {
			List subList = privilegeList.subList(0, 1000);
			privilegeList = privilegeList.subList(1000, privilegeList.size());
			sb.append(" rp.privilege_id IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (privilegeList.size() <= 1000) {
			sb.append(" rp.privilege_id IN( " + Util.buildStringInClauseContent(privilegeList) + " )) ");
		}

		Collection roleForPrivileges = (Collection) jt.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> roles = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {
						String roleID = rs.getString("ROLE_ID");
						roles.add(roleID);
					}
				}
				return roles;
			}
		});

		return roleForPrivileges;
	}

	/**
	 * Gets the USer/Agent/Roles Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public Map getAgentUserRoleData(ReportsSearchCriteria reportsSearchCriteria) {

		JdbcTemplate jt = new JdbcTemplate(getDataSource(null));

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT U.USER_ID, R.ROLE_NAME ");
		sb.append(" FROM T_ROLE R, T_USER_ROLE U ");
		sb.append(" WHERE R.ROLE_ID = U.ROLE_ID AND ( ");
		// sb.append("       R.STATUS = 'ACT' AND ( ");

		List<String> userList = new ArrayList<String>(reportsSearchCriteria.getUsers());

		while (userList.size() > 1000) {
			List subList = userList.subList(0, 1000);
			userList = userList.subList(1000, userList.size());
			sb.append("       U.USER_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (userList.size() <= 1000) {
			sb.append("       U.USER_ID IN( " + Util.buildStringInClauseContent(userList) + " )) ");
		}

		if (reportsSearchCriteria.getRoles() != null) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( R.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");
		}

		Map userRoleMap = (HashMap) jt.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Map userRoleMap = new HashMap();

				Map recData = null;
				Collection records = null;

				while (rs.next()) {
					recData = new HashMap();
					String userID = rs.getString("USER_ID");

					recData.put("USER_ID", userID);
					recData.put("ROLE_NAME", rs.getString("ROLE_NAME"));

					records = (Collection) userRoleMap.get(userID);

					if (records == null) {
						records = new ArrayList();
					}
					records.add(recData);

					userRoleMap.put(userID, records);
					records = null;
				}

				Collection keyList = userRoleMap.keySet();

				for (Iterator iter = keyList.iterator(); iter.hasNext();) {
					String key = (String) iter.next();
					Collection recList = (Collection) userRoleMap.get(key);
					// userRoleMap.put(key, new JRMapCollectionDataSource(recList));
					userRoleMap.put(key, recList);
				}

				return userRoleMap;
			}
		});
		return userRoleMap;
	}

	/**
	 * Gets the inbound Out Bound Connection Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInbDatas(ReportsSearchCriteria reportsSearchCriteria) {
		try {
			// sets TRANSACTION READ ONLY, if the module config specifies
			setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			HashMap<String, Object> map = new HashMap<String, Object>();

			int conTime = Integer.parseInt(reportsSearchCriteria.getConTime()) * 60;
			Integer intCon = new Integer(conTime);
			map.put("fromDate", reportsSearchCriteria.getDateFrom());
			map.put("toDate", reportsSearchCriteria.getDateTo());
			map.put("segment", reportsSearchCriteria.getCountryOfResidence());
			map.put("flightNo", reportsSearchCriteria.getFlightNumber());
			map.put("con", intCon);
			map.put("IO", reportsSearchCriteria.getReportOption());

			try {

				String sql = new ReportsStatementCreator().getInbounOutboundConnectionsQuery(reportsSearchCriteria);
				SqlParameter spParam[] = new SqlParameter[7];
				spParam[0] = new SqlParameter("fromDate", Types.TIMESTAMP);
				spParam[1] = new SqlParameter("toDate", Types.TIMESTAMP);
				spParam[2] = new SqlParameter("segment", Types.VARCHAR);
				spParam[3] = new SqlParameter("flightNo", Types.VARCHAR);
				spParam[4] = new SqlParameter("IO", Types.VARCHAR);
				spParam[5] = new SqlParameter("con", Types.INTEGER);
				spParam[6] = new SqlOutParameter("cursor", OracleTypes.CURSOR, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							rowSet.populate(rs);
							log.debug("RowSet populated");
						}
						return null;
					}
				});

				StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()),
						sql, spParam);
				spCall.callStoredProc(map);

			} catch (Exception e) {
				log.error("Exception in getInbDatas " + e.getMessage());
			}
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	/**
	 * Gets the Revenue & Tax report Data for REVENUE_TAX_REPORT_OPTION_DETAIL
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public void getROPRevenueData(ReportsSearchCriteria reportsSearchCriteria) {

		// sets TRANSACTION READ ONLY, if the module config specifies
		if (log.isDebugEnabled())
			log.debug("Started Revenue proc execution");

		setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
		map.put("toDate", reportsSearchCriteria.getDateRangeTo());
		map.put("usrId", reportsSearchCriteria.getUserId());
		map.put("stations", reportsSearchCriteria.getStation());
		map.put("segments", reportsSearchCriteria.getSegment());
		map.put("bcs", reportsSearchCriteria.getBookingClass());
		map.put("agents", reportsSearchCriteria.getAgentCode());
		map.put("carrierCode", reportsSearchCriteria.getCarrierCode());
		map.put("rptFormat", reportsSearchCriteria.getReqReportFormat());
		map.put("chargeCodes", reportsSearchCriteria.getCharge());
		map.put("chargeGroupCodes", reportsSearchCriteria.getChargeGrooup());
		map.put("bookFromDate", reportsSearchCriteria.getDateRange2From());
		map.put("bookToDate", reportsSearchCriteria.getDateRange2To());
		map.put("includeCabinClass", reportsSearchCriteria.getCos());
		
		try {

			String sql = new ReportsStatementCreator().getRevenueProcQuery(reportsSearchCriteria);
			SqlParameter spParam[] = new SqlParameter[14];
			spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
			spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
			spParam[2] = new SqlParameter("usrId", Types.VARCHAR);
			spParam[3] = new SqlParameter("stations", Types.VARCHAR);
			spParam[4] = new SqlParameter("segments", Types.VARCHAR);
			spParam[5] = new SqlParameter("bcs", Types.VARCHAR);
			spParam[6] = new SqlParameter("agents", Types.VARCHAR);
			spParam[7] = new SqlParameter("carrierCode", Types.VARCHAR);
			spParam[8] = new SqlParameter("rptFormat", Types.VARCHAR);
			spParam[9] = new SqlParameter("chargeCodes", Types.VARCHAR);
			spParam[10] = new SqlParameter("chargeGroupCodes", Types.VARCHAR);
			spParam[11] = new SqlParameter("bookFromDate", Types.VARCHAR);
			spParam[12] = new SqlParameter("bookToDate", Types.VARCHAR);
			spParam[13] = new SqlParameter("includeCabinClass", Types.VARCHAR);
			
			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()), sql,
					spParam);
			spCall.callStoredProc(map);

		} catch (Exception e) {
			log.error("Exception in revenue proc " + e.getMessage());
		}
	}

	/**
	 * Gets the Revenue & Tax report Data for P_REVEN_SUMMARY_REPT,P_REVEN_AGENT_SUMMARY_REPT and
	 * P_REVEN_CHARGES_SUMMARY_REPT
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getROPRevenueReportData(ReportsSearchCriteria reportsSearchCriteria) {
		try {
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();

			// sets TRANSACTION READ ONLY, if the module config specifies
			setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

			// ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY
			String reportOption = reportsSearchCriteria.getReportOption();

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
			map.put("toDate", reportsSearchCriteria.getDateRangeTo());
			if (!ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equals(reportOption)) {
				map.put("usrId", reportsSearchCriteria.getUserId());
			}
			map.put("stations", reportsSearchCriteria.getStation());
			map.put("segments", reportsSearchCriteria.getSegment());
			map.put("bcs", reportsSearchCriteria.getBookingClass());
			map.put("agents", reportsSearchCriteria.getAgentCode());
			map.put("carrierCode", reportsSearchCriteria.getCarrierCode());
			map.put("rptFormat", reportsSearchCriteria.getReqReportFormat());
			if (!ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equals(reportOption)) {
				map.put("pc_cur_reqd", reportsSearchCriteria.getIsCurSerRequired());
			}
			map.put("chargeCodes", reportsSearchCriteria.getCharge());
			map.put("chargeGroupCodes", reportsSearchCriteria.getChargeGrooup());
			map.put("bookFromDate", reportsSearchCriteria.getDateRange2From());
			map.put("bookToDate", reportsSearchCriteria.getDateRange2To());

			try {

				String sql = new ReportsStatementCreator().getRevenueProcQuery(reportsSearchCriteria);
				List<SqlParameter> spParam = new ArrayList<SqlParameter>();

				spParam.add(new SqlParameter("fromDate", Types.VARCHAR));
				spParam.add(new SqlParameter("toDate", Types.VARCHAR));
				if (!ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equals(reportOption)) {
					spParam.add(new SqlParameter("usrId", Types.VARCHAR));
				}
				spParam.add(new SqlParameter("stations", Types.VARCHAR));
				spParam.add(new SqlParameter("segments", Types.VARCHAR));
				spParam.add(new SqlParameter("bcs", Types.VARCHAR));
				spParam.add(new SqlParameter("agents", Types.VARCHAR));
				spParam.add(new SqlParameter("carrierCode", Types.VARCHAR));
				spParam.add(new SqlParameter("rptFormat", Types.VARCHAR));
				if (!ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equals(reportOption)) {
					spParam.add(new SqlParameter("pc_cur_reqd", Types.VARCHAR));
				}
				spParam.add(new SqlParameter("chargeCodes", Types.VARCHAR));
				spParam.add(new SqlParameter("chargeGroupCodes", Types.VARCHAR));
				spParam.add(new SqlParameter("bookFromDate", Types.VARCHAR));
				spParam.add(new SqlParameter("bookToDate", Types.VARCHAR));
				spParam.add(new SqlOutParameter("cursor", OracleTypes.CURSOR, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							rowSet.populate(rs);
							log.debug("RowSet populated");
						}
						return null;
					}
				}));

				SqlParameter[] sqlParamArray = new SqlParameter[spParam.size()];
				spParam.toArray(sqlParamArray);

				StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()),
						sql, sqlParamArray);
				spCall.callStoredProc(map);

			} catch (Exception e) {
				log.error("Exception in revenue proc " + e.getMessage());
			}
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	/**
	 * Gets the Forward sales report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public void getForwardSalesData(ReportsSearchCriteria reportsSearchCriteria) {

		setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
		map.put("toDate", reportsSearchCriteria.getDateRangeTo());
		map.put("usrId", reportsSearchCriteria.getUserId());
		map.put("stations", reportsSearchCriteria.getStation());
		map.put("segments", reportsSearchCriteria.getSegment());
		map.put("bcs", reportsSearchCriteria.getBookingClass());
		map.put("agents", reportsSearchCriteria.getAgentCode());
		map.put("carrierCode", reportsSearchCriteria.getCarrierCode());
		map.put("rptFormat", reportsSearchCriteria.getReqReportFormat());

		try {

			String sql = new ReportsStatementCreator().getForwardSalesProcQuery(reportsSearchCriteria);
			SqlParameter spParam[] = new SqlParameter[9];
			spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
			spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
			spParam[2] = new SqlParameter("usrId", Types.VARCHAR);
			spParam[3] = new SqlParameter("stations", Types.VARCHAR);
			spParam[4] = new SqlParameter("segments", Types.VARCHAR);
			spParam[5] = new SqlParameter("bcs", Types.VARCHAR);
			spParam[6] = new SqlParameter("agents", Types.VARCHAR);
			spParam[7] = new SqlParameter("carrierCode", Types.VARCHAR);
			spParam[8] = new SqlParameter("rptFormat", Types.VARCHAR);
			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()), sql,
					spParam);
			spCall.callStoredProc(map);

		} catch (Exception e) {
			log.error("Exception in forward sales report proc " + e.getMessage());
		}

	}

	/**
	 * Gets the Sgents Productivity report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentProductivityData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentProductivityQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Sets the Transaction Read Only
	 * 
	 * @param dataSourceType
	 *            the Data source
	 */
	private void setTransactionReadOnly(String dataSourceType) {
		if (LookupUtils.getReportingConfig().isSetTransactionReadOnly()) {
			// call the proc for setting TRASACTION READ ONLY
			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(dataSourceType), "P_REPORT_SVR_RO_TXN",
					new SqlParameter[] {});
			spCall.callStoredProc(new HashMap());
			if (log.isDebugEnabled())
				log.debug("Started READ ONLY TRASACTION");
		}
	}

	/**
	 * @return - Returns the dataSource.
	 */
	public DataSource getDataSource(String type) {
		if (type == null || type.equals(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT)) {
			return defaultDataSource;
		} else {
			return rptDataSource;
		}
	}

	/**
	 * @return - Returns the dataSource.
	 */
	public DataSource getDefaultDataSource() {
		return defaultDataSource;
	}

	/**
	 * @param defaultDataSource
	 *            - The dataSource to set.
	 */
	public void setDefaultDataSource(DataSource defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}

	/**
	 * @return - Returns the rptDataSource.
	 */
	public DataSource getRptDataSource() {
		return rptDataSource;
	}

	/**
	 * @param rptDataSource
	 *            - The dataSource to set.
	 */
	public void setRptDataSource(DataSource rptDataSource) {
		this.rptDataSource = rptDataSource;
	}

	/**
	 * @return Returns - Returns the queryString.
	 */
	public Properties getQueryString() {
		return queryString;
	}

	/**
	 * @param queryString
	 *            - The queryString to set.
	 */
	public void setQueryString(Properties queryString) {
		this.queryString = queryString;
	}

	/**
	 * Gets the Invoice Summary Attachment
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInvoiceSummaryAttachment(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInvoiceSummaryAttachment(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Invoice Summary Attachment
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInvoiceSummaryAttachmentByCurrency(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInvoiceSummaryAttachmentByCurrency(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * Gets the Invoice Summary Attachment
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInvoiceSummaryAttachmentByTransaction(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInvoiceSummaryAttachmentByTransaction(
						reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getCCTransactionDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCCTransactionReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Get per day transaction fraud status.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public RowSet getPerDayCCFraudDetails(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getPerDayCCFraudDetails(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	
	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public RowSet getPerDayCCTopUpFraudDetails(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getPerDayCCTopUpFraudDetails(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	/**
	 * Returns Credit card refunds details as RowSet
	 * 
	 * @param reportsSearchCriteria
	 *            The ReportsSearchCriteria which contains search options/filter options
	 * @return RowSet the report data
	 */
	public RowSet getCCTransactionRefundDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCCTransactionRefundReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Returns Credit card pending refunds details as RowSet
	 * 
	 * @param reportsSearchCriteria
	 *            The ReportsSearchCriteria which contains search options/filter options
	 * @return RowSet the report data
	 */
	public RowSet getCCTransactionPendingRefundDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getCCTransactionPendingRefundReportQuery(
						reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getFreightDetailsReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getFreightReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Administration Audit Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAdministrationAuditData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAdministrationAuditDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Reservation Audit Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getReservationAuditData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getReservationAuditDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the PAX Status Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getPaxStatusReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getPaxStatusDetailQuery(reportsSearchCriteria, 1),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets Pax count for Pax Status Report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return Map<String, Integer>
	 */
	public Map<String, Integer> getPaxCountForPaxStatusReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getPaxCountsForPaxStatusReport(
				new ReportsStatementCreator().getPaxStatusDetailQuery(reportsSearchCriteria, 2),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Interline Sales Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInterlineSalesData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getInterlineSalesReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Interline Revenue & Tax report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public void getInterlineRevenueData(ReportsSearchCriteria reportsSearchCriteria) {

		// sets TRANSACTION READ ONLY, if the module config specifies
		setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
		map.put("toDate", reportsSearchCriteria.getDateRangeTo());
		map.put("usrId", reportsSearchCriteria.getUserId());
		map.put("stations", reportsSearchCriteria.getStation());
		map.put("segments", reportsSearchCriteria.getSegment());
		map.put("bcs", reportsSearchCriteria.getBookingClass());
		map.put("agents", reportsSearchCriteria.getAgentCode());
		map.put("carrierCode", reportsSearchCriteria.getCarrierCode());

		try {

			String sql = new ReportsStatementCreator().getInterlineRevenueProcQuery(reportsSearchCriteria);
			SqlParameter spParam[] = new SqlParameter[8];
			spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
			spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
			spParam[2] = new SqlParameter("usrId", Types.VARCHAR);
			spParam[3] = new SqlParameter("stations", Types.VARCHAR);
			spParam[4] = new SqlParameter("segments", Types.VARCHAR);
			spParam[5] = new SqlParameter("bcs", Types.VARCHAR);
			spParam[6] = new SqlParameter("agents", Types.VARCHAR);
			spParam[7] = new SqlParameter("carrierCode", Types.VARCHAR);

			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()), sql,
					spParam);
			spCall.callStoredProc(map);

		} catch (Exception e) {
			log.error("Exception in revenue proc " + e.getMessage());
		}

	}

	public RowSet getFlightLoadData(ReportsSearchCriteria reportsSearchCriteria) {
		try {
			setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			HashMap map = new HashMap();
			map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
			map.put("toDate", reportsSearchCriteria.getDateRangeTo());
			map.put("status", reportsSearchCriteria.getStatus());
			if (reportsSearchCriteria.getOperationType() != null) {
				map.put("opType", Integer.parseInt(reportsSearchCriteria.getOperationType()));
			} else {
				map.put("opType", 1);
			}

			map.put("origin", reportsSearchCriteria.getFrom());
			map.put("destination", reportsSearchCriteria.getTo());
			map.put("flightno", reportsSearchCriteria.getFlightNumber());
			if (reportsSearchCriteria.getConTime() != null) {
				map.put("conntime", Integer.parseInt(reportsSearchCriteria.getConTime()));
			} else {
				map.put("conntime", 2160);
			}

			try {

				String sql = new ReportsStatementCreator().getFlightLoadProcQuery(reportsSearchCriteria);
				SqlParameter spParam[] = new SqlParameter[9];
				spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
				spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
				spParam[2] = new SqlParameter("status", Types.VARCHAR);
				spParam[3] = new SqlParameter("opType", Types.INTEGER);
				spParam[4] = new SqlParameter("origin", Types.VARCHAR);
				spParam[5] = new SqlParameter("destination", Types.VARCHAR);
				spParam[6] = new SqlParameter("flightno", Types.VARCHAR);
				spParam[7] = new SqlParameter("conntime", Types.INTEGER);
				spParam[8] = new SqlOutParameter("cursor", OracleTypes.CURSOR, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							rowSet.populate(rs);
							log.debug("RowSet populated");
						}
						return null;
					}
				});

				StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()),
						sql, spParam);
				spCall.callStoredProc(map);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Exception in Flight Load proc " + e.getMessage());
				}
			}
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	public RowSet getAgentSalesStatusData(ReportsSearchCriteria reportsSearchCriteria) {
		try {
			setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();

			HashMap map = new HashMap();

			map.put("agents", reportsSearchCriteria.getAgentCode());
			map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
			map.put("toDate", reportsSearchCriteria.getDateRangeTo());

			try {

				String sql = new ReportsStatementCreator().getAgentSalesStatusProcQuery(reportsSearchCriteria);
				SqlParameter spParam[] = new SqlParameter[4];

				spParam[0] = new SqlParameter("agents", Types.VARCHAR);
				spParam[1] = new SqlParameter("fromDate", Types.VARCHAR);
				spParam[2] = new SqlParameter("toDate", Types.VARCHAR);
				spParam[3] = new SqlOutParameter("cursor", OracleTypes.CURSOR, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							rowSet.populate(rs);
							log.debug("RowSet populated");
						}
						return null;
					}
				});

				StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()),
						sql, spParam);
				spCall.callStoredProc(map);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Exception in Flight Load proc " + e.getMessage());
				}
			}
			return rowSet;
		} catch (SQLException e) {
			log.error("Error " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", ReportingConstants.MODULE_NAME);
		}
	}

	/**
	 * Gets the Agent Statement Summary Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentStatementSummaryData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAgentStatementSummaryDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Agent Statement Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAgentStatementDetailData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAgentStatementDetailDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Email Outstanding Balance Detail Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getEmailOutstandingBalanceDetailData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getEmailOutstandingBalanceDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Flights Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInsurenceDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInsurenceDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Privilige Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getPrivilegeDetailsData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getPrivilegeDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAgentSalesReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentsSalesReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAgentSalesPerformanceReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentsSalesPerformanceReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAgentWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAgentWiseForwardSalesReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getTicketWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getTicketWiseForwardSalesReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Booking Class Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getBCReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBCReporttQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Invoice Settlement History Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getInvoiceSettlemenyHistoryReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getInvoiceSettlemenyHistoryDetailsQuery(
						reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Name Change Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getNameChangeDetailsReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getNameChangeDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the booked Hala services report data
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public RowSet getBookedHalaServicesSummaryReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getBookedHalaServiceSummaryRequestQuery(
						reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the booked Hala services report data
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public RowSet getBookedHalaServicesDetailReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getBookedHalaServiceDetailRequestQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the booked Hala services summary data for Scheduler notification.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public Collection<HalaServiceDTO> getBookedHalaServicesSummary(ReportsSearchCriteria reportsSearchCriteria) {
		return getBookedHalaServicesDetails(
				new ReportsStatementCreator().getBookedHalaServiceNotifiationQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Hala services details per pax. This is used by Scheduler notification.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public Collection<PaxSSRDTO> getBookedHalaServicePaxDetails(ReportsSearchCriteria reportsSearchCriteria) {
		return getBookedHalaServicePaxDetails(
				new ReportsStatementCreator().getBookedHalaServiceDetailRequestQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	/**
	 * @author Navod Ediriweera
	 * @param reportsSearchCriteria
	 * @return
	 */
	public RowSet getSegmentTotalPaxByStatus(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getSegmentPaxTotalData(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Meal Detail Report Data By Currency
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return int to the Report Data
	 */
	public Map<String, Map<String, Integer>> getCountOfPassengers(ReportsSearchCriteria reportsSearchCriteria)
			throws ModuleException {
		setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());
		String sql = new ReportsStatementCreator().getCountOfPassengers(reportsSearchCriteria);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource(reportsSearchCriteria.getDataSoureType()));
		Map<String, Map<String, Integer>> statusWiseSegmentWisePaxCount = (Map) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Map extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Map<String, Integer>> statusWiseSegmentWisePaxCount = new HashMap<String, Map<String, Integer>>();
				while (rs.next()) {
					String status = rs.getString("status");
					if (statusWiseSegmentWisePaxCount.get(status) == null)
						statusWiseSegmentWisePaxCount.put(status, new HashMap<String, Integer>());
					statusWiseSegmentWisePaxCount.get(status).put(rs.getString("segment_code"), new Integer(rs.getInt("cnt")));
				}
				return statusWiseSegmentWisePaxCount;
			}
		});
		return statusWiseSegmentWisePaxCount;
	}

	/**
	 * Gets the Meal Detail Report Data By Flight number and Departure date
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getMealSummaryReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getMealSummaryQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Meal Detail Report Data By Flight number and Departure date
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getMealDetailReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getMealDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFlightAncillaryDetailsReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getFlightAncillaryDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getHalaServiceCommissions(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getHalaServiceCommissionRequestQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getPnrInvoiceReceiptReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getPnrInvoiceReceiptReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getLoyaltyPointsData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getLoyaltyPointsDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Ancillary Revenue Data for the Given Search
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAncillaryRevenueReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAncillaryRevenueQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Advanced Ancillary Revenue Data for given search
	 * 
	 * @param reportsSearchCriteria
	 * 
	 * @return RowSet the Report Data
	 */
	public RowSet getAdvancedAncillaryRevenueReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAdvancedAncillaryRevenueReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getCurrencyConversionDetailReport(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getCurrencyConversionDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Airport Tax Data for the Given Search
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAirportTaxReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAirportTaxReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Adjustment/Correction Audit Data for the Given Search
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getAdjustmentAuditReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAdjustmentAuditReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Void Reservation Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getVoidReservationDetailsReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getVoidReservationDetailsReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the BookingCount Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return
	 */
	public RowSet getBookingCountDetailsReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBookingCountDetailsReportQuery(reportsSearchCriteria,
						false),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getBookingCountDetailAgentReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBookingCountDetailAgentReportQuery(reportsSearchCriteria,
						false),
				reportsSearchCriteria.getDataSoureType());
	}

	public Integer getBookingCountDetailsReportPaxCount(ReportsSearchCriteria reportsSearchCriteria) {
		return getPaxTotalForBookingCountDetailsReport(
				new ReportsStatementCreator().getBookingCountDetailsReportQuery(reportsSearchCriteria, true),
				reportsSearchCriteria.getDataSoureType());
	}

	public Integer getBookingCountDetailAgentReportPaxCount(ReportsSearchCriteria reportsSearchCriteria) {
		return getPaxTotalForBookingCountDetailsReport(
				new ReportsStatementCreator().getBookingCountDetailAgentReportQuery(reportsSearchCriteria, true),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets lcc and Dry Collection data
	 * 
	 * @param reportsSearchCriteria
	 *            the Search Criteria
	 * @return RowSet the Report Data
	 */
	public RowSet getLccAndDryCollectionData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getLccAndDryCollectionReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * gets the Fare Rule Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getFareRuleDetailsData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getFareRuleDetailsDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFareRuleVisibilityData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getFareRuleVisibilityDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAgentsForFareRuleData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAgentsForFareRuleDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getMisProductSalesData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getMisProductSalesAnalysisReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Booked PAX Segment Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getBookedPAXSegmentData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBookedPAXSegmentData(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	public RowSet getOriginCountryOfCallData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getOriginCountryOfCallDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getOriginCountryOfCallDetailData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getOriginCountryOfCallDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}


	public RowSet getIncentiveSchemeData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getIncentiveSchemeDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAgentIncentiveSchemeData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAgentIncentiveSchemeDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAirportListData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAirportListDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getRouteDetailData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getRouteDetailDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Booked SSR Details report Data for P_BOOKED_SSR_DETAILS_REPT
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getBookedSSRDetailsReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getSSRDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Fare Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getFareDetailsData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getFareDetailsReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAppParameterListData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAppParameterDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAgentHandlingFeeData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAgentHandlingFeeDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFlownPassengerListData(ReportsSearchCriteria reportsSearchCriteria, String viewMode) {
		return getReportData(new ReportsStatementCreator().getFlownPassengerFeeDataQuery(reportsSearchCriteria, viewMode),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getFlownPassengerData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getFlownPassengerDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the charge adjustments data.
	 * 
	 * @param reportsSearchCriteria
	 *            : The reports search criteria.
	 * @return : The {@link RowSet} the report data.
	 */
	public RowSet getChargeAdjustmentsData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getChargeAdjustmentsDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Booked SSR Summary report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getBookedSSRSummaryReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBookedSSRSummaryQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Booked SSR Details report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getBookedSSRDetailReportData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBookedSSRDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAirportTransferDataforReports(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getAirportTransferDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the Agent Sales/Modify/Refund Report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 */
	public void getAgentSalesModifyRefundReportData(ReportsSearchCriteria reportsSearchCriteria) {

		setTransactionReadOnly(reportsSearchCriteria.getDataSoureType());

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("reportType", reportsSearchCriteria.getReportType());
		map.put("flightType", reportsSearchCriteria.getSearchFlightType());
		map.put("originAirport", reportsSearchCriteria.getOriginAirport());
		map.put("destinAirport", reportsSearchCriteria.getDestinationAirport());
		map.put("journeyType", reportsSearchCriteria.getJourneyType());
		map.put("flightNo", reportsSearchCriteria.getFlightNumber());
		map.put("cos", reportsSearchCriteria.getCos());
		map.put("fareBasis", reportsSearchCriteria.getFareBasisCode());
		map.put("paidCurrency", reportsSearchCriteria.getCurrencies());
		map.put("fromDate", reportsSearchCriteria.getDateRangeFrom());
		map.put("toDate", reportsSearchCriteria.getDateRangeTo());
		map.put("agents", reportsSearchCriteria.getAgentCode());
		map.put("station", reportsSearchCriteria.getStation());
		map.put("country", reportsSearchCriteria.getCountryCode());
		map.put("agentCity", reportsSearchCriteria.getAddressCity());
		map.put("agentTerritory", reportsSearchCriteria.getTerritory());
		map.put("paxTypeCode", reportsSearchCriteria.getPaxCategory());
		map.put("usrId", reportsSearchCriteria.getUserId());

		try {

			String sql = new ReportsStatementCreator().getAgentSalesModifyRefundProcQuery(reportsSearchCriteria);
			SqlParameter spParam[] = new SqlParameter[18];

			spParam[0] = new SqlParameter("fromDate", Types.VARCHAR);
			spParam[1] = new SqlParameter("toDate", Types.VARCHAR);
			spParam[2] = new SqlParameter("usrId", Types.VARCHAR);
			spParam[3] = new SqlParameter("reportType", Types.VARCHAR);
			spParam[4] = new SqlParameter("flightNo", Types.VARCHAR);
			spParam[5] = new SqlParameter("flightType", Types.VARCHAR);
			spParam[6] = new SqlParameter("cos", Types.VARCHAR);
			spParam[7] = new SqlParameter("journeyType", Types.VARCHAR);
			spParam[8] = new SqlParameter("originAirport", Types.VARCHAR);
			spParam[9] = new SqlParameter("destinAirport", Types.VARCHAR);
			spParam[10] = new SqlParameter("fareBasis", Types.VARCHAR);
			spParam[11] = new SqlParameter("paidCurrency", Types.VARCHAR);
			spParam[12] = new SqlParameter("paxTypeCode", Types.VARCHAR);
			spParam[13] = new SqlParameter("agentCity", Types.VARCHAR);
			spParam[14] = new SqlParameter("country", Types.VARCHAR);
			spParam[15] = new SqlParameter("agentTerritory", Types.VARCHAR);
			spParam[16] = new SqlParameter("station", Types.VARCHAR);
			spParam[17] = new SqlParameter("agents", Types.VARCHAR);

			StoredProcedureCall spCall = new StoredProcedureCall(getDataSource(reportsSearchCriteria.getDataSoureType()), sql,
					spParam);
			spCall.callStoredProc(map);

		} catch (Exception e) {
			log.error("Exception in agent sales/modify/refund proc " + e.getMessage());
		}
	}

	/**
	 * Gets the User Incentive Details Report
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getUserIncentiveDetailsReport(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getUserIncentiveDetailsReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFlightHistoryDetailsReport(String flightId, String flightNo, String flightDate, String reverseDate,
			Integer schduleId, String fromDate, String toDate, boolean showResHistory) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getFlightHistoryDetailsReport(flightId, flightNo, flightDate,
				reverseDate, schduleId, fromDate, toDate, showResHistory), ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
	}

	/**
	 * Gets the Tax History Report Data
	 * 
	 * @param reportsSearchCriteria
	 * 
	 * @return RowSet the Report Data
	 * 
	 */

	public RowSet getTaxHistoryData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getTaxHistoryDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getGDSReservationsData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getGDSReservationsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFlightConnectivityData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();
		String busCarrier = LookupUtils.getFlightBD().getDefaultBusCarrierCode();
		String[] originMinMaxTransitDurations = null;
		String[] destinationMinMaxTransitDurations = null;
		String airlineCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		boolean isGroundSegment = false;

		String[] airportsStr = { reportsSearchCriteria.getOriginAirport(), reportsSearchCriteria.getDestinationAirport() };

		Collection<CachedAirportDTO> airports = LookupUtils.getAirportBD()
				.getCachedOwnAirportMap((Collection) Arrays.asList(airportsStr)).values();
		for (CachedAirportDTO airport : airports) {
			if (airport.isSurfaceSegment()) {
				isGroundSegment = true;
			}
		}

		// Logic to get the minmax connection times for origin/destination airports
		try {
			if (isGroundSegment) {
				if (reportsSearchCriteria.getFlightType().equals("IN")) {
					if (busConnectingAirports.contains(reportsSearchCriteria.getOriginAirport())
							&& !StringUtil.isNullOrEmpty(busCarrier)) {
						originMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
								reportsSearchCriteria.getOriginAirport(), busCarrier, airlineCarrier);
					}
					reportsSearchCriteria.setOriginMinConncetTime(originMinMaxTransitDurations[0]);
					reportsSearchCriteria.setOriginMaxConncetTime(originMinMaxTransitDurations[1]);
				} else if (reportsSearchCriteria.getFlightType().equals("OUT")) {
					if (busConnectingAirports.contains(reportsSearchCriteria.getDestinationAirport())
							&& !StringUtil.isNullOrEmpty(busCarrier)) {
						destinationMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
								reportsSearchCriteria.getDestinationAirport(), busCarrier, airlineCarrier);
					}
					reportsSearchCriteria.setDestinationMinConncetTime(destinationMinMaxTransitDurations[0]);
					reportsSearchCriteria.setDestinationMaxConncetTime(destinationMinMaxTransitDurations[1]);
				} else {
					if (busConnectingAirports.contains(reportsSearchCriteria.getOriginAirport())
							&& !StringUtil.isNullOrEmpty(busCarrier)) {
						originMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
								reportsSearchCriteria.getOriginAirport(), busCarrier, airlineCarrier);
					}
					if (busConnectingAirports.contains(reportsSearchCriteria.getDestinationAirport())
							&& !StringUtil.isNullOrEmpty(busCarrier)) {
						destinationMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
								reportsSearchCriteria.getDestinationAirport(), busCarrier, airlineCarrier);
					}
					reportsSearchCriteria.setOriginMinConncetTime(originMinMaxTransitDurations[0]);
					reportsSearchCriteria.setOriginMaxConncetTime(originMinMaxTransitDurations[1]);
					reportsSearchCriteria.setDestinationMinConncetTime(destinationMinMaxTransitDurations[0]);
					reportsSearchCriteria.setDestinationMaxConncetTime(destinationMinMaxTransitDurations[1]);
				}

			} else {
				if (reportsSearchCriteria.getFlightType().equals("IN")) {

					originMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
							reportsSearchCriteria.getOriginAirport(), null, null);

					reportsSearchCriteria.setOriginMinConncetTime(originMinMaxTransitDurations[0]);
					reportsSearchCriteria.setOriginMaxConncetTime(originMinMaxTransitDurations[1]);

				} else if (reportsSearchCriteria.getFlightType().equals("OUT")) {

					destinationMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
							reportsSearchCriteria.getDestinationAirport(), null, null);

					reportsSearchCriteria.setDestinationMinConncetTime(destinationMinMaxTransitDurations[0]);
					reportsSearchCriteria.setDestinationMaxConncetTime(destinationMinMaxTransitDurations[1]);
				} else {

					originMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
							reportsSearchCriteria.getOriginAirport(), null, null);
					destinationMinMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
							reportsSearchCriteria.getDestinationAirport(), null, null);

					reportsSearchCriteria.setOriginMinConncetTime(originMinMaxTransitDurations[0]);
					reportsSearchCriteria.setOriginMaxConncetTime(originMinMaxTransitDurations[1]);
					reportsSearchCriteria.setDestinationMinConncetTime(destinationMinMaxTransitDurations[0]);
					reportsSearchCriteria.setDestinationMaxConncetTime(destinationMinMaxTransitDurations[1]);
				}
			}
		} catch (ModuleRuntimeException e) {
			if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(e.getExceptionCode())) {
				String zeroHourMin = "00:00";
				if (reportsSearchCriteria.getFlightType().equals("IN")) {
					reportsSearchCriteria.setOriginMinConncetTime(zeroHourMin);
					reportsSearchCriteria.setOriginMaxConncetTime(zeroHourMin);
				} else if (reportsSearchCriteria.getFlightType().equals("OUT")) {
					reportsSearchCriteria.setDestinationMinConncetTime(zeroHourMin);
					reportsSearchCriteria.setDestinationMaxConncetTime(zeroHourMin);
				} else {
					reportsSearchCriteria.setOriginMinConncetTime(zeroHourMin);
					reportsSearchCriteria.setOriginMaxConncetTime(zeroHourMin);
					reportsSearchCriteria.setDestinationMinConncetTime(zeroHourMin);
					reportsSearchCriteria.setDestinationMaxConncetTime(zeroHourMin);
				}
			}
		}

		return getReportData(new ReportsStatementCreator().getFlightConnectivityDataDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getPromotionRequestsSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getPromotionRequestsSummary(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getPromotionNextSeatFreeSummary(ReportsSearchCriteria reportSearchCrieria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getPromotionNextSeatFreeSummary(reportSearchCrieria),
				reportSearchCrieria.getDataSoureType());
	}

	public RowSet getPromotionNextSeatFreeSummaryForCrew(ReportsSearchCriteria reportSearchCrieria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getPromotionNextSeatFreeSummaryForCrew(reportSearchCrieria),
				reportSearchCrieria.getDataSoureType());
	}

	public RowSet getPromotionFlexiDateSummary(ReportsSearchCriteria reportSearchCrieria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getPromotionNextSeatFreeSummaryForCrew(reportSearchCrieria),
				reportSearchCrieria.getDataSoureType());
	}

	public RowSet getPromoCodeDetailsData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getPromoCodeDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFlightOverBookSummary(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getFlightOverbookSummary(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getSeatMapChangesDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getSeaTMapChangesSummary(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getJNTaxReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getJNTaxReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getSalesChannelCodes(String SaleChannelIDs) {
		String sql = queryString.getProperty(SQL_SALES_CHANNEL_DESC);
		sql = sql.replace("$param$1", SaleChannelIDs);

		return getReportData(sql, null, null, null);
	}
	
	public RowSet getIbeExitDetails(ReportsSearchCriteria reportsSearchCriteria){
		return getReportData(new ReportsStatementCreator().getIbeExitDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getIbeExitDetails(String exitDetailsId) {
		return getReportData(new ReportsStatementCreator().getIbeExitDetailsQueryById(exitDetailsId),
				ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
	}

	public RowSet getBundledFareReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getBundledFareReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getInternationalFlightDepartureArrivalDetails(ReportsSearchCriteria reportsSearchCriteria){
		return getReportData(new ReportsStatementCreator().getInternationalFlightDepartureArrivalQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getPassengerInternationalFlightDetailsData(ReportsSearchCriteria reportsSearchCriteria){
		return getReportData(new ReportsStatementCreator().getPassengerInternationalFlightDetailsDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getFlightLoyaltyMembers(ReportsSearchCriteria reportsSearchCriteria){
		return getReportData(new ReportsStatementCreator().getFlightLoyaltyMembersDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getPointRedemptionDetailedReport(ReportsSearchCriteria reportsSearchCriteria){
		return getReportData(new ReportsStatementCreator().getPointRedemptionReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getNameChangeChargeReportData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getNameChangeChargeReportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * Gets the NIL Transactions Agents Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getNILTransactionsAgentsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getNILTransactionsAgentsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	/**
	 * Gets the User Privilege Details Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public Map getAgentUserRolePrivilegeData(ReportsSearchCriteria reportsSearchCriteria) {

		JdbcTemplate jt = new JdbcTemplate(getDataSource(null));

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT U.USER_ID, R.ROLE_NAME ");
		sb.append(" FROM T_ROLE R, T_USER_ROLE U ");
		sb.append(" WHERE R.ROLE_ID = U.ROLE_ID AND ( ");
		// sb.append("       R.STATUS = 'ACT' AND ( ");

		List<String> userList = new ArrayList<String>(reportsSearchCriteria.getUsers());

		while (userList.size() > 1000) {
			List subList = userList.subList(0, 1000);
			userList = userList.subList(1000, userList.size());
			sb.append("       U.USER_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (userList.size() <= 1000) {
			sb.append("       U.USER_ID IN( " + Util.buildStringInClauseContent(userList) + " )) ");
		}

		if (reportsSearchCriteria.getRoles() != null) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( R.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");
		}

		Map userRoleMap = (HashMap) jt.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Map userRoleMap = new HashMap();

				Map recData = null;
				Collection records = null;

				while (rs.next()) {
					recData = new HashMap();
					String userID = rs.getString("USER_ID");
					
					recData.put("USER_ID", userID);
					recData.put("ROLE_NAME", rs.getString("ROLE_NAME"));

					records = (Collection) userRoleMap.get(userID);

					if (records == null) {
						records = new ArrayList();
					}
					records.add(recData);

					userRoleMap.put(userID, records);
					records = null;
				}

				Collection keyList = userRoleMap.keySet();

				for (Iterator iter = keyList.iterator(); iter.hasNext();) {
					String key = (String) iter.next();
					Collection recList = (Collection) userRoleMap.get(key);
					// userRoleMap.put(key, new JRMapCollectionDataSource(recList));
					userRoleMap.put(key, recList);
				}

				return userRoleMap;
			}
		});
		return userRoleMap;
	}
	
	/**
	 * Gets the USer/Agent/Roles Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public Map getAgentUserRoleDetailData(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT DISTINCT U.USER_ID, R.ROLE_NAME, P.PRIVILEGE ");
		sb.append(" FROM T_USER U, T_USER_ROLE UR, T_ROLE_PRIVILEGE RP, T_PRIVILEGE P, T_PRIVILEGE_CATEGORY C, T_ROLE R ");
		sb.append(" WHERE U.USER_ID = UR.USER_ID ");
		sb.append("       AND UR.ROLE_ID = RP.ROLE_ID ");
		sb.append("       AND RP.PRIVILEGE_ID = P.PRIVILEGE_ID ");
		sb.append("       AND P.PRIVILEGE_CATEGORY_ID = C.PRIVILEGE_CATEGORY_ID ");
		sb.append("       AND UR.ROLE_ID = R.ROLE_ID ");

		List<String> userList = new ArrayList<String>(reportsSearchCriteria.getUsers());

		while (userList.size() > 1000) {
			List subList = userList.subList(0, 1000);
			userList = userList.subList(1000, userList.size());
			sb.append("   AND    U.USER_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (userList.size() <= 1000) {
			sb.append("   AND    U.USER_ID IN( " + Util.buildStringInClauseContent(userList) + " ) ");
		}

		if ((reportsSearchCriteria.getPrivileges() == null) && (reportsSearchCriteria.getRoles() != null)) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( R.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");
		}

		if ((reportsSearchCriteria.getRoles() != null) && (reportsSearchCriteria.getPrivileges() != null)) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( R.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");

			List<String> privilegeList = new ArrayList<String>(reportsSearchCriteria.getPrivileges());
			sb.append(" AND ( ");

			while (privilegeList.size() > 100) {
				List<String> subList = privilegeList.subList(0, 100);
				privilegeList = privilegeList.subList(101, privilegeList.size());
				sb.append(" P.PRIVILEGE_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
			}

			if (privilegeList.size() <= 100) {
				sb.append(" P.PRIVILEGE_ID IN( " + Util.buildStringInClauseContent(privilegeList) + " )) ");
			}
		}
		sb.append("       AND P.STATUS = 'ACT' ");
		sb.append("       ORDER BY R.ROLE_NAME ASC");
	
		JdbcTemplate jt = new JdbcTemplate(getDataSource(null));

		Map userRoleMap = (HashMap) jt.query(sb.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Map userRoleMap = new HashMap();

				Map recData = null;
				Collection records = null;

				while (rs.next()) {
					recData = new HashMap();
					String userID = rs.getString("USER_ID");

					recData.put("USER_ID", userID);
					recData.put("ROLE_NAME", rs.getString("ROLE_NAME"));
					recData.put("PRIVILEGE", rs.getString("PRIVILEGE"));

					records = (Collection) userRoleMap.get(userID);

					if (records == null) {
						records = new ArrayList();
					}

					records.add(recData);

					userRoleMap.put(userID, records);
					records = null;
				}

				Collection keyList = userRoleMap.keySet();

				for (Iterator iter = keyList.iterator(); iter.hasNext();) {
					String key = (String) iter.next();
					Collection recList = (Collection) userRoleMap.get(key);
					userRoleMap.put(key, recList);
				}

				return userRoleMap;
			}
		});
		return userRoleMap;
	}

	public RowSet getPFSDetailedExportData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getPFSDetailedExportQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public RowSet getPassengerStatusAndRevenueQuery(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getPassengerStatusAndRevenueReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getFlightMessageDetailsReport(String flightId, Integer scheduleId, String fromDate, String toDate)
			throws ModuleException {
		return getReportData(new ReportsStatementCreator().getFlightMessageDetailsReport(flightId, scheduleId, fromDate, toDate),
				ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
	}

	public RowSet getScheduleMessageDetailsReport(String scheduleId, String fromDate, String toDate) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getScheduleMessageDetailsReport(scheduleId, fromDate, toDate),
				ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
	}

	public RowSet getPublishedMessageDetailsReport(String scheduleFlightId, String strFromDate, String strToDate)
			throws ModuleException {
		return getReportData(
				new ReportsStatementCreator().getPublishMessagesDetailsReport(scheduleFlightId, strFromDate, strToDate),
				ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
	}
	
	public RowSet getMCODetailReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		return getReportData(new ReportsStatementCreator().getMCODetailReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getMCOSummaryReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		return getReportData(new ReportsStatementCreator().getMCOSummaryReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getForceConfirmedBookingReport(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		return getReportData(new ReportsStatementCreator().getForceConfirmedBookingReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}

	public RowSet getGSTAdditionalReportData(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getGSTAdditionalReportData(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}


	public Map<String, ResultSet> getGstr1(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		return getReportData(new ReportsStatementCreator().getGstr1Report(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());

	}


	public Map<String, ResultSet> getGstr3(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		return getReportData(new ReportsStatementCreator().getGstr3Report(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getBlockedLoyaltyPointsData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getBlockedLoyaltyPointsDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getCCTransactionsLMSPointsData(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getCCTransactionsLMSPointsDataQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getCCTransactionsLMSPointsDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		return getReportData(new ReportsStatementCreator().getCCTransactionsLMSPointsDetailQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getCCTopUpTransactionDetails(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {
		return getReportData(new ReportsStatementCreator().getCCTopUpTransactionReport(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}


	/**
	 * Gets the blacklisted Passengers Report Data
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return RowSet the Report Data
	 */
	public RowSet getBlacklistedPassengersData(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getBlacklistedPassengersQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
	
	public RowSet getPromotionCriteraDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getPromotionCriteriaDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}

	public RowSet getAutomaticCheckinDetails(ReportsSearchCriteria reportsSearchCriteria) {

		return getReportData(new ReportsStatementCreator().getAutomaticCheckinDetailsQuery(reportsSearchCriteria),
				reportsSearchCriteria.getDataSoureType());
	}
}