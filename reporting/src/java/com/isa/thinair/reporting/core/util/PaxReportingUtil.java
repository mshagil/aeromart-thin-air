package com.isa.thinair.reporting.core.util;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class PaxReportingUtil {

	public static String getGenderByTitle(String passengerTitle) {
		if (passengerTitle != null) {
			if (passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MR)
					|| passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MASTER)) {

				return PaxType.MALE.toString();
			} else if (passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MS)
					|| passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MRS)
					|| passengerTitle.equalsIgnoreCase(ReservationInternalConstants.PassengerTitle.MISS)) {
				return PaxType.FEMALE.toString();
			}
		}
		return PaxType.UNKNOWN.toString();
	}

	private static enum PaxType {
		MALE, FEMALE, UNKNOWN;
	};
}
