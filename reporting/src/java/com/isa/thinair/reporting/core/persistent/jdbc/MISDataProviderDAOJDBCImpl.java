package com.isa.thinair.reporting.core.persistent.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.mis.OutstandingInvoiceInfoTO;
import com.isa.thinair.reporting.api.model.AgentPerformanceTO;
import com.isa.thinair.reporting.api.model.BookingsTO;
import com.isa.thinair.reporting.api.model.DistributionFlightInfoTO;
import com.isa.thinair.reporting.api.model.MISAccountSummaryTO;
import com.isa.thinair.reporting.api.model.MISOutstandingInvoicesTO;
import com.isa.thinair.reporting.api.model.MISSeatFactorSummaryDTO;
import com.isa.thinair.reporting.api.model.SalesRevenueTO;
import com.isa.thinair.reporting.core.persistent.dao.MISDataProviderDAOJDBC;
import com.isa.thinair.reporting.core.persistent.util.MISStatementCreator;
import com.isa.thinair.reporting.core.persistent.util.ReportsStatementCreator;

public class MISDataProviderDAOJDBCImpl extends PlatformBaseJdbcDAOSupport implements MISDataProviderDAOJDBC {

	private static Log log = LogFactory.getLog(MISDataProviderDAOJDBCImpl.class);
	private boolean halaEnabled;
	private boolean seatEnabled;
	private boolean mealEnabled;
	private boolean insEnabled;
	private static final GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	// ************************************** MIS Dash board related queries ******************************//
	@Override
	public long getMISDashboardGoal(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = "";
		Long target = new Long(0);
		if (searchCriteria.getRegion() != null && !searchCriteria.getRegion().equals("NET")) {
			sql = "SELECT max_booking_scale AS target FROM t_region WHERE region_code = '" + searchCriteria.getRegion() + "'";
		} else {
			sql = "SELECT SUM (max_booking_scale) AS target FROM t_region ";
		}
		target = (Long) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Long lngTarget = new Long(0);
				while (rs.next()) {
					lngTarget = rs.getLong("target");
				}
				return lngTarget;
			}
		});
		return target.longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<BigDecimal> getMISDashRevenueBreakdown(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<BigDecimal> revenueBreakDown = null;
		String sql = MISStatementCreator.getMISDashRevBreakdownSQL(searchCriteria);

		revenueBreakDown = (Collection<BigDecimal>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<BigDecimal> revBreakDown = new ArrayList<BigDecimal>();
				while (rs.next()) {
					revBreakDown.add(rs.getBigDecimal("revenue"));
					revBreakDown.add(rs.getBigDecimal("fare"));
					revBreakDown.add(rs.getBigDecimal("tax"));
					revBreakDown.add(rs.getBigDecimal("surcharges"));
				}
				return revBreakDown;
			}
		});

		return revenueBreakDown;

	}

	@SuppressWarnings("unchecked")
	@Override
	/** Sales by channel count -  this is used in distribution tab*/
	public Map<String, BigDecimal> getMISDashSalesByChannel(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, BigDecimal> salesByChannelMap = null;
		String sql = MISStatementCreator.getMISDashSalesByChannelPaxCount(searchCriteria);
		salesByChannelMap = (Map<String, BigDecimal>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, BigDecimal> salesByChn = new HashMap<String, BigDecimal>();
				while (rs.next()) {
					if (salesByChn.containsKey(rs.getString("channel"))) {
						BigDecimal sales = salesByChn.get(rs.getString("channel"));
						sales = AccelAeroCalculator.add(sales, rs.getBigDecimal("sales"));
						salesByChn.put(rs.getString("channel"), sales);
					} else {
						salesByChn.put(rs.getString("channel"), rs.getBigDecimal("sales"));
					}
				}
				return salesByChn;
			}
		});

		return salesByChannelMap;
	}

	public Map<String, Map<String, Map<String, BigDecimal>>> getMISSeatFactorReportData(
			ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "";
		String query = "";
		Map<String, Map<String, BigDecimal>> mapSeatFactors = new TreeMap<String, Map<String, BigDecimal>>();
		Map<String, Map<String, BigDecimal>> mapYield = new TreeMap<String, Map<String, BigDecimal>>();
		Map<String, Map<String, Map<String, BigDecimal>>> finalMapSeatFactors = new TreeMap<String, Map<String, Map<String, BigDecimal>>>();
		final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		reportsSearchCriteria.setByStation(false);

		if (reportsSearchCriteria.getRegion() != null
				&& !"ALL".equalsIgnoreCase(reportsSearchCriteria.getRegion())
				&& (reportsSearchCriteria.getStation() == null || (reportsSearchCriteria.getStation() != null && "ALL"
						.equalsIgnoreCase(reportsSearchCriteria.getStation())))) {
			if (log.isDebugEnabled())
				log.debug("Called By Region");
			reportsSearchCriteria.setByStation(true);

			String subQuery = "SELECT distinct st.station_code FROM t_station st, t_country cn WHERE st.country_code = cn.country_code "
					+ "   AND cn.region_code      = '" + reportsSearchCriteria.getRegion() + "'";

			sql = "SELECT * FROM ( ";
			sql += MISStatementCreator.getSeatFactorSummaryInfo(reportsSearchCriteria);
			sql += "  )  tbl  WHERE tbl.station in (" + subQuery + ")";

		} else if (reportsSearchCriteria.getStation() != null && !"ALL".equalsIgnoreCase(reportsSearchCriteria.getStation())) {// By
																																// Station
			if (log.isDebugEnabled())
				log.debug("Called By Station");
			reportsSearchCriteria.setByStation(true);
			sql = "SELECT * FROM ( ";
			sql += MISStatementCreator.getSeatFactorSummaryInfo(reportsSearchCriteria);
			sql += "  )  tbl  WHERE tbl.station = '" + reportsSearchCriteria.getStation() + "' ";

		} else { // All
			if (log.isDebugEnabled())
				log.debug("Called by ALL");
			sql = MISStatementCreator.getSeatFactorSummaryInfo(reportsSearchCriteria);
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String arrDaterange[][] = reportsSearchCriteria.getDateRangeArray();
		Map<String, List<BigDecimal>> mapSeatMap = new TreeMap<String, List<BigDecimal>>();
		Map<String, List<BigDecimal>> mapYieldMap = new TreeMap<String, List<BigDecimal>>();

		query = new String(sql);
		query = query.replaceAll("@startDate1", arrDaterange[0][0]);
		query = query.replaceAll("@EndDate1", arrDaterange[0][1]);
		query = query.replaceAll("@startDate2", arrDaterange[1][0]);
		query = query.replaceAll("@EndDate2", arrDaterange[1][1]);
		query = query.replaceAll("@startDate3", arrDaterange[2][0]);
		query = query.replaceAll("@EndDate3", arrDaterange[2][1]);
		query = query.replaceAll("@startDate4", arrDaterange[3][0]);
		query = query.replaceAll("@EndDate4", arrDaterange[3][1]);

		Map<String, List<Map<String, List<MISSeatFactorSummaryDTO>>>> mapSeatList = new TreeMap<String, List<Map<String, List<MISSeatFactorSummaryDTO>>>>();
		List<Map<String, List<MISSeatFactorSummaryDTO>>> lisMaps = new ArrayList<Map<String, List<MISSeatFactorSummaryDTO>>>();

		String[] currYearArr = arrDaterange[0][0].split("-");
		String[] currYearPrevMonArr = arrDaterange[1][0].split("-");
		String[] prevYearArr = arrDaterange[2][0].split("-");

		String currYear = currYearArr[2];
		String prevYear = prevYearArr[2];

		String currMonth = currYearArr[1];
		String prevMonth = currYearPrevMonArr[1];

		Map<String, List<MISSeatFactorSummaryDTO>> mapMonth = new TreeMap<String, List<MISSeatFactorSummaryDTO>>();
		mapMonth.put(currMonth, new ArrayList());
		lisMaps.add(mapMonth);

		mapMonth = new TreeMap<String, List<MISSeatFactorSummaryDTO>>();
		mapMonth.put(prevMonth, new ArrayList());
		lisMaps.add(mapMonth);

		mapSeatList.put(currYear, lisMaps);
		mapSeatList.put(prevYear, lisMaps);

		if (log.isDebugEnabled())
			log.debug(" SQL to excute            : " + query);

		List<MISSeatFactorSummaryDTO> listOfTotal = (List<MISSeatFactorSummaryDTO>) jdbcTemplate.query(query.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						List<MISSeatFactorSummaryDTO> list = new ArrayList<MISSeatFactorSummaryDTO>();
						BigDecimal totalSFBigDec = new BigDecimal(0);
						int count = 0;

						while (rs.next()) {

							MISSeatFactorSummaryDTO dto = new MISSeatFactorSummaryDTO();
							// dto.setFlightId(rs.getInt("flight_id"));
							// dto.setSeatFactor(rs.getBigDecimal("calc_seat_factor"));
							dto.setTotalCapacity(new Integer(rs.getInt("allocated_seats")));
							dto.setTotalSeatSold(rs.getInt("sold_seats"));
							dto.setTotalSeatOnHold(rs.getInt("on_hold_seats"));
							dto.setTotalFareAmount(rs.getBigDecimal("amount"));
							dto.setTotalSurChargeAmount(rs.getBigDecimal("surCharge"));
							dto.setDepartureDateLocal(rs.getDate("departure_date"));
							dto.setEstTimeDepartureLocal(dateFormat.format(rs.getDate("departure_date")));
							// BigDecimal yield = dto.getTotalFareAmount().add(dto.getTotalSurChargeAmount());

							// yield = yield.divide(new BigDecimal(dto.getTotalCapacity()), 6, BigDecimal.ROUND_DOWN);
							// dto.setYield(yield);
							list.add(dto);

						}

						return list;
					}
				});

		List<MISSeatFactorSummaryDTO> listCurYrCurMon = new ArrayList<MISSeatFactorSummaryDTO>();
		List<MISSeatFactorSummaryDTO> listCurYrPrvMon = new ArrayList<MISSeatFactorSummaryDTO>();
		List<MISSeatFactorSummaryDTO> listPrvYrCurMon = new ArrayList<MISSeatFactorSummaryDTO>();
		List<MISSeatFactorSummaryDTO> listPrvYrPrvMon = new ArrayList<MISSeatFactorSummaryDTO>();

		if (listOfTotal != null && listOfTotal.size() > 0) {
			for (MISSeatFactorSummaryDTO dto : listOfTotal) {
				if (dto.getEstTimeDepartureLocal() != null && dto.getEstTimeDepartureLocal().compareTo("") != 0) {
					String[] estDepArr = dto.getEstTimeDepartureLocal().split("-");
					// List<Map<String,List<MISSeatFactorSummaryDTO>>> mapDtoList = mapSeatList.get(estDepArr[2]);

					if (estDepArr != null && estDepArr[1] != null && estDepArr[2] != null) {
						if (estDepArr[2].equalsIgnoreCase(currYear)) {
							if (estDepArr[1].equalsIgnoreCase(currMonth)) {
								listCurYrCurMon.add(dto);
							} else if (estDepArr[1].equalsIgnoreCase(prevMonth)) {
								listCurYrPrvMon.add(dto);
							}
						} else if (estDepArr[2].equalsIgnoreCase(prevYear)) {
							if (estDepArr[1].equalsIgnoreCase(currMonth)) {
								listPrvYrCurMon.add(dto);
							} else if (estDepArr[1].equalsIgnoreCase(prevMonth)) {
								listPrvYrPrvMon.add(dto);
							}
						}
					}
				}
			}
		}

		Map<String, BigDecimal> mapCurYrCurMon = getReportValues(listCurYrCurMon);
		Map<String, BigDecimal> mapCurYrPrvMon = getReportValues(listCurYrPrvMon);
		Map<String, BigDecimal> mapPrvYrCurMon = getReportValues(listPrvYrCurMon);
		Map<String, BigDecimal> mapPrvYrPrvMon = getReportValues(listPrvYrPrvMon);

		// Map
		Map<String, BigDecimal> mapMonthSF = new TreeMap<String, BigDecimal>();
		Map<String, BigDecimal> mapMonthYield = new TreeMap<String, BigDecimal>();

		mapMonthSF.put(currMonth, mapCurYrCurMon.get("SF"));
		mapMonthYield.put(currMonth, mapCurYrCurMon.get("YD"));

		mapMonthSF.put(prevMonth, mapCurYrPrvMon.get("SF"));
		mapMonthYield.put(prevMonth, mapCurYrPrvMon.get("YD"));

		mapSeatFactors.put(currYear, mapMonthSF);
		mapYield.put(currYear, mapMonthYield);

		mapMonthSF = new TreeMap<String, BigDecimal>();
		mapMonthYield = new TreeMap<String, BigDecimal>();

		mapMonthSF.put(currMonth, mapPrvYrCurMon.get("SF"));
		mapMonthYield.put(currMonth, mapPrvYrCurMon.get("YD"));

		mapMonthSF.put(prevMonth, mapPrvYrPrvMon.get("SF"));
		mapMonthYield.put(prevMonth, mapPrvYrPrvMon.get("YD"));

		mapSeatFactors.put(prevYear, mapMonthSF);
		mapYield.put(prevYear, mapMonthYield);

		finalMapSeatFactors.put("SF", mapSeatFactors);
		finalMapSeatFactors.put("YD", mapYield);

		if (log.isDebugEnabled())
			log.debug("<------- END ------>");
		return finalMapSeatFactors;
	}

	private Map<String, BigDecimal> getReportValues(List<MISSeatFactorSummaryDTO> list) {
		BigDecimal totalSeatSold = new BigDecimal(0);
		BigDecimal totalSeatOnHold = new BigDecimal(0);
		BigDecimal totalCapacity = new BigDecimal(0);

		BigDecimal totalFareAmount = new BigDecimal(0);
		BigDecimal totalSurChargeAmount = new BigDecimal(0);
		Map<String, BigDecimal> map = new TreeMap<String, BigDecimal>();

		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			MISSeatFactorSummaryDTO misDTO = (MISSeatFactorSummaryDTO) iterator.next();
			totalCapacity = totalCapacity.add(new BigDecimal(misDTO.getTotalCapacity()));
			totalSeatSold = totalSeatSold.add(new BigDecimal(misDTO.getTotalSeatSold()));
			totalSeatOnHold = totalSeatOnHold.add(new BigDecimal(misDTO.getTotalSeatOnHold()));

			// fare
			totalFareAmount = totalFareAmount.add(misDTO.getTotalFareAmount());
			totalSurChargeAmount = totalSurChargeAmount.add(misDTO.getTotalSurChargeAmount());
		}

		BigDecimal seatSoldAndOnHold = totalSeatSold.add(totalSeatOnHold);
		BigDecimal sf = new BigDecimal(0);
		if (!seatSoldAndOnHold.equals(new BigDecimal(0)) && !totalCapacity.equals(new BigDecimal(0)))
			sf = seatSoldAndOnHold.divide(totalCapacity, 6, BigDecimal.ROUND_DOWN).multiply(new BigDecimal(100))
					.setScale(3, BigDecimal.ROUND_HALF_UP);

		BigDecimal totalAmount = totalFareAmount.add(totalSurChargeAmount);
		BigDecimal yield = new BigDecimal(0);
		if (!totalAmount.equals(new BigDecimal(0)) && !totalCapacity.equals(new BigDecimal(0)))
			yield = totalAmount.divide(totalCapacity, 6, BigDecimal.ROUND_DOWN).setScale(2, BigDecimal.ROUND_HALF_UP);

		map.put("SF", sf);
		map.put("YD", yield);
		return map;
	}

	public Map<String, List<Map<String, List<BigDecimal>>>> getYieldTrendAnalysisReport(
			ReportsSearchCriteria reportsSearchCriteria) {
		Map<String, List<Map<String, List<BigDecimal>>>> finalResult = new TreeMap<String, List<Map<String, List<BigDecimal>>>>();
		String sql = "";
		String query = "";
		final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		final String months[] = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
		String PREFIX_SF = "SF";
		String PREFIX_YD = "YD";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		if (log.isDebugEnabled()) {
			log.debug("Called getYieldTrendAnalysisReport");
		}

		sql = MISStatementCreator.getSeatFactorSummaryInfo(reportsSearchCriteria);

		Set<String> keys = reportsSearchCriteria.getDateRangeMap().keySet();

		for (String key : keys) {
			String dateRangeArray[][] = reportsSearchCriteria.getDateRangeMap().get(key);

			for (int i = 0; i < dateRangeArray.length; i++) {
				Map<String, List<BigDecimal>> mapSfAndYdPrvYr = new TreeMap<String, List<BigDecimal>>();
				Map<String, List<BigDecimal>> mapSfAndYdCurYr = new TreeMap<String, List<BigDecimal>>();

				query = new String(sql);
				query = query.replaceAll("@startDate1", dateRangeArray[i][0]);
				query = query.replaceAll("@EndDate1", dateRangeArray[i][1]);

				String[] prevYearArr = dateRangeArray[0][0].split("-");
				String[] currYearArr = dateRangeArray[0][1].split("-");

				String currYear = currYearArr[2];
				String prevYear = prevYearArr[2];

				finalResult.put(currYear, new ArrayList<Map<String, List<BigDecimal>>>());
				finalResult.put(prevYear, new ArrayList<Map<String, List<BigDecimal>>>());

				if (log.isDebugEnabled())
					log.debug(" SQL to excute            : " + query);

				List<MISSeatFactorSummaryDTO> listOfTotal = (List<MISSeatFactorSummaryDTO>) jdbcTemplate.query(query.toString(),
						new ResultSetExtractor() {
							public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

								List<MISSeatFactorSummaryDTO> list = new ArrayList<MISSeatFactorSummaryDTO>();
								BigDecimal totalSFBigDec = new BigDecimal(0);
								int count = 0;

								while (rs.next()) {

									MISSeatFactorSummaryDTO dto = new MISSeatFactorSummaryDTO();
									// dto.setFlightId(rs.getInt("flight_id"));
									// dto.setSeatFactor(rs.getBigDecimal("calc_seat_factor"));
									dto.setTotalCapacity(new Integer(rs.getInt("allocated_seats")));
									dto.setTotalSeatSold(rs.getInt("sold_seats"));
									dto.setTotalSeatOnHold(rs.getInt("on_hold_seats"));
									dto.setTotalFareAmount(rs.getBigDecimal("amount"));
									dto.setTotalSurChargeAmount(rs.getBigDecimal("surCharge"));
									dto.setDepartureDateLocal(rs.getDate("departure_date"));
									dto.setEstTimeDepartureLocal(dateFormat.format(rs.getDate("departure_date")));

									// BigDecimal yield = dto.getTotalFareAmount().add(dto.getTotalSurChargeAmount());

									// yield = yield.divide(new BigDecimal(dto.getTotalCapacity()), 6,
									// BigDecimal.ROUND_DOWN);
									// dto.setYield(yield);
									list.add(dto);

								}

								return list;
							}
						});

				List<MISSeatFactorSummaryDTO> listCurYrJan = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrFeb = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrMar = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrApr = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrMay = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrJun = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrJul = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrAug = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrSep = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrOct = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrNov = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listCurYrDec = new ArrayList<MISSeatFactorSummaryDTO>();

				List<MISSeatFactorSummaryDTO> listPrvYrJan = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrFeb = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrMar = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrApr = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrMay = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrJun = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrJul = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrAug = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrSep = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrOct = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrNov = new ArrayList<MISSeatFactorSummaryDTO>();
				List<MISSeatFactorSummaryDTO> listPrvYrDec = new ArrayList<MISSeatFactorSummaryDTO>();

				if (listOfTotal != null && listOfTotal.size() > 0) {
					for (MISSeatFactorSummaryDTO dto : listOfTotal) {
						if (dto.getEstTimeDepartureLocal() != null && dto.getEstTimeDepartureLocal().compareTo("") != 0) {
							String[] estDepArr = dto.getEstTimeDepartureLocal().split("-");
							// List<Map<String,List<MISSeatFactorSummaryDTO>>> mapDtoList =
							// mapSeatList.get(estDepArr[2]);

							if (estDepArr != null && estDepArr[1] != null && estDepArr[2] != null) {
								if (estDepArr[2].equalsIgnoreCase(currYear)) { // Current Year Data
									if (estDepArr[1].equalsIgnoreCase(months[0])) // Jan data
										listCurYrJan.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[1])) // Feb data
										listCurYrFeb.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[2])) // Mar data
										listCurYrMar.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[3])) // Apr data
										listCurYrApr.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[4])) // May data
										listCurYrMay.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[5])) // Jun data
										listCurYrJun.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[6])) // Jul data
										listCurYrJul.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[7])) // Aug data
										listCurYrAug.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[8])) // Sep data
										listCurYrSep.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[9])) // Oct data
										listCurYrOct.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[10])) // Nov data
										listCurYrNov.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[11])) // Dec data
										listCurYrDec.add(dto);
								} else if (estDepArr[2].equalsIgnoreCase(prevYear)) { // Previous Year Data
									if (estDepArr[1].equalsIgnoreCase(months[0])) // Jan data
										listPrvYrJan.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[1])) // Feb data
										listPrvYrFeb.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[2])) // Mar data
										listPrvYrMar.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[3])) // Apr data
										listPrvYrApr.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[4])) // May data
										listPrvYrMay.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[5])) // Jun data
										listPrvYrJun.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[6])) // Jul data
										listPrvYrJul.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[7])) // Aug data
										listPrvYrAug.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[8])) // Sep data
										listPrvYrSep.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[9])) // Oct data
										listPrvYrOct.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[10])) // Nov data
										listPrvYrNov.add(dto);
									else if (estDepArr[1].equalsIgnoreCase(months[11])) // Dec data
										listPrvYrDec.add(dto);
								}
							}
						}
					}
				}

				Map<String, BigDecimal> mapCurYrJan = getReportValues(listCurYrJan);
				Map<String, BigDecimal> mapCurYrFeb = getReportValues(listCurYrFeb);
				Map<String, BigDecimal> mapCurYrMar = getReportValues(listCurYrMar);
				Map<String, BigDecimal> mapCurYrApr = getReportValues(listCurYrApr);
				Map<String, BigDecimal> mapCurYrMay = getReportValues(listCurYrMay);
				Map<String, BigDecimal> mapCurYrJun = getReportValues(listCurYrJun);
				Map<String, BigDecimal> mapCurYrJul = getReportValues(listCurYrJul);
				Map<String, BigDecimal> mapCurYrAug = getReportValues(listCurYrAug);
				Map<String, BigDecimal> mapCurYrSep = getReportValues(listCurYrSep);
				Map<String, BigDecimal> mapCurYrOct = getReportValues(listCurYrOct);
				Map<String, BigDecimal> mapCurYrNov = getReportValues(listCurYrNov);
				Map<String, BigDecimal> mapCurYrDec = getReportValues(listCurYrDec);

				Map<String, BigDecimal> mapPrvYrJan = getReportValues(listPrvYrJan);
				Map<String, BigDecimal> mapPrvYrFeb = getReportValues(listPrvYrFeb);
				Map<String, BigDecimal> mapPrvYrMar = getReportValues(listPrvYrMar);
				Map<String, BigDecimal> mapPrvYrApr = getReportValues(listPrvYrApr);
				Map<String, BigDecimal> mapPrvYrMay = getReportValues(listPrvYrMay);
				Map<String, BigDecimal> mapPrvYrJun = getReportValues(listPrvYrJun);
				Map<String, BigDecimal> mapPrvYrJul = getReportValues(listPrvYrJul);
				Map<String, BigDecimal> mapPrvYrAug = getReportValues(listPrvYrAug);
				Map<String, BigDecimal> mapPrvYrSep = getReportValues(listPrvYrSep);
				Map<String, BigDecimal> mapPrvYrOct = getReportValues(listPrvYrOct);
				Map<String, BigDecimal> mapPrvYrNov = getReportValues(listPrvYrNov);
				Map<String, BigDecimal> mapPrvYrDec = getReportValues(listPrvYrDec);

				for (int j = 0; j < months.length; j++) {
					mapSfAndYdPrvYr.put(months[j], new ArrayList());
					mapSfAndYdCurYr.put(months[j], new ArrayList());
				}

				// Current Year Data
				mapSfAndYdCurYr.get(months[0]).add(mapCurYrJan.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[0]).add(mapCurYrJan.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[1]).add(mapCurYrFeb.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[1]).add(mapCurYrFeb.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[2]).add(mapCurYrMar.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[2]).add(mapCurYrMar.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[3]).add(mapCurYrApr.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[3]).add(mapCurYrApr.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[4]).add(mapCurYrMay.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[4]).add(mapCurYrMay.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[5]).add(mapCurYrJun.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[5]).add(mapCurYrJun.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[6]).add(mapCurYrJul.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[6]).add(mapCurYrJul.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[7]).add(mapCurYrAug.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[7]).add(mapCurYrAug.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[8]).add(mapCurYrSep.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[8]).add(mapCurYrSep.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[9]).add(mapCurYrOct.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[9]).add(mapCurYrOct.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[10]).add(mapCurYrNov.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[10]).add(mapCurYrNov.get(PREFIX_YD));

				mapSfAndYdCurYr.get(months[11]).add(mapCurYrDec.get(PREFIX_SF));
				mapSfAndYdCurYr.get(months[11]).add(mapCurYrDec.get(PREFIX_YD));

				// Previous Year Data------------------------------------------

				mapSfAndYdPrvYr.get(months[0]).add(mapPrvYrJan.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[0]).add(mapPrvYrJan.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[1]).add(mapPrvYrFeb.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[1]).add(mapPrvYrFeb.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[2]).add(mapPrvYrMar.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[2]).add(mapPrvYrMar.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[3]).add(mapPrvYrApr.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[3]).add(mapPrvYrApr.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[4]).add(mapPrvYrMay.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[4]).add(mapPrvYrMay.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[5]).add(mapPrvYrJun.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[5]).add(mapPrvYrJun.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[6]).add(mapPrvYrJul.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[6]).add(mapPrvYrJul.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[7]).add(mapPrvYrAug.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[7]).add(mapPrvYrAug.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[8]).add(mapPrvYrSep.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[8]).add(mapPrvYrSep.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[9]).add(mapPrvYrOct.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[9]).add(mapPrvYrOct.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[10]).add(mapPrvYrNov.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[10]).add(mapPrvYrNov.get(PREFIX_YD));

				mapSfAndYdPrvYr.get(months[11]).add(mapPrvYrDec.get(PREFIX_SF));
				mapSfAndYdPrvYr.get(months[11]).add(mapPrvYrDec.get(PREFIX_YD));

				finalResult.get(currYear).add(mapSfAndYdCurYr);
				finalResult.get(prevYear).add(mapSfAndYdPrvYr);

			}
		}

		return finalResult;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<SalesRevenueTO> getMISDashYearlySales(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<SalesRevenueTO> yearlySales = null;
		String sql = MISStatementCreator.getMISDashRevenueByMonth(searchCriteria);
		yearlySales = (Collection<SalesRevenueTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<SalesRevenueTO> colYearlySales = new ArrayList<SalesRevenueTO>();
				SalesRevenueTO fareTO = null;
				SalesRevenueTO chargeTO = null;
				while (rs.next()) {
					fareTO = new SalesRevenueTO();
					fareTO.setSourceOfRevenue(SalesRevenueTO.REV_TYPE_FARE);
					fareTO.setMonth(rs.getString("MONTH"));
					fareTO.setYear(rs.getString("YEAR"));
					if (rs.getBigDecimal("fare") != null) {
						fareTO.setAmount(rs.getBigDecimal("fare").toPlainString());
					}

					colYearlySales.add(fareTO);
					chargeTO = new SalesRevenueTO();
					chargeTO.setSourceOfRevenue(SalesRevenueTO.REV_TYPE_CHARGE);
					chargeTO.setMonth(rs.getString("MONTH"));
					chargeTO.setYear(rs.getString("YEAR"));
					if (rs.getBigDecimal("charges") != null) {
						chargeTO.setAmount(rs.getBigDecimal("charges").toPlainString());
					}
					colYearlySales.add(chargeTO);
				}
				return colYearlySales;
			}
		});

		return yearlySales;
	}

	@Override
	public long getMISYtdBookings(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Long ytdBookings = null;
		String sql = MISStatementCreator.getMISDashYtdPaxSegmentCount(searchCriteria);
		ytdBookings = (Long) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Long sales = new Long(0);
				while (rs.next()) {
					sales += rs.getLong("sales");
				}
				return sales;
			}
		});

		return ytdBookings.longValue();
	}

	// ************************ MIS Distribution related queries **********************************//

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Collection<BookingsTO>> getMISDistChannelwiseSalesForCountry(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, Collection<BookingsTO>> bookingsList = null;
		final String countryCode = searchCriteria.getCountryCodes();
		String sql = MISStatementCreator.getMISDistSalesByChannelPerCountry(searchCriteria);

		bookingsList = (Map<String, Collection<BookingsTO>>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Collection<BookingsTO>> colBookings = new HashMap<String, Collection<BookingsTO>>();
				List<BookingsTO> bookingsTOList = null;
				while (rs.next()) {
					String country = rs.getString("country");
					BookingsTO bookingsTO = new BookingsTO();
					bookingsTO.setNoOfBookings(rs.getString("sales"));
					bookingsTO.setSalesChannel(rs.getString("channel"));
					bookingsTO.setSalesChannelColor(rs.getString("channelColor"));
					bookingsTO.setCountryName(rs.getString("country"));
					if (colBookings.containsKey(country)) {
						colBookings.get(country).add(bookingsTO);
					} else {
						bookingsTOList = new ArrayList<BookingsTO>();
						bookingsTOList.add(bookingsTO);
						colBookings.put(country, bookingsTOList);
					}
				}
				return colBookings;
			}
		});
		return bookingsList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<DistributionFlightInfoTO> getMISDistCountryWiseFlights(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		List<DistributionFlightInfoTO> flightsCitiesList = null;
		String sql = MISStatementCreator.getMISDistributionFlightsCitiesNAgents(searchCriteria);

		flightsCitiesList = (List<DistributionFlightInfoTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<DistributionFlightInfoTO> colBookings = new ArrayList<DistributionFlightInfoTO>();
				DistributionFlightInfoTO distributionFlightInfoTO = null;
				while (rs.next()) {
					distributionFlightInfoTO = new DistributionFlightInfoTO();
					distributionFlightInfoTO.setAgents(rs.getString("agents"));
					distributionFlightInfoTO.setCities(rs.getString("cities"));
					distributionFlightInfoTO.setCountryCode(rs.getString("country_code"));
					distributionFlightInfoTO.setFlights(rs.getString("flights"));
					distributionFlightInfoTO.setCountry(rs.getString("country"));
					colBookings.add(distributionFlightInfoTO);
				}
				return colBookings;
			}
		});

		return flightsCitiesList;
	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * This method is called at Dash board -  Revenue by Channel
	 */
	public Map<String, BigDecimal> getMISDistRevenueBreakDownChannelwise(MISReportsSearchCriteria searchCriteria) {

		Map<String, BigDecimal> revBreakDownChannelWise = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = MISStatementCreator.getMISDashRevenueByChannel(searchCriteria);
		revBreakDownChannelWise = (Map<String, BigDecimal>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, BigDecimal> revenueByChn = new HashMap<String, BigDecimal>();
				while (rs.next()) {
					revenueByChn.put(rs.getString("channel"), rs.getBigDecimal("revenue"));
				}
				return revenueByChn;
			}
		});
		return revBreakDownChannelWise;
	}

	@Override
	public Collection getMISDistPerformanceOfPortals(MISReportsSearchCriteria searchCriteria) {
		// TODO Auto-generated method stub
		return null;
	}

	// ********************** MIS Region related queries *************************//
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Long> getMisRegionYearlySales(MISReportsSearchCriteria searchCriteria) {

		String sql = MISStatementCreator.getMISRegYearlySales(searchCriteria);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, Long> regSales = null;

		regSales = (Map<String, Long>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Long> regSalesByYear = new HashMap<String, Long>();
				while (rs.next()) {
					regSalesByYear.put(rs.getString("year"), rs.getLong("sales"));
				}
				return regSalesByYear;
			}
		});
		return regSales;

	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * This method will return a Map of sales for 6 months region wise.
	 * Returns Map --> key : RegionName
	 * 				   Value : Map --> key : month*year (ex: "01*2009")
	 * 						           Value : sales(ex: "5000")	
	 * 
	 */
	public Map<String, Map<String, String>> getMISRegionRollingSalesTrend(MISReportsSearchCriteria searchCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, Map<String, String>> salesTrendList = null;

		String sql = MISStatementCreator.getMISRegionalSixMonsRollingTrend(searchCriteria);

		salesTrendList = (Map<String, Map<String, String>>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Map<String, String>> colSalesTrend = new TreeMap<String, Map<String, String>>();

				while (rs.next()) {
					String keyForSales = rs.getString("year") + "*" + rs.getString("month");
					if (colSalesTrend.containsKey(keyForSales)) {
						colSalesTrend.get(keyForSales).put(rs.getString("region"), rs.getString("sales"));
					} else {
						colSalesTrend.put(keyForSales, new TreeMap<String, String>());
						colSalesTrend.get(keyForSales).put(rs.getString("region"), rs.getString("sales"));
					}
				}
				return colSalesTrend;
			}
		});
		return salesTrendList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgentPerformanceTO> getBestPerformingAgent(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<AgentPerformanceTO> agentPerformanceCollection = null;
		String sql = MISStatementCreator.getMISRegionalBestPerformingAgents(searchCriteria);
		agentPerformanceCollection = (Collection<AgentPerformanceTO>) jdbcTemplate.query(sql.toString(),
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						AgentPerformanceTO agentTO = null;
						Collection<AgentPerformanceTO> agentsList = new ArrayList<AgentPerformanceTO>();
						while (rs.next()) {
							agentTO = new AgentPerformanceTO();
							agentTO.setAgentCode(rs.getString("AgentCode"));
							agentTO.setAgentName(rs.getString("AgentName"));
							agentTO.setAgentLocation(rs.getString("StationName"));
							agentTO.setAddress1(rs.getString("address1"));
							String address2 = rs.getString("address2");
							agentTO.setAddress2(address2 != null ? address2 : "");
							agentTO.setAmount(AccelAeroCalculator.formatAsDecimal(rs.getBigDecimal("revenue")));
							agentTO.setCity(rs.getString("city"));
							agentTO.setCountry(rs.getString("country"));
							agentTO.setContact(rs.getString("ContactPerson"));
							agentTO.setEmail(rs.getString("email_id"));
							// agentTO.setLastSale(rs.getString(""));
							agentTO.setBestPerfoming(true);
							agentTO.setTelNo(rs.getString("telephone"));

							agentsList.add(agentTO);
						}
						return agentsList;
					}
				});

		Map<String, BigDecimal> agentYearlySales = getAgentsYearlySales(agentPerformanceCollection, searchCriteria);
		if (agentYearlySales != null) {
			for (AgentPerformanceTO agentPerformanceTO : agentPerformanceCollection) {
				if (agentYearlySales.containsKey(agentPerformanceTO.getAgentCode())) {
					agentPerformanceTO.setYearlySales(AccelAeroCalculator.formatAsDecimal(agentYearlySales.get(agentPerformanceTO
							.getAgentCode())));
				}
			}
		}

		return agentPerformanceCollection;
	}

	@SuppressWarnings("unchecked")
	private Map<String, BigDecimal> getAgentsYearlySales(Collection<AgentPerformanceTO> agents,
			MISReportsSearchCriteria searchCriteria) {
		List<String> agentCodes = new ArrayList<String>();
		Map<String, BigDecimal> agentYearlySalesCollection = null;
		if (agents != null && agents.size() > 0) {
			for (AgentPerformanceTO agent : agents) {
				agentCodes.add(agent.getAgentCode());
			}
			String agentCodesStr = Util.buildStringInClauseContent(agentCodes);
			String nominalCodes = Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());

			searchCriteria.setAgentCodes(agentCodesStr);
			searchCriteria.setPaymentNominalCodes(nominalCodes);
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

			String sql = MISStatementCreator.getMISAgentsYearySales(searchCriteria);
			agentYearlySalesCollection = (Map<String, BigDecimal>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Map<String, BigDecimal> agentsList = new HashMap<String, BigDecimal>();
					while (rs.next()) {
						agentsList.put(rs.getString("agentcode"), rs.getBigDecimal("sales"));
					}
					return agentsList;
				}
			});
		}
		return agentYearlySalesCollection;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgentPerformanceTO> getLowPerformingAgent(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<AgentPerformanceTO> agentPerformanceCollection = null;
		String sql = MISStatementCreator.getMISRegionalLowPerformingAgents(searchCriteria);

		agentPerformanceCollection = (Collection<AgentPerformanceTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				AgentPerformanceTO agentTO = null;
				Collection<AgentPerformanceTO> agentsList = new ArrayList<AgentPerformanceTO>();
				while (rs.next()) {
					agentTO = new AgentPerformanceTO();
					agentTO.setAgentCode(rs.getString("AgentCode"));
					agentTO.setAgentName(rs.getString("AgentName"));
					agentTO.setAgentLocation(rs.getString("StationName"));
					agentTO.setAddress1(rs.getString("address1"));
					agentTO.setAddress2(rs.getString("address2"));
					agentTO.setAmount(AccelAeroCalculator.formatAsDecimal(rs.getBigDecimal("revenue")));
					agentTO.setCity(rs.getString("city"));
					agentTO.setCountry(rs.getString("country"));
					agentTO.setContact(rs.getString("ContactPerson"));
					agentTO.setEmail(rs.getString("email_id"));
					// agentTO.setLastSale(rs.getString(""));
					agentTO.setLowPerforming(true);
					agentTO.setTelNo(rs.getString("telephone"));

					agentsList.add(agentTO);

				}
				return agentsList;
			}
		});
		Map<String, BigDecimal> agentYearlySales = getAgentsYearlySales(agentPerformanceCollection, searchCriteria);
		if (agentYearlySales != null) {
			for (AgentPerformanceTO agentPerformanceTO : agentPerformanceCollection) {
				if (agentYearlySales.containsKey(agentPerformanceTO.getAgentCode())) {
					agentPerformanceTO.setYearlySales(AccelAeroCalculator.formatAsDecimal(agentYearlySales.get(agentPerformanceTO
							.getAgentCode())));
				}
			}
		}
		return agentPerformanceCollection;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<AgentPerformanceTO> getRegionalProductSalesData(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<AgentPerformanceTO> regionalProductSalesCollection = null;
		String sql = MISStatementCreator.getMISRegionalProductSalesDataQuery(searchCriteria);

		regionalProductSalesCollection = (Collection<AgentPerformanceTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				AgentPerformanceTO agentProductSalesTO = null;
				Collection<AgentPerformanceTO> salesList = new ArrayList<AgentPerformanceTO>();
				while (rs.next()) {
					agentProductSalesTO = new AgentPerformanceTO();
					agentProductSalesTO.setRegionName(rs.getString("region"));
					// agentProductSalesTO.setActualSales(new Long(rs.getString("sales")));
					agentProductSalesTO.setSalesValue(rs.getBigDecimal("sales"));
					agentProductSalesTO.setAgentName(rs.getString("agent"));
					agentProductSalesTO.setAgentLocation(rs.getString("location"));
					salesList.add(agentProductSalesTO);
				}
				return salesList;
			}
		});

		return regionalProductSalesCollection;
	}

	@Override
	public Long getMISRegionalAvgAgentSales(MISReportsSearchCriteria searchCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Long avgSales = null;

		String sql = MISStatementCreator.getMISRegionalAVGAgentSales(searchCriteria);
		avgSales = (Long) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Long agentAvgSales = null;
				while (rs.next()) {
					agentAvgSales = rs.getLong("AVG");
				}
				return agentAvgSales;
			}
		});
		return avgSales;
	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * This method will summarize all the ancillary sales data for a given search criteria.
	 * 
	 */
	public Map<String, BigDecimal> getAncillarySalesSummary(MISReportsSearchCriteria searchCriteria) {
		String sql = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, BigDecimal> ancillarySalesSummary = null;
		if (searchCriteria.isAnciByCount()) {
			sql = MISStatementCreator.getAncillarySummaryDataByCount(searchCriteria);
		} else {
			sql = MISStatementCreator.getAncillarySummaryDataBySales(searchCriteria);
		}

		ancillarySalesSummary = (Map<String, BigDecimal>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, BigDecimal> ancSalesSummary = new HashMap<String, BigDecimal>();
				while (rs.next()) {
					ancSalesSummary.put("HALA", rs.getBigDecimal("hala"));
					ancSalesSummary.put("INSURANCE", rs.getBigDecimal("ins"));
					ancSalesSummary.put("MEAL", rs.getBigDecimal("meal"));
					ancSalesSummary.put("SEAT", rs.getBigDecimal("seat"));
				}
				return ancSalesSummary;
			}
		});

		return ancillarySalesSummary;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<String, String>> getAncillarySalesByRoute(MISReportsSearchCriteria searchCriteria) {
		setEanbledAncillary();
		String sql = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, Map<String, String>> anciSalesByRoute = null;
		final boolean anciByCount = searchCriteria.isAnciByCount();
		if (searchCriteria.isAnciByCount()) {
			sql = MISStatementCreator.getAncillarySummaryDataForRouteByCount(searchCriteria);
		} else {
			sql = MISStatementCreator.getAncillarySummaryDataForRouteBySales(searchCriteria);
		}

		anciSalesByRoute = (Map<String, Map<String, String>>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Map<String, String>> ancSalesByRoute = new HashMap<String, Map<String, String>>();
				String baseCurr = AppSysParamsUtil.getBaseCurrency();
				Map<String, String> halaSalesByRoute = new HashMap<String, String>();
				Map<String, String> insSalesByRoute = new HashMap<String, String>();
				Map<String, String> mealSalesByRoute = new HashMap<String, String>();
				Map<String, String> seatSalesByRoute = new HashMap<String, String>();
				while (rs.next()) {
					BigDecimal halaSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal insSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal mealSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal seatSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					halaSales = rs.getBigDecimal("hala");
					insSales = rs.getBigDecimal("ins");
					mealSales = rs.getBigDecimal("meal");
					seatSales = rs.getBigDecimal("seat");
					String route = rs.getString("segment_code");

					if (anciByCount) {
						halaSales = halaSales.setScale(0);
						insSales = insSales.setScale(0);
						mealSales = mealSales.setScale(0);
						seatSales = seatSales.setScale(0);
						if (halaSales.compareTo(new BigDecimal(0)) > 0) {
							halaSalesByRoute.put(route, halaSales.toPlainString());
						}
						if (insSales.compareTo(new BigDecimal(0)) > 0) {
							insSalesByRoute.put(route, insSales.toPlainString());
						}
						if (mealSales.compareTo(new BigDecimal(0)) > 0) {
							mealSalesByRoute.put(route, mealSales.toPlainString());
						}
						if (seatSales.compareTo(new BigDecimal(0)) > 0) {
							seatSalesByRoute.put(route, seatSales.toPlainString());
						}
					} else {
						if (halaSales.compareTo(new BigDecimal(0)) > 0) {
							halaSalesByRoute.put(route, AccelAeroCalculator.formatAsDecimal(halaSales) + " " + baseCurr);
						}
						if (insSales.compareTo(new BigDecimal(0)) > 0) {
							insSalesByRoute.put(route, AccelAeroCalculator.formatAsDecimal(insSales) + " " + baseCurr);
						}
						if (mealSales.compareTo(new BigDecimal(0)) > 0) {
							mealSalesByRoute.put(route, AccelAeroCalculator.formatAsDecimal(mealSales) + " " + baseCurr);
						}
						if (seatSales.compareTo(new BigDecimal(0)) > 0) {
							seatSalesByRoute.put(route, AccelAeroCalculator.formatAsDecimal(seatSales) + " " + baseCurr);
						}
					}
					if (halaEnabled) {
						ancSalesByRoute.put("HALA", halaSalesByRoute);
					}
					if (insEnabled) {
						ancSalesByRoute.put("INSURANCE", insSalesByRoute);
					}
					if (seatEnabled) {
						ancSalesByRoute.put("SEAT", seatSalesByRoute);
					}
					if (mealEnabled) {
						ancSalesByRoute.put("MEAL", mealSalesByRoute);
					}
				}
				return ancSalesByRoute;
			}
		});

		return anciSalesByRoute;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<String, BigDecimal>> getAncillarySalesBychannel(MISReportsSearchCriteria searchCriteria) {
		String sql = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, Map<String, BigDecimal>> anciSalesByChannel = null;
		final boolean anciByCount = searchCriteria.isAnciByCount();
		setEanbledAncillary();
		if (searchCriteria.isAnciByCount()) {
			sql = MISStatementCreator.getAncillaryChannelWiseSalesCount(searchCriteria);
		} else {
			sql = MISStatementCreator.getAncillaryChannelWiseSales(searchCriteria);
		}
		anciSalesByChannel = (Map<String, Map<String, BigDecimal>>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Map<String, BigDecimal>> ancSalesByRoute = new HashMap<String, Map<String, BigDecimal>>();
				Map<String, BigDecimal> halaSalesPerCategory = new HashMap<String, BigDecimal>();
				Map<String, BigDecimal> insSalesPerCategory = new HashMap<String, BigDecimal>();
				Map<String, BigDecimal> mealSalesPerCategory = new HashMap<String, BigDecimal>();
				Map<String, BigDecimal> seatSalesPerCategory = new HashMap<String, BigDecimal>();
				while (rs.next()) {
					BigDecimal halaSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal insSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal mealSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal seatSales = AccelAeroCalculator.getDefaultBigDecimalZero();
					halaSales = rs.getBigDecimal("hala");
					insSales = rs.getBigDecimal("ins");
					mealSales = rs.getBigDecimal("meal");
					seatSales = rs.getBigDecimal("seat");
					String channel = rs.getString("channel");
					if (anciByCount) {
						halaSales = halaSales.setScale(0);
						insSales = insSales.setScale(0);
						mealSales = mealSales.setScale(0);
						seatSales = seatSales.setScale(0);
					} else {
						halaSales = halaSales.setScale(2);
						insSales = insSales.setScale(2);
						mealSales = mealSales.setScale(2);
						seatSales = seatSales.setScale(2);
					}
					if (halaSales.compareTo(new BigDecimal(0)) > 0) {
						halaSalesPerCategory.put(channel, halaSales);
					}
					if (insSales.compareTo(new BigDecimal(0)) > 0) {
						insSalesPerCategory.put(channel, insSales);
					}
					if (mealSales.compareTo(new BigDecimal(0)) > 0) {
						mealSalesPerCategory.put(channel, mealSales);
					}
					if (seatSales.compareTo(new BigDecimal(0)) > 0) {
						seatSalesPerCategory.put(channel, seatSales);
					}
				}
				if (halaEnabled) {
					ancSalesByRoute.put("HALA", halaSalesPerCategory);
				}
				if (insEnabled) {
					ancSalesByRoute.put("INSURANCE", insSalesPerCategory);
				}
				if (seatEnabled) {
					ancSalesByRoute.put("SEAT", seatSalesPerCategory);
				}
				if (mealEnabled) {
					ancSalesByRoute.put("MEAL", mealSalesPerCategory);
				}
				return ancSalesByRoute;
			}
		});
		return anciSalesByChannel;
	}

	// ********************** MIS Accounts related queries *************************//

	@SuppressWarnings("unchecked")
	@Override
	public Collection<MISAccountSummaryTO> getMISAccountRefunds(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<MISAccountSummaryTO> accountsRefunds = null;
		sql.append("SELECT   SUM (abs(a.amount)) AS sales, a.nominal_code, b.description "
				+ "FROM t_pax_transaction a,t_pax_trnx_nominal_code b,t_pnr_passenger c, "
				+ "t_reservation d,t_station st,t_region r,t_country cn "
				+ "WHERE d.pnr = c.pnr AND c.pnr_pax_id = a.pnr_pax_id AND d.origin_pos = st.station_code ");
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		sql.append("AND st.country_code = cn.country_code " + "AND cn.region_code = r.region_code ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND r.region_code = '" + searchCriteria.getRegion() + "' ");
		}
		sql.append("AND a.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate() + " 00:00:00', 'dd/mm/yyyy hh24:mi:ss') "
				+ "AND TO_DATE ('" + searchCriteria.getToDate() + "  23:59:59', 'dd/mm/yyyy hh24:mi:ss') "
				+ "AND a.nominal_code = b.nominal_code " + "AND b.nominal_code in(" + searchCriteria.getRefundNominalCodes()
				+ ") " + "GROUP BY a.nominal_code, b.description");
		accountsRefunds = (Collection<MISAccountSummaryTO>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<MISAccountSummaryTO> accRefunds = new ArrayList<MISAccountSummaryTO>();
				MISAccountSummaryTO accSummary = null;
				while (rs.next()) {
					accSummary = new MISAccountSummaryTO();
					accSummary.setNominalCode(new Integer(rs.getInt("nominal_code")));
					accSummary.setSalesType(MISAccountSummaryTO.SALES_TYPE_REFUND);
					accSummary.setAmount(rs.getBigDecimal("sales"));
					accRefunds.add(accSummary);
				}
				return accRefunds;
			}
		});

		return accountsRefunds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<MISAccountSummaryTO> getMISAccountSales(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<MISAccountSummaryTO> accountsSales = null;

		sql.append("SELECT   SUM (ABS (a.amount)) AS sales, a.nominal_code, b.description " + "FROM t_reservation d, "
				+ "t_pnr_passenger c, " + "t_pax_transaction a, " + "t_pax_trnx_nominal_code b, " + "t_station st, "
				+ "t_country cn, " + "t_region r " + "WHERE d.pnr = c.pnr " + "AND c.pnr_pax_id = a.pnr_pax_id "
				+ "AND a.tnx_date BETWEEN TO_DATE ('" + searchCriteria.getFromDate() + " 00:00:00','dd/mm/yyyy hh24:mi:ss' ) "
				+ "AND TO_DATE ('" + searchCriteria.getToDate() + " 23:59:59','dd/mm/yyyy hh24:mi:ss') "
				+ "AND a.nominal_code = b.nominal_code " + "AND a.nominal_code IN (" + searchCriteria.getPaymentNominalCodes()
				+ ") " + "AND d.origin_pos = st.station_code " + "AND st.country_code = cn.country_code "
				+ "AND cn.region_code = r.region_code ");
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		if (searchCriteria.getRegion() != null) {
			sql.append("AND r.region_code = '" + searchCriteria.getRegion() + "' ");
		}

		sql.append("GROUP BY a.nominal_code, b.description");
		accountsSales = (Collection<MISAccountSummaryTO>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<MISAccountSummaryTO> accSales = new ArrayList<MISAccountSummaryTO>();
				MISAccountSummaryTO accSummary = null;
				while (rs.next()) {
					accSummary = new MISAccountSummaryTO();
					accSummary.setNominalCode(new Integer(rs.getInt("nominal_code")));
					accSummary.setSalesType(MISAccountSummaryTO.SALES_TYPE_REVENUE);
					accSummary.setAmount(rs.getBigDecimal("sales"));
					accSales.add(accSummary);
				}
				return accSales;
			}
		});

		return accountsSales;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<MISOutstandingInvoicesTO> getMISAccountOutstandingInvoices(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Collection<MISOutstandingInvoicesTO> outstandingInvoices = null;
		String sql = MISStatementCreator.getMISAccountsOutStandingInvQuery(searchCriteria);
		outstandingInvoices = (Collection<MISOutstandingInvoicesTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<MISOutstandingInvoicesTO> invoicesList = new ArrayList<MISOutstandingInvoicesTO>();
				MISOutstandingInvoicesTO objInvoice = null;
				while (rs.next()) {
					if (rs.getBigDecimal("amount").compareTo(new BigDecimal(0)) > 0) {
						objInvoice = new MISOutstandingInvoicesTO();
						objInvoice.setAgentName(rs.getString("agent_name"));
						objInvoice.setInvoiceAmount(AccelAeroCalculator.formatAsDecimal(rs.getBigDecimal("amount")));
						String invDate = DateFormatUtils.format(rs.getDate("invdate"), "dd-MM-yyyy");
						objInvoice.setInvoiceDate(invDate);
						objInvoice.setInvoiceNumber(rs.getString("invoiceno"));
						invoicesList.add(objInvoice);
					}
				}
				return invoicesList;
			}
		});

		return outstandingInvoices;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, BigDecimal> getMISOutstandingByRegion(MISReportsSearchCriteria searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, BigDecimal> outStandingInvoices = new HashMap<String, BigDecimal>();

		String sql = MISStatementCreator.getMISAccountsOutStandingByRegion(searchCriteria);

		outStandingInvoices = (Map<String, BigDecimal>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, BigDecimal> invoiceTotals = new HashMap<String, BigDecimal>();
				while (rs.next()) {
					if (rs.getBigDecimal("amount") != null) {
						invoiceTotals.put(rs.getString("region_name"), rs.getBigDecimal("amount"));
					}
				}
				return invoiceTotals;
			}
		});

		return outStandingInvoices;
	}

	@Override
	public BigDecimal getMISAccountTotalOutstandingCredits(MISReportsSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		BigDecimal totOutStandingVal = AccelAeroCalculator.getDefaultBigDecimalZero();
		sql.append("SELECT SUM (amount) AS amount  FROM t_agent agn, t_station st, t_country cn,t_region r, "
				+ "(SELECT   a.agent_code AS agent_code, " + "SUM (a.invoice_amount - a.settled_amount) AS amount "
				+ "FROM t_invoice a, t_agent ag " + "WHERE     ag.agent_code = a.agent_code "
				+ "AND a.invoice_date between TO_DATE('" + searchCriteria.getFromDate() + "00:00:00', 'dd/mm/yyyy hh24:mi:ss') "
				+ "and TO_DATE('" + searchCriteria.getToDate() + "23:59:59', 'dd/mm/yyyy hh24:mi:ss') "
				+ "GROUP BY a.agent_code  ORDER BY amount DESC) aaa " + "WHERE agn.station_code = st.station_code "
				+ "AND agn.agent_code = aaa.agent_code ");
		if (searchCriteria.getPos() != null) {
			sql.append("AND st.station_code = '" + searchCriteria.getPos() + "' ");
		}
		sql.append("AND st.country_code = cn.country_code " + "AND cn.region_code = r.region_code ");
		if (searchCriteria.getRegion() != null) {
			sql.append("AND r.region_code = '" + searchCriteria.getRegion() + "' ");
		}

		totOutStandingVal = (BigDecimal) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BigDecimal invoiceTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
				while (rs.next()) {
					if (rs.getBigDecimal("amount") != null) {
						invoiceTotal = rs.getBigDecimal("amount");
					}
				}
				return invoiceTotal;
			}
		});
		return totOutStandingVal;
	}

	// ************************************** MIS Agents queries *******************************************//
	public HashMap<String, BigDecimal> getGSATurnover(MISReportsSearchCriteria searchCriteria, List<String> agentCodes) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		ReportsStatementCreator creator = new ReportsStatementCreator();

		ReportsSearchCriteria reportsSearchCriteria = new ReportsSearchCriteria();
		reportsSearchCriteria.setReportType(ReportsSearchCriteria.COMPANY_PAYMENT_SUMMARY);

		SimpleDateFormat misFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat reportingFormat = new SimpleDateFormat("dd-MMM-yyyy");

		HashMap<String, BigDecimal> turnoverMap = null;
		String fromDate = null;
		String toDate = null;

		try {

			fromDate = reportingFormat.format(misFormat.parse(searchCriteria.getFromDate()));
			toDate = reportingFormat.format(misFormat.parse(searchCriteria.getToDate()));

			reportsSearchCriteria.setDateRangeFrom(fromDate);
			reportsSearchCriteria.setDateRangeTo(toDate);
			Collection<Integer> nominalCodeList = ReservationTnxNominalCode.getPaymentAndRefundNominalCodes();
			Collection<String> paymentTypes = new ArrayList<String>();
			for (Integer code : nominalCodeList) {
				paymentTypes.add(code.toString());
			}
			reportsSearchCriteria.setPaymentTypes(paymentTypes);
			reportsSearchCriteria.setAgents(agentCodes);
			String sql = creator.getCompanyPaymentQuery(reportsSearchCriteria);

			turnoverMap = (HashMap<String, BigDecimal>) jdbcTemplate.query(sql, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap<String, BigDecimal> turnovers = new HashMap<String, BigDecimal>();
					while (rs.next()) {
						String agentCode = rs.getString("agent_code");
						BigDecimal turnOver = rs.getBigDecimal("net_amt");
						turnovers.put(agentCode, turnOver);
					}
					return turnovers;
				}
			});

		} catch (ParseException e) {
			// do nothing @ exception
			log.error("Error pharsing MIS dates", e);
		}

		return turnoverMap;
	}

	public Collection<OutstandingInvoiceInfoTO> getOutstandingInvoiceInfo(MISReportsSearchCriteria searchCriteria,
			String agentCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		ReportsStatementCreator creator = new ReportsStatementCreator();

		ReportsSearchCriteria reportsSearchCriteria = new ReportsSearchCriteria();
		reportsSearchCriteria.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT);

		SimpleDateFormat misFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat reportingFormat = new SimpleDateFormat("dd-MMM-yyyy");

		Collection<OutstandingInvoiceInfoTO> outstandingCollection = null;
		String fromDate = null;
		String toDate = null;

		try {

			fromDate = reportingFormat.format(misFormat.parse(searchCriteria.getFromDate()));
			toDate = reportingFormat.format(misFormat.parse(searchCriteria.getToDate()));

			reportsSearchCriteria.setDateRangeFrom(fromDate);
			reportsSearchCriteria.setDateRangeTo(toDate);
			reportsSearchCriteria.setAgentCode(agentCode);
			String sql = creator.getOutstandingBalanceDetailQuery(reportsSearchCriteria);

			outstandingCollection = (Collection<OutstandingInvoiceInfoTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Collection<OutstandingInvoiceInfoTO> oiList = new ArrayList<OutstandingInvoiceInfoTO>();
					while (rs.next()) {
						String invoiceNumber = rs.getString("invoice_number");
						Date invoiceDate = rs.getDate("invoice_date");
						BigDecimal invoiceAmount = rs.getBigDecimal("invoice_amount");
						BigDecimal settledAmount = rs.getBigDecimal("settled_amount");

						OutstandingInvoiceInfoTO infoTO = new OutstandingInvoiceInfoTO();
						infoTO.setInvoceNumber(invoiceNumber);
						infoTO.setInvoiceDate(invoiceDate);
						infoTO.setInvoiceAmount(invoiceAmount);
						infoTO.setSettledAmount(settledAmount);

						oiList.add(infoTO);
					}
					return oiList;
				}
			});

		} catch (ParseException e) {
			// do nothing @ exception
			log.error("Error pharsing MIS dates", e);
		}

		return outstandingCollection;
	}

	public Integer getNoOfEmployees(String agentCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = MISStatementCreator.getNoOfEmployeesForAgentQuery(agentCode);
		int count = jdbcTemplate.queryForObject(sql, Integer.class);
		return count;
	}

	public Collection<HashMap<String, String>>
			getRouteWiseAgentRevenue(MISReportsSearchCriteria searchCriteria, String agentCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = MISStatementCreator.getRouteWiseAgentRevenueQuery(searchCriteria, agentCode);
		Collection<HashMap<String, String>> routeWiseAgentRevenue = (Collection<HashMap<String, String>>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<HashMap<String, String>> resultCollection = new ArrayList<HashMap<String, String>>();
						while (rs.next()) {
							HashMap<String, String> routeRevenue = new HashMap<String, String>();
							BigDecimal amount = rs.getBigDecimal("amount");
							String segmentCode = rs.getString("segment_code");
							routeRevenue.put("segmentCode", segmentCode);
							routeRevenue.put("amount",
									AccelAeroCalculator.formatAsDecimal(amount) + " " + AppSysParamsUtil.getBaseCurrency());
							routeRevenue.put("amountAsValue", AccelAeroCalculator.formatAsDecimal(amount));

							resultCollection.add(routeRevenue);
						}
						return resultCollection;
					}
				});

		return routeWiseAgentRevenue;
	}

	public Collection<HashMap<String, String>>
			getStaffWiseAgentRevenue(MISReportsSearchCriteria searchCriteria, String agentCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = MISStatementCreator.getStaffWiseAgentRevenueQuery(searchCriteria, agentCode);
		Collection<HashMap<String, String>> routeWiseAgentRevenue = (Collection<HashMap<String, String>>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<HashMap<String, String>> resultCollection = new ArrayList<HashMap<String, String>>();
						while (rs.next()) {
							HashMap<String, String> routeRevenue = new HashMap<String, String>();
							BigDecimal amount = rs.getBigDecimal("amount");
							String staffName = rs.getString("display_name");
							routeRevenue.put("staffName", staffName);
							routeRevenue.put("amount",
									AccelAeroCalculator.formatAsDecimal(amount) + " " + AppSysParamsUtil.getBaseCurrency());

							resultCollection.add(routeRevenue);
						}
						return resultCollection;
					}
				});

		return routeWiseAgentRevenue;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Integer> getMISDashSalesByChannelPaxCount(MISReportsSearchCriteria searchCriteria) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Map<String, Integer> salesByChannelMap = null;

		String sql = MISStatementCreator.getMISDashSalesByChannelPaxCount(searchCriteria);
		salesByChannelMap = (Map<String, Integer>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> salesByChn = new HashMap<String, Integer>();
				while (rs.next()) {
					if (salesByChn.containsKey(rs.getString("channel"))) {
						Integer sales = salesByChn.get(rs.getString("channel"));
						sales = sales + rs.getInt("sales");
						salesByChn.put(rs.getString("channel"), sales);
					} else {
						salesByChn.put(rs.getString("channel"), rs.getInt("sales"));
					}
				}
				return salesByChn;
			}
		});

		return salesByChannelMap;
	}

	private void setEanbledAncillary() {
		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_SEAT_MAP))) {
			seatEnabled = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_TRAVEL_INSURANCE))) {
			insEnabled = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_MEAL))) {
			mealEnabled = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_AIRPORT_SERVICES))) {
			halaEnabled = true;
		}
	}

	private void validateMap(Map<String, List<BigDecimal>> map) {

		if (map != null && map.size() > 2) {
			Set<String> keys = map.keySet();

			List<String> rmvList = new ArrayList<String>();
			if (keys != null && keys.size() > 2) {
				Iterator<String> it = keys.iterator();
				String key = null;
				BigDecimal rmvValue = null;
				if (it.hasNext()) {
					key = it.next();
					rmvValue = (BigDecimal) map.get(key).get(0);
					rmvList.add(key);
				}
				if (it.hasNext()) {
					key = it.next();
					map.get(key).add(rmvValue);
					rmvValue = map.get(key).get(0);
					map.get(key).remove(0);

				}
				if (it.hasNext()) {
					key = it.next();
					map.get(key).add(rmvValue);
				}

				for (String rmKey : rmvList) {
					map.remove(rmKey);
				}

			}
		}
	}

}