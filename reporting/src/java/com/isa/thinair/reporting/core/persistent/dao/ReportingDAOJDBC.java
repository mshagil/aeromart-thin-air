package com.isa.thinair.reporting.core.persistent.dao;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.dto.AgentUserRolesDTO;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;

public interface ReportingDAOJDBC {

	public Page<FlightInventorySummaryDTO> getFlightsForInventoryUpdating(FlightSearchCriteria criteria, int startIndex,
			int pageSize);

	public FCCAgentsSeatMvDTO getFCCAgentsSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList);

	public FCCAgentsSeatMvDTO getOwnerAgentWiseFCCSeatMovementSummary(int flightId, String cabinClassCode, boolean loadPNRList);

	public FCCAgentsSeatMvDTO
			getFCCAgentsSeatMovementSummaryForLogicalCC(int flightId, String logicalCCCode, boolean loadPNRList);

	public Page<String> getFlightsForFlightLoadAnalysis(FlightSearchCriteria criteria, int startIndex, int pageSize);

	public Page getRolesForPrivilege(AgentUserRolesDTO agentUserRolesDTO,
			int startIndex, int pageLength);

}
