/**
 * 
 */
package com.isa.thinair.reporting.core.util;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author sanjeewaf Utility Classes used by Reporting framework
 */
public class ReportUtils {

	private static Log log = LogFactory.getLog(ReportUtils.class);

	/**
	 * Method returns a string contains (key = 'value' OR key = 'value' ..) , replacement for IN clause (ORA error when
	 * IN( ) clause cannot exceed 1000 records.)
	 * 
	 * @param key
	 * @param valueList
	 * @param negateIN
	 *            Makes this method a NOT IN() method
	 * @return
	 */
	public static String getReplaceStringForIN(String key, Collection<String> valueList, boolean negateIN) {

		String in_notIn = "=";
		String or_and = " OR ";

		// Changing this to a NOT IN() method
		if (negateIN) {
			in_notIn = "<>";
			or_and = " AND ";
		}

		StringBuilder oraStr = new StringBuilder("( ");
		if (valueList != null && !key.equals("")) {
			for (String value : valueList) {
				oraStr.append(key).append(in_notIn).append("'").append(value).append("'").append(or_and);
			}
		} else {
			log.error("Invalid database Statement" + "key :" + key + "ValueList : " + valueList);
		}

		String returnStr = oraStr.toString();
		if (oraStr.toString().endsWith(or_and)) {
			returnStr = returnStr.substring(0, returnStr.lastIndexOf(or_and));
		}
		returnStr += " )";

		return returnStr;
	}
}