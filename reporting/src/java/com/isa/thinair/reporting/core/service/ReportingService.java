/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.reporting.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Byorn
 * 
 *         Security Module's service interface
 * @isa.module.service-interface module-name="reporting" description="inventory module"
 */
public class ReportingService extends DefaultModule {

}
