/*
 * 
==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * 
===============================================================================
 */
package com.isa.thinair.reporting.core.persistent.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateReservationFlows;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.ModifyOperation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.FLOWN_FROM;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypes;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypesDesc;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.core.util.ReportUtils;
import com.isa.thinair.reportingframework.api.dto.ReportStructure;

/**
 * Class to create the query strings for reports
 * 
 * @author Vinothini
 */
public class ReportsStatementCreator {

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Method to create the query for the Performance Of Sales Staff Summary Report.
	 * 
	 * @param reportSearchCriteria
	 */
	public String getPerformanceOfSalesStaffSummaryQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getUsers());
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)
				|| (singleDataValue.length() == 0)) {
			return null;
		}

		String sql = null;
		Iterator<String> iter = null;
		Collection<String> paymentTypes = reportsSearchCriteria.getPaymentTypes();
		if (paymentTypes != null) {
			iter = paymentTypes.iterator();
		}

		if (paymentTypes != null && paymentTypes.size() == 2) {
			sql = " AND EXISTS (  SELECT 'X' " + " FROM t_pax_transaction t1 " + " WHERE t1.pnr_pax_id = p1.pnr_pax_id "
			// + " AND (t1.PAYMENT_CARRIER_CODE is null or t1.PAYMENT_CARRIER_CODE = '" // Remove this since
			// only origin user should check
			// + AppSysParamsUtil.getDefaultCarrierCode() + "') "
					+ "  AND t1.nominal_code IN ("
					+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes()) + ") "
					+ " AND ABS(t1.AMOUNT) > 0 " + " UNION" + " SELECT 'X' " + // sales from external
					" FROM t_pax_ext_carrier_transactions exttnx " + " WHERE exttnx.pnr_pax_id  = p1.pnr_pax_id "
					+ " AND EXTTNX.NOMINAL_CODE IN ("
					+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes()) + ") "
					+ " AND ABS(exttnx.AMOUNT)   > 0 " + " ) ";
		} else {

			String paymentOption = null;
			while (iter.hasNext()) {

				paymentOption = iter.next();

				// fully paid booking
				if ("1".equals(paymentOption)) {

					sql = " AND EXISTS (select 'X' from t_pax_transaction t "
							+ " where r1.status='CNF' and p1.pnr_pax_id=t.pnr_pax_id AND " + " p1.pnr=r1.pnr "
							// + " AND (t.PAYMENT_CARRIER_CODE is null or t.PAYMENT_CARRIER_CODE = '"
							// + AppSysParamsUtil.getDefaultCarrierCode() + "') " // Remove this since only origin user
							// should check
							+ " group by p1.pnr having sum(t.amount)<=0 " + " UNION	" + " SELECT 'X' "
							+ " FROM T_PAX_EXT_CARRIER_TRANSACTIONS exttnx" + " WHERE r1.status  ='CNF'"
							+ " AND p1.pnr_pax_id=exttnx.pnr_pax_id " + " AND p1.pnr       =r1.pnr " + " GROUP BY P1.PNR "
							+ " HAVING SUM(exttnx.amount)<=0" + ") "; // TODO this is not accurate as we cannot
																		// distinguish at this level the partially paid
																		// external bookings
				}

				// partially paid booking
				if ("2".equals(paymentOption)) {

					sql = " AND EXISTS (select 'X' from t_pax_transaction t "
							+ " where r1.status='CNF' and p1.pnr_pax_id=t.pnr_pax_id AND "
							+ " p1.pnr=r1.pnr group by p1.pnr having sum(t.amount)>0)";
					// TODO - we cannot correctly distinguish partially paid external bookings and fully paid external
					// bookings
				}
			}
		}

		StringBuilder sb = new StringBuilder();
		log.debug("inside getPerformanceOfSalesStaffSummaryQuery");

		sb.append(" SELECT   r.origin_agent_code AS agent_code, ag.agent_name AS agent_name, ");
		sb.append("      r.origin_user_id AS login_id, u.display_name AS display_name, ");
		sb.append("        COUNT(DISTINCT p.pnr) AS no_of_res, ");
		sb.append(
				"        COUNT(DISTINCT (DECODE (p.pax_type_code, 'AD', p.pnr_pax_id, 'CH', p.pnr_pax_id, NULL))) AS no_of_pax, ");
		sb.append("        SUM(p.total_fare + p.total_tax + p.total_surcharges ");
		sb.append("           + p.total_cnx + p.total_mod + p.total_adj) AS total_charges, ");
		sb.append("        SUM(-1 * p.total_discount) AS total_discount ");
		sb.append("   FROM t_reservation r, t_pnr_passenger p, t_agent ag, t_user u ");
		// sb.append(" WHERE r.origin_user_id IN (" + singleDataValue + ") ");

		sb.append(" WHERE ");
		sb.append(ReportUtils.getReplaceStringForIN("r.origin_user_id", reportsSearchCriteria.getUsers(), false) + " ");

		sb.append("    AND r.pnr IN ( ");
		sb.append("            SELECT pnr ");
		sb.append("              FROM t_reservation r1 ");
		sb.append("             WHERE EXISTS ( ");
		sb.append("                      SELECT 'x' ");
		sb.append("                        FROM t_pnr_passenger p1 ");
		sb.append("                        WHERE p1.pnr = r1.pnr ");

		sb.append(sql);
		/*
		 * sb.append("                          AND EXISTS ( ");
		 * sb.append("                                  SELECT 'X' ");
		 * sb.append("                                    FROM t_pax_transaction t1 ");
		 * sb.append("                                  WHERE t1.pnr_pax_id = p1.pnr_pax_id ");
		 * sb.append("                                     AND t1.nominal_code IN ");
		 * sb.append("                                                 (19, 18, 15, 16, 28, 17, 6)) ");
		 */
		sb.append(" ) ");

		sb.append("               AND r1.booking_timestamp ");
		sb.append("                       BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom() + " 00:00:00', ");
		sb.append("                                        'dd-mon-yyyy HH24:mi:ss') ");
		sb.append("                           AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', ");
		sb.append("                                       'dd-mon-yyyy HH24:mi:ss') ");
		sb.append(" 	AND R.DUMMY_BOOKING  <> 'Y' )");
		sb.append("     AND r.pnr = p.pnr ");
		sb.append("     AND r.origin_agent_code = ag.agent_code ");
		sb.append("     AND r.origin_user_id = u.user_id ");
		// if(reportsSearchCriteria.getAgentCode()!= null && !reportsSearchCriteria.getAgentCode().isEmpty()){
		// sb.append(" AND (ag.gsa_code= '"+reportsSearchCriteria.getAgentCode()+"' OR ag.agent_code =
		// '"+reportsSearchCriteria.getAgentCode()+"' )");
		// }
		sb.append(" GROUP BY r.origin_agent_code, ag.agent_name, r.origin_user_id, u.display_name ");
		sb.append(" ORDER BY ag.agent_name, u.display_name ");

		if (log.isDebugEnabled()) {
			log.debug(sb.toString());
		}
		return sb.toString();
	}

	/**
	 * Method to create the query for the Customer Travel History Detail Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getCustomerTravelHistoryDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT	cus.customer_id AS CUSTOMER_ID, ");
		sb.append(" cus.email_id AS LOGIN_ID, cus.title || ' ' || ");
		sb.append("	cus.first_name || ' ' || cus.last_name AS CUSTOMER_NAME, ");
		sb.append(" ROUND(MONTHS_BETWEEN(SYSDATE, cus.date_of_birth) / 12) ");
		sb.append(" AS AGE,	cus.gender AS GENDER, ");
		sb.append(" con.country_name AS COUNTRY_RES, ");
		sb.append(" TO_CHAR(flgSeg.est_time_departure_zulu, 'DD-MON-YYYY') AS DATE_TRAVEL, ");
		sb.append(" flg.flight_number AS FLIGHT_NUMBER, ");
		sb.append(" SUBSTR(flgSeg.segment_code, 0 , 3) AS SECTOR_FROM, ");
		sb.append(" SUBSTR(flgSeg.segment_code, ");
		sb.append(" (LENGTH(flgSeg.segment_code) - 2), 3) AS SECTOR_TO ");
		sb.append(" FROM T_CUSTOMER cus, T_COUNTRY con, T_FLIGHT flg, ");
		sb.append(" T_FLIGHT_SEGMENT flgSeg, T_PNR_SEGMENT pnrSeg, T_RESERVATION res, T_RESERVATION_CONTACT rCon, ");
		sb.append(" T_PNR_PASSENGER pax ");
		sb.append(" WHERE flgSeg.est_time_departure_zulu BETWEEN TO_DATE('");
		sb.append(reportsSearchCriteria.getDateRangeFrom() + "', 'DD-MON-YYYY HH24:mi:ss' ) ");
		sb.append(" AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss') ");
		sb.append(" AND	cus.customer_id = '" + reportsSearchCriteria.getCustomerId() + "' ");

		if (reportsSearchCriteria.isBySector()) {
			sb.append(" AND UPPER(SUBSTR(flgSeg.segment_code, 0 , 3)) = ");
			sb.append("UPPER('" + reportsSearchCriteria.getSectorFrom());
			sb.append("') ");
			if (reportsSearchCriteria.getSectorTo().equals("ALL")) {

			} else {
				sb.append("  AND UPPER(SUBSTR(flgSeg.segment_code, (LENGTH(flgSeg.segment_code) - 2), 3)) = UPPER('");
				sb.append(reportsSearchCriteria.getSectorTo() + "') ");
			}

		}

		sb.append(" AND flg.flight_id = flgSeg.flight_id ");
		sb.append(" AND	flgSeg.flt_seg_id = pnrSeg.flt_seg_id ");
		sb.append(" AND	pnrSeg.pnr = res.pnr ");
		sb.append(" AND	rCon.pnr = res.pnr ");
		sb.append(" AND	rCon.customer_id = cus.customer_id ");
		sb.append(" AND	cus.country_code = con.country_code ");
		sb.append(" GROUP BY TO_CHAR(flgSeg.est_time_departure_zulu, 'DD-MON-YYYY'), ");
		sb.append(" flg.flight_number, SUBSTR(flgSeg.segment_code, 0 , 3), ");
		sb.append(" SUBSTR(flgSeg.segment_code, ");
		sb.append(" (LENGTH(flgSeg.segment_code) - 2), 3), ");
		sb.append(" cus.customer_id , cus.email_id, cus.title || ' ' || ");
		sb.append(" cus.first_name || ' ' || cus.last_name, ");
		sb.append(" cus.date_of_birth, cus.gender, con.country_name ");
		sb.append(" ORDER BY TO_CHAR(flgSeg.est_time_departure_zulu, 'DD-MON-YYYY')");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Airport Tax Detail Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getAirportTaxQuery(ReportsSearchCriteria reportsSearchCriteria) {
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DECODE(C.ARR_DEP,'D',SUBSTR(FS.segment_code,1,3),SUBSTR(FS.segment_code,-3)) AIRPORT, ");
		sb.append(" c.charge_description,sum(pfst.amount) amount ");
		sb.append("	FROM T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT PS , ");
		sb.append(" T_PNR_PAX_FARE_SEGMENT PPFS,T_PNR_PAX_OND_CHARGES PFT, ");
		sb.append(" t_pnr_pax_seg_charges PFST, ");
		sb.append(" T_CHARGE C,T_CHARGE_RATE CR ");
		sb.append(" WHERE FS.flt_seg_id=PS.flt_seg_id          AND ");
		sb.append(" PS.pnr_seg_id=PPFS.pnr_seg_id        AND ");
		sb.append("  PPFS.ppf_id=PFT.ppf_id               AND ");
		sb.append(" PPFS.ppfs_id=PFST.ppfs_id            AND");
		sb.append(" PFT.charge_rate_id=CR.charge_rate_id AND");
		sb.append("  PFT.pft_id=PFST.pft_id               AND  ");
		sb.append(" CR.charge_code=C.charge_code         AND ");
		sb.append(" pft.charge_date BETWEEN TO_DATE('");
		sb.append(reportsSearchCriteria.getDateRangeFrom() + "', 'DD-MON-YYYY HH24:mi:ss' ) ");
		sb.append(" AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss') and ");
		sb.append(" DECODE(C.ARR_DEP,'D',SUBSTR(FS.segment_code,1,3),SUBSTR(FS.segment_code,-3)) IN ");
		sb.append(
				"(SELECT DECODE(C.ARR_DEP,'D',DECODE(SUBSTR(OC.ond_code,1,3),'***',SUBSTR(FS.segment_code,1,3),SUBSTR(OC.ond_code,1,3)), ");
		sb.append("DECODE(SUBSTR(OC.ond_code,-3),'***',SUBSTR(FS.segment_code,-3),SUBSTR(OC.ond_code,1,3))) ");
		sb.append("FROM T_OND_CHARGE OC ");
		sb.append("WHERE OC.charge_code=C.charge_code) ");
		sb.append("group by DECODE(C.ARR_DEP,'D',SUBSTR(FS.segment_code,1,3),SUBSTR(FS.segment_code,-3)), c.charge_description ");
		sb.append("order by 1,2 ");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Fare Discounts Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getFareDiscountsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();

		sb.append("			SELECT res.pnr, res.segment_code,  res.charge_date,  res.value_percentage,  res.origin_user_id, ");
		sb.append("  res.origin_agent_code,  aud.user_notes FROM ");
		sb.append(
				"		  (SELECT DISTINCT a.pnr,  c.segment_code,   e.charge_date,   e.value_percentage,  a.origin_user_id,  a.origin_agent_code,");
		sb.append("   h.fare_id,RANK () OVER (PARTITION BY a.pnr ORDER BY a.pnr, e.charge_date DESC) AS pnrid");
		sb.append(
				"  FROM t_reservation a, t_pnr_segment b,  t_flight_segment c,   t_pnr_pax_fare_segment d,  t_pnr_pax_ond_charges e,");

		sb.append("		   t_pnr_pax_seg_charges f,  t_pnr_passenger g,   t_pnr_pax_fare h,  t_flight i");
		sb.append("  WHERE a.pnr = b.pnr  AND a.pnr = g.pnr AND b.flt_seg_id = c.flt_seg_id  AND d.pnr_seg_id = b.pnr_seg_id ");
		sb.append("		  AND h.ppf_id  = e.ppf_id  AND f.pft_id  = e.pft_id AND f.ppfs_id  = d.ppfs_id");
		sb.append("    AND g.pnr_pax_id  = h.pnr_pax_id  AND h.ppf_id = d.ppf_id  AND c.flight_id = i.flight_id");
		sb.append(" AND e.charge_group_code = '" + ReservationInternalConstants.ChargeGroup.FAR + "'  AND e.discount < 0 ");
		sb.append(" AND b.status ='" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' ");

		sb.append(" AND e.charge_date BETWEEN TO_DATE('" + fromDate + "', 'DD-MON-YYYY HH24:mi:ss' )  AND ");
		sb.append(" TO_DATE('" + toDate + "', 'DD-MON-YYYY HH24:mi:ss') ");

		if (null != reportsSearchCriteria.getUsers() && !reportsSearchCriteria.getUsers().isEmpty()) {
			sb.append(" AND " + ReportUtils.getReplaceStringForIN("a.origin_user_id", reportsSearchCriteria.getUsers(), false));
		}

		if (null != reportsSearchCriteria.getAgents() && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append("  AND "
					+ ReportUtils.getReplaceStringForIN("a.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		}

		sb.append(
				" ) res, (SELECT ra.pnr,  ra.TIMESTAMP,  ra.user_notes,  RANK () OVER (PARTITION BY ra.pnr ORDER BY ra.pnr ASC, ra.TIMESTAMP DESC) AS pnrid ");
		sb.append("   FROM t_reservation_audit ra  WHERE 1 = 1  AND ra.user_notes LIKE '%FareDiscountNote:%' ) aud ");

		if (reportsSearchCriteria.isOverriddenDiscountDataOnly() || reportsSearchCriteria.isNormalDiscountDataOnly()) {
			sb.append(" , t_fare_rule fr, t_ond_fare ofare ");
		}

		sb.append("	WHERE res.pnr = aud.pnr	AND res.pnrid = aud.pnrid ");
		if (reportsSearchCriteria.isOverriddenDiscountDataOnly()) {
			sb.append(" AND (res.value_percentage < fr.fare_discount_min OR res.value_percentage > fr.fare_discount_max) ");
		} else if (reportsSearchCriteria.isNormalDiscountDataOnly()) {
			sb.append(" AND (res.value_percentage >= fr.fare_discount_min AND res.value_percentage <= fr.fare_discount_max)");
		}

		if (reportsSearchCriteria.isOverriddenDiscountDataOnly() || reportsSearchCriteria.isNormalDiscountDataOnly()) {
			sb.append(" AND res.fare_id = ofare.fare_id AND fr.fare_rule_id = ofare.fare_rule_id");
		}

		sb.append(" ORDER BY res.pnr, res.charge_date ");

		return sb.toString();

	}

	/**
	 * Method to create the query for the Airport Tax Detail Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getAgentCommisionQuery(ReportsSearchCriteria reportsSearchCriteria) {
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)) {
			return null;
		}
		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());

		/*
		 * sb.append(" select  "); sb.append(" case when agent_type_code ='GSA' then "); sb.append(" PAY.AGENT_CODE ");
		 * sb.append(" when report_to_gsa in ('N') then "); sb.append(" '' ");
		 * sb.append(" when report_to_gsa in ('Y') then "); sb.append(" (select agent_code ");
		 * sb.append(" from t_agent "); sb.append(" where agent_type_code='GSA' and "); sb.append(
		 * " TERRITORY_CODE in(select TERRITORY_CODE from t_station where station_code=pay.station_code)and rownum=1) "
		 * ); sb.append(" end GSA, ");
		 * sb.append(" pay.agent_code,pay.AGENT_NAME,pay.payment,nvl(handling_charge,0)handling_charge, "); sb.append(
		 * " decode(value_percentage_flag,'P',(pay.payment * (pay.commission / 100)),(pay.commission)) commission,  ");
		 * sb.append(
		 * " pay.payment - ( decode(value_percentage_flag,'P',(pay.payment * (pay.commission / 100)),(pay.commission)) )- NVL (handling_charge, 0) net "
		 * ); sb.append(" from "); sb.
		 * append(" (SELECT A.AGENT_CODE,a.AGENT_NAME,a.agent_type_code,a.report_to_gsa,station_code,SUM(AMOUNT)PAYMENT "
		 * ); sb.append(" , agcr.commission, agcr.value_percentage_flag ");
		 * sb.append("  FROM t_agent_transaction T,T_AGENT A , t_agent_commission_rate agcr ");
		 * sb.append(" WHERE A.agent_code=T.agent_code    AND  ");
		 * sb.append(ReportUtils.getReplaceStringForIN("A.AGENT_CODE", reportsSearchCriteria.getAgents(), false) +
		 * "  AND  "); sb.append(" TNX_DATE BETWEEN TO_DATE('");
		 * sb.append(reportsSearchCriteria.getDateRangeFrom()+"', 'DD-MON-YYYY HH24:mi:ss' ) ");
		 * sb.append(" AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss') ");
		 * sb.append(" and a.agent_commission_code = agcr.agent_commission_code ");
		 * sb.append(" AND NOMINAL_CODE IN(1) "); sb.append(
		 * " GROUP BY A.AGENT_CODE,a.AGENT_NAME,a.agent_type_code,a.report_to_gsa,station_code,agcr.commission,agcr.value_percentage_flag)pay, "
		 * ); sb.append(" (SELECT  A.origin_agent_code AGENT_CODE,SUM(d.amount)handling_charge ");
		 * sb.append(" FROM           t_reservation a, "); sb.append("                t_pnr_passenger b,");
		 * sb.append("                t_pnr_pax_fare c , "); sb.append("                t_pnr_pax_ond_charges d ");
		 * sb.append(" WHERE           a.pnr=b.pnr "); sb.append("   AND b.pnr_pax_id = c.pnr_pax_id ");
		 * sb.append("   AND c.ppf_id = d.ppf_id   and");
		 * sb.append(ReportUtils.getReplaceStringForIN("A.origin_agent_code", reportsSearchCriteria.getAgents(), false)
		 * + "  "); sb.append(" AND d.charge_date between TO_DATE('");
		 * sb.append(reportsSearchCriteria.getDateRangeFrom()+"', 'DD-MON-YYYY HH24:mi:ss' ) ");
		 * sb.append(" AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss')  and ");
		 * sb.append(" CHARGE_RATE_ID in (select CHARGE_RATE_ID from t_charge_rate ");
		 * sb.append("      where charge_code in (select charge_code from t_charge ");
		 * sb.append("       where charge_category_code='POS')) "); sb.append("      GROUP BY  origin_agent_code)chg ");
		 * sb.append("      where pay.agent_code=chg.agent_code(+)   "); sb.append("      order by 1,2  ");
		 */

		StringBuilder sb = new StringBuilder();

		if (reportsSearchCriteria.isReportViewNew()) {
			sb.append(RevenueReportsStatementCreator.getNewAgentCommissionQuery(reportsSearchCriteria));
		} else {
			sb.append(
					"SELECT t.agent_code, A.agent_name, -1 * sum( t.AMOUNT) as sum, -1 * sum(t.LOCALAMT) as sum_local, SUM(t.far_amt) as total_fare, SUM(t.tot_charges) as total_charges, SUM(t.commission) as commission, ");
			sb.append("a.account_code, a.currency_code , (t.rate/100) as rate ");
			sb.append("				FROM ( ");
			sb.append(
					"				SELECT T.AGENT_CODE AS AGENT_CODE, p.pnr, T.AMOUNT AS AMOUNT, pbrk.total_amount_paycur as LOCALAMT, ");
			sb.append(
					"                        mtb.far_amt , (mtb.tax_amt + mtb.sur_amt + mtb.cnx_amt + mtb.mod_amt + mtb.adj_amt ) as tot_charges,");
			sb.append(
					"                        DECODE(ACR.value_percentage_flag, null, 0, 'P', (mtb.far_amt* ACR.commission)/100, 'V', ACR.commission  ) as commission, ");
			sb.append("                        ACR.commission as rate ");
			sb.append(
					"				FROM T_PAX_TRANSACTION T, T_pnr_passenger p, t_pax_txn_breakdown_summary pbrk, t_mis_txn_breakdown_summary mtb, ");
			sb.append("				        (SELECT agc.agent_commission_rate_id, agc.agent_commission_code, ag.agent_code, ");
			sb.append("                                agc.value_percentage_flag, agc.commission ");
			sb.append(
					"                           FROM t_agent_commission_rate agc,  t_agent ag  WHERE ag.agent_commission_code = agc.agent_commission_code  AND agc.effective_from_date < sysdate  AND agc.effective_to_date >  sysdate AND agc.status = 'ACT') ACR ");
			sb.append("				WHERE t.pnr_pax_id = p.pnr_pax_id  ");
			sb.append("						AND t.agent_code = ACR.agent_code (+) ");
			sb.append("				AND t.txn_id = mtb.pax_txn_id (+) ");
			sb.append("						AND t.txn_id = pbrk.pax_txn_id and T.NOMINAL_CODE IN ('19', '25') and ");
			sb.append("						T.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'DD-MON-YYYY HH24:mi:ss' )  AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("			    ) T, T_AGENT A ");
			sb.append("				where A.AGENT_CODE = T.AGENT_CODE ");
			sb.append("             AND  "
					+ ReportUtils.getReplaceStringForIN("A.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
			sb.append("				AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
			sb.append("group by t.agent_code,A.agent_name, a.account_code, a.currency_code , t.rate  ");
		}
		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Breakdown Summary Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getReservationBreakdownSummaryQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)
				|| (singleDataValue.length() == 0)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT  res.origin_agent_code agent_code, res.agent_name, ");
		sb.append("			res.fare AS FARE, res.tax AS TAX, res.sch AS SURCHARGE, ");
		sb.append("			(NVL(res.fare, 0) + NVL(res.tax, 0) + NVL(sch, 0)) as TOTAL, ");
		sb.append("			NVL(res_refund.refund, 0) REFUND ");
		sb.append("		FROM (SELECT  a.origin_agent_code, b.agent_name, ");
		sb.append("					SUM(a.total_fare) fare, SUM(a.total_tax) tax, ");
		sb.append("					SUM(a.total_surcharges) sch ");
		sb.append("			   	FROM    T_RESERVATION a , T_AGENT b ");
		sb.append("				WHERE   a.origin_agent_code = b.agent_code ");
		sb.append("					AND   ");
		sb.append(ReportUtils.getReplaceStringForIN("a.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append("					AND     a.BOOKING_TIMESTAMP  BETWEEN (TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ "00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append("					AND     (TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
				+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");
		sb.append("					GROUP BY a.origin_agent_code, b.agent_name) res ");
		sb.append("		LEFT OUTER JOIN ");
		sb.append(" 		(SELECT r.origin_agent_code, sum(t.amount) refund ");
		sb.append(" 			FROM    T_RESERVATION r, T_PNR_PASSENGER p, T_PAX_TRANSACTION t");
		sb.append(" 			WHERE   r.pnr = p.pnr ");
		sb.append(" 				AND     p.pnr_pax_id = t.pnr_pax_id  ");
		sb.append("					AND  ");
		sb.append(ReportUtils.getReplaceStringForIN("r.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append(" 				AND     r.BOOKING_TIMESTAMP  BETWEEN (TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ "00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append(" 				AND     (TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
				+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");
		sb.append("					AND     t.nominal_code in ("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getRefundTypeNominalCodes()) + ")");
		sb.append("				GROUP BY r.origin_agent_code) res_refund");
		sb.append(" 		ON res.origin_agent_code = res_refund.origin_agent_code");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Breakdown Detail Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getReservationBreakdownDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String strAgent = reportsSearchCriteria.getAgentCode();
		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String strSearchFrom = reportsSearchCriteria.getFrom();
		String strSearchTo = reportsSearchCriteria.getTo();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();
		// String strOperationType = reportsSearchCriteria.getOperationType();

		StringBuilder sb = new StringBuilder();
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)) {
			return null;
		}

		String strSegment = reportsSearchCriteria.getSegment();
		if (strSegment != null && strSegment.equals("SEGMENT")) {
			// change query
			sb.append("SELECT     PNR, PAX_NAME, TRAVEL_DATE, FLIGHT_NUMBER, STATUS, AGENT_USER, ");
			sb.append("				BOOKED_DATE, FARE, TAX, SURCHARGE, (FARE + TAX + SURCHARGE) AS TOTAL_AMOUNT ");
			sb.append("	FROM  ");
			sb.append("		(SELECT  	g.pnr_pax_id, (g.first_name || ' ' || g.last_name) AS PAX_NAME, ");
			sb.append("					a.pnr AS PNR, c.segment_code AS SEGMENT_CODE, ");
			sb.append("				SUM(DECODE(e.charge_group_code, 'FAR', f.amount, 0)) AS FARE, ");
			sb.append("       		SUM(DECODE(e.charge_group_code, 'TAX', f.amount, 0)) AS TAX, ");
			sb.append("     		SUM(DECODE(e.charge_group_code, 'SUR', f.amount, 'INF', f.amount, 0)) AS SURCHARGE, ");
			sb.append("          	a.booking_timestamp AS BOOKED_DATE, c.est_time_departure_zulu AS TRAVEL_DATE, ");
			sb.append("         	i.flight_number AS FLIGHT_NUMBER, b.status AS STATUS, ");
			sb.append("           	a.origin_agent_code AS AGENT_USER ");
			sb.append("     	FROM t_reservation a, ");
			sb.append("           		t_pnr_segment b, t_flight_segment c, t_pnr_pax_fare_segment d, ");
			sb.append("              	t_pnr_pax_ond_charges e, t_pnr_pax_seg_charges f, ");
			sb.append("             	t_pnr_passenger g,t_pnr_pax_fare h, t_flight i ");
			sb.append("        WHERE a.pnr = b.pnr ");
			sb.append(" 			AND   a.pnr=g.pnr ");
			sb.append("   			AND b.flt_seg_id = c.flt_seg_id  AND    ");
			sb.append(ReportUtils.getReplaceStringForIN("a.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
			sb.append("   			AND d.pnr_seg_id = b.pnr_seg_id ");
			sb.append("  			AND d.ppf_id = h.ppf_id ");
			sb.append("   			AND f.pft_id = e.pft_id ");
			sb.append("           	AND f.ppfs_id=d.ppfs_id ");
			sb.append("           	AND g.pnr_pax_id = h.pnr_pax_id ");
			sb.append("           	AND h.ppf_id = d.ppf_id ");
			sb.append("          	AND b.status_mod_date BETWEEN (TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append("        		AND	    (TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append("           	AND c.flight_id = i.flight_id ");
			sb.append("          	AND SUBSTR(c.segment_code, 0, 3) = UPPER('" + strSearchFrom + "') ");
			sb.append("         	AND SUBSTR(c.segment_code, (LENGTH(segment_code) - 2), 3) = UPPER('" + strSearchTo + "') ");
			sb.append("    	GROUP BY g.pnr_pax_id, (g.first_name || ' ' || g.last_name),");
			sb.append("              a.pnr, c.segment_code, a.booking_timestamp, c.est_time_departure_zulu,  ");
			sb.append("            i.flight_number, b.status, a.origin_agent_code 					) res  ");
			sb.append(" 	ORDER BY PNR ");

		} else if (strSegment != null && strSegment.equals("FLIGHT")) {

			sb.append("SELECT      PNR, PAX_NAME, TRAVEL_DATE,SEGMENT_CODE, STATUS, AGENT_USER,");
			sb.append("            BOOKED_DATE, FARE, TAX, SURCHARGE, (FARE + TAX + SURCHARGE) AS TOTAL_AMOUNT ");
			sb.append("            FROM  ");
			sb.append("				(SELECT  g.pnr_pax_id, (g.first_name || ' ' || g.last_name) AS PAX_NAME, ");
			sb.append("                    	a.pnr AS PNR, c.segment_code AS SEGMENT_CODE, ");
			sb.append("                    	SUM(DECODE(e.charge_group_code, 'FAR', f.amount, 0)) AS FARE, ");
			sb.append("						SUM(DECODE(e.charge_group_code, 'TAX', f.amount, 0)) AS TAX, ");
			sb.append(
					"						SUM(DECODE(e.charge_group_code, 'SUR', f.amount, 'INF', f.amount, 0)) AS SURCHARGE, ");
			sb.append("						a.booking_timestamp AS BOOKED_DATE, c.est_time_departure_zulu AS TRAVEL_DATE, ");
			sb.append("						i.flight_number AS FLIGHT_NUMBER, b.status AS STATUS, ");
			sb.append("						a.origin_agent_code AS AGENT_USER ");
			sb.append("					FROM t_reservation a, t_pnr_segment b, ");
			sb.append("						t_flight_segment c, t_pnr_pax_fare_segment d, ");
			sb.append("						t_pnr_pax_ond_charges e, t_pnr_pax_seg_charges f, ");
			sb.append("						t_pnr_passenger g, t_pnr_pax_fare h,  t_flight i ");
			sb.append("					WHERE a.pnr = b.pnr ");
			sb.append("						AND   a.pnr = g.pnr ");
			sb.append("						AND b.flt_seg_id = c.flt_seg_id   AND   ");
			sb.append(ReportUtils.getReplaceStringForIN("a.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
			sb.append("						AND d.pnr_seg_id = b.pnr_seg_id ");
			sb.append("						AND h.ppf_id = e.ppf_id ");
			sb.append("						AND f.pft_id = e.pft_id ");
			sb.append("						AND f.ppfs_id=d.ppfs_id ");
			sb.append("						AND g.pnr_pax_id = h.pnr_pax_id ");
			sb.append("						AND h.ppf_id = d.ppf_id ");
			sb.append("						AND b.status_mod_date BETWEEN (TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append("						AND	    (TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append("						AND c.flight_id = i.flight_id ");
			sb.append(" 					AND UPPER(i.flight_number) = UPPER('" + strFlightNo + "') ");
			sb.append("  				GROUP BY g.pnr_pax_id, (g.first_name || ' ' || g.last_name), ");
			sb.append("                 		a.pnr, c.segment_code, a.booking_timestamp, c.est_time_departure_zulu, ");
			sb.append("                      i.flight_number, b.status, a.origin_agent_code     	) res ");
			sb.append("			ORDER BY PNR ");

		} else {

			sb.append("SELECT PAX_NAME, PNR, ");
			sb.append(
					"			NVL((SELECT SUM(DECODE(NOMINAL_CODE, 5,t.amount, 22,t.amount, 23,t.amount, 24,t.amount, 25,t.amount, 26,t.amount, "
							+ " 29,t.amount, 31,t.amount, 33,t.amount, 0)) ");
			sb.append("					FROM T_PAX_TRANSACTION t ");
			sb.append("					WHERE t.pnr_pax_id = res.pnr_pax_id), 0) REFUND, ");
			sb.append("							SEGMENT_CODE, FARE, TAX, SURCHARGE, (FARE + TAX + SURCHARGE) AS TOTAL_AMOUNT ");
			sb.append("		FROM ");
			sb.append("			(SELECT  g.pnr_pax_id, (g.first_name || ' ' || g.last_name) AS PAX_NAME, ");
			sb.append(" 				a.pnr AS PNR, c.segment_code AS SEGMENT_CODE,");
			sb.append("					SUM(DECODE(e.charge_group_code, 'FAR', f.amount, 0)) AS FARE, ");
			sb.append(" 				SUM(DECODE(e.charge_group_code, 'TAX', f.amount, 0)) AS TAX, ");
			sb.append(" 				SUM(DECODE(e.charge_group_code, 'SUR', f.amount, 'INF', f.amount, 0)) AS SURCHARGE");
			sb.append(" 			FROM 	t_reservation a,");
			sb.append(" 					t_pnr_segment b, ");
			sb.append("						t_flight_segment c,  ");
			sb.append(" 					t_pnr_pax_fare_segment d,     ");
			sb.append("  					t_pnr_pax_ond_charges e,     ");
			sb.append(" 					t_pnr_pax_seg_charges f,        ");
			sb.append("  					t_pnr_passenger g,  ");
			sb.append("  					t_pnr_pax_fare h   ");
			sb.append("  			WHERE a.pnr = b.pnr ");
			sb.append("  				AND   a.pnr=g.pnr      ");
			sb.append("  				AND b.flt_seg_id = c.flt_seg_id  ");
			sb.append("  				AND a.origin_agent_code = '" + strAgent + "' ");
			sb.append("  				AND d.pnr_seg_id = b.pnr_seg_id  ");
			sb.append(" 				AND h.ppf_id = e.ppf_id       ");
			sb.append("  				AND f.pft_id = e.pft_id  ");
			sb.append("  				AND f.ppfs_id=d.ppfs_id  ");
			sb.append("					AND g.pnr_pax_id = h.pnr_pax_id  ");
			sb.append(" 				AND h.ppf_id = d.ppf_id");
			sb.append(" 				AND b.status_mod_date BETWEEN (TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append(" 				AND	(TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append(" 			GROUP BY a.pnr, g.pnr_pax_id,  c.segment_code,      ");
			sb.append("   					g.first_name || ' ' || g.last_name ) res  ");
			sb.append("  	ORDER BY PNR ");

		}

		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Taxes Summary Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getReservationBreakdownTaxesQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		StringBuilder sb = new StringBuilder();

		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)
				|| (singleDataValue.length() == 0)) {
			return null;
		}

		sb.append("SELECT   origin_agent_code, agent_name, SUM (amt) as AMOUNT, charge_code ");
		sb.append("	    FROM (SELECT y.origin_agent_code, y.agent_name, y.amount AS amt,");
		sb.append("             CASE");
		sb.append("                WHEN y.rpt_charge_group_id IS NULL ");
		sb.append("                   THEN y.charge_code ");
		sb.append("                ELSE (SELECT x.rpt_charge_group_code ");
		sb.append("                        FROM t_rpt_charge_group x ");
		sb.append("                       WHERE x.rpt_charge_group_id = y.rpt_charge_group_id) ");
		sb.append("             END AS charge_code ");
		sb.append("        FROM (SELECT   r.origin_agent_code, a.agent_name, cr.charge_code, ");
		sb.append("                       pft.amount AS amount, ch.rpt_charge_group_id ");
		sb.append("                  FROM t_agent a, ");
		sb.append("                       t_reservation r, ");
		sb.append("                       t_pnr_pax_fare ppf, ");
		sb.append("                       t_pnr_pax_ond_charges pft, ");
		sb.append("                       t_pnr_passenger pax, ");
		sb.append("                       t_charge_rate cr, ");
		sb.append("                       t_charge ch ");
		sb.append("                 WHERE pft.charge_group_code = 'TAX' ");
		sb.append("                   AND ch.charge_code = cr.charge_code AND ");
		sb.append(ReportUtils.getReplaceStringForIN("r.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append("                   AND r.booking_timestamp BETWEEN (TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom());
		sb.append(" 00:00:00', ");
		sb.append("                                            'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append("                              AND (TO_DATE ('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', ");
		sb.append("                                            'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append("                   AND a.agent_code = r.origin_agent_code ");
		sb.append("                   AND r.pnr = pax.pnr ");
		sb.append("                   AND pax.pnr_pax_id = ppf.pnr_pax_id ");
		sb.append("                   AND ppf.ppf_id = pft.ppf_id ");
		sb.append("                   AND pft.charge_rate_id = cr.charge_rate_id ");
		sb.append("              ORDER BY r.origin_agent_code) y) ");
		sb.append("GROUP BY origin_agent_code, agent_name, charge_code ");
		sb.append("ORDER BY origin_agent_code  ");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Outstanding Balance Summary Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getOutstandingBalanceSummaryQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgencyTypes());
		StringBuilder sb = new StringBuilder();
		if ((reportsSearchCriteria.getDateRangeFrom() == null) && (reportsSearchCriteria.getDateRangeTo() == null)
				&& (singleDataValue.length() == 0)) {
			return null;
		}
		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REPORT_OPTION_DEBIT_ONLY)) {
			sb.append(" select agent_code,agent_name,  amt debit,");
			sb.append("0 credit, ");
			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)<31),0) inv_31, ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)between 31 and 59),0) inv_60,    ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE) between 60 and 89),0) inv_90,       ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)>180),0)inv_abv180 ");

			sb.append("from ");
			sb.append("( ");
			sb.append("SELECT a.agent_code,a.agent_name,sum(invoice_amount-settled_amount)amt ");
			sb.append("FROM t_invoice i,t_agent a ");
			sb.append("where   a.agent_code=i.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");
			sb.append("group by  a.agent_code,a.agent_name ");
			sb.append(")a WHERE AMT>0 ");

		}
		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REPORT_OPTION_CREDIT_ONLY)) {
			sb.append(" select agent_code,agent_name,0 debit, ");
			sb.append("amt credit, ");
			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)<31),0) inv_31,");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)between 31 and 59),0) inv_60, ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append(" from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE) between 60 and 89),0) inv_90, ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)>180),0) inv_abv180 ");

			sb.append("from ");
			sb.append("( ");
			sb.append("SELECT a.agent_code,a.agent_name,sum(invoice_amount-settled_amount)amt ");
			sb.append("FROM t_invoice i,t_agent a ");
			sb.append("where   a.agent_code=i.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");
			sb.append("group by  a.agent_code,a.agent_name ");
			sb.append(")a  WHERE AMT<0 ");
		}
		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT)) {
			sb.append(" select agent_code,agent_name,CASE  WHEN amt>0 THEN amt ELSE 0 END debit, ");
			sb.append("CASE  WHEN amt<0 THEN amt ELSE 0 END credit, ");
			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)<31),0) inv_31, ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)between 31 and 59),0) inv_60, ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE) between 60 and 89),0) inv_90, ");

			sb.append("nvl((select sum(invoice_amount-settled_amount) ");
			sb.append("from t_invoice ");
			sb.append("where  agent_code=a.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(
					reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and  ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to))and ");
			sb.append("trunc(sysdate)-trunc(INVOICE_DATE)>180),0) inv_abv180 ");

			sb.append("from ");
			sb.append("( ");
			sb.append("SELECT a.agent_code,a.agent_name,sum(invoice_amount-settled_amount)amt ");
			sb.append("FROM t_invoice i,t_agent a ");
			sb.append("where   a.agent_code=i.agent_code and ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");
			sb.append("group by  a.agent_code,a.agent_name  ");
			sb.append(")a ");
		}
		return sb.toString();
	}

	public String getOutstandingBalanceDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		String strAgent = reportsSearchCriteria.getAgentCode();
		if ((reportsSearchCriteria.getDateRangeFrom() == null) && (reportsSearchCriteria.getDateRangeTo() == null)
				&& (strAgent == null)) {
			return null;
		}
		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REPORT_OPTION_DEBIT_ONLY)) {
			sb.append("SELECT invoice_number, invoice_date, billing_period, invoice_amount ");
			sb.append("FROM 	t_invoice ");
			sb.append("WHERE agent_code = '" + strAgent + "' ");
			sb.append("AND (to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");

		}
		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REPORT_OPTION_CREDIT_ONLY)) {
			sb.append("SELECT invoice_number, invoice_date, billing_period, ");
			sb.append("      	settled_amount ");
			sb.append("FROM 	t_invoice ");
			sb.append("WHERE agent_code = '" + strAgent + "' ");
			sb.append("AND (to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");

		}
		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT)) {
			sb.append("SELECT invoice_number, invoice_date, billing_period, invoice_amount, ");
			sb.append("      	settled_amount ");
			sb.append("FROM 	t_invoice ");
			sb.append("WHERE agent_code = '" + strAgent + "' ");
			sb.append("AND (to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");

		}
		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Taxes Summary Report.
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getAgentProductivityQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)
				|| (singleDataValue.length() == 0)) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strMonth = "01" + strToDate.substring(2, strToDate.length());
		String strYear = "01-Jan" + strToDate.substring(6, strToDate.length());

		sb.append(" SELECT	currentSale.AGENT_CODE, agent.AGENT_NAME, ");
		sb.append("			currentSale.CURRENT_SALE, ");
		sb.append("			NVL(ytdSaleForMonth.YTD_SALE_FOR_MONTH, 0) AS YTD_SALE_FOR_MONTH,");
		sb.append(" 		NVL(ytdSale.YTD_SALE, 0) AS YTD_SALE");
		sb.append("		FROM ( ");
		sb.append("			SELECT	agent.agent_code AS AGENT_CODE, ");
		sb.append("					SUM(vPaxTxn.amount) * -1 ");
		sb.append("					AS CURRENT_SALE ");
		sb.append("			FROM	V_PNR_PAX_TRANSACTION vPaxTxn, ");
		sb.append("					T_AGENT agent ");
		sb.append("			WHERE	vPaxTxn.nominal_code in (19) ");
		sb.append("			and		vPaxTxn.tnx_date BETWEEN (TO_DATE('");
		sb.append(strFromdate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append(" 				AND	(TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append("			AND		vPaxTxn.agent_code = agent.agent_code   AND         ");
		sb.append(ReportUtils.getReplaceStringForIN("vPaxTxn.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append("			GROUP BY	agent.agent_code ");
		sb.append("			) currentSale ");
		sb.append("			LEFT OUTER JOIN ");
		sb.append("			( ");
		sb.append("			SELECT	vPaxTxn.agent_code AS AGENT_CODE, ");
		sb.append("					SUM(vPaxTxn.amount) * -1 ");
		sb.append("					AS YTD_SALE_FOR_MONTH ");
		sb.append("			FROM	V_PNR_PAX_TRANSACTION vPaxTxn ");
		sb.append("			WHERE	vPaxTxn.nominal_code in (19) ");
		sb.append("			AND		vPaxTxn.tnx_date BETWEEN  (TO_DATE('");
		sb.append(strMonth + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append(" 				AND	(TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))    AND     ");
		sb.append(ReportUtils.getReplaceStringForIN("vPaxTxn.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append("			GROUP BY	vPaxTxn.agent_code ");
		sb.append("			) ytdSaleForMonth ");
		sb.append("				ON currentSale.agent_code = ytdSaleForMonth.agent_code ");
		sb.append("			LEFT OUTER JOIN ");
		sb.append("			( ");
		sb.append("			SELECT	vPaxTxn.agent_code AS AGENT_CODE, ");
		sb.append("					SUM(vPaxTxn.amount)* -1 AS YTD_SALE ");
		sb.append("			FROM	V_PNR_PAX_TRANSACTION vPaxTxn ");
		sb.append("			WHERE	vPaxTxn.nominal_code in (19) ");
		sb.append("			AND		vPaxTxn.tnx_date BETWEEN (TO_DATE('");
		sb.append(strYear + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append(" 				AND	(TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))   AND    ");
		sb.append(ReportUtils.getReplaceStringForIN("vPaxTxn.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append("			GROUP BY	vPaxTxn.agent_code ");
		sb.append("			) ytdSale ");
		sb.append("			ON currentSale.agent_code = ytdSale.agent_code, T_AGENT agent ");
		sb.append("			WHERE currentSale.AGENT_CODE = agent.agent_code ");

		return sb.toString();
	}

	public String getPaxContactDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String flightFromDate = reportsSearchCriteria.getDepartureDateRangeFrom();
		String flightToDate = reportsSearchCriteria.getDepartureDateRangeTo();
		String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String strFlightNo = reportsSearchCriteria.getFlightNumber();
		String strPrintDetailsOf = reportsSearchCriteria.getPrintDetailsOf();

		sb.append("SELECT * FROM ( ");

		sb.append(" SELECT DISTINCT R.PNR,");
		sb.append("       INITCAP(P.FIRST_NAME)||' '||INITCAP(P.LAST_NAME)NAME,");
		sb.append("       FS.SEGMENT_CODE,");
		sb.append("       INITCAP(RC.C_FIRST_NAME)||' '||INITCAP(RC.C_LAST_NAME)C_NAME,");
		sb.append("       NVL(RC.C_STREET_ADDRESS_1,'--')C_STREET_ADDRESS_1,");
		sb.append("       NVL(RC.C_STREET_ADDRESS_2,'--')C_STREET_ADDRESS_2,");
		sb.append("       NVL(RC.C_CITY,'--')C_CITY,");
		sb.append("       NVL(RC.C_STATE,'--')C_STATE,");
		sb.append("       C.COUNTRY_NAME,");
		sb.append("       NVL(RC.C_ZIP_CODE,'--')C_ZIP_CODE,");
		sb.append("       NVL(RC.C_MOBILE_NO,'--')C_MOBILE_NO,");
		sb.append("       NVL(RC.C_PHONE_NO,'--')C_PHONE_NO,");
		sb.append("       NVL(RC.C_FAX,'--')C_FAX,");
		sb.append("       NVL(RC.C_EMAIL,'--')C_EMAIL,");
		sb.append("       INITCAP(RC.E_FIRST_NAME)||' '||INITCAP(RC.E_LAST_NAME)E_NAME,");
		sb.append("       NVL(RC.E_PHONE_NO,'--')E_PHONE_NO,");
		sb.append("       NVL(RC.E_EMAIL,'--')E_EMAIL,");
		sb.append("       NVL(ADINFO.PASSPORT_NUMBER,'')PASSPORT_NUMBER,");
		sb.append("       ADINFO.PASSPORT_EXPIRY PASSPORT_EXPIRY,");
		sb.append(
				"       (SELECT CTRY.COUNTRY_NAME FROM T_COUNTRY CTRY WHERE CTRY.COUNTRY_CODE =  ADINFO.PASSPORT_ISSUED_CNTRY) ISSUED_COUNTRY,");
		sb.append(
				"       (SELECT NTNL.DESCRIPTION FROM T_NATIONALITY NTNL WHERE NTNL.NATIONALITY_CODE = P.NATIONALITY_CODE) NATIONALITY,");
		sb.append("       P.DATE_OF_BIRTH DOB,");
		sb.append("       P.TITLE TITLE");
		sb.append(" FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT S, T_RESERVATION R, ");
		sb.append(" T_RESERVATION_CONTACT RC, T_PNR_PASSENGER P, T_COUNTRY C, T_PNR_PAX_ADDITIONAL_INFO ADINFO ");
		sb.append(" WHERE R.PNR = P.PNR");
		sb.append("       AND R.PNR = S.PNR AND R.PNR=RC.PNR");
		sb.append("       AND S.FLT_SEG_ID = FS.FLT_SEG_ID");
		sb.append("       AND FS.FLIGHT_ID = F.FLIGHT_ID");
		sb.append("       AND RC.C_COUNTRY_CODE = C.COUNTRY_CODE");
		sb.append("       AND P.PNR_PAX_ID = ADINFO.PNR_PAX_ID");

		// JIRA : AARESAA-3016 fixed by Lalanthi - Date : 29/09/2009
		if (isNotEmptyOrNull(strFromdate) && isNotEmptyOrNull(strToDate)) {
			sb.append("       AND R.booking_timestamp BETWEEN TO_DATE('" + strFromdate
					+ "', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strToDate + "', 'DD-MON-YYYY HH24:MI:SS') ");
		}
		if (isNotEmptyOrNull(flightFromDate) && isNotEmptyOrNull(flightToDate)) {
			sb.append("       AND FS.EST_TIME_DEPARTURE_ZULU BETWEEN TO_DATE('" + flightFromDate
					+ "', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + flightToDate + "', 'DD-MON-YYYY HH24:MI:SS') ");
		}
		if (strPrintDetailsOf.equals("REGISTERED")) {
			sb.append("       AND RC.CUSTOMER_ID IS NOT NULL ");
		}
		if (!"".equals(strFlightNo)) {
			sb.append("       AND F.FLIGHT_NUMBER = '" + strFlightNo + "' ");
		}
		if (!"''".equals(strAgents)) {
			sb.append("       AND  ");
			sb.append(ReportUtils.getReplaceStringForIN("R.OWNER_AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "  ");
		}

		sb.append(" UNION ");

		sb.append(" SELECT DISTINCT R.PNR,");
		sb.append("       INITCAP(P.FIRST_NAME)||' '||INITCAP(P.LAST_NAME)NAME,");
		sb.append("       FS.SEGMENT_CODE,");
		sb.append("       INITCAP(RC.C_FIRST_NAME)||' '||INITCAP(RC.C_LAST_NAME)C_NAME,");
		sb.append("       NVL(RC.C_STREET_ADDRESS_1,'--')C_STREET_ADDRESS_1,");
		sb.append("       NVL(RC.C_STREET_ADDRESS_2,'--')C_STREET_ADDRESS_2,");
		sb.append("       NVL(RC.C_CITY,'--')C_CITY,");
		sb.append("       NVL(RC.C_STATE,'--')C_STATE,");
		sb.append("       C.COUNTRY_NAME,");
		sb.append("       NVL(RC.C_ZIP_CODE,'--')C_ZIP_CODE,");
		sb.append("       NVL(RC.C_MOBILE_NO,'--')C_MOBILE_NO,");
		sb.append("       NVL(RC.C_PHONE_NO,'--')C_PHONE_NO,");
		sb.append("       NVL(RC.C_FAX,'--')C_FAX,");
		sb.append("       NVL(RC.C_EMAIL,'--')C_EMAIL,");
		sb.append("       INITCAP(RC.E_FIRST_NAME)||' '||INITCAP(RC.E_LAST_NAME)E_NAME,");
		sb.append("       NVL(RC.E_PHONE_NO,'--')E_PHONE_NO,");
		sb.append("       NVL(RC.E_EMAIL,'--')E_EMAIL,");
		sb.append("       NVL(ADINFO.PASSPORT_NUMBER,'')PASSPORT_NUMBER,");
		sb.append("       ADINFO.PASSPORT_EXPIRY PASSPORT_EXPIRY,");
		sb.append(
				"       (SELECT CTRY.COUNTRY_NAME FROM T_COUNTRY CTRY WHERE CTRY.COUNTRY_CODE =  ADINFO.PASSPORT_ISSUED_CNTRY) ISSUED_COUNTRY,");
		sb.append(
				"       (SELECT NTNL.DESCRIPTION FROM T_NATIONALITY NTNL WHERE NTNL.NATIONALITY_CODE = P.NATIONALITY_CODE) NATIONALITY,");
		sb.append("       P.DATE_OF_BIRTH DOB,");
		sb.append("       P.TITLE TITLE");
		sb.append(" FROM  T_EXT_FLIGHT_SEGMENT FS, T_EXT_PNR_SEGMENT S, T_RESERVATION R, ");
		sb.append(" T_RESERVATION_CONTACT RC, T_PNR_PASSENGER P, T_COUNTRY C, T_PNR_PAX_ADDITIONAL_INFO ADINFO ");
		sb.append(" WHERE R.PNR = P.PNR");
		sb.append("       AND R.PNR = S.PNR AND R.PNR=RC.PNR");
		sb.append("       AND S.EXT_FLT_SEG_ID = FS.EXT_FLT_SEG_ID ");
		sb.append("       AND RC.C_COUNTRY_CODE = C.COUNTRY_CODE");
		sb.append("       AND P.PNR_PAX_ID = ADINFO.PNR_PAX_ID");

		// JIRA : AARESAA-3016 fixed by Lalanthi - Date : 29/09/2009
		if (isNotEmptyOrNull(strFromdate) && isNotEmptyOrNull(strToDate)) {
			sb.append("       AND R.booking_timestamp BETWEEN TO_DATE('" + strFromdate
					+ "', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strToDate + "', 'DD-MON-YYYY HH24:MI:SS') ");
		}
		if (isNotEmptyOrNull(flightFromDate) && isNotEmptyOrNull(flightToDate)) {
			sb.append("       AND FS.EST_TIME_DEPARTURE_ZULU BETWEEN TO_DATE('" + flightFromDate
					+ "', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + flightToDate + "', 'DD-MON-YYYY HH24:MI:SS') ");
		}
		if (strPrintDetailsOf.equals("REGISTERED")) {
			sb.append("       AND RC.CUSTOMER_ID IS NOT NULL ");
		}
		if (!"".equals(strFlightNo)) {
			sb.append("       AND FS.FLIGHT_NUMBER = '" + strFlightNo + "' ");
		}
		if (!"''".equals(strAgents)) {
			sb.append("       AND  ");
			sb.append(ReportUtils.getReplaceStringForIN("R.OWNER_AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "  ");
		}

		sb.append(" ) ORDER BY PNR,SEGMENT_CODE,NAME ASC ");

		return sb.toString();
	}

	public String getIBEOnholdPassengersQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String fromdate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();

		String depFromdate = reportsSearchCriteria.getDepartureDateRangeFrom();
		String depToDate = reportsSearchCriteria.getDepartureDateRangeTo();

		String bookFromDate = reportsSearchCriteria.getDateRange2From();
		String bookToDate = reportsSearchCriteria.getDateRange2To();

		String stateTransition = reportsSearchCriteria.getStatus();

		String flightNumber = reportsSearchCriteria.getFlightNumber();

		boolean isInLocalTime = reportsSearchCriteria.isShowInLocalTime();

		String stateTransUser = null;
		String schedulerUserId = AppSysParamsUtil.getDefaultAirlineIdentifierCode() + User.SCHEDULER_USER_ID;

		StringBuilder sb = new StringBuilder();
		sb.append("select * from (");
		sb.append("(select unique ");
		sb.append("To_char (pax.start_timestamp, 'DD/mm/YY') ");
		sb.append("        AS reservation_date,");
		sb.append("To_char (pax.release_timestamp,'DD/mm/YY HH24:mi:ss') ");
		sb.append("        AS release_date,");

		if (!isInLocalTime) {
			sb.append("To_char (flgseg.est_time_departure_zulu,'DD/mm/YY HH24:mi:ss') ");
			sb.append("        AS flight_date,");
		} else {
			sb.append("To_char (flgseg.est_time_departure_local,'DD/mm/YY HH24:mi:ss') ");
			sb.append("        AS flight_date,");
		}

		sb.append("flg.flight_number ");
		sb.append("        AS flight_number,");
		sb.append("res.pnr AS pnr,");
		sb.append("SUM (DECODE (pax.pax_type_code, 'AD', 1, 0)) OVER (partition by pnrseg.pnr_seg_id) ||  '\\' ");
		sb.append("          || SUM (DECODE (pax.pax_type_code, 'CH', 1, 0)) OVER (partition by pnrseg.pnr_seg_id) || '\\'");
		sb.append("          || SUM (DECODE (pax.pax_type_code, 'IN', 1, 0))  OVER (partition by pnrseg.pnr_seg_id) ");
		sb.append("        AS no_of_pax_onhold,");
		sb.append("rcon.c_first_name || ' ' || rcon.c_last_name ");
		sb.append("        AS contact_name,");
		sb.append("flgseg.segment_code ");
		sb.append("        AS sector,");
		sb.append("DECODE (rcon.c_phone_no, NULL, '-', rcon.c_phone_no) ");
		sb.append("        AS res_tel,");
		sb.append("DECODE (rcon.c_mobile_no, NULL, '-', rcon.c_mobile_no) ");
		sb.append("        AS mobile,");
		sb.append("pnrseg.status ");
		sb.append("        AS seg_status,");
		sb.append("To_char (pnrseg.status_mod_date, 'DD/mm/YY HH24:mi') ");
		sb.append("        AS mx_time,");
		sb.append("To_char (pnrseg.status_mod_date, 'DD/mm/YY HH24:mi') ");
		sb.append("        AS mod_date");

		sb.append(" from ");

		sb.append("t_reservation res, t_pnr_passenger pax, t_pnr_segment pnrseg, t_flight_segment flgseg,");
		sb.append("t_flight flg, t_reservation_contact rcon, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs ");

		sb.append(" WHERE ");
		sb.append(" res.owner_channel_code=4 and ");
		sb.append(" res.pnr = rcon.pnr and");
		sb.append(" res.pnr= pax.pnr AND ");
		sb.append(" res.pnr= pnrseg.pnr AND");
		sb.append(" pnrseg.flt_seg_id = flgseg.flt_seg_id AND");
		sb.append(" flg.flight_id = flgseg.flight_id AND");
		sb.append(" ppf.pnr_pax_id = pax.pnr_pax_id AND");
		sb.append(" ppfs.ppf_id = ppf.ppf_id AND ");
		sb.append(" pnrseg.pnr_seg_id = ppfs.pnr_seg_id ");
		sb.append("     AND    pax.release_timestamp BETWEEN TO_DATE('" + fromdate + "', 'DD-MON-YYYY HH24:MI:SS') AND	TO_DATE('"
				+ toDate + "', 'DD-MON-YYYY HH24:MI:SS') ");

		if (isNotEmptyOrNull(bookFromDate) && isNotEmptyOrNull(bookToDate)) {
			sb.append(" AND res.booking_timestamp BETWEEN TO_DATE('" + bookFromDate
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + bookToDate
					+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(depFromdate) && isNotEmptyOrNull(depToDate)) {
			sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + depFromdate
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND	TO_DATE('" + depToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");
		}

		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" AND flg.flight_number='" + flightNumber + "' ");
		}
		sb.append(" AND ");
		sb.append("(");
		// passenger is an adult and is not open return
		sb.append(" (pax.pax_type_code IN ( 'AD','CH' ) AND");
		sb.append("  ppfs.booking_code IN ( SELECT b.booking_code FROM t_booking_class b WHERE bc_type NOT IN ( 'OPENRT' )) ) ");
		sb.append("           	  OR           ");
		// passenger is an infant and its adult is not open return
		sb.append("	 (pax.pax_type_code = 'IN' AND ");
		sb.append("   pax.adult_id IN (SELECT pf.pnr_pax_id FROM t_pnr_pax_fare_segment ps,t_pnr_pax_fare pf WHERE");
		sb.append("                      ps.ppf_id = pf.ppf_id AND ");
		sb.append("                      pf.pnr_pax_id =pax.adult_id AND ");
		sb.append("                      ps.pnr_seg_id =ppfs.pnr_seg_id AND ");
		sb.append(
				"                      ps.booking_code IN (SELECT b.booking_code FROM t_booking_class b WHERE bc_type NOT IN ( 'OPENRT' )))");
		sb.append(" )");
		sb.append(")");

		if (isNotEmptyOrNull(stateTransition)) {
			if (stateTransition.equals("CNX_SCHD")) {
				// onhold to canceled by scheduler
				sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
				stateTransUser = " AND pnrseg.status_mod_user_id='" + schedulerUserId + "'";
			} else if (stateTransition.equals("CNX_USER")) {
				// onhold to canceled by user
				sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
				stateTransUser = " AND pnrseg.status_mod_user_id <> '" + schedulerUserId + "'";
			} else if (stateTransition.equals("CNF")) {
				// onhold to confirm
				sb.append(" AND res.status = 'CNF'  AND pnrseg.status='CNF' ");
			} else if (stateTransition.equals("OHD")) {
				// onhold reservation
				sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
			} else {
				// onhold all
				log.info("All Onhold reservations PnrSegment");
			}
		} else {
			// onhold all
			sb.append("");

		}
		if (isNotEmptyOrNull(stateTransUser)) {
			sb.append(stateTransUser);
		}

		sb.append(")");
		sb.append(" UNION ");
		sb.append("(select unique ");
		sb.append("To_char (pax.start_timestamp, 'DD/mm/YY') ");
		sb.append("        AS reservation_date,");
		sb.append("To_char (pax.release_timestamp,'DD/mm/YY HH24:mi:ss') ");
		sb.append("        AS release_date,");

		/*
		 * if (isInZuluTime) { sb.append("To_char (flgseg.est_time_departure_zulu,'DD/mm/YY HH24:mi:ss') ");
		 * sb.append("        AS flight_date,"); } else {
		 * sb.append("To_char (flgseg.est_time_departure_local,'DD/mm/YY HH24:mi:ss') ");
		 * sb.append("        AS flight_date,"); }
		 */
		sb.append("To_char (flgseg.est_time_departure_local,'DD/mm/YY HH24:mi:ss') ");
		sb.append("        AS flight_date,");
		sb.append("flgseg.flight_number");
		sb.append("        AS flight_number,");
		sb.append("res.pnr AS pnr,");
		sb.append("SUM (DECODE (pax.pax_type_code, 'AD', 1, 0)) OVER (partition by extpnrseg.ext_pnr_seg_id) ||  '\\' ");
		sb.append(
				"          || SUM (DECODE (pax.pax_type_code, 'CH', 1, 0)) OVER (partition by extpnrseg.ext_pnr_seg_id) || '\\'");
		sb.append("          || SUM (DECODE (pax.pax_type_code, 'IN', 1, 0))  OVER (partition by extpnrseg.ext_pnr_seg_id) ");
		sb.append("        AS no_of_pax_onhold,");
		sb.append("rcon.c_first_name || ' ' || rcon.c_last_name ");
		sb.append("        AS contact_name,");
		sb.append("flgseg.segment_code ");
		sb.append("        AS sector,");
		sb.append("DECODE (rcon.c_phone_no, NULL, '-', rcon.c_phone_no) ");
		sb.append("        AS res_tel,");
		sb.append("DECODE (rcon.c_mobile_no, NULL, '-', rcon.c_mobile_no) ");
		sb.append("        AS mobile,");
		sb.append("extpnrseg.status ");
		sb.append("        AS seg_status,");
		sb.append("To_char (extpnrseg.status_mod_date, 'DD/mm/YY HH24:mi') ");
		sb.append("        AS mx_time,");
		sb.append("To_char (extpnrseg.status_mod_date, 'DD/mm/YY HH24:mi') ");
		sb.append("        AS mod_date");

		sb.append(" from ");

		sb.append(
				"t_reservation res, t_pnr_passenger pax, t_ext_pnr_segment extpnrseg, t_ext_flight_segment flgseg,t_pnr_segment pnrseg,");
		sb.append("t_reservation_contact rcon ");

		sb.append(" WHERE ");
		sb.append(" res.owner_channel_code=4 AND");
		sb.append(" res.pnr = rcon.pnr and");
		sb.append(" res.pnr= pax.pnr AND ");
		sb.append(" res.pnr= extpnrseg.pnr AND");
		sb.append(" res.pnr = pnrseg.pnr AND");

		sb.append(" extpnrseg.ext_flt_seg_id = flgseg.ext_flt_seg_id");
		sb.append("     AND    pax.release_timestamp BETWEEN TO_DATE('" + fromdate + "', 'DD-MON-YYYY HH24:MI:SS') AND	TO_DATE('"
				+ toDate + "', 'DD-MON-YYYY HH24:MI:SS') ");

		if (isNotEmptyOrNull(bookFromDate) && isNotEmptyOrNull(bookToDate)) {
			sb.append(" AND res.booking_timestamp BETWEEN TO_DATE('" + bookFromDate
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + bookToDate
					+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(depFromdate) && isNotEmptyOrNull(depToDate)) {
			sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + depFromdate
					+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND	TO_DATE('" + depToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");
		}

		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" AND flgseg.flight_number='" + flightNumber + "' ");
		}
		if (isNotEmptyOrNull(stateTransition)) {
			if (stateTransition.equals("CNX_SCHD")) {
				// onhold to canceled by scheduler
				sb.append(" AND res.status = 'CNX'  AND extpnrseg.status='CNX' ");
				stateTransUser = " AND pnrseg.status_mod_user_id='" + schedulerUserId + "'";
			} else if (stateTransition.equals("CNX_USER")) {
				// onhold to canceled by user
				sb.append(" AND res.status = 'CNX'  AND extpnrseg.status='CNX' ");
				stateTransUser = " AND pnrseg.status_mod_user_id <> '" + schedulerUserId + "'";
			} else if (stateTransition.equals("CNF")) {
				// onhold to confirm
				sb.append(" AND res.status = 'CNF'  AND extpnrseg.status='CNF' ");
			} else if (stateTransition.equals("OHD")) {
				// onhold reservation
				sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND extpnrseg.status <> 'CNX' ");
			} else {
				// onhold all
				log.info("All Onhold reservations Ext Pnr Segments");
			}

		} else {
			// onhold all
			sb.append("");
		}
		if (isNotEmptyOrNull(stateTransUser)) {
			sb.append(stateTransUser);
		}

		sb.append("))");
		return sb.toString();
	}

	public String getOnholdPassengersQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String flightNumber = reportsSearchCriteria.getFlightNumber();

		String strDepFromdate = reportsSearchCriteria.getDepartureDateRangeFrom();
		String strDepToDate = reportsSearchCriteria.getDepartureDateRangeTo();
		String rptType = reportsSearchCriteria.getReportType();
		String strBookFromDate = reportsSearchCriteria.getDateRange2From();
		String strBookToDate = reportsSearchCriteria.getDateRange2To();

		String stateTransition = reportsSearchCriteria.getStatus();
		boolean isExtendOnHold = reportsSearchCriteria.isExtendOnhold();
		String schedulerUserId = User.SCHEDULER_USER_ID;

		if (AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers()) {
			schedulerUserId = AppSysParamsUtil.getDefaultAirlineIdentifierCode() + User.SCHEDULER_USER_ID;
		}
		String resStatus = "OHD";
		String stateTransUser = null;

		String templateCode = "NEWONH";

		if (isExtendOnHold) {
			templateCode = "EXTOHD";
		}

		if (rptType.equals("DETAIL")) {
			sb.append("SELECT * FROM (");
			sb.append("	SELECT DISTINCT reservation_date,release_date,flight_date,flight_number,");
			sb.append("	pnr,no_of_pax_onhold,contact_name,sector,res_tel,mobile,agent_name,");
			sb.append("	agent_tel,mod_user_id,seg_status,res_status,");
			sb.append(" TO_CHAR (mxtime, 'DD/mm/YY HH24:mi' ) AS mxtime,TO_CHAR (mod_date, 'DD/mm/YY HH24:mi' ) AS mod_date ");
			sb.append(" FROM (");
			sb.append("		SELECT restab.reservation_date,restab.release_date,restab.flight_date,");
			sb.append("		restab.flight_number,restab.pnr,restab.no_of_pax_onhold,restab.contact_name,");
			sb.append("		restab.sector,restab.res_tel,restab.mobile,restab.agent_name,restab.agent_tel,");
			sb.append("		pnseg.status_mod_user_id AS mod_user_id,pnseg.status_mod_date AS mod_date,");
			sb.append("		MAX (pnseg.status_mod_date) OVER (PARTITION BY restab.pnr) AS mxtime,seg_status,res_status FROM (");
			sb.append("		      SELECT *  FROM (");
			sb.append("		      SELECT *  FROM (");
			sb.append("		          SELECT TO_CHAR (pax.start_timestamp, 'DD/mm/YY' ) AS reservation_date,");
			sb.append("		          TO_CHAR (pax.release_timestamp, 'DD/mm/YY HH24:mi:ss' ) AS release_date,");
			sb.append("		          TO_CHAR (flgseg.est_time_departure_local, 'DD/mm/YY' )  AS flight_date,");
			sb.append("		          flg.flight_number AS flight_number,res.pnr AS pnr,");
			sb.append("		          SUM (DECODE (pax.pax_type_code, 'AD', 1, 0 ) ) || '\\'");
			sb.append("		          || SUM (DECODE (pax.pax_type_code, 'CH', 1, 0 ) ) || '\\'");
			sb.append("               || SUM (DECODE (pax.pax_type_code, 'IN', 1, 0 ) ) AS no_of_pax_onhold,");
			sb.append("            rcon.c_first_name|| ' '|| rcon.c_last_name AS contact_name,");
			sb.append(
					"flgseg.segment_code AS sector, pnrseg.pnr_seg_id AS pnr_seg_id, DECODE (rcon.c_phone_no, NULL, '-', rcon.c_phone_no ) AS res_tel,");
			sb.append("DECODE (rcon.c_mobile_no, NULL, '-', rcon.c_mobile_no ) AS mobile,AGENT.agent_name AS agent_name,");
			sb.append("AGENT.telephone AS agent_tel,pnrseg.status AS seg_status,res.status AS res_status ");
			sb.append("    FROM t_reservation res,t_reservation_contact rcon,t_pnr_passenger pax,");
			sb.append(
					"    t_pnr_segment pnrseg,t_flight_segment flgseg,t_flight flg,t_agent AGENT,t_pnr_pax_fare ppf,t_pnr_pax_fare_segment ppfs ");
			sb.append(" WHERE pax.release_timestamp BETWEEN TO_DATE('");
			sb.append(strFromdate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" 				AND	TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");

			if (isNotEmptyOrNull(strBookFromDate) && isNotEmptyOrNull(strBookToDate)) {
				sb.append(" AND res.booking_timestamp BETWEEN TO_DATE('" + strBookFromDate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookToDate
						+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
			}

			if (isNotEmptyOrNull(strDepFromdate) && isNotEmptyOrNull(strDepToDate)) {
				sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + strDepFromdate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND	TO_DATE('" + strDepToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");
			}

			sb.append(" AND res.pnr = pax.pnr AND res.pnr = rcon.pnr  AND res.pnr = pnrseg.pnr");
			sb.append(" AND pnrseg.flt_seg_id= flgseg.flt_seg_id AND flgseg.flight_id = flg.flight_id ");

			sb.append(" AND ppf.pnr_pax_id = pax.pnr_pax_id AND ppfs.ppf_id = ppf.ppf_id ");
			sb.append(" AND ppfs.pnr_seg_id    = pnrseg.pnr_seg_id ");

			sb.append(" AND ( ( pax.pax_type_code IN ('AD', 'CH') AND ppfs.booking_code IN ");
			sb.append(" (SELECT b.booking_code FROM t_booking_class b WHERE bc_type NOT IN ('OPENRT'))) ");
			sb.append(" OR ( pax.pax_type_code = 'IN' AND pax.adult_id IN (");
			sb.append(" (SELECT pf.pnr_pax_id FROM t_pnr_pax_fare_segment ps,t_pnr_pax_fare pf ");
			sb.append(" WHERE ps.ppf_id = pf.ppf_id AND pf.pnr_pax_id = pax.adult_id AND ps.pnr_seg_id = ppfs.pnr_seg_id ");
			sb.append(" AND ps.booking_code IN ");
			sb.append(" (SELECT b.booking_code FROM t_booking_class b WHERE bc_type NOT IN ('OPENRT'))");
			sb.append(" )) ) )");

			if (isNotEmptyOrNull(stateTransition)) {
				if (stateTransition.equals("CNX_SCHD")) {
					// onhold to canceled by scheduler
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
					stateTransUser = " AND mod_user_id='" + schedulerUserId + "'";
					resStatus = "CNX";
				} else if (stateTransition.equals("CNX_USER")) {
					// onhold to canceled by user
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
					stateTransUser = " AND mod_user_id <> '" + schedulerUserId + "'";
					resStatus = "CNX";
				} else if (stateTransition.equals("CNF")) {
					// onhold to confirm
					sb.append(" AND res.status = 'CNF'  AND pnrseg.status='CNF' ");
					resStatus = "CNF";
				} else {
					// onhold reservation
					sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
				}
			} else {
				sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
			}

			sb.append(" AND pnrseg.sub_status is null ");
			sb.append(" AND res.origin_agent_code = AGENT.agent_code AND ");

			if (null != reportsSearchCriteria.getUsers() && !reportsSearchCriteria.getUsers().isEmpty()) {
				sb.append(ReportUtils.getReplaceStringForIN("res.origin_user_id", reportsSearchCriteria.getUsers(), false)
						+ "  AND");
			}

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" flg.flight_number = UPPER('" + flightNumber + "') AND  ");
			}
			sb.append(" ( " + ReportUtils.getReplaceStringForIN("AGENT.agent_code", reportsSearchCriteria.getAgents(), false)
					+ " ) ");

			sb.append(" GROUP BY TO_CHAR (flgseg.est_time_departure_local, 'DD/mm/YY' ),");
			sb.append(" flg.flight_number,res.pnr,TO_CHAR (pax.start_timestamp, 'DD/mm/YY' ),");
			sb.append(
					"TO_CHAR (pax.release_timestamp, 'DD/mm/YY HH24:mi:ss' ),res.pnr,rcon.c_first_name||' '||rcon.c_last_name,");
			sb.append(
					"flgseg.segment_code, pnrseg.pnr_seg_id, rcon.c_phone_no,rcon.c_mobile_no,AGENT.agent_name,AGENT.telephone,pnrseg.status,res.status ");

			sb.append(") UNION (");

			sb.append("SELECT TO_CHAR (pax.start_timestamp, 'DD/mm/YY' )        AS reservation_date,");
			sb.append("TO_CHAR (pax.release_timestamp, 'DD/mm/YY HH24:mi:ss' ) AS release_date,");
			sb.append("TO_CHAR (flgseg.est_time_departure_local, 'DD/mm/YY' )  AS flight_date,");
			sb.append("flgseg.flight_number                                    AS flight_number,");
			sb.append("res.pnr                                                 AS pnr,");
			sb.append(
					"SUM (DECODE (pax.pax_type_code, 'AD', 1, 0 ) )|| '\\'|| SUM (DECODE (pax.pax_type_code, 'CH', 1, 0 ) )|| '\\'");
			sb.append("  || SUM (DECODE (pax.pax_type_code, 'IN', 1, 0 ) ) AS no_of_pax_onhold,");
			sb.append("rcon.c_first_name|| ' '|| rcon.c_last_name AS contact_name,");
			sb.append("flgseg.segment_code AS sector,");
			sb.append(" pnrseg.EXT_PNR_SEG_ID AS pnr_seg_id, ");
			sb.append(" DECODE (rcon.c_phone_no, NULL, '-', rcon.c_phone_no )   AS res_tel,");
			sb.append("DECODE (rcon.c_mobile_no, NULL, '-', rcon.c_mobile_no ) AS mobile,");
			sb.append("AGENT.agent_name                                        AS agent_name,");
			sb.append(
					"AGENT.telephone                                         AS agent_tel,pnrseg.status AS seg_status,res.status  AS res_status ");
			sb.append(" FROM t_reservation res,t_reservation_contact rcon,t_pnr_passenger pax,t_ext_pnr_segment pnrseg,");
			sb.append("t_ext_flight_segment flgseg,t_agent AGENT");
			sb.append(" WHERE pax.release_timestamp BETWEEN TO_DATE('");
			sb.append(strFromdate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" 				AND	TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");

			if (isNotEmptyOrNull(strBookFromDate) && isNotEmptyOrNull(strBookToDate)) {
				sb.append(" AND res.booking_timestamp BETWEEN TO_DATE('" + strBookFromDate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookToDate
						+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
			}

			if (isNotEmptyOrNull(strDepFromdate) && isNotEmptyOrNull(strDepToDate)) {
				sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + strDepFromdate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND	TO_DATE('" + strDepToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");
			}

			sb.append(" AND res.pnr = pax.pnr AND res.pnr = rcon.pnr ");
			sb.append(" AND res.pnr = pnrseg.pnr  AND pnrseg.ext_flt_seg_id = flgseg.ext_flt_seg_id");

			if (isNotEmptyOrNull(stateTransition)) {
				if (stateTransition.equals("CNX_SCHD")) {
					// onhold to canceled by scheduler
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
					stateTransUser = " AND mod_user_id='" + schedulerUserId + "'";
				} else if (stateTransition.equals("CNX_USER")) {
					// onhold to canceled by user
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
					stateTransUser = " AND mod_user_id <> '" + schedulerUserId + "'";
				} else if (stateTransition.equals("CNF")) {
					// onhold to confirm
					sb.append(" AND res.status = 'CNF'  AND pnrseg.status='CNF' ");
				} else {
					// onhold reservation
					sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
				}
			} else {
				sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
			}

			sb.append(" AND PNRSEG.SUB_STATUS is null ");
			sb.append(" AND res.origin_agent_code = AGENT.agent_code AND ");

			if (null != reportsSearchCriteria.getUsers() && !reportsSearchCriteria.getUsers().isEmpty()) {
				sb.append(ReportUtils.getReplaceStringForIN("res.origin_user_id", reportsSearchCriteria.getUsers(), false)
						+ "  AND");
			}

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" flgseg.flight_number = UPPER('" + flightNumber + "') AND  ");
			}

			sb.append(ReportUtils.getReplaceStringForIN("AGENT.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");

			sb.append(" GROUP BY flgseg.est_time_departure_local,flgseg.flight_number,res.pnr,pax.start_timestamp,");
			sb.append("pax.release_timestamp,res.pnr,");
			sb.append("rcon.c_first_name|| ' '|| rcon.c_last_name,flgseg.segment_code, pnrseg.EXT_PNR_SEG_ID, rcon.c_phone_no,");
			sb.append("rcon.c_mobile_no,AGENT.agent_name,AGENT.telephone,pnrseg.status,res.status ");
			sb.append("         )");
			sb.append("      )");
			sb.append(" WHERE pnr IN (SELECT x.pnr FROM t_reservation_audit x WHERE x.template_code = '" + templateCode + "') ");

			if (!"EXTOHD".equals(templateCode)) {
				sb.append(" OR (res_status='" + resStatus
						+ "'  AND pnr IN (SELECT x.pnr FROM t_reservation_audit x WHERE x.template_code = 'NEWSPT' OR x.template_code ='NEWRMP')) ");
			}

			sb.append(" ) restab,t_pnr_segment pnseg ");
			sb.append(" WHERE pnseg.pnr = restab.pnr ORDER BY pnseg.pnr,pnseg.status_mod_date");
			sb.append("	 )");
			sb.append("	)");
			sb.append(" WHERE mxtime = mod_date ");

			if (isNotEmptyOrNull(stateTransUser)) {
				sb.append(stateTransUser);
			}

			sb.append(" ORDER BY pnr");

		} else if (rptType.equals("SUMMARY")) {

			sb.append("SELECT   flight_number, est_time_departure_zulu, segment_code, agent_code,");
			sb.append("agent_name, telephone, ohd_pnr, ohd_pax, current_status ");
			sb.append(" FROM (SELECT   flight_number,");
			sb.append("TO_CHAR(flgseg.est_time_departure_zulu,'DD/mm/YY HH24:mi:ss') est_time_departure_zulu,");
			sb.append(
					"flgseg.segment_code, AGENT.agent_code, AGENT.agent_name, AGENT.telephone, COUNT (DISTINCT res.pnr) ohd_pnr,");
			sb.append("COUNT (DISTINCT pax.pnr_pax_id) ohd_pax,pnrseg.status AS current_status ");
			sb.append(" FROM t_reservation res,t_pnr_passenger pax,t_pnr_segment pnrseg,");
			sb.append("t_flight_segment flgseg,t_flight flg,t_agent AGENT,t_pnr_pax_fare ppf,t_pnr_pax_fare_segment ppfs ");

			sb.append(" WHERE pax.release_timestamp BETWEEN TO_DATE('" + strFromdate + "  00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND	TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");

			if (isNotEmptyOrNull(strBookFromDate) && isNotEmptyOrNull(strBookToDate)) {
				sb.append(" AND res.booking_timestamp BETWEEN TO_DATE('" + strBookFromDate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookToDate
						+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
			}

			if (isNotEmptyOrNull(strDepFromdate) && isNotEmptyOrNull(strDepToDate)) {
				sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + strDepFromdate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND	TO_DATE('" + strDepToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");
			}

			sb.append(" AND res.pnr = pax.pnr AND res.pnr = pnrseg.pnr");
			sb.append(" AND pnrseg.flt_seg_id = flgseg.flt_seg_id AND flgseg.flight_id = flg.flight_id");

			sb.append(" AND ppf.pnr_pax_id     = pax.pnr_pax_id AND ppfs.ppf_id        = ppf.ppf_id ");
			sb.append(" AND ppfs.pnr_seg_id    = pnrseg.pnr_seg_id ");
			sb.append(" AND ppfs.booking_code IN (SELECT B.BOOKING_CODE FROM T_BOOKING_CLASS B WHERE BC_TYPE NOT IN('OPENRT'))");

			sb.append(" AND pax.pax_type_code <> 'IN'  ");

			if (isNotEmptyOrNull(stateTransition)) {
				if (stateTransition.equals("CNX_SCHD")) {
					// onhold to canceled by scheduler
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
					stateTransUser = " AND pnrseg.status_mod_user_id='" + schedulerUserId + "'";
				} else if (stateTransition.equals("CNX_USER")) {
					// onhold to canceled by user
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
					stateTransUser = " AND pnrseg.status_mod_user_id <> '" + schedulerUserId + "'";
				} else if (stateTransition.equals("CNF")) {
					// onhold to confirm
					sb.append(" AND res.status = 'CNF'  AND pnrseg.status='CNF' ");
				} else {
					// onhold reservation
					sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
				}
			} else {
				sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
			}

			if (isNotEmptyOrNull(stateTransUser)) {
				sb.append(stateTransUser);
			}

			sb.append(" AND res.origin_agent_code = AGENT.agent_code AND ");

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" flg.flight_number = UPPER('" + flightNumber + "') AND  ");
			}

			if (null != reportsSearchCriteria.getUsers() && !reportsSearchCriteria.getUsers().isEmpty()) {
				sb.append(ReportUtils.getReplaceStringForIN("res.origin_user_id", reportsSearchCriteria.getUsers(), false)
						+ "  AND");
			}

			sb.append(ReportUtils.getReplaceStringForIN("AGENT.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");

			sb.append(" AND res.pnr IN (SELECT c.pnr FROM t_reservation_audit c WHERE c.template_code = '" + templateCode + "' ");
			if (!"EXTOHD".equals(templateCode)) {
				sb.append(" OR c.template_code    = 'NEWSPT' OR c.template_code    ='NEWRMP' ");
			}
			sb.append(") ");

			sb.append(" GROUP BY flg.flight_number,flgseg.est_time_departure_zulu,flgseg.segment_code,AGENT.agent_code,");
			sb.append("AGENT.agent_name,AGENT.telephone,pnrseg.status ");
			sb.append(" UNION ");
			sb.append(
					" SELECT flg.flight_number,TO_CHAR(flgseg.est_time_departure_zulu,'DD/mm/YY HH24:mi:ss') est_time_departure_zulu,");
			sb.append(
					"flgseg.segment_code, AGENT.agent_code, AGENT.agent_name,AGENT.telephone, COUNT (DISTINCT res.pnr) ohd_pnr, ");
			sb.append("COUNT (DISTINCT pax.pnr_pax_id) ohd_pax,pnrseg.status AS current_status");
			sb.append(" FROM t_reservation res,t_pnr_passenger pax,t_ext_pnr_segment pnrseg,");
			sb.append(" t_ext_flight_segment flgseg,t_agent AGENT, t_flight flg ");
			sb.append(" WHERE pax.release_timestamp BETWEEN TO_DATE('" + strFromdate + "  00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND	TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");

			if (isNotEmptyOrNull(strBookFromDate) && isNotEmptyOrNull(strBookToDate)) {
				sb.append(" AND res.booking_timestamp BETWEEN TO_DATE('" + strBookFromDate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookToDate
						+ " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
			}

			if (isNotEmptyOrNull(strDepFromdate) && isNotEmptyOrNull(strDepToDate)) {
				sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + strDepFromdate
						+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND	TO_DATE('" + strDepToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')   ");
			}

			sb.append(
					" AND res.pnr = pax.pnr AND res.pnr = pnrseg.pnr AND pnrseg.ext_flt_seg_id = flgseg.ext_flt_seg_id AND flgseg.flight_number  = flg.flight_number ");

			sb.append(" AND pax.pax_type_code <> 'IN'  ");

			if (isNotEmptyOrNull(stateTransition)) {
				if (stateTransition.equals("CNX_SCHD")) {
					// onhold to canceled by scheduler
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
				} else if (stateTransition.equals("CNX_USER")) {
					// onhold to canceled by user
					sb.append(" AND res.status = 'CNX'  AND pnrseg.status='CNX' ");
				} else if (stateTransition.equals("CNF")) {
					// onhold to confirm
					sb.append(" AND res.status = 'CNF'  AND pnrseg.status='CNF' ");
				} else {
					// onhold reservation
					sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
				}
			} else {
				sb.append(" AND pax.status = 'OHD' AND res.status = 'OHD'  AND pnrseg.status <> 'CNX' ");
			}

			sb.append(" AND res.origin_agent_code = AGENT.agent_code AND ");

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" flg.flight_number = UPPER('" + flightNumber + "') AND  ");
			}

			if (null != reportsSearchCriteria.getUsers() && !reportsSearchCriteria.getUsers().isEmpty()) {
				sb.append(ReportUtils.getReplaceStringForIN("res.origin_user_id", reportsSearchCriteria.getUsers(), false)
						+ "  AND");
			}

			sb.append(ReportUtils.getReplaceStringForIN("AGENT.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
			sb.append(" AND res.pnr IN (SELECT c.pnr FROM t_reservation_audit c WHERE c.template_code = '" + templateCode + "' ");

			if (!"EXTOHD".equals(templateCode)) {
				sb.append(" OR c.template_code    = 'NEWSPT' OR c.template_code    ='NEWRMP' ");
			}
			sb.append(") ");

			sb.append(" GROUP BY flg.flight_number,flgseg.est_time_departure_zulu,flgseg.segment_code,");
			sb.append(" AGENT.agent_code,AGENT.agent_name,AGENT.telephone,pnrseg.status) ORDER BY 1, 2 ");

		}

		return sb.toString();
	}

	public String getShowAgentChargeAdjustmentsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strNominalCodes = PlatformUtiltiies.constructINStringForInts(reportsSearchCriteria.getNominalCodes());
		List<String> carriersList = (List<String>) reportsSearchCriteria.getCarrierCodes();

		sb.append("SELECT a.agent_code,ta.agent_name, a.user_id, a.amount, a.tnx_date, b.description as type, a.notes, a.dr_cr  FROM t_agent_transaction a, t_agent_transaction_nc b, t_agent ta WHERE a.nominal_code = b.nominal_code ");
		sb.append(" AND (ta.AGENT_CODE = a.AGENT_CODE) ");
		if (reportsSearchCriteria.getUsers() != null && !reportsSearchCriteria.getUsers().isEmpty()) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN("a.user_id", reportsSearchCriteria.getUsers(), false));
		} else if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN("a.agent_code", reportsSearchCriteria.getAgents(), false));
		}

		if (strNominalCodes != null && !strNominalCodes.isEmpty()) {
			sb.append(" AND a.nominal_code IN (" + strNominalCodes + ") AND a.nominal_code = b.nominal_code ");
		}

		sb.append("AND a.txn_id IN "
				+ "(SELECT SUBSTR (c.content, INSTR (c.content, '||txnID:=') + LENGTH ('||txnID:='), INSTR (c.content, '||txnType:=') - LENGTH ('||txnType:=') - INSTR (c.content, '||txnID:=') + 2 )"
				+ "FROM t_audit c WHERE c.task_code = '1056' AND c.timestamp BETWEEN to_date (" + strFromdate
				+ " ) AND to_date ( " + strToDate + " ))");

		if (carriersList != null && !carriersList.isEmpty()) { // filter out by carrier
			sb.append("AND a.adjustment_carrierCode in (" + PlatformUtiltiies.constructINStringForCharactors(carriersList) + ")");
		}

		sb.append(" AND a.tnx_date between to_date (" + strFromdate + ") AND to_date ( " + strToDate + " ) ");
		sb.append("  order by a.tnx_date, a.agent_code, a.user_id, b.description");

		return sb.toString();
	}

	/**
	 * Method to return the data collection as one string.
	 * 
	 * @param parameters
	 * @return String - dataValue
	 */
	private String getSingleDataValue(Collection<String> parameters) {
		StringBuffer sbData = new StringBuffer();
		Iterator<String> it = null;
		String data = null;

		if ((parameters != null) && (!parameters.isEmpty())) {
			it = parameters.iterator();
			while (it.hasNext()) {
				data = it.next();
				if (data != null) {
					sbData.append("'");
					sbData.append(data);
					sbData.append("'");
					if (it.hasNext()) {
						sbData.append(",");
					}
				}
			}
		}
		return sbData.toString();
	}
	
	// Since schedule mop report doesn't use request handler to convert paymentTypes, method is moved to common place
	// Reason: nominal codes in T_PAX_TRNX_NOMINAL_CODE and T_AGENT_TRANSACTION_NC are different
	// MOP - Mode of payment report
	// FIXME add List<String> agentNominalCodes and filter by that
	private Collection<String> populatePaymentTypesForMOP(Collection<String> parameters) {
		Collection<String> paymentTypes = new ArrayList<String>();

		String refundCodes[] = { "29", "24", "23", "22", "26", "25", "31", "33", "37", "47" };

		for (String param : parameters) {
			if (param.equals("28")) {
				paymentTypes.add("28");
				paymentTypes.add(refundCodes[0]);
			} else if (param.equals("17")) {
				paymentTypes.add("17");
				paymentTypes.add(refundCodes[1]);
			} else if (param.equals("16")) {
				paymentTypes.add("16");
				paymentTypes.add(refundCodes[2]);
			} else if (param.equals("15")) {
				paymentTypes.add("15");
				paymentTypes.add(refundCodes[3]);
			} else if (param.equals("18")) {
				paymentTypes.add("18");
				paymentTypes.add(refundCodes[4]);
			} else if (param.equals("19")) {
				paymentTypes.add("19");
				paymentTypes.add(refundCodes[5]);
			} else if (param.equals("30")) {
				paymentTypes.add("30");
				paymentTypes.add(refundCodes[6]);
			} else if (param.equals("32")) {
				paymentTypes.add("32");
				paymentTypes.add(refundCodes[7]);
			} else if (param.equals("36")) {
				paymentTypes.add("36");
				paymentTypes.add("21");
				paymentTypes.add(refundCodes[8]);
			} else if (param.equals("41")) {
				// Generic debit payments
				paymentTypes.add("41");
			} else if (param.equals("46")) {
				paymentTypes.add("46");
				paymentTypes.add(refundCodes[9]);
			} else if (param.equals("48")) {
				// Voucher payments
				paymentTypes.add("48");
			} else if (param.equals("21")) {
				// Voucher payments
				paymentTypes.add("21");
			} else {
				paymentTypes.add(param);
			}
		}

		return paymentTypes;
	}

	private String getSpecificNominalCodes(Collection<String> parameters, String... nominalCodes) {
		Collection<String> filteredCodes = new ArrayList<String>();
		for (String nominalCode : nominalCodes) {
			if (parameters.contains(nominalCode)) {
				filteredCodes.add(nominalCode);
			}
		}
		return getSingleDataValue(filteredCodes);
	}

	/**
	 * Method to return the data collection as one string.
	 * 
	 * @param parameters
	 * @return String - dataValue
	 */
	/**
	 * private String getSingleDataValue(String[] parameters) { StringBuffer sbData = new StringBuffer(); String data =
	 * null;
	 * 
	 * if ((parameters != null) && parameters.length > 0) { for (int len = 0; len < parameters.length; len++) { data =
	 * parameters[len]; if (data != null) { sbData.append("'"); sbData.append(data); sbData.append("'"); if (len !=
	 * (parameters.length - 1)) { sbData.append(","); } } } } return sbData.toString(); }
	 **/

	public String getScheduleCapacityVarienceQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		sb.append("select CURR.origin, CURR.destination, CURR.segment_code , ");
		sb.append("CURR.NoOfFlights, (nvl(CURR.ask, 0) / 1000) ask , nvl(CURR.NoOfPax, 0) NoOfPax, ");
		sb.append("((nvl(CURR.ask, 0) - nvl(PREV.ask, 0)) / 1000) varianceASK , ");
		sb.append("(nvl(CURR.NoOfFlights, 0 ) - nvl(PREV.NoOfFlights,0)) varianceNoOfFlights, ");
		sb.append("(nvl(CURR.NoOfPax, 0) - nvl(PREV.NoOfPax,0)) varianceNoOfPAX ");
		sb.append("from (SELECT count(a.flight_id) NoOfFlights, a.origin, a.destination,");
		sb.append(" sum(a.available_seat_kilometers) ask , sum(noPax) NoOfPax, maxSeg.segment_code ");
		sb.append("FROM t_flight a,(select b.flight_id, sum(b.sold_seats) as noPax ");
		sb.append("from t_fcc_seg_alloc b group by b.flight_id) sold, ");
		sb.append("(SELECT  seg.schedule_id,seg.segment_code FROM T_FLIGHT_SCHEDULE_SEGMENT seg ");
		sb.append("WHERE length(seg.segment_code) = (SELECT max(length(sseg.segment_code)) ");
		sb.append("FROM T_FLIGHT_SCHEDULE_SEGMENT sseg WHERE seg.schedule_id = sseg.schedule_id)) ");
		sb.append("maxSeg where a.departure_date between  to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
		sb.append("and  to_date('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') and");

		if (reportsSearchCriteria.getFlightNumber() != null) {
			sb.append(" a.flight_number= '" + reportsSearchCriteria.getFlightNumber() + "' AND ");
		}
		if (reportsSearchCriteria.getOperationType() != null) {
			sb.append(" a.operation_type_id = '" + reportsSearchCriteria.getOperationType() + "' AND ");
		}
		if (reportsSearchCriteria.getSectorFrom() != null && reportsSearchCriteria.getSectorTo() != null) {
			sb.append(" a.ORIGIN='" + reportsSearchCriteria.getSectorFrom() + "' AND ");
			sb.append(" a.DESTINATION='" + reportsSearchCriteria.getSectorTo() + "' AND ");
		} else if (reportsSearchCriteria.getSectorFrom() != null) {
			sb.append(" a.ORIGIN='" + reportsSearchCriteria.getSectorFrom() + "' AND ");
		} else if (reportsSearchCriteria.getSectorTo() != null) {
			sb.append(" a.DESTINATION='" + reportsSearchCriteria.getSectorTo() + "' AND ");
		}

		sb.append(" a.status <> 'CNX' ");
		sb.append("and a.flight_id = sold.flight_id and a.schedule_id = maxSeg.schedule_id ");
		sb.append("group by a.origin, a.destination, maxSeg.segment_code) CURR, ");
		sb.append("(SELECT count(a.flight_id) NoOfFlights, a.origin, a.destination,");
		sb.append(" sum(a.available_seat_kilometers) ask , sum(noPax) NoOfPax, ");
		sb.append("maxSeg.segment_code FROM t_flight a,(select b.flight_id, ");
		sb.append("sum(b.sold_seats) as noPax from t_fcc_seg_alloc b group by b.flight_id) sold,");
		sb.append("(SELECT  seg.schedule_id,seg.segment_code FROM T_FLIGHT_SCHEDULE_SEGMENT seg ");
		sb.append("WHERE length(seg.segment_code) = (SELECT max(length(sseg.segment_code)) ");
		sb.append("FROM T_FLIGHT_SCHEDULE_SEGMENT sseg ");
		sb.append("WHERE seg.schedule_id = sseg.schedule_id)) maxSeg ");
		sb.append("where a.departure_date between  ");
		sb.append(" to_date(to_char(to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "', 'DD-MON-YYYY')-(365), 'DD-MON-YYYY') || ' 00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
		sb.append(" and  to_date(to_char(to_date('" + reportsSearchCriteria.getDateRangeTo()
				+ "', 'DD-MON-YYYY')-(365), 'DD-MON-YYYY') || ' 23:59:59', 'DD-MON-YYYY HH24:mi:ss') and ");
		if (reportsSearchCriteria.getFlightNumber() != null) {
			sb.append(" a.flight_number= '" + reportsSearchCriteria.getFlightNumber() + "' AND ");
		}
		if (reportsSearchCriteria.getOperationType() != null) {
			sb.append(" a.operation_type_id = '" + reportsSearchCriteria.getOperationType() + "' AND ");
		}
		if (reportsSearchCriteria.getSectorFrom() != null && reportsSearchCriteria.getSectorTo() != null) {
			sb.append(" a.ORIGIN='" + reportsSearchCriteria.getSectorFrom() + "' AND ");
			sb.append(" a.DESTINATION='" + reportsSearchCriteria.getSectorTo() + "' AND ");
		} else if (reportsSearchCriteria.getSectorFrom() != null) {
			sb.append(" a.ORIGIN='" + reportsSearchCriteria.getSectorFrom() + "' AND ");
		} else if (reportsSearchCriteria.getSectorTo() != null) {
			sb.append(" a.DESTINATION='" + reportsSearchCriteria.getSectorTo() + "' AND ");
		}
		sb.append("  a.status <> 'CNX' ");
		sb.append("and a.flight_id = sold.flight_id ");
		sb.append("and a.schedule_id = maxSeg.schedule_id group by a.origin, ");
		sb.append("a.destination, maxSeg.segment_code) PREV ");
		sb.append("WHERE CURR.ORIGIN=PREV.ORIGIN(+) AND CURR.DESTINATION=PREV.DESTINATION(+) ");
		sb.append(" AND CURR.segment_code = PREV.segment_code(+) ");
		sb.append(" ORDER BY CURR.origin,  CURR.destination");

		return sb.toString();
	}

	public String getViewSeatInventoryFareMovements(ReportsSearchCriteria reportsSearchCriteria) {
		String combination = null;
		String origin = null;
		String destination = null;
		// boolean isStation = reportsSearchCriteria.getStation().equals("YES") ? true: false;
		StringBuilder sb = new StringBuilder();

		String strOperationType = reportsSearchCriteria.getOperationType();
		origin = reportsSearchCriteria.getSectorFrom();
		destination = reportsSearchCriteria.getSectorTo();
		List<String> viaPoints = reportsSearchCriteria.getViaPoints();
		String flightStatus = reportsSearchCriteria.getFlightStatus();
		boolean matchExactCombinationOnly = reportsSearchCriteria.isMatchCombination();
		String carrierCode = getSingleDataValue(reportsSearchCriteria.getCarrierCodes());

		if (reportsSearchCriteria.getReportOption().equals(ReportsSearchCriteria.FORWARD_BOOKING)) {
			if (reportsSearchCriteria.isByAgent()) {

				sb.append("SELECT departure_date,flight_number,status,segment_code,agent_code,agent_name,station_name,");
				sb.append(
						"cabin_class_code,logical_cabin_class_code,allocated_seats,oversell_seats,curtailed_seats,fixed_seats,sold_seats,on_hold_seats,");
				sb.append("available_seats,seat_factor,amount,surcharge,SEG_COUNT, ");
				sb.append(
						"CASE WHEN (sold_seats - fixed_available) < 0 THEN 0 ELSE (sold_seats - fixed_available) END AS sold_seats_new,");
				sb.append(
						"CASE WHEN ((allocated_seats+oversell_seats-curtailed_seats-on_hold_seats)-(sold_seats+ fixed_seats)) < 0 THEN 0 ");
				sb.append(
						"ELSE ((allocated_seats+oversell_seats-curtailed_seats-on_hold_seats)-(sold_seats+ fixed_seats)) END AS available_seats_new, baggage_charge ");
				sb.append(" FROM ( SELECT fs.est_time_departure_local departure_date, f.flight_number flight_number, ");
				sb.append(
						"f.status, fs.segment_code segment_code,  a.agent_name, st.station_name, ");
				sb.append(" nvl(ps.mkting_agent_code,DECODE(nvl(ps.STATUS_MOD_CHANNEL_CODE,r.ORIGIN_CHANNEL_CODE), 4, 'Web',20,'IOS',21,'ANDROID', ps.STATUS_AGENT_CODE)) agent_code, ");
				sb.append(
						"lcc.cabin_class_code, fsa.logical_cabin_class_code, fsa.allocated_seats, fsa.oversell_seats, fsa.curtailed_seats, fsa.fixed_seats, ");
				sb.append(
						"COUNT(DISTINCT (CASE WHEN PS.STATUS='CNF' AND P.STATUS='CNF' and p.pax_type_code in('AD','CH') AND Ppfs.Booking_Code IN (SELECT Booking_Code FROM t_booking_class WHERE cabin_class_code=fsa.logical_cabin_class_code) THEN ppfs.ppfs_id ELSE NULL END ))SOLD_SEATS, ");
				sb.append(
						"COUNT(DISTINCT (CASE WHEN PS.STATUS='CNF' AND P.STATUS='OHD' and p.pax_type_code in('AD','CH') AND Ppfs.Booking_Code IN (SELECT Booking_Code FROM t_booking_class WHERE cabin_class_code=fsa.logical_cabin_class_code) THEN ppfs.ppfs_id ELSE NULL END )) ON_HOLD_SEATS,fsa.available_seats, ");
				sb.append(
						"ROUND(((NVL((count(DISTINCT (CASE WHEN PS.STATUS='CNF' AND P.STATUS='CNF' AND p.pax_type_code in('AD','CH') AND Ppfs.Booking_Code IN (SELECT Booking_Code FROM t_booking_class WHERE cabin_class_code=fsa.logical_cabin_class_code) THEN ppfs.ppfs_id ELSE NULL END ))),0) + ");
				sb.append(
						"NVL((count(DISTINCT (CASE WHEN PS.STATUS='CNF' AND P.STATUS='OHD' AND p.pax_type_code in('AD','CH') AND Ppfs.Booking_Code IN (SELECT Booking_Code FROM t_booking_class WHERE cabin_class_code=fsa.logical_cabin_class_code) THEN ppfs.ppfs_id ELSE NULL END ))),0))/fsa.allocated_seats),2) SEAT_FACTOR, ");
				sb.append("SUM(decode(ppoc.charge_group_code, 'FAR', ppsc.amount, 0)) AMOUNT, ");
				sb.append("SUM(decode(ppoc.charge_group_code, 'FAR', 0, 'TAX', 0,ppsc.amount)) surCharge, ");
				sb.append("COUNT(distinct ps.pnr_seg_id) SEG_COUNT, ");
				sb.append("MAX (NVL ((SELECT SUM (fccbc.available_seats) FROM t_fcc_seg_bc_alloc fccbc,t_booking_class bc ");
				sb.append("WHERE fccbc.fccsa_id = fsa.fccsa_id AND fccbc.flight_id = fsa.flight_id ");
				sb.append(
						"AND fccbc.booking_code = bc.booking_code AND bc.fixed_flag = 'Y' ), 0 ) ) AS fixed_available, SUM(decode(c.charge_code,'BG',ppsc.amount,0)) as baggage_charge  ");
				sb.append("FROM  t_pnr_segment ps, t_pnr_pax_fare_segment ppfs, t_pnr_pax_seg_charges ppsc, ");
				sb.append("t_pnr_pax_ond_charges ppoc, t_charge_rate cr,t_charge c, T_PNR_PAX_FARE PPF, t_pnr_passenger p, ");
				sb.append("t_agent a,t_station st, t_booking_class bc ,");
				sb.append("t_fcc_seg_alloc fsa,t_flight_segment fs, t_flight f, t_logical_cabin_class lcc , t_reservation r ");
				sb.append("WHERE p.pnr_pax_id=ppf.pnr_pax_id and ps.flt_seg_id= fsa.flt_seg_id and  ps.pnr = r.pnr ");
				sb.append("AND p.pnr=ps.pnr and ps.pnr_seg_id = ppfs.pnr_seg_id and ppfs.ppfs_id = ppsc.ppfs_id ");
				sb.append(
						"AND ppoc.pft_id = ppsc.pft_id and ppoc.ppf_id=ppf.ppf_id and ppf.ppf_id=ppfs.ppf_id AND ppfs.ppfs_id=ppsc.ppfs_id ");

				if (reportsSearchCriteria.getDateRange2From() != null && reportsSearchCriteria.getDateRange2To() != null) {
					sb.append("AND ps.status_mod_date BETWEEN to_date('" + reportsSearchCriteria.getDateRange2From()
							+ "', 'DD-MON-YYYY HH24:mi:ss') ");
					sb.append("AND to_date('" + reportsSearchCriteria.getDateRange2To() + "', 'DD-MON-YYYY HH24:mi:ss') ");
				}

				sb.append("AND ppoc.charge_rate_id=cr.charge_rate_id(+) ");
				sb.append("AND cr.charge_code=c.charge_code(+) AND ps.mkting_agent_code = a.agent_code (+) ");
				sb.append(
						"AND ps.flt_seg_id = fs.flt_seg_id AND f.flight_id = fs.flight_id AND a.station_code = st.station_code(+) ");
				sb.append("AND fsa.logical_cabin_class_code = lcc.logical_cabin_class_code ");
				sb.append(
						"AND (C.SUR_REVENUE='Y' OR PPOC.CHARGE_GROUP_CODE='FAR' OR C.charge_code in('BG')) AND PPFS.BOOKING_CODE ");
				sb.append("IN(SELECT B.BOOKING_CODE FROM T_BOOKING_CLASS B WHERE BC_TYPE NOT IN('STANDBY','OPENRT' )) ");
				sb.append(
						" and ppfs.booking_code   = bc.booking_code and bc.logical_cabin_class_code = fsa.logical_cabin_class_code ");
				if (reportsSearchCriteria.getDateRangeFrom() != null && reportsSearchCriteria.getDateRangeTo() != null) {
					sb.append("AND fs.est_time_departure_local BETWEEN to_date('" + reportsSearchCriteria.getDateRangeFrom()
							+ "', 'DD-MON-YYYY HH24:mi:ss')");
					sb.append("AND to_date('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss') ");
				}

				if (reportsSearchCriteria.getFlightNumber() != null) {
					if (reportsSearchCriteria.isExactFlightNumber()) {
						sb.append(" AND f.flight_number = '" + reportsSearchCriteria.getFlightNumber() + "' ");
					} else {
						sb.append(" AND f.flight_number LIKE UPPER('" + reportsSearchCriteria.getFlightNumber() + "%')	");
					}
				}
				if (isNotEmptyOrNull(carrierCode)) {
					sb.append(" AND SUBSTR(f.flight_number,1,2) IN(" + carrierCode + ")	");
				}

				if (!((origin == null || origin.equals("")) && (viaPoints == null || viaPoints.size() == 0)
						&& (destination == null || destination.equals("")))) {
					combination = PlatformUtiltiies.prepareSegmentCode(origin, destination, viaPoints, matchExactCombinationOnly);
					sb.append(" AND fs.segment_code LIKE '" + combination + "' ");
				}

				if (!flightStatus.equalsIgnoreCase("All")) {
					sb.append(" and f.status in('" + flightStatus + "') ");
				} else {
					sb.append(" and f.status IN ('" + FlightStatusEnum.ACTIVE + "','" + FlightStatusEnum.CLOSED + "','"
							+ FlightStatusEnum.CANCELLED + "') ");
				}
				if (origin != null) {
					sb.append(" and  f.origin = '" + origin + "' ");
				}
				if (destination != null) {
					sb.append(" and f.destination = '" + destination + "' ");
				}

				// check for operation type
				if (isNotEmptyOrNull(strOperationType)) {
					if (!strOperationType.equalsIgnoreCase("All")) {
						sb.append(" and	f.operation_type_id in " + strOperationType); // Operation Type
					}
				}

				sb.append(" GROUP BY  a.agent_name,st.station_name, ");
				sb.append(" nvl(ps.mkting_agent_code,DECODE(nvl(ps.STATUS_MOD_CHANNEL_CODE,r.ORIGIN_CHANNEL_CODE), 4, 'Web',20,'IOS',21,'ANDROID', ps.STATUS_AGENT_CODE)) , ");
				sb.append("fs.est_time_departure_local, f.flight_number, f.status, fs.segment_code,");
				sb.append(
						"lcc.cabin_class_code, fsa.logical_cabin_class_code, fsa.allocated_seats, fsa.oversell_seats, fsa.curtailed_seats, fsa.fixed_seats, fsa.available_seats)");

			} else {

				sb.append(
						" SELECT station,departure_date,flight_number,status,segment_code,cabin_class_code, logical_cabin_class_code, allocated_seats,");
				sb.append("oversell_seats,curtailed_seats,fixed_seats,sold_seats,on_hold_seats,available_seats,");
				sb.append("seat_factor,amount,surcharge, baggage_charge,");
				sb.append("CASE WHEN (sold_seats > 0) AND (fixed_available > 0 ) ");
				sb.append("THEN (sold_seats + fixed_available) ELSE sold_seats END AS sold_seats_new,");
				sb.append(
						"CASE WHEN ((allocated_seats+oversell_seats-curtailed_seats-on_hold_seats)-(sold_seats+ fixed_seats)) < 0 THEN 0 ");
				sb.append(
						"ELSE ((allocated_seats+oversell_seats-curtailed_seats-on_hold_seats)-(sold_seats+ fixed_seats)) END AS available_seats_new FROM (");

				if (!reportsSearchCriteria.isByStation()) {
					sb.append(" select 's' station, departure_date, flight_number, status, segment_code, cabin_class_code,");
					sb.append(" logical_cabin_class_code, ");
					sb.append(
							" allocated_seats, oversell_seats, curtailed_seats, fixed_seats, sum(sold_seats) sold_seats, sum(on_hold_seats) on_hold_seats, available_seats ,  ");
					sb.append(
							" SEAT_FACTOR, SUM(amount) amount, SUM(surCharge) surCharge, fixed_available, sum(baggage_charge) baggage_charge  ");
					sb.append(" from ( ");
				}
				sb.append(
						" select d.station_code station, b.est_time_departure_local as departure_date, a.flight_number, a.status, ");
				sb.append(
						" b.segment_code, (SELECT x.cabin_class_code FROM t_logical_cabin_class x WHERE x.logical_cabin_class_code = NVL (lcc.logical_cabin_class_code, c.logical_cabin_class_code )) ");
				sb.append(
						" AS cabin_class_code, NVL (lcc.logical_cabin_class_code, c.logical_cabin_class_code) AS logical_cabin_class_code, ");
				sb.append(" c.allocated_seats, c.oversell_seats, c.curtailed_seats, c.fixed_seats, ");
				sb.append(" NVL(d.SOLD_SEATS,0) SOLD_SEATS, NVL(d.onhold_seats,0) on_hold_seats, c.available_seats , ");
				sb.append(
						" round(((NVL((select sum(x.SOLD_SEATS) from t_rep_fwd_bkg x where x.flt_seg_id=d.flt_seg_id and x.logical_cabin_class_code=d.logical_cabin_class_code),0) ");
				sb.append(
						" +NVL((select sum(x.onhold_seats) from t_rep_fwd_bkg x where x.flt_seg_id=d.flt_seg_id and x.logical_cabin_class_code=d.logical_cabin_class_code),0))/c.allocated_seats),2) SEAT_FACTOR, ");
				sb.append(" round(nvl(d.far_amt,0),2) amount, ");
				sb.append(" round(nvl(d.sur_amt,0),2) surCharge, ");

				sb.append(" round(nvl(d.baggage_charge,0),2) baggage_charge, ");
				sb.append(" NVL ((SELECT SUM (fccbc.available_seats) FROM t_fcc_seg_bc_alloc fccbc,t_booking_class bc ");
				sb.append(" WHERE fccbc.fccsa_id = c.fccsa_id AND fccbc.flight_id = c.flight_id ");
				sb.append(" AND fccbc.booking_code = bc.booking_code AND bc.fixed_flag = 'Y' ), 0 ) AS fixed_available");

				sb.append(" from t_flight a, t_flight_segment b, t_fcc_seg_alloc c ,t_logical_cabin_class lcc,t_rep_fwd_bkg d ");
				sb.append(" where a.flight_id = b.flight_id ");
				sb.append(" and b.flt_seg_id = c.flt_seg_id ");
				sb.append(" and c.flt_seg_id = d.flt_seg_id (+) ");
				sb.append(" and d.logical_cabin_class_code  = lcc.logical_cabin_class_code (+)");
				sb.append(" AND c.logical_cabin_class_code = d.logical_cabin_class_code (+)");
				sb.append(" and b.est_time_departure_local between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'DD-MON-YYYY HH24:mi:ss') ");
				sb.append("	and to_date('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss') ");

				if (reportsSearchCriteria.getFlightNumber() != null) {
					if (reportsSearchCriteria.isExactFlightNumber()) {
						sb.append(" AND a.flight_number = '" + reportsSearchCriteria.getFlightNumber() + "' ");
					} else {
						sb.append(" and a.flight_number LIKE UPPER('" + reportsSearchCriteria.getFlightNumber() + "%')	");
					}
				}
				if (isNotEmptyOrNull(carrierCode)) {
					sb.append(" and SUBSTR(A.flight_number,1,2) IN(" + carrierCode + ")	");
				}

				if (!((origin == null || origin.equals("")) && (viaPoints == null || viaPoints.size() == 0)
						&& (destination == null || destination.equals("")))) {
					combination = PlatformUtiltiies.prepareSegmentCode(origin, destination, viaPoints, matchExactCombinationOnly);
					sb.append(" 	AND b.segment_code LIKE '" + combination + "' ");
				}

				if (!flightStatus.equalsIgnoreCase("All")) {
					sb.append(" and a.status in('" + flightStatus + "') ");
				} else {
					sb.append(" and a.status IN ('" + FlightStatusEnum.ACTIVE + "','" + FlightStatusEnum.CLOSED + "','"
							+ FlightStatusEnum.CANCELLED + "') ");
				}
				if (origin != null) {
					sb.append(" and  a.origin = '" + origin + "' ");
				}
				if (destination != null) {
					sb.append(" and a.destination = '" + destination + "' ");
				}

				// check for operation type
				if (isNotEmptyOrNull(strOperationType)) {
					if (!strOperationType.equalsIgnoreCase("All")) {
						sb.append(" and		a.operation_type_id in " + strOperationType); // Operation Type
					}
				}

				if (reportsSearchCriteria.isByStation()) {
					sb.append("	order by b.est_time_departure_local, a.flight_number, b.segment_code ) ");
				} else {
					sb.append(
							" ) group by departure_date, flight_number, status, segment_code, cabin_class_code, logical_cabin_class_code, allocated_seats, oversell_seats,  ");
					sb.append(" curtailed_seats, fixed_seats, available_seats, SEAT_FACTOR,fixed_available ) ");
					sb.append(" order by departure_date, flight_number, segment_code ");
				}
			}
		}

		return sb.toString();
	}

	/**
	 * 
	 * Note : All the following queries combine existing payments with LCC interline payments, the only exception are
	 * External payment reports (Holiday System)
	 * 
	 * TF - The Entity code is hard coded for pax transactions.Correct it when properly added
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */

	public String getModeofPaymentQuery(ReportsSearchCriteria reportsSearchCriteria) {
		
		Collection<String> paymentTypes = populatePaymentTypesForMOP(reportsSearchCriteria.getPaymentTypes());

		StringBuilder sb = new StringBuilder();
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		String singleDataValuePayments = getSingleDataValue(paymentTypes);
		String filteredNominalCodes = getSpecificNominalCodes(paymentTypes, "19", "21");
		String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_SUMMARY)) {
			sb.append(
					"SELECT DECODE(PAY, 15, 1, 16, 2, 28, 3, 17, 4, 19, 5, 18, 6, 30, 7, 32, 8, 36, 9, 41, 10, 46, 11, 48, 12,21) as PAYMENT_CODE,  ");
			sb.append(
					"  DECODE(PAY,21,'BSP EXTERNAL',  36, 'BSP', 19, 'On Account', 18, 'Cash', 15, 'Visa', 16, 'Master', 28, 'Amex', 17, 'Diners', 30, 'Generic', 32, 'CMI', 41, 'Generic debit', 42, '    From :GA', 43, '    From :3O', 44, '    From :E5', 45, '    From :9P', 46, 'Loyalty Payment', 48, 'Voucher Payment') AS PAYMENT_MODE,  ");
			sb.append(
					"  -1 * AMT                                                                                                                                                                                                                                  AS AMT,  ");
			sb.append("  PAY,  ");
			sb.append("  GW , ENTITY ");
			sb.append("FROM  ");
			sb.append(
					"  (SELECT DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE) AS PAY,  ");
			sb.append(
					"    SUM(AMT)                                                                                                 AS AMT,  ");
			sb.append("    GW, ENTITY  ");
			sb.append("  FROM  ");
			sb.append("    (SELECT A.NOMINAL_CODE,  ");
			sb.append("      A.AMOUNT AMT,  ");
			sb.append("      (SELECT DISTINCT PG.PROVIER_NAME  ");
			sb.append("        || '-'  ");
			sb.append("        || PG.MODULE_CODE  ");
			sb.append("      FROM T_PAYMENT_GATEWAY PG  ");
			sb.append("      WHERE PG.PAYMENT_GATEWAY_ID IN  ");
			sb.append("        (SELECT PGC.PAYMENT_GATEWAY_ID  ");
			sb.append("        FROM T_PAYMENT_GATEWAY_CURRENCY PGC  ");
			sb.append("        WHERE PGC.PAYMENT_GATEWAY_CURRENCY_CODE IN  ");
			sb.append("          (SELECT GATEWAY_NAME  ");
			sb.append("          FROM T_CCARD_PAYMENT_STATUS CC  ");
			sb.append("          WHERE CC.TRANSACTION_REF_NO IN  ");
			sb.append("            (SELECT PCD.TRANSACTION_REF_NO  ");
			sb.append("            FROM T_RES_PCD PCD  ");
			sb.append("            WHERE PCD.TXN_ID = A.TXN_ID  ");
			sb.append("            )  ");
			sb.append("          )  ");
			sb.append("        )  ");
			sb.append("      ) GW , 'E_RES' AS ENTITY  ");
			sb.append("    FROM T_PAX_TRANSACTION A,  ");
			sb.append("      T_AGENT G  ");
			sb.append("    WHERE A.NOMINAL_CODE IN ( " + singleDataValuePayments + " )  ");
			sb.append("    AND A.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("    AND A.AGENT_CODE                = G.AGENT_CODE(+)  ");
			sb.append("    AND (A.PAYMENT_CARRIER_CODE     = '" + thisAirlineCode + "'  ");
			sb.append("    OR A.PAYMENT_CARRIER_CODE      IS NULL )  "); // we want to get only the payments made by
																			// this carrier.
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("    UNION ALL  ");
			sb.append("    SELECT A.NOMINAL_CODE,  ");
			sb.append("      A.AMOUNT AMT,  ");
			sb.append("      (SELECT DISTINCT PG.PROVIER_NAME  ");
			sb.append("        || '-'  ");
			sb.append("        || PG.MODULE_CODE  ");
			sb.append("      FROM T_PAYMENT_GATEWAY PG  ");
			sb.append("      WHERE PG.PAYMENT_GATEWAY_ID IN  ");
			sb.append("        (SELECT PGC.PAYMENT_GATEWAY_ID  ");
			sb.append("        FROM T_PAYMENT_GATEWAY_CURRENCY PGC  ");
			sb.append("        WHERE PGC.PAYMENT_GATEWAY_CURRENCY_CODE IN  ");
			sb.append("          (SELECT GATEWAY_NAME  ");
			sb.append("          FROM T_CCARD_PAYMENT_STATUS CC  ");
			sb.append("          WHERE CC.TRANSACTION_REF_NO IN  ");
			sb.append("            (SELECT PCD.TRANSACTION_REF_NO  ");
			sb.append("            FROM T_RES_PCD PCD  ");
			sb.append("            WHERE PCD.EXT_TXN_ID = A.PAX_EXT_CARRIER_TXN_ID  ");
			sb.append("            )  ");
			sb.append("          )  ");
			sb.append("        )  ");
			sb.append("      ) GW,   'E_RES' AS ENTITY");
			sb.append("    FROM T_PAX_EXT_CARRIER_TRANSACTIONS A  ");
			sb.append("    WHERE A.NOMINAL_CODE IN (" + singleDataValuePayments + ")  ");
			sb.append("    AND A.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");

			if (filteredNominalCodes != null && !filteredNominalCodes.isEmpty()) {
				sb.append("     UNION ALL ");
				sb.append("      SELECT AGENTTNX.NOMINAL_CODE, ");
				sb.append("      (AGENTTNX.AMOUNT*-1)  AS AMOUNT ,  ");
				sb.append("      null as GW ,PROD.ENTITY_CODE AS ENTITY");
				sb.append("      FROM T_AGENT_TRANSACTION AGENTTNX, T_PRODUCT PROD");
				sb.append("    WHERE AGENTTNX.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");

				sb.append("      AND AGENTTNX.NOMINAL_CODE IN (" + filteredNominalCodes + ") ");
				sb.append("     AND AGENTTNX.PRODUCT_TYPE <> 'P' ");
				sb.append("     AND AGENTTNX.PRODUCT_TYPE = PROD.PRODUCT_TYPE  ");
			}

			sb.append("    UNION ALL  ");
			sb.append("    SELECT DECODE(PAYMENT_CARRIER_CODE,'G9',42,'3O',43,'E5',44, '9P', 45) AS NOMINAL_CODE,  ");
			sb.append("      SUM(AMT)                                                  AS AMOUNT,  ");
			sb.append("      NULL                                                      AS GW , 'E_RES' AS ENTITY ");
			sb.append("    FROM  ");
			sb.append("      (SELECT A.NOMINAL_CODE,  ");
			sb.append("        A.AMOUNT AMT,  ");
			sb.append("        A.PAYMENT_CARRIER_CODE,  ");
			sb.append("        NULL AS GW  ");
			sb.append("      FROM T_PAX_TRANSACTION A,  ");
			sb.append("        T_AGENT G  ");
			sb.append("      WHERE A.NOMINAL_CODE IN (" + singleDataValuePayments + ")  ");
			sb.append("      AND A.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("      AND A.AGENT_CODE                = G.AGENT_CODE(+)  ");
			sb.append("      AND (A.PAYMENT_CARRIER_CODE    <> '" + thisAirlineCode + "'  ");
			sb.append("      AND A.PAYMENT_CARRIER_CODE     IS NOT NULL)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("      )  ");
			sb.append("    GROUP BY DECODE(PAYMENT_CARRIER_CODE,'G9',42,'3O',43,'E5',44, '9P' , 45)  ");
			sb.append("    )  ");
			sb.append(
					"  GROUP BY DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE),  ");
			sb.append("    GW,  ENTITY");
			sb.append("  ) where ENTITY =  '"+  reportsSearchCriteria.getEntity()   +"'");
			sb.append(" ORDER BY PAYMENT_CODE");

		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_CURRENCY_SUMMARY)) {

			if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewModeOfPaymentSummaryByCurrencyQuery(reportsSearchCriteria,
						singleDataValuePayments));
			} else {
				sb.append(
						"SELECT DECODE(PAY, 15, 1, 16, 2, 28, 3, 17, 4, 19, 5, 18, 6, 30, 7, 32, 8, 36, 9, 41, 10, 48, 11,21) as PAYMENT_CODE,  ");
				sb.append(
						"  DECODE(PAY,21,'BSP EXTERNAL', 36, 'BSP', 19, 'On Account', 18, 'Cash', 15, 'Visa', 16, 'Master', 28, 'Amex', 17, 'Diners', 30, 'Generic', 32, 'CMI', 41, 'Generic debit', 46, 'Loyalty Payment' ,48, 'Voucher Payment') AS PAYMENT_MODE,  ");
				sb.append("  PAYMENT_CURRENCY_CODE,  ");
				sb.append("  AMT,  ");
				sb.append("  AMT_BASE,  ");
				sb.append("  PAY  , ENTITY   ");
				sb.append("  FROM  ");
				sb.append("  (SELECT PAYMENT_CURRENCY_CODE,  ");
				sb.append("    (-1 * SUM(TOTAL_AMOUNT_PAYCUR)) AMT,  ");
				sb.append("    (-1 * SUM(TOTAL_AMOUNT)) AMT_BASE,  ");
				sb.append(
						"    DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE) PAY , ENTITY ");
				sb.append("  FROM  ");
				sb.append("    (SELECT A.PAYMENT_CURRENCY_CODE AS PAYMENT_CURRENCY_CODE,  ");
				sb.append("      A.AMOUNT_PAYCUR               AS TOTAL_AMOUNT_PAYCUR,  ");
				sb.append("      A.AMOUNT                      AS TOTAL_AMOUNT,  ");
				sb.append("      A.NOMINAL_CODE                AS NOMINAL_CODE,  ");
				sb.append("     'E_RES' AS ENTITY  ");
				sb.append("    FROM T_PAX_TRANSACTION A,  ");
				sb.append("      T_AGENT G  ");
				sb.append("    WHERE A.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("    AND A.AGENT_CODE                = G.AGENT_CODE(+)  ");
				// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("    AND A.NOMINAL_CODE             IN (" + singleDataValuePayments + ")  ");
				sb.append("    UNION ALL  ");
				sb.append("    SELECT A.PAYCUR_CODE     AS PAYMENT_CURRENCY_CODE,  ");
				sb.append("      A.AMOUNT_PAYCUR AS TOTAL_AMOUNT_PAYCUR,  ");
				sb.append("      A.AMOUNT        AS TOTAL_AMOUNT,  ");
				sb.append("      A.NOMINAL_CODE         AS NOMINAL_CODE,  ");
				sb.append("     'E_RES' AS ENTITY  ");
				sb.append("    FROM T_PAX_EXT_CARRIER_TRANSACTIONS A,  ");
				sb.append("      T_AGENT G  ");
				sb.append("    WHERE A.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("    AND A.AGENT_CODE                = G.AGENT_CODE(+)  ");
				// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("    AND A.NOMINAL_CODE             IN (" + singleDataValuePayments + ")  ");

				sb.append("    UNION ALL  ");
				sb.append("    SELECT A.CURRENCY_CODE     AS PAYMENT_CURRENCY_CODE,  ");
				sb.append("     ( A.AMOUNT * -1 ) AS TOTAL_AMOUNT_PAYCUR,  ");
				sb.append("      ( A.AMOUNT * -1 )    AS TOTAL_AMOUNT,  ");
				sb.append("      A.NOMINAL_CODE         AS NOMINAL_CODE,  ");
				sb.append("      PROD.ENTITY_CODE AS ENTITY    ");
				sb.append("    FROM T_AGENT_TRANSACTION A,  ");
				sb.append("      T_AGENT G ,  T_PRODUCT PROD  ");
				sb.append("    WHERE A.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("    AND A.AGENT_CODE                = G.AGENT_CODE(+)  ");
				sb.append("    AND A.NOMINAL_CODE             IN (" + singleDataValuePayments + ")  ");
				sb.append("     AND A.PRODUCT_TYPE <> 'P' ");
				sb.append("     AND A.PRODUCT_TYPE = PROD.PRODUCT_TYPE  ");

				sb.append("    )  ");
				sb.append(
						"  GROUP BY DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE),  ");
				sb.append("    PAYMENT_CURRENCY_CODE  , ENTITY");
				sb.append("  ) WHERE  ENTITY = '"+ reportsSearchCriteria.getEntity()+"' ");
				sb.append("  ORDER BY 1,2");
			}
		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_ONACCOUNT_DETAIL)) {

			sb.append("SELECT DECODE(VPAXTXN.PNR,null,' ',VPAXTXN.PNR) AS PNR ,  ");
			sb.append("  (VPAXTXN.TITLE  ");
			sb.append("  || ' '  ");
			sb.append("  || VPAXTXN.PNR_FIRST_NAME  ");
			sb.append("  || ' '  ");
			sb.append("  || VPAXTXN.PNR_LAST_NAME) AS PNR_PAX_NAME,  ");
			sb.append("  DECODE(SALES_CHANNEL_CODE, 4, 'Web',20,'IOS',21,'ANDROID', DISPLAY_NAME) DISPLAY_NAME,  ");
			sb.append("  TO_CHAR(VPAXTXN.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,  ");
			sb.append("  (VPAXTXN.AMOUNT * -1)                   AS AMOUNT ,  ");
			sb.append("  VPAXTXN.ACTUAL_PAY                      AS ACTUAL_PAY,  ");
			sb.append("  VPAXTXN.PAY_REF                         AS PAY_REF , ");
			sb.append("  VPAXTXN.PRODUCT_TYPE                    AS PRODUCT_TYPE,  ");
			sb.append("  VPAXTXN.ENTITY                          AS ENTITY  ");
			sb.append("FROM  ");
			sb.append("  (SELECT V.PNR,  ");
			sb.append("    V.TITLE,  ");
			sb.append("    V.PNR_FIRST_NAME,  ");
			sb.append("    V.PNR_LAST_NAME,  ");
			sb.append("    V.AMOUNT,  ");
			sb.append("    V.TNX_DATE,  ");
			sb.append("    V.NOMINAL_CODE,  ");
			sb.append("    V.SALES_CHANNEL_CODE,  ");
			sb.append("    V.USER_ID,  ");
			sb.append("    actual_p.payment_mode AS ACTUAL_PAY ,  ");
			sb.append("    pt.EXT_REFERENCE      AS PAY_REF , 'P' AS PRODUCT_TYPE, ");
			sb.append("    'E_RES'                                 AS ENTITY  ");
			sb.append("  FROM V_PNR_PAX_TRANSACTION V,  ");
			sb.append("    T_AGENT G,  ");
			sb.append("    T_ACTUAL_PAYMENT_METHOD actual_p,  ");
			sb.append("    t_pax_transaction pt  ");
			sb.append("  WHERE V.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("  AND V.AGENT_CODE                = G.AGENT_CODE(+)  ");
			sb.append("  AND actual_p.payment_mode_id (+)= pt.payment_mode_id  ");
			sb.append("  AND PT.TXN_ID                   = V.TXN_ID  ");
			sb.append("  AND (V.PAYMENT_CARRIER_CODE     = '" + thisAirlineCode + "'  ");
			sb.append("  OR V.PAYMENT_CARRIER_CODE      IS NULL)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("  UNION ALL  ");
			sb.append("  SELECT PAX.PNR,  ");
			sb.append("    PAX.TITLE,  ");
			sb.append("    PAX.FIRST_NAME,  ");
			sb.append("    PAX.LAST_NAME,  ");
			sb.append("    PAXTXN.AMOUNT AS AMOUNT,  ");
			sb.append("    PAXTXN.TXN_TIMESTAMP AS TNX_DATE,  ");
			sb.append("    PAXTXN.NOMINAL_CODE,  ");
			sb.append("    PAXTXN.SALES_CHANNEL_CODE,  ");
			sb.append("    PAXTXN.USER_ID,  ");
			sb.append("    '' AS ACTUAL_PAY ,  ");
			sb.append("    '' AS PAY_REF  , 'P' AS PRODUCT_TYPE, ");
			sb.append("    'E_RES'         AS ENTITY  ");
			sb.append("  FROM T_PNR_PASSENGER PAX,  ");
			sb.append("    T_PAX_EXT_CARRIER_TRANSACTIONS PAXTXN,  ");
			sb.append("    T_AGENT G  ");
			sb.append("  WHERE PAX.PNR_PAX_ID = PAXTXN.PNR_PAX_ID  ");
			sb.append("  AND PAXTXN.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("  AND PAXTXN.AGENT_CODE           = G.AGENT_CODE(+)  ");
			sb.append("  AND PAXTXN.NOMINAL_CODE        IN (" + singleDataValuePayments + ")  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");

			sb.append(" UNION ALL ");
			sb.append("  SELECT  AGENTTNX.PNR AS PNR , ");
			sb.append("  ''  AS TITLE, ");
			sb.append("  ''  AS FIRST_NAME, ");
			sb.append("  '' AS LAST_NAME, ");
			sb.append("  (AGENTTNX.AMOUNT * -1) AS AMOUNT, ");
			sb.append("  AGENTTNX.TNX_DATE, ");
			sb.append("  AGENTTNX.NOMINAL_CODE, ");
			sb.append("  AGENTTNX.SALES_CHANNEL_CODE, ");
			sb.append("  AGENTTNX.USER_ID, ");
			sb.append("  ''  AS ACTUAL_PAY , ");			
			sb.append("  AGENTTNX.EXT_REFERENCE AS PAY_REF , AGENTTNX.PRODUCT_TYPE, ");
			sb.append("  PROD.ENTITY_CODE AS ENTITY ");
			sb.append("  FROM T_AGENT_TRANSACTION AGENTTNX,  T_PRODUCT PROD ");
			sb.append("  WHERE AGENTTNX.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("  AND AGENTTNX.PRODUCT_TYPE <> 'P'  ");
			sb.append("  AND AGENTTNX.PRODUCT_TYPE = PROD.PRODUCT_TYPE ");

			sb.append("  ) VPAXTXN,  ");
			sb.append("  T_USER U  ");
			sb.append("WHERE VPAXTXN.USER_ID     = U.USER_ID(+)  ");
			sb.append("AND VPAXTXN.NOMINAL_CODE IN (" + singleDataValuePayments + ")  ");
			sb.append(" AND VPAXTXN.ENTITY  ='"+reportsSearchCriteria.getEntity()+"' ");
			sb.append("ORDER BY 1");
		}

		else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_DETAIL)) {
			sb.append("SELECT VPAXTXN.PNR,  ");
			sb.append("  (VPAXTXN.TITLE  ");
			sb.append("  || ' '  ");
			sb.append("  || VPAXTXN.PNR_FIRST_NAME  ");
			sb.append("  || ' '  ");
			sb.append("  || VPAXTXN.PNR_LAST_NAME) AS PNR_PAX_NAME,  ");
			sb.append("  DECODE(SALES_CHANNEL_CODE, 4, 'Web',20,'IOS',21,'ANDROID', DISPLAY_NAME) DISPLAY_NAME,  ");
			sb.append("  TO_CHAR(VPAXTXN.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,  ");
			sb.append("  (VPAXTXN.AMOUNT * -1)                   AS AMOUNT ,  ");
			sb.append("  VPAXTXN.ACTUAL_PAY                      AS ACTUAL_PAY,  ");
			sb.append("  VPAXTXN.PAY_REF                         AS PAY_REF ,  ");
			sb.append("  ' '                         AS PRODUCT_TYPE,  ");
			sb.append("  VPAXTXN.ENTITY AS ENTITY  ");
			sb.append("FROM  ");
			sb.append("  (SELECT V.PNR,  ");
			sb.append("    V.TITLE,  ");
			sb.append("    V.PNR_FIRST_NAME,  ");
			sb.append("    V.PNR_LAST_NAME,  ");
			sb.append("    V.AMOUNT,  ");
			sb.append("    V.TNX_DATE,  ");
			sb.append("    V.NOMINAL_CODE,  ");
			sb.append("    V.SALES_CHANNEL_CODE,  ");
			sb.append("    V.USER_ID,  ");
			sb.append("    actual_p.payment_mode AS ACTUAL_PAY ,  ");
			sb.append("    pt.EXT_REFERENCE      AS PAY_REF,  ");
			sb.append("    'E_RES'               AS ENTITY ");
			sb.append("  FROM V_PNR_PAX_TRANSACTION V,  ");
			sb.append("    T_AGENT G,  ");
			sb.append("    T_ACTUAL_PAYMENT_METHOD actual_p,  ");
			sb.append("    t_pax_transaction pt  ");
			sb.append("  WHERE V.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("  AND V.AGENT_CODE                = G.AGENT_CODE(+)  ");
			sb.append("  AND actual_p.payment_mode_id (+)= pt.payment_mode_id  ");
			sb.append("  AND pt.txn_id                   = v.txn_id  ");
			sb.append("  AND (V.PAYMENT_CARRIER_CODE = '" + thisAirlineCode + "'  ");
			sb.append("  OR V.PAYMENT_CARRIER_CODE  IS NULL)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("  UNION ALL  ");
			sb.append("  SELECT PAX.PNR,  ");
			sb.append("    PAX.TITLE,  ");
			sb.append("    PAX.FIRST_NAME,  ");
			sb.append("    PAX.LAST_NAME,  ");
			sb.append("    PAXTXN.AMOUNT,  ");
			sb.append("    PAXTXN.TXN_TIMESTAMP,  ");
			sb.append("    PAXTXN.NOMINAL_CODE,  ");
			sb.append("    PAXTXN.SALES_CHANNEL_CODE,  ");
			sb.append("    PAXTXN.USER_ID,  ");
			sb.append("    '' AS ACTUAL_PAY ,  ");
			sb.append("    '' AS PAY_REF,  ");
			sb.append("    'E_RES'               AS ENTITY ");
			sb.append("  FROM T_PNR_PASSENGER PAX,  ");
			sb.append("    T_PAX_EXT_CARRIER_TRANSACTIONS PAXTXN,  ");
			sb.append("    T_AGENT G  ");
			sb.append("  WHERE PAX.PNR_PAX_ID = PAXTXN.PNR_PAX_ID  ");
			sb.append("  AND PAXTXN.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("  AND PAXTXN.AGENT_CODE           = G.AGENT_CODE(+)  ");
			sb.append("  AND PAXTXN.NOMINAL_CODE        IN (" + singleDataValuePayments + ")  ");
			sb.append("  AND (G.IS_LCC_INTEGRATION_AGENT = 'N'  ");
			sb.append("  OR G.IS_LCC_INTEGRATION_AGENT  IS NULL)  ");
			sb.append("  ) VPAXTXN,  ");
			sb.append("  T_USER U  ");
			sb.append("WHERE VPAXTXN.USER_ID     = U.USER_ID(+)  ");
			sb.append("AND VPAXTXN.NOMINAL_CODE IN (" + singleDataValuePayments + ")  ");
			sb.append("  AND  VPAXTXN.ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");
			sb.append(" ORDER BY 1");
		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_CREDIT_DETAIL)) {
			sb.append("SELECT *  ");
			sb.append("FROM  ");
			sb.append("  (SELECT *  ");
			sb.append("  FROM  ");
			sb.append("    (SELECT vpaxtxn.pnr,  ");
			sb.append("      (rCon.c_first_name  ");
			sb.append("      || ' '  ");
			sb.append("      || rCon.c_last_name) AS pnr_pax_name,  ");
			sb.append("      DECODE(vpaxtxn.sales_channel_code,4,'Web',20,'IOS',21,'ANDROID',display_name)display_name,  ");
			sb.append("      TO_CHAR ( vpaxtxn.tnx_date, 'DD/mm/YYYY') AS tnx_date,  ");
			sb.append("      NVL(AID_CCCOMPNAY,'N/A')AID_CCCOMPNAY,  ");
			sb.append("      pg.provier_name  ");
			sb.append("      || '-'  ");
			sb.append("      || pg.module_code pg,  ");
			sb.append("      SUM((vpaxtxn.amount * -1)) AS amount ,  ");
			sb.append("      actual_p.payment_mode      AS ACTUAL_PAY ,  ");
			sb.append("      pt.EXT_REFERENCE           AS PAY_REF,  ");
			sb.append("      'E_RES' AS ENTITY     ");
			sb.append("    FROM v_pnr_pax_transaction vpaxtxn,  ");
			sb.append("      t_user u,  ");
			sb.append("      t_reservation r,  ");
			sb.append("      t_reservation_contact rCon,  ");
			sb.append("      t_ccard_payment_status cc,  ");
			sb.append("      t_res_pcd pcd,  ");
			sb.append("      t_payment_gateway_currency pgc,  ");
			sb.append("      T_PAYMENT_GATEWAY pg,  ");
			sb.append("      T_AGENT G ,  ");
			sb.append("      T_ACTUAL_PAYMENT_METHOD actual_p,  ");
			sb.append("      t_pax_transaction pt  ");
			sb.append("    WHERE pg.payment_gateway_id          =pgc.payment_gateway_id  ");
			sb.append("    AND pgc.payment_gateway_currency_code=cc.gateway_name  ");
			sb.append("    AND vpaxtxn.pnr                      =r.pnr  ");
			sb.append("    AND rCon.pnr                         =r.pnr  ");
			sb.append("    AND vpaxtxn.user_id                  =u.user_id(+)  ");
			sb.append("    AND actual_p.payment_mode_id (+)     = pt.payment_mode_id  ");
			sb.append("    AND pt.txn_id                        = vpaxtxn.txn_id  ");
			sb.append("    AND pcd.transaction_ref_no           =CC.transaction_ref_no(+)  ");
			sb.append("    AND vpaxtxn.pnr_pax_id               =pcd.pnr_pax_id (+)  ");
			sb.append("    AND vpaxtxn.txn_id                   =pcd.txn_id(+)  ");
			sb.append("    AND vpaxtxn.tnx_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("    AND vpaxtxn.nominal_code       IN (" + singleDataValuePayments + ")  ");
			sb.append("    AND ((RESULT_CODE               =0  ");
			sb.append("    AND TRANSACTION_RESULT_CODE     =1)  ");
			sb.append("    OR (RESULT_CODE                IS NULL  ");
			sb.append("    AND TRANSACTION_RESULT_CODE    IS NULL)  ");
			sb.append("    OR ( CC.TPT_ID IN (SELECT TMPCC.TPT_ID FROM T_CCARD_PAYMENT_STATUS TMPCC ");
			sb.append("						  WHERE cc.tpt_id = tmpcc.tpt_id ");
			sb.append("						  AND TMPCC.RESULT_CODE = 0 ");
			sb.append("						  and TMPCC.TRANSACTION_RESULT_CODE = 1 ");
			sb.append("						  AND TMPCC.SERVICE_TYPE = 'ccquery'))) ");
			sb.append("    AND VPAXTXN.AGENT_CODE          = G.AGENT_CODE(+)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("    GROUP BY vpaxtxn.pnr,  ");
			sb.append("      (rCon.c_first_name  ");
			sb.append("      || ' '  ");
			sb.append("      || rCon.c_last_name) ,  ");
			sb.append("      DECODE(vpaxtxn.sales_channel_code,4,'Web',20,'IOS',21,'ANDROID',display_name),  ");
			sb.append("      TO_CHAR (vpaxtxn.tnx_date, 'DD/mm/YYYY') ,  ");
			sb.append("      NVL(AID_CCCOMPNAY,'N/A'),  ");
			sb.append("      pg.provier_name  ");
			sb.append("      || '-'  ");
			sb.append("      || pg.module_code,  ");
			sb.append("      actual_p.payment_mode,  ");
			sb.append("      pt.EXT_REFERENCE  ");
			sb.append("    )  ");
			if (StringUtils.isNotEmpty(reportsSearchCriteria.getPaymentGW())
					&& !"null".equals(reportsSearchCriteria.getPaymentGW().trim())) {
				sb.append("  WHERE pg = '" + reportsSearchCriteria.getPaymentGW() + "'");
				sb.append("  AND ENTITY = '"+reportsSearchCriteria.getEntity()+"'   ");
			} else {
				sb.append("  WHERE ENTITY = '"+reportsSearchCriteria.getEntity()+"'   ");
			}
			
			sb.append("  )  ");
			sb.append("UNION ALL  ");
			sb.append("SELECT *  ");
			sb.append("FROM  ");
			sb.append("  (SELECT *  ");
			sb.append("  FROM  ");
			sb.append("    (SELECT R.PNR,  ");
			sb.append("      (RCON.C_FIRST_NAME  ");
			sb.append("      || ' '  ");
			sb.append("      || RCON.C_LAST_NAME) AS PNR_PAX_NAME,  ");
			sb.append("      DECODE(PAXEXTTXN.SALES_CHANNEL_CODE,4,'Web',20,'IOS',21,'ANDROID',DISPLAY_NAME)DISPLAY_NAME,  ");
			sb.append("      TO_CHAR ( PAXEXTTXN.TXN_TIMESTAMP, 'DD/mm/YYYY') AS TNX_DATE,  ");
			sb.append("      NVL(AID_CCCOMPNAY,'N/A')AID_CCCOMPNAY,  ");
			sb.append("      PG.PROVIER_NAME  ");
			sb.append("      || '-'  ");
			sb.append("      || PG.MODULE_CODE PG,  ");
			sb.append("      SUM(PAXEXTTXN.AMOUNT * -1) AS AMOUNT ,  ");
			sb.append("      ''                           AS ACTUAL_PAY ,  ");
			sb.append("      ''                           AS PAY_REF,  ");
			sb.append("      'E_RES' AS ENTITY     ");
			sb.append("    FROM T_PAX_EXT_CARRIER_TRANSACTIONS PAXEXTTXN,  ");
			sb.append("      T_PNR_PASSENGER PNRPAX,  ");
			sb.append("      T_USER U,  ");
			sb.append("      T_RESERVATION R,  ");
			sb.append("      T_RESERVATION_CONTACT RCON,  ");
			sb.append("      T_CCARD_PAYMENT_STATUS CC,  ");
			sb.append("      T_RES_PCD PCD,  ");
			sb.append("      T_PAYMENT_GATEWAY_CURRENCY PGC,  ");
			sb.append("      T_PAYMENT_GATEWAY PG,  ");
			sb.append("      T_AGENT G  ");
			sb.append("    WHERE PG.PAYMENT_GATEWAY_ID           = PGC.PAYMENT_GATEWAY_ID  ");
			sb.append("    AND PGC.PAYMENT_GATEWAY_CURRENCY_CODE = CC.GATEWAY_NAME  ");
			sb.append("    AND PNRPAX.PNR_PAX_ID                 = PAXEXTTXN.PNR_PAX_ID  ");
			sb.append("    AND PNRPAX.PNR                        = R.PNR  ");
			sb.append("    AND RCON.PNR                          = R.PNR  ");
			sb.append("    AND PAXEXTTXN.USER_ID                 = U.USER_ID(+)  ");
			sb.append("    AND PCD.TRANSACTION_REF_NO            = CC.TRANSACTION_REF_NO(+)  ");
			sb.append("    AND PAXEXTTXN.PNR_PAX_ID              = PCD.PNR_PAX_ID (+)  ");
			sb.append("    AND PAXEXTTXN.PAX_EXT_CARRIER_TXN_ID  = PCD.EXT_TXN_ID(+)  ");
			sb.append("    AND PAXEXTTXN.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("    AND PAXEXTTXN.NOMINAL_CODE     IN (" + singleDataValuePayments + ")  ");
			sb.append("    AND ((RESULT_CODE               =0  ");
			sb.append("    AND TRANSACTION_RESULT_CODE     =1)  ");
			sb.append("    OR (RESULT_CODE                IS NULL  ");
			sb.append("    AND TRANSACTION_RESULT_CODE    IS NULL) ");
			sb.append("    OR ( CC.TPT_ID IN (SELECT TMPCC.TPT_ID FROM T_CCARD_PAYMENT_STATUS TMPCC ");
			sb.append("						  WHERE cc.tpt_id = tmpcc.tpt_id ");
			sb.append("						  AND TMPCC.RESULT_CODE = 0 ");
			sb.append("						  and TMPCC.TRANSACTION_RESULT_CODE = 1 ");
			sb.append("						  AND TMPCC.SERVICE_TYPE = 'ccquery'))) ");
			sb.append("    AND PAXEXTTXN.AGENT_CODE        = G.AGENT_CODE(+)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("    GROUP BY R.PNR,  ");
			sb.append("      (RCON.C_FIRST_NAME  ");
			sb.append("      || ' '  ");
			sb.append("      || RCON.C_LAST_NAME),  ");
			sb.append("      DECODE(PAXEXTTXN.SALES_CHANNEL_CODE,4,'Web',20,'IOS',21,'ANDROID',DISPLAY_NAME),  ");
			sb.append("      TO_CHAR ( PAXEXTTXN.TXN_TIMESTAMP, 'DD/mm/YYYY'),  ");
			sb.append("      NVL(AID_CCCOMPNAY,'N/A'),  ");
			sb.append("      PG.PROVIER_NAME  ");
			sb.append("      || '-'  ");
			sb.append("      || PG.MODULE_CODE,  ");
			sb.append("      '',  ");
			sb.append("      ''  ");
			sb.append("    )  ");
			if (StringUtils.isNotEmpty(reportsSearchCriteria.getPaymentGW())
					&& !"null".equals(reportsSearchCriteria.getPaymentGW().trim())) {
				sb.append("  WHERE pg = '" + reportsSearchCriteria.getPaymentGW() + "'");
				sb.append("  AND ENTITY = '"+reportsSearchCriteria.getEntity()+"'   ");
			} else {
				sb.append("  WHERE ENTITY = '"+reportsSearchCriteria.getEntity()+"'   ");			
			}
			sb.append("  )  ");
			sb.append("ORDER BY 1 ");

		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_SUMMARY)) {
			sb.append(
					"SELECT DECODE(PAY,21,'BSP EXTERNAL', 36, 'BSP', 19, 'On Account', 18, 'Cash', 15, 'Visa', 16, 'Master', 28, 'Amex', 17, 'Dinners', 30, 'Generic', 32, 'CMI', 41, 'Generic debit', 46, 'Loyalty Payment', 48, 'Voucher Payment') AS PAYMENT_MODE,  ");
			sb.append("  DECODE(P.USER_ID, 'AAWEBUSER', 'WEB', U.DISPLAY_NAME) DISPLAY_NAME,  ");
			sb.append("  (-1 * AMT) AMT,  ");
			sb.append("  PAY,  ");
			sb.append("  U.USER_ID,  ");
			sb.append("  GW, ENTITY  ");
			sb.append("FROM  ");
			sb.append(
					"  (SELECT DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE) PAY,  ");
			sb.append("    USER_ID,  ");
			sb.append("    SUM(AMT) AMT,  ");
			sb.append("    GW, ENTITY  ");
			sb.append("  FROM  ");
			sb.append("    (SELECT NOMINAL_CODE,  ");
			sb.append("      NVL(USER_ID, 'AAWEBUSER') USER_ID,  ");
			sb.append("      SUM(AMOUNT) AMT,  ");
			sb.append("      GW, ENTITY  ");
			sb.append("    FROM (  ");
			sb.append("      (SELECT NOMINAL_CODE,  ");
			sb.append("        USER_ID,  ");
			sb.append("        AMOUNT,  ");
			sb.append("        (SELECT DISTINCT PG.PROVIER_NAME  ");
			sb.append("          || '-'  ");
			sb.append("          || PG.MODULE_CODE  ");
			sb.append("        FROM T_PAYMENT_GATEWAY PG  ");
			sb.append("        WHERE PG.PAYMENT_GATEWAY_ID IN  ");
			sb.append("          (SELECT PGC.PAYMENT_GATEWAY_ID  ");
			sb.append("          FROM T_PAYMENT_GATEWAY_CURRENCY PGC  ");
			sb.append("          WHERE PGC.PAYMENT_GATEWAY_CURRENCY_CODE IN  ");
			sb.append("            (SELECT GATEWAY_NAME  ");
			sb.append("            FROM T_CCARD_PAYMENT_STATUS CC  ");
			sb.append("            WHERE CC.TRANSACTION_REF_NO IN  ");
			sb.append("              (SELECT PCD.TRANSACTION_REF_NO  ");
			sb.append("              FROM T_RES_PCD PCD  ");
			sb.append("              WHERE PCD.TXN_ID = VPAXTXN.TXN_ID  ");
			sb.append("              )  ");
			sb.append("            )  ");
			sb.append("          )  ");
			sb.append("        ) GW, 'E_RES' AS ENTITY   ");
			sb.append("      FROM T_PAX_TRANSACTION VPAXTXN,  ");
			sb.append("        T_AGENT G  ");
			sb.append("      WHERE VPAXTXN.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("      AND VPAXTXN.AGENT_CODE          = G.AGENT_CODE(+)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("      AND VPAXTXN.NOMINAL_CODE       IN (" + singleDataValuePayments + ")  ");
			sb.append("      )  ");

			// External Products
			if (filteredNominalCodes != null && !filteredNominalCodes.isEmpty()) {
				sb.append("     UNION ALL ");
				sb.append("      SELECT AGENTTNX.NOMINAL_CODE,AGENTTNX.USER_ID , ");
				sb.append("      (AGENTTNX.AMOUNT*-1)  AS AMOUNT ,  ");
				sb.append("      null as GW , PROD.ENTITY_CODE AS ENTITY ");
				sb.append("      FROM T_AGENT_TRANSACTION AGENTTNX , T_PRODUCT PROD");
				sb.append("    WHERE AGENTTNX.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("      AND AGENTTNX.NOMINAL_CODE     IN (" + filteredNominalCodes + ")  ");
				sb.append("     AND AGENTTNX.PRODUCT_TYPE <> 'P' ");
				sb.append("     AND AGENTTNX.PRODUCT_TYPE = PROD.PRODUCT_TYPE ");
			}

			sb.append("    UNION ALL  ");
			sb.append("      (SELECT NOMINAL_CODE,  ");
			sb.append("        USER_ID,  ");
			sb.append("        AMOUNT AS AMOUNT,  ");
			sb.append("        (SELECT DISTINCT PG.PROVIER_NAME  ");
			sb.append("          || '-'  ");
			sb.append("          || PG.MODULE_CODE  ");
			sb.append("        FROM T_PAYMENT_GATEWAY PG  ");
			sb.append("        WHERE PG.PAYMENT_GATEWAY_ID IN  ");
			sb.append("          (SELECT PGC.PAYMENT_GATEWAY_ID  ");
			sb.append("          FROM T_PAYMENT_GATEWAY_CURRENCY PGC  ");
			sb.append("          WHERE PGC.PAYMENT_GATEWAY_CURRENCY_CODE IN  ");
			sb.append("            (SELECT GATEWAY_NAME  ");
			sb.append("            FROM T_CCARD_PAYMENT_STATUS CC  ");
			sb.append("            WHERE CC.TRANSACTION_REF_NO IN  ");
			sb.append("              (SELECT PCD.TRANSACTION_REF_NO  ");
			sb.append("              FROM T_RES_PCD PCD  ");
			sb.append("              WHERE PCD.EXT_TXN_ID = PAXEXTTNX.PAX_EXT_CARRIER_TXN_ID  ");
			sb.append("              )  ");
			sb.append("            )  ");
			sb.append("          )  ");
			sb.append("        ) GW, 'E_RES' AS ENTITY   ");
			sb.append("      FROM T_PAX_EXT_CARRIER_TRANSACTIONS PAXEXTTNX,  ");
			sb.append("        T_AGENT G  ");
			sb.append("      WHERE PAXEXTTNX.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("      AND PAXEXTTNX.AGENT_CODE        = G.AGENT_CODE(+)  ");
			// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
			// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
			sb.append("      AND PAXEXTTNX.NOMINAL_CODE     IN (" + singleDataValuePayments + ")  ");
			sb.append("      ))  ");
			sb.append("    GROUP BY NOMINAL_CODE,  ");
			sb.append("      NVL(USER_ID, 'AAWEBUSER'),  ");
			sb.append("      GW , ENTITY ");
			sb.append("    )  ");
			sb.append(
					"  GROUP BY DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE),  ");
			sb.append("    USER_ID,  ");
			sb.append("    GW , ENTITY  ");
			sb.append("  ) P,  ");
			sb.append("  T_USER U  ");
			sb.append(" WHERE P.USER_ID = U.USER_ID  ");
			sb.append(" AND ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");
			sb.append(" ORDER BY 2,  ");
			sb.append("  1");

		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_NAME_SUMMARY)) {
			sb.append(
					"SELECT DECODE(PAY,21,'BSP EXTERNAL', 36, 'BSP', 19, 'On Account', 18, 'Cash', 15, 'Visa', 16, 'Master', 28, 'Amex', 17, 'Diners', 30, 'Generic', 32, 'CMI', 41, 'Generic debit', 46, 'Loyalty Payment') AS PAYMENT_MODE,  ");
			sb.append("  AGENT,  ");
			sb.append("  DISPLAY_NAME,  ");
			sb.append("  -1 * AMT AMT,  ");
			sb.append("  PAY,  ");
			sb.append("  USER_ID , ENTITY  ");
			sb.append("FROM  ");
			sb.append(
					"  (SELECT DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE) PAY,  ");
			sb.append("    AGENT,  ");
			sb.append("    DISPLAY_NAME,  ");
			sb.append("    SUM(AMT) AMT,  ");
			sb.append("    USER_ID , ENTITY ");
			sb.append("  FROM  ");
			sb.append("    (SELECT VPAXTXN.NOMINAL_CODE,  ");
			sb.append("      DECODE(VPAXTXN.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', A.AGENT_NAME) AGENT,  ");
			sb.append("      DECODE(VPAXTXN.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.DISPLAY_NAME) DISPLAY_NAME,  ");
			sb.append("      DECODE(VPAXTXN.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.USER_ID) USER_ID,  ");
			sb.append("      SUM(VPAXTXN.AMOUNT) AMT,  'E_RES' AS ENTITY ");
			sb.append("    FROM T_PAX_TRANSACTION VPAXTXN,  ");
			sb.append("      T_AGENT A,  ");
			sb.append("      T_USER U  ");
			sb.append("    WHERE VPAXTXN.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("    AND VPAXTXN.NOMINAL_CODE      IN (" + singleDataValuePayments + ")  ");
			sb.append("    AND VPAXTXN.AGENT_CODE         = A.AGENT_CODE(+)  ");
			sb.append("    AND VPAXTXN.USER_ID            = U.USER_ID(+)  ");
			// sb.append(" AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
			sb.append("    GROUP BY NOMINAL_CODE,  ");
			sb.append("      DECODE(VPAXTXN.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', AGENT_NAME),  ");
			sb.append("      DECODE(VPAXTXN.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.DISPLAY_NAME),  ");
			sb.append("      DECODE(VPAXTXN.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.USER_ID)  ");
			sb.append("    UNION ALL  ");
			sb.append("    SELECT P.NOMINAL_CODE,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', A.AGENT_NAME) AGENT,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.DISPLAY_NAME) DISPLAY_NAME,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.USER_ID) USER_ID,  ");
			sb.append("      SUM(P.AMOUNT) AMT,  'E_RES' AS ENTITY ");
			sb.append("    FROM T_PAX_EXT_CARRIER_TRANSACTIONS P,  ");
			sb.append("      T_AGENT A,  ");
			sb.append("      T_USER U  ");
			sb.append("    WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("    AND P.NOMINAL_CODE            IN (" + singleDataValuePayments + ")  ");
			sb.append("    AND P.AGENT_CODE               = A.AGENT_CODE(+)  ");
			sb.append("    AND P.USER_ID                  = U.USER_ID(+)  ");
			// sb.append(" AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
			sb.append("    GROUP BY P.NOMINAL_CODE,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', A.AGENT_NAME),  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.DISPLAY_NAME),  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB',20,'IOS',21,'ANDROID', U.USER_ID)  ");

			sb.append("    UNION ALL  ");
			sb.append("    SELECT P.NOMINAL_CODE,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB', 20,'IOS',21,'ANDROID',A.AGENT_NAME) AGENT,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB', 20,'IOS',21,'ANDROID',U.DISPLAY_NAME) DISPLAY_NAME,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB', 20,'IOS',21,'ANDROID',U.USER_ID) USER_ID,  ");
			sb.append("      SUM(P.AMOUNT * -1) AMT, PROD.ENTITY_CODE AS ENTITY ");
			sb.append("    FROM T_AGENT_TRANSACTION P,  ");
			sb.append("      T_AGENT A,  ");
			sb.append("      T_USER U  ,  T_PRODUCT PROD  ");
			sb.append("    WHERE P.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
			sb.append("    AND P.NOMINAL_CODE            IN (" + singleDataValuePayments + ")  ");
			sb.append("    AND P.AGENT_CODE               = A.AGENT_CODE(+)  ");
			sb.append("    AND P.USER_ID                  = U.USER_ID(+)  ");
			sb.append("    AND P.PRODUCT_TYPE <> 'P' ");
			sb.append("    AND P.PRODUCT_TYPE = PROD.PRODUCT_TYPE  ");
			sb.append("    GROUP BY P.NOMINAL_CODE,  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB', 20,'IOS',21,'ANDROID',A.AGENT_NAME),  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB', 20,'IOS',21,'ANDROID',U.DISPLAY_NAME),  ");
			sb.append("      DECODE(P.SALES_CHANNEL_CODE, 4, 'WEB', 20,'IOS',21,'ANDROID',U.USER_ID), PROD.ENTITY_CODE  ");

			sb.append("    )  ");
			sb.append(
					"  GROUP BY DECODE(NOMINAL_CODE, 25, 19, 22, 15, 23, 16, 24, 17, 26, 18, 29, 28, 6, 5, 31, 30, 33, 32, 37, 36, 47, 46, NOMINAL_CODE),  ");
			sb.append("    AGENT,  ");
			sb.append("    DISPLAY_NAME,  ");
			sb.append("    USER_ID, ENTITY  ");
			sb.append("  ) P WHERE ENTITY = '"+reportsSearchCriteria.getEntity()+"' ");
			sb.append(" ORDER BY 2,  ");
			sb.append("  3,  ");
			sb.append("  1");

		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_EXTERNAL_SUMMARY)) {
			sb.append(" SELECT ag.agent_name, ");
			sb.append("    ROUND(SUM(agtran.amount),2) Amount ");
			sb.append(" FROM t_agent_transaction agtran ,t_agent ag ");
			sb.append(" WHERE  agtran.tnx_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy HH24:mi:ss')  AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy HH24:mi:ss') ");
			sb.append(" 	and nominal_code in (10,11,19,20) ");
			// sb.append(" and SALES_CHANNEL_CODE = 11 "); un comment withe right sales channel code when the holidays
			// send the correct data
			sb.append("       and ag.agent_code = agtran.agent_code ");
			sb.append(" group by ag.agent_name ");
			sb.append(" order by ag.agent_name ");

		} else if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MODE_OF_PAYMENT_CURRENCY_DETAIL)) {

			if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewModeOfPaymentDetailsByCurrencyQuery(reportsSearchCriteria,
						singleDataValuePayments));
			} else {
				sb.append("SELECT *  ");
				sb.append("FROM  ");
				sb.append("  (SELECT PAX.PNR,  ");
				sb.append("    (PAX.TITLE  ");
				sb.append("    || ' '  ");
				sb.append("    || PAX.FIRST_NAME  ");
				sb.append("    || ' '  ");
				sb.append("    || PAX.LAST_NAME) AS PNR_PAX_NAME,  ");
				sb.append("    DECODE(PAXTNX.SALES_CHANNEL_CODE, 4, 'Web', DISPLAY_NAME) DISPLAY_NAME,  ");
				sb.append("    TO_CHAR(PAXTNX.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,  ");
				sb.append("    (-1 * PAXTNX.AMOUNT_PAYCUR)         AS AMOUNT ,  ");
				sb.append("    actual_p.payment_mode                   AS ACTUAL_PAY ,  ");
				sb.append("    PAXTNX.EXT_REFERENCE                        AS PAY_REF  , ");
				sb.append("    'P'     AS PRODUCT_TYPE,  'E_RES' AS ENTITY  ");
				sb.append("  FROM T_PNR_PASSENGER PAX,  ");
				sb.append("    T_USER U,  ");
				sb.append("    T_AGENT G ,  ");
				sb.append("    T_ACTUAL_PAYMENT_METHOD actual_p,  ");
				sb.append("    T_PAX_TRANSACTION PAXTNX  ");
				sb.append("  WHERE PAXTNX.USER_ID             = U.USER_ID(+)  ");
				sb.append("  AND PAXTNX.AGENT_CODE          = G.AGENT_CODE(+)  ");
				sb.append("  AND actual_p.payment_mode_id (+)= PAXTNX.payment_mode_id  ");
				// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("  AND PAXTNX.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("  AND PAXTNX.PNR_PAX_ID  = PAX.PNR_PAX_ID ");
				sb.append("  AND PAXTNX.NOMINAL_CODE      IN (" + singleDataValuePayments + ")  ");
				sb.append("  AND PAXTNX.PAYMENT_CURRENCY_CODE = '" + reportsSearchCriteria.getPaymentCode() + "'  ");
				sb.append("  UNION ALL  ");
				sb.append("  SELECT PAX.PNR,  ");
				sb.append("    (PAX.TITLE  ");
				sb.append("    || ' '  ");
				sb.append("    || PAX.FIRST_NAME  ");
				sb.append("    || ' '  ");
				sb.append("    || PAX.LAST_NAME) AS PNR_PAX_NAME,  ");
				sb.append("    DECODE(SALES_CHANNEL_CODE, 4, 'Web', DISPLAY_NAME) DISPLAY_NAME,  ");
				sb.append("    TO_CHAR(PAXTXN.TXN_TIMESTAMP, 'DD/mm/YYYY') AS TNX_DATE,  ");
				sb.append("    (-1 * PAXTXN.AMOUNT_PAYCUR)                 AS AMOUNT ,  ");
				sb.append("    ''                                          AS ACTUAL_PAY ,  ");
				sb.append("    ''                                          AS PAY_REF , ");
				sb.append("    'P'     AS PRODUCT_TYPE, 'E_RES' AS ENTITY  ");
				sb.append("  FROM T_PNR_PASSENGER PAX,  ");
				sb.append("    T_PAX_EXT_CARRIER_TRANSACTIONS PAXTXN,  ");
				sb.append("    T_USER U,  ");
				sb.append("    T_AGENT G  ");
				sb.append("  WHERE PAXTXN.PNR_PAX_ID         = PAX.PNR_PAX_ID  ");
				sb.append("  AND PAXTXN.USER_ID              = U.USER_ID(+)  ");
				sb.append("  AND PAXTXN.AGENT_CODE           = G.AGENT_CODE(+)  ");
				// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("  AND PAXTXN.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("  AND PAXTXN.NOMINAL_CODE IN (" + singleDataValuePayments + ")  ");
				sb.append("  AND PAXTXN.PAYCUR_CODE   = '" + reportsSearchCriteria.getPaymentCode() + "'  ");

				sb.append("  UNION ALL  ");
				sb.append("  SELECT AGENTTRN.PNR,  ");
				sb.append("   ' ' AS PNR_PAX_NAME,  ");
				sb.append("    DECODE(SALES_CHANNEL_CODE, 4, 'Web',20,'IOS',21,'ANDROID', DISPLAY_NAME) DISPLAY_NAME,  ");
				sb.append("    TO_CHAR(AGENTTRN.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,  ");
				sb.append("   AGENTTRN.AMOUNT                 AS AMOUNT ,  ");
				sb.append("    ''                                          AS ACTUAL_PAY ,  ");
				sb.append("    AGENTTRN.EXT_REFERENCE     AS PAY_REF , ");
				sb.append("    AGENTTRN.PRODUCT_TYPE     AS PRODUCT_TYPE, PROD.ENTITY_CODE    ");
				sb.append("  FROM T_AGENT_TRANSACTION AGENTTRN,  ");
				sb.append("    T_USER U,  ");
				sb.append("    T_AGENT G, T_PRODUCT PROD  ");
				sb.append("  WHERE AGENTTRN.USER_ID              = U.USER_ID(+)  ");
				sb.append("  AND AGENTTRN.AGENT_CODE           = G.AGENT_CODE(+)  ");
				sb.append("  AND AGENTTRN.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("  AND AGENTTRN.NOMINAL_CODE IN (" + singleDataValuePayments + ")  ");
				sb.append("  AND AGENTTRN.CURRENCY_CODE   = '" + reportsSearchCriteria.getPaymentCode() + "'  ");
				sb.append("  AND AGENTTRN.PRODUCT_TYPE <> 'P' ");
				sb.append("  AND AGENTTRN.PRODUCT_TYPE = PROD.PRODUCT_TYPE  ");

				sb.append("  ) WHERE  ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");
				sb.append("ORDER BY 1");
			}
		} else {

			if (reportsSearchCriteria.getUserId().equalsIgnoreCase("WEB") || reportsSearchCriteria.getUserId().equalsIgnoreCase("ANDROID")
					|| reportsSearchCriteria.getUserId().equalsIgnoreCase("IOS")) {
				
				int salesChannel =-1;
				
				if (reportsSearchCriteria.getUserId().equalsIgnoreCase("WEB")) {
					salesChannel = SalesChannelsUtil.SALES_CHANNEL_WEB;
				} else if (reportsSearchCriteria.getUserId().equalsIgnoreCase("IOS")) {
					salesChannel = SalesChannelsUtil.SALES_CHANNEL_IOS;
				} else if (reportsSearchCriteria.getUserId().equalsIgnoreCase("ANDROID")) {
					salesChannel = SalesChannelsUtil.SALES_CHANNEL_ANDROID;
				}
				
				
				sb.append("SELECT V.PNR,  ");
				sb.append("  (V.TITLE  ");
				sb.append("  || ' '  ");
				sb.append("  || V.PNR_FIRST_NAME  ");
				sb.append("  || ' '  ");
				sb.append("  || V.PNR_LAST_NAME)               AS PNR_PAX_NAME,  ");
				sb.append("  TO_CHAR(V.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,  ");
				sb.append("  (V.AMOUNT * -1)                   AS AMOUNT,  ");
				sb.append("  v.ACTUAL_PAY                      AS ACTUAL_PAY ,  ");
				sb.append("  v.PAY_REF                         AS PAY_REF, ");
				sb.append("  v.ENTITY                          AS ENTITY,  ");
				sb.append("  v.PRODUCT_TYPE                    AS PRODUCT_TYPE ");
				sb.append("FROM  ");
				sb.append("  (SELECT V.PNR,  ");
				sb.append("    V.TITLE,  ");
				sb.append("    V.PNR_FIRST_NAME,  ");
				sb.append("    V.PNR_LAST_NAME,  ");
				sb.append("    V.TNX_DATE,  ");
				sb.append("    V.AMOUNT,  ");
				sb.append("    V.NOMINAL_CODE,  ");
				sb.append("    V.SALES_CHANNEL_CODE,  ");
				sb.append("    V.AGENT_CODE AGENT_CODE ,  ");
				sb.append("    actual_p.payment_mode AS ACTUAL_PAY ,  ");
				sb.append("    pt.EXT_REFERENCE      AS PAY_REF,  'P'     AS PRODUCT_TYPE, 'E_RES' AS  ENTITY   ");
				sb.append("  FROM V_PNR_PAX_TRANSACTION V ,  ");
				sb.append("    T_ACTUAL_PAYMENT_METHOD actual_p,  ");
				sb.append("    t_pax_transaction pt  ");
				sb.append("  WHERE actual_p.payment_mode_id (+)= pt.payment_mode_id  ");
				sb.append("  AND PT.TXN_ID                     = V.TXN_ID  ");
				sb.append("  UNION ALL  ");
				sb.append("  SELECT P.PNR                   AS PNR,  ");
				sb.append("    P.TITLE                      AS TITLE,  ");
				sb.append("    P.FIRST_NAME                 AS PNR_FIRST_NAME,  ");
				sb.append("    P.LAST_NAME                  AS PNR_LAST_NAME,  ");
				sb.append("    PAXEXTTXN.TXN_TIMESTAMP      AS TNX_DATE,  ");
				sb.append("    PAXEXTTXN.AMOUNT      AS AMOUNT,  ");
				sb.append("    PAXEXTTXN.NOMINAL_CODE       AS NOMINAL_CODE,  ");
				sb.append("    PAXEXTTXN.SALES_CHANNEL_CODE AS SALES_CHANNEL_CODE,  ");
				sb.append("    PAXEXTTXN.AGENT_CODE         AS AGENT_CODE ,  ");
				sb.append("    ''                           AS ACTUAL_PAY ,  ");
				sb.append("    ''                           AS PAY_REF,  'P'     AS PRODUCT_TYPE, 'E_RES' AS  ENTITY   ");
				sb.append("  FROM T_PAX_EXT_CARRIER_TRANSACTIONS PAXEXTTXN,  ");
				sb.append("    T_PNR_PASSENGER P  ");
				sb.append("  WHERE PAXEXTTXN.PNR_PAX_ID = P.PNR_PAX_ID  ");
				sb.append("  ) V,  ");
				sb.append("  T_AGENT G  ");
				sb.append("WHERE V.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("AND V.NOMINAL_CODE             IN (" + singleDataValuePayments + ")  ");
				sb.append("AND V.SALES_CHANNEL_CODE        = "+ salesChannel);
				sb.append(" AND V.AGENT_CODE                = G.AGENT_CODE(+)  ");
                                sb.append(" AND ENTITY =  '"+reportsSearchCriteria.getEntity()+"' ");
				// sb.append("AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append("OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("ORDER BY 1 ");

			} else {
				sb.append("SELECT *  ");
				sb.append("FROM  ");
				sb.append("  (SELECT PNR,  ");
				sb.append("    PNR_PAX_NAME,  ");
				sb.append("    TNX_DATE,  ");
				sb.append("    AMOUNT ,  ");
				sb.append("    ACTUAL_PAY,  ");
				sb.append("    PAY_REF  , 'P' AS PRODUCT_TYPE, ENTITY   ");
				sb.append("  FROM  ");
				sb.append("    (SELECT VPAXTXN.PNR,  ");
				sb.append("      (VPAXTXN.TITLE  ");
				sb.append("      || ' '  ");
				sb.append("      || VPAXTXN.PNR_FIRST_NAME  ");
				sb.append("      || ' '  ");
				sb.append("      || VPAXTXN.PNR_LAST_NAME)               AS PNR_PAX_NAME,  ");
				sb.append("      TO_CHAR(VPAXTXN.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,  ");
				sb.append("      (VPAXTXN.AMOUNT * -1)                   AS AMOUNT ,  ");
				sb.append("      actual_p.payment_mode                   AS ACTUAL_PAY ,  ");
				sb.append("      pt.EXT_REFERENCE                        AS PAY_REF,  ");
				sb.append("      'E_RES' AS  ENTITY ");

				if (reportsSearchCriteria.getPaymentGW() != null && !reportsSearchCriteria.getPaymentGW().trim().equals("")
						&& !reportsSearchCriteria.getPaymentGW().trim().equals("null")) {
					sb.append("     , (SELECT DISTINCT PG.PROVIER_NAME  ");
					sb.append("        || '-'  ");
					sb.append("        || PG.MODULE_CODE  ");
					sb.append("      FROM T_PAYMENT_GATEWAY PG  ");
					sb.append("      WHERE PG.PAYMENT_GATEWAY_ID IN  ");
					sb.append("        (SELECT PGC.PAYMENT_GATEWAY_ID  ");
					sb.append("        FROM T_PAYMENT_GATEWAY_CURRENCY PGC  ");
					sb.append("        WHERE PGC.PAYMENT_GATEWAY_CURRENCY_CODE IN  ");
					sb.append("          (SELECT GATEWAY_NAME  ");
					sb.append("          FROM T_CCARD_PAYMENT_STATUS CC  ");
					sb.append("          WHERE CC.TRANSACTION_REF_NO IN  ");
					sb.append("            (SELECT PCD.TRANSACTION_REF_NO  ");
					sb.append("            FROM T_RES_PCD PCD  ");
					sb.append("            WHERE PCD.TXN_ID = VPAXTXN.TXN_ID  ");
					sb.append("            )  ");
					sb.append("          )  ");
					sb.append("        )  ");
					sb.append("      ) GW  ");
				}

				sb.append("    FROM V_PNR_PAX_TRANSACTION VPAXTXN,  ");
				sb.append("      T_AGENT G ,  ");
				sb.append("      T_ACTUAL_PAYMENT_METHOD actual_p,  ");
				sb.append("      t_pax_transaction pt  ");
				sb.append("    WHERE VPAXTXN.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("    AND VPAXTXN.NOMINAL_CODE       IN (" + singleDataValuePayments + ")  ");
				sb.append("    AND actual_p.payment_mode_id (+)= pt.payment_mode_id  ");
				sb.append("    AND pt.txn_id                   = VPAXTXN.txn_id  ");
				sb.append("    AND VPAXTXN.USER_ID             = '" + reportsSearchCriteria.getUserId() + "'  ");
				sb.append("    AND VPAXTXN.AGENT_CODE          = G.AGENT_CODE(+)  ");
				// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("    )  ");
				if (reportsSearchCriteria.getPaymentGW() != null && !reportsSearchCriteria.getPaymentGW().trim().equals("")
						&& !reportsSearchCriteria.getPaymentGW().trim().equals("null")) {
					sb.append("    WHERE GW = '" + reportsSearchCriteria.getPaymentGW() + "' ");
					sb.append("    AND ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");
				} else {
					sb.append("    WHERE ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");				
				}
				sb.append("  ORDER BY 1  ");
				sb.append("  )  ");
				sb.append("UNION ALL  ");
				sb.append("SELECT *  ");
				sb.append("FROM  ");
				sb.append("  (SELECT PNR,  ");
				sb.append("    PNR_PAX_NAME,  ");
				sb.append("    TNX_DATE,  ");
				sb.append("    AMOUNT ,  ");
				sb.append("    ACTUAL_PAY ,  ");
				sb.append("    PAY_REF , 'P' AS PRODUCT_TYPE, ENTITY  ");
				sb.append("  FROM  ");
				sb.append("    (SELECT PAX.PNR,  ");
				sb.append("      (PAX.TITLE  ");
				sb.append("      || ' '  ");
				sb.append("      || PAX.FIRST_NAME  ");
				sb.append("      || ' '  ");
				sb.append("      || PAX.LAST_NAME)                           AS PNR_PAX_NAME,  ");
				sb.append("      TO_CHAR(PAXTXN.TXN_TIMESTAMP, 'DD/mm/YYYY') AS TNX_DATE,  ");
				sb.append("      (PAXTXN.AMOUNT * -1)                        AS AMOUNT ,  ");
				sb.append("      ''                                          AS ACTUAL_PAY ,  ");
				sb.append("      ''                                          AS PAY_REF,   ");
				sb.append("      'E_RES'                                     AS  ENTITY   ");
				

				if (reportsSearchCriteria.getPaymentGW() != null && !reportsSearchCriteria.getPaymentGW().trim().equals("")
						&& !reportsSearchCriteria.getPaymentGW().trim().equals("null")) {
					sb.append("     , (SELECT DISTINCT PG.PROVIER_NAME  ");
					sb.append("        || '-'  ");
					sb.append("        || PG.MODULE_CODE  ");
					sb.append("      FROM T_PAYMENT_GATEWAY PG  ");
					sb.append("      WHERE PG.PAYMENT_GATEWAY_ID IN  ");
					sb.append("        (SELECT PGC.PAYMENT_GATEWAY_ID  ");
					sb.append("        FROM T_PAYMENT_GATEWAY_CURRENCY PGC  ");
					sb.append("        WHERE PGC.PAYMENT_GATEWAY_CURRENCY_CODE IN  ");
					sb.append("          (SELECT GATEWAY_NAME  ");
					sb.append("          FROM T_CCARD_PAYMENT_STATUS CC  ");
					sb.append("          WHERE CC.TRANSACTION_REF_NO IN  ");
					sb.append("            (SELECT PCD.TRANSACTION_REF_NO  ");
					sb.append("            FROM T_RES_PCD PCD  ");
					sb.append("            WHERE PCD.EXT_TXN_ID = PAXTXN.PAX_EXT_CARRIER_TXN_ID  ");
					sb.append("            )  ");
					sb.append("          )  ");
					sb.append("        )  ");
					sb.append("      ) GW  ");
				}

				sb.append("    FROM T_PNR_PASSENGER PAX,  ");
				sb.append("      T_PAX_EXT_CARRIER_TRANSACTIONS PAXTXN,  ");
				sb.append("      T_AGENT G  ");
				sb.append("    WHERE PAXTXN.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append("    AND PAXTXN.NOMINAL_CODE        IN (" + singleDataValuePayments + ")  ");
				sb.append("    AND USER_ID                     = '" + reportsSearchCriteria.getUserId() + "'  ");
				sb.append("    AND PAXTXN.AGENT_CODE           = G.AGENT_CODE(+)  ");
				sb.append("    AND PAX.PNR_PAX_ID              = PAXTXN.PNR_PAX_ID  ");
				// sb.append(" AND (G.IS_LCC_INTEGRATION_AGENT = 'N' ");
				// sb.append(" OR G.IS_LCC_INTEGRATION_AGENT IS NULL) ");
				sb.append("    )  ");
				if (reportsSearchCriteria.getPaymentGW() != null && !reportsSearchCriteria.getPaymentGW().trim().equals("")
						&& !reportsSearchCriteria.getPaymentGW().trim().equals("null")) {
					sb.append("    WHERE GW = '" + reportsSearchCriteria.getPaymentGW() + "' ");
					sb.append("    AND ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");
				} else {
					sb.append("    WHERE ENTITY = '"+reportsSearchCriteria.getEntity()+"'  ");
				}
				sb.append("  ORDER BY 1  ");
				sb.append("  )");

				// External Amount ammendment
				sb.append(" UNION ALL ");
				sb.append("  SELECT  AGENTTNX.PNR AS PNR , ");
				sb.append("  ''  AS PNR_PAX_NAME, ");
				sb.append("  TO_CHAR(AGENTTNX.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE, ");
				sb.append("  AGENTTNX.AMOUNT AS AMOUNT, ");
				sb.append("  ''  AS ACTUAL_PAY , ");
				sb.append("  AGENTTNX.EXT_REFERENCE AS PAY_REF , AGENTTNX.PRODUCT_TYPE , PROD.ENTITY_CODE");
				sb.append("  FROM T_AGENT_TRANSACTION AGENTTNX , T_PRODUCT PROD");
				sb.append("  WHERE AGENTTNX.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "', 'dd-mon-yyyy HH24:mi:ss') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "', 'dd-mon-yyyy HH24:mi:ss')  ");
				sb.append(" AND AGENTTNX.USER_ID = '" + reportsSearchCriteria.getUserId() + "'  ");
				sb.append("    AND AGENTTNX.NOMINAL_CODE        IN (" + singleDataValuePayments + ")  ");
				sb.append("  AND AGENTTNX.PRODUCT_TYPE <> 'P'  ");
				sb.append("  AND AGENTTNX.PRODUCT_TYPE = PROD.PRODUCT_TYPE ");
				sb.append("   AND PROD.ENTITY_CODE = '"+reportsSearchCriteria.getEntity()+"'  ");
				sb.append("  ORDER BY 1  ");

			}
		}
		return sb.toString();
	}

	public String getInvoiceSummaryQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		StringBuilder sb = new StringBuilder();
		String startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
		String endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  23:59:59";
		String endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
		// String invoiceNum = reportsSearchCriteria.getYear().substring(2) + reportsSearchCriteria.getMonth() + "0"
		// + reportsSearchCriteria.getBillingPeriod();
		// Date date = new Date();
		// SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.AGENT_INVOICE_SUMMARY)) {

			sb.append("select b.agent_name, (b.station_code ||' '|| b.territory_code ) as station_code, ");
			sb.append(
					" a.invoice_number, sum(tr.amount) as invoice_amount, tr.agent_code, a.invoice_amount_local , b.currency_code  ");
			sb.append("  from T_AGENT_TRANSACTION  tr, t_invoice a, t_agent b ");
			sb.append(" where   tr.agent_code = a.agent_code ");
			sb.append(" and a.entity = '"+ reportsSearchCriteria.getEntity() + "' ");
			sb.append(" and a.agent_code = b.agent_code  and     ");
			sb.append(ReportUtils.getReplaceStringForIN("tr.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");

			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {
				sb.append(" and   tr.NOMINAL_CODE in (10,11) and ");
				// sb.append(" tr.sales_channel_code = " + SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS + " and"); un
				// comment withe right sales channel code when the holidays send the correct data
			} else {
				sb.append(" and   tr.NOMINAL_CODE in (1,7,19,20) ");
				sb.append(" and (tr.sales_channel_code is null OR tr.sales_channel_code <> "
						+ SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS + ") and");

			}
			if (reportsSearchCriteria.getBillingPeriod() == 1) {
				sb.append("  tr.TNX_DATE between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
			} else {
				startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
				sb.append(" tr.TNX_DATE between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
				sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");

			}
			// sb.append(" and a.invoice_number like '" + invoiceNum + "%'");
			sb.append(" and to_date(a.invioce_period_from,'dd-mm-yy') = to_date('" + startDate + "','dd-mm-yyyy hh24:mi:ss')");
			sb.append(
					" group by tr.agent_code, b.agent_name,(b.station_code ||' '|| b.territory_code ), a.invoice_number,  a.invoice_amount_local , b.currency_code ");
		} else {

			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {
				sb.append("SELECT  tr.pnr_pax_id,a.pnr, (a.title||' '|| a.first_name|| ' '|| a.last_name) as pnr_pax_name, ");
				sb.append(" ''   AS EXT_REFERENCE, ");
				sb.append(" sum (decode (tr.CR_DR,'CR',tr.amount_local * -1,'DR',tr.amount_local,0))as TOTAL_AMOUNT_PAYCUR, ");
				sb.append(" sum (decode (tr.CR_DR,'CR',tr.amount * -1,'DR',tr.amount,0))as amount, ");
				sb.append(" c.agent_code, TO_CHAR(tr.timestamp, 'DD/mm/YYYY') as TNX_DATE, b.display_name, ");
				sb.append(" 0			            AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" 0				        AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR  ,");
				sb.append(" tr.currency_code as PAY_CURRENCY,  ");
				sb.append(" 'Holiday Land Booking' as PAY_TYPE ");
				sb.append(" FROM t_pax_ext_transactions tr, T_AGENT_TRANSACTION c , t_pnr_passenger a, t_user b ");
				sb.append(" where  tr.txn_id = c.txn_id ");
				// sb.append(" and tr.sales_channel_code = " + SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS);
				sb.append(" and     c.agent_code ='" + reportsSearchCriteria.getAgentCode() + "'");
				sb.append(" and     a.pnr_pax_id = tr.pnr_pax_id ");
				sb.append(" and     b.user_id = tr.user_id ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" and  tr.timestamp between to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('"
							+ endDate + "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" and  tr.timestamp between to_date('" + startDate
							+ "','dd-mm-yyyy HH24:mi:ss')  and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				sb.append(" group by tr.pnr_pax_id, a.pnr, (a.title||' '|| a.first_name|| ' '|| a.last_name), ");
				sb.append(" c.agent_code, TO_CHAR(tr.timestamp, 'DD/mm/YYYY'), b.display_name,tr.currency_code ");

			} else {

				sb.append(" SELECT V.PNR_PAX_ID,");
				sb.append(" V.PNR             ,");
				sb.append(" (V.TITLE   ||' '   || V.FIRST_NAME  || ' '  || V.LAST_NAME) AS PNR_PAX_NAME,");
				sb.append(" V.ETICKET_ID as ETICKET_ID,"); // Eric check this line
				sb.append(" ( V.AMOUNT*-1)AMOUNT           ,");
				sb.append(" V.AGENT_CODE                   ,");
				sb.append(" V.EXT_REFERENCE                ,");
				sb.append(" ((V.AMOUNT*-1 ) / ");
				sb.append(
						" (SELECT NVL(EXRATE.EXRATE_BASE_TO_CUR, 1) AS EXCHANGE_RATE FROM T_CURRENCY_EXCHANGE_RATE EXRATE  WHERE TO_DATE('02-Mar-2010 00:00:00','dd-mon-yyyy hh24:mi:ss') BETWEEN EXRATE.EFFECTIVE_FROM AND EXRATE.EFFECTIVE_TO   AND EXRATE.STATUS       = 'ACT'   AND EXRATE.CURRENCY_CODE= ");
				sb.append(
						" (SELECT MAX(AG2.CURRENCY_CODE)  FROM T_AGENT AG2  WHERE AG2.AGENT_CODE=AGENT_CODE  AND AG2.AGENT_CODE ='SHJ001' ) ");
				sb.append(" )) TOTAL_AMOUNT_PAYCUR  ,");
				sb.append(" TO_CHAR(V.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE,");
				sb.append(" C.DISPLAY_NAME,  V.ISRESERVATION , ");
				sb.append(" 0			            AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" 0				        AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR  ");

				sb.append(" FROM");
				sb.append(" (");

				// Normal Sales
				sb.append(" SELECT P.PNR     AS PNR         ,");
				sb.append(" P.PNR_PAX_ID    AS PNR_PAX_ID   ,");
				sb.append(" P.TITLE         AS TITLE        ,");
				sb.append(" T.EXT_REFERENCE AS EXT_REFERENCE,");
				sb.append(" P.FIRST_NAME    AS FIRST_NAME   ,");
				sb.append(" P.LAST_NAME     AS LAST_NAME    ,");
				// sb.append(" F_GET_ETICKET_STRING(P.PNR_PAX_ID) AS ETICKET_ID ,");

				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = P.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append(" T.NOMINAL_CODE  AS NOMINAL_CODE ,");
				sb.append(" T.TNX_DATE      AS TNX_DATE     ,");
				sb.append(" T.USER_ID       AS USER_ID      ,");
				sb.append(" T.AMOUNT        AS AMOUNT       ,");
				sb.append(" T.AGENT_CODE    AS AGENT_CODE,	");
				sb.append("   'Y'           AS ISRESERVATION, ");
				sb.append("   'E_RES'       AS ENTITY  ");
				sb.append("		FROM T_PNR_PASSENGER P, T_PAX_TRANSACTION T");
				sb.append(" WHERE P.PNR_PAX_ID = T.PNR_PAX_ID");
				sb.append(" AND T.NOMINAL_CODE IN (19,25) ");
				sb.append(" AND (T.PAYMENT_CARRIER_CODE IS NULL OR T.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND TNX_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') AND TO_DATE('" + endDate
							+ "','dd-mm-yyyy HH24:MI:SS')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND TNX_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') AND LAST_DAY(TO_DATE('"
							+ endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				sb.append(String.format(" AND ( T.AGENT_CODE = '%s' )", reportsSearchCriteria.getAgentCode()));
				sb.append(" AND ( T.SALES_CHANNEL_CODE IS NULL or T.SALES_CHANNEL_CODE <> 11)");

				// External Sales
				sb.append(" UNION ALL");
				sb.append(" SELECT X.PNR AS PNR         ,");
				sb.append(" X.PNR_PAX_ID      AS PNR_PAX_ID   ,");
				sb.append(" X.TITLE           AS TITLE        ,");
				sb.append(" ''                AS EXT_REFERENCE,");
				sb.append(" X.FIRST_NAME      AS FIRST_NAME   ,");
				sb.append(" X.LAST_NAME       AS LAST_NAME    ,");
				// sb.append(" F_GET_ETICKET_STRING(X.PNR_PAX_ID) AS ETICKET_ID ,");

				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = X.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append(" P.NOMINAL_CODE    AS NOMINAL_CODE ,");
				sb.append(" P.TXN_TIMESTAMP       AS TNX_DATE     ,");
				sb.append(" P.USER_ID         AS USER_ID      ,");
				sb.append(" P.AMOUNT     AS AMOUNT       ,");
				sb.append(" P.AGENT_CODE      AS AGENT_CODE,	");
				sb.append("   'Y'          AS ISRESERVATION,  ");
				sb.append("   'E_RES'       AS ENTITY  ");
				sb.append("		FROM T_PAX_EXT_CARRIER_TRANSACTIONS P, T_PNR_PASSENGER X ");
				sb.append(" WHERE P.PNR_PAX_ID = X.PNR_PAX_ID");
				sb.append(" AND P.NOMINAL_CODE IN (19,25) ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND P.TXN_TIMESTAMP BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') AND TO_DATE('"
							+ endDate + "','dd-mm-yyyy HH24:MI:SS')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND P.TXN_TIMESTAMP BETWEEN TO_DATE('" + startDate
							+ "','dd-mm-yyyy HH24:mi:ss') AND LAST_DAY(TO_DATE('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				sb.append(String.format(" AND ( P.AGENT_CODE = '%s' )", reportsSearchCriteria.getAgentCode()));
				sb.append(" AND ( P.SALES_CHANNEL_CODE IS NULL or P.SALES_CHANNEL_CODE <> 11)");

				// Add Holidays Transaction
				if (reportsSearchCriteria.getShowExternal() != null && reportsSearchCriteria.getShowExternal().equals("Y")) {

					sb.append(" UNION ALL ");
					sb.append(" SELECT P.PNR       AS PNR         ,");
					sb.append(" P.pnr_pax_id 	 AS PNR_PAX_ID   ,");
					sb.append(" P.TITLE          AS TITLE        ,");
					sb.append(" 'HoliDays'       AS EXT_REFERENCE,");
					sb.append(" P.FIRST_NAME     AS FIRST_NAME   ,");
					sb.append(" P.LAST_NAME      AS LAST_NAME    ,");
					// sb.append(" F_GET_ETICKET_STRING(P.PNR_PAX_ID) AS ETICKET_ID ,");
					sb.append(
							" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
									+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = P.PNR_PAX_ID),'-') AS ETICKET_ID,");

					sb.append(" px.NOMINAL_CODE   AS NOMINAL_CODE ,");
					sb.append(" ag.TNX_DATE       AS TNX_DATE     ,");
					sb.append(" px.USER_ID        AS USER_ID      ,");
					sb.append("  SUM (DECODE (px.CR_DR,'DR',px.amount * -1,'CR',px.amount,0))    AS AMOUNT, ");
					sb.append(" a.AGENT_CODE        AS AGENT_CODE   , ");
					sb.append("   'N'          AS ISRESERVATION  ,");					
					sb.append("   prod.entity_code AS ENTITY ");
					sb.append(" FROM t_pax_ext_transactions px, t_agent_transaction ag, t_agent a, t_pnr_passenger p, t_user c, t_product prod ");
					sb.append(" WHERE px.txn_id      = ag.txn_id ");
					sb.append("	AND  prod.entity_code='"+ reportsSearchCriteria.getEntity() +"' ");
					sb.append(" AND  prod.product_type = ag.product_type ");
					sb.append(" AND px.nominal_code IN (10,11) ");
					sb.append(" AND ag.agent_code    = a.agent_code ");
					sb.append(" AND p.pnr_pax_id     = px.pnr_pax_id  ");
					sb.append(" AND c.user_id        = ag.user_id  ");
					if (reportsSearchCriteria.getBillingPeriod() == 1) {
						sb.append(" AND ag.TNX_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') AND TO_DATE('"
								+ endDate + "','dd-mm-yyyy HH24:MI:SS')");
					} else {
						startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear()
								+ "  00:00:00";
						sb.append(" AND TNX_DATE BETWEEN TO_DATE('" + startDate
								+ "','dd-mm-yyyy HH24:mi:ss') AND LAST_DAY(TO_DATE('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
					}
					sb.append(String.format(" AND ( ag.AGENT_CODE = '%s' )", reportsSearchCriteria.getAgentCode()));
					sb.append(
							" GROUP BY p.PNR, P.pnr_pax_id,  P.TITLE, P.FIRST_NAME, P.LAST_NAME, px.NOMINAL_CODE, ag.TNX_DATE, px.USER_ID, a.AGENT_CODE, prod.entity_code ");
				}

				// goquo new reservations (after invoice separated)
				// Entity set as 'E_HOLIDAYS', INVOICE_SEPARATED = 'Y'
				sb.append(" union all ")
						.append(" select pnr, null, null, ext_reference, null, null, null,  null, tnx_date, user_id, - amount, agent_code, 'Y', p.entity_code ")
						.append(" from t_agent_transaction a, t_product p ")
						.append(" where a.agent_code = '" + reportsSearchCriteria.getAgentCode() + "' ")
						.append(" and p.entity_code='" + reportsSearchCriteria.getEntity() + "' ")
						.append(" and p.product_type = a.product_type ")
						.append(" and a.INVOICE_SEPARATED = 'Y' ");

				sb.append(" 	and a.nominal_code in ( 19 , 20 ) ");

				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND tnx_date BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('" + endDate
							+ "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND tnx_date BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and last_day(to_date('"
							+ endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				
				// goquo old reservations (before invoice separated)
				// Entity set as 'E_RES', INVOICE_SEPARATED = 'N'
				// TODO remove this code when all the agent transactions are invoice_separated='Y'
				sb.append(" union all ")
						.append(" select pnr, null, null, ext_reference, null, null, null,  null, tnx_date, user_id, - amount, agent_code, 'Y', 'E_RES' ")
						.append(" from t_agent_transaction ")
						.append(" where agent_code = '" + reportsSearchCriteria.getAgentCode() + "' ")
						.append(" and INVOICE_SEPARATED = 'N' ");

				sb.append(" 	and nominal_code in ( 19 , 20 ) ");

				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND tnx_date BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('"
							+ endDate + "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND tnx_date BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and last_day(to_date('"
							+ endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}


				sb.append(" ) V ,  T_USER C");
				sb.append(" WHERE V.USER_ID   = C.USER_ID");
				sb.append(" AND v.ENTITY = '" + reportsSearchCriteria.getEntity() + "'");
				// sb.append(" AND V.NOMINAL_CODE IN (19,25)");
				sb.append(" ORDER BY V.PNR,  V.TNX_DATE");

				/*
				 * sb.append("SELECT  a.pnr_pax_id, ");
				 * sb.append("    a.pnr,(a.title||' '|| a.first_name|| ' '|| a.last_name) as pnr_pax_name,  ");
				 * sb.append("   ( b.amount*-1)amount,b.agent_code, "); sb.append("b.ext_reference, ");
				 *//** JIRA -AARESAA :3128 (Lalanthi) */
				/*
				 * 
				 * /// For GSA Managment / will have to show the local currency / below line should be removed for
				 * Moroco project and instead use the pax.local_amount //sb.append(
				 * "((b.amount*-1 ) / (SELECT nvl(exRate.exchange_rate, 1) as exchange_rate   FROM t_currency_exchange_rate exRate  WHERE  to_date('"
				 * +sdf.format(date)+
				 * " 00:00:00','dd-mon-yyyy hh24:mi:ss') BETWEEN exRate.effective_from AND  exRate.effective_to  and exRate.status = 'ACT'  and exRate.currency_code=  (select max(ag2.currency_code)   from t_agent ag2   where ag2.agent_code=agent_code   and ag2.agent_code ='"
				 * +reportsSearchCriteria.getAgentCode() +"'))) amount_local,") ;
				 *//** JIRA : 3087 - Lalanthi */
				/*
				 * sb.append(
				 * "((b.amount*-1 ) / (SELECT nvl(exRate.exrate_base_to_cur, 1) as exchange_rate   FROM t_currency_exchange_rate exRate  WHERE  to_date('"
				 * +sdf.format(date)+
				 * " 00:00:00','dd-mon-yyyy hh24:mi:ss') BETWEEN exRate.effective_from AND  exRate.effective_to  and exRate.status = 'ACT'  and exRate.currency_code=  (select max(ag2.currency_code)   from t_agent ag2   where ag2.agent_code=agent_code   and ag2.agent_code ='"
				 * +reportsSearchCriteria.getAgentCode() +"'))) amount_local,") ;
				 * sb.append("  TO_CHAR(b.tnx_date, 'DD/mm/YYYY') as TNX_DATE,c.display_name ");
				 * sb.append("  FROM t_pnr_passenger a, t_pax_transaction b, t_user c ");
				 * sb.append("  where  a.pnr_pax_id = b.pnr_pax_id and  "); sb.append("  b.user_id = c.user_id and   ");
				 * sb.append("  b.agent_code ='"+reportsSearchCriteria.getAgentCode()+"'  and ");
				 * sb.append("  b.nominal_code in (19,25) and ");
				 * sb.append(" (b.sales_channel_code is null OR b.sales_channel_code <> 11) and ");
				 * 
				 * if(reportsSearchCriteria.getBillingPeriod() == 1){
				 * sb.append("b.tnx_date between to_date('"+startDate+
				 * "','dd-mm-yyyy HH24:mi:ss') and to_date('"+endDate+"','dd-mm-yyyy HH24:mi:ss')"); }else{
				 * startDate="16-"+reportsSearchCriteria.getMonth()+"-"+reportsSearchCriteria.getYear()+"  00:00:00";
				 * sb.
				 * append("b.tnx_date between to_date('"+startDate+"','dd-mm-yyyy HH24:mi:ss') and last_day(to_date('"
				 * +endPeriod+"','mm-yyyy HH24:MI:SS'))"); } sb.append(" order by  a.pnr, b.tnx_date ");
				 */
			}
		}

		return sb.toString();
	}

	public String getInvoiceSummaryQueryByCurrency(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		StringBuilder sb = new StringBuilder();
		String startDate = null;
		String endDate = null;
		String endPeriod = null;
		int agentPrefixLength = AppSysParamsUtil.getDefaultAirlineIdentifierCode().trim().length() + 1; // Added one
																										// since substr
																										// method is one
																										// base.
		boolean showAirlineIdentifierInReports = AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers();

		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			startDate = reportsSearchCriteria.getFromDate();
			endDate = reportsSearchCriteria.getToDate();
			endPeriod = reportsSearchCriteria.getToDate();
		} else {
			startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
			endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
			endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";
		}

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.AGENT_INVOICE_SUMMARY)) {

			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {
				// HOLIDAY Data
				sb.append(" SELECT C.AGENT_NAME,");
				sb.append(" C.STATION_CODE AS STATION_CODE 									 ,");
				sb.append(" STA.STATION_NAME AS STATION_NAME 								 ,");
				sb.append(" C.TERRITORY_CODE AS TERRITORY_CODE								 ,");
				sb.append(" COU.COUNTRY_NAME AS COUNTRY_NAME								 ,");
				sb.append(" INV.INVOICE_NUMBER                                               ,");
				sb.append(" INV.EMAILS_SENT		                                             ,");
				sb.append(" INV.INVOICE_AMOUNT as INVOICE_AMOUNT_ACTUAL     				 ,");
				sb.append(" (-1 * SUM(V.TOTAL_AMOUNT)) INVOICE_AMOUNT                        ,");
				sb.append(" (-1 * SUM(V.TOTAL_AMOUNT_PAYCUR)) INVOICE_AMOUNT_PAYCURR         ,");
				sb.append(" (-1 * SUM(V.TOTAL_FARE_AMOUNT)) TOTAL_FARE                       ,");
				sb.append(" (-1 * SUM (V.TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCURR,");
				sb.append(" (-1 * SUM(V.TOTAL_AMOUNT)) INVOICE_AMOUNT_BASECURR               ,");
				sb.append(" (-1 * SUM(V.TOTAL_TAX_AMOUNT)) TOTAL_TAX                         ,");
				sb.append(" (-1 * SUM(V.TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_PAYCURR          ,");
				sb.append(" (-1 * SUM(V.TOTAL_SUR_AMOUNT)) TOTAL_SUR                         ,");
				sb.append(" (-1 * SUM(V.TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_PAYCURR          ,");
				sb.append(" (-1 * SUM(V.TOTAL_MOD_AMOUNT)) TOTAL_MOD                         ,");
				sb.append(" (-1 * SUM(V.TOTAL_MOD_AMOUNT_PAYCUR)) TOTAL_MOD_PAYCURR          ,");
				sb.append(" (-1 * SUM(V.TOTAL_AIR_AMOUNT)) TOTAL_AIR_AMOUNT	         		 ,");
				sb.append(" (-1 * SUM(V.TOTAL_LAND_AMOUNT)) TOTAL_LAND_AMOUNT				 ,");
				sb.append(" (-1 * SUM(V.TOTAL_AIR_AMOUNT_PAYCURR)) TOTAL_AIR_AMOUNT_PAYCURR   ,");
				sb.append(" (-1 * SUM(V.TOTAL_LAND_AMOUNT_PAYCURR)) TOTAL_LAND_AMOUNT_PAYCURR ,");
				sb.append(" PAYMENT_CURRENCY_CODE                                            ,");
				if (showAirlineIdentifierInReports) {
					sb.append(" substr(C.AGENT_CODE, " + agentPrefixLength + ") AS AGENT_CODE");
				} else {
					sb.append(" C.AGENT_CODE");
				}
				sb.append("  FROM");
				sb.append(" (");
				sb.append(" SELECT at.AGENT_CODE     AS AGENT_CODE             ,");
				sb.append(" et.NOMINAL_CODE  AS NOMINAL_CODE            ,");
				sb.append(" ( -1 * et.AMOUNT)            	AS TOTAL_AMOUNT            ,");
				sb.append(" ( -1 * et.AMOUNT_LOCAL)         AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" ( -1 * et.AMOUNT)          		AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" ( -1 * et.AMOUNT_LOCAL)         AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				sb.append(" et.CURRENCY_CODE		AS PAYMENT_CURRENCY_CODE   ,");
				sb.append(" ( -1 * et.AMOUNT)       AS TOTAL_AIR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_LAND_AMOUNT       ,");
				sb.append(" ( -1 * et.AMOUNT_LOCAL) AS TOTAL_AIR_AMOUNT_PAYCURR ,");
				sb.append(" 0                       AS TOTAL_LAND_AMOUNT_PAYCURR ");
				sb.append(" 	FROM t_pax_ext_transactions et,    t_agent_transaction at, T_PRODUCT p");
				sb.append(" WHERE et.TXN_ID = at.TXN_ID ");
				sb.append(" AND p.PRODUCT_TYPE = at.PRODUCT_TYPE ");
				sb.append("	AND et.nominal_code in (10, 11) ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" and et.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
					sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" and et.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
					sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");
				}

				if (reportsSearchCriteria.getAgents() != null) { // if all agents selected,
																	// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(
							ReportUtils.getReplaceStringForIN("at.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "  ");
				}
				sb.append(" AND at.SALES_CHANNEL_CODE <> 11");
				sb.append(" AND p.entity_code ='" + reportsSearchCriteria.getEntity() + "' ");
				sb.append(" )V ,  T_AGENT C ,  T_INVOICE INV, T_STATION STA, T_COUNTRY COU");
				sb.append(" WHERE V.AGENT_CODE = C.AGENT_CODE");
				sb.append(" AND C.AGENT_CODE  = INV.AGENT_CODE");
				sb.append(" AND STA.STATION_CODE = C.STATION_CODE");
				sb.append(" AND COU.COUNTRY_CODE = STA.COUNTRY_CODE");
				sb.append(" AND TO_DATE(INV.INVIOCE_PERIOD_FROM,'dd-mon-yy hh24:mi:ss') = TO_DATE('" + startDate
						+ "','dd-mm-yyyy hh24:mi:ss')");
				sb.append(
						" GROUP BY C.AGENT_CODE,   C.AGENT_NAME ,   C.STATION_CODE,   C.TERRITORY_CODE,  STA.STATION_NAME, COU.COUNTRY_NAME, INV.INVOICE_NUMBER, INV.EMAILS_SENT , PAYMENT_CURRENCY_CODE");
				sb.append(" ,INV.INVOICE_AMOUNT");

			} else if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewInvoiceSummaryQueryByCurrency(reportsSearchCriteria));
			} else {
				sb.append(" SELECT * FROM (");
				sb.append(" SELECT C.AGENT_NAME,");
				sb.append(" C.STATION_CODE AS STATION_CODE 									 ,");
				sb.append(" STA.STATION_NAME AS STATION_NAME 								 ,");
				sb.append(" C.TERRITORY_CODE AS TERRITORY_CODE								 ,");
				sb.append(" COU.COUNTRY_NAME AS COUNTRY_NAME								 ,");
				sb.append(" INV.INVOICE_NUMBER                                               ,");
				sb.append(" INV.EMAILS_SENT		                                             ,");
				sb.append(" INV.INVOICE_AMOUNT as INVOICE_AMOUNT_ACTUAL     				 ,");
				sb.append(" (-1 * SUM(V.TOTAL_AMOUNT)) INVOICE_AMOUNT                        ,");
				sb.append(" (-1 * SUM(V.TOTAL_AMOUNT_PAYCUR)) INVOICE_AMOUNT_PAYCURR         ,");
				sb.append(" (-1 * SUM(V.TOTAL_FARE_AMOUNT)) TOTAL_FARE                       ,");
				sb.append(" (-1 * SUM (V.TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCURR,");
				sb.append(" (-1 * SUM(V.TOTAL_AMOUNT)) INVOICE_AMOUNT_BASECURR               ,");
				sb.append(" (-1 * SUM(V.TOTAL_TAX_AMOUNT)) TOTAL_TAX                         ,");
				sb.append(" (-1 * SUM(V.TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_PAYCURR          ,");
				sb.append(" (-1 * SUM(V.TOTAL_SUR_AMOUNT)) TOTAL_SUR                         ,");
				sb.append(" (-1 * SUM(V.TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_PAYCURR          ,");
				sb.append(" (-1 * SUM(V.TOTAL_MOD_AMOUNT)) TOTAL_MOD                         ,");
				sb.append(" (-1 * SUM(V.TOTAL_MOD_AMOUNT_PAYCUR)) TOTAL_MOD_PAYCURR          ,");
				sb.append(" (-1 * SUM(V.TOTAL_AIR_AMOUNT)) TOTAL_AIR_AMOUNT		 			 ,");
				sb.append(" (-1 * SUM(V.TOTAL_LAND_AMOUNT)) TOTAL_LAND_AMOUNT                ,");
				sb.append(" (-1 * SUM(V.TOTAL_AIR_AMOUNT_PAYCURR)) TOTAL_AIR_AMOUNT_PAYCURR    ,");
				sb.append(" (-1 * SUM(V.TOTAL_LAND_AMOUNT_PAYCURR)) TOTAL_LAND_AMOUNT_PAYCURR  ,");
				sb.append(" PAYMENT_CURRENCY_CODE                                            ,");
				if (showAirlineIdentifierInReports) {
					sb.append(" substr(C.AGENT_CODE, " + agentPrefixLength + ") AS AGENT_CODE,");
				} else {
					sb.append(" C.AGENT_CODE,");
				}
				sb.append(" inv.entity_code");
				
				sb.append("  FROM");

				sb.append(" (");
				sb.append(" SELECT B.AGENT_CODE AGENT_CODE                        ,");
				sb.append(" B.NOMINAL_CODE             AS NOMINAL_CODE            ,");
				sb.append(" A.TOTAL_AMOUNT             AS TOTAL_AMOUNT            ,");
				sb.append(" A.TOTAL_AMOUNT_PAYCUR      AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" A.TOTAL_FARE_AMOUNT        AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" A.TOTAL_FARE_AMOUNT_PAYCUR AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" A.TOTAL_TAX_AMOUNT         AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" A.TOTAL_TAX_AMOUNT_PAYCUR  AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" A.TOTAL_SUR_AMOUNT         AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" A.TOTAL_SUR_AMOUNT_PAYCUR  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" A.TOTAL_MOD_AMOUNT         AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" A.TOTAL_MOD_AMOUNT_PAYCUR  AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				sb.append(" A.PAYMENT_CURRENCY_CODE    AS PAYMENT_CURRENCY_CODE   ,");
				sb.append(" A.TOTAL_AMOUNT             AS TOTAL_AIR_AMOUNT        ,");
				sb.append(" 0                          AS TOTAL_LAND_AMOUNT       ,");
				sb.append(" A.TOTAL_AMOUNT_PAYCUR      AS TOTAL_AIR_AMOUNT_PAYCURR ,");
				sb.append(" 0                          AS TOTAL_LAND_AMOUNT_PAYCURR ");
				sb.append(" 	FROM T_PAX_TXN_BREAKDOWN_SUMMARY A, T_PAX_TRANSACTION B");
				sb.append(" WHERE A.PAX_TXN_ID = B.TXN_ID");
				sb.append(" AND B.NOMINAL_CODE IN (19,25) ");
				sb.append(" AND (B.PAYMENT_CARRIER_CODE IS NULL OR B.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')  ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" and A.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
					sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" and A.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
					sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");
				}
				if (reportsSearchCriteria.getAgents() != null) { // if all agents selected,
																	// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(
							ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "  ");
				}
				sb.append(" AND B.SALES_CHANNEL_CODE <> 11");

				// External Sales (Values from t_pax_ext_carrier_transactions) : TODO - no breakdown exist for extenal
				// payments.
				sb.append(" UNION ALL");
				sb.append(" SELECT P.AGENT_CODE     AS AGENT_CODE              ,");
				sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
				sb.append(" P.AMOUNT           AS TOTAL_AMOUNT            ,");
				sb.append(" P.AMOUNT_PAYCUR    AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" P.AMOUNT           AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" P.AMOUNT_PAYCUR    AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				sb.append(" P.PAYCUR_CODE  			AS PAYMENT_CURRENCY_CODE   ,");
				sb.append(" P.AMOUNT				AS TOTAL_AIR_AMOUNT        ,");
				sb.append(" 0			            AS TOTAL_LAND_AMOUNT       ,");
				sb.append(" P.AMOUNT_PAYCUR			AS TOTAL_AIR_AMOUNT_PAYCURR ,");
				sb.append(" 0				        AS TOTAL_LAND_AMOUNT_PAYCURR ");
				sb.append(" 	FROM T_PAX_EXT_CARRIER_TRANSACTIONS P");
				sb.append(" WHERE P.NOMINAL_CODE IN (19,25) ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" and P.TXN_TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
					sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" and P.TXN_TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
					sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");
				}
				if (reportsSearchCriteria.getAgents() != null) { // if all agents selected,
																	// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(
							ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "  ");
				}
				sb.append(" AND P.SALES_CHANNEL_CODE <> 11");

				// ADD HOLIDAY External Data
				if (reportsSearchCriteria.getShowExternal() != null
						&& reportsSearchCriteria.getShowExternal().equalsIgnoreCase("Y")) {
					sb.append(" UNION ALL");
					sb.append(" SELECT at.AGENT_CODE     AS AGENT_CODE             ,");
					sb.append(" et.NOMINAL_CODE  AS NOMINAL_CODE            ,");
					sb.append(" ( -1 * et.AMOUNT)            	AS TOTAL_AMOUNT            ,");
					sb.append(" ( -1 * et.AMOUNT_LOCAL)         AS TOTAL_AMOUNT_PAYCUR     ,");
					sb.append(" ( -1 * et.AMOUNT)          		AS TOTAL_FARE_AMOUNT       ,");
					sb.append(" ( -1 * et.AMOUNT_LOCAL)         AS TOTAL_FARE_AMOUNT_PAYCUR,");
					sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
					sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
					sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR ,");
					sb.append(" et.CURRENCY_CODE		AS PAYMENT_CURRENCY_CODE   ,");
					sb.append(" 0						AS TOTAL_AIR_AMOUNT        ,");
					sb.append(" ( -1 * et.AMOUNT)       AS TOTAL_LAND_AMOUNT       ,");
					sb.append(" 0						AS TOTAL_AIR_AMOUNT_PAYCURR ,");
					sb.append(" ( -1 * et.AMOUNT_LOCAL) AS TOTAL_LAND_AMOUNT_PAYCURR ");
					sb.append(" 	FROM t_pax_ext_transactions et,    t_agent_transaction at ,T_PRODUCT p");
					sb.append(" WHERE et.TXN_ID = at.TXN_ID ");
					sb.append(" and at.PRODUCT_TYPE = p.PRODUCT_TYPE ");
					sb.append("	AND et.nominal_code in (10, 11) ");
					if (reportsSearchCriteria.getBillingPeriod() == 1) {
						sb.append(" and et.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
						sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
					} else {
						startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear()
								+ "  00:00:00";
						sb.append(" and et.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss')and ");
						sb.append("    last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS')) ");
					}
					if (reportsSearchCriteria.getAgents() != null) { // if all agents selected,
																		// reportsSearchCriteria.getAgents() == null
						sb.append(" AND ").append(
								ReportUtils.getReplaceStringForIN("at.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
										+ "  ");
					}
					sb.append(" AND at.SALES_CHANNEL_CODE <> 11");
					sb.append(" AND p.entity_code ='" + reportsSearchCriteria.getEntity() + "' ");
				}

				sb.append(" )V ,  T_AGENT C ,  T_INVOICE INV, T_STATION STA, T_COUNTRY COU");

				sb.append(" WHERE V.AGENT_CODE = C.AGENT_CODE");
				sb.append(" AND C.AGENT_CODE  = INV.AGENT_CODE");
				sb.append(" AND STA.STATION_CODE = C.STATION_CODE");
				sb.append(" AND COU.COUNTRY_CODE = STA.COUNTRY_CODE");
				// sb.append(" AND V.NOMINAL_CODE IN (19,25)");
				sb.append(" AND TO_DATE(INV.INVIOCE_PERIOD_FROM,'dd-mon-yy hh24:mi:ss') = TO_DATE('" + startDate
						+ "','dd-mm-yyyy hh24:mi:ss')");
				sb.append(" AND inv.ENTITY_CODE = 'E_RES' ");

				sb.append(
						" GROUP BY C.AGENT_CODE,   C.AGENT_NAME ,   C.STATION_CODE,   C.TERRITORY_CODE,  INV.INVOICE_NUMBER , STA.STATION_NAME,");
				sb.append(" COU.COUNTRY_NAME, INV.EMAILS_SENT, PAYMENT_CURRENCY_CODE");
				sb.append(" ,INV.INVOICE_AMOUNT, inv.entity_code");
				
				//Non Reservation Entity Data
				sb.append(" UNION ALL");
				sb.append(" SELECT a.AGENT_NAME, a.STATION_CODE, s.STATION_NAME, a.TERRITORY_CODE, c.COUNTRY_NAME, i.INVOICE_NUMBER, i.EMAILS_SENT, ");
				sb.append(" i.INVOICE_AMOUNT as invoice_amount_actual, i.INVOICE_AMOUNT, ");
				sb.append(" 0 as invoice_amount_actual_paycurr, 0 as total_fare, 0 as total_fare_amount_paycurr, i.INVOICE_AMOUNT_LOCAL as invoice_amount_basecurr, ");
				sb.append(" 0 as TOTAL_TAX,0 as TOTAL_TAX_PAYCURR,0 as TOTAL_SUR, 0 as TOTAL_SUR_PAYCURR,0 total_mod, 0 total_mod_paycurr,");
				sb.append(" 0 total_air_amount, 0 total_land_amount, 0 total_air_amount_paycurr, 0 total_land_amount_paycurr,");
				sb.append(" '-' as payment_currency_code,substr(a.agent_code,4) AS agent_code , i.entity_code");
				sb.append(" FROM T_INVOICE i,T_AGENT a,T_STATION s,t_country c ");
				sb.append(" WHERE i.AGENT_CODE = a.AGENT_CODE ");
				sb.append(" AND a.STATION_CODE = s.STATION_CODE ");
				sb.append(" AND   c.country_code = s.country_code ");
				sb.append(" AND TO_DATE(i.INVIOCE_PERIOD_FROM,'dd-mon-yy hh24:mi:ss') = TO_DATE('" + startDate
						+ "','dd-mm-yyyy hh24:mi:ss')");
				if (reportsSearchCriteria.getAgents() != null) { // if all agents selected,
					// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(
							ReportUtils.getReplaceStringForIN("a.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "  ");
				}
				sb.append(" AND i.ENTITY_CODE <> 'E_RES' ");
				sb.append(" ) x");
				sb.append(" WHERE x.entity_code ='" + reportsSearchCriteria.getEntity() + "' ");
				
				
			}
		} else {

			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {

				sb.append("SELECT  tr.pnr_pax_id,a.pnr, (a.title||' '|| a.first_name|| ' '|| a.last_name) as pnr_pax_name, ");
				sb.append(" SUM (DECODE (tr.CR_DR,'CR',tr.amount * -1,'DR',tr.amount,0))AS AMOUNT, ");
				sb.append(" SUM (DECODE (tr.CR_DR,'CR',tr.amount_local * -1,'DR',tr.amount_local,0))AS TOTAL_AMOUNT_PAYCUR,  ");
				sb.append(" SUM (DECODE (tr.CR_DR,'CR',tr.amount * -1,'DR',tr.amount,0))AS TOTAL_FARE_AMOUNT,  ");
				sb.append(
						" SUM (DECODE (tr.CR_DR,'CR',tr.amount_local * -1,'DR',tr.amount_local,0))AS TOTAL_FARE_AMOUNT_PAYCUR,  ");
				sb.append(" 0 as TOTAL_TAX_AMOUNT,  ");
				sb.append(" 0 as TOTAL_TAX_AMOUNT_PAYCUR,  ");
				sb.append(" 0 as TOTAL_SUR_AMOUNT,  ");
				sb.append(" 0 as TOTAL_SUR_AMOUNT_PAYCUR,  ");
				sb.append(" 0 as TOTAL_MOD_AMOUNT,  ");
				sb.append(" 0 as TOTAL_MOD_AMOUNT_PAYCUR,    ");
				sb.append(" tr.currency_code as PAY_CURRENCY,  ");
				sb.append(" 'Holiday Land Booking' as PAY_TYPE, ");
				sb.append(" '' as EXT_REFERENCE,  ");
				sb.append(" pr.entity_code, ");
				sb.append(" c.invoice_separated, ");
				if (showAirlineIdentifierInReports) {
					sb.append(" substr(c.agent_code, " + agentPrefixLength
							+ ") as AGENT_CODE, TO_CHAR(tr.timestamp, 'DD/mm/YYYY') as TNX_DATE, b.display_name");
				} else {
					sb.append(" c.agent_code, TO_CHAR(tr.timestamp, 'DD/mm/YYYY') as TNX_DATE, b.display_name");
				}
				sb.append(" FROM t_pax_ext_transactions tr, T_AGENT_TRANSACTION c , t_pnr_passenger a, t_user b, t_product pr");
				sb.append(" where  tr.txn_id = c.txn_id ");
				// sb.append(" and tr.sales_channel_code = " + SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS);
				sb.append(" and     c.agent_code ='" + reportsSearchCriteria.getAgentCode() + "'");
				sb.append(" and     a.pnr_pax_id = tr.pnr_pax_id ");
				sb.append(" and     b.user_id = tr.user_id ");
				sb.append(" and     pr.product_type = c.product_type ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" and  tr.timestamp between to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('"
							+ endDate + "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" and  tr.timestamp between to_date('" + startDate
							+ "','dd-mm-yyyy HH24:mi:ss')  and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				sb.append(" and (pr.entity_code = '" + reportsSearchCriteria.getEntity() + "' OR c.invoice_separated = 'N' )");
				sb.append(" group by tr.pnr_pax_id, a.pnr, (a.title||' '|| a.first_name|| ' '|| a.last_name), ");
				sb.append(" c.agent_code, TO_CHAR(tr.timestamp, 'DD/mm/YYYY'), b.display_name,   tr.currency_code ,pr.entity_code ,c.invoice_separated");

			} else if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewInvoiceDetailsQueryByCurrency(reportsSearchCriteria));
			}

			else {

				sb.append(" SELECT V.PNR_PAX_ID,");
				sb.append(" V.PNR             ,");
				sb.append(" (V.TITLE ||' ' || V.FIRST_NAME || ' ' || V.LAST_NAME) AS PNR_PAX_NAME ,");
				if (showAirlineIdentifierInReports) {
					sb.append(" substr(V.AGENT_CODE, " + agentPrefixLength + ") AS AGENT_CODE,");
				} else {
					sb.append(" V.AGENT_CODE                                                 ,");
				}
				sb.append(" V.EXT_REFERENCE                                           ,");
				sb.append(" TO_CHAR(V.TNX_DATE, 'DD/mm/YYYY') AS TNX_DATE             ,");
				sb.append(" C.DISPLAY_NAME                                            ,");
				sb.append(" (-1 * V.TOTAL_AMOUNT) AMOUNT                              ,");
				sb.append(" (-1 * V.TOTAL_AMOUNT_PAYCUR) TOTAL_AMOUNT_PAYCUR          ,");
				sb.append(" (-1 * V.TOTAL_FARE_AMOUNT) TOTAL_FARE_AMOUNT              ,");
				sb.append(" (-1 * V.TOTAL_FARE_AMOUNT_PAYCUR) TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" (-1 * V.TOTAL_TAX_AMOUNT) TOTAL_TAX_AMOUNT                ,");
				sb.append(" (-1 * V.TOTAL_TAX_AMOUNT_PAYCUR) TOTAL_TAX_AMOUNT_PAYCUR  ,");
				sb.append(" (-1 * V.TOTAL_SUR_AMOUNT) TOTAL_SUR_AMOUNT                ,");
				sb.append(" (-1 * V.TOTAL_SUR_AMOUNT_PAYCUR) TOTAL_SUR_AMOUNT_PAYCUR  ,");
				sb.append(" (-1 * V.TOTAL_MOD_AMOUNT) TOTAL_MOD_AMOUNT                ,");
				sb.append(" (-1 * V.TOTAL_MOD_AMOUNT_PAYCUR) TOTAL_MOD_AMOUNT_PAYCUR  ,");
				sb.append("  V.PAY_CUR_CODE AS PAY_CURRENCY							  ,");
				sb.append("  V.PAY_TYPE AS PAY_TYPE									  ,");
				sb.append("  v.entity_code											  ,");
				sb.append("  v.invoice_separated										   ");
				sb.append("		FROM");

				sb.append(" (");
				sb.append(" SELECT B.AGENT_CODE AGENT_CODE                        ,");
				sb.append(" B.NOMINAL_CODE             AS NOMINAL_CODE            ,");
				sb.append(" B.USER_ID                  AS USER_ID                 ,");
				sb.append(" B.EXT_REFERENCE            AS EXT_REFERENCE           ,");
				sb.append(" P.PNR_PAX_ID               AS PNR_PAX_ID              ,");
				sb.append(" P.PNR                      AS PNR                     ,");
				sb.append(" P.TITLE                    AS TITLE                   ,");
				sb.append(" P.FIRST_NAME               AS FIRST_NAME              ,");
				sb.append(" P.LAST_NAME                AS LAST_NAME               ,");
				sb.append(" A.TIMESTAMP                AS TNX_DATE                ,");
				sb.append(" A.TOTAL_AMOUNT             AS TOTAL_AMOUNT            ,");
				sb.append(" A.TOTAL_AMOUNT_PAYCUR      AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" A.TOTAL_FARE_AMOUNT        AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" A.TOTAL_FARE_AMOUNT_PAYCUR AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" A.TOTAL_TAX_AMOUNT         AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" A.TOTAL_TAX_AMOUNT_PAYCUR  AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" A.TOTAL_SUR_AMOUNT         AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" A.TOTAL_SUR_AMOUNT_PAYCUR  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" A.TOTAL_MOD_AMOUNT         AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" A.TOTAL_MOD_AMOUNT_PAYCUR  AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				sb.append(" 'Air ticket reservation'       AS PAY_TYPE 				  ,");
				sb.append("  A.payment_currency_code    AS PAY_CUR_CODE, ");
				sb.append(" 'E_RES'    AS entity_code, ");
				sb.append(" 'Y'    AS invoice_separated ");
				sb.append(" 	FROM T_PAX_TXN_BREAKDOWN_SUMMARY A,     T_PAX_TRANSACTION B , T_PNR_PASSENGER P ");
				sb.append(" WHERE A.PAX_TXN_ID = B.TXN_ID");
				sb.append(" AND (B.PAYMENT_CARRIER_CODE IS NULL OR B.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
				sb.append(" AND B.PNR_PAX_ID     = P.PNR_PAX_ID");
				sb.append(" AND B.NOMINAL_CODE IN (19,25) ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND A.TIMESTAMP BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('"
							+ endDate + "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND A.TIMESTAMP BETWEEN to_date('" + startDate
							+ "','dd-mm-yyyy HH24:mi:ss') and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				sb.append(String.format(" AND( B.AGENT_CODE ='%s' )", reportsSearchCriteria.getAgentCode()));
				sb.append(" AND B.SALES_CHANNEL_CODE <> 11");

				sb.append(" union all ")
						.append(" select agent_code, null, user_id, EXT_REFERENCE, null, pnr, null, null, null, tnx_date, ")
						.append("	-1 * amount, -1 * amount_local, null, null, null, null, null, null, null, null, ")
						.append("	decode (nominal_code, 19, 'Package reservation', 20, 'Package Refund', '-') as pay_type, ")
						.append("	currency_code ,p.entity_code,tr.invoice_separated").append(" from t_agent_transaction tr,T_PRODUCT p ")
						.append(" 	where tr.PRODUCT_TYPE = p.PRODUCT_TYPE and agent_code = '" + reportsSearchCriteria.getAgentCode() + "' ");

				sb.append(" 	and nominal_code in ( 19 , 20 ) ");

				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND tnx_date BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('" + endDate
							+ "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND tnx_date BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and last_day(to_date('"
							+ endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}

				// External Sales
				sb.append(" UNION ALL");
				sb.append(" SELECT P.AGENT_CODE AS AGENT_CODE             ,");
				sb.append(" P.NOMINAL_CODE     AS NOMINAL_CODE            ,");
				sb.append(" P.USER_ID          AS USER_ID                 ,");
				sb.append(" ''                 AS EXT_REFERENCE           ,");
				sb.append(" X.PNR_PAX_ID       AS PNR_PAX_ID              ,");
				sb.append(" X.PNR              AS PNR                     ,");
				sb.append(" X.TITLE            AS TITLE                   ,");
				sb.append(" X.FIRST_NAME       AS FIRST_NAME              ,");
				sb.append(" X.LAST_NAME        AS LAST_NAME               ,");
				sb.append(" P.TXN_TIMESTAMP    AS TNX_DATE                ,");
				sb.append(" P.AMOUNT		   AS TOTAL_AMOUNT            ,");
				sb.append(" P.AMOUNT_PAYCUR    AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" P.AMOUNT      	   AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" P.AMOUNT_PAYCUR    AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                  AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                  AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                  AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" 0                  AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" 0                  AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				sb.append(" 'Air ticket reservation'       AS PAY_TYPE 				  ,");
				sb.append(" P.PAYCUR_CODE    AS PAY_CUR_CODE, ");
				sb.append(" 'E_RES'    AS entity_code, ");
				sb.append(" 'Y'    AS invoice_separated ");
				sb.append("		FROM T_PAX_EXT_CARRIER_TRANSACTIONS P, T_PNR_PASSENGER X");
				sb.append(" WHERE X.PNR_PAX_ID = P.PNR_PAX_ID ");
				sb.append(" AND P.NOMINAL_CODE IN (19,25) ");
				if (reportsSearchCriteria.getBillingPeriod() == 1) {
					sb.append(" AND P.TXN_TIMESTAMP BETWEEN to_date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') and to_date('"
							+ endDate + "','dd-mm-yyyy HH24:mi:ss')");
				} else {
					startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
					sb.append(" AND P.TXN_TIMESTAMP BETWEEN to_date('" + startDate
							+ "','dd-mm-yyyy HH24:mi:ss') and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
				}
				sb.append(String.format(" AND( P.AGENT_CODE ='%s' )", reportsSearchCriteria.getAgentCode()));
				sb.append(" AND P.SALES_CHANNEL_CODE <> 11");

				// Holiday Data
				if (reportsSearchCriteria.getShowExternal() != null && reportsSearchCriteria.getShowExternal().equals("Y")) {
					sb.append("  UNION ALL   ");
					sb.append("  SELECT a.AGENT_CODE       AS AGENT_CODE,  ");
					sb.append("  px.NOMINAL_CODE                                              AS NOMINAL_CODE ,  ");
					sb.append("  px.USER_ID                                                   AS USER_ID , ");
					sb.append("  'HoliDays'                                                   AS EXT_REFERENCE,  ");
					sb.append("  P.pnr_pax_id                                                 AS PNR_PAX_ID ,  ");
					sb.append("  P.PNR || '  LAND AMOUNT'                                           AS PNR ,  ");
					sb.append("  P.TITLE                                                      AS TITLE ,  ");
					sb.append("  P.FIRST_NAME                                                 AS FIRST_NAME ,  ");
					sb.append("  P.LAST_NAME                                                  AS LAST_NAME ,  ");
					sb.append("  ag.TNX_DATE                                                  AS TNX_DATE ,  ");
					sb.append("  SUM (DECODE (px.CR_DR,'DR',px.amount * -1,'CR',px.amount,0))                AS TOTAL_AMOUNT,  ");
					sb.append(
							"  SUM (DECODE (px.CR_DR,'DR',px.amount_local * -1,'CR',px.amount_local,0))     AS TOTAL_AMOUNT_PAYCUR,  ");
					sb.append(
							"  SUM (DECODE (px.CR_DR,'DR',px.amount * -1,'CR',px.amount,0))               AS TOTAL_FARE_AMOUNT ,  ");
					sb.append(
							"  SUM (DECODE (px.CR_DR,'DR',px.amount_local * -1,'CR',px.amount_local,0))     AS TOTAL_FARE_AMOUNT_PAYCUR,  ");
					sb.append("  0                 AS TOTAL_TAX_AMOUNT ,  ");
					sb.append("  0                 AS TOTAL_TAX_AMOUNT_PAYCUR ,  ");
					sb.append("  0                 AS TOTAL_SUR_AMOUNT ,  ");
					sb.append("  0                 AS TOTAL_SUR_AMOUNT_PAYCUR ,  ");
					sb.append("  0                 AS TOTAL_MOD_AMOUNT ,  ");
					sb.append("  0                 AS TOTAL_MOD_AMOUNT_PAYCUR, ");
					sb.append(" 'Holiday/Land Payment'       AS PAY_TYPE 				  ,");
					sb.append("  A.currency_code   AS PAY_CUR_CODE, ");
					sb.append("  pr.entity_code, ");
					sb.append("  ag.invoice_separated ");
					sb.append("  FROM t_pax_ext_transactions px, t_agent_transaction ag, t_agent a, t_pnr_passenger p, t_user c ,T_PRODUCT pr ");
					sb.append("  WHERE px.txn_id      = ag.txn_id  ");
					sb.append("  and pr.product_type = ag.product_type  ");
					sb.append("  AND px.nominal_code IN (10,11)  ");
					sb.append("  AND ag.agent_code    = a.agent_code  ");
					sb.append("  AND p.pnr_pax_id     = px.pnr_pax_id  ");
					sb.append("  AND c.user_id        = ag.user_id  ");
					if (reportsSearchCriteria.getBillingPeriod() == 1) {
						sb.append(" AND ag.TNX_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') AND TO_DATE('"
								+ endDate + "','dd-mm-yyyy HH24:MI:SS')");
					} else {
						startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear()
								+ "  00:00:00";
						sb.append(" AND ag.TNX_DATE BETWEEN TO_DATE('" + startDate
								+ "','dd-mm-yyyy HH24:mi:ss') AND LAST_DAY(TO_DATE('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
					}
					sb.append(String.format(" AND ( ag.AGENT_CODE = '%s' )", reportsSearchCriteria.getAgentCode()));
					sb.append(
							" GROUP BY p.PNR, P.pnr_pax_id,  P.TITLE, P.FIRST_NAME, P.LAST_NAME, px.NOMINAL_CODE, ag.TNX_DATE, px.USER_ID, a.AGENT_CODE, A.currency_code ,pr.entity_code,ag.invoice_separated ");
				}

				sb.append(" ) V , T_USER C");

				sb.append(" WHERE V.USER_ID   = C.USER_ID");
				sb.append(" and (v.entity_code = '" + reportsSearchCriteria.getEntity() + "' OR v.invoice_separated = 'N' )");
				
				// sb.append(" AND V.NOMINAL_CODE IN (19,25)");
				sb.append(" ORDER BY V.PNR, V.TNX_DATE");
			}
		}

		return sb.toString();
	}

	public String getInvoiceSummaryAttachment(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		sb.append(
				" select inv.invoice_number, inv.invoice_date, to_char(inv.invioce_period_from,'DD/mm/YYYY') invioce_period_from, ");
		sb.append(" to_char(inv.invoice_period_to,'DD/mm/YYYY')invoice_period_to, ");
		sb.append(
				" inv.invoice_amount+inv.commission_amount as INVOICE_AMOUNT, inv.invoice_amount_local as INVOICE_AMOUNT_LOCAL, inv.invoice_amount as amount_due, inv.invoice_amount_local as amount_due_local, inv.total_fare_amount, inv.commission_amount, ag.address_line_1, ag.address_line_2, ");
		sb.append(" ag.address_state_province, ag.agent_name,  ag.pref_rpt_format  from T_INVOICE inv, T_Agent ag");

		sb.append(" where inv.agent_code = ag.agent_code and inv.billing_period = '" + reportsSearchCriteria.getBillingPeriod()
				+ "' ");
		sb.append(" and inv.entity_code = '"+ reportsSearchCriteria.getEntity() +"' ");
		sb.append(" and trunc(inv.invioce_period_from) >= to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','DD-MON-YYYY') ");
		sb.append(
				" and trunc(inv.invoice_period_to) <= to_date('" + reportsSearchCriteria.getDateRangeTo() + "','DD-MON-YYYY') ");
		sb.append(" and inv.agent_code = '" + reportsSearchCriteria.getAgentCode() + "'");

		return sb.toString();
	}

	public String getInvoiceSummaryAttachmentByCurrency(ReportsSearchCriteria reportsSearchCriteria) {
		// String invoiceNum = "";
		// if (reportsSearchCriteria.getMonth().length() < 2) {
		// invoiceNum = reportsSearchCriteria.getYear().substring(2) + "0" + reportsSearchCriteria.getMonth() + "0"
		// + reportsSearchCriteria.getBillingPeriod();
		// } else {
		// invoiceNum = reportsSearchCriteria.getYear().substring(2) + reportsSearchCriteria.getMonth() + "0"
		// + reportsSearchCriteria.getBillingPeriod();
		// }
		int year = Integer.parseInt(reportsSearchCriteria.getYear());
		int month = Integer.parseInt(reportsSearchCriteria.getMonth());
		// TODO move to a util function
		String startDate;
		String endDate;

		if (reportsSearchCriteria.getBillingPeriod() == 1) {
			startDate = "01-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 00:00:00";
			endDate = "15-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  23:59:59";
		} else {
			startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
			endDate = Integer.toString(CalendarUtil.getLastDayOfYearAndMonth(year, month)) + "-"
					+ reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  23:59:59";
		}

		// TODO remove END PERIOD and replace with end date
		String endPeriod = reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + " 23:59:59";

		StringBuilder sb = new StringBuilder();
		if (reportsSearchCriteria.isReportViewNew()) {
			sb.append(RevenueReportsStatementCreator.getNewInvoiceSummaryAttachmentByCurrencyQuery(reportsSearchCriteria));
		} else {
			sb.append(
					" select inoice.*, nvl(holiday.holi_amount, 0) holi_amount, nvl(holiday.holi_amount_local, 0)  holi_amount_local,  ");
			sb.append(
					" nvl(goquo.goquo_amount, 0) goquo_amount, nvl(goquo.goquo_amount_local, 0) goquo_amount_local, product.goquo goquo, product.mco mco ");
			sb.append(" from ( select * from  ( ");
			sb.append(" select invoice_number, invoice_date, to_char(invioce_period_from,'DD/mm/YYYY') invioce_period_from, ");
			sb.append(" to_char(invoice_period_to,'DD/mm/YYYY') invoice_period_to ");
			sb.append(" from  t_invoice ");
			sb.append(" where agent_code  = '" + reportsSearchCriteria.getAgentCode() + "' ");
			sb.append(" and entity_code = '" + reportsSearchCriteria.getEntity() + "' ");
			// sb.append(" and invoice_number like '" + invoiceNum + "%') inv, ");
			sb.append(" and to_date(invioce_period_from,'dd-mm-yy') = to_date('" + startDate
					+ "','dd-mm-yyyy hh24:mi:ss') ) inv, ");

			// my addidng
			sb.append(" (");
			sb.append(" SELECT (-1 * SUM(V.TOTAL_AMOUNT)) INVOICE_AMOUNT                       ,");
			sb.append(" (      -1 * SUM(V.TOTAL_AMOUNT_PAYCUR)) INVOICE_AMOUNT_PAYCURR         ,");
			sb.append(" (      -1 * SUM(V.TOTAL_FARE_AMOUNT)) TOTAL_FARE                       ,");
			sb.append(" (      -1 * SUM (V.TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCURR,");
			sb.append(" (      -1 * SUM(V.TOTAL_TAX_AMOUNT)) TOTAL_TAX                         ,");
			sb.append(" (      -1 * SUM(V.TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_PAYCURR          ,");
			sb.append(" (      -1 * SUM(V.TOTAL_SUR_AMOUNT)) TOTAL_SUR                         ,");
			sb.append(" (      -1 * SUM(V.TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_PAYCURR          ,");
			sb.append(" (      -1 * SUM(V.TOTAL_MOD_AMOUNT)) TOTAL_MOD                         ,");
			sb.append(" (      -1 * SUM(V.TOTAL_MOD_AMOUNT_PAYCUR)) TOTAL_MOD_PAYCURR          ,");
			// sb.append(" V.PAYMENT_CURRENCY_CODE ,");
			sb.append(" B.AGENT_CODE                                                           ,");
			sb.append(" B.AGENT_NAME                                                           ,");
			sb.append(" B.ADDRESS_LINE_1                                                       ,");
			sb.append(" B.ADDRESS_LINE_2                                                       ,");
			sb.append(" B.ADDRESS_STATE_PROVINCE                                               ,");
			sb.append(" 'E_RES' AS entity ");
			// sb.append(" V.EXT_REFERENCE");
			sb.append(" FROM");

			sb.append(" (");
			sb.append(" SELECT B.AGENT_CODE AGENT_CODE                         ,");
			sb.append(" B.NOMINAL_CODE             AS NOMINAL_CODE            ,");
			sb.append(" B.USER_ID                  AS USER_ID                 ,");
			// sb.append(" B.EXT_REFERENCE AS EXT_REFERENCE ,");
			sb.append(" A.TIMESTAMP                AS TNX_DATE                ,");
			sb.append(" A.TOTAL_AMOUNT             AS TOTAL_AMOUNT            ,");
			sb.append(" A.TOTAL_AMOUNT_PAYCUR      AS TOTAL_AMOUNT_PAYCUR     ,");
			sb.append(" A.TOTAL_FARE_AMOUNT        AS TOTAL_FARE_AMOUNT       ,");
			sb.append(" A.TOTAL_FARE_AMOUNT_PAYCUR AS TOTAL_FARE_AMOUNT_PAYCUR,");
			sb.append(" A.TOTAL_TAX_AMOUNT         AS TOTAL_TAX_AMOUNT        ,");
			sb.append(" A.TOTAL_TAX_AMOUNT_PAYCUR  AS TOTAL_TAX_AMOUNT_PAYCUR ,");
			sb.append(" A.TOTAL_SUR_AMOUNT         AS TOTAL_SUR_AMOUNT        ,");
			sb.append(" A.TOTAL_SUR_AMOUNT_PAYCUR  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
			sb.append(" A.TOTAL_MOD_AMOUNT         AS TOTAL_MOD_AMOUNT        ,");
			sb.append(" A.TOTAL_MOD_AMOUNT_PAYCUR  AS TOTAL_MOD_AMOUNT_PAYCUR");
			// Updated
			// sb.append(" A.TOTAL_MOD_AMOUNT_PAYCUR AS TOTAL_MOD_AMOUNT_PAYCUR ,");
			// sb.append(" A.PAYMENT_CURRENCY_CODE AS PAYMENT_CURRENCY_CODE");
			sb.append(" 	FROM T_PAX_TXN_BREAKDOWN_SUMMARY A, T_PAX_TRANSACTION B");
			sb.append(" WHERE A.PAX_TXN_ID = B.TXN_ID");
			sb.append(" AND (B.PAYMENT_CARRIER_CODE IS NULL OR  B.PAYMENT_CARRIER_CODE='"
					+ AppSysParamsUtil.getDefaultCarrierCode() + "') ");
			if (reportsSearchCriteria.getBillingPeriod() == 1) {
				sb.append(" and A.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
			} else {
				startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
				sb.append(" and A.TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
			}
			sb.append(" AND( B.AGENT_CODE         ='" + reportsSearchCriteria.getAgentCode() + "' )");
			sb.append(" AND B.SALES_CHANNEL_CODE <> 11");

			sb.append(" UNION ALL");

			sb.append(" SELECT P.AGENT_CODE      AS AGENT_CODE              ,");
			sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
			sb.append(" P.USER_ID               AS USER_ID                 ,");
			// sb.append(" '' AS EXT_REFERENCE ,");
			sb.append(" P.TXN_TIMESTAMP         AS TNX_DATE                ,");
			sb.append(" P.AMOUNT           		AS TOTAL_AMOUNT            ,");
			sb.append(" P.AMOUNT_PAYCUR        	AS TOTAL_AMOUNT_PAYCUR     ,");
			sb.append(" P.AMOUNT           		AS TOTAL_FARE_AMOUNT       ,");// FIXME put the real value- when the
																				// breakdown for ext payments are
																				// present
			sb.append(" P.AMOUNT_PAYCUR        	AS TOTAL_FARE_AMOUNT_PAYCUR,");// FIXME put the real
																				// TOTAL_FARE_AMOUNT_PAYCUR in the
																				// lcc when it is done
			sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
			sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
			sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
			sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
			sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
			sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR ");
			// UPDATED
			// sb.append(" 0 AS TOTAL_MOD_AMOUNT_PAYCUR ,");
			// sb.append(" P.PAYMENT_CURRENCY_CODE AS PAYMENT_CURRENCY_CODE");
			sb.append(" FROM T_PAX_EXT_CARRIER_TRANSACTIONS P");
			sb.append(" WHERE (P.AGENT_CODE         ='" + reportsSearchCriteria.getAgentCode() + "')");
			if (reportsSearchCriteria.getBillingPeriod() == 1) {
				sb.append(" and P.TXN_TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
			} else {
				startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
				sb.append(" and P.TXN_TIMESTAMP between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
			}
			sb.append(" AND P.SALES_CHANNEL_CODE <> 11");

			sb.append(" ) V , T_AGENT B");

			sb.append(" WHERE V.AGENT_CODE = B.AGENT_CODE");
			sb.append(" AND  V.NOMINAL_CODE  IN (19,25) ");
			// sb.append(" GROUP BY B.AGENT_CODE, B.AGENT_NAME, V.PAYMENT_CURRENCY_CODE , B.ADDRESS_LINE_1,
			// B.ADDRESS_LINE_2, B.ADDRESS_STATE_PROVINCE, V.EXT_REFERENCE");
			sb.append(" GROUP BY B.AGENT_CODE, B.AGENT_NAME, B.ADDRESS_LINE_1, B.ADDRESS_LINE_2, B.ADDRESS_STATE_PROVINCE ");
			
			sb.append(" UNION ALL");
			
			sb.append(" SELECT  SUM(at.amount) AS invoice_amount, ");
			sb.append(" 		SUM(at.amount_local) AS invoice_amount_paycurr, ");
			sb.append(" 		0 AS total_fare, ");
			sb.append(" 		0 AS total_fare_amount_paycurr, ");
			sb.append(" 		0 AS total_tax, ");
			sb.append(" 		0 AS total_tax_paycurr, ");
			sb.append(" 		0 AS total_sur, ");
			sb.append(" 		0 AS total_sur_paycurr, ");
			sb.append(" 		0 AS total_mod, ");
			sb.append(" 		0 AS total_mod_paycurr, ");
			sb.append(" 		b.agent_code AS agent_code, ");
			sb.append(" 		b.agent_name AS agent_name, ");
			sb.append(" 		b.address_line_1 AS address_line_1, ");
			sb.append(" 		b.address_line_2 AS address_line_2, ");
			sb.append(" 		b.address_state_province AS address_stat_province, ");
			sb.append("         p.entity_code as entity ");
			sb.append(" FROM ");
			sb.append("			t_agent_transaction at, t_agent b, t_product p ");
			sb.append(" WHERE ");
			sb.append(" 		at.agent_code = '"+ reportsSearchCriteria.getAgentCode() +"' ");
			sb.append("			AND  p.entity_code = '"+ reportsSearchCriteria.getEntity() +"' ");
			sb.append("         AND  p.product_type = at.product_type ");
			sb.append("			AND at.agent_code = b.agent_code ");
			sb.append("			AND at.nominal_code IN (19,20) ");
			sb.append("			AND at.product_type IN ('G') ");
			sb.append("			AND at.invoice_separated = 'Y' ");
			
			if (reportsSearchCriteria.getBillingPeriod() == 1) {
				sb.append(" and at.tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
			} else {
				startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
				sb.append(" and at.tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
			}
			
			sb.append(" GROUP BY ");
			sb.append(" 	b.agent_code, b.agent_name, b.address_line_1, b.address_line_2, b.address_state_province, p.entity_code ");
			sb.append(" ) BREAK");
			
			sb.append(" WHERE break.entity ='" + reportsSearchCriteria.getEntity() + "' ");

			// ----------------------
			/*
			 * sb.append("( select "); sb.append(
			 * "(-1 * sum(total_amount)) invoice_amount ,(-1 * sum(brsum.total_amount_paycur)) invoice_amount_paycurr, "
			 * ); sb.append(
			 * "(-1 * sum(brsum.total_fare_amount)) total_fare, (-1 * sum (brsum.total_fare_amount_paycur)) total_fare_amount_paycurr,"
			 * ); sb.
			 * append("(-1 * sum(total_tax_amount)) total_tax, (-1 * sum(total_tax_amount_paycur)) total_tax_paycurr, "
			 * ); sb.
			 * append("(-1 * sum(total_sur_amount)) total_sur, (-1 * sum(total_sur_amount_paycur)) total_sur_paycurr, "
			 * ); sb.
			 * append("(-1 * sum(total_mod_amount)) total_mod, (-1 * sum(total_mod_amount_paycur)) total_mod_paycurr, "
			 * ); sb.append(
			 * "payment_currency_code, b.agent_code, b.agent_name,b.address_line_1, b.address_line_2,b.address_state_province"
			 * ); sb.append(", pax.ext_reference ");//AARESAA:3128
			 * sb.append("FROM t_pax_txn_breakdown_summary brsum, t_pax_transaction pax, t_agent b ");
			 * sb.append("where brsum.pax_txn_id = pax.txn_id "); sb.append("and   pax.agent_code = b.agent_code ");
			 * sb.append(" and   b.agent_code  = '"+reportsSearchCriteria.getAgentCode()+"'");
			 * sb.append(" and   pax.NOMINAL_CODE in (19,25) ");
			 * 
			 * if(reportsSearchCriteria.getBillingPeriod()==1){
			 * sb.append(" and brsum.timestamp between to_Date('"+startDate+"','dd-mm-yyyy HH24:mi:ss') ");
			 * sb.append("   and to_Date('"+endDate+"','dd-mm-yyyy HH24:mi:ss') "); }else {
			 * startDate="16-"+reportsSearchCriteria.getMonth()+"-"+reportsSearchCriteria.getYear()+"  00:00:00";
			 * sb.append(" and brsum.timestamp between to_Date('"+startDate+"','dd-mm-yyyy HH24:mi:ss') ");
			 * sb.append(" and last_day(to_date('"+endPeriod+"','mm-yyyy HH24:MI:SS'))"); }
			 * 
			 * sb.append(" group by b.agent_code, b.agent_name, "); sb.append(
			 * "			payment_currency_code, b.address_line_1, b.address_line_2,b.address_state_province,pax.ext_reference "
			 * );//AARESAA:3128 sb.append(")  break ");
			 */
			// -----------------------------------

			sb.append(")  inoice, ");

			sb.append(
					"( select agent_code ,nvl(sum(amount), 0) holi_amount, nvl(sum (amount_local), 0) holi_amount_local from t_agent_transaction  ");
			sb.append("    where agent_code  = '" + reportsSearchCriteria.getAgentCode() + "'");
			if (reportsSearchCriteria.getBillingPeriod() == 1) {
				sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append("   and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
			} else {
				startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
				sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
			}
			sb.append("       and nominal_code in (10,11) ");
			sb.append("       GROUP by agent_code ");
			sb.append(" ) holiday, ");

			sb.append(" ( ")
					.append(" 	select agent_code ,nvl(sum(amount), 0) goquo_amount, nvl(sum (amount_local), 0) goquo_amount_local ")
					.append("	from t_agent_transaction ")
					.append("	where agent_code = '" + reportsSearchCriteria.getAgentCode() + "' ")
					.append("		and nominal_code in ( 19, 20 ) ").append("		and product_type   in ( 'G' ) ")
					.append("       and invoice_separated = 'N' ");

			if (reportsSearchCriteria.getBillingPeriod() == 1) {
				sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and to_Date('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
			} else {
				startDate = "16-" + reportsSearchCriteria.getMonth() + "-" + reportsSearchCriteria.getYear() + "  00:00:00";
				sb.append(" and tnx_date between to_Date('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" and last_day(to_date('" + endPeriod + "','mm-yyyy HH24:MI:SS'))");
			}

			sb.append("		group by agent_code ");

			sb.append(" ) goquo ");

			sb.append(", ( select goquo, mco ");
			sb.append("		from ( ");
			sb.append("		select product_type from t_product ");
			sb.append("		) pivot ");
			sb.append("		( count(1) for product_type ");
			sb.append("			in ('G' as goquo, 'M' as mco) ");
			sb.append("		) ");
			sb.append("	) product ");

			sb.append(" where inoice.agent_code = holiday.agent_code(+) ");
			sb.append(" and inoice.agent_code = goquo.agent_code(+) ");
		}
		return sb.toString();
	}

	public String getInvoiceSummaryAttachmentByTransaction(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();

		String invDate = reportsSearchCriteria.getDateRangeFrom();
		String agentCode = reportsSearchCriteria.getAgentCode();
		String invoiceNo = reportsSearchCriteria.getInvoiceNumber();
		String pnr = reportsSearchCriteria.getPnr();

		sb.append(" SELECT  inv.invoice_number, TO_CHAR (inv.invoice_date,'DD/mm/YYYY HH24:mi:ss') invoice_date, ");
		sb.append(
				" TO_CHAR (inv.invioce_period_from, 'DD/mm/YYYY') invioce_period_from, TO_CHAR (inv.invoice_period_to, 'DD/mm/YYYY') invoice_period_to, ");
		sb.append(
				" TO_CHAR (res.booking_timestamp,'DD/mm/YYYY HH24:mi:ss') booked_date , (rCon.c_first_name || ' ' || rCon.c_last_name) as res_name, inv.invoice_amount_local invoice_amount, inv.pnr");
		sb.append(
				" , ag.account_code, (fs.segment_code || ' ' || TO_CHAR (fs.est_time_departure_local, 'DDMonYY') ) as route, SUM(inv.invoice_amount_local) as total,ag.agent_name ");
		sb.append(
				" FROM t_invoice inv, t_agent ag, t_reservation res, t_reservation_contact rCon, t_pnr_segment seg, t_flight_segment fs ");
		sb.append(" WHERE res.pnr=rCon.pnr AND inv.agent_code = ag.agent_code ");
		if (invDate != null) {
			sb.append(" and inv.invoice_date = to_date('");
			sb.append(invDate);
			sb.append("','mm/dd/yyyy hh24:mi:ss') ");
		}
		if (isNotEmptyOrNull(invoiceNo)) {
			sb.append(" AND inv.invoice_number = '" + invoiceNo.trim() + "'");
		}
		if (isNotEmptyOrNull(agentCode)) {
			sb.append(" AND inv.agent_code = '");
			sb.append(agentCode.trim());
			sb.append("'");
		}
		if (isNotEmptyOrNull(pnr)) {
			sb.append(" AND inv.invoice_number = '" + pnr.trim() + "'");
		}
		sb.append(
				" and inv.pnr = res.pnr and res.pnr = seg.pnr and seg.flt_seg_id = fs.flt_seg_id and  res.status <> 'CNX' and seg.status <> 'CNX' ");
		sb.append(
				" group by inv.invoice_number, inv.invoice_date,inv.invioce_period_from,inv.invoice_period_to,res.booking_timestamp,rCon.c_first_name, ");
		sb.append(
				"  rCon.c_last_name, inv.invoice_amount_local, inv.pnr,ag.account_code,fs.segment_code,fs.est_time_departure_local,ag.agent_name ");

		return sb.toString();
	}

	public String getSceduleDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();
		String strOrigin = reportsSearchCriteria.getSectorFrom();
		String strDestination = reportsSearchCriteria.getSectorTo();
		String strOpType = reportsSearchCriteria.getOperationType();
		String strStatus = reportsSearchCriteria.getScheduleStatus();
		StringBuilder sb = new StringBuilder();

		String timeZone = reportsSearchCriteria.getTimeZone();
		String timePostfix = "";

		if (timeZone.equals("Zulu")) {
			timePostfix = "Zulu";
		} else {
			timePostfix = "Local";
		}

		Integer startDayOfWeek = AppSysParamsUtil.getOffsetFromStandardStartDayOfWeek();
		sb.append(" " + "SELECT distinct flight_number,");
		if (timeZone.equals("Zulu")) {
			sb.append(" start_date, stop_date, ");
		} else {
			sb.append(" f_calculate_localdatetime(origin,start_date) as start_date,");
			sb.append(" f_calculate_localdatetime(origin,stop_date) as stop_date,");
		}
		sb.append(
				" concat(SUBSTR(sub.D0, 0, 2), concat(SUBSTR(sub.D1,0,2), concat(SUBSTR(sub.D2,0,2), concat(SUBSTR(sub.D3,0,2), concat(SUBSTR(sub.D4,0,2),concat(SUBSTR(sub.D5,0,2),SUBSTR(sub.D6,0,2))))))) Frequency,");
		sb.append(" SEGMENT_CODE, ");
		sb.append(" TO_CHAR(dept" + timePostfix + " ,'HH24:MI') AS dept_time, " + " TO_CHAR(arrival" + timePostfix);
		sb.append(" ,'HH24:MI') AS arrival_time, ");
		sb.append(" Seg_Leg_code, " + " flight_Segment_code, " + " segtime" + timePostfix + " as  seg_ArrDep_time,");
		sb.append(" OPT_TYPE," + " ASK," + " OVER_LAP," + " NO_OF_FLIGHTS," + " schedule_id, LEG_NUMBER," + " BUILD_STATUS ");
		
		sb.append("		FROM ");
		sb.append("		( ");
		sb.append("			SELECT a.flight_number, a.start_date,a.stop_date, a.origin, ");
		if (timeZone.equals("Zulu")) {
			sb.append("				decode(a.frequency_d0, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ startDayOfWeek + ")), '__') D0, ");
			sb.append("				decode(a.frequency_d1, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D1, ");
			sb.append("				decode(a.frequency_d2, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D2, ");
			sb.append("				decode(a.frequency_d3, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D3, ");
			sb.append("				decode(a.frequency_d4, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D4, ");
			sb.append("				decode(a.frequency_d5, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D5, ");
			sb.append("				decode(a.frequency_d6, 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D6, ");
		} else {
			sb.append("		decode(case datediff when -1 then a.frequency_d1 when 1 then a.frequency_d6 else a.frequency_d0 end");
			sb.append("         , 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = " + startDayOfWeek
					+ ")), '__') D0, ");
			sb.append("		decode(case datediff when -1 then a.frequency_d2 when 1 then a.frequency_d0 else a.frequency_d1 end");
			sb.append(", 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D1, ");
			sb.append("		decode(case datediff when -1 then a.frequency_d3 when 1 then a.frequency_d1 else a.frequency_d2 end");
			sb.append(", 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D2, ");
			sb.append("		decode(case datediff when -1 then a.frequency_d4 when 1 then a.frequency_d2 else a.frequency_d3 end");
			sb.append(", 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D3, ");
			sb.append("		decode(case datediff when -1 then a.frequency_d5 when 1 then a.frequency_d3 else a.frequency_d4 end");
			sb.append(", 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D4, ");
			sb.append("		decode(case datediff when -1 then a.frequency_d6 when 1 then a.frequency_d4 else a.frequency_d5 end");
			sb.append(", 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D5, ");
			sb.append("		decode(case datediff when -1 then a.frequency_d0 when 1 then a.frequency_d5 else a.frequency_d6 end");
			sb.append(", 1, initcap((SELECT d.day FROM t_day_of_week d where d.day_number = "
					+ ((7 - ++startDayOfWeek > 0) ? startDayOfWeek : Math.abs(7 - startDayOfWeek)) + ")), '__') D6, ");
		}
		sb.append("				seg.segment_code	AS SEGMENT_CODE, to_char(leg.est_time_departure_zulu, 'HH24:MI') AS D_TIMEZULU, ");
		sb.append("				opt.operation_type_description AS OPT_TYPE,");
		sb.append("				decode(a.available_seat_kilometers,null,0,a.available_seat_kilometers /1000) AS ASK, ");
		sb.append("				decode(a.overlap_schedule_id,null,'N','Y') as OVER_LAP , ");
		sb.append("				decode(a.number_of_departures,null,0,a.number_of_departures) AS NO_OF_FLIGHTS , ");
		sb.append("				a.schedule_id, a.schedule_build_status_code AS BUILD_STATUS, ");
		sb.append("				flightTime.flight_id as flightId,  ");
		sb.append("				flightTime.est_time_departure_local as deptLocal,   ");
		sb.append("				flightTime.est_time_departure_zulu as deptZulu,  ");
		sb.append("				flightTime.est_time_arrival_local as arrivalLocal,  ");
		sb.append("				flightTime.est_time_arrival_zulu as arrivalZulu,  ");
		sb.append("				FLIGHT_SEG_DETAILs.Seg_Leg_code as Seg_Leg_code,   ");
		sb.append("				FLIGHT_SEG_DETAILs.Segment_code as flight_Segment_code,  ");
		sb.append("				FLIGHT_SEG_DETAILs.timeZulu as segtimeZulu,   ");
		sb.append("				FLIGHT_SEG_DETAILs.timeLocal as segtimeLocal,  ");
		sb.append("				FLIGHT_SEG_DETAILs.leg_number AS LEG_NUMBER    ");
		sb.append(
				"		FROM 	t_flight_schedule a, t_operation_type opt, t_flight_schedule_leg leg,  T_FLIGHT_SCHEDULE_SEGMENT seg, ");
		sb.append("			(select fs.flight_id,   					");
		sb.append("		            f.flight_number,					");
		sb.append("		            f.schedule_id,						");
		sb.append("		            fs.segment_code,					");
		sb.append("		            fs.est_time_departure_local,		");
		sb.append("		            fs.est_time_departure_zulu,			");
		sb.append("		            fs.est_time_arrival_local,			");
		sb.append("		            fs.est_time_arrival_zulu,			");
		sb.append("					trunc(fs.est_time_departure_local) - trunc(fs.est_time_departure_zulu) datediff");
		sb.append("		    from t_flight_segment fs,					");
		sb.append("		    t_flight_schedule a,						");
		sb.append("		    t_flight f									");
		sb.append("		    where f.flight_number = a.flight_number		");
		sb.append("		    and f.schedule_id = a.schedule_id			");
		sb.append("		    and f.flight_id = fs.flight_id				");
		sb.append("		    ) flightTime,								");

		sb.append("	(select distinct FLIGHT_SEG_DETAIL.Seg_code as Seg_Leg_code, 		");
		sb.append(" FLIGHT_SEG_DETAIL.flt_segcode as Segment_code, 						");
		sb.append("                FLIGHT_SEG_DETAIL.depTime_Local || ' / ' || arrivalTime_Local as timeLocal,		");
		sb.append("                FLIGHT_SEG_DETAIL.depTime_zulu  || ' / ' || arrivalTime_Zulu as timeZulu,			");
		sb.append("                FLIGHT_SEG_DETAIL.leg_number, 													");
		sb.append("                FLIGHT_SEG_DETAIL.schedule_id,													");
		sb.append("                FLIGHT_SEG_DETAIL.flight_id,													");
		sb.append("                FLIGHT_SEG_DETAIL.flight_number													");
		sb.append(" from 																		");
		sb.append("      (																							");
		sb.append("            select flt_leg.schedule_id, tfseg.flt_seg_id, tfseg.segment_code as Seg_code, "
				+ "	flt_leg.segment_code as flt_segcode, flt_leg.route_id, flt_leg.leg_number, ");
		sb.append("    TO_CHAR(tfseg.est_time_departure_local,'HH24:MI') AS depTime_Local, ");
		sb.append(" TO_CHAR(tfseg.est_time_arrival_local,'HH24:MI') AS arrivalTime_Local, ");
		sb.append(" TO_CHAR(tfseg.est_time_departure_Zulu,'HH24:MI') AS depTime_Zulu, ");
		sb.append(" TO_CHAR(tfseg.est_time_arrival_Zulu,'HH24:MI') AS arrivalTime_Zulu, ");
		sb.append(" tfseg.est_time_departure_Zulu, tf.flight_id,  tf.flight_number ");
		sb.append(" from t_flight_schedule_segment tfss, ");
		sb.append(" t_flight tf, ");
		sb.append(" t_flight_segment tfseg, ");
		sb.append(" (select fss.segment_code,  ");
		sb.append(" fssl.schedule_id, ");
		sb.append(" fssl.origin,  ");
		sb.append(" fssl.destination, ");
		sb.append(" fssl.route_id,  ");
		sb.append(" fssl.leg_number,  ");
		sb.append(" TO_CHAR(fssl.est_time_departure_zulu,'HH24:MI') AS dep,  ");
		sb.append(" TO_CHAR(fssl.est_time_arrival_zulu,'HH24:MI') AS arrival ");
		sb.append(" from t_flight_schedule_segment fss, ");
		sb.append(" t_flight_schedule_leg fssl, ");
		sb.append(" t_flight_schedule tfs ");
		sb.append(" where fss.schedule_id = fssl.schedule_id ");
		sb.append(" and tfs.schedule_id = fss.schedule_id ");
		// sb.append(" --and length(fss.segment_code) >7 ");
		sb.append(" order by fssl.schedule_id asc, fssl.leg_number asc)  flt_leg ");
		sb.append(" where tf.schedule_id = tfss.schedule_id ");
		sb.append(" and tfseg.flight_id = tf.flight_id ");
		sb.append(" and tfseg.segment_code = tfss.segment_code ");
		sb.append(" and tfss.schedule_id = flt_leg.schedule_id ");
		sb.append(" and tfss.segment_code = flt_leg.route_id ");
		sb.append(" and tfseg.est_time_departure_" + timePostfix + " between TO_DATE('" + strFromDate
				+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
		sb.append(" order by tfseg.est_time_departure_Zulu ");
		sb.append(" )FLIGHT_SEG_DETAIL ");
		sb.append(" ) FLIGHT_SEG_DETAILs ");

		sb.append("		WHERE opt.operation_type_id = a.operation_type_id ");
		sb.append("		AND leg.schedule_id = a.schedule_id ");
		sb.append("		AND leg.leg_number = 1 ");
		sb.append(" 	AND seg.schedule_id = a.schedule_id ");
		sb.append("		AND a.start_date <  (TO_DATE('" + strToDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))");
		sb.append("		AND a.stop_date > (TO_DATE('" + strFromDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))");

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append(" AND a.flight_number = '" + strFlightNo + "'");
		}
		if (isNotEmptyOrNull(strOpType)) {
			sb.append(" AND  a.operation_type_id = '" + strOpType + "'");
		}
		if (isNotEmptyOrNull(strOrigin)) {
			sb.append(" AND a.origin = '" + strOrigin + "'");
		}
		if (isNotEmptyOrNull(strDestination)) {
			sb.append(" AND a.destination = '" + strDestination + "'");
		}
		if (isNotEmptyOrNull(strStatus)) {
			sb.append(" AND a.schedule_status_code = '" + strStatus + "'");
		}

		sb.append("  and flightTime.flight_number = a.flight_number   ");
		sb.append("	 and flightTime.schedule_id = a.schedule_id   ");

		sb.append("  and flightTime.est_time_departure_" + timePostfix + " between TO_DATE('" + strFromDate
				+ " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') and TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')  ");
		sb.append("  and flightTime.segment_code = seg.segment_code  ");

		sb.append("   and FLIGHT_SEG_DETAILs.schedule_id = a.schedule_id   ");
		sb.append("   and FLIGHT_SEG_DETAILs.Segment_code = seg.segment_code  ");
		sb.append("   AND FLIGHT_SEG_DETAILs.flight_id =  flightTime.flight_id ");
		sb.append("   AND FLIGHT_SEG_DETAILs.flight_number =  flightTime.flight_number ");

		sb.append("		AND length(seg.segment_code) = ");
		sb.append("   (SELECT max(length(sseg.segment_code)) ");
		sb.append("       FROM T_FLIGHT_SCHEDULE_SEGMENT sseg ");
		sb.append("       WHERE seg.schedule_id = sseg.schedule_id) ");
		sb.append("	) sub");
		sb.append(" order by trunc(start_date), trunc(stop_date), schedule_id, LEG_NUMBER");
		return sb.toString();
	}

	public String getIssuedVoucherDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strVoucherName = reportsSearchCriteria.getVoucherName();
		String strFirstName = reportsSearchCriteria.getFirstName();
		String strLastName = reportsSearchCriteria.getLastName();
		String strStatus = reportsSearchCriteria.getVoucherStatus();

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT distinct voucher_id,voucher_template_id, "
				+ "INITCAP(T_VOUCHER.pax_first_name) || ' ' ||INITCAP(T_VOUCHER.pax_last_name)NAME, "
				+ " round(amount,2) as AMOUNT");

		sb.append(" FROM T_VOUCHER");

		sb.append("	WHERE issued_time <  (TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))");
		sb.append("	AND issued_time > (TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))");

		if (isNotEmptyOrNull(strVoucherName)) {
			sb.append(" AND voucher_id = '" + strVoucherName + "'");
		}
		if (isNotEmptyOrNull(strFirstName)) {
			sb.append(" AND  upper(pax_first_name) = '" + strFirstName.toUpperCase() + "'");
		}
		if (isNotEmptyOrNull(strLastName)) {
			sb.append(" AND upper(pax_last_name) = '" + strLastName.toUpperCase() + "'");
		}
		if (isNotEmptyOrNull(strStatus)) {
			sb.append(" AND T_VOUCHER.status = '" + strStatus + "'");
		}
		sb.append(" ORDER by voucher_id");
		return sb.toString();
	}

	public String getRedeemedVoucherDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strVoucherName = reportsSearchCriteria.getVoucherName();

		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT distinct T_VOUCHER.voucher_id, "
				+ " INITCAP(T_VOUCHER.pax_first_name) || ' ' ||INITCAP(T_VOUCHER.pax_last_name)NAME, "
				+ " round(ABS(T_VOUCHER.redeem_amount),2) as REDEEM_AMOUNT, " + " T_PNR_PASSENGER.pnr ");

		sb.append(" FROM T_VOUCHER, T_VOUCHER_REDEEM, T_PAX_TRANSACTION , T_PNR_PASSENGER, T_PAX_EXT_CARRIER_TRANSACTIONS ");

		sb.append("	WHERE issued_time <  (TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))");
		sb.append("	AND issued_time > (TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))");

		if (isNotEmptyOrNull(strVoucherName)) {
			sb.append(" AND T_VOUCHER.voucher_id = '" + strVoucherName + "'");
		}

		sb.append(" AND T_VOUCHER.status = 'U'");
		sb.append(" AND T_VOUCHER_REDEEM.voucher_id = T_VOUCHER.voucher_id");
		sb.append(" AND ((T_VOUCHER_REDEEM.txn_id = T_PAX_TRANSACTION.txn_id");
		sb.append(" AND T_PAX_TRANSACTION.pnr_pax_id = T_PNR_PASSENGER.pnr_pax_id)");
		sb.append(" OR (T_VOUCHER_REDEEM.pax_ext_carrier_txn_id = T_PAX_EXT_CARRIER_TRANSACTIONS.pax_ext_carrier_txn_id");
		sb.append(" AND T_PAX_EXT_CARRIER_TRANSACTIONS.pnr_pax_id = T_PNR_PASSENGER.pnr_pax_id");
		sb.append(" AND T_PAX_EXT_CARRIER_TRANSACTIONS.nominal_code = ");
		sb.append(ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode());
		sb.append(" ) )");
		sb.append(" order by voucher_id");
		return sb.toString();
	}

	public String getFlightDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();
		String strOrigin = reportsSearchCriteria.getSectorFrom();
		String strDestination = reportsSearchCriteria.getSectorTo();
		String strOpType = reportsSearchCriteria.getOperationType();
		String strStatus = reportsSearchCriteria.getFlightStatus();
		String strModel = reportsSearchCriteria.getModel();
		String strTime = reportsSearchCriteria.getTimeZone();
		StringBuilder sb = new StringBuilder();
		String strTimeField = "";
		if (strTime.equals("Zulu")) {
			strTimeField = "Zulu";
		} else {
			strTimeField = "Local";
		}

		sb.append(
				"SELECT opt.operation_type_description,a.flight_number,  to_date(a.departure_date, 'DD-MM-YYYY') AS  depatredate, ");
		sb.append(" to_char(SegmentDetails.est_time_departure_" + strTimeField + ",'HH24:MI') AS dept_time, ");
		sb.append(" to_char(SegmentDetails.est_time_arrival_" + strTimeField + ",'HH24:MI') AS arrival_time, ");
		sb.append("SegmentDetails.segment_code, a.model_number,a.status, ");
		sb.append("SegmentLegDetails.leg_segment_code as leg_segment_code, ");
		sb.append("SegmentLegDetails.arrDep_time_" + strTimeField + " as arrDept_time, ");
		sb.append("decode(a.available_seat_kilometers,null,0,a.available_seat_kilometers /1000) AS ASK, ");
		sb.append("decode(a.overlap_flight_instance_id,null,'N','Y') as OVER_LAP , ");
		sb.append("sum(sa.allocated_seats + sa.oversell_seats - sa.curtailed_seats) as SEATS_ALLOC, ");
		sb.append("sum(sa.sold_seats) as SEATS_SOLD,  ");
		sb.append("sum(sa.on_hold_seats) as SEATS_ONHOLD, ");
		sb.append("a.flight_id, SegmentLegDetails.ext_dept_time ");
		sb.append("FROM t_flight a, t_operation_type opt, T_FLIGHT_SEGMENT seg, t_fcc_seg_alloc sa, ");
		sb.append("(SELECT seg.flight_id, seg.segment_code , seg.est_time_departure_zulu, ");
		sb.append("seg.est_time_departure_local, seg.est_time_arrival_zulu, est_time_arrival_local 	 ");
		sb.append("FROM T_FLIGHT_SEGMENT seg ");
		sb.append("WHERE length(seg.segment_code) = ");
		sb.append("(SELECT max(length(sseg.segment_code)) ");
		sb.append("FROM T_FLIGHT_SEGMENT sseg ");
		sb.append("WHERE seg.flight_id = sseg.flight_id)) SegmentDetails, ");

		sb.append(" (select fseg.flight_id, ");
		sb.append("		        fseg.segment_code as leg_segment_code,  ");
		sb.append(
				" TO_CHAR(fseg.est_time_departure_local ,'HH24:MI') || ' / ' || TO_CHAR(fseg.est_time_arrival_local , 'HH24:MI') as arrDep_time_local, ");
		sb.append(
				" TO_CHAR(fseg.est_time_departure_zulu, 'HH24:MI') || ' / ' || TO_CHAR(fseg.est_time_arrival_zulu, 'HH24:MI') as arrDep_time_zulu, fseg.est_time_departure_zulu as ext_dept_time  ");
		sb.append(" from t_flight_segment fseg ");
		sb.append(" where LENGTH(fseg.segment_code) = 7  ");
		sb.append(" ) SegmentLegDetails  ");

		sb.append("WHERE 	a.operation_type_id = opt.operation_type_id  ");
		sb.append("AND 	seg.flight_id = a.flight_id   ");
		sb.append("AND sa.flt_seg_id = seg.flt_seg_id ");
		sb.append("and SegmentDetails.flight_id = a.flight_id ");

		if (strTime.equals("Zulu")) {
			sb.append("AND  SegmentDetails.est_time_departure_zulu BETWEEN ");
			sb.append(" TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')");

		} else {
			sb.append("AND     SegmentDetails.est_time_departure_local BETWEEN ");
			sb.append(" TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')  ");
			sb.append(" AND TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");

		}
		sb.append(" AND SegmentLegDetails.flight_id = a.flight_id ");
		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append("AND a.flight_number = '" + strFlightNo + "' ");
		}
		if (isNotEmptyOrNull(strOrigin)) {
			sb.append("AND a.origin = '" + strOrigin + "' ");
		}
		if (isNotEmptyOrNull(strDestination)) {
			sb.append("AND a.destination = '" + strDestination + "' ");
		}
		if (isNotEmptyOrNull(strStatus)) {
			sb.append("AND a.status = '" + strStatus + "'");
		}
		if (isNotEmptyOrNull(strOpType)) {
			sb.append("AND a.operation_type_id = '" + strOpType + "' ");
		}
		if (isNotEmptyOrNull(strModel)) {
			sb.append("AND a.model_number = '" + strModel + "' ");
		}

		sb.append(" GROUP BY a.flight_id,opt.operation_type_description, ");
		sb.append("a.flight_number, a.departure_date, ");
		sb.append("a.model_number,a.status, ");
		sb.append("decode(a.available_seat_kilometers,null,0,a.available_seat_kilometers /1000), ");
		sb.append("decode(a.overlap_flight_instance_id,null,'N','Y'), SegmentDetails.segment_code, ");
		sb.append(" SegmentDetails.est_time_departure_" + strTimeField + ", ");
		sb.append(" SegmentDetails.est_time_arrival_" + strTimeField + ", ");
		sb.append(" SegmentLegDetails.arrDep_time_" + strTimeField + ", ");
		sb.append(" SegmentLegDetails.leg_segment_code, SegmentLegDetails.ext_dept_time ");
		sb.append(" ORDER BY TRUNC(departure_date), a.departure_date,  a.flight_number, SegmentLegDetails.ext_dept_time ");
		// sb.append("WHERE BA.BOOKING_CODE = BC.BOOKING_CODE");

		return sb.toString();
	}

	public String getAvsSeatQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String strGdsCode = reportsSearchCriteria.getGdsCode();
		String strBcCode = reportsSearchCriteria.getBcCode();
		String strEventCode = reportsSearchCriteria.getEventCode();
		String strFlightNum = reportsSearchCriteria.getFlightNumber();
		String strDepTimeLocal = reportsSearchCriteria.getDepTimeLocal();
		String strSegmentCodeFrom = reportsSearchCriteria.getSegmentCodeFrom();
		String strSegmentCodeTo = reportsSearchCriteria.getSegmentCodeTo();

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT  G.GDS_CODE, BA.BOOKING_CODE, ");
		sb.append("PA.EVENT_CODE, ");
		sb.append("PA.SEG_AVAILABLE_SEATS, PA.BC_AVAILABLE_SEATS,");
		sb.append("SA.AVAILABLE_SEATS CUR_SEG_AVAILABLE_SEATS, BA.AVAILABLE_SEATS CUR_BC_AVAILABLE_SEATS, ");
		sb.append("F.FLIGHT_NUMBER, FS.EST_TIME_DEPARTURE_LOCAL, FS.SEGMENT_CODE ");
		sb.append("FROM T_FCC_SEG_BC_ALLOC BA, ");
		sb.append("T_PUBLISH_AVAILABILITY PA,");
		sb.append("T_BOOKING_CLASS BC, ");
		sb.append("T_FCC_SEG_ALLOC SA, ");
		sb.append("T_FLIGHT_SEGMENT FS, ");
		sb.append("T_FLIGHT F, ");
		sb.append("T_GDS G ");
		sb.append("WHERE PA.FCCSBA_ID = BA.FCCSBA_ID ");
		sb.append("AND BA.BOOKING_CODE = BC.BOOKING_CODE ");
		sb.append("AND SA.FLT_SEG_ID = FS.FLT_SEG_ID ");
		sb.append("AND FS.FLIGHT_ID = F.FLIGHT_ID ");

		if (isNotEmptyOrNull(strGdsCode)) {
			sb.append("AND G.GDS_ID = " + strGdsCode + " ");
		}
		if (isNotEmptyOrNull(strBcCode)) {
			sb.append("AND BC.BOOKING_CODE = '" + strBcCode + "' ");
		}
		if (isNotEmptyOrNull(strEventCode)) {
			sb.append("AND PA.EVENT_CODE = '" + strEventCode + "' ");
		}
		if (isNotEmptyOrNull(strDepTimeLocal)) {
			sb.append("AND FS.EST_TIME_DEPARTURE_LOCAL = TO_DATE('" + strDepTimeLocal + " 6:00:00 AM','DD/MM/YYYY HH:MI:SS AM')");
		}
		if (isNotEmptyOrNull(strFlightNum)) {
			sb.append("AND F.FLIGHT_NUMBER = '" + strFlightNum + "' ");
		}
		if (isNotEmptyOrNull(strSegmentCodeFrom)) {
			sb.append("AND FS.SEGMENT_CODE = '" + strSegmentCodeFrom + "/" + strSegmentCodeTo + "' ");
		}

		return sb.toString();
	}

	public String getEnplanementQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();

		sb.append(" SELECT to_char(b.est_time_departure_zulu, 'mm/dd/yyyy HH24:mi:ss') AS ");
		sb.append("  			departure_date, a.flight_number, b.segment_code, ");
		// sb.append(" count(distinct(decode(e.pax_type_code, 'AD', e.pnr_pax_id, null))) || ' \\ ' || ");
		sb.append(
				"             count(distinct(decode(e.pax_type_code, 'AD', e.pnr_pax_id,'CH', e.pnr_pax_id, null))) || ' \\ ' || ");
		sb.append(" 			count(distinct(decode(e.pax_type_code, 'IN', e.pnr_pax_id, null))) booked_load, ");
		sb.append(" 			count(distinct(decode(d.pax_status, 'N', e.pnr_pax_id, null)))nosho_count,  ");
		sb.append(" 			count(distinct(decode(d.pax_status, 'R',e.pnr_pax_id, null))) norec_count,  ");
		sb.append(" 			count(distinct(decode(d.pax_status, 'G', e.pnr_pax_id, null))) gosho_count, ");
		sb.append(" 			count(distinct(decode(d.pax_status, 'F', e.pnr_pax_id, null))) +  ");
		sb.append(" 			count(distinct(decode(d.pax_status, 'G',e.pnr_pax_id, null)))flown_count ");
		sb.append(" 		FROM t_flight a, t_flight_segment b, t_pnr_segment c, ");
		sb.append(" 				t_pnr_pax_fare_segment d, t_pnr_passenger e, t_pnr_pax_fare f ");
		sb.append(" 		WHERE b.est_time_departure_zulu BETWEEN to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ " 00:00:00','dd-mon-yyyy HH24:mi:ss') AND ");
		sb.append(
				" 				to_date('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy HH24:mi:ss') ");
		sb.append(" 		AND a.flight_id = b.flight_id ");

		if ((reportsSearchCriteria.getStation() != null) && (!reportsSearchCriteria.getStation().equals(""))) {
			sb.append(" 	AND upper(substr(b.segment_code, 0, 3)) = upper('" + reportsSearchCriteria.getStation() + "') ");
		}

		sb.append("			AND b.flt_seg_id = c.flt_seg_id ");
		sb.append(" 		AND c.pnr_seg_id = d.pnr_seg_id ");
		sb.append(" 		AND c.status ='CNF' ");
		sb.append(" 		AND e.pnr = c.pnr ");
		sb.append(" 		AND e.pnr_pax_id = f.pnr_pax_id ");
		sb.append(" 		AND e.status IN ('CNF','OHD') ");
		sb.append(" 		AND f.ppf_id = d.ppf_id ");

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append("       AND a.FLIGHT_NUMBER = '" + strFlightNo + "' ");
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" b.SEGMENT_CODE ", segmentCodes, false) + " ");
		}

		sb.append(" 	GROUP BY b.est_time_departure_zulu, a.flight_number, b.segment_code ");
		sb.append(" 	ORDER BY b.est_time_departure_zulu, a.flight_number, b.segment_code ");

		return sb.toString();
	}

	public String getAgentGSADataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		String strAgentType = reportsSearchCriteria.getAgentType();
		if (strAgentType != null) {
			strAgentType = strAgentType.trim();
		}
		StringBuilder sb = new StringBuilder();
		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.AGENT_GSA_SUMMARY)) {
			sb.append("SELECT agent_code, agent_name, agent_type_Code, ");
			sb.append("territory_code, country_code, country_name, station_code, ");
			sb.append("TO_NUMBER(SUBSTR(PARENT,1,INSTR(PARENT,'|')-1)) credit_limit, ");
			sb.append("bank_guarantee_cash_advance, TO_NUMBER(SUBSTR(PARENT,INSTR(PARENT,'|') +1)) creditBalance, ");
			sb.append("email_id, status, account_code, has_credit_limit ");
			sb.append("FROM (select agent.agent_code ,agent.agent_name,agent.agent_type_Code,");
			sb.append(" 	agent.territory_code,station.country_code, country.country_name,");
			sb.append(" 	agent.station_code, F_GET_SHARED_CL_FOR_AGENT(agent.agent_code) PARENT,");
			sb.append(" 	agent.bank_guarantee_cash_advance,");
			sb.append("     agent.email_id,agent.status,agent.account_code,agent.has_credit_limit  ");
			sb.append(" 	from t_agent agent, t_station station ,t_country country,t_agent_summary creditBal");
			sb.append(" 	where station.station_code=agent.station_code and");
			sb.append(" 	country.country_code=station.country_code ");
			// sb.append(" AND agent.agent_code = creditBal.agent_code(+) ");
			sb.append("	 AND agent.agent_code = creditBal.agent_code ");
			if (strAgentType != null && strAgentType.equals("All")) {
				sb.append(" 	and agent.AGENT_TYPE_CODE in('" + strAgentType + "')  ");
			}

			if (reportsSearchCriteria.getStation() != null && !reportsSearchCriteria.getStation().equals("")) {
				sb.append(" 	and agent.station_code='" + reportsSearchCriteria.getStation() + "' ");
			}
			if (reportsSearchCriteria.getTerritory() != null && !reportsSearchCriteria.getTerritory().equals("")) {
				sb.append(" and	agent.territory_code='" + reportsSearchCriteria.getTerritory() + "' ");
			}
			if (singleDataValue != null && !singleDataValue.equals("''")) {
				sb.append(" 	and  ");
				sb.append(ReportUtils.getReplaceStringForIN("agent.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
			}

			if (reportsSearchCriteria.getCountryOfResidence() != null
					&& !reportsSearchCriteria.getCountryOfResidence().equals("")) {
				sb.append(" and	station.country_code='" + reportsSearchCriteria.getCountryOfResidence() + "'  ");
			}
			if (reportsSearchCriteria.getStatus() != null && !reportsSearchCriteria.getStatus().equals("")) {
				sb.append(" and	agent.status='" + reportsSearchCriteria.getStatus() + "'  ");
			}
			if (reportsSearchCriteria.getDateRangeFrom() != null && !reportsSearchCriteria.getDateRangeFrom().equals("")
					&& reportsSearchCriteria.getDateRangeTo() != null && !reportsSearchCriteria.getDateRangeTo().equals("")) {
				sb.append(" and	agent.CREATED_DATE BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
						+ " 00:00:00', 'dd-mon-yyyy HH24:mi:ss') ");
				sb.append("     AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ " 23:59:59', 'dd-mon-yyyy HH24:mi:ss') ");
			}

			// sb.append(" group by  agent.agent_code ,agent.agent_name,agent.agent_type_Code,");
			// sb.append(" 	agent.territory_code,station.country_code, country.country_name,");
			// sb.append(" 	agent.station_code, ");
			// sb.append("	agent.credit_limit,creditBal.avilable_credit,");
			// sb.append("  agent.bank_guarantee_cash_advance, agent.email_id, agent.status ");
			if (reportsSearchCriteria.getSortByColumnName() != null
					&& ("AGENT_NAME").equals(reportsSearchCriteria.getSortByColumnName().trim())) {
				sb.append("	order by agent.agent_name");
			} else {
				sb.append("	order by agent.agent_code");
			}
			sb.append(" )");

		} else {

			// credit Details
			sb.append(" select  TO_CHAR(agCrHis.update_date, 'DD/mm/YYYY') update_date,agCrHis.agent_code,");
			sb.append(" usr.user_id,usr.display_name,agCrHis.credit_limitamount, agCrHis.remarks from");
			sb.append(" t_agent_credit_history agCrHis,t_user usr");
			sb.append(" where agCrHis.created_by=usr.user_id and ");
			sb.append(" agCrHis.agent_code='" + reportsSearchCriteria.getAgentCode() + "' order by agCrHis.update_date");
		}
		return sb.toString();
	}

	public String getAgentUserDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT A.AGENT_CODE, A.AGENT_NAME, U.USER_ID, U.DISPLAY_NAME USER_NAME ");
		sb.append(" FROM T_USER U,T_AGENT A, T_USER_ROLE UR ");
		sb.append(" WHERE U.AGENT_CODE = A.AGENT_CODE ");
		sb.append("       AND U.USER_ID = UR.USER_ID AND ( ");

		List<String> userList = new ArrayList<String>(reportsSearchCriteria.getUsers());

		while (userList.size() > 100) {
			List<String> subList = userList.subList(0, 100);
			userList = userList.subList(101, userList.size());
			sb.append("       U.USER_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (userList.size() <= 100) {
			sb.append("       U.USER_ID IN( " + Util.buildStringInClauseContent(userList) + " )) ");
		}

		if (reportsSearchCriteria.getRoles() != null) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( UR.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");
		}

		sb.append(" ORDER BY A.AGENT_NAME, USER_NAME ");
		return sb.toString();
	}

	public String getUserPrivilegeDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT DISTINCT C.PRIVILEGE_CATEGORY_DESC CATEGORY, R.ROLE_NAME, P.PRIVILEGE, U.USER_ID ,U.DISPLAY_NAME NAME ");
		sb.append(" FROM T_USER U, T_USER_ROLE UR, T_ROLE_PRIVILEGE RP, T_PRIVILEGE P, T_PRIVILEGE_CATEGORY C, T_ROLE R ");
		sb.append(" WHERE U.USER_ID = UR.USER_ID ");
		sb.append("       AND UR.ROLE_ID = RP.ROLE_ID ");
		sb.append("       AND RP.PRIVILEGE_ID = P.PRIVILEGE_ID ");
		sb.append("       AND P.PRIVILEGE_CATEGORY_ID = C.PRIVILEGE_CATEGORY_ID ");
		sb.append("       AND UR.ROLE_ID = R.ROLE_ID ");
		sb.append("       AND U.USER_ID = '" + reportsSearchCriteria.getUserId() + "' ");

		if ((reportsSearchCriteria.getPrivileges() == null) && (reportsSearchCriteria.getRoles() != null)) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( R.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");
		}

		if ((reportsSearchCriteria.getRoles() != null) && (reportsSearchCriteria.getPrivileges() != null)) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( R.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");

			List<String> privilegeList = new ArrayList<String>(reportsSearchCriteria.getPrivileges());
			sb.append(" AND ( ");

			while (privilegeList.size() > 100) {
				List<String> subList = privilegeList.subList(0, 100);
				privilegeList = privilegeList.subList(101, privilegeList.size());
				sb.append(" P.PRIVILEGE_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
			}

			if (privilegeList.size() <= 100) {
				sb.append(" P.PRIVILEGE_ID IN( " + Util.buildStringInClauseContent(privilegeList) + " )) ");
			}
		}
		sb.append("       AND P.STATUS = 'ACT' ");
		sb.append("       ORDER BY R.ROLE_NAME ASC");

		return sb.toString();
	}

	/**
	 * Gets the Inbound outbound Stored Proc name
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getInbounOutboundConnectionsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "P_IO_BOUND_CONN_REPT";
		return sql;

	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getInbounOutboundScheduleQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = " P_IO_FLIGHT_SCHEDULE_REPT";
		return sql;

	}

	/**
	 * Gets the Revenue Reports stored Proc
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getRevenueProcQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String sql = null;

		if (reportsSearchCriteria.getReportOption()
				.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY)) {
			sql = "P_REVEN_SUMMARY_REPT";
		} else if (reportsSearchCriteria.getReportOption()
				.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY)) {
			sql = "P_REVEN_CHARGES_SUMMARY_REPT";
		} else if (reportsSearchCriteria.getReportOption()
				.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY)) {
			sql = "P_REVEN_AGENT_SUMMARY_REPT";
		} else {
			sql = "P_REVEN_REPT";
		}
		return sql;

	}

	/**
	 * Gets the Forward Sales Reports stored Proc
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getForwardSalesProcQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "P_FWD_SALES_REPT";
		return sql;

	}

	/**
	 * Gets the Interline Revenue Reports stored Proc
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getInterlineRevenueProcQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "P_REVEN_REPT_CARRIER";
		return sql;
	}

	public String getFlightLoadProcQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "P_FLIGHT_LOAD_REPT";
		return sql;
	}

	public String getAgentSalesStatusProcQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "P_AGENT_TXN";
		return sql;
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCustomerProfileNationality(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String secFrom = reportsSearchCriteria.getSectorFrom();
		String secTo = reportsSearchCriteria.getSectorTo();
		String residenci = reportsSearchCriteria.getCountryOfResidence();
		String fromToSegment = null;

		if (secFrom != null && secTo != null) {
			fromToSegment = secFrom + "/" + secTo;
		}

		sb.append("SELECT	cus.customer_id AS CUSTOMER_ID, ");
		sb.append("cus.email_id AS LOGIN_ID, cus.title || ' ' || ");
		sb.append("cus.first_name || ' ' || cus.last_name AS CUSTOMER_NAME, ");
		sb.append("ROUND(MONTHS_BETWEEN(SYSDATE, cus.date_of_birth) / 12) AS AGE, ");
		sb.append("cus.gender AS GENDER, nationality.description ");
		sb.append("AS NATIONALITY,	country.country_name AS COUNTRY_RES, ");
		sb.append("cus.mobile AS MOBILE ");
		sb.append("FROM	T_CUSTOMER cus INNER JOIN T_NATIONALITY nationality ");
		sb.append("ON cus.nationality_code = nationality.nationality_code ");
		sb.append("LEFT OUTER JOIN	T_COUNTRY country ON cus.country_code = country.country_code ");

		if (fromToSegment != null) {
			sb.append(" , t_reservation tr, t_reservation_contact rCon, t_pnr_segment ps, t_flight_segment fs ");
		}

		sb.append(" WHERE 	cus.nationality_code = '" + reportsSearchCriteria.getNationality() + "' ");

		if (residenci != null && !residenci.equals("")) {
			sb.append(" AND 	cus.country_code = '" + residenci + "' ");
		}

		sb.append(" AND cus.registration_date between to_date('" + fromDate + "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
		sb.append("	 AND to_date('" + toDate + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss') ");

		if (fromToSegment != null) {
			sb.append(" and tr.pnr = rCon.pnr ");
			sb.append(" and cus.customer_id = rCon.customer_id ");
			sb.append(" and tr.pnr = ps.pnr ");
			sb.append(" and ps.flt_seg_id = fs.flt_seg_id ");
			sb.append(" and fs.segment_code = '" + fromToSegment + "' ");
		}

		if (reportsSearchCriteria.isByCustomer() && reportsSearchCriteria.getCustomerFirstName() != null) {
			sb.append("and cus.first_name = '" + reportsSearchCriteria.getCustomerFirstName() + "' ");
		}
		if (reportsSearchCriteria.isByCustomer() && reportsSearchCriteria.getCustomerLastName() != null) {
			sb.append("and cus.last_name = '" + reportsSearchCriteria.getCustomerLastName() + "' ");
		}
		sb.append("GROUP BY	cus.customer_id, ");
		sb.append("cus.email_id, cus.title || ' ' || cus.first_name || ' ' || ");
		sb.append("cus.last_name, cus.date_of_birth, cus.gender, nationality.description,");
		sb.append(" country.country_name, cus.mobile ORDER BY	cus.customer_id");
		return sb.toString();
	}

	public String getWebProfilesData(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT   fltseg.segment_code,  res.pnr,  rCon.c_title || ' ' ||  rCon.c_first_name || ' ' || rCon.c_last_name as name, res.status , ");
		sb.append(" TO_CHAR(fltseg.est_time_departure_zulu, 'DD-MM-YYYY') AS DATE_TRAVEL, ");
		sb.append(" cntry.country_name as country_res,  nat.description  ");
		sb.append(" FROM   t_reservation res, t_reservation_contact rCon, t_pnr_segment pnrseg,  t_flight_segment fltseg,  t_country cntry,  t_nationality nat ");

		sb.append(" WHERE    res.pnr=rCon.pnr ");
		if (reportsSearchCriteria.getSalesChannelCode() != null) {
			sb.append(" AND ORIGIN_CHANNEL_CODE=" + reportsSearchCriteria.getSalesChannelCode() + " and ");
		} else {
			sb.append(" AND ORIGIN_CHANNEL_CODE=" + SalesChannelsUtil.SALES_CHANNEL_WEB + " and ");
		}

		if (!reportsSearchCriteria.isRequestAllPassengers()) {
			if (!reportsSearchCriteria.isByBookingStatus()) {
				sb.append("          res.status='CNF' and pnrseg.status='CNF' and ");
			}
		}
		sb.append("          res.booking_timestamp BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ "', 'DD-MON-YYYY HH24:mi:ss' ) ");
		sb.append("  	AND	TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "', 'DD-MON-YYYY HH24:mi:ss') ");
		sb.append("     AND    res.pnr = pnrseg.pnr and  fltseg.flt_seg_id = pnrseg.flt_seg_id ");

		if (reportsSearchCriteria.isByBookingStatus()) {
			if (!reportsSearchCriteria.getBookingStatus().equals("ALL")) {
				sb.append("AND res.status = '" + reportsSearchCriteria.getBookingStatus() + "' ");
			}
		}

		// optional - search by secto
		if (reportsSearchCriteria.isBySector()) {
			sb.append(" AND UPPER(fltseg.segment_code) like ");

			if (!reportsSearchCriteria.getSectorTo().equals("ALL")) {
				sb.append("UPPER('" + reportsSearchCriteria.getSectorFrom() + "%" + reportsSearchCriteria.getSectorTo() + "')  ");
			} else {
				sb.append("UPPER('" + reportsSearchCriteria.getSectorFrom() + "%') ");
			}
		}

		if (reportsSearchCriteria.isByNationality()) {
			sb.append(" and nat.nationality_code =' " + reportsSearchCriteria.getNationality() + "' ");
			sb.append(" and  nat.nationality_code = rCon.c_nationality_code ");
		} else {
			sb.append("   and nat.nationality_code(+) = rCon.c_nationality_code ");
		}

		if (reportsSearchCriteria.isByCountryOfResidence()) {
			sb.append(" and cntry.country_code = '" + reportsSearchCriteria.getCountryOfResidence() + " '");
			sb.append("  and cntry.country_code = rCon.c_country_code  ");
		} else {
			sb.append("   and   cntry.country_code(+) = rCon.c_country_code ");
		}
		sb.append(" ORDER BY  res.booking_timestamp  ");
		return sb.toString();
	}

	public String getCompanyPaymentQuery(ReportsSearchCriteria reportsSearchCriteria) {

		/**
		 * TODO by @nafly once/if product_type is added to t_pax_transaction and t_pax_ext_carrier_transactions tables.
		 * Queries which has 'E_RES' as entity_code has to be modified to join the t_product table and get the
		 * entity_code from their. Also add a where clause to each subquery in UNION to filter the records.
		 * 
		 */
		String singleDataValuePayments = getSingleDataValue(reportsSearchCriteria.getPaymentTypes());
		StringBuilder sb = new StringBuilder();

		String gmtFromOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtFromOffsetInMinutes() + "/1440 ";
		String gmtToOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtToOffsetInMinutes() + "/1440 ";
		Collection<String> externalProducts;
		List<String> agentNominalCodes = new ArrayList<>();

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.COMPANY_PAYMENT_SUMMARY)) {
			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {

				sb.append(" select distinct agent_code,iata_agent_code,agent_name, account_code  ,pmt_amt, ref_amt as total_refund, (pmt_amt - ref_amt) as total_amount,");
				sb.append(" 0 as payment_currency_code,");
				sb.append("	0 as total_amount_paycur,");
				sb.append("	0 as total_fare_amount, 0 as total_fare_amount_paycur, ");
				sb.append(" 0 as total_tax_amount,  0 as total_tax_amount_paycur,");
				sb.append(" 0 as total_sur_amount , 0 as total_sur_amount_paycur, ");
				sb.append(" 0 as total_modify, ");
				sb.append(" 0 as total_modify_local, 0 as total_refund_local,");
				sb.append(" 0 as pmt_amt_local, credit_limit as agent_credit_limit, avilable_credit as avilable_credit");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , tranCount as tranCount ");
				} else {
					sb.append(" , 0 as tranCount");
				}
				sb.append(" from");
				sb.append(" ( SELECT ag.agent_code,a.agent_iata_number AS IATA_AGENT_CODE,a.agent_name, a.account_code,a.credit_limit credit_limit,tas.avilable_credit avilable_credit, ");
				sb.append(" sum (decode (px.CR_DR,'CR',(round(px.amount,2) * -1),0)) ref_amt, ");
				sb.append(" sum (decode (px.cr_dr,'DR',round(px.amount,2),0))  pmt_amt ");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , COUNT(TAT.txn_id) tranCount ");
				}
				sb.append("		 FROM t_pax_ext_transactions px, t_agent_transaction ag, t_agent a, t_agent_summary tas, t_product p ");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append("  , T_AGENT_TRANSACTION TAT ");
				}
				sb.append("      where px.txn_id = ag.txn_id ");
				sb.append("        and a.agent_code = tas.agent_code");
				sb.append("        and p.product_type = ag.product_type");
				sb.append("        and px.nominal_code in(" + singleDataValuePayments + ") ");
				sb.append("        and ag.tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("        and to_date('" + reportsSearchCriteria.getDateRangeTo()
						+ "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
				sb.append(gmtToOffsetInMinutesStr);
				sb.append("        and ag.agent_code = a.agent_code ");
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("        and  ");
					sb.append(
							ReportUtils.getReplaceStringForIN(" ag.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("        and  ");
					sb.append(
							ReportUtils.getReplaceStringForIN(" ag.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
					sb.append(" AND a.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
				}
				// sb.append(" and px.sales_channel_code = 11 ");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" AND A.AGENT_CODE               = TAT.AGENT_CODE(+) ");
					sb.append(" AND TAT.TNX_DATE(+) BETWEEN TO_DATE('" + reportsSearchCriteria.getToDate()
							+ "' || ' 00:00:00' , 'dd-mon-yyyy hh24:mi:ss')");
					sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
							+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
				}
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( a.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"')) ");
				}
				sb.append(" and p.entity_code ='"+ reportsSearchCriteria.getEntity() + "' ");
				sb.append(
						"        group by ag.agent_code, a.agent_name,a.account_code,a.agent_iata_number, a.credit_limit, tas.avilable_credit ");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , TAT.txn_id ");
				}
				sb.append(" ) ");

			} else {
				// Following query combines existing payments with LCC interline payments. With the current
				// CompanyPaymentReportRH implimentation this code path will not occor.
				// TODO For summary in both base and pay curriency the getCompanyPaymentQueryByCurrency summary paty
				// will be used. remove this or fix the CompanyPaymentReportRH.
				sb.append("SELECT	agent_code, agent_name, (-1 * pmt_amt) pmt_amt,");
				sb.append(" (-1 * ref_amt) ref_amt, ((-1 * pmt_amt) - ref_amt) net_amt ");
				sb.append(" FROM ");
				sb.append(" (SELECT A.agent_code,A.agent_name,");
				sb.append(
						" sum(decode(t.nominal_code, 36,t.amount, 30,t.amount, 32,t.amount, 6,t.amount, 28,t.amount,18,t.amount,17,t.amount,16,t.amount,19,t.amount,15,t.amount,48,t.amount,0)) pmt_amt,");
				sb.append(
						" sum(decode(t.nominal_code, 37,t.amount, 31,t.amount, 33,t.amount, 5,t.amount, 29,t.amount,26,t.amount,24,t.amount,23,t.amount,25,t.amount,22,t.amount,0)) ref_amt");
				sb.append(" FROM (SELECT T.AGENT_CODE AS AGENT_CODE, T.NOMINAL_CODE AS NOMINAL_CODE, T.AMOUNT AS AMOUNT , 'E_RES' as entity_code");
				sb.append("          FROM T_PAX_TRANSACTION T");
				if (reportsSearchCriteria.getSearchFlightType() != null) {
					sb.append(", t_pnr_passenger p");
				}
				sb.append("         WHERE TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("           AND ");
				sb.append("               TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation() ) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				
				sb.append("           AND (T.PAYMENT_CARRIER_CODE IS NULL OR T.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");

				if (reportsSearchCriteria.getSearchFlightType() != null) {
					// NOTE : if a reservation has at-least a international flight, total fare considered to be
					// international fare
					sb.append(" AND " + (reportsSearchCriteria.getSearchFlightType().equals("INT") ? "" : "NOT") + " EXISTS (");
					sb.append("       SELECT pp1.pnr");
					sb.append("          FROM t_pax_transaction b1,");
					sb.append("         t_pnr_segment ps          ,");
					sb.append("         t_pnr_passenger pp1       ,");
					sb.append("         t_flight_segment fs       ,");
					sb.append("         t_flight f");
					sb.append("         WHERE ps.pnr                 = pp1.pnr");
					sb.append("       AND p.pnr                      = ps.pnr");
					sb.append("       AND pp1.pnr_pax_id             = b1.pnr_pax_id");
					sb.append("       AND pp1.pnr_pax_id             = T.pnr_pax_id");
					sb.append("       AND p.pnr_pax_id               = b1.pnr_pax_id");
					sb.append("       AND ps.flt_seg_id              = fs.flt_seg_id");
					sb.append("       AND fs.flight_id               = f.flight_id");
					sb.append("       AND f.flight_type              = 'INT'");
					sb.append("       AND ( b1.payment_carrier_code IS NULL");
					sb.append("              OR b1.payment_carrier_code = '" + AppSysParamsUtil.getDefaultCarrierCode() + "')");
					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation() ) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					sb.append("         AND b1.sales_channel_code <> 11)");
				}

				sb.append("        UNION ALL ");
				sb.append("        SELECT P.AGENT_CODE AS AGENT_CODE, ");
				sb.append("               P.NOMINAL_CODE AS NOMINAL_CODE, ");
				sb.append("               P.AMOUNT AS AMOUNT , 'E_RES' as entity_code ");
				sb.append("          FROM T_PAX_EXT_CARRIER_TRANSACTIONS P");
				sb.append("           WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("           AND ");
				sb.append("               TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
				}
				sb.append("	            ) T,");
				sb.append("       T_AGENT A");
				sb.append("  where t.nominal_code in(" + singleDataValuePayments + ") ");
				sb.append("   AND A.AGENT_CODE = T.AGENT_CODE");
				sb.append("   AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
				sb.append("   AND t.entity_code = '" + reportsSearchCriteria.getEntity() + "'");
				if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
					sb.append(" AND A.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
				}
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( A.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"')) ");

				}
				
				sb.append(" group by A.agent_code,A.agent_name)");
				sb.append("	order by agent_code	 ");
			}

		} else {
			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {
				sb.append(" SELECT AGENT_CODE, description,agent_name,pnr_pax_id ,payment_date,pnr,PASSENGER, ");
				sb.append(" F_COMPANY_PYMT_REPORT(A.pnr) routing,USERNAME , ");
				sb.append(" F_CONCATENATE_LIST ( cursor (select PET.E_TICKET_NUMBER from T_PNR_PASSENGER PP,T_PAX_E_TICKET PET "
						+ " where PP.PNR_PAX_ID = PET.PNR_PAX_ID and PP.PNR_PAX_ID = A.PNR_PAX_ID ),'-') as ETICKET, ");
				sb.append(" ACTUAL_PAY,PAY_REF,INFANT,REF_AMT , pmt_amt ");
				sb.append(" FROM ( ");
				sb.append(" SELECT ag.agent_code,'On Account' description, a.agent_name, px.pnr_pax_id , ");
				sb.append(" sum (decode (px.CR_DR,'CR',round(px.amount,2) * -1,0) ) ref_amt , ");
				sb.append(" sum (decode (px.cr_dr,'DR',round(px.amount,2),0))as pmt_amt , ");
				sb.append(" to_char(px.timestamp,'dd-MON-yyyy' ) payment_date, b.pnr, ");
				sb.append(" b.first_name||' '||b.last_name passenger, ");
				sb.append(" c.display_name username , ");
				sb.append("	'' as ACTUAL_PAY, '' as PAY_REF, '' as INFANT");
				sb.append(" FROM t_pax_ext_transactions px, t_agent_transaction ag, t_agent a, t_pnr_passenger b, t_user c , t_product p");
				sb.append(" where px.txn_id = ag.txn_id ");
				sb.append(" and px.nominal_code in (10,11) ");
				sb.append(" and ag.agent_code = a.agent_code ");
				sb.append(" and b.pnr_pax_id = px.pnr_pax_id ");
				sb.append(" and c.user_id = ag.user_id ");
				sb.append(" and ag.product_type = p.product_type ");
				sb.append(" and  tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" and to_date('" + reportsSearchCriteria.getDateRangeTo() + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("ag.agent_code", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("ag.agent_code", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( a.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"')) ");
				}
				sb.append(" AND p.product_type = '"+ reportsSearchCriteria.getEntity() + "'");
				sb.append(" group by px.pnr_pax_id, ag.agent_code, a.agent_name ,to_char(px.timestamp,'dd-MON-yyyy' ),b.pnr, ");
				sb.append(" b.first_name||' '||b.last_name,  c.display_name)A ");

			} else {
				// Following query combines existing payments with LCC interline payments & Dry sell.
				sb.append("select agent_code,agent_name,decode(nominal_code,19,'On Account',25,'On Account',18,'Cash',26,'Cash',15,'Visa',22,'Visa',16,'Master',23,'Master',28,'Amex',29,'Amex',17,'Diners',24,'Diners',30,'Generic',31,'Generic',32,'CMI',33,'CMI',5,'CREDIT',6,'CREDIT' ,36,'BSP', 37,'BSP', 48, 'VOUCHER') description,to_char(payment_date,'dd-MON-yyyy')payment_date,");
				sb.append(" pnr, external_rec_locator, passenger,routing,username,-1*pmt_amt pmt_amt,");
				sb.append("  -1*ref_amt ref_amt ");
				sb.append(",eticket");
				sb.append(",PAY_REF, product_type, agent_pay_ref, ACTUAL_PAY, INFANT");// Integrating Actual Payment Method
				sb.append(" 	FROM ");
				sb.append(" 	(");

				sb.append(" 	SELECT A.agent_code,A.agent_name,V.nominal_code,nvl(V.eticket_id,' ') as eticket, ");
				sb.append(
						"  V.tnx_date payment_date,V.pnr pnr, v.external_rec_locator external_rec_locator, V.first_name||' '||V.last_name passenger,u.display_name username, ");
				sb.append(
						" 	sum( case when v.product_type IS NULL OR v.product_type='V' THEN decode(V.nominal_code, 36,V.amount, 30,V.amount, 32,V.amount, 6,V.amount, 28,V.amount,18,V.amount,17,V.amount,16,V.amount,19,V.amount,15,V.amount,48,V.amount,0) ELSE decode(v.nominal_code, 19, v.amount, 21, v.amount, 0) END )pmt_amt,");
				sb.append(
						" 	sum( case when v.product_type IS NULL OR v.product_type='V' THEN decode(V.nominal_code, 37,V.amount, 31,V.amount, 33,V.amount, 5,V.amount, 29,V.amount,26,V.amount,24,V.amount,23,V.amount,25,V.amount,22,V.amount,0) ELSE decode(v.nominal_code, 20, v.amount, 22, v.amount, 0) END )ref_amt");
				sb.append(
						"		,v.routing ,v.ACTUAL_PAY as ACTUAL_PAY, v.PAY_REF as PAY_REF, v.product_type as product_type, v.agent_pay_ref as agent_pay_ref, case when V.INFANT is null THEN ' ' else '/INF' end AS INFANT ,v.entity_code");
				sb.append("     FROM (");

				// Normal Sell
				sb.append("           SELECT P.PNR          AS PNR,");
				sb.append("					 r.external_rec_locator external_rec_locator, ");
				sb.append("                  P.PNR_PAX_ID   AS PNR_PAX_ID,");
				sb.append("                  P.FIRST_NAME   AS FIRST_NAME,");
				sb.append("                  P.LAST_NAME    AS LAST_NAME,");
				// sb.append("                  F_GET_ETICKET_STRING(P.PNR_PAX_ID)  AS ETICKET_ID,");
				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = P.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append("                  T.NOMINAL_CODE AS NOMINAL_CODE,");
				sb.append("                  T.TNX_DATE     AS TNX_DATE,");
				sb.append("                  T.USER_ID      AS USER_ID,");
				sb.append("                  T.AMOUNT       AS AMOUNT,");
				sb.append("                  T.AGENT_CODE   AS AGENT_CODE,");
				sb.append("			  	 	 F_COMPANY_PYMT_REPORT(P.PNR) AS ROUTING ");
				sb.append("            	 	,actual_p.payment_mode as ACTUAL_PAY");
				sb.append("					,T.EXT_REFERENCE as PAY_REF");
				sb.append("					,f_concatenate_list ( cursor ");
				sb.append("						( select ext_reference ");
				sb.append("							from t_agent_transaction ");
				sb.append("							where pnr = r.pnr and product_type = 'G' ) ) agent_pay_ref ");
				sb.append(
						"					,(select pp.first_name from t_pnr_passenger pp where pp.adult_id = P.PNR_PAX_ID ) AS INFANT , null product_type,'E_RES' as entity_code ");
				sb.append("             FROM T_PNR_PASSENGER P, T_PAX_TRANSACTION T ");
				sb.append("   			,T_ACTUAL_PAYMENT_METHOD actual_p, t_reservation r ");
				sb.append("            WHERE P.PNR_PAX_ID = T.PNR_PAX_ID  and p.pnr = r.pnr ");
				sb.append("            AND 	 (T.PAYMENT_CARRIER_CODE IS NULL OR T.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
				sb.append("              AND t.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("           AND ");
				sb.append("                  TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				sb.append("              AND T.SALES_CHANNEL_CODE <> 11");
				sb.append(" 			 AND actual_p.payment_mode_id (+)= T.payment_mode_id ");

				if (reportsSearchCriteria.getSearchFlightType() != null) {
					// NOTE : if a reservation has at-least a international flight, total fare considered to be
					// international fare
					sb.append(" AND " + (reportsSearchCriteria.getSearchFlightType().equals("INT") ? "" : "NOT") + " EXISTS (");
					sb.append(" SELECT pp1.pnr");
					sb.append("  FROM t_pax_transaction b1,");
					sb.append(" t_pnr_segment ps          ,");
					sb.append(" t_pnr_passenger pp1       ,");
					sb.append(" t_flight_segment fs       ,");
					sb.append(" t_flight f");
					sb.append(" WHERE ps.pnr                 = pp1.pnr");
					sb.append(" AND p.pnr                    = ps.pnr");
					sb.append(" AND pp1.pnr_pax_id           = b1.pnr_pax_id");
					sb.append(" AND pp1.pnr_pax_id           = T.pnr_pax_id");
					sb.append(" AND p.pnr_pax_id             = b1.pnr_pax_id");
					sb.append(" AND ps.flt_seg_id            = fs.flt_seg_id");
					sb.append(" AND fs.flight_id             = f.flight_id");
					sb.append(" AND f.flight_type            = 'INT'");
					sb.append(" AND ( b1.payment_carrier_code IS NULL");
					sb.append("              OR b1.payment_carrier_code = '" + AppSysParamsUtil.getDefaultCarrierCode() + "')");
					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					sb.append("         AND b1.sales_channel_code <> 11)");
				}

				// External sales union
				sb.append("           UNION ALL");
				sb.append("           SELECT X.PNR      AS PNR, r.external_rec_locator external_rec_locator, ");
				sb.append("                  X.PNR_PAX_ID     AS PNR_PAX_ID,");
				sb.append("                  X.FIRST_NAME     AS FIRST_NAME,");
				sb.append("                  X.LAST_NAME      AS LAST_NAME,");
				// sb.append("                  F_GET_ETICKET_STRING(X.PNR_PAX_ID)      AS ETICKET_ID ,");
				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = X.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append("                  P.NOMINAL_CODE   AS NOMINAL_CODE, ");
				sb.append("                  P.TXN_TIMESTAMP  AS TNX_DATE,");
				sb.append("                  P.USER_ID        AS USER_ID,");
				sb.append("                  P.AMOUNT    	  AS AMOUNT,");
				sb.append("                  P.AGENT_CODE     AS AGENT_CODE,");
				sb.append("                  F_COMPANY_PYMT_REPORT(X.PNR, 'Y') AS ROUTING ");
				sb.append("            	  	,'' as ACTUAL_PAY");
				sb.append("            	  	,'' as PAY_REF");
				sb.append("            	  	,null as agent_pay_ref");
				sb.append(
						"					, (select pp.first_name from t_pnr_passenger pp where pp.adult_id = x.PNR_PAX_ID ) AS INFANT, null product_type , 'E_RES' as entity_code");
				sb.append("            FROM T_PAX_EXT_CARRIER_TRANSACTIONS P, T_PNR_PASSENGER X , t_reservation r ");
				sb.append("            WHERE P.PNR_PAX_ID = X.PNR_PAX_ID and x.pnr = r.pnr ");
				sb.append("              AND P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("           AND ");
				sb.append("                 TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				sb.append("             AND P.SALES_CHANNEL_CODE <> 11");

				// External sales before 11/06/2011 Dry sell Phase 2 release - union
				sb.append("           UNION ALL");
				sb.append("           SELECT X.PNR      AS PNR,  r.external_rec_locator external_rec_locator, ");
				sb.append("                  X.PNR_PAX_ID     AS PNR_PAX_ID,");
				sb.append("                  X.FIRST_NAME     AS FIRST_NAME,");
				sb.append("                  X.LAST_NAME      AS LAST_NAME,");
				// sb.append(" F_GET_ETICKET_STRING(X.PNR_PAX_ID) AS ETICKET_ID,");
				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = X.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append("                  P.NOMINAL_CODE   AS NOMINAL_CODE, ");
				sb.append("                  P.TXN_TIMESTAMP  AS TNX_DATE,");
				sb.append("                  P.USER_ID        AS USER_ID,");
				sb.append("                  -1 * P.AMOUNT    AS AMOUNT,");
				sb.append("                  P.AGENT_CODE     AS AGENT_CODE,");
				sb.append("                  F_COMPANY_PYMT_REPORT(X.PNR) AS ROUTING ");
				sb.append("            	  	,'' as ACTUAL_PAY");
				sb.append("            	  	,'' as PAY_REF");
				sb.append("            	  	,null as agent_pay_ref");
				sb.append(
						"					, (select pp.first_name from t_pnr_passenger pp where pp.adult_id = x.PNR_PAX_ID ) AS INFANT , null product_type , 'E_RES' as entity_code");
				sb.append("            FROM T_PAX_EXT_CC_TRANSACTION_OLD P, T_PNR_PASSENGER X , t_reservation r ");
				sb.append("            WHERE P.PNR_PAX_ID = X.PNR_PAX_ID and x.pnr = r.pnr ");
				sb.append("              AND P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("           AND ");
				sb.append("                 TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				sb.append("             AND P.SALES_CHANNEL_CODE <> 11");

				sb.append("	UNION ALL");
				sb.append(" SELECT v.voucher_group_id AS PNR,");
				sb.append(" null AS external_rec_locator,");
				sb.append(" null AS PNR_PAX_ID,");
				sb.append(" V.pax_first_name AS FIRST_NAME,");
				sb.append(" V.pax_last_name AS LAST_NAME,");
				sb.append(" null AS ETICKET_ID,");
				sb.append(" vp.NOMINAL_CODE AS NOMINAL_CODE,");
				sb.append(" v.issued_time AS TNX_DATE,");
				sb.append(" v.issued_user_id AS USER_ID,");
				sb.append(" -1 *vP.AMOUNT AS AMOUNT,");
				sb.append(" u.agent_code AS AGENT_CODE,");
				sb.append(" null AS ROUTING,");
				sb.append(" '' as ACTUAL_PAY, ");
				sb.append(" '' as PAY_REF, ");
				sb.append(" NULL as agent_pay_ref, ");
				sb.append(" null as INFANT, ");
				sb.append(" 'V' as product_type, ");
				sb.append(" 'E_RES' as entity_code ");
				sb.append("	from t_voucher_payment vp, t_voucher v, t_user u");
				sb.append("	where vp.voucher_id=v.voucher_id AND");
				sb.append("	v.issued_user_id=user_id");
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("		AND" + ReportUtils
							.getReplaceStringForIN("u.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("		AND" + ReportUtils
							.getReplaceStringForIN("u.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
				}
				sb.append("	AND v.status in('I','U') and");
				sb.append("	v.issued_time BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);

				sb.append(" 	) V,t_agent a, t_user u");
				sb.append("		  where (	") ;
				sb.append(" 	   V.nominal_code in(" + singleDataValuePayments + ") )  and");
				sb.append(" 	  		V.user_id = u.user_id(+) ");
				sb.append(" 	        AND a.agent_code=V.agent_code ");
				sb.append("             AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
				sb.append("             AND v.entity_code = '"+ reportsSearchCriteria.getEntity() + "' ");
				
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( A.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"')) ");
				}
				
				sb.append(" 	group by V.pnr_pax_id,V.nominal_code,A.agent_code,A.agent_name,");
				sb.append(
						"  pnr, external_rec_locator, V.first_name||' '||V.last_name,u.display_name,V.tnx_date ,V.eticket_id ,v.routing,V.PAY_REF, v.product_type, V.agent_pay_ref, V.ACTUAL_PAY, V.INFANT,v.entity_code");


				if (reportsSearchCriteria.getPaymentTypes().contains("19")) {
					agentNominalCodes.add("19");
				}
				if (reportsSearchCriteria.getPaymentTypes().contains("25")) {
					agentNominalCodes.add("20");
				}
				if (reportsSearchCriteria.getPaymentTypes().contains("36")) {
					agentNominalCodes.add("21");
				}
				if (reportsSearchCriteria.getPaymentTypes().contains("37")) {
					agentNominalCodes.add("22");
				}

				if (!agentNominalCodes.isEmpty()) {
					sb.append("	union all ")

							.append("	select a.agent_code, a.agent_name,decode( t.nominal_code, 19, 19, 20, 25, 21,36, 22, 37 ), null, t.tnx_date, t.pnr, null, ")
							.append("		nvl( (select listagg(first_name || ' ' || last_name, ', ') within group (order by 1) from t_pnr_passenger p where p.pnr = t.pnr ) , ' '), ")
							.append("		u.display_name, - decode(t.nominal_code, 19, t.amount, 21, t.amount, 0) pmt_amt, ")
							.append("		- decode(t.nominal_code, 20, t.amount, 22, t.amount, 0) ref_amt, ")
							.append("		 null, null, null, t.product_type, t.ext_reference, ' ' , p.entity_code")
							.append("	from t_agent_transaction t, t_agent a, t_user u ,t_product p ")
							.append("	where p.product_type = t.product_type  ")
							.append("	and  t.agent_code = a.agent_code and t.user_id = u.user_id (+)  ");

					sb.append("	and" + ReportUtils.getReplaceStringForIN("nominal_code", agentNominalCodes, false));

					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN(" t.agent_code", reportsSearchCriteria.getAgents(), false));
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN(" t.agent_code", reportsSearchCriteria.getAgents(), false));
					}
					sb.append(" and t.product_type<>'V'");
					if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
						sb.append(" AND a.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
					}
					if (reportsSearchCriteria.isOnlyReportingAgents()) {

						sb.append(" AND( a.AGENT_CODE IN (SELECT AGENT_CODE");
						sb.append(" FROM T_AGENT");
						sb.append(" WHERE STATUS = 'ACT'");
						sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
						sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"')) ");
					}
					sb.append("        AND tnx_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
							+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtFromOffsetInMinutesStr);
					sb.append("        		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
							+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
					
					sb.append(" AND p.entity_code = '"+ reportsSearchCriteria.getEntity() + "' ");

				}

				
				sb.append(" 	)P               ");
				sb.append(" 	order by agent_code,description,payment_date,pnr ");
			}

		}

		return sb.toString();
	}

	/**
	 * By currency
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCompanyPaymentQueryByCurrency(ReportsSearchCriteria reportsSearchCriteria) {

		/**
		 * TODO by @nafly once/if product_type is added to t_pax_transaction and t_pax_ext_carrier_transactions tables.
		 * Queries which has 'E_RES' as entity_code has to be modified to join the t_product table and get the
		 * entity_code from their. Also add a where clause to each subquery in UNION to filter the records.
		 * 
		 */
		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String singleDataValuePayments = getSingleDataValue(reportsSearchCriteria.getPaymentTypes());

		StringBuilder sb = new StringBuilder();

		String gmtFromOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtFromOffsetInMinutes() + "/1440 ";
		String gmtToOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtToOffsetInMinutes() + "/1440 ";

		List<String> agentNominalCodes = new ArrayList<>();
		
		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.COMPANY_PAYMENT_SUMMARY)) {
			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {
				// FIXME : Seems like this block will never execute
				sb.append(" select pax.agent_code, pax.agent_name, pax.account_code, (pax.pmt_amt + ext.pmt_amt) pmt_amt,(pax.ref_amt + ext.ref_amt) ref_amt, (pax.net_amt + ext.net_amt) net_amt from ");

				sb.append(" ( ");
				sb.append(" SELECT	agent_code, agent_name, account_code , (-1 * pmt_amt) pmt_amt, ");
				sb.append(" (-1 * ref_amt) ref_amt, ((-1 * pmt_amt) - ref_amt) net_amt ");
				sb.append(" FROM ");
				sb.append(" (SELECT A.agent_code,A.agent_name, A.account_code, ");
				sb.append(
						" sum(decode(t.nominal_code, 36,t.amount, 30,t.amount, 32,t.amount, 6,t.amount, 28,t.amount,18,t.amount,17,t.amount,16,t.amount,19,t.amount,15,t.amount,48,t.amount,0)) pmt_amt, ");
				sb.append(
						" sum(decode(t.nominal_code, 37,t.amount, 31,t.amount, 33,t.amount, 5,t.amount, 29,t.amount,26,t.amount,24,t.amount,23,t.amount,25,t.amount,22,t.amount,0)) ref_amt,'E_RES' as entity_code ");
				sb.append(" FROM T_PAX_TRANSACTION t,t_agent a ");
				sb.append(" where t.nominal_code in(" + singleDataValuePayments + ") ");
				sb.append(" and a.agent_code=t.agent_code ");
				sb.append(" and tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" and to_date('" + reportsSearchCriteria.getDateRangeTo() + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append(" AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append(" AND ");
					sb.append(
							ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + "   ");
				}
				sb.append(" and T.sales_channel_code <> 11                   ");
				sb.append("							group by A.agent_code,A.agent_name,A.account_code) ");
				sb.append(" where entity_code='" + reportsSearchCriteria.getEntity() +"' ");
				sb.append(" order by agent_code ");
				sb.append(" )pax , ");

				sb.append(" ( ");
				sb.append(" select agent_code, pmt_amt, ref_amt, (pmt_amt - ref_amt) as net_amt from ");
				sb.append(" ( SELECT ag.agent_code, ");
				sb.append(" sum (decode (px.CR_DR,'CR',(round(px.amount,2) * -1),0)) ref_amt, ");
				sb.append(" sum (decode (px.cr_dr,'DR',round(px.amount,2),0))  pmt_amt ");

				sb.append(" FROM t_pax_ext_transactions px, t_agent_transaction ag ,t_product p");
				sb.append(" where px.txn_id = ag.txn_id ");
				sb.append(" and ag.PRODUCT_TYPE = p.PRODUCT_TYPE ");
				sb.append(" and px.nominal_code in (10,11) ");
				sb.append(" and tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" and to_date('" + reportsSearchCriteria.getDateRangeTo() + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("ag.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("ag.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				sb.append(" and px.sales_channel_code <> 11 ");
				sb.append(" and p.entity_code='" + reportsSearchCriteria.getEntity() +"' ");
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( ag.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"'))");

				}
				sb.append(" group by ag.agent_code) ");
				sb.append(" ) ext ");
				sb.append(" where ext.agent_code = pax.agent_code ");

			}else{
				if (reportsSearchCriteria.isReportViewNew()) {
					sb.append(RevenueReportsStatementCreator.getNewCompanyPaymentSummaryByCurrencyQuery(reportsSearchCriteria,
							singleDataValuePayments));
				} else {
					// Following query combines existing payments with LCC interline payments & DRY sales payments
					// FIXME fare breakdown is not available in the llc payments at the moment. return 0 for all the fields
					// which does not have values in lcc_t_pnr_transaction
					sb.append(
							"SELECT distinct entity_code,agent_code,iata_agent_code,agent_name,NVL(account_code,'none') as account_code , agent_credit_limit,avilable_credit,payment_currency_code, ");
					sb.append("		total_amount, total_amount_paycur,");
					sb.append("	 total_fare_amount,  total_fare_amount_paycur, ");
					sb.append(" total_tax_amount,  total_tax_amount_paycur,");
					sb.append(" ROUND(total_sur_amount,2) total_sur_amount,  ROUND(total_sur_amount_paycur,2) total_sur_amount_paycur, ");
					sb.append(" ROUND(total_modify,2) total_modify,  total_refund, ");
					sb.append(" ROUND(total_modify_local,2) total_modify_local,  total_refund_local, ");
					sb.append(" (total_amount + total_refund) pmt_amt,");
					sb.append(" ROUND(TOTAL_PENALTY,2) TOTAL_PENALTY, ");
					sb.append(" ROUND((case WHEN TOTAL_ANCI_SUR_AMOUNT is null THEN 0 ");
					sb.append(" ELSE TOTAL_ANCI_SUR_AMOUNT END),2) AS TOTAL_ANCI_SUR_AMOUNT, ");
					sb.append(" (total_amount_paycur + total_refund_local) pmt_amt_local");
					sb.append(" , ROUND(TOTAL_ANCI_SUR_AMOUNT_PAYCUR,2) TOTAL_ANCI_SUR_AMOUNT_PAYCUR ");
					sb.append(" , ROUND(TOTAL_PENALTY_PAYCUR,2) TOTAL_PENALTY_PAYCUR ");
					if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
						sb.append(" , tranCount tranCount");
					} else {
						sb.append(" , 0 as tranCount");
					}
					sb.append(" FROM ");
	
					sb.append(" (");// begin sub select 1
					sb.append(" SELECT A.AGENT_CODE,");
					sb.append(" A.AGENT_NAME,");
					sb.append(" A.ACCOUNT_CODE,");
					sb.append(" A.AGENT_IATA_NUMBER AS IATA_AGENT_CODE,");
					sb.append(" A.CREDIT_LIMIT agent_credit_limit,");
					sb.append(" TAS.AVILABLE_CREDIT AVILABLE_CREDIT,");
					sb.append(" T.PAYMENT_CURRENCY_CODE,");
					sb.append(" (-1 * SUM(T.TOTAL_AMOUNT)) TOTAL_AMOUNT,");
					sb.append(" (-1 * SUM(T.TOTAL_AMOUNT_PAYCUR)) TOTAL_AMOUNT_PAYCUR,");
					sb.append(" (-1 * SUM(T.TOTAL_FARE_AMOUNT)) TOTAL_FARE_AMOUNT,");
					sb.append(" (-1 * SUM(TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCUR,");
					sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT)) TOTAL_TAX_AMOUNT,");
					sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_AMOUNT_PAYCUR,");
					sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT)) TOTAL_SUR_AMOUNT,");
					sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_AMOUNT_PAYCUR,");
					sb.append(" (-1 * SUM(DECODE(T.NOMINAL_CODE, 36,TOTAL_MOD_AMOUNT, 30,TOTAL_MOD_AMOUNT, 32,TOTAL_MOD_AMOUNT, 6,TOTAL_MOD_AMOUNT, 28,TOTAL_MOD_AMOUNT,18,TOTAL_MOD_AMOUNT,17,TOTAL_MOD_AMOUNT,16,TOTAL_MOD_AMOUNT,19,TOTAL_MOD_AMOUNT,15,TOTAL_MOD_AMOUNT,48,TOTAL_MOD_AMOUNT,0))) TOTAL_MODIFY                                                                      ,");
					sb.append(" SUM(DECODE(T.NOMINAL_CODE, 37,TOTAL_MOD_AMOUNT, 31,TOTAL_MOD_AMOUNT, 33,TOTAL_MOD_AMOUNT, 5,TOTAL_MOD_AMOUNT, 29,TOTAL_MOD_AMOUNT,26,TOTAL_MOD_AMOUNT,24,TOTAL_MOD_AMOUNT,23,TOTAL_MOD_AMOUNT,25,TOTAL_MOD_AMOUNT,22,TOTAL_MOD_AMOUNT,0)) TOTAL_REFUND                                                                             ,");
					sb.append(" (-1 * SUM(DECODE(T.NOMINAL_CODE, 36,TOTAL_MOD_AMOUNT_PAYCUR, 30,TOTAL_MOD_AMOUNT_PAYCUR, 32,TOTAL_MOD_AMOUNT_PAYCUR, 6,TOTAL_MOD_AMOUNT_PAYCUR, 28,TOTAL_MOD_AMOUNT_PAYCUR,18,TOTAL_MOD_AMOUNT_PAYCUR,17,TOTAL_MOD_AMOUNT_PAYCUR,16,TOTAL_MOD_AMOUNT_PAYCUR,19,TOTAL_MOD_AMOUNT_PAYCUR,15,TOTAL_MOD_AMOUNT_PAYCUR,48,TOTAL_MOD_AMOUNT_PAYCUR,0))) TOTAL_MODIFY_LOCAL ,");
					sb.append(" (SUM(T.TOTAL_ANCI_SUR_AMOUNT)) TOTAL_ANCI_SUR_AMOUNT,");
					sb.append(" (SUM(T.TOTAL_PENALTY)) TOTAL_PENALTY,");
					sb.append(" (SUM(T.TOTAL_ANCI_SUR_AMOUNT_PAYCUR)) TOTAL_ANCI_SUR_AMOUNT_PAYCUR,");
					sb.append(" (SUM(T.TOTAL_PENALTY_PAYCUR)) TOTAL_PENALTY_PAYCUR,");
					sb.append(" SUM(DECODE(T.NOMINAL_CODE, 37,TOTAL_MOD_AMOUNT_PAYCUR, 31,TOTAL_MOD_AMOUNT_PAYCUR, 33,TOTAL_MOD_AMOUNT_PAYCUR, 5,TOTAL_MOD_AMOUNT_PAYCUR, 29,TOTAL_MOD_AMOUNT_PAYCUR,26,TOTAL_MOD_AMOUNT_PAYCUR,24,TOTAL_MOD_AMOUNT_PAYCUR,23,TOTAL_MOD_AMOUNT_PAYCUR,25,TOTAL_MOD_AMOUNT_PAYCUR,22,TOTAL_MOD_AMOUNT_PAYCUR,0)) TOTAL_REFUND_LOCAL ,t.entity_code       ");
					
					if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
						sb.append(" , Count(TAT.txn_id) tranCount ");
					}
					sb.append("	FROM");
					sb.append(" (");// begin sub select 2
	
					sb.append(" SELECT b.agent_code agent_code, b.nominal_code AS nominal_code,");
					sb.append("   a.total_amount AS total_amount,");
					sb.append("   a.total_amount_paycur AS total_amount_paycur,");
	
					sb.append("a.total_fare_amount        as total_fare_amount,");
					sb.append("a.total_fare_amount_paycur as total_fare_amount_paycur,");
					sb.append("a.total_tax_amount         as total_tax_amount,");
					sb.append("a.total_tax_amount_paycur  as total_tax_amount_paycur,");
					sb.append("a.total_sur_amount         as total_sur_amount,");
					sb.append("a.total_sur_amount_paycur  as total_sur_amount_paycur,");
	//AEROMART-3166
					/*sb.append("CASE WHEN (a.total_mod_amount = 0 OR a.total_mod_amount    > 0) AND (a.total_fare_amount > 0 ");
					sb.append(" OR a.total_tax_amount > 0 OR a.total_sur_amount > 0) ");
					sb.append(" THEN (a.total_fare_amount + a.total_tax_amount+ a.total_sur_amount+a.total_mod_amount) ");
					sb.append(" ELSE a.total_mod_amount END AS total_mod_amount,");
					sb.append("CASE WHEN (a.total_mod_amount_paycur = 0 OR a.total_mod_amount_paycur > 0) ");
					sb.append(" AND (a.total_fare_amount_paycur > 0 OR a.total_tax_amount_paycur > 0 OR a.total_sur_amount_paycur > 0) ");
					sb.append(" THEN (a.total_fare_amount_paycur + a.total_tax_amount_paycur+ a.total_sur_amount_paycur+a.total_mod_amount_paycur) ");
					sb.append(" ELSE a.total_mod_amount_paycur END AS total_mod_amount_paycur,");*/
					
					if (AppSysParamsUtil.displayAmountsInSeperateColumnsWithCategory()) {
						sb.append(" CASE WHEN a.total_sur_amount_paycur <> 0 THEN ");
						sb.append(" (a.total_sur_amount_paycur + ((SELECT NVL(SUM(PPOC.AMOUNT),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP, T_PNR_PAX_OND_CHARGES PPOC, T_CHARGE_RATE CR ");
						sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
						sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
						sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
						sb.append(" ) / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)))) ELSE a.total_sur_amount_paycur ");
						sb.append(" END AS total_sur_amount_paycur, ");
						
						sb.append(" CASE WHEN a.total_sur_amount <> 0 THEN ");
						sb.append(" (a.total_sur_amount +  ((SELECT NVL(SUM(PPOC.AMOUNT),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP, T_PNR_PAX_OND_CHARGES PPOC, T_CHARGE_RATE CR ");
						sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
						sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
						sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
						sb.append(" )))  ELSE a.total_sur_amount END AS total_sur_amount, ");
						
						sb.append(" CASE WHEN a.total_sur_amount <> 0 THEN ");
						sb.append(" (SELECT NVL(SUM(PPOC.AMOUNT),0)  FROM T_PNR_PAX_OND_PAYMENTS PPOP,T_PNR_PAX_OND_CHARGES PPOC,T_CHARGE_RATE CR ");
						sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID  AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
						sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
						sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID  AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
						sb.append(" ) ELSE 0 END AS TOTAL_ANCI_SUR_AMOUNT, ");
						
						sb.append(" CASE WHEN (a.total_mod_amount    <> 0) THEN (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN' ) ELSE 0 END AS total_penalty, ");
						
						sb.append(" CASE WHEN a.total_sur_amount <> 0 THEN ");
						sb.append(" (SELECT NVL(SUM(PPOC.AMOUNT),0)  FROM T_PNR_PAX_OND_PAYMENTS PPOP,T_PNR_PAX_OND_CHARGES PPOC,T_CHARGE_RATE CR ");
						sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID  AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
						sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
						sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID  AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
						sb.append(" ) / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)) ELSE 0 END AS TOTAL_ANCI_SUR_AMOUNT_PAYCUR, ");
						
						sb.append(" (CASE WHEN (a.total_mod_amount    <> 0) THEN (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN' ) ELSE 0 END) / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)) AS total_penalty_paycur, ");
														  
						sb.append(" CASE ");
						sb.append(" WHEN (a.total_mod_amount <> 0) ");
						sb.append(" THEN CASE WHEN (a.total_mod_amount > 0 AND (a.total_fare_amount > 0 OR a.total_tax_amount    > 0 OR a.total_sur_amount > 0))");
						sb.append(" THEN ((a.total_fare_amount + a.total_tax_amount+ a.total_sur_amount+a.total_mod_amount) + ");
						sb.append(" (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN')) ELSE (a.total_mod_amount + ");
						sb.append(" (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN')) END ");					
						sb.append(" ELSE CASE WHEN (a.total_fare_amount > 0 ");
						sb.append(" OR a.total_tax_amount    > 0 OR a.total_sur_amount    > 0) ");
						sb.append(" THEN ((a.total_fare_amount + a.total_tax_amount+ a.total_sur_amount+a.total_mod_amount) + ");
						sb.append(" (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN')) ELSE a.total_mod_amount END ");
						sb.append(" END AS total_mod_amount, ");
						
						sb.append(" CASE ");
						sb.append(" WHEN (a.total_mod_amount_paycur <> 0) ");
						sb.append(" THEN CASE WHEN (a.total_mod_amount_paycur > 0 AND (a.total_fare_amount_paycur > 0 OR a.total_tax_amount_paycur    > 0 OR a.total_sur_amount_paycur > 0))");
						sb.append(" THEN ((a.total_sur_amount_paycur + a.total_tax_amount_paycur+ a.total_sur_amount_paycur+a.total_mod_amount_paycur) + ");
						sb.append(" ((SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN') / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)))) ELSE (a.total_mod_amount_paycur + ");
						sb.append(" ((SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN') / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)))) END ");					
						sb.append(" ELSE CASE WHEN (a.total_fare_amount_paycur > 0 ");
						sb.append(" OR a.total_tax_amount_paycur    > 0 OR a.total_tax_amount_paycur    > 0) ");
						sb.append(" THEN ((a.total_fare_amount_paycur + a.total_tax_amount_paycur+ a.total_sur_amount_paycur+a.total_mod_amount_paycur) + ");
						sb.append(" ((SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
						sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
						sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN') / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)))) ELSE a.total_mod_amount_paycur END ");
						sb.append(" END AS total_mod_amount_paycur, ");					
					} else { //AEROMART-3166
				//		sb.append("a.total_sur_amount_paycur  as total_sur_amount_paycur,");
				//		sb.append(" a.total_sur_amount AS total_sur_amount, ");
						sb.append("CASE WHEN (a.total_mod_amount = 0 OR a.total_mod_amount    > 0) AND (a.total_fare_amount > 0 ");
						sb.append(" OR a.total_tax_amount > 0 OR a.total_sur_amount > 0) ");
						sb.append(" THEN (a.total_fare_amount + a.total_tax_amount+ a.total_sur_amount+a.total_mod_amount) ");
						sb.append(" ELSE a.total_mod_amount END AS total_mod_amount,");
						
						sb.append("CASE WHEN (a.total_mod_amount_paycur = 0 OR a.total_mod_amount_paycur > 0) ");
						sb.append(" AND (a.total_fare_amount_paycur > 0 OR a.total_tax_amount_paycur > 0 OR a.total_sur_amount_paycur > 0) ");
						sb.append(" THEN (a.total_fare_amount_paycur + a.total_tax_amount_paycur+ a.total_sur_amount_paycur+a.total_mod_amount_paycur) ");
						sb.append(" ELSE a.total_mod_amount_paycur END AS total_mod_amount_paycur,");
						sb.append(" 0 as TOTAL_ANCI_SUR_AMOUNT, ");
						sb.append(" 0 as total_penalty, ");
						sb.append(" 0 as TOTAL_ANCI_SUR_AMOUNT_PAYCUR, ");
						sb.append(" 0 as total_penalty_paycur, ");
					}
					if (reportsSearchCriteria.isInbaseCurr()) {
						sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "'    AS payment_currency_code");
					} else {
						sb.append("   a.payment_currency_code AS payment_currency_code");
					}
					sb.append(", null product_type ");
					sb.append(", 'E_RES' as entity_code ");
					sb.append(" FROM t_pax_txn_breakdown_summary a, t_pax_transaction b");
					if (reportsSearchCriteria.getSearchFlightType() != null) {
						sb.append(", t_pnr_passenger pp");
					}
					if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
						sb.append(", t_reservation res");
					}
					sb.append(" WHERE a.pax_txn_id = b.txn_id");
					sb.append(" AND (b.payment_carrier_code IS NULL OR b.payment_carrier_code = '"
							+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
					if (reportsSearchCriteria.getSearchFlightType() != null) {
						sb.append(" AND pp.pnr_pax_id = b.pnr_pax_id");
					}
					if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
						sb.append(" AND pp.pnr = res.pnr");
						sb.append(" AND res.fare_discount_code = '" + reportsSearchCriteria.getFareDiscountCode() + "'");
					}
					sb.append(" AND b.tnx_date BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
							+ "'  ||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtFromOffsetInMinutesStr);
					sb.append(" AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
							+ "'   ||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("	AND"
								+ ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("	AND"
								+ ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					if (AppSysParamsUtil.isModificationFilterEnabled()) {
						if (reportsSearchCriteria.isSales() && reportsSearchCriteria.isRefund()
								&& !reportsSearchCriteria.isModificationDetails()) {
							sb.append("	AND a.total_mod_amount >= 0");
						} else if (reportsSearchCriteria.isSales() && !reportsSearchCriteria.isRefund()
								&& !reportsSearchCriteria.isModificationDetails()) {
							sb.append("	AND a.total_mod_amount = 0");
						} else if (!reportsSearchCriteria.isSales() && reportsSearchCriteria.isModificationDetails()) {
							sb.append("	AND a.total_mod_amount <> 0");
						}
					}
					sb.append(" AND B.SALES_CHANNEL_CODE <> 11");
					if (reportsSearchCriteria.getSearchFlightType() != null) {
						// NOTE : if a reservation has at-least a international flight, total fare considered to be
						// international fare
						sb.append(" AND " + (reportsSearchCriteria.getSearchFlightType().equals("INT") ? "" : "NOT") + " EXISTS (");
						sb.append("       SELECT pp1.pnr");
						sb.append("         FROM t_pax_transaction b1,");
						sb.append("              t_pnr_segment ps,");
						sb.append("              t_pnr_passenger pp1,");
						sb.append("              t_flight_segment fs,");
						sb.append("              t_flight f");
						sb.append("        WHERE ps.pnr = pp1.pnr");
						sb.append("         AND pp.pnr = ps.pnr");
						sb.append("         AND pp1.pnr_pax_id = b1.pnr_pax_id");
						sb.append("         AND pp1.pnr_pax_id = b.pnr_pax_id");
						sb.append("         AND pp.pnr_pax_id = b1.pnr_pax_id");
						sb.append("         AND ps.flt_seg_id = fs.flt_seg_id");
						sb.append("         AND fs.flight_id = f.flight_id");
						sb.append("         AND f.flight_type = 'INT'");
						sb.append("         AND (   b1.payment_carrier_code IS NULL");
						sb.append("              OR b1.payment_carrier_code = '" + AppSysParamsUtil.getDefaultCarrierCode() + "')");
						if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
							sb.append("		AND"
									+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
						}
						if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
								&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
							sb.append("		AND"
									+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
						}
						sb.append("         AND b1.sales_channel_code <> 11)");
					}
	
					// External SALES - UNION
					sb.append("	UNION ALL");
					sb.append(" SELECT P.AGENT_CODE     AS AGENT_CODE              ,");
					sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
					sb.append(" P.AMOUNT           		AS TOTAL_AMOUNT            ,");
					sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_AMOUNT_PAYCUR     ,");
					sb.append(" P.AMOUNT           		AS TOTAL_FARE_AMOUNT       ,");
					sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_FARE_AMOUNT_PAYCUR,");
					sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
					sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
					sb.append(" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT, 31,P.AMOUNT, 33,P.AMOUNT, 5,P.AMOUNT, 29,P.AMOUNT,26,P.AMOUNT,24,P.AMOUNT,23,P.AMOUNT,25,P.AMOUNT,22,P.AMOUNT,48,P.AMOUNT,0) AS TOTAL_MOD_AMOUNT,");
					sb.append(" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT_PAYCUR, 31,P.AMOUNT_PAYCUR, 33,P.AMOUNT_PAYCUR, 5,P.AMOUNT_PAYCUR, 29,P.AMOUNT_PAYCUR,26,P.AMOUNT_PAYCUR,24,P.AMOUNT_PAYCUR,23,P.AMOUNT_PAYCUR,25,P.AMOUNT_PAYCUR,22,P.AMOUNT_PAYCUR,48,P.AMOUNT_PAYCUR,0) AS TOTAL_MOD_AMOUNT_PAYCUR ,");
					
					sb.append(" 0 as TOTAL_ANCI_SUR_AMOUNT, ");
					sb.append(" 0 as total_penalty, ");
					sb.append(" 0 as TOTAL_ANCI_SUR_AMOUNT_PAYCUR, ");
					sb.append(" 0 as total_penalty_paycur, ");
					
					if (reportsSearchCriteria.isInbaseCurr()) {
						sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE");
					} else {
						sb.append(" P.PAYCUR_CODE AS PAYMENT_CURRENCY_CODE");
					}
					sb.append(", null product_type ");
					sb.append(", 'E_RES' as entity_code ");
					sb.append("	FROM T_PAX_EXT_CARRIER_TRANSACTIONS P");
					sb.append("		WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
							+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtFromOffsetInMinutesStr);
					sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
							+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
					}
					sb.append("		AND P.SALES_CHANNEL_CODE <> 11");
	
					// External SALES before 11/06/2011 Dry sell Phase 2 release- UNION
					sb.append("	UNION ALL");
					sb.append(" SELECT P.AGENT_CODE     AS AGENT_CODE              ,");
					sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
					sb.append(" -1 * P.AMOUNT           AS TOTAL_AMOUNT            ,");
					sb.append(" -1 * P.AMOUNT_PAYCUR    AS TOTAL_AMOUNT_PAYCUR     ,");
					sb.append(" -1 * P.AMOUNT           AS TOTAL_FARE_AMOUNT       ,");
					sb.append(" -1 * P.AMOUNT_PAYCUR    AS TOTAL_FARE_AMOUNT_PAYCUR,");
					sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
					sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
					sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
					sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR ,");
					sb.append(" 0 						as TOTAL_ANCI_SUR_AMOUNT, ");
					sb.append(" 0 						as total_penalty, ");
					sb.append(" 0 as TOTAL_ANCI_SUR_AMOUNT_PAYCUR, ");
					sb.append(" 0 as total_penalty_paycur, ");
					if (reportsSearchCriteria.isInbaseCurr()) {
						sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE");
					} else {
						sb.append(" P.PAYCUR_CODE AS PAYMENT_CURRENCY_CODE ");
					}
					sb.append(", null product_type");
					sb.append(", 'E_RES' as entity_code ");
					sb.append("	FROM T_PAX_EXT_CC_TRANSACTION_OLD P");
					sb.append("		WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
							+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtFromOffsetInMinutesStr);
					sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
							+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
					}
					sb.append("		AND P.SALES_CHANNEL_CODE <> 11");
	
					// external products
					if (reportsSearchCriteria.getPaymentTypes().contains("19")) {
						agentNominalCodes.add("19");
					}
					if (reportsSearchCriteria.getPaymentTypes().contains("25")) {
						agentNominalCodes.add("20");
					}
					if (reportsSearchCriteria.getPaymentTypes().contains("36")) {
						agentNominalCodes.add("21");
					}
					if (reportsSearchCriteria.getPaymentTypes().contains("37")) {
						agentNominalCodes.add("22");
					}
	
					if (!agentNominalCodes.isEmpty()) {
						sb.append(" union all ");
						sb.append("	select at.agent_code, at.nominal_code, -1*at.amount, -1*at.amount_local, null, null, null, null, null, null, null, null,null,null,null,null, ");
						sb.append("		at.currency_code, at.product_type ,p.ENTITY_CODE ");
						sb.append("	from t_agent_transaction at ,T_PRODUCT p ");
						sb.append("	where at.PRODUCT_TYPE = p.PRODUCT_TYPE ");
	
						if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
							sb.append("	and"
									+ ReportUtils.getReplaceStringForIN("at.agent_code", reportsSearchCriteria.getAgents(), false));
						}
						if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
								&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
							sb.append("	and"
									+ ReportUtils.getReplaceStringForIN("at.agent_code", reportsSearchCriteria.getAgents(), false));
						}
	
						sb.append("	and" + ReportUtils.getReplaceStringForIN("at.nominal_code", agentNominalCodes, false));
	
						sb.append("		and at.tnx_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom() + "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
						sb.append(gmtFromOffsetInMinutesStr);
						sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
						sb.append(gmtToOffsetInMinutesStr);
					}
				}


				sb.append(" ) T,  T_AGENT A, T_AGENT_SUMMARY TAS");// end sub select 2
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" ,  T_AGENT_TRANSACTION TAT");
				}
				sb.append(" WHERE ( t.product_type is not null or T.NOMINAL_CODE IN(" + singleDataValuePayments + ") ) ");
				sb.append(" AND A.AGENT_CODE               = T.AGENT_CODE");
				sb.append(" AND A.IS_LCC_INTEGRATION_AGENT = 'N'");
				sb.append(" AND A.AGENT_CODE = TAS.AGENT_CODE ");
				if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
					sb.append(" AND A.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
				}
				
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" AND A.AGENT_CODE               = TAT.AGENT_CODE(+) ");
					sb.append(" AND TAT.TNX_DATE(+) BETWEEN TO_DATE('" + reportsSearchCriteria.getToDate()
							+ "' || ' 00:00:00' , 'dd-mon-yyyy hh24:mi:ss')");
					sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
							+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
				}
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( A.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"'))");

				}
				sb.append(" AND t.entity_code='" + reportsSearchCriteria.getEntity()+"' ");
				sb.append(
						" GROUP BY A.AGENT_CODE,  A.AGENT_NAME ,  A.ACCOUNT_CODE, A.AGENT_IATA_NUMBER, T.PAYMENT_CURRENCY_CODE, A.CREDIT_LIMIT, TAS.AVILABLE_CREDIT, t.entity_code");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , TAT.txn_id ");
				}
				sb.append(" )"); // end sub select 1

				sb.append(" order by agent_code, payment_currency_code ");
			}

		} else {
			if (reportsSearchCriteria.getPaymentSource() != null && reportsSearchCriteria.getPaymentSource().equals("EXTERNAL")) {

				sb.append(" select p.agent_code,agent_name,p.account_code, ");
				sb.append(" decode(nominal_code,36,'BSP',37,'BSP',19,'On Account',25,'On Account',18,'Cash',26,'Cash',15,'Visa',22,'Visa',16,'Master',23,'Master',28,'Amex',29,'Amex',17,'Diners',24,'Diners',30,'Generic',31,'Generic',32,'CMI',33,'CMI',5,'CREDIT',6,'CREDIT',48,'VOUCHER') description, ");
				sb.append(" to_char(payment_date,'dd-MON-yyyy')payment_date, ");
				sb.append("eticket , ");
				sb.append(" pnr,passenger,F_COMPANY_PYMT_REPORT(pnr)routing,username,(-1* p.pmt_amt + ext.pmt_amt) pmt_amt, ");
				sb.append(" (-1*p.ref_amt + ext.ref_amt) ref_amt ");
				sb.append("	,'' as ACTUAL_PAY, '' as PAY_REF");
				sb.append(" FROM ");
				sb.append(" ( ");
				sb.append(" SELECT A.agent_code,A.agent_name, A.account_code,n.nominal_code,p.pnr_pax_id,");

				sb.append(" nvl(F_CONCATENATE_LIST ( CURSOR(SELECT  (CASE WHEN pet.ext_e_ticket_number is not null "
						+ " THEN pet.ext_e_ticket_number "
						+ " ELSE pet.e_ticket_number "
						+ " END) FROM t_pnr_passenger pp,t_pax_e_ticket pet, t_pnr_pax_fare_seg_e_ticket ppet "
						+ " WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id = p.pnr_pax_id AND pet.pax_e_ticket_id = ppet.pax_e_ticket_id),'-'),' ') AS eticket,");

				sb.append(" T.tnx_date payment_date,p.pnr pnr,p.first_name||' '||p.last_name passenger,u.display_name username, ");
				sb.append(" sum(decode(t.nominal_code, 36,t.amount, 30,t.amount, 32,t.amount, 6,t.amount, 28,t.amount,18,t.amount,17,t.amount,16,t.amount,19,t.amount,15,t.amount,48,t.amount,0))pmt_amt, ");
				sb.append(" sum(decode(t.nominal_code, 37,t.amount, 31,t.amount, 33,t.amount, 5,t.amount, 29,t.amount,26,t.amount,24,t.amount,23,t.amount,25,t.amount,22,t.amount,0))ref_amt, 'E_RES' as entity_code");
				sb.append(" FROM t_pnr_passenger p, T_PAX_TRANSACTION t,T_PAX_TRNX_NOMINAL_CODE N,t_agent a, t_user u ");
				sb.append(" where t.nominal_code in(" + singleDataValuePayments + ")  and ");
				sb.append(" t.user_id = u.user_id and ");
				sb.append(" p.pnr_pax_id=t.pnr_pax_id and ");
				sb.append(" T.nominal_code=N.nominal_code and ");
				sb.append(" a.agent_code=t.agent_code and ");
				sb.append(" tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" and to_date('" + reportsSearchCriteria.getDateRangeTo() + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("T.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
				}
				if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
					sb.append(" AND a.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
				}
				sb.append(" and T.sales_channel_code <> 11 ");
				sb.append(" group by p.pnr_pax_id,n.nominal_code,A.agent_code,A.agent_name,A.account_code,");
				// nvl(f_get_eticket_string(p.pnr_pax_id),' '), ");

				sb.append("nvl(F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = p.pnr_pax_id),'-'),' '),");

				sb.append(" pnr,p.first_name||' '||p.last_name,u.display_name,T.tnx_date ");
				sb.append(" )P , ");

				sb.append(" 			                    ( ");
				sb.append(" SELECT ag.agent_code,pnr_pax_id , ");
				sb.append(" sum (decode (px.CR_DR,'CR',round(px.amount,2) * -1,0) ) ref_amt , ");
				sb.append(" sum (decode (px.cr_dr,'DR',round(px.amount,2),0))as pmt_amt ");

				sb.append(" FROM t_pax_ext_transactions px, t_agent_transaction ag ,t_product p ");
				sb.append(" where px.txn_id = ag.txn_id ");
				sb.append(" and p.product_type = ag.product_type ");
				sb.append(" and px.nominal_code in (10,11) ");
				sb.append(" and  tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" and to_date('" + reportsSearchCriteria.getDateRangeTo() + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("ag.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("           AND ");
					sb.append(ReportUtils.getReplaceStringForIN("ag.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
				}
				if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
					sb.append(" AND a.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
				}
				sb.append(" and px.sales_channel_code <> 11 ");
				
				sb.append(" and p.entity_code ='"+ reportsSearchCriteria.getEntity() + "' ");
				
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( ag.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '"+reportsSearchCriteria.getGsaCode()+"'))");

				}
				sb.append(" group by pnr_pax_id, ag.agent_code ");
				sb.append(" ) ext ");
				sb.append(" where P.agent_code = ext.agent_code ");
				sb.append(" and P.pnr_pax_id = ext.pnr_pax_id ");
				sb.append(" and p.entity_code ='"+ reportsSearchCriteria.getEntity() + "' ");
				sb.append(" order by agent_code,description,payment_date,pnr ");

			} else if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewCompanyPaymentDetailsByCurrencyQuery(reportsSearchCriteria,
						singleDataValuePayments));
			}

			else {
				// Following query combines existing payments with LCC interline payments in paid currency & DRY sales
				// payments
				// FIXME fare breakdown is not available in the llc payments at the moment. return 0 for all the fields
				// which does not have values in lcc_t_pnr_transaction

				boolean showFarebasis = AppSysParamsUtil.isShowFarebasisCodesInCompanyPayments();

				sb.append("select agent_code,agent_name,NVL(account_code,'N/A') as account_code, decode(nominal_code,19,'On Account',25,'On Account',18,'Cash',26,'Cash',15,'Visa',22,'Visa',16,'Master',23,'Master',28,'Amex',29,'Amex',17,'Diners',24,'Diners',"
						+ "30,'Generic',31,'Generic',32,'CMI',33,'CMI',5,'CREDIT',6,'CREDIT' ,36,'BSP',37,'BSP',48,'VOUCHER') description,");
				if(AppSysParamsUtil.showPaymentDateWithTimestamp()){
					sb.append("to_char(payment_date,'YYYY-MM-DD hh24:mm:ss')payment_date,");
				}else {
					sb.append("to_char(payment_date,'dd-MON-yyyy')payment_date,");
				}
				sb.append("		pnr, external_rec_locator, passenger,routing,username,");
				sb.append("		 total_amount,  total_amount_paycur,");
				sb.append("		 total_fare_amount,  total_fare_amount_paycur, ");
				sb.append("		 total_tax_amount,  total_tax_amount_paycur,");
				sb.append("		 total_sur_amount,  total_sur_amount_paycur, ");
				sb.append("		 total_modify,  total_refund,");
				sb.append("		 total_modify_local,  total_refund_local, payment_currency_code, ");
				sb.append(" (total_amount + total_refund) pmt_amt,");
				sb.append(" (total_amount_paycur + total_refund_local) pmt_amt_local ,");
				sb.append(
						" eticket , ACTUAL_PAY as ACTUAL_PAY, PAY_REF as PAY_REF,(total_amount * 0.00)REF_AMT , agent_pay_ref as agent_pay_ref,TOTAL_ANCI_SUR_AMOUNT,TOTAL_PENALTY,TOTAL_ANCI_SUR_AMOUNT_PAYCUR,TOTAL_PENALTY_PAYCUR,product_type as product_type,(' ') AS INFANT");

				if (showFarebasis)
					sb.append(" , FARE_BASIS_CODES AS TOTAL_FARE_BASIS_CODES ");
				else
					sb.append(", NULL AS TOTAL_FARE_BASIS_CODES ");

				sb.append("FROM ");

				sb.append(" ( "); // begin select 1
				sb.append(" SELECT C.AGENT_CODE     ,");
				sb.append(" C.AGENT_NAME           ,");
				sb.append(" C.ACCOUNT_CODE         ,");
				sb.append(" V.NOMINAL_CODE         ,");
				sb.append(" V.TNX_DATE PAYMENT_DATE,");
				sb.append(" PNR PNR                ,");
				sb.append(" external_rec_locator external_rec_locator ,");
				sb.append(" V.FIRST_NAME || ' ' || V.LAST_NAME PASSENGER,");
				sb.append(" U.DISPLAY_NAME USERNAME,");
				sb.append(" NVL(V.ETICKET_ID, ' ') AS ETICKET,");
				sb.append(" (-1 * SUM(TOTAL_AMOUNT)) TOTAL_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_AMOUNT_PAYCUR)) TOTAL_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_FARE_AMOUNT)) TOTAL_FARE_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT)) TOTAL_TAX_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT)) TOTAL_SUR_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(DECODE(V.NOMINAL_CODE, 36, TOTAL_MOD_AMOUNT, 30, TOTAL_MOD_AMOUNT, 32, TOTAL_MOD_AMOUNT, 6, TOTAL_MOD_AMOUNT, 28, TOTAL_MOD_AMOUNT, 18, TOTAL_MOD_AMOUNT, 17, TOTAL_MOD_AMOUNT, 16, TOTAL_MOD_AMOUNT, 19, TOTAL_MOD_AMOUNT, 15, TOTAL_MOD_AMOUNT, 48, TOTAL_MOD_AMOUNT, 0))) TOTAL_MODIFY                                                                     ,");
				sb.append(" SUM(DECODE(V.NOMINAL_CODE, 37, TOTAL_MOD_AMOUNT, 31, TOTAL_MOD_AMOUNT, 33, TOTAL_MOD_AMOUNT, 5, TOTAL_MOD_AMOUNT, 29, TOTAL_MOD_AMOUNT, 26, TOTAL_MOD_AMOUNT, 24, TOTAL_MOD_AMOUNT, 23, TOTAL_MOD_AMOUNT, 25, TOTAL_MOD_AMOUNT, 22, TOTAL_MOD_AMOUNT, 0)) TOTAL_REFUND                                                                            ,");
				sb.append(" (-1 * SUM(DECODE(V.NOMINAL_CODE, 36, TOTAL_MOD_AMOUNT_PAYCUR, 30, TOTAL_MOD_AMOUNT_PAYCUR, 32, TOTAL_MOD_AMOUNT_PAYCUR, 6, TOTAL_MOD_AMOUNT_PAYCUR, 28, TOTAL_MOD_AMOUNT_PAYCUR, 18, TOTAL_MOD_AMOUNT_PAYCUR, 17, TOTAL_MOD_AMOUNT_PAYCUR, 16, TOTAL_MOD_AMOUNT_PAYCUR, 19, TOTAL_MOD_AMOUNT_PAYCUR, 15, TOTAL_MOD_AMOUNT_PAYCUR, 48, TOTAL_MOD_AMOUNT_PAYCUR,0))) TOTAL_MODIFY_LOCAL,");
				sb.append(" SUM(DECODE(V.NOMINAL_CODE, 37, TOTAL_MOD_AMOUNT_PAYCUR, 31, TOTAL_MOD_AMOUNT_PAYCUR, 33, TOTAL_MOD_AMOUNT_PAYCUR, 5, TOTAL_MOD_AMOUNT_PAYCUR, 29, TOTAL_MOD_AMOUNT_PAYCUR, 26, TOTAL_MOD_AMOUNT_PAYCUR, 24, TOTAL_MOD_AMOUNT_PAYCUR, 23, TOTAL_MOD_AMOUNT_PAYCUR, 25, TOTAL_MOD_AMOUNT_PAYCUR, 22, TOTAL_MOD_AMOUNT_PAYCUR, 0)) TOTAL_REFUND_LOCAL       ,");
				sb.append(" PAYMENT_CURRENCY_CODE, ");
				sb.append(" V.ROUTING ");
				sb.append("	,v.ACTUAL_PAY as ACTUAL_PAY, v.PAY_REF as PAY_REF, v.agent_pay_ref as agent_pay_ref ,SUM(V.TOTAL_ANCI_SUR_AMOUNT) TOTAL_ANCI_SUR_AMOUNT, SUM(V.TOTAL_PENALTY) TOTAL_PENALTY , SUM(V.TOTAL_ANCI_SUR_AMOUNT_PAYCUR) TOTAL_ANCI_SUR_AMOUNT_PAYCUR, SUM(V.TOTAL_PENALTY_PAYCUR) TOTAL_PENALTY_PAYCUR, v.product_type as product_type ,v.entity_code");

				if (showFarebasis)
					sb.append(" ,V.FARE_BASIS_CODES  AS FARE_BASIS_CODES");

				sb.append(" FROM");
				sb.append(" (");// begin select 2
				sb.append(" SELECT ");
				sb.append(" P.PNR                      AS PNR                     ,");
				sb.append(" r.external_rec_locator external_rec_locator ,");
				sb.append(" B.AGENT_CODE               AS AGENT_CODE              ,");
				sb.append(" B.NOMINAL_CODE             AS NOMINAL_CODE            ,");
				sb.append(" B.USER_ID                  AS USER_ID                 ,");
				sb.append(" P.PNR_PAX_ID               AS PNR_PAX_ID              ,");
				sb.append(" P.FIRST_NAME               AS FIRST_NAME              ,");
				sb.append(" P.LAST_NAME                AS LAST_NAME               ,");
				sb.append(" A.TIMESTAMP                AS TNX_DATE                ,");
				sb.append(" A.TOTAL_AMOUNT             AS TOTAL_AMOUNT            ,");
				sb.append(" A.TOTAL_AMOUNT_PAYCUR      AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" A.TOTAL_FARE_AMOUNT        AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" A.TOTAL_FARE_AMOUNT_PAYCUR AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" A.TOTAL_TAX_AMOUNT         AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" A.TOTAL_TAX_AMOUNT_PAYCUR  AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				/*sb.append(" A.TOTAL_SUR_AMOUNT         AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" A.TOTAL_SUR_AMOUNT_PAYCUR  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append("CASE WHEN (A.TOTAL_MOD_AMOUNT = 0 OR A.TOTAL_MOD_AMOUNT    > 0) AND (A.TOTAL_FARE_AMOUNT > 0 ");
				sb.append(" OR A.TOTAL_TAX_AMOUNT    > 0 OR A.TOTAL_SUR_AMOUNT    > 0) ");
				sb.append(" THEN (A.TOTAL_FARE_AMOUNT + A.TOTAL_TAX_AMOUNT+ A.TOTAL_SUR_AMOUNT+A.TOTAL_MOD_AMOUNT) ");
				sb.append(" ELSE A.TOTAL_MOD_AMOUNT END AS TOTAL_MOD_AMOUNT,");
				sb.append(
						"CASE WHEN (A.TOTAL_MOD_AMOUNT_PAYCUR = 0 OR A.TOTAL_MOD_AMOUNT_PAYCUR    > 0) AND (A.TOTAL_FARE_AMOUNT_PAYCUR > 0 ");
				sb.append("	OR A.TOTAL_TAX_AMOUNT_PAYCUR           > 0 OR A.TOTAL_SUR_AMOUNT_PAYCUR           > 0) ");
				sb.append(
						"	THEN (A.TOTAL_FARE_AMOUNT_PAYCUR + A.TOTAL_TAX_AMOUNT_PAYCUR+ A.TOTAL_SUR_AMOUNT_PAYCUR+A.TOTAL_MOD_AMOUNT_PAYCUR) ");
				sb.append(" ELSE A.TOTAL_MOD_AMOUNT_PAYCUR END AS TOTAL_MOD_AMOUNT_PAYCUR,");			*/				
				
				if (AppSysParamsUtil.displayAmountsInSeperateColumnsWithCategory()) {
					sb.append(" (a.total_sur_amount +  ((SELECT NVL(SUM(PPOC.AMOUNT),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP, T_PNR_PAX_OND_CHARGES PPOC, T_CHARGE_RATE CR ");
					sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
					sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
					sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
					sb.append(" ))) AS total_sur_amount, ");
					
					sb.append(" (a.TOTAL_SUR_AMOUNT_PAYCUR +  ((SELECT NVL(SUM(PPOC.AMOUNT),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP, T_PNR_PAX_OND_CHARGES PPOC, T_CHARGE_RATE CR ");
					sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
					sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
					sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
					sb.append(" ) / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)))) AS TOTAL_SUR_AMOUNT_PAYCUR, ");

				} else {
					sb.append(" A.TOTAL_SUR_AMOUNT         AS TOTAL_SUR_AMOUNT        ,");
					sb.append(" A.TOTAL_SUR_AMOUNT_PAYCUR  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				}
				
				
				if (AppSysParamsUtil.displayAmountsInSeperateColumnsWithCategory()) {
					sb.append(" CASE ");
					sb.append(" WHEN (a.total_mod_amount = 0 OR a.total_mod_amount    > 0) AND (a.total_fare_amount > 0 ");
					sb.append(" OR a.total_tax_amount    > 0 OR a.total_sur_amount    > 0) ");
					sb.append(" THEN ((a.total_fare_amount + a.total_tax_amount+ a.total_sur_amount+a.total_mod_amount) + ");
					sb.append(" (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
					sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
					sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN'))");
					sb.append(" ELSE (a.total_mod_amount + "); 
					sb.append(" (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
					sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
					sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN')) ");
					sb.append(" END AS total_mod_amount, ");
					
					sb.append(" CASE ");
					sb.append(" WHEN (a.total_mod_amount_paycur = 0 OR a.total_mod_amount_paycur    > 0) AND (a.total_fare_amount_paycur > 0 ");
					sb.append(" OR a.total_tax_amount_paycur    > 0 OR a.total_sur_amount_paycur    > 0) ");
					sb.append(" THEN ((a.total_fare_amount_paycur + a.total_tax_amount_paycur+ a.total_sur_amount_paycur+a.total_mod_amount_paycur) + ");
					sb.append(" ((SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
					sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
					sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN') / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date))))");
					sb.append(" ELSE (a.total_mod_amount_paycur + "); 
					sb.append(" ((SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
					sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
					sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN') / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)))) ");
					sb.append(" END AS total_mod_amount_paycur, ");
				} else {
					sb.append("CASE WHEN (A.TOTAL_MOD_AMOUNT = 0 OR A.TOTAL_MOD_AMOUNT    > 0) AND (A.TOTAL_FARE_AMOUNT > 0 ");
					sb.append(" OR A.TOTAL_TAX_AMOUNT    > 0 OR A.TOTAL_SUR_AMOUNT    > 0) ");
					sb.append(" THEN (A.TOTAL_FARE_AMOUNT + A.TOTAL_TAX_AMOUNT+ A.TOTAL_SUR_AMOUNT+A.TOTAL_MOD_AMOUNT) ");
					sb.append(" ELSE A.TOTAL_MOD_AMOUNT END AS TOTAL_MOD_AMOUNT,");
					sb.append("CASE WHEN (A.TOTAL_MOD_AMOUNT_PAYCUR = 0 OR A.TOTAL_MOD_AMOUNT_PAYCUR    > 0) AND (A.TOTAL_FARE_AMOUNT_PAYCUR > 0 ");
					sb.append("	OR A.TOTAL_TAX_AMOUNT_PAYCUR           > 0 OR A.TOTAL_SUR_AMOUNT_PAYCUR           > 0) ");
					sb.append("	THEN (A.TOTAL_FARE_AMOUNT_PAYCUR + A.TOTAL_TAX_AMOUNT_PAYCUR+ A.TOTAL_SUR_AMOUNT_PAYCUR+A.TOTAL_MOD_AMOUNT_PAYCUR) ");
					sb.append(" ELSE A.TOTAL_MOD_AMOUNT_PAYCUR END AS TOTAL_MOD_AMOUNT_PAYCUR,");
				}
				
				if (reportsSearchCriteria.isInbaseCurr()) {
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE, ");
				} else {
					sb.append(" A.PAYMENT_CURRENCY_CODE    AS PAYMENT_CURRENCY_CODE, ");
				}
				// sb.append(" F_GET_ETICKET_STRING(P.PNR_PAX_ID)         AS ETICKET_ID              ,");

				sb.append(" nvl(F_CONCATENATE_LIST ( CURSOR(SELECT  (CASE WHEN ppet.ext_e_ticket_number is not null "
						+ " THEN ppet.ext_e_ticket_number " + " ELSE pet.e_ticket_number "
						+ " END) FROM t_pnr_passenger pp,t_pax_e_ticket pet,  t_pnr_pax_fare_seg_e_ticket ppet "
						+ " WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = P.pnr_pax_id "
						+ " AND pet.pax_e_ticket_id = ppet.pax_e_ticket_id " + "),'-'),' ') AS ETICKET_ID,");

				sb.append(" F_COMPANY_PYMT_REPORT(P.PNR) AS ROUTING                ");
				sb.append("            	 	,actual_p.payment_mode as ACTUAL_PAY");
				sb.append("					,B.EXT_REFERENCE as PAY_REF");
				sb.append("					, f_concatenate_list ( cursor ");
				sb.append("						( select ext_reference ");
				sb.append("							from t_agent_transaction ");
				sb.append("							where pnr = r.pnr and product_type = 'G' ) ) agent_pay_ref ,  null product_type ");

				if (AppSysParamsUtil.displayAmountsInSeperateColumnsWithCategory()) {
					sb.append(" , (SELECT NVL(SUM(PPOC.AMOUNT),0)  FROM T_PNR_PAX_OND_PAYMENTS PPOP,T_PNR_PAX_OND_CHARGES PPOC,T_CHARGE_RATE CR ");
					sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID  AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
					sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
					sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID  AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
					sb.append(" ) AS TOTAL_ANCI_SUR_AMOUNT, ");
					sb.append("  CASE WHEN (a.total_mod_amount    <> 0) THEN (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
					sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
					sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN' ) ELSE 0 END AS total_penalty, ");
					
					sb.append(" (SELECT NVL(SUM(PPOC.AMOUNT),0)  FROM T_PNR_PAX_OND_PAYMENTS PPOP,T_PNR_PAX_OND_CHARGES PPOC,T_CHARGE_RATE CR ");
					sb.append(" WHERE PPOP.PNR_PAX_ID   = b.PNR_PAX_ID  AND PPOC.CHARGE_RATE_ID = CR.CHARGE_RATE_ID ");
					sb.append(" AND CR.CHARGE_CODE     IN ('ML','SSR','INS','SM','BG','HALA','AT') ");
					sb.append(" AND PPOP.PFT_ID         = PPOC.PFT_ID  AND PPOP.PAYMENT_TXN_ID = b.txn_id ");
					sb.append(" ) / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)) AS TOTAL_ANCI_SUR_AMOUNT_PAYCUR, ");
					
					sb.append(" (CASE WHEN (a.total_mod_amount    <> 0) THEN (SELECT NVL(SUM(PPOP.amount),0) FROM T_PNR_PAX_OND_PAYMENTS PPOP ");
					sb.append(" WHERE PPOP.PNR_PAX_ID      = b.PNR_PAX_ID AND PPOP.PAYMENT_TXN_ID    = b.txn_id ");
					sb.append(" AND PPOP.CHARGE_GROUP_CODE = 'PEN' ) ELSE 0 END) / (F_GET_CURR_RATE_TO_BASE(a.payment_currency_code, b.tnx_date)) AS total_penalty_paycur ");
					
				} else {
					sb.append(" , 0 as TOTAL_ANCI_SUR_AMOUNT, ");
					sb.append(" 0 as total_penalty ");
					sb.append(" , 0 as TOTAL_ANCI_SUR_AMOUNT_PAYCUR, ");
					sb.append(" 0 as total_penalty_paycur ");
				}
				if (showFarebasis) {
					sb.append("	, f_concatenate_list( CURSOR (SELECT G.fare_basis_code");
					sb.append(" from t_pnr_pax_fare E, t_ond_fare F, t_fare_rule G, t_pnr_segment pnrs, t_pnr_pax_fare_segment ppfs");
					sb.append(" where E.PNR_PAX_ID = P.PNR_PAX_ID");
					sb.append(" AND E.FARE_ID = F.FARE_ID");
					sb.append(" AND F.FARE_RULE_ID = G.FARE_RULE_ID");
					sb.append(" AND e.ppf_id       = ppfs.ppf_id");
					sb.append(" AND ppfs.pnr_seg_id = pnrs.pnr_seg_id");
					sb.append(" order by pnrs.segment_seq");
					sb.append(" ), ',') AS FARE_BASIS_CODES");
				}

				sb.append(" , 'E_RES' as entity_code ");
				sb.append(" FROM T_PAX_TXN_BREAKDOWN_SUMMARY A,  T_PAX_TRANSACTION B , T_PNR_PASSENGER P ");
				sb.append(" ,T_ACTUAL_PAYMENT_METHOD actual_p , t_reservation r ");

				if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
					sb.append(", T_RESERVATION RES");
				}
				sb.append(" WHERE A.PAX_TXN_ID = B.TXN_ID and p.pnr = r.pnr ");
				sb.append(" AND (B.PAYMENT_CARRIER_CODE IS NULL OR B.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
				sb.append(" AND B.PNR_PAX_ID     = P.PNR_PAX_ID");
				if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
					sb.append(" AND P.PNR = RES.PNR");
					sb.append(" AND RES.fare_discount_code = '" + reportsSearchCriteria.getFareDiscountCode() + "'");
				}
				sb.append(" AND B.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("	AND" + ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
							+ " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("	AND" + ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
							+ " ");
				}
				sb.append(" AND actual_p.payment_mode_id (+)= B.payment_mode_id ");
				if (AppSysParamsUtil.isModificationFilterEnabled()) {
					if (reportsSearchCriteria.isSales() && reportsSearchCriteria.isRefund()
							&& !reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND A.total_mod_amount >= 0");
					} else if (reportsSearchCriteria.isSales() && !reportsSearchCriteria.isRefund()
							&& !reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND A.total_mod_amount = 0");
					} else if (!reportsSearchCriteria.isSales() && reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND A.total_mod_amount <> 0");
					}
				}
				sb.append(" AND B.SALES_CHANNEL_CODE <> 11");
				
				if(AppSysParamsUtil.skipDummyPayments()){
					sb.append(" AND ( B.DUMMY_PAYMENT ='N'  AND P.STATUS NOT IN('OHD')  AND R.STATUS NOT IN('OHD')) ");
				}

				if (reportsSearchCriteria.getSearchFlightType() != null) {
					// NOTE : if a reservation has at-least a international flight, total fare considered to be
					// international fare
					sb.append(" AND " + (reportsSearchCriteria.getSearchFlightType().equals("INT") ? "" : "NOT") + " EXISTS (");
					sb.append(" SELECT pp1.pnr");
					sb.append("  FROM t_pax_transaction b1,");
					sb.append(" t_pnr_segment ps          ,");
					sb.append(" t_pnr_passenger pp1       ,");
					sb.append(" t_flight_segment fs       ,");
					sb.append(" t_flight f");
					sb.append(" WHERE ps.pnr                 = pp1.pnr");
					sb.append(" AND p.pnr                    = ps.pnr");
					sb.append(" AND pp1.pnr_pax_id           = b1.pnr_pax_id");
					sb.append(" AND pp1.pnr_pax_id           = b.pnr_pax_id");
					sb.append(" AND p.pnr_pax_id             = b1.pnr_pax_id");
					sb.append(" AND ps.flt_seg_id            = fs.flt_seg_id");
					sb.append(" AND fs.flight_id             = f.flight_id");
					sb.append(" AND f.flight_type            = 'INT'");
					sb.append(" AND ( b1.payment_carrier_code IS NULL");
					sb.append("              OR b1.payment_carrier_code = '" + AppSysParamsUtil.getDefaultCarrierCode() + "')");
					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					sb.append("         AND b1.sales_channel_code <> 11)");
				}

				// External SALES - UNION
				sb.append(" UNION ALL");
				sb.append(" SELECT ");
				sb.append(" X.PNR                   AS PNR                     ,");
				sb.append(" r.external_rec_locator external_rec_locator ,");
				sb.append(" P.AGENT_CODE      	    AS AGENT_CODE              ,");
				sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
				sb.append(" P.USER_ID               AS USER_ID                 ,");
				sb.append(" X.PNR_PAX_ID            AS PNR_PAX_ID              ,");
				sb.append(" X.FIRST_NAME            AS FIRST_NAME              ,");
				sb.append(" X.LAST_NAME             AS LAST_NAME               ,");
				sb.append(" P.TXN_TIMESTAMP         AS TNX_DATE                ,");
				sb.append(" P.AMOUNT           		AS TOTAL_AMOUNT            ,");
				sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" P.AMOUNT           		AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT, 31,P.AMOUNT, 33,P.AMOUNT, 5,P.AMOUNT, 29,P.AMOUNT,26,P.AMOUNT,24,P.AMOUNT,23,P.AMOUNT,25,P.AMOUNT,22,P.AMOUNT,0)                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT_PAYCUR, 31,P.AMOUNT_PAYCUR, 33,P.AMOUNT_PAYCUR, 5,P.AMOUNT_PAYCUR, 29,P.AMOUNT_PAYCUR,26,P.AMOUNT_PAYCUR,24,P.AMOUNT_PAYCUR,23,P.AMOUNT_PAYCUR,25,P.AMOUNT_PAYCUR,22,P.AMOUNT_PAYCUR,0)  AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				if (reportsSearchCriteria.isInbaseCurr()) {
					// for base currency report
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE, ");
				} else {
					sb.append(" P.PAYCUR_CODE AS PAYMENT_CURRENCY_CODE, ");// for payment currency report.
				}
				// sb.append(" F_GET_ETICKET_STRING(X.PNR_PAX_ID)       AS ETICKET_ID			   ,");

				sb.append(" nvl(F_CONCATENATE_LIST ( CURSOR(SELECT  (CASE WHEN ppet.ext_e_ticket_number is not null "
						+ " THEN ppet.ext_e_ticket_number " + " ELSE pet.e_ticket_number "
						+ " END) FROM t_pnr_passenger pp,t_pax_e_ticket pet,  t_pnr_pax_fare_seg_e_ticket ppet "
						+ " WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = X.pnr_pax_id "
						+ " AND pet.pax_e_ticket_id = ppet.pax_e_ticket_id " + "),'-'),' ') AS ETICKET_ID,");

				sb.append(" F_COMPANY_PYMT_REPORT(X.PNR, 'Y') AS ROUTING      ,");
				sb.append(" '' as ACTUAL_PAY ,");
				sb.append(" '' as PAY_REF ,");
				sb.append(" null as agent_pay_ref , ");
				sb.append("  null product_type ");
				sb.append(" , null AS TOTAL_ANCI_SUR_AMOUNT ");
				sb.append(" , null AS TOTAL_PENALTY ");
				sb.append(" , null AS TOTAL_ANCI_SUR_AMOUNT_PAYCUR ");
				sb.append(" , null AS TOTAL_PENALTY_PAYCUR ");				
				
				if (showFarebasis) {
					sb.append(" , '' AS FARE_BASIS_CODES");
				}

				sb.append(" , 'E_RES' as entity_code ");
				sb.append(" FROM T_PAX_EXT_CARRIER_TRANSACTIONS P, T_PNR_PASSENGER X , t_reservation r ");
				sb.append(" WHERE P.PNR_PAX_ID = X.PNR_PAX_ID and x.pnr = r.pnr ");
				sb.append(" AND P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
					sb.append("	AND" + ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
							+ " ");
				}
				if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
						&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append("	AND" + ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
							+ " ");
				}
				sb.append(" AND P.SALES_CHANNEL_CODE <> 11");


				if (reportsSearchCriteria.getPaymentTypes().contains("19")) {
					agentNominalCodes.add("19");
				}
				if (reportsSearchCriteria.getPaymentTypes().contains("25")) {
					agentNominalCodes.add("20");
				}
				if (reportsSearchCriteria.getPaymentTypes().contains("36")) {
					agentNominalCodes.add("21");
				}
				if (reportsSearchCriteria.getPaymentTypes().contains("37")) {
					agentNominalCodes.add("22");
				}

				if (!agentNominalCodes.isEmpty()) {

					sb.append(" union all ");

					sb.append(" select at.pnr, null, at.agent_code, at.nominal_code, at.user_id, null, null, null, at.tnx_date, -1 * at.amount_local, -1 * at.amount, ");
					sb.append(" 	null, null, null, null, null, null, null, null, at.currency_code, null, null, null, null, at.ext_reference, at.product_type,null, null,null,null ");

					if (showFarebasis) {
						sb.append(" , null ");
					}

					sb.append(" , p.entity_code ");
					sb.append(" from t_agent_transaction at , t_product p");
					sb.append("	where at.product_type = p.product_type ");

					if (!reportsSearchCriteria.isSelectedAllAgents() && !reportsSearchCriteria.isByStation()) {
						sb.append("	and"
								+ ReportUtils.getReplaceStringForIN("at.agent_code", reportsSearchCriteria.getAgents(), false));
					}
					if (reportsSearchCriteria.isByStation() && !reportsSearchCriteria.isSelectedAllAgents()
							&& reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
						sb.append("	and"
								+ ReportUtils.getReplaceStringForIN("at.agent_code", reportsSearchCriteria.getAgents(), false));
					}

					sb.append("	and" + ReportUtils.getReplaceStringForIN("at.nominal_code", agentNominalCodes, false));

					sb.append("		and at.tnx_date BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
							+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtFromOffsetInMinutesStr);
					sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
					sb.append("	AND p.entity_code ='" + reportsSearchCriteria.getEntity() + "'" );

				}


				sb.append(" ) V , T_AGENT C, T_PAX_TRNX_NOMINAL_CODE N, T_USER U");// end select 2

				sb.append(" WHERE ( v. product_type is not null or V.NOMINAL_CODE IN(" + singleDataValuePayments + ") ) ");
				sb.append(" AND V.PAYMENT_CURRENCY_CODE = '" + reportsSearchCriteria.getCurrencies() + "'");
				sb.append(" AND V.AGENT_CODE = C.AGENT_CODE");
				sb.append(" AND V.USER_ID        = U.USER_ID(+)");
				sb.append(" AND V.NOMINAL_CODE   = N.NOMINAL_CODE");
				sb.append(" AND C.IS_LCC_INTEGRATION_AGENT = 'N'");
				sb.append("	AND V.entity_code ='" + reportsSearchCriteria.getEntity() + "'" );
				
				if(reportsSearchCriteria.getStations() != null && !reportsSearchCriteria.getStations().isEmpty()){
					sb.append(" AND C.STATION_CODE IN (" + Util.buildStringInClauseContent(reportsSearchCriteria.getStations()) + ")");
				}
				if (reportsSearchCriteria.isOnlyReportingAgents()) {

					sb.append(" AND( C.AGENT_CODE IN (SELECT AGENT_CODE");
					sb.append(" FROM T_AGENT");
					sb.append(" WHERE STATUS = 'ACT'");
					sb.append(" CONNECT BY PRIOR AGENT_CODE = GSA_CODE");
					sb.append(" START WITH AGENT_CODE  = '" + reportsSearchCriteria.getGsaCode() + "'))");

				}
				sb.append(
						" GROUP BY V.PNR_PAX_ID, V.NOMINAL_CODE, C.AGENT_CODE, C.AGENT_NAME, C.ACCOUNT_CODE, V.PAYMENT_CURRENCY_CODE, V.PNR , v.external_rec_locator, "
								+ "V.FIRST_NAME  || ' '  || V.LAST_NAME, U.DISPLAY_NAME, V.TNX_DATE, V.ETICKET_ID, V.ROUTING,V.PAY_REF, v.agent_pay_ref, v.product_type, V.ACTUAL_PAY,v.entity_code");

				if (showFarebasis) {
					sb.append(" ,  V.FARE_BASIS_CODES");
				}

				sb.append(" ) "); // end select 1

				sb.append(" order by  agent_code,description,payment_currency_code,payment_date,pnr ");
			}

		}
		return sb.toString();
	}

	private boolean addFareDiscountCodeCheck(ReportsSearchCriteria reportsSearchCriteria) {

		if (AppSysParamsUtil.enableFareDiscountCodes() && reportsSearchCriteria.getFareDiscountCode() != null
				&& !reportsSearchCriteria.getFareDiscountCode().equals("null")
				&& !reportsSearchCriteria.getFareDiscountCode().equals("")
				&& !reportsSearchCriteria.getFareDiscountCode().equals("All")) {
			return true;
		}

		return false;
	}

	/**
	 * Creates the Sql for Company Payment Report by Point of Sale
	 * 
	 * @param reportsSearchCriteria
	 *            the ReportsSearchCriteria
	 * @return String the created Sql
	 */
	public String getCompanyPaymentPOSQuery(ReportsSearchCriteria reportsSearchCriteria) {

		/** THIS REPORT IS BROKEN - Feb.02.2010 */

		String strStations = getSingleDataValue(reportsSearchCriteria.getStations());
		String singleDataValuePayments = getSingleDataValue(reportsSearchCriteria.getPaymentTypes());
		StringBuilder sb = new StringBuilder();

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.COMPANY_PAYMENT_POS_SUMMARY)) {
			// Following query combines existing payments with LCC interline payments
			sb.append("SELECT STATION_CODE, AGENT_CODE, AGENT_NAME, ");
			sb.append("       (-1 * PMT_AMT) PMT_AMT, ");
			sb.append("       (-1 * REF_AMT) REF_AMT, ");
			sb.append("       ((-1 * PMT_AMT) - REF_AMT) NET_AMT ");
			sb.append("  FROM (SELECT A.STATION_CODE, A.AGENT_CODE, A.AGENT_NAME, ");
			sb.append("               SUM(DECODE(T.NOMINAL_CODE, 30, T.AMOUNT, 32, T.AMOUNT, 6, T.AMOUNT, 28, T.AMOUNT, 18, T.AMOUNT, 17, T.AMOUNT, 16, T.AMOUNT, 19, T.AMOUNT, 15, T.AMOUNT, 48, T.AMOUNT, 0)) PMT_AMT, ");
			sb.append("               SUM(DECODE(T.NOMINAL_CODE, 31, T.AMOUNT, 33, T.AMOUNT, 5, T.AMOUNT, 29, T.AMOUNT, 26, T.AMOUNT, 24, T.AMOUNT, 23, T.AMOUNT, 25, T.AMOUNT, 22, T.AMOUNT, 0)) REF_AMT ");
			sb.append("          FROM (SELECT T.AGENT_CODE AS AGENT_CODE, T.NOMINAL_CODE AS NOMINAL_CODE, T.AMOUNT AS AMOUNT ");
			sb.append("                  FROM T_PAX_TRANSACTION T ");
			sb.append("                 WHERE TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss') AND ");
			sb.append("                       TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss') ");
			sb.append("                 AND (T.PAYMENT_CARRIER_CODE IS NULL OR T.PAYMENT_CARRIER_CODE='"
					+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
			sb.append("                UNION ALL ");
			sb.append("                SELECT P.AGENT_CODE AS AGENT_CODE, P.NOMINAL_CODE AS NOMINAL_CODE, P.AMOUNT AS AMOUNT ");
			sb.append("                  FROM T_PAX_EXT_CARRIER_TRANSACTIONS P");
			sb.append("                   WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss') AND ");
			sb.append("                       TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss') ");
			sb.append("		  ) T, T_AGENT A ");
			sb.append("         WHERE T.NOMINAL_CODE IN (" + singleDataValuePayments + ") ");
			sb.append("           AND A.AGENT_CODE = T.AGENT_CODE ");
			sb.append("           AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
			sb.append("           AND A.STATION_CODE IN (" + strStations + ") ");
			sb.append("         GROUP BY A.STATION_CODE, A.AGENT_CODE, A.AGENT_NAME) ");
			sb.append(" ORDER BY STATION_CODE, AGENT_CODE ");
		} else {
			// Following query combines existing payments with LCC interline payments
			sb.append("select agent_code,agent_name,decode(nominal_code,19,'On Account',25,'On Account',18,'Cash',26,'Cash',15,'Visa',22,'Visa',16,'Master',23,'Master',28,'Amex',29,'Amex',17,'Diners',24,'Diners',"
					+ "30,'Generic',31,'Generic',32,'CMI',33,'CMI',5,'CREDIT',6,'CREDIT',48,'VOUCHER') description,to_char(payment_date,'dd-MON-yyyy')payment_date,");
			sb.append(" pnr,passenger,F_COMPANY_PYMT_REPORT(pnr)routing,username,-1*pmt_amt pmt_amt,");
			sb.append("  -1*ref_amt ref_amt, '' as PAY_REF, '' as ACTUAL_PAY, '' as ETICKET,' ' as INFANT");
			sb.append(" 	FROM ");
			sb.append(" 	(");
			sb.append(" 	SELECT A.agent_code,A.agent_name,n.nominal_code,");
			sb.append("  T.tnx_date payment_date,p.pnr pnr,p.first_name||' '||p.last_name passenger,u.display_name username, ");
			sb.append(" 	sum(decode(t.nominal_code,30,t.amount, 32,t.amount, 6,t.amount, 28,t.amount,18,t.amount,17,t.amount,16,t.amount,19,t.amount,15,t.amount,48,t.amount,0))pmt_amt,");
			sb.append(" 	sum(decode(t.nominal_code,31,t.amount, 33,t.amount, 5,t.amount, 29,t.amount,26,t.amount,24,t.amount,23,t.amount,25,t.amount,22,t.amount,0))ref_amt");
			sb.append(" 	FROM t_pnr_passenger p, ");
			sb.append("          (SELECT L.NOMINAL_CODE, L.USER_ID, L.AGENT_CODE, L.TNX_DATE, L.PNR_PAX_ID, L.AMOUNT ");
			sb.append("             FROM T_PAX_TRANSACTION L ");
			sb.append("             WHERE (L.PAYMENT_CARRIER_CODE IS NULL OR L.PAYMENT_CARRIER_CODE='"
					+ AppSysParamsUtil.getDefaultCarrierCode() + "' )");
			sb.append("           UNION ALL ");
			sb.append("           SELECT P.NOMINAL_CODE   AS NOMINAL_CODE, ");
			sb.append("                  P.USER_ID        AS USER_ID, ");
			sb.append("                  P.AGENT_CODE     AS AGENT_CODE, ");
			sb.append("                  P.TXN_TIMESTAMP  AS TNX_DATE, ");
			sb.append("                  N.PNR_PAX_ID     AS PNR_PAX_ID, ");
			sb.append("                  P.AMOUNT AS AMOUNT ");
			sb.append("             FROM T_PAX_EXT_CARRIER_TRANSACTIONS P, T_PNR_PASSENGER N ");
			sb.append("            WHERE P.PNR_PAX_ID = N.PNR_PAX_ID ");
			sb.append("		   	 ) T, ");
			sb.append("          T_PAX_TRNX_NOMINAL_CODE N,t_agent a, t_user u");
			sb.append(" 	  where t.nominal_code in(" + singleDataValuePayments + ") ");
			sb.append(" 	  		and t.user_id = u.user_id ");
			sb.append(" 	        and p.pnr_pax_id=t.pnr_pax_id ");
			sb.append("             and T.nominal_code=N.nominal_code ");
			sb.append(" 	        and a.agent_code=t.agent_code ");
			sb.append(" 	        and tnx_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
					+ "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
			sb.append(" 	        and to_date('" + reportsSearchCriteria.getDateRangeTo()
					+ "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			sb.append(" 	        and T.agent_code IN('" + reportsSearchCriteria.getAgentCode() + "')  ");
			sb.append("             AND A.IS_LCC_INTEGRATION_AGENT = 'N' ");
			sb.append(" 	group by p.pnr_pax_id,n.nominal_code,A.agent_code,A.agent_name,");
			sb.append("  pnr,p.first_name||' '||p.last_name,u.display_name,T.tnx_date  ");
			sb.append(" 	)P               ");
			sb.append(" 	order by agent_code,description,payment_date,pnr ");
		}
		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Breakdown Detail Report.
	 * 
	 * @param reportsSearchCriteria
	 */

	public String getCustomerExistingCreditDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String strSearchFrom = reportsSearchCriteria.getDateRangeFrom();
		String strSearchTo = reportsSearchCriteria.getDateRangeTo();

		StringBuilder sb = new StringBuilder();
		if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeTo() == null)) {
			return null;
		}

		sb.append("SELECT A.agent_name, ");
		sb.append("       P.first_name||' '||P.last_name AS PASSENGER_NAME, ");
		sb.append("       R.pnr AS PNR, ");
		sb.append("       C.balance AS AMOUNT, ");
		sb.append("       T.tnx_date AS PAYMENT_DATE, ");
		sb.append("       N.description AS PAYMENT_METHOD, ");
		sb.append("       C.date_exp AS EXPIRY_DATE ");
		sb.append("FROM   t_pax_credit C, t_pax_transaction T, T_AGENT A, T_PNR_PASSENGER P, ");
		sb.append("       T_RESERVATION R, T_PAX_TRNX_NOMINAL_CODE N ");
		sb.append("WHERE  R.PNR=P.PNR ");
		sb.append("       AND P.pnr_pax_id=T.pnr_pax_id ");
		sb.append("       AND C.txn_id=T.txn_id ");
		sb.append("       AND t.nominal_code in(28,18,17,16,19,15,29,26,24,23,25,22,6,5,11,30,31,32,33) ");
		sb.append("       AND t.nominal_code=n.nominal_code ");
		sb.append("       AND r.owner_agent_code=A.agent_code AND  ");
		sb.append(ReportUtils.getReplaceStringForIN("R.owner_agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		sb.append("       AND C.date_exp BETWEEN TO_DATE('" + strSearchFrom
				+ " 00:00:00','DD-MON-YYYY hh24:mi:ss') AND TO_DATE('" + strSearchTo + " 23:59:59','DD-MON-YYYY hh24:mi:ss') ");
		sb.append("       AND balance<>0 ");
		sb.append("ORDER BY A.agent_name, P.first_name, P.last_name ASC");

		return sb.toString();

	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCustomerProfileCountry(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String secFrom = reportsSearchCriteria.getSectorFrom();
		String secTo = reportsSearchCriteria.getSectorTo();
		String nationality = reportsSearchCriteria.getNationality();
		String fromToSegment = null;

		if (secFrom != null && secTo != null) {
			fromToSegment = secFrom + "/" + secTo;
		}

		sb.append("SELECT	cus.customer_id AS CUSTOMER_ID, ");
		sb.append("cus.email_id AS LOGIN_ID, cus.title || ' ' || ");
		sb.append("cus.first_name || ' ' || cus.last_name AS CUSTOMER_NAME, ");
		sb.append("ROUND(MONTHS_BETWEEN(SYSDATE, cus.date_of_birth) / 12) AS AGE,");
		sb.append("	cus.gender AS GENDER, nationality.description AS NATIONALITY, ");
		sb.append("country.country_name AS COUNTRY_RES, ");
		sb.append("cus.mobile AS MOBILE ");
		sb.append("FROM	T_CUSTOMER cus INNER JOIN T_COUNTRY country ON ");
		sb.append("cus.country_code = country.country_code ");
		sb.append("LEFT OUTER JOIN	T_NATIONALITY nationality ON ");
		sb.append("cus.nationality_code = nationality.nationality_code ");

		if (fromToSegment != null) {
			sb.append(" , t_reservation tr,t_reservation_contact rCon, t_pnr_segment ps, t_flight_segment fs ");
		}

		sb.append(" WHERE 	cus.country_code = '" + reportsSearchCriteria.getCountryOfResidence() + "' ");

		if (nationality != null && !nationality.equals("")) {
			sb.append(" AND	cus.nationality_code = '" + nationality + "' ");
		}

		sb.append(" AND cus.registration_date between to_date('" + fromDate + "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
		sb.append("	 AND to_date('" + toDate + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss') ");

		if (fromToSegment != null) {
			sb.append(" and cus.customer_id = rCon.customer_id ");
			sb.append(" and tr.pnr = ps.pnr ");
			sb.append(" and tr.pnr = rCon.pnr ");
			sb.append(" and ps.flt_seg_id = fs.flt_seg_id ");
			sb.append(" and fs.segment_code = '" + fromToSegment + "' ");
		}

		if (reportsSearchCriteria.isByCustomer() && reportsSearchCriteria.getCustomerFirstName() != null) {
			sb.append("and cus.first_name = '" + reportsSearchCriteria.getCustomerFirstName() + "' ");
		}
		if (reportsSearchCriteria.isByCustomer() && reportsSearchCriteria.getCustomerLastName() != null) {
			sb.append("and cus.last_name = '" + reportsSearchCriteria.getCustomerLastName() + "' ");
		}
		sb.append("GROUP BY	cus.customer_id, cus.email_id, ");
		sb.append("cus.title || ' ' || cus.first_name || ' ' || ");
		sb.append("cus.last_name, cus.date_of_birth, cus.gender, ");
		sb.append("nationality.description, country.country_name, cus.mobile ORDER BY	cus.customer_id");
		return sb.toString();
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCustomerProfile(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String secFrom = reportsSearchCriteria.getSectorFrom();
		String secTo = reportsSearchCriteria.getSectorTo();
		String fromToSegment = null;

		if (secFrom != null && secTo != null) {
			fromToSegment = secFrom + "/" + secTo;
		}

		sb.append("SELECT	cus.customer_id AS CUSTOMER_ID, ");
		sb.append("cus.email_id AS LOGIN_ID, cus.title || ' ' || ");
		sb.append("cus.first_name || ' ' || cus.last_name AS CUSTOMER_NAME, ");
		sb.append("ROUND(MONTHS_BETWEEN(SYSDATE, cus.date_of_birth) / 12) AS AGE,");
		sb.append("	cus.gender AS GENDER, nationality.description AS NATIONALITY, ");
		sb.append("country.country_name AS COUNTRY_RES, ");
		sb.append("cus.mobile AS MOBILE ");
		sb.append("FROM	T_CUSTOMER cus INNER JOIN T_COUNTRY country ON ");
		sb.append("cus.country_code = country.country_code ");
		sb.append("LEFT OUTER JOIN	T_NATIONALITY nationality ON ");
		sb.append("cus.nationality_code = nationality.nationality_code ");

		if (fromToSegment != null) {
			sb.append(" , t_reservation tr, t_reservation_contact rCon, t_pnr_segment ps, t_flight_segment fs ");
		}

		sb.append(" WHERE cus.registration_date between to_date('" + fromDate + "'||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
		sb.append("	 AND to_date('" + toDate + "'||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");

		if (fromToSegment != null) {
			sb.append(" and cus.customer_id = rCon.customer_id ");
			sb.append(" and tr.pnr = rCon.pnr ");
			sb.append(" and tr.pnr = ps.pnr ");
			sb.append(" and ps.flt_seg_id = fs.flt_seg_id ");
			sb.append(" and fs.segment_code = '" + fromToSegment + "' ");
		}

		if (reportsSearchCriteria.isByCustomer() && reportsSearchCriteria.getCustomerFirstName() != null) {
			sb.append("and cus.first_name = '" + reportsSearchCriteria.getCustomerFirstName() + "' ");
		}
		if (reportsSearchCriteria.isByCustomer() && reportsSearchCriteria.getCustomerLastName() != null) {
			sb.append("and cus.last_name = '" + reportsSearchCriteria.getCustomerLastName() + "' ");
		}
		sb.append("GROUP BY	cus.customer_id, cus.email_id, ");
		sb.append("cus.title || ' ' || cus.first_name || ' ' || ");
		sb.append("cus.last_name, cus.date_of_birth, cus.gender, ");
		sb.append("nationality.description, country.country_name, cus.mobile ORDER BY	cus.customer_id");
		return sb.toString();
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCountryContributionPerFlightSegmentData(ReportsSearchCriteria reportsSearchCriteria) {

		String strOperationType = reportsSearchCriteria.getOperationType();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT a.agent_code AS agent_code, a.agent_name,count(distinct(p.pnr_pax_id))pax, ");
		sb.append(" SUM (DECODE (charge_group_code, 'CNX', pfst.amount, 'MOD', pfst.amount, 'FAR', pfst.amount, 'INF',  pfst.amount, 'SUR', pfst.amount, 0)) revenue, ");
		sb.append(" SUM (DECODE (charge_group_code, 'CNX', pfst.amount, 'MOD', pfst.amount, 'FAR', pfst.amount, 'INF',  pfst.amount, 'SUR', pfst.amount, 0))/count(distinct(p.pnr_pax_id)) yield, ");
		sb.append(" SUM (DECODE (charge_group_code, 'DIS', pfst.amount, 0)) discount, c.country_name ");
		sb.append("	FROM  t_pnr_pax_seg_charges pfst, t_pnr_pax_fare_segment ppfs, t_reservation r, t_pnr_segment ps, t_agent a,  ");
		sb.append("    	  t_pnr_pax_ond_charges pft,t_pnr_pax_fare ppf,t_pnr_passenger p, t_country c,   t_station s , t_flight_segment fs ");
		sb.append("	WHERE  r.pnr=ps.pnr                  and  ");
		sb.append("      	ps.pnr_seg_id=ppfs.pnr_seg_id and ");
		sb.append("     	p.pnr=r.pnr                   and ");
		sb.append("      	p.pnr_pax_id=ppf.pnr_pax_id   and ");
		sb.append("      	ppfs.ppf_id=ppf.ppf_id        and ");
		sb.append("      	ppf.ppf_id=pft.ppf_id         and ");
		sb.append("    		pfst.ppfs_id=ppfs.ppfs_id     and ");
		sb.append("    	    pfst.pft_id=pft.pft_id        and ");
		sb.append("    		r.origin_agent_code=a.agent_code   and ");
		sb.append("    		r.status='CNF' and ps.status='CNF' and ");
		sb.append("  	ps.flt_seg_id = fs.flt_seg_id and");

		if (reportsSearchCriteria.getFlightNumber() != null && !reportsSearchCriteria.getFlightNumber().trim().equals("")) {
			sb.append(" ps.flt_seg_id IN (SELECT flt_seg_id FROM t_flight_segment ");
			sb.append("    WHERE flight_id in ");
			sb.append("                     (select flight_id  ");
			sb.append("						  from t_flight  ");
			sb.append("						  where flight_number='" + reportsSearchCriteria.getFlightNumber() + "')) ");

		} else if (reportsSearchCriteria.getSelectedSegments() != null
				&& reportsSearchCriteria.getSelectedSegments().size() > 0) {

			List<String> segmentsWithNames = new ArrayList<String>(); // segments like SHJ/CMB
			List<String> segmentsWithAllString = new ArrayList<String>(); // segments like SHJ/All
			String selectedSegmentsSQL = "";
			String selectedSegmentsWithAllStrigSQL = "";
			String selectedSegmentsWithNamesSQL = "";
			for (String segemnt : reportsSearchCriteria.getSelectedSegments()) {
				if (segemnt.contains("All")) {
					segmentsWithAllString.add(segemnt);
				} else {
					segmentsWithNames.add(segemnt);
				}
			}
			if (segmentsWithNames.size() > 0) {
				selectedSegmentsWithNamesSQL = "'" + segmentsWithNames.get(0).toUpperCase() + "'";
				if (segmentsWithNames.size() > 1) {
					for (int i = 1; i < segmentsWithNames.size(); i++) {
						selectedSegmentsWithNamesSQL += ",'" + segmentsWithNames.get(i).toUpperCase() + "'";
					}
				}
			}
			if (segmentsWithAllString.size() > 0) {
				for (String segment : segmentsWithAllString) {
					segment = segment.replaceAll("All", "%");
					selectedSegmentsWithAllStrigSQL += "OR segment_code like '" + segment + "'";
				}
			}

			if (selectedSegmentsWithNamesSQL == "") {
				selectedSegmentsWithNamesSQL = "''";
			}
			selectedSegmentsSQL = selectedSegmentsWithNamesSQL + ")" + selectedSegmentsWithAllStrigSQL;
			sb.append(" ps.flt_seg_id IN (SELECT flt_seg_id FROM t_flight_segment ");
			sb.append(" WHERE UPPER(segment_code) IN (" + selectedSegmentsSQL + ")");
		}
		// by booking date
		if (reportsSearchCriteria.getDateRangeFrom() != null && reportsSearchCriteria.getDateRangeFrom().length() > 0) {

			sb.append(" AND	booking_timestamp BETWEEN to_date('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') and ");
			sb.append("							  to_date('" + reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') ");
		}
		// by flight date
		if (reportsSearchCriteria.getFlightDateRangeFrom() != null
				&& reportsSearchCriteria.getFlightDateRangeFrom().length() > 0) {

			sb.append(" AND	est_time_departure_local BETWEEN to_date('" + reportsSearchCriteria.getFlightDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') and ");
			sb.append("							  to_date('" + reportsSearchCriteria.getFlightDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') ");
		}

		if (reportsSearchCriteria.getSelectedCountries() != null && reportsSearchCriteria.getSelectedCountries().size() > 0) {
			String selectedCountrySQL = "'" + reportsSearchCriteria.getSelectedCountries().get(0) + "'";
			if (reportsSearchCriteria.getSelectedCountries().size() > 1) {
				for (int i = 1; i < reportsSearchCriteria.getSelectedCountries().size(); i++) {
					selectedCountrySQL += ",'" + reportsSearchCriteria.getSelectedCountries().get(i) + "'";
				}
			}
			sb.append(" AND a.station_code  = s.station_code AND s.country_code  =c.country_code");
			sb.append(" AND c.country_name IN ( " + selectedCountrySQL + ") ");
		}

		if (!strOperationType.equalsIgnoreCase("All")) {
			sb.append("  and ps.flt_seg_id IN (select flt_seg_id FROM t_flight_segment ");
			sb.append(" where flight_id in ");
			sb.append("     (select flight_id from t_flight where operation_type_id in " + strOperationType + " )) ");
		}

		sb.append("	GROUP BY  a.agent_code, a.agent_name, c.country_name ");
		return sb.toString();

	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getPOSContributionData(ReportsSearchCriteria reportsSearchCriteria) {

		String strOperationType = reportsSearchCriteria.getOperationType();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT a.agent_code AS agent_code, a.agent_name,count(distinct(p.pnr_pax_id))pax, ");
		sb.append(
				" SUM (DECODE (charge_group_code, 'CNX', pfst.amount, 'MOD', pfst.amount, 'FAR', pfst.amount, 'INF',  pfst.amount, 'SUR', pfst.amount, 0)) revenue, ");
		sb.append(
				" SUM (DECODE (charge_group_code, 'CNX', pfst.amount, 'MOD', pfst.amount, 'FAR', pfst.amount, 'INF',  pfst.amount, 'SUR', pfst.amount, 0))/count(distinct(p.pnr_pax_id)) yield, ");
		sb.append(" SUM (DECODE (charge_group_code, 'DIS', (pfst.amount * -1), 0)) discount, a.station_code ");
		sb.append(
				"	FROM  t_pnr_pax_seg_charges pfst, t_pnr_pax_fare_segment ppfs, t_reservation r, t_pnr_segment ps, t_agent a, ");
		sb.append("    	  t_pnr_pax_ond_charges pft,t_pnr_pax_fare ppf,t_pnr_passenger p, t_flight_segment fs  ");
		sb.append("	WHERE  r.pnr=ps.pnr                  and  ");
		sb.append("      	ps.pnr_seg_id=ppfs.pnr_seg_id and ");
		sb.append("     	p.pnr=r.pnr                   and ");
		sb.append("      	p.pnr_pax_id=ppf.pnr_pax_id   and ");
		sb.append("      	ppfs.ppf_id=ppf.ppf_id        and ");
		sb.append("      	ppf.ppf_id=pft.ppf_id         and ");
		sb.append("    		pfst.ppfs_id=ppfs.ppfs_id     and ");
		sb.append("    	    pfst.pft_id=pft.pft_id        and ");
		sb.append("    		r.origin_agent_code=a.agent_code   and ");
		sb.append("    		r.status='CNF' and ps.status='CNF' and ");
		sb.append("  	ps.flt_seg_id = fs.flt_seg_id and");

		if (reportsSearchCriteria.getFlightNumber() != null && !reportsSearchCriteria.getFlightNumber().trim().equals("")) {
			sb.append(" ps.flt_seg_id IN (SELECT flt_seg_id FROM t_flight_segment ");
			sb.append("    WHERE flight_id in ");
			sb.append("                     (select flight_id  ");
			sb.append("						  from t_flight  ");
			sb.append("						  where flight_number='" + reportsSearchCriteria.getFlightNumber() + "')) AND ");

		} else if (reportsSearchCriteria.getSelectedSegments() != null
				&& reportsSearchCriteria.getSelectedSegments().size() > 0) {

			List<String> segmentsWithNames = new ArrayList<String>(); // segments like SHJ/CMB
			List<String> segmentsWithAllString = new ArrayList<String>(); // segments like SHJ/All
			String selectedSegmentsSQL = "";
			String selectedSegmentsWithAllStrigSQL = "";
			String selectedSegmentsWithNamesSQL = "";
			for (String segemnt : reportsSearchCriteria.getSelectedSegments()) {
				if (segemnt.contains("All")) {
					segmentsWithAllString.add(segemnt);
				} else {
					segmentsWithNames.add(segemnt);
				}
			}
			if (segmentsWithNames.size() > 0) {
				selectedSegmentsWithNamesSQL = "'" + segmentsWithNames.get(0).toUpperCase() + "'";
				if (segmentsWithNames.size() > 1) {
					for (int i = 1; i < segmentsWithNames.size(); i++) {
						selectedSegmentsWithNamesSQL += ",'" + segmentsWithNames.get(i).toUpperCase() + "'";
					}
				}
			}
			if (segmentsWithAllString.size() > 0) {
				for (String segment : segmentsWithAllString) {
					segment = segment.replaceAll("All", "%");
					selectedSegmentsWithAllStrigSQL += "OR segment_code like '" + segment + "'";
				}
			}

			if (selectedSegmentsWithNamesSQL == "") {
				selectedSegmentsWithNamesSQL = "''";
			}
			selectedSegmentsSQL = selectedSegmentsWithNamesSQL + ")" + selectedSegmentsWithAllStrigSQL;
			sb.append(" ps.flt_seg_id IN (SELECT flt_seg_id FROM t_flight_segment ");
			sb.append(" WHERE UPPER(segment_code) IN (" + selectedSegmentsSQL + ") ");
		}

		// by booking date
		if (reportsSearchCriteria.getDateRangeFrom() != null && reportsSearchCriteria.getDateRangeFrom().length() > 0) {

			sb.append(" AND	booking_timestamp BETWEEN to_date('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') and ");
			sb.append("							  to_date('" + reportsSearchCriteria.getDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') ");
		}
		// by flight date
		if (reportsSearchCriteria.getFlightDateRangeFrom() != null
				&& reportsSearchCriteria.getFlightDateRangeFrom().length() > 0) {

			sb.append("AND 	est_time_departure_local BETWEEN to_date('" + reportsSearchCriteria.getFlightDateRangeFrom()
					+ " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') and ");
			sb.append("							  to_date('" + reportsSearchCriteria.getFlightDateRangeTo()
					+ " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') ");
		}

		if (reportsSearchCriteria.getSelectedPOSs() != null && reportsSearchCriteria.getSelectedPOSs().size() > 0) {
			String selectedPOSSQL = "'" + reportsSearchCriteria.getSelectedPOSs().get(0) + "'";
			if (reportsSearchCriteria.getSelectedPOSs().size() > 1) {
				for (int i = 1; i < reportsSearchCriteria.getSelectedPOSs().size(); i++) {
					selectedPOSSQL += ",'" + reportsSearchCriteria.getSelectedPOSs().get(i) + "'";
				}
			}
			sb.append(" and station_code in(" + selectedPOSSQL + ")");
		}

		if (!strOperationType.equalsIgnoreCase("All")) {
			sb.append("  and ps.flt_seg_id IN (select flt_seg_id FROM t_flight_segment ");
			sb.append(" where flight_id in ");
			sb.append("     (select flight_id from t_flight where operation_type_id in " + strOperationType + " )) ");
		}

		sb.append("	GROUP BY  a.agent_code, a.agent_name, a.station_code ");
		return sb.toString();

	}

	public String getAgentTransactionDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		String strNominalCodes = getSingleDataValue(reportsSearchCriteria.getPaymentTypes());
		StringBuilder sb = new StringBuilder();

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.AGENT_TRANS_SUMMARY)) {
			sb.append("	SELECT agent_code, agent_name, TO_NUMBER(SUBSTR(PARENT,1,INSTR(PARENT,'|')-1)) credit_limit, ");
			sb.append("TO_NUMBER(SUBSTR(PARENT,INSTR(PARENT,'|')  +1)) avilable_credit, due, avilable_funds_for_inv ");
			sb.append("FROM ( SELECT agent.agent_code, AGENT.agent_name, AGENT.credit_limit, ");
			sb.append("summ.avilable_credit, (summ.shared_credit_due_amount + summ.distributed_credit_collection) AS due, ");
			sb.append("summ.avilable_funds_for_inv, F_GET_SHARED_CL_FOR_AGENT(agent.agent_code) PARENT ");
			sb.append("FROM t_agent agent, t_agent_summary summ ");
			sb.append("where agent.agent_code = summ.agent_code AND ");
			sb.append(ReportUtils.getReplaceStringForIN("agent.agent_code", reportsSearchCriteria.getAgents(), false) + ") ");
		}
		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.AGENT_TRANS_DETAILS)) {
			sb.append("SELECT TXN_ID,AGENT_CODE, ");
			sb.append("DECODE(DR_CR,'DR',AMOUNT,0)DEBIT_AMOUNT,DECODE(DR_CR,'CR',AMOUNT,0)CREDIT_AMOUNT, ");
			sb.append("TNX_DATE,NOTES,USER_ID ,");
			sb.append(" DECODE(ADJUSTMENT_CARRIER_CODE,null,' ',ADJUSTMENT_CARRIER_CODE) as ADJUSTMENT_CARRIER_CODE ");
			sb.append(" FROM t_agent_transaction ");
			sb.append("WHERE AGENT_CODE='" + reportsSearchCriteria.getAgentCode() + "' ");
			sb.append("AND TNX_DATE BETWEEN ");
			sb.append(" to_date('" + reportsSearchCriteria.getDateRangeFrom() + " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("and  to_date('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') ");
			if (strNominalCodes.length() > 0) {
				sb.append("AND NOMINAL_CODE IN(" + strNominalCodes + ")");
			}
		}
		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.AGENT_TRANS_AGENT_DETAIL)) {
			sb.append("SELECT TXN_ID,agentra.AGENT_CODE,agent.AGENT_NAME, ");
			sb.append("DECODE(DR_CR,'DR',AMOUNT,0)DEBIT_AMOUNT,DECODE(DR_CR,'CR',AMOUNT,0)CREDIT_AMOUNT, ");
			sb.append("TNX_DATE,agentra.NOTES,USER_ID, ");
			sb.append(" DECODE(agentra.ADJUSTMENT_CARRIER_CODE,null,' ',agentra.ADJUSTMENT_CARRIER_CODE) as ADJUSTMENT_CARRIER_CODE ");
			sb.append(" FROM t_agent_transaction agenTra, t_agent agent WHERE  ");
			sb.append(ReportUtils.getReplaceStringForIN("agenTra.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
			sb.append("AND agenTra.TNX_DATE BETWEEN ");
			sb.append(" to_date('" + reportsSearchCriteria.getDateRangeFrom() + " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') ");
			sb.append("and  to_date('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') ");
			if (strNominalCodes.length() > 0) {
				sb.append("AND agenTra.NOMINAL_CODE IN(" + strNominalCodes + ")");
			} else {
				sb.append(" AND agenTra.NOMINAL_CODE NOT IN('1') ");
			}
			sb.append(" AND agenTra.AGENT_CODE = agent.AGENT_CODE ");
			sb.append("  order by AGENT_CODE");
		}
		return sb.toString();
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCnxreservationQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom(); // Cancellation From Date
		String strToDate = reportsSearchCriteria.getDateRangeTo(); // Cancellation To Date
		String strFlightNo = reportsSearchCriteria.getFlightNumber();

		String strFromDepDate = reportsSearchCriteria.getDepartureDateRangeFrom();
		String strToDepDate = reportsSearchCriteria.getDepartureDateRangeTo();
		String timezone = reportsSearchCriteria.getTimeZone();
		String reportType = reportsSearchCriteria.getReportType();

		String strField = "";
		if (timezone.equals("ZULU")) {
			strField = "est_time_departure_zulu";
		} else {
			strField = "est_time_departure_local";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT R.pnr,TO_CHAR(" + strField + ", 'DD-MM-YYYY') as flt_date,  ");
		sb.append("				segment_code sector,TO_CHAR(status_mod_date,'DD-MM-YYYY') as cnxl_date,  ");
		sb.append("				nvl(agent_name,DECODE(ps.STATUS_MOD_CHANNEL_CODE, " + SalesChannelsUtil.SALES_CHANNEL_WEB + " ,'WEB',"
				+ SalesChannelsUtil.SALES_CHANNEL_IOS + ",'IOS'," + SalesChannelsUtil.SALES_CHANNEL_ANDROID
				+ ",'ANDROID',agent_name)) originator, ");
		sb.append("  nvl(u.display_name,DECODE(ps.STATUS_MOD_CHANNEL_CODE,4,'WEB',20,'IOS',21,'ANDROID',u.display_name)) cnxld_by, ");
		sb.append("		F_CONCATENATE_LIST (cursor(select user_notes from t_reservation_audit			\n");
		sb.append("			where pnr=r.pnr and user_notes is not null and trunc(timestamp)=trunc(status_mod_date)),', ')USER_comments				");
		sb.append("		FROM t_reservation r,t_pnr_segment ps,t_flight_segment fs,t_user u ,t_agent a,t_flight f  ");
		sb.append("		WHERE r.pnr=ps.pnr and ");
		sb.append("				ps.flt_seg_id=fs.flt_seg_id  and ");
		sb.append("				f.flight_id=fs.flight_id     and ");
		sb.append("				a.agent_code(+)=ps.STATUS_AGENT_CODE and ");
		sb.append("				ps.status='CNX'       and ");
		sb.append("				status_mod_date between (TO_DATE('" + strFromDate + "  00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append("				and (TO_DATE('" + strToDate + "  23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");

		if (strFromDepDate != null && !strFromDepDate.equals("") && strToDepDate != null && !strToDepDate.equals("")) {

			sb.append("			and " + strField + " between (TO_DATE('" + strFromDepDate + "  00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
			sb.append("			and (TO_DATE('" + strToDepDate + "  23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");
		}

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append("AND f.flight_number = '" + strFlightNo + "'");
		}

		sb.append("		and u.user_id(+)=ps.status_mod_user_id     and ");
		if (reportType.equals("POST_DEP")) {
			sb.append("				" + strField + "<=status_mod_date ");
		} else if (reportType.equals("PRE_DEP")) {
			sb.append("				" + strField + ">status_mod_date ");
		}

		sb.append("				order by 4 ");

		return sb.toString();

	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getAcquiredCreditQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT p.pnr pnr ,p.first_name||' '||p.last_name pax_name ,t.amount, ");
		sb.append(" 	TO_CHAR((SELECT MIN(tnx_date) FROM t_pax_transaction T1 ");
		sb.append("		WHERE T1.pnr_pax_id=T.pnr_pax_id AND ");
		sb.append("		nominal_code in (28,18,17,16,19,15,29,26,24,23,25,22,6,5,30,31,32,33,36)),'DD-MM-YYYY')payment_date , ");
		sb.append("		TO_CHAR(t.tnx_date,'DD-MM-YYYY') expired_date  ");
		sb.append("			FROM t_pnr_passenger P,t_pax_transaction T ");
		sb.append("		WHERE P.pnr_pax_id=T.pnr_pax_id AND ");
		sb.append("		T.nominal_code=12         AND   "); // --CREDIT_ACQUIRE
		sb.append("		T.tnx_date BETWEEN (TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
		sb.append("			and (TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");
		sb.append(" order by tnx_date ");
		return sb.toString();

	}

	public String getAvailableCreditQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT P.PNR PNR,");
		sb.append("       P.FIRST_NAME || ' ' || P.LAST_NAME PAX_NAME,");
		sb.append("       C.BALANCE BALANCE,");
		// START - GET PAYMENT_DATE SQL
		sb.append("       TO_CHAR((SELECT MIN(TNX_DATE)");
		sb.append("         FROM T_PAX_TRANSACTION T1");
		sb.append("        WHERE T1.PNR_PAX_ID = C.PNR_PAX_ID");
		sb.append("          AND NOMINAL_CODE IN");
		sb.append("              (28, 18, 17, 16, 19, 15, 29, 26, 24, 23, 25, 22, 6,5,30,31,32,33,36)),");
		sb.append("       'DD-MM-YYYY') PAYMENT_DATE,");
		// STOP - GET PAYMENT_DATE SQL
		sb.append("       TO_CHAR(C.DATE_EXP, 'DD-MM-YYYY') EXPIRY_DATE");
		sb.append("  FROM T_PNR_PASSENGER P, T_PAX_CREDIT C");
		sb.append(" WHERE P.PNR_PAX_ID = C.PNR_PAX_ID");
		sb.append("  AND C.DATE_EXP >= (TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))");
		sb.append("  AND C.DATE_EXP <= (add_months(TO_DATE('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS'), 12))"); // Limit
																																// to
																																// 1yr
																																// starting
																																// from
																																// "strFromDate"
		sb.append("   AND C.BALANCE > 0");
		sb.append(" ORDER BY DATE_EXP ASC, PNR ASC");
		return sb.toString();
	}

	/**
	 * Query for OND Reports
	 * 
	 * @return String the Query
	 */
	public String getONDDataQuery() {
		String sql = "P_REVEN_REPT_OND";
		return sql;
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("Select") || str.trim().equals("-1"));
	}

	/**
	 * credit card payment transaction report
	 * 
	 * @return String the Query
	 */
	public String getCCTransactionReport(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String entityCode = reportsSearchCriteria.getEntity();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT to_date(TRUNC(TPT.timestamp),'dd-mm-yy') date_of_txn, ");
		sb.append("		COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS
				+ "',1,null)) ReservationSucces,SUM(DECODE(TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS
				+ "',TPT.amount,0)) ValueOfReservationSucces, ");
		sb.append("		(COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT + "',1,null))) Initiated, ");
		sb.append("		(SUM(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT + "',TPT.amount,0))) ValueOfInitiated, ");
		sb.append("		COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE
				+ "',1,null)) Rejected,SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE + "',TPT.amount,0)) ValueOfRejected, ");
		sb.append("		(COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST + "',1,null))) PendingRefunds, ");
		sb.append("		(SUM(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST
				+ "',TPT.amount,0))) ValueOfPendingRefunds, ");
		sb.append("		(COUNT(DECODE(TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS + "',1,null))) SuccessfulReversals, ");
		sb.append("		(SUM(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS
				+ "',TPT.amount,0))) ValueOfSuccessfulReversals, ");
		sb.append("		COUNT(DECODE(TPT.FRaud_status,'REVIEW',1,NULL))  REVIEWED     ");
		sb.append("		FROM T_TEMP_PAYMENT_TNX TPT, T_CCARD_PAYMENT_STATUS CC, T_PRODUCT P ");
		sb.append("	WHERE TPT.TIMESTAMP BETWEEN TO_DATE('" + strFromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		sb.append("	AND TPT.DR_CR ='" + ReservationInternalConstants.TnxTypes.CREDIT + "'");
		sb.append(" AND CC.service_type = 'ccauthorize' ");
		sb.append(" AND TPT.tpt_id = CC.tpt_id ");
		sb.append(" AND TPT.product_type = P.product_type ");
		sb.append(" AND P.entity_code = '" + entityCode + "' ");
		sb.append("	GROUP BY TRUNC(TPT.timestamp)");
		sb.append(" order by date_of_txn asc ");

		return sb.toString();
	}
	
	
	public String getCCTopUpTransactionReport(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT to_date(TRUNC(TPT.timestamp),'dd-mm-yy') date_of_txn, ");
		sb.append("		COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS
				+ "',1,null)) ReservationSucces,SUM(DECODE(TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS
				+ "',TPT.amount,0)) ValueOfReservationSucces, ");
		sb.append("		(COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT + "',1,null))) Initiated, ");
		sb.append("		(SUM(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT + "',TPT.amount,0))) ValueOfInitiated, ");
		sb.append("		COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE
				+ "',1,null)) Rejected,SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE + "',TPT.amount,0)) ValueOfRejected, ");
		sb.append("		(COUNT(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST + "',1,null))) PendingRefunds, ");
		sb.append("		(SUM(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST
				+ "',TPT.amount,0))) ValueOfPendingRefunds, ");
		sb.append("		(COUNT(DECODE(TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS
				+ "',1,null)) + COUNT(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS + "',1,null))) SuccessfulReversals, ");
		sb.append("		(SUM(DECODE (TPT.status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS
				+ "',TPT.amount,0)) + SUM(DECODE (TPT.status,'"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_SUCCESS
				+ "',TPT.amount,0))) ValueOfSuccessfulReversals, ");
		sb.append("		COUNT(DECODE(TPT.FRaud_status,'REVIEW',1,NULL))  REVIEWED     ");
		sb.append("		FROM T_TEMP_PAYMENT_TNX TPT, T_CCARD_PAYMENT_STATUS CC ");
		sb.append("	WHERE TPT.TIMESTAMP BETWEEN TO_DATE('" + strFromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		sb.append("	AND TPT.DR_CR ='" + ReservationInternalConstants.TnxTypes.CREDIT + "'");
		sb.append(" AND TPT.tpt_id = CC.tpt_id ");
		sb.append(" AND CC.service_type = 'ccauthorize' ");
		sb.append(" AND TPT.PRODUCT_TYPE ='T' ");
		sb.append("	GROUP BY TRUNC(TPT.timestamp)");
		sb.append(" order by date_of_txn asc ");

		return sb.toString();
	}

	/**
	 * CC transaction fraud status per day.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getPerDayCCFraudDetails(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String entityCode = reportsSearchCriteria.getEntity();
		StringBuilder sb = new StringBuilder();
	

		// FIXME : will need to concat the segment code for full journey
		if (reportsSearchCriteria.isIncludeAdvanceCCDetails()) {
			sb.append("SELECT (r.total_pax_count + r.total_pax_child_count + r.total_pax_infant_count) AS paxCount");
			sb.append(",TO_CHAR(r.booking_timestamp, 'dd-mm-yyyy HH24:MI:SS') BookingTime ");
			sb.append(",TO_DATE(TRUNC(T.TIMESTAMP),'dd-mm-yyyy HH24:MI:SS') TIMESTAMP ");
		} else {
			sb.append("SELECT TO_DATE(TRUNC(T.TIMESTAMP),'dd-mm-yyyy HH24:MI:SS') TIMESTAMP ");
			sb.append(", null AS BookingTime ");
			sb.append(", null AS paxCount ");
		}
		sb.append(", T.TPT_ID TNX_ID ");
		sb.append(", T.PNR ");
		sb.append(", T.CONTACT_PERSON ");
		sb.append(", T.TELEPHONE_NO ");
		sb.append(", T.MOBILE_NO  ");
		sb.append(", T.IP_ADDRESS  ");
		sb.append(", rCon.c_email ");
		sb.append(", (DECODE (T.CHANNEL_ID,'4','WEB',T.USER_ID)) USER_ID");
		sb.append(", T.PAYMENT_CURRENCY_AMOUNT ");
		sb.append(", T.PAYMENT_CURRENCY_CODE  ");

		if (reportsSearchCriteria.isIncludeAdvanceCCDetails()) {
			sb.append(", F_CONCATENATE_LIST ( CURSOR (SELECT fs.segment_code ");
			sb.append("  FROM t_pnr_segment ps,t_flight_segment fs WHERE ps.flt_seg_id = fs.flt_seg_id ");
			sb.append("  AND ps.pnr = r.pnr AND ps.status = 'CNF' order by fs.est_time_departure_zulu ),'-') AS segment, ");
			sb.append("  F_CONCATENATE_LIST(CURSOR (SELECT unique fr.fare_basis_code ");
			sb.append("  FROM t_pnr_segment ps, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs, t_ond_fare ondFare, t_fare_rule fr ");
			sb.append("  WHERE ps.pnr = r.pnr AND ps.status = 'CNF' ");
			sb.append("  AND ppf.ppf_id = ppfs.ppf_id AND ppfs.pnr_seg_id = ps.pnr_seg_id AND ppf.fare_id = ondfare.fare_id ");
			sb.append("  AND ondfare.fare_rule_id = fr.fare_rule_id ), '-') AS basisCode ");
			sb.append(", F_CONCATENATE_LIST(CURSOR (SELECT TO_CHAR(fs.est_time_departure_local, 'dd-mm-yyyy HH24:MI:SS') AS depTime ");
			sb.append(" FROM t_pnr_segment ps, t_flight_segment fs ");
			sb.append(" WHERE ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = r.pnr AND ps.status = 'CNF' AND r.dummy_booking = 'N' UNION ");
			sb.append(" SELECT TO_CHAR(fs.est_time_departure_local, 'dd-mm-yyyy HH24:MI:SS') AS depTime ");
			sb.append(" FROM t_ext_pnr_segment ps, t_ext_flight_segment fs ");
			sb.append(" WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id AND ps.pnr = r.pnr AND ps.status = 'CNF' AND r.dummy_booking = 'Y' ");
			sb.append(" ),' -- ') AS DepartureDateList ");
			sb.append(" , NULL AS departure");
			sb.append(" ,f_get_fare_type_pnr_seg_id((select ps.pnr_seg_id from t_pnr_segment ps ");
			sb.append(" where ps.pnr = t.pnr and rownum = 1) ");
			sb.append(" ,(select ppf.fare_id from t_pnr_pax_fare ppf,t_pnr_passenger pp ");
			sb.append(" where pp.pnr = t.pnr and pp.pnr_pax_id = ppf.pnr_pax_id and rownum = 1)) as journeyType ");
		} else {
			sb.append(", (SELECT fs.segment_code ");
			sb.append("FROM t_pnr_segment ps, t_flight_segment fs ");
			sb.append("WHERE ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'N' and rownum = 1");
			sb.append(" union SELECT fs.segment_code ");
			sb.append("FROM t_ext_pnr_segment ps, t_ext_flight_segment fs ");
			sb.append("WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'Y' and rownum = 1 ");
			sb.append(" ) as segment ");
			sb.append(", NULL AS basisCode ");
			sb.append(", NULL AS journeyType");
			sb.append(", NULL AS DepartureDateList");
			sb.append(" , (SELECT fs.est_time_departure_local ");
			sb.append("FROM t_pnr_segment ps, t_flight_segment fs ");
			sb.append("WHERE ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'N' and rownum = 1 ");
			sb.append(" union SELECT fs.est_time_departure_local ");
			sb.append("FROM t_ext_pnr_segment ps, t_ext_flight_segment fs ");
			sb.append("WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'Y' and rownum = 1 ");
			sb.append(" ) as departure ");
		}

		sb.append(", replace(replace(C.error_specification,'REJECTED ',''),'VALID HASH.', '') as REASON ");
		sb.append(", (DECODE(T.STATUS,'RS','ReservationSucces','I','Initiated','II','Initiated LeftOver','PF','Rejected','UF','PendingRefunds','US','SuccessfulReversals','IS','SuccessfulReversals', 'PS', 'PaymentSuccess', 'RF', 'ReservationFail')) PAYMENT_STATUS, T.FRAUD_STATUS ");
		sb.append(", T.FRAUD_MESSAGE  ");
		sb.append(", T.FRAUD_REFERENCE  ");
		sb.append(", C.NM_TRANSACTION_REFERENCE MERCHANT_REF_NUMBER ");
		sb.append(", C.TRANSACTION_REFERENCE PGW_TXN_NUMBER  ");
		sb.append(", PGW.DESCRIPTION PGW_NAME ");
		sb.append(", REGEXP_REPLACE((REGEXP_SUBSTR(C.request_text, 'vpc_OrderInfo=([a-zA-Z0-9]*-)*(.)((([0-9])*)\\.)*[0-9]*')), 'vpc_OrderInfo=', '') as order_reference ");
		sb.append("FROM T_TEMP_PAYMENT_TNX T, T_CCARD_PAYMENT_STATUS C  , t_reservation r, t_reservation_contact rCon, t_product p ");
		sb.append(", t_payment_gateway_currency pgwCurr, T_PAYMENT_GATEWAY pgw ");
		sb.append("WHERE T.TPT_ID = C.TPT_ID (+) ");
		sb.append("AND r.pnr = rCon.pnr (+) ");
		sb.append("AND t.pnr = r.pnr (+) ");
		sb.append("AND T.TIMESTAMP BETWEEN TO_DATE('" + strFromDate + "  00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + "  23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		if (reportsSearchCriteria.getReportType() != null
				&& reportsSearchCriteria.getReportType().equals(ReservationInternalConstants.ExtPayTxStatus.REFUNDED)) {
			sb.append("AND T.DR_CR ='" + ReservationInternalConstants.TnxTypes.DEBIT + "'");
			sb.append("AND C.service_type = 'ccrefund' ");
			sb.append("AND C.ERROR_SPECIFICATION is null ");
		} else {
			sb.append("AND T.DR_CR ='" + ReservationInternalConstants.TnxTypes.CREDIT + "'");
			sb.append("AND C.service_type = 'ccauthorize' ");
		}
		sb.append(" AND pgwCurr.PAYMENT_GATEWAY_CURRENCY_CODE = C.GATEWAY_NAME ");
		sb.append(" AND pgwCurr.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID ");
		sb.append(" AND T.product_type = p.product_type ");
		sb.append(" AND p.entity_code = '" + entityCode + "' ");
		sb.append(" order by r.booking_timestamp ");
		return sb.toString();
		
	}
	
	public String getPerDayCCTopUpFraudDetails(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		StringBuilder sb = new StringBuilder();
	

		// FIXME : will need to concat the segment code for full journey
		if (reportsSearchCriteria.isIncludeAdvanceCCDetails()) {
			sb.append("SELECT (r.total_pax_count + r.total_pax_child_count + r.total_pax_infant_count) AS paxCount");
			sb.append(",TO_CHAR(r.booking_timestamp, 'dd-mm-yyyy HH24:MI:SS') BookingTime ");
			sb.append(",TO_DATE(TRUNC(T.TIMESTAMP),'dd-mm-yyyy HH24:MI:SS') TIMESTAMP ");
		} else {
			sb.append("SELECT TO_DATE(TRUNC(T.TIMESTAMP),'dd-mm-yyyy HH24:MI:SS') TIMESTAMP ");
			sb.append(", null AS BookingTime ");
			sb.append(", null AS paxCount ");
		}
		sb.append(", T.TPT_ID TNX_ID ");
		sb.append(", T.PNR ");
		sb.append(", T.CONTACT_PERSON ");
		sb.append(", T.TELEPHONE_NO ");
		sb.append(", T.MOBILE_NO  ");
		sb.append(", T.IP_ADDRESS  ");
		sb.append(", rCon.c_email ");
		sb.append(", (DECODE (T.CHANNEL_ID,'4','WEB','20','IOS','21','ANDROID',T.USER_ID)) USER_ID");
		sb.append(", T.PAYMENT_CURRENCY_AMOUNT ");
		sb.append(", T.PAYMENT_CURRENCY_CODE  ");

		if (reportsSearchCriteria.isIncludeAdvanceCCDetails()) {
			sb.append(", F_CONCATENATE_LIST ( CURSOR (SELECT fs.segment_code ");
			sb.append("  FROM t_pnr_segment ps,t_flight_segment fs WHERE ps.flt_seg_id = fs.flt_seg_id ");
			sb.append("  AND ps.pnr = r.pnr AND ps.status = 'CNF' order by fs.est_time_departure_zulu ),'-') AS segment, ");
			sb.append("  F_CONCATENATE_LIST(CURSOR (SELECT unique fr.fare_basis_code ");
			sb.append("  FROM t_pnr_segment ps, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs, t_ond_fare ondFare, t_fare_rule fr ");
			sb.append("  WHERE ps.pnr = r.pnr AND ps.status = 'CNF' ");
			sb.append("  AND ppf.ppf_id = ppfs.ppf_id AND ppfs.pnr_seg_id = ps.pnr_seg_id AND ppf.fare_id = ondfare.fare_id ");
			sb.append("  AND ondfare.fare_rule_id = fr.fare_rule_id ), '-') AS basisCode ");
			sb.append(", F_CONCATENATE_LIST(CURSOR (SELECT TO_CHAR(fs.est_time_departure_local, 'dd-mm-yyyy HH24:MI:SS') AS depTime ");
			sb.append(" FROM t_pnr_segment ps, t_flight_segment fs ");
			sb.append(" WHERE ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = r.pnr AND ps.status = 'CNF' AND r.dummy_booking = 'N' UNION ");
			sb.append(" SELECT TO_CHAR(fs.est_time_departure_local, 'dd-mm-yyyy HH24:MI:SS') AS depTime ");
			sb.append(" FROM t_ext_pnr_segment ps, t_ext_flight_segment fs ");
			sb.append(" WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id AND ps.pnr = r.pnr AND ps.status = 'CNF' AND r.dummy_booking = 'Y' ");
			sb.append(" ),' -- ') AS DepartureDateList ");
			sb.append(" , NULL AS departure");
			sb.append(" ,f_get_fare_type_pnr_seg_id((select ps.pnr_seg_id from t_pnr_segment ps ");
			sb.append(" where ps.pnr = t.pnr and rownum = 1) ");
			sb.append(" ,(select ppf.fare_id from t_pnr_pax_fare ppf,t_pnr_passenger pp ");
			sb.append(" where pp.pnr = t.pnr and pp.pnr_pax_id = ppf.pnr_pax_id and rownum = 1)) as journeyType ");
		} else {
			sb.append(", (SELECT fs.segment_code ");
			sb.append("FROM t_pnr_segment ps, t_flight_segment fs ");
			sb.append("WHERE ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'N' and rownum = 1");
			sb.append(" union SELECT fs.segment_code ");
			sb.append("FROM t_ext_pnr_segment ps, t_ext_flight_segment fs ");
			sb.append("WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'Y' and rownum = 1 ");
			sb.append(" ) as segment ");
			sb.append(", NULL AS basisCode ");
			sb.append(", NULL AS journeyType");
			sb.append(", NULL AS DepartureDateList");
			sb.append(" , (SELECT fs.est_time_departure_local ");
			sb.append("FROM t_pnr_segment ps, t_flight_segment fs ");
			sb.append("WHERE ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'N' and rownum = 1 ");
			sb.append(" union SELECT fs.est_time_departure_local ");
			sb.append("FROM t_ext_pnr_segment ps, t_ext_flight_segment fs ");
			sb.append("WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id AND ps.pnr = t.pnr AND ps.status = 'CNF' AND r.dummy_booking   = 'Y' and rownum = 1 ");
			sb.append(" ) as departure ");
		}

		sb.append(", replace(replace(C.error_specification,'REJECTED ',''),'VALID HASH.', '') as REASON ");
		sb.append(", (DECODE(T.STATUS,'RS','ReservationSucces','I','Initiated','II','Initiated LeftOver','PF','Rejected','UF','PendingRefunds','US','SuccessfulReversals','IS','SuccessfulReversals', 'PS', 'PaymentSuccess', 'RF', 'ReservationFail')) PAYMENT_STATUS, T.FRAUD_STATUS ");
		sb.append(", T.FRAUD_MESSAGE  ");
		sb.append(", T.FRAUD_REFERENCE  ");
		sb.append(", C.NM_TRANSACTION_REFERENCE MERCHANT_REF_NUMBER ");
		sb.append(", C.TRANSACTION_REFERENCE PGW_TXN_NUMBER  ");
		sb.append(", PGW.DESCRIPTION PGW_NAME ");
		sb.append(", REGEXP_REPLACE((REGEXP_SUBSTR(C.request_text, 'vpc_OrderInfo=([a-zA-Z0-9]*-)*(.)((([0-9])*)\\.)*[0-9]*')), 'vpc_OrderInfo=', '') as order_reference ");
		sb.append("FROM T_TEMP_PAYMENT_TNX T, T_CCARD_PAYMENT_STATUS C  , t_reservation r, t_reservation_contact rCon ");
		sb.append(", t_payment_gateway_currency pgwCurr, T_PAYMENT_GATEWAY pgw ");
		sb.append("WHERE T.TPT_ID = C.TPT_ID (+) ");
		sb.append("AND r.pnr = rCon.pnr (+) ");
		sb.append("AND t.pnr = r.pnr (+) ");
		sb.append("AND T.TIMESTAMP BETWEEN TO_DATE('" + strFromDate + "  00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + "  23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		if (reportsSearchCriteria.getReportType() != null
				&& reportsSearchCriteria.getReportType().equals(ReservationInternalConstants.ExtPayTxStatus.REFUNDED)) {
			sb.append("AND T.DR_CR ='" + ReservationInternalConstants.TnxTypes.DEBIT + "'");
			sb.append("AND C.service_type = 'ccrefund' ");
			sb.append("AND C.ERROR_SPECIFICATION is null ");
		} else {
			sb.append("AND T.DR_CR ='" + ReservationInternalConstants.TnxTypes.CREDIT + "'");
			sb.append("AND C.service_type = 'ccauthorize' ");
		}
		sb.append(" AND T.PRODUCT_TYPE ='T' ");
		sb.append(" AND pgwCurr.PAYMENT_GATEWAY_CURRENCY_CODE = C.GATEWAY_NAME ");
		sb.append(" AND pgwCurr.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID ");
		sb.append(" order by r.booking_timestamp ");
		return sb.toString();

	}


	/**
	 * Returns the sql query for Credit Card TransactionRefundReport
	 * 
	 * @param reportsSearchCriteria
	 *            which contains the searching options
	 * @return String query
	 */
	public String getCCTransactionRefundReportQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String entityCode = reportsSearchCriteria.getEntity();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT TO_DATE(TRUNC(TIMESTAMP),'dd-mm-yy') date_of_txn,");
		sb.append(" (COUNT(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS
				+ "',1,null)) + COUNT(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS
				+ "',1,null))) SuccesRefund,");
		sb.append(" (SUM(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS
				+ "',amount,0)) + SUM(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_SUCCESS
				+ "',amount,0))) ValueOfSuccesRefund,");
		sb.append(" (COUNT(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE
				+ "',1,null)) + COUNT(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE
				+ "',1,null))) FailedRefund,");
		sb.append(" (SUM(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE
				+ "',amount,0)) + SUM(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE
				+ "',amount,0))) ValueOfFailedRefund,");
		sb.append(" (COUNT(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED
				+ "',1,null)) + COUNT(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT
				+ "',1,null))) InitiatedRefund,");
		sb.append(" (SUM(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED
				+ "',amount,0)) + SUM(DECODE (status,'" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_NO_PAYMENT
				+ "',amount,0)))ValueOfInitiatedRefund");
		sb.append(" FROM T_TEMP_PAYMENT_TNX TPT, T_PRODUCT P ");
		sb.append(" WHERE TIMESTAMP between TO_DATE('" + strFromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss')");
		sb.append(" AND DR_CR = '" + ReservationInternalConstants.TnxTypes.DEBIT + "'");
		sb.append(" AND TPT.product_type = P.product_type ");
		sb.append(" AND P.entity_code = '"+ entityCode + "' ");
		sb.append(" GROUP BY TRUNC(timestamp)");
		sb.append(" order by date_of_txn asc ");
		return sb.toString();

	}

	/**
	 * Returns the sql query for Credit Card TransactionPendingRefundReport
	 * 
	 * @param reportsSearchCriteria
	 *            which contains the searching options
	 * @return String query
	 */
	public String getCCTransactionPendingRefundReportQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String entityCode = reportsSearchCriteria.getEntity();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT TO_DATE(TRUNC(tnx.TIMESTAMP),'dd-mm-yy') date_of_txn,TO_CHAR(tnx.timestamp,'hh24:mi:ss') time_of_txn,");
		sb.append(" tnx.pnr pnr_number, tnx.contact_person contact_person,tnx.mobile_no mobile,");
		sb.append(" tnx.ip_address ip,tnx.amount payment,");
		sb.append(" (SELECT tpc.transaction_ref_cccompany FROM t_ccard_payment_status  tpc ");
		sb.append(" 	WHERE TPC.tpt_id=tnx.tpt_id AND tpc.service_type = 'ccrefund'");
		sb.append(" 	AND TRANSACTION_REF_NO =(SELECT MAX(TRANSACTION_REF_NO) FROM  t_ccard_payment_status");
		sb.append("									WHERE TPT_ID=TPC.tpt_id)");
		sb.append("	)merchant,");
		sb.append(" (SELECT tpc.nm_transaction_reference FROM t_ccard_payment_status  tpc");
		sb.append("		WHERE TPC.tpt_id=tnx.tpt_id AND tpc.service_type = 'ccrefund'");
		sb.append("		AND TRANSACTION_REF_NO=(SELECT MAX(TRANSACTION_REF_NO) FROM  t_ccard_payment_status");
		sb.append("								WHERE TPT_ID=TPC.tpt_id)");
		sb.append(" )pgw");
		sb.append(" FROM t_temp_payment_tnx tnx, t_product p ");
		sb.append(" 	WHERE tnx.timestamp BETWEEN TO_DATE('" + strFromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss')");
		sb.append(" AND tnx.status IN ('" + ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED + "', '"
				+ ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST + "','"
				+ ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE + "') AND tnx.dr_cr = '"
				+ ReservationInternalConstants.TnxTypes.CREDIT + "'");
		sb.append(" AND  tnx.product_type = p.product_type ");
		sb.append(" AND p.entity_code = '"+ entityCode + "' ");
		/*
		 * sb.append(" AND ((tnx.status IN('"+ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_REFUND_FAILED+"','"
		 * +ReservationInternalConstants.TempPaymentTnxTypes.INITIATED_PAYMENT_EXIST+"')");
		 * sb.append(" AND tnx.dr_cr = '"
		 * +ReservationInternalConstants.TnxTypes.DEBIT+"') OR (tnx.dr_cr = '"+ReservationInternalConstants
		 * .TnxTypes.CREDIT
		 * +"' AND tnx.status = '"+ReservationInternalConstants.TempPaymentTnxTypes.UNDO_OPERATION_FAILURE+"'))");
		 */
		return sb.toString();
	}

	/**
	 * Freight report
	 * 
	 * @return String the Query
	 */
	public String getFreightReport(ReportsSearchCriteria reportsSearchCriteria) {

		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strFlightNumber = reportsSearchCriteria.getFlightNumber();
		StringBuilder sb = new StringBuilder();

		sb.append("select flight_date,flight_number,segment_code,fare,tax_surcharges, ");
		sb.append("sum(nvl(EXCESS_WEIGHT_KGS,0))Excess_Weight, ");
		sb.append("sum(nvl(EXCESS_WEIGHT_TOTAL_REVENUE,0))Excess_Collection, ");
		sb.append("sum(decode(FREIGHT_TYPE_CODE,'CRO',EXCESS_WEIGHT_KGS,0))Cargo_Weight, ");
		sb.append("sum(decode(FREIGHT_TYPE_CODE,'CRO',EXCESS_WEIGHT_TOTAL_REVENUE,0))Cargo_Collection ");
		sb.append(" from ");
		sb.append("( ");
		sb.append("SELECT trunc(b.est_time_departure_local)flight_date , a.flight_number, b.segment_code,flt_seg_id, ");
		sb.append("(SELECT SUM( G.AMOUNT) ");
		sb.append("FROM t_pnr_pax_seg_charges g ");
		sb.append("WHERE g.ppfs_id IN ");
		sb.append(" (SELECT E.ppfs_id FROM t_pnr_pax_fare_segment  e ");
		sb.append(" WHERE E.pnr_seg_id IN ");
		sb.append(" (SELECT D.pnr_seg_id FROM t_pnr_segment  d ");
		sb.append(" WHERE  d.status  = 'CNF' AND ");
		sb.append(" d.flt_seg_id =b.flt_seg_id)AND ");
		sb.append(" e.ppf_id IN(SELECT F.ppf_id FROM t_pnr_pax_ond_charges  f ");
		sb.append(" WHERE f.pft_id=g.pft_id AND f.charge_group_code = 'FAR' )) ");
		sb.append(" )fare, ");
		sb.append("  (SELECT SUM( G.AMOUNT) ");
		sb.append(" FROM t_pnr_pax_seg_charges g ");
		sb.append(" WHERE g.ppfs_id IN ");
		sb.append(" (SELECT E.ppfs_id FROM t_pnr_pax_fare_segment  e ");
		sb.append(" WHERE  E.pnr_seg_id IN ");
		sb.append(" (SELECT D.pnr_seg_id FROM t_pnr_segment  d ");
		sb.append(" WHERE  d.status  = 'CNF' AND ");
		sb.append(" d.flt_seg_id =b.flt_seg_id)AND ");
		sb.append(" e.ppf_id IN(SELECT F.ppf_id FROM t_pnr_pax_ond_charges  f ");
		sb.append(" WHERE f.pft_id=g.pft_id AND f.charge_group_code in('TAX','SUR') )) ");
		sb.append(" )tax_surcharges ");
		sb.append(" FROM t_flight a, t_flight_segment b ");
		sb.append(" WHERE ");
		if (isNotEmptyOrNull(strFlightNumber)) {
			sb.append(" a.flight_number='" + strFlightNumber + "'  and "); // optional
		}
		sb.append(" b.est_time_departure_local  between  to_date('" + strFromDate + " 00:00:00', 'DD-MON-YYYY HH24:mi:ss')and ");
		sb.append(" to_date('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:mi:ss')and ");
		sb.append("   a.flight_id = b.flight_id  ");
		sb.append(" )a, t_flight_segment_exces_freight  c ");
		sb.append(" where a.flt_seg_id=c.flt_seg_id ");
		sb.append(" group by flight_date,flight_number,segment_code,fare,tax_surcharges ");
		sb.append(" order by flight_date,flight_number,segment_code ");

		return sb.toString();
	}

	/**
	 * sector contribution by agent report
	 * 
	 * @return String the Query
	 */
	public String getSectorContributionAgent(ReportsSearchCriteria reportsSearchCriteria) {

		String strSectorFrom = reportsSearchCriteria.getSectorFrom();
		String strSectorTo = reportsSearchCriteria.getSectorTo();
		String strDateFrom = reportsSearchCriteria.getDateRangeFrom();
		String strDateTo = reportsSearchCriteria.getDateRangeTo();

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT a.agent_code AS agent_code, a.agent_name,count(distinct(p.pnr_pax_id))pax, ");
		sb.append("	SUM (DECODE (charge_group_code, 'CNX', pfst.amount, 'MOD', pfst.amount, 'FAR', pfst.amount, 'INF',  pfst.amount, 'SUR', pfst.amount, 0)) revenue, ");
		sb.append("	SUM (DECODE (charge_group_code, 'CNX', pfst.amount, 'MOD', pfst.amount, 'FAR', pfst.amount, 'INF',  pfst.amount, 'SUR', pfst.amount, 0))/count(distinct(p.pnr_pax_id)) yield ");
		sb.append(" FROM  t_pnr_pax_seg_charges pfst, t_pnr_pax_fare_segment ppfs, t_reservation r, t_pnr_segment ps, t_agent a, ");
		sb.append("		 t_pnr_pax_ond_charges pft,t_pnr_pax_fare ppf,t_pnr_passenger p ");
		sb.append("WHERE  r.pnr=ps.pnr                  and ");
		sb.append("	ps.pnr_seg_id=ppfs.pnr_seg_id and ");
		sb.append("	p.pnr=r.pnr                   and ");
		sb.append("	p.pnr_pax_id=ppf.pnr_pax_id   and ");
		sb.append("	ppfs.ppf_id=ppf.ppf_id        and ");
		sb.append("	ppf.ppf_id=pft.ppf_id         and ");
		sb.append("	pfst.ppfs_id=ppfs.ppfs_id     and ");
		sb.append(" pfst.pft_id=pft.pft_id        and ");
		sb.append(" r.origin_agent_code=a.agent_code   and ");
		sb.append(" r.status='CNF' and ps.status='CNF' and ");
		sb.append(" booking_timestamp BETWEEN to_date('" + strDateFrom + " 00:00:00', 'DD-MON-YYYY HH24:mi:ss') and to_date('"
				+ strDateTo + " 23:59:59', 'DD-MON-YYYY HH24:mi:ss') AND ");
		sb.append(" ps.flt_seg_id IN (SELECT flt_seg_id FROM t_flight_segment ");
		sb.append(" WHERE ");
		sb.append(" SUBSTR(segment_code, 0, 3) = UPPER('" + strSectorFrom + "') AND ");
		sb.append(" SUBSTR(segment_code, (LENGTH(segment_code) - 2), 3) = UPPER('" + strSectorTo + "') )  "); // segment
		sb.append("	GROUP BY  a.agent_code, a.agent_name ");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Admin Audit Report.
	 * 
	 * @param reportsSearchCriteria
	 * @return String query
	 */
	public String getAdministrationAuditDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String strSearchFrom = reportsSearchCriteria.getDateRangeFrom();
		String strSearchTo = reportsSearchCriteria.getDateRangeTo();
		String taskGroup = reportsSearchCriteria.getTaskGroup();
		String task = reportsSearchCriteria.getTask();
		String userId = reportsSearchCriteria.getUserId();
		String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String content = reportsSearchCriteria.getContentSearch();

		// Replaces * with % and ? with _
		List<String> replaceOptions = new ArrayList<String>();
		replaceOptions.add("*");
		replaceOptions.add("?");
		content = BeanUtils.wildCardParser(content, replaceOptions);

		StringBuilder sb = new StringBuilder();

		try {
			if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeFrom() == "")
					|| (reportsSearchCriteria.getDateRangeTo() == null) || (reportsSearchCriteria.getDateRangeTo() == "")) {

				throw new ModuleException("reporting.date.invalid");
			}

		} catch (ModuleException ex) {
			log.error("Invalid date error" + ex.getMessageString());
		}

		sb.append("SELECT a.audit_id, b.description, to_char(a.timestamp,'dd/mm/yyyy HH24:mi:ss') as timestamp, a.user_id, a.content");
		sb.append("	FROM   t_audit a, t_task b, t_agent c, t_user d, t_task_group e");

		sb.append("	WHERE  a.task_code = b.task_code");
		sb.append("       AND    c.agent_code = d.agent_code");
		sb.append("       AND (a.user_id = d.user_id OR substr(a.user_id,1,instr(a.user_id,'$',1)-1) = d.user_id) ");
		sb.append(" 	  AND 	b.task_group_code = e.task_group_code");
		sb.append("		 AND a.timestamp BETWEEN TO_DATE('" + strSearchFrom + " 00:00:00','dd-mon-yyyy hh24:mi:ss')");
		sb.append("	     AND TO_DATE('" + strSearchTo + " 23:59:59','dd-mon-yyyy hh24:mi:ss')");

		if (!taskGroup.equalsIgnoreCase("All")) {
			sb.append("       AND    b.task_group_code IN ('" + taskGroup + "')");
		}

		if (isNotEmptyOrNull(task)) {
			if (!task.equalsIgnoreCase("All")) {
				sb.append("       AND    a.task_code IN (" + task + ")");
			}
		}

		if (isNotEmptyOrNull(userId)) {
			if (!userId.equalsIgnoreCase("All")) {
				sb.append(" 	  AND    d.user_id  IN ('" + userId + "')");
			}
		}

		if (isNotEmptyOrNull(strAgents)) {
			sb.append(" 	  AND ");
			sb.append(ReportUtils.getReplaceStringForIN("c.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		}

		if (isNotEmptyOrNull(content)) {
			sb.append(" 	AND a.content LIKE ('%" + content + "%')");

		}

		sb.append("			ORDER BY a.audit_id");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Audit Report.
	 * 
	 * @param reportsSearchCriteria
	 * @return String query
	 */
	public String getReservationAuditDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String strSearchFrom = reportsSearchCriteria.getDateRangeFrom();
		String strSearchTo = reportsSearchCriteria.getDateRangeTo();
		String taskGroup = reportsSearchCriteria.getTaskGroup();
		String pnrNumber = reportsSearchCriteria.getPnr();
		String userId = reportsSearchCriteria.getUserId();
		String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String content = reportsSearchCriteria.getContentSearch();

		// Replaces * with % and ? with _
		content = BeanUtils.wildCardParser(content, null);

		StringBuilder sb = new StringBuilder();

		try {
			if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeFrom() == "")
					|| (reportsSearchCriteria.getDateRangeTo() == null) || (reportsSearchCriteria.getDateRangeTo() == "")) {

				throw new ModuleException("reporting.date.invalid");
			}

		} catch (ModuleException ex) {
			log.error("Invalid date error" + ex.getMessageString());
		}

		sb.append("SELECT a.audit_id, b.template_desc , a.pnr, ");
		sb.append("to_char(a.timestamp,'dd/mm/yyyy HH24:mi:ss') as timestamp, a.user_id, a.user_id, NVL(to_char(a.customer_id),'-') as customer_id, ");
		sb.append(" NVL(to_char(a.content),'-') as content, NVL(to_char(a.user_notes),'-') as user_notes ");
		sb.append("from t_reservation_audit a, t_audit_template b, t_agent c, t_user d ");
		sb.append("where a.template_code=b.template_code AND ");
		sb.append("c.agent_code = d.agent_code  AND  a.user_id = d.user_id ");
		sb.append("AND a.timestamp BETWEEN TO_DATE('" + strSearchFrom + " 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
		sb.append("AND TO_DATE('" + strSearchTo + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");

		if (!taskGroup.equalsIgnoreCase("All")) {
			sb.append("       AND    b.template_code IN ('" + taskGroup + "')");
		}

		if (isNotEmptyOrNull(userId)) {
			if (!userId.equalsIgnoreCase("All")) {
				sb.append(" 	  AND    d.user_id  IN ('" + userId + "')");
			}
		}

		if (isNotEmptyOrNull(strAgents)) {
			sb.append(" 	  AND ");
			sb.append(ReportUtils.getReplaceStringForIN("c.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		}

		if (isNotEmptyOrNull(content)) {
			sb.append(" 	AND a.content LIKE ('%" + content + "%')");

		}
		if (isNotEmptyOrNull(pnrNumber)) {

			sb.append(" 	  AND    a.pnr  IN ('" + pnrNumber + "')");

		}

		sb.append("			ORDER BY a.audit_id");

		return sb.toString();
	}

	/**
	 * Method to create the query for the Reservation Audit Report.
	 * 
	 * @param reportsSearchCriteria
	 * @return String query
	 */
	public String getPaxStatusDetailQuery(ReportsSearchCriteria reportsSearchCriteria, int option) {
		String strSearchFrom = reportsSearchCriteria.getDateRangeFrom();
		String strSearchTo = reportsSearchCriteria.getDateRangeTo();
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String selDept = reportsSearchCriteria.getFrom();
		String selArrival = reportsSearchCriteria.getTo();
		String selPaxStatus = reportsSearchCriteria.getStatus();
		String selPfsStatus = reportsSearchCriteria.getPfsStatus();
		String basedOnAgent = reportsSearchCriteria.getBasedOnAgent();

		StringBuilder sb = new StringBuilder();

		try {
			if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeFrom() == "")
					|| (reportsSearchCriteria.getDateRangeTo() == null) || (reportsSearchCriteria.getDateRangeTo() == "")) {

				throw new ModuleException("reporting.date.invalid");
			}

		} catch (ModuleException ex) {
			log.error("Invalid date error" + ex.getMessageString());
		}

		if (option == 1) {
			sb.append("SELECT * FROM ( ");

			sb.append("SELECT DISTINCT TO_CHAR(FS.EST_TIME_DEPARTURE_LOCAL,'dd/mm/yyyy HH24:mi:ss') AS DEPARTURE_DATE, ");
			sb.append(" F.FLIGHT_NUMBER AS FLIGHT_NUMBER, F.ORIGIN AS ORIGIN, F.DESTINATION AS DESTINATION, R.PNR AS PNR_NUMBER, ");
			sb.append(" INITCAP(P.TITLE) ||' ' ||INITCAP(P.FIRST_NAME) ||' ' ||INITCAP(P.LAST_NAME)      AS PAX_NAME, ");
			sb.append(" PPFS.PAX_STATUS AS PAX_STATUS, PA.PASSPORT_NUMBER AS PASSPORT_NUMBER, P.TITLE AS PAX_TITLE, P.PAX_TYPE_CODE AS PAX_TYPE_CODE, ");
			sb.append(" decode(p.pax_type_code,'IN',F_GET_INFANT_CABIN_CLASS_CODE(P.PNR_PAX_ID,S.PNR_SEG_ID,'CC'),bc.logical_cabin_class_code) AS COS, ");
			sb.append(" nvl(F_CONCATENATE_LIST ( CURSOR(SELECT  (CASE WHEN PPFSET.ext_e_ticket_number is not null "
					+ " THEN PPFSET.ext_e_ticket_number ELSE pet.e_ticket_number END) "
					+ " FROM t_pax_e_ticket pet, t_pnr_pax_fare_seg_e_ticket PPFSET "
					+ " WHERE p.pnr_pax_id = pet.pnr_pax_id AND pet.pax_e_ticket_id = PPFSET.pax_e_ticket_id AND PPFSET.ppfs_id=PPFS.ppfs_id),'-'),' ') AS ETICKET_ID,");

			sb.append(" decode(p.pax_type_code,'IN',null,bc.BOOKING_CODE) AS BOOKING_CODE, ");
			sb.append(" (SELECT UNIQUE(PPFSET.status) FROM t_pax_e_ticket pet,t_pnr_pax_fare_seg_e_ticket PPFSET WHERE  PET.pnr_pax_id = P.PNR_PAX_ID ");
			sb.append(" AND PPFSET.pax_e_ticket_id=PET.pax_e_ticket_id AND PPFSET.ppfs_id=PPFS.ppfs_id ");
			sb.append(" AND PPFSET.pnr_pax_fare_seg_e_ticket_id IN(SELECT MAX(pnr_pax_fare_seg_e_ticket_id) ");
//AEROMART-3168		sb.append(" FROM t_pnr_pax_fare_seg_e_ticket PPFSET1 WHERE PPFSET1.pax_e_ticket_id=PET.pax_e_ticket_id AND PPFSET1.ppfs_id=PPFSET.ppfs_id) and rownum=1)ETICKET_STATUS ");
			sb.append(" FROM t_pnr_pax_fare_seg_e_ticket PPFSET1 WHERE PPFSET1.ppfs_id=PPFSET.ppfs_id))ETICKET_STATUS ");
		}

		if (option == 2) {
			sb.append("SELECT DISTINCT ");
			sb.append("CASE WHEN y.mf_count = 1 THEN 'Male' WHEN y.mf_count = 2 THEN 'Female' ");
			sb.append("WHEN y.mf_count = 3 THEN 'Child' WHEN y.mf_count = 4 THEN 'Infant' ELSE 'Unknown' END AS gender , ");
			sb.append("COUNT (*) OVER (PARTITION BY y.mf_count) AS gender_count FROM ");
			sb.append("(SELECT x.PNR_NUMBER, x.PAX_TITLE , x.PAX_TYPE_CODE , CASE WHEN x.PAX_TYPE_CODE = 'AD' THEN CASE WHEN x.PAX_TITLE IN ");
			sb.append("( SELECT a.title_code FROM T_PAX_TITLE a WHERE a.gender = 'M') THEN 1 WHEN x.PAX_TITLE IN ( SELECT a.title_code FROM T_PAX_TITLE a WHERE a.gender = 'F') ");
			sb.append("THEN 2 ELSE 5 END WHEN x.PAX_TYPE_CODE = 'CH' THEN 3 WHEN x.PAX_TYPE_CODE = 'IN' THEN 4 WHEN (x.PAX_TITLE   IS NULL AND x.PAX_TYPE_CODE != 'IN' ) THEN 5 ");
			sb.append("END AS mf_count FROM ( ");
			sb.append("SELECT DISTINCT P.PNR_PAX_ID, ");
			sb.append(" R.PNR AS PNR_NUMBER ,P.TITLE AS PAX_TITLE ,P.PAX_TYPE_CODE AS PAX_TYPE_CODE ");
		}

		sb.append("FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT S, T_RESERVATION R, T_PNR_PASSENGER P, T_PFS_PARSED PP, T_PNR_PAX_ADDITIONAL_INFO PA ");
		sb.append(" ,T_PNR_PAX_FARE PPF, T_PNR_PAX_FARE_SEGMENT PPFS, t_pnr_pax_fare_seg_e_ticket ppfst,t_pax_e_ticket pet, T_BOOKING_CLASS bc ");
		if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) {
			sb.append(", T_AGENT AGENT ");
		}

		sb.append("WHERE R.PNR = P.PNR AND R.PNR = S.PNR AND P.PNR = PP.PNR(+) AND  P.PNR_PAX_ID=PA.PNR_PAX_ID AND S.FLT_SEG_ID = FS.FLT_SEG_ID AND FS.FLIGHT_ID = F.FLIGHT_ID ");
		sb.append(" AND p.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id AND s.pnr_seg_id = ppfs.pnr_seg_id ");
		sb.append(" AND BC.BC_TYPE <> 'OPENRT' AND NVL(ppfs.booking_code,F_GET_INFANT_CABIN_CLASS_CODE(P.PNR_PAX_ID,S.PNR_SEG_ID,'BC') ) = bc.booking_code");
		sb.append(" AND FS.EST_TIME_DEPARTURE_ZULU BETWEEN TO_DATE('" + strSearchFrom + " 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
		sb.append(" AND TO_DATE('" + strSearchTo + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		sb.append(" AND (S.sub_status IS NULL OR SUB_STATUS<>'EX') ");

		if (isNotEmptyOrNull(selPaxStatus) && !selPaxStatus.equalsIgnoreCase("ALL")) {
			sb.append("  AND    P.STATUS IN ('" + selPaxStatus + "')");
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(selPaxStatus)
					|| ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(selPaxStatus)) {
				sb.append("  AND    S.STATUS = 'CNF' ");
			} else {
				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(selPaxStatus)) {
					sb.append("  AND    S.STATUS = 'CNX' ");
				}
			}
		}

		sb.append(" AND P.PNR_PAX_ID = pet.pnr_pax_id ");
		sb.append(" AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id AND ppfst.ppfs_id = ppfs.ppfs_id ");

		if (isNotEmptyOrNull(selPfsStatus) && !selPfsStatus.equalsIgnoreCase("ALL")) {
			sb.append(" AND ppfs.pax_status IN ('" + selPfsStatus + "')");
		}

		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" 	  AND    F.FLIGHT_NUMBER  IN ('" + flightNumber + "')");

		}

		if (isNotEmptyOrNull(selDept)) {
			sb.append("   AND    F.ORIGIN  IN ('" + selDept + "')");
		}

		if (isNotEmptyOrNull(selArrival)) {
			sb.append(" 	AND F.DESTINATION IN ('" + selArrival + "')");

		}

		if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) {
			sb.append(" AND " + ReportUtils.getReplaceStringForIN(" AGENT.agent_code", reportsSearchCriteria.getAgents(), false)
					+ "  ");
			if(basedOnAgent.equals(ReportsSearchCriteria.BASED_ON_ORIGIN_AGENT)){
				sb.append(" AND R.ORIGIN_AGENT_CODE = AGENT.AGENT_CODE ");
			}else {
				sb.append(" AND R.OWNER_AGENT_CODE = AGENT.AGENT_CODE ");
			}
			
		}

		sb.append(" UNION ALL ");

		if (option == 1) {
			sb.append("SELECT to_char(FS.EST_TIME_DEPARTURE_LOCAL,'dd/mm/yyyy HH24:mi:ss') as DEPARTURE_DATE, ");
			sb.append("FS.FLIGHT_NUMBER as FLIGHT_NUMBER, SUBSTR(TRIM(FS.SEGMENT_CODE),1,3) as ORIGIN, ");
			sb.append("SUBSTR(TRIM(FS.SEGMENT_CODE),5) AS DESTINATION, R.PNR AS PNR_NUMBER, ");
			sb.append("INITCAP(P.TITLE)||' '||INITCAP(P.FIRST_NAME)||' '||INITCAP(P.LAST_NAME) AS PAX_NAME, ");
			sb.append("PPFS.PAX_STATUS AS PAX_STATUS, PA.PASSPORT_NUMBER AS PASSPORT_NUMBER,");

			sb.append("P.TITLE AS PAX_TITLE, P.PAX_TYPE_CODE AS PAX_TYPE_CODE, ");
			sb.append(" nvl(F_CONCATENATE_LIST ( CURSOR(SELECT  (CASE WHEN PPFSET.ext_e_ticket_number is not null "
					+ " THEN PPFSET.ext_e_ticket_number ELSE pet.e_ticket_number END) "
					+ " FROM t_pax_e_ticket pet, t_pnr_pax_fare_seg_e_ticket PPFSET "
					+ " WHERE p.pnr_pax_id = pet.pnr_pax_id AND pet.pax_e_ticket_id = PPFSET.pax_e_ticket_id AND PPFSET.ppfs_id=PPFS.ppfs_id),'-'),' ') AS ETICKET_ID,");

			sb.append(" decode(p.pax_type_code,'IN', null ,bc.BOOKING_CODE) AS BOOKING_CODE, ");
			sb.append(" decode(p.pax_type_code,'IN',F_GET_INFANT_CABIN_CLASS_CODE(P.PNR_PAX_ID,S.EXT_PNR_SEG_ID,'CC'),bc.logical_cabin_class_code) AS COS, ");
			sb.append(" (SELECT PPFSET.status FROM t_pax_e_ticket pet,t_pnr_pax_fare_seg_e_ticket PPFSET ");
			sb.append(" WHERE  PET.pnr_pax_id = P.PNR_PAX_ID AND PPFSET.pax_e_ticket_id=PET.pax_e_ticket_id ");
			sb.append(" AND PPFSET.ppfs_id=PPFS.ppfs_id AND PPFSET.pnr_pax_fare_seg_e_ticket_id IN(SELECT MAX(pnr_pax_fare_seg_e_ticket_id) ");
			sb.append(" FROM t_pnr_pax_fare_seg_e_ticket PPFSET1 WHERE PPFSET1.pax_e_ticket_id=PET.pax_e_ticket_id AND PPFSET1.ppfs_id=PPFSET.ppfs_id) ");
			sb.append(" and rownum=1)ETICKET_STATUS ");
		}

		if (option == 2) {
			sb.append("SELECT DISTINCT P.PNR_PAX_ID, ");
			sb.append("R.PNR AS PNR_NUMBER ,P.TITLE AS PAX_TITLE ,P.PAX_TYPE_CODE AS PAX_TYPE_CODE ");
		}

		sb.append("FROM T_EXT_FLIGHT_SEGMENT FS, T_EXT_PNR_SEGMENT S, T_RESERVATION R, T_PNR_PASSENGER P, T_PFS_PARSED PP, T_PNR_PAX_ADDITIONAL_INFO PA, ");
		sb.append(" T_PNR_PAX_FARE PPF, T_PNR_PAX_FARE_SEGMENT PPFS , t_pnr_pax_fare_seg_e_ticket ppfst,t_pax_e_ticket pet,T_BOOKING_CLASS bc ");
		sb.append("WHERE R.PNR = P.PNR AND P.PNR = PP.PNR(+) AND R.PNR = S.PNR AND P.PNR_PAX_ID=PA.PNR_PAX_ID AND S.EXT_FLT_SEG_ID = FS.EXT_FLT_SEG_ID ");
		sb.append(" AND p.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id ");
		sb.append(" AND BC.BC_TYPE <> 'OPENRT' AND NVL(ppfs.booking_code,F_GET_INFANT_CABIN_CLASS_CODE(P.PNR_PAX_ID,S.EXT_PNR_SEG_ID,'BC') ) = bc.booking_code ");
		sb.append(" AND FS.EST_TIME_DEPARTURE_ZULU BETWEEN TO_DATE('" + strSearchFrom + " 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
		sb.append(" AND TO_DATE('" + strSearchTo + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		sb.append(" AND (S.sub_status IS NULL OR SUB_STATUS<>'EX') ");

		if (isNotEmptyOrNull(selPaxStatus) && !selPaxStatus.equalsIgnoreCase("ALL")) {
			sb.append("  AND    P.STATUS IN ('" + selPaxStatus + "')");
		}

		sb.append(" AND P.PNR_PAX_ID = pet.pnr_pax_id ");
		sb.append(" AND pet.pax_e_ticket_id = ppfst.pax_e_ticket_id AND ppfst.ppfs_id = ppfs.ppfs_id ");
		if (isNotEmptyOrNull(selPfsStatus) && !selPfsStatus.equalsIgnoreCase("ALL")) {
			sb.append(" AND ppfs.pax_status IN ('" + selPfsStatus + "')");
			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(selPaxStatus)
					|| ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(selPaxStatus)) {
				sb.append("  AND    S.STATUS = 'CNF' ");
			} else {
				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(selPaxStatus)) {
					sb.append("  AND    S.STATUS = 'CNX' ");
				}
			}
		}

		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" 	  AND    FS.FLIGHT_NUMBER  IN ('" + flightNumber + "')");

		}

		if (isNotEmptyOrNull(selDept)) {
			sb.append(" AND SUBSTR(TRIM(FS.SEGMENT_CODE),1,3) = '" + selDept + "' ");
		}

		if (isNotEmptyOrNull(selArrival)) {
			sb.append(" AND SUBSTR(TRIM(FS.SEGMENT_CODE),5) = '" + selArrival + "' ");
		}

		if (isNotEmptyOrNull(selPaxStatus) && !selPaxStatus.equalsIgnoreCase("CNF")) {
			sb.append(" UNION ALL ");
			if (option == 1) {
				sb.append("SELECT * FROM ( ");

				sb.append("SELECT DISTINCT to_char(FS.EST_TIME_DEPARTURE_LOCAL,'dd/mm/yyyy HH24:mi:ss') as DEPARTURE_DATE, ");
				sb.append("F.FLIGHT_NUMBER as FLIGHT_NUMBER, F.ORIGIN as ORIGIN, ");
				sb.append("F.DESTINATION AS DESTINATION, R.PNR AS PNR_NUMBER, ");
				sb.append("INITCAP(P.TITLE)||' '||INITCAP(P.FIRST_NAME)||' '||INITCAP(P.LAST_NAME) AS PAX_NAME, ");
				sb.append("PPFS.PAX_STATUS AS PAX_STATUS, PA.PASSPORT_NUMBER AS PASSPORT_NUMBER,");

				sb.append("P.TITLE AS PAX_TITLE, P.PAX_TYPE_CODE AS PAX_TYPE_CODE, ");
				sb.append(
						" DECODE(p.pax_type_code,'IN',F_GET_INFANT_CABIN_CLASS_CODE(P.PNR_PAX_ID,S.PNR_SEG_ID,'CC'),bc.logical_cabin_class_code) AS COS, ");
				sb.append(" null AS ETICKET_ID,  ");
				sb.append(" decode(p.pax_type_code,'IN',null,bc.BOOKING_CODE) AS BOOKING_CODE, ");
				sb.append(" null AS ETICKET_STATUS ");
			}

			if (option == 2) {
				sb.append("SELECT DISTINCT P.PNR_PAX_ID, ");
				sb.append(" R.PNR AS PNR_NUMBER ,P.TITLE AS PAX_TITLE ,P.PAX_TYPE_CODE AS PAX_TYPE_CODE ");
			}

			sb.append(
					"FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT S, T_RESERVATION R, T_PNR_PASSENGER P, T_PNR_PAX_ADDITIONAL_INFO PA ");
			sb.append(" ,T_PNR_PAX_FARE PPF, T_PNR_PAX_FARE_SEGMENT PPFS, T_BOOKING_CLASS bc  ");
			if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) {
				sb.append(", T_AGENT AGENT ");
			}

			sb.append("WHERE R.PNR = P.PNR AND R.PNR = S.PNR AND P.PNR_PAX_ID=PA.PNR_PAX_ID AND S.FLT_SEG_ID = FS.FLT_SEG_ID AND FS.FLIGHT_ID = F.FLIGHT_ID ");
			sb.append(" AND p.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id AND ( ppfs.booking_code <> 'OPENRTY' OR ppfs.booking_code IS NULL ) AND s.pnr_seg_id = ppfs.pnr_seg_id ");
			sb.append(" AND NVL(ppfs.booking_code,F_GET_INFANT_CABIN_CLASS_CODE(P.PNR_PAX_ID,S.PNR_SEG_ID,'BC') ) = bc.booking_code ");
			sb.append("AND FS.EST_TIME_DEPARTURE_ZULU BETWEEN TO_DATE('" + strSearchFrom
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
			sb.append("AND TO_DATE('" + strSearchTo + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");

			if (isNotEmptyOrNull(selPaxStatus) && selPaxStatus.equalsIgnoreCase("CNX")) {
				sb.append("  AND  (  P.STATUS IN ('" + selPaxStatus + "')");
				sb.append(" OR  S.STATUS IN ('CNX')) ");
			} else if (isNotEmptyOrNull(selPaxStatus) && selPaxStatus.equalsIgnoreCase("ALL")) {
				sb.append("  AND    P.STATUS IN ('OHD' , 'CNX')");
			} else if (isNotEmptyOrNull(selPaxStatus) && !selPaxStatus.equalsIgnoreCase("ALL")) {
				sb.append("  AND  (  P.STATUS IN ('" + selPaxStatus + "')");
				sb.append(" AND S.STATUS IN ('CNF'))");
			}

			if (isNotEmptyOrNull(selPaxStatus) && !selPaxStatus.equalsIgnoreCase("OHD")) {
				sb.append("  AND R.pnr IN ");
				sb.append("  (SELECT x.pnr ");
				sb.append("  FROM t_reservation_audit x ");
				sb.append("  WHERE x.template_code = 'NEWONH') ");
			}

			if (isNotEmptyOrNull(selPfsStatus) && !selPfsStatus.equalsIgnoreCase("ALL")) {
				sb.append(" AND ppfs.pax_status IN ('" + selPfsStatus + "')");
			}

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" 	  AND    F.FLIGHT_NUMBER  IN ('" + flightNumber + "')");

			}

			if (isNotEmptyOrNull(selDept)) {
				sb.append("   AND    F.ORIGIN  IN ('" + selDept + "')");
			}

			if (isNotEmptyOrNull(selArrival)) {
				sb.append(" 	AND F.DESTINATION IN ('" + selArrival + "')");

			}

			if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) {
				sb.append(" AND "
						+ ReportUtils.getReplaceStringForIN(" AGENT.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
				if(basedOnAgent.equals(ReportsSearchCriteria.BASED_ON_ORIGIN_AGENT)){
					sb.append(" AND R.ORIGIN_AGENT_CODE = AGENT.AGENT_CODE ");
				}else {
					sb.append(" AND R.OWNER_AGENT_CODE = AGENT.AGENT_CODE ");
				}
			}
			sb.append(" AND R.STATUS IN ('OHD','CNX') ");

			sb.append(" UNION ALL ");

			if (option == 1) {
				sb.append("SELECT to_char(FS.EST_TIME_DEPARTURE_LOCAL,'dd/mm/yyyy HH24:mi:ss') as DEPARTURE_DATE, ");
				sb.append("FS.FLIGHT_NUMBER as FLIGHT_NUMBER, SUBSTR(TRIM(FS.SEGMENT_CODE),1,3) as ORIGIN, ");
				sb.append("SUBSTR(TRIM(FS.SEGMENT_CODE),5) AS DESTINATION, R.PNR AS PNR_NUMBER, ");
				sb.append("INITCAP(P.TITLE)||' '||INITCAP(P.FIRST_NAME)||' '||INITCAP(P.LAST_NAME) AS PAX_NAME, ");
				sb.append("PPFS.PAX_STATUS AS PAX_STATUS, PA.PASSPORT_NUMBER AS PASSPORT_NUMBER,");

				sb.append("P.TITLE AS PAX_TITLE, P.PAX_TYPE_CODE AS PAX_TYPE_CODE, ");
				sb.append(" bc.logical_cabin_class_code AS COS, ");
				sb.append(" null AS ETICKET_ID,  ");
				sb.append(" decode(p.pax_type_code,'IN',null,bc.BOOKING_CODE) AS BOOKING_CODE, ");
				sb.append(" null AS ETICKET_STATUS ");
			}

			if (option == 2) {
				sb.append("SELECT DISTINCT P.PNR_PAX_ID, ");
				sb.append( " R.PNR AS PNR_NUMBER ,P.TITLE AS PAX_TITLE ,P.PAX_TYPE_CODE AS PAX_TYPE_CODE ");
			}

			sb.append("FROM T_EXT_FLIGHT_SEGMENT FS, T_EXT_PNR_SEGMENT S, T_RESERVATION R, T_PNR_PASSENGER P, T_PNR_PAX_ADDITIONAL_INFO PA, ");
			sb.append(" T_PNR_PAX_FARE PPF, T_PNR_PAX_FARE_SEGMENT PPFS , T_BOOKING_CLASS bc ");
			sb.append("WHERE R.PNR = P.PNR AND R.PNR = S.PNR AND P.PNR_PAX_ID=PA.PNR_PAX_ID AND S.EXT_FLT_SEG_ID = FS.EXT_FLT_SEG_ID ");
			sb.append(" AND p.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id AND ppfs.booking_code <> 'OPENRTY' AND ppfs.booking_code = bc.booking_code ");
			sb.append("AND FS.EST_TIME_DEPARTURE_ZULU BETWEEN TO_DATE('" + strSearchFrom
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') ");
			sb.append("AND TO_DATE('" + strSearchTo + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");

			if (isNotEmptyOrNull(selPaxStatus) && selPaxStatus.equalsIgnoreCase("CNX")) {
				sb.append("  AND    P.STATUS IN ('" + selPaxStatus + "')");
			} else if (isNotEmptyOrNull(selPaxStatus) && selPaxStatus.equalsIgnoreCase("ALL")) {
				sb.append("  AND    P.STATUS IN ('OHD' , 'CNX')");
			}

			if (isNotEmptyOrNull(selPaxStatus) && !selPaxStatus.equalsIgnoreCase("OHD")) {
				sb.append("  AND R.pnr IN ");
				sb.append("  (SELECT x.pnr ");
				sb.append("  FROM t_reservation_audit x ");
				sb.append("  WHERE x.template_code = 'NEWONH') ");
			}

			if (isNotEmptyOrNull(selPfsStatus) && !selPfsStatus.equalsIgnoreCase("ALL")) {
				sb.append(" AND ppfs.pax_status IN ('" + selPfsStatus + "')");
			}

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" 	  AND    FS.FLIGHT_NUMBER  IN ('" + flightNumber + "')");

			}

			if (isNotEmptyOrNull(selDept)) {
				sb.append(" AND SUBSTR(TRIM(FS.SEGMENT_CODE),1,3) = '" + selDept + "' ");
			}

			if (isNotEmptyOrNull(selArrival)) {
				sb.append(" AND SUBSTR(TRIM(FS.SEGMENT_CODE),5) = '" + selArrival + "' ");
			}
			if (option == 1) {
				sb.append("	) ");
			}

		}
		if (option == 1) {
			sb.append("	) ORDER BY PNR_NUMBER ");
		}

		if (option == 2) {
			sb.append(" )x)y ");
		}
		return sb.toString();
	}

	/**
	 * Method to create the query for the Admin Audit Report.
	 * 
	 * @param reportsSearchCriteria
	 * @return String query
	 */
	public String getInterlineSalesReportQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String carrierCode = getSingleDataValue(reportsSearchCriteria.getCarrierCodes());

		StringBuilder sb = new StringBuilder();

		try {
			if ((reportsSearchCriteria.getDateRangeFrom() == null) || (reportsSearchCriteria.getDateRangeFrom() == "")
					|| (reportsSearchCriteria.getDateRangeTo() == null) || (reportsSearchCriteria.getDateRangeTo() == "")) {

				throw new ModuleException("reporting.date.invalid");
			}

		} catch (ModuleException ex) {
			log.error("Invalid date error" + ex.getMessageString());
		}

		sb.append("SELECT pnr,Origin_Agent,routing,Nett_Total,Nett_Total-op_carr_Collection mkt_carr_Collection,op_carr_Collection");
		sb.append("	FROM(select pnr,a.agent_name Origin_Agent,F_COMPANY_PYMT_REPORT(pnr)routing, nvl((select sum(amount) * -1");
		sb.append("	from t_pax_transaction where nominal_code in(28,18,17,16,19,15,29,26,24,23,25,22,6,5,30,31,32,33,48)and");
		sb.append("	pnr_pax_id in (select pnr_pax_id from t_pnr_passenger p where p.pnr=r.pnr)),0) Nett_Total,");
		sb.append("	nvl((select SUM(AMOUNT)");
		sb.append("	FROM t_pnr_pax_SEG_charges PPFS");
		sb.append("	WHERE PPFS.ppfs_id IN (SELECT PPFS_ID FROM t_pnr_pax_fare_segment");
		sb.append("	where PNR_SEG_ID IN (SELECT PNR_SEG_ID FROM T_PNR_SEGMENT");
		sb.append("	WHERE FLT_SEG_ID IN (SELECT FLT_SEG_ID FROM T_FLIGHT_SEGMENT");
		sb.append("	WHERE FLIGHT_ID IN (SELECT FLIGHT_ID FROM T_FLIGHT ");
		sb.append("	WHERE SUBSTR(FLIGHT_NUMBER,1,2)IN(SELECT carrier_code");
		sb.append("	FROM t_interlined_airlines WHERE carrier_code IN (" + carrierCode + ")))))) and");
		sb.append("	PFT_ID IN (SELECT PFT_ID from t_pnr_pax_ond_charges where ppf_id in");
		sb.append("	(select ppf_id from t_pnr_pax_fare where pnr_pax_id in");
		sb.append("	(select pnr_pax_id from t_pnr_passenger");
		sb.append("	where pnr =r.pnr)))),0) op_carr_Collection");
		sb.append("	from t_reservation r,t_agent a");
		sb.append("	where r.origin_agent_code=a.agent_code and");
		sb.append(ReportUtils.getReplaceStringForIN("r.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  and");
		sb.append("	r.booking_timestamp between to_date('" + fromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') and to_date('"
				+ toDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss'))");
		sb.append("	where NETT_TOTAL>0 ORDER BY 2,3,4");

		return sb.toString();
	}

	/**
	 * Gets the data for Agent Statement Detail Report
	 * 
	 * @since Aug 7, 2008
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getAgentStatementDetailDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		String strAgent = null;
		if (reportsSearchCriteria.getAgentCode() == null) {
			return null;
		} else {
			strAgent = reportsSearchCriteria.getAgentCode();
		}
		sb.append("SELECT AGENT_CODE, AGENT_NAME,");
		sb.append(" 100 OpeningReceivable,");
		sb.append(" 200 OpeningPayable,");
		sb.append(" 150 Invoice ,");
		sb.append(" 10  Cash,");
		sb.append(" 20  Cheque,");
		sb.append(" 0   Debit,");
		sb.append(" 0   Credit,");
		sb.append(" 0   Adjustments,");
		sb.append(" 70  Sales,");
		sb.append(" 50  ClosingPayable,");
		sb.append(" 160 ClosingReceivable");
		sb.append(" FROM T_AGENT");
		sb.append(" WHERE AGENT_CODE='" + strAgent + "'");

		return sb.toString();
	}

	/**
	 * Gets the data for Agent Statement Summary Report
	 * 
	 * @since Aug 7, 2008
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getAgentStatementSummaryDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT AGENT_CODE, AGENT_NAME,");

		sb.append(" 100 OpeningReceivable,");
		sb.append(" 200 OpeningPayable,");
		sb.append(" 150 Invoice ,");
		sb.append(" 10  Cash ,");
		sb.append(" 20  Cheque,");
		sb.append(" 0   Debit ,");
		sb.append(" 0   Credit ,");
		sb.append(" 0   Adjustments,");
		sb.append(" 70  Sales,");
		sb.append(" 50  ClosingPayable,");
		sb.append(" 160 ClosingReceivable");
		sb.append(" FROM T_AGENT");

		return sb.toString();
	}

	public String getEmailOutstandingBalanceDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());

		if ((reportsSearchCriteria.getDateRangeFrom() == null) && (reportsSearchCriteria.getDateRangeTo() == null)
				&& (strAgents == null)) {
			return null;
		}

		sb.append("SELECT i.AGENT_CODE, a.AGENT_NAME, i.invoice_number, i.invoice_date, i.billing_period, ");
		sb.append(" i.invoice_amount,i.settled_amount ");
		sb.append(" FROM t_invoice i, t_agent a ");
		sb.append(" WHERE i.agent_code = a.agent_code AND  ");
		sb.append(ReportUtils.getReplaceStringForIN("i.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
		sb.append(" AND (to_date('");
		sb.append(reportsSearchCriteria.getDateRangeFrom()
				+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
		sb.append("to_date('");
		sb.append(reportsSearchCriteria.getDateRangeTo()
				+ " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
		sb.append("(to_date('");
		sb.append(reportsSearchCriteria.getDateRangeFrom() + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
		sb.append("to_date('");
		sb.append(reportsSearchCriteria.getDateRangeTo() + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");
		if (reportsSearchCriteria.isExcludeZeroBalance()) {
			sb.append("	AND i.invoice_amount != 0 ");
		}

		sb.append("	ORDER BY i.AGENT_CODE");
		return sb.toString();
	}

	public String getInsurenceDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT res.pnr, policy_code, TO_CHAR(ins.sell_timestamp, 'DD-MM-YYYY') as booking_date, ");
		sb.append(" TO_CHAR(date_of_travel, 'DD-MM-YYYY') travel_date,TO_CHAR(date_of_return, 'DD-MM-YYYY') return_date,amount, tax_amount, co.country_name,TOT_PAX_COUNT as N_PAX, ");
		sb.append(" decode(state, 'SC', 'Success', 'FL', 'Failed', 'RQ', 'Unknown', 'TO','Unknown','CX','Canceled') status, NET_AMOUNT, user_id ");
		sb.append(" FROM t_pnr_insurance ins, t_reservation res, t_station s, t_country co, t_airport a ");
		sb.append(" WHERE ");
		// exclude removed passengers.
		sb.append(" NOT EXISTS (SELECT 'X' FROM t_reservation_audit au, t_reservation res1 WHERE au.pnr=res1.pnr AND au.template_code in ('NEWRMP') AND res.pnr=res1.pnr)");
		sb.append(" AND res.pnr = ins.pnr ");
		sb.append("	AND ins.sell_timestamp BETWEEN to_date('" + fromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date('"
				+ toDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss')");
		sb.append(" AND ins.origin= a.airport_code and a.station_code = s.station_code AND s.country_code=co.country_code AND ins.policy_code is not null ");
		sb.append(" order by res.booking_timestamp");

		return sb.toString();
	}

	public String getPrivilegeDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT DISTINCT P.PRIVILEGE, P.BEHAVIOR_DESCRIPTION, P.PRIVILEGE_CATEGORY_ID ");
		sb.append(" FROM T_PRIVILEGE P , T_ROLE_PRIVILEGE RP ");
		sb.append(" WHERE  P.PRIVILEGE_ID = RP.PRIVILEGE_ID ");

		if ((reportsSearchCriteria.getPrivileges() == null) && (reportsSearchCriteria.getRoles() != null)) {
			List<String> roleList = new ArrayList<String>(reportsSearchCriteria.getRoles());
			sb.append(" AND ( RP.ROLE_ID IN( " + Util.buildStringInClauseContent(roleList) + " )) ");
		}

		if ((reportsSearchCriteria.getRoles() != null) && (reportsSearchCriteria.getPrivileges() != null)) {
			List<String> privilegeList = new ArrayList<String>(reportsSearchCriteria.getPrivileges());
			sb.append(" AND ( ");

			while (privilegeList.size() > 100) {
				List<String> subList = privilegeList.subList(0, 100);
				privilegeList = privilegeList.subList(101, privilegeList.size());
				sb.append(" P.PRIVILEGE_ID IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
			}

			if (privilegeList.size() <= 100) {
				sb.append(" P.PRIVILEGE_ID IN( " + Util.buildStringInClauseContent(privilegeList) + " )) ");
			}
		}
		sb.append(" AND P.STATUS = 'ACT' ");
		sb.append(" ORDER BY P.PRIVILEGE_CATEGORY_ID ASC");

		return sb.toString();

	}

	public String getAgentWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) {
		// fromDate and toDate fields will be used in further development
		// String fromDate = reportsSearchCriteria.getDateRangeFrom();
		// String toDate = reportsSearchCriteria.getDateRangeTo();
		String country = reportsSearchCriteria.getCountryCode();
		String station = reportsSearchCriteria.getStation();

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT");
		sb.append(" A.AGENT_TYPE_CODE 			AGENT_TYPE,");
		sb.append(" C.COUNTRY_NAME				COUNTRY,");
		sb.append(" A.ADDRESS_STATE_PROVINCE	STATE,");
		sb.append(" A.STATION_CODE 				CITY,");
		sb.append(" A.ACCOUNT_CODE 				ACCOUNT_CODE,");
		sb.append(" A.STATUS 					STATUS,");
		sb.append(" A.CREDIT_LIMIT 				CREDIT_LIMIT,");
		sb.append(" NULL AS 					AMOUNT_DUE,");
		sb.append(" ASUMMARY.AVILABLE_CREDIT 	AVAILABLE_CREDIT,");
		sb.append(" A.GSA_AGENT AS				REPORTING_AGENT,");
		sb.append(" NULL AS 					ACTIVITY_START_DATE,");
		sb.append(" NULL AS 					ACTIVITY_END_DATE");
		sb.append(" FROM ((SELECT");
		sb.append(" Y.AGENT_CODE AS 			GSA_AGENT,");
		sb.append(" X.TERRITORY_CODE,");
		sb.append(" X.AGENT_CODE,");
		sb.append(" X.STATION_CODE,");
		sb.append(" X.CREDIT_LIMIT,");
		sb.append(" X.STATUS,");
		sb.append(" X.ACCOUNT_CODE,");
		sb.append(" X.ADDRESS_STATE_PROVINCE,");
		sb.append(" X.AGENT_TYPE_CODE");
		sb.append(" FROM T_AGENT X,");
		sb.append(" T_AGENT Y");
		sb.append(" WHERE X.TERRITORY_CODE	= Y.TERRITORY_CODE");
		sb.append(" AND Y.AGENT_TYPE_CODE  	= 'GSA' AND X.REPORT_TO_GSA = 'Y') ");
		sb.append(" UNION (SELECT null AS GSA_AGENT, X.TERRITORY_CODE, X.AGENT_CODE, X.STATION_CODE, ");
		sb.append(" X.CREDIT_LIMIT, X.STATUS, X.ACCOUNT_CODE, X.ADDRESS_STATE_PROVINCE, X.AGENT_TYPE_CODE ");
		sb.append(" FROM T_AGENT X, T_AGENT Y ");
		sb.append(" WHERE X.TERRITORY_CODE = Y.TERRITORY_CODE AND Y.AGENT_TYPE_CODE  = 'GSA' AND x.REPORT_TO_GSA='N')) A,");
		sb.append(" T_COUNTRY 				C,");
		sb.append(" T_STATION 				S,");
		sb.append(" T_AGENT_SUMMARY 		ASUMMARY");
		sb.append(" WHERE S.COUNTRY_CODE 	= C.COUNTRY_CODE");
		sb.append(" AND A.STATION_CODE   	= S.STATION_CODE");
		sb.append(" AND A.AGENT_CODE     	= ASUMMARY.AGENT_CODE");
		sb.append(" AND " + ReportUtils.getReplaceStringForIN("A.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
		if (!country.isEmpty()) {
			sb.append(" AND C.COUNTRY_NAME = '" + country + "'");
		}
		if (!station.isEmpty()) {
			sb.append("	AND A.station_code = '" + station + "'");
		}
		return sb.toString();
	}

	public String getTicketWiseForwardSalesReport(ReportsSearchCriteria reportsSearchCriteria) {
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String fromTktNumber = reportsSearchCriteria.getFromTktNumber();
		String toTktNumber = reportsSearchCriteria.getToTktNumber();
		String country = reportsSearchCriteria.getCountryCode();
		String station = reportsSearchCriteria.getStation();

		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT");
		sb.append(" 	(SELECT MAX(e_ticket_number)");
		sb.append(" 	FROM t_pax_e_ticket");
		sb.append(" 	WHERE pnr_pax_id = a.pnr_pax_id");
		sb.append(" 	) TKT_NO,");
		sb.append(" 	TKT_STATUS,");
		sb.append(" 	DATE_OF_ISSUE,");
		sb.append(" 	AGENT_TYPE,");
		sb.append(" 	AGENT_CODE,");
		sb.append(" 	AGENT_NAME,");
		sb.append(" 	ACCOUNT_CODE,");
		sb.append(" 	STATE,");
		sb.append(" 	CITY,");
		sb.append(" 	COUNTRY,");
		sb.append(" 	(SELECT PARAM_VALUE FROM T_APP_PARAMETER WHERE PARAM_KEY='AIRLINE_4'");
		sb.append(" 	)CURRENCY,");
		sb.append(" 	GROSS_FARE,");
		sb.append(" 	TAX,");
		sb.append(" 	PAX_TYPE,");
		sb.append(" 	PNR,");
		sb.append(" 	PASSENGERNAME,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'ROUTING')ROUTING,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'CARRIER')CARRIER,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'FLIGHT_NO')FLIGHT_NO,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'FLIGHT_DATE')FLIGHT_DATE,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'CABIN_CLASS')CABIN_CLASS,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'FARE_BASIS')FARE_BASIS,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'FLIGHT_TIME')FLIGHT_TIME,");
		sb.append(" 	F_GET_PNR_PAX_DETAILS(a.pnr,a.pnr_pax_id,'STATUS')STATUS");
		sb.append(" FROM");
		sb.append(" 	(SELECT DECODE(r.STATUS,'CNF','Confirmed','Cancelled')TKT_STATUS,");
		sb.append(" 	TO_CHAR(R.booking_timestamp,'dd-MON-YYYY') DATE_OF_ISSUE,");
		sb.append(" 	a.agent_type_code 			agent_type,");
		sb.append(" 	a.agent_code,");
		sb.append(" 	A.AGENT_NAME,");
		sb.append(" 	a.account_code,");
		sb.append(" 	A.address_state_province 	STATE,");
		sb.append(" 	A.station_code 				CITY,");
		sb.append(" 	C.country_name 				COUNTRY,");
		sb.append(" 	SUM(");
		sb.append(" 	CASE");
		sb.append(" 		WHEN PFT.charge_group_code='FAR'");
		sb.append(" 			THEN PFT.AMOUNT");
		sb.append(" 		ELSE 0");
		sb.append(" 	END)GROSS_FARE,");
		sb.append(" 	SUM(");
		sb.append(" 	CASE");
		sb.append(" 		WHEN PFT.charge_group_code='TAX'");
		sb.append(" 			THEN PFT.AMOUNT");
		sb.append(" 		ELSE 0");
		sb.append(" 	END)TAX,");
		sb.append(" 	PT.description 				PAX_TYPE,");
		sb.append(" 	R.PNR,");
		sb.append(" 	P.title");
		sb.append(" 	|| ' '");
		sb.append(" 	|| P.first_name");
		sb.append(" 	|| ' '");
		sb.append(" 	|| P.last_name 				PASSENGERNAME,");
		sb.append(" 	P.pnr_pax_id");
		sb.append(" 	FROM T_RESERVATION 		R,");
		sb.append(" 	T_PNR_PASSENGER 		P,");
		sb.append(" 	T_PNR_PAX_FARE 			PPF,");
		sb.append(" 	T_PNR_PAX_OND_CHARGES 	PFT,");
		sb.append(" 	T_AGENT 				A,");
		sb.append(" 	T_PAX_TYPE 				PT,");
		sb.append(" 	T_CHARGE_RATE 			CR,");
		sb.append(" 	T_COUNTRY 				C,");
		sb.append(" 	T_STATION 				S,");
		sb.append("		t_pax_e_ticket 			E");
		sb.append(" WHERE R.pnr = P.pnr");
		sb.append(" 	AND P.pnr_pax_id = PPF.pnr_pax_id");
		sb.append("		AND DECODE(P.PAX_TYPE_CODE,'IN',P.ADULT_ID,P.PNR_PAX_ID) IN");
		sb.append(" 	(SELECT PNR_PAX_ID");
		sb.append(" 	FROM T_PAX_TRANSACTION");
		sb.append(" 	WHERE NOMINAL_CODE IN(28,18,17,16,19,15,6,30,32,34,35,36,38,48)");
		sb.append(" 	)");
		sb.append(" 	AND booking_timestamp BETWEEN to_date('" + fromDate + " 00:00:00','dd-MON-YYYY hh24:mi:ss') and to_date('"
				+ toDate + " 23:59:59','dd-MON-YYYY hh24:mi:ss')");
		sb.append(" 	AND PPF.ppf_id 		   = PFT.ppf_id");
		sb.append(" 	AND PT.pax_type_code   = P.pax_type_code");
		sb.append(" 	AND R.origin_agent_code= A.agent_code");
		sb.append(" 	AND A.STATION_CODE     = S.STATION_CODE");
		sb.append(" 	AND S.COUNTRY_CODE     = C.COUNTRY_CODE");
		sb.append(" 	AND PFT.charge_rate_id = CR.charge_rate_id(+)");
		sb.append("		AND E.pnr_pax_id 	   = p.pnr_pax_id");
		if (!fromTktNumber.isEmpty() && !toTktNumber.isEmpty()) {
			sb.append("	AND E.e_ticket_number BETWEEN '" + fromTktNumber + "' AND '" + toTktNumber + "'");
		}
		if (!country.isEmpty()) {
			sb.append(" AND C.COUNTRY_NAME = '" + country + "'");
		}
		if (!station.isEmpty()) {
			sb.append("	AND A.station_code = '" + station + "'");
		}
		sb.append(" 	GROUP BY DECODE(r.STATUS,'CNF','Confirmed','Cancelled'),");
		sb.append(" 	TO_CHAR(R.booking_timestamp,'dd-MON-YYYY'),");
		sb.append(" 	a.agent_type_code,");
		sb.append(" 	a.agent_code,");
		sb.append(" 	A.AGENT_NAME,");
		sb.append(" 	a.account_code,");
		sb.append(" 	A.address_state_province,");
		sb.append(" 	A.station_code ,");
		sb.append(" 	C.country_name ,");
		sb.append(" 	PT.description ,");
		sb.append(" 	R.PNR,");
		sb.append(" 	P.title");
		sb.append(" 	|| ' '");
		sb.append(" 	|| P.first_name");
		sb.append(" 	|| ' '");
		sb.append(" 	|| P.last_name,");
		sb.append(" 	P.pnr_pax_id");
		sb.append(" 	) A");

		return sb.toString();
	}

	public String getAgentsSalesReport(ReportsSearchCriteria reportsSearchCriteria) {

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		StringBuilder sb = new StringBuilder();
		sb.append(" select substr(routing,13,3)origin ,RES_NO,ROUTING,BOOKED_AGENCY,BOOKED_STAFF_NAME,payments,fare,");
		sb.append(" agent_code,agent_type,TOTAL_FARE,TOTAL_TAX,TOTAL_SURCHARGES,fees,discount_amount,PAX_TYPE_CODE,e_ticket_number,"
				+ "e_ticket_status,issie_date,issue_agent,p_name ,");
		sb.append(" total_pax_count,");
		sb.append(" (select sum(amount) ");
		sb.append(" FROM t_pnr_pax_ond_charges pft,t_pnr_pax_fare ppf,t_pnr_passenger pp ");
		sb.append(" WHERE pft.ppf_id=ppf.ppf_id and ppf.pnr_pax_id=pp.pnr_pax_id and pp.pnr =res_no) total_price, ");
		sb.append(" (SELECT nvl(sum(amount),0) ");
		sb.append(" FROM t_pnr_pax_ond_charges pft,t_pnr_pax_fare ppf,t_pnr_passenger pp,t_charge_rate cr ");
		sb.append(" WHERE pft.ppf_id=ppf.ppf_id and ppf.pnr_pax_id=pp.pnr_pax_id ");
		sb.append(" and cr.charge_rate_id=pft.charge_rate_id and cr.charge_code='HC' ");
		sb.append(" and pp.pnr =res_no ) handling_fee ");
		sb.append(" from");
		sb.append(" (");
		sb.append(" SELECT R.PNR RES_NO,");
		sb.append(" F_CONCATENATE_LIST(cursor  (select TO_CHAR(trunc(EST_TIME_DEPARTURE_ZULU),'DD-Mon-rrrr')||' '||segment_code|| ' '||f.flight_number|| ' '||ps.status routing");
		sb.append("                      from t_flight_segment fs,t_pnr_segment ps,t_flight f");
		sb.append("                      where  f.flight_id=fs.flight_id    and");
		sb.append("                             ps.flt_seg_id=fs.flt_seg_id and");
		sb.append("                             PS.pnr=r.pnr AND PS.status='CNF'");
		sb.append("                              order by EST_TIME_DEPARTURE_ZULU),'; ')ROUTING,");
		sb.append(" A.agent_name BOOKED_AGENCY,U.display_name BOOKED_STAFF_NAME,");
		sb.append(" F_CONCATENATE_LIST(cursor(SELECT DISTINCT REPLACE(INITCAP(LOWER(N.description)),'_',' ')||'/'||A.agent_name");
		sb.append("                   FROM t_pax_transaction T,T_PAX_TRNX_NOMINAL_CODE N,T_AGENT A");
		sb.append("                   WHERE T.agent_code=A.agent_code AND T.nominal_code=N.nominal_code AND");
		sb.append("                         PNR_PAX_ID IN(SELECT PNR_PAX_ID FROM T_PNR_PASSENGER WHERE PNR=r.pnr)AND");
		sb.append("                          N.nominal_code in(28,18,17,16,19,15,29,26,24,23,25,22,6,5,30,31,32,33))) payments, R.total_fare FARE,");
		sb.append(" (TOTAL_PAX_COUNT+TOTAL_PAX_CHILD_COUNT)total_pax_count,");
		sb.append(" A.agent_code agent_code,A.agent_type_code agent_type,PP.TOTAL_FARE TOTAL_FARE,PP.TOTAL_TAX TOTAL_TAX,"
				+ "   PP.TOTAL_SURCHARGES TOTAL_SURCHARGES,(PP.TOTAL_CNX + PP.TOTAL_MOD + PP.TOTAL_ADJ) fees, (-1 * pp.total_discount) AS discount_amount, "
				+ "   PP.PAX_TYPE_CODE PAX_TYPE_CODE,");
		// + "   NVL (F_GET_ETICKET_STRING(PP.PNR_PAX_ID),'-') e_ticket_number,");
		sb.append(" NVL(F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pax_e_ticket pet "
				+ "WHERE PP.pnr_pax_id = pet.pnr_pax_id ),'-'),'-') e_ticket_number,  ");

		sb.append("   PP.STATUS e_ticket_status,R.BOOKING_TIMESTAMP issie_date, R.ORIGIN_AGENT_CODE issue_agent,"
				+ "   (PP.TITLE || ' ' || PP.FIRST_NAME || ' ' || PP.LAST_NAME) p_name");

		sb.append(" FROM T_RESERVATION R,T_AGENT A,T_USER U,T_PNR_PASSENGER PP ");
		sb.append(" WHERE R.origin_agent_code=A.agent_code and");
		sb.append("       r.origin_user_id=u.user_id  and pp.pnr         =r.pnr ");
		sb.append(" and r.status='CNF' ");
		sb.append(" and r.booking_timestamp between to_date('" + fromDate + " 00:00:00','dd-MON-YYYY hh24:mi:ss') and to_date('"
				+ toDate + " 23:59:59','dd-MON-YYYY hh24:mi:ss')  and   ");
		sb.append(ReportUtils.getReplaceStringForIN("r.origin_agent_code", reportsSearchCriteria.getAgents(), false) + "  and  ");
		sb.append(" exists (select 'x' from t_pnr_passenger p,t_pax_transaction t");
		sb.append("           where p.pnr_pax_id=t.pnr_pax_id and p.pnr=r.pnr group by p.pnr having sum(t.amount)<=0)");
		sb.append(" ) order by 1,2,3");

		return sb.toString();
	}

	public String getAgentsSalesPerformanceReport(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		String fromdate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String saleAmount = reportsSearchCriteria.getSalesAmount();

		sb.append(" SELECT ag.agent_code,ag.agent_name,0-SUM(pt.amount) totalAmount ");
		sb.append(" FROM t_pax_transaction pt,t_agent ag ");
		sb.append(" WHERE pt.agent_code  = ag.agent_code ");
		sb.append(" AND pt.nominal_code IN(28,18,17,16,19,15,29,26,24,23,25,22,6,5,30,31,32,33,48) ");
		sb.append(" AND pt.tnx_date BETWEEN to_date('" + fromdate + " 00:00:00','dd-MON-YYYY hh24:mi:ss') ");
		sb.append(" AND to_date('" + toDate + " 23:59:59','dd-MON-YYYY hh24:mi:ss') ");
		sb.append(" GROUP BY ag.agent_code,ag.agent_name ");
		sb.append(" HAVING (0-SUM(pt.amount)) <= '" + saleAmount + "' ");
		sb.append(" ORDER BY totalAmount DESC ");

		return sb.toString();
	}
	
	public String getFlownPassengerDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT TO_CHAR(res.BOOKING_TIMESTAMP,'yyyy/mm/dd')      AS BOOKING_DATE,");
		sb.append(" f.FLIGHT_NUMBER                                         AS FLIGHT_NUMBER, ");
		sb.append(" res.PNR                                                 AS PNR,");
		sb.append(" ppfs.BOOKING_CODE                                       AS RBD,");
		sb.append(" ag.AGENT_NAME                                           AS AGENT_ISSUED_TKT,");
		sb.append(" pp.TITLE                                                AS TITLE,");
		sb.append(" pp.FIRST_NAME                                           AS FIRST_NAME,");
		sb.append(" pp.LAST_NAME                                            AS LAST_NAME,");
		sb.append(" nat.DESCRIPTION                                         AS NATIONALITY,");
		sb.append(" ppfs.PAX_STATUS                                         AS PAX_STATUS,");
		sb.append(" CONCAT(CONCAT(CONCAT(contact.C_TITLE , ' '), CONCAT(contact.C_FIRST_NAME,' ')),contact.C_LAST_NAME) AS PAX_CONTACT, ");
		sb.append(" (SELECT paxSsr.SSR_TEXT FROM T_PNR_PAX_SEGMENT_SSR paxSsr ,T_SSR_INFO ssr ");
		sb.append(" WHERE ssr.SSR_ID   = paxSsr.SSR_ID AND paxSsr.PPFS_ID = ppfs.PPFS_ID ");
		sb.append(" AND ssr.SSR_CODE   = 'FQTV' )                            AS FFP_NO, ");
		sb.append(" contact.E_EMAIL                                    AS EMAIL, ");
		sb.append(" f.STATUS                                           AS FLIGHT_STATUS, ");
		sb.append(" addInfo.NATIONALID_NO                              AS NATIONALID_NO, ");
		sb.append(" TO_CHAR(pp.DATE_OF_BIRTH, 'yyyy/mm/dd')            AS DOB, ");
		sb.append(" addInfo.PASSPORT_NUMBER                            AS PASSPORT_NUMBER, ");
		sb.append(" fs.SEGMENT_CODE                                    AS SECTOR, ");
		sb.append(" TO_CHAR(fs.EST_TIME_DEPARTURE_LOCAL, 'yyyy/mm/dd') AS DEPARTURE_DATE, ");
		sb.append(" TO_CHAR(fs.EST_TIME_DEPARTURE_LOCAL, 'HH24:MI:SS')   AS DEPARTURE_TIME, ");
		sb.append(" et.E_TICKET_NUMBER                                 AS TKT_NO, ");
		sb.append(" eticket.COUPON_NUMBER                              AS TKT_COUPON_NO, ");
		sb.append(" eticket.STATUS                                     AS TKT_STATUS ");
		sb.append(" FROM T_RESERVATION res, T_PNR_PASSENGER pp, T_PNR_PAX_FARE ppf, T_PNR_PAX_FARE_SEGMENT ppfs, ");
		sb.append(" T_PNR_SEGMENT ps , T_FLIGHT f , T_FLIGHT_SEGMENT fs, T_AGENT ag, T_NATIONALITY nat, T_RESERVATION_CONTACT contact, ");
		sb.append(" T_PNR_PAX_ADDITIONAL_INFO addInfo, T_PAX_E_TICKET et, T_PNR_PAX_FARE_SEG_E_TICKET eticket ");
		sb.append(" WHERE fs.EST_TIME_DEPARTURE_LOCAL BETWEEN TO_DATE('" + fromDate + "', 'dd-mon-yyyy hh24:mi:ss') ");
		sb.append(" AND TO_DATE('" + toDate + "' , 'dd-mon-yyyy hh24:mi:ss') ");

		sb.append(" AND f.FLIGHT_ID = fs.FLIGHT_ID ");
		if (flightNumber != null) {
			sb.append("AND f.FLIGHT_NUMBER LIKE '%" + flightNumber + "%' ");
		}
		sb.append(" AND fs.FLT_SEG_ID           = ps.FLT_SEG_ID ");
		sb.append(" AND ps.PNR                  = pp.PNR ");
		sb.append(" AND pp.PNR                  = res.PNR ");
		sb.append(" AND pp.PNR_PAX_ID           = ppf.PNR_PAX_ID ");
		sb.append(" AND ppf.PPF_ID              = ppfs.PPF_ID ");
		sb.append(" AND ppfs.PNR_SEG_ID 		= ps.PNR_SEG_ID ");
		sb.append(" AND ag.AGENT_CODE           = res.ORIGIN_AGENT_CODE ");
		sb.append(" AND nat.NATIONALITY_CODE    = pp.NATIONALITY_CODE ");
		sb.append(" AND contact.PNR             = res.PNR ");
		sb.append(" AND addInfo.PNR_PAX_ID      = pp.PNR_PAX_ID ");
		sb.append(" AND ps.STATUS 				= 'CNF' ");
		if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.PFS) {
			sb.append(" AND (ppfs.PAX_STATUS    = '" + ReservationInternalConstants.PaxFareSegmentTypes.FLOWN + "' OR ");
			sb.append(" ppfs.PAX_STATUS     = '" + ReservationInternalConstants.PaxFareSegmentTypes.NO_REC + "')");
		} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.ETICKET) {
			sb.append(" AND eticket.STATUS      = '" + EticketStatus.FLOWN.code() + "' ");
		} else if (AppSysParamsUtil.getFlownCalculateMethod() == FLOWN_FROM.DATE) {
			sb.append(" AND fs.EST_TIME_DEPARTURE_LOCAL < sysdate ");
		}

		sb.append(" AND eticket.PPFS_ID         = ppfs.PPFS_ID ");
		sb.append(" AND et.PAX_E_TICKET_ID = eticket.PAX_E_TICKET_ID ");
		sb.append(" AND ppfs.BOOKING_CODE NOT     IN ('OPENRTY', 'OPENRTC') ");
		sb.append(" AND ps.OPEN_RT_CONFIRM_BEFORE IS NULL ");
		sb.append(" AND eticket.PAX_E_TICKET_ID = (SELECT MAX(e.PAX_E_TICKET_ID) FROM T_PNR_PAX_FARE_SEG_E_TICKET e ");
		sb.append(" WHERE e.PPFS_ID = ppfs.PPFS_ID ) ORDER BY fs.EST_TIME_DEPARTURE_LOCAL, f.FLIGHT_NUMBER");

		return sb.toString();
	}

	public String getFlownPassengerFeeDataQuery(ReportsSearchCriteria reportsSearchCriteria, String viewMode) {
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String bookFromDate = reportsSearchCriteria.getDateRange2From();
		String bookToDate = reportsSearchCriteria.getDateRange2To();
		String paxSegStatus = reportsSearchCriteria.getStatus();
		String interlineCode = reportsSearchCriteria.getInterlineCode();
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		ArrayList<String> segCodes = (ArrayList<String>) reportsSearchCriteria.getSegmentCodes();

		StringBuilder sb = new StringBuilder();
		if (("VIEW").equals(viewMode)) {
			sb.append(" SELECT   a.e_ticket_number 	AS eticket_number,");
			sb.append(" pax.pnr 					AS pnr,");
			sb.append(" pax.pax_type_code 			AS pax_type,");
			sb.append(" LTRIM(pax.title");
			sb.append(" || ' '");
			sb.append(" || pax.first_name");
			sb.append(" || ' '");
			sb.append(" || pax.last_name) 												AS pax_name,");
			sb.append(" frule.fare_basis_code 											AS fare_basis,");
			sb.append(" flight.flight_number 											AS flight_number,");
			sb.append(" TO_CHAR (fseg.est_time_departure_local,'DD-MON-YYYY HH24:MI')	AS flight_date,");
			sb.append(" '" + interlineCode + "' AS interline,");
			sb.append(" ''																AS discount,");
			sb.append(" CASE WHEN pax.pax_type_code = 'IN' THEN f_get_infant_cabin_class_code (pax.pnr_pax_id, fsegment.pnr_seg_id, 'CC' ) ELSE bcls.cabin_class_code END AS cabin_class_code,");
			sb.append(" pax.pnr_pax_id 													AS pax_id,");
			sb.append(" (select ta.agent_name from t_agent ta where ta.agent_code = r.origin_agent_code) AS agent_name ,");
			sb.append("CASE WHEN pax.pax_type_code = 'IN' THEN f_get_infant_cabin_class_code (pax.pnr_pax_id, fsegment.pnr_seg_id, 'BC' ) ELSE bcls.booking_code END AS booking_code ");

			sb.append("FROM t_pnr_passenger pax,"
					+ "(SELECT pf.pnr_pax_id, pf.ppf_id, pf.fare_id FROM t_pnr_pax_fare pf) pfare,"
					+ "t_pnr_pax_fare_segment fsegment,"
					+ "(SELECT ps.flt_seg_id, ps.status, ps.pnr_seg_id FROM t_pnr_segment ps) pseg,"
					+ "(SELECT fs1.est_time_departure_local, fs1.flt_seg_id, fs1.flight_id,fs1.segment_code FROM t_flight_segment fs1) fseg,"
					+ "(SELECT f.flight_id, f.flight_number FROM t_flight f) flight,"
					+ "(SELECT e.pax_e_ticket_id, e.e_ticket_number FROM t_pax_e_ticket e) a,"
					+ "(SELECT se.ppfs_id, se.pax_e_ticket_id FROM t_pnr_pax_fare_seg_e_ticket se) b,"
					+ "(SELECT of1.fare_rule_id, of1.fare_id FROM t_ond_fare of1) ondfare,"
					+ "(SELECT fr.fare_basis_code, fr.fare_rule_id FROM t_fare_rule fr) frule,"
					+ "(SELECT bc.cabin_class_code, bc.booking_code FROM t_booking_class bc) bcls,"
					+ "(SELECT tr.origin_agent_code, tr.pnr,tr.booking_timestamp FROM t_reservation tr) r");
			sb.append(" WHERE pax.pnr_pax_id 		=  pfare.pnr_pax_id");
			sb.append(" AND pfare.ppf_id 			=  fsegment.ppf_id");
			sb.append(" AND fsegment.pnr_seg_id 	=  pseg.pnr_seg_id");
			sb.append(" AND pseg.flt_seg_id 		=  fseg.flt_seg_id");
			sb.append(" AND fseg.flight_id 			=  flight.flight_id");
			sb.append(" AND a.pax_e_ticket_id 		=  b.pax_e_ticket_id");
			sb.append(" AND b.ppfs_id 				=  fsegment.ppfs_id");
			sb.append(" AND ondfare.fare_rule_id 	=  frule.fare_rule_id");
			sb.append(" AND pfare.fare_id 			=  ondfare.fare_id");
			sb.append(" AND fsegment.booking_code 	=  bcls.booking_code(+)");
			sb.append(" AND pseg.status           	=  '" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "'");
			sb.append(" AND PAX.status            	=  '" + ReservationInternalConstants.ReservationPaxStatus.CONFIRMED + "'");
			sb.append(" AND pax.pnr = r.pnr ");
			sb.append(" AND est_time_departure_local between to_date('" + fromDate + "','dd-MON-YYYY hh24:mi:ss') and to_date('"
					+ toDate + "','dd-MON-YYYY hh24:mi:ss')");

			if (isNotEmptyOrNull(bookFromDate) && isNotEmptyOrNull(bookToDate)) {
				sb.append(" AND r.booking_timestamp between to_date('" + bookFromDate
						+ "','dd-MON-YYYY hh24:mi:ss') and to_date('" + bookToDate + "','dd-MON-YYYY hh24:mi:ss')");
			}

			if (isNotEmptyOrNull(paxSegStatus) && !paxSegStatus.equalsIgnoreCase("All")) {
				if (paxSegStatus.equals("F")) {
					sb.append(" AND fsegment.pax_status = 'F' ");
				} else {
					sb.append(" AND fsegment.pax_status <> 'F' ");
				}
			}

			/*
			 * If agent list is empty, add the clause to view reservations from IBE. IBE reservations have null values
			 * for agent code. Can also be done with ORIGIN_CHANNEL_CODE but using the is null was easier.
			 */
			if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) {
				sb.append(" AND "
						+ ReportUtils.getReplaceStringForIN(" r.origin_agent_code", reportsSearchCriteria.getAgents(), false));
			} else {
				sb.append(" AND r.origin_agent_code IS NULL");
			}

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" AND  flight.flight_number='" + flightNumber + "' ");
			}

			if (segCodes != null && !segCodes.isEmpty()) {
				sb.append(" AND fseg.segment_code IN ( ");
				sb.append(Util.buildStringInClauseContent(segCodes));
				sb.append(") ");
			}

			sb.append(" ORDER BY est_time_departure_local,PNR,ETICKET_NUMBER ");
		} else if (("VIEWSUMMARY").equals(viewMode)) {
			sb.append(" SELECT   pax_type, flight_number, flight_date, booking_code,COUNT (*) AS COUNT ");
			sb.append(" FROM (SELECT pax.pax_type_code AS pax_type, flight.flight_number, ");
			sb.append(" TO_CHAR (fseg.est_time_departure_local,'DD-MON-YYYY') AS flight_date, ");
			sb.append("	CASE ");
			sb.append("	WHEN pax.pax_type_code = 'IN' ");
			sb.append("    	THEN f_get_infant_cabin_class_code ");
			sb.append(" 		(pax.pnr_pax_id,fsegment.pnr_seg_id,'BC') ");
			sb.append("  	ELSE bcls.booking_code ");
			sb.append("		END AS booking_code ");
			sb.append(" FROM t_pnr_passenger pax, ");
			sb.append(" (SELECT pf.pnr_pax_id, pf.ppf_id, pf.fare_id FROM t_pnr_pax_fare pf) pfare,t_pnr_pax_fare_segment fsegment, ");
			sb.append(" (SELECT ps.flt_seg_id, ps.status, ps.pnr_seg_id FROM t_pnr_segment ps) pseg, ");
			sb.append(" (SELECT fs1.est_time_departure_local, fs1.flt_seg_id,fs1.flight_id, fs1.segment_code FROM t_flight_segment fs1) fseg, ");
			sb.append(" (SELECT f.flight_id, f.flight_number FROM t_flight f) flight, ");
			sb.append(" (SELECT of1.fare_rule_id, of1.fare_id FROM t_ond_fare of1) ondfare, ");
			sb.append(" (SELECT bc.cabin_class_code, bc.booking_code FROM t_booking_class bc) bcls,");
			sb.append(" (SELECT tr.origin_agent_code, tr.pnr, tr.booking_timestamp FROM t_reservation tr) r ");
			sb.append(" WHERE pax.pnr_pax_id = pfare.pnr_pax_id AND pfare.ppf_id = fsegment.ppf_id AND fsegment.pnr_seg_id = pseg.pnr_seg_id");
			sb.append(" AND pseg.flt_seg_id = fseg.flt_seg_id AND fseg.flight_id = flight.flight_id AND pfare.fare_id = ondfare.fare_id ");
			sb.append(" AND fsegment.booking_code = bcls.booking_code(+) AND pseg.status = '"
					+ ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' ");
			sb.append(" AND pax.status = '" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED
					+ "' AND pax.pnr = r.pnr ");
			sb.append(" AND est_time_departure_local BETWEEN TO_DATE ('" + fromDate + "','dd-MON-YYYY hh24:mi:ss') ");
			sb.append(" AND TO_DATE ('" + toDate + "','dd-MON-YYYY hh24:mi:ss')");
			if (isNotEmptyOrNull(bookFromDate) && isNotEmptyOrNull(bookToDate)) {
				sb.append(" AND r.booking_timestamp between to_date('" + bookFromDate
						+ "','dd-MON-YYYY hh24:mi:ss') and to_date('" + bookToDate + "','dd-MON-YYYY hh24:mi:ss')");
			}

			if (isNotEmptyOrNull(paxSegStatus) && !paxSegStatus.equalsIgnoreCase("All")) {
				if (paxSegStatus.equals("F")) {
					sb.append(" AND fsegment.pax_status = 'F' ");
				} else {
					sb.append(" AND fsegment.pax_status <> 'F' ");
				}
			}

			sb.append(" AND "
					+ ReportUtils.getReplaceStringForIN(" r.origin_agent_code", reportsSearchCriteria.getAgents(), false));

			if (isNotEmptyOrNull(flightNumber)) {
				sb.append(" AND flight.flight_number='" + flightNumber + "' ");
			}

			if (segCodes != null && !segCodes.isEmpty()) {
				sb.append(" AND fseg.segment_code IN ( ");
				sb.append(Util.buildStringInClauseContent(segCodes));
				sb.append(") ");
			}
			sb.append(" ) GROUP BY pax_type, flight_number, flight_date, booking_code ");
			sb.append(" ORDER BY flight_date,flight_number,pax_type ");
		}
		return sb.toString();
	}

	public String getBCReporttQuery(ReportsSearchCriteria reportsSearchCriteria) {

		log.debug("inside getBCReporttQuery");
		String strBcCode = getSingleDataValue(reportsSearchCriteria.getBookingCodes());
		String strCos = reportsSearchCriteria.getCos();
		String strLogicalCC = reportsSearchCriteria.getLogicalCC();
		String strBcCat = getSingleDataValue(reportsSearchCriteria.getBcCategory());
		String strBcType = reportsSearchCriteria.getBcType();
		String strAllocationType = reportsSearchCriteria.getAllocationType();
		String strStatus = reportsSearchCriteria.getStatus();
		String[] bcCatArray = null;
		boolean fixed = false;
		boolean standard = false;
		boolean nonStandard = false;

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT BC.booking_code,BC.booking_code_description,CC.cabin_class_code,BC.standard_code,BC.nest_rank,BC.fixed_flag, BC.status,BC.allocation_type,BC.bc_type, ");
		sb.append("lcc.LOGICAL_CABIN_CLASS_CODE, ");
		sb.append("NVL((SELECT 'Y' ");
		sb.append("FROM T_BC_CHARGE_GROUP BCG ");
		sb.append("WHERE BCG.booking_code=BC.booking_code AND charge_group_code='TAX'),'N') TAXES, ");
		sb.append("NVL((SELECT 'Y' ");
		sb.append("FROM T_BC_CHARGE_GROUP BCG ");
		sb.append("WHERE BCG.booking_code=BC.booking_code AND charge_group_code='SUR'),'N') SURCHARGE , ");
		sb.append("BC.onhold_flag ");
		sb.append("FROM T_BOOKING_CLASS BC, ");
		sb.append("T_LOGICAL_CABIN_CLASS LCC, ");
		sb.append("T_CABIN_CLASS CC ");
		sb.append("WHERE BC.cabin_class_code = CC.cabin_class_code ");
		sb.append("AND lcc.logical_cabin_class_code = bc.logical_cabin_class_code ");
		if (isNotEmptyOrNull(strBcCode)) {
			sb.append("AND "); 
			sb.append(ReportUtils.getReplaceStringForIN("BC.booking_code", reportsSearchCriteria.getBookingCodes(), false) + " ");
		}
		if (isNotEmptyOrNull(strCos)) {
			sb.append("and CC.cabin_class_code = '" + strCos + "' ");
		}
		if (isNotEmptyOrNull(strLogicalCC)) {
			sb.append("and LCC.logical_Cabin_Class_Code = '" + strLogicalCC + "' ");
		}
		if (isNotEmptyOrNull(strBcType)) {
			sb.append("and BC.bc_type = '" + strBcType + "' ");
		}
		if (isNotEmptyOrNull(strAllocationType)) {
			sb.append("and BC.allocation_type = '" + strAllocationType + "' ");
		}
		if (isNotEmptyOrNull(strBcCat)) {
			bcCatArray = strBcCat.split(",");
			if (bcCatArray.length < 2) {
				if (strBcCat.equals("'Fixed'")) {
					sb.append("and BC.fixed_flag = 'Y'");
				} else {
					sb.append("and BC.fixed_flag = 'N'");
				}
				if (strBcCat.equals("'Standard'")) {
					sb.append("and BC.standard_code = 'Y' ");
				} else if (strBcCat.equals("'Non-Standard'")) {
					sb.append("and BC.standard_code = 'N' ");
				}
			} else if (bcCatArray.length != 3) {

				for (String element : bcCatArray) {
					if (element.equals("'Fixed'")) {
						sb.append("and BC.fixed_flag = 'Y'");
						fixed = true;
					} else if (!fixed && !standard && !nonStandard) {
						sb.append("and BC.fixed_flag = 'N'");
					}
					if (nonStandard && !fixed && element.equals("'Standard'")) {
						sb.append("or BC.standard_code = 'Y' ");
						standard = true;
					} else if (!nonStandard && element.equals("'Standard'")) {
						sb.append("and BC.standard_code = 'Y' ");
						standard = true;
					} else if (nonStandard && fixed && element.equals("'Standard'")) {
						sb.append("or BC.standard_code = 'Y') ");
						standard = true;
					}
					if (standard && element.equals("'Non-Standard'")) {
						sb.append("or BC.standard_code = 'N' ");
						nonStandard = true;
					} else if (!standard && !fixed && element.equals("'Non-Standard'")) {
						sb.append("and BC.standard_code = 'N' ");
						nonStandard = true;
					} else if (!standard && fixed && element.equals("'Non-Standard'") && bcCatArray.length < 3) {
						sb.append("and BC.standard_code = 'N' ");
						nonStandard = true;
					} else if (!standard && fixed && element.equals("'Non-Standard'") && bcCatArray.length > 2) {
						sb.append("and (BC.standard_code = 'N' ");
						nonStandard = true;
					}
				}
			}

		}
		if (isNotEmptyOrNull(strStatus)) {
			sb.append("and BC.status = '" + strStatus + "' ");
		}
		sb.append("ORDER BY BC.booking_code");
		return sb.toString();
	}

	public String getInvoiceSettlemenyHistoryDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String reportType = reportsSearchCriteria.getReportType();

		if (reportType.equals("SETTLEMENT")) {
			sb.append("SELECT a.settllement_id, a.invoice_number, a.settlement_date,");
			sb.append("	a.amount_paid, a.reason,");
			sb.append("	a.user_id, b.agent_code, ag.agent_name , e.ENTITY_NAME");
			sb.append("	FROM t_invoice_settlement a, t_agent ag, t_invoice b, T_ENTITY e");
			sb.append("	WHERE a.invoice_number =b.invoice_number");
			sb.append("	and b.ENTITY_CODE = e.ENTITY_CODE and ");
			sb.append(" ag.agent_code=b.agent_code and ");
			sb.append(ReportUtils.getReplaceStringForIN("b.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
			// sb.append(" b.agent_code IN("+singleDataValue+")");
			sb.append(" and a.settlement_date between to_date('" + fromDate + " 00:00:00','dd-MON-YYYY hh24:mi:ss') ");
			sb.append(" and to_date('" + toDate + " 23:59:59','dd-MON-YYYY hh24:mi:ss')");
			sb.append(" AND e.ENTITY_CODE = '" + reportsSearchCriteria.getEntity() + "'");
			sb.append(" order by b.agent_code, a.invoice_number, a.settllement_id ");

		} else if (reportType.equals("INVOICE")) {
			sb.append("SELECT a.invoice_number, a.invoice_date, a.agent_code,");
			sb.append(" a.invioce_period_from, a.invoice_period_to, a.invoice_amount, ");
			sb.append(" a.settled_amount, a.status, a.billing_period , ag.agent_name ,e.ENTITY_NAME ");
			sb.append(" FROM t_invoice a, t_agent ag, T_ENTITY e");
			sb.append(" WHERE a.agent_code = ag.agent_code ");
			sb.append(" AND a.ENTITY_CODE = e.ENTITY_CODE and ");
			sb.append(ReportUtils.getReplaceStringForIN(" a.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
			// sb.append(" and a.agent_code IN("+singleDataValue+")");
			sb.append(" AND (to_date('");
			sb.append(fromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("to_date('");
			sb.append(toDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') between invioce_period_from and invoice_period_to OR ");
			sb.append("(to_date('");
			sb.append(fromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<invioce_period_from and ");
			sb.append("to_date('");
			sb.append(toDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>invoice_period_to)) ");
			sb.append(" AND e.ENTITY_CODE = '" + reportsSearchCriteria.getEntity() + "'");
			sb.append(" order by a.agent_code, a.invoice_number");
		}

		return sb.toString();
	}

	public String getNameChangeDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String flightNo = reportsSearchCriteria.getFlightNumber();
		String strSectorFrom = reportsSearchCriteria.getSectorFrom();
		String strSectorTo = reportsSearchCriteria.getSectorTo();
		String userId = reportsSearchCriteria.getUserId();
		String strDepFromdate = reportsSearchCriteria.getDepartureDateRangeFrom();
		String strDepToDate = reportsSearchCriteria.getDepartureDateRangeTo();

		sb.append(" SELECT DISTINCT TIMESTAMP, user_id, ");
		sb.append("	content, user_notes, pnr, BOOKINGDATE,");
		sb.append(" substr(names,1,instr(names,'~',1,1)-1)new_name,");
		sb.append(" substr(names,instr(names,'~',1,1)+1)old_name");
		sb.append(" FROM ");
		sb.append(" (SELECT TO_CHAR(a.TIMESTAMP,'dd/mm/yyyy HH24:mi') TIMESTAMP, a.user_id, a.user_notes, a.content, a.pnr, ");
		sb.append(" res.booking_timestamp AS BOOKINGDATE, F_GET_NAME_CHANGE_FROM_RES_AUD(content)names ");
		sb.append("	FROM t_reservation_audit a, T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, T_FLIGHT f , T_RESERVATION res ");
		sb.append("	WHERE a.template_code = 'MODPAX'");
		sb.append("	AND lower(a.content) like '%name%' ");
		sb.append("	and a.pnr = ps.pnr  and ps.status = 'CNF' ");
		sb.append("	and ps.flt_seg_id = fs.flt_seg_id ");
		sb.append("	and fs.flight_id = f.flight_id ");
		sb.append(" and res.pnr = a.pnr ");
		sb.append("	and a.timestamp between TO_DATE('" + fromDate + " 00:00:00','DD-MON-YYYY HH24:MI:SS') ");
		sb.append("	and TO_DATE('" + toDate + " 23:59:59','DD-MON-YYYY HH24:MI:SS')  ");
		/* Jira 3433 - Issue 1 */
		if (isNotEmptyOrNull(userId) && !userId.equalsIgnoreCase("All")) {
			sb.append("	and a.user_id = '" + userId + "' ");
		}
		if (isNotEmptyOrNull(flightNo)) {
			sb.append("	and f.flight_number = '" + flightNo + "' ");
		}

		if (isNotEmptyOrNull(strSectorFrom) && isNotEmptyOrNull(strSectorTo)) {
			sb.append("	and fs.segment_code like '" + strSectorFrom + "%" + strSectorTo + "'");
		} else if (isNotEmptyOrNull(strSectorFrom)) {
			sb.append("	and SUBSTR(fs.segment_code, 0, 3) = UPPER('" + strSectorFrom + "') ");
		} else if (isNotEmptyOrNull(strSectorTo)) {
			sb.append("	AND SUBSTR(fs.segment_code, (LENGTH(fs.segment_code) - 2), 3) = UPPER('" + strSectorTo + "') ");
		}

		if (isNotEmptyOrNull(strDepFromdate) && isNotEmptyOrNull(strDepToDate)) {
			sb.append("	AND fs.est_time_departure_zulu between TO_DATE('" + strDepFromdate
					+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') ");
			sb.append("	AND TO_DATE('" + strDepToDate + " 23:59:59','DD-MON-YYYY HH24:MI:SS')  ");
		}
		sb.append(" order by a.timestamp )");

		return sb.toString();
	}

	/**
	 * Retrieves booked hala services for a given time period/ airport / flight.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getBookedHalaServiceSummaryRequestQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String fromAirport = reportsSearchCriteria.getFrom();
		String toAirport = reportsSearchCriteria.getTo();
		String flightNo = reportsSearchCriteria.getFlightNumber();
		String emailStatus = reportsSearchCriteria.getStatus();
		String ssrCategory = reportsSearchCriteria.getSsrCategory();
		String hubAirPort = reportsSearchCriteria.getStation();
		Integer noofAttempts = reportsSearchCriteria.getNoOfAttempts();

		sb.append("SELECT chg.ssr_charge_id AS ssr_charge_id, "
				+ "chg.ssr_charge_code charge_code,chg.description AS charge_description,flightno, "
				+ "seg_code,airport, destination,depzulu, ARRVLOCAL,deplocal,ARRZULU, email.email,saa.airport_code as ssr_airport,arrivaldate,depdate "
				+ ",decode(substr(seg_code,1,3),'SHJ',deplocal,NULL)AS DEPTIME , "
				+ "decode(substr(seg_code,length(seg_code)-2,3),'" + hubAirPort + "',arrvlocal,NULL)AS ARRIVALTIME, "
				+ "DECODE (INSTR (seg_code,'" + hubAirPort + "'),0 , NULL, '" + hubAirPort + "') AS serviceairport, "
				+ "count(decode( PAX.pax_type_code,'AD',PAX.pnr_pax_id,NULL))ADS, "
				+ "count(decode( PAX.pax_type_code,'CH',PAX.pnr_pax_id,NULL))CHS, "
				+ "count(decode( PAX.pax_type_code,'IN',PAX.pnr_pax_id,NULL))INS "
				+ "FROM  t_ssr_info info, t_ssr_applicable_airport saa,t_ssr_applicability_containts sac, "
				+ "t_ssr_sub_category subcat,t_ssr_category_email email,  t_pnr_pax_fare fr,t_pnr_passenger pax,t_ssr_charge chg "
				+ "INNER JOIN "
				+ "(SELECT ppss.context_id as contextID, ppfsid,ppfID, pnrsegid, pnr, seg_code, "
				+ "deplocal, airport, destination, depzulu, ARRVLOCAL,ARRZULU, flightno,arrivaldate,depdate "
				+ " FROM t_pnr_pax_segment_ssr ppss "
				+ "INNER JOIN "
				+ "(SELECT ppfs.ppfs_id AS ppfsid,ppfs.ppf_id as ppfID, pnrsegid, pnr,seg_code, deplocal, airport, destination, "
				+ "depzulu, ARRVLOCAL, ARRZULU, flightno,arrivaldate,depdate FROM t_pnr_pax_fare_segment ppfs "
				+ "INNER JOIN "
				+ "(SELECT ps.pnr_seg_id AS pnrsegid,ps.pnr AS pnr, seg_code, deplocal, "
				+ "airport, destination, depzulu,ARRVLOCAL, ARRZULU, flightno,arrivaldate,depdate FROM t_pnr_segment ps "
				+ "INNER JOIN "
				+ "(SELECT fs.flt_seg_id AS seg_id,fs.segment_code AS seg_code, fs.est_time_departure_local AS deplocal, "
				+ "f.origin AS airport,f.destination AS destination, fs.est_time_departure_zulu AS depzulu, "
				+ "fs.est_time_arrival_local AS ARRVLOCAL,fs.est_time_arrival_zulu  AS ARRZULU,"
				+ "to_char(fs.est_time_departure_local , 'dd/mm/yy HH24:mi') depdate, "
				+ "to_char(fs.est_time_arrival_local , 'dd/mm/yy HH24:mi') arrivaldate, "
				+ "f.flight_number AS flightno FROM t_flight_segment fs,t_flight f ");

		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			sb.append("WHERE fs.est_time_departure_zulu BETWEEN TO_DATE(" + fromDate + ") AND TO_DATE(" + toDate + ") ");
		}
		if (isNotEmptyOrNull(flightNo)) {
			sb.append("and f.flight_number='" + flightNo + "' ");
		}
		if (isNotEmptyOrNull(fromAirport)) {
			sb.append("and f.origin ='" + fromAirport + "' ");
		}
		if (isNotEmptyOrNull(toAirport)) {
			sb.append("and f.destination='" + toAirport + "' ");
		}

		sb.append(" AND f.flight_id = fs.flight_id) aaa ON ps.flt_seg_id = aaa.seg_id "
				+ "AND ps.status <> 'CNX') bbb ON ppfs.pnr_seg_id = bbb.pnrsegid) ccc "
				+ "ON ppss.ppfs_id = ccc.ppfsid AND ppss.status <> 'CNX' ");
		if (isNotEmptyOrNull(emailStatus)) {
			sb.append("AND ppss.email_status='N' ");
		}
		if (noofAttempts != null) {
			int attempts = noofAttempts.intValue();
			sb.append("AND (ppss.NO_OF_ATTEMPTS is null OR ppss.NO_OF_ATTEMPTS<" + attempts + ")");
		}
		sb.append(" ) ddd ON chg.ssr_charge_id = ddd.contextID "
				+ "where   chg.ssr_id = info.ssr_id AND info.ssr_sub_cat_id = subcat.ssr_sub_cat_id "
				+ "AND subcat.ssr_cat_id = email.ssr_cat_id ");
		if (isNotEmptyOrNull(ssrCategory)) {
			sb.append("AND subCat.ssr_cat_id =" + ssrCategory);
		}
		sb.append("and chg.ssr_id = sac.ssr_id and sac.ssr_app_con_id = saa.ssr_app_con_id "
				+ "and fr.ppf_id = ddd.ppfID  AND pax.pnr_pax_id =fr.pnr_pax_id  and pax.status <> 'CNX'");

		sb.append(" group by contextID,chg.ssr_charge_id, chg.ssr_charge_code, chg.description, "
				+ "flightno,seg_code,airport, destination,depzulu, ARRVLOCAL,deplocal,ARRZULU,email.email,saa.airport_code");
		sb.append(" order by flightno");
		return sb.toString();
	}

	/**
	 * Create the SQL query to retrieve booked hala services detail for a given search criteria - flight no.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 * @author lalanthi
	 */
	public String getBookedHalaServiceDetailRequestQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String fromAirport = reportsSearchCriteria.getFrom();
		String flightNo = reportsSearchCriteria.getFlightNumber();
		String ssrChargeId = reportsSearchCriteria.getSsrChargeId();
		String emailStatus = reportsSearchCriteria.getStatus();

		sb.append("select res.pnr, TO_CHAR (res.booking_timestamp, 'dd/mm/yy HH24:mi') AS bookedtime,"
				+ "INITCAP (pax.title||'.'||pax.first_name || ' ' || pax.last_name) AS fullname, rCon.c_mobile_no AS contactno, "
				+ "chg.charge_amount AS amount, chg.description, res.booking_timestamp AS servicebookedtime "
				+ "from  t_ssr_charge chg,t_pnr_pax_fare F,t_pnr_passenger pax,t_reservation res, t_reservation_contact rCon, "
				+ "(select ssr.ppss_id ,ssr.context_id as contextID,ssr.charge_amount as amount,ppf_id  from t_pnr_pax_segment_ssr ssr "
				+ "inner join (select ppfs_id,ppf_id from t_pnr_pax_fare_segment fs inner join "
				+ "(select pnr_seg_id from t_pnr_segment PS where PS.flt_seg_id in(select flt_seg_id from "
				+ "t_flight_segment seg,t_flight f where ");

		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			sb.append("seg.est_time_departure_local = to_date(" + fromDate + ") and seg.est_time_arrival_local = to_date("
					+ toDate + ") AND");
		}

		if (isNotEmptyOrNull(flightNo)) {
			sb.append(" F.flight_number = '" + flightNo + "' ");
		}
		if (isNotEmptyOrNull(fromAirport)) {
			sb.append("AND F.origin ='" + fromAirport + "' ");
		}
		sb.append(")) AAA on fs.pnr_seg_id=AAA.pnr_seg_id)BBB on  ssr.ppfs_id = BBB.ppfs_id and ssr.status<>'CNX' ");

		if (isNotEmptyOrNull(emailStatus)) {
			sb.append(" AND ssr.email_status='N' ");
		}
		sb.append(") CCC where chg.ssr_charge_id = CCC.contextID ");
		if (isNotEmptyOrNull(ssrChargeId)) {
			sb.append("AND CHG.ssr_charge_id =" + ssrChargeId + " ");
		}
		sb.append("and res.pnr=rCon.pnr AND F.ppf_id =  CCC.ppf_id and F.pnr_pax_id =pax.pnr_pax_id and pax.pnr = res.pnr "
				+ "AND pax.status<>'CNX' AND res.status <> 'CNX'");
		sb.append("order by  CHG.description, RES.pnr");

		return sb.toString();

	}

	/**
	 * Create sql to retrieves commission details for Hala services for a given time period.
	 * 
	 * @param reportsSearchCriteria
	 * @return String
	 * @author lalanthi
	 */
	public String getHalaServiceCommissionRequestQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();

		sb.append("select PAX.pnr, to_char(RES.booking_timestamp, 'dd/mm/yy HH24:mi')as booking_timestamp,CHANNEL.description as BOOKED_BY, "
				+ "PAX.title||'.'||INITCAP(PAX.first_name||' '||PAX.last_name) AS PAXNAME, "
				+ "ppss.charge_amount*count(ppss.ppfs_id) as CHARGE_AMOUNT,sc.description , "
				+ "decode(sc.commission_in,'P',ppss.charge_amount*(sc.commission_value/100)*count(ppss.ppfs_id),sc.commission_value*count(ppss.ppfs_id) )as CommissionValue "
				+ "from T_PNR_Segment PS, T_PNR_Passenger PAX, t_pnr_pax_fare_segment ppfs, t_pnr_pax_segment_ssr ppss, "
				+ "t_ssr_charge sc ,t_pnr_pax_fare PF, t_flight_segment fs, t_flight f,t_reservation RES,t_sales_channel CHANNEL "
				+ "where "
				+ "f.flight_id = fs.flight_id "
				+ "and PS.flt_seg_id = fs.flt_seg_id "
				+ "and PAX.pnr = PS.pnr "
				+ "and RES.pnr = PAX.pnr "
				+ "and RES.origin_channel_code = CHANNEL.sales_channel_code "
				+ "and PF.pnr_pax_id = PAX.pnr_pax_id "
				+ "and ppfs.ppf_id = PF.ppf_id "
				+ "and PS.pnr_seg_id = ppfs.pnr_seg_id "
				+ "and ppfs.ppfs_id = ppss.ppfs_id "
				+ "and sc.ssr_charge_id = ppss.context_id "
				+ "and PS.status<>'CNX' "
				+ "AND pax.pax_type_code <> 'IN'");

		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			sb.append("and fs.est_time_departure_local between to_date(" + fromDate + ") and to_date(" + toDate + ") ");
		}
		sb.append("group by ppss.charge_amount,ppss.ppfs_id,ppss.context_id,RES.booking_timestamp,CHANNEL.description, "
				+ "sc.commission_in,sc.commission_value,sc.description ,PAX.title,PAX.first_name,PAX.pnr,PAX.last_name,"
				+ "PAX.pnr_pax_id ORDER BY PAX.pnr_pax_id");

		return sb.toString();
	}

	/**
	 * Retrieves booked hala services details for Send Notification to relevant Airport's.
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getBookedHalaServiceNotifiationQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String fromAirport = reportsSearchCriteria.getFrom();
		String toAirport = reportsSearchCriteria.getTo();
		String flightNo = reportsSearchCriteria.getFlightNumber();
		String emailStatus = reportsSearchCriteria.getStatus();
		String ssrCategory = reportsSearchCriteria.getSsrCategory();
		String hubAirPort = reportsSearchCriteria.getStation();
		Integer noofAttempts = reportsSearchCriteria.getNoOfAttempts();

		sb.append("SELECT pax.pnr AS pnr,f.flight_number AS flightno,"
				+ "INITCAP (pax.title  ||'.'  ||pax.first_name  || ' '  || pax.last_name) AS fullname,"
				+ "fs.segment_code AS seg_code,  f.origin AS origin,  f.destination AS destination,"
				+ "TO_CHAR(fs.est_time_departure_local , 'dd/mm/yy HH24:mi') depdate,"
				+ "TO_CHAR(fs.est_time_arrival_local , 'dd/mm/yy HH24:mi') arrivaldate,"
				+ "sinf.ssr_code AS ssr_code,  sinf.ssr_desc AS ssr_desc,  ppss.airport_code AS airport, ppss.PPSS_ID AS PPSS_ID, "
				+ "fs.flt_seg_id AS flt_segment, sinf.ssr_id AS ssr_id, scat.SSR_CAT_ID AS SSR_CAT_ID ");
		sb.append("FROM t_flight_segment fs, t_flight f, t_pnr_segment ps, t_pnr_pax_fare_segment ppfs,"
				+ " t_pnr_pax_fare ppf,t_ssr_info sinf ,   t_ssr_sub_category scat,  t_pnr_passenger pax ,"
				+ "t_pnr_pax_segment_ssr ppss ");
		sb.append("WHERE pax.pnr_pax_id  = ppf.pnr_pax_id AND ppf.ppf_id      = ppfs.ppf_id "
				+ "AND ppfs.pnr_seg_id = ps.pnr_seg_id AND ps.flt_seg_id   = fs.flt_seg_id "
				+ "AND fs.flight_id    = f.flight_id AND ppss.ppfs_id    = ppfs.ppfs_id "
				+ "AND ppss.ssr_id     = sinf.ssr_id AND sinf.ssr_sub_cat_id = scat.ssr_sub_cat_id "
				+ "AND ps.status <> 'CNX' AND pax.status <>'CNX' " + "AND ppss.status <> 'CNX' ");

		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			sb.append("AND fs.est_time_departure_zulu BETWEEN TO_DATE(" + fromDate + ") AND TO_DATE(" + toDate + ") ");
		}

		if (isNotEmptyOrNull(ssrCategory)) {
			sb.append("AND scat.ssr_cat_id =" + ssrCategory + " ");
		}

		if (noofAttempts != null) {
			int attempts = noofAttempts.intValue();
			sb.append("AND (ppss.NO_OF_ATTEMPTS IS NULL OR ppss.NO_OF_ATTEMPTS < " + attempts + ") ");
		}

		if (isNotEmptyOrNull(hubAirPort)) {
			sb.append("AND ppss.airport_code ='" + hubAirPort + "' ");
		}

		if (isNotEmptyOrNull(fromAirport)) {
			sb.append("AND f.origin ='" + fromAirport + "' ");
		}

		if (isNotEmptyOrNull(toAirport)) {
			sb.append("AND f.destination ='" + toAirport + "' ");
		}

		if (isNotEmptyOrNull(flightNo)) {
			sb.append("AND f.flight_number='" + flightNo + "' ");
		}

		if (isNotEmptyOrNull(emailStatus)) {
			sb.append("AND ppss.email_status='N' ");
		}

		sb.append("ORDER BY fs.est_time_departure_local,fs.est_time_arrival_local,f.flight_number ");

		return sb.toString();
	}

	/**
	 * @author Navod Ediriweera Creates the SQL for Segment Pax Chart Report
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getSegmentPaxTotalData(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		sb.append("select * from (select t1.*,ROWNUM rn from (");

		sb.append(" select sum(q1.tot_pax) as Pax_Total,q1.status as Status,q1.segment_code as Segment");
		sb.append("  from (SELECT (c.total_pax_count+ c.total_pax_child_count) as tot_pax, a.status, b.segment_code");
		sb.append("  FROM t_pnr_segment a, t_flight_segment b, t_reservation c ");
		sb.append("  where a.flt_seg_id = b.flt_seg_id and a.pnr = c.pnr");
		sb.append("  and c.booking_timestamp between TO_DATE('" + fromDate + " 00:00:00','DD-MON-YYYY HH24:MI:SS')");
		sb.append("      and TO_DATE('" + toDate + " 23:59:59','DD-MON-YYYY HH24:MI:SS')");
		sb.append("  and a.status = 'CNF'");
		sb.append("  group by b.segment_code, a.status, c.total_pax_count, c.total_pax_child_count, c.total_pax_infant_count) q1");
		sb.append("  group by q1.segment_code,q1.status");
		sb.append("  order by Pax_Total desc");
		sb.append(" )t1) where rn between 0 and 11");
		return sb.toString();
	}

	/**
	 * @author Farooq Sheikh Creates the SQL for External Payment Reconciliation Report
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getExtPmtReconcilSummaryDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String agentUserId = reportsSearchCriteria.getUserId();

		sb.append("SELECT EX.PNR, EX.PAX_COUNT, EX.BAL_QUERY_KEY, TO_CHAR(EX.INTERNAL_START_TIME,'DD-MON-YYYY HH24:MI:SS') as INTERNAL_START_TIME,EX.EXTERNAL_PAY_ID, TO_CHAR(EX.EXTERNAL_END_TIME, 'DD-MON-YYYY HH24:MI:SS') as EXTERNAL_END_TIME,");
		sb.append("SC.DESCRIPTION, EX.AMOUNT, EX.USER_ID,  ag.agent_name,SUM(EX.AMOUNT) OVER() as GTOTAL");
		sb.append(" FROM T_EXTERNAL_PAYMENT_TX EX , T_SALES_CHANNEL SC, T_Agent ag");
		sb.append(" WHERE EX.USER_ID ='" + agentUserId + "'");
		sb.append(" AND ag.agent_code = (SELECT U.AGENT_CODE FROM T_USER U WHERE U.USER_ID = '" + agentUserId + "') ");
		sb.append(" AND EX.STATUS ='S' AND EX.CHANNEL = SC.SALES_CHANNEL_CODE");
		sb.append(" AND EX.INTERNAL_END_TIME BETWEEN TO_DATE('" + fromDate + " 00:00:00','DD-MON-YYYY HH24:MI:SS')");
		sb.append(" AND TO_DATE('" + toDate + " 23:59:59','DD-MON-YYYY HH24:MI:SS')");
		sb.append("ORDER BY EX.INTERNAL_END_TIME ASC");

		return sb.toString();
	}

	/**
	 * Added by manju
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getMealsSummaryDataQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.MEALS_SUMMARY)) {

			sb.append(" select a.meal_name , a.meal_price, count(a.pas_id) as number_meals, sum(a.meal_price) as tot_amt, SUM(COUNT (a.pas_id)) OVER() as total from (");
			sb.append(" select mel.meal_name as meal_name, pp.amount as meal_price, pp.pnr_pax_id as pas_id");
			sb.append(" from ml_t_meal mel,   ml_t_pnr_pax_seg_meal pp,");
			sb.append(" t_pnr_segment ps, t_flight_segment fs, t_flight ft ");
			sb.append(" where mel.meal_id =  pp.meal_id and pp.pnr_seg_id = ps.pnr_seg_id 	");
			sb.append(" and ps.flt_seg_id = fs.flt_seg_id and fs.flight_id = ft.flight_id");

			if (!reportsSearchCriteria.getFlightNumber().equals("")) {
				sb.append(" and ft.flight_number = '" + reportsSearchCriteria.getFlightNumber().toUpperCase() + "'");
			}
			sb.append(" AND pp.status <> 'CNX' AND ps.status <> 'CNX' ");
			sb.append(" AND fs.est_time_departure_local between TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'dd-mon-yyyy HH24:mi:ss') ");
			sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss') ");
			sb.append(" order by meal_name) a");
			sb.append(" group by a.meal_name,a.meal_price ");

		} else {

			// credit Details
			sb.append(" select ft.flight_number as flt_number, pas.pnr as pnr, pas.upper_first_name || ' ' || pas.upper_last_name as pax_name,");
			sb.append(" fs.segment_code as seg_code, TO_CHAR(ft.departure_date, 'DD-MON-YYYY') as dep_date,  mel.meal_name as meal_name, pp.amount as meal_price, ");
			sb.append(" agnt.agent_name ,st.station_name station,ps.status_mod_user_id user_id ");
			sb.append(" from ml_t_meal mel,  ml_t_pnr_pax_seg_meal pp, t_pnr_segment ps, t_flight_segment fs, t_flight ft, ");
			sb.append(" t_pnr_passenger pas, t_agent agnt, t_station st ");
			sb.append(" where mel.meal_id =  pp.meal_id and pas.pnr_pax_id = pp.pnr_pax_id and pp.pnr_seg_id = ps.pnr_seg_id");
			sb.append(" AND pp.status <> 'CNX' AND ps.status <> 'CNX' AND pas.status <> 'CNX' ");
			sb.append(" and ps.status_agent_code = agnt.agent_code (+) and agnt.station_code = st.station_code (+) ");
			sb.append(" and ps.flt_seg_id = fs.flt_seg_id and fs.flight_id = ft.flight_id ");

			if (!reportsSearchCriteria.getFlightNumber().equals("")) {
				sb.append(" and ft.flight_number = '" + reportsSearchCriteria.getFlightNumber().toUpperCase() + "'");
			}
			sb.append(" and upper(mel.meal_name) like upper('%" + reportsSearchCriteria.getMealsName() + "%')");
			sb.append(" AND fs.est_time_departure_local between TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
					+ " 00:00:00', 'dd-mon-yyyy HH24:mi:ss') ");
			sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss') ");
			sb.append(" order by pp.pnr_pax_id");
		}
		return sb.toString();
	}

	public String getPassengerMealSummary(ReportsSearchCriteria reportsSearchCriteria) {
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String dateFrom = reportsSearchCriteria.getDateRangeFrom();
		String dateTo = reportsSearchCriteria.getDateRangeTo();
		return generatePassenegerSummaryReport(dateFrom, dateTo, flightNumber, true);
	}

	public String getFlightPassengerManifestSummary(ReportsSearchCriteria reportsSearchCriteria){
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String dateFrom = reportsSearchCriteria.getDateRangeFrom();
		return generatePassenegerSummaryReport(dateFrom,dateFrom,flightNumber,false);
	}

	private String generatePassenegerSummaryReport(String departureFrom,String departureTo,String flightNumber,boolean hasMeal) {
		StringBuilder sb = new StringBuilder();
		sb.append("select count(passenger.pnr_pax_id) as NUMBER_OF_PASSENGERS,trim(f_convert_message(messages.message_content)) as BUNDLE_NAME " +
				"from t_reservation reserv " +
				"inner join t_pnr_passenger passenger on reserv.pnr = passenger.pnr " +
				"inner join t_pnr_segment pnrSeg on pnrSeg.pnr = reserv.pnr " +
				"inner join T_FLIGHT_SEGMENT flightSeg on flightSeg.flt_seg_id = pnrSeg.flt_seg_id " +
				"inner join t_flight flight on flight.FLIGHT_ID = flightSeg.FLIGHT_ID " +
				"inner join t_bundled_fare_period bundledFarePeriod on pnrSeg.bundled_fare_period_id = bundledFarePeriod.bundled_fare_period_id " +
				"inner join t_bundled_fare_bundledFare on bundledFare.bundled_fare_id = bundledFarePeriod.bundled_fare_id " +
				
 "inner join t_bundled_fare_category bundledCategory on bundledFare.bundled_fare_category_id = bundledCategory.bundled_fare_category_id "
				+ "inner join  t_i18n_message messages on messages.I18N_MESSAGE_KEY = ('BUNDLE_CATEGORY_NAME' || bundledFare.bundled_fare_category_id) ");
		if (hasMeal) {
			sb.append("and (select MAX(DECODE(freeService.service_name,'MEAL',1,0)) from t_bundled_fare_free_service freeService where bundled_fare_period_id = bundledFarePeriod.bundled_fare_period_id) = 1");
		}
		sb.append("and pnrSeg.status <> 'CNX'  and passenger.pax_type_code <> 'IN' ");
		sb.append("AND (flightSeg.est_time_departure_local between TO_DATE('" + departureFrom + " 00:00:00', 'dd-mon-yyyy HH24:mi:ss') " +
				"AND TO_DATE('" + departureTo + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss'))");
		if (flightNumber != null && !flightNumber.isEmpty()) {
			sb.append("and flight.flight_number = '" + flightNumber + "' ");
		}
		sb.append("group by f_convert_message(messages.message_content)");
		return sb.toString();
	}
	/**
	 * @author Harsha Udayapriya Creates the SQL for Segment Meal detail Report
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getCountOfPassengers(ReportsSearchCriteria reportsSearchCriteria) throws ModuleException {

		StringBuilder sb = new StringBuilder();

		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String depatureDate = reportsSearchCriteria.getDepTimeLocal();
		String paxStatus = Util.buildStringInClauseContent(reportsSearchCriteria.getPaxStatus());
		if (paxStatus == null) {
			throw new ModuleException("reporting.params.invalid");
		}

		sb.append(" SELECT  pp.status, b.segment_code, COUNT (pp.pnr_pax_id) cnt ");
		sb.append(" FROM t_flight a, t_flight_segment b, t_pnr_segment c, t_pnr_passenger pp ");
		sb.append(" WHERE a.flight_id = b.flight_id ");
		sb.append(" AND b.flt_seg_id = c.flt_seg_id AND c.pnr = pp.pnr  ");
		sb.append(" AND c.status != 'CNX' ");
		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" and a.flight_number = '" + flightNumber + "' ");
		}
		if (isNotEmptyOrNull(depatureDate)) {
			sb.append(" AND b.est_time_departure_local between to_date('" + depatureDate
					+ " 00:00:00','dd-mon-yyyy hh24:mi:ss') and to_date('" + depatureDate + " ");
			sb.append("23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		}
		sb.append(" and pp.status in (" + paxStatus + ") ");
		sb.append(" and pp.pax_type_code in ('AD','CH')	group by pp.status, b.segment_code ");
		return sb.toString();

	}

	/**
	 * @author Harsha Udayapriya Creates the SQL for Segment Meal Summary Report
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getMealSummaryQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String depatureDate = reportsSearchCriteria.getDepTimeLocal();

		sb.append(" select e.meal_name, count(e.meal_name) as amount, e.meal_code,f.segment_code as segment, f.dep_time_local ");
		sb.append(" from ml_t_pnr_pax_seg_meal d, ml_t_meal e,  ");
		sb.append(" 		(select c.pnr_seg_id,b.segment_code, to_char(b.est_time_departure_local, 'hh24:mi:ss') dep_time_local ");
		sb.append("			from t_flight a, t_flight_segment b, t_pnr_segment c");
		sb.append(" 		where a.flight_number = ");
		if (isNotEmptyOrNull(flightNumber)) {
			sb.append("'" + flightNumber + "' ");
		}
		sb.append(" and b.est_time_departure_local between to_date(' ");
		if (isNotEmptyOrNull(depatureDate)) {
			sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') and to_date('");
			sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss')");
		}
		sb.append(" 		and a.flight_id = b.flight_id and b.flt_seg_id = c.flt_seg_id) f ");
		sb.append(" where d.status != 'CNX' and d.meal_id = e.meal_id and d.pnr_seg_id = f.pnr_seg_id ");
		sb.append(" group by (e.meal_name , e.meal_code,f.segment_code, f.dep_time_local ) ");
		sb.append(" order by(f.segment_code) ");
		return sb.toString();
	}

	/**
	 * Creates the SQL for Segment Meal detail Report
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getMealDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String depatureDate = reportsSearchCriteria.getDepTimeLocal();
		String paxCategory = reportsSearchCriteria.getPaxCategory();

		sb.append(" SELECT x.* ");
		sb.append(" FROM ( ");
		sb.append("  SELECT pp.pnr_pax_id, pp.first_name, pp.last_name, pp.pnr, pp.status, ");
		sb.append(" 	(SELECT ams.seat_code ");
		sb.append(" 	   FROM sm_t_pnr_pax_seg_am_seat pss, ");
		sb.append("			 	sm_t_flight_am_seat fs, ");
		sb.append(" 			sm_t_aircraft_model_seats ams ");
		sb.append("		  WHERE pss.flight_am_seat_id = fs.flight_am_seat_id ");
		sb.append("		    AND fs.am_seat_id = ams.am_seat_id ");
		sb.append("			AND pss.status = 'RES' ");
		sb.append("			AND pss.pnr_pax_id = pp.pnr_pax_id ");
		sb.append("			AND pss.pnr_seg_id = c.pnr_seg_id) seat_code, ");
		sb.append("    f_concatenate_list(cursor(SELECT meal_code ");
		sb.append("    	   FROM ml_t_pnr_pax_seg_meal psm, ml_t_meal ml ");
		sb.append("	  	  WHERE psm.meal_id = ml.meal_id ");
		sb.append("		    AND psm.status = 'RES' ");
		sb.append("			AND psm.pnr_pax_id = pp.pnr_pax_id ");
		sb.append("		    AND psm.pnr_seg_id = c.pnr_seg_id) , ',') meal_code , to_char(b.est_time_departure_local, 'hh24:mi:ss') dep_time_local");
		sb.append("	   FROM t_pnr_passenger pp, t_flight a, t_flight_segment b, t_pnr_segment c ");
		sb.append("	  WHERE pp.status IN ('CNF', 'OHD') ");
		sb.append("	    AND pp.pnr = c.pnr ");
		sb.append("		AND c.status = 'CNF' ");
		sb.append("		AND a.flight_number =  ");
		if (isNotEmptyOrNull(flightNumber)) {
			sb.append("'" + flightNumber + "' ");
		}
		sb.append("			   AND b.est_time_departure_local BETWEEN TO_DATE (' ");
		if (isNotEmptyOrNull(depatureDate)) {
			sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('");
			sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		}
		sb.append("		AND a.flight_id = b.flight_id ");
		sb.append("		AND b.flt_seg_id = c.flt_seg_id ");
		sb.append(" ) x ");
		if (isNotEmptyOrNull(paxCategory) && !paxCategory.equals("ALL")) {
			sb.append(" WHERE x.meal_code IS NOT NULL ");
		}
		sb.append(" ORDER BY x.first_name, x.last_name ");

		return sb.toString();
	}

	/**
	 * Creates the SQL for Segment Flight Ancillary detail Report
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getFlightAncillaryDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		boolean isSelected = false;

		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String depatureDate = reportsSearchCriteria.getDepTimeLocal();

		sb.append(" SELECT vw.pnr, vw.paxname as paxName,");
		sb.append("    SUBSTR (vw.contact_infomation,");
		sb.append("            1,");
		sb.append("            INSTR (vw.contact_infomation, '~', 1) - 1");
		sb.append("           ) AS contactPerson,");
		sb.append("    SUBSTR (vw.contact_infomation,");
		sb.append("            INSTR (vw.contact_infomation, '~', 1) + 1,");
		sb.append("              INSTR (vw.contact_infomation, '~', 1, 2)");
		sb.append("            - INSTR (vw.contact_infomation, '~', 1)");
		sb.append("            - 1");
		sb.append("           ) AS mobileNumber,");
		sb.append("    SUBSTR (vw.contact_infomation,");
		sb.append("            INSTR (vw.contact_infomation, '~', 1, 2) + 1,");
		sb.append("              INSTR (vw.contact_infomation, '~', 1, 3)");
		sb.append("            - INSTR (vw.contact_infomation, '~', 1, 2)");
		sb.append("            - 1");
		sb.append("           ) AS phoneNumber,");
		sb.append("    SUBSTR (vw.contact_infomation,");
		sb.append("            INSTR (vw.contact_infomation, '~', 1, 3) + 1");
		sb.append("           ) AS eMail,");
		sb.append("    vw.departuretimelocal as departureTimeLocal, vw.segmentcode as segmentCode");
		if (reportsSearchCriteria.isInsuranceSelected()) {
			sb.append(", vw.insuranceselected as insuranceSelected");
		}
		if (reportsSearchCriteria.isMealSelected()) {
			sb.append(",vw.mealselected as mealSelected");
		}
		if (reportsSearchCriteria.isBaggageSeleted()) {
			sb.append(", vw.baggageselected as baggageSelected");
		}
		if (reportsSearchCriteria.isSeatSelected()) {
			sb.append(", vw.seatselected as seatSelected");
		}
		if (reportsSearchCriteria.isHalaSerivceSelected()) {
			sb.append(", vw.halaselected as halaSelected");
		}
		if (reportsSearchCriteria.isFlexiSelected()) {
			sb.append(",vw.flexiselected as flexiSelected");
		}
		if (reportsSearchCriteria.isBusSelected()) {
			sb.append(", vw.busselected as busSelected");
		}
		sb.append(" FROM (SELECT   ins.pnr, ins.paxname AS paxname,");
		sb.append("              ins.contact_infomation AS contact_infomation,");
		sb.append("              to_char(ins.est_time_departure_local,'HH24:MI') AS departuretimelocal,");
		sb.append("              ins.segment_code AS segmentcode,");
		sb.append("              ins.insuarance_selected AS insuranceselected");
		if (reportsSearchCriteria.isMealSelected()) {
			sb.append(",              CASE");
			sb.append("                 WHEN meal.meal_count > 0");
			sb.append("                    THEN 'YES'");
			sb.append("                 ELSE 'NO'");
			sb.append("              END AS mealselected");
		}
		if (reportsSearchCriteria.isBaggageSeleted()) {
			sb.append(",              CASE");
			sb.append("                 WHEN (bag.baggage_id IS NOT NULL)");
			sb.append("                    THEN 'YES'");
			sb.append("                 ELSE 'NO'");
			sb.append("              END AS baggageselected");
		}
		if (reportsSearchCriteria.isSeatSelected()) {
			sb.append(",              CASE");
			sb.append("                 WHEN seat.flight_am_seat_id IS NOT NULL");
			sb.append("                    THEN 'YES'");
			sb.append("                 ELSE 'NO'");
			sb.append("              END AS seatselected");
		}
		if (reportsSearchCriteria.isHalaSerivceSelected()) {
			sb.append(",              CASE");
			sb.append("                 WHEN ssr.hala_count > 0");
			sb.append("                    THEN 'YES'");
			sb.append("                 ELSE 'NO'");
			sb.append("              END AS halaselected");
		}
		if (reportsSearchCriteria.isFlexiSelected()) {
			sb.append(",              CASE");
			sb.append("                 WHEN flexi.flexi_count > 0");
			sb.append("                    THEN 'YES'");
			sb.append("                 ELSE 'NO'");
			sb.append("              END AS flexiselected");
		}
		if (reportsSearchCriteria.isBusSelected()) {
			sb.append(",              CASE");
			sb.append("                 WHEN bus.ground_seg_id IS NOT NULL");
			sb.append("                    THEN 'YES'");
			sb.append("                 ELSE 'NO'");
			sb.append("              END AS busselected");
		}
		sb.append("         FROM (SELECT r0.pnr,");
		sb.append("                         px0.title");
		sb.append("                      || ' '");
		sb.append("                      || px0.first_name");
		sb.append("                      || ' '");
		sb.append("                      || px0.last_name AS paxname,");
		sb.append("                      CASE");
		sb.append("                         WHEN i0.ins_id IS NOT NULL");
		sb.append("                            THEN 'YES'");
		sb.append("                         ELSE 'NO'");
		sb.append("                      END AS insuarance_selected,");
		sb.append("                      (SELECT    rc.c_title");
		sb.append("                              || ' '");
		sb.append("                              || rc.c_first_name");
		sb.append("                              || ' '");
		sb.append("                              || rc.c_last_name");
		sb.append("                              || '~'");
		sb.append("                              || rc.c_mobile_no");
		sb.append("                              || '~'");
		sb.append("                              || rc.c_phone_no");
		sb.append("                              || '~'");
		sb.append("                              || rc.c_email");
		sb.append("                         FROM t_reservation_contact rc");
		sb.append("                        WHERE rc.pnr = r0.pnr) AS contact_infomation,");
		sb.append("                      ps0.pnr_seg_id, px0.pnr_pax_id, fs0.segment_code,");
		sb.append("                      fs0.est_time_departure_local");
		sb.append("                 FROM t_reservation r0,");
		sb.append("                      t_pnr_insurance i0,");
		sb.append("                      t_pnr_passenger px0,");
		sb.append("                      t_pnr_segment ps0,");
		sb.append("                      t_flight_segment fs0,");
		sb.append("                      t_flight f0");
		sb.append("                WHERE px0.pnr = r0.pnr");
		sb.append("                  AND i0.pnr(+) = r0.pnr");
		sb.append("                  AND ps0.pnr = r0.pnr");
		sb.append("                  AND fs0.flt_seg_id = ps0.flt_seg_id");
		sb.append("                  AND f0.flight_id = fs0.flight_id");
		sb.append("    				 AND f0.flight_number =");
		if (isNotEmptyOrNull(flightNumber)) {
			sb.append("'" + flightNumber + "' ");
		}
		sb.append("                 AND fs0.est_time_departure_local BETWEEN to_date('");
		if (isNotEmptyOrNull(depatureDate)) {
			sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
			sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		}
		sb.append("                 AND ps0.status != 'CNX') ins");
		if (reportsSearchCriteria.isMealSelected()) {
			sb.append("             ,(SELECT meal1.pnr_pax_id, meal1.pnr_seg_id, count(meal1.meal_id) as meal_count");
			sb.append("                FROM ml_t_pnr_pax_seg_meal meal1,");
			sb.append("                     t_flight f1,");
			sb.append("                     t_flight_segment fs1,");
			sb.append("                     t_pnr_segment ps1,");
			sb.append("                     t_pnr_passenger pp1");
			sb.append("               WHERE f1.flight_id = fs1.flight_id");
			sb.append("                 AND f1.flight_number = ");
			if (isNotEmptyOrNull(flightNumber)) {
				sb.append("'" + flightNumber + "' ");
			}
			sb.append("                 AND fs1.est_time_departure_local BETWEEN to_date('");
			if (isNotEmptyOrNull(depatureDate)) {
				sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
				sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			}
			sb.append("                 AND fs1.flt_seg_id = ps1.flt_seg_id");
			sb.append("                 AND meal1.pnr_seg_id = ps1.pnr_seg_id");
			sb.append("                 AND pp1.pnr_pax_id = meal1.pnr_pax_id");
			sb.append("                 AND ps1.status ! = 'CNX'");
			sb.append("                 AND meal1.status ! = 'CNX'");
			sb.append("                 GROUP BY meal1.pnr_pax_id, meal1.pnr_seg_id) meal");
		}
		if (reportsSearchCriteria.isBaggageSeleted()) {
			sb.append("             ,(SELECT baggage2.pnr_pax_id, baggage2.pnr_seg_id,");
			sb.append("                     baggage2.baggage_id");
			sb.append("                FROM bg_t_pnr_pax_seg_baggage baggage2,");
			sb.append("                     t_flight f2,");
			sb.append("                     t_flight_segment fs2,");
			sb.append("                     t_pnr_segment ps2,");
			sb.append("                     t_pnr_passenger pp2,");
			sb.append("                     bg_t_baggage bg2");
			sb.append("               WHERE f2.flight_id = fs2.flight_id");
			sb.append("                 AND f2.flight_number = ");
			if (isNotEmptyOrNull(flightNumber)) {
				sb.append("'" + flightNumber + "' ");
			}
			sb.append("                 AND fs2.est_time_departure_local BETWEEN to_date('");
			if (isNotEmptyOrNull(depatureDate)) {
				sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
				sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			}
			sb.append("                 AND fs2.flt_seg_id = ps2.flt_seg_id");
			sb.append("                 AND baggage2.pnr_seg_id = ps2.pnr_seg_id");
			sb.append("                 AND pp2.pnr_pax_id = baggage2.pnr_pax_id");
			sb.append("                 AND ps2.status ! = 'CNX'");
			sb.append("                 AND baggage2.status ! = 'CNX'");
			sb.append("                 AND baggage2.baggage_id = bg2.baggage_id");
			sb.append("                 AND bg2.baggage_weight > 0 ) bag");
		}
		if (reportsSearchCriteria.isSeatSelected()) {
			sb.append("             ,(SELECT seat3.pnr_pax_id, seat3.pnr_seg_id,");
			sb.append("                     seat3.flight_am_seat_id");
			sb.append("                FROM sm_t_pnr_pax_seg_am_seat seat3,");
			sb.append("                     t_flight f3,");
			sb.append("                     t_flight_segment fs3,");
			sb.append("                     t_pnr_segment ps3,");
			sb.append("                     t_pnr_passenger pp3");
			sb.append("               WHERE f3.flight_id = fs3.flight_id");
			sb.append("                 AND f3.flight_number = ");
			if (isNotEmptyOrNull(flightNumber)) {
				sb.append("'" + flightNumber + "' ");
			}
			sb.append("                 AND fs3.est_time_departure_local BETWEEN to_date('");
			if (isNotEmptyOrNull(depatureDate)) {
				sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
				sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			}
			sb.append("                 AND fs3.flt_seg_id = ps3.flt_seg_id");
			sb.append("                 AND seat3.pnr_seg_id = ps3.pnr_seg_id");
			sb.append("                 AND pp3.pnr_pax_id = seat3.pnr_pax_id");
			sb.append("                 AND ps3.status ! = 'CNX'");
			sb.append("                 AND seat3.status ! = 'CNX') seat");
		}
		if (reportsSearchCriteria.isHalaSerivceSelected()) {
			sb.append("             ,(SELECT ppfs4.pnr_seg_id, ppf4.pnr_pax_id, count(ppss4.ssr_id) as hala_count");
			sb.append("                FROM t_pnr_pax_segment_ssr ppss4,");
			sb.append("                     t_pnr_pax_fare_segment ppfs4,");
			sb.append("                     t_pnr_pax_fare ppf4,");
			sb.append("                     t_flight f4,");
			sb.append("                     t_flight_segment fs4,");
			sb.append("                     t_pnr_segment ps4,");
			sb.append("                     t_pnr_passenger pp4");
			sb.append("               WHERE f4.flight_id = fs4.flight_id");
			sb.append("                 AND ppss4.ppfs_id = ppfs4.ppfs_id");
			sb.append("                 AND ppfs4.ppf_id = ppf4.ppf_id");
			sb.append("                 AND f4.flight_number = ");
			if (isNotEmptyOrNull(flightNumber)) {
				sb.append("'" + flightNumber + "' ");
			}
			sb.append("                 AND fs4.est_time_departure_local BETWEEN to_date('");
			if (isNotEmptyOrNull(depatureDate)) {
				sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
				sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			}
			sb.append("                 AND fs4.flt_seg_id = ps4.flt_seg_id");
			sb.append("                 AND ps4.pnr_seg_id = ppfs4.pnr_seg_id");
			sb.append("                 AND pp4.pnr_pax_id = ppf4.pnr_pax_id");
			sb.append("                 AND ppss4.status != 'CNX'");
			sb.append("                 AND ppss4.status ! = 'CNX'");
			sb.append("                 AND ppss4.airport_code is not null");
			sb.append("                 GROUP BY ppfs4.pnr_seg_id,ppf4.pnr_pax_id) ssr");
		}
		if (reportsSearchCriteria.isFlexiSelected()) {
			sb.append("             ,(SELECT count(ppof5.flexi_rule_flexi_type_id) as flexi_count, ps5.pnr_seg_id,");
			sb.append("                     ppf5.pnr_pax_id");
			sb.append("                FROM t_flight f5,");
			sb.append("                     t_flight_segment fs5,");
			sb.append("                     t_pnr_pax_ond_flexibility ppof5,");
			sb.append("                     t_pnr_pax_fare_segment ppfs5,");
			sb.append("                     t_pnr_segment ps5,");
			sb.append("                     t_pnr_pax_fare ppf5");
			sb.append("               WHERE f5.flight_id = fs5.flight_id");
			sb.append("                 AND f5.flight_number = ");
			if (isNotEmptyOrNull(flightNumber)) {
				sb.append("'" + flightNumber + "' ");
			}
			sb.append("                 AND fs5.est_time_departure_local BETWEEN to_date('");
			if (isNotEmptyOrNull(depatureDate)) {
				sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
				sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			}
			sb.append("                 AND ppof5.ppf_id = ppfs5.ppf_id");
			sb.append("                 AND ppof5.ppf_id = ppf5.ppf_id");
			sb.append("                 AND ps5.pnr_seg_id = ppfs5.pnr_seg_id");
			sb.append("                 AND ps5.flt_seg_id = fs5.flt_seg_id");
			sb.append("                 AND ppof5.status != 'INA'");
			sb.append("                 GROUP BY ps5.pnr_seg_id, ppf5.pnr_pax_id) flexi");
		}
		if (reportsSearchCriteria.isBusSelected()) {
			sb.append("             ,(select distinct bussegments.pnr,bussegments.ground_seg_id");
			sb.append("             FROM");
			sb.append("             (select distinct r.pnr,ps.ground_seg_id");
			sb.append("             FROM t_reservation r,");
			sb.append("             t_pnr_passenger px,");
			sb.append("             t_pnr_segment ps,");
			sb.append("             t_flight_segment fs6,");
			sb.append("             t_flight f ");
			sb.append("             WHERE px.pnr= r.pnr");
			sb.append("             AND ps.pnr= r.pnr");
			sb.append("             AND fs6.flt_seg_id= ps.flt_seg_id");
			sb.append("             AND f.flight_id= fs6.flight_id");
			sb.append("             AND f.flight_number = ");
			if (isNotEmptyOrNull(flightNumber)) {
				sb.append("'" + flightNumber + "' ");
			}
			sb.append("             AND fs6.est_time_departure_local BETWEEN to_date('");
			if (isNotEmptyOrNull(depatureDate)) {
				sb.append(depatureDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') AND to_date ('");
				sb.append(depatureDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') ");
			}
			sb.append("              AND ps.status != 'CNX'");
			sb.append("              AND ps.ground_seg_id is not null) bussegments,");
			sb.append("              t_pnr_segment busps");
			sb.append("              where bussegments.ground_seg_id=busps.pnr_seg_id");
			sb.append("              AND busps.status!='CNX') bus");
		}
		if (reportsSearchCriteria.isMealSelected() || reportsSearchCriteria.isBaggageSeleted()
				|| reportsSearchCriteria.isSeatSelected() || reportsSearchCriteria.isHalaSerivceSelected()
				|| reportsSearchCriteria.isBusSelected() || reportsSearchCriteria.isFlexiSelected()) {
			sb.append(" WHERE ");
		}
		if (reportsSearchCriteria.isMealSelected()) {
			sb.append("       ins.pnr_seg_id = meal.pnr_seg_id(+)");
			sb.append("         AND ins.pnr_pax_id = meal.pnr_pax_id(+)");
			isSelected = true;
		}
		if (reportsSearchCriteria.isBaggageSeleted()) {
			if (isSelected) {
				sb.append(" AND ");
			}
			sb.append("          ins.pnr_seg_id = bag.pnr_seg_id(+)");
			sb.append("         AND bag.pnr_pax_id(+) = ins.pnr_pax_id");
			isSelected = true;
		}
		if (reportsSearchCriteria.isSeatSelected()) {
			if (isSelected) {
				sb.append(" AND ");
			}
			sb.append("         ins.pnr_seg_id = seat.pnr_seg_id(+)");
			sb.append("         AND seat.pnr_pax_id(+) = ins.pnr_pax_id");
			isSelected = true;
		}
		if (reportsSearchCriteria.isHalaSerivceSelected()) {
			if (isSelected) {
				sb.append(" AND ");
			}
			sb.append("         ins.pnr_seg_id = ssr.pnr_seg_id(+)");
			sb.append("         AND ssr.pnr_pax_id(+) = ins.pnr_pax_id");
			isSelected = true;
		}
		if (reportsSearchCriteria.isBusSelected()) {
			if (isSelected) {
				sb.append(" AND ");
			}
			sb.append("         ins.pnr = bus.pnr(+)");
			isSelected = true;
		}
		if (reportsSearchCriteria.isFlexiSelected()) {
			if (isSelected) {
				sb.append(" AND ");
			}
			sb.append("         ins.pnr_seg_id = flexi.pnr_seg_id(+)");
			sb.append("         AND flexi.pnr_pax_id(+) = ins.pnr_pax_id");
			isSelected = true;
		}
		sb.append("      ORDER BY ins.pnr DESC) vw");

		return sb.toString();
	}

	public String getPnrInvoiceReceiptReport(ReportsSearchCriteria reportsSearchCriteria) {

		String invNo = reportsSearchCriteria.getInvoiceNumber();
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT  distinct(c.segment_code), b.status_mod_date  ,c.est_time_departure_zulu ,");
		sb.append(" a.total_fare , a.TOTAL_TAX , a.total_surcharges, a.total_cnx , a.total_mod ,a.total_adj");
		sb.append(" from  t_reservation a, t_pnr_segment b,  t_flight_segment c, t_invoice inv");
		sb.append(" WHERE a.pnr = b.pnr  AND b.flt_seg_id = c.flt_seg_id  and a.pnr = inv.pnr  AND ");
		sb.append("inv.invoice_number =  ");
		sb.append("'" + invNo + "' ");

		return sb.toString();
	}

	/**
	 * @added by Harsha Udayapriya Creates the SQL for Segment Loyalty Points Report
	 * @param reportsSearchCriteria
	 */
	public String getLoyaltyPointsDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String accountNo = reportsSearchCriteria.getAccountNumber();
		String reportType = reportsSearchCriteria.getReportType();
		String nominalCode = "ONACCOUNT_PAYMENT";

		StringBuilder sb = new StringBuilder();
		if (isNotEmptyOrNull(reportType) && reportType.equals("CRE_SUM")) {
			sb.append(" SELECT lcp.loyalty_account_no, lower(cus.email_id) as aaprofile , lcp.city, cntry.country_name, lcp.mobile, lower(lcp.email) email, ");
			sb.append(" na.description nationality, lc.credit_Balance credit, to_char(lc.date_exp ,'dd/mm/yy hh:mi') date_exp ");
			sb.append(" FROM t_loyalty_credit lc, t_loyalty_customer_profile lcp, t_customer cus, t_nationality na, t_country cntry ");
			sb.append(" WHERE lc.loyalty_account_no ");
			if (!reportsSearchCriteria.isByCredit()) {
				sb.append(" (+) ");
			}
			sb.append(" = lcp.loyalty_account_no ");
			sb.append(" AND cus.customer_id ");
			if (!reportsSearchCriteria.isByCredit()) {
				sb.append(" (+) ");
			}
			sb.append(" = lcp.customer_id  ");
			sb.append(" AND lcp.nationality_code = na.nationality_code ");
			sb.append(" AND cntry.country_code = lcp.country_code ");
			if (isNotEmptyOrNull(accountNo)) {
				sb.append(" AND lc.loyalty_account_no = '" + accountNo + "' ");
			} else {
				if (reportsSearchCriteria.isByCredit()) {
					if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
						sb.append(" AND lc.date_exp between to_date(" + fromDate + ")  ");
						sb.append(" AND TO_DATE( " + toDate + " ) ");
					} else {
						sb.append(" and lc.credit_Balance is not null and lc.credit_Balance != 0 ");
					}
				}
			}
			sb.append(" order by lcp.loyalty_account_no ");
		} else {
			sb.append(" SELECT lcp.loyalty_account_no, c.email_id, r.pnr, ");
			sb.append(" pp.first_name || ' ' || pp.last_name AS pax_name, ");
			sb.append(" to_char(pt.tnx_date) consumed_date, ABS (pt.amount) amount_consumed,pt.agent_code ");
			sb.append(" FROM t_pax_transaction pt, t_pnr_passenger pp,t_reservation r, t_reservation_contact rCon, ");
			sb.append(" t_customer c, t_loyalty_customer_profile lcp ");
			sb.append(" WHERE r.pnr = pp.pnr AND pp.pnr_pax_id = pt.pnr_pax_id AND r.pnr=rCon.pnr ");
			sb.append(" AND rCon.customer_id = c.customer_id AND c.customer_id = lcp.customer_id ");
			sb.append(" AND pt.nominal_code = (SELECT ptnc.nominal_code FROM t_pax_trnx_nominal_code ptnc ");
			sb.append(" where ptnc.description = '" + nominalCode + "') ");
			sb.append(" AND pt.agent_code IN (SELECT param_value FROM t_app_parameter WHERE param_key = 'RES_84') ");
			sb.append(" AND pt.amount <> 0 AND pt.tnx_date BETWEEN TO_DATE ( ");
			if (isNotEmptyOrNull(fromDate)) {
				sb.append(fromDate);
			}
			sb.append(" ) AND TO_DATE ( ");
			if (isNotEmptyOrNull(toDate)) {
				sb.append(toDate);
			}
			sb.append(" ) ");
			if (isNotEmptyOrNull(accountNo)) {
				sb.append(" AND lcp.loyalty_account_no = '" + accountNo + "'");
			}
		}

		return sb.toString();
	}

	/**
	 * @added by Harsha Creates the SQL for Ancillary Reveneue Report
	 * @param reportsSearchCriteria
	 */
	public String getAncillaryRevenueQuery(ReportsSearchCriteria search) {
		String agents = getSingleDataValue(search.getAgents());
		String fromDate = search.getDateRangeFrom();
		String toDate = search.getDateRangeTo();
		StringBuffer sb = new StringBuffer();
		String reportOpt = search.getReportOption();

		String betweenTime = " BETWEEN TO_DATE ( " + fromDate + ") AND TO_DATE (" + toDate + ") ";

		if (search.isByCustomer()) {// exclude web sales
			sb.append(" SELECT   a.agent_code, a.agent_name,  ");
			if (isNotEmptyOrNull(reportOpt) && reportOpt.equals("AGENT")) {// agent wise
				sb.append("  null user_id, null user_name, ");
			} else { // agent user wise
				sb.append("  u.user_id, u.DISPLAY_NAME user_name, ");
			}
		} else {
			sb.append(" SELECT nvl(a.agent_code,DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',a.agent_code)) agent_code, ");
			sb.append(" nvl(a.agent_code,DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',a.agent_name)) agent_name, ");
			if (isNotEmptyOrNull(reportOpt) && reportOpt.equals("AGENT")) {// agent wise
				sb.append(" NVL (u.user_id, '') user_id, NVL  (u.display_name, '') user_name, ");
			} else { // agent user wise
				sb.append(" NVL (u.user_id, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',u.user_id)) user_id, ");
				sb.append(" NVL  (u.display_name, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',u.display_name)) user_name, ");
			}
		}
		sb.append("	SUM (ins_count) insurance_sold, SUM (ins_amount) insurance_value, SUM (seat_count) seats_sold, ");
		sb.append(" SUM (seat_amount) seat_value, SUM (meal_count) meals_sold, ");
		sb.append(" SUM (meal_amount) meals_value ");
		sb.append(" , SUM (baggage_count) baggages_sold, SUM (baggage_amount) baggages_value, ");
		sb.append(" SUM (-1 * discount_amount) discount_amount ");
		sb.append(" FROM ( SELECT  ");
		if (search.isByCustomer()) {
			sb.append(" user_id,  ");
		} else {
			sb.append(" NVL (user_id, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',user_id)) user_id, ");
		}
		sb.append(" COUNT (DISTINCT policy_code) ins_count,	 ");
		sb.append(" SUM (amount) ins_amount, 0 seat_count, 0 seat_amount, ");
		sb.append(" 0 meal_count, 0 meal_amount, ");
		sb.append(" 0 baggage_count, 0 baggage_amount, 0 discount_amount ,SALES_CHANNEL_CODE");
		sb.append(" FROM t_pnr_insurance ins WHERE ins.state = 'SC' ");
		sb.append("  AND ins.sell_timestamp ");
		sb.append(betweenTime);
		if (search.isByCustomer()) {
			sb.append(" AND ins.user_id IS NOT NULL ");
		}
		sb.append(" AND ins.pnr IN ( SELECT r.pnr ");
		sb.append(" FROM t_reservation r ");
		sb.append(" WHERE r.status = 'CNF' ) ");
		sb.append(" GROUP BY user_id,SALES_CHANNEL_CODE ");
		sb.append(" UNION SELECT  ");
		if (search.isByCustomer()) {
			sb.append(" user_id,  ");
		} else {
			sb.append(" NVL (user_id, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',user_id)) user_id, ");
		}
		sb.append(" 0, 0, COUNT (DISTINCT flight_am_seat_id) seat_count,  ");
		sb.append(" SUM (charge_amount) seat_amount, 0, 0, 0, 0, 0 ,SALES_CHANNEL_CODE");
		sb.append(" FROM sm_t_pnr_pax_seg_am_seat sm WHERE sm.status = 'RES' ");
		sb.append(" AND sm.timestamp ");
		sb.append(betweenTime);
		if (search.isByCustomer()) {
			sb.append(" AND sm.user_id IS NOT NULL ");
		}
		sb.append(" AND sm.pnr_pax_id IN ( ");
		sb.append(" SELECT pax.pnr_pax_id FROM t_pnr_passenger pax WHERE pax.pnr IN ( ");
		sb.append(" SELECT r.pnr FROM t_reservation r ");
		sb.append(" WHERE r.status = 'CNF' )) ");
		sb.append(" GROUP BY user_id ,SALES_CHANNEL_CODE  UNION SELECT  ");
		if (search.isByCustomer()) {
			sb.append(" user_id,  ");
		} else {
			sb.append(" NVL (user_id, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',user_id)) user_id, ");
		}
		sb.append(" 0, 0, 0, 0, COUNT (meal_id) meal_count, ");
		sb.append(" SUM (amount) meal_amount, 0, 0, 0 ,SALES_CHANNEL_CODE FROM ml_t_pnr_pax_seg_meal ml ");
		sb.append(" WHERE ml.status = 'RES' ");
		sb.append(" AND ml.timestamp ");
		sb.append(betweenTime);
		if (search.isByCustomer()) {
			sb.append(" AND ml.user_id IS NOT NULL ");
		}
		sb.append(" AND ml.pnr_pax_id IN ( ");
		sb.append(" SELECT pax.pnr_pax_id FROM t_pnr_passenger pax WHERE pax.pnr IN ( ");
		sb.append(" SELECT r.pnr FROM t_reservation r ");
		sb.append(" WHERE r.status = 'CNF' )) ");

		sb.append(" GROUP BY user_id ,SALES_CHANNEL_CODE UNION SELECT  ");
		if (search.isByCustomer()) {
			sb.append(" origin_user_id as user_id,  ");
		} else {
			sb.append(" NVL (origin_user_id, DECODE (ORIGIN_SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',origin_user_id)) user_id, ");
		}
		sb.append(" 0, 0, 0, 0, 0, 0, COUNT (bgps.baggage_id) baggage_count, ");
		sb.append(" SUM (amount) baggage_amount, 0 ,ORIGIN_SALES_CHANNEL_CODE as SALES_CHANNEL_CODE FROM bg_t_pnr_pax_seg_baggage bgps, bg_t_baggage bg ");
		sb.append(" WHERE bgps.baggage_id = bg.baggage_id ");
		sb.append(" AND bg.baggage_weight > 0 ");
		sb.append(" AND bgps.status = 'RES' ");
		sb.append(" AND bgps.created_time ");
		sb.append(betweenTime);
		if (search.isByCustomer()) {
			sb.append(" AND bgps.origin_user_id IS NOT NULL ");
		}
		sb.append(" AND bgps.pnr_pax_id IN ( ");
		sb.append(" SELECT pax.pnr_pax_id FROM t_pnr_passenger pax WHERE pax.pnr IN ( ");
		sb.append(" SELECT r.pnr FROM t_reservation r ");
		sb.append(" WHERE r.status = 'CNF' )) ");
		sb.append(" GROUP BY origin_user_id ,ORIGIN_SALES_CHANNEL_CODE");
		sb.append(" UNION SELECT NVL (r.origin_user_id, DECODE (r.ORIGIN_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',r.origin_user_id)) AS user_id, ");
		sb.append(" 0, 0, 0, 0, 0, 0, 0, 0,  SUM (ppoc.amount * -1) discount_amount ,r.ORIGIN_CHANNEL_CODE as SALES_CHANNEL_CODE ");
		sb.append(" FROM t_reservation r, T_PNR_PASSENGER pp, t_pnr_pax_fare ppf, t_pnr_pax_ond_charges ppoc, t_charge_rate cr ");
		sb.append(" WHERE r.pnr=pp.pnr AND pp.pnr_pax_id=ppf.pnr_pax_id AND ppf.ppf_id=ppoc.ppf_id ");
		sb.append(" AND cr.charge_rate_id=ppoc.charge_rate_id AND cr.charge_code='" + ChargeCodes.FREE_SERVICE_PROMO + "' ");
		sb.append(" AND r.booking_timestamp BETWEEN TO_DATE ( ");
		if (isNotEmptyOrNull(fromDate)) {
			sb.append(fromDate);
		}
		sb.append(" ) AND TO_DATE ( ");
		if (isNotEmptyOrNull(toDate)) {
			sb.append(toDate);
		}
		sb.append(" ) and r.status = 'CNF' ");
		sb.append("  GROUP BY origin_user_id,r.ORIGIN_CHANNEL_CODE)");

		sb.append(" RESULT, ");
		sb.append(" t_agent a, t_user u  ");
		if (search.isByCustomer()) {
			sb.append(" WHERE a.agent_code = u.agent_code AND u.user_id = RESULT.user_id ");
		} else {
			sb.append(" WHERE a.agent_code(+) = u.agent_code AND u.user_id(+) = RESULT.user_id ");
		}
		if (!search.isSelectedAllAgents() && isNotEmptyOrNull(agents) || !search.isByCustomer()) {
			sb.append("and ( ");

			if (!search.isSelectedAllAgents() && isNotEmptyOrNull(agents)) {
				sb.append(ReportUtils.getReplaceStringForIN("A.AGENT_CODE", search.getAgents(), false));
			}

		}
		if (!search.isByCustomer()) {
			sb.append(" OR RESULT.user_id in('WEB','ANDROID','IOS') ");
		}
		sb.append(" ) ");
		if (search.isByCustomer()) {
			sb.append(" GROUP BY a.agent_code, a.agent_name ");
			if (isNotEmptyOrNull(reportOpt) && !reportOpt.equals("AGENT")) {
				sb.append(" , u.user_id, u.display_name ");
			}
		} else {
			sb.append(" GROUP BY nvl(a.agent_code,DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',a.agent_code)), ");
			sb.append(" nvl(a.agent_code,DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',a.agent_name)) ");
			if (isNotEmptyOrNull(reportOpt) && reportOpt.equals("AGENT")) {
				sb.append("  ,NVL (u.user_id, ''), NVL  (u.display_name, '') ");
			} else {
				sb.append(" ,NVL (u.user_id, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',u.user_id)), ");
				sb.append(" NVL (u.display_name, DECODE (SALES_CHANNEL_CODE,'4','WEB','20','IOS','21','ANDROID',u.display_name)) ");
			}
		}
		sb.append(" ORDER BY 1, 2, 3, 4 ");

		return sb.toString();
	}

	/**
	 * Creates a SQL for AdvancedAncillaryRevenueReport
	 * 
	 * @param ReportsSearchCriteria
	 */
	public String getAdvancedAncillaryRevenueReportQuery(ReportsSearchCriteria search) {

		String reportOpt = search.getReportOption();
		boolean searchByUser = isNotEmptyOrNull(reportOpt) && !reportOpt.equals("AGENT");
		String stations = getSingleDataValue(search.getStations());
		// String agents = getSingleDataValue(search.getAgents());

		String departureDateFrom = search.getDepartureDateRangeFrom();
		String departureDateRanceTo = search.getDepartureDateRangeTo();

		String salesDateFrom = search.getDateRangeFrom();
		String salesDateTo = search.getDateRangeTo();

		boolean linkSalesDates = false;
		boolean linkFlightDates = false;
		boolean includeWebSales = false;

		if (isNotEmptyOrNull(salesDateFrom) && isNotEmptyOrNull(salesDateTo)) {
			linkSalesDates = true;
		}

		if (isNotEmptyOrNull(departureDateFrom) && isNotEmptyOrNull(departureDateRanceTo)) {
			linkFlightDates = true;
		}
		
		if (search.getSalesChannels().contains(String.valueOf(SalesChannelsUtil.SALES_CHANNEL_WEB))
				|| search.getSalesChannels().contains(String.valueOf(SalesChannelsUtil.SALES_CHANNEL_IOS))
				|| search.getSalesChannels().contains(String.valueOf(SalesChannelsUtil.SALES_CHANNEL_ANDROID))) {
			includeWebSales = true;
		}

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT NVL(RES.agent_code , DECODE(res.ORIGIN_CHANNEL_CODE, " + SalesChannelsUtil.SALES_CHANNEL_WEB
				+ " ,'WEB'," + SalesChannelsUtil.SALES_CHANNEL_IOS + ",'IOS'," + SalesChannelsUtil.SALES_CHANNEL_ANDROID
				+ ",'ANDROID','WEB-OT')) AS agent_code   ,");
		if (searchByUser) {
			sb.append("  nvl( RES.user_id, DECODE(res.ORIGIN_CHANNEL_CODE," + SalesChannelsUtil.SALES_CHANNEL_WEB + ",'WEB-USER',"
					+ SalesChannelsUtil.SALES_CHANNEL_IOS + ",'IOS-USER'," + SalesChannelsUtil.SALES_CHANNEL_ANDROID
					+ ",'ANDROID-USER','WEB-USER-OT')) as user_id      ,");
			sb.append("  nvl( u.display_name, DECODE(res.ORIGIN_CHANNEL_CODE," + SalesChannelsUtil.SALES_CHANNEL_WEB
					+ ",'WEB-USER'," + SalesChannelsUtil.SALES_CHANNEL_IOS + ",'IOS-USER',"
					+ SalesChannelsUtil.SALES_CHANNEL_ANDROID + ",'ANDROID-USER','WEB-USER-OT')) as user_name ,");
		} else {
			sb.append("  NULL as user_id      						,");
			sb.append("  NULL as user_name    						,");
		}

		sb.append("  NVL(agnt.agent_name , DECODE(res.ORIGIN_CHANNEL_CODE, " + SalesChannelsUtil.SALES_CHANNEL_WEB + " ,'WEB',"
				+ SalesChannelsUtil.SALES_CHANNEL_IOS + ",'IOS'," + SalesChannelsUtil.SALES_CHANNEL_ANDROID
				+ ",'ANDROID','WEB-OT')) AS agent_name   ,");
		sb.append(" SUM((select count(policy_code) " + " FROM t_pnr_insurance where  pnr=res.pnr AND state='SC'"
				+ " AND ((user_id    =RES.user_id AND RES.user_id IS NOT NULL) "
				+ " OR (user_id IS NULL  AND RES.user_id IS NULL))" + "))  AS insurance_sold ,");
		sb.append("  SUM(ins_amount)                AS insurance_value ,");
		sb.append("  SUM(seat_count)                AS seats_sold      ,");
		sb.append("  SUM(seat_amount)               AS seat_value      ,");
		sb.append("  SUM(meal_count)                AS meals_sold      ,");
		sb.append("  SUM(meal_amount)               AS meals_value     ,");
		sb.append("  SUM(baggage_count)             AS baggages_sold   ,");
		sb.append("  SUM(baggage_amount)            AS baggages_value  ,");
		sb.append("  SUM(ssr_count)                 AS ssr_sold        ,");
		sb.append("  SUM(ssr_amount)                AS ssr_value       ,");
		sb.append("  SUM(hala_count)                AS hala_sold       ,");
		sb.append("  SUM(hala_amount)               AS hala_value      ,");
		sb.append("  SUM(apt_transfer_count)        AS apt_transfer_sold, ");
		sb.append("  SUM(apt_transfer_amount)       AS apt_transfer_value ");
		sb.append("   FROM ( ");
		sb.append("   SELECT        ");
		sb.append("      res.pnr,poc.agent_code,poc.user_id ");

		// insurance
		sb.append(" , SUM ( decode(poc.charge_rate_id,"
				+ decodeArgumentComposer(search.getLstInsuranceCodes(), 1, " ppsc.amount", null) + ", 0)) ins_amount ,  ");

		// seat
		/**
		 * sb.append(" (SELECT COUNT(ppsam_seat_id) FROM sm_t_pnr_pax_seg_am_seat " + " WHERE PNR_PAX_ID IN(SELECT
		 * PNR_PAX_ID FROM T_PNR_PASSENGER WHERE PNR=RES.PNR) " + " and STATUS = 'RES' " + " AND ((user_id =poc.user_id
		 * AND poc.user_id is not null) OR (user_id is null AND poc.user_id is null)) ) seat_count , " );
		 **/
		sb.append(" SUM ( CASE WHEN "
				+ decodeArgumentComposer(search.getLstSeatMapCodes(), 0, "poc.charge_rate_id", "ppsc.amount") + ") seat_count ,");

		sb.append(" SUM ( decode(poc.charge_rate_id,"
				+ decodeArgumentComposer(search.getLstSeatMapCodes(), 1, " ppsc.amount", null) + ",0)) seat_amount, ");

		// meal
		/**
		 * sb.append(" (SELECT COUNT(pnr_pax_seg_meal_id) FROM  ml_t_pnr_pax_seg_meal " +
		 * " WHERE PNR_PAX_ID IN(SELECT PNR_PAX_ID FROM T_PNR_PASSENGER WHERE PNR=RES.PNR) " + " and STATUS = 'RES' " +
		 * " AND ((user_id =poc.user_id AND poc.user_id is not null) OR (user_id is null AND poc.user_id is null))) meal_count,"
		 * );
		 **/
		sb.append(" SUM ( CASE WHEN " + decodeArgumentComposer(search.getLstmealCodes(), 0, "poc.charge_rate_id", "ppsc.amount")
				+ ") meal_count ,");

		sb.append(" SUM ( decode(poc.charge_rate_id," + decodeArgumentComposer(search.getLstmealCodes(), 1, " ppsc.amount", null)
				+ ",0)) meal_amount,");

		// baggage
		/**
		 * sb.append("(SELECT COUNT(PNR_PAX_SEG_BAGGAGE_ID) FROM  bg_t_pnr_pax_seg_baggage " +
		 * " WHERE PNR_PAX_ID IN(SELECT PNR_PAX_ID FROM T_PNR_PASSENGER WHERE PNR=RES.PNR) " +
		 * " and STATUS = 'RES' and amount >0 " +
		 * " AND ((origin_user_id =poc.user_id AND poc.user_id is not null) OR (origin_user_id is null AND poc.user_id is null)) ) baggage_count ,"
		 * );
		 **/

		sb.append(" SUM ( CASE WHEN "
				+ decodeArgumentComposer(search.getLstBaggageCodes(), 0, "poc.charge_rate_id", "ppsc.amount")
				+ ") baggage_count ,");

		sb.append("SUM ( decode(poc.charge_rate_id,"
				+ decodeArgumentComposer(search.getLstBaggageCodes(), 1, " ppsc.amount", null) + ",0)) baggage_amount, ");

		// ssr

		sb.append(" SUM ( CASE WHEN " + decodeArgumentComposer(search.getLstSSRCodes(), 0, "poc.charge_rate_id", "ppsc.amount")
				+ ") ssr_count ," + "SUM ( decode(poc.charge_rate_id,"
				+ decodeArgumentComposer(search.getLstSSRCodes(), 1, " ppsc.amount", null) + ",0)) ssr_amount, ");

		// hala

		sb.append(" SUM ( CASE WHEN " + decodeArgumentComposer(search.getLstHALACodes(), 0, "poc.charge_rate_id", "ppsc.amount")
				+ ") hala_count ," + "SUM ( decode(poc.charge_rate_id,"
				+ decodeArgumentComposer(search.getLstHALACodes(), 1, " ppsc.amount", null) + ",0)) hala_amount,  ");

		// airport tranfer

		sb.append(" SUM ( CASE WHEN "
				+ decodeArgumentComposer(search.getLstAptTransferCodes(), 0, "poc.charge_rate_id", "ppsc.amount")
				+ ") apt_transfer_count , SUM ( decode(poc.charge_rate_id,"
				+ decodeArgumentComposer(search.getLstAptTransferCodes(), 1, " ppsc.amount", null) + ",0)) apt_transfer_amount , res.ORIGIN_CHANNEL_CODE");

		sb.append(" FROM ");
		if (linkFlightDates) {
			sb.append(" t_flight_segment flgseg        ,");
		}
		sb.append("    t_pnr_segment pnrseg              ,");
		sb.append("    t_pnr_pax_ond_charges poc         ,");
		sb.append("    T_PNR_PAX_OND_PAYMENTS ppop       ,");
		sb.append("    t_reservation res                 ,");
		sb.append("    t_pnr_passenger pax               ,");
		sb.append("    t_pnr_pax_fare ppf                ,");
		sb.append("    t_pnr_pax_fare_segment PPFS       ,");
		sb.append("    t_pnr_pax_seg_charges ppsc        ,");
		sb.append("    t_pnr_pax_seg_payments ppsp        ");
		sb.append("    WHERE poc.pft_id  = ppop.pft_id    ");
		sb.append("  AND ppop.pnr_pax_id = pax.pnr_pax_id ");
		sb.append("  AND pax.pnr         = res.pnr        ");
		sb.append("  AND res.pnr         = pnrseg.pnr     ");
		sb.append("  AND ppf.pnr_pax_id  = pax.pnr_pax_id    ");
		sb.append("  AND PPF.PPF_ID      = PPFS.PPF_ID    ");
		sb.append("  AND pnrseg.PNR_SEG_ID= PPFS.PNR_SEG_ID    ");
		sb.append("  AND ppsc.ppfs_id     =PPFS.ppfs_id  ");
		sb.append("  AND ppsc.ppfs_id     =PPFS.ppfs_id    ");
		sb.append("  AND ppsc.pft_id    = poc.pft_id    ");
		sb.append("  AND ppsp.ppop_id 	= ppop.ppop_id    ");
		sb.append("  AND ppsc.PFST_ID 	= ppsp.PFST_ID    ");

		if (linkFlightDates) {
			sb.append(" AND pnrseg.flt_seg_id  = flgseg.flt_seg_id ");
			sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE(");
			sb.append(departureDateFrom);
			sb.append(" ) AND TO_DATE( ");
			sb.append(departureDateRanceTo);
			sb.append(" )  ");
		}
		sb.append(" AND ");
		sb.append((ReportUtils.getReplaceStringForIN("pnrseg.STATUS_MOD_CHANNEL_CODE", search.getSalesChannels(), false) + "  "));

		if ((search.getAgents() != null) && (search.getAgents().size() > 0)) {
			sb.append(" AND ( ");
			sb.append(composeInQuery("poc.agent_code", search.getAgents()));
			if (includeWebSales) {
				sb.append(" OR poc.agent_code is null ");
			}
			sb.append(" ) ");
		}

		sb.append(" AND poc.charge_rate_id   " + "IN (" + getSingleDataValue(search.getLstInsuranceCodes()) + ","
				+ getSingleDataValue(search.getLstSeatMapCodes()) + "," + getSingleDataValue(search.getLstBaggageCodes()) + ","
				+ getSingleDataValue(search.getLstmealCodes()) + "," + getSingleDataValue(search.getLstSSRCodes()) + ","
				+ getSingleDataValue(search.getLstHALACodes()) + "," + getSingleDataValue(search.getLstAptTransferCodes()) + ")");

		sb.append(" AND res.status <>  '" + ReservationInternalConstants.ReservationStatus.CANCEL + "'");
		sb.append(" AND pnrseg.status <>  '" + ReservationInternalConstants.ReservationSegmentStatus.CANCEL + "'");

		if (linkSalesDates) {
			sb.append(" AND poc.charge_date BETWEEN TO_DATE( ");
			sb.append(salesDateFrom);
			sb.append(" ) AND TO_DATE( ");
			sb.append(salesDateTo);
			sb.append(" )  ");
		}

		sb.append("  GROUP BY poc.agent_code,res.pnr,poc.user_id ,res.ORIGIN_CHANNEL_CODE ");

		sb.append("    ) RES       ,");
		sb.append("  t_agent agnt       ");

		if (searchByUser) {
			sb.append("  , t_user u       ");
		}

		sb.append(" WHERE  RES.agent_code = agnt.agent_code ");
		if (includeWebSales) {
			sb.append("(+) ");
		}
		sb.append(" AND ( ");

		if (stations.length() > 0) {
			sb.append("  agnt.station_code in (" + stations + ") ");
		}
		if (includeWebSales) {
			if (stations.length() > 0) {
				sb.append(" OR ");
			}
			sb.append(" agnt.station_code is null ");
		}
		sb.append(" ) ");

		if (searchByUser) {
			sb.append("    AND RES.user_id = u.user_id(+) ");
		}

		sb.append(" GROUP BY RES.agent_code,agnt.agent_name ,res.ORIGIN_CHANNEL_CODE ");

		if (searchByUser) {
			sb.append(" , RES.user_id,u.display_name ");
		}

		return sb.toString();
	}

	private String decodeArgumentComposer(List<String> parameters, int option, String value1, String value2) {
		StringBuffer sbData = new StringBuffer();
		StringBuffer sbMinusData = new StringBuffer();
		String data = null;
		ListIterator<String> it = null;

		if ((parameters != null) && (!parameters.isEmpty())) {

			it = parameters.listIterator();

			if (option == 0) {
				if (value1 != null && value2 != null) {
					String chargeCode = null;
					sbData.append("(");
					sbMinusData.append("WHEN (");
					while (it.hasNext()) {
						chargeCode = null;
						if (it.hasNext() && it.previousIndex() > -1) {
							sbData.append(" OR ");
							sbMinusData.append(" OR ");
						}
						sbData.append(" ( ");
						sbMinusData.append(" ( ");
						sbData.append(value1);
						sbMinusData.append(value1);
						sbData.append(" = ");
						sbMinusData.append(" = ");
						sbData.append(" ' ");
						sbMinusData.append(" ' ");
						chargeCode = it.next();
						sbData.append(chargeCode);
						sbMinusData.append(chargeCode);
						sbData.append(" ' ");
						sbMinusData.append(" ' ");
						sbData.append(" AND ");
						sbMinusData.append(" AND ");
						sbData.append(value2);
						sbMinusData.append(value2);
						sbData.append(" > 0 ");
						sbMinusData.append(" < 0 ");
						sbData.append(" ) ");
						sbMinusData.append(" ) ");
					}
					sbData.append(")");
					sbMinusData.append(")");
					sbData.append(" THEN 1 ");
					sbMinusData.append(" THEN -1 ");
					sbData.append(sbMinusData);
					sbData.append("ELSE 0 END");
				}

			} else if (option == 1) {
				if (value1 != null) {

					while (it.hasNext()) {
						sbData.append("'");
						sbData.append(it.next());
						sbData.append("'");
						sbData.append(",");
						sbData.append(value1);

						if (it.hasNext()) {
							sbData.append(",");
						}
					}
				}
			}

		}
		return sbData.toString();

	}

	private String composeInQuery(String field, Collection<String> items) {
		StringBuffer sb = new StringBuffer();
		List<String> itemList = new ArrayList<String>(items);

		while (itemList.size() > 100) {
			List<String> subList = itemList.subList(0, 100);
			itemList = itemList.subList(101, itemList.size());
			sb.append(" " + field + " IN ( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (itemList.size() <= 100) {
			sb.append(" " + field + " IN ( " + Util.buildStringInClauseContent(itemList) + " ) ");
		}
		return sb.toString();
	}

	public String getCurrencyConversionDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String currencies = reportsSearchCriteria.getCurrencies();
		String[] currencyArray = null;
		StringBuffer sbCurrency = new StringBuffer();
		if (isNotEmptyOrNull(currencies)) {
			currencyArray = currencies.trim().split(",");
		}
		for (int i = 0; i < currencyArray.length; i++) {
			sbCurrency.append("'");
			sbCurrency.append(currencyArray[i]);
			sbCurrency.append("'");

			if (i != currencyArray.length - 1) {
				sbCurrency.append(",");
			}
		}
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String status = reportsSearchCriteria.getStatus();
		String xbeVisibility = reportsSearchCriteria.getxBEVisibililty();
		String ibeVisibility = reportsSearchCriteria.getiBEVisibililty();

		sb.append(" SELECT c.currency_code, c.currency_descriprion, DECODE(c.xbe_visiblity,1 , 'Y', 'N') xbe_visiblity, DECODE(c.ibe_visiblity,1 , 'Y', 'N') ibe_visiblity, ");
		sb.append(" DECODE(c.cc_payment_visibility,1 , 'Y', 'N') cc_payment_visibility, c.status as curr_status, TO_CHAR(cer.effective_from,'DD/mm/YYYY HH24:mi:ss') AS effective_from,");
		sb.append(" TO_CHAR(cer.effective_to, 'DD/mm/YYYY HH24:mi:ss') AS effective_to, cer.exrate_base_to_cur sales_exchange_rate, cer.exrate_cur_to_base purchase_exchange_rate, cer.status, ");
		sb.append(" (SELECT st.exrate_base_to_cur FROM t_currency_exchange_rate st WHERE SYSDATE BETWEEN st.effective_from AND st.effective_to AND st.currency_code = c.currency_code) AS current_rate,");
		sb.append(" DECODE(c.currency_code, (SELECT param_value FROM t_app_parameter WHERE param_key = 'AIRLINE_4'), 'Y', 'N') base_curr ");
		sb.append(" FROM  t_currency c, t_currency_exchange_rate cer");
		sb.append(" WHERE c.currency_code = cer.currency_code");
		sb.append(" AND c.currency_code in");
		sb.append(" (" + sbCurrency.toString() + ") ");
		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			sb.append(" AND (TO_DATE('");
			sb.append(fromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss') BETWEEN cer.effective_from AND cer.effective_to OR ");
			sb.append(" TO_DATE('");
			sb.append(toDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss') BETWEEN cer.effective_from AND cer.effective_to OR ");
			sb.append(" (TO_DATE('");
			sb.append(fromDate + " 00:00:00','dd-mon-yyyy hh24:mi:ss')<cer.effective_from AND ");
			sb.append(" TO_DATE('");
			sb.append(toDate + " 23:59:59','dd-mon-yyyy hh24:mi:ss')>cer.effective_to)) ");
		} else {
			sb.append(" AND SYSDATE BETWEEN cer.effective_from AND cer.effective_to ");
		}
		if (status != null && status.trim().length() == 0) {
			sb.append(" AND  (c.status = ");
			sb.append(" '" + com.isa.thinair.airmaster.api.model.Currency.STATUS_ACTIVE + "' OR ");
			sb.append(" c.status = ");
			sb.append(" '" + com.isa.thinair.airmaster.api.model.Currency.STATUS_INACTIVE + "' )");
		} else {
			sb.append(" AND  c.status = ");
			sb.append(" '" + status + "' ");
		}
		if (isNotEmptyOrNull(xbeVisibility)) {
			sb.append(" AND c.xbe_visiblity = ");
			sb.append(xbeVisibility);
		}
		if (isNotEmptyOrNull(ibeVisibility)) {
			sb.append(" AND c.ibe_visiblity = ");
			sb.append(ibeVisibility);
		}
		sb.append(" ORDER BY c.currency_code ASC, cer.effective_from  DESC");

		return sb.toString();
	}

	/**
	 * @added by Harsha Creates the SQL for Airport Tax Report
	 * @param reportsSearchCriteria
	 */
	public String getAirportTaxReportQuery(ReportsSearchCriteria search) {
		String fromDateDep = search.getDateRangeFrom();
		String toDateDep = search.getDateRangeTo();
		String fromDateBook = search.getFrom();
		String toDateBook = search.getTo();
		ArrayList<String> segCodes = (ArrayList<String>) search.getSegmentCodes();
		StringBuffer sb = new StringBuffer();

		sb.append(" SELECT DISTINCT pp.pnr, TO_CHAR (fs.est_time_departure_local,'DD/mm/YY HH24:mi:ss') AS flight_dep_local, ");
		sb.append(" INITCAP (pp.title || ' ' || pp.first_name || ' '|| pp.last_name) contact_name, fs.segment_code,");
		sb.append(" TO_CHAR (res.booking_timestamp,'DD/mm/YY HH24:mi:ss') AS booked_time,fl.flight_number ");
		sb.append(" FROM t_pnr_segment ps,t_flight_segment fs, t_pnr_passenger pp, t_pnr_pax_fare ppf, ");
		sb.append(" t_pnr_pax_fare_segment ppfs, t_flight fl, t_reservation res ");
		sb.append(" WHERE ");
		if (isNotEmptyOrNull(fromDateDep) && isNotEmptyOrNull(toDateDep)) {
			sb.append(" fs.est_time_departure_local BETWEEN TO_DATE ( ");
			sb.append(fromDateDep);
			sb.append(" ) AND TO_DATE ( ");
			sb.append(toDateDep);
			sb.append(") ");
		}
		if (isNotEmptyOrNull(fromDateBook) && isNotEmptyOrNull(toDateBook)) {
			sb.append(" and ps.status_mod_date BETWEEN TO_DATE ( ");
			sb.append(fromDateBook);
			sb.append(" ) AND TO_DATE ( ");
			sb.append(toDateBook);
			sb.append(") ");
		} else if (isNotEmptyOrNull(fromDateBook) && !isNotEmptyOrNull(toDateBook)) {
			sb.append(" AND ps.status_mod_date < TO_DATE( ");
			sb.append(fromDateBook);
			sb.append(" ) ");
		}
		sb.append(" AND ps.flt_seg_id = fs.flt_seg_id AND ps.pnr = pp.pnr AND pp.pnr = res.pnr ");
		sb.append(" AND pp.pnr_pax_id = ppf.pnr_pax_id AND ps.pnr_seg_id = ppfs.pnr_seg_id ");
		sb.append(" AND ppf.ppf_id = ppfs.ppf_id AND fs.flight_id = fl.flight_id ");
		sb.append(" AND EXISTS ( SELECT 'x' FROM t_ond_fare tof WHERE tof.fare_id = ppf.fare_id ");
		sb.append(" AND tof.fare_id NOT IN (SELECT y.fare_id FROM t_ond_fare y WHERE y.ond_code LIKE '%/SHJ/%')) ");
		sb.append(" AND fs.status <> 'CNX' AND ps.status = 'CNF' AND pp.status = 'CNF' ");
		sb.append(" AND res.status = 'CNF' AND pp.pax_type_code <> 'IN' ");

		if (segCodes != null && !segCodes.isEmpty()) {
			sb.append(" AND fs.segment_code IN ( ");
			sb.append(Util.buildStringInClauseContent(segCodes));
			sb.append(")");
		}
		sb.append(" AND fs.segment_code LIKE 'SHJ/%' ");
		sb.append(" ORDER BY pp.pnr ");

		return sb.toString();
	}

	/**
	 * @added by Harsha Creates the SQL for Adjustment/Correction Audit Report
	 * @param reportsSearchCriteria
	 */
	public String getAdjustmentAuditReportQuery(ReportsSearchCriteria search) {
		String fromDate = search.getDateRangeFrom();
		String toDate = search.getDateRangeTo();
		String reportOpt = search.getReportOption();

		StringBuffer sb = new StringBuffer();

		sb.append(" SELECT pnr, flight_number, SEGMENT, to_char(date_of_travel, 'DD/MM/YYYY HH24:MI:SS') date_of_travel, ");
		sb.append(" adjustment_amount, passenger, CATEGORY, charge_type, a.station_code,  ");
		sb.append(" SUBSTR (pay, 1, INSTR (pay, ',', 1, 1) - 1) agent_code, ");
		sb.append(" SUBSTR (pay, INSTR (pay, ',', 1, 1) + 1, INSTR (pay, ',', 1, 2) - INSTR (pay, ',', 1, 1) - 1 ");
		sb.append("  ) user_id, SUBSTR (pay, INSTR (pay, ',', 1, 2) + 1) remarks, a.agent_name, st.station_name ");
		sb.append(" , RESULT.status FROM (SELECT a.pnr, i.flight_number, c.segment_code SEGMENT, ");
		sb.append(" c.est_time_departure_zulu date_of_travel,f.amount adjustment_amount, ");
		sb.append(" g.title || ' ' || g.first_name || ' ' || g.last_name passenger, ");
		sb.append(" charge_category_code CATEGORY, charge_description charge_type, ");
		sb.append(" (SELECT m.agent_code || ',' || m.user_id || ',' || m.remarks ");
		sb.append("  FROM t_pax_transaction m WHERE m.tnx_date = e.charge_date ");
		sb.append(" AND m.amount = e.amount AND m.nominal_code = 11 ");
		sb.append(" AND m.pnr_pax_id = g.pnr_pax_id AND ROWNUM = 1) pay ,a.status ");
		sb.append(" FROM t_reservation a, t_pnr_segment b, t_flight_segment c, ");
		sb.append(" t_pnr_pax_fare_segment d, t_pnr_pax_ond_charges e, t_pnr_pax_seg_charges f, ");
		sb.append(" t_pnr_passenger g,t_pnr_pax_fare h, t_flight i,t_charge_rate k, t_charge l ");
		sb.append(" WHERE a.pnr = b.pnr AND a.pnr = g.pnr AND b.flt_seg_id = c.flt_seg_id ");
		sb.append(" AND d.pnr_seg_id = b.pnr_seg_id AND h.ppf_id = e.ppf_id ");
		sb.append(" AND f.pft_id = e.pft_id AND f.ppfs_id = d.ppfs_id ");
		sb.append(" AND g.pnr_pax_id = h.pnr_pax_id AND h.ppf_id = d.ppf_id ");
		sb.append(" AND c.flight_id = i.flight_id AND e.charge_rate_id = k.charge_rate_id ");
		sb.append(" AND k.charge_code = l.charge_code  AND e.charge_group_code = 'ADJ' ");
		sb.append("  ");
		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			sb.append("  AND e.charge_date BETWEEN TO_DATE (");
			sb.append(fromDate);
			sb.append(") AND TO_DATE (");
			sb.append(toDate);
			sb.append(") ");
		}
		if (isNotEmptyOrNull(reportOpt)) {
			if (reportOpt.equals("POST")) {
				sb.append(" AND e.charge_date > c.est_time_departure_zulu  ");
			} else {
				sb.append(" AND e.charge_date < c.est_time_departure_zulu  ");
			}
		}
		sb.append(" ) RESULT, t_agent a, t_station st ");
		sb.append(" WHERE SUBSTR (pay, 1, INSTR (pay, ',', 1, 1) - 1) = a.agent_code  ");
		sb.append(" and a.station_code = st.station_code order by RESULT.pnr ");
		return sb.toString();
	}

	/**
	 * Creates the SQL for Void Reservation Details Report
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getVoidReservationDetailsReportQuery(ReportsSearchCriteria search) {

		String dateRangeFrom = search.getDateRangeFrom();
		String dateRangeTo = search.getDateRangeTo();
		Collection<String> users = search.getUsers();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT res.pnr AS pnr,");
		sb.append("res.booking_timestamp AS booking_date,");
		sb.append("res.origin_user_id AS origin_user,");
		sb.append("res.total_fare AS total_fare,");
		sb.append("flgseg.est_time_departure_zulu AS flight_dep_zulu ");
		sb.append("FROM t_reservation res , t_pnr_passenger pax , t_pnr_segment pnrseg , ");
		sb.append("t_flight_segment flgseg ");
		sb.append("WHERE pax.pnr = res.pnr ");
		sb.append("AND res.pnr = pnrseg.pnr ");
		sb.append("AND pnrseg.flt_seg_id = flgseg.flt_seg_id ");
		sb.append("AND res.void_reservation = 'Y' ");

		if (isNotEmptyOrNull(search.getPnr())) {
			sb.append(" AND pax.pnr = '" + search.getPnr() + "'");
		}

		if (isNotEmptyOrNull(dateRangeFrom) && isNotEmptyOrNull(dateRangeTo)) {
			sb.append(" AND flgseg.est_time_departure_zulu BETWEEN TO_DATE('" + dateRangeFrom
					+ " 00:00:00','DD/MM/YYYY HH24:mi:ss') AND TO_DATE('" + dateRangeTo
					+ "  23:59:00','DD/MM/YYYY HH24:mi:ss') AND (");
		}

		List<String> userList = new ArrayList<String>(users);

		while (userList.size() > 100) {
			List<String> subList = userList.subList(0, 100);
			userList = userList.subList(101, userList.size());
			sb.append(" res.origin_user_id IN( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (userList.size() <= 100) {
			sb.append(" res.origin_user_id IN( " + Util.buildStringInClauseContent(userList) + " )) ");
		}

		return sb.toString();
	}

	public String getBookingCountDetailsReportQuery(ReportsSearchCriteria search, boolean count) {

		String dateRangeFrom = search.getDateRangeFrom();
		String dateRangeTo = search.getDateRangeTo();
		Collection<String> users = search.getUsers();

		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append("SELECT SUM(pax_count) total_count FROM (");
		}
		sb.append(" SELECT TO_CHAR (TRUNC ( r.booking_timestamp), 'DD-MON-YY') AS booking_date , null agent_code , null agent_name , ");

		if (!count) {
			sb.append(" null total_count , ");
		}

		sb.append(" COUNT(r.pnr) pax_count FROM t_reservation r ");
		sb.append(" WHERE 1 = 1 AND ");

		if (isNotEmptyOrNull(dateRangeFrom) && isNotEmptyOrNull(dateRangeTo)) {
			sb.append(" booking_timestamp BETWEEN TO_DATE ( '" + dateRangeFrom
					+ " 00:00:00','DD/MM/YYYY HH24:mi:ss') AND TO_DATE('" + dateRangeTo + " 23:59:00','DD/MM/YYYY HH24:mi:ss' ) ");
		}
		sb.append(" AND r.status = 'CNF' ");
		sb.append(" GROUP BY TO_CHAR (TRUNC ( r.booking_timestamp), 'DD-MON-YY') ");
		sb.append(" ORDER BY TO_CHAR (TRUNC ( r.booking_timestamp), 'DD-MON-YY') ");

		if (count) {
			sb.append(")");
		}
		return sb.toString();

	}

	public String getBookingCountDetailAgentReportQuery(ReportsSearchCriteria search, boolean count) {

		String dateRangeFrom = search.getDateRangeFrom();
		String dateRangeTo = search.getDateRangeTo();
		Collection<String> agents = search.getAgents();

		StringBuffer sb = new StringBuffer();

		if (count) {
			sb.append(" SELECT SUM(pax_count) total_count FROM (");
		}
		sb.append(" SELECT TRUNC (r.booking_timestamp) AS booking_date, ");
		sb.append("r.owner_agent_code                AS agent_code  , ");

		if (!count) {
			sb.append(" null total_count , ");
		}

		sb.append("COUNT (r.pnr) pax_count , agnt.agent_name AS agent_name ");
		sb.append("FROM t_reservation r , t_agent agnt ");
		sb.append("WHERE 1=1 AND ");

		if (isNotEmptyOrNull(dateRangeFrom) && isNotEmptyOrNull(dateRangeTo)) {
			sb.append(" r.booking_timestamp BETWEEN TO_DATE ( '" + dateRangeFrom
					+ " 00:00:00','DD/MM/YYYY HH24:mi:ss') AND TO_DATE('" + dateRangeTo
					+ " 23:59:00', 'DD/MM/YYYY HH24:mi:ss' ) ");
		}

		List<String> agentList = new ArrayList<String>(agents);

		sb.append(" AND ");

		while (agentList.size() > 100) {
			List<String> subList = agentList.subList(0, 100);
			agentList = agentList.subList(101, agentList.size());
			sb.append(" r.owner_agent_code IN ( " + Util.buildStringInClauseContent(subList) + " ) OR ");
		}

		if (agentList.size() <= 100) {
			sb.append(" r.owner_agent_code IN ( " + Util.buildStringInClauseContent(agentList) + " ) ");
		}
		sb.append(" AND r.owner_agent_code = agnt.agent_code ");
		sb.append("GROUP BY TRUNC (r.booking_timestamp), ");
		sb.append("r.owner_agent_code , agnt.agent_name ");
		sb.append("ORDER BY booking_date ");

		if (count) {
			sb.append(") ");
		}
		return sb.toString();
	}

	/**
	 * Creates the SQL for lcc and Dry Collection data
	 * 
	 * @param reportsSearchCriteria
	 */
	public String getLccAndDryCollectionReportQuery(ReportsSearchCriteria search) {
		String fromDate = search.getDateRangeFrom();
		String toDate = search.getDateRangeTo();
		StringBuffer sb = new StringBuffer();
		Collection<Integer> nominalCodes = search.getNominalCodes();
		// Collection<Integer> externalNominalCodes = search.getExtPaymentNominalCodes();
		// Collection<Integer> paxCreditNominalCodes = search.getPaxCreditNominalCodes();
		String searchOptions = search.getReportOption();
		String dryCarrierCode = search.getCarrierCode();
		String agentCode = search.getAgentCode();
		// String payCurrency = search.getCurrencies();

		// Collection<String> interlineCarriers = search.getCarrierCodes();
		// Collection<String> creditConsumeAgents = search.getCreditConsumeAgents();

		if (searchOptions.equals("SUMMARY_PAYABLE")) {
			sb.append(" SELECT DECODE(T2.PAYMENT_MODE, 19, 'ON_ACCOUNT', 18, 'CASH', 15, 'VISA', 16, 'MASTER', 28, 'AMEX', 17, 'DINERS', 30, 'GENERIC', 32, 'CMI',6,'CREDIT',0,'AIRLINE_CREDIT') AS PAYMENT_MODE,");
			sb.append(" T2.CARRIER_CODE,T2.AGENT_CODE, T2.PAYMENT_CURRENCY_CODE, T2.AMOUNT, T2.PAYMENT_CURRENCY_AMOUNT, T2.AGENT_NAME,T2.STATION_CODE");
			sb.append(" FROM (");
			sb.append(" SELECT DECODE(T1.NOMINAL_CODE, 25, 19,26,18,22, 15, 23, 16, 29, 28,24,17, 31, 30, 33, 32,5,6, T1.NOMINAL_CODE) AS PAYMENT_MODE, ");
			sb.append(" T1.AGENT_CODE AGENT_CODE,SUM(-1 * T1.AMOUNT) AMOUNT, SUM(-1 * T1.AMOUNT_PAYCUR) PAYMENT_CURRENCY_AMOUNT, T1.EXT_CARRIER_CODE CARRIER_CODE, ");
			sb.append(" T1.PAYCUR_CODE PAYMENT_CURRENCY_CODE,TA.AGENT_NAME AGENT_NAME,TA.STATION_CODE STATION_CODE ");
			sb.append(" FROM T_PAX_EXT_CARRIER_TRANSACTIONS T1, T_AGENT TA");
			sb.append(String
					.format(" WHERE T1.TXN_TIMESTAMP BETWEEN TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') AND TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') ",
							fromDate, toDate));
			if (nominalCodes != null && !nominalCodes.isEmpty()) {
				sb.append(String.format(" AND T1.NOMINAL_CODE IN (%s)", Util.buildIntegerInClauseContent(nominalCodes)));
			}
			sb.append(" AND T1.AGENT_CODE = TA.AGENT_CODE(+)");
			sb.append(" GROUP BY T1.AGENT_CODE, ");
			sb.append(" DECODE(T1.NOMINAL_CODE, 25, 19,26,18,22, 15, 23, 16, 29, 28,24,17, 31, 30, 33, 32,5,6, T1.NOMINAL_CODE),");
			sb.append(" T1.EXT_CARRIER_CODE, T1.PAYCUR_CODE, TA.AGENT_NAME, TA.STATION_CODE)");
			sb.append(" T2 order by CARRIER_CODE");

		} else if (searchOptions.equals("SUMMARY_RECEIVABLE")) {

			sb.append(" SELECT DECODE(T2.PAYMENT_MODE, 19, 'ON_ACCOUNT', 18, 'CASH', 15, 'VISA', 16, 'MASTER', 28, 'AMEX', 17, 'DINERS', 30, 'GENERIC', 32, 'CMI',6,'CREDIT',0,'AIRLINE_CREDIT') AS PAYMENT_MODE, ");
			sb.append(" T2.CARRIER_CODE,T2.AGENT_CODE, T2.PAYMENT_CURRENCY_CODE, T2.AMOUNT, T2.PAYMENT_CURRENCY_AMOUNT, T2.AGENT_NAME, T2.STATION_CODE ");
			sb.append(" FROM (");
			sb.append(" SELECT DECODE(T1.NOMINAL_CODE, 25, 19,26,18,22, 15, 23, 16, 29, 28,24,17, 31, 30, 33, 32,5,6, T1.NOMINAL_CODE) AS PAYMENT_MODE, ");
			sb.append(" T1.AGENT_CODE AGENT_CODE, SUM(T1.AMOUNT*-1) AMOUNT, SUM(T1.AMOUNT_PAYCUR*-1) PAYMENT_CURRENCY_AMOUNT, T1.PAYMENT_CARRIER_CODE CARRIER_CODE, ");
			sb.append(" T1.PAYMENT_CURRENCY_CODE PAYMENT_CURRENCY_CODE, TA.AGENT_NAME AGENT_NAME, TA.STATION_CODE STATION_CODE ");
			sb.append(" FROM T_PAX_TRANSACTION T1, T_AGENT TA");
			sb.append(String
					.format(" WHERE T1.TNX_DATE BETWEEN TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') AND TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') ",
							fromDate, toDate));
			if (nominalCodes != null && !nominalCodes.isEmpty()) {
				sb.append(String.format(" AND T1.NOMINAL_CODE IN (%s)", Util.buildIntegerInClauseContent(nominalCodes)));
			}
			sb.append("	AND T1.PAYMENT_CARRIER_CODE != (SELECT PARAM_VALUE FROM T_APP_PARAMETER WHERE PARAM_KEY='AIRLINE_13') ");
			sb.append(" AND T1.AGENT_CODE = TA.AGENT_CODE(+)");
			sb.append(" GROUP BY T1.AGENT_CODE, ");
			sb.append(
					" DECODE(T1.NOMINAL_CODE, 25, 19,26,18,22, 15, 23, 16, 29, 28,24,17, 31, 30, 33, 32,5,6, T1.NOMINAL_CODE),");
			sb.append(" T1.PAYMENT_CARRIER_CODE, T1.PAYMENT_CURRENCY_CODE, TA.AGENT_NAME, TA.STATION_CODE ) T2");
			sb.append(" ORDER BY CARRIER_CODE");

		} else if (searchOptions.equals("DETAIL_PAYABLE")) {

			sb.append(" SELECT INFO.PNR, INFO.PAX_NAME, INFO.PAYMENT_CURRENCY_CODE,INFO.USER_ID,");
			sb.append(" SUM(INFO.BASE_AMOUNT) AS BASE_AMOUNT, SUM(INFO.PAYMENT_CURRENCY_AMOUNT) AS PAYMENT_CURRENCY_AMOUNT,");
			sb.append(" TO_CHAR(INFO.TIMESTAMP,'dd-Mon-yyyy hh24:mi:ss') TIMESTAMP,");
			sb.append(
					" DECODE(INFO.PAYMENT_MODE, 19, 'ON_ACCOUNT', 18, 'CASH', 15, 'VISA', 16, 'MASTER', 28, 'AMEX', 17, 'DINERS', 30, 'GENERIC', 32, 'CMI',6,'CREDIT',0,'AIRLINE_CREDIT') AS PAYMENT_MODE");
			sb.append(" FROM");
			sb.append(" (SELECT PAS.PNR AS PNR, PAS.PAX_SEQUENCE  AS PAX_SEQUENCE, TRX.TXN_TIMESTAMP AS TIMESTAMP, ");
			sb.append(" (PAS.TITLE || ' ' || PAS.FIRST_NAME || ' ' || PAS.LAST_NAME) as PAX_NAME, ");
			sb.append(
					" DECODE(TRX.NOMINAL_CODE, 25, 19,26,18,22, 15, 23, 16, 29, 28,24,17, 31, 30, 33, 32,5,6, TRX.nominal_code) AS PAYMENT_MODE, ");
			sb.append(
					" -1 * TRX.AMOUNT BASE_AMOUNT, -1 * TRX.AMOUNT_PAYCUR PAYMENT_CURRENCY_AMOUNT, TRX.PAYCUR_CODE PAYMENT_CURRENCY_CODE, TRX.USER_ID ");
			sb.append(" FROM T_PAX_EXT_CARRIER_TRANSACTIONS TRX, T_PNR_PASSENGER PAS  WHERE TRX.PNR_PAX_ID = PAS.PNR_PAX_ID");
			sb.append(String.format(
					"   AND TRX.TXN_TIMESTAMP BETWEEN TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') AND TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') ",
					fromDate, toDate));
			sb.append(" AND TRX.AGENT_CODE ='" + agentCode + "'");
			sb.append(" AND TRX.EXT_CARRIER_CODE ='" + dryCarrierCode + "'");
			if (nominalCodes != null && !nominalCodes.isEmpty()) {
				sb.append(String.format(" AND TRX.NOMINAL_CODE IN (%s)", Util.buildIntegerInClauseContent(nominalCodes)));
			}
			sb.append(
					" ) INFO GROUP BY PAYMENT_MODE,PNR,PAX_SEQUENCE,PAX_NAME,TO_CHAR(TIMESTAMP,'dd-Mon-yyyy hh24:mi:ss'),USER_ID,PAYMENT_CURRENCY_CODE ");
			sb.append(" ORDER BY PNR,PAX_SEQUENCE,PAYMENT_MODE ");

		} else if (searchOptions.equals("DETAIL_RECEIVABLE")) {

			sb.append(" SELECT INFO.PNR, INFO.PAX_NAME, INFO.PAYMENT_CURRENCY_CODE, INFO.USER_ID,");
			sb.append(
					" SUM(INFO.BASE_AMOUNT*-1) AS BASE_AMOUNT, SUM(INFO.PAYMENT_CURRENCY_AMOUNT*-1) AS PAYMENT_CURRENCY_AMOUNT,");
			sb.append(" TO_CHAR(INFO.TIMESTAMP,'dd-Mon-yyyy hh24:mi:ss') TIMESTAMP,");
			sb.append(
					" DECODE(INFO.PAYMENT_MODE, 19, 'ON_ACCOUNT', 18, 'CASH', 15, 'VISA', 16, 'MASTER', 28, 'AMEX', 17, 'DINERS', 30, 'GENERIC', 32, 'CMI',6,'CREDIT',0,'AIRLINE_CREDIT') AS PAYMENT_MODE");
			sb.append(" FROM");
			sb.append(" (SELECT PAS.PNR AS PNR, PAS.PAX_SEQUENCE  AS PAX_SEQUENCE, TRX.TNX_DATE AS TIMESTAMP, ");
			sb.append(" (PAS.TITLE || ' ' || PAS.FIRST_NAME || ' ' || PAS.LAST_NAME) as PAX_NAME, ");
			sb.append(
					" DECODE(TRX.NOMINAL_CODE, 25, 19,26,18,22, 15, 23, 16, 29, 28,24,17, 31, 30, 33, 32,5,6, TRX.NOMINAL_CODE) AS PAYMENT_MODE, ");
			sb.append(
					" TRX.AMOUNT BASE_AMOUNT, TRX.AMOUNT_PAYCUR PAYMENT_CURRENCY_AMOUNT, TRX.PAYMENT_CURRENCY_CODE PAYMENT_CURRENCY_CODE, TRX.USER_ID ");
			sb.append(" FROM T_PAX_TRANSACTION TRX, T_PNR_PASSENGER PAS  WHERE TRX.PNR_PAX_ID = PAS.PNR_PAX_ID");
			sb.append(String.format(
					"   AND TRX.TNX_DATE BETWEEN TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') AND TO_DATE ('%s', 'dd-Mon-yyyy hh24:mi:ss') ",
					fromDate, toDate));
			sb.append(" AND TRX.AGENT_CODE ='" + agentCode + "'");
			sb.append(" AND TRX.PAYMENT_CARRIER_CODE='" + dryCarrierCode + "'");
			if (nominalCodes != null && !nominalCodes.isEmpty()) {
				sb.append(String.format(" AND TRX.NOMINAL_CODE IN (%s)", Util.buildIntegerInClauseContent(nominalCodes)));
			}
			sb.append(
					" ) INFO GROUP BY PAYMENT_MODE,PNR,PAX_SEQUENCE,PAX_NAME,TO_CHAR(TIMESTAMP,'dd-Mon-yyyy hh24:mi:ss'),USER_ID,PAYMENT_CURRENCY_CODE");
			sb.append(" ORDER BY PNR,PAX_SEQUENCE,PAYMENT_MODE ");

		}
		return sb.toString();
	}

	public String getFareRuleVisibilityDataQuery(ReportsSearchCriteria reportSearchCriteria) {
		Integer fareRuleId = reportSearchCriteria.getFareRuleId();

		StringBuffer sb = new StringBuffer("");
		log.debug("inside getFareRuleVisibilityDataQuery()");

		sb.append("			SELECT DISTINCT SC.DESCRIPTION	AS VISIBILITY				\n");
		sb.append("			FROM T_FARE_VISIBILITY FV,							\n");
		sb.append("			T_SALES_CHANNEL SC											\n");
		sb.append("			WHERE FV.fare_rule_id = '" + fareRuleId + "'  AND		\n");
		sb.append("			FV.SALES_CHANNEL_CODE = SC.SALES_CHANNEL_CODE				\n");

		return sb.toString();
	}

	public String getAgentsForFareRuleDataQuery(ReportsSearchCriteria reportSearchCriteria) {
		Integer fareRuleId = reportSearchCriteria.getFareRuleId();

		StringBuffer sb = new StringBuffer("");
		log.debug("inside getAgentsForFareRuleDataQuery()");

		sb.append("			SELECT DISTINCT AG.AGENT_NAME	AS AGENTS				\n");
		sb.append("			FROM T_FARE_AGENT MAT,							\n");
		sb.append("			T_AGENT AG												\n");
		sb.append("			WHERE MAT.FARE_RULE_ID = '" + fareRuleId + "'		\n");
		sb.append("			AND MAT.AGENT_CODE = AG.AGENT_CODE						\n");

		return sb.toString();
	}

	/**
	 * Created By M. Naeem Akhtar Creates SQL for Report 'Fare Rule Details'
	 * 
	 * @param reportsSearchCriteria
	 * @return String
	 */

	public String getFareRuleDetailsDataQuery(ReportsSearchCriteria reportSearchCriteria) {
		String fareRules = getSingleDataValue(reportSearchCriteria.getBcCategory());
		String visibility = getSingleDataValue(reportSearchCriteria.getCarrierCodes());
		String agents = getSingleDataValue(reportSearchCriteria.getAgents());
		String status = reportSearchCriteria.getStatus();
		String flightWay = reportSearchCriteria.getOperationType();

		StringBuffer sb = new StringBuffer("");

		log.debug("inside getFareRuleDetailsDataQuery");

		sb.append("		SELECT DISTINCT FR.fare_rule_id AS FARERULEID,									\n");
		sb.append("		FR.fare_rule_code AS FARERULE,													\n");
		sb.append("		FR.fare_rule_description AS DESCRIPTION,										\n");
		sb.append("		FR.fare_basis_code AS BASIS,													\n");
		sb.append("		FR.return_flag AS FLIGHTWAY,													\n");
		sb.append("		FR.status AS STATUS,															\n");
		sb.append("		FR.modification_charge_amount AS CHARGE_RESTRICTTION,							\n");
		sb.append("		FR.print_expire_date AS PRINT_EXPIRY_DATE,										\n");
		sb.append("		FR.modification_charge_amount AS MODI_CHARGE,									\n");
		sb.append("		FR.cancellation_charge_amount AS CANCEL_CHARGE,									\n");
		sb.append("		FR.advance_booking_days AS ADV_BOOKING_DATE,									\n");
		sb.append("		FR.minimum_stay_over_mins as MIN_STAY_MINS,										\n");
		sb.append("		FR.maximum_stay_over_mins as MAX_STAY_MINS,										\n");
		sb.append("		CASE																			\n");
		sb.append("				WHEN FR.minimum_stay_over_months IS NOT NULL THEN fr.minimum_stay_over_months || 'm' ELSE ''							\n");
		sb.append("		END as MIN_MONTH,																												\n");
		sb.append("		CASE																															\n");
		sb.append("				WHEN FR.minimum_stay_over_mins IS NOT NULL THEN ROUND(fr.minimum_stay_over_mins / (24 * 60)) || 'd' ELSE ''   			\n");
		sb.append("		END as MIN_DAY,    																												\n");
		sb.append("		CASE																															\n");
		sb.append("				WHEN FR.minimum_stay_over_mins IS NOT NULL THEN ROUND(MOD(fr.minimum_stay_over_mins,  (24 * 60)) / 60) || 'h'  ELSE ''	\n");
		sb.append("		END as MIN_HOUR,																												\n");
		sb.append("		CASE																															\n");
		sb.append("				WHEN FR.minimum_stay_over_mins IS NOT NULL THEN ROUND(MOD(MOD(fr.minimum_stay_over_mins,  (24 * 60)),  60)) || 'mi'  ELSE '' 	\n");
		sb.append("		END as MIN_MINS,																												\n");
		sb.append("		CASE																															\n");
		sb.append("				WHEN FR.maximum_stay_over_months IS NOT NULL THEN fr.maximum_stay_over_months || 'm' ELSE ''  							\n");
		sb.append("		END as MAX_MONTH,    																											\n");
		sb.append("		 CASE 																															\n");
		sb.append("				WHEN FR.maximum_stay_over_mins IS NOT NULL THEN ROUND(fr.maximum_stay_over_mins / (24 * 60)) || 'd' ELSE ''  			\n");
		sb.append("		END as MAX_DAY,      																											\n");
		sb.append("		CASE 																															\n");
		sb.append("				WHEN FR.maximum_stay_over_mins IS NOT NULL THEN ROUND(MOD(fr.maximum_stay_over_mins,  (24 * 60)) / 60) || 'h'  ELSE '' \n");
		sb.append("		END as MAX_HOUR,																												\n");
		sb.append("		CASE																															\n");
		sb.append("				WHEN FR.maximum_stay_over_mins IS NOT NULL THEN ROUND(MOD(MOD(fr.maximum_stay_over_mins,  (24 * 60)),  60)) || 'mi'  ELSE '' 	\n");
		sb.append("		END as MAX_MINS,																\n");
		sb.append("		FR.print_expire_date  AS P_EXP_DATE,											\n");
		sb.append("		FR.valid_days_of_week_a0 AS INB1,												\n");
		sb.append("		FR.valid_days_of_week_a1 AS INB2,												\n");
		sb.append("		FR.valid_days_of_week_a2 AS INB3,												\n");
		sb.append("		FR.valid_days_of_week_a3 AS INB4,												\n");
		sb.append("		FR.valid_days_of_week_a4 AS INB5,												\n");
		sb.append("		FR.valid_days_of_week_a5 AS INB6,												\n");
		sb.append("		FR.valid_days_of_week_a6 AS INB7,												\n");
		sb.append("		TO_CHAR(FR.arrival_time_from, 'HH24:MI') as IN_TRAVEL_TIME_FROM,				\n");
		sb.append("		TO_CHAR(FR.arrival_time_to, 'HH24:MI') as IN_TRAVEL_TIME_TO, 					\n");
		sb.append("		FR.valid_days_of_week_d0 AS OB1,												\n");
		sb.append("		FR.valid_days_of_week_d1 AS OB2,												\n");
		sb.append("		FR.valid_days_of_week_d2 AS OB3,												\n");
		sb.append("		FR.valid_days_of_week_d3 AS OB4,												\n");
		sb.append("		FR.valid_days_of_week_d4 AS OB5,												\n");
		sb.append("		FR.valid_days_of_week_d5 AS OB6,												\n");
		sb.append("		FR.valid_days_of_week_d6 AS OB7,												\n");
		sb.append("		TO_CHAR(FR.departure_time_to, 'HH24:MI') as D_TRAVEL_TIME_TO,					\n");
		sb.append("		TO_CHAR(FR.departure_time_from, 'HH24:MI') as D_TRAVEL_TIME_FROM,				\n");
		sb.append("		FR.PAX_CATEGORY_CODE AS PAX_CAT,												\n");
		sb.append("		FR.FARE_CAT_ID AS FARE_CAT,														\n");
		sb.append("		FR.MODIFY_BUFFER_TIME AS MOD_BUFF_TIME,											\n");
		sb.append("		FR.RULES_COMMENTS AS RULE_COMMENTS,												\n");
		sb.append("		CHILD_CAT.MRULE AS M_F_RULE,													\n");
		sb.append("		CHILD_CAT.PAY AS CHILD_PAY_APPLICABLE, 											\n");
		sb.append("		CHILD_CAT.REFUND AS CHILD_REFUND, 												\n");
		sb.append("		CHILD_CAT.MODF AS CHILD_MODF,													\n");
		sb.append("		CHILD_CAT.CNX AS CHILD_CNX,  													\n");
		sb.append("		CHILD_CAT.NOSHOW AS CHILD_NOSHOW,												\n");
		sb.append("		INFANT_CAT.PAY AS INFANT_PAY,     												\n");
		sb.append("		INFANT_CAT.REFUND AS INFANT_REFUND, 											\n");
		sb.append("		INFANT_CAT.MODF AS INFANT_MODF,													\n");
		sb.append("		INFANT_CAT.CNX AS INFANT_CNX,        											\n");
		sb.append("		INFANT_CAT.NOSHOW AS INFANT_NOSHOW,												\n");
		sb.append("		ADULT_CAT.PAY AS ADULT_PAY,  													\n");
		sb.append("		ADULT_CAT.REFUND AS ADULT_REFUND, 												\n");
		sb.append("		ADULT_CAT.MODF AS ADULT_MODF,       											\n");
		sb.append("		ADULT_CAT.CNX AS ADULT_CNX,      												\n");
		sb.append("		ADULT_CAT.NOSHOW AS ADULT_NOSHOW,												\n");
		sb.append("		FR.IS_OPEN_RETURN AS OPEN_RETURN,												\n");
		sb.append("		FR.OPENRT_CONF_PERIOD_MINS AS OPEN_RETURN_PERIOD_MINS,							\n");
		sb.append("		CASE																			\n");
		sb.append(
				"			 WHEN FR.openrt_conf_stay_over_months IS NOT NULL THEN fr.openrt_conf_stay_over_months || 'm' ELSE ''  								\n");
		sb.append(
				"		END as OR_MONTH, 																														\n");
		sb.append(
				"		CASE 																																	\n");
		sb.append(
				"			WHEN FR.OPENRT_CONF_PERIOD_MINS IS NOT NULL THEN ROUND(fr.OPENRT_CONF_PERIOD_MINS / (24 * 60)) || 'd' ELSE ''   					\n");
		sb.append(
				"		END as OR_DAY,     																														\n");
		sb.append(
				"		CASE 																																	\n");
		sb.append(
				"				WHEN FR.OPENRT_CONF_PERIOD_MINS IS NOT NULL THEN ROUND(MOD(fr.OPENRT_CONF_PERIOD_MINS,  (24 * 60)) / 60) || 'h'  ELSE '' 		\n");
		sb.append(
				"		 END as OR_HOUR,																														\n");
		sb.append(
				"		CASE 																																	\n");
		sb.append(
				"				 WHEN FR.OPENRT_CONF_PERIOD_MINS IS NOT NULL THEN ROUND(MOD(MOD(FR.OPENRT_CONF_PERIOD_MINS,  (24 * 60)),  60)) || 'mi'  ELSE ''\n");
		sb.append(
				"		 END as OR_MINS																															\n");

		sb.append("FROM t_fare_rule FR,													\n");
		if (agents != null && !agents.equals("") && !agents.equals("''")) {
			sb.append("			t_fare_agent FA,										\n");
		}
		if (visibility != null && !visibility.equals("") && !visibility.equals("''")) {
			sb.append("			t_fare_visibility FV,								\n");
			sb.append("			t_sales_channel SC,											\n");
		}

		sb.append("(SELECT MPAX.fare_rule_id AS MRULE,  												\n"
				+ "     MPAX.pax_type_code AS PAY,																\n"
				+ "      MPAX.refundable_flag AS REFUND,														\n"
				+ "      MPAX.apply_mod_charge AS MODF,															\n"
				+ "      MPAX.apply_cnx_charge AS CNX,															\n"
				+ "      MPAX.no_show_charge AS NOSHOW 															\n"
				+ "FROM t_fare_rule_pax MPAX																\n" + "WHERE "
				+ ReportUtils.getReplaceStringForIN("MPAX.fare_rule_id", reportSearchCriteria.getBcCategory(), false)
				+ " 										\n"
				+ "AND MPAX.pax_type_code = 'CH') CHILD_CAT,													\n");

		sb.append("(SELECT  MPAX.fare_rule_id AS MRULE,												\n"
				+ "      MPAX.pax_type_code AS PAY,																\n"
				+ "      MPAX.refundable_flag AS REFUND,														\n"
				+ "      MPAX.apply_mod_charge AS MODF,															\n"
				+ "      MPAX.apply_cnx_charge AS CNX,															\n"
				+ "      MPAX.no_show_charge AS NOSHOW															\n"
				+ "   FROM t_fare_rule_pax MPAX															\n" + "   WHERE "
				+ ReportUtils.getReplaceStringForIN("MPAX.fare_rule_id", reportSearchCriteria.getBcCategory(), false)
				+ " 										\n"
				+ "   AND MPAX.pax_type_code = 'IN' ) INFANT_CAT,												\n");

		sb.append("(SELECT  MPAX.fare_rule_id AS MRULE,												\n"
				+ "      MPAX.pax_type_code AS PAY,																\n"
				+ "      MPAX.refundable_flag AS REFUND,														\n"
				+ "      MPAX.apply_mod_charge AS MODF,															\n"
				+ "      MPAX.apply_cnx_charge AS CNX,															\n"
				+ "      MPAX.no_show_charge AS NOSHOW 															\n"
				+ "   FROM t_fare_rule_pax MPAX															\n" + "   WHERE "
				+ ReportUtils.getReplaceStringForIN("MPAX.fare_rule_id", reportSearchCriteria.getBcCategory(), false)
				+ " 										\n"
				+ "   AND MPAX.pax_type_code = 'AD') ADULT_CAT													\n");

		sb.append("			WHERE FR.fare_rule_id = CHILD_CAT.MRULE									\n");
		sb.append("			AND FR.fare_rule_id = INFANT_CAT.MRULE 									\n");
		sb.append("			AND FR.fare_rule_id = ADULT_CAT.MRULE			 							\n");

		// Agent Filter Condition
		if (agents != null && !agents.equals("") && !agents.equals("''")) {
			sb.append("		AND FR.fare_rule_id = FA.fare_rule_id 						\n");
			// sb.append(" AND FA.agent_code IN (" + agents +") \n");
			sb.append(" 	and  ");
			sb.append(ReportUtils.getReplaceStringForIN("FA.agent_code", reportSearchCriteria.getAgents(), false) + " ");
		}

		// Status Filter Condition
		if (status != "" && status.equalsIgnoreCase("ACTIVE")) {
			sb.append("		AND FR.status = 'ACT' 							\n");
		} else if (status != "" && status.equalsIgnoreCase("INACTIVE")) {
			sb.append("		AND FR.status = 'INA' 							\n");
		} else if (status != "" && status.equalsIgnoreCase("ALL")) {
			sb.append("		AND FR.status IN ('ACT' , 'INA') 				\n");
		}

		// Reutrn Flag Filter Condition
		if (flightWay != "" && flightWay.equalsIgnoreCase("ONEWAY")) {
			sb.append("		AND FR.return_flag = 'N'     \n");
		} else if (flightWay != "" && flightWay.equalsIgnoreCase("RETURN")) {
			sb.append("		AND FR.return_flag = 'Y'     \n");
		} else if (flightWay != null && flightWay.equalsIgnoreCase("ALL")) {
			sb.append("		AND FR.return_flag IN ('Y' , 'N') 				\n");
		}

		// VISIBILITY CHECK
		if (visibility != null && !visibility.equals("") && !visibility.equals("''")) {
			sb.append("			AND FR.fare_rule_id = FV.fare_rule_id 	\n");
			sb.append("			AND FV.sales_channel_code = SC.sales_channel_code 			\n");
			sb.append("			AND SC.DESCRIPTION IN (" + visibility + ")					\n");
		}
		sb.append("			ORDER BY FR.fare_rule_code			");
		return sb.toString();
	}

	public String getMisProductSalesAnalysisReportQuery(ReportsSearchCriteria reportSearchCriteria) {
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT   SUM (txn.amount) * -1 AS sales, ag.agent_name as agent, st.station_name as location, "
				+ "rg.region_name as region " + "FROM  t_pax_transaction txn, " + "t_agent ag, " + "t_station st, "
				+ " t_country cn, " + "t_region rg " + "WHERE txn.tnx_date BETWEEN TO_DATE ('"
				+ reportSearchCriteria.getDateRangeFrom() + " 00:00:00','dd/mm/yyyy HH24:mi:ss') " + " AND TO_DATE ('"
				+ reportSearchCriteria.getDateRangeTo() + " 23:59:59', 'dd/mm/yyyy HH24:mi:ss' ) "
				+ "AND txn.agent_code = ag.agent_code " + "AND ag.station_code = st.station_code "
				+ "and st.country_code = cn.country_code " + " and cn.region_code = rg.region_code " + "AND txn.nominal_code IN("
				+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes()) + ") "
				+ "AND ag.station_code = st.station_code " + "AND st.country_code = cn.country_code "
				+ "AND cn.region_code = rg.region_code " + "GROUP BY ag.agent_name, " + " st.station_name, " + "cn.country_name, "
				+ " rg.region_name ORDER BY rg.region_name");

		return sql.toString();
	}

	public String getBookedPAXSegmentData(ReportsSearchCriteria reportsSearchCriteria) {
		String singleDataValue = getSingleDataValue(reportsSearchCriteria.getAgents());
		StringBuilder sb = new StringBuilder();
		String startDate = reportsSearchCriteria.getDateRangeFrom();
		String endDate = reportsSearchCriteria.getDateRangeTo();
		String rptType = reportsSearchCriteria.getReportType();
		boolean webChannelEnabled = false;
		
		List<String> webAndMobileChannels = new ArrayList<String>();
		webAndMobileChannels.add(String.valueOf(SalesChannelsUtil.SALES_CHANNEL_WEB));
		webAndMobileChannels.add(String.valueOf(SalesChannelsUtil.SALES_CHANNEL_IOS));
		webAndMobileChannels.add(String.valueOf(SalesChannelsUtil.SALES_CHANNEL_ANDROID));
		
		List<String> webAndMobileCommon = new ArrayList<String>(reportsSearchCriteria.getSalesChannels());
		
		webAndMobileCommon.retainAll(webAndMobileChannels);
		
		if (rptType.equalsIgnoreCase(ReportsSearchCriteria.BOOKED_PAX_SEGMENT_DETAIL)) {
			// Append part to get IBE web Booking
			if (CollectionUtils.containsAny(reportsSearchCriteria.getSalesChannels(), webAndMobileChannels)) {

				sb.append(" SELECT P.pnr AS PNR													,");
				sb.append(" nvl(ag.agent_name,DECODE (ps.status_mod_channel_code,'4','WEB','20','IOS','21','ANDROID',ag.agent_name)) as agent_name		,");
				sb.append(" PS.STATUS_MOD_DATE AS SEGMENT_BOOKED_DATE							,");
				sb.append(" FS.SEGMENT_CODE AS SEGMENT											,");
				sb.append(" FS.EST_TIME_DEPARTURE_ZULU AS DEPARTURE_DATE						,");
				sb.append(" P.FIRST_NAME||' '||P.LAST_NAME AS PAX_NAME							 ");
				sb.append(" FROM ");
				sb.append(" T_RESERVATION R, ");
				sb.append(
						" (SELECT STATUS,PNR,MKTING_CARRIER_CODE,STATUS_MOD_DATE,STATUS_MOD_CHANNEL_CODE,MKTING_AGENT_CODE,FLT_SEG_ID AS FLT_SEG_ID,MKTING_STATION_CODE FROM T_PNR_SEGMENT ");
				sb.append("  UNION ALL ");
				sb.append(
						"  SELECT STATUS,PNR,MKTING_CARRIER_CODE,STATUS_MOD_DATE,STATUS_MOD_CHANNEL_CODE,MKTING_AGENT_CODE,EXT_FLT_SEG_ID AS FLT_SEG_ID,MKTING_STATION_CODE FROM T_EXT_PNR_SEGMENT ");
				sb.append("  ) PS, ");
				sb.append(" (SELECT SEGMENT_CODE,EST_TIME_DEPARTURE_ZULU,FLT_SEG_ID AS FLT_SEG_ID FROM T_FLIGHT_SEGMENT ");
				sb.append("  UNION ALL ");
				sb.append(
						"  SELECT SEGMENT_CODE,EST_TIME_DEPARTURE_ZULU,EXT_FLT_SEG_ID AS FLT_SEG_ID FROM T_EXT_FLIGHT_SEGMENT ");
				sb.append("  ) FS, ");
				sb.append(" T_AGENT AG, ");
				sb.append(" T_PNR_PASSENGER P ");
				sb.append(" WHERE ");
				sb.append(" PS.FLT_SEG_ID=FS.FLT_SEG_ID AND ");
				sb.append(" PS.PNR= P.PNR AND ");
				sb.append(" R.PNR = PS.PNR AND ");
				sb.append(" R.PNR = P.PNR AND ");
				sb.append(" P.PAX_TYPE_CODE !='IN' AND ");
				sb.append(" PS.MKTING_AGENT_CODE = AG.AGENT_CODE (+) AND ");
				if (reportsSearchCriteria.getIncldeOnhold()) {
					sb.append(" (PS.STATUS='CNF' OR ");
					sb.append(" R.STATUS='OHD') AND ");
				} else {
					sb.append(" PS.STATUS='CNF' AND ");
					sb.append(" R.STATUS='CNF' AND ");
				}
				sb.append(" PS.STATUS_MOD_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" AND TO_DATE('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" AND ").append(
						ReportUtils.getReplaceStringForIN("PS.STATUS_MOD_CHANNEL_CODE", webAndMobileCommon, false) + "  ");
				sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_CARRIER_CODE",
						reportsSearchCriteria.getCarrierCodes(), false) + "  ");

				reportsSearchCriteria.getSalesChannels().removeAll(webAndMobileChannels);
				if (reportsSearchCriteria.getSalesChannels().size() > 0) {
					sb.append(" UNION ALL");
				}
			}
			if (reportsSearchCriteria.getSalesChannels().size() > 0) {
				sb.append(" SELECT P.pnr AS PNR													,");
				sb.append(" nvl(ag.agent_name,DECODE (ps.status_mod_channel_code,'4','WEB','20','IOS','21','ANDROID',ag.agent_name)) as agent_name		,");
				sb.append(" PS.STATUS_MOD_DATE AS SEGMENT_BOOKED_DATE							,");
				sb.append(" FS.SEGMENT_CODE AS SEGMENT											,");
				sb.append(" FS.EST_TIME_DEPARTURE_ZULU AS DEPARTURE_DATE						,");
				sb.append(" P.FIRST_NAME||' '||P.LAST_NAME AS PAX_NAME							 ");
				sb.append(" FROM ");
				sb.append(" T_RESERVATION R, ");
				sb.append(
						" (SELECT STATUS,PNR,MKTING_CARRIER_CODE,STATUS_MOD_DATE,STATUS_MOD_CHANNEL_CODE,MKTING_AGENT_CODE,FLT_SEG_ID AS FLT_SEG_ID,MKTING_STATION_CODE FROM T_PNR_SEGMENT ");
				sb.append("  UNION ALL ");
				sb.append(
						"  SELECT STATUS,PNR,MKTING_CARRIER_CODE,STATUS_MOD_DATE,STATUS_MOD_CHANNEL_CODE,MKTING_AGENT_CODE,EXT_FLT_SEG_ID AS FLT_SEG_ID,MKTING_STATION_CODE FROM T_EXT_PNR_SEGMENT ");
				sb.append("  ) PS, ");
				sb.append(" (SELECT SEGMENT_CODE,EST_TIME_DEPARTURE_ZULU,FLT_SEG_ID AS FLT_SEG_ID FROM T_FLIGHT_SEGMENT ");
				sb.append("  UNION ALL ");
				sb.append(
						"  SELECT SEGMENT_CODE,EST_TIME_DEPARTURE_ZULU,EXT_FLT_SEG_ID AS FLT_SEG_ID FROM T_EXT_FLIGHT_SEGMENT ");
				sb.append("  ) FS, ");
				sb.append(" T_AGENT AG, ");
				sb.append(" T_PNR_PASSENGER P ");
				sb.append(" WHERE ");
				sb.append(" PS.FLT_SEG_ID=FS.FLT_SEG_ID AND ");
				sb.append(" PS.PNR= P.PNR AND ");
				sb.append(" R.PNR = PS.PNR AND ");
				sb.append(" R.PNR = P.PNR AND ");
				sb.append(" P.PAX_TYPE_CODE !='IN' AND ");
				sb.append(" PS.MKTING_AGENT_CODE = AG.AGENT_CODE (+) AND ");
				if (reportsSearchCriteria.getIncldeOnhold()) {
					sb.append(" (PS.STATUS='CNF' OR ");
					sb.append(" R.STATUS='OHD') AND ");
				} else {
					sb.append(" PS.STATUS='CNF' AND ");
					sb.append(" R.STATUS='CNF' AND ");
				}
				sb.append(" PS.STATUS_MOD_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" AND TO_DATE('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				if (reportsSearchCriteria.getSalesChannels() != null && reportsSearchCriteria.getSalesChannels().size() > 0) {
					sb.append(" AND ").append((ReportUtils.getReplaceStringForIN("PS.STATUS_MOD_CHANNEL_CODE",
							reportsSearchCriteria.getSalesChannels(), false) + "  "));
				}
				sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_CARRIER_CODE",
						reportsSearchCriteria.getCarrierCodes(), false) + "  ");
				if (reportsSearchCriteria.getStations() != null && reportsSearchCriteria.getStations().size() > 0) {
					// if all agents selected,
					// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_STATION_CODE",
							reportsSearchCriteria.getStations(), false) + "  ");
				}
				if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) {
					// if all agents selected,
					// reportsSearchCriteria.getAgents() == null
					sb.append(" AND (").append(
							ReportUtils.getReplaceStringForIN("PS.MKTING_AGENT_CODE", reportsSearchCriteria.getAgents(), false)
									+ " ) ");
				}
			}
			sb.append(" ORDER BY 1, 2, 3, 4 ");

		} else if (rptType.equalsIgnoreCase(ReportsSearchCriteria.BOOKED_PAX_SEGMENT_SUMMARY)) {
			sb.append(" Select V.DESCRIPTION AS SALES_CHANNEL								,");
			sb.append(" V.CARRIER_CODE AS MARKETING_CARRIER								,");

			if (reportsSearchCriteria.getIncldeOnhold()) {
				sb.append(" sum(V.SOLD_PAX_SEG) AS CNF_PAX_SEG						 		,");
				sb.append(" sum(V.ONHOLD_PAX_SEG) AS ONHOLD_PAX_SEG 						,");
				sb.append(" (sum(V.SOLD_PAX_SEG) + sum(V.ONHOLD_PAX_SEG)) AS SOLD_PAX_SEG	 ");
			} else {
				sb.append(" sum(V.SOLD_PAX_SEG) AS CNF_PAX_SEG						 		,");
				sb.append(" sum(V.SOLD_PAX_SEG) AS SOLD_PAX_SEG								,");
				sb.append(" 0 AS ONHOLD_PAX_SEG						 						 ");
			}

			sb.append(" FROM ( ");

			// Append for IBE confirm Booking
			if (CollectionUtils.containsAny(reportsSearchCriteria.getSalesChannels(), webAndMobileChannels)) {
				sb.append(" SELECT SC.DESCRIPTION AS DESCRIPTION, PS.MKTING_CARRIER_CODE AS CARRIER_CODE,");
				sb.append(" COUNT(PSE.PNR) AS SOLD_PAX_SEG, ");
				sb.append(" 0 AS ONHOLD_PAX_SEG ");
				sb.append(" FROM T_RESERVATION R, ");
				sb.append(
						" (SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE FROM T_PNR_SEGMENT");
				sb.append("  UNION ALL");
				sb.append(
						"  SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE FROM T_EXT_PNR_SEGMENT");
				sb.append("  ) PS,");
				sb.append(" T_AGENT AG, T_SALES_CHANNEL SC, T_PNR_PASSENGER PSE");
				sb.append(" WHERE ");
				sb.append(" R.PNR = PS.PNR AND ");
				sb.append(" PSE.PNR = PS.PNR AND ");
				sb.append(" PS.MKTING_AGENT_CODE = AG.AGENT_CODE (+) AND ");
				sb.append(" R.STATUS ='CNF' AND ");
				sb.append(" PS.STATUS ='CNF' AND ");
				sb.append(" PSE.PAX_TYPE_CODE !='IN' AND ");
				sb.append(" SC.SALES_CHANNEL_CODE = PS.STATUS_MOD_CHANNEL_CODE AND ");
				sb.append(" PS.STATUS_MOD_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" AND TO_DATE('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.STATUS_MOD_CHANNEL_CODE",
						webAndMobileCommon, false) + "  ");
				sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_CARRIER_CODE",
						reportsSearchCriteria.getCarrierCodes(), false) + "  ");
				sb.append(" GROUP BY SC.DESCRIPTION,  PS.MKTING_CARRIER_CODE  ");

				reportsSearchCriteria.getSalesChannels().removeAll(webAndMobileChannels);

				if (reportsSearchCriteria.getSalesChannels().size() > 0) {
					sb.append(" UNION ALL  ");
				}

				webChannelEnabled = true;
			}
			if (reportsSearchCriteria.getSalesChannels().size() > 0) {
				sb.append(" SELECT SC.DESCRIPTION AS DESCRIPTION, PS.MKTING_CARRIER_CODE AS CARRIER_CODE,");
				sb.append(" COUNT(PSE.PNR) AS SOLD_PAX_SEG, ");
				sb.append(" 0 AS ONHOLD_PAX_SEG ");
				sb.append(" FROM T_RESERVATION R,");
				sb.append(
						" (SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE, MKTING_STATION_CODE FROM T_PNR_SEGMENT");
				sb.append("  UNION ALL");
				sb.append(
						"  SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE, MKTING_STATION_CODE FROM T_EXT_PNR_SEGMENT");
				sb.append("  ) PS,");
				sb.append(" T_AGENT AG, T_SALES_CHANNEL SC, T_PNR_PASSENGER PSE");
				sb.append(" WHERE ");
				sb.append(" R.PNR = PS.PNR AND ");
				sb.append(" PSE.PNR = PS.PNR AND ");
				sb.append(" PS.MKTING_AGENT_CODE = AG.AGENT_CODE (+) AND ");
				sb.append(" R.STATUS ='CNF' AND ");
				sb.append(" PS.STATUS ='CNF' AND ");
				sb.append(" PSE.PAX_TYPE_CODE !='IN' AND ");
				sb.append(" SC.SALES_CHANNEL_CODE = PS.STATUS_MOD_CHANNEL_CODE AND ");
				sb.append(" PS.STATUS_MOD_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
				sb.append(" AND TO_DATE('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
				if (reportsSearchCriteria.getSalesChannels() != null) {
					sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.STATUS_MOD_CHANNEL_CODE",
							reportsSearchCriteria.getSalesChannels(), false) + "  ");
				}
				sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_CARRIER_CODE",
						reportsSearchCriteria.getCarrierCodes(), false) + "  ");
				if (reportsSearchCriteria.getStations() != null && reportsSearchCriteria.getStations().size() > 0) { // if
																														// all
																														// agents
																														// selected,
					// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_STATION_CODE",
							reportsSearchCriteria.getStations(), false) + "  ");
				}
				if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) { // if
																													// all
																													// agents
																													// selected,
					// reportsSearchCriteria.getAgents() == null
					sb.append(" AND ").append(
							ReportUtils.getReplaceStringForIN("PS.MKTING_AGENT_CODE", reportsSearchCriteria.getAgents(), false)
									+ "  ");
				}
				sb.append(" GROUP BY SC.DESCRIPTION,  PS.MKTING_CARRIER_CODE  ");

			}
			if (reportsSearchCriteria.getIncldeOnhold()) {
				if (reportsSearchCriteria.getSalesChannels().size() > 0) {
					sb.append(" UNION ALL  ");

					sb.append(" SELECT SC.DESCRIPTION AS DESCRIPTION, PS.MKTING_CARRIER_CODE AS CARRIER_CODE,");
					sb.append(" 0 AS SOLD_PAX_SEG, ");
					sb.append(" COUNT(PSE.PNR) AS ONHOLD_PAX_SEG ");
					sb.append(" FROM T_RESERVATION R, ");
					sb.append(" (SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE, MKTING_STATION_CODE FROM T_PNR_SEGMENT");
					sb.append("  UNION ALL");
					sb.append("  SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE, MKTING_STATION_CODE FROM T_EXT_PNR_SEGMENT");
					sb.append("  ) PS,");
					sb.append(" T_AGENT AG, T_SALES_CHANNEL SC, T_PNR_PASSENGER PSE");
					sb.append(" WHERE ");
					sb.append(" R.PNR = PS.PNR AND ");
					sb.append(" PSE.PNR = PS.PNR AND ");
					sb.append(" PS.MKTING_AGENT_CODE = AG.AGENT_CODE (+) AND ");
					sb.append(" R.STATUS ='OHD' AND ");
					sb.append(" PS.STATUS ='CNF' AND ");
					sb.append(" PSE.PAX_TYPE_CODE !='IN' AND ");
					sb.append(" SC.SALES_CHANNEL_CODE = PS.STATUS_MOD_CHANNEL_CODE AND ");
					sb.append(" PS.STATUS_MOD_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
					sb.append(" AND TO_DATE('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
					if (reportsSearchCriteria.getSalesChannels() != null) {
						sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.STATUS_MOD_CHANNEL_CODE",
								reportsSearchCriteria.getSalesChannels(), false) + "  ");
					}
					sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_CARRIER_CODE",
							reportsSearchCriteria.getCarrierCodes(), false) + "  ");
					if (reportsSearchCriteria.getStations() != null && reportsSearchCriteria.getStations().size() > 0) { // if
																															// all
																															// agents
																															// selected,
						// reportsSearchCriteria.getAgents() == null
						sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_STATION_CODE",
								reportsSearchCriteria.getStations(), false) + "  ");
					}
					if (reportsSearchCriteria.getAgents() != null && reportsSearchCriteria.getAgents().size() > 0) { // if
																														// all
																														// agents
																														// selected,
						// reportsSearchCriteria.getAgents() == null
						sb.append(" AND ").append(
								ReportUtils.getReplaceStringForIN("PS.MKTING_AGENT_CODE", reportsSearchCriteria.getAgents(),
										false) + "  ");
					}
					sb.append(" GROUP BY SC.DESCRIPTION,  PS.MKTING_CARRIER_CODE  ");

				}

				// Append for IBE Onhold Bookings
				if (webChannelEnabled) {
					sb.append(" UNION ALL  ");
					sb.append(" SELECT SC.DESCRIPTION AS DESCRIPTION, PS.MKTING_CARRIER_CODE AS CARRIER_CODE,");
					sb.append(" 0 AS SOLD_PAX_SEG, ");
					sb.append(" COUNT(PSE.PNR) AS ONHOLD_PAX_SEG ");
					sb.append(" FROM T_RESERVATION R,");
					sb.append(
							" (SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE, MKTING_STATION_CODE FROM T_PNR_SEGMENT");
					sb.append(" UNION ALL");
					sb.append(
							" SELECT STATUS, PNR, MKTING_CARRIER_CODE, STATUS_MOD_DATE, STATUS_MOD_CHANNEL_CODE, MKTING_AGENT_CODE, MKTING_STATION_CODE FROM T_EXT_PNR_SEGMENT");
					sb.append(" ) PS,");
					sb.append(" T_AGENT AG, T_SALES_CHANNEL SC, T_PNR_PASSENGER PSE");
					sb.append(" WHERE ");
					sb.append(" R.PNR = PS.PNR AND ");
					sb.append(" PSE.PNR = PS.PNR AND ");
					sb.append(" PS.MKTING_AGENT_CODE = AG.AGENT_CODE (+) AND ");
					sb.append(" R.STATUS ='OHD' AND ");
					sb.append(" PS.STATUS ='CNF' AND ");
					sb.append(" PSE.PAX_TYPE_CODE !='IN' AND ");
					sb.append(" SC.SALES_CHANNEL_CODE = PS.STATUS_MOD_CHANNEL_CODE AND ");
					sb.append(" PS.STATUS_MOD_DATE BETWEEN TO_DATE('" + startDate + "','dd-mm-yyyy HH24:mi:ss') ");
					sb.append(" AND TO_DATE('" + endDate + "','dd-mm-yyyy HH24:mi:ss') ");
					sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.STATUS_MOD_CHANNEL_CODE",
							webAndMobileCommon, false) + "  ");
					sb.append(" AND ").append(ReportUtils.getReplaceStringForIN("PS.MKTING_CARRIER_CODE",
							reportsSearchCriteria.getCarrierCodes(), false) + "  ");
					sb.append(" GROUP BY SC.DESCRIPTION,  PS.MKTING_CARRIER_CODE  ");
				}
			}
			sb.append(" ) V ");
			sb.append(" GROUP BY V.DESCRIPTION, V.CARRIER_CODE");

		}
		return sb.toString();
	}

	public String getOriginCountryOfCallDataQuery(ReportsSearchCriteria criteria) {
		StringBuilder sb = new StringBuilder();
		String fromDate = criteria.getDateRangeFrom();
		String toDate = criteria.getDateRangeTo();
		List<String> countryCodes = criteria.getSelectedCountries();

		sb.append("SELECT (select country_name from t_country where country_code in (ORIGIN_COUNTRY_OF_CALL)) ")
				.append(" ORIGIN_COUNTRY_OF_CALL,  SUM(CASE WHEN STATUS = 'CNF' THEN 1  ELSE 0 END) AS CNF_COUNT, ")
				.append(" SUM(CASE WHEN STATUS = 'OHD' THEN 1  ELSE 0 END) AS OHD_COUNT FROM T_RESERVATION ")
				.append(" WHERE BOOKING_TIMESTAMP  BETWEEN (TO_DATE('" + fromDate + "00:00:00', ")
				.append(" 'DD-MON-YYYY HH24:MI:SS')) AND (TO_DATE('" + toDate + " 23:59:59', ")
				.append(" 'DD-MON-YYYY HH24:MI:SS')) AND (STATUS = 'CNF' OR STATUS = 'OHD')");

		if (countryCodes != null && !countryCodes.isEmpty()) {
			sb.append(" AND ORIGIN_COUNTRY_OF_CALL IN (");
			sb.append(Util.buildStringInClauseContent(countryCodes));
			sb.append(") ");
		}
		sb.append(" GROUP BY (ORIGIN_COUNTRY_OF_CALL)");
		return sb.toString();
	}

	public String getOriginCountryOfCallDetailQuery(ReportsSearchCriteria criteria) {
		StringBuilder sb = new StringBuilder();
		String fromDate = criteria.getDateRangeFrom();
		String toDate = criteria.getDateRangeTo();
		String originCountry = criteria.getOriginCountry();

		sb.append("SELECT (select country_name from t_country where country_code in (R.ORIGIN_COUNTRY_OF_CALL))")
				.append(" AS ORIGIN_COUNTRY, R.ORIGIN_AGENT_CODE AS AGENT_CODE, R.ORIGIN_USER_ID AS USER_ID, ")
				.append(" R.PNR AS PNR, R.STATUS AS STATUS, TO_CHAR(R.BOOKING_TIMESTAMP, 'YYYY-MM-DD') AS ")
				.append(" BOOKING_DATE, (R.TOTAL_PAX_COUNT+R.TOTAL_PAX_INFANT_COUNT) AS PAX_COUNT, R.TOTAL_FARE  ")
				.append("AS TOTAL_FARE, A.HANDLING_CHARGE_AMOUNT AS HANDLING_FEE FROM T_RESERVATION R INNER JOIN ")
				.append(" T_AGENT A ON R.ORIGIN_AGENT_CODE = A.AGENT_CODE WHERE R.BOOKING_TIMESTAMP  BETWEEN ")
				.append("(TO_DATE('" + fromDate + "00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ")
				.append(" AND (TO_DATE('" + toDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS')) ")
				.append(" AND R.ORIGIN_COUNTRY_OF_CALL = (select country_code from t_country where country_name = (")
				.append("'" + originCountry + "'").append("))").append(" AND (R.STATUS = 'CNF' OR R.STATUS = 'OHD')");

		return sb.toString();
	}


	public String getIncentiveSchemeDataQuery(ReportsSearchCriteria criteria) {
		StringBuilder sb = new StringBuilder();
		String fromDate = criteria.getDateRangeFrom();
		String toDate = criteria.getDateRangeTo();
		Collection<String> setSchemeIDs = criteria.getSchemeCodes();
		List<String> lstScheme = new ArrayList<String>(setSchemeIDs);

		sb.append(" SELECT   a.agent_incentive_scheme_id, a.scheme_name, ");
		sb.append(" TO_CHAR (a.effective_from_date,'DD/MM/YYYY HH24:mm:ss') || '  '|| ");
		sb.append(" TO_CHAR (a.effective_to_date, 'DD/MM/YYYY HH24:mm:ss') effective_period, a.status, ");
		sb.append(" TO_CHAR (a.created_date, 'DD/MM/YYYY HH24:mm:ss') created_date, ");
		sb.append(" DECODE (a.ticket_price_component,'T', 'Fare Only','Fare & Surcharges') || ' '|| ");
		sb.append(" DECODE (a.inc_promo_bookings, 'Y', 'Promo Fares') || ' '|| ");
		sb.append(" DECODE (a.inc_own_trf_bookings, 'Y', 'Ownership Transfer Fares') || ' '|| ");
		sb.append(" DECODE (a.inc_rpt_agent_sales, 'Y', 'Reporting Agent Rev.') || ' '|| ");
		sb.append(" DECODE (a.incentive_basis, 'S', 'All Issued', 'Flown Only') inclutions, ");
		sb.append(" b.range_start_value || ' - ' || b.range_end_value || ' ' || b.percentage ");
		sb.append(" || ' %' slab_detail FROM t_agent_incentive_scheme a, t_agent_incentive_scheme_slab b ");
		sb.append("  WHERE a.agent_incentive_scheme_id = b.agent_incentive_scheme_id ");
		sb.append(" AND " + ReportUtils.getReplaceStringForIN("a.agent_incentive_scheme_id", lstScheme, false) + " ");
		sb.append(" AND (   a.effective_from_date BETWEEN TO_DATE ( ");
		sb.append(fromDate);
		sb.append(" ) AND TO_DATE (");
		sb.append(toDate);
		sb.append(") OR a.effective_to_date BETWEEN TO_DATE ( ");
		sb.append(fromDate);
		sb.append(" ) AND TO_DATE ( ");
		sb.append(toDate);
		sb.append(" )) ");
		sb.append(" ORDER BY a.agent_incentive_scheme_id ");

		return sb.toString();
	}

	public String getAgentIncentiveSchemeDataQuery(ReportsSearchCriteria criteria) {
		StringBuilder sb = new StringBuilder();
		String fromDate = criteria.getDateRangeFrom();
		String toDate = criteria.getDateRangeTo();
		Collection<String> colScheme = criteria.getSchemeCodes();
		Collection<String> colAgent = criteria.getAgents();
		List<String> lstSchemeID = new ArrayList<String>(colScheme);
		List<String> lstAgntCode = new ArrayList<String>(colAgent);
		String repCat = criteria.getReportType();

		if (isNotEmptyOrNull(repCat) && repCat.equals("AGENTSCHEME")) {
			sb.append(" SELECT ag.agent_code, ag.agent_name, ag.credit_limit, ag.status, ");
			sb.append(" inc.agent_incentive_scheme_id, inc.scheme_name, TO_CHAR (inc.effective_from_date, ");
			sb.append(" 'DD/MM/YYYY HH24:mm:ss')|| '  '|| TO_CHAR (inc.effective_to_date, 'DD/MM/YYYY HH24:mm:ss') ");
			sb.append(" effective_period,inc.status SCHEME_STATUS ");
			sb.append(" FROM t_agent ag, t_agent_incentive_scheme inc,t_agent_incentive_schemes sch ");
			sb.append(" where ag.agent_code = sch.agent_code   ");
			sb.append(" and sch.agent_incentive_scheme_id = inc.agent_incentive_scheme_id ");
			sb.append(" and ");
			sb.append(ReportUtils.getReplaceStringForIN("ag.agent_code", lstAgntCode, false));
			sb.append(" and (inc.effective_from_date BETWEEN TO_DATE ( ");
			sb.append(fromDate);
			sb.append(" ) AND TO_DATE (");
			sb.append(toDate);
			sb.append(") OR inc.effective_to_date BETWEEN TO_DATE ( ");
			sb.append(fromDate);
			sb.append(" ) AND TO_DATE ( ");
			sb.append(toDate);
			sb.append(" )) ");
		} else if (isNotEmptyOrNull(repCat) && repCat.equals("AGENTONLY")) {
			sb.append(" SELECT ag.agent_code, ag.agent_name, ag.credit_limit, ag.status, ");
			sb.append(" '' AGENT_INCENTIVE_SCHEME_ID, '' SCHEME_NAME, '' EFFECTIVE_PERIOD, '' SCHEME_STATUS ");
			sb.append(" FROM t_agent ag  ");
			sb.append(" where ag.agent_code not in (select ch.agent_code from t_agent_incentive_schemes ch) ");

		} else if (isNotEmptyOrNull(repCat) && repCat.equals("SCHEMEAGENT")) {
			sb.append("  SELECT ag.agent_code, ag.agent_name, inc.agent_incentive_scheme_id, inc.scheme_name, ");
			sb.append(" TO_CHAR (inc.effective_from_date, 'DD/MM/YYYY HH24:mm:ss')|| '  ' ");
			sb.append(" || TO_CHAR (inc.effective_to_date, 'DD/MM/YYYY HH24:mm:ss')effective_period,inc.status SCHEME_STATUS ");
			sb.append(" FROM t_agent ag, t_agent_incentive_scheme inc,t_agent_incentive_schemes sch ");
			sb.append(" where ag.agent_code = sch.agent_code   ");
			sb.append(" and sch.agent_incentive_scheme_id = inc.agent_incentive_scheme_id ");
			sb.append(" and ");
			sb.append(ReportUtils.getReplaceStringForIN("inc.agent_incentive_scheme_id", lstSchemeID, false));
			sb.append(" and (inc.effective_from_date BETWEEN TO_DATE ( ");
			sb.append(fromDate);
			sb.append(" ) AND TO_DATE (");
			sb.append(toDate);
			sb.append(") OR inc.effective_to_date BETWEEN TO_DATE ( ");
			sb.append(fromDate);
			sb.append(" ) AND TO_DATE ( ");
			sb.append(toDate);
			sb.append(" )) ");
		} else if (isNotEmptyOrNull(repCat) && repCat.equals("SCHEMEONLY")) {
			sb.append(" select inc.agent_incentive_scheme_id, inc.scheme_name, TO_CHAR (inc.effective_from_date,  ");
			sb.append(" 'DD/MM/YYYY HH24:mm:ss')|| '  '|| TO_CHAR (inc.effective_to_date, 'DD/MM/YYYY HH24:mm:ss') ");
			sb.append(" effective_period, inc.status SCHEME_STATUS,'' AGENT_CODE, '' AGENT_NAME  ");
			sb.append(" from t_agent_incentive_scheme inc ");
			sb.append(
					" where inc.agent_incentive_scheme_id not in (select ch.agent_incentive_scheme_id from t_agent_incentive_schemes ch) ");
			sb.append(" and (inc.effective_from_date BETWEEN TO_DATE ( ");
			sb.append(fromDate);
			sb.append(" ) AND TO_DATE (");
			sb.append(toDate);
			sb.append(") OR inc.effective_to_date BETWEEN TO_DATE ( ");
			sb.append(fromDate);
			sb.append(" ) AND TO_DATE ( ");
			sb.append(toDate);
			sb.append(" )) ");
		}
		return sb.toString();
	}

	/**
	 * Creates the SQL query for retrieving the list of airports as per given criteria.
	 * 
	 * @param criteria
	 * @return
	 */
	public String getAirportListDataQuery(ReportsSearchCriteria criteria) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT ");
		sb.append(
				" airport.airport_code ||' - '|| airport.airport_name airport_name, station.station_name, country.country_name, ");
		sb.append(" airport.status, airport.gmt_offset_action, airport.gmt_offset_hours, ");
		sb.append(" airport.lcc_publish_status, airport.min_stopover_time, airport.online_status, ");
		sb.append(
				" airport.anci_notify_start_cutover, airport.anci_notify_end_cutover, NVL(agency.agent_name,'n/a' ) as agent_name, ");
		sb.append(" airport.is_surface_station, NVL(dst.dst_adjust_time,0) as dst_adjust_time ");
		sb.append(" FROM ");
		sb.append(" t_airport airport LEFT OUTER JOIN t_station station ON ");
		sb.append(" station.station_code = airport.station_code ");
		sb.append(" LEFT OUTER JOIN t_country country ON ");
		sb.append(" country.country_code = station.country_code ");
		sb.append(" LEFT OUTER JOIN t_agent agency ON ");
		sb.append(" agency.agent_code = airport.goshow_agent_code ");
		sb.append(" LEFT OUTER JOIN t_airport_dst dst ");
		sb.append(" ON (airport.airport_code = dst.airport_code ");
		sb.append(" AND sysdate BETWEEN dst.dst_start_date_time AND dst.dst_end_date_time) ");

		// selection clause.
		String clause = "WHERE";

		if (isNotEmptyOrNull(criteria.getCountryCode())) {
			// sb.append("AND country.country_code = 'AE'");
			sb.append(String.format(" %s country.country_code = '%s' ", clause, criteria.getCountryCode()));
			clause = "AND";
		}

		if (isNotEmptyOrNull(criteria.getStation())) {
			// sb.append("AND country.country_code = 'AE'");
			sb.append(String.format(" %s airport.station_code = '%s' ", clause, criteria.getStation()));
			clause = "AND";
		}

		if (isNotEmptyOrNull(criteria.getAirportCode())) {
			sb.append(String.format(" %s airport.airport_code = '%s' ", clause, criteria.getAirportCode()));
			clause = "AND";
		}

		if (isNotEmptyOrNull(criteria.getStatus())) {
			// sb.append("AND country.country_code = 'AE'");
			sb.append(String.format(" %s airport.status = '%s' ", clause, criteria.getStatus()));
			clause = "AND";
		}

		if (isNotEmptyOrNull(criteria.getSortByColumnName())) {
			sb.append(String.format(" ORDER BY UPPER(%s) ", criteria.getSortByColumnName()));

			if (isNotEmptyOrNull(criteria.getSortByOrder())) {
				sb.append(String.format(" %s ", criteria.getSortByOrder()));
			}
		} else {
			sb.append(" ORDER BY country.country_name, airport.airport_code");
		}
		return sb.toString();
	}

	/**
	 * Creates the SQL query for retrieving the list of routes as per given criteria.
	 * 
	 * @param criteria
	 * @return
	 */
	public String getRouteDetailDataQuery(ReportsSearchCriteria criteria) {
		StringBuilder sb = new StringBuilder();

		sb.append(
				" SELECT ond_code,distance,duration,duration_tolerance,direct_flag,no_of_transits,status,is_lcc_published,enable_anci_reminders ");
		sb.append(" FROM t_route_info ");

		// selection clause.
		String clause = "WHERE";

		if (isNotEmptyOrNull(criteria.getOriginAirport())) {
			sb.append(String.format(" %s from_airport = '%s' ", clause, criteria.getOriginAirport()));
			clause = "AND";
		}

		if (isNotEmptyOrNull(criteria.getDestinationAirport())) {
			sb.append(String.format(" %s to_airport = '%s' ", clause, criteria.getDestinationAirport()));
			clause = "AND";
		}

		if (((criteria.getOriginAirport() != null && !(criteria.getOriginAirport().equals("")))
				|| criteria.getViaPoints().size() > 0)
				|| (criteria.getDestinationAirport() != null && !(criteria.getDestinationAirport().equals("")))) {

			String routeCode = PlatformUtiltiies.getRouteCode(criteria.getOriginAirport(), criteria.getDestinationAirport(),
					criteria.getViaPoints());
			sb.append(String.format(" %s ond_code LIKE '%s' ", clause, routeCode));

			clause = "AND";

		}

		if (isNotEmptyOrNull(criteria.getStatus())) {
			sb.append(String.format(" %s status = '%s' ", clause, criteria.getStatus()));
			clause = "AND";
		}

		if (isNotEmptyOrNull(criteria.getSortByColumnName())) {
			sb.append(String.format(" ORDER BY UPPER(%s) ", criteria.getSortByColumnName()));
		}

		return sb.toString();
	}

	/**
	 * Gets the Bookes SSR Details stored Proc
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getSSRDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strBookedFromDate = reportsSearchCriteria.getDateRange2From();
		String strBookedToDate = reportsSearchCriteria.getDateRange2To();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();

		Collection<String> ssrCodes = reportsSearchCriteria.getSsrCodes();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();

		sb.append(" SELECT ");
		sb.append("       INITCAP(P.TITLE)||' '||INITCAP(P.FIRST_NAME)||' '||INITCAP(P.LAST_NAME)NAME,");
		sb.append("       TO_CHAR(FS.EST_TIME_DEPARTURE_LOCAL, 'DD-MON-YYYY') AS DEPARTURE_DATE,");
		sb.append("       TO_CHAR(FS.EST_TIME_DEPARTURE_LOCAL, 'HH24:MI') AS DEPARTURE_TIME,");
		sb.append("       F.FLIGHT_NUMBER,");
		sb.append("       P.PNR,");
		sb.append("       SI.SSR_CODE,");
		sb.append("       PPSR.SSR_TEXT");
		sb.append(
				" FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT S, T_RESERVATION R, T_PNR_PASSENGER P, T_SSR_INFO SI, T_PNR_PAX_SEGMENT_SSR PPSR,T_PNR_PAX_FARE_SEGMENT PPFS,T_PNR_PAX_FARE PPF");

		sb.append(" WHERE R.PNR = P.PNR");
		sb.append("       AND S.FLT_SEG_ID = FS.FLT_SEG_ID");
		sb.append("       AND FS.FLIGHT_ID = F.FLIGHT_ID");
		sb.append("       AND SI.SSR_ID = PPSR.SSR_ID");
		sb.append("       AND PPSR.PPFS_ID = PPFS.PPFS_ID");
		sb.append("       AND S.PNR_SEG_ID =  PPFS.PNR_SEG_ID");
		sb.append("       AND PPF.PNR_PAX_ID=P.PNR_PAX_ID");
		sb.append("       AND PPF.PPF_ID = PPFS.PPF_ID");
		// Need to check
		sb.append("       AND P.STATUS = 'CNF' ");
		sb.append("       AND S.STATUS = 'CNF' ");
		sb.append("       AND PPSR.STATUS = 'CNF' ");

		if (isNotEmptyOrNull(strBookedFromDate) && isNotEmptyOrNull(strBookedToDate)) {
			sb.append("       AND R.booking_timestamp BETWEEN TO_DATE('" + strBookedFromDate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookedToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}
		if (isNotEmptyOrNull(strFromdate) && isNotEmptyOrNull(strToDate)) {
			sb.append("       AND FS.EST_TIME_DEPARTURE_LOCAL BETWEEN TO_DATE('" + strFromdate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append("       AND F.FLIGHT_NUMBER = '" + strFlightNo + "' ");
		}

		if (ssrCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" SI.SSR_CODE", ssrCodes, false) + " ");
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" FS.SEGMENT_CODE ", segmentCodes, false) + " ");
		}

		sb.append(" ORDER BY FS.EST_TIME_DEPARTURE_ZULU,FLIGHT_NUMBER ");

		return sb.toString();

	}

	public String getFareDetailsReportQuery(ReportsSearchCriteria reportsSearchCriteria) {
		log.debug("inside getFareDetailsReportQuery ");

		String salEffecFrmDate = reportsSearchCriteria.getDateRangeFrom();
		String salEffecToDate = reportsSearchCriteria.getDateRangeTo();
		String selStatus = reportsSearchCriteria.getStatus();
		String depValidFrmDate = reportsSearchCriteria.getDateRange2From();
		String depValidToDate = reportsSearchCriteria.getDateRange2To();
		String flightWay = reportsSearchCriteria.getOperationType();

		String sortByColumn = reportsSearchCriteria.getSortByColumnName();
		String sortByOrder = reportsSearchCriteria.getSortByOrder();
		String reportType = reportsSearchCriteria.getReportType();
		String cos = reportsSearchCriteria.getCos();

		ArrayList<String> segCodes = (ArrayList<String>) reportsSearchCriteria.getSegmentCodes();
		Collection<String> flightNoColl = reportsSearchCriteria.getFlightNoCollection();

		Collection<String> bcCatColl = reportsSearchCriteria.getBcCategory();

		Collection<String> bcTypeColl = reportsSearchCriteria.getBcTypeCollection();

		Collection<String> allocTypeColl = reportsSearchCriteria.getAllocTypeCollection();

		Collection<String> bcClsColl = reportsSearchCriteria.getBookingCodes();

		Collection<String> fareRulesColl = reportsSearchCriteria.getSsrCodes();

		Collection<String> agentsColl = reportsSearchCriteria.getAgents();

		boolean enableBookingCls = false;
		if ((bcCatColl != null && !bcCatColl.isEmpty() && !bcCatColl.contains("All"))
				|| (bcTypeColl != null && !bcTypeColl.isEmpty() && !bcTypeColl.contains("All"))
				|| (allocTypeColl != null && !allocTypeColl.isEmpty() && !allocTypeColl.contains("All"))
				|| (bcClsColl != null && !bcClsColl.isEmpty() && !bcClsColl.contains("All"))) {
			enableBookingCls = true;
		}

		StringBuilder sb = new StringBuilder();

		if (reportType != null && reportType.equalsIgnoreCase("FARE_DET")) {
			log.debug("FareDetailsReport Selected");

			sb.append(
					"SELECT ab.ond_code,ab.fare_id,gh.description AS CLS_DESCRIPTION,gh.cabin_class_code AS cb_class,lcc.logical_cabin_class_code,ab.effective_from_date,ab.effective_to_date,ab.sales_valid_from,ab.sales_valid_to,");
			sb.append("ab.fare_amount,");
			sb.append(
					"CASE WHEN ab.child_fare_type = 'P' THEN ab.child_fare || '%' ELSE TO_CHAR( NVL( ab.child_fare, 0 ), '9999999999.99' ) END as child_fare,");
			sb.append(
					"CASE WHEN ab.infant_fare_type = 'P' THEN ab.infant_fare || '%' ELSE TO_CHAR( NVL( ab.infant_fare, 0 ), '9999999999.99' ) END as infant_fare,");
			sb.append(
					"ab.status,cd.fare_rule_code,ab.booking_code,kl.description as CHAL_DESCRIPTION,ab.rt_effective_from_date,ab.rt_effective_to_date,cd.fare_basis_code ");
			sb.append(
					"FROM t_ond_fare ab,t_fare_rule cd,t_booking_class ef,t_logical_cabin_class lcc,t_cabin_class gh,t_fare_visibility ij,t_sales_channel kl ");

			if (agentsColl != null && !agentsColl.isEmpty()) {
				sb.append(",t_fare_agent fa ");
			}

			sb.append("WHERE ab.fare_rule_id=cd.fare_rule_id ");
			sb.append("and ab.booking_code = ef.booking_code ");
			sb.append("and ef.cabin_class_code = gh.cabin_class_code ");
			sb.append("and ab.fare_rule_id = ij.fare_rule_id ");
			sb.append("and ij.sales_channel_code = kl.sales_channel_code ");
			sb.append("and lcc.logical_cabin_class_code = ef.logical_cabin_class_code ");
			sb.append("and lcc.cabin_class_code = gh.cabin_class_code ");

			if (isNotEmptyOrNull(salEffecFrmDate) && isNotEmptyOrNull(salEffecToDate)) {
				if (log.isDebugEnabled()) {
					log.debug("Sales Effective Date FILTER ++ " + salEffecFrmDate + " - " + salEffecToDate);
				}

				sb.append("AND (ab.sales_valid_from BETWEEN TO_DATE ('" + salEffecFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + salEffecToDate + "', 'dd-mon-yyyy hh24:mi:ss') ");

				sb.append("OR ab.sales_valid_to BETWEEN TO_DATE ('" + salEffecFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + salEffecToDate + "', 'dd-mon-yyyy hh24:mi:ss'))  ");

			}

			if (isNotEmptyOrNull(selStatus) && (!selStatus.equalsIgnoreCase("ALL"))) {
				if (log.isDebugEnabled()) {
					log.debug("status FILTER ++ " + selStatus);
				}
				sb.append("AND ab.status='" + selStatus + "' ");
			}

			if (isNotEmptyOrNull(depValidFrmDate) && isNotEmptyOrNull(depValidToDate)) {
				sb.append("AND ((ab.effective_from_date >= TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND ab.effective_to_date    >= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND ab.effective_from_date  <= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') ) ");
				sb.append("OR (ab.effective_from_date  <=TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND ab.effective_to_date    <=TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND ab.effective_to_date    >=TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss')) ");
				sb.append("OR (ab.effective_from_date  <=TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND ab.effective_to_date    >=TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') ) ");
				sb.append("OR (ab.effective_from_date  >=TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND ab.effective_to_date    <=TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss'))) ");

			}

			if (segCodes != null && !segCodes.isEmpty()) {
				sb.append("AND ab.ond_code IN ( ");
				sb.append(Util.buildStringInClauseContent(segCodes));
				sb.append(") ");
			}

			if (isNotEmptyOrNull(cos) && !cos.equalsIgnoreCase("All")) {
				sb.append(" AND ef.cabin_class_code      = '" + cos + "' ");
			}

			if (flightNoColl != null && !flightNoColl.isEmpty() && !flightNoColl.contains("All")) {

				sb.append("and ab.ond_code in (SELECT  pq.ond_code FROM t_ond pq,t_flight rs WHERE rs.origin = pq.origin ");
				sb.append("and rs.destination = pq.destination ");
				sb.append("AND rs.flight_number IN ( ");
				sb.append(Util.buildStringInClauseContent(flightNoColl));
				sb.append(")) ");
			}

			if (bcCatColl != null && !bcCatColl.isEmpty() && !bcCatColl.contains("All")) {

				if (bcCatColl.contains("Fixed")) {
					sb.append("AND ef.fixed_flag = 'Y' ");
				} else {
					sb.append("AND ef.fixed_flag = 'N' ");
				}

				if (bcCatColl.contains("Non-Standard") && bcCatColl.contains("Standard")) {
					sb.append("AND (ef.standard_code = 'N' OR ef.standard_code = 'Y') ");
				} else if (bcCatColl.contains("Non-Standard")) {
					sb.append("AND ef.standard_code = 'N' ");
				} else if (bcCatColl.contains("Standard")) {
					sb.append("AND ef.standard_code = 'Y' ");
				}
			}

			if (bcTypeColl != null && !bcTypeColl.isEmpty() && !bcTypeColl.contains("All")) {
				sb.append("AND ef.bc_type IN ( ");
				sb.append(Util.buildStringInClauseContent(bcTypeColl));
				sb.append(") ");
			}

			if (allocTypeColl != null && !allocTypeColl.isEmpty() && !allocTypeColl.contains("All")) {
				sb.append("AND ef.allocation_type IN ( ");
				sb.append(Util.buildStringInClauseContent(allocTypeColl));
				sb.append(") ");

			}

			if (bcClsColl != null && !bcClsColl.isEmpty() && !bcClsColl.contains("All")) {
				sb.append("AND ef.booking_code IN ( ");
				sb.append(Util.buildStringInClauseContent(bcClsColl));
				sb.append(") ");

			}

			if (fareRulesColl != null && !fareRulesColl.isEmpty() && !fareRulesColl.contains("All")) {
				sb.append("AND cd.fare_rule_code IN ( ");
				sb.append(Util.buildStringInClauseContent(fareRulesColl));
				sb.append(") ");

			}

			if (agentsColl != null && !agentsColl.isEmpty()) {
				sb.append("AND cd.fare_rule_id = fa.fare_rule_id(+) ");
				sb.append("AND ((kl.sales_channel_code <>'" + SalesChannelsUtil.SALES_CHANNEL_AGENT
						+ "') OR (kl.sales_channel_code    ='" + SalesChannelsUtil.SALES_CHANNEL_AGENT + "' ");
				sb.append(" AND ").append(
						ReportUtils.getReplaceStringForIN("fa.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
				sb.append(" )) ");
			}

			if (flightWay != "" && flightWay.equalsIgnoreCase("ONEWAY")) {
				sb.append("AND cd.return_flag = 'N' ");
			} else if (flightWay != "" && flightWay.equalsIgnoreCase("RETURN")) {
				sb.append("AND cd.return_flag = 'Y' ");
			}

			if (isNotEmptyOrNull(sortByColumn)) {

				if (sortByColumn.equalsIgnoreCase("OND")) {
					sortByColumn = "ab.ond_code";
				} else if (sortByColumn.equalsIgnoreCase("SALVALDATE")) {
					sortByColumn = "ab.sales_valid_from";
				} else if (sortByColumn.equalsIgnoreCase("DEPVAL")) {
					sortByColumn = "ab.effective_from_date";
				} else if (sortByColumn.equalsIgnoreCase("FARRUL")) {
					sortByColumn = "cd.fare_rule_code";
				} else if (sortByColumn.equalsIgnoreCase("BKCLS")) {
					sortByColumn = "ab.booking_code";
				}

				sb.append(String.format(" ORDER BY %s ", sortByColumn));

				if (isNotEmptyOrNull(sortByOrder)) {
					sb.append(String.format(" %s  ", sortByOrder));
				}
			}

		} else {

			log.debug("FlightWiseFareDetailsReport Selected");

			sb.append("SELECT flg_tab.flight_id,");
			sb.append("flg_tab.flight_number,");
			sb.append("flg_tab.departure_date,");
			sb.append("fcc_tab.segment_code,");
			sb.append("cbcls_tab.description,");
			sb.append("cbcls_tab.cabin_class_code AS cb_class,");
			sb.append("lcc.logical_cabin_class_code, ");
			sb.append("far_tab.effective_from_date,");
			sb.append("far_tab.effective_to_date,");
			sb.append("far_tab.sales_valid_from,");
			sb.append("far_tab.sales_valid_to,");
			sb.append("far_tab.fare_amount,");
			sb.append("CASE WHEN far_tab.child_fare_type = 'P' THEN far_tab.child_fare || '%' ELSE TO_CHAR( NVL( far_tab.child_fare, 0 ), '9999999999.99' ) END as child_fare,");
			sb.append("CASE WHEN far_tab.infant_fare_type = 'P' THEN far_tab.infant_fare || '%' ELSE TO_CHAR( NVL( far_tab.infant_fare, 0 ), '9999999999.99' ) END as infant_fare,");
			sb.append("frle_tab.fare_rule_code,");
			sb.append("fcc_tab.booking_code,");
			sb.append("slch_tab.description AS CHAL_DESCRIPTION,");
			sb.append("far_tab.fare_id,");
			sb.append("far_tab.rt_effective_from_date,");
			sb.append("far_tab.rt_effective_to_date,frle_tab.fare_basis_code ");
			sb.append("FROM t_fcc_seg_bc_alloc fcc_tab,");
			sb.append("t_logical_cabin_class lcc,");
			sb.append("t_flight flg_tab,");
			sb.append("t_cabin_class cbcls_tab,");
			sb.append("t_ond_fare far_tab,");
			sb.append("t_fare_rule frle_tab,");
			sb.append("t_sales_channel slch_tab ");
			if (enableBookingCls) {
				sb.append(",t_booking_class bcls_tab ");
			}

			if (agentsColl != null && !agentsColl.isEmpty()) {
				sb.append(",t_fare_agent fa ");
			}

			sb.append("WHERE flg_tab.flight_id = fcc_tab.flight_id ");
			sb.append("AND lcc.cabin_class_code    = cbcls_tab.cabin_class_code ");
			sb.append("AND fcc_tab.booking_code        = far_tab.booking_code ");
			sb.append("AND fcc_tab.segment_code        = far_tab.ond_code ");
			sb.append("AND frle_tab.fare_rule_id       = far_tab.fare_rule_id ");
			sb.append("AND lcc.logical_cabin_class_code = fcc_tab.logical_cabin_class_code ");
			sb.append("AND far_tab.fare_rule_id in (select fvsb_tab.fare_rule_id from t_fare_visibility fvsb_tab where fvsb_tab.sales_channel_code = slch_tab.sales_channel_code) ");
			if (enableBookingCls) {
				sb.append("AND fcc_tab.booking_code = bcls_tab.booking_code ");
			}

			if (isNotEmptyOrNull(salEffecFrmDate) && isNotEmptyOrNull(salEffecToDate)) {
				if (log.isDebugEnabled()) {
					log.debug("salEffecFrmDate FILTER Added - " + salEffecFrmDate + " - " + salEffecToDate);
				}

				sb.append("AND (far_tab.sales_valid_from BETWEEN TO_DATE ('" + salEffecFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + salEffecToDate + "', 'dd-mon-yyyy hh24:mi:ss') ");

				sb.append("OR far_tab.sales_valid_to BETWEEN TO_DATE ('" + salEffecFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + salEffecToDate + "', 'dd-mon-yyyy hh24:mi:ss'))  ");
			}

			if (isNotEmptyOrNull(selStatus) && (!selStatus.equalsIgnoreCase("ALL"))) {
				if (log.isDebugEnabled()) {
					log.debug("status FILTER Added - " + selStatus);
				}
				sb.append("AND far_tab.status='" + selStatus + "' ");
			}

			if (isNotEmptyOrNull(depValidFrmDate) && isNotEmptyOrNull(depValidToDate)) {
				sb.append("AND ((far_tab.effective_from_date >= TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND far_tab.effective_to_date    >= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND far_tab.effective_from_date  <= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') ) ");
				sb.append("OR (far_tab.effective_from_date  <= TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND far_tab.effective_to_date    <= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND far_tab.effective_to_date    >= TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss')) ");
				sb.append("OR (far_tab.effective_from_date  <= TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND far_tab.effective_to_date    >= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') ) ");
				sb.append("OR (far_tab.effective_from_date  >= TO_DATE ('" + depValidFrmDate
						+ "', 'dd-mon-yyyy hh24:mi:ss') AND far_tab.effective_to_date    <= TO_DATE ('" + depValidToDate
						+ "', 'dd-mon-yyyy hh24:mi:ss'))) ");

			}

			if (segCodes != null && !segCodes.isEmpty()) {
				sb.append("AND fcc_tab.segment_code IN ( ");
				sb.append(Util.buildStringInClauseContent(segCodes));
				sb.append(") ");
			}

			if (isNotEmptyOrNull(cos) && !cos.equalsIgnoreCase("All")) {
				sb.append(" AND fcc_tab.cabin_class_code      = '" + cos + "' ");
			}

			if (flightNoColl != null && !flightNoColl.isEmpty() && !flightNoColl.contains("All")) {
				sb.append("AND flg_tab.flight_number IN ( ");
				sb.append(Util.buildStringInClauseContent(flightNoColl));
				sb.append(" ) ");
			}

			if (bcCatColl != null && !bcCatColl.isEmpty() && !bcCatColl.contains("All")) {

				if (bcCatColl.contains("Fixed")) {
					sb.append("AND bcls_tab.fixed_flag = 'Y' ");
				} else {
					sb.append("AND bcls_tab.fixed_flag = 'N' ");
				}

				if (bcCatColl.contains("Non-Standard") && bcCatColl.contains("Standard")) {
					sb.append("AND (bcls_tab.standard_code = 'N' OR bcls_tab.standard_code = 'Y') ");
				} else if (bcCatColl.contains("Non-Standard")) {
					sb.append("AND bcls_tab.standard_code = 'N' ");
				} else if (bcCatColl.contains("Standard")) {
					sb.append("AND bcls_tab.standard_code = 'Y' ");
				}
			}

			if (bcTypeColl != null && !bcTypeColl.isEmpty() && !bcTypeColl.contains("All")) {
				sb.append("AND bcls_tab.bc_type IN ( ");
				sb.append(Util.buildStringInClauseContent(bcTypeColl));
				sb.append(") ");
			}

			if (allocTypeColl != null && !allocTypeColl.isEmpty() && !allocTypeColl.contains("All")) {
				sb.append("AND bcls_tab.allocation_type IN ( ");
				sb.append(Util.buildStringInClauseContent(allocTypeColl));
				sb.append(") ");

			}

			if (bcClsColl != null && !bcClsColl.isEmpty() && !bcClsColl.contains("All")) {
				sb.append("AND fcc_tab.booking_code IN ( ");
				sb.append(Util.buildStringInClauseContent(bcClsColl));
				sb.append(") ");

			}

			if (fareRulesColl != null && !fareRulesColl.isEmpty() && !fareRulesColl.contains("All")) {
				sb.append("AND frle_tab.fare_rule_code IN ( ");
				sb.append(Util.buildStringInClauseContent(fareRulesColl));
				sb.append(") ");

			}

			if (agentsColl != null && !agentsColl.isEmpty()) {
				sb.append("AND frle_tab.fare_rule_id = fa.fare_rule_id(+) ");
				sb.append("AND ((slch_tab.sales_channel_code <>'" + SalesChannelsUtil.SALES_CHANNEL_AGENT
						+ "') OR (slch_tab.sales_channel_code    ='" + SalesChannelsUtil.SALES_CHANNEL_AGENT + "' ");
				sb.append(" AND ").append(
						ReportUtils.getReplaceStringForIN("fa.agent_code", reportsSearchCriteria.getAgents(), false) + "  ");
				sb.append(" )) ");
			}

			if (flightWay != "" && flightWay.equalsIgnoreCase("ONEWAY")) {
				sb.append("AND frle_tab.return_flag = 'N' ");
			} else if (flightWay != "" && flightWay.equalsIgnoreCase("RETURN")) {
				sb.append("AND frle_tab.return_flag = 'Y' ");
			}

			if (isNotEmptyOrNull(sortByColumn)) {

				if (sortByColumn.equalsIgnoreCase("OND")) {
					sortByColumn = "fcc_tab.segment_code";
				} else if (sortByColumn.equalsIgnoreCase("SALVALDATE")) {
					sortByColumn = "far_tab.sales_valid_from";
				} else if (sortByColumn.equalsIgnoreCase("DEPVAL")) {
					sortByColumn = "far_tab.effective_from_date";
				} else if (sortByColumn.equalsIgnoreCase("FARRUL")) {
					sortByColumn = "frle_tab.fare_rule_code";
				} else if (sortByColumn.equalsIgnoreCase("BKCLS")) {
					sortByColumn = "fcc_tab.booking_code";
				}

				sb.append(String.format(" ORDER BY %s ", sortByColumn));

				if (isNotEmptyOrNull(sortByOrder)) {
					sb.append(String.format(" %s  ", sortByOrder));
				}
			}

		}

		return sb.toString();
	}

	public String getAppParameterDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ");
		sb.append(" param_key as PARAM_KEY, description as PARAM_DESC, param_value as PARAM_VALUE, ");
		sb.append(" editable as PARAM_EDITABLE, param_type as PARAM_TYPE from t_app_parameter ");
		if (reportsSearchCriteria.getParamSelectionType() != null) {
			if (reportsSearchCriteria.getParamSelectionType().equals("Key")) {
				sb.append(" WHERE param_key IN ( ");
				List<String> keys = reportsSearchCriteria.getParamDataList();
				for (int i = 0; i < keys.size(); i++) {
					if (i == 0) {
						sb.append(String.format(" '%s' , ", keys.get(0)));
					}
					if (i == (keys.size() - 1)) {
						sb.append(String.format(" '%s' ", keys.get(i)));
					} else {
						sb.append(String.format(" '%s' , ", keys.get(i)));
					}
				}
				sb.append(" ) ");
			} else if (reportsSearchCriteria.getParamSelectionType().equals("Description")) {
				sb.append(" WHERE description IN ( ");
				List<String> keys = reportsSearchCriteria.getParamDataList();
				for (int i = 0; i < keys.size(); i++) {
					if (i == 0) {
						sb.append(String.format(" '%s' , ", keys.get(0)));
					}
					if (i == (keys.size() - 1)) {
						sb.append(String.format(" '%s' ", keys.get(i)));
					} else {
						sb.append(String.format(" '%s' , ", keys.get(i)));
					}
				}
				sb.append(" ) ");
			}
		} else {
			sb.append(" ORDER BY PARAM_KEY");
		}
		return sb.toString();
	}

	public String getAgentHandlingFeeDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT r.pnr                                       AS pnr          ,");
		sb.append("	fs.segment_code                                    AS segment_code ,");
		sb.append("	ROUND(sum (ppsc.amount ) ,2)                       AS hf_amount    ,");
		sb.append(" AG.agent_code                                      AS agent_code   ,");
		sb.append(" AG.agent_name                                      AS agent_name   ,");
		sb.append(" ST.station_name                                    AS station_name ,");
		sb.append(" cr.rate_journey_type                               AS journey_Type ,");
		sb.append(" TO_CHAR (R.booking_timestamp,'DD/mm/YYYY HH24:mi') AS booking_date ,");
		sb.append(" ps.status                                          AS segment_status, ");
		sb.append(" ppoc.operation_type                                AS operation");
		sb.append(" FROM t_reservation r       ,");
		sb.append(" t_pnr_passenger pp          ,");
		sb.append(" t_pnr_pax_fare ppf          ,");
		sb.append(" t_pnr_pax_fare_segment ppfs ,");
		sb.append(" t_pnr_segment ps            ,");
		sb.append(" t_pnr_pax_seg_charges ppsc  ,");
		sb.append(" t_pnr_pax_ond_charges ppoc  ,");
		sb.append(" t_charge_rate CR            ,");
		sb.append(" t_flight_segment FS         ,");
		sb.append(" t_agent AG                  ,");
		sb.append(" t_station ST				");
		sb.append(" WHERE F_REVENUE_TAX_REPORT(PP.PNR_PAX_ID,PS.STATUS,PP.PAX_TYPE_CODE)='YES' ");
		sb.append(" AND R.origin_agent_code IS NOT NULL ");
		sb.append(" AND r.origin_agent_code = AG.agent_code ");
		sb.append(" AND AG.airline_code = '" + AppSysParamsUtil.getDefaultAirlineIdentifierCode() + "'");
		sb.append(" AND AG.station_code         = ST.station_code");
		sb.append(" AND PP.PNR                  = R.PNR");
		sb.append(" AND PPF.PNR_PAX_ID          = PP.PNR_PAX_ID");
		sb.append(" AND ppfs.ppf_id             = ppf.ppf_id");
		sb.append(" AND ppsc.ppfs_id            = ppfs.ppfs_id");
		sb.append(" AND ps.pnr_seg_id           = ppfs.pnr_seg_id");
		sb.append(" AND ppoc.pft_id             = ppsc.pft_id");
		sb.append(" AND cr.charge_code          ='HC'");
		sb.append(" AND cr.charge_rate_id       = ppoc.charge_rate_id");
		sb.append(" AND PS.flt_seg_id           = FS.flt_seg_id");
		if (isNotEmptyOrNull(reportsSearchCriteria.getDateRangeFrom())
				&& isNotEmptyOrNull(reportsSearchCriteria.getDateRangeTo())) {
			sb.append(" AND R.booking_timestamp BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
					+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
					+ "', 'dd-mon-yyyy hh24:mi:ss') ");
		}
		if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append(
					" AND " + ReportUtils.getReplaceStringForIN("R.origin_agent_code", reportsSearchCriteria.getAgents(), false));
		}

		if (isNotEmptyOrNull(reportsSearchCriteria.getJourneyType())) {
			sb.append(" AND cr.rate_journey_type ='" + reportsSearchCriteria.getJourneyType() + "' ");
		}
		if (isNotEmptyOrNull(reportsSearchCriteria.getStation())) {
			sb.append(" AND ST.station_code ='" + reportsSearchCriteria.getStation() + "' ");
		}
		if (ChargeRateReservationFlows.CREATE == reportsSearchCriteria.getReservationFlow()
				|| ChargeRateReservationFlows.MODIFY == reportsSearchCriteria.getReservationFlow()) {
			
			if(ChargeRateReservationFlows.MODIFY == reportsSearchCriteria.getReservationFlow()){
				sb.append(" AND " + ReportUtils.getReplaceStringForIN("ppoc.operation_type",
						ModifyOperation.getReservationModifyingOperations(), false));
			}else{
				
				sb.append(" AND ppoc.operation_type = '" + ChargeRateReservationFlows.CREATE + "' ");
			}
		}
		sb.append(" GROUP BY r.pnr    ,");
		sb.append("   fs.segment_code ,");
		sb.append("   AG.agent_code ,");
		sb.append("   AG.agent_name , ");
		sb.append("   ST.station_name   , ");
		sb.append("   cr.rate_journey_type  ,   ");
		sb.append("   R.booking_timestamp , ");
		sb.append("   ps.status,");
		sb.append("	  ppoc.operation_type");
		sb.append(" ORDER BY r.pnr    ,");
		sb.append("   fs.segment_code ,");
		sb.append("   AG.agent_code ");
		return sb.toString();
	}

	/**
	 * Creates the SQL query for charge adjustments report.
	 * 
	 * @param reportsSearchCriteria
	 *            : The reports search criteria.
	 * @return : The sql query string.
	 */
	public String getChargeAdjustmentsDataQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT res.pnr as pnr, ");
		sb.append("  ag.agent_code as agent_code, ");
		sb.append("  pax.first_name || ' ' || pax.last_name as pax_name, ");
		sb.append("  F_COMPANY_PYMT_REPORT(res.pnr) as routing, ");
		sb.append("  ag.agent_name as agent_name, ");
		sb.append("  ppoc.user_id as user_id, ");
		sb.append("  usr.display_name as user_name, ");
		sb.append("  pax.total_fare as fare,  ");
		sb.append("  '" + AppSysParamsUtil.getBaseCurrency() + "' as local_currency,  ");
		sb.append("  ppoc.amount as ca_amount, ");
		sb.append("  charge.charge_description as charge_description, ");
		sb.append("  charge.charge_code as charge_code, ");
		sb.append(" TO_CHAR (ppoc.charge_date,'DD/mm/YYYY HH24:mi') AS charge_date, ");
		sb.append(" res.status as status ");
		sb.append(" FROM t_reservation res, ");
		sb.append("  t_pnr_passenger pax, ");
		sb.append("  t_pnr_pax_fare ppf, ");
		sb.append("  t_pnr_pax_ond_charges ppoc, ");
		sb.append("  t_charge charge, ");
		sb.append("  t_charge_rate crate, ");
		sb.append("  t_user usr, ");
		sb.append("  t_agent ag ");
		sb.append(" WHERE res.pnr             = pax.pnr ");
		sb.append(" AND pax.pnr_pax_id        = ppf.pnr_pax_id ");
		sb.append(" AND ppoc.agent_code       = ag.agent_code ");
		sb.append(" AND ppf.ppf_id            = ppoc.ppf_id ");
		sb.append(" AND crate.charge_rate_id  = ppoc.charge_rate_id ");
		sb.append(" AND charge.charge_code    = crate.charge_code ");
		sb.append(" AND usr.user_id = ppoc.user_id ");
		sb.append(" AND ppoc.is_refundable_operation = 'N' ");
		sb.append(" AND res.status IN ( 'CNF' ,'CNX') ");
		sb.append(" AND ppoc.charge_date BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
				+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
				+ "', 'dd-mon-yyyy hh24:mi:ss') ");

		// filter by the charge codes in t_charge.
		if (reportsSearchCriteria.getSelectedChargeCodes() != null && !reportsSearchCriteria.getSelectedChargeCodes().isEmpty()) {
			sb.append(" AND " + ReportUtils.getReplaceStringForIN("charge.charge_code",
					reportsSearchCriteria.getSelectedChargeCodes(), false));
		}

		// filter by the agent in the t_reservation.
		if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append(" AND " + ReportUtils.getReplaceStringForIN("ppoc.agent_code", reportsSearchCriteria.getAgents(), false));
		}

		sb.append(" ORDER BY res.pnr, ");
		sb.append("   ppoc.charge_date, ");
		sb.append("   ag.agent_code, ppoc.user_id ");
		return sb.toString();
	}

	/**
	 * Creates the SQL query for Booked SSR Summary Report
	 * 
	 * @param reportsSearchCriteria
	 *            : The reports search criteria.
	 * @return : The sql query string.
	 */
	public String getBookedSSRSummaryQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strBookedFromDate = reportsSearchCriteria.getDateRange2From();
		String strBookedToDate = reportsSearchCriteria.getDateRange2To();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();

		String ssrCategory = reportsSearchCriteria.getSsrCategory();

		Collection<String> ssrCodes = reportsSearchCriteria.getSsrCodes();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();
		Collection<String> pfsStatusList = reportsSearchCriteria.getPaxStatusList();

		sb.append("SELECT ppfs.booking_code AS BOOKING_CODE, fs.est_time_departure_local AS DEPARTURE_DATE,"
				+ "TO_CHAR(fs.est_time_departure_local, 'HH24:MI') AS DEPARTURE_TIME,"
				+ "fs.est_time_arrival_local AS ARRIVAL_DATE,TO_CHAR(fs.est_time_arrival_local, 'HH24:MI') AS ARRIVAL_TIME,"
				+ "f.flight_number AS FLIGHT_NO,ppss.airport_code AS AIRPORT_CODE,"
				+ "fs.segment_code AS SEGMENT_CODE,sinf.ssr_code AS SSR_CODE,"
				+ "sinf.ssr_desc AS SSR_DESC,COUNT(DECODE( PAX.pax_type_code,'AD',PAX.pnr_pax_id,NULL))ADS,"
				+ "COUNT(DECODE( PAX.pax_type_code,'CH',PAX.pnr_pax_id,NULL))CHS,"
				+ "COUNT(DECODE( PAX.pax_type_code,'IN',PAX.pnr_pax_id,NULL))INS, " + "SUM(ppss.charge_amount) AS AMOUNT, "
				+ "res.pnr AS PNR, ppfs.pax_status AS PAX_STATUS ");
		sb.append("FROM t_flight_segment fs , t_flight f , t_pnr_segment ps , t_pnr_pax_fare_segment ppfs, "
				+ "t_pnr_pax_fare ppf ,t_ssr_info sinf , t_ssr_sub_category scat, t_pnr_passenger pax ,"
				+ "t_pnr_pax_segment_ssr ppss,t_reservation res ");
		sb.append("WHERE pax.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id AND ppfs.pnr_seg_id = ps.pnr_seg_id "
				+ "AND ps.flt_seg_id = fs.flt_seg_id AND fs.flight_id = f.flight_id AND ppss.ppfs_id = ppfs.ppfs_id "
				+ "AND ppss.ssr_id= sinf.ssr_id AND sinf.ssr_sub_cat_id = scat.ssr_sub_cat_id ");

		if (!reportsSearchCriteria.isIncludeCancelledDataToReport()) {
			sb.append("AND ps.status <> 'CNX' AND pax.status <>'CNX' AND ppss.status <> 'CNX' ");
		}
		sb.append("AND res.pnr = pax.pnr ");

		if (isNotEmptyOrNull(strFromdate) && isNotEmptyOrNull(strToDate)) {
			sb.append("AND fs.est_time_departure_local BETWEEN TO_DATE('" + strFromdate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(strBookedFromDate) && isNotEmptyOrNull(strBookedToDate)) {
			sb.append("AND res.booking_timestamp BETWEEN TO_DATE('" + strBookedFromDate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookedToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(ssrCategory)) {
			sb.append("AND scat.ssr_cat_id =" + ssrCategory + " ");
		}

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append("AND f.flight_number='" + strFlightNo + "' ");
		}

		sb.append("AND ");
		sb.append(ReportUtils.getReplaceStringForIN(" sinf.ssr_code ", ssrCodes, false) + " ");

		if (pfsStatusList != null) {
			sb.append("AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" ppfs.pax_status ", pfsStatusList, false) + " ");
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" fs.segment_code ", segmentCodes, false) + " ");
		}

		sb.append("GROUP BY fs.est_time_departure_local, fs.est_time_arrival_local, f.flight_number ,ppss.airport_code, fs.segment_code, sinf.ssr_code, sinf.ssr_desc,  ppfs.booking_code,res.pnr , ppfs.pax_status ");
		sb.append("ORDER BY fs.est_time_departure_local,f.flight_number,ppss.airport_code ");

		return sb.toString();

	}

	/**
	 * Creates the SQL query for Booked SSR Detail Report
	 * 
	 * @param reportsSearchCriteria
	 *            : The reports search criteria.
	 * @return : The sql query string.
	 */
	public String getBookedSSRDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String strBookedFromDate = reportsSearchCriteria.getDateRange2From();
		String strBookedToDate = reportsSearchCriteria.getDateRange2To();
		String strFlightNo = reportsSearchCriteria.getFlightNumber();

		String ssrCategory = reportsSearchCriteria.getSsrCategory();

		Collection<String> ssrCodes = reportsSearchCriteria.getSsrCodes();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();
		Collection<String> paxStatusList = reportsSearchCriteria.getPaxStatusList();

		sb.append("SELECT ppfs.booking_code AS BOOKING_CODE, fs.est_time_departure_local AS DEPARTURE_DATE,"
				+ "TO_CHAR(fs.est_time_departure_local, 'HH24:MI') AS DEPARTURE_TIME,"
				+ "fs.est_time_arrival_local AS ARRIVAL_DATE,TO_CHAR(fs.est_time_arrival_local, 'HH24:MI') AS ARRIVAL_TIME,"
				+ "f.flight_number AS FLIGHT_NO,fs.segment_code AS SEG_CODE,ppss.airport_code AS AIRPORT,pax.pnr AS PNR,"
				+ "INITCAP (pax.title||'.'||pax.first_name|| ' '|| pax.last_name) AS PAX_NAME,"
				+ "sinf.ssr_code AS SSR_CODE,sinf.ssr_desc AS SSR_DESC,ppss.ssr_text AS SSR_TEXT, "
				+ "ppss.charge_amount AS AMOUNT, ppfs.PAX_STATUS ");
		sb.append("FROM t_flight_segment fs , t_flight f , t_pnr_segment ps , t_pnr_pax_fare_segment ppfs, "
				+ "t_pnr_pax_fare ppf ,t_ssr_info sinf , t_ssr_sub_category scat, t_pnr_passenger pax ,"
				+ "t_pnr_pax_segment_ssr ppss,t_reservation res ");
		sb.append("WHERE pax.pnr_pax_id = ppf.pnr_pax_id AND ppf.ppf_id = ppfs.ppf_id AND ppfs.pnr_seg_id = ps.pnr_seg_id "
				+ "AND ps.flt_seg_id = fs.flt_seg_id AND fs.flight_id = f.flight_id AND ppss.ppfs_id = ppfs.ppfs_id "
				+ "AND ppss.ssr_id= sinf.ssr_id AND sinf.ssr_sub_cat_id = scat.ssr_sub_cat_id ");
		if (!reportsSearchCriteria.isIncludeCancelledDataToReport()) {
			sb.append("AND ps.status <> 'CNX' AND pax.status <>'CNX' AND ppss.status <> 'CNX' ");
		}
		sb.append("AND res.pnr = pax.pnr ");
		if (isNotEmptyOrNull(strFromdate) && isNotEmptyOrNull(strToDate)) {
			sb.append("AND fs.est_time_departure_local BETWEEN TO_DATE('" + strFromdate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(strBookedFromDate) && isNotEmptyOrNull(strBookedToDate)) {
			sb.append("AND res.booking_timestamp BETWEEN TO_DATE('" + strBookedFromDate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strBookedToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(ssrCategory)) {
			sb.append("AND scat.ssr_cat_id =" + ssrCategory + " ");
		}

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append("AND f.flight_number='" + strFlightNo + "' ");
		}

		sb.append("AND ");
		sb.append(ReportUtils.getReplaceStringForIN(" sinf.ssr_code ", ssrCodes, false) + " ");

		if (paxStatusList != null) {
			sb.append("AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" ppfs.pax_status ", paxStatusList, false) + " ");
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" fs.segment_code ", segmentCodes, false) + " ");
		}
		sb.append("ORDER BY fs.est_time_departure_local,f.flight_number,ppss.airport_code,pax.pnr");

		return sb.toString();

	}

	public String getAirportTransferDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String strDepFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strDepToDate = reportsSearchCriteria.getDateRangeTo();

		String strTransferFromDate = reportsSearchCriteria.getDateRange2From();
		String strTransferToDate = reportsSearchCriteria.getDateRange2To();

		String strFlightNo = reportsSearchCriteria.getFlightNumber();

		Collection<String> aptCodes = reportsSearchCriteria.getLstAptTransferCodes();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();

		boolean includeOnlyConfirmed = reportsSearchCriteria.isFilterByConfirmed();

		sb.append(" SELECT * FROM  ");
		sb.append("  ( select distinct FLIGHT_DATE, FLIGHT_TIME, APPLICABILITY, FLIGHT_NO, SEG_CODE, AIRPORT, STATUS, SALES_CHANNEL, AGENT_NAME, PNR, PNR_PAX_ID, PAX_NAME, TRANSFER_DATE, TRANSFER_TIME,CONTACT_NO, ADDRESS,FFP_NUMBER,BOOKING_DATE ");
		sb.append("  from( ");
		sb.append("   SELECT DECODE( SUBSTR(fs.segment_code, 1, 3), psat.airport_code, fs.est_time_departure_local, fs.est_time_arrival_local)                                  AS FLIGHT_DATE  , ");
		sb.append("   DECODE( SUBSTR(fs.segment_code, 1, 3), psat.airport_code, TO_CHAR(fs.est_time_departure_local, 'HH24:MI'), TO_CHAR(fs.est_time_arrival_local, 'HH24:MI')) AS FLIGHT_TIME  , ");
		sb.append("   DECODE( SUBSTR(fs.segment_code, 1, 3), psat.airport_code, 'D', 'A')                                                                                       AS APPLICABILITY, ");
		sb.append("   f.flight_number                                                                                                                                           AS FLIGHT_NO    , ");
		sb.append("   fs.segment_code                                                                                                                                           AS SEG_CODE     , ");
		sb.append("   psat.airport_code                                                                                                                                         AS AIRPORT      , ");
		sb.append("   psat.status                                                                                                                                               AS STATUS       , ");
		sb.append("   NVL(psat.BOOKING_TIMESTAMP,'')                                                                                                                            AS BOOKING_DATE , ");
		sb.append("   sales.description 																																		AS SALES_CHANNEL, ");
		sb.append("   NVL(agn.agent_name , DECODE(psat.SALES_CHANNEL_CODE, " + SalesChannelsUtil.SALES_CHANNEL_WEB
				+ " ,'WEB-USER'," + SalesChannelsUtil.SALES_CHANNEL_IOS + ",'IOS-USER',"
				+ SalesChannelsUtil.SALES_CHANNEL_ANDROID + ",'ANDROID-USER')) 																									AS AGENT_NAME,    ");
		sb.append("   pax.pnr                                                                                                                                                   AS PNR          , ");
		sb.append("   pax.pnr_pax_id 																																			AS PNR_PAX_ID   , ");
		sb.append("   INITCAP (pax.title  ||'.' ||pax.first_name  || ' ' || pax.last_name) AS PAX_NAME, ");
		sb.append("   psat.request_timestamp AS TRANSFER_DATE                    , ");
		sb.append("   TO_CHAR(psat.request_timestamp, 'HH24:MI') AS TRANSFER_TIME, ");
		sb.append("   psat.contact_no  AS CONTACT_NO                             , ");
		sb.append("   psat.address AS ADDRESS ,");
		sb.append("   (SELECT DECODE((si.ssr_code),'FQTV',ppss.ssr_text,'') ");
		sb.append("    FROM t_ssr_info si, t_pnr_pax_segment_ssr ppss ");
		sb.append("   WHERE ppfs.ppfs_id = ppss.ppfs_id and si.ssr_id = ppss.ssr_id and rownum <= 1 ) AS FFP_NUMBER ");
		sb.append("    FROM t_flight_segment fs       , ");
		sb.append("   t_flight f                      , ");
		sb.append("   t_pnr_segment ps                , ");
		sb.append("   t_pnr_pax_fare_segment ppfs     , ");
		sb.append("   t_pnr_pax_fare ppf              , ");
		sb.append("   t_pnr_passenger pax             , ");
		sb.append("   t_sales_channel sales           , ");
		sb.append("   t_pax_transaction txn           , ");
		sb.append("   t_agent agn                     , ");
		sb.append("   t_pnr_pax_seg_airport_transfer psat, ");
		sb.append("   t_user usr, ");
		sb.append("   t_reservation res ");
		sb.append("   WHERE pax.pnr_pax_id  = ppf.pnr_pax_id ");
		sb.append(" AND ppf.ppf_id          = ppfs.ppf_id ");
		sb.append(" AND ppfs.pnr_seg_id     = ps.pnr_seg_id ");
		sb.append(" AND ps.flt_seg_id       = fs.flt_seg_id ");
		sb.append(" AND fs.flight_id        = f.flight_id ");
		sb.append(" AND txn.pnr_pax_id      = pax.pnr_pax_id ");
		sb.append(" AND txn.dr_cr           = 'CR' ");
		sb.append(" AND txn.agent_code      = agn.agent_code(+) ");
		sb.append(" AND txn.sales_channel_code = sales.sales_channel_code ");
		sb.append(" AND txn.nominal_code in ( ");
		sb.append(Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes()));
		sb.append(" ) ");
		sb.append(" AND psat.ppf_id        = ppf.ppf_id ");
		sb.append(" AND psat.pnr_pax_id     = ppf.pnr_pax_id ");
		sb.append(" AND ps.sub_status      IS NULL ");

		if (includeOnlyConfirmed) {
			sb.append("  AND ps.status          <> 'CNX' ");
			sb.append("  AND pax.status         <>'CNX' ");
			sb.append("  AND psat.status        <> 'CNX' ");
		}

		sb.append(" AND res.pnr = pax.pnr ");

		if (isNotEmptyOrNull(strDepFromdate) && isNotEmptyOrNull(strDepToDate)) {
			sb.append(" AND fs.est_time_departure_local BETWEEN TO_DATE('" + strDepFromdate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND TO_DATE('" + strDepToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(strTransferFromDate) && isNotEmptyOrNull(strTransferToDate)) {
			sb.append(" AND psat.request_timestamp BETWEEN TO_DATE('" + strTransferFromDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
			sb.append(" AND TO_DATE('" + strTransferToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" fs.segment_code ", segmentCodes, false) + " ");
		}

		if (isNotEmptyOrNull(strFlightNo)) {
			sb.append(" AND f.flight_number='" + strFlightNo + "' ");
		}
		sb.append(" AND (res.origin_user_id = usr.user_id OR res.origin_user_id is NULL)");

		sb.append(" ) ) ");
		sb.append("  ORDER BY FLIGHT_DATE, ");
		sb.append("  FLIGHT_TIME         , ");
		sb.append("  AIRPORT             , ");
		sb.append("  PNR                   ");

		return sb.toString();
	}

	public String getAgentSalesModifyRefundProcQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String sql = "PK_SALES_MODIFY_REFUND_REPORT.P_SALES_MODIFY_REFUND_REPORT";

		/*
		 * Zest requsted this report in a specific format to them. For any other airline that format will not be
		 * usefull. Changed the proc only for Zest.
		 */
		if (AppSysParamsUtil.getDefaultCarrierCode().equalsIgnoreCase("Z2")) {
			sql = "PK_SALES_MODIFY_REFUND_REPORT.P_SALES_MODIFY_REFUND_REPORT";
		}
		return sql;
	}

	/**
	 * Creates the SQL query for User Incentive Details Report
	 * 
	 * @param reportsSearchCriteria
	 *            : The reports search criteria.
	 * @return : The sql query string.
	 */
	public String getUserIncentiveDetailsReport(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();
		Collection<String> bookingClassCodes = reportsSearchCriteria.getBookingCodes();
		Collection<String> paxStatus = reportsSearchCriteria.getPaxStatus();

		sb.append("SELECT * ");
		sb.append("FROM ( ");
		sb.append(
				" SELECT ffp_number, user_id, display_name, booking_code, COUNT ('x') COUNT, sum(total_fare) as AMOUNT FROM ( ");
		sb.append(" SELECT usr.ffp_number, ");
		sb.append("	 usr.user_id, ");
		sb.append("  usr.display_name, ");
		sb.append(
				"  (CASE WHEN ppfs.booking_code is null THEN f_get_infant_cabin_class_code (ppx.pnr_pax_id, ppfs.pnr_seg_id, 'BC' ) ELSE ppfs.booking_code END) AS booking_code, ");
		sb.append(" ppf.total_fare as total_fare ");
		sb.append("  FROM t_pnr_segment pnrseg, ");
		sb.append("       t_reservation res, ");
		sb.append("       t_user usr, ");
		sb.append("       t_flight_segment seg, ");
		sb.append("       t_pnr_pax_fare ppf, ");
		sb.append("       t_pnr_pax_fare_segment ppfs, ");
		sb.append("       t_pnr_passenger ppx, ");
		sb.append("       t_flight flt ");
		sb.append("  WHERE pnrseg.pnr = res.pnr ");
		sb.append("  AND res.pnr = ppx.pnr ");
		sb.append("  AND ppf.pnr_pax_id = ppx.pnr_pax_id ");
		sb.append("  AND ppf.ppf_id = ppfs.ppf_id ");
		sb.append("  AND pnrseg.flt_seg_id = seg.flt_seg_id ");
		sb.append("  AND seg.flight_id = flt.flight_id ");
		sb.append("  AND pnrseg.pnr_seg_id = ppfs.pnr_seg_id ");
		sb.append("  AND pnrseg.STATUS_MOD_USER_ID = usr.user_id ");
		sb.append("  AND pnrseg.status='CNF' ");
		sb.append(
				"  AND  DECODE(PPX.PAX_TYPE_CODE,'IN',PPX.ADULT_ID,PPX.PNR_PAX_ID) IN(SELECT ptxn.PNR_PAX_ID FROM t_pax_transaction ptxn WHERE ptxn.nominal_code IN("
						+ Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes()) + ")) ");

		if (reportsSearchCriteria.getDateRangeFrom() != null) {
			sb.append("	        AND res.booking_timestamp >= TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
					+ "','DD-MON-YYYY HH24:mi:ss')  ");
		}
		if (reportsSearchCriteria.getDateRangeTo() != null) {
			sb.append(" AND res.booking_timestamp <= TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
					+ "','DD-MON-YYYY HH24:mi:ss') ");
		}
		if (reportsSearchCriteria.getDepartureDateRangeFrom() != null) {
			sb.append(" and seg.est_time_departure_local >= TO_DATE ( '" + reportsSearchCriteria.getDepartureDateRangeFrom()
					+ "' , 'DD-MON-YYYY HH24:mi:ss' ) ");
		}

		if (reportsSearchCriteria.getDepartureDateRangeTo() != null) {
			sb.append(" and seg.est_time_departure_local <= TO_DATE ( '" + reportsSearchCriteria.getDepartureDateRangeTo()
					+ "' , 'DD-MON-YYYY HH24:mi:ss' ) ");
		}

		if (reportsSearchCriteria.getUserId() != null) {
			sb.append(" AND usr.user_id ='" + reportsSearchCriteria.getUserId() + "' ");
		}

		if (reportsSearchCriteria.getFlightType() != null) {
			if ("ALL".equals(reportsSearchCriteria.getFlightType())) {
				sb.append("");
			} else {
				sb.append(" AND ");
				sb.append(" flt.flight_type    = '" + reportsSearchCriteria.getFlightType() + "' ");
			}
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" seg.segment_code ", segmentCodes, false) + " ");
		}

		if (reportsSearchCriteria.getFfpNumber() != null) {
			sb.append(" AND usr.ffp_number ='");
			sb.append(reportsSearchCriteria.getFfpNumber() + "' ");
		} else {
			sb.append(" AND usr.ffp_number is Not Null ");
		}

		if (paxStatus != null && !paxStatus.isEmpty()) {
			sb.append(" AND  ");
			sb.append(ReportUtils.getReplaceStringForIN(" ppfs.pax_status ", paxStatus, false) + " ");
		}

		if (paxStatus != null && !paxStatus.isEmpty()) {
			sb.append(" AND  ");
			sb.append(ReportUtils.getReplaceStringForIN(" ppfs.pax_status ", paxStatus, false) + " ");
		}

		sb.append(") ");

		if (bookingClassCodes != null) {
			sb.append(" WHERE ");
			sb.append(ReportUtils.getReplaceStringForIN(" booking_code ", bookingClassCodes, false) + " ");
		}

		sb.append(" GROUP BY ");
		sb.append(" ffp_number, ");
		sb.append(" user_id, ");
		sb.append(" display_name, ");
		sb.append(" booking_code ");
		if ("LT".equals(reportsSearchCriteria.getQuantifier())) {
			sb.append(") where count < " + reportsSearchCriteria.getCount() + " order by ffp_number,user_id");
		} else if ("GT".equals(reportsSearchCriteria.getQuantifier())) {
			sb.append(") where count > " + reportsSearchCriteria.getCount() + " order by ffp_number,user_id");
		} else if ("EQ".equals(reportsSearchCriteria.getQuantifier())) {
			sb.append(") where count = " + reportsSearchCriteria.getCount() + " order by ffp_number,user_id");
		} else {
			sb.append(") order by ffp_number,user_id");
		}

		return sb.toString();
	}

	public String getFlightHistoryDetailsReport(String flightId, String flightNo, String flightDepartureDate, String reverseDate,
			Integer scheduleId, String fromDate, String toDate, boolean showResHistory) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		sb.append(
				" SELECT b.description,TO_CHAR(a.timestamp,'dd/mm/yyyy HH24:mi:ss') AS TIMESTAMP,a.user_id,a.content,a.timestamp as times ");
		sb.append(" FROM t_audit a,t_task b,t_agent c,t_user d,t_task_group e ");
		sb.append(" WHERE a.task_code     = b.task_code ");
		sb.append(" AND c.agent_code      = d.agent_code ");
//		sb.append(" AND a.user_id         = d.user_id ");
		sb.append(" AND (a.user_id = d.user_id OR substr(a.user_id,1,instr(a.user_id,'$',1)-1) = d.user_id) ");
		sb.append(" AND b.task_group_code = e.task_group_code ");
		sb.append(" AND b.task_group_code IN ('admin.plan.flight','admin.plan.invn','admin.sita','xbe.reservation') ");
		sb.append(" and (lower(a.content) like '%flightid%:=" + flightId.trim() + "%' ");
		sb.append(" or lower(a.content) like '%flight id%:=%" + flightId.trim() + "%' ");

		if (scheduleId != null) {
			sb.append(" or (a.content like '%flight%:%" + flightNo.trim()
					+ "%' and a.task_code=1065) and a.content like '%schedule%id%:%" + scheduleId + "%' ");
			sb.append(" or (a.content like '%schedule%id%:%" + scheduleId + "%' and a.task_code in (1062,2055,2056)) ");
			sb.append(" or (a.content like '%new%schedule%ids%from%split%:%" + scheduleId + "%' and a.task_code = 1064) ");
		}
		sb.append(" or (a.content like '%flight%id%:%" + flightId + "%' and a.task_code in (2057,2058)) ");
		sb.append(" or (a.content like '%flight%No%:%" + flightNo.trim() + "%' and a.content like '%dep%date%:%"
				+ flightDepartureDate.substring(0, 10) + "%')");
		sb.append(" or (a.content like '%Flight%Number%" + flightNo.trim() + "%' and a.task_code=2047)");
		sb.append(" )");
		sb.append(" AND a.timestamp BETWEEN TO_DATE('" + fromDate + "','dd/mm/yyyy HH24:mi:ss') AND TO_DATE('" + toDate
				+ "', 'dd/mm/yyyy HH24:mi:ss') ");

		if (showResHistory) {
			sb.append(" union ");
			sb.append(" (SELECT temp.template_desc, TO_CHAR(ra.timestamp,'dd/mm/yyyy HH24:mi:ss') AS TIMESTAMP,");
			sb.append(" ra.user_id,ra.content,ra.timestamp as times ");
			sb.append(" FROM t_reservation_audit ra,t_audit_template temp ");
			sb.append(" WHERE ");
			sb.append(" ra.timestamp BETWEEN TO_DATE('" + fromDate + "','dd/mm/yyyy HH24:mi:ss') AND TO_DATE('" + toDate
					+ "', 'dd/mm/yyyy HH24:mi:ss') ");
			sb.append(" AND ((ra.template_code='NEWCNF' ");
			sb.append(" AND ra.content LIKE '%flight segment(s): %" + flightNo + "-%-" + reverseDate + "%'");
			sb.append(" AND ra.template_code=temp.template_code) ");
			sb.append(" OR (ra.template_code='NEWONH' ");
			sb.append(" AND ra.content LIKE '%flight segment(s): %" + flightNo + "-%-" + reverseDate + "%'");
			sb.append(" AND ra.template_code=temp.template_code))) ");

		}

		sb.append(" ORDER BY times ");

		return sb.toString();

	}

	public String getTaxHistoryDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();

		String chargeCode = reportsSearchCriteria.getChargeCode();
		String chargeGroup = reportsSearchCriteria.getChargeGrooup();
		String category = reportsSearchCriteria.getCategory();
		String selStatus = reportsSearchCriteria.getStatus();
		String dateRangeFrm = reportsSearchCriteria.getDateRangeFrom();
		String dateRangeTo = reportsSearchCriteria.getDateRangeTo();
		String sortByOrder = reportsSearchCriteria.getSortByOrder();

		sb.append("SELECT chr.charge_code,chr.status AS charge_code_status,chrrt.charge_effective_from_date,chrrt.charge_effective_to_date,chrrt.sales_valid_from,chrrt.sales_valid_to,chrrt.cabin_class_code AS cb_class,chrrt.status AS charge_rate_status,chrrt.charge_value_percentage,");

		sb.append("CASE WHEN chr.applies_to = 0 THEN 'All' WHEN chr.applies_to = 1 THEN 'Adult Only' WHEN chr.applies_to = 2 THEN 'Infant Only' WHEN chr.applies_to = 3 THEN 'Child Only' WHEN chr.applies_to = 4 THEN 'Adult,Infant' WHEN chr.applies_to = 5 THEN 'Adult,Child' WHEN chr.applies_to = 6 THEN 'Reservation' WHEN chr.applies_to = 7 THEN 'OND' END AS applies_to,");

		sb.append("CASE chrrt.charge_basis WHEN  'V' THEN 'V' WHEN  'PF' THEN 'PF' WHEN  'PTF' THEN 'PTF' WHEN  'PS' THEN 'PS' WHEN  'PFS' THEN 'PFS' WHEN  'PTFS' THEN 'PTFS' END AS flag ");

		sb.append("FROM t_charge chr LEFT JOIN t_charge_rate chrrt ON CHR.charge_code = chrrt.charge_code WHERE 1 = 1 ");

		if (isNotEmptyOrNull(selStatus) && (!selStatus.equalsIgnoreCase("ALL"))) {
			sb.append("AND chrrt.status='" + selStatus + "' ");
		}
		if (isNotEmptyOrNull(chargeCode) && (!chargeCode.equalsIgnoreCase("ALL"))) {
			sb.append("AND chr.charge_code='" + chargeCode + "' ");
		}
		if (isNotEmptyOrNull(chargeGroup) && (!chargeGroup.equalsIgnoreCase("ALL"))) {
			sb.append("AND chr.charge_group_code='" + chargeGroup + "' ");
		}
		if (isNotEmptyOrNull(category) && (!category.equalsIgnoreCase("ALL"))) {
			sb.append("AND chr.charge_category_code='" + category + "' ");
		}
		if (isNotEmptyOrNull(dateRangeFrm) && isNotEmptyOrNull(dateRangeTo)) {
			sb.append("AND ((chrrt.charge_effective_from_date >='" + dateRangeFrm + "' ");
			sb.append("AND chrrt.charge_effective_to_date >='" + dateRangeTo + "' ");
			sb.append("AND chrrt.charge_effective_from_date <='" + dateRangeTo + "') ");

			sb.append("OR (chrrt.charge_effective_from_date <='" + dateRangeFrm + "' ");

			sb.append("AND chrrt.charge_effective_to_date <='" + dateRangeTo + "' ");

			sb.append("AND chrrt.charge_effective_from_date >='" + dateRangeFrm + "') ");

			sb.append("OR (chrrt.charge_effective_from_date <='" + dateRangeFrm + "' ");
			sb.append("AND chrrt.charge_effective_to_date >='" + dateRangeTo + "') ");
			sb.append("OR (chrrt.charge_effective_from_date >='" + dateRangeFrm + "' ");
			sb.append("AND chrrt.charge_effective_to_date <='" + dateRangeFrm + "')) ");

		}
		if ((dateRangeFrm == null) && (dateRangeTo == null)) {
			if (reportsSearchCriteria.isOnlyChargesWithoutRate()) {
				sb.append("AND chrrt.charge_code is null ");
			}
		}
		if (sortByOrder.equals("ASC")) {
			sb.append("ORDER BY chr.charge_code ASC");
		} else {
			sb.append("ORDER BY chr.charge_code DESC");
		}

		return sb.toString();
	}

	public String getFlightConnectivityDataDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuffer sb = new StringBuffer();
		String date = reportsSearchCriteria.getDate();
		String flightType = reportsSearchCriteria.getFlightType();
		String origin = reportsSearchCriteria.getOriginAirport();
		String destination = reportsSearchCriteria.getDestinationAirport();
		String originMinConnectTime = reportsSearchCriteria.getOriginMinConncetTime();
		String originMaxConnectTime = reportsSearchCriteria.getOriginMaxConncetTime();
		String destinationMinConnectTime = reportsSearchCriteria.getDestinationMinConncetTime();
		String destinationMaxConnectTime = reportsSearchCriteria.getDestinationMaxConncetTime();

		sb.append(" SELECT query2.flight_number AS FlightNo,query1.flight_number AS ConFlightNo,");
		sb.append(" query1.origin AS Origin,query1.destination AS Destination,");
		sb.append("	query1.est_time_departure AS DepartureTime,");
		sb.append(" query1.est_time_arrival AS ArrivalTime");
		if (flightType.equals("IN")) {
			sb.append(",TRUNC((query2.est_time_departure_local - query1.est_time_arrival_local)*24) || ':' || extract (minute from numtodsinterval((query2.est_time_departure_local - query1.est_time_arrival_local)*24, 'HOUR' )) AS InBoundConTime ");
			sb.append(", null AS OutBoundConTime ");
		} else if (flightType.equals("OUT")) {
			sb.append(", null AS InBoundConTime ");
			sb.append(",TRUNC((query1.est_time_departure_local - query2.est_time_arrival_local)*24) || ':' || extract (minute from numtodsinterval((query1.est_time_departure_local - query2.est_time_arrival_local)*24, 'HOUR' )) AS OutBoundConTime ");
		} else {
			sb.append(",TRUNC((query2.est_time_departure_local - query1.est_time_arrival_local)*24) || ':' || extract (minute from numtodsinterval((query2.est_time_departure_local - query1.est_time_arrival_local)*24, 'HOUR' )) AS InBoundConTime ");
			sb.append(",TRUNC((query1.est_time_departure_local - query1.est_time_arrival_local)*24) || ':' || extract (minute from numtodsinterval((query1.est_time_departure_local - query2.est_time_arrival_local)*24, 'HOUR' )) AS OutBoundConTime ");
		}
		sb.append(" FROM(SELECT f.flight_number,fs.segment_code, ");
		sb.append(" SUBSTR(fs.segment_code,1,3) AS origin,SUBSTR(fs.segment_code,-3,3) AS destination, ");
		sb.append(" fs.est_time_arrival_local,fs.est_time_departure_local, ");
		sb.append(" to_char(fs.est_time_arrival_local,'DD-MM-YY HH24:MI') as est_time_arrival, ");
		sb.append(" to_char(fs.est_time_departure_local,'DD-MM-YY HH24:MI') as est_time_departure ");
		sb.append(" FROM t_flight f,t_flight_segment fs ");
		sb.append(" WHERE fs.flight_id = f.flight_id ");
		sb.append(" AND fs.est_time_arrival_local BETWEEN to_timestamp('" + date + " 00:00:00','DD/MM/YYYY HH24:MI:SS') ");
		sb.append(" AND to_timestamp('" + date + " 23:59:59','DD/MM/YYYY HH24:MI:SS') ");

		if (flightType.equals("IN")) {
			sb.append(" AND SUBSTR(fs.segment_code,-3,3) = '" + origin + "') ");
		} else if (flightType.equals("OUT")) {
			sb.append(" AND SUBSTR(fs.segment_code, 1,3) = '" + destination + "') ");
		} else {
			sb.append(" AND (SUBSTR(fs.segment_code,-3,3) = '" + origin + "' ");
			sb.append(" OR SUBSTR(fs.segment_code, 1,3) = '" + destination + "')) ");
		}
		sb.append(" query1, ");

		sb.append(" (SELECT f.flight_number,fs.segment_code,SUBSTR(fs.segment_code,1,3),SUBSTR(fs.segment_code,-3,3) ,fs.est_time_arrival_local,fs.est_time_departure_local ");
		sb.append(" FROM t_flight f,t_flight_segment fs ");
		sb.append(" WHERE fs.flight_id = f.flight_id ");
		sb.append(" AND fs.est_time_departure_local BETWEEN to_timestamp('" + date + " 00:00:00','DD/MM/YYYY HH24:MI:SS') ");
		sb.append(" AND to_timestamp('" + date + " 23:59:59','DD/MM/YYYY HH24:MI:SS')");
		sb.append(" AND (SUBSTR(fs.segment_code,-3,3) = '" + destination + "' ");
		sb.append(" AND SUBSTR(fs.segment_code,1,3)  = '" + origin + "' )) query2");

		sb.append(" WHERE ");
		if (flightType.equals("IN")) {
			sb.append(" (query2.est_time_departure_local - query1.est_time_arrival_local)*24*60 ");
			sb.append(" BETWEEN (to_date('" + originMinConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60 ");
			sb.append(" AND (to_date('" + originMaxConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60 ");
		} else if (flightType.equals("OUT")) {
			sb.append(" (query1.est_time_departure_local - query2.est_time_arrival_local)*24*60 ");
			sb.append(" BETWEEN (to_date('" + destinationMinConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60 ");
			sb.append(" AND (to_date('" + destinationMaxConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60 ");
		} else {
			sb.append(" ((query2.est_time_departure_local - query1.est_time_arrival_local)*24*60 ");
			sb.append(" BETWEEN (to_date('" + originMinConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60 ");
			sb.append(" AND (to_date('" + originMaxConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60) ");
			sb.append(" AND ((query1.est_time_departure_local - query2.est_time_arrival_local)*24*60 ");
			sb.append(" BETWEEN (to_date('" + destinationMinConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60 ");
			sb.append(" AND (to_date('" + destinationMaxConnectTime + "','HH24:MI')-to_date('00:00','HH24:MI'))*24*60) ");
		}

		return sb.toString();
	}

	public String getPromotionRequestsSummary(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("	 SELECT f.flight_number flight_number , fs.segment_code segment_code , to_char(fs.est_time_departure_zulu,'DD-MM-YYYY HH24:MI') as departure_time ,	")
				.append("         pt.promotion_name promotion_name , COUNT ( 1 ) promotion_requests , SUM (DECODE (pr.status, 'APR', 1, 0)) apr_reqs ,	")
				.append("         SUM (DECODE (pr.status, 'REJ', 1, 0)) rej_reqs , SUM (DECODE (pr.status, 'ACT', 1, 0)) act_reqs ")
				.append("     FROM t_promotion_request pr , t_promotion p , t_promotion_type pt ,	")
				.append("         t_pnr_segment rs , t_flight_segment fs , t_flight f	")
				.append("     WHERE pr.promotion_id = p.promotion_id AND p.promotion_type_id = pt.promotion_type_id	")
				.append("         AND pr.pnr_seg_id = rs.pnr_seg_id AND rs.flt_seg_id = fs.flt_seg_id	")
				.append("         AND fs.flight_id = f.flight_id AND pr.status != 'INA'	")
				.append("         AND f.flight_number LIKE '%" + reportsSearchCriteria.getFlightNumber() + "%'")
				.append("		  AND fs.est_time_departure_zulu BETWEEN TO_DATE (" + reportsSearchCriteria.getDateRangeFrom()
						+ " ) 		AND TO_DATE ( " + reportsSearchCriteria.getDateRangeTo() + " )	")
				.append("     GROUP BY f.flight_number , fs.segment_code , fs.est_time_departure_zulu , pt.promotion_name	")
				.append("     ORDER BY fs.est_time_departure_zulu , f.flight_number	");

		return queryContent.toString();
	}

	public String getFlightOverbookSummary(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder queryContent = new StringBuilder();
		if (reportsSearchCriteria.isConsideringFixedSeats()) {
			queryContent.append(" ( ");
		}
		queryContent
				.append("     SELECT	distinct f.departure_date ,f.origin, f.destination,f.flight_number,	  fs.segment_code,fcca.flight_id, fcca.cabin_class_code,	")
				.append("        fcca.actual_capacity, fcca.infant_capacity,	fccsa.available_infant_seats,	fccsa.allocated_seats, fccsa.oversell_seats,	")
				.append("        fccsa.allocated_waitlist_seats, fccsa.waitlisted_seats,	fccsa.on_hold_seats, fccsa.sold_seats,fccsa.available_seats, ")
				.append("     fccsa.infant_allocation, fccsa.sold_infant_seats,	fccsa.onhold_infant_seats, fccsa.curtailed_seats,fccsa.logical_cabin_class_code as fccsa_logical_cc_code,	fccsa.fixed_seats, fccsa.sold_seats + fccsa.on_hold_seats -(fccsa.allocated_seats+fccsa.oversell_seats-fccsa.curtailed_seats) as overbook_seats			")
				.append("         FROM	t_fcc_alloc fcca,	t_fcc_seg_alloc fccsa,t_fcc_seg_bc_alloc fccsba,t_booking_class bc,	t_flight f,	t_flight_segment fs,t_logical_cabin_class lcc	")
				.append("    		WHERE	fcca.fcca_id = fccsa.fcca_id AND fccsa.fccsa_id = fccsba.fccsa_id (+)	AND fccsba.booking_code = bc.booking_code (+) AND fcca.flight_id = f.flight_id AND fccsa.flt_seg_id = fs.flt_seg_id     	")
				.append("         AND f.status = '" + FlightStatusEnum.ACTIVE + "'")
				.append("		  AND f.departure_date BETWEEN TO_DATE (" + reportsSearchCriteria.getDateRangeFrom()
						+ " ) 		AND TO_DATE ( " + reportsSearchCriteria.getDateRangeTo() + " )	")
				.append("     AND (fccsa.allocated_seats+fccsa.oversell_seats-fccsa.curtailed_seats)<fccsa.sold_seats + fccsa.on_hold_seats ");

		if (reportsSearchCriteria.isConsideringFixedSeats()) {
			queryContent
					.append(" UNION SELECT DISTINCT f.departure_date ,f.origin, f.destination, f.flight_number, fs.segment_code, fcca.flight_id, fcca.cabin_class_code, ")
					.append(" fcca.actual_capacity, fcca.infant_capacity, fccsa.available_infant_seats, fccsa.allocated_seats, fccsa.oversell_seats, fccsa.allocated_waitlist_seats, ")
					.append(" fccsa.waitlisted_seats, fccsa.on_hold_seats, fccsa.sold_seats, fccsa.available_seats, fccsa.infant_allocation, fccsa.sold_infant_seats, ")
					.append(" fccsa.onhold_infant_seats, fccsa.curtailed_seats, fccsa.logical_cabin_class_code AS fccsa_logical_cc_code, (SELECT SUM( fccsba.allocated_seats) -SUM( fccsba.sold_seats + fccsba.onhold_seats)  FROM t_booking_class bc  WHERE fccsa.fccsa_id   = fccsba.fccsa_id  AND fccsba.booking_code = bc.booking_code  AND fccsa.flight_id     = f.flight_id  AND fccsa.valid_flag    = 'Y'  AND bc.fixed_flag       ='Y') AS fixed_seats, ")
					.append(" ((fccsa.sold_seats             + fccsa.on_hold_seats) +  ")
					.append(" (SELECT SUM(fccsba.allocated_seats) - SUM( fccsba.sold_seats + fccsba.onhold_seats) FROM t_booking_class bc ")
					.append(" WHERE fccsa.fccsa_id    = fccsba.fccsa_id AND fccsba.booking_code = bc.booking_code ")
					.append(" AND fccsa.flight_id     = f.flight_id  AND fccsa.valid_flag    = 'Y' AND bc.fixed_flag       ='Y' ")
					.append(" ) - (fccsa.allocated_seats + fccsa.oversell_seats - fccsa.curtailed_seats))   AS overbook_seats ")
					.append(" FROM t_fcc_alloc fcca, t_fcc_seg_alloc fccsa, t_fcc_seg_bc_alloc fccsba, t_booking_class bc, t_flight f, t_flight_segment fs, t_logical_cabin_class lcc ")
					.append(" WHERE fcca.fcca_id      = fccsa.fcca_id AND fccsa.fccsa_id      = fccsba.fccsa_id (+) AND fccsba.booking_code = bc.booking_code (+) AND fcca.flight_id      = f.flight_id ")
					.append(" AND fccsa.flt_seg_id    = fs.flt_seg_id AND f.status            = '" + FlightStatusEnum.ACTIVE
							+ "' AND f.departure_date BETWEEN TO_DATE (" + reportsSearchCriteria.getDateRangeFrom()
							+ " ) AND TO_DATE (" + reportsSearchCriteria.getDateRangeTo() + " ) ")
					.append(" AND (fccsa.allocated_seats + fccsa.oversell_seats - fccsa.curtailed_seats ) < (fccsa.sold_seats + fccsa.on_hold_seats + ")
					.append(" (SELECT SUM(fccsba.allocated_seats) - SUM( fccsba.sold_seats + fccsba.onhold_seats) ")
					.append(" FROM t_booking_class bc WHERE fccsa.fccsa_id    = fccsba.fccsa_id AND fccsba.booking_code = bc.booking_code ")
					.append(" AND fccsa.flight_id     = f.flight_id AND fccsa.valid_flag    = 'Y' AND bc.fixed_flag       ='Y')) ")
					.append(" ) ");
		}
		queryContent.append("     ORDER BY departure_date, flight_number	");
		return queryContent.toString();
	}

	public String getPromotionNextSeatFreeSummary(ReportsSearchCriteria reportSearchCriteria) {
		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("	 SELECT promo_req.flight_number flight_number, promo_req.segment_code segment_code , TO_CHAR ( promo_req.departure_time , 'dd-mm-yyyy hh24:mi' ) departure_time , ")
				.append("       SUM ( promo_req.promo_reqs ) total_promo_reqs , SUM ( promo_req.total_charges ) total_charges , SUM ( promo_req.total_seats ) total_seats ,	")
				.append("       SUM ( DECODE ( promo_req.status , 'APR' , promo_req.promo_reqs , 0 ) ) approved_promo_reqs ,	")
				.append("       SUM ( DECODE ( promo_req.status , 'APR' , promo_req.total_charges , 0 ) ) approved_promo_charges ,	")
				.append("       SUM ( DECODE ( promo_req.status , 'APR' , promo_req.total_seats , 0 ) ) approved_promo_seats ,	")
				.append("       SUM ( DECODE ( promo_req.status , 'REJ' , promo_req.refund_charge, 0 ) ) refund_charge   ")
				.append("    FROM (  SELECT  promo_req.flight_number flight_number , promo_req.segment_code segment_code , promo_req.departure_time departure_time ,	")
				.append("               promo_req.promotion_id promotion_id , promo_req.status status , COUNT ( promo_req.promotion_request_id ) promo_reqs ,	")
				.append("               SUM ( promo_req.seats ) total_seats , SUM ( promo_req.total_charges ) total_charges ,	")
				.append("               SUM ( promo_req.nonrefund_charge ) nonrefund_charge , SUM( promo_req.refund_charge ) refund_charge ")
				.append("           FROM (  SELECT promo_req.flight_number flight_number , promo_req.segment_code segment_code ,	")
				.append("                       promo_req.departure_time departure_time , promo_req.promotion_id promotion_id ,	")
				.append("                       promo_req.status status , MAX ( promo_req.seats ) seats ,	")
				.append("                       promo_req.promotion_request_id promotion_request_id , SUM ( opc.amount ) total_charges ,	")
				.append("                       SUM ( DECODE ( opc.promotion_charge_code , CONCAT ( 'NEXT_SEAT_FREE_REG_CHRG_' , promo_req.seats ) , opc.amount  ) ) nonrefund_charge , ")
				.append("                       SUM ( DECODE ( opc.promotion_charge_code , CONCAT ( 'NEXT_SEAT_FREE_CHRG_SEATS_' , promo_req.seats ) , opc.amount  ) ) refund_charge ")
				.append("                   FROM (  SELECT f.flight_number flight_number , fs.segment_code segment_code , fs.est_time_departure_zulu departure_time ,	")
				.append("                               p.promotion_id promotion_id , pr.promotion_request_id promotion_request_id ,	")
				.append("                               pr.status status , MAX ( DECODE ( prc.req_param , 'SEATS_TO_FREE' , prc.param_value ) ) seats	")
				.append("                           FROM t_promotion_request pr , t_promotion p , t_pnr_segment rs ,	")
				.append("                              t_flight_segment fs , t_flight f , t_promotion_request_config prc	")
				.append("                           WHERE pr.promotion_id = p.promotion_id AND pr.pnr_seg_id = rs.pnr_seg_id	")
				.append("                               AND rs.flt_seg_id = fs.flt_seg_id AND fs.flight_id = f.flight_id	")
				.append("                               AND f.flight_number LIKE '%" + reportSearchCriteria.getFlightNumber()
						+ "%'")
				.append("                               AND pr.status != 'INA' AND p.promotion_type_id = 1 AND pr.promotion_request_id = prc.promotion_request_id	")
				.append("                           GROUP BY f.flight_number , fs.segment_code , fs.est_time_departure_zulu ,  p.promotion_id ,	")
				.append("                               pr.promotion_request_id , pr.status ) promo_req , t_ond_promotion op , t_ond_promotion_charge opc	")
				.append("                   WHERE op.ond_promotion_id IN (  ")
				.append("                       SELECT MIN ( op_1.ond_promotion_id )	")
				.append("                        FROM t_ond_promotion op_1	")
				.append("                           WHERE op_1.promotion_id = promo_req.promotion_id	")
				.append("                       ) AND promo_req.promotion_id = op.promotion_id AND op.ond_promotion_id = opc.ond_promotion_id	")
				.append("                       AND opc.promotion_charge_code IN ( concat ( 'NEXT_SEAT_FREE_REG_CHRG_' , promo_req.seats ) , concat ( 'NEXT_SEAT_FREE_CHRG_SEATS_' , promo_req.seats ) )	")
				.append("                    GROUP BY promo_req.flight_number , promo_req.segment_code , promo_req.departure_time , promo_req.promotion_id , 	")
				.append("                            promo_req.promotion_request_id , promo_req.status ) promo_req	")
				.append("            GROUP BY promo_req.flight_number , promo_req.segment_code , promo_req.departure_time , promo_req.promotion_id ,	")
				.append("                   promo_req.status ) promo_req	")
				.append("	WHERE promo_req.departure_time BETWEEN TO_DATE ( ")
				.append(reportSearchCriteria.getDateRangeFrom())
				.append(" ) AND TO_DATE ( ")
				.append(reportSearchCriteria.getDateRangeTo())
				.append(" ) ")
				.append("   GROUP BY promo_req.flight_number , promo_req.segment_code , promo_req.departure_time , promo_req.promotion_id	")
				.append("	ORDER BY promo_req.departure_time, promo_req.flight_number ");

		return queryContent.toString();
	}

	public String getPromotionNextSeatFreeSummaryForCrew(ReportsSearchCriteria reportSearchCriteria) {

		StringBuilder queryContent = new StringBuilder();

		queryContent
				.append("SELECT flight_number,segment_code,departure_time, pnr, pnr_pax_id,NAME,")
				.append("	LTRIM (MAX (SYS_CONNECT_BY_PATH (seat_code, ','))KEEP (DENSE_RANK LAST ORDER BY curr), ',' ) AS seats,cabin_class_code ")
				.append("FROM(SELECT f.flight_number,fs.segment_code,fs.est_time_departure_zulu AS departure_time, pr.pnr, pp.pnr_pax_id,")
				.append("		pp.first_name || ' ' || pp.last_name AS NAME,ams.seat_code AS seat_code,")
				.append("		ROW_NUMBER () OVER (PARTITION BY pp.pnr_pax_id ORDER BY ams.seat_code)     AS curr,")
				.append("		ROW_NUMBER () OVER (PARTITION BY pp.pnr_pax_id ORDER BY ams.seat_code) - 1 AS prev,ams.cabin_class_code ")
				.append("	FROM sm_t_aircraft_model_seats ams,sm_t_flight_am_seat fas,sm_t_pnr_pax_seg_am_seat ppsas,")
				.append("  		t_promotion_request pr,t_promotion_type pt,t_promotion p,t_pnr_passenger pp,t_pnr_segment ps,t_flight_segment fs,t_flight f ")
				.append(" 		WHERE ams.am_seat_id = fas.am_seat_id AND fas.flight_am_seat_id = ppsas.flight_am_seat_id AND pr.pnr_pax_id = ppsas.pnr_pax_id")
				.append(" 		AND pr.pnr_seg_id = ppsas.pnr_seg_id  AND ps.pnr = pr.pnr AND pp.pnr = ps.pnr AND pp.pnr_pax_id = pr.pnr_pax_id AND ps.pnr_seg_id = pr.pnr_seg_id")
				.append(" 		AND ps.flt_seg_id = fs.flt_seg_id AND fs.flight_id = f.flight_id AND pr.promotion_id = p.promotion_id")
				.append(" 		AND p.promotion_type_id = pt.promotion_type_id AND ppsas.status like 'RES' AND pt.promotion_type_id = 1")
				.append("       AND f.flight_number LIKE '%" + reportSearchCriteria.getFlightNumber() + "%'")
				.append("		AND fs.est_time_departure_zulu BETWEEN TO_DATE ( ")
				.append(reportSearchCriteria.getDateRangeFrom())
				.append("       ) AND TO_DATE ( ")
				.append(reportSearchCriteria.getDateRangeTo())
				.append(" ) )")
				.append(" GROUP BY flight_number,segment_code,departure_time, pnr, pnr_pax_id,NAME,cabin_class_code")
				.append(" CONNECT BY prev = PRIOR curr AND pnr_pax_id = PRIOR pnr_pax_id START WITH curr = 1 ORDER BY departure_time ");
		return queryContent.toString();
	}

	public String getGDSReservationsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();

		String dateRangeFrom = reportsSearchCriteria.getDateRangeFrom();
		String dateRangeTo = reportsSearchCriteria.getDateRangeTo();
		String gdsID = reportsSearchCriteria.getGdsCode();

		sb.append("SELECT tr.pnr,");
		sb.append("tr.booking_timestamp,");
		sb.append("tr.status ");
		sb.append("FROM t_reservation tr,");
		sb.append("t_gds tg ");
		sb.append("WHERE tr.gds_id = tg.gds_id ");
		if (gdsID != null && !gdsID.equals("")) {
			sb.append("AND tg.gds_id = '" + gdsID + "' ");
		}
		if (dateRangeFrom != null && dateRangeTo != null) {
			sb.append("AND tr.booking_timestamp BETWEEN TO_DATE ('" + dateRangeFrom
					+ "', 'dd-mon-yyyy hh24:mi:ss') AND TO_DATE ('" + dateRangeTo + "', 'dd-mon-yyyy hh24:mi:ss') ");
			sb.append("order by tr.booking_timestamp ");
		}

		return sb.toString();
	}

	public String getPromoCodeDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		String departureDateFrom = reportsSearchCriteria.getDepartureDateRangeFrom();
		String departureDateTo = reportsSearchCriteria.getDepartureDateRangeTo();
		String bookedDateFrom = reportsSearchCriteria.getBookingDateFrom();
		String bookedDateTo = reportsSearchCriteria.getBookingDateTo();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT DISTINCT r.pnr, (-1 * r.total_discount) as total_discount, pc.promo_name, pc.promo_code, ");
		sb.append(" nvl(r.origin_agent_code,DECODE(r.ORIGIN_CHANNEL_CODE, 4, 'Web',20,'IOS',21,'ANDROID', r.origin_agent_code)) AS agent_code ");
		sb.append(", LOWER(DECODE(pc.discount_type, null, 'PERCENTAGE', pc.discount_type)) AS discount_type ");
		sb.append(", DECODE(pc.discount_value, null, 100, pc.discount_value) AS discount_value ");
		sb.append(", DECODE(pc.discriminator, '" + PromotionCriteriaTypes.BUYNGET + "', '"
				+ PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA + "'");
		sb.append(", '" + PromotionCriteriaTypes.DISCOUNT + "', '" + PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA + "'");
		sb.append(", '" + PromotionCriteriaTypes.FREESERVICE + "', '" + PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA + "'");
		sb.append(", '" + PromotionCriteriaTypes.SYSGEN + "', '" + PromotionCriteriaTypesDesc.SYS_GEN_PROMO);
		sb.append("') AS promo_type ");
		sb.append(", DECODE(pc.discount_apply_as, NULL, 'Money', pc.discount_apply_as) AS discount_as ");
		sb.append(", SUM(r.total_fare+r.total_surcharges+r.total_tax+r.total_pen+r.total_adj+r.total_cnx+r.total_mod) AS total_price ");
		sb.append("FROM t_reservation r, t_promotion_criteria pc ");
		sb.append("WHERE r.promo_criteria_id=pc.promo_criteria_id AND r.promo_criteria_id IS NOT NULL ");

		sb.append("AND r.pnr IN( SELECT ps.pnr FROM t_pnr_segment ps, t_flight_segment fs, t_flight f ");
		sb.append("WHERE ps.flt_seg_id=fs.flt_seg_id AND fs.flight_id=f.flight_id ");

		if (departureDateFrom != null && departureDateTo != null && !"".equals(departureDateFrom) && !"".equals(departureDateTo)) {
			sb.append("AND fs.est_time_departure_zulu BETWEEN TO_DATE ('" + departureDateFrom
					+ " 00:00:00', 'dd-mm-yyyy hh24:mi:ss') AND TO_DATE ('" + departureDateTo
					+ " 23:59:59', 'dd-mm-yyyy hh24:mi:ss') ");
		}

		if (reportsSearchCriteria.getSegmentCodes() != null && !reportsSearchCriteria.getSegmentCodes().isEmpty()) {
			sb.append("AND fs.segment_code IN(" + Util.buildStringInClauseContent(reportsSearchCriteria.getSegmentCodes()) + ") ");
		}

		if (reportsSearchCriteria.getFlightNoCollection() != null && !reportsSearchCriteria.getFlightNoCollection().isEmpty()) {
			sb.append("AND f.flight_number IN(" + Util.buildStringInClauseContent(reportsSearchCriteria.getFlightNoCollection())
					+ ") ");
		}
		sb.append(") ");

		if (bookedDateFrom != null && bookedDateTo != null && !"".equals(bookedDateFrom) && !"".equals(bookedDateTo)) {
			sb.append("AND r.booking_timestamp BETWEEN TO_DATE ('" + bookedDateFrom
					+ "', 'dd-mm-yyyy hh24:mi:ss') AND TO_DATE ('" + bookedDateTo + "', 'dd-mm-yyyy hh24:mi:ss') ");
		}

		if (reportsSearchCriteria.getPromotionTypes() != null && !reportsSearchCriteria.getPromotionTypes().isEmpty()) {
			sb.append("AND pc.discriminator IN(" + Util.buildStringInClauseContent(reportsSearchCriteria.getPromotionTypes())
					+ ") ");
		}

		if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append(" AND (" + composeInQuery("r.origin_agent_code", reportsSearchCriteria.getAgents()) + " ) ");
		}

		if (reportsSearchCriteria.getSalesChannels() != null && !reportsSearchCriteria.getSalesChannels().isEmpty()) {
			Collection<Integer> colSalesChannels = new ArrayList<Integer>();
			for (String strCh : reportsSearchCriteria.getSalesChannels()) {
				colSalesChannels.add(Integer.parseInt(strCh));
			}
			sb.append("AND r.origin_channel_code IN(" + Util.buildIntegerInClauseContent(colSalesChannels) + ") ");
		}
		// sb.append("AND r.total_discount <> 0 ");for future credit cases discount is not in the PNR

		sb.append("GROUP BY r.pnr, r.total_discount, pc.promo_name, pc.promo_code, r.origin_agent_code, pc.discount_type ");
		sb.append(", pc.discount_value, pc.discriminator, pc.discount_apply_as ,r.ORIGIN_CHANNEL_CODE ");

		return sb.toString();
	}

	public String getJNTaxReportQuery(ReportsSearchCriteria reportsSearchCriteria) {
		if (reportsSearchCriteria.getDateRangeFrom() == null || reportsSearchCriteria.getDateRangeTo() == null
				&& "".equals(reportsSearchCriteria.getDateRangeFrom()) || "".equals(reportsSearchCriteria.getDateRangeTo())) {
			return null;
		}

		String paymentDateFrom = reportsSearchCriteria.getDateRangeFrom();
		String paymentDateTo = reportsSearchCriteria.getDateRangeTo();
		paymentDateTo = paymentDateTo + " 23:59:59 ";

		String bookingDateFrom = reportsSearchCriteria.getDateRange2From();
		String bookingDateTo = reportsSearchCriteria.getDateRange2To();

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT i.pnr, i.no_of_pax, i.sector, i.fare, i.tax, i.surcharge,i.anci_total, i.other, ");
		sb.append("nvl(i.agent_code,DECODE(r.origin_channel_code, 4, 'web',20,'ios',21,'android', i.agent_code)) agent_code, ");
		sb.append("nvl(i.station,DECODE(r.origin_channel_code, 4, 'web',20,'ios',21,'android', i.station)) station, i.tnx_date, r.booking_timestamp booking_date, ");
		sb.append("i.jn_tax , i.jn_mod, i.jn_anci, i.jn_other, lower(i.mod_of_pay) mod_of_pay ");
		sb.append("FROM t_reservation r , ");
		sb.append("(SELECT /*+ no_index(ppop fk_pnr_pax_ond_payments_pft) */ p.pnr, COUNT(DISTINCT DECODE(p.PAX_TYPE_CODE,'AD',p.pnr_pax_id,'CH',p.pnr_pax_id,NULL))no_of_pax, ");
		sb.append("fs.segment_code sector, ");
		sb.append("SUM(CASE ");
		sb.append("WHEN pn.NOMINAL_CODE       IN(28,18,17,16,19,15,6,30,32,34,35,36,38) ");
		sb.append("AND ppop.CHARGE_GROUP_CODE IN('FAR','DIS') ");
		sb.append("AND (P.PAX_TYPE_CODE       IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) THEN PPSP.AMOUNT ELSE 0 END)fare, ");
		sb.append("SUM(CASE ");
		sb.append("WHEN ppop.CHARGE_GROUP_CODE='TAX' ");
		sb.append("AND (P.PAX_TYPE_CODE       IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) THEN PPSP.AMOUNT ELSE 0 END)tax, ");
		sb.append("SUM(CASE ");
		sb.append("WHEN ppop.CHARGE_GROUP_CODE ='SUR' AND (ppop.pft_id=pft.pft_id AND pft.charge_rate_id=cr.charge_rate_id ");
		sb.append("AND cr.charge_code NOT IN('ML', 'BG', 'SM', 'INS', 'HALA', 'AT', 'SSR', 'FL')) ");
		sb.append("AND (P.PAX_TYPE_CODE       IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) THEN PPSP.AMOUNT ELSE 0 END)surcharge, ");
		sb.append("SUM(CASE ");
		sb.append("WHEN ppop.CHARGE_GROUP_CODE ='SUR' AND (ppop.pft_id=pft.pft_id AND pft.charge_rate_id=cr.charge_rate_id ");
		sb.append("AND cr.charge_code IN('ML', 'BG', 'SM', 'INS', 'HALA', 'AT', 'SSR', 'FL')) ");
		sb.append("AND (P.PAX_TYPE_CODE       IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) THEN PPSP.AMOUNT ELSE 0 END)anci_total, ");
		sb.append("SUM(CASE ");
		sb.append("WHEN ppop.CHARGE_GROUP_CODE NOT IN('FAR','DIS','TAX','SUR') ");
		sb.append("AND (P.PAX_TYPE_CODE       IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) ");
		sb.append("THEN PPSP.AMOUNT ELSE 0 END)OTHER, ");
		sb.append("a.agent_code, a.station_code as station, ptxn.tnx_date tnx_date, ");
		sb.append("SUM(CASE WHEN (P.PAX_TYPE_CODE IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) AND CHARGE_CODE='JN' THEN PPSP.AMOUNT ELSE 0 END) JN_TAX, ");
		sb.append("SUM(CASE WHEN (P.PAX_TYPE_CODE IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) AND CHARGE_CODE='JNEXT' THEN PPSP.AMOUNT ELSE 0 END) JN_MOD, ");
		sb.append("SUM(CASE WHEN (P.PAX_TYPE_CODE IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) AND CHARGE_CODE='JNANCI' THEN PPSP.AMOUNT ELSE 0 END) JN_ANCI, ");
		sb.append("SUM(CASE WHEN (P.PAX_TYPE_CODE IN('AD','CH') OR (P.adult_id IS NULL AND P.PAX_TYPE_CODE ='IN')) AND CHARGE_CODE='JNOTHER' THEN PPSP.AMOUNT ELSE 0 END) JN_OTHER, ");
		sb.append("pn.description mod_of_pay ");
		sb.append("FROM T_PNR_PASSENGER P, t_pax_transaction ptxn, t_agent a, t_pax_trnx_nominal_code pn, ");
		sb.append("t_pnr_pax_ond_charges pft, t_pnr_pax_ond_payments ppop, t_pnr_pax_seg_payments ppsp, ");
		sb.append("t_pnr_pax_seg_charges pfst, t_charge_rate cr, t_pnr_pax_fare ppf, t_pnr_pax_fare_segment ppfs, ");
		sb.append("t_pnr_segment ps, t_flight_segment fs, t_flight f ");
		sb.append("WHERE P.pnr_pax_id IN (SELECT PPF1.PNR_PAX_ID FROM T_PNR_PAX_FARE PPF1 ");
		sb.append("WHERE PPF1.PPF_ID IN (SELECT PFT1.PPF_ID FROM T_PNR_PAX_OND_CHARGES PFT1 ");
		sb.append("WHERE AMOUNT <>0 AND PFT1.CHARGE_RATE_ID IN ");
		sb.append("(SELECT CR1.CHARGE_RATE_ID FROM T_CHARGE_RATE CR1 WHERE CHARGE_CODE IN('JN', 'JNEXT', 'JNANCI', 'JNOTHER')))) ");
		sb.append("AND DECODE(P.pax_type_code,'IN',DECODE(P.ADULT_ID,NULL,PPOP.PNR_PAX_ID,P.ADULT_ID),P.PNR_PAX_ID)=ptxn.pnr_pax_id ");
		sb.append("AND ptxn.agent_code                                   =a.agent_code(+) ");
		sb.append("AND ppop.nominal_code                                 =pn.nominal_code ");
		sb.append("AND PTXN.nominal_code                                 IN(28,18,17,16,19,15,6,30,32,34,35,36,38, 29,26,24,23,25,22,31,33,35,37,39,6,48) ");
		// sb.append("AND PTXN.nominal_code                                 =ppop.nominal_code  ");
		sb.append("AND ppop.pft_id                                       =pft.pft_id ");
		sb.append("AND ppop.ppop_id                                      =ppsp.ppop_id ");
		sb.append("AND pfst.pfst_id                                      =ppsp.pfst_id ");
		sb.append("AND pfst.pft_id                                       =pft.pft_id ");
		sb.append("AND ppfs.ppfs_id                                      =pfst.ppfs_id ");
		sb.append("AND ppfs.ppf_id                                       =pft.ppf_id ");
		sb.append("AND ps.pnr                                            =p.pnr ");
		sb.append("AND ppf.ppf_id                                        =pft.ppf_id ");
		sb.append("AND ppf.ppf_id                                        =ppfs.ppf_id ");
		sb.append("AND ppfs.pnr_seg_id                                   =ps.pnr_seg_id ");
		sb.append("AND ps.flt_seg_id                                     =fs.flt_seg_id ");
		sb.append("AND fs.flight_id                                      =f.flight_id ");
		sb.append("AND ((ppop.PAYMENT_TXN_ID =ptxn.txn_id  AND PTXN.NOMINAL_CODE IN(28,18,17,16,19,15,30,32,34,35,36,38,6,48) ");
		sb.append("AND PPOP.NOMINAL_CODE  IN(28,18,17,16,19,15,30,32,34,35,36,38,6,48))OR ");
		sb.append("(PTXN.NOMINAL_CODE  IN(29,26,24,23,25,22,31,33,35,37,39,6) ");
		sb.append("AND PPOP.NOMINAL_CODE IN(29,26,24,23,25,22,31,33,35,37,39,6)  AND ");
		sb.append("ptxn.txn_id  = ppop.refund_txn_id)) ");
		sb.append("AND cr.charge_rate_id(+) =pft.charge_rate_id ");
		sb.append("AND PTXN.tnx_date BETWEEN to_date('" + paymentDateFrom + "','dd-mm-yyyy hh24:mi:ss') AND to_date('"
				+ paymentDateTo + "','dd-mm-yyyy hh24:mi:ss') ");
		sb.append("GROUP BY P.PNR, FS.SEGMENT_CODE, a.agent_code, a.station_code, ptxn.tnx_date, pn.description ");
		sb.append("HAVING SUM(DECODE(CHARGE_CODE,'JN',PPSP.AMOUNT,0))<>0 ");
		sb.append("OR SUM(DECODE(CHARGE_CODE,'JNEXT',PPSP.AMOUNT,0))<>0 ");
		sb.append("OR SUM(DECODE(CHARGE_CODE,'JNANCI',PPSP.AMOUNT,0))<>0 ");
		sb.append("OR SUM(DECODE(CHARGE_CODE,'JNOTHER',PPSP.AMOUNT,0))<>0) i ");
		sb.append("WHERE r.pnr=i.pnr ");

		if (bookingDateFrom != null && bookingDateTo != null && !"".equals(bookingDateFrom) && !"".equals(bookingDateTo)) {
			bookingDateTo = bookingDateTo + " 23:59:59 ";
			sb.append(" AND r.booking_timestamp BETWEEN to_date('" + bookingDateFrom + "','dd-mm-yyyy hh24:mi:ss') AND to_date('"
					+ bookingDateTo + "','dd-mm-yyyy hh24:mi:ss') ");
		}

		if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
			sb.append(" AND (" + composeInQuery("r.origin_agent_code", reportsSearchCriteria.getAgents()) + " ) ");
		}

		if (reportsSearchCriteria.getSalesChannels() != null && !reportsSearchCriteria.getSalesChannels().isEmpty()) {
			Collection<Integer> colSalesChannels = new ArrayList<Integer>();
			for (String strCh : reportsSearchCriteria.getSalesChannels()) {
				colSalesChannels.add(Integer.parseInt(strCh));
			}
			sb.append(" AND r.origin_channel_code IN(" + Util.buildIntegerInClauseContent(colSalesChannels) + ") ");
		}

		return sb.toString();
	}

	public String getIbeExitDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuffer sb = new StringBuffer();
		sb.append("SELECT t.ibe_user_exit_details_id, t.pax_email, t.pax_flt_search_from, t.pax_flt_search_to, t.pax_flt_search_departure_date,");
		sb.append("t.pax_flight_search_arrival_date, t.pax_flight_search_no_of_adt, t.pax_flight_search_no_of_chd,");
		sb.append("t.pax_flight_search_no_of_inf, t.exit_step, t.search_time, t.search_flt_type, t.title, t.first_name,  ");
		sb.append("t.last_name, t.nationality, t.country, t.lang, t.mobile_number, t. travel_mobile_number, t.is_click_email_link ");
		sb.append("FROM T_IBE_USER_EXIT_DETAILS t ");
		sb.append("WHERE ");
		sb.append("t.pax_flt_search_departure_date ");
		sb.append("BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom() + " 00:00:00', ");
		sb.append("'dd-mm-yyyy HH24:mi:ss') ");
		sb.append("AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo() + " 23:59:59', ");
		sb.append("'dd-mm-yyyy HH24:mi:ss') ");
		return sb.toString();
	}

	public String getSeaTMapChangesSummary(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append("	select distinct pnr,f.flight_number,f.departure_date,tpp.first_name  ||' ' || tpp.last_name as name,f.model_number,(select  seatmodel.seat_code from	")
				.append("       sm_t_aircraft_model_seats seatmodel  where seatmodel.model_number=f.model_number  and seatmodel.seat_code=modseat.seat_code) 	")
				.append("       as seat_code,modseat.seat_code as old_seat_code,modseat.model_number as old_model from sm_t_pnr_pax_seg_am_seat pss inner join sm_t_flight_am_seat fas ")
				.append("     on fas.flight_am_seat_id=pss.flight_am_seat_id inner join sm_t_am_seat_charge sc on sc.am_seat_id=fas.am_seat_id inner join t_flight_segment tfs on tfs.flt_seg_id=fas.flt_seg_id			")
				.append("        inner join  sm_t_aircraft_model_seats modseat on modseat.am_seat_id=fas.am_seat_id	")
				.append("    		inner join t_flight f on f.flight_id=tfs.flight_id inner join t_pnr_segment tpseg on tpseg.pnr_seg_id =pss.pnr_seg_id      	")
				.append("       inner join t_pnr_passenger tpp on tpp.pnr=tpseg.pnr and tpp.pnr_pax_id=pss.pnr_pax_id where ( sc.amc_template_id!=f.amc_template_id or (f.amc_template_id is null and sc.amc_template_id>0) ) ")
				.append("		  AND f.departure_date BETWEEN TO_DATE (" + reportsSearchCriteria.getDateRangeFrom()
						+ " ) 		AND TO_DATE ( " + reportsSearchCriteria.getDateRangeTo() + " )	")
				.append("     ORDER BY f.departure_date, f.flight_number	");

		return queryContent.toString();
	}

	public String getIbeExitDetailsQueryById(String exitDetailsID) {
		StringBuffer sb = new StringBuffer();
		sb.append("select * from t_ibe_user_exit_details where ibe_user_exit_details_id=" + exitDetailsID);
		return sb.toString();

	}

	public String getBundledFareReportQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuffer sb = new StringBuffer();

		StringBuffer sbDynamicWhereCondition = new StringBuffer();

		String salesDateFrom = reportsSearchCriteria.getDateRangeFrom();
		String salesDateTo = reportsSearchCriteria.getDateRangeTo();

		String flightDateFrom = reportsSearchCriteria.getDateRange2From();
		String flightDateTo = reportsSearchCriteria.getDateRange2To();

		String salesDateWhereCondition = "";
		if (salesDateFrom != null && salesDateTo != null && !"".equals(salesDateFrom) && !"".equals(salesDateTo)) {
			salesDateTo = salesDateTo + " 23:59:59 ";
			salesDateWhereCondition = " AND ppoc.charge_date BETWEEN to_date('" + salesDateFrom
					+ "', 'dd/mm/yyyy') AND to_date('" + salesDateTo + "', 'dd/mm/yyyy HH24:MI:SS') ";
		}

		if (flightDateFrom != null && flightDateTo != null && !"".equals(flightDateFrom) && !"".equals(flightDateTo)) {
			flightDateTo = flightDateTo + " 23:59:59 ";
			sbDynamicWhereCondition.append(" AND fs.est_time_departure_zulu BETWEEN to_date('" + flightDateFrom
					+ "', 'dd/mm/yyyy') AND to_date('" + salesDateTo + "', 'dd/mm/yyyy HH24:MI:SS') ");
		}

		if (reportsSearchCriteria.getSegmentCodes() != null && !reportsSearchCriteria.getSegmentCodes().isEmpty()) {
			sbDynamicWhereCondition.append(" AND (" + composeInQuery("fs.segment_code", reportsSearchCriteria.getSegmentCodes())
					+ " ) ");
		}

		if (reportsSearchCriteria.getSalesChannels() != null && !reportsSearchCriteria.getSalesChannels().isEmpty()) {
			Collection<Integer> colSalesChannels = new ArrayList<Integer>();
			for (String strCh : reportsSearchCriteria.getSalesChannels()) {
				colSalesChannels.add(Integer.parseInt(strCh));
			}
			sbDynamicWhereCondition.append(" AND r.origin_channel_code IN(" + Util.buildIntegerInClauseContent(colSalesChannels)
					+ ") ");
		}

		if (reportsSearchCriteria.getBundledFarePeriodIds() != null && !reportsSearchCriteria.getBundledFarePeriodIds().isEmpty()) {
			sbDynamicWhereCondition.append(" AND bfp.bundled_fare_period_id IN("
					+ Util.buildIntegerInClauseContent(reportsSearchCriteria.getBundledFarePeriodIds()) + ") ");
		}

		if (reportsSearchCriteria.isDetailReport()) {
			sb.append("SELECT r.pnr, ps.status_mod_date AS sales_date, fs.segment_code AS sector,");
			sb.append(" bf.bundled_fare_name,sc.description AS sales_channel,");
			sb.append(" fs.est_time_departure_zulu AS flight_date, round (psc.amount,2) as bundled_fee");
			sb.append(" FROM t_reservation r");
			sb.append(" INNER JOIN t_pnr_passenger pp on ( r.pnr = pp.pnr)");
			sb.append(" INNER JOIN t_pnr_segment ps ON ps.pnr = pp.pnr");
			sb.append(" INNER JOIN t_pnr_pax_fare_segment ppfs ON ppfs.pnr_seg_id = ps.pnr_seg_id");
			sb.append(" INNER JOIN t_pnr_pax_seg_charges psc ON psc.PPFS_ID = ppfs.PPFS_ID");
			sb.append(" INNER JOIN t_pnr_pax_ond_charges ppoc ON ppoc.pft_id = psc.PFT_ID");
			sb.append(" INNER JOIN t_charge_rate cr ON cr.charge_rate_id = ppoc.charge_rate_id");
			sb.append(" INNER JOIN t_charge c ON c.charge_code = cr.charge_code ");
			sb.append(" INNER JOIN t_bundled_fare_period bfp on  (ps.bundled_fare_period_id = bfp.bundled_fare_period_id)");
			sb.append(" INNER JOIN t_bundled_fare bf on  (c.CHARGE_CODE = bf.CHARGE_CODE and bf.bundled_fare_id = bfp.bundled_fare_id)");
			sb.append(" INNER JOIN t_pnr_pax_fare paxfare on (ppfs.ppf_id = paxfare.ppf_id and paxfare.pnr_pax_id = pp.pnr_pax_id)");
			sb.append(" INNER JOIN t_flight_segment fs on fs.flt_seg_id = ps.flt_seg_id");
			sb.append(" INNER JOIN t_sales_channel sc on (r.origin_channel_code = sc.sales_channel_code)");
			sb.append(" WHERE c.bundled_fare_charge = 'Y'");
			sb.append(salesDateWhereCondition);
			sb.append(sbDynamicWhereCondition);
		} else {
			sb.append("SELECT to_char(ps.status_mod_date, 'dd/mm/yyyy') as sales_date,");
			sb.append(" fs.segment_code AS SECTOR,");
			sb.append(" sc.description AS SALES_CHANNEL,");
			sb.append(" count(pp.pnr_pax_id) as PNR_COUNT,");
			sb.append(" count(r.pnr) as NO_OF_PAX,");
			sb.append(" round(sum(psc.amount),2) AS TOTAL_AMOUNT");
			sb.append(" FROM t_reservation r");
			sb.append(" INNER JOIN t_pnr_passenger pp on ( r.pnr = pp.pnr)");
			sb.append(" INNER JOIN t_pnr_segment ps ON ps.pnr = pp.pnr");
			sb.append(" INNER JOIN t_pnr_pax_fare_segment ppfs ON ppfs.pnr_seg_id = ps.pnr_seg_id");
			sb.append(" INNER JOIN t_pnr_pax_seg_charges psc ON psc.PPFS_ID = ppfs.PPFS_ID");
			sb.append(" INNER JOIN t_pnr_pax_ond_charges ppoc ON ppoc.pft_id = psc.PFT_ID");
			sb.append(" INNER JOIN t_charge_rate cr ON cr.charge_rate_id = ppoc.charge_rate_id");
			sb.append(" INNER JOIN t_charge c ON c.charge_code = cr.charge_code ");
			sb.append(" INNER JOIN t_bundled_fare_period bfp on  (ps.bundled_fare_period_id = bfp.bundled_fare_period_id)");
			sb.append(" INNER JOIN t_bundled_fare bf on (c.CHARGE_CODE = bf.CHARGE_CODE and ps.bundled_fare_id = bf.bundled_fare_id)");
			sb.append(" INNER JOIN t_pnr_pax_fare paxfare on (ppfs.ppf_id = paxfare.ppf_id and paxfare.pnr_pax_id = pp.pnr_pax_id)");
			sb.append(" INNER JOIN t_flight_segment fs on fs.flt_seg_id = ps.flt_seg_id");
			sb.append(" INNER JOIN t_sales_channel sc on (r.origin_channel_code = sc.sales_channel_code)");
			sb.append(" WHERE c.bundled_fare_charge = 'Y'");
			sb.append(salesDateWhereCondition);
			sb.append(sbDynamicWhereCondition);
			sb.append(" group by fs.segment_code,sc.description,to_char(ps.status_mod_date, 'dd/mm/yyyy')");
		}
		return sb.toString();
	}

	public String getInternationalFlightDepartureArrivalQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();
		boolean isBothDatesSelected = false;
		String reportType = reportsSearchCriteria.getReportType();
		String departFromDate = reportsSearchCriteria.getDepartureDateRangeFrom();
		String departToDate = reportsSearchCriteria.getDepartureDateRangeTo();
		String arrFromDate = reportsSearchCriteria.getArrivalDateRangeFrom();
		String arrToDate = reportsSearchCriteria.getArrivalDateRangeTo();
		String flightNo = reportsSearchCriteria.getFlightNumber();

		if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate) && isNotEmptyOrNull(arrFromDate)
				&& isNotEmptyOrNull(arrToDate)) {
			isBothDatesSelected = true;
		}

		if (reportType.equals("SUMMARY")) {
			sb.append(" SELECT * FROM (");
			sb.append("SELECT ");
			if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate)) {
				sb.append(" ppai.arrival_intl_flightno           AS FLIGHT_NO, ");
				sb.append(" TO_CHAR(ppai.arrival_date, 'DD-MON-YYYY') AS ARRIVAL_DATE, ");
				sb.append(" TO_CHAR(ppai.arrival_date, 'HH24:mi')     AS ARRIVAL_TIME, ");
				sb.append(" NULL                                      AS DEPARTURE_DATE, ");
				sb.append(" NULL                                      AS DEPARTURE_TIME, ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" ppai.departure_intl_flightno           AS FLIGHT_NO, ");
				sb.append(" NULL                                        AS ARRIVAL_DATE, ");
				sb.append(" NULL                                        AS ARRIVAL_TIME, ");
				sb.append(" TO_CHAR(ppai.departure_date, 'DD-MON-YYYY') AS DEPARTURE_DATE, ");
				sb.append(" TO_CHAR(ppai.departure_date, 'HH24:mi')     AS DEPARTURE_TIME, ");
			}
			sb.append(" SUM (DECODE (pp.pax_type_code, 'AD', 1, 'CH', 1, 0 ) ) AS ADT_PAX_COUNT, ");
			sb.append(" SUM (DECODE (pp.pax_type_code, 'IN', 1, 0 ) )          AS INF_PAX_COUNT ");
			sb.append(" FROM t_reservation r, ");
			sb.append(" t_pnr_passenger pp, ");
			sb.append(" t_pnr_pax_additional_info ppai ");
			sb.append(" WHERE r.pnr       = pp.pnr ");
			sb.append(" AND pp.status     ='CNF' ");
			sb.append(" AND pp.pnr_pax_id = ppai.pnr_pax_id ");
			if (isBothDatesSelected && !isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			} else if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate)) {
				sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			}

			if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
				sb.append(" AND  ("
						+ ReportUtils.getReplaceStringForIN("r.owner_agent_code", reportsSearchCriteria.getAgents(), false)
						+ ") ");
			}
			if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.arrival_intl_flightno = '" + flightNo.trim() + "' ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.departure_intl_flightno = '" + flightNo.trim() + "' ");
			}
			sb.append(" GROUP BY ");
			if (isNotEmptyOrNull(arrFromDate)) {
				sb.append(" ppai.arrival_intl_flightno, ");
				sb.append(" ppai.arrival_date ");
			} else if (isNotEmptyOrNull(departFromDate)) {
				sb.append(" ppai.departure_intl_flightno, ");
				sb.append(" ppai.departure_date ");
			}

			if (isBothDatesSelected) {
				sb.append(" UNION ALL ");
				sb.append(" SELECT ");
				if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
					sb.append(" ppai.departure_intl_flightno           AS FLIGHT_NO, ");
					sb.append(" NULL                                        AS ARRIVAL_DATE, ");
					sb.append(" NULL                                        AS ARRIVAL_TIME, ");
					sb.append(" TO_CHAR(ppai.departure_date, 'DD-MON-YYYY') AS DEPARTURE_DATE, ");
					sb.append(" TO_CHAR(ppai.departure_date, 'HH24:mi')     AS DEPARTURE_TIME, ");
				}
				sb.append(" SUM (DECODE (pp.pax_type_code, 'AD', 1, 'CH', 1, 0 ) ) AS ADT_PAX_COUNT, ");
				sb.append(" SUM (DECODE (pp.pax_type_code, 'IN', 1, 0 ) )          AS INF_PAX_COUNT ");
				sb.append(" FROM t_reservation r, ");
				sb.append(" t_pnr_passenger pp, ");
				sb.append(" t_pnr_pax_additional_info ppai ");
				sb.append(" WHERE r.pnr       = pp.pnr ");
				sb.append(" AND pp.status     ='CNF' ");
				sb.append(" AND pp.pnr_pax_id = ppai.pnr_pax_id ");
				if (isBothDatesSelected && !isNotEmptyOrNull(flightNo)) {
					sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
					sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
					sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
					sb.append(" AND ppai.departure_intl_flightno = '" + flightNo.trim() + "' ");
				}
				if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append(" AND  ("
							+ ReportUtils.getReplaceStringForIN("r.owner_agent_code", reportsSearchCriteria.getAgents(), false)
							+ ") ");
				}
				sb.append(" GROUP BY ppai.departure_intl_flightno, ");
				sb.append(" ppai.departure_date ");

			}
			sb.append(" ) order by ARRIVAL_DATE,ARRIVAL_TIME,DEPARTURE_DATE,DEPARTURE_TIME");
		} else if (reportType.equals("DETAIL")) {
			sb.append("SELECT * FROM (");
			sb.append("SELECT FLIGHT_NO, ");
			sb.append(" ARRIVAL_DATE, ");
			sb.append(" ARRIVAL_TIME, ");
			sb.append(" DEPARTURE_DATE, ");
			sb.append(" DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
			sb.append(" DOMESTIC_DEPARTURE_DATE, ");
			sb.append(" DOMESTIC_DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
			sb.append(" DOMESTIC_ARRIVAL_DATE, ");
			sb.append(" DOMESTIC_ARRIVAL_TIME, ");
			sb.append(" SUM(TMP_ADT_PAX_COUNT) AS ADT_PAX_COUNT, ");
			sb.append(" SUM(TMP_INF_PAX_COUNT) AS INF_PAX_COUNT, ");
			sb.append(" AGENT_CODE, ");
			sb.append(" AGENT_NAME ");
			sb.append(" FROM ");
			sb.append(" (SELECT r.pnr                                                                                                               AS pnr, ");
			if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate)) {
				sb.append(" ppai.arrival_intl_flightno                                                                                                AS FLIGHT_NO, ");
				sb.append(" TO_CHAR(ppai.arrival_date, 'DD-MON-YYYY')                                                                                 AS ARRIVAL_DATE, ");
				sb.append(" TO_CHAR(ppai.arrival_date, 'HH24:mi')                                                                                     AS ARRIVAL_TIME, ");
				sb.append(" NULL                                                                                                                      AS DEPARTURE_DATE, ");
				sb.append(" NULL                                                                                                                      AS DEPARTURE_TIME, ");
				sb.append(" f.flight_number                                                                                                           AS DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" TO_CHAR(fs.est_time_departure_local, 'DD-MON-YYYY')                                                                       AS DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" TO_CHAR(fs.est_time_departure_local, 'HH24:mi')                                                                           AS DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" NULL                                                                                                                      AS DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" NULL                                                                                                                      AS DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" NULL                                                                                                                      AS DOMESTIC_ARRIVAL_TIME, ");
				sb.append(" SUM (DECODE (pp.pax_type_code, 'AD', 1, 'CH', 1, 0 ) ) 																	  AS TMP_ADT_PAX_COUNT, ");
				sb.append(" SUM (DECODE (pp.pax_type_code, 'IN', 1, 0 ) )          																	  AS TMP_INF_PAX_COUNT, ");
				sb.append(" row_number () over (partition BY r.pnr,ppai.arrival_intl_flightno,ppai.arrival_date order by fs.est_time_departure_zulu ) AS row_number, ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" ppai.departure_intl_flightno                                                                                                    AS FLIGHT_NO, ");
				sb.append(" NULL                                                                                                                            AS ARRIVAL_DATE, ");
				sb.append(" NULL                                                                                                                            AS ARRIVAL_TIME, ");
				sb.append(" TO_CHAR(ppai.departure_date, 'DD-MON-YYYY')                                                                                     AS DEPARTURE_DATE, ");
				sb.append(" TO_CHAR(ppai.departure_date, 'HH24:mi')                                                                                         AS DEPARTURE_TIME, ");
				sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" f.flight_number                                                                                                                 AS DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" TO_CHAR(fs.est_time_arrival_local, 'DD-MON-YYYY')                                                                               AS DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" TO_CHAR(fs.est_time_arrival_local, 'HH24:mi')                                                                                   AS DOMESTIC_ARRIVAL_TIME, ");
				sb.append(" SUM (DECODE (pp.pax_type_code, 'AD', 1, 'CH', 1, 0 ) ) 																	  		AS TMP_ADT_PAX_COUNT, ");
				sb.append(" SUM (DECODE (pp.pax_type_code, 'IN', 1, 0 ) )          																	  		AS TMP_INF_PAX_COUNT, ");
				sb.append(" row_number () over (partition BY r.pnr,ppai.departure_intl_flightno,ppai.departure_date order by fs.est_time_arrival_zulu DESC) AS row_number, ");
			}

			sb.append(" r.owner_agent_code                                                                                                       AS AGENT_CODE, ");
			sb.append(" ta.agent_name                                                                                                             AS AGENT_NAME ");
			sb.append(" FROM t_reservation r, ");
			sb.append(" t_pnr_passenger pp, ");
			sb.append(" t_pnr_pax_additional_info ppai, ");
			sb.append(" t_pnr_segment ps, ");
			sb.append(" t_flight_segment fs, ");
			sb.append(" t_flight f, ");
			sb.append(" t_agent ta ");
			sb.append(" WHERE r.pnr             = pp.pnr ");
			sb.append(" AND pp.status           ='CNF' ");
			sb.append(" AND pp.pnr_pax_id       = ppai.pnr_pax_id ");
			sb.append(" AND r.pnr               = ps.pnr ");
			sb.append(" AND ps.status           = 'CNF' ");
			sb.append(" AND ps.flt_seg_id       = fs.flt_seg_id ");
			sb.append(" AND fs.flight_id        = f.flight_id ");
			sb.append(" AND r.owner_agent_code = ta.agent_code ");
			if (isBothDatesSelected && !isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			} else if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate)) {
				sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			}

			if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
				sb.append(" AND  ("
						+ ReportUtils.getReplaceStringForIN("r.owner_agent_code", reportsSearchCriteria.getAgents(), false)
						+ ") ");
			}
			if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.arrival_intl_flightno = '" + flightNo.trim() + "' ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.departure_intl_flightno = '" + flightNo.trim() + "' ");
			}
			sb.append(" GROUP BY r.pnr, ");
			if ((isBothDatesSelected && !isNotEmptyOrNull(flightNo))
					|| (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate))) {
				sb.append(" ppai.arrival_intl_flightno, ");
				sb.append(" ppai.arrival_date, ");
				sb.append(" fs.est_time_departure_zulu, ");
				sb.append(" fs.est_time_departure_local, ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" ppai.departure_intl_flightno, ");
				sb.append(" ppai.departure_date, ");
				sb.append(" fs.est_time_arrival_zulu, ");
				sb.append(" fs.est_time_arrival_local, ");
			}
			sb.append(" f.flight_number, ");
			sb.append(" r.owner_agent_code, ");
			sb.append(" ta.agent_name ");
			sb.append("  ) c ");
			sb.append(" WHERE c.row_number=1 ");
			sb.append(" GROUP BY FLIGHT_NO, ");
			sb.append(" ARRIVAL_DATE, ");
			sb.append(" ARRIVAL_TIME, ");
			sb.append(" DEPARTURE_DATE, ");
			sb.append(" DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
			sb.append(" DOMESTIC_DEPARTURE_DATE, ");
			sb.append(" DOMESTIC_DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
			sb.append(" DOMESTIC_ARRIVAL_DATE, ");
			sb.append(" DOMESTIC_ARRIVAL_TIME, ");
			sb.append(" AGENT_CODE, ");
			sb.append(" AGENT_NAME ");
			if (isBothDatesSelected) {
				sb.append(" UNION ALL ");
				sb.append("SELECT FLIGHT_NO, ");
				sb.append(" ARRIVAL_DATE, ");
				sb.append(" ARRIVAL_TIME, ");
				sb.append(" DEPARTURE_DATE, ");
				sb.append(" DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" DOMESTIC_ARRIVAL_TIME, ");
				sb.append(" SUM(TMP_ADT_PAX_COUNT) AS ADT_PAX_COUNT, ");
				sb.append(" SUM(TMP_INF_PAX_COUNT) AS INF_PAX_COUNT, ");
				sb.append(" AGENT_CODE, ");
				sb.append(" AGENT_NAME ");
				sb.append(" FROM ");
				if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
					sb.append(" (SELECT r.pnr                                                                                                               AS pnr, ");
					sb.append(" ppai.departure_intl_flightno                                                                                                    AS FLIGHT_NO, ");
					sb.append(" NULL                                                                                                                            AS ARRIVAL_DATE, ");
					sb.append(" NULL                                                                                                                            AS ARRIVAL_TIME, ");
					sb.append(" TO_CHAR(ppai.departure_date, 'DD-MON-YYYY')                                                                                     AS DEPARTURE_DATE, ");
					sb.append(" TO_CHAR(ppai.departure_date, 'HH24:mi')                                                                                         AS DEPARTURE_TIME, ");
					sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_FLT_NO, ");
					sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_DATE, ");
					sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_TIME, ");
					sb.append(" f.flight_number                                                                                                                 AS DOMESTIC_ARRIVAL_FLT_NO, ");
					sb.append(" TO_CHAR(fs.est_time_arrival_local, 'DD-MON-YYYY')                                                                               AS DOMESTIC_ARRIVAL_DATE, ");
					sb.append(" TO_CHAR(fs.est_time_arrival_local, 'HH24:mi')                                                                                   AS DOMESTIC_ARRIVAL_TIME, ");
					sb.append(" SUM (DECODE (pp.pax_type_code, 'AD', 1, 'CH', 1, 0 ) ) 																	  		AS TMP_ADT_PAX_COUNT, ");
					sb.append(" SUM (DECODE (pp.pax_type_code, 'IN', 1, 0 ) )          																	  		AS TMP_INF_PAX_COUNT, ");
					sb.append(" row_number () over (partition BY r.pnr,ppai.departure_intl_flightno,ppai.departure_date order by fs.est_time_arrival_zulu DESC) AS row_number, ");
					sb.append(" r.owner_agent_code                                                                                                       AS AGENT_CODE, ");
					sb.append(" ta.agent_name                                                                                                             AS AGENT_NAME ");
				}
				sb.append(" FROM t_reservation r, ");
				sb.append(" t_pnr_passenger pp, ");
				sb.append(" t_pnr_pax_additional_info ppai, ");
				sb.append(" t_pnr_segment ps, ");
				sb.append(" t_flight_segment fs, ");
				sb.append(" t_flight f, ");
				sb.append(" t_agent ta ");
				sb.append(" WHERE r.pnr             = pp.pnr ");
				sb.append(" AND pp.status           ='CNF' ");
				sb.append(" AND pp.pnr_pax_id       = ppai.pnr_pax_id ");
				sb.append(" AND r.pnr               = ps.pnr ");
				sb.append(" AND ps.status           = 'CNF' ");
				sb.append(" AND ps.flt_seg_id       = fs.flt_seg_id ");
				sb.append(" AND fs.flight_id        = f.flight_id ");
				sb.append(" AND r.origin_agent_code = ta.agent_code ");
				if (isBothDatesSelected && !isNotEmptyOrNull(flightNo)) {
					sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
					sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
					sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
					sb.append(" AND ppai.departure_intl_flightno = '" + flightNo.trim() + "' ");
				}
				if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append(" AND  ("
							+ ReportUtils.getReplaceStringForIN("r.owner_agent_code", reportsSearchCriteria.getAgents(), false)
							+ ") ");
				}
				sb.append(" GROUP BY r.pnr, ");
				sb.append(" ppai.departure_intl_flightno, ");
				sb.append(" ppai.departure_date, ");
				sb.append(" f.flight_number, ");
				sb.append(" fs.est_time_arrival_zulu, ");
				sb.append(" fs.est_time_arrival_local, ");
				sb.append(" r.owner_agent_code, ");
				sb.append(" ta.agent_name ");
				sb.append("  ) c ");
				sb.append(" WHERE c.row_number=1 ");
				sb.append(" GROUP BY FLIGHT_NO, ");
				sb.append(" ARRIVAL_DATE, ");
				sb.append(" ARRIVAL_TIME, ");
				sb.append(" DEPARTURE_DATE, ");
				sb.append(" DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" DOMESTIC_ARRIVAL_TIME, ");
				sb.append(" AGENT_CODE, ");
				sb.append(" AGENT_NAME ");
			}
			sb.append(") ORDER BY AGENT_CODE,ARRIVAL_DATE,ARRIVAL_TIME,DEPARTURE_DATE,DEPARTURE_TIME");
		} else if (reportType.equals("PAX_DETAILS")) {
			sb.append("SELECT * FROM (");
			sb.append("SELECT FLIGHT_NO, ");
			sb.append(" ARRIVAL_DATE, ");
			sb.append(" ARRIVAL_TIME, ");
			sb.append(" DEPARTURE_DATE, ");
			sb.append(" DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
			sb.append(" DOMESTIC_DEPARTURE_DATE, ");
			sb.append(" DOMESTIC_DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
			sb.append(" DOMESTIC_ARRIVAL_DATE, ");
			sb.append(" DOMESTIC_ARRIVAL_TIME, ");
			sb.append(" AGENT_CODE, ");
			sb.append(" AGENT_NAME, ");
			sb.append(" ETICKET, ");
			sb.append(" FIRST_NAME, ");
			sb.append(" LAST_NAME, ");
			sb.append(" TITLE, ");
			sb.append(" STATUS, ");
			sb.append(" SEGMENT, ");
			sb.append(" PNR, ");
			sb.append(" PAX_TYPE, ");
			sb.append(" PAX_GROUP ");
			sb.append(" FROM ");

			sb.append(" (SELECT r.pnr                                                                                                               AS PNR, ");
			if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate)) {
				sb.append(" ppai.arrival_intl_flightno                                                                                                AS FLIGHT_NO, ");
				sb.append(" TO_CHAR(ppai.arrival_date, 'DD-MON-YYYY')                                                                                 AS ARRIVAL_DATE, ");
				sb.append(" TO_CHAR(ppai.arrival_date, 'HH24:mi')                                                                                     AS ARRIVAL_TIME, ");
				sb.append(" NULL                                                                                                                      AS DEPARTURE_DATE, ");
				sb.append(" NULL                                                                                                                      AS DEPARTURE_TIME, ");
				sb.append(" f.flight_number                                                                                                           AS DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" TO_CHAR(fs.est_time_departure_local, 'DD-MON-YYYY')                                                                       AS DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" TO_CHAR(fs.est_time_departure_local, 'HH24:mi')                                                                           AS DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" NULL                                                                                                                      AS DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" NULL                                                                                                                      AS DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" NULL                                                                                                                      AS DOMESTIC_ARRIVAL_TIME, ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" ppai.departure_intl_flightno                                                                                                    AS FLIGHT_NO, ");
				sb.append(" NULL                                                                                                                            AS ARRIVAL_DATE, ");
				sb.append(" NULL                                                                                                                            AS ARRIVAL_TIME, ");
				sb.append(" TO_CHAR(ppai.departure_date, 'DD-MON-YYYY')                                                                                     AS DEPARTURE_DATE, ");
				sb.append(" TO_CHAR(ppai.departure_date, 'HH24:mi')                                                                                         AS DEPARTURE_TIME, ");
				sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" f.flight_number                                                                                                                 AS DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" TO_CHAR(fs.est_time_arrival_local, 'DD-MON-YYYY')                                                                               AS DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" TO_CHAR(fs.est_time_arrival_local, 'HH24:mi')                                                                                   AS DOMESTIC_ARRIVAL_TIME, ");
			}

			sb.append(" fs.segment_code                                                                                                       AS SEGMENT, ");
			sb.append(" r.owner_agent_code                                                                                                       AS AGENT_CODE, ");
			sb.append(" ta.agent_name                                                                                                             AS AGENT_NAME, ");
			sb.append(" et.e_ticket_number                                                                                                        AS ETICKET, ");
			sb.append(" pp.status                                                                                                        AS STATUS, ");
			sb.append(" pp.first_name                                                                                                             AS FIRST_NAME, ");
			sb.append(" pp.last_name                                                                                                              AS LAST_NAME, ");
			sb.append(" pp.title                                                                                                                  AS TITLE, ");
			sb.append(" pp.pax_type_code                                                                                                          AS PAX_TYPE, ");
			sb.append(" ppai.group_id                                                                                                             AS PAX_GROUP ");

			sb.append(" FROM t_reservation r, ");
			sb.append(" t_pnr_passenger pp, ");
			sb.append(" t_pnr_pax_additional_info ppai, ");
			sb.append(" t_pnr_segment ps, ");
			sb.append(" t_flight_segment fs, ");
			sb.append(" t_flight f, ");
			sb.append(" t_agent ta, ");
			sb.append(" t_pax_e_ticket et ");
			sb.append(" WHERE r.pnr             = pp.pnr ");
			sb.append(" AND pp.status           ='CNF' ");
			sb.append(" AND pp.pnr_pax_id       = ppai.pnr_pax_id ");
			sb.append(" AND et.pnr_pax_id = ppai.pnr_pax_id ");
			sb.append(" AND r.pnr               = ps.pnr ");
			sb.append(" AND ps.status           = 'CNF' ");
			sb.append(" AND ps.flt_seg_id       = fs.flt_seg_id ");
			sb.append(" AND fs.flight_id        = f.flight_id ");
			sb.append(" AND r.owner_agent_code = ta.agent_code ");
			if (isBothDatesSelected && !isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND fs.est_time_arrival_zulu BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND fs.est_time_departure_zulu BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			} else if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate)) {
				sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND fs.est_time_arrival_zulu BETWEEN TO_DATE('" + arrFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				sb.append(" AND fs.est_time_departure_zulu BETWEEN TO_DATE('" + departFromDate
						+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
						+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
			}

			if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
				sb.append(" AND  ("
						+ ReportUtils.getReplaceStringForIN("r.owner_agent_code", reportsSearchCriteria.getAgents(), false)
						+ ") ");
			}
			if (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.arrival_intl_flightno = '" + flightNo.trim() + "' ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(flightNo)) {
				sb.append(" AND ppai.departure_intl_flightno = '" + flightNo.trim() + "' ");
			}
			sb.append(" GROUP BY r.pnr, ");
			if ((isBothDatesSelected && !isNotEmptyOrNull(flightNo))
					|| (isNotEmptyOrNull(arrFromDate) && isNotEmptyOrNull(arrToDate))) {
				sb.append(" ppai.arrival_intl_flightno, ");
				sb.append(" ppai.arrival_date, ");
				sb.append(" fs.est_time_departure_zulu, ");
				sb.append(" fs.est_time_departure_local, ");
			} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
				sb.append(" ppai.departure_intl_flightno, ");
				sb.append(" ppai.departure_date, ");
				sb.append(" fs.est_time_arrival_zulu, ");
				sb.append(" fs.est_time_arrival_local, ");
			}
			sb.append(" f.flight_number, ");
			sb.append(" r.owner_agent_code, ");
			sb.append(" ta.agent_name, ");
			sb.append(" fs.segment_code, ");
			sb.append(" et.e_ticket_number, ");
			sb.append(" pp.first_name, ");
			sb.append(" pp.last_name, ");
			sb.append(" pp.title, ");
			sb.append(" pp.status, ");
			sb.append(" pp.pax_type_code, ");
			sb.append(" ppai.group_id ");
			sb.append("  ) c ");
			sb.append(" GROUP BY FLIGHT_NO, ");
			sb.append(" ARRIVAL_DATE, ");
			sb.append(" ARRIVAL_TIME, ");
			sb.append(" DEPARTURE_DATE, ");
			sb.append(" DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
			sb.append(" DOMESTIC_DEPARTURE_DATE, ");
			sb.append(" DOMESTIC_DEPARTURE_TIME, ");
			sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
			sb.append(" DOMESTIC_ARRIVAL_DATE, ");
			sb.append(" DOMESTIC_ARRIVAL_TIME, ");
			sb.append(" AGENT_CODE, ");
			sb.append(" AGENT_NAME, ");
			sb.append(" ETICKET, ");
			sb.append(" FIRST_NAME, ");
			sb.append(" LAST_NAME, ");
			sb.append(" TITLE, ");
			sb.append(" STATUS, ");
			sb.append(" SEGMENT, ");
			sb.append(" PNR, ");
			sb.append(" PAX_TYPE, ");
			sb.append(" PAX_GROUP ");

			if (isBothDatesSelected) {
				sb.append(" UNION ALL ");
				sb.append("SELECT FLIGHT_NO, ");
				sb.append(" ARRIVAL_DATE, ");
				sb.append(" ARRIVAL_TIME, ");
				sb.append(" DEPARTURE_DATE, ");
				sb.append(" DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" DOMESTIC_ARRIVAL_TIME, ");
				sb.append(" AGENT_CODE, ");
				sb.append(" AGENT_NAME, ");
				sb.append(" ETICKET, ");
				sb.append(" FIRST_NAME, ");
				sb.append(" LAST_NAME, ");
				sb.append(" TITLE, ");
				sb.append(" STATUS, ");
				sb.append(" SEGMENT, ");
				sb.append(" PNR, ");
				sb.append(" PAX_TYPE, ");
				sb.append(" PAX_GROUP ");
				sb.append(" FROM ");
				if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
					sb.append(" (SELECT r.pnr                                                                                                               AS PNR, ");
					sb.append(" ppai.departure_intl_flightno                                                                                                    AS FLIGHT_NO, ");
					sb.append(" NULL                                                                                                                            AS ARRIVAL_DATE, ");
					sb.append(" NULL                                                                                                                            AS ARRIVAL_TIME, ");
					sb.append(" TO_CHAR(ppai.departure_date, 'DD-MON-YYYY')                                                                                     AS DEPARTURE_DATE, ");
					sb.append(" TO_CHAR(ppai.departure_date, 'HH24:mi')                                                                                         AS DEPARTURE_TIME, ");
					sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_FLT_NO, ");
					sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_DATE, ");
					sb.append(" NULL                                                                                                                            AS DOMESTIC_DEPARTURE_TIME, ");
					sb.append(" f.flight_number                                                                                                                 AS DOMESTIC_ARRIVAL_FLT_NO, ");
					sb.append(" TO_CHAR(fs.est_time_arrival_local, 'DD-MON-YYYY')                                                                               AS DOMESTIC_ARRIVAL_DATE, ");
					sb.append(" TO_CHAR(fs.est_time_arrival_local, 'HH24:mi')                                                                                   AS DOMESTIC_ARRIVAL_TIME, ");
					sb.append(" r.owner_agent_code                                                                                                       AS AGENT_CODE, ");
					sb.append(" ta.agent_name                                                                                                             AS AGENT_NAME, ");
					sb.append(" et.e_ticket_number                                                                                                         AS ETICKET, ");
					sb.append(" pp.first_name                                                                                                             AS FIRST_NAME, ");
					sb.append(" pp.last_name                                                                                                              AS LAST_NAME, ");
					sb.append(" pp.title                                                                                                                  AS TITLE, ");
					sb.append(" pp.pax_type_code                                                                                                          AS PAX_TYPE, ");
					sb.append(" pp.status                                                                                                        AS STATUS, ");
					sb.append(" fs.segment_code                                                                                                       AS SEGMENT, ");
					sb.append(" ppai.group_id                                                                                                             AS PAX_GROUP ");

				}
				sb.append(" FROM t_reservation r, ");
				sb.append(" t_pnr_passenger pp, ");
				sb.append(" t_pnr_pax_additional_info ppai, ");
				sb.append(" t_pnr_segment ps, ");
				sb.append(" t_flight_segment fs, ");
				sb.append(" t_flight f, ");
				sb.append(" t_agent ta, ");
				sb.append(" t_pax_e_ticket et ");
				sb.append(" WHERE r.pnr             = pp.pnr ");
				sb.append(" AND pp.status           ='CNF' ");
				sb.append(" AND pp.pnr_pax_id       = ppai.pnr_pax_id ");
				sb.append(" AND et.pnr_pax_id = ppai.pnr_pax_id ");
				sb.append(" AND r.pnr               = ps.pnr ");
				sb.append(" AND ps.status           = 'CNF' ");
				sb.append(" AND ps.flt_seg_id       = fs.flt_seg_id ");
				sb.append(" AND fs.flight_id        = f.flight_id ");
				sb.append(" AND r.owner_agent_code = ta.agent_code ");
				if (isBothDatesSelected && !isNotEmptyOrNull(flightNo)) {
					sb.append(" AND ppai.arrival_date BETWEEN TO_DATE('" + arrFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + arrToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
					sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
				} else if (isNotEmptyOrNull(departFromDate) && isNotEmptyOrNull(departToDate)) {
					sb.append(" AND ppai.departure_date BETWEEN TO_DATE('" + departFromDate
							+ " 00:00:00','DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + departToDate
							+ " 23:59:59','DD-MON-YYYY HH24:MI:SS') ");
					sb.append(" AND ppai.departure_intl_flightno = '" + flightNo.trim() + "' ");
				}
				if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
					sb.append(" AND  ("
							+ ReportUtils.getReplaceStringForIN("r.owner_agent_code", reportsSearchCriteria.getAgents(), false)
							+ ") ");
				}
				sb.append(" GROUP BY r.pnr, ");
				sb.append(" ppai.departure_intl_flightno, ");
				sb.append(" ppai.departure_date, ");
				sb.append(" f.flight_number, ");
				sb.append(" fs.est_time_arrival_zulu, ");
				sb.append(" fs.est_time_arrival_local, ");
				sb.append(" fs.segment_code, ");
				sb.append(" r.owner_agent_code, ");
				sb.append(" ta.agent_name, ");
				sb.append(" et.e_ticket_number, ");
				sb.append(" pp.first_name, ");
				sb.append(" pp.last_name, ");
				sb.append(" pp.title, ");
				sb.append(" pp.status, ");
				sb.append(" pp.pax_type_code, ");
				sb.append(" ppai.group_id ");
				sb.append("  ) c ");
				sb.append(" GROUP BY FLIGHT_NO, ");
				sb.append(" ARRIVAL_DATE, ");
				sb.append(" ARRIVAL_TIME, ");
				sb.append(" DEPARTURE_DATE, ");
				sb.append(" DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_DEPARTURE_FLT_NO, ");
				sb.append(" DOMESTIC_DEPARTURE_DATE, ");
				sb.append(" DOMESTIC_DEPARTURE_TIME, ");
				sb.append(" DOMESTIC_ARRIVAL_FLT_NO, ");
				sb.append(" DOMESTIC_ARRIVAL_DATE, ");
				sb.append(" DOMESTIC_ARRIVAL_TIME, ");
				sb.append(" AGENT_CODE, ");
				sb.append(" AGENT_NAME,  ");
				sb.append(" ETICKET, ");
				sb.append(" FIRST_NAME, ");
				sb.append(" LAST_NAME, ");
				sb.append(" TITLE, ");
				sb.append(" STATUS, ");
				sb.append(" SEGMENT, ");
				sb.append(" PNR, ");
				sb.append(" PAX_TYPE, ");
				sb.append(" PAX_GROUP ");
			}
			sb.append(") ORDER BY AGENT_CODE,ARRIVAL_DATE,ARRIVAL_TIME,DEPARTURE_DATE,DEPARTURE_TIME");
		}
		return sb.toString();
	}

	public String getPassengerInternationalFlightDetailsDataQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		if (reportsSearchCriteria.getReportOption().equals("SUMMARY")) {

			sb.append("SELECT  segment_code,  count(pnr_pax_id) as count,  ");
			if (reportsSearchCriteria.getJourneyType().equals("ARRIVAL")) {
				sb.append("arrival_intl_flightno intFlit ");
			} else {
				sb.append("departure_intl_flightno intFlit ");
			}
			sb.append(" FROM ( SELECT  fs.segment_code,  pp.pnr_pax_id,  ");
			if (reportsSearchCriteria.getJourneyType().equals("ARRIVAL")) {
				sb.append("arrival_intl_flightno ");
				sb.append(", ROW_NUMBER() over (partition by pp.pnr_pax_id  order by fs.EST_TIME_DEPARTURE_ZULU asc) as pseg_order ");
			} else {
				sb.append("departure_intl_flightno  ");
				sb.append(", ROW_NUMBER() over (partition by pp.pnr_pax_id  order by fs.EST_TIME_DEPARTURE_ZULU desc) as pseg_order ");
			}
			sb.append(" FROM t_pnr_segment ps, ");
			sb.append(" t_flight_segment fs, ");
			sb.append(" t_pnr_passenger pp, ");
			sb.append(" t_pnr_pax_additional_info ppai, ");
			sb.append(" t_flight tf, ");
			sb.append(" t_reservation r, ");
			sb.append(" t_route_info tri ");

			sb.append("WHERE ");
			sb.append("ps.flt_seg_id=fs.flt_seg_id ");
			sb.append("AND pp.pnr=ps.pnr ");
			sb.append("AND ps.pnr = r.pnr ");
			sb.append("AND pp.pnr_pax_id  = ppai.pnr_pax_id ");
			sb.append("AND fs.flight_id=tf.flight_id ");
			sb.append("AND fs.flight_id=tf.flight_id ");
			sb.append(" AND ps.status           = 'CNF' ");

			if (!(reportsSearchCriteria.getOriginAirport().isEmpty())) {
				sb.append("AND tri.from_airport ='" + reportsSearchCriteria.getOriginAirport() + "' ");
			}
			if (!(reportsSearchCriteria.getDestinationAirport().isEmpty())) {
				sb.append("AND tri.to_airport ='" + reportsSearchCriteria.getDestinationAirport() + "' ");
			}
			sb.append("AND fs.segment_code = tri.ond_code ");

			if (reportsSearchCriteria.getJourneyType().equals("ARRIVAL")) {
				sb.append("AND ppai.arrival_intl_flightno IS NOT NULL ");
				sb.append("AND ppai.arrival_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') and to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') + 0.9999 ");
			} else {
				sb.append("AND ppai.departure_intl_flightno IS NOT NULL ");
				sb.append("AND ppai.departure_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') and to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') + 0.9999 ");
			}
			// sb.append("AND to_date(fs.est_time_departure_local, 'dd-MON-YY') = to_date('"
			// + reportsSearchCriteria.getDateRangeFrom() + "','dd/mm/YY')");
			sb.append("AND r.owner_agent_code IN ( ");
			sb.append(Util.buildStringInClauseContent(reportsSearchCriteria.getAgents()));
			sb.append(") ) WHERE  pseg_order = 1 ");
			sb.append(" group by segment_code,");
			if (reportsSearchCriteria.getJourneyType().equals("ARRIVAL")) {
				sb.append("arrival_intl_flightno ");
			} else {
				sb.append("departure_intl_flightno ");
			}

		} else {

			long checkingTimeInSeconds;
			try {
				checkingTimeInSeconds = AppSysParamsUtil.getCheckingTimeDifferenceInMiliSeconds() / 1000;
			} catch (ModuleException e) {
				log.error("Retriving check in time from app parameter error", e);
				checkingTimeInSeconds = 0;
			}
			sb.append("SELECT * FROM ( SELECT pp.first_name, pp.last_name, pp.title, ppai.arrival_intl_flightno, r.owner_agent_code, tf.flight_number, ");
			if (reportsSearchCriteria.getJourneyType().equals("ARRIVAL")) {
				sb.append("ppai.arrival_intl_flightno as intFltNo, ");
				sb.append("TO_CHAR( ppai.arrival_date, 'DD-MON-YYYY') as int_date, ");
				sb.append("TO_CHAR( ppai.arrival_date, 'HH24:MI') as int_time, ");
				sb.append("ROW_NUMBER() over (partition by pp.pnr_pax_id  order by fs.EST_TIME_DEPARTURE_ZULU asc) as pseg_order,");
			} else {
				sb.append("ppai.departure_intl_flightno as intFltNo, ");
				sb.append("TO_CHAR( ppai.departure_date, 'DD-MON-YYYY') as int_date,  ");
				sb.append("TO_CHAR( ppai.departure_date, 'HH24:MI') as int_time,  ");
				sb.append("ROW_NUMBER() over (partition by pp.pnr_pax_id  order by fs.EST_TIME_DEPARTURE_ZULU desc) as pseg_order,");
			}
			sb.append("TO_CHAR( fs.est_time_departure_local, 'DD-MON-YYYY') as domestic_departure_date, ");
			sb.append("TO_CHAR( fs.est_time_departure_local, 'HH24:MI') as domestic_departure_time, ");
			sb.append("TO_CHAR( fs.est_time_arrival_local, 'DD-MON-YYYY') as domestic_arrival_date, ");
			sb.append("TO_CHAR( fs.est_time_arrival_local, 'HH24:MI') as domestic_arrival_time, ");
			sb.append("TO_CHAR(fs.est_time_departure_local - (NVL((");
			sb.append("(select ct.checkin_gap_mins from t_airport_checkin_times ct ");
			sb.append("where ct.flight_number = tf.flight_number ");
			sb.append("and ct.airport_code    = SUBSTR(fs.segment_code, 1,3) ");
			sb.append("and rownum             = 1 )/1440),NVL((");
			sb.append("(select ct.checkin_gap_mins from t_airport_checkin_times ct ");
			sb.append("where ct.carrier_code = SUBSTR(tf.flight_number,1,2) ");
			sb.append("and ct.airport_code   = SUBSTR(fs.segment_code, 1,3) ");
			sb.append("and rownum            = 1 )/1440),NVL((");
			sb.append("(select ct.checkin_gap_mins from t_airport_checkin_times ct ");
			sb.append("where ct.airport_code   = SUBSTR(fs.segment_code, 1,3) ");
			sb.append("and rownum            = 1 )/1440)," + checkingTimeInSeconds + " / 86400");
			sb.append(")))), 'DD-MON-YYYY HH24:MI' ) as CHECKING_TIME, ");
			sb.append("NVL(ppai.group_id,' ') group_id, ");
			sb.append("TO_CHAR(fs.est_time_departure_local, 'DD-MON-YYYY') est_time_departure_local, ");
			sb.append("pp.date_of_birth, ");
			sb.append("fs.segment_code, ");
			sb.append("r.pnr, ");
			sb.append("COUNT(*)  over(partition by fs.segment_code,ppai.departure_intl_flightno order by fs.segment_code) as count ");
			sb.append("FROM ");
			sb.append("t_pnr_segment ps,t_flight_segment fs,t_pnr_passenger pp,t_pnr_pax_additional_info ppai,t_flight tf,");
			sb.append("t_reservation r, ");
			sb.append(" t_route_info tri ");
			sb.append("WHERE ");
			sb.append("ps.flt_seg_id=fs.flt_seg_id ");
			sb.append("AND pp.pnr=ps.pnr ");
			sb.append("AND ps.pnr = r.pnr ");
			sb.append("AND pp.pnr_pax_id  = ppai.pnr_pax_id ");
			sb.append("AND fs.flight_id=tf.flight_id ");
			sb.append(" AND ps.status           = 'CNF' ");

			if (!(reportsSearchCriteria.getOriginAirport().isEmpty())) {
				sb.append("AND tri.from_airport ='" + reportsSearchCriteria.getOriginAirport() + "' ");
			}
			if (!(reportsSearchCriteria.getDestinationAirport().isEmpty())) {
				sb.append("AND tri.to_airport ='" + reportsSearchCriteria.getDestinationAirport() + "' ");
			}
			sb.append("AND fs.segment_code = tri.ond_code ");
			if (reportsSearchCriteria.getJourneyType().equals("ARRIVAL")) {
				sb.append("AND ppai.arrival_intl_flightno IS NOT NULL ");
				sb.append("AND ppai.arrival_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') and to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') + 0.9999 ");
			} else {
				sb.append("AND ppai.departure_intl_flightno IS NOT NULL ");
				
				sb.append("AND ppai.departure_date between to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') and to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "','dd/mm/YYYY') + 0.9999 ");
			}
			// sb.append("AND to_date(fs.est_time_departure_local, 'dd-MON-YY') = to_date('"
			// + reportsSearchCriteria.getDateRangeFrom() + "','dd/mm/YY')");
			sb.append("AND r.owner_agent_code IN ( ");
			sb.append(Util.buildStringInClauseContent(reportsSearchCriteria.getAgents()));
			sb.append(") ");
			sb.append(" )  WHERE pseg_order = 1" );
			sb.append(" ORDER BY flight_number,est_time_departure_local,pnr");
		}

		return sb.toString();
	}

	public String getFlightLoyaltyMembersDataQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT fs.EST_TIME_DEPARTURE_LOCAL as DEPARTURE_DATE, f.FLIGHT_NUMBER as FLIGHT_NUMBER,");
		sb.append(" fs.SEGMENT_CODE as SEGMENT_CODE, ppax.FFID as FFID, lmsm.FIRST_NAME as FIRST_NAME,");
		sb.append(" lmsm.LAST_NAME as LAST_NAME,  lmsm.MOBILE_PHONE_NUMBER as MOBILE_PHONE_NUMBER,");
		sb.append(" r.PNR as PNR FROM T_PNR_PAX_ADDITIONAL_INFO ppax,");
		sb.append(" T_PNR_PASSENGER ppx, T_RESERVATION r, T_LMS_MEMBER lmsm,");
		sb.append(" T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, T_FLIGHT f ");
		sb.append(" WHERE FS.EST_TIME_DEPARTURE_LOCAL BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
				+ " 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
				+ " 23:59:59','DD/MM/YYYY HH24:MI:SS') ");

		sb.append(" AND ppax.PNR_PAX_ID = ppx.PNR_PAX_ID");
		sb.append(" AND ppx.PNR         = r.PNR");
		sb.append(" AND r.PNR           = ps.PNR");
		sb.append(" AND ps.FLT_SEG_ID   = fs.FLT_SEG_ID");
		sb.append(" AND fs.FLIGHT_ID    = f.FLIGHT_ID");
		sb.append(" AND lmsm.EMAIL_ID   = ppax.FFID");
		sb.append(" AND r.STATUS <> 'CNX'");
		sb.append(" AND ps.STATUS <> 'CNX'");

		if (reportsSearchCriteria.getFlightNumber() != null && !"".equals(reportsSearchCriteria.getFlightNumber())) {
			sb.append(" AND f.FLIGHT_NUMBER = '" + reportsSearchCriteria.getFlightNumber() + "'");
		}

		if (reportsSearchCriteria.getViaPoints() != null) {
			String combination = PlatformUtiltiies.getCombinationsForViaPoints(reportsSearchCriteria.getViaPoints());
			sb.append(" AND fs.SEGMENT_CODE IN " + combination);
		} else if ((!"".equals(reportsSearchCriteria.getFrom()) && reportsSearchCriteria.getFrom() != null)
				&& (!"".equals(reportsSearchCriteria.getTo()) && reportsSearchCriteria.getTo() != null)) {
			sb.append(" AND fs.SEGMENT_CODE = '" + reportsSearchCriteria.getFrom() + "/" + reportsSearchCriteria.getTo() + "'");
		} else if ((!"".equals(reportsSearchCriteria.getFrom()) && reportsSearchCriteria.getFrom() != null)) {
			sb.append(" AND fs.SEGMENT_CODE like '" + reportsSearchCriteria.getFrom() + "/%'");
		} else if ((!"".equals(reportsSearchCriteria.getTo()) && reportsSearchCriteria.getTo() != null)) {
			sb.append(" AND fs.SEGMENT_CODE = '%/" + reportsSearchCriteria.getTo() + "'");
		}

		sb.append(" ORDER BY fs.EST_TIME_DEPARTURE_LOCAL, f.FLIGHT_NUMBER, fs.SEGMENT_CODE, r.pnr");

		return sb.toString();
	}

	public String getPointRedemptionReportQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		sb.append(" SELECT EST_TIME_DEPARTURE_LOCAL, FLIGHT_NUMBER, segment_code, pnr,");
		sb.append(" pax_name, member_name, email_id, mobile_phone_number, reward_ids,");
		sb.append(" loyalty_product_code, SUM(amount) total_value_base, ROUND(SUM(amount/exch_rate),2) total_value_usd,");
		sb.append(" ROUND(SUM(amount/exch_rate),2)*(SELECT REPLACE(SUBSTR(APP.PARAM_VALUE, INSTR(APP.PARAM_VALUE,");
		sb.append(" 'CURRENCY_TO_POINTS=', -1), LENGTH(APP.PARAM_VALUE)), 'CURRENCY_TO_POINTS=' , '')");
		sb.append(" FROM T_APP_PARAMETER app WHERE APP.PARAM_KEY = 'AIRLINE_233') total_points");
		sb.append(" FROM (SELECT c.EST_TIME_DEPARTURE_LOCAL,  i.FLIGHT_NUMBER,  c.SEGMENT_CODE,");
		sb.append(" a.pnr,  g.TITLE || ' ' || g.FIRST_NAME || ' '");
		sb.append(" || g.LAST_NAME pax_name, l.FIRST_NAME || ' '");
		sb.append(" || l.LAST_NAME member_name, l.email_id, l.mobile_phone_number,");
		sb.append(" REPLACE(PT.REMARKS,'LMS Reward IDs' ,'' ) reward_ids, (CASE");
		sb.append(" WHEN o.loyalty_product_code IS NOT NULL THEN o.loyalty_product_code");
		sb.append(" WHEN m.charge_group_code='FAR' THEN 'FLIGHT'");
		sb.append(" WHEN m.charge_group_code   IN ('SUR', 'INF') AND o.loyalty_product_code IS NULL");
		sb.append(" THEN 'FLIGHT' END)loyalty_product_code, SUM(n.amount)amount,");
		sb.append(" MAX((SELECT exrate_base_to_cur FROM t_currency_exchange_rate WHERE currency_code='USD'");
		sb.append(" AND charge_date BETWEEN effective_from AND effective_to AND ROWNUM=1");
		sb.append(" ))exch_rate FROM t_reservation a, t_pnr_segment b, t_flight_segment c,");
		sb.append(" t_pnr_pax_fare_segment d, t_pnr_pax_ond_charges e, t_pnr_pax_seg_charges f,");
		sb.append(" t_pnr_passenger g, t_pnr_pax_fare h, t_flight i , t_charge_rate j ,");
		sb.append(" T_LMS_MEMBER l, t_pnr_pax_ond_payments m, t_pnr_pax_seg_payments n ,");
		sb.append(" t_loyalty_product_charge o, T_PAX_TRANSACTION pt WHERE a.pnr= b.pnr");
		sb.append(" AND a.pnr                   =g.pnr  				AND b.flt_seg_id            = c.flt_seg_id");
		sb.append(" AND d.pnr_seg_id            = b.pnr_seg_id  		AND h.ppf_id                = e.ppf_id");
		sb.append(" AND f.pft_id                = e.pft_id  			AND f.ppfs_id               =d.ppfs_id");
		sb.append(" AND g.pnr_pax_id            = h.pnr_pax_id 			AND h.ppf_id                = d.ppf_id");
		sb.append(" AND c.flight_id             =i.flight_id 			AND e.charge_rate_id        =j.charge_rate_id(+)");
		sb.append(" AND PT.TXN_ID               = M.PAYMENT_TXN_ID 		AND PT.EXT_REFERENCE = L.EMAIL_ID");
		sb.append(" AND e.pft_id                =m.pft_id 				AND m.ppop_id               =n.ppop_id");
		sb.append(" AND n.pfst_id               =f.pfst_id 				AND m.nominal_code          =46");
		sb.append(" AND o.charge_code(+)=j.charge_code");

		if (reportsSearchCriteria.getFlightNumber() != null && !"".equals(reportsSearchCriteria.getFlightNumber())) {
			sb.append(" AND i.FLIGHT_NUMBER = '" + reportsSearchCriteria.getFlightNumber() + "' ");
		}

		if (reportsSearchCriteria.getViaPoints() != null) {
			String combination = PlatformUtiltiies.getCombinationsForViaPoints(reportsSearchCriteria.getViaPoints());
			sb.append(" AND c.SEGMENT_CODE IN " + combination);
		} else if ((!"".equals(reportsSearchCriteria.getFrom()) && reportsSearchCriteria.getFrom() != null)
				&& (!"".equals(reportsSearchCriteria.getTo()) && reportsSearchCriteria.getTo() != null)) {
			sb.append(" AND c.SEGMENT_CODE = '" + reportsSearchCriteria.getFrom() + "/" + reportsSearchCriteria.getTo() + "'");
		} else if ((!"".equals(reportsSearchCriteria.getFrom()) && reportsSearchCriteria.getFrom() != null)) {
			sb.append(" AND c.SEGMENT_CODE like '" + reportsSearchCriteria.getFrom() + "/%'");
		} else if ((!"".equals(reportsSearchCriteria.getTo()) && reportsSearchCriteria.getTo() != null)) {
			sb.append(" AND c.SEGMENT_CODE = '%/" + reportsSearchCriteria.getTo() + "'");
		}

		if (reportsSearchCriteria.getName() != null && !"".equals(reportsSearchCriteria.getName())) {
			sb.append(" AND lower(g.FIRST_NAME || ' ' || g.LAST_NAME) like '%" + reportsSearchCriteria.getName().toLowerCase()
					+ "%' ");
		}

		if (reportsSearchCriteria.getMobileNumber() != null && !"".equals(reportsSearchCriteria.getMobileNumber())) {
			sb.append(" AND l.MOBILE_PHONE_NUMBER = '" + reportsSearchCriteria.getMobileNumber() + "' ");
		}

		if (reportsSearchCriteria.getFfid() != null && !"".equals(reportsSearchCriteria.getFfid())) {
			sb.append(" AND l.EMAIL_ID = '" + reportsSearchCriteria.getFfid() + "' ");
		}

		if (reportsSearchCriteria.getPnr() != null && !"".equals(reportsSearchCriteria.getPnr())) {
			sb.append(" AND a.pnr = '" + reportsSearchCriteria.getPnr() + "' ");
		}

		DateFormat outDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null;
		Date endDate = null;

		final String[] formats = { "dd-MMM-yyyy HH:mm:ss", "dd-MM-yyyy HH:mm:ss", "dd/MM/yyyy", "dd-MM-yyyy" };

		for (String parseFormat : formats) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(parseFormat);
			try {
				startDate = simpleDateFormat.parse(reportsSearchCriteria.getDateRangeFrom());
				endDate = simpleDateFormat.parse(reportsSearchCriteria.getDateRangeTo());
				reportsSearchCriteria.setDateRangeFrom(outDateFormat.format(startDate) + " 00:00:00");
				reportsSearchCriteria.setDateRangeTo(outDateFormat.format(endDate) + " 23:59:59");
			} catch (ParseException e) {
				log.error(e + " cannot parse with " + parseFormat);
			}
		}

		sb.append(" AND c.EST_TIME_DEPARTURE_ZULU between to_date('" + reportsSearchCriteria.getDateRangeFrom()
				+ "' ,'DD/MM/YYYY HH24:MI:SS') AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
				+ "' ,'DD/MM/YYYY HH24:MI:SS') ");

		sb.append(" GROUP BY c.EST_TIME_DEPARTURE_LOCAL, i.FLIGHT_NUMBER,  c.SEGMENT_CODE,");
		sb.append(" a.pnr,  g.TITLE  || ' '  || g.FIRST_NAME  || ' ' || g.LAST_NAME , l.FIRST_NAME || ' '  || l.LAST_NAME ,");
		sb.append(" l.email_id,  l.mobile_phone_number, REPLACE(PT.REMARKS,'LMS Reward IDs' ,'' ), (");
		sb.append(" CASE WHEN o.loyalty_product_code IS NOT NULL THEN o.loyalty_product_code");
		sb.append(" WHEN m.charge_group_code='FAR' THEN 'FLIGHT' WHEN m.charge_group_code   IN ('SUR', 'INF')");
		sb.append(" AND o.loyalty_product_code IS NULL  THEN 'FLIGHT'");
		sb.append(" END) ) A");

		if (reportsSearchCriteria.getProductCode() != null && !"".equals(reportsSearchCriteria.getProductCode())) {
			sb.append(" WHERE loyalty_product_code = '" + reportsSearchCriteria.getProductCode() + "'");
		} else {
			sb.append(
					" WHERE loyalty_product_code IN('SEAT','FLIGHT','BAGGAGE','INSURANCE', 'MEAL', 'FLEXI', 'AIRPORT_SERVICE','BUNDLE_FARE')");
		}

		sb.append(" GROUP BY EST_TIME_DEPARTURE_LOCAL, FLIGHT_NUMBER, segment_code, pnr, pax_name, member_name,");
		sb.append(
				" email_id, mobile_phone_number, reward_ids , loyalty_product_code ORDER BY pnr , EST_TIME_DEPARTURE_LOCAL, pax_name");

		return sb.toString();
	}

	public String getNameChangeChargeReportQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();
		Collection<String> agentCol = reportsSearchCriteria.getAgents();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();
		Collection<String> channels = reportsSearchCriteria.getSalesChannels();

		String reportType = reportsSearchCriteria.getReportType();
		String projStr = "";
		String orderBy = null;
		String groupBy = "";
		String joinTables = "";
		String whereClause = "";
		if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_AGENT.equalsIgnoreCase(reportType)) {
			projStr = "CASE WHEN NCA.AGENT_CODE IS NULL THEN 'Web' ELSE NCA.AGENT_CODE END AS AGENT_CODE";
			projStr += ",0 AS CHANNEL_CODE,0 AS CHANNEL,0 AS SEGMENT_CODE";
			groupBy = "NCA.AGENT_CODE";
			orderBy = "NCA.AGENT_CODE";
		} else if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_CHANNEL.equalsIgnoreCase(reportType)) {
			projStr = "NCA.SALES_CHANNEL_CODE AS CHANNEL_CODE,SC.DESCRIPTION AS CHANNEL,0 AS AGENT_CODE,0 AS SEGMENT_CODE";
			groupBy = "NCA.SALES_CHANNEL_CODE,SC.DESCRIPTION";
			orderBy = "SC.DESCRIPTION";
			joinTables = "T_SALES_CHANNEL SC,";
			whereClause = "AND SC.SALES_CHANNEL_CODE=NCA.SALES_CHANNEL_CODE ";
		} else if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_SEGMENT.equalsIgnoreCase(reportType)) {
			projStr = "a.SEGMENT_CODE,0 AS AGENT_CODE,0 AS CHANNEL_CODE,0 AS CHANNEL";
			groupBy = "a.SEGMENT_CODE";
		}

		sb.append("SELECT " + projStr
				+ ",COUNT(distinct NCA.NAME_CHANGE_CHARGE_AUDIT_ID) AS CHANGE_COUNT ,SUM(a.AMOUNT) AS AMOUNT ");
		sb.append("FROM T_NAME_CHANGE_CHARGE_AUDIT NCA ," + joinTables);
		sb.append("	(SELECT PPOC.AMOUNT, NCS.NAME_CHANGE_CHARGE_AUDIT_ID, FS.SEGMENT_CODE ");
		sb.append("		FROM T_PNR_PAX_OND_CHARGES PPOC,T_PNR_PAX_SEG_CHARGES PPSC, T_PNR_SEGMENT PS,");
		sb.append("			T_PNR_PAX_FARE_SEGMENT PPFS, T_NAME_CHANGE_CHARGE_AUDIT_SEG NCS, t_flight_segment fs ");
		sb.append("		WHERE PPOC.PFT_ID = PPSC.PFT_ID AND PPFS.PPFS_ID = PPSC.PPFS_ID ");
		sb.append("		AND PPFS.PNR_SEG_ID = PS.PNR_SEG_ID AND PS.PNR_SEG_ID = NCS.PNR_SEG_ID ");
		sb.append("		AND FS.FLT_SEG_ID        = PS.FLT_SEG_ID AND PPOC.CHARGE_RATE_ID ");
		sb.append("		IN (SELECT CHARGE_RATE_ID FROM T_CHARGE_RATE WHERE CHARGE_CODE='NCC' ) ) a ");
		sb.append("WHERE NCA.CREATED_DATE BETWEEN ");
		sb.append("TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom() + "','dd-mon-yyyy HH24:mi:ss') AND ");
		sb.append("TO_DATE ('" + reportsSearchCriteria.getDateRangeTo() + "','dd-mon-yyyy HH24:mi:ss') ");
		sb.append("AND A.NAME_CHANGE_CHARGE_AUDIT_ID = NCA.NAME_CHANGE_CHARGE_AUDIT_ID ");
		sb.append(whereClause);

		if (agentCol != null && !agentCol.isEmpty()) {
			sb.append("AND  ");
			sb.append(ReportUtils.getReplaceStringForIN("NCA.AGENT_CODE", agentCol, false) + "  ");
		}

		if (channels != null && !channels.isEmpty()) {
			sb.append("AND  ");
			sb.append(ReportUtils.getReplaceStringForIN("NCA.SALES_CHANNEL_CODE", channels, false) + "  ");
		}

		if (segmentCodes != null) {
			sb.append("AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" a.SEGMENT_CODE ", segmentCodes, false) + " ");
		}

		sb.append(" GROUP BY " + groupBy + " ");

		if (orderBy != null) {
			sb.append("ORDER BY " + orderBy + " ");
		}

		return sb.toString();
	}

	public String getNILTransactionsAgentsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		// String strAgents = getSingleDataValue(reportsSearchCriteria.getAgents());
		String singleDataValuePayments = getSingleDataValue(reportsSearchCriteria.getPaymentTypes());

		StringBuilder sb = new StringBuilder();

		String gmtFromOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtFromOffsetInMinutes() + "/1440 ";
		String gmtToOffsetInMinutesStr = " -" + reportsSearchCriteria.getGmtToOffsetInMinutes() + "/1440 ";

		if (reportsSearchCriteria.getReportType().equals(ReportsSearchCriteria.NIL_AGENT_TRANS_SUMMARY)) {
			sb.append("select s.agent_code,s.agent_name,s.account_code  from t_agent s where s.agent_code not in");
			sb.append("	( SELECT agent_code from(");
			if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewCompanyPaymentSummaryByCurrencyQuery(reportsSearchCriteria,
						singleDataValuePayments));
			} else {
				// Following query combines existing payments with LCC interline payments & DRY sales payments
				// FIXME fare breakdown is not available in the llc payments at the moment. return 0 for all the fields
				// which does not have values in lcc_t_pnr_transaction
				sb.append("SELECT distinct agent_code, agent_name,NVL(account_code,'none') as account_code , agent_credit_limit,avilable_credit,payment_currency_code, ");
				sb.append("		total_amount, total_amount_paycur,");
				sb.append("	 total_fare_amount,  total_fare_amount_paycur, ");
				sb.append(" total_tax_amount,  total_tax_amount_paycur,");
				sb.append("  total_sur_amount ,  total_sur_amount_paycur, ");
				sb.append(" total_modify,  total_refund, ");
				sb.append(" total_modify_local,  total_refund_local, ");
				sb.append(" (total_amount + total_refund) pmt_amt,");
				sb.append(" (total_amount_paycur + total_refund_local) pmt_amt_local");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , tranCount tranCount");
				} else {
					sb.append(" , 0 as tranCount");
				}
				sb.append(" FROM ");

				sb.append(" (");// begin sub select 1
				sb.append(" SELECT A.AGENT_CODE,");
				sb.append(" A.AGENT_NAME,");
				sb.append(" A.ACCOUNT_CODE,");
				sb.append(" A.CREDIT_LIMIT agent_credit_limit,");
				sb.append(" TAS.AVILABLE_CREDIT AVILABLE_CREDIT,");
				sb.append(" T.PAYMENT_CURRENCY_CODE,");
				sb.append(" (-1 * SUM(T.TOTAL_AMOUNT)) TOTAL_AMOUNT,");
				sb.append(" (-1 * SUM(T.TOTAL_AMOUNT_PAYCUR)) TOTAL_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(T.TOTAL_FARE_AMOUNT)) TOTAL_FARE_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT)) TOTAL_TAX_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT)) TOTAL_SUR_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_AMOUNT_PAYCUR,");
				sb.append(
						" (-1 * SUM(DECODE(T.NOMINAL_CODE, 36,TOTAL_MOD_AMOUNT, 30,TOTAL_MOD_AMOUNT, 32,TOTAL_MOD_AMOUNT, 6,TOTAL_MOD_AMOUNT, 28,TOTAL_MOD_AMOUNT,18,TOTAL_MOD_AMOUNT,17,TOTAL_MOD_AMOUNT,16,TOTAL_MOD_AMOUNT,19,TOTAL_MOD_AMOUNT,15,TOTAL_MOD_AMOUNT,0))) TOTAL_MODIFY                                                                      ,");
				sb.append(
						" SUM(DECODE(T.NOMINAL_CODE, 37,TOTAL_MOD_AMOUNT, 31,TOTAL_MOD_AMOUNT, 33,TOTAL_MOD_AMOUNT, 5,TOTAL_MOD_AMOUNT, 29,TOTAL_MOD_AMOUNT,26,TOTAL_MOD_AMOUNT,24,TOTAL_MOD_AMOUNT,23,TOTAL_MOD_AMOUNT,25,TOTAL_MOD_AMOUNT,22,TOTAL_MOD_AMOUNT,0)) TOTAL_REFUND                                                                             ,");
				sb.append(
						" (-1 * SUM(DECODE(T.NOMINAL_CODE, 36,TOTAL_MOD_AMOUNT_PAYCUR, 30,TOTAL_MOD_AMOUNT_PAYCUR, 32,TOTAL_MOD_AMOUNT_PAYCUR, 6,TOTAL_MOD_AMOUNT_PAYCUR, 28,TOTAL_MOD_AMOUNT_PAYCUR,18,TOTAL_MOD_AMOUNT_PAYCUR,17,TOTAL_MOD_AMOUNT_PAYCUR,16,TOTAL_MOD_AMOUNT_PAYCUR,19,TOTAL_MOD_AMOUNT_PAYCUR,15,TOTAL_MOD_AMOUNT_PAYCUR,0))) TOTAL_MODIFY_LOCAL ,");
				sb.append(
						" SUM(DECODE(T.NOMINAL_CODE, 37,TOTAL_MOD_AMOUNT_PAYCUR, 31,TOTAL_MOD_AMOUNT_PAYCUR, 33,TOTAL_MOD_AMOUNT_PAYCUR, 5,TOTAL_MOD_AMOUNT_PAYCUR, 29,TOTAL_MOD_AMOUNT_PAYCUR,26,TOTAL_MOD_AMOUNT_PAYCUR,24,TOTAL_MOD_AMOUNT_PAYCUR,23,TOTAL_MOD_AMOUNT_PAYCUR,25,TOTAL_MOD_AMOUNT_PAYCUR,22,TOTAL_MOD_AMOUNT_PAYCUR,0)) TOTAL_REFUND_LOCAL        ");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , Count(TAT.txn_id) tranCount ");
				}
				sb.append("	FROM");
				sb.append(" (");// begin sub select 2

				sb.append(" SELECT b.agent_code agent_code, b.nominal_code AS nominal_code,");
				sb.append("   a.total_amount AS total_amount,");
				sb.append("   a.total_amount_paycur AS total_amount_paycur,");

				sb.append("a.total_fare_amount        as total_fare_amount,");
				sb.append("a.total_fare_amount_paycur as total_fare_amount_paycur,");
				sb.append("a.total_tax_amount         as total_tax_amount,");
				sb.append("a.total_tax_amount_paycur  as total_tax_amount_paycur,");
				sb.append("a.total_sur_amount         as total_sur_amount,");
				sb.append("a.total_sur_amount_paycur  as total_sur_amount_paycur,");

				sb.append("CASE WHEN (a.total_mod_amount = 0 OR a.total_mod_amount    > 0) AND (a.total_fare_amount > 0 ");
				sb.append(" OR a.total_tax_amount > 0 OR a.total_sur_amount > 0) ");
				sb.append(" THEN (a.total_fare_amount + a.total_tax_amount+ a.total_sur_amount+a.total_mod_amount) ");
				sb.append(" ELSE a.total_mod_amount END AS total_mod_amount,");
				sb.append("CASE WHEN (a.total_mod_amount_paycur = 0 OR a.total_mod_amount_paycur > 0) ");
				sb.append(
						" AND (a.total_fare_amount_paycur > 0 OR a.total_tax_amount_paycur > 0 OR a.total_sur_amount_paycur > 0) ");
				sb.append(
						" THEN (a.total_fare_amount_paycur + a.total_tax_amount_paycur+ a.total_sur_amount_paycur+a.total_mod_amount_paycur) ");
				sb.append(" ELSE a.total_mod_amount_paycur END AS total_mod_amount_paycur,");
				if (reportsSearchCriteria.isInbaseCurr()) {
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "'    AS payment_currency_code");
				} else {
					sb.append("   a.payment_currency_code AS payment_currency_code");
				}
				sb.append(" FROM t_pax_txn_breakdown_summary a, t_pax_transaction b");
				if (reportsSearchCriteria.getSearchFlightType() != null) {
					sb.append(", t_pnr_passenger pp");
				}
				if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
					sb.append(", t_reservation res");
				}
				sb.append(" WHERE a.pax_txn_id = b.txn_id");
				sb.append(" AND (b.payment_carrier_code IS NULL OR b.payment_carrier_code = '"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
				if (reportsSearchCriteria.getSearchFlightType() != null) {
					sb.append(" AND pp.pnr_pax_id = b.pnr_pax_id");
				}
				if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
					sb.append(" AND pp.pnr = res.pnr");
					sb.append(" AND res.fare_discount_code = '" + reportsSearchCriteria.getFareDiscountCode() + "'");
				}
				sb.append(" AND b.tnx_date BETWEEN TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
						+ "'  ||' 00:00:00','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" AND TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
						+ "'   ||' 23:59:59','dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents()) {
					sb.append("	AND"
							+ ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
				}
				if (AppSysParamsUtil.isModificationFilterEnabled()) {
					if (reportsSearchCriteria.isSales() && reportsSearchCriteria.isRefund()
							&& !reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND a.total_mod_amount >= 0");
					} else if (reportsSearchCriteria.isSales() && !reportsSearchCriteria.isRefund()
							&& !reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND a.total_mod_amount = 0");
					} else if (!reportsSearchCriteria.isSales() && reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND a.total_mod_amount <> 0");
					}
				}
				sb.append(" AND B.SALES_CHANNEL_CODE <> 11");
				if (reportsSearchCriteria.getSearchFlightType() != null) {
					// NOTE : if a reservation has at-least a international flight, total fare considered to be
					// international fare
					sb.append(" AND " + (reportsSearchCriteria.getSearchFlightType().equals("INT") ? "" : "NOT") + " EXISTS (");
					sb.append("       SELECT pp1.pnr");
					sb.append("         FROM t_pax_transaction b1,");
					sb.append("              t_pnr_segment ps,");
					sb.append("              t_pnr_passenger pp1,");
					sb.append("              t_flight_segment fs,");
					sb.append("              t_flight f");
					sb.append("        WHERE ps.pnr = pp1.pnr");
					sb.append("         AND pp.pnr = ps.pnr");
					sb.append("         AND pp1.pnr_pax_id = b1.pnr_pax_id");
					sb.append("         AND pp1.pnr_pax_id = b.pnr_pax_id");
					sb.append("         AND pp.pnr_pax_id = b1.pnr_pax_id");
					sb.append("         AND ps.flt_seg_id = fs.flt_seg_id");
					sb.append("         AND fs.flight_id = f.flight_id");
					sb.append("         AND f.flight_type = 'INT'");
					sb.append("         AND (   b1.payment_carrier_code IS NULL");
					sb.append("              OR b1.payment_carrier_code = '" + AppSysParamsUtil.getDefaultCarrierCode() + "')");
					if (!reportsSearchCriteria.isSelectedAllAgents()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					sb.append("         AND b1.sales_channel_code <> 11)");
				}

				// External SALES - UNION
				sb.append("	UNION ALL");
				sb.append(" SELECT P.AGENT_CODE     AS AGENT_CODE              ,");
				sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
				sb.append(" P.AMOUNT           		AS TOTAL_AMOUNT            ,");
				sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" P.AMOUNT           		AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(
						" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT, 31,P.AMOUNT, 33,P.AMOUNT, 5,P.AMOUNT, 29,P.AMOUNT,26,P.AMOUNT,24,P.AMOUNT,23,P.AMOUNT,25,P.AMOUNT,22,P.AMOUNT,0) AS TOTAL_MOD_AMOUNT,");
				sb.append(
						" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT_PAYCUR, 31,P.AMOUNT_PAYCUR, 33,P.AMOUNT_PAYCUR, 5,P.AMOUNT_PAYCUR, 29,P.AMOUNT_PAYCUR,26,P.AMOUNT_PAYCUR,24,P.AMOUNT_PAYCUR,23,P.AMOUNT_PAYCUR,25,P.AMOUNT_PAYCUR,22,P.AMOUNT_PAYCUR,0) AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				if (reportsSearchCriteria.isInbaseCurr()) {
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE");
				} else {
					sb.append(" P.PAYCUR_CODE AS PAYMENT_CURRENCY_CODE");
				}
				sb.append("	FROM T_PAX_EXT_CARRIER_TRANSACTIONS P");
				sb.append("		WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents()) {
					sb.append("		AND"
							+ ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
				}
				sb.append("		AND P.SALES_CHANNEL_CODE <> 11");

				// External SALES before 11/06/2011 Dry sell Phase 2 release- UNION
				sb.append("	UNION ALL");
				sb.append(" SELECT P.AGENT_CODE     AS AGENT_CODE              ,");
				sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
				sb.append(" -1 * P.AMOUNT           AS TOTAL_AMOUNT            ,");
				sb.append(" -1 * P.AMOUNT_PAYCUR    AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" -1 * P.AMOUNT           AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" -1 * P.AMOUNT_PAYCUR    AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				if (reportsSearchCriteria.isInbaseCurr()) {
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE");
				} else {
					sb.append(" P.PAYCUR_CODE AS PAYMENT_CURRENCY_CODE");
				}
				sb.append("	FROM T_PAX_EXT_CC_TRANSACTION_OLD P");
				sb.append("		WHERE P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents()) {
					sb.append("		AND"
							+ ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false) + " ");
				}
				sb.append("		AND P.SALES_CHANNEL_CODE <> 11");

				sb.append(" ) T,  T_AGENT A, T_AGENT_SUMMARY TAS");// end sub select 2
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" ,  T_AGENT_TRANSACTION TAT");
				}
				sb.append(" WHERE T.NOMINAL_CODE IN(" + singleDataValuePayments + ")");
				sb.append(" AND A.AGENT_CODE               = T.AGENT_CODE");
				sb.append(" AND A.IS_LCC_INTEGRATION_AGENT = 'N'");
				sb.append(" AND A.AGENT_CODE = TAS.AGENT_CODE ");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" AND A.AGENT_CODE               = TAT.AGENT_CODE(+) ");
					sb.append(" AND TAT.TNX_DATE(+) BETWEEN TO_DATE('" + reportsSearchCriteria.getToDate()
							+ "' || ' 00:00:00' , 'dd-mon-yyyy hh24:mi:ss')");
					sb.append("		AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
							+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
					sb.append(gmtToOffsetInMinutesStr);
				}
				sb.append(" GROUP BY A.AGENT_CODE,  A.AGENT_NAME ,  A.ACCOUNT_CODE, T.PAYMENT_CURRENCY_CODE, A.CREDIT_LIMIT, TAS.AVILABLE_CREDIT");
				if (reportsSearchCriteria.isDisplayAgentAdditionalInfo()) {
					sb.append(" , TAT.txn_id ");
				}
				sb.append(" )"); // end sub select 1

				sb.append(" order by agent_code, payment_currency_code ");
			}

		} else {
			if (reportsSearchCriteria.isReportViewNew()) {
				sb.append(RevenueReportsStatementCreator.getNewCompanyPaymentDetailsByCurrencyQuery(reportsSearchCriteria,
						singleDataValuePayments));
			}

			else {
				// Following query combines existing payments with LCC interline payments in paid currency & DRY sales
				// payments
				// FIXME fare breakdown is not available in the llc payments at the moment. return 0 for all the fields
				// which does not have values in lcc_t_pnr_transaction

				boolean showFarebasis = AppSysParamsUtil.isShowFarebasisCodesInCompanyPayments();

				sb.append("select agent_code,agent_name,account_code, decode(nominal_code,19,'On Account',25,'On Account',18,'Cash',26,'Cash',15,'Visa',22,'Visa',16,'Master',23,'Master',28,'Amex',29,'Amex',17,'Diners',24,'Diners',"
						+ "30,'Generic',31,'Generic',32,'CMI',33,'CMI',5,'CREDIT',6,'CREDIT' ,36,'BSP',37,'BSP') description,to_char(payment_date,'dd-MON-yyyy')payment_date,");
				sb.append("		pnr,passenger,routing,username,");
				sb.append("		 total_amount,  total_amount_paycur,");
				sb.append("		 total_fare_amount,  total_fare_amount_paycur, ");
				sb.append("		 total_tax_amount,  total_tax_amount_paycur,");
				sb.append("		 total_sur_amount,  total_sur_amount_paycur, ");
				sb.append("		 total_modify,  total_refund,");
				sb.append("		 total_modify_local,  total_refund_local, payment_currency_code, ");
				sb.append(" (total_amount + total_refund) pay_amt,");
				sb.append(" (total_amount_paycur + total_refund_local) pay_amt_local ,");
				sb.append(" eticket , ACTUAL_PAY as ACTUAL_PAY, PAY_REF as PAY_REF ");

				if (showFarebasis)
					sb.append(" , FARE_BASIS_CODES AS TOTAL_FARE_BASIS_CODES ");
				else
					sb.append(", NULL AS TOTAL_FARE_BASIS_CODES ");

				sb.append("FROM ");

				sb.append(" ( "); // begin select 1
				sb.append(" SELECT C.AGENT_CODE     ,");
				sb.append(" C.AGENT_NAME           ,");
				sb.append(" C.ACCOUNT_CODE         ,");
				sb.append(" V.NOMINAL_CODE         ,");
				sb.append(" V.TNX_DATE PAYMENT_DATE,");
				sb.append(" PNR PNR                ,");
				sb.append(" V.FIRST_NAME || ' ' || V.LAST_NAME PASSENGER,");
				sb.append(" U.DISPLAY_NAME USERNAME,");
				sb.append(" NVL(V.ETICKET_ID, ' ') AS ETICKET,");
				sb.append(" (-1 * SUM(TOTAL_AMOUNT)) TOTAL_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_AMOUNT_PAYCUR)) TOTAL_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_FARE_AMOUNT)) TOTAL_FARE_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_FARE_AMOUNT_PAYCUR)) TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT)) TOTAL_TAX_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_TAX_AMOUNT_PAYCUR)) TOTAL_TAX_AMOUNT_PAYCUR,");
				sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT)) TOTAL_SUR_AMOUNT,");
				sb.append(" (-1 * SUM(TOTAL_SUR_AMOUNT_PAYCUR)) TOTAL_SUR_AMOUNT_PAYCUR,");
				sb.append(
						" (-1 * SUM(DECODE(V.NOMINAL_CODE, 36, TOTAL_MOD_AMOUNT, 30, TOTAL_MOD_AMOUNT, 32, TOTAL_MOD_AMOUNT, 6, TOTAL_MOD_AMOUNT, 28, TOTAL_MOD_AMOUNT, 18, TOTAL_MOD_AMOUNT, 17, TOTAL_MOD_AMOUNT, 16, TOTAL_MOD_AMOUNT, 19, TOTAL_MOD_AMOUNT, 15, TOTAL_MOD_AMOUNT, 0))) TOTAL_MODIFY                                                                     ,");
				sb.append(
						" SUM(DECODE(V.NOMINAL_CODE, 37, TOTAL_MOD_AMOUNT, 31, TOTAL_MOD_AMOUNT, 33, TOTAL_MOD_AMOUNT, 5, TOTAL_MOD_AMOUNT, 29, TOTAL_MOD_AMOUNT, 26, TOTAL_MOD_AMOUNT, 24, TOTAL_MOD_AMOUNT, 23, TOTAL_MOD_AMOUNT, 25, TOTAL_MOD_AMOUNT, 22, TOTAL_MOD_AMOUNT, 0)) TOTAL_REFUND                                                                            ,");
				sb.append(
						" (-1 * SUM(DECODE(V.NOMINAL_CODE, 36, TOTAL_MOD_AMOUNT_PAYCUR, 30, TOTAL_MOD_AMOUNT_PAYCUR, 32, TOTAL_MOD_AMOUNT_PAYCUR, 6, TOTAL_MOD_AMOUNT_PAYCUR, 28, TOTAL_MOD_AMOUNT_PAYCUR, 18, TOTAL_MOD_AMOUNT_PAYCUR, 17, TOTAL_MOD_AMOUNT_PAYCUR, 16, TOTAL_MOD_AMOUNT_PAYCUR, 19, TOTAL_MOD_AMOUNT_PAYCUR, 15, TOTAL_MOD_AMOUNT_PAYCUR, 0))) TOTAL_MODIFY_LOCAL,");
				sb.append(
						" SUM(DECODE(V.NOMINAL_CODE, 37, TOTAL_MOD_AMOUNT_PAYCUR, 31, TOTAL_MOD_AMOUNT_PAYCUR, 33, TOTAL_MOD_AMOUNT_PAYCUR, 5, TOTAL_MOD_AMOUNT_PAYCUR, 29, TOTAL_MOD_AMOUNT_PAYCUR, 26, TOTAL_MOD_AMOUNT_PAYCUR, 24, TOTAL_MOD_AMOUNT_PAYCUR, 23, TOTAL_MOD_AMOUNT_PAYCUR, 25, TOTAL_MOD_AMOUNT_PAYCUR, 22, TOTAL_MOD_AMOUNT_PAYCUR, 0)) TOTAL_REFUND_LOCAL       ,");
				sb.append(" PAYMENT_CURRENCY_CODE, ");
				sb.append(" V.ROUTING ");
				sb.append("	,v.ACTUAL_PAY as ACTUAL_PAY, v.PAY_REF as PAY_REF");

				if (showFarebasis)
					sb.append(" ,V.FARE_BASIS_CODES  AS FARE_BASIS_CODES");

				sb.append(" FROM");
				sb.append(" (");// begin select 2
				sb.append(" SELECT ");
				sb.append(" P.PNR                      AS PNR                     ,");
				sb.append(" B.AGENT_CODE               AS AGENT_CODE              ,");
				sb.append(" B.NOMINAL_CODE             AS NOMINAL_CODE            ,");
				sb.append(" B.USER_ID                  AS USER_ID                 ,");
				sb.append(" P.PNR_PAX_ID               AS PNR_PAX_ID              ,");
				sb.append(" P.FIRST_NAME               AS FIRST_NAME              ,");
				sb.append(" P.LAST_NAME                AS LAST_NAME               ,");
				sb.append(" A.TIMESTAMP                AS TNX_DATE                ,");
				sb.append(" A.TOTAL_AMOUNT             AS TOTAL_AMOUNT            ,");
				sb.append(" A.TOTAL_AMOUNT_PAYCUR      AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" A.TOTAL_FARE_AMOUNT        AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" A.TOTAL_FARE_AMOUNT_PAYCUR AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" A.TOTAL_TAX_AMOUNT         AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" A.TOTAL_TAX_AMOUNT_PAYCUR  AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" A.TOTAL_SUR_AMOUNT         AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" A.TOTAL_SUR_AMOUNT_PAYCUR  AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append("CASE WHEN (A.TOTAL_MOD_AMOUNT = 0 OR A.TOTAL_MOD_AMOUNT    > 0) AND (A.TOTAL_FARE_AMOUNT > 0 ");
				sb.append(" OR A.TOTAL_TAX_AMOUNT    > 0 OR A.TOTAL_SUR_AMOUNT    > 0) ");
				sb.append(" THEN (A.TOTAL_FARE_AMOUNT + A.TOTAL_TAX_AMOUNT+ A.TOTAL_SUR_AMOUNT+A.TOTAL_MOD_AMOUNT) ");
				sb.append(" ELSE A.TOTAL_MOD_AMOUNT END AS TOTAL_MOD_AMOUNT,");
				sb.append(
						"CASE WHEN (A.TOTAL_MOD_AMOUNT_PAYCUR = 0 OR A.TOTAL_MOD_AMOUNT_PAYCUR    > 0) AND (A.TOTAL_FARE_AMOUNT_PAYCUR > 0 ");
				sb.append("	OR A.TOTAL_TAX_AMOUNT_PAYCUR           > 0 OR A.TOTAL_SUR_AMOUNT_PAYCUR           > 0) ");
				sb.append(
						"	THEN (A.TOTAL_FARE_AMOUNT_PAYCUR + A.TOTAL_TAX_AMOUNT_PAYCUR+ A.TOTAL_SUR_AMOUNT_PAYCUR+A.TOTAL_MOD_AMOUNT_PAYCUR) ");
				sb.append(" ELSE A.TOTAL_MOD_AMOUNT_PAYCUR END AS TOTAL_MOD_AMOUNT_PAYCUR,");

				if (reportsSearchCriteria.isInbaseCurr()) {
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE, ");
				} else {
					sb.append(" A.PAYMENT_CURRENCY_CODE    AS PAYMENT_CURRENCY_CODE, ");
				}
				// sb.append(" F_GET_ETICKET_STRING(P.PNR_PAX_ID) AS ETICKET_ID ,");

				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = P.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append(" F_COMPANY_PYMT_REPORT(P.PNR) AS ROUTING                ");
				sb.append("            	 	,actual_p.payment_mode as ACTUAL_PAY");
				sb.append("					,B.EXT_REFERENCE as PAY_REF");

				if (showFarebasis) {
					sb.append("	, f_concatenate_list( CURSOR (SELECT G.fare_basis_code");
					sb.append(" from t_pnr_pax_fare E, t_ond_fare F, t_fare_rule G, t_pnr_segment pnrs, t_pnr_pax_fare_segment ppfs");
					sb.append(" where E.PNR_PAX_ID = P.PNR_PAX_ID");
					sb.append(" AND E.FARE_ID = F.FARE_ID");
					sb.append(" AND F.FARE_RULE_ID = G.FARE_RULE_ID");
					sb.append(" AND e.ppf_id       = ppfs.ppf_id");
					sb.append(" AND ppfs.pnr_seg_id = pnrs.pnr_seg_id");
					sb.append(" order by pnrs.segment_seq");
					sb.append(" ), ',') AS FARE_BASIS_CODES");
				}

				sb.append(" FROM T_PAX_TXN_BREAKDOWN_SUMMARY A,  T_PAX_TRANSACTION B , T_PNR_PASSENGER P ");
				sb.append(" ,T_ACTUAL_PAYMENT_METHOD actual_p");

				if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
					sb.append(", T_RESERVATION RES");
				}
				sb.append(" WHERE A.PAX_TXN_ID = B.TXN_ID");
				sb.append(" AND (B.PAYMENT_CARRIER_CODE IS NULL OR B.PAYMENT_CARRIER_CODE='"
						+ AppSysParamsUtil.getDefaultCarrierCode() + "')");
				sb.append(" AND B.PNR_PAX_ID     = P.PNR_PAX_ID");
				if (addFareDiscountCodeCheck(reportsSearchCriteria)) {
					sb.append(" AND P.PNR = RES.PNR");
					sb.append(" AND RES.fare_discount_code = '" + reportsSearchCriteria.getFareDiscountCode() + "'");
				}
				sb.append(" AND B.TNX_DATE BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents()) {
					sb.append("	AND" + ReportUtils.getReplaceStringForIN("B.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
							+ " ");
				}
				sb.append(" AND actual_p.payment_mode_id (+)= B.payment_mode_id ");
				if (AppSysParamsUtil.isModificationFilterEnabled()) {
					if (reportsSearchCriteria.isSales() && reportsSearchCriteria.isRefund()
							&& !reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND A.total_mod_amount >= 0");
					} else if (reportsSearchCriteria.isSales() && !reportsSearchCriteria.isRefund()
							&& !reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND A.total_mod_amount = 0");
					} else if (!reportsSearchCriteria.isSales() && reportsSearchCriteria.isModificationDetails()) {
						sb.append("	AND A.total_mod_amount <> 0");
					}
				}
				sb.append(" AND B.SALES_CHANNEL_CODE <> 11");

				if (reportsSearchCriteria.getSearchFlightType() != null) {
					// NOTE : if a reservation has at-least a international flight, total fare considered to be
					// international fare
					sb.append(" AND " + (reportsSearchCriteria.getSearchFlightType().equals("INT") ? "" : "NOT") + " EXISTS (");
					sb.append(" SELECT pp1.pnr");
					sb.append("  FROM t_pax_transaction b1,");
					sb.append(" t_pnr_segment ps          ,");
					sb.append(" t_pnr_passenger pp1       ,");
					sb.append(" t_flight_segment fs       ,");
					sb.append(" t_flight f");
					sb.append(" WHERE ps.pnr                 = pp1.pnr");
					sb.append(" AND p.pnr                    = ps.pnr");
					sb.append(" AND pp1.pnr_pax_id           = b1.pnr_pax_id");
					sb.append(" AND pp1.pnr_pax_id           = b.pnr_pax_id");
					sb.append(" AND p.pnr_pax_id             = b1.pnr_pax_id");
					sb.append(" AND ps.flt_seg_id            = fs.flt_seg_id");
					sb.append(" AND fs.flight_id             = f.flight_id");
					sb.append(" AND f.flight_type            = 'INT'");
					sb.append(" AND ( b1.payment_carrier_code IS NULL");
					sb.append("              OR b1.payment_carrier_code = '" + AppSysParamsUtil.getDefaultCarrierCode() + "')");
					if (!reportsSearchCriteria.isSelectedAllAgents()) {
						sb.append("		AND"
								+ ReportUtils.getReplaceStringForIN("b1.AGENT_CODE", reportsSearchCriteria.getAgents(), false));
					}
					sb.append("         AND b1.sales_channel_code <> 11)");
				}

				// External SALES - UNION
				sb.append(" UNION ALL");
				sb.append(" SELECT ");
				sb.append(" X.PNR                   AS PNR                     ,");
				sb.append(" P.AGENT_CODE      	    AS AGENT_CODE              ,");
				sb.append(" P.NOMINAL_CODE          AS NOMINAL_CODE            ,");
				sb.append(" P.USER_ID               AS USER_ID                 ,");
				sb.append(" X.PNR_PAX_ID            AS PNR_PAX_ID              ,");
				sb.append(" X.FIRST_NAME            AS FIRST_NAME              ,");
				sb.append(" X.LAST_NAME             AS LAST_NAME               ,");
				sb.append(" P.TXN_TIMESTAMP         AS TNX_DATE                ,");
				sb.append(" P.AMOUNT           		AS TOTAL_AMOUNT            ,");
				sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_AMOUNT_PAYCUR     ,");
				sb.append(" P.AMOUNT           		AS TOTAL_FARE_AMOUNT       ,");
				sb.append(" P.AMOUNT_PAYCUR    		AS TOTAL_FARE_AMOUNT_PAYCUR,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_TAX_AMOUNT_PAYCUR ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT        ,");
				sb.append(" 0                       AS TOTAL_SUR_AMOUNT_PAYCUR ,");
				sb.append(" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT, 31,P.AMOUNT, 33,P.AMOUNT, 5,P.AMOUNT, 29,P.AMOUNT,26,P.AMOUNT,24,P.AMOUNT,23,P.AMOUNT,25,P.AMOUNT,22,P.AMOUNT,0)                       AS TOTAL_MOD_AMOUNT        ,");
				sb.append(" DECODE(P.NOMINAL_CODE, 37,P.AMOUNT_PAYCUR, 31,P.AMOUNT_PAYCUR, 33,P.AMOUNT_PAYCUR, 5,P.AMOUNT_PAYCUR, 29,P.AMOUNT_PAYCUR,26,P.AMOUNT_PAYCUR,24,P.AMOUNT_PAYCUR,23,P.AMOUNT_PAYCUR,25,P.AMOUNT_PAYCUR,22,P.AMOUNT_PAYCUR,0)  AS TOTAL_MOD_AMOUNT_PAYCUR ,");
				if (reportsSearchCriteria.isInbaseCurr()) {
					// for base currency report
					sb.append(" '" + AppSysParamsUtil.getBaseCurrency() + "' AS PAYMENT_CURRENCY_CODE, ");
				} else {
					sb.append(" P.PAYCUR_CODE AS PAYMENT_CURRENCY_CODE, ");// for payment currency report.
				}
				// sb.append(" F_GET_ETICKET_STRING(X.PNR_PAX_ID) AS ETICKET_ID ,");

				sb.append(" F_CONCATENATE_LIST ( CURSOR(SELECT pet.e_ticket_number FROM t_pnr_passenger pp,t_pax_e_ticket pet "
						+ "WHERE pp.pnr_pax_id = pet.pnr_pax_id AND pp.pnr_pax_id   = X.PNR_PAX_ID),'-') AS ETICKET_ID,");

				sb.append(" F_COMPANY_PYMT_REPORT(X.PNR, 'Y') AS ROUTING      ,");
				sb.append(" '' as ACTUAL_PAY ,");
				sb.append(" '' as PAY_REF");

				if (showFarebasis) {
					sb.append(" , '' AS FARE_BASIS_CODES");
				}

				sb.append(" FROM T_PAX_EXT_CARRIER_TRANSACTIONS P, T_PNR_PASSENGER X ");
				sb.append(" WHERE P.PNR_PAX_ID = X.PNR_PAX_ID");
				sb.append(" AND P.TXN_TIMESTAMP BETWEEN TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "' || ' 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtFromOffsetInMinutesStr);
				sb.append(" AND TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "' || ' 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
				sb.append(gmtToOffsetInMinutesStr);
				if (!reportsSearchCriteria.isSelectedAllAgents()) {
					sb.append("	AND" + ReportUtils.getReplaceStringForIN("P.AGENT_CODE", reportsSearchCriteria.getAgents(), false)
							+ " ");
				}
				sb.append(" AND P.SALES_CHANNEL_CODE <> 11");
				sb.append(" ) V , T_AGENT C, T_PAX_TRNX_NOMINAL_CODE N, T_USER U");// end select 2

				sb.append(" WHERE V.NOMINAL_CODE IN(" + singleDataValuePayments + ")");
				sb.append(" AND V.PAYMENT_CURRENCY_CODE = '" + reportsSearchCriteria.getCurrencies() + "'");
				sb.append(" AND V.AGENT_CODE = C.AGENT_CODE");
				sb.append(" AND V.USER_ID        = U.USER_ID(+)");
				sb.append(" AND V.NOMINAL_CODE   = N.NOMINAL_CODE");
				sb.append(" AND C.IS_LCC_INTEGRATION_AGENT = 'N'");
				sb.append(
						" GROUP BY V.PNR_PAX_ID, V.NOMINAL_CODE, C.AGENT_CODE, C.AGENT_NAME, C.ACCOUNT_CODE, V.PAYMENT_CURRENCY_CODE, V.PNR, V.FIRST_NAME  || ' '  || V.LAST_NAME, U.DISPLAY_NAME, V.TNX_DATE, V.ETICKET_ID, V.ROUTING,V.PAY_REF,V.ACTUAL_PAY");

				if (showFarebasis) {
					sb.append(" ,  V.FARE_BASIS_CODES");
				}

				sb.append(" ) "); // end select 1

				sb.append(" order by  agent_code,description,payment_currency_code,payment_date,pnr ");
			}

		}
		sb.append("  ) )");
		if (!reportsSearchCriteria.isSelectedAllAgents()) {
			sb.append(" and" + ReportUtils.getReplaceStringForIN("s.agent_code", reportsSearchCriteria.getAgents(), false) + " ");
		}
		return sb.toString();
	}

	public String getPFSDetailedExportQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String commonDition = "";

		if (reportsSearchCriteria.getAirportCode() != null && !reportsSearchCriteria.getAirportCode().equals("")) {
			commonDition = commonDition + " AND p.from_airport='" + reportsSearchCriteria.getAirportCode() + "' ";
		}

		if (reportsSearchCriteria.getDateRangeFrom() != null) {
			commonDition = commonDition + " AND  p.date_of_download >=  TO_DATE ('" + reportsSearchCriteria.getDateRangeFrom()
					+ "','dd-mon-yyyy HH24:mi:ss') ";
		}

		if (reportsSearchCriteria.getDateRangeTo() != null) {
			commonDition = commonDition + " AND  p.date_of_download <=  TO_DATE ('" + reportsSearchCriteria.getDateRangeTo()
					+ "','dd-mon-yyyy HH24:mi:ss') ";
		}

		if (reportsSearchCriteria.getDepartureDateRangeFrom() != null) {
			commonDition = commonDition + " AND  pp.flight_date >=  TO_DATE ('"
					+ reportsSearchCriteria.getDepartureDateRangeFrom() + "','dd-mon-yyyy HH24:mi:ss') ";
		}

		if (reportsSearchCriteria.getDepartureDateRangeTo() != null) {
			commonDition = commonDition + " AND  pp.flight_date <=  TO_DATE ('" + reportsSearchCriteria.getDepartureDateRangeTo()
					+ "','dd-mon-yyyy HH24:mi:ss') ";
		}

		if (reportsSearchCriteria.getProcessType() != null && !reportsSearchCriteria.getProcessType().equals("")) {
			commonDition = commonDition + " AND p.processed_status='" + reportsSearchCriteria.getProcessType() + "' ";
		}
		if (reportsSearchCriteria.getFlightNumber() != null && !reportsSearchCriteria.getFlightNumber().equals("")) {
			commonDition = commonDition + " AND p.flight_number='" + reportsSearchCriteria.getFlightNumber() + "' ";
		}

		String sql = " 	SELECT adult.flight_date, adult.pax_type, adult.pax_title, adult.first_name, "
				+ "	adult.last_name, adult.flight_no, adult.pax_status, adult.pnr, "
				+ "	adult.etkt_no, adult.destination_airport, adult.proc_status, adult.error_description, "
				+ "	adult.download_date, adult.from_airport , adult.from_address, adult.flight_proc_status, "
				+ "	infant.infant_detail " + "	FROM " + "	(SELECT pp.pp_id                                AS parent_ppId, "
				+ "	TO_CHAR(pp.flight_date,'YYYY-MM-DD HH24:MI:SS') AS flight_date, "
				+ "	    pp.pax_type_code                                AS pax_type, "
				+ "	    pp.title                                        AS pax_title, "
				+ "	    pp.first_name                                   AS first_name, "
				+ "	    pp.last_name                                    AS last_name, "
				+ "	    pp.flight_number                                AS flight_no, " + "	    ( " + "	    CASE pp.pax_status "
				+ "	      WHEN 'G'  THEN 'GOSHO' " + "	      WHEN 'N'  THEN 'NOSHO' " + "	      WHEN 'O'  THEN 'OFFLD' "
				+ "	      WHEN 'R'  THEN 'NOREC' " + "	      ELSE 'Unknown' END)                          AS pax_status, "
				+ "	    pp.pnr                                         AS pnr, "
				+ "	    pp.eticket_no                                  AS etkt_no, "
				+ "	    pp.destination_station                         AS destination_airport, " + "	    ( "
				+ "	    CASE pp.proc_status " + "	      WHEN NULL      THEN NULL "
				+ "	      WHEN 'P'       THEN 'Processed' " + "	      WHEN 'E'       THEN 'Error Occured' "
				+ "	      WHEN 'N'       THEN 'Not Processed' "
				+ "	      ELSE NULL    END)                                 AS proc_status, "
				+ "	    pp.error_description                                AS error_description, "
				+ "	    TO_CHAR(p.date_of_download,'YYYY-MM-DD HH24:MI:SS') AS download_date, "
				+ "	    p.from_airport                                      AS from_airport , "
				+ "	    p.from_address                                      AS from_address, " + "	    ( "
				+ "	    CASE p.processed_status " + "	      WHEN NULL     THEN NULL " + "	      WHEN 'P'      THEN 'Parsed' "
				+ "	      WHEN 'E'      THEN 'Processed with errors' " + "	      WHEN 'R'      THEN 'Processed' "
				+ "	      WHEN 'U'      THEN 'Unparsed' " + "	      WHEN 'W'      THEN 'Waiting' "
				+ "	      ELSE NULL    END)                                 AS flight_proc_status " + "	  FROM T_PFS_PARSED pp, "
				+ "	       T_PFS p " + "	  WHERE p.pfs_id          = pp.pfs_id "
				+ "	  AND (pp.pax_type_code != 'IN' OR pp.parent_ppid is null) ";

		sql = sql + commonDition + " ) adult " + "	LEFT OUTER JOIN "
				+ "	  (SELECT CONCAT(CONCAT(CONCAT(CONCAT(pp.title , ' ') , pp.first_name ), ' ') , pp.last_name) AS infant_detail, "
				+ "	     pp.parent_ppid                                                                           AS parent_ppid "
				+ "	  FROM T_PFS_PARSED pp, " + "	  T_PFS p " + "	  WHERE  p.pfs_id = pp.pfs_id  "
				+ "	AND pp.parent_ppid IS NOT NULL  ";

		sql = sql + commonDition + " ) infant " + "	ON adult.parent_ppId = infant.parent_ppid ";

		return sql;
	}

	/**
	 * 
	 * @param reportsSearchCriteria
	 * @return
	 */
	public String getPassengerStatusAndRevenueReport(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		if (reportsSearchCriteria != null) {

			String strtDepTime = "";

			if (reportsSearchCriteria.isShowInLocalTime()) {
				strtDepTime = "c.est_time_departure_local";
			} else {
				strtDepTime = "c.est_time_departure_zulu";
			}

			sb.append("SELECT TO_CHAR(" + strtDepTime + ",'DD/MM/YYYY HH24:MI') As Departure_Date, ");
			sb.append("f.flight_number, ");
			sb.append("f.origin, ");
			sb.append("f.destination, ");
			sb.append("c.segment_code, ");
			sb.append("a.pnr, ");
			sb.append("b.status, ");
			sb.append("TO_CHAR(a.booking_timestamp,'DD/MM/YYYY HH24:MI') AS Booking_Date, ");
			sb.append("g.first_name || ' ' || g.last_name as Pax_name, ");
			sb.append("g.pax_type_code, ");
			sb.append("n.description nationality, ");
			sb.append("ppai.passport_number, ");
			sb.append("DECODE(ppfs.pax_status,'F','FLOWN','N','NOSHOW','G','GOSHOW','C','CONFIRM','R','NOREC') PFS_Status, ");
			sb.append("DECODE(bc.pax_type,'GOSHOW',bc.pax_type,bc.bc_type) Booking_Type, ");
			sb.append(
					"(SELECT WM_CONCAT(ra.user_notes) FROM t_reservation_audit ra where ra.pnr =a.pnr and ra.user_notes is not null)user_notes, ");
			sb.append("a.owner_agent_code, ");
			sb.append("a.origin_agent_code, ");
			sb.append("ppfs.booking_code, ");
			sb.append("fr.fare_basis_code, ");
			sb.append("fr.fare_rule_code, ");
			sb.append("f.flight_type, ");
			sb.append("sc.description sales_channel, ");
			sb.append("sum(ppf.total_fare) total_fare, ");
			sb.append("sum(ppf.total_surcharges+ppf.total_cnx+ppf.total_mod+ppf.total_adj) total_surcharge, ");
			sb.append("sum(ppf.total_tax) total_tax ");
			sb.append("FROM t_reservation a, ");
			sb.append("t_pnr_segment b, ");
			sb.append("t_flight_segment c, ");
			sb.append("t_pnr_passenger g , ");
			sb.append("t_flight f , ");
			sb.append("T_PNR_PAX_FARE_SEGMENT PPFS, ");
			sb.append("T_PNR_PAX_FARE PPF, ");
			sb.append("t_ond_fare ondf, ");
			sb.append("T_fare_rule fr, ");
			sb.append("t_sales_channel sc, ");
			sb.append("t_nationality n, ");
			sb.append("t_pnr_pax_additional_info ppai, ");
			sb.append("t_booking_class bc ");
			sb.append("WHERE a.pnr = b.pnr ");
			sb.append("AND a.pnr = g.pnr ");
			sb.append("AND b.flt_seg_id = c.flt_seg_id ");
			sb.append("AND c.flight_id  = f.flight_id ");
			sb.append("AND G.pnr_pax_id =PPF.pnr_pax_id ");
			sb.append("AND PPF.ppf_id = PPFS.ppf_id ");
			sb.append("AND PPFS.pnr_seg_id =B.pnr_seg_id ");
			sb.append("AND ondf.fare_id = PPF.fare_id ");
			sb.append("AND ondf.fare_rule_id = fr.fare_rule_id ");
			sb.append("AND a.origin_channel_code =sc.sales_channel_code ");
			sb.append("AND g.nationality_code =n.nationality_code(+) ");
			sb.append("AND g.pnr_pax_id =ppai.pnr_pax_id ");
			sb.append("AND  ppfs.booking_code =bc.booking_code(+) ");

			if (reportsSearchCriteria.getDateRangeFrom() != null && reportsSearchCriteria.getDateRangeTo() != null) {
				sb.append("			AND " + strtDepTime + " between (TO_DATE('" + reportsSearchCriteria.getDateRangeFrom()
						+ "  00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
				sb.append("			AND (TO_DATE('" + reportsSearchCriteria.getDateRangeTo()
						+ "  23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");
			}

			if (reportsSearchCriteria.getBookingDateFrom() != null && reportsSearchCriteria.getBookingDateTo() != null) {
				sb.append("			AND a.booking_timestamp between (TO_DATE('" + reportsSearchCriteria.getBookingDateFrom()
						+ "  00:00:00', 'DD-MON-YYYY HH24:MI:SS')) ");
				sb.append("			AND (TO_DATE('" + reportsSearchCriteria.getBookingDateTo()
						+ "  23:59:59', 'DD-MON-YYYY HH24:MI:SS'))  ");
			}

			if (reportsSearchCriteria.getFlightNoCollection() != null
					&& !reportsSearchCriteria.getFlightNoCollection().isEmpty()) {
				sb.append(" AND  ");
				sb.append(Util.getReplaceStringForIN(" F.FLIGHT_NUMBER", reportsSearchCriteria.getFlightNoCollection()) + " ");
			}

			if (reportsSearchCriteria.getPfsStatus() != null) {
				sb.append(" AND ppfs.pax_status = '" + reportsSearchCriteria.getPfsStatus() + "' ");
			}

			if (reportsSearchCriteria.getBookingClass() != null) {
				sb.append(" AND ppfs.booking_code = '" + reportsSearchCriteria.getBookingClass() + "' ");
			}

			if (reportsSearchCriteria.getFareBasisCode() != null) {
				sb.append(" AND fr.fare_basis_code = '" + reportsSearchCriteria.getFareBasisCode() + "' ");
			}

			if (reportsSearchCriteria.getFareRuleCode() != null) {
				sb.append(" AND fr.fare_rule_code = '" + reportsSearchCriteria.getFareRuleCode() + "' ");
			}

			if (reportsSearchCriteria.getOriginAirport() != null) {
				sb.append(" AND f.origin = '" + reportsSearchCriteria.getOriginAirport() + "' ");
			}

			if (reportsSearchCriteria.getDestinationAirport() != null) {
				sb.append(" AND f.destination = '" + reportsSearchCriteria.getDestinationAirport() + "' ");
			}

			if (reportsSearchCriteria.getAgents() != null && !reportsSearchCriteria.getAgents().isEmpty()) {
				sb.append(" AND  ");
				sb.append(Util.getReplaceStringForIN(" a.owner_agent_code", reportsSearchCriteria.getAgents()) + " ");
			}

			// sb.append(" AND A.status = '" + ReservationInternalConstants.ReservationStatus.CONFIRMED + "' ");
			// sb.append(" AND B.status = '" + ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED + "' ");
			sb.append(" AND  b.sub_status is null");
			sb.append(" GROUP BY  " + strtDepTime + ", ");
			sb.append(" f.flight_number, ");
			sb.append(" f.origin, ");
			sb.append(" f.destination, ");
			sb.append(" c.segment_code, ");
			sb.append(" a.pnr, ");
			sb.append(" b.status, ");
			sb.append(" a.booking_timestamp, ");
			sb.append(" g.first_name || ' ' || g.last_name, ");
			sb.append(" g.pax_type_code, ");
			sb.append(" n.description, ");
			sb.append(" DECODE(ppfs.pax_status,'F','FLOWN','N','NOSHOW','G','GOSHOW','C','CONFIRM','R','NOREC'), ");
			sb.append(" DECODE(bc.pax_type,'GOSHOW',bc.pax_type,bc.bc_type), ");
			sb.append(" a.owner_agent_code, ");
			sb.append(" a.origin_agent_code, ");
			sb.append(" ppfs.booking_code, ");
			sb.append(" fr.fare_basis_code, ");
			sb.append(" fr.fare_rule_code, ");
			sb.append(" f.flight_type, ");
			sb.append(" sc.description, ");
			sb.append(" ppai.passport_number ");

			sb.append(" ORDER BY " + strtDepTime + ",f.flight_number,a.booking_timestamp,a.pnr ");

		}
		return sb.toString();
	}

	public String getFlightMessageDetailsReport(String flightId, Integer scheduleId, String fromDate, String toDate)
			throws ModuleException {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT a.audit_id,b.description,");
		sb.append("TO_CHAR(a.timestamp,'dd/mm/yyyy HH24:mi:ss') AS TIMESTAMP,");
		sb.append("a.user_id,c.agent_code,a.content ");
		sb.append(" FROM t_audit a,t_task b,t_agent c, t_user d, t_task_group e ");
		sb.append(" WHERE a.task_code = b.task_code ");
		sb.append(" AND c.agent_code = d.agent_code ");
		sb.append(" AND a.user_id = d.user_id ");
		sb.append(" AND b.task_group_code = e.task_group_code ");
		if (!StringUtil.isNullOrEmpty(fromDate) && !StringUtil.isNullOrEmpty(toDate)) {
			sb.append(" AND a.timestamp BETWEEN TO_DATE('" + fromDate + "','dd/mm/yyyy HH24:mi:ss') AND TO_DATE('" + toDate
					+ "', 'dd/mm/yyyy HH24:mi:ss') ");
		}

		sb.append(" AND b.task_group_code IN ('admin.proc.ssmasm') ");
		sb.append(" AND a.task_code IN (" + TasksUtil.PROCESS_SSM + "," + TasksUtil.PROCESS_ASM + ") ");
		sb.append(" AND (lower(a.content) LIKE '%flightid%:=%" + flightId + "%' ");
		sb.append(" OR lower(a.content) LIKE '%flight id%:=%" + flightId + "%'");
		if (scheduleId != null) {
			sb.append(" OR lower(a.content) LIKE '%scheduleId%:=%" + scheduleId + "%' ");
			sb.append(" OR lower(a.content) LIKE '%schedule id%:=%" + scheduleId + "%' ");
		}
		sb.append(") ");
		sb.append(" ORDER BY a.audit_id");

		return sb.toString();

	}

	public String getScheduleMessageDetailsReport(String scheduleId, String fromDate, String toDate) throws ModuleException {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT a.audit_id,b.description,");
		sb.append("TO_CHAR(a.timestamp,'dd/mm/yyyy HH24:mi:ss') AS TIMESTAMP,");
		sb.append("a.user_id,c.agent_code,a.content ");
		sb.append(" FROM t_audit a,t_task b,t_agent c, t_user d, t_task_group e ");
		sb.append(" WHERE a.task_code = b.task_code ");
		sb.append(" AND c.agent_code = d.agent_code ");
		sb.append(" AND a.user_id = d.user_id ");
		sb.append(" AND b.task_group_code = e.task_group_code ");
		if (!StringUtil.isNullOrEmpty(fromDate) && !StringUtil.isNullOrEmpty(toDate)) {
			sb.append(" AND a.timestamp BETWEEN TO_DATE('" + fromDate + "','dd/mm/yyyy HH24:mi:ss') AND TO_DATE('" + toDate
					+ "', 'dd/mm/yyyy HH24:mi:ss') ");
		}
		sb.append(" AND b.task_group_code IN ('admin.proc.ssmasm') ");
		sb.append(" AND a.task_code IN (" + TasksUtil.PROCESS_SSM + ") ");
		sb.append(" AND (lower(a.content) LIKE '%scheduleId%:=%" + scheduleId + "%' ");
		sb.append(" OR lower(a.content) LIKE '%schedule id%:=%" + scheduleId + "%') ");
		sb.append(" ORDER BY a.audit_id");

		return sb.toString();

	}

	public String getPublishMessagesDetailsReport(String scheduleFlightId, String fromDate, String toDate)
			throws ModuleException {

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT PMT.GDS_ID GDS_ID,GD.GDS_CODE GDS_CODE,PMT.STATUS STATUS,PMT.MESSAGE_TYPE MESSAGE_TYPE, ");
		sb.append(
				" TO_CHAR(PMT.CREATED_DATE,'DD-MON-YYYY HH24:MI:SS') PUBLISHED_ON,UTL_RAW.CAST_TO_VARCHAR2 (OM.ROW_MESSAGE) MESSAGE ");
		sb.append("FROM T_GDS GD, T_GDS_PUBLISH_MSG_TRACKING PMT ");
		sb.append(
				"JOIN MB_T_OUT_MESSAGE OM ON UTL_RAW.CAST_TO_VARCHAR2(OM.ROW_MESSAGE) LIKE '%'||UPPER(PMT.REFERENCE_KEY)||'%' ");

		sb.append(" WHERE PMT.MESSAGE_TYPE_ID = " + scheduleFlightId);
		sb.append(" AND PMT.GDS_ID = GD.GDS_ID ");
		if (!StringUtil.isNullOrEmpty(fromDate) && !StringUtil.isNullOrEmpty(toDate)) {
			sb.append(" AND PMT.CREATED_DATE BETWEEN TO_DATE('" + fromDate + "','dd/mm/yyyy HH24:mi:ss') AND TO_DATE('" + toDate
					+ "', 'dd/mm/yyyy HH24:mi:ss') ");
		}
		sb.append(" ORDER BY PMT.CREATED_DATE ");
		return sb.toString();

	}

	public String getMCODetailReport(ReportsSearchCriteria searchCriteria) throws ModuleException {
		String fromDate = searchCriteria.getDateRangeFrom();
		String toDate = searchCriteria.getDateRangeTo();
		String flightNo = searchCriteria.getFlightNumber();
		String flightDate = searchCriteria.getFlightDateRangeFrom();
		Collection<String> agents = searchCriteria.getAgents();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT mco.MCO_NUMBER                  AS MCO_NO, ");
		sb.append(" pax.PNR                               AS PNR, ");
		sb.append(" TO_CHAR(mco.CREATED_DATE,'DD-MON-YY') AS ISSUED_DATE, ");
		sb.append(" service.SERVICE_NAME                  AS SERVICE_NAME, ");
		sb.append(" users.AGENT_CODE                      AS AGENT_CODE, ");
		sb.append(" mco.REMARKS                           AS REMARKS, ");
		sb.append(" mco.AMOUNT_IN_LOCAL                   AS AMOUNT, ");
		sb.append(" mco.CURRENCY_CODE                     AS CURRENCY, ");
		sb.append(" mco.FLIGHT_NO                         AS FLIGHT_NO, ");
		sb.append(" TO_CHAR(mco.FLIGHT_DATE,'DD-MON-YY')  AS FLIGHT_DATE, ");
		sb.append(" mco.STATUS                            AS STATUS, ");
		sb.append(" mco.OND_CODE                            AS OND ");
		sb.append(" FROM T_MCO mco, T_MCO_SERVICE service, T_PNR_PASSENGER pax, T_USER users ");
		sb.append(" WHERE ");
		sb.append(" mco.CREATED_DATE BETWEEN TO_DATE('" + fromDate + " 00:00','dd/mm/yyyy hh24:mi') AND TO_DATE('" + toDate
				+ " 23:59','dd/mm/yyyy hh24:mi') ");

		if (!StringUtil.isNullOrEmpty(flightNo) && !StringUtil.isNullOrEmpty(flightNo)) {
			sb.append(" AND mco.FLIGHT_NO      = '" + flightNo + "' ");
		}
		if (!StringUtil.isNullOrEmpty(flightDate) && !StringUtil.isNullOrEmpty(flightDate)) {
			sb.append(" AND mco.FLIGHT_DATE   BETWEEN TO_DATE('" + flightDate + " 00:00:00','dd/mm/yyyy hh24:mi:ss') " +
					"AND TO_DATE('" + flightDate + " 23:59:59','dd/mm/yyyy hh24:mi:ss') ");
		}
		if (agents != null && agents.size() > 0) {
			sb.append("  AND " + ReportUtils.getReplaceStringForIN("users.AGENT_CODE", agents, false) + "  ");
		}
		sb.append(" AND users.USER_ID   = (CASE WHEN mco.MODIFIED_BY IS NULL THEN mco.CREATED_BY ELSE mco.MODIFIED_BY END) ");
		sb.append(" AND mco.MCO_SERVICE_ID = service.MCO_SERVICE_ID ");
		sb.append(" AND mco.PNR_PAX_ID     = pax.PNR_PAX_ID ");
		sb.append(" ORDER BY mco.MCO_NUMBER ");
		return sb.toString();
	}

	public String getMCOSummaryReport(ReportsSearchCriteria searchCriteria) throws ModuleException {
		String fromDate = searchCriteria.getDateRangeFrom();
		String toDate = searchCriteria.getDateRangeTo();
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT * FROM ( ");
		sb.append(" SELECT TO_CHAR(mco.CREATED_DATE,'DD-MON-YY') AS ISSUED_DATE, ");
		sb.append(" agents.STATION_CODE, SUM(mco.AMOUNT) AS AMOUNT ");
		sb.append(" FROM T_MCO mco, T_USER users, ");
		if (StringUtils.isNotEmpty(searchCriteria.getAgentCode())) {
			sb.append("(SELECT * FROM T_AGENT WHERE AGENT_CODE IN ('" + searchCriteria.getAgentCode() + "')) agents ");
		} else {
			sb.append("T_AGENT agents ");
		}
		sb.append(" WHERE mco.CREATED_DATE BETWEEN TO_DATE('" + fromDate + " 00:00','dd/mm/yyyy hh24:mi') AND TO_DATE('" + toDate
				+ " 23:59','dd/mm/yyyy hh24:mi') ");
		sb.append(" AND mco.CREATED_BY = users.USER_ID ");
		sb.append(" AND users.AGENT_CODE = agents.AGENT_CODE ");
		sb.append(" AND mco.STATUS = '" + MCO.FLOWN + "' ");
		sb.append(" GROUP BY TO_CHAR(mco.CREATED_DATE,'DD-MON-YY'), agents.STATION_CODE");
		sb.append(" ) ORDER BY ISSUED_DATE");
		return sb.toString();
	}
	
	
	public String getForceConfirmedBookingReport(ReportsSearchCriteria searchCriteria) throws ModuleException {
		
		String fromDate = searchCriteria.getDateRangeFrom();
		String toDate = searchCriteria.getDateRangeTo();
		String segmentCode = "";
		String fromDepartureDate ="";
		String toDepartureDate ="";

		boolean filterFromDepartureDate = false;
		boolean filterFromSegment = false;
		boolean filterOnlyTotalToPay = searchCriteria.isOnlyTotalAmountToPay();
		boolean applyPayStatusFilter = searchCriteria.isIncludePaidStatusFilter();
		
		if (!StringUtil.isNullOrEmpty(searchCriteria.getDateRange2From())
				&& !StringUtil.isNullOrEmpty(searchCriteria.getDateRange2To())) {
			fromDepartureDate = searchCriteria.getDateRange2From();
			toDepartureDate = searchCriteria.getDateRange2To();
			filterFromDepartureDate = true;
		}

		if (!StringUtil.isNullOrEmpty(searchCriteria.getFrom())&& !StringUtil.isNullOrEmpty(searchCriteria.getTo())) {
			
			segmentCode = searchCriteria.getFrom() + "/"+ searchCriteria.getTo();
			filterFromSegment = true;
		}
		
		
		StringBuilder sb = new StringBuilder();
		sb.append("");
		sb.append("select r.PNR, r.booking_timestamp, r.ORIGIN_USER_ID, u.display_name,");
		sb.append(" (select listagg(pp.first_name||' '||last_name,chr(10)) within group (order by pp.pax_sequence)");
		sb.append(" from t_pnr_passenger pp where pp.pnr = r.pnr) pax_list,");
		sb.append("  (select user_notes from (");
		sb.append(" select ra.pnr,ra.user_notes,row_number() over(partition by ra.pnr order by ra.audit_id desc) rn");
		sb.append(" from T_RESERVATION_AUDIT ra");
		//sb.append(" WHERE ra.template_code = 'ADDUNOTE'");
		sb.append(" )ra where ra.pnr = r.pnr and ra.rn =1)");
		sb.append(" user_notes,");
		sb.append(" (select sum(pt.amount)");
		sb.append(" from t_pax_transaction pt join t_pnr_passenger pp on pt.pnr_pax_id = pp.pnr_pax_id");
		sb.append(" where pp.pnr = r.pnr) pt_amount,");
		sb.append(" (SELECT listagg(f.flight_number,chr(10)) within group (order by ps.OND_GROUP_ID)");
		sb.append(" FROM T_PNR_SEGMENT ps JOIN T_FLIGHT_SEGMENT fs ON ps.flt_seg_id   = fs.flt_seg_id");
		sb.append(" join t_flight f on fs.flight_id = f.flight_id");
		sb.append(" WHERE ps.pnr = r.pnr");
		sb.append(" and ps.status = 'CNF')");
		sb.append(" flight_numbers");
		sb.append(" from T_RESERVATION r join t_user u on r.origin_user_id = u.user_id");
		sb.append(" where r.status = 'CNF' and (BOOKING_TIMESTAMP BETWEEN TO_DATE('" + fromDate + " 00:00','dd/mm/yyyy hh24:mi') AND");
		sb.append(" TO_DATE('" + toDate + " 23:59','dd/mm/yyyy hh24:mi'))");
		sb.append(" AND exists (SELECT 1");
		sb.append(" from T_PAX_TRANSACTION pt join t_pnr_passenger pp on pt.pnr_pax_id = pp.pnr_pax_id");
		sb.append("  where pp.pnr = r.pnr group by pp.pnr_pax_id having sum(pt.amount) > 0 )");
		if(applyPayStatusFilter && filterOnlyTotalToPay){
			sb.append(" AND not exists (SELECT 1");
			sb.append(" from T_PAX_TRANSACTION pt  join t_pnr_passenger pp on pt.pnr_pax_id = pp.pnr_pax_id");
			sb.append("  where pp.pnr = r.pnr and pt.nominal_code in ("+Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes())+") )");
		}else {
			
			if(applyPayStatusFilter && !filterOnlyTotalToPay){
				sb.append(" AND exists (SELECT 1");
				sb.append(" from T_PAX_TRANSACTION pt  join t_pnr_passenger pp on pt.pnr_pax_id = pp.pnr_pax_id");
				sb.append("  where pp.pnr = r.pnr and pt.nominal_code in ("+Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes())+") )");
			}
		}
		sb.append(" AND exists (SELECT 1  FROM T_PNR_SEGMENT pnr_seg JOIN T_FLIGHT_SEGMENT flt_seg");
		sb.append(" ON pnr_seg.flt_seg_id = flt_seg.flt_seg_id WHERE pnr_seg.pnr  = r.pnr");
		sb.append(" AND pnr_seg.status = 'CNF'");
		  
		if(filterFromDepartureDate){    
			sb.append(" AND (flt_seg.est_time_departure_zulu BETWEEN TO_DATE('" + fromDepartureDate + " 00:00','dd/mm/yyyy hh24:mi')");
			sb.append(" AND TO_DATE ('" + toDepartureDate + " 23:59','dd/mm/yyyy hh24:mi'))");
			}
		
		if(filterFromSegment){
			sb.append(" and flt_seg.SEGMENT_CODE='"+segmentCode+"'");
		}
		sb.append(" ) ");
		
		
		return sb.toString();
	}
	
		public Map<String, String> getGstr1Report(ReportsSearchCriteria criteria) {

		/*
		 * TODO -- india hard-coded invoice hard-coded
		 */

		String fromDate = criteria.getDateRangeFrom();
		String toDate = criteria.getDateRangeTo();
		String stateCode = criteria.getStateCode();

		StringBuilder invoiceSubquery = new StringBuilder();
		invoiceSubquery
				.append(" select * from t_tax_invoice ti ")
				.append(" where ti.invoice_type = 'I' ")
				.append(" and ti.state = '" + stateCode + "' ")
				.append(" and ti.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate
						+ "', 'dd/mm/yyyy hh24:mi:ss')  ");

		StringBuilder debitNoteSubquery = new StringBuilder();
		debitNoteSubquery
				.append(" select * from t_tax_invoice ti ")
				.append(" where ti.invoice_type = 'D' ")
				.append(" and ti.state = '" + stateCode + "' ")
				.append(" and ti.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate
						+ "', 'dd/mm/yyyy hh24:mi:ss')  ");

		StringBuilder invoiceAndDebitNoteSubquery = new StringBuilder();
		invoiceAndDebitNoteSubquery
				.append(" select * from t_tax_invoice ti ")
				.append(" where (ti.invoice_type = 'I' or ti.invoice_type = 'D') ")
				.append(" and ti.state = '" + stateCode + "' ")
				.append(" and ti.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate
						+ "', 'dd/mm/yyyy hh24:mi:ss')  ");

		StringBuilder gstSubquery = new StringBuilder();
		gstSubquery
				.append(" with c as ( ")
				.append(" select r.charge_rate_id, r.charge_value_percentage, c.charge_code, c.country_code, c.state_id, c.inter_state, s.state_code, s.state_name ")
				.append(" from t_charge_rate r, t_charge c, t_state s ")
				.append(" where r.charge_code = c.charge_code and c.state_id = s.state_id (+) and c.country_code = 'IN' ")
				.append(" ) ");

		StringBuilder gstTaxView = new StringBuilder();
		gstTaxView
				.append(" select i.ti_id, c1.charge_value_percentage cgst_rate, i.tax1_amount cgst_amount, ")
				.append(" 	c2.charge_value_percentage sgst_rate, i.tax2_amount sgst_amount, ")
				.append(" 	c3.charge_value_percentage igst_rate, i.tax3_amount igst_amount, nvl(c2.state_code, c3.state_code) state_code, NVL(c2.state_name, c3.state_name) state_name ")
				.append(" from t_tax_invoice i, c c1, c c2, c c3 ")
				.append(" where i.tax1_rate_id = c1.charge_rate_id (+) and  i.tax2_rate_id = c2.charge_rate_id (+) and  i.tax3_rate_id = c3.charge_rate_id (+) ")
				.append("  	and i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate
						+ "', 'dd/mm/yyyy hh24:mi:ss')  ").append("  	and i.state = '" + stateCode + "'");

		StringBuilder sgstTaxView = new StringBuilder();
		sgstTaxView
				.append(" select i.ti_id, c1.charge_value_percentage cgst_rate, i.tax1_amount cgst_amount, ")
				.append(" 	c2.charge_value_percentage sgst_rate, i.tax2_amount sgst_amount, ")
				.append(" 	c2.state_code state_code ")
				.append(" from t_tax_invoice i, c c1, c c2 ")
				.append(" where i.tax1_rate_id = c1.charge_rate_id (+) and  i.tax2_rate_id = c2.charge_rate_id (+) ")
				.append("  	and i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate
						+ "', 'dd/mm/yyyy hh24:mi:ss')  ").append("  	and i.state = '" + stateCode + "'");

		StringBuilder igstTaxView = new StringBuilder();
		igstTaxView
				.append(" select i.ti_id, ")
				.append(" 	c3.charge_value_percentage igst_rate, i.tax3_amount igst_amount, c3.state_code ")
				.append(" from t_tax_invoice i, c c1, c c2, c c3 ")
				.append(" where i.tax1_rate_id = c1.charge_rate_id (+) and  i.tax2_rate_id = c2.charge_rate_id (+) and  i.tax3_rate_id = c3.charge_rate_id (+) ")
				.append("  	and i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate
						+ "', 'dd/mm/yyyy hh24:mi:ss')  ").append("  	and i.state = '" + stateCode + "'");

		StringBuilder section4A = new StringBuilder();
		section4A.append(gstSubquery).append(" select ti.tax_reg_no, ti.invoice_number, ti.date_of_issue, ")
				.append("  	ti.taxable_amount + ti.non_taxable_amount total_amount, ti.taxable_amount,  ")
				.append(" 	cgst_rate, cgst_amount,  sgst_rate, sgst_amount, igst_rate, igst_amount, state_name ")
				.append(" from t_tax_invoice ti, ").append(" ( ").append(gstTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and ti.tax_reg_no is not null and ti.invoice_type = 'I' ");

		StringBuilder section5A = new StringBuilder();
		section5A
				.append(gstSubquery)
				.append(" select rc.c_state recipient_state, (rc.c_title || ' ' || rc.c_first_name || ' ' || rc.c_last_name) recipient_name, ")
				.append(" 	ti.tax_reg_no, ti.invoice_number, ti.date_of_issue, ")
				.append("  	ti.taxable_amount + ti.non_taxable_amount total_amount, ti.taxable_amount,  ")
				.append(" 	igst_rate, igst_amount, state_code ").append(" from t_tax_invoice ti, ").append(" 	( ")
				.append(igstTaxView).append(" ) g, ").append("	t_reservation_contact rc ").append(" where ti.ti_id = g.ti_id ")
				.append(" 	and ti.tax_reg_no is null ").append("	and ti.original_pnr = rc.pnr ")
				.append(" 	and (g.igst_rate is not null and (ti.taxable_amount + ti.non_taxable_amount) > 250000) ");

		StringBuilder section7 = new StringBuilder();
		section7.append(gstSubquery)
				.append(" select nvl(state_code, ti.state) state, sum ( ti.taxable_amount ) taxable_amount,  ")
				.append("  	cgst_rate, sum ( cgst_amount ) cgst_amount,  sgst_rate, sum ( sgst_amount ) sgst_amount,  igst_rate, sum ( igst_amount ) igst_amount ")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ")
				.append(gstTaxView)
				.append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and ti.tax_reg_no is null ")
				.append(" 	and (g.sgst_rate is not null or ( g.igst_rate is not null and (ti.taxable_amount + ti.non_taxable_amount) <= 250000) ) ")
				.append(" 	and ti.invoice_type = 'I' ")
				.append(" group by cgst_rate, sgst_rate, igst_rate, nvl(state_code, ti.state) ");

		StringBuilder section7A_1 = new StringBuilder();
		section7A_1.append(gstSubquery)
				.append(" select nvl(state_code, ti.state) state, sum ( ti.taxable_amount ) taxable_amount,  ")
				.append("  	cgst_rate, sum ( cgst_amount ) cgst_amount,  sgst_rate, sum ( sgst_amount ) sgst_amount ")
				.append(" from t_tax_invoice ti, ").append(" ( ").append(sgstTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and ti.tax_reg_no is null ")
				.append(" 	and g.sgst_rate is not null ")
				.append(" group by cgst_rate, sgst_rate, nvl(state_code, ti.state) ");

		StringBuilder section7B_1 = new StringBuilder();
		section7B_1.append(gstSubquery)
				.append(" select nvl(state_code, ti.state) state, sum ( ti.taxable_amount ) taxable_amount,  ")
				.append("  	igst_rate, sum ( igst_amount ) igst_amount ").append(" from t_tax_invoice ti, ").append(" ( ")
				.append(igstTaxView).append(" ) g ").append(" where ti.ti_id = g.ti_id and ti.tax_reg_no is null ")
				.append(" 	and (g.igst_rate is not null and ((ti.taxable_amount + ti.non_taxable_amount) <= 250000) ) ")
				.append(" group by igst_rate, nvl(state_code, ti.state) ");

		StringBuilder section9B = new StringBuilder();
		section9B
				.append(gstSubquery)
				.append(" select ti.tax_reg_no, ti.invoice_number, ti.date_of_issue, ti.taxable_amount, (ti.taxable_amount + ti.non_taxable_amount) debit_note_value, ")
				.append(" oti.invoice_number original_invoice_number, oti.tax_reg_no original_invoice_tax_reg_no, oti.date_of_issue original_date_of_issue, ")
				.append("  	cgst_rate, cgst_amount,  sgst_rate, sgst_amount,  igst_rate, igst_amount, state_name ")
				.append(" from t_tax_invoice ti, t_tax_invoice oti, ").append(" ( ").append(gstTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and ti.original_ti_id = oti.ti_id ").append(" 	and ti.invoice_type = 'D' ")
				.append(" and ti.tax_reg_no is not null ");

		StringBuilder section12 = new StringBuilder();
		section12
				.append(gstSubquery)
				.append(" select sum(taxable_amount) + sum(non_taxable_amount) as total_value, ")
				.append(" sum(taxable_amount) as total_taxable_value, ").append(" sum(tax1_amount) as total_central_tax, ")
				.append(" sum(tax2_amount) as total_state_tax, ").append(" sum(tax3_amount) as total_integrated_tax ")
				.append(" from ( " + invoiceAndDebitNoteSubquery + " )");

		StringBuilder section13_1 = new StringBuilder();
		section13_1
				.append(gstSubquery)
				.append(" select count(*) as total_number, min(invoice_number) as invoice_number_from, max(invoice_number) as invoice_number_to ")
				.append(" from ( " + invoiceSubquery + " )");

		StringBuilder section13_2 = new StringBuilder();
		section13_2
				.append(gstSubquery)
				.append(" select count(*) as total_number, min(invoice_number) as invoice_number_from, max(invoice_number) as invoice_number_to ")
				.append(" from ( " + debitNoteSubquery + " )");

		Map<String, String> sqlsMap = new HashMap<>();
		sqlsMap.put(ReportStructure.GST.GSTR1.S_4A, section4A.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_5A, section5A.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_7A_1, section7A_1.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_7B_1, section7B_1.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_9B, section9B.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_12, section12.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_13_1, section13_1.toString());
		sqlsMap.put(ReportStructure.GST.GSTR1.S_13_2, section13_2.toString());

		return sqlsMap;
	}


	public Map<String, String> getGstr3Report(ReportsSearchCriteria criteria) {

		/*
		   TODO --
		   india hard-coded
		   invoice hard-coded

		 */

		String fromDate = criteria.getDateRangeFrom();
		String toDate = criteria.getDateRangeTo();
		String stateCode = criteria.getStateCode();

		StringBuilder gstSubquery = new StringBuilder();
		gstSubquery
				.append(" with c as ( ")
				.append(" select r.charge_rate_id, r.charge_value_percentage, c.charge_code, c.country_code, c.state_id, c.inter_state, s.state_code ")
				.append(" from t_charge_rate r, t_charge c, t_state s ")
				.append(" where r.charge_code = c.charge_code and c.state_id = s.state_id (+) and c.country_code = 'IN' ")
				.append(" ) ");

		StringBuilder cgstAndsgstTaxView = new StringBuilder();
		cgstAndsgstTaxView
				.append(" select i.ti_id, c1.charge_value_percentage cgst_rate, i.tax1_amount cgst_amount, ")
				.append(" 	c2.charge_value_percentage sgst_rate, i.tax2_amount sgst_amount, c2.state_code ")
				.append(" from t_tax_invoice i, c c1, c c2 ")
				.append(" where i.tax1_rate_id = c1.charge_rate_id (+) and  i.tax2_rate_id = c2.charge_rate_id (+) ")
				.append("  	and i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append("  	and i.state = '" + stateCode + "'");
		
		StringBuilder cgstAndsgstInvoiceTaxView = new StringBuilder();
		cgstAndsgstInvoiceTaxView
				.append(" select i.ti_id, c1.charge_value_percentage cgst_rate, i.tax1_amount cgst_amount, ")
				.append(" c2.charge_value_percentage sgst_rate, i.tax2_amount sgst_amount, c2.state_code ")
				.append(" from t_tax_invoice i, c c1, c c2 ")
				.append(" where i.tax1_rate_id = c1.charge_rate_id (+) and  i.tax2_rate_id = c2.charge_rate_id (+) ")
				.append(" and i.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append(" and i.state = '" + stateCode + "' and i.invoice_type = 'I'");
		
		StringBuilder cgstAndsgstDebitNoteTaxView = new StringBuilder();
		cgstAndsgstDebitNoteTaxView
				.append(" select i.ti_id, c1.charge_value_percentage cgst_rate, i.tax1_amount cgst_amount, ")
				.append(" c2.charge_value_percentage sgst_rate, i.tax2_amount sgst_amount, c2.state_code ")
				.append(" from t_tax_invoice i, c c1, c c2 ")
				.append(" where i.tax1_rate_id = c1.charge_rate_id (+) and  i.tax2_rate_id = c2.charge_rate_id (+) ")
				.append(" and i.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append(" and i.state = '" + stateCode + "' and i.invoice_type = 'D'");


		StringBuilder igstTaxView = new StringBuilder();
		igstTaxView
				.append(" select i.ti_id, c3.charge_value_percentage igst_rate, i.tax3_amount igst_amount, c3.state_code ")
				.append(" from t_tax_invoice i, c c3 ")
				.append(" where i.tax3_rate_id = c3.charge_rate_id (+) ")
				.append(" and i.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append(" and i.state = '" + stateCode + "'");
		
		StringBuilder fullTaxView = new StringBuilder();
		fullTaxView
				.append(" select i.ti_id, i.tax3_amount igst_amount, c3.state_code, ")
				.append(" c1.charge_value_percentage cgst_rate, c2.charge_value_percentage sgst_rate, c3.charge_value_percentage igst_rate")
				.append(" from t_tax_invoice i, c c1, c c2, c c3 ")
				.append(" where i.tax3_rate_id = c3.charge_rate_id (+) ")
				.append(" AND i.tax1_rate_id   = c1.charge_rate_id (+) ")
				.append(" AND i.tax2_rate_id   = c2.charge_rate_id (+) ")
				.append(" and i.date_of_issue between ")
				.append(" to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append(" and i.state = '" + stateCode + "'");
		
		StringBuilder igstInvoiceDebitTaxView = new StringBuilder();
		igstInvoiceDebitTaxView
				.append(" select i.ti_id, c3.charge_value_percentage igst_rate, i.tax3_amount igst_amount, c3.state_code ")
				.append(" from t_tax_invoice i, c c3 ")
				.append(" where i.tax3_rate_id = c3.charge_rate_id (+) ")
				.append("  	and i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append("  	and i.state = '" + stateCode + "' ")
				.append("  	and ( i.invoice_type = 'I' or (i.invoice_type = 'D' and i.tax_reg_no is null)) ");
		
		StringBuilder igstDebitNoteTaxView = new StringBuilder();
		igstDebitNoteTaxView
				.append(" select i.ti_id, c3.charge_value_percentage igst_rate, i.tax3_amount igst_amount, c3.state_code ")
				.append(" from t_tax_invoice i, c c3 ")
				.append(" where i.tax3_rate_id = c3.charge_rate_id (+) ")
				.append("  	and i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append("  	and i.state = '" + stateCode + "' and i.invoice_type = 'D'");

		
		StringBuilder section_3 = new StringBuilder();
		section_3.append(" select round(sum(i.taxable_amount),0) as total_taxable_amount")
				.append(" from t_tax_invoice i ")
				.append(" where i.date_of_issue between ")
				.append("  		to_date('" + fromDate + "', 'dd/mm/yyyy hh24:mi:ss') and to_date('" + toDate + "', 'dd/mm/yyyy hh24:mi:ss')  ")
				.append("  	and i.state = '" + stateCode + "'");


		StringBuilder section_4_1 = new StringBuilder();
		section_4_1
				.append(gstSubquery)
				.append(" select state_code, igst_rate, sum(ti.taxable_amount), sum(igst_amount) ")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ").append(igstInvoiceDebitTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and g.state_code is not null ")
				.append(" group by state_code, igst_rate ");

		StringBuilder section_4_2 = new StringBuilder();
		section_4_2
				.append(gstSubquery)
				.append(" select state_code,  sum(ti.taxable_amount), sum(cgst_amount), sum(sgst_amount), sgst_rate, cgst_rate ")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ").append(cgstAndsgstTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and g.state_code is not null ")
				.append(" group by state_code, sgst_rate, cgst_rate ");
		
		StringBuilder section_4_3_I = new StringBuilder();
		section_4_3_I
				.append(gstSubquery)
				.append(" select state_code, igst_rate, sum(ti.taxable_amount), sum(igst_amount) ")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ").append(igstDebitNoteTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and ti.tax_reg_no is not null and g.state_code is not null ")
				.append(" group by state_code, igst_rate ");

		StringBuilder section_4_3_II = new StringBuilder();
		section_4_3_II
				.append(gstSubquery)
				.append(" select state_code,  sum(ti.taxable_amount), sum(cgst_amount), sum(sgst_amount), sgst_rate, cgst_rate ")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ").append(cgstAndsgstDebitNoteTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id and ti.tax_reg_no is not null and g.state_code is not null ")
				.append(" group by state_code, sgst_rate, cgst_rate ");

		StringBuilder section_8 = new StringBuilder();
		section_8
				.append(gstSubquery)
				.append(" select trim(to_char(to_date(to_char(ti.date_of_issue, 'yyyymm'), 'yyyymm'), 'yyyy-MONTH')) AS MONTH, ") 
				.append(" sum(ti.taxable_amount) AS VALUE, ")
				.append("DECODE(SUM(ti.tax1_amount), 0, null, SUM(ti.tax1_amount)) AS CGST_AMOUNT, ")
				.append("DECODE(SUM(ti.tax2_amount), 0, null, SUM(ti.tax2_amount)) AS SGST_AMOUNT, ")
				.append("DECODE(SUM(ti.tax3_amount), 0, null, SUM(ti.tax3_amount)) AS IGST_AMOUNT, ")
				.append(" cgst_rate, sgst_rate, igst_rate")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ").append(fullTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id ")
				.append(" group by to_char(ti.date_of_issue, 'yyyymm'), cgst_rate, sgst_rate, igst_rate")
				.append(" order by to_char(ti.date_of_issue, 'yyyymm') ");
		
		StringBuilder section_12 = new StringBuilder();
		section_12
				.append(gstSubquery)
				.append(" select trim(to_char(to_date(to_char(ti.date_of_issue, 'yyyymm'), 'yyyymm'), 'yyyy-MONTH')) AS MONTH, ") 
				.append(" 	sum(ti.taxable_amount) AS VALUE, sum(ti.tax1_amount) AS CGST_AMOUNT, sum(ti.tax2_amount) AS SGST_AMOUNT, sum(ti.tax3_amount) AS IGST_AMOUNT ")
				.append(" from t_tax_invoice ti, ")
				.append(" ( ").append(igstTaxView).append(" ) g ")
				.append(" where ti.ti_id = g.ti_id ")
				.append(" group by to_char(ti.date_of_issue, 'yyyymm') ")
				.append(" order by to_char(ti.date_of_issue, 'yyyymm') ");

		Map<String, String> sqlsMap = new HashMap<>();
		sqlsMap.put(ReportStructure.GST.GSTR3.S_3, section_3.toString());
		sqlsMap.put(ReportStructure.GST.GSTR3.S_4_1, section_4_1.toString());
		sqlsMap.put(ReportStructure.GST.GSTR3.S_4_2, section_4_2.toString());
		sqlsMap.put(ReportStructure.GST.GSTR3.S_4_3_I, section_4_3_I.toString());
		sqlsMap.put(ReportStructure.GST.GSTR3.S_4_3_II, section_4_3_II.toString());
		sqlsMap.put(ReportStructure.GST.GSTR3.S_8, section_8.toString());
		sqlsMap.put(ReportStructure.GST.GSTR3.S_12, section_12.toString());

		return sqlsMap;

	}
	
	public String getBlockedLoyaltyPointsDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		
		String fromDate = reportsSearchCriteria.getDateRangeFrom();
		String toDate = reportsSearchCriteria.getDateRangeTo();
		String lmsId = reportsSearchCriteria.getAccountNumber();
		String pnr = reportsSearchCriteria.getPnr();

		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT lmsb.EMAIL_ID, tmpt.PNR, lmsb.BLOCKED_CREDIT_AMOUNT,");
		sb.append("lmsb.TOTAL_PAYMENT_AMOUNT, lmsb.CREATED_DATE, UTILIZED_STATUS as STATUS");
		sb.append(" FROM T_LMS_CREDIT_BLOCKED_INFO lmsb INNER JOIN  T_TEMP_PAYMENT_TNX tmpt ON");
		sb.append(" lmsb.TPT_ID = tmpt.TPT_ID");
		sb.append(" WHERE lmsb.CREATED_DATE BETWEEN TO_DATE (" + fromDate + ")");
		sb.append(" AND TO_DATE( " + toDate + " ) ");

		if (isNotEmptyOrNull(lmsId)) {
			sb.append(" AND lower(lmsb.EMAIL_ID) =  '" + lmsId.toLowerCase() + "' ");
		}
		if (isNotEmptyOrNull(pnr)) {
			sb.append("  AND tmpt.PNR = '" + pnr + "' ");
		}
		
		return sb.toString();
	}
	
	public String getCCTransactionsLMSPointsDataQuery(ReportsSearchCriteria reportsSearchCriteria) {
		
		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String entityCode = reportsSearchCriteria.getEntity();
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT to_char(TRUNC(TPT.timestamp),'YYYY-MM-DD') DATE_OF_TXN,");
		sb.append(" COUNT(DECODE (TPT.status,'"+ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS+"',1,NULL)) SUCCESS,");
		sb.append(" SUM(DECODE(TPT.status,'"+ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS+"',TPT.amount,0)) VALUEOFRESERVATIONSUCCES,");
		sb.append(" SUM(DECODE (TPT.status,'"+ ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE+"',TPT.amount,0)) VALUEOFCCFAILED,");
		sb.append(" COUNT(DECODE (TPT.status,'"+ ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE+"',1,NULL)) RESFAILED,");
		sb.append(" SUM(DECODE (LMS.UTILIZED_STATUS,'SUCCESS',LMS.BLOCKED_CREDIT_AMOUNT,0)) VALUELMSSUCCESS,");
		sb.append(" SUM(DECODE (LMS.UTILIZED_STATUS,'FAIL',LMS.BLOCKED_CREDIT_AMOUNT,0)) VALUEOFLMSFAILED");
		sb.append(" FROM T_TEMP_PAYMENT_TNX TPT,");
		sb.append(" T_CCARD_PAYMENT_STATUS CC,");
		sb.append(" T_LMS_CREDIT_BLOCKED_INFO LMS,");
		sb.append(" T_PRODUCT P");
		sb.append(" WHERE TPT.TIMESTAMP BETWEEN TO_DATE('" + strFromDate + " 00:00:00','dd/mm/yyyy HH24:mi:ss' ) ");
		sb.append(" AND TO_DATE('" + strToDate + " 23:59:59','dd/mm/yyyy HH24:mi:ss' ) ");
		sb.append("	AND TPT.DR_CR ='" + ReservationInternalConstants.TnxTypes.CREDIT + "'");
		sb.append(" AND TPT.tpt_id      = CC.tpt_id");
		sb.append(" AND TPT.TPT_ID      = LMS.TPT_ID");
		sb.append(" AND CC.service_type = 'ccauthorize'");
		sb.append(" AND TPT.product_type = P.product_type ");
		sb.append(" AND P.entity_code = '" + entityCode + "' ");
		sb.append(" GROUP BY TRUNC(TPT.timestamp)");
		sb.append(" ORDER BY date_of_txn ASC");

		return sb.toString();
	}
	
	
	public String getCCTransactionsLMSPointsDetailQuery(ReportsSearchCriteria reportsSearchCriteria) {

		String strFromDate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String entityCode = reportsSearchCriteria.getEntity();
		
		StringBuilder sb = new StringBuilder();

		sb.append("SELECT to_char(T.TIMESTAMP,'DD-Mon-YY:HH12:MI') BK_TIMESTAMP,");
		sb.append(" T.TPT_ID TNX_ID,T.PNR,T.CONTACT_PERSON,");
		sb.append(" T.TELEPHONE_NO,T.MOBILE_NO ,T.IP_ADDRESS,");
		sb.append(" (DECODE (T.CHANNEL_ID,'4','WEB','20','IOS','21','ANDROID',T.USER_ID)) USER_ID,");
		sb.append(" T.PAYMENT_CURRENCY_AMOUNT,T.PAYMENT_CURRENCY_CODE,");

		sb.append(" (SELECT fs.segment_code");
		sb.append(" FROM t_pnr_segment ps,");
		sb.append(" t_flight_segment fs");
		sb.append(" WHERE ps.flt_seg_id = fs.flt_seg_id");
		sb.append(" AND ps.pnr = t.pnr");
		sb.append(" AND ps.status = 'CNF'");
		sb.append(" AND r.dummy_booking = 'N'");
		sb.append(" AND rownum = 1");
		sb.append(" UNION SELECT fs.segment_code");
		sb.append(" FROM t_ext_pnr_segment ps,");
		sb.append(" t_ext_flight_segment fs");
		sb.append(" WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id");
		sb.append(" AND ps.pnr = t.pnr");
		sb.append(" AND ps.status = 'CNF'");
		sb.append(" AND r.dummy_booking = 'Y'");
		sb.append(" AND rownum  = 1");
		sb.append(" ) AS segment,");

		sb.append(" (SELECT fs.est_time_departure_local FROM t_pnr_segment ps, t_flight_segment fs");
		sb.append(" WHERE ps.flt_seg_id = fs.flt_seg_id");
		sb.append(" AND ps.pnr = t.pnr AND ps.status = 'CNF'");
		sb.append(" AND r.dummy_booking = 'N'");
		sb.append(" AND rownum = 1 UNION");
		sb.append(" SELECT fs.est_time_departure_local");
		sb.append(" FROM t_ext_pnr_segment ps, t_ext_flight_segment fs");
		sb.append(" WHERE ps.ext_flt_seg_id = fs.ext_flt_seg_id");
		sb.append(" AND ps.pnr = t.pnr  AND ps.status = 'CNF'");
		sb.append(" AND r.dummy_booking = 'Y'");
		sb.append(" AND rownum  = 1)  AS departure,");

		sb.append(" REPLACE(REPLACE(C.error_specification,'REJECTED ',''),'VALID HASH.', '') AS REASON,");
		sb.append(
				" (DECODE(T.STATUS,'RS','ReservationSucces','I','Initiated','II','Initiated LeftOver','PF','Rejected','UF','PendingRefunds','US','SuccessfulReversals','IS','SuccessfulReversals', 'PS', 'PaymentSuccess', 'RF', 'ReservationFail')) PAYMENT_STATUS,");
		sb.append(" T.FRAUD_STATUS,C.NM_TRANSACTION_REFERENCE MERCHANT_REF_NUMBER , C.TRANSACTION_REFERENCE PGW_TXN_NUMBER,");
		sb.append(" PGW.DESCRIPTION PGW_NAME,");
		sb.append(
				" REGEXP_REPLACE((REGEXP_SUBSTR(C.request_text, 'vpc_OrderInfo=([a-zA-Z0-9]*-)*(.)((([0-9])*)\\.)*[0-9]*')), 'vpc_OrderInfo=', '') AS order_reference,");
		sb.append(" LMS.EMAIL_ID AS LMS_ID,");
		sb.append(" LMS.BLOCKED_CREDIT_AMOUNT AS LMS_AMOUNT,");
		sb.append(" LMS.UTILIZED_STATUS AS LMS_STATUS");
		sb.append(" FROM T_TEMP_PAYMENT_TNX T,");
		sb.append(" T_CCARD_PAYMENT_STATUS C,");
		sb.append(" t_reservation r,");
		sb.append(" t_reservation_contact rCon,");
		sb.append(" t_payment_gateway_currency pgwCurr,");
		sb.append(" T_PAYMENT_GATEWAY pgw,");
		sb.append(" T_LMS_CREDIT_BLOCKED_INFO LMS,");
		sb.append(" T_PRODUCT P");
		sb.append(" WHERE T.TPT_ID = C.TPT_ID (+)");
		sb.append(" AND r.pnr = rCon.pnr (+)");
		sb.append(" AND T.TPT_ID = LMS.TPT_ID");
		sb.append(" AND t.pnr = r.pnr (+)");
		sb.append(" AND T.TIMESTAMP BETWEEN TO_DATE('" + strFromDate + "  00:00:00','dd-mon-yyyy hh24:mi:ss') AND TO_DATE('"
				+ strToDate + "  23:59:59','dd-mon-yyyy hh24:mi:ss') ");
		
		sb.append("AND T.DR_CR ='" + ReservationInternalConstants.TnxTypes.CREDIT + "'");
		
		sb.append("AND C.service_type = 'ccauthorize' ");
		
		sb.append(" AND pgwCurr.PAYMENT_GATEWAY_CURRENCY_CODE = C.GATEWAY_NAME ");
		sb.append(" AND pgwCurr.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID ");
		sb.append(" AND T.product_type = P.product_type ");
		sb.append(" AND P.entity_code = '" + entityCode + "' ");
		sb.append(" order by r.booking_timestamp ");
		
		return sb.toString();

	}


	public String getGSTAdditionalReportData(ReportsSearchCriteria searchCriteria) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		sb.append("WITH c AS ")
				.append("  (SELECT r.charge_rate_id, r.charge_value_percentage, c.charge_code, c.country_code,  c.state_id,")
				.append(" c.inter_state, s.state_code, s.state_name ")
				.append("  FROM t_charge_rate r , t_charge c, t_state s   WHERE r.charge_code = c.charge_code ")
				.append("  AND c.state_id   = s.state_id (+) AND c.country_code  = '" + searchCriteria.getCountryCode() + "' ) ")
				.append("SELECT ti.original_pnr, NVL(SUBSTR(LTRIM(ti.journey_ond), 1,3), f.origin) origin , ")
				.append(" ti.invoice_number, ")
				.append(" to_char(ti.date_of_issue, 'DD/MM/YYYY') date_of_issue, ")
				.append("  ti.taxable_amount + ti.non_taxable_amount total_amount, ti.taxable_amount, ")
				.append("  NVL(t2.INVOICE_NUMBER, ' ') original_inv_number, NVL(cgst_rate, 0) cgst_rate,")
				.append("NVL (cgst_amount, 0) cgst_amount, NVL(sgst_rate, 0) sgst_rate, ")
				.append("  NVL(sgst_amount, 0) sgst_amount, NVL(igst_rate, 0) igst_rate, NVL(igst_amount, 0) igst_amount, ")
				.append("place_of_supply, to_char(booking_timestamp, 'DD/MM/YYYY') booking_timestamp, ")
				.append("  no_of_pax, contact_country_name, NVL(contact_state_name, ' ') contact_state_name, "
						+ "NVL(SUBSTR(LTRIM(ti.journey_ond), 1,7), segment_code) segment_code, ")
				.append("  NVL(tax_reg_number, ' ') tax_reg_number, state.state_name tax_deposit_state,")
				.append(" state.AIRLINE_OFC_TAX_REG_NUMBER, (CASE  ")
				.append("  WHEN   ti.invoice_type = 'I' AND tax_reg_number is not null THEN '4A' ")
				.append("  WHEN   ti.invoice_type = 'D' AND tax_reg_number is not null THEN '9B' ")
				.append("  WHEN   tax_reg_number is null ")
				.append(" AND ti.taxable_amount + ti.non_taxable_amount> 250000 AND igst_rate is not null")
				.append(" AND sgst_rate is not null THEN '5A, 7A_1' ").append("  WHEN  tax_reg_number is null ")
				.append(" AND ti.taxable_amount + ti.non_taxable_amount> 250000 AND igst_amount > 0 THEN '5A' ")
				.append("  WHEN tax_reg_number is null AND sgst_rate is not null AND igst_rate is not null")
				.append(" AND ti.taxable_amount + ti.non_taxable_amount <= 250000 THEN '7A_1, 7B_1' ")
				.append(" WHEN tax_reg_number is null AND sgst_rate is not null THEN '7A_1' ")
				.append(" WHEN tax_reg_number is null AND igst_rate is not null ")
				.append(" AND ti.taxable_amount + ti.non_taxable_amount <= 250000 THEN '7B_1' ELSE ' ' ")
				.append("  END) gstr_section FROM t_tax_invoice ti, t_tax_invoice t2, t_state state, ")
				.append("  (SELECT i.ti_id,   c1.charge_value_percentage cgst_rate,   i.tax1_amount cgst_amount, ")
				.append("    c2.charge_value_percentage sgst_rate,   i.tax2_amount sgst_amount, ")
				.append("    c3.charge_value_percentage igst_rate,   i.tax3_amount igst_amount, ")
				.append("    NVL(c2.state_code, c3.state_code) state_code, NVL(c2.state_name, c3.state_name) place_of_supply ")
				.append("  FROM t_tax_invoice i,   c c1, c c2 , c c3 WHERE i.tax1_rate_id = c1.charge_rate_id (+) ")
				.append("  AND i.tax2_rate_id   = c2.charge_rate_id (+) AND i.tax3_rate_id   = c3.charge_rate_id (+) ")
				.append("  AND i.date_of_issue BETWEEN to_date('" + searchCriteria.getDateRangeFrom()
						+ "', 'dd/mm/yyyy hh24:mi:ss') ")
				.append("AND to_date('" + searchCriteria.getDateRangeTo() + "', 'dd/mm/yyyy hh24:mi:ss') ")
				.append("    AND i.state IN ( " + searchCriteria.getStateCode() + " ) )")
				.append(" g, (SELECT res.booking_timestamp booking_timestamp, ")
				.append("    res.total_pax_count + res.total_pax_child_count + res.total_pax_infant_count no_of_pax, ")
				.append("    coun.country_name contact_country_name,  contact_state.state_name contact_state_name, ")
				.append("    res.pnr pnr   , cont.TAX_REG_NUMBER tax_reg_number FROM t_reservation res, ")
				.append("    t_reservation_contact cont, t_country coun , t_state contact_state ")
				.append("  WHERE res.pnr           = cont.pnr AND cont.c_country_code = coun.country_code (+) ")
				.append("  AND cont.c_state        = contact_state.state_code (+) ) r, (SELECT ps.pnr pnr, ")
				.append("    fs.segment_code,   f1.ORIGIN ,  s.COUNTRY_CODE FROM t_pnr_segment ps, ")
				.append("    t_flight_segment fs,   t_flight f1,t_airport ap, t_station s WHERE ps.flt_seg_id = fs.flt_seg_id ")
				.append("  AND fs.FLIGHT_ID = f1.FLIGHT_ID AND ps.segment_seq  ='1' ")
				.append("  AND f1.origin = ap.AIRPORT_CODE and ap.STATION_CODE = s.station_code ")
				.append("  ) f WHERE ti.ti_id      = g.ti_id AND ti.original_pnr = r.pnr AND ti.original_pnr = f.pnr ")
				.append("AND state.state_code = ti.state AND ti.original_ti_id = t2.ti_id (+) ORDER BY ti.invoice_number ");
		return sb.toString();
	}

	public String getBlacklistedPassengersQuery(ReportsSearchCriteria reportsSearchCriteria) {
		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();

		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String blType = reportsSearchCriteria.getBlacklistType();

		String rptType = reportsSearchCriteria.getReportType();
//blpax.first_name as FNAME, blpax.last_name as LNAME
		sb.append("select flt.flight_number as FLTNO, pnrpax.pnr as PNR, blpax.PAX_FULL_NAME as BLPAXNAME, ");
		sb.append(" blpax.passport_number as PPNO, blpax.dob as DOB, nat.description as NATION, blpax.bl_type as BLTYPE,  blpax.valid_until as VALIDUNTIL ");
		sb.append(" FROM T_BL_PNR_PASSENGER  blres,	t_bl_passenger   blpax,	t_pnr_passenger  pnrpax, t_pnr_segment  pnrseg, ");
		sb.append(" t_flight_segment fltseg, t_flight flt, t_reservation  res, t_nationality  nat ");
		
		
		sb.append(" WHERE blres.bl_passenger_id = blpax.bl_passenger_id and ");
		sb.append("	pnrpax.pnr_pax_id = blres.pnr_pax_id  and ");
		sb.append("	pnrseg.pnr = pnrpax.pnr and ");
		sb.append("	fltseg.flt_seg_id = pnrseg.flt_seg_id and ");
		sb.append("	flt.flight_id = fltseg.flight_id and ");
		sb.append("	res.pnr = pnrpax.pnr and ");
		sb.append(" blres.IS_ACTIONED = 'N' and ");
		sb.append("	nat.nationality_code = blpax.nationality_code ");
				
		sb.append(" AND flt.DEPARTURE_DATE BETWEEN TO_DATE('");
		sb.append(strFromdate + " 00:00:00', 'DD-MON-YYYY HH24:MI:SS') ");
		sb.append(" AND	TO_DATE('" + strToDate + " 23:59:59', 'DD-MON-YYYY HH24:MI:SS') ");
		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" AND flt.flight_number = UPPER('" + flightNumber + "') ");
		}
		sb.append(" AND blpax.bl_type = '" + blType + "'");
		

		sb.append(" ORDER BY pnr");

		return sb.toString();
	}
	
	public String getPromotionCriteriaDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {
		
		StringBuilder sb = new StringBuilder();
		String criteriaId = reportsSearchCriteria.getPromoCriteriaID();
		
		sb.append("SELECT DISTINCT p_code.promo_code AS code, ");
		sb.append("p_criteria.promo_criteria_id,p_criteria.PROMO_NAME, p_criteria.DISCRIMINATOR, p_criteria.IS_PROMOCODE_ENABLED, p_criteria.PER_FLIGHT_LIMIT, ");
		sb.append("p_criteria.TOTAL_LIMIT, p_criteria.APPLIED_AMOUNT, p_criteria.RESTRICT_CNX, p_criteria.RESTRICT_MOD, p_criteria.RESTRICT_SPLIT, ");
		sb.append("p_criteria.STATUS, p_criteria.LIMITING_LOAD_FACTOR, p_criteria.DISCOUNT_APPLY_AS, ");
		sb.append("CASE  WHEN (DISCOUNT_APPLY_TO = 0) THEN 'Fare' WHEN (DISCOUNT_APPLY_TO = 1) THEN 'FareSurcharges' ");
		sb.append("WHEN (DISCOUNT_APPLY_TO = 2) THEN 'Total' WHEN (DISCOUNT_APPLY_TO = 3) THEN 'Reservation' ");
		sb.append("WHEN (DISCOUNT_APPLY_TO = 4) THEN 'Passenger' end AS DISCOUNT_APPLY_TO, ");
		sb.append("p_criteria.DISCOUNT_TYPE, p_criteria.DISCOUNT_VALUE, p_criteria.SYSTEM_GENERATED, p_criteria.USED_CREDIT, ");
		sb.append("p_criteria.APPLICABLE_FOR_ONEWAY, p_criteria.APPLICABLE_FOR_RETURN, p_criteria.NUM_BY_AGENT, p_criteria.NUM_BY_SALES_CHANNEL, ");
		sb.append("p_criteria.APPLICABILITY, p_criteria.SAME_PAX_RESTRICTION, p_criteria.SAME_SECTOR_RESTRICTION, ");
		sb.append("to_char(cast(p_criteria.DEP_DATE_FROM_RESTRICTION  as date),'DD-MM-YYYY') as DEP_DATE_FROM_RESTRICTION, ");
		sb.append("to_char(cast(p_criteria.DEP_DATE_TO_RESTRICTION  as date),'DD-MM-YYYY') as DEP_DATE_TO_RESTRICTION, ");
		sb.append("to_char(cast(p_criteria.EXPIRY_DATE_FROM_RESTRICTION  as date),'DD-MM-YYYY') as EXPIRY_DATE_FROM_RESTRICTION, ");
		sb.append("to_char(cast(p_criteria.EXPIRY_DATE_TO_RESTRICTION  as date),'DD-MM-YYYY') as EXPIRY_DATE_TO_RESTRICTION, ");
		sb.append("p_criteria.VERSION, p_criteria.RESTRICT_REMOVE_PAX, p_criteria.APPLICABLE_FOR_LOYALTY_MEMBERS, ");
		sb.append("p_criteria.APPLICABLE_FOR_REG_MEMBERS, p_criteria.APPLICABLE_FOR_GUESTS, ");  
		sb.append("UTL_RAW.CAST_TO_NVARCHAR2(TO_CHAR(i18msg.contents)) AS I18N_MESSAGE_KEY, ");
		sb.append("NVL2(p_criteria.promo_code,'N/A',NVL(p_code.fully_utilized,'N/A')) AS fully_utilized,");
		sb.append("reg_period.period AS registration_periods,flt_period.period AS flight_periods ");
		sb.append("FROM T_PROMOTION_CRITERIA p_criteria LEFT OUTER JOIN (SELECT PROMO_CRITERIA_ID,");
		sb.append("LISTAGG(TO_CHAR(FROM_DATE,'DD/MM/YYYY')||'-'||TO_CHAR(TO_DATE,'DD/MM/YYYY'),CHR(10)) ");
		sb.append("WITHIN GROUP(ORDER BY PROMO_CRITERIA_ID) AS period ");
		sb.append("FROM T_PROMO_CRITERIA_RES_PERIOD GROUP BY PROMO_CRITERIA_ID) reg_period ");
		sb.append("ON p_criteria.PROMO_CRITERIA_ID = reg_period.PROMO_CRITERIA_ID ");
		sb.append("LEFT OUTER JOIN (SELECT PROMO_CRITERIA_ID,");
		sb.append("LISTAGG(TO_CHAR(FROM_DATE,'DD/MM/YYYY')||'-'||TO_CHAR(TO_DATE,'DD/MM/YYYY'),CHR(10)) ");
		sb.append("WITHIN GROUP(ORDER BY PROMO_CRITERIA_ID) AS period ");
		sb.append("FROM T_PROMO_CRITERIA_FLIGHT_PERIOD GROUP BY PROMO_CRITERIA_ID) flt_period ");
		sb.append("ON p_criteria.PROMO_CRITERIA_ID = flt_period.PROMO_CRITERIA_ID ");
		sb.append("LEFT OUTER JOIN T_PROMO_CRITERIA_CODE p_code ");
		sb.append("ON p_criteria.PROMO_CRITERIA_ID = p_code.PROMO_CRITERIA_ID ");
		sb.append("LEFT OUTER JOIN (SELECT I18N_MESSAGE_KEY,  LISTAGG( message_content , '002C') " ); 
		sb.append("WITHIN GROUP(ORDER BY I18N_MESSAGE_KEY) AS contents FROM T_I18N_MESSAGE "); 
		sb.append("GROUP BY I18N_MESSAGE_KEY ) i18msg ON p_criteria.I18N_MESSAGE_KEY = i18msg.I18N_MESSAGE_KEY ");
		sb.append("WHERE p_criteria.PROMO_CRITERIA_ID = UPPER('" + criteriaId + "') ");
		
		return sb.toString();
		
	}

	public String getAutomaticCheckinDetailsQuery(ReportsSearchCriteria reportsSearchCriteria) {

		StringBuilder sb = new StringBuilder();

		String strFromdate = reportsSearchCriteria.getDateRangeFrom();
		String strToDate = reportsSearchCriteria.getDateRangeTo();
		String flightNumber = reportsSearchCriteria.getFlightNumber();
		String status = reportsSearchCriteria.getStatus();
		Collection<String> segmentCodes = reportsSearchCriteria.getSegmentCodes();
		
		sb.append("SELECT pp.pnr,pp.TITLE||' '||pp.FIRST_NAME||' '||pp.LAST_NAME AS pax_name, pp.pax_sequence,"
				+ "tf.flight_number AS flight_number,tf.departure_date AS flight_date,tfs.segment_code AS sector,"
				+ "ppsac.dcs_checkin_status, smtams.seat_code, ppsac.dcs_response_text, ppsac.seat_type_preference,"
				+ "CASE WHEN ppsac.sit_together_pnr_pax_id IS NOT NULL THEN 'Y' ELSE 'N' END AS sit_together,"
				+ "CASE WHEN (SELECT COUNT(*) FROM t_pnr_pax_seg_auto_checkin ppsac,t_pnr_pax_seg_auto_chkin_seat t2 WHERE ppsac.ppac_id = t2.ppac_id) "
				+ "> 0 THEN 'Y' ELSE 'N' END AS pre_booked_seat ");

		sb.append("FROM t_pnr_pax_seg_auto_checkin ppsac JOIN t_pnr_passenger pp ON ppsac.pnr_pax_id = pp.pnr_pax_id "
				+ "JOIN t_pnr_segment tps ON ppsac.pnr_seg_id = tps.pnr_seg_id JOIN t_flight_segment tfs ON tps.flt_seg_id = tfs.flt_seg_id "
				+ "JOIN t_flight tf ON tfs.flight_id = tf.flight_id JOIN sm_t_flight_am_seat stfam ON stfam.flight_am_seat_id = ppsac.flight_am_seat_id "
				+ "JOIN sm_t_aircraft_model_seats smtams ON smtams.am_seat_id = stfam.am_seat_id ");

		if (isNotEmptyOrNull(status) && !status.equalsIgnoreCase("ALL")) {
			sb.append("WHERE ppsac.dcs_checkin_status = UPPER('" + status + "') ");
		}

		if (isNotEmptyOrNull(strFromdate) && isNotEmptyOrNull(strToDate)) {
			sb.append("AND tf.departure_date BETWEEN TO_DATE('" + strFromdate
					+ " ', 'DD-MON-YYYY HH24:MI:SS') AND TO_DATE('" + strToDate + " ', 'DD-MON-YYYY HH24:MI:SS') ");
		}

		if (isNotEmptyOrNull(flightNumber)) {
			sb.append(" AND ");
			sb.append("tf.flight_number = '" + flightNumber + "'");
		}

		if (segmentCodes != null) {
			sb.append(" AND ");
			sb.append(ReportUtils.getReplaceStringForIN(" tfs.segment_code ", segmentCodes, false) + " ");
		}

		sb.append("group by pp.pnr, pp.title || ' ' || pp.first_name || ' ' || pp.last_name, pp.last_name, pp.pax_sequence, tf.flight_number,"
				+ "tf.departure_date, tfs.segment_code, ppsac.dcs_checkin_status, smtams.seat_code, ppsac.dcs_response_text,"
				+ "ppsac.seat_type_preference, CASE WHEN ppsac.sit_together_pnr_pax_id IS NOT NULL THEN 'Y' ELSE 'N' END");

		return sb.toString();
	}

}

