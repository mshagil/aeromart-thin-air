package com.isa.thinair.reporting.core.persistent.util;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * Class to load report data
 * 
 * @author Sumudu
 */

public class StoredProcedureCall extends StoredProcedure {

	private static Log log = LogFactory.getLog(StoredProcedureCall.class);

	public StoredProcedureCall(DataSource ds, String SQL, SqlParameter param[]) {
		setDataSource(ds);
		setSql(SQL);
		for (int paramLength = 0; paramLength < param.length; paramLength++) {
			declareParameter(param[paramLength]);
		}
		compile();
		if (log.isDebugEnabled()) {
			log.debug("SQL " + SQL);
		}
	}

	public Map callStoredProc(HashMap map) {
		Map out = null;
		try {
			out = execute(map);

		} catch (Exception e) {
			log.error("Exception in exec " + e.getMessage());
		}
		return out;
	}

}
