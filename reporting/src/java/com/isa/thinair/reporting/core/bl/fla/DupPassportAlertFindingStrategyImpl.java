package com.isa.thinair.reporting.core.bl.fla;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;
import com.isa.thinair.reporting.api.service.ReportingUtils;
import com.isa.thinair.reporting.core.persistent.dao.FLADataProviderDAOJDBC;
import com.isa.thinair.reporting.core.util.InternalConstants;

public class DupPassportAlertFindingStrategyImpl implements AlertFinderStatergy {

	@Override
	public List<FlaAlertDTO> composeAlerts(List<Integer> lstFlightIDs) throws ModuleException {
		HashMap<String, FlaAlertDTO> lstAlertRearrange = new HashMap<String, FlaAlertDTO>();

		List<FlaAlertDTO> lstDupPassAlert = ((FLADataProviderDAOJDBC) ReportingUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLADataProviderDAO)).getFLADupPassportAlert(lstFlightIDs);

		if (lstDupPassAlert != null && !lstDupPassAlert.isEmpty()) {

			for (FlaAlertDTO tbaAlertDto : lstDupPassAlert) {
				String tmpPNR = tbaAlertDto.getPnr();
				if (!lstAlertRearrange.containsKey(tmpPNR)) {
					lstAlertRearrange.put(tmpPNR, tbaAlertDto);

				}
			}
		}
		return new ArrayList(lstAlertRearrange.values());

	}

}
