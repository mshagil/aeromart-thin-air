package com.isa.thinair.reporting.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.sql.DataSource;

import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.reporting.api.criteria.SchduledReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.ReportMetaData;
import com.isa.thinair.reporting.api.model.SRCountDTO;
import com.isa.thinair.reporting.api.model.SRSentItem;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.reporting.core.persistence.dao.ScheduledReportDAO;

/**
 * @isa.module.dao-impl dao-name="ScheduledReportDAO"
 */
public class ScheduledReportDAOImpl extends PlatformBaseHibernateDaoSupport implements ScheduledReportDAO {

	public void saveScheduledReport(ScheduledReport scheduledReport) {
		hibernateSaveOrUpdate(scheduledReport);
	}

	public void saveSRSentItem(SRSentItem sentItem) {
		hibernateSaveOrUpdate(sentItem);
	}

	public ScheduledReport getScheduledReport(Integer scheduledReportId) {
		return (ScheduledReport) get(ScheduledReport.class, scheduledReportId);
	}

	public ReportMetaData getReportMetaData(String reportName) {
		return (ReportMetaData) get(ReportMetaData.class, reportName);
	}

	public Collection<ScheduledReport> getNotActivatedScheduleReports() {

		Date currentDate = new Date();
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE, 1);
		Date dateLater = cal.getTime();

		Object[] params = { dateLater, ScheduledReport.STATUS_NOT_STARTED };
		return find("from ScheduledReport sr " + " where sr.endDate > ? and sr.status = ?", params, ScheduledReport.class);
	}

	public Collection<ScheduledReport> getCompletionRequiredScheduleReports() {

		Date currentDate = new Date();
		Object[] params = { currentDate, ScheduledReport.STATUS_STARTED };
		return find("from ScheduledReport sr " + " where sr.endDate <= ? and sr.status = ?", params, ScheduledReport.class);
	}

	public Collection<Integer> getStartedScheduledReportIdsWithNoTriggers() {

		final Collection<Integer> idList = new ArrayList<Integer>();

		StringBuilder sql = new StringBuilder();
		sql.append("select rt.sr_templete_id ");
		sql.append("from t_sched_rept_templete rt ");
		sql.append("where not exists (select * from qrtz_triggers  qt where qt.trigger_name = rt.quartz_job_name) ");
		sql.append("and rt.status = 'STARTED' ");
		sql.append("and rt.start_date < current_date and rt.end_date > current_date");

		LookupService lookup = LookupServiceFactory.getInstance();
		JdbcTemplate jt = new JdbcTemplate((DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN));

		Object count = (Integer) jt.query(sql.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					Integer id = rs.getInt("sr_templete_id");
					idList.add(id);
				}
				return null;
			}
		});

		return idList;
	}

	public SRCountDTO getScheduledReportCount(String reportName, String userName, Date startDate, Date endDate) {

		String countGbl = "select count(*) from ScheduledReport sr where sr.reportName = '" + reportName
				+ "' AND sr.startDate >= ? AND sr.endDate <= ?";
		String countUser = countGbl + " and sr.scheduledBy = '" + userName + "'";
		Query query = getSession().createQuery(countGbl);
		query.setParameter(0, startDate);
		query.setParameter(1, endDate);
		int globleCount = ((Long) query.uniqueResult()).intValue();

		Query userCountQuerry = getSession().createQuery(countUser);
		userCountQuerry.setParameter(0, startDate);
		userCountQuerry.setParameter(1, endDate);
		int userCount = ((Long) userCountQuerry.uniqueResult()).intValue();

		return new SRCountDTO(globleCount, userCount);
	}

	@Override
	public Page getAllScheduledReports(SchduledReportsSearchCriteria searchCriteria, int startIndex, int pageSize) {
		String hqlCount = "select sr from ScheduledReport sr";
		StringBuilder sb = new StringBuilder(hqlCount);
		sb.append(" WHERE sr.scheduledReportId = sr.scheduledReportId ");// FIXME
		ArrayList<Object> params = new ArrayList<Object>();
		if (searchCriteria != null) {
			if (searchCriteria.getReportName() != null && !"".equals(searchCriteria.getReportName())) {
				sb.append(" AND sr.reportName LIKE '");
				sb.append(searchCriteria.getReportName());
				sb.append("' ");
			}
			if (searchCriteria.getReportUser() != null && !"".equals(searchCriteria.getReportUser())) {
				sb.append(" AND sr.scheduledBy LIKE '%");
				sb.append(searchCriteria.getReportUser().toUpperCase());
				sb.append("%' ");
			}
			if (searchCriteria.getFromDate() != null && !"".equals(searchCriteria.getFromDate())) {
				sb.append(" AND sr.startDate >= ?");
				params.add(searchCriteria.getFromDateAsDateTime());

			}
			if (searchCriteria.getToDate() != null && !"".equals(searchCriteria.getToDate())) {
				sb.append(" AND sr.endDate <= ?");
				params.add(searchCriteria.getToDateAsDateTime());
			}
		}
		sb.append(" ORDER BY sr.reportName, sr.startDate, sr.endDate, sr.scheduledBy, sr.status ");
		Query query = getSession().createQuery(sb.toString());
		Query queryCount = getSession().createQuery(sb.toString());
		int count = 0;
		for (Object object : params) {
			query.setParameter(count, object);
			queryCount.setParameter(count, object);
			count++;
		}
		query.setFirstResult(startIndex).setMaxResults(pageSize);
		return new Page(queryCount.list().size(), startIndex, startIndex + pageSize, query.list());
	}

	@Override
	public Page getAllSRSentItemsForScheduledReport(Long scheduledReportID, int startIndex, int pageSize) {
		String hqlCount = "select srSent from SRSentItem srSent WHERE srSent.scheduledReportId = " + scheduledReportID + " order by srSent.sentTime desc";
		StringBuilder sb = new StringBuilder(hqlCount);
		Query query = getSession().createQuery(sb.toString()).setFirstResult(startIndex).setMaxResults(pageSize);
		Query queryCount = getSession().createQuery(hqlCount);

		return new Page(queryCount.list().size(), startIndex, startIndex + pageSize, query.list());
	}
}
