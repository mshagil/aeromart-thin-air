<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="AirportTaxSurchargeReport" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="0" bottomMargin="0" uuid="def4fab8-97ed-4620-80b0-94cf31b02978">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="ID" class="java.lang.String" isForPrompting="false"/>
	<parameter name="IMG" class="java.lang.String" isForPrompting="false"/>
	<parameter name="FROM_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="TO_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="AGENT_NAME" class="java.lang.String" isForPrompting="false"/>
	<parameter name="PAYMENT_MODES" class="java.lang.String" isForPrompting="false"/>
	<parameter name="CARRIER" class="java.lang.String" isForPrompting="false"/>
	<parameter name="REPORT_MEDIUM" class="java.lang.String" isForPrompting="false"/>
	<parameter name="LOCALE" class="java.lang.String" isForPrompting="false"/>
	<field name="AIRPORT" class="java.lang.String"/>
	<field name="CHARGE_DESCRIPTION" class="java.lang.String"/>
	<field name="AMOUNT" class="java.lang.Double"/>
	<variable name="Airport_Total" class="java.lang.Double" resetType="Group" resetGroup="Station_Group" calculation="Sum">
		<variableExpression><![CDATA[$F{AMOUNT}]]></variableExpression>
	</variable>
	<variable name="Grand_Amount" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{AMOUNT}]]></variableExpression>
	</variable>
	<variable name="numFormat" class="java.text.NumberFormat">
		<variableExpression><![CDATA[NumberFormat.getInstance(new Locale($P{LOCALE}));
((NumberFormat)value).setMinimumFractionDigits(2)]]></variableExpression>
	</variable>
	<group name="Station_Group">
		<groupExpression><![CDATA[$F{AIRPORT}]]></groupExpression>
		<groupHeader>
			<band height="20" splitType="Stretch">
				<textField pattern="" isBlankWhenNull="true">
					<reportElement uuid="51929684-c183-413c-93d2-4cff0ff8c0f7" key="textField" mode="Transparent" x="4" y="1" width="66" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{AIRPORT}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="27" splitType="Stretch">
				<line>
					<reportElement uuid="5855ce2d-5c90-4732-8c04-e422cb3a5d50" key="line-8" mode="Opaque" x="99" y="21" width="320" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
					<graphicElement fill="Solid">
						<pen lineWidth="0.5" lineStyle="Solid"/>
					</graphicElement>
				</line>
				<line>
					<reportElement uuid="7439daf7-9102-4bf2-925e-75f4f4dd8749" key="line-8" mode="Opaque" x="99" y="2" width="320" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
					<graphicElement fill="Solid">
						<pen lineWidth="0.5" lineStyle="Solid"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0.00" isBlankWhenNull="true">
					<reportElement uuid="76407758-b874-4bd8-be5c-f42d74a78b64" key="textField" mode="Transparent" x="285" y="4" width="123" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{numFormat}.format($V{Airport_Total})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement uuid="ec3269ef-7d3b-4d57-9a9f-f11b6ac3878d" key="staticText-50" mode="Transparent" x="99" y="4" width="170" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Airport Total : ]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="188" splitType="Stretch">
			<staticText>
				<reportElement uuid="eb3818a6-f216-4aee-9ed0-f3afec9388a7" key="staticText-2" mode="Transparent" x="285" y="29" width="240" height="37" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" isPrintWhenDetailOverflows="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="14" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Airport Tax & Surcharge]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1a6c3cd4-ce27-4607-b6a3-ce220470bcc4" key="staticText-6" mode="Transparent" x="285" y="97" width="123" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Report ID :]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="33fcb407-961a-4855-b8ab-73e6714b2d72" key="staticText-6" mode="Transparent" x="285" y="110" width="123" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Date :]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7ec77846-3000-4cea-95b3-85ce1a398165" key="staticText-7" mode="Transparent" x="285" y="124" width="123" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Print Time :]]></text>
			</staticText>
			<textField pattern="HH.mm.ss" isBlankWhenNull="false">
				<reportElement uuid="b50ba540-a734-46d9-ba4b-bd1551c01856" key="textField-1" mode="Transparent" x="417" y="124" width="55" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
				<reportElement uuid="26c0d56e-42b9-4bee-9aba-05eacd4b504e" key="textField-1" mode="Transparent" x="417" y="110" width="56" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="3c8ec365-0a66-4f84-a2aa-9f76696f14d3" key="textField" mode="Transparent" x="417" y="97" width="85" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{ID}]]></textFieldExpression>
			</textField>
			<image scaleImage="Clip" hAlign="Left" vAlign="Top" isUsingCache="false" isLazy="true">
				<reportElement uuid="cdbd09a1-603d-4001-89eb-2d0813c2de39" key="image-1" mode="Opaque" x="4" y="29" width="195" height="56" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<graphicElement fill="Solid">
					<pen lineWidth="0.0" lineStyle="Solid"/>
				</graphicElement>
				<imageExpression><![CDATA[$P{IMG}]]></imageExpression>
			</image>
			<line direction="BottomUp">
				<reportElement uuid="3477ab60-df3e-4ee6-a8ae-ad8dba6f79bd" key="line-7" mode="Opaque" x="4" y="182" width="500" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="ed91e29a-c535-47b2-acbd-efae9527b8d5" key="staticText-40" mode="Transparent" x="285" y="162" width="123" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Amount]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="ea1de32e-9339-46cc-a5ee-0d09a98ef94c" key="staticText-4" mode="Transparent" x="4" y="110" width="56" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[From Date]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="9df7fefc-7c0b-426e-8cbd-b02aa5e88ea4" key="staticText-5" mode="Transparent" x="4" y="124" width="56" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[To Date]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="0f8482fb-84bf-4c6c-a876-c3c1e4f57573" key="textField" mode="Transparent" x="99" y="110" width="100" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{FROM_DATE}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement uuid="79cc6918-5283-42b3-b938-a1c9956347f8" key="textField" mode="Transparent" x="99" y="124" width="100" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[": "+$P{TO_DATE}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="2a3c2484-dde3-434c-a342-670406efe11a" key="staticText-44" mode="Transparent" x="474" y="124" width="31" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Zulu]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="a30e12f3-af62-4377-bc2a-8e90d8b105f4" key="staticText-45" mode="Transparent" x="99" y="162" width="170" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Charge Description]]></text>
			</staticText>
			<line>
				<reportElement uuid="bd117d4f-b97e-44c6-bc7b-d2b66f8f607c" key="line-9" mode="Opaque" x="3" y="158" width="500" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="edff5691-8a38-4970-94a4-21634c937941" key="staticText-49" mode="Transparent" x="4" y="162" width="66" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Airport]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="18" splitType="Prevent">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="c3a8fa81-215b-4761-aa78-cd171ff782bd" key="textField" mode="Transparent" x="99" y="2" width="170" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CHARGE_DESCRIPTION}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="a34ea2ad-2504-4056-ba47-2be28dbbd53e" key="textField" mode="Transparent" x="285" y="2" width="123" height="14" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($F{AMOUNT})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="77" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="314c63e8-c1fa-4213-851e-5ba69349baf1" key="textField" mode="Transparent" x="417" y="35" width="62" height="16" isPrintWhenDetailOverflows="true" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="cb9b844f-f8fd-49d6-b396-d9ed33a48f98" key="staticText-14" mode="Transparent" x="4" y="52" width="159" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="true" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[*** End of Report ***]]></text>
			</staticText>
			<line>
				<reportElement uuid="48379053-0649-4977-be20-65d0f454bc28" key="line-8" mode="Opaque" x="3" y="6" width="500" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<line>
				<reportElement uuid="2c4d54d4-c198-48fe-8d3b-b6d18dda1060" key="line-8" mode="Opaque" x="3" y="28" width="500" height="1" forecolor="#000000" backcolor="#FFFFFF"/>
				<graphicElement fill="Solid">
					<pen lineWidth="0.5" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement uuid="f31795fe-f3b7-4a70-9b3f-e42add32e59e" key="staticText-47" mode="Transparent" x="99" y="10" width="170" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Grand Total : ]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement uuid="5913d360-4387-443f-a0b2-a1df162e2a42" key="textField" mode="Transparent" x="4" y="37" width="159" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="7" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{CARRIER}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement uuid="d652d365-49fd-4aed-9ec5-cca4d936f79d" key="textField" mode="Transparent" x="285" y="10" width="123" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="10" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{numFormat}.format($V{Grand_Amount})]]></textFieldExpression>
			</textField>
		</band>
	</lastPageFooter>
	<summary>
		<band height="23" splitType="Stretch"/>
	</summary>
</jasperReport>
