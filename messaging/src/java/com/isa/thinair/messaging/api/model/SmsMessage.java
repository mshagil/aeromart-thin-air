package com.isa.thinair.messaging.api.model;

import java.io.Serializable;
import java.util.Collection;

public class SmsMessage implements Message, Serializable {

	private static final long serialVersionUID = 3316251864184830003L;

	private String subject;

	private String body;

	private String recipient;

	private Collection auditInfo;

	private Collection objectInfo;

	public String getBody() {
		return body;
	}

	public String getSubject() {
		return subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return Returns the auditInfo.
	 */
	public Collection getAuditInfo() {
		return auditInfo;
	}

	/**
	 * @param auditInfo
	 *            The auditInfo to set.
	 */
	public void setAuditInfo(Collection auditInfo) {
		this.auditInfo = auditInfo;
	}

	/**
	 * @return Returns the objectInfo.
	 */
	public Collection getObjectInfo() {
		return objectInfo;
	}

	/**
	 * @param objectInfo
	 *            The objectInfo to set.
	 */
	public void setObjectInfo(Collection objectInfo) {
		this.objectInfo = objectInfo;
	}

}
