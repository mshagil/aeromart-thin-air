/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

/**
 * @author sumudu
 */
public class Topic implements Serializable {

	private static final long serialVersionUID = 6466071649065864438L;

	public String topicName;

	private HashMap topicParams;

	private String contentType = null;

	private Locale locale;

	private String mailServerConfigurationName = null;

	private Collection<String> attachmentFileNames;

	private Collection auditInfo;

	private Collection objectInfo;

	private boolean attachMessageBody = false;

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public HashMap getTopicParams() {
		return topicParams;
	}

	public void setTopicParams(HashMap topicParams) {
		this.topicParams = topicParams;
	}

	public String getMailServerConfigurationName() {
		return mailServerConfigurationName;
	}

	public void setMailServerConfigurationName(String mailServerConfigurationName) {
		this.mailServerConfigurationName = mailServerConfigurationName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return Returns the locale.
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            The locale to set.
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Collection getAttachmentFileNames() {
		return attachmentFileNames;
	}

	public void addAttachmentFileName(String attachmentFileName) {
		if (this.attachmentFileNames == null) {
			this.attachmentFileNames = new ArrayList<String>();
		}
		this.attachmentFileNames.add(attachmentFileName);
	}

	/**
	 * @return Returns the attachMessage.
	 */
	public boolean isAttachMessageBody() {
		return attachMessageBody;
	}

	/**
	 * @param attachMessage
	 *            The attachMessage to set.
	 */
	public void setAttachMessageBody(boolean attachMessage) {
		this.attachMessageBody = attachMessage;
	}

	/**
	 * @return Returns the auditInfo.
	 */
	public Collection getAuditInfo() {
		return auditInfo;
	}

	/**
	 * @param auditInfo
	 *            The auditInfo to set.
	 */
	public void setAuditInfo(Collection auditInfo) {
		this.auditInfo = auditInfo;
	}

	/**
	 * @return Returns the objectInfo.
	 */
	public Collection getObjectInfo() {
		return objectInfo;
	}

	/**
	 * @param objectInfo
	 *            The objectInfo to set.
	 */
	public void setObjectInfo(Collection objectInfo) {
		this.objectInfo = objectInfo;
	}
}
