package com.isa.thinair.messaging.api.dto;

import java.io.Serializable;

/**
 * DTO that holds update status information when an email is sent.
 * 
 * @author lalanthi
 */
public class UpdateEmailStatusInfoDTO implements Serializable {
	private static final long serialVersionUID = -7301034438677182265L;
	private long ppssId;
	private String status;
	
	public long getPpssId() {
		return ppssId;
	}

	public void setPpssId(long ppssId) {
		this.ppssId = ppssId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
