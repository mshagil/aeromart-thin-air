/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.processor;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.isa.thinair.messaging.api.model.Message;

/**
 * @author Nasly
 */
public class MessageParser {

	private final Log log = LogFactory.getLog(getClass());

	public void parse(InputStream inputStream, Message message) {
		StringBuffer subject = new StringBuffer();
		StringBuffer content = new StringBuffer();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilderFactory.setCoalescing(true);
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(inputStream);

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("subject");

			int intRecords = listOfRecords.getLength();

			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				subject.append(menuNode.getFirstChild().getNodeValue());
			}

			listOfRecords = doc.getElementsByTagName("body");

			intRecords = listOfRecords.getLength();

			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				content.append(menuNode.getFirstChild().getNodeValue());
			}

			message.setSubject(subject.toString());
			message.setBody(content.toString());

			if (log.isDebugEnabled()) {
				log.debug("Parsed data [subject=" + subject.toString() + ",body=" + content.toString() + "]");
			}
		} catch (SAXParseException spe) {
			log.error("Parsing error" + ", line " + spe.getLineNumber() + ", uri " + spe.getSystemId(), spe);
			log.error("Parsing error" + ", stream " + inputStream.toString());
		} catch (SAXException e) {
			log.error(e);
		} catch (Throwable t) {
			log.error(t);
		}
	}
}
