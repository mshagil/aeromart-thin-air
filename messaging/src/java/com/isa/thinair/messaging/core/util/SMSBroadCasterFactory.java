/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.util;

import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.transport.AlayuobiSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.AxonSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.DhiraaguSMSBroadCaster;
import com.isa.thinair.messaging.core.transport.LeSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.MagfaSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.MessageBroadcaster;
import com.isa.thinair.messaging.core.transport.NetCastSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.ResonSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.SmsBoxSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.SmsconnectSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.SyriatelSMSBroadcaster;
import com.isa.thinair.messaging.core.transport.VectraMindSMSBroadcaster;

/**
 * Class to Retrieve The SMS BroadCaster
 * 
 * @author Thushara
 * 
 */
public class SMSBroadCasterFactory {

	public static MessageBroadcaster getSMSBroadcaster() {
		MessageBroadcaster messageBroadCaster = null;
		MessagingModuleConfig mesgConfig = MessagingModuleUtils.getMessagingModuleConfig();
		String strBroadCaster = mesgConfig.getSmsGateway();
		if (strBroadCaster != null) {
			if (strBroadCaster.equals(MessagingCustomConstants.RESON_SMS_BROADCASTER)) {
				messageBroadCaster = new ResonSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.ALAYOUBI_SMS_BROADCASTER)) {
				messageBroadCaster = new AlayuobiSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.VECTRAMIND_SMS_BROADCASTER)) {
				messageBroadCaster = new VectraMindSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.LESMS_BROADCASTER)) {
				messageBroadCaster = new LeSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.SMSCONNECT_SMS_BROADCASTER)) {
				messageBroadCaster = new SmsconnectSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.MAGFA_SMS_BROADCASTER)) {
				messageBroadCaster = new MagfaSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.NETCAST_SMS_BROADCASTER)) {
				messageBroadCaster = new NetCastSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.AXON_SMS_BROADCASTER)) {
				messageBroadCaster = new AxonSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.SYRIATEL_SMS_BROADCASTER)) {
				messageBroadCaster = new SyriatelSMSBroadcaster();
			} else if (strBroadCaster.equals(MessagingCustomConstants.DHIRAAGU_SMS_BROADCASTER)) {
				messageBroadCaster = new DhiraaguSMSBroadCaster();
			} else if(strBroadCaster.equals(MessagingCustomConstants.SMSBOX_SMS_BROADCASTER)) {
				messageBroadCaster = new SmsBoxSMSBroadcaster();
			}
		}

		return messageBroadCaster;
	}

}
