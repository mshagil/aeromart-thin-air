package com.isa.thinair.messaging.core.transport;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.ucs.sms.reson8.client.SMSConnector;

public class ResonSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());
	private static final String SMS_CONFIG_NAME = "reson_sms";

	public void sendMessages(List messageList) {

		SMSConnector smsConn = null;
		try {
			smsConn = new SMSConnector();
			if (messageList != null && messageList.size() > 0) {
				send(smsConn, messageList);
			}
		} catch (Throwable e) {
			log.error(e);
		} finally {
			try {
				if (smsConn != null) {
					smsConn.disconnect();
				}
			} catch (Exception e) {
				log.error(e);
			}
		}

	}

	private void send(SMSConnector smsConn, List<SmsMessage> lstSmsMessage) {

		log.debug("Start ResonSMSBroadcaster::send ");

		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		Throwable xceptionToAudit = null;
		boolean success = true;
		String server = smsConfig.getSmsServer();
		String smsDbUser = smsConfig.getSmsDBName();
		String smsDbpassword = smsConfig.getSmsDBPassword();
		String smsUserName = smsConfig.getSmsUserName();
		String smsPassword = smsConfig.getSmsUserPassword();
		int smsMode = smsConfig.getSmsMode();

		boolean isConnected = smsConn.connect(server, smsDbUser, smsDbpassword, smsUserName, smsPassword);

		if (log.isDebugEnabled()) {
			log.debug("ResonSMSBroadcaster connected[" + isConnected + "] for server[" + server + "] " + lstSmsMessage.size()
					+ " messages to send ");
		}

		String smsText = null;
		String recipient = null;
		String smsHeader = null;
		String errorMessage = "";

		for (SmsMessage smsMessage : lstSmsMessage) {
			try {
				smsText = smsMessage.getBody();
				recipient = smsMessage.getRecipient();
				smsHeader = smsMessage.getSubject();
				recipient = recipient.replace("-", "");

				if (smsText.length() > 160)
					smsText = smsText.substring(0, 159);

				String msg;
				if (isConnected) {
					if (log.isDebugEnabled()) {
						log.debug("About to send a message for recipient[" + recipient + "]");
					}

					msg = smsConn.sendMessage(recipient, smsText, smsMode, smsHeader);

					if (log.isDebugEnabled()) {
						log.debug("About to send a message for recipient[" + recipient + "] status[" + msg + "]");
					}

					if (!msg.equals("0")) {
						if (log.isDebugEnabled()) {
							log.debug("SMS successfully sent. [message id=" + msg + ",recipient=" + recipient + ",smsText="
									+ smsText + "]");
						}
					} else {
						success = false;
						if (log.isErrorEnabled()) {
							log.error("Sending SMS failed. [error= " + smsConn.getErrorDescription() + ",recipient=" + recipient
									+ ",smsText=" + smsText + "]");
						}
						errorMessage = "Error sending SMS ";
						sendErrorAlert(errorMessage, messModConfig, errorMessage + " : " + smsConn.getErrorDescription());
					}
				} else {
					success = false;
					log.error("Connecting to SMS server failed. " + "[recipient=" + recipient + ",smsText=" + smsText + "]");
					errorMessage = "Cannot Connect to SMS server";
					sendErrorAlert(errorMessage, messModConfig, errorMessage + " : " + server);
				}

			} catch (Exception e) {
				xceptionToAudit = e;
				log.error("Sending SMS failed. [recipient=" + recipient + ",smsText=" + smsText + "]", e);
				errorMessage = "Exception in Sending SMS";
				sendErrorAlert(errorMessage, messModConfig, messModConfig + " : " + e);
			} finally {
				// smsConn.disconnect();
				// Performs auditing
				this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, success, smsMessage.getRecipient());
				if (smsMessage.getAuditInfo() != null)
					smsMessage.getAuditInfo().clear();
				updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, success);
			}
		}

		log.debug("End ResonSMSBroadcaster::send ");
	}

}
