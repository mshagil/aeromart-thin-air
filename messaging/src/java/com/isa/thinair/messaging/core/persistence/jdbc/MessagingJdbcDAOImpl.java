package com.isa.thinair.messaging.core.persistence.jdbc;

import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.messaging.api.dto.UpdateEmailStatusInfoDTO;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.persistence.dao.MessagingJdbcDAO;
import com.isa.thinair.messaging.core.persistence.util.MessageStatusUpdateStatementCreater;
import com.isa.thinair.messaging.core.util.EmailUtil;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class MessagingJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements MessagingJdbcDAO {

	private final Log log = LogFactory.getLog(MessagingJdbcDAOImpl.class);

	/**
	 * Saves the failed email message and update the noOfRetries and status for failed emails.
	 * 
	 * @param message
	 */
	public void saveEmail(EmailMessage message) {
		if (message.getId() == null) {
			String sqlInsert = "INSERT INTO T_MESSAGES_AUDIT(ID, TIME_STAMP, MSG_PARAMS, MSG_CONTENT, STATUS, NO_OF_RETRIES) VALUES(?, ?, ?, ?, ?, ?)";
			Object[] args = new Object[6];
			LobHandler lobHandler = new DefaultLobHandler();
			// args[3] = new SqlLobValue(EmailUtil.getMessageBody(message),
			// lobHandler);
			int result = 0;
			try {
				args[0] = getNextNumber();
				args[1] = new Date();
				args[2] = EmailUtil.getMessageParam(message);
				args[3] = new SqlLobValue(EmailUtil.getMessageBody(message).getBytes("UTF-8"), lobHandler);
				args[4] = "FAILED";
				args[5] = new Integer(0);
				int[] argTypes = new int[] { java.sql.Types.INTEGER, java.sql.Types.TIMESTAMP, Types.VARCHAR,
						java.sql.Types.BLOB, java.sql.Types.VARCHAR, java.sql.Types.INTEGER };
				JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
				result = jdbcTemplate.update(sqlInsert, args, argTypes);
				
				log.error("Record inserted to messages audit with ID = " + String.valueOf(args[0]));
			} catch (UnsupportedEncodingException e) {
				log.error("Error converting the message content when saving to blob " + e.getMessage());
			}
		} else {
			Integer messageId = new Integer(message.getId());
			int noOfRetries = getNoOfRetries(messageId).intValue();

			// get the maximum number of times to attempt from configureation
			// file.
			MessagingModuleConfig moduleConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
					MessagingConstants.MODULE_NAME);
			String maxAttempt = moduleConfig.getMaxAttempts();
			int maxNoOfAttempts = 1;
			if (maxAttempt != null) {
				maxNoOfAttempts = Integer.parseInt(maxAttempt);
			}
			if (noOfRetries <= maxNoOfAttempts) {
				String sqlUpdate = "UPDATE T_MESSAGES_AUDIT SET NO_OF_RETRIES = (NO_OF_RETRIES + 1) WHERE ID = " + messageId;
				String logMsg = "Increase NO_OF_RETRIES in T_MESSAGES_AUDIT of message id :" + messageId;
				if (noOfRetries == maxNoOfAttempts) {
					sqlUpdate = "UPDATE T_MESSAGES_AUDIT SET STATUS = 'INVALID' WHERE ID = " + messageId;
					logMsg = "STATUS made 'INVALID' in T_MESSAGES_AUDIT of message id :" + messageId;
				}
				JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
				jdbcTemplate.execute(sqlUpdate);
				log.error(logMsg);
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public List loadEmails() {
		String sqlSelect = "SELECT ID, MSG_PARAMS, MSG_CONTENT FROM T_MESSAGES_AUDIT WHERE STATUS = 'FAILED'";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (List) jdbcTemplate.query(sqlSelect, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List lstMessages = new ArrayList();
				EmailMessage message = null;
				Clob clob = null;
				Blob blob = null;
				String param = null;
				String content = null;
				int id = 0;
				if (rs != null) {
					while (rs.next()) {
						id = rs.getInt("ID");
						param = rs.getString("MSG_PARAMS");
						// clob = rs.getClob("MSG_CONTENT");
						// content = clob.getSubString(1, (int) clob.length());
						// message = EmailUtil.getEmailMessage(id, param,
						// content);
						blob = rs.getBlob("MSG_CONTENT");
						byte[] test = blob.getBytes(1, (int) blob.length());
						message = EmailUtil.getEmailMessageNew(id, param, test);
						lstMessages.add(message);
					}
				}
				return lstMessages;
			}
		});
	}
	
	public boolean isMessageExists(Integer id) {
		String sqlSelect = "SELECT ID FROM T_MESSAGES_AUDIT WHERE ID = " + id;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Integer recs = (Integer) jdbcTemplate.query(sqlSelect, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("ID"));
				}
				return null;
			}
		});
		if (recs != null && recs.intValue() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Deletes the successfuly sent emails.
	 * 
	 * @param id
	 */
	public void deleteEmail(Integer id) {
		String sqlDelete = "DELETE FROM T_MESSAGES_AUDIT WHERE ID = " + id;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		jdbcTemplate.execute(sqlDelete);
	}

	/**
	 * Returns the next number
	 * 
	 * @return
	 */
	private Integer getNextNumber() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = "SELECT S_MESSAGES_AUDIT.NEXTVAL nextVal FROM DUAL";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Integer nextVal = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int val = 0;
				if (rs != null) {
					if (rs.next()) {
						val = rs.getInt(1);
					}
				}
				return new Integer(val);
			}
		});
		return nextVal;
	}

	/**
	 * Returns the number of retries for a particular message id.
	 * 
	 * @param id
	 * @return
	 */
	private Integer getNoOfRetries(Integer id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = "SELECT NO_OF_RETRIES FROM T_MESSAGES_AUDIT WHERE ID = " + id;

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		Integer retries = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int val = 0;
				if (rs != null) {
					if (rs.next()) {
						val = rs.getInt(1);
					}
				}
				return new Integer(val);
			}
		});
		return retries;
	}

	/**
	 * Updates the email sending status.
	 * 
	 * @author lalanthi
	 */
	public void updatePaxSegmentSSREmailSentStatus(UpdateEmailStatusInfoDTO updateInfoDTO) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String sql = new MessageStatusUpdateStatementCreater().getPaxSegmentSSREmailUpdateQuery(updateInfoDTO);
		template.execute(sql);
	}
}
