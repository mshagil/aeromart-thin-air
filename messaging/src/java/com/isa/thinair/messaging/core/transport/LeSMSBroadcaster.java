package com.isa.thinair.messaging.core.transport;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class LeSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());
	private final String SMS_CONFIG_NAME = "le_sms";
	private String url;
	private StringBuilder smsText = new StringBuilder();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.messaging.core.transport.MessageBroadcaster#sendMessages(java.util.List)
	 */
	@Override
	public void sendMessages(List messageList) {

		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		url = buildGetRequest(smsConfig);

		if (messageList != null) {
			for (Iterator iterator = messageList.iterator(); iterator.hasNext();) {
				SmsMessage smsMessage = (SmsMessage) iterator.next();
				// Send SMS
				sendSMS(smsConfig, smsMessage);
			}
		}
	}

	private void sendSMS(SmsConfig smsConfig, SmsMessage smsMessage) {
		String smsUrl = null;
		// Create an instance of HttpClient.
		HttpClient client = new HttpClient();
		Throwable xceptionToAudit = null;

		// If proxy enabled go through proxy
		if (smsConfig.isProxyEnable()) {
			HostConfiguration hcon = new HostConfiguration();
			hcon.setProxy(smsConfig.getProxyServer(), smsConfig.getProxyPort());
			client.setHostConfiguration(hcon);
		}

		smsUrl = url + getMessage(smsMessage);
		if (log.isDebugEnabled()) {
			log.info("[LeSMSBroadcaster] Compile SMS URL :" + smsUrl);
		}

		// Create a method instance.
		GetMethod method = new GetMethod(smsUrl);

		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

		try {
			log.info("[LeSMSBroadcaster] Sendign SMSss to :" + smsMessage.getRecipient() + "   message:" + smsMessage.getBody());
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				log.info("[LeSMSBroadcaster] Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();

			// Deal with the response.
			log.info("[LeSMSBroadcaster] SMS Sent " + new String(responseBody) + ":"
					+ responseDescription(new String(responseBody)));
			// TODO : Remove this part. jst to test
			if (AppSysParamsUtil.isTestStstem()) {
				smsText.append("Recipient : ").append(buildMobileNumber(smsMessage.getRecipient())).append("\n")
						.append("Message : ").append(smsMessage.getBody()).append("\n").append("Response : ")
						.append(responseDescription(new String(responseBody)));
				ShowDialogBox db = new ShowDialogBox();
			}

		} catch (HttpException e) {
			xceptionToAudit = e;
			log.error("[LeSMSBroadcaster]Fatal protocol violation: ", e);
		} catch (IOException e) {
			xceptionToAudit = e;
			log.error("[LeSMSBroadcaster]Fatal transport error: ", e);
		} catch (Exception e) {
			xceptionToAudit = e;
			log.error("[LeSMSBroadcaster]Fatal transport error: ", e);
		} finally {
			// Perform Auditing
			this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, true, smsMessage.getRecipient());
			if (smsMessage.getAuditInfo() != null) {
				smsMessage.getAuditInfo().clear();
			}
			this.updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, true);

			// Release the connection.
			method.releaseConnection();
		}
	}

	private String getMessage(SmsMessage smsMessage) {
		StringBuilder smsDetails = new StringBuilder();
		String smsText = smsMessage.getBody();
		if (smsText.length() > 160) {
			smsText = smsText.substring(0, 159);
		}
		smsDetails.append("&numero=" + buildMobileNumber(smsMessage.getRecipient()));
		smsDetails.append("&message=" + smsText);
		if (AppSysParamsUtil.isTestStstem()) {
			smsDetails.append("&test=o");
		}
		return smsDetails.toString();
	}

	private String buildGetRequest(SmsConfig smsConfig) {
		StringBuilder smsURL = new StringBuilder();

		smsURL.append(smsConfig.getSmsServer());
		smsURL.append("?email=" + smsConfig.getSmsUserName());
		smsURL.append("&pass=" + smsConfig.getSmsUserPassword());
		return smsURL.toString();
	}

	private String buildMobileNumber(String mobileNo) {
		return mobileNo.replaceAll("-", "");
	}

	private String responseDescription(String responseCode) {
		String result = null;
		// check if a two digit response code
		if (responseCode.length() == 2) {
			int input = Integer.parseInt(responseCode);

			switch (input) {
			case 80:
				result = "the message was sent";
				break;
			case 81:
				result = "the message is stored for a delayed transmission";
				break;
			case 82:
				result = "the username or password is invalid";
				break;
			case 83:
				result = "you must credit the account";
				break;
			case 84:
				result = "the mobile number is not valid";
				break;
			case 85:
				result = "the delayed transmission format is not valid";
				break;
			case 86:
				result = "the group of contacts is empty";
				break;
			case 87:
				result = "email value is empty";
				break;
			case 88:
				result = "pass the value is empty";
				break;
			case 89:
				result = "the number value is empty";
				break;
			case 90:
				result = "the value message is empty";
				break;
			case 91:
				result = "the message has been sent to this number today";
				break;
			case 92:
				result = "the test message was sent";
				break;
			case 93:
				result = "to make sending SMS to the overseas territories, you must activate the option (14) in the customer area";
				break;
			case 94:
				result = "DIFFERED SENDING IS CANCELLED your shipment is delayed removed";
				break;
			case 95:
				result = "CAN NOT BE CANCELLED DIFFERED SENDING delayed your shipment could not be deleted";
				break;
			case 96:
				result = "INVALID IP ADDRESS Your IP address is not valid";
				break;
			default:
				result = "Unable to be determined.";
			}
			return result;
		} else {
			return "No Value Returned.";
		}
	}

	public class ShowDialogBox {
		JFrame frame;

		public ShowDialogBox() {
			frame = new JFrame("Show SMS Message to test");
			JButton button = new JButton("Show SMS");
			button.addActionListener(new MyAction());
			frame.add(button);
			frame.setSize(400, 400);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		}

		public class MyAction implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frame, smsText.toString());
			}
		}
	}
}
