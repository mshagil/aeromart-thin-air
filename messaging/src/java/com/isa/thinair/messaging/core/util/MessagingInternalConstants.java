/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.util;

public abstract class MessagingInternalConstants {

	public static interface DAOProxyNames {
		public static String MESSAGING_JDBC_DAO = "messagingJdbcDAO";
	}
}