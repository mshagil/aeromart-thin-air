package com.isa.thinair.messaging.core.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.core.processor.MessageManager;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;


public class SendMessage extends PlatformTestCase {

	public void TestSendMessage(){
		MessageProfile profile= new MessageProfile();
		
		UserMessaging user=new UserMessaging();
		user.setFirstName("Mohamed");
		user.setLastName("Nasly");
		user.setToAddres("naslyy@gmail.com");
		List messageList= new ArrayList();
		messageList.add(user);
		profile.setUserMessagings(messageList);
		
		Topic topic= new Topic();
		HashMap map = new HashMap();
		map.put("user", user);
		
		topic.setTopicParams(map);
		topic.setTopicName("itinerary");
        topic.setContentType(EmailMessage.CONTENT_TYPE_HTML);//current impl only support html
        topic.setMailServerConfigurationName("default");
        Locale locale = new Locale("ar");
        
        topic.setLocale(locale);
//        topic.addAttachmentFileName("netmanagement_output.txt");
		profile.setTopic(topic);
		
		List list= new ArrayList();
		list.add(profile);
		new MessageManager().processMessages(list);
	}
	
}
