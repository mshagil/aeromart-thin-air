/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.remoting.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.core.controller.ModuleFramework;


/**
 * @author Srikantha
 *
 */
public class TestMessagingMDBThread {
        
    public static void main(String testSendMessagesThread[]){
        ModuleFramework.startup();
        MessagingThread thread = new MessagingThread();
        thread.start();
    }        
}

class MessagingThread extends Thread {
        private List createMails() {
            ArrayList list= new ArrayList();
            for (int i = 0; i < 100; i++) {
                System.out.println("=============================");
                System.out.println("Running the thread["+ i +"] to MessagingMDB test...");
                MessageProfile profile= new MessageProfile();
               
                UserMessaging user=new UserMessaging();
                user.setFirstName("Srikanth");
                user.setLastName("Kanagaratnam");
                user.setToAddres("srikanth@jkcsworld.com");
                
                List messageList= new ArrayList();
                messageList.add(user);
                profile.setUserMessagings(messageList);
    
                Topic topic= new Topic();
                HashMap map = new HashMap();
                map.put("user", user);
                
                topic.setTopicParams(map);
                topic.setTopicName("dst_change");
                topic.setContentType(EmailMessage.CONTENT_TYPE_HTML);//current impl only support html
                topic.setMailServerConfigurationName("mailserver_sita.1");
                topic.addAttachmentFileName("c:/netmanagement_output.txt");
                
                profile.setTopic(topic);

                list.add(profile);
            }
            return list;
        } 
        
        public void run() {
            System.out.println("Calling the MessagingMDB service...");
            getMessagingService().sendMessages(createMails());  
            System.out.println("=============================");
            
        }
        private MessagingServiceBD getMessagingService(){
            return (MessagingServiceBD) MessagingModuleUtils.getInstance()
                .lookupServiceBDSpecifyingFullBDKey(MessagingConstants.MODULE_NAME, 
                        MessagingConstants.FullBDKeys.MESSAGING_SERVICE_REMOTE);
        }
}
