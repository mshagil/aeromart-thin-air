package com.isa.thinair.airadmin.api.flight;

import java.io.Serializable;

public class FlightSeatMvSummaryDTO  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String agentName;
	
	private String soldSeats;
	
	private String onholdSeats;
	
	private String cancelledSeats;
	
	private String totalSold;
	
	private String totalOnhold;
	
	private String totalCancelled;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(String soldSeats) {
		this.soldSeats = soldSeats;
	}

	public String getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(String onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public String getCancelledSeats() {
		return cancelledSeats;
	}

	public void setCancelledSeats(String cancelledSeats) {
		this.cancelledSeats = cancelledSeats;
	}

	public String getTotalSold() {
		return totalSold;
	}

	public void setTotalSold(String totalSold) {
		this.totalSold = totalSold;
	}

	public String getTotalOnhold() {
		return totalOnhold;
	}

	public void setTotalOnhold(String totalOnhold) {
		this.totalOnhold = totalOnhold;
	}

	public String getTotalCancelled() {
		return totalCancelled;
	}

	public void setTotalCancelled(String totalCancelled) {
		this.totalCancelled = totalCancelled;
	}

}
