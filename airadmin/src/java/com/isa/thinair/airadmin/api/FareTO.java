package com.isa.thinair.airadmin.api;


public class FareTO {

	private Integer fareID;

	private Integer fareRuleID;

	private String fareRuleCode;

	private String fareBasisCode;

	private String effectiveFromDate;

	private String effectiveToDate;

	private double fareAmount;

	private double fareAmountLocal;

	private String status;

	private String ondCode;

	private String bookingCode;

	private String childFareType;

	private double childFare;

	private double childFareLocal;

	private String infantFareType;

	private double infantFare;

	private double infantFareLocal;

	private String salesEffectiveFrom;

	private String salesEffectiveTo;

	private String returnEffectiveFromDate;

	private String returnEffectiveToDate;

	private String via1;

	private String via2;

	private String via3;

	private String via4;

	private String origin;

	private String destination;

	private Integer version;

	private String modifyToSameFare;

	private String enableLinkFare;

	private String currencyCode;

	private String masterFareRefCode;

	private String createReturnFare;

	private String returnFlag;

	private String fareDefType;

	public String getFareDefType() {
		return fareDefType;
	}

	public void setFareDefType(String fareDefType) {
		this.fareDefType = fareDefType;
	}

	public Integer getFareID() {
		return fareID;
	}

	public void setFareID(Integer fareID) {
		this.fareID = fareID;
	}

	public Integer getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(Integer fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public double getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
		if (ondCode != null) {
			String[] ondArr = ondCode.split("/");
			origin = ondArr[0];
			destination = ondArr[1];
		}
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getChildFareType() {
		return childFareType;
	}

	public void setChildFareType(String childFareType) {
		this.childFareType = childFareType;
	}

	public double getChildFare() {
		return childFare;
	}

	public void setChildFare(double childFare) {
		this.childFare = childFare;
	}

	public String getInfantFareType() {
		return infantFareType;
	}

	public void setInfantFareType(String infantFareType) {
		this.infantFareType = infantFareType;
	}

	public double getInfantFare() {
		return infantFare;
	}

	public void setInfantFare(double infantFare) {
		this.infantFare = infantFare;
	}

	public String getSalesEffectiveFrom() {
		return salesEffectiveFrom;
	}

	public void setSalesEffectiveFrom(String salesEffectiveFrom) {
		this.salesEffectiveFrom = salesEffectiveFrom;
	}

	public String getSalesEffectiveTo() {
		return salesEffectiveTo;
	}

	public void setSalesEffectiveTo(String salesEffectiveTo) {
		this.salesEffectiveTo = salesEffectiveTo;
	}

	public String getReturnEffectiveFromDate() {
		return returnEffectiveFromDate;
	}

	public void setReturnEffectiveFromDate(String returnEffectiveFromDate) {
		this.returnEffectiveFromDate = returnEffectiveFromDate;
	}

	public String getReturnEffectiveToDate() {
		return returnEffectiveToDate;
	}

	public void setReturnEffectiveToDate(String returnEffectiveToDate) {
		this.returnEffectiveToDate = returnEffectiveToDate;
	}

	public String getVia1() {
		return via1;
	}

	public void setVia1(String via1) {
		this.via1 = via1;
	}

	public String getVia2() {
		return via2;
	}

	public void setVia2(String via2) {
		this.via2 = via2;
	}

	public String getVia3() {
		return via3;
	}

	public void setVia3(String via3) {
		this.via3 = via3;
	}

	public String getVia4() {
		return via4;
	}

	public void setVia4(String via4) {
		this.via4 = via4;
	}

	public String getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public String getEffectiveFromDate() {
		return effectiveFromDate;
	}

	public void setEffectiveFromDate(String effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public String getEffectiveToDate() {
		return effectiveToDate;
	}

	public void setEffectiveToDate(String effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getModifyToSameFare() {
		return modifyToSameFare;
	}

	public void setModifyToSameFare(String modifyToSameFare) {
		this.modifyToSameFare = modifyToSameFare;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public double getChildFareLocal() {
		return childFareLocal;
	}

	public void setChildFareLocal(double childFareLocal) {
		this.childFareLocal = childFareLocal;
	}

	public double getInfantFareLocal() {
		return infantFareLocal;
	}

	public void setInfantFareLocal(double infantFareLocal) {
		this.infantFareLocal = infantFareLocal;
	}

	public double getFareAmountLocal() {
		return fareAmountLocal;
	}

	public void setFareAmountLocal(double fareAmountLocal) {
		this.fareAmountLocal = fareAmountLocal;
	}

	public String getEnableLinkFare() {
		return enableLinkFare;
	}

	public void setEnableLinkFare(String enableLinkFare) {
		this.enableLinkFare = enableLinkFare;
	}

	public String getMasterFareRefCode() {
		return masterFareRefCode;
	}

	public void setMasterFareRefCode(String masterFareRefCode) {
		this.masterFareRefCode = masterFareRefCode;
	}

	public String getCreateReturnFare() {
		return createReturnFare;
	}

	public void setCreateReturnFare(String createReturnFare) {
		this.createReturnFare = createReturnFare;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

}
