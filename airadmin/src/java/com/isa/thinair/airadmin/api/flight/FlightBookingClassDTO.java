package com.isa.thinair.airadmin.api.flight;

import java.io.Serializable;

public class FlightBookingClassDTO implements Serializable {
	
	private static final long serialVersionUID = 7189836040371391136L;
	
	private String bookingClass;
	
	private String rank;
	
	private char fixed;
	
	private char priority;
	
	private int allocation;
	
	private int sold;
	
	private int onhold;
	
	private int cancelled;
	
	private int aquired;
	
	private int available;
	
	private char closed;
	
	private int seatsSoldAquiredByNesting;
	
	private int seatsSoldNested;
	
	private int seatsOnHoldAquiredByNesting;
	
	private int seatsOnHoldNested;
	
	private String fareFareRules;
	
	private int agents;

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public char getFixed() {
		return fixed;
	}

	public void setFixed(char fixed) {
		this.fixed = fixed;
	}

	public char getPriority() {
		return priority;
	}

	public void setPriority(char priority) {
		this.priority = priority;
	}

	public int getAllocation() {
		return allocation;
	}

	public void setAllocation(int allocation) {
		this.allocation = allocation;
	}

	public int getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold = sold;
	}

	public int getOnhold() {
		return onhold;
	}

	public void setOnhold(int onhold) {
		this.onhold = onhold;
	}

	public int getCancelled() {
		return cancelled;
	}

	public void setCancelled(int cancelled) {
		this.cancelled = cancelled;
	}

	public int getAquired() {
		return aquired;
	}

	public void setAquired(int aquired) {
		this.aquired = aquired;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}	

	public char getClosed() {
		return closed;
	}

	public void setClosed(char closed) {
		this.closed = closed;
	}

	public int getSeatsSoldAquiredByNesting() {
		return seatsSoldAquiredByNesting;
	}

	public void setSeatsSoldAquiredByNesting(int seatsSoldAquiredByNesting) {
		this.seatsSoldAquiredByNesting = seatsSoldAquiredByNesting;
	}

	public int getSeatsSoldNested() {
		return seatsSoldNested;
	}

	public void setSeatsSoldNested(int seatsSoldNested) {
		this.seatsSoldNested = seatsSoldNested;
	}

	public int getSeatsOnHoldAquiredByNesting() {
		return seatsOnHoldAquiredByNesting;
	}

	public void setSeatsOnHoldAquiredByNesting(int seatsOnHoldAquiredByNesting) {
		this.seatsOnHoldAquiredByNesting = seatsOnHoldAquiredByNesting;
	}

	public int getSeatsOnHoldNested() {
		return seatsOnHoldNested;
	}

	public void setSeatsOnHoldNested(int seatsOnHoldNested) {
		this.seatsOnHoldNested = seatsOnHoldNested;
	}

	public String getFareFareRules() {
		return fareFareRules;
	}

	public void setFareFareRules(String fareFareRules) {
		this.fareFareRules = fareFareRules;
	}

	public int getAgents() {
		return agents;
	}

	public void setAgents(int agents) {
		this.agents = agents;
	}	

}
