package com.isa.thinair.airadmin.api;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.InboundDepartureFrequency;
import com.isa.thinair.airpricing.api.model.OutboundDepartureFrequency;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class FareRuleDTO {

	public static String NO = "N";

	public static String DATA_SEPARATOR = ":";

	private String fareRuleCode;

	private String description;

	private String status;

	private String advanceBookingDays;

	private String returnFlag = NO;

	private String halfReturnFlag = NO;

	private String minimumStayOver;

	private Long minimumStayoverMins;

	private Long minimumStayoverMonths;

	private String maximumStayOver;

	private Long maximumStayoverMins;

	private Long maximumStayoverMonths;

	private String outboundDepatureFreqencyString;

	private OutboundDepartureFrequency outboundDepatureFreqency;

	private String inboundDepatureFrequencyString;

	private InboundDepartureFrequency inboundDepatureFrequency;

	private String outboundDepatureTimeFrom;

	private String outboundDepatureTimeTo;

	private String inboundDepatureTimeFrom;

	private String inboundDepatureTimeTo;

	private String rulesComments;

	private String agentComments;

	private String agentCommissionType;

	private String agentCommissionAmount;

	private String agentCommissionAmountLocal;

	private String fareBasisCode;

	private String paxCategoryCode;

	private String fareCategoryCode;

	private String intnModifyBufferTime;

	private String domModifyBufferTime;

	private String printExpDate = NO;

	private String OpenReturnFlag = NO;

	private String openRetConfPeriod;

	private Long openRetConfPeriodMins;

	private Long openRenConfPeriodMonths;

	private String flexiCodes;

	private Set<Integer> flexiCodesCol;

	private String modifyByDate = NO;

	private String modifyByRoute = NO;

	private Integer loadFactorMin;

	private Integer loadFactorMax;

	private String visibleChannelIds;

	private Set<Integer> visibleChannelCol;

	private String visibileAgentCodes;

	private Set<String> visibileAgentCol;

	private FareRulePax adPax;

	private FareRulePax chPax;

	private FareRulePax inPax;

	private Long version;

	private Integer fareRuleID;

	private String paxApplicabilityJSON;

	private String modificationChargeAmountStr;

	private String cancellationChargeAmountStr;

	private String modificationChargeType;

	private String cancellationChargeType;

	private String cancellationChargeLocalStr;

	private String modificationChargeLocalStr;

	private String nameChangeChargeType;

	private String nameChangeChargeAmountStr;

	private String nameChangeChargeAmountLocalStr;

	private String minCnxChargeAmountStr;

	private String maxCnxChargeAmountStr;

	private String minModChargeAmountStr;

	private String maxModChargeAmountStr;

	private String minNCCAmountStr;

	private String maxNCCAmountStr;

	private String localCurrency;

	private String onlyUpdateComments;

	private String minFareDiscountStr;

	private String maxFareDiscountStr;

	private String visiblePOSStr;

	private Set<String> visiblePOS;

	private Boolean applicableToCountries;
	
	private String bulkTicket;
	
	private Integer minBulkTicketSeatCount;

	public FareRuleDTO() {
		adPax = new FareRulePax();
		adPax.setPaxType(PaxTypeTO.ADULT);
		chPax = new FareRulePax();
		chPax.setPaxType(PaxTypeTO.CHILD);
		inPax = new FareRulePax();
		inPax.setPaxType(PaxTypeTO.INFANT);
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdvanceBookingDays() {
		return advanceBookingDays;
	}

	public void setAdvanceBookingDays(String advanceBookingDays) {
		this.advanceBookingDays = advanceBookingDays;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getHalfReturnFlag() {
		return halfReturnFlag;
	}

	public void setHalfReturnFlag(String halfReturnFlag) {
		this.halfReturnFlag = halfReturnFlag;
	}

	public String getMinimumStayOver() {
		return minimumStayOver;
	}

	public void setMinimumStayOver(String minimumStayOver) {
		this.minimumStayOver = minimumStayOver;
		if (minimumStayOver != null) {
			setMinimumStayoverMins(createMinutes(minimumStayOver));
			setMinimumStayoverMonths(createMonths(minimumStayOver));
		}
	}

	public Long getMinimumStayoverMins() {
		return minimumStayoverMins;
	}

	private void setMinimumStayoverMins(Long minimumStayoverMins) {
		this.minimumStayoverMins = minimumStayoverMins;
	}

	public Long getMinimumStayoverMonths() {
		return minimumStayoverMonths;
	}

	private void setMinimumStayoverMonths(Long minimumStayoverMonths) {
		this.minimumStayoverMonths = minimumStayoverMonths;
	}

	public String getMaximumStayOver() {
		return maximumStayOver;
	}

	public void setMaximumStayOver(String maximumStayOver) {
		this.maximumStayOver = maximumStayOver;
		if (maximumStayOver != null) {
			setMaximumStayoverMins(createMinutes(maximumStayOver));
			setMaximumStayoverMonths(createMonths(maximumStayOver));
		}
	}

	public Long getMaximumStayoverMins() {
		return maximumStayoverMins;
	}

	private void setMaximumStayoverMins(Long maximumStayoverMins) {
		this.maximumStayoverMins = maximumStayoverMins;
	}

	public Long getMaximumStayoverMonths() {
		return maximumStayoverMonths;
	}

	private void setMaximumStayoverMonths(Long maximumStayoverMonths) {
		this.maximumStayoverMonths = maximumStayoverMonths;
	}

	public String getOutboundDepatureFreqencyString() {
		return outboundDepatureFreqencyString;
	}

	public void setOutboundDepatureFreqencyString(String outboundDepatureFreqencyString) {
		this.outboundDepatureFreqencyString = outboundDepatureFreqencyString;

		if (outboundDepatureFreqencyString != null) {
			String frequery[] = outboundDepatureFreqencyString.split(DATA_SEPARATOR);
			outboundDepatureFreqency = new OutboundDepartureFrequency();
			outboundDepatureFreqency.setDay0(Integer.parseInt(frequery[0]));
			outboundDepatureFreqency.setDay1(Integer.parseInt(frequery[1]));
			outboundDepatureFreqency.setDay2(Integer.parseInt(frequery[2]));
			outboundDepatureFreqency.setDay3(Integer.parseInt(frequery[3]));
			outboundDepatureFreqency.setDay4(Integer.parseInt(frequery[4]));
			outboundDepatureFreqency.setDay5(Integer.parseInt(frequery[5]));
			outboundDepatureFreqency.setDay6(Integer.parseInt(frequery[6]));
		}
	}

	public OutboundDepartureFrequency getOutboundDepatureFreqency() {
		return outboundDepatureFreqency;
	}

	public String getInboundDepatureFrequencyString() {
		return inboundDepatureFrequencyString;
	}

	public void setInboundDepatureFrequencyString(String inboundDepatureFrequencyString) {
		this.inboundDepatureFrequencyString = inboundDepatureFrequencyString;
		if (inboundDepatureFrequencyString != null) {
			inboundDepatureFrequency = new InboundDepartureFrequency();
			String frequency[] = inboundDepatureFrequencyString.split(DATA_SEPARATOR);
			inboundDepatureFrequency.setDay0(Integer.parseInt(frequency[0]));
			inboundDepatureFrequency.setDay1(Integer.parseInt(frequency[1]));
			inboundDepatureFrequency.setDay2(Integer.parseInt(frequency[2]));
			inboundDepatureFrequency.setDay3(Integer.parseInt(frequency[3]));
			inboundDepatureFrequency.setDay4(Integer.parseInt(frequency[4]));
			inboundDepatureFrequency.setDay5(Integer.parseInt(frequency[5]));
			inboundDepatureFrequency.setDay6(Integer.parseInt(frequency[6]));
		}
	}

	public InboundDepartureFrequency getInboundDepatureFrequency() {
		return inboundDepatureFrequency;
	}

	public String getOutboundDepatureTimeFrom() {
		return outboundDepatureTimeFrom;
	}

	public void setOutboundDepatureTimeFrom(String outboundDepatureTimeFrom) {
		this.outboundDepatureTimeFrom = outboundDepatureTimeFrom;
		// SimpleDateFormat dateForamt = new SimpleDateFormat("HH:mm:ss");

	}

	public String getOutboundDepatureTimeTo() {
		return outboundDepatureTimeTo;
	}

	public void setOutboundDepatureTimeTo(String outboundDepatureTimeTo) {
		this.outboundDepatureTimeTo = outboundDepatureTimeTo;
	}

	public String getInboundDepatureTimeFrom() {
		return inboundDepatureTimeFrom;
	}

	public void setInboundDepatureTimeFrom(String inboundDepatureTimeFrom) {
		this.inboundDepatureTimeFrom = inboundDepatureTimeFrom;
	}

	public String getInboundDepatureTimeTo() {
		return inboundDepatureTimeTo;
	}

	public void setInboundDepatureTimeTo(String inboundDepatureTimeTo) {
		this.inboundDepatureTimeTo = inboundDepatureTimeTo;
	}

	public String getRulesComments() {
		return rulesComments;
	}

	public void setRulesComments(String rulesComments) {
		this.rulesComments = rulesComments;
	}

	public String getAgentComments() {
		return agentComments;
	}

	public void setAgentComments(String agentComments) {
		this.agentComments = agentComments;
	}

	public String getAgentCommissionType() {
		return agentCommissionType;
	}

	public void setAgentCommissionType(String agentCommissionType) {
		this.agentCommissionType = agentCommissionType;
	}

	public String getAgentCommissionAmount() {
		return agentCommissionAmount;
	}

	public void setAgentCommissionAmount(String agentCommissionAmount) {
		this.agentCommissionAmount = agentCommissionAmount;
	}

	public String getAgentCommissionAmountLocal() {
		return agentCommissionAmountLocal;
	}

	public void setAgentCommissionAmountLocal(String agentCommissionAmountLocal) {
		this.agentCommissionAmountLocal = agentCommissionAmountLocal;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
		if (fareBasisCode != null)
			this.fareBasisCode = fareBasisCode.toUpperCase();
	}

	public String getPaxCategoryCode() {
		return paxCategoryCode;
	}

	public void setPaxCategoryCode(String paxCategoryCode) {
		this.paxCategoryCode = paxCategoryCode;
	}

	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	public String getIntnModifyBufferTime() {
		return intnModifyBufferTime;
	}

	public void setIntnModifyBufferTime(String intnModifyBufferTime) {
		if (intnModifyBufferTime != null && !intnModifyBufferTime.isEmpty()) {
			this.intnModifyBufferTime = getFormatedModBuffer(intnModifyBufferTime);
		}
	}

	public String getDomModifyBufferTime() {
		return domModifyBufferTime;
	}

	public void setDomModifyBufferTime(String domModifyBufferTime) {
		if (domModifyBufferTime != null && !domModifyBufferTime.isEmpty()) {
			this.domModifyBufferTime = getFormatedModBuffer(domModifyBufferTime);
		}
	}

	public String getPrintExpDate() {
		return printExpDate;
	}

	public void setPrintExpDate(String printExpDate) {
		this.printExpDate = printExpDate;
	}

	public String getOpenReturnFlag() {
		return OpenReturnFlag;
	}

	public void setOpenReturnFlag(String openReturnFlag) {
		OpenReturnFlag = openReturnFlag;
	}

	public String getOpenRetConfPeriod() {
		return openRetConfPeriod;
	}

	public void setOpenRetConfPeriod(String openRetConfPeriod) {
		this.openRetConfPeriod = openRetConfPeriod;
		if (openRetConfPeriod != null) {
			setOpenRetConfPeriodMins(createMinutes(openRetConfPeriod));
			setOpenRenConfPeriodMonths(createMonths(openRetConfPeriod));
		}
	}

	public Long getOpenRetConfPeriodMins() {
		return openRetConfPeriodMins;
	}

	private void setOpenRetConfPeriodMins(Long openRetConfPeriodMins) {
		this.openRetConfPeriodMins = openRetConfPeriodMins;
	}

	public Long getOpenRenConfPeriodMonths() {
		return openRenConfPeriodMonths;
	}

	private void setOpenRenConfPeriodMonths(Long openRenConfPeriodMonths) {
		this.openRenConfPeriodMonths = openRenConfPeriodMonths;
	}

	public String getFlexiCodes() {
		return flexiCodes;
	}

	public void setFlexiCodes(String flexiCodes) {
		this.flexiCodes = flexiCodes;
		if (flexiCodes != null && !flexiCodes.trim().isEmpty() && !"-1".equals(flexiCodes)) {
			if (flexiCodesCol == null) {
				flexiCodesCol = new HashSet<Integer>();
			}
			StringTokenizer strTok = new StringTokenizer(flexiCodes, ",");
			while (strTok.hasMoreTokens()) {
				flexiCodesCol.add(Integer.parseInt(strTok.nextToken().trim()));
			}
			setFlexiCodesCol(flexiCodesCol);
		}
	}

	public String getModifyByDate() {
		return modifyByDate;
	}

	public void setModifyByDate(String modifyByDate) {
		this.modifyByDate = modifyByDate;
	}

	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	public String getModifyByRoute() {
		return modifyByRoute;
	}

	public void setModifyByRoute(String modifyByRoute) {
		this.modifyByRoute = modifyByRoute;
	}

	public Integer getLoadFactorMin() {
		return loadFactorMin;
	}

	public void setLoadFactorMin(Integer loadFactorMin) {
		this.loadFactorMin = loadFactorMin;
	}

	public Integer getLoadFactorMax() {
		return loadFactorMax;
	}

	public void setLoadFactorMax(Integer loadFactorMax) {
		this.loadFactorMax = loadFactorMax;
	}

	public void setVisibleChannelIds(String visibleChannelIds) {
		this.visibleChannelIds = visibleChannelIds;
		if (visibleChannelIds != null && !visibleChannelIds.trim().isEmpty()) {
			if (visibleChannelCol == null) {
				visibleChannelCol = new HashSet<Integer>();
			}
			String arrVisibility[] = null;
			if (visibleChannelIds.indexOf(",") != -1) {
				arrVisibility = visibleChannelIds.split(",");
			} else {
				arrVisibility = new String[1];
				arrVisibility[0] = visibleChannelIds;
			}
			for (int t = 0; t < arrVisibility.length; t++) {
				if (arrVisibility[t] != null && !arrVisibility[t].equals("")) {
					visibleChannelCol.add(new Integer(Integer.parseInt(arrVisibility[t])));
				}
			}
			setVisibleChannelCol(visibleChannelCol);
		}
	}

	public String getVisibileAgentCodes() {
		return visibileAgentCodes;
	}

	public void setVisibileAgentCodes(String visibileAgentCodes) {
		this.visibileAgentCodes = visibileAgentCodes;
		if (visibileAgentCodes != null && !visibileAgentCodes.trim().isEmpty()) {
			if (visibileAgentCol == null) {
				visibileAgentCol = new HashSet<String>();
			}
			String arrAgents[] = null;
			if (visibileAgentCodes.indexOf(",") != -1) {
				arrAgents = visibileAgentCodes.split(",");
			} else {
				arrAgents = new String[1];
				arrAgents[0] = visibileAgentCodes;
			}

			for (int t = 0; t < arrAgents.length; t++) {
				if (arrAgents[t] != null && !arrAgents[t].equals("")) {
					visibileAgentCol.add(new String(arrAgents[t]));
				}
			}
			setVisibileAgentCol(visibileAgentCol);
		}

	}

	private long createMinutes(String durationString) {
		// Assume format is (MM : DD : HR : MI) from front end
		String[] durationArray = durationString.split(DATA_SEPARATOR);
		long minutes = 0;
		if (durationArray.length == 2) {
			if (!durationArray[1].equals("") && !durationArray[1].equals("0")) {
				minutes += Integer.parseInt(durationArray[1]) * 24 * 60;
			}
		} else if (durationArray.length == 3) {
			if (!durationArray[1].equals("") && !durationArray[1].equals("0")) {
				minutes += Integer.parseInt(durationArray[1]) * 24 * 60;
			}
			if (!durationArray[2].equals("") && !durationArray[2].equals("0")) {
				minutes += Integer.parseInt(durationArray[2]) * 60;
			}
		} else if (durationArray.length == 4) {
			if (!durationArray[1].equals("") && !durationArray[1].equals("0")) {
				minutes += Integer.parseInt(durationArray[1]) * 24 * 60;
			}
			if (!durationArray[2].equals("") && !durationArray[2].equals("0")) {
				minutes += Integer.parseInt(durationArray[2]) * 60;
			}
			if (!durationArray[3].equals("") && !durationArray[3].equals("0")) {
				minutes += Integer.parseInt(durationArray[3]);
			}
		}

		return minutes;
	}

	private long createMonths(String durationString) {
		// Assume format is (MM : DD : HR : MI) from front end
		String[] durationArray = durationString.split(DATA_SEPARATOR);
		long months = 0;
		if (!StringUtil.isNullOrEmpty(durationArray[0])) {
			months = Integer.parseInt(durationArray[0]);
		}
		return months;
	}

	public Set<Integer> getFlexiCodesCol() {
		return flexiCodesCol;
	}

	private void setFlexiCodesCol(Set<Integer> flexiCodesCol) {
		this.flexiCodesCol = flexiCodesCol;
	}

	public Set<Integer> getVisibleChannelCol() {
		return visibleChannelCol;
	}

	private void setVisibleChannelCol(Set<Integer> visibleChannelCol) {
		this.visibleChannelCol = visibleChannelCol;
	}

	public Set<String> getVisibileAgentCol() {
		return visibileAgentCol;
	}

	private void setVisibileAgentCol(Set<String> visibileAgentCol) {
		this.visibileAgentCol = visibileAgentCol;
	}

	public FareRulePax getAdPax() {
		return adPax;
	}

	public FareRulePax getChPax() {
		return chPax;
	}

	public FareRulePax getInPax() {
		return inPax;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(Integer fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public String getVisibleChannelIds() {
		return visibleChannelIds;
	}

	public String getPaxApplicabilityJSON() {
		return paxApplicabilityJSON;
	}

	public void setPaxApplicabilityJSON(String paxApplicabilityJSON) {
		this.paxApplicabilityJSON = paxApplicabilityJSON;
	}

	public String getModificationChargeType() {
		return modificationChargeType;
	}

	public void setModificationChargeType(String modificationChargeType) {
		this.modificationChargeType = modificationChargeType;
	}

	public String getCancellationChargeType() {
		return cancellationChargeType;
	}

	public void setCancellationChargeType(String cancellationChargeType) {
		this.cancellationChargeType = cancellationChargeType;
	}

	public String getModificationChargeAmountStr() {
		return modificationChargeAmountStr;
	}

	public void setModificationChargeAmountStr(String modificationChargeAmountStr) {
		this.modificationChargeAmountStr = modificationChargeAmountStr;
	}

	public String getCancellationChargeAmountStr() {
		return cancellationChargeAmountStr;
	}

	public void setCancellationChargeAmountStr(String cancellationChargeAmountStr) {
		this.cancellationChargeAmountStr = cancellationChargeAmountStr;
	}

	public String getCancellationChargeLocalStr() {
		return cancellationChargeLocalStr;
	}

	public void setCancellationChargeLocalStr(String cancellationChargeLocalStr) {
		this.cancellationChargeLocalStr = cancellationChargeLocalStr;
	}

	public String getModificationChargeLocalStr() {
		return modificationChargeLocalStr;
	}

	public void setModificationChargeLocalStr(String modificationChargeLocalStr) {
		this.modificationChargeLocalStr = modificationChargeLocalStr;
	}

	public String getMinCnxChargeAmountStr() {
		return minCnxChargeAmountStr;
	}

	public void setMinCnxChargeAmountStr(String minCnxChargeAmountStr) {
		this.minCnxChargeAmountStr = minCnxChargeAmountStr;
	}

	public String getMaxCnxChargeAmountStr() {
		return maxCnxChargeAmountStr;
	}

	public void setMaxCnxChargeAmountStr(String maxCnxChargeAmountStr) {
		this.maxCnxChargeAmountStr = maxCnxChargeAmountStr;
	}

	public String getMinModChargeAmountStr() {
		return minModChargeAmountStr;
	}

	public void setMinModChargeAmountStr(String minModChargeAmountStr) {
		this.minModChargeAmountStr = minModChargeAmountStr;
	}

	public String getMaxModChargeAmountStr() {
		return maxModChargeAmountStr;
	}

	public void setMaxModChargeAmountStr(String maxModChargeAmountStr) {
		this.maxModChargeAmountStr = maxModChargeAmountStr;
	}

	public String getOnlyUpdateComments() {
		return onlyUpdateComments;
	}

	public void setOnlyUpdateComments(String onlyUpdateComments) {
		this.onlyUpdateComments = onlyUpdateComments;
	}

	public String getMinFareDiscountStr() {
		return minFareDiscountStr;
	}

	public void setMinFareDiscountStr(String minFareDiscountStr) {
		this.minFareDiscountStr = minFareDiscountStr;
	}

	public String getMaxFareDiscountStr() {
		return maxFareDiscountStr;
	}

	public void setMaxFareDiscountStr(String maxFareDiscountStr) {
		this.maxFareDiscountStr = maxFareDiscountStr;
	}

	public String getNameChangeChargeType() {
		return nameChangeChargeType;
	}

	public void setNameChangeChargeType(String nameChangeChargeType) {
		this.nameChangeChargeType = nameChangeChargeType;
	}

	public String getNameChangeChargeAmountStr() {
		return nameChangeChargeAmountStr;
	}

	public void setNameChangeChargeAmountStr(String nameChangeChargeAmountStr) {
		this.nameChangeChargeAmountStr = nameChangeChargeAmountStr;
	}

	public String getNameChangeChargeAmountLocalStr() {
		return nameChangeChargeAmountLocalStr;
	}

	public void setNameChangeChargeAmountLocalStr(String nameChangeChargeAmountLocalStr) {
		this.nameChangeChargeAmountLocalStr = nameChangeChargeAmountLocalStr;
	}

	public String getMinNCCAmountStr() {
		return minNCCAmountStr;
	}

	public void setMinNCCAmountStr(String minNCCAmountStr) {
		this.minNCCAmountStr = minNCCAmountStr;
	}

	public String getMaxNCCAmountStr() {
		return maxNCCAmountStr;
	}

	public void setMaxNCCAmountStr(String maxNCCAmountStr) {
		this.maxNCCAmountStr = maxNCCAmountStr;
	}

	public String getVisiblePOSStr() {
		return visiblePOSStr;
	}

	public void setVisiblePOSStr(String visiblePOSStr) {
		this.visiblePOSStr = visiblePOSStr;
		if (visiblePOSStr != null && !visiblePOSStr.trim().isEmpty()) {
			if (visiblePOS == null) {
				visiblePOS = new HashSet<String>();
			}
			String arrPOS[] = null;
			if (visiblePOSStr.indexOf(",") != -1) {
				arrPOS = visiblePOSStr.split(",");
			} else {
				arrPOS = new String[1];
				arrPOS[0] = visiblePOSStr;
			}

			for (int t = 0; t < arrPOS.length; t++) {
				if (arrPOS[t] != null && !arrPOS[t].equals("")) {
					visiblePOS.add(new String(arrPOS[t]));
				}
			}
			setVisiblePOS(visiblePOS);
		}
	}

	public Set<String> getVisiblePOS() {
		return visiblePOS;
	}

	public void setVisiblePOS(Set<String> visiblePOS) {
		this.visiblePOS = visiblePOS;
	}

	public Boolean getApplicableToCountries() {
		return applicableToCountries;
	}

	public void setApplicableToCountries(Boolean applicableToCountries) {
		this.applicableToCountries = applicableToCountries;
	}

	public String getBulkTicket() {
		return bulkTicket;
	}

	public void setBulkTicket(String bulkTicket) {
		this.bulkTicket = bulkTicket;
	}

	public Integer getMinBulkTicketSeatCount() {
		return minBulkTicketSeatCount;
	}

	public void setMinBulkTicketSeatCount(Integer minBulkTicketSeatCount) {
		this.minBulkTicketSeatCount = minBulkTicketSeatCount;
	}

	private String getFormatedModBuffer(String bufferTime) {
		String modifyBufferTime = bufferTime;
		String[] durationArray = modifyBufferTime.split(DATA_SEPARATOR);
		String strDay = ((!durationArray[0].equals("")) && (Integer.parseInt(durationArray[0]) <= 9999))
				? (durationArray[0] + ":")
				: "0000:";
		String strHour = (!durationArray[1].equals("")) ? (durationArray[1] + ":") : "00:";
		String strMin = (!durationArray[2].equals("")) ? (durationArray[2] + ":") : "00";
		String strmodBufTime = strDay + strHour + strMin;
		long modBufTimeInMilSec = AppSysParamsUtil.getTimeInMillis(strmodBufTime);
		if (modBufTimeInMilSec != 0) {
			modifyBufferTime = strmodBufTime;
		} else {
			modifyBufferTime = null;
		}

		return modifyBufferTime;
	}
}