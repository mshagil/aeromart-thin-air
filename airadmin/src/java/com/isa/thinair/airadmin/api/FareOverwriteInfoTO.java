package com.isa.thinair.airadmin.api;

import java.io.Serializable;
import java.math.BigDecimal;

public class FareOverwriteInfoTO implements Serializable, Comparable<FareOverwriteInfoTO> {

	private static final long serialVersionUID = 3805181670461206500L;

	public static final String TXT_BEFORE_DEPARTURE = "Before Departure";
	public static final String TXT_CA_AFTER_DEPARTURE = "After Departure";
	public static final String TXT_CA_ON_OR_BEFORE_DEPARTURE = "On or before departure";
	public static final String TXT_CA_ON_OR_AFTER_DEPARTURE = "On or after departure";

	public static final String TXT_TT_MON = "Months";
	public static final String TXT_TT_DAY = "Days";
	public static final String TXT_TT_HRS = "Hours";
	public static final String TXT_TT_WEK = "Weeks";
	public static final String TXT_TT_MIN = "Minutes";

	private Long id;

	private String chargeType;

	private String chargeAmountType;

	private BigDecimal chargeAmount;

	private BigDecimal min;

	private BigDecimal max;

	private String compareAgainst;

	private String timeType;

	private String timeValue;

	private String status;

	private String compareAgainstText;

	private String timeTypeText;

	private String statusTxt;

	private BigDecimal chargeAmountInLocal = BigDecimal.ZERO;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getChargeAmountType() {
		return chargeAmountType;
	}

	public void setChargeAmountType(String chargeAmountType) {
		this.chargeAmountType = chargeAmountType;
	}

	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public BigDecimal getMin() {
		return min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

	public String getCompareAgainst() {
		return compareAgainst;
	}

	public void setCompareAgainst(String compareAgainst) {
		this.compareAgainst = compareAgainst;
	}

	public String getTimeType() {
		return timeType;
	}

	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	public String getTimeValue() {
		return timeValue;
	}

	public void setTimeValue(String timeValue) {
		this.timeValue = timeValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCompareAgainstText() {
		return compareAgainstText;
	}

	public void setCompareAgainstText(String compareAgainstText) {
		this.compareAgainstText = compareAgainstText;
	}

	public String getTimeTypeText() {
		return timeTypeText;
	}

	public void setTimeTypeText(String timeTypeText) {
		this.timeTypeText = timeTypeText;
	}

	public String getStatusTxt() {
		return statusTxt;
	}

	public void setStatusTxt(String statusTxt) {
		this.statusTxt = statusTxt;
	}

	public BigDecimal getChargeAmountInLocal() {
		return chargeAmountInLocal;
	}

	public void setChargeAmountInLocal(BigDecimal chargeAmountInLocal) {
		this.chargeAmountInLocal = chargeAmountInLocal;
	}

	@Override
	public int compareTo(FareOverwriteInfoTO o) {
		return (this.getChargeType() + this.getId()).compareTo(o.getChargeType() + o.getId());
	}
}
