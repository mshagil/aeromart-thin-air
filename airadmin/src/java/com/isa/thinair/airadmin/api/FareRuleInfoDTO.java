package com.isa.thinair.airadmin.api;

import java.util.List;

public class FareRuleInfoDTO {

	private List<String[]> fareRuleIdTypeList;

	private List<String[]> fareVisibilityList;

	private List<String[]> paxCategoryList;

	private List<String[]> fareCategoryList;

	private List<String[]> flexiCategoryList;

	private List<String[]> flexiJournylist;

	private List<String[]> fixedBCFareRuleList;

	private List<String[]> fareModValueTypes;
	
	private List<String[]> paxChargeTypes;

	private List<String[]> currencyCodeList;

	private boolean isChildVisible;

	private boolean isOpenReturnSupport;

	private boolean isHalfReturnSupport;

	private boolean enableFareDiscount;

	private boolean enableAgentCommission;

	private boolean enableAgentInstruction;

	private boolean enableFareInDepAirPCurr;

	private boolean enableDomesticBufferTime;

	private double defaultModCharge;

	private double defaultCnxCharge;

	private boolean isShowNoFareSplitOption;

	private long domesticModBufferTime;

	private long internationalModBufferTime;

	private boolean enableFareRuleNCC;
	
	private int minRequiredSeatsForBTRes;

	public List<String[]> getFareRuleIdTypeList() {
		return fareRuleIdTypeList;
	}

	public void setFareRuleIdTypeList(List<String[]> fareRuleIdTypeList) {
		this.fareRuleIdTypeList = fareRuleIdTypeList;
	}

	public List<String[]> getFareVisibilityList() {
		return fareVisibilityList;
	}

	public void setFareVisibilityList(List<String[]> fareVisibilityList) {
		this.fareVisibilityList = fareVisibilityList;
	}

	public List<String[]> getPaxCategoryList() {
		return paxCategoryList;
	}

	public void setPaxCategoryList(List<String[]> paxCategoryList) {
		this.paxCategoryList = paxCategoryList;
	}

	public List<String[]> getFareCategoryList() {
		return fareCategoryList;
	}

	public void setFareCategoryList(List<String[]> fareCategoryList) {
		this.fareCategoryList = fareCategoryList;
	}

	public List<String[]> getFlexiCategoryList() {
		return flexiCategoryList;
	}

	public void setFlexiCategoryList(List<String[]> flexiCategoryList) {
		this.flexiCategoryList = flexiCategoryList;
	}

	public List<String[]> getFlexiJournylist() {
		return flexiJournylist;
	}

	public void setFlexiJournylist(List<String[]> flexiJournylist) {
		this.flexiJournylist = flexiJournylist;
	}

	public List<String[]> getFixedBCFareRuleList() {
		return fixedBCFareRuleList;
	}

	public void setFixedBCFareRuleList(List<String[]> fixedBCFareRuleList) {
		this.fixedBCFareRuleList = fixedBCFareRuleList;
	}

	public List<String[]> getFareModValueTypes() {
		return fareModValueTypes;
	}

	public void setFareModValueTypes(List<String[]> fareModValueTypes) {
		this.fareModValueTypes = fareModValueTypes;
	}

	public boolean isChildVisible() {
		return isChildVisible;
	}

	public void setChildVisible(boolean isChildVisible) {
		this.isChildVisible = isChildVisible;
	}

	public boolean isOpenReturnSupport() {
		return isOpenReturnSupport;
	}

	public void setOpenReturnSupport(boolean isOpenReturnSupport) {
		this.isOpenReturnSupport = isOpenReturnSupport;
	}

	public boolean isHalfReturnSupport() {
		return isHalfReturnSupport;
	}

	public void setHalfReturnSupport(boolean isHalfReturnSupport) {
		this.isHalfReturnSupport = isHalfReturnSupport;
	}

	public boolean isEnableFareDiscount() {
		return enableFareDiscount;
	}

	public void setEnableFareDiscount(boolean enableFareDiscount) {
		this.enableFareDiscount = enableFareDiscount;
	}

	public boolean isEnableAgentCommission() {
		return enableAgentCommission;
	}

	public void setEnableAgentCommission(boolean enableAgentCommission) {
		this.enableAgentCommission = enableAgentCommission;
	}

	public List<String[]> getPaxChargeTypes() {
		return paxChargeTypes;
	}

	public void setPaxChargeTypes(List<String[]> paxChargeTypes) {
		this.paxChargeTypes = paxChargeTypes;
	}

	public boolean isEnableAgentInstruction() {
		return enableAgentInstruction;
	}

	public void setEnableAgentInstruction(boolean enableAgentInstruction) {
		this.enableAgentInstruction = enableAgentInstruction;
	}

	public boolean isEnableFareInDepAirPCurr() {
		return enableFareInDepAirPCurr;
	}

	public void setEnableFareInDepAirPCurr(boolean enableFareInDepAirPCurr) {
		this.enableFareInDepAirPCurr = enableFareInDepAirPCurr;
	}

	public List<String[]> getCurrencyCodeList() {
		return currencyCodeList;
	}

	public void setCurrencyCodeList(List<String[]> currencyCodeList) {
		this.currencyCodeList = currencyCodeList;
	}

	public boolean isEnableDomesticBufferTime() {
		return enableDomesticBufferTime;
	}

	public void setEnableDomesticBufferTime(boolean enableDomesticBufferTime) {
		this.enableDomesticBufferTime = enableDomesticBufferTime;
	}

	public double getDefaultModCharge() {
		return defaultModCharge;
	}

	public void setDefaultModCharge(double defaultModCharge) {
		this.defaultModCharge = defaultModCharge;
	}

	public double getDefaultCnxCharge() {
		return defaultCnxCharge;
	}

	public void setDefaultCnxCharge(double defaultCnxCharge) {
		this.defaultCnxCharge = defaultCnxCharge;
	}

	public boolean isShowNoFareSplitOption() {
		return isShowNoFareSplitOption;
	}

	public void setShowNoFareSplitOption(boolean isShowNoFareSplitOption) {
		this.isShowNoFareSplitOption = isShowNoFareSplitOption;
	}

	public long getDomesticModBufferTime() {
		return domesticModBufferTime;
	}

	public void setDomesticModBufferTime(long domesticModBufferTime) {
		this.domesticModBufferTime = domesticModBufferTime;
	}

	public long getInternationalModBufferTime() {
		return internationalModBufferTime;
	}

	public void setInternationalModBufferTime(long internationalModBufferTime) {
		this.internationalModBufferTime = internationalModBufferTime;
	}

	public boolean isEnableFareRuleNCC() {
		return enableFareRuleNCC;
	}

	public void setEnableFareRuleNCC(boolean enableFareRuleNCC) {
		this.enableFareRuleNCC = enableFareRuleNCC;
	}

	public int getMinRequiredSeatsForBTRes() {
		return minRequiredSeatsForBTRes;
	}

	public void setMinRequiredSeatsForBTRes(int minRequiredSeatsForBTRes) {
		this.minRequiredSeatsForBTRes = minRequiredSeatsForBTRes;
	}
}
