package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class CancelResrveReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(CancelResrveReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("CancelResrveReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("CancelResrveReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("CancelResrveReportRH setReportView Success");
				return null;
			} else {
				log.error("CancelResrveReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CancelResrveReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String flightNo = request.getParameter("FlightNo");
		String strTime = request.getParameter("hdnTime");
		String depFromDate = request.getParameter("txtDepFromDate");
		String depToDate = request.getParameter("txtDepToDate");
		String reportType = request.getParameter("radOption");

		String reportTemplate = "CancelReservReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false); 
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strReportId = "UC_REPM_017";
		String reportName = "CancelledReservationReport";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}

			if (depFromDate != null && !depFromDate.equals("")) {
				search.setDepartureDateRangeFrom(ReportsHTMLGenerator.convertDate(depFromDate));
			}

			if (depToDate != null && !depToDate.equals("")) {
				search.setDepartureDateRangeTo(ReportsHTMLGenerator.convertDate(depToDate));
			}

			search.setTimeZone(strTime);

			String agentStation = "";
			String timeToDisplay = "";
			Date localTime;
			ZuluLocalTimeConversionHelper timeConversionHelper;
			Date currentTimeInZulu = CalendarUtil.getCurrentSystemTimeInZulu();

			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			agentStation = user.getAgentStation();
			timeConversionHelper = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportServiceBD());
			localTime = timeConversionHelper.getLocalDateTime(agentStation, currentTimeInZulu);

			if (strTime.equalsIgnoreCase(WebConstants.LOCALTIME)) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(localTime);
				timeToDisplay = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
						+ calendar.get(Calendar.SECOND);
			} else {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(currentTimeInZulu);
				timeToDisplay = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
						+ calendar.get(Calendar.SECOND);
			}

			if (flightNo != null && !flightNo.trim().equals("")) {
				search.setFlightNumber(flightNo.toUpperCase());
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			search.setReportType(reportType);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getCnxReservationDetails(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("REPORT_ID", strReportId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			parameters.put("TIME_ZONE", strTime);
			parameters.put("TIME", timeToDisplay);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (reportType.trim().equals("POST_DEP")) {
				parameters.put("REPORT_HEADING", "POST DEPATURE CANCELLATIONS");
			} else if (reportType.trim().equals("PRE_DEP")) {
				parameters.put("REPORT_HEADING", "PRE DEPATURE CANCELLATIONS");
			}

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=" + reportName + ".pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}
