package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class AdvancedAncillaryRevenueReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AncillaryRevenueReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AncillaryRevenueReportRH setReportView Failed " + e.getMessageString(), e);
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("Error in AncillaryRevenueReportRH execute()" + e.getMessage(), e);
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setAgentsMultiSelect(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setStationList(request);
		setAncillaryList(request);
		setSalesChannelList(request);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the agent multiselect
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentsMultiSelect(HttpServletRequest request) throws ModuleException {
		String agentType = "All";
		String agentList = ReportsHTMLGenerator.createAgentGSAMultiSelect(agentType, false, false);
		request.setAttribute(WebConstants.REQ_AGENTS_STATION, agentList);
	}

	/**
	 * Sets the Station List Multibox to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStationList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createStationList();
		request.setAttribute(WebConstants.REQ_HTML_STN_LIST, strList);
	}

	/**
	 * Sets the
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAncillaryList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createAncillaryList();
		request.setAttribute(WebConstants.REQ_HTML_ANCI_LIST, strList);
	}

	private static void setSalesChannelList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createSalesChannelHtml();
		request.setAttribute(WebConstants.REQ_HTML_SALES_CHANNEL_LIST, strList);
	}

	/**
	 * Gets the report data and sets to the corresponding view option.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ReportsSearchCriteria search = null;
		ResultSet resultSet = null;
		ArrayList<String> lstAgents = new ArrayList<String>();
		ArrayList<String> lstSalesChannels = new ArrayList<String>();
		ArrayList<String> stnCol = new ArrayList<String>();
		String byAgentUser = "N";
		String showMeal = "N";
		String showBaggage = "N";
		String showSeat = "N";
		String showInsuarance = "N";
		String showSSR = "N";
		String showHALA = "N";
		String showAirportTransfer = "N";
		
		String id = "UC_REPM_070";
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportTemplate = "AdvancedAncillaryRevenueReport.jasper";
		Map<String, Object> parameters = new HashMap<String, Object>();

		search = new ReportsSearchCriteria();

		String salesFromDate = request.getParameter("txtSalesFromDate");
		String salesToDate = request.getParameter("txtSalesToDate");
		String flightFromDate = request.getParameter("txtFlightFromDate");
		String flightToDate = request.getParameter("txtFlightToDate");

		String value = request.getParameter("radReportOption");
		String reportCategory = request.getParameter("radReportCat");
		String agents = request.getParameter("hdnAgents");
		String salesChannels = request.getParameter("hdnSalesChannels");
		String stations = request.getParameter("hdnStations");
		String ancillaries = request.getParameter("hdnAncillaries");

		if (isNotEmptyOrNull(ancillaries)) {
			String anciArr[] = ancillaries.split(",");

			for (int c = 0; c < anciArr.length; c++) {

				if ("SEAT_MAP".equals(anciArr[c]) && AppSysParamsUtil.isShowSeatMap()) {
					showSeat = "Y";
				}
				if ("MEALS".equals(anciArr[c]) && AppSysParamsUtil.isShowMeals()) {
					showMeal = "Y";
				}
				if ("INSURANCE".equals(anciArr[c]) && AppSysParamsUtil.isShowTravelInsurance()) {
					showInsuarance = "Y";
				}
				if ("BAGGAGES".equals(anciArr[c]) && AppSysParamsUtil.isShowBaggages()) {
					showBaggage = "Y";
				}
				if ("SSR".equals(anciArr[c])) {
					showSSR = "Y";
				}
				if ("HALA".equals(anciArr[c])) {
					showHALA = "Y";
				}
				if ("AIRPORT_TRANSFER".equals(anciArr[c]) && AppSysParamsUtil.isAirportTransferEnabled()) {
					showAirportTransfer = "Y";
				}

			}
		}

		if (isNotEmptyOrNull(stations)) {
			String stnArr[] = stations.split(",");
			for (int r = 0; r < stnArr.length; r++) {
				stnCol.add(stnArr[r]);
			}
			search.setStations(stnCol);
		}

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		if (isNotEmptyOrNull(salesFromDate) && isNotEmptyOrNull(salesToDate)) {
			String strFromDate = "'" + salesFromDate + " 00:00:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			String strToDate = "'" + salesToDate + " 23:59:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			search.setDateRangeFrom(strFromDate);
			search.setDateRangeTo(strToDate);
		}

		if (isNotEmptyOrNull(flightFromDate) && isNotEmptyOrNull(flightToDate)) {
			String strFromDate = "'" + flightFromDate + " 00:00:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			String strToDate = "'" + flightToDate + " 23:59:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			search.setDepartureDateRangeFrom(strFromDate);
			search.setDepartureDateRangeTo(strToDate);
		}

		if (isNotEmptyOrNull(agents)) {
			String[] arrAgents = agents.split(",");
			for (String agent : arrAgents) {
				lstAgents.add(agent);
			}
			search.setAgents(lstAgents);
		}

		if (isNotEmptyOrNull(salesChannels)) {
			String[] arrSalesChannels = salesChannels.split(",");
			for (String salesChannel : arrSalesChannels) {
				lstSalesChannels.add(salesChannel);
			}
			search.setSalesChannels(lstSalesChannels);
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));
		if (isNotEmptyOrNull(reportCategory) && reportCategory.equals("AGENT")) {
			search.setReportOption(reportCategory);
		} else {
			search.setReportOption(reportCategory);
			byAgentUser = "Y";
		}

		Map externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getAncillaryChargeCodesMap();
		List<String> lstmealCodes = Arrays.asList(externalChargesMap.get("MEALS").toString().split(","));
		List<String> lstSeatMapCodes = Arrays.asList(externalChargesMap.get("SEAT_MAP").toString().split(","));
		List<String> lstInsuranceCodes = Arrays.asList(externalChargesMap.get("INSURANCE").toString().split(","));
		List<String> lstBaggageCodes = Arrays.asList(externalChargesMap.get("BAGGAGES").toString().split(","));
		List<String> lstSSRCodes = Arrays.asList(externalChargesMap.get("SSR").toString().split(","));
		List<String> lstHALACodes = Arrays.asList(externalChargesMap.get("HALA").toString().split(","));
		List<String> lstAptTransferCodes = Arrays.asList(externalChargesMap.get("AIRPORT_TRANSFER").toString().split(","));

		if (lstmealCodes != null && !lstmealCodes.isEmpty()) {
			search.setLstmealCodes(lstmealCodes);
		}
		if (lstSeatMapCodes != null && !lstSeatMapCodes.isEmpty()) {
			search.setLstSeatMapCodes(lstSeatMapCodes);
		}
		if (lstInsuranceCodes != null && !lstInsuranceCodes.isEmpty()) {
			search.setLstInsuranceCodes(lstInsuranceCodes);
		}
		if (lstBaggageCodes != null && !lstBaggageCodes.isEmpty()) {
			search.setLstBaggageCodes(lstBaggageCodes);
		}
		if (lstSSRCodes != null && !lstSSRCodes.isEmpty()) {
			search.setLstSSRCodes(lstSSRCodes);
		}
		if (lstHALACodes != null && !lstHALACodes.isEmpty()) {
			search.setLstHALACodes(lstHALACodes);
		}
		
		if (lstAptTransferCodes != null && !lstAptTransferCodes.isEmpty()) {
			search.setLstAptTransferCodes(lstAptTransferCodes);
		}

		resultSet = ModuleServiceLocator.getDataExtractionBD().getAdvancedAncillaryRevenueReportData(search);

		parameters.put("BY_AGENT_USER", byAgentUser);
		parameters.put("SALES_FROM_DATE", salesFromDate);
		parameters.put("SALES_TO_DATE", salesToDate);
		parameters.put("FLIGHT_FROM_DATE", flightFromDate);
		parameters.put("FLIGHT_TO_DATE", flightToDate);

		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("AMT_1", "(" + strBase + ")");
		parameters.put("REPORT_MEDIUM", value);
		parameters.put("SHW_MEAL", showMeal);
		parameters.put("SHW_SEAT", showSeat);
		parameters.put("SHW_AIG", showInsuarance);
		parameters.put("SHW_BAGGAGE", showBaggage);
		parameters.put("SHW_SSR", showSSR);
		parameters.put("SHW_HALA", showHALA);
		parameters.put("SHW_APT", showAirportTransfer);

		// To provide Report Format Options
		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (value.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=AncillaryRevenueReport.xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=AncillaryRevenueReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}
}
