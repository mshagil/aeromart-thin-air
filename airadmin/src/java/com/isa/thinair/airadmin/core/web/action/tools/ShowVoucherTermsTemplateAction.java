package com.isa.thinair.airadmin.core.web.action.tools;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateDTO;
import com.isa.thinair.commons.api.dto.VoucherTermsTemplateSearchDTO;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * Action class to manage air admin voucher terms and conditions page.
 * 
 * @author chanaka
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowVoucherTermsTemplateAction extends BaseRequestAwareAction {

	private static final int PAGE_LENGTH = 20;

	private Collection<VoucherTermsTemplateDTO> termsTemplates;
	private VoucherTermsTemplateDTO termsTemplateToSave;
	private Integer termsTemplateID;
	private int page;
	private int totalRecords;
	private int totalPages;

	private boolean success = true;
	private String messageTxt;

	private boolean isNewSearch;

	private VoucherTermsTemplateSearchDTO searchCriteria = new VoucherTermsTemplateSearchDTO();

	private Log log = LogFactory.getLog(ShowVoucherTermsTemplateAction.class);

	public String execute() {
		if (AppSysParamsUtil.isVoucherEnabled()) {
			try {
				checkPrivileges();
				setEmptySearchParamsToNull();

				if (isNewSearch) {
					page = 1;
				}

				int startingRecordIndex = (page - 1) * PAGE_LENGTH;

				Page<VoucherTermsTemplateDTO> page = ModuleServiceLocator.getVoucherBD().getVoucherTermsTemplatePage(
						searchCriteria, startingRecordIndex, PAGE_LENGTH);
				termsTemplates = page.getPageData();
				totalRecords = page.getTotalNoOfRecords();
				totalPages = totalRecords % PAGE_LENGTH > 0 ? (totalRecords / PAGE_LENGTH) + 1 : (totalRecords / PAGE_LENGTH);

			} catch (Exception x) {
				success = false;
				messageTxt = "Failed to load terms and conditions";
				log.error(messageTxt, x);
			}
		}
		return AdminStrutsConstants.AdminAction.SUCCESS;
	}

	public String updateTermsAndConditions() {
		try {
			checkPrivileges();

			TrackInfoDTO trackingInfo = getTrackInfo();
			trackingInfo.setOriginUserId(((UserPrincipal) request.getUserPrincipal()).getUserId());

			 ModuleServiceLocator.getVoucherBD().updateVoucherTermsTemplate(termsTemplateToSave, trackingInfo);
		} catch (Exception x) {
			success = false;
			messageTxt = "Failed to update voucher terms and conditions";
			log.error(messageTxt, x);
		}
		return AdminStrutsConstants.AdminAction.SUCCESS;
	}

	private void checkPrivileges() {
		if (!BasicRequestHandler.hasPrivilege(request, PriviledgeConstants.ALLOW_VOUCHER_TERMS_N_CONDITIONS_EDITING)) {
			log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":"
					+ PriviledgeConstants.ALLOW_VOUCHER_TERMS_N_CONDITIONS_EDITING);
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		}
	}

	private void setEmptySearchParamsToNull() {
		if ("".equals(searchCriteria.getLanguage()))
			searchCriteria.setLanguage(null);
		if ("".equals(searchCriteria.getName()))
			searchCriteria.setName(null);
	}

	public Collection<VoucherTermsTemplateDTO> getTermsTemplates() {
		return termsTemplates;
	}

	public void setTermsTemplates(Collection<VoucherTermsTemplateDTO> termsTemplates) {
		this.termsTemplates = termsTemplates;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int currentPage) {
		this.page = currentPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public VoucherTermsTemplateDTO getTermsTemplateToSave() {
		return termsTemplateToSave;
	}

	public void setTermsTemplateToSave(VoucherTermsTemplateDTO termsTemplateToSave) {
		this.termsTemplateToSave = termsTemplateToSave;
	}

	public Integer getTermsTemplateID() {
		return termsTemplateID;
	}

	public void setTermsTemplateID(Integer termsTemplateID) {
		this.termsTemplateID = termsTemplateID;
	}

	public VoucherTermsTemplateSearchDTO getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(VoucherTermsTemplateSearchDTO searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public boolean isNewSearch() {
		return isNewSearch;
	}

	public void setNewSearch(boolean isNewSearch) {
		this.isNewSearch = isNewSearch;
	}

}

