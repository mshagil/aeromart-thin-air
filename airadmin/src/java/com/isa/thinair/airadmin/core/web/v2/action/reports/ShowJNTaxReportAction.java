package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ReportingUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_JN_TAX_JSP)
public class ShowJNTaxReportAction extends BaseRequestResponseAwareAction {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static String JN_TAX_REPORT_TEMPLATE = "JNTaxReport.jasper";
	private static String JN_TAX_DETAIL_REPORT_TEMPLATE = "JNTaxDetailReport.jasper";
	private static String OPT_DETAIL = "DETAIL";

	private String paymentDateFrm;
	private String paymentDateTo;
	private String bookedDateFrm;
	private String bookedDateTo;
	private String hdnAgents;
	private String hdnChannels;
	private String radReportOption;
	private String radOption;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setReportView();
			return null;
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private void setReportView() throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;
		String id = "UC_REPM_085";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = ReportingUtil.ReportingLocations.image_loc + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		Map<String, Object> parameters = new HashMap<String, Object>();
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		String fileName = "JNTaxReport";
		String reportTemplate = JN_TAX_REPORT_TEMPLATE;
		if (OPT_DETAIL.equals(radOption)) {
			reportTemplate = JN_TAX_DETAIL_REPORT_TEMPLATE;
			fileName = "JNTaxDetailReport";
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		search.setDateRangeFrom(this.paymentDateFrm);
		search.setDateRangeTo(this.paymentDateTo);
		search.setDateRange2From(this.bookedDateFrm);
		search.setDateRange2To(this.bookedDateTo);

		if (!StringUtil.isNullOrEmpty(this.hdnAgents)) {
			String[] arrAgents = this.hdnAgents.split(",");
			Collection<String> lstAgent = new HashSet<String>();
			for (String agentCode : arrAgents) {
				lstAgent.add(agentCode);
			}
			search.setAgents(lstAgent);
		}

		if (!StringUtil.isNullOrEmpty(this.hdnChannels)) {
			String[] arrChannels = this.hdnChannels.split(",");
			Collection<String> lstChannel = new HashSet<String>();
			for (String salesChannenl : arrChannels) {
				lstChannel.add(salesChannenl);
			}
			search.setSalesChannels(lstChannel);
		}

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		resultSet = ModuleServiceLocator.getDataExtractionBD().getJNTaxReportData(search);

		parameters.put("CURRENCY", AppSysParamsUtil.getBaseCurrency());
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", this.paymentDateFrm);
		parameters.put("TO_DATE", this.paymentDateTo);

		if (radReportOption.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (radReportOption.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName + ".pdf");
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName + ".csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}
	}

	public void setPaymentDateFrm(String paymentDateFrm) {
		this.paymentDateFrm = paymentDateFrm;
	}

	public void setPaymentDateTo(String paymentDateTo) {
		this.paymentDateTo = paymentDateTo;
	}

	public void setBookedDateFrm(String bookedDateFrm) {
		this.bookedDateFrm = bookedDateFrm;
	}

	public void setBookedDateTo(String bookedDateTo) {
		this.bookedDateTo = bookedDateTo;
	}

	public void setHdnAgents(String hdnAgents) {
		this.hdnAgents = hdnAgents;
	}

	public void setHdnChannels(String hdnChannels) {
		this.hdnChannels = hdnChannels;
	}

	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	public void setRadOption(String radOption) {
		this.radOption = radOption;
	}
}
