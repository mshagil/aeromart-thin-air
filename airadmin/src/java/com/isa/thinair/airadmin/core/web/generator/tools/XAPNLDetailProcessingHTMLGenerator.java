/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.XAPnlPaxEntry;

public class XAPNLDetailProcessingHTMLGenerator {

	private static String clientErrors;
	private static final String PARAM_CURRENT_PFS = "hdnCurrentPfs";
	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private String strFormFieldsVariablesJS = "";

	/**
	 * Gets the PFS Passenger Details Rows for the Selected PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of PFS Detail Rows
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws Exception
	 *             the Exception
	 */
	public final String getPFSProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIModeJS = "var isSearchMode = false; var strPFSContent;";
		String strProcessStatus = "";
		strUIModeJS = "var isSearchMode = true; var strPFSContent;";

		strProcessStatus = request.getParameter(PARAM_SEARCH_PROCESS_STATUS);
		strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		setFormFieldValues(strFormFieldsVariablesJS);

		return createPFSProcessingRowHTML(request);
	}

	/**
	 * Creates the PFS Processing Row for the Selected PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containig the PFS details
	 * @throws Exception
	 *             the Exception
	 */
	private String createPFSProcessingRowHTML(HttpServletRequest request) throws Exception {

		String strCurrentPFS = "";

		strCurrentPFS = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS));
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		String tempDesc = "";
		String[] arrCurrentPFS = strCurrentPFS.split(",");
		Collection<XAPnlPaxEntry> xapnlPaxEntries = ModuleServiceLocator.getXApnlBD().getXAPnlPaxEntries(
				Integer.valueOf(arrCurrentPFS[3]).intValue(), null);

		if (xapnlPaxEntries != null && !xapnlPaxEntries.isEmpty()) {
			int i = 0;
			String strTemp = "";
			for (Iterator<XAPnlPaxEntry> iterPfsPaxEntries = xapnlPaxEntries.iterator(); iterPfsPaxEntries.hasNext();) {
				XAPnlPaxEntry xapnlPaxEntry = (XAPnlPaxEntry) iterPfsPaxEntries.next();
				sb.append("arrData[" + i + "] = new Array();");

				if (xapnlPaxEntry.getTitle() != null) {
					sb.append("arrData[" + i + "][0]= '" + xapnlPaxEntry.getTitle() + "';");
				} else {
					sb.append("arrData[" + i + "][0] = ''; ");
				}

				if (xapnlPaxEntry.getFirstName() != null) {
					sb.append("arrData[" + i + "][1]= '" + xapnlPaxEntry.getFirstName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][1] = ''; ");
				}

				if (xapnlPaxEntry.getLastName() != null) {
					sb.append("arrData[" + i + "][2] = '" + xapnlPaxEntry.getLastName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][2] = ''; ");
				}

				if (xapnlPaxEntry.getPnr() != null) {
					sb.append("arrData[" + i + "][3] = '" + xapnlPaxEntry.getPnr() + "'; ");
				} else {
					sb.append("arrData[" + i + "][3] = ''; ");
				}

				if (xapnlPaxEntry.getArrivalAirport() != null) {
					sb.append("arrData[" + i + "][4] = '" + xapnlPaxEntry.getArrivalAirport() + "'; ");
				} else {
					sb.append("arrData[" + i + "][4] = ''; ");
				}

				if (xapnlPaxEntry.getProcessedStatus() != null) {
					tempDesc = "";
					if (xapnlPaxEntry.getProcessedStatus().equals(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED)) {
						tempDesc = "Error Occured";
						if (xapnlPaxEntry.getErrorDescription() != null && !xapnlPaxEntry.getErrorDescription().trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (xapnlPaxEntry.getErrorDescription() == null ? "" : xapnlPaxEntry.getErrorDescription()
											.trim()) + "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");
						} else {
							sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
						}
					} else if (xapnlPaxEntry.getProcessedStatus().equals(
							ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED)) {
						tempDesc = "Not Processed";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					} else if (xapnlPaxEntry.getProcessedStatus().equals(ReservationInternalConstants.PfsProcessStatus.PROCESSED)) {
						tempDesc = "Processed";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					} else {
						tempDesc = "Unknown";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					}
				} else {
					sb.append("arrData[" + i + "][6] = '';");
				}

				if (xapnlPaxEntry.getProcessedStatus() != null) {
					sb.append("arrData[" + i + "][8] = '" + xapnlPaxEntry.getProcessedStatus() + "';");
				} else {
					sb.append("arrData[" + i + "][8] = '';");
				}

				if (xapnlPaxEntry.getPaxType() != null) {
					sb.append("arrData[" + i + "][15]= '" + xapnlPaxEntry.getPaxType() + "'; ");
				} else {
					sb.append("arrData[" + i + "][15] = ''; ");
				}
				sb.append("arrData[" + i + "][9] = '" + xapnlPaxEntry.getPnlPaxId() + "';");
				sb.append("arrData[" + i + "][10] = '" + xapnlPaxEntry.getVersion() + "';");
				sb.append("arrData[" + i + "][11] = '" + xapnlPaxEntry.getPnlId().toString() + "';");
				sb.append("arrData[" + i + "][12] = '"
						+ (xapnlPaxEntry.getErrorDescription() == null ? "" : xapnlPaxEntry.getErrorDescription().trim()) + "';");
				sb.append("arrData[" + i + "][13] = '" + xapnlPaxEntry.getCabinClassCode() + "';");
				sb.append("arrData[" + i + "][14] = '" + xapnlPaxEntry.getPaxType() + "';");
				i++;
			}
		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for PFS Detail Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.pfsprocess.form.fromdate.required", "fromDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.todate.required", "toDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.fromdate.invalid", "fromDateInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.todate.invalid", "toDateInvalid");
			moduleErrs.setProperty("um.process.form.airport.required", "airportRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.todate.lessthan.fromdate", "todateLessthanFromDate");
			moduleErrs.setProperty("um.pfsprocess.form.todate.lessthan.currentdate", "todateLessthanCurrentDate");
			moduleErrs.setProperty("um.process.form.flightDate.required", "flightDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.flightDateFormat.invaid", "flightDateFormatInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.flightTimeFormat.invaid", "flightTimeFormatInvalid");
			moduleErrs.setProperty("um.process.form.downloadTS.required", "downloadTSRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.downloadTSDateFormat.invaid", "downloadTSDateFormatInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.downloadTSTimeFormat.invaid", "downloadTSTimeFormatInvalid");
			moduleErrs.setProperty("um.process.form.flightNo.required", "flightNoRqrd");
			moduleErrs.setProperty("um.process.form.title.required", "titleRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.firstName.required", "firstNameRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.lastName.required", "lastNameRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.PNR.required", "PNRRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.destination.required", "destinationRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.paxStatus.required", "paxStatusRqrd");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
			moduleErrs.setProperty("um.pfsprocess.form.noofpax.required", "paxnoRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.cc.notmatch", "ccnotmatch");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

}
