package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.ONDCodeRH;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

import javax.xml.ws.Response;

/**
 * @author mano
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowONDCodeAction {

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private Integer templateId;
	private String templateCode;
	private String description;

	private String ondCodeComplete;
	private String createdBy;
	private Date createdDate;
	private long version;
	private String selONDCode;
	private String selONDtemplateCodeID;
	private String msgType;
	private String templOption;
	private String selStatus;

	private boolean baggageMandatory;

	private String ondCode;
	private String origin;
	private String destination;
	private Integer baggageChargeTemplateId;
	private String status;
	private long ondVersion;
	private long ondBagTempVersion;
	private String ondStatus;

	private BaggageTemplateTo baggageTemplateTo;
	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static Log log = LogFactory.getLog(ShowONDCodeAction.class);

	public String execute() {
		try {
			log.debug("saving ond baggage template");
			ONDBaggageChargeTemplate baggageChargeTemplate = createTemplateForSaveUpdate();
			ONDCodeRH.saveONDCodes(baggageChargeTemplate);

			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				this.succesMsg = airadminConfig.getMessage("um.ondCode.form.ondCode.exist");
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createONDCodeList();
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String saveBaggageTemplate() {

		try {
			ModuleServiceLocator.getChargeBD().saveBaggageTemplate(baggageTemplateTo);
			this.succesMsg = "Baggage Template Successfully Saved.";
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			log.error("Baggage Error:", e);
			this.succesMsg = airadminConfig.getMessage(e.getExceptionCode());

			if (e.getExceptionCode().equals("um.inventory.logic.baggage.template.overlap")) {
				this.succesMsg += " IDs : " + e.getExceptionDetails();
			}

			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception e) {
			log.error("Baggage Error:", e);
			this.succesMsg = "Baggage Save Failed";
			this.msgType = WebConstants.MSG_ERROR;
		}

		return S2Constants.Result.SUCCESS;

	}

	public String deleteONDCodes() {
		if (log.isDebugEnabled()) {
			log.debug("deleting ond codes" + baggageChargeTemplateId);
		}
		// ONDBaggageTemplate baggageTemplate = createTemplate();
		ONDBaggageChargeTemplate baggageChargeTemplate = createTemplate();
		try {
			// ONDBaggageTemplateRH.deleteONDBaggageTemplate(new ONDBaggageTemplate());
			ONDCodeRH.deleteONDCodes(baggageChargeTemplate);

			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}
		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createONDCodeList();
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchONDCodes() {

		log.debug("inside searchONDCodes");

		ONDCodeSearchCriteria criteria = new ONDCodeSearchCriteria();

		if (this.selONDCode != null && !this.selONDCode.trim().equals(""))
			criteria.setOndCode(this.selONDCode);

		Page pgOnd = null;
		try {

			pgOnd = ONDCodeRH.searchONDCodes(this.page, criteria);

			this.records = pgOnd.getTotalNoOfRecords();
			this.total = pgOnd.getTotalNoOfRecords() / 20;
			int mod = pgOnd.getTotalNoOfRecords() % 20;
			if (mod > 0)
				this.total = this.total + 1;
			if (this.page < 0)
				this.page = 1;

			Collection<BaggageTemplateTo> baggageTemplatets = pgOnd.getPageData();
			if (baggageTemplatets != null) {
				Object[] dataRow = new Object[baggageTemplatets.size()];
				int index = 1;
				Iterator<BaggageTemplateTo> iter = baggageTemplatets.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();

					BaggageTemplateTo templateTo = iter.next();

					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("baggageTemplate", templateTo);

					dataRow[index - 1] = counmap;
					index++;
				}
				this.rows = dataRow;
			}
			this.baggageMandatory = AppSysParamsUtil.isBaggageMandatory();

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private ONDBaggageChargeTemplate createTemplate() {

		ONDBaggageChargeTemplate baggageChargeTemplate = new ONDBaggageChargeTemplate();
		if (baggageChargeTemplateId != null) {
			baggageChargeTemplate.setOndBaggageChargeTemplateId(baggageChargeTemplateId);
			baggageChargeTemplate.setVersion(version);
		}

		ONDBaggageTemplate baggageTemplate = new ONDBaggageTemplate();
		baggageTemplate.setTemplateId(templateId);
		baggageTemplate.setVersion(ondBagTempVersion);

		baggageChargeTemplate.setOndCode(ondCode);
		baggageChargeTemplate.setOndBaggageTemplate(baggageTemplate);

		return baggageChargeTemplate;
	}

	private ONDBaggageChargeTemplate createTemplateForSaveUpdate() throws ModuleException {
		ONDBaggageChargeTemplate baggageChargeTemplate = new ONDBaggageChargeTemplate();

		if (baggageChargeTemplateId != null) {
			baggageChargeTemplate.setOndBaggageChargeTemplateId(baggageChargeTemplateId);
			baggageChargeTemplate.setVersion(version);

		}
		String origin = null;
		String destination = null;
		if (ondCodeComplete != null) {

			String[] ondPoints = ondCodeComplete.split("/");

			baggageChargeTemplate.setOndCode(ondCodeComplete.trim());
			int i = 0;
			for (String s : ondPoints) {
				if (i == 0)
					origin = ondPoints[0].trim();
				if (i + 1 == ondPoints.length)
					destination = ondPoints[i].trim();
				i++;
			}

		}

		// Only ALL/ALL is allowed. ALL/SHJ/ALL not allowed.
		if (origin.equals("All") && destination.equals("All")) {
			if (!baggageChargeTemplate.getOndCode().equals("All/All")) {
				throw new ModuleException("airinventory.logic.ondbaggage.departure.arrival.mismatch");
			}
		}

		// Only ALL/XXX or XXX/ALL is allowed. XXX/YYY/ALL is not allowed.
		if (origin.equals("All") || destination.equals("All")) {
			String[] onds = StringUtils.split(baggageChargeTemplate.getOndCode(), "/");
			if (onds.length > 2) {
				throw new ModuleException("airinventory.logic.ondbaggage.partial.ond.all.not.supported");
			}
		}

		ONDBaggageTemplate baggageTemplate = new ONDBaggageTemplate();
		baggageTemplate.setTemplateId(templateId);
		baggageTemplate.setVersion(ondBagTempVersion);

		baggageChargeTemplate.setOndCode(ondCodeComplete.trim());
		baggageChargeTemplate.setOndBaggageTemplate(baggageTemplate);

		return baggageChargeTemplate;
	}

	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the templateId
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the templateCode
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the baggageCharges
	 */
	public String getBaggageCharges() {
		return ondCodeComplete;
	}

	/**
	 * @param baggageCharges
	 *            the baggageCharges to set
	 */
	public void setBaggageCharges(String baggageCharges) {
		this.ondCodeComplete = baggageCharges;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the templOption
	 */
	public String getTemplOption() {
		return templOption;
	}

	/**
	 * @param templOption
	 *            the templOption to set
	 */
	public void setTemplOption(String templOption) {
		this.templOption = templOption;
	}

	/**
	 * @return the selStatus
	 */
	public String getSelStatus() {
		return selStatus;
	}

	/**
	 * @param selStatus
	 *            the selStatus to set
	 */
	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public boolean isBaggageMandatory() {
		return baggageMandatory;
	}

	public void setBaggageMandatory(boolean baggageMandatory) {
		this.baggageMandatory = baggageMandatory;
	}

	/**
	 * @return the selONDCode
	 */
	public String getSelONDCode() {
		return selONDCode;
	}

	/**
	 * @param selONDCode
	 *            the selONDCode to set
	 */
	public void setSelONDCode(String selONDCode) {
		this.selONDCode = selONDCode;
	}

	/**
	 * @return the ondCode
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the baggageChargeTemplateId
	 */
	public Integer getBaggageChargeTemplateId() {
		return baggageChargeTemplateId;
	}

	/**
	 * @param baggageChargeTemplateId
	 *            the baggageChargeTemplateId to set
	 */
	public void setBaggageChargeTemplateId(Integer baggageChargeTemplateId) {
		this.baggageChargeTemplateId = baggageChargeTemplateId;
	}

	/**
	 * @return the ondVersion
	 */
	public long getOndVersion() {
		return ondVersion;
	}

	/**
	 * @param ondVersion
	 *            the ondVersion to set
	 */
	public void setOndVersion(long ondVersion) {
		this.ondVersion = ondVersion;
	}

	/**
	 * @return the ondBagTempVersion
	 */
	public long getOndBagTempVersion() {
		return ondBagTempVersion;
	}

	/**
	 * @param ondBagTempVersion
	 *            the ondBagTempVersion to set
	 */
	public void setOndBagTempVersion(long ondBagTempVersion) {
		this.ondBagTempVersion = ondBagTempVersion;
	}

	/**
	 * @return the ondStatus
	 */
	public String getOndStatus() {
		return ondStatus;
	}

	/**
	 * @param ondStatus
	 *            the ondStatus to set
	 */
	public void setOndStatus(String ondStatus) {
		this.ondStatus = ondStatus;
	}
	
	public BaggageTemplateTo getBaggageTemplateTo() {
		return baggageTemplateTo;
	}

	public void setBaggageTemplateTo(BaggageTemplateTo baggageTemplateTo) {
		this.baggageTemplateTo = baggageTemplateTo;
	}

	public String getSelONDtemplateCodeID() {
		return selONDtemplateCodeID;
	}

	public void setSelONDtemplateCodeID(String selONDtemplateCodeID) {
		this.selONDtemplateCodeID = selONDtemplateCodeID;
	}

}
