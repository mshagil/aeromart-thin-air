package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.BCForGroupingTO;
import com.isa.thinair.airinventory.api.dto.inventory.BCDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class GroupBookingClassAction extends CommonAdminRequestResponseAwareCommonAction {

	private static Log log = LogFactory.getLog(GroupBookingClassAction.class);

	private String mode;

	private List<BCForGroupingTO> bcsForGrouping = new ArrayList<BCForGroupingTO>();

	private String bcGroupJSON;

	private List<String[]> cabinClassList;

	private String defauldCabinClass;

	/**
	 * main Execute Method For Group BookingClass Action
	 * 
	 * @return String the Forward Action
	 */
	public String execute() {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setDefaultLoadSuccessMessage();

		try {
			if (mode != null && (mode.equals(WebConstants.ACTION_SAVE))) {
				saveGroup();
			} else if (mode != null && (mode.equals(WebConstants.ACTION_DELETE))) {
				deleteGroup();
			}
			displayDetails();
		} catch (Exception exception) {
			log.error("Exception in GroupBookingClassAction.execute()", exception);
			this.msgType = WebConstants.MSG_ERROR;
			this.message = airadminConfig.getMessage(WebConstants.KEY_LOAD_FAIL);
		}

		return forward;
	}

	private void setDefaultLoadSuccessMessage() {
		this.message = airadminConfig.getMessage(WebConstants.KEY_LOAD_SUCCESS);
		this.msgType = WebConstants.MSG_SUCCESS;
	}

	private void displayDetails() throws ModuleException {
		List<BCDetailDTO> bcDetailsList = ModuleServiceLocator.getBookingClassBD().getBCsForGrouping();

		for (BCDetailDTO bcDetail : bcDetailsList) {
			if (bcsForGrouping == null) {
				bcsForGrouping = new ArrayList<BCForGroupingTO>();
			}
			BCForGroupingTO bcForGroupingTO = new BCForGroupingTO();
			bcForGroupingTO.setBcCode(bcDetail.getBcCode());
			bcForGroupingTO.setCcCode(bcDetail.getCcCode());
			bcForGroupingTO.setLogicalCCCode(bcDetail.getLogicalCCCode());
			bcForGroupingTO.setGroupId(bcDetail.getGroupId());
			bcForGroupingTO.setEditable(bcDetail.isEditable());
			bcForGroupingTO.setLogicalCCDescription(bcDetail.getLogicalCCDescription());

			bcsForGrouping.add(bcForGroupingTO);
		}

		cabinClassList = SelectListGenerator.createAllCabinClassList();
		defauldCabinClass = AppSysParamsUtil.getDefaultCOS();

	}

	private void saveGroup() throws Exception {
		JSONParser parser = new JSONParser();
		JSONArray jsonBCGroupArray = (JSONArray) parser.parse(bcGroupJSON);
		List<String> bcCodes = new ArrayList<String>();
		int groupId = 0;

		if (jsonBCGroupArray.size() > 0) {
			for (int i = 0; i < jsonBCGroupArray.size(); i++) {
				JSONObject bcGroup = (JSONObject) jsonBCGroupArray.get(i);
				bcCodes.add(bcGroup.get("bcCode").toString());
				groupId = Integer.valueOf(bcGroup.get("groupId").toString());
			}
			if (groupId != 0 && !bcCodes.isEmpty()) {
				ModuleServiceLocator.getBookingClassBD().updateBCGroupIds(bcCodes, groupId);
			}
		}
	}

	private void deleteGroup() throws Exception {
		JSONParser parser = new JSONParser();
		JSONArray jsonBCGroupArray = (JSONArray) parser.parse(bcGroupJSON);
		List<String> bcCodes = new ArrayList<String>();

		if (jsonBCGroupArray.size() > 0) {
			for (int i = 0; i < jsonBCGroupArray.size(); i++) {
				JSONObject bcGroup = (JSONObject) jsonBCGroupArray.get(i);
				bcCodes.add(bcGroup.get("bcCode").toString());
			}
			if (!bcCodes.isEmpty()) {
				ModuleServiceLocator.getBookingClassBD().updateBCGroupIds(bcCodes, 0);
			}
		}
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<BCForGroupingTO> getBcsForGrouping() {
		return bcsForGrouping;
	}

	public void setBcsForGrouping(List<BCForGroupingTO> bcsForGrouping) {
		this.bcsForGrouping = bcsForGrouping;
	}

	public String getBcGroupJSON() {
		return bcGroupJSON;
	}

	public void setBcGroupJSON(String bcGroupJSON) {
		this.bcGroupJSON = bcGroupJSON;
	}

	public List<String[]> getCabinClassList() {
		return cabinClassList;
	}

	public void setCabinClassList(List<String[]> cabinClassList) {
		this.cabinClassList = cabinClassList;
	}

	public String getDefauldCabinClass() {
		return defauldCabinClass;
	}

	public void setDefauldCabinClass(String defauldCabinClass) {
		this.defauldCabinClass = defauldCabinClass;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;

	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
}
