/**
 * 
 */
package com.isa.thinair.airadmin.core.web.v2.action.reports;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Janaka Padukka
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_BSP_RECON_JSP),
		@Result(name = S2Constants.Result.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class LoadBSPReconciliationReportAction extends BaseRequestResponseAwareAction {

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			request.setAttribute(S2Constants.RequestConstant.BSP_TNX_TYPES_LIST, SelectListGenerator.getBspTransactionTypes());
			request.setAttribute(S2Constants.RequestConstant.BSP_COUNTRY_LIST, SelectListGenerator.createBSPCountryList());
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, ReportsHTMLGenerator.getClientErrors(request));
			request.setAttribute(WebConstants.REP_SET_LIVE, request.getParameter(WebConstants.REP_HDN_LIVE));
			request.setAttribute(WebConstants.REQ_ENTITIES, SelectListGenerator.createEntityListByName());
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}
}
