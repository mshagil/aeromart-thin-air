package com.isa.thinair.airadmin.core.web.action.tools;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.tools.CreateRequestQueueHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.tools.RequestQueueHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.RQST_QUE_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowRequestQueueAction extends BaseRequestAwareAction{
	
	public String execute() throws Exception {
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, CreateRequestQueueHTMLGenerator.getClientErrors(request));
		return RequestQueueHandler.execute(request);
	}
}
