/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.XAPNLDetailProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Dhanushka Navin Ranatunga
 * 
 * 
 *         Preferences - Java - Code Style - Code Templates
 */

public final class XAPNLDetailProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(XAPNLDetailProcessingRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_CURRENT_PFS = "hdnCurrentPfs";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_TITLE = "selTitle";
	private static final String PARAM_PAX_TYPE = "selPaxType";
	private static final String PARAM_FIRST_NAME = "txtFirstName";
	private static final String PARAM_LAST_NAME = "txtLastName";
	private static final String PARAM_PNR = "txtPNR";
	private static final String PARAM_DESTINATION = "selDest";
	private static final String PARAM_PAX_STATUS = "selAddStatus";
	private static final String PARAM_PAX_ID = "hdnPaxID";
	private static final String PARAM_PAX_VERSION = "hdnPaxVersion";
	private static final String PARAM_PROCESS_STATUS = "hdnProcessSta";
	private static final String PARAM_GRIDROW = "hdnCurrentRowNum";
	private static final String PARAM_CURRENT_PFS_ID = "hdnPFSID";
	private static final String PARAM_FIRST_TIME = "hdnState";
	private static final String PARAM_NO_PAX = "txtPaxNO";
	private static final String PARAM_CABINCLAS = "selCC";
	private static final String PARAM_PAXCATEGORY = "selPaxCat";

	private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	/**
	 * Sucess Values: 0 - Saved Successfuly 1 - Delete Successfully -1 - Error occurred - No releavant field identified
	 * -2 - Error occurred - Pax Title -3 - Error occurred - Pax First Name -4 - Error occurred - Pax Last Name -5 -
	 * Error occurred - PNR -6 - Error occurred - Destination -7 - Error occurred - Status
	 * 
	 */
	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For PFS Actions Sets the Succes Values 0-Not Applicable, 1-PFS process Success, 2-PFS save
	 * Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var strPFSContent;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if ((strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE))
					|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE))) {
				processPSFData(request);
			}

			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	/**
	 * Process the PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean processed-true not-false
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings("unchecked")
	private static boolean processPSFData(HttpServletRequest request) throws Exception {

		String strCurrentPFS = null;
		String strCurrentPFSID = null;
		String strTitle = null;
		String strPaxType = null;
		String strFirstNAme = null;
		String strLastName = null;
		String strPNR = null;
		String strDestination = null;
		String strPAXStatus = null;
		String strPaxVersion = null;
		String strProcessStatus = null;
		String strGridRowNo = null;
		String strPaxId = null;
		String strHdnMode = "";
		String strHdnUIMode = "";
		String strFirstTime = "";
		String strNoPax = null;
		String strCC = null;
		String strPaxCat = null;

		try {
			strHdnMode = request.getParameter(PARAM_MODE);
			strHdnUIMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_UI_MODE));
			strCurrentPFS = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS));
			strCurrentPFSID = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS_ID));
			strTitle = AiradminUtils.getNotNullString(request.getParameter(PARAM_TITLE)).trim();
			strPaxType = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_TYPE)).trim();
			strFirstNAme = AiradminUtils.getNotNullString(request.getParameter(PARAM_FIRST_NAME));
			strLastName = AiradminUtils.getNotNullString(request.getParameter(PARAM_LAST_NAME));
			strPNR = AiradminUtils.getNotNullString(request.getParameter(PARAM_PNR)).trim();
			strDestination = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESTINATION));
			strPAXStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_STATUS));
			strPaxVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_VERSION));
			strProcessStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_PROCESS_STATUS));
			strGridRowNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strPaxId = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_ID));
			strFirstTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_FIRST_TIME));
			strNoPax = request.getParameter(PARAM_NO_PAX);
			strCC = AiradminUtils.getNotNullString(request.getParameter(PARAM_CABINCLAS));
			strPaxCat = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAXCATEGORY));

			String[] arrCurrentPFS = new String[0];
			Pfs currentPfs = null;
			currentPfs = (Pfs) request.getSession().getAttribute("CurrentPFS");
			if (!strHdnMode.equals("") && (strHdnMode.equals("SAVE") || strHdnMode.equals("DELETE"))) {

				if (strCurrentPFS != null && !strCurrentPFS.equals("")) {
					// A PFS record is selected
					arrCurrentPFS = strCurrentPFS.split(",");

					// Get the PFS data set in the PFS form
					// Collection pfsDataSet = (Collection) getAttribInRequest(request,"PFSDataSet");
					// Find the current pfs record
					if (currentPfs == null || strFirstTime.equals("")) {
						Collection<Pfs> pfsDataSet = (Collection<Pfs>) request.getSession().getAttribute("PFSDataSet");
						for (Iterator<Pfs> iterPfsData = pfsDataSet.iterator(); iterPfsData.hasNext();) {
							Pfs pfs = (Pfs) iterPfsData.next();
							if (pfs.getPpId() == Integer.valueOf(strCurrentPFSID).intValue()) {
								currentPfs = pfs;
								break;
							}
						}
					}

					PfsPaxEntry pfsPaxEntry = new PfsPaxEntry();
					pfsPaxEntry.setArrivalAirport(strDestination);
					pfsPaxEntry.setCabinClassCode(strCC);
					pfsPaxEntry.setDepartureAirport(arrCurrentPFS[11]);
					pfsPaxEntry.setEntryStatus(strPAXStatus);
					pfsPaxEntry.setFirstName(strFirstNAme);
					pfsPaxEntry.setFlightDate(outputDateFormat.parse(arrCurrentPFS[2]));
					pfsPaxEntry.setFlightNumber(arrCurrentPFS[1].toUpperCase());
					pfsPaxEntry.setLastName(strLastName);
					pfsPaxEntry.setPfsId(Integer.valueOf(strCurrentPFSID));
					pfsPaxEntry.setPnr(strPNR);
					if (!strPaxId.trim().equals("")) {
						pfsPaxEntry.setPpId(Integer.valueOf(strPaxId).intValue());
					}
					pfsPaxEntry.setProcessedStatus("N");
					pfsPaxEntry.setReceivedDate(outputDateFormat.parse(arrCurrentPFS[0]));
					pfsPaxEntry.setTitle(strTitle == "" ? null : strTitle);
					pfsPaxEntry.setPaxType(strPaxType);
					pfsPaxEntry.setPaxCategoryCode(strPaxCat);

					if (!strHdnMode.equals("") && strHdnMode.equals("SAVE")) {
						int psgrNo = 1;

						if (strNoPax != null && !strNoPax.trim().equals("")) {
							psgrNo = Integer.parseInt(strNoPax);
						}

						int pnrValidation = 0;
						pnrValidation = validatePNR(strPNR, strTitle, strFirstNAme, strLastName, arrCurrentPFS[1].toUpperCase(),
								outputDateFormat.parse(arrCurrentPFS[2]), arrCurrentPFS[11], strDestination);
						if (pnrValidation == 0) {
							for (int i = 0; i < psgrNo; i++) {
								if (strPaxVersion != null && !strPaxVersion.equals(""))
									pfsPaxEntry.setVersion(Long.parseLong(strPaxVersion));
								ModuleServiceLocator.getReservationBD().savePfsParseEntry(currentPfs, pfsPaxEntry);
							}

							setIntSuccess(request, 0);
							saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS),
									WebConstants.MSG_SUCCESS);
							// Get latest currentPfs
							request.getSession().setAttribute("CurrentPFS",
									ModuleServiceLocator.getReservationBD().getPFS(currentPfs.getPpId()));

							setExceptionOccured(request, false);
							log.debug("PFSProcessingRequestHandler.processPSFData() " + "method is successfully executed.");
						} else {
							log.error("PFSProcessingRequestHandler.processPSFData() " + "method is failed : Invalid PNR");
							setIntSuccess(request, -5);
							setErrorFormData(request, strCurrentPFS, strTitle, strFirstNAme, strLastName, strPNR, strDestination,
									strPAXStatus, strPaxVersion, strProcessStatus, strGridRowNo, strHdnMode, strHdnUIMode,
									strPaxId, strCurrentPFSID, strNoPax, strCC, strPaxCat);
							if (pnrValidation == 3) {
								// Passenger cannot identify in the PNR
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.pax.invalid"),
										WebConstants.MSG_ERROR);
							} else if (pnrValidation == 5) {
								// Reservation already cancelled
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.reservation.cancelled"),
										WebConstants.MSG_ERROR);
							} else if (pnrValidation == 6) {
								// Many flights exists for the selected flight number,
								// departure date / time and the segment
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.ambiguous.flights"),
										WebConstants.MSG_ERROR);
							} else if (pnrValidation == 2) {
								// Segment is not available in reservation
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.no.segment"),
										WebConstants.MSG_ERROR);
							} else if (pnrValidation == 7) {
								// Segment is already cancelled
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.segment.cancelled"),
										WebConstants.MSG_ERROR);
							} else {
								// PNR segment is not valid
								// Passenger is not confirmed
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.pax.notconfirmed"),
										WebConstants.MSG_ERROR);
							}
						}
					} else if (!strHdnMode.equals("") && strHdnMode.equals("DELETE")) {
						// reservationAuxBD.deletePfsParsedEntry(strPaxId);
						if (strPaxVersion != null && !strPaxVersion.equals(""))
							pfsPaxEntry.setVersion(Long.parseLong(strPaxVersion));
						ModuleServiceLocator.getReservationBD().deletePfsParseEntry(currentPfs, pfsPaxEntry);
						setIntSuccess(request, 1);
						saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
						// Get latest currentPfs
						request.getSession().setAttribute("CurrentPFS",
								ModuleServiceLocator.getReservationBD().getPFS(currentPfs.getPpId()));

						setExceptionOccured(request, false);
						log.debug("PFSProcessingRequestHandler.processPSFData() method is successfully executed.");
					}

				}
			}

		} catch (ModuleException moduleException) {
			log.error("PFSProcessingRequestHandler.processPSFData() method is failed :" + moduleException.getMessageString(),
					moduleException);
			// 4. PNR no. foriegn key validation should give ameaning full message.
			if (moduleException.getExceptionCode().equals("module.saveconstraint.childerror")) {
				setIntSuccess(request, -5);
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.invalid"), WebConstants.MSG_ERROR);
			} else if (moduleException.getExceptionCode().equals("airreservations.arg.noLongerExist")) {
				setIntSuccess(request, -5);
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.invalid"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
			setErrorFormData(request, strCurrentPFS, strTitle, strFirstNAme, strLastName, strPNR, strDestination, strPAXStatus,
					strPaxVersion, strProcessStatus, strGridRowNo, strHdnMode, strHdnUIMode, strPaxId, strCurrentPFSID, strNoPax,
					strCC, strPaxCat);

		} catch (Exception exception) {
			setIntSuccess(request, -1);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
		return true;
	}

	/**
	 * Sets the Form Data if An error occured & sets the Succes Int value 0-Not Applicable, 1-SendNewPNLSuccess,
	 * 2-SendNewADLSuccess 3-ResendPNLSuccess, 4-ResendADLSuccess 5-PrintSuccess, 6-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strCurrentPFS
	 *            the current PFS
	 * @param strTitle
	 *            the Title
	 * @param strFirstNAme
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param strPNR
	 *            the Reservation
	 * @param strDestination
	 *            the Destination
	 * @param strPAXStatus
	 *            the Passenger Status
	 * @param strPaxVersion
	 *            the Pax Version
	 * @param strProcessStatus
	 *            the PFS Process Status
	 * @param strGridRowNo
	 *            the Grid Row No
	 * @param strHdnMode
	 *            the Mode
	 * @param strHdnUIMode
	 *            the UI Mode
	 * @param strPaxId
	 *            the Passenger ID
	 * @param strCurrentPFSID
	 *            the Current PFS ID
	 * @param strPaxNo
	 *            the Passenger Number
	 */
	private static void setErrorFormData(HttpServletRequest request, String strCurrentPFS, String strTitle, String strFirstNAme,
			String strLastName, String strPNR, String strDestination, String strPAXStatus, String strPaxVersion,
			String strProcessStatus, String strGridRowNo, String strHdnMode, String strHdnUIMode, String strPaxId,
			String strCurrentPFSID, String strPaxNo, String strCabin, String strPaxCat) {

		setExceptionOccured(request, true);

		String strFormData = "var arrFormData = new Array();";
		strFormData += "arrFormData[0] = '" + strCurrentPFS + "';";
		strFormData += "arrFormData[1] = '" + strTitle + "';";
		strFormData += "arrFormData[2] = '" + strFirstNAme + "';";
		strFormData += "arrFormData[3] = '" + strLastName + "';";
		strFormData += "arrFormData[4] = '" + strPNR + "';";
		strFormData += "arrFormData[5] = '" + strDestination + "';";
		strFormData += "arrFormData[6] = '" + strPAXStatus + "';";
		strFormData += "arrFormData[7] = '" + strPaxVersion + "';";
		strFormData += "arrFormData[8] = '" + strProcessStatus + "';";
		strFormData += "arrFormData[9] = '" + strGridRowNo + "';";
		strFormData += "arrFormData[10] = '" + strHdnMode + "';";
		strFormData += "arrFormData[11] = '" + strHdnUIMode + "';";
		strFormData += "arrFormData[12] = '" + strPaxId + "';";
		strFormData += "arrFormData[13] = '" + strCurrentPFSID + "';";
		strFormData += "arrFormData[14] = '" + strPaxNo + "';";
		strFormData += "arrFormData[15] = '" + strCabin + "';";
		strFormData += "arrFormData[16] = '" + strPaxCat + "';";

		strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var strPFSContent = '';";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

	}

	/**
	 * Validate Reservation
	 * 
	 * @param strPNR
	 *            the Reservation
	 * @param strTitle
	 *            the Title
	 * @param strFirstName
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param flightNumber
	 *            the Flight Number
	 * @param departureDate
	 *            the Depature Date
	 * @param fromSegmentCode
	 *            the Origin
	 * @param toSegmentCode
	 *            the Destination
	 * @return int the success value 0-success 1-PNR segment is not valid 2-Segment is not available in reservation
	 *         3-Passenger cannot identify in the PNR 4-Passenger is not confirmed 5-Reservation Cancelled 6-Flight Id
	 *         is Not maching 7-Segment is already cancelled
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static int validatePNR(String strPNR, String strTitle, String strFirstName, String strLastName, String flightNumber,
			Date departureDate, String fromSegmentCode, String toSegmentCode) throws ModuleException {

		if (strPNR != null && !strPNR.equals("")) {

			FlightReconcileDTO flightReconcileDTO = null;
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			Reservation reservation = null;
			ReservationPax reservationPax = null;
			Collection<FlightReconcileDTO> colFlightReconcileDTO = null;

			pnrModesDTO.setPnr(strPNR);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);
			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				return 5;
			}

			colFlightReconcileDTO = ModuleServiceLocator.getResSegmentBD().getPnrSegmentsForReconcilation(flightNumber,
					departureDate, fromSegmentCode, toSegmentCode, strPNR);
			Integer flighSegId = null;

			for (Iterator<FlightReconcileDTO> iterFlightReconcileDTO = colFlightReconcileDTO.iterator(); iterFlightReconcileDTO
					.hasNext();) {
				flightReconcileDTO = (FlightReconcileDTO) iterFlightReconcileDTO.next();
				if (flighSegId != null && !flighSegId.equals(flightReconcileDTO.getFlightSegId())) {
					return 6;
				}
				flighSegId = flightReconcileDTO.getFlightSegId();
			}
			if (flighSegId == null) {
				return 1;
			}

			// see flightsegid exist in reservation
			Set<ReservationSegment> segs = reservation.getSegments();
			ReservationSegment reservationSegment = null;
			for (Iterator<ReservationSegment> iterSegs = segs.iterator(); iterSegs.hasNext();) {
				reservationSegment = (ReservationSegment) iterSegs.next();
				if (reservationSegment.getFlightSegId().equals(flighSegId)) {
					break;
				}
			}
			if (reservationSegment == null) {
				return 2;
			} else if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				return 7;
			}

			boolean resPaxFound = false;
			Set<ReservationPax> paxs = reservation.getPassengers();
			// Get the correct names as apear in PFS
			strTitle = makeStringCompliant(strTitle.toUpperCase());
			strFirstName = makeStringCompliant(strFirstName);
			strLastName = makeStringCompliant(strLastName);
			for (Iterator<ReservationPax> iterPaxs = paxs.iterator(); iterPaxs.hasNext();) {
				reservationPax = (ReservationPax) iterPaxs.next();

				// Validate the passenger
				if (validatePassenger(strTitle, strFirstName, strLastName, reservationPax)) {
					resPaxFound = true;
					break;
				}
			}
			if (!resPaxFound) {
				return 3;
			}
			if (reservationPax == null || reservationPax.getTotalAvailableBalance().doubleValue() > 0) {
				return 4;// Passenger is not confirmed
			}

		} else
			return 0;

		return 0;
	}

	/**
	 * Validate the passenger with the Reservations Detail
	 * 
	 * @param strTitle
	 *            the Title
	 * @param strFirstName
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param reservationPax
	 *            the Passenger
	 * @return boolean the validated true false
	 */
	private static boolean validatePassenger(String strTitle, String strFirstName, String strLastName,
			ReservationPax reservationPax) {
		String paxTitle = makeStringCompliant(reservationPax.getTitle());
		String paxFName = makeStringCompliant(reservationPax.getFirstName());
		String paxLName = makeStringCompliant(reservationPax.getLastName());

		if (strTitle.equals(paxTitle) && strFirstName.equals(paxFName) && strLastName.equals(paxLName)) {

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sets the Display Data For PFS Processing Page & Succes int value //0-Not Applicable, 1-PFS process Success, 2-PFS
	 * save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setPFSProcessingRowHtml(request);
		setTitleList(request);
		setPaxTypesList(request);
		setAirportList(request);
		setClientErrors(request);
		setDestinationAirportList(request);
		setCabinClassHtml(request);
		setPaxCategory(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	/**
	 * Sets the Destination airport list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDestinationAirportList(HttpServletRequest request) throws Exception {

		try {
			StringBuffer sb = new StringBuffer();

			String strCurrentPFS = request.getParameter(PARAM_CURRENT_PFS) == null ? "" : request.getParameter(PARAM_CURRENT_PFS);
			String[] arrCurrentPFS = new String[0];
			if (strCurrentPFS != null && !strCurrentPFS.equals("")) {
				// A PFS record is selected
				arrCurrentPFS = strCurrentPFS.split(",");

				List<String> l = ModuleServiceLocator.getFlightServiceBD().getPossibleDestinations(
						arrCurrentPFS[1].toUpperCase(), outputDateFormat.parse(arrCurrentPFS[2]), arrCurrentPFS[11]);

				Iterator<String> iter = l.iterator();
				while (iter.hasNext()) {
					String s = (String) iter.next();

					sb.append("<option value='" + s + "'>" + s + "</option>");
				}
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, sb.toString());
			} else {
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, SelectListGenerator.createActiveAirportCodeList());
			}

		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	/**
	 * Sets the Title list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setTitleList(HttpServletRequest request) throws Exception {
		String arrTitles = SelectListGenerator.createPaxTitleVisibleArray();
		request.setAttribute(WebConstants.PAX_TITLES, arrTitles);
	}

	private static void setPaxTypesList(HttpServletRequest request) throws ModuleException {
		String arrPaxTypes = SelectListGenerator.createPaxTypesArray();
		request.setAttribute(WebConstants.PAX_TYPE, arrPaxTypes);
	}

	/**
	 * Sets the Active Online Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = XAPNLDetailProcessingHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("XAPNLProcessingRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the PFS Processing rows to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setPFSProcessingRowHtml(HttpServletRequest request) throws Exception {
		try {
			XAPNLDetailProcessingHTMLGenerator htmlGen = new XAPNLDetailProcessingHTMLGenerator();
			String strHtml = htmlGen.getPFSProcessingRowHtml(request);

			String strFirstTime = request.getParameter(PARAM_FIRST_TIME) == null ? "" : request.getParameter(PARAM_FIRST_TIME);
			Pfs pfs = (Pfs) request.getSession().getAttribute("CurrentPFS");
			if (pfs != null && strFirstTime.equals("")) {
				strHtml += " pfsState = '" + pfs.getProcessedStatus() + "'; ";
			}
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("XAPNLProcessingHandler.setPFSProcessingRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Making the String Compatible Spaces
	 * 
	 * @param value
	 *            the string to make compatible
	 * @return String the compatible String
	 */
	private static String makeStringCompliant(String value) {
		char[] charArray = PlatformUtiltiies.nullHandler(value).toUpperCase().toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			int ascii = (int) charArray[i];
			if (ascii < 65 | ascii > 90) {
				charArray[i] = ' ';
			}
		}
		String g = new String(charArray);
		return g.replaceAll(" ", "");
	}

	/**
	 * Sets the Cabinclasses List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCabinClassHtml(HttpServletRequest request) throws ModuleException {
		String strCabinClassList = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strCabinClassList);
	}

	/**
	 * Sets Pax Category Type to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createPaxCatagoryList();
		request.setAttribute(WebConstants.REQ_PAXCAT_LIST, strHtml);
	}
}
