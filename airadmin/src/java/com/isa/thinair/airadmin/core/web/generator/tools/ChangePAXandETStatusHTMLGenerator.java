package com.isa.thinair.airadmin.core.web.generator.tools;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ChangePAXandETStatusHTMLGenerator {

	private static String clientErrors;

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.ChangePAXandETStatus.search.futureDate", "futureDate");
			moduleErrs.setProperty("um.ChangePAXandETStatus.search.invaliddate", "invaliddate");
			moduleErrs.setProperty("um.ChangePAXandETStatus.search.mandatorydate", "mandatorydate");
			moduleErrs.setProperty("um.ChangePAXandETStatus.search.mandatoryflight", "mandatoryflight");


			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

}
