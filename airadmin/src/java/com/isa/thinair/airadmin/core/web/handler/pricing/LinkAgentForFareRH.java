package com.isa.thinair.airadmin.core.web.handler.pricing;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;

public class LinkAgentForFareRH extends BasicRequestHandler {

	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setAgentMultiDDL(request);
		} catch (Exception e) {

		}
		return forward;
	}

	public static void setAgentMultiDDL(HttpServletRequest request) throws ModuleException {

		String strHtml = JavascriptGenerator.createAgentStationMDList();
		request.setAttribute(WebConstants.REQ_AGENTFClINK_DDL, strHtml);

	}
}
