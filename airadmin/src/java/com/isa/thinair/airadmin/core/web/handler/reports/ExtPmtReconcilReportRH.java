package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class ExtPmtReconcilReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ExtPmtReconcilReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public ExtPmtReconcilReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("ExtPmtReconcilReportRH success");
		} catch (Exception e) {
			forward = "error";
			log.error("ExtPmtReconcilReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("ExtPmtReconcilReportRH setReportView Success");
				return null;
			} else {
				log.error("ExtPmtReconcilReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ExtPmtReconcilReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);

	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String agentUserId = request.getParameter("txtAgentCode");
		String reportType = request.getParameter("hdnRptType");
		String value = request.getParameter("radReportOption");

		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		agentUserId = (agentUserId == null) ? "" : AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode()
				+ agentUserId.trim();

		String id = "UC_REPM_075";
		String reportTemplateSummary = "ExtPmtReconcilSummaryReport.jasper";
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (reportType.equals("Summary")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));
				search.setReportType(ReportsSearchCriteria.EXT_PMT_RECONCIL_SUMMARY);

				if (toDate != null && !toDate.equals("")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
				}
				if (fromDate != null && !fromDate.equals("")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
				}
				search.setUserId(agentUserId);

				parameters.put("AGENT_USER_ID", agentUserId.trim().toUpperCase());
				parameters.put("FROM_DATE", search.getDateRangeFrom());
				parameters.put("TO_DATE", search.getDateRangeTo());

			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getExtPmtReconcilSummaryData(search);
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

			parameters.put("ID", id);
			parameters.put("OPTION", value);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition", "attachment;filename=ExtPmtReconcilReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=ExtPmtReconcilReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
