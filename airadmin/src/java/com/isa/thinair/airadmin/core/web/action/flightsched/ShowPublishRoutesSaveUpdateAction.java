package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")

public class ShowPublishRoutesSaveUpdateAction  extends BaseRequestAwareAction {
	
	private boolean success = true;
	
	private static Log log = LogFactory.getLog(ShowPublishRoutesSaveUpdateAction.class);
	private static final String HDN_MODE = "hdnMode";
	private static final String GDS_ID = "gdsID";
	private static final String GDS_CODE = "gdsCode";
	private static final String SELECTED_ONDS = "selectedOnds";
	private static final String ENABLE_AUTO_PUBLISH = "enableAutoPublishForGds";
	
	public String execute() {

		String hdnMode;
		String gdsCode;

		try {
			String forward = null;
			gdsCode = AiradminUtils.getNotNullString(request.getParameter(GDS_CODE));
			hdnMode = request.getParameter(HDN_MODE);
			if (hdnMode != null) {

				if (hdnMode.equals(WebConstants.ACTION_VIEW)) {
					request.setAttribute(GDS_CODE, gdsCode);
					forward = AdminStrutsConstants.AdminAction.SUCCESS;
					return forward;

				} else if (hdnMode.equals(WebConstants.ACTION_UPDATE)) {
					saveOrUpdateData(request);
					request.setAttribute(GDS_CODE, gdsCode);
					forward = AdminStrutsConstants.AdminAction.SUCCESS;
				}

			}

			return forward;
		} catch (Exception e) {
			request.setAttribute(WebConstants.REQ_MESSAGE, e.getMessage());
			success = false;
			return AdminStrutsConstants.AdminAction.ERROR;
		}
	}

	@SuppressWarnings("null")
	public static void saveOrUpdateData(HttpServletRequest request) throws NumberFormatException, ModuleException {

		String gdsId = null;
		
		String strSelectedOnds = null;
		String strEnableAutoPublishForGds = null;

		gdsId = AiradminUtils.getNotNullString(request.getParameter(GDS_ID));
		strSelectedOnds = AiradminUtils.getNotNullString(request.getParameter(SELECTED_ONDS));
		strEnableAutoPublishForGds = AiradminUtils.getNotNullString(request.getParameter(ENABLE_AUTO_PUBLISH));
		if(strEnableAutoPublishForGds == null){
			strEnableAutoPublishForGds = "false";
		}
		Gds gds = ModuleServiceLocator.getGdsServiceBD().getGds(Integer.parseInt(gdsId));

		String[] selectedOnds = strSelectedOnds.split(",");

		if (selectedOnds != null && !selectedOnds[0].isEmpty()) {

			gds.setGdsAutoPublishRoutes(getOndSetForSave(selectedOnds));
			gds.setEnableRouteWiseAutoPublish(isRouteWiseAutoPublishedEnabled(strEnableAutoPublishForGds));
			ModuleServiceLocator.getGdsServiceBD().saveGds(gds);

		} else {
			ModuleServiceLocator.getScheduleServiceBD().removeGdsAutopublishRoutes(gdsId);
		}

	}

	private static Set<String> getOndSetForSave(String[] selectedOnds) {

		Set<String> autoPublishOndSet = new HashSet<String>();
		List<String> selectedOndlist = Arrays.asList(selectedOnds);

		for (String ondCode : selectedOndlist) {

			autoPublishOndSet.add(ondCode);

		}

		return autoPublishOndSet;
	}

	private static char isRouteWiseAutoPublishedEnabled(String strEnableAutoPublishForGds) {

		char isEnabled;
		if (strEnableAutoPublishForGds.equals("true")) {
			isEnabled = 'Y';
		} else {
			isEnabled = 'N';
		}

		return isEnabled;

	}

	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
