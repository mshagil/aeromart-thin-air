package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.model.InventoryAllocationTO;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

/**
 * @author Ruckman Colins
 * 
 */
public class MarketingCarrierRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(MarketingCarrierRequestHandler.class);

	static public final String FLIGHT_ID = "flightId";
	static private GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request) {
		log.debug("Begin MarketingCarrierRequestHandler.execute(HttpServletRequest request)");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			log.error("MarketingCarrierRequestHandler.execute() execute is failed :" + e);
		}
		return forward;
	}

	/**
	 * Retrive the inventory allocation data from markteing carrier site and diplay
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @throws ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		String flightId = request.getParameter("flightId");
		String departureDate = request.getParameter("depDate");
		String cabinClass = request.getParameter("cabinClass");
		String timeZone = request.getParameter("timeZone");
		String depAirPort = request.getParameter("depAirPort");

		/* Retrieve the all interline airline details */
		Map<String, InterlinedAirLineTO> interLineAirlines = globalConfig.getActiveInterlinedCarriersMap();

		/* build the input search TO in order to retrieve inventory allocation from markteing carrier */
		InventoryAllocationTO allocationTO = new InventoryAllocationTO();
		allocationTO.setFlightNumber(flightId);
		allocationTO.setCabinClass(cabinClass);

		allocationTO.setDepatureDate(convertDateStringToCalender(departureDate, timeZone, depAirPort));

		/* Call the necessary webservice BD to get the data */
		List carrierList = WPModuleUtils.getWSClientBD().getFlightInventoryAllocations(allocationTO, interLineAirlines);

		request.setAttribute("CarrierList", carrierList);
	}

	/**
	 * Format the date string into dd-MM-yyyy HH:mm, Returns the calender. If time zone is local it will convert to ZULU
	 * time
	 * 
	 * @param date
	 *            String (01-02-2008 12:45)
	 * @return Calendar
	 * @throws ModuleException
	 * @throws Exception
	 */
	public static Calendar convertDateStringToCalender(String date, String timeZone, String depAirPort) throws ModuleException {

		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportServiceBD());
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		Date zuluDate = null;

		try {
			zuluDate = formatter.parse(date);
		} catch (ParseException e) {
			log.error("Error in formatting date string " + e.getMessage());
			throw new ModuleException("airschedules.arg.invalid.cannot.calculate.zulutime");
		}

		/* If time is in LOCALTIME, need to convert to ZULUTIME */
		if (timeZone.equals(WebConstants.LOCALTIME)) {
			zuluDate = helper.getZuluDateTime(depAirPort, zuluDate);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(zuluDate);
		return cal;
	}

}