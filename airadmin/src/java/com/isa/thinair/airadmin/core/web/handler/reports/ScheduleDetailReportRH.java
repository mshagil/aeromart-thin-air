package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ScheduleDetailReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ScheduleDetailReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("ScheduleDetailReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("ScheduleDetailReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("ScheduleDetailReportRH setReportView Success");
				return null;
			} else {
				log.error("ScheduleDetailReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ScheduleDetailReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setStatusHtml(request);
		setOperationTypeHtml(request);
		setStationComboList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setStationComboList(HttpServletRequest request) throws ModuleException {
		// String strStation = SelectListGenerator.createStationList();
		String strStation = SelectListGenerator.createAirportsList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */
	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createScheduleStatus();
		request.setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);
	}

	/**
	 * sets the Operation Type list
	 * 
	 * @param request
	 */
	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");

		String operationType = request.getParameter("selOperationType");
		String flightNo = request.getParameter("FlightNo");
		String from = request.getParameter("selFrom");
		String to = request.getParameter("selTo");
		String status = request.getParameter("selStatus");
		String timeZone = request.getParameter("radTimeIn");
		String reportTemplate = "ScheduleDetailsReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strReportId = "UC_REPM_017";
		String reportName = "ScheduleDetailsReport";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}

			search.setFlightNumber(flightNo);
			search.setSectorFrom(from);
			search.setSectorTo(to);
			search.setTimeZone(timeZone);

			search.setOperationType(operationType);
			search.setFlightNumber(flightNo);
			search.setScheduleStatus(status);

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getScheduleDetails(search);

			String zuluTimePart = "";
			String agentStation = "";
			String localTimePart = "";
			Date localtime;
			ZuluLocalTimeConversionHelper timeConverter;
			Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();

			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			agentStation = user.getAgentStation();
			timeConverter = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportServiceBD());
			localtime = timeConverter.getLocalDateTime(agentStation, currentTimestamp);

			if (!timeZone.equalsIgnoreCase(WebConstants.LOCALTIME)) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(currentTimestamp);
				zuluTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
						+ calendar.get(Calendar.SECOND);
			} else {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(localtime);
				localTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
						+ calendar.get(Calendar.SECOND);
			}

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("REPORT_ID", strReportId);
			parameters.put("TIME_ZONE", timeZone);
			parameters.put("ZULU_TIME", zuluTimePart);
			parameters.put("LOCAL_TIME", localTimePart);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=" + reportName + ".pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
