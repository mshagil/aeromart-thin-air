package com.isa.thinair.airadmin.core.web.v2.action.system;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.promotion.api.to.constants.BundledFareConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Thushara Fernando
 * 
 *         This is a special action file to show JSP files directly with out any presentations logic being executed.
 *         File Name itself is used instead of separate result value to make mappings simple. So in the result
 *         annotation filename itself is specified as name as well as the value.
 * 
 *         eg : @Result(name=S2Constants.Jsp.Common.EXAMPLE_FILE, value=S2Constants.Jsp.Common.EXAMPLE_FILE)
 * 
 * 
 *         To work this <constant name="struts.enable.DynamicMethodInvocation" value="true" /> should be set in the
 *         struts.xml file
 * 
 */

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Jsp.Master.FLIE_UPLOAD_JSP, value = S2Constants.Jsp.Master.FLIE_UPLOAD_JSP)
public class ShowUploadAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ShowUploadAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();
	private File mealImage; // The actual file
	private File mealThumbnailImage; // The actual file
	private File ssrImage;
	private File ssrThumbnailImage;
	private File fileHOTFile;
	private String succesMsg;
	private String msgType;
	private String mode;
	private String fileName = "";
	private String lang = "";

	public File getSsrImage() {
		return ssrImage;
	}

	public void setSsrImage(File ssrImage) {
		this.ssrImage = ssrImage;
	}

	public File getSsrThumbnailImage() {
		return ssrThumbnailImage;
	}

	public void setSsrThumbnailImage(File ssrThumbnailImage) {
		this.ssrThumbnailImage = ssrThumbnailImage;
	}

	public File getMealImage() {
		return mealImage;
	}

	public void setMealImage(File mealImage) {
		this.mealImage = mealImage;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String execute() {
		try {
			AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
			String fullFileName = null;
			File theFile = null;
			boolean outputMessageSet = false;

			if ("mealImage".equals(mode)) {
				checkFileExtention("mealImage");
				fullFileName = adminCon.getMealImages() + "mealImage_test.jpg";
				theFile = new File(fullFileName);
				FileUtils.copyFile(mealImage, theFile);
			} else if ("mealThumbnailImage".equals(mode)) {
				checkFileExtention("mealThumbnailImage");
				fullFileName = adminCon.getMealImages() + "mealThumbnailImage_test.jpg";
				theFile = new File(fullFileName);
				FileUtils.copyFile(mealThumbnailImage, theFile);
			} else if ("ssrImage".equals(mode)) {
				checkFileExtention("ssrImage");
				fullFileName = adminCon.getMealImages() + "ssrImage_test.jpg";
				theFile = new File(fullFileName);
				FileUtils.copyFile(ssrImage, theFile);
			} else if ("ssrThumbnailImage".equals(mode)) {
				checkFileExtention("ssrThumbnailImage");
				fullFileName = adminCon.getMealImages() + "ssrThumbnailImage_test.jpg";
				theFile = new File(fullFileName);
				FileUtils.copyFile(ssrThumbnailImage, theFile);
			} else if ("bundle".equals(mode)) {
				isPNGFlie("fileHOTFile");
				fullFileName = adminCon.getMealImages() + "bundledFare_test_" + lang + BundledFareConstants.IMG_SUFFIX;
				theFile = new File(fullFileName);
				FileUtils.copyFile(fileHOTFile, theFile);
			} else if ("stripe".equals(mode)) {
				isPNGFlie("fileHOTFile");
				fullFileName = adminCon.getMealImages() + BundledFareConstants.IMG_STRIPE_PREFIX + "test_" + lang
						+ BundledFareConstants.IMG_STRIPE_SUFFIX;
				theFile = new File(fullFileName);
				FileUtils.copyFile(fileHOTFile, theFile);
			} else if (LogicalCabinClassDTO.TEMP_LCC_PREFIX.equals(mode)
					|| LogicalCabinClassDTO.TEMP_LCC_WITH_FLEXI_PREFIX.equals(mode)) {
				isPNGFlie("fileHOTFile");
				fullFileName = adminCon.getMealImages() + mode + "_test_" + lang + LogicalCabinClassDTO.IMG_SUFFIX;
				theFile = new File(fullFileName);
				FileUtils.copyFile(fileHOTFile, theFile);
			} else if ("hotFileUploadNew".equals(mode) || "hotFileUploadNProcess".equals(mode)) {

				boolean uploadSuccess = false;
				boolean processSuccess = false;
				try {
					theFile = new File(PlatformConstants.getAbsBSPDownloadPath() + File.separatorChar + fileName);

					FileUtils.copyFile(fileHOTFile, theFile);
					UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
					BSPHOTFileLog hotFile = new BSPHOTFileLog();
					hotFile.setFileName(fileName);
					hotFile.setUploadedBy(userPrincipal.getUserId());
					hotFile.setUploadedOn(new Date());
					hotFile.setFileStatus(BSPHOTFileLog.HOT_FILE_STATUS.UPLOADED.toString());
					ModuleServiceLocator.getInvoicingBD().saveOrUpdateBSPHOTFile(hotFile);
					uploadSuccess = true;
				} catch (Exception e) {
					log.error("Error in uploading Hot file " + e);
					this.msgType = WebConstants.MSG_ERROR;
					this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.upload.failed");
					if (e instanceof ModuleException) {
						if (((ModuleException) e).getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
							// HOT file with the same name exists
							this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.upload.file.exisits");
							theFile.delete();
						}
					} else if (e instanceof IOException) {
						this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.upload.failed");
					} 
				}

				if (uploadSuccess && "hotFileUploadNew".equals(mode)) {
					// Upload success
					this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.upload.success");
					this.msgType = WebConstants.MSG_SUCCESS;
				}

				if (uploadSuccess && "hotFileUploadNProcess".equals(mode)) {

					try {
						ModuleServiceLocator.getInvoicingBD()
								.processBSPHOTFile(FileUtils.readFileToString(fileHOTFile), fileName);
						processSuccess = true;
					} catch (Exception e) {
						log.error(e);
						this.msgType = WebConstants.MSG_ERROR;
						if (e instanceof ModuleException) {
							this.succesMsg = airadminConfig.getMessage(((ModuleException) e).getExceptionCode());
						} else if (e instanceof IOException) {
							this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.read.failed");
						} else {
							this.succesMsg = airadminConfig.getMessage(e.getMessage());
						}
					}

					if (processSuccess) {
						this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.upload.processing.successful");
						this.msgType = WebConstants.MSG_SUCCESS;
					}
				}
				outputMessageSet = true;
			}

			if (!outputMessageSet) {
				this.succesMsg = "upload success";
				this.msgType = WebConstants.MSG_SUCCESS;
			}

		} catch (Exception e) {
			log.error("Error In uploead " + e.toString());
			this.succesMsg = e.getMessage();
			this.msgType = WebConstants.MSG_ERROR;
		}
		log.debug("Finish Upload");
		request.setAttribute("returnData", "var responseText = new Array();responseText['msgType']= '" + this.msgType
				+ "';responseText['succesMsg']= '" + this.succesMsg + "';responseText['fileName']= '" + this.fileName
				+ "';responseText['lang']= '" + this.lang + "'");
		return S2Constants.Jsp.Master.FLIE_UPLOAD_JSP;
	}

	private void checkFileExtention(String fileToBeChecked) {
		// Other wise this is an empty file upload request. (No File Detected.
		// ). No need to validate the format.
		if (ServletActionContext.getRequest() instanceof MultiPartRequestWrapper) {
			Collection<String> allowedFormats = AppSysParamsUtil.getAllowedImageFormatsForUpload();
			MultiPartRequestWrapper requestWrapper = ((MultiPartRequestWrapper) ServletActionContext.getRequest());
			if (!FilenameUtils.isExtension(requestWrapper.getFileNames(fileToBeChecked)[0], allowedFormats)) {
				throw new IllegalArgumentException("File type must be of types " + allowedFormats);
			}
		}
	}

	private void isPNGFlie(String fileToBeChecked) {
		if (ServletActionContext.getRequest() instanceof MultiPartRequestWrapper) {
			MultiPartRequestWrapper requestWrapper = ((MultiPartRequestWrapper) ServletActionContext.getRequest());
			if (!FilenameUtils.isExtension(requestWrapper.getFileNames(fileToBeChecked)[0], "png")) {
				throw new IllegalArgumentException("File type must be of type PNG");
			}
		}
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public File getMealThumbnailImage() {
		return mealThumbnailImage;
	}

	public void setMealThumbnailImage(File mealThumbnailImage) {
		this.mealThumbnailImage = mealThumbnailImage;
	}

	public File getFileHOTFile() {
		return fileHOTFile;
	}

	public void setFileHOTFile(File fileHOTFile) {
		this.fileHOTFile = fileHOTFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getLang() {
		return lang;
	}
}
