package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.criteria.AdminFeeAgentSearchCriteria;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AdminFeeAgentHTMLGenerator {

	private static Log log = LogFactory.getLog(AdminFeeAgentHTMLGenerator.class);

	private static String clientErrors;
	
	private static final String PARAM_SEARCH_ADMIN_FEE_AGENT = "txtAdminFeeAgent";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_RECORD_NUMBER = "hdnRecNo";

	private String strFormFieldsVariablesJS = "";

	@SuppressWarnings("unchecked")
	public final String getAgentRowHtml(HttpServletRequest request) throws ModuleException {

		String strUIModeJS = "var isSearchMode = false;";

		log.debug("inside AdminFeeAgentHTMLGenerator.getAdminFeeAgentRowHtml()");
		String strUIMode = request.getParameter(PARAM_UI_MODE);

		String strAgentCode = "";

		List<Agent> list = null;

		int recordNo = 0;
		if (request.getParameter(PARAM_RECORD_NUMBER) != null && !request.getParameter(PARAM_RECORD_NUMBER).equals(""))
			recordNo = Integer.parseInt(request.getParameter(PARAM_RECORD_NUMBER)) - 1;
		Page page = null;

		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";

			AdminFeeAgentSearchCriteria criteria = new AdminFeeAgentSearchCriteria();

			strAgentCode = request.getParameter(PARAM_SEARCH_ADMIN_FEE_AGENT);
			strFormFieldsVariablesJS += "var agentCode ='';";

			if (strAgentCode != null && !strAgentCode.equals("")) {
				criteria.setAgentCode(strAgentCode.trim());
			}

			page = ModuleServiceLocator.getTravelAgentBD().getActiveAgents(criteria, recordNo, 20);
			
			totalRecords = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			list = (List<Agent>) page.getPageData();

		} else {

			strUIModeJS = "var isSearchMode = false;";

			strFormFieldsVariablesJS += "var activeStatus ='';";
			strFormFieldsVariablesJS += "var agentCode ='';";

			page = ModuleServiceLocator.getTravelAgentBD().getActiveAgents(new AdminFeeAgentSearchCriteria(), recordNo, 20);
			totalRecords = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			list = (List<Agent>) page.getPageData();
			log.debug("inside AdminFeeAgentHTMLGenerator.getAdminFeeAgentRowHtml SEARCH else condition()");
		}

		setFormFieldValues(strFormFieldsVariablesJS);
		return createAdminFeeAgentRowHTML(list);
	}

	private String createAdminFeeAgentRowHTML(Collection<Agent> agents) {

		List<Agent> list = (List<Agent>) agents;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer();

		String strMyArr = "var arrData = new Array();";
		Agent agent = null;

		for (int i = 0; i < listArr.length; i++) {
			agent = (Agent) listArr[i];

			strMyArr += "arrData[" + i + "] = new Array();";
			
			if (agent.getAgentCode() != null) {
				strMyArr += "arrData[" + i + "][2] = '" + agent.getAgentCode() + "';";
			} else {
				strMyArr += "arrData[" + i + "][2] = '';";
			}

		}
		sb.append(strMyArr);
		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.sitaaddress.form.id.required", "sitaIdRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.airport.required", "sitaAirportRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.location.required", "sitaLocationRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.carriercode.required", "sitaCarrierCodeRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.address.required", "sitaAddressOrEmailRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.delivery.method.required", "deliveryMethodRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.add.required", "sitaAddressRqrd");
			// moduleErrs.setProperty("um.sitaaddress.form.type.required",
			// "sitaTypeRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.activestatus.required", "sitaActiveStatusRqrd");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.sitaaddress.form.email.invalid", "invalidEmail");
			moduleErrs.setProperty("um.sitaaddress.form.email.only.one", "onlyOneEmail");
			moduleErrs.setProperty("um.sitaaddress.from.pnladl.or.palcal.requires", "pnladlOrpalcalRqrd");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {

		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {

		return strFormFieldsVariablesJS;
	}
}
