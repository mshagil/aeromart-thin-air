package com.isa.thinair.airadmin.core.web.v2.action.fla;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.CosWiseDataDTO;
import com.isa.thinair.airadmin.core.web.v2.dto.FlaExtraFlightDataDTO;
import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchFlightLoadAction {
	private static Log log = LogFactory.getLog(SearchFlightLoadAction.class);
	private static String DDMMYYYY = "dd/MM/yyyy";
	private static String HH24SS = "kk:mm:ss";

	String strOrg;
	String strDest;
	String strDateStart;
	String strDateEnd;
	int intLen;
	int totRec = -1;
	int recNo = 0;
	private String succesMsg;
	private String msgType;
	private boolean success;
	String arrRetData;
	/*
	 * Keep Flight ID in a String to overcome the JSON parse error, related to JIRA: 5262
	 */
	HashMap<String, FlaExtraFlightDataDTO> addtionalDetailMap = new HashMap<String, FlaExtraFlightDataDTO>();

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		try {

		} catch (Exception ex) {
			strForward = WebConstants.FORWARD_ERROR;
		}
		return strForward;
	}

	/**
	 * <p>
	 * Search Flights
	 * </p>
	 * 
	 * @return String array arrRetData
	 */
	public String searchFlights() {
		if (log.isDebugEnabled()) {
			log.debug("inside SearchFlightLoadAction.searchFlights search values");
		}
		String arrData = new String();
		Page page = null;
		Page<String> pageFlghtNo = null;
		Collection<FlightInventorySummaryDTO> flightCol = null;
		List<String> flightNoCol = null;
		if (totRec < 0) {
			totRec = 0;
		}
		if (recNo < 0) {
			recNo = 0;
		}

		FlightSearchCriteria flightSearchCriteria = new FlightSearchCriteria();

		// Setting seat factor to search for Overloaded flights - AARESAA-4594 (Issue 6 )
		flightSearchCriteria.setSeatFactor(new int[] { 0, 1000 });

		// preparing and setting the departureDateFrom date
		if (ScheduleUtils.isNotEmptyOrNull(this.strDateStart)) {
			Date date = stringToDate(strDateStart);
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			flightSearchCriteria.setDepartureDateFrom(calendar.getTime());

		}
		if (ScheduleUtils.isNotEmptyOrNull(this.strDateEnd)) {
			Date dateStart = stringToDate(strDateStart);
			Date modEnd = CalendarUtil.addDateVarience(dateStart, 6);
			Date dateEnd = stringToDate(strDateEnd);
			if (CalendarUtil.isGreaterThan(modEnd, dateEnd)) {
				flightSearchCriteria.setDepartureDateTo(dateEnd);
			} else {
				flightSearchCriteria.setDepartureDateTo(modEnd);
			}

		}

		if (ScheduleUtils.isNotEmptyOrNull(this.strOrg)) {
			flightSearchCriteria.setOrigin(this.strOrg);
		}

		if (ScheduleUtils.isNotEmptyOrNull(this.strDest)) {
			flightSearchCriteria.setDestination(this.strDest);
		}

		// setting local time to get data from retrieving airport
		flightSearchCriteria.setTimeInLocal(true);
		try {
			pageFlghtNo = ModuleServiceLocator.getDataExtractionBD().getFlightsForFlightLoadAnalysis(flightSearchCriteria, recNo,
					25);
			flightNoCol = (List) pageFlghtNo.getPageData();
			flightSearchCriteria.setFlightNumbers(flightNoCol);
			page = ModuleServiceLocator.getDataExtractionBD().searchFlightsForInventoryUpdation(flightSearchCriteria, 0, 300);

			if (page != null && (pageFlghtNo.getTotalNoOfRecords() < recNo)) {
				int totRec = flightNoCol.size();
				int pageNo = totRec / 25;
				recNo = (pageNo - 1) * 25;
				if (recNo < 0) {
					recNo = 0;
				}
				pageFlghtNo = ModuleServiceLocator.getDataExtractionBD().getFlightsForFlightLoadAnalysis(flightSearchCriteria,
						recNo, 25);
				flightNoCol = (List) pageFlghtNo.getPageData();
				flightSearchCriteria.setFlightNumbers(flightNoCol);
				page = ModuleServiceLocator.getDataExtractionBD().searchFlightsForInventoryUpdation(flightSearchCriteria, 0, 300);
			}
			success = true;
		} catch (ModuleException me) {
			success = false;
			msgType = WebConstants.MSG_ERROR;
			succesMsg = AiradminConfig.getServerMessage(me.getExceptionCode());
		}
		// get the page data
		if (page != null) {
			flightCol = page.getPageData();
			totRec = pageFlghtNo.getTotalNoOfRecords();
			intLen = flightNoCol.size();
		}
		arrData = createGridData(flightCol, stringToDate(strDateStart), flightNoCol);
		this.arrRetData = arrData;
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @param flightCol
	 * @param pageSize
	 * @return
	 */
	private String createGridData(Collection<FlightInventorySummaryDTO> flightCol, Date startDate, List<String> flightNoCol) {

		StringBuffer sb = new StringBuffer();
		Map<String, ArrayList<FlightInventorySummaryDTO>> flightInvMap = null;

		if (flightCol != null && !flightCol.isEmpty()) {
			sb.append("arrData = new Array();");
			flightInvMap = createFlightNumberDTOmap(flightCol, flightNoCol);
			if (flightInvMap != null) {
				Set<String> keySet = flightInvMap.keySet();
				Integer cntFlight = new Integer(0);
				for (String key : keySet) {
					List<FlightInventorySummaryDTO> flightList = flightInvMap.get(key);
					sb.append(arrangeFlightData(flightList, startDate, cntFlight));
					cntFlight++;
				}
			} else {
				sb.append("arrData  = null;");
			}
		} else {
			sb.append("arrData  = null;");
		}

		return sb.toString();
	}

	/**
	 * <p>
	 * arrange flights suitable for the front end
	 * </p>
	 * 
	 * @param flightList
	 * @return
	 */
	private String arrangeFlightData(List<FlightInventorySummaryDTO> flightList, Date startDate, Integer cntFlight) {
		final int days = 7;
		Integer flightID = null;
		String flightNumber = null;
		String origin = null;
		String dest = null;
		Integer capacityAD = null;
		String status = "";
		String strDispStatus = "";
		String strDispF = "";
		Date depDate = null;
		/*
		 * Keep Flight ID in a String to overcome the JSON parse error, related to JIRA: 5262
		 */
		HashMap<String, FlaExtraFlightDataDTO> extraDetailMap = new HashMap<String, FlaExtraFlightDataDTO>();
		StringBuffer sb = new StringBuffer();
		/* Added by Lalanthi JIRA: 4932 */
		int maxSoldAdultCountForLeg = 0;
		int totalAdlutAllocForAllBCs = 0;

		Map<String, String[]> daySeqMap = getDateSequenceMap(startDate, days);
		for (FlightInventorySummaryDTO flightInvSumm : flightList) {
			FlaExtraFlightDataDTO extraFlghtData = new FlaExtraFlightDataDTO();

			flightID = flightInvSumm.getFlightId();
			flightNumber = flightInvSumm.getFlightNumber();
			origin = flightInvSumm.getDeparture();
			dest = flightInvSumm.getArrival();
			status = flightInvSumm.getFlightStatus();
			depDate = flightInvSumm.getDepartureDateTimeLocal();
			Integer soldAdultSeatTot = new Integer(0);

			extraFlghtData.setFlightID(flightID);
			extraFlghtData.setDepartDate(dateToString(depDate, HH24SS));
			extraFlghtData.setStatus(status);
			if (flightInvSumm.getDepartureDateTimeZulu().compareTo(new Date()) < 0) {
				extraFlghtData.setFlown(true);
			} else {
				extraFlghtData.setFlown(false);
			}
			totalAdlutAllocForAllBCs = 0;
			List<FCCInventorySummaryDTO> fccInvenSumLst = flightInvSumm.getFccInventorySummaryDTO();
			if (fccInvenSumLst != null && !fccInvenSumLst.isEmpty()) {
				HashMap<String, CosWiseDataDTO> cosWiseDataMap = new HashMap<String, CosWiseDataDTO>();
				Integer soldnOHDcnt = null;
				Map<String, Integer> segWiseAdultsold = new HashMap<String, Integer>();
				for (FCCInventorySummaryDTO fccInvSumm : fccInvenSumLst) {
					CosWiseDataDTO cosData = new CosWiseDataDTO();
					cosData.setCosClass(fccInvSumm.getCabinClassCode());
					capacityAD = fccInvSumm.getEffectiveAdultBaseCapacity();
					totalAdlutAllocForAllBCs += capacityAD;
					int cnfCnt = 0;
					int ohdCnt = 0;
					int infCnfCnt = 0;
					int infOhdCnt = 0;
					List<FCCSegInvSummaryDTO> fccSeginvLst = fccInvSumm.getFccSegInventorySummary();

					if (fccSeginvLst != null && !fccSeginvLst.isEmpty()) {
						for (FCCSegInvSummaryDTO fccSegInvSumm : fccSeginvLst) {
							soldnOHDcnt = fccSegInvSumm.getAdultSold() + fccSegInvSumm.getAdultOnhold();
							// soldAdultSeatTot += soldnOHDcnt;
							cnfCnt += fccSegInvSumm.getAdultSold();
							ohdCnt += fccSegInvSumm.getAdultOnhold();
							infCnfCnt += fccSegInvSumm.getInfantSold();
							infOhdCnt += fccSegInvSumm.getInfantOnhold();
							/*
							 * Keep the segment wise sold adult seats count to get load details correctly for a
							 * multi-leg flight
							 */
							if (segWiseAdultsold.get(fccSegInvSumm.getSegmentCode()) != null) {
								int soldAdultForSeg = segWiseAdultsold.get(fccSegInvSumm.getSegmentCode());
								segWiseAdultsold.put(fccSegInvSumm.getSegmentCode(), soldAdultForSeg + soldnOHDcnt);
							} else {
								segWiseAdultsold.put(fccSegInvSumm.getSegmentCode(), soldnOHDcnt);
							}
						}
						cosData.setCnfCnt(cnfCnt);
						cosData.setOhdCnt(ohdCnt);
						cosData.setInfCnfCnt(infCnfCnt);
						cosData.setInfOhdCnt(infOhdCnt);
					}
					cosWiseDataMap.put(fccInvSumm.getCabinClassCode(), cosData);
					extraFlghtData.setCosWiseData(cosWiseDataMap);
				}

				maxSoldAdultCountForLeg = getMaxAdultsSoldForASeg(segWiseAdultsold);
				double percentage = (maxSoldAdultCountForLeg * 100) / totalAdlutAllocForAllBCs;
				// double oldpercentage = (soldAdultSeatTot*100)/capacityAD;
				BigDecimal percntg = new BigDecimal(percentage);
				strDispStatus = percntg.toString();
			}

			extraDetailMap.put(Integer.toString(flightID), extraFlghtData);
			addtionalDetailMap.putAll(extraDetailMap);
			Date tempDepartDate = flightInvSumm.getDepartureDateTime();
			String strTmpDepDate = dateToString(tempDepartDate, DDMMYYYY);
			if (ScheduleUtils.isNotEmptyOrNull(status)) {
				if (status.equals("ACT")) {
					strDispF = "A";
				} else if (status.equals("CRE")) {
					strDispF = "C";
				} else if (status.equals("CNX")) {
					strDispF = "X";
				} else if (status.equals("CLS")) {
					strDispF = "S";
				}
			}

			if (ScheduleUtils.isNotEmptyOrNull(strTmpDepDate) && daySeqMap.containsKey(strTmpDepDate)) {
				String[] invData = { maxSoldAdultCountForLeg + "/" + totalAdlutAllocForAllBCs, strDispStatus, strDispF,
						flightID != null ? flightID.toString() : null };
				daySeqMap.put(strTmpDepDate, invData);
			}

		}

		sb.append("arrData[" + cntFlight + "]  = new Array(" + cntFlight + ",'" + flightNumber + "','" + origin + "','" + dest
				+ "', '" + status + "', '', '" + flightID + "','');");
		sb.append("arrData[" + cntFlight + "][5] = new Array();");
		sb.append("arrData[" + cntFlight + "][7] = new Array();");

		for (int dayCnt = 0; dayCnt < 7; dayCnt++) {
			Date addedDate = addDays(startDate, dayCnt);
			String strDate = dateToString(addedDate, DDMMYYYY);
			if (daySeqMap.containsKey(strDate)) {
				String[] strData = daySeqMap.get(strDate);
				if (strData.length == 4) {
					sb.append("arrData[" + cntFlight + "][5]['" + strDate + "'] = new Array('" + strData[0] + "', '" + strData[1]
							+ "', '" + strData[2] + "');");
					sb.append("arrData[" + cntFlight + "][7]['" + strDate + "'] = new Array('" + strData[3] + "');");
				} else {
					sb.append("arrData[" + (cntFlight) + "][5]['" + strDate + "'] = new Array();");
					sb.append("arrData[" + (cntFlight) + "][7]['" + strDate + "'] = new Array('');");
				}
			} else {
				sb.append("arrData[" + (cntFlight) + "][5]['" + strDate + "'] = new Array('&nbsp;','-1','');");
				sb.append("arrData[" + (cntFlight) + "][7]['" + strDate + "'] = new Array('');");
			}

		}

		return sb.toString();
	}

	private Map<String, String[]> getDateSequenceMap(Date startDate, int days) {
		Map<String, String[]> daySeqMap = new HashMap<String, String[]>();
		String[] strArray = new String[3];
		for (int i = 0; i < days; i++) {
			Date seqDate = addDays(startDate, i);
			daySeqMap.put(dateToString(seqDate, DDMMYYYY), strArray);
		}
		return daySeqMap;
	}

	private Date addDays(Date date, int dayCnt) {
		if (date != null && dayCnt > -1) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int day = cal.get(Calendar.DATE);
			Calendar calDum = new GregorianCalendar();
			calDum.set(GregorianCalendar.YEAR, cal.get(Calendar.YEAR));
			calDum.set(GregorianCalendar.MONTH, cal.get(Calendar.MONTH));
			calDum.set(GregorianCalendar.DATE, (day + dayCnt));
			return calDum.getTime();
		} else {
			return date;
		}
	}

	/**
	 * <p>
	 * create flightInvSummDTO map keeping key as flightNo
	 * </p>
	 * 
	 * @param flightCol
	 * @return
	 */
	private Map<String, ArrayList<FlightInventorySummaryDTO>> createFlightNumberDTOmap(
			Collection<FlightInventorySummaryDTO> flightCol, List<String> flightNoCol) {
		Map<String, ArrayList<FlightInventorySummaryDTO>> flightInvMap = null;
		if (flightNoCol != null && !flightNoCol.isEmpty()) {
			flightInvMap = new LinkedHashMap<String, ArrayList<FlightInventorySummaryDTO>>();
			for (String flightNo : flightNoCol) {
				ArrayList<FlightInventorySummaryDTO> flghtLst = new ArrayList<FlightInventorySummaryDTO>();
				flightInvMap.put(flightNo, flghtLst);
			}
		}

		if (flightCol != null && !flightCol.isEmpty()) {
			if (flightInvMap != null) {
				for (FlightInventorySummaryDTO flightInvSumm : flightCol) {
					// only consider active flights
					if (!"ACT".equals(flightInvSumm.getFlightStatus())) {
						continue;
					}

					String tmpFlightNo = flightInvSumm.getFlightNumber();
					if (flightInvMap.containsKey(tmpFlightNo)) {
						flightInvMap.get(tmpFlightNo).add(flightInvSumm);
					} else {
						ArrayList<FlightInventorySummaryDTO> flghtLst = new ArrayList<FlightInventorySummaryDTO>();
						flghtLst.add(flightInvSumm);
						flightInvMap.put(tmpFlightNo, flghtLst);
					}
				}
			}
		}
		return flightInvMap;
	}

	/**
	 * <p>
	 * Checks if two calendars represent the same day ignoring time.
	 * </p>
	 * 
	 * @param date1
	 *            the first date, not altered, not null
	 * @param date2
	 *            the second date, not altered, not null
	 * @return true if they represent the same day
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		if (date1 == null && date2 == null) {
			throw new IllegalArgumentException();
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1
				.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
	}

	public Date stringToDate(String strDate) {
		Date date = null;
		if (strDate != null && !"".equals(strDate)) {
			String[] arrDate = strDate.split("/");
			Calendar cal = new GregorianCalendar();
			cal.set(new Integer(arrDate[2]), new Integer(arrDate[1]) - 1, new Integer(arrDate[0]));
			date = new Date(cal.getTimeInMillis());
		}
		return date;
	}

	public String dateToString(Date date, String format) {
		String formattedDate = null;
		if (date != null && format != null && !format.equals("")) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			formattedDate = dateFormat.format(date);
		}
		return formattedDate;
	}

	public Date addDays(String strDate, int days) {
		Date date = null;
		if (strDate != null && !"".equals(strDate)) {
			String[] arrDate = strDate.split("/");
			Calendar cal = new GregorianCalendar();
			int day = new Integer(arrDate[0]).intValue() + days;
			cal.set(new Integer(arrDate[2]), new Integer(arrDate[1]) - 1, day);
			date = new Date(cal.getTimeInMillis());
		}
		return date;
	}

	/**
	 * Added by Lalanthi JIRA: 4932
	 * 
	 * @param segWiseAdultsold
	 * @return
	 */
	private Integer getMaxAdultsSoldForASeg(Map<String, Integer> segWiseAdultsold) {
		int maxAdultsSold = 0;
		if (segWiseAdultsold != null && segWiseAdultsold.size() > 0) {
			for (String segCode : segWiseAdultsold.keySet()) {
				int segMaxSold = segWiseAdultsold.get(segCode);
				if (segMaxSold > maxAdultsSold) {
					maxAdultsSold = segMaxSold;
				}
			}
		}

		return maxAdultsSold;
	}

	public String getStrOrg() {
		return strOrg;
	}

	public void setStrOrg(String strOrg) {
		this.strOrg = strOrg;
	}

	public String getStrDest() {
		return strDest;
	}

	public void setStrDest(String strDest) {
		this.strDest = strDest;
	}

	public String getStrDateStart() {
		return strDateStart;
	}

	public int getTotRec() {
		return totRec;
	}

	public void setTotRec(int totRec) {
		this.totRec = totRec;
	}

	public int getRecNo() {
		return recNo;
	}

	public void setRecNo(int recNo) {
		this.recNo = recNo;
	}

	public void setStrDateStart(String strDateStart) {
		this.strDateStart = strDateStart;
	}

	public String getStrDateEnd() {
		return strDateEnd;
	}

	public void setStrDateEnd(String strDateEnd) {
		this.strDateEnd = strDateEnd;
	}

	public String getArrRetData() {
		return arrRetData;
	}

	public void setArrRetData(String arrRetData) {
		this.arrRetData = arrRetData;
	}

	public int getIntLen() {
		return intLen;
	}

	public void setIntLen(int intLen) {
		this.intLen = intLen;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public HashMap<String, FlaExtraFlightDataDTO> getAddtionalDetailMap() {
		return addtionalDetailMap;
	}

	public void setAddtionalDetailMap(HashMap<String, FlaExtraFlightDataDTO> addtionalDetailMap) {
		this.addtionalDetailMap = addtionalDetailMap;
	}
}
