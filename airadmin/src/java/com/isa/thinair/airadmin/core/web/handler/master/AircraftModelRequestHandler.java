package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.AircraftModelHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Shakir
 * 
 * 
 */

public final class AircraftModelRequestHandler extends BasicRequestHandler {

	private static AiradminConfig adminConfig = new AiradminConfig();

	private static Log log = LogFactory.getLog(AircraftModelRequestHandler.class);

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_FLEET = "txtFleet";
	private static final String PARAM_DESC = "txtDesc";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_COSCAPACITIES = "hdnCOSCapacities";
	private static final String PARAM_HDNMODEL = "hdnModel";
	private static final String PARAM_ACTION = "hdnMode";
	// Haider 16Sep08
	// private static final String PARAM_MASTER_MODEL_CODE = "masterModelCode";
	private static final String PARAM_IATA_AIRCRAFT_TYPE = "iataAircraftType";
	private static final String PARAM_SSR_TEMPLATE = "selSSRTemplate";
	private static final String PARAM_AIRCRAFT_TYPE_CODE = "selAircraftTypeCode";
	private static final String PARAM_ITINERARY_DESC = "txtItinDesc";
	private static final String PARAM_IS_DEFAULT_IATA_AIRCRAFT = "defaultIataAircraft";
	

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (adminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Main execute Method for AircraftModel Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnModel = request.getParameter("hdnModel");
		String strAction = request.getParameter("hdnMode");
		String strMainAction = request.getParameter("hdnMainMode");

		if (strMainAction != null && strMainAction.equals("checkFlights")) {
			setExceptionOccured(request, true);
			setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			try {
				checkFlightData(request);
			} catch (Exception exception) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				log.error("Exception in AircraftModelRequestHandler:execute()", exception);
			}
			setDisplayData(request);
		} else {
			String strFormData = "var arrFormData = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			setExceptionOccured(request, false);
			setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			try {
				if (strHdnModel != null && strHdnModel.equals(WebConstants.ACTION_SAVE)) {
					log.debug("\nAIRCRAFTMODELREQUESTHANDLER SAVEDATA() SUCCESS");
					if (strAction.equals(WebConstants.ACTION_ADD)) {
						checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRMODEL_ADD);
						setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
						saveData(request);
					} else if (strAction.equals(WebConstants.ACTION_EDIT)) {
						checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRMODEL_EDIT);
						setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
						saveData(request);
					}
				}
			} catch (Exception exception) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				log.error("Exception in AircraftModelRequestHandler:execute()", exception);
			}

			if (strHdnModel != null && strHdnModel.equals(WebConstants.ACTION_DELETE)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRMODEL_DELETE);
				deleteData(request);
				log.debug("\nAircraftModelRequestHandler DELETEDATA() SUCCESS");
			}
			setDisplayData(request);
		}
		return forward;
	}

	/**
	 * Sets the DisplayData to the AircraftModel Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	public static void setDisplayData(HttpServletRequest request) {
		try {
			JavascriptGenerator.createCabinClasses(request);
			setClientErrors(request);
			setAircraftModelRowHtml(request);
			setCabinClassHtml(request);
			setSSRTemplatesHtml(request);
			setAircraftTypeCodeHtml(request);
			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

			if (request.getParameter(PARAM_HDNMODEL) == null) {
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
						getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));

			} else if (request.getParameter(PARAM_HDNMODEL) != null
					&& !request.getParameter(PARAM_HDNMODEL).equals(WebConstants.ACTION_SAVE)) {

				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
						getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
			} else {

				request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
						getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
			}
			request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
					+ isSuccessfulSaveMessageDisplayEnabled() + ";");

			if (!isExceptionOccured(request)) {
				String strFormData = "var arrFormData = new Array();";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			}
			boolean isEnableDowngrade = AppSysParamsUtil.enableDowngradeAircraftModel();
			setAttribInRequest(request, "isEnableDowngrade", "var isEnableDowngrade = " + isEnableDowngrade + ";");
			String strMainAction = request.getParameter("hdnMainMode");
			if (strMainAction != null && strMainAction.equals("checkFlights")) {
				setAttribInRequest(request, "isNeedChecked", "var isNeedChecked = " + false + ";");
			} else {
				setAttribInRequest(request, "isNeedChecked", "var isNeedChecked = " + true + ";");
			}
			boolean isIATAAircraftTypeMandatory =  AppSysParamsUtil.isIATAAircraftTypeMandatory();
			setAttribInRequest(request, "isIATAAircraftTypeMandatory", "var isIATAAircraftTypeMandatory = " + isIATAAircraftTypeMandatory + ";");

		} catch (Exception e) {
			log.error("Exception in AircraftModelRequestHandler:setDisplayData()", e);
		}
	}

	/**
	 * Saves Add/Update the Air craft Model
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void saveData(HttpServletRequest request) throws Exception {

		String strVersion = null;
		String strFleet = null;
		String strDesc = null;
		String strStatus = null;
		String strCOSCapacities = null;
		String strHdnModel = null;
		String strAction = null;
		String ssrTemplateId = null;
		// String masterModelCode = null;
		String iataAircraftType = null;
		String strItineraryDesc = null;
		String aircraftTypeCode = null;
		String defaultIataAircraft = null;
		try {
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strFleet = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLEET));
			strDesc = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESC));
			strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
			strCOSCapacities = AiradminUtils.getNotNullString(request.getParameter(PARAM_COSCAPACITIES));
			strHdnModel = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDNMODEL));
			strAction = AiradminUtils.getNotNullString(request.getParameter(PARAM_ACTION));
			ssrTemplateId = AiradminUtils.getNotNullString(request.getParameter(PARAM_SSR_TEMPLATE));

			aircraftTypeCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRCRAFT_TYPE_CODE));
			strItineraryDesc = AiradminUtils.getNotNullString(request.getParameter(PARAM_ITINERARY_DESC));

			// Haider 16Sep08
			// masterModelCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MASTER_MODEL_CODE));
			iataAircraftType = AiradminUtils.getNotNullString(request.getParameter(PARAM_IATA_AIRCRAFT_TYPE));
			defaultIataAircraft = AiradminUtils.getNotNullString(request.getParameter(PARAM_IS_DEFAULT_IATA_AIRCRAFT));
			
			String strValue = strCOSCapacities;
			AircraftModel AM;
			if (strVersion.equals("")) {
				// New Model
				AM = new AircraftModel();
				Set<AircraftCabinCapacity> set1 = new HashSet<AircraftCabinCapacity>();
				if (strValue.indexOf(":") != -1) {
					String[] arrCOS = strValue.split(":");
					for (int x = 0; x < (arrCOS.length); x++) {
						String strValue1 = arrCOS[x];
						if (strValue1.indexOf("/") != -1) {
							String[] arrCOSDetails = strValue1.split("/");
							AircraftCabinCapacity accNew = new AircraftCabinCapacity();
							accNew.setAircraftModelNumber(strFleet.toUpperCase());
							accNew.setCabinClassCode(arrCOSDetails[2]);
							accNew.setSeatCapacity((new Integer(arrCOSDetails[0])).intValue());
							accNew.setInfantCapacity((new Integer(arrCOSDetails[1])).intValue());
							set1.add(accNew);
						}
					}
				}
				AM.setAircraftCabinCapacitys(set1);
			} else {
				// Existing Model
				AM = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(strFleet);
				if (AM == null) {
					throw new ModuleException("module.hibernate.staledata", MessagesUtil.getMessage("module.hibernate.staledata"));
				}
				Set<AircraftCabinCapacity> setCOSRemove = new HashSet<AircraftCabinCapacity>();
				if (strValue.indexOf(":") != -1) {
					String[] arrCOS = strValue.split(":");
					for (Iterator<AircraftCabinCapacity> iterCOS = AM.getAircraftCabinCapacitys().iterator(); iterCOS.hasNext();) {
						AircraftCabinCapacity existingCOS = (AircraftCabinCapacity) iterCOS.next();
						boolean cosFound = false;
						for (int x = 0; x < (arrCOS.length); x++) {
							String strValue1 = arrCOS[x];
							if (strValue1.indexOf("/") != -1) {
								String[] arrCOSDetails = strValue1.split("/");
								if (arrCOSDetails[4] != null && !arrCOSDetails[4].equals("")
										&& existingCOS.getAcccId() == (new Integer(arrCOSDetails[4])).intValue()) {
									// COS found modify it.

									existingCOS.setVersion((new Integer(arrCOSDetails[5])).intValue());
									existingCOS.setSeatCapacity((new Integer(arrCOSDetails[0])).intValue());
									existingCOS.setInfantCapacity((new Integer(arrCOSDetails[1])).intValue());
									cosFound = true;
									break;
								}
							}
						}
						if (!cosFound) {
							// COS not found Remove it
							setCOSRemove.add(existingCOS);
						}
					}
					AM.getAircraftCabinCapacitys().removeAll(setCOSRemove);
					// Adding newly added Cabin Class Capasity
					for (int x = 0; x < (arrCOS.length); x++) {
						String strValue1 = arrCOS[x];
						if (strValue1.indexOf("/") != -1) {
							String[] arrCOSDetails = strValue1.split("/");

							if (arrCOSDetails[4] == null || arrCOSDetails[4].equals("")) {
								AircraftCabinCapacity acc = new AircraftCabinCapacity();
								acc.setAircraftModelNumber(AM.getModelNumber());
								acc.setCabinClassCode(arrCOSDetails[2]);
								acc.setInfantCapacity((new Integer(arrCOSDetails[1])).intValue());
								acc.setSeatCapacity((new Integer(arrCOSDetails[0])).intValue());
								AM.getAircraftCabinCapacitys().add(acc);
							}
						}
					}

				}

			}

			if (strVersion != null && !"".equals(strVersion)) {
				AM.setVersion(Long.parseLong(strVersion));
			}

			AM.setModelNumber(strFleet.toUpperCase());
			AM.setDescription(strDesc);

			// Haider 16Sep08
			// AM.setMasterModelCode(masterModelCode);
			AM.setIataAircraftType(iataAircraftType);
			if (strStatus.equals("on")) {
				AM.setStatus(Aircraft.STATUS_ACTIVE);
			} else {
				AM.setStatus(Aircraft.STATUS_INACTIVE);
			}

			if (!ssrTemplateId.equals("-1") && !ssrTemplateId.equals("")) {
				AM.setSsrTemplateId(new Integer(ssrTemplateId));
			} else {
				AM.setSsrTemplateId(null);
			}

			if (!aircraftTypeCode.equals("-1") && !aircraftTypeCode.equals("")) {
				AM.setAircraftTypeCode(aircraftTypeCode);
			} else {
				AM.setAircraftTypeCode("");
			}
			
			if ("on".equalsIgnoreCase(defaultIataAircraft)) {
				if (Aircraft.STATUS_ACTIVE.equals(AM.getStatus())) {
					validateDefaultAircraftAlreadyAssigned(iataAircraftType, AM.getModelNumber());
				}
				AM.setDefaultIataAircraft("Y");
			} else {
				AM.setDefaultIataAircraft("N");
			}		
			
			AM.setItineraryDesc(strItineraryDesc);

			ModuleServiceLocator.getAircraftServiceBD().saveAircraftModel(AM);

			saveMessage(request, adminConfig.getMessage("um.airadmin.add.success"), WebConstants.MSG_SUCCESS);

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");

			AM = null;
		} catch (ModuleException moduleException) {
			log.error("Exception in AircraftModelRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);

			setExceptionOccured(request, true);

			strCOSCapacities = AiradminUtils.getNotNullString(request.getParameter(PARAM_COSCAPACITIES));

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strFleet + "';";
			strFormData += "arrFormData[0][2] = '" + strDesc + "';";
			strFormData += "arrFormData[0][3] = '" + strStatus + "';";
			strFormData += "arrFormData[0][4] = '" + strCOSCapacities + "';";
			strFormData += "arrFormData[0][6] = '" + strVersion + "';";
			strFormData += "arrFormData[0][7] = '" + strHdnModel + "';";
			strFormData += "arrFormData[0][8] = '" + strAction + "';";
			strFormData += "arrFormData[0][8] = '" + strAction + "';";
			// strFormData += "arrFormData[0][9] = '" + masterModelCode + "';";
			strFormData += "arrFormData[0][9] = '" + iataAircraftType + "';";
			strFormData += "arrFormData[0][10] = '" + ssrTemplateId + "';";
			strFormData += "arrFormData[0][11] = '" + strItineraryDesc + "';";
			strFormData += "arrFormData[0][12] = '" + aircraftTypeCode + "';";

			strFormData += "arrFormData[0][13] = '" + moduleException.getCustumValidateMessage() + "';";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, adminConfig.getMessage("um.agent.form.code.defined"), WebConstants.MSG_ERROR);
			} else if ("um.aircraft.form.default.iata.defined".equals(moduleException.getExceptionCode())) {
				saveMessage(request, adminConfig.getMessage("um.aircraft.form.default.iata.defined"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		} catch (Exception exception) {
			log.error("Exception in AircraftModelRequestHandler:saveData()", exception);

			setExceptionOccured(request, true);
			strCOSCapacities = AiradminUtils.getNotNullString(request.getParameter(PARAM_COSCAPACITIES));

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				String strFormData = "var arrFormData = new Array();";
				strFormData += "arrFormData[0] = new Array();";
				strFormData += "arrFormData[0][1] = '" + strFleet + "';";
				strFormData += "arrFormData[0][2] = '" + strDesc + "';";
				strFormData += "arrFormData[0][3] = '" + strStatus + "';";
				strFormData += "arrFormData[0][4] = '" + strCOSCapacities + "';";
				strFormData += "arrFormData[0][6] = '" + strVersion + "';";
				strFormData += "arrFormData[0][7] = '" + strHdnModel + "';";
				strFormData += "arrFormData[0][8] = '" + strAction + "';";
				// strFormData += "arrFormData[0][9] = '" + masterModelCode + "';";
				strFormData += "arrFormData[0][9] = '" + iataAircraftType + "';";
				strFormData += "arrFormData[0][10] = '" + ssrTemplateId + "';";
				strFormData += "arrFormData[0][11] = '" + strItineraryDesc + "';";
				strFormData += "arrFormData[0][12] = '" + aircraftTypeCode + "';";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}

	}

	public static void checkFlightData(HttpServletRequest request) throws Exception {

		String strVersion = null;
		String strFleet = null;
		String strDesc = null;
		String strStatus = null;
		String strCOSCapacities = null;
		String strHdnModel = null;
		String strAction = null;
		String ssrTemplateId = null;
		// String masterModelCode = null;
		String iataAircraftType = null;
		String strItineraryDesc = null;
		String aircraftTypeCode = null;
		String flights = null;
		try {

			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strFleet = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLEET));
			strDesc = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESC));
			strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
			strCOSCapacities = AiradminUtils.getNotNullString(request.getParameter(PARAM_COSCAPACITIES));
			strHdnModel = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDNMODEL));
			strAction = AiradminUtils.getNotNullString(request.getParameter(PARAM_ACTION));
			ssrTemplateId = AiradminUtils.getNotNullString(request.getParameter(PARAM_SSR_TEMPLATE));

			aircraftTypeCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRCRAFT_TYPE_CODE));
			strItineraryDesc = AiradminUtils.getNotNullString(request.getParameter(PARAM_ITINERARY_DESC));
			AircraftModel AM = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(strFleet);

			String strValue = strCOSCapacities;
			Set<AircraftCabinCapacity> set1 = new HashSet<AircraftCabinCapacity>();
			if (strValue.indexOf(":") != -1) {
				String[] arrCOS = strValue.split(":");
				for (int x = 0; x < (arrCOS.length); x++) {
					String strValue1 = arrCOS[x];
					if (strValue1.indexOf("/") != -1) {
						String[] arrCOSDetails = strValue1.split("/");
						AircraftCabinCapacity accNew = new AircraftCabinCapacity();
						accNew.setAircraftModelNumber(strFleet.toUpperCase());
						accNew.setCabinClassCode(arrCOSDetails[2]);
						accNew.setSeatCapacity((new Integer(arrCOSDetails[0])).intValue());
						accNew.setInfantCapacity((new Integer(arrCOSDetails[1])).intValue());
						set1.add(accNew);
					}
				}
			}
			AM.setAircraftCabinCapacitys(set1);
			flights = ModuleServiceLocator.getAircraftServiceBD().getAffectedFlights(AM);
			if (!flights.equals("")) {
				// Haider 16Sep08
				// masterModelCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MASTER_MODEL_CODE));
				iataAircraftType = AiradminUtils.getNotNullString(request.getParameter(PARAM_IATA_AIRCRAFT_TYPE));
				String strFormData = "var arrFormData = new Array();";
				strFormData += "arrFormData[0] = new Array();";
				strFormData += "arrFormData[0][1] = '" + strFleet + "';";
				strFormData += "arrFormData[0][2] = '" + strDesc + "';";
				strFormData += "arrFormData[0][3] = '" + strStatus + "';";
				strFormData += "arrFormData[0][4] = '" + strCOSCapacities + "';";
				strFormData += "arrFormData[0][6] = '" + strVersion + "';";
				strFormData += "arrFormData[0][7] = '" + strHdnModel + "';";
				strFormData += "arrFormData[0][8] = '" + strAction + "';";
				// strFormData += "arrFormData[0][9] = '" + masterModelCode + "';";
				strFormData += "arrFormData[0][9] = '" + iataAircraftType + "';";
				strFormData += "arrFormData[0][10] = '" + ssrTemplateId + "';";
				strFormData += "arrFormData[0][11] = '" + strItineraryDesc + "';";
				strFormData += "arrFormData[0][12] = '" + aircraftTypeCode + "';";
				strFormData += "arrFormData[0][13] = '" + flights + "';";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			} else {
				String strFormData = "var arrFormData = new Array();";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveData(request);

			}
		} catch (Exception exception) {
			saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
		}

	}

	/**
	 * Delete the AirCraft Model
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void deleteData(HttpServletRequest request) {
		String strModel = request.getParameter("txtFleet");
		try {
			if (strModel != null) {

				AircraftModel existingAircraftModel = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(strModel);
				if (existingAircraftModel == null)
					ModuleServiceLocator.getAircraftServiceBD().removeAircraftModel(strModel);
				else {
					String strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingAircraftModel.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getAircraftServiceBD().removeAircraftModel(existingAircraftModel);
					} else {
						ModuleServiceLocator.getAircraftServiceBD().removeAircraftModel(strModel);
					}
				}
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
				saveMessage(request, adminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException e) {
			log.error("Exception in AircraftModelRequestHandler:saveData() [origin module=" + e.getModuleDesc() + "]", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = AircraftModelHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Aircraft Model Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setAircraftModelRowHtml(HttpServletRequest request) throws Exception {
		AircraftModelHTMLGenerator htmlGen = new AircraftModelHTMLGenerator();
		String strHtml = htmlGen.getAircraftModelRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
	}

	/**
	 * Sets the Cabinclasses List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setCabinClassHtml(HttpServletRequest request) throws ModuleException {
		String strCabinClassList = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strCabinClassList);
	}

	private static void setSSRTemplatesHtml(HttpServletRequest request) throws ModuleException {
		String ssrTemplateOptStr = "<option value='-1' selected='selected'></option>";
		ssrTemplateOptStr += SelectListGenerator.createSSRTemplateListACT();
		request.setAttribute(S2Constants.RequestConstant.SSR_TEMPLATE_ALL, ssrTemplateOptStr);
	}

	private static void setAircraftTypeCodeHtml(HttpServletRequest request) throws ModuleException {
		String aircraftTypeOptStr = "<option value='-1' selected='selected'></option>";
		aircraftTypeOptStr += SelectListGenerator.createAircraftTypeCodeList();
		request.setAttribute(S2Constants.RequestConstant.AIRCRAFT_TYPE_CODE_ALL, aircraftTypeOptStr);
	}
	
	private static void validateDefaultAircraftAlreadyAssigned(String iataAircraftType, String modelNumber) throws ModuleException {
		boolean isDefaultAircraftAlreadyAssigned = ModuleServiceLocator.getAircraftServiceBD()
				.isDefaultAircraftAssignedForIataAircraftType(iataAircraftType, modelNumber);
		if (isDefaultAircraftAlreadyAssigned) {
			throw new ModuleException("um.aircraft.form.default.iata.defined");
		}
	}

}
