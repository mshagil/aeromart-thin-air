package com.isa.thinair.airadmin.core.web.action.pricing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.api.FareOverwriteInfoTO;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.util.pricing.FareUtil;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FareOverwriteAction {
	private static final Log log = LogFactory.getLog(FareOverwriteAction.class);

	private static final String MASTER_MODE = "manageFare";
	private static final String FARE_MODE = "creatFare";

	private String fareRuleCode;

	private String fareRuleId;

	private String compareAgainst;

	private String timeType;

	private String chargeType;

	private String searchMode;

	private String actionMode;

	private List<FareOverwriteInfoTO> overwriteFares;

	private FareOverwriteInfoTO fee;

	private boolean success;

	private String messageTxt = "";

	private String feeCutoverAppliedStatus;

	private String ruleVersion;

	// This rule version introduced to compare the versions when updating happens, but for the moment this attribute is
	// not using since manage fare screen is not refreshing after we do updates in the overwrite fare pop up screen.
	// once refreshing functionalities are done for the below level screens, you can use this attribute for version
	// matching. Ideally when pop up screen closes,below level screens must be refreshed to reflect the actual versions.

	public String execute() {
		try {
			overwriteFares = new ArrayList<FareOverwriteInfoTO>();

			if (PlatformUtiltiies.nullHandler(searchMode).equals(MASTER_MODE)) {
				// When searching master fare rule's fee data
				FareRule fareRule = ModuleServiceLocator.getRuleBD().getFareRule(Integer.parseInt(fareRuleId));
				if (fareRule != null) {
					Collection<FareRuleFee> fareFees = fareRule.getFees();
					if (fareFees != null) {
						FareUtil.populateFareOverWriteInfo(fareFees, overwriteFares, compareAgainst, timeType, chargeType);
					}
					feeCutoverAppliedStatus = Character.toString(fareRule.getFeeTwlHrsCutOverApplied());
				}
			} else if (PlatformUtiltiies.nullHandler(searchMode).equals(FARE_MODE)) {
				// When searching fare rule's fee data
				FareRule fareRule = ModuleServiceLocator.getFareBD().getFare(Integer.parseInt(fareRuleId)).getFareRule();
				if (fareRule != null) {
					Collection<FareRuleFee> fareFees = fareRule.getFees();
					if (fareFees != null) {
						FareUtil.populateFareOverWriteInfo(fareFees, overwriteFares, compareAgainst, timeType, chargeType);
					}
					feeCutoverAppliedStatus = Character.toString(fareRule.getFeeTwlHrsCutOverApplied());
				}
			}

			if (overwriteFares != null && overwriteFares.size() > 0) {
				Collections.sort(overwriteFares);
			}
			success = true;
		} catch (ModuleException e) {
			log.error("FareOverwriteAction error " + e.getMessage());
			success = false;
			messageTxt = "Searching overwrite fees failed. Please try again later.";
		} catch (Exception ex) {
			log.error("FareOverwriteAction error " + ex.toString());
			success = false;
			messageTxt = "Searching overwrite fees failed. Please try again later.";
		}

		return S2Constants.Result.SUCCESS;
	}

	public String overwriteFare() {
		if (PlatformUtiltiies.nullHandler(actionMode).length() > 0 && fee != null) {
			try {
				FareRule fareRule;

				if (PlatformUtiltiies.nullHandler(searchMode).equals(MASTER_MODE)) {
					fareRule = ModuleServiceLocator.getRuleBD().getFareRule(Integer.parseInt(fareRuleId));
				} else {
					fareRule = ModuleServiceLocator.getFareBD().getFare(Integer.parseInt(fareRuleId)).getFareRule();
				}

				if ((fareRule.getFareRuleCode() != null && PlatformUtiltiies.nullHandler(fareRuleCode).length() > 0)
						|| fareRule.getFareRuleCode() == null) {
					String baseCurrency = AppSysParamsUtil.getBaseCurrency();
					String selectedCurrency = fareRule.getLocalCurrencyCode();

					if (selectedCurrency != null && !selectedCurrency.equals(baseCurrency)) {
						fareRule.setLocalCurrencySelected(true);
						fareRule.setLocalCurrencyCode(selectedCurrency);
					}

					if (actionMode.equals(FareUtil.ACTION_ADD)) {

						FareRuleFee fareRuleFee = FareUtil.composeFareRuleFee(fee);
						Pair<Boolean, String> validatedResult = FareUtil.validateFareFees(fareRule.getFees(), fareRuleFee,
								actionMode);

						if (validatedResult.getLeft() != null && validatedResult.getLeft() == true) {
							fareRule.addFee(fareRuleFee);
							ModuleServiceLocator.getRuleBD().updateFareRule(fareRule);
							success = true;
						} else {
							success = false;
							if (PlatformUtiltiies.nullHandler(validatedResult.getRight()).length() > 0) {
								messageTxt = validatedResult.getRight();
							}
						}
					} else if (actionMode.equals(FareUtil.ACTION_EDIT)) {

						Collection existingFees = fareRule.getFees();
						FareRuleFee existingFee = FareUtil.getFareRuleFee(existingFees, fee.getId());
						FareUtil.updateFareRuleFeeFields(existingFee, fee);

						Pair<Boolean, String> validatedResult = FareUtil.validateFareFees(fareRule.getFees(), existingFee,
								actionMode);

						if (validatedResult.getLeft() != null && validatedResult.getLeft() == true) {
							ModuleServiceLocator.getRuleBD().updateFareRule(fareRule);
							success = true;
						} else {
							success = false;
							if (PlatformUtiltiies.nullHandler(validatedResult.getRight()).length() > 0) {
								messageTxt = validatedResult.getRight();
							}
						}
					} else if (actionMode.equals(FareUtil.ACTION_DELETE)) {
						Collection existingFees = fareRule.getFees();
						FareRuleFee existingFee = FareUtil.getFareRuleFee(existingFees, fee.getId());
						fareRule.removeFee(existingFee);
						ModuleServiceLocator.getRuleBD().updateFareRule(fareRule);
						success = true;
					}
				}
			} catch (ModuleException e) {
				log.error("FareOverwriteAction : overwriteFare error " + e.getMessage());
				messageTxt = "Editing overwrite fees failed. Please try again later.";
				success = false;
			} catch (Exception ex) {
				log.error("FareOverwriteAction : overwriteFare error " + ex.toString());
				success = false;
				messageTxt = "Editing overwrite fees failed. Please try again later.";
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public String updateFareFeeTwlHrsCutover() {
		if (PlatformUtiltiies.nullHandler(fareRuleId).length() > 0
				&& PlatformUtiltiies.nullHandler(feeCutoverAppliedStatus).length() > 0) {

			try {
				FareRule fareRule = ModuleServiceLocator.getRuleBD().getFareRule(Integer.parseInt(fareRuleId));
				if (feeCutoverAppliedStatus.equals(Character.toString(FareRule.FEE_CUTOVER_YES))) {
					fareRule.setFeeTwlHrsCutOverApplied(FareRule.FEE_CUTOVER_YES);
				} else {
					fareRule.setFeeTwlHrsCutOverApplied(FareRule.FEE_CUTOVER_NO);
				}
				ModuleServiceLocator.getRuleBD().updateFareRule(fareRule);
				success = true;
			} catch (ModuleException e) {
				log.error("FareOverwriteAction : updateFareFeeTwlHrsCutover error " + e.getMessage());
				messageTxt = "Editing fee cutover failed. Please try again later.";
				success = false;
			} catch (Exception ex) {
				log.error("FareOverwriteAction : updateFareFeeTwlHrsCutover error " + ex.toString());
				success = false;
				messageTxt = "Editing fee cutover failed. Please try again later.";
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getCompareAgainst() {
		return compareAgainst;
	}

	public void setCompareAgainst(String compareAgainst) {
		this.compareAgainst = compareAgainst;
	}

	public String getTimeType() {
		return timeType;
	}

	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public String getSearchMode() {
		return searchMode;
	}

	public void setSearchMode(String searchMode) {
		this.searchMode = searchMode;
	}

	public List<FareOverwriteInfoTO> getOverwriteFares() {
		return overwriteFares;
	}

	public void setOverwriteFares(List<FareOverwriteInfoTO> overwriteFares) {
		this.overwriteFares = overwriteFares;
	}

	public String getActionMode() {
		return actionMode;
	}

	public void setActionMode(String actionMode) {
		this.actionMode = actionMode;
	}

	public FareOverwriteInfoTO getFee() {
		return fee;
	}

	public void setFee(FareOverwriteInfoTO fee) {
		this.fee = fee;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getFeeCutoverAppliedStatus() {
		return feeCutoverAppliedStatus;
	}

	public void setFeeCutoverAppliedStatus(String feeCutoverAppliedStatus) {
		this.feeCutoverAppliedStatus = feeCutoverAppliedStatus;
	}

	public String getRuleVersion() {
		return ruleVersion;
	}

	public void setRuleVersion(String ruleVersion) {
		this.ruleVersion = ruleVersion;
	}

	public String getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(String fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

}
