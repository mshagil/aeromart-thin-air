package com.isa.thinair.airadmin.core.web.v2.action.samples;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thushara Fernando
 * 
 *         This is a special action file to show JSP files directly with out any presentations logic being executed.
 *         File Name itself is used instead of separate result value to make mappings simple. So in the result
 *         annotation filename itself is specified as name as well as the value.
 * 
 *         eg : @Result(name=S2Constants.Jsp.Common.EXAMPLE_FILE, value=S2Constants.Jsp.Common.EXAMPLE_FILE)
 * 
 * 
 *         To work this <constant name="struts.enable.DynamicMethodInvocation" value="true" /> should be set in the
 *         struts.xml file
 * 
 */

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Jsp.Samples.COUNTRY_LOAD_JSP, value = S2Constants.Jsp.Samples.COUNTRY_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Samples.FLIGHT_LOAD_JSP, value = S2Constants.Jsp.Samples.FLIGHT_LOAD_JSP) })
public class ShowLoadJspSampleAction extends BaseRequestAwareAction {

	public String loadCountry() {
		try {
			String strCurrencyCodeHtml = SelectListGenerator.createCurrencyList();
			request.setAttribute(WebConstants.REQ_HTML_CURRENCY_LIST, strCurrencyCodeHtml);
		} catch (ModuleException moduleException) {
			// do nothing
		}
		/* return shifted for formatting */return S2Constants.Jsp.Samples.COUNTRY_LOAD_JSP;
	}

	public String flightLoad() {
		return S2Constants.Jsp.Samples.FLIGHT_LOAD_JSP;
	}
}
