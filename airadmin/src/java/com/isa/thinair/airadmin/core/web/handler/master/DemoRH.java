package com.isa.thinair.airadmin.core.web.handler.master;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.master.DemoHG;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.LookupUtils;

public class DemoRH {

	private static Log log = LogFactory.getLog(DemoRH.class);

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) throws ModuleException {
		String strForward = WebConstants.FORWARD_SUCCESS;
		String strErrorMsg = "";
		String strReturnData = "";
		String strHdnMode = "";
		try {
			if (log.isDebugEnabled()) {
				log.debug(request.getParameter("fromLocation"));
				log.debug(request.getParameter("toLocation"));
				log.debug(request.getParameter("region"));
				log.debug(request.getParameter("pos"));
				log.debug(request.getParameter("fromDate"));
				log.debug(request.getParameter("toDate"));
				log.debug(request.getParameter("hdnMode"));
				log.debug(request.getParameter("hdnAgentCode"));
				log.debug(request.getParameter("hdnBranchCode"));
			}

			strHdnMode = request.getParameter("hdnMode");
			if ((strHdnMode == null) || (strHdnMode.equals("")) || (strHdnMode.equals("TAB_2")) || (strHdnMode.equals("TAB_3"))
					|| (strHdnMode.equals("TAB_4")) || (strHdnMode.equals("TAB_5")) || (strHdnMode.equals("TAB_6"))) {

				strReturnData = getMISData(request);

				// Regional Performance
			} else if ((strHdnMode.equals("REGIONAL_BP")) || (strHdnMode.equals("REGIONAL_LP"))) {
				strReturnData = getRegionalPerofmacneData(request);

				// Agents - Lists
			} else if ((strHdnMode.equals("AGENTS_A")) || (strHdnMode.equals("AGENTS_T")) || (strHdnMode.equals("AGENTS_N"))
					|| (strHdnMode.equals("AGENTS_O"))) {
				strReturnData = getAgentAgentWiseInfo(request, request.getParameter("hdnCntryCode"));

				// Agents - Details
			} else if (strHdnMode.equals("AGENTS_DETAILS")) {
				strReturnData = getAgentIndividualInfo(request);

				// Agents - Branch Wise Data
			} else if (strHdnMode.equals("AGENTS_BRANCH")) {
				strReturnData = getAgentBranchInfo(request);

				// Ancillary
			} else if ((strHdnMode.equals("ANSI_S")) || (strHdnMode.equals("ANSI_M")) || (strHdnMode.equals("ANSI_I"))
					|| (strHdnMode.equals("ANSI_H"))) {
				strReturnData = getAncillaryKPIData(request);
			}
		} catch (Exception ex) {
			strErrorMsg = "Unhandled error in  - ShowMISSearchDataAction";
			strForward = WebConstants.FORWARD_ERROR;
		}

		/*
		 * Ajax Return Data
		 */
		strReturnData += "strSYSError = '" + strErrorMsg + "';\n";
		request.setAttribute("returnData", strReturnData);

		return strForward;
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String createAirportList() throws ModuleException {
		Collection<Airport> collection = ModuleServiceLocator.getAirportServiceBD().getAirports();
		return DemoHG.createAirportList(collection);
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private static String getMISData(HttpServletRequest request) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		/**
		 * TODO
		 * 
		 * - Have to re-organize this code. This is done only for the demo
		 * 
		 * - For the Time being hard code values in the code. Move it to actual
		 */

		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);

		cal2.add(Calendar.MONTH, -6);

		// Dash Board Tab Data
		String strHdnMode = request.getParameter("hdnMode");
		if ((strHdnMode == null) || (strHdnMode.equals(""))) {
			String[] params = new String[1];
			params = new String[1];
			if (request.getParameter("fromDate").equals("")) {
				params[0] = "01/01/" + year;
			} else {
				params[0] = request.getParameter("fromDate");
			}
			strbBuffer.append(DemoHG
					.getDashBoardRevenueData(LookupUtils.LookupDAO().getList("misDashRevnueBreakDown", params, 4)));

			params = new String[2];
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			params[0] = "01/" + format.format(cal2.getTime()).substring(3);
			params[1] = "01/" + format.format(cal.getTime()).substring(3);
			strbBuffer.append(DemoHG.getDashBoardRevenueBreakDown(LookupUtils.LookupDAO()
					.getList("misDashYearlySales", params, 3)));

			params = new String[1];
			if (request.getParameter("fromDate").equals("")) {
				params[0] = "01/01/" + year;
			} else {
				params[0] = request.getParameter("fromDate");
			}
			strbBuffer.append(DemoHG.getDashBoardYTD(LookupUtils.LookupDAO().getList("misYTDBooking", params, 1)));
			strbBuffer.append(DemoHG.getDashBoardSalesChannelWiseInfo(LookupUtils.LookupDAO().getList("misDashSalesChnl", params,
					2)));

		} else if (strHdnMode.equals("TAB_2")) {
			// Distribution Tab Data
			strbBuffer.append(DemoHG.getDistPerformanceOfPortals());
			strbBuffer.append(DemoHG.getDistBookingByChannel());
			strbBuffer.append(DemoHG.getDistBookingByChannelGraph());
			strbBuffer.append(DemoHG.getDistRevenueByChannel());

		} else if (strHdnMode.equals("TAB_3")) {
			String[] params = new String[2];
			params[0] = "01/01/" + (year - 3);
			params[1] = "31/12/" + (year - 1);

			// Regional Tab Data
			strbBuffer.append(DemoHG.getRegionalYearlySales(LookupUtils.LookupDAO().getList("misRegionYearlySales", params, 2)));
			strbBuffer.append(DemoHG.getRegionalMonthRolling());
			strbBuffer.append(DemoHG.getRegionalProductSales());
			strbBuffer.append(getRegionalPerofmacneData(request));

		} else if (strHdnMode.equals("TAB_4")) {
			// Agents
			Collection collCountries = LookupUtils.LookupDAO().getList("misCountryStation");
			String strSltdCntry = DemoHG.getFirstCountryCode(collCountries);
			strbBuffer.append(DemoHG.getAgentsCountryCities(collCountries));
			strbBuffer.append(getAgentAgentWiseInfo(request, strSltdCntry));

		} else if (strHdnMode.equals("TAB_5")) {
			// Accounts Tab Data
			strbBuffer.append(DemoHG.getAccountsRevnueSummary());
			strbBuffer.append(DemoHG.getAccountsSalesSummary());
			strbBuffer.append(DemoHG.getAccountsCreditInfo());
			strbBuffer.append(DemoHG.getAccountsOutstandingPaymentsData());
			strbBuffer.append(DemoHG.getAccountsOutstandingPayablesData());

		} else if (strHdnMode.equals("TAB_6")) {
			// Ancillary
			strbBuffer.append(DemoHG.getAnsiSalesInfo());
			strbBuffer.append(DemoHG.getAnsiSalesByChannelInfo());
			strbBuffer.append(getAncillaryKPIData(request));
		}

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private static String getAgentAgentWiseInfo(HttpServletRequest request, String strSltdCntry) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		String[] params = new String[1];
		params[0] = strSltdCntry;
		strbBuffer.append(DemoHG.getAgentsAllAgentsInfo(request.getParameter("hdnMode"),
				LookupUtils.LookupDAO().getList("misCountryStationAgentsSelected", params, 3)));
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private static String getRegionalPerofmacneData(HttpServletRequest request) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();
		String strSQL = "misRegionAgentsDesc";
		String strMode = request.getParameter("hdnMode");

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);

		String[] params = new String[1];
		if (request.getParameter("fromDate").equals("")) {
			params[0] = "01/01/" + year;
		} else {
			params[0] = request.getParameter("fromDate");
		}

		if ((strMode == null) || (!strMode.equals("REGIONAL_LP")) || (strMode.equals("TAB_3"))) {
			strSQL = "misRegionAgentsDesc";
		} else {
			strSQL = "misRegionAgentsASC";
		}

		Collection collection = LookupUtils.LookupDAO().getList(strSQL, params, 13);
		Collection collectionAvg = LookupUtils.LookupDAO().getList("misRegionAgentsAvg", params, 1);

		strbBuffer.append(DemoHG.getRegionalPerformance(strMode, collection, collectionAvg));
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private static String getAncillaryKPIData(HttpServletRequest request) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();
		strbBuffer.append(DemoHG.getAnsiKPIViewerInfo(request.getParameter("hdnMode")));
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private static String getAgentIndividualInfo(HttpServletRequest request) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		String[] params = new String[1];
		params[0] = request.getParameter("hdnAgentCode");
		strbBuffer.append(DemoHG.getAgentsIndividualDetails(LookupUtils.LookupDAO().getList("misCountryStationAgentInfo", params,
				7)));
		strbBuffer.append(getAgentBranchInfo(request));
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private static String getAgentBranchInfo(HttpServletRequest request) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();
		strbBuffer.append(DemoHG.getAgentsBranchInfo());
		return strbBuffer.toString();
	}
}
