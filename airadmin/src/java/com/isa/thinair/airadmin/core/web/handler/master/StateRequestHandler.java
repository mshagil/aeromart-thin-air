package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.StationHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public final class StateRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(StationRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_STATIONID = "txtStationId";
	private static final String PARAM_DESCRIPTION = "txtDesc";
	private static final String PARAM_ONLINE_STATUS = "radOnline";
	private static final String PARAM_TERRITORYCODE = "selTerritory";
	private static final String PARAM_CONTRYCODE = "selCountry";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_CONTACT = "txtContact";
	private static final String PARAM_PHONE = "txtPhone";
	private static final String PARAM_FAX = "txtFax";
	private static final String PARAM_THRESHOLD_TIME = "txtThresholdTime";

	private static int intSuccess = 0; // 0-Not
										// Applicable,
										// 1-Success,
										// 2-Fail

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Check the Territory Validity
	 * 
	 * @param stationId
	 *            the Station Id
	 * @param territoryId
	 *            the Territory Id
	 * @param countryCode
	 *            the Country Code
	 * @param strVersion
	 *            the Version
	 * @return int 0- Success 1- The changed Territory inactive 2- The changed Country inactive
	 * @throws ModuleException
	 */
	private static int checkValidTerotory(String stationId, String territoryId, String countryCode, String strVersion)
			throws ModuleException {

		Station station = null;
		if (stationId != null) {
			station = ModuleServiceLocator.getLocationServiceBD().getStation(stationId);
		}

		Territory territory = ModuleServiceLocator.getLocationServiceBD().getTerritory(territoryId);
		if (territory == null)
			return 1;

		Country country = ModuleServiceLocator.getLocationServiceBD().getCountry(countryCode);
		if (country == null)
			return 2;

		if (strVersion != null && !"".equals(strVersion)) {

		} else {
			if (station != null) {
				return 3;
			}
		}

		if (territory.getStatus().equals(Territory.STATUS_INACTIVE)) {
			if (station == null)
				return 1;
			else if (!station.getTerritoryCode().equals(territory.getTerritoryCode()))
				return 1;
		}
		if (country.getStatus().equals(Country.STATUS_INACTIVE)) {
			if (station == null)
				return 2;
			else {
				if (!station.getCountryCode().equals(country.getCountryCode()))
					return 2;
			}
		}
		return 0;
	}

	/**
	 * Main Execute Method for Station Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		intSuccess = 0;
		String strFormData = "var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail";
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		log.debug("Mode: " + strHdnMode);
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

		try {

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_ADD)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_STATION_ADD);

				setAttribInRequest(request, "strModeJS", "var isAddMode = true;");

				saveData(request);

				if (!isExceptionOccured(request)) {
					intSuccess = 1;
					strFormData = " var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail";
					strFormData += " var arrFormData = new Array();";
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
			}

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_STATION_EDIT);

				setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				saveData(request);

				if (!isExceptionOccured(request)) {
					intSuccess = 1;
					strFormData = " var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail";
					strFormData += " var arrFormData = new Array();";
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
			}

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_STATION_DELETE);
				deleteData(request);
			}
			request.setAttribute("reqAllowExtendedNameModificaion",
					"var isAllowExtendedNameModificaion = " + AppSysParamsUtil.applyExtendedNameModificationFunctionality() + ";");
			setDisplayData(request);
		} catch (Exception exception) {
			log.error("Exception in StationHTMLGenerator.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "top[0]", "");
			}
		}

		return forward;
	}

	/**
	 * Delete the Station
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void deleteData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the deleteData()...");
		String strStationId = request.getParameter("txtStationId");

		try {
			log.debug("deleteData()..." + strStationId);
			if (strStationId != null) {
				Station existingStation = ModuleServiceLocator.getLocationServiceBD().getStation(strStationId);
				if (existingStation == null)
					ModuleServiceLocator.getLocationServiceBD().removeStation(strStationId);
				else {
					String strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingStation.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getLocationServiceBD().removeStation(existingStation);
					} else {
						ModuleServiceLocator.getLocationServiceBD().removeStation(strStationId);
					}
				}

				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.deleteData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception exception) {
			log.error("Exception in StationHTMLGenerator.deleteData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}

	}

	/**
	 * Saves the Station
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the saveData()...");
		Station station = new Station();
		Properties stp = getProperty(request);

		try {

			if (!"".equals(stp.getProperty(PARAM_VERSION))) {
				station.setVersion(Long.parseLong(stp.getProperty(PARAM_VERSION)));
			}

			station.setStationCode(stp.getProperty(PARAM_STATIONID).toUpperCase());
			station.setStationName(stp.getProperty(PARAM_DESCRIPTION));
			station.setCountryCode(stp.getProperty(PARAM_CONTRYCODE));
			station.setRemarks(stp.getProperty(PARAM_REMARKS).trim());
			station.setNameChangeThresholdTime(stp.getProperty(PARAM_THRESHOLD_TIME));

			String strHdnMode = request.getParameter(PARAM_MODE);

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_ADD)) {
				station.setLccPublishStatus(Util.LCC_TOBE_PUBLISHED);
			} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				station.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);
			}

			if (stp.getProperty(PARAM_STATUS).equals("on")) {
				station.setStatus(Station.STATUS_ACTIVE);
			} else {
				station.setStatus(Station.STATUS_INACTIVE);
			}

			station.setOnlineStatus(Integer.parseInt(stp.getProperty(PARAM_ONLINE_STATUS)));
			station.setTerritoryCode(stp.getProperty(PARAM_TERRITORYCODE));
			station.setContact(stp.getProperty(PARAM_CONTACT));
			station.setTelephone(stp.getProperty(PARAM_PHONE));
			station.setFax(stp.getProperty(PARAM_FAX));

			int validity = checkValidTerotory(stp.getProperty(PARAM_STATIONID), stp.getProperty(PARAM_TERRITORYCODE),
					stp.getProperty(PARAM_CONTRYCODE), stp.getProperty(PARAM_VERSION));

			switch (validity) {
			case 1:
				// Territory is invalid
				setExceptionOccured(request, true);
				saveMessage(request, airadminConfig.getMessage("um.user.form.territory.inactive"), WebConstants.MSG_ERROR);
				break;
			case 2:
				// Country is invalid
				setExceptionOccured(request, true);
				saveMessage(request, airadminConfig.getMessage("um.user.form.country.inactive"), WebConstants.MSG_ERROR);
				break;

			case 3:
				// Station Code Exists
				setExceptionOccured(request, true);
				saveMessage(request, airadminConfig.getMessage("um.station.form.id.already.exist"), WebConstants.MSG_ERROR);
				break;
			}

			if (isExceptionOccured(request) && validity != 0) {

				String strFormData = setErrorForm(request);
				intSuccess = 2;
				strFormData += " var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail

				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			} else {

				ModuleServiceLocator.getLocationServiceBD().saveStation(station);
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
				station = null;
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
				setExceptionOccured(request, false);
				log.debug("StationRequestHandler.saveData() method is successfully executed.");
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			setExceptionOccured(request, true);
			String strFormData = setErrorForm(request);
			intSuccess = 2;
			strFormData += " var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			if (moduleException.getExceptionCode().equals("module.duplicate.key")) {
				saveMessage(request, airadminConfig.getMessage("um.station.form.id.already.exist"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		} catch (Exception exception) {
			log.error("Exception in StationHTMLGenerator.saveData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}

	}

	/**
	 * Sets the Display Data & succes int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setDisplayData(HttpServletRequest request) {

		setStationRowHtml(request);
		setCountryList(request);
		setTerritoryList(request);
		setClientErrors(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + intSuccess + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));
		if (request.getParameter(PARAM_MODE) == null) {
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else if (request.getParameter(PARAM_MODE) != null && !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_ADD)
				&& !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else {
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}

		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");

	}

	/**
	 * Sets the Country List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCountryList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createCountryList();
			request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strHtml);
			String strCountryHtml = SelectListGenerator.createCountryListWOTag();
			request.setAttribute(WebConstants.REQ_HTML_COUNTRY_COMBO, strCountryHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.setCountryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Territory List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setTerritoryList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createTerritoryList_SG_Desc();
			request.setAttribute(WebConstants.REQ_HTML_TERRITORY_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.setTerritoryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations for Station Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = StationHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.setClientErrors() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
		}
	}

	/**
	 * Sets the Station Grid data to the Request
	 * 
	 * @param request
	 */
	protected static void setStationRowHtml(HttpServletRequest request) {

		try {
			StationHTMLGenerator htmlGen = new StationHTMLGenerator();
			String strHtml = htmlGen.getStationRowHtml(request);

			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.setStationRowHtml() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static Properties getProperty(HttpServletRequest request) {
		Properties prop = new Properties();

		prop.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION)));
		prop.setProperty(PARAM_STATIONID, AiradminUtils.getNotNullString(request.getParameter(PARAM_STATIONID)));
		prop.setProperty(PARAM_DESCRIPTION, AiradminUtils.getNotNullString(request.getParameter(PARAM_DESCRIPTION)));
		prop.setProperty(PARAM_REMARKS, AiradminUtils.getNotNullString(request.getParameter(PARAM_REMARKS)));
		prop.setProperty(PARAM_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS)));
		prop.setProperty(PARAM_ONLINE_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_ONLINE_STATUS)));
		prop.setProperty(PARAM_TERRITORYCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_TERRITORYCODE)));
		prop.setProperty(PARAM_CONTRYCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_CONTRYCODE)));
		prop.setProperty(PARAM_CONTACT, AiradminUtils.getNotNullString(request.getParameter(PARAM_CONTACT)));
		prop.setProperty(PARAM_PHONE, AiradminUtils.getNotNullString(request.getParameter(PARAM_PHONE)));
		prop.setProperty(PARAM_FAX, AiradminUtils.getNotNullString(request.getParameter(PARAM_FAX)));
		prop.setProperty(PARAM_THRESHOLD_TIME, AiradminUtils.getNotNullString(request.getParameter(PARAM_THRESHOLD_TIME)));

		return prop;
	}

	private static String setErrorForm(HttpServletRequest request) {
		Properties erp = getProperty(request);
		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");

		ersb.append("arrFormData[0][1] = '" + erp.getProperty(PARAM_STATIONID) + "';");
		ersb.append("arrFormData[0][2] = '" + erp.getProperty(PARAM_DESCRIPTION) + "';");
		ersb.append("arrFormData[0][3] = '" + erp.getProperty(PARAM_TERRITORYCODE) + "';");
		ersb.append("arrFormData[0][4] = '" + erp.getProperty(PARAM_ONLINE_STATUS) + "';");
		ersb.append("arrFormData[0][5] = '" + erp.getProperty(PARAM_STATUS) + "';");
		ersb.append("arrFormData[0][6] = '" + erp.getProperty(PARAM_REMARKS) + "';");
		ersb.append("arrFormData[0][7] = '" + erp.getProperty(PARAM_CONTRYCODE) + "';");
		ersb.append("arrFormData[0][8] = '" + erp.getProperty(PARAM_VERSION) + "';");
		ersb.append("arrFormData[0][9] = '" + erp.getProperty(PARAM_CONTACT) + "';");
		ersb.append("arrFormData[0][10] = '" + erp.getProperty(PARAM_PHONE) + "';");
		ersb.append("arrFormData[0][11] = '" + erp.getProperty(PARAM_FAX) + "';");
		ersb.append("arrFormData[0][11] = '" + erp.getProperty(PARAM_THRESHOLD_TIME) + "';");

		return ersb.toString();
	}
}
