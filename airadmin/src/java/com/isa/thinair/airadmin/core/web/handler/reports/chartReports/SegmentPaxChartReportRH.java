/**
 * 
 */
package com.isa.thinair.airadmin.core.web.handler.reports.chartReports;

import java.awt.Color;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.labels.StandardCategorySeriesLabelGenerator;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.data.category.CategoryDataset;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reportingframework.api.dto.BarChartDTO;
import com.isa.thinair.reportingframework.core.util.ChartConstants.CHART_DATA_SET_TYPES;
import com.isa.thinair.reportingframework.core.util.ChartConstants.CHART_TYPES;

/**
 * Chart Report showing the top Confirmed Segments
 * 
 * @author Navod Ediriweera
 * @since Aug 10, 2009
 */
public class SegmentPaxChartReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(SegmentPaxChartReportRH.class);

	private static final String X_AXIS_NAME = "Segment";
	private static final String Y_AXIS_NAME = "Total Passengers";

	/**
	 * Main Execute Method for Audit Report Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
		} catch (Exception e) {
			log.error("SegmentPaxChartReportRH setDisplayData() FAILED " + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			} else {
				log.error("SegmentPaxChartReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("SegmentPaxChartReportRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Display data For Security Loging Audit Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
	}

	/**
	 * Displays the Security Loging Audit Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String fromDate = request.getParameter("hdnFromDate");
		String toDate = request.getParameter("hdnToDate");

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getSegmentTotalPaxByStatus(search);

			BarChartDTO barChartDTO = new BarChartDTO(CHART_TYPES.BAR_CHART_3D);
			barChartDTO.setChartResultSetWithType(resultSet, CHART_DATA_SET_TYPES.THREE_COLUMN_DATA);
			barChartDTO.setWidth(900);
			barChartDTO.setHeight(350);
			barChartDTO.setBackGroundPaint(new Color(244, 244, 244));
			barChartDTO.setXAxisName(X_AXIS_NAME);
			barChartDTO.setYAxisName(Y_AXIS_NAME);

			BarRenderer3D barRenderer3D = new BarRenderer3D();
			CustomLableGenerator customLabelGenerator = new CustomLableGenerator();
			barRenderer3D.setLegendItemLabelGenerator(customLabelGenerator);
			barRenderer3D.setSeriesPaint(0, Color.GREEN);

			barChartDTO.setRendere(barRenderer3D);

			ModuleServiceLocator.getReportingFrameworkBD().createAndExportBarChart(barChartDTO, response);

		} catch (Exception e) {
			log.error("setReportView :: ", e);
		}
	}

	/**
	 * Replacing the series name of the series
	 * 
	 * @author Navod Ediriweera
	 * @since Aug 11, 2009
	 */
	private static class CustomLableGenerator extends StandardCategorySeriesLabelGenerator {

		private static final String CONFIRMED_STR = "Confirmed";

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.jfree.chart.labels.StandardCategorySeriesLabelGenerator#generateLabel(org.jfree.data.category.CategoryDataset
		 * , int)
		 */
		@Override
		public String generateLabel(CategoryDataset dataset, int series) {
			if (series == 0) {
				return CONFIRMED_STR;
			} else {
				return dataset.getRowKey(series).toString();
			}
		}
	}
}
