package com.isa.thinair.airadmin.core.web.util;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author nafly
 * 
 */
public class PassengerUtil {

	public static final String NOSHOW_STATUS = "NOSHOW";
	public static final String GOSHOW_STATUS = "GOSHOW";
	public static final String FLOWN_STATUS = "FLOWN";
	public static final String NOREC_STATUS = "NOREC";
	public static final String CONFIRM_STATUS = "CONFIRM";
	public static final String CNF_STATUS = "CNF";
	public static final String OHD_STATUS = "OHD";
	public static final String ALL_STATUS = "ALL";
	public static final String CNX_STATUS = "CNX";
	public static final String NOSHOW_STS = "N";
	public static final String GOSHOW_STS = "G";
	public static final String FLOWN_STS = "F";
	public static final String CONFIRMED = "C";
	public static final String NO_REC = "R";
	/**
	 * Sets the Status Select Box to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setPAXStatus(HttpServletRequest request) throws ModuleException {

		StringBuffer strList = new StringBuffer();
		strList.append("<option value='" + ALL_STATUS + "'>ALL</option>");
		strList.append("<option value='" + CNF_STATUS + "'>CNF</option>");
		strList.append("<option value='" + CNX_STATUS + "'>CNX</option>");
		strList.append("<option value='" + OHD_STATUS + "'>OHD</option>");

		request.setAttribute(WebConstants.REQ_PAX_STATUS_LIST, strList.toString());

	}

	public static void setPFSStatus(HttpServletRequest request) throws ModuleException {
		StringBuffer strList = new StringBuffer();
		strList.append("<option value='" + ALL_STATUS + "'>ALL</option>");
		strList.append("<option value='" + NOSHOW_STATUS + "'>NOSHOW</option>");
		strList.append("<option value='" + GOSHOW_STATUS + "'>GOSHOW</option>");
		strList.append("<option value='" + FLOWN_STATUS + "'>FLOWN</option>");
		strList.append("<option value='" + NOREC_STATUS + "'>NOREC</option>");
		strList.append("<option value='" + CONFIRM_STATUS + "'>CONFIRM</option>");

		request.setAttribute(WebConstants.REQ_PFS_STATUS_LIST, strList.toString());
	}

	public static String getPfsStatus(String selStatus) throws ModuleException {
		if (selStatus.equals(NOSHOW_STATUS)) {
			return NOSHOW_STS;
		} else if (selStatus.equals(GOSHOW_STATUS)) {
			return GOSHOW_STS;
		} else if (selStatus.equals(FLOWN_STATUS)) {
			return FLOWN_STS;
		} else if (selStatus.equals(NOREC_STATUS)) {
			return NO_REC;
		} else if (selStatus.equals(CONFIRM_STATUS)) {
			return CONFIRMED;
		}

		return null;
	}
	
}
