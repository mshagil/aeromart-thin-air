/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.ETLDetailProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public final class ETLDetailProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ETLDetailProcessingRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static final String PARAM_CURRENT_ETL = "hdnCurrentEtl";
	private static final String PARAM_CURRENT_ETL_ID = "hdnETLID";
	private static final String PARAM_FIRST_TIME = "hdnState";
	private static final String PARAM_DOWNLOAD_DATE = "hdnDownloadDate";
	private static final String PARAM_FLIGHT_NUMBER = "hdnFlightNo";
	private static final String PARAM_FLIGHT_DATE = "hdnFlightDate";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_VERSION = "hdnPaxVersion";
	private static final String PARAM_ORIGIN = "hdnOrigin";
	private static final String PARAM_ETL_PAX_ID = "hdnETLPaxID";
	private static final String PARAM_PROCESS_STATUS = "hdnProcessSta";
	private static final String PARAM_ETL_STATUS = "hdnETLProcessStatus";
	private static final String PARAM_PAX_TITLE = "selTitle";
	private static final String PARAM_FIRST_NAME = "txtFirstName";
	private static final String PARAM_LAST_NAME = "txtLastName";
	private static final String PARAM_STATUS = "selAddStatus";
	private static final String PARAM_PAX_TYPE = "selPaxType";
	private static final String PARAM_ETICKET_NUMBER = "txtEtNo";
	private static final String PARAM_INFANT_ETICKET_NUMBER = "txtInfEtNo";
	private static final String PARAM_DESTINATION = "selDest";
	private static final String PARAM_COUPEN_NUMBER = "txtCoupenNo";
	private static final String PARAM_INF_COUPEN_NUMBER = "txtInfCoupenNo";
	private static final String PARAM_IS_NOREC_PASSENGER = "hdnIsNoREC";
	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm";
	private static final String EMPTY_STRING = "";
	private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

	/**
	 * 
	 * @param request
	 * @param value
	 */
	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	/**
	 * Sucess Values: 0 - Saved Successfuly 1 - Delete Successfully -1 - Error occurred - No releavant field identified
	 * -2 - Error occurred - Pax Title -3 - Error occurred - Pax First Name -4 - Error occurred - Pax Last Name -5 -
	 * Error occurred - PNR -6 - Error occurred - Destination -7 - Error occurred - Status
	 * 
	 */
	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For ETL Actions Sets the Succes Values 0-Not Applicable, 1-ETL process Success, 2-ETL save
	 * Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var strETLContent;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			String strHdnMode = request.getParameter(PARAM_MODE);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
				saveETLPaxEntry(request);
			} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
				String etlPaxId = request.getParameter(PARAM_ETL_PAX_ID);
				deleteETLPaxEntry(request, new Integer(etlPaxId));
			}

		} catch (ModuleException moduleException) {
			if (("um.pfsprocess.form.pfs.pax.alreadyReconiled").equals(moduleException.getExceptionCode())) {
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pax.alreadyReconiled"),
						WebConstants.MSG_ERROR);
			} else if (("um.pfsprocess.codeshare.pnr.pax").equals(moduleException.getExceptionCode())) {
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.codeshare.pnr.pax"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
			if (airadminConfig.getMessage(exception.getMessage()) != null) {
				saveMessage(request, airadminConfig.getMessage(exception.getMessage()), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_FAIL), WebConstants.MSG_ERROR);
			}
		}
		try {
			setDisplayData(request);
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Sets the Display Data For ETL Processing Page & Succes int value //0-Not Applicable, 1-ETL process Success, 2-ETL
	 * save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setETLProcessingRowHtml(request);
		setTitleList(request);
		setPaxTypesList(request);
		setAirportList(request);
		setClientErrors(request);
		setDestinationAirportList(request);
		setPaxCategory(request);
		setInfantTitles(request);
		setChildTitles(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	private static void setInfantTitles(HttpServletRequest request) throws Exception {
		String arrInfantTitles = SelectListGenerator.createInfantTitlesArray();
		request.setAttribute(WebConstants.INFANT_TITLES, arrInfantTitles);
	}
	
	private static void setChildTitles(HttpServletRequest request) throws Exception {
		String arrInfantTitles = SelectListGenerator.createPaxTitleArrayWithChild();
		request.setAttribute(WebConstants.PAX_TITLES_FOR_CHD, arrInfantTitles);
	}

	private static void deleteETLPaxEntry(HttpServletRequest request, int etlPaxId) throws Exception {
		ModuleServiceLocator.getEtlBD().deleteETLPaxEntry(etlPaxId);
		setIntSuccess(request, 1);
	}

	private static void saveETLPaxEntry(HttpServletRequest request) throws Exception {
		String etlId = request.getParameter(PARAM_CURRENT_ETL_ID);
		String paxTitle = request.getParameter(PARAM_PAX_TITLE);
		String firstName = request.getParameter(PARAM_FIRST_NAME);
		String lastName = request.getParameter(PARAM_LAST_NAME);
		String paxType = request.getParameter(PARAM_PAX_TYPE);
		String eticketNumber = request.getParameter(PARAM_ETICKET_NUMBER);
		String infEticketNumber = request.getParameter(PARAM_INFANT_ETICKET_NUMBER);
		String status = request.getParameter(PARAM_STATUS);
		String destination = request.getParameter(PARAM_DESTINATION);
		String receivedDate = request.getParameter(PARAM_DOWNLOAD_DATE);
		String flightDate = request.getParameter(PARAM_FLIGHT_DATE);
		String flightNumber = request.getParameter(PARAM_FLIGHT_NUMBER);
		String hdnVersion = request.getParameter(PARAM_VERSION);
		String departureAirport = request.getParameter(PARAM_ORIGIN);
		String etlPaxId = request.getParameter(PARAM_ETL_PAX_ID);
		String processStatus = request.getParameter(PARAM_PROCESS_STATUS);
		String coupenNumber = request.getParameter(PARAM_COUPEN_NUMBER);
		String infCoupenNumber = request.getParameter(PARAM_INF_COUPEN_NUMBER);
		boolean isNoRECPassenger = new Boolean(request.getParameter(PARAM_IS_NOREC_PASSENGER));
		String etlProcessStatus = request.getParameter(PARAM_ETL_STATUS);
		String segmentCode = departureAirport + "/%" + destination;
		boolean isValidETicket = false;
		ReservationPax reservationPax = null;
		Long version = Long.parseLong(hdnVersion);

		boolean isNOSHOPaxEntry = false;
		
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
			if (ModuleServiceLocator.getReservationBD().isCodeSharePNR(eticketNumber)) {
				throw new ModuleException("um.pfsprocess.codeshare.pnr.pax");
			}
		}		
		
		if (isNoRECPassenger) {
			if (paxType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				isNOSHOPaxEntry = ModuleServiceLocator.getReservationBD().isNOSHOPaxEntry(infEticketNumber,
						ReservationInternalConstants.PfsPaxStatus.NO_SHORE, segmentCode);
			} else {
				isNOSHOPaxEntry = ModuleServiceLocator.getReservationBD().isNOSHOPaxEntry(eticketNumber,
						ReservationInternalConstants.PfsPaxStatus.NO_SHORE, segmentCode);
			}
		}

		if (!isNOSHOPaxEntry) {
			EticketTO eTicketTO = null;
			if (paxType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				eTicketTO = ModuleServiceLocator.getReservationBD()
						.getEticketInfo(infEticketNumber, new Integer(infCoupenNumber));
			} else {
				eTicketTO = ModuleServiceLocator.getReservationBD().getEticketInfo(eticketNumber, new Integer(coupenNumber));
			}
			if (eTicketTO != null) {
				PassengerBD paxBD = InvoicingModuleUtils.getPassengerBD();
				reservationPax = paxBD.getPassenger(eTicketTO.getPnrPaxId(), false);
				if (!isNoRECPassenger) {
					Collection<FlightSegement> flightSegList = ModuleServiceLocator.getFlightServiceBD().getFlightSegmentList(
							segmentCode, outputDateFormat.parse(flightDate));
					if (eTicketTO != null) {
						for (FlightSegement flightsegment : flightSegList) {
							if (flightsegment.getFltSegId().intValue() == eTicketTO.getFlightSegId().intValue()) {
								isValidETicket = true;
							}
						}
					}
				} else {
					FlightSegement flightSegment = ModuleServiceLocator.getFlightServiceBD().getFlightSegment(
							eTicketTO.getFlightSegId());
					if (flightSegment.getSegmentCode().startsWith(departureAirport)
							&& flightSegment.getSegmentCode().endsWith(destination)) {
						isValidETicket = true;
					}
				}
			}
			if (isValidETicket) {
				String pnr = BeanUtils.nullHandler(ModuleServiceLocator.getReservationQueryBD().getPNRByEticketNo(eticketNumber));
				ETLPaxEntry etlPaxEntry = new ETLPaxEntry();
				if (etlPaxId != null && (!("").equals(etlPaxId))) {
					etlPaxEntry.setEtlPaxId(new Integer(etlPaxId));
				}
				if (!paxType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
					etlPaxEntry.setEticketNumber(eticketNumber);
					etlPaxEntry.setCoupNumber(new Integer(coupenNumber));
				} 
				
				if(infEticketNumber != null && !"".equals(infEticketNumber)){
					etlPaxEntry.setInfEticketNumber(infEticketNumber);
					etlPaxEntry.setInfCoupNumber(new Integer(infCoupenNumber));
				}
				if (version == -1) {
					etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
				} else {
					etlPaxEntry.setProcessedStatus(processStatus);
				}
				etlPaxEntry.setEtlId(new Integer(etlId));
				etlPaxEntry.setReceivedDate(outputDateFormat.parse(receivedDate));
				etlPaxEntry.setFlightDate(outputDateFormat.parse(flightDate));
				etlPaxEntry.setFlightNumber(flightNumber);
				etlPaxEntry.setDepartureAirport(departureAirport);
				if (EMPTY_STRING.equals(paxTitle)) {
					etlPaxEntry.setTitle(reservationPax.getTitle());
				} else {
					if (reservationPax.getTitle().equals(paxTitle)) {
						etlPaxEntry.setTitle(paxTitle);
					} else {
						setIntSuccess(request, -1);
						throw new Exception("um.process.form.title.invalid");
					}
				}
				if (EMPTY_STRING.equals(paxType)) {
					etlPaxEntry.setPaxType(reservationPax.getPaxType());
				} else {
					if (reservationPax.getPaxType().equals(paxType)) {
						etlPaxEntry.setPaxType(paxType);
					} else {
						setIntSuccess(request, -2);
						throw new Exception("um.process.form.paxType.invalid");
					}
				}
				if (EMPTY_STRING.equals(firstName)) {
					etlPaxEntry.setFirstName(reservationPax.getFirstName());
				} else {
					if (reservationPax.getFirstName().equalsIgnoreCase(firstName)) {
						etlPaxEntry.setFirstName(firstName);
					} else {
						setIntSuccess(request, -3);
						throw new Exception("um.process.form.firstName.invalid");
					}
				}
				if (EMPTY_STRING.equals(lastName)) {
					etlPaxEntry.setLastName(reservationPax.getLastName());
				} else {
					if (reservationPax.getLastName().equalsIgnoreCase(lastName)) {
						etlPaxEntry.setLastName(lastName);
					} else {
						setIntSuccess(request, -4);
						throw new Exception("um.process.form.lastName.invalid");
					}
				}
				etlPaxEntry.setPaxStatus(status);
				etlPaxEntry.setArrivalAirport(destination);
				etlPaxEntry.setVersion(version);
				etlPaxEntry.setPnr(pnr);
				char useAeroMartETS = AppSysParamsUtil.isEnableAeroMartETS() == true ? 'Y' : 'N';
				etlPaxEntry.setUseAeroMartETS(useAeroMartETS);
				ModuleServiceLocator.getEtlBD().saveETLParseEntry(etlPaxEntry);
				setIntSuccess(request, 0);
				if (!ParserConstants.ETLProcessStatus.NOT_PROCESSED.equals(etlProcessStatus)) {
					ETL etl = new ETL();
					etl.setEtlId(new Integer(etlId));
					etl.setProcessedStatus(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
					ModuleServiceLocator.getEtlBD().updateETLEntry(etl);

				}
			} else {
				setIntSuccess(request, -9);
				throw new ModuleException("airreservation.pfs.invalid.eticket.number");
			}
		} else {
			throw new ModuleException("um.pfsprocess.form.pfs.pax.alreadyReconiled");
		}
	}

	/**
	 * Sets the Destination airport list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDestinationAirportList(HttpServletRequest request) throws Exception {

		try {
			StringBuffer sb = new StringBuffer();

			String strCurrentETL = request.getParameter(PARAM_CURRENT_ETL) == null ? "" : request.getParameter(PARAM_CURRENT_ETL);
			String[] arrCurrentETL = new String[0];
			if (strCurrentETL != null && !strCurrentETL.equals("")) {

				arrCurrentETL = strCurrentETL.split(",");

				List<String> l = ModuleServiceLocator.getFlightServiceBD().getPossibleDestinations(
						arrCurrentETL[1].toUpperCase(), outputDateFormat.parse(arrCurrentETL[2]), arrCurrentETL[11]);

				Iterator<String> iter = l.iterator();
				while (iter.hasNext()) {
					String s = (String) iter.next();

					sb.append("<option value='" + s + "'>" + s + "</option>");
				}
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, sb.toString());
			} else {
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, SelectListGenerator.createActiveAirportCodeList());
			}

		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	/**
	 * Sets the Title list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setTitleList(HttpServletRequest request) throws Exception {
		String arrTitles = SelectListGenerator.createPaxTitleArrayForAdult();
		request.setAttribute(WebConstants.PAX_TITLES, arrTitles);
	}

	private static void setPaxTypesList(HttpServletRequest request) throws ModuleException {
		String arrPaxTypes = SelectListGenerator.createPaxTypesArray();
		request.setAttribute(WebConstants.PAX_TYPE, arrPaxTypes);
	}

	/**
	 * Sets the Active Online Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"ETLDetailProcessingRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ETLDetailProcessingHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("ETLProcessingRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the ETL Processing rows to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setETLProcessingRowHtml(HttpServletRequest request) throws Exception {
		try {
			ETLDetailProcessingHTMLGenerator htmlGen = new ETLDetailProcessingHTMLGenerator();
			String strHtml = htmlGen.getETLProcessingRowHtml(request);

			String strFirstTime = request.getParameter(PARAM_FIRST_TIME) == null ? "" : request.getParameter(PARAM_FIRST_TIME);
			int currentEtlId = 0;
			ETL etl = null;
			Object activeETL = request.getParameter(PARAM_CURRENT_ETL_ID);
			if (activeETL != null) {
				currentEtlId = Integer.parseInt(activeETL.toString());
			}
			if (currentEtlId != 0) {
				etl = ModuleServiceLocator.getEtlBD().getETL(currentEtlId);
			}
			if (etl != null && strFirstTime.equals("")) {
				strHtml += " etlState = '" + etl.getProcessedStatus() + "'; ";
			}
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("ETLProcessingHandler.setETLProcessingRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Making the String Compatible Spaces
	 * 
	 * @param value
	 *            the string to make compatible
	 * @return String the compatible String
	 */
	// private static String makeStringCompliant(String value) {
	// char[] charArray = PlatformUtiltiies.nullHandler(value).toUpperCase().toCharArray();
	// for (int i = 0; i < charArray.length; i++) {
	// int ascii = (int) charArray[i];
	// if (ascii < 65 | ascii > 90) {
	// charArray[i] = ' ';
	// }
	// }
	// String g = new String(charArray);
	// return g.replaceAll(" ", "");
	// }

	/**
	 * Sets Pax Category Type to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createPaxCatagoryList();
		request.setAttribute(WebConstants.REQ_PAXCAT_LIST, strHtml);
	}
}
