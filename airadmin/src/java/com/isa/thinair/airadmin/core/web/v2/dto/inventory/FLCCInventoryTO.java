package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;

/**
 * @author eric
 * 
 */
public class FLCCInventoryTO implements Serializable, Comparable<FLCCInventoryTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int fccInvId;

	private int flightId;

	private String ccCode;

	private String logicalCCCode;

	private String logicalCCName;

	private int logicalCCRank;

	private int adultSeatAllocation;

	private int infantSeatAllocation;

	private String logicalCabinClassDescription;

	public int getFccInvId() {
		return fccInvId;
	}

	public void setFccInvId(int fccInvId) {
		this.fccInvId = fccInvId;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public int getAdultSeatAllocation() {
		return adultSeatAllocation;
	}

	public void setAdultSeatAllocation(int adultSeatAllocation) {
		this.adultSeatAllocation = adultSeatAllocation;
	}

	public int getInfantSeatAllocation() {
		return infantSeatAllocation;
	}

	public void setInfantSeatAllocation(int infantSeatAllocation) {
		this.infantSeatAllocation = infantSeatAllocation;
	}

	public String getLogicalCCName() {
		return logicalCCName;
	}

	public void setLogicalCCName(String logicalCCName) {
		this.logicalCCName = logicalCCName;
	}

	public int getLogicalCCRank() {
		return logicalCCRank;
	}

	public void setLogicalCCRank(int logicalCCRank) {
		this.logicalCCRank = logicalCCRank;
	}

	public String getLogicalCabinClassDescription() {
		return this.logicalCabinClassDescription;
	}

	public void setLogicalCabinClassDescription(String logicalCabinClassDescription) {
		this.logicalCabinClassDescription = logicalCabinClassDescription;
	}

	@Override
	public int compareTo(FLCCInventoryTO fccsegInv) {
		return Integer.valueOf(this.getLogicalCCRank()).compareTo(Integer.valueOf(fccsegInv.getLogicalCCRank()));
	}

}
