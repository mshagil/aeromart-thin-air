package com.isa.thinair.airadmin.core.web.v2.action.master;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.BundleCategorySearch;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.isa.thinair.airadmin.core.web.constants.WebConstants.*;


@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowBundledCategoryAction {

    private final int PAGE_LENGTH = 10;

    private Object[] rows;
    private int total;
    private int records;
    private int page;
    private static Log log = LogFactory.getLog(ShowBundledCategoryAction.class);
    private BundleFareCategoryDTO bundleFareCategory;
    private Collection<Language> allLanguages;
    private List<String> allPriorities;
    private BundleCategorySearch searchCriteria;
    private String defaultLanguage;

    private boolean success = true;
    private String messageTxt;

    public String searchCategories() {

        try {

            if(searchCriteria == null) {
                searchCriteria = new BundleCategorySearch();
            }
            searchCriteria.setPageNumber((page == 0)? 1 : page);
            searchCriteria.setPageSize(PAGE_LENGTH);

            Page<BundleFareCategoryDTO> results = ModuleServiceLocator.getBundledFareBD().searchBundledFareCategories(searchCriteria);
            rows = results.getPageData().toArray();

            records = rows.length;
            total = results.getTotalNoOfRecords();

            records = results.getTotalNoOfRecords();
            total = records % PAGE_LENGTH > 0 ? (records / PAGE_LENGTH) + 1 : (records / PAGE_LENGTH);

            messageTxt = "Sucessfully searched the bundle categories";

        } catch (Exception e) {

            success = false;
            messageTxt = "Failed to search bundle categories";
            log.error(messageTxt, e);
        }

        return S2Constants.Result.SUCCESS;
    }

    public String execute() {

        try {

            allLanguages = PromotionModuleServiceLocator.getCommonMasterBD().getLanguages();
            allPriorities = ModuleServiceLocator.getBundledFareBD().getAllCategoryPriorities();
            defaultLanguage = AppSysParamsUtil.getSystemDefaultLanguage();

        } catch (ModuleException e) {

            success = false;
            messageTxt = "Failed to get initialising data for bundle categories";
            log.error(messageTxt, e);
        }

        return S2Constants.Result.SUCCESS;
    }

    public String add() {

        try {

            Map<String, BundleFareCategoryDTO.BundledFareCategoryTranslation> translations =  bundleFareCategory.getTranslations();
            if(translations == null) {
                translations = new HashMap<>();
            }

            translations.put(AppSysParamsUtil.getSystemDefaultLanguage(),
                    new BundleFareCategoryDTO.BundledFareCategoryTranslation(bundleFareCategory.getCategoryName(), bundleFareCategory.getDescription()));
            bundleFareCategory.setTranslations(translations);

            ModuleServiceLocator.getBundledFareBD().createOrUpdateCategory(bundleFareCategory);

            messageTxt = "Bundle category successfully added";

        } catch (ModuleException e) {

            success = false;
            messageTxt = "Failed to add bundle category";
            log.error(messageTxt, e);
        }

        return S2Constants.Result.SUCCESS;
    }


    public String edit() {

        try {

            Map<String, BundleFareCategoryDTO.BundledFareCategoryTranslation> translations =  bundleFareCategory.getTranslations();
            if(translations == null) {
                translations = new HashMap<>();
            }

            translations.put(AppSysParamsUtil.getSystemDefaultLanguage(),
                    new BundleFareCategoryDTO.BundledFareCategoryTranslation(bundleFareCategory.getCategoryName(), bundleFareCategory.getDescription()));
            bundleFareCategory.setTranslations(translations);

            ModuleServiceLocator.getBundledFareBD().createOrUpdateCategory(bundleFareCategory);

            messageTxt = "Bundle category successfully edited";

        } catch (ModuleException e) {

            success = false;
            messageTxt = "Failed to edit bundle category";
            log.error(messageTxt, e);
        }

        return S2Constants.Result.SUCCESS;
    }


    public String delete() {

        try {

            ModuleServiceLocator.getBundledFareBD().deleteCategory(bundleFareCategory);

            messageTxt = "Bundle category successfully deleted";

        } catch (ModuleException e) {

            success = false;
            messageTxt = "Failed to delete bundle category";
            log.error(messageTxt, e);
        }

        return S2Constants.Result.SUCCESS;
    }


    public BundleFareCategoryDTO getBundleFareCategory() {
        return bundleFareCategory;
    }

    public void setBundleFareCategory(BundleFareCategoryDTO bundleFareCategory) {
        this.bundleFareCategory = bundleFareCategory;
    }

    public Object[] getRows() {
        return rows;
    }

    public void setRows(Object[] rows) {
        this.rows = rows;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getRecords() {
        return records;
    }

    public void setRecords(int records) {
        this.records = records;
    }

    public BundleCategorySearch getSearchCriteria() {
        return searchCriteria;
    }

    public void setSearchCriteria(BundleCategorySearch searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    public Collection<Language> getAllLanguages() {
        return allLanguages;
    }

    public void setAllLanguages(Collection<Language> allLanguages) {
        this.allLanguages = allLanguages;
    }

    public List<String> getAllPriorities() {
        return allPriorities;
    }

    public void setAllPriorities(List<String> allPriorities) {
        this.allPriorities = allPriorities;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageTxt() {
        return messageTxt;
    }

    public void setMessageTxt(String messageTxt) {
        this.messageTxt = messageTxt;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
}
