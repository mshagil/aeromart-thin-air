package com.isa.thinair.airadmin.core.web.handler.reports;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.util.LookupUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.util.*;

public class OriginCountryOfCallReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(OriginCountryOfCallReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static final String REPORT_ID = "UC_REPM_088";

	private static final String HDN_MODE = "hdnMode";

	private static final String COUNTRY_LIST = "countrylist";

	private static final String TXT_FROM_DATE = "txtFromDate";

	private static final String TXT_TO_DATE = "txtToDate";

	private static final String REPORT_OPTION = "radReportOption";

	private static final String HDN_COUNTRIES = "hdnCountries";

	private static final String FROM = "from";

	private static final String TO = "to";

	private static final String OPTION = "option";

	private static final String ORIGIN_COUNTRY = "originCountry";

	private static final String IMAGE = "IMG";

	private static final String REPORT_FORMAT = "radRptNumFormat";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter(HDN_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("OriginCountryOfCallReportRH setDisplayData() SUCCESS");

			if (WebConstants.ACTION_VIEW.equals(strHdnMode)) {
				setReportView(request, response);
				log.error("OriginCountryOfCallReportRH setReportView() Success");
				return null;
			} else if (WebConstants.ACTION_DETAIL.equals(strHdnMode)) {
				setDetailReportView(request, response);
				log.error("OriginCountryOfCallReportRH setDetailReportView Success");
				return null;
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("OriginCountryOfCallReportRH ReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("OriginCountryOfCallReportRH setDisplayData() FAILED " + e.getMessage());
		}
		return forward;
	}

	/**
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountryList(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	public static void setCountryList(HttpServletRequest request) throws ModuleException {
		String strCountryHtml = JavascriptGenerator.createCountryHtml();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strCountryHtml);
	}

	/**
	 * Sets the report medium to the request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			BeanUtils.nullHandler(strClientErrors);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 *
	 * @param request the HttpServletRequest
	 * @throws ModuleException the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * @param strCountryList
	 * @return
	 */
	protected static List<String> getCountryCodes(String strCountryList) {
		List<String> countryList = new ArrayList<>();
		if (strCountryList != null && strCountryList != "") {
			countryList = Arrays.asList(strCountryList.split(","));
		}
		List<String> countryCodes = new ArrayList<>();
		List<String[]> countries = LookupUtils.LookupDAO().getTwoColumnMap(COUNTRY_LIST);
		for (String[] countryDetails : countries) {
			if (countryList.contains(countryDetails[1])) {
				countryCodes.add(countryDetails[0]);
			}
		}
		return countryCodes;
	}

	/**
	 * Set the Report view
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response)
			throws ModuleException {

		ResultSet resultSet;
		String fromDate = request.getParameter(TXT_FROM_DATE);
		String toDate = request.getParameter(TXT_TO_DATE);
		String value = request.getParameter(REPORT_OPTION);
		String strCountryList = request.getParameter(HDN_COUNTRIES);
		List<String> countryCodes = getCountryCodes(strCountryList);

		String templateOriginCountrySummary = "OriginCountryOfCallSummaryReport.jasper";

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			Map<String, Object> parameters = new HashMap<>();

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (countryCodes != null && countryCodes.size() > 0) {
				search.setSelectedCountries(countryCodes);
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(templateOriginCountrySummary));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getOriginCountryOfCallData(search);

			setReportOutPutOption(request, response, resultSet, fromDate, toDate, value, REPORT_ID, parameters);
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static void setReportOutPutOption(HttpServletRequest request, HttpServletResponse response,
			ResultSet resultSet, String fromDate, String toDate, String value, String id,
			Map<String, Object> parameters) throws ModuleException {
		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		parameters.put("ID", id);

		parameters.put("DETAIL_ABS_TARGET", "");

		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);

		// To provide Report Format Options
		String reportNumFormat = request.getParameter(REPORT_FORMAT);
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (WebConstants.REPORT_HTML.equals(value.trim())) {
			parameters.put(IMAGE, reportsRootDir);
			ModuleServiceLocator.getReportingFrameworkBD()
					.createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null, reportsRootDir, response);
		} else if (WebConstants.REPORT_PDF.equals(value.trim())) {
			reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put(IMAGE, reportsRootDir);
			response.reset();
			response.addHeader("Content-Disposition", "filename=OriginCountryOfCall.pdf");
			ModuleServiceLocator.getReportingFrameworkBD()
					.createPDFReport(WebConstants.REPORT_REF, parameters, resultSet, response);
		} else if (WebConstants.REPORT_EXCEL.equals(value.trim())) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=OriginCountryOfCall.xls");
			ModuleServiceLocator.getReportingFrameworkBD()
					.createXLSReport(WebConstants.REPORT_REF, parameters, resultSet, response);
		} else if (WebConstants.REPORT_CSV.equals(value.trim())) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=OriginCountryOfCall.csv");
			ModuleServiceLocator.getReportingFrameworkBD()
					.createCSVReport(WebConstants.REPORT_REF, parameters, resultSet, response);
		}
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	protected static void setDetailReportView(HttpServletRequest request, HttpServletResponse response)
			throws ModuleException {
		ResultSet resultSet;
		String fromDate = request.getParameter(FROM);
		String toDate = request.getParameter(TO);
		String value = request.getParameter(OPTION);
		String country = request.getParameter(ORIGIN_COUNTRY);

		String templateOriginCountry = "OriginCountryOfCallDetailsReport.jasper";

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			Map<String, Object> parameters = new HashMap<>();

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (!StringUtil.isNullOrEmpty(country)) {
				search.setOriginCountry(country);
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(templateOriginCountry));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getOriginCountryOfCallDetail(search);

			setReportOutPutOption(request, response, resultSet, fromDate, toDate, value, REPORT_ID, parameters);

		} catch (Exception e) {
			log.error(e);
		}
	}
}
