package com.isa.thinair.airadmin.core.web.action.security;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.security.ShowMenuRH;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.MENU_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.PASS_CHANGE, value = AdminStrutsConstants.AdminJSP.PASS_CHANGE_JSP) })
public class ShowMenuAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		return ShowMenuRH.execute(request);
	}

}
