package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airpricing.api.criteria.DefaultAnciTemplSearchCriteria;
import com.isa.thinair.airpricing.api.dto.DefaultAnciTemplateTO;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplateRoute;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.Constants;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowDefaultAnciTemplateAction {

	private static Log log = LogFactory.getLog(ShowDefaultAnciTemplateAction.class);

	private Object[] rows;

	private String succesMsg;

	private String msgType;

	private DefaultAnciTemplateTO defAnciTemplTO;

	private DefaultAnciTemplSearchCriteria searchCriteria;

	private int records;

	private int total;

	private int page;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String execute() {
		try {

			Page<DefaultAnciTemplateTO> pgAnciTmpl = ModuleServiceLocator.getCommonAncillaryServiceBD().searchDefAnciTemplate(
					searchCriteria, records, 20);

			this.records = pgAnciTmpl.getTotalNoOfRecords();
			this.total = pgAnciTmpl.getTotalNoOfRecords() / 20;
			int mod = pgAnciTmpl.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page < 0) {
				this.page = 1;
			}

			Collection<DefaultAnciTemplateTO> defAnciTemplcol = pgAnciTmpl.getPageData();
			if (defAnciTemplcol != null) {
				Object[] dataRow = new Object[defAnciTemplcol.size()];
				int index = 1;
				for (DefaultAnciTemplateTO grdCountry : defAnciTemplcol) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("defAnciTemplate", grdCountry);
					counmap.put("defAnciTemplateRoutes", createOndString(grdCountry.getAssignedOnds()));
					dataRow[index - 1] = counmap;
					index++;
				}
				this.rows = dataRow;
			}

		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String saveDefaultAnciTemplate() {
		try {

			doValidation();
			DefaultAnciTemplate defAnciTemp = createDefaultAnciTemplate();

			ModuleServiceLocator.getCommonAncillaryServiceBD().saveDefaultAnciTemplate(defAnciTemp);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			log.error("Error @ DefaultAnciTemplate  templates", e);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				this.succesMsg = airadminConfig.getMessage("um.DefaultAnciTemplate.ond.exist");
			} else if (e.getMessageString().equals(MessagesUtil.getMessage("module.saveconstraint.childerror"))) {
				this.succesMsg = airadminConfig.getMessage("um.DefaultAnciTemplate.invalidRoutes.exist");
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			log.error("Error @ savig Default Anci templates", ex);
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteDefaultAnciTemplate() {
		try {
			DefaultAnciTemplate defAnciTemp = createDefaultAnciTemplate();

			ModuleServiceLocator.getCommonAncillaryServiceBD().deleteDefaultAnciTemplate(defAnciTemp);

			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			log.error("Error @ DefaultAnciTemplate  templates", e);
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			log.error("Error @ savig Default Anci templates", ex);
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}
	
	private void doValidation() throws ModuleException {
		ModuleServiceLocator.getCommonAncillaryServiceBD().checkDuplicateDefaultAnciTemplateForSameRoute(defAnciTemplTO);
	}

	private Object createOndString(List<DefaultAnciTemplateRoute> assignedONDs) {
		StringBuilder sb = new StringBuilder();

		for (DefaultAnciTemplateRoute ond : assignedONDs) {
			sb.append(ond.getDefaultAnciTemplateRouteId()).append(Constants.COMMA_SEPARATOR);
			sb.append(ond.getOndCode()).append(Constants.COMMA_SEPARATOR);
			sb.append(ond.getVersion()).append(Constants.COMMA_SEPARATOR);
			sb.append(Constants.CARET_SEPERATOR);
		}
		return sb.toString();
	}

	private DefaultAnciTemplate createDefaultAnciTemplate() {
		DefaultAnciTemplate defAnciTempl = new DefaultAnciTemplate();

		defAnciTempl.setDefaultAnciTemplateId(defAnciTemplTO.getDefaultAnciTemplateId());
		defAnciTempl.setAnciTemplateId(defAnciTemplTO.getAnciTemplateId());
		defAnciTempl.setAncillaryType(defAnciTemplTO.getAncillaryType());
		defAnciTempl.setStatus(defAnciTemplTO.getStatus());
		defAnciTempl.setCreatedBy(defAnciTemplTO.getCreatedBy());
		defAnciTempl.setCreatedDate(defAnciTemplTO.getCreatedDate());

		if (DefaultAnciTemplate.ANCI_TEMPLATES.SEAT.toString().equals(defAnciTemplTO.getAncillaryType())) {
			defAnciTempl.setAirCraftModelnumber(defAnciTemplTO.getAirCraftModelnumber());
		}

		List<String> ondCodeList = new ArrayList<>();
		ondCodeList.addAll(Arrays.asList(defAnciTemplTO.getOndString().split(Constants.COMMA_SEPARATOR)));

		if (defAnciTemplTO.getVersion() == -1) {
			defAnciTempl.setAssignedONDs(createDefaultAnciTemplateRoutes(defAnciTempl, ondCodeList));
		} else {
			defAnciTempl.setVersion(defAnciTemplTO.getVersion());
			defAnciTempl.setAssignedONDs(updateDefaultAnciTemplateRoutes(defAnciTempl, ondCodeList));
		}
		return defAnciTempl;
	}

	private Set<DefaultAnciTemplateRoute> updateDefaultAnciTemplateRoutes(DefaultAnciTemplate defAnciTempl,
			List<String> updatedONDList) {
		Set<DefaultAnciTemplateRoute> routeSet = new HashSet<>();

		for (DefaultAnciTemplateRoute route : defAnciTemplTO.getAssignedOnds()) {
			if (updatedONDList.contains(route.getOndCode())) {
				updatedONDList.remove(updatedONDList.indexOf(route.getOndCode()));

				DefaultAnciTemplateRoute defAnciTemplRout = new DefaultAnciTemplateRoute();
				defAnciTemplRout.setOndCode(route.getOndCode());
				defAnciTemplRout.setTemplate(defAnciTempl);	
				
				routeSet.add(defAnciTemplRout);
			}
		}
		routeSet.addAll(createDefaultAnciTemplateRoutes(defAnciTempl, updatedONDList));
		return routeSet;
	}

	private Set<DefaultAnciTemplateRoute> createDefaultAnciTemplateRoutes(DefaultAnciTemplate defAnciTempl,
			List<String> ondStrList) {
		Set<DefaultAnciTemplateRoute> routeSet = new HashSet<>();

		for (String ondCode : ondStrList) {
			DefaultAnciTemplateRoute defAnciTemplRout = new DefaultAnciTemplateRoute();
			defAnciTemplRout.setOndCode(ondCode);
			defAnciTemplRout.setTemplate(defAnciTempl);
			routeSet.add(defAnciTemplRout);
		}

		return routeSet;
	}

	/**
	 * @return the defAnciTemplTO
	 */
	public DefaultAnciTemplateTO getDefAnciTemplTO() {
		return defAnciTemplTO;
	}

	/**
	 * @param defAnciTemplTO
	 *            the defAnciTemplTO to set
	 */
	public void setDefAnciTemplTO(DefaultAnciTemplateTO defAnciTemplTO) {
		this.defAnciTemplTO = defAnciTemplTO;
	}

	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the searchCriteria
	 */
	public DefaultAnciTemplSearchCriteria getSearchCriteria() {
		return searchCriteria;
	}

	/**
	 * @param searchCriteria
	 *            the searchCriteria to set
	 */
	public void setSearchCriteria(DefaultAnciTemplSearchCriteria searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

}
