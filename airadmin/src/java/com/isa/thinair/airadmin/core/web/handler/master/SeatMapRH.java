package com.isa.thinair.airadmin.core.web.handler.master;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.SeatChargesHG;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thush
 * 
 */

public final class SeatMapRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(SeatMapRH.class);
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_SEATDATA = "hdnSeatsCharge";
	private static final String PARAM_SEARCH_MODEL = "selModelNo";
	private static final String PARAM_SEARCH_TEMPLATE = "selTemplate";
	private static final String PARAM_RECNO = "hdnRecNo";

	private static final String PARAM_TMPL_CODE = "txtTepmplateCode";
	private static final String PARAM_TMPL_DESCRIPTION = "txtDescription";
	private static final String PARAM_TMPL_DEFALULT = "txtChargeAmt";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_TMPL_ID = "hdnTmplId";
	private static final String PARAM_MODEL_NO = "selFltModelNo";
	private static final String PARAM_ACTION = "hdnAction";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_PREV_DEF_CHARGE_AMT = "hdnChargeAmount";
	private static final String PARAM_LOCAL_CURR_CODE ="localCurrCode";
	private static final String PARAM_IS_EDIT_DEFAULT_AMOUMNT ="hdnIsEditDefaultAmount";
	private static final String STATUS_ACTIVE ="ACT";

	private String seatMap;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Main Execute Method for Cgarge Template Allocation
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled()) {
			log.debug("Inside seat map execution");
		}
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strFormData = "";
		String strHdnMode = request.getParameter(PARAM_MODE);
		String strHdnAction = request.getParameter(PARAM_ACTION);

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					checkPrivilege(request, WebConstants.MAS_SEAT_CHARGE_EDIT);
					saveData(request);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);

				} else {
					checkPrivilege(request, WebConstants.MAS_SEAT_CHARGE_ADD);
					saveData(request);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
				}
			}

		} catch (ModuleException me) {

			saveMessage(request, me.getExceptionCode(), WebConstants.MSG_ERROR);
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException exp) {

			}
			if (log.isErrorEnabled()) {
				log.error("Exception in setmap.execute " + "[origin module=" + me.getModuleDesc() + "]", me);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("error on saving seat map : " + e.getStackTrace(), e);
			}
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException exp) {

			}
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		try {

			if (strHdnMode != null && strHdnMode.equals("VIEW")) {
				// checkPrivilege(request,WebConstants.MAS_SEAT_CHARGE_SEAT);
				setSeatMap(request);
				forward = AdminStrutsConstants.AdminAction.VIEW;
				return forward;
			}

		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
				checkPrivilege(request, WebConstants.MAS_SEAT_CHARGE_CANCELL);
				Properties prop = getProperties(request);
				String chrgeId = prop.getProperty(PARAM_TMPL_ID);
				validate(null, true);
				if (chrgeId != null && !chrgeId.trim().equals("")) {
					AirInventoryModuleUtils.getChargeBD().deleteTemplate(new Integer(chrgeId));
				}
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}

		} catch (ModuleException me) {
			saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException exp) {

			}
			if (log.isErrorEnabled()) {
				log.error("Exception in setmap.execute " + "[origin module=" + me.getModuleDesc() + "]", me);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("error on saving seat map : " + e.getStackTrace(), e);
			}
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException exp) {

			}
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		try {
			displayData(request);
		} catch (ModuleException me) {
			saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException exp) {

			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("error on saving seat map : " + e.getStackTrace(), e);
			}
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException exp) {

			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		return forward;
	}

	/**
	 * creates the Display Data
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void displayData(HttpServletRequest request) throws ModuleException {

		setFlightModel(request);
		setTemplateCodes(request);
		setClientErrors(request);
		setSeatCharges(request);
		setCurrencyComboList(request);
		setLocalCurrPriv(request);
		setBaseCurrency(request);
	}

	public static void setLogicalCabinClassList(HttpServletRequest request) throws ModuleException {
		String strHtmlLCC = SelectListGenerator.createAllLogicalCabinClass();
		request.setAttribute(WebConstants.REQ_HTML_LOGICALCABINCLASS_LIST, strHtmlLCC);
	}
	
	public static void setCurrencyComboList(HttpServletRequest request) throws ModuleException {
		String strCurrencyComboList =SelectListGenerator.createCurrencyList(); 
		request.setAttribute(WebConstants.REQ_HTML_CURRENCY_COMBO, strCurrencyComboList);		
	}
	
	public static void setLocalCurrPriv(HttpServletRequest request) throws ModuleException {
		String privlege  =String.valueOf(BasicRequestHandler.hasPrivilege(request, WebConstants.MAS_BG_CHRG_LOCL_CURR));
		request.setAttribute(WebConstants.REQ_SEAT_CHRG_LOCL_CURR,privlege);		
	}
	
	public static void setBaseCurrency(HttpServletRequest request) throws ModuleException {
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		request.setAttribute(WebConstants.REQ_BASE_CURRENCY, baseCurrency);		
	}

	public static void setCabinClassList(HttpServletRequest request) throws ModuleException {
		String strHtmlCC = SelectListGenerator.createAllCabinClass();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strHtmlCC);
	}

	/**
	 * sets the model data for search
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightModel(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAircraftModelListForSeatMap_SG();
		request.setAttribute(WebConstants.SEAT_MODEL_DATA, strHtml);
	}

	/**
	 * sets the template codes for search data
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargesTemplate();
		request.setAttribute(WebConstants.SEAT_TEMPL_CODE, strHtml);
	}

	/**
	 * Creates the seat map data for a perticular model
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSeatMap(HttpServletRequest request) throws ModuleException {
		String strHtml = "";
		SeatChargesHG seatMapHg = new SeatChargesHG(getProperties(request));
		strHtml = seatMapHg.getSeatMapHtml(request);
		request.setAttribute(WebConstants.SEAT_MAP_DATA, strHtml);
		String strSeatCharges = "";
		strSeatCharges = seatMapHg.getSeatCharges(request);
		request.setAttribute(WebConstants.SEAT_CHRAGES_DATA, strSeatCharges);
	}

	/**
	 * Sets the charge template data
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSeatCharges(HttpServletRequest request) throws ModuleException {
		String strHtml = "";
		SeatChargesHG seatChrgesHg = new SeatChargesHG(getProperties(request));
		strHtml = seatChrgesHg.getSeatChargesHtml(request);
		request.setAttribute(WebConstants.SEAT_CHRAGES_DATA, strHtml);
	}

	/**
	 * Sets the client Validation Data
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = SeatChargesHG.getClientErrors(request);
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Saves the charges template Data
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void saveData(HttpServletRequest request) throws ModuleException {
		Properties prop = getProperties(request);
		boolean isDefAmountChanged = Boolean.parseBoolean(prop.getProperty(PARAM_IS_EDIT_DEFAULT_AMOUMNT));
		String strSeatData = request.getParameter(PARAM_SEATDATA);
		ChargeTemplate templ = new ChargeTemplate();
		int datalen = 7;
		SeatDTO seatChgDTO = null;
		Collection<SeatDTO> seatDTOList = new ArrayList<SeatDTO>();

		templ.setModelNo(prop.getProperty(PARAM_MODEL_NO));
		if (!prop.getProperty(PARAM_TMPL_ID).equals("")) {
			templ.setTemplateId(new Integer(prop.getProperty(PARAM_TMPL_ID)));
		}
		templ.setTemplateCode(prop.getProperty(PARAM_TMPL_CODE));
		templ.setDescription(prop.getProperty(PARAM_TMPL_DESCRIPTION));
		if (!prop.getProperty(PARAM_TMPL_DEFALULT).equals("")) {
			templ.setDefaultChargeAmount(new BigDecimal(prop.getProperty(PARAM_TMPL_DEFALULT)));
		}
		templ.setUserNote(prop.getProperty(PARAM_REMARKS));
		if (!prop.getProperty(PARAM_VERSION).equals("")) {
			templ.setVersion(Long.parseLong(prop.getProperty(PARAM_VERSION)));
		}

		templ.setStatus(prop.getProperty(PARAM_STATUS).equals("Active") ? "ACT" : "INA");

		templ.setChargeLocalCurrencyCode(prop.getProperty(PARAM_LOCAL_CURR_CODE));
		
		if (strSeatData != null && !strSeatData.equals("")) {

			String seatArr[] = strSeatData.split(",");
			int noofseats = seatArr.length / datalen;
			int noofleft = seatArr.length % datalen;
			if (noofleft > 0) {
				noofseats = noofseats + 1;
			}
			int arrItem = 0;
			for (int x = 0; x < noofseats; x++) {
				arrItem = datalen * x;
				seatChgDTO = new SeatDTO();
				if (seatArr[arrItem + 0] != null && !seatArr[arrItem + 0].equals("")) {
					seatChgDTO.setChargeID(new Integer(seatArr[arrItem + 0]));
				}
				if (seatArr[arrItem + 1] != null) {
					seatChgDTO.setSeatID(new Integer(seatArr[arrItem + 1]));
				}
				if (seatArr[arrItem + 3] != null && !seatArr[arrItem + 3].equals("")) {
					if (!prop.getProperty(PARAM_PREV_DEF_CHARGE_AMT).equals("")
							&& (!prop.getProperty(PARAM_PREV_DEF_CHARGE_AMT).equals(prop.getProperty(PARAM_TMPL_DEFALULT)))) {
						if (!seatArr[arrItem + 3].equals(prop.getProperty(PARAM_PREV_DEF_CHARGE_AMT))
								&& !seatArr[arrItem + 3].equals(prop.getProperty(PARAM_TMPL_DEFALULT))) {
							seatChgDTO.setChargeAmount(new BigDecimal(seatArr[arrItem + 3]));
						} else if (seatArr[arrItem + 3].equals(prop.getProperty(PARAM_PREV_DEF_CHARGE_AMT))) {
							seatChgDTO.setChargeAmount(new BigDecimal(prop.getProperty(PARAM_TMPL_DEFALULT)));
						} else {
							seatChgDTO.setChargeAmount(new BigDecimal(seatArr[arrItem + 3]));
						}
					} else {
						seatChgDTO.setChargeAmount(new BigDecimal(seatArr[arrItem + 3]));
					}

				}
				if (seatArr[arrItem + 4] != null && (seatArr[arrItem + 4].equals("ACT") || seatArr[arrItem + 4].equals("A"))) {
					seatChgDTO.setStatus("ACT");
				} else {
					seatChgDTO.setStatus("INA");
				}

				if (noofleft == 0) {
					if (seatArr[arrItem + 5] != null && !seatArr[arrItem + 5].equals("")) {
						seatChgDTO.setVersion(new Long(seatArr[arrItem + 5]));
					}
				} else {
					if (x != noofseats - 1) {
						if (seatArr[arrItem + 5] != null && !seatArr[arrItem + 5].equals("")) {
							seatChgDTO.setVersion(new Long(seatArr[arrItem + 5]));
						}
					}
				}
				if(Integer.parseInt(seatArr[arrItem + 6]) == 1){
					seatChgDTO.setEditedSeat(true);
				}
				seatDTOList.add(seatChgDTO);
			}
		}	
		validate(templ, false);	
		AirInventoryModuleUtils.getChargeBD().saveSeatChargeTemplate(templ, seatDTOList, isDefAmountChanged);
	}
	
	private static void validate(ChargeTemplate template, boolean fromDelete) throws ModuleException {
		if (fromDelete || (template.getVersion() != -1 && !STATUS_ACTIVE.equals(template.getStatus())))
			ModuleServiceLocator.getCommonAncillaryServiceBD().checkTemplateAttachedToRouteWiseDefAnciTempl(
					DefaultAnciTemplate.ANCI_TEMPLATES.SEAT, template.getTemplateId());

	}
	 

	/**
	 * Creates and return property file from the input data
	 * 
	 * @param request
	 *            the request
	 * @return the property file
	 */
	private static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();
		String strSearchModel = request.getParameter(PARAM_SEARCH_MODEL);
		String strSearchtemplate = request.getParameter(PARAM_SEARCH_TEMPLATE);
		String strHdnRecNo = request.getParameter(PARAM_RECNO);

		String strModelNo = request.getParameter(PARAM_MODEL_NO);
		String strTemplateId = request.getParameter(PARAM_TMPL_ID);
		String strTemplateCode = request.getParameter(PARAM_TMPL_CODE);
		String strDescription = request.getParameter(PARAM_TMPL_DESCRIPTION);
		String strDefAmount = request.getParameter(PARAM_TMPL_DEFALULT);
		String strRemarks = request.getParameter(PARAM_REMARKS);
		String strVersion = request.getParameter(PARAM_VERSION);
		String strSeatData = request.getParameter(PARAM_SEATDATA);
		String strStatus = request.getParameter(PARAM_STATUS);
		String strPrevDefChargeAmt = request.getParameter(PARAM_PREV_DEF_CHARGE_AMT);
		String strLocalCurrCode = request.getParameter(PARAM_LOCAL_CURR_CODE);
		String strIsEditDefaultamount = request.getParameter(PARAM_IS_EDIT_DEFAULT_AMOUMNT);
		
		props.setProperty(PARAM_SEARCH_MODEL, AiradminUtils.getNotNullString(strSearchModel));
		props.setProperty(PARAM_SEARCH_TEMPLATE, AiradminUtils.getNotNullString(strSearchtemplate));
		props.setProperty(PARAM_RECNO, AiradminUtils.getNotNullString(strHdnRecNo));

		props.setProperty(PARAM_MODEL_NO, AiradminUtils.getNotNullString(strModelNo));
		props.setProperty(PARAM_TMPL_ID, AiradminUtils.getNotNullString(strTemplateId));
		props.setProperty(PARAM_TMPL_CODE, AiradminUtils.getNotNullString(strTemplateCode));
		props.setProperty(PARAM_TMPL_DESCRIPTION, AiradminUtils.getNotNullString(strDescription));
		props.setProperty(PARAM_TMPL_DEFALULT, AiradminUtils.getNotNullString(strDefAmount));
		props.setProperty(PARAM_REMARKS, AiradminUtils.getNotNullString(strRemarks));
		props.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(strVersion));
		props.setProperty(PARAM_SEATDATA, AiradminUtils.getNotNullString(strSeatData));
		props.setProperty(PARAM_STATUS, (strStatus == null) ? "INA" : strStatus);
		props.setProperty(PARAM_PREV_DEF_CHARGE_AMT, AiradminUtils.getNotNullString(strPrevDefChargeAmt));
		props.setProperty(PARAM_LOCAL_CURR_CODE, AiradminUtils.getNotNullString(strLocalCurrCode));
		props.setProperty(PARAM_IS_EDIT_DEFAULT_AMOUMNT, AiradminUtils.getNotNullString(strIsEditDefaultamount));

		return props;
	}

	/**
	 * Creates the form data entered by the user
	 * 
	 * @param request
	 *            the request
	 * @return String the js array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String fillEnteredFormData(HttpServletRequest request) throws ModuleException {
		Properties props = getProperties(request);
		StringBuilder sb = new StringBuilder();
		sb.append("arrFormData[1] = '" + props.getProperty(PARAM_MODEL_NO) + "';");
		sb.append("arrFormData[2] = '" + props.getProperty(PARAM_TMPL_ID) + "';");
		sb.append("arrFormData[3] = '" + props.getProperty(PARAM_TMPL_CODE) + "';");
		sb.append("arrFormData[4] = '" + props.getProperty(PARAM_TMPL_DESCRIPTION) + "';");
		sb.append("arrFormData[5] = '" + props.getProperty(PARAM_TMPL_DEFALULT) + "';");
		sb.append("arrFormData[6] = '" + props.getProperty(PARAM_REMARKS) + "';");
		sb.append("arrFormData[7] = '" + props.getProperty(PARAM_VERSION) + "';");
		sb.append("arrFormData[8] = '" + props.getProperty(PARAM_SEATDATA) + "';");
		sb.append("arrFormData[9] = '" + props.getProperty(PARAM_STATUS) + "';");
		sb.append("arrFormData[10] = '" + request.getParameter(PARAM_ACTION) + "';");
		sb.append("arrFormData[11] = '" + props.getProperty(PARAM_LOCAL_CURR_CODE) + "';");
		return sb.toString();

	}

	/**
	 * @return the seatMap
	 */
	public String getSeatMap() {
		return seatMap;
	}

	/**
	 * @param seatMap
	 *            the seatMap to set
	 */
	public void setSeatMap(String seatMap) {
		this.seatMap = seatMap;
	}

}
