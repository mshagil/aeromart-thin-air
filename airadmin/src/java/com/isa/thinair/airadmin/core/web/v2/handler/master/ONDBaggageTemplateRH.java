package com.isa.thinair.airadmin.core.web.v2.handler.master;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ONDBaggageTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Kasun
 * 
 */
public class ONDBaggageTemplateRH {

	public static Page searchONDBaggageTemplate(int pageNo, BaggageTemplateSearchCriteria criteria) throws ModuleException {
		Page page = ModuleServiceLocator.getChargeBD().getONDBaggageTemplates(criteria, (pageNo - 1) * 20, 20);
		return page;
	}

	public static void saveONDBaggageTemplate(ONDBaggageTemplateDTO baggageTemplateDTO) throws ModuleException {
		ModuleServiceLocator.getChargeBD().saveONDBaggageTemplate(baggageTemplateDTO, null);
	}

	public static void deleteONDBaggageTemplate(ONDBaggageTemplateDTO baggageTemplateDTO) throws ModuleException {
		ModuleServiceLocator.getChargeBD().deleteONDBaggageTemplate(baggageTemplateDTO);
	}
}
