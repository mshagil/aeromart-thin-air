package com.isa.thinair.airadmin.core.web.handler.reports.lms;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class FlightLoyaltyMemberRequestHandler extends BasicRequestHandler {

	
	private static Log log = LogFactory.getLog(FlightLoyaltyMemberRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public FlightLoyaltyMemberRequestHandler() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setClientErrors(request);
			ReportsHTMLGenerator.createPreferedReportOptions(request);
			setOnlineAirportHtml(request);
			
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("FlightLoyaltyMemberRequestHandler setReportView Success");
				return null;
			} else {
				log.error("FlightLoyaltyMemberRequestHandler setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("FlightLoyaltyMemberRequestHandler setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
		}
	}
	
	/**
	 * Sets the Online Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOnlineAirportListWOTag();
		request.setAttribute(WebConstants.SES_HTML_ONLINE_AIRPORTCODE_LIST_DATA, strHtml);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String id = "SC_PROMO_07";
		String reportTemplate = "FlightLoyaltyMember.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setDateRangeFrom(fromDate);
			search.setDateRangeTo(toDate);
			search.setFlightNumber(request.getParameter("txtFlightNo"));
			
			String source = request.getParameter("selFromStn");
			String destination = request.getParameter("selToStn");
			
			search.setFrom(source);
			search.setTo(destination);
			
			String via1 = request.getParameter("selVia1");
			String via2 = request.getParameter("selVia2");
			
			if((via1 != null && via1 != "") || (via2 != null  && via2 != "")){
				List<String> viaPoints = new ArrayList<String>();
			
				if(!"".equals(source) && source != null){
					viaPoints.add(source);
				}
				if(!"".equals(via1) && via1 != null){
					viaPoints.add(via1);
				}
				if(!"".equals(via2) && via2 != null){
					viaPoints.add(via2);
				}
				if(!"".equals(destination) && destination != null){
					viaPoints.add(destination);
				}
				search.setViaPoints(viaPoints);
			}
			
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getFlightLoyaltyMembers(search);

			Map<Object, Object> imagesMap = new HashMap<Object, Object>();
			imagesMap.put("IMG_PATH", image);
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM", fromDate);
			parameters.put("TO", toDate);
			parameters.put("REPORT_ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG_PATH", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, imagesMap,
						null, response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=FlightLoyaltyMember.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlightLoyaltyMember.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlightLoyaltyMember.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
