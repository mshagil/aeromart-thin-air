package com.isa.thinair.airadmin.core.web.v2.action.search;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.dto.SmartSearchDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SmartSearchServicesAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(SmartSearchServicesAction.class);

	private SmartSearchDTO searchTO;

	private boolean success = true;
	private String messageTxt;

	public String search() {
		try {

			searchTO.setSearchResult(ModuleServiceLocator.getSmartSearchBD().search(searchTO));

		} catch (Exception x) {
			success = false;
			messageTxt = "Search service fail";
			log.error(messageTxt, x);
		}
		return S2Constants.Result.SUCCESS;
	}

	public SmartSearchDTO getSearchTO() {
		return searchTO;
	}

	public void setSearchTO(SmartSearchDTO searchTO) {
		this.searchTO = searchTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}
}
