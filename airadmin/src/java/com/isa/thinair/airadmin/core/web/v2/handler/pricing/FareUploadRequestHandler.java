package com.isa.thinair.airadmin.core.web.v2.handler.pricing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.airadmin.core.web.v2.util.FareUtil;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Jagath Kumara
 * 
 */
public class FareUploadRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(FareUploadRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static List<String> msgList;

	private static String airportmessage = "";
	private static String validationmessage = "";
	private static boolean isExceptionOccured = false;
	private static final String PARAM_UPLOAD_OPTION = "rndOption";
	private static final String UPLOAD_FILE = "csvFareUploadFileName";
	private static SimpleDateFormat dateForamt = new SimpleDateFormat("dd/MM/yyyy");

	private static String PARAM_HDN_OND = "hdnOND";
	private static String ADD_ALL_ACTION = "ADD_ALL";

	private static final String PARAM_ORIGIN = "hdnOrigin";
	private static final String PARAM_DESTINATION = "hdnDestination";
	private static final String PARAM_VIA1 = "hdnVia1";
	private static final String PARAM_VIA2 = "hdnVia2";
	private static final String PARAM_VIA3 = "hdnVia3";
	private static final String PARAM_VIA4 = "hdnVia4";

	private static final String PARAM_BC = "hdnBC";

	private static final String PARAM_AED = "txtAED";
	private static final String PARAM_AED_CHL = "txtCHL";
	private static final String PARAM_AED_INF = "txtINF";
	private static final String PARAM_AED_CHL_SEL = "selCamt";
	private static final String PARAM_AED_INF_SEL = "selIamt";

	private static final String PARAM_SEL_LOCAL_CURRENCY = "hdnLocalCurrencySelected";
	private static final String PARAM_BASE_IN_LOCAL = "txtBaseInLocal";
	private static final String PARAM_CH_FARE_IN_LOCAL = "txtCHFareInLocal";
	private static final String PARAM_INF_FARE_IN_LOCAL = "txtINFInLocal";
	private static final String PARAM_CHL_SEL_IN_LOCAL = "selCamtInLocal";
	private static final String PARAM_INF_SEL_IN_LOCAL = "selIamtInLocal";

	private static final String PARAM_FROMDATE = "hdnfromDate";
	private static final String PARAM_SALES_FROMDATE = "hdnSalesfromDate";
	private static final String PARAM_RTFROMDATE = "hdnfromDateInbound";
	private static final String PARAM_TODATE = "txtToDt";
	private static final String PARAM_SALESTODATE = "txtSlsEnDate";
	private static final String PARAM_RTTODATE = "txtToDtInbound";
	private static final String PARAM_FC = "hdnFareCode";
	private static final String PARAM_FARE_ID = "hdnFareID";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_CHK_RETURN = "chkAllowReturn";

	private static final String FARE_VERSION = "hdnFareVersion";
	private static final String MODE = "hdnMode";

	/**
	 * Main Execute Method for Fare Upload Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String uploadForward = S2Constants.Result.SUCCESS;
		String mode = request.getParameter(MODE);
		try {
			if (mode != null && mode.equals(WebConstants.ACTION_UPLOAD)) {
				msgList = new ArrayList<String>();
				saveDataFromFile(request);
				return uploadForward;
			}
		} catch (Exception ex) {
			log.error(ex);
			return ex.getMessage();
		}
		return uploadForward;
	}

	private static void saveDataFromFile(HttpServletRequest request) {

		try {
			// store file into Properties array list
			ArrayList<Properties> propList = getPropertiesList(request);
			if (null == propList) {
				log.error("Uploaded Fare File is Empty");
				msgList.add(WebConstants.MSG_SUCCESS + "|" + airadminConfig.getMessage("um.fare.fareRule.file.empty"));
			} else if (propList.isEmpty()) {
				log.error("Uploaded Fare File has conflicted fares");
				msgList.add(WebConstants.MSG_SUCCESS + "|" + airadminConfig.getMessage("um.fare.fareRule.conflicted"));
			} else {
				int lineNumber = 0;
				for (Properties props : propList) {
					if (validateFileUplaod(props))
						saveFareFromFile(request, props);
					else
						msgList.add(WebConstants.MSG_SUCCESS + "|" + validationmessage);
					lineNumber++;
				}
			}
			String uploadStatus = "var isUploaded = true;";
			setAttribInRequest(request, "reqUploadStatus", uploadStatus);
			saveMessage(request, airadminConfig.getMessage("um.fare.upload.message"), WebConstants.MSG_SUCCESS);
			if (!msgList.isEmpty()) {
				String uploadStatusMsgs = "var strLog = \"" + ErrorMessageUtil.generateClientMessageInUploadingFares(msgList)
						+ "\";";
				setAttribInRequest(request, "reqUploadStatusMsgs", uploadStatusMsgs);
			}
		} catch (Exception e) {
			log.error("FareUploadRequestHandler.saveDataFromFile() method is failed : ", e);
		}

	}

	private static ArrayList<Properties> getPropertiesList(HttpServletRequest request) {
		ArrayList<Properties> propertiesList = null;
		try {
			FareBD fareBD = ModuleServiceLocator.getFareBD();
			String option = request.getParameter(PARAM_UPLOAD_OPTION);

			// csv file containing data
			String strFile = ((File) request.getAttribute(UPLOAD_FILE)).getAbsolutePath();

			BufferedReader br = new BufferedReader(new FileReader(strFile));

			String strLine = "";
			String dataArr[] = null;
			int lineNumber = 0;
			while ((strLine = br.readLine()) != null) {
				dataArr = strLine.replace("\"", "").split(",");
				if (dataArr.length >= 14 && 0 < lineNumber) {
					Properties props = createPropertiesFileFromUploadedFile(dataArr);
					if (option != null && option.equals(ADD_ALL_ACTION)) {
						if (validateFileUplaod(props)) {
							boolean returnFlag = false;
							// check whether new record has conflicts if ADD_ALL option is selected
							FareDTO fareDto = getFareDto(request, props);
							if (fareDto.getFareRule().getReturnFlag()  == 'Y') {
								returnFlag = true;
							}
							if (!fareBD.isFareLegal(fareDto.getFareRuleCode(), fareDto.getFare().getBookingCode(), fareDto
									.getFare().getOndCode(), fareDto.getFare().getEffectiveFromDate(), fareDto.getFare()
									.getEffectiveToDate(), fareDto.getFare().getSalesEffectiveFrom(), fareDto.getFare()
									.getSalesEffectiveTo(), fareDto.getFare().getReturnEffectiveFromDate(), fareDto.getFare()
									.getReturnEffectiveToDate(), returnFlag)) {
								return new ArrayList<Properties>();
							}
						} else
							msgList.add(WebConstants.MSG_SUCCESS + "|" + validationmessage);

					}
					if (null == propertiesList)
						propertiesList = new ArrayList<Properties>();
					propertiesList.add(props);
				} else {
					log.error("FareUploadRequestHandler.getPropertiesList() array does not contains enough data ");
				}
				lineNumber++;
			}
		} catch (Exception e) {
			log.error("FareUploadRequestHandler.getPropertiesList() method is failed : ", e);
		}
		return propertiesList;
	}

	@SuppressWarnings("unused")
	private static boolean validateFileUplaod(Properties props) {

		try {

			BookingClass bc = ModuleServiceLocator.getBookingClassBD().getLightWeightBookingClass(props.getProperty(PARAM_BC));
			FareRule fr = ModuleServiceLocator.getRuleBD().getFareRule(props.getProperty(PARAM_FC));
			if (("").equals(props.getProperty(PARAM_BC))) {
				validationmessage = airadminConfig.getMessage("um.fare.bc.required");
				return false;
			} else if (("").equals(props.getProperty(PARAM_FC))) {
				validationmessage = airadminConfig.getMessage("um.fare.fareRule.farerule.required");
				return false;
			} else if (("").equals(props.getProperty(PARAM_ORIGIN))) {
				validationmessage = airadminConfig.getMessage("um.fare.fareRule.route.required");
				return false;
			} else if (null == bc) {
				validationmessage = airadminConfig.getMessage("um.fare.bc.invalid");
				return false;
			} else if (bc.getStatus().equals("INA")) {
				validationmessage = airadminConfig.getMessage("um.fare.bc.inactive");
				return false;
			} else if (null == fr) {
				validationmessage = airadminConfig.getMessage("um.fare.fareRule.invalid");
				return false;
			} else {
				try {
					dateForamt.setLenient(false);
					Date fromDate = dateForamt.parse(props.getProperty(PARAM_FROMDATE));
					Date toDate = dateForamt.parse(props.getProperty(PARAM_TODATE));
					Date salesFromDate = dateForamt.parse(props.getProperty(PARAM_SALES_FROMDATE));
					Date salesToDate = dateForamt.parse(props.getProperty(PARAM_SALESTODATE));
					Date returnFromDate = null;
					Date returnToDate = null;
					if (null != props.getProperty(PARAM_RTFROMDATE) && !("").equals(props.getProperty(PARAM_RTFROMDATE)))
						returnFromDate = dateForamt.parse(props.getProperty(PARAM_RTFROMDATE));
					if (null != props.getProperty(PARAM_RTTODATE) && !("").equals(props.getProperty(PARAM_RTTODATE)))
						returnToDate = dateForamt.parse(props.getProperty(PARAM_RTTODATE));
					// This is to validate whether user added past date
					Date currentDate = new Date();
					if (!CalendarUtil.isSameDay(fromDate, currentDate) && CalendarUtil.isLessThan(fromDate, currentDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.fromdate.exceeds.today");
						return false;
					} else if (!CalendarUtil.isSameDay(salesFromDate, currentDate)
							&& CalendarUtil.isLessThan(salesFromDate, currentDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.slsfromdate.exceeds.current");
						return false;
					} else if (null != returnFromDate && !CalendarUtil.isSameDay(returnFromDate, currentDate)
							&& CalendarUtil.isLessThan(returnFromDate, currentDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.returnfromdate.exceeds.current");
						return false;
					} else if (null != returnFromDate && !CalendarUtil.isSameDay(returnFromDate, fromDate)
							&& CalendarUtil.isLessThan(returnFromDate, fromDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.fromdate.exceeds.rtfromdate");
						return false;
					} else if (!CalendarUtil.isSameDay(toDate, salesToDate) & CalendarUtil.isLessThan(toDate, salesToDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.slstodate.exceeds.todate");
						return false;
					} else if (CalendarUtil.isLessThan(toDate, fromDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.Todate.shoulExceedFrom");
						return false;
					} else if (CalendarUtil.isLessThan(salesToDate, salesFromDate)) {
						validationmessage = airadminConfig.getMessage("um.fare.Todate.shoulExceedFrom");
						return false;
					}
				} catch (Exception e) {
					validationmessage = airadminConfig.getMessage("um.fare.fareRule.date.invalid");
					return false;
				}
			}
		} catch (Exception e) {
			log.error("FareUploadRequestHandler.validateFileUplaod() method is failed :" + e.getMessage());
		}
		return true;
	}

	private static void saveFareFromFile(HttpServletRequest request, Properties props) {
		try {
			boolean validairport = true;
			String fareID = props.getProperty(PARAM_FARE_ID);
			String createReturn = props.getProperty(PARAM_CHK_RETURN);
			if (fareID.equals("")) {
				validairport = validateAirports(props);
			}

			if (validairport) {
				saveUploadedFares(request, props);
				if (createReturn != null && createReturn.equalsIgnoreCase("Y")
						&& request.getAttribute("isExceptionOccured").toString().equals("false")) {
					saveReturnFare(request, props);
				}
			} else {
				msgList.add(WebConstants.MSG_SUCCESS + "|" + airportmessage);
			}
		} catch (Exception e) {
			log.error("FareUploadRequestHandler.saveFareFromFile() method is failed :" + e.getMessage());
		}
	}

	@SuppressWarnings("unused")
	private static Properties createPropertiesFileFromUploadedFile(String dataArr[]) {
		Properties props = new Properties();

		// raw data from File
		String route = AiradminUtils.getNotNullString(dataArr[0]);
		String bookingClass = AiradminUtils.getNotNullString(dataArr[1]);
		String fareRule = AiradminUtils.getNotNullString(dataArr[2]);
		String salesFromDate = AiradminUtils.getNotNullString(dataArr[3]);
		String salesToDate = AiradminUtils.getNotNullString(dataArr[4]);
		String departureFromDate = AiradminUtils.getNotNullString(dataArr[5]);
		String departureToDate = AiradminUtils.getNotNullString(dataArr[6]);
		String returnFromDate = AiradminUtils.getNotNullString(dataArr[7]);
		String returnToDate = AiradminUtils.getNotNullString(dataArr[8]);
		String AdultFareAmount = AiradminUtils.getNotNullString(dataArr[9]);
		String childFareAmount = AiradminUtils.getNotNullString(dataArr[10]);
		String childFareType = AiradminUtils.getNotNullString(dataArr[11]);
		String infantFareAmount = AiradminUtils.getNotNullString(dataArr[12]);
		String infantFareType = AiradminUtils.getNotNullString(dataArr[13]);
		String status = "on";
		String uploadRequested = "";
		if (14 < dataArr.length)
			uploadRequested = AiradminUtils.getNotNullString(dataArr[14]);
		String createFareForReturn = "N";
		if (16 == dataArr.length && ("Y").equals(dataArr[15].trim()))
			createFareForReturn = AiradminUtils.getNotNullString(dataArr[15]);

		// Manipulate data to save
		props.setProperty(PARAM_HDN_OND, route);
		setRouteValues(route, props);
		props.setProperty(PARAM_AED, AdultFareAmount);
		props.setProperty(PARAM_AED_CHL, childFareAmount);
		props.setProperty(PARAM_AED_INF, infantFareAmount);

		props.setProperty(PARAM_FROMDATE, departureFromDate);
		props.setProperty(PARAM_TODATE, departureToDate);
		props.setProperty(PARAM_SALES_FROMDATE, salesFromDate);
		props.setProperty(PARAM_SALESTODATE, salesToDate);
		props.setProperty(PARAM_RTFROMDATE, returnFromDate);
		props.setProperty(PARAM_RTTODATE, returnToDate);

		props.setProperty(PARAM_FC, fareRule);
		props.setProperty(PARAM_FARE_ID, "");
		props.setProperty(PARAM_STATUS, status);
		props.setProperty(PARAM_BC, bookingClass);

		props.setProperty(PARAM_AED_CHL_SEL, childFareType.toUpperCase());
		props.setProperty(PARAM_AED_INF_SEL, infantFareType.toUpperCase());

		props.setProperty(PARAM_BASE_IN_LOCAL, "");
		props.setProperty(PARAM_CH_FARE_IN_LOCAL, "");
		props.setProperty(PARAM_INF_FARE_IN_LOCAL, "");
		props.setProperty(PARAM_CHL_SEL_IN_LOCAL, "");
		props.setProperty(PARAM_INF_SEL_IN_LOCAL, "");
		props.setProperty(FARE_VERSION, "");
		props.setProperty(PARAM_SEL_LOCAL_CURRENCY, "");
		props.setProperty(PARAM_CHK_RETURN, createFareForReturn);

		return props;
	}

	private static void setRouteValues(String route, Properties props) {
		String routeData[] = route.split("/");
		String origination = routeData[0];
		String destination = "";
		String via1 = "";
		String via2 = "";
		String via3 = "";
		String via4 = "";
		if (2 < routeData.length)
			destination = routeData[routeData.length - 1];
		else
			destination = routeData[1];

		if (2 < routeData.length)
			via1 = routeData[1];
		if (3 < routeData.length)
			via2 = routeData[2];
		if (4 < routeData.length)
			via3 = routeData[3];
		if (5 < routeData.length)
			via4 = routeData[4];

		props.setProperty(PARAM_ORIGIN, AiradminUtils.getNotNullString(origination));
		props.setProperty(PARAM_DESTINATION, AiradminUtils.getNotNullString(destination));

		props.setProperty(PARAM_VIA1, AiradminUtils.getNotNullString(via1));
		props.setProperty(PARAM_VIA2, AiradminUtils.getNotNullString(via2));
		props.setProperty(PARAM_VIA3, AiradminUtils.getNotNullString(via3));
		props.setProperty(PARAM_VIA4, AiradminUtils.getNotNullString(via4));
	}

	private static boolean validateAirports(Properties props) throws ModuleException {
		boolean validairport = true;
		List<Map<String, String>> lstAirport = SelectListGenerator.createAirportsListWithStatus();
		if (!props.getProperty(PARAM_ORIGIN).equals("")) {

			validairport = validateAirports(lstAirport, props.getProperty(PARAM_ORIGIN));
			if (!validairport) {
				airportmessage = airadminConfig.getMessage("um.fare.origin.not.active");
			}
			String via1 = props.getProperty(PARAM_VIA1);
			if (!via1.equals("")) {
				if (validairport) {
					validairport = validateAirports(lstAirport, props.getProperty(PARAM_VIA1));
					if (!validairport) {
						airportmessage = airadminConfig.getMessage("um.fare.via1.not.active");
					}
				}
			}
			String via2 = props.getProperty(PARAM_VIA2);
			if (!via2.equals("")) {
				if (validairport) {
					validairport = validateAirports(lstAirport, props.getProperty(PARAM_VIA2));
					if (!validairport) {
						airportmessage = airadminConfig.getMessage("um.fare.via2.not.active");
					}
				}
			}
			String via3 = props.getProperty(PARAM_VIA3);
			if (!via3.equals("")) {
				if (validairport) {
					validairport = validateAirports(lstAirport, props.getProperty(PARAM_VIA3));
					if (!validairport) {

						airportmessage = airadminConfig.getMessage("um.fare.via3.not.active");
					}
				}
			}
			String via4 = props.getProperty(PARAM_VIA4);
			if (!via4.equals("")) {
				if (validairport) {
					validairport = validateAirports(lstAirport, props.getProperty(PARAM_VIA4));
					if (!validairport) {
						airportmessage = airadminConfig.getMessage("um.fare.via4.not.active");
					}
				}
			}
			if (validairport) {
				validairport = validateAirports(lstAirport, props.getProperty(PARAM_DESTINATION));
				if (!validairport) {
					airportmessage = airadminConfig.getMessage("um.fare.destination.not.active");
				}
			}
		}
		return validairport;

	}

	/**
	 * Validate a Given Airport
	 * 
	 * @param airport
	 *            the Airport
	 * @return boolean true-valid false-invalid
	 */
	private static boolean validateAirports(List<Map<String, String>> lstAirport, String airport) {

		boolean valid = false;
		try {
			Iterator<Map<String, String>> ite = lstAirport.iterator();
			while (ite.hasNext()) {

				Map<String, String> keyValues = (Map<String, String>) ite.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				while (keyIterator.hasNext()) {
					String status = (String) keyValues.get(keyIterator.next());
					String port = (String) keyValues.get(keyIterator.next());

					if (port.equals(airport)) {
						if (status.equals(Airport.STATUS_ACTIVE)) {
							return true;
						} else {
							return false;
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("FareUploadRequestHandler.setFareRuleData() method is failed :" + e.getMessage());
		}
		return valid;
	}

	/**
	 * Save the Uploaded Fares
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param props
	 *            the Properties
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveUploadedFares(HttpServletRequest request, Properties props) throws Exception {
		String saveMsg = "var saveMsg = false;";
		FareBD fareBD = ModuleServiceLocator.getFareBD();
		// recordNumber++;
		try {

			fareBD.saveFare(getFareDto(request, props));

			isExceptionOccured = false;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));

			msgList.add(WebConstants.MSG_SUCCESS + "|" + airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS));

		} catch (ModuleException moduleException) {
			saveMsg = "var saveMsg=true;";
			setAttribInRequest(request, WebConstants.REQ_MSG, saveMsg);
			log.error("FaresRequestHandler.saveData() method is failed :" + moduleException.getMessageString());

			isExceptionOccured = true;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));

			msgList.add(WebConstants.MSG_ERROR + "|" + moduleException.getMessageString());

		} catch (Exception exception) {
			saveMsg = "var saveMsg = true;";
			isExceptionOccured = true;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));
			setAttribInRequest(request, WebConstants.REQ_MSG, saveMsg);
			saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			log.error("FaresRequestHandler.saveData() method is failed exception:" + exception.getMessage());
			if (exception instanceof RuntimeException) {
				throw exception;
			}

		}

	}

	private static FareDTO getFareDto(HttpServletRequest request, Properties props) {
		Fare fare = new Fare();
		FareDTO fareDto = new FareDTO();
		FareRule fareRule = new FareRule();
		fare.setUploaded(true);

		String ondCode = "";
		String fareID = null;
		String version = null;

		try {
			fareID = props.getProperty(PARAM_FARE_ID);
			if (!fareID.equals("")) {
				if (!props.getProperty(PARAM_ORIGIN).equals("")) {
					ondCode += props.getProperty(PARAM_HDN_OND);
				}
			} else {
				if (!props.getProperty(PARAM_ORIGIN).equals("") && !props.getProperty(PARAM_DESTINATION).equals("")) {
					ondCode += props.getProperty(PARAM_ORIGIN) + "/";

					String via1 = props.getProperty(PARAM_VIA1);
					if (!via1.equals("")) {
						ondCode += via1 + "/";
					}

					String via2 = props.getProperty(PARAM_VIA2);
					if (!via2.equals("")) {
						ondCode += via2 + "/";
					}
					String via3 = props.getProperty(PARAM_VIA3);
					if (!via3.equals("")) {
						ondCode += via3 + "/";
					}

					String via4 = props.getProperty(PARAM_VIA4);
					if (!via4.equals("")) {
						ondCode += via4 + "/";
					}
					ondCode += props.getProperty(PARAM_DESTINATION);
				}
			}
			fare.setOndCode(ondCode);
			fare = setCommonFields(props, fare, request);

			if (FareUtil.isShowFareDefInDepAirPCurr() && !props.getProperty(PARAM_SEL_LOCAL_CURRENCY).toString().equals("")) {
				fare = setFaresInDepAirportCurr(props, fare, request);
				fare.setLocalCurrencySelected(true);
			}

			if (!fareID.equals("")) {
				fare.setFareId(Integer.parseInt(fareID));
			}
			version = props.getProperty(FARE_VERSION);
			if (!"".equals(version)) {
				fare.setVersion(Long.parseLong(version));
			}

			// Attach to a master fare rule
			String rule = props.getProperty(PARAM_FC).toString();

			// Set Fare rule details
			fareRule.setFareRuleCode(rule);

			String returnFlag = props.getProperty(PARAM_CHK_RETURN);
			if (!returnFlag.equals("") && returnFlag.equals("Y")) {
				fareRule.setReturnFlag('Y');
			}

			fareDto.setFareRule(fareRule);

			fareDto.setFareRuleCode(rule);
			fareDto.setFare(fare);

		} catch (Exception exception) {
			log.error("FaresRequestHandler.getFareDto() method is failed exception:" + exception.getMessage());
		}
		return fareDto;
	}

	/**
	 * Saves the return fare
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param props
	 *            the Properties
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveReturnFare(HttpServletRequest request, Properties props) throws Exception {

		String saveMsg = "var saveMsg = false;";
		FareBD fareBD = ModuleServiceLocator.getFareBD();

		FareDTO fareReturnDto = new FareDTO();
		try {

			Fare newReturnFare = new Fare();
			String returnOnd = "";

			// ond code
			if (!props.getProperty(PARAM_ORIGIN).equals("") && !props.getProperty(PARAM_DESTINATION).equals("")) {
				returnOnd += props.getProperty(PARAM_DESTINATION) + "/";
				String via4 = props.getProperty(PARAM_VIA4);
				if (!via4.equals("")) {
					returnOnd += via4 + "/";
				}
				String via3 = props.getProperty(PARAM_VIA3);
				if (!via3.equals("")) {
					returnOnd += via3 + "/";
				}
				String via2 = props.getProperty(PARAM_VIA2);
				if (!via2.equals("")) {
					returnOnd += via2 + "/";
				}
				String via1 = props.getProperty(PARAM_VIA1);
				if (!via1.equals("")) {
					returnOnd += via1 + "/";
				}
				returnOnd += props.getProperty(PARAM_ORIGIN);
			}

			newReturnFare.setOndCode(returnOnd);
			setCommonFields(props, newReturnFare, request);

			String rule = props.getProperty(PARAM_FC).toString();
			if (rule != null && !rule.equals("")) {
				fareReturnDto.setFareRuleCode(rule);
			}

			fareReturnDto.setFare(newReturnFare);
			fareBD.saveFare(fareReturnDto);

			isExceptionOccured = false;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
		} catch (ModuleException moduleException) {

			saveMsg = "var saveMsg =true;";
			setAttribInRequest(request, WebConstants.REQ_MSG, saveMsg);
			log.error("FareUploadRequestHandler.SaveData() method is failed :" + moduleException.getMessageString());
		}
	}

	/**
	 * Sets the common fields to the both fare & return to avoid duplication
	 * 
	 * @param props
	 *            the properties file
	 * @param fare
	 *            the Fare
	 * @param request
	 *            the HttpServletRequest
	 * @return Fare the Fare
	 * @throws Exception
	 *             the Exception
	 */
	private static Fare setCommonFields(Properties props, Fare fare, HttpServletRequest request) throws Exception {

		String from = props.getProperty(PARAM_FROMDATE);
		String to = props.getProperty(PARAM_TODATE);
		String salesFrom = props.getProperty(PARAM_SALES_FROMDATE);
		String salesTo = props.getProperty(PARAM_SALESTODATE);
		String returnFrom = props.getProperty(PARAM_RTFROMDATE);
		String returnTo = props.getProperty(PARAM_RTTODATE);
		fare.setEffectiveFromDate(dateForamt.parse(from));
		fare.setEffectiveToDate(CalendarUtil.getEndTimeOfDate(dateForamt.parse(to)));
		fare.setSalesEffectiveFrom(dateForamt.parse(salesFrom));
		fare.setSalesEffectiveTo(CalendarUtil.getEndTimeOfDate(dateForamt.parse(salesTo)));
		if (returnFrom != null && !"".equals(returnFrom))
			fare.setReturnEffectiveFromDate(dateForamt.parse(returnFrom));
		if (returnTo != null && !"".equals(returnTo))
			fare.setReturnEffectiveToDate(CalendarUtil.getEndTimeOfDate(dateForamt.parse(returnTo)));

		if (!props.getProperty(PARAM_AED).toString().equals("")) {
			fare.setFareAmount(Double.parseDouble(props.getProperty(PARAM_AED)));
		}
		fare = composeChildFare(fare, request, props);
		fare.setBookingCode(props.getProperty(PARAM_BC));

		if (props.getProperty(PARAM_STATUS).equalsIgnoreCase(WebConstants.CHECKBOX_ON)) {
			fare.setStatus(Fare.STATUS_ACTIVE);
		} else {
			fare.setStatus(Fare.STATUS_INACTIVE);
		}
		return fare;
	}

	/**
	 * Sets the the Fare Amounts in departing airport currency
	 * 
	 * @param props
	 *            the properties file
	 * @param fare
	 *            the Fare
	 * @param request
	 *            the HttpServletRequest
	 * @return Fare the Fare
	 * @throws Exception
	 *             the Exception
	 */
	private static Fare setFaresInDepAirportCurr(Properties props, Fare fare, HttpServletRequest request) throws Exception {

		if (!props.getProperty(PARAM_BASE_IN_LOCAL).toString().equals("")) {
			fare.setFareAmountInLocal(new Double(props.getProperty(PARAM_BASE_IN_LOCAL)));
		}

		if (FareUtil.isChildVisible()) {

			if (!props.getProperty(PARAM_CH_FARE_IN_LOCAL).toString().equals("")) {
				fare.setChildFareInLocal(new Double(props.getProperty(PARAM_CH_FARE_IN_LOCAL)));
			}
			if (!props.getProperty(PARAM_INF_FARE_IN_LOCAL).toString().equals("")) {
				fare.setInfantFareInLocal(new Double(props.getProperty(PARAM_INF_FARE_IN_LOCAL)));
			}
			if (!props.getProperty(PARAM_CHL_SEL_IN_LOCAL).toString().equals("")) {
				fare.setChildFareType(props.getProperty(PARAM_CHL_SEL_IN_LOCAL));
			}
			if (!props.getProperty(PARAM_INF_SEL_IN_LOCAL).toString().equals("")) {
				fare.setInfantFareType(props.getProperty(PARAM_INF_SEL_IN_LOCAL));
			}

		}
		if (!props.getProperty(PARAM_ORIGIN).equals("")) {
			String[] orogin = new String[1];
			String strHtml[] = new String[1];
			String currCode = "";
			Collection<String[]> currCodeCol = new ArrayList<String[]>();
			orogin[0] = props.getProperty(PARAM_ORIGIN);

			currCodeCol = SelectListGenerator.getAirportCurrency(orogin);
			if (currCodeCol != null && currCodeCol.size() > 0) {
				Iterator<String[]> itr = currCodeCol.iterator();
				while (itr.hasNext()) {
					strHtml = (String[]) itr.next();
					currCode = strHtml[0];
				}

			}
			if (strHtml[0] != null && strHtml[0] != "") {
				currCode = strHtml[0];
			}
			fare.setLocalCurrencyCode(currCode);

		}

		return fare;
	}

	/**
	 * Compose the child & Infant fares
	 * 
	 * @param fare
	 *            the Fare
	 * @param request
	 *            the HttpServletRequest
	 * @param props
	 *            the Properties
	 * @return Fare the Fare
	 */
	private static Fare composeChildFare(Fare fare, HttpServletRequest request, Properties props) {
		if (FareUtil.isChildVisible()) {
			if (!props.getProperty(PARAM_AED_CHL).toString().equals("")) {
				fare.setChildFare(Double.parseDouble(props.getProperty(PARAM_AED_CHL)));
			}
			if (!props.getProperty(PARAM_AED_INF).toString().equals("")) {
				fare.setInfantFare(Double.parseDouble(props.getProperty(PARAM_AED_INF)));
			}
			if (!props.getProperty(PARAM_AED_CHL_SEL).toString().equals("")) {
				fare.setChildFareType(props.getProperty(PARAM_AED_CHL_SEL));
			}
			if (!props.getProperty(PARAM_AED_INF_SEL).toString().equals("")) {
				fare.setInfantFareType(props.getProperty(PARAM_AED_INF_SEL));
			}
		} else {
			fare.setChildFare(100);
			fare.setInfantFare(0);
			fare.setChildFareType(Fare.VALUE_PERCENTAGE_FLAG_P);
			fare.setInfantFareType(Fare.VALUE_PERCENTAGE_FLAG_P);
		}
		return fare;
	}

}