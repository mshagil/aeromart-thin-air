package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.FCCSegBCInventoryTO;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.FCCSegInventoryTO;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.FareTO;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.SeatMvSummaryTO;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCBCOnewayReturnPaxCountDTO;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.FareSummaryLightDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airreservation.api.dto.WLConfirmationRequestDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.SeatMvSummaryDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LoadFlightAllocationAction extends CommonAdminRequestResponseAwareCommonAction {

	private static Log log = LogFactory.getLog(LoadFlightAllocationAction.class);

	private static final String ADD_BC = "A";

	private static final String DELETE_BC = "D";

	private static final String EDIT_BC = "E";
	
	private static final int AGENTS_THRESHOLD = 900;

	private String mode;

	private String flightNo;

	private String flightId;

	private String olFlightId;

	private String flightSummary;

	private String flightRollForwardAuditSummary;

	private String seatFactorMin;

	private String seatFactorMax;

	private String aircraftModelNumber;

	private String ccCode;

	private String fromFromPage;

	private List<FCCSegInventoryTO> fccSegInventories = new ArrayList<FCCSegInventoryTO>();

	private List<String[]> flightCCCodes = new ArrayList<String[]>();

	private String fccSegInventoryJSON;

	private String blnSeatMap;

	private String blnSeatMapTmlt;

	private String blnShowMeal;

	private String blnShowBaggage;

	private String blnShowBaggageAllowanceRemarks;

	private String flightDefCap;

	private List<String[]> cabinClassList;

	private List<String[]> airportList;

	private List<String[]> flightStatusList;

	private String[] userCarrierCode;

	private String isTimeInZulu = "false";

	private String interLineDisplay;

	private Boolean hideLogicalCC = true;

	private Boolean hideWaitListing = true;

	private String availableSeatKilometers = "";

	private Boolean hideAllocScreenExtraDetails = false;

	private List<Map<String, String>> clientMessages;

	private HashMap<String, List<SeatMvSummaryTO>> agentWiseSeatMovementDataMap = new HashMap<String, List<SeatMvSummaryTO>>();

	private final AiradminConfig airadminConfig = new AiradminConfig();

	private Boolean showOverBookAlloc = AppSysParamsUtil.isShowBCWiseOverBookingInInventory();

	private boolean showNestedAvailableSeats = AppSysParamsUtil.showNestedAvailableSeats();
	
	private boolean showFCCSegBCInventoryAllocGrouping = AppSysParamsUtil.showFCCSegBCInventoryAllocGrouping();

	/**
	 * main Execute Method For Load Flight Allocation Action
	 * 
	 * @return String the Forward Action
	 */
	public String execute() {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setDefaultLoadSuccessMessage();

		try {
			if (mode != null && (mode.equals(WebConstants.ACTION_SAVE) || mode.equals("SAVEOL"))) {
				FCCSegInventoryUpdateStatusDTO updateStatus = saveData();

				if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_SUCCESS
						|| updateStatus.getStatus() == OperationStatusDTO.OPERATION_COMPLETED) {
					setDefaultSuccessMessage();
				} else if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_FAILURE) {
					this.msgType = WebConstants.MSG_ERROR;
					if (updateStatus.getMsg() != null && !updateStatus.getMsg().isEmpty()) {
						this.message = updateStatus.getMsg();
					} else {
						this.message = airadminConfig.getMessage(WebConstants.KEY_SAVE_FAIL);
					}
				}
			}
			displayDetails(request);
		} catch (Exception exception) {
			log.error("Exception in LoadFlightAllocationAction.execute()", exception);
			this.msgType = WebConstants.MSG_ERROR;
			this.message = airadminConfig.getMessage(WebConstants.KEY_LOAD_FAIL);
		}

		return forward;
	}

	private void setDefaultLoadSuccessMessage() {
		this.message = airadminConfig.getMessage(WebConstants.KEY_LOAD_SUCCESS);
		this.msgType = WebConstants.MSG_SUCCESS;
	}

	private void displayDetails(HttpServletRequest request) throws ModuleException {
		// Seat Inventory - Seat Allocation/Search Flights for Seat Allocation, related attributes
		setRequestAttributes();
		setDefaultCabinClass();

		try {
			cabinClassList = SelectListGenerator.createAllCabinClassList();
			clientMessages = getAllocationClientErrors();
			airportList = SelectListGenerator.createActiveAirportCodes();
			flightStatusList = SelectListGenerator.createFlightStatusList();
			hideLogicalCC = !AppSysParamsUtil.isLogicalCabinClassEnabled();
			hideWaitListing = !AppSysParamsUtil.isWaitListedReservationsEnabled();
			hideAllocScreenExtraDetails = AppSysParamsUtil.isHideAllocateScreenExtraDetails();
			if (AppSysParamsUtil.isDefaultTimeZulu()) {
				isTimeInZulu = "true";
			}

		} catch (ModuleException moduleException) {
			log.error("LoadFlightAllocationAction.displayDetails() method is failed :" + moduleException);
			setMessage(moduleException.getMessageString());
			setMsgType(WebConstants.MSG_ERROR);
		}

		setFlightCCDetails();
		setFccSegInventoryList();
		checkInterlineAirLineAvailability();
		setUserCarriers();
	}

	private void setFccSegInventoryList() throws ModuleException {
		Collection<FCCInventoryDTO> fccInventoryDTOs = new ArrayList<FCCInventoryDTO>();

		for (String[] ccCodeDesAndDefaultCapacity : flightCCCodes) {
			Integer intFlightId = null;
			Integer intOverlappingFlightId = null;
			if (mode != null && (mode.equals("SAVEOL"))) {
				intFlightId = (olFlightId == null || olFlightId.equals("") || olFlightId.equalsIgnoreCase("null"))
						? null
						: Integer.parseInt(olFlightId);
				intOverlappingFlightId = Integer.parseInt(flightId);
			} else {
				intFlightId = Integer.parseInt(flightId);
				intOverlappingFlightId = (olFlightId == null || olFlightId.equals("") || olFlightId.equalsIgnoreCase("null"))
						? null
						: Integer.parseInt(olFlightId);
			}
			Collection<FCCInventoryDTO> fccInvDTOs = ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(intFlightId,
					ccCodeDesAndDefaultCapacity[0], intOverlappingFlightId, false);
			if (fccInvDTOs != null) {
				fccInventoryDTOs.addAll(fccInvDTOs);
			}
		}

		Set<String> logicalCabinClasses = new HashSet<String>();
		Set<Integer> fareIds = new HashSet<Integer>();
		for (FCCInventoryDTO fccInventoryDTO : fccInventoryDTOs) {
			for (FCCSegInventoryDTO fccSegInventoryDTO : fccInventoryDTO.getFccSegInventoryDTOs()) {
				logicalCabinClasses.add(fccSegInventoryDTO.getLogicalCCCode());
				for (ExtendedFCCSegBCInventory exFCCSegBCInventory : fccSegInventoryDTO.getFccSegBCInventories()) {
					for (FareSummaryLightDTO fareSummaryLightDTO : exFCCSegBCInventory.getLinkedEffectiveFares()) {
						fareIds.add(fareSummaryLightDTO.getFareId());
					}
				}
			}
		}

		Map<String, List<String[]>> logicalCabinClassWiseBCDetails = new HashMap<String, List<String[]>>();
		if (!logicalCabinClasses.isEmpty()) {
			logicalCabinClassWiseBCDetails.putAll(getBookingCodesForLogicalCC(logicalCabinClasses));
		}

		Map<Integer, FareTO> fareDTOsMap = new HashMap<Integer, FareTO>();
		if (!fareIds.isEmpty()) {
			fareDTOsMap.putAll(getFareTOs(fareIds));
		}

		Map<String, FCCBCOnewayReturnPaxCountDTO> bcPaxCountMap = Collections.emptyMap();
		boolean showOwRetPaxCounts = AppSysParamsUtil.showFCCSegBCInventoryOneWayReturnPaxCounts();
		
		// iterate flight fcc Inventories
		for (FCCInventoryDTO fccInventoryDTO : fccInventoryDTOs) {
			// prepare fcc seg inventories
			Collection<FCCSegInventoryDTO> fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
			// iterate fcc segments in the flight
			for (FCCSegInventoryDTO fccSegInventoryDTO : fccSegInventoryDTOs) {

				FCCSegInventoryTO fccSegInventoryTO = new FCCSegInventoryTO();
				List<SeatMvSummaryTO> agentWiseSeatMvSummaryTOs = new ArrayList<SeatMvSummaryTO>();
				List<SeatMvSummaryTO> ownerWiseSeatMvSummaryTOs = new ArrayList<SeatMvSummaryTO>();
				List<FCCSegBCInventoryTO> fccSegBCInvs = new ArrayList<FCCSegBCInventoryTO>();
				int totalBCAllocation = 0;
				int totalFixedAllocation = 0;
				int totalSoldFixedSeats = 0;

				FCCSegInventory fccSegInventory = fccSegInventoryDTO.getFccSegInventory();

				fccSegInventoryTO.setFlightNo(fccSegInventoryDTO.getFlightNumber());
				fccSegInventoryTO.setSegmentCode(fccSegInventory.getSegmentCode());
				fccSegInventoryTO.setAllocatedSeats(fccSegInventory.getAllocatedSeats());
				fccSegInventoryTO.setOversellSeats(fccSegInventory.getOversellSeats());
				fccSegInventoryTO.setCurtailedSeats(fccSegInventory.getCurtailedSeats());
				fccSegInventoryTO.setWaitListSeats(fccSegInventory.getAllocatedWaitListSeats());
				fccSegInventoryTO.setWaitListedSeats(fccSegInventory.getWaitListedSeats());
				fccSegInventoryTO.setInfantAllocation(fccSegInventory.getInfantAllocation());
				fccSegInventoryTO.setSeatsSold(fccSegInventory.getSeatsSold() + "/" + fccSegInventory.getSoldInfantSeats());
				fccSegInventoryTO.setOnHoldSeats(fccSegInventory.getOnHoldSeats() + "/" + fccSegInventory.getOnholdInfantSeats());
				fccSegInventoryTO.setAvailableSeats(fccSegInventory.getAvailableSeats() + "/"
						+ fccSegInventory.getAvailableInfantSeats());
				fccSegInventoryTO.setFlightId(fccSegInventory.getFlightId());
				fccSegInventoryTO.setFccInvId(fccSegInventory.getFccInvId());
				fccSegInventoryTO.setFccsInvId(fccSegInventory.getFccsInvId());
				fccSegInventoryTO.setVersion(fccSegInventory.getVersion());
				fccSegInventoryTO.setFlightSegmentStatus(fccSegInventoryDTO.getFlightSegmentStatus());
				fccSegInventoryTO.setCcCode(fccInventoryDTO.getCabinClassCode());
				fccSegInventoryTO.setLogicalCCCode(fccSegInventory.getLogicalCCCode());
				fccSegInventoryTO.setLogicalCCRank(fccSegInventoryDTO.getLogicalCCRank());

				fccSegInventoryTO.setFlightSegId(fccSegInventory.getFlightSegId());

				FCCAgentsSeatMvDTO fccAgentSeatMvDTO = ModuleServiceLocator.getDataExtractionBD()
						.getFCCSeatMovementSummaryForLogicalCC(Integer.parseInt(flightId), fccSegInventory.getLogicalCCCode(),
								false);
				FCCAgentsSeatMvDTO fccOwnerSeatMvDTO = ModuleServiceLocator.getDataExtractionBD()
						.getOwnerAgentWiseFCCSeatMovementSummary(Integer.parseInt(flightId), fccSegInventory.getLogicalCCCode(),
								false);
				if (fccAgentSeatMvDTO != null) {
					Collection<SeatMvSummaryDTO> seatMvInfo = fccAgentSeatMvDTO
							.getSegAgentsSeatMvWithFCCAgentsSeatMv(new Integer(fccSegInventory.getFlightSegId()));

					for (SeatMvSummaryDTO seatMvSummaryDTO : seatMvInfo) {
						SeatMvSummaryTO seatMvSummaryTO = new SeatMvSummaryTO();

						seatMvSummaryTO.setAgentName(seatMvSummaryDTO.getAgentName());
						seatMvSummaryTO.setSoldSeats(seatMvSummaryDTO.getSoldSeats() + "/" + seatMvSummaryDTO.getSoldChildSeats()
								+ "/" + seatMvSummaryDTO.getSoldInfantSeats());
						seatMvSummaryTO.setOnholdSeats(seatMvSummaryDTO.getOnholdSeats() + "/"
								+ seatMvSummaryDTO.getOnHoldChildSeats() + "/" + seatMvSummaryDTO.getOnholdInfantSeats());
						seatMvSummaryTO.setCancelledSeats(seatMvSummaryDTO.getCancelledSeats() + "/"
								+ seatMvSummaryDTO.getCancelledChildSeats() + "/" + seatMvSummaryDTO.getCancelledInfantSeats());
						seatMvSummaryTO.setSoldSeatsSum(seatMvSummaryDTO.getSoldSeatsSum() + "/"
								+ seatMvSummaryDTO.getSoldChildSeatsSum() + "/" + seatMvSummaryDTO.getSoldInfantSeatsSum());
						seatMvSummaryTO.setOnholdSeatsSum(seatMvSummaryDTO.getOnholdSeatsSum() + "/"
								+ seatMvSummaryDTO.getOnHoldChildSeatsSum() + "/" + seatMvSummaryDTO.getOnholdInfantSeatsSum());
						seatMvSummaryTO.setCancelledSeatsSum(seatMvSummaryDTO.getCancelledSeatsSum() + "/"
								+ seatMvSummaryDTO.getCancelledChildSeatsSum() + "/"
								+ seatMvSummaryDTO.getCancelledInfantSeatsSum());

						agentWiseSeatMvSummaryTOs.add(seatMvSummaryTO);
					}

					if (agentWiseSeatMvSummaryTOs.size() > 0) {
						agentWiseSeatMovementDataMap.put(String.valueOf(fccSegInventory.getFccsInvId()) + "_AgentWise",
								agentWiseSeatMvSummaryTOs);
					}
				}

				if (fccOwnerSeatMvDTO != null) {
					Collection<SeatMvSummaryDTO> seatMvInfo = fccOwnerSeatMvDTO
							.getSegAgentsSeatMvWithFCCAgentsSeatMv(new Integer(fccSegInventory.getFlightSegId()));

					for (SeatMvSummaryDTO seatMvSummaryDTO : seatMvInfo) {
						SeatMvSummaryTO seatMvSummaryTO = new SeatMvSummaryTO();

						seatMvSummaryTO.setAgentName(seatMvSummaryDTO.getAgentName());
						seatMvSummaryTO.setSoldSeats(seatMvSummaryDTO.getSoldSeats() + "/" + seatMvSummaryDTO.getSoldChildSeats()
								+ "/" + seatMvSummaryDTO.getSoldInfantSeats());
						seatMvSummaryTO.setOnholdSeats(seatMvSummaryDTO.getOnholdSeats() + "/"
								+ seatMvSummaryDTO.getOnHoldChildSeats() + "/" + seatMvSummaryDTO.getOnholdInfantSeats());
						seatMvSummaryTO.setCancelledSeats(seatMvSummaryDTO.getCancelledSeats() + "/"
								+ seatMvSummaryDTO.getCancelledChildSeats() + "/" + seatMvSummaryDTO.getCancelledInfantSeats());
						seatMvSummaryTO.setSoldSeatsSum(seatMvSummaryDTO.getSoldSeatsSum() + "/"
								+ seatMvSummaryDTO.getSoldChildSeatsSum() + "/" + seatMvSummaryDTO.getSoldInfantSeatsSum());
						seatMvSummaryTO.setOnholdSeatsSum(seatMvSummaryDTO.getOnholdSeatsSum() + "/"
								+ seatMvSummaryDTO.getOnHoldChildSeatsSum() + "/" + seatMvSummaryDTO.getOnholdInfantSeatsSum());
						seatMvSummaryTO.setCancelledSeatsSum(seatMvSummaryDTO.getCancelledSeatsSum() + "/"
								+ seatMvSummaryDTO.getCancelledChildSeatsSum() + "/"
								+ seatMvSummaryDTO.getCancelledInfantSeatsSum());

						ownerWiseSeatMvSummaryTOs.add(seatMvSummaryTO);
					}
					if (ownerWiseSeatMvSummaryTOs.size() > 0) {
						agentWiseSeatMovementDataMap.put(String.valueOf(fccSegInventory.getFccsInvId()) + "_OwnerWise",
								ownerWiseSeatMvSummaryTOs);
					}
				}

				Collection<ExtendedFCCSegBCInventory> fccSegBCInventories = fccSegInventoryDTO.getFccSegBCInventories();
				
				if (showOwRetPaxCounts) {
					bcPaxCountMap = ModuleServiceLocator.getFlightInventoryBD()
							.getFCCBCOnewayReturnPaxCountDTOMap(fccSegInventoryDTO.getFccSegInventory().getFlightSegId());
				}
				int bcIndex = 1;

				log.info("Loading flight inventory Allocation for Flight Id : " + flightId + " in Seg Inventory Id : "
						+ fccInventoryDTO.getFccaInvId() + " which has " + fccSegBCInventories.size() + " Booking Classes");

				for (ExtendedFCCSegBCInventory exFCCSegBCInventory : fccSegBCInventories) {
					FCCSegBCInventoryTO fccSegBCInventoryTO = new FCCSegBCInventoryTO();
					List<FareTO> fareTOs = new ArrayList<FareTO>();

					FCCSegBCInventory fccSegBCInventory = exFCCSegBCInventory.getFccSegBCInventory();

					fccSegBCInventoryTO.setIndex(bcIndex);
					fccSegBCInventoryTO.setBookingCode(fccSegBCInventory.getBookingCode());
					
					fccSegBCInventoryTO.setStandardCode((exFCCSegBCInventory.isStandardCode()
							? BookingClass.STANDARD_CODE_Y
							: BookingClass.STANDARD_CODE_N));
					fccSegBCInventoryTO.setFixedFlag((exFCCSegBCInventory.isFixedFlag()
							? BookingClass.FIXED_FLAG_Y
							: BookingClass.FIXED_FLAG_N));
					fccSegBCInventoryTO.setPriorityFlag(fccSegBCInventory.getPriorityFlag());
					// Set allocated and fixed seat counts to the segment
					totalBCAllocation += fccSegBCInventory.getSeatsAllocated();
					if (exFCCSegBCInventory.isFixedFlag()) {
						totalFixedAllocation += fccSegBCInventory.getSeatsAllocated();
						totalSoldFixedSeats += fccSegBCInventory.getActualSeatsSold() + fccSegBCInventory.getActualSeatsOnHold();
					}
					fccSegBCInventoryTO.setSeatsAllocated(fccSegBCInventory.getSeatsAllocated());
					fccSegBCInventoryTO.setAllocatedWaitListSeats(fccSegBCInventory.getAllocatedWaitListSeats());
					fccSegBCInventoryTO.setActualSeatsSold(fccSegBCInventory.getActualSeatsSold());
					fccSegBCInventoryTO.setActualSeatsOnHold(fccSegBCInventory.getActualSeatsOnHold());
					fccSegBCInventoryTO.setActualWaitListedSeats(fccSegBCInventory.getWaitListedSeats());
					fccSegBCInventoryTO.setSeatsCancelled(fccSegBCInventory.getSeatsCancelled());
					fccSegBCInventoryTO.setSeatsAcquired(fccSegBCInventory.getSeatsAcquired());
					fccSegBCInventoryTO.setSeatsAvailable(fccSegBCInventory.getSeatsAvailable());
					fccSegBCInventoryTO.setStatusClosed((fccSegBCInventory.getStatus().endsWith(FCCSegBCInventory.Status.CLOSED)
							? true
							: false));

					// Need to add the fareTO list.
					List<FareSummaryLightDTO> effectiveFareList = (List<FareSummaryLightDTO>) exFCCSegBCInventory.getLinkedEffectiveFares();
					Collections.sort(effectiveFareList);
					
					Iterator<FareSummaryLightDTO> faresIt = effectiveFareList.iterator();
					String fareLink = "";
					for (int fareIndex = 0; faresIt.hasNext(); ++fareIndex) {
						FareSummaryLightDTO fareSummaryDTO = faresIt.next();
						int fareId = fareSummaryDTO.getFareId();
						String journeyType = "Y".equals(fareSummaryDTO.getReturnFlag()) ? "R" : "O";
						FareTO fareTO = fareDTOsMap.get(fareId);
						fareTOs.add(fareTO);

						if (faresIt.hasNext()) {
							fareLink += String.valueOf(fareSummaryDTO.getFareAmount()) +"(" + journeyType + "), ";
						} else {
							fareLink += fareSummaryDTO.getFareAmount() +"(" + journeyType + ")";
						}
					}
					fccSegBCInventoryTO.setFareLink(fareLink);
					fccSegBCInventoryTO.setFareTOs(fareTOs);

					if (showFCCSegBCInventoryAllocGrouping) {
						if (BookingClass.AllocationType.SEGMENT.equals(exFCCSegBCInventory.getAllocationType())
								|| BookingClass.AllocationType.CONNECTION
										.equals(exFCCSegBCInventory.getAllocationType())) {
							fccSegBCInventoryTO.setAllocationType("ONE-WAY");
						} else if (BookingClass.AllocationType.RETURN.equals(exFCCSegBCInventory.getAllocationType())) {
							fccSegBCInventoryTO.setAllocationType("RETURN");
						} else {
							fccSegBCInventoryTO.setAllocationType("COMBINED");
						}
						
						if (BookingClass.BookingClassType.STANDBY.equals(exFCCSegBCInventory.getBcType())){
							fccSegBCInventoryTO.setBcType("3. " + exFCCSegBCInventory.getBcType());
						} else {
							fccSegBCInventoryTO
									.setBcType((exFCCSegBCInventory.isStandardCode() ? "1. STANDARD" : "2. NON-STANDARD"));
						} 
					} else {
						fccSegBCInventoryTO.setAllocationType("");
						fccSegBCInventoryTO.setBcType("");
					}
					
					fccSegBCInventoryTO
							.setLinkedEffectiveAgents(((exFCCSegBCInventory.getLinkedEffectiveAgents() != null && exFCCSegBCInventory
									.getLinkedEffectiveAgents().size() > 0) ? Integer.toString(exFCCSegBCInventory
									.getLinkedEffectiveAgents().size()) : "0"));

					fccSegBCInventoryTO.setFccsbInvId(fccSegBCInventory.getFccsbInvId());
					fccSegBCInventoryTO.setStatusChangeAction(fccSegBCInventory.getStatusChangeAction());
					fccSegBCInventoryTO.setVersion(fccSegBCInventory.getVersion());
					fccSegBCInventoryTO.setStausEdited("N");
					fccSegBCInventoryTO.setEnableFareLink("");
					fccSegBCInventoryTO.setSeatsSold(fccSegBCInventory.getSeatsSold());
					fccSegBCInventoryTO.setOnHoldSeats(fccSegBCInventory.getOnHoldSeats());
					// nest rank
					if (exFCCSegBCInventory.getNestRank() != null) {
						fccSegBCInventoryTO.setNestRank(exFCCSegBCInventory.getNestRank());
					} else {
						fccSegBCInventoryTO.setNestRank(0);
					}
					fccSegBCInventoryTO.setSeatsSoldAquiredByNesting(fccSegBCInventory.getSeatsSoldAquiredByNesting());
					fccSegBCInventoryTO.setSeatsSoldNested(fccSegBCInventory.getSeatsSoldNested());
					fccSegBCInventoryTO.setSeatsOnHoldAquiredByNesting(fccSegBCInventory.getSeatsOnHoldAquiredByNesting());
					fccSegBCInventoryTO.setSeatsOnHoldNested(fccSegBCInventory.getSeatsOnHoldNested());
					if (showOwRetPaxCounts) {
						FCCBCOnewayReturnPaxCountDTO fccBCOnewayReturnPaxDetails = bcPaxCountMap
								.get(fccSegBCInventory.getBookingCode());
						if (fccBCOnewayReturnPaxDetails != null) {
							fccSegBCInventoryTO.setOneWayPaxCount(fccBCOnewayReturnPaxDetails.getOneWayPaxCount());
							fccSegBCInventoryTO
									.setOneWayConnectionPaxCount(fccBCOnewayReturnPaxDetails.getOneWayConnectionPaxCount());
							fccSegBCInventoryTO.setReturnPaxCount(fccBCOnewayReturnPaxDetails.getReturnPaxCount());
							fccSegBCInventoryTO
									.setReturnConnectionPaxCount(fccBCOnewayReturnPaxDetails.getReturnConnectionPaxCount());
							fccSegBCInventoryTO
									.setOneWayConnectionSegments(fccBCOnewayReturnPaxDetails.getOneWayConnectionSegments());
							fccSegBCInventoryTO
									.setReturnConnectionSegments(fccBCOnewayReturnPaxDetails.getReturnConnectionSegments());
						}
					}
					fccSegBCInventoryTO.setGdsId(exFCCSegBCInventory.getGdsId());
					fccSegBCInventoryTO.setExposedToGDS(isExposedToGDS(fccSegBCInventory.getFccsbInvId()));
					if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
						fccSegBCInventoryTO.setGroupId(exFCCSegBCInventory.getGroupId());
					}

					// update the nested total available nested seat count
					if (fccSegBCInventory.getSeatsAvailableNested() != null) {
						fccSegBCInventoryTO.setSeatsAvailableNested(fccSegBCInventory.getSeatsAvailableNested());
					} else {
						fccSegBCInventoryTO.setSeatsAvailableNested(fccSegBCInventory.getSeatsAvailable());
					}

					int overBookCount = Math.max(fccSegBCInventory.getSeatsAllocated() + fccSegBCInventory.getSeatsAcquired()
							- fccSegBCInventory.getSeatsCancelled(), 0)
							- (fccSegBCInventory.getOnHoldSeats() + fccSegBCInventory.getSeatsSold());
					fccSegBCInventoryTO.setOverBookCount(overBookCount < 0 ? (-1 * overBookCount) : 0);

					fccSegBCInvs.add(fccSegBCInventoryTO);
					bcIndex++;
				}

				if (showOverBookAlloc) { // segment status
					List<Integer> currentSegInvId = new ArrayList<Integer>();
					currentSegInvId.add(fccSegInventory.getFccsInvId());
					int totalSoldOhdCountInInterceptingSegments = ModuleServiceLocator.getFlightInventoryBD()
							.getMaximumEffectiveReservedSetsCountInInterceptingSegments(currentSegInvId);

					int overbookCount = (totalSoldOhdCountInInterceptingSegments + fccSegInventory.getSoldAndOnholdAdultSeats()
							- fccSegInventory.getOversellSeats() - fccSegInventory.getAllocatedSeats() + fccSegInventory
							.getCurtailedSeats());

					fccSegInventoryTO.setOverbookCount(overbookCount < 0 ? 0 : overbookCount);

					/*
					 * fccSegInventoryTO.setAvailableSeats(Math.max(fccSegInventory.getAvailableSeats(),
					 * totalFixedAllocation - totalSoldFixedSeats) + "/" + fccSegInventory.getAvailableInfantSeats());
					 */
				}
				fccSegInventoryTO.setFccSegBCInventories(fccSegBCInvs);
				fccSegInventoryTO.setTotalBCAllocation(totalBCAllocation);
				fccSegInventoryTO.setTotalFixedAllocation(totalFixedAllocation);
				fccSegInventoryTO.setBcCodeList(logicalCabinClassWiseBCDetails.get(fccSegInventoryDTO.getLogicalCCCode()));
				fccSegInventories.add(fccSegInventoryTO);
			}
		}
		if (fccSegInventories != null && fccSegInventories.size() > 1) {
			Collections.sort(fccSegInventories);
		}
	}

	/**
	 * saves the Inventory Data
	 * 
	 * @return FCCSegInventoryUpdateStatusDTO the response data
	 * @throws Exception
	 *             the Exception
	 */
	private FCCSegInventoryUpdateStatusDTO saveData() throws Exception {

		FCCSegInventoryUpdateStatusDTO updateStatus = null;
		FCCSegInventoryUpdateDTO fCCSegInventoryUpdateDTO = null;
		// Variable to set the newly added inventories
		Set<FCCSegBCInventory> setAddedInventories = null;
		// Variable to set the edited inventories
		Set<FCCSegBCInventory> setEditedInventories = null;
		// Variable to set the deleted inventories
		LinkedHashSet<Integer> setDeletedInventories = null;
		LinkedHashSet<String> setDeletedBookingCodes = null;
		JSONParser parser = new JSONParser();
		JSONArray jsonSegInvArray = (JSONArray) parser.parse(fccSegInventoryJSON);

		if (jsonSegInvArray.size() > 0) {
			for (int i = 0; i < jsonSegInvArray.size(); i++) {
				JSONObject fccSegInventoryTOObj = (JSONObject) jsonSegInvArray.get(i);
				int flightId = Integer.valueOf(fccSegInventoryTOObj.get("FlightId").toString());
				fCCSegInventoryUpdateDTO = new FCCSegInventoryUpdateDTO();

				fCCSegInventoryUpdateDTO.setFccsInventoryId(Integer.valueOf(fccSegInventoryTOObj.get("FccsInvId").toString()));
				fCCSegInventoryUpdateDTO.setFlightId(flightId);
				fCCSegInventoryUpdateDTO.setSegmentCode(fccSegInventoryTOObj.get("SegmentCode").toString());
				fCCSegInventoryUpdateDTO.setLogicalCCCode(fccSegInventoryTOObj.get("LogicalCCCode").toString());
				fCCSegInventoryUpdateDTO.setOversellSeats(Integer.valueOf(fccSegInventoryTOObj.get("OversellSeats").toString()));
				fCCSegInventoryUpdateDTO
						.setCurtailedSeats(Integer.valueOf(fccSegInventoryTOObj.get("CurtailedSeats").toString()));
				if (fccSegInventoryTOObj.get("WaitListSeats") != null) {
					fCCSegInventoryUpdateDTO.setWaitListSeats(Integer.valueOf(fccSegInventoryTOObj.get("WaitListSeats")
							.toString()));
				} else {
					fCCSegInventoryUpdateDTO.setWaitListSeats(0);
				}
				fCCSegInventoryUpdateDTO.setInfantAllocation(Integer.valueOf(fccSegInventoryTOObj.get("InfantAllocation")
						.toString()));
				fCCSegInventoryUpdateDTO.setVersion(Integer.valueOf(fccSegInventoryTOObj.get("Version").toString()));

				JSONArray jsonSegBCInvArray = (JSONArray) fccSegInventoryTOObj.get("fccSegBCInventories");
				if (jsonSegBCInvArray.size() > 0) {

					for (int j = 0; j < jsonSegBCInvArray.size(); j++) {
						JSONObject fccSegBCInventoryTOObj = (JSONObject) jsonSegBCInvArray.get(j);
						if (fccSegBCInventoryTOObj.get("Action") != null
								&& fccSegBCInventoryTOObj.get("Action").toString().equalsIgnoreCase(DELETE_BC)
								&& fccSegBCInventoryTOObj.get("FccsbInvId") != null) {
							// bc inv deleted
							if (setDeletedInventories == null && setDeletedBookingCodes == null) {
								setDeletedInventories = new LinkedHashSet<Integer>();
								fCCSegInventoryUpdateDTO.setDeletedFCCSegBCInventories(setDeletedInventories);
								setDeletedBookingCodes = new LinkedHashSet<String>();
								fCCSegInventoryUpdateDTO.setDeletedFCCSegBookingCodes(setDeletedBookingCodes);
							}
							setDeletedInventories.add(Integer.valueOf(fccSegBCInventoryTOObj.get("FccsbInvId").toString()));
							setDeletedBookingCodes.add(fccSegBCInventoryTOObj.get("BookingCode").toString());
						} else if (fccSegBCInventoryTOObj.get("Action") != null
								&& fccSegBCInventoryTOObj.get("Action").toString().equalsIgnoreCase(ADD_BC)) {
							// bc added
							if (setAddedInventories == null) {
								setAddedInventories = new HashSet<FCCSegBCInventory>();
								fCCSegInventoryUpdateDTO.setAddedFCCSegBCInventories(setAddedInventories);
							}
							FCCSegBCInventory bcInv = new FCCSegBCInventory();
							bcInv.setfccsInvId(Integer.valueOf(fccSegInventoryTOObj.get("FccsInvId").toString()));
							bcInv.setFlightId(flightId);
							bcInv.setSegmentCode(fccSegInventoryTOObj.get("SegmentCode").toString());
							bcInv.setLogicalCCCode(fccSegInventoryTOObj.get("LogicalCCCode").toString());
							bcInv.setBookingCode(fccSegBCInventoryTOObj.get("BookingCode").toString());
							bcInv.setPriorityFlag(Boolean.valueOf(fccSegBCInventoryTOObj.get("PriorityFlag").toString()));
							bcInv.setSeatsAllocated(Integer.valueOf(fccSegBCInventoryTOObj.get("SeatsAllocated").toString()));
							if (fccSegBCInventoryTOObj.get("AllocatedWaitListSeats") != null) {
								bcInv.setAllocatedWaitListSeats(Integer.valueOf(fccSegBCInventoryTOObj.get(
										"AllocatedWaitListSeats").toString()));
							} else {
								bcInv.setAllocatedWaitListSeats(0);
							}
							if (Boolean.valueOf(fccSegBCInventoryTOObj.get("StatusClosed").toString())) {
								bcInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.MANUAL);
								bcInv.setStatus(FCCSegBCInventory.Status.CLOSED);
							} else {
								bcInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);
								bcInv.setStatus(FCCSegBCInventory.Status.OPEN);
							}

							bcInv.setSeatsAcquired(Integer.valueOf(fccSegBCInventoryTOObj.get("SeatsAcquired").toString()));
							bcInv.setVersion(Integer.valueOf(fccSegBCInventoryTOObj.get("Version").toString()));
							if (fccSegBCInventoryTOObj.get("soldSeats") != null) {
								bcInv.setSeatsSold(Integer.valueOf(fccSegBCInventoryTOObj.get("soldSeats").toString()));
							}
							setAddedInventories.add(bcInv);

						} else if (fccSegBCInventoryTOObj.get("Action") != null
								&& fccSegBCInventoryTOObj.get("Action").toString().equalsIgnoreCase(EDIT_BC)) {
							// bc edited
							if (setEditedInventories == null) {
								setEditedInventories = new HashSet<FCCSegBCInventory>();
								fCCSegInventoryUpdateDTO.setUpdatedFCCSegBCInventories(setEditedInventories);
							}
							FCCSegBCInventory bcInv = new FCCSegBCInventory();
							bcInv.setFccsbInvId(Integer.valueOf(fccSegBCInventoryTOObj.get("FccsbInvId").toString()));
							bcInv.setfccsInvId(Integer.valueOf(fccSegInventoryTOObj.get("FccsInvId").toString()));
							bcInv.setFlightId(flightId);
							bcInv.setSegmentCode(fccSegInventoryTOObj.get("SegmentCode").toString());
							bcInv.setLogicalCCCode(fccSegInventoryTOObj.get("LogicalCCCode").toString());
							bcInv.setBookingCode(fccSegBCInventoryTOObj.get("BookingCode").toString());
							bcInv.setPriorityFlag(Boolean.valueOf(fccSegBCInventoryTOObj.get("PriorityFlag").toString()));
							bcInv.setSeatsAllocated(Integer.valueOf(fccSegBCInventoryTOObj.get("SeatsAllocated").toString()));
							if (fccSegBCInventoryTOObj.get("AllocatedWaitListSeats") != null) {
								bcInv.setAllocatedWaitListSeats(Integer.valueOf(fccSegBCInventoryTOObj.get(
										"AllocatedWaitListSeats").toString()));
							} else {
								bcInv.setAllocatedWaitListSeats(0);
							}
							bcInv.setStatus(Boolean.valueOf(fccSegBCInventoryTOObj.get("StatusClosed").toString())
									? FCCSegBCInventory.Status.CLOSED
									: FCCSegBCInventory.Status.OPEN);

							bcInv.setStatusChangeAction(fccSegBCInventoryTOObj.get("StatusChangeAction").toString());
							bcInv.setSeatsAcquired(Integer.valueOf(fccSegBCInventoryTOObj.get("SeatsAcquired").toString()));
							bcInv.setVersion(Integer.valueOf(fccSegBCInventoryTOObj.get("Version").toString()));
							bcInv.setSeatsSold(Integer.valueOf(fccSegBCInventoryTOObj.get("soldSeats").toString()));
							// bcInv.setSeatsSold(Integer.valueOf(fccSegBCInventoryTOObj.get("onHoldSeats").toString()));
							setEditedInventories.add(bcInv);
						}
					}
				}

				updateStatus = ModuleServiceLocator.getFlightInventoryBD().updateFCCSegInventory(fCCSegInventoryUpdateDTO);

				if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_SUCCESS) {
					// update flight status if necessary
					int bcInvCount = ModuleServiceLocator.getFlightInventoryBD().getFCCSegBCInventoriesCount(flightId);
					Collection<Integer> flightIds = new ArrayList<Integer>();
					flightIds.add(new Integer(flightId));
					if (bcInvCount > 0) {// check status update is really required
						ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(flightIds, FlightStatusEnum.ACTIVE);
					} else {
						ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(flightIds, FlightStatusEnum.CREATED);
					}
					// Checking for wait listed reservations, whether is there any possibility to confirm them
					if (AppSysParamsUtil.isWaitListedReservationsEnabled()) {
						FCCSegInventory fltSegInventory = ModuleServiceLocator.getFlightInventoryBD().getFlightSegmentsInventory(
								fCCSegInventoryUpdateDTO.getFccsInventoryId());
						if (fltSegInventory.getWaitListedSeats() > 0) {
							List<WLConfirmationRequestDTO> wlRequests = new ArrayList<WLConfirmationRequestDTO>();
							Collection<FCCSegBCInventory> bcInvs = ModuleServiceLocator.getFlightInventoryBD()
									.getFCCSegBCInventories(fltSegInventory.getFccsInvId());
							WLConfirmationRequestDTO wlDTO = new WLConfirmationRequestDTO();
							wlDTO.setLogicalCabinCalss(fltSegInventory.getLogicalCCCode());
							wlDTO.setSegmentAvailabileCount(fltSegInventory.getAvailableSeats() - fltSegInventory.getFixedSeats());
							wlDTO.setSegmentAvailableInfantCount(fltSegInventory.getAvailableInfantSeats());
							wlDTO.setFlightSegId(fltSegInventory.getFlightSegId());
							wlDTO.setFlightId(fltSegInventory.getFlightId());
							wlDTO.setSegmentCode(fltSegInventory.getSegmentCode());
							Map<String, Integer> bcAvailableCount = new HashMap<String, Integer>();
							for (FCCSegBCInventory bcInv : bcInvs) {
								bcAvailableCount.put(bcInv.getBookingCode(), bcInv.getSeatsAvailable());
							}
							wlDTO.setSegmentBcAvailableCount(bcAvailableCount);
							wlRequests.add(wlDTO);
							ModuleServiceLocator.getReservationBD().confirmWaitListedReservations(wlRequests);
						}
					}
				} else {
					return updateStatus;
				}

				setAddedInventories = null;
				setEditedInventories = null;
				setDeletedInventories = null;
			}
		} else {
			updateStatus = new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "No changes for update",
					null, FCCSegInventoryUpdateStatusDTO.UNIDENTIFIED_FAILURE, null, null);
		}
		return updateStatus;
	}

	private Map<Integer, FareTO> getFareTOs(Collection<Integer> fareIds) throws ModuleException {
		Collection<FareDTO> fareDTOs = ModuleServiceLocator.getFareBD().getFares(fareIds);
		Map<Integer, FareTO> fareTosMap = new HashMap<Integer, FareTO>();
		for (FareDTO fareDTO : fareDTOs) {
			fareTosMap.put(fareDTO.getFare().getFareId(), extractFareToFromFareDTO(fareDTO));
		}
		return fareTosMap;
	}

	private FareTO extractFareToFromFareDTO(FareDTO fareDTO) {
		FareTO fareTO = new FareTO();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
		FareRule fareRule = fareDTO.getFareRule();
		Fare fare = fareDTO.getFare();

		String strAllVisibleChannelNames = "";
		String strAllAgentCodes = "";

		fareTO.setFareRuleId(fareRule.getFareRuleId());
		fareTO.setEffectiveFromDate(formatter.format(fare.getEffectiveFromDate()));
		fareTO.setEffectiveToDate(formatter.format(fare.getEffectiveToDate()));
		fareTO.setFareBasisCode(fareRule.getFareBasisCode());
		fareTO.setFareRuleDescription(fareRule.getFareRuleDescription() != null ? fareRule.getFareRuleDescription() : " ");
		fareTO.setPaxLevelRefundability(getPaxLevelRefundability(fareRule.getPaxDetails()));
		fareTO.setReturnFlag(String.valueOf(fareRule.getReturnFlag()));

		int advbkTime = (fareRule.getAdvanceBookingDays() == null) ? 0 : fareRule.getAdvanceBookingDays();
		String[] advBKTimeDDHHMM = AiradminUtils.getAdvBkDDHHMM(advbkTime);
		fareTO.setAdvBookingTime(advBKTimeDDHHMM[0] + ":" + advBKTimeDDHHMM[1] + ":" + advBKTimeDDHHMM[2]);
		fareTO.setFareId(fare.getFareId());
		fareTO.setFareAmount(fare.getFareAmount());
		fareTO.setFareRuleCode(fareRule.getFareRuleCode() != null ? fareRule.getFareRuleCode() : " ");

		Set<String> agentCodes = fareRule.getVisibileAgentCodes();
		int agentCount = 0;
		if (agentCodes != null) {
			for (String agentCode : agentCodes) {
				if (!strAllAgentCodes.equals("")) {
					strAllAgentCodes += ", ";
				}
				strAllAgentCodes += agentCode;
				agentCount++;
				if(agentCount == AGENTS_THRESHOLD) {
					strAllAgentCodes += " ...";
					break;
				}
				
			}
		}

		Set<Integer> visibleChannels = fareRule.getVisibleChannelIds();
		if (visibleChannels != null) {
			for (Integer visibleChannel : visibleChannels) {
				String strVisibleChannelName = SalesChannelsUtil.getSalesChannelName(visibleChannel.intValue());
				if (strVisibleChannelName != null) {
					if (!strAllVisibleChannelNames.equals("")) {
						strAllVisibleChannelNames += ", ";
					}
					strAllVisibleChannelNames += strVisibleChannelName;
				}
			}
		}

		fareTO.setAgentCodes(strAllAgentCodes);
		fareTO.setVisibleChannelNames(strAllVisibleChannelNames);

		return fareTO;
	}

	private String getPaxLevelRefundability(Collection<FareRulePax> paxDetails) {

		String adultRefundability = "";
		String childRefundability = "";
		String infantRefundability = "";
		String refundability = "";
		String seperator = "/";

		if (paxDetails != null) {
			for (FareRulePax fareRulePax : paxDetails) {
				if (fareRulePax.getPaxType().equalsIgnoreCase(PaxTypeTO.ADULT)) {
					if (fareRulePax.getRefundableFares()) {
						adultRefundability = "Yes";
					} else {
						adultRefundability = "No";
					}
				}

				if (fareRulePax.getPaxType().equalsIgnoreCase(PaxTypeTO.CHILD)) {
					if (fareRulePax.getRefundableFares()) {
						childRefundability = "Yes";
					} else {
						childRefundability = "No";
					}
				}
				if (fareRulePax.getPaxType().equalsIgnoreCase(PaxTypeTO.INFANT)) {
					if (fareRulePax.getRefundableFares()) {
						infantRefundability = "Yes";
					} else {
						infantRefundability = "No";
					}
				}
			}
		}
		refundability = (adultRefundability + seperator + childRefundability + seperator + infantRefundability);
		return refundability;
	}

	private void setRequestAttributes() throws ModuleException {

		Collection<String[]> colTemplates = SelectListGenerator.createChargesTemplateIDs(aircraftModelNumber);

		boolean blnSeatMap = false;
		boolean blnSeatMapTmlt = false;
		boolean blnShowMeal = false;
		boolean blnShowBaggage = false;
		boolean blnShowBaggageAllowance = false;

		String strSeatMap = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP);
		if (strSeatMap != null && strSeatMap.trim().equals("Y")) {
			blnSeatMap = true;

			if (colTemplates != null && colTemplates.size() > 0) {
				blnSeatMapTmlt = true;
			}
		}
		String strMeal = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL);
		if (strMeal != null && strMeal.trim().equals("Y")) {
			blnShowMeal = true;
		}

		String strBaggage = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggage != null && strBaggage.trim().equals("Y")) {
			// enabled in application parameter
			log.debug("show baggage enabled in app-param");
			Collection<String[]> colBaggageTemplates = SelectListGenerator.createBaggageChargesTemplateIDs(aircraftModelNumber);
			if (colBaggageTemplates != null && colBaggageTemplates.size() > 0) {
				// active baggage templates available
				log.debug("baggage template available-showbaggage");
				blnShowBaggage = true;
			}
		}

		String strBaggageAllowanceRemarks = ModuleServiceLocator.getGlobalConfig().getBizParam(
				SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS);

		if (strBaggageAllowanceRemarks != null && strBaggageAllowanceRemarks.trim().equals("Y")) {
			blnShowBaggageAllowance = true;
		}

		this.blnSeatMap = String.valueOf(blnSeatMap);
		this.blnSeatMapTmlt = String.valueOf(blnSeatMapTmlt);
		this.blnShowMeal = String.valueOf(blnShowMeal);
		this.blnShowBaggage = String.valueOf(blnShowBaggage);
		this.blnShowBaggageAllowanceRemarks = String.valueOf(blnShowBaggageAllowance);

		if (seatFactorMin == null || seatFactorMin.equals("")) {
			seatFactorMin = "0";
		}
		if (seatFactorMax == null || seatFactorMax.equals("")) {
			seatFactorMax = "100";
		}

		if (flightSummary != null && !flightSummary.equals("")) {
			if (getFlightSummary().trim().length() != (getFlightSummary().trim().lastIndexOf("@") + 1)) {
				flightSummary = getFlightSummary() + "@";
			}
			flightSummary = flightSummary.substring(0, (flightSummary.length() - 1));
		}

		if (flightRollForwardAuditSummary != null && !flightRollForwardAuditSummary.equals("")) {
			if (flightRollForwardAuditSummary.trim().length() != (flightRollForwardAuditSummary.trim().lastIndexOf("@") + 1)) {
				flightRollForwardAuditSummary = flightRollForwardAuditSummary + "@";
			}
			flightRollForwardAuditSummary = flightRollForwardAuditSummary.substring(0,
					(flightRollForwardAuditSummary.length() - 1));
		}
	}

	/**
	 * Method to set Carrier codes
	 * 
	 * @throws ModuleException
	 */
	private void setUserCarriers() throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		String[] carriers = user.getCarriers().toArray(new String[user.getCarriers().size()]);

		userCarrierCode = carriers;
	}

	private void setFlightCCDetails() throws ModuleException {
		if (aircraftModelNumber != null && !aircraftModelNumber.trim().equals("")) {
			AircraftModel aircraftModel = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(aircraftModelNumber);
			if (aircraftModel != null) {
				Set<AircraftCabinCapacity> ccCapacities = aircraftModel.getAircraftCabinCapacitys();
				if (ccCapacities != null) {
					for (String[] ccCodeAndDes : cabinClassList) {
						for (AircraftCabinCapacity ccCapacity : ccCapacities) {
							if (ccCapacity.getCabinClassCode().equalsIgnoreCase(ccCodeAndDes[0])) {
								String[] ccCodeDesAndDefaultCapacity = new String[3];
								String flightDefCap = ccCapacity.getSeatCapacity() + "/" + ccCapacity.getInfantCapacity();

								ccCodeDesAndDefaultCapacity[0] = ccCodeAndDes[0];
								ccCodeDesAndDefaultCapacity[1] = ccCodeAndDes[1];
								ccCodeDesAndDefaultCapacity[2] = flightDefCap;

								flightCCCodes.add(ccCodeDesAndDefaultCapacity);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * If display TRUE operating carrier would see the summary of marketing carrier seat allocation of certain operating
	 * carrier flight.
	 */
	private void checkInterlineAirLineAvailability() throws ModuleException {
		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
		/* Retrieve all the interline airlines */
		Collection<String> interLines = globalConfig.getActiveInterlinedCarriersMap().keySet();

		int fltId = 0;
		/* Get the system carrier codes */
		Collection<String> systemCarriers = globalConfig.getActiveSysCarriersMap().keySet();
		boolean display = false;

		if (flightId != null) {
			fltId = Integer.parseInt(flightId);
		}
		Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(fltId);

		if (flight != null && !systemCarriers.contains(AppSysParamsUtil.extractCarrierCode(flight.getFlightNumber()))
				&& interLines.contains(AppSysParamsUtil.extractCarrierCode(flight.getFlightNumber()))) {
			display = true;
		}

		if (flight != null) {
			availableSeatKilometers = String.valueOf(flight.getAvailableSeatKilometers().intValue() / 1000);
		}
		interLineDisplay = String.valueOf(display);
	}

	/**
	 * Prepare bookingCodes list
	 * 
	 * @param logicalCCCode
	 * @throws ModuleException
	 */
	private Map<String, List<String[]>> getBookingCodesForLogicalCC(Set<String> logicalCCCodes) throws ModuleException {

		Map<String, List<String[]>> logicalCabinClassWiseBCDetails = new HashMap<String, List<String[]>>();
		Collection<Object[]> bookingCodes = ModuleServiceLocator.getBookingClassBD().getBookingCodesWithFlagsForLogicalCC(
				logicalCCCodes);
		if (bookingCodes != null) {
			for (Object[] bcWithFlags : bookingCodes) {
				String logicalCabinClass = bcWithFlags[5].toString();
				if (logicalCabinClassWiseBCDetails.get(logicalCabinClass) == null) {
					logicalCabinClassWiseBCDetails.put(logicalCabinClass, new ArrayList<String[]>());
				}
				// Same BCs use for both GDSs and normal bookings
				// Haider 15Sep08 don't add the booking class to the list if it is linked to GDS
				// if (bcWithFlags[5] != null) {
				// continue;
				// }
				String stdFlag = (String) bcWithFlags[1];
				String fixedFlag = (String) bcWithFlags[2];
				String bcType = BookingClassUtil.getBcTypeThreeLetterCode(((String) bcWithFlags[3]));
				String allocType = (String) bcWithFlags[4];
				String allocTypeTxt = "";
				
				if (BookingClass.AllocationType.SEGMENT.equals(bcWithFlags[4]) || BookingClass.AllocationType.CONNECTION.equals(bcWithFlags[4])) {
					allocTypeTxt = "ONE-WAY";
				} else if (BookingClass.AllocationType.RETURN.equals(bcWithFlags[4])) {
					allocTypeTxt = "RETURN";
				} else {
					allocTypeTxt = "COMBINED";
				}

				String suffix = " ";
				if (stdFlag.equals("Y")) {
					suffix += "[STD,";
				} else if (fixedFlag.equals("Y")) {
					suffix += "[FXD,";
				} else {
					suffix += "[NSD,";
				}
				if (allocType.equals(BookingClass.AllocationType.SEGMENT)) {
					suffix += "SEG,";
				} else if (allocType.equals(BookingClass.AllocationType.CONNECTION)) {
					suffix += "CON,";
				} else if (allocType.equals(BookingClass.AllocationType.RETURN)) {
					suffix += "RET,";
				} else {
					suffix += "COM,";
				}
				suffix += bcType + "]";

				String bcKey = bcWithFlags[0] + "," + bcWithFlags[1] + "," + bcWithFlags[2] + "," + bcWithFlags[3] + "," + allocTypeTxt;
				String bcValue = bcWithFlags[0] + suffix;

				String[] bcInfo = { bcKey, bcValue };
				logicalCabinClassWiseBCDetails.get(logicalCabinClass).add(bcInfo);
			}
		}
		return logicalCabinClassWiseBCDetails;
	}

	private boolean isExposedToGDS(Integer fccSegBCinventoryID) {
		FlightInventoryBD flightInventoryBD = ModuleServiceLocator.getFlightInventoryBD();
		return flightInventoryBD.isExposedToGDS(fccSegBCinventoryID);
	}

	/**
	 * Sets the Default Cabin Classes to the Request
	 * 
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private void setDefaultCabinClass() throws ModuleException {
		if (ccCode == null || (ccCode != null && ccCode.trim().equals(""))) {
			ccCode = AppSysParamsUtil.getDefaultCOS();
		}
	}

	/**
	 * Create the Client Validations for Seat Allocation Page
	 * 
	 * @return List<String[]> of client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static List<Map<String, String>> getAllocationClientErrors() throws ModuleException {

		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("um.ManageInventory.form.segmentcurtailed.Invalid", "curtaildInvaid");
		moduleErrs.setProperty("um.ManageInventory.form.segmentoversell.Invalid", "oversellInvaid");
		moduleErrs.setProperty("um.ManageInventory.form.bcallocation.Less", "bcLess");
		moduleErrs.setProperty("um.ManageInventory.form.infantallocation.Less", "infantLess");
		moduleErrs.setProperty("um.ManageInventory.form.acquired.null", "acquirednull");
		moduleErrs.setProperty("um.ManageInventory.form.acquired.zro", "acquiredzero");
		moduleErrs.setProperty("um.searchinventory.form.fromdate.required", "departureDateFromRqrd");
		moduleErrs.setProperty("um.searchinventory.form.todate.required", "departureDateToRqrd");
		moduleErrs.setProperty("um.searchinventory.form.fromdatelessthancurrentdate.voilated", "departureDateFromBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.depdategreaterthanarrivaldate.voilated", "departureDateToBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.departureandarrivalsame.voilated", "departureAndArrivalBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.viapoints.invalid", "viapointsinvalid");
		moduleErrs.setProperty("um.flightschedule.search.invalidcarrier", "invalidcarrier");
		moduleErrs.setProperty("um.searchinventory.form.flight.null", "carriernull");

		List<Map<String, String>> errorList = JavascriptGenerator.createClientErrorList(moduleErrs);

		return errorList;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getFlightSummary() {
		return flightSummary;
	}

	public void setFlightSummary(String flightSummary) {
		this.flightSummary = flightSummary;
	}

	public String getFlightRollForwardAuditSummary() {
		return flightRollForwardAuditSummary;
	}

	public void setFlightRollForwardAuditSummary(String flightRollForwardAuditSummary) {
		this.flightRollForwardAuditSummary = flightRollForwardAuditSummary;
	}

	public String getSeatFactorMin() {
		return seatFactorMin;
	}

	public void setSeatFactorMin(String seatFactorMin) {
		this.seatFactorMin = seatFactorMin;
	}

	public String getSeatFactorMax() {
		return seatFactorMax;
	}

	public void setSeatFactorMax(String seatFactorMax) {
		this.seatFactorMax = seatFactorMax;
	}

	public String getAircraftModelNumber() {
		return aircraftModelNumber;
	}

	public void setAircraftModelNumber(String aircraftModelNumber) {
		this.aircraftModelNumber = aircraftModelNumber;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getOlFlightId() {
		return olFlightId;
	}

	public void setOlFlightId(String olFlightId) {
		this.olFlightId = olFlightId;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getFromFromPage() {
		return fromFromPage;
	}

	public void setFromFromPage(String fromFromPage) {
		this.fromFromPage = fromFromPage;
	}

	public List<FCCSegInventoryTO> getFccSegInventories() {
		return fccSegInventories;
	}

	public void setFccSegInventories(List<FCCSegInventoryTO> fccSegInventories) {
		this.fccSegInventories = fccSegInventories;
	}

	public List<String[]> getFlightCCCodes() {
		return flightCCCodes;
	}

	public void setFlightCCCodes(List<String[]> flightCCCodes) {
		this.flightCCCodes = flightCCCodes;
	}

	public String getFccSegInventoryJSON() {
		return fccSegInventoryJSON;
	}

	public void setFccSegInventoryJSON(String fccSegInventoryJSON) {
		this.fccSegInventoryJSON = fccSegInventoryJSON;
	}

	public String getBlnSeatMap() {
		return blnSeatMap;
	}

	public void setBlnSeatMap(String blnSeatMap) {
		this.blnSeatMap = blnSeatMap;
	}

	public String getBlnSeatMapTmlt() {
		return blnSeatMapTmlt;
	}

	public void setBlnSeatMapTmlt(String blnSeatMapTmlt) {
		this.blnSeatMapTmlt = blnSeatMapTmlt;
	}

	public String getBlnShowMeal() {
		return blnShowMeal;
	}

	public void setBlnShowMeal(String blnShowMeal) {
		this.blnShowMeal = blnShowMeal;
	}

	public String getBlnShowBaggage() {
		return blnShowBaggage;
	}

	public void setBlnShowBaggage(String blnShowBaggage) {
		this.blnShowBaggage = blnShowBaggage;
	}

	public String getFlightDefCap() {
		return flightDefCap;
	}

	public void setFlightDefCap(String flightDefCap) {
		this.flightDefCap = flightDefCap;
	}

	public String[] getUserCarrierCode() {
		return userCarrierCode;
	}

	public void setUserCarrierCode(String[] userCarrierCode) {
		this.userCarrierCode = userCarrierCode;
	}

	public String getIsTimeInZulu() {
		return isTimeInZulu;
	}

	public void setIsTimeInZulu(String isTimeInZulu) {
		this.isTimeInZulu = isTimeInZulu;
	}

	public String getInterLineDisplay() {
		return interLineDisplay;
	}

	public void setInterLineDisplay(String interLineDisplay) {
		this.interLineDisplay = interLineDisplay;
	}

	public List<String[]> getCabinClassList() {
		return cabinClassList;
	}

	public void setCabinClassList(List<String[]> cabinClassList) {
		this.cabinClassList = cabinClassList;
	}

	public List<String[]> getAirportList() {
		return airportList;
	}

	public void setAirportList(List<String[]> airportList) {
		this.airportList = airportList;
	}

	public List<String[]> getFlightStatusList() {
		return flightStatusList;
	}

	public void setFlightStatusList(List<String[]> flightStatusList) {
		this.flightStatusList = flightStatusList;
	}

	public List<Map<String, String>> getClientMessages() {
		return clientMessages;
	}

	public void setClientMessages(List<Map<String, String>> clientMessages) {
		this.clientMessages = clientMessages;
	}

	public Boolean getHideLogicalCC() {
		return hideLogicalCC;
	}

	public void setHideLogicalCC(Boolean hideLogicalCC) {
		this.hideLogicalCC = hideLogicalCC;
	}

	public Boolean getHideWaitListing() {
		return hideWaitListing;
	}

	public void setHideWaitListing(Boolean hideWaitListing) {
		this.hideWaitListing = hideWaitListing;
	}

	public Boolean getHideAllocScreenExtraDetails() {
		return hideAllocScreenExtraDetails;
	}

	public void setHideAllocScreenExtraDetails(Boolean hideAllocScreenExtraDetails) {
		this.hideAllocScreenExtraDetails = hideAllocScreenExtraDetails;
	}

	public String getAvailableSeatKilometers() {
		return availableSeatKilometers;
	}

	public void setAvailableSeatKilometers(String availableSeatKilometers) {
		this.availableSeatKilometers = availableSeatKilometers;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;

	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public HashMap<String, List<SeatMvSummaryTO>> getAgentWiseSeatMovementDataMap() {
		return agentWiseSeatMovementDataMap;
	}

	public void setAgentWiseSeatMovementDataMap(HashMap<String, List<SeatMvSummaryTO>> agentWiseSeatMovementDataMap) {
		this.agentWiseSeatMovementDataMap = agentWiseSeatMovementDataMap;
	}

	public String getBlnShowBaggageAllowanceRemarks() {
		return blnShowBaggageAllowanceRemarks;
	}

	public void setBlnShowBaggageAllowanceRemarks(String blnShowBaggageAllowanceRemarks) {
		this.blnShowBaggageAllowanceRemarks = blnShowBaggageAllowanceRemarks;
	}

	/**
	 * @param showOverBookAlloc
	 *            the showOverBookAlloc to set
	 */
	public void setShowOverBookAlloc(Boolean showOverBookAlloc) {
		this.showOverBookAlloc = showOverBookAlloc;
	}

	/**
	 * @return the showOverBookAlloc
	 */
	public Boolean getShowOverBookAlloc() {
		return showOverBookAlloc;
	}

	public boolean isShowNestedAvailableSeats() {
		return showNestedAvailableSeats;
	}

	public void setShowNestedAvailableSeats(boolean showNestedAvailableSeats) {
		this.showNestedAvailableSeats = showNestedAvailableSeats;
	}
	
	public boolean isShowFCCSegBCInventoryAllocGrouping() {
		return showFCCSegBCInventoryAllocGrouping;
	}
}
