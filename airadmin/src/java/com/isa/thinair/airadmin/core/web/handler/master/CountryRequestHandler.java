package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.CountryHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 */

public final class CountryRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CountryRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_COUNTRYCODE = "txtCountryId";
	private static final String PARAM_COUNTRYNAME = "txtCountryDes";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_CURRENCYCODE = "selCurrencyCode";
	private static final String PARAM_REGION_CODE = "selRegionCode";
	private static final String PARAM_ALLOW_OHD = "chkOhdEnabled";
	private static final String PARAM_BSP_ENABLED = "chkBSPEnabled";
	private static final String PARAM_ISO_CODE_ALPHA_3 = "txtIsoCodeAlpha3";

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_SEARCH_COUNTRYNAME = "selCountryDesc";

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method for Country Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setExceptionOccured(request, false);
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

		try {
			if (strHdnMode != null) {

				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_COUNTRY_ADD);
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					saveData(request);
				} else if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_COUNTRY_EDIT);
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					saveData(request);
				} else if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_COUNTRY_DELETE);
					deleteData(request);
				}

			}
			setDisplayData(request);

		} catch (Exception exception) {
			log.error("Exception in CountryRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Delete the Country Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void deleteData(HttpServletRequest request) {

		String strCountryCode = request.getParameter(PARAM_COUNTRYCODE);

		try {
			if (strCountryCode != null) {
				Country existingCountry = ModuleServiceLocator.getLocationServiceBD().getCountry(strCountryCode);
				if (existingCountry == null)
					ModuleServiceLocator.getLocationServiceBD().deleteCountry(strCountryCode);
				else {
					String strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingCountry.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getLocationServiceBD().deleteCountry(existingCountry);
					} else {
						ModuleServiceLocator.getLocationServiceBD().deleteCountry(strCountryCode);
					}
				}
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				log.debug("CountryRequestHandler.deleteData() method is successfully executed.");
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in CountryRequestHandler:execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, airadminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Saves the Country Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {
		log.debug("Inside the CountryRequestHandler saveData()...");

		Properties countryProp = getProperty(request);

		try {
			Country country = new Country();
			int intGoAhead = 0;

			// Validate for existing country - Only for Add mode
			if (!"".equals(countryProp.getProperty(PARAM_COUNTRYCODE))) {
				if ("".equals(countryProp.getProperty(PARAM_VERSION))) {
					Country country1 = ModuleServiceLocator.getLocationServiceBD().getCountry(
							countryProp.getProperty(PARAM_COUNTRYCODE));
					if (country1 != null) {
						intGoAhead = 2;
					}
				}
			}

			// Validate for inactive currency
			if (!countryProp.getProperty(PARAM_CURRENCYCODE).equals("")
					&& !countryProp.getProperty(PARAM_CURRENCYCODE).equals("-1")) {

				if (checkValidCurrency(countryProp.getProperty(PARAM_COUNTRYCODE), countryProp.getProperty(PARAM_CURRENCYCODE)) == 1) {
					intGoAhead = 1;
				}
				country.setCurrencyCode(countryProp.getProperty(PARAM_CURRENCYCODE).trim());
			}

			if (intGoAhead != 0) {

				setExceptionOccured(request, true);
				String strFormData = getErroForm(countryProp);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

				switch (intGoAhead) {
				case 1:
					saveMessage(request, airadminConfig.getMessage("um.country.form.currency.inactive"), WebConstants.MSG_ERROR);
					break;
				case 2:
					saveMessage(request, airadminConfig.getMessage("um.country.form.id.already.exist"), WebConstants.MSG_ERROR);
					break;
				}

			} else {

				if (!"".equals(countryProp.getProperty(PARAM_VERSION))) {
					country.setVersion(Long.parseLong(countryProp.getProperty(PARAM_VERSION)));
				}
				country.setCountryCode(countryProp.getProperty(PARAM_COUNTRYCODE).toUpperCase());
				country.setCountryName(countryProp.getProperty(PARAM_COUNTRYNAME).trim());
				country.setRemarks(countryProp.getProperty(PARAM_REMARKS).trim());
				country.setRegionCode(countryProp.getProperty(PARAM_REGION_CODE).trim());

				if (countryProp.getProperty(PARAM_STATUS).equals("on"))
					country.setStatus(Country.STATUS_ACTIVE);
				else
					country.setStatus(Country.STATUS_INACTIVE);

				if ("on".equals(countryProp.getProperty(PARAM_ALLOW_OHD))) {
					country.setOnholdEnabled(AiradminUtils.YES);
				} else {
					country.setOnholdEnabled(AiradminUtils.NO);
				}

				if ("on".equals(countryProp.getProperty(PARAM_BSP_ENABLED))) {
					country.setBspEnabled(AiradminUtils.YES);
				} else {
					country.setBspEnabled(AiradminUtils.NO);
				}

				country.setIcaoCode(countryProp.getProperty(PARAM_ISO_CODE_ALPHA_3).trim());

				ModuleServiceLocator.getLocationServiceBD().saveCountry(country);

				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
				country = null;
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
				log.debug("CountryRequestHandler.saveData() method is successfully executed.");
			}

		} catch (ModuleException moduleException) {
			log.error("Exception in CountryRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);

			setExceptionOccured(request, true);
			String strFormData = getErroForm(countryProp);
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, airadminConfig.getMessage("um.country.form.id.already.exist"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {

			log.error("Exception in CountryRequestHandler:saveData()", exception);
			setExceptionOccured(request, true);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {

				String strFormData = getErroForm(countryProp);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Sets the Display Data for the Country Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	public static void setDisplayData(HttpServletRequest request) {

		setCountryList(request);
		setCountryRowHtml(request);
		setClientErrors(request);
		setCurrencyCodeList(request);
		setRegionCodeList(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		boolean isBSPEnabledForAirLine = AppSysParamsUtil.isBspPaymentsAcceptedForMCCreateReservation();
		request.setAttribute(WebConstants.REQ_BSP_ENABLED_FOR_AIRLINE, isBSPEnabledForAirLine);

		if (request.getParameter(PARAM_MODE) == null) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));

		} else if (request.getParameter(PARAM_MODE) != null && !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_ADD)
				&& !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else {

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
	}

	/**
	 * Sets the Currency List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCurrencyCodeList(HttpServletRequest request) {

		try {

			String strCurrencyCodeHtml = SelectListGenerator.createCurrencyList();
			request.setAttribute(WebConstants.REQ_HTML_CURRENCY_LIST, strCurrencyCodeHtml);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in CountryRequestHandler:setCurrencyCodeList() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Region List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setRegionCodeList(HttpServletRequest request) {

		try {

			String strRegionCodeHtml = SelectListGenerator.createRegionList();
			request.setAttribute(WebConstants.REQ_HTML_REGION_LIST, strRegionCodeHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in CountryRequestHandler:setRegionCodeList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations for the Country Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {

			String strClientErrors = CountryHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in CountryRequestHandler:setClientErrors() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
		}
	}

	/**
	 * Sets the Country list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCountryList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createCountryDescListWOTag();
			request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in CountryRequestHandler:setCountryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Country Data Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCountryRowHtml(HttpServletRequest request) {

		try {

			CountryHTMLGenerator htmlGen = new CountryHTMLGenerator();
			String strHtml = htmlGen.getCountryRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in CountryRequestHandler:setCountryRowHtml() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Ckecks the Validity of the Currency
	 * 
	 * @param countryID
	 *            the Country id
	 * @param currencyCode
	 *            the Corrency code
	 * @return int the 0- Success 1- The changed Currency inactive
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static int checkValidCurrency(String countryID, String currencyCode) throws ModuleException {

		Country country = null;
		if (countryID != null && !countryID.equals("")) {
			country = ModuleServiceLocator.getLocationServiceBD().getCountry(countryID);
		}
		Currency currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(currencyCode);
		if (currency == null)
			return 1;
		if (currency.getStatus().equals(Currency.STATUS_INACTIVE)) {
			if (country == null)
				return 1;
			else {
				Currency currencyofCountry = ModuleServiceLocator.getCommonServiceBD().getCurrency(country.getCurrencyCode());
				if (currencyofCountry == null)
					return 1;
				if (!currencyofCountry.getCurrencyCode().equals(currency.getCurrencyCode()))
					return 1;
			}
		}
		return 0;
	}

	/**
	 * Creats a Propert file with Country Data from the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the property File with Country data
	 */
	private static Properties getProperty(HttpServletRequest request) {

		Properties props = new Properties();

		String strSearchData = request.getParameter(PARAM_SEARCHDATA);

		String searchDataArr[] = null;

		if (strSearchData != null && !strSearchData.equals("")) {
			searchDataArr = strSearchData.split(",");
		}

		props.setProperty(PARAM_SEARCHDATA, AiradminUtils.getNotNullString(strSearchData));
		props.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION)));
		props.setProperty(PARAM_COUNTRYCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_COUNTRYCODE)));
		props.setProperty(PARAM_COUNTRYNAME, AiradminUtils.getNotNullString(request.getParameter(PARAM_COUNTRYNAME)));
		props.setProperty(PARAM_REMARKS, AiradminUtils.getNotNullString(request.getParameter(PARAM_REMARKS)));
		props.setProperty(PARAM_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS)));
		props.setProperty(PARAM_CURRENCYCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENCYCODE)));
		props.setProperty(PARAM_REGION_CODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_REGION_CODE)));
		props.setProperty(PARAM_SEARCH_COUNTRYNAME, (searchDataArr != null ? searchDataArr[0] : ""));
		props.setProperty(PARAM_ALLOW_OHD, AiradminUtils.getNotNullString(request.getParameter(PARAM_ALLOW_OHD)));
		props.setProperty(PARAM_BSP_ENABLED, AiradminUtils.getNotNullString(request.getParameter(PARAM_BSP_ENABLED)));
		props.setProperty(PARAM_ISO_CODE_ALPHA_3, AiradminUtils.getNotNullString(request.getParameter(PARAM_ISO_CODE_ALPHA_3)));

		return props;
	}

	/**
	 * Creates Form Data Array from a Property File
	 * 
	 * @param erp
	 *            the Property file with Form Data
	 * @return String the Form Data Array
	 */
	private static String getErroForm(Properties erp) {
		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");
		ersb.append("arrFormData[0] = new Array();");
		ersb.append("arrFormData[0][1] = '" + erp.getProperty(PARAM_COUNTRYCODE) + "';");
		ersb.append("arrFormData[0][2] = '" + erp.getProperty(PARAM_COUNTRYNAME) + "';");
		ersb.append("arrFormData[0][3] = '" + erp.getProperty(PARAM_REMARKS) + "';");
		ersb.append("arrFormData[0][4] = '" + erp.getProperty(PARAM_STATUS) + "';");
		ersb.append("arrFormData[0][5] = '" + erp.getProperty(PARAM_CURRENCYCODE) + "';");
		ersb.append("arrFormData[0][6] = '" + erp.getProperty(PARAM_VERSION) + "';");
		ersb.append("arrFormData[0][7] = '" + erp.getProperty(PARAM_REGION_CODE) + "';");
		ersb.append("arrFormdata[0][8] = '" + erp.getProperty(PARAM_ALLOW_OHD) + "';");
		ersb.append("arrFormdata[0][9] = '" + erp.getProperty(PARAM_BSP_ENABLED) + "';");
		ersb.append("arrFormdata[0][10] = '" + erp.getProperty(PARAM_ISO_CODE_ALPHA_3) + "';");
		return ersb.toString();
	}
}
