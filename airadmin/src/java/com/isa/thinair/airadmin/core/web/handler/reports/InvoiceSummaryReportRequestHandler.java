/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class InvoiceSummaryReportRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(InvoiceSummaryReportRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public InvoiceSummaryReportRequestHandler() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("InvoiceSummaryReportRequestHandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("InvoiceSummaryReportRequestHandler SETDISPLAYDATA() FAILED ", e);
		}

		try {
			if (strHdnMode != null
					&& (strHdnMode.equals(WebConstants.ACTION_VIEW) || strHdnMode.equals(WebConstants.ACTION_EXPORT))) {
				setReportView(request, response);
				log.debug("InvoiceSummaryReportRequestHandler setReportView Success");
				return null;
			} else {
				log.error("InvoiceSummaryReportRequestHandler setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("InvoiceSummaryReportRequestHandler setReportView Failed ", e);
		}

		return forward;

	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setInvoiceYear(request);
		setInvoicePeriod(request);
		setLiveStatus(request);
		setPaySource(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setTransactInvoiceEnable(request);
		setEntities(request);
	}

	private static void setTransactInvoiceEnable(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_ISTRNINV, AppSysParamsUtil.isTransactInvoiceEnable());
		request.setAttribute(WebConstants.REQ_VARISTRNINV, "var isJSTrnsWsInv =" + AppSysParamsUtil.isTransactInvoiceEnable()
				+ ";");
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		String strList = ReportsHTMLGenerator.createOwnAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * Method to get invoice period details.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setInvoicePeriod(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		sb.append("<option value='1' selected>1</option>");
		sb.append("<option value='2'>2</option>");

		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			sb.append("<option value='3'>3</option>");
			sb.append("<option value='4'>4</option>");
			sb.append("<option value='5'>5</option>");
		}

		request.setAttribute(WebConstants.REQ_IS_WEEKLYINVOICE_ENABLED, AppSysParamsUtil.isWeeklyInvoiceEnabled());
		request.setAttribute(WebConstants.REQ_INVOICEPERIOD, sb.toString());
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setPaySource(HttpServletRequest request) throws ModuleException {
		String strPay = "false";
		if (AccelAeroClients.AIR_ARABIA.equals(CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
				|| AccelAeroClients.AIR_ARABIA_GROUP.equals(CommonsServices.getGlobalConfig().getBizParam(
						SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))) {
			strPay = "true";
		}
		request.setAttribute(WebConstants.REP_RPT_SHOW_PAY, strPay);
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Method get current year and previous year.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setInvoiceYear(HttpServletRequest request) throws ModuleException {
		String currentYear = null;
		String previousYear = null;
		String selectYear = null;

		StringBuffer sb = new StringBuffer();

		Calendar year = new GregorianCalendar();
		currentYear = Integer.toString(year.get(Calendar.YEAR));
		previousYear = Integer.toString(year.get(Calendar.YEAR) - 1);
		String s[] = new String[4];
		s[0] = currentYear;
		s[1] = currentYear;
		s[2] = previousYear;
		s[3] = previousYear;

		sb.append("<option value='" + s[0] + "' selected>" + s[1] + "</option>");
		sb.append("<option value='" + s[2] + "'>" + s[3] + "</option>");

		selectYear = sb.toString();

		request.setAttribute(WebConstants.REQ_CURRENT_YEAR, currentYear);
		request.setAttribute(WebConstants.REQ_INVOICEYEAR, selectYear);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String year = request.getParameter("selYear");
		String month = request.getParameter("selMonth");
		String bilingPeriod = request.getParameter("selBP");
		String value = request.getParameter("radReportOption");
		String agent = request.getParameter("hdnAgents");
		String agentName = request.getParameter("hdnAgentName");
		String invoiceNo = request.getParameter("hdnInvoice");
		String rptView = request.getParameter("hdnRpt");
		String strFrom = request.getParameter("hdnFrompage");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strSource = request.getParameter("selPaySource");
		String strBase = request.getParameter("chkBase");
		String strReportMode = request.getParameter("hdnMode");
		String fromDate = request.getParameter("txtInvoiceFrom");
		String toDate = request.getParameter("txtInvoiceTo");
		boolean blnBase = (strBase != null && strBase.equalsIgnoreCase("on")) ? true : false;
		String strView = request.getParameter("chkNewview");

		boolean isNewView = (strView != null && strView.equalsIgnoreCase("on")) ? true : false;
		
		String entity = request.getParameter("selEntity");
		String entityText = request.getParameter("hdnEntityText");

		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String id = "UC_REPM_008";

		// String reportTemplateSummary = "InvoiceSummaryReport.jasper";
		String reportTemplateSummary = "InvoiceSummary.jasper";
		String reportTemplateDetails = "InvoiceDetailReportTemplat.jasper";

		String showPaymentBreakDown = "N";
		String showPaymentCurrency = "N";
		String showPaymentCurrWithBreakDown = "N";
		String showPetrofacReference = "N";
		String showExternalAmount = "N";

		Date firstday = null;
		Date lastday = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			if (fromDate != null && toDate != null) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
				int[] intFromDate = ReportsHTMLGenerator.getYearMonthDay(fromDate);
				int[] intToDate = ReportsHTMLGenerator.getYearMonthDay(toDate);
				year = String.valueOf(intFromDate[0]);
				month = String.valueOf(intFromDate[1]);
				GregorianCalendar frmDate = new GregorianCalendar();
				GregorianCalendar tDate = new GregorianCalendar();
				frmDate.set(Calendar.YEAR, intFromDate[0]);
				frmDate.set(Calendar.MONTH, (intFromDate[1] - 1));
				frmDate.set(Calendar.DATE, intFromDate[2]);

				tDate.set(Calendar.YEAR, intToDate[0]);
				tDate.set(Calendar.MONTH, (intToDate[1] - 1));
				tDate.set(Calendar.DATE, intToDate[2]);
				SimpleDateFormat queryDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				search.setFromDate(queryDateFormat.format(frmDate.getTime()) + " 00:00:00");
				search.setToDate(queryDateFormat.format(tDate.getTime()) + " 23:59:59");
			}
		} else {
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(Calendar.YEAR, Integer.parseInt(year));
			gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
			gc.set(Calendar.DATE, 1);

			if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
				firstday = BLUtil
						.getInvoceDate(Integer.parseInt(year), (Integer.parseInt(month)), Integer.parseInt(bilingPeriod));
				Calendar last = Calendar.getInstance();
				last.setTime(firstday);
				last.add(Calendar.DATE, 6);
				lastday = last.getTime();
			} else {
				if (bilingPeriod.equals("1")) {
					firstday = gc.getTime();
					gc.set(Calendar.DATE, 15);
					lastday = gc.getTime();

				} else {
					gc.set(Calendar.DATE, 16);
					firstday = gc.getTime();
					gc.set(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
					lastday = gc.getTime();
				}
			}

			search.setDateRangeFrom(formatter.format(firstday) + " 00:00:00");
			search.setDateRangeTo(formatter.format(lastday) + "  23:59:59");
		}

		if (agent != "") {
			String agentArr[] = agent.split(",");
			ArrayList<String> agentCol = new ArrayList<String>();
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
			search.setAgents(agentCol);
		} else {
			search.setAgents(null);
		}

		if (isNewView) {
			search.setReportViewNew(isNewView);
		}

		// AIRLINE_37
		String paymentBreakDown = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
		// AIRLINE_33
		String paymentInAgCur = ModuleServiceLocator.getGlobalConfig().getBizParam(
				SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY);
		// AIRLINE_34
		String invoiceInAgCur = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY);
		boolean isShowPetrofac = ModuleServiceLocator.getGlobalConfig().getPetroFacAgentsMap().containsKey(agent);

		String holidayIntegration = ModuleServiceLocator.getGlobalConfig()
				.getBizParam(SystemParamKeys.ENABLE_HOLIDAY_INTEGRATION);

		if (paymentBreakDown != null && paymentBreakDown.equalsIgnoreCase("Y")) {
			showPaymentBreakDown = "Y";
		}
		if (paymentInAgCur.equalsIgnoreCase("Y") && invoiceInAgCur.equalsIgnoreCase("Y") && blnBase) {
			showPaymentCurrency = "Y";
		}
		if (showPaymentCurrency.equalsIgnoreCase("Y") && showPaymentBreakDown.equalsIgnoreCase("Y")) {
			showPaymentCurrWithBreakDown = "Y";
			showPaymentBreakDown = "N";
		}
		if (isShowPetrofac) {
			showPetrofacReference = "Y";
		}
		if (strSource != null) {
			if (strSource.equalsIgnoreCase("EXTERNAL")) {
				showExternalAmount = "Y";
				search.setPaymentSource(strSource);
			} else if (strSource.equalsIgnoreCase("BOTH")) {
				showExternalAmount = "Y";
				search.setShowExternal("Y");
			}
		}

		String showAirAndLand = "N";
		if (holidayIntegration != null && holidayIntegration.equalsIgnoreCase("Y")) {
			showAirAndLand = "Y";
		}

		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String period = "";
		String reportName = "InvoiceDetailReport";
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			String agentPrefix = AppSysParamsUtil.getDefaultAirlineIdentifierCode().trim();
			boolean showAirlineIdentifierInReports = AppSysParamsUtil.isAllowAirlineCarrierPrefixForAgentsAndUsers();

			if (rptView.equals("Summary")) {
				reportName = "InvoiceSummaryReport";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));

				search.setYear(year);
				search.setMonth(month);
				search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_SUMMARY);
				parameters.put("INVOICE_YEAR", year);
				parameters.put("INVOICE_MONTH", month);				
				if (isNewView) {
					parameters.put("VIEW", "ON");
				}

			} else {
				reportName = "InvoiceDetailReport";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));
				search.setAgentCode(agent);
				search.setYear(year);
				search.setMonth(month);
				search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_DETAILS);

				if (agent != null && !showAirlineIdentifierInReports) {
					boolean withAgentPrefix = agent.startsWith(agentPrefix);
					if (withAgentPrefix) {
						String agentCodewithoutPrefix = agent.substring(agentPrefix.length());
						parameters.put("AGENT_CODE", agentCodewithoutPrefix);
					} else {
						parameters.put("AGENT_CODE", agent);
					}
				} else {
					parameters.put("AGENT_CODE", agent);
				}
				parameters.put("AGENT_NAME", agentName);
				parameters.put("INVOICE_NO", invoiceNo);
				parameters.put("ID", id);
			}
			parameters.put("ID", id);
			if (strSource == null) {
				strSource = "INTERNAL";
			}

			parameters.put("SOURCE", strSource);
			parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));

			parameters.put("SHOW_BREAKDOWN", showPaymentBreakDown);
			parameters.put("SHOW_PAY_CUR", showPaymentCurrency);
			parameters.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
			parameters.put("SHOW_PAYREF", showPetrofacReference);
			parameters.put("SHOW_EXTERNAL", showExternalAmount);
			parameters.put("SHOW_AIRANDLAND", showAirAndLand);

			if (blnBase) {
				parameters.put("CHK_BASE", "on");
			}
			if (AppSysParamsUtil.isTransactInvoiceEnable()) {
				parameters.put("SHOW_INVOICE_ACTUAL_AMOUNT", "Y");
			} else {
				parameters.put("SHOW_INVOICE_ACTUAL_AMOUNT", "N");
			}

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			search.setPaymentSource(strSource);
			if (bilingPeriod != null && !bilingPeriod.equals("")) {
				search.setBillingPeriod(Integer.parseInt(bilingPeriod));
				period += String.valueOf(bilingPeriod);
			}

			search.setEntity(entity);
			
			if (rptView.equals("Summary")) {
				resultSet = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryDataByCurrency(search);
			} else {
				resultSet = ModuleServiceLocator.getDataExtractionBD().getInvoiceSummaryDataByCurrency(search);
			}

			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + strLogo;

			parameters.put("INVOICE_PERIOD", period);
			parameters.put("OPTION", value);

			if (showAirlineIdentifierInReports) {
				parameters.put("PRIFIX_FOR_FORMATED_AGENT_NAMES", agentPrefix);
			} else {
				parameters.put("PRIFIX_FOR_FORMATED_AGENT_NAMES", "");
			}

			parameters.put("ENTITY", entityText);
			parameters.put("ENTITY_CODE", entity);
			
			if (strReportMode.equals(WebConstants.ACTION_EXPORT)) {
				parameters.put("SHOW_HEADING", "N");
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			} else {
				if (AppSysParamsUtil.isTransactInvoiceEnable()) {
					parameters.put("DISPLAY_LINK", "N");
				} else {
					parameters.put("DISPLAY_LINK", "Y");
				}
				parameters.put("SHOW_HEADING", "Y");

				if (value.trim().equals("HTML")) {
					reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
					parameters.put("IMG", reportsRootDir);

					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							null, response);

				} else if (value.trim().equals("PDF")) {
					response.reset();
					response.addHeader("Content-Disposition", "filename=" + reportName + ".pdf");
					parameters.put("IMG", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
				} else if (value.trim().equals("EXCEL")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".xls");
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
				} else if (value.trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
				}
			}

		} catch (Exception e) {
			log.error("Error in setReportView()", e);
		}
	}
	
	private static void setEntities(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_ENTITIES, SelectListGenerator.createEntityListByName());
	}
}
