/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

public class PRLDetailProcessingHTMLGenerator {

	private static String clientErrors;
	private static final String PARAM_CURRENT_PRL = "hdnCurrentPrl";
	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private String strFormFieldsVariablesJS = "";
	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm";
	SimpleDateFormat outputDateFormat;

	/**
	 * Gets the PRL Passenger Details Rows for the Selected PRL
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of PRL Detail Rows
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws Exception
	 *             the Exception
	 */
	public final String getPRLProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIModeJS = "var isSearchMode = false; var strPRLContent;";
		String strProcessStatus = "";
		outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		strUIModeJS = "var isSearchMode = true; var strPRLContent;";

		strProcessStatus = request.getParameter(PARAM_SEARCH_PROCESS_STATUS);
		strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		setFormFieldValues(strFormFieldsVariablesJS);

		return createPRLProcessingRowHTML(request);
	}

	/**
	 * Creates the PRL Processing Row for the Selected PRL
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containig the PRL details
	 * @throws Exception
	 *             the Exception
	 */
	private String createPRLProcessingRowHTML(HttpServletRequest request) throws Exception {

		String strCurrentPRL = "";

		strCurrentPRL = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PRL));
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		String tempDesc = "";
		String paxProcessedStatus = "";
		String paxErrorStatus = "";
		String[] arrCurrentPRL = strCurrentPRL.split(",");
		Collection<PRLPaxEntry> prlPaxEntries = ModuleServiceLocator.getPRLBD().getPRLParseEntries(
				Integer.valueOf(arrCurrentPRL[3]).intValue());

		if (prlPaxEntries != null && !prlPaxEntries.isEmpty()) {
			int i = 0;
			String strTemp = "";
			for (Iterator<PRLPaxEntry> iterPrlPaxEntries = prlPaxEntries.iterator(); iterPrlPaxEntries.hasNext();) {
				PRLPaxEntry prlPaxEntry = (PRLPaxEntry) iterPrlPaxEntries.next();
				paxProcessedStatus = "";
				paxErrorStatus = "";
				if (prlPaxEntry.getPaxType().equals("IN")) {
					continue;
				}

				sb.append("arrData[" + i + "] = new Array();");

				if (prlPaxEntry.getTitle() != null) {
					sb.append("arrData[" + i + "][0]= '" + prlPaxEntry.getTitle() + "';");
				} else {
					sb.append("arrData[" + i + "][0] = ''; ");
				}

				if (prlPaxEntry.getFirstName() != null) {
					sb.append("arrData[" + i + "][1]= '" + prlPaxEntry.getFirstName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][1] = ''; ");
				}

				sb.append("arrData[" + i + "][16] = ''; ");

				if (prlPaxEntry.getLastName() != null) {
					sb.append("arrData[" + i + "][2] = '" + prlPaxEntry.getLastName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][2] = ''; ");
				}

				if (prlPaxEntry.getPnr() != null) {
					sb.append("arrData[" + i + "][3] = '" + prlPaxEntry.getPnr() + "'; ");
				} else {
					sb.append("arrData[" + i + "][3] = ''; ");
				}

				if (prlPaxEntry.getArrivalAirport() != null) {
					sb.append("arrData[" + i + "][4] = '" + prlPaxEntry.getArrivalAirport() + "'; ");
				} else {
					sb.append("arrData[" + i + "][4] = ''; ");
				}

				sb.append("arrData[" + i + "][5] = '" + (prlPaxEntry.getPaxStatus() != null ?
						prlPaxEntry.getPaxStatus() :
						"") + "';");
				if (prlPaxEntry.getProcessedStatus() != null) {
					tempDesc = "";
					if (prlPaxEntry.getProcessedStatus().equals(ParserConstants.PRLProcessStatus.ERROR_OCCURED)
							|| paxErrorStatus != null && !paxErrorStatus.equals("")) {
						tempDesc = "Error Occured";
						if (prlPaxEntry.getErrorDescription() != null
								&& !prlPaxEntry.getErrorDescription().trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (prlPaxEntry.getErrorDescription() == null ? "" : prlPaxEntry
											.getErrorDescription().trim()) + "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");
						} else if (paxErrorStatus != null && !paxErrorStatus.trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (paxErrorStatus == null ? "" : paxErrorStatus.trim()) + "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");

						} else {
							sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
						}
					} else if (prlPaxEntry.getProcessedStatus().equals(
							ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED)) {
						tempDesc = "Not Processed";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					} else if (prlPaxEntry.getProcessedStatus().equals(ParserConstants.PRLProcessStatus.PROCESSED)) {
						if (paxProcessedStatus.equals("")
								|| paxProcessedStatus.equals(ParserConstants.PRLProcessStatus.PROCESSED)) {
							tempDesc = "Processed";
						} else {
							tempDesc = "Not Processed";
						}
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					} else {
						tempDesc = "Unknown";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					}
				} else {
					sb.append("arrData[" + i + "][6] = '';");
				}

				sb.append("arrData[" + i + "][7] = '" + prlPaxEntry.getPaxStatus() + "';");

				if (prlPaxEntry.getProcessedStatus() != null) {
					sb.append("arrData[" + i + "][8] = '" + prlPaxEntry.getProcessedStatus() + "';");
				} else {
					sb.append("arrData[" + i + "][8] = '';");
				}

				if (prlPaxEntry.getPaxType() != null) {
					sb.append("arrData[" + i + "][15]= '" + prlPaxEntry.getPaxType() + "'; ");
				} else {
					sb.append("arrData[" + i + "][15] = ''; ");
				}
				sb.append("arrData[" + i + "][9] = '" + prlPaxEntry.getPrlPaxId() + "';");
				sb.append("arrData[" + i + "][10] = '" + prlPaxEntry.getVersion() + "';");
				sb.append("arrData[" + i + "][11] = '" + prlPaxEntry.getPrlId().toString() + "';");
				sb.append("arrData[" + i + "][12] = '"
						+ (prlPaxEntry.getErrorDescription() == null ? "" : prlPaxEntry.getErrorDescription().trim())
						+ "';");
				sb.append("arrData[" + i + "][13] = '" + prlPaxEntry.getCabinClassCode() + "';");
				sb.append("arrData[" + i + "][14] = '" + "" + "';");
				sb.append("arrData[" + i + "][16] = '" + (prlPaxEntry.getEticketNumber() != null ?
						prlPaxEntry.getEticketNumber() :
						"") + "';");
				sb.append("arrData[" + i + "][17] = '" + "N" + "';");
				sb.append("arrData[" + i + "][18] = '"
						+ (prlPaxEntry.getInfEticketNumber() != null ? prlPaxEntry.getInfEticketNumber() : "") + "';");
				sb.append("arrData[" + i + "][19] = '" + (prlPaxEntry.getPassportNumber() != null ?
						prlPaxEntry.getPassportNumber() :
						"") + "';");
				if (prlPaxEntry.getPassportExpiryDate() != null) {
					sb.append("arrData[" + i + "][20] = '" + CalendarUtil
							.getDateInFormattedString(CalendarUtil.PATTERN1, prlPaxEntry.getPassportExpiryDate())
							+ "';");
				} else {
					sb.append("arrData[" + i + "][20] = '';");
				}
				sb.append("arrData[" + i + "][21] = '" + (prlPaxEntry.getCountryOfIssue() != null ? prlPaxEntry.getCountryOfIssue() : "") + "';");
				sb.append("arrData[" + i + "][22] = '" + prlPaxEntry.getSeatNumber() + "';");
				sb.append("arrData[" + i + "][23] = '" + prlPaxEntry.getWeightIndicator() + "';");
				sb.append("arrData[" + i + "][24] = '" + prlPaxEntry.getNoOfCheckedBaggage() + "';");
				sb.append("arrData[" + i + "][25] = '" + prlPaxEntry.getCheckedBaggageWeight() + "';");
				sb.append("arrData[" + i + "][26] = '" + prlPaxEntry.getUncheckedBaggageWeight() + "';");
				sb.append("arrData[" + i + "][27] = '" + prlPaxEntry.getCoupNumber() + "';");
				sb.append("arrData[" + i + "][28] = '" + prlPaxEntry.getInfCoupNumber() + "';");
				i++;
			}
		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for PRL Detail Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
			moduleErrs.setProperty("um.prlprocess.form.prl.passportNo", "passportNoRqrd");
			moduleErrs.setProperty("um.prlprocess.form.prl.passPortExpDate", "passportExpDateRqrd");
			moduleErrs.setProperty("um.prlprocess.form.prl.countryOfIssue", "issueCountryRqrd");
			moduleErrs.setProperty("um.prlprocess.form.prl.seatNo", "seatNoRqrd");
			moduleErrs.setProperty("um.prlprocess.form.prl.weightIndicator", "weightIndicatorRqrd");
			moduleErrs.setProperty("um.prlprocess.form.prl.noOfCheckBag", "noOfcheckBagRqrd");
			moduleErrs.setProperty("um.prlprocess.form.prl.checkedBagWeight", "checkedBagWeightRqrd");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	public String getInfantPaxParentHTML(HttpServletRequest request) throws ModuleException {

		String strCurrentPRL = "";
		StringBuffer sb = new StringBuffer("<option value=''></option>");

		strCurrentPRL = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PRL));
		String[] arrCurrentPRL = strCurrentPRL.split(",");
		Collection<PRLPaxEntry> prlPaxEntries = ModuleServiceLocator.getPRLBD().getPRLParseEntries(
				Integer.valueOf(arrCurrentPRL[3]).intValue());

		if (prlPaxEntries != null && !prlPaxEntries.isEmpty()) {
			for (Iterator prlPaxIterator = prlPaxEntries.iterator(); prlPaxIterator.hasNext();) {
				PRLPaxEntry prlPaxEntry = (PRLPaxEntry) prlPaxIterator.next();

				if (prlPaxEntry.getPaxType().equals("AD")
						&& prlPaxEntry.getProcessedStatus().equals(
								ReservationInternalConstants.PfsProcessStatus.PROCESSED)) {
					sb.append(" <option value='" + prlPaxEntry.getPrlPaxId() + "'>" + prlPaxEntry.getTitle() + " "
							+ prlPaxEntry.getFirstName() + " " + prlPaxEntry.getLastName() + "</option>");
				}
			}

		}

		return sb.toString();
	}

}
