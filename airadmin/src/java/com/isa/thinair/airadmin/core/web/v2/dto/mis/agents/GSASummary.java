package com.isa.thinair.airadmin.core.web.v2.dto.mis.agents;

public class GSASummary {

	private boolean isIndependentAgent = false;

	private boolean isSalesOffice = false;

	private String stationCode;

	private String stationDesc;

	private String gsaName;

	private String locationUrl;

	private String gsaCode;

	private GSADetailDTO gsaDetail;

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getStationDesc() {
		return stationDesc;
	}

	public void setStationDesc(String stationDesc) {
		this.stationDesc = stationDesc;
	}

	public String getGsaName() {
		return gsaName;
	}

	public void setGsaName(String gsaName) {
		this.gsaName = gsaName;
	}

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public boolean isIndependentAgent() {
		return isIndependentAgent;
	}

	public void setIndependentAgent(boolean isIndependentAgent) {
		this.isIndependentAgent = isIndependentAgent;
	}

	public GSADetailDTO getGsaDetail() {
		return gsaDetail;
	}

	public void setGsaDetail(GSADetailDTO gsaDetail) {
		this.gsaDetail = gsaDetail;
	}

	public String getGsaCode() {
		return gsaCode;
	}

	public void setGsaCode(String gsaCode) {
		this.gsaCode = gsaCode;
	}

	public boolean isSalesOffice() {
		return isSalesOffice;
	}

	public void setSalesOffice(boolean isSalesOffice) {
		this.isSalesOffice = isSalesOffice;
	}
}
