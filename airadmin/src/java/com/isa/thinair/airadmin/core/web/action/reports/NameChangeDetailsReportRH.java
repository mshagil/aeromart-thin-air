/**
 * 
 */
package com.isa.thinair.airadmin.core.web.action.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Noshani Perera
 * 
 */
public class NameChangeDetailsReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(NameChangeDetailsReportRH.class);

	private static final String NAME_CHANGE_DETAILS_REPORT = "NameChangeDetailsReport.jasper";

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static AiradminModuleConfig config = AiradminModuleUtils.getConfig();

	/**
	 * Main Execute Method for Name Change Report Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			log.debug("NameChangeDetailsReportRH setDisplayData() SUCCESS");
		} catch (Exception e) {
			log.error("NameChangeDetailsReportRH setDisplayData() FAILED " + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("NameChangeDetailsReportRH setReportView Success");
				return null;
			} else {
				log.error("NameChangeDetailsReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("NameChangeDetailsReportRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Display data For Name Change Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {

		setClientErrors(request);
		setLiveStatus(request);
		setUserIdList(request);
		setAirportComboList(request);
		setReportingPeriod(request);

	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAirportComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Sets the user ids to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setUserIdList(HttpServletRequest request) throws ModuleException {

		String strUserIdList = SelectListGenerator.createUsersList();
		request.setAttribute(WebConstants.REQ_USER_ID_LIST, strUserIdList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null) {
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Displays the Name Change Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String fromDepDate = request.getParameter("txtDepFromDate");
		String toDepDate = request.getParameter("txtDepToDate");
		String userId = request.getParameter("selUserId");
		String flightNo = request.getParameter("txtFlightNumber");
		String segFrom = request.getParameter("selFrom");
		String segTo = request.getParameter("selTo");
		String value = request.getParameter("radReportOption");

		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));

			if (fromDepDate != null && !fromDepDate.equals("")) {
				search.setDepartureDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDepDate));
			}
			if (toDepDate != null && !toDepDate.equals("")) {
				search.setDepartureDateRangeTo(ReportsHTMLGenerator.convertDate(toDepDate));
			}

			search.setUserId(userId);
			search.setFlightNumber(flightNo);
			search.setSectorFrom(segFrom);
			search.setSectorTo(segTo);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getNameChangeDetailsData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("USER_ID", userId);
			parameters.put("REPORT_TYPE", value);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("ID", "UC_REPM_066");
			parameters.put("INCLUDE_ADVANCED_DETAILS", config.getIncludeAdvancedNameChangeDetails());
			if ((segFrom != null && !segFrom.equals("")) || (segTo != null && !segTo.equals(""))) {
				parameters.put("SEGMENT", segFrom + "/" + segTo);
			} else {
				parameters.put("SEGMENT", "");
			}

			reportTemplate = NAME_CHANGE_DETAILS_REPORT;

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);

			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=NameChangeDetails.pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=NameChangeDetails.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=NameChangeDetails.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error("setReportView :: ", e);
		}
	}

}
