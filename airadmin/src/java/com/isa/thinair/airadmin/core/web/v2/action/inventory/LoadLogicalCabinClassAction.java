package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.inventory.LogicalCabinClassRH;
import com.isa.thinair.airinventory.api.dto.inventory.LogicalCabinClassDTO;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadLogicalCabinClassAction extends BaseRequestAwareAction {
	private final Log log = LogFactory.getLog(getClass());
	private LogicalCabinClassDTO searchParams;
	private LogicalCabinClass saveParams = new LogicalCabinClass();
	private Object[] rows;
	private String msgType;
	private String succesMsg;
	private int page;
	private int records;
	private int total;
	private String status;
	private String chkNestShift;
	private long version;
	private String strLccImageUploads;
	private String strWithFlexiImageUploads;

	/*
	 * The following 4 fields are the JSON strings that will passed from the View to this action class. They will
	 * contain the internationalized messages to be parsed and save for each respective field.
	 */
	private String descriptionForDisplay;
	private String flexiDescriptionForDisplay;
	private String flexiCommentForDisplay;
	private String commentsForDisplay;

	public String execute() {
		try {
			if (saveParams.getStatus() == null) {
				saveParams.setStatus(CommonsConstants.STATUS_INACTIVE);
			}

			if (saveParams.getAllowSingleMealOnly() == null) {
				saveParams.setAllowSingleMealOnly(CommonsConstants.NO);
			}

			if (saveParams.getFreeSeatEnabled() == null) {
				saveParams.setFreeSeatEnabled(CommonsConstants.NO);
			}

			if (saveParams.getFreeFlexiEnabled() == null) {
				saveParams.setFreeFlexiEnabled(CommonsConstants.NO);
			}

			if (chkNestShift == null) {
				chkNestShift = CommonsConstants.STATUS_INACTIVE;
			}

			LogicalCabinClassRH.saveTemplate(saveParams, chkNestShift);
			saveUploadedImage(saveParams, com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.TEMP_LCC_PREFIX);
			saveUploadedImage(saveParams, com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.TEMP_LCC_WITH_FLEXI_PREFIX);
			this.msgType = WebConstants.MSG_SUCCESS;
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_FAIL);
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchCabinClass() {
		try {
			if (this.page == 0) {
				this.page = 1;
			}
			Page<LogicalCabinClass> cabinClass = LogicalCabinClassRH.searchCabinClass(page, searchParams);
			Map<String, Map<String, String>> translationMap = LogicalCabinClassRH.getTranslatedMessages();
			if (cabinClass != null) {
				this.records = cabinClass.getTotalNoOfRecords();
				this.total = cabinClass.getTotalNoOfRecords() / 20;
				int mod = cabinClass.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}
				if (this.page <= 0) {
					this.page = 1;
				}
				// The JSON data that will be sent to the View
				Collection<LogicalCabinClass> colCabinClass = cabinClass.getPageData();
				if (colCabinClass != null) {
					Object[] dataRow = new Object[colCabinClass.size()];
					int index = 1;
					for (LogicalCabinClass lcc : colCabinClass) {
						Map<String, Object> counmap = new HashMap<String, Object>();
						counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
						counmap.put("logicalCC", lcc.getLogicalCCCode());
						counmap.put("description", lcc.getDescription());
						counmap.put("descriptionI18n", lcc.getDescriptionI18n());
						counmap.put("descriptionForDisplay", translationMap.get(lcc.getDescriptionI18n()));
						counmap.put("cabinClass", lcc.getCabinClassCode());
						counmap.put("rank", lcc.getNestRank());
						counmap.put("status", lcc.getStatus());
						counmap.put("version", lcc.getVersion());
						counmap.put("allowSingleMealOnly", lcc.getAllowSingleMealOnly());
						counmap.put("freeSeatEnabled", lcc.getFreeSeatEnabled());
						counmap.put("freeFlexiEnabled", lcc.getFreeFlexiEnabled());
						counmap.put("flexiDescription", lcc.getFlexiDescription());
						counmap.put("flexiDescriptionI18n", lcc.getFlexiDescriptionI18n());
						counmap.put("flexiDescriptionForDisplay", translationMap.get(lcc.getFlexiDescriptionI18n()));
						counmap.put("flexiComment", lcc.getFlexiComment());
						counmap.put("flexiCommentI18n", lcc.getFlexiCommentI18n());
						counmap.put("flexiCommentForDisplay", translationMap.get(lcc.getFlexiCommentI18n()));
						counmap.put("comments", lcc.getComments());
						counmap.put("commentsI18n", lcc.getCommentsI18n());
						counmap.put("commentsForDisplay", translationMap.get(lcc.getCommentsI18n()));
						counmap.put("bundleDescId",lcc.getBundleDescTemplateId());
						dataRow[index - 1] = counmap;
						index++;
					}

					this.rows = dataRow;
				}
			}
			this.msgType = WebConstants.MSG_SUCCESS;
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
		} catch (Exception e) {
			log.error("Error occured : ", e);
			this.msgType = WebConstants.MSG_ERROR;
			this.succesMsg = e.getMessage();
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteLogicalCabinClass() {
		try {
			boolean checkDeleteSuccess = LogicalCabinClassRH.deleteLogicalCabinClass(saveParams);
			if (checkDeleteSuccess == true) {
				this.msgType = WebConstants.MSG_SUCCESS;
				this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			} else {
				this.msgType = WebConstants.MSG_ERROR;
				this.succesMsg = airadminConfig.getMessage("um.logicalCC.delete.fail");
			}
		} catch (Exception e) {
			this.msgType = WebConstants.MSG_ERROR;
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String saveTranslations() {
		try {
			JSONParser parser = new JSONParser();
			JSONObject descriptionJSON = (JSONObject) parser.parse(descriptionForDisplay);
			JSONObject flexiDescriptionJSON = (JSONObject) parser.parse(flexiDescriptionForDisplay);
			JSONObject flexiCommentJSON = (JSONObject) parser.parse(flexiCommentForDisplay);
			JSONObject commentsJSON = (JSONObject) parser.parse(commentsForDisplay);

			Map<String, Map<String, String>> translationsMap = new HashMap<String, Map<String, String>>();
			translationsMap.put(saveParams.getCommentsI18n(), new HashMap<String, String>());
			translationsMap.put(saveParams.getDescriptionI18n(), new HashMap<String, String>());
			translationsMap.put(saveParams.getFlexiCommentI18n(), new HashMap<String, String>());
			translationsMap.put(saveParams.getFlexiDescriptionI18n(), new HashMap<String, String>());

			for (Object strKey : descriptionJSON.keySet()) {
				Map<String, String> commentMap = translationsMap.get(saveParams.getCommentsI18n());
				Map<String, String> descMap = translationsMap.get(saveParams.getDescriptionI18n());
				Map<String, String> flexiCommentMap = translationsMap.get(saveParams.getFlexiCommentI18n());
				Map<String, String> flexiDescMap = translationsMap.get(saveParams.getFlexiDescriptionI18n());

				commentMap.put(strKey.toString(), commentsJSON.get(strKey).toString());
				descMap.put(strKey.toString(), descriptionJSON.get(strKey).toString());
				flexiCommentMap.put(strKey.toString(), flexiCommentJSON.get(strKey).toString());
				flexiDescMap.put(strKey.toString(), flexiDescriptionJSON.get(strKey).toString());
			}
			LogicalCabinClassRH.saveTranslations(translationsMap);
			this.msgType = WebConstants.MSG_SUCCESS;
			this.succesMsg = "Translations saved successfully";
		} catch (Exception e) {
			this.msgType = WebConstants.MSG_ERROR;
			this.succesMsg = "Failed to save translations.";
		}
		return S2Constants.Result.SUCCESS;
	}

	@SuppressWarnings("unchecked")
	private void saveUploadedImage(LogicalCabinClass logicalCabinClass, String imageType) throws IOException {
		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		String mountPath = adminCon.getMealImages();
		File tmpFile = null;
		String FlexiFlag = "N";
		String tempImagePrefix = com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.TEMP_LCC_PREFIX;
		Map<String, String> langWiseUploads = null;

		if (com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.TEMP_LCC_WITH_FLEXI_PREFIX.equals(imageType)) {
			if (this.strWithFlexiImageUploads != null && !this.strWithFlexiImageUploads.trim().equals("")) {
				langWiseUploads = JSONFETOParser.parseMap(HashMap.class, String.class, strWithFlexiImageUploads);
				FlexiFlag = "Y";
				tempImagePrefix = com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.TEMP_LCC_WITH_FLEXI_PREFIX;
			}
		} else {
			if (this.strLccImageUploads != null && !this.strLccImageUploads.trim().equals("")) {
				langWiseUploads = JSONFETOParser.parseMap(HashMap.class, String.class, strLccImageUploads);
			}
		}

		if (!langWiseUploads.isEmpty()) {
			for (Entry<String, String> uploadEntry : langWiseUploads.entrySet()) {
				String langCode = uploadEntry.getKey();
				String uploadStatus = uploadEntry.getValue();
				if (uploadStatus != null && uploadStatus.equals("true")) {

					StringBuilder fileNameBuilder = new StringBuilder(mountPath);
					fileNameBuilder.append(tempImagePrefix);
					fileNameBuilder.append("_test_");
					fileNameBuilder.append(langCode).append(com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.IMG_SUFFIX);
					tmpFile = new File(fileNameBuilder.toString());

					fileNameBuilder = new StringBuilder(mountPath);
					fileNameBuilder.append(logicalCabinClass.getLogicalCCCode()).append(
							com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.SEPARATOR);
					fileNameBuilder.append(FlexiFlag).append(com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.SEPARATOR);
					fileNameBuilder.append(langCode).append(com.isa.thinair.commons.api.dto.LogicalCabinClassDTO.IMG_SUFFIX);
					File dataFile = new File(fileNameBuilder.toString());
					FileUtils.copyFile(tmpFile, dataFile);
				}
			}
		}
	}

	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the chkNestShift
	 */
	public String getChkNestShift() {
		return chkNestShift;
	}

	/**
	 * @param chkNestShift
	 *            the chkNestShift to set
	 */
	public void setChkNestShift(String chkNestShift) {
		this.chkNestShift = chkNestShift;
	}

	/**
	 * @return the saveParams
	 */
	public LogicalCabinClass getSaveParams() {
		return saveParams;
	}

	/**
	 * @param saveParams
	 *            the saveParams to set
	 */
	public void setSaveParams(LogicalCabinClass saveParams) {
		this.saveParams = saveParams;
	}

	/**
	 * @return the searchParams
	 */
	public LogicalCabinClassDTO getSearchParams() {
		return searchParams;
	}

	/**
	 * @param searchParams
	 *            the searchParams to set
	 */
	public void setSearchParams(LogicalCabinClassDTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @param descriptionForDisplay
	 *            the descriptionForDisplay to set
	 */
	public void setDescriptionForDisplay(String descriptionForDisplay) {
		this.descriptionForDisplay = descriptionForDisplay;
	}

	/**
	 * @return the descriptionForDisplay
	 */
	public String getDescriptionForDisplay() {
		return descriptionForDisplay;
	}

	/**
	 * @param flexiDescriptionForDisplay
	 *            the flexiDescriptionForDisplay to set
	 */
	public void setFlexiDescriptionForDisplay(String flexiDescriptionForDisplay) {
		this.flexiDescriptionForDisplay = flexiDescriptionForDisplay;
	}

	/**
	 * @return the flexiDescriptionForDisplay
	 */
	public String getFlexiDescriptionForDisplay() {
		return flexiDescriptionForDisplay;
	}

	/**
	 * @param flexiCommentForDisplay
	 *            the flexiCommentForDisplay to set
	 */
	public void setFlexiCommentForDisplay(String flexiCommentForDisplay) {
		this.flexiCommentForDisplay = flexiCommentForDisplay;
	}

	/**
	 * @return the flexiCommentForDisplay
	 */
	public String getFlexiCommentForDisplay() {
		return flexiCommentForDisplay;
	}

	/**
	 * @param commentsForDisplay
	 *            the commentsForDisplay to set
	 */
	public void setCommentsForDisplay(String commentsForDisplay) {
		this.commentsForDisplay = commentsForDisplay;
	}

	/**
	 * @return the commentsForDisplay
	 */
	public String getCommentsForDisplay() {
		return commentsForDisplay;
	}
	
	public void setBundleDescId (String bundleDescId) {
		saveParams.setBundleDescId(bundleDescId);
	}

	public void setLogicalCCCode(String logicalCCCode) {
		saveParams.setLogicalCCCode(logicalCCCode);
	}

	public void setCabinClassCode(String cabinClassCode) {
		saveParams.setCabinClassCode(cabinClassCode);
	}

	public void setDescription(String description) {
		saveParams.setDescription(description);
	}

	public void setDescriptionI18n(String descriptionI18n) {
		saveParams.setDescriptionI18n(descriptionI18n);
	}

	public void setNestRank(int nestRank) {
		saveParams.setNestRank(nestRank);
	}

	public void setAllowSingleMealOnly(String allowSingleMealOnly) {
		saveParams.setAllowSingleMealOnly(allowSingleMealOnly);
	}

	public void setFreeSeatEnabled(String freeSeatEnabled) {
		saveParams.setFreeSeatEnabled(freeSeatEnabled);
	}

	public void setFreeFlexiEnabled(String freeFlexiEnabled) {
		saveParams.setFreeFlexiEnabled(freeFlexiEnabled);
	}

	public void setFlexiDescription(String flexiDescription) {
		saveParams.setFlexiDescription(flexiDescription);
	}

	public void setFlexiDescriptionI18n(String flexiDescriptionI18n) {
		saveParams.setFlexiDescriptionI18n(flexiDescriptionI18n);
	}

	public void setComments(String comments) {
		saveParams.setComments(comments);
	}

	public void setCommentsI18n(String commentsI18n) {
		saveParams.setCommentsI18n(commentsI18n);
	}

	public void setFlexiComment(String flexiComment) {
		saveParams.setFlexiComment(flexiComment);
	}

	public void setFlexiCommentI18n(String flexiCommentI18n) {
		saveParams.setFlexiCommentI18n(flexiCommentI18n);
	}

	public void setClsStatus(String clsStatus) {
		saveParams.setStatus(clsStatus);
	}

	public void setLccVersion(String lccVersion) {
		saveParams.setVersion(Long.parseLong(lccVersion));
	}

	public String getStrLccImageUploads() {
		return strLccImageUploads;
	}

	public void setStrLccImageUploads(String strLccImageUploads) {
		this.strLccImageUploads = strLccImageUploads;
	}

	public String getStrWithFlexiImageUploads() {
		return strWithFlexiImageUploads;
	}

	public void setStrWithFlexiImageUploads(String strWithFlexiImageUploads) {
		this.strWithFlexiImageUploads = strWithFlexiImageUploads;
	}
}