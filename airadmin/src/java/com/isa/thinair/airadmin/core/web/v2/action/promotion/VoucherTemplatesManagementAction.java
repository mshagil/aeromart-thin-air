package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateRequest;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;

/**
 * @author chanaka
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class VoucherTemplatesManagementAction {

	private static final Log log = LogFactory.getLog(VoucherTemplatesManagementAction.class);

	// in
	private String voucherName;
	private String startDate;
	private String endDate;
	private String status;
	private VoucherTemplateTo voucherTemplateTo;
	private VoucherTemplateRequest voucherTemplateRequest = new VoucherTemplateRequest();

	// out
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private boolean success;
	private String message;

	public String createVoucherTemplate() {
		if (AppSysParamsUtil.isVoucherEnabled()) {

			voucherTemplateTo.setSalesFrom(AiradminUtils.getFormattedDate(startDate));
			voucherTemplateTo.setSalesTo(AiradminUtils.getFormattedDate(endDate));

			try {
				ServiceResponce resp = ModuleServiceLocator.getVoucherTemplateBD().createVoucherTemplate(
						voucherTemplateTo);
				success = resp.isSuccess();
				message = resp.getResponseCode();
			} catch (Exception e) {
				log.error("saveVoucherTemplate ", e);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchVoucherTemplates() {
		if (AppSysParamsUtil.isVoucherEnabled()) {
			Page<VoucherTemplateTo> templatesSearchPage = null;
			int start = (page - 1) * 20;
			Map<String, Object> row;

			voucherTemplateRequest.setVoucherName(voucherName);
			if (status != null) {
				voucherTemplateRequest.setStatus(status);
			}

			try {
				if (voucherTemplateRequest != null) {

					templatesSearchPage = ModuleServiceLocator.getVoucherTemplateBD().searchVoucherTemplates(
							voucherTemplateRequest, start, 20);
					this.records = templatesSearchPage.getTotalNoOfRecords();
					this.total = templatesSearchPage.getTotalNoOfRecords() / 20;
					int mod = templatesSearchPage.getTotalNoOfRecords() % 20;
					if (mod > 0) {
						this.total = this.total + 1;
					}

					Collection<VoucherTemplateTo> templates = templatesSearchPage.getPageData();
					rows = new ArrayList<Map<String, Object>>();
					int a = 1;
					for (VoucherTemplateTo template : templates) {
						row = new HashMap<String, Object>();
						row.put("id", ((page - 1) * 20) + a++);
						row.put("template", template);
						rows.add(row);
					}
				}
			} catch (Exception e) {
				log.error("searchVoucherTemplates ", e);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public VoucherTemplateTo getVoucherTemplateTo() {
		return voucherTemplateTo;
	}

	public void setVoucherTemplateTo(VoucherTemplateTo voucherTemplateTo) {
		this.voucherTemplateTo = voucherTemplateTo;
	}

	public VoucherTemplateRequest getVouchersSearchTo() {
		return voucherTemplateRequest;
	}

	public void setVouchersSearchTo(VoucherTemplateRequest voucherTemplateRequest) {
		this.voucherTemplateRequest = voucherTemplateRequest;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public int getTotal() {
		return total;
	}

	public boolean getSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public String getVoucherName() {
		return voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public VoucherTemplateRequest getVoucherTemplateRequest() {
		return voucherTemplateRequest;
	}

	public void setVoucherTemplateRequest(VoucherTemplateRequest voucherTemplateRequest) {
		this.voucherTemplateRequest = voucherTemplateRequest;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

}
