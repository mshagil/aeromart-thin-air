package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ReportingUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_AGENT_INCENTIVE_SCHEME_JSP)
public class ShowAgentIncentiveSchemeReportAction extends BaseRequestResponseAwareAction {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private String searchDateFrm;
	private String searchDateTo;
	private String hdnReportView;
	private String hdnIncentives;
	private String hdnAgents;
	private String radReportOption;
	private String radReportCat;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setReportView();
			return null;
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private void setReportView() throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;
		String id = "UC_REPM_055";
		String SUB_HDR_AGNT_SCM = "Selected Agents with linked Schemes";
		String SUB_HDR_SCHEME_AENT = "Selecetd Schemes assigned to Agents";
		String SUB_HDR_AGNT_ONLY = "Agents not linked with a Scheme";
		String SUB_HDR_SCM_ONLY = "Scheme not linked to Any Agents";
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String image = ReportingUtil.ReportingLocations.image_loc + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String template = "AgentIncentiveReport.jasper";
		String templateReArrage = "AgentIncenRptRearrnge.jasper";
		Map<String, Object> parameters = new HashMap<String, Object>();

		if (isNotEmptyOrNull(this.searchDateFrm)) {
			search.setDateRangeFrom("'" + ReportsHTMLGenerator.convertDate(this.searchDateFrm)
					+ " 00:00:00', 'dd-mon-yyyy HH24:mi:ss'");
		}
		if (isNotEmptyOrNull(this.searchDateTo)) {
			search.setDateRangeTo("'" + ReportsHTMLGenerator.convertDate(searchDateTo) + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss'");
		}
		if (isNotEmptyOrNull(this.hdnIncentives)) {
			String[] arrIncentive = this.hdnIncentives.split(",");
			Collection<String> lstIncentive = new HashSet<String>();
			for (String incentiveId : arrIncentive) {
				lstIncentive.add(incentiveId);
			}
			// List<Object> list = new ArrayList<Object>(set);//FIXME
			search.setSchemeCodes(lstIncentive);
		}
		if (isNotEmptyOrNull(hdnAgents)) {
			String[] arrAgents = this.hdnAgents.split(",");
			Collection<String> lstAgent = new HashSet<String>();
			for (String agentCode : arrAgents) {
				lstAgent.add(agentCode);
			}
			search.setAgents(lstAgent);
		}
		if (isNotEmptyOrNull(radReportCat)) {
			search.setReportType(radReportCat);
		}
		if (isNotEmptyOrNull(radReportCat) && "AGENTSCHEME".equals(radReportCat)) {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(template));
			parameters.put("SUB_HEADER", SUB_HDR_AGNT_SCM);
			parameters.put("SHW_SCHEME", "Y");
		} else if (isNotEmptyOrNull(radReportCat) && "SCHEMEAGENT".equals(radReportCat)) {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(templateReArrage));
			parameters.put("SUB_HEADER", SUB_HDR_SCHEME_AENT);
			parameters.put("SHW_AGENT", "Y");
		} else if (isNotEmptyOrNull(radReportCat) && "AGENTONLY".equals(radReportCat)) {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(template));
			parameters.put("SUB_HEADER", SUB_HDR_AGNT_ONLY);
			parameters.put("SHW_SCHEME", "N");
		} else if (isNotEmptyOrNull(radReportCat) && "SCHEMEONLY".equals(radReportCat)) {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(templateReArrage));
			parameters.put("SUB_HEADER", SUB_HDR_SCM_ONLY);
			parameters.put("SHW_AGENT", "N");
		}

		resultSet = ModuleServiceLocator.getDataExtractionBD().getAgentIncentiveSchemeData(search);

		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", searchDateFrm);
		parameters.put("TO_DATE", searchDateTo);

		if (radReportOption.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null, response);
		} else if (radReportOption.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("EXCEL")) {

			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=AgentIncentiveSchemeReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}

	}

	public String getSearchDateFrm() {
		return searchDateFrm;
	}

	public void setSearchDateFrm(String searchDateFrm) {
		this.searchDateFrm = searchDateFrm;
	}

	public String getSearchDateTo() {
		return searchDateTo;
	}

	public void setSearchDateTo(String searchDateTo) {
		this.searchDateTo = searchDateTo;
	}

	public String getHdnReportView() {
		return hdnReportView;
	}

	public void setHdnReportView(String hdnReportView) {
		this.hdnReportView = hdnReportView;
	}

	public String getHdnAgents() {
		return hdnAgents;
	}

	public void setHdnAgents(String hdnAgents) {
		this.hdnAgents = hdnAgents;
	}

	public String getRadReportOption() {
		return radReportOption;
	}

	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	public String getRadReportCat() {
		return radReportCat;
	}

	public void setRadReportCat(String radReportCat) {
		this.radReportCat = radReportCat;
	}

	public String getHdnIncentives() {
		return hdnIncentives;
	}

	public void setHdnIncentives(String hdnIncentives) {
		this.hdnIncentives = hdnIncentives;
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
