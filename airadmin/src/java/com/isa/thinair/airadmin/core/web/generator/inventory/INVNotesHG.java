package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.auditor.api.model.InventoryAudit;
import com.isa.thinair.commons.api.exception.ModuleException;

public class INVNotesHG {

	private static Log log = LogFactory.getLog(INVNotesHG.class);

	/**
	 * Gets the coolection of Notes
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static String getUserNotesDetails(HttpServletRequest request) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Geting the Inventory  User Notes");
		}
		String strFlightId = request.getParameter("flightId");
		Collection<InventoryAudit> colUserNotes = null;
		if (strFlightId != null && !strFlightId.equals("")) {
			colUserNotes = ModuleServiceLocator.getFlightInventoryBD().getInventoryNotes(Integer.parseInt(strFlightId));
		}

		return createUserNotes(colUserNotes);
	}

	/**
	 * Creates the Grid Array
	 * 
	 * @param colNotes
	 * @return
	 * @throws ModuleException
	 */
	private static String createUserNotes(Collection<InventoryAudit> colNotes) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Iterator<InventoryAudit> iter = null;
		InventoryAudit invAudit = null;
		SimpleDateFormat smft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		int i = 0;
		if (colNotes != null) {
			iter = colNotes.iterator();
			while (iter.hasNext()) {
				invAudit = (InventoryAudit) iter.next();
				sb.append("arrUsernotes [" + i + "] = new Array();");
				sb.append("arrUsernotes [" + i + "][1] = '" + smft.format(invAudit.getAuditTime()) + "';");
				sb.append("arrUsernotes [" + i + "][2] = '" + invAudit.getContent() + "';");
				sb.append("arrUsernotes [" + i + "][3] = '" + invAudit.getUserId() + "';");
				sb.append("arrUsernotes [" + i + "][4] = '" + invAudit.getSegmentCode() + "';");
				sb.append("arrUsernotes [" + i + "][5] = '" + invAudit.getLogicalCCCode() + "';");
				i++;
			}

		}
		return sb.toString();
	}
}