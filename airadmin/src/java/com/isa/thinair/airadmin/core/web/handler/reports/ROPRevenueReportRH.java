package com.isa.thinair.airadmin.core.web.handler.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ROPRevenueReportRH extends BasicRequestHandler {

	private static final String REPORT_VIEW_TYPE_HTML = "HTML";
	private static final String REPORT_VIEW_TYPE_PDF = "PDF";

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static Log log = LogFactory.getLog(ROPRevenueReportRH.class);

	private static int reportWaitTime = 0;

	/**
	 * The constructor.
	 */
	public ROPRevenueReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;
		String reportViewOption = request.getParameter("radReportOption");
		String reportType = request.getParameter("selReportType");
		String timestamp = DateUtil.formatDate(DateUtil.getZuluTime(), "yyMMddHHmmss");

		try {
			setDisplayData(request);
			log.debug("ROPRevenueReportRH success");

		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("ROPRevenueReportRH execute()" + e.getMessage());
		}
		boolean isJapserReport = false;
		if (REPORT_VIEW_TYPE_HTML.equalsIgnoreCase(reportViewOption) || REPORT_VIEW_TYPE_PDF.equalsIgnoreCase(reportViewOption)) {
			// calling differently for HTML/PDF
			isJapserReport = true;
		} else if (ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equals(reportType)) { // Agent summary
																										// is CVS only
																										// jasper
																										// report.
			isJapserReport = true;
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {

				setReportViewAll(request, response, isJapserReport, timestamp);
				forward = AdminStrutsConstants.AdminAction.RES_SUCCESS;
				// if(!isJapserReport){
				// AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
				//
				// if(adminCon.getEnableFTPRevenue().equals("true")) {
				// downloadLocal(request);
				// }
				// }
				setDownloadURL(request, timestamp);
				if (isJapserReport) {
					return null;
				}
			} else {
				log.error("ROPRevenueReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ROPRevenueReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("ROPRevenueReportRH setReportView Failed ", e);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals("download")) {
				setReportResult(request, response, timestamp);
				forward = AdminStrutsConstants.AdminAction.RES_SUCCESS;
			} else {
				log.error("ROPRevenueReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ROPRevenueReportRH download Failed ", e);
		} catch (Exception e) {
			log.error("ROPRevenueReportRH download Failed ", e);
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setReportTypeList(request);
		setAirportList(request);
		setBcHtml(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setStaions(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		setChargesCodeList(request);
		setCarrierCodeList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * create the charges list
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setChargesCodeList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createChargesListWithGroup();
		// String strList = ReportsHTMLGenerator.createChargesList();
		request.setAttribute(WebConstants.REQ_HTML_CHAGRES_CODE_LIST, strList);
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCarrierCodeList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createCarrierCodeHtml(getUserCarrierCodes(request));
		request.setAttribute(WebConstants.REQ_HTML_CARRIER_CODE_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response, boolean isJapserReport,
			String timestamp) throws ModuleException {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");

		String bookedFromDate = request.getParameter("txtBookedFromDate");
		String bookedToDate = request.getParameter("txtBookedToDate");

		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		// ISASUP-2385 : append timestamp to userId.
		if (timestamp != null) {
			strUsrerId += "_" + timestamp;
		}

		String strAgents = request.getParameter("hdnAgents");
		String strBCs = request.getParameter("hdnBCs");
		String strSegments = request.getParameter("hdnSegments");
		String strStations = request.getParameter("hdnStations");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strCarrierCodes = request.getParameter("hdnCarrierCode");
		String strChargesCodes = request.getParameter("hdnChargesCode");
		String strChargesGroupCodes = request.getParameter("hdnChargeGroupCodes");

		String includeCabinClass = request.getParameter("chkCabinClass");

		if (includeCabinClass != null && includeCabinClass.trim().equals("CabinClass")) {
			includeCabinClass = "Y";
		} else {
			includeCabinClass = "N";
		}

		/* get Report Type from jsp */
		String strReportType = request.getParameter("selReportType");

		// To provide Report Format Options
		String strReportFormat = request.getParameter("radRptNumFormat");

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setReportType(ReportsSearchCriteria.REVENUE_TAX_REPORT);
			search.setCos(includeCabinClass);

			/* Default Report : Detailed */
			if (strReportType != null && !strReportType.equals(""))
				search.setReportOption(strReportType);
			else
				search.setReportOption(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_DETAIL);

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if ((bookedFromDate != null && !bookedFromDate.isEmpty()) && (bookedToDate != null && !bookedToDate.isEmpty())) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(bookedFromDate) + " 00:00:00");
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(bookedToDate) + " 23:59:59");
			}

			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}
			if (strAgents != null && !strAgents.equals("")) {
				search.setAgentCode(strAgents);
			}
			if (strBCs != null && !strBCs.equals("")) {
				search.setBookinClass(strBCs);
			}
			if (strSegments != null && !strSegments.equals("")) {
				search.setSegment(strSegments);
			}
			if (strStations != null && !strStations.equals("")) {
				search.setStation(strStations);
			}

			if (strChargesCodes != null && !strChargesCodes.isEmpty()) {
				search.setCharge(strChargesCodes);
			}
			if (strChargesGroupCodes != null && !strChargesGroupCodes.isEmpty()) {
				search.setChargeGrooup(strChargesGroupCodes);
			}
			if (strCarrierCodes != null && !strCarrierCodes.equals("")) {
				search.setCarrierCode(strCarrierCodes);
			} else {
				search.setCarrierCode(CommonUtil.getCollectionElementsStr(getUserCarrierCodes(request)));
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			if (isJapserReport) {
				// setting the parameter to execute the cursor to return a resultset.
				search.setIsCurSerRequired(ReportsSearchCriteria.IS_CUR_REQUIRED_Y);
				// setting to execute in US format only because PDF/HTML are only genegerated in US
				search.setReqReportFormat("");
			}
			// summary reports returns a cursor
			if (ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY.equals(strReportType)
					|| ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY.equals(strReportType)
					|| ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equals(strReportType)) {
				ResultSet rs = ModuleServiceLocator.getDataExtractionBD().getROPRevenueDataforReports(search);
				if (isJapserReport) {
					// if report view only jasper is called
					viewJasperReport(request, response, rs, search, fromDate, toDate);
				}
			} else if (ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_DETAIL.equals(strReportType)) {
				ModuleServiceLocator.getDataExtractionBD().getROPRevenue(search);
			}

		} catch (Exception e) {
			log.error("download method is failed :", e);
		}
	}

	protected static void setReportResult(HttpServletRequest request, HttpServletResponse response, String timestamp)
			throws ModuleException {

		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		// ISASUP-2385 : append timestamp to userId.
		if (timestamp != null) {
			strUsrerId += "_" + timestamp;
		}
		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		String strRevPath = adminCon.getRevenueReportPath();
		String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/reven_rept";

		/*
		 * get Report Type from jsp
		 */

		String strReportType = request.getParameter("selReportType");

		if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY))
			reportsRootDir = reportsRootDir + "_sumry";

		if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY))
			reportsRootDir = reportsRootDir + "_charges_sumry";

		if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY))
			reportsRootDir = reportsRootDir + "_agent_sumry";

		String filePath = "";
		if (adminCon.getEnableFTPRevenue().equals("true")) {
			filePath = reportsRootDir + "_" + strUsrerId + ".csv";
		} else {
			filePath = strRevPath + "_" + strUsrerId + ".csv";
		}

		String fileNameNew = "RevenueAndTaxReport.csv";

		FileInputStream fis = null;
		try {
			File uFile = new File(filePath);
			int fSize = (int) uFile.length();
			log.debug("file size " + fSize);
			fis = new FileInputStream(uFile);
			response.setContentType("application/x-msdownload");
			response.addHeader("Content-Disposition", "attachment; filename=" + fileNameNew);
			response.setContentLength(fSize);

			PrintWriter pw = response.getWriter();
			int c = -1;
			// Loop to read and write bytes.

			while ((c = fis.read()) != -1) {
				pw.print((char) c);
			}
			// Close output and input resources.
			fis.close();
			pw.flush();
			pw = null;

			request.setAttribute("Revresult", "Download Complete");
		} catch (IOException e) {
			log.error("download method is failed :", e);
			request.setAttribute("Revresult", "Download Failed. Please contact system administrator.");
		} catch (Exception e) {
			log.error("download method is failed :", e);
			request.setAttribute("Revresult", "Download Failed. Please contact system administrator.");
		}

	}

	private static void setReportTypeList(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		/* Need to check if selReportType is already selected */

		String strReportType = request.getParameter("selReportType");

		if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY)) {
			sb.append("<option value='RTREOPRT_DETAIL'>Detailed Revnue Report </option>");
			sb.append("<option selected='selected' value='RTREOPRT_SEGMENT_SUMMARY'>Revnue By Segment Summary</option>");
			sb.append("<option value='RTREOPRT_CHARGES_SUMMARY'>Revnue By Charges</option>");
			sb.append("<option value='RTREOPRT_AGENT_SUMMARY'>Revnue By Agent Summary </option>");
		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY)) {
			sb.append("<option value='RTREOPRT_DETAIL'>Detailed Revnue Report </option>");
			sb.append("<option value='RTREOPRT_SEGMENT_SUMMARY'>Revnue By Segment Summary</option>");
			sb.append("<option selected='selected' value='RTREOPRT_CHARGES_SUMMARY'>Revnue By Charges</option>");
			sb.append("<option value='RTREOPRT_AGENT_SUMMARY'>Revnue By Agent Summary </option>");

		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY)) {
			sb.append("<option value='RTREOPRT_DETAIL'>Detailed Revnue Report </option>");
			sb.append("<option value='RTREOPRT_SEGMENT_SUMMARY'>Revnue By Segment Summary</option>");
			sb.append("<option value='RTREOPRT_CHARGES_SUMMARY'>Revnue By Charges</option>");
			sb.append("<option selected='selected' value='RTREOPRT_AGENT_SUMMARY'>Revnue By Agent Summary </option>");

		} else {
			sb.append("<option selected='selected' value='RTREOPRT_DETAIL'>Detailed Revnue Report </option>");
			sb.append("<option value='RTREOPRT_SEGMENT_SUMMARY'>Revnue By Segment Summary</option>");
			sb.append("<option value='RTREOPRT_CHARGES_SUMMARY'>Revnue By Charges</option>");
			sb.append("<option value='RTREOPRT_AGENT_SUMMARY'>Revnue By Agent Summary </option>");
		}

		// String strHtml = SelectListGenerator.createAirportCodeList();

		String strHtml = sb.toString();

		request.setAttribute("reqReportTypeList", strHtml);
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	public static void setBcHtml(HttpServletRequest request) throws ModuleException {
		String strBcList = JavascriptGenerator.createBookingCodeHtml();
		request.setAttribute(WebConstants.REQ_HTML_BC_DATA, strBcList);
	}

	public static void setStaions(HttpServletRequest request) throws ModuleException {
		String strstnList = JavascriptGenerator.createStationsByCountry();
		request.setAttribute(WebConstants.REQ_HTML_STN_DATA, strstnList);
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setDownloadURL(HttpServletRequest request, String timestamp) throws Exception {
		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		// ISASUP-2385 : append timestamp to userId.
		if (timestamp != null) {
			strUsrerId += "_" + timestamp;
		}

		String strurl = globalConfig.getBizParam(SystemParamKeys.REPORT_URL_PREFIX) + "reven_rept";

		/*
		 * Get Report Type from RequestMake RevPath base on ReportTypePlease see Procedure output File Name for Ref
		 */
		String strReportType = request.getParameter("selReportType");

		/* Before D/L Page Display, We need to set ReportType; so user can download proper file */
		request.setAttribute("selReportType", strReportType);

		if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY)) {
			strurl = strurl + "_summary";
		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY)) {
			strurl = strurl + "_charges_summary";
		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY)) {
			strurl = strurl + "_agent_summary";
		}
		strurl = strurl + "_" + strUsrerId + ".zip";
		waitForReportToGenerate(strurl);
		request.setAttribute("reqReptURl", strurl);

	}

	private static void waitForReportToGenerate(String url) {

		if (AppSysParamsUtil.isReportWaitingEnabled()) {
			try {
				URL netUrl = new URL(url);
				HttpURLConnection httpCon = (HttpURLConnection) netUrl.openConnection();
				int responseCode = httpCon.getResponseCode();

				if (responseCode != 404 || reportWaitTime >= AppSysParamsUtil.reportTimeOut()) {
					reportWaitTime = 0;
					return;
				} else {
					Thread.sleep(AppSysParamsUtil.reportWaitInterval());
					reportWaitTime += AppSysParamsUtil.reportWaitInterval();
					waitForReportToGenerate(url);
				}
			} catch (Exception e) {
				log.error("Error while waiting for report", e);
				
				try {
					Thread.sleep(AppSysParamsUtil.reportWaitInterval());
				} catch (Exception ex) {
					log.error("Error in Secondary waiting for report", ex);
				}
			}
		}
	}
	private static void downloadLocal(HttpServletRequest request) throws Exception {

		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		String strRevPath = adminCon.getRevFTPPath();

		/*
		 * Get Report Type from RequestMake RevPath base on ReportTypePlease see Procedure output File Name for Ref
		 */
		String strReportType = request.getParameter("selReportType");

		/* Before D/L Page Display, We need to set ReportType; so user can download proper file */
		request.setAttribute("selReportType", strReportType);

		String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/reven_rept";

		if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY)) {
			strRevPath = strRevPath + "_summary";
			reportsRootDir = reportsRootDir + "_sumry";
		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY)) {
			strRevPath = strRevPath + "_charges_sumry";
			reportsRootDir = reportsRootDir + "_charges_sumry";
		} else if (strReportType != null
				&& strReportType.equalsIgnoreCase(ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY)) {
			strRevPath = strRevPath + "_agent_summary";
			reportsRootDir = reportsRootDir + "_agent_sumry";
		}

		strRevPath = strRevPath + "_" + strUsrerId + ".csv";
		reportsRootDir = reportsRootDir + "_" + strUsrerId + ".csv";

		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		// Code to FTP
		serverProperty.setServerName(adminCon.getRptFTPServer());
		if (adminCon.getRptFTPUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(adminCon.getRptFTPUserName());
		}

		if (adminCon.getRptFTPPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(adminCon.getRptFTPPassword());
		}
		boolean downloaded = false;

		try {
			if (ftpUtil.connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(),
					serverProperty.getPassword())) {
				ftpUtil.binary();
				downloaded = ftpUtil.downloadFile(strRevPath, reportsRootDir);
				if (downloaded) {
					downloaded = new File(reportsRootDir).exists();
				}
			}
		} catch (Exception e) {
			log.error("download attachment failed [" + e.getMessage() + "]", e);
		} finally {
			try {
				if (ftpUtil.isConnected()) {
					ftpUtil.disconnect();
				}
			} catch (Exception e) {
				log.error("Disconnecting FTP connection failed.", e);
			}
		}

	}

	private static void viewJasperReport(HttpServletRequest request, HttpServletResponse response, ResultSet resultSet,
			ReportsSearchCriteria search, String fromDate, String toDate) throws ModuleException {
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		String strReportType = request.getParameter("selReportType");
		String strReportId = "UC_REPM_006";
		String reportTemplate = "";
		// Identifying Which report to Display
		if (ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_SEGMENT_SUMMARY.equalsIgnoreCase(strReportType)) {
			reportTemplate = "RevenueBySegmentSummeryReport.jasper";
		} else if (ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_CHARGES_SUMMARY.equalsIgnoreCase(strReportType)) {
			reportTemplate = "ChargesSummary.jasper";
		} else if (ReportsSearchCriteria.REVENUE_TAX_REPORT_OPTION_AGENT_SUMMARY.equalsIgnoreCase(strReportType)) {
			reportTemplate = "RevenueByAgentSummeryReport.jasper";
		} else {
			// throw ex
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_ID", strReportId);
		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		parameters.put("LOCALE", "US");
		parameters.put("DELIMITER", ",");
		if (value.trim().equals(WebConstants.REPORT_HTML)) {
			parameters.put("IMG", strPath);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null,
					null, response);
		} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
			strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", strPath);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.csv", reportTemplate));
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		}
	}
}
