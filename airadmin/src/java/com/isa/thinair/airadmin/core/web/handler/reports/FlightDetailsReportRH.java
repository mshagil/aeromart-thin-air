package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class FlightDetailsReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(FlightDetailsReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public FlightDetailsReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("FlightDetailsReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("FlightDetailsReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("FlightDetailsReportRH setReportView Success");
				return null;
			} else {
				log.error("FlightDetailsReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("FlightDetailsReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setStatusHtml(request);
		setOperationTypeHtml(request);
		setStationComboList(request);
		setAirCraftModel(request);
		setFlightStatus(request);
		setLiveStatus(request);
		// setFlightNoList(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);

	}

	protected static void setAirCraftModel(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createAircraftModelList_SG();
		request.setAttribute(WebConstants.REQ_MODELS, strStation);
	}

	protected static void setFlightStatus(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createFlightStatus();
		request.setAttribute(WebConstants.REQ_FLIGHT_STATUS, strStation);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createAirportsList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */
	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightStatus();
		request.setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);
	}

	protected static void setFlightNoList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createFlightNoList();
		request.setAttribute(WebConstants.REQ_FLIGHT_NO_LIST, strStation);
	}

	/**
	 * sets the Operation Type list
	 * 
	 * @param request
	 */
	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);

	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");

		String operationType = request.getParameter("selOperationType");
		String flightNo = request.getParameter("selFlightNumber");
		String from = request.getParameter("selFrom");
		String to = request.getParameter("selTo");
		String model = request.getParameter("selAircraftModel");
		String status = request.getParameter("selFlightStatus");
		String time = request.getParameter("radTimeIn");
		String opTypeText = request.getParameter("hdnOpType");
		String reportTemplate = "FlightDetailsReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strReportId = "UC_REPM_020";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			search.setFlightNumber(flightNo);
			search.setOperationType(operationType);
			search.setFlightNumber(flightNo);
			search.setFlightStatus(status);
			search.setTimeZone(time);
			search.setSectorFrom(from);
			search.setSectorTo(to);
			search.setModel(model);
			resultSet = ModuleServiceLocator.getDataExtractionBD().getFlightDetails(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORT_ID", strReportId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			if (isNotnull(operationType))
				parameters.put("FLT_TYPE", opTypeText);
			if (isNotnull(flightNo))
				parameters.put("FLT_NUM", flightNo);
			if (isNotnull(from))
				parameters.put("FLT_DEPT", from);
			if (isNotnull(to))
				parameters.put("FLT_ARRIVAL", to);
			if (isNotnull(model))
				parameters.put("FLT_MODEL", model);
			if (time.equals("Zulu")) {
				parameters.put("REPT_DEPTTIME", "Zulu");
			} else {
				parameters.put("REPT_DEPTTIME", "Local");
			}
			parameters.put("START_DATE", fromDate);
			parameters.put("STOP_DATE", toDate);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=FlightDetailsReport.pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlightDetailsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlightDetailsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static boolean isNotnull(String str) {
		boolean blnCond = false;
		if (str == null || str.trim().equals("Select")) {
			blnCond = false;
		} else {
			blnCond = true;
		}
		return blnCond;
	}
}
