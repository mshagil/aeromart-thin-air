package com.isa.thinair.airadmin.core.web.v2.handler.inventory;

import java.util.Map;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.dto.inventory.LogicalCabinClassDTO;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public final class LogicalCabinClassRH extends BasicRequestHandler {

	public static Page<LogicalCabinClass> searchCabinClass(int pageNo, LogicalCabinClassDTO searchParams) throws ModuleException {
		return ModuleServiceLocator.getLogicalCabinClassServiceBD().getLogicalCabinClass(searchParams, (pageNo - 1) * 20, 20);
	}

	public static void saveTemplate(LogicalCabinClass logicalCabinClass, String chkNestShift) throws ModuleException {
		ModuleServiceLocator.getLogicalCabinClassServiceBD().saveTemplate(logicalCabinClass, chkNestShift);
	}

	public static boolean deleteLogicalCabinClass(LogicalCabinClass logicalCabinClass) throws ModuleException {
		return ModuleServiceLocator.getLogicalCabinClassServiceBD().deleteLogicalCabinClass(logicalCabinClass) == 0;
	}

	/**
	 * Returns all the language translations for LogicalCabinClass
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public static Map<String, Map<String, String>> getTranslatedMessages() throws ModuleException {
		return ModuleServiceLocator.getLogicalCabinClassServiceBD().getTranslatedMessages();
	}

	/**
	 * Updates and saves new translations.
	 * 
	 * @param translations
	 *            A map in the form of Map<MessageKey,Map<Locale,Translated Messages>>
	 * @throws ModuleException
	 *             If any of the underlying operations throw an exception.
	 */
	public static void saveTranslations(Map<String, Map<String, String>> translations) throws ModuleException {
		ModuleServiceLocator.getLogicalCabinClassServiceBD().saveTranslations(translations);
	}
}