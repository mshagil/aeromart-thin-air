package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.BookingCountDetailsRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * 
 * @author asiri
 * 
 */

@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_BOOKING_COUNT_DETAIL_REPORT_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowBookingCountDetailsReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		return BookingCountDetailsRH.execute(request, response);
	}
}
