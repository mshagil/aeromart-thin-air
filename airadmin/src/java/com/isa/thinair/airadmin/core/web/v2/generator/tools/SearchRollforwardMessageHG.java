package com.isa.thinair.airadmin.core.web.v2.generator.tools;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SearchRollforwardMessageHG {
	
	private static String templateclientErrors;

	public static String getTemplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.seatinventory.form.rollforward.fromdate.required", "rollForwardFromDateRqrd");
			moduleErrs.setProperty("um.seatinventory.form.rollforward.todate.required", "rollForwardToDateRqrd");
			moduleErrs.setProperty("um.seatinventory.form.rollforward.todate.notlessthan.fromdate",
					"rollForwardFromDateLessthanFromDate");
			
			templateclientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return templateclientErrors;
	}

}
