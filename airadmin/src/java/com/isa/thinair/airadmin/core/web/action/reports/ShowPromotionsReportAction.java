package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.reports.PromotionFlexiDateSummaryReportRH;
import com.isa.thinair.airadmin.core.web.handler.reports.PromotionNextSeatFreeSummaryForCrewReportRH;
import com.isa.thinair.airadmin.core.web.handler.reports.PromotionNextSeatFreeSummaryReportRH;
import com.isa.thinair.airadmin.core.web.handler.reports.PromotionRequestsSummaryReportRH;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_PROMOTIONS_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowPromotionsReportAction extends BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(ShowPromotionsReportAction.class);

	public String execute() {

		String promoReportType = request.getParameter("selPromoReportType");
		if (promoReportType != null) {
			if (WebConstants.PromotionReportsType.PROMOTION_REQUEST_SUMMARY.equals(promoReportType)) {
				return PromotionRequestsSummaryReportRH.execute(request, response);
			} else if (WebConstants.PromotionReportsType.PROMO_NEXT_SEAT_FREE_SUMMARY.equals(promoReportType)) {
				return PromotionNextSeatFreeSummaryReportRH.execute(request, response);
			} else if (WebConstants.PromotionReportsType.PROMO_FLEXI_DATE_SUMMARY.equals(promoReportType)) {
				return PromotionFlexiDateSummaryReportRH.execute(request, response);
			} else if (WebConstants.PromotionReportsType.PROMO_NEXT_SEAT_FREE_SUMMARY_FOR_CREW.equals(promoReportType)) {
				return PromotionNextSeatFreeSummaryForCrewReportRH.execute(request, response);
			}
		} else {
			try {
				String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

				if (strClientErrors == null) {
					strClientErrors = "";
				}
				request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
			} catch (ModuleException moduleException) {
				log.error("setClientErrors failed : ", moduleException);
				return AdminStrutsConstants.AdminAction.ERROR;
			}
			return AdminStrutsConstants.AdminAction.SUCCESS;
		}

		return AdminStrutsConstants.AdminAction.ERROR;
	}

}
