/**
 * 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.PassengerUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Manjula
 * 
 */
public class PAXStatusReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(PAXStatusReportRH.class);

	private static final String PAX_STATUS_REPORT = "PaxStatusReport.jasper";

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static final String NOSHOW_STATUS = "NOSHOW";
	private static final String GOSHOW_STATUS = "GOSHOW";
	private static final String FLOWN_STATUS = "FLOWN";
	private static final String NOREC_STATUS = "NOREC";
	private static final String CNF_STATUS = "CNF";
	private static final String OHD_STATUS = "OHD";
	private static final String ALL_STATUS = "ALL";
	private static final String CNX_STATUS = "CNX";
	private static final String NOSHOW_STS = "N";
	private static final String GOSHOW_STS = "G";
	private static final String FLOWN_STS = "F";
	private static final String CONFIRMED = "C";
	private static final String NO_REC = "R";

	/**
	 * Main Execute Method for Audit Report Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			log.debug("PAXStatusReportRH setDisplayData() SUCCESS");
		} catch (Exception e) {
			log.error("PAXStatusReportRH setDisplayData() FAILED " + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("PAXStatusReportRH setReportView Success");
				return null;
			} else {
				log.error("PAXStatusReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("PAXStatusReportRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Display data For Security Loging Audit Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {

		setAirportComboList(request);
		setClientErrors(request);
		PassengerUtil.setPAXStatus(request);
		PassengerUtil.setPFSStatus(request);
		setDisplayAgencyMode(request);
		setGSAMultiSelectList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAirportComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Displays the PAX status Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		Map<String, Integer> paxCountMap = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String flightNumber = request.getParameter("txtFlightNumber");
		String selDept = request.getParameter("selDept");
		String selArrival = request.getParameter("selArival");
		String selPaxStatus = request.getParameter("selPAXStatus");
		String selPfsStatus = request.getParameter("selPFSStatus");

		String value = request.getParameter("radReportOption");

		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		String showFoid = globalConfig.getBizParam(SystemParamKeys.SHOW_PASSPORT_IN_ITINERARY);

		String agents = "";
		ArrayList<String> agentCol = new ArrayList<String>();
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		if (agents != null) {
			String agentArr[] = agents.split(",");
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
		}

		showFoid = (showFoid == null) ? "" : showFoid;

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			search.setFlightNumber(flightNumber);
			search.setFrom(selDept);
			search.setTo(selArrival);
			search.setStatus(selPaxStatus);
			search.setAgents(agentCol);
			if ((selPfsStatus != null) && !("".equals(selPfsStatus))) {
				if (selPfsStatus.equals(PassengerUtil.NOSHOW_STATUS)) {
					search.setPfsStatus(PassengerUtil.NOSHOW_STS);
				}
				if (selPfsStatus.equals(PassengerUtil.GOSHOW_STATUS)) {
					search.setPfsStatus(PassengerUtil.GOSHOW_STS);
				}
				if (selPfsStatus.equals(PassengerUtil.FLOWN_STATUS)) {
					search.setPfsStatus(PassengerUtil.FLOWN_STS);
				}
				if (selPfsStatus.equals(PassengerUtil.NOREC_STATUS)) {
					search.setPfsStatus(PassengerUtil.NO_REC);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getPaxStatusReport(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			// parameters.put("USER_ID", userId);
			parameters.put("REPORT_TYPE", value);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("ID", "UC_REPM_065");
			parameters.put("SHOW_COS", "Y");

			if (showFoid.equalsIgnoreCase("Y")) {
				parameters.put("SHOW_FOID", "Y");
			} else {
				parameters.put("SHOW_FOID", "N");
			}

			if (AppSysParamsUtil.isShowPaxETKT()) {
				parameters.put("SHOW_ETKT", "Y");
			}
			
			reportTemplate = PAX_STATUS_REPORT;

			paxCountMap = ModuleServiceLocator.getDataExtractionBD().getPaxCountForPaxStatusReport(search);

			if (!paxCountMap.isEmpty()) {

				Integer totalPaxCount = 0;

				for (Entry<String, Integer> entry : paxCountMap.entrySet()) {
					if ("Male".equals(entry.getKey())) {
						parameters.put("MALE_COUNT", Integer.toString(entry.getValue()));
					} else if ("Female".equals(entry.getKey())) {
						parameters.put("FEMALE_COUNT", Integer.toString(entry.getValue()));
					} else if ("Child".equals(entry.getKey())) {
						parameters.put("CHILD_COUNT", Integer.toString(entry.getValue()));
					} else if ("Infant".equals(entry.getKey())) {
						parameters.put("INFANT_COUNT", Integer.toString(entry.getValue()));
					} else {
						parameters.put("UNKNOWN_COUNT", Integer.toString(entry.getValue()));
					}
					totalPaxCount += entry.getValue();
				}
				parameters.put("TOTAL_COUNT", Integer.toString(totalPaxCount));
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);

			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				response.addHeader("Content-Disposition", "attachment;filename=PaxStatusReport.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.addHeader("Content-Disposition", "attachment;filename=PaxStatusReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=PaxStatusReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error("setReportView :: ", e);
		}
	}

	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, false, false);
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);

	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.res.pax.all") != null) {
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "0");
		}
	}
}
