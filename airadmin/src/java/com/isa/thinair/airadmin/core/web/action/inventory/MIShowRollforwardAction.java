/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.action.inventory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.inventory.MIRollforwardRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.SEAT_ALLOC_INV_ROLL_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS_ADVANCE, value = AdminStrutsConstants.AdminJSP.ROLLFORWARD_CONFIRMATION_PAGE),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class MIShowRollforwardAction extends BaseRequestAwareAction { 

	private static final Log log = LogFactory.getLog(MIShowRollforwardAction.class);

	public String execute() throws Exception {
		log.debug("Inside the ShowRollAllocationAction - execute()");
		return MIRollforwardRequestHandler.execute(request);
	}

}
