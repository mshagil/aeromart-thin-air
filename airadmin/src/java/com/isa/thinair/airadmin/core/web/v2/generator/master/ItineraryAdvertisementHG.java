package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ItineraryAdvertisementHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for Advertisement Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.itinerary.form.title.required", "advertisementTitleRqrd");
			moduleErrs.setProperty("um.itinerary.form.language.required", "languageRqrd");
			moduleErrs.setProperty("um.itinerary.form.banner.required", "bannerRqrd");
			moduleErrs.setProperty("um.itinerary.form.status.required", "statusRqrd");
			moduleErrs.setProperty("um.itinerary.form.remarks.required", "remarksRqrd");
			moduleErrs.setProperty("um.itinerary.form.segment.required", "segmentRqrd");
			moduleErrs.setProperty("um.itinerary.form.ond.invalid", "invalidOND");
			moduleErrs.setProperty("um.itinerary.form.depatureRequired.required", "depatureRequired");
			moduleErrs.setProperty("um.itinerary.form.arrivalRequired.required", "arrivalRequired");
			moduleErrs.setProperty("um.itinerary.form.depatureArriavlSame.Same", "depatureArriavlSame");
			moduleErrs.setProperty("um.itinerary.form.arriavlViaSame.Same", "arriavlViaSame");
			moduleErrs.setProperty("um.itinerary.form.depatureViaSame.Same", "depatureViaSame");
			moduleErrs.setProperty("um.itinerary.form.vianotinSequence.notin", "vianotinSequence");
			moduleErrs.setProperty("um.itinerary.form.via.same", "viaSame");
			moduleErrs.setProperty("um.itinerary.form.file.pixel", "pixelNotAllowed");
			moduleErrs.setProperty("um.itinerary.form.file.size", "sizeNotAllowed");
			moduleErrs.setProperty("um.itinerary.search.OND", "originDestinationSame");
			moduleErrs.setProperty("um.itinerary.grid.checkBox", "selCheckBox");
			moduleErrs.setProperty("um.itinerary.grid.checkBox.reload", "reloadPageForCheckBox");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
