/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.ETLProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public final class ETLProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ETLProcessingRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_DOWNLOAD_DATE = "txtDownloadTS";
	private static final String PARAM_FLIGHT_NO = "txtFlightNo";
	private static final String PARAM_FLIGHT_DATE = "txtFlightDateTime";
	private static final String PAMAM_FLIGHT_TIME = "txFlightTime";
	private static final String PARAM_DESTINATION = "selDest";
	private static final String PARAM_FROM_ADDRESS = "txFromAddress";
	private static final String PARAM_PROCESSING_STATUS = "selStatus";
	private static final String FALSE = "false";

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For ETL Processing & Sets the Success int 0-Not Applicable, 1-ETL process Success, 2-ETL save
	 * Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			String hdnMode = request.getParameter("hdnMode");
			if (hdnMode != null) {
				if (hdnMode.equals("PROCESS")) {
					String etlId = request.getParameter("hdnETLID");
					ModuleServiceLocator.getEtlBD().reconcileETLReservations(new Integer(etlId),
							AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
				} else if (hdnMode.equals("SAVE")) {
					saveETLEntry(request);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
				}
			}
		} catch (ModuleException me) {
			saveMessage(request, airadminConfig.getMessage(me.getExceptionCode()), WebConstants.MSG_ERROR);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		try {
			setDisplayData(request);
		} catch (Exception e) {
			forward = WebConstants.FORWARD_ERROR;
			JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
		}
		return forward;
	}

	/**
	 * Sets the Display Data for ETL Process Data sets the Success int for the form 0-Not Applicable, 1-ETL process
	 * Success, 2-ETL save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setETLProcessingRowHtml(request);
		setAgentList(request);
		setAirportList(request);
		setClientErrors(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	/**
	 * Sets the Active Online Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Agents List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAgentList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAgentCodes();
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("ETLProcessingRequestHandler.setAgentList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Set the ETL Details Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setETLProcessingRowHtml(HttpServletRequest request) throws Exception {

		try {
			ETLProcessingHTMLGenerator htmlGen = new ETLProcessingHTMLGenerator();
			String strHtml = htmlGen.getETLProcessingRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());

		} catch (ModuleException moduleException) {
			log.error("ETLProcessingHandler.setETLProcessingRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setClientErrors(HttpServletRequest request) throws Exception {
		String strHtml = ETLProcessingHTMLGenerator.getClientErrors();
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strHtml);
	}

	private static void saveETLEntry(HttpServletRequest request) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String flightNumber = request.getParameter(PARAM_FLIGHT_NO);
		String fromAddress = request.getParameter(PARAM_FROM_ADDRESS);
		String fromAirport = request.getParameter(PARAM_DESTINATION);
		String flightDate = request.getParameter(PARAM_FLIGHT_DATE);
		String flightTime = request.getParameter(PAMAM_FLIGHT_TIME);
		String status = request.getParameter(PARAM_PROCESSING_STATUS);
		boolean flightDateOnly = true;
		if (flightTime != "") {
			flightDateOnly = false;
		}
		Date departureDate = dateFormat.parse(flightDate + ((flightTime != "") ? (" " + flightTime) : " 00:00"));
		Date downloadDate = dateFormat.parse(request.getParameter(PARAM_DOWNLOAD_DATE) + " 00:00");

		boolean hasETL = ModuleServiceLocator.getEtlBD().hasETL(flightNumber, departureDate, fromAirport, null);
		if (!hasETL) {
			List<FlightSegement> flightSegList = (List<FlightSegement>) ModuleServiceLocator.getFlightServiceBD()
					.getFlightSegmentsForLocalDate(fromAirport, null, flightNumber, departureDate, flightDateOnly);
			if (FALSE.equals(AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture())
					&& CalendarUtil.isLessThan(CalendarUtil.getCurrentSystemTimeInZulu(), flightSegList.get(0)
							.getEstTimeDepatureLocal())) {
				throw new ModuleException("um.process.form.downloadTS.lessthanFlightTs");
			}
			if (flightSegList.size() == 1) {
				ETL etl = new ETL();
				etl.setDateDownloaded(downloadDate);
				etl.setFlightNumber(flightNumber);
				etl.setDepartureDate(flightSegList.get(0).getEstTimeDepatureLocal());
				etl.setFromAddress(fromAddress);
				etl.setFromAirport(fromAirport);
				etl.setCarrierCode(AppSysParamsUtil.getCarrierCode());
				etl.setVersion(-1);
				etl.setProcessedStatus(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
				ModuleServiceLocator.getEtlBD().saveETLEntry(etl);
			} else {
				throw new ModuleException("um.process.form.pfs.no.flight");
			}
		} else {
			throw new ModuleException("um.airadmin.etl.already.exists");
		}
	}
}
