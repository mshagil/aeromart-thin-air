package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.ParamHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ParamRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ParamRequestHandler.class);

	private static final String PARAM_CARRIER_CODE = "selCCode";

	private static final String PARAM_PARAMETER_NAME = "txtParameterName";

	private static final String PARAM_PARAMETER_KEY = "txtParamKey";

	private static final String PARAM_PARAMETER_VALUE = "txtParameterValue";

	private static final String PARAM_PARAMETER_TYPE = "selType";

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_ACTION = "hdnAction";

	private static final String PARAM_EDITABLE = "hdnEditable";

	private static final String PARAM_RECNO = "hdnRecNo";

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_GRIDDATA = "hdnGridData";

	private static final String PARAM_SEARCH_PARAM_KEY = "selParamKey";

	private static final String PARAM_SEARCH_PARAM_NAME = "selParameterName";

	private static final String PARAM_SEARCH_CARRIER_CODE = "selCarrierCode";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	/**
	 * Main Execute Method for User Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String strHdnAction = request.getParameter(PARAM_ACTION);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		String strFormData = "var saveSuccess = 0; ";
		strFormData += "var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");

		try {

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {

				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					checkPrivilege(request, WebConstants.PRIV_SYS_PARAM);
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					checkPrivilege(request, WebConstants.PRIV_SYS_PARAM);
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}

				Properties props = getProperties(request);

				saveData(props, request);

				if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);

					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				} else {
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				}
				strFormData = "var saveSuccess = 1; ";
				strFormData += "var arrFormData = new Array();";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SYNC_ALL)) {
				syncAllAppParamsToCache();
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in ParamRequestHandler.execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, airadminConfig.getMessage("um.user.form.id.dupkey"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
			if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
				setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
			} else {
				setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
			}
			// isExceptionOccured = true;
			setExceptionOccured(request, true);

			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException me) {
				log.error("Exception in ParamRequestHandler.execute() [origin module=" + me.getModuleDesc() + "]", me);
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		} catch (Exception exception) {
			log.error("Exception in ParamRequestHandler.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
			setExceptionOccured(request, true);
			if (strHdnAction != null && (strHdnAction.equals(WebConstants.ACTION_EDIT))) {
				setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
			} else {
				setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
			}
			try {
				strFormData = fillEnteredFormData(request);
			} catch (ModuleException me) {
				log.error("Exception in ParamRequestHandler.execute() [origin module=" + me.getModuleDesc() + "]", me);
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			log.error("Exception in ParamRequestHandler.execute() [origin module=" + e.getModuleDesc() + "]", e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		return forward;
	}

	/**
	 * Saves the Parameter Data
	 * 
	 * @param props
	 *            the Property file Containg User Request values
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void saveData(Properties props, HttpServletRequest request) throws ModuleException {

		BusinessSystemParameter param = ModuleServiceLocator.getCommonServiceBD().getBusinessSystemParameter(
				props.getProperty(PARAM_CARRIER_CODE), props.getProperty(PARAM_PARAMETER_KEY));

		if (param != null && props.getProperty(PARAM_ACTION).equals(WebConstants.ACTION_EDIT)) {
			param.setParamValue(props.getProperty(PARAM_PARAMETER_VALUE));
		} else if (props.getProperty(PARAM_ACTION) != null && props.getProperty(PARAM_ACTION).equals(WebConstants.ACTION_ADD)) {
			if (param != null) {
				throw new ModuleException("module.duplicate.key", MessagesUtil.getMessage("module.duplicate.key"));
			}
			param = new BusinessSystemParameter();
			param.setCarrierCode(props.getProperty(PARAM_CARRIER_CODE));
			param.setDescription(props.getProperty(PARAM_PARAMETER_NAME));
			param.setEditable("Y");
			param.setMaxLengthParamVal(props.getProperty(PARAM_PARAMETER_VALUE).length());
			param.setParamKey(props.getProperty(PARAM_PARAMETER_KEY));
			param.setParamType(props.getProperty(PARAM_PARAMETER_TYPE));
			param.setParamValue(props.getProperty(PARAM_PARAMETER_VALUE));
		}

		String appCode = WebConstants.APP_CODE;

		ModuleServiceLocator.getCommonServiceBD().saveBusinessSystemParameter(param, appCode);
		param = null;

	}
	
	private static void syncAllAppParamsToCache() throws ModuleException {
		if (CommonsServices.getGlobalConfig().isCacheAppParamsInCachingDB()
				&& CommonsServices.getAerospikeCachingService().isCachingDBEnabled()) {
			CommonsServices.getAerospikeCachingService().syncAllAppParamsToCache();
		}
	}

	/**
	 * Sets the Display Data For User Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
		setCarrierHtml(request);
		setParamNameHtml(request);
		setParamKeyHtml(request);
		setParamTypeHtml(request);
		setParamRowHtml(request);
		setRequestHtml(request);
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

	}

	/**
	 * Sets the User Client validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {

		String strClientErrors = ParamHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null)
			strClientErrors = "";
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	public static void setCarrierHtml(HttpServletRequest request) throws ModuleException {

		String str = SelectListGenerator.createParamCarrierCodeList();
		request.setAttribute(WebConstants.REQ_HTML_PARAM_CC_LIST, str);

	}

	public static void setParamNameHtml(HttpServletRequest request) throws ModuleException {

		String str = SelectListGenerator.createParamDescriptionList();
		request.setAttribute(WebConstants.REQ_HTML_PARAM_NAME_LIST, str);

	}

	public static void setParamKeyHtml(HttpServletRequest request) throws ModuleException {

		String str = SelectListGenerator.createParamKeyList();
		request.setAttribute(WebConstants.REQ_HTML_PARAM_KEY_LIST, str);

	}

	public static void setParamTypeHtml(HttpServletRequest request) throws ModuleException {

		String str = SelectListGenerator.createParamTypeList();
		request.setAttribute(WebConstants.REQ_HTML_PARAM_TYPE_LIST, str);

	}

	/**
	 * Sets User Grid Array to request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setParamRowHtml(HttpServletRequest request) throws ModuleException {

		ParamHTMLGenerator htmlGen = new ParamHTMLGenerator(getProperties(request));
		String strHtml = htmlGen.getParamRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_PARAM_DATA, strHtml);
	}

	/**
	 * Sets the User Request To a Property File
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the File Containg User Request Values
	 */
	protected static Properties getProperties(HttpServletRequest request) {

		Properties props = new Properties();

		String carrierCode = request.getParameter(PARAM_CARRIER_CODE);
		String parameterName = request.getParameter(PARAM_PARAMETER_NAME);
		String parameterKey = request.getParameter(PARAM_PARAMETER_KEY);
		String parameterValue = request.getParameter(PARAM_PARAMETER_VALUE);
		String parameterType = request.getParameter(PARAM_PARAMETER_TYPE);

		String strMode = request.getParameter(PARAM_MODE);
		String strhdnAction = request.getParameter(PARAM_ACTION);
		String strHdneditable = request.getParameter(PARAM_EDITABLE);
		String strRecNo = request.getParameter(PARAM_RECNO);
		String strSearchData = request.getParameter(PARAM_SEARCHDATA);
		String strGridData = request.getParameter(PARAM_GRIDDATA);

		String searchDataArr[] = null;

		props.setProperty(PARAM_CARRIER_CODE, AiradminUtils.getNotNullString(carrierCode));
		props.setProperty(PARAM_PARAMETER_KEY, AiradminUtils.getNotNullString(parameterKey).toUpperCase());
		props.setProperty(PARAM_PARAMETER_NAME, AiradminUtils.getNotNullString(parameterName));
		props.setProperty(PARAM_PARAMETER_VALUE, AiradminUtils.getNotNullString(parameterValue));
		props.setProperty(PARAM_PARAMETER_TYPE, AiradminUtils.getNotNullString(parameterType));
		props.setProperty(PARAM_ACTION, AiradminUtils.getNotNullString(strhdnAction));
		props.setProperty(PARAM_EDITABLE, AiradminUtils.getNotNullString(strHdneditable));
		props.setProperty(PARAM_SEARCHDATA, AiradminUtils.getNotNullString(strSearchData));
		props.setProperty(PARAM_GRIDDATA, AiradminUtils.getNotNullString(strGridData));
		props.setProperty(PARAM_RECNO, (strRecNo == null) ? "1" : strRecNo);
		props.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(strMode));

		if (strSearchData != null && !strSearchData.equals("")) {
			searchDataArr = strSearchData.split(",");
		}

		props.setProperty(PARAM_SEARCH_CARRIER_CODE, ((searchDataArr != null && !strSearchData.equals("")) ? searchDataArr[0]
				: ""));
		props.setProperty(PARAM_SEARCH_PARAM_NAME, ((searchDataArr != null && !strSearchData.equals("")) ? searchDataArr[1] : ""));
		props.setProperty(PARAM_SEARCH_PARAM_KEY, ((searchDataArr != null && !strSearchData.equals("")) ? searchDataArr[2] : ""));

		return props;
	}

	/**
	 * Sets the Form Data to be Used in User Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strFormData
	 * @return
	 * @throws ModuleException
	 */
	private static String fillEnteredFormData(HttpServletRequest request) throws ModuleException {
		Properties props = getProperties(request);

		StringBuilder sbf = new StringBuilder("var arrFormData = new Array();");
		sbf.append("arrFormData[0] = new Array();");
		sbf.append("arrFormData[0][1] = '" + props.getProperty(PARAM_CARRIER_CODE) + "';");
		sbf.append("arrFormData[0][2] = '" + props.getProperty(PARAM_PARAMETER_NAME) + "';");
		sbf.append("arrFormData[0][3] = '" + props.getProperty(PARAM_PARAMETER_KEY) + "';");
		sbf.append("arrFormData[0][4] = '" + props.getProperty(PARAM_PARAMETER_VALUE) + "';");
		sbf.append("arrFormData[0][5] = '" + props.getProperty(PARAM_PARAMETER_TYPE) + "';");

		sbf.append(" var saveSuccess = 2;"); // 0-Not Applicable, 1-Success,
		// 2-Fail
		sbf.append(" var preAction = '" + props.getProperty(PARAM_ACTION) + "';");
		sbf.append(" var preEditable = '" + props.getProperty(PARAM_EDITABLE) + "';");

		return sbf.toString();
	}

	/**
	 * Sets Secure Context to the User request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setRequestHtml(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();

		sb.append("request['contextPath']='" + BasicRequestHandler.getContextPath(request) + "';");
		sb.append("request['secureUrl']='" + BasicRequestHandler.getSecureUrl(request) + "';");
		sb.append("request['userId']='" + request.getRemoteUser() + "';");
		sb.append("request['protocolStatus']='" + BasicRequestHandler.isProtocolStatusEqual() + "';");
		request.setAttribute(WebConstants.REQ_HTML_REQUEST, sb.toString());

	}
}
