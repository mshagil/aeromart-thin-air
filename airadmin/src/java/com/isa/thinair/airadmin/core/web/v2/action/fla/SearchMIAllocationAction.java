package com.isa.thinair.airadmin.core.web.v2.action.fla;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.model.FlaGraphBarBreakDwnDTO;
import com.isa.thinair.reporting.api.model.FlightInvBkngDTO;
import com.isa.thinair.reporting.api.model.FlightInventroyDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchMIAllocationAction {
	private static Log log = LogFactory.getLog(SearchMIAllocationAction.class);
	private Collection<FlightInventroyDTO> fltInvInfoList = new ArrayList<FlightInventroyDTO>();
	private boolean success = true;
	private boolean successGrph = true;
	private String message = "";
	private String flightID;
	private String cabinCode;
	private String departureDate;
	private String graphData;
	private Integer axiRange;
	private String rangeValues;
	private String channels;
	private String grphFlightID;
	private String	 grphChannel;
	private ArrayList<FlaGraphBarBreakDwnDTO> lstGrphBarBrkDwn;
	private ArrayList<String> lstChannels = new ArrayList<String>();
	private ArrayList<Integer> lstSeven = new ArrayList<Integer>();
	private ArrayList<Integer> lstThirty = new ArrayList<Integer>();
	private int barWidth = 0;

	public String execute() throws Exception {

		FlightInventroyDTO flightInventroyDTO = null;
		String strForward = WebConstants.FORWARD_SUCCESS;
		Date currDate = Calendar.getInstance().getTime();
		Date daprtureDate = null;
		ArrayList<FlightInvBkngDTO> graphDataLst = null;
		ArrayList<String> lstChannel = new ArrayList<String>();
		try {
			if ((flightID != null && !"".equals(flightID))) {
				daprtureDate = stringToDate(departureDate);
				if (CalendarUtil.isLessThan(currDate, daprtureDate)) {
					flightInventroyDTO = ModuleServiceLocator.getFLADataProviderBD().getFlightInventoryDetails(
							new Integer(flightID), dateToString(CalendarUtil.add(currDate, 0, 0, -7, 0, 0, 0)),
							dateToString(CalendarUtil.add(currDate, 0, 0, -30, 0, 0, 0)), dateToString(currDate));
				} else {
					flightInventroyDTO = ModuleServiceLocator.getFLADataProviderBD().getFlightInventoryDetails(
							new Integer(flightID), dateToString(CalendarUtil.add(daprtureDate, 0, 0, -7, 0, 0, 0)),
							dateToString(CalendarUtil.add(daprtureDate, 0, 0, -30, 0, 0, 0)), dateToString(daprtureDate));
				}
			}
			if (flightID != null && !"".equals(flightID)) {
				String bkngChanels = getSalesChannels();// AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORTING_PERIOD);
				if (bkngChanels != null && !"".equals(bkngChanels)) {
					String[] arrChannels = bkngChanels.split(",");
					for (String channel : arrChannels) {
						lstChannel.add(channel);
					}

				}

				if (CalendarUtil.isLessThan(currDate, daprtureDate)) {
					graphDataLst = ModuleServiceLocator.getFLADataProviderBD().getFlightInvGraphData(new Integer(flightID),
							dateToString(CalendarUtil.add(currDate, 0, 0, -7, 0, 0, 0)),
							dateToString(CalendarUtil.add(currDate, 0, 0, -30, 0, 0, 0)), dateToString(currDate), lstChannel);
				} else {
					graphDataLst = ModuleServiceLocator.getFLADataProviderBD().getFlightInvGraphData(new Integer(flightID),
							dateToString(CalendarUtil.add(daprtureDate, 0, 0, -7, 0, 0, 0)),
							dateToString(CalendarUtil.add(daprtureDate, 0, 0, -30, 0, 0, 0)), dateToString(daprtureDate),
							lstChannel);
				}
			}
			// arrangeChartData(graphDataLst);
			arrangeVisualChartData(graphDataLst);

			fltInvInfoList.add(flightInventroyDTO);
		} catch (ModuleException ex) {
			strForward = WebConstants.FORWARD_SUCCESS;
			success = false;
			message = "Error has occured in MIAllocation.";
		}
		return strForward;
	}

	private String getSalesChannels() {
		Collection<String[]> salesChanels;
		try {
			salesChanels = SelectListGenerator.createSalesChannelBookingOriginaterList();
		} catch (ModuleException e) {
			success = false;
			message = "Error has occured in MIAllocation.";
			return WebConstants.FORWARD_SUCCESS;
		}
		StringBuilder sb = new StringBuilder();
		for (Iterator<String[]> iterator = salesChanels.iterator(); iterator.hasNext();) {
			String[] strings = iterator.next();
			sb.append(strings[0]);
			if (iterator.hasNext()) {
				sb.append(",");
			}
		}

		// return "EBIATM,EBICDM,TravelAgent,Web,AAHolidays,APIAgencies,Interlined,LCC";
		return sb.toString();
	}

	/**
	 * Arrange data for the 3D graph
	 * 
	 * @param graphDataLst
	 */
	private void arrangeVisualChartData(ArrayList<FlightInvBkngDTO> graphDataLst) {

		if (graphDataLst != null && !graphDataLst.isEmpty()) {
			int cnt = 0;
			for (FlightInvBkngDTO flightInvBkngDTO : graphDataLst) {
				Integer lastSevn = (flightInvBkngDTO.getLast7() == null) ? 0 : flightInvBkngDTO.getLast7();
				Integer lastThrty = (flightInvBkngDTO.getLast30() == null) ? 0 : flightInvBkngDTO.getLast30();
				lstChannels.add(flightInvBkngDTO.getChannel());
				lstSeven.add(lastSevn);
				lstThirty.add(lastThrty - lastSevn);
			}
		}

	}

	/**
	 * Search data for the bar click in the graph return channel wise breakDown
	 */

	public String searchGraphData() {
		String strForward = WebConstants.FORWARD_SUCCESS;
		if ((grphFlightID != null && !grphFlightID.equals("")) && (grphChannel != null && !grphChannel.equals(""))) {
			ArrayList<Integer> nominalCodes = (ArrayList<Integer>) ReservationTnxNominalCode.getPaymentAndRefundNominalCodes();
			try {
				lstGrphBarBrkDwn = ModuleServiceLocator.getFLADataProviderBD().getGraphDataBreakDown(new Integer(grphFlightID),
						grphChannel, nominalCodes);

			} catch (NumberFormatException e) {
				successGrph = false;
				if (log.isDebugEnabled()) {
					log.debug("SearchMIAllocationAction in searchGraphData() Flight number incorrect format");
				}
				log.error(e);
			} catch (ModuleException e) {
				successGrph = false;
				message = "Error has occured in MIAllocation!searchGraphData().";
				log.error(e);
			}
		}
		return strForward;
	}

	/**
	 * @param strDate
	 * @return java.util Date
	 */
	public Date stringToDate(String strDate) {
		Date date = null;
		if (strDate != null && !"".equals(strDate)) {
			String[] arrDate = strDate.split("/");
			Calendar cal = new GregorianCalendar();
			cal.set(new Integer(arrDate[2]), new Integer(arrDate[1]) - 1, new Integer(arrDate[0]));
			date = new Date(cal.getTimeInMillis());
		}
		return date;
	}

	/**
	 * @param date
	 * @return String formatted date
	 */
	public String dateToString(Date date) {
		String formattedDate = null;
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			formattedDate = dateFormat.format(date);
		}
		return formattedDate;
	}

	/**
	 * <p>
	 * Search Inventory
	 * </p>
	 * 
	 * @return String array arrRetData
	 */
	public String searchFlights() {
		if (log.isDebugEnabled()) {
			log.debug("inside SearchMIAllocationAction search values");
		}
		return null;
	}

	public String getCabinCode() {
		return cabinCode;
	}

	public void setCabinCode(String cabinCode) {
		this.cabinCode = cabinCode;
	}

	public String getFlightID() {
		return flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public Collection<FlightInventroyDTO> getFltInvInfoList() {
		return fltInvInfoList;
	}

	public void setFltInvInfoList(Collection<FlightInventroyDTO> fltInvInfoList) {
		this.fltInvInfoList = fltInvInfoList;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getGraphData() {
		return graphData;
	}

	public Integer getAxiRange() {
		return axiRange;
	}

	public ArrayList<String> getLstChannels() {
		return lstChannels;
	}

	public ArrayList<Integer> getLstSeven() {
		return lstSeven;
	}

	public ArrayList<Integer> getLstThirty() {
		return lstThirty;
	}

	public void setLstChannels(ArrayList<String> lstChannels) {
		this.lstChannels = lstChannels;
	}

	public void setLstSeven(ArrayList<Integer> lstSeven) {
		this.lstSeven = lstSeven;
	}

	public void setLstThirty(ArrayList<Integer> lstThirty) {
		this.lstThirty = lstThirty;
	}

	public void setGraphData(String graphData) {
		this.graphData = graphData;
	}

	public void setAxiRange(Integer axiRange) {
		this.axiRange = axiRange;
	}

	public String getRangeValues() {
		return rangeValues;
	}

	public void setRangeValues(String rangeValues) {
		this.rangeValues = rangeValues;
	}

	public String getChannels() {
		return channels;
	}

	public void setChannels(String channels) {
		this.channels = channels;
	}

	public int getBarWidth() {
		return barWidth;
	}

	public void setBarWidth(int barWidth) {
		this.barWidth = barWidth;
	}

	public String getGrphFlightID() {
		return grphFlightID;
	}

	public String getGrphChannel() {
		return grphChannel;
	}

	public void setGrphFlightID(String grphFlightID) {
		this.grphFlightID = grphFlightID;
	}

	public void setGrphChannel(String grphChannel) {
		this.grphChannel = grphChannel;
	}

	public boolean isSuccessGrph() {
		return successGrph;
	}

	public ArrayList<FlaGraphBarBreakDwnDTO> getLstGrphBarBrkDwn() {
		return lstGrphBarBrkDwn;
	}

	public void setSuccessGrph(boolean successGrph) {
		this.successGrph = successGrph;
	}

	public void setLstGrphBarBrkDwn(ArrayList<FlaGraphBarBreakDwnDTO> lstGrphBarBrkDwn) {
		this.lstGrphBarBrkDwn = lstGrphBarBrkDwn;
	}

}
