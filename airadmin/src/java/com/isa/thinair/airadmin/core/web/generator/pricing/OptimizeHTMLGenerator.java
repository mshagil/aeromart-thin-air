/**
 * 
 */
package com.isa.thinair.airadmin.core.web.generator.pricing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.pricing.OptimizeRequestHandler;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author cBandara
 * 
 */
public class OptimizeHTMLGenerator {

	private static String clientErrors;
	private Properties props;
	private static Log log = LogFactory.getLog(OptimizeRequestHandler.class);

	public OptimizeHTMLGenerator(Properties props) {
		this.props = props;
	}

	/**
	 * Gets the OptiMiZe Data for the Search
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid for the search
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getOptimizeRowHtml(HttpServletRequest request) throws ModuleException {

		Collection<FCCInventoryDTO> colOptimize = null;
		InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria();

		// setting the flight number

		String strFlightNumber = this.props.getProperty("txtFlightNo");
		if (strFlightNumber.length() > 2) {
			searchCriteriaDTO.setFlightNumber(strFlightNumber);
		}
		searchCriteriaDTO.setRestrictNumberOfRecords(true);

		// preparing and setting the from date
		String strFromDate = this.props.getProperty("txtFromDate");
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		Date fromDate = null;
		if (strFromDate != null) {
			try {
				fromDate = dtfmt.parse(strFromDate);
				searchCriteriaDTO.setFromDate(fromDate);
			} catch (ParseException ex) {
				log.error(ex.getMessage());
			}
		}

		// preparing and setting the to date
		String strToDate = this.props.getProperty("txtToDate");
		Date toDate = null;
		if (strToDate != null) {
			try {
				toDate = dtfmt.parse(strToDate);
				Calendar calToDate = new GregorianCalendar();
				calToDate.setTime(toDate);
				calToDate.set(Calendar.HOUR, 23);
				calToDate.set(Calendar.MINUTE, 59);
				calToDate.set(Calendar.SECOND, 59);
				calToDate.set(Calendar.MILLISECOND, 999);

				searchCriteriaDTO.setToDate(calToDate.getTime());

			} catch (ParseException ex) {
				log.error(ex.getMessage());
			}
		}

		// preparing and setting the segment codes
		String strSegmentCodeCollect = this.props.getProperty("hdnAssignOnD");
		String logicalCC = this.props.getProperty("hdnLogicalCC");
		// String strSegmentCodeCollect = "CMB/SHJ,SSH/LXR";
		if (strSegmentCodeCollect != null && strSegmentCodeCollect.length() > 0) {
			String[] strSegmentArr = strSegmentCodeCollect.split(",");

			for (int i = 0; i < strSegmentArr.length; i++) {
				String strSegmentCode = strSegmentArr[i];
				searchCriteriaDTO.addBookingClasses(strSegmentCode, logicalCC, null);
			}
		}

		// preparing the flight IDs and Departure Dates
		Set<Integer> flightIds = new HashSet<Integer>();
		Map<Integer, FlightSummaryDTO> depatureDatesMap = new HashMap<Integer, FlightSummaryDTO>();
		Collection<FlightSummaryDTO> collectionFligthSummary = ModuleServiceLocator.getFlightServiceBD().getLikeFlightSummary(
				searchCriteriaDTO);

		if (collectionFligthSummary != null && collectionFligthSummary.size() > 0) {
			FlightSummaryDTO flightSummaryDTO = null;
			Iterator<FlightSummaryDTO> iter = collectionFligthSummary.iterator();

			while (iter.hasNext()) {
				flightSummaryDTO = (FlightSummaryDTO) iter.next();
				int flightID = flightSummaryDTO.getFlightId();
				flightIds.add(new Integer(flightID));
				// depatureDates.add(flightSummaryDTO.getDepatureDate());
				depatureDatesMap.put(new Integer(flightID), flightSummaryDTO);
			}
		}

		OptimizeSeatInvCriteriaDTO optimizeSeatInvCriteriaDTO = new OptimizeSeatInvCriteriaDTO();

		String strBCDisType = this.props.getProperty("hdnBCDisType");
		String strAssignBCs = this.props.getProperty("hdnAssignBC");
		String strAllocType = this.props.getProperty("hdnAllocType");
		String strSeats = this.props.getProperty("txtSeats");
		String strMoreOrLess = this.props.getProperty("selMoreLess");
		boolean blnSeglevel = this.props.getProperty("radSegLevel").equals("A") ? true : false;
		boolean blnBCLevel = this.props.getProperty("radSegLevel").equals("B") ? true : false;
		boolean blnAgent = this.props.getProperty("radShowBc").equals("Agent") ? true : false;
		String strAgent = this.props.getProperty("selAgent");

		if (strBCDisType.equals("A")) {
			optimizeSeatInvCriteriaDTO.setIncludeAllBCs(true);
		} else if (strBCDisType.equals("S")) {
			optimizeSeatInvCriteriaDTO.setIncludeStandardBCsOnly(true);
		} else {
			Set<String> bookingClass = new HashSet<String>();
			if (strAssignBCs != null && strAssignBCs.length() > 0) {
				String[] strBCArr = strAssignBCs.split(",");

				for (int i = 0; i < strBCArr.length; i++) {
					String strBC = strBCArr[i];
					bookingClass.add(strBC);
				}
			}
			optimizeSeatInvCriteriaDTO.setSelectedBCs(bookingClass);
		}

		if (strAllocType.equals("NA")) {
			optimizeSeatInvCriteriaDTO.setCheckNegativeAvailability(true);
			optimizeSeatInvCriteriaDTO.setPerformAvailableOnSegment(true);
		} else {
			if (strMoreOrLess.equals("Less than")) {
				optimizeSeatInvCriteriaDTO.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_LESS_THAN);
			} else if (strMoreOrLess.equals("More than")) {
				optimizeSeatInvCriteriaDTO.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_GREATER_THAN);
			} else if (strMoreOrLess.equals("More than or equal")) {
				optimizeSeatInvCriteriaDTO
						.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_GREATER_THAN_OR_EQUAL);
			} else if (strMoreOrLess.equals("Less than or equal")) {
				optimizeSeatInvCriteriaDTO
						.setAvailabilityOperator(OptimizeSeatInvCriteriaDTO.AVAILABILITY_OPERATOR_LESS_THAN_OR_EQUAL);
			}
			optimizeSeatInvCriteriaDTO.setNoOfSeats(Integer.parseInt(strSeats));
			if (blnSeglevel) {
				optimizeSeatInvCriteriaDTO.setPerformAvailableOnSegment(true);
			} else if (blnBCLevel) {
				optimizeSeatInvCriteriaDTO.setPerformAvailableOnBC(true);
			}
		}

		if (blnAgent) {
			optimizeSeatInvCriteriaDTO.setAgentCode(strAgent);
		}
		colOptimize = ModuleServiceLocator.getFlightInventoryBD().getFlightInventoriesForOptimization(flightIds,
				optimizeSeatInvCriteriaDTO);
		return createOptimizeRowHtml(colOptimize, depatureDatesMap, request);
	}

	/**
	 * Creates the OPTIMIZE Grid from a collection of FCCInventoryDtos and a Day Map
	 * 
	 * @param colOptimize
	 *            the Collection of Dtos
	 * @param depatureDatesMap
	 *            the Map of days
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Created Grid Array
	 */
	private static String createOptimizeRowHtml(Collection<FCCInventoryDTO> colOptimize, Map<Integer, FlightSummaryDTO> depatureDatesMap,
			HttpServletRequest request) {

		Vector<String[]> vecExcelRowData;
		String[] strExcelColData;
		StringBuffer sb = new StringBuffer("var optimizeData = new Array();");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
		int intRowCount = 0;
		boolean blnFor1 = false;
		boolean blnFor2 = false;
		int intTempInvId;
		int intTempFlightId;
		int intTempAllocInf;
		int intTempOversell;
		int intTempCurtailed;
		long lngTempSegVersion;
		String strTempCC;
		String strTempSegment;
		long lngDateDiff = 0;

		vecExcelRowData = new Vector<String[]>();
		String[] strExcelColHead = { "Date", "Flight #", "Days Dep", "COS", "Segment", "Alloc Ad", "Alloc Inf", "Sold Ad\\Inf",
				"Onhold Ad\\Inf", "Oversell", "Curtailed", "Avai Ad\\Inf", "BC", "Logical CC", "Allo.", "Closed", "Fixed Y/N", "Priority Y/N",
				"Sold", "Onhold", "Cancelled", "Aquired", "Available" };

		vecExcelRowData.add(strExcelColHead);

		if (colOptimize != null) {
			for (Iterator<FCCInventoryDTO> iter = colOptimize.iterator(); iter.hasNext();) {
				FCCInventoryDTO fccInventoryDTO = (FCCInventoryDTO) iter.next();

				Date currentDate = new Date();

				FlightSummaryDTO flightSummaryDTO = (FlightSummaryDTO) depatureDatesMap.get(new Integer(fccInventoryDTO
						.getFlightId()));

				lngDateDiff = (flightSummaryDTO.getDepatureDate().getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24);

				strExcelColData = new String[23];
				for (int i = 0; i < strExcelColData.length; i++) {
					strExcelColData[i] = ",";
				}
				vecExcelRowData.add(strExcelColData);
				strExcelColData[0] = String.valueOf(flightSummaryDTO.getDepatureDate() != null ? formatter
						.format(flightSummaryDTO.getDepatureDate()) : "");
				strExcelColData[1] = String.valueOf(flightSummaryDTO.getFlightNumber());
				strExcelColData[2] = String.valueOf(lngDateDiff);
				strExcelColData[3] = fccInventoryDTO.getCabinClassCode();

				sb.append("optimizeData[" + intRowCount + "]    = ");
				sb.append("new Array('','','','','','','','','','','','','','','','','','','','','','','','");
				sb.append(String.valueOf(fccInventoryDTO.getFlightId()) + "','");
				sb.append(fccInventoryDTO.getCabinClassCode() + "','','','','','','','','','','','','','');");
				sb.append("optimizeData[" + intRowCount + "][0] = '" + intRowCount + "';");
				sb.append("optimizeData[" + intRowCount + "][1] = '");
				sb.append(String.valueOf(flightSummaryDTO.getDepatureDate() != null ? formatter.format(flightSummaryDTO
						.getDepatureDate()) : "") + "';");
				sb.append("optimizeData[" + intRowCount + "][2] = '");
				sb.append(flightSummaryDTO.getFlightNumber() + "';");
				sb.append("optimizeData[" + intRowCount + "][3] = '");
				sb.append(String.valueOf(lngDateDiff) + "';");
				sb.append("optimizeData[" + intRowCount + "][4] = '");
				sb.append(fccInventoryDTO.getCabinClassCode() + "';");

				intTempFlightId = fccInventoryDTO.getFlightId();
				strTempCC = fccInventoryDTO.getCabinClassCode();
				blnFor1 = true;

				for (Iterator<FCCSegInventoryDTO> iterSeg = fccInventoryDTO.getFccSegInventoryDTOs().iterator(); iterSeg.hasNext();) {

					FCCSegInventoryDTO fccSegInventoryDTO = (FCCSegInventoryDTO) iterSeg.next();
					FCCSegInventory fccSegInventory = fccSegInventoryDTO.getFccSegInventory();

					if (!blnFor1) {
						intRowCount++;
						sb.append("optimizeData[" + intRowCount + "]    = ");
						sb.append("new Array('','','','','','','','','','','','','','','','','','','','','','',");
						sb.append("'','" + intTempFlightId + "','" + strTempCC);
						sb.append("','','','','','','','','','','','','','');");
						strExcelColData = new String[23];
						for (int i = 0; i < strExcelColData.length; i++) {
							strExcelColData[i] = ",";
						}
						vecExcelRowData.add(strExcelColData);
					}
					strExcelColData[4] = (fccSegInventory.getSegmentCode());
					strExcelColData[5] = (String.valueOf(fccSegInventory.getAllocatedSeats()));
					strExcelColData[6] = (String.valueOf(fccSegInventory.getInfantAllocation()));
					strExcelColData[7] = (fccSegInventory.getSeatsSold() + "\\" + fccSegInventory.getSoldInfantSeats());
					strExcelColData[8] = (fccSegInventory.getOnHoldSeats() + "\\" + fccSegInventory.getOnholdInfantSeats());
					strExcelColData[9] = (String.valueOf(fccSegInventory.getOversellSeats()));
					strExcelColData[10] = (String.valueOf(fccSegInventory.getCurtailedSeats()));
					strExcelColData[11] = (fccSegInventory.getAvailableSeats() + "\\" + fccSegInventory.getAvailableInfantSeats());

					sb.append("optimizeData[" + intRowCount + "][25]  = '");
					sb.append(fccSegInventory.getFccsInvId() + "';");
					sb.append("optimizeData[" + intRowCount + "][26]  = '");
					sb.append(fccSegInventory.getSegmentCode() + "';");
					sb.append("optimizeData[" + intRowCount + "][5]   = '");
					sb.append(fccSegInventory.getSegmentCode() + "';");
					sb.append("optimizeData[" + intRowCount + "][6]   = '");
					sb.append(fccSegInventory.getAllocatedSeats() + "';");
					sb.append("optimizeData[" + intRowCount + "][7]   = '");
					sb.append(fccSegInventory.getInfantAllocation() + "';");
					sb.append("optimizeData[" + intRowCount + "][8]   = '");
					sb.append(fccSegInventory.getSeatsSold() + "/");
					sb.append(fccSegInventory.getSoldInfantSeats() + "';");
					sb.append("optimizeData[" + intRowCount + "][9]   = '");
					sb.append(fccSegInventory.getOnHoldSeats() + "/");
					sb.append(fccSegInventory.getOnholdInfantSeats() + "';");
					sb.append("optimizeData[" + intRowCount + "][10]  = '");
					sb.append(fccSegInventory.getOversellSeats() + "';");
					sb.append("optimizeData[" + intRowCount + "][11]  = '");
					sb.append(fccSegInventory.getCurtailedSeats() + "';");
					sb.append("optimizeData[" + intRowCount + "][12]  = '");
					sb.append(fccSegInventory.getAvailableSeats() + "/");
					sb.append(fccSegInventory.getAvailableInfantSeats() + "';");
					sb.append("optimizeData[" + intRowCount + "][31]  = '");
					sb.append(fccSegInventory.getVersion() + "';");

					intTempInvId = fccSegInventory.getFccsInvId();
					intTempAllocInf = fccSegInventory.getInfantAllocation();
					intTempOversell = fccSegInventory.getOversellSeats();
					intTempCurtailed = fccSegInventory.getCurtailedSeats();
					strTempSegment = fccSegInventory.getSegmentCode();
					lngTempSegVersion = fccSegInventory.getVersion();
					blnFor2 = true;

					for (Iterator<ExtendedFCCSegBCInventory> iterFccSegBCInventory = fccSegInventoryDTO.getFccSegBCInventories().iterator(); iterFccSegBCInventory
							.hasNext();) {

						ExtendedFCCSegBCInventory extendedFCCSegBCInventory = (ExtendedFCCSegBCInventory) iterFccSegBCInventory
								.next();
						FCCSegBCInventory fccSegBCInventory = extendedFCCSegBCInventory.getFccSegBCInventory();

						if (!blnFor2) {
							intRowCount++;
							sb.append("optimizeData[" + intRowCount + "]    = ");
							sb.append("new Array('','','','','','','','','','','','','','','','','','','','',");
							sb.append("'','','','" + intTempFlightId + "','" + strTempCC + "','" + intTempInvId + "','");
							sb.append(strTempSegment + "','','" + intTempAllocInf + "','" + intTempOversell + "','");
							sb.append(intTempCurtailed + "','" + lngTempSegVersion + "','','','','','','');");
							strExcelColData = new String[23];
							for (int i = 0; i < strExcelColData.length; i++) {
								strExcelColData[i] = ",";
							}
							vecExcelRowData.add(strExcelColData);
						}
						strExcelColData[12] = (fccSegBCInventory.getBookingCode());
						strExcelColData[13] = (String.valueOf(fccSegBCInventory.getLogicalCCCode()));
						strExcelColData[14] = (String.valueOf(fccSegBCInventory.getSeatsAllocated()));
						strExcelColData[15] = ((String.valueOf(fccSegBCInventory.getStatus())).equalsIgnoreCase("CLS") ? "Yes"
								: "No");
						strExcelColData[16] = ((String.valueOf((extendedFCCSegBCInventory.isFixedFlag())))
								.equalsIgnoreCase("FALSE") ? "N" : "Y");
						strExcelColData[17] = ((fccSegBCInventory.getPriorityFlag()) ? "Y" : "N");
						strExcelColData[18] = (String.valueOf(fccSegBCInventory.getActualSeatsSold()));
						strExcelColData[19] = (String.valueOf(fccSegBCInventory.getActualSeatsOnHold()));
						strExcelColData[20] = (String.valueOf(fccSegBCInventory.getSeatsCancelled()));
						strExcelColData[21] = (String.valueOf(fccSegBCInventory.getSeatsAcquired()));
						strExcelColData[22] = (String.valueOf(fccSegBCInventory.getSeatsAvailable()));

						sb.append("optimizeData[" + intRowCount + "][27] = '");
						sb.append(fccSegBCInventory.getFccsbInvId() + "';");
						sb.append("optimizeData[" + intRowCount + "][13] = '");
						sb.append(fccSegBCInventory.getBookingCode() + "';");
						sb.append("optimizeData[" + intRowCount + "][14] = '");
						sb.append(fccSegBCInventory.getSeatsAllocated() + "';");
						sb.append("optimizeData[" + intRowCount + "][15] = '");
						sb.append(fccSegBCInventory.getStatus() + "';");
						sb.append("optimizeData[" + intRowCount + "][16] = '");
						sb.append(extendedFCCSegBCInventory.isFixedFlag() + "';");
						sb.append("optimizeData[" + intRowCount + "][17] = '");
						sb.append(fccSegBCInventory.getActualSeatsSold() + "';");
						sb.append("optimizeData[" + intRowCount + "][18] = '");
						sb.append(fccSegBCInventory.getActualSeatsOnHold() + "';");
						sb.append("optimizeData[" + intRowCount + "][19] = '");
						sb.append(fccSegBCInventory.getSeatsCancelled() + "';");
						sb.append("optimizeData[" + intRowCount + "][20] = '");
						sb.append(fccSegBCInventory.getSeatsAcquired() + "';");

						if (fccSegBCInventory.getStatusChangeAction().equals(FCCSegBCInventory.StatusAction.MANUAL)) {
							sb.append("optimizeData[" + intRowCount + "][21] = '");
							sb.append(fccSegBCInventory.getSeatsAvailable() + " *';");
						} else {
							sb.append("optimizeData[" + intRowCount + "][21] = '");
							sb.append(fccSegBCInventory.getSeatsAvailable() + "';");
						}

						sb.append("optimizeData[" + intRowCount + "][22] = '");
						sb.append(fccSegBCInventory.getPriorityFlag() + "';");
						sb.append("optimizeData[" + intRowCount + "][32] = '");
						sb.append(fccSegBCInventory.getVersion() + "';");
						sb.append("optimizeData[" + intRowCount + "][40] = '");
						sb.append(fccSegBCInventory.getStatusChangeAction() + "';");
						sb.append("optimizeData[" + intRowCount + "][41] = '");
						sb.append(fccSegBCInventory.getStatusChangeAction() + "';");
						sb.append("optimizeData[" + intRowCount + "][42] = '");
						sb.append(fccSegBCInventory.getLogicalCCCode() + "';");

						blnFor1 = false;
						blnFor2 = false;
					}
					blnFor1 = false;
					blnFor2 = false;
				}
				intRowCount++;
				blnFor1 = false;
				blnFor2 = false;
			}
		}
		HttpSession session = request.getSession(true);
		session.setAttribute("ExcelData", vecExcelRowData);
		return sb.toString();
	}

	/**
	 * Creates the Client Validations for the OPTIMIZE Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getSearchClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.searchOptimizeInventory.form.fromdate.required", "fromdateRqrd");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.todate.required", "todateRqrd");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.fromdate.incorrect", "FromDateIncorrect");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.todate.incorrect", "ToDateIncorrect");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.fromdategreaterthantodate.voilated", "todateLess");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.SegmentOrFlight.required", "SegmentOrFlightRqd");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.Seats.required", "SeatRqd");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureRequired.required", "depatureRequired");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.arrivalRequired.required", "arrivalRequired");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureArriavlSame.Same", "depatureArriavlSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.arriavlViaSame.Same", "arriavlViaSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureViaSame.Same", "depatureViaSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.vianotinSequence.notin", "vianotinSequence");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.via.same", "viaSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.BCs.required", "BCsReq");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.OnD.exist", "OnDExists");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.agent.required", "AgentRqd");

			moduleErrs.setProperty("um.OptimizeInventory.form.segmentcurtailed.Invalid", "curtaildInvaid");
			moduleErrs.setProperty("um.OptimizeInventory.form.segmentoversell.Invalid", "oversellInvaid");
			moduleErrs.setProperty("um.OptimizeInventory.form.bcallocation.Less", "bcLess");
			moduleErrs.setProperty("um.OptimizeInventory.form.infantallocation.Less", "infantLess");
			moduleErrs.setProperty("um.flightschedule.search.invalidcarrier", "invalidcarrier");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
