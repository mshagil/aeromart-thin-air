package com.isa.thinair.airadmin.core.web.action.flightsched;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.master.GDSRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.GDS_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowGDSAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		return GDSRequestHandler.execute(request);
	}

}
