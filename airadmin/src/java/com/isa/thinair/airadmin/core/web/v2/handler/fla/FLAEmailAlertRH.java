package com.isa.thinair.airadmin.core.web.v2.handler.fla;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.EmailMessage;

/**
 * @author harsha
 * 
 */
public final class FLAEmailAlertRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(FLAEmailAlertRH.class);
	private static Object lock = new Object();
	private static String clientErrors;
	private static final String PARAM_MODE = "hdnMode";

	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled())
			log.debug("Begin FLA Email Alert:" + System.currentTimeMillis());
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String toAddress = request.getParameter("txtToAddress");
		String subject = request.getParameter("txtSubject");
		String content = request.getParameter("txtMsgBody");
		String FROM_ADDRESS = "res@accelaero.com";
		try {
			if (strHdnMode != null && strHdnMode.equals("EMAIL")) {
				if (toAddress != null && !"".equals(toAddress)) {
					EmailMessage msg = new EmailMessage();
					msg.setFromAddress(FROM_ADDRESS);
					msg.setToAddress(toAddress);
					msg.setSubject(subject);

					msg.setBody(content);
					msg.setMailServerConfigurationName("default");

					List messagesList = new ArrayList();
					messagesList.add(msg);
					ModuleServiceLocator.getMessagingBD().sendMessages(messagesList);

					saveMessage(request, "Email has succesfully sent.", WebConstants.MSG_SUCCESS);
					if (log.isDebugEnabled())
						log.debug("Email has succesfully sent:" + System.currentTimeMillis());
				}
			}
			setDisplayData(request);

		} catch (ModuleException me) {
			log.error("Exception in  FLA Email Alert:", me);
			saveMessage(request, "Error has occured while sending the email.", WebConstants.MSG_ERROR);
		}

		return forward;
	}

	/**
	 * Set the Display Data For FLA Email Alert Page
	 * 
	 * @param request
	 *            the HttpServletRequests
	 * @throws ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
	}

	/**
	 * Sets the client Validations
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Creates Client validations for FLA Email Alert Page using the inventory validates
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Client Validations Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		synchronized (lock) {
			if (clientErrors == null) {
				Properties moduleErrs = new Properties();

				moduleErrs.setProperty("um.airportdst.form.email.required", "emailRequired");
				moduleErrs.setProperty("um.agent.form.invalid.email", "invalidEmailId");

				clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
			}
			return clientErrors;
		}
	}

}
