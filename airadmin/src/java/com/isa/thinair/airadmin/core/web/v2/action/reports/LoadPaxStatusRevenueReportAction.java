package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.util.PassengerUtil;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author nafly
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_PAX_STATUS_JSP),
		@Result(name = S2Constants.Result.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class LoadPaxStatusRevenueReportAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(LoadPaxStatusRevenueReportAction.class);
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();


	public String execute() {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("LoadPaxStatusRevenueReportAction setDisplayData() SUCCESS");
		} catch (Exception e) {
			log.error("LoadPaxStatusRevenueReportAction setDisplayData() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("LoadPaxStatusRevenueReportAction setReportView Success");
				return null;
			} else {
				log.error("LoadPaxStatusRevenueReportAction setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("LoadPaxStatusRevenueReportAction setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	public static void setDisplayData(HttpServletRequest request) throws Exception {

		// PFS Status LIST
		PassengerUtil.setPFSStatus(request);
		// FareRule List
		request.setAttribute(WebConstants.REQ_HTML_FARECLASS_DATA, SelectListGenerator.createFareCodeRuleList());
		// FareBasis List
		request.setAttribute(WebConstants.REQ_HTML_BASIS_LIST, SelectListGenerator.createBasisCodeList());
		// Booking Class List
		request.setAttribute(WebConstants.REQ_HTML_BC_LIST, SelectListGenerator.createBookingCodeList());
		// AirPort List
		// AgentType list
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, SelectListGenerator.createAgentTypeCodesList());
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, SelectListGenerator.createAirportCodeList());
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, ReportsHTMLGenerator.getClientErrors(request));
		setLiveStatus(request);
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, SelectListGenerator.createAgentTypeList_SG());
		setDisplayAgencyMode(request);
		setGSAMultiSelectList(request);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {

		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.comp.all") != null) {// TODO have to set the correct privileges
			// Show both the controls
			request.setAttribute("displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.comp.rpt") != null) {
			request.setAttribute("displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.comp") != null) {
			request.setAttribute("displayAgencyMode", "0");
		} else {
			request.setAttribute("displayAgencyMode", "-1");
		}
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {

		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (request.getAttribute("displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}

		if (request.getAttribute("displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (request.getAttribute("displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				request.setAttribute("currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (request.getAttribute("displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (request.getAttribute("displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				request.setAttribute("currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					request.setAttribute("displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS, "displayAgencyMode = " + request.getAttribute("displayAgencyMode")
				+ ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		// TODO
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String fromDepDate = request.getParameter("txtFromDate");
		String toDepDate = request.getParameter("txtToDate");
		String chkLocal = request.getParameter("chkLocalTime");
		String fromBookDate = request.getParameter("txtBookFromDate");
		String toBookDate = request.getParameter("txtBookToDate");
		String flightNumber = request.getParameter("flightNo");
		String pfsStatus = request.getParameter("pfsStatus");
		String bookingClass = request.getParameter("bookingClass");
		String fareBasis = request.getParameter("fareBasis");
		String fareRule = request.getParameter("fareRule");
		String origin = request.getParameter("origin");
		String destination = request.getParameter("destination");
		String agents = null;
		String rptOption = request.getParameter("radReportOption");

		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		if (fromDepDate != null && !fromDepDate.trim().equals("")) {
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDepDate));
		}

		if (toDepDate != null && !toDepDate.trim().equals("")) {
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDepDate));
		}

		boolean isDepInLocalTime = (chkLocal != null && chkLocal.equalsIgnoreCase("on")) ? true : false;
		search.setShowInLocalTime(isDepInLocalTime);

		if (fromBookDate != null && !fromBookDate.trim().equals("")) {
			search.setBookingDateFrom(ReportsHTMLGenerator.convertDate(fromBookDate));
		}

		if (toBookDate != null && !toBookDate.trim().equals("")) {
			search.setBookingDateTo(ReportsHTMLGenerator.convertDate(toBookDate));
		}
		
		if (flightNumber != null && !flightNumber.trim().equals("")) {
			String[] array = flightNumber.split(",");
			search.setFlightNoCollection(Arrays.asList(array));
		}

		if (pfsStatus != null && !pfsStatus.trim().equals("")) {
			search.setPfsStatus(PassengerUtil.getPfsStatus(pfsStatus));
		}

		if (bookingClass != null && !bookingClass.trim().equals("") && !"All".equalsIgnoreCase(bookingClass.trim())) {
			search.setBookinClass(bookingClass);
		}

		if (fareBasis != null && !fareBasis.trim().equals("") && !"All".equalsIgnoreCase(fareBasis.trim())) {
			search.setFareBasisCode(fareBasis);
		}

		if (fareRule != null && !fareRule.trim().equals("") && !"All".equalsIgnoreCase(fareRule.trim())) {
			search.setFareRuleCode(fareRule);
		}

		if (origin != null && !origin.trim().equals("")) {
			search.setOriginAirport(origin);
		}

		if (destination != null && !destination.trim().equals("")) {
			search.setDestinationAirport(destination);
		}
		
		if (agents != null && !agents.trim().equals("")) {
			String[] array = agents.split(",");
			search.setAgents(Arrays.asList(array));
		}

		ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getPassengerStatusAndRevenueData(search);


		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String id = "UC_REPM_087";
		String templateDetail = "PaxStatusAndRevenueReport.jasper";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", fromDepDate);
		parameters.put("TO_DATE", toDepDate);
		parameters.put("ID", id);

		String reportName = "PaxStatusAndRevenueReport";

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(templateDetail));

		if (rptOption.trim().equals("HTML")) {
			parameters.put("IMG", reportsRootDir);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
					reportsRootDir, response);

		} else if (rptOption.trim().equals("PDF")) {
			response.reset();
			reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", reportsRootDir);
			response.addHeader("Content-Disposition",
					String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

		} else if (rptOption.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition",
					String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

		} else if (rptOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition",
					String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

		}
	}

	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}
}
