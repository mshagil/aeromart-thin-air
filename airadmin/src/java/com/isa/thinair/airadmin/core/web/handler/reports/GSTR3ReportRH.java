package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reportingframework.api.dto.ReportParam;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRResultSetDataSource;

public class GSTR3ReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(GSTR3ReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("GSTR3ReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("GSTR3ReportRHexecute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("GSTR3ReportRHReportRH setReportView Success");
				return null;
			} else {
				log.error("GSTR3ReportRHReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("GSTR3ReportRHReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountriesAndStates(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets the countries and relevant states
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setCountriesAndStates(HttpServletRequest request) throws ModuleException {
		String strCountriesHtml = SelectListGenerator.createCountryState();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_STATE_LIST, strCountriesHtml);

		String strStatesHtml = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strStatesHtml);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String value = request.getParameter("radReportOption");

		String stateCode = request.getParameter("selState");

		String fromDateString = request.getParameter("txtFromDate");
		String toDateString = request.getParameter("txtToDate");

		int[] ymd = ReportsHTMLGenerator.getYearMonthDay(fromDateString);
		int year = ymd[0];
		int month = ymd[1];

		String reportName = "GSTR3Report";
		String reportTemplate = "GSTR3_Main.jasper";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			State state = ModuleServiceLocator.getCommonServiceBD().getState(stateCode);

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDateString != null && !fromDateString.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDateString) + " 00:00:00");
			}

			if (toDateString != null && !toDateString.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDateString) + "  23:59:59");
			}

			if (stateCode != null && !stateCode.equals("")) {
				search.setStateCode(stateCode);
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			Map<String, ResultSet> resultSetMap = ModuleServiceLocator.getDataExtractionBD().getGstr3(search);

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, 1);
			calendar.set(Calendar.MONTH, month - 1);
			calendar.set(Calendar.YEAR, year);
			Date date = calendar.getTime();

			HashMap mainReportParameterMap = new HashMap();

			mainReportParameterMap.put("GSTIN", Arrays.asList(state.getAirlineOfficeTaxRegNo().split("")));
			mainReportParameterMap.put("PERSON_NAME", AppSysParamsUtil.getDefaultCarrierName());
			mainReportParameterMap.put("ADDRESS", state.getAirlineOfficeSTAddress1() + ", " + state.getAirlineOfficeSTAddress2()
					+ ", " + state.getAirlineOfficeCity() + ".");
			mainReportParameterMap.put("MONTH", ReportsHTMLGenerator.getMonthName(date));
			mainReportParameterMap.put("YEAR", Arrays.asList(Integer.toString(year).split("")));

			Iterator it = resultSetMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				mainReportParameterMap.put(pair.getKey(), (JRDataSource) new JRResultSetDataSource((ResultSet) pair.getValue()));
			}

			Map parameters = new HashMap();
			parameters.put("mainReportParameterMap", mainReportParameterMap);
			parameters.put("SUBREPORT_DIR", PlatformConstants.getConfigRootAbsPath() + "/templates/reports/GSTR3/");

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				ModuleServiceLocator.getReportingFrameworkBD().createReport(ReportParam.ReportType.HTML, "WebReport1",
						parameters, resultSetMap, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createReport(ReportParam.ReportType.PDF, "WebReport1", parameters,
						resultSetMap, response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".xls");
				ModuleServiceLocator.getReportingFrameworkBD().createReport(ReportParam.ReportType.XSL, "WebReport1", parameters,
						resultSetMap, response);
			} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createReport(ReportParam.ReportType.CSV, "WebReport1", parameters,
						resultSetMap, response);
			}

		} catch (Exception e) {
			log.error(e);
		}
	}
}
