package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXRuleTO;
import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowBlacklistPAXRulesDetailAction {

	private static Log log = LogFactory.getLog(ShowBlacklistPAXRulesDetailAction.class);

	private BlacklistPAXRuleSearchTO blacklistPAXRuleSearchTO = new BlacklistPAXRuleSearchTO();

	/** BlacklistPAXRuleTO object to transfer data to the back end */
	private BlacklistPAXRuleTO blacklistPAXRuleTO = new BlacklistPAXRuleTO();

	private String message;

	private String msgType;

	private int pageSize = 20;

	private int page;

	private int records;

	private int total;

	private ArrayList<Map<String, Object>> blacklistPAXRuleList;

	/** Search result and pagination data */
	private Collection<Map<String, Object>> rows;

	private String errorList;
	
	private boolean blacklistRulesExsistence;

	public String search() {
		BlacklistPAXBD blacklistPAXBD = ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD();
		try {
			int start = (page - 1) * pageSize;
			Page<BlacklistPAXRuleTO> blacklistPage = blacklistPAXBD.getBlacklistPAXRules(start, pageSize,
					blacklistPAXRuleSearchTO);

			List<BlacklistPAXRuleTO> blacklistPAXRuleTOs = new ArrayList<BlacklistPAXRuleTO>();
			blacklistPAXRuleTOs.addAll(blacklistPage.getPageData());
			setBlacklistPAXRuleList(getBlacklistPAXRuleList(blacklistPAXRuleTOs));

			records = blacklistPage.getTotalNoOfRecords();
			total = blacklistPage.getTotalNoOfRecords() / pageSize;
			int mod = blacklistPage.getTotalNoOfRecords() % pageSize;

			if (mod > 0) {
				total = this.total + 1;
			}

			msgType = S2Constants.Result.SUCCESS;
		} catch (Exception e) {
			log.error("error occured : " + e.getCause());
			msgType = S2Constants.Result.ERROR;
			message = e.getMessage();

		}
		return S2Constants.Result.SUCCESS;

	}

	public static ArrayList<Map<String, Object>> getBlacklistPAXRuleList(List<BlacklistPAXRuleTO> blacklistRuleList) {
		ArrayList<Map<String, Object>> blacklistPAXRuleTOList = new ArrayList<Map<String, Object>>();
		BlacklistPAXRuleTO blacklistPAXRuleTO = null;
		int i = 0;
		Iterator<BlacklistPAXRuleTO> iterator = blacklistRuleList.iterator();
		while (iterator.hasNext()) {
			blacklistPAXRuleTO = iterator.next();
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("blacklistPAXRuleTO", blacklistPAXRuleTO);
			row.put("id", i++);
			blacklistPAXRuleTOList.add(row);
		}

		return blacklistPAXRuleTOList;
	}

	public String save() {
		BlacklistPAXBD blacklistPAXBD = ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD();
		try {
			blacklistPAXBD.saveBlacklistRule(blacklistPAXRuleTO);

		} catch (Exception e) {
			log.error("error occured : " + e.getCause());
			msgType = S2Constants.Result.ERROR;
			message = e.getMessage();
		}
		return S2Constants.Result.SUCCESS;

	}
	

	public String check() {
		BlacklistPAXBD blacklistPAXBD = ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD();
		try {
			setBlacklistRulesExsistence(blacklistPAXBD.checkRuleAvailability(blacklistPAXRuleTO.getStrRules(), blacklistPAXRuleTO.getRuleId()));

		} catch (Exception e) {
			log.error("error occured : " + e.getCause());
			msgType = S2Constants.Result.ERROR;
			message = e.getMessage();
		}
		return S2Constants.Result.SUCCESS;

	}

	public BlacklistPAXRuleSearchTO getBlacklistPAXRuleSearchTO() {
		return blacklistPAXRuleSearchTO;
	}

	public void setBlacklistPAXRuleSearchTO(BlacklistPAXRuleSearchTO blacklistPAXRuleSearchTO) {
		this.blacklistPAXRuleSearchTO = blacklistPAXRuleSearchTO;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public ArrayList<Map<String, Object>> getBlacklistPAXRuleList() {
		return blacklistPAXRuleList;
	}

	public void setBlacklistPAXRuleList(ArrayList<Map<String, Object>> blacklistPAXRuleList) {
		this.blacklistPAXRuleList = blacklistPAXRuleList;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public String getErrorList() {
		return errorList;
	}

	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public BlacklistPAXRuleTO getBlacklistPAXRuleTO() {
		return blacklistPAXRuleTO;
	}

	public void setBlacklistPAXRuleTO(BlacklistPAXRuleTO blacklistPAXRuleTO) {
		this.blacklistPAXRuleTO = blacklistPAXRuleTO;
	}

	public boolean isBlacklistRulesExsistence() {
		return blacklistRulesExsistence;
	}

	public void setBlacklistRulesExsistence(boolean blacklistRulesExsistence) {
		this.blacklistRulesExsistence = blacklistRulesExsistence;
	}

}
