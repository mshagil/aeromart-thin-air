package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CalendarUtil;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PromotionRequestsSummaryAction {

	private static Log log = LogFactory.getLog(PromotionRequestsSummaryAction.class);

	// in
	private String requestStatus;
	private String flightNo;
	private String startDate;
	private String endDate;

	// out
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;

	public String execute() {

		int pageSize = 20;
		int startRec = (page - 1) * pageSize;

		try {

			Date start = startDate != null ? AiradminUtils.getFormattedDate(startDate) : CalendarUtil
					.getStartTimeOfDate(new Date());
			Date end = endDate != null ? AiradminUtils.getFormattedDate(endDate) : CalendarUtil.getEndTimeOfDate(new Date());
			Page results = ModuleServiceLocator.getPromotionAdministrationBD().getAuditView(flightNo, requestStatus, start, end, startRec,
					pageSize);

			this.records = results.getTotalNoOfRecords();
			this.total = results.getTotalNoOfRecords() / pageSize;
			int mod = results.getTotalNoOfRecords() % pageSize;
			if (mod > 0) {
				this.total = this.total + 1;
			}

			Collection<Object> recs = results.getPageData();
			rows = new ArrayList<Map<String, Object>>();
			int a = 1;
			Map<String, Object> row;
			for (Object rec : recs) {
				row = new HashMap<String, Object>();
				row.put("id", ((page - 1) * pageSize) + a++);
				row.put("view", rec);
				rows.add(row);
			}

		} catch (Exception e) {
			log.error("execute ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String initSearch() {
		return S2Constants.Result.SUCCESS;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public int getTotal() {
		return total;
	}
}
