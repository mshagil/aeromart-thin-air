package com.isa.thinair.airadmin.core.web.action.tools;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.jfree.util.Log;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AuditPFSHistroyAction extends BaseRequestAwareAction {

	private String action;
	private String userNote;
	private long pfsID;

	public String execute() {

		try {
			AuditorBD auditorBD = ModuleServiceLocator.getAuditorServiceBD();
			PFSAuditDTO pfsAuditDTO = createPfsAuditDTO();
			auditorBD.preparePFSAudit(pfsAuditDTO, getPfsID(), getAction(), "User viewed the PFS entry.");
		} catch (Exception e) {
			// TODO: handle exception
			Log.error("Error occured while trying to view pfs audits ",e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private PFSAuditDTO createPfsAuditDTO() {
		PFSAuditDTO pfsAuditDTO = new PFSAuditDTO(((UserPrincipal) request.getUserPrincipal()).getIpAddress(),
				((UserPrincipal) request.getUserPrincipal()).getUserId(), this.getUserNote(), null);
		return pfsAuditDTO;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setPfsID(long pfsID) {
		this.pfsID = pfsID;
	}

	public long getPfsID() {
		return pfsID;
	}

}
