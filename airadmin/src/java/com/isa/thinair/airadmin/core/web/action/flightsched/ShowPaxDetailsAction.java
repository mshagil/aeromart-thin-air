package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.FlightConnectionDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "") })
public class ShowPaxDetailsAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(ShowPaxDetailsAction.class);
	private static final String PARAM_FLIGHT_ID = "hdnFlightId";
	private List<ReservationPaxDetailsDTO> pnrPaxMap = new ArrayList<ReservationPaxDetailsDTO>();

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String errorMsg;

	public String execute() throws Exception {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			getFlightSegPaxDetails(request);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Error Occured While Retriveing Data From the Data Base" + e);
				setErrorMsg("error");
			}
		}
		return forward;
	}

	public void getFlightSegPaxDetails(HttpServletRequest request) throws ModuleException {
		String flightId = request.getParameter(PARAM_FLIGHT_ID);
		if (getPage() <= 0) {
			setPage(1);
		}
		setPnrPaxMap(ModuleServiceLocator.getReservationBD().getPAXDetails(new Integer(flightId), getPage()));
		setRecords(ModuleServiceLocator.getReservationBD().getPAXDetailsCount(new Integer(flightId)));
		if (getPnrPaxMap().size() > 0) {
			Object[] dataRow = new Object[getPnrPaxMap().size()];
			int index = 1;
			Iterator<ReservationPaxDetailsDTO> iter = getPnrPaxMap().iterator();

			while (iter.hasNext()) {
				Map<String, Object> counmap = new HashMap<String, Object>();
				ReservationPaxDetailsDTO reservationPaxDetailsDTO = iter.next();
				counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
				counmap.put("paxDetail", reservationPaxDetailsDTO);

				String firstName = reservationPaxDetailsDTO.getFirstName();
				String lastName = reservationPaxDetailsDTO.getLastName();
				String title = reservationPaxDetailsDTO.getTitle() == null ? "" : reservationPaxDetailsDTO.getTitle();
				String pnr = reservationPaxDetailsDTO.getPnr();
				Integer pnrSegId = reservationPaxDetailsDTO.getPnrSegId();
				Integer flgSedId = reservationPaxDetailsDTO.getFlightSegId();
				Integer journeySec = reservationPaxDetailsDTO.getJourneySequence();
				Integer voucherToFlgSegId = reservationPaxDetailsDTO.getVoucherToFlightSegID();
				if (voucherToFlgSegId != null) {
					if (voucherToFlgSegId.equals(0)) {
						counmap.put("reprotect", "N");
					} else {
						if (flgSedId.equals(voucherToFlgSegId)) {
							counmap.put("reprotect", "Y");
						} else {
							counmap.put("reprotect", "N");
						}

					}
				} else {
					counmap.put("reprotect", "N");
				}
			
				FlightConnectionDTO connectionFlight = getConnectionFlightDetails(pnr, pnrSegId, journeySec);
				String displayNote = getConnectionFlightDisplayNote(connectionFlight);
				counmap.put("conection", displayNote);

				counmap.put("paxName", title + " " + firstName + " " + lastName);
				dataRow[index - 1] = counmap;
				index++;
			}

			setRows(dataRow);
			setTotal(getRecords() / 20);
			int mod = getRecords() % 20;
			if (mod > 0) {
				setTotal(getTotal() + 1);
			}
		}
	}

	private String getConnectionFlightDisplayNote(FlightConnectionDTO connectionFlight) {
		StringBuilder displayNote = new StringBuilder();
		if (connectionFlight != null) {
			String flightNumber = connectionFlight.getFligthNumber();
			displayNote.append(flightNumber);
			displayNote.append(WebConstants.Delimeters.COMMA_WITH_SPACE);

			Date localDepartureTime = connectionFlight.getLocalDepartureTime();
			String formattedDate = CalendarUtil.formatDate(localDepartureTime, CalendarUtil.PATTERN11);
			displayNote.append(formattedDate);
			displayNote.append(" ");
			displayNote.append(WebConstants.LOCALTIME);
			displayNote.append(WebConstants.Delimeters.COMMA_WITH_SPACE);

			if (connectionFlight.isInbound()) {
				displayNote.append(WebConstants.RouteDesignators.INBOUND);
			} else {
				displayNote.append(WebConstants.RouteDesignators.OUTBOUND);
			}
		}
		return displayNote.toString();
	}

	public FlightConnectionDTO getConnectionFlightDetails(String pnr, int currentPnrSegId, int journeySec) throws ModuleException {
		List<FlightConnectionDTO> flightConnectionDTOs = ModuleServiceLocator.getReservationBD().getConnectionDetials(pnr);

		FlightConnectionDTO currentFlight = null;
		FlightConnectionDTO connectionFlight = null;

		for (FlightConnectionDTO flightConnectionDTO : flightConnectionDTOs) {
			Integer pnrSegIdTemp = flightConnectionDTO.getPnrSegId();
			if (pnrSegIdTemp == currentPnrSegId) {
				currentFlight = flightConnectionDTO;
			}
		}

		if (currentFlight != null) {
			for (FlightConnectionDTO flightConnectionDTO : flightConnectionDTOs) {
				Integer pnrSegIdTemp = flightConnectionDTO.getPnrSegId();
				Integer journeySecTemp = flightConnectionDTO.getJourney_seq();

				String currentSegementCode = currentFlight.getSegmentCode();
				String currentDeparture = SegmentUtil.getFromAirport(currentSegementCode);
				String currentArrival = SegmentUtil.getToAirport(currentSegementCode);

				String segmentCode = flightConnectionDTO.getSegmentCode();
				String departueCode = SegmentUtil.getFromAirport(segmentCode);
				String arrivalCode = SegmentUtil.getToAirport(segmentCode);

				if (pnrSegIdTemp != currentPnrSegId && journeySecTemp.equals(journeySec)) {
					if (currentArrival.equals(departueCode)
							&& currentFlight.getArrivalTime().before(flightConnectionDTO.getDepartureTime())) {
						flightConnectionDTO.setInbound(false);
						connectionFlight = flightConnectionDTO;
						break;
					} else if (currentDeparture.equals(arrivalCode)
							&& currentFlight.getDepartureTime().after(flightConnectionDTO.getArrivalTime())) {
						flightConnectionDTO.setInbound(true);
						connectionFlight = flightConnectionDTO;
						break;
					}

				}
			}
		}
		return connectionFlight;
	}

	public List<ReservationPaxDetailsDTO> getPnrPaxMap() {
		return pnrPaxMap;
	}

	public void setPnrPaxMap(List<ReservationPaxDetailsDTO> pnrPaxMap) {
		this.pnrPaxMap = pnrPaxMap;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
