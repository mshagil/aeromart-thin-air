package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
public class ShowAgentChargeAdjustmentsReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ShowAgentChargeAdjustmentsReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static final String PARAM_AGENTS = "hdnAgents";

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ShowAgentChargeAdjustmentsReportRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setDisplayAgencyMode(request);
		setClientErrors(request);
		setLiveStatus(request);
		setGSAMultiSelectList(request);
		setAgentTypes(request);
		setReportingPeriod(request);

		setUserMultiSelectList(request, false);
	}

	protected static void setUserMultiSelectList(HttpServletRequest request, boolean loadFromAgent) throws ModuleException {
		String strAgents = null;

		if (null != request.getParameter(PARAM_AGENTS) && !request.getParameter(PARAM_AGENTS).equals("")) {
			strAgents = request.getParameter(PARAM_AGENTS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			strAgents = getAttribInRequest(request, "currentAgentCode").toString();
		}
		String strList = ReportsHTMLGenerator.createUserAgentMultiSelect(strAgents);
		request.setAttribute(WebConstants.REQ_HTML_AGENT_USER_LIST, strList);
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {

		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.res.ohp.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.res.ohp.gsa") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.res.ohp") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {

		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String selectedCarriers = request.getParameter("hdnSelectedCarriers");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		String chkPayTypeR = request.getParameter("chkTypeR");
		String chkPayTypeC = request.getParameter("chkTypeC");
		String chkPayTypeD = request.getParameter("chkTypeD");
		String chkPayTypeRR = request.getParameter("chkTypeRR");
		String chkPayTypeA = request.getParameter("chkTypeA");
		String chkPayTypeRV = request.getParameter("chkTypeRV");
		String chkPayTypeQ = request.getParameter("chkTypeQ");
		String chkPayTypeBSP = request.getParameter("chkTypeBSP");

		String agents = "";
		String users = "";
		String id = "UC_REPM_082";
		String reportTemplate = "agentChargeAdjustmentsReport.jasper";

		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
				agents = (String) getAttribInRequest(request, "currentAgentCode");
			} else {
				agents = request.getParameter("hdnAgents");
			}
			String agentArr[] = agents.split(",");
			ArrayList<String> agentCol = new ArrayList<String>();
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
			users = request.getParameter("hdnUsers");
			ArrayList<String> userCol = new ArrayList<String>();
			if (!"".equals(users)) {
				String userArr[] = users.split(",");
				for (int r = 0; r < userArr.length; r++) {
					userCol.add(userArr[r]);
				}
			}

			if (getNotNull(fromDate) && getNotNull(toDate)) {
				fromDate = fromDate + " 00:00:00";
				toDate = toDate + " 23:59:59";
			}

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom("'" + fromDate + "' , 'DD/MM/YYYY HH24:MI:SS'");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo("'" + toDate + "' , 'DD/MM/YYYY HH24:MI:SS'");
			}

			Collection<Integer> trnsColl = new HashSet<Integer>();

			if (getNotNull(chkPayTypeR))
				trnsColl.add(AgentTransactionNominalCode.ACC_SALE.getCode());
			if (getNotNull(chkPayTypeC))
				trnsColl.add(AgentTransactionNominalCode.ACC_CREDIT.getCode());
			if (getNotNull(chkPayTypeD))
				trnsColl.add(AgentTransactionNominalCode.ACC_DEBIT.getCode());
			if (getNotNull(chkPayTypeRR))
				trnsColl.add(AgentTransactionNominalCode.ACC_REFUND.getCode());
			if (getNotNull(chkPayTypeA))
				trnsColl.add(AgentTransactionNominalCode.PAY_INV_CASH.getCode());
			if (getNotNull(chkPayTypeQ))
				trnsColl.add(AgentTransactionNominalCode.PAY_INV_CHQ.getCode());
			if (getNotNull(chkPayTypeRV))
				trnsColl.add(AgentTransactionNominalCode.ACC_PAY_REVERSE.getCode());
			if (getNotNull(chkPayTypeBSP))
				trnsColl.add(AgentTransactionNominalCode.BSP_SETTLEMENT.getCode());

			search.setAgents(agentCol);
			search.setAgentCode(agents);
			search.setUsers(userCol);

			if (selectedCarriers != null && !selectedCarriers.isEmpty()) {
				List<String> carrierList = Arrays.asList(selectedCarriers.split(","));
				if (!carrierList.contains("ALL")) {
					search.setCarrierCodes(carrierList);
				}
			}

			search.setNominalCodes(trnsColl);
			
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getAgentChargeAdjustmentsData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			
			if(AppSysParamsUtil.displayCreditDebitEntriesInOtherWay()){
				parameters.put("ENABLE_REV", new Boolean(true));
			}else{
				parameters.put("ENABLE_REV", new Boolean(false));
			}
			
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=OnHoldPassengerReportForFlight.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static boolean getNotNull(String str) {
		return ((str != null) && !(str.trim().equals(""))) ? true : false;
	}
}
