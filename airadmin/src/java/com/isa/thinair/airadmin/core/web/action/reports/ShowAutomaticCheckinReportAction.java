package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.AutomaticCheckinReportRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_AUTOMATIC_CHECKING_REPORT_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowAutomaticCheckinReportAction extends BaseRequestResponseAwareAction{

	public String execute() throws Exception {
		return AutomaticCheckinReportRH.execute(request, response);
	}
}
