package com.isa.thinair.airadmin.core.web.handler.flightsched;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.flightsched.FlightHTMLGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.FlightUtils;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.dto.airportTransfer.PaxAirportTransferTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.OverlappedFlightDTO;
import com.isa.thinair.airschedules.api.dto.OverlappedScheduleDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author Thushara
 * 
 */

public final class FlightRequestHandler extends BasicRequestHandler {

	private static final String PARAM_COPY_DATE = "hdnCopyToDay";
	private static final String PARAM_FLIGHTID = "hdnFlightId";
	private static final String PARAM_TIMEMODE = "hdnTimeMode";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_MAIN_MODE = "hdnMainMode";
	private static final String PARAM_FLIGHT_NO = "hdnFlightNo";
	private static final String PARAM_FLIGHT_DEPARTURE_DATE = "hdnFlightDate";

	private static final String PARAM_REPROTECTALERT = "hdnCancelAlert";
	private static final String PARAM_REPROTECTEMAIL = "hdnCancelEmail";
	private static final String PARAM_RESCHEDULEALERT = "hdnReSchedAlert";
	private static final String PARAM_RESCHEDULEEMAIL = "hdnReSchedEmail";

	private static final String PARAM_EMAILTO = "hdnEmailTo";
	private static final String PARAM_EMAILSUBJECT = "hdnEmailSubject";
	private static final String PARAM_EMAILCONTENT = "hdnEmailContent";
	private static final String PARAM_REPROTECT = "hdnReprotect";

	private static final String PARAM_SMSALERT = "hdnSMSAlert";
	private static final String PARAM_EMAILALERT = "hdnEmailAlert";
	private static final String PARAM_SMSCNF = "hdnSmsCnf";
	private static final String PARAM_CMSONH = "hdnSmsOnH";

	private static final String PARAM_SHOW_RES_HISTORY = "hdnShowResHistory";	
	private static final String PARAM_VIEW_AUDIT_TYPE = "hdnAuditType";
	
	private static final String RECEIVED_MESSAGES = "IN_MSG";
	private static final String PUBLISHED_MESSAGES = "PUB_MSG";


	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static SimpleDateFormat dateFormatReverse = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private static Log log = LogFactory.getLog(FlightRequestHandler.class);

	private static String strErrorMEssage = "";
	private static boolean isSaveError = false;

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static AiradminModuleConfig adminConfig = AiradminModuleUtils.getConfig();
	private static String strHdnMainMode = "";
	private static final String IS_UPDATE_ETICKET_STATUS = "hdnOpenEtStatus";

	/**
	 * Main Execute Method for Flight Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String displayMessage = "";
		strErrorMEssage = "";
		isSaveError = false;
		String strHdnMode = request.getParameter(PARAM_MODE);
		String hdnAuditType = request.getParameter(PARAM_VIEW_AUDIT_TYPE);

		if (request.getParameter(PARAM_MAIN_MODE) != null) {
			strHdnMainMode = request.getParameter(PARAM_MAIN_MODE);
		}

		try {
			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_VIEW)) {
					if (RECEIVED_MESSAGES.equals(hdnAuditType)) {
						viewFlightMsgReport(request, response);
						return null;
					} else if (PUBLISHED_MESSAGES.equals(hdnAuditType)) {
						viewFlightPublishedMessagesAudit(request, response);
						return null;
					} else {
						setReportView(request, response);
					}
				} else {
					if (strHdnMode.equals(WebConstants.ACTION_UPDATE) && strHdnMainMode.equals("isNeedChecked")) {
						displayMessage = checkData(request);
						if (!displayMessage.equals("")) {
							strErrorMEssage = "This operation leads to flight overbook. Please click save to update the flight";
							saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
							strHdnMainMode = "";
						}
					} else {
						forward = saveData(request);
					}
				}
			}

		} catch (ModuleException moduleException) {
			log.error("Exception in FlightRequestHandler.execute [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			strErrorMEssage = moduleException.getMessageString();
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
			try {
				setModel(request, "");
			} catch (Exception e) {

			}
		} catch (Exception e) {
			log.error("Exception in FlightRequestHandler.execute", e);
			isSaveError = true;
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				strErrorMEssage = e.getMessage();
				saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			}
			try {
				setModel(request, "");
			} catch (Exception ex) {

			}
		}

		try {
			if (!forward.equalsIgnoreCase(WebConstants.ACTION_FORWARD_REPROTECT)) {
				setDisplayData(request, displayMessage);
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in FlightRequestHandler.execute " + "- setDisplayData(request) [origin module="
					+ moduleException.getModuleDesc() + "]", moduleException);

			if (!isSaveError) {
				strErrorMEssage = moduleException.getMessageString();
			}

			request.setAttribute("txtFlightNoStartSearch", "");
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error("Exception in FlightRequestHandler.execute " + "- setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				if (!isSaveError) {
					strErrorMEssage = e.getMessage();
				}
				saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			}

		}
		return forward;
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException,
			ParseException {
		Properties properties = FlightUtils.getProperties(request);
		String flightId = properties.getProperty(PARAM_FLIGHTID);
		String flightNo = properties.getProperty(PARAM_FLIGHT_NO);
		Date fromDate = CalendarUtil
				.getStartTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("fromDate"), "dd/MM/yyyy"));
		Date toDate = CalendarUtil.getEndTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("toDate"), "dd/MM/yyyy"));
		String strFromDate = CalendarUtil.getFormattedDate(fromDate, "dd/MM/yyyy HH:mm:ss");
		String strToDate = CalendarUtil.getFormattedDate(toDate, "dd/MM/yyyy HH:mm:ss");

		String flightDate = dateFormat.format(dateFormatReverse.parse(properties.getProperty(PARAM_FLIGHT_DEPARTURE_DATE)));
		String reverseDate = dateFormatReverse
				.format(dateFormatReverse.parse(properties.getProperty(PARAM_FLIGHT_DEPARTURE_DATE)));
		Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(flightId));
		
		boolean showResHistory = request.getParameter(PARAM_SHOW_RES_HISTORY) != null
				&& request.getParameter(PARAM_SHOW_RES_HISTORY).equals("true");
		
		ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getFlightHistoryDetailsReport(flightId, flightNo,
				flightDate, reverseDate, flight.getScheduleId(), strFromDate, strToDate, showResHistory);

		String reportTemplate = "FlightHistoryReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(true);
		String strPath = "../../images/" + strLogo;
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String reportsRootDir = "../../images/" + strLogo;

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("FLIGHTID", flightId);
		parameters.put("REPORTTYPE", WebConstants.REPORT_HTML);
		parameters.put("IMG", strPath);
		parameters.put("IMG", reportsRootDir);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));
		ModuleServiceLocator.getReportingFrameworkBD()
				.createHTMLReport("WebReport1", parameters, resultSet, null, null, response);
	}

	/**
	 * Saves the Flight and Return the Response
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the response
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws ParseException
	 *             the ParseException
	 */
	private static String saveData(HttpServletRequest request) throws ModuleException, ParseException {

		Flight flight = null;
		FlightAllowActionDTO flightAllowActionDTO = new FlightAllowActionDTO();
		Properties props = FlightUtils.getProperties(request);
		String strMode = props.getProperty(PARAM_MODE);
		String strTimeMode = props.getProperty(PARAM_TIMEMODE);

		boolean blnAlert = getBooleanValue(request.getParameter(PARAM_REPROTECTALERT));
		boolean blnEmail = getBooleanValue(request.getParameter(PARAM_REPROTECTEMAIL));
		boolean blnReschedAlert = getBooleanValue(request.getParameter(PARAM_RESCHEDULEALERT));
		boolean blnReschedEmail = getBooleanValue(request.getParameter(PARAM_RESCHEDULEEMAIL));
		boolean blnTimeLocal = strTimeMode.equals(WebConstants.LOCALTIME);
		String strEmailTo = request.getParameter(PARAM_EMAILTO);
		String strEmailSubject = request.getParameter(PARAM_EMAILSUBJECT);
		String strEmailContent = request.getParameter(PARAM_EMAILCONTENT);
		String flightId = props.getProperty(PARAM_FLIGHTID);

		boolean blnEmailAlert = getBooleanValue(request.getParameter(PARAM_EMAILALERT));
		boolean blnSmsAlert = getBooleanValue(request.getParameter(PARAM_SMSALERT));
		boolean blnSmsCnf = getBooleanValue(request.getParameter(PARAM_SMSCNF));
		boolean blnSmsOnH = getBooleanValue(request.getParameter(PARAM_CMSONH));
		String chargeTemplate = request.getParameter("selSeatChargeTemplate");
		// declaring the Service response object
		ServiceResponce serviseResponse = null;
		String strResponseMessage = "success";
		boolean isAllowUpdateEticketStatusForAllPax = getBooleanValue(
				request.getParameter(IS_UPDATE_ETICKET_STATUS));

		// if action was add
		if (WebConstants.ACTION_ADD.equals(strMode)) {
			checkOrConditionForPrivileges(request, WebConstants.PLAN_FLIGHT_MANAGE_ADD_ANY_FLIGHT,
					WebConstants.PLAN_FLIGHT_MANAGE_ADD_OPERATIONS_FLIGHT);
			flight = FlightUtils.createFlight(props);
			flightAllowActionDTO = FlightUtils.createAllowActions(props);
			CommonUtil.checkCarrierCodeAccessibility(getUserCarrierCodes(request),
					CommonUtil.getCarrierCode(flight.getFlightNumber()));
			serviseResponse = ModuleServiceLocator.getFlightServiceBD().createFlight(flightAllowActionDTO, flight, false);
		} else if (strMode.equals(WebConstants.ACTION_UPDATE)) {
			// if action was update
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_MANAGE_EDIT);
			flight = FlightUtils.createFlight(props);
			flight.setFlightId(new Integer(flightId));
			flightAllowActionDTO = FlightUtils.createAllowActions(props);
			FlightAlertDTO fltAltDTO = new FlightAlertDTO();
			fltAltDTO.setAlertForRescheduledFlight(blnReschedAlert);
			fltAltDTO.setEmailForRescheduledFlight(blnReschedEmail);

			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
			fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
			fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
			fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);

			if (blnReschedEmail) {
				fltAltDTO.setEmailTo(strEmailTo);
				fltAltDTO.setEmailSubject(strEmailSubject);
				fltAltDTO.setEmailContent(strEmailContent);
			}
			CommonUtil.checkCarrierCodeAccessibility(getUserCarrierCodes(request),
					CommonUtil.getCarrierCode(flight.getFlightNumber()));

			Collection<Integer> getPaxFareSegETIds = null;
			if (isAllowUpdateEticketStatusForAllPax) {
				getPaxFareSegETIds = FlightUtil
						.getPaxFareSegIdsInPastFlight(request.getParameter(WebConstants.REQ_HDN_FLIGHT_ID));
			}

			serviseResponse = ModuleServiceLocator.getFlightServiceBD()
					.updateFlight(flightAllowActionDTO, flight, fltAltDTO,
							hasPrivilege(request, WebConstants.PLAN_FLIGHT_DOWNGRADE_TO_ANY_MODEL),
							hasPrivilege(request, WebConstants.PLAN_FLIGHT_ACTIVATE_PAST_FLIGHT), false, false);

			//if updated flight is past, then open all the pax and E-tickets status
			if ((serviseResponse.isSuccess()) && isAllowUpdateEticketStatusForAllPax && !CollectionUtils
					.isEmpty(getPaxFareSegETIds)) {
				PastFlightInfoDTO pastFlightInfoDTO = new PastFlightInfoDTO();
				pastFlightInfoDTO.setFlightNumber(request.getParameter(WebConstants.REQ_HDN_FLIGHT_NO));
				pastFlightInfoDTO.setDepartureDate(request.getParameter(WebConstants.REQ_TXT_DEPT_DATE));
				pastFlightInfoDTO.setFlightId(request.getParameter(WebConstants.REQ_HDN_FLIGHT_ID));
				pastFlightInfoDTO.setPaxFareSegETIds(getPaxFareSegETIds);
				pastFlightInfoDTO.setHdnMode(strMode);
				FlightUtil.updatePaxETStatus(pastFlightInfoDTO);
			}

		} else if (WebConstants.ACTION_CANCEL.equals(strMode)
				&& (Boolean.valueOf(request.getParameter(PARAM_REPROTECT)).booleanValue())) {
			// if action is cancel and refreshing the cancellation screen when reprotect
			// NOTE : This method should always be called before cancel
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_MANAGE_CANCEL);
			flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(flightId));

			serviseResponse = ModuleServiceLocator.getFlightServiceBD().getFlightReservationsForCancel(
					Integer.parseInt(flightId), false);
		} else if (WebConstants.ACTION_CANCEL.equals(strMode)) {
			// if action was cancel
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_MANAGE_CANCEL);
			flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(flightId));
			CommonUtil.checkCarrierCodeAccessibility(getUserCarrierCodes(request),
					CommonUtil.getCarrierCode(flight.getFlightNumber()));
			serviseResponse = ModuleServiceLocator.getFlightServiceBD().cancelFlight(Integer.parseInt(flightId), false,
					new FlightAlertDTO());
		} else if (WebConstants.ACTION_CONFIRM_CANCEL.equals(strMode)) {
			// if action was confirm cancel
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_MANAGE_CANCEL);
			FlightAlertDTO fltAltDTO = new FlightAlertDTO();
			fltAltDTO.setAlertForCancelledFlight(blnAlert);
			fltAltDTO.setEmailForCancelledFlight(blnEmail);

			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
			fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
			fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
			fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
			if (blnEmail) {
				fltAltDTO.setEmailTo(strEmailTo);
				fltAltDTO.setEmailSubject(strEmailSubject);
				fltAltDTO.setEmailContent(strEmailContent);
			}
			serviseResponse = ModuleServiceLocator.getFlightServiceBD().cancelFlight(
					Integer.parseInt(props.getProperty(PARAM_FLIGHTID)), true, fltAltDTO);
			flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(flightId));
			cancelBookedAirportTransfers(flight);
		} else if (WebConstants.ACTION_COPY.equals(strMode)) {
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_MANAGE_COPY);
			Date dateCopyDate = null;
			dateCopyDate = ScheduleUtils.parseToDate(props.getProperty(PARAM_COPY_DATE));
			flightAllowActionDTO = FlightUtils.createAllowActions(props);

			// calling the backend
			serviseResponse = ModuleServiceLocator.getFlightServiceBD().copyFlight(flightAllowActionDTO,
					Integer.parseInt(flightId), dateCopyDate, blnTimeLocal);
		} else if ("SAVEMANAGE".equals(strMode)) {
			updateFlightStatus(request);
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			// request.setAttribute(WebConstants.POPUPMESSAGE,
			// airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_SAVE_SUCCESS));
		}
		if (serviseResponse != null) {
			strResponseMessage = setResponsePage(request, serviseResponse, flight);
		}
		return strResponseMessage;
	}

	/**
	 * Sets the Response to the Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param respon
	 *            the ServiceResponce of the Flight Action
	 * @param flight
	 *            the Flight created
	 * @return String success or Error
	 */
	@SuppressWarnings("unchecked")
	private static String setResponsePage(HttpServletRequest request, ServiceResponce respon, Flight flight) {

		String strResponseMessage = "success";
		String strWarnMessage = "";
		Properties props = FlightUtils.getProperties(request);
		String strTimeMode = props.getProperty(PARAM_MODE);
		String responceCode = (respon.getResponseCode() == null) ? "" : respon.getResponseCode();

		if (respon.isSuccess()) {

			if (responceCode.equals(ResponceCodes.CREATE_FLIGHT_SUCCESSFULL)) {
				if (strTimeMode != null && strTimeMode.equals(WebConstants.ACTION_COPY)) {
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_COPY_SUCCESS), WebConstants.MSG_SUCCESS);
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_COPY_SUCCESS));
				} else {

					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_SAVE_SUCCESS));
				}
			} else if (responceCode.equals(ResponceCodes.AMMEND_FLIGHT_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_UPDATE_SUCCESS));

			} else if (responceCode.equals(ResponceCodes.COPY_FLIGHT_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_COPY_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE, airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_COPY_SUCCESS));

			} else if (responceCode.equals(ResponceCodes.CANCEL_FLIGHT_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_DELETE_SUCCESS));
			}
		} else if (responceCode.equals(ResponceCodes.WARNING_FOR_FLIGHT_UPDATE)) {

			if (respon.getResponseParam(ResponceCodes.FLIGHT_FLIGHT_CONFLICT) != null) {

				Collection<OverlappedFlightDTO> col = (Collection<OverlappedFlightDTO>) respon
						.getResponseParam(ResponceCodes.FLIGHT_FLIGHT_CONFLICT);
				Iterator<OverlappedFlightDTO> iter = col.iterator();
				OverlappedFlightDTO flightdto = iter.next();
				strWarnMessage = "coflict,Flight No " + flightdto.getFlightNumber()
						+ " already scheduled for the same day. Do you want to continue?";
				request.setAttribute("warningmessage", strWarnMessage);

			} else if (respon.getResponseParam(ResponceCodes.SCHEDULE_FLIGHT_CONFLICT) != null) {

				Collection<OverlappedScheduleDTO> col = (Collection<OverlappedScheduleDTO>) respon
						.getResponseParam(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT);
				Iterator<OverlappedScheduleDTO> iter = col.iterator();
				OverlappedScheduleDTO schedDto = iter.next();
				strWarnMessage = "coflict,Conflict With the Schedule " + schedDto.getScheduleId() + " Do You Want to Continue ?";
				request.setAttribute("warningmessage", strWarnMessage);

			} else if (respon.getResponseParam(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT) != null) {

				Collection<OverlappedScheduleDTO> col = (Collection<OverlappedScheduleDTO>) respon
						.getResponseParam(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT);
				Iterator<OverlappedScheduleDTO> iter = col.iterator();
				OverlappedScheduleDTO schedDto = iter.next();
				strWarnMessage = "coflict,Conflict With the Schedule " + schedDto.getScheduleId() + " Do You Want to Continue ?";
				request.setAttribute("warningmessage", strWarnMessage);

			} else if (respon.getResponseParam(ResponceCodes.INVALID_LEG_DURATION) != null) {
				// strWarnMessage = "duration,Duration Error Do you want to Continue any way?";
				strWarnMessage = "";
				request.setAttribute("warningmessage", strWarnMessage);

				// to show the duration error
				Flight flt = (Flight) respon.getResponseParam(ResponceCodes.INVALID_LEG_DURATION);

				Collection<FlightLeg> legs = flt.getFlightLegs();
				Iterator<FlightLeg> itLegs = legs.iterator();
				String strError = "";
				String strDurations = "";

				while (itLegs.hasNext()) {
					FlightLeg leg = (FlightLeg) itLegs.next();
					strError = strError + "(" + leg.getLegNumber() + "," + leg.getDurationError() + ")";
					strDurations = strDurations + leg.getLegNumber() + "," + leg.getDurationError() + "," + leg.getDuration()
							+ ",";
				}

				saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_INVALID_LEG) + strError,
						WebConstants.MSG_ERROR);
				request.setAttribute(WebConstants.DURATIONERROR, strDurations);

			} else if (respon.getResponseParam(ResponceCodes.CC_CAPACITY_INSUFFICIENT) != null) {
				strWarnMessage = airadminConfig.getMessage(
						WebConstants.CC_CAPACITY_INSUFFICIENT,
						new Object[] { respon.getResponseParam(ResponceCodes.CC_CAPACITY_INSUFFICIENT),
								respon.getResponseParam(ResponceCodes.DATE) });
				request.setAttribute("warningmessage", strWarnMessage);
			} else if (respon.getResponseParam(ResponceCodes.CABIN_CLASS_NOT_FOUND) != null) {
				strWarnMessage = airadminConfig.getMessage(
						WebConstants.CABIN_CLASS_NOT_FOUND,
						new Object[] { respon.getResponseParam(ResponceCodes.CABIN_CLASS_NOT_FOUND),
								respon.getResponseParam(ResponceCodes.DATE) });
				request.setAttribute("warningmessage", strWarnMessage);
			} else if (respon.getResponseParam(ResponceCodes.CANNOT_DELETE_BC_INVENTORY) != null) {
				strWarnMessage = airadminConfig.getMessage(
						WebConstants.CANNOT_DELETE_BC_INVENTORY,
						new Object[] { respon.getResponseParam(ResponceCodes.CANNOT_DELETE_BC_INVENTORY),
								respon.getResponseParam(ResponceCodes.DATE) });
				request.setAttribute("warningmessage", strWarnMessage);
			} else if (respon.getResponseParam(ResponceCodes.RESERVATIONS_EXIST_FOR_INVALIDATED_SEGMENT) != null) {
				strWarnMessage = "segment,Reservation exist for invalidated Segment Do You Want to Continue ?";
				request.setAttribute("warningmessage", strWarnMessage);
			} else if (respon.getResponseParam(ResponceCodes.RESERVATIONS_EXIST_FOR_OVERLAP_REMOVED_SEGMENT) != null) {
				strWarnMessage = "overlap,Reservation Exist for the Flight  Do You Want to Continue ?";
				request.setAttribute("warningmessage", strWarnMessage);
			}

			if (responceCode.equals(ResponceCodes.FLIGHT_FLIGHT_CONFLICT)) {

			} else if (responceCode.equals(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT)) {
				// allow conflicts
			}

		} else if (responceCode.equals(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT)) {

			request.setAttribute(WebConstants.REQ_MODEL, flight);
			Collection<OverlappedScheduleDTO> col = (Collection<OverlappedScheduleDTO>) respon
					.getResponseParam(ResponceCodes.FLIGHT_SCHEDULE_CONFLICT);
			Iterator<OverlappedScheduleDTO> iter = col.iterator();
			OverlappedScheduleDTO schedDto = iter.next();
			saveMessage(request,
					airadminConfig.getMessage(WebConstants.FLIGHT_SCHEDULE_CONFLICT) + "Flight = " + schedDto.getFlightNumber()
							+ "," + schedDto.getStartDate(), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.FLIGHT_SCHEDULE_CONFLICT) + "Flight = "
					+ schedDto.getFlightNumber() + "," + schedDto.getStartDate();

		} else if (responceCode.equals(ResponceCodes.FLIGHT_FLIGHT_CONFLICT)) {

			request.setAttribute(WebConstants.REQ_MODEL, flight);
			Collection<OverlappedScheduleDTO> col = (Collection<OverlappedScheduleDTO>) respon
					.getResponseParam(ResponceCodes.FLIGHT_FLIGHT_CONFLICT);
			Iterator<OverlappedScheduleDTO> iter = col.iterator();
			OverlappedScheduleDTO flightSch = iter.next();
			saveMessage(request,
					airadminConfig.getMessage(WebConstants.FLIGHT_FLIGHT_CONFLICT) + "Schedule = " + flightSch.getFlightNumber()
							+ "," + flightSch.getStartDate(), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.FLIGHT_FLIGHT_CONFLICT) + "Schedule = "
					+ flightSch.getFlightNumber() + "," + flightSch.getStartDate();

		} else if (responceCode.equals(ResponceCodes.INVALID_LEG_DURATION)) {

			FlightSchedule flsched = (FlightSchedule) respon.getResponseParam(ResponceCodes.INVALID_LEG_DURATION);

			Collection<FlightScheduleLeg> legs = flsched.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itLegs = legs.iterator();
			String strError = "";

			while (itLegs.hasNext()) {
				FlightScheduleLeg leg = itLegs.next();
				strError = strError + "(" + leg.getLegNumber() + "," + leg.getDurationError() + ")";
			}

			saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_INVALID_LEG) + strError, WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_INVALID_LEG) + strError;

		} else if (responceCode.equals(ResponceCodes.AllOWED_DURATION_ZERO)) {

			FlightLeg leg = (FlightLeg) respon.getResponseParam(ResponceCodes.AllOWED_DURATION_ZERO);
			saveMessage(request,
					airadminConfig.getMessage(WebConstants.SCHEDULE_ALLOWED_DURATION_ZERO) + " " + leg.getModelRouteId(),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_ALLOWED_DURATION_ZERO) + " "
					+ leg.getModelRouteId();

		} else if (responceCode.equals(ResponceCodes.ALL_SEGMENTS_INVALID)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.ALL_SEGMENTS_INVALID), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.ALL_SEGMENTS_INVALID);
		} else if (responceCode.equals(ResponceCodes.CABIN_CLASS_NOT_FOUND)) {
			strErrorMEssage = airadminConfig.getMessage(
					WebConstants.CABIN_CLASS_NOT_FOUND,
					new Object[] { respon.getResponseParam(ResponceCodes.CABIN_CLASS_NOT_FOUND),
							respon.getResponseParam(ResponceCodes.DATE) });
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.CANNOT_DELETE_BC_INVENTORY)) {
			strErrorMEssage = airadminConfig.getMessage(
					WebConstants.CANNOT_DELETE_BC_INVENTORY,
					new Object[] { respon.getResponseParam(ResponceCodes.CANNOT_DELETE_BC_INVENTORY),
							respon.getResponseParam(ResponceCodes.DATE) });
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.CC_CAPACITY_INSUFFICIENT)) {
			strErrorMEssage = airadminConfig.getMessage(
					WebConstants.CC_CAPACITY_INSUFFICIENT,
					new Object[] { respon.getResponseParam(ResponceCodes.CC_CAPACITY_INSUFFICIENT),
							respon.getResponseParam(ResponceCodes.DATE) });

			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);

			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.CC_WAITLISTED_CAPACITY_INSUFFICIENT)) {

			strErrorMEssage = airadminConfig.getMessage(ResponceCodes.CC_WAITLISTED_CAPACITY_INSUFFICIENT);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;

		} else if (responceCode.equals(ResponceCodes.CC_CURTAILED_CAPACITY_INSUFFICIENT)) {

			strErrorMEssage = airadminConfig.getMessage(ResponceCodes.CC_CURTAILED_CAPACITY_INSUFFICIENT);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;

		} else if (responceCode.equals(ResponceCodes.CC_INFANT_CAPACITY_INSUFFICIENT)) {

			strErrorMEssage = airadminConfig.getMessage(ResponceCodes.CC_INFANT_CAPACITY_INSUFFICIENT);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;

		} else if (responceCode.equals(ResponceCodes.TRYING_ILLEGAL_FLIGHT_UPDATION)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.TRYING_ILLEGAL_FLIGHT_UPDATION), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.TRYING_ILLEGAL_FLIGHT_UPDATION);
		} else if (responceCode.equals(ResponceCodes.INVALID_OVERLAPPING_FLIGHT)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_FLIGHT), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_FLIGHT);
		} else if (responceCode.equals(ResponceCodes.OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT);
		} else if (responceCode.equals(ResponceCodes.INVALID_OVERLAPPING_FLIGHT_EXISTING_FLIGHT)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_FLIGHT_EXISTING_FLIGHT),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_FLIGHT_EXISTING_FLIGHT);
		} else if (responceCode.equals(ResponceCodes.RESERVATIONS_FOUND_FOR_FLIGHT)) {
			// strResponseMessage = "cancelSchedule";
			HashMap resrMap = (HashMap) respon.getResponseParam(ResponceCodes.RESERVATIONS_FOUND_FOR_FLIGHT);
			FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
			String strCan = htmlGen.getCanReprotectArray(resrMap);
			SimpleDateFormat smdt = new SimpleDateFormat("dd/MM/yyyy");
			request.setAttribute(WebConstants.REQ_SEATS_TO_BE_REPROTECTED, strCan);
			request.setAttribute(WebConstants.REQ_CANCEL_TRUE, WebConstants.CANCEL_TRUE);
			request.setAttribute(WebConstants.REQ_REPRO_FLIGHTID, flight.getFlightId());
			request.setAttribute(WebConstants.REQ_REPRO_FLIGHT_NUMBER, flight.getFlightNumber());
			request.setAttribute(WebConstants.REQ_REPRO_DEP_DATE, smdt.format(flight.getDepartureDate()));
			request.setAttribute(WebConstants.REQ_REPRO_FLT_DESTINATION, flight.getDestinationAptCode());
			request.setAttribute(WebConstants.REQ_REPRO_FLT_ORIGIN, flight.getOriginAptCode());
			if (Boolean.valueOf(request.getParameter(PARAM_REPROTECT)).booleanValue()) {
				strResponseMessage = WebConstants.ACTION_FORWARD_REPROTECT;
			}
		} else if (responceCode.equals(ResponceCodes.AIRCRAFT_UPDATED_MODEL_SEATATTACHED)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.AIRCRAFT_UPDATED_MODEL_SEATATTACHED),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.AIRCRAFT_UPDATED_MODEL_SEATATTACHED);
		} else if (responceCode.equals(ResponceCodes.INVALID_DEPARTURE_DATE)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_DEPARTURE_DATE), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_DEPARTURE_DATE);
		} else if (responceCode.equals(ResponceCodes.CC_FIXED_SEATS_INSUFFICIENT)) {

			saveMessage(request, airadminConfig.getMessage(WebConstants.FIXED_SEATS_INSUFFICIENT), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.FIXED_SEATS_INSUFFICIENT);
		} else {
			saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_UNHANDLED_ERROR), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_UNHANDLED_ERROR);
		}
		return strResponseMessage;
	}

	/**
	 * Sets the Display Data to the Flight Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request, String message) throws ModuleException {
		String strFromPage = request.getParameter("hdnFromPage");
		request.setAttribute("isManageFlt", (strFromPage != null && strFromPage.equals("MANAGE")) ? true : false);
		request.setAttribute("reqMngFlt", (strFromPage != null && strFromPage.equals("MANAGE")) ? "MANAGE" : "ERR");
		request.setAttribute(WebConstants.REQ_FLIFHTS_TO_BE_REPROTECTED, AppSysParamsUtil.isEnablePaxReprotectPostCNX());

		setClientErrors(request);
		setOnlineActiveAirportHtml(request);
		setStopOverTime(request);
		setOperationTypeHtml(request);
		setOperationTypeDetails(request);
		setStatusHtml(request);
		setFlightTypesHtml(request);
		setAircraftModelDetails(request);
		setTimeInModeHtml(request);
		setModelData(request);
		setOnlineAirportHtml(request);
		setUserCarriers(request);
		setModel(request, message);
		setFlightRowHtml(request);
		setPrivilagesHtml(request);
		isFlightUserNoteVisible(request);
		setHasPrivilegeToEditFlNumber(request);
		setHasPrivilegeToDelFlownFlts(request);
		setAvailabilityOfMultipleFltTypes(request);
		setMealTemplateCodes(request);
		setSeatChargeTemplateCodes(request);
		setMaxFlightNumberLength(request);
		setPastFlightActivationDuration(request);
		setCodeShareListHtml(request);
		isCodeShareEnable(request);
		setGdsCarrierCodeArray(request);
		setTimeChangeAndCancellationForPastFlights(request);

	}

	/**
	 * Sets the Online Active Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOnlineActiveAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOnlineActiveAirportListWOTag();
		request.setAttribute(WebConstants.SES_HTML_ONLINE_ACTIVE_AIRPORTCODE_LIST_DATA, strHtml);
	}

	/**
	 * Sets the Online Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOnlineAirportListWOTag();
		request.setAttribute(WebConstants.SES_HTML_ONLINE_AIRPORTCODE_LIST_DATA, strHtml);
	}

	/**
	 * Sets the OperationTypes of the Flights
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationType();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

	private static void setOperationTypeDetails(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationTypeArray();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_DETAILS, strHtml);
	}

	/**
	 * Sets the Status of the Flights
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightStatus();
		request.setAttribute(WebConstants.SES_HTML_STATUS_DATA, strHtml);
	}

	/**
	 * sets the GDS list added by Haider 31Aug08
	 * 
	 * @param request
	 */
	private static void setGDSListHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createGDSList();
		request.setAttribute(WebConstants.SES_HTML_GDS_LIST_DATA, strHtml);
	}

	private static void setFlightTypesHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightTypeList(true);
		request.setAttribute(WebConstants.REQ_HTML_FLIGHT_TYPES, strHtml);
	}

	/**
	 * sets the Build Status list
	 * 
	 * @param request
	 */

	/**
	 * Sets the minimum stop over times for airports
	 * 
	 * @param request
	 *            the request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStopOverTime(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createMinStopOverTime();
		request.setAttribute(WebConstants.SES_HTML_MINSTOPOVERTIME_DATA, strHtml);
	}

	/**
	 * Sets Model Data of the Created Flight
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setModelData(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createActiveAircraftModelList_SG();
		request.setAttribute(WebConstants.SES_HTML_AIRCRAFTMODEL_LIST_DATA, strHtml);
	}

	/**
	 * Sets the Flight Grid Data to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightRowHtml(HttpServletRequest request) throws ModuleException {
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strHtml = htmlGen.getFlightRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_FLIGHT_DATA, strHtml);

	}

	/**
	 * Sets the AircraftModels to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAircraftModelDetails(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createAircraftModelDtls();
		request.setAttribute(WebConstants.SES_HTML_AIRCRAFTMODEL_DETAILS, strHtml);
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = FlightHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Time Mode to the request LOCAL/ZULU
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setTimeInModeHtml(HttpServletRequest request) throws ModuleException {
		if (AppSysParamsUtil.isDefaultTimeZulu()) {
			request.setAttribute(WebConstants.REQ_HTML_TIMEIN, "var isTimeInZulu=true;");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_TIMEIN, "var isTimeInZulu=false;");
		}
	}

	/**
	 * Sets the Privilages required to use in javascript to the Request
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setPrivilagesHtml(HttpServletRequest request) throws ModuleException {

		HashMap mapPrivileges = (HashMap) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		boolean hasOperationsPrevi = (mapPrivileges.get(WebConstants.PLAN_FLIGHT_MANAGE_ADD_OPERATIONS_FLIGHT) != null);
		boolean hasAnyTypePrevi = (mapPrivileges.get(WebConstants.PLAN_FLIGHT_MANAGE_ADD_ANY_FLIGHT) != null);

		boolean hasOnlyOperations = hasOperationsPrevi && !hasAnyTypePrevi;
		request.setAttribute(WebConstants.REQ_HTML_ADD_OPERATIONS_ONLY, hasOnlyOperations);

		boolean allowActivatePastFlights = (mapPrivileges.get(WebConstants.PLAN_FLIGHT_ACTIVATE_PAST_FLIGHT) != null);
		request.setAttribute(WebConstants.REQ_HTML_ACTIVATE_PAST_FLIGHTS, allowActivatePastFlights);
	}

	private static void isFlightUserNoteVisible(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_USER_NOTE, AppSysParamsUtil.isFlightUserNoteEnabled());
	}

	private static void setPastFlightActivationDuration(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_PAST_FLIGHTS_ACTIVATION_DURATION_IN_DAYS,
				AppSysParamsUtil.getPastFlightActivationDurationInDays());
	}

	private static void setTimeChangeAndCancellationForPastFlights(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_TIME_CHANGE_CANCELLATION_AND_REPROTECTION_FOR_PAST_FLIGHTS,
				AppSysParamsUtil.isAllowedModificationForPastFlight());
	}


	/**
	 * Sets the Flights Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setModel(HttpServletRequest request, String mesage) throws ModuleException {
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strHtml = htmlGen.createModel(request, mesage);
		request.setAttribute(WebConstants.SES_HTML_FLTMODEL_DATA, strHtml);

	}

	private static void setHasPrivilegeToEditFlNumber(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_EDIT_FLIGHTNUMBER,
				hasPrivilege(request, WebConstants.PLAN_FLIGHT_EDIT_FLIGHT_NUMBER));
	}

	private static void setMaxFlightNumberLength(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_FLIGHTNUMBER_LENGTH, AppSysParamsUtil.getMaximumFlightNumberLength());
	}

	private static void setHasPrivilegeToDelFlownFlts(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_DEL_FLOWN_FLIGHTS,
				hasPrivilege(request, WebConstants.PLAN_FLIGHT_DELETE_FLOWN_FLIGHTS));
	}

	private static void setAvailabilityOfMultipleFltTypes(HttpServletRequest request) {
		String fltAvailablity = FlightUtils.checkForMultipleFltTypesAvailability();
		request.setAttribute(WebConstants.REQ_MULTIPLE_FLIGHT_TYPE_AVAILABILITY, fltAvailablity);
	}

	/**
	 * Method to set Carrier codes
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setUserCarriers(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		StringBuilder usb = new StringBuilder();

		String strDefaultCarrierCode = "";
		strDefaultCarrierCode = user.getDefaultCarrierCode();
		if (!(strDefaultCarrierCode != null && !strDefaultCarrierCode.trim().equals(""))) {
			strDefaultCarrierCode = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_CRRIERCODE, strDefaultCarrierCode);

		Set<String> stCarriers = user.getCarriers();
		if (stCarriers != null) {
			int i = 0;
			Iterator<String> stIter = stCarriers.iterator();
			while (stIter.hasNext()) {
				usb.append(" arrCarriers[" + i + "] = '" + stIter.next() + "';");
				i++;
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_USER_CRRIERCODE, usb.toString());
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	private static boolean getBooleanValue(String str) {
		return (str == null || !str.equals("true")) ? false : true;
	}

	private static void updateFlightStatus(HttpServletRequest request) throws ModuleException {

		String strFltManageDetails = request.getParameter("hdnManageFltIds");
		String strScheduleID = request.getParameter("hdnScheduleId");
		String strReinstate = request.getParameter("hdnReInstate");

		String strradActOpnSel = request.getParameter("hdnradActOpnSel");
		String hdnradActClsSel = request.getParameter("hdnradActClsSel");
		String hdnradActOpenAll = request.getParameter("hdnradActOpenAll");
		String hdnradActClsAll = request.getParameter("hdnradActClsAll");
		String hdnradActSysClsSel = request.getParameter("hdnradActSysClsSel");

		if (strFltManageDetails != null && !strFltManageDetails.equals("")) {
			processFlightData(strFltManageDetails, strScheduleID, strReinstate, isSelected(strradActOpnSel),
					isSelected(hdnradActClsSel), isSelected(hdnradActOpenAll), isSelected(hdnradActClsAll),
					isSelected(hdnradActSysClsSel));
		}

	}

	private static boolean isSelected(String value) {
		return (value != null && value.equalsIgnoreCase("true") ? true : false);
	}

	private static void processFlightData(String strFltManageDetails, String strScheduleID, String strReinstate, boolean actSel,
			boolean clsSel, boolean actAll, boolean clsAll, boolean actSysClsSel) {

		String[] arrFltInfo = strFltManageDetails.split("_");

		boolean isReistate = strReinstate.equalsIgnoreCase("true");
		String fltSchedID = "";
		// Collection<Integer> colFlightList = new ArrayList<Integer>();

		Collection<Integer> colmakeACTFlightList = new ArrayList<Integer>();
		Collection<Integer> colmakeCLSFlightList = new ArrayList<Integer>();
		Collection<Integer> colmakeCNXACTFlightList = new ArrayList<Integer>();

		Collection<Integer> colReinstateFlightList = new ArrayList<Integer>();
		for (int a = 0; a < arrFltInfo.length; a++) {
			String tmpSplit = arrFltInfo[a].trim();
			String[] arrIndFltInfo = tmpSplit.split("\\|");
			int fltId = Integer.parseInt(arrIndFltInfo[0]); // id
			String strStatus = arrIndFltInfo[1]; // status
			String selected = arrIndFltInfo[2]; // selected T F
			boolean isPassedDepTime = Boolean.valueOf(arrIndFltInfo[4]);
			boolean isSelected = selected != null && selected.equals("T") ? true : false;
			if (strStatus.equalsIgnoreCase("CLS") && actSel && isSelected && !isPassedDepTime) { // act selected
				colmakeACTFlightList.add(fltId);
			}
			if (strStatus.equals("ACT") && actSysClsSel && isSelected && isPassedDepTime) {
				colmakeACTFlightList.add(fltId);
			}
			if (strStatus.equalsIgnoreCase("CLS") && actAll) { // open all flights
				colmakeACTFlightList.add(fltId);
				fltSchedID = arrIndFltInfo[3];
			}
			if (strStatus.equalsIgnoreCase("ACT") && clsSel && isSelected && !isPassedDepTime) { // close selected
				colmakeCLSFlightList.add(fltId);
			}
			if (strStatus.equalsIgnoreCase("ACT") && clsAll) { // Close all flight
				colmakeCLSFlightList.add(fltId);
				fltSchedID = arrIndFltInfo[3];
			}
			if (strStatus.equalsIgnoreCase("CNX") && isReistate && isSelected) {
				colmakeCNXACTFlightList.add(fltId);
			}

		} // end for
			// act selected
			// Activate \ inactivate selected
			// INA All
			// Activate All\

		try {

			if (actSel && !colmakeACTFlightList.isEmpty()) { // Activate

				ModuleServiceLocator.getFlightServiceBD().manageFlightStatus(colmakeACTFlightList, FlightStatusEnum.ACTIVE);
			}
			if (actSysClsSel && !colmakeACTFlightList.isEmpty()) {
				ModuleServiceLocator.getFlightServiceBD().activatePastFlights(colmakeACTFlightList.iterator().next());
			}
			if (actAll) {
				// pass the schedule id and open all flights in schedule
				if (fltSchedID != null && fltSchedID != "") {
					Integer intSched = Integer.parseInt(fltSchedID);
					ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(intSched, FlightStatusEnum.ACTIVE);
				}
			}
			if (clsSel && !colmakeCLSFlightList.isEmpty()) { // Close

				ModuleServiceLocator.getFlightServiceBD().manageFlightStatus(colmakeCLSFlightList, FlightStatusEnum.CLOSED);
			}
			if (clsAll) {
				// pass scheudle id and close all flights in schedule
				if (fltSchedID != null && fltSchedID != "") {
					Integer intSched = Integer.parseInt(fltSchedID);
					ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(intSched, FlightStatusEnum.CLOSED);
				}

			}

			if (isReistate && !colmakeCNXACTFlightList.isEmpty()) {
				ModuleServiceLocator.getFlightServiceBD().manageFlightStatus(colmakeCNXACTFlightList, FlightStatusEnum.ACTIVE);
			}
		} catch (ModuleException e) {
			log.error(e);

		}

	}

	private static void setMealTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strTmplCodes = SelectListGenerator.createMealTemplateList();
		request.setAttribute(WebConstants.MEAL_TEMPL_CODE, strTmplCodes);
	}

	private static void setSeatChargeTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSeatMapChargeTemplateListStr();
		request.setAttribute(WebConstants.SEAT_TEMPL_CODE, strHtml);
	}

	private static String checkData(HttpServletRequest request) throws ModuleException, ParseException {
		if (request.getParameter("hdnFlightId") != null && !request.getParameter("hdnFlightId").equals("")) {
			String strAircraftModelNumber = request.getParameter("selModelNo") == null
					? request.getParameter("hdnModelNo")
					: request.getParameter("selModelNo");
			Integer flightID = Integer.parseInt(request.getParameter("hdnFlightId"));
			AircraftModel model = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(strAircraftModelNumber);
			String message = ModuleServiceLocator.getAircraftServiceBD().getAffectedFlights(model, flightID);
			if (message.equals("")) {
				saveData(request);
			} else {
				setModel(request, message);
			}
			return message;
		} else {
			return "";
		}
	}

	private static void cancelBookedAirportTransfers(Flight updated) throws ModuleException {
		List<Integer> fltSegIDs = new ArrayList<Integer>();
		if (updated != null) {
			for (FlightSegement updatedSeg : updated.getFlightSegements()) {
				if (!fltSegIDs.contains(updatedSeg.getFltSegId())
						&& ReservationInternalConstants.ReservationExternalFlightStatus.OPEN.equals(updatedSeg.getStatus())) {
					fltSegIDs.add(updatedSeg.getFltSegId());
				}
			}
		}
		if (fltSegIDs != null && fltSegIDs.size() > 0) {
			Collection<ReservationSegment> reservationSegments = ModuleServiceLocator.getSegmentBD().getPnrSegments(fltSegIDs,
					true, true);
			Collection<Integer> pnrSegIds = new ArrayList<Integer>();
			Collection<Reservation> reservationList = new ArrayList<Reservation>();
			for (ReservationSegment seg : reservationSegments) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(seg.getStatus())) {
					pnrSegIds.add(seg.getPnrSegId());
					Reservation reservation = seg.getReservation();
					reservationList.add(reservation);
				}
			}

			for (Reservation reservation : reservationList) {
				Collection<Integer> pnrSegmentIds = new ArrayList<Integer>();
				loadAirportTransferInfo(reservation);
				Map<Integer, Collection<PaxAirportTransferTO>> cancelledAptMap = new HashMap<Integer, Collection<PaxAirportTransferTO>>();
				if (reservation.getAirportTransfers() != null) {
					for (PaxAirportTransferTO paxAirportTransferTO : reservation.getAirportTransfers()) {
						if (pnrSegIds.contains(paxAirportTransferTO.getPnrSegId())) {
							if (cancelledAptMap.containsKey(paxAirportTransferTO.getPnrPaxId())) {
								cancelledAptMap.get(paxAirportTransferTO.getPnrPaxId()).add(paxAirportTransferTO);
							} else {
								Collection<PaxAirportTransferTO> cancelledApts = new ArrayList<PaxAirportTransferTO>();
								cancelledApts.add(paxAirportTransferTO);
								cancelledAptMap.put(paxAirportTransferTO.getPnrPaxId(), cancelledApts);
								pnrSegmentIds.add(paxAirportTransferTO.getPnrSegId());
							}
						}
					}
					if (!cancelledAptMap.isEmpty()) {
						ModuleServiceLocator.getSegmentBD().cancelAirportTransferRequests(reservation, pnrSegmentIds,
								cancelledAptMap, null);
					}
				}
			}
		}
	}

	private static void loadAirportTransferInfo(Reservation reservation) throws ModuleException {
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();
		if (AppSysParamsUtil.isAirportTransferEnabled()) {
			if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
				Collection<PaxAirportTransferTO> paxAirportTransferDTOs = null;
				paxAirportTransferDTOs = ModuleServiceLocator.getReservationBD().getReservationAirportTransfersByPaxId(pnrPaxIds);
				reservation.setAirportTransfers(paxAirportTransferDTOs);
			} else {
				throw new ModuleException("airreservations.load.reservation");
			}
		}
	}
	/**
	 * sets the CodeShare list
	 * 
	 * @param request
	 */
	private static void setCodeShareListHtml(HttpServletRequest request) throws ModuleException {
		
		StringBuffer sb = new StringBuffer();
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.isCodeShareCarrier()) {
				sb.append("<option value='" + gdsStatusTO.getCarrierCode() + "'>" + gdsStatusTO.getCarrierCode() + " </option>");
			}
		}
		
		String strHtml = sb.toString();
		request.setAttribute(WebConstants.SES_HTML_CodeShare_LIST_DATA , strHtml);
	}

	private static void isCodeShareEnable(HttpServletRequest request)
			throws ModuleException {
		request.setAttribute(WebConstants.REQ_CS_ENABLE, AppSysParamsUtil.isCodeShareEnable());
	}
	
	private static void setGdsCarrierCodeArray(HttpServletRequest request) throws ModuleException {
		String gdsCarrierCodeArr = "var gdsCarrierCodeArray = new Array();";
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
			int i = 0;
			for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
				if (gdsStatusTO != null) {
					gdsCarrierCodeArr += "gdsCarrierCodeArray[" + i + "] = '" + gdsStatusTO.getGdsId() + "|"
							+ gdsStatusTO.getCarrierCode() + "';";
				}
				i++;
			}

		}
		request.setAttribute(WebConstants.REQ_GDS_CARRIER_CODE_ARRAY, gdsCarrierCodeArr);
	}
	
	private static void viewFlightMsgReport(HttpServletRequest request, HttpServletResponse response) {
		try {
			Properties properties = FlightUtils.getProperties(request);
			String flightId = properties.getProperty(PARAM_FLIGHTID);
			String flightNo = properties.getProperty(PARAM_FLIGHT_NO);
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(flightId));

			Date fromDate = CalendarUtil.getStartTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("fromDate"),
					"dd/MM/yyyy"));
			Date toDate = CalendarUtil.getEndTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("toDate"), "dd/MM/yyyy"));
			String strFromDate = CalendarUtil.getFormattedDate(fromDate, "dd/MM/yyyy HH:mm:ss");
			String strToDate = CalendarUtil.getFormattedDate(toDate, "dd/MM/yyyy HH:mm:ss");

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getFlightMessageDetailsReport(flightId,
					flight.getScheduleId(), strFromDate, strToDate);

			String reportTemplate = "AdHocMsgHistoryReport.jasper";
			String strLogo = AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			String reportsRootDir = "../../images/" + strLogo;

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORTTYPE", WebConstants.REPORT_HTML);
			parameters.put("IMG", reportsRootDir);
			parameters.put("FLIGHT_NO", flightNo);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} catch (Exception e) {
			log.error("viewFlightMsgReport :: ", e);
		}
	}

	private static void viewFlightPublishedMessagesAudit(HttpServletRequest request, HttpServletResponse response) {
		try {
			Properties properties = FlightUtils.getProperties(request);
			String flightId = properties.getProperty(PARAM_FLIGHTID);
			String flightNo = properties.getProperty(PARAM_FLIGHT_NO);

			Date fromDate = CalendarUtil.getStartTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("fromDate"),
					"dd/MM/yyyy"));
			Date toDate = CalendarUtil.getEndTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("toDate"), "dd/MM/yyyy"));
			String strFromDate = CalendarUtil.getFormattedDate(fromDate, "dd/MM/yyyy HH:mm:ss");
			String strToDate = CalendarUtil.getFormattedDate(toDate, "dd/MM/yyyy HH:mm:ss");

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getPublishedMessageDetailsReport(flightId,
					strFromDate, strToDate);

			String reportTemplate = "PublishedMsgHistoryReport.jasper";
			String strLogo = AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			String reportsRootDir = "../../images/" + strLogo;

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORTTYPE", WebConstants.REPORT_HTML);
			parameters.put("IMG", reportsRootDir);
			parameters.put("FLIGHT_NO", flightNo);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} catch (Exception e) {
			log.error("viewFlightPublishedMessagesAudit :: ", e);
		}
	}
}
