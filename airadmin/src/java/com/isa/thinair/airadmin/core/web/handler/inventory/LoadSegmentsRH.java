/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.LoadSegmentsHg;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airpricing.api.model.SeatCharge.SeatChargeStatus;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thush
 */
public class LoadSegmentsRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(LoadSegmentsRH.class);

	private static String clientErrors;

	private static Object lock = new Object();

	private static final String PARAM_MODE = "hdnMode";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Execute Method for template Charges
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the forward action
	 */
	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled()) {
			log.debug("Begin LoadSegmentsRH.execute(HttpServletRequest request)::" + System.currentTimeMillis());
		}

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);

			} catch (ModuleException mex) {
				log.error("Exception in  Seat charge Roll forward:", mex);
				saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

			} catch (Exception exception) {
				log.error("Exception in  LoadSegmentsRH.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else {
					try {
						setFormData(request);
					} catch (Exception e) {
						log.error(e);
					}
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
			try {
				setEditData(request);
				setLogicalCabinClassList(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);

				forward = AdminStrutsConstants.AdminAction.EDIT_TEMPLATE;

			} catch (ModuleException mex) {
				log.error("Exception in  Seat charge edit:", mex);
				saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

			} catch (Exception exception) {
				log.error("Exception in  LoadSegmentsRH.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else {
					try {
						setFormData(request);
					} catch (Exception e) {
						log.error(e);
					}
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
			return forward;
		}

		if (strHdnMode != null && strHdnMode.equals("EDITSAVE")) {
			try {
				saveEditData(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);
			} catch (ModuleException mex) {
				log.error("Exception in  Seat charge edit:", mex);
				saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

			} catch (IllegalStateException exception) {
				log.error("Exception in  LoadSegmentsRH.execute()", exception);
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, "Time Out. Could Not Complete Transaction", "", "");
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);

			} catch (Exception exception) {
				log.error("Exception in  LoadSegmentsRH.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else {
					try {
						setFormData(request);
					} catch (Exception e) {
						log.error(e);
					}
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}

		try {
			setDisplayData(request);
		} catch (ModuleException mex) {
			log.error("Exception in  Seat charge Roll forward:", mex);
			saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			log.error("Exception in  LoadSegmentsRH.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Saves the Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {
		String strData = request.getParameter("hdnData").replaceAll("\\<a.*?a>","");// for remove unwanted html part in data string 
		String modelNo = request.getParameter("modelNo");
		String[] arrData = strData.split(",");
		Collection<FlightSeatsDTO> colSegs = new ArrayList<FlightSeatsDTO>();
		int noOfSegs = arrData.length / 6;
		for (int i = 0; i < noOfSegs; i++) {
			int dataIndex = 6 * i;
			FlightSeatsDTO fltSetDto = new FlightSeatsDTO();
			fltSetDto.setSegmentCode(arrData[dataIndex + 1]);
			fltSetDto.setFlightSegmentID(new Integer(arrData[dataIndex + 2]));
			if (arrData[dataIndex + 3] != null && !arrData[dataIndex + 3].trim().equals("")) {
				fltSetDto.setTemplateId(new Integer(arrData[dataIndex + 3]));
			}
			colSegs.add(fltSetDto);
		}
		ServiceResponce resp = ModuleServiceLocator.getSeatMapBD().assignFlightSeatCharges(colSegs, modelNo);
		setServiceResponse(resp, request);
	}

	/**
	 * Saves the Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveEditData(HttpServletRequest request) throws Exception {
		String strSeatData = request.getParameter("hdnData");
		String modelNo = request.getParameter("modelNo");
		String strFlightSegId = request.getParameter("fltSegId");
		ServiceResponce resp = null;
		int datalen = 6;
		int noofseats;
		int noofleft;
		SeatCharge seatChg = null;
		Set<SeatCharge> colSeats = new HashSet<SeatCharge>();

		if (strSeatData != null && !strSeatData.equals("")) {
			String seatArr[] = strSeatData.split(",");
			if (strSeatData.endsWith(",")) {
				noofseats = (seatArr.length + 1) / datalen;
				noofleft = (seatArr.length + 1) % datalen;
			} else {
				noofseats = seatArr.length / datalen;
				noofleft = seatArr.length % datalen;
			}
			if (noofleft > 0) {
				noofseats = noofseats + 1;
			}
			int arrItem = 0;
			for (int x = 0; x < noofseats; x++) {
				seatChg = new SeatCharge();
				arrItem = datalen * x;

				if (seatArr[arrItem + 0] != null && !seatArr[arrItem + 0].equals("")) {
					seatChg.setChargeId(new Integer(seatArr[arrItem + 0]));
				}
				if (seatArr[arrItem + 1] != null) {
					seatChg.setSeatId(new Integer(seatArr[arrItem + 1]));
				}
				if (seatArr[arrItem + 3] != null && !seatArr[arrItem + 3].equals("")) {
					seatChg.setChargeAmount(new BigDecimal(seatArr[arrItem + 3]));
				}
				if (seatArr[arrItem + 4] != null && (seatArr[arrItem + 4].equals("INA") || seatArr[arrItem + 4].equals("I"))) {
					seatChg.setStatus(SeatChargeStatus.INACTIVE.getStatusCode());
				} else {
					seatChg.setStatus(SeatChargeStatus.ACTIVE.getStatusCode());
				}
				if (noofleft == 0) {
					if (seatArr.length > arrItem + 5) {
						if (seatArr[arrItem + 5] != null && !seatArr[arrItem + 5].equals("")) {
							seatChg.setVersion(new Long(seatArr[arrItem + 5]));
						}
					}
				} else {
					if (x != noofseats - 1) {
						if (seatArr.length > arrItem + 5) {
							if (seatArr[arrItem + 5] != null && !seatArr[arrItem + 5].equals("")) {
								seatChg.setVersion(new Long(seatArr[arrItem + 5]));
							}
						}
					}
				}
				colSeats.add(seatChg);
			}
		}
		SeatMapBD seatMapBd = ModuleServiceLocator.getSeatMapBD();
		resp = seatMapBd.updateTemplateFromFlight(new Integer(strFlightSegId), colSeats, modelNo);
		setServiceResponse(resp, request);
	}

	/**
	 * sets the service responce data to the request
	 * 
	 * @param resp
	 *            the ServiceResponce
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setServiceResponse(ServiceResponce resp, HttpServletRequest request) {

		if (resp.isSuccess()) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
		}

	}

	/**
	 * Sets the display data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setTemplateCodes(request);
		setClientErrors(request);
		setFlightData(request);
		setFlitStatus(request);
	}

	public static void setLogicalCabinClassList(HttpServletRequest request) throws ModuleException {
		String strHtmlLCC = SelectListGenerator.createAllLogicalCabinClass();
		request.setAttribute(WebConstants.REQ_HTML_LOGICALCABINCLASS_LIST, strHtmlLCC);
	}

	private static void setFlitStatus(HttpServletRequest request) throws ModuleException {
		String strFlightId = request.getParameter("fltId");
		if (strFlightId != null && !"".equals(strFlightId)) {
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(new Integer(strFlightId));
			if (flight.getScheduleId() != null) {
				request.setAttribute(WebConstants.SEAT_SCED_STATS, "blnIsSced = true;");
			}
		}

	}

	/**
	 * Sets the template data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strModel = request.getParameter("modelNo");
		String s[] = null;
		int i = 0;
		StringBuilder sb = new StringBuilder();
		StringBuilder ssb = new StringBuilder();
		Collection<String[]> colTemplates = SelectListGenerator.createChargesTemplateIDs(strModel);
		if (colTemplates != null) {
			Iterator<String[]> iter = colTemplates.iterator();
			while (iter.hasNext()) {
				s = iter.next();
				ssb.append("arrTemp[" + i + "] = new Array('" + s[0] + "','" + s[1] + "','" + s[2] + "');");
				sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
				i++;
			}
		} else {
			sb.append("<option value=''></option>");
		}
		request.setAttribute("reqSelModel", strModel);
		request.setAttribute(WebConstants.SEAT_TEMPL_CODE, sb.toString());
		request.setAttribute("reqTemplArray", ssb.toString());
	}

	/**
	 * Sets the Client validations
	 * 
	 * @param request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void setFormData(HttpServletRequest request) throws ModuleException {
		String strData = request.getParameter("hdnData");
		request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, strData);
	}

	/**
	 * Gets the client validations
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		synchronized (lock) {
			if (clientErrors == null) {
				Properties moduleErrs = new Properties();

				clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
			}
			return clientErrors;
		}
	}

	/**
	 * Sets the Flight Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightData(HttpServletRequest request) throws ModuleException {
		String strHtml = LoadSegmentsHg.getFlightData(request);
		request.setAttribute(WebConstants.SEAT_SEGMENT_DATA, strHtml);
	}

	/**
	 * Sets the editData for Template
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setEditData(HttpServletRequest request) throws ModuleException {
		String strHtml = LoadSegmentsHg.editData(request);
		request.setAttribute(WebConstants.SEAT_EDIT_DATA, strHtml);
	}

}
