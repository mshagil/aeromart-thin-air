package com.isa.thinair.airadmin.core.web.action.inventory;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.INVENTORY_TEMPLATES_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.INVTEMPLATE_BC, value = AdminStrutsConstants.AdminJSP.TEMPLATES_BC_ALLOC_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowInventoryTemplatesAction extends BaseRequestAwareAction {
	private String showRW;
	private String airCraftModel;
	private String segment;
	private String status;
	private int templateID;
	public String execute() {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		return forward;
	}

	public String templateBCAlloc() {
		return AdminStrutsConstants.AdminAction.INVTEMPLATE_BC;
	}

	public String showInventoryTemplatePage() {
		return AdminStrutsConstants.AdminJSP.INVENTORY_TEMPLATES_JSP;
	}
	public String getShowRW() {
		return showRW;
	}

	public void setShowRW(String showRW) {
		this.showRW = showRW;
	}

	public String getAirCraftModel() {
		return airCraftModel;
	}

	public void setAirCraftModel(String airCraftModel) {
		this.airCraftModel = airCraftModel;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getTemplateID() {
		return templateID;
	}

	public void setTemplateID(int templateID) {
		this.templateID = templateID;
	}
}
