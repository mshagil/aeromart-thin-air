package com.isa.thinair.airadmin.core.web.action.pricing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.FareUtil;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.commons.api.exception.ModuleException;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FareOverrideOHDAction {

	private static final Log log = LogFactory.getLog(FareOverrideOHDAction.class);

	private boolean success;

	private String messageTxt = "";

	private String msgType;

	private Integer fareRuleId;

	private Collection rows;

	private static final String MOD_IBE = "IBE";

	private static final String MOD_XBE = "XBE";

	private static final String ANY = "ANY";

	private static final String OPT_DEP_DATE = "DEP_DATE";

	private static final String OPT_BKG_DATE = "BKG_DATE";

	private String selModuleCode;

	private String releaseTime;

	private int relTimeId;

	private String startCutoverTime;

	private String endCutoverTime;

	private String selRelTimeRespectTo;

	private Long version;

	private AiradminConfig airadminConfig = new AiradminConfig();

	public String execute() {
		try {

			if (fareRuleId != null || !("").equals(fareRuleId)) {
				Collection<OnHoldReleaseTimeDTO> ohdRelDTOs = ModuleServiceLocator.getRuleBD().getOHDRelTimeDTOForFareRuleId(
						fareRuleId);

				if (ohdRelDTOs != null && ohdRelDTOs.size() > 0) {
					rows = createFareRuleOHDTimeGrid(ohdRelDTOs);
				}
				success = true;
			}

		} catch (ModuleException e) {
			log.error("FareOverwriteAction error " + e.getMessage());
			success = false;
			messageTxt = "Searching overwrite fees failed. Please try again later.";
		} catch (Exception ex) {
			log.error("FareOverwriteAction error " + ex.toString());
			success = false;
			messageTxt = "Searching overwrite fees failed. Please try again later.";
		}

		return S2Constants.Result.SUCCESS;
	}

	public String saveOHDTime() {
		String forward = S2Constants.Result.SUCCESS;

		try {
			OnholdReleaseTime onholdRelTime = new OnholdReleaseTime();
			onholdRelTime.setRelTimeId(getRelTimeId());
			onholdRelTime.setAgentCode(ANY);
			onholdRelTime.setBookingCode(ANY);
			onholdRelTime.setCabinClassCode(ANY);
			onholdRelTime.setIsDomestic('N');
			onholdRelTime.setOndCode(ANY);
			onholdRelTime.setRanking(0);

			if ("XBE".equals(getSelModuleCode())) {
				onholdRelTime.setModuleCode(MOD_XBE);
			} else if ("IBE".equals(getSelModuleCode())) {
				onholdRelTime.setModuleCode(MOD_IBE);
			} else if (ANY.equals(getSelModuleCode())) {
				onholdRelTime.setModuleCode(ANY);
			}

			if (OPT_DEP_DATE.equals(selRelTimeRespectTo)) {
				onholdRelTime.setRelTimeWrt(OPT_DEP_DATE);
			} else if (OPT_BKG_DATE.equals(selRelTimeRespectTo)) {
				onholdRelTime.setRelTimeWrt(OPT_BKG_DATE);
			}

			onholdRelTime.setStartCutover(FareUtil.parseStringToMins(getStartCutoverTime()));
			onholdRelTime.setEndCutover(FareUtil.parseStringToMins(getEndCutoverTime()));
			onholdRelTime.setReleaseTime(FareUtil.parseStringToMins(getReleaseTime()));

			onholdRelTime.setFareRuleId(getFareRuleId());

			Map<String, String[]> contentMap = new HashMap<String, String[]>();

			if (getVersion() != null && getVersion() >= 0) {
				onholdRelTime.setVersion(getVersion());
				OnholdReleaseTime originalConfig = ModuleServiceLocator.getReservationBD().getOnholdReleaseTime(
						onholdRelTime.getRelTimeId());
				createUpdatedDataMapForAudit(contentMap, originalConfig, onholdRelTime);
			} else {
				onholdRelTime.setVersion(-1);
			}

			if (isOhdRelCfgLegal(onholdRelTime)) {
				ModuleServiceLocator.getReservationBD().saveOrUpdateOnholdReleaseTime(onholdRelTime, contentMap);
				success = true;
				this.messageTxt = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;
			} else {
				msgType = WebConstants.MSG_ERROR;
				success = false;
				messageTxt = "Start cutover time or End cutover time is conflicting with existing configuration.";
			}

		} catch (ModuleException ex) {
			log.error(ex);
			msgType = WebConstants.MSG_ERROR;
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			msgType = WebConstants.MSG_ERROR;
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	@SuppressWarnings("unchecked")
	private Collection createFareRuleOHDTimeGrid(Collection<OnHoldReleaseTimeDTO> ohdRelDTOs) throws ModuleException {
		List ohdTimes = new ArrayList();
		Map dtoMap = null;
		if (ohdRelDTOs != null) {
			for (OnHoldReleaseTimeDTO ohdTimeDTO : ohdRelDTOs) {
				dtoMap = new HashMap();
				FareUtil.getFareRuleOHDTimeDate(ohdTimeDTO, dtoMap);
				ohdTimes.add(dtoMap);
			}
		}
		return ohdTimes;
	}

	public String deleteOnholdTimeConfig() {
		try {
			ModuleServiceLocator.getReservationBD().deleteOnholdReleaseTimeConfig(getRelTimeId());
			success = true;
			this.messageTxt = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			success = false;
			this.messageTxt = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
		}
		return S2Constants.Result.SUCCESS;
	}

	private boolean isOhdRelCfgLegal(OnholdReleaseTime onholdRelTime) {
		try {
			return ModuleServiceLocator.getReservationBD().isOhdRelCfgLegal(onholdRelTime);
		} catch (ModuleException ex) {
			log.error(ex);
		}

		return false;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Integer getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public Collection getRows() {
		return rows;
	}

	public void setRows(Collection rows) {
		this.rows = rows;
	}

	public String getSelModuleCode() {
		return selModuleCode;
	}

	public void setSelModuleCode(String selModuleCode) {
		this.selModuleCode = selModuleCode;
	}

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}

	public String getStartCutoverTime() {
		return startCutoverTime;
	}

	public void setStartCutoverTime(String startCutoverTime) {
		this.startCutoverTime = startCutoverTime;
	}

	public String getEndCutoverTime() {
		return endCutoverTime;
	}

	public void setEndCutoverTime(String endCutoverTime) {
		this.endCutoverTime = endCutoverTime;
	}

	public String getSelRelTimeRespectTo() {
		return selRelTimeRespectTo;
	}

	public void setSelRelTimeRespectTo(String selRelTimeRespectTo) {
		this.selRelTimeRespectTo = selRelTimeRespectTo;
	}

	public int getRelTimeId() {
		return relTimeId;
	}

	public void setRelTimeId(int relTimeId) {
		this.relTimeId = relTimeId;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	private void createUpdatedDataMapForAudit(Map<String, String[]> contentMap, OnholdReleaseTime originalConfig,
			OnholdReleaseTime onholdRelTime) {
		List<String> changedValList = new ArrayList<String>();
		if (!(onholdRelTime.getAgentCode().equals(originalConfig.getAgentCode()))) {
			changedValList.add("AgentCode " + originalConfig.getAgentCode() + " modified to " + onholdRelTime.getAgentCode());
		}
		if (!(onholdRelTime.getBookingCode().equals(originalConfig.getBookingCode()))) {
			changedValList.add("BookingCode " + originalConfig.getBookingCode() + " modified to "
					+ onholdRelTime.getBookingCode());
		}
		if (!(onholdRelTime.getCabinClassCode().equals(originalConfig.getCabinClassCode()))) {
			changedValList.add("CabinClassCode " + originalConfig.getCabinClassCode() + " modified to "
					+ onholdRelTime.getCabinClassCode());
		}
		if (onholdRelTime.getIsDomestic() != originalConfig.getIsDomestic()) {
			changedValList.add((originalConfig.getIsDomestic() == 'Y' ? "Domestic" : "International") + " modified to "
					+ (onholdRelTime.getIsDomestic() == 'Y' ? "Domestic" : "International"));
		}
		if (!(onholdRelTime.getModuleCode().equals(originalConfig.getModuleCode()))) {
			changedValList.add("ModuleCode " + originalConfig.getModuleCode() + " modified to " + onholdRelTime.getModuleCode());
		}
		if (!(onholdRelTime.getOndCode().equals(originalConfig.getOndCode()))) {
			changedValList.add("OndCode " + originalConfig.getOndCode() + " modified to " + onholdRelTime.getOndCode());
		}
		if (!(onholdRelTime.getRanking().equals(originalConfig.getRanking()))) {
			changedValList.add("Rank " + Integer.toString(originalConfig.getRanking()) + " modified to "
					+ Integer.toString(onholdRelTime.getRanking()));
		}
		if (!(onholdRelTime.getReleaseTime().equals(originalConfig.getReleaseTime()))) {
			changedValList.add("ReleaseTime " + Integer.toString(originalConfig.getReleaseTime()) + " modified to "
					+ Integer.toString(onholdRelTime.getReleaseTime()));
		}
		if (!(onholdRelTime.getRelTimeWrt().equals(originalConfig.getRelTimeWrt()))) {
			changedValList.add("ReleaseTimeWRT "
					+ (originalConfig.getRelTimeWrt().equals("DEP_DATE") ? "Departure Date" : "Booking Date") + " modified to "
					+ (onholdRelTime.getRelTimeWrt().equals("DEP_DATE") ? "Departure Date" : "Booking Date"));
		}
		if (!(onholdRelTime.getStartCutover().equals(originalConfig.getStartCutover()))) {
			changedValList.add("StartCutover " + Integer.toString(originalConfig.getStartCutover()) + " modified to "
					+ Integer.toString(onholdRelTime.getStartCutover()));
		}
		if (!(onholdRelTime.getEndCutover().equals(originalConfig.getEndCutover()))) {
			changedValList.add("EndCutover " + Integer.toString(originalConfig.getEndCutover()) + " modified to "
					+ Integer.toString(onholdRelTime.getEndCutover()));
		}

		contentMap.put("updated_config", changedValList.toArray(new String[changedValList.size()]));
	}

}
