package com.isa.thinair.airadmin.core.web.action.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.StateDTO;
import com.isa.thinair.airadmin.core.web.v2.handler.master.StateRH;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowStateAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowStateAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();
	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;

	private long version;
	private String createdBy;
	private Date createdDate;

	private String hdnMode;
	private String mode;

	private String selCountry;
	private StateDTO stateDTO;
	private Integer stateId;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getSelCountry() {
		return selCountry;
	}

	public void setSelCountry(String selCountry) {
		this.selCountry = selCountry;
	}

	public StateDTO getStateDTO() {
		return stateDTO;
	}

	public void setStateDTO(StateDTO stateDTO) {
		this.stateDTO = stateDTO;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String execute() throws Exception {
		try {

			State state = new State();
			State existState = null;

			if (stateDTO != null) {
				if (stateDTO.getStateId() != null) {
					state.setStateId(stateDTO.getStateId());
				}
				state.setStateCode(stateDTO.getStateCode());
				state.setStateName(stateDTO.getStateName());
				state.setCountryCode(stateDTO.getCountryCode());
				state.setAirlineOfficeSTAddress1(stateDTO.getAirlineOfficeSTAddress1());
				state.setAirlineOfficeSTAddress2(stateDTO.getAirlineOfficeSTAddress2());
				state.setAirlineOfficeCity(stateDTO.getAirlineOfficeCity());
				state.setAirlineOfficeTaxRegNo(stateDTO.getAirlineOfficeTaxRegNo());
				state.setDigitalSignature(stateDTO.getStateDigitalSignature());
				if (stateDTO.getStatus() != null) {
					state.setStatus(stateDTO.getStatus());
				} else {
					state.setStatus(STATUS_INACTIVE);
				}

				if (this.hdnMode != null
						&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
					if (hdnMode.equals(WebConstants.ACTION_EDIT)) {
						if (stateDTO.getStateId() != null) {
							existState = StateRH.getState(stateDTO.getStateId());
							existState.setStateName(stateDTO.getStateName());
							existState.setCountryCode(stateDTO.getCountryCode());
							existState.setAirlineOfficeSTAddress1(stateDTO.getAirlineOfficeSTAddress1());
							existState.setAirlineOfficeSTAddress2(stateDTO.getAirlineOfficeSTAddress2());
							existState.setAirlineOfficeCity(stateDTO.getAirlineOfficeCity());
							existState.setAirlineOfficeTaxRegNo(stateDTO.getAirlineOfficeTaxRegNo());
							existState.setDigitalSignature(stateDTO.getStateDigitalSignature());
							if (stateDTO.getStatus() != null) {
								existState.setStatus(stateDTO.getStatus());
							} else {
								existState.setStatus(STATUS_INACTIVE);
							}
						}
						state = existState;
					}
					try {
						StateRH.saveState(state);
						this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
						this.msgType = WebConstants.MSG_SUCCESS;

					} catch (ModuleException e) {
						this.succesMsg = e.getMessageString();
						if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
							this.succesMsg = airadminConfig.getMessage("um.State.form.id.already.exist");
						}
						this.msgType = WebConstants.MSG_ERROR;
					} catch (Exception ex) {
						this.succesMsg = airadminConfig.getMessage("um.State.system.error");
						this.msgType = WebConstants.MSG_ERROR;
						if (log.isErrorEnabled()) {
							log.error(ex);
						}
					}
				}
			} else {
				this.succesMsg = airadminConfig.getMessage("um.State.form.fields.required");
				this.msgType = WebConstants.MSG_ERROR;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.State.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in saving State:", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteState() throws Exception {
		try {
			if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_DELETE)) && stateId != null)) {
				try {
					StateRH.deleteState(stateId);
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;

				} catch (ModuleException e) {
					this.succesMsg = e.getMessageString();
					if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
						this.succesMsg = airadminConfig.getMessage("um.State.form.id.already.exist");
					}
					this.msgType = WebConstants.MSG_ERROR;
				} catch (Exception ex) {
					this.succesMsg = airadminConfig.getMessage("um.State.system.error");
					this.msgType = WebConstants.MSG_ERROR;
					if (log.isErrorEnabled()) {
						log.error(ex);
					}
				}

			} else {
				this.succesMsg = airadminConfig.getMessage("um.State.system.delete.required");
				this.msgType = WebConstants.MSG_ERROR;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.State.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in saving State:", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchState() throws Exception {
		try {

			List<ModuleCriterion> critrian = null;

			if (selCountry != null && !selCountry.equals("-1")) {
				critrian = new ArrayList<ModuleCriterion>();
				ModuleCriteria criteriaStateCountry = new ModuleCriteria();
				List<String> lstCountryCodes = new ArrayList<String>();
				criteriaStateCountry.setFieldName("countryCode");
				criteriaStateCountry.setCondition(ModuleCriteria.CONDITION_LIKE);
				lstCountryCodes.add("%" + selCountry + "%");
				criteriaStateCountry.setValue(lstCountryCodes);
				critrian.add(criteriaStateCountry);
			}

			Page<State> pgState = StateRH.searchState(this.page, critrian);
			this.records = pgState.getTotalNoOfRecords();
			this.total = pgState.getTotalNoOfRecords() / 20;
			int mod = pgState.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page <= 0) {
				this.page = 1;
			}

			Collection<State> colState = pgState.getPageData();

			if (!StringUtil.isNullOrEmpty(colState.toString())) {
				Object[] dataRow = new Object[colState.size()];
				int index = 1;
				Iterator<State> iter = colState.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					State state = iter.next();
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("state", state);

					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.State.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in search State", e);
		}
		return S2Constants.Result.SUCCESS;
	}

}
