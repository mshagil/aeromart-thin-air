package com.isa.thinair.airadmin.core.web.handler.flightsched;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.flightsched.FlightHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Haider
 * 
 * 
 */
public final class FltGDSPublishingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(FltGDSPublishingRequestHandler.class);

	private static final String PARAM_VERSION = "hdnVersion";

	private static final String PARAM_FLIGHT_ID = "hdnFlightId";
	private static final String PARAM_VIEW_MODE_ONLY = "hdnViewModeOnly";
	private static final String PARAM_GDS_Publishing = "hdnGDSPublishing";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_HAVE_MC_FLIGHT = "hdnHaveMCFlight";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Main execute Method for GDSPublishing Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strErrorMEssage = "";
		boolean isSaveError = false;
		try {
			forward = saveData(request);
		} catch (ModuleException e) {
			isSaveError = true;
			log.error("Exception in GDSPublishingeRequestHandler.execute " + "[origin module=" + e.getModuleDesc() + "]", e);
			// /for any error return the error page -- /public/showError

			strErrorMEssage = e.getMessageString();
			request.setAttribute(WebConstants.REQ_MESSAGE, e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			isSaveError = true;
			log.error("Exception in GDSPublishingeRequestHandler.save", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				strErrorMEssage = e.getMessage();
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}

		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			log.error("Exception in GDSPublishingeRequestHandler.setDisplay " + "- setDisplayData(request)" + " [origin module="
					+ e.getModuleDesc() + "]", e);
			// /for any error return the error page -- /public/showError
			if (!isSaveError) {
				strErrorMEssage = e.getMessageString();
			}

			request.setAttribute(WebConstants.REQ_MESSAGE, e);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error("Exception in GDSPublishingeRequestHandler.execute " + "- setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				if (!isSaveError) {
					strErrorMEssage += " : " + e.getMessage();
				}
				saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			}

		}
		return forward;
	}

	/**
	 * Saves Flight Schedule Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws ParseException
	 *             the ParseException
	 */
	private static String saveData(HttpServletRequest request) throws ModuleException, ParseException {

		String strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String version = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
		// declaring the Service response object
		String strResponseMessage = "success";
		ServiceResponce serviceResponce = null;
		String flightId = "";
		String gdsIds = "";
		Long temp = null;
		// if action is adding data
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			flightId = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_ID));
			gdsIds = AiradminUtils.getNotNullString(request.getParameter(PARAM_GDS_Publishing));
			String[] strArrGDSIds = StringUtils.split(gdsIds, ',');
			List<Integer> list = new ArrayList<Integer>();
			for (int i = 0; i < strArrGDSIds.length; i++)
				list.add(Integer.valueOf(strArrGDSIds[i]));
			serviceResponce = ModuleServiceLocator.getFlightServiceBD().publishFlightToGDS(Integer.valueOf(flightId), list,
					Integer.valueOf(version));
			String responceCode = AiradminUtils.getNotNullString(serviceResponce.getResponseCode());
			if (responceCode.equals(ResponceCodes.ADD_REMOVE_GDS_SUCCESS)) {
				temp = (Long) serviceResponce.getResponseParam(ResponceCodes.ADD_REMOVE_GDS_SUCCESS);
				version = String.valueOf(temp);
				if (gdsIds.isEmpty() || gdsIds.equals("")) {
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_GDS_UNPUBLISH_SUCCESS));
				} else {
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_GDS_PUBLISH_SUCCESS));
				}
			}

		}
		request.setAttribute("version", version);

		return strResponseMessage;

	}

	/**
	 * Sets the Display Data to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setGDSListHtml(request);

		request.setAttribute("gdsList", request.getParameter(PARAM_GDS_Publishing));
		request.setAttribute("flightID", request.getParameter(PARAM_FLIGHT_ID));
		request.setAttribute("viewModeOnly", request.getParameter(PARAM_VIEW_MODE_ONLY));
		request.setAttribute("haveMCFlight", request.getParameter(PARAM_HAVE_MC_FLIGHT));
	}

	/**
	 * sets the GDS list
	 * 
	 * @param request
	 */
	private static void setGDSListHtml(HttpServletRequest request) throws ModuleException {
		String gdsPublishing = request.getParameter(PARAM_GDS_Publishing);
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strFlightId = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_ID));
		if (!strFlightId.equalsIgnoreCase("")) {
			String strHtml = htmlGen.getGDSData(gdsPublishing, Integer.valueOf(strFlightId).intValue());
			request.setAttribute(WebConstants.SES_HTML_GDS_LIST_DATA, strHtml);
		}
	}

}
