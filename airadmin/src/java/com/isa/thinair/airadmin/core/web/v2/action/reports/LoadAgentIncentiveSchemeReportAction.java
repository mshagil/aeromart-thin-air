package com.isa.thinair.airadmin.core.web.v2.action.reports;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_AGENT_INCENTIVE_SCHEME_JSP)
public class LoadAgentIncentiveSchemeReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			String agentmultiSelect = ReportsHTMLGenerator.createAgentGSAMultiSelectStation("All", true, false, "190px", "350px");
			request.setAttribute("reqAgents", agentmultiSelect);
			String schemeMutiSelect = ReportsHTMLGenerator.createIncentiveMultiSelect();
			request.setAttribute("reqIncenScheme", schemeMutiSelect);
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}
}
