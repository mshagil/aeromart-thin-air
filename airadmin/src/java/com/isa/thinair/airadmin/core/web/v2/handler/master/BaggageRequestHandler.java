/**
 * 	mano
	May 5, 2011 
	2011
 */
package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.List;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.to.BaggageTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author mano
 * 
 */
public class BaggageRequestHandler {

	public static Page<Baggage> searchBaggage(int pageNumber, List<ModuleCriterion> criterion) throws ModuleException {

		Page<Baggage> page = null;
		if (criterion == null) {
			page = ModuleServiceLocator.getCommonServiceBD().getBaggages((pageNumber - 1) * 20, 20);
		} else {
			if (pageNumber == 0) {
				pageNumber = 1;
			}
			page = ModuleServiceLocator.getCommonServiceBD().getBaggages((pageNumber - 1) * 20, 20, criterion);
		}
		return page;

	}

	public static void saveBaggage(Baggage baggage, BaggageTO baggageTo) throws ModuleException {

		ModuleServiceLocator.getCommonServiceBD().saveBaggage(baggage, baggageTo);
	}

	public static Baggage getBaggage(String baggageName) throws ModuleException {

		Baggage baggage = ModuleServiceLocator.getCommonServiceBD().getBaggage(baggageName);

		return baggage;
	}

	public static void deleteBaggage(Integer baggageId) throws ModuleException {

		ModuleServiceLocator.getCommonServiceBD().deleteBaggage(baggageId);
	}

}
