/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class CountryContributionFlightOrSegmentRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CountryContributionFlightOrSegmentRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public CountryContributionFlightOrSegmentRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("CountryContributionFlightOrSegmentRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("CountryContributionFlightOrSegmentRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("CountryContributionFlightOrSegmentRH setReportView Success");
				return null;
			} else {
				log.error("CountryContributionFlightOrSegmentRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CountryContributionFlightOrSegmentRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountryList(request);
		setStationComboList(request);
		setLiveStatus(request);
		setReportingPeriod(request);

		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createStationList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setCountryList(HttpServletRequest request) throws ModuleException {
		String country = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, country);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String segFrom = request.getParameter("selSegFrom");
		String segTo = request.getParameter("selTo");
		String value = request.getParameter("selSegTo");
		String flightNo = request.getParameter("txtFlightNo");
		String country = request.getParameter("selCountry");
		String isFlight = request.getParameter("radSegmentFlight");
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			// ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
			// "c:/reports/CountryContributionForFlightOrSegment.jasper");
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					"c:/reports/CountryContributionForFlight.jasper");

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			search.setCountryOfResidence(country);
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (isFlight.equals("Flight")) {
				search.setByFlight(true);
				search.setFlightNumber(flightNo);
			} else {
				search.setBySector(true);
				search.setSectorFrom(segFrom);
				search.setSectorTo(segTo);
			}
			resultSet = ModuleServiceLocator.getDataExtractionBD().getCountryContributionPerFlightSegmentData(search); // /
																														// no
																														// reporting
																														// part

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("AMT_1", "(" + strBase + ")");

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=CountryContributionForFlight.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CountryContributionForFlight.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CountryContributionForFlight.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}
