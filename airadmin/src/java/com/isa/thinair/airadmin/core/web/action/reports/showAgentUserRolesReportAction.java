package com.isa.thinair.airadmin.core.web.action.reports;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.dto.AgentUserRolesDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class showAgentUserRolesReportAction extends BaseRequestResponseAwareAction {

	private static final Log log = LogFactory.getLog(showAgentUserRolesReportAction.class);

	private static final int PAGE_LENGTH = 20;
	private String msgType;
	private String messageTxt;
	private int page;
	private int total;
	private int records;
	private AgentUserRolesDTO agentUserRolesSearchDTO;
	private AgentUserRolesDTO agentUserRolesResultDTO;
	private Collection<Map<String, Object>> colAgentUserRoles;

	public String execute() {

		try {
			int startIndex = 0;
			if (page > 1) {
				startIndex = (page - 1) * PAGE_LENGTH;
			}
			if (agentUserRolesSearchDTO != null && !agentUserRolesSearchDTO.getPrivilege().isEmpty()) {

				Page pagedReocrds = ModuleServiceLocator.getDataExtractionBD().getRolesForPrivilege(agentUserRolesSearchDTO,
						startIndex, PAGE_LENGTH);

				bindData(pagedReocrds);
			}
		} catch (ModuleException me) {
			this.setMsgType(WebConstants.MSG_ERROR);
			this.setMessageTxt(me.getMessage());
			if (log.isErrorEnabled()) {
				log.error(me.getMessage());
			}
		} catch (Exception e) {
			this.setMsgType(WebConstants.MSG_ERROR);
			this.setMessageTxt(e.getMessage());
			if (log.isErrorEnabled()) {
				log.error(e.getMessage());
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private void bindData(Page pagedReocrds) {
		if (pagedReocrds != null) {
			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.setTotal(getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords()));
			this.setRecords(pagedReocrds.getTotalNoOfRecords());

			this.setColAgentUserRoles(((Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(
					pagedReocrds.getStartPosition(), pagedReocrds.getPageData(), decorateRow())));
		}
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	private RowDecorator<AgentUserRolesDTO> decorateRow() {
		RowDecorator<AgentUserRolesDTO> decorator = new RowDecorator<AgentUserRolesDTO>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, AgentUserRolesDTO record) {

				try {
					row.put("agentUserRolesResultDTO.role", record.getRole());

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		return decorator;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public AgentUserRolesDTO getAgentUserRolesResultDTO() {
		return agentUserRolesResultDTO;
	}

	public void setAgentUserRolesResultDTO(AgentUserRolesDTO agentUserRolesResultDTO) {
		this.agentUserRolesResultDTO = agentUserRolesResultDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public AgentUserRolesDTO getAgentUserRolesSearchDTO() {
		return agentUserRolesSearchDTO;
	}

	public void setAgentUserRolesSearchDTO(AgentUserRolesDTO agentUserRolesSearchDTO) {
		this.agentUserRolesSearchDTO = agentUserRolesSearchDTO;
	}

	public Collection<Map<String, Object>> getColAgentUserRoles() {
		return colAgentUserRoles;
	}

	public void setColAgentUserRoles(Collection<Map<String, Object>> colAgentUserRoles) {
		this.colAgentUserRoles = colAgentUserRoles;
	}

}
