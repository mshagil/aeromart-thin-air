/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class TopSectorReportRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(TopSectorReportRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public TopSectorReportRequestHandler() {
		super();
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());

		}
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		// String strHdnAction = request.getParameter("hdnAction");

		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setClientErrors(request);
			setLiveStatus(request);
			setReportingPeriod(request);
			setSalesChannelList(request);
			ReportsHTMLGenerator.createPreferedReportOptions(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("TopSectorReportRequestHandler setReportView Success");
				return null;
			} else {
				log.error("TopSectorReportRequestHandler setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("TopSectorReportRequestHandler setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ArrayList<String> lstSalesChannels = new ArrayList<String>();
		setClientErrors(request);
		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String noOfAgents = request.getParameter("radNoTopAgents");
		String otherNo = request.getParameter("txtTopAgentsOther");
		String[] salesChannels = request.getParameterValues("salesChanels");
		String id = "UC_REPM_013";
		String reportTemplate = "TopSegmentsReport.jasper";
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		ResultSet saleChannelsRS = null;

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (toDate != null && !toDate.equals("")) {
				search.setDateTo(ReportsHTMLGenerator.toDateType(toDate, false));
			}
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateFrom(ReportsHTMLGenerator.toDateType(fromDate, true));
			}

			if (noOfAgents != null && !noOfAgents.equals("")) {
				if (!noOfAgents.equals("other")) {
					search.setNoOfTops(Integer.parseInt(noOfAgents));
				} else {
					search.setNoOfTops(Integer.parseInt(otherNo));
				}
			}

			if (salesChannels != null && !salesChannels.equals("")) {
				// String[] arrSalesChannels = salesChannels.split(",");
				for (String salesChannel : salesChannels) {
					lstSalesChannels.add("'" + salesChannel + "'");
				}
				search.setSalesChannels(lstSalesChannels);
			}
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			resultSet = ModuleServiceLocator.getDataExtractionBD().getTopSegmentsData(search);

			saleChannelsRS = ModuleServiceLocator.getDataExtractionBD().getSalesChannelCodes(
					lstSalesChannels.toString().replace("[", "").replace("]", ""));

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("SALES_CHANNELES", getSalesChannelList(saleChannelsRS));
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", true);
			} else {
				parameters.put("SHW_DISCOUNT", false);
			}

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=TopSegmentsReport.pdf");
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=TopSegmentsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=TopSegmentsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static void setSalesChannelList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSalesChannelSelect();
		request.setAttribute(WebConstants.REQ_HTML_SALES_CHANNEL_LIST, strHtml);
	}

	private static String getSalesChannelList(ResultSet salesChannelRS) {
		StringBuilder sb = new StringBuilder();
		try {
			while (salesChannelRS.next()) {
				sb.append(salesChannelRS.getString(1));
				sb.append(" , ");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return sb.toString();
	}
}
