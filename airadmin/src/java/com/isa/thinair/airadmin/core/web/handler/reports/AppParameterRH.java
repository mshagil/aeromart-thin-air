package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * @author janandith jayawardena
 * 
 */

public class AppParameterRH extends BasicRequestHandler {

	/** logger for AppParameterRH class */
	private static Log log = LogFactory.getLog(AppParameterRH.class);

	/** Global Configurations */
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);

			log.debug("AppParameterRH setDisplayData() SUCCESS");
		} catch (Exception ex) {
			log.error("AppParameterRH setDisplayData() FAILED" + ex.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("AppParameterRH setReportView Success");
				return null;
			} else {
				log.error("AppParameterRH setReportView not selected");
			}

		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AppParameterRH setReportView Failed " + ex.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setAppParamSelections(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String strReportId = "UC_REPM_079";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportTemplate = "AppParamListReport.jasper";
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		try {
			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					searchCriteria.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					searchCriteria.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			String sortby = request.getParameter("selSortBy");

			List<String> paramData = new ArrayList<String>();

			if (request.getParameter("selFields").equals("Key")) {
				searchCriteria.setParamSelectionType("Key");
			} else if (request.getParameter("selFields").equals("Description")) {
				searchCriteria.setParamSelectionType("Description");
			}

			String paramDataSet = request.getParameter("hdnParams");
			if (paramDataSet != null || !paramDataSet.isEmpty()) {
				String paramsArr[] = paramDataSet.split(",");
				for (int r = 0; r < paramsArr.length; r++) {
					paramData.add(paramsArr[r]);
				}
				searchCriteria.setParamDataList(paramData);
			}

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getAppParameterDataforReports(searchCriteria);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("ID", strReportId);
			parameters.put("CARRIER", strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=App_Param_List_Report.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=App_Param_List_Report.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=App_Param_List_Report.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}

		} catch (Exception ex) {
		}
	}

	private static void setAppParamSelections(HttpServletRequest request) throws ModuleException {
		String strList = "";
		String strFieldType = request.getParameter("selFields");
		String reqFieldType = request.getParameter("hdnParamsType");

		if ((strFieldType != null && strFieldType.equals("Key")) || (reqFieldType != null && reqFieldType.equals("Key"))) {
			strList = ReportsHTMLGenerator.createAppParamsMultiSelect("Key");
		} else if (strFieldType != null && strFieldType.equals("Description")
				|| (reqFieldType != null && reqFieldType.equals("Description"))) {
			strList = ReportsHTMLGenerator.createAppParamsMultiSelect("Description");
		} else {
			strList = ReportsHTMLGenerator.createAppParamsMultiSelect("");
		}
		request.setAttribute("reqdata", strList);
		if (strFieldType != null && !strFieldType.isEmpty()) {
			request.setAttribute("hdnParamsType", strFieldType);
		}
		setAttribInRequest(request, "displayAgencyMode", 0);
	}
}
