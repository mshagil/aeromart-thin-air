package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.agents.CountryInfo;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.agents.GSASummary;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airtravelagents.api.dto.AgentInfoDTO;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.AgentPerformanceTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISAgentsGsaListAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowMISAgentsGsaListAction.class);
	private boolean success = true;
	private String messageTxt = null;
	private MISReportsSearchCriteria misSearchCriteria = null;
	private Collection<CountryInfo> countryList = new ArrayList<CountryInfo>();

	private Collection<HashMap<String, String>> top10Agents = new ArrayList<HashMap<String, String>>();
	private Collection<HashMap<String, String>> nonPerfAgents = new ArrayList<HashMap<String, String>>();
	private Collection<HashMap<String, String>> allAgents = new ArrayList<HashMap<String, String>>();

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		try {

			misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(request.getParameter("fromDate"));
			misSearchCriteria.setToDate(request.getParameter("toDate"));
			if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
				misSearchCriteria.setRegion(request.getParameter("region"));
			}
			if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
				misSearchCriteria.setPos(request.getParameter("pos"));
			}

			// Select all country codes for region.
			ArrayList<String> regionList = new ArrayList<String>();
			if (misSearchCriteria.getRegion() != null && !misSearchCriteria.getRegion().equals("")
					&& !misSearchCriteria.getRegion().equalsIgnoreCase("all")) {
				regionList.add(misSearchCriteria.getRegion());
			}
			List<Country> countriesForRegion = ModuleServiceLocator.getLocationServiceBD().getCountryCodesForRegions(regionList);

			List<String> countryCodes = new ArrayList<String>();
			LinkedHashMap<String, CountryInfo> countryInfoMap = new LinkedHashMap<String, CountryInfo>();

			for (Country country : countriesForRegion) {
				countryCodes.add(country.getCountryCode());

				CountryInfo countryInfo = new CountryInfo();
				countryInfo.setCountryCode(country.getCountryCode());
				countryInfo.setCountryName(country.getCountryName());

				countryInfoMap.put(country.getCountryCode(), countryInfo);
			}

			// Retrieve all agents for the countries
			HashMap<String, List<AgentInfoDTO>> agentInfoMap = ModuleServiceLocator.getTravelAgentBD().getAgentsForCountries(
					countryCodes);
			for (String countryCode : agentInfoMap.keySet()) {
				List<AgentInfoDTO> agentInfoDTOList = agentInfoMap.get(countryCode);
				for (AgentInfoDTO agentInfoDTO : agentInfoDTOList) {

					if (agentInfoDTO.getReportToGSA() != null && agentInfoDTO.getReportToGSA().equalsIgnoreCase("N")) {
						GSASummary gsaSummary = new GSASummary();
						gsaSummary.setGsaName(agentInfoDTO.getAgentName());
						gsaSummary.setLocationUrl(agentInfoDTO.getLocationUrl());
						gsaSummary.setStationCode(agentInfoDTO.getStationCode());
						gsaSummary.setStationDesc(agentInfoDTO.getStationName());
						gsaSummary.setGsaCode(agentInfoDTO.getAgentCode());

						if (agentInfoDTO.getAgentTypeCode() != null && agentInfoDTO.getAgentTypeCode().equals("TA")
								&& agentInfoDTO.getReportToGSA().equalsIgnoreCase("N")) {
							gsaSummary.setIndependentAgent(true);
							countryInfoMap.get(countryCode).addGSASummary(gsaSummary);
						} else if (agentInfoDTO.getAgentTypeCode() != null && agentInfoDTO.getAgentTypeCode().equals("GSA")) {
							countryInfoMap.get(countryCode).addGSASummary(gsaSummary);
						} else if (agentInfoDTO.getAgentTypeCode() != null && agentInfoDTO.getAgentTypeCode().equals("SO")) {
							gsaSummary.setSalesOffice(true);
							countryInfoMap.get(countryCode).addGSASummary(gsaSummary);
						}
					}

					HashMap<String, String> agentInfo = new HashMap<String, String>();
					agentInfo.put("agentName", agentInfoDTO.getAgentName());
					agentInfo.put("agentCode", agentInfoDTO.getAgentCode());
					allAgents.add(agentInfo);
				}
			}

			// set the country info list
			for (String countryCode : countryInfoMap.keySet()) {
				CountryInfo info = countryInfoMap.get(countryCode);
				if (info.getGsaList().size() > 0) {
					this.countryList.add(info);
				}
			}

			Collection<AgentPerformanceTO> bestPerfomance = ModuleServiceLocator.getMISDataProviderBD()
					.getMISRegionalBestPerformingAgents(misSearchCriteria);
			for (AgentPerformanceTO agentPerformanceTO : bestPerfomance) {
				HashMap<String, String> agentInfo = new HashMap<String, String>();
				agentInfo.put("agentName", agentPerformanceTO.getAgentName());
				agentInfo.put("agentCode", agentPerformanceTO.getAgentCode());
				top10Agents.add(agentInfo);
			}

			Collection<AgentPerformanceTO> nonPerfomance = ModuleServiceLocator.getMISDataProviderBD()
					.getMISRegionalLowPerformingAgents(misSearchCriteria);
			for (AgentPerformanceTO agentPerformanceTO : nonPerfomance) {
				HashMap<String, String> agentInfo = new HashMap<String, String>();
				agentInfo.put("agentName", agentPerformanceTO.getAgentName());
				agentInfo.put("agentCode", agentPerformanceTO.getAgentCode());
				nonPerfAgents.add(agentInfo);
			}

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public MISReportsSearchCriteria getMisSearchCriteria() {
		return misSearchCriteria;
	}

	public void setMisSearchCriteria(MISReportsSearchCriteria misSearchCriteria) {
		this.misSearchCriteria = misSearchCriteria;
	}

	public Collection<CountryInfo> getCountryList() {
		return countryList;
	}

	public void setCountryList(Collection<CountryInfo> countryList) {
		this.countryList = countryList;
	}

	public Collection<HashMap<String, String>> getTop10Agents() {
		return top10Agents;
	}

	public void setTop10Agents(Collection<HashMap<String, String>> top10Agents) {
		this.top10Agents = top10Agents;
	}

	public Collection<HashMap<String, String>> getNonPerfAgents() {
		return nonPerfAgents;
	}

	public void setNonPerfAgents(Collection<HashMap<String, String>> nonPerfAgents) {
		this.nonPerfAgents = nonPerfAgents;
	}

	public Collection<HashMap<String, String>> getAllAgents() {
		return allAgents;
	}

	public void setAllAgents(Collection<HashMap<String, String>> allAgents) {
		this.allAgents = allAgents;
	}
}
