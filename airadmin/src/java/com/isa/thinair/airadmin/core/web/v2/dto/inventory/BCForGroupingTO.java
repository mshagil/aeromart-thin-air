package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;

public class BCForGroupingTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String bcCode;

	private String logicalCCCode;

	private String logicalCCDescription;

	private String ccCode;

	private int groupId;

	private boolean isEditable;

	public String getBcCode() {
		return bcCode;
	}

	public void setBcCode(String bcCode) {
		this.bcCode = bcCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getLogicalCCDescription() {
		return logicalCCDescription;
	}

	public void setLogicalCCDescription(String logicalCCDescription) {
		this.logicalCCDescription = logicalCCDescription;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}
}
