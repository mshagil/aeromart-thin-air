package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

public class AgentUserPrivilegesReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ScheduleDetailReportRH.class);

	private static final String PARAM_AGENTS = "hdnAgents";

	private static final String PARAM_USERS = "hdnUsers";

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_AGENCIES = "selAgencies";

	private static final String PARAM_ROLES = "hdnRoles";

	private static final String PARAM_PRIVILEGES = "hdnPrivileges";

	private static final String PARAM_CHK_TA = "chkTAs";

	private static final String PARAM_CHK_CO = "chkCOs";

	private static final String PARAM_COUNTRY = "selCountry";

	private static final String PARAM_RPT_OPTION_1 = "radReportOption";

	private static final String PARAM_RPT_TYPE = "hdnRptType";

	private static final String PARAM_AGENT_NAME = "hdnAgentName";

	private static final String PARAM_PRIV_INDEX_FLG = "hdnPrivIndexFlag";

	private static final int MAX_URL_LENGTH = 1600;

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public AgentUserPrivilegesReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		Properties prop = getProperties(request);

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(prop, request);
			log.debug("AgentUserPrivilegesReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("AgentUserPrivilegesReportRH execute()" + e.getMessage());
		}

		try {
			if (prop.getProperty(PARAM_MODE) != null && prop.getProperty(PARAM_MODE).equals(WebConstants.ACTION_VIEW)) {
				setReportView(prop, request, response);
				log.error("AgentUserPrivilegesReportRH setReportView Success");
				return null;
			} else {
				log.error("AgentUserPrivilegesReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AgentUserPrivilegesReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(Properties prop, HttpServletRequest request) throws Exception {
		setAgentTypes(request);
		setGSAMultiSelectList(prop, request);
		setUserMultiSelectList(prop, request, false);
		setRoleMultiSelectList(prop, request);
		setPrivilegesMultiSelectList(prop, request);
		setClientErrors(request);
		setCountryList(request);
		setStationComboList(request);
		setTerritoryList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	protected static void setGSAMultiSelectList(Properties prop, HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		if (!"".equals(prop.getProperty(PARAM_CHK_TA))) {
			blnWithTAs = (prop.getProperty(PARAM_CHK_TA).equals("on") ? true : false);
		}
		if (!"".equals(prop.getProperty(PARAM_CHK_CO))) {
			blnWithCOs = (prop.getProperty(PARAM_CHK_CO).equals("on") ? true : false);
		}
		String strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(prop.getProperty(PARAM_AGENCIES), blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setUserMultiSelectList(Properties prop, HttpServletRequest request, boolean loadFromAgent)
			throws ModuleException {
		String strAgents = null;
		if (!prop.getProperty(PARAM_AGENTS).equals("")) {
			strAgents = prop.getProperty(PARAM_AGENTS);
		}

		String strList = ReportsHTMLGenerator.createUserAgentMultiSelect(strAgents);
		request.setAttribute(WebConstants.REQ_HTML_AGENT_USER_LIST, strList);
	}

	protected static void setPrivilegesMultiSelectList(Properties prop, HttpServletRequest request) throws ModuleException {
		String strRoles = null;
		if (!prop.getProperty(PARAM_ROLES).equals("")) {
			strRoles = prop.getProperty(PARAM_ROLES);
		}
		String strList = ReportsHTMLGenerator.createRolePrivilegeMultiSelect(strRoles);
		request.setAttribute(WebConstants.REQ_HTML_ROLE_PRIVILEGE_LIST, strList);
	}

	protected static void setRoleMultiSelectList(Properties prop, HttpServletRequest request) throws ModuleException {
		String strUsers = null;
		if (!prop.getProperty(PARAM_USERS).equals("")) {
			strUsers = prop.getProperty(PARAM_USERS);
		}
		String strList = ReportsHTMLGenerator.createFilteredRoleMultiSelect(strUsers);
		request.setAttribute(WebConstants.REQ_HTML_ROLE_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createStationList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	public static void setCountryList(HttpServletRequest request) throws ModuleException {
		String country = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, country);
	}

	public static void setAgentTypeList(HttpServletRequest request) throws ModuleException {
		String types = SelectListGenerator.createAgentTypeCodesList();
		request.setAttribute(WebConstants.REQ_AGENT_TYPE, types);
	}

	public static void setTerritoryList(HttpServletRequest request) throws ModuleException {
		String types = SelectListGenerator.createTerritoryList();
		request.setAttribute(WebConstants.REQ_TERRITORY, types);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(Properties prop, HttpServletRequest request, HttpServletResponse response)
			throws ModuleException {

		if (prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_LIST_SUMMARY)
				|| prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_LIST_DETAILS)
						|| prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_PRIVILEGE_DETAILS)) {

			ResultSet resultSet = null;
			Map subReportDataMap = null;
			String reportTemplateSummary;
			String reportName;

			String id = "UC_REPM_028";

			Map parameters = new HashMap<String, String>();

			try {
				ReportsSearchCriteria search = new ReportsSearchCriteria();

				if (request.getParameter(WebConstants.REP_HDN_LIVE).equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (request.getParameter(WebConstants.REP_HDN_LIVE).equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}

				ArrayList<String> userCol = new ArrayList<String>();
				String userArr[] = prop.getProperty(PARAM_USERS).split(",");
				for (int r = 0; r < userArr.length; r++) {
					userCol.add(userArr[r]);
				}

				search.setUsers(userCol);

				String privilegesCol = "";
				String privIndexCol = "";

				if (!prop.getProperty(PARAM_ROLES).equals("")) {
					ArrayList<String> roleCollection = new ArrayList<String>();
					String roleArray[] = prop.getProperty(PARAM_ROLES).split(",");
					for (String role : roleArray) {
						roleCollection.add(role.trim());
					}

					if (!prop.getProperty(PARAM_PRIVILEGES).equals("")) {
						ArrayList<String> privilegeCollecton = new ArrayList<String>();
						String priArray[] = prop.getProperty(PARAM_PRIVILEGES).split(",");
						privilegesCol = prop.getProperty(PARAM_PRIVILEGES);

						if (prop.getProperty(PARAM_PRIV_INDEX_FLG).equals("1")) {
							Collection<String[]> privilages = SelectListGenerator.getAllPrivileges();
							Iterator<String[]> strArr = privilages.iterator();

							List<String> lst = new ArrayList<String>();
							while (strArr.hasNext()) {
								String[] sr = strArr.next();
								String privId = sr[0];
								lst.add(privId);
							}

							for (String priv : priArray) {
								privIndexCol = privIndexCol + Integer.toString(lst.indexOf(priv)) + ",";
							}

							privilegesCol = privIndexCol;
						}

						for (String privilege : priArray) {
							privilegeCollecton.add(privilege);
						}
						search.setPrivileges(privilegeCollecton);

						ArrayList<String> roleList = new ArrayList<String>(ModuleServiceLocator.getDataExtractionBD()
								.getRoleForPrivileges(search));
						search.setRoles(roleList);
					} else {
						search.setRoles(roleCollection);
					}
				}

				resultSet = ModuleServiceLocator.getDataExtractionBD().getAgentUserData(search);	
				
				// Print User List With Privilege Detail Report
				if( prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_PRIVILEGE_DETAILS)){
				subReportDataMap = ModuleServiceLocator.getDataExtractionBD().getAgentUserRoleDetailData(search);
				reportTemplateSummary = "AgentUserPrivilegesDetails.jasper";
				reportName = "AgentUserPrivilegesDetails";
				
				}else{
				subReportDataMap = ModuleServiceLocator.getDataExtractionBD().getAgentUserRoleData(search);
				reportTemplateSummary = "AgentUserPrivileges.jasper";
				reportName = "AgentUserPrivileges";
				}
				if (subReportDataMap != null || subReportDataMap.size() != 0) {
					Collection col = subReportDataMap.keySet();
					for (Iterator iter = col.iterator(); iter.hasNext();) {
						String key = (String) iter.next();
						Collection recList = (Collection) subReportDataMap.get(key);
						subReportDataMap.put(key, new JRMapCollectionDataSource(recList));
					}
				}

				String strLogo = AppSysParamsUtil.getReportLogo(false);
				String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
				String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

				StringBuffer rolesSB = new StringBuffer();
				if (search.getRoles() != null) {
					List<String> roleList = new ArrayList<String>(search.getRoles());
					for (int i = 0; i < roleList.size(); i++) {
						rolesSB.append(roleList.get(i));
						if ((i + 1) < roleList.size()) {
							rolesSB.append(",");
						}
					}
				}

				StringBuffer urlSB = new StringBuffer();
				urlSB.append(rolesSB.toString()).append(privilegesCol);
				if (urlSB.length() > MAX_URL_LENGTH) {
					privilegesCol = "";
					rolesSB = new StringBuffer();
				}

				parameters.put("SUB_REP_DATA", subReportDataMap);
				parameters.put("REPORT_ID", id);
				parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameters.put("REPORT_MEDIUM", request.getParameter(WebConstants.REP_HDN_LIVE));
				parameters.put("SUBREPORT_DIR", PlatformConstants.getConfigRootAbsPath() + "/templates/reports/");
				parameters.put("PARAM_ROLES", rolesSB.toString());
				parameters.put("PARAM_PRIVILEGES", privilegesCol);

				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));

				// To provide Report Format Options
				String reportNumFormat = request.getParameter("radRptNumFormat");
				ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

				if ((prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_LIST_SUMMARY)
						|| prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("CSV")) 
						&& !prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_PRIVILEGE_DETAILS)) {
					parameters.put("REPORT_TYPE", ReportsSearchCriteria.USER_LIST_SUMMARY);
				} else {
					parameters.put("REPORT_TYPE", ReportsSearchCriteria.USER_LIST_DETAILS);
				}

				if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("HTML")) {
					parameters.put("IMG_PATH", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							null, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("PDF")) {
					response.reset();
					response.addHeader("Content-Disposition", String.format("filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
					reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG_PATH", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("EXCEL")) {
					reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG_PATH", reportsRootDir);
					response.addHeader("Content-Disposition",String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition",String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

				}
			} catch (Exception e) {
				log.error(e.getStackTrace());
			}
		} else if (prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.USER_LIST_PRIVI)) {

			ResultSet resultSet = null;

			String id = "UC_REPM_028";
			String reportTemplateSummary = "AgentUserPrivileges_RolePriviList.jasper";
			Map<String, Object> parameters = new HashMap<String, Object>();
			String reportName = "AgentUserPrivilegesRolePriviList";

			try {
				ReportsSearchCriteria search = new ReportsSearchCriteria();

				if (request.getParameter(WebConstants.REP_HDN_LIVE).equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (request.getParameter(WebConstants.REP_HDN_LIVE).equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}

				search.setUserId(prop.getProperty(PARAM_USERS));

				if (!prop.getProperty(PARAM_ROLES).equals("")) {
					ArrayList<String> roleCollection = new ArrayList<String>();
					String roleArray[] = prop.getProperty(PARAM_ROLES).split(",");
					for (String role : roleArray) {
						roleCollection.add(role.trim());
					}

					if (!prop.getProperty(PARAM_PRIVILEGES).equals("")) {
						ArrayList<String> privilegeCollecton = new ArrayList<String>();
						String priArray[] = prop.getProperty(PARAM_PRIVILEGES).split(",");

						if (prop.getProperty(PARAM_PRIV_INDEX_FLG).equals("2")) {
							Collection<String[]> priv = SelectListGenerator.getAllPrivileges();
							List<String[]> lstCol = new ArrayList<String[]>(priv);
							for (String strIndex : priArray) {
								int index = Integer.valueOf(strIndex);
								String[] arrRec = (String[]) lstCol.get(index);
								privilegeCollecton.add(arrRec[0]);
							}
						}

						search.setPrivileges(privilegeCollecton);

						ArrayList<String> roleList = new ArrayList<String>(ModuleServiceLocator.getDataExtractionBD()
								.getRoleForPrivileges(search));
						search.setRoles(roleList);
					} else {
						search.setRoles(roleCollection);
					}
				}

				resultSet = ModuleServiceLocator.getDataExtractionBD().getUserPrivilegeData(search);

				String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
				String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
				String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

				parameters.put("REPORT_ID", id);
				parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameters.put("REPORT_MEDIUM", request.getParameter(WebConstants.REP_HDN_LIVE));

				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));

				if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("HTML")) {
					parameters.put("IMG_PATH", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							null, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("PDF")) {
					reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG_PATH", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("EXCEL")) {
					parameters.put("IMG_PATH", reportsRootDir);
					response.addHeader("Content-Disposition",String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition",String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

				}
			} catch (Exception e) {
				log.error(e.getStackTrace());
			}
		} else if (prop.getProperty(PARAM_RPT_TYPE).equals(ReportsSearchCriteria.PRIVILEGE_DETAILS)) {

			ResultSet resultSet = null;

			String id = "UC_REPM_028";
			String reportTemplateSummary = "PrivilegeDetails.jasper";
			Map<String, Object> parameters = new HashMap<String, Object>();
			String reportName = "PrivilegeDetails";

			try {
				ReportsSearchCriteria search = new ReportsSearchCriteria();

				if (request.getParameter(WebConstants.REP_HDN_LIVE).equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (request.getParameter(WebConstants.REP_HDN_LIVE).equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}

				search.setUserId(prop.getProperty(PARAM_USERS));

				if (!prop.getProperty(PARAM_ROLES).equals("")) {
					ArrayList<String> roleCollection = new ArrayList<String>();
					String roleArray[] = prop.getProperty(PARAM_ROLES).split(",");
					for (String role : roleArray) {
						roleCollection.add(role.trim());
					}
					search.setRoles(roleCollection);

					if (!prop.getProperty(PARAM_PRIVILEGES).equals("")) {
						ArrayList<String> privilegeCollecton = new ArrayList<String>();
						String priArray[] = prop.getProperty(PARAM_PRIVILEGES).split(",");
						for (String privilege : priArray) {
							privilegeCollecton.add(privilege);
						}
						search.setPrivileges(privilegeCollecton);
					}
				}

				resultSet = ModuleServiceLocator.getDataExtractionBD().getPrivilegeDetailsData(search);

				String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
				String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
				String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

				parameters.put("REPORT_ID", id);
				parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameters.put("REPORT_MEDIUM", request.getParameter(WebConstants.REP_HDN_LIVE));

				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));

				if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("HTML")) {
					parameters.put("IMG_PATH", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							null, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("PDF")) {
					reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG_PATH", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("EXCEL")) {
					parameters.put("IMG_PATH", reportsRootDir);
					response.addHeader("Content-Disposition",String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
				} else if (prop.getProperty(PARAM_RPT_OPTION_1).trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition",String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

				}
			} catch (Exception e) {
				log.error(e.getStackTrace());
			}
		}
	}

	/**
	 * Sets the User Request To a Property File
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the File Containg User Request Values
	 */
	protected static Properties getProperties(HttpServletRequest request) {

		Properties props = new Properties();

		String hdnAgents = request.getParameter(PARAM_AGENTS);
		String hdnUsers = request.getParameter(PARAM_USERS);
		String hdnRoles = request.getParameter(PARAM_ROLES);
		String hdnPrivileges = request.getParameter(PARAM_PRIVILEGES);
		String hdnMode = request.getParameter(PARAM_MODE);
		String hdnRptType = request.getParameter(PARAM_RPT_TYPE);
		String selAgencies = request.getParameter(PARAM_AGENCIES);
		String chkTAs = request.getParameter(PARAM_CHK_TA);
		String chkCOs = request.getParameter(PARAM_CHK_CO);
		String hdnPrivIndexFlag = request.getParameter(PARAM_PRIV_INDEX_FLG);

		String selCountry = request.getParameter(PARAM_COUNTRY);
		String radReportOption = request.getParameter(PARAM_RPT_OPTION_1);
		String hdnAgentName = request.getParameter(PARAM_AGENT_NAME);

		props.setProperty(PARAM_AGENTS, AiradminUtils.getNotNullString(hdnAgents));
		props.setProperty(PARAM_USERS, AiradminUtils.getNotNullString(hdnUsers));
		props.setProperty(PARAM_ROLES, AiradminUtils.getNotNullString(hdnRoles));
		props.setProperty(PARAM_PRIVILEGES, AiradminUtils.getNotNullString(hdnPrivileges));
		props.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(hdnMode));
		props.setProperty(PARAM_AGENCIES, AiradminUtils.getNotNullString(selAgencies));
		props.setProperty(PARAM_CHK_TA, AiradminUtils.getNotNullString(chkTAs));
		props.setProperty(PARAM_CHK_CO, AiradminUtils.getNotNullString(chkCOs));
		props.setProperty(PARAM_COUNTRY, AiradminUtils.getNotNullString(selCountry));
		props.setProperty(PARAM_RPT_OPTION_1, AiradminUtils.getNotNullString(radReportOption));
		props.setProperty(PARAM_RPT_TYPE, AiradminUtils.getNotNullString(hdnRptType));
		props.setProperty(PARAM_AGENT_NAME, AiradminUtils.getNotNullString(hdnAgentName));
		props.setProperty(PARAM_PRIV_INDEX_FLG, AiradminUtils.getNotNullString(hdnPrivIndexFlag));

		return props;
	}

}
