package com.isa.thinair.airadmin.core.web.handler.master;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.web.action.flightsched.ShowPublishRoutesAction;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;



public class ShowPublishRoutesRH extends BasicRequestHandler {

	private static final String HDN_MODE = "hdnMode";
	private static final String GDS_CODE = "gdsCode";
	
	private static Log log = LogFactory.getLog(ShowPublishRoutesAction.class);

	public static String execute(HttpServletRequest request) {

		String hdnMode;
		String gdsCode;

		try {
			String forward = null;
			gdsCode = AiradminUtils.getNotNullString(request.getParameter(GDS_CODE));
			hdnMode = request.getParameter(HDN_MODE);
			if (hdnMode != null) {

				if (hdnMode.equals(WebConstants.ACTION_VIEW)) {
					request.setAttribute(GDS_CODE, gdsCode);
					forward = AdminStrutsConstants.AdminAction.SUCCESS;
					return forward;

				} 

			}

			return forward;
		} catch (Exception e) {
			request.setAttribute(WebConstants.REQ_MESSAGE, e.getMessage());
			return AdminStrutsConstants.AdminAction.ERROR;
		}

	}

	
}

