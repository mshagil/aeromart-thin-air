/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */
package com.isa.thinair.airadmin.core.web.handler.tools;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.CashSalesTransferHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.LookupUtils;

/**
 * @author srikantha
 * 
 * 
 *         Preferences - Java - Code Style - Code Templates
 */

public final class CashSalesTransferRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CashSalesTransferRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_SEARCH_AGENTCODE = "selAgent";

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_SELECTED_DATES = "hdnSelectedDates";

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * @param request
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		// Retriving App param on viewing Report Download Activation
		Boolean isViewReport = AppSysParamsUtil.isDownLoadCSVCashSalesReport();

		String strUIModeJS = "var isSearchMode = false; var isAgentSelect = false;";
		String strCashSalesStatus = "var isShowReport = " + isViewReport.toString() + ";";
		setExceptionOccured(request, false);
		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";"; // 0-Not Applicable,
																					// 1-GenerateSuccess,
																					// 2-TransferSuccess 3-Fail
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		request.setAttribute(WebConstants.REQ_VIEW_REPORT_STATUS, strCashSalesStatus);
		try {
			if (strHdnMode != null && strHdnMode.equals("REPORT") && AppSysParamsUtil.isDownLoadCSVCashSalesReport()) {
				String strSelectedDates = request.getParameter(PARAM_SELECTED_DATES);
				String ds[] = strSelectedDates.split(",");
				Collection<Date> dateList = new ArrayList<Date>();
				for (int i = 0; i < ds.length; i++) {
					if (!ds[i].equals("")) {
						Date day = getDate(ds[i]);
						dateList.add(day);
					}
				}
				setReportView(response, dateList);
				setDisplayData(request);
				return null;
			} else {
				if ((strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GENERATE))
						|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_TRANSFER))) {
					if (transferCashSales(request, response)) {

					}
				}
				setDisplayData(request);
			}
		} catch (Exception exception) {
			log.error("CashSalesTransferRequestHandler.execute method is failed :" + exception.getMessage(), exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static boolean transferCashSales(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			String strHdnMode = request.getParameter(PARAM_MODE);
			String strSelectedDates = request.getParameter(PARAM_SELECTED_DATES);

			String ds[] = strSelectedDates.split(",");
			Collection<Date> dateList = new ArrayList<Date>();
			for (int i = 0; i < ds.length; i++) {
				if (!ds[i].equals("")) {
					Date day = getDate(ds[i]);
					dateList.add(day);
				}
			}
			if (!strHdnMode.equals("") && strHdnMode.equals("GENERATE")) { // When Status equals U

				ModuleServiceLocator.getReservationAuxilliaryBD().transferManualCashSalesCollection(dateList);
				setIntSuccess(request, 1);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_GENERATE_SUCCESS), WebConstants.MSG_SUCCESS);
			} else if (!strHdnMode.equals("") && strHdnMode.equals("TRANSFER")) { // When Status equals N

				ModuleServiceLocator.getReservationAuxilliaryBD().transferManualCashSalesCollection(dateList);
				setIntSuccess(request, 2);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_TRANSFER_SUCCESS), WebConstants.MSG_SUCCESS);
			}
			setExceptionOccured(request, false);

		} catch (ModuleException moduleException) {
			log.error(
					"CashSalesTransferRequestHandler.transferCashSales() method is failed :" + moduleException.getMessageString(),
					moduleException);

			setExceptionOccured(request, true);
			setIntSuccess(request, 3);
			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";

			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";"; // 0-Not Applicable,
																					// 1-SendNewPNLSuccess,
																					// 2-SendNewADLSuccess
																					// 3-ResendPNLSuccess,
																					// 4-ResendADLSuccess
																					// 5-PrintSuccess, 6-Fail
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			setIntSuccess(request, 3);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
		return true;
	}

	private static Date getDate(String format) throws java.text.ParseException {
		SimpleDateFormat dateFormat = null;
		Date dt = null;
		if (!format.equals("")) {
			if (format.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yy");
			}
			if (format.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yy");
			}
			if (format.indexOf(' ') != -1) {
				format = format.substring(0, format.indexOf(' '));
			}

			dt = dateFormat.parse(format);
		}
		return dt;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);
		String strAgent = request.getParameter(PARAM_SEARCH_AGENTCODE);

		decideAgentList(request);

		if (strAgent != null && (strUIMode.equals("AGENTCHANGE") || strUIMode.equals(WebConstants.ACTION_SEARCH))) {
			setUserHtml(request);
		}
		if (strUIMode != null && (strUIMode.equals(WebConstants.ACTION_SEARCH) || strUIMode.equals("AGENTCHANGE"))) {
			setCashSalesTransferRowHtml(request);
		}

		setClientErrors(request);

		if (!isExceptionOccured(request)) {

			String strFormData = "var arrFormData = new Array();";

			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";"; // 0-Not Applicable, 1-GenerateSuccess,
																					// 2-TransferSuccess 3-Fail
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

	}

	private static void setUserHtml(HttpServletRequest request) throws ModuleException {

		String strUserList = JavascriptGenerator.createUsersForTavelAgentHtml(request.getParameter(PARAM_SEARCH_AGENTCODE));
		request.setAttribute(WebConstants.REQ_HTML_USER_DATA, strUserList);
	}

	private static void decideAgentList(HttpServletRequest request) throws ModuleException {
		Collection<String[]> l = null;
		try {

			String strFromDate = "";
			String strToDate = "";
			String dates[];

			dates = constructDatesForSql(request.getParameter("txtFrom"), request.getParameter("txtTo"));

			if (dates != null && dates.length == 2) {

				strFromDate = dates[0];
				strToDate = dates[1];

				String sql = " select distinct t.agent_code, ta.agent_name " + " from t_pax_transaction t, t_agent ta "
						+ " where t.agent_code = ta.agent_code and " + " t.tnx_date between to_date(" + strFromDate + " ) "
						+ " and to_date( " + strToDate + ")" + " and t.nominal_code in(18,26)" + " and t.agent_code not in "
						+ " (select distinct a.AGENT_CODE " + " from T_AGENT a,T_agent_payment_method b "
						+ " where a.agent_code=b.agent_code and b.payment_code ='CA')";
				l = LookupUtils.LookupDAO().getTwoColumnsData(sql);
			}

			Collection<String[]> list = SelectListGenerator.getAgentCodesForCashSales();

			if (l != null && l.size() < 0) {
				list.addAll(l);
			}
			String strHtml = SelectListGenerator.create_Code_Description_List(list);
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("CashSalesTransferRequestHandler.setAgentList method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static String[] constructDatesForSql(String strFromDate, String strToDate) throws ModuleException {

		if (strFromDate == null || strToDate == null || strFromDate.equals("") || strToDate.equals("")) {
			return null;
		}
		Date fromDate = null;
		Date toDate = null;
		SimpleDateFormat dateFormat = null;

		if (!strFromDate.equals("")) {
			if (strFromDate.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yy");
			}
			if (strFromDate.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yy");
			}
			if (strFromDate.indexOf(' ') != -1) {
				strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
			}

			try {
				fromDate = dateFormat.parse(strFromDate);
			} catch (ParseException e) {
				log.error(e);
			}
		}

		if (!strToDate.equals("")) {
			if (strToDate.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yy");
			}
			if (strFromDate.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yy");
			}
			if (strFromDate.indexOf(' ') != -1) {
				strToDate = strToDate.substring(0, strToDate.indexOf(' '));
			}

			try {
				toDate = dateFormat.parse(strToDate);
			} catch (ParseException e) {
				log.error(e);
			}
		}

		SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		String dtFrom = newDateFormat.format(fromDate);
		String dtEnd = newDateFormat.format(toDate);

		dtFrom = "'" + dtFrom + " 00:00:00'" + ", " + "'dd-mon-yyyy HH24:mi:ss'";
		dtEnd = "'" + dtEnd + " 23:59:59'" + ", " + "'dd-mon-yyyy HH24:mi:ss'";
		return new String[] { dtFrom, dtEnd };

	}

	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = CashSalesTransferHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error(
					"CashSalesTransferRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	private static void setCashSalesTransferRowHtml(HttpServletRequest request) throws Exception {

		try {

			CashSalesTransferHTMLGenerator htmlGen = new CashSalesTransferHTMLGenerator();
			String strHtml = htmlGen.getCashSalesTransferRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());

		} catch (ModuleException moduleException) {
			log.error(
					"CashSalesTransferHandler.setCashSalesTransferRowHtml() method is failed :"
							+ moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setReportView(HttpServletResponse response, Collection<Date> dateList) throws ModuleException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			String reportTemplate = "cashSalesTransferReport.jasper";
			ResultSet resultSet = ModuleServiceLocator.getReservationAuxilliaryBD().getSalesTransferServiceData(dateList,
					ReservationTnxNominalCode.getCashTypeNominalCodes());

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=cashSalesTransferReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		} catch (Exception e) {
			log.error(e);
		}
	}
}
