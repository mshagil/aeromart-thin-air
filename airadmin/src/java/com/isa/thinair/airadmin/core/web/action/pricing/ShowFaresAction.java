package com.isa.thinair.airadmin.core.web.action.pricing;

import java.io.File;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.handler.pricing.FareUploadRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.FARE_CHG_UPLOAD_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.RES_SUCCESS, value = AdminStrutsConstants.AdminJSP.FARE_CHG_UPLOAD_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowFaresAction extends BaseRequestAwareAction {
	private File upload;

	public String execute() throws Exception {
		setFilePath();
		return FareUploadRequestHandler.execute(request);
	}

	private void setFilePath() {
		if (null != upload) {
			try {
				request.setAttribute("csvFareUploadFileName", upload);
			} catch (Exception e) {

			}
		}

	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}
}
