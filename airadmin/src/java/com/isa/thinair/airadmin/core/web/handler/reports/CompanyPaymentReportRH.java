/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.webplatform.api.util.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class CompanyPaymentReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CompanyPaymentReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {


		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		String currentTimestamp = DateUtil.formatDate(new Date(), "dd/MM/yyyy");
		setAttribInRequest(request, "systemDate", currentTimestamp);

		String offlineReportParams = AppSysParamsUtil.getEnableOfflineReportParams();

		setAttribInRequest(request, "offlineReportParams", offlineReportParams);

		try {
			setDisplayData(request);
			log.debug("CompanyPaymentReportRH SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("CompanyPaymentReportRH SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("CompanyPaymentReportRH setReportView Success");
				return null;
			} else {
				log.error("CompanyPaymentReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ModeOfPaymentsRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {

		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		String agentStatus = "";
		
		if (request.getParameter("agentStatusFilter") != null) {
			agentStatus = request.getParameter("agentStatusFilter");
		}

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs, agentStatus);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setClientErrors(request);
		setGSAMultiSelectList(request);
		setPaymentModeList(request);
		setLiveStatus(request);
		setPaySource(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setPaymentType(request);
		setFlightTypesHtml(request);
		setFareDiscountCodes(request);
		setAgentStatusTypes(request);
		setStaions(request);
		setEntities(request);
		
	}

	private static void setFlightTypesHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightTypeList(false);
		setAttribInRequest(request, WebConstants.REQ_HTML_FLIGHT_TYPES, strHtml);
	}

	private static void setFareDiscountCodes(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFareDiscountCodes();
		setAttribInRequest(request, WebConstants.REQ_HTML_FARE_DISCOUNT_TYPES, strHtml);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {

		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.comp.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.comp.rpt") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.comp") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	private static void setPaymentModeList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createPaymentModeHtml(false);
		request.setAttribute(WebConstants.REQ_HTML_PAYMENTTYPE_LIST, strList);
	}

	private static void setPaymentType(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REP_MOD_DETAILS_ENABLED, AppSysParamsUtil.isModificationFilterEnabled());
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setPaySource(HttpServletRequest request) throws ModuleException {
		String strPay = "false";
		if (AccelAeroClients.AIR_ARABIA.equals(CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
				|| AccelAeroClients.AIR_ARABIA_GROUP.equals(CommonsServices.getGlobalConfig().getBizParam(
						SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))) {
			strPay = "true";
		}
		request.setAttribute(WebConstants.REP_RPT_SHOW_PAY, strPay);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String localTime = request.getParameter("chkLocalTime");
		String value = request.getParameter("radReportOption");
		String agents = "";
		String reportView = request.getParameter("hdnReportView");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strPaySource = request.getParameter("selPaySource");
		String reportViewType = request.getParameter("chkNewview");
		String selFlightType = request.getParameter("selFlightType");
		boolean isSales = request.getParameter("chkSales") != null && request.getParameter("chkSales").trim().equals("Sales");
		boolean isRefunds = request.getParameter("chkRefunds") != null
				&& request.getParameter("chkRefunds").trim().equals("Refunds");
		boolean modificationDetails = request.getParameter("chkModifications") != null
				&& request.getParameter("chkModifications").trim().equals("Modifications");
		boolean isNewView = (reportViewType != null && reportViewType.equalsIgnoreCase("on")) ? true : false;
		String fareDiscountCode = request.getParameter("selFareDiscountCode");
		boolean isReportInLocalTime = (localTime != null && localTime.equalsIgnoreCase("on")) ? true : false;
		boolean isSelectedAllAgents = false;
		boolean hasSelectedAgents = true;

		/*if (request.getParameter("hdnSelectedAllAgents") != null && request.getParameter("hdnSelectedAllAgents").equals("true")) {
			isSelectedAllAgents = true;
		}*/

		String currencies = request.getParameter("currencies");
		if (strPaySource == null) {
			strPaySource = "INTERNAL";
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}
		
		String strEntity = request.getParameter("selEntity");
		String strEntityText = request.getParameter("hdnEntityText");
		ArrayList<String> agentCol = new ArrayList<String>();
		String payments = request.getParameter("hdnPayments");
		String stations = request.getParameter("hdnStations");
		String chkAddInfo = request.getParameter("chkAddInfo");
		boolean bolAdditionalInfo = (chkAddInfo != null) ? true : false;

		String id = "UC_REPM_023";
		String templateDetail = "CompanyPaymentReportDetail.jasper";
		// String templateSummary = "CompanyPaymentReportSummary.jasper";
		// Replaces consolidated Summary Report Template
		String templateCompanyPaymentSummary = "CompPayRptSummary.jasper";

		String showPaymentCurrency = "N";
		String showOnlyPaymentBreakDown = "N";
		String showCurrencyWithBreakDown = "N";
		String showAdditionalInfo = "N";

		if (!isSelectedAllAgents) {
			String agentArr[] = agents.split(",");
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
		}
		ArrayList<String> paymentCol = new ArrayList<String>();

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			ReportsSearchCriteria searchRecs = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			if (isReportInLocalTime) {
				Collection<UserDST> userDST = ((UserPrincipal) request.getUserPrincipal()).getColUserDST();
				for (UserDST dst : userDST) {
					if (dst.getDstStartDateTime() == null
							|| DateUtil.compareZuluDates(dst.getDstStartDateTime(), dst.getDstEndDateTime(),
									DateUtil.parseDate(fromDate, "dd/MM/yyyy"))) {
						search.setGmtFromOffsetInMinutes(dst.getGmtOffsetMinutes());
					}
					if (dst.getDstStartDateTime() == null
							|| DateUtil.compareZuluDates(dst.getDstStartDateTime(), dst.getDstEndDateTime(),
									DateUtil.parseDate(toDate, "dd/MM/yyyy"))) {
						search.setGmtToOffsetInMinutes(dst.getGmtOffsetMinutes());
					}
				}
			}

			Calendar invoicePeriodToDate = new GregorianCalendar();
			int[] toDateArr = ReportsHTMLGenerator.getYearMonthDay(toDate);
			invoicePeriodToDate.set(Calendar.YEAR, toDateArr[0]);
			invoicePeriodToDate.set(Calendar.MONTH, toDateArr[1] - 1);
			invoicePeriodToDate.set(Calendar.DAY_OF_MONTH, toDateArr[2]);

			Date lastInvoiceDate = getLastInvoicePeriodStartDate();

			if (invoicePeriodToDate.getTime().after(lastInvoiceDate)) {
				SimpleDateFormat queryDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				search.setToDate(queryDateFormat.format(lastInvoiceDate.getTime()));
				search.setToDate(ReportsHTMLGenerator.convertDate(search.getToDate()));
				search.setDisplayAgentAdditionalInfo(true);
			} else {
				search.setDisplayAgentAdditionalInfo(false);
			}

			search.setFareDiscountCode(fareDiscountCode);

			Map<String, Object> parameters = new HashMap<String, Object>();
			search.setAgentCode(agents);
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
				searchRecs.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
				searchRecs.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
			}
			String paymentArr[] = null;
			if (payments.indexOf(",") != -1) {
				paymentArr = payments.split(",");
			} else {
				paymentArr = new String[1];
				paymentArr[0] = payments;
			}

			boolean isSalesModifications = isSales;
			search.setSales(isSales);
			search.setRefund(isRefunds);
			if (AppSysParamsUtil.isModificationFilterEnabled()) {
				isSalesModifications = isSales || modificationDetails;
			}
			
			if(stations !=null && !stations.isEmpty()){
				String stationArr[] = null;
				stationArr = stations.split(",");
				search.setStations(Arrays.asList(stationArr));
				search.setByStation(true);
			}else{
				search.setStations(null);
				search.setByStation(false);
			}
			
			String paymentStr = "";
			if (paymentArr != null) {
				for (int r = 0; r < paymentArr.length; r++) {

					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];

					}
				}

				String refundCodes[] = { "29", "24", "23", "22", "26", "25", "31", "33", "37", "47" };
				for (int r = 0; r < paymentArr.length; r++) {
					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];
					}
					if (paymentStr.equals("28")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[0]);
					}
					if (paymentStr.equals("17")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[1]);
					}
					if (paymentStr.equals("16")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[2]);
					}
					if (paymentStr.equals("15")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[3]);
					}
					if (paymentStr.equals("18")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[4]);
					}
					if (paymentStr.equals("19")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[5]);
						if (strPaySource.equals("EXTERNAL")) {// hard coding all other codes are hard coded
							accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, "10", "11");
						}
					}
					if (paymentStr.equals("30")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[6]);
					}
					if (paymentStr.equals("32")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[7]);
					}
					if (paymentStr.equals("36")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[8]);
					}
					if (paymentStr.equals("46")) {
						accumulateNominalCodes(paymentCol, isSalesModifications, isRefunds, paymentStr, refundCodes[9]);
					}
					if (paymentStr.equals("48")) {
						paymentCol.add(paymentStr);
					}
				}
			}
			Iterator<String> paymentIte = paymentCol.iterator();
			String pmts = "";
			while (paymentIte.hasNext()) {
				pmts += paymentIte.next() + ",";
			}

			search.setSelectedAllAgents(isSelectedAllAgents);
			
			if(agentCol.size() == 1 && agentCol.get(0).equals("")){
				agentCol = null;
				hasSelectedAgents = false;
			}
			search.setAgents(agentCol);
			search.setPaymentSource(strPaySource);
			search.setReportViewNew(isNewView);
			
			if (!hasSelectedAgents && hasPrivilege(request, "rpt.ta.comp.rpt")
					&& !hasPrivilege(request, "rpt.ta.comp.all")) {
				
				UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
				if (hasPrivilege(request, "rpt.ta.comp.stations.full")
						&& userPrincipal.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					search.setOnlyReportingAgents(false);
				} else {					
					search.setGsaCode(userPrincipal.getAgentCode());
					search.setOnlyReportingAgents(true);				
				}
			}else{
				search.setOnlyReportingAgents(false);
			}
			
			String paymentBreakDown = globalConfig.getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
			String reportInPayCur = globalConfig.getBizParam(SystemParamKeys.REPORTS_IN_AGENTS_CURRENCY);
			String chkBase = request.getParameter("chkBase");

			if (paymentBreakDown.equalsIgnoreCase("Y") && strPaySource.equals("INTERNAL")) {
				showOnlyPaymentBreakDown = "Y";
			}

			if ((chkBase == null) && reportInPayCur.equalsIgnoreCase("Y")) {
				showPaymentCurrency = "Y";
			}
			if (showPaymentCurrency.equalsIgnoreCase("Y") && showOnlyPaymentBreakDown.equalsIgnoreCase("Y")) {
				showCurrencyWithBreakDown = "Y";
				showOnlyPaymentBreakDown = "N";
			}
			if (bolAdditionalInfo) {
				showAdditionalInfo = "Y";
				showCurrencyWithBreakDown = "N";
				showOnlyPaymentBreakDown = "N";
			}

			search.setInbaseCurr(true);
			if (globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY).equalsIgnoreCase("Y")) {
				if (reportInPayCur.equalsIgnoreCase("Y")) {
					templateDetail = "CompPayRptDetailCurr.jasper";
					// templateSummary = "CompPayRptSummByCurr.jasper";
					search.setInbaseCurr(false);
					if (hasPrivilege(request, "rpt.ta.comp.base") && (chkBase != null) && (chkBase.equals("on"))) {
						templateDetail = "CompPayRptDetailBase.jasper";
						// templateSummary = "CompPayRptSummByBase.jasper";
						search.setInbaseCurr(true);
					}
				}
			}


			String offlineReportParams = AppSysParamsUtil.getEnableOfflineReportParams();
			String[] reportParams = offlineReportParams.split(",");
			if (strlive.equals(WebConstants.REP_LIVE) && reportParams[0].equals("Y")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
				Date reportFromDate = DateUtil.parseDate(search.getDateRangeFrom(), "dd-MMM-yyyy");
				int noOfLiveReportDays = Integer.parseInt(reportParams[1]);

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_YEAR, -1 * noOfLiveReportDays);
				Date validReportFromDate = dateFormat.parse(dateFormat.format(cal.getTime()));
				if (validReportFromDate.compareTo(reportFromDate) > 0) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				} else {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				}
			}

			if (reportView.equals("SUMMARY")) {
				search.setReportType(ReportsSearchCriteria.COMPANY_PAYMENT_SUMMARY);
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templateCompanyPaymentSummary));
				search.setPaymentTypes(paymentCol);
			} else {
				search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_DETAILS);
				if (strPaySource != null && strPaySource.equalsIgnoreCase("INTERNAL") && paymentBreakDown.equals("Y")
						&& reportInPayCur.equals("N")) {
					templateDetail = "CompPayRptDetailCurr.jasper";
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(templateDetail));
				} else {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(templateDetail));
				}
				search.setPaymentTypes(paymentCol);

			}

			/*
			 * if(globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY).equalsIgnoreCase("Y") &&
			 * blnDetail && globalConfig.getBizParam(SystemParamKeys.REPORTS_IN_AGENTS_CURRENCY).equalsIgnoreCase("Y")){
			 * search.setCurrencies(currencies); resultSet =
			 * ModuleServiceLocator.getDataExtractionBD().getCompanyPaymentCurrencyData(search); }else { resultSet =
			 * ModuleServiceLocator.getDataExtractionBD().getCompanyPaymentData(search); }
			 */
			// Fix for comp pay issue remove this once the report query is corrected.
			search.setDisplayAgentAdditionalInfo(false);

			if (!"All".equals(selFlightType)) {
				search.setSearchFlightType(selFlightType);
			}
			if (modificationDetails) {
				search.setModificationDetails(modificationDetails);
			}

			search.setEntity(strEntity);
			if (strPaySource != null && strPaySource.equalsIgnoreCase("EXTERNAL")) {
				resultSet = ModuleServiceLocator.getDataExtractionBD().getCompanyPaymentData(search);
			} else if (reportView.equals("DETAIL")) {
				if ((globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY).equalsIgnoreCase("Y") && globalConfig
						.getBizParam(SystemParamKeys.REPORTS_IN_AGENTS_CURRENCY).equalsIgnoreCase("Y"))
						|| (paymentBreakDown.equals("Y") && reportInPayCur.equals("N"))) {
					search.setCurrencies(currencies);
					resultSet = ModuleServiceLocator.getDataExtractionBD().getCompanyPaymentCurrencyData(search);
				} else {
					resultSet = ModuleServiceLocator.getDataExtractionBD().getCompanyPaymentData(search);
				}
			} else {
				resultSet = ModuleServiceLocator.getDataExtractionBD().getCompanyPaymentCurrencyData(search);
			}

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("AGENT_CODE", agents);
			parameters.put("AGENT_NAME", request.getParameter("hdnAgentName"));
			parameters.put("PAYMENT_MODES", payments);
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("SOURCE", strPaySource);
			if (AppSysParamsUtil.displayAmountsInSeperateColumnsWithCategory()) {
				parameters.put("VIEW_IN_SEPERATE_COLUMNS", "Y");
			} else {
				parameters.put("VIEW_IN_SEPERATE_COLUMNS", "N");
			}
			if (AppSysParamsUtil.isShowPaxETKT()) {
				parameters.put("ETICKET", "Y");
			}
			if (isNewView) {
				parameters.put("VIEW", "ON");
			}

			parameters.put("SHOW_BREAKDOWN", showOnlyPaymentBreakDown);
			parameters.put("SHOW_PAY_CURR", showPaymentCurrency);
			parameters.put("SHOW_CUR_BREAKDOWN", showCurrencyWithBreakDown);
			parameters.put("SHOW_ADDITIONAL_INFO", showAdditionalInfo);
			parameters.put("SHOW_GDS_RLOC", AppSysParamsUtil.displayGdsRlocInCompanyPaymentsReport());

			if (chkBase != null) {
				parameters.put("CHK_BASE", "on");
			}
			parameters.put("DISPLAY_ADDITIONAL_PAYMENT_MODE", new Boolean(AppSysParamsUtil.isAllowCapturePayRef()).toString());

			Collection actualPayModes = DatabaseUtil.getActualPayment();
			String showActualPayMode = "N";
			if (actualPayModes != null && actualPayModes.size() > 1 && AppSysParamsUtil.isAllowCapturePayRef()) {
				showActualPayMode = "Y";
			}
			parameters.put("DISPLAY_ACTUAL_PAY_MODE", showActualPayMode);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("SEL_FLIGHT_TYPE", selFlightType);
			parameters.put("SEL_DISCOUNT_TYPE", fareDiscountCode);
			parameters.put("CHK_SALES", isSales ? "Sales" : null);
			parameters.put("CHK_REFUND", isRefunds ? "Refunds" : null);
			parameters.put("CHK_MODIFICATIONS", modificationDetails ? "Modifications" : null);
			parameters.put("CHK_TIME_IN_LOCAL", isReportInLocalTime ? "on" : null);
			parameters.put("TIME_ZONE", isReportInLocalTime ? "LOCAL" : "ZULU");

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);
			
			// To provide Report Translation
			String locale = null;
//			Object localeObject = request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
//			if (localeObject == null){
				locale = WebConstants.DEFAULT_LANGUAGE;
//			}
//			else{
//				locale = localeObject.toString();
//			}
			parameters.put("language", locale);

			String reportName = "CompanyPaymentReport";
			
			parameters.put("ENTITY",strEntityText);
			parameters.put("ENTITY_CODE",strEntity);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (value.trim().equals("PDF")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	public static Date getLastInvoicePeriodStartDate() {

		Calendar calendarDate = new GregorianCalendar();

		int dayOfMonth = calendarDate.get(Calendar.DAY_OF_MONTH);
		int month = calendarDate.get(Calendar.MONTH);
		int year = calendarDate.get(Calendar.YEAR);

		int invoicePeriodDay = 0;

		if (dayOfMonth >= 1 && dayOfMonth < 15) {
			invoicePeriodDay = 1;
		} else if (dayOfMonth >= 15 && dayOfMonth <= CalendarUtil.getLastDayOfYearAndMonth(year, month + 1)) {
			invoicePeriodDay = 15;
		}

		calendarDate.set(Calendar.SECOND, 0);
		calendarDate.set(Calendar.MINUTE, 0);
		calendarDate.set(Calendar.HOUR, 0);

		calendarDate.set(Calendar.DAY_OF_MONTH, invoicePeriodDay);

		return calendarDate.getTime();
	}

	private static void accumulateNominalCodes(ArrayList<String> paymentCol, boolean isSales, boolean isRefunds, String saleNC,
			String refundNC) {
		if (isSales) {
			paymentCol.add(saleNC);
		}
		if (isRefunds) {
			paymentCol.add(refundNC);
		}
	}
	
	private static void setAgentStatusTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentStatusTypes();
		request.setAttribute(WebConstants.REQ_HTML_CMP_PAY_AGENTSTATUS_LIST, strATList);
	}
	
	public static void setStaions(HttpServletRequest request) throws ModuleException {

		String strstnList = "";
		if (hasPrivilege(request, "rpt.ta.comp.all")) {
			strstnList = JavascriptGenerator.createStationsByCountry();
			request.setAttribute(WebConstants.REP_CMP_PAY_STATIONS_ENABLED, true);
			request.setAttribute(WebConstants.REQ_HTML_STN_DATA, strstnList);
		} else if (hasPrivilege(request, "rpt.ta.comp.rpt")) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			strstnList = JavascriptGenerator.createStationsByCountryForComapanyPamentRptGSA(userPrincipal);
			request.setAttribute(WebConstants.REP_CMP_PAY_STATIONS_ENABLED, true);
			request.setAttribute(WebConstants.REQ_HTML_STN_DATA, strstnList);
		} else {
			request.setAttribute(WebConstants.REP_CMP_PAY_STATIONS_ENABLED, false);
		}
	}
	
	private static void setEntities(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_ENTITIES, SelectListGenerator.createEntityListByName());
	}

}