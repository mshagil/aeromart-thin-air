package com.isa.thinair.airadmin.core.web.v2.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Jsp.Fare.FARE_RULE_MANAGE, value = S2Constants.Jsp.Fare.FARE_RULE_MANAGE),
		@Result(name = S2Constants.Jsp.Fare.AGENT_FARE, value = S2Constants.Jsp.Fare.AGENT_FARE),
		@Result(name = S2Constants.Jsp.Fare.SETUP_FARE, value = S2Constants.Jsp.Fare.SETUP_FARE),
		@Result(name = S2Constants.Jsp.Fare.OVERRIDEOHD, value = S2Constants.Jsp.Fare.OVERRIDEOHD),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowLoadFareJspAction extends BaseRequestAwareAction {

	public String manageRule() {
		request.setAttribute("mode", request.getParameter("mode"));
		request.setAttribute("fareReqID", request.getParameter("fareId"));
		request.setAttribute("flexiEnabled", AppSysParamsUtil.isFlexiFareEnabled());
		request.setAttribute("posWebFaresEnabled", AppSysParamsUtil.isPosWebFaresEnabled());
		request.setAttribute("multilingualCommentEnabled", AppSysParamsUtil.isEnableFareRuleComments());
		request.setAttribute("languagesList", JavascriptGenerator.getLanguageOptions());
		return S2Constants.Jsp.Fare.FARE_RULE_MANAGE;
	}

	public String showAgentFareRule() {
		return S2Constants.Jsp.Fare.AGENT_FARE;
	}

	public String showSetupFare() {
		return S2Constants.Jsp.Fare.SETUP_FARE;
	}

	public String GridV2() {
		return AdminStrutsConstants.AdminJSP.FARE_RULE_GRID_DATA_V2;
	}

	public String overRideOHD() {
		request.setAttribute("mode", request.getParameter("rqFrom"));
		request.setAttribute("fareRuleCode", request.getParameter("frc"));
		request.setAttribute("fareRuleID", request.getParameter("frId"));
		request.setAttribute("version", request.getParameter("version"));
		return S2Constants.Jsp.Fare.OVERRIDEOHD;
	}

}
