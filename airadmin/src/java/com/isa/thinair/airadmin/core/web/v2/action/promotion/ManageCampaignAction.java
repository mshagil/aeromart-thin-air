package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.CampaignTO;
import com.isa.thinair.promotion.api.to.CustomerPromoCodeSelectionRequest;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ManageCampaignAction extends BaseRequestAwareAction{
	
	private static Log log = LogFactory.getLog(ManageCampaignAction.class);

	private CustomerPromoCodeSelectionRequest customerPromoSegRequset;
	
	private static final String DATE_FORMAT = "dd/MM/yyyy";
	
	private boolean success = true;
	
	private List<String> emailIDs; 
	
	private CampaignTO campaignTO;
	
	private String errorMessage;

	private static String UI_DATE_FORMAT = "dd/MM/yyyy";

	private String interval;

	private String startDate;
	
	private String endDate;	
	
	private String oNDs;
	
	public String search() {

		try {
			if (customerPromoSegRequset != null) {
				emailIDs = ModuleServiceLocator.getPromotionManagementBD().getCustomerEmailsForPromoCodes(customerPromoSegRequset);
				success = true;
			}

		} catch (Exception e) {
			log.error("Filtering Customers Failed", e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}
	
	public String save(){
		try {		
			
			if (campaignTO != null) {
				
				if (startDate != null && !"".equals(startDate)) {
					SimpleDateFormat format = new SimpleDateFormat(UI_DATE_FORMAT + " HH:mm:ss");
					campaignTO.setStartDate(format.parse(startDate + " 00:00:00"));
					campaignTO.setEndDate(format.parse(endDate + " 23:59:59"));
					campaignTO.setInterval(interval);
					campaignTO.setScheduledCampaign(true);
				}

				ModuleServiceLocator.getPromotionManagementBD().saveCampaignAndEmailPromoCodes(campaignTO);
				success = true;
			}

		} catch (Exception e) {
			log.error("Saving Campaign and Sending Emails Failed", e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	public CustomerPromoCodeSelectionRequest getCustomerPromoSegRequset() {
		return customerPromoSegRequset;
	}

	public void setCustomerPromoSegRequset(CustomerPromoCodeSelectionRequest customerPromoSegRequset) {
		this.customerPromoSegRequset = customerPromoSegRequset;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public void setReservationFromDate(String reservationFromDate) throws ParseException {
		customerPromoSegRequset.setReservationFromDate(new SimpleDateFormat(DATE_FORMAT).parse(reservationFromDate));
	}

	public void setReservationToDate(String reservationToDate) throws ParseException {
		customerPromoSegRequset.setReservationToDate(new SimpleDateFormat(DATE_FORMAT).parse(reservationToDate));
	}

	public void setFlightFromDate(String flightFromDate) throws ParseException {
		customerPromoSegRequset.setFlightFromDate(new SimpleDateFormat(DATE_FORMAT).parse(flightFromDate));
	}

	public void setFlightToDate(String flightToDate) throws ParseException {
		customerPromoSegRequset.setFlightToDate(new SimpleDateFormat(DATE_FORMAT).parse(flightToDate));
	}

	public List<String> getEmailIDs() {
		return emailIDs;
	}

	public void setEmailIDs(List<String> emailIDs) {
		this.emailIDs = emailIDs;
	}

	public String getoNDs() {
		return oNDs;
	}

	public void setoNDs(String oNDs) throws JSONException {
		this.oNDs = oNDs;
		Set<String> ondList = new HashSet<String>((List<String>) JSONUtil.deserialize(oNDs));
		customerPromoSegRequset.setApplicableONDs(ondList);
	}

	public void setONDs(String oNDs) throws JSONException {
		Set<String> ondList = new HashSet<String>((List<String>) JSONUtil.deserialize(oNDs));
		customerPromoSegRequset.setApplicableONDs(ondList);
	}

	public void setLocations(String locations) throws JSONException {
		Set<String> locationList = new HashSet<String>((List<String>) JSONUtil.deserialize(locations));
		customerPromoSegRequset.setLocations(locationList);
	}


	public void setFareTypes(String fareTypes) throws JSONException {
		Set<String> fareList = new HashSet<String>((List<String>) JSONUtil.deserialize(fareTypes));
		Set<String> standardFareList = new HashSet<String>();
		if (!fareList.isEmpty()) {
			for(String fareType : fareList){
				if(fareType.equals("One Way")){
					standardFareList.add("N");
				} else if (fareType.equals("Return")) {
					standardFareList.add("Y");
				}
			}
		}
		customerPromoSegRequset.setFareTypes(standardFareList);
	}

	public CampaignTO getCampaignTO() {
		return campaignTO;
	}

	public void setCampaignTO(CampaignTO campaignTO) {
		this.campaignTO = campaignTO;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
