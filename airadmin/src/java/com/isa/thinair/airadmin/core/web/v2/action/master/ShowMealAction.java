package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.MealRH;
import com.isa.thinair.airmaster.api.dto.MealSearchDTO;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCOS;
import com.isa.thinair.airmaster.api.model.MealForDisplay;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowMealAction {

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;
	private String mealName;
	private String mealDescription;
	private BigDecimal amount;
	private String mealCode;
	private String status;
	private long version;
	private String createdBy;
	private Date createdDate;
	private Integer mealId;
	private Integer mealCategory;
	private String uploadImage;
	private String uploadImageThumbnail;
	private String mealsForDisplay;
	private String mealTitleForDisplay;
	private String hdnMode;
	private MealSearchDTO mealSrch;
	private String selectListData;
	private String iataCode;
	private String cos;
	private String mealCos; // struts2 does not pass disabled elements, hadling it.
	private String chkXBE;
	private String chkIBE;
	private String chkWS;
	private String chkIOS;
	private String chkAndriod;

	public String getMealTitleForDisplay() {
		return mealTitleForDisplay;
	}

	public void setMealTitleForDisplay(String mealTitleForDisplay) {
		this.mealTitleForDisplay = mealTitleForDisplay;
	}

	public String getMealCos() {
		return mealCos;
	}

	public void setMealCos(String mealCos) {
		this.mealCos = mealCos;
	}

	public String getCos() {
		return cos;
	}

	public void setCos(String cos) {
		this.cos = cos;
	}

	public String getSelectListData() {
		return selectListData;
	}

	public void setSelectListData(String selectListData) {
		this.selectListData = selectListData;
	}

	public MealSearchDTO getMealSrch() {
		return mealSrch;
	}

	public void setMealSrch(MealSearchDTO mealSrch) {
		this.mealSrch = mealSrch;
	}

	private static Log log = LogFactory.getLog(ShowMealAction.class);

	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getMealDescription() {
		return mealDescription;
	}

	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getUploadImage() {
		return uploadImage;
	}

	public void setUploadImage(String uploadImage) {
		this.uploadImage = uploadImage;
	}

	public String getMealsForDisplay() {
		return mealName;
	}

	public void setMealsForDisplay(String mealsForDisplay) {
		this.mealsForDisplay = mealsForDisplay;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	/**
	 * @return the iataCode
	 */
	public String getIataCode() {
		return iataCode;
	}

	/**
	 * @param iataCode
	 *            the iataCode to set
	 */
	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public void setUploadImageThumbnail(String uploadImageThumbnail) {
		this.uploadImageThumbnail = uploadImageThumbnail;
	}

	public void setMealCategory(Integer mealCategory) {
		this.mealCategory = mealCategory;
	}

	public String getChkXBE() {
		return chkXBE;
	}

	public void setChkXBE(String chkXBE) {
		this.chkXBE = chkXBE;
	}

	public String getChkIBE() {
		return chkIBE;
	}

	public void setChkIBE(String chkIBE) {
		this.chkIBE = chkIBE;
	}

	public String getChkWS() {
		return chkWS;
	}

	public void setChkWS(String chkWS) {
		this.chkWS = chkWS;
	}

	public String getChkIOS() {
		return chkIOS;
	}

	public void setChkIOS(String chkIOS) {
		this.chkIOS = chkIOS;
	}

	public String getChkAndriod() {
		return chkAndriod;
	}

	public void setChkAndriod(String chkAndriod) {
		this.chkAndriod = chkAndriod;
	}

	public String execute() {

		try {

			String strMealCodes = SelectListGenerator.createMealCode();
			setSelectListData(strMealCodes);

			Meal savedMeal = null;
			Meal meal = new Meal();
			meal.setMealName(mealName.trim());
			meal.setMealDescription(mealDescription.trim());
			meal.setMealCode(mealCode.trim());
			meal.setIataCode(iataCode.trim());
			if (status != null) {
				meal.setStatus(status);
			} else {
				meal.setStatus(Meal.STATUS_INACTIVE);
			}
			meal.setVersion(version);
			meal.setCreatedBy(createdBy);
			meal.setCreatedDate(createdDate);
			meal.setMealId(mealId);
			meal.setMealCategoryID(mealCategory);
			
			Set<String> salesChannel = new HashSet<String>();
			if(chkXBE != null){
				salesChannel.add(chkXBE);
			}
			if(chkIBE != null){
				salesChannel.add(chkIBE);
			}
			if(chkWS != null){
				salesChannel.add(chkWS);
			}
			if(chkAndriod != null){
				salesChannel.add(chkAndriod);
			}
			if(chkIOS != null){
				salesChannel.add(chkIOS);
			}
			meal.setChannels(salesChannel);

			String[] cosList = mealCos.split(Constants.COMMA_SEPARATOR);
			Set<MealCOS> mealCOSList = new HashSet<MealCOS>();
			List<String[]> attachedMealList = null;
			if (this.hdnMode != null && (hdnMode.equals(WebConstants.ACTION_EDIT))) {
				try {
					attachedMealList = SelectListGenerator.getMealAttachedToMT(mealId);
				} catch (ModuleException e) {
					log.error(e + " Retrieving Attached Meal Details Failed.");
				}
			}

			for (String element : cosList) {
				MealCOS mealCOS = new MealCOS();
				mealCOS.setMeal(meal);
				String[] splittedCabinClassAndLogicalCabinClass = SplitUtil.getSplittedCabinClassAndLogicalCabinClass(element);
				mealCOS.setCabinClassCode(splittedCabinClassAndLogicalCabinClass[0]);
				mealCOS.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
				mealCOSList.add(mealCOS);
			}

			if (attachedMealList != null) {
				for (String[] attachedMeal : attachedMealList) {
					MealCOS mealCOS = new MealCOS();
					mealCOS.setMeal(meal);
					mealCOS.setCabinClassCode(attachedMeal[3]);
					mealCOS.setLogicalCCCode(attachedMeal[2]);
					if (!checkCOSAlreadyExist(mealCOSList, mealCOS)) {
						mealCOSList.add(mealCOS);
					}
				}
			}
			meal.setMealCOS(mealCOSList);

			if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
				try {
					MealRH.saveMeal(meal);
					savedMeal = MealRH.getMeal(mealName.trim());
					if (this.uploadImage != null && this.uploadImage.trim().equalsIgnoreCase("UPLOADED")) {
						AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
						String mountPath = adminCon.getMealImages();
						String fullFileName = mountPath + "mealImage_test.jpg";
						File tmpFile = new File(fullFileName);
						String imageFile = mountPath + "meal_" + savedMeal.getMealId() + ".jpg"; // no need to add the
																									// suffix
						File dataFile = new File(imageFile);
						FileUtils.copyFile(tmpFile, dataFile);
					}
					if (this.uploadImageThumbnail != null && this.uploadImageThumbnail.trim().equalsIgnoreCase("UPLOADED")) {
						AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
						String mountPath = adminCon.getMealImages();
						String fullFileName = mountPath + "mealThumbnailImage_test.jpg";
						File tmpFile = new File(fullFileName);
						String imageFile = mountPath + "meal_Thumbnail_" + savedMeal.getMealId() + ".jpg"; // no need to
																											// add
																											// the
																											// suffix
						File dataFile = new File(imageFile);
						FileUtils.copyFile(tmpFile, dataFile);
					}
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;

				} catch (ModuleException e) {
					this.succesMsg = e.getMessageString();
					if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
						this.succesMsg = airadminConfig.getMessage("um.meal.form.mealcode.dulply");
					}
					this.msgType = WebConstants.MSG_ERROR;
				} catch (Exception ex) {
					this.succesMsg = "System Error Please Contact Administrator";
					this.msgType = WebConstants.MSG_ERROR;
					if (log.isErrorEnabled()) {
						log.error(ex);
					}
				}
			} else {
				savedMeal = meal;
			}

			try {
				List<MealForDisplay> mealsFD = new ArrayList<MealForDisplay>();
				if (mealsForDisplay != null && !mealsForDisplay.equals("")) {
					String[] arrMealTrl = mealsForDisplay.split("~");
					String[] mealTrl = null;
					for (int i = 0; i < arrMealTrl.length; i++) {
						if (arrMealTrl[i] != null && !arrMealTrl[i].equals("")) {
							mealTrl = arrMealTrl[i].split(Constants.COMMA_SEPARATOR);
							MealForDisplay mfd = new MealForDisplay();
							mfd.setLanguageCode(mealTrl[0]);
							mfd.setMealDescriptionOl(mealTrl[1]);
							mfd.setMealId(savedMeal.getMealId());
							mfd.setId(new Integer(mealTrl[3]));
							mfd.setVersion(new Long(mealTrl[4]));
							/* Jira 4640 */
							mfd.setMealTitleOl(mealTrl[5]);
							mealsFD.add(mfd);
						}
					}

				}
				ModuleServiceLocator.getTranslationBD().saveOrUpdateMeals(mealsFD);
				this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;

			} catch (ModuleException e) {
				if (this.hdnMode != null
						&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
					this.succesMsg = this.succesMsg + ". Saving Description in Other languages failed.";
				} else {
					this.succesMsg = e.getMessageString();
					if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
						this.succesMsg = airadminConfig.getMessage("um.meal.form.mealcode.dulply");
					}
					this.msgType = WebConstants.MSG_ERROR;
				}

			} catch (Exception ex) {
				if (this.hdnMode != null
						&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
					this.succesMsg = this.succesMsg + ". Saving Description in Other languages failed.";
				} else {
					this.succesMsg = "System Error Please Contact Administrator";
					this.msgType = WebConstants.MSG_ERROR;
				}
				if (log.isErrorEnabled()) {
					log.error(ex);
				}

			}

		} catch (Exception e) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in saving meal", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private boolean checkCOSAlreadyExist(Set<MealCOS> mealCOSList, MealCOS mealCOS) {
		for (MealCOS tmpMealCOS : mealCOSList) {
			if (mealCOS.getCabinClassCode() != null && tmpMealCOS.getCabinClassCode() != null
					&& mealCOS.getCabinClassCode().equals(tmpMealCOS.getCabinClassCode())) {
				return true;
			} else if (mealCOS.getLogicalCCCode() != null && tmpMealCOS.getLogicalCCCode() != null
					&& mealCOS.getLogicalCCCode().equals(tmpMealCOS.getLogicalCCCode())) {
				return true;
			}
		}
		return false;
	}

	public String deleteMeal() {

		try {
			ModuleServiceLocator.getTranslationBD().removeMeals(mealId);
			MealRH.deleteMeal(mealId);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			log.error("Error in deleting meal", e);
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception e) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in deleting meal", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String searchMeal() {

		try {

			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			if (mealSrch != null) {
				if (mealSrch.getMealCode() != null && !mealSrch.getMealCode().trim().equals("")) {
					ModuleCriteria criteriaMLCode = new ModuleCriteria();
					List<Integer> lstmealCode = new ArrayList<Integer>();
					criteriaMLCode.setFieldName("meal.mealId");
					criteriaMLCode.setCondition(ModuleCriteria.CONDITION_EQUALS);
					lstmealCode.add(Integer.parseInt(mealSrch.getMealCode()));
					criteriaMLCode.setValue(lstmealCode);
					critrian.add(criteriaMLCode);
				}
				if (mealSrch.getMealName() != null && !mealSrch.getMealName().trim().equals("")) {
					ModuleCriteria criteriaMLName = new ModuleCriteria();
					List<String> lstmealName = new ArrayList<String>();
					criteriaMLName.setFieldName("lower(meal.mealName)");
					criteriaMLName.setCondition(ModuleCriteria.CONDITION_LIKE);
					lstmealName.add("%" + mealSrch.getMealName().toLowerCase() + "%");
					criteriaMLName.setValue(lstmealName);
					critrian.add(criteriaMLName);
				}
				if (mealSrch.getStatus() != null && !mealSrch.getStatus().trim().equals("")) {
					if (mealSrch.getStatus().equals("ALL")) {
						mealSrch.setStatus("%");
					}
					ModuleCriteria criteriaMLStatus = new ModuleCriteria();
					List<String> lstmealStatus = new ArrayList<String>();
					criteriaMLStatus.setFieldName("meal.status");
					criteriaMLStatus.setCondition(ModuleCriteria.CONDITION_LIKE);
					lstmealStatus.add(mealSrch.getStatus().toUpperCase());
					criteriaMLStatus.setValue(lstmealStatus);
					critrian.add(criteriaMLStatus);
				}
				if (mealSrch.getClassOfService() != null && !mealSrch.getClassOfService().trim().equals("")) {
					String classOfService = mealSrch.getClassOfService();
					ModuleCriteria criteriaMLCC = new ModuleCriteria();
					List<String> lstmealStatus = new ArrayList<String>();
					String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
							.getSplittedCabinClassAndLogicalCabinClass(classOfService);
					if (splittedCabinClassAndLogicalCabinClass[0] != null) {
						criteriaMLCC.setFieldName("mealCOS.cabinClassCode");
						lstmealStatus.add(splittedCabinClassAndLogicalCabinClass[0]);
					} else {
						criteriaMLCC.setFieldName("mealCOS.logicalCCCode");
						lstmealStatus.add(splittedCabinClassAndLogicalCabinClass[1]);
					}
					criteriaMLCC.setCondition(ModuleCriteria.CONDITION_EQUALS);
					criteriaMLCC.setValue(lstmealStatus);
					critrian.add(criteriaMLCC);
				}
				if (mealSrch.getIataCode() != null && !mealSrch.getIataCode().trim().equals("")) {
					ModuleCriteria criteriaMLIataCode = new ModuleCriteria();
					List<String> lstmealIataCode = new ArrayList<String>();
					criteriaMLIataCode.setFieldName("meal.iataCode");
					criteriaMLIataCode.setCondition(ModuleCriteria.CONDITION_EQUALS);
					lstmealIataCode.add(mealSrch.getIataCode());
					criteriaMLIataCode.setValue(lstmealIataCode);
					critrian.add(criteriaMLIataCode);
				}
			}

			Page<Meal> pgmeal = MealRH.searchMeal(this.page, critrian);
			this.records = pgmeal.getTotalNoOfRecords();
			this.total = pgmeal.getTotalNoOfRecords() / 20;
			int mod = pgmeal.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page <= 0) {
				this.page = 1;
			}

			Collection<Meal> colMeal = pgmeal.getPageData();
			String strMeals = null;
			List<MealForDisplay> mealForDisplayList = null;
			List<String[]> mealsWithMealTem = null;
			try {
				mealForDisplayList = ModuleServiceLocator.getTranslationBD().getMeals();
			} catch (ModuleException e) {
				this.succesMsg = e.getMessageString();
				this.msgType = WebConstants.MSG_ERROR;
			}

			if (colMeal != null) {
				Object[] dataRow = new Object[colMeal.size()];
				int index = 1;
				Iterator<Meal> iter = colMeal.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					Meal grdMeal = iter.next();

					strMeals = formMealDisplayString(mealForDisplayList, grdMeal);
					String attachedToMT = "N";
					String suffix = "^";
					List<String> attachedCOSList = new ArrayList<String>();
					try {
						mealsWithMealTem = SelectListGenerator.getMealAttachedToMT(grdMeal.getMealId());
					} catch (ModuleException e) {
						this.succesMsg = e.getMessageString();
						this.msgType = WebConstants.MSG_ERROR;
					}
					if (mealsWithMealTem.size() > 0) {
						Iterator<String[]> mealChg = mealsWithMealTem.iterator();
						while (mealChg.hasNext()) {
							String s[] = mealChg.next();
							if (!s[1].equals("")) {
								attachedToMT = "Y";
							}

							if (!s[2].equals("")) {
								attachedCOSList.add(s[2] + suffix);
							} else {
								if (!attachedCOSList.contains(s[3])) {
									attachedCOSList.add(s[3]);
								}
							}
						}
					}
					Map<String, LogicalCabinClassDTO> lccMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
					ArrayList<String> ccList = new ArrayList<String>();
					ArrayList<String> lccList = new ArrayList<String>();
					for (MealCOS mealCOSList : grdMeal.getMealCOS()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							if (mealCOSList.getLogicalCCCode() != null) {
								lccList.add(mealCOSList.getLogicalCCCode() + Constants.CARET_SEPERATOR);
							}

							if (mealCOSList.getCabinClassCode() != null) {
								ccList.add(mealCOSList.getCabinClassCode());
							}
						} else {

							if (mealCOSList.getCabinClassCode() != null) {
								ccList.add(mealCOSList.getCabinClassCode());
							} else {
								ccList.add((lccMap.get(mealCOSList.getLogicalCCCode())).getCabinClassCode());
							}
						}
					}
					
					//Adding Sales Channels
					ArrayList<String> channelList = new ArrayList<String>();
					for(String strChannel :grdMeal.getChannels()){
						if(strChannel != null){
							channelList.add(strChannel);							
						}						
					}
				

					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("meal", grdMeal);
					counmap.put("ccList", ccList);
					counmap.put("lccList", lccList);
					counmap.put("mealForDisplay", strMeals);
					counmap.put("hasMealTemp", attachedToMT);
					counmap.put("attachedCOS", attachedCOSList);
					counmap.put("channels", channelList);
					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;
			}
		} catch (Exception e) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in searchMeal", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private String formMealDisplayString(Collection<MealForDisplay> colMealForDisplay, Meal meal) {
		Collection<MealForDisplay> chCol = new HashSet<MealForDisplay>();
		StringBuilder sb = new StringBuilder();
		if (colMealForDisplay != null && meal != null) {
			for (MealForDisplay mlForDisplay : colMealForDisplay) {
				if (meal.getMealId().equals(mlForDisplay.getMealId())) {
					chCol.add(mlForDisplay);
				}
			}
		}
		String ucs = "";

		if (chCol != null) {
			for (MealForDisplay mlfd : chCol) {
				ucs = StringUtil.getUnicode(mlfd.getMealDescriptionOl());
				ucs = ucs.replace(",", "^");
				sb.append(mlfd.getLanguageCode());
				sb.append(",");
				sb.append(ucs);
				sb.append(",");
				sb.append(mlfd.getMealId());
				sb.append(",");
				sb.append(mlfd.getId());
				sb.append(",");
				sb.append(mlfd.getVersion());
				/* For Jira 4640 */
				sb.append(",");
				ucs = "";
				ucs = StringUtil.getUnicode(mlfd.getMealTitleOl());
				ucs = ucs.replace(",", "^");
				sb.append(ucs);
				sb.append("~");

			}
		}
		return sb.toString();
	}

}
