package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPromotionTemplatesManagementAction {

	private static Log log = LogFactory.getLog(LoadPromotionTemplatesManagementAction.class);

	// out
	private List<String[]> airportsList;
	private List<String[]> onds;

	// in
	private String origin;
	private String destination;

	public String execute() {
		try {
			airportsList = SelectListGenerator.createActiveAirportCodes();
		} catch (Exception e) {
			log.error("execute ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String availableRoutes() {

		try {
			Collection<RouteInfo> routes = ModuleServiceLocator.getCommonServiceBD().getRouteInfo(origin, destination);
			onds = new ArrayList<String[]>();

			if (routes != null) {
				for (RouteInfo routeInfo : routes) {
					onds.add(new String[] { routeInfo.getRouteCode() });
				}
			}
		} catch (Exception e) {
			log.error("availableRoutes ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public List<String[]> getAirportsList() {
		return airportsList;
	}

	public List<String[]> getOnds() {
		return onds;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
