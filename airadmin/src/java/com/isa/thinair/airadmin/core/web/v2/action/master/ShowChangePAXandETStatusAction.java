package com.isa.thinair.airadmin.core.web.v2.action.master;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airschedules.api.dto.FlightBasicInfoDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 
 * @author Bimsara V
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
,@Result(name = S2Constants.Result.ERROR, type = JSONResult.class, value = "")
})
public class ShowChangePAXandETStatusAction {

	private static final Log log = LogFactory.getLog(ShowChangePAXandETStatusAction.class);

	private static final int PAGE_LENGTH = 10;

	private int page;

	private String msgType;

	private String messageTxt;
	
	private boolean success = true;

	private FlightBasicInfoDTO flightsSearchReqDTO;

	private FlightBasicInfoDTO flightsResultsResDTO;

	private Collection<Map<String, Object>> colFlownFlightDetails;

	private int total;

	private int records;

	private String hdnMode;

	private String flightNumber;

	private String deptDate;

	private static AiradminConfig airadminConfig = new AiradminConfig();
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH.mm.ss");
	
	private Date currentDate = new Date();
	
	String forward = S2Constants.Result.SUCCESS;

	public String execute() {
		try {

			if (flightsSearchReqDTO != null && !flightsSearchReqDTO.getFlightNumber().isEmpty()) {

				int startIndex = 0;
				if (page > 1) {
					startIndex = (page - 1) * PAGE_LENGTH;
				}

				Collection<FlightBasicInfoDTO> flightsDetails = ModuleServiceLocator.getFlightServiceBD().getFlightDetails(
						flightsSearchReqDTO, startIndex, PAGE_LENGTH);
				Collection<FlightBasicInfoDTO> flownFlightsDetails = new ArrayList<FlightBasicInfoDTO>();

				if (flightsDetails.size() > 0) {
					Iterator iter = flightsDetails.iterator();
					boolean haveFlight = false;
					while (iter.hasNext()) {
						try {
							FlightBasicInfoDTO flightBasicInfo = (FlightBasicInfoDTO) iter.next();
							if (flightBasicInfo.getFlightNumber().equals(flightsSearchReqDTO.getFlightNumber())) {
								haveFlight = true;
								if (!dateFormat.parse(flightBasicInfo.getDepartureDate()).before(currentDate)) {
									throw new ModuleException("um.ChangePAXandETStatus.search.futureDate");
								} else {
									flightBasicInfo.setDepartureDate(CalendarUtil.getDateInFormattedString("dd/MM/yyyy",dateFormat.parse(flightBasicInfo.getDepartureDate())));
									flownFlightsDetails.add(flightBasicInfo);
								}
							}
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (!haveFlight) {
						log.info("um.ChangePAXandETStatus.search.invalidflight" + flightsSearchReqDTO.getFlightNumber());
						throw new ModuleException("um.ChangePAXandETStatus.search.invalidflight");
					}
				}

				Page pagedReocrds = new Page(1, startIndex, startIndex + PAGE_LENGTH, flownFlightsDetails);

				this.colFlownFlightDetails = ((Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(
						pagedReocrds.getStartPosition(), pagedReocrds.getPageData(), decorateRow()));

				this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
				this.setTotal(getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords()));
				this.setRecords(pagedReocrds.getTotalNoOfRecords());
			}
			if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_UPDATE)))) {
				PastFlightInfoDTO pastFlightInfoDTO = new PastFlightInfoDTO();
				pastFlightInfoDTO.setDepartureDate(getDeptDate());
				pastFlightInfoDTO.setFlightNumber(getFlightNumber());
				Map<String, String[]> contentMap = FlightUtil.createUpdatedDataMapForAudit(pastFlightInfoDTO);
				pastFlightInfoDTO.setContentMap(contentMap);
				pastFlightInfoDTO.setHdnMode(hdnMode);
				ModuleServiceLocator.getFlightServiceBD()
						.updateFlownReservationsDetails(pastFlightInfoDTO);


				this.setMessageTxt(airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS));
				this.setMsgType(WebConstants.MSG_SUCCESS);

			}

		} catch (ModuleException me) {
			this.setMsgType(WebConstants.MSG_ERROR);
			this.setMessageTxt(airadminConfig.getMessage(me.getExceptionCode()));
			this.setSuccess(false);
			if (log.isErrorEnabled()) {
				log.error(airadminConfig.getMessage(me.getExceptionCode()));
			}
		} catch (Exception e) {
			this.setMsgType(WebConstants.MSG_ERROR);
			this.setMessageTxt(e.getMessage());
			if (log.isErrorEnabled()) {
				log.error(e.getMessage());
			}
		}
		return forward;
	}

	private RowDecorator<FlightBasicInfoDTO> decorateRow() {
		RowDecorator<FlightBasicInfoDTO> decorator = new RowDecorator<FlightBasicInfoDTO>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, FlightBasicInfoDTO record) {

				try {
					row.put("flightsResultsResDTO.flightNumber", record.getFlightNumber());
					row.put("flightsResultsResDTO.departureDate", record.getDepartureDate());
					row.put("flightsResultsResDTO.segment", record.getSegment());

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		return decorator;
	}

	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Collection<Map<String, Object>> getColFlownFlightDetails() {
		return colFlownFlightDetails;
	}

	public void setColFlownFlightDetails(Collection<Map<String, Object>> colFlownFlightDetails) {
		this.colFlownFlightDetails = colFlownFlightDetails;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public FlightBasicInfoDTO getFlightsResultsResDTO() {
		return flightsResultsResDTO;
	}

	public void setFlightsResultsResDTO(FlightBasicInfoDTO flightsResultsResDTO) {
		this.flightsResultsResDTO = flightsResultsResDTO;
	}

	public FlightBasicInfoDTO getFlightsSearchReqDTO() {
		return flightsSearchReqDTO;
	}

	public void setFlightsSearchReqDTO(FlightBasicInfoDTO flightsSearchReqDTO) {
		this.flightsSearchReqDTO = flightsSearchReqDTO;
	}

	public String getDeptDate() {
		return deptDate;
	}

	public void setDeptDate(String deptDate) {
		this.deptDate = deptDate;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
