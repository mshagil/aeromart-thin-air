package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;

public class FareTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int fareId;

	private int fareRuleId;

	private double fareAmount;

	private String effectiveFromDate;

	private String effectiveToDate;

	private String fareBasisCode;

	private String fareRuleDescription;

	private String paxLevelRefundability;

	private String returnFlag;

	private String advBookingTime;

	private String fareRuleCode;

	private String agentCodes;

	private String visibleChannelNames;

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public double getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	public String getEffectiveFromDate() {
		return effectiveFromDate;
	}

	public void setEffectiveFromDate(String effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public String getEffectiveToDate() {
		return effectiveToDate;
	}

	public void setEffectiveToDate(String effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getFareRuleDescription() {
		return fareRuleDescription;
	}

	public void setFareRuleDescription(String fareRuleDescription) {
		this.fareRuleDescription = fareRuleDescription;
	}

	public String getPaxLevelRefundability() {
		return paxLevelRefundability;
	}

	public void setPaxLevelRefundability(String paxLevelRefundability) {
		this.paxLevelRefundability = paxLevelRefundability;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getAdvBookingTime() {
		return advBookingTime;
	}

	public void setAdvBookingTime(String advBookingTime) {
		this.advBookingTime = advBookingTime;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getAgentCodes() {
		return agentCodes;
	}

	public void setAgentCodes(String agentCodes) {
		this.agentCodes = agentCodes;
	}

	public String getVisibleChannelNames() {
		return visibleChannelNames;
	}

	public void setVisibleChannelNames(String visibleChannelNames) {
		this.visibleChannelNames = visibleChannelNames;
	}
}
