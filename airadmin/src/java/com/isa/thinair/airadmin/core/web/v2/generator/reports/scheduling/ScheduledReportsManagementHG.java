package com.isa.thinair.airadmin.core.web.v2.generator.reports.scheduling;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ScheduledReportsManagementHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for Meal Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.meal.form.name.required", "mealNameRqrd");
			moduleErrs.setProperty("um.meal.form.description.required", "descriptionRqrd");
			moduleErrs.setProperty("um.meal.form.amount.required", "amountRqrd");
			moduleErrs.setProperty("um.meal.form.mealcode.required", "mealCodeRqrd");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	public static String getReportNamesFromReportMetaData(HttpServletRequest request) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		sb.append(SelectListGenerator.getScheduledReportTypes());
		return sb.toString();
	}
}
