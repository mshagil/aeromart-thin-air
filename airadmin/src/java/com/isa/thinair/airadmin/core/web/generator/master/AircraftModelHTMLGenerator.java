package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * @author Shakir
 */
public class AircraftModelHTMLGenerator {

	private static String clientErrors;

	private static Log log = LogFactory.getLog(AircraftModelHTMLGenerator.class);

	/**
	 * Gets the AirCraftModel Data Grid Array
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the String containing the Aircraft Model Data Grid
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getAircraftModelRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("***********inside AircraftHTMLGenerator.getAircraftModelRowHtml()");

		int RecNo;
		Collection<AircraftModel> colData = null;
		StringBuffer sb = null;
		AircraftModel AM = null;
		AircraftCabinCapacity ACC = null;
		if (request.getParameter("hdnRecNo") == null) {
			RecNo = 1;
		} else {
			RecNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		try {
			sb = new StringBuffer("var arrData = new Array();");
			sb.append("var arrCOSData = new Array();");
			Page page = null;
			page = ModuleServiceLocator.getAircraftServiceBD().getAircraftModels((RecNo - 1), 20); // Get

			String strJavascriptTotalNoOfRecs = "";
			if (RecNo > page.getTotalNoOfRecords()) {
				page = ModuleServiceLocator.getAircraftServiceBD().getAircraftModels(0, 20); // Get
				strJavascriptTotalNoOfRecs += "top[0].intLastRec = 1; ";
			}

			int totRec = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs += "var totalRecords = " + totRec + ";";

			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

			colData = page.getPageData();

			Set<String> cabinClassesSet = CommonsServices.getGlobalConfig().getActiveCabinClassesMap().keySet();
			String temp1 = "";
			for (String cabinClass : cabinClassesSet) {
				if (temp1.equals("")) {
					temp1 = cabinClass;
				} else {
					temp1 = temp1 + "," + cabinClass;
				}
			}
			String[] cabinClasses = temp1.split(",");

			int count = 0;
			Iterator<AircraftModel> iter = colData.iterator();
			Iterator<AircraftCabinCapacity> iterIn = null;
			Collection<AircraftCabinCapacity> col1 = null;

			while (iter.hasNext()) {
				AM = iter.next();
				if (AM.getAircraftCabinCapacitys() != null) {
					col1 = AM.getAircraftCabinCapacitys();
				}

				if (col1 != null) { // Check whether the returned model has been
					// set Cabin Capacities

					sb.append("arrData[" + count + "] = new Array();");
					sb.append("arrData[" + count + "][1] = '" + AM.getModelNumber() + "';");

					if (AM.getDescription() != null) {
						sb.append("arrData[" + count + "][2] = '" + AM.getDescription() + "';");
					} else {
						sb.append("arrData[" + count + "][2] = '';");
					}

					sb.append("arrData[" + count + "][3] = '" + AiradminUtils.getModifiedStatus(AM.getStatus()) + "';");
					sb.append("arrData[" + count + "][4] = '" + AM.getVersion() + "';");

					iterIn = col1.iterator();
					String strCOSArr = "";

					for (int countCC = 0; countCC < cabinClasses.length; countCC++) {
						sb.append("arrData[" + count + "][" + (countCC + 6) + "] = '-';");
					}
					int cosRowCount = 0;
					while (iterIn.hasNext()) {
						ACC = iterIn.next();
						for (int countCC = 0; countCC < cabinClasses.length; countCC++) {
							if (ACC.getCabinClassCode().equals(cabinClasses[countCC])) { // Set Cabin
								// Capacities
								// ***
								String cabinClassDesc = CommonsServices.getGlobalConfig().getActiveCabinClassesMap()
										.get(ACC.getCabinClassCode());
								sb.append("arrData[" + count + "][" + (countCC + 6) + "] = '" + ACC.getSeatCapacity() + "/"
										+ ACC.getInfantCapacity() + "';");

								strCOSArr += ACC.getSeatCapacity() + "/" + ACC.getInfantCapacity() + "/"
										+ ACC.getCabinClassCode() + "/" + cabinClassDesc + "/" + ACC.getAcccId() + "/"
										+ ACC.getVersion() + "/" + "E/" + cosRowCount + "/:";
								cosRowCount++;
							}
						}
					}
					if (!strCOSArr.equals("")) {
						strCOSArr = "arrData[" + count + "][5] = '" + strCOSArr + "';";
					} else {
						strCOSArr = "arrData[" + count + "][5] = '';";
					}
					sb.append(strCOSArr);

					// if(AM.getIataAircraftType() != null){
					// sb.append("arrData[" + count + "][6] = '" + AM.getAircraftTypeCode() + "';");
					// }

					// Haider 16Sep08 add the new two field to the end of the arrData
					// sb.append("arrData[" + count +
					// "]["+(cabinClasses.length+6)+"] = '"+AiradminUtils.getNotNullString(AM.getMasterModelCode())+"';");
					sb.append("arrData[" + count + "][" + (cabinClasses.length + 6) + "] = '"
							+ AiradminUtils.getNotNullString(AM.getIataAircraftType()) + "';");

					if (AM.getSsrTemplateId() != null) {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 7) + "] = '" + AM.getSsrTemplateId() + "';");
					} else {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 7) + "] = '-1';");
					}

					if (AM.getItineraryDesc() != null) {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 8) + "] = '" + AM.getItineraryDesc() + "';");
					} else {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 8) + "] = '';");
					}

					if (AM.getAircraftTypeCode() != null) {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 9) + "] = '" + AM.getAircraftTypeCode()
								+ "';");
					}
					
					if ("Y".equalsIgnoreCase(AM.getDefaultIataAircraft())) {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 10) + "] = 'Y';");
					} else {
						sb.append("arrData[" + count + "][" + (cabinClasses.length + 10) + "] = 'N';");
					}

					count++;
				}

			}
			// Create Cabin Class string if 1 is active
			String theActiveCOS = "";
			Map<String, String> cabinClassesMap = CommonsServices.getGlobalConfig().getActiveCabinClassesMap();
			for (String cabinClass : cabinClassesMap.keySet()) {
				if (theActiveCOS.equals("")) {
					theActiveCOS = " var theActiveCOS = '0/0/" + cabinClass + "/" + cabinClassesMap.get(cabinClass) + "///"
							+ "E/0/";
				} else {
					theActiveCOS += ":0/0/" + cabinClass + "/" + cabinClassesMap.get(cabinClass) + "///" + "E/0/";
				}

			}
			if (theActiveCOS.equals("")) {
				theActiveCOS = " var theActiveCOS = ''; ";
			} else {
				theActiveCOS += "';";
			}
			sb.append(theActiveCOS);

		} catch (ModuleException me) {
			log.error("Exception in AircraftModelHTMLGenerator:getAircraftModelRowHtml() [origin module=" + me.getModuleDesc()
					+ "]", me);
		}
		return sb.toString();
	}

	/**
	 * Creates the Client Validations for the Aircraft Model
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the array containg the Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.agent.form.select.aircraft.toedit", "SelectAircraftToEdit");
			moduleErrs.setProperty("um.agent.form.select.aircraft.todelete", "SelectAircraftToDelete");
			moduleErrs.setProperty("um.agent.form.model.number.empty", "ModelNumberEmpty");
			moduleErrs.setProperty("um.agent.form.description.empty", "DescriptionFieldEmpty");
			moduleErrs.setProperty("um.agent.form.adult.CC.empty", "AdultCCFieldEmpty");
			moduleErrs.setProperty("um.agent.form.code.defined", "CodeDefined");
			moduleErrs.setProperty("um.agent.form.model.number.invalid", "ModelNumberInvalid");
			moduleErrs.setProperty("um.agent.form.description.invalid", "DescriptionFieldInvalid");
			moduleErrs.setProperty("um.agent.form.infant.CC.invalid", "InfantCCFieldInvalid");
			moduleErrs.setProperty("um.agent.form.adult.CC.invalid", "AdultCCFieldInvalid");
			moduleErrs.setProperty("um.agent.form.adult.CC.cannotzero", "AdultCCFieldCannotZero");
			moduleErrs.setProperty("um.agent.form.adult.CC.CannotLessThanInfants", "AdultCCFieldLessThanInfants");
			moduleErrs.setProperty("um.uiadmn020.COS.Exists", "COSAlreadyEntered");
			moduleErrs.setProperty("um.uiadmn020.COS.null", "COSMustBeEntered");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.agent.form.itin.desc.empty", "itinDescEmpty");
			moduleErrs.setProperty("um.agent.form.itin.desc.invalid", "itinDescInvalid");
			moduleErrs.setProperty("um.agent.form.iata.type.empty", "IATATypeEmpty");
			

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

}
