package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.inventory.MISearchHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author cBandara
 * 
 */

public final class MISearchRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(MISearchRequestHandler.class);
	private static final String PARAM_MODE = "hdnMode";

	/**
	 * Main Execute Method for Flight Seach in Inventory(Seat) Allocation
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
		} catch (Exception exp) {
			log.error("MISearchRequestHandler.execute() method is failed :" + exp);
			saveMessage(request, exp.toString(), WebConstants.MSG_ERROR);
		}
		return forward;
	}

	/**
	 * Sets the Display Data for the Seach Page of Seat Allocation
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		String strSearchMode = request.getParameter(PARAM_MODE);

		if (strSearchMode != null && strSearchMode.equals(WebConstants.ACTION_SEARCH)) {
			setSeatInventoryRowHtml(request);
		} else {
			String strJavascriptTotalNoOfRecs = "var totalRecords = 0;";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			String strMyArr = "var f = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strMyArr);
		}
		setAirportList(request);
		setCabinClassList(request);
		setFlightStatusList(request);
		setClientErrors(request);
		setUserCarriers(request);
		setTimeZoneModeHtml(request);
	}

	/**
	 * Default time zone mode
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setTimeZoneModeHtml(HttpServletRequest request) throws ModuleException {
		if (AppSysParamsUtil.isDefaultTimeZulu()) {
			request.setAttribute(WebConstants.REQ_HTML_TIMEIN, "var isTimeInZulu=true;");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_TIMEIN, "var isTimeInZulu=false;");
		}
	}

	/**
	 * Sets the Airport list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("MISearchRequestHandler.setAirportList() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Flight Status List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setFlightStatusList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createFlightStatus();
			request.setAttribute(WebConstants.REQ_HTML_FLIGHTSTATUS_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("MISearchRequestHandler.setFlightStatusList() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Cabin Class List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCabinClassList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createAllCabinClass();
			request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("MISearchRequestHandler.setCabinClassList() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations for search page Seat Inventory
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = MISearchHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("MISearchRequestHandler.setClientErrors() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Flight Grid for the Seach Page Seat Inventory
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setSeatInventoryRowHtml(HttpServletRequest request) {
		try {
			MISearchHTMLGenerator htmlGen = new MISearchHTMLGenerator();
			String strHtml = htmlGen.getSeatInventoryRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
		} catch (ModuleException moduleException) {
			log.error("MISearchRequestHandler.setSeatInventoryRowHtml() method is failed :", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception exception) {
			log.error("MISearchRequestHandler.setSeatInventoryRowHtml() method is failed :", exception);
			saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Method to set Carrier codes
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setUserCarriers(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		StringBuilder usb = new StringBuilder();
		Set<String> stCarriers = user.getCarriers();
		if (stCarriers != null) {
			int i = 0;
			Iterator<String> stIter = stCarriers.iterator();
			while (stIter.hasNext()) {
				usb.append(" arrCarriers[" + i + "] = '" + (String) stIter.next() + "';");
				i++;
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_USER_CRRIERCODE, usb.toString());
	}
}
