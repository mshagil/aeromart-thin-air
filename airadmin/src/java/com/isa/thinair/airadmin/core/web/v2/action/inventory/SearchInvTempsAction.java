package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchInvTempsAction extends BaseRequestAwareAction {

	Log log = LogFactory.getLog(SearchInvTempsAction.class);

	private String airCraftModel;

	private int pageSize = 20;

	private int page;

	private int records;

	private int total;

	private ArrayList<Map<String, Object>> invTemplateList;

	public String execute() {
		
		try {
			int start = (page - 1) * 20;

			Page<InvTempDTO> invTempPage = ModuleServiceLocator.getInventoryTemplateBD().searchInvTemp(start, pageSize,
					airCraftModel);
			setInvTemplateList(getInvTempDTOList((List<InvTempDTO>) invTempPage.getPageData()));
			this.setRecords(invTempPage.getTotalNoOfRecords());
			this.total = invTempPage.getTotalNoOfRecordsInSystem() / 20;
			int mod = invTempPage.getTotalNoOfRecordsInSystem() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
		} catch (ModuleException e) {
			log.error("SearchInvTempAction ==> ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public static ArrayList<Map<String, Object>> getInvTempDTOList(List<InvTempDTO> invTemplateList) {
		ArrayList<Map<String, Object>> invTempDTOList = new ArrayList<Map<String, Object>>();
		InvTempDTO invTempDTO = null;
		int i = 0;
		Iterator<InvTempDTO> iterator = invTemplateList.iterator();
		while (iterator.hasNext()) {
			invTempDTO = iterator.next();
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("invTempDTO", invTempDTO);
			row.put("id", i++);
			invTempDTOList.add(row);
		}

		return invTempDTOList;
	}

	public String getAirCraftModel() {
		return airCraftModel;
	}

	public void setAirCraftModel(String airCraftModel) {
		this.airCraftModel = airCraftModel;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public ArrayList<Map<String, Object>> getInvTemplateList() {
		return invTemplateList;
	}

	public void setInvTemplateList(ArrayList<Map<String, Object>> invTemplateList) {
		this.invTemplateList = invTemplateList;
	}
}
