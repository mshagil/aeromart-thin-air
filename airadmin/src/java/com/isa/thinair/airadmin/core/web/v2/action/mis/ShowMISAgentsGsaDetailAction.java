package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.agents.AgentSummary;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.agents.GSADetailDTO;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airtravelagents.api.dto.AgentTransactionsDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.mis.OutstandingInvoiceInfoTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISAgentsGsaDetailAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowMISAgentsGsaDetailAction.class);
	private boolean success = true;
	private String messageTxt = null;
	private String gsaCode = null;
	private String fromDate = null;
	private String toDate = null;
	private String region = null;
	private String pos = null;
	private GSADetailDTO gsaDetail = null;

	public String execute() throws Exception {

		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		try {

			gsaDetail = new GSADetailDTO();

			// set the employee count
			Integer employeesCount = ModuleServiceLocator.getMISDataProviderBD().getNoOfEmployees(gsaCode);
			gsaDetail.setEmployees(employeesCount.toString());

			Agent gsaAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(gsaCode);

			// set the management
			String management = gsaAgent.getContactPersonName();
			if (management == null)
				management = "Not Given";
			gsaDetail.addManagement(management);

			ArrayList<AgentSummary> agentSummaryList = new ArrayList<AgentSummary>();
			// set the branches count
			int noOfBranches = 0;
			List<String> agentCodeList = new ArrayList<String>();
			agentCodeList.add(gsaCode);
			BigDecimal gsaTurnOver = null;

			// set the turnover
			MISReportsSearchCriteria misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(this.fromDate);
			misSearchCriteria.setToDate(this.toDate);
			misSearchCriteria.setRegion(this.region);
			misSearchCriteria.setPos(this.pos);

			if (gsaAgent.getAgentTypeCode().equals("GSA")) {
				Collection<Agent> agentsList = ModuleServiceLocator.getTravelAgentBD().getActiveAgentsForGSA(gsaCode);

				if (agentsList != null) {
					for (Agent agent : agentsList) {
						if (agent.getReportingToGSA().equalsIgnoreCase("Y"))
							agentCodeList.add(agent.getAgentCode());
					}
				}

				HashMap<String, BigDecimal> turnOverMap = ModuleServiceLocator.getMISDataProviderBD().getGSATurnover(
						misSearchCriteria, agentCodeList);

				for (Agent agent : agentsList) {
					if (agent.getReportingToGSA().equalsIgnoreCase("Y")) {
						AgentSummary agentSummary = new AgentSummary();
						agentSummary.setAgentCode(agent.getAgentCode());
						agentSummary.setAgentName(agent.getAgentDisplayName());

						BigDecimal agentTurnOver = turnOverMap.get(agent.getAgentCode());
						if (agentTurnOver == null)
							agentTurnOver = AccelAeroCalculator.getDefaultBigDecimalZero();
						agentSummary.setTurnover(AccelAeroCalculator.formatAsDecimal(agentTurnOver));
						agentSummary.setTurnoverValue(agentTurnOver);

						agentSummaryList.add(agentSummary);

						noOfBranches++;
					}
				}

				gsaTurnOver = turnOverMap.get(gsaCode);

			} else {

				HashMap<String, BigDecimal> turnOverMap = ModuleServiceLocator.getMISDataProviderBD().getGSATurnover(
						misSearchCriteria, agentCodeList);
				gsaTurnOver = turnOverMap.get(gsaCode);
			}

			if (gsaTurnOver == null)
				gsaTurnOver = AccelAeroCalculator.getDefaultBigDecimalZero();

			// add the GSA the list
			AgentSummary agentSummary = new AgentSummary();
			agentSummary.setAgentCode(gsaAgent.getAgentCode());
			agentSummary.setAgentName(gsaAgent.getAgentName());
			agentSummary.setTurnover(AccelAeroCalculator.formatAsDecimal(gsaTurnOver));
			agentSummary.setTurnoverValue(gsaTurnOver);
			agentSummaryList.add(agentSummary);

			Collections.sort(agentSummaryList, new Comparator<AgentSummary>() {
				@Override
				public int compare(AgentSummary one, AgentSummary two) {
					return one.getTurnoverValue().compareTo(two.getTurnoverValue()) * -1;
				}
			});

			gsaDetail.setBranches(noOfBranches + "");
			gsaDetail.setAgentSummaryList(agentSummaryList);
			gsaDetail.setTurnOver(gsaTurnOver.toString() + " " + AppSysParamsUtil.getBaseCurrency());

			// set the period
			gsaDetail.setPeriodEnd(this.toDate);
			gsaDetail.setPeriodStart(this.fromDate);

			Collection<OutstandingInvoiceInfoTO> outstandingInvoiceInfo = ModuleServiceLocator.getMISDataProviderBD()
					.getOutstandingInvoiceInfo(misSearchCriteria, gsaCode);

			BigDecimal totalSettlement = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalCollection = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal outstanding30 = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal outstanding60 = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal outstanding90 = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal outstandingOther = AccelAeroCalculator.getDefaultBigDecimalZero();
			Date today = new Date(System.currentTimeMillis());

			for (OutstandingInvoiceInfoTO oiTO : outstandingInvoiceInfo) {
				BigDecimal settled = oiTO.getSettledAmount();
				BigDecimal invoiced = oiTO.getInvoiceAmount();
				BigDecimal outstanding = AccelAeroCalculator.subtract(invoiced, settled);

				totalSettlement = AccelAeroCalculator.add(totalSettlement, settled);
				int dayDiff = CalendarUtil.daysUntil(oiTO.getInvoiceDate(), today);

				if (dayDiff < 31) {
					outstanding30 = AccelAeroCalculator.add(outstanding30, outstanding);
				} else if (dayDiff < 61) {
					outstanding60 = AccelAeroCalculator.add(outstanding60, outstanding);
				} else if (dayDiff < 91) {
					outstanding90 = AccelAeroCalculator.add(outstanding90, outstanding);
				} else {
					outstandingOther = AccelAeroCalculator.add(outstandingOther, outstanding);
				}
			}

			// finding the total colletion
			AgentTransactionsDTO agentTransDTO = new AgentTransactionsDTO();
			SimpleDateFormat smpDateSql = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date startDate = smpDateSql.parse(this.fromDate + " 00:00:00");
			Date endDate = smpDateSql.parse(this.toDate + " 23:59:59");

			agentTransDTO.setTnxFromDate(startDate);
			agentTransDTO.setTnxToDate(endDate);
			agentTransDTO.setAgentCode(gsaCode);
			Collection<AgentTransactionNominalCode> trnsColl = new HashSet<AgentTransactionNominalCode>();
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CASH);
			trnsColl.add(AgentTransactionNominalCode.PAY_INV_CHQ);
			agentTransDTO.setAgentTransactionNominalCodes(trnsColl);
			Collection agentTxns = ModuleServiceLocator.getTravelAgentFinanceBD().getAgentTransactions(agentTransDTO);
			
			if (agentTxns != null && agentTxns.size() > 0) {
				for (Object object : agentTxns) {
					AgentTransaction agentTnx = (AgentTransaction) object;
					totalCollection = AccelAeroCalculator.add(totalCollection, agentTnx.getAmount());
				}
			}
			
			totalCollection = totalCollection.negate();

			BigDecimal totalUninvoced = ModuleServiceLocator.getTravelAgentBD().getUninvocedTotal(gsaCode, startDate, endDate);

			gsaDetail.setTotalCollection(AccelAeroCalculator.formatAsDecimal(totalCollection));
			gsaDetail.setTotalSettlement(AccelAeroCalculator.formatAsDecimal(totalSettlement));
			gsaDetail.setTotalNonInvoiced(AccelAeroCalculator.formatAsDecimal(totalUninvoced));
			gsaDetail.setOutstanding30(AccelAeroCalculator.formatAsDecimal(outstanding30));
			gsaDetail.setOutstanding60(AccelAeroCalculator.formatAsDecimal(outstanding60));
			gsaDetail.setOutstanding90(AccelAeroCalculator.formatAsDecimal(outstanding90));
			gsaDetail.setOutstandingOther(AccelAeroCalculator.formatAsDecimal(outstandingOther));
			gsaDetail.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getGsaCode() {
		return gsaCode;
	}

	public void setGsaCode(String gsaCode) {
		this.gsaCode = gsaCode;
	}

	public GSADetailDTO getGsaDetail() {
		return gsaDetail;
	}

	public void setGsaDetail(GSADetailDTO gsaDetail) {
		this.gsaDetail = gsaDetail;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}
}
