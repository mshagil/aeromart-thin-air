package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class MealCategoryHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for Meal Category Page.
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.meal.category.form.name.required", "mealCategoryNameRqrd");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
