package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * @author Eshan
 * 
 */

public class IbeExitReportRH extends BasicRequestHandler {

	/** logger for IbeExitReportRH class */
	private static Log log = LogFactory.getLog(IbeExitReportRH.class);

	/** Global Configurations */
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String exitDetailsId = request.getParameter("exitDetailsId");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("IbeExitReportRH setDisplayData() SUCCESS");
		} catch (Exception ex) {
			log.error("IbeExitReportRH setDisplayData() FAILED" + ex.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("IbeExitReportRH setReportView Success");
				return null;
			} else {
				log.error("IbeExitReportRH setReportView not selected");
			}

		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("IbeExitReportRH setReportView Failed " + ex.getMessageString());
		}
		
		try{
			
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DETAIL)) {
				setReportViewForPaxDetails(request, response,exitDetailsId);
				log.debug("IbeExitReportRH setReportView Success");
				return null;
			} else {
				log.error("IbeExitReportRH setReportView not selected");
			}
		}catch(ModuleException me){
			
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setAppParamSelections(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			moduleException.printStackTrace();
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String strReportId = "UC_REPM_086";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String reportTemplate = "IbeExitDetailsReport.jasper";
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		try {
			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					searchCriteria.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					searchCriteria.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			
			if (fromDate != null && !fromDate.equals("") && toDate != null && !toDate.equals("")) {
				
				searchCriteria.setDateRangeFrom(dateFormat.format(format.parse(fromDate)));
				searchCriteria.setDateRangeTo(dateFormat.format(format.parse(toDate)));
			}else{
				
			}
			
			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getIbeExitDataForReports(searchCriteria);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("ID", strReportId);
			parameters.put("CARRIER", strCarrier);
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Ibe_Exit_Details_Report.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Ibe_Exit_Details_Report.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Ibe_Exit_Details_Report.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void setReportViewForPaxDetails(HttpServletRequest request, HttpServletResponse response, String ibeExitId)
			throws ModuleException {
		
		String strReportId = "UC_REPM_086";
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String reportTemplate = "IbeExitDetailPaxDetails.jasper";

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ID", strReportId);
		parameters.put("CARRIER", strCarrier);
		
		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		// To provide Report Format Options
		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);
		ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getIbeExitDataForReports(ibeExitId);
		response.reset();
		response.addHeader("Content-Disposition", "attachment;filename=IbeExitContactDetails_Report.xls");
		ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
				response);
	}

	private static void setAppParamSelections(HttpServletRequest request) throws ModuleException {
		
	}
}
