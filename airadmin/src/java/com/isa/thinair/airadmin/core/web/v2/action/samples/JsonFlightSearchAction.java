package com.isa.thinair.airadmin.core.web.v2.action.samples;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

@Namespace(S2Constants.Namespace.PRIVATE_SAMPLES)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class JsonFlightSearchAction {

	private static Log log = LogFactory.getLog(JsonFlightSearchAction.class);

	private Date startDate;

	private Date stoptDate;

	private Collection<Flight> flights;

	public String execute() throws Exception {

		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		ModuleCriterion moduleCriterionStartD = new ModuleCriterion();

		// start date of the schedule
		moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
		moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
		List<Date> valueStartDate = new ArrayList<Date>();
		valueStartDate.add(startDate);
		moduleCriterionStartD.setValue(valueStartDate);
		critrian.add(moduleCriterionStartD);

		// stop date of the schedule
		ModuleCriterion moduleCriterionStopD = new ModuleCriterion();
		moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
		moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
		List<Date> valueStopDate = new ArrayList<Date>();
		valueStopDate.add(stoptDate);
		moduleCriterionStopD.setValue(valueStopDate);

		critrian.add(moduleCriterionStopD);
		try {
			Page page = ModuleServiceLocator.getFlightServiceBD().searchFlightsForDisplay(critrian, 0, 20, false, null, true, false, null);
			this.flights = page.getPageData();
		} catch (ModuleException e) {
			log.error(e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public Collection<Flight> getFlights() {
		return flights;
	}

	public void setFlights(Collection<Flight> flights) {
		this.flights = flights;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getStoptDate() {
		return stoptDate;
	}

	public void setStoptDate(Date stoptDate) {
		this.stoptDate = stoptDate;
	}
}
