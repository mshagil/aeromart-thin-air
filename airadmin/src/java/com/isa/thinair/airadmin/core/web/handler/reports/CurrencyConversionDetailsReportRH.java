package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class CurrencyConversionDetailsReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CurrencyConversionDetailsReportRH.class);
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public CurrencyConversionDetailsReportRH() {
		super();
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("CurrencyConversionDetailsReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("CurrencyConversionDetailsReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("CurrencyConversionDetailsReportRH setReportView Success");
				return null;
			} else {
				log.error("CurrencyConversionDetailsReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CurrencyConversionDetailsReportRH setReportView Failed ", e);
		}

		return forward;
	}

	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCurrencyCodes(request);
		setCurrencyStatus(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	protected static void setCurrencyCodes(HttpServletRequest request) throws ModuleException {
		String strATList = ReportsHTMLGenerator.createCurrencyCodeHtml();
		request.setAttribute(WebConstants.REQ_HTML_CURRENCY_CODE_LIST, strATList);
	}

	private static void setCurrencyStatus(HttpServletRequest request) throws ModuleException {
		String strHTML = "<option value='" + Currency.STATUS_ACTIVE + "' selected>Active</option><option value='"
				+ Currency.STATUS_INACTIVE + "'>In-Active</option>";
		request.setAttribute(WebConstants.CURRENCY_STATUS, strHTML);

	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String reportTemplate = null;

		String strCurrencies = request.getParameter("hdnCurrencies");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String status = request.getParameter("selStatus");
		String iBEVisibililty = request.getParameter("chkIBEVisibililty");
		String xBEVisibililty = request.getParameter("chkXBEVisibililty");
		String value = request.getParameter("radReportOption");

		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && fromDate.trim().length() != 0) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}
			if (toDate != null && toDate.trim().length() != 0) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}
			if (iBEVisibililty != null && iBEVisibililty.trim().equals("Y")) {
				iBEVisibililty = "1";
			}
			if (xBEVisibililty != null && xBEVisibililty.trim().equals("Y")) {
				xBEVisibililty = "1";
			}

			search.setiBEVisibililty(iBEVisibililty);
			search.setxBEVisibililty(xBEVisibililty);
			search.setStatus(status);
			search.setCurrencies(strCurrencies);
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getCurrencyConversionData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORT_TYPE", value);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("ID", "UC_REPM_030");
			if (fromDate != null && fromDate.trim().length() != 0) {
				parameters.put("fromDate", fromDate);
				parameters.put("toDate", toDate);
			} else {
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				parameters.put("fromDate", formatter.format(new Date()));
				parameters.put("toDate", formatter.format(new Date()));
			}

			reportTemplate = "CurrencyConversionDetailsReport.jasper";

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);

			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CurrencyConversionDetailsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error("setReportView :: ", e);
		}
	}
}
