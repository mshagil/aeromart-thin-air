package com.isa.thinair.airadmin.core.web.v2.action.pricing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.api.FareInfoDTO;
import com.isa.thinair.airadmin.api.FareRuleCommentDTO;
import com.isa.thinair.airadmin.api.FareRuleDTO;
import com.isa.thinair.airadmin.api.FareRuleFeeDTO;
import com.isa.thinair.airadmin.api.FareTO;
import com.isa.thinair.airadmin.api.ProrationTO;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.airadmin.core.web.v2.util.FareUtil;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRuleComment;
import com.isa.thinair.airpricing.api.model.Proration;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
@SuppressWarnings("rawtypes")
public class FareAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(FareAction.class);

	private FareInfoDTO fare;

	private boolean success = true;

	private String messageTxt;

	private Map errorInfo;

	private Map fareRuleData;

	private FareTO fareTO;

	private FareRuleDTO fareRule;

	private Collection<FareRuleFeeDTO> fees;

	private Collection<ProrationTO> prorationData;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private String fareToJSONList;

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private String prorationJSON;

	private boolean isEnableProration;

	private String departingAirport;

	private String departingAirportCurrency;

	private Collection<FareRuleCommentDTO> fareRuleComments;

	public String getAirportCurrency() {
		String forward = S2Constants.Result.SUCCESS;
		if (departingAirport != null) {
			String[] orogin = new String[1];
			orogin[0] = departingAirport;
			try {
				Collection<String[]> airportCurrencyList = SelectListGenerator.getAirportCurrency(orogin);
				departingAirportCurrency = airportCurrencyList.iterator().next()[0];
			} catch (ModuleException e) {
				log.error(e);
				success = false;
				messageTxt = e.getMessageString();
			}
		}
		return forward;
	}

	public String createPageInitailData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			if (fare == null) {
				fare = new FareInfoDTO();
			}
			createFareInfo();
			String fareID = request.getParameter("fareID");
			errorInfo = ErrorMessageUtil.getFareErrors(AiradminConfig.defaultLanguage);
			isEnableProration = AppSysParamsUtil.isEnableFareProration();
			if (!StringUtil.isNullOrEmpty(fareID)) {
				FareDTO fare = ModuleServiceLocator.getFareBD().getFare(Integer.parseInt(fareID));
				fareRuleData = new HashMap();
				FareUtil.addFareRuleData(fare.getFareRule(), fareRuleData, new SimpleDateFormat("HH:mm"));
				addFare(fare.getFare());
				fareTO.setFareRuleCode(fare.getFareRuleCode());
				fareTO.setReturnFlag(String.valueOf(fare.getFareRule().getReturnFlag()));
				createProration(fare.getProrations());
			}
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String loadFareRule() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			FareRule fareRuleTmp = ModuleServiceLocator.getRuleBD().getFareRule(
					Integer.parseInt(request.getParameter("fareRuleID")));
			fareRuleData = new HashMap();
			FareUtil.addFareRuleData(fareRuleTmp, fareRuleData, new SimpleDateFormat("HH:mm"));
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String saveFare() {
		String forward = S2Constants.Result.SUCCESS;

		try {

			FareDTO fareDto = new FareDTO();
			FareBD fareBD = ModuleServiceLocator.getFareBD();

			Fare tmpFare = new Fare();
			FareTO tmpFareTo = getFareTO();

			if (tmpFareTo != null && tmpFareTo.getFareID() != null) {
				tmpFare.setFareId(tmpFareTo.getFareID());
			}
			if (tmpFareTo != null && tmpFareTo.getVersion() != null) {
				tmpFare.setVersion(tmpFareTo.getVersion());
			}
			tmpFare.setOndCode(getOndCodeFromVia(tmpFareTo));
			setCommonFields(tmpFareTo, tmpFare);

			if (tmpFareTo.getFareID() != null) {
				if (CalendarUtil.getCurrentSystemTimeInZulu().after(tmpFare.getSalesEffectiveFrom())) {
					FareDTO currentfare = fareBD.getFare(tmpFareTo.getFareID());
					if (CalendarUtil.isSameDay(currentfare.getFare().getSalesEffectiveFrom(), tmpFare.getSalesEffectiveFrom())) {
						tmpFare.setSalesEffectiveFrom(currentfare.getFare().getSalesEffectiveFrom());
					}
				}
			}

			if (tmpFareTo != null && tmpFareTo.getFareRuleID() != null) {
				tmpFare.setFareRuleId(tmpFareTo.getFareRuleID());
				fareDto.setFareRuleCode(tmpFareTo.getFareRuleCode());
				fareDto.setFareRuleID(tmpFareTo.getFareRuleID());
			}

			boolean skipSplitFare = false;

			if (tmpFareTo != null && tmpFareTo.getFareRuleCode().equals("")) {
				FareRule fareRuleNew = FareUtil.getFareRuleFromDto(getFareRule(), fees, false);

				if (fareRule.getOnlyUpdateComments() != null && fareRule.getOnlyUpdateComments().equals("Y")) {
					FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();
					skipSplitFare = true;
					fareRuleNew = fareRuleBD.getFareRule(fareRule.getFareRuleID());
					fareRuleNew.setRulesComments(fareRule.getRulesComments());
					fareRuleNew.setAgentInstructions(fareRule.getAgentComments());
					fareRuleNew.setFareRuleDescription(fareRule.getDescription());
					fareRuleNew.setPointOfSale(fareRule.getVisiblePOS());
					fareRuleNew.setApplicableToAllCountries(fareRule.getApplicableToCountries());
				}
				fareRuleNew.setVisibileAgentCodes(fareRule.getVisibileAgentCol());
				fareDto.setFareRule(fareRuleNew);
				//FIXME : Enable Multilingual support for fare rule comments needs to be reviewed 
				if (AppSysParamsUtil.isEnableFareRuleComments() && fareRuleNew.getFareRuleId() != null) {
					FareRule fareRule = ModuleServiceLocator.getRuleBD().getFareRule(fareRuleNew.getFareRuleId());
					FareUtil.updateComments(fareRule, fareRuleComments);
					ModuleServiceLocator.getRuleBD().createFareRule(fareRule, false);
				}
			}
			if (tmpFareTo != null && tmpFareTo.getFareDefType() != null) {
				tmpFare.setFareDefType(tmpFareTo.getFareDefType());
			}

			if (FareUtil.isEnableLinkFare()) {
				// TODO
				tmpFare.setMasterFareRefCode(tmpFareTo.getMasterFareRefCode());
			}

			tmpFare.setProrations(getProrationList());
			fareDto.setFare(tmpFare);
			fareDto.setProrations(getProrationList());

			Integer fareId = fareBD.updateFare(fareDto, skipSplitFare);

			if (fareTO.getCreateReturnFare().equals("Y")) {
				saveReturnFare(tmpFareTo);
			}

			if (fareId != null) {
				fareTO.setFareID(fareId);
			}

		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	@SuppressWarnings("unchecked")
	private Collection<Proration> getProrationList() {
		
		prorationData = JSONFETOParser.parseCollection(ArrayList.class,
				ProrationTO.class, getProrationJSON());

		if (prorationData != null && prorationData.size() > 0) {
			Collection<Proration> prorations = new LinkedHashSet<Proration>();
			for (ProrationTO obj : prorationData) {
				Proration proration = new Proration();
				if (obj.getProrationId() != null && obj.getProrationId() == 0) {
					proration.setProrationId(null);
				} else {
					proration.setProrationId(obj.getProrationId());
				}

				proration.setProration(obj.getProration());
				proration.setSegmentCode(obj.getSegmentCode());
				proration.setVersion(obj.getVersion());
				prorations.add(proration);
			}
			return prorations;
		} else {
			return null;
		}

	}

	private void saveReturnFare(FareTO tmpFareTo) throws Exception {

		FareBD fareBD = ModuleServiceLocator.getFareBD();
		FareDTO fareReturnDto = new FareDTO();
		try {
			Fare newReturnFare = new Fare();
			String returnOnd = "";

			if (!tmpFareTo.getOrigin().equals("") && !tmpFareTo.getDestination().equals("")) {
				returnOnd += tmpFareTo.getDestination() + "/";
				if (tmpFareTo.getVia4() != null && !tmpFareTo.getVia4().equals("")) {
					returnOnd += tmpFareTo.getVia4() + "/";
				}
				if (tmpFareTo.getVia3() != null && !tmpFareTo.getVia3().equals("")) {
					returnOnd += tmpFareTo.getVia3() + "/";
				}
				if (tmpFareTo.getVia2() != null && !tmpFareTo.getVia2().equals("")) {
					returnOnd += tmpFareTo.getVia2() + "/";
				}
				if (tmpFareTo.getVia1() != null && !tmpFareTo.getVia1().equals("")) {
					returnOnd += tmpFareTo.getVia1() + "/";
				}
				returnOnd += tmpFareTo.getOrigin();
			}

			newReturnFare.setOndCode(returnOnd);
			setCommonFields(tmpFareTo, newReturnFare);
			if (tmpFareTo.getFareRuleID() != null) {
				newReturnFare.setFareRuleId(tmpFareTo.getFareRuleID());
				fareReturnDto.setFareRuleCode(tmpFareTo.getFareRuleCode());
				fareReturnDto.setFareRuleID(tmpFareTo.getFareRuleID());
			}

			fareReturnDto.setFare(newReturnFare);
			fareReturnDto.setProrations(getReturnProration());
			fareBD.saveFare(fareReturnDto);

		} catch (ModuleException moduleException) {
			log.error("Save Return Fare failed :" + moduleException.getMessageString());
		}
	}

	private Collection<Proration> getReturnProration() {
		Collection<Proration> returnProrations = new LinkedHashSet<Proration>();
		List<ProrationTO> prorationList = new ArrayList<ProrationTO>(prorationData);

		for (int i = prorationList.size() - 1; i >= 0; i--) {
			Proration proration = new Proration();
			ProrationTO prorationTO = prorationList.get(i);
			proration.setProration(prorationTO.getProration());
			String[] segCode = prorationTO.getSegmentCode().split("/");
			proration.setSegmentCode(segCode[1] + "/" + segCode[0]);
			proration.setVersion(prorationTO.getVersion());
			if (prorationTO.getProrationId() == null || prorationTO.getProrationId().equals(0)) {
				proration.setProrationId(null);
			} else {
				proration.setProrationId(proration.getProrationId());
			}
			returnProrations.add(proration);

		}
		return returnProrations;

	}

	private void setCommonFields(FareTO tmpFareTo, Fare tmpFare) throws Exception {
		tmpFare.setBookingCode(tmpFareTo.getBookingCode());
		tmpFare.setStatus(tmpFareTo.getStatus());

		tmpFare.setFareAmount(tmpFareTo.getFareAmount());

		tmpFare.setEffectiveFromDate(dateFormat.parse(tmpFareTo.getEffectiveFromDate()));
		tmpFare.setEffectiveToDate(CalendarUtil.getEndTimeOfDate(dateFormat.parse(tmpFareTo.getEffectiveToDate())));
		tmpFare.setSalesEffectiveFrom(dateFormat.parse(tmpFareTo.getSalesEffectiveFrom()));
		tmpFare.setSalesEffectiveTo(CalendarUtil.getEndTimeOfDate(dateFormat.parse(tmpFareTo.getSalesEffectiveTo())));

		if (tmpFareTo.getReturnEffectiveFromDate() != null && !tmpFareTo.getReturnEffectiveFromDate().equals("")) {
			tmpFare.setReturnEffectiveFromDate(dateFormat.parse(tmpFareTo.getReturnEffectiveFromDate()));
		}

		if (tmpFareTo.getReturnEffectiveToDate() != null && !tmpFareTo.getReturnEffectiveToDate().equals("")) {
			tmpFare.setReturnEffectiveToDate(CalendarUtil.getEndTimeOfDate(dateFormat.parse(tmpFareTo.getReturnEffectiveToDate())));
		}

		tmpFare.setChildFareType(tmpFareTo.getChildFareType());
		tmpFare.setChildFare(tmpFareTo.getChildFare());
		tmpFare.setInfantFareType(tmpFareTo.getInfantFareType());
		tmpFare.setInfantFare(tmpFareTo.getInfantFare());

		if (FareUtil.isShowFareDefInDepAirPCurr() && tmpFareTo.getCurrencyCode() != null
				&& !tmpFareTo.getCurrencyCode().equals("")) {
			tmpFare.setLocalCurrencyCode(tmpFareTo.getCurrencyCode());
			tmpFare.setLocalCurrencySelected(true);
			tmpFare.setChildFareInLocal(tmpFareTo.getChildFareLocal());
			tmpFare.setInfantFareInLocal(tmpFareTo.getInfantFareLocal());
			tmpFare.setFareAmountInLocal(tmpFareTo.getFareAmountLocal());
		}

		if (FareUtil.isEnableSameFareChecking()) {
			tmpFare.setModifyToSameFare(tmpFareTo.getModifyToSameFare());
		}

	}

	public String saveFares() {
		String forward = S2Constants.Result.SUCCESS;
		try {

			JSONParser parser = new JSONParser();
			JSONArray jsonFareToArray = (JSONArray) parser.parse(getFareToJSONList());
			FareBD fareBD = ModuleServiceLocator.getFareBD();

			Collection<Fare> fareCol = new ArrayList<Fare>();
			Iterator jsonArrayIte = jsonFareToArray.iterator();
			while (jsonArrayIte.hasNext()) {
				JSONObject fareToJsonObj = (JSONObject) jsonArrayIte.next();
				Fare fare = FareUtil.getFareFromJSON(fareToJsonObj);
				fareCol.add(fare);
			}
			fareBD.updateFares(fareCol);
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	private void addFare(Fare fare) {
		fareTO = new FareTO();
		fareTO.setBookingCode(fare.getBookingCode());
		fareTO.setChildFare(fare.getChildFare());
		fareTO.setChildFareType(fare.getChildFareType());
		fareTO.setEffectiveFromDate(CalendarUtil.formatDate(fare.getEffectiveFromDate(), CalendarUtil.PATTERN1));
		fareTO.setEffectiveToDate(CalendarUtil.formatDate(fare.getEffectiveToDate(), CalendarUtil.PATTERN1));
		fareTO.setFareAmount(fare.getFareAmount());
		fareTO.setFareID(fare.getFareId());
		fareTO.setFareRuleID(fare.getFareRuleId());
		fareTO.setInfantFare(fare.getInfantFare());
		fareTO.setInfantFareType(fare.getInfantFareType());
		fareTO.setOndCode(fare.getOndCode());
		fareTO.setReturnEffectiveFromDate(CalendarUtil.formatDate(fare.getReturnEffectiveFromDate(), CalendarUtil.PATTERN1));
		fareTO.setReturnEffectiveToDate(CalendarUtil.formatDate(fare.getReturnEffectiveToDate(), CalendarUtil.PATTERN1));
		fareTO.setSalesEffectiveTo(CalendarUtil.formatDate(fare.getSalesEffectiveTo(), CalendarUtil.PATTERN1));
		fareTO.setSalesEffectiveFrom(CalendarUtil.formatDate(fare.getSalesEffectiveFrom(), CalendarUtil.PATTERN1));
		fareTO.setStatus(fare.getStatus());
		fareTO.setVersion(new Long(fare.getVersion()).intValue());
		fareTO.setModifyToSameFare(fare.getModifyToSameFare());
		fareTO.setFareAmountLocal(fare.getFareAmountInLocal() != null ? fare.getFareAmountInLocal() : 0);
		fareTO.setChildFareLocal(fare.getChildFareInLocal() != null ? fare.getChildFareInLocal() : 0);
		fareTO.setInfantFareLocal(fare.getInfantFareInLocal() != null ? fare.getInfantFareInLocal() : 0);
		fareTO.setCurrencyCode(fare.getLocalCurrencyCode());
		fareTO.setMasterFareRefCode(fare.getMasterFareRefCode());
	}

	private void createProration(Collection<Proration> prorations) {
		prorationData = new ArrayList<ProrationTO>();

		for (Proration proration : prorations) {
			ProrationTO prorationTO = new ProrationTO();
			prorationTO.setProrationId(proration.getProrationId());
			prorationTO.setProration(proration.getProration());
			prorationTO.setVersion(proration.getVersion());
			prorationTO.setSegmentCode(proration.getSegmentCode());
			prorationData.add(prorationTO);
		}

	}

	private void createFareInfo() throws ModuleException {
		fare.setSystemDate(FareUtil.getSystemDate());
		fare.setChildVisible(FareUtil.isChildVisible());
		fare.setFareCategoryList(SelectListGenerator.createFareCategoryArrayList());
		fare.setFareModValueTypes(SelectListGenerator.createFareModCancTypesList());
		fare.setPaxChargeTypes(SelectListGenerator.createPaxChargeTypesList());
		fare.setFareRuleIdTypeList(SelectListGenerator.createFareRuleIDTypeList());
		fare.setFareVisibilityList(SelectListGenerator.createFareVisibilityArrayList());
		fare.setFixedBCFareRuleList(SelectListGenerator.createFixedBCFareRuleArrayList());
		fare.setFlexiCategoryList(SelectListGenerator.createFlexiCodeArrayList());
		fare.setFlexiJournylist(SelectListGenerator.createFlexiJourneyArrayList());
		fare.setOpenReturnSupport(FareUtil.isOpenReturnSupport());
		fare.setPaxCategoryList(SelectListGenerator.createPaxCatagoryArrayList());
		fare.setViaPointList(SelectListGenerator.createAirportComboList());
		fare.setBookingCodeList(FareUtil.getBookingClassList());
		fare.setFarePaxConfig(FareUtil.getPaxFareParams());
		fare.setCurrencyCodeList(SelectListGenerator.createActiveCurrencyCodeList());
		fare.setEnableFareDiscount(AppSysParamsUtil.isFareDiscountEnabled());
		if (!AppSysParamsUtil.isAllowReturnParticipateInHalfReturn()) {
			fare.setHalfReturnSupport(true);
		}
		addSystemParameters();
		addHub();
	}

	private void addSystemParameters() throws ModuleException {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String bufferTime = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.INTERNATIONAL_CHANGEBUFFER_TIME);
		long bufferTimeInMilliSec = AppSysParamsUtil.getInternationalBufferDurationInMillis();

		fare.setEnableDomesticBufferTime(false);

		List<FlightTypeDTO> lstFltTypes = AppSysParamsUtil.availableFlightTypes();
		for (FlightTypeDTO fltType : lstFltTypes) {
			if (fltType.getFlightType().equals("DOM") && fltType.getFlightTypeStatus().equals("Y")) {
				fare.setEnableDomesticBufferTime(true);
			}
		}

		try {
			ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
			Collection<String> chargeGroupCol = new ArrayList<String>();
			chargeGroupCol.add(PricingConstants.ChargeGroups.MODIFICATION_CHARGE);
			chargeGroupCol.add(PricingConstants.ChargeGroups.CANCELLATION_CHARGE);
			HashMap map = chargeBD.quoteUniqueChargeForGroups(chargeGroupCol);
			QuotedChargeDTO quoteDTOMod = (QuotedChargeDTO) map.get(PricingConstants.ChargeGroups.MODIFICATION_CHARGE);
			QuotedChargeDTO quoteDTOCancel = (QuotedChargeDTO) map.get(PricingConstants.ChargeGroups.CANCELLATION_CHARGE);
			boolean isOpenRTEnabled = AppSysParamsUtil.isOpenEndedReturnEnabled();

			if (quoteDTOMod != null) {
				fare.setModCharge(quoteDTOMod.getChargeValuePercentage());
			}
			if (quoteDTOCancel != null) {
				fare.setCnxCharge(quoteDTOCancel.getChargeValuePercentage());
			}
			fare.setBufferTime(bufferTime + ":00");
			fare.setBufTimeInMilSec(bufferTimeInMilliSec);
			fare.setDomBufferTimeInMilSec(AppSysParamsUtil.getDomesticBufferDurationInMillis());
			fare.setOpenRTEnabled(isOpenRTEnabled);
			fare.setEnableFareInDepAirPCurr(AppSysParamsUtil.isShowFareDefInDepAirPCurr());
			fare.setEnableSameFareChecking(FareUtil.isEnableSameFareChecking());
			fare.setEnableLinkFare(FareUtil.isEnableLinkFare());
			// Checking Fare rule level Name Change Charge enable for XBE here
			fare.setEnableFareRuleNCC(AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.XBE));
		} catch (Exception e) {
			log.error(e);
		}
	}

	public void addHub() {
		Collection hubList = CommonsServices.getGlobalConfig().getHubsMap().values();
		List hubs = new ArrayList();
		if (hubList != null && !hubList.isEmpty()) {
			for (Iterator hubListIter = hubList.iterator(); hubListIter.hasNext();) {
				HubAirportTO hubTo = (HubAirportTO) hubListIter.next();
				hubs.add(hubTo.getAirportCode());
			}
		}
		fare.setHubList(hubs);
	}

	private boolean validateAirports() throws ModuleException {
		boolean validairport = true;
		List<Map<String, String>> lstAirport = SelectListGenerator.createAirportsListWithStatus();
		String origin = fareTO.getOrigin();
		String destination = fareTO.getDestination();

		if (!"".equals(fareTO.getOndCode())) {
			validairport = validateAirports(lstAirport, origin);
			if (!validairport) {
				messageTxt = airadminConfig.getMessage("um.fare.origin.not.active");
			}
			if (!"".equals(fareTO.getVia1())) {
				if (validairport) {
					validairport = validateAirports(lstAirport, fareTO.getVia1());
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via1.not.active");
					}
				}
			}
			if (!"".equals(fareTO.getVia2())) {
				if (validairport) {
					validairport = validateAirports(lstAirport, fareTO.getVia2());
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via2.not.active");
					}
				}
			}
			if (!"".equals(fareTO.getVia3())) {
				if (validairport) {
					validairport = validateAirports(lstAirport, fareTO.getVia3());
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via3.not.active");
					}
				}
			}
			if (!"".equals(fareTO.getVia4())) {
				if (validairport) {
					validairport = validateAirports(lstAirport, fareTO.getVia4());
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via4.not.active");
					}
				}
			}
			if (validairport) {
				validairport = validateAirports(lstAirport, destination);
				if (!validairport) {
					messageTxt = airadminConfig.getMessage("um.fare.destination.not.active");
				}
			}
		}
		return validairport;
	}

	private boolean validateAirports(List<Map<String, String>> lstAirport, String airport) {

		boolean valid = false;
		try {
			Iterator<Map<String, String>> ite = lstAirport.iterator();
			while (ite.hasNext()) {

				Map<String, String> keyValues = (Map<String, String>) ite.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				while (keyIterator.hasNext()) {
					String status = (String) keyValues.get(keyIterator.next());
					String port = (String) keyValues.get(keyIterator.next());

					if (port.equals(airport)) {
						if (status.equals(Airport.STATUS_ACTIVE)) {
							return true;
						} else {
							return false;
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("FareRequestHandler.setFareRuleData() method is failed :" + e.getMessage());
		}
		return valid;
	}

	private String getOndCodeFromVia(FareTO tmpFareTo) {
		String ondCode = "";
		ondCode += tmpFareTo.getOrigin();

		if (tmpFareTo.getVia1() != null && !tmpFareTo.getVia1().equals("")) {
			ondCode += "/" + tmpFareTo.getVia1();
		}
		if (tmpFareTo.getVia2() != null && !tmpFareTo.getVia2().equals("")) {
			ondCode += "/" + tmpFareTo.getVia2();
		}
		if (tmpFareTo.getVia3() != null && !tmpFareTo.getVia3().equals("")) {
			ondCode += "/" + tmpFareTo.getVia3();
		}
		if (tmpFareTo.getVia4() != null && !tmpFareTo.getVia4().equals("")) {
			ondCode += "/" + tmpFareTo.getVia4();
		}

		ondCode += "/" + tmpFareTo.getDestination();

		return ondCode;

	}

	/*
	 * private Collection<ProrationTO> getReturnProration(Collection<ProrationTO> prorationTos){ Collection<ProrationTO>
	 * returnProration=null; List<ProrationTO> prorationList=new ArrayList<ProrationTO>();
	 * 
	 * if(prorationList!=null && prorationList.size()>0){ for(int i=prorationList.size()-1;i>=0;i--){ ProrationTO
	 * prorationTO=prorationList.get(i); String[] segments = prorationTO.getSegmentCode().split("/"); String
	 * returnSegment=segments[1] } }
	 * 
	 * return returnProration; }
	 */

	public FareInfoDTO getFare() {
		return fare;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Map getErrorInfo() {
		return errorInfo;
	}

	public void setFareTO(FareTO fareTO) {
		this.fareTO = fareTO;
	}

	public FareTO getFareTO() {
		return fareTO;
	}

	public Map getFareRuleData() {
		return fareRuleData;
	}

	public FareRuleDTO getFareRule() {
		return fareRule;
	}

	public void setFareRule(FareRuleDTO fareRule) {
		this.fareRule = fareRule;
	}

	public Collection<FareRuleFeeDTO> getFees() {
		return fees;
	}

	public void setFees(Collection<FareRuleFeeDTO> fees) {
		this.fees = fees;
	}

	public String getFareToJSONList() {
		return fareToJSONList;
	}

	public void setFareToJSONList(String fareToJSONList) {
		this.fareToJSONList = fareToJSONList;
	}

	public Collection<ProrationTO> getProrationData() {
		return prorationData;
	}

	public void setProrationData(Collection<ProrationTO> prorationData) {
		this.prorationData = prorationData;
	}

	public String getProrationJSON() {
		return prorationJSON;
	}

	public void setProrationJSON(String prorationJSON) {

		this.prorationJSON = prorationJSON;
	}

	public boolean isEnableProration() {
		return isEnableProration;
	}

	public void setEnableProration(boolean isEnableProration) {
		this.isEnableProration = isEnableProration;
	}

	public String getDepartingAirport() {
		return departingAirport;
	}

	public void setDepartingAirport(String departingAirport) {
		this.departingAirport = departingAirport;
	}

	public String getDepartingAirportCurrency() {
		return departingAirportCurrency;
	}

	public void setDepartingAirportCurrency(String departingAirportCurrency) {
		this.departingAirportCurrency = departingAirportCurrency;
	}

	public Collection<FareRuleCommentDTO> getFareRuleComments() {
		return fareRuleComments;
	}

	public void setFareRuleComments(Collection<FareRuleCommentDTO> fareRuleComments) {
		this.fareRuleComments = fareRuleComments;
	}
}
