package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.FCCSegBCInventoryTO;
import com.isa.thinair.airadmin.core.web.v2.handler.tools.FlightSummaryRequestHandler;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowFlightSummaryAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ShowFlightSummaryAction.class);
	private String flightNumber;
	private String departureDate;
	private int page;
	private int records;
	private int total;
	private Object[] rows;
	private Object[] bcRows;
	private int bcPage;
	private int bcRecords;
	private int bcTotal;

	public String execute() {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			int flightId = FlightSummaryRequestHandler.getFlightId(flightNumber, dateFormat.parse(departureDate));
			if (flightId != -1) {
				Collection<FCCInventoryDTO> fccInvDTOList = FlightSummaryRequestHandler.getFCCSegInvDTOList(flightId);
				searchFlightSegmentSummary(fccInvDTOList);
				searchFlightBCSummary(fccInvDTOList);
			}
		} catch (Exception e) {
			log.error("Exception in ShowFlightSummaryAction.execute", e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	private void searchFlightBCSummary(Collection<FCCInventoryDTO> fccInvDTOList) throws Exception {
		int bcCount = 0;
		Map<String, Collection<FCCSegBCInventoryTO>> cabinClassWiseFccSegBCInvDTOList = new HashMap<String, Collection<FCCSegBCInventoryTO>>();
		for (FCCInventoryDTO fccInvDTO : fccInvDTOList) {
			for (FCCSegInventoryDTO fccSegInvDTO : fccInvDTO.getFccSegInventoryDTOs()) {
				Collection<FCCSegBCInventoryTO> fccSegBCInvDTOList = new ArrayList<FCCSegBCInventoryTO>();
				for (ExtendedFCCSegBCInventory exFCCBCInv : fccSegInvDTO.getFccSegBCInventories()) {
					FCCSegBCInventory fccSegBCInventory = exFCCBCInv.getFccSegBCInventory();
					FCCSegBCInventoryTO fccSegInvTO = new FCCSegBCInventoryTO();
					fccSegInvTO.setBookingCode(fccSegBCInventory.getBookingCode());
					fccSegInvTO.setStandardCode(exFCCBCInv.isStandardCode() ? BookingClass.STANDARD_CODE_Y
							: BookingClass.STANDARD_CODE_N);
					fccSegInvTO.setFixedFlag(exFCCBCInv.isFixedFlag() ? BookingClass.FIXED_FLAG_Y : BookingClass.FIXED_FLAG_N);
					fccSegInvTO.setPriorityFlag(fccSegBCInventory.getPriorityFlag());
					fccSegInvTO.setSeatsAllocated(fccSegBCInventory.getSeatsAllocated());
					fccSegInvTO.setActualSeatsSold(fccSegBCInventory.getActualSeatsSold());
					fccSegInvTO.setActualSeatsOnHold(fccSegBCInventory.getActualSeatsOnHold());
					fccSegInvTO.setSeatsCancelled(fccSegBCInventory.getSeatsCancelled());
					fccSegInvTO.setSeatsAcquired(fccSegBCInventory.getSeatsAcquired());
					fccSegInvTO.setSeatsAvailable(fccSegBCInventory.getSeatsAvailable());
					fccSegInvTO.setStatusClosed(fccSegBCInventory.getStatus().endsWith(FCCSegBCInventory.Status.CLOSED) ? true
							: false);
					fccSegInvTO.setPriorityFlag(fccSegBCInventory.getPriorityFlag());
					fccSegInvTO.setNestRank((exFCCBCInv.getNestRank() != null) ? exFCCBCInv.getNestRank() : 0);
					int overBookCount = Math.max(fccSegBCInventory.getSeatsAllocated() + fccSegBCInventory.getSeatsAcquired()
							- fccSegBCInventory.getSeatsCancelled(), 0)
							- (fccSegBCInventory.getOnHoldSeats() + fccSegBCInventory.getSeatsSold());
					fccSegInvTO.setOverBookCount(overBookCount < 0 ? (-1 * overBookCount) : 0);
					fccSegBCInvDTOList.add(fccSegInvTO);
					bcCount++;
				}
				cabinClassWiseFccSegBCInvDTOList.put(fccSegInvDTO.getLogicalCCCode(), fccSegBCInvDTOList);
			}
		}
		if (this.bcPage == 0) {
			this.bcPage = 1;
		}
		this.bcRecords = bcCount;
		this.bcTotal = bcCount / 20;
		int mod = bcCount % 20;
		if (mod > 0) {
			this.bcTotal = this.bcTotal + 1;
		}
		int index = 1;
		Object[] dataRow = new Object[bcCount];
		for (Entry<String, Collection<FCCSegBCInventoryTO>> entry : cabinClassWiseFccSegBCInvDTOList.entrySet()) {
			Collection<FCCSegBCInventoryTO> fccSegInvDetails = entry.getValue();
			if (fccSegInvDetails != null) {
				for (FCCSegBCInventoryTO ffccSegInv : fccSegInvDetails) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					counmap.put("id", new Integer(((bcPage - 1) * 20) + index).toString());
					counmap.put("cos", entry.getKey());
					counmap.put("bookingCode", ffccSegInv.getBookingCode());
					counmap.put("standardCode", ffccSegInv.getStandardCode());
					counmap.put("nestRank", ffccSegInv.getNestRank());
					counmap.put("fixedFlag", ffccSegInv.getFixedFlag());
					counmap.put("priorityFlag", (ffccSegInv.isPriorityFlag() == true ? "Y" : "N"));
					counmap.put("seatsAllocated", ffccSegInv.getSeatsAllocated());
					counmap.put("actualSeatsSold", ffccSegInv.getActualSeatsSold());
					counmap.put("actualSeatsOnHold", ffccSegInv.getActualSeatsOnHold());
					counmap.put("seatsCancelled", ffccSegInv.getSeatsCancelled());
					counmap.put("seatsAcquired", ffccSegInv.getSeatsAcquired());
					counmap.put("seatsAvailable", ffccSegInv.getSeatsAvailable());
					counmap.put("overBookCount", ffccSegInv.getOverBookCount());
					counmap.put("statusClosed", (ffccSegInv.isStatusClosed()) == true ? "Y" : "N");
					dataRow[index - 1] = counmap;
					index++;
				}
			}
		}
		this.bcRows = dataRow;
	}

	private void searchFlightSegmentSummary(Collection<FCCInventoryDTO> fccInvDTOList) throws Exception {
		int pageSize = 5;
		int index = 1;
		if (this.page == 0) {
			this.page = 1;
		}
		Object[] dataRow = new Object[fccInvDTOList.size()];
		for (FCCInventoryDTO fccInvDTO : fccInvDTOList) {
			Collection<FCCSegInventoryDTO> fccSegInvDetails = fccInvDTO.getFccSegInventoryDTOs();
			if (fccSegInvDetails != null) {
				for (FCCSegInventoryDTO ffccSegInv : fccSegInvDetails) {
					FCCSegInventory fccSegInventory = ffccSegInv.getFccSegInventory();
					Map<String, Object> counmap = new HashMap<String, Object>();
					counmap.put("id", new Integer(((page - 1) * pageSize) + index).toString());
					counmap.put("segmentCode", fccSegInventory.getSegmentCode());
					counmap.put("cos", fccSegInventory.getLogicalCCCode());
					counmap.put("allocatedSeats", fccSegInventory.getAllocatedSeats());
					counmap.put("oversellSeats", fccSegInventory.getOversellSeats());
					counmap.put("curtailedSeats", fccSegInventory.getCurtailedSeats());
					counmap.put("infantAllocation", fccSegInventory.getInfantAllocation());
					counmap.put("seatsSold", fccSegInventory.getSeatsSold() + "/" + fccSegInventory.getSoldInfantSeats());
					counmap.put("onHoldSeats", fccSegInventory.getOnHoldSeats() + "/" + fccSegInventory.getOnholdInfantSeats());
					counmap.put("availableSeats",
							fccSegInventory.getAvailableSeats() + "/" + fccSegInventory.getAvailableInfantSeats());
					counmap.put("flightSegmentStatus", FlightStatusEnum.ACTIVE.getCode());
					List<Integer> currentSegInvId = new ArrayList<Integer>();
					currentSegInvId.add(fccSegInventory.getFccsInvId());
					int totalSoldOhdCountInInterceptingSegments = ModuleServiceLocator.getFlightInventoryBD()
							.getMaximumEffectiveReservedSetsCountInInterceptingSegments(currentSegInvId);

					int overBookCount = (totalSoldOhdCountInInterceptingSegments + fccSegInventory.getSoldAndOnholdAdultSeats()
							- fccSegInventory.getOversellSeats() - fccSegInventory.getAllocatedSeats() + fccSegInventory
							.getCurtailedSeats());
					counmap.put("overbookCount", (overBookCount < 0) ? 0 : overBookCount);
					dataRow[index - 1] = counmap;
					index++;
				}
			}
		}
		this.rows = dataRow;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public Object[] getBcRows() {
		return bcRows;
	}

	public void setBcRows(Object[] bcRows) {
		this.bcRows = bcRows;
	}

	public int getBcPage() {
		return bcPage;
	}

	public void setBcPage(int bcPage) {
		this.bcPage = bcPage;
	}

	public int getBcRecords() {
		return bcRecords;
	}

	public void setBcRecords(int bcRecords) {
		this.bcRecords = bcRecords;
	}

	public int getBcTotal() {
		return bcTotal;
	}

	public void setBcTotal(int bcTotal) {
		this.bcTotal = bcTotal;
	}
}