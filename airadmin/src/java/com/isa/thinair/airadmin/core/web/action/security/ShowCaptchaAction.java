package com.isa.thinair.airadmin.core.web.action.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.Constants;


@Namespace(AdminStrutsConstants.AdminNameSpace.PUBLIC_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowCaptchaAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowCaptchaAction.class);

	private boolean captchaEnabled = true;

	public String execute() {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		String userIP = getIpAddress(request);

		if (AppSysParamsUtil.isCaptchaEnabledForAirAdminLogin()) {
			ForceLoginInvoker.defaultLogin();
			try {
				captchaEnabled = ModuleServiceLocator.getSecurityBD().isCaptchaEnableForUser(userIP);
			} catch (Exception e) {
				forward = AdminStrutsConstants.AdminAction.ERROR;
				log.error(e);
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			}
			ForceLoginInvoker.close();
		} else {
			captchaEnabled = false;
		}

		return forward;
	}

	private String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		if (ip != null && !ip.trim().isEmpty()) {
			String[] clientIpArray = ip.split(Constants.COMMA_SEPARATOR);
			if (clientIpArray.length > 0) {
				for (String ipAddress : clientIpArray) {
					if (ipAddress != null && !ipAddress.trim().isEmpty()) {
						ip = ipAddress.trim();
						break;
					}
				}
			}
		}

		return ip;
	}

	/**
	 * @return the captchaEnabled
	 */
	public boolean isCaptchaEnabled() {
		return captchaEnabled;
	}

	/**
	 * @param captchaEnabled
	 *            the captchaEnabled to set
	 */
	public void setCaptchaEnabled(boolean captchaEnabled) {
		this.captchaEnabled = captchaEnabled;
	}

}
