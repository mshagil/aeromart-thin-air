<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<%@ page language="java"%>
<%@ page import="java.util.*,java.text.*"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Officers Notification Configuration</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">

<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
 
 
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript"src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.form_last_worked_ver.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>


<script type="text/javascript">
	var reqIsMobileNumMandatory = JSON.parse("<c:out value="${requestScope.reqIsMobileNumMandatory}" escapeXml="false" />");
	var reqIsEmailMandatory = JSON.parse("<c:out value="${requestScope.reqIsEmailMandatory}" escapeXml="false" />");
</script>


<script type="text/javascript"src="../../js/notifications/OfficersNotificationsConfiguration.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>


</head>

<body class="tabBGColor" scroll="no" onkeypress=''
	oncontextmenu="return false" onkeydown='' ondrag='return false'>

		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td>
					<table width="99%">
						<tr>
						
						<td>
							<%@ include file="../common/IncludeMandatoryText.jsp"%>
						</td>
				
						</tr>
					</table>
				</td>
			</tr>
				<tr>
				<td>
				<form method='post' action="showOfficersNotificationsConfiguration.action" id="frmConfigMobNums"> 

					<input type="text" name="contName"  id="contName" style="display: none;" />
					<input type="text" name="mobNumber"  id="mobNumber" style="display: none;" />
					</form>
				</td>
				</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"
				%>Configured Mobile Numbers<%@
				 include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="4"
						ID="Table9">
						<tr>
							<td>
								<table id="tblConfigMobNums" width='100%' border='0' cellpadding='0' cellspacing='0'></table>
								<div id="divConfigMobNumsPager" class="scroll" style="text-align:center;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="tool.officer.contact.add">
								<input type="button" id="btnAddConfigMobNums"
								name="btnAddConfigMobNums" class="Button" value="Add"
								tabindex="6">  
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="tool.officer.contact.edit">
								<input type="button"
								id="btnEditConfigMobNums" name="btnEditConfigMobNums"
								class="Button" value="Edit" tabindex="7">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="tool.officer.contact.delete">
								<input type="button"
								id="btnDeleteConfigMobNums" name="btnDeleteConfigMobNums"
								class="Button" value="Delete">
								</u:hasPrivilege>
								</td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>

			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Add/Modify Mobile Numbers <%@ include
						file="../common/IncludeFormHD.jsp"%>
					<table width="98%" border="0" cellpadding="0" cellspacing="0"
						align="center" ID="Table1">
						<tr>
							<td>
								<form method='post' action="showOfficersNotificationsConfiguration.action" id="frmAddModifyConfigs"> 
									<table width="98%" border="0" cellpadding="0" cellspacing="0"
										align="left" id="Table2">
										<tr>
											<td width="75%" valign="top">
												<table>
												
												<tr>
													<td><font>Officer ID</font></td>
													<td colspan="2"><font><span id="spnOfficerId"></span></font></td>
												</tr>
	
													<tr>
														<td align="left"><font>Contact Name</font></td>
														
														<td><input name="contactName" type="text"
															id="contactName" maxlength="30" size="20"
															tabindex="14"><font class="mandatory"> &nbsp;* </font></td>
														<td></td>
													</tr>
													<tr>
														<td align="left"><font>Moblie Number</font></td>
														
														<td>
																<input type='text' id='txtMobCntry' name='contactInfo.mobileCountry' title="Enter Your Country Code" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtMobArea' name='contactInfo.mobileArea' title="Enter Your Area Code" style="width:30px;" maxlength="4"/>
																<input type='text' id='txtMobiNo' name='contactInfo.mobileNo' title="Enter Your Mobile Number" style="width:75px;" maxlength="10" />
															<font class="mandatory"> &nbsp;* </font>
															</td>
														<td></td>
														
													</tr>
													<tr>
														<td align="left"><font>Contact Email Address</font></td>
														
														<td><input name="contactEmail" type="text"
															id="contactEmail" maxlength="90" size="20"
															tabindex="14"></td>
														<td></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
										<input type="text" name="moblieNumber"  id="moblieNumber" style="display: none;" />
										<input type="text" name="version"  id="version" style="display: none;" />
										<input type="hidden" name="hdnId" id="hdnId" /> 
										<input type="hidden" name="hdnMode" id="hdnMode" />
										<input type="hidden" name="hdnVersion" id="hdnVersion" /> 
								</form>
							</td>
						</tr>

						<tr>
							<td style="height: 42px;" colspan="2" valign="bottom">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
										<input tabindex="16" type="button" name="btnCloseConfigMobNums"
								
											id="btnCloseConfigMobNums" class="Button" value="Close"> 
										<input tabindex="17"
											name="btnResetConfigMobNums" type="button" class="Button"
											id="btnResetConfigMobNums" value="Reset"></td>
										<td colspan="5" align="center"><input tabindex="21"
											name="btnSaveConfigMobNums" type="button" class="Button"
											id="btnSaveConfigMobNums" value="Save"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>

		</table>


</body>


<script>
		var screenId = 'SC_SHDS_0037';
		top[2].HideProgress();
</script>

</html>

