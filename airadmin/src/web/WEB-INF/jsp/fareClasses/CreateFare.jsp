<%-- 
	 @Author 	: 	Sumudu
  --%>

<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"   type="text/javascript"></script>
<script	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	 type="text/javascript"></script>
<script	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	 type="text/javascript"></script>
<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
<script	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	 type="text/javascript"></script>
<script	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	 type="text/javascript"></script>
<script	src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	 type="text/javascript"></script>
<script	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/fareClasses/ManageFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">	
	var saveMsg=false;
	var chidVisibility = "<c:out value="${requestScope.childVisibility}" escapeXml="false" />";
	var masterRules = new Array();
	<c:out value="${requestScope.reqFareRuleData}" escapeXml="false" />
	var model= new Array();
	<c:out value="${requestScope.reqModelData}" escapeXml="false" />
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	<c:out value="${requestScope.reqSysParam}" escapeXml="false" />
	<c:out value="${requestScope.reqSystemDate}" escapeXml="false" />
	<c:out value="${requestScope.reqHubs}" escapeXml="false" />	
	<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
	<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
	<c:out value="${requestScope.saveMsg}" escapeXml="false" />
	var arrAirports= new Array();
	<c:out value="${requestScope.reqViaList}" escapeXml="false" />
	arrAirports[arrAirports.length] = new Array("", "");
	var fareRules = new Array();
	<c:out value="${requestScope.reqPaxFareData}" escapeXml="false" />
	var isOpenRetSupport = "<c:out value="${requestScope.openReturnSupport}" escapeXml="false" />";
	var isCurrencyLoaded = false;
	<c:out value="${requestScope.reqLoadCurr}" escapeXml="false" />
	<c:out value="${requestScope.reqFlexiJourneyList}" escapeXml="false" />
	<c:out value="${requestScope.reqFlexiCodeList}" escapeXml="false"/>
	<c:out value="${requestScope.fareDiscountVisibility}" escapeXml="false" />
	<c:out value="${requestScope.agentFareCommissionVisibility}" escapeXml="false" />
	<c:out value="${requestScope.reqLinkedFaresVisibility}" escapeXml="false" />
	<c:out value="${requestScope.reqMultiFltTypesAvailability}" escapeXml="false" />
	<c:out value="${requestScope.reqSameFareModificationVisibility}" escapeXml="false" />
</script>
</head>
<body onkeydown='return Body_onKeyDown(event)' scroll="yes" oncontextmenu="return showContextMenu()"	onkeypress='return Body_onKeyPress(event)' ondrag='return false'
		class="tabBGColor"	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
<form name="frmCreateFare" id="frmCreateFare" action="showFares.action" method="post">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Fare<%@ include  file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
			<tr>
			<td>
			<table width="100%">
				<tr>
				<td width="10%"><font>Fare ID</font></td>
				<td><font class="fntBold"><span id="spnId"></span></font>
				</tr>
				
			</table>
			</td>
			</tr>
			<tr>
				<td>
					<table width="100%">
						<tr>					
							<td width="12%"><font>Origin</font></td>
							<td align="center" width="9%"></td>
							<td width="2%" ><font class="mandatory">*</font></td>
			
							<td width="12%"><font>Destination</font></td>
							<td width="9%" align="center"></td>
							<td width="4%"> <font class="mandatory">*</font></td>
			
							<td width="6%"><font>Via 1</font></td>
							<td width="9%"></td>
			
							<td width="5%" align="right"><font>Via 2</font></td>
							<td width="10%"></td>
			
							<td width="5%" align="right"><font>Via 3</font></td>
							<td width="10%"></td>
			
							<td width="6%" align="right"><font>Via 4</font></td>
							<td></td>		
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%">
						<tr>			
							<td width="10%"><font>Booking Class</font></td>
							<td width="10%"><select id="selBc" name="selBc" size="1" style="width: 60px;" onChange="bc_onSelect();required_changed()" tabindex="7">
								<option value=""></option>
								<c:out value="${requestScope.reqBookingCodes}" escapeXml="false" />
							</select><font class="mandatory">&nbsp;*</font></td>			
							<td width="47%" align="left"><font class="fntBold"><span id="spnFixed"></span></font></td>			
							<td width="10%"><font>Active</font></td>
							<td><input type="checkbox" name="chkStatus" id="chkStatus" class="NoBorder" onChange="required_changed()" tabindex="8"></td>
						</tr>
					</table>
				</td>
			</tr>
						<tr>
				<td colspan="12"><font class="fntBold">Fare Rules</font></td>
			</tr>
			<tr>
				<td>
					<table width="52%">
						<tr>
							<td align="left"><font>Fare Rule Code</font></td>
							<td><select id="selFC" name="selFC" size="1" style="width: 100px;" onChange="ruleCodeSelect();required_changed()" onBlur="ruleCodeSelect()" tabindex="24">
								<option value=""></option>
								<c:out value="${requestScope.reqFareClassList}" escapeXml="false" />
							</select></td>
							<td><input type="button" id="btnAdd" value="Add" class="Button" onClick="add_click()" tabindex="25">
							    <input type="button" id="btnAddHocRule" value="Ad-Hoc Rule" class="Button" onClick="addhoc_click()" style="width: 100px;" tabindex="26">
							</td>			
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
				<div id="divDateOutbound">					
					<table width="100%">
						<tr>
							<td width="10%"><font>From Date (outbound)</font></td>
							<td width="10%"><input type="text" id="txtFromDt" name="txtFromDt" maxlength="10" size="8" onBlur="dateChk('txtFromDt');setSalesDate('FROM');setInBoundDatesOnChange();extendToDateTenYears_OnBlur('OB');"
								invalidText="true" onChange="required_changed();setInBoundDatesOnChange();" tabindex="9"> 
							</td>
							<td width="7%">
								<a href="javascript:void(0)" onclick="LoadCalendarDate(0,event); return false;" title="View Calendar">
									<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
			
							<td width="10%" align="left"><font>To Date (outbound)</font></td>
							<td width="10%"><input type="text" id="txtToDt" name="txtToDt" maxlength="10" size="8" onBlur="dateChk('txtToDt');setSalesDate('TO');setInBoundDatesOnChange();extendToDateTenYears_Click();" 
								invalidText="true" onChange="required_changed();setInBoundDatesOnChange();" tabindex="10">
							</td>
							<td>	
								<a href="javascript:void(0)" onclick="LoadCalendarDate(1,event); return false;" title="View Calendar">
									<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>	
						</tr>
					</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
				<div id="divDateInbound">					
					<table width="100%">
						<tr>
							<td width="10%"><font>From Date (inbound)</font></td>
							<td width="10%"><input type="text" id="txtFromDtInbound" name="txtFromDtInbound" maxlength="10" size="8" onBlur="dateChk('txtFromDtInbound');extendToDateTenYears_OnBlur('IB');"
								invalidText="true" onChange="required_changed()" tabindex="11"> 
							</td>
							<td width="7%">
								<a href="javascript:void(0)" onclick="LoadCalendarDate(4,event); return false;" title="View Calendar">
									<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
			
							<td width="10%" align="left"><font>To Date (inbound)</font></td>
							<td width="10%"><input type="text" id="txtToDtInbound" name="txtToDtInbound" maxlength="10" size="8" onBlur="dateChk('txtToDtInbound');extendToDateTenYears_Click();" 
								invalidText="true" onChange="required_changed()" tabindex="11">
							</td>
							<td>	
								<a href="javascript:void(0)" onclick="LoadCalendarDate(5,event); return false;" title="View Calendar">
									<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>				
						</tr>
					</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
				<div id="divSalesDates">
					<table width="100%">
						<tr>
							
							<td width="10%"><font>Sales Start Date</font></td>
							<td width="10%"><input type="text" id="txtSlsStDate" name="txtSlsStDate" maxlength="10" size="8" onBlur="dateChk('txtSlsStDate');extendToDateTenYears_OnBlur('SL');"
								invalidText="true" onChange="required_changed()" tabindex="12"> 
								</td>
							<td width="7%">
								<a href="javascript:void(0)" onclick="LoadCalendarDate(2,event); return false;" title="View Calendar">
									<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
			
							<td width="10%"align="left"><font>Sales End Date</font></td>
							<td width="10%"><input type="text" id="txtSlsEnDate" name="txtSlsEnDate" maxlength="10" size="8" onBlur="dateChk('txtSlsEnDate');extendToDateTenYears_Click();" 
								invalidText="true" onChange="required_changed()" tabindex="12">
							</td>
							<td>
							<a href="javascript:void(0)" onclick="LoadCalendarDate(3,event); return false;" title="View Calendar">
								<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
							
						</tr>
					</table>
					<table width="100%">
						<tr>
							<td width="40%"><font>Apply 10 year difference between from dates and to dates</font></td>
							<td align="left" width="60%"><input type="checkBox" name="chkExtendToDate" id="chkExtendToDate" class="noBorder" onclick="extendToDateTenYears_Click()">
						</tr>
					</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<div id="divCurrSelect">
						<table width="100%">
							<tr>
								<td width="10%"><font>Show Fares In</font> </td>
								<td width="20%">
									<input type="radio" id="radCurr" name="radCurr" value="BASE_CURR"  
										class="NoBorder"onClick="currencyClick();required_changed();"
										onChange="" onblur="" checked="checked">
									<font>Base Currency</font>
								</td>
								<td width="25%">
									
									<input type="radio" id="radSelCurr" name="radCurr" value="SEL_CURR"  
															class="NoBorder"onClick="getAirportCurr();currencyClick();required_changed()"
															onchange="">
															<font>Departing Airport Currency</font>
								</td>
								<td width="45%"></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>
				<div id="divFareAmounts">
					<table width="100%" border="0">
						<tr>
							<td width="9%"><font>Base Fare</font></td>
							<td width="12%"><input type="text" name="txtAED" id="txtAED" onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)"
								maxLength="13" size="13" onChange="required_changed()" onblur="BLValidateDecimel(this);" class="rightText" tabindex="13"><font class="mandatory">*</font></td>
							<td width="53%">
								<table>
									<tr>
										<td align="right"><font>Child Fare</font></td>
										<td><select id="selCamt" name="selCamt" size="1" style="width: 35px; height: 15px;" onChange="setFareType(this)" tabindex="14">
											<option value="V">V</option>
											<option value="P">P</option>
											</select>
										</td>
										<td align="center"><input type="text" name="txtCHL" id="txtCHL" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"
											 maxLength="13" size="13" onChange="required_changed()"
											onblur="BLValidateDecimel(this);" class="rightText" tabindex="15"></td>
										<td>&nbsp;&nbsp;&nbsp;</td>
										<td align="right"><font>Infant Fare</font></td>
										<td><select id="selIamt" name="selIamt" size="1" style="width: 35px; height: 15px;" onChange="setFareType(this)" tabindex="16">
											<option value="V">V</option>
											<option value="P">P</option>
										</select></td>
										<td align="center"><input type="text" name="txtINF" id="txtINF" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"
											maxLength="13" size="13" onChange="required_changed()"
											onblur="BLValidateDecimel(this);" class="rightText" tabindex="17"></td>
									</tr>
								</table>
							</td>
							<td width="13%"><font>Create Return Fare </font></td>
							<td align="left" width="1%"><input type="checkBox" name="chkAllowReturn" id="chkAllowReturn" class="noBorder" tabindex="18" onChange="required_changed()">
							<td width="13%"><font>Fare Basis Code</font></td>
							<td><font class="fntBold"><input type="text" id="txtFBC" name="txtFBC" size=8 maxlength="7"></td>
						</tr>
					</table>
					</div>				
				</td>
			</tr>
			
			<tr>
				<td>
					<div id="divFare">
						<table width="100%" border="0">
							<tr>
								<td width="15%"><font>Base Fare In (<c:out value="${requestScope.reqAirportCurrency}" escapeXml="false" />)</font></td>
								<td width="10%"><input type="text" name="txtBaseInLocal" id="txtBaseInLocal" onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)"
									maxLength="13" size="8" onChange="required_changed()" onblur="BLValidateDecimel(this);" class="rightText" tabindex="19"><font class="mandatory">*</font></td>
								
								<td width="53%">
								<table>
									<tr>
										<td align="right"><font>Child Fare</font></td>
										<td><select id="selCamtInLocal" name="selCamtInLocal" onChange="setFareType(this)" size="1" style="width: 35px; height: 15px;" onChange="" tabindex="20">
											<option value="V">V</option>
											<option value="P">P</option>
										</select></td>
										<td align="center"><input type="text" name="txtCHFareInLocal" id="txtCHFareInLocal" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"
											maxLength="13" size="13" onChange="required_changed()"
											onblur="BLValidateDecimel(this);" class="rightText" tabindex="21"></td>
										<td>&nbsp;&nbsp;&nbsp;</td>
										<td align="right"><font>Infant Fare</font></td>
										<td><select id="selIamtInLocal" name="selIamtInLocal" size="1" style="width: 35px; height: 15px;" onChange="setFareType(this)"  tabindex="22">
											<option value="V">V</option>
											<option value="P">P</option>
										</select></td>
										<td align="center"><input type="text" name="txtINFInLocal" id="txtINFInLocal" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"
											maxLength="13" size="13" onChange="required_changed()"
											onblur="BLValidateDecimel(this);" class="rightText" tabindex="23"></td>
									</tr>
								</table>
								</td>
								<td width="10%"><font></font></td>
								<td align="left" width="1%">
								<td width="10%"><font></font></td>
								<td></td>				
							</tr>
						</table>
					</div>
				</td>
			</tr>
						
			<tr>
				<td rowspan="2">			
					<div id="tblMasterLinkedFares">
						<table>
							<tr>
								<td>
									<table width="100%" border="0">
										<tr>							
											<td width="9%"><font>Master Fare</font></td>
											<td align="left" width="1%"><input type="checkBox" name="chkMasterFare" id="chkMasterFare" class="noBorder" tabindex="18" onChange="required_changed()" onclick="masterFareValidate()">
											<td width="20%"><font>Master Fare Reference Code</font></td>
											<td width="10%"><input type="text" id="masterFareRefCode" name="masterFareRefCode" size="10" maxlength="10" class="rightText" onKeyPress="alphaNumericWithoutSpaceValidate(this)" onKeyUp="alphaNumericWithoutSpaceValidate(this)" ></td>
											<td width="60%"></td>							
										</tr>
									</table>				
								</td>
							</tr>
							
							<tr>
								<td>
									<table width="100%" border="0">
										<tr>
											<td width="9%"><font>Linked Fare</font></td>
											<td align="left" width="1%"><input type="checkBox" name="chkLinkFare" id="chkLinkFare" class="noBorder" tabindex="18" onChange="required_changed()" onclick="linkFareValidate()">						
											<td width="10%"><input type="button" id="btnLinkFares" name="btnLinkFares" class="Button" value="Link Fares" style="width: 140px;" onclick="linkFares_click()"></td>
											<td width="15%"><font>Master Fare ID</font></td>
											<!--<td width="10%"><input type="text" id="selectedMasterFareID" name="selectedMasterFareID" size="10" ></td>-->
											<td width="15%"><select id="selectedMasterFareID" name="selectedMasterFareID"
											size="1" style="width: 100px">
											<option value="" selected="selected"></option>
											</select></td>
											<td width="20%"><font>Link Fare Percentage</font></td>
											<td width="10%"><input type="text" id="linkFarePercentage" name="linkFarePercentage" class="rightText" onClick="required_changed()" onKeyPress="validateFarePercentage()" onKeyUp="validateFarePercentage()" size="3" maxlength="3"></td>							
											<td width="20%"></td>														
										</tr>
									</table>				
								</td>
							</tr>
						</table>
					</div>
				</td>	
			</tr>
			
			<tr>
				<td>
					<div id="divModifyToSameFare">
						<table width="100%" border="0">
							<tr>
							 	<td width="20%"><font>Modify To Same Fare </font></td>
								<td align="left" width="30%"><input type="checkBox" name="chkModToSameFare" id="chkModToSameFare" class="noBorder" onChange="required_changed()">
							</tr>
						</table>
					</div>
				</td>
			</tr>
			
		</table><%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	
	<tr>
	<td><%@ include file="../common/IncludeFormTop.jsp"%>Fare Rule<%@ include file="../common/IncludeFormHD.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
		<tr>
			<td>
			
			<table border="0" width="100%">
				<tr><td width="50%">
				<table border="0" width="100%">
					<tr>
					<td colspan="7">
					<table border="0" width="100%">
					<tr>
					<td>
												
												<input type="radio" id="radCurrFareRule" name="radCurrFareRule" value="BASE_CURR"  
												class="NoBorder"onClick="fareRuleCurrClick();required_changed()" checked="checked">
												<font>Base Currency</font>
											</td>
											<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
												<td>
													
													<input type="radio" id=radSelCurrFareRule name="radCurrFareRule" value="SEL_CURR"  
													class="NoBorder"onClick="fareRuleCurrClick();required_changed()">
													<font>Selected Currency</font>
												</td>
											</c:if>
											<td>
												<select name="selCurrencyCode" id="selCurrencyCode" size="1" style="width:105;" onChange="setPageEdited(true)">
													<option value="-1"></option> 
													<c:out value="${requestScope.reqCurrencyList}" escapeXml="false" />
												</select><font class="mandatory"></font>
											</td>
					
					</tr>
					</table>
					</td>
					
					</tr>					
					<tr>
					<td width="10%" valign="top" align="left"><font>Adv.Bkg Days:Hrs:Mins</font></td>
						<td width="1%" align="left"><!-- input type="checkbox" id="chkAdvance" name="chkAdvance" class="NoBorder"
							onClick="DisableAdnvanceBKDays()" onChange="required_changed()" value="ON" -->
							<input type="checkbox" id="chkAdvance" name="chkAdvance" class="NoBorder" 
						onClick="DisableAdnvanceBKDays();required_changed()"tabindex="27">
							</TD>
						<td width="5%" align="left">
							<!--  
							<input type="text" id="txtAdvBookDays" name="txtAdvBookDays" maxlength="3" size="3"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookDays')" onKeyPress="KPValidatePositiveInteger('txtAdvBookDays')"
							class="rightText" onChange="required_changed()">
							-->							
							<input type="text" id="txtAdvBookDays" name="txtAdvBookDays" maxlength="3" size="3"
						onKeyUp="KPValidatePositiveInteger('txtAdvBookDays')" onKeyPress="KPValidatePositiveInteger('txtAdvBookDays')"
						class="rightText" onChange="required_changed()" tabindex="28">
							</td>	
						<td width="5%">
							<!-- 
							<input type="text" id="txtAdvBookHours" name="txtAdvBookHours" maxlength="2" size="2"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')" onKeyPress="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')"
							class="rightText" onChange="required_changed()" tabindex="28">
							 -->
							
							<input type="text" id="txtAdvBookHours" name="txtAdvBookHours" maxlength="2" size="2"
						onKeyUp="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')" onKeyPress="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')"
						class="rightText" onChange="required_changed()" tabindex="28">
							
							</td>
						<td width="5%">
							<!--  
							<input type="text" id="txtAdvBookMins" name="txtAdvBookMins" maxlength="2" size="2"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')" onKeyPress="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')"
							class="rightText" onChange="required_changed()" tabindex="28">
							 -->
							
							
							<input type="text" id="txtAdvBookMins" name="txtAdvBookMins" maxlength="2" size="2"
						onKeyUp="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')" onKeyPress="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')"
						class="rightText" onChange="required_changed()" tabindex="28">
							</td>
											
						
						<td width="13%" align="left"><font>Charge Restrictions</font></td>
						<td width="2%" align="left">
							<!-- 
							<input type="checkbox" id="chkChrgRestrictions" name="chkChrgRestrictions" 
							class="NoBorder" onClick="DisableRestrictions()" onChange="required_changed()" value="ON">
							 -->
							<input type="checkbox" id="chkChrgRestrictions" name="chkChrgRestrictions" class="NoBorder" onClick="DisableRestrictions();required_changed()" tabindex="18">
							</td>
					</tr>
				</table>		
				
				
				</td>	
				<td>
				<table border="0" width="100%">
					<tr>
					<td align="left"  width="8%" align="left"><font>Mod</font></td>
						<td width="62%">
						<!-- 
						<select id="selDefineModType" name="selDefineModType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="14">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />							
							</select>
						<input type="text" id="txtModification" name="txtModification" size="10"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="13">
						 -->	
							
						<select id="selDefineModType" name="selDefineModType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="30">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />							
							</select>
						<input type="text" id="txtModification" name="txtModification" size="10" 
						onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="29" maxLength="13">
							
						<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">	
							<span id="spModLocal">
							<font>&nbsp;Sel. Curr</font>
							<input type="text" id="txtModificationInLocal" name="txtModificationInLocal" size="10" 
							onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="29" maxLength="13">						
							</span>	
						</c:if>	

						</td>						
						<td width="7%"><font>Min.</font></td>
						<td width="8%">						
							<input type="text" id="txtMin_mod" name="txtMin_mod" class="rightText" onClick="required_changed()" tabindex="31" size="3">	
						</td>
						<td width="7%"><font>Max.</font></td>
						<td width="8%">
							<input type="text" id="txtMax_mod" name="txtMax_mod" class="rightText" onClick="required_changed()" tabindex="32" size="3">
						</td>
					</tr>
					<tr>
					<td align="left" ><font>Cancellation</font></td>
						<td >
						<!-- 
						<select id="selDefineCanType" name="selDefineCanType" size="1" style="width: 40px;" onChange="required_changed();clearCancTypeOnClick();" tabindex="18">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />
							</select>
						<input type="text" id="txtCancellation" name="txtCancellation" size="10"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="17">
						 -->
							
						<select id="selDefineCanType" name="selDefineCanType" size="1" style="width: 40px;" onChange="required_changed();clearCancTypeOnClick();" tabindex="34">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />	
							</select>
						<input type="text" id="txtCancellation" name="txtCancellation" size="10" 
						onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="33" maxLength="13">	
						<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">	
						<span id="spCancelLocal">
							<font>&nbsp;Sel. Curr</font>
								<input type="text" id="txtCancellationInLocal" name="txtCancellationInLocal" size="10" 
							onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="33" maxLength="13">
							</span>	
						</c:if>	
						</td>
						<td ><font>Min.</font></td>
						<td >								
							<input type="text" id="txtMin_canc" name="txtMin_canc" class="rightText" onClick="required_changed()" tabindex="35" size="3">				
						</td>
						<td><font>Max.</font></td>
						<td >
							<input type="text" id="txtMax_canc" name="txtMax_canc" class="rightText" onClick="required_changed()" tabindex="36" size="3">						
						</td>
					</tr>
					</table>
				</td>	</tr>				
					
				</table>
			
			
			<%--
			<table width="100%" border="0">
				<tr>
					<td width="10%" valign="top"><font>Adv. Bkg Days:hrs:Mins</font></td>
					
					<td width="1%"><input type="checkbox" id="chkAdvance" name="chkAdvance" class="NoBorder" 
						onClick="DisableAdnvanceBKDays();required_changed()"tabindex="27"></td>
					
					<td width="5%"><input type="text" id="txtAdvBookDays" name="txtAdvBookDays" maxlength="3" size="3"
						onKeyUp="KPValidatePositiveInteger('txtAdvBookDays')" onKeyPress="KPValidatePositiveInteger('txtAdvBookDays')"
						class="rightText" onChange="required_changed()" tabindex="28"></td>
					<td width="5%"><input type="text" id="txtAdvBookHours" name="txtAdvBookHours" maxlength="2" size="2"
						onKeyUp="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')" onKeyPress="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')"
						class="rightText" onChange="required_changed()" tabindex="28"></td>
					<td width="5%"><input type="text" id="txtAdvBookMins" name="txtAdvBookMins" maxlength="2" size="2"
						onKeyUp="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')" onKeyPress="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')"
						class="rightText" onChange="required_changed()" tabindex="28"></td>
						
					<td width="13%"><font>Charge Restrictions</font></td>
					
					<td width="2%"><input type="checkbox" id="chkChrgRestrictions" name="chkChrgRestrictions" class="NoBorder" onClick="DisableRestrictions();required_changed()" tabindex="18">
					</td>
					
					<td align="left" width="4%"><font>Mod</font></td>
					<td width="13%">
						<select id="selDefineModType" name="selDefineModType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="30">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />							
							</select>
						<input type="text" id="txtModification" name="txtModification" size="6" 
						onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="29" maxLength="13">

					</td>
					<td width="3%"><font>Min.</font></td>
					<td width="4%"><input type="text" id="txtMin_mod" name="txtMin_mod" class="rightText" onClick="required_changed()" tabindex="31" size="3">
					</td>
					<td width="3%"><font>Max.</font></td>
					<td width="4%"><input type="text" id="txtMax_mod" name="txtMax_mod" class="rightText" onClick="required_changed()" tabindex="32" size="3">
					</td>
					<td width="7%"><font>Cancellation</font></td>
					<td width="13%">
						<select id="selDefineCanType" name="selDefineCanType" size="1" style="width: 40px;" onChange="required_changed();clearCancTypeOnClick();" tabindex="34">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />	
							</select>
						<input type="text" id="txtCancellation" name="txtCancellation" size="6" 
						onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="33" maxLength="13">
								
					
					</td>
					<td width="3%"><font>Min.</font></td>
					<td width="4%"><input type="text" id="txtMin_canc" name="txtMin_canc" class="rightText" onClick="required_changed()" tabindex="35" size="3">
					</td>
					<td width="3%"><font>Max.</font></td>
					<td width="4%"><input type="text" id="txtMax_canc" name="txtMax_canc" class="rightText" onClick="required_changed()" tabindex="36" size="3">
					</td>
				</tr>
			</table>
			--%>
			</td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0">
				<tr>
					<td width="10%"><input type="radio" id="radFare" name="radFare" value="OneWay"
						onClick="DisableReturnDays();required_changed()" class="NoBorder" tabindex="37"><font>One Way</font></td>
					<td width="10%"><input type="radio" id="radReturn" name="radFare" value="Return" 
						onClick="DisableReturnDays();required_changed()"class="NoBorder" tabindex="38"><font>Return</font></td>
					<td id="halfReturnLabel" width="7%"><font> Half Return</font> </td>
					<td id="halfReturnCheck" width="2%"> <input type="checkbox" id="chkHRT" name="chkHRT" class="NoBorder" onChange="required_changed()"></td>
					<td width="7%"><font> Print Expiry</font> </td>
					<td width="2%"> <input type="checkbox" id="chkPrintExp" name="chkPrintExp" class="NoBorder" onChange="required_changed()"></td>
					<td width="7%" id="tdLblChkOpenRT"><font> Open RT</font></td>
					<td width="5%" id="tdChkOPenRT"> <input type="checkbox" id="chkOpenRT" name="chkOpenRT" class="NoBorder" onChange="required_changed()" 
						onclick="EnableConfirmPeriod(this)"></td> 
					<td width="9%"><font>Pax Category</font></td>
					<td width="10%"><select id="selPaxCat" name="selPaxCat" size="1" style="width: 100px" tabindex="45">
						<c:out value="${requestScope.reqPaxCatList}" escapeXml="false" /></select></td>
					<td width="10%"><font>Fare Category</font></td>
					<td width="10%"><select id="selFareCat" name="selFareCat" size="1" style="width: 100px" tabindex="46" 
						onchange="selFareOnchange()"><c:out value="${requestScope.reqFareCatList}" escapeXml="false" />
						</select><font class="mandatory">*</font></td>
					<td width="10%"><font>Flexi Code</font></font></td>
					<td width="10%"><select id="selFlexiCode" name="selFlexiCode" size="1" style="width: 100px" tabindex="47" 
						></select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr height="22">
			<td colspan="2">
			<table border="0" width="100%">
				<tr>			
					<td width="21%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="20"><font>Fare Visibility</font><font class="mandatory">*</font></td>				
							</tr>
							<tr>
								<td align="left" width="108" valign="top"><select id="selFareVisibility" size="4" name="selFareVisibility"
									style="width: 110; height: 70" multiple onChange="ctrl_visibility_Select();ctrl_visibilityAgents()"
									onBlur="ctrl_visibilityAgents()"><c:out value="${requestScope.reqVisibilityList}" escapeXml="false" />
									</select></td>						
							</tr>
						</table>
					</td>
					<td width="45%"><%@ include file="../common/IncludeFareTimeDurations.jsp"%></td>		
					<td colspan="4" rowspan="2" valign="middle" width="38%"><%@ include file="../common/includePaxFareConditions.jsp"%></td>						
					<td><input style="display: none;" type="checkbox" id="chkRefundable" name="chkRefundable" class="NoBorder"
						onChange="required_changed()" tabindex="42"></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
			<!-- fare rule modifications and load factor-->
			<td colspan="2">
				<table border="0" width="100%">

					<tr>
						<td width="3%"><font>Load Factor</font></td>
						<td width="1%" align="left"><input type="checkbox" id="loadFactorEnabled" name="loadFactorEnabled" 
							class="NoBorder" onClick="loadFactorValidation()" onChange="required_changed()" value="ON"></td>	
						<td width="4%"><font>Min.%</font></td>
						<td width="4%"><input type="text" id="loadFactorMin" name="loadFactorMin" class="rightText" onClick="required_changed()" onKeyPress="VaidateFactor()" onKeyUp="VaidateFactor()" tabindex="15" size="3" maxlength="3">
						</td>
						<td width="4%"><font>Max.%</font></td>
						<td width="4%"><input type="text" id="loadFactorMax" name="loadFactorMax" class="rightText" onClick="required_changed()" onKeyPress="VaidateFactor()" onKeyUp="VaidateFactor()" tabindex="16" size="3" maxlength="3">
						</td>
						
						<td width="5%" ></td>
						
						
						<td width="13%" valign="left" align="left"><font>Allow Modify Date</font></td>
						<td width="1%" align="left"><input type="checkbox" id="modifyByDate" name="modifyByDate" class="NoBorder"
							onClick="" onChange="required_changed()" value="ON" checked="checked"></TD>
						<td width="2%"></td>					
						<td width="13%" align="left"><font>Allow Modify OnD</font></td>
						<td width="1%" align="left"><input type="checkbox" id="modifyByOND" name="modifyByOND" 
							class="NoBorder" onClick="" onChange="required_changed()" value="ON" checked="checked"></td>
							
						<td width="5%" ></td>
						
						<td colspan="6">
							<table id="tblFareDiscount">
								<tr>	
									<td width="10%"><font>Fare Discount</font></td>
									<td width="1%" align="left"><input type="checkbox" id="fareDiscount" name="fareDiscount" 
										class="NoBorder" onClick="enableDisableFareDiscount()" onChange="required_changed()" value="Y"></td>	
									<td width="4%"><font>Min.%</font></td>
									<td width="4%"><input type="text" id="fareDiscountMin" name="fareDiscountMin" class="rightText" onClick="required_changed()" onKeyPress="allowOnlyPositiveInt('fareDiscountMin')" onKeyUp="allowOnlyPositiveInt('fareDiscountMin')" tabindex="15" size="3" maxlength="3">
									</td>
									<td width="4%"><font>Max.%</font></td>
									<td width="4%"><input type="text" id="fareDiscountMax" name="fareDiscountMax" class="rightText" onClick="required_changed()" onKeyPress="allowOnlyPositiveInt('fareDiscountMax')" onKeyUp="allowOnlyPositiveInt('fareDiscountMax')" tabindex="16" size="3" maxlength="3">
									</td>	
								</tr>	
							</table>
						</td>
						
						<td width="5%"></td>
					</tr>
				</table>
				</td>
			</tr>			<tr>
				<td colspan="3">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="50%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td><font class="fntBold">Valid Days of week </font></td>
							</tr>
							<tr>
								<td><font>Out Bound&nbsp;&nbsp;&nbsp;All</font>&nbsp;
									<input type="checkbox" id="chkOutAll" name="chkOutAll" class="NoBorder" onClick="required_changed();selectAll(true)"> <font>Mo</font>&nbsp;
									<input type="checkbox" id="chkOutMon" name="chkOutMon" class="NoBorder" onClick="required_changed()"> <font>Tu</font>&nbsp;
									<input type="checkbox" id="chkOutTues" name="chkOutTues" class="NoBorder" onClick="required_changed()"> <font>We</font>&nbsp;
									<input type="checkbox" id="chkOutWed" name="chkOutWed" class="NoBorder" onClick="required_changed()"> <font>Th</font>&nbsp;
									<input type="checkbox" id="chkOutThu" name="chkOutThu" class="NoBorder" onClick="required_changed()"> <font>Fr</font>&nbsp;
									<input type="checkbox" id="chkOutFri" name="chkOutFri" class="NoBorder" onClick="required_changed()"> <font>Sa</font>&nbsp;
									<input type="checkbox" id="chkOutSat" name="chkOutSat" class="NoBorder" onClick="required_changed()"> <font>Su</font>&nbsp;
									<input type="checkbox" id="chkOutSun" name="chkOutSun" class="NoBorder" onClick="required_changed()">
								</td>	
							</tr>
							<tr>
								<td><font>In Bound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All</font>&nbsp;
									<input type="checkbox" id="chkInAll" name="chkInAll" class="NoBorder" onClick="required_changed();selectAll(false)"> <font>Mo</font>&nbsp;
									<input type="checkbox" id="chkInbMon" name="chkInbMon" class="NoBorder" onChange="required_changed()"> <font>Tu</font>&nbsp;
									<input type="checkbox" id="chkInbTues" name="chkInbTues" class="NoBorder" onChange="required_changed()"> <font>We</font>&nbsp;
									<input type="checkbox" id="chkInbWed" name="chkInbWed" class="NoBorder" onChange="required_changed()"> <font>Th</font>&nbsp;
									<input type="checkbox" id="chkInbThu" name="chkInbThu" class="NoBorder" onChange="required_changed()"> <font>Fr</font>&nbsp;
									<input type="checkbox" id="chkInbFri" name="chkInbFri" class="NoBorder" onChange="required_changed()"> <font>Sa</font>&nbsp;
									<input type="checkbox" id="chkInbSat" name="chkInbSat" class="NoBorder" onChange="required_changed()"> <font>Su</font>&nbsp;
									<input type="checkbox" id="chkInbSun" name="chkInbSun" class="NoBorder" onChange="required_changed()"></td>
							</tr>
							<tr>
									<td>
					                    	<table id="tblAgentCommission" width="90%">
												<tr>
												 	<td><font class="fntBold"> Agent Commission</font></td>
												</tr>
												<tr>
													<td>
														<font>Apply By</font>
													</td>
											    	<td>
											    		<select id="selAgentComType" name="selAgentComType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="30">
															<option value="V">V</option>
															<option value="PF">PF</option>
														</select>
											    	</td>
											    	<td>
														<td align="left" align="left"><font>Commission Value</font>
													</td>
											    	<td>
											    		<input type="text" id="txtAgentCommision" name="txtAgentCommision" size="6" onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="33" maxLength="13">
											    		<font class="mandatory"><b>*</b></font>
											    	</td>
												</tr>
											</table>
							    	</td>
							  </tr>
						</table>
						</td>	
						<td width="15%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="middle" align="center">	<font class="fntBold">Local Time</font></td>
							</tr>
							<tr>
								<td><font>From</font></td>
								<td><font>To</font></td>
							</tr>
							<tr>
								<td width="50%" align="left"><input type="text" id="txtOutTmFrom" name="txtOutTmFrom" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtOutTmFrom)"></td>
								<td width="50%" align="left"><input type="text" id="txtOutTmTo" name="txtOutTmTo" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtOutTmTo)"></td>
							</tr>
							<tr>		
								<td><font class="fntSmall">&nbsp;</font></td>
							</tr>
							<tr>
								<td width="50%" align="left"><input type="text" id="txtInbTmFrom" name="txtInbTmFrom" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtInbTmFrom)"></td>
								<td width="50%" align="left"><input type="text" id="txtInbTmTo" name="txtInbTmTo" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtInbTmTo)"></td>
							</tr>
						</table>
						</td>		
						<td width="35%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td><font>&nbsp;&nbsp;Rules &amp; Comments</font></td>
							</tr>
							<tr>
								<td width="100%">
									<textarea name="txtaRulesCmnts" id="txtaRulesCmnts" cols="45" rows="2"
									onkeyUp="validateTextArea(this);removeAvoidableChar(this)" onkeyPress="validateTextArea(this);removeAvoidableChar(this)" title="Enter text to be displayed in Fare Quote"
									onChange="required_changed()"></textarea></td>
							</tr>
							<tr>
								<td><font>&nbsp;&nbsp;Agent Instructions</font></td>
							</tr>
							<tr>
								<td width="100%">
									<textarea name="txtaInstructions" id="txtaInstructions" cols="45" rows="1"
									onkeyUp="validateTextArea(this);removeAvoidableChar(this)" onkeyPress="validateTextArea(this);removeAvoidableChar(this)" title="Enter Agent Instructions to be displayed in Fare Quote"
									onChange="required_changed()"></textarea></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()" tabindex="66"> 
						<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()" tabindex="67"> 
						<input name="btnBack" type="button" class="Button" id="btnBack" value="Back" onClick="back_click();clearMessages()" tabindex="68">
						<input type="button" id="btnOverWriteRule" name="btnOverWriteRule" class="Button" value="View & Overwrite Fee" style="width: 140px;" onclick="validateSelectedCurrency()">
					</td>
					<td><font>
						<label id=lblUpdateCommentsOnly style="visibility:hidden" title="If ticked won't create new Fares. Only allows updating comments and agent instructions">
							No Fare Split
						</label>
					</font></td>
					<td>
						<input type="checkbox" name="chkUpdateCommentsOnly" id="chkUpdateCommentsOnly" title="If ticked won't create new Fares. Only allows updating comments and agent instructions"
							class="NoBorder" onChange="updateCommentAndDescription()" style="visibility:hidden">
					</td>
					<td align="right">
					<input name="btnLnkAgent" type="button" class="Button" id="btnLnkAgent" value="Link Agents" onclick="CWindowOpen(0)"> 
						<u:hasPrivilege privilegeId="plan.fares.setup.editrule">
							<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit Rule" onClick="edit_ruleClick();clearMessages()" tabindex="69">
					</u:hasPrivilege> 
					<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="save_click();" tabindex="70"></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<%@ include file="../common/IncludeFormBottom.jsp"%>
	</td>
	</tr>
</table>

<input type="hidden" id="hdnMode" name="hdnMode" value="Mode"> 
<input type="hidden" id="cos" name="cos" value=<c:out value="${requestScope.cos}" escapeXml="false" />> 
<input type="hidden" id="hdnVersion" name="hdnVersion" value=""> 
<input type="hidden" id="hdnarrDays" name="hdnarrDays" value=""> 
<input type="hidden" id="hdnFareID" name="hdnFareID" value=""> 
<input type="hidden" id="hdndeptDays" name="hdndeptDays" value=""> 
<input type="hidden" id="hdnVisibility" name="hdnVisibility" value=""> 
<input type="hidden" id="hdnAgents" name="hdnAgents" value=""> 
<input type="hidden" id="hdnFareCode" name="hdnFareCode" value=""> 
<input type="hidden" id="hdnBC" name="hdnBC" value=""> 
<input type="hidden" id="hdnFareData" name="hdnFareData" value=""> 
<input type="hidden" id="hdnFareVersion" name="hdnFareVersion" value="">
<input type="hidden" id="hdnFareRuleVersion" name="hdnFareRuleVersion" value=""> 
<input type="hidden" id="hdnFareRuleID" name="hdnFareRuleID" value=""> 
<input type="hidden" id="hdnFareVisibilityIDs" name="hdnFareVisibilityIDs" value="">
<input type="hidden" id="hdnOND" name="hdnOND" value=""> 
<input type="hidden" id="hdnArrIntDays" name="hdnArrIntDays" value="">
<input type="hidden" id="hdnDeptIntDays" name="hdnDeptIntDays" value="">
<input type="hidden" id="hdnTempMode" name="hdnTempMode" value=""> 
<input type="hidden" id="hdnfromDate" name="hdnfromDate" value=""> 
<input type="hidden" id="hdnSalesfromDate" name="hdnSalesfromDate" value="">
<input type="hidden" id="hdnfromDateInbound" name="hdnfromDateInbound" value="">
<input type="hidden" id="hdnExistingFareRuleVersion" name="hdnExistingFareRuleVersion" value=""> 

<input type="hidden" id="hdnOrigin" name="hdnOrigin" value="<%=session.getAttribute("origin")%>">
<input type="hidden" id="hdnDestination" name="hdnDestination" value="">
<input type="hidden" id="hdnVia1" name="hdnVia1" value=""> 
<input type="hidden" id="hdnVia2" name="hdnVia2" value=""> 
<input type="hidden" id="hdnVia3" name="hdnVia3" value=""> 
<input type="hidden" id="hdnVia4" name="hdnVia4" value=""> 
<input type="hidden" id="hdnGrd1" name="hdnGrd1" value=""> 
<input type="hidden" id="hdnGrd2" name="hdnGrd2" value=""> 
<input type="hidden" id="hdnGrd3" name="hdnGrd3" value=""> 
<input type="hidden" id="hdnVersionAD" name="hdnVersionAD" value="">
<input type="hidden" id="hdnVersionCH" name="hdnVersionCH" value="">
<input type="hidden" id="hdnVersionIN" name="hdnVersionIN" value="">

<input type="hidden" id="hdnFRPAXIDAD" name="hdnFRPAXIDAD" value="">
<input type="hidden" id="hdnFRPAXIDCH" name="hdnFRPAXIDCH" value="">
<input type="hidden" id="hdnFRPAXIDIN" name="hdnFRPAXIDIN" value="">

<input type="hidden" id="hdnADDetails" name="hdnADDetails" value="">
<input type="hidden" id="hdnCHDetails" name="hdnCHDetails" value="">
<input type="hidden" id="hdnINDetails" name="hdnINDetails" value="">
<input type="hidden" id="hdnFlexiID" name="hdnFlexiID" value="">

<input type="hidden" id="hdnLocalCurrencySelected" name="hdnLocalCurrencySelected" value="">
<!--fare rule modifications and load factor  -->
<input type="hidden" id="hdnLoadFactorMax" name="hdnLoadFactorMax" value="">
<input type="hidden" id="hdnLoadFactorMin" name="hdnLoadFactorMin" value="">
<input type="hidden" id="hdnFareDiscountMin" name="hdnFareDiscountMin" value="">
<input type="hidden" id="hdnFareDiscountMax" name="hdnFareDiscountMax" value="">

<!--link fares  -->
<input type="hidden" id="hdnIsMasterFare" name="hdnIsMasterFare" value="">
<input type="hidden" id="hdnMasterFareRefCode" name="hdnMasterFareRefCode" value="">
<input type="hidden" id="hdnMasterFareID" name="hdnMasterFareID" value="">
<input type="hidden" id="hdnMasterFarePercentage" name="hdnMasterFarePercentage" value="">

</form>
<script	src="../../js/fareClasses/CreateFareValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
	<!--
		var objProgressCheck = setInterval("ClearProgressbar()", 300);
		function ClearProgressbar(){
		   	if (objCmb6.loaded && objCmb5.loaded  && objCmb4.loaded && objCmb3.loaded){
		   		clearTimeout(objProgressCheck);
		   		top[2].HideProgress();
		   	}
		}
		    
		//-->
</script>
</body>
</html>