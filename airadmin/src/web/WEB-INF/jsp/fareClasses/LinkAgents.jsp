<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Agent Wise Fare Rules</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/fareClasses/AgentFareValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
			<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript">
//	document.getElementById("frmLinkAgents").reset();
		</script>
	</head>
	<body onkeydown='return Body_onKeyDown(event)' scroll="no" oncontextmenu="return showContextMenu()" onkeypress='return Body_onKeyPress(event)' ondrag='return false'  onLoad="winOnLoad()" >
		<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
			<tr>
			<form action="showLinkAgentMultiDDL.action" name="frmLinkAgents" id="frmLinkAgents" method="post">
				<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:300px;">
						<tr>
							<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								<!-- Your Form start here -->
								<br>
								<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
									<tr>
										<td>
										<%@ include file="../common/IncludeFormTop.jsp"%>
										Link Agents<%@ include file="../common/IncludeFormHD.jsp"%>								
										<table width="100%" border="0" cellpadding="2" cellspacing="0" ID="Table2">
											<tr>
												<td colspan="2">
													<span id="spnMDrop"></span>
												</td>											
											</tr>				
											<tr>
												<td>
													<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="javascript:window.close()">		
												</td>
												<td align="right">
												<input type="button" id="btnUpdate" value="Update" class="Button" onClick="update_Click()">
																											
												</td>
												<input type="hidden" id="hdnLAMode" name="hdnLAMode"  value="Mode" >
												<input type="hidden" id="hdnAgentsAssigned" name="hdnAgentsAssigned"  value="" >
												</form>
											</tr>							
										</table>
										<%@ include file="../common/IncludeFormBottom.jsp"%>
										</td>
									</tr>
								</table>
								<br>
							</td>
							<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
<script type="text/javascript">
<!--
//================================
document.getElementById("frmLinkAgents").reset();
var g1 = new Array("Agents","GSAs");
var g2 = new Array("Agents","GSAs");
<c:out value="${requestScope.reqAgentDDL}" escapeXml="false" />
 
var ls = new Listbox('lstAgents', 'lstAssignedAgents', "spnMDrop");

var assignedAgents= new Array();
assignedAgents[0] = new Array();
	assignedAgents[1] = new Array();
var stat=opener.status;
ls.group1 = g1;
ls.group2 = g2;
ls.list1 = agents;
ls.list2 = assignedAgents;
ls.width = "220px";
ls.height = "260px";
ls.headingLeft = "&nbsp;&nbsp;All Agents";
ls.headingRight = "&nbsp;&nbsp;Assigned Agents";

var inactiveAgents=new Array();
/*
 * Iterate over agents array and put inactive agent code into inactiveAgents
 */
for(var i=0;i<agents.length;i++)
{
	for(var j=0;j<agents[i].length;j++)
	{
		if(agents[i][j][2]=='INA')
		{
			inactiveAgents.push(agents[i][j][1]);
		}
	}
}

ls.drawListBox();

/*
 * Iterate over agents array and put inactive agent code into inactiveAgents
 * Must be done before ls.drawListBox because that method changes the data.
 */
var listBox=document.getElementById('lstAgents');
var k = listBox.options.length - 1;
while ( k >= 0 )
{
  var option = listBox.options[k];
  if(typeof option != 'undefined')
  {
	  var agentCode=option.value.split(":")[1]; //Values are in ABD:ABD14. Second value is the agentCode
	  if(typeof agentCode != 'undefined')
	  {
		  if( inactiveAgents.indexOf(agentCode) > -1 )
		  {
			option.className="classInactive";
			option.disabled="disabled";
		  }
		  else
		  {
			option.className="classActive";  
		  }
	  }
  }
  k--;
}

if(stat=="Edit"){
	var row=opener.rowNo;
	//ls.selectedData(opener.arrData[row][19]);
	
}




//================================


	//-->
		</script>  
	</body>
</html>
