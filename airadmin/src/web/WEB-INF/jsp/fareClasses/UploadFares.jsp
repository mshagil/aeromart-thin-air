<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 	<head>
	    <title>Upload Fares</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
 	</head>
  	<body scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  			onUnload="opener.resetVariables()"  onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
		<%@ include file="../common/IncludeWindowTop.jsp"%>	
		<form method="post"  id="uploadFaresFrom" action="showFares.action" enctype="multipart/form-data">
		<script type="text/javascript">			
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />				
		</script>
		<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
			  
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								
							</td>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>  

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">		
			
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Upload Fares<%@ include file="../common/IncludeFormHD.jsp"%>	
					<br>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						
						
						<tr>
							<td width="30%" align="left"><font>Upload File</font></td>
							<td width="12%">
								<input type="file" name="upload">
							</td>
							<td width="5%">
								
							</td>												
							<td width="15%" align="right">
							</td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="10%" align="left"></td>
							<td width="11%"></td>
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="30%" align="right"><input type="radio" title="Option" id="rndOption1" name="rndOption" value="ADD" class="NoBorder" ></td>
							<td width="12%" align = "left">
								
								<font>Avoid conflicting fares and accommodate others</font>
							</td>
							<td width="5%">
								
							</td>												
							<td width="15%" align="right">
							</td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="10%" align="left"></td>
							<td width="11%"></td>
						</tr>
						<tr>
							<td width="30%" align="right"><input type="radio" title="Option" id="rndOption2" name="rndOption" value="ADD_ALL" class="NoBorder" checked="checked"></td>
							<td width="12%" align = "left">
								
								<font>Accommodate new fares only if there's no conflict</font>
							</td>
							<td width="5%">
								
							</td>												
							<td width="15%" align="right">
							</td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="10%" align="left"></td>
							<td width="11%"></td>
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
				 		
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
						</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="4">									
								<input tabindex="14" type="button" id="btnClose" class="Button" value="Close" onclick="windowclose()">	
								<input tabindex="15" name="btnView" type="button" class="Button" id="btnView" onClick="viewFareUploadStatus()" value="View Status" >
							</td>
							<td align="right" colspan="4">
								<input tabindex="16"  name="btnSave" type="button" class="Button" id="btnSave" onClick="saveFares()" value="Save">
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>	
			<tr><td><font class="fntSmall"></font></td></tr>							
		</table>
		<%@ include file="../common/IncludeWindowBottom.jsp"%>
		
		
		<input type=hidden name="hdnMode" id="hdnMode">
		
		
	</form>	
  </body>
  	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/fareClasses/FareUploadValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/fareClasses/FareUploadStatusPopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	
	<script type="text/javascript">
		<c:out value="${requestScope.reqUploadStatus}" escapeXml="false" />	
		<c:out value="${requestScope.reqUploadStatusMsgs}" escapeXml="false" />	
	</script>
</html>
