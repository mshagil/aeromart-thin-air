<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!--jsp:inlude page="../common/Directives.jsp" /-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View Fare Rules by Agent</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/fareClasses/AgentFareValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<SCRIPT type="text/javascript">
			<c:out value="${requestScope.reqAgentFareGrid}" escapeXml="false" />
    	</SCRIPT>
	</head>
	<body scroll="no" onkeydown='return Body_onKeyDown(event)'  oncontextmenu="return showContextMenu()" onkeypress='return Body_onKeyPress(event)' ondrag='return false' onLoad="winOnLoad()" >
  		<form action="showAgentRules.action" name="frmAgentFC" id="frmAgentFC" method="post">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:300px;">
						<tr>
						
							<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								<!-- Your Form start here -->
								<br>
								<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
									<tr>
										<td>
										<%@ include file="../common/IncludeFormTop.jsp"%>
										View Agent Wise Fare Rules
										<%@ include file="../common/IncludeFormHD.jsp"%>								
										<table width="100%" border="0" cellpadding="2" cellspacing="0" ID="Table2">
											<tr>
												<td width="15%">
													<font>Agent Name</font>
												</td>
												<td width="70%">
													<select id="selAgent" name="selAgent" style="width:200px">
													<option value=""></option>
													<option value="All">All</option>
													<c:out value="${requestScope.reqAgentList}" escapeXml="false" />
													</select>
												</td>
												<td align="right">
													<input type="button" id="btnSearch" name="btnSearch" value="Search" class="Button" onClick="ctrl_TA_Search_click()">
												</td>
											</tr>
											<tr>
												<td colspan="3">
													<span id="spnFC"></span>
													<font>&nbsp;** indicates the relevant fare rule is linked to a fare</font>
												</td>											
											</tr>				
											<tr>
												<td colspan="3" align="right">
													<input type="button" id="btnCancel" value="Close" class="Button" onclick="javascript:window.close()">																
											<input type="hidden" name="hdnMode" id="hdnMode" value="AgentFares">
											<input type="hidden" id="hdnSearchCriteria" name="hdnSearchCriteria"  value="" >
											<input type="hidden" id="hdnRecNo" name="hdnRecNo"  value="" >
												</td>
											</tr>							
										</table>
										</form>
										<%@ include file="../common/IncludeFormBottom.jsp"%>
										</td>
									</tr>
								</table>
								<br>
							</td>
							<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
		<script src="../../js/fareClasses/AgentsFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	</body>
</html>
