<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Seat Charges</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	
	 	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  </head><!--oncontextmenu="return false"-->
  <body oncontextmenu="return false" class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

   <form method="post"  id="formSeatMap" name="formSeatMap" action="showSeat.action">
		<script type="text/javascript">			
			var modelChargeData = new Array();
			var arrFormData = new Array();	
			var totalNoOfRecords= 0;
			totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
			var reqBaseCurr = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml='false' />";
			<c:out value="${requestScope.reqSeatCharges}" escapeXml="false" />					
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
								
		</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>				
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Seat Charge Template - Search<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
				  			
							<tr>
								<td><font>Aircraft Model No:</font></td>
								<td>
									<select name="selModelNo" size="1" id="selModelNo"  tabindex="1">
										<option value="">All</option>																				
										<c:out value="${requestScope.reqFlightModel}" escapeXml="false" />
								  </select>
								</td>
								<td><font>Charge Template</font></td>
								<td>
									<select name="selTemplate" size="1" id="selTemplate"  tabindex="2">
										<option value="">All</option>										
										<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" />
								  </select>
								</td>								
								<td align="right">									
									<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search"  tabindex="2" onclick="searchTemplate()" tabindex="3">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Seat Charge Template<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">					
							<tr>
								<td><span id="spnSeatCharges"></span></td>
							</tr>
							<tr>
								<td>
									<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<u:hasPrivilege privilegeId="mas.seat.charge.add">
													<input type="button" id="btnAdd"  name="btnAdd" class="Button" value="Add" onClick="addClick()" tabIndex="4">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="mas.seat.charge.edit">
													<input type="button" id="btnEdit"  name="btnEdit" class="Button" value="Edit" onClick="editClick()" tabIndex="5">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="mas.seat.charge.cancel">
													<input type="button" id="btnCancel"  name="btnCancel" class="Button" value="Delete" onclick="cancelClick()" tabIndex="6">
												</u:hasPrivilege>												
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>								
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Seats<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
							<tr><td colspan="2"><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td colspan="2">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>																					
											<td><font>Aircraft Model No</font></td>
											<td>
												<select name="selFltModelNo" size="1" id="selFltModelNo"  tabindex="7" style="width:120;">
													<option value=""></option>																				
													<c:out value="${requestScope.reqFlightModel}" escapeXml="false" />
								  				</select>
								  				<font class="mandatory"> &nbsp;* </font>
											</td>
										</tr>										
										<tr>
											<td><font>Template Id</font></td>
											<td><font><span id="spnTemplateId"></span></font></td>
										</tr>
										
										<tr>
											<td width="15%"><font>Template Code</font></td>
											<td>
												<input type="text" id="txtTepmplateCode" name="txtTepmplateCode" maxlength="20"  value="" tabindex="8">
												<font class="mandatory"> &nbsp;* </font>
											</td>											
											
										</tr>
										<tr>
											<td width="15%"><font>Description</font></td>
											<td>
												<input type="text" id="txtDescription" name="txtDescription" maxlength="20"  value=""  tabindex="9">
												<font class="mandatory"> &nbsp;* </font>
											</td>											
										</tr>
										<tr>
											<td width="15%"><font>Default Charge</font></td>
											<td><input type="text" id="txtChargeAmt" name="txtChargeAmt" maxlength="6"  value=""  class="rightText" onKeyUp="validateHandlingChrg()" onKeyPress="validateHandlingChrg()" tabindex="10" onBlur="setDefault()"></td>											
											
										</tr>
										<tr>
											<td width="15%"><font>Status</font></td>
											<td><input name="chkStatus" id="chkStatus" maxlength="1" type="checkbox" value="Active" onChange="clickChange()"></td>											
											
										</tr>										
										
										<tr>
											<td valign="top"><font>User Notes</font></td>
											<td colspan="3"><textarea name="txtRemarks" id="txtRemarks" cols="50" rows="3" onKeyPress="validateTA(this,255)" onKeyUp="validateTA(this,255)" onChange="setPageEdited(true)" tabindex="11"></textarea></td>
																						
										</tr>
										<tr>
										<td><font>Currency Code</font>	</td>
										<td>
										<c:choose>
											<c:when test="${requestScope.reqPrivSeatLoclCUrr == true}">
												<select name="localCurrCode" size="1" id="localCurrCode" >
														<c:out value="${requestScope.reqCurrencyCombo}" escapeXml="false"/>
											    </select><font class="mandatory">&nbsp;*</font> 
											    <label id="lblLocalCurrencyCode" style="display:none;"></label>
											</c:when>
											<c:otherwise>
												<label id="lblLocalCurrencyCode"></label>
												<input type="text" name="localCurrCode" id="localCurrCode"  />
											</c:otherwise>						
										</c:choose>    				
												</td>
										</tr>										
									</table>
								</td>
							</tr>							
							<tr><td><font>&nbsp;</font></td></tr>							
							<tr>
								<td>
									<input type="button" id="btnClose" class="Button" name="btnClose" value="Close"  onclick="closeClick()" tabindex="12">
								</td>
								<td align="right">																		
									<input type="button" id="btnReset" class="Button" name="btnReset" value="Reset"  onclick="resetClick()" tabindex="13">
									<u:hasPrivilege privilegeId="mas.seat.charge.map">
										<input type="button" id="btnSeatMap" class="Button" name="btnSeatMap" value="Seat Map"  onclick="seatMapClick()" tabindex="14">
									</u:hasPrivilege>
									<input type="button" id="btnSave" class="Button" name="btnSave" value="Save"  onclick="saveClick()" tabindex="15">
								</td>
								
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>    			
    	<script src="../../js/seatmap/SeatCharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript">
		<!--
			var objProgressCheck = setInterval("ClearProgressbar()", 300);
			function ClearProgressbar() {
		    	if (objDG.loaded) {
					clearTimeout(objProgressCheck);
					top[2].HideProgress();
				}
			}
	    
	   	//-->
		</script>
		<input type="hidden" name="hdnSeatMap"  id="hdnSeatMap"  value=""/>
		<input type="hidden" name="hdnVersion" id="hdnVersion" value=""/>	
 		<input type="hidden" name="hdnMode"    id="hdnMode"    value="SEARCH"/>
 		<input type="hidden" name="hdnRecNo"   id="hdnRecNo"   value=""/>	
 		<input type="hidden" name="hdnTmplId"  id="hdnTmplId"  value=""/>
 		<input type="hidden" name="hdnModelNo" id="hdnModelNo" value=""/>
 		<input type="hidden" name="hdnAction"  id="hdnAction"  value=""/>
 		<input type="hidden" name="hdnSeatsCharge"  id="hdnSeatsCharge"  value=""/>
 		<input type="hidden" name="hdnChargeAmount"  id="hdnChargeAmount"  value=""/>
 		<input type="hidden" name="hdnIsEditDefaultAmount" id="hdnIsEditDefaultAmount" value=""/>
 					
  </form>		
 </body>
</html>