<%@ page language="java"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Edit Terminals</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
  </head>
  <body onLoad="onLoad()" oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
  <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
  	    <script type="text/javascript">
			<c:out value="${requestScope.reqSegmentValidationData}" escapeXml="false" />	
			<c:out value="${requestScope.reqOverLapSchedDataArray}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />		
		</script>	
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
					<tr>
						<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="8"></td>
						<td valign="top" align="center" class="PageBackGround">
						<!-- Your Form start here -->
							<form method="post" action="showFlight.action">
							<br>
							<table width="100%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
								<tr>
									<td><font class="Header">Terminals Validation for the Flights</font></td>
								</tr>
								<tr>
									<td>
									<%@ include file="../common/IncludeFormTop.jsp"%>Edit Terminals<%@ include file="../common/IncludeFormHD.jsp"%>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td width="48%"><font class="fntBold">Select Terminals for the Flight</font></td>
												<td width="2%"><font>&nbsp;</font></td>
												<td width="48%">
													
												</td>
											</tr>
																				
											<tr>
												<td></td>
												<td><font>&nbsp;</font></td>
												<td>
													
												</td>
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">	
											<tr>	
												<td valign="top"><span id="spnValidSeg"></span></td>
												
											</tr>
										</table>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">		
											<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
											<tr>
												<td colspan="3"><hr></td>
											</tr>
											<tr>
												<td>
													<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="cancelClick()">
												</td>
												<td><font>&nbsp;</font></td>
												<td align="right">&nbsp;
													<input type="button" id="btnConfirm"  name="btnConfirm" value="Confirm" class="Button" onClick="confirmValidTerminal()">
												</td>
											</tr>
										</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
							</table>
					<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
					<input type="hidden" name="hdnLegs" id="hdnLegs"   value="<%=request.getParameter("strLegs")%>"/>
					<input type="hidden" name="hdnLegDtls" id="hdnLegDtls"   value="<%=request.getParameter("strLegDtls")%>"/>
					<input type="hidden" name="hdnOverlapId" id="hdnOverlapId"  value="<%=request.getParameter("strOverlapFltID")%>"/>
					<input type="hidden" name="hdnSegNOFlag" id="hdnSegNOFlag"   value="<%=request.getParameter("strSaveMode")%>"/>
					<input type="hidden" name="hdnOverlapFlightId" id="hdnOverlapFlightId"  value="<%=request.getParameter("strOverlapFltID")%>"/>
					</form>
						</td>
					</tr>
				</table>						
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
  	<script src="../../js/flight/ValidateFlightTerminals.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>
