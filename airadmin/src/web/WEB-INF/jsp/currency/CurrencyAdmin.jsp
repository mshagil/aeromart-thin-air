 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/currency/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
  </head>
  <body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  	<form name="frmPage" id="frmPage" action="showCurrency.action" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
		<script type="text/javascript">
			var arrFormData = new Array();
			var orgData = new Array();
			var isSaveTransactionSuccessful = false;
			var blnPayble = false;
			var strBaseCurrencyCode ="<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
			var showItineraryFareBreakDown ="<c:out value="${requestScope.showItineraryFareBreakDownOption}" escapeXml="false" />";
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
			var currBoundary = "<c:out value="${requestScope.currencyBoundary}" escapeXml="false" />";
			var engineVisiblity = "<c:out value="${requestScope.engineVisibility}" escapeXml="false" />";
			var sysDate = "<c:out value="${requestScope.reqSysDate}" escapeXml="false" />";
			var exRateAutomationStatus = "<c:out value="${requestScope.exRateAutomationEnabled}" escapeXml="false" />";
			<c:out value="${requestScope.rqPayenable}" escapeXml="false" />
		</script>
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3"><%@ include file="../common/IncludeFormTop.jsp"%>Search Currency<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
						<tr>
							<td width="10%"><font>Currency Code</font></td>								
							<td width="9%"><font>Description</font></td>
							<td width="10%"><font>Status</font></td>
							<td width="15%"></td>
							<td width="8%"><font>From Date</font></td>
							<td width="2%"><font>&nbsp;</font></td>
							<td width="8%"><font>To Date</font></td>
							<td width="2%"><font>&nbsp;</font></td>
							<td width="10%" align="right" rowspan="2">
								<input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchCurrency()" value="Search" tabindex="9">
							</td>
						</tr>
						<tr>
							<td width="15%"><input name="txtCurrencyCodeSearch" type="text" id="txtCurrencyCodeSearch" style="width:75px;" class="UCase" maxlength="3" onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)" tabindex="1"></td>
							<td width="20%" align="left"><input name="txtDescriptionSearch" type="text" id="txtDescriptionSearch" size="20"  maxlength="30" onkeyUp="CDSPress()" onkeyPress="CDSPress()" tabindex="2"></td>
							<td>
								<select id="selStatus" name="selStatus" tabindex="3">
									<option value="">All</option>
									<c:out value="${requestScope.reqCurrStatus}" escapeXml="false" />
								</select>
							</td>
							<td>
								<input type="checkbox" id="radRateCat" name="radRateCat"   class="NoBorder"  value="on" onClick="setWithoutCharges()" tabindex="4"><font>Effective During</font>
							</td>
							<td><input type="text" id="txtDateFrom" name="txtDateFrom" style="width:75px;" maxlength="12" onblur="dateChk('txtDateFrom')"  invalidText="true" tabindex="5"></td>
							<td>
								<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0" tabindex="6"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td>
								<input type="text" id="txtDateTo" name="txtDateTo" style="width:75px;" maxlength="12" onblur="dateChk('txtDateTo')" invalidText="true" tabindex="7">
							</td>
							<td>
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0" tabindex="8"></a>
							</td>							
						</tr>
					</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td colspan="3" valign="top">
						
						<%@ include file="../common/IncludeFormTop.jsp"%>Currencies<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					  		<!-- tr><td><font class="fntTiny">&nbsp;</font></td></tr -->
							<tr>
								<td colspan="2">
									<span id="spnCurrencies"></span> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.currency.add">
										<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()" tabindex="10">
									</u:hasPrivilege>	
									<u:hasPrivilege privilegeId="sys.mas.currency.edit">	
										<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()" tabindex="11">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.currency.fordisp">	
										<input type="button" value= "Add Description For Display" class="ButtonLong" onClick="return POSenable();" id="btnCurrencyFD" style="width:175px;">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.currency.delete">
										<input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete" onClick="deleteClick()" tabindex="12">
									</u:hasPrivilege>	
								</td>
								<td><font class="mandatory" ><span id="spnClock"></span> </font></td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
				
				<tr>
					<td valign="top">
						<%@ include file="../common/IncludeFormTop.jsp"%>Add/Modify Currency
						<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="98%" border="0" cellpadding="0" cellspacing="4" align="center" ID="Table1">
							<tr>
								<td width="20%"><font>Currency Code</font></td>
								<td><input name="txtCurrencyCode" type="text" id="txtCurrencyCode" style="width:75px;" class="UCase" maxlength="3" onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)" onChange="clickChange()" tabindex="13"><font class="mandatory">&nbsp;*</font></td>
							</tr>
							<tr>
								<td><font>Description</font></td>
								<td><input name="txtDescription" type="text" id="txtDescription" size="32"  maxlength="30" onkeyUp="CDPress()" onkeyPress="CDPress()" onChange="clickChange()" tabindex="14"><font class="mandatory" >&nbsp;*</font>
								</td>
							</tr>		
							<tr>
								<td><font>Visibility</font></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
										<tr>
										<td width="5%"><input type="checkbox" name= "chkIBEVisibililty" id="chkIBEVisibililty" value="Y" 
																		onClick="clickChange()" tabindex="15" class="NoBorder"></td>
										<td width="10%"><font>IBE</font></td>
										
										<td width="5%"><input type="checkbox" name= "chkXBEVisibililty" id="chkXBEVisibililty" value="Y" 
															onClick="clickChange()" tabindex="16" class="NoBorder"></td>
										<td width="10%"><font>XBE</font></td>
										
										<td width="5%"><input type="checkbox" name="chkCardPayVisibililty" id="chkCardPayVisibililty" value="Y" 
															onClick="clickCreditChange()" tabindex="17" class="NoBorder"></td>
										<td><font>Card Pay</font></td>
										
									</tr>
								</table>
							</td>
						</tr>		
						
						<tr>
								<td><font>PG for IBE</font></td>
								<td>
									<select name="selPaymentGatewayIbe" id="selPaymentGatewayIbe" size="1" name="selPaymentGatewayIbe" style="width:200px" tabindex="1" maxlength="200"  title="Payment Gateways">
									<OPTION value="" selected></OPTION>	
									<c:out value="${requestScope.ibePaymentGatewayList}" escapeXml="false" />
								</td>
						</tr>
						
						<tr>
								<td><font>PG for XBE</font></td>
								<td>
									<select name="selPaymentGatewayXbe" id="selPaymentGatewayXbe" size="1" name="selPaymentGatewayXbe" style="width:200px" tabindex="1" maxlength="200"  title="Payment Gateways">
									<OPTION value="" selected></OPTION>	
									<c:out value="${requestScope.xbePaymentGatewayList}" escapeXml="false" />
								</td>
						</tr>
						
						<tr>
								<td><font>Decimal Places</font></td>
								<td><input name="txtDecimal" type="text" id="txtDecimal" size="10"  maxlength="30" onkeyUp="CDPress()" onkeyPress="CDPress()" onChange="clickChange()" tabindex="19">
								</td>
						</tr>
						<tr>
							<c:if test="${requestScope.showItineraryFareBreakDownOption == true }">
								<td><font>Show total payment in base currency</font></td>
								<td><input type="checkbox" name= "chkItineraryFareBreakDownEnalbed" id="chkItineraryFareBreakDownEnalbed"  onClick="clickChange()" tabindex="20" class="NoBorder"></td>						
	   	 					</c:if>
						</tr>				
						<tr>
							<td><font>Active</font></td>
							<td><input type="checkbox" name= "chkStatus" id="chkStatus"  onClick="clickChange()" tabindex="18" class="NoBorder"></td>										
						</tr>
						<tr>
							<td colspan="2"><span id="spnRound"></span></td>
						</tr>
					</table>
					<div id="exRateAutomateConfigDiv" style="display:none" >
					<fieldset>
					<legend><font>Exchange Rate Auto Update Configuration</font></legend>
					<table>
						<tr>
							<td><font>Enabled &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
							<td align="left"><input type="checkbox" name= "exrateAutoUpdateStatus" id="exrateAutoUpdateStatus"  onClick="clickChange()" tabindex="18" class="NoBorder" disabled="disabled"></td>										
						</tr>
						<tr>
								<td><font>Variance   </font></td>
								<td align="left"><input name="exrateVariance" type="text" id="exrateVariance" style="width:75px;" class="UCase" maxlength="6" onkeyUp="KPValidateDecimal(this)" onkeyPress="KPValidateDecimal(this)" onChange="clickChange()" tabindex="13" disabled="disabled"></td>
						</tr>
					</table>
					</fieldset>
					</div>
					<%@ include file="../common/IncludeFormBottom.jsp"%>	
				</td>
				<td width="1%"><font>&nbsp;</font></td>
				<td width="50%" valign="top">
					<table width="100%" border="0" cellpadding="0" cellspacing="2" align="center">
						<tr>
							<td>
								<%@ include file="../common/IncludeFormTop.jsp"%>Add/Modify Exchange Rates 
								<%@ include file="../common/IncludeFormHD.jsp"%>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td style="height:100px;">
											<span id="spnExRate"></span> 
										</td>
									</tr>									
									<tr>
										<td>
											<input type="button" id="btnExRateAdd" name="btnExRateAdd" value="Add" class="Button" onClick="addExRateRow()" tabindex="21">
											<input type="button" id="btnExRateEdit" name="btnExRateEdit" value="Edit" class="Button" onClick="editExRateRow()" tabindex="22">
											<input type="button" id="btnExRateSplit" name="btnExRateSplit" value="Split" class="Button" onClick="splitExRateRow()" tabindex="23">
										</td>
										
									</tr>
								</table><%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
							
						<tr>
							<td>
								<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>			
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td valign="bottom">
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" tabindex="24">
											<input name="btnReset" type="button" class="Button" id="btnReset" onClick="resetClick()" value="Reset" tabindex="25">
										</td>
										<td align="right" valign="bottom">
											<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveCurrency()" value="Save" tabindex="26">
  										</td>
									</tr>
								</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
					</table>
				</td>				
			</tr>				
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnExRateData" id="hdnExRateData" value="">
		<input type="hidden" name="hdnExRateGrid" id="hdnExRateGrid" value="">
		<input type="hidden" name="hdnCurrencyForDisplay" id="hdnCurrencyForDisplay" value="">
		
	</form>
  </body>
  <script type="text/javascript">
    	 var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
  		var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;
  		
  </script>
  
  <script src="../../js/currency/CurrencyValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  <script src="../../js/currency/CurrencyAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  <script src="../../js/currency/CurrencyExRateGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);  	
  	function ClearProgressbar(){
  		
  			if (typeof(objDG) == "object"){
		  		if (objDG.loaded){
		  			clearTimeout(objProgressCheck);					
					for(var ol=0;ol<orgData.length;ol++) {						
						if (arrExRateData[ol][4] == 'ACT' || arrExRateData[ol][4] == true) {
							arrExRateData[ol][4] = true;
							objDGExr.setCellValue(ol,4,"true");	
						} else {
							arrExRateData[ol][4] = false;
							objDGExr.setCellValue(ol,4,"false");
						}
					}
		  			top[2].HideProgress();
		  		}
		  	}
	  	
  	}
  	 
  	//-->
  </script>

</html>
