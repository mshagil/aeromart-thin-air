<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Top Segments</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadTS()" class="tabBGColor" onkeypress='return Body_onKeyPress(event)'
	onbeforeunload="beforeUnload()" oncontextmenu="return false"
	onkeydown="return Body_onKeyDown(event)" scroll="no">
<form action="" method="post" id="frmTopSegments"
	name="frmModeofPayments">
<table width="99%" align="center" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Top
		Segments<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">
			<tr>
				<td><font class="fntBold">Date Range</font></td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="6%" align="left"><font>From </font><input
							name="txtFromDate" type="text" id="txtFromDate" size="10"
							style="width: 75px;" maxlength="10"
							onblur="dateChk('txtFromDate')" invalidText="true"></td>
						<td width="5%"><a href="javascript:void(0)"
							onclick="LoadCalendar(0,event); return false;" title="Date From"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font
							class="mandatory"><b>*</b></font></td>
						<td width="5%"><font>To </font><input name="txtToDate"
							type="text" id="txtToDate" size="10" style="width: 75px;"
							maxlength="10" onblur="dateChk('txtToDate')" invalidText="true"></td>
						<td width="29%"><a href="javascript:void(0)"
							onclick="LoadCalendar(1,event); return false;" title="Date To"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font
							class="mandatory"><b>*</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td><font class="fntBold">Sales Channels</font></td>
					</tr>
					<tr>
						<td>
						<table width="70%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td><font> </font>
									<select multiple="multiple" size="5" style="height: 118px" name= "salesChanels" id="salesChanels" >
										<c:out value="${reqSalesChannel}" escapeXml='false' />
									</select><font
							class="mandatory"><b>*</b></font>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>
				<table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td><font class="fntBold">No. of Top Segments</font></td>
					</tr>
					<tr>
						<td>
						<table width="70%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="20%"><input type="radio" name="radNoTopAgents"
									id="radNoTopAgents" value="5" onClick="chkClick()"
									class="noBorder" checked><font>5</font></td>
								<td width="20%"><input type="radio" name="radNoTopAgents"
									id="radNoTopAgents10" value="10" onClick="chkClick()"
									class="noBorder"><font>10</font></td>
								<td width="20%"><input type="radio" name="radNoTopAgents"
									id="radNoTopAgents15" value="15" onClick="chkClick()"
									class="noBorder"><font>15</font></td>
								<td width="20%"><input type="radio" name="radNoTopAgents"
									id="radNoTopAgentsOther" value="other" onClick="chkClick()"
									class="noBorder"><font>Other</font></td>
								<td><input name="txtTopAgentsOther" type="text"
									id="txtTopAgentsOther" size="3" style="width: 75px;"
									maxlength="3"
									onKeyUp="KPValidatePositiveInteger('txtTopAgentsOther')"
									onKeyPress="KPValidatePositiveInteger('txtTopAgentsOther')"
									class="rightText"></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><font class="fntBold">Output Option</font></td>
			</tr>
			<tr>
				<td>
				<table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="20%"><input type="radio" name="radReportOption"
							id="radReportOption" value="HTML" class="noBorder" checked><font>HTML</font></td>
						<td width="20%"><input type="radio" name="radReportOption"
							id="radReportOption" value="PDF" class="noBorder"><font>PDF</font></td>
						<td width="20%"><input type="radio" name="radReportOption"
							id="radReportOption" value="EXCEL" class="noBorder"><font>EXCEL</font></td>
						<td width="40%"><input type="radio" name="radReportOption"
							id="radReportOption" value="CSV" class="noBorder"><font>CSV</font></td>
					</tr>
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="30%"><input type="button" id="btnClose"
							name="btnClose" value="Close" class="Button"
							onclick="closeClick()"></td>
						<td width="70%" align="right"><input type="button"
							id="btnView" name="btnView" value="View" class="Button"
							onclick="viewClick()"></td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode"> <input
	type="hidden" name="hdnLive" id="hdnLive" value=""></form>
</body>
<script
	src="../../js/reports/TopSegments.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>