<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Credit Card Transaction Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />

<script	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" 	type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
<!--For Calendar-->
</head>
<body onload="pageLoad()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<form action="" method="post" id="frmTransactionReport"
	name="frmTransactionReport">
<table width="99%" align="center" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Credit
		Card Transaction Report<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">
			<tr>
				<td><font class="fntBold">Filter By Date</font></td>
			</tr>
			<tr>
				<td width="100%">
				<table width="80%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="9%" align="left"><font>From&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
						<td width="10%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width: 75px;" maxlength="10" onblur="dateChk('txtFromDate')"></td>
						<td width="10%">&nbsp;&nbsp;<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
						<td width="20%"><font>&nbsp;To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input name="txtToDate" type="text" id="txtToDate" size="10" style="width: 75px;" maxlength="10" onblur="dateChk('txtToDate')"></td>
						<td width="15%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
						<td width="35%"></td>						
					</tr>
				</table>
				</td>
			</tr>
			<tr>
			    <td>
			    	<table width="45%">
						<td class="noBorder"><b>Entity &nbsp;</b></td>
    					<td>
							<select name="selEntity" id="selEntity" size="1" style="width:90px" onChange="objOnFocus()">
								<c:out value="${requestScope.reqEntities}" escapeXml="false" />
							</select>
    					</td>
    				</table>
				</td>
			</tr>			
			<tr>
				<td>
				<table width="45%">
					<tr>
						<td class="noBorder"><b>Report Options</b></td>
					</tr>
					<tr>
						<th scope="col">
						<table width="100%" border="0">
							<u:hasPrivilege privilegeId="rpt.cc.txn.charge">
								<tr>
									<td width="10%"><input type="radio" name="ccReportName" id="ccReportName" value="CC_TRAN" class="noBorder" checked></td>
									<td width="90%" class="noBorder">Credit Card Transactions Report</td>
								</tr>
							</u:hasPrivilege>
							<u:hasPrivilege privilegeId="rpt.cc.txn.refund">
								<tr>
									<td width="10%"><input type="radio" name="ccReportName" id="ccReportName" value="CC_REFUND" class="noBorder"></td>
									<td width="85%" class="noBorder">Credit Card Transactions Refund Report</td>
								</tr>
							</u:hasPrivilege>
							<u:hasPrivilege privilegeId="rpt.cc.txn.refund.pending">
								<tr>
									<td width="15%"><input type="radio" name="ccReportName" id="ccReportName" value="CC_PEND_REFUND" class="noBorder"></td>
									<td width="85%" class="noBorder">Credit Card Transactions Pending Refunds Report</td>
								</tr>
							</u:hasPrivilege>
							<u:hasPrivilege privilegeId="rpt.cc.txn.topup">
								<tr>
									<td width="15%"><input type="radio" name="ccReportName" id="ccReportName" value="CC_TOPUP" class="noBorder"></td>
									<td width="85%" class="noBorder">Credit Card Transactions Self TopUp Report</td>
								</tr>
							</u:hasPrivilege>
						</table>
						</th>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td><font class="fntBold">Output Option</font></td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" value="HTML" class="noBorder" checked><font>HTML</font></td>
						<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" value="PDF" class="noBorder"><font>PDF</font></td>
						<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" value="EXCEL" class="noBorder"><font>EXCEL</font></td>
						<td><input type="radio" name="radReportOption" id="radReportOption" value="CSV" class="noBorder"><font>CSV</font></td>
					</tr>
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width=20%><input type="button" id="btnClose" name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
			<u:hasPrivilege privilegeId="rpt.sch.cctransaction">
				<td align="right">
				<div id="divSchedDetailFrom"></div>
				<input name="btnSchedDetail" type="button" class="Button" id="btnSchedDetail" 
				value="Schedule Detail" onClick="detailViewClick(true)" style="width: 115px"></td>
									
				<td align="right" width=20% >
				<div id="divSchedFrom"></div>
				<input style="height:23px; width:115px" name="btnSched" type="button" class="Button" id="btnSched"
					value="Schedule Summary" onClick="viewClick(true)"></td>
				</u:hasPrivilege>	
				<td width=50%><input type="button" id="btnDetailView" name="btnDetailView" value="DetailView" class="Button" onclick="detailViewClick(false)"></td>
				<td width=10% align="right"><input type="button" id="btnView" name="btnView" value="View" class="Button" onclick="viewClick(false)"></td>
			</tr>
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode"> 
<input type="hidden" name="hdnLive" id="hdnLive" value="">
<input type="hidden" name="hdnDetail" id="hdnDetail"> 
<input type="hidden" name="hdnEntityText" id="hdnEntityText" value="">
</form>
</body>
<script
	src="../../js/reports/TransactionReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
</script>
</html>