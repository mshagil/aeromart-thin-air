 <%-- 
	 @Author 	: 	Dhanushka Ranatunga
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	
</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
  	<form name="frmAgentGsaDetails" id="frmAgentGsaDetails" a method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Agent User Privileges<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  								
							<tr>							
								<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
								<tr>
								<td>
									<table width="50%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<font class="fntBold">Agencies</font>
												<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
												<option value=""></option>
												<option value="All">All</option>
												<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
											</select>
										</td>
										<td>
											<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
										</td>
										<td>
											<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"><font class="mandatory"><b>&nbsp;*</b></font> 
										</td>
									</tr>
									<tr>
										<td><font>&nbsp;</font></td>
										<td>
											<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
										</td>												
									</tr>
								</table>
							 </td>
							</tr>							
							<tr>
								<td>
									<font class="fntBold">Agents</font>
								</td>
							</tr>
							<tr>
								<td valign="top"><span id="spn1"></span></td>
								<td valign="bottom" width="3px"><font class="mandatory"><b>&nbsp;*</b></font></td>
							</tr>
							<tr>
								<td align="right" valign="bottom" height="30px">
									<input name="btnLoadUsers" type="button" class="Button" id="btnLoadUsers" value=" Load Users " onClick="loadUsersClick()">
								</td>
							</tr>
							<tr>
								<td valign="top"><span id="spn2"></span></td>
								<td valign="bottom" width="3px"><font class="mandatory"><b>&nbsp;*</b></font></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<!--<tr>
								<td>
									<font class="fntBold">Roles</font>
								</td>
							</tr>  -->
							
							<tr>
								<td align="right" valign="bottom" height="30px">
									<input name="btnLoadRoles" type="button" class="Button" id="btnLoadRoles" value=" Load Roles " onClick="loadRolesClick()">
								</td>
							</tr>
							<tr>
								<td valign="top"><span id="spn3"></span></td>
								<td valign="bottom" width="3px"><font class="mandatory"><b>&nbsp;*</b></font></td>
								<!-- <td>&nbsp;</td>  -->
							</tr>
							
							<tr>
								<td align="right" valign="bottom" height="30px">
									<input name="btnLoadPrivilages" type="button" class="Button" id="btnLoadPrivilages" value=" Load Privileges " onClick="loadPrivilegesClick()" style="width:110px;">
								</td>
							</tr>
							<tr>
								<td valign="top"><span id="spn4"></span></td>
								<td>&nbsp;</td>
							</tr>		
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>				
							<td><font class="fntBold">Output Options</font></td>							
							</tr>
							<tr>				
								<td>
									<table width="" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td>
												<input type="radio" name="chkOption" id="chkOption" value="UserListSummary"  class="noBorder" onClick="radioReportChanged()" checked><font>Print User List&nbsp;&nbsp;&nbsp;</font>
											</td>
											<td>
												<input type="radio" name="chkOption" id="chkOption1" value="UserListDetails" class="noBorder" onClick="radioReportChanged()"><font>Print User List With Privileges&nbsp;&nbsp;&nbsp;</font>
											</td>
											<td>
												<input type="radio" name="chkOption" id="chkOption2" value="PrivilegeDetails" class="noBorder" onClick="radioReportChanged()"><font>Privilege Details&nbsp;&nbsp;&nbsp;</font>
											</td>
											<td>
												<input type="radio" name="chkOption" id="chkOption3" value="UserPrivilegeDetails" class="noBorder" onClick="radioReportChanged()"><font>Print User List With Privilege Detail</font>
											</td>
										</tr>
									</table>
								</td>									
							</tr>
							<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV"  class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		
					</td>
				</tr>
				
			</table>
	<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />
				<c:out value="${requestScope.reqRoleList}" escapeXml="false" />
				<c:out value="${requestScope.reqRolePrivilegeList}" escapeXml="false" />
				
		</script>
<%@ include file="../common/IncludeFormBottom.jsp"%>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnUsers" id="hdnUsers" value="">
		<input type="hidden" name="hdnRoles" id="hdnRoles" value="">
		<input type="hidden" name="hdnPrivileges" id="hdnPrivileges" value="">			
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnPrivIndexFlag" id="hdnPrivIndexFlag" value="">

	</form>
  </body>
	<script src="../../js/reports/AgentUserPrivilegesReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
    }
    
   //-->
  </script>
</html>

