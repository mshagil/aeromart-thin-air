 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		var Maxcontime = "<c:out value="${requestScope.reqMaxConTime}" escapeXml="false" />";
	</script>
  </head>
	<body class="tabBGColor" onLoad="pageLoadOB()" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
  		<form name="frmInbOutConnections" id="frmInbOutConnections" action="showInBoundOutBoundConnection.action" method="post">
			<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td>
							<%@ include file="../common/IncludeMandatoryText.jsp"%>
						</td>
					</tr>
					<tr>
						<td>
							<%@ include file="../common/IncludeFormTop.jsp"%>Inbound Outbound Connections<%@ include file="../common/IncludeFormHD.jsp"%>
				  			<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  			<tr>
					  				<td><font class="fntSmall">&nbsp;</font></td>
					  			</tr>						
								<tr><td><font>&nbsp;</font></td></tr>
							    <tr>
									<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">
											<tr>
												<td>
													<table width="70%" border="0" cellpadding="0" cellspacing="2">
														<tr>
															<td width="40%" colspan="2"><font class="fntBold">Date Range</font></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table width="100%" border="0" cellpadding="0" cellspacing="2">
														<tr>
															<td width="25%"><font>From </font>
																<input type="text" name="txtFromDate" id="txtFromDate" size="10" onBlur="dateChk('txtFromDate')" invalidText="true" maxlength="10">
																	<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"><b>*</b></font></td>
																	<td ><font>To </font>
																<input type="text" name="txtToDate" id="txtToDate" maxlength="10"  onBlur="dateChk('txtToDate')" invalidText="true" size="10">
																	<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"><b>*</b></font></td>
														</tr>
													</table>
												</td>	
											</tr>
											<tr><td><font class="fntSmall">&nbsp;</font></td></tr>								
											<tr>
												<td>
													<table width="70%" border="0" cellpadding="0" cellspacing="2">
														<tr>
															<td width="20%"><font>Connection Time </font></td>
															<td><input type="text" name="txtMaxConTime" id="txtMaxConTime"  align="right" size="6" maxlength="2" onKeyPress='validateDecimalAmount(this)' onKeyUp='validateDecimalAmount(this)'><font>&nbsp;in Hours</font><font class="mandatory">*</font></td>					
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
													<tr>
													<td width="20%"><font>Flight Number </font></td>
													<td><input type="text" name="FlightNo" id="FlightNo" class="UCase" size="7" maxlength="7"></td>					
													</tr>
													</table>
												</td>	
											</tr>	
											<tr>
												<td>
													<table width="70%" border="0" cellpadding="0" cellspacing="2">
														<tr>
															<td valign="top"><span id="spn1"></span>
															</td>
															<td valign="bottom"><font class="mandatory"><b>*</b></font></td>
														</tr>
													</table>												
												</td>
											</tr>
											<tr>
												<td>
													<table width="70%" border="0" cellpadding="0" cellspacing="2">
														<tr>
															<td><input type="radio" name="radOpt" id="radOpt" class="noBorder" value="I" checked><font>InBound</font></td>			
															<td><input type="radio" name="radOpt" id="radOptInbound" class="noBorder" value="O"><font>OutBound</font></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
											<tr>				
												<td><font class="fntBold">Output Option</font></td>							
											</tr>
											<tr>
												<td><table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
													value="HTML"  class="noBorder" checked><font>HTML</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
													value="PDF"  class="noBorder"><font>PDF</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
												<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
													value="CSV"  class="noBorder"><font>CSV</font></td>
											</tr>						
										</table>
										<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
									</td>									
								</tr>
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
								<tr>
									<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">
											<tr>
												<td align="left"><input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()"></td>
												<td align="right">
													<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>	
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	</table>	
	<%@ include file="../common/IncludeFormBottom.jsp"%>	
	<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
	<input type="hidden" name="hdnCountries" id="hdnCountries" value="">
	<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
	<input type="hidden" name="hdnLive" id="hdnLive" value="">
	<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
	</script>
	</form>
  </body>
	<script src="../../js/reports/InboundOutBoundConnectionsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
       clearTimeout(objProgressCheck);
      top[2].HideProgress();
 
   }
    
   //-->
  </script>
</html>

