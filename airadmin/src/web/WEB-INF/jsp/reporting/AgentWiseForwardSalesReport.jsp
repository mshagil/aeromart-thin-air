<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Agent wise Forward Sales Report</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</head>
<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	var displayAgencyMode = 0;
	<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />		

</script>
<body class="tabBGColor" style="align:left" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" >
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmAgentWiseForwardSalesReport" id="frmAgentWiseForwardSalesReport" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Agent wise Forward Sales Report<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>			
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font class="fntBold">From Sales Date </font></td>
										<td width="55%"><input type="text" name="txtFromDate" id="txtFromDate" size="10" maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"><font class="mandatory"> &nbsp;* </font></a></td>			
										<td width="20%"></td>	
										<td width="20%"></td>	
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font class="fntBold">To Sales Date </font></td>
										<td width="55%"><input type="text" name="txtToDate" id="txtToDate" maxlength="10" size="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>					
										
										<td width="20%"></td>
										<td width="20%"></td> 
										</tr>
										</table>
									</td>	
								</tr>											
								<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="2">
											<tr>
											<td width = 15% ><font class="fntBold">Country</font></td>
											<td width="20%">
												<select name="selCountry" size="1" id="selCountry" style="width:55px;">
													<option value=""></option>		
													<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
												</select>
											</td>
											<td width = 15%><font class="fntBold">Stations</font></td>
											<td width = 50%>	
												<select name="selStations" size="1" id="selStations" style="width:55px;">
												<option value=""></option>		
												<c:out value="${requestScope.reqStationList}" escapeXml="false" />
												</select>
											</td>
											<td></td>
											</tr>									
										</table>
									</td>									
								</tr>
								<tr><td>
									<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td>
										<div id="divAgencies">
											<table width="50%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td>
														<font class="fntBold">Agencies</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														&nbsp;&nbsp;&nbsp;
															<select id="selAgencies" size="1" name="selAgencies" style="width:76px">
															<option value=""></option>
															<option value="All">All</option>
															<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
														</select>
													</td>												
													<td>
														<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
														<font class="mandatory"><b>&nbsp;*</b></font> 
													</td>
												</tr>											
											</table>
										</div>
								 	</td>
								</tr>
								<tr>
									<td>
										<div id="divAgents">
											<table>
												<tr>
													<td>
														<font class="fntBold">Agents</font>
													</td>
												</tr>
												<tr>
													<td valign="top"><span id="spn1"></span></td>								
												</tr>
											</table>
										</div>
									</td>
									</tr>										
								</table>
								</td>
								</tr>
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							
							<tr>
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
									<td width="80%">
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">										
									</td>
									<td width="20%">
										<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
									</td>
								</table>
								</td>
							</tr>
						</table>
				  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}	
				
		</script>	
				<%@ include file="../common/IncludeFormBottom.jsp"%>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>		
</body>		
<script src="../../js/reports/AgentWiseForwardSalesReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript">
</script>
</html>
