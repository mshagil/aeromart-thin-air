<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Seat Inventory Collections</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />	
	</script>
<!--For Calendar-->
</head>
	<body onload="pageLoad()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
		<form action="" method="post" id="frmViewSeatInventoryCollections"
		name="frmViewSeatInventoryCollections">
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Seat Inventory Collections<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"	ID="tblHead">
						<tr>
							<td>		
									<table width="100%"><tbody>
											<tr>							
											<td style="width: 100px;"><font class="fntBold">Report Type</font>
											</td>
												<td align="left" width="" colspan="2">	
											 		<select name="selReportType" size="1" id="selReportType" style="width: auto;" onchange="reportTypeOnChage()">
														<option value=""></option>
														<c:out value="${requestScope.reqReportTypeList}" escapeXml="false" />								
													</select>
													<font class="mandatory">&nbsp*</font>
												</td>					
										
										</tr>
										<tr>	<td colspan="3">
													<hr>
												</td>
										
										</tr>
									</tbody></table>
							</td>
						</tr>
						<tr>
							<td>
								<table border="0" cellpadding="0" cellspacing="0" id="tblBookedDateOpt" width="60%">	
									<tr>
										<td width="29%"><font class="fntBold">Booked Date</font></td>						
									
										<td width="22%" align="left"><font>From&nbsp;</font>
											<input name="txtBookedFromDate" type="text" id="txtBookedFromDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtBookedFromDate')" invalidText="true">
										</td>
										<td width="9%" align="left">
											<a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>
										</td>
										<td width="20%"><font>&nbsp;&nbsp;&nbsp;To&nbsp;</font>
											<input name="txtBookedToDate" type="text" id="txtBookedToDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtBookedToDate')" invalidText="true">
										</td>
										<td width="8%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table border="0" cellpadding="0" cellspacing="0" width="60%">	
									<tr>
										<td width="29%"><font class="fntBold">Departure Date</font></td>	
									
										<td width="22%" align="left"><font>From&nbsp;&nbsp;</font><input
											name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')">
										</td>
										<td width="9%" align="left">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>
											<font class="mandatory">&nbsp*</font>
										</td>
										<td width="20%"><font>&nbsp;&nbsp;&nbsp;To&nbsp;&nbsp;</font><input
											name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')">
										</td>
										<td width="8%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>
											<font class="mandatory">&nbsp*</font>										
										</td>
									</tr>
								</table>
							</td>
						</tr>			
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>				
										<td width="7%"><font>Departure</font></td><td width="10%"><select id="selDept" size="1" name="selDept" style="width:85px" tabindex="1" maxlength="10">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     	</select> 
									     </td>	<td><font id="departureMandatory" style="display:none;" class="mandatory">&nbsp*</font></td>								      
									     
										<td width="7%"><font>Arrival&nbsp;&nbsp;</font></td><td width="10%"><select id="selArival" size="1" name="selArival" style="width:85px" tabindex="1" maxlength="10">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     	</select>									     	
									    </td> <td><font id="arrivalMandatory" style="display:none;" class="mandatory">&nbsp*</font></td>
									    
									    			
										<td width="12%"><font>Via 1&nbsp;</font><select id="selVia1" size="1" name="selVia1" style="width:60px" tabindex="1" maxlength="5">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     	</select>
									    </td>	
									     
										<td width="12%"><font>Via 2&nbsp;</font><select id="selVia2" size="1" name="selVia2" style="width:60px" tabindex="1" maxlength="5">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     	</select>
									   </td>
										<td width="12%"><font>Via 3&nbsp;</font><select id="selVia3" size="1" name="selVia3" style="width:60px" tabindex="1" maxlength="5">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
											</select>
										</td>	
									     
										<td width="12%"><font>Via 4&nbsp;</font><select id="selVia4" size="1" name="selVia4" style="width:60px" tabindex="1" maxlength="5">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
											</select>
										</td>
									</tr>
								</table>
							</tr>			
							<tr>			
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="2">				
										<tr>
											<td  width="52%"><font>Flight No&nbsp;&nbsp;&nbsp;&nbsp; </font><input name="txtFlightNo" type="text" id="txtFlightNo" size="10" style="width:75px;" maxlength="7"> <input type="checkbox" name="chkExactFN" id="chkExactFN" value="on" checked="checked" class="noBorder" align="left"><font>Exact match</font></td>
										</tr>				
										<tr>
				
											<td align="left" width="20%"><font>Operation Type&nbsp;&nbsp; </font><select name="selOperationType" id="selOperationType" size="1" style="width:120px" title="Operation Type">																					
												<option value="All">All</option>
												<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
												</select>
											</td>	
										</tr>
										<tr><td></td></tr>
										<tr>
											<td align="left" width="20%">
												<font>Flight Status</font>
												<select name="selFlightStatus" id="selFlightStatus" size="1" title="Flight Status">
													<option value="All">All</option>
													<option value="ACT" selected="selected">ACTIVE</option>
													<option value="CNX">CANCELLED</option>
													<option value="CLS">CLOSED</option>
												</select>
											</td>
										</tr>				
										<tr><td><font class="fntSmall">&nbsp;</font></td></tr>				
										<tr>
											<td>
												<table id="carrierCode" style="display: none;">
													<tr>
														<td>
															<font class="fntBold"> Carrier Code </font>										
														</td>							
													</tr>
													
													<tr>
														<td>
															<table>
																<tr>
																	<td valign="top"><span id="spn3"></span></td>
																	<td valign="bottom"><font class="mandatory"> <b>*</b></font></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>	
											</td>
										</tr>			
									</table>
								</td> 						     						    			
							</tr>			
							<tr><td>&nbsp;</td></tr>				
							<tr>				
								<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML" class="noBorder"><font>HTML</font></td>
								<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
									value="PDF" class="noBorder"><font>PDF</font></td>
								<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
									value="EXCEL" class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOption" 
									value="CSV" class="noBorder" checked><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td>&nbsp;</td></tr>	
							<tr>	
								<td width="100%">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
												<td width="80%"><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
									
												<td align="right" width="10%">
													<u:hasPrivilege privilegeId="rpt.sch.forwardbooking">
													<div id="divSchedFrom"></div>
													<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched"
														value="Schedule" onClick="viewClick(true)">
													</u:hasPrivilege>
												</td>
												<td width="10%" align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick(false)"></td>	
										</tr>									
									</table>
								</td>			
							</tr>		
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
			</table>
			<input type="hidden" name="hdnMode" id="hdnMode">
			<input type="hidden" name="hdnLive" id="hdnLive" value="">
			<input type="hidden" name="hdnCarrierCode" id="hdnCarrierCode"> 

		</form>
	</body>
	<script src="../../js/reports/ViewSeatInventoryCollections.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type="text/javascript">	
		 
		<c:out value="${requestScope.carrierCodeList}" escapeXml="false" />
	  
	   <!--
	   var objProgressCheck = setInterval("ClearProgressbar()", 300);
	   function ClearProgressbar(){ 
	      clearTimeout(objProgressCheck);
	      top[2].HideProgress();
	 
	   }
	    
	   //-->
	</script>
</html>