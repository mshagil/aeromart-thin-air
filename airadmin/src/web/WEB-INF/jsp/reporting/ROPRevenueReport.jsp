<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Call Centre Sales Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
<!--For Calendar-->
</head>
<body class="tabBGColor" onbeforeunload="beforeUnload()" onLoad="winOnLoad()" oncontextmenu="return false" scroll="yes">
<form action="showRevenueReport.action" method="post" id="frmROPRevenue" name="frmROPRevenue">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">		
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>ROP Revenue Report<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblHead">
	<tr>
		<td colspan="2">		
				<table width="100%"><tbody>
						<tr>							
						<td style="width: 100px;"><font class="fntBold">Report Type</font>
						</td>
							<td align="left" width="" colspan="2">	
						 		<select name="selReportType" size="1" id="selReportType" style="width: auto;" onchange="reportTypeOnChage()">
									<option value=""></option>
									<c:out value="${requestScope.reqReportTypeList}" escapeXml="false" />								
								</select>
							</td>					
					
					</tr>
					<tr>	<td colspan="3">
								<hr>
							</td>
					
					</tr>
				</tbody></table>
		</td>
	</tr>
					
			<tr>
				<td colspan="2">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">	
						<tr>
							<td colspan="3">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">	
									<tr>
										<td width="12%"><font class="fntBold">Date Range</font></td>						
									
										<td width="14%" align="left"><font>From </font>
											<input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtFromDate')" invalidText="true">
										</td>
										<td width="4%">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font>
										</td>
										<td width="12%"><font>To </font>
											<input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtToDate')" invalidText="true">
										</td>
										<td width="4%"><a href="javascript:void(0)" onclick="LoadCalendarTo(1,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
										<td width="18%">
											<div id="divCabinClassOpt" >
												<input type="checkbox" name="chkCabinClass" id="chkCabinClass" class="noBorder" value="CabinClass" align="left">&nbsp;<font>Include Cabin Class</font>
											</div>										
										</td>										
										<td >&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">	
									<tr>
										<td width="12%"><font class="fntBold">Booked Date</font></td>						
									
										<td width="14%" align="left"><font>From </font>
											<input name="txtBookedFromDate" type="text" id="txtBookedFromDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtBookedFromDate')" invalidText="true">
										</td>
										<td width="4%">
											<a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>
										</td>
										<td width="12%"><font>To </font>
											<input name="txtBookedToDate" type="text" id="txtBookedToDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtBookedToDate')" invalidText="true">
										</td>
										<td width="4%"><a href="javascript:void(0)" onclick="LoadCalendarTo(3,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a></td>
										<td width="12%">&nbsp;</td>										
										<td >&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						
					<tr>
						<td colspan="3">
							<hr>
						</td>
					</tr>
					<tr>
						<td colspan='3'>
							<table width="70%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><font class="fntBold">O &amp; D</font></td>					
									<td><font>Departure</font></td>
									<td>	
										<select name="selDeparture" size="1" id="selDeparture" style="width:55px;">
											<option value=""></option>
											<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
									</td>
									<td><font>Arrival</font></td>
									<td>	
										<select name="selArrival" size="1" id="selArrival" style="width:55px;">
											<option value=""></option>
											<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
									</td>
									<td><font>&nbsp;</font></td>
									<td colspan="2" rowspan="3">
										<select id="selSegment" name="selSegment" multiple size="1" style="width:200px;height:85px">
										</select>
									</td>
								</tr> 
								<tr> 
									<td><font>&nbsp;</font></td>								
									<td><font>Via 1</font></td>
									<td>	
										<select name="selVia1" size="1" id="selVia1" style="width:55px;">
											<option value=""></option>
											<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
									</td>
									<td><font>Via 2</font></td>
									<td>	
										<select name="selVia2" size="1" id="selVia2" style="width:55px;">
											<option value=""></option>
											<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
									</td>
									<td align="center"><a href="javascript:void(0)" onclick="addToList()" title="Add to list"><img src="../../images/AA115_no_cache.gif" border="0"></a></td>
								</tr> 
								<tr> 
									<td><font>&nbsp;</font></td>							
									<td><font>Via 3</font></td>
									<td>	
										<select name="selVia3" size="1" id="selVia3" style="width:55px;">
											<option value=""></option>
											<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
									</td>
									<td><font>Via 4</font></td>
									<td>	
										<select name="selVia4" size="1" id="selVia4" style="width:55px;">
											<option value=""></option>
											<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
									</td>
									<td align="center"><a href="javascript:void(0)" onclick="removeFromList()" title="Remove from list"><img src="../../images/AA114_no_cache.gif" border="0"></a></td> 
							  	</tr> 							
							</table>
						</td>
					</tr>					
					<tr>
						<td width='48%'>
							<font class="fntBold">Booking Classes to Display</font>
						</td>
						<td width='2%'>
						</td>
						<td>
							<font class="fntBold">Agents Stations</font>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>	
									<td colspan ="3" rowspan="7" valign="top">
										<span id="spnBC" class="FormBackGround"></span>
									</td>
							  	</tr>
							</table>
						</td>
						<td></td>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>	
									<td colspan ="3" rowspan="7" valign="top">
										<span id="spnSTC" class="FormBackGround"></span>
									</td>
							  	</tr>
							</table>
						</td>
					</tr>
				 	<tr>
						<td>									
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<font class="fntBold">Agencies</font>
											<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
											<option value=""></option>
											<option value="All">All</option>
											<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
										</select>
									</td>
									<td>
										<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
									</td>
									<td>
										<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
									</td>
								</tr>
								<tr>
									<td><font>&nbsp;</font></td>
									<td>
										<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
									</td>												
								</tr>
							</table>
									
						</td>
					</tr>
					<tr>
						<td colspan='3'><font class="fntBold">Agents</font></td>
					</tr>							
					<tr>
						<td colspan='3'>
							<table width="62%" border="0" cellpadding="0" cellspacing="0">	
								<tr>
									<td valign="top" width="2%"><span id="spn1"></span></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td ><font>&nbsp;</font></td></tr>
					
					<tr>
						<td colspan='3'><font class="fntBold"> Charges</font></td>							
					</tr>
							
					<tr>
						<td colspan='3'>
							<table width="62%" border="0" cellpadding="0" cellspacing="0">	
								<tr>
									<td valign="top" width="2%"><span id="spnCharges"></span></td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr><td><font>&nbsp;</font></td></tr>	
					
					
					<tr>
						<td>
							<font class="fntBold"> Carrier Code </font>										
						</td>							
					</tr>
							
					<tr>
						<td valign="top"><span id="spn2"></span></td>
						<td valign="bottom"><font class="mandatory"><b>*</b></font></td>
					</tr>
					
					<tr><td><font>&nbsp;</font></td></tr>	
					<tr><td colspan = "3">
					<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
					</td>
					</tr>
					<tr>
						<td>
							<div id="divReportViewOpts">						
							<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>				
									<td colspan='3'><font class="fntBold">Output Option</font></td>							
								</tr>
								<tr>
									<td colspan='3' align="left">
										<table width="50%" border="0" cellpadding="0" cellspacing="2"><tr>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
												value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
												value="PDF" class="noBorder"><font>PDF</font></td>
											<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
												value="CSV" class="noBorder"><font>CSV</font></td>
											</tr>	
										</table>						
									</td>									
								</tr>
							</table>
						</div>
						</td>
					</tr>	
					<tr>
						<td colspan='2'>
							<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
						</td>
						<td align="right">
						<u:hasPrivilege privilegeId="rpt.sch.revenueandtax">	
									<div id="divSchedFrom"></div>
									<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched" value="Schedule" onClick="viewClick(true)">
						</u:hasPrivilege>						
							<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
						</td>
					</tr>				
				</table>
			</td>
		</tr>		
	</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
</tr>
	
</table>
<input type="hidden" name="hdnMode" id="hdnMode">
<input type="hidden" name="hdnAgents" id="hdnAgents">
<input type="hidden" name="hdnBCs" id="hdnBCs">
<input type="hidden" name="hdnStations" id="hdnStations">
<input type="hidden" name="hdnSegments" id="hdnSegments">
<input type="hidden" name="hdnLive" id="hdnLive" value="">
<input type="hidden" name="hdnChargesCode" id="hdnChargesCode">
<input type="hidden" name="hdnCarrierCode" id="hdnCarrierCode">
<input type="hidden" name="hdnChargeGroupCodes" id="hdnChargeGroupCodes">

</form>

</body>
<script src="../../js/reports/ROPRevenueReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
	<c:out value="${requestScope.reqBcHtmlData}" escapeXml="false" />
	<c:out value="${requestScope.reqStnHtmlData}" escapeXml="false" />
	<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
	<c:out value="${requestScope.chargesCodeList}" escapeXml="false" />
	<c:out value="${requestScope.carrierCodeList}" escapeXml="false" />
	if(bcls) {
		bcls.height = '125px';
		bcls.width = '160px';				
		bcls.drawListBox();
	}
   
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){ 
      clearTimeout(objProgressCheck);
      top[2].HideProgress();   
   }    
  
</script>
</html>