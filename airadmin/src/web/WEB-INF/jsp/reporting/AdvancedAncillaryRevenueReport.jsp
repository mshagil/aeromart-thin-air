<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Advanced Ancillary Revenue Report</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var stns = new Array();  
	var agentsArr = new Array();
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
<form action="" method="post" id="frmAncillaryRevenueReport"
	name="frmAncillaryRevenueReport">
	
	<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
		
		<tr>
			<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
		</tr>
		<tr>
			
      <td> 
        <%@ include file="../common/IncludeFormTop.jsp"%>Ancillary Revenue<%@ include file="../common/IncludeFormHD.jsp"%>
        <table width="100%" border="0" cellpadding="0" cellspacing="2"
					ID="tblHead">	
					<tr>					
						<td width="100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">	
								<tr><td width="15%" align="left" colspan="2"><font>Sales From Date</font></td><td width="10%"><input
										name="txtSalesFromDate" type="text" id="txtSalesFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtSalesFromDate')"></td>
									<td width="16%" align="left">&nbsp;&nbsp;<a href="javascript:void(0)"
							onclick="LoadCalendar(0,event); return false;" title="Date From"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><!--<font class="mandatory">&nbsp;*</font> --></td>
									<td width="20%" colspan="2"><font>Sales To Date </font><input
										name="txtSalesToDate" type="text" id="txtSalesToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtSalesToDate')"></td>
									<td width="20%"><a href="javascript:void(0)"
							onclick="LoadCalendar(1,event); return false;" title="Date To"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><!--<font class="mandatory">&nbsp;*</font> --></td>
									<td width="10%"></td>
								</tr>
								
								<tr><td width="15%" align="left" colspan="2"><font>Flight From Date</font></td><td width="10%"><input
										name="txtFlightFromDate" type="text" id="txtFlightFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFlightFromDate')"></td>
									<td width="16%" align="left">&nbsp;&nbsp;<a href="javascript:void(0)"
							onclick="LoadCalendar(2,event); return false;" title="Date From"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><!--<font class="mandatory">&nbsp;*</font> --></td>
									<td width="20%" colspan="2"><font>Flight To Date </font><input
										name="txtFlightToDate" type="text" id="txtFlightToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFlightToDate')"></td>
									<td width="20%"><a href="javascript:void(0)"
							onclick="LoadCalendar(3,event); return false;" title="Date To"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><!--<font class="mandatory">&nbsp;*</font> --> </td>
									<td width="10%"></td>
								</tr>
								
								<!-- 
								<tr><td align="left"><font>Sales Channel</font></td>
									<td colspan="2"><select name="selSalesChannel" id="selSalesChannel" size="1">
										<option value=""></option>
										<option value="XBE">XBE</option>
										<option value="IBE">IBE</option>
									</select><font class="mandatory"> &nbsp;* </font></td>
								</tr>
								 -->
								 
								<!-- 
								<tr><td align="left"><font>Ancillary Type</font></td>
									<td colspan="2"><select name="selAncillaryType" id="selAncillaryType" size="1">
										<option value=""></option>
										<option value="SEAT_MAP">Seat Map</option>
										<option value="MEALS">Meals</option>
										<option value="SSR">SSR</option>
										<option value="HALA">Hala</option>
										<option value="INSURANCE">Insurance</option>
										<option value="BAGGAGES">Baggage</option>
										<option value="AIRPORT_SERVICE">Airport Services</option>
									</select></td>
									<td><font class="mandatory"> &nbsp;* </font></td>
								</tr>
								 -->
								 
							</table>
						</td>
					</tr>															
				
				<tr>
					<td>
						<font class="fntBold">Sales Channel</font>
					</td>			
				</tr>
				<tr>
					<td>
						<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="15%"><span id="spn6"></span></td>
								<td valign="bottom"><font class="mandatory"><b>*</b></font><td>								
							</tr>
						</table>
					</td>
				</tr>
	
				<tr>
					<td>
						<font class="fntBold">Ancillaries</font>
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="5"><span id="spn4" name="spn4"></span></td>
					<td valign="bottom"><font class="mandatory"><b>*</b></font><td>								
				</tr>
				
				
				<tr>
					<td>
						<font class="fntBold">Agents</font>
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="5"><span id="spn1" name="spn1"></span>
					<td valign="bottom"><font class="mandatory"><b>*</b></font><td>								
				</tr>
				<tr>
					<td><font class="fntBold">Point of Sale</font></td>
				</tr>
				<tr>
					<td>
						<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="15%"><span id="spn3"></span></td>
								<td valign="bottom"><font class="mandatory"><b>*</b></font></td>
							</tr>
						</table>
					</td>
				</tr>	
				
				<tr>				
					<td><font class="fntBold">Report Option</font></td>							
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="15%"><input type="radio" name="radReportCat" id="radReportCat"
						value="AGENT" class="noBorder"  checked><font>By Agent</font></td>
					<td width="15%"><input type="radio" name="radReportCat" id="radReportCat" 
						value="USER" class="noBorder"><font>By Agent User</font></td>
					<td width="15%"></td>
					<td width="15%"></td>
					</tr>									
					</table>					
					</td>
				</tr>	
				<tr>				
					<td><font class="fntBold">Output Option</font></td>							
				</tr>
				<tr>
					<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="HTML" class="noBorder"  checked><font>HTML</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
						value="PDF" class="noBorder"><font>PDF</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="EXCEL" class="noBorder"><font>EXCEL</font></td>
					<td><input type="radio" name="radReportOption" id="radReportOption" 
						value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=90%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=10% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnReportView" id="hdnReportView">
	<input type="hidden" name="hdnLive" id="hdnLive">
	<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
	<input type="hidden" name="hdnStations" id="hdnStations" value="">
	<input type="hidden" name="hdnAncillaries" id="hdnAncillaries" value="">
	<input type="hidden" name="hdnSalesChannels" id="hdnSalesChannels" value="">
</form>
<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script src="../../js/reports/AdvancedAncillaryRevenueReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
 <script type="text/javascript">
   <!--
   <c:out value="${requestScope.reqAgentStation}" escapeXml="false" />
   <c:out value="${requestScope.reqStationList}" escapeXml="false" />		
   <c:out value="${requestScope.reqAncillaryList}" escapeXml="false" />
   <c:out value="${requestScope.reqSalesChannel}" escapeXml="false" />
   
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
  </script>
</html>