<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Fare Details Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>


<script type="text/javascript">
	var stns = new Array();
	var agentsArr = new Array();
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()"
	oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')"
	scroll="no">
	<form action="showFareDetailsReport.action" method="post"
		id="frmFareDetailReport" name="frmFareDetailReport">

		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">

			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>

			<tr>

				<td><%@ include file="../common/IncludeFormTop.jsp"%>Fare
					Details<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="tblHead">
						<tr>
							<td><font class="fntSmall">&nbsp;</font>
							</td>
						</tr>

						<tr>
							<td>
								<table width="90%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="70%">
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td width="30%"><font class="fntBold">Departure
															Validity Period</font></td>
													<td width="25%" align="left"><font>From </font> <input
														name="txtBookedFromDate" type="text"
														id="txtBookedFromDate" size="10" style="width: 75px;"
														maxlength="10" onblur="dateChk('txtBookedFromDate')"
														invalidText="true"></td>
													<td width="10%"><a href="javascript:void(0)"
														onclick="LoadCalendar(2,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
													<td width="20%"><font>To </font> <input
														name="txtBookedToDate" type="text" id="txtBookedToDate"
														size="10" style="width: 75px;" maxlength="10"
														onblur="dateChk('txtBookedToDate')" invalidText="true">
													</td>
													<td width="10%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(3,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font>
													</td>
												</tr>
											</table></td>
										<td width="8%"><font>Status</font></td>
										<td width="15%">
											<select id="selStatus" size="1"
												name="selStatus" style="width: 76px">
													<option value="ALL">All</option>
													<option value="ACT" selected="selected">Active</option>
													<option value="INA">Inactive</option>
											</select>
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>

											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td width="30%"><font class="fntBold">Sales
															Effective Period</font>
													</td>

													<td width="25%" align="left"><font>From </font> <input
														name="txtFromDate" type="text" id="txtFromDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtFromDate')" invalidText="true">
													</td>
													<td width="10%"><a href="javascript:void(0)"
														onclick="LoadCalendar(0,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a></td>
													<td width="20%"><font>To </font> <input
														name="txtToDate" type="text" id="txtToDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtToDate')" invalidText="true">
													</td>
													<td width="10%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(1,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a></td>
												</tr>
											</table></td>
										<td><font>COS</font></td>
										<td>
											<select id="selCOS" name="selCOS" size="1" tabindex="9" style="width: 125px">																	
												<option value="ALL" selected="selected">All</option>
												<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
											</select>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>

						<!-- O & D -->
						<tr>
							<td>
								<table width="98%" border="0" cellpadding="0" cellspacing="2"
									ID="Table9">
									<tr>
										<td><font class="fntBold">&nbsp;&nbsp;&nbsp;O
												&amp; D</font>
										</td>
										<td><font>Departure</font>
										</td>
										<td><select name="selDeparture" size="1"
											id="selDeparture" style="width: 55px;">		
												<option value=""></option>										
												<c:out value="${requestScope.reqAirportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;&nbsp;Arrival</font>
										</td>
										<td><select name="selArrival" size="1" id="selArrival"
											style="width: 55px;">	
												<option value=""></option>											
												<c:out value="${requestScope.reqAirportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;</font>
										</td>
										<td colspan="2" rowspan="3"><select id="selSegment"
											name="selSegment" multiple size="1"
											style="width: 200px; height: 85px">
										</select>
										<font class="mandatory"><b>*</b></font>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td><font>Via 1</font>
										</td>
										<td><select name="selVia1" size="1" id="selVia1"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqAirportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;&nbsp;Via 2</font>
										</td>
										<td><select name="selVia2" size="1" id="selVia2"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqAirportList}"
													escapeXml="false" />
										</select></td>
										<td align="center"><a href="javascript:void(0)"
											onclick="addToList()" title="Add to list"><img
												src="../../images/AA115_no_cache.gif" border="0"> </a>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td><font>Via 3</font>
										</td>
										<td><select name="selVia3" size="1" id="selVia3"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqAirportList}"
													escapeXml="false" />
										</select></td>
										<td><font>&nbsp;&nbsp;Via 4</font>
										</td>
										<td><select name="selVia4" size="1" id="selVia4"
											style="width: 55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqAirportList}"
													escapeXml="false" />
										</select></td>
										<td align="center"><a href="javascript:void(0)"
											onclick="removeFromList()" title="Remove from list"><img
												src="../../images/AA114_no_cache.gif" border="0"> </a>
										</td>
									</tr>

								</table></td>
						</tr>

						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table width="90%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td valign="top" width="15%"><font class="fntBold">&nbsp;Flight
												No</font>
										<select multiple
											style="width: 100px; height: 90;"
											class="ListBox" id="selFlightNo" name="selFlightNo" size="5">
												<option value="All" selected="selected">All</option>
												<c:out value="${requestScope.reqFlightNoList}"
													escapeXml="false" />
										</select>
										</td>
										<td valign="top" width="10%">&nbsp;</td>
										<td valign="top" width="20%"><font class="fntBold">&nbsp;Booking
												Class Category</font>
										<select multiple
											style="width: 125px; height: 90;"
											class="ListBox" id="selBCCat" name="selBCCat" size="5"
											onchange="filterBookingClasses()">
												<option value="All" selected="selected">All</option>
												<c:out value="${requestScope.reqBCTypeList}"
													escapeXml="false" />

										</select>
										</td>
										<td valign="top" width="10%">&nbsp;</td>
										<td valign="top" width="20%"><font class="fntBold">&nbsp;Booking
												Class Type</font>
										<select multiple
											style="width: 125px; height: 85;"
											class="ListBox" id="selBCTyp" name="selBCTyp" size="5"
											onchange="filterBookingClasses()">
												<option value="All" selected="selected">All</option>
												<c:out value="${requestScope.reqBookingClassTypeList}"
													escapeXml="false" />

										</select></td>
										<td valign="top">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="6">&nbsp;</td>
									</tr>
									<tr>
										<td valign="top"><font class="fntBold">&nbsp;Allocation
												Type</font>
										<select multiple
											style="width: 125px; height: 85;"
											class="ListBox" id="selAllocTyp" name="selAllocTyp" size="5"
											onchange="filterBookingClasses()">
												<option value="All" selected="selected">All</option>
												<c:out value="${requestScope.reqAllocationTypeList}"
													escapeXml="false" />
										</select>
										</td>
										<td valign="top">&nbsp;</td>
										<td valign="top"><font class="fntBold">&nbsp;Booking
												Classes</font>
										<select multiple
											style="width: 125px; height: 85;"
											class="ListBox" id="selBCls" name="selBCls" size="5">												
												<option value="All" selected="selected">All</option>
												<c:out value="${requestScope.reqBookingCodesList}"
													escapeXml="false" />

										</select>
										</td>
										<td valign="top">&nbsp;</td>
										<td valign="top"><font class="fntBold">&nbsp;All
												Fare Rules</font>
										<select multiple
											style="width: 125px; height: 85;"
											class="ListBox" id="selFareRuls" name="selFareRuls" size="5">
												<option value="All" selected="selected">All</option>
												<c:out value="${requestScope.reqFareClassList}"
													escapeXml="false" />
										</select>
										</td>
										<td valign="top">&nbsp;</td>
									</tr>
								</table></td>
						</tr>

						<tr>
							<td>&nbsp;</td>
						</tr>


						<tr>
							<td><table width="75%" border="0" cellpadding="0"
									cellspacing="0">
									<tr>
										<td width="20%"><font class="fntBold">Agencies</font> <select
											id="selAgencies" size="1" name="selAgencies"
											onClick="clickAgencies()" onChange="changeAgencies()"
											style="width: 76px">
												<option value=""></option>
												<option value="All">All</option>
												<c:out value="${requestScope.reqAgentTypeList}"
													escapeXml="false" />
										</select></td>
										<td width="20%"><input type="checkbox" name="chkTAs" id="chkTAs"
											class="noborder"><font>With reporting TA's</font></td>
										<td width="20%"><input name="btnGetAgent" type="button"
											class="Button" id="btnGetAgent" value="List"
											onClick="getAgentClick()"></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td><font>&nbsp;</font></td>
										<td><input type="checkbox" name="chkCOs" id="chkCOs"
											class="noborder"><font>With reporting CO's</font></td>
										<td><font>&nbsp;</font></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="4"><font class="fntBold">Agents</font>
										</td>
									</tr>
									<tr>
										<td colspan="4" valign="top"><span id="spn1"></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="60%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="22%"><font class="fntBold">Oneway/Return
										</font></td>
										<td width="15%"><input type="radio"
											name="selFlightWayOption" id="selFlightWayOption" value="ALL"
											class="noBorder" checked><font>All</font></td>
										<td width="20%"><input type="radio"
											name="selFlightWayOption" id="selFlightWayOption"
											value="ONEWAY" class="noBorder"><font>Oneway</font></td>
										<td width="20%"><input type="radio"
											name="selFlightWayOption" id="selFlightWayOption"
											value="RETURN" class="noBorder"><font>Return</font>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="60%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><font>Sort By </font></td>
										<td><select name="selSortBy" id="selSortBy" size="1"
											style="width: 145px" title="Status">
												<option value="OND" selected="selected">OnD</option>
												<option value="SALVALDATE">Sales Validity Date</option>
												<option value="DEPVAL">Departure Validity</option>
												<option value="FARRUL">Fare Rule</option>
												<option value="BKCLS">Booking Class</option>

										</select>
										</td>

										<td><font>&nbsp;</font></td>

										<td><font>Order </font></td>

										<td><select name="selSortByOrder" id="selSortByOrder"
											size="1" style="width: 100px" title="Status">

												<option value="ASC">Ascending</option>
												<option value="DESC">Descending</option>
										</select>
										</td>
									<tr>
								</table></td>
						</tr>
						<tr>
							<td><font class="fntBold">Report Options</font>&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td>


								<table width="80%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="20%"><input type="radio" name="radReportType"
											id="radReportType" value="FARE_DET" class="noBorder" checked><font>Fare
												Details</font></td>
										<td width="25%"><input type="radio" name="radReportType"
											id="radReportType" value="FLIGHT_WISE" class="noBorder"><font>Flight
												wise Fare Details</font></td>
										<td width="15%">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								</table></td>
						</tr>
						<tr>
							<td><font class="fntBold">Output Options</font>&nbsp;&nbsp;</td>
						</tr>
						

						<tr>
							<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="20%"><input type="radio"
											name="radReportOption" id="radReportOption" value="HTML"
											class="noBorder" checked><font>HTML</font>
										</td>
										<td width="20%"><input type="radio"
											name="radReportOption" id="radReportOptionPDF" value="PDF"
											class="noBorder"><font>PDF</font>
										</td>
										<td width="20%"><input type="radio"
											name="radReportOption" id="radReportOptionEXCEL"
											value="EXCEL" class="noBorder"><font>EXCEL</font>
										</td>
										<td width="40%"><input type="radio"
											name="radReportOption" id="radReportOptionCSV" value="CSV"
											class="noBorder"><font>CSV</font>
										</td>
									</tr>
								</table> <c:out value="${requestScope.rptFormatOption}"
									escapeXml="false" /></td>
						</tr>

						<tr>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><input type="button" id="btnClose" name="btnClose"
											value="Close" class="Button" onclick="closeClick()">
										</td>
										<td align="right"><input type="button" id="btnView"
											name="btnView" value="View" class="Button"
											onclick="viewClick()"></td>
										<td width="10%">&nbsp;</td>
									</tr>
								</table></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode"> <input
			type="hidden" name="hdnReportView" id="hdnReportView"> <input
			type="hidden" name="hdnLive" id="hdnLive"> <input
			type="hidden" name="hdnSegments" id="hdnSegments" value=""> <input
			type="hidden" name="hdnAgents" id="hdnAgents" value=""> <input
			type="hidden" name="hdnRptType" id="hdnRptType" value=""> <input
			type="hidden" name="hdnAgentName" id="hdnAgentName" value="">

		<input type="hidden" name="hdnFlightNo" id="hdnFlightNo"> <input
			type="hidden" name="hdnBCCat" id="hdnBCCat"> <input
			type="hidden" name="hdnBCTyp" id="hdnBCTyp"> <input
			type="hidden" name="hdnAllocTyp" id="hdnAllocTyp"> <input
			type="hidden" name="hdnBCls" id="hdnBCls"> <input
			type="hidden" name="hdnFareRuls" id="hdnFareRuls"> <input
			type="hidden" name="hdnAllBCls" id="hdnAllBCls"
			value="<c:out value='${requestScope.reqBookingCodesDesc}' escapeXml='false' />">

	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script
	src="../../js/reports/FareDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
	<c:out value="${requestScope.reqAgentList}" escapeXml="false" />
	<c:out value="${requestScope.reqGSAList}" escapeXml="false" />

	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	function ClearProgressbar() {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
	}
</script>
</html>
