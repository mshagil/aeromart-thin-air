<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Mode of Payments</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadCus()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<form action="" method="post" id="frmWebProfile" name="frmWebProfile">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Customer Profile / Travel History<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">
			<tr>
				<td><font class="fntBold">Date Range</font></td>				
			</tr>
			<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">	
				<tr><td width="17%" align="left"><font>From </font><input
					name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"></td>
				<td width="20%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
				<td width="15%"><font>To </font><input
					name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')"></td>
				<td><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"><font class="mandatory">&nbsp;*</font></a></td>
				</tr>
			</table>
			</td>
			</tr>
			<tr><td>&nbsp;</td></tr>					
			<tr><td><table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				 <td width="23%"><input type="checkbox" name="radCustomerNationality" id="radCustomerNationality"
					value="Nationality" onClick="chkClick()" class="noBorder"><font>Nationality</font></td>
				 <td width="77%"><select id="selNationality" size="1" name="selNationality" style="width:150px">
										<option value="">&nbsp;</option>
											<c:out value="${requestScope.reqNationalityList}" escapeXml="false" />										
									    </select></td>				
				</tr>
				<tr>
				 <td><input type="checkbox" name="radCustomerCountry" id="radCustomerCountry"
					value="CountryofRes" onClick="chkClick()" class="noBorder"><font>Country of Residence</font></td>
				 <td><select id="selCountryOfRes" size="1" name="selCountryOfRes" style="width:150px">
											<option value="">&nbsp;</option>
											<c:out value="${requestScope.reqCountryList}" escapeXml="false" />	
									     </select></td>				
				</tr>
				</table>
				</td>			
			</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td><table width="50%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td  width="50%" align="left">
							<font>Get All Passengers including Cancelled : </font>
						</td>
						<td width="50%" align="left">
							<input type="checkbox" id="chkGetAllPax" name="chkGetAllPax">
						</td>
					</tr>
					</table>
					</td>
				</tr>	
				<tr><td>&nbsp;</td></tr>			
				<tr><td>
					<input type="checkbox" name="chkSector" id="chkSector" onclick="checkClick()" class="NoBorder"><font class="fntBold">Sector</font>							
					</td>
				</tr>
				<tr>
					<td width="100%"><table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
					<td width="47%"><font>From </font><select id="selSectorFrom" size="1" name="selSectorFrom" style="width:75px" tabindex="1">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />										
									     </select></td>
					<td width="53%"><font>To </font><select id="selSectorTo" size="1" name="selSectorTo" style="width:75px" tabindex="1">
											<option value="">&nbsp;</option>
											<option value="ALL">All</option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     </select></td>
					</tr>
					</table></td>
				</tr>
				
				<tr><td>&nbsp;</td></tr>
				<tr><td>
					<input type="checkbox" name="chkBookingStatus" id="chkBookingStatus" onclick="checkBookingStatusClick()" class="NoBorder"><font class="fntBold">Booking Status</font>
				</td>
				</tr>
				<tr>
					<td width="100%"><table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="50%"><font>Booking Status </font><select id="selBookingStatus" size="1" name="selBookingStatus" style="width:75px" tabindex="1">
											<option value=""></option>
											<option value="ALL">All</option>
											<option value="CNF">Confirmed</option>
											<option value="CNX">Cancelled</option>										
									     </select></td>
					</tr>
					<tr>
						<td><font>Sales Channel</font>
							<select id="selSalesChannelCode" size="1" name="selSalesChannelCode" style="width:75px" tabindex="1">
								<c:out value="${requestScope.reqSalesChannelCodes}" escapeXml="false" />
							</select>
						</td>
					</tr>
					</table></td>
				</tr>
				
				
				
				
				</table>
				</td>			
			</tr>
			<tr><td>&nbsp;</td></tr>				
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="10%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
					value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
				<td width="10%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
					value="PDF" class="noBorder"><font>PDF</font></td>
				<td width="10%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
					value="EXCEL" class="noBorder"><font>EXCEL</font></td>
				<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
					value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
					<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />					
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=70%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=30% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode" value="Mode"> 
<input type="hidden" name="hdnLive" id="hdnLive" value="">
<input type="hidden" name="hdnRequestAllPassengers" id="hdnRequestAllPassengers" value="" >
</form>
</body>
<script src="../../js/reports/WebProfile.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress(); 
   }
    
   //-->
  </script>
</html>