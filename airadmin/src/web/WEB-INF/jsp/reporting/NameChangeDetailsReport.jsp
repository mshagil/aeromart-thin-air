<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Name Change Details</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body onload="onLoad()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
<form action="" method="post" id="frmNameChange" name="frmNameChangeDetails">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Name Change Details<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">

			<tr>
				<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				<td width="40%" colspan="2"><font class="fntBold">Modification Date Range</font></td>				
				
				</tr>
				</table></td>
			</tr>

			<tr>
			<td>
			<table width="50%" border="0" cellpadding="0" >	
				<tr>
				<td width="15%" align="left"><font>From </font><input
					name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')" invalidText="true">
				<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
				<td width="25%"><font>To </font><input
					name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')" invalidText="true">
				<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
								
			
				</tr>				
			</table>
			</td>
			</tr>

			<tr><td>&nbsp;</td></tr>
			
			<tr>
				<td>
					<table>
						<tr> <td> <font class="fntBold">Departure Date Range</font> </td> </tr>
						<tr>
							<td width="15%" align="left"><font>From </font>
							<input name="txtDepFromDate" type="text" id="txtDepFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtDepFromDate')" invalidText="true">
							<a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
							<td width="25%"><font>To </font>
							<input name="txtDepToDate" type="text" id="txtDepToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtDepToDate')" invalidText="true">
							<a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b></b></font></td>
								
						</tr>
						
					</table>
				</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>

			<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">	
			<tr>
			<td ><font class="fntBold">Airports</font></td>
			</tr>
			</table>
			</td>
			</tr>	

			<tr>
			<td>
			<table width="50%" border="0" cellpadding="0" >
			<tr>
				<td width="15%"><font>From </font><select id="selFrom" size="1" name="selFrom" style="width:75px">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     </select><font class="mandatory"><b></b></td>
				<td width="25%"><font>To </font><select id="selTo" size="1" name="selTo" style="width:75px">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
								     </select><font class="mandatory"><b></b></td>
					     
			</tr>
			</table>
			</td>
			</tr>


			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					<table width="70%" border="0" cellpadding="0" cellspacing="2">
						<tr>	
							<td><font>Flight Number </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							name="txtFlightNumber" type="text" id="txtFlightNumber" size="7" style="width:75px;"  class="UCase" maxlength="7"><font class="mandatory"><b></b></font></td>
						</tr>												
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td align="left" width="20%"><font class="fntBold">User ID&nbsp;&nbsp;&nbsp; </font>
					<select name="selUserId" id="selUserId" size="1" title="User Id">
						<option value=""></option>																					
						<option value="All">All</option>
						<c:out value="${requestScope.reqUserIdList}" escapeXml="false" />
					</select>
					
				</td>	
			</tr>
							
			<tr><td>&nbsp;</td></tr>				
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
					value="HTML"  class="noBorder" checked="checked"><font>HTML</font></td>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
					value="PDF"  class="noBorder"><font>PDF</font></td>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
					value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
			<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
					value="CSV"  class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">	
			<tr>
				<td width="10%" align="left"><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>	
				<td width="90%" align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>
			</tr>
			</table>
			</td>
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode">
<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>
</body>
<script src="../../js/reports/ValidateNameChangeDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>