<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Top Agents</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadTA()" class="tabBGColor" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
<form action="" method="post" id="frmTopAgents"
	name="frmModeofPayments">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Top Agents<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">

			<tr>
				<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				<td width="40%" colspan="2"><font class="fntBold">Date Range</font></td>				
				
				</tr>
				</table></td>
			</tr>

			<tr>
			<td>
			<table width="50%" border="0" cellpadding="0" >	
				<tr>
				<td width="15%" align="left"><font>From </font><input
					name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')" invalidText="true">
				<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
				<td width="25%"><font>To </font><input
					name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')" invalidText="true">
				<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
								
			
				</tr>				
			</table>
			</td>
			</tr>

			<tr><td>&nbsp;</td></tr>

			<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">	
			<tr>
			<td ><font class="fntBold">Airports</font></td>
			</tr>
			</table>
			</td>
			</tr>	

			<tr>
			<td>
			<table width="50%" border="0" cellpadding="0" >
			<tr>
				<td width="15%"><font>From </font><select id="selFrom" size="1" name="selFrom" style="width:75px">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     </select></td>
				<td width="25%"><font>To </font><select id="selTo" size="1" name="selTo" style="width:75px">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
								     </select></td>
					     
			</tr>
			<tr><td>&nbsp;</td></tr>		
			<tr>
				<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				<td width="40%" colspan="2"><font class="fntBold">Route</font></td>				
				
				</tr>
				</table></td>
			</tr>
			<tr>
				<td width="15%"><font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
				<select id="selFlightType" size="1" name="selFlightType" style="width:75px">
											<option value="">All</option>
											<option value="INT">International</option>
											<option value="DOM">Domestic</option>
											
					     
			</tr>
			</table>
			</td>
			</tr>


			<tr><td>&nbsp;</td></tr>
			<tr>			
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
				<tr><td><font class="fntBold">No. of Top Agents</font></td>
				</tr>				
				<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
					<tr>
					<td width="20%"><input type="radio" name="radNoTopAgents" id="radNoTopAgents"
					value="5" onClick="checkTA()" class="noBorder" checked="checked"><font>5</font></td>
					<td width="20%"><input type="radio" name="radNoTopAgents" id="radNoTopAgents10" 
					value="10" onClick="checkTA()" class="noBorder"><font>10</font></td>
					<td width="20%"><input type="radio" name="radNoTopAgents" id="radNoTopAgents15"
					value="15" onClick="checkTA()" class="noBorder"><font>15</font></td>
					<td width="20%"><input type="radio" name="radNoTopAgents" id="radNoTopAgentsO"
					value="other" onClick="checkTA()" class="noBorder"><font>Other</font></td>
					<td><input name="txtTopAgentsOther" type="text" id="txtTopAgentsOther" size="3" style="width:75px;" maxlength="3" onKeyUp="KPValidatePositiveInteger('txtTopAgentsOther')" onKeyPress="KPValidatePositiveInteger('txtTopAgentsOther')" class="rightText"></td>
				</tr>									
				</table>
				</td>
				</tr>
				</table>
				</td>			
			</tr>
			<tr><td>&nbsp;</td></tr>				
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
					value="HTML"  class="noBorder" checked="checked"><font>HTML</font></td>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
					value="PDF"  class="noBorder"><font>PDF</font></td>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
					value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
			<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
					value="CSV"  class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">	
			<tr>
				<td width="10%" align="left"><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>	
				<td width="80%" align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>
				<td width="10%" align="right">
					<div id="divSchedFrom"></div>
					<input name="btnSchedule" type="button" class="Button" id="btnSchedule" value="Schedule" onClick="scheduleClick()">
				</td>			
			</tr>
			</table>
			</td>
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode">
<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>
</body>
<script src="../../js/reports/TopAgents.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html><%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
