<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page language="java"%>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View ETL Processing</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
    <script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  </head>
  <body scroll="no"  scroll="no" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" ondrag='return false'
  	onUnload="resetVariables()" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
<%@ include file="../common/IncludeWindowTop.jsp"%>
  <form method="post" action="showETLDetailProcessing.action" id="frmETLDetail">
  			<script type="text/javascript">
				var arrData = new Array();
				var currentRow="";
				var arrTitle=new Array();
				var arrInfantTitles = new Array();
				var paxTypes = new Array();
				var arrTitleChild = new Array();
				var etlState="";
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
				<c:out value="${requestScope.reqPaxTitles}" escapeXml="false" />
				<c:out value="${requestScope.reqPaxTitlesForChild}" escapeXml="false" />
				<c:out value="${requestScope.reqPaxType}" escapeXml="false" />
				<c:out value="${requestScope.reqInfTitles}" escapeXml="false" /> 
				
			</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<!--tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr-->

				
				<!--tr><td><font class="fntSmall">&nbsp;</font></td></tr-->
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%> ETL Header Details<%@ include file="../common/IncludeFormHD.jsp"%> 
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">							
							<tr>
								<td>
								<span id="spnHeader" name="spnHeader">
								</span>
									
								</td>
								
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>

				
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>ETL Details <%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table10">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
								<span id="spnETLProcessing"></span>
								</td>
							</tr>
							<tr>
								<td>
								
								</td>
							</tr>
							<tr>
								<td>
									<input type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()">
									<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()">
									<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()">
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						ETL Details 					
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							  <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" ID="Table1">
								<tr><td>
									<table width="98%" border="0" cellpadding="0" cellspacing="0" align="left" ID="Table1">
									<tr>
									<td width="10%" align="left"><font>Pax Title</font>
									</td>
										<td width="10%">
										<select name="selTitle" id="selTitle" size="1" style="width:75;" onchange="pageOnChange()" tabindex="1">
											</select>
											<font id="paxTitleMandater" class="mandatory" style="display: inline;">&nbsp;*</font>
											</td>
											<td></td>
									</tr>
								    <tr>
									<td width="10%" align="left"><font>Pax Type</font></td>
										<td width="10%">
											<select name="selPaxType" id="selPaxType" size="1" style="width:75;" tabindex="2" onChange="paxChange();titleOnChange()">
											</select>										
										</td>
										<td width="10%">										
										</td>
									</tr>
									<tr>
										<td width="15%" align="left"><font>Pax First Name</font></td>
										<td width="40%"><input  name="txtFirstName" type="text" id="txtFirstName" maxlength="50" size="50" onKeyup="pageOnChange(),KeyPress(this, 'ANW')" onkeypress="KeyPress(this, 'ANW')" onchange="pageOnChange()" class="fontCapitalize" tabindex="3">		
										</td>
										<td rowspan="6" >&nbsp;&nbsp;&nbsp;&nbsp;
												
										
									</tr>
									<tr>
										<td width="15%" align="left"><font>Pax Last Name</font></td>
										<td><input  name="txtLastName" type="text" id="txtLastName" maxlength="50" size="50" onKeyup="pageOnChange(),KeyPress(this, 'ANW')" onkeypress="KeyPress(this, 'ANW')" onchange="pageOnChange()" class="fontCapitalize" tabindex="4">	</td>
									</tr>
									<tr>
										<td width="15%" align="left"><font>Eticket No</font></td>
										<td width="8%"><input  name="txtEtNo" type="text" id="txtEtNo" maxlength="20" size="20" onKeyup="pageOnChange(),KeyPress(this, 'AN')" onkeypress="KeyPress(this, 'AN')" onchange="pageOnChange()" tabindex="6">
										<font id="eTicketMandater" class="mandatory" style="display: inline;">&nbsp;*</font></td>
									</tr>
									<tr>
										<td width="15%" align="left"><font>Coupen No</font></td>
										<td width="8%"><input  name="txtCoupenNo" type="text" id="txtCoupenNo" maxlength="20" size="20" onchange="pageOnChange()" onkeyup="validateCoupenNo()" tabindex="7">
										<font id="coupenNoMandater" class="mandatory" style="display: inline;">&nbsp;*</font></td>
									</tr>
									<SPAN id="parentDetail" name="parentDetail">
										<tr>								
												<td width="10%" align="left"><font>Infant's Eticket No</font></td>
												<td width="10%">
													<input  name="txtInfEtNo" type="text" id="txtInfEtNo" maxlength="20" size="20" onKeyup="pageOnChange(),KeyPress(this, 'AN')" onkeypress="KeyPress(this, 'AN')" onchange="pageOnChange()" tabindex="8">
													<font id="infantEticketMandater" class="mandatory" style="display: none;">&nbsp;*</font>
												</td>
												<td align="left" ></td>
										</tr>										
									</SPAN>
									<tr>
									<tr>
										<td width="15%" align="left"><font>Infant's Coupen No</font></td>
										<td width="8%"><input  name="txtInfCoupenNo" type="text" id="txtInfCoupenNo" maxlength="20" size="20" onkeyup="validateInfCoupenNo()" onchange="pageOnChange()" tabindex="9">
										<font id="infantCoupenNoMandater" class="mandatory" style="display: none;">&nbsp;*</font></td>
									</tr>
									<td width="10%" align="left"><font>Status</font></td>
										<td width="10%" ><select name="selAddStatus" id="selAddStatus" size="1" style="width:105;" onKeyup="pageOnChange()" onchange="pageOnChange();changeStatus()" tabindex="10">
										</select></td>
									</tr>

									<tr>
									<td width="15%" align="left"><font>Destination</font></td>
										<td><select tabindex="11" name="selDest" id="selDest" onKeyup="pageOnChange()" onChange="pageOnChange()">
										<option value=""> </option>
<%-- 									<c:out value="${requestScope.reqAirportList}" escapeXml="false" /> --%>
										<c:out value="${requestScope.reqAirportCombo}" escapeXml="false" />
										
	                                </select></td>
									</tr>
									<tr>
									<td>
										<font>Is NOREC Passenger</font>
									</td>
									<td>
										<input type="checkbox" id="chkNoREC" name="chkNoREC">
									</td>
									</tr>
																								
							  </table>
								
								</td></tr>
								<tr>
								  <td style="height:42px;" colspan="2" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input tabindex="13" type="button" id="btnClose" class="Button" value="Close"  onclick="windowclose()">	
													<input tabindex="14" name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetETL()" value="Reset">
												 </td>
												  <td align="right">
													<input tabindex="15" name="btnSave" type="button" class="Button" id="btnSave" onClick="processSaveETL('SAVE')" value="Save" style="width:70px;">
												 </td>
												 <td>&nbsp;&nbsp;</td>
											</tr>
										</table>
								   </td>
								</tr>
							  </table>
							
						  <%@ include file="../common/IncludeFormBottom.jsp"%>	
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
<%@ include file="../common/IncludeWindowBottom.jsp"%>
		<script src="../../js/scheduledservices/ETLDetailGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<input type="hidden" name="hdnId" id="hdnId"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnETLData" id="hdnETLData"/>
		<input type="hidden" name="hdnETLID" id="hdnETLID"/>
		<input type="hidden" name="hdnPaxVersion" id="hdnPaxVersion"/>
		<input type="hidden" name="hdnPaxID" id="hdnPaxID"/>
		<input type="hidden" name="hdnETLPaxID" id="hdnETLPaxID"/>
		<input type="hidden" name="hdnCurrentEtl" id="hdnCurrentEtl"/>
		<input type="hidden" name="hdnIsParent" id="hdnIsParent" value="N"/>
		<input type="hidden" name="hdnETLProcessStatus" id="hdnETLProcessStatus"/>
		<input type="hidden" name="hdnCurrentRowNum" id="hdnCurrentRowNum"/>
		<input type="hidden" name="hdnETLAddedData" id="hdnETLAddedData"/>
		<input type="hidden" name="hdnProcessSta" id="hdnProcessSta"/>
		<input type="hidden" name="hdnState" id="hdnState"/>
		<input type="hidden" name="txtaETLCmnts" id="txtaETLCmnts"/>
		<input type="hidden" name="hdnDownloadDate" id="hdnDownloadDate">
		<input type="hidden" name="hdnFlightNo" id="hdnFlightNo">
		<input type="hidden" name="hdnFlightDate" id="hdnFlightDate">
		<input type="hidden" name="hdnOrigin" id="hdnOrigin">
		<input type="hidden" name="hdnIsNoREC" id="hdnIsNoREC">
	</form>	
  </body>
  	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/scheduledservices/ETLDetailValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</html>

