 <%-- 
	 @Author 	: 	Dhanushka Navin Ranatunga
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page language="java"%>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View XAPNL Processing</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body scroll="no"  scroll="no" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" ondrag='return false'
  	onUnload="resetVariables()" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
<%@ include file="../common/IncludeWindowTop.jsp"%>
  <form method="post" action="showXAPNLDetailProcessing.action" id="frmPFSDetail">
  			<script type="text/javascript">
				var arrData = new Array();
				var currentRow="";
				var arrTitle=new Array();
				var arrPaxTitle=new Array();
				var paxTypes = new Array();
				var pfsState="";
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
				<c:out value="${requestScope.reqPaxTitles}" escapeXml="false" />
				<c:out value="${requestScope.reqPaxType}" escapeXml="false" />
				
			</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<!--tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr-->

				
				<!--tr><td><font class="fntSmall">&nbsp;</font></td></tr-->
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%> XAPNL Header Details<%@ include file="../common/IncludeFormHD.jsp"%> 
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">							
							<tr>
								<td>
								<span id="spnHeader" name="spnHeader">
								</span>
									
								</td>
								
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>

				
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>XAPNL Details <%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table10">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
								<span id="spnPFSProcessing"></span>
								</td>
							</tr>
							<tr>
								<td>
								<!-- 
									<input type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()">
									<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()">
									<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()">
									<input type="button" id="btnTBA" class="Button" value="T.B.A." onClick="tbaClick()">
								 -->
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						XAPNL Entry Details 					
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							  <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" ID="Table1">
								<tr><td>
									<table width="98%" border="0" cellpadding="0" cellspacing="0" align="left" ID="Table1">
									<tr>
									<td width="10%" align="left"><font>Pax Title</font></td>
										<td width="10%">
										<select name="selTitle" id="selTitle" size="1" style="width:75;" onchange="pageOnChange()" tabindex="1">
											</select></td>
											<td><font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;XAPNL Content</font></td>
									</tr>
								    <tr>
									<td width="10%" align="left"><font>Pax Type</font></td>
										<td width="10%">
										<select name="selPaxType" id="selPaxType" size="1" style="width:75;" tabindex="2">
										</select>
										</td>
										<td>&nbsp;</font></td>
									</tr>
									<tr>
										<td width="15%" align="left"><font>Pax First Name</font></td>
										<td width="40%"><input  name="txtFirstName" type="text" id="txtFirstName" maxlength="50" size="50" onKeyup="pageOnChange()" onchange="pageOnChange()" class="fontCapitalize" tabindex="3"><font class="mandatory">&nbsp;*</font>		
										</td>
										<td rowspan="6" >&nbsp;&nbsp;&nbsp;&nbsp;
											<textarea name="txtaPFSCmnts" id="txtaPFSCmnts" cols="60" rows="11" maxlength="225" onkeyPress="validateTextArea(this)" title="Can enter only up to 255 charactors" readOnly="true" style="display:none;"></textarea>
											<span id="spnContent" name="spnContent"  style='background: white;font-size: 9pt;height: 140px;width: 350px; overflow:scroll; display:none;'></span></td>
										
									</tr>
									<tr>
										<td width="15%" align="left"><font>Pax Last Name</font></td>
										<td><input  name="txtLastName" type="text" id="txtLastName" maxlength="50" size="50" onKeyup="pageOnChange()" onchange="pageOnChange()" class="fontCapitalize" tabindex="4"><font class="mandatory">&nbsp;*</font>	</td>
									</tr>
									<tr>
										<td width="15%" align="left"><font>PNR</font></td>
										<td width="8%"><input  name="txtPNR" type="text" id="txtPNR" maxlength="20" size="20" onKeyup="pageOnChange()" onchange="pageOnChange()" tabindex="5"></td>
									</tr>
									
									<tr style="display:none;">
									<td width="10%" align="left"><font>Status</font></td>
										<td width="10%" ><select name="selAddStatus" id="selAddStatus" size="1" style="width:105;" onKeyup="pageOnChange()" onchange="pageOnChange();changeStatus()" tabindex="6">
										</select></td>
									</tr>

									<tr>
									<td width="15%" align="left"><font>Destination</font></td>
										<td><select tabindex="7" name="selDest" id="selDest" onKeyup="pageOnChange()" onChange="pageOnChange()">
										<option value=""> </option>
										<!--c:out value="${requestScope.reqAirportList}" escapeXml="false" /-->
										<c:out value="${requestScope.reqAirportCombo}" escapeXml="false" />
										
	                                </select></td>
									</tr>
									<tr style="display:none;">
										<td><font>No. of Pax</font></td>
										<td><input  name="txtPaxNO" type="text" id="txtPaxNO" maxlength="4" size="6" onKeyup="pageOnChange()" onchange="pageOnChange()" tabindex="8"></td>										
									</tr>
									<tr>
										<td><font>Class of Service</font></td>
										<td><select  name="selCC" id="selCC"  onchange="pageOnChange()" tabindex="9">
											<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />		
											</select><font class="mandatory">&nbsp;*</font>	
										</td>
										
									</tr>
									<tr style="display:none;">
										<td><font>Pax Category</font></td>
										<td><select  name="selPaxCat" id="selPaxCat"  onchange="pageOnChange()" tabindex="10">
											<c:out value="${requestScope.reqPaxCatList}" escapeXml="false" />
											</select>											
										</td>										
									</tr>																
							  </table>
								
								</td></tr>
								<tr>
								  <td style="height:42px;" colspan="2" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input tabindex="11" type="button" id="btnClose" class="Button" value="Close"  onclick="windowclose()">
													<!-- 	
													<input tabindex="12" name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetPFS()" value="Reset">
													 -->
												 </td>
												 <td align="right">
												 <!--
													<input tabindex="13" name="btnSave" type="button" class="Button" id="btnSave" onClick="processSavePFS('SAVE')" value="Save" style="width:70px;">
												 -->
												 </td>
												 <td>&nbsp;&nbsp;</td>
											</tr>
										</table>
								   </td>
								</tr>
							  </table>
							
						  <%@ include file="../common/IncludeFormBottom.jsp"%>	
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
<%@ include file="../common/IncludeWindowBottom.jsp"%>
		<script src="../../js/scheduledservices/XAPNLDetailGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<input type="hidden" name="hdnId" id="hdnId"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnPFSData" id="hdnPFSData"/>
		<input type="hidden" name="hdnPFSID" id="hdnPFSID"/>
		<input type="hidden" name="hdnPaxVersion" id="hdnPaxVersion"/>
		<input type="hidden" name="hdnPaxID" id="hdnPaxID"/>
		<input type="hidden" name="hdnCurrentPfs" id="hdnCurrentPfs"/>

		<input type="hidden" name="hdnCurrentRowNum" id="hdnCurrentRowNum"/>
		<input type="hidden" name="hdnPFSAddedData" id="hdnPFSAddedData"/>
		<input type="hidden" name="hdnProcessSta" id="hdnProcessSta"/>
		<input type="hidden" name="hdnState" id="hdnState"/>
		<input type="hidden" name="txtaPFSCmnts" id="txtaPFSCmnts"/>
		
	</form>	
  </body>
  	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/scheduledservices/XAPNLDetailValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</html>

