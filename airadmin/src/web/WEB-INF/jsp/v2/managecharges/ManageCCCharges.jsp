<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Credit Card Wise Charge</title>
	    <meta http-equiv="pragma" content="no-cache"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="-1"/>
		<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>		
		<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script src="../../js/v2/manageCharges/manageCCCharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		  
		   
		<script type="text/javascript">	
			var paymentGateways = "<c:out value="${requestScope.reqPaymentGateways}" escapeXml="false" />";

		</script>
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
 						<td width='8%'><font>PGW</font></td>
						<td width="10%">
							<select id='selPGWSearch' name='selPGWSearch' size='1' tabindex="1">
									<option value="***">ANY</option>
									<c:out value="${requestScope.reqPaymentGateways}" escapeXml="false" />
							</select>				
						</td>
						<td width="10%" >
							<font>&nbsp;</font>
							<font>Effective Date</font>
						</td>
						<td  width="10%"><input name="selEffectiveDate" type="text" id="selEffectiveDate" style="width:78px;" maxlength="11" tabindex="2" onblur="dateChk('selEffectiveDate')" /></td>
						<td width="10%" >
							<font>&nbsp;</font>
							<font>Status</font>
						</td>
						<td width="10%">
							<select id='selStatus' name='selStatus' size='1' tabindex="3">
									<option value="***">ALL</option>
									<option value="ACT">Active</option>
									<option value="INA">Inactive</option>
							</select>				
						</td>
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table id="listCCWiseCharge" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="CCWiseChargepager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="plan.fares.cccharge.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="plan.fares.cccharge.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
								
			</div>			  		
			<div id="divDispCCWiseCharge" style="height:330px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showCreditCardCharge!SaveOrUpdateCCWiseCharge.action" id="frmCCWiseCharge" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
					<tr><td>&nbsp;</td></tr>					
					<tr>
						<td width='10%'><font>PGW</font></td>
						<td width="30%">
							<select id='txtPGW' name='txtPGW' size='1' tabindex="4">
									<option value="***"></option>
									<c:out value="${requestScope.reqPaymentGateways}" escapeXml="false" />
							</select>	
							<font class="mandatory">&nbsp;*</font>			
						</td>
					</tr>
					<tr>
						<td width="10%" >
							<font>Effective - From</font>
						</td>
						<td width="20%"><input name="txtEffectiveDateFrom" type="text" id="txtEffectiveDateFrom" style="width:78px;" maxlength="11" tabindex="5" onblur="dateChk('txtEffectiveDateFrom')" /><font class="mandatory">&nbsp;*</font></td>
						<td width="10%">
							<font>Effective Date - To</font>
						</td>
						<td width="20%"><input name="txtEffectiveDateTo" type="text" id="txtEffectiveDateTo" style="width:78px;" maxlength="11"  tabindex="6"  onblur="dateChk('txtEffectiveDateTo')" /><font class="mandatory">&nbsp;*</font></td>
						<td width='60%'><font>&nbsp;</font></td>
					</tr>
					<tr>
						<td width="10%"><font>Value Percentage</font></td>
						<td width="20%">
							<select name="valuePercentage" size="1" tabindex="7" id="valuePercentage">										
								<option value="V" selected>V</option>
								<option value="P">P</option>
							</select>
						</td>
						<td><font>Amount</font></td>
						<td width="20%"><input type="text"  id="amount" tabindex="8" name="amount">&nbsp;<font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td width="10%"><font>Status</font></td>
						<td width="7%">
							<select name="status" size="1" tabindex="9" id="status">										
								<option value="ACT" selected>Active</option>
								<option value="INA">Inactive</option>
							</select>
						</td>
					</tr>	
					<tr>
						<td>
						    <input type="hidden" name="strCCWCId"  id="strCCWCId"  />	
						    <input type="hidden" name="strPGW"  id="strPGW"  />	
						    <input type="hidden" name="hdnMode"  id="hdnMode"  />				
						</td>
					</tr>							
			 		<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>

					</tr>
			  	</table>		

			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_INVN_025';
		top[2].HideProgress();
	</script>
  </body>
</html>