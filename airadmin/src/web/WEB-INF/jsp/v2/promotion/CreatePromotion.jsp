<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoTemplateParam" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Create Promotion Templates</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/promotion/CreatePromotion.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">	

</script>
</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>

<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
	<div id="divSearch" style="height: 30px; text-align: left;">
		<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
			<tr>
				<td><font>Promotion Type</font></td>
				<td><select name="selMealCode" size="1" id="searchPromotionType" style="width: 100px;">
						<option value="-1">All</option>
						<c:out value="${requestScope.promotionTypesList}" escapeXml="false" />
				</select>
				</td>
				<td><font>Start Date</font></td>
				<td><input type="text" name="txtStartDateSearch" id="txtStartDateSearch"  value=<%=StartDate %> style="width: 72px;" maxlength="10" ><font class="mandatory">&nbsp;*</font></td>
				<td><font>End Date</font></td>
				<td><input type="text" name="txtStopDateSearch" id="txtStopDateSearch"  value=<%=StopDate %> style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font></td>
				<td class="UCase">Status</td>
				<td>
					<select name="promoSearchStatus" id="promoSearchStatus" size="1" style="width:105;" class="UCase">
						<option value="ALL">All</option>
						<option value="ACT">Active</option>
						<option value="INA">In-Active</option>										
					</select>
			   </td>
				<td align="right"><input name="btnSearch" type="button" id="btnSearch" value="Search" onclick="searchPromotionTemplates()" class="button">
				</td>
			</tr>
		</table>
	</div>
	<div id="divResultsPanel" style="height: 240px; text-align: left; background-color: #ECECEC;">
		<div id="jqGridPromoTemplatesContainer" style="margin-top: 5px;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridPromoTemplatesData">
			</table>
			<div id="jqGridPromoTemplatesPages"></div>
		</div>
		 
		<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
		
		<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
	</div>
	<div id="promotionsTemplateDetails" style="height: 275px; text-align: left; background-color: #ECECEC;">
		<form method="post" action="promotionTemplatesManagementAction.action" id="frmPromotion">
			<div id="promotionsTabs" class="admin-panel ui-border-topDis">
				<ul class="ui-border-leftDis ui-border-rightDis">
					<li id="tabAddModChargeTop"><a href="#tabPromoDetails">Promotion Details</a></li>
					<li id="tabAddModChargeTop"><a href="#tabApplicableRoutes">Applicable Routes</a></li>
					<li id="tabAddModChargeRateTop"><a href="#tabPromCharges">Charges/Rewards</a></li>
				</ul>
				<div id="tabPromoDetails">
					<div style="display: none;">
						<input type="hidden" id="promotionId">
					</div>
					<table width="96%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr>
							<td width="50%">
								<div>
									<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table1">
										<tr>
											<td width="40%"><font>Promotion Type</font></td>
											<td><select name="selPromotionCode" size="1" id="promotionType" style="width: 100px;" onchange="promotionTypeChange()">
													<c:out value="${requestScope.promotionTypesList}" escapeXml="false" />
											</select></td>
										</tr>
										<tr>
											<td><font>Start Date</font></td>
											<td width="100px"><input type="text" name="txtStartDateDetail" id="txtStartDateDetail" value=""
												style="width: 100px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
											</td>
										</tr>
										<tr>
											<td><font>End Date</font></td>
											<td width="100px"><input type="text" name="txtEndDateDetail" id="txtEndDateDetail" value=""
												style="width: 100px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
											</td>
										</tr>
										<tr>
											<td><font>Status</font></td>
											<td><select size="1" id="selStatus" style="width: 100px;">
													<option value="ACT">Active</option>
													<option value="INA">Inactive</option>
											</select> <font class="mandatory">&nbsp;*</font>
											</td>
										</tr>
									</table>
								</div></td>
							<td width="50%" valign="top">
								<fieldset style="border-width: 2px;">
									<legend>
										<font>Promotion Params:</font>
									</legend>
									<div id="promoParamsPane">
										<table width="100%" border="0" cellpadding="0" cellspacing="2" id="paramsNextSeatFree" style="display: none;">
											<tr style="display: none">
												<td width="40%"><font>Min Seats</font>
												</td>
												<td><input type="text" id="txtFreeSeatMinSeats" style="display: none;" maxlength="10" value="1" onblur="generateChargesAndRewardsParamHtml()"  >
												</td>
											</tr>
											<tr style="display: none">
												<td width="40%"><font>Max Seats</font>
												</td>
												<td><input type="text" id="txtFreeSeatMaxSeats"style="display: none;" maxlength="10" value="1" onblur="generateChargesAndRewardsParamHtml()">
												</td>
											</tr>
											<tr>
												<td width="40%"><font>No of additional Seats</font>
												</td>
												<td><label id="defaultSeats">1</label>
												</td>
											</tr>
											<tr>
												<td width="40%"><font>Min Vacant Seats</font>
												</td>
												<td><input type="text" id="txtMinVacantSeats" style="width: 100px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
												</td>
											</tr>
											<tr>
												<td width="40%"><font>Preserve Seats</font>
												</td>
												<td><input type="text" id="txtPreserveSeats" style="width: 100px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
												</td>
											</tr>			
										</table>
										<table width="100%" border="0" cellpadding="0" cellspacing="2" id="paramsFlexiDate" style="display: none;">
											<tr>
												<td width="40%"><font>Lower Boundary (Days)</font>
												</td>
												<td><input type="text" id="txtFlexiDateLowerBound" style="width: 100px;" maxlength="10" onblur="generateChargesAndRewardsParamHtml()"><font class="mandatory">&nbsp;*</font>
												</td>
											</tr>
											<tr>
												<td width="40%"><font>Upper Boundary (Days)</font>
												</td>
												<td><input type="text" id="txtFlexiDateUpperBound" style="width: 100px;" maxlength="10" onblur="generateChargesAndRewardsParamHtml()"><font class="mandatory">&nbsp;*</font>
												</td>
											</tr>
										</table>
									</div>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				<div id="tabApplicableRoutes">
					<table width="96%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr>
							<td width="15%"><font>Origin</font>
							</td>
							<td width="15%"><select name="originAirport" size="1" id="originAirport" style="width: 100px;" onchange="loadRoutes()">
								<option value="ALL">ALL</option>
							</select><font class="mandatory">&nbsp;*</font></td>
							
							<td align="left" valign="center" width="15%" rowspan="4"><font>Available Routes</font></td>
							<td align="left" width="20%" rowspan="4"><select id="availableRoutes" name="availableRoutes" style="width: 150; height: 150" multiple >
							</select></td>
							
							<td width="5%" rowspan="4">
								<table>
							  		<tr>
							  			<td align="center" onclick="addRoutes()">
											<div class="Button" style="width:23px;height: 17px">></div>
										</td>
									</tr>
									<tr>
										<td align="center" onclick="removeRoutes()">
											<div class="Button" style="width:23px;height: 17px"><</div>
										</td>
							  		</tr>
								</table>
							</td>
							<td align="center" width="20%" rowspan="4"><select id="applicableRoutes_1" size="8" name="applicableRoutes_1" style="width: 150; height: 150" multiple >
							</select><font class="mandatory">&nbsp;*</font></td>
						</tr>
						<tr>
							<td>
								<font>Via Point 1</font>
							</td>
							<td width="15%">
								<select name="viaPoint1" size="1" id="viaPoint1" style="width: 100px;" onchange="loadRoutes()" >
									<option value=""></option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<font>Via Point 2</font>
							</td>
							<td width="15%">
								<select name="viaPoint2" size="1" id="viaPoint2" style="width: 100px;" onchange="loadRoutes()">
									<option value=""></option>
								</select>
							</td>
						</tr>
						<tr>
							<td><font>Destination</font>
							</td>
							<td><select name="destAirport" size="1" id="destAirport" style="width: 100px;" onchange="loadRoutes()">
								<option value="ALL">ALL</option>
							</select><font class="mandatory">&nbsp;*</font></td>
						</tr>
					</table>
				</div>
				<div id="tabPromCharges">
					<table width="96%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr>
							<td width="50%">
								<div>
									<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table1">
										<tr>
											<td width="40%"><font>Applicable Routes</font>
											</td>
											<td colspan="2"><select id="applicableRoutes_2" size="8" name="applicableRoutes_2" style="width: 100; height: 150" multiple >
											</select></td>
										</tr>
									</table>
								</div></td>
							<td>
								<div id="chargesRewardsPane">
									
								</div></td>
						</tr>
					</table>
				</div>
			</div>
			<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input name="btnClose" type="button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="button">
					<input name="btnReset" type="button" id="btnReset" value="Reset" onclick="resetClick()" class="button"></td>
					<td align="right" valign="bottom">
					<input name="btnSave" type="button" id="btnSave" value="Save" class="button" onclick="submitPromotionTemplate()"></td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		var screenId = "SC_PROM_01";		
		// Promotion template params
		var PromoTemplateParams = {};
		PromoTemplateParams['FlexiDate.DaysLowerBound'] = "<%= PromoTemplateParam.TemplateConfig.FLEXI_DATE_LOWER_BOUND %>";
		PromoTemplateParams['FlexiDate.DaysUpperBound'] = "<%= PromoTemplateParam.TemplateConfig.FLEXI_DATE_UPPER_BOUND %>";
		PromoTemplateParams['FreeSeat.SeatsMin'] = "<%= PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_SEATS_MIN %>";
		PromoTemplateParams['FreeSeat.SeatsMax'] = "<%= PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_SEATS_MAX %>";
		PromoTemplateParams['FreeSeat.MinVacant'] = "<%= PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_MIN_VACANT_SEATS %>";
		PromoTemplateParams['FreeSeat.SeatsPreserve'] = "<%= PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_PRESERVE_SEATS %>";
		
		// Promotion charges/rewards params
		var PromoRewardsAndCharges = {};
		PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] = "<%= PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD.getPrefix() %>" ;
		PromoRewardsAndCharges['FreeSeat.RegistrationCharge'] = "<%= PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_REG_CHRG.getPrefix() %>" ;
		PromoRewardsAndCharges['FreeSeat.PrefixChargeOnSeats'] = "<%= PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG.getPrefix() %>" ;
		
		top[2].HideProgress();
	</script>
</body>
</html>