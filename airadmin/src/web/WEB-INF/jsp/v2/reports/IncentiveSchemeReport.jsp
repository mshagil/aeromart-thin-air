<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
	
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
   	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.templete.js??<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
			
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDownDup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		<c:out value="${requestScope.reqSchemeList}" escapeXml="false" />
		
</script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" class="tabBGColor">
 
	<div id="divIncentiveRpt" style="height:560px;text-align:left;background-color:#ECECEC;">
  	<form name="frmIncentiveSchRpt" id="frmIncentiveSchRpt" action="" method="post">
		
		 <table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblInvoiceScheme">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td width="20%" align="left"><font class="fntBold">Scheme Date Range From</font></td>
				<td width="15%" valign="top"><input name="searchDateFrm" type="text" id="searchDateFrm" size="10" style="width:80px;" maxlength="10"><font class="mandatory">*</font></td>
				<td width="10%" align="left"><font  class="fntBold">To</font></td>
				<td width="60%" valign="top"><input name="searchDateTo" type="text" id="searchDateTo" size="10" style="width:80px;" maxlength="10"><font class="mandatory">*</font></td>
			</tr>	
			<tr>
				<td align="left" colspan="4"><font>Scheme Inclusion</font></td>
			</tr>
			<tr>
				<td align="left" colspan="4"><span id="spnIncentive"></span></td>
			</tr>

			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>				
				<td><font class="fntBold">Output Option</font></td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>
				<td colspan="4">
					<table  width="100%" border="0" cellpadding="0" cellspacing="2">
						<tr>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption"
								value="HTML" class="noBorder"  checked><font>HTML</font></td>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption" 
								value="PDF" class="noBorder"><font>PDF</font></td>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption"
								value="EXCEL" class="noBorder"><font>EXCEL</font></td>					
							<td width="46%"><input type="radio" name="radReportOption" id="radReportOption" 
								value="CSV" class="noBorder"><font>CSV</font></td>
						</tr>									
					</table>
				</td>	
			</tr>	
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>						
			<tr>
				<td colspan="3">
					<input name="btnClose" type="button" class="Button" id="btnClose" value="Close">
				</td>
				<td align="right">
					<input name="btnView" type="button" class="Button" id="btnView" value="View">
				</td>
			</tr>
		</table>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnIncentives" id="hdnIncentives" value="">
		<input type="hidden" name="hdnReportView" id="hdnReportView" value="">
	</form>
	</div>
	<script type="text/javascript">
  	<!--
	var screenId = 'UC_REPM_054';
	top[2].HideProgress();
  	//-->
  </script>
  </body>  
  <script type="text/javascript" src="../../js/v2/reports/IncetiveSchemeReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
</html>
