<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Meal Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/meal/Meal.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script src="../../js/meal/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		   
		<script type="text/javascript">	
			var mlImagePath = "";
			<c:out value="${requestScope.reqMealImgPath}" escapeXml="false" />
			var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
			var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;
			var focMealSelEnabled = <c:out value="${requestScope.reqFOCMealSelEnable}" escapeXml="false" />;
		</script>
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="10%"><font>Meal Name</font></td>
						<td width="14%"><input name="srchMealName" type="text" id="srchMealName" size="15"  maxlength="50" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"><font class="mandatory">&nbsp;*</font></td>

						<td width="10%"><font>Meal Code</font></td>
						<td width="10%">
							<select name="selMealCode" size="1" id="selMealCode">
								<option value="">All</option>																				
								<c:out value="${requestScope.reqMeallist}" escapeXml="false" />
							</select>
						</td>
						<td><font>COS</font></td>
								<td colspan="2">
									<select name="selCOS" size="1" id="selCOS" style="width: 80px" >
									<option value="">All</option>	
										<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
						</td>
						<td width="7%"><font>IATA</font></td>
						<td width="10%">
							<select name="selIATACode" size="1" id="selIATACode">
								<option value="">All</option>
								<c:out value="${requestScope.reqIATACodeList}" escapeXml="false" />
							</select>
						</td>
						<td width="8%"><font>Status</font></td>
						<td width="7%">
							<select name="selStatus" size="1" id="selStatus">
								<option value="ALL">All</option>										
								<option value="ACT">Active</option>
								<option value="INA">Inactive</option>
							  </select>
						</td>								
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table id="listMeal" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="mealpager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.meal.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.meal.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.meal.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.meal.add">
						<input type="button" value= "Add Description For Display" class="btnDefault" onClick="return POSenable();" id="btnMealFD" style="width:200px;">
					</u:hasPrivilege>				
				
			</div>			  		
			<div id="divDispMeal" style="height:330px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showMeal.action" id="frmMeal" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr><td>&nbsp;</td></tr>					
						<tr>
						<td width="15%"><font>Meal Name</font></td>
						<td colspan="2"><input name="mealName" type="text" id="mealName" size="20"  maxlength="50" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"><font class="mandatory">&nbsp;*</font></td>
						<td rowspan="6"><img  name="imgMealThamb" id= "imgMealThamb" src="" alt="" height="150"> &nbsp;&nbsp;  <img  name="imgMeal" id= "imgMeal" src="" alt="" height="150"> </td>
					</tr>
					<tr>
						<td><font>Meal Description</font></td>
						<td colspan="2"><input name="mealDescription" type="text" id="mealDescription" size="30" maxlength="125"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Meal Category</font></td>
						<td colspan="2">
							<select name="mealCategory" size="1" id="mealCategory">	
								<option value="-1"></option>
								<c:out value="${requestScope.reqMeaLCatlist}" escapeXml="false" />
							</select>
							<font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr>
						<td><font>Meal Code</font></td>
						<td colspan="2"><input name="mealCode" type="text" id="mealCode" style="width:105;" maxlength="6" onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)"><font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr>
						<td><font>Class of Service</font></td>
						<td colspan="2">
							<select name="cos" size="4" id="cos" multiple="multiple" style="height: 58px; width: 150px" >	
								<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
							</select>
							<font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr>
						<td><font>Applicable Channels</font></td>
						<td colspan="2">
							<table>
							   <tr>
								<td><font>XBE</font></td>
								<td><input type="checkbox" name="chkXBE" id="chkXBE" value="3"></td>
								<td><font>IBE</font></td>
								<td><input type="checkbox" name="chkIBE" id="chkIBE" value="4"></td>
								<td><font>WS</font></td>
								<td><input type="checkbox" name="chkWS" id="chkWS" value="12"></td>
								<td><font>IOS</font></td>
								<td><input type="checkbox" name="chkIOS" id="chkIOS" value="20"></td>
								<td><font>ANDRIOD</font></td>
								<td><input type="checkbox" name="chkAndriod" id="chkAndriod" value="21"></td>						    
							   </tr>

							</table>
						</td>
					</tr>
					<tr>
						<td><font>IATA Code</font></td>
						<td colspan="2">
							<select name="iataCode" size="1" id="iataCode">	
								<option value="-1"></option>
								<c:out value="${requestScope.reqIATACodeList}" escapeXml="false" />	
							</select>
							<font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr>
						<td><font>Active</font></td>
						<td colspan="2"><input type="checkbox" name="status" id="status" value="ACT"></td>
					</tr>
					
					<tr>
						<td><font>Image</font></td>
						<td width="20%"><div id="dvUpLoad"><input type="file"  id="mealImage" name="mealImage">&nbsp;</div></td>
						<td align="left" valign="top"> &nbsp;&nbsp;<input name="btnUpload" type="button"  id="btnUpload" value="Upload" ></td>
						
					</tr>
					<tr>
						<td><font> Thumbnail image  </font></td>
						<td width="20%"><div id="dvUpLoadThum"><input type="file"  id="mealThumbnailImage" name="mealThumbnailImage">&nbsp;</div></td>
						<td align="left" valign="top"> &nbsp;&nbsp;<input name="btnUploadThumbnail" type="button"  id="btnUploadThumbnail" value="Upload" ></td>
						
					</tr>
			 		<tr>
						<td colspan="3" style="height:22px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
						    <input type="text" name="mealCos"  id="mealCos" style="display: none;" />
							<input type="text" name="version"  id="version" style="display: none;" />
							<input type="text" name="mealId"  id="mealId" style="display: none;" />							
							<input type="text" name="createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="createdDate"  id="createdDate" style="display: none;" />
							<input type="text" name="uploadImage"  id="uploadImage" style="display: none;" />
							<input type="text" name="uploadImageThumbnail"  id="uploadImageThumbnail" style="display: none;" />
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />						
						</td>
				   </tr>
			  </table>		
			  <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			  <input type="hidden" name="hdnModel" id="hdnModel" value="">
			  <input type="hidden" name="hdnMealForDisplay" id="hdnMealForDisplay" value="">	
			   <input type="hidden" name="mealsForDisplay" id="mealsForDisplay" value="">		
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_ADMN_030';
		top[2].HideProgress();
	</script>
  </body>
</html>