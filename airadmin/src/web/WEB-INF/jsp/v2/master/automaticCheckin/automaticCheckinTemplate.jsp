<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/WEB-INF/jsp/v2/master/automaticCheckin/automaticCheckinInit.jsp" %>
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
 	
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearchPanel" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td><font>Airport</font></td>
						<td>
							 <select name="selAirportCode" size="1" id="selAirportCode">
							 	<option value="">All</option>
							      <c:forEach var="airportListMap" items="${requestScope.reqAirportList}">
							        <option value='<c:out value="${airportListMap.key}"/>'><c:out value="${airportListMap.value}"/></option>
							      </c:forEach>
						    </select>
						</td>
						<td><font>Status</font></td>
						<td>
							<select name="selStatus" size="1" id="selStatus" style="width:100%;">
								<option value="ACT">Active</option>
								<option value="INA">Inactive</option>
								<option value="ALL">All</option>
							</select>
						</td>									
						<td align="right">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search" style="width:100%;">
						</td>
					</tr>
				</table>			
			</div>
			<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table id="listACITemp" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="temppager" class="scroll" style="text-align:center;"></div>
				<u:hasPrivilege privilegeId="sys.mas.autocheckintempl.add">
					<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
				</u:hasPrivilege>
				<u:hasPrivilege privilegeId="sys.mas.autocheckintempl.edit">
					<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
				</u:hasPrivilege>
			</div>		
			<div id="divDispAutoCheckinTemp" style="height:250px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showAutoCheckInTemplate.action" id="frmAutoCheckin"> 
					<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr>
							<td><input name="automaticCheckinTemplateId" type="hidden" id="automaticCheckinTemplateId"/></td>
						</tr>
						
						<tr>
							<td valign="top" >
								<font>Airport List</font>
							</td>
							<td>
								<span id="airportList"></span>									
							</td>
						</tr>				
						<tr>
							<td><font>Charge per pax/airport in base currency</font></td>
							<td>
								<span class="paxAmount">
									<input name="amount" type="text" id="amount" size="5" maxlength="10" ><font class="mandatory">&nbsp;*</font>
								</span>
								<span class="activeCheck" style="margin-left: 6px;">
									Active <input type="checkbox" name="status" id="status" value="ACT" style="margin: 6px 0 0 6px;">
								</span>
							</td>
						</tr>
				 		<tr>
							<td colspan="2" style="height:25px;"  valign="bottom">
								<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
							</td>
							<td align="right" valign="bottom">
								<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" name="selAirportLst"  id="selAirportLst"/>						
							</td>
					   </tr>
				  </table>					
			  </form>
			</div>	
	<script>
		var screenId = 'SC_ADMN_027';
		top[2].HideProgress();
	</script>
	<script type="text/javascript">
		<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />
	</script>
  </body>
</html>