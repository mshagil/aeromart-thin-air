
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Airport Transfer Template</title>
        <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
        <LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
        <LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
	    
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script type="text/javascript" src="../../js/v2/jquery/grid.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/grid.inlinedit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.RESCRIPTED.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/isa.airadmin.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	

  	   <script type="text/javascript">  		
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			var flightNos = <c:out value="${requestScope.reqFltNumber}" escapeXml="false" />
      </script>
      <style>	
		.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}	  
	  </style>
  </head>
  
<body class='tabBGColor'>

<div style="margin-top:0px;">
	<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						 	<td align="left" class="fntBold" width="5%" ><font>Flight No</font></td>
							<td align="left" width="10%">
								<input type="text" id="searchFlightNo" name="searchFlightNo" style="width:50px;" maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />" />
							</td>
							<td align="left" class="fntBold" width="5%" ><font>Cabin Class</font></td>
							<td align="left" width="10%">
								<select id="searchCabinClassCode" name="searchCabinClassCode" size="1" style="width:80px">	
								     <option value=""></option>																			
									 <c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />
							   </select>
							</td>
							<td align="left" class="fntBold" width="5%" ><font>Aircraft Model</font></td>
							<td align="left" width="10%">
								<select id="searchAircraftModel" name="searchAircraftModel" size="1" style="width:80px">	
								     <option value=""></option>																			
									 <c:out value="${requestScope.sesAircraftModelListData}" escapeXml="false" />
							   </select>
							</td>
							<td align="left" class="fntBold" width="5%" ><font>Status</font></td>
							<td align="left" width="10%">
								<select id="searchStatus" name="searchStatus" size="1" style="width:80px">
									 <option value="">ALL</option>		
								     <option value="ACT">ACT</option>																			
									 <option value="INA">INA</option>
							   </select>
							</td>
			
							<td align="right" width="8%">									
								<input name="btnSearch" type="button" id="btnSearch" value="Search">
							</td>
					</tr>
				 </table>
	</div>
	
	<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table id="tblAirportTransferTemplates" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="transferpager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.airport.transfer.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.airport.transfer.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
	</div>			
  	
	
	<div id="divDispTransferTemplate" style="height:400px;text-align:left;background-color:#ECECEC;">
	<form method="post"  action="showAirportTransferTemplate!save.action" id="frmAirportTransferTemplate"  enctype="multipart/form-data"> 
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
			  <tr>
			  	  <td>&nbsp;</td>
			  </tr>
			  <tr>
					<td align="left"><font>Flight No</font></td>
					<td>
						<input type="text" id=flightNumber name="transferTemplate.flightNumber" maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />" tabIndex="10" onkeyUp="customWhiteSpaceValidate(this)" onkeyPress="customWhiteSpaceValidate(this)" />						
					</td>												
		     </tr>
		    <tr><td>&nbsp;</td></tr>	
			<tr>
				<td align="left"><font>Departure Date</font></td>
				<td>
					<input type="text" id="departureDate" name="transferTemplate.departureDate" style="width:75px;" readonly="readonly" maxlength="10" tabIndex="5">
					<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>		
			<tr>
				<td align="left"><font>Cabin Class</font></td>
				<td><select name="transferTemplate.cabinClassCode" size="1" id="cabinClassCode" onchange="cabinClassOnChange()">
						<option value="ANY">ANY</option>
						<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />
				</select>
					<input name="btnLoadBookingClasses" type="button"  id="btnLoadBookingClasses" value="Load Booking Classes" class="btnDefault" style="
    						width: 150px;"/>
				</td>
			</tr>
			
			<tr>
				<td>
					<font>Booking Class</font>
				</td>
			</tr>
							
			<tr>
				<td colspan="2" id="bookingClassSelect">
					<table width="70%" border="0" cellpadding="0" cellspacing="2">
						<tr>
							<td valign="top"><span id="spn2"></span></td>
							<td align="left" valign="bottom"><font class="mandatory"><b>*</b></font></td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
			
			<tr>
				<td valign="top"><font>Aircraft Model</font></td>
				<td valign="top">
					<select name="transferTemplate.modelNumber" id="modelNumber" size="1" style="width:300px">
						<option value="ANY">ANY</option>
						<c:out value="${requestScope.sesAircraftModelListData}" escapeXml="false" />
					</select>				
				</td>											
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr class="airportServiceCharge">
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;ADL&nbsp;<input
					name="transferTemplate.adultAmount" type="text" id="adultAmount" size="9"
					maxlength="8" onkeyUp="validateAmount(this)"
					onkeyPress="validateAmount(this)">
					&nbsp;&nbsp;CHD&nbsp;<input name="transferTemplate.childAmount" type="text"
					id="childAmount" size="9" maxlength="8"
					onkeyUp="validateAmount(this)"
					onkeyPress="validateAmount(this)">
					&nbsp;&nbsp;INF&nbsp;<input name="transferTemplate.infantAmount" type="text"
					id="infantAmount" size="9" maxlength="8"
					onkeyUp="validateAmount(this)"
					onkeyPress="validateAmount(this)"></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td valign="top" colspan="3">
					<table border="0" width="80%" id="tblSelApplicableOnd">
						<tr>
							<td colspan="4">
								<font>Applicable OnDs</font> &nbsp; 
							</td>
						</tr>
						<tr>
							<td align="right"><font>Departure</font></td>
							<td >
					  			<select id="selDepature" name="selDepature" size="1" style="width:60px;">
									<option value="All">All</option>
									<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
								</select>
							</td>
							<td align="right"><font>Arrival</font></td>
							<td>
								<select id="selArrival" name="selArrival" size="1" style="width:60px;">
									<option value="All">All</option>
									<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
								</select>
							</td>
							<td align="right"><font>Via 1</font></td>
							<td>
								<select id="selVia1" name="selVia1" size="1" style="width:60px;">
									<OPTION value=""></OPTION>
									<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
								</select>
							</td>
							<td align="right"><font>Via 2</font></td>
							<td ><select id="selVia2" name="selVia2" size="1" style="width:60px;">
									<OPTION value=""></OPTION>
									<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
								</select>
							</td>
						</tr>
					</table>	
				</td>
			</tr>	
			<tr><td>&nbsp;</td></tr>	
			<tr>
				   <td>Active</td>
					<td colspan="2"><input type="checkbox" name="chkStatus" id="chkStatus"></td>
		    </tr>
		 </table>				
				
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">						
			<tr>
				<td colspan="3" style="height:42px;"  valign="bottom">
					<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId);" class="btnDefault" />
				    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault" />
				</td>
				<td align="right" valign="bottom">
					<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault" />
				</td>
			</tr>
		</table>
			<input type="hidden" name="transferTemplate.status" id="status" value="INA">
		    <input type="hidden" name="transferTemplate.ondCode" id="ondCode" value="" />
		    <input type="hidden" name="transferTemplate.version" id="version" value="">
			<input type="hidden" name="transferTemplate.airportTransferTemplateId" id="airportTransferTemplateId" value="">
			<input type="hidden" name="transferTemplate.bookingClasses" id="bookingClasses" value="">
			<input type="hidden" name="transferTemplate.bookingClassesIds" id="bookingClassesIds" value="">
		</form>
	</div>
	
</div>

<script type="text/javascript">
	<c:out value="${requestScope.reqBookingClasses}" escapeXml="false" />
  	var screenId = 'SC_ADMN_043';
	top[2].HideProgress();
</script>
	 <script type="text/javascript" src="../../js/v2/master/airportTransfer/airportTransferTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
	 <script type="text/javascript" src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	 <script type="text/javascript" src="../../js/v2/master/airportMessages/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
  </body>
</html>
