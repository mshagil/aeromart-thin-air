<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>SSR Info Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>


<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/ssr/SSRInfoAdmin.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/ssr/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>


</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	<script type="text/javascript">			
				var jsonSSRSubCatMap = <c:out value="${requestScope.reqSubCategoryList}" escapeXml="false" />;
				var ssrInvEnabled = <c:out value="${requestScope.reqSSRInventoryEnabled}" escapeXml="false" />;
				var ssrCutOffTimeEnabled = <c:out value="${requestScope.reqSSRCutoffTimeEnabled}" escapeXml="false" />;
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />			
				var ssrImagePath = "";
				<c:out value="${requestScope.reqSSRImgPath}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />	
				var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
				var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;	
			</script>
	<div id="divSearch" style="height: 30px; text-align: left;">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"
			ID="Table9">
			<tr>
				<td width="15%"><font>SSR Code</font>
				</td>
				<td width="20%"><input name="selSsrCode" type="text"
					id="selSsrCode" size="10" maxlength="4" class="UCase">
				</td>
				<td width="15%"><font>Service Category</font>
				</td>
				<td width="20%"><select name="selCategory" size="1" id="selCategory">
						<option value="ALL">All</option>
						<c:out value="${requestScope.reqCategoryList}" escapeXml="false" />
				</select>
				</td>
				<td width="15%"><font>Service</font>
				</td>
				<td width="20%"><select name="selSubCategory" size="1"
					id="selSubCategory">
						<option value="ALL">All</option>
				</select>
				</td>

				<td width="10%"><font>Status</font>
				</td>
				<td width="10%"><select name="selStatus" size="1"
					id="selStatus">
						<option value="ALL">All</option>
						<option value="ACT">Active</option>
						<option value="INA">Inactive</option>
				</select></td>
				<td align="right"><input name="btnSearch" type="button"
					id="btnSearch" value="Search"></td>
				<td width="2%">&nbsp;</td>
			</tr>
		</table>
	</div>

	<div id="divResultsPanel"
		style="height: 300px; text-align: left; background-color: #ECECEC;">
		<table id="listSSRInfo" class="scroll" cellpadding="0" cellspacing="0"></table>
		<div id="ssrinfopager" class="scroll" style="text-align: center;"></div>

		<u:hasPrivilege privilegeId="sys.mas.ssr.add">
			<input name="btnAdd" type="button" id="btnAdd" value="Add"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ssr.edit">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ssr.delete">
			<input name="btnDelete" type="button" id="btnDelete" value="Delete"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ssr.add">
			<input type="button" value="Add Description For Display"
				class="btnDefault" onClick="return POSenable();" id="btnSSRInfoFD"
				style="width: 200px;">
		</u:hasPrivilege>

	</div>
	<div id="divDispSSRInfo"
		style="height: 500px; text-align: left; background-color: #ECECEC;">
		<form method="post" action="showSSRInfo.action" id="frmSSRInfo"
			enctype="multipart/form-data">
			<table width="98%" border="0" cellpadding="0" cellspacing="2"
				align="center" ID="Table1">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table width="98%" border="0" cellpadding="0" cellspacing="2"
							align="center">
							<tr>
								<td width="65%">
									<table width="98%" border="0" cellpadding="0" cellspacing="5"
										align="center">
										<tr>
											<td width="25%"><font>SSR Code</font>
											</td>
											<td colspan="2"><input name="ssrCode" type="text"
												id="ssrCode" size="20" maxlength="4" class="UCase"
												onkeyUp="alphaCustomValidate(this)"
												onkeyPress="alphaCustomValidate(this)"><font
												class="mandatory">&nbsp;*</font>
											</td>
											<td rowspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td><font>Name</font>
											</td>
											<td colspan="2"><input name="ssrName" type="text"
												id="ssrName" size="40" maxlength="100"><font
												class="mandatory">&nbsp;*</font>
											</td>
										</tr>
										<tr>
											<td><font>Description</font>
											</td>
											<td colspan="2"><input name="ssrDesc" type="text"
												id="ssrDesc" size="40" maxlength="1000"
												onkeyUp="descriptionOLTransVal(this)"
												onkeyPress="descriptionOLTransVal(this)"><font
												class="mandatory">&nbsp;*</font>
											</td>
										</tr>
										<tr>
											<td><font>Service Category</font>
											</td>
											<td colspan="2"><select name="ssrCatId" size="1" id="ssrCatId">
													<option value="-1"></option>
													<c:out value="${requestScope.reqCategoryList}"
														escapeXml="false" />

											</select> <font class="mandatory">&nbsp;*</font></td>
										</tr>

										<tr>
											<td><font>Service </font>
											</td>
											<td colspan="2"><select name="ssrSubCatId" size="1"
												id="ssrSubCatId">
													<option value="-1"></option>
											</select> <font class="mandatory">&nbsp;*</font></td>
										</tr>

										<tr>
											<td><font>Active</font>
											</td>
											<td colspan="2"><input type="checkbox" name="status"
												id="status" value="ACT">
											</td>
										</tr>
										<tr>
											<td colspan="3">
                                                Show in XBE&nbsp;<input type="checkbox"
												name="shownInXBECheck" id="shownInXBECheck" value="ACT">&nbsp;&nbsp;
												Show in IBE&nbsp;<input type="checkbox"
												name="shownInIBECheck" id="shownInIBECheck" value="Y">&nbsp;&nbsp;
                                                Show in GDS&nbsp;<input type="checkbox"
                                                name="shownInGdsCheck" id="shownInGdsCheck" value="Y">&nbsp;&nbsp;
                                                Show in WS&nbsp;<input type="checkbox"
                                                name="shownInWsCheck" id="shownInWsCheck" value="Y">
											</td>
										</tr>
										<tr id="trApplicability">
											<td valign="bottom"><font>Applicability</font>&nbsp;</td>
											<td colspan="2" valign="bottom">Departure&nbsp;<input
												type="checkbox" name="applDeparture" id="applDeparture"
												value="Y">&nbsp;&nbsp;Arrival &nbsp;<input
												type="checkbox" name="applArrival" id="applArrival"
												value="Y"> &nbsp;&nbsp;<label class="transit">Transit</label> &nbsp;<input
												type="checkbox" name="applTransit" id="applTransit"
												value="Y" class="transit" >
											</td>
										</tr>
										<tr>
											<td valign="top" colspan="3">
												<table width="98%" border="0" cellpadding="0" cellspacing="2"
													align="center" id="tblSelApplicableAirport" style="display: none;">
													<tr>
														<td colspan="4">
															<font>Applicable Airports</font>
														</td>
													</tr>
													<tr>
														<td valign="top" colspan="3"><span id="spn3"></span></td>
														<td valign="bottom"><font class="mandatory">&nbsp;*</font></td>
													</tr>										
												</table>
												
												<table border="0" width="80%" id="tblSelApplicableOnd" style="display: none;">
													<tr>
														<td colspan="4">
															<font>Applicable OnDs</font> &nbsp; <font class="mandatory">&nbsp;*</font>
														</td>
													</tr>
													<tr>
														<td width="17%"><font>Departure</font></td>
														<td >
												  			<select id="selDepature" name="selDepature" size="1" style="width:60px;">
																<option value="All">All</option>
																<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
															</select>
														</td>
														<td width="16%"><font>Arrival</font></td>
														<td>
															<select id="selArrival" name="selArrival" size="1" style="width:60px;">
																<option value="All">All</option>
																<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
															</select>
														</td>
													</tr>
													<tr>
														<td width="17%"><font>Via 1</font></td>
														<td colspan="2">
															<select id="selVia1" name="selVia1" size="1" style="width:60px;">
																<OPTION value=""></OPTION>
																<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
															</select>
														</td>
														<td rowspan="4">
															<select id="selSelected" size="3" name="selSelected" style="width:150;height:70" multiple>
															</select>
														</td>
													</tr>
													<tr>
														<td width="17%"><font>Via 2</font></td>
														<td ><select id="selVia2" name="selVia2" size="1" style="width:60px;">
																<OPTION value=""></OPTION>
																<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
															</select>
														</td>
														<td align="center"><input type="button" id="btnAddSeg" name="btnAddSeg" class="Button" value=">" style="width:40px;height:17px" onClick="btnAddSegment_click()"> </td>
														<td></td>
													</tr>
			
													<tr>
														<td width="17%"><font>Via 3</font></td>
														<td ><select id="selVia3" name="selVia3" size="1" style="width:60px;">
																<OPTION value=""></OPTION>
																<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
															</select>
														</td>
														<td align="center"><input type="button" id="btnRemSeg" name="btnRemSeg" class="Button" value="<" style="width:40px;height:17px" onClick="btnRemoveSegment_click()"> </td>
														<td></td>
													</tr>
													<tr>
														<td width="17%"><font>Via 4</font></td>
														<td colspan="3"><select id="selVia4" name="selVia4" size="1" style="width:60px;">
																<OPTION value=""></OPTION>
																<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
															</select>
														</td>												
													</tr>
												</table>	
											</td>
										</tr>
										<tr>
											<td><font>Image</font>
											</td>
											<td><div id="dvUpLoad">
													<input type="file" name="ssrImage">&nbsp;
												</div>
											</td>
											<td align="left" valign="top">&nbsp;&nbsp;<input
												name="btnUpload" type="button" id="btnUpload" value="Upload">
											</td>
										</tr>
										<%-- tr id="trMaxQty">
											<td><font>Max Qty Per Flight</font>
											</td>
											<td colspan="2">
												<input type="text" name="maxQtyPerFlight" id="maxQtyPerFlight" size="20" maxlength="5"/>
												<font class="mandatory">&nbsp;*</font>
											</td>
										</tr --%>
										<tr>
											<td><font> Thumbnail image </font>
											</td>
											<td><div id="dvUpLoadThum">
													<input type="file" name="ssrThumbnailImage">&nbsp;
												</div>
											</td>
											<td align="left" valign="top">&nbsp;&nbsp;<input
												name="btnUploadThumbnail" type="button"
												id="btnUploadThumbnail" value="Upload">
											</td>

										</tr>
										<tr>
											<td><font> Service Display Position </font></td>
											<td><select name="sortOrder" size="1" id="sortOrder"></select>
												<font class="mandatory">&nbsp;*</font>
											</td>

										</tr>
										<tr id="trSsrCutoffTime">
											<td><font> SSR Cut-off Time </font></td>
											<td>
												<input type="text" name="ssrCutoffTime" id="ssrCutoffTime" maxlength="5" style="width:50px" 
												onkeyup="positiveWholeNumberValidate(this)"		onblur="setTimeWithColon(document.forms[0].ssrCutoffTime)" />
											</td>

										</tr>
										<td><font>Valid for PAL/CAl Messages</font>
											</td>
											<td colspan="2"><input type="checkbox" name="validForPalCal"
												id="validForPalCal" value="N">
											</td>
										<td><font>User Defined SSR</font>
											</td>
											<td colspan="2"><input type="checkbox" name="userDefinedSSR"
												id="userDefinedSSR" value="N">
											</td>

									</table>
								</td>
								<td valign="top" width="35%">
									<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" >
										<tr>
											<td><img name="imgSSR" id="imgSSR" src="" alt=""
												height="150">
											</td>
										</tr>
										<tr>
											<td><img name="imgSSRThamb" id="imgSSRThamb" src=""
												alt="" height="150">
											</td>
										</tr>
									</table>								
								</td>

							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input
						name="btnClose" type="button" id="btnClose" value="Close"
						onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						<input name="btnReset" type="button" id="btnReset" value="Reset"
						class="btnDefault"></td>
					<td align="right" valign="bottom"><input name="btnSave"
						type="button" id="btnSave" value="Save" class="btnDefault">
					</td>
				</tr>
				<tr>
					<td><input type="text" name="version" id="version"
						style="display: none;" /> <input type="text" name="createdBy"
						id="createdBy" style="display: none;" /> <input type="text"
						name="createdDate" id="createdDate" style="display: none;" /> <input
						type="text" name="rowNo" id="rowNo" style="display: none;" />
					</td>
				</tr>
			</table>

			<input type="hidden" name="ssrId" id="ssrId"/> 
			<input type="hidden" name="appConsId" id="appConsId"/> 
			<input type="hidden" name="applyConstarints" id="applyConstarints"/>
			<input type="hidden" name="applicableAirports" id="applicableAirports"/> 
			<input type="hidden" name="applicableOnds" id="applicableOnds"/> 
			<input type="hidden" name="consMaxMinTime" id="consMaxMinTime"/> 
			<input type="hidden" name="hdnAirportList" id="hdnAirportList"/> 
			<input type="hidden" name="hdnOndList" id="hdnOndList"/> 
			<input type="hidden" name="hdnMode" id="hdnMode" value="Mode"/>  
			<input type="hidden" name="transitMinMins" id="transitMinMins"/> 
			<input type="hidden" name="transitMaxMins" id="transitMaxMins"/> 
			<input type="hidden" name="shownInIBE" id="shownInIBE"/> 
			<input type="hidden" name="shownInXBE" id="shownInXBE"/> 
			<input type="hidden" name="shownInGds" id="shownInGds"/>
			<input type="hidden" name="shownInWs" id="shownInWs"/>
			<input type="hidden" name="hdnOldAirportList" id="hdnOldAirportList"/>
			<input type="hidden" name="hdnOldOndList" id="hdnOldOndList"/>
			<input type="hidden" name="totalAirportServiceCount"
				id="totalAirportServiceCount"
				value='<c:out value="${requestScope.airportServiceCount}" escapeXml="false" />' />
			<input type="hidden" name="totalInflightServiceCount"
				id="totalInflightServiceCount"
				value='<c:out value="${requestScope.inflightServiceCount}" escapeXml="false" />' />
			<input type="hidden" name="totalAirportTranferCount"
				id="totalAirportTranferCount"
				value='<c:out value="${requestScope.airportTranferCount}" escapeXml="false" />' />
			<input type="text" name="uploadImage" id="uploadImage"
				style="display: none;" /> <input type="text"
				name="uploadImageThumbnail" id="uploadImageThumbnail"
				style="display: none;" /> <input type="hidden"
				name=ssrDescForDisplay id="ssrDescForDisplay" value="">
		</form>
	</div>
	<script>
	<c:out value="${requestScope.reqAirportMulti}" escapeXml="false" />
		var screenId = 'SC_ADMN_036';
		top[2].HideProgress();
	</script>
</body>
</html>