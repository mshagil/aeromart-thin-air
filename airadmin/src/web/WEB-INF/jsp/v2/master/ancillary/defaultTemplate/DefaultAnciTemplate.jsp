<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/manageCharges/ManageChargesValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
   	 	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	    
	    <script type="text/javascript">
	       	var mealTemplSelect = "<c:out value="${requestScope.reqMealTempl}" escapeXml="false" />;"
	       	var seatTemplates = "<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" />";
	       	var baggageTemplates = "<c:out value="${requestScope.reqBaggageTempl}" escapeXml="false" />";
	       
	       	
	    </script>
	    
	    
	    <script type="text/javascript" src="../../js/ancillary/defaultTemplate/DefaultAnciTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 	
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
<!-- --------------------SEARCH PANEL--------------------------------------------------------------------------- -->					
			<div id="divSearchPanel" style="height:30px;text-align:left;">
<!-- 			<form method="post" action="showDefaultAnciTemplate.action" id="frmDefAnciTemplSearch">  -->
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td><font>Ancillary Type</font></td>
						<td>
							<select name="searchCriteria.anciType" size="1" id="selTemplate">
								<option value=""></option>										
								<c:out value="${requestScope.reqAnciTemplType}" escapeXml="false" />
							  </select>
						</td>
						<td><font>Status</font></td>
						<td>
							<select name="searchCriteria.status" size="1" id="selStatus">
								<option value="">All</option>
								<option value="ACT">Active</option>
								<option value="INA">Inactive</option>								
							</select>
						</td>
						<td><font>Origin</font></td>
						<td>
							<select name="searchCriteria.origin" size="1" id="selRouteOrigin">
								<option value=""></option>										
								<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
							  </select>
						</td>
						<td><font>Destination</font></td>
						<td>
							<select name="searchCriteria.depature" size="1" id="selRouteDestination">
								<option value=""></option>										
								<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
							  </select>
						</td>									
						<td align="right">
						<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>					
				</table>
				<!-- </form>	 -->		
			</div>
<!-- ----------------SEARCH RESULTS------------------------------------------------------------------------------- -->		
			<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table id="listAncilTemp" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="temppager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.anci.deftempl.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.anci.deftempl.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.anci.deftempl.delete">
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>
				
			</div>			  		
<!-- ----------------ADD/EDIT PANEL------------------------------------------------------------------------------- -->					
			<div id="divDispDefAnciTemp" style="height:250px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showDefaultAnciTemplate!saveDefaultAnciTemplate.action" id="frmDefAnciTempl"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">

<!-- ---------------------------------- -->
					
					<table width="98%">
						<tr></tr>
						<tr>
							<td><font>Ancillary Type</font></td>
							<td>
								<select id="sleectedAnciType" name="defAnciTemplTO.ancillaryType" size="1" style="width:100px;">
									<option value=""></option>
									<c:out value="${requestScope.reqAnciTemplType}" escapeXml="false" />
								</select><font class="mandatory">&nbsp;*</font>
							</td>
							<td><font>Aircraft Model</font></td>
							<td colspan="2">
								<select id="selectedAirCraftModel" name=" defAnciTemplTO.airCraftModelnumber" size="1" style="width:360px;">
									<option value=""></option>
									<c:out value="${requestScope.reqAircraftModels}" escapeXml="false" />
								</select>
							</td>
						</tr>
						<tr></tr>
						<tr>
							<td><font>Template</font></td>
							<td colspan="1">
								<select id="selectedanciTemplate" name="defAnciTemplTO.anciTemplateId" size="1" style="width:360px;">
									<option value=""></option>
									<c:out value="${requestScope.reAnciTempl}" escapeXml="false" />
								</select><font class="mandatory">&nbsp;*</font>
							</td>
							<td><font>Status</font></td>
							<td>
								<select id="selectedTemplStatus" name="defAnciTemplTO.status" size="1" style="width:100px;">
									<c:out value="${requestScope.reqAnciTemplStatus}" escapeXml="false" />
								</select><font class="mandatory">&nbsp;*</font>
							</td>
					
						</tr>
						<tr></tr>
						<tr>
						<td><font>OND</font></td>
						<td colspan="4">
							<table border="0" width="80%">
								<tr>
									<td width="17%"><font>Departure</font></td>
									<td >
										<select id="selDepature" name="selDepature" size="1" style="width:60px;">
											<option value="***">All</option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td width="16%"><font>Arrival</font></td>
									<td>
										<select id="selArrival" name="selArrival" size="1" style="width:60px;">
											<option value="***">All</option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
								</tr>
								<tr>
									<td width="17%"><font>Via 1</font></td>
									<td colspan="2">
										<select id="selVia1" name="selVia1" size="1" style="width:60px;">
											<OPTION value=""></OPTION>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td rowspan="4">
										<select id="selSelected" size="3" name="selSelected" style="width:150;height:70" multiple>
										</select><font class="mandatory">&nbsp;*</font>
									</td>
								</tr>
								<tr>
									<td width="17%"><font>Via 2</font></td>
									<td ><select id="selVia2" name="selVia2" size="1" style="width:60px;">
											<OPTION value=""></OPTION>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td align="center"><input type="button" id="btnAddSeg" name="btnAddSeg" class="Button" value=">" style="width:40px;height:17px" onClick="btnAddSegment_click()"> </td>
									<td></td>
								</tr>
					
								<tr>
									<td width="17%"><font>Via 3</font></td>
									<td ><select id="selVia3" name="selVia3" size="1" style="width:60px;">
											<OPTION value=""></OPTION>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td align="center"><input type="button" id="btnRemSeg" name="btnRemSeg" class="Button" value="<" style="width:40px;height:17px" onClick="btnRemoveSegment_click()"> </td>
									<td></td>
								</tr>
								<tr>
									<td width="17%"><font>Via 4</font></td>
									<td colspan="3"><select id="selVia4" name="selVia4" size="1" style="width:60px;">
											<OPTION value=""></OPTION>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>		
								</tr>
							</table>
						</td>
						<td></td>
						</tr>
						<tr></tr>
						<tr>
							 <td colspan="2" style="height:25px;"  valign="bottom">
								<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
							        <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
							</td>
							<td align="right" valign="bottom" colspan="2">
								<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
							</td>
						</tr>
						<!-- hidden data-->
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />
							<input type="text" name="defAnciTemplTO.version"  id="version" style="display: none;" />
							<input type="text" name="defAnciTemplTO.createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="defAnciTemplTO.createdDate"  id="createdDate" style="display: none;" />
						    <input type="text" name="defAnciTemplTO.defaultAnciTemplateId"  id="defaultAnciTemplateId" style="display: none;" />
						    <input type="text" name="defAnciTemplTO.ondString"  id="ondString" style="display: none;" />
							<!-- <input type="text" name="defAnciTemplTO.assignedOnds"  id="assignedOnds" style="display: none;" /> -->
					
					</table>

			  </table>					
			  </form>
			</div>	
	
	<script>
		var screenId = 'SC_ADMN_045';
		top[2].HideProgress();
	</script>
  </body>
</html>