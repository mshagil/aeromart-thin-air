<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>OND Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" 
   type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script src="../../js/common/MultiDropDown1.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
    type="text/javascript"></script>
<script src="../../js/baggage/ONDTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>

	<script type="text/javascript">
        var agentsStationsJson = <c:out value="${requestScope.reqAgentStation}" escapeXml="false" /> ;

        <c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
        var flightList ="<c:out value="${requestScope.reqFlightNoList}" escapeXml="false" />";
        
        var bkgClassOptions = "<c:out value="${requestScope.reqBookingCodesList}" escapeXml="false" />";

	</script>

	<div id="divSearchPanel" style="height: 25px; text-align: left;padding-top:5px">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"
			ID="Table9">
			<tr>
				<td colspan="2"><font>OND Baggage Code</font>&nbsp;<select name="selONDCode" size="1" id="selONDCode">
						<option value="">All</option>
						<c:out value="${requestScope.reqONDCodeList}" escapeXml="false" />
				</select>
				</td>
                <td align="right"><input name="btnSearch" type="button"
					id="btnSearch" value="Search"></td>
			</tr>
		</table>
	</div>
	<div id="divResultsPanel"
		style="height: 250px; text-align: left; background-color: #ECECEC;">
		<table id="listONDCode" class="scroll" cellpadding="0"
			cellspacing="0"></table>
		<div id="temppager" class="scroll" style="text-align: center;"></div>
		
		<!--TODO New privileges should be added to the privileges table-->

        <%--<u:hasPrivilege privilegeId="sys.mas.ondbaggage.add">
			<input name="btnAdd" type="button" id="btnAdd" value="Add"
				class="btnDefault">
		</u:hasPrivilege>--%>

		<u:hasPrivilege privilegeId="sys.mas.ondbaggage.edit">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit"
				class="btnDefault">
		</u:hasPrivilege>
		<%--<u:hasPrivilege privilegeId="sys.mas.ondbaggage.delete">
			<input name="btnDelete" type="button" id="btnDelete" value="Delete"
				class="btnDefault">
		</u:hasPrivilege>--%>
	</div>
	<form method="post" action="showONDCode.action"
		id="frmBaggage">

        <table width="100%">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="25%"><font>OND Baggage Template Code </font>
                            </td>
                            <td>
                                <input name="templateCode" type="text" id="templateCode" size="60" class="UCase" maxlength="60">
                                <font class="mandatory">&nbsp;*</font>
                                <input type="hidden" name="templateId" id="templateId" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                <div id="baggageTemplateTabs" class="admin-panel ui-border-topDis">
                <ul class="ui-border-leftDis ui-border-rightDis">
                    <li id="tabBookingClasses"><a href="#tabPaneBookingClasses">Booking Classes</a></li>
                    <li id="tabSalesChannels"><a href="#tabPaneChannels">Sales Channels</a></li>
                    <li id="tabAgents"><a href="#tabPaneAgents">Agents</a></li>
                    <li id="tabFlights"><a href="#tabPaneFlights">Flight Numbers</a></li>
                    <li id="tabONDs"><a href="#tabPaneBaggageONDs">OND Codes</a></li>
                </ul>

                <div id="tabPaneBookingClasses">
                    <table width="50%">
                        <tr>
                            <td width="20%"><select id="bcAvailable" name="bcAvailable" style="width: 150; height: 150" multiple >
                                <option value="ALL">**ALL**</option>
                                <c:out value="${requestScope.reqBookingCodesList}" escapeXml="false" />
                            </select></td>
                            <td width="10%">
                                <table width="100%">
                                    <tr>
                                        <td width="10%" id="addBookingClasses">
                                            <div class="Button" style="width:23px;height: 17px">&gt;</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" id="removeBookingClasses">
                                            <div class="Button" style="width:23px;height: 17px">&lt;</div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="20%"><select id="bcAssigned" size="8" name="bcAssigned" style="width: 150; height: 150" multiple >
                            </select></td>
                        </tr>
                    </table>
                </div>
				<div id="tabPaneChannels">
                     <table width="50%">
                        <tr>
                            <td width="20%">
	                            <select id="channelAvailable" name="channelAvailable" style="width: 150; height: 150" multiple >
	                                
	                            </select>
                            </td>
                            <td width="10%">
                                <table width="100%">
                                    <tr>
                                        <td width="10%" id="addChannel">
                                            <div class="Button" style="width:23px;height: 17px">&gt;</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="10%" id="removeChannel">
                                            <div class="Button" style="width:23px;height: 17px">&lt;</div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="20%"><select id="channelAssigned" size="8" name="channelAssigned" style="width: 150; height: 150" multiple >
                            </select></td>
                        </tr>
                    </table>
                </div>
                <div id="tabPaneAgents">
                    <span id="agentsSpan"></span>
                </div>

                <div id="tabPaneFlights">
                	<span id="flightsSpan"></span> 
                </div>

                <div id="tabPaneBaggageONDs"
                     style="text-align: left;">
                <table width="50%" border="0" cellpadding="0" cellspacing="2"ID="Table1">
                <tr>
                    <td width="20%">
                        <table width="100%">
                            <tr>
                                <td width="50%"><font>Departure</font></td>
                                <td>
                                    <select id="selDepature" name="selDepature" size="1" style="width:60px;">
                                        <option value="All">All</option>
                                        <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"><font>Arrival</font></td>
                                <td>
                                    <select id="selArrival" name="selArrival" size="1" style="width:60px;">
                                        <option value="All">All</option>
                                        <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"><font>Via 1</font></td>
                                <td>
                                    <select id="selVia1" name="selVia1" size="1" style="width:60px;">
                                        <OPTION value=""></OPTION>
                                        <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"><font>Via 2</font></td>
                                <td>
                                    <select id="selVia2" name="selVia2" size="1" style="width:60px;">
                                        <OPTION value=""></OPTION>
                                        <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"><font>Via 3</font></td>
                                <td>
                                    <select id="selVia3" name="selVia3" size="1" style="width:60px;">
                                        <OPTION value=""></OPTION>
                                        <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"><font>Via 4</font></td>
                                <td>
                                    <select id="selVia4" name="selVia4" size="1" style="width:60px;">
                                        <OPTION value=""></OPTION>
                                        <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%">
                        <table width="100%">
                            <tr>
                                <td width="10%" id="addOnd">
                                    <div class="Button" style="width:23px;height: 17px">&gt;</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%" id="removeOnd">
                                    <div class="Button" style="width:23px;height: 17px">&lt;</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%">
                        <select id="assignedOnds" size="8" name="assignedOnds" style="width: 150; height: 150" multiple >
                        </select>
                    </td>
                </tr>
                </table>
                </div>

                </div>

                </td>
            </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                    <tr>
                        <td colspan="3" style="height: 25px;" valign="bottom"><input
                                name="btnClose" type="button" id="btnClose" value="Close"
                                onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
                            <input name="btnReset" type="button" id="btnReset" value="Reset"
                                   class="btnDefault"></td>
                        <td align="right" valign="bottom"><input name="btnSave"
                                                                 type="button" id="btnSave" value="Save" class="btnDefault">
                        </td>
                    </tr>
                    </tr>
                </table>
            </td>
        </tr>
        </table>



    </form>
	<script>
		var screenId = 'SC_ADMN_041';
		top[2].HideProgress();
	</script>
</body>
</html>