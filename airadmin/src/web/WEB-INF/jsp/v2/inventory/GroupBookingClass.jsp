<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<style>
			.LClass{
				float:left;
			}
			.LClass{border-right:1px solid #888;border-top:1px solid #888;border-bottom:1px solid #888}
			.LClass .LClassHead{border-bottom:1px solid #888;}
			.LClass .LClassBody{height:450px; overflow-y:auto;background: #fff}
			.LClassHead input {margin:2px 5px;}
			#groupLCClass{border-left:1px solid #888;width:99%}
			.mOveritem, .newGroup{background: #333! important;}
			.mOveritem *, .newGroup *{color:#fff! important;}
			.ClS{border-bottom:1px solid #999;}
			.ClS td.bc{padding:5px 0;text-align:center}
		</style>
		
  	</head>
  	<body style="background-color:#fff;">
		<div id="divGroupBookingClass" style="background-color:#ECECEC;" align="center">
			<table width="100%" border="0">
			<tr>
				<td align="right" style="padding:3px 6px;" colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td align="left" width="85%">
								<div id="colorCodes">
									
								</div>
							</td>
							<td align="right" width="15%">
								<label>COS</label> :<select id='selBookingClass' style='width:100px;'></select>
							</td>
						</tr>
					</table>
					
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<div id="groupLCClass" >
							
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align="left"><input type="button" id="btnReload" name="btnReload" class="Button" value="Refresh"/></td>
				<td align="right" style="padding:3px 6px;">
					<u:hasPrivilege privilegeId="plan.invn.bc.group.add">
						<input type="button" name="btnGroup" id="btnGroup" class="Button" value="Create Group" style="width:auto"/>
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="plan.invn.bc.group.delete">
						<input type="button" name="btnDelGroup" id="btnDelGroup" class="Button" value="Delete Group" style="width:auto"/>
					</u:hasPrivilege>
					<input type="button" name="btnSave" id="btnSave" class="Button" value="Save" />
					<input type="button" id="btnClose" name="btnClose" class="Button" value="Close"/>
				</td>
			</tr>
			</table>
		</div>
  	</body>
  	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  	 	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/v2/inventory/GroupBookingClass.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</html>