	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<style>
		.google-visualization-table-table{font-size: 8pt;}
	</style>
<script type="text/javascript"><!--
//Tab 1 data
var dataProLost = new google.visualization.DataTable();
				dataProLost.addColumn('string', '');
      			dataProLost.addColumn('number', 'Revenue');
		        dataProLost.addColumn('number', 'Expenses');
		        dataProLost.addColumn('number', 'Profit');

		        dataProLost.addRows(4);
		        dataProLost.setValue(0, 0, 'Q1-2009');
		        dataProLost.setValue(0, 1, 500);
		        dataProLost.setValue(0, 2, 520);
		        dataProLost.setValue(0, 3, -20);
		        
		        dataProLost.setValue(1, 0, 'Q2-2009');
		        dataProLost.setValue(1, 1, 600);
		        dataProLost.setValue(1, 2, 600);
		        dataProLost.setValue(1, 3, 0);
		        
		        dataProLost.setValue(2, 0, 'Q3-2009');
		        dataProLost.setValue(2, 1, 500);
		        dataProLost.setValue(2, 2, 600);
		        dataProLost.setValue(2, 3, -100);
		        
		        dataProLost.setValue(3, 0, 'Q4-2009');
		        dataProLost.setValue(3, 1, 600);
		        dataProLost.setValue(3, 2, 590);
		        dataProLost.setValue(3, 3, 10);

var dataOperatingCost = new google.visualization.DataTable();
						dataOperatingCost.addColumn('string', 'Year');
						dataOperatingCost.addColumn('number', 'Cost');
						dataOperatingCost.addColumn('number', 'Revenue');
						dataOperatingCost.addRows([
						  ['2004', 1000, 400],
						  ['2005', 1170, 460],
						  ['2006', 660, 1120],
						  ['2007', 1030, 540]
						]);


var dataServices = new google.visualization.DataTable();
				dataServices.addColumn('string', '');
      			dataServices.addColumn('number', 'Product1');
		        dataServices.addColumn('number', 'Product2');
		        dataServices.addColumn('number', 'Product3');
		        dataServices.addColumn('number', 'Product4');
		        dataServices.addColumn('number', 'Product5');

		        dataServices.addRows(1);

		        dataServices.setValue(0, 0, 'product');
		        dataServices.setValue(0, 1, 68);
		        dataServices.setValue(0, 2, 80);
		        dataServices.setValue(0, 3, 70);
		        dataServices.setValue(0, 4, 68);
		        dataServices.setValue(0, 5, 68);		        

var dataProjectCost = new google.visualization.DataTable();
      			dataProjectCost.addColumn('number', 'Fuel');
		        dataProjectCost.addColumn('number', 'Payroll');
		        dataProjectCost.addColumn('number', 'Amortization');
		        dataProjectCost.addColumn('number', 'Airport Fees');
		        dataProjectCost.addColumn('number', 'Maintenance');
		        dataProjectCost.addColumn('number', 'Advertising');

		        dataProjectCost.addRows(1);
		        
		        dataProjectCost.setValue(0, 0, 35000);
		        dataProjectCost.setValue(0, 1, 28000);
		        dataProjectCost.setValue(0, 2, 10000);
		        dataProjectCost.setValue(0, 3, 12000);
		        dataProjectCost.setValue(0, 4, 15000);
		        dataProjectCost.setValue(0, 5, 8000);

// Tab 2 Data
	var dataTop = {
        cols: [{id: 'route', label: 'Route', type: 'string'},
            {id: 'revenue', label: 'Revenue', type: 'number'},
            {id: 'cost', label: 'Cost', type: 'number'},
            {id: 'profit', label: 'Profit', type: 'number'}],
        rows: [{c:[{v: 'SHJ-CMB '},  {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-SAW'},  {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-DEL'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-DOH'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-BOM'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-DAM'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-KTM'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-KUL'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-NBO'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-ATH'}, {v: 492}, {v: 410}, {v: 460}]}
            ]
            };

var dataSummary = {
        cols: [{id: 'date', label: 'Date', type: 'string'},
            {id: 'fcap', label: 'F Cap', type: 'number'},
            {id: 'fload', label: 'F Load', type: 'number'},
            {id: 'jcap', label: 'J Cap', type: 'number'},
            {id: 'jload', label: 'J Laod', type: 'number'},
            {id: 'ycap', label: 'Y Cap', type: 'number'},
            {id: 'yload', label: 'Y Load', type: 'number'},
            {id: 'ttlcap', label: 'TTL Cap', type: 'number'},
            {id: 'ttlload', label: 'TTL Load', type: 'number'},
            {id: 'rev', label: 'Revenue', type: 'number'},
            {id: 'profit', label: 'Profit', type: 'number'}],
        rows: [{c:[{v: '16/6/10 '},  {v: 49}, {v: 25}, {v: 36},
                   {v: 2}, {v: 132}, {v: 158},
                   {v: 100}, {v: 100},
                   {v: 82}, {v: 60}]},
                   {c:[{v: '16/6/10 '},  {v: 49}, {v: 25}, {v: 36},
                       {v: 2}, {v: 132}, {v: 158},
                       {v: 100}, {v: 100},
                       {v: 82}, {v: 70}]},
                       {c:[{v: '16/6/10 '},  {v: 49}, {v: 25}, {v: 36},
                           {v: 2}, {v: 132}, {v: 158},
                           {v: 100}, {v: 100},
                           {v: 82}, {v: 40}]},
                           {c:[{v: '16/6/10 '},  {v: 49}, {v: 25}, {v: 36},
                               {v: 2}, {v: 132}, {v: 158},
                               {v: 100}, {v: 100},
                               {v: 82}, {v: 40}]},
                               {c:[{v: '16/6/10 '},  {v: 49}, {v: 25}, {v: 36},
                                   {v: 2}, {v: 132}, {v: 158},
                                   {v: 100}, {v: 100},
                                   {v: 82}, {v: 40}]},
                                   {c:[{v: '16/6/10 '},  {v: 49}, {v: 25}, {v: 36},
                                       {v: 2}, {v: 132}, {v: 58},
                                       {v: 100}, {v: 100},
                                       {v: 82}, {v: 40}]}
            
            ]
            };

var dataBottom = {
        cols: [{id: 'route', label: 'Route', type: 'string'},
            {id: 'revenue', label: 'Revenue', type: 'number'},
            {id: 'cost', label: 'Cost', type: 'number'},
            {id: 'profit', label: 'Profit', type: 'number'}],
        rows: [{c:[{v: 'SHJ-ATH'},  {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-SAW'},  {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-DEL'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-DOH'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-BOM'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-DAM'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-KTM'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-KUL'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-NBO'}, {v: 492}, {v: 410}, {v: 460}]},
            {c:[{v: 'SHJ-ATH'}, {v: 492}, {v: 410}, {v: 460}]}
            ]
            };

function initTabOneChart(dataset1,dataset2,dataset3,dataset4){
	var tab1chart1 = new google.visualization.ColumnChart(document.getElementById('chart1_divTab1'));
	tab1chart1.draw(dataset1, {width: 316, height: 170, title: ''});
    var tab1chart2 = new google.visualization.AreaChart(document.getElementById('chart2_divTab1'));
    tab1chart2.draw(dataset2, {width: 316, height: 170, title: ''});
    var tab1chart3 = new google.visualization.BarChart(document.getElementById('chart3_divTab1'));
    tab1chart3.draw(dataset3, {width: 316, height: 170, title: '',colors:['#07ca09','#09fd0c','#48fe4a','#8efc90','#d1fcd1']});
    var tab1chart4 = new google.visualization.BarChart(document.getElementById('chart4_divTab1'));
    tab1chart4.draw(dataset4, {width: 316, height: 170, title: ''});
}


function initProfitability(){
	function drawChart(dat){
	 	var JSONObject = dat;
	      var data = new google.visualization.DataTable(JSONObject, 0.5);
	      
	      //var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	      //chart.draw(data, {width: 316, height: 185, title: 'DXB - CDG'});
	      
	      visualization = new google.visualization.Table(document.getElementById('table-data'));
	      visualization.draw(data, {'allowHtml': true,width: 265, height: 185});
	};

	$("#tabs").tabs({
		select: function(event, ui) {
			var selected = ui.index;
			if (selected == 0){
				//GUnload();
				initTabOneChart(dataProLost,dataOperatingCost,dataServices,dataProjectCost)
			}else if (selected == 1){
				//GUnload()
				drawChart(dataTop);
				drawChart1();
				//drawMap();
				initializeMap();
				drawSummary(dataSummary);
			}else{
				//GUnload()
			}
		}
	});
	initTabOneChart(dataProLost,dataOperatingCost,dataServices,dataProjectCost);
	$("#routeSelector").change(function(){
		if ($(this).val() == "top10")
			drawChart(dataTop)
		else
			drawChart(dataBottom)	
				
	});

	$("#graphSelector").change(function(){
		$("#chart_div1").hide();
		$("#table_div1").hide();
		if ($(this).val() == "graph")
			$("#chart_div1").show();
		else
			$("#table_div1").show();
				
	});


	function drawChart1(){
	 	var JSONObject = {
	          cols: [{id: 'period', label: '&nbsp', type: 'string'},
	              {id: 'RAK', label: 'Qatar Airways', type: 'number'},
	              {id: 'IND', label: 'Emirates', type: 'number'},
	              {id: 'FLY', label: 'TAROM', type: 'number'},
	              {id: 'EMI', label: 'Turkish', type: 'number'}],
	          rows: [{c:[{v: 'High'}, {v: 340}, {v: 492}, {v: 410}, {v: 460}]},
	              {c:[{v: 'Medium'}, {v: 313}, {v: 492}, {v: 410}, {v: 460}]},
	              {c:[{v: 'Low'}, {v: 313}, {v: 492}, {v: 410}, {v: 460}]}]};
	    
	      var data = new google.visualization.DataTable(JSONObject, 0.5);
	      
	      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
	      chart.draw(data, {width: 636, height: 150, title: 'DXB - CDG'});
	      
	      visualization = new google.visualization.Table(document.getElementById('table_div1'));
	      visualization.draw(data, {'allowHtml': true,width: 636, height: 150});
	};

	function drawSummary(dat){
		var JSONObject = dat;
	      var data = new google.visualization.DataTable(JSONObject, 0.5);
	      
	      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
	      chart.draw(data, {width: 650, height: 150, title: 'Route Summary'});
	      
	      visualization = new google.visualization.Table(document.getElementById('table_div1'));
	      visualization.draw(data, {'allowHtml': true,width: 650, height: 150});
	};
	/*
	function drawMap() {
		$("#mapID").html('<div id="map_canvas" style="width: 376px; height: 180px"></div>');
		if (map)
			map.empty();
	    var data = new google.visualization.DataTable();
	    data.addColumn('number', 'Lat');
	    data.addColumn('number', 'Lon');
	    data.addColumn('string', 'Name');
	    data.addRows(4);
	    data.setCell(0, 0, 37.4232);
	    data.setCell(0, 1, -122.0853);
	    data.setCell(0, 2, 'Work');
	    data.setCell(1, 0, 37.4289);
	    data.setCell(1, 1, -122.1697);
	    data.setCell(1, 2, 'University');
	    data.setCell(2, 0, 37.6153);
	    data.setCell(2, 1, -122.3900);
	    data.setCell(2, 2, 'Airport');
	    data.setCell(3, 0, 37.4422);
	    data.setCell(3, 1, -122.1731);
	    data.setCell(3, 2, 'Shopping');
	    var geoView = new google.visualization.DataView(data);
	    geoView.setColumns([0, 1]);

	    var map = new google.visualization.Map(document.getElementById('map_canvas'));
	    map.draw(geoView, {showTip: false,zoomLevel:3,mapType:'terrain',showLine:true});
	 }
*/
	function initializeMap() {
	      /*if (GBrowserIsCompatible()) {
	        var map = new GMap2(document.getElementById("map_canvas"));
	        map.setCenter(new GLatLng(48.3, 28.5), 2);
	        var polyOptions = {geodesic:true};
	        var polyline = new GPolyline([
	        new GLatLng(25.3, 55.5),
	        new GLatLng(6.05, 80.5)
	    	], "#27BC27", 2, 1, polyOptions);
	    	map.addOverlay(polyline);
	        map.setMapType(G_PHYSICAL_MAP);
	      }else{*/
	    	 // $("#map_canvas").html('<img alt="map" src="../../images/map_no_cache.gif"/>');
		  //}
	};


	function drawSummary(dat){
		var JSONObject = dat;
	      var data = new google.visualization.DataTable(JSONObject, 0.5);
	      
	      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
	      chart.draw(data, {width: 670, height: 150, title: 'Route Summary'});
	      
	      visualization = new google.visualization.Table(document.getElementById('table_div1'));
	      visualization.draw(data, {'allowHtml': true,width: 670, height: 150});
	};
	
}


--></script>
			<div id="tabs" class="tabs-bottom">
				<ul>
					<li><a href="#tabs-1">Overview</a></li>
					<li><a href="#tabs-2">Routes</a></li>
					<li><a href="#tabs-3">Cost Entry</a></li>
				</ul>
				<div id="tabs-1">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width='50%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td valign='top'>
																<font><b>Profit/Loss </b></font>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
														<img src="../../images/spacer_no_cache.gif"></td></tr>
														<tr>
															<td style='height:170px;' align='center'>
																<div id="chart1_divTab1"></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
											<td width='50%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td colspan='2'>
																<font><b>Operating Cost vs Revenue </b></font>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
														<img src="../../images/spacer_no_cache.gif"></td></tr>
														<tr>
															<td colspan='2' valign='top' style='height:170px;'>
																<div id="chart2_divTab1"></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
										<tr>
											<td width='50%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td valign='top'>
																<font><b>Services </b></font>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
														<img src="../../images/spacer_no_cache.gif"></td></tr>
														<tr>
															<td style='height:170px;' align='center'>
																<div id="chart3_divTab1"></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
											<td width='50%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td colspan='2'>
																<font><b>Projected Cost </b></font>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
														<img src="../../images/spacer_no_cache.gif"></td></tr>
														<tr>
															<td colspan='2' valign='top' style='height:170px;'>
																<div id="chart4_divTab1"></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
									</table>

				</div>
				<div id="tabs-2">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width='35%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td valign='top'>
																<font><b>Route Activity </b></font>
															</td>
															<td valign='top' align="right">
																<select id="routeSelector">
																	<option value="top10">Top 10</option>
																	<option value="bot10">Bottom 10</option>
																</select>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
														<img src="../../images/spacer_no_cache.gif"></td></tr>
														<tr>
															<td style='height:190px;' align='left' colspan="2">
																<div id="table-data"></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
											<td width='65%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td>
																<font><b>Hub Activity</b></font>
															</td>
															<td valign='top' align="right">
																<select id="hubSelector">
																	<option value="all">All</option>
																	<option value="25.3:55.5|6.05:80.5">SHJ-CMB</option>
																</select>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
															<img src="../../images/spacer_no_cache.gif">
														</td></tr>
														<tr>
															<td colspan='2' valign='top' style='height:190px;' colspan="2" id="mapID">
																<div id="map_canvas" style="width: 376px; height: 180px">
																	<img alt="map" src="../../images/map_no_cache.gif" width="376"/>
																</div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
										<tr>
											<td width='100%' valign='top' colspan="2">
												<%@ include file="inc_paneTop.jsp"%>
													<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td valign='top'>
																<font><b>Route Summary</b></font>
															</td>
															<td valign='top' align="right">
																<select id="graphSelector">
																	<option value="table">Table</option>
																	<option value="graph">Graph</option>
																</select>
															</td>
														</tr>
														<tr><td style="height: 5px;border-bottom:1px solid #999;" colspan="2">
														<img src="../../images/spacer_no_cache.gif">
														</td></tr>
														<tr>
															<td style='height:130px;' align="left" colspan="2">
																<div id='chart_div1' style="display: none"></div>
																<div id='table_div1' ></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
									</table>
				</div>
				<div id="tabs-3">
					<div style="font-size: 14px;color: #ff0000">Request denied</div>
				</div>
			</div>	