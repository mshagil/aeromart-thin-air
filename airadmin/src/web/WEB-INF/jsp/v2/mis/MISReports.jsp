<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>MIS Report</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
    <%@ include file="inc_pgHD.jsp" %>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">	
	<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/ToolTip.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jQuery.print.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/v2/jquery/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<!--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAACXT-lkE8fE8wXllNk8A1lxRKNzJ-_9njnryRTbvC6CtrS4sRvRQMUxuDZ2esmpnd4yEZNl6cw_Qi3w"></script>--><!--
	ABQIAAAACXT-lkE8fE8wXllNk8A1lxTLg5yOzItCYQmLHjmmQy9DuLCNKhQ3koZ0sTrmLMPdwxOQ6eTHENs9Eg
	
 	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAACXT-lkE8fE8wXllNk8A1lxTLg5yOzItCYQmLHjmmQy9DuLCNKhQ3koZ0sTrmLMPdwxOQ6eTHENs9Eg" type="text/javascript"></script>
 	-->
	<script type="text/javascript">
		google.load('visualization', '1', {packages: ['table','corechart']});
	</script>
	<script type="text/javascript">
	<!--

	var DATA_MIS = new Array();
	DATA_MIS["initialParams"] = eval('(' + '<c:out value="${requestScope.req_InitialParams}" escapeXml="false" />' + ')');

	var jsRevGraphData = {fareValues: {},chargeValues: {}};
	var jsRevenue = {};				
	
	var jsBkgYTD = ({budget:0, value:0});
	var jsBkgChnl ={ channelWiseBookings:{}};
	var jsDisBkgChnl = { highestValue:0,bookingValues:{ channelWiseBookings : {}} };
	var jsRevChnl={};
	var jsPP ={};
	//-->
	</script>
	<style type="text/css">
	table.sample {
	border-width: 1px;
	border-spacing: ;
	border-style: outset;
	border-color: gray;
	border-collapse: separate;
	background-color: white;}
	
	</style>

  </head>
  <body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)'>
  <form id='frmAction' name='frmAction' method='post' action=''>
	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
  		<tr>
			<td valign='top'>
				<%@ include file="../../common/IncludeFormTop.jsp"%>Search<%@ include file="../../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">	
							<tr>
								<td colspan='8' style='height:5px;'></td>
							</tr>	
							<tr>
								<td>
									<font>From Date :</font>
								</td>
								<td>
									<table border='0' cellpadding='0' cellspacing='1'>
										<tr>
											<td><input type='text' id='txtFDate' name='txtFDate' style='width:80px;' maxlength="10" /></td>
										</tr>
									</table>
								</td>
								<td>
									<font>To Date :</font>
								</td>
								<td>
									<table border='0' cellpadding='0' cellspacing='1'>
										<tr>
											<td><input type='text' id='txtTDate' name='txtTDate' style='width:80px;' maxlength="10"></td>
										</tr>
									</table>
								</td>
								<td>
									<!-- <font>From :</font> -->
								</td>
								<td>
									<select id='selFrom' name='selFrom' size='1' style='display:none'></select>
								</td>
								<td>
									<!-- <font>To Date :</font> -->
								</td>
								<td>
									<select id='selTo' name='selTo' size='1' style='display:none'></select>
								</td>
							</tr>
							<tr>
								
								<td>
									<font>Region :</font>
								</td>
								<td>
									<select id='selRegion' name='selRegion' size='1' style='width:100px;'>
									</select>
								</td>
								<td>
									<font>Station :</font>
								</td>
								<td>
									<select id='selPOS' name='selPOS' size='1' style='width:100px;'>
									</select>
								</td>
								<td align="right" colspan='4'>
									<input name="btnCancel" type="button" class="button" id="btnCancel"  value="Close">
									<input name="btnReset" type="button" class="button" id="btnReset"  value="Reset">
									<input name="btnSearch" type="button" class="button" id="btnSearch" value="View">
								</td> 
							</tr>
						</table>
				<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr>
			<td style='height:5px;'>
			</td>
		</tr>
		<tr>
			<td><div id='divResultPane' style='display:none;'>
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td style='background-image:url(../../images/M004_no_cache.jpg);background-repeat:repeat-x;background-position:bottom;'>
							<table border='0' cellpadding='0' cellspacing='0'>
								<tr>
											
										<td id='tdTab1' class='cursorPointer' style='width:90px;height:22px;background-image:url(../../images/M012_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(1)'><font class="FormHeader">Dashboard</font></td>
										<td style='width:2px;'></td>
										<td id='tdTab2' class='cursorPointer' style='width:90px;background-image:url(../../images/M013_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(2)'><font class="FormHeader">Distribution</font></td>
										<td style='width:2px;'></td>
										<td id='tdTab3' class='cursorPointer' style='width:90px;background-image:url(../../images/M013_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(3)'><font class="FormHeader">Regional</font></td>
										<td style='width:2px;'></td>
										<td id='tdTab4' class='cursorPointer' style='width:90px;background-image:url(../../images/M013_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(4)'><font class="FormHeader">Agents</font></td>
										<td style='width:2px;'></td>
										<td id='tdTab5' class='cursorPointer' style='width:90px;background-image:url(../../images/M013_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(5)'><font class="FormHeader">Accounts</font></td>
										<td style='width:2px;'></td>
										<td id='tdTab6' class='cursorPointer' style='width:90px;background-image:url(../../images/M013_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(6)'><font class="FormHeader">Ancillary</font></td>
										<td style='width:2px;'></td>
										<td id='tdTab7' class='cursorPointer' style='width:90px;background-image:url(../../images/M013_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='UI_MISReports.tblClick(7)'><font class="FormHeader">Profitability</font></td>
									
								</tr>
							</table>
						</td>		
					</tr>
					<tr>
						<td>						
							<span id='spnTabPane1'>
								<%@ include file="inc_MISDashBoard.jsp" %>								
							</span>
							
							<span id='spnTabPane2' style='position:absolute;overflow-X:hidden; overflow-Y:auto;display:none;height:493px;width:730px;'>
								<%@ include file="inc_MISDistribution.jsp" %>	
							</span>
							
							<span id='spnTabPane3' style='position:absolute;overflow-X:hidden; overflow-Y:auto;display:none;height:493px;width:730px;'>
								<%@ include file="inc_MISRegional.jsp" %>	
							</span>
							
							<span id='spnTabPane4' style='position:absolute;overflow-X:hidden; overflow-Y:hidden;display:none;height:493px;width:730px;'>
								<%@ include file="inc_MISAgents.jsp" %>
							</span>
							
							<span id='spnTabPane5' style='position:absolute;overflow-X:hidden; overflow-Y:auto;display:none;height:493px;width:730px;'>
								<%@ include file="inc_MISAccounts.jsp" %>
							</span>
							
							<span id='spnTabPane6' style='position:absolute;overflow-X:hidden; overflow-Y:auto;display:none;height:493px;width:730px;'>
								<%@ include file="inc_MISMisc.jsp" %>
							</span>
							
							<span id='spnTabPane7' style='position:absolute;overflow-X:hidden; overflow-Y:auto;display:none;height:493px;width:730px;'>
								<%@ include file="inc_MISMiscProfitability.jsp" %>
							</span>
						</td>
					</tr>
				</table></div>
			</td>
		</tr>
	</table>
	</form>
	<form id="frmExcelReport" name="frmExcelReport">
	<input
	type="hidden" id="hdnFromDate" name="hdnFromDate" /> <input type="hidden" id="hdnToDate" name="hdnToDate" />
	</form>

<span id='spnGraphPopup'
	style='position: absolute; top: 300px; left: 100px; width: 500px; display: none;'>
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td valign='top'><%@ include
			file="../../common/IncludeFormTop.jsp"%>Product Sales 
		Graph<%@ include file="../../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="1" cellspacing="0">
			<tr>
				<td colspan="2" width="100%"><div id="divOuter" style="overflow:auto;width:450px"><div id="divPopupGraph" style="margin-left:20px"/></div></div></td>
			</tr>
			<tr>
				<td><input name="btnCancel" type="button" class="button"
					id="btnCancel" onClick="Close_Popup();" value="Close"></td>				
			</tr>
		</table>
		<%@ include file="../../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
</table>
</span>
	
<span id='spnFeedBack'
	style='position: absolute; top: 300px; left: 200px; width: 400px; display: none;'>
<table width='100%' border='0' cellpadding='0' cellspacing='0'>
	<tr>
		<td valign='top'><%@ include
			file="../../common/IncludeFormTop.jsp"%>Send
		Feedback<%@ include file="../../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="1" cellspacing="0">
			<tr>
				<td colspan='2' style='height: 5px;'></td>
			</tr>
			<tr>
				<td><font>To :</font></td>
				<td><input type='text' id='txtFeedBackTo' name='txtFeedBackTo'
					style='width: 300px;'></td>
			</tr>
			<tr>
				<td valign='top'><font>Feedback :</font></td>
				<td><textarea id='txtFeedBack' name='txtFeedBack'
					style='height: 100px; width: 300px;'></textarea></td>
			</tr>
			<tr>
				<td><input name="btnCancel" type="button" class="button"
					id="btnCancel" onClick="pgTab0ButtonClick(0)" value="Cancel"></td>
				<td align='right'><input name="btnCancel" type="button"
					class="button" id="btnCancel" onClick="pgTab0ButtonClick(1)"
					value="Send"></td>
			</tr>
		</table>
		<%@ include file="../../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
</table>
</span>
<div id="spnTT" name="spnTT" style="position: absolute; z-index: 1000;"></div>

<script type='text/javascript'
	src='../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>

<script type='text/javascript'>
	<!--
	var arrAirports = new Array();
	var arrAllRegions = new Array();
	var arrAllSalesChannels = new Array();
		
	// Airports
	<c:out value='${requestScope.req_Airports}' escapeXml='false' />
	<c:out value='${requestScope.req_Regions}' escapeXml='false' />
	<c:out value='${requestScope.req_salesChannels}' escapeXml='false' />
	//-->
	</script> <script type='text/javascript'
	src='../../js/v2/mis/tabMISDashBoard.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
<script type='text/javascript'
	src='../../js/v2/mis/tabMISDistribution.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
<script type='text/javascript'
	src='../../js/v2/mis/tabMISRegional.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
<script type='text/javascript'
	src='../../js/v2/mis/tabMISAgents.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
<script type='text/javascript'
	src='../../js/v2/mis/tabMISAccounts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
<script type='text/javascript'
	src='../../js/v2/mis/tabMISMisc.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>

<script type='text/javascript'
	src='../../js/v2/mis/MISReports.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
</body>
</html>
