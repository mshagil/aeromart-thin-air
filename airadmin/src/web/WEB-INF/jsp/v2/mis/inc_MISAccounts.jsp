									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width='60%'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
															<td>
																<font><b>Sales Summary</b></font>
															</td>
														</tr>
														<tr>
															<td valign='top' style='height:142px;'>
																<table id='tblSalesSummary' width='100%' border='0' cellpadding='1' cellspacing='1' style='border-top:1px solid grey;border-bottom:1px solid grey;border-left:1px solid grey'>
																	<tbody></tbody>
																</table>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
											<td width='40%'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													  <tr>
														<td>
																<font><b>Comparative Sales Summary</b></font>
														</td>
													  </tr>
													  <tr>
														<td valign='top'>
																<div id='imgSales'></div>
														</td>
													   </tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
										<tr>
											<td colspan='2'>
												<%@ include file="inc_paneTopDark.jsp" %>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td>
																<font><b>Total Refunds : </b></font>
															</td>
															<td>
																<font><div id='divTotalRefunds'>0.00</div></font>
															</td>
															<td>
																<font><b>Total Sales : </b></font>
															</td>
															<td>
																<font><div id='divTotalSales'>0.00</div></font>
															</td>
															<td style='width:50px;'></td>
															<td valign='top'>
																<font><b>Total Outstanding Invoices :</b></font>
															</td>
															<td>
																<font><div id='divTotalCreditAmt'>0.00</div></font>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottomDark.jsp" %>
											</td>
										</tr>
										<tr>
											<td>
												<%@ include file="inc_paneTop.jsp" %>
													<table width='100%' border='0' cellpadding='0' cellspacing='0' height='233px'>
														<tr>
															<td>
																<font><b>Outstanding By Region</b></font>
															</td>
														</tr>
														<tr>
															<td valign='top'>
																<table id='tblOutPayments'  width='100%' border='0' cellpadding='1' cellspacing='1' style='border-top:1px solid grey;border-bottom:1px solid grey;border-left:1px solid grey'>
																	<tbody></tbody>
																</table>
															</td>
														</tr>
														<tr>
															<td colspan="3">
																<font style='fntSmall'>Note :This section shows outstanding invoice total for each region for a given date range for ALL regions and ALL stations.</font>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>	
											</td>
											<td>
												<%@ include file="inc_paneTop.jsp" %>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td>
																<font><b>Outstanding Invoices (Top 10)</b></font>
															</td>
														</tr>
														<tr>
															<td>
																&nbsp;
															</td>
														</tr>
														<tr>
															<td valign='top'>
															<div id="divPastInv" style="overflow:auto;height:200px;" >
																<table id='tblPastInv' width='100%' border='0' cellpadding='1' cellspacing='1' style='border-top:1px solid grey;border-bottom:1px solid grey;border-left:1px solid grey'>
																	<tbody></tbody>
																</table>
															</div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>	
											</td>
										</tr>
									</table>
									
