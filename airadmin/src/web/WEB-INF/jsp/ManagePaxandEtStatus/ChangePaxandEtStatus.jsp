
<%-- 
	 @Author 	: 	Bimsara V
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<%@ page language="java"%>
<%@ page import="java.util.*, java.text.*"%>
<%
	Date dStartDate = null;

	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH, 1);
	String CurrentDate = formatter.format(dStartDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Change PAX & ET Status</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">

<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">


<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form_last_worked_ver.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/ManagePaxandEtStatus/ChangePaxandEtStatus.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

</head>

<body class="tabBGColor" scroll="no" onkeypress=''
	oncontextmenu="return false" onkeydown='' ondrag='return false'>
	<script type="text/javascript">
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
	<form method="post" action="showChangePAXandETStatus.action"
		id=frmFlightPaxSt>
		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td>
					<table width="99%">
						<tr>
							<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
							</td>

						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Search Flight <%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="Table2">
						<tr>
							<td align="left"><font>Flight Number</font></td>
							<td align="left"><input tabindex="1" type="text"
								name="txtFlightNo" id="txtFlightNo" size="10" maxlength="8">
								<font class="mandatory"> &nbsp;*</font></td>

							<td align="left"><font>Dept Date </font></td>
							<td align="left"><input tabindex="2" type="text"
								name="txtDept" id="txtDept" size="12" maxlength="10"> <font
								class="mandatory"> &nbsp;*</font></td>

							<td align="right"><input tabindex="3" name="btnSearch"
								type="button" class="button" id="btnSearch" value="Search"></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Flown
					Flight<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="98%" border="0" cellpadding="0" cellspacing="4"
						ID="Table9">
						<tr>
							<td colspan="2">
								<table id="tblFlownFlights" width='98%' border='0'
									cellpadding='0' cellspacing='0'></table>
								<div id="divFlownFlightsPager" class="scroll"
									style="text-align: center;"></div>
							</td>
						</tr>
						<tr>
							<td><input tabindex="4" type="button" id="btnClose"
								class="Button" value="Close"></td>
							<td align="right"><input tabindex="5" name="btnUpdateStatus"
								type="button" class="Button" id="btnUpdateStatus"
								value="Update Status" style="width: 100px;"> &nbsp;</td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>

		</table>
		<input type="hidden" name="hdnMode" id="hdnMode" /> <input
			type="hidden" name="deptDate" id="deptDate" /> <input type="text"
			name="flightNumber" id="flightNumber" style="display: none;" /> <input
			type="hidden" name="hdnCurrentDate" id="hdnCurrentDate"
			value="<%=CurrentDate %>">


	</form>
</body>
<script>
	var screenId = 'SC_SHDS_0038';
	top[2].HideProgress();
</script>


</html>

