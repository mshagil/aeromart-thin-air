<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js"></script>
	<script src="../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.pstrength.js"></script>
		<script language="javascript">
		var isReset = "<c:out value='${requestScope.pwdResetMsg}' escapeXml='false' />"; 
	</script>
	<script src="../../js/passwordchange/PasswordChange.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body class="tabBGColor" onLoad="onLoad()" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' oncontextmenu="return false">
  <form id="formChangePassword" name="formChangePassword" method="post" action="changePassword.action">
	<script type="text/javascript">
			<c:out value="${requestScope.logUID}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
		<table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>Change Password <%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="1" cellspacing="1" align="center">
								<tr>
									<td width="100%" colspan="3">
										<div id="pwdReset" name="pwdReset" valign="center"><br><img src="../../images/icons/info_no_cache.gif"  align="middle">&nbsp;&nbsp;<font>Your password has been reset by the administrator. Please change your password and 
										sign in again.<br></font></div>
									</td>							 
								</tr>
							<tr>
								<td width="20%">
									<font>User ID </font>
								</td>
								<td width="28%" align="left">
									<input type="text" name="txtUserId" id="txtUserId" size="20" onChange="pageOnChange()" DISABLED>
									<font class="mandatory">&nbsp;&nbsp;</font> 
								</td>
							</tr>
							<tr>
								<td>
									<font>Old Password </font>
								</td>
								<td align="left">
									<input type="password" name="pwdOld" id="pwdOld" size="20" maxlength="12" onChange="pageOnChange()"><font class="mandatory"> *</font>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<font>New Password </font>
								</td>
								<td valign="top" align="left">
									<input type="password" name="pwdNew" id="pwdNew"size="20" maxlength="12" onChange="pageOnChange()" class="password">
								</td>
								<td></td>
							</tr>
							<tr>
								<td>
									<font>Confirm Password </font>
								</td>
								<td align="left">
									<input type="password" name="pwdConfirm" id="pwdConfirm"size="20" maxlength="12" onChange="pageOnChange()"><font class="mandatory"> *</font>
								</td>
							</tr>
							<tr>
								<td style="height:30px" colspan="3">
									<font class="mandatory fntBold"><span id="spnError">&nbsp;</span></font>
								</td>
							</tr>
						</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>

			<tr>
				<td>
					<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
					<table width="100%" border="0" cellpadding="1" cellspacing="1" align="center">
						<tr>
							<td>
								<input name="btnClose" type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()" >
				                <input type="button" id="btnReset" value="Reset" class="Button" onclick="resetMe()">
				            </td>
				            <td align="right" valign="bottom">
								<input type="button" id="btnSave" value="Save" class="Button" onclick="save()">
			  				</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%> 
				</td>
			</tr>
		</table>
	<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
	<script language="javascript">
	<!--
		var request=new Array();
		var reqMessage='<c:out value='${requestScope.reqMessage}' escapeXml='false' />';
		<c:out value='${requestScope.reqRequest}' escapeXml='false' /> 
	//-->
	</script>
</form>
</body>
</html>
