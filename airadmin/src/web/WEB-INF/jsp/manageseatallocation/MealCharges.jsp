<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Meal Charges</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
</head>
<body oncontextmenu="return false" ondrag="return false"
	onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)" scroll="no"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
<script type="text/javascript">			
		var arrMealSegData = new Array();
		var originalFlightMeals = new Array();
		var strModelNo = "";
		var blnIsSced = false;
		var arrTemp = new Array();
		<c:out value="${requestScope.reqMealSegment}" escapeXml="false" />				
		var strFlightId = '<c:out value="${requestScope.mealFlightId}" escapeXml="false" />';
		var strClassOfService = '<c:out value="${requestScope.classOfService}" escapeXml="false" />';
		var strOpt = "<c:out value="${requestScope.reqMealTempl}" escapeXml="false" />";		
		var errString = "<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />";
		strModelNo =    "<c:out value="${requestScope.reqSelModel}" escapeXml="false" />";
		<c:out value="${requestScope.reqTemplArray}" escapeXml="false" />	//used in Edit 
		<c:out value="${requestScope.reqIsSCED}" escapeXml="false" />
		
										
	</script>
<%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
<table width="100%" cellpadding="0" cellspacing="0" border="0"
	class="PageBorder" ID="Table7" style="height: 300px;">
	<tr>
		<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%"
			height="1"></td>
		<td valign="top" align="center" class="PageBackGround"><!-- Your Form start here -->
		<form method="post" id="frmMealAlloc" name="frmMealAlloc"
			action="loadMealSegments.action"><br>
		<table width="98%" cellpadding="0" cellspacing="0" border="0"
			ID="Table7">
			<tr>
				<td><font class="Header">Meal Charges</font></td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Meal
				Charges<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2"
					ID="Table2">
					<tr>
						<td colspan="2"><span id="spnMeals"></span></td>
					</tr>

					<tr>
						<td colspan="2">
						<hr>
						</td>
					</tr>
					<tr>
						<td>
						<div id='divLoadMsg'
							style='visibility: hidden; z-index: 1000; background: transparent;'>
						<iframe src="LoadMsg" id='frmLoadMsg' name='frmLoadMsg'
							frameborder='0' scrolling='no'
							style='position: absolute; top: 50%; left: 40%; width: 260; height: 40;'></iframe>
						</div>
						</td>
					</tr>
					<tr>
						<td><input type="button" id="btnCancel" value="Close" title="Close"
							class="Button" onclick="cancelClick()" name="btnCancel"
							tabindex="4"></td>
						
						<td align="right">
						
						<u:hasPrivilege privilegeId="plan.invn.alloc.meal.roll">
							<input type="button" id="btnRmvRollForward" class="Button" 
								name="btnRmvRollForward" value="Remove Template for Schedule" title="Remove Template for Schedule"
								style="width: 200px;" onclick="removeTemplateRollForward()" tabindex="5" >
						</u:hasPrivilege>
						
						<u:hasPrivilege
							privilegeId="plan.invn.alloc.meal.roll">
							<input type="button" id="btnRollForward" class="Button"
								name="btnRollForward" value="Roll Forward" title="Roll Forward" onclick="rollClick()"
								tabindex="6">
						</u:hasPrivilege> 
						
						<input type="button" id="btnSave" value="Save" class="Button" title="Save"
							name="btnSave" onclick="SaveClick()" tabindex="7"></td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
		</table>
		</form>
		<!-- Your Form ends here --></td>
		<td width="15"><img src="../../images/spacer_no_cache.gif" width="100%"
			height="1"></td>
	</tr>
</table>
<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
<script
	src="../../js/manageseatallocation/MealAlloc.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<input type="hidden" name="hdnData" id="hdnData" value="" />
<input type="hidden" name="hdnMode" id="hdnMode" value="" />
<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</body>
</html>
