<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Seat Map</title>
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	</head>
	<body oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" onLoad="winOnLoad()">
	<script type="text/javascript">
			var stMap = new Array();
			var seatCharges = new Array();			
			var templateId = "";
			var flightId = "";
			var fltSegId = "";
			var templateName = "";	
					
			<c:out value="${requestScope.reqSeatEdit}" escapeXml="false" />
			templateId = "<c:out value="${requestScope.reqTemplatId}" escapeXml="false" />";
			flightId = "<c:out value="${requestScope.flightId}" escapeXml="false" />";
			fltSegId = "<c:out value="${requestScope.reqFltSegId}" escapeXml="false" />";
			templateName = "<c:out value="${requestScope.reqTemplatName}" escapeXml="false" />";
								
		</script>
	<%@ include file="../common/IncludeWindowTop.jsp" %><!-- Page Background Top page -->		
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:500px;">
			<tr>
				<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
				<td valign="top" align="center" class="PageBackGround">
				<!-- Your Form start here -->
				<form method="post"  id="frmSeatMap" name="frmSeatMap" action="loadSegments.action">
				<br>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
						<tr>
							<td><font class="Header">Seat Map</font></td>
						</tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr>
							<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
							</td>
						</tr>
						<tr>
							<td><%@ include file="../common/IncludeFormTop.jsp"%>Seat Map<%@ include file="../common/IncludeFormHD.jsp"%>
								<table width="100%" border="0" cellpadding="2" cellspacing="2" ID="Table2">								
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>									
									<tr>
										<td colspan="6"><span id= "spnSeatMap"></span></td>																					
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td colspan="6">
											<table>
												<tr>
												<td  bgColor="red" width="12" height="12"><td>
												<td><font>Selected Seat</font><td>
												<td  bgColor="green" width="12" height="12" ><td>
												<td><font>Vacant Seats</font><td>
												<td  bgColor="black" width="12" height="12" ><td>
												<td><font> In Active Seats</font><td>
												<td  bgColor="gray" width="12" height="12" ><td>
												<td><font>Sold Seats</font><td>
												</tr>
											</table>
										</td>																					
									</tr>	
									<tr>
										<td colspan="6"><hr></td>
									</tr>
									<tr>
										<td><font>Seat Status</font></td>
										<td>
											<select name="selStatus" size="1" id="selStatus" tabindex="3">
												<option value="VAC">Active</option>
												<option value="INA">IN-Active</option>
											</select>
										</td>
										<td><input type="button" id="btnAsgnStat" value="Assign Status" class="Button" onclick="assignStatusClick()" name="btnAsgnStat" tabindex="4" style="width: 100px">										
										<input type="button" id="btnSelect" value="Clear All" class="Button" onclick="clearClick()" name="btnSelect" tabindex="5"></td>
									</tr>
									<tr>
										<td colspan="6"><hr></td>
									</tr>									
									<tr>
										<td colspan="5">
										<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="cancelClick()" name="btnCancel" tabindex="7">
										</td>
										<td align="right">
										<input type="button" id="btnReset" class="Button" name="btnReset" value="Reset"  onclick="resetClick()" tabindex="8">
										<input type="button" id="btnConfirmed" value="Save" class="Button" name="btnConfirmed" onclick="ConfirmSeat()"  tabindex="9">
														
										</td>
									</tr>
								</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
					</table>
					<input type="hidden" name="hdnData"    id="hdnData"    value=""/>
					<input type="hidden" name="hdnSoldSeats" id="hdnSoldSeats" value=""/>
					<input type="hidden" name="overRide" id="overRide" value=""/>
				</form>
				<!-- Your Form ends here -->
			</td>
			<td width="15"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
		</tr>
	</table>				
		<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
		<script src="../../js/manageseatallocation/SeatMap.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</body>
</html>
