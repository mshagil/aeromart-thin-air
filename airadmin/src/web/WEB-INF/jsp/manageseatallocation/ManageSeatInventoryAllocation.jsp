<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Manage Seat Inventory</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 </head>

 <body oncontextmenu="return false" class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" onkeydown='return Body_onKeyDown(event)' 
		ondrag='return false'>	
		
		<div id="seatMovementInfoOptionsWindow" 
		 style="display: none ;position:absolute;top:165px;left:705px;width:210px;z-index:1002;display:none;overflow-y: auto;background-color: white;height: 80px; -moz-border-radius: 5px;
         border-radius: 5px;  border: 1px solid grey">
				<div class="ui-widget ui-widget-content ui-corner-all addPaddingBottom" >
					<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table1">
						<tr>
							<td height="20" class="FormHeadBackGround"> <font class="FormHeader"> Agent Options</font> </td>
						</tr>
						<tr>
			                <td>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table8" border="1">							
										
									  <tr>
									      <td height="20"> <font>Select respective agent type</font> </td>
									  </tr>
									  <tr> 
									      <td>  
									       <input type="radio" name="agentOption" id="radAgentOptionOrigin"	value="originAgent" onClick="setSeatMovmntInfoWithOriginAgent()" class="noBorder" />
									       <font>Origin Agent</font>									    
									       <input type="radio" name="agentOption" id="radAgentOptionOwner" value="ownerAgent" onClick="setSeatMovmntInfoWithOwnerAgent()" class="noBorder" />
									       <font>Owner Agent</font>									    
									      </td> 
									  </tr>
									  
																									
								</table>
										
                             </td>
						</tr>	
				</table>
			 </div>
	</div>
		
	<form method="post" name="formManageSeatInventory"  id="formManageSeatInventory" action="">
		<script type="text/javascript">
			var blnSeatMap = false;
			var blnSeatMapTmlt = false;
			var blnShowMeal = false;
			var seatFactorMin = 0;
			var seatFactorMax = 100;
			var varFlightRollForwardAuditSummary = null;
			<c:out value="${requestScope.reqCabinClass}" escapeXml="false" />
			<c:out value="${requestScope.reqFlightSumm}" escapeXml="false" />
			<c:out value="${requestScope.reqFlightRollForwardAuditSumm}" escapeXml="false" />
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFlightDefCap}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqSeatMap}" escapeXml="false" />
			<c:out value="${requestScope.reqSeatMapTmlt}" escapeXml="false" />
			<c:out value="${requestScope.reqShowMeal}" escapeXml="false" />
			<c:out value="${requestScope.reqShowBaggage}" escapeXml="false" />
			var hdnFromFromPage = "";
			<c:out value="${requestScope.fromFromPage}" escapeXml="false" />
			<c:out value="${requestScope.reqSeatFactorMin}" escapeXml="false" />
			<c:out value="${requestScope.reqseatFactorMax}" escapeXml="false" />			
		</script>
	<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0">
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Seat Allocation - Summary<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table9">
					<tr><td style="font-size:2px" colspan="5">&nbsp;</td></tr>
					<tr>
						<td width="15%"><font><span id="idFlightLable"></span></font></td>
						<td width="30%"><font><span id="idDepature"></span></font></td>
						<td width="25%"><font><span id="idArrival"></span></font></td>
						<td>&nbsp;</td>
						<td align="right"><a href="javascript:void(0)" onclick="showFlightSearchResult()" title="Show/Hide flight search result"><img src="../../images/AU_no_cache.gif" border="0"><font> Flight Search Results</font></a>
						<span id="spnFlightSearchResults" style="position:absolute; top:60px;left:200px;border:1px solid;"></span></td> 
					</tr>
					<tr><td style="font-size:2px" colspan="5">&nbsp;</td></tr>
					<tr>
						<td><font><span id="idModel"></span></font></td>
						<td><font><span id="idCapacity"></span></font></td>
						<td><font><span id="idAsk"></span></font></td>
						<td><b><font style="color:#FF0000"><span id="idOverlap" name="idOverlap">Overlapping Flight</span><font></b></td>
						<td align="right" id="COSList">
							<select id="selCOS" name="selCOS" size="1" style="width:100px">
								<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />
							</select>
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Segment Allocations<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr><td style="font-size:2px" colspan='2'>&nbsp;</td></tr>
					<tr><td colspan='2'>
						<span id="spnSegmentAllocations" style='height:85px;width:auto;overflow-X:hidden; overflow-Y:hidden;'></span>
					</td></tr>
					<tr>
						<td>
							<u:hasPrivilege privilegeId="plan.invn.alloc.bc.segupdate">
								<input type="button" name="btnSegEdit" id="btnSegEdit" class="Button" value="Edit" onclick="editSegmentAllocation()">
								<input type="button" name="btnUnAdd" id="btnUnAdd" class="Button" value="Add Note" onclick="addUserNote()">	
								<input type="button" name="btnUnView" id="btnUnView" class="Button" value="View Note" onclick="viewUserNote()">
							</u:hasPrivilege>							
						</td>
					<td width='49%'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="3%" id="tdBCOverload" class="fltStatus03"></td>
									<td><font>&nbsp;Overloaded Flight</font></td>
									<td width="3%" id="tdBCOverLap" class="fltStatus04"></td>
									<td><font>&nbsp;Overlapped Flight</font></td>
									<td width="3%" id="tdBCClsSeg" class="fltStatus02"></td>
									<td><font>&nbsp;Closed Segment</font></td>
									
								</tr>
							</table>
					</td>
					</tr>					
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp" %></td>
			<td class="FormHeadBackGround">
				<table  width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width= '30%' align='left'><font class="FormHeader">Booking Code Allocations :: <b><span id="spnBCSegmentCode"></span></b></font>
						</td>
						<td width= '30%' align='center'><c:if test="${requestScope.interLineDisplay == 'true'}"><a href="javascript:void(0)" onclick="showCarrierSummary()" title="Description to be shown"><font class="FormHeader">Marketing Carrier Summary ::</font><img src="../../images/AD_no_cache.gif" border="0" align="absmiddle"></a></c:if></td>
						<td width= '40%' align='right'><a href="javascript:void(0)" onclick="showSeatMovement()" title="Show/Hide Seat movement"><font class="FormHeader"> Seat Movement Information :: </font><img src="../../images/AD_no_cache.gif" border="0"></a> 
						<span id="spnSeatMovementsInfo"  style="position:absolute; top:33px;right:0px;"></span></td>
					</tr>
				</table>				
				<%@ include file="../common/IncludeFormHD.jsp"%>
			</td>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr><td colspan="4" style="font-size:2px">&nbsp;</td></tr>
					<tr><td colspan="4">
							<span id="spnBookingCodeAllocations" style='height:240px;width:auto;overflow-X:hidden;overflow-Y:hidden;'></span>
						</td>
					</tr>
					<tr><td colspan="4" style="font-size:2px">&nbsp;</td></tr>					
					<tr>
						<td><font><b>Total BC Allocation : </b></font>
							<font><span id="idTotBC"></span></font>&nbsp;&nbsp;&nbsp;&nbsp;
							<font><b>Fixed Allocation : </b></font>
							<font><span id="idFixAllo"></span></font>
						</td>
						<td>
							<font>Check/Uncheck All Priority<font><input type="checkBox" id="checkAllPriority" value="ON" onclick="allPriority_Click()"/>
						</td>
						<td>
							<u:hasPrivilege privilegeId="plan.invn.alloc.bc.status">
								<font>Check/Uncheck All Closed</font><input type="checkBox" id="checkAllClosed" value="ON" onclick="allClosed_Click()"/>
							</u:hasPrivilege>
						</td>
						
					</tr>
					<tr><td colspan="4" style="font-size:4px">&nbsp;</td></tr>
					<tr>
						<td>
							<font>Booking Class</font>
							<select id="selBookingCode" name="selBookingCode" size="1" style="width:130px">
								<c:out value="${requestScope.reqBookingCodesList}" escapeXml="false" />
							</select>
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.add">
									<input type="button" name="btnAddBC" id="btnAddBC" class="Button" value="Add" onclick="addBookingClassInvnentory()">							
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.update">	
									<input type="button" name="btnEditBC" id="btnEditBC" class="Button" value="Edit" onclick="editByBookingCode()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.delete">	
									<input type="button" name="btnDeleteBC" id="btnDeleteBC" class="Button" value="Delete" onclick="deleteBookingClassInvnentories()">
								</u:hasPrivilege>
						</td>						
						<td colspan="2" align='right'>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="3%" id="tdBCManClose" class="bcStatus01"></td>
									<td><font>&nbsp;Manually Closed</font></td>
									<td width="3%" id="tdBCManOpen" class="bcStatus02"></td>
									<td><font>&nbsp;Manually Opened</font></td>
									<td width="3%" id="tdBCAutCls" class="bcStatus03"></td>
									<td><font>&nbsp;System Closed</font></td>
									<td width="3%" id="tdBCAutOpen" class="bcStatus04"></td>
									<td><font>&nbsp;System Opened</font></td>
								</tr>
							</table>
						</td>
					</tr>				
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>		
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<input type="button" id="btnClose" id="btnClose" class="Button" value="Close"  onclick="CloseClicked()">
								<input type="button" id="btnBack" id="btnBack" class="Button" value="Back"  onclick="BackClicked()">
							</td>
							<td>
							<a href="javascript:void(0)" onclick="showRollForwardAudits()" title="Show/Hide roll forward audits"><img src="../../images/AU_no_cache.gif" border="0"><font> Roll Forward Audits</font></a>
							<span id="spnRollForwardAudits" style="position:absolute; top:460px;left:200px;border:1px solid;"></span>
							</td>
							<td align="right">
								<input type="button" id="btnPrintBookingClasses" name="btnPrintBookingClasses" class="Button" value="Print"  style="visibility:show;" onclick="loadBookingClassPrint()">
								
								<input type="button" id="btnBaggageAllocation" name="btnBaggageAllocation" class="Button" value="Link Baggage"  style="visibility:show;width:105px;" onclick="loadBaggageCharges()">
								
								<u:hasPrivilege privilegeId="plan.invn.alloc.meal">
									<input type="button" id="btnMealAllocation" name="btnMealAllocation" class="Button" value="Link Meals"  style="visibility:show;" onclick="loadMealCharges()">
								</u:hasPrivilege>								
								<u:hasPrivilege privilegeId="plan.invn.alloc.seat">
									<input type="button" id="btnSeatCharge" name="btnSeatCharge" class="Button" value="Seat Charge"  width="200px" style="visibility:show;" onclick="loadSeatCharges()">
								</u:hasPrivilege>								
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.roll">
									<input type="button" id="btnRollForward" name="btnRollForward" class="Button" value="Roll Forward"  style="visibility:show;" onclick="loadRollForward()">
								</u:hasPrivilege>
								<input type="button" name="btnSaveBC" id="btnSaveBC" class="Button" value="Save" onclick="saveInventories()">
							</td>
						</tr>
					</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%> 
			</td>
		</tr>
	</table>
	
	<!-- Page Background Bottom page -->
	<input type="hidden" name="hdnMode" id="hdnMode"/>	
	<input type="hidden" name="hdnFlightSumm" id="hdnFlightSumm" style="width:500px;"/>		
	<input type="hidden" name="hdnFlightRollForwardAuditSummary" id="hdnFlightRollForwardAuditSummary" value="" style="width:500px;"/>
	<input type="hidden" name="hdnSaveSegData" id="hdnSaveSegData" style="width:500px;"/>
	<input type="hidden" name="hdnSaveBCData" id="hdnSaveBCData" style="width:500px;"/>
	<input type="hidden" name="hdnFlightID" id="hdnFlightID">
	<input type="hidden" name="hdnClassOfService" id="hdnClassOfService">
	<input type="hidden" name="hdnOlFlightId" id="hdnOlFlightId">
	<input type="hidden" name="hdnFromFromPage" id="hdnFromFromPage">
	<input type="hidden" name="hdnModelNo" id="hdnModelNo">	<input type="hidden" name="hdnFCCInventoryInfo" id="hdnFCCInventoryInfo" style="width:500px;"/>	
	<input type="hidden" name="hdnSeatFactorMin" id="hdnSeatFactorMin">
	<input type="hidden" name="hdnSeatFactorMax" id="hdnSeatFactorMax">	
	</form>
	<script src="../../js/manageseatallocation/dummySeatInventoryData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageseatallocation/ManageSeatInventoryGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageseatallocation/ManageSeatInventory.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>');
	</script>
</body>
</html>