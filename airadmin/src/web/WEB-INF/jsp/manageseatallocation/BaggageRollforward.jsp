<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Baggage RollForward</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/manageseatallocation/BaggageRollforward.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

</head>

<body scroll="no" onkeypress='return Body_onKeyPress(event)'
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

<%@ include file="../common/IncludeWindowTop.jsp"%>
<form method="post" name="formBaggageRoll" id="formBaggageRoll"
	action="showBaggageRoll.action"><script type="text/javascript">
			<!--
			var rollFwdStatusReportData = new Array();
			var flightData = new Array();
			var frequncy = "";
			var reqMode = '';
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFrequncy}" escapeXml="false" />
			<c:out value="${requestScope.reqSelectedDataJS}" escapeXml="false" />
			<c:out value="${requestScope.reqSegmentList}" escapeXml="false" />
			<c:out value="${requestScope.reqRollBaggageFlight}" escapeXml="false" />			
			var rollType = "<c:out value="${requestScope.rollType}" escapeXml="false" />";			
			var strClassOfService = '<c:out value="${requestScope.classOfService}" escapeXml="false" />';
			var privOverrideFrequency = '<c:out value="${requestScope.reqOverrideFrequency}" escapeXml="false" />';
			//-->
		</script>

<table width="100%" cellpadding="0" cellspacing="0" border="0"
	class="PageBorder" ID="Table7" style="height: 350px;">
	<tr>
		<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%"
			height="1"></td>
		<td valign="top" align="left" class="PageBackGround"><!-- Your Form start here -->
		<font class="Header">Baggage Template - Roll Forward</font> <font
			class="fntSmall">&nbsp;</font> <br><%@ include
			file="../common/IncludeMandatoryText.jsp"%> <br>
		<table width="98%" cellpadding="0" cellspacing="0" border="0"
			ID="Table7">
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Baggage
				Template - Roll Forward<%@ include
					file="../common/IncludeFormHD.jsp"%> <br>
				<table width="100%" border="0" cellpadding="2" cellspacing="0"
					ID="Table2">
					<tr>
						<td width="10%"><font>Flight</font></td>
						<td width="15%"><font class="fntBold"><span
							id="spnFlightNo"></span></font></td>
						<td width="10%"><font>Origin</font></td>
						<td width="15%"><font class="fntBold"><span
							id="spnOrigin"></span></font></td>
						<td width="10%"><font>Destination</font></td>
						<td width="15%"><font class="fntBold"><span
							id="spnDestination"></span></font></td>

					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><font>From Date</font></td>
						<td colspan="2"><input type="text" id="txtFromDate"
							style="width: 75px;" maxlength="10" name="txtFromDate"
							onBlur="dateChk('txtFromDate')"> <a
							href="javascript:void(0)"
							onclick="LoadCalendar(0,event); return false;"
							title="View Calendar"><img SRC="../../images/calendar_no_cache.gif"
							border="0"></a><font class="mandatory">&nbsp;*</font></td>
						<td><font>To Date</font></td>
						<td colspan="2"><input type="text" id="txtToDate"
							style="width: 75px;" maxlength="10" name="txtToDate"
							onBlur="dateChk('txtToDate')"> <a
							href="javascript:void(0)"
							onclick="LoadCalendar(1,event); return false;"
							title="View Calendar"><img SRC="../../images/calendar_no_cache.gif"
							border="0"></a><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Segment</font></td>
						<td valign="top" colspan="5"><span id="spn1"
							class="FormBackGround"></span> <font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr>
						<td><font>Frequency</font></td>
						<td colspan="5"><font>M</font>&nbsp;<input type="checkbox"
							id="chkMonday" name="chkMonday" class="NoBorder"> <font>Tu</font>&nbsp;<input
							type="checkbox" id="chkTuesday" name="chkTuesday"
							class="NoBorder"> <font>W</font>&nbsp;<input
							type="checkbox" id="chkWednesday" name="chkWednesday"
							class="NoBorder"> <font>Th</font>&nbsp;<input
							type="checkbox" id="chkThursday" name="chkThursday"
							class="NoBorder"> <font>F</font>&nbsp;<input
							type="checkbox" id="chkFriday" name="chkFriday" class="NoBorder">
						<font>Sa</font>&nbsp;<input type="checkbox" id="chkSaturday"
							name="chkSaturday" class="NoBorder"> <font>Su</font>&nbsp;<input
							type="checkbox" id="chkSunday" name="chkSunday" class="NoBorder">
						<font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td colspan="3"><input type="button" id="btnClose"
							class="Button" value="Close" onclick="javascript:window.close();">
						</td>
						
						<td colspan="4" align="right"><input type="button"
								id="btnSave" name="btnSave" class="Button" value="Save"
								onclick="saveRollForward(rollType)"></td>
						
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
				<!-- Stable -->
			</tr>
		</table>
		<br>
		</td>
		<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%"
			height="1"></td>
	</tr>

	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
</table>
<input type="hidden" name="hdnVersion" id="hdnVersion" /> <input
	type="hidden" name="hdnMode" id="hdnMode" /> <input type="hidden"
	name="hdnFlightIDRF" id="hdnFlightIDRF" /> <input type="hidden"
	name="hdntemplate" id="hdntemplate" /> <input type="hidden"
	name="hdnFlightNumber" id="hdnFlightNumber" /> <input type="hidden"
	name="hdnOrigin" id="hdnOrigin" /> <input type="hidden"
	name="hdnDestination" id="hdnDestination" /> <input type="hidden"
	name="hdnSelectedSegs" id="hdnSelectedSegs" /> <input type="hidden"
	name="hdnNotSelectedSegs" id="hdnNotSelectedSegs" /> <input
	type="hidden" name="hdnFlightInfo" id="hdnFlightInfo" /></form>
</body>
<script type="text/javascript">
		<!--
		var objCal1 = new Calendar("spnCalendarDG1");
		objCal1.onClick = "setDate";
		objCal1.buildCalendar();
		//-->
	</script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</html>
