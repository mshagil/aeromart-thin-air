<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 	<head>
	    <title>Airport Admin Terminals</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	

 	</head>
  	<body scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  			onUnload="opener.resetVariables()"  onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
		<%@ include file="../common/IncludeWindowTop.jsp"%>	
		<form method="post"  id="AirportAdminTerminalFrom" action="showAirportTerminal.action">
		<script type="text/javascript">
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			<c:out value="${requestScope.reqTerminalDisplayAlert}" escapeXml="false" />
		</script>
		<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
			  
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								
							</td>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>  

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr>				
				<td><font class="Header">Terminals - Airport&nbsp;&nbsp;<span id="spnTerminalHeader"></span></font></td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Terminals<%@ include file="../common/IncludeFormHD.jsp"%>	
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr><td><font>&nbsp;</font></td></tr>
						<tr>
							<td style="height:125px;" valign="top">
								<span id="spnTerminal"></span>
							</td>
						</tr>
						<tr><td><font>&nbsp;</font></td></tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="sys.mas.airport.term.add">
									<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.airport.term.edit">
									<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit"  onClick="editClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.airport.term.delete">
									<input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete"  onClick="deleteClick()">
								</u:hasPrivilege>
								
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Add / Modify Terminals<%@ include file="../common/IncludeFormHD.jsp"%>	
					<br>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="30%"><font>Terminal Name</font></td>
							<td width="12%">
								<input tabindex="1" type="text" name="txtTerminalName" id="txtTerminalName" size="20"  maxlength="10"> 
								<font class="mandatory">&nbsp;*</font> 
							</td>
							<td width="8%">
								
							</td>												
							<td width="15%" align="right"></td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="18%" align="left"></td>
							<td width="11%" >								
							</td>															
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="30%"><font>Terminal Code (Two Letters)</font></td>
							<td width="12%">
								<input tabindex="1" type="text" name="txtTerminalCode" id="txtTerminalCode" size="5" maxlength="2"> 
								
							</td>
							<td width="8%">
								
							</td>												
							<td width="15%" align="right"></td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="18%" align="left"></td>
							<td width="11%" >								
							</td>															
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="30%"><font>Description</font></td>
							<td width="12%">
								<textarea name="txtTerminalDes" id="txtTerminalDes" cols="47"
					rows="3" onkeyUp="validateDes(this,255);commaValidate(this);"
					onkeyPress="validateDes(this,255);commaValidate(this);"
					onChange="pageOnChange();"
					title="Can enter only up to 255 charactors"></textarea>
								
							</td>
							<td width="8%">
								<font class="mandatory">&nbsp;*</font> 
							</td>												
							<td width="15%" align="right"></td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="18%" align="left"></td>
							<td width="11%" >								
							</td>															
						</tr>
						</table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="21%"><font>Default</font></td>
							<td width="12%">
								<input type="checkbox" name="chkDefault" id="chkDefault" onChange="pageOnChange();">								
							</td>
							<td width="8%"><font>Active</font></td>								
							<td width="15%" >
                            	<input type="checkbox" name="chkActive" id="chkActive" onChange="pageOnChange();"> 
                            </td>
							<td width="20%" colspan="2">
								
							</td>					
							<td width="18%" align="left"></td>
							<td width="11%" >								
							</td>															
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
				 		
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
						</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="4">									
								<input tabindex="14" type="button" id="btnClose" class="Button" value="Close" onclick="windowclose()">	
								<input tabindex="15" name="btnReset" type="button" class="Button" id="btnReset" onClick="resetAirportTerminal()" value="Reset" >
							</td>
							<td align="right" colspan="4">
								<input tabindex="16"  name="btnSave" type="button" class="Button" id="btnSave" onClick="saveAirportTerminal()" value="Save">
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>	
			<tr><td><font class="fntSmall"></font></td></tr>							
		</table>
		<%@ include file="../common/IncludeWindowBottom.jsp"%>
		<script src="../../js/airport/AirportAdminTerminal.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
		<input type=hidden name="hdnAirportCode"  id="hdnAirportCode">	
		<input type=hidden name="hdnTerminalId"  id="hdnTerminalId">	
		<input type=hidden name="hdnMode" id="hdnMode">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow" value="">
		
	</form>	
  </body>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/airport/AirportTerminalValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
</html>
