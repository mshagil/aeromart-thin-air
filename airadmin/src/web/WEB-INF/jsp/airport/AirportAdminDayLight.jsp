 <%-- 
	 @Author 	: 	Srikanth
	 @Copyright : 	ISA
  --%>
<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 	<head>
	    <title>Airport Admin Day Light Saving</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script> 
		
 	</head>
  	<body scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  			onUnload="opener.resetVariables()"  onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
		<%@ include file="../common/IncludeWindowTop.jsp"%>	
		<form method="post"  id="AirportAdminDSTFrom" action="showAirportDST.action">
		<script type="text/javascript">
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
		</script>
		<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
			  
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								
							</td>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>  

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr>				
				<td><font class="Header">Day Light Saving Time - Airport&nbsp;&nbsp;<span id="spnDSTHeader"></span></font></td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Day Light Saving Time<%@ include file="../common/IncludeFormHD.jsp"%>	
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr><td><font>&nbsp;</font></td></tr>
						<tr>
							<td style="height:125px;" valign="top">
								<span id="spnDL"></span>
							</td>
						</tr>
						<tr><td><font>&nbsp;</font></td></tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="sys.mas.airport.dst.add">
									<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.airport.dst.edt">
									<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit"  onClick="editClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.airport.dst.rollback">
									<input name="btnDisableRollback" type="button" class="Button" style="width:110px;" id="btnDisableRollback" value="Disable & Rollback"  onClick="disableRollbackClick()">
								</u:hasPrivilege>
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Add / Modify Day Light Saving Time<%@ include file="../common/IncludeFormHD.jsp"%>	
					<br>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="10%"><font>Start</font></td>
							<td width="12%">
								<input tabindex="1" type="text" name="txtStart" id="txtStart" size="10" maxlength="11" onBlur="dateChk('txtStart')" onChange="pageOnChange()">
							</td>
							<td width="8%">
								<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font>
							</td>												
							<td width="15%" align="right"><font>Zulu&nbsp;Time</font><font>&nbsp;(HH:MM)&nbsp;&nbsp;</font></td>
							<td width="20%" colspan="2">
								<input tabindex="2" type="text" name="txtStartTime" id="txtStartTime" size="5"  maxlength="5" onblur="setTimeWithColon(document.forms[0].txtStartTime)" onKeyPress="StartTimePress(this)" onKeyUp="StartTimePress(this)" onChange="pageOnChange()"><font class="mandatory"> &nbsp;*</font>
							</td>					
							<td width="18%" align="left"><font>Adjust&nbsp;Time</font><font>&nbsp;(HH:MM)</font></td>
							<td width="31%" >
								<select tabindex="3" id="selOperator" size="1" name="selOperator" style="width:35px;" onChange="pageOnChange()">
									<option value="+">+</option>
									<option value="-">-</option>														
								</select>
								<input tabindex="4" type="text" name="txtAdjTime" id="txtAdjTime" size="5" maxlength="5" onblur="setTimeWithColon(document.forms[0].txtAdjTime)" onKeyPress="AdjTimePress(this)" onKeyUp="AdjTimePress(this)" onChange="pageOnChange()"><font class="mandatory">&nbsp;*</font>												
							</td>															
						</tr>
						<tr>
							<td width="10%" align="left"><font>End&nbsp;&nbsp;</font></td>
							<td width="12%">
								<input tabindex="5" type="text" name="txtEnd" id="txtEnd" size="10"  maxlength="11" onBlur="dateChk('txtEnd')" onChange="pageOnChange()">
							</td>
							<td width="5%">
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font>
							</td>												
							<td width="15%" align="right"><font>Zulu&nbsp;Time</font><font>&nbsp;(HH:MM)&nbsp;&nbsp;</font>	
							</td>
							<td width="20%" colspan="2">
								<input tabindex="6" type="text" name="txtEndTime" id="txtEndTime" size="5"  maxlength="5" onblur="setTimeWithColon(document.forms[0].txtEndTime)" onKeyPress="EndTimePress(this)" onKeyUp="EndTimePress(this)"  onChange="pageOnChange()"><font class="mandatory"> &nbsp;*</font>
							</td>					
							<td width="10%" align="left"><font>Active&nbsp;&nbsp;</font></td>
							<td width="31%"><input type="checkbox" name= "chkStatus" id="chkStatus" value="Active"></td>
						</tr>
						<tr>
							<td colspan="8"><font><b>Note: DST should be entered in Zulu</b></font></td>
						</tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
				 		<tr>
				 			<td colspan="8">
								<span id="spnNotifyNote">
									 <table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>		
											<td colspan="8">
												<p><font><font class="mandatory">&nbsp;!&nbsp;</font>There can be Flights scheduled for the defined DST period. All Schedules/Flights for the period will be rescheduled.</font></p>
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td><font>Alerts&nbsp;</font></td >
											<td><font>Emails&nbsp;</font></td>
											<td><font>Email Pax&nbsp;</font></td>
											<td><font>SMS Pax&nbsp;</font></td>
											<td><font>SMS CNF Pax&nbsp;</font></td>
											<td><font>SMS Onhold Pax&nbsp;</font></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><font>Send Alerts &amp; Emails for rescheduled Flights?</font></td>
											<td>
												<input tabindex="7" type="checkbox" name= "chkAlerts" id="chkAlerts" onClick="pageOnChange()" checked>
											</td>
											<td>
												<input tabindex="8" type="checkbox" name= "chkEmails" id="chkEmails" onClick="clickEmail(this)" >
											</td>		
											<td>
												<input tabindex="10" type="checkbox" name= "chkEmailPax" id="chkEmailPax" onClick="clickEmailPax()" >
											</td>
											<td>
												<input tabindex="11" type="checkbox" name= "chkSmsPax" id="chkSmsPax" onClick="clickSmsPax(this)" >
											</td>
											<td>
												<input tabindex="12" type="checkbox" name= "chkSmsCNF" id="chkSmsCNF">
											</td>
											<td>
												<input tabindex="13" type="checkbox" name= "chkSmsOnhold" id="chkSmsOnhold">
											</td>									
											<td>&nbsp;</td>											
										</tr>
										<tr>
											<td></td>
											<td colspan="7" align="left">
												<div id="divEmailAddr">
													<font>Email Address&nbsp;&nbsp;</font><input tabindex="9" type="text" name="txtEmailID" id="txtEmailID" size="25" maxlength="100" onChange="pageOnChange()">
												</div>												
											</td>
										</tr>
									</table>
								</span>
							</td>
						</tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4">									
								<input tabindex="14" type="button" id="btnClose" class="Button" value="Close" onclick="windowclose()">	
								<input tabindex="15" name="btnReset" type="button" class="Button" id="btnReset" onClick="resetAirportDst()" value="Reset" >
							</td>
							<td align="right" colspan="4">
								<input tabindex="16"  name="btnSave" type="button" class="Button" id="btnSave" onClick="saveAirportDst()" value="Save">
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>	
			<tr><td><font class="fntSmall"></font></td></tr>							
		</table>
		<%@ include file="../common/IncludeWindowBottom.jsp"%>
		<script src="../../js/airport/AirportAdminDayLight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
		<input type=hidden name="hdnAirportCode"  id="hdnAirportCode">	
		<input type=hidden name="hdnDSTId"  id="hdnDSTId">	
		<input type=hidden name="hdnMode" id="hdnMode">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow" value="">
		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
		<input type=hidden name="selCountry"  id="selCountry" value="<%=request.getParameter("strCountry")%>">	
	</form>	
  </body>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/airport/AirportDstValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
</html>
