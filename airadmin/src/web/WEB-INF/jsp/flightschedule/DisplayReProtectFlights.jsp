 <%-- 
	 @Author 	: 	Srikanth
  --%>
  
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Search Flight to Re-Protect</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  </head>
  <body oncontextmenu="return false" scroll="yes" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" >
  	
   <form method="post"  id="frmFlightReprotectSearch" name="frmFlightReprotectSearch" action="showFlightsToReprotect.action">
			  <script type="text/javascript">
					var arrFlightsToReprotectData = new Array();
					<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
					<!-- c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" /-->		
					<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
					<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />		
					<c:out value="${requestScope.PostBack}" escapeXml="false" />		
					<c:out value="${requestScope.reqReprotectFlightDataByCCCode}" escapeXml="false" />		
					<c:out value="${requestScope.reqCabinClassData}" escapeXml="false" />
					var popupmessage = "<c:out value="${requestScope.popupmessage}" escapeXml="false" />";	
					var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";	
					var recNo = "<c:out value="${requestScope.recNo}" escapeXml="false" />";
					var agentReprotectEmailEnabled = "<c:out value="${requestScope.agentEmailEnabled}" escapeXml="false" />"
					var isSelfReprotectionAllowed = "<c:out value="${requestScope.isSelfReprotectionAllowed}" escapeXml="false" />"
					var externalIntlFlightDetailCaptureEnabled = "<c:out value="${requestScope.externalIntlFlightDetailCaptureEnabled}" escapeXml="false" />"
					var autoDateFillEnabled = "<c:out value="${requestScope.dateAutoFillEnabled}" escapeXml="false" />"
					var allowDisplayRollFwdBtn ="<c:out value="${requestScope.displayRollFwdBtn}" escapeXml="false" />"
					<c:out value="${requestScope.reqSystemDate}" escapeXml="false" />
			  </script>

   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	   <tr>
			<td colspan="3"><font class="Header">Search Flights to Re-protect</font></td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td colspan="3">
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>				
		<tr>
			<td colspan="3">
				<%@ include file="../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2" class="FormBackGround">
							<tr valign="middle">
								<td width="10%" >
									<font>Departure Date - From</font>
								</td>
								<td width="13%">
									<input type="text" name="txtDepartureDateFrom" id="txtDepartureDateFrom" maxlength="10" size="8" onBlur="dateChk('txtDepartureDateFrom')" invalidText="true">
									<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
								</td>
								<td width="10%">
									<font>Departure Date - To</font>
								</td>
								<td width="13%">
									<input type="text" name="txtDepartureDateTo" id="txtDepartureDateTo" maxlength="10" size="8" onBlur="dateChk('txtDepartureDateTo')" invalidText="true">
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
								</td>
								<td width="10%">
									<font>Flight Number</font>
								</td>
								<td>
									<input type="text" id="txtFlightNumber" name="txtFlightNumber" maxlength="7" size="7">
								</td>
								<td>
									<font>Departure</font>
								</td>
								<td>
									<select name="selDeparture" size="1" id="selDeparture"  tabindex="5" title="Original departure location" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
						   			</select>
								</td>
								<td>
									<font>Arrival</font>
								</td>
								<td>
									<select name="selArrival" size="1" id="selArrival"  tabindex="6" title=" Final Destination" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
							</tr>
							<tr>								
								<td>
									<font>Via1</font>
								</td>
								<td>
									<select name="selVia1" size="1" id="selVia1"  tabindex="8" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
						   			</select>
								</td>
								<td>
									<font>Via2</font>
								</td>
								<td>
									<select name="selVia2" size="1" id="selVia2"  tabindex="9" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
								<td>
									<font>Via3</font>
								</td>
								<td>
									<select name="selVia3" size="1" id="selVia3"  tabindex="10" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
								<td>
									<font>Via4</font>
								</td>
								<td>
									<select name="selVia4" size="1" id="selVia4"  tabindex="11" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
								<td align="right" colspan="2">
									<input type="Button" class="Button" name="btnSearch" id="btnSearch" value="Search" onClick="searchFlightsToReprotect()">
								</td>
							</tr>
					</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td colspan="3">
			<%@ include file="../common/IncludeFormTop.jsp"%>Flight Details Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
				<span id="spnSearch" style="height:160px"></span>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		<tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td width="47%" valign="top">
			<%@ include file="../common/IncludeFormTop.jsp"%>Re-protect From<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td><font class="fntBold">Flight No</font>&nbsp;<font><span id="spnFlightNo"></span></font></td>
						<td><font class="fntBold">Departure</font>&nbsp;<font><span id="spnDeparture"></span></font></td>
						<td><font class="fntBold">Arrival</font>&nbsp;<font><span id="spnArrival"></span></font></td>
					</tr>
					<tr>
						<td>
							<font class="fntBold">COS</font>&nbsp;<span id="spnCOSFrom"></span>
						</td>
						<td colspan="2">
							<font class="fntBold">Dep. Date</font>&nbsp;<font><span id="spnDepDate"></span></font>
						</td>
					</tr>
					<tr>
						<td colspan="3" valign="top">
							<span id="spnReprotectFrom" style="height:150px"></span>
						</td>
					</tr>
				</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
			<td width="4%" align="center">
				<input type="button" class="Button" name="btnMove" id="btnMove" value=">>" style="width:25px" onClick="reprotectPassenger()">
			</td>
		   <td width="49%">
			<span id="spnReprotectTo" style="visibility:hidden;" style="width:430px">
			<%@ include file="../common/IncludeFormTop.jsp"%>Re-protect To<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td><font class="fntBold">Flight No</font>&nbsp;<font><span id="spnFlightNoOther"></span></font></td>
						<td><font class="fntBold">Departure</font>&nbsp;<font><span id="spnDepartureOther"></span></font></td>
						<td><font class="fntBold">Arrival</font>&nbsp;<font><span id="spnArrivalOther"></span></font></td>
					</tr>
					<tr>
						<td><font class="fntBold">COS</font>&nbsp;<span id="spnCOSOther"></span></td>
						<td>
							<font class="fntBold">Dep. Date</font>&nbsp;<font><span id="spnDepDateOther"></span></font>
						</td>
						<td>
							<font class="fntBold">Model</font>&nbsp;<font><span id="spnModelCap"></span></font>
						</td>
					</tr>
					<tr>
						<td colspan="3" valign="top">
							<span id="spnOther" style="height:80px"></span>
						</td>
					</tr>
					<tr>						
						<td colspan="2">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
									<input type="checkbox" id="chkGA" name="chkGA" class="NoBorder" onClick="setPageEdited(true)" checked>&nbsp;<font class="font">Generate Alerts</font>
									</td>
									<td>
									<input type="checkbox" id="chkSendSMS" name="chkSendSMS" class="NoBorder" onClick="enableSMSChk()">&nbsp;<font class="font">SMS Pax</font>
									</td>
									<td>
									<input type="checkbox" id="chkSendEmail" name="chkSendEmail" class="NoBorder" onClick="setPageEdited(true)">&nbsp;<font class="font">Email Pax</font>
									</td>
								</tr>
								<tr>
									<td>
										<input type="checkbox" id="chkSmsCnf" name="chkSmsCnf" class="NoBorder" onClick="setPageEdited(true)">&nbsp;<font class="font">SMS CNF Pax</font>
										</td>
										<td colspan="2">
										<input type="checkbox" id="chkSmsOnH" name="chkSmsOnH" class="NoBorder" onClick="setPageEdited(true)">&nbsp;<font class="font">SMS ON HOLD Pax</font>
									</td>
								</tr>
								<tr id="agentEmailRow">
									<td>
										<input type="checkbox" id="chkSendEmailOriginAgent" name="chkSendEmailOriginAgent" class="NoBorder" onClick="setPageEdited(true)">&nbsp;<font class="font">Origin Agent Email</font>
										</td>
										<td colspan="2">
										<input type="checkbox" id="chkSendEmailOwnerAgent" name="chkSendEmailOwnerAgent" class="NoBorder" onClick="setPageEdited(true)">&nbsp;<font class="font">Owner Agent Email</font>
									</td>
								</tr>
								<c:if test="${requestScope.showETOpeningSection == true}">
									<u:hasPrivilege privilegeId="plan.flight.flt.deleteFlown">
										<tr id="chkETOpen">
											<td>
												<input type="checkbox" id="chkOpenEtickets"
													   name="chkOpenEtickets"
													   class="NoBorder">&nbsp;<font class="font">OPN All
												Etickets</font>
											</td>
										</tr>
									</u:hasPrivilege>
								</c:if>
							</table>
						</td>
						<td align="right">
							<input type="Button" class="Button" name="btnReset" id="btnReset" value="Reset" onClick="resetReprotect()">
						</td>
					</tr>
					
					
				</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</span>
			</td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<input type="Button" class="Button" name="btnClose" id="btnClose" value="Close" onCLick="closeClick()" style="width:100px">
			</td>
			<td colspan="2" align="right">
				<span id="spnViewRollForwardBtn" style="visibility:show;">
					<input type="Button" class="Button" name="btnCRF" id="btnCRF" value="Roll Forward" style="width:120px" onClick="confirmReprotectRollForward()">
				</span>
				<input type="Button" class="Button" name="btnConfirm" id="btnConfirm" value="Reprotect" style="width:100px" onClick="confirmReprotectPAX()">				
			</td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<!-- Used in FlightsToReprotectRequestHandler.java file -->
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnLogicalCabinClassCode" id="hdnLogicalCabinClassCode">
	<input type="hidden" name="hdnTargetCabinClassCode" id="hdnTargetCabinClassCode">
	<input type="hidden" name="hdnTargetCabinClassCode" id="hdnTargetCabinClassCode">
	<input type="hidden" name="hdnTargetFlightId" id="hdnTargetFlightId">
	<input type="hidden" name="hdnTargetSegmentCode" id="hdnTargetSegmentCode">
	<input type="hidden" name="hdnTargetFlightSegmentId" id="hdnTargetFlightSegmentId">
	<input type="hidden" name="hdnSourceFlightDistributions" id="hdnSourceFlightDistributions">
	<input type="hidden" name="hdnFlightId" id="hdnFlightId">
	<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">	
	<input type="hidden" name="hdnlAlert" id="hdnAlert" value="false"/>	
	
	<input type="hidden" name="hdnSMSAlert" id="hdnSMSAlert" value=""/>	
	<input type="hidden" name="hdnEmailAlert" id="hdnEmailAlert" value=""/>	
	<input type="hidden" name="hdnSmsCnf" id="hdnSmsCnf" value=""/>	
	<input type="hidden" name="hdnSmsOnH" id="hdnSmsOnH" value=""/>
	<input type="hidden" name="hdnEmailOriginAgent" id="hdnEmailOriginAgent" value=""/>	
	<input type="hidden" name="hdnEmailOwnerAgent" id="hdnEmailOwnerAgent" value=""/>
	<input type="hidden" name="hdnSelectedPNR" id="hdnSelectedPNR" value=""/>	
	<input type="hidden" name="hdnSelectedSoldOhdCount" id="hdnSelectedSoldOhdCount" value=""/>
	<input type="hidden" name="hdnOpenEtStatus" id="hdnOpenEtStatus" value=""/>
	<input type="hidden" name="hdnFlightNo" id="hdnFlightNo" value=""/>
	<input type="hidden" name="hdnDepatureDate" id="hdnDepatureDate" value=""/>
	<input type="hidden" name="hdnDepatureDateTime" id="hdnDepatureDateTime" value=""/>
   </form>
  <script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  <script src="../../js/flightschedule/DRFFromGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script src="../../js/flightschedule/DRFOtherGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script src="../../js/flightschedule/DisplayReprotectFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script src="../../js/flightschedule/validateDisplayReprotectFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  
  </body>
</html>