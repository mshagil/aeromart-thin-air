<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.DATE,1);
	dStopDate = cal.getTime(); //Next Month Date
	String CurrentDate =  formatter.format(dStartDate);
	String NextDate = formatter.format(dStopDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Split Flight Schedule</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	</head>
	<body oncontextmenu="return false" ondrag="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="no" onload="winOnLoad()">
	<%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
	<form method="post"  id="frmSchdSplit" name="frmSchdSplit"action="showSchedule.action">		
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:300px;">
			<tr>
				<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
				<td valign="top" align="center" class="PageBackGround">
			<!-- Your Form start here -->
			
				<br>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
						<tr>
							<td><font class="Header">Split Flight Schedule</font></td>
						</tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr>
							<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
							</td>
						</tr>
						<tr>
							<td>
								<%@ include file="../common/IncludeFormTop.jsp"%>
								Split Flight Schedule<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="100%" border="0" cellpadding="2" cellspacing="4">
										<tr>											
														
											
											<td colspan="4" align="left"><font><span id="spnSplRange"></span></font></td>										
											<td colspan="2" align="left" ><font><span id="spnSplSchedid"></span></font></td>
										</tr>
									
										<tr>
											<td width="30%"><font class="fntBold">Split From Date</font></td>
											<td width="20%"><input type="text" id="txtSplitStart" style="width:65px;" maxlength="10" name="txtSplitStart"  invalidText="true"  onBlur="dateChk('txtSplitStart')" onchange="dataChanged()" title="1st Split date" value="" align="middle"></td>
											<td width="13%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
											<td width="15%"><font class="fntBold">To Date</font></td>
											<td><input type="text" id="txtSplitStop" style="width:65px;" maxlength="10" name="txtSplitStop"  invalidText="true"  onBlur="dateChk('txtSplitStop')" onchange="dataChanged()" title="2nd Split date"></td>
											<td width="13%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a></td>
										</tr>
										<tr>
											<td colspan="6"><font>Choose Day of weeks to be Split</font></td>
										</tr>
										<tr>
											<td colspan="6"><span id="spnSplFrequency"></span></td>
										</tr>
										<tr>	
											<td colspan="6"><font><span id="spnSplOverlap"></span></font>
											</td>													
										</tr>
										<tr>
											<td colspan="6"><hr></td>
										</tr>
										<tr>
											<td colspan="3">
												<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="cancelClick()" name="btnCancel">
											</td>
											<td colspan="3" align="right">
												<input type="button" id="btnConfirm" value="Confirm" class="Button" onclick="SplitConfirm()">
											</td>
										</tr>
									</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
								</td>
							</tr>
						</table>
					</td>
					<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
				</tr>		
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
		<input type="hidden" name="hdnNextDate" id="hdnNextDate" value="<%=NextDate %>">
		<input type="hidden" name="hdnStartD" id="hdnStartD" value=""/>
		<input type="hidden" name="hdnStopD" id="hdnStopD"  value=""/>
		<input type="hidden" name="hdnSplitStartD" id="hdnSplitStartD"  value=""/>
		<input type="hidden" name="hdnSplitStopD" id="hdnSplitStopD"  value=""/>
		<input type="hidden" name="hdnScheduleId" id="hdnScheduleId" value=""/>
		<input type="hidden" name="hdnOlapSchId" id="hdnOlapSchId" value=""/>
	</form>				
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->		
	<script src="../../js/flightschedule/SplitSchedule.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</body>
</html>
