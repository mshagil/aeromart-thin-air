 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>   	
	</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body oncontextmenu="return false" ondrag="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="yes">
   <form id="frmFlightCancel" name="frmFlightCancel">
  <script type="text/javascript">
 	<c:out value="${requestScope.reqSeatsToBeReprotected}" escapeXml="false" />
  </script>

   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><font class="Header"><span id="spnCancelHeader"></font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>				
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../common/IncludeFormHD.jsp"%>
		  		<div id="divSchedHeader">
						<span id="spnSchedHeader"></span>
				</div>
				<div id="divFlightHeader">
					<span id="spnFlightHeader"></span>
				</div>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
			<%@ include file="../common/IncludeFormTop.jsp"%>List of Flight Reservations to be Cancelled
			 <%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr>
						<td colspan="3">
							<div id="divCancel">
								<span id="spnCancel"></span>
							</div>
						</td>
					</tr>
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td colspan="3">
							<font><span id="spnOverlappingString"></span></font>
						</td>
					</tr>
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>					
					<tr>
						<td colspan="3">
							<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table10">
							<tr>
								<td colspan="3">
									<div id="divOverWrite">
										<font class="fntBold">Flight update option</font><br>
										<input type="radio" id="radOverwrite" name="radOverwrite" value="ALL" class="NoBorder"  onClick="dataChanged()" checked>
										<font  class="fntBold">ALL</font>
										<input type="radio" id="radOverwrite" name="radOverwrite" value="SCHEDONLY"  onClick="dataChanged()" class="NoBorder">
										<font  class="fntBold">Update Flights same as schedule</font>
									</div>
								</td>
							</tr>
							<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>		
								<td colspan="3">
									<div id="divAlertHeader">	
										<span id="spnAlertHeader"></span>					
									<!--td>
										<font class="fntBold">Send Alerts - Select Options</font>
									</td>
									<td>
										<font class="fntBold">Generate Alerts</font>
									</td>								
									<td>
										<font class="fntBold">Send EMail</font>
									</td-->
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div id="divCancelFlt">
										<span id="spnCancelChk"></span>
									</div>
								</td>
							</tr>
														
							
							<tr>
								<td colspan="3">
									<div id="divResched">
										<span id="spnReSchedChk"></span>
									</div>
								</td>
							
							</tr>	
							
							<tr>
								<td colspan="3">
									<div id="divSendSMSOrEmail">
										<span id="spnSMSOrEmailChk"></span>
									</div>
								</td>
							</tr>
							
							<tr>
								<td colspan="3">
									<div id="divSMSForCnfOrOnH">
										<span id="spnSMSForCnfOrOnHChk"></span>
									</div>
								</td>
							</tr>
													
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div id="divEmailDesc">
								<span id="spnEmail"></span>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="3"><hr></td>
					</tr>
					<tr>
						<td colspan="3">
							<table width="100%" border="0" cellpadding="0" cellspacing="5">
								<tr>
									<td width="15%">
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="cancelClick()">				
									</td>
									<td>
										<div id="divClearButton">
											<input type="button" id="btnClear" name="btnClear" value="Clear Email" class="Button" onclick="clearClick()">
										</div>
									</td>
									<td align="right">							
										<!--input name="btnReprotect" type="button" class="Button" id="btnReprotect" style="width:145px"  title="Confirm Cancellation and proceed to re-protect" value="Confirm & Re-protect" onClick="reprotectClick()"-->
										<div id="divUpdateButton">
											<input name="btnConfirm" type="button" class="Button" id="btnConfirm" value="Update All" title="Confirm Updation" onClick="confirmClick()">
										</div>
									</td>
									<td align="right" width="10%">
										<div id="divShowPaxDetails">
											<input align="left" name="btnPaxDetails" type="button" class="Button" id="btnPaxDetails" value="Pax Details" title="Show Pax Details" onClick="paxDetailsClick()">
										</div>
									</td>
									<td align="right" width="10%">
										<div id="divCancelButton">
											<input name="btnConfirm" type="button" class="Button" id="btnConfirm" value="Cancel All" title="Confirm Cancellation" onClick="confirmClick()">
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
<input type="hidden" name="hdnMode" id="hdnMode" value=""/>
<input type="hidden" name="hdnFromPage" id="hdnFromPage" value="<%=request.getParameter("strFromPage")%>"/>
  </form>
  	<script src="../../js/flightschedule/validateFlightCancel.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>