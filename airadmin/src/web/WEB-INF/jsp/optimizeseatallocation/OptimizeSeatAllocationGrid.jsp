<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Optimize Seat Allocation</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<!--<script src="../../js/common/SYNCSCROLL.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>-->
	<script src="../../js/common/jquery-1.3.2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
  </head>
  <style type="text/css">
	<!--
	.Optdiv {
		border		: 1px solid #6d6b27;
		padding		: 2px;
		position	: absolute;
		width		: 100px;
		height		: 100px;
	}

	#divOpt1 {
		overflow	: hidden;
		left		: 12px;
		top			: 100px;
		width		: 385px;
		height		: 35px;
	}

	#divOpt2 {
		overflow	: hidden;
		left		: 410px;
		top			: 100px;
		width		: 490px;
		height		: 35px;
	}

	#divOpt3 {
		overflow	: auto;
		left		: 12px;
		top			: 135px;
		width		: 390px;
		height		: 435px;
	}

	#divOpt4 {
		overflow	: hidden;
		left		: 410px;
		top			: 135px;
		width		: 490px;
		height		: 420px;
	}

//-->

</style> 
  <body class="tabBGColor" onkeydown="return Body_onKeyDown(event)" onkeypress='return Body_onKeyPress(event)' onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
	  <form name="frmOptimizeGrid" id="frmOptimizeGrid" method="post" action="showOptimize.action">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Optimize Seat Allocation<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
				  			<tr>
				  				<td width="7%">
				  					<font>From Date :</font>
				  				</td>
				  				<td width="15%">
				  					<font class="fntBold" id="idFromDate"></font>
				  				</td>				  				
				  				<td width="7%">
				  					<font>To Date :</font>
				  				</td>
				  				<td width="15%">
				  					<font class="fntBold" id="idToDate"></font>
				  				</td>	
				  				<td width="5%">
				  					<font id="idFlightLable">Flight :</font>
				  				</td>
				  				<td width="15%">
				  					<font class="fntBold" id="idFlightNo"></font>
				  				</td>	
				  				<td width="5%">
				  					<font id="idAgtLable">Agent :</font>
				  				</td>
				  				<td>
				  					<font class="fntBold" id="idAgent"></font>
				  				</td>	
				  			</tr>
				  			<tr>
				  				<td>
				  					<font id="idSegLable" title="Selected O&Ds">O&amp;Ds&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</font>
				  				</td>
				  				<td colspan="4">
				  					<font class="fntBold" id="idSegs"></font>
				  				</td>
				  				<td colspan="3" align='right'>
									<table width="82%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="3%" id="tdBCManClose" class="bcStatus01"></td>
											<td><font>&nbsp;Manually Closed</font></td>
											<td width="3%" id="tdBCManOpen" class="bcStatus02"></td>
											<td><font>&nbsp;Manually Opened</font></td>
											<td width="3%" id="tdBCAutCls" class="bcStatus03"></td>
											<td><font>&nbsp;System Closed</font></td>
											<td width="3%" id="tdBCAutOpen" class="bcStatus04"></td>
											<td><font>&nbsp;System Opened</font></td>
										</tr>
									</table>
								</td>			  				
				  			</tr>
				  			<tr>
				  				<td colspan="8"><font>&nbsp;</font></td>
				  			</tr>
				  			<tr>
				  				<td colspan="8">
									<span id="idGridTable">
									<table width="100%">
										<tr>
											<td width="70%">
												<div class="Optdiv" id="divOpt1">	
													<nobr>
														<table border="0" cellspacing="1" cellpadding="2" bgcolor="black" width="727px">
															<tr bgcolor="#a7a692" align="center">
																<td width="52px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Departure Date'><b>Date</b></font></td>
																<td width="48px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Flight Number'><b>Flight#</b></font></td>
																<td width="34px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='No. of Days till Departure'><b>Days Dep</b></font></td>
																<td width="30px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Class of Service'><b>COS</b></font></td>
																<td width="122px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Flying Segment'><b>Segment</b></font></td>
																<td width="38px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Adult Allocation'><b>Alloc Ad</b></font></td>
																<td width="45px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Infant Allocation'><b>Alloc Inf</b></font></td>
																<td width="48px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Sold Seats Adults/Infants'><b>Sold Ad/Inf</b></font></td>
																<td width="48px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='On hold seats Adults/Infants'><b>Onhold Ad/Inf</b></font></td>
																<td width="45px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Oversell allowed on Segment'><b>Over<BR>sell</b></font></td>
																<td width="45px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Curtailed on Segment'><b>Curt<BR>ailed</b></font></td>
																<td width="45px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Available Seats Adults/Infants'><b>Avai Ad/Inf</b></font></td>
																<td width="25px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Booking Class'><b>BC</b></font></td>
																<td width="25px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Logical CC'><b>Logi: CC</b></font></td>
															</tr>
														</table>
													</nobr>
												</div>
											</td>
											<td width="30%">
												<div class="Optdiv" id="divOpt2">	
													<nobr>
														<table border="0" cellspacing="1" cellpadding="2" bgcolor="black" width="485px">
															<tr bgcolor="#a7a692" align="center">
																<td width="45px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Allocated to Booking Class'><b>Allo.</b></font></td>
																<td width="40px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Closed'><b>Closed</b></font></td>
																<td width="50px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Fixed Allocation'><b>Fixed Y/N</b></font></td>
																<td width="50px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Priority'><b>Priority Y/N</b></font></td>
																<td width="35px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Sold'><b>Sold</b></font></td>
																<td width="45px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Onhold'><b>Onhold</b></font></td>
																<td width="55px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Seats Cancelled from BC'><b>Cancelled</b></font></td>
																<td width="50px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Seats inherited from other Booking Classes'><b>Aquired</b></font></td>
																<td width="55px" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font title='Available'><b>Available</b></font></td>
															</tr>
														</table>
													</nobr>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="Optdiv" id="divOpt3">	
													<nobr>
														<span id="spn1" class="FormBackGround"></span>
													</nobr>
												</div>
											</td>
											<td>
												<div class="Optdiv" id="divOpt4">	
													<nobr>
														<span id="spn2" class="FormBackGround"></span>
													</nobr>
												</div>
											</td>
										</tr>
									</table>
									</span>
								</td>
				  			</tr>
				  			<tr style="height:480px">
				  				<td colspan="8"><font>&nbsp;</font></td>
				  			</tr>
				  			<tr>
								<td colspan="4">
									<!-- <input type="button" id="btnCalculate" class="Button" value="Calculate" onclick="CalculateClick()"> -->
									<input type="button" id="btnClose" class="Button" value="Close"  onclick="BackCloseClick('Close')">
									<input type="button" id="btnBack" class="Button" value="Back"  onclick="BackCloseClick('Back')">
								</td>
								<td colspan="4" align="right">
									<u:hasPrivilege privilegeId="plan.invn.opt.export">
										<input type="button" id="btnExport" class="Button" value="Export" onclick="ExportClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="plan.invn.opt.edit">
										<input type="button" id="btnSave" class="Button" value="Save" onclick="SaveClick()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
	<script type="text/javascript">
		<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		<c:out value="${requestScope.reqOptimizeResult}" escapeXml="false" />
		<c:out value="${requestScope.reqFormData}" escapeXml="false" />
		var optiStatus = "<c:out value="${requestScope.reqOptimizeStatus}" escapeXml="false" />";
	</script> 
	<script type="text/javascript">
   	   <!--
	   var objProgressCheck = setInterval("ClearProgressbar()", 300);
	   function ClearProgressbar(){
		    clearTimeout(objProgressCheck);
		    top[2].HideProgress();
	   }
      //-->
    </script>
	<script src="../../js/optimizeseatallocation/OptimizeSeatAllocationGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>					  
	    <input type="hidden" name="hdnMode" id="hdnMode"/> 
  		<input type="hidden" name="hdnVersion" id="hdnVersion"/> 
		<input type="hidden" name="txtFromDate" id="txtFromDate"/> 
		<input type="hidden" name="txtToDate" id="txtToDate"/> 
		<input type="hidden" name="txtFlightNo" id="txtFlightNo"/> 
		<input type="hidden" name="hdnAssignOnD" id="hdnAssignOnD"/>	
		<input type="hidden" name="hdnAssignBC" id="hdnAssignBC"/> 	
		<input type="hidden" name="hdnAllocType" id="hdnAllocType"/> 
	    <input type="hidden" name="txtSeats" id="txtSeats"/> 
  		<input type="hidden" name="selMoreLess" id="selMoreLess"/> 
		<input type="hidden" name="radSegLevel" id="radSegLevel"/> 
		<input type="hidden" name="radBCLevel" id="radBCLevel"/> 
		<input type="hidden" name="radShowBc" id="radShowBc"/> 
		<input type="hidden" name="radAgent" id="radAgent"/>	
		<input type="hidden" name="selAgent" id="selAgent"/> 	
		<input type="hidden" name="hdnBCDisType" id="hdnBCDisType"/> 
		<input type="hidden" name="hdnLogicalCC" id ="hdnLogicalCC"/>
		<input type="hidden" name="hdnOptimizeValues" id="hdnOptimizeValues" style="width:800px"/> 
	</form>
  </body>
</html>
