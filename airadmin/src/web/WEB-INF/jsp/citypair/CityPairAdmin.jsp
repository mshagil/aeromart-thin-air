<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="../common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>
			Template Page
		</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/citypair/CityPairValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	</head>

	<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" ondrag='return false' onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
		<form action="showCityPair.action" method="post" name="frmCityPair" id="frmCityPair">
			<script type="text/javascript">
				<c:out value="${requestScope.reqHtmlCityPairData}" escapeXml="false" />	
				var totalNoOfRecords =<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />	
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqOriginFocus}" escapeXml="false" />
				<c:out value="${requestScope.reqDestinationFocus}" escapeXml="false" />	
				var directRoutUsageArr = new Array();
				<c:out value="${requestScope.reqHtmlDirectRouteUsageData}" escapeXml="false" />
				var routsInUse = new Array();
				<c:out value="${requestScope.reqHtmlRoutesInUseData}" escapeXml="false" />
			</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">			
				<tr>						
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Search Route<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4">
									<table width='100%' border='0' cellpadding='1' cellspacing='0'>
										<tr valign="top">
											<td width='8%'><font>Origin</font></td>
											<td width="10%">
												<select id='selOriginSearch' name='selOriginSearch' size='1' tabindex="1">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
												
											</td>
											<td width='8%'><font>Destination</font></td>
											<td width="10%">
												<select id='selDestinationSearch' name='selDestinationSearch' size='1' tabindex="2">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
												
											</td>
											<td width='5%'><font>Via 1</font></td>
											<td width="9%">
												<select id='selVia1Search' name='selVia1Search' size='1' tabindex="3">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td width='5%'><font>Via 2</font></td>
											<td width="9%">
												<select id='selVia2Search' name='selVia2Search' size='1' tabindex="4">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td width="5%"><font>Via 3</font></td>
											<td width="9%">
												<select id='selVia3Search' name='selVia3Search' size='1' tabindex="5">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td width="5%"><font>Via 4</font></td>
											<td width="9%">
												<select id='selVia4Search' name='selVia4Search' size='1' tabindex="6">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td align="right" valign="bottom">
												<input name="btnSearch" type="button" class="Button" id="btnSearch" onClick="searchCityPair()" value="Search" tabindex="7">
											</td>
										</tr>
																				
									</table>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>

				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Routes<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
							<tr>
								<td>
									<font class="fntSmall">&nbsp;</font></td>
							</tr>
							<tr>
								<td>
									<span id="spnCityPairs"></span>
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.route.add">
										<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()" tabindex="8">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.route.edit">
										<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()" tabindex="9">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.route.delete">
										<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()" tabindex="10">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<font class="fntSmall">
							&nbsp;
						</font>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Edit Route
						<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
							<tr>
								<td colspan="4">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<table width='100%' border='0' cellpadding='1' cellspacing='0'>
										<tr valign="top">
											<td width='14%'>
												<font>
													Origin
												</font>
											</td>
											<td width="10%">
												<select id='selOrigin' name='selOrigin' size='1' onChange="clickChange()" tabindex="11">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
												<font class="mandatory">
													&nbsp;*
												</font>
											</td>
											<td width='8%'>
												<font>
													Destination
												</font>
											</td>
											<td width="10%">
												<select id='selDestination' name='selDestination' size='1' onChange="clickChange()" tabindex="12">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
												<font class="mandatory">
													&nbsp;*
												</font>
											</td>
											<td width='5%'>
												<font>
													Via 1
												</font>
											</td>
											<td width="10%">
												<select id='selVia1' name='selVia1' size='1' onChange="clickChange()" tabindex="13">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td width='5%'>
												<font>
													Via 2
												</font>
											</td>
											<td width="9%">
												<select id='selVia2' name='selVia2' size='1' onChange="clickChange()" tabindex="14">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td width="5%">
												<font>
													Via 3
												</font>
											</td>
											<td width="9%">
												<select id='selVia3' name='selVia3' size='1' onChange="clickChange()" tabindex="15">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
											<td width="5%">
												<font>
													Via 4
												</font>
											</td>
											<td>
												<select id='selVia4' name='selVia4' size='1' onChange="clickChange()" tabindex="16">
													<option value=""></option>
													<c:out value="${requestScope.reqViaList}" escapeXml="false" />
												</select>
											</td>
										</tr>
										<tr>
											<td>
												<font>
													Distance (KM)
												</font>
											</td>
											<td colspan="2">
												<input type="text" id="txtDistance" name="txtDistance" style="width:75px;" size="9" class="rightText" maxlength="9" onChange="clickChange()" onKeyPress="DistancePress(this)" onKeyUp="DistancePress(this)" tabindex="17">
												<font class="mandatory">
													*
												</font>
											</td>
											<td colspan="2">
												<font>
													Travel Duration (HH:MM)
												</font>
											</td>
											<td colspan="7">
												<input type="text" id="txtDuration" name="txtDuration" style="width:75px;" maxlength="5" onChange="clickChange()" onblur="setTimeWithColon(document.forms[0].txtDuration)" onKeyPress="DurationPress(this)" onKeyUp="DurationPress(this)" tabindex="18">
												<font class="mandatory">
													*
												</font>
											</td>
										</tr>
										<tr>
											<td>
												<font>
													Duration Tolerance %
												</font>
											</td>
											<td colspan="2">
												<input type="text" id="txtTolerance" name="txtTolerance" style="width:75px;" size="6" class="rightText" maxlength="5" onChange="clickChange()" onKeyPress="TolerancePress(this)" onKeyUp="TolerancePress(this)" tabindex="19">
											</td>
											<td colspan="2">
												<font>
													Active
												</font>
											</td>
											<td colspan="1">
												<input name="chkStatus" type="checkbox" id="chkStatus" size="1" value="Active" onChange="clickChange()" tabindex="20">
											</td>											
											<td colspan="3">
												<div id="divLCCConStatus">
													<table width='100%' border='0' cellpadding='1' cellspacing='0'>
														<tr>
															<td>
																<font>
																	LCC Published
																</font>
															</td>
															<td colspan="3">
																<input name="chkLCCPublishedStatus" type="checkbox" id="chkLCCPublishedStatus" size="1" value="Active" disabled="disabled">
															</td>
														</tr>
													</table>
												</div>	
											</td>								
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="4" style="height:53px;">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="3" valign="bottom">
									<input type="button" id="btnClose" class="Button" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" tabindex="21">
									<input name="btnReset" type="button" class="Button" id="btnReset" onClick="resetClick()" value="Reset" tabindex="22">
								</td>
								<td align="right" valign="bottom">
									<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveCityPair()" value="Save" tabindex="23">
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<font class="fntSmall">
							&nbsp;
						</font>
					</td>
				</tr>
			</table>

			<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
			<input type="hidden" name="hdnRouteId" id="hdnRouteId" />
			<input type="hidden" name="hdnGridRow" id="hdnGridRow" />
			<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1" />
			<input type="hidden" name="hdnROuteCode" id="hdnROuteCode" />
			<input type="hidden" name="hdnAction" id="hdnAction" />
			<input type="hidden" name="hdnId" id="hdnId" />
			<input type="hidden" name="hdnVia1Id" id="hdnVia1Id" />
			<input type="hidden" name="hdnVia2Id" id="hdnVia2Id" />
			<input type="hidden" name="hdnVia3Id" id="hdnVia3Id" />
			<input type="hidden" name="hdnVia4Id" id="hdnVia4Id" />
			<input type="hidden" name="hdnSearchCriteria" id="hdnSearchCriteria" value="">
		</form>
		<script src="../../js/citypair/CityPairAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	</body>

	<script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>

</html>
