var temGrid;

var IMAGE_UPLOAD_TYPE = {LCC : 'logicalCCImage', WITH_FLEXI : 'logicalCCWithFlexiImage'};
var TEMP_IMG_PREFIX = {LCC : 'logicalCCImage', WITH_FLEXI : 'logicalCCWithFlexiImage'};
var lblLCCImage = "Upload Logical Cabin Tooltip Images";
var lblWithFlexiImage = "Upload With Flexi Logical Cabin Tooltip Images";
var uploadingType = '';
var strLccUploads = {};
var strWithFlexiUploads = {};
var selectedRowId = -1;
var imageSuffix = '.png';

jQuery(document).ready(function(){
	disableControls(true);
	$("#divSearchPanel").decoratePanel("Search Logical Cabin Classes");
	$("#divResultsPanel").decoratePanel("Logical Cabin Classes");
	$("#divDispResultsPanel").decoratePanel("Add/Modify Logical Cabin Classes");
	
	//Allows manipulation of the grid data as actual JSON data instead of String values via the variable temGrid.
	temGrid=jQuery("#listCabinClass").jqGrid({
		datatype: function(postdata) {
	        $.ajax({
		           url: 'loadLogicalCabinClass!searchCabinClass.action',
		           data: $.extend(postdata, null),
		           dataType:"json",
		           complete: function(jsonData,stat){	        	  
		              if(stat=="success") {
		            	  mydata = eval("("+jsonData.responseText+")");
		            	  if (mydata.msgType != "Error"){
			            	 $.data(temGrid[0], "gridData", mydata.rows);
			            	 temGrid[0].addJSONData(mydata);
		            	  }else{
		            		  alert(mydata.message)
		            	  }
		              }
		           }
		        });
		    },
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},
		colNames:['&nbsp','Logical CC','Description', 'CC','Rank','Status','','&nbsp', '', '', '', '', '','','','','','','','','',''], 
		colModel:[ 	{name:'Id', width:40, jsonmap:'id'},
		           	{name:'logicalCCCode', width:50, jsonmap:'logicalCC'},  
				   	{name:'description', width:150,  jsonmap:'description'},
					{name:'cabinClassCode', width:50, align:"center", jsonmap:'cabinClass'},
				   	{name:'nestRank', width:50, align:"center", jsonmap:'rank'},
				   	{name:'clsStatus', width:100, align:"center", jsonmap:'status'},
				   	{name:'comments', width:100, align:"center",hidden:true,  jsonmap:'comments'},
				   	{name:'lccVersion', width:125, align:"center",hidden:true,  jsonmap:'version'},
					{name:'allowSingleMealOnly', width:225, align:"center",hidden:true,  jsonmap:'allowSingleMealOnly'},
					{name:'freeSeatEnabled', width:225, align:"center",hidden:true,  jsonmap:'freeSeatEnabled'},
					{name:'freeFlexiEnabled', width:225, align:"center",hidden:true,  jsonmap:'freeFlexiEnabled'},
					{name:'flexiDescription', width:225, align:"center",hidden:true,  jsonmap:'flexiDescription'},
					{name:'flexiComment', width:225, align:"center",hidden:true,  jsonmap:'flexiComment'},
					{name:'descriptionI18n', width:225, align:"center",hidden:true,  jsonmap:'descriptionI18n'},
					{name:'flexiDescriptionI18n', width:225, align:"center",hidden:true,  jsonmap:'flexiDescriptionI18n'},
					{name:'flexiCommentI18n', width:225, align:"center",hidden:true,  jsonmap:'flexiCommentI18n'},
					{name:'commentsI18n', width:225, align:"center",hidden:true,  jsonmap:'commentsI18n'},
					{name:'descriptionForDisplay', width:225, align:"center",hidden:true,  jsonmap:'descriptionForDisplay'},
					{name:'flexiDescriptionForDisplay', width:225, align:"center",hidden:true,  jsonmap:'flexiDescriptionForDisplay'},
					{name:'flexiCommentForDisplay', width:225, align:"center",hidden:true,  jsonmap:'flexiCommentForDisplay'},
					{name:'commentsForDisplay', width:225, align:"center",hidden:true,  jsonmap:'commentsForDisplay'},
					{name:'bundleDescId', width:50, align:"center",hidden:true,  jsonmap:'bundleDescId'}
				   	],
		imgpath: '../../themes/default/images/jquery', multiselect:false,
		pager: jQuery('#temppager'),
		rowNum:20,						
		viewrecords: true,
		height:175,
		width:925,
		loadui:'block',
		onSelectRow: function(rowid){
			selectedRowId = rowid;
			$('#frmCabinClass').clearForm();
			hideMessage();
			$("#rowNo").val(rowid);	
			fillForm(rowid);
			setLCCForDisplayList();
			disableControls(true);
			enableGridButtons(true);	
			resetImagesUploaded();
		}
	}).navGrid("#temppager",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$('#logicalCCCode').keyup(function() {
		var str = $("#logicalCCCode").val(); 
		str = str.replace(/[^a-zA-Z 0-9]+/g,'');
		$("#logicalCCCode").attr("value",str);
	});
	
	$('#description').keyup(function(e) {
		var str = $("#description").val(); 
		str = str.replace(/[^a-zA-Z 0-9]+/g,'');
		$("#description").attr("value",str);
		if(e.keyCode == 13) {
			e.preventDefault();
	        return false;
		}
	});
	
	$('#nestRank').keyup(function() {
		var str = $("#nestRank").val(); 
		str = str.replace(/[^0-9]+/g,'');
		$("#nestRank").attr("value",str);
	});
	
	$('#btnAdd').click(function() {
		setField("hdnMode","ADD");
		hideMessage();
		disableControls(false);
		setField("version",-1);
		setField("descriptionI18n","");
		setField("flexiDescriptionI18n","");
		setField("flexiCommentI18n","");
		setField("commentsI18n","");
		$('#btnEdit').disableButton();
		$('#btnDelete').disableButton();
		$('#btnInternationalize').disableButton();
		$("#btnSave").enableButton();
		$('#frmCabinClass').resetForm();		
		
	});
	
	$('#btnEdit').click(function() {
		setField("hdnMode","EDIT");
		disableControls(false);
		if ($(".ui-state-highlight").length == 0/* && type=='edit'*/){ 
			showERRMessage("Select a row to edit");	
		}
		if($('#selLogicalCabinClass').val() != null){
			$("#selLogicalCabinClass").attr('disabled', true); 
			$("#selCabinClass").attr('disabled', true);
			$("#logicalCCCode").attr('disabled', true); 
			$("#cabinClass").attr('disabled', true); 
			$('#btnInternationalize').disableButton();
		}
		$("#btnSave").enableButton();
	}); 
	
	$('#btnReset').click(function() {
		if($("#rowNo").val() == '') {
			$('#frmCabinClass').resetForm();
		}else {			
			fillForm($("#rowNo").val());
		}			
	});
	
	$('#btnDelete').click(function() {
		if ($(".ui-state-highlight").length == 0/* && type=='edit'*/){ 
			showERRMessage("Select a row to delete");	
		}
		disableControls(false);
		$('#frmCabinClass').ajaxSubmit(delOptions);
		return false;			
	}); 
	
	$('#btnSave').click(function() {
		disableControls(false);
		if($("#status").attr('checked')){
			$('#status').attr("value","ACT")
		}else{
			$('#status').attr("value","INA")
		}
		
		if(strLccUploads != null && strLccUploads != ''){		
			$('#strLccImageUploads').val(JSON.stringify(strLccUploads));
		}
		if(strWithFlexiUploads != null && strWithFlexiUploads != ''){
			$('#strWithFlexiImageUploads').val(JSON.stringify(strWithFlexiUploads));
		}
		
		$('#frmCabinClass').ajaxSubmit({
			cache: false,	 
			beforeSubmit:  showRequest,
			success: showResponse,
			async: false,
			dataType:  'json'
		});
		document.getElementById('frmCabinClass').reset();
		return false;
	});
	
	$('#btnSearch').click(function(){		
		hideMessage();
		$("#frmSearchLogicalCabinClass").ajaxSubmit({ dataType: 'json', success: displayData});	
	});
	
	$('#btnInternationalize').click(function() {
		disableControls(false);
		openTranslationPopup();
	});
	
	//This is the HTML code for the popup form
	var htmlCodeForPopup='<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr>  <td>   <table width="100%" cellpadding="0" cellspacing="0" border="0"    align="center" ID="Table4">    <tr>     <td width="10" height="20"><img      src="../../images/form_top_left_corner_no_cache.gif">     </td>     <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font      class="FormHeader"> Logical Cabin Class(LCC) Information For Display </font></td>     <td width="11" height="20"><img      src="../../images/form_top_right_corner_no_cache.gif">     </td>    </tr>    <tr>     <td class="FormBackGround"></td>     <td class="FormBackGround" valign="top">      <table>       <tr>        <td width="663" align="left"><table width="98%"          height="105" border="0" cellpadding="0" cellspacing="0">          <tr align="left" valign="middle" class="fntMedium">           <td width="19%" class="fntMedium"><font>Language</font>           </td>           <td width="33%" align="left" style="padding-bottom: 3px;">            <select name="language" id="language" style="width: 150px;">             <option value="-1"></option> ';
	htmlCodeForPopup+=languageHTML;
	htmlCodeForPopup+='</select> </td> <td width="13%" rowspan="3" align="center" valign="top"> <input name="add" type="button" class="Button" id="add" style="width: 50px;" title="Add Item"            onclick="addLCCForDisplay();" value="&gt;&gt;" />             <input name="remove" type="button" class="Button" id="remove" style="width: 50px;"             title="Remove Item" onclick="removeLCCForDisplay();" value="&lt;&lt;" />           </td>           <td width="35%" rowspan="3" align="left" valign="top">           <table width="100%" border="0" cellspacing="0" cellpadding="0">             <tr>              <td>              <select name="translation" size="5" id="translation" style="width: 175px;               height: 100px;">              </select>              </td>             </tr>             <tr>             </tr>           </table>           </td>          </tr>          <tr align="left" valign="middle" class="fntMedium">           <td width="25%" valign="top" class="fntMedium">            <font>LCC Name* </font>           </td>           <td align="left" valign="top">            <input name="txtLCCNameDisplay" type="text" id="txtLCCNameDisplay" size="32" />           </td>          </tr>          <tr align="left" valign="middle" class="fntMedium">           <td width="25%" valign="top" class="fntMedium">            <font>Comment* </font>           </td>           <td valign="top"><input name="txtCommentDisplay"            type="text" id="txtCommentDisplay" size="32"   />           </td>          </tr>          <tr align="left" valign="middle" class="fntMedium">           <td width="25%" valign="top" class="fntMedium">            <font>With Flexi Name* </font>           </td>           <td valign="top"><input name="txtFlexiDescription"            type="text" id="txtFlexiDescription" size="32"    />           </td>          </tr>          <tr align="left" valign="middle" class="fntMedium">           <td width="25%" valign="top" class="fntMedium">            <font>Flexi Comment* </font>           </td>           <td valign="top"><input name="txtFlexiComment"            type="text" id="txtFlexiComment" size="32"  />           </td>          </tr>         </table>        </td>       </tr>       <tr>        <td align="top"><span id="spnSta"></span>        </td>       </tr>       <tr>        <td align="right">         <input type="button" id="btnUpdate" value="Save" class="Button" onClick="saveLCCForDisplay();">         &nbsp;         <input type="button" id="btnPopupClose" value="Close" class="Button" onClick="closeDialog();">        </td>       </tr>      </table>     </td>     <td class="FormBackGround"></td>    </tr>    <tr>     <td height="12"><img      src="../../images/form_bottom_left_corner_no_cache.gif">     </td>     <td height="12" class="FormBackGround"><img      src="../../images/spacer_no_cache.gif" width="100%" height="1">     </td>     <td height="12"><img      src="../../images/form_bottom_right_corner_no_cache.gif">     </td>    </tr>   </table></td> </tr></table>';
	$('#displayData').html(htmlCodeForPopup);
	$('#displayData').hide()
	var popupDialog = $('#displayData');

	popupDialog.dialog({
	    width:    600,
	    autoOpen: false,
	    modal: true,
	    dialogClass: 'alert'
	});
	
	$('#selImageLanguage').change(function() {
		showUploadedImage($(this).val());
	});
    
    $('#btnUpload').click(function() {
    	var data = {};
    	data['mode'] = uploadingType;
    	data['lang'] = $('#selImageLanguage').val();
    	
		$('#frmImageUpload').ajaxSubmitWithFileInputs({				 
			beforeSubmit:  showUpdate,  
			success: showUploadResponseImage,
			data: data,
			url: 'showUpload.action',
			dataType:  'script'     
		});
		return false;	
	});
	
	$("#btnToolTipImage").click(function() {
    	initImageUploadDialog(IMAGE_UPLOAD_TYPE.LCC);
	});
    
    $("#btnWithFlexiToolTipImage").click(function() {
    	initImageUploadDialog(IMAGE_UPLOAD_TYPE.WITH_FLEXI);
	});
    
    $('#btnImageDialogClose').click(function(){
    	$('#imageUploadDialog').dialog('close');
    });
	
	$(".ui-dialog-titlebar").hide(); //Hide popup title bar.
});

var options = {
		cache: false,	 
		beforeSubmit:  showRequest,
		success: showResponse,
		async: false,
		dataType:  'json'
	}; 

var delOptions = {	
		cache: false, 
		beforeSubmit:  showDelete,
		success: showResponse,   				 
		url: 'loadLogicalCabinClass!deleteLogicalCabinClass.action',        
		dataType:  'json',       
		resetForm: true        			 
	 };

initImageUploadDialog = function(uploadType){
	uploadingType = uploadType;
	var title = lblLCCImage;
	if(uploadType == IMAGE_UPLOAD_TYPE.WITH_FLEXI){
		title = lblWithFlexiImage;
	}
	$("#imgLcc").attr('src', '');
	$('#selImageLanguage').find('option:first').attr('selected', 'selected');
	showUploadedImage($('#selImageLanguage').val());
	$("#imageUploadDialog").dialog({ height: 400, width: 500, title: title, modal: true });	
}

function showUploadedImage(selectedLang){
	if($('#hdnMode').val() == 'EDIT' && selectedRowId != -1){		
		var selLCCCode = jQuery("#listCabinClass").getCell(selectedRowId,'logicalCCCode');
		var imageName = '';
		if(uploadingType == IMAGE_UPLOAD_TYPE.LCC){
			if(strLccUploads[selectedLang] == undefined || strLccUploads[selectedLang] == null
					|| !strLccUploads[selectedLang]){				
				imageName = selLCCCode + '_N_' + selectedLang + imageSuffix;		
			} else {
				imageName = tempLccPrefix + '_test_' + selectedLang + imageSuffix;
			}
		} else {
			if(strWithFlexiUploads[selectedLang] == undefined || strWithFlexiUploads[selectedLang] == null
					|| !strWithFlexiUploads[selectedLang]){
				imageName = selLCCCode + '_Y_' + selectedLang + imageSuffix;
			} else {
				imageName = tempWithFlexiPrefix + '_test_' + selectedLang + imageSuffix;
			}
		}
		
	} else if($('#hdnMode').val() == 'ADD'){
		if(uploadingType == IMAGE_UPLOAD_TYPE.LCC && 
				(strLccUploads[selectedLang] != undefined && strLccUploads[selectedLang] != null
						&& strLccUploads[selectedLang])){
			imageName = tempLccPrefix + '_test_' + selectedLang + imageSuffix;
		} else if(uploadingType == IMAGE_UPLOAD_TYPE.WITH_FLEXI && 
				(strWithFlexiUploads[selectedLang] != undefined && strWithFlexiUploads[selectedLang] != null
						&& strWithFlexiUploads[selectedLang])) {
			imageName = tempWithFlexiPrefix + '_test_' + selectedLang + imageSuffix;
		}
	}
	
	$("#imgLcc").safeImageUrl({url : lccImagePath + imageName + '?rel=' + new Date().getTime()});
}

function showUpdate(formData, jqForm, options) {
	var imgPath = $("#fileHOTFile").val();
	if(imgPath == null || imgPath == ''){
		showCommonError("Error", "Please select an image to upload");
		return false;
	} else {
		var newImg = new Image();
		newImg.src = $("#imgLcc").attr('src');	
		var height = newImg.height;
		var width = newImg.width;
		
		if(width == 1024 || height == 700){
			showCommonError("Error", "Image is too large ");
			return false;
		}
		top[2].ShowProgress();
	}
}

function showUploadResponseImage(responseText, statusText)  {
	top[2].HideProgress();
    eval(responseText);		    		
    showCommonError(responseText['msgType'], responseText['succesMsg']);
    if(responseText['msgType'] != "Error"){
    	var selectedLang = responseText['lang'];
    	var imageName = '';
    	$('#dvUpLoad').html($('#dvUpLoad').html());

    	if(uploadingType == IMAGE_UPLOAD_TYPE.LCC){
    		imageName = tempLccPrefix + '_test_' + selectedLang + imageSuffix;
    		strLccUploads[selectedLang] = true;
    	} else {    		
    		imageName =  tempWithFlexiPrefix +'_test_' + selectedLang + imageSuffix;
    		strWithFlexiUploads[selectedLang] = true;
    	}

    	$("#imgLcc").safeImageUrl({url : lccImagePath + imageName + '?rel=' + new Date().getTime()});
    } else {
    	if(uploadingType == IMAGE_UPLOAD_TYPE.LCC){
    		strLccUploads[selectedLang] = false;
    	} else {
    		strWithFlexiUploads[selectedLang] = false;
    	}
    }
    
} 

function fillForm(rowid) {
	jQuery("#listCabinClass").GridToForm(rowid, '#frmCabinClass');	
	if(jQuery("#listCabinClass").getCell(rowid,'clsStatus') == 'ACT'){
		$("#status").attr('checked', true);
	}else {
		$("#status").attr('checked', false);
	}
	
	if(jQuery("#listCabinClass").getCell(rowid,'allowSingleMealOnly') == 'Y'){
		$("#chkAllowSingleMeal").attr('checked', true);
	}else {
		$("#chkAllowSingleMeal").attr('checked', false);
	}
	
	if(jQuery("#listCabinClass").getCell(rowid,'freeSeatEnabled') == 'Y'){
		$("#chkEnableFreeSeat").attr('checked', true);
	}else {
		$("#chkEnableFreeSeat").attr('checked', false);
	}
	
	if(jQuery("#listCabinClass").getCell(rowid,'freeFlexiEnabled') == 'Y'){
		$("#chkEnableFreeFlexi").attr('checked', true);
	}else {
		$("#chkEnableFreeFlexi").attr('checked', false);
	}
	
	$("#logicalCCCode").val($("#listCabinClass").getCell(rowid, 'logicalCCCode'));
	$("#description").val($("#listCabinClass").getCell(rowid, 'description'));
	$("#cabinClassCode").val($("#listCabinClass").getCell(rowid, 'cabinClassCode'));
	$("#comments").val($("#listCabinClass").getCell(rowid, 'comments'));
	$("#nestRank").val($("#listCabinClass").getCell(rowid, 'nestRank'));
	$("#flexiComment").val($("#listCabinClass").getCell(rowid,'flexiComment'));
	$("#flexiDescription").val($("#listCabinClass").getCell(rowid, 'flexiDescription'));
	$("#version").val($("#listCabinClass").getCell(rowid, 'lccVersion'));
	$("#bundleDescId").val($("#listCabinClass").getCell(rowid, 'bundleDescId'));
}

/**
 * Set the translations in the ListBox in the usual format LANGUAGE=>TranslatedMessage=>TranslatedMessage etc.
 */
function setLCCForDisplayList(){			

	if(jQuery("#listMeal").getCell($("#rowNo").val())>=0 ){
		
		$('#txtLCCNameDisplay').val('');
		$('#txtCommentDisplay').val('');
		$('#txtFlexiDescription').val('');
		$('#txtFlexiComment').val('');
	
		var lccList = getFieldByID("translation");				
		lccList.options.length = 0;
		var langCode ;
		var mealName ;
		var langName;
		var mealTitle;
		var i=0;
		
		var descriptionTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].descriptionForDisplay;
		var commentTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].commentsForDisplay;
		var flexiDescTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].flexiDescriptionForDisplay;
		var flexiCommentTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].flexiCommentForDisplay;
		
		var langArr=new Array();
		
		for (var key in descriptionTranslation) 
		{
			var selLangName = getListLabel("language", key);
			var lccName = descriptionTranslation[key];
			var comment = commentTranslation[key];
			var flexiDescription = flexiDescTranslation[key];
			var flexiComment = flexiCommentTranslation[key];
			
			addToList("translation", selLangName+"=>"+lccName+"=>"+comment+"=>"+flexiDescription+"=>"+flexiComment, key);
		}
	}
}

function displayData(response){
	$("#listCabinClass")[0]
	.addJSONData(response);
}

function hideMessage() {
	top[2].HidePageMessage();
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	hideMessage();
}

function disableControls(cond){
	$("input").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("textarea").each(function(){ 
	      $(this).attr('disabled', cond); 
	});
	$("#cabinClassCode").attr('disabled',cond);
	$("#bundleDescId").attr('disabled',cond);
	$("#cabinClass").attr('disabled', cond);
	$("#btnClose").attr('disabled', false);	
	$("#btnSearch").attr('disabled', false);
	$("#btnAdd").attr('disabled', false);
	$("#btnInternationalize").attr('disabled',false);
}

function enableGridButtons(cond){
	if(cond) {
		$("#btnEdit").enableButton();
		$("#btnDelete").enableButton();
		$("#btnInternationalize").enableButton();
	}else {
		$("#btnEdit").disableButton();
		$("#btnDelete").disableButton();
		$("#btnInternationalize").disableButton();
	}	
}

function enableSearch(){
	$("#btnSearch").attr('disabled', false);		
	$("#selStatus").attr('disabled', false);
}

function showDelete(formData, jqForm,delOptions) {				
	return confirm(deleteRecoredCfrm);  
} 

function showResponse(responseText, statusText) { 
	showCommonError(responseText['msgType'], responseText['succesMsg']);
	if(!(responseText['msgType'] == "Error")){
		$('#frmCabinClass').resetForm();
	}
	jQuery("#listCabinClass").trigger("reloadGrid");
	$("#selLogicalCabinClass").attr('disabled', false); 
	$("#selCabinClass").attr('disabled', false);
	enableGridButtons(false);
	resetImagesUploaded();
}

function resetImagesUploaded(){
	strLccUploads = {};
	strWithFlexiUploads = {};
	$('#strLccImageUploads').val('');
	$('#strWithFlexiImageUploads').val('');
}

function showRequest(formData, jqForm, options) { 
	if(trim($("#logicalCCCode").val()) == ""){
		showCommonError("Error", "Logical Cabin Class Required");
		return false;
	}else if(trim($("#cabinClassCode").val()) == ""){
		showCommonError("Error", "Cabin Class Required");
		return false;
	}else if(trim($("#description").val()) == ""){
		showCommonError("Error", "Logical Cabin Class Description Required");
		return false;
	}else if(trim($("#nestRank").val()) == ""){
		showCommonError("Error", "Rank Required");
		return false;
	}else if(trim($("#flexiDescription").val()) == ""){
		showCommonError("Error", "Flexi Description Required");
		return false;
	}else if(trim($("#comments").val()) == ""){
		showCommonError("Error", "Comment Required");
		return false;
	}else if(trim($("#flexiComment").val()) == ""){
		showCommonError("Error", "Flexi Comment Required");
		return false;
	}
}

function openTranslationPopup(){
	if($("#logicalCCCode").val()=="" || $("#logicalCCCode").val()==null)
	{
		alert("No Logical Cabin Class is selected, please select a Logical Cabin CLass and try again.");
		return;
	}
	$('#displayData').dialog('open');
}

function addLCCForDisplay()
{			
	if($('#txtLCCNameDisplay').val().length==0 || $('#txtCommentDisplay').val().length==0 || $('#txtFlexiDescription').val().length==0
			|| $('#txtFlexiComment').val().length==0)
	{
		return false;
	}
	var selLang = $('#language').val();
	if(selLang == '-1')
		return false;
	if(findValueInList(selLang, "translation") == true)
	{
		alert("Meal description for the selected language already exist");
		return false;
	}

	var selLangName = getCurSelectedLabel("language");
	var lccName = $('#txtLCCNameDisplay').val();
	var comment = $('#txtCommentDisplay').val();
	var flexiDescription = $('#txtFlexiDescription').val();
	var flexiComment = $('#txtFlexiComment').val();
	addToList("translation", selLangName+"=>"+lccName+"=>"+comment+"=>"+flexiDescription+"=>"+flexiComment, selLang);	
	$('#txtLCCNameDisplay').val('');
	$('#txtCommentDisplay').val('');
	$('#txtFlexiDescription').val('');
	$('#txtFlexiComment').val('');
	setField("language","-1");
}

function removeLCCForDisplay()
{			
	var lccList = getFieldByID("translation");
	var selected = lccList.selectedIndex;
	if(selected != -1){
		var langCode = lccList.options[selected].value;
		var label = lccList.options[selected].text.split("=>");
		$('#language').val(langCode);
		$('#txtLCCNameDisplay').val(label[1]);
		$('#txtCommentDisplay').val(label[2]);
		$('#txtFlexiDescription').val(label[3]);
		$('#txtFlexiComment').val(label[4]);
		lccList.remove(selected);
	}
}

function closeDialog()
{
	$('#displayData').dialog('close');
}

function saveLCCForDisplay()
{
	if($('#txtLCCNameDisplay').val().length>0 || $('#txtCommentDisplay').val().length>0 || $('#txtFlexiDescription').val().length>0
			|| $('#txtFlexiComment').val().length>0 && getValue("language")!='-1'){
		if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
			return false;
	}
	
	var descriptionTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].descriptionForDisplay;
	var commentTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].commentsForDisplay;
	var flexiDescTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].flexiDescriptionForDisplay;
	var flexiCommentTranslation=$.data(temGrid[0], "gridData")[$("#rowNo").val() - 1].flexiCommentForDisplay;
	
	$('#translation > option').each(function(index) {
	    var row = $(this).text();
		var langCode=$(this).val();
		
		var splitString=new Array();
		splitString=row.split("=>");
		
		descriptionTranslation[langCode]=splitString[1];
		commentTranslation[langCode]=splitString[2];
		flexiDescTranslation[langCode]=splitString[3];
		flexiCommentTranslation[langCode]=splitString[4];
	});
	
	$('#descriptionForDisplay').val(JSON.stringify(descriptionTranslation));
	$('#commentsForDisplay').val(JSON.stringify(commentTranslation));
	$('#flexiDescriptionForDisplay').val(JSON.stringify(flexiDescTranslation));
	$('#flexiCommentForDisplay').val(JSON.stringify(flexiCommentTranslation));
	
	$('#frmCabinClass').ajaxSubmit({
		url: 'loadLogicalCabinClass!saveTranslations.action',
		cache: false,
		success: showResponse,  
		dataType:  'json'});
	closeDialog();
}