//On Page loading make sure that all the page input fields are cleared
var screenId = "SC_ADMN_004";
var valueSeperator = "~";

var rowNo;
var strRowData;
var isLoadCal = "NO";
var isChangedDST = "NO";
var btnSeq = "btnAdd,btnEdit,btnDelete";

function winOnLoad(strMsg, strMsgType) {
	
	clearAirportTerminal();
	disableInputControls();
	opener.setPageEdited(false, true);
	buttonSetFocus("btnAdd", btnSeq);
	if (strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType, strMsg);

	Disable("btnEdit", true);
	Disable("btnDelete", true);
	Disable("txtTerminalCode", true);
	Disable("txtTerminalName", true);
	Disable("txtTerminalDes", true);
	Disable("chkDefault", true);
	Disable("chkActive", true);
	
	
	var values = opener.getTabValues(screenId, valueSeperator,
			"airportDstCompination");
	values = values.split(",");
	var airportCode = values[0];
	setField("hdnAirportCode",airportCode);

	document.getElementById("spnTerminalHeader").innerHTML = airportCode;
			

	

	if (arrFormData != null && arrFormData.length > 0) {

		opener.setPageEdited(true, true);
		enableInputControls();
		isLoadCal = "YES";
		
		if (isAddMode) {
			setField("hdnMode", "ADD");
			
		} else {
			setField("hdnMode", "EDIT");
			// If date less or equal to current date, Disable Start date/time in
			// edit mode
			
			Disable("btnEdit", false);
			
			setField("hdnVersion", prevVersion);
			setField("hdnTerminalId", prevTerminalId);
			setField("hdnGridRow", prevGridRow);
			strGridRow = prevGridRow;
		}
	}
	displayAlertMessage();
}


function displayAlertMessage(){
	if('true' == displayAlert){
		alert("Please set One active terminal as Default");
	}
	
}
function validateAirportTerminal() {	
	//TODO add messages to properties file
	if (getFieldByID("txtTerminalName").value == "" || trim(getFieldByID("txtTerminalName").value).length == 0) {
		showWindowCommonError("Error", terminalNameRqrd);
		getFieldByID("txtTerminalName").focus();
		return;
	}
	else if (getFieldByID("txtTerminalDes").value == "" || trim(getFieldByID("txtTerminalDes").value).length == 0) {
		showWindowCommonError("Error", terminalDesRqrd);
		getFieldByID("txtTerminalDes").focus();
		return;
	}	
	else if(getFieldByID("chkDefault").checked && isDefaultTerminalExist()){
		showWindowCommonError("Error", "Default Terminal already exists");
		getFieldByID("chkDefault").focus();
		return;
	}
	else {
		if(!getFieldByID("chkActive").checked && isAllTerminalsInactive()){
	        alert("All the terminals for this Airport are Inactive");		      
        }
		return true;
	}
}

function isAllTerminalsInactive(){
	var i=0;
	for(i=0;i<arrData.length;i++){
		if(rowNo != i && arrData[i][4] == "ACT"){
			return false;
		}
	}
	return true;
}

function isDefaultTerminalExist(){
	var i=0;
	for(i=0;i<arrData.length;i++){
		if(arrData[i][3] == "Y"){
			return true;
		}
	}
	return false;
}

function clearAirportTerminal() {
	setField("txtTerminalCode", "");	
	setField("txtTerminalName", "");	
	setField("txtTerminalDes", "");	
	setField("chkDefault", false);	
	setField("chkActive", false);	
	
	setField("hdnVersion", "");	
	setField("hdnTerminalId", "");
	
	
}

function disableInputControls() {
	Disable("txtTerminalCode", true);	
	Disable("txtTerminalName", true);	
	Disable("txtTerminalDes", true);	
	Disable("chkDefault", true);	
	Disable("chkActive", true);	
	Disable("btnSave", true);
	Disable("btnReset", true);

}

function enableInputControls() {
	Disable("txtTerminalCode", false);	
	Disable("txtTerminalName", false);	
	Disable("txtTerminalDes", false);	
	Disable("chkDefault", false);	
	Disable("chkActive", false);	
	Disable("btnSave", false);
	Disable("btnReset", false);
	

}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (chkChanges()) {

		rowNo = strRowNo;
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		
		setField("txtTerminalCode", arrData[strRowNo][1]);
		setField("txtTerminalName", arrData[strRowNo][2]);		
		setField("txtTerminalDes", arrData[strRowNo][5]);
		setField("hdnVersion", arrData[strRowNo][6]);
		setField("hdnTerminalId", arrData[strRowNo][7]);
		
		if (arrData[strRowNo][3] == "Y"){
			getFieldByID("chkDefault").checked = true;
		}else{
			getFieldByID("chkDefault").checked = false;
		}
		if (arrData[strRowNo][4] == "ACT"){
			getFieldByID("chkActive").checked = true;
		}else{
			getFieldByID("chkActive").checked = false;
		}
		
		
		document.getElementById("btnSave").value = "Save";
		
		
		
		Disable("btnAdd", false);
		Disable("btnEdit", false);
		if (arrData[strRowNo][8] == "false"){
			Disable("btnDelete", false);
		}
		opener.setPageEdited(false, true);
		disableInputControls();
		
	}
}

function saveAirportTerminal() {
	if (!validateAirportTerminal())
		return;
	enableInputControls();
	
	var strAirportCode = opener.document.getElementById("txtAirportCode").value;
	
	isChangedDST = "YES";
	document.forms[0].action = "showAirportTerminal.action?txtAirportCode="
			+ strAirportCode;
	document.forms[0].submit();
	Disable("chkStatus", false);
}

function resetAirportTerminal() {
	opener.setPageEdited(false, true);
	
	var strMode = getText("hdnMode");
	document.getElementById("btnSave").value = "Save";
	
	isLoadCal = "YES";
	if (strMode == "ADD") {
		clearAirportTerminal();
		getField("selCarrierCode").focus();
		
	}
	if (strMode == "EDIT") {		
		clearAirportTerminal();
		disableInputControls();
		opener.setPageEdited(false, true);

		buttonSetFocus("btnAdd", btnSeq);

		Disable("btnEdit", true);		
	}
	Disable("btnAdd", false);
	objOnFocus();
}

function addClick() {
	if (chkChanges()) {
		enableInputControls();
		clearAirportTerminal();
		opener.setPageEdited(false, true);
		
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		Disable("btnSave", false);
		Disable("btnReset", false);
		
		Disable("txtTerminalCode", false);
		Disable("txtTerminalName", false);
		Disable("txtTerminalDes", false);
		Disable("chkDefault", false);
		Disable("chkActive", false);
		
		setField("hdnMode", "ADD");
		isLoadCal = "YES";
		setField("hdnTerminalId", 0);		
		document.getElementById("btnSave").value = "Save";
	}
	objOnFocus();
	
}
function editClick() {
	objOnFocus();
	
	//enableInputControls();

	Disable("btnSave", false);
	Disable("btnReset", false);	
	
	Disable("txtTerminalCode", false);
	Disable("txtTerminalName", false);
	Disable("txtTerminalDes", false);
	Disable("chkDefault", false);
	Disable("chkActive", false);
	
	setField("hdnMode", "EDIT");	
	document.getElementById("btnSave").value = "Save";
	
}
function deleteClick() {
	
	var confirmStr = confirm(deleteRecoredCfrm);
	if (!confirmStr)
		return;
	if (strRowData[0] == null) {
		showCommonError("Error", "Please select a row to delete!");
	} else {
		enableInputControls();

		var strAirportCode = opener.document.getElementById("txtAirportCode").value;
		setField("hdnMode", "DELETE");
		document.forms[0].target = "_self";
		
		document.forms[0].action = "showAirportTerminal.action?txtAirportCode="
				+ strAirportCode;
		
		
		document.forms[0].submit();
	}
}



function objOnFocus() {
	HidePageMessage();
}


function pageOnChange() {
	opener.setPageEdited(true, true);
}
function chkChanges() {
	// if (opener.top.pageEdited) {
	if (opener.top[1].objTMenu.tabGetPageEidted(screenId)) {
		return confirm("Changes will be lost! Do you wish to Continue?");
	} else {
		return true;
	}
}



function getValueTrimed(strValue) {
	return trim(getValue(strValue));
}



function windowclose() {
	if (opener.isAirportDstEdited) {
		// opener.closeParentAndChildWindow();
		var cfrm = confirm("Changes will be lost! Do you wish to Continue?");
		if (cfrm) {
			opener.setPageEdited(false, false);
			opener.isAirportEdited = false;
				
			window.close();
		}
	} else {		
		window.close();
	}
}

function validateDes(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtTerminalDes", strValue.substr(0, strLength - 1));
		getFieldByID("txtTerminalDes").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}