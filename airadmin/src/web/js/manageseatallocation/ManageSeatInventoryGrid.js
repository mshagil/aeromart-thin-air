//-------------------------------------------
//		Flight Search Results Grid
//-------------------------------------------
var objFltSearchCol1 = new DGColumn();
objFltSearchCol1.columnType = "label";
objFltSearchCol1.width = "10%";
objFltSearchCol1.arrayIndex = 0;
objFltSearchCol1.toolTip = "Flight No";
objFltSearchCol1.headerText = "Flight No";
objFltSearchCol1.itemAlign = "center"
objFltSearchCol1.sort = true;

var objFltSearchCol2 = new DGColumn();
objFltSearchCol2.columnType = "label";
objFltSearchCol2.width = "8%";
objFltSearchCol2.arrayIndex = 1;
objFltSearchCol2.toolTip = "Departure";
objFltSearchCol2.headerText = "Departure";
objFltSearchCol2.itemAlign = "center"

var objFltSearchCol3 = new DGColumn();
objFltSearchCol3.columnType = "label";
objFltSearchCol3.width = "20%";
objFltSearchCol3.arrayIndex = 2;
objFltSearchCol3.toolTip = "Departure Date/Time";
objFltSearchCol3.headerText = "Departure Date/Time";
objFltSearchCol3.itemAlign = "center"

var objFltSearchCol4 = new DGColumn();
objFltSearchCol4.columnType = "label";
objFltSearchCol4.width = "8%";
objFltSearchCol4.arrayIndex = 3;
objFltSearchCol4.toolTip = "Arrival";
objFltSearchCol4.headerText = "Arrival";
objFltSearchCol4.itemAlign = "center"

var objFltSearchCol5 = new DGColumn();
objFltSearchCol5.columnType = "label";
objFltSearchCol5.width = "20%";
objFltSearchCol5.arrayIndex = 4;
objFltSearchCol5.toolTip = "Arrival Date/Time";
objFltSearchCol5.headerText = "Arrival Date/Time";
objFltSearchCol5.itemAlign = "center"

var objFltSearchCol6 = new DGColumn();
objFltSearchCol6.columnType = "label";
objFltSearchCol6.width = "13%";
objFltSearchCol6.arrayIndex = 5;
objFltSearchCol6.toolTip = "via";
objFltSearchCol6.headerText = "via";
objFltSearchCol6.itemAlign = "center"

var objFltSearchCol7 = new DGColumn();
objFltSearchCol7.columnType = "label";
objFltSearchCol7.width = "10%";
objFltSearchCol7.arrayIndex = 6;
objFltSearchCol7.toolTip = "Status";
objFltSearchCol7.headerText = "Status";
objFltSearchCol7.itemAlign = "center"

var objFltSearchCol8 = new DGColumn();
objFltSearchCol8.columnType = "label";
objFltSearchCol8.width = "8%";
objFltSearchCol8.arrayIndex = 7;
objFltSearchCol8.toolTip = "Overlap";
objFltSearchCol8.headerText = "Overlap";
objFltSearchCol8.itemAlign = "center"

var objFltSearchDG = new DataGrid("spnFlightSearchResults");
objFltSearchDG.addColumn(objFltSearchCol1);
objFltSearchDG.addColumn(objFltSearchCol2);
objFltSearchDG.addColumn(objFltSearchCol3);
objFltSearchDG.addColumn(objFltSearchCol4);
objFltSearchDG.addColumn(objFltSearchCol5);
objFltSearchDG.addColumn(objFltSearchCol6);
objFltSearchDG.addColumn(objFltSearchCol7);
objFltSearchDG.addColumn(objFltSearchCol8);

objFltSearchDG.width = "600px";
objFltSearchDG.height = "100px";
objFltSearchDG.headerBold = false;
// objFltSearchDG.arrGridData = fa;
objFltSearchDG.arrGridData = new Array();
objFltSearchDG.seqNo = true;
objFltSearchDG.seqStartNo = 1;
objFltSearchDG.paging = false;
objFltSearchDG.rowSelect = true;
objFltSearchDG.rowClick = "fltSearhResultDGRowClick";

// -------------------------------------------
// Segment Inventory Grid
// -------------------------------------------
var objSegInvCol1 = new DGColumn();
objSegInvCol1.columnType = "label";
objSegInvCol1.width = "8%";
objSegInvCol1.arrayIndex = 1;
objSegInvCol1.toolTip = "Flight No";
objSegInvCol1.headerText = "Flight No";
objSegInvCol1.itemAlign = "center"

var objSegInvCol2 = new DGColumn();
objSegInvCol2.columnType = "label";
objSegInvCol2.width = "12%";
objSegInvCol2.arrayIndex = 2;
objSegInvCol2.toolTip = "Segment";
objSegInvCol2.headerText = "Segment";
objSegInvCol2.itemAlign = "Center"

var objSegInvCol3 = new DGColumn();
objSegInvCol3.columnType = "label";
objSegInvCol3.width = "8%";
objSegInvCol3.arrayIndex = 3;
objSegInvCol3.toolTip = "Allocated Adult";
objSegInvCol3.headerText = "Allocated<br>Adult";
objSegInvCol3.itemAlign = "Center"

var objSegInvCol4 = new DGColumn();
objSegInvCol4.columnType = "CUSTOM";
objSegInvCol4.width = "9%";
objSegInvCol4.arrayIndex = 4;
objSegInvCol4.toolTip = "Oversell Adult";
objSegInvCol4.headerText = "Oversell <br>Adult";
objSegInvCol4.ID = 'txtOS';
objSegInvCol4.htmlTag = "<input type='text' id='txtOS' name='txtOS' :CUSTOM: size='6' maxlength='3' onBlur='parent.blurOversellAdt(this)' onkeyUp='parent.positiveWholeNumberValidate(this)' onkeyPress='parent.positiveWholeNumberValidate(this)'>";
objSegInvCol4.onChange = "changeOversellAdt"
objSegInvCol4.itemAlign = "Center"

var objSegInvCol5 = new DGColumn();
objSegInvCol5.columnType = "CUSTOM";
objSegInvCol5.width = "8%";
objSegInvCol5.arrayIndex = 5;
objSegInvCol5.toolTip = "Curtailed Adult";
objSegInvCol5.headerText = "Curtailed<br>Adult";
objSegInvCol5.ID = 'txtCurAdult';
objSegInvCol5.htmlTag = "<input type='text' id='txtCurAdult' name='txtCurAdult' :CUSTOM: size='6' maxlength='3' onBlur='parent.blurCurtaiedAdt(this)' onkeyUp='parent.positiveWholeNumberValidate(this)' onkeyPress='parent.positiveWholeNumberValidate(this)'>";
objSegInvCol5.onChange = "changeCurtaiedAdt"
objSegInvCol5.itemAlign = "Center"

var objSegInvCol6 = new DGColumn();
objSegInvCol6.columnType = "CUSTOM";
objSegInvCol6.width = "8%";
objSegInvCol6.arrayIndex = 6;
objSegInvCol6.toolTip = "Allocated Infant";
objSegInvCol6.headerText = "Allocated<br>Infant";
objSegInvCol6.ID = 'txtAllocInfant';
objSegInvCol6.htmlTag = "<input type='text' id='txtAllocInfant' name='txtAllocInfant' :CUSTOM: size='6' maxlength='3' onBlur='parent.blurInfantAlloc(this)' onkeyUp='parent.positiveWholeNumberValidate(this)' onkeyPress='parent.positiveWholeNumberValidate(this)'>";
objSegInvCol6.onChange = "changeInfantAlloc"
objSegInvCol6.itemAlign = "Center"

var objSegInvCol7 = new DGColumn();
objSegInvCol7.columnType = "label";
objSegInvCol7.width = "10%";
objSegInvCol7.arrayIndex = 7;
objSegInvCol7.toolTip = "Sold Adult/ Infant";
objSegInvCol7.headerText = "Sold<br> Adult/ Infant";
objSegInvCol7.itemAlign = "Center"

var objSegInvCol8 = new DGColumn();
objSegInvCol8.columnType = "label";
objSegInvCol8.width = "10%";
objSegInvCol8.arrayIndex = 8;
objSegInvCol8.toolTip = "On Hold Adult/ Infant";
objSegInvCol8.headerText = "On Hold<br> Adult/ Infant";
objSegInvCol8.itemAlign = "Center"

var objSegInvCol9 = new DGColumn();
objSegInvCol9.columnType = "label";
objSegInvCol9.width = "10%";
objSegInvCol9.arrayIndex = 9;
objSegInvCol9.toolTip = "Available Adult/ Infant";
objSegInvCol9.headerText = "Available<br> Adult/ Infant";
objSegInvCol9.itemAlign = "Center"

var objSegInvCol10 = new DGColumn();
objSegInvCol10.columnType = "label";
objSegInvCol10.width = "8%";
objSegInvCol10.arrayIndex = 16;
objSegInvCol10.toolTip = "Overbook Adult";
objSegInvCol10.headerText = "Overbook<br>Adult";
objSegInvCol10.itemAlign = "Center"	
	
var objSegInvCol11 = new DGColumn();
objSegInvCol11.columnType = "label";
objSegInvCol11.width = "6%";
objSegInvCol11.arrayIndex = 15;
objSegInvCol11.toolTip = "Flight Segment Status";
objSegInvCol11.headerText = "Status";
objSegInvCol11.itemAlign = "Center"

// ---------------- Grid
var objSegInvDG = new DataGrid("spnSegmentAllocations");
objSegInvDG.addColumn(objSegInvCol1);
objSegInvDG.addColumn(objSegInvCol2);
objSegInvDG.addColumn(objSegInvCol3);
objSegInvDG.addColumn(objSegInvCol4);
objSegInvDG.addColumn(objSegInvCol5);
objSegInvDG.addColumn(objSegInvCol6);
objSegInvDG.addColumn(objSegInvCol7);
objSegInvDG.addColumn(objSegInvCol8);
objSegInvDG.addColumn(objSegInvCol9);
objSegInvDG.addColumn(objSegInvCol10);
objSegInvDG.addColumn(objSegInvCol11);

objSegInvDG.width = "100%";
objSegInvDG.height = "55px";
objSegInvDG.headerBold = false;
objSegInvDG.rowSelect = true;
objSegInvDG.setColors = "setSegInvDGRowColor";
objSegInvDG.arrGridData = sa;
objSegInvDG.rowClick = "segInvDGRowClick";
objSegInvDG.seqNo = true;

// --------------------------------------------------
// Seat Movement Infomation Grid
// --------------------------------------------------

var objSeatMvCol1 = new DGColumn();
objSeatMvCol1.columnType = "label";
objSeatMvCol1.width = "19%";
objSeatMvCol1.arrayIndex = 1;
objSeatMvCol1.toolTip = "Channel/Agent Name";
objSeatMvCol1.headerText = "Channel/Agent Name";
objSeatMvCol1.itemAlign = "center"

var objSeatMvCol2 = new DGColumn();
objSeatMvCol2.columnType = "label";
objSeatMvCol2.width = "12%";
objSeatMvCol2.arrayIndex = 2;
objSeatMvCol2.toolTip = "Sold Adult/Child/Infant  on the selected segment";
objSeatMvCol2.headerText = "Sold<br> AD/CH/INF";
objSeatMvCol2.itemAlign = "center"

var objSeatMvCol3 = new DGColumn();
objSeatMvCol3.columnType = "label";
objSeatMvCol3.width = "12%";
objSeatMvCol3.arrayIndex = 3;
objSeatMvCol3.toolTip = "Onhold Adult/Child/Infant on the selected segment";
objSeatMvCol3.headerText = "Onhold<br> AD/CH/INF";
objSeatMvCol3.itemAlign = "center"

var objSeatMvCol4 = new DGColumn();
objSeatMvCol4.columnType = "label";
objSeatMvCol4.width = "12%";
objSeatMvCol4.arrayIndex = 4;
objSeatMvCol4.toolTip = "Cancelled Adult/Child/Infant  on the selected segment";
objSeatMvCol4.headerText = "Cancelled<br> AD/CH/INF";
objSeatMvCol4.itemAlign = "center"

var objSeatMvCol5 = new DGColumn();
objSeatMvCol5.columnType = "label";
objSeatMvCol5.width = "14%";
objSeatMvCol5.arrayIndex = 5;
objSeatMvCol5.toolTip = "Total Sold Adult/Child/Infant";
objSeatMvCol5.headerText = "Total Sold<br> AD/CH/INF";
objSeatMvCol5.itemAlign = "center"

var objSeatMvCol6 = new DGColumn();
objSeatMvCol6.columnType = "label";
objSeatMvCol6.width = "14%";
objSeatMvCol6.arrayIndex = 6;
objSeatMvCol6.toolTip = "Total Onhold Adult/Child/Infant";
objSeatMvCol6.headerText = "Total Onhold<br> AD/CH/INF";
objSeatMvCol6.itemAlign = "center"

var objSeatMvCol7 = new DGColumn();
objSeatMvCol7.columnType = "label";
objSeatMvCol7.width = "14%";
objSeatMvCol7.arrayIndex = 7;
objSeatMvCol7.toolTip = "Total Cancelled Adult/Child/Infant";
objSeatMvCol7.headerText = "Total Cancelled<br> AD/CH/INF";
objSeatMvCol7.itemAlign = "center"

var objSeatMvDG = new DataGrid("spnSeatMovementsInfo");
objSeatMvDG.addColumn(objSeatMvCol1);
objSeatMvDG.addColumn(objSeatMvCol2);
objSeatMvDG.addColumn(objSeatMvCol3);
objSeatMvDG.addColumn(objSeatMvCol4);
// if (sa != null && sa.length > 1){
objSeatMvDG.addColumn(objSeatMvCol5);
objSeatMvDG.addColumn(objSeatMvCol6);
objSeatMvDG.addColumn(objSeatMvCol7);
// }

objSeatMvDG.width = "100%";
objSeatMvDG.height = "180px";
objSeatMvDG.headerBold = false;
// if (sma != null && sma.length > 0){
// objSeatMvDG.arrGridData = sma[0];
// }
// objSeatMvDG.arrGridData = new Array();
objSeatMvDG.seqNo = true;
objSeatMvDG.seqStartNo = 1;
objSeatMvDG.paging = false;

// -----------------------------------------------------
// booking code allocatins grid
// -----------------------------------------------------
var objBCInvCol1 = new DGColumn();
objBCInvCol1.columnType = "label";
objBCInvCol1.width = "5%";
objBCInvCol1.arrayIndex = 1;
objBCInvCol1.toolTip = "Booking Class";
objBCInvCol1.headerText = "BC";
objBCInvCol1.itemAlign = "center";

var objBCInvCol2 = new DGColumn();
objBCInvCol2.columnType = "label";
objBCInvCol2.width = "4%";
objBCInvCol2.arrayIndex = 2;
objBCInvCol2.toolTip = "Standard Class";
objBCInvCol2.headerText = "Std";
objBCInvCol2.itemAlign = "Center";

var objBCInvCol3 = new DGColumn();
objBCInvCol3.columnType = "label";
objBCInvCol3.width = "4%";
objBCInvCol3.arrayIndex = 3;
objBCInvCol3.toolTip = "Fixed Class";
objBCInvCol3.headerText = "Fixed";
objBCInvCol3.itemAlign = "Center";

var objBCInvCol4 = new DGColumn();
objBCInvCol4.columnType = "CHECKBOX";
objBCInvCol4.width = "5%";
objBCInvCol4.arrayIndex = 4;
objBCInvCol4.controlStatusIndex = 4;
objBCInvCol4.toolTip = "Priority";
objBCInvCol4.headerText = "Priority";
objBCInvCol4.linkOnClick = "piorityOnClick"
// objBCInvCol4.ID = 'chkPriority';
// objBCInvCol4.htmlTag = "<input type='checkbox' id='chkPriority'
// name='chkPriority'>";
objBCInvCol4.itemAlign = "Center";

var objBCInvCol5 = new DGColumn();
objBCInvCol5.columnType = "CUSTOM";
objBCInvCol5.width = "5%";
objBCInvCol5.arrayIndex = 5;
objBCInvCol5.toolTip = "Allocation";
objBCInvCol5.headerText = "Alloc";
objBCInvCol5.ID = 'txtBCAlloc';
objBCInvCol5.htmlTag = "<input type='text' id='txtBCAlloc' name='txtBCAlloc' :CUSTOM: size='3'  maxlength='3'  onBlur='parent.blurBcAlloc(this,:ROW:)' onkeyUp='parent.positiveWholeNumberValidate(this)' onkeyPress='parent.positiveWholeNumberValidate(this)'>";
objBCInvCol5.onChange = "changeBcAllocation"
objBCInvCol5.itemAlign = "Center";

var objBCInvCol6 = new DGColumn();
objBCInvCol6.columnType = "label";
objBCInvCol6.width = "4%";
objBCInvCol6.arrayIndex = 6;
objBCInvCol6.toolTip = "Sold Seats after applying nesting";
objBCInvCol6.headerText = "Sold";
objBCInvCol6.itemAlign = "Center";

var objBCInvCol7 = new DGColumn();
objBCInvCol7.columnType = "label";
objBCInvCol7.width = "4%";
objBCInvCol7.arrayIndex = 7;
objBCInvCol7.toolTip = "On Hold Seats after applying nesting";
objBCInvCol7.headerText = "Ohd";
objBCInvCol7.itemAlign = "Center";

var objBCInvCol8 = new DGColumn();
objBCInvCol8.columnType = "label";
objBCInvCol8.width = "4%";
objBCInvCol8.arrayIndex = 8;
objBCInvCol8.toolTip = "Cancelled";
objBCInvCol8.headerText = "Cnxd";
objBCInvCol8.itemAlign = "Center";

var objBCInvCol9 = new DGColumn();
objBCInvCol9.columnType = "CUSTOM";
objBCInvCol9.width = "5%";
objBCInvCol9.arrayIndex = 9;
objBCInvCol9.toolTip = "Seat Acquired from onhold release and cancellations";
objBCInvCol9.headerText = "Aquired";
objBCInvCol9.ID = 'txtBCAcquired';
objBCInvCol9.htmlTag = "<input type='text' id='txtBCAcquired' name='txtBCAcquired' :CUSTOM: size='3'  maxlength='3'  onBlur='parent.blurBcAcquired(this,:ROW:)' onkeyUp='parent.positiveWholeNumberValidate(this)' onkeyPress='parent.positiveWholeNumberValidate(this)'>";
objBCInvCol9.onChange = "changeBcAcquired"
objBCInvCol9.itemAlign = "Center";

var objBCInvCol10 = new DGColumn();
objBCInvCol10.columnType = "label";
objBCInvCol10.width = "6%";
objBCInvCol10.arrayIndex = 10;
objBCInvCol10.toolTip = "Available Seats";
objBCInvCol10.headerText = "Avail";
objBCInvCol10.itemAlign = "Center";

var objBCInvCol11 = new DGColumn();
objBCInvCol11.columnType = "CHECKBOX";
objBCInvCol11.width = "5%";
objBCInvCol11.arrayIndex = 11;
objBCInvCol11.controlStatusIndex = 11;
objBCInvCol11.toolTip = "Closed Flag";
objBCInvCol11.headerText = "Closed";
objBCInvCol11.linkOnClick = "closedOnClick"
// objBCInvCol11.ID = 'chkCloseFlag';
// objBCInvCol11.htmlTag = "<input type='checkbox' id='chkCloseFlag'
// name='chkCloseFlag'>";
objBCInvCol11.itemAlign = "Center";

var objBCInvCol12 = new DGColumn();
objBCInvCol12.columnType = "label";
objBCInvCol12.width = "15%";
objBCInvCol12.arrayIndex = 18;
objBCInvCol12.toolTip = "View Fares & Rules";
objBCInvCol12.headerText = "Fares & Rules";
objBCInvCol12.itemAlign = "Center";

var objBCInvCol13 = new DGColumn();
objBCInvCol13.columnType = "label";
objBCInvCol13.width = "5%";
objBCInvCol13.arrayIndex = 13;
objBCInvCol13.toolTip = "No. of Agents";
objBCInvCol13.headerText = "Agents";
objBCInvCol13.itemAlign = "Center";

var objBCInvCol14 = new DGColumn();
objBCInvCol14.columnType = "label";
objBCInvCol14.width = "4%";
objBCInvCol14.arrayIndex = 27;
objBCInvCol14.toolTip = "Nested Rank";
objBCInvCol14.headerText = "Rank";
objBCInvCol14.itemAlign = "Center";

var objBCInvCol15 = new DGColumn();
objBCInvCol15.columnType = "label";
objBCInvCol15.width = "4%";
objBCInvCol15.arrayIndex = 28;
objBCInvCol15.toolTip = "Sold Seats Nested In";
objBCInvCol15.headerText = "Sld I";
objBCInvCol15.itemAlign = "Center";

var objBCInvCol16 = new DGColumn();
objBCInvCol16.columnType = "label";
objBCInvCol16.width = "4%";
objBCInvCol16.arrayIndex = 29;
objBCInvCol16.toolTip = "Sold Seats Nested Out";
objBCInvCol16.headerText = "Sld O";
objBCInvCol16.itemAlign = "Center";

var objBCInvCol17 = new DGColumn();
objBCInvCol17.columnType = "label";
objBCInvCol17.width = "4%";
objBCInvCol17.arrayIndex = 30;
objBCInvCol17.toolTip = "Onhold Seats Nested in";
objBCInvCol17.headerText = "Ohd I";
objBCInvCol17.itemAlign = "Center";

var objBCInvCol18 = new DGColumn();
objBCInvCol18.columnType = "label";
objBCInvCol18.width = "4%";
objBCInvCol18.arrayIndex = 31;
objBCInvCol18.toolTip = "Onhold Seats Nested Out";
objBCInvCol18.headerText = "Ohd O";
objBCInvCol18.itemAlign = "Center";

var objBCInvCol19 = new DGColumn();
objBCInvCol19.columnType = "label";
objBCInvCol19.width = "5%";
objBCInvCol19.arrayIndex = 34;
objBCInvCol19.toolTip = "Overbook Seats";
objBCInvCol19.headerText = "OverB";
objBCInvCol19.itemAlign = "Center";

// ---------------- Grid
var objBCInvDG = new DataGrid("spnBookingCodeAllocations");
objBCInvDG.addColumn(objBCInvCol1);// bc
objBCInvDG.addColumn(objBCInvCol2);// std flag
objBCInvDG.addColumn(objBCInvCol14);// nested rank
objBCInvDG.addColumn(objBCInvCol3);// std flag
objBCInvDG.addColumn(objBCInvCol4);// priority
objBCInvDG.addColumn(objBCInvCol5);// allocation
objBCInvDG.addColumn(objBCInvCol6);// sold
objBCInvDG.addColumn(objBCInvCol7);// onhold
objBCInvDG.addColumn(objBCInvCol8);// cancelled
objBCInvDG.addColumn(objBCInvCol9);// acquired
objBCInvDG.addColumn(objBCInvCol10);// avail
objBCInvDG.addColumn(objBCInvCol19);// over sell
objBCInvDG.addColumn(objBCInvCol11);// closed flag
objBCInvDG.addColumn(objBCInvCol15);// nested in sold
objBCInvDG.addColumn(objBCInvCol16);// nested out sold
objBCInvDG.addColumn(objBCInvCol17);// nested in onhold
objBCInvDG.addColumn(objBCInvCol18);// nested out onhold
objBCInvDG.addColumn(objBCInvCol12);// fares
objBCInvDG.addColumn(objBCInvCol13);// agents

objBCInvDG.width = "99%";
objBCInvDG.height = "225px";
objBCInvDG.headerBold = false;
objBCInvDG.rowSelect = true;
objBCInvDG.rowMultiSelect = true;
objBCInvDG.controlValue = 1;
// if (ba != null && ba.length > 0){
objBCInvDG.arrGridData = ba[0];
// }
// objBCInvDG.arrGridData = new Array();
objBCInvDG.setColors = "setBCInvDGRowColor";
objBCInvDG.rowClick = "bcInvDGRowClick";
objBCInvDG.seqNo = true;
objBCInvDG.displayGrid();

function setSegInvDGRowColor(strRowData) {
	var soldAdultCount = strRowData[7].split("/");
	var onholdAdultCount = strRowData[8].split("/");
	if ((Number(soldAdultCount[0]) + Number(onholdAdultCount[0])) > 
		(Number(strRowData[3]) + Number(strRowData[4]) - Number(strRowData[5]))) {
		return 'red';
	}
	if (strRowData[14] == 1) {
		return 'green';
	}
	if (strRowData[15] == "CLS") {
		return 'gray';
	}
}

function setBCInvDGRowColor(strRowData) {
	if (strRowData[11] == true && strRowData[15] == 'M') {// manually closed
		return 'blue';
	} else if (strRowData[11] != true && strRowData[15] == 'M') {// manually
																	// open
		return '#FF6666';
		//return '#FFAB1A';
	} else if (strRowData[11] == true) {//system closed
		return '#CC00FF';
	} else if (strRowData[11] != true && strRowData[15] == 'R') {//system opened
		return '#A52A2A';
	}
}

//check/uncheck priority/closed all
function allPriority_Click(){
	var isChecked = document.getElementById('checkAllPriority').checked;
	var priorities = new Array();
	
	priorities = document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementsByName('CHK_spnBookingCodeAllocations_0_4');
	
	if(isChecked){		
		for(var i=0;i < priorities.length;++i){			
			var priorityID = priorities[i].id;			
			document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(priorityID).checked = true;
			document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(priorityID).value = "true";
			gridChkBoxClick(priorities[i],i,'4','spnBookingCodeAllocations','piorityOnClick');
		}
	}else{		
		for(var i=0;i < priorities.length;++i){			
			var priorityID = priorities[i].id;			
			document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(priorityID).checked = false;
			document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(priorityID).value = "false";
			gridChkBoxClick(priorities[i],i,'4','spnBookingCodeAllocations','piorityOnClick');
		}
	}
}

var tmpBa = null;

function cloneObj(obj){
	if(obj == null){
		return null;
	}
	var clone = (obj instanceof Array) ? [] : {};
    for(var i in obj) {
        if(typeof(obj[i])=="object")
            clone[i] = cloneObj(obj[i]);
        else
            clone[i] = obj[i];
    }
    return clone;
}

function allClosed_Click(){
	var isChecked = document.getElementById('checkAllClosed').checked;
	var closedItems = new Array();
	
	if(tmpBa == null){
		tmpBa = cloneObj(ba);		
	}
	
	closedItems = document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementsByName('CHK_spnBookingCodeAllocations_0_12');
	
	ba = cloneObj(tmpBa);
	objBCInvDG.arrGridData = ba[0];
	if(isChecked){		
		for(var i=0;i < closedItems.length;++i){
			//need to consider the system closed
			//need to consider the manually close
			var isManuallyClosed = (tmpBa[0][i][11] == true && tmpBa[0][i][15] == 'M');
			var isSystemClosed = (!isManuallyClosed && tmpBa[0][i][11] == true);
			var closedItemID = closedItems[i].id;
			if(!isManuallyClosed && !isSystemClosed){							
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).checked = true;
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).value = "true";
				gridChkBoxClick(closedItems[i],i,'12','spnBookingCodeAllocations','closedOnClick');
			}else{
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).checked = true;
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).value = "true";
			}			
		}
	}else{		
		for(var i=0;i < closedItems.length;++i){
			//need to consider the system open
			//need to consider the manually open
			var isManuallyOpen = ( tmpBa[0][i][11] != true && tmpBa[0][i][15] == 'M' );
			var isSystemOpen = (!isManuallyOpen && (tmpBa[0][i][11] != true && tmpBa[0][i][15] == 'R'));
			var closedItemID = closedItems[i].id;
			if(!isManuallyOpen && !isSystemOpen){							
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).checked = false;
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).value = "false";
				gridChkBoxClick(closedItems[i],i,'12','spnBookingCodeAllocations','closedOnClick');
			}else{
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).checked = false;
				document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).value = "false";
			}
		}
	}
	
}

function setPriorityAndClosedAll(){
	var closedItems = document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementsByName('CHK_spnBookingCodeAllocations_0_12');
	var priorities = document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementsByName('CHK_spnBookingCodeAllocations_0_4');
	
	var isAllCheckedPriority = true;
	var isAllCkeckedClosed = true;
	
	for(var i=0;i<closedItems.length;++i){
		var closedItemID = closedItems[i].id
		var checked = document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(closedItemID).checked;
		if(!checked){
			isAllCkeckedClosed = false;
			break;
		}
	}
	
	for(var i=0;i< priorities.length ; ++i ){
		var priorityID = priorities[i].id;
		var checked = document.getElementById('frm_spnBookingCodeAllocations').contentDocument.getElementById(priorityID).checked;
		if(!checked){
			isAllCheckedPriority = false;
			break;
		}
	}
	
	if(isAllCheckedPriority){
		document.getElementById('checkAllPriority').checked = true;
	}else{
		document.getElementById('checkAllPriority').checked = false;
	}
	
	if(isAllCkeckedClosed){
		document.getElementById('checkAllClosed').checked = true;
	}else{
		document.getElementById('checkAllClosed').checked = false;
	}
	document.getElementById('checkAllClosed').disabled = true;
	document.getElementById('checkAllPriority').disabled = true;
}


//-------------------------------------------
//Roll Forward Audit Grid
//-------------------------------------------
var objRollFWDAuditCol1 = new DGColumn();
objRollFWDAuditCol1.columnType = "label";
objRollFWDAuditCol1.width = "12%";
objRollFWDAuditCol1.arrayIndex = 1;
objRollFWDAuditCol1.toolTip = "Flight No";
objRollFWDAuditCol1.headerText = "Flight No";
objRollFWDAuditCol1.itemAlign = "center";
objRollFWDAuditCol1.sort = true;

var objRollFWDAuditCol2 = new DGColumn();
objRollFWDAuditCol2.columnType = "label";
objRollFWDAuditCol2.width = "10%";
objRollFWDAuditCol2.arrayIndex = 3;
objRollFWDAuditCol2.toolTip = "Departure";
objRollFWDAuditCol2.headerText = "Departure";
objRollFWDAuditCol2.itemAlign = "center";

var objRollFWDAuditCol3 = new DGColumn();
objRollFWDAuditCol3.columnType = "label";
objRollFWDAuditCol3.width = "28%";
objRollFWDAuditCol3.arrayIndex = 2;
objRollFWDAuditCol3.toolTip = "Departure Date/Time";
objRollFWDAuditCol3.headerText = "Departure Date/Time";
objRollFWDAuditCol3.itemAlign = "center";

var objRollFWDAuditCol4 = new DGColumn();
objRollFWDAuditCol4.columnType = "label";
objRollFWDAuditCol4.width = "50%";
objRollFWDAuditCol4.arrayIndex = 4;
objRollFWDAuditCol4.toolTip = "Roll Forward Audit Notes";
objRollFWDAuditCol4.headerText = "Roll Forward Audit Notes";
objRollFWDAuditCol4.itemAlign = "left"

//Constract Grid
var objRollFWDAuditDG = new DataGrid("spnRollForwardAudits");
objRollFWDAuditDG.addColumn(objRollFWDAuditCol1);
objRollFWDAuditDG.addColumn(objRollFWDAuditCol2);
objRollFWDAuditDG.addColumn(objRollFWDAuditCol3);
objRollFWDAuditDG.addColumn(objRollFWDAuditCol4);

objRollFWDAuditDG.width = "600px";
objRollFWDAuditDG.height = "100px";
objRollFWDAuditDG.headerBold = false;
// objFltSearchDG.arrGridData = fa;
objRollFWDAuditDG.arrGridData = new Array();
objRollFWDAuditDG.seqNo = true;
objRollFWDAuditDG.seqStartNo = 1;
objRollFWDAuditDG.paging = false;
objRollFWDAuditDG.rowSelect = true;
objRollFWDAuditDG.rowClick = "rollFWDAuditRowClick";
objRollFWDAuditDG.displayGrid();
setVisible("spnRollForwardAudits", false);
setTimeout('setPriorityAndClosedAll()',1500);