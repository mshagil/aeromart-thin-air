var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "35%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Segment";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left"

var objCol2 = new DGColumn();
objCol2.columnType = "CUSTOM";
objCol2.width = "45%";
objCol2.arrayIndex = 3;
objCol2.toolTip = "Meal Template";
objCol2.headerText = "Meal Template <font class='mandatory'>*<\/font>";
objCol2.ID = 'selTemplate';
objCol2.htmlTag = "<select  id='selTemplate' name='selTemplate' style='width:200px' :CUSTOM:><option value=''></option>"
		+ strOpt + "</select>";
objCol2.onChange = "changetemplate";
objCol2.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnMeals");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.width = "99%";
objDG.height = "90px";
objDG.headerBold = false;
objDG.arrGridData = arrMealSegData;
objDG.rowOver = false;
objDG.seqNo = false;
objDG.backGroundColor = "#ECECEC";
objDG.displayGrid();

var blnPageedit = false;
var arrFlt = opener.sa;
var cabinCode = "";
var intRowNo = 0;

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function rollClick() {
	var blnFound = false;
	var cnf = false;
	for ( var sl = 0; sl < arrMealSegData.length; sl++) {

		if (arrMealSegData[sl][3] != "") {
			blnFound = true;
			break;
		}

	}
	if (!blnFound) {
		cnf = confirm("This will Remove all the Templates from selected Flights. Do you wish to Continue ?");

	}
	if (blnFound || cnf) {
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 620) / 2;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=450,resizable=no,top='
				+ intTop + ',left=' + intLeft;
		if ((opener.top[0].objMealRoll) && (!opener.top[0].objMealRoll.closed)) {
			opener.top[0].objMealRoll.close();
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		} else {
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		}
		var objMealAllocationForm = document.getElementById("frmMealAlloc");
		objMealAllocationForm.target = "CMealWin";

		objMealAllocationForm.action = "showMealRoll.action?fltId="
				+ strFlightId+ "&rollType=UPDATE&cos=" + strClassOfService;
		objMealAllocationForm.submit();
	}

}

function removeTemplateRollForward(){
	var cnf = false;
	
	cnf = confirm("This will Remove all the Templates from selected Flights Schedules. Do you wish to Continue ?");
	
	if(cnf){
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 620) / 2;
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=450,resizable=no,top='
					  + intTop + ',left=' + intLeft;
		
		if((opener.top[0].objMealRoll) && (!opener.top[0].objMealRoll.closed)){
			opener.top[0].objMealRoll.close();
			opener.top[0].objMealRoll = window.open("about:blank", "RMealRFWin", strProp);
		} else {
			opener.top[0].objMealRoll = window.open("about:blank", "RMealRFWin", strProp);
		}
		
		var objMealRmvRollForwardForm = document.getElementById("frmMealAlloc");
		objMealRmvRollForwardForm.target = "RMealRFWin";
		
		objMealRmvRollForwardForm.action = "showMealRoll.action?fltId="+ strFlightId+ "&rollType=REMOVE&cos=" + strClassOfService;
		objMealRmvRollForwardForm.submit();
		
	}
	
}

function showEdit(segId, templateId, flightId) {

	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;
	var templName = "";
	var status = "";

	for ( var xl = 0; xl < arrTemp.length; xl++) {
		if (arrTemp[xl][0] == templateId) {
			templName = arrTemp[xl][1];
			status = arrTemp[xl][2];
			break;
		}
	}

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=550,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if (trim(templName) != "") {
		if ((opener.top[0].objMealRoll) && (!opener.top[0].objMealRoll.closed)) {
			opener.top[0].objMealRoll.close();
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		} else {
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		}
		var objMealAllocationForm = document.getElementById("frmMealAlloc");
		objMealAllocationForm.target = "CMealWin";

		objMealAllocationForm.action = "loadMealSegments.action?hdnMode=EDIT&segId="
				+ segId
				+ "&templateId="
				+ templateId
				+ "&flightId="
				+ flightId
				+ "&templateName=" + templName;
		objMealAllocationForm.submit();
	}

}

function SaveClick() {
	
	var status = "";

	//Validate
	if(!isChangingToANewTemplate()) {
		showpopMssage('This is the existing template','ERROR');
		return;
	}
	
	if(!isChangingToANewTemplateFromEmptyTemplate()) {
		showpopMssage('Please assign empty template before assigning a new tempalate','ERROR');
		return;
	}
		
	var objMealForm = document.getElementById("frmMealAlloc");
	objMealForm.target = "CWindow";
	objMealForm.action = "loadMealSegments.action?hdnMode=SAVE&hdnData="
			+ arrMealSegData + "&fltId=" + strFlightId + "&modelNo="
			+ opener.strModelNo + "&ccCode=" + cabinCode + "&cos=" + strClassOfService;
	objMealForm.submit();
	ShowPopProgress();
	
}

function changetemplate(obj, rowNo, value) {	
	setPageEdited(true);
	/*var templateId = arrMealSegData[rowNo][3];
	for ( var i = 0; i < arrTemp.length; i++) {		
		if (templateId == arrTemp[i][0]) {
			cabinCode = arrTemp[i][3];			
		}
	}*/
	cabinCode = arrMealSegData[rowNo][4];
	disableControls(false);	
	disableRmvRollForward();

}

function showpopMssage(msg, msgType) {
	objMsg.MessageText = msg;
	objMsg.MessageType = msgType;
	ShowPageMessage();
}

function disableControls(cond) {
	Disable("btnSave", cond);
	Disable("btnRollForward", !cond);
}

function checkBnkTemplate(){	
	var isBlank = true;
	for(var i=0; i<arrMealSegData.length; i++){
		if(arrMealSegData[i][3] != ''){
			isBlank = false;
			break;
		}
	}	
	return isBlank;
}


function isChangingToANewTemplateFromEmptyTemplate() {
	var changeToNewTemplateFromEmpty = true;
	for ( var i = 0; i < arrMealSegData.length; i++) {
		if (originalFlightMeals[i][3] != '' && arrMealSegData[i][3] != '') {
			changeToNewTemplateFromEmpty = false;
			break;
		}
	}

	return changeToNewTemplateFromEmpty;
}

function isChangingToANewTemplate() {
	var changeToNewTemplate = true;
	for ( var i = 0; i < arrMealSegData.length; i++) {
		if (originalFlightMeals[i][3] == arrMealSegData[i][3]) {
			changeToNewTemplate = false;
			break;
		}
	}

	return changeToNewTemplate;
}

function disableRmvRollForward(){
	var templatExist = checkBnkTemplate();	
	if(document.getElementById('btnSave').disabled && 
			document.getElementById('btnRollForward').disabled && templatExist) {
		Disable("btnRmvRollForward", false);	
	} else{
		Disable("btnRmvRollForward", true);
	}
}


function winOnLoad(message, msgType) {
	disableControls(true);	
	//Disable("btnRmvRollForward", (blnIsSced));
	if (message != "" && msgType != "") {
		showpopMssage(message, msgType);
	}
	if (trim(errString) != "") {
		var errFormArray = new Array();
		var arrErrStr = errString.split(",");
		var noofsegs = arrErrStr.length / 5;
		var erIndex = 0;
		for ( var el = 0; el < noofsegs; el++) {
			erIndex = 5 * el;
			errFormArray[el] = new Array();
			errFormArray[el][0] = arrErrStr[erIndex + 0];
			errFormArray[el][1] = arrErrStr[erIndex + 1];
			errFormArray[el][2] = arrErrStr[erIndex + 2];
			errFormArray[el][3] = arrErrStr[erIndex + 3];
			errFormArray[el][4] = arrErrStr[erIndex + 4];
		}
		arrayClone(errFormArray, arrMealSegData);
		objDG.arrGridData = arrMealSegData;
		objDG.refresh();
		disableControls(false);
	}
	
	arrayClone(arrMealSegData,originalFlightMeals);
	
	//Existing flight details
	if (getFieldByID("btnRollForward"))
		Disable("btnRollForward", !(blnIsSced));
	
	disableRmvRollForward();
	
	if ((message == trim("Record Successfully updated"))
			|| (message == trim("Save/Update operation is successful"))) {
		alert("Record Successfully Saved!");
	}
	HidePopProgress();
}

function ShowPopProgress() {
	setVisible("divLoadMsg", true);
}

function HidePopProgress() {
	setVisible("divLoadMsg", false);
}