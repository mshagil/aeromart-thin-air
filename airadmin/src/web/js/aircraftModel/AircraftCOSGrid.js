var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "18%";
objCol1.arrayIndex = 2;
objCol1.toolTip = "Class of Service";
objCol1.headerText = "Class of Service";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "42%";
objCol2.arrayIndex = 3;
objCol2.toolTip = "Description";
objCol2.headerText = "Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "custom";
objCol3.width = "20%";
objCol3.arrayIndex = 0;
objCol3.ID = "txtAdultCapacity";
objCol3.htmlTag = "<input :CUSTOM: type='text' id='txtAdultCapacity' name='txtAdultCapacity' style='width:40px;' maxlength='3' onkeyUp='parent.capacityValidate(this)' onkeyPress='parent.capacityValidate(this)' onblur='parent.flightValidate(this)'>";
objCol3.toolTip = "Adult Capacity";
objCol3.headerText = "Adult Capacity";
objCol3.itemAlign = "center"

var objCol4 = new DGColumn();
objCol4.columnType = "custom";
objCol4.width = "17%";
objCol4.arrayIndex = 1;
objCol4.ID = "txtInfantCapacity";
objCol4.htmlTag = "<input :CUSTOM: type='text' id='txtInfantCapacity' name='txtInfantCapacity' style='width:40px;' maxlength='3' onkeyUp='parent.capacityValidate(this)' onkeyPress='parent.capacityValidate(this)' onblur='parent.flightValidate(this)'>";
objCol4.toolTip = "Infant Capacity";
objCol4.headerText = "Infant Capacity";
objCol4.itemAlign = "center"

// ---------------- Grid
var objDG2 = new DataGrid("spnCOSs");
objDG2.addColumn(objCol1);
objDG2.addColumn(objCol2);
objDG2.addColumn(objCol3);
objDG2.addColumn(objCol4);

objDG2.align = "left"
objDG2.width = "76%";
objDG2.height = "125px";
objDG2.headerBold = false;
objDG2.rowSelect = true;
objDG2.arrGridData = arrCOSData;
objDG2.seqNo = false;
objDG2.paging = false;
objDG2.rowClick = "rowCOSClick";
objDG2.displayGrid();