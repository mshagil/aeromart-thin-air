jQuery(document).ready(function(){ 		
			
			$("#divSearch").decoratePanel("Search HOT Files");
			$("#txtUpDateFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date from'});
			$("#txtUpDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date to'});
			$("#txtDpcDateFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date from'});
			$("#txtDpcDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date to'});
			$("#txtProcDateFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date from'});
			$("#txtProcDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date to'});	
			
			
			$("#txtUpDateFrom").blur(function() { dateOnBlur({field: 'txtUpDateFrom'});});
			$("#txtUpDateTo").blur(function() { dateOnBlur({field: 'txtUpDateTo'});});
			$("#txtDpcDateFrom").blur(function() { dateOnBlur({field: 'txtDpcDateFrom'});});
			$("#txtDpcDateTo").blur(function() { dateOnBlur({field: 'txtDpcDateTo'});});
			$("#txtProcDateFrom").blur(function() { dateOnBlur({field: 'txtProcDateFrom'});});
			$("#txtProcDateTo").blur(function() { dateOnBlur({field: 'txtProcDateTo'});});
			
			
			$("#divResultsPanel").decoratePanel("HOT Files");
			$("#divDispBSP").decoratePanel("Upload/Process HOT File");			
			$('#btnClose').decorateButton();
			$('#btnUploadNew').decorateButton();
			$('#btnUpload').decorateButton();
			$('#btnProcess').decorateButton();
			$('#btnDelete').decorateButton();	
			$('#btnUploadNProcess').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSearch').decorateButton();
			disableUploadSection();
			disableGridButtons();
			searchData();
	
			jQuery("#listBSPHOTFiles").jqGrid({ 
				url:'showBSPHOTFileManagement!searchHOTFiles.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Log ID','File Name', 'Status','Uploaded By', 'Uploaded On','Processed By','Processed On','DPC Processed On'], 
				colModel:[ 	{name:'Id', width:30, jsonmap:'id'},   
						 	{name:'logID',index:'logID', width:70, jsonmap:'hotFile.logID'}, 
						   	{name:'fileName', width:200,  jsonmap:'hotFile.fileName'}, 
						   	{name:'fileStatus', width:70, align:"center", jsonmap:'hotFile.fileStatus'},
						   	{name:'uploadedBy', width:100, align:"center",  jsonmap:'hotFile.uploadedBy'},
						   	{name:'uploadedOn', width:120, align:"center",  jsonmap:'hotFile.uploadedOn',formatter:'date',formatoptions:{srcformat: 'Y-m-dTH:i:s',newformat: 'Y-m-d H:i:s'}},
							{name:'processedBy', width:100, align:"right",  jsonmap:'hotFile.processedBy'},
							{name:'processedOn', width:120, align:"center",  jsonmap:'hotFile.processedOn',formatter:'date',formatoptions:{srcformat: 'Y-m-dTH:i:s',newformat: 'Y-m-d H:i:s'}},
							{name:'dpcProcessedDate', width:120, align:"center",  jsonmap:'hotFile.dpcProcessedDate',formatter:'date',formatoptions:{srcformat: 'Y-m-dTH:i:s',newformat: 'Y-m-d H:i:s'} },						   							   	
						   
						  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#bspHOTFilesPager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				onSelectRow: function(rowid){
						$("#rowNo").val(rowid);
						disableUploadSection();
						$('#hdnHOTFileLogId').val(jQuery("#listBSPHOTFiles").getCell(rowid,'logID'));
						$('#hdnHOTFileName').val(jQuery("#listBSPHOTFiles").getCell(rowid,'fileName'));
						var status = jQuery("#listBSPHOTFiles").getCell(rowid,'fileStatus');
						if (status == "UPLOADED"){
							$('#btnProcess').enableButton();
							$('#btnDelete').enableButton();		
						} else if(status == "PROCESSED"){
							$('#btnProcess').disableButton();
							$('#btnDelete').disableButton();	
						}

			}}).navGrid("#bspHOTFilesPager",{refresh: true, edit: false, add: false, del: false, search: false});
			
			
			$('#btnUpload').click(function() {
				disableGridButtons();
				$("#listBSPHOTFiles").resetSelection();				 
				$('#divDispBSP').find("*").prop("disabled", false);
			});

			$('#btnProcess').click(function() {
				action("PROCESS");
			});

			$('#btnDelete').click(function() {
				action("DELETE");
			});
			
			$('#btnUploadNew').click(function() {
				uploadFile('hotFileUploadNew');				
			});
			
			$('#btnUploadNProcess').click(function() {				
				uploadFile('hotFileUploadNProcess');
			});
			
			$('#btnReset').click(function() {
				$('#fileHOTFile').val("");
			});
			 			
			$('#btnSearch').click(function() {
				disableGridButtons();
				searchData();			
			});

			function action(mode) {
				disableUploadSection();
				top[2].ShowProgress();
				var data = {};
				data['hdnHOTFileLogId'] = $("#hdnHOTFileLogId").val();
				data['hdnHOTFileName'] = $("#hdnHOTFileName").val();
				data['hdnMode'] = mode;
				var url = "showBSPHOTFileManagement.action";
				$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
					success: actionSuccess, error:actionFailed, cache : false});

				return false;	
			} 
			
			function uploadFile(mode) {
				setFileName();
				disableGridButtons();			
				var upOptionsHOTFile = {				 
						beforeSubmit:  validateFile, 
						success: showUploadHOTFile,				 
						url: 'showUpload.action?mode='+mode+'&fileName='+$("#hdnHOTFileName").val(),
						dataType:  'script' 		
					 };
					$('#frmBSPHOTFiles').ajaxSubmitWithFileInputs(upOptionsHOTFile);

			  return false;	
			} 

			function setFileName() {
				var fullFilePath = $("#fileHOTFile").attr('value');
				var fileName = fullFilePath.split(/(\\|\/)/g).pop();
				$("#hdnHOTFileName").val(fileName);
			}

			function disableGridButtons(){
			     $('#btnProcess').disableButton();
			     $('#btnDelete').disableButton();
			}

			function validateFile(formData, jqForm, options) {

				if($("#hdnHOTFileName").val() == '') {
					showCommonError('ERROR', 'Please select a file to upload');
					return false;	
				}
				
				var regex = RegExp('^[A-Z]{2}.HOT.[A-Z]{1}.[0-9]{6}.'+bspAirlineCode+'$');

				if(!regex.test($("#hdnHOTFileName").val())) {
					showCommonError('ERROR', 'File name or format invalid');
					return false;
				}     
				top[2].ShowProgress();
			} 

			function showUploadHOTFile(responseText, statusText)  {
				top[2].HideProgress();
				$("#hdnHOTFileName").val('');
				eval(responseText);
				searchData();
				disableGridButtons();
				showCommonError(responseText['msgType'], responseText['succesMsg']);
			} 
			
			function searchData(){
				
				var newUrl = 'showBSPHOTFileManagement!searchHOTFiles.action?';
			 	newUrl += 'fileName=' + $("#txtFileName").val();
			 	newUrl += '&status=' + $("#selStatus").val();
			 	newUrl += '&uploadedDateFrom=' + $("#txtUpDateFrom").val();
			 	newUrl += '&uploadedDateTo=' + $("#txtUpDateTo").val();
			 	newUrl += '&processedDateFrom=' + $("#txtProcDateFrom").val();
			 	newUrl += '&processedDateTo=' + $("#txtProcDateTo").val();
			 	newUrl += '&dpcProcessedDateFrom=' + $("#txtDpcDateFrom").val();
			 	newUrl += '&dpcProcessedDateTo=' + $("#txtDpcDateTo").val();
			 	newUrl += '&uploadedBy=' + $("#txtUploadedBy").val();
			 	newUrl += '&processedBy=' + $("#txtProcessedBy").val();
			 	
			 	$("#listBSPHOTFiles").clearGridData();
			 	$.getJSON(newUrl, function(response){
					$("#listBSPHOTFiles")[0].addJSONData(response);
				
			 	});	
			}
			
			function actionSuccess(response){
				top[2].HideProgress();
				showCommonError(response.msgType, response.succesMsg);
				searchData();
				disableGridButtons();
			}
			
			function actionFailed(response){
				top[2].HideProgress();
				disableGridButtons();
				alert (response);
			}
			
			function disableUploadSection(){
				$("#fileHOTFile").val('');
				$('#divDispBSP').find("*").prop("disabled", true);
				$('#divDispBSP').find("#btnClose").prop("disabled", false);
			}
		});	

		function dateOnBlur(inParam){
				dateChk(inParam.field);
		}