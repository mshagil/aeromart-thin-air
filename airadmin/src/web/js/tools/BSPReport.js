var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();
var begin1 = [["ALL", "ALL"]];

jQuery(document).ready(function(){  //the page is ready				
			
			$("#divSearch").decoratePanel("Search BSP Reports");
			$("#divResultsPanel").decoratePanel("BSP Reports");
			$("#divDispBSP").decoratePanel("Add/Modify BSP Reports");			
			$('#btnClose').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnResubmit').decorateButton();
			$('#btnSearch').decorateButton();
			disableBSPButton();
			createPopUps();
			var grdArray = new Array();	
			jQuery("#listBSPReport").jqGrid({ 
				url:'showBSPReport!searchBSPReport.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Log ID','Agent Code', 'TNX ID', 'LCC Unique ID', 'Description', 'Reporting User ID', 'Status','Pax Sequence','Created Date','Modified By','Modified Date','Country Code','version'], 
				colModel:[ 	{name:'Id', width:40, jsonmap:'id'},   
						 	{name:'bspTxnLogID',index:'bspTxnLogID', width:180, jsonmap:'bspreport.bspTxnLogID'}, 
						   	{name:'agentCode',index:'agentCode', width:125,  jsonmap:'bspreport.agentCode'}, 
						   	{name:'tnxID', width:90, align:"center", jsonmap:'bspreport.tnxID'},
						   	{name:'lccUniqueID', width:125, align:"center",  jsonmap:'bspreport.lccUniqueID'},
						   	{name:'description', width:360, align:"center",  jsonmap:'bspreport.description'},
							{name:'reportingUserID', width:90, align:"right",  jsonmap:'bspreport.reportingUserID'},
							{name:'status', width:125, align:"center",  jsonmap:'bspreport.status'},
							{name:'paxSequence', width:225, align:"center",  jsonmap:'bspreport.paxSequence'},						   							   	
						   	{name:'creatingDate', width:225, align:"center",  jsonmap:'bspreport.creatingDate'},
						   	{name:'modifiedBy', width:225, align:"center",  jsonmap:'bspreport.modifiedBy'},
						   	{name:'modifiedDate', width:225, align:"center",  jsonmap:'bspreport.modifiedDate'},
						   	{name:'countryCode', width:225, align:"center",  jsonmap:'bspreport.countryCode'},
						   	{name:'version', width:225, align:"center",  jsonmap:'bspreport.version'},
						   	
						   
						  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#bspreportpager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				onSelectRow: function(rowid){
						$("#rowNo").val(rowid);
						$('#dvUpLoad').html($('#dvUpLoad').html());
						$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
						fillForm(rowid);

						enableSearch();
						disableBSPButton();
						var dbStatusID = jQuery("#listBSPReport").getCell(rowid,'status');
						if (dbStatusID == "ERROR"){
							enableGridButtons();
						} else {
							disableGridButtons();
						}

			}}).navGrid("#bspreportpager",{refresh: true, edit: false, add: false, del: false, search: false});
			
			
			function showDelete(formData, jqForm, options) {				
			    return confirm(deleteRecoredCfrm); 
			} 
			$('#btnResubmit').click(function() {

				setField("hdnMode","UPDATE");
				var data = {};
				data['bspTxnLogID'] = $("#bspTxnLogID").val();
				data['hdnMode'] = "UPDATE";
				data['hdnSelBSPEditStatus']= "RESUBMIT";
				var url = "showBSPReport.action";
				$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
					success: saveBSPSuccess, error:saveBSPError, cache : false});

				return false;	
			});
			 
			$('#btnEdit').click(function() {				

				$("#btnResubmit").enableButton();
				setField("hdnMode","EDIT");
			}); 
			
			$('#btnSearch').click(function() {
				searchData();			
			});
			
			function searchData(){
				var agentCodeVal = $("#selAgentCode").val();
				if (agentCodeVal == "ALL" || agentCodeVal == "") {
					agentCodeVal = "";
				}
				var newUrl = 'showBSPReport!searchBSPReport.action?';
			 	newUrl += 'bspReportSrch.countryName=' + $("#selCountry").val();
			 	newUrl += '&bspReportSrch.agentCode=' + agentCodeVal;
			 	newUrl += '&bspReportSrch.status=' + $("#selStatus").val();
			 	newUrl += '&bspReportSrch.frmDate=' + $("#txtDateFrom").val();
			 	newUrl += '&bspReportSrch.toDate=' + $("#txtDateTo").val();
			 	
			 	$("#listBSPReport").clearGridData();
			 	$.getJSON(newUrl, function(response){
					$("#listBSPReport")[0].addJSONData(response);
				
			 	});	
			}
			
			function saveBSPSuccess(response){
				if (response.success == true){
					if (response.hdnMode == "UPDATE"){
						searchData();
						//jQuery("#listBSPReport").trigger("reloadGrid");
						disableBSPButton();
						$('#frmBSPReport').resetForm();
						alert( "Record saved successfully!!!");
					}
				} else {
					alert( response.txtMsg);
				}
			}
			
			function saveBSPError(response){
				alert (response);
			}

			function fillForm(rowid) {
				setField("hdnMode","");
				jQuery("#listBSPReport").GridToForm(rowid, '#frmBSPReport');					
				var dbStatusID = jQuery("#listBSPReport").getCell(rowid,'status');

				if (dbStatusID == "ERROR"){
					enableGridButtons();
				} else {
					disableGridButtons();
				}
				
			}

			
			function enableSearch(){
				$('#selCountry').attr("disabled", false); 
				if($("#selCountry").val()=="" || $("#selCountry").val()==null){
					$("#selAgentCode").html('<option value="ALL">ALL</option>');
					$('#selAgentCode').attr("disabled", true);
				} else {
					$('#selAgentCode').attr("disabled", false);
				}
				$('#selStatus').attr("disabled", false);
				$('#selCabinClass').attr("disabled", false); 
				$('#btnSearch').enableButton();
			}
			
			
			function disableBSPButton(){
				$("#btnEdit").disableButton();
				$("#btnResubmit").disableButton();
			}

			function enableGridButtons(){
				$("#btnEdit").enableButton();

			}
			
			function disableGridButtons(){
				$("#btnEdit").disableButton();
			}
			
			$("#btnAdd").enableButton();
			$('#selCountry').attr("disabled", false); 
			$('#selCabinClass').attr("disabled", false); 
			$('#selIATACode').attr("disabled", false); 
			if($("#selCountry").val()=="" || $("#selCountry").val()==null){
				$("#selAgentCode").html('<option value="ALL">ALL</option>');
				$('#selAgentCode').attr("disabled", true);
			} else {
				$('#selAgentCode').attr("disabled", false);
			}
			$('#selStatus').attr("disabled", false); 
			$('#btnSearch').enableButton();
			
			

		
		
			
		});	

		function filterBSPAgents(){
			var tempAgentArray = new Array();
			var agentCount = 0;
			var strBSPCountryCode;
			strBSPCountryCode = $("#selCountry").val();
			
			if(strBSPCountryCode != "ALL" && strBSPCountryCode !=""){
				var tempFilteredArray=new Array();
				tempFilteredArray=begin1;
				for (var k = 0; k < arrAgents.length; k++){
					if (strBSPCountryCode == arrAgents[k][1]){
						tempAgentArray[agentCount] = arrAgents [k];
						agentCount++;
					}
				}
				$("#selAgentCode").html('<option value="ALL">ALL</option>');
				$("#selAgentCode").fillDropDown( { dataArray:tempAgentArray , keyIndex:0 , valueIndex:0, firstEmpty:false });
				$('#selAgentCode').attr("disabled", false);
			}
			else {
				$("#selAgentCode").html('<option value="ALL">ALL</option>');
				$("#selAgentCode").fillDropDown( { dataArray:begin1, keyIndex:0 , valueIndex:0, firstEmpty:false });
				$('#selAgentCode').attr("disabled", true);
			}
		}

		
		function LoadCalendar(strID, objEvent) {
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		
		function setDate(strDate, strID) {
			switch (strID) {
			case "0":
				setField("txtDateFrom", strDate);
				break;
			case "1":
				setField("txtDateTo", strDate);
				break;
			case "3":
				setDate(strDate, strID);
				break;
			case "4":
				setDate(strDate, strID);
				break;
			}
		}
		

