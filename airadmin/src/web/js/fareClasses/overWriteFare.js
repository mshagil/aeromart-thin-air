overWriteFareRule = {};
overWriteFareRule.rowEditFlag = false;
overWriteFareRule.selRowID = 0;
overWriteFareRule.actionMode = "";
overWriteFareRule.isLocalCurrency = false;
var erroImgPath = "";
overWriteFareRule.ready = function(){
	$("#fareRuleCode").text( ofrcode );
	$("#btnSearch").click(function(){
		MessageResetForStatusBar();
		overWriteFareRule.search();
	});
	$("#txtChargeAmount").numeric({allowDecimal:true});
	$("#txtChargeAmountInLocal").numeric({allowDecimal:true});
	$("#txtMinPChargeAmount").numeric({allowDecimal:true});
	$("#txtMaxPChargeAmount").numeric({allowDecimal:true});
	$("#txtTimeValue").numeric({allowDecimal:false});
	overWriteFareRule.fillDropDownBoxes();
	$("#btnAdd").click(function(){
		overWriteFareRule.addNewOFR();
	});
	$("#btnEdit").click(function(){
		overWriteFareRule.editOFR();
	});
	$("#btnDelete").click(function(){
		overWriteFareRule.deleteOFR();
	});
	$("#btnSave").click(function(){
		overWriteFareRule.saveOFR();
	});
	$("#selChargeAmountType").change(function(){
		if ($(this).val() != "V"){
			$("#txtMinPChargeAmount").enable();
			$("#txtMaxPChargeAmount").enable();
			setField("txtChargeAmountInLocal","0.00");
			getFieldByID("txtChargeAmount").readOnly = false;
			getFieldByID("txtChargeAmountInLocal").readOnly = true;
		}else{
			$("#txtMinPChargeAmount").val("");
			$("#txtMaxPChargeAmount").val("");
			$("#txtMinPChargeAmount").disable();
			$("#txtMaxPChargeAmount").disable();
			if(overWriteFareRule.isLocalCurrency){
				setField("txtChargeAmount","0.00");
				getFieldByID("txtChargeAmount").readOnly = true;
				getFieldByID("txtChargeAmountInLocal").readOnly = false;
			}else{
				getFieldByID("txtChargeAmount").readOnly = false;
				getFieldByID("txtChargeAmountInLocal").readOnly = true;
			}
		}
	})
	$("#btnReset").click(function(){
		overWriteFareRule.resetForm();
		MessageResetForStatusBar();
	});
	$("#chkStatus").click(function(){
		if ($(this).attr("checked") == 'checked'){
			$(this).val("Y");
		}else{
			$(this).val("N");
		}
	});
	$("#chkTwlHrsCutOver").click(function(){
		if ($(this).attr("checked") == 'checked'){
			$(this).val("Y");
		}else{
			$(this).val("N");
		}
	});
	$("#btnTwlCutOverApply").click(function(){
		overWriteFareRule.updateFeeTwlHrsCutover();
	});
	$("#btnTwlCutOverEdit").click(function(){
		overWriteFareRule.editFeeTwlHrsCutover();
	});
	$("#btnTwlCutOverApply").disable();
	$("#chkTwlHrsCutOver").disable();
	$("#btnTwlCutOverEdit").enable();
	//Build Data grid structure
	overWriteFareRule.buildGrid();
	//on page load call to search action to fill the Grid
	MessageResetForStatusBar();
	overWriteFareRule.search();
};
overWriteFareRule.fillDropDownBoxes = function(){
	$.each(listChargeType, function(i, opt){
		var t = $("<option>"+opt.value+"</option>").attr({value:opt.key})
		$("#selChargeTypeSH, #selChargeTypeSH").append(t);
	});
	$.each(listCompareAgainst, function(i, opt){
		var t = $("<option>"+opt.value+"</option>").attr({value:opt.key})
		$("#selCompareAgainstSH, #selCompareAgainst").append(t);
	});
	$.each(listTimeType, function(i, opt){
		var t = $("<option>"+opt.value+"</option>").attr({value:opt.key});
		$("#selTimeTypeSH, #selTimeType").append(t);
	});
	$.each(listChargeType, function(i, opt){
		var t = $("<option>"+opt.value+"</option>").attr({value:opt.key});
		$("#selChargeType").append(t);
	});
	$.each(listChargeAmtType, function(i, opt){
		var t = $("<option>"+opt.value+"</option>").attr({value:opt.key});
		$("#selChargeAmountType").append(t);
	});
};

overWriteFareRule.resetForm = function(){
	$("#tblOWFarerule").find("tr").removeClass("ui-state-highlight");
	$("#tblFormData").find("select").val("-");
	$("#tblFormData").find("input[type!='button']").val("");
	$("#btnEdit").disable();
	$("#btnDelete").disable();
	$("#btnSave").disable();
	$("#btnReset").disable();
	overWriteFareRule.disableEnableFields('disable');
};
overWriteFareRule.addNewOFR = function(){
	overWriteFareRule.resetForm();
	overWriteFareRule.disableEnableFields('enable');
	overWriteFareRule.actionMode = "add";
	overWriteFareRule.rowEditFlag = true;
	$("#txtMinPChargeAmount").val("");
	$("#txtMaxPChargeAmount").val("");
	$("#txtMinPChargeAmount").disable();
	$("#txtMaxPChargeAmount").disable();
	$("#chkStatus").val("Y");
	$("#chkStatus").attr("checked", true);
	$("#btnSave").enable();
	$("#btnReset").enable();
};
overWriteFareRule.editOFR = function(){
	overWriteFareRule.disableEnableFields('enable');
	overWriteFareRule.actionMode = "edit";
	$("#btnSave").enable();
	$("#btnReset").enable();
};
overWriteFareRule.errorLoading = function(response){
	alert(response);
};

overWriteFareRule.search = function(){
	if (overWriteFareRule.rowEditFlag){
		var t = confirm("Changes will be lost, Do you want to continue?");
		if (t){
			overWriteFareRule.rowEditFlag = false;
			overWriteFareRule.callSearchAction();
			MessageResetForStatusBar();
		}
	}else{
		overWriteFareRule.callSearchAction();
	}
};

overWriteFareRule.callSearchAction = function(){
	overWriteFareRule.resetForm();
	var data = {};
	data.fareRuleCode = ofrcode;
	data.fareRuleId = ofrId;
	data.compareAgainst = $("#selCompareAgainstSH").val();
	data.timeType = $("#selTimeTypeSH").val();
	data.chargeType = $("#selChargeTypeSH").val();
	data.searchMode = requestFrom;
	data.ruleVersion = ruleVersion;

	var url = "fareOverwrite.action";
	$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
	success: overWriteFareRule.searchSucessProcess, error:overWriteFareRule.errorLoading, cache : false});	
};

overWriteFareRule.disableEnableFields = function(state){
	if (state.toLowerCase() == "disable"){
		$("#tblFormData").find("select").disable();
		$("#tblFormData").find("input[type!='button']").disable();
	}else{
		$("#tblFormData").find("select").enable();
		$("#tblFormData").find("input[type!='button']").enable();
	}
};

overWriteFareRule.searchSucessProcess = function(response){
	$("#tblOWFarerule").jqGrid("clearGridData", true);	
	var gridData = response.overwriteFares;
	$.each(gridData, function(i, rowData){
		$("#tblOWFarerule").addRowData(i + 1, rowData);
	});
	if (response.feeCutoverAppliedStatus.toUpperCase() == 'Y'){
		$("#chkTwlHrsCutOver").attr("checked", true);
		$("#chkTwlHrsCutOver").val("Y");
	}else{
		$("#chkTwlHrsCutOver").attr("checked", false);
		$("#chkTwlHrsCutOver").val("N");
	}
}

overWriteFareRule.buildGrid = function(){
	$("#tblOWFarerule").jqGrid({
		datatype: "local",
		colNames:['&nbsp;','Charge Type','Ch. Amt Type', 'Charge Amount', 'Min','Max','Compare Against', 
		          'Time Type', 'Compare Againstval', 'Time TypeVal', 'Time Value','Status','Status Val','Amount In Local'],
	   	colModel:[  {name:'id', width:30, label:"id"},
	   	            {name:'chargeType',index:'chargeType', width:90 , align:'center' },
	   	            {name:'chargeAmountType', index:'chargeAmountType', width:90 ,align:'center'},
	   	            {name:'chargeAmount', width:100, align:'right' },
	   	            {name:'min', width:70, align:'right' },
	   	            {name:'max', width:70, align:'right' },
	   	            {name:'compareAgainstText', width:120, align:'center' },		
	   	            {name:'timeTypeText', width:80, align:'center' },
	   	            {name:'compareAgainst', align:'' , hidden:true},		
	   	            {name:'timeType', align:'' , hidden:true},
	   	            {name:'timeValue', width:80, align:'right' },
	   	            {name:'statusTxt', width:60, align:'center'},
	   	            {name:'status', align:'' , hidden:true },
	   	            {name:'chargeAmountInLocal',align:'' , hidden:true}
	   	         ],
	 	height: 130,
		imgpath: '',
		multiselect: false,
		viewrecords: true,
		onSelectRow: function(rowid){
			if (overWriteFareRule.rowEditFlag){
				var t = confirm("Changes will be lost, Do you want to continue?");
				if (t){
					overWriteFareRule.rowEditFlag = false;
					MessageResetForStatusBar();
				}
			}else{
				$("#btnEdit").enable();
				$("#btnDelete").enable();
				$("#btnSave").disable();
				$("#btnReset").disable();
				var rowData = jQuery(this).getRowData(rowid); 
	            overWriteFareRule.fillForm(rowData);
			}
			overWriteFareRule.selRowID = rowid;
		}
	});
};
overWriteFareRule.deleteOFR = function(){
	var t = confirm("Are you sure to delete this?");
	if (t){
		MessageResetForStatusBar();
		var data = {};
		data['fee.id'] = $("#fareFeeId").val();		
		data['actionMode'] = 'delete';
		data['fareRuleCode'] = ofrcode;
		data['fareRuleId'] = ofrId;
		data.ruleVersion = ruleVersion;
		data.searchMode = requestFrom;

		var url = "fareOverwrite!overwriteFare.action";
		//if(requestFrom == 'manageFare'){
		//	url = "fareOverwrite!overwriteMaster.action"
		//}else if(requestFrom == "creatFare"){
		//	url = "fareOverwrite!overwriteFare.action"
		//}
		$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
			success: overWriteFareRule.saveOFRSucessProcess, error:overWriteFareRule.errorLoading, cache : false});
	}
};

overWriteFareRule.updateFeeTwlHrsCutover = function(){
	MessageResetForStatusBar();
	var feeCutoverApplyStatus = "N";
	if($("#chkTwlHrsCutOver").is(':checked')){
		feeCutoverApplyStatus = "Y";
	}
	var data = {};
	data['fareRuleCode'] = ofrcode;	
	data['fareRuleId'] = ofrId;	
	data['feeCutoverAppliedStatus'] = $("#chkTwlHrsCutOver").val();
	data.ruleVersion = ruleVersion;
	data.searchMode = requestFrom;

	var url = "fareOverwrite!updateFareFeeTwlHrsCutover.action"
	
	$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
			success: overWriteFareRule.updateFeeTwlHrsCutoverSuccess, error:overWriteFareRule.errorLoading, cache : false});
	
};
overWriteFareRule.editFeeTwlHrsCutover = function(){
	$("#btnTwlCutOverApply").enable();
	$("#chkTwlHrsCutOver").enable();
	$("#btnTwlCutOverEdit").disable();
}

overWriteFareRule.updateFeeTwlHrsCutoverSuccess = function(response){
	if (response.success == true){
		overWriteFareRule.rowEditFlag = false;
		showWindowCommonError("Confirmation", "Record updated successfully!!!")
		overWriteFareRule.search();
		$("#btnTwlCutOverApply").disable();
		$("#chkTwlHrsCutOver").disable();
		$("#btnTwlCutOverEdit").enable();
	}else{
		showWindowCommonError("Error", response.messageTxt);
	}
}


overWriteFareRule.validateForm = function(){
	var temp = true;
	var  isLocalCurrency = overWriteFareRule.isLocalCurrency;
	if ( "-" == $.trim($("#selChargeType").val()) ){
		showWindowCommonError("Error", "Please select a Charge Type");
		temp = false;
	}else if ( "-" == $.trim($("#selChargeAmountType").val()) ){
		showWindowCommonError("Error", "Please select a Charge Amount Type");
		temp = false;
	}else if ( "-" == $.trim($("#selCompareAgainst").val()) ){
		showWindowCommonError("Error", "Please select a Compare Against");
		temp = false;
	}else if ( "-" == $.trim($("#selTimeType").val()) ){
		showWindowCommonError("Error", "Please select a Time Type");
		temp = false;
	}else if (!isLocalCurrency && "" == $.trim($("#txtChargeAmount").val()) ){
		showWindowCommonError("Error", "Please enter a Charge Amount");
		temp = false;
	}else if (isLocalCurrency && "" == $.trim($("#txtChargeAmountInLocal").val()) ){
		showWindowCommonError("Error", "Please enter a Charge Amount");
		temp = false;
	}else if (!isLocalCurrency && $("#selChargeAmountType").val() !="V" && parseInt($("#txtChargeAmount").val()) > 100 ){
		showWindowCommonError("Error", "Percentage cannot be greater than 100");
		temp = false;
	}else if (isLocalCurrency && $("#selChargeAmountType").val() !="V" && parseInt($("#txtChargeAmountInLocal").val()) > 100 ){
		showWindowCommonError("Error", "Percentage cannot be greater than 100");
		temp = false;
	}else if ($("#selChargeAmountType").val() !="V" && "" != $.trim($("#txtMinPChargeAmount").val()) && "" != $.trim($("#txtMaxPChargeAmount").val()) 
			&& parseInt($("#txtMinPChargeAmount").val()) >= parseInt($("#txtMaxPChargeAmount").val()) ){
		showWindowCommonError("Error", "Maximum charge amount should be greater than the minimum");
		temp = false;
	}else if ( "" == $.trim($("#txtTimeValue").val()) ){
		showWindowCommonError("Error", "Please enter a Time Value");
		temp = false;
	}
	return temp;
};
overWriteFareRule.saveOFR = function(){
	if ( overWriteFareRule.rowEditFlag && overWriteFareRule.validateForm() ){
		MessageResetForStatusBar();
		var data = {};
		data['fee.id'] = $("#fareFeeId").val();
		data['fee.chargeType'] = $("#selChargeType").val();
		data['fee.compareAgainst'] = $("#selCompareAgainst").val();
		data['fee.chargeAmountType'] = $("#selChargeAmountType").val();
		data['fee.timeType'] = $("#selTimeType").val();
		data['fee.chargeAmount'] = $("#txtChargeAmount").val();
		data['fee.chargeAmountInLocal'] = $("#txtChargeAmountInLocal").val();
		data['fee.timeValue'] = $("#txtTimeValue").val();
		data['fee.min'] = $("#txtMinPChargeAmount").val();
		data['fee.max'] = $("#txtMaxPChargeAmount").val();
		data['fee.status'] = $("#chkStatus").prop("checked") ? "Y" : "N";
		data['actionMode'] = overWriteFareRule.actionMode;
		data['fareRuleCode'] = ofrcode;
		data['fareRuleId'] = ofrId;
		data.ruleVersion = ruleVersion;
		data.searchMode = requestFrom;
	
		var url = "fareOverwrite!overwriteFare.action";
		
		$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
		success: overWriteFareRule.saveOFRSucessProcess, error:overWriteFareRule.errorLoading, cache : false});
	}
};

overWriteFareRule.saveOFRSucessProcess = function(response){
	if (response.success == true){
		overWriteFareRule.rowEditFlag = false;
		if(response.actionMode == 'delete'){
			showWindowCommonError("Confirmation", "Record deleted successfully!!!")
		}else{
			showWindowCommonError("Confirmation", "Record saved successfully!!!");
		}
		overWriteFareRule.search();
	}else{
		showWindowCommonError("Error", response.messageTxt);
	}
};

overWriteFareRule.fillForm = function(rowData){
	overWriteFareRule.disableEnableFields('enable');
	$("#fareFeeId").val(rowData['id']);
	$("#selChargeType").val(rowData['chargeType']);
	$("#selCompareAgainst").val(rowData['compareAgainst']);
	$("#selChargeAmountType").val(rowData['chargeAmountType']);
	if (rowData['chargeAmountType'] != "V"){
		$("#txtMinPChargeAmount").enable();
		$("#txtMinPChargeAmount").enable();
		$("#txtMinPChargeAmount").val(rowData['min']);
		$("#txtMaxPChargeAmount").val(rowData['max']);
	}else{
		$("#txtMinPChargeAmount").val("");
		$("#txtMaxPChargeAmount").val("");
		$("#txtMinPChargeAmount").disable();
		$("#txtMaxPChargeAmount").disable();
	}
	$("#selTimeType").val(rowData['timeType']);
	$("#txtChargeAmount").val(rowData['chargeAmount']);
	$("#txtChargeAmountInLocal").val(rowData['chargeAmountInLocal']);
	$("#txtTimeValue").val(rowData['timeValue']);
	if (rowData['status'].toUpperCase() == 'Y'){
		$("#chkStatus").attr("checked", true);
		$("#chkStatus").val("Y");
	}else{
		$("#chkStatus").attr("checked", false);
		$("#chkStatus").val("N");
	}
	overWriteFareRule.disableEnableFields('disable');
};
//change any select box of the form
$("#tblFormData").find("select").change(function(){
	overWriteFareRule.rowEditFlag = true;
});
//change any input box of the form
$("#tblFormData").find("input[type!='button']").change(function(){
	overWriteFareRule.rowEditFlag = true;
});

$(function() {
	overWriteFareRule.ready();
	overWriteFareRule.setFareRuleSelectedCurrency();
});

overWriteFareRule.setFareRuleSelectedCurrency = function() {
	if (fareRuleLocalCurr != null && fareRuleLocalCurr != "") {
		overWriteFareRule.isLocalCurrency = true; 
		getFieldByID("txtChargeAmount").readOnly = true;
		getFieldByID("txtChargeAmountInLocal").readOnly = false;
	} else {
		overWriteFareRule.isLocalCurrency = false;
		getFieldByID("txtChargeAmount").readOnly = false;
		getFieldByID("txtChargeAmountInLocal").readOnly = true;
	}
};

overWriteFareRule.closeMe =  function() {
	var srtSearchParams = 'fareRuleCode|' + opener.$("#selFareRuleCode").val() + ',status|' + opener.$("#selStatus").val();	
	var delimiter = "^";
	var redirectTo = window.opener.location.pathname+"?mode="+opener.screeMode;
	if(opener.fareID != undefined && opener.fareID != ""){
		redirectTo = redirectTo +"&fareId="+opener.fareID;
	}
	opener.setTabValues("SC_ADMN_018", delimiter, "strSearchCriteria", srtSearchParams);
	
	window.opener.location.replace(redirectTo);
    window.close();
};

$(window).unload(function() {
	var srtSearchParams = 'fareRuleCode|' + opener.$("#selFareRuleCode").val() + ',status|' + opener.$("#selStatus").val();	
	var delimiter = "^";
	var redirectTo = window.opener.location.pathname+"?mode="+opener.screeMode;
	if(opener.fareID != undefined && opener.fareID != ""){
		redirectTo = redirectTo +"&fareId="+opener.fareID;
	}
	opener.setTabValues("SC_ADMN_018", delimiter, "strSearchCriteria", srtSearchParams);
	window.opener.location.replace(redirectTo);
    window.close();
});