var screenId = "SC_SHDS_005";
var valueSeperator = "~";
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var blnRowClick = false;

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Date of Sale";
objCol1.headerText = "Date of Sale";
objCol1.itemAlign = "left";
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "20%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Card Type";
objCol2.headerText = "Card Type";
objCol2.itemAlign = "left"
objCol2.sort = "true";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "10%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Amount";
objCol3.headerText = "Amount";
objCol3.itemAlign = "right"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "25%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Transfer Timestamp";
objCol4.headerText = "Transfer Timestamp";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "25%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Status";
objCol5.headerText = "Status";
objCol5.itemAlign = "Left"

var objCol6 = new DGColumn();
objCol6.columnType = "checkbox";
objCol6.width = "10%";
objCol6.arrayIndex = 8;
objCol6.toolTip = "Action";
objCol6.headerText = "Action";
objCol6.itemAlign = "Left"
objCol6.checkAll = false;
objCol6.linkOnClick = "chkvalidate";
objCol6.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnCreditCardSalesTransfer");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);

objDG.width = "99%";
objDG.height = "210px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrCreditData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";

function objOnFocus() {
	top[2].HidePageMessage();
}

function chkvalidate(rowNo) {
	objOnFocus();
	var count = 0;
	if (isClicked == false) {
		isClicked = true;
		var selected = arrCreditData[rowNo][9];
		var isEqual = true;
		strGridRow = rowNo;
		var selectedToProcess = objDG.getSelectedColumn(5);
		for ( var i = 0; i < selectedToProcess.length; i++) {
			var iteVal = arrCreditData[i][9];
			if (selectedToProcess[i][1] && iteVal != selected) {
				if (rowNo != i) {
					objDG.setCellValue(i, 5, "false");
				}
			}
			if (selectedToProcess[i][1]) {
				count++
			}
		}
		isClicked = false;
		if (count > 0) {
			Disable("btnGenTransfer", "");
			displayDownLoadReportBtn();
		} else {
			Disable("btnGenTransfer", "true");
			Disable("btnViewReport",true);
			getFieldByID("btnGenTransfer").value = "Generate/Transfer";
		}
		if (selectedToProcess[rowNo][1] && arrCreditData[rowNo][9] == "Y") {
			getFieldByID("btnGenTransfer").value = "Re-transfer";
			document.forms[0].hdnMode.value = "TRANSFER";
		} else if (selectedToProcess[rowNo][1]
				&& arrCreditData[rowNo][9] == "N") {
			getFieldByID("btnGenTransfer").value = "Transfer";
			document.forms[0].hdnMode.value = "TRANSFER";
		} else if (selectedToProcess[rowNo][1]
				&& (arrCreditData[rowNo][9] == "U" || arrCreditData[rowNo][9] == "")) {
			getFieldByID("btnGenTransfer").value = "Generate";
			document.forms[0].hdnMode.value = "GENERATE";
		}
	}
}

var isClicked = false;

function writeTotal() {
	var strHTMLText = "";
	strHTMLText += '<table width="99%" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="center"   style="position:absolute;left:350px;top:455px;>';
	strHTMLText += '<tr>';
	strHTMLText += '	<td align="left">';
	strHTMLText += '	<font><b>' + totString + '<\/b><\/font>';
	strHTMLText += '	<\/td>';
	strHTMLText += '	<\/tr>';
	strHTMLText += '	<\/table>';
	DivWrite("spnTotalCreditSales", strHTMLText);
}

writeTotal();
