var screenId = "SC_SHDS_0034";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var strETLGridRow;
var intLastRec;
var initialArr=new Array();
initialArr=arrData;
var closeAction;

var paxStatusArr= new Array("FLOWN" ,"NOT BOARDED");
var paxStatusOptArr= new Array("F" ,"B");

var processTypeArr= new Array("All" ,"Parsed", "Reconciled","Unparsed");
var processTypeOptArr= new Array("All" ,"P", "R","U");
var isPax=true;
var initStat = "";
var genAction = "" ;
var etlStat = "";
var etlPaxStat = "";
var isChangedPAX="NO";
var currentETL="";

var values=opener.currentETLData;

var DownloadTS = values[0];
var FlightNo = values[1];
var FlightDate = values[2];
var ETLId = values[3];
var FromAirport = values[11];
var FromAddress = values[12];
var Status ="";
if(etlState!=""){
	var state;
	if(etlState=="N"){
		state="Parsed"
	}if(etlState=="U"){
		state="Unparsed"
	}if(etlState=="R"){
		state="Processed"
	}
	 Status = state;
}else{
	 Status = values[4];
}
var EtlContent=values[5];
setField("txtaETLCmnts",EtlContent);

//On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg,strMsgType) {
	if(strMsg != "" && strMsgType != ""){
		showWindowCommonError(strMsgType,strMsg);
	}	
	objOnFocus();
	setField("hdnCurrentEtl",values);
	genAction="PL";
	getFieldByID("selTitle").focus();
	opener.setPageEdited(false,isPax);
	populateStatusCombo(arrTitle,"selTitle",arrTitle);
	populatePaxTypeCombo(paxTypes,"selPaxType",paxTypes);
	populateCombo(paxStatusArr,"selAddStatus",paxStatusOptArr);
	setDefaultAirport();

	//setField("selPaxCat","A");
	if(arrFormData!=null && arrFormData.length>0){
		setField("hdnCurrentEtl",arrFormData[0]);		
		values=arrFormData[0].split(",");
		setField("selTitle",arrFormData[1]);
		setField("selPaxType",arrFormData[14]);
		setField("txtFirstName",arrFormData[2]);
		setField("txtLastName",arrFormData[3]);
		setField("selDest",arrFormData[5]);
		setField("selAddStatus",arrFormData[6]);
		changeStatus();
		setField("hdnPaxVersion",arrFormData[7]);
		setField("hdnProcessSta",arrFormData[8]);
		setField("hdnCurrentRowNum",arrFormData[9]);
		strGridRow=arrFormData[9];
		setField("hdnMode",arrFormData[10]);
		setField("hdnUIMode",arrFormData[11]);
		setField("hdnPaxID",arrFormData[12]);
		setField("hdnETLID",arrFormData[13]);
		//setField("txtPaxNO",arrFormData[14]);

		//setField("selPaxCat",arrFormData[16]);		
		 DownloadTS = values[0];
		 FlightNo = values[1];
		 FlightDate = values[2];
		 ETLId = values[3];
		 FromAirport = values[11];
		 FromAddress = values[12];
		 Status ="";
		 if(etlState!=""){
		var state;
		if(etlState=="P"){
			state="Parsed"
		}if(etlState=="U"){
			state="Unparsed"
		}if(etlState=="R"){
			state="Processed"
		}
		 Status = state;
	}else{
		 Status = values[4];
	}
	var EtlContent=values[5];
	setField("txtaETLCmnts",EtlContent);
	DivWrite("spnContent",EtlContent);
		if(arrFormData[11]=="ADD"){
			genAction="CA";
			
		}
		if(arrFormData[11]=="EDIT"){
			genAction="CE";
		} 
		setField("hdnState","");
		opener.setPageEdited(true,isPax);
			if(saveSuccess==-1){
				getFieldByID("selTitle").focus();// 
			}if(saveSuccess==-2){
				getFieldByID("selPaxType").focus();//
			}if(saveSuccess==-3){
				getFieldByID("txtFirstName").focus();//
			}if(saveSuccess==-4){
				getFieldByID("txtLastName").focus();//
			}if(saveSuccess==-5){
				getFieldByID("txtEtNo").focus();
			}if(saveSuccess==-6){
				getFieldByID("txtCoupenNo").focus();
			}if(saveSuccess==-7){
				getFieldByID("selDest").focus();//
			}if(saveSuccess==-8){
				getFieldByID("selAddStatus").focus();//
			}
	}

	if(saveSuccess==0){
			alert("Record Successfully saved!");
			isChangedPAX="YES";
			setField("hdnState","");
	}
	if(saveSuccess==1){
		isChangedPAX="YES";
		setField("hdnState","");
		
	}
	
	var strCurrentETL="<table width='80%' border='0' cellpadding='0' cellspacing='4' ID='Table9'><tr>"
									+"<td><font><b>ETL ID</font></td>"
									+"<td><font><b>Downloaded Time</font></td>"
									+"<td><font><b>Flight No</font></td>"
									+"<td><font><b>Flight Date</font></td>"
									+"<td><font><b>Airport</font></td>"
									+"<td><font><b>SITA Address</font></td>"
									+"<td><font><b>ETL Status</font></td></tr>"
													+ "<tr><td><font>" +ETLId+"</font></td>"
													+"<td><font>"+DownloadTS+"</td>"		
													+ "<td><font>" +FlightNo+"</font></td>"
													+ "<td><font>" +FlightDate+"</font></td>"
													+ "<td><font>" +FromAirport+"</font></td>"
													+ "<td><font>" +FromAddress+"</font></td>"
													+ "<td><font>" +Status+"</font></td></tr>";

	
	document.getElementById("spnHeader").innerHTML=strCurrentETL;
	currentETL=opener.currentETL;
	setVisible("spnHeader",true);
	controlBehaviour(initStat, genAction,etlStat,etlPaxStat);
	if(arrFormData[11] == "ADDTBA") {
		//Disable("txtPaxNO",false);		
		//getFieldByID("txtPaxNO").focus();
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selAddStatus",true);
		Disable("selPaxType",true);
		Disable("selTitle",true);
		Disable("selDest",true);
	}
}

function titleOnChange(){
	var title = getFieldByID("selPaxType").value;
	if(title == "IN"){
		populateStatusCombo(arrInfantTitles,"selTitle",arrInfantTitles);
	} else if(title == "CH"){
		populateStatusCombo(arrTitleChild,"selTitle",arrTitleChild);
	} else {
		populateStatusCombo(arrTitle,"selTitle",arrTitle);
	}
}

function setDefaultAirport(){
	if(getFieldByID("selDest").options.length==2){
			getFieldByID("selDest").options[1].selected=true;
	}

}

function populateCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	for(var t=0;t<dataArr.length;t++){
		control.options[t]=new Option(dataArr[t],optArr[t]);
	}

}

function populateStatusCombo(dataArr,controlName,optArr){	
	var control=document.getElementById(controlName);
	document.getElementById(controlName).value="";
	control.options.length = 0;
	control.options[0]=new Option("","");
	var p=0;
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
		p=t+1;
		control.options[p]=new Option(strArr[1],strArr[0]);
	}

}
function populatePaxTypeCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	//control.options[0]=new Option("","");
	var p=0;
	//sort the array
	sort(dataArr,1);
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
	//	p=t+1;
		control.options[t]=new Option(strArr[1],strArr[0]);
	}

}



function tbaClick(){
	closeAction="DA";
	if(etlDetailChanged(closeAction)){
		setField("hdnPaxVersion","-1")
		opener.setPageEdited(false,isPax);
		clearControls();
		genAction="CA";
		controlBehaviour(initStat, genAction,etlStat,etlPaxStat);
		//Disable("txtPaxNO",false);		
		//getFieldByID("txtPaxNO").focus();
		setField("txtFirstName" , "T.B.A.");
		setField("txtLastName", "T.B.A.");
		setField("selAddStatus", "G");
		//setField("selPaxCat", "A");		
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selAddStatus",true);
		Disable("selTitle",true);
		Disable("selDest",true);
		Disable("selPaxType",true);
		changeStatus();
		setField("hdnMode","ADD");
		setField("hdnETLID",ETLId);
		setField("hdnUIMode","ADDTBA");
	}	
}

function clearControls(){
	setField("selTitle","");
	setField("selPaxType",paxTypes[0][0]);
	setField("txtFirstName","");
	setField("txtLastName","");
	setField("selDest","");
	setField("txtEtNo","");
	setField("txtCoupenNo","");
	setField("txtInfCoupenNo","");
	setField("txtInfEtNo","");
	setDefaultAirport();
	setField("selAddStatus","N");
	//setField("txtPaxNO","");
	setField("hdnPaxID","");
	//setField("selPaxCat","A");
}

function controlBehaviour(initStat, genAction,etlStat,etlPaxStat){
	
	enableDataEntryControls(genAction);


}

function enableDataEntryControls(action){

	if(action=="SR1" || action=="PL"){
		Disable("selTitle",true);
		Disable("selPaxType",true);
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selDest",true);
		Disable("selAddStatus",true);
		//Disable("selPaxCat",true);		
		//Disable("txtPaxNO",true);
		Disable("txtEtNo",true);
		Disable("txtInfEtNo",true);
		
	}else{
		Disable("selTitle",false);
		Disable("selPaxType",false);
		Disable("txtFirstName",false);
		Disable("txtLastName",false);
		Disable("selDest",false);
		Disable("selAddStatus",false);
		//Disable("selPaxCat",false);
		//Disable("txtPaxNO",true);
		Disable("txtEtNo",false);
		Disable("txtInfEtNo",false);

	}
	if(action=="CD"){
		//Disable("txtPaxNO",false);
		//Disable("selPaxCat",false);
	}

}

function Enable(strControlId, enable){
	Disable(strControlId,!enable);
}

function setStatus(strSelStatus,ctrlName){
	var control=document.getElementById(ctrlName);

	for(var t=0;t<(control.length);t++){
			if(control.options[t].text==strSelStatus){
			control.options[t].selected=true;
			break;
		}
	}

}

//on Grid2 Row click
function RowClick(strRowNo){
	objOnFocus();
	closeAction="DRC";
	if(etlDetailChanged(closeAction)){
		closeAction="DG";
		genAction = "SR1"
		opener.setPageEdited(false,isPax);
		etlPaxStat=arrData[strRowNo][8];
		//etlStat=arrData[strRowNo][8];
		strGridRow=strRowNo;
		controlBehaviour(initStat, genAction,etlStat,etlPaxStat);
		setField("selPaxType",arrData[strRowNo][14]);
		titleOnChange();
		setField("hdnGridRow",strRowNo);
		setField("hdnMode","");
		setField("selTitle",arrData[strRowNo][0]);		
		setField("txtFirstName",arrData[strRowNo][1]);
		setField("txtLastName",arrData[strRowNo][2]);
		setField("hdnETLPaxID",arrData[strRowNo][3]);
		setField("selDest",arrData[strRowNo][4]);
		setField("selAddStatus",arrData[strRowNo][7]);
		setField("hdnProcessSta",arrData[strRowNo][8]);
		setField("hdnPaxID",arrData[strRowNo][9]);
		setField("hdnPaxVersion",arrData[strRowNo][10]);
		setField("hdnETLID",arrData[strRowNo][11]);
				
		if(arrData[strRowNo][14] == "IN"){
			setField("txtEtNo","");	
			setField("txtCoupenNo","");
			setField("txtInfEtNo",arrData[strRowNo][19]);
			setField("txtInfCoupenNo",arrData[strRowNo][18]);
		}else{
			if(arrData[strRowNo][18] != "null"){
				setField("txtInfEtNo",arrData[strRowNo][19]);
				setField("txtInfCoupenNo",arrData[strRowNo][18]);
			} else {
				setField("txtInfEtNo","");
				setField("txtInfCoupenNo","");
			}			
			setField("txtEtNo",arrData[strRowNo][15]);
			setField("txtCoupenNo",arrData[strRowNo][17]);
		}
		setField("hdnCurrentRowNum",strRowNo);		
		setField("hdnOrigin",FromAirport);	
	}
}




function resetETL(){
	objOnFocus();
	getFieldByID("selTitle").focus();
	opener.setPageEdited(false,isPax);
		var mode=getText("hdnUIMode");
	if(mode=="ADD"){
		genAction="CA";
		clearControls();
	
	}if(mode=="EDIT"){
		genAction="CE";
		if (strGridRow<arrData.length && arrData[strGridRow][9] == getText("hdnPaxID")) {
			setField("hdnMode","");
			setField("selTitle",arrData[strGridRow][0]);
			setField("txtFirstName",arrData[strGridRow][1]);
			setField("txtLastName",arrData[strGridRow][2]);
			setField("selDest",arrData[strGridRow][4]);
			setField("selAddStatus",arrData[strGridRow][7]);
			setField("hdnPaxVersion",arrData[strGridRow][10]);
			setField("hdnPaxID",arrData[strGridRow][9]);
			setField("hdnProcessSta",arrData[strGridRow][8]);
			//setField("selPaxCat",arrData[strGridRow][14]);
			
			setField("hdnCurrentRowNum",strGridRow);
			changeStatus();
			setStatus(arrData[strGridRow][6],"selAddStatus");
		}else{
			clearControls();
			genAction="PL";
		}
	}
	
	controlBehaviour(initStat, genAction,etlStat,etlPaxStat);
	
	if(mode=="ADDTBA") {
		tbaClick();
	}
}

function objOnFocus(){
	opener.top[2].HidePageMessage();
}

function pageOnChange(){
		opener.setPageEdited(true,isPax);
	
}

function chkChanges(){
	
	if (opener.top[1].objTMenu.tabGetPageEidted(screenId)) {	
		return confirm("Changes will be lost! Do you wish to Continue?");
	}else{		
		return true;
	}
}


function windowclose(){
	closeAction="DC";
	if(etlDetailChanged(closeAction)){
		checkETLDetailChanged();
	}
	
}

	function etlDetailChanged(action){
		var confirmed=true;
		if((action=="GD" || action=="DC" || action=="DA" || action=="DRC") && opener.isEtlPAXScreenEdited){
			confirmed=confirm("Changes will be lost! Do you wish to Continue?");
			if(confirmed){
				opener.setPageEdited(false, isPax);
			}
		}else if((action=="DC" ) && (opener.isEtlScreenEdited)){// Detail Close Clicked when header changed
			confirmed=confirm("Changes in ETL Header screen will be lost! Do you wish to Continue?");
			if(confirmed){
				opener.setPageEdited(false, isPax);
			}
		
		}else if(action=="DFC" && opener.isEtlPAXScreenEdited){// Window unlod
			opener.setPageEdited(false, isPax);
		}

		return confirmed;

	}

	function reloadETLHEader(){
		if(closeAction!="R"){
			if(isChangedPAX == "YES"){
			//	confirmed=confirm("Changes in ETL Header screen will be lost! Do you wish to Continue?");
		//	if(confirmed){
					var strTxt=opener.top[1].objTMenu.tabGetValue(screenId);
					var arrTabData=new Array()
					if(strTxt!=""){
						 arrTabData=strTxt.split("~");
					}
					opener.location="showETLProcessing.action?hdnMode=SEARCH&hdnUIMode=SEARCH"+ "&hdnRecNo="+arrTabData[4]+"&txtFrom="+arrTabData[0]+"&txtTo="+arrTabData[1]+"&selAirport="+arrTabData[2]+"&selProcessType="+arrTabData[3];
			}

		 // }
		}

	}

	
	function checkETLDetailChanged(){
		if(isChangedPAX == "YES"){
			var strTxt=opener.top[1].objTMenu.tabGetValue(screenId);
			var arrTabData=new Array()
			if(strTxt!=""){
				 arrTabData=strTxt.split("~");
			}
			opener.location="showETLProcessing.action?hdnMode=SEARCH&hdnUIMode=SEARCH"+ "&hdnRecNo="+arrTabData[4]+"&txtFrom="+arrTabData[0]+"&txtTo="+arrTabData[1]+"&selAirport="+arrTabData[2]+"&selProcessType="+arrTabData[3];
			window.close();
		} else {
			window.close();
		}
	}
	

function resetVariables() {
	etlDetailChanged("DFC");
}

function changeStatus(){
 	if(getValue("selAddStatus")=="N"){
		Disable("chkNoREC",true);
		//Disable("selPaxCat",true);		
 	}else {
		Disable("chkNoREC",false);
		//Disable("selPaxCat",false);
 	}
}

function paxChange(){
	var paxType = getFieldByID("selPaxType").value;
	if(paxType == 'IN'){
		
		document.getElementById("infantEticketMandater").style.display = "inline";
		document.getElementById("infantCoupenNoMandater").style.display = "inline";
		document.getElementById("eTicketMandater").style.display = "none";
		document.getElementById("coupenNoMandater").style.display = "none";
		document.getElementById("paxTitleMandater").style.display = "none";
		
	} else {
		
		document.getElementById("infantEticketMandater").style.display = "none";
		document.getElementById("infantCoupenNoMandater").style.display = "none";
		document.getElementById("eTicketMandater").style.display = "inline";
		document.getElementById("coupenNoMandater").style.display = "inline";
		document.getElementById("paxTitleMandater").style.display = "inline";
		
	}	
}


function addClick(){
	enableDisableControls(false);
	setField("hdnPaxVersion",-1);
	setField("hdnUIMode","ADD");
	paxChange();
	clearControls();
	titleOnChange();
}

function editClick(){	
	setField("hdnUIMode","EDIT");
	paxChange();
	enableDisableControls(false);
}

function deleteClick(){
	var status = confirm(deleteMessage);
	if(status){
		setField("hdnMode","DELETE");
		document.forms[0].submit();
		clearControls();
	}	
}

function processSaveETL(mode){
	
	if(validateSearch()){
		setField("hdnMode",mode);
		setField("hdnETLID",ETLId);
		setField("hdnDownloadDate",DownloadTS);
		setField("hdnFlightNo",FlightNo);
		setField("hdnFlightDate",FlightDate);
		setField("hdnOrigin",FromAirport);
		setField("hdnETLProcessStatus",Status);
		if(document.getElementById("chkNoREC").checked){
			setField("hdnIsNoREC",true);
		}else{
			setField("hdnIsNoREC",false);
		}
		isChangedPAX = "YES";
		document.forms[0].hdnMode.value=mode;
		document.forms[0].submit();
	}
}

function validateSearch(){
	var eTicketNo = "";
	var coupenNumber = "";
	var isValidSearch = false;
	if(getText("selPaxType") == "AD" || getText("selPaxType") == "CH"){
		eTicketNo = getText("txtEtNo");
		coupenNumber = getText("txtCoupenNo");
	} else {
		eTicketNo = getText("txtInfEtNo");
		coupenNumber = getText("txtInfCoupenNo");
	}
	if(eTicketNo == ""){
		showWindowCommonError("Error",eTicketReq);
		return false;
	}
	if(coupenNumber == ""){
		showWindowCommonError("Error",coupenNoReq);
		return false;
	}
	if(getText("selPaxType") != "IN" && getText("selTitle") == ""){
		showWindowCommonError("Error",titleReq);
		getFieldByID("selTitle").focus();
		return false;
	}
	return true;
}

function enableDisableControls(mode){
	Disable("selTitle",mode);
	Disable("selPaxType",mode);
	Disable("txtFirstName",mode);
	Disable("txtLastName",mode);
	Disable("txtEtNo",mode);
	Disable("txtInfEtNo",mode);
	Disable("selAddStatus",mode);
	Disable("selDest",mode);
}

function validateCoupenNo(){
	var strCoupNo =  getFieldByID("txtCoupenNo").value;
	strCoupNo = strCoupNo.replace(/[^0-9]+/g,'');
	setField("txtCoupenNo",strCoupNo);
}

function validateInfCoupenNo(){
	var strInfCoupNo =  getFieldByID("txtInfCoupenNo").value;
	strInfCoupNo = strInfCoupNo.replace(/[^0-9]+/g,'');
	setField("txtInfCoupenNo",strInfCoupNo);	
}