var objWindow;
setField("hdnLive", repLive);
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;		
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_090";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function radioReportChanged(){
		setField("hdnRptType","DETAIL");
}

function viewClick() {
	
	
	setField("hdnRptType","DETAIL");
		 
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();	
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtInvalid);
		getFieldByID("txtToDate").focus();	
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {

		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmBlPaxPag");
		objForm.target = "CWindow";

		objForm.action = "blacklistedPassengersReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function load() {
	getFieldByID("txtFlightNumber").focus();
}

function radioOptionChanged() {

	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");
	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");
	}
}

function loadUsersClick() {
	
		var strSearchCriteria = getValue("txtFromDate") + "#"
		+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
		+ getFieldByID("chkTAs").checked + "#"
		+ getFieldByID("chkCOs").checked + "#"
		+ getValue("txtFlightNumber") + "#" 
		+ getValue("txtFromDepDate") + "#"
		+ getValue("txtToDepDate")+ "#"
		+ getFieldByID("radOption").checked + "#"
		+ ls.getSelectedDataWithGroup() + "#"
		+ ls2.getSelectedDataWithGroup() + "#"
		+ getValue("selBookCurStatus")+"#"
		+getFieldByID("chkExtOnhold").checked+"#"
		+ getValue("txtBookFromDate") + "#" + getValue("txtBookToDate");
		
		
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmBlPaxPag");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();

}

function isFlightNoValid(txtfield) {
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}

function checkWithSysDate(strdate){
	
	var dateValue = getText(strdate);
	var tempDate = dateValue;
	
	tempDay=tempDate.substring(0,2);
	tempMonth=tempDate.substring(3,5);
	tempYear=tempDate.substring(6,10); 	
	var tempODate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
			
	if(tempODate > strSysODate){		
		return false;
	} else{		
		return true;
	}
}