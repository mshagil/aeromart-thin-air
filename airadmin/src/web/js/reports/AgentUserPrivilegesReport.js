var objWindow;
setField("hdnLive", repLive);
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

var screenId = "UC_REPM_028";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function radioReportChanged() {
	if (getValue("chkOption") == "UserListDetails") {
		setField("hdnRptType", "UserListDetails");
	} else if (getValue("chkOption") == "UserListSummary") {
		setField("hdnRptType", "UserListSummary");
	} else if (getValue("chkOption") == "PrivilegeDetails") {
		setField("hdnRptType", "PrivilegeDetails");
	} else if (getValue("chkOption") == "UserPrivilegeDetails") {
		setField("hdnRptType", "UserPrivilegeDetails");
	}
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function getUsers() {
	var strUsers = ls2.getSelectedDataWithGroup();
	var newUsers;
	if (strUsers.indexOf(":") != -1) {
		strUsers = replaceall(strUsers, ":", ",");
	}
	if (strUsers.indexOf("|") != -1) {
		newUsers = replaceall(strUsers, "|", ",");
	} else {
		newUsers = strUsers;
	}
	return newUsers;
}

function getRoles() {
	var strRoles = ls3.getSelectedDataWithGroup();
	var newRoles;
	if (strRoles.indexOf(":") != -1) {
		strRoles = replaceall(strRoles, ":", ",");
	}
	
	if (strRoles.indexOf("|") != -1) {
		newRoles = replaceall(strRoles, "|", ",");
	} else {
		newRoles = strRoles;
	}
	
	return newRoles;
}

function getPrivileges() {
	var strPrivileges = ls4.getSelectedDataWithGroup();
	var newPrivileges;
	if (strPrivileges.indexOf(":") != -1) {
		strPrivileges = replaceall(strPrivileges, ":", ",");
	}
	
	if (strPrivileges.indexOf("|") != -1) {
		newPrivileges = replaceall(strPrivileges, "|", ",");
	} else {
		newPrivileges = strPrivileges;
	}
	
	return newPrivileges;
}

function setSelectedDataWithGroup(strData,listBoxID) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();

	for ( var x = 0; x < groups.length; x++) {

		if (groups[x].indexOf(":") != -1) {
			var dd = groups[x].split(":");
			groupList = dd[1].split(",");

			for ( var z = 0; z < groupList.length; z++) {
				if (str == "") {
					str += dd[0] + ":" + groupList[z];
				} else {
					str += "," + dd[0] + ":" + groupList[z];
				}
			}
		} else {
			if (str == "") {
				str += groups[x];
			} else {
				str += "," + groups[x];
			}
		}
	}
	listBoxID.move('>', str);
}

function removeSelectedDataWithGroup(strData,listBoxID) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();

	for ( var x = 0; x < groups.length; x++) {

		if (groups[x].indexOf(":") != -1) {
			var dd = groups[x].split(":");
			groupList = dd[1].split(",");

			for ( var z = 0; z < groupList.length; z++) {
				if (str == "") {
					str += dd[0] + ":" + groupList[z];
				} else {
					str += "," + dd[0] + ":" + groupList[z];
				}
			}
		} else {
			if (str == "") {
				str += groups[x];
			} else {
				str += "," + groups[x];
			}
		}
	}
	listBoxID.move('<', str);
}

function winOnLoad() {
	getFieldByID("selAgencies").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");

		setField("selAgencies", strSearch[0]);
		Disable('chkTAs', false);
		Disable('chkCOs', false);

		if (strSearch[1] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}

		if (strSearch[2] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}

		if (strSearch[3] != null && strSearch[3] != "") {
			setSelectedDataWithGroup(strSearch[3], ls);
		}

		if (strSearch[4] != null && strSearch[4] != "") {
			setSelectedDataWithGroup(strSearch[4], ls2);
		}

		if (strSearch[5] != null && strSearch[5] != "") {
			if (strSearch[7] == 'true') {
				removeSelectedDataWithGroup(strSearch[5], ls3);
			} else {
				setSelectedDataWithGroup(strSearch[5], ls3);
			}
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
	ls2.clear();
	ls3.removeAllFromListbox();
	ls4.clear();
}

function viewClick() {
	setField("hdnAgents", getAgents());
	setField("hdnUsers", getUsers());
	setField("hdnRoles", getRoles());
	setField("hdnPrivileges", getPrivileges());
	setField("hdnPrivIndexFlag","1")
	radioReportChanged();

	if (getValue("chkOption") != "PrivilegeDetails") {
		if (getText("selAgencies") == "") {
			showCommonError("Error", agentTypeRqrd);
			getFieldByID("selAgencies").focus();
		} else if (getFieldByID("hdnUsers").value == "") {
			showCommonError("Error", usersRqrd);
			getFieldByID("lstUsers").focus();
		} else {
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmAgentGsaDetails");
			objForm.target = "CWindow";
			objForm.action = "showAgentUserPrivileges.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	} else {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAgentGsaDetails");
		objForm.target = "CWindow";
		objForm.action = "showAgentUserPrivileges.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		ls.clear();
		ls2.clear();
		ls4.clear();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ ls.getSelectedDataWithGroup() + "#"
				+ ls2.getSelectedDataWithGroup() + "#"
				+ ls3.getSelectedDataWithGroup() + "#"
				+ ls4.getSelectedDataWithGroup() + "#"
				+ "true";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmAgentGsaDetails");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function loadUsersClick() {

	var agents = getAgents();

	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else if (agents == undefined || agents == null || agents == "") {
		showCommonError("Error", agentsRqrd);
		getFieldByID("lstRoles").focus();
	} else {
		ls2.removeAllFromListbox();
		ls4.removeAllFromListbox();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ ls.getSelectedDataWithGroup() + "#"
				+ ls2.getSelectedDataWithGroup() + "#"
				+ ls3.getSelectedDataWithGroup() + "#"
				+ ls4.getSelectedDataWithGroup() + "#"
				+ "true";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", getAgents());
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmAgentGsaDetails");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function loadRolesClick(){
	var users = getUsers();
	
	if(users == undefined || users == null || users == ""){
		showCommonError("Error", "");
		//getFieldByID("selAgencies").focus();
	}else{
		ls3.removeAllFromListbox();
		var strSearchCriteria = getValue("selAgencies") + "#"
		+ getFieldByID("chkTAs").checked + "#"
		+ getFieldByID("chkCOs").checked + "#"
		+ ls.getSelectedDataWithGroup() + "#"
		+ ls2.getSelectedDataWithGroup() + "#"
		+ ls3.getSelectedDataWithGroup() + "#"
		+ ls4.getSelectedDataWithGroup() + "#"
		+ "true";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		
		if (ls.getSelectedDataWithGroup() == "") {
			setField("selAgencies", "");
		}	
		if ((ls.getSelectedDataWithGroup() == "") && (ls2.getSelectedDataWithGroup() == "")) {
			setField("selAgencies", "");
		}
		if ((ls.getSelectedDataWithGroup() != "") && (ls2.getSelectedDataWithGroup() != "")) {
			setField("hdnAgents", getAgents());
			setField("hdnUsers", getUsers());
		}
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmAgentGsaDetails");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function loadPrivilegesClick(){
	var roles = getRoles();
	
	if(roles == undefined || roles == null || roles == "") {
		showCommonError("Error", rolesRqrd);
		getFieldByID("lstAllRoles").focus();
	} else {
		ls4.removeAllFromListbox();
		var strSearchCriteria = getValue("selAgencies") + "#"
		+ getFieldByID("chkTAs").checked + "#"
		+ getFieldByID("chkCOs").checked + "#"
		+ ls.getSelectedDataWithGroup() + "#"
		+ ls2.getSelectedDataWithGroup() + "#"
		+ ls3.getSelectedDataWithGroup() + "#"
		+ ls4.getSelectedDataWithGroup() + "#"
		+ "false";
	top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);	
	
	if (ls.getSelectedDataWithGroup() == "") {
		setField("selAgencies", "");
	}	
	if ((ls.getSelectedDataWithGroup() == "") && (ls2.getSelectedDataWithGroup() == "")) {
		setField("selAgencies", "");
	}
	
	if ((ls.getSelectedDataWithGroup() != "") && (ls2.getSelectedDataWithGroup() != "")) {
		setField("hdnAgents", getAgents());
		setField("hdnUsers", getUsers());
	}
	setField("hdnRoles", getRoles());
	setField("hdnMode", "SEARCH");
	var objForm = document.getElementById("frmAgentGsaDetails");
	objForm.target = "_self";
	document.forms[0].submit();
	top[2].ShowProgress();		
	}
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}