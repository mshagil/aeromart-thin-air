var objWindow;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

setField("hdnLive", repLive);

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

var screenId = "UC_REPM_097";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}
function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
function detailViewClick(isSchedule){
	setField("hdnDetail",true);
	setField("hdnEntityText",getFieldByID("selEntity").options[getFieldByID("selEntity").selectedIndex].innerText);
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (getFieldByID("ccReportName") == null
			|| getText("ccReportName") == "") {
		showCommonError("Error", reportOptionNotSelected);
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", "Enter a valid TO date");
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
		&& dateValidDate(getText("txtToDate")) == true
		&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		if(isSchedule){
			scheduleReport();
		}else {
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmCCTransactionsLMSReport");
			objForm.target = "CWindow";
			objForm.action = "cCTransactionsLMSPointsReports.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}	
}
function viewClick(isSchedule) {
	setField("hdnDetail",false);
	setField("hdnEntityText",getFieldByID("selEntity").options[getFieldByID("selEntity").selectedIndex].innerText);
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (getFieldByID("ccReportName") == null
			|| getText("ccReportName") == "") {
		showCommonError("Error", reportOptionNotSelected);
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", "Enter a valid TO date");
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		if(isSchedule){
			scheduleReport();
		}else {
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmCCTransactionsLMSReport");
			objForm.target = "CWindow";
			objForm.action = "cCTransactionsLMSPointsReports.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}
}

function scheduleReport() {

UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmCCTransactionsLMSReport', 
		composerName: 'ccLmsTransactionsReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function pageLoad() {
	getFieldByID("txtFromDate").focus();

}
