var objWindow;
var screenID = "UC_REPM_001";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
setField("hdnLive", repLive);
function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	if (top[1].objTMenu.tabGetValue(screenID) != ""
			&& top[1].objTMenu.tabGetValue(screenID) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenID).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
	resSelect();
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}
function setGSA() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
	}
}
function viewClick() {

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true && checkGreat()
			&& validateAgents() && validateProductivity()) { // &&
																// validatePaymentMode()

		setField("hdnMode", "VIEW");
		setField("hdnAgents", getAgents());		
		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmReservation");
		objForm.target = "CWindow";

		objForm.action = "showReservationsReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function getAgentClick() {
	if (trim(getValue("selAgencies")) == '') {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
		return;
	}
	setField("hdnAgents", "");
	var strSearchCriteria = getValue("txtFromDate") + "#"
			+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
			+ getFieldByID("chkTAs").checked + "#"
			+ getFieldByID("chkCOs").checked;
	top[1].objTMenu.tabSetValue(screenID, strSearchCriteria);
	setField("hdnMode", "SEARCH");
	document.getElementById("frmReservation").target = "_self";
	document.forms[0].submit();
	top[2].ShowProgress();
}

function exportClick() {
	alert("Export Click");
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);

}

function FlightClick() {
	if (getFieldByID("chkSegment1").checked) {
		Disable('txtFlight', "");
		setField('selFrom', "");
		setField('selTo', "");
		Disable('selFrom', "false");
		Disable('selTo', "false");

	}
}
function checkGreat() {
	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (!CheckDates(repStartDate, getText("txtFromDate"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function segmentClick() {
	if (getFieldByID("chkSegment").checked) {
		setField('txtFlight', "");
		Disable('selFrom', "");
		Disable('selTo', "");
		Disable('txtFlight', "false");

	}
}
function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}
function validateAgents() {
	var validate = false;
	if (trim(ls.getselectedData()).length > 0) {
		validate = true;
	} else {
		showCommonError("Error", agentsRqrd);
	}
	return validate;
}

function validatePaymentMode() {
	var validate = false;
	if (trim(lspm.getselectedData()).length > 0) {
		validate = true;
	} else {
		showCommonError("Error", paymentmodesRqrd);
	}
	return validate;
}

function validateProductivity() {
	var validate = false;
	if (getFieldByID("chkOption2").checked) {
		if (getFieldByID("chkSegment").checked
				|| getFieldByID("chkSegment1").checked) {
			validate = true;
			if (getFieldByID("chkSegment").checked) {
				if (trim(getFieldByID("selFrom").value) == ""
						|| trim(getFieldByID("selFrom").value) == "Select") {
					validate = false;
					showCommonError("Error", segmentFromEmpty);
					getFieldByID("selFrom").focus();

				} else if (trim(getFieldByID("selTo").value) == "Select"
						|| trim(getFieldByID("selTo").value) == "") {
					validate = false;
					showCommonError("Error", segmentToEmpty);
					getFieldByID("selTo").focus();

				} else if (getFieldByID("selTo").value == getFieldByID("selFrom").value) {
					validate = false;
					showCommonError("Error", sameSegment);
					getFieldByID("selTo").focus();

				}
			} else if (getFieldByID("chkSegment1").checked) {
				if (trim(getFieldByID("txtFlight").value) == "") {
					validate = false;
					showCommonError("Error", flightNoEmpty);
					getFieldByID("txtFlight").focus();
				}
			}

		} else {
			showCommonError("Error", criterianull);
			getFieldByID("chkSegment").focus();
		}

	} else {
		validate = true;
	}
	return validate;
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}

	return newAgents;
}

function chkClick() {
	dataChanged();
}

function closeClick() {

	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)

}
function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function resSelect() {
	getFieldByID("chkSegment").checked = false;
	getFieldByID("chkSegment1").checked = false;
	setField('txtFlight', "");
	setField('selFrom', "");
	setField('selTo', "");
	Disable('selFrom', "false");
	Disable('selTo', "false");
	Disable('txtFlight', "false");
	Disable('chkSegment', "false");

}

function productSelect() {	
	Disable('chkSegment', "");

}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}