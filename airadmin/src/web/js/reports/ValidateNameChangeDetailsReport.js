var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "2":
		setField("txtDepFromDate", strDate);
		break;
	case "3":
		setField("txtDepToDate", strDate);
		break;
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_066";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function KPValidatePositiveInteger(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isPositiveInt(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}

}

function viewClick() {
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if ((getText("txtDepFromDate") != "" )&& dateValidDate(getText("txtDepFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtDepFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if ((getText("txtDepToDate") != "") && dateValidDate(getText("txtDepToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtDepToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if(getText("txtDepFromDate") != "" && getText("txtDepToDate") =="") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtDepToDate").focus();
	}

	else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true 
			&& checkGreat()) {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmNameChange");
		objForm.target = "CWindow";

		objForm.action = "showNameChangeDetailsReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function checkGreat() {
	var validate = false;
	if(!CheckDates(getText("txtFromDate"),getText("txtToDate"))) {
		validate = false;
		showCommonError("Error",fromDtExceed);
		getFieldByID("txtToDate").focus();
	}else if (!CheckDates(repStartDate,getText("txtFromDate"))) {
		showCommonError("Error",rptReriodExceeds +" "+ repStartDate);
		getFieldByID("txtFromDate").focus();			
	} else if(!CheckDates(getText("txtDepFromDate"),getText("txtDepToDate"))) {
		validate = false;
		showCommonError("Error",fromDtExceed);
		getFieldByID("txtDepToDate").focus();
	}
	else {
		validate = true;
	}
	return validate;
}

function onLoad() {
	getFieldByID("txtFromDate").focus();

}

