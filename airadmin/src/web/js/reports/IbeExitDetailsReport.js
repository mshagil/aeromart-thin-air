var objWindow;
var value;
setField("hdnLive",repLive);
	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = false;
	objCal1.buildCalendar();
	
function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtFromDate",strDate);break ;
		case "1" : setField("txtToDate",strDate);break ;
	}
}

function beforeUnload(){
	if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);	
	
}
var screenId="UC_REPM_086";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited){
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick(){	
	if(getText("txtFromDate")==""){
		showCommonError("Error","From date is blank");
		getFieldByID("txtFromDate").focus();
	}else if(getText("txtToDate")==""){
		showCommonError("Error","To date is blank");
		getFieldByID("txtToDate").focus();
	}  else if( validateReportDate("txtFromDate", "txtToDate", repStartDate)==true) {
			
		setField("hdnMode","VIEW");	
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);			
		var objForm  = document.getElementById("frmIbeExitDetailseport");
			objForm.target = "CWindow";
	
		objForm.action = "showIbeExitReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	
	}
}