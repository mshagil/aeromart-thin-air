var objWindow;
var value;
var screenId = "UC_REPM_081";
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "2":
		setField("txtBookedFromDate", strDate);
		break;
	case "3":
		setField("txtBookedToDate", strDate);
		break;

	}
}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		values += control.options[t].text + ",";
	}
	return values;
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarTo(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}


function winOnLoad() {
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {

		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");

		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selStatus", strSearch[2]);
		setField("txtBookedFromDate", strSearch[3]);
		setField("txtBookedToDate", strSearch[4]);

		setField("selDeparture", strSearch[5]);
		setField("selArrival", strSearch[6]);
		setSelectedListOptions("selFlightNo", strSearch[7]);
		setSelectedListOptions("selBCCat", strSearch[8]);
		setSelectedListOptions("selBCTyp", strSearch[9]);
		setSelectedListOptions("selAllocTyp", strSearch[10]);

		setSelectedListOptions("selFareRuls", strSearch[12]);
		setField("selAgencies", strSearch[18]);

		filterBookingClasses();
		setSelectedListOptions("selBCls", strSearch[11]);
		setField("selCOS", strSearch[21]);
		
		if(strSearch[22]!=null && strSearch[22]!=""){
			setAllSegments(strSearch[22]);	
		}
		
		/*
		 * var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		 * lsfrules.selectedData(strSearch[0]); setField("selFareRuleStatus",
		 * strSearch[1]); setField("selFareRuleWay", strSearch[2]);
		 * lsfvisibility.selectedData(strSearch[3]); setField("selAgencies",
		 * strSearch[4]);
		 * 
		 * if (strSearch[5] == 'true') { getFieldByID("chkTAs").checked = true; }
		 * else { getFieldByID("chkTAs").checked = false;
		 * if(getValue("selAgencies")!='GSA'){ Disable('chkTAs', true); } } if
		 * (strSearch[6] == 'true') { getFieldByID("chkCOs").checked = true; }
		 * else { getFieldByID("chkCOs").checked = false;
		 * if(getValue("selAgencies")!='GSA'){ Disable('chkCOs', true); } }
		 */

	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function viewClick() {

	var reportType = getValue("radReportType");		 	

	if ((reportType == "FLIGHT_WISE") && (getText("txtBookedFromDate") == "")) {
		showCommonError("Error", depFrmDtEmp);
		getFieldByID("txtBookedFromDate").focus();
	} else if ((getText("txtBookedFromDate") != "") && (dateValidDate(getText("txtBookedFromDate")) == false)) {
		showCommonError("Error", depFrmDtInvld);
		getFieldByID("txtBookedFromDate").focus();
	} else if ((reportType == "FLIGHT_WISE") && (getText("txtBookedToDate") == "")) {
		showCommonError("Error", depToDtEmp);
		getFieldByID("txtBookedToDate").focus();
	} else if ((getText("txtBookedToDate") != "") && (dateValidDate(getText("txtBookedToDate")) == false)) {
		showCommonError("Error", depToDtInvld);
		getFieldByID("txtBookedToDate").focus();
	} else if ((getText("txtFromDate") != "") && (dateValidDate(getText("txtFromDate")) == false)) {
		showCommonError("Error", slEffecFrmDtInvld);
		getFieldByID("txtFromDate").focus();
	} else if ((getText("txtToDate") != "") && (dateValidDate(getText("txtToDate")) == false)) {
		showCommonError("Error", slEffecToDtInvld);
		getFieldByID("txtToDate").focus();	
	} else if ((getText("txtBookedToDate") != "") && !CheckDates(getText("txtBookedFromDate"), getText("txtBookedToDate"))){		
		showCommonError("Error", depFrmDtGrt);
		getFieldByID("txtBookedToDate").focus();
	} else if ((getText("txtFromDate") != "") && !CheckDates(getText("txtFromDate"), getText("txtToDate"))){		
		showCommonError("Error", slEffecFrmDtGrt);
		getFieldByID("txtToDate").focus();	
	} else if (document.getElementById("selSegment").length < 1) {
		showCommonError("Error", ondEmpty);
		getFieldByID("selDeparture").focus();
	} else {
		
			setField("hdnMode", "VIEW");

			var seg = getAllSegments().substr(0, getAllSegments().length - 1);
			setField("hdnSegments", seg);
			setField("hdnAgents", getAgents());

			var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmFareDetailReport");
			objForm.target = "CWindow";
			objForm.action = "showFareDetailsReport.action";
			objForm.submit();
			top[2].HidePageMessage();				
	}
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenId)

}
function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenId))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function checkGreat() {
	var validate = false;
	if (!CheckDates(getText("txtBookedFromDate"), getText("txtBookedToDate"))) {
		validate = false;
		showCommonError("Error", depFrmDtGrt);
		getFieldByID("txtBookedToDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function checkGreatSalesDate() {
	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", slEffecFrmDtGrt);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function clickAgencies() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}

	return newAgents;
}

function changeAgencies() {
	ls.clear();
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		setSelectedList("selFlightNo", "hdnFlightNo");
		setSelectedList("selBCCat", "hdnBCCat");
		setSelectedList("selBCTyp", "hdnBCTyp");
		setSelectedList("selAllocTyp", "hdnAllocTyp");
		setSelectedList("selBCls", "hdnBCls");
		setSelectedList("selFareRuls", "hdnFareRuls");
		
		var selSegStr = getSelectedSegment();

		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selStatus") + "#"
				+ getValue("txtBookedFromDate") + "#"
				+ getValue("txtBookedToDate") + "#" + getValue("selDeparture")
				+ "#" + getValue("selArrival") + "#" + getValue("hdnFlightNo")
				+ "#" + getValue("hdnBCCat") + "#" + getValue("hdnBCTyp") + "#"
				+ getValue("hdnAllocTyp") + "#" + getValue("hdnBCls") + "#"
				+ getValue("hdnFareRuls") + "#" + getValue("selVia1") + "#"
				+ getValue("selVia2") + "#" + getValue("selVia3") + "#"
				+ getValue("selVia4") + "#" + getValue("selFlightNo") + "#"
				+ getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked+ "#"
				+ getValue("selCOS")+"#"
				+ selSegStr;
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnMode", "SEARCH");
		setField("hdnAgents", "");
		var objForm = document.getElementById("frmFareDetailReport");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");
	var arr = getValue("selArrival");
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRequired);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRequired);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(OnDExists);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}

}

function getSelectedSegment(){
	var control = document.getElementById("selSegment");

	var segmentStr='';
	var intCount = control.length;
	for ( var i = 0; i < intCount; i++) {
		segmentStr += control.options[i].value + ",";		
	}	
	
	return segmentStr;
}

function setAllSegments(segs) {
	var control = document.getElementById("selSegment");
	var selSegs = segs.split(",");

	for ( var i = 0; i < selSegs.length; i++) {
		if (trim(selSegs[i]) != "") {
			control.options[control.length] = new Option(selSegs[i], selSegs[i]);
		}
	}
}

function setSelectedList(fieldById, hidenFieldId) {
	var strVisibile = "";
	if (getFieldByID(fieldById)) {
		var intCount = getFieldByID(fieldById).length;
		for ( var i = 0; i < intCount; i++) {
			if (getFieldByID(fieldById).options[i].selected) {
				strVisibile += getFieldByID(fieldById).options[i].value + ",";
			}
		}
	}

	setField(hidenFieldId, strVisibile);
}

function setSelectedListOptionValue(fieldById, selectedCode) {
	if (getFieldByID(fieldById)) {
		var intCount = getFieldByID(fieldById).length;
		for ( var i = 0; i < intCount; i++) {
			if (trim(getFieldByID(fieldById).options[i].value) == trim(selectedCode)) {
				getFieldByID(fieldById).options[i].selected = true;
			}
		}
	}
}

function setSelectedListOptions(fieldById, selectedCodesStr) {
	var arrCarriers = selectedCodesStr.split(",");
	for ( var i = 0; i < arrCarriers.length; i++) {
		setSelectedListOptionValue(fieldById, arrCarriers[i]);
	}
}

function filterBookingClasses(){
// NORMAL,N,N,COM;AY^NORMAL,N,N,COM;A6^
// bc_type,standard_code,fixed_flag,allocation_type;booking_code^

	getFieldByID("selBCls").options.length = 0;

	appendOptionLast("All","All");

	setSelectedList("selBCCat", "hdnBCCat");
	setSelectedList("selBCTyp", "hdnBCTyp");
	setSelectedList("selAllocTyp", "hdnAllocTyp");

	var selectedBcCat = getValue("hdnBCCat");
	var selectedBcTyp = getValue("hdnBCTyp");
	var selectedAllocTyp = getValue("hdnAllocTyp");

	var bClsCatArray = selectedBcCat.split(",");
	var bClsTypeArray = selectedBcTyp.split(",");
	var allocTypeArray = selectedAllocTyp.split(",");

	var allBClsList= getValue("hdnAllBCls");
	var bClsFullArray = allBClsList.split("^");

	for(var i=0;i<bClsFullArray.length;i++){
		remove = false;
		// NORMAL,N,N,COM;AY
		var bClsCodeArray = bClsFullArray[i].split(";");
		// NORMAL,N,N,COM - 0
		// AY - 1
		var bClsCodeParamArray = bClsCodeArray[0].split("|");
		var bclsTypeTemp = bClsCodeParamArray[0];
		var stndCodeTemp = bClsCodeParamArray[1];
		var fixFlagTemp = bClsCodeParamArray[2];
		var allocTypeTemp = bClsCodeParamArray[3];
		
		if(!inArray(bClsTypeArray,"All") && bClsTypeArray.length > 1){
			if(!inArray(bClsTypeArray,bclsTypeTemp)){
				// appendOptionLast(bClsCodeArray[1],bClsFullArray[i]);
				remove = true;	   		
			}			
		}

		if(!inArray(allocTypeArray,"All") && allocTypeArray.length > 1){
			if(!inArray(allocTypeArray,allocTypeTemp)){
				// appendOptionLast(bClsCodeArray[1],bClsFullArray[i]);
				remove = true;				   				   		
			}
		}

		if(!inArray(bClsCatArray,"All") && bClsCatArray.length > 1){
			if(inArray(bClsCatArray,"Fixed") && fixFlagTemp!="Y"){
				remove = true;		
			}
			
			if(inArray(bClsCatArray,"Non-Standard") && inArray(bClsCatArray,"Standard")){
				// nothing to do
			}else if(inArray(bClsCatArray,"Non-Standard") && stndCodeTemp!="N"){
				remove = true;	
			}else if(inArray(bClsCatArray,"Standard") && stndCodeTemp!="Y"){
				remove = true;
			}
		}

		if(!remove){
			appendOptionLast(bClsCodeArray[1],bClsFullArray[i]);
		}	

			
	}

	if(getFieldByID("selBCls").options.length == 1){
		getFieldByID("selBCls").options.length = 0;
	}
	
}

function inArray(a, obj){
  for(var i = 0; i < a.length; i++) {
    if(a[i] === obj){
      return true;
    }
  }
  return false;
}

function disableOptionValue(fieldById, selectedCode,flag) {
	if (getFieldByID(fieldById)) {
		var intCount = getFieldByID(fieldById).length;
		for ( var i = 0; i < intCount; i++) {
			if (trim(getFieldByID(fieldById).options[i].value) == trim(selectedCode)) {
				if(flag){
					getFieldByID(fieldById).options[i].disabled = "disabled";				
				}else{
					getFieldByID(fieldById).options[i].disabled = false;
				}
				
				// getFieldByID(fieldById).remove(i);
				// alert("Removed ");
			}
		}
	}
}


function appendOptionLast(textP,valueP)
{
  var elOptNew = document.createElement('option');
  elOptNew.text = textP;
  elOptNew.value = valueP;
  var elSel = document.getElementById('selBCls');

  try {
    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
  }
  catch(ex) {
    elSel.add(elOptNew); // IE only
  }
}

function removeOptionByCode(fieldById,selectedCode) {
	if (getFieldByID(fieldById)) {
		var intCount = getFieldByID(fieldById).length;
		for ( var i = 0; i < intCount; i++) {
			if (trim(getFieldByID(fieldById).options[i].value) == trim(selectedCode)) {							
				getFieldByID(fieldById).remove(i);
			}
		}
	}
}

function ClearOptionsFast(id){
	var selectObj = document.getElementById(id);
	var selectParentNode = selectObj.parentNode;
	var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
	selectParentNode.replaceChild(newSelectObj, selectObj);
	return newSelectObj;
}

/**
 *following function can be used when limit the maximum no selection
 */
function maxSelector(fieldById) {

	var selectedOption = 0;
	var maxOptions = 5;
    var selObj = getFieldByID(fieldById);
	//var selIndex = selObj.selectedIndex; 

	if (selObj) {
		var intCount = selObj.length;
		for ( var i = 0; i < intCount; i++) {
			
			if((selObj.options[i].value == "All") && (selObj.options[i].selected==true)) {	
				selObj.selectedIndex = -1;						
				selObj.options[i].selected = true;	
				break;
			}else if ((selObj.options[i].value != "All") && (selObj.options[i].selected==true)) {							
				selectedOption++;
				if(selectedOption > maxOptions){
					selObj.options[i].selected = false;
					alert("Maximum allowed selection "+maxOptions+'\nTo select More than '+maxOptions+' select All');				
				}
			}
		}
	}		
      
}
