var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_028";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick() {

	if (getText("txtFromDate") == "") {
		alert("dfd");
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();

	} else if ((getFieldByID("radAgentOptionAgent").checked)
			&& trim(getFieldByID("selagent").value) == "") {
		showCommonError("Error", selectedAgency);
		getFieldByID("selagent").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {

		if (getFieldByID("radAgentOptionAgent").checked) {
			setField("hdnMode", "DETAIL");
		} else {
			setField("hdnMode", "VIEW");
		}

		var agentTypeCode = trim(getText("selagent"));
		setField("hdnAgentTypeCode", agentTypeCode);

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAgentStatementReport");
		objForm.target = "CWindow";

		objForm.action = "showAgentStatementReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function pageLoadOB() {
	getFieldByID("txtFromDate").focus();
	Disable("selagent", true);

}

function chkClick(strSegmentFlight) {

}

function agentsClick() {

	if (getFieldByID("radAgentOptionALL").checked) {
		setField("selagent", "");
		Disable("selagent", true);
	} else if (getFieldByID("radAgentOptionAgent").checked) {
		Disable("selagent", false);

	}
}