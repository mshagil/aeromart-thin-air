var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

var screenId = "UC_REPM_013";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function KPValidatePositiveInteger(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isPositiveInt(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}

}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick() {
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (validateTextField(trim(getValue("salesChanels")), saleChannelEmpty)) {
//		?showCommonError("Error", "Sales Channel canot be empty");
		getFieldByID("salesChanels").focus();
	} else if (getFieldByID("radNoTopAgentsOther").checked == true
			&& getText("txtTopAgentsOther") == "") {
		showCommonError("Error", noOfAgentsRqrd);
		getFieldByID("txtTopAgentsOther").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmTopSegments");
		objForm.target = "CWindow";
		objForm.action = "showTopSectorReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function chkClick() {
	if (getFieldByID("radNoTopAgentsOther").checked == true) {
		Disable("txtTopAgentsOther", false);
		getFieldByID("txtTopAgentsOther").focus();
	} else {
		setField("txtTopAgentsOther", "");
		Disable("txtTopAgentsOther", true);
	}
}

function pageLoadTS() {
	getFieldByID("txtFromDate").focus();
	Disable("txtTopAgentsOther", true);
}

function validateDecimal() {
	setPageEdited(true);
	objOnFocus();
	var strCR = getText("txtTopAgentsOther");
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 13, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 13) {
				setField("txtTopAgentsOther", strCR.substr(0,
						wholeNumber.length - 1));
			} else {
				setField("txtTopAgentsOther", strCR.substr(0, strLen - 1));
			}
		} else {
			setField("txtTopAgentsOther", strCR.substr(0, strLen - 1));
		}
		getFieldByID("txtTopAgentsOther").focus();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function objOnFocus() {
	top[2].HidePageMessage();
}