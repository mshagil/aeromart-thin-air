var objWindow;
setField("hdnLive",repLive);
function setPageEdited(isEdited){
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

var screenId="UC_REPM_029";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function beforeUnload(){
		if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
			top[0].objWindow.close();
		}
}
function stdValidation(){
	
	
	var strBccat = bccat.getselectedData();
	var arrBccat = [];
	
		arrBccat = strBccat.split(",");
	
	
	var standard = false;
	var nonStandard = false;
	var validate = false;
	var i;
	
	
	for(i=0;i<arrBccat.length;i++){
	
	if(arrBccat[i] == "Standard"){
	standard = true;
	
		}
	else if(arrBccat[i] == "Non-Standard"){
	nonStandard = true;
	
		}
	}
		
	if(standard === true && nonStandard === true){
	showCommonError("Error",maxBcCategory);
	
	}else{
	validate = true;
	}
	return validate;
}

function winOnLoad() {
	if(top.logicalCCVisibility == "false")
		document.getElementById('logicalCC').style.visibility='hidden';
	
	getFieldByID("selCOS").focus();
	if(top[1].objTMenu.tabGetValue(screenId) != "" && top[1].objTMenu.tabGetValue(screenId) != null)
	{
		var strSearch=top[1].objTMenu.tabGetValue(screenId).split("#");

		setField("selCOS",strSearch[0]);
		if(strSearch[3]!= null && strSearch[3]!= ""){
			setSelectedDataWithGroup(strSearch[3]);
			bcls.selectedData(strSearch[4]);
		}
		bccat.selectedData(strSearch[5])
	}
}


function viewClick(){	
	
	setField("hdnBCs",bcls.getselectedData());
	setField("hdnBCCat",bccat.getselectedData())
	
	//if(stdValidation()){
			
		setField("hdnMode","VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);
		var objForm  = document.getElementById("frmBookingClassReport");
		objForm.target = "CWindow";
		objForm.action = "showBookingClassReport.action";
		objForm.submit();
		top[2].HidePageMessage();
		//}
	

}

function settingValidation(cntfield){
	if (!dateChk(cntfield))	{
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function filterBookingClass(selValue, filterBy){
	if(filterBy == 'cos'){
		var lccObj = getFieldByID('selLogicalCC');
		var lccValue = lccObj.options[lccObj.selectedIndex].value;
		if(selValue == '' && lccValue == ''){
			rebuildBCList(bcs);
		} else if(selValue != ''){
			for (var i=0;i<lccObj.options.length;i++) {
			    if (lccObj.options[i].value == ''){			    	
			    	lccObj.options[i].selected = true;
			    	filterBookingClass('', 'lcc');
			    }
			}
		}
	} else if(filterBy == 'lcc'){
		var cosObj = getFieldByID('selCOS');
		var cosValue = cosObj.options[cosObj.selectedIndex].value;
		
		if(selValue == '' && cosValue == ''){
			rebuildBCList(bcs);
		} else if(selValue == '' && cosValue != '') {
			rebuildBCList(getFilterBCList(2, cosValue));
		} else if(selValue != ''){
			rebuildBCList(getFilterBCList(3, selValue));
		}
	}
}

function getFilterBCList(index, cos){
	var filterArr = new Array();
	var count=0;
	for(var i=0;i<bcs.length;i++){
		var bcsObj = bcs[i];
		if(bcsObj[index] == cos){
			filterArr[count] = bcsObj;
			count++;
		}
	}
	
	return filterArr;
}

function rebuildBCList(bcs){
	arrData[0] = bcs;
	arrData[1] = new Array();
	var arrGroup2 = new Array();
	var arrData2 = new Array();
	arrData2[0] = new Array();
	
	if(bcls){				
		bcls.group1 = arrData[0];
		bcls.list2 = arrData2;
		bcls.drawListBox();
	}
}