var screenId = "UC_REPM_074";
var objWindow;
var value;

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function winOnLoad() {
	
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		lsfrules.selectedData(strSearch[0]);
		setField("selFareRuleStatus", strSearch[1]);
		setField("selFareRuleWay", strSearch[2]);
		lsfvisibility.selectedData(strSearch[3]);
		setField("selAgencies", strSearch[4]);
		
		if (strSearch[5] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			if(getValue("selAgencies")!='GSA'){
				Disable('chkTAs', true);
			}
		}
		if (strSearch[6] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			if(getValue("selAgencies")!='GSA'){
				Disable('chkCOs', true);
			}
		}
		
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function viewClick() {
	if (lsfrules.getselectedData().length == 0) {		
		showCommonError("Error", fareclassRqrd);
		getFieldByID("lstFReules").focus();		
	} else {
		setField("hdnMode", "VIEW");	
		var fareRules = trim(lsfrules.getselectedData());
		var fareVisibilities = trim(lsfvisibility.getselectedData());		
		var fareAgents = trim(ls.getselectedData());
		
		setField("hdnFareRules", fareRules);
		setField("hdnFareRuleVisibility", fareVisibilities);
		setField("hdnFareAgents", fareAgents);		
		setField("hdnLive", repLive);
		
		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmFareRuleDetails");
		objForm.target = "CWindow";
		objForm.action = "showFareRuleDetailsReport.action";		
		objForm.submit();
		
		top[2].HidePageMessage();
	}
}

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function chkClick() {
	dataChanged();
}


function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenId))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}	

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {						
	ls.clear();
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria =
				lsfrules.getselectedData() + "#"
				+ getValue("selFareRuleStatus") + "#"
				+ getValue("selFareRuleWay") + "#"
				+ lsfvisibility.getselectedData() + "#"
				+ getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked;		
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnMode", "SEARCH");
		setField("hdnAgents", "");		
		var objForm = document.getElementById("frmFareRuleDetails");
		objForm.target = "_self";		
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}
