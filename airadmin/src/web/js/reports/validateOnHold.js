var objWindow;
setField("hdnLive", repLive);
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;		
	case "2":
		setField("txtFromDepDate", strDate);
		break;
	case "3":
		setField("txtToDepDate", strDate);
		break;
	case "4": 
		setField("txtBookFromDate", strDate);
		break;
	case "5": 
		setField("txtBookToDate", strDate);
		break;	
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_005";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function radioReportChanged(){
	if(getValue("radOption")=="DETAIL"){
		setField("hdnRptType","DETAIL");
	}else {
		setField("hdnRptType","SUMMARY");
	}
}

function viewClick() {
	
	if(getValue("radOption")=="DETAIL"){
		setField("hdnRptType","DETAIL");
	}else {
		setField("hdnRptType","SUMMARY");
	}

	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}
	setField("hdnUsers", getUsers());
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if ((getText("txtFromDepDate") != "") && dateValidDate(getText("txtFromDepDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDepDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if ((getText("txtToDepDate") != "") && dateValidDate(getText("txtToDepDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDepDate").focus();
	}else if ((getText("txtBookFromDate") != "") && (dateValidDate(getText("txtBookFromDate")) == false)) {
		showCommonError("Error",bookedFromDtInvalid);
		getFieldByID("txtBookFromDate").focus();
	} else if ((getText("txtBookFromDate") != "") && (getText("txtBookToDate") == "")) {			
		showCommonError("Error",bookedToDtInvalid);
		getFieldByID("txtBookFromDate").focus();
	} else if ((getText("txtBookToDate") != "") && (getText("txtBookFromDate") == "")) {			
		showCommonError("Error",bookedFromDtInvalid);
		getFieldByID("txtBookFromDate").focus();		
	} else if ((getText("txtBookToDate") != "") && (dateValidDate(getText("txtBookToDate")) == false)) {
		showCommonError("Error",bookedToDtInvalid);
		getFieldByID("txtBookToDate").focus();
	} else if ((getText("txtBookToDate") != "") && !CheckDates(getText("txtBookFromDate"), getText("txtBookToDate"))){		
		showCommonError("Error",bookDatePerInvld);
		getFieldByID("txtBookToDate").focus();
	} else if((getText("txtBookFromDate") != "") && (!checkWithSysDate("txtBookFromDate"))){				
		showCommonError("Error",fromDtExceedsToday);
		getFieldByID("txtBookFromDate").focus();
	} else if((getText("txtBookToDate") != "") && (!checkWithSysDate("txtBookToDate"))){		
		showCommonError("Error",toDtExceedsToday);		
		getFieldByID("txtBookToDate").focus();	
	} else if ((getText("txtFlightNumber") != "") &&  (!isFlightNoValid("txtFlightNumber"))) {
		showCommonError("Error",flightNoInvalid);
		getFieldByID("txtFlightNumber").focus();	
	} else if(getText("selBookCurStatus") == "") { 
		showCommonError("Error",bookStatusReq);
		getFieldByID("selBookCurStatus").focus();
	} else if (displayAgencyMode == 1 && getText("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();

	} else if ((displayAgencyMode == 1 || displayAgencyMode == 2)
			&& getText("hdnAgents") == "") {
		showCommonError("Error", agentsRqrd);
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {

		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "CWindow";

		objForm.action = "onHoldPassengersReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function load() {
	getFieldByID("txtFromDate").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		setField("txtFlightNumber", strSearch[5]);
		setField("selBookCurStatus", strSearch[11]);
		
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		setField("txtFromDepDate", strSearch[6]);
		setField("txtToDepDate", strSearch[7]);
		if(strSearch[8] == 'true'){
			getFieldByID("radOption").checked = true;
		}else {
			getFieldByID("radOptionSummary").checked = true;
		}
		if (strSearch[9] != null && strSearch[9] != "") {
			if(ls.getNotSelectedDataWithGroup() == strSearch[9]){
                setSelectedDataWithGroup(strSearch[9],ls,true);
			}
			else{
                setSelectedDataWithGroup(strSearch[9],ls,false);                        
			}
		}
		
		if (strSearch[10] != null && strSearch[10] != "") {
			setSelectedDataWithGroup(strSearch[10],ls2);
		}
		
		setField("txtBookFromDate", strSearch[13]);
		setField("txtBookToDate", strSearch[14]);
		
		if (strSearch[12] == 'true') {
			getFieldByID("chkExtOnhold").checked = true;
		}else {
			getFieldByID("chkExtOnhold").checked = false;
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function radioOptionChanged() {

	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");
	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");
	}
}

function clickAgencies() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}
function getUsers() {
	var strUsers = ls2.getselectedData();
	var newUsers;
	if (strUsers.indexOf(":") != -1) {
		strUsers = replaceall(strUsers, ":", ",");
	}
	if (strUsers.indexOf("|") != -1) {
		newUsers = replaceall(strUsers, "|", ",");
	} else {
		newUsers = strUsers;
	}
	return newUsers;
}
function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ getValue("txtFlightNumber") + "#" 
				+ getValue("txtFromDepDate") + "#"
				+ getValue("txtToDepDate")+ "#"
				+ getFieldByID("radOption").checked+ "#"
				+"#"
				+"#"
				+ getValue("selBookCurStatus")+"#"
				+getFieldByID("chkExtOnhold").checked+"#"
				+ getValue("txtBookFromDate") + "#" + getValue("txtBookToDate");
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function loadUsersClick() {

	var agents = getAgents();

	if (displayAgencyMode == 1  && getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else if (agents == undefined || agents == null || agents == "") {
		showCommonError("Error", agentsRqrd);
		getFieldByID("lstRoles").focus();
	} else {
		ls2.removeAllFromListbox();		
		
		var strSearchCriteria = getValue("txtFromDate") + "#"
		+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
		+ getFieldByID("chkTAs").checked + "#"
		+ getFieldByID("chkCOs").checked + "#"
		+ getValue("txtFlightNumber") + "#" 
		+ getValue("txtFromDepDate") + "#"
		+ getValue("txtToDepDate")+ "#"
		+ getFieldByID("radOption").checked + "#"
		+ ls.getSelectedDataWithGroup() + "#"
		+ ls2.getSelectedDataWithGroup() + "#"
		+ getValue("selBookCurStatus")+"#"
		+getFieldByID("chkExtOnhold").checked+"#"
		+ getValue("txtBookFromDate") + "#" + getValue("txtBookToDate");
		
		
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", getAgents());
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function setSelectedDataWithGroup(strData,listBoxID,isAll) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();
	
	if(isAll){
        listBoxID.moveOptions('>>');
    }
    else{
    	for ( var x = 0; x < groups.length; x++) {

			if (groups[x].indexOf(":") != -1) {
				var dd = groups[x].split(":");
				groupList = dd[1].split(",");

				for ( var z = 0; z < groupList.length; z++) {
					if (str == "") {
						str += dd[0] + ":" + groupList[z];
					} else {
						str += "," + dd[0] + ":" + groupList[z];
					}
				}
			} else {
				if (str == "") {
					str += groups[x];
				} else {
					str += "," + groups[x];
				}
			}
		}
    	listBoxID.move('>', str);
    }
}

function isFlightNoValid(txtfield) {
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}

function checkWithSysDate(strdate){
	
	var dateValue = getText(strdate);
	var tempDate = dateValue;
	
	tempDay=tempDate.substring(0,2);
	tempMonth=tempDate.substring(3,5);
	tempYear=tempDate.substring(6,10); 	
	var tempODate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
			
	if(tempODate > strSysODate){		
		return false;
	} else{		
		return true;
	}
}