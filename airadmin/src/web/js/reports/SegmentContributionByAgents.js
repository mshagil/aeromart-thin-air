var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "2":
		setField("txtFlightFromDate", strDate);
		break;
	case "3":
		setField("txtFlightToDate", strDate);
		break;
	}
}
function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}
function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_014";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
function validateDate(txtFromDate,txtToDate) {
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;

	var dateFrom = getText(txtFromDate);
	var dateTo = getText(txtToDate);

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;

	tempDay = tempIStartDate.substring(0, 2);
	tempMonth = tempIStartDate.substring(3, 5);
	tempYear = tempIStartDate.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = tempIEndDate.substring(0, 2);
	tempMonth = tempIEndDate.substring(3, 5);
	tempYear = tempIEndDate.substring(6, 10);
	var tempOEndDate = (tempYear + tempMonth + tempDay);

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM;
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD;
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);

	strSysODate = (tempYear + tempMonth + tempDay);
	
	if (dateFrom == "" && dateTo != "" ){
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID(txtFromDate).focus();
		
	}else if (dateTo == "" && dateFrom != "" ){
			showERRMessage(arrError["toDtEmpty"]);
			getFieldByID(txtToDate).focus();
			
	}else if (tempOStartDate > tempOEndDate) {
		showERRMessage(arrError["fromDtExceed"]);
		getFieldByID(txtToDate).focus();

	} else {
		validate = true;
	}
	return validate;
}

function viewClick() {

    var selectedSegments = convertToStrArray (document.getElementById("segmentsSelected").options);
    var selectedPOSs = convertToStrArray(document.getElementById("selectedPOS").options);
    var selectedCountries = convertToStrArray (document.getElementById("selectedCntries").options);
    	
    if ((getText("txtFromDate") == "") && (getText("txtFlightFromDate") == "")) {
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
		return;

    } else if ( (getText("txtFromDate") != "") && dateValidDate(getText("txtFromDate")) == false ) {
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
		return;
		
    } else if ( (getText("txtFlightFromDate") != "") && dateValidDate(getText("txtFlightFromDate")) == false){
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFlightFromDate").focus();
		return;
		
    } else if ((getText("txtToDate") == "" ) && (getText("txtFlightToDate") == "")) {
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
		return;
		
    } else if ((getText("txtToDate") != "") && dateValidDate(getText("txtToDate")) == false ){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();
		return;
		
	} else if((getText("txtFlightToDate") != "")  && dateValidDate(getText("txtFlightToDate")) == false){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtFlightToDate").focus();
		return;
		
//	}else if (getText("txtFromDate") == "") && dateTo != "" ){
//		showERRMessage(arrError["fromDtEmpty"]);
//		getFieldByID(txtFromDate).focus();
//		return;	
//		
//	}else if (dateTo == "" && dateFrom != "" ){
//		showERRMessage(arrError["toDtEmpty"]);
//		getFieldByID(txtToDate).focus();
//		return;	
//		
//	}else if (dateFrom == "" && dateTo != "" ){
//		showERRMessage(arrError["fromDtEmpty"]);
//		getFieldByID(txtFromDate).focus();
//		return;	
//		
//	}else if (dateTo == "" && dateFrom != "" ){
//		showERRMessage(arrError["toDtEmpty"]);
//		getFieldByID(txtToDate).focus();
//		return;	
	
	}else if (!((getText("txtFromDate") != "") && (getText("txtToDate") != "")) 
		&& !((getText("txtFlightFromDate") != "") && (getText("txtFlightToDate") != ""))) {
		showERRMessage(arrError["atleastOneDateSetRequired"]);
		getFieldByID("txtFromDate").focus();
		return;
		
	} else if((selectedSegments == "" || selectedSegments.length == 0) && getText("txtFlightNo") == "" ){
		showCommonError("Error", segmentsNotSelected);
		getFieldByID("selDepature").focus();
	} else if ( selectedSegments.length > 0 && getText("txtFlightNo") != "" ){
		showCommonError("Error", segmetntAndFlightNumCannotSelected);
		getFieldByID("txtFlightNo").focus();
	} else if(getFieldByID("radCountry").checked == true && selectedCountries.length == 0){
		showCommonError("Error", countryEmpty);
		getFieldByID("btnCountries").focus();
	}else if(getFieldByID("radPOS").checked == true && selectedPOSs.length == 0){
		showCommonError("Error", stationEmpty);
		getFieldByID("btnPOS").focus();
	}else if ( validateDate(("txtFromDate"),("txtToDate"))
			&& validateDate(("txtFlightFromDate"),("txtFlightToDate"))) {

		setField("hdnMode", "VIEW");
		setField("hdnCountry", selectedCountries.toString());
		setField("hdnPOSs", selectedPOSs.toString());
		setField("hdnSegments", selectedSegments.toString());

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmSegmentContributionByAgents");
		objForm.target = "CWindow";

		objForm.action = "showSectorContributionByAgent.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function pageLoadSC() {
	getFieldByID("txtFromDate").focus();
	showPopupData();
	setTimeout('loadPOSList()', 2000);
	setTimeout('loadCountryList()', 2000);
	Disable("btnPOS", true);	
}

function chkClick(strSegmentFlight) {
	if (getFieldByID("radCountry").checked == true) {
		Disable("btnCountries", false);
		Disable("btnPOS", true);
		clearListBox("selectedPOS");
		
	} else if(getFieldByID("radPOS").checked == true){
		Disable("btnCountries", true) ;
		Disable("btnPOS", false);
		clearListBox("selectedCntries");
	}
}

function changeCase(varFlightNo) {
	setField("txtFlightNo", varFlightNo.value.toUpperCase());
}

function showPopupData() {

	 var htmlPOSList = '';
	 htmlPOSList += '<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0" style="background-color: #5A5C63;">';
	 htmlPOSList += '<tr><td>';
	 htmlPOSList += '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	 htmlPOSList += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	 htmlPOSList += '<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	 htmlPOSList += 'Select POS';
	 htmlPOSList += '</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
	 htmlPOSList += '</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	 htmlPOSList += '<table><tr><td align="left">';
	 htmlPOSList += '</td></tr>';
	 htmlPOSList += '<tr><td align="top"><span id="spnSta"></span></td></tr>';
	 htmlPOSList += '<tr><td align="right"><input type="button" id="btnInsertPOS" value= "Insert" class="Button" onClick="return insertSelectedPOS();">&nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return showPOSMultiSelect(false);"></td></tr></table>';
	 htmlPOSList += '</td><td class="FormBackGround"></td></tr><tr>';
	 htmlPOSList += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
	 htmlPOSList += '<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>';
	 htmlPOSList += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>';
	 htmlPOSList += '</tr></table>';
	 htmlPOSList += '</td></tr></table>';

DivWrite("POS", htmlPOSList);

var htmlCountryList = '';
	htmlCountryList += '<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0" style="background-color: #5A5C63;">';
	htmlCountryList += '<tr><td>';
	htmlCountryList += '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	htmlCountryList += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	htmlCountryList += '<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	htmlCountryList += 'Select Countries';
	htmlCountryList += '</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
	htmlCountryList += '</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	htmlCountryList += '<table><tr><td align="left">';
	htmlCountryList += '</td></tr>';
	htmlCountryList += '<tr><td align="top"><span id="spnCountrList"></span></td></tr>';
	htmlCountryList += '<tr><td align="right"><input type="button" id="btnInsertCountry" value= "Insert" class="Button" onClick="return insertSelectedCountries();">&nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return showCountryMultiSelect(false);"></td></tr></table>';
	htmlCountryList += '</td><td class="FormBackGround"></td></tr><tr>';
	htmlCountryList += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
	htmlCountryList += '<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>';
	htmlCountryList += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>';
	htmlCountryList += '</tr></table>';
	htmlCountryList += '</td></tr></table>';

DivWrite("Country", htmlCountryList);

}

function showPOSMultiSelect(show){
	setVisible("POS", show);
}

function loadPOSList(){
	lssta.drawListBox();
}

function showCountryMultiSelect(show){
	setVisible("Country", show);
}

function loadCountryList(){
	countryListBox.drawListBox();
}

function insertSelectedCountries(){	
	clearListBox("selectedCntries");
	var control = document.getElementById("selectedCntries");
	if (countryListBox.getselectedData() != "" && countryListBox.getselectedData() != null) {
		var countries = (countryListBox.getselectedData()).split(",");
		if (countries != "" && countries.length > 0) {
			for ( var i = 0; i < countries.length; i++) {
				control.options[i] = new Option(countries[i], countries[i]);
			}
		}
	}	
	showCountryMultiSelect(false);
}

function insertSelectedPOS() {
	clearListBox("selectedPOS");
	var control = document.getElementById("selectedPOS");
	if (lssta.getselectedData() != "" && lssta.getselectedData() != null) {
		var POSs = (lssta.getselectedData()).split(",");
		if (POSs != "" && POSs.length > 0) {
			for ( var i = 0; i < POSs.length; i++) {
				control.options[i] = new Option(POSs[i], POSs[i]);
			}
		}
	}
	showPOSMultiSelect(false);
}

function clearListBox(listBox) {
	var control = document.getElementById(listBox);
	control.options.length = 0;
	for ( var t = 0; t <= (control.length); t++) {
		control.options[t] = null;
	}
}

function fillCurrentlySelectedCountries(){	
	var control = document.getElementById("selectedCntries");
	var selectedCountries = control.options;
	if(selectedCountries != "" && selectedCountries != null){
		countryListBox.selectedData(selectedCountries);
	} 	 
}

function fillCurrentleSelectedPOS(){
	var control = document.getElementById("selectedPOS");
	var selectedPOSs = control.options;
	if(selectedPOSs != "" && selectedPOSs != null){
		countryListBox.selectedData(selectedPOSs);
	}
}

function btnAddSegment_click() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getText("selDepature");
	var arr = getText("selArrival");
	var via1 = getText("selVia1");
	var via2 = getText("selVia2");
	var via3 = getText("selVia3");
	var via4 = getText("selVia4");

	var deptVal = getValue("selDepature");
	var arrVal = getValue("selArrival");
	var via1Val = getValue("selVia1");
	var via2Val = getValue("selVia2");
	var via3Val = getValue("selVia3");
	var via4Val = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRqrd);
		getFieldByID("selDepature").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage(departureInactive);
		getFieldByID("selDepature").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRqrd);
		getFieldByID("selArrival").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage(arrivalInactive);
		getFieldByID("selArrival").focus();

	} else if (dept == arr && dept != "All") {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (dept == arr && dept == "All" && via1 == "") {
		showERRMessage(viaPointRqrd);
		getFieldByID("selVia1").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDepature").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage(viaEqual);
		getFieldByID("selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + viaInactive);
		getFieldByID("selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + viaInactive);
		getFieldByID("selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + viaInactive);
		getFieldByID("selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + viaInactive);
		getFieldByID("selVia4").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}
		str += "/" + arr;

		var control = document.getElementById("segmentsSelected");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(ondRequired);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
		}
	}
}

function btnRemoveSegment_click() {
	var control = document.getElementById("segmentsSelected");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
		}
	}

}

function convertToStrArray(selectedItemsFromListBox){
	var selectedItems = new Array();
	if(selectedItemsFromListBox != "" && selectedItemsFromListBox != null){
		for(i = 0; i < selectedItemsFromListBox.length; i++ ){
			   selectedItems[i] = selectedItemsFromListBox[i].text;
		  }
	}	
	return selectedItems;	   
}




