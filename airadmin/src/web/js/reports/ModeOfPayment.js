var objWindow;
var screenId = "UC_REPM_002";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
setField("hdnLive", repLive);

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();
}

function radioReportChanged() {
	if (getValue("chkOption") == "PM") {
		setField("hdnRptType", "SUMMARY_PM");
	} else if (getValue("chkOption") == "AC") {
		setField("hdnRptType", "SUMMARY_AC");
	} else if (getValue("chkOption") == "AN") {
		setField("hdnRptType", "SUMMARY_AN"); // mode of payment Agent Name
	} else if (getValue("chkOption") == "EP") {
		setField("hdnRptType", "SUMMARY_EXTERNAL");
	} else if (getValue("chkOption") == "BC") {
		setField("hdnRptType", "CURRENCY_SUMMARY");
	}
}

function zuluClick() {
	setField("hdnTime", "ZULU");
}

function localClick() {
	setField("hdnTime", "LOCAL");
}

function viewClick(isSchedule) {
	if (getValue("chkOption") == "PM") {
		setField("hdnRptType", "SUMMARY_PM");
	} else if (getValue("chkOption") == "AC") {
		setField("hdnRptType", "SUMMARY_AC");
	} else if (getValue("chkOption") == "AN") {
		setField("hdnRptType", "SUMMARY_AN");
	} else if (getValue("chkOption") == "EP") {
		setField("hdnRptType", "SUMMARY_EXTERNAL");
	} else if (getValue("chkOption") == "BC") {
		setField("hdnRptType", "CURRENCY_SUMMARY");
	}

	if (getValue("chkZulu") == "ZULU") {
		setField("hdnTime", "ZULU");
	} else {
		setField("hdnTime", "LOCAL");
	}
	setField("hdnPayments", lspm.getselectedData());
	setField("hdnEntity", getFieldByID("selEntity").options[getFieldByID("selEntity").selectedIndex].innerText);
	
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (lspm.getselectedData() == "" && getValue("chkOption") != "EP") {
		showCommonError("Error", paymentmodesRqrd);
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		if(isSchedule) {
			scheduleReport();
		} else {
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmPage");
			objForm.target = "CWindow";
			objForm.action = "showModeOfPayments.action";
			objForm.submit();
			top[2].HidePageMessage();
		}

	}

}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmPage', 
		composerName: 'modeofPaymentRpt',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function objOnFocus() {
	top[2].HidePageMessage();
}