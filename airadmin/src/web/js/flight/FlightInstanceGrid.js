var recNumPerPage = 20;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "5%";
objCol2.arrayIndex = 1;
objCol2.toolTip = "Flight No";
objCol2.headerText = "Flight<br>No";
objCol2.itemAlign = "center";
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "8%";
objCol3.arrayIndex = 2;
objCol3.toolTip = "Depature Date";
objCol3.headerText = "D.Date<br>zulu";
objCol3.itemAlign = "center";
objCol3.sort = true;

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "5%";
objCol4.arrayIndex = 3;
objCol4.toolTip = "Origin";
objCol4.headerText = "Org.<br>";
objCol4.itemAlign = "center";
objCol4.sort = true;

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "5%";
objCol5.arrayIndex = 4;
objCol5.toolTip = "Destination";
objCol5.headerText = "Dest.<br>";
objCol5.itemAlign = "center";
objCol5.sort = true;

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "14%";
objCol6.arrayIndex = 5;
// objCol6.toolTip = "Segments" ;
objCol6.headerText = "Segments";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "5%";
objCol7.arrayIndex = 6;
objCol7.toolTip = "Estimated Time Departure - Zulu";
objCol7.headerText = "ETD<BR>&nbsp;&nbsp;&nbsp;zulu";
objCol7.itemAlign = "center";
objCol7.sort = true;

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "5%";
objCol8.arrayIndex = 24;
objCol8.toolTip = "Estimated Time Departure - Local";
objCol8.headerText = "ETD<BR>local";
objCol8.itemAlign = "center";

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "5%";
objCol9.arrayIndex = 8; 
objCol9.toolTip = "Operation Type";
objCol9.headerText = "O.Type<br>";
objCol9.itemAlign = "center";
objCol9.sort = true;

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "5%";
objCol10.arrayIndex = 9;
// objCol10.toolTip = "Overlap Flight" ;
objCol10.headerText = "O.lap<br>Flight";
objCol10.itemAlign = "center";

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 10;
// objCol11.toolTip = "Seats Allocated" ;
objCol11.headerText = "Seats<br>Alloc.";
objCol11.itemAlign = "right";

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "5%";
objCol13.arrayIndex = 11;
// objCol13.toolTip = "Seats Sold" ;
objCol13.headerText = "Seats<br>Sold";
objCol13.itemAlign = "right";

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "5%";
objCol14.arrayIndex = 12;
// objCol14.toolTip = "Seats on Hold" ;
objCol14.headerText = "On<br>Hold";
objCol14.itemAlign = "right";

var objCol15 = new DGColumn();
objCol15.columnType = "image";
objCol15.width = "4%";
objCol15.arrayIndex = 32;
// objCol15.toolTip = "Status" ;
objCol15.headerText = "Status";
objCol15.itemAlign = "center";

var objCol16 = new DGColumn();
objCol16.columnType = "label";
objCol16.width = "5%";
objCol16.arrayIndex = 14;
// objCol16.toolTip = "ASK'000" ;
objCol16.headerText = "ASK'<br>000";
objCol16.itemAlign = "right";

var objCol17 = new DGColumn();
objCol17.columnType = "label";
objCol17.width = "5%";
objCol17.arrayIndex = 15;
// objCol17.toolTip = "Schedule ID" ;
objCol17.headerText = "Sch.<br>ID";
objCol17.itemAlign = "right";

var objCol18 = new DGColumn();
objCol18.columnType = "checkbox";
objCol18.width = "2%";
objCol18.arrayIndex = 33;
// objCol13.toolTip = "Select All" ;
objCol18.checkAll = true;
 objCol18.linkOnClick = "chkvalidate1";
objCol18.headerText = "";
objCol18.itemAlign = "center"

var objCol19 = new DGColumn();
objCol19.columnType = "label";
objCol19.width = "8%";
objCol19.arrayIndex = 37;
objCol19.headerText = "Flight<br/>Type";
objCol19.itemAlign = "center";

// ---------------- Grid

var objDG = new DataGrid("spnInstances");
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);
objDG.addColumn(objCol13);
objDG.addColumn(objCol14);
objDG.addColumn(objCol15);
objDG.addColumn(objCol16);
objDG.addColumn(objCol17);
objDG.addColumn(objCol19);
	//if(isInManageFltMode =="MANAGE"){
		objDG.addColumn(objCol18); // be cautious when adding new columns
	//}


objDG.width = "99%";
objDG.height = "142px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = flightData;
objDG.seqNo = true;

var seqSize = Number(getTabValue()[0]) + (Number(recNumPerPage) - 1);
if (new String(seqSize).length > 2) {
	objDG.seqNoWidth = "4%";
} else {
	objDG.seqNoWidth = "3%";
}
objDG.backGroundColor = "#ECECEC";
// objDG.seqStartNo = top[0].intLastRec;
objDG.seqStartNo = getTabValue()[0];
objDG.setColors = "setColors";
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = recNumPerPage;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "gridClick";
objDG.displayGrid();


function writeLedgend(objDG){
	var strHTMLText = "<img src='../../images/act_status_no_cache.gif'>Active&nbsp;<img src='../../images/cls_status_no_cache.gif'>Closed&nbsp;";
	strHTMLText+="&nbsp;<img src='../../images/cnx_status_no_cache.gif'>Canceled&nbsp;<img src='../../images/cre_status_no_cache.gif'>Created"
	DivWrite("spn_SA_"+objDG.ID, strHTMLText);
	
}

function setColors(strRowData) {
	var strColor = "";
	var StartD = dateChk(strRowData[2]);
	var Today = dateChk(getVal("hdnCurrentDate"));

	StartD = formatDate(StartD, "dd/mm/yyyy", "dd-mmm-yyyy");
	Today = formatDate(Today, "dd/mm/yyyy", "dd-mmm-yyyy");

	if (compareDates(StartD, Today, '<')) { // passed date
		strColor = "#AAAAAA";
	} else if ((parseInt(strRowData[10])) < (parseInt(strRowData[11]) + parseInt(strRowData[12]))) { // overload
		strColor = "red";
	} else if ((strRowData[19] == "true") && (strRowData[15] != '&nbsp')) { // manually
																			// changed
		strColor = "blue";
	} else if (trim(strRowData[9]) != "&nbsp") { // overlap
		strColor = "green";
	}
	return strColor;
}

function chkvalidate1() {
	focusTab(0);
	disableManageFlights('disable');
	var count = 0;
	var selscedtobld = objDG.getSelectedColumn(15);
	
	for ( var i = 0; i < selscedtobld.length; i++) {
		 
			if (selscedtobld[i][1]) {
				//
				count++;
			}
	}
	
	if (count > 0) {
		
		 Disable("btnManageFlight", false);
	}else{
		Disable("btnManageFlight", true);
		disableManageFlights('disable');
	}
	 
	 
  
}// end chkvalidate

 



function SelectAll() {
	/* blnRowClick = false;
	if (document.forms[0].chkSelAll.checked) {
		var intLength = flightData.length;
		for ( var i = 0; i < intLength; i++) {
			 
			objDG.setCellValue(i, 15, "true");
		}
		//disableCommonButtons("false");
		//disablePrintBuildButtons("");
	} else {
		var intLength = flightData.length;
		for ( var i = 0; i < intLength; i++) {
			objDG.setCellValue(i, 15, "");
		}
		//disableCommonButtons("flase");
		//disablePrintBuildButtons("false");
	}
	*/
	
	//clearInputSection();
	//clearLegSection();
	//disableInputSection("false");
	//disableLegSection("false");
	//disableSaveResetButtons("false");

} // end SelectAll

var dayOffset = 0;
function writeFrequency() {
	if (dayOffset == "")
		dayOffset = "0";
	var dayArr = new Array();
	dayArr[0] = new Array("chkSunday", "Su", "Sunday");
	dayArr[1] = new Array("chkMonday", "Mo", "Monday");
	dayArr[2] = new Array("chkTuesday", "Tu", "Tuesday");
	dayArr[3] = new Array("chkWednesday", "We", "Wednesday");
	dayArr[4] = new Array("chkThursday", "Th", "Thirsday");
	dayArr[5] = new Array("chkFriday", "Fr", "Friday");
	dayArr[6] = new Array("chkSaturday", "Sa", "Saturday");

	var strHTMLText = "";
	var i = parseInt(dayOffset) % 7;

	strHTMLText += '<table width="99%" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="center">';
	strHTMLText += '<tr>';
	strHTMLText += '	<td><font>Frequency</font></td>';
	//strHTMLText += '	<td align="right"><input type="checkbox" id="chkAll" name="chkAll" class="NoBorder" onClick="selectFrequency()" onchange="dataChanged()" tabindex="20" title="All"></td>';
	//strHTMLText += '	<td align="left" style="padding-right:3px;"><font>All</font></td>';

	for ( var j = 0; j < 7; j++) {
		if (i == 7)
			i = 0;
		strHTMLText += '<td align="right"><input type="checkbox" id='
				+ dayArr[i][0]
				+ ' name='
				+ dayArr[i][0]
				+ ' class="NoBorder" tabindex="27" title='
				+ dayArr[i][2] + '></td>';
		strHTMLText += '<td align="left" style="padding-right:3px;"><font>' + dayArr[i][1] + '</font></td>';
		i++
	}

	//strHTMLText += '		<font class="mandatory"> &nbsp;* </font>';
	strHTMLText += '	</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';
	DivWrite("spnFrequency", strHTMLText);

}


function selectFrequency() {

	if (document.getElementById("chkAll").checked) {

		document.getElementById("chkSunday").checked = true;
		document.getElementById("chkMonday").checked = true;
		document.getElementById("chkTuesday").checked = true;
		document.getElementById("chkWednesday").checked = true;
		document.getElementById("chkThursday").checked = true;
		document.getElementById("chkFriday").checked = true;
		document.getElementById("chkSaturday").checked = true;
	} else {
		document.getElementById("chkSunday").checked = false;
		document.getElementById("chkMonday").checked = false;
		document.getElementById("chkTuesday").checked = false;
		document.getElementById("chkWednesday").checked = false;
		document.getElementById("chkThursday").checked = false;
		document.getElementById("chkFriday").checked = false;
		document.getElementById("chkSaturday").checked = false;
	}
}