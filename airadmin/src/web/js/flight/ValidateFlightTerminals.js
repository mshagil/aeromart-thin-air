var pageEdited = false;
arrSegData = sort(arrSegData, 1);
var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "50%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Segment";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left";

var objCol3 = new DGColumn();
objCol3.columnType = "CUSTOM-SELECT";
objCol3.width = "25%";
objCol3.arrayIndex = 2;
objCol3.linkOnClick = "invalidateSegments";
objCol3.toolTip = "Departure Terminal";
objCol3.headerText = "Departure Terminal";
objCol3.itemAlign = "center";
objCol3.ID = 'selArrivalTerminal';
objCol3.htmlTag = "<select id='selArrivalTerminal' name='selArrivalTerminal'  style='width:100px' :CUSTOM: ><option value=''> </option>:DATA:</select>";

var objCol4 = new DGColumn();
objCol4.columnType = "CUSTOM-SELECT";
objCol4.width = "25%";
objCol4.arrayIndex = 3;
objCol4.linkOnClick = "invalidateSegments";
objCol4.toolTip = "Arrival Terminal";
objCol4.headerText = "Arrival Terminal";
objCol4.itemAlign = "center";
objCol4.ID = 'selDepartureTerminal';
objCol4.htmlTag = "<select id='selDepartureTerminal' name='selDepartureTerminal'  style='width:100px' :CUSTOM: ><option value=''> </option>:DATA:</select>";

// ---------------- Grid
var objDG = new DataGrid("spnValidSeg");
objDG.addColumn(objCol1);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.width = "99%";
objDG.height = "90px";
objDG.headerBold = false;
objDG.arrGridData = arrSegData;
objDG.rowOver = false;
objDG.seqNo = false;
objDG.backGroundColor = "#ECECEC";
objDG.setColors = "setReversColorGrid";
objDG.displayGrid();


var overLapSegCode = "";
var blnOverlap = false;
var overschedid = "";
var olSegment = new Array();


// setting the color in the grid
function setColorGrid(strData) {
	var arrReturnData = strData;
	
	for ( var i = 0; i < arrSegData.length; i++) {
		if (arrSegData[i][1] == arrReturnData[1]) {
			overLapSegCode = arrSegData[i][2]; 
			return "green";
		}
	}
	
}

function setReversColorGrid(strData) {
	
	for ( var i = 0; i < olSegment.length; i++) {
		if (olSegment[i][1] == strData[1]) {
			return "green";
		}
	}
	
}



function oLapGridCheck() {
	if (objDG2.loaded) {
		clearInterval(objTimer);
		objDG.refresh();
	}
}
// show the overlaps
function ViewOverLap() {
	var strArray = objDG.getSelectedColumn(1);
	blnOverlap = true;
	var validity = false;
	for ( var j = 0; j < strArray.length; j++) {
		if (strArray[j][1]) {
			validity = true;
			break;
		}
	}
	if (validity) {
		setVisible("divOLap1", true);
	} else {
		validateError(onevalidseg);
		return;
	}
}

var objTimer = setInterval("onLoad()", 300);

function onLoad() {
	var selectedTerminals = opener.getValidatedTerminals();
	var loadGrid = "";
	if (objDG.loaded == true) {
		clearInterval(objTimer);
		var selectedValues = opener.getFieldByID("hdnSegArray").value;
		var arrSelectedData = new Array();
		if ((selectedTerminals != null) && (selectedTerminals != "")) {
			arrSelectedData = selectedTerminals.split("|");
		}

		for ( var i = 0; i < arrSelectedData.length; i++) {
			if ((arrSelectedData[i] != null) && (arrSelectedData[i] != "")) {
				var arrSelectedSegData = arrSelectedData[i].split("^");
				for ( var j = 0; j < arrSegData.length; j++) {
					if (arrSegData[j][1] == arrSelectedSegData[0]) {						
						objDG.setCellValue(j, 1, arrSelectedSegData[1]);
						objDG.setCellValue(j, 2, arrSelectedSegData[2]);						
						break;
					}
				}
			}
		}
		loadGrid = objDG.getSelectedColumn(1);
		var olid = trim(getFieldByID("hdnOverlapId").value);
		if (olid == "null" || olid == "") {
			clearInterval(objTimer);
			Disable("btnOLap", "");
		} else {

			
			arrOLSegData = sort(arrOLSegData, 1);
			//objDG2.arrGridData = arrOLSegData;
			olSegment = arrOLSegData;
			overschedid = olid;
			
			objDG.refresh();
			
			objTimer = setInterval('oLapGridCheck()', 300);
			clearInterval(objTimer);
			

		}
		
	}
	setPageEdited(false);
}

function confirmValidTerminal() {	
	var selVal = "";
	for ( var i = 0; i < arrSegData.length; i++) {		
		selVal = selVal + "|" + objDG.getCellValue(i, 0) +"^"+objDG.getCellValueDup(i, 1)+"^"+objDG.getCellValueDup(i, 2);		
	}
	opener.validatedTerminals(selVal.substring(1));
	opener.top[0].objWindow.close();
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
	var olid = trim(getFieldByID("hdnOverlapId").value);
	if (olid != "null" && olid != "") {
		
	}
}

function removeOverLap() {
	var strValidSegment = "";
	var strconfirm = confirm("Overlap with Schedule removed. Confirm update ");
	if (strconfirm == true) {
	
		var strArray = objDG.getSelectedColumn(1);
		if (!validateSegments(strArray, ""))
			return;
	}
}

function validateSegments(strArray, olap) {
	var strValidSegment = "";
	var validity = false;
	for ( var j = 0; j < strArray.length; j++) {
		if (strArray[j][1]) {
			validity = true;
			break;
		}
	}

	if (validity) {
		for ( var i = 0; i < strArray.length; i++) {
			if ((!strArray[i][1]) && (arrSegData[i][3] == "true")) {
				validateError(resExist);
				return false;
			}
		}

		for ( var i = 0; i < arrSegData.length; i++) {
			strValidSegment += arrSegData[i][1] + "_";
			strValidSegment += strArray[i][1] + ",";
		}
		if (olap != "") {
			var selscedtobld = objDG.getSelectedColumn(1);
			for ( var i = 0; i < olSegment.length; i++) {
				for ( var j = 0; j < arrSegData.length; j++) {
					if (olSegment[i][1] == arrSegData[j][1]) {
						if ((olSegment[i][2] == "Y") && (selscedtobld[j][1])) {
							validateError(needinvalid);
							return false;
						}
					}
				}
			}
		}
		opener.validatedSegments(strValidSegment, overLapSegCode, olap);
		opener.top[0].objWindow.close();
		// opener.objWindow.close();
	} else {
		validateError(onevalidseg);
		return false;
	}
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

function dataChanged() {
	setPageEdited(true);
}

function invalidateSegments() {
	dataChanged();
}

function cancelClick() {
	if (pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}