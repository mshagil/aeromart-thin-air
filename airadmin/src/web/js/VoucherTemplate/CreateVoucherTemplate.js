var templatesData = {};
var voucherTemplateNames = new Array();
var generateChargesPane = false;
var isAddClick = false;
var notExists = true;
var isUpdate = false;

var selectedGridRowId = "";

jQuery(document)
		.ready(
				function() {
					$("#divSearch").decoratePanel("Search Gift Voucher Templates");
					$("#divResultsPanel").decoratePanel("Gift Voucher Templates");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$("#btnEdit").hide();
					$("#btnUpdate").hide();					
			
					$('#voucherTemplateDetails :input').attr('disabled', true);
					$("#btnClose").attr('disabled', false);
					$('#txtVoucherName').attr('disabled', true);

					enableGridButtons(false);
					enbleFormButtons(false);

					loadDisplayData();
					getAllTemplateNames();
					clearPanel('voucherTemplateDetails');
					constructVoucherTemplateGrid();
					
					$("#txtStartDateDetail").datepicker({
						showOn : "button",
						dateFormat : 'dd/mm/yy',
						buttonImage : "../../images/calendar_no_cache.gif",
						yearRange : "+0:+99",
						minDate : "0Y",
						buttonImageOnly : true,
						buttonText : "Select date",
						onSelect: function (date) {
							var date2 = $('#txtStartDateDetail').datepicker('getDate');
							$('#txtEndDateDetail').datepicker('option', 'minDate', date2);
						},
						changeMonth : true,
						changeYear : true
					});

					$("#txtEndDateDetail").datepicker({
						showOn : "button",
						dateFormat : 'dd/mm/yy',
						buttonImage : "../../images/calendar_no_cache.gif",
						yearRange : "+0:+99",
						minDate : "0Y",
						buttonImageOnly : true,
						buttonText : "Select date",
						onClose: function () {
							var dt1 = $('#txtStartDateDetail').datepicker('getDate');
							var dt2 = $('#txtEndDateDetail').datepicker('getDate');
					            
					        if (dt2 <= dt1) {
					        	var minDate = $('#txtEndDateDetail').datepicker('option', 'minDate');
					        	$('#txtEndDateDetail').datepicker('setDate', minDate);
					        }
					    },
						changeMonth : true,
						changeYear : true
					});

					$('#btnAdd').click(
							function() {
								formEnable();
								selectedGridRowId = "";
								isAddClick = true;
								hideMessage();
								clearPanel('voucherTemplateDetails');
								grdArray = new Array();
								enbleFormButtons(true);
								$('#voucherTemplateDetails :input').attr(
										'disabled', false);
								$("#txtStartDateDetail, #txtEndDateDetail")
										.datepicker('enable');
							});
					$("#btnEdit").click(function() {
						editClicked();						
					});
					$("#btnUpdate").click(function() {
						saveUpdatedTemplate();
					});

				});

function enbleFormButtons(cond) {
	if (cond) {
		$("#btnReset").enableButton();
		$("#btnSave").enableButton();
		$("#btnSave").show();
		$("#btnUpdate").hide();
	} else {
		$("#btnReset").disableButton();
		$("#btnSave").disableButton();
	}

}

function enableGridButtons(cond) {
	if (cond) {
		$("#btnDelete").enableButton();
		$("#btnAddOND").enableButton();
	} else {
		$("#btnDelete").disableButton();
		$("#btnAddOND").disableButton();
	}
}

function hideMessage() {
	top[2].HidePageMessage();
}

function disableControls(cond) {
	$("input").each(function() {
		$(this).attr('disabled', cond);
	});
	$("textarea").each(function() {
		$(this).attr('disabled', cond);
	});
	$("select").each(function() {
		$(this).attr('disabled', cond);
	});
	$("#btnClose").attr('disabled', false);
}

function constructVoucherTemplateGrid(strUrl) {
	$("#jqGridVoucherTemplatesData")
			.jqGrid(
					{
						url : 'voucherTemplatesManagement!searchVoucherTemplates.action',
						datatype : "json",
						mtype : 'POST',
						jsonReader : {
							root : "rows",
							page : "page",
							total : "total",
							records : "records",
							repeatitems : false,
							id : "0"
						},
						rowNum : 20,
						height : 160,
						width : 930,
						loadui : 'block',
						colNames : [ '&nbsp;', 'Voucher Name', 'Amount',
								'Currency', 'Valid (in Days)', 'Sales From',
								'Sales To', 'remarks' ],
						colModel : [ {
							name : 'template.voucherID',
							index : 'voucherId',
							hidden : true,
							width : 30
						}, {
							name : 'template.voucherName',
							index : 'voucherName',
							width : 100,
							align : "center"
						}, {
							name : 'template.amountInLocal',
							index : 'amountInLocal',
							width : 100,
							align : "center"
						}, {
							name : 'template.currencyCode',
							index : 'currencyCode',
							width : 100,
							align : "center"
						}, {
							name : 'template.voucherValidity',
							index : 'voucherValidity',
							width : 100,
							align : "center"
						}, {
							name : 'template.salesFrom',
							index : 'salesFrom',
							width : 100,
							align : "center",
							formatter : dateFormatter
						}, {
							name : 'template.salesTo',
							index : 'salesTo',
							width : 100,
							align : "center",
							formatter : dateFormatter
						}, {
							name : 'template.remarks',
							index : 'remarks',
							hidden : true
						} ],
						multiselect : false,
						viewrecords : true,
						onSelectRow : function(rowid) {
							if (true) {
								selectedGridRowId = rowid;
								performOperationsOnRowClick(rowid);
							}
						},
						loadComplete : function(response) {
							var resp = eval("(" + response.responseText + ")");
							templatesData = resp.rows;
						},
						pager : jQuery('#jqGridVoucherTemplatesPages')
					}).navGrid("#mealpager", {
				refresh : false,
				add : false,
				del : false,
				edit : false,
				search : false
			});
}

function loadDisplayData() {
	$.ajax({
		url : 'loadVoucherTemplatesManagement.action',
		beforeSend : top[2].ShowProgress(),
		data : $.extend({}, {}),
		dataType : "json",
		complete : function(resp) {
			resp = eval("(" + resp.responseText + ")");

			$("#selectCurrency").fillDropDown({
				firstEmpty : false,
				dataArray : resp.voucherCurrencyList,
				keyIndex : 0,
				valueIndex : 1
			});

			top[2].HideProgress();
		}
	});
}

function searchVoucherTemplates() {

	var strUrl = "voucherTemplatesManagement!searchVoucherTemplates.action?";

	strUrl += "voucherName=" + $("#searchVoucherName").val();
	strUrl += "&status=" + $("#statusSrch").val();

	$("#jqGridVoucherTemplatesData").setGridParam({
		url : strUrl,
		page : 1
	});
	$("#jqGridVoucherTemplatesData").trigger("reloadGrid");
	$("#btnAdd").attr('disabled', false);
	enableGridButtons(false);
	$('#voucherTemplateDetails :input').attr('disabled', true);
	$("#btnClose").attr('disabled', false);
	clearPanel('voucherTemplateDetails');
	selectedGridRowId = "";

}

function performOperationsOnRowClick(rowid) {
	clearPanel('voucherTemplateDetails');
	hideMessage();
	$('#txtVoucherName').attr('disabled', true);
	// setPageEdited(false);
	loadVoucherTemplateToDetailsPane(rowid);
	enableGridButtons(true);
	$("#btnClose").attr('disabled', false);
}

function getAllTemplateNames () {
	var ddl = document.getElementById('voucherNames');
	for (i = 0; i < ddl.options.length; i++) {
		voucherTemplateNames[i] = ddl.options[i].text;
	}
}

function loadVoucherTemplateToDetailsPane(rowid) {

	isAddClick = false;
	var voucherTemplate = templatesData[parseInt(rowid) - 1].template;

	$('#txtStartDateDetail').val(formatDateToDisplay(voucherTemplate.salesFrom));
	$('#txtEndDateDetail').val(formatDateToDisplay(voucherTemplate.salesTo));
	$("#voucherId").val(voucherTemplate.voucherId);
	$("#txtVoucherName").val(voucherTemplate.voucherName);
	$("#txtAmount").val(voucherTemplate.amountInLocal);
	$("#txtRemarks").val(voucherTemplate.remarks);
	$("#status").val(voucherTemplate.status);
	$("#validity").val(voucherTemplate.voucherValidity);
	$("#selectCurrency").val(voucherTemplate.currencyCode);
	formDisable();
	$("#btnEdit").show();
}

function submitVoucherTemplate() {
	if (warningForSameName() && validateVoucher()) {
		var data1 = {};
		var strUrl;
		
		data1['voucherTemplateTo.voucherName'] = $("#txtVoucherName").val();
		data1['voucherTemplateTo.amountInLocal'] =  Math.floor($("#txtAmount").val() * 100) / 100;
		data1['voucherTemplateTo.voucherValidity'] = $("#validity").val();
		data1['voucherTemplateTo.remarks'] = $("#txtRemarks").val();
		data1['voucherTemplateTo.status'] = $("#status").val();
		data1['voucherTemplateTo.currencyCode'] = $("#selectCurrency").val();
		data1['startDate'] = $("#txtStartDateDetail").val();
		data1['endDate'] = $("#txtEndDateDetail").val();

		strUrl = 'voucherTemplatesManagement!createVoucherTemplate.action'
		var success;
		var message;
		$.ajax({
			url : strUrl,
			beforeSend : top[2].ShowProgress(),
			data : data1,
			type : "POST",
			dataType : "json",
			complete : function(resp) {
				clearPanel('voucherTemplateDetails');
				resp = resp = eval("(" + resp.responseText + ")");
				if (resp.success) {
					//searchVoucherTemplates();
					alert("Voucher Template is Successfully Saved.");
					location.reload();
					$("#btnEdit").hide();
				} else {
					alert("Voucher's Add/Update Unsuccessful.")
				}
				top[2].HideProgress();
			}
		});		
	}

}

function dateFormatter(cellvalue, options, rowObject) {
	return formatDate(cellvalue);
}

function formatDate(value) {
	return value.substr(0, 10);
}

function resetClick() {
	clearPanel('voucherTemplateDetails');
}

function clearPanel(panelId) {
	$("#" + panelId + " input[type=\"text\"]").val('');
	$("#" + panelId + " input[type=\"hidden\"]").val('');
	$("#" + panelId + " textarea").val('');
	$("#" + panelId + " select[id=\"selectCurrency\"] option:first-child")
			.attr("selected", "selected");
	$("#" + panelId + " select[id=\"validity\"] option:first-child").attr(
			"selected", "selected");

	clearERRMessage();

}

function hideMessage() {
	top[2].HidePageMessage();
}

function spclCharsVali(val) {
	customValidate(val, /[`!@#$%*?\[\]{}()|\\\/+=:,;^~]/g);
}

function validateVoucher() {

	var StrCurrentDate = DateToString(new Date());
	if ($("#txtVoucherName").val() == "") {
		showERRMessage(voucherNameRequired);
		$("#txtVoucherName").focus();
		return false;
	} else if (!isAlphaNumericWhiteSpace(trim($("#txtVoucherName").val()))) {
		showERRMessage(voucherNameInvalid);
		$("#txtVoucherName").focus();
		return false;
	} else if ($("#txtAmount").val() == "") {
		showERRMessage(amountRequired);
		$("#txtAmount").focus();
		return false;
	} else if (isNaN($("#txtAmount").val())) {
		showERRMessage(amounIsNumber);
		$("#txtAmount").focus();
		return false;
	} else if ($("#txtAmount").val() < 0) {
		showERRMessage(buildError(negativeNumber, 'Amount'));
		$("#txtAmount").focus();
		return false;
	} else if ($("#txtStartDateDetail").val() == "") {
		showERRMessage(startDateRequired);
		$("#txtStartDateDetail").focus();
		return false;
	} else if (!CheckDates(formatDate(StrCurrentDate, "dd/mm/yyyy",
			"dd-mmm-yyyy"), formatDate($("#txtStartDateDetail").val(),
			"dd/mm/yyyy", "dd-mmm-yyyy"))) {
		showERRMessage(buildError(lowerCurrentDate, 'Start date '));
		$("#txtStartDateDetail").focus();
		return false;
	} else if ($("#txtEndDateDetail").val() == "") {
		showERRMessage(endDateRequired);
		$("#txtEndDateDetail").focus();
		return false;
	} else if (!CheckDates(formatDate(StrCurrentDate, "dd/mm/yyyy",
			"dd-mmm-yyyy"), formatDate($("#txtEndDateDetail").val(),
			"dd/mm/yyyy", "dd-mmm-yyyy"))) {
		showERRMessage(buildError(lowerCurrentDate, 'End date'));
		$("#txtEndDateDetail").focus();
		return false;
	} else if (!CheckDates(formatDate($("#txtStartDateDetail").val(),
			"dd/mm/yyyy", "dd-mmm-yyyy"), formatDate($("#txtEndDateDetail")
			.val(), "dd/mm/yyyy", "dd-mmm-yyyy"))) {
		showERRMessage(lowEndDate);
		$("#txtEndDateDetail").focus();
		return false;
	} else {
		return true;
	}

}

function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

isNumberKey = function(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function checkAlreadyExists(tempName) {
	if ($.inArray(tempName, voucherTemplateNames) != -1) {
		return true;
	} else {
		return false;
	}
}

function warningForSameName() {
	tempName = $("#txtVoucherName").val();
	if (checkAlreadyExists(tempName)) {
		var isChange = window
				.confirm("Voucher Template Name "
						+ tempName
						+ " already exists. \n\nDo you want to create new Template in same name ?");
		if (isChange) {
			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function formDisable() {
	$("#txtStartDateDetail").prop('disabled', true);
	$("#txtEndDateDetail").prop('disabled', true);
	$("#txtVoucherName").prop('disabled', true);
	$("#txtAmount").prop('disabled', true);
	$("#selectCurrency").prop('disabled', true);	
	$("#status").prop('disabled', true);
	$("#txtRemarks").prop('disabled', true);
	$("#validity").prop('disabled', true);
}

function formEnable() {
	$("#txtStartDateDetail").datepicker("option", "disabled", false);
	$("#txtEndDateDetail").datepicker("option", "disabled", false);
	$("#txtStartDateDetail").prop('disabled', false);
	$("#txtEndDateDetail").prop('disabled', false);
	$("#txtVoucherName").prop('disabled', false);
	$("#txtAmount").prop('disabled', false);
	$("#selectCurrency").prop('disabled', false);	
	$("#status").prop('disabled', false);
	$("#txtRemarks").prop('disabled', false);
	$("#validity").prop('disabled', false);
}

function updateVoucherTemplate() {
	$("#txtStartDateDetail").datepicker("option", "disabled", false);
	$("#txtEndDateDetail").datepicker("option", "disabled", false);
	$("#status").prop('disabled', false);
	$("#btnUpdate").attr('disabled', false);
}

function saveUpdatedTemplate() {
	if (validateVoucher()) {
		var data1 = {};
		var strUrl;
		data1['voucherTemplateTo.voucherId'] = $("#voucherId").val();
		data1['voucherTemplateTo.voucherName'] = $("#txtVoucherName").val();
		data1['voucherTemplateTo.amountInLocal'] =  Math.floor($("#txtAmount").val() * 100) / 100;
		data1['voucherTemplateTo.voucherValidity'] = $("#validity").val();
		data1['voucherTemplateTo.remarks'] = $("#txtRemarks").val();
		data1['voucherTemplateTo.status'] = $("#status").val();
		data1['voucherTemplateTo.currencyCode'] = $("#selectCurrency").val();
		data1['startDate'] = $("#txtStartDateDetail").val();
		data1['endDate'] = $("#txtEndDateDetail").val();

		strUrl = 'voucherTemplatesManagement!createVoucherTemplate.action'
		var success;
		var message;
		$.ajax({
			url : strUrl,
			beforeSend : top[2].ShowProgress(),
			data : data1,
			type : "POST",
			dataType : "json",
			complete : function(resp) {
				clearPanel('voucherTemplateDetails');
				resp = resp = eval("(" + resp.responseText + ")");
				if (resp.success) {
					//searchVoucherTemplates();
					alert("Voucher Template is Successfully Updated.");
					location.reload();
					$("#btnEdit").hide();
					$("#btnUpdate").hide();
					$("#btnAdd").show();
				} else {
					alert("Voucher's Update Unsuccessful.");
					$("#btnEdit").hide();
					$("#btnUpdate").hide();
					$("#btnAdd").show();
				}
				top[2].HideProgress();
			}
		});		
	}
}

function formatDateToDisplay (date) {
	var oldFormatArray = date.split("T");
	var oldDate = oldFormatArray[0];
	var splitedDate = oldDate.split("-");
	var yyyy = splitedDate[0];
	var mm = splitedDate[1];
	var dd = splitedDate[2];
	
	var newDate = dd+"/"+mm+"/"+yyyy;
	return newDate;
}

function editClicked () {
	if ($("#txtVoucherName").val() == "") {
		showERRMessage(selectRecord);
	} else {
		$("#btnSave").hide();
		$("#btnUpdate").show();
		updateVoucherTemplate();
	}
}
