var screenId = "SC_ADMN_0022";
var valueSeperator = "~";
var recstart = 1;

if (trim(initialrec) != "") {
	recstart = initialrec;
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Carrier";
objCol1.itemAlign = "left"
objCol1.sort = "true"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "27%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Parameter Name";
objCol2.itemAlign = "left"
objCol2.sort = "true"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "18%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Parameter Key";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "23%";
objCol4.arrayIndex = 4;
objCol4.headerText = "Parameter Value";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "8%";
objCol5.arrayIndex = 5;
objCol5.headerText = "Type";
objCol5.itemAlign = "left"
	
var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "8%";
objCol6.arrayIndex = 6;
objCol6.headerText = "Editable";
objCol6.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnParameters");
if(getFieldByID("selCarrierCode").length <= 2){
	setVisible("fntCarrierCode", false);
	setVisible("selCarrierCode",false);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol6);
} else {
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol6);
}
objDG.width = "99%";
objDG.height = "200px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = paramData;
objDG.seqNo = true;
objDG.seqStartNo = recstart
// Grid
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();