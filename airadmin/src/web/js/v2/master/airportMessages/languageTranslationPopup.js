
function UI_AirportMsg_LanguagePopup() {
}

UI_AirportMsg_LanguagePopup.PopUpData= function ()
{
	var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Airport Message Translation For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
	html += '<select name="language" id="language" class="aa-input" style="width:150px; "> ';
	html += '<option value="-1"></option>'+languageHTML;				
	html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="btnLanTrAdd" id="btnLanTrAdd" type="button" class="Button" style="width:50px; " title="Add Item"  value="&gt;&gt;" /> <input name="btnLanTrRemove" id="btnLanTrRemove" type="button" class="Button" style="width:50px;" title="Remove Item" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" class="aa-input" style="width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font> Message Translation * </font></td> <td align="left" valign="top"><textarea rows="4" cols="20" id="textLangugeTranslation" name="textLangugeTranslation" class="aa-input"></textarea></td> </tr> ';
	
	html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" name="btnLanTrUpdate" id="btnLanTrUpdate" value= "Save" class="Button" > &nbsp;<input type="button" name="btnLanTrClose" id="btnLanTrClose" value= "Close" class="Button" ></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

    DivWrite("spnPopupData",html);
    
    $('#btnLanTrAdd').decorateButton();
    $('#btnLanTrRemove').decorateButton();
    $('#btnLanTrUpdate').decorateButton();
    $('#btnLanTrClose').decorateButton();   
    
    $('#btnLanTrAdd').click(function() { UI_AirportMsg_LanguagePopup.addLangugeTranslation (); });
	$('#btnLanTrRemove').click(function() {UI_AirportMsg_LanguagePopup.removeLangugeTranslation (); });
	$('#btnLanTrUpdate').click(function() { UI_AirportMsg_LanguagePopup.saveClick(); });
	$('#btnLanTrClose').click(function() { UI_AirportMsg_LanguagePopup.POSdisable(); });
	
}


UI_AirportMsg_LanguagePopup.POSenable= function()
{
	var message = UI_AirportMsg.getSelectedMessage(); 
	if ( message== null )
	  {
		  alert("No message is selected, please select message and try again");
	      return;
	  }	
	UI_AirportMsg_LanguageData.showPopUp(1);	
}

UI_AirportMsg_LanguagePopup.initilize = function() {	
	$('#language').clearFields();
	$('#textLangugeTranslation').clearFields();
	$('#translation option').each(function(i, option){ $(option).remove(); });
}

UI_AirportMsg_LanguagePopup.isLangaugeSelected = function( selectedLanguge ) {
    var isSelected = false;	
	$("#translation option").each(function() {
		var val = $(this).val();
		if (val == selectedLanguge)
		{
			isSelected = true;
			return;
		}		
	});
	return isSelected;
}

UI_AirportMsg_LanguagePopup.POSdisable = function()
{
	UI_AirportMsg_LanguageData.hidePopUp(1);
}

UI_AirportMsg_LanguagePopup.addLangugeTranslation = function() {
	var selectedLanguge = $('#language').val();
	var langugetranslation = $('#textLangugeTranslation').val();
	var languageDescription = $("#language option:selected").text();
	
	if (selectedLanguge =="" || langugetranslation =="" || selectedLanguge =="-1" )
	{
	    return ;	
	}
	var isAlreadyExist = UI_AirportMsg_LanguagePopup.isLangaugeSelected(selectedLanguge);
	if (isAlreadyExist){
		alert("Language translation for the selected language already exist");
		return;
	}
	
	var optionvalue = selectedLanguge;
	var diplayText =  languageDescription + "=>" +langugetranslation;
	var option = new Option(diplayText,optionvalue);
	option.title = langugetranslation;
	$('#translation').append(option);
	
}

UI_AirportMsg_LanguagePopup.removeLangugeTranslation = function() {
	$('#language').val( $('#translation :selected').val());
	var languageTranslation = $('#translation :selected').text();	
	$('#textLangugeTranslation').val(languageTranslation.split("=>")[1]);
	
	$('#translation :selected').remove();
}

UI_AirportMsg_LanguagePopup.setLanguageTranslationList = function(){	
	var languageList = UI_AirportMsg.getSelectedLanguageList(); 
	UI_AirportMsg_LanguagePopup.initilize();
	if ( languageList != null )
	  {	
		$.each(languageList, function(index, i18nMsg) {
			var optionvalue = i18nMsg.id;
			var language = UI_AirportMsg_LanguagePopup.getLanguageName(i18nMsg.id);	
			var transText = decodeFromHex (i18nMsg.text);
			var diplayText = language + "=>" + transText;	
			var option = new Option(diplayText,optionvalue);
			option.title = diplayText;
			$('#translation').append(option);
		});	  
	}
}

UI_AirportMsg_LanguagePopup.getLanguageName =  function(languageCode)
{
	var language; 
	$("#language option").each(function() {
		var val = $(this).val();
		if (languageCode == val)
		{
			language = $(this).text();
			return;
		}		
	});	
	return language;
}

UI_AirportMsg_LanguagePopup.getSelectedLanguageTranslations =  function()
{
	var selectedLanguageTranslations = new Array();
	var i = 0;
	$("#translation option").each(function() {
		var optValue = $(this).val();
		var optText = $(this).text();
		var translation = optText.split("=>")[1]; //.replace(/^\s+|\s+$/g, '');	
		translation = getUnicode(translation);
		var val = optValue + "^"+ translation ; 
		
		selectedLanguageTranslations[i] = val;
		i++;
		
	});
	return selectedLanguageTranslations;
}

UI_AirportMsg_LanguagePopup.saveClick =  function()
{ 
	if (!UI_AirportMsg_LanguagePopup.validate())
	{
		alert("At least one language translation shuld be set");
		return;
	}
	
	var selectedLanguageTranslations = UI_AirportMsg_LanguagePopup.getSelectedLanguageTranslations();	

	var data = {};
	data['airportMsgTO.messageId'] = $('#hdnMessageID').val();
	data['airportMsgTO.selectedLanguageTranslations'] = selectedLanguageTranslations.toString();
	
	$.ajax({
		  type: "POST",
		  url: 'airportMessages!addEditMessageLanguageTranslation.action',
		  data: data,
		  success: UI_AirportMsg.afterSave,
		  dataType: 'json'
		});
	
	UI_AirportMsg_LanguagePopup.POSdisable();
}


UI_AirportMsg_LanguagePopup.validate =  function()
{
	if ($('#translation option').size()>0)
	{
		return true;
	}
	return false;
}

