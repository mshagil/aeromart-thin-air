/**
 * Airport Message Management - flight numbers selection 
 * @author EP
 */
UI_AirportMsg_flightNumbers.multiSelect = null;
/**
 * Onload function
 */
$( function() {
	UI_AirportMsg_flightNumbers.initilize();
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_AirportMsg_flightNumbers() {
}
UI_AirportMsg_flightNumbers.getMultiSelectData = function (name){
	if(UI_AirportMsg_flightNumbers.multiSelect == null) return "";
	return UI_AirportMsg_flightNumbers.multiSelect.getSelectedItemsAsArray();
}

UI_AirportMsg_flightNumbers.setSelectedItems = function (objSelectedItems){
	if(UI_AirportMsg_flightNumbers.multiSelect == null) UI_AirportMsg_flightNumbers.setMultiSelect(allFlightsNumbers,objSelectedItems);
	UI_AirportMsg_flightNumbers.multiSelect.setSelectedItems(objSelectedItems);
}

UI_AirportMsg_flightNumbers.getInlusionStatus = function (){
	return $('input:radio[name=radFlight_inc]:checked').val();
}

UI_AirportMsg_flightNumbers.initilize = function (){
	UI_AirportMsg_flightNumbers.setMultiSelect(allFlightsNumbers,null);
}

UI_AirportMsg_flightNumbers.enableMultiSelect = function (blnStatus){
	$('#divFlighNumbers :input').attr('disabled',!blnStatus);
	if(blnStatus){
		UI_AirportMsg_flightNumbers.multiSelect.enable();
	}else {
		UI_AirportMsg_flightNumbers.multiSelect.disable();
	}
}

UI_AirportMsg_flightNumbers.setMultiSelect = function (allOptions, selectedOptions){
	$('#flightNumbersMulti').empty();
	var objConfig = {allOptions:allOptions,
			 group:false,
			 optionValue:"id",
			 optionText:"text",
			 container:"flightNumbersMulti",
			 titleAllItems:"Flight Numbers",
			 titleSelectedItems:"Selected Flight Numbers",
			 boxWidth:UI_AirportMsg.sizeOptions.width,
			 boxHeight:UI_AirportMsg.sizeOptions.height}; 

	UI_AirportMsg_flightNumbers.multiSelect  = new RichSelector(objConfig); 
	UI_AirportMsg_flightNumbers.multiSelect.setSelectedItems(selectedOptions);
}

UI_AirportMsg_flightNumbers.validateInclusion = function (){
	if(UI_AirportMsg_flightNumbers.getMultiSelectData() != ""){
		if ( $('.radFlight_inc').is(':checked') ) {
			return true;
		}
		return false;
	}else {
		return true;
	}
	
}