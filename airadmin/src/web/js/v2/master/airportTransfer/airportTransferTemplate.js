function UI_AirportTransferTemplate() {};

var objCal = new Calendar();
objCal.onClick = "setDate";
objCal.buildCalendar();

function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("departureDate",strDate);break ;
	}
}

$(document).ready(function() {
	
	$("#divSearch").decoratePanel("Search Airport Transfer Templates");
	$("#divResultsPanel").decoratePanel("Airport Transfer Templates");
	$("#divDispTransferTemplate").decoratePanel("Add/Modify Airport Transfer Templates");		
	
	$('#btnSearch').decorateButton();
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnSave').decorateButton();
	
	$('#btnLoadBookingClasses').decorateButton();
	
	UI_AirportTransferTemplate.disableControls(true);
	UI_AirportTransferTemplate.disableButtons();
	UI_AirportTransferTemplate.buildGrid();
	
	$('#btnSearch').click(function (){ 
		UI_AirportTransferTemplate.searchClicked()
	});
	
	$('#btnAdd').click(function (){
		UI_AirportTransferTemplate.addClicked();
	});
	
	$('#btnEdit').click(function (){
		UI_AirportTransferTemplate.editClicked();
	});
	
	$('#btnSave').click(function (){
		UI_AirportTransferTemplate.saveClicked();
	});
	
	$('#btnReset').click(function (){
		UI_AirportTransferTemplate.resetClicked();
	})

	$( "#flightNumber" ).autocomplete({
        source: flightNos,
        scroll: false
    });
	
	$('#btnLoadBookingClasses').click(function (){ 
		UI_AirportTransferTemplate.getBookingClassesForCC();
	});
	
});

UI_AirportTransferTemplate.buildGrid = function (){
	
	$("#tblAirportTransferTemplates").jqGrid({ 
		url:'showAirportTransferTemplate!search.action',
		datatype: "json",	
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,	
			  id: "0"				  
			},													
	   colNames:['&nbsp;','OnD Code','CabinClass', 'Flight Number','Departure Date', 'Model Number','Adult Amount','Child Amount','Infant Amount', 'Status', 'airportTransferTemplateId', 'bookingClasses', 'bookingClassesIds'], 
	   colModel:[ {name:'id', width:30, jsonmap:'id'},   
		          {name:"transferTemplate.ondCode",		    index: 'ondCode',      width:90, align:"left" },
		          {name:"transferTemplate.cabinClassCode",	index: 'cabinClassCode',     width:100, align:"left" },	
		          {name:"transferTemplate.flightNumber",	index: 'flightNumber',   width:120, align:"left" },	
		          {name:"transferTemplate.departureDate",	index: 'departureDate',   width:120, align:"left" },	
		          {name:"transferTemplate.modelNumber",		index: 'modelNumber',    width:180, align:"left" },
		          {name:"transferTemplate.adultAmount",		index: 'adultAmount',   width:120, align:"left" },	
		          {name:"transferTemplate.childAmount",		index: 'childAmount',   width:120, align:"left" },	
		          {name:"transferTemplate.infantAmount",	index: 'infantAmount',    width:120, align:"left" },
		          {name:"transferTemplate.status",			index: 'status',           width:80, align:"left" },
		          {name:"transferTemplate.airportTransferTemplateId",   index: 'airportTransferTemplateId',hidden:true },
		          {name:"transferTemplate.bookingClasses",   index: 'bookingClasses',hidden:true },
		          {name:"transferTemplate.bookingClassesIds",   index: 'bookingClassesIds',hidden:true }
			   ],
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#transferpager'),
		rowNum:10,						
		viewrecords: true,
		height:170,
		width:910,	
		onSelectRow: function(rowid){
			UI_AirportTransferTemplate.rowClick(rowid);
			
		},
	   	loadComplete: function (e){	   
			//
	   	}
	}).navGrid("#divAirportMsgPager",{refresh: false, edit: false, add: false, del: false, search: false});
}

UI_AirportTransferTemplate.searchClicked = function (){
	var postData = {};
	postData['status']= $('#searchStatus').val();
	postData['cabinClassCode']= $('#searchCabinClassCode').val();
	postData['flightNo']= $('#searchFlightNo').val();
	postData['aircraftModel']= $('#searchAircraftModel').val();
	$("#tblAirportTransferTemplates").setGridParam({page:1,postData:postData});
 	$("#tblAirportTransferTemplates").trigger("reloadGrid");
 	UI_AirportTransferTemplate.clearTransferForm();
 	UI_AirportTransferTemplate.disableControls(true);
 	UI_AirportTransferTemplate.disableButtons();
}

UI_AirportTransferTemplate.disableButtons = function (){
	$("#btnEdit").disableButton();
	$("#btnReset").disableButton();
	$("#btnSave").disableButton();
}

UI_AirportTransferTemplate.isPageEdited = function () {
	return top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId));
}

UI_AirportTransferTemplate.hideMessage = function (){
	top[2].HidePageMessage();
}	

UI_AirportTransferTemplate.setPageEdited = function (isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	UI_AirportTransferTemplate.hideMessage();
}

UI_AirportTransferTemplate.clearTransferForm = function (){
	$('#frmAirportTransferTemplate')[0].reset();
	$("#version").val('-1');
	$("#rowNo").val('');
	$("#cabinClassCode").val('');
	$("#flightNumber").val('');
	$("#departureDate").val(''); 
	$("#modelNumber").val(''); 
	$("#selDepature").val('');
	$("#selArrival").val('');
	$("#selVia1").val('');
	$("#selVia2").val('');
	$("#adultAmount").val('');
	$("#childAmount").val(''); 
	$("#infantAmount").val('');
	$("#airportTransferTemplateId").val('');
	lsbc.clear();
}

UI_AirportTransferTemplate.rowClick = function (rowid){
	UI_AirportTransferTemplate.clearTransferForm();
 	$("#tblAirportTransferTemplates").GridToForm(rowid,"#frmAirportTransferTemplate");
 	
 	$.when(UI_AirportTransferTemplate.getBookingClassesForCC()).done(function(){
 		var rowData = $("#tblAirportTransferTemplates").getRowData(rowid);
 		lsbc.move(">",rowData['transferTemplate.bookingClasses']);
 	});
 	
 	//var providerNumber = $("#tblAirportTransferTemplates").getCell(rowid,'transfer.providerNumber');
 	//UI_AirportTransferTemplate.populateDepArr(ondCode);
 	
	if($("#tblAirportTransferTemplates").getCell(rowid,'transferTemplate.status') == 'ACT'){
		$("#chkStatus").attr('checked', true);
	} else {
		$("#chkStatus").attr('checked', false);
	}
	var ond = $("#tblAirportTransferTemplates").getCell(rowid,'transferTemplate.ondCode').split("/");
	var noOfPorts = ond.length;
	var viaIndex = 1;
	$("#selDepature").val(ond[0]);
	$("#selArrival").val(ond[noOfPorts - 1]);
	
	if(noOfPorts > 2 && ((noOfPorts - 2) >= viaIndex)){
		$("#selVia" + viaIndex).val(ond[viaIndex]);
		viaIndex++;
	}
	
	$('#btnEdit').enableButton();
	UI_AirportTransferTemplate.disableControls(true);
	
}

UI_AirportTransferTemplate.disableControls = function (cond){
	$("#divDispTransferTemplate  input").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("#divDispTransferTemplate  select").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("#btnClose").attr('disabled', false);
	$("#bookingClassSelect").attr('disabled', cond);
}

UI_AirportTransferTemplate.addClicked = function (){
	UI_AirportTransferTemplate.disableControls(false);
	UI_AirportTransferTemplate.clearTransferForm();
						
	$('#btnEdit').disableButton();
	$("#btnSave").enableButton();
	$("#btnReset").enableButton();		
	
}

UI_AirportTransferTemplate.editClicked = function (){
	UI_AirportTransferTemplate.disableControls(false);
	$("#btnSave").enableButton();
	$("#btnReset").enableButton();	
}

UI_AirportTransferTemplate.saveClicked = function (){
	UI_AirportTransferTemplate.populateOnD();	
	var stat = "INA";
	if($('#chkStatus').attr('checked')== 'checked'){
		stat ="ACT";
	}
	$("#status").val(stat);
	setField("bookingClasses", UI_AirportTransferTemplate.getBookingClasses());
	if(!UI_AirportTransferTemplate.validateSave()){
		return false;
	}
	
	$('#frmAirportTransferTemplate').ajaxSubmit({
		dataType    : 'json', 
		cache       : false,
		beforeSubmit: UI_AirportTransferTemplate.beforeSave, 
		success     : UI_AirportTransferTemplate.saveSuccess		
	});
}

UI_AirportTransferTemplate.validateSave = function (){
	if($("#flightNumber").isFieldEmpty()){
		$("#flightNumber").val("ANY");	
	}
	
	if($("#flightNumber").val()!="ANY" && flightNos!=undefined 
			&& flightNos!=null && flightNos.indexOf($("#flightNumber").val())==-1){
		showERRMessage("Entered Flight Number is invalid");	
		return false;
	}
	
	if($("#bookingClasses").val() == ""){
		showERRMessage("Select Booking class/classes to save Airport Transfer Template");	
		return false;
	}
	
	return true;	
}

UI_AirportTransferTemplate.beforeSave = function (){
	top[2].ShowProgress();
}

UI_AirportTransferTemplate.saveSuccess = function (response){
	top[2].HideProgress();
	if(response.msgType != 'Error'){
		UI_AirportTransferTemplate.clearTransferForm();
		var postData = {};
		postData['searchCode']= $('#searchAirport').val();
		
		$("#tblAirportTransferTemplates").setGridParam({page:1,postData:postData});
	 	$("#tblAirportTransferTemplates").trigger("reloadGrid");
	 	UI_AirportTransferTemplate.disableControls(true);
	 	$('#btnEdit').disableButton();
		$("#btnSave").disableButton();
	}	
	UI_Common.displayMessage (response);	
}

UI_AirportTransferTemplate.resetClicked = function () {
    if($("#tblAirportTransferTemplates").getGridParam("selrow") != null){
    	UI_AirportTransferTemplate.rowClick($("#tblAirportTransferTemplates").getGridParam("selrow"));
     } else {
    	 $("#frmAirportTransferTemplate").clearForm();
    	 UI_AirportTransferTemplate.searchClicked();
     }
}

UI_AirportTransferTemplate.populateDepArr = function (ondCode){
	
	var ondCodes = ondCode.split("/");
	var origin = ondCodes[0];
	var destination = ondCodes[ondCodes.length-1];
	var via1 = "";
	var via2 = "";
	var via3 = "";
	var via4 = "";
	if (ondCodes.length == 3) {
		via1 = ondCodes[1];
	} else if (ondCodes.length == 4) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
	} else if (ondCodes.length == 5) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
		via3 = ondCodes[3];
	} else if (ondCodes.length == 6) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
		via3 = ondCodes[3];
		via4 = ondCodes[4];
	}
	$("#selDepature").val(origin);
	$("#selArrival").val(destination);
	$("#selVia1").val(via1);
	$("#selVia2").val(via2);
	$("#selVia3").val(via3);
	$("#selVia4").val(via4);
}

UI_AirportTransferTemplate.populateOnD = function (){
	var origin = $("#selDepature").val();
	var dest = $("#selArrival").val();
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();

	var ond=origin;
	if (origin != null && origin != "" && dest != null && dest != "") {
		if (via1 != null && via1 != ""){
			ond = ond + "/" + via1;
		}
		if (via2 != null && via2 != ""){
			ond = ond + "/" + via2;
		}
		ond = ond + "/" + dest;
	}
	$("#ondCode").val(ond);		   
}

//Custom Regex's
function validateWithComma(val){
	customValidate(val,/[`!@#$%*&?\"\'\[\]{}()|\\+=_><:;^~]/g);
}

function positiveNumberCustomValidate(objTxt){
	var strValue = objTxt.value;
	if (!isPositiveInt(strValue)) {
		objTxt.value = removeChars(strValue,/[a-zA-Z`!@#$%\s&*?\"\'\[\]{}()|\\\/=><:.,;^~_+-]/g);
	}	
}

function isCustomWhiteSpace(s){return RegExp("^[a-zA-Z0-9 \w\s]+$").test(s);}

function customWhiteSpaceValidate(objTxt){
	var strValue = objTxt.value;
	if (!isCustomWhiteSpace(strValue)) {
		objTxt.value = removeChars(strValue,/[`\/\'\"!@#$%&*?\[\]{}()|\\\/><+-=_:.,;^~]/g);
	}	
}

function LoadCalendar(strID, objEvent) {
	objCal.ID = strID;
	objCal.top = 400;
	objCal.left = 300;
	objCal.onClick = "setDate";
	if (!getFieldByID("departureDate").disabled) {
		objCal.showCalendar(objEvent);
	}
}

function settingValidation(cntfield){
	if (top[1].objTMenu.focusTab == screenId){
		if (!dateChk(cntfield))	{
			showERRMessage("Invalid date");
			getFieldByID(cntfield).focus();		
		return;
		}
	}
}

function validateAmount(objTxt) {
	var strCR = objTxt.value;
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 8, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 9) {				
				objTxt.value = strCR.substr(0,wholeNumber.length - 1);
			} else {
				objTxt.value = strCR.substr(0, strLen - 1);
			}
		} else {
			objTxt.value = strCR.substr(0, strLen - 1);
		}
		objTxt.focus();
	}
}

function cabinClassOnChange() {
	lsbc.clear();
}

UI_AirportTransferTemplate.getBookingClassesForCC = function() {
	
	var data = {};
	
	data['cabinClassCode'] = $('#cabinClassCode').val();
	
	return $.ajax({
		url : 'showAirportTransferTemplate!loadBookingClassesAPTTemplate.action',
		type : 'post',
		data : data,
		success :function (response) {

	        (new Function(response.bookingClasses))(); 

	    }								
	});
			
}

UI_AirportTransferTemplate.getBookingClasses = function() {
	
	var strBookingClasses = lsbc.getSelectedDataWithGroup();
	var newBookingClasses;
	if (strBookingClasses.indexOf(":") != -1) {
		strBookingClasses = replaceall(strBookingClasses, ":", ",");
	}
	if (strBookingClasses.indexOf("|") != -1) {
		newBookingClasses = replaceall(strBookingClasses, "|", ",");
	} else {
		newBookingClasses = strBookingClasses;
	}
	
	return newBookingClasses;
	
}



		