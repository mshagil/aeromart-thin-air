function UI_AirportTransfer() {};

$(document).ready(function() {
	
	$("#divSearch").decoratePanel("Search Airport Transfers");
	$("#divResultsPanel").decoratePanel("Airport Transfers");
	$("#divDispTransfer").decoratePanel("Add/Modify Airport Transfers");		
	
	$('#btnSearch').decorateButton();
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnSave').decorateButton();
	
	UI_AirportTransfer.disableControls(true);
	UI_AirportTransfer.disableButtons();
	UI_AirportTransfer.buildGrid();
	
	$('#btnSearch').click(function (){ 
		UI_AirportTransfer.searchClicked()
	});
	
	$('#btnAdd').click(function (){
		UI_AirportTransfer.addClicked();
	});
	
	$('#btnEdit').click(function (){
		UI_AirportTransfer.editClicked();
	});
	
	$('#btnSave').click(function (){
		UI_AirportTransfer.saveClicked();
	});
	
	$('#btnReset').click(function (){
		UI_AirportTransfer.resetClicked();
	})
	
});

UI_AirportTransfer.buildGrid = function (){
	
	$("#tblAirportTransfers").jqGrid({ 
		url:'showAirportTransfer!search.action',
		datatype: "json",	
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,	
			  id: "0"				  
			},													
	   colNames:['&nbsp;','Airport Code','Service Provider Name', 'Service Provider Address','Service Provider Number', 'Service Provider Email', 'Status','version','createdBy','createdDate','modifiedBy','modifiedDate', 'airportTransferId'], 
	   colModel:[ {name:'id', width:30, jsonmap:'id'},   
		          {name:"transfer.airportCode",		    index: 'airportCode',      width:120, align:"left" },
		          {name:"transfer.providerName",		index: 'providerName',     width:180, align:"left" },	
		          {name:"transfer.providerAddess",		index: 'providerAddess',   width:200, align:"left" },	
		          {name:"transfer.providerNumber",		index: 'providerNumber',   width:180, align:"left" },	
		          {name:"transfer.providerEmail",		index: 'providerEmail',    width:180, align:"left" },	
		          {name:"transfer.status",				index: 'status',           width:80, align:"left" },
		          {name:"transfer.version",			    index: 'version',          hidden:true },	
		          {name:"transfer.createdBy",			index: 'createdBy',        hidden:true },	
		          {name:"transfer.createdDate",		    index: 'createdDate',      hidden:true },	
		          {name:"transfer.modifiedBy",			index: 'modifiedBy',       hidden:true },	
		          {name:"transfer.modifiedDate",		index: 'modifiedDate',     hidden:true },	
		          {name:"transfer.airportTransferId",   index: 'airportTransferId',hidden:true }
			   ],
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#transferpager'),
		rowNum:10,						
		viewrecords: true,
		height:170,
		width:910,	
		onSelectRow: function(rowid){
			UI_AirportTransfer.rowClick(rowid);
			
		},
	   	loadComplete: function (e){	   
			//
	   	}
	}).navGrid("#divAirportMsgPager",{refresh: false, edit: false, add: false, del: false, search: false});
}

UI_AirportTransfer.searchClicked = function (){
	var postData = {};
	postData['searchCode']= $('#searchAirport').val();
	postData['searchStatus']= $('#selStatus').val();
	
	$("#tblAirportTransfers").setGridParam({page:1,postData:postData});
 	$("#tblAirportTransfers").trigger("reloadGrid");
 	UI_AirportTransfer.clearTransferForm();
 	UI_AirportTransfer.disableControls(true);
 	UI_AirportTransfer.disableButtons();
}

UI_AirportTransfer.disableButtons = function (){
	$("#btnEdit").disableButton();
	$("#btnReset").disableButton();
	$("#btnSave").disableButton();
}

UI_AirportTransfer.isPageEdited = function () {
	return top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId));
}

UI_AirportTransfer.hideMessage = function (){
	top[2].HidePageMessage();
}	

UI_AirportTransfer.setPageEdited = function (isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	UI_AirportTransfer.hideMessage();
}

UI_AirportTransfer.clearTransferForm = function (){
	$('#frmAirportTransfer')[0].reset();
	$("#version").val('-1');
	$("#rowNo").val('');
	$("#providerNumber").val('');
	$("#countryCode").val('');
	$("#areaCode").val(''); 
	$("#contactNo").val(''); 
	$("#createdBy").val('');
	$("#modifiedBy").val('');
	$("#modifiedDate").val('');
	$("#airportTransferId").val('');
}

UI_AirportTransfer.rowClick = function (rowid){
	UI_AirportTransfer.clearTransferForm();
 	$("#tblAirportTransfers").GridToForm(rowid,"#frmAirportTransfer");
 	
 	var providerNumber = $("#tblAirportTransfers").getCell(rowid,'transfer.providerNumber');
 	UI_AirportTransfer.populateProviderContact(providerNumber);
 	
	if($("#tblAirportTransfers").getCell(rowid,'transfer.status') == 'ACT'){
		$("#chkStatus").attr('checked', true);
	} else {
		$("#chkStatus").attr('checked', false);
	}
	$('#btnEdit').enableButton();
	UI_AirportTransfer.disableControls(true);
	
}

UI_AirportTransfer.disableControls = function (cond){
	$("#divDispTransfer  input").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("#divDispTransfer  select").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("#btnClose").attr('disabled', false);
}

UI_AirportTransfer.addClicked = function (){
	UI_AirportTransfer.disableControls(false);
	UI_AirportTransfer.clearTransferForm();
						
	$('#btnEdit').disableButton();
	$("#btnSave").enableButton();
	$("#btnReset").enableButton();		
	
}

UI_AirportTransfer.editClicked = function (){
	UI_AirportTransfer.disableControls(false);
	$("#btnSave").enableButton();
	$("#btnReset").enableButton();	
}

UI_AirportTransfer.saveClicked = function (){
	var stat = "INA";
	if($('#chkStatus').attr('checked')== 'checked'){
		stat ="ACT";
	}
	$("#status").val(stat);
	
	if(!UI_AirportTransfer.validateSave()){
		return false;
	}
	
	$('#frmAirportTransfer').ajaxSubmit({
		dataType    : 'json', 
		cache       : false,
		beforeSubmit: UI_AirportTransfer.beforeSave, 
		success     : UI_AirportTransfer.saveSuccess		
	});
}

UI_AirportTransfer.validateSave = function (){
	
	if($("#airportCode").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Airport Code cannot be empty");			
		return false;
	}else if($("#providerName").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Provider Name cannot be empty");
		$("#providerName").focus();
		return false;
	}else if($("#providerAddess").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Provider Address cannot be empty");
		$("#providerAddess").focus();
		return false;
	}else if($("#countryCode").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Provider Country Code cannot be empty");
		$("#countryCode").focus();
		return false;
	}else if($("#areaCode").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Provider Area Code cannot be empty");
		$("#areaCode").focus();
		return false;
	}else if($("#contactNo").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Provider Contact Code cannot be empty");
		$("#contactNo").focus();
		return false;
	}else if(getText("countryCode").length < 2 || getText("areaCode").length < 2 || getText("contactNo").length < 2){
		UI_Common.showCommonError("Error", "Provider Number is invalid");
		return false;
	}else if($("#providerEmail").isFieldEmpty()){
		UI_Common.showCommonError("Error", "Provider Email cannot be empty");
		$("#providerEmail").focus();
		return false;
	}else if(!checkEmail(trim(getText("providerEmail")))){
		UI_Common.showCommonError("Error", "Provider Email is incorrect");
		$("#providerEmail").focus();
		return false;
	 }
    $("#providerNumber").val(getText("countryCode") + "-" + getText("areaCode") + "-" + getText("contactNo"));	
  return true;	
}

UI_AirportTransfer.beforeSave = function (){
	top[2].ShowProgress();
}

UI_AirportTransfer.saveSuccess = function (response){
	top[2].HideProgress();
	if(response.msgType != 'Error'){
		//$('#frmAirportTransfer').clearForm();
		UI_AirportTransfer.clearTransferForm();
		var postData = {};
		postData['searchCode']= $('#searchAirport').val();
		postData['searchStatus']= $('#selStatus').val();
		
		$("#tblAirportTransfers").setGridParam({page:1,postData:postData});
	 	$("#tblAirportTransfers").trigger("reloadGrid");
	 	UI_AirportTransfer.disableControls(true);
	 	$('#btnEdit').disableButton();
		$("#btnSave").disableButton();
	}	
	UI_Common.displayMessage (response);	
}

UI_AirportTransfer.resetClicked = function () {
    if($("#tblAirportTransfers").getGridParam("selrow") != null){
    	UI_AirportTransfer.rowClick($("#tblAirportTransfers").getGridParam("selrow"));
     } else {
    	 $("#frmAirportTransfer").clearForm();
    	 UI_AirportTransfer.searchClicked();
     }
}

UI_AirportTransfer.populateProviderContact = function (contact){
	var noArr = contact.split("-");
	 if(noArr[0] != undefined && noArr[0].length >0){
		$("#countryCode").val(noArr[0]); 
	 }
	 if(noArr[1] != undefined && noArr[1].length >0){
		$("#areaCode").val(noArr[1]); 
	 }
	 if(noArr[2] != undefined && noArr[2].length >0){
		$("#contactNo").val(noArr[2]); 
	 }
}

//Custom Regex's
function validateWithComma(val){
	customValidate(val,/[`!@#$%*&?\"\'\[\]{}()|\\+=_><:;^~]/g);
}

function positiveNumberCustomValidate(objTxt){
	var strValue = objTxt.value;
	if (!isPositiveInt(strValue)) {
		objTxt.value = removeChars(strValue,/[a-zA-Z`!@#$%\s&*?\"\'\[\]{}()|\\\/=><:.,;^~_+-]/g);
	}	
}

function isCustomAlphaWhiteSpace(s){return RegExp("^[a-zA-Z \w\s]+$").test(s);}

function customAlphaWhiteSpaceValidate(objTxt){
	var strValue = objTxt.value;
	if (!isCustomAlphaWhiteSpace(strValue)) {
		objTxt.value = removeChars(strValue,/[0-9`\/\'\"!@#$%&*?\[\]{}()|\\\/><+-=_:.,;^~]/g);
	}	
	
}
			