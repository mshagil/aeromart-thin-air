/**
 * ManageCharges.js
 * 
 * @author Baladewa welathanthri
 *  
 */

function ManageCharges(){};

ManageCharges.showFareDefByDepCountryCurrency = 'N'
ManageCharges.isLoadAgents = false;
ManageCharges.isLoadStations = false;
$(function(){
	ManageCharges.ready();
});


ManageCharges.ready = function(){
	$("#txtDateFrom").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true, maxDate:'+1Y', yearRange:'-1:+1', dateFormat: 'd/m/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtDateTo").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true, maxDate:'+1Y', yearRange:'-1:+1', dateFormat: 'd/m/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtDateFrom").blur(function(){dateChk('txtDateFrom')});
	$("#txtDateTo").blur(function(){dateChk('txtDateTo')});
	$("#btnSearch").click(function(){ManageCharges.searchData()});
	$("#btnAdd").click(function(){ManageCharges.buttonAddClick()});
	$("#btnGAdd").click(function(){ManageCharges.addChargeRateRow(null);})
	$("input[name='radCat1']").click(function(){ManageCharges.radioCategoryClick(null)});
	$("input[name='radCurr']").click(function(){ManageCharges.radioCurrencyClick(null,"-1")});
	$("#btnAddSeg").click(function(){ManageCharges.addsement()});
	$("#btnRemSeg").click(function(){ManageCharges.removesement()});
	$("#tabs").tabs({
		select: function(event, ui){
			if(ui.index == 3){
				if(ManageCharges.isLoadStations == false)
					ManageCharges.tabPOSClick();
			}
			if(ui.index == 4){
				if(ManageCharges.isLoadAgents == false)
					ManageCharges.tabAgentClick();
			}
		}
	});
	//ManageCharges.tabPOSClick();
	//ManageCharges.tabAgentClick();
	$("#btnSave").click(function(){ManageCharges.bttonSaveClick()});
	$("#txtDesc").keypress(function(){ChargesValidate.replaceInvalids($(this))});
	$("#btnEdit").click(function(){ManageCharges.buttonEditClick()})
	$("#btnEdit").disable();
	$("#btnDelete").disable();
	$("#tabPSO").showHideTabs(false,"Top");
	$("#tabAgent").showHideTabs(false, "Top");
	ManageCharges.initilizePage();
	ManageCharges.addChargeRateRow(null);
};

ManageCharges.initilizePage = function(){
	ManageCharges.showFareDefByDepCountryCurrency = 'N'
	chargeCodeData = [["ADJCHG1","ADJCHG1"],["ADJCHG2","ADJCHG2"],["CNX","CNX"],["CNX1","CNX1"],["ADJCHG5","ADJCHG5"]];
	groupData = [["ADJ","ADJ"],["CNX","CNX"],["ESU","ESU"],["INF","INF"],["MOD","MOD"]];
	categoryData = [["Global","Global"],["POS","POS"],["OND","OND"]];
	applyToData = [["All","All"],["Adult","Adult"],["Infant","Infant"],["Chaid","Child"],["Adult-Infant","Adult-Infant"],["Adult-Child","Adult-Child"],["Reservation","Reservation"],["Ond","Ond"]];
	falgData = [["V","V"],["PF","PF"],["PTF","PTF"], ["PFS","PFS"],["PTFST","PTFST"]];
	airportsData = [["BWG","BWG"],["BSR","BSR"],["DBX","DBX"],["EBL","EBL"]];
	currencyData = [["USD","USD"],["AED","AED"],["AUD","AUD"],["GBP","GBP"]];
	addSelCategoryGroupData = [["0","ADJ"],["0","CNX"],["0","ESU"],["0","INF"],["0","MOD"],["0","ISU"],["1","SUR"],["1","TAX"]]
	$("#selChargeCode").fillDropDown({dataArray:chargeCodeData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selGroup").fillDropDown({dataArray:groupData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selCategory").fillDropDown({dataArray:categoryData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selApplyTo").fillDropDown({dataArray:applyToData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#AddSegselGroup").fillDropDown({dataArray:addSelCategoryGroupData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selDepature").fillDropDown({dataArray:airportsData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selCurrencyCode").fillDropDown({dataArray:currencyData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selPer").fillDropDown({dataArray:falgData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selArrival").fillDropDown({dataArray:airportsData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selVia1").fillDropDown({dataArray:airportsData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selVia2").fillDropDown({dataArray:airportsData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selVia3").fillDropDown({dataArray:airportsData, keyIndex:0, valueIndex:1, firstEmpty:false});
	$("#selVia4").fillDropDown({dataArray:airportsData, keyIndex:0, valueIndex:1, firstEmpty:false});
	if (ManageCharges.showFareDefByDepCountryCurrency == "N") {
		$('#divCurrSelect').show();
	} else {
		$('#divCurrSelect').hide();
	}
	ManageCharges.radioCategoryClick(null);
	ManageCharges.radioCurrencyClick(null,"-1");
	ManageCharges.searchChargesDataProcess();
	disableAllControles();
};

ManageCharges.radioCurrencyClick = function(currType,curr){
	var currency = (currType == null) ? $("input[name='radCurr']:checked").val() : currType;
	if (currency == "BASE_CURR"){
		$("#radCurr").attr("checked", true);
		$("#selCurrencyCode").disable();
	}else if(currency == "SEL_CURR"){
		$("#radSelCurr").attr("checked", true);	
		$("#selCurrencyCode").enable();
	}
	$("#selCurrencyCode").val(curr);
};

ManageCharges.searchData = function() {	
	var newUrl = 'showLoadChargesJsp!gridV2.action?';
 	newUrl += 'chargeCode=' + $("#selChargeCode").val();
 	newUrl += '&group=' + $("#selGroup").val();
 	newUrl += '&category=' + $("#selCategory").val();
 	newUrl += '&radRateCat=' + $("input[name='radRateCat']:checked").val();
 	newUrl += '&dateFrom=' + $("#txtDateFrom").val();
 	newUrl += '&dateTo=' + $("#txtDateTo").val();	
 	
 	$("#spnMCPager").clearGridData();
 	$.getJSON(newUrl, function(response){ 
 		$("#spnMC")[0].addJSONData(response); 		
 	});	
}

ManageCharges.buttonAddClick = function(){
	enableAllControles();
	$("#txtChargeCode").val("");
	$("#AddSegselGroup").val("");
	$("#txtDesc").val("");
	$("#selJourney").val("A");
	$("#selApplyTo").val("All");
	$("#selOndSeg").val("A");
	$("#selChannel").val("-1");
	$("#chkRefundable").attr("checked", false);
	$("#chkStatus").attr("checked", true);
	$( $("input[name='radDept1']").get(0) ).attr("checked", true);
	$("#chkRevenue").attr("checked", false);
	$("#txtRevenue").val("");
	$("#chkInwardTrn").val("N");
	$("#chkOutwardTrn").val("N");
	$("#chkAllTrn").val("N");
	$("#hdnMinTrnDuration").val("");
	$("#hdnMaxTrnDuration").val("");
	ManageCharges.radioCategoryClick("GLOBAL");
	ManageCharges.radioCurrencyClick("BASE_CURR","-1");
};

ManageCharges.buttonEditClick = function(){
	var selRow = jQuery("#spnMC").getGridParam("selrow");
	if (selRow > 1){
		enableAllControles();
	}else{
		alert("please select a row");
	}
}

ManageCharges.searchChargesDataProcess = function(){
	var mydata = null;
	jQuery("#spnMC").jqGrid({   
		datatype: function(postdata) {			
			postdata["chargeCode"] = $("#selChargeCode").val();
			postdata["group"] = $("#selGroup").val();
			postdata["category"] = $("#selCategory").val();
			postdata["radRateCat"] = $("input[name='radRateCat']:checked").val();
			postdata["dateFrom"] = $("#txtDateFrom").val();
			postdata["dateTo"] = $("#txtDateTo").val();			
	        jQuery.ajax({
	        //TODO perform correct action
	           url: 'showLoadChargesJsp!gridV2.action',
	           data:postdata,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 $("#spnMC")[0].addJSONData(mydata);            	
	              }
	           }
	        });
	    },
		colNames:['&nbsp;','Charge Code','Description', 'Group', 'Category <br>Criteria','Category','Value',
		          'Apply To','Refund','No. of <br>Charges','Status','Dep/Arr <br>Charges','OND <br>Segment','Journey <br>Type'],
	   	colModel:[  {name:'id', width:30,label:"id"},
	   	            {name:'chargeCode',index:'ruleCode', width:75 , align:"left" },
	   	            {name:'description', width:120 ,align:"left" },
	   	            {name:'group', width:70, align:"left" },
	   	            {name:'categoryCri', width:75, align:"left" },
	   	            {name:'category', width:75, align:"center" },
	   	            {name:'value', width:50, align:"center" },		
	   	            {name:'applyTo', width:70, align:"left" },
	   	            {name:'refund', width:50, align:"center" },
	   	            {name:'NoOfChages', width:60, align:"right" },
	   	            {name:'status', width:60, align:"center" },
	   	            {name:'depArrCharge', width:70, align:"center" },
	   	            {name:'ondSeg', width:40, align:"center" },
	   	            {name:'journeyType', width:40, align:"center" }
	   	],
		height: 95,
	   	rowNum:10,
	   	jsonReader: { 
			root: "rows",
		  	page: "page",
		  	total: "total",
		  	records: "records",
			repeatitems: false,
			id: "0" 
		},
	   	pager: jQuery('#spnMCPager'),
	   	sortname: 'id',
		multiselect: false,
		viewrecords: true,
	    sortorder: "desc",
	    caption:"Fare Rules",
	    loadui:'block',
	    onSelectRow: function(rowid){
	    	ManageCharges.FillGridToForm(rowid, mydata.rows);
	    }
	}).navGrid("#fareRulePager",{refresh: true, edit:false,add:false,del:false});
	objDG.loaded = true;
	$("#tabs").show();
};

ManageCharges.FillGridToForm = function(rowid, dataObj){
	$("#btnEdit").enable();
	$("#btnDelete").enable();
	$("#txtChargeCode").val(dataObj[rowid-1].chargeCode);
	$("#AddSegselGroup").val(dataObj[rowid-1].group);
	$("#txtDesc").val(dataObj[rowid-1].description);
	$("#selJourney").val(dataObj[rowid-1].journeyType);
	$("#selApplyTo").val(dataObj[rowid-1].applyTo);
	$("#selOndSeg").val(dataObj[rowid-1].ondSeg);
	$("#chkRefundable").attr("checked", dataObj[rowid-1].refund == "Y" ? true : false);
	$("#chkStatus").attr("checked", dataObj[rowid-1].status == "Active" ? true : false);
	if ( $.trim (dataObj[rowid-1].depArrCharge) == "D" ){
		$( $("input[name='radDept1']").get(0) ).attr("checked", true);
	}else{
		$( $("input[name='radDept1']").get(1) ).attr("checked", true);
	}
	$("#chkRevenue").attr("checked", dataObj[rowid-1].isRevenue == "Y" ? true : false);
	$("#txtRevenue").val(dataObj[rowid-1].revenue);
	$("#chkInwardTrn").val(dataObj[rowid-1].inWTransit);
	$("#chkOutwardTrn").val(dataObj[rowid-1].outWTransit);
	$("#chkAllTrn").val(dataObj[rowid-1].ondTransit);
	$("#hdnMinTrnDuration").val(dataObj[rowid-1].minTranDuration);
	$("#hdnMaxTrnDuration").val(dataObj[rowid-1].minTranDuration);
	ManageCharges.radioCurrencyClick(dataObj[rowid-1].currType, dataObj[rowid-1].currency);
	ManageCharges.radioCategoryClick(dataObj[rowid-1].category);
	ManageCharges.addChargeRateRow(dataObj[rowid-1].chgRates);
	disableAllControles();
	$("#tabs").tabs("option", "active",0);
};

ManageCharges.radioCategoryClick = function(cat){
	var category = (cat == null) ? $("input[name='radCat1']:checked").val() : cat;
	if (category == "POS"){
		$("#radPos").attr("checked", true);	
		$("#tabPSO").showHideTabs(true, "Top");
		$("#tabAgent").showHideTabs(true, "Top");
		$("#spnSegment").show();
	}else if(category == "OND"){
		$("#radSegment").attr("checked", true);	
		$("#tabPSO").showHideTabs(false, "Top");
		$("#tabAgent").showHideTabs(true, "Top");
		$("#spnSegment").show();
	}else{
		$("#radCat1").attr("checked", true);
		$("#tabPSO").showHideTabs(false,"Top");
		$("#tabAgent").showHideTabs(false, "Top");
		$("#spnSegment").hide();
	}
};

ManageCharges.addChargeRateRow = function (dataObj){
	if (dataObj == null){
		var lastRow = $("#ChargeRateTemplate").parent().find("tr:last").attr("id");
		var objectIndex = parseInt ( ( lastRow.indexOf("_") > -1 ) ? lastRow.split("_")[1] : 0 );
		var clonedTempId = "ChargeRateTemplate_" + (objectIndex + 1);
		var clone = $("#ChargeRateTemplate").clone();
		clone.attr("id", clonedTempId );
		clone.appendTo($("#ChargeRateTemplate").parent());
		elementIdAppend =  "ChargeRate[" + (objectIndex + 1) + "].";
		$("#"+clonedTempId).find("input").each(function(intIndex){
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		$("#"+clonedTempId).find("select").each(function(intIndex){
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		$("#"+clonedTempId).find("label").each(function(intIndex){
			$(this).attr("id",  elementIdAppend +  this.id);
			$(this).html((objectIndex + 1));
		});
		$("#"+clonedTempId).show();
	}else{
		var temp = $("#ChargeRateTemplate");
		var tempParent = $("#ChargeRateTemplate").parent();
		tempParent.empty();
		tempParent.append(temp);
		$("#ChargeRateTemplate").iterateTemplete({templeteName:"ChargeRateTemplate", data:dataObj, dtoName:"ChargeRate"});
	}
};

ManageCharges.addsement = function(){
	var isContained = false;
	top[2].HidePageMessage();
	var dept = $("#selDepature option:selected").text();
	var arr = $("#selArrival option:selected").text();
	var via1 = $("#selVia1 option:selected").text();
	var via2 = $("#selVia2 option:selected").text();
	var via3 = $("#selVia3 option:selected").text();
	var via4 = $("#selVia4 option:selected").text();

	var deptVal = $("#selDepature").val();
	var arrVal = $("#selArrival").val();
	var via1Val = $("#selVia1").val();
	var via2Val = $("#selVia2").val();
	var via3Val = $("#selVia3").val();
	var via4Val = $("#selVia4").val();

	if (dept == "") {
		showERRMessage("departer required");
		$("#selDepature").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage("departure inactive");
		$("#selDepature").focus();

	} else if (arr == "") {
		showERRMessage("arrival required");
		$("#selArrival").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage("arrival inactive");
		$("#selArrival").focus();

	} else if (dept == arr) {
		showERRMessage("departure abd arrival same");
		$("#selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage("arrival abd via points can not be same");
		$("#selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage("departure abd via points can not be same");
		$("#selDepature").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage("vai points and not in a sequency");
		$("#selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage("vai points and not in a sequency");
		$("#selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage("vai points and not in a sequency");
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage("vai point can not be same");
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage("vai point can not be same");
		$("#selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage("vai point can not be same");
		$("#selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage("vai point can not be same");
		$("#selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + "is inactive vau point");
		$("#selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + "is inactive vau point");
		$("#selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + "is inactive vau point");
		$("#selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + "is inactive vau point");
		$("#selVia4").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}
		str += "/" + arr;

		var control = document.getElementById("selSelected");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage("this entry is already exist");
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
		}
	}
	
};

ManageCharges.removesement = function(){
	var control = document.getElementById("selSelected");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
		}
	}
};

ManageCharges.bttonSaveClick = function(){
	if ( ChargesValidate.validate() ) {
		alert("saving..");
	}
};

ManageCharges.getSearchData = function() {
	var strCC = $("#selChargeCode option:selected").text();
	var strGroup = $("#selGroup option:selected").text()
	var selCat = $("#selCategory option:selected").text()
	var strToDate = $("#txtDateTo").val();
	var strFeromDate = $("#txtDateFrom").val();
	var searchStr = strCC + "," + strGroup + "," + selCat + ","
			+ $("input[name='radRateCat']:checked").val() + ",";

	if (strToDate == "") {
		searchStr += "NoData,";
	} else {
		searchStr += strToDate + ",";
	}

	if (strFeromDate == "") {
		searchStr += "NoData,";
	} else {
		searchStr += strFeromDate + ",";
	}
	return searchStr;
};

ManageCharges.tabPOSClick = function(){
	//TODO call the action to load the stations and process with the following function by giveing responce data
	ManageCharges.isLoadStations = true;
	ManageCharges.tabPOSDataProcess();
};

ManageCharges.tabPOSDataProcess = function (){
	var POSData = [["ADJCHG1","ADJCHG1"],["ADJCHG2","ADJCHG2"],["CNX","CNX"],["CNX1","CNX1"],["ADJCHG5","ADJCHG5"]];//assing to resonce data
	$("#lstStations").multiSelect({
		data:POSData,
		label:"Stations",
		leftListBox:"lstStations"
	});
	//$("#spnSta_1").click(function(){$.fn.multiSelect.allRight()});
	//$("#spnSta_4").click(function(){$.fn.multiSelect.allLeft()});
	//$("#spnSta_3").click(function(){$.fn.multiSelect.oneLeft()});
	//$("#spnSta_2").click(function(){$.fn.multiSelect.oneRight()});
};

ManageCharges.tabAgentClick = function(){
	//TODO call the action to load the Agents and process with the following function by giveing responce data
	ManageCharges.isLoadAgents = true;
	ManageCharges.tabAgentDataProcess();
};

ManageCharges.tabAgentDataProcess = function(){
	var POSData = [["ADJCHG1","ADJCHG1"],["ADJCHG2","ADJCHG2"],["CNX","CNX"],["CNX1","CNX1"],["ADJCHG5","ADJCHG5"]];//assing to resonce data
	$("#lstAgents").multiSelect({
		data:POSData,
		label:"Agents",
		leftListBox:"lstAgents"
	});
	//$("#spnAgents_1").click(function(){$.fn.multiSelect.allRight()});
	//$("#spnAgents_4").click(function(){$.fn.multiSelect.allLeft()});
	//$("#spnAgents_3").click(function(){$.fn.multiSelect.oneLeft()});
	//$("#spnAgents_2").click(function(){$.fn.multiSelect.oneRight()});
};



function disableAllControles(){
	$(".formSection").find("input").disable();
	$(".formSection").find("select").disable();
	$(".formSection").find("textarea").disable();
};

function enableAllControles(){
	$(".formSection").find("input").enable();
	$(".formSection").find("select").enable();
	$(".formSection").find("textarea").enable();
}