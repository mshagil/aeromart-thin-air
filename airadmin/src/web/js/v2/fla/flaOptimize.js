	UI_FLA_Optimize.screenID               = "SC_FLGT_002";
	UI_FLA_Optimize.flightOptimizeDataList = null;
	UI_FLA_Optimize.flightID               = null;
	UI_FLA_Optimize.selectedRowId          = null;
	UI_FLA_Optimize.detailsLength		   = 0;
	UI_FLA_Optimize.blnValidated		   = true;
	
	/**
	 * UI_FLA_Optimize for FLA Optimize UI related functionalities
	 */
	function UI_FLA_Optimize(){}
	
	$(document).ready(function(){
		$("#btnOptCancel").click(function() {UI_FLA_Optimize.btnCancelClick();});
		$("#btnSave").click(function() {UI_FLA_Optimize.saveOnClick();});
		$("#btnEdit").click(function() {UI_FLA_Optimize.editOnClick();});
		UI_FLA_Optimize.initialize();
		UI_FLA_Optimize.selectedRowId = 0;	
	});
	
	
	UI_FLA_Optimize.initialize =  function(){		
		$("#btnOptCancel").attr('disabled','disabled');		
		$("#btnSave").attr('disabled','disabled');
		$("#btnSave").hide();		
		$("#btnEdit").attr('disabled','disabled');				
	}
	
	UI_FLA_Optimize.submitSuccess =  function(response){
		top[2].HideProgress();
		if(response.success){
			UI_FLA_Optimize.flightOptimizeDataList = response.flightSummaryDTOList;
			UI_FLA_Optimize.createSummaryGrid();
			UI_FLA_Optimize.fillSummaryData();
		}
		else {
			showERRMessage("Optimize Search Error.");
		}		
	}
	
	/**
	 * Constructs the Summary grid on the left panel
	 */
	UI_FLA_Optimize.createSummaryGrid = function(inParams){
		
		var gridimgpath = '../themes/default/images/jquery';
		//construct grid
		$("#tblSummaryData").jqGrid({
			datatype: "local",
			height: 120,
			width: 400,
			colNames:['InvID','SegInvID','Version','Segment', 'COS', 'Allc.ADT', 'Allc.Inf','Sold AD/Inf','OnHold AD/Inf','Oversell','Curtail','Avail ADT/Inf'],
			colModel:[
				{name:'fccInventoryID', index:'fccInventoryID', width:0, hidden:true },
				{name:'fccsInventoryID', index:'fccsInventoryID', width:0, hidden:true },
				{name:'version', index:'version', width:0, hidden:true },
				{name:'segmentCode',  index:'segmentCode', width:50, align:"center",sortable:false },
				{name:'cabinClass', index:'cabinClass', width:50, align:"center",sortable:false },
				{name:'allocatedAdults', index:'allocatedAdults', width:50, align:"left",sortable:false },
				{name:'allocatedInfants', index:'allocatedInfants', width:50, align:"right",sortable:false,editable:true,editoptions:{maxLength:"2",onchange:'UI_FLA_Optimize.editGridCell(this)'} },
				{name:'soldAdultsInfants', index:'soldAdultsInfants', width:50, align:"right",sortable:false },
				{name:'onholdAdultsInfants', index:'onholdAdultsInfants', width:50, align:"right",sortable:false },
				{name:'overSellSeats', index:'overSellSeats', width:50, align:"right",sortable:false,editable:true,editoptions:{maxLength:"2",onchange:'UI_FLA_Optimize.editGridCell(this)'} },
				{name:'curtailedSeats', index:'curtailedSeats', width:50, align:"right",sortable:false,editable:true,editoptions:{maxLength:"2",onchange:'UI_FLA_Optimize.editGridCell(this)'} },
				{name:'availableAdultsInfants', index:'availableAdultsInfants', width:50, align:"right",sortable:false}
			],		
			editurl: "clientArray",
			imgpath: gridimgpath,
			multiselect: false,
			caption: "Segment Allocation",
			viewrecords: true,
			onSelectRow: function(rowid){UI_FLA_Optimize.onRowClicked(rowid);}
		});
	}
	
	UI_FLA_Optimize.fillSummaryData = function(){
		UI_FLA_Optimize.fillGridData({id:"#tblSummaryData", data:UI_FLA_Optimize.flightOptimizeDataList});
		
	}
	
	/**
	 * Triggered when  a user clicked on a summary grid row.
	 * This method will restore previously selected row data and make the new row editable.
	 */
	UI_FLA_Optimize.onRowClicked = function(rowid){
		hideERRMessage();
		if(UI_FLA_Optimize.selectedRowId!=rowid){
			$("#tblSummaryData").saveRow(UI_FLA_Optimize.selectedRowId, false, 'clientArray');
			UI_FLA_Optimize.selectedRowId = rowid;
			UI_FLA_Optimize.initializeBCGrid();
		}
	}
	
	UI_FLA_Optimize.initializeBCGrid =  function(){
		UI_FLA_Optimize.createDetailsGrid();
		$("#btnSave").attr('disabled','disabled');
		$("#btnSave").hide();
		$("#btnEdit").removeAttr('disabled');
		$("#btnEdit").show();
	}
	
	UI_FLA_Optimize.editOnClick = function(){
		if(DATA_MIS.initialParams.updateSegmentAlloc ==1){
			$("#btnSave").removeAttr('disabled');
			$("#btnSave").show();
			$("#btnEdit").attr('disabled','disabled');
			$("#btnEdit").hide();
			$("#btnOptCancel").removeAttr('disabled');	
			if(!jsClickedFlight.isFlown){
				$("#tblSummaryData").editRow(UI_FLA_Optimize.selectedRowId);
			}
			if(DATA_MIS.initialParams.updateBookingClassAlloc ==1){				
				var i           = 0;
				var selectedObj = UI_FLA_Optimize.flightOptimizeDataList[UI_FLA_Optimize.selectedRowId-1];
				var dataList    = selectedObj.bookingClassDetails;
				if(!jsClickedFlight.isFlown){
					for(i=0;i<UI_FLA_Optimize.detailsLength;i++){
						var rowData = dataList[i];
						if(!rowData.openReturnBc){
							$("#tblOptimizeDetails").editRow(i+1);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Construct the details grid on the right hand side panel
	 */
	UI_FLA_Optimize.createDetailsGrid = function(){
		var gridimgpath = '../themes/default/images/jquery';
		var selectedObj = UI_FLA_Optimize.flightOptimizeDataList[UI_FLA_Optimize.selectedRowId-1];
		var dataCollection= selectedObj.bookingClassDetails;
		UI_FLA_Optimize.detailsLength = dataCollection.length;
		//construct grid
		$("#tblOptimizeDetails").jqGrid({
			datatype: "local",
			height: 120,
			width: 450,
			colNames:['BCInvID','SegInvID','Version','BC','Allc.', 'Closed', 'Fixed Y/N', 'Priority Y/N','Sold','OnHold','Cancelled','Aqcuired','Availble','SegCode','CC','Action','OldAcion','OldStatus','OpenRT'],
			colModel:[
			    {name:'fccsbcInvId', index:'fccsbcInvId', width:0, hidden:true },
			    {name:'fccsInvId', index:'fccsInvId', width:0, hidden:true },
			    {name:'version', index:'version', width:0, hidden:true },
			    {name:'bookingClassCode', index:'bookingClassCode', width:60 },
				{name:'allocatedToBookingClass',  index:'allocatedToBookingClass', width:50, align:"center",sortable:false,editable:true,editoptions:{maxLength:"3",onchange:'UI_FLA_Optimize.editGridCell(this)'} },
				{name:'closed', index:'closed', width:40, align:"center",sortable:false,editable:true,edittype:"checkbox", formatter:'checkbox',editoptions:{value:'0:1'} },
				{name:'fixed', index:'fixed', width:50, align:"left",sortable:false },
				{name:'priority', index:'priority', width:50, align:"right",sortable:false,editable:true,edittype:"checkbox", formatter:'checkbox',editoptions:{value:'0:1'}},
				{name:'soldSeats', index:'soldSeats', width:50, align:"right",sortable:false },
				{name:'onholdSeats', index:'onholdSeats', width:50, align:"right",sortable:false },
				{name:'cancelledSeats', index:'cancelledSeats', width:50, align:"right",sortable:false},
				{name:'acquiredSeats', index:'acquiredSeats', width:50, align:"right",sortable:false},
				{name:'availableSeats', index:'availableSeats', width:50, align:"right",sortable:false},
				{name:'segmentCode', index:'segmentCode', width:0,hidden:true},
				{name:'cabinClassCode', index:'cabinClassCode', width:0,hidden:true},
				{name:'action', index:'action', width:0,hidden:true},
				{name:'oldAction', index:'oldAction', width:0,hidden:true},
				{name:'oldStatus', index:'oldAction', width:0,hidden:true},
				{name:'openReturnBc', index:'openReturnBc', width:0,hidden:true}
			],		
			editurl: "clientArray",
			imgpath: gridimgpath,
			caption: "Booking Class Allocation",
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){}
		});
		UI_FLA_Optimize.fillGridData({id:"#tblOptimizeDetails", data:dataCollection});
		setDisplay("div_Optimize_Details", true);
	}
	
	/** Sends the modified data to the server 
	 *  TODO: Validate data before submitting as in V1
	 * */
	UI_FLA_Optimize.saveOnClick = function(){
		var data = {};
		var selectedRow = UI_FLA_Optimize.selectedRowId;
		data['flightID'] = jsClickedFlight.flightID;
		//First get the selected row data of summary
		$("#tblSummaryData").saveRow(UI_FLA_Optimize.selectedRowId, false, 'clientArray');
		if(UI_FLA_Optimize.validateSave()){
			data['summary.allocatedInfants'] = $("#tblSummaryData").getRowData(selectedRow)["allocatedInfants"];
			data['summary.overSellSeats']    = $("#tblSummaryData").getRowData(selectedRow)["overSellSeats"];
			data['summary.curtailedSeats']   = $("#tblSummaryData").getRowData(selectedRow)["curtailedSeats"];
			data['summary.version']          = $("#tblSummaryData").getRowData(selectedRow)["version"];
			data['summary.segInventoryID']   = $("#tblSummaryData").getRowData(selectedRow)["fccsInventoryID"];
			data['summary.InvID']            = $("#tblSummaryData").getRowData(selectedRow)["fccInventoryID"];
			
			//Get all the edited data in details grid
			var selectedObj = UI_FLA_Optimize.flightOptimizeDataList[UI_FLA_Optimize.selectedRowId-1];
			var intLen  = selectedObj.bookingClassDetails.length;
			var dataList    = selectedObj.bookingClassDetails;
			for(var x=0;x<intLen;x++){
				var index = x+1;
				var rowData = dataList[x];
				if(!rowData.openReturnBc){
					var rowTR = $("#"+ index + "_allocatedToBookingClass").closest('tr');

					data['details['+x+'].fccsbcInvId']             = rowTR.find('td:eq(0)').text();
					data['details['+x+'].fccsInvId']               = rowTR.find('td:eq(1)').text();
					data['details['+x+'].version']                 = rowTR.find('td:eq(2)').text();	
					data['details['+x+'].bookingClassCode']        = rowTR.find('td:eq(3)').text();	

					data['details['+x+'].allocatedToBookingClass'] = $("#"+ index + "_allocatedToBookingClass").val();
					//For some reason the actual checked value is not reflecting, so checking the checked status
					if($("#"+ index + "_closed").attr('checked')){
						data['details['+x+'].closed'] = '1';
					}
					else{
						data['details['+x+'].closed'] = '0';
					}
					if($("#"+ index + "_priority").attr('checked')){
						data['details['+x+'].priority']= '1';
					}
					else{
						data['details['+x+'].priority']= '0';
					}
					data['details['+x+'].soldSeats']               = rowTR.find('td:eq(8)').text();		
					data['details['+x+'].onholdSeats']             = rowTR.find('td:eq(9)').text();		
					data['details['+x+'].cancelledSeats']          = rowTR.find('td:eq(10)').text();
					data['details['+x+'].acquiredSeats']           = rowTR.find('td:eq(11)').text();
					data['details['+x+'].segmentCode']             = rowTR.find('td:eq(13)').text();
					data['details['+x+'].cabinClassCode']          = rowTR.find('td:eq(14)').text();
					data['details['+x+'].action']          		   = rowTR.find('td:eq(15)').text();
					data['details['+x+'].oldAction']          	   = rowTR.find('td:eq(16)').text();
					data['details['+x+'].oldStatus']          	   = rowTR.find('td:eq(17)').text();

					$("#tblOptimizeDetails").saveRow(index, false, 'clientArray');
				}
				
			}			
			var url = "searchOptimize!saveOptimizedData.action";
			top[2].ShowProgress();
			$.ajax( {
				url : url,
				type : 'POST',
				data : data,
				dataType : 'json',
				success : function(response) {
				UI_FLA_Optimize.saveOptimaizeDataSuccess(response);
				},
				error : function(requestObj, textStatus, errorThrown) {
					UI_FLA.setErrorStatus(requestObj);
				}
			});
		}
	}
	
	UI_FLA_Optimize.saveOptimaizeDataSuccess = function(response){
		top[2].HideProgress();
		if(response.success){
			alert('Record Sucessfully Saved');
			UI_FLA_Optimize.submitSuccess(response);
			UI_FLA_Optimize.initializeBCGrid();
		}
		else{
			showERRMessage(response.messageTxt);
			UI_FLA_Optimize.initialize();
			UI_FLA_Optimize.fillSummaryData();
			UI_FLA_Optimize.initializeBCGrid();
		}
		
	}
	
	UI_FLA_Optimize.btnCancelClick = function(){
		if(UI_FLA_Optimize.selectedRowId>0){
			$("#tblSummaryData").restoreRow(UI_FLA_Optimize.selectedRowId);
			UI_FLA_Optimize.initializeBCGrid();
			$("#btnSave").attr('disabled','disabled');
			$("#btnSave").hide();
			$("#btnEdit").show();
			$("#btnEdit").removeAttr('disabled');	
		}
	
	}
	
	
	/** Changes the color of the edited cell in to red */
	UI_FLA_Optimize.editGridCell = function(obj){
		$(obj).css('color','red');
	}
	
		
	/***
	 * Fills the summary grid data, but make it non-editable until a user selects a row.
	 */
	UI_FLA_Optimize.fillGridData = function(p){
		var i = 0;
		p = $.extend({id:"",
					 data:null,
					 editable:false}, 
					p);
					
		$(p.id).clearGridData();
		
		if (p.data != null){
			$.each(p.data, function(){
				$(p.id).addRowData(i + 1, p.data[i]);
				i++;
			});
		}
	}
	/**
	 * Validation before save the data
	 */
	UI_FLA_Optimize.validateSave = function(){
		var selectedRow 	= UI_FLA_Optimize.selectedRowId;
		var allocatedInf 	= $("#tblSummaryData").getRowData(selectedRow)["allocatedInfants"];
		var overSellSeats 	= $("#tblSummaryData").getRowData(selectedRow)["overSellSeats"];
		var curtailedSeats	 = $("#tblSummaryData").getRowData(selectedRow)["curtailedSeats"];
		if((allocatedInf == null) || (allocatedInf == '')){
			UI_FLA_Optimize.blnValidated = false;
			showERRMessage("Allocated Infant Amount is Empty.");
			$("#tblSummaryData").editRow(UI_FLA_Optimize.selectedRowId);
			$("#"+ selectedRow + "_allocatedInfants").focus();
			return UI_FLA_Optimize.blnValidated;
		}else if((overSellSeats == null) || (overSellSeats == '')){
			UI_FLA_Optimize.blnValidated = false;
			showERRMessage("OverSell Seat Amount is Empty.");
			$("#tblSummaryData").editRow(UI_FLA_Optimize.selectedRowId);
			$("#"+ selectedRow + "_overSellSeats").focus();
			return UI_FLA_Optimize.blnValidated;
		}else if((curtailedSeats == null) || (curtailedSeats == '')){
			UI_FLA_Optimize.blnValidated = false;
			showERRMessage("Curtailed Seat Amount is Empty.");
			$("#tblSummaryData").editRow(UI_FLA_Optimize.selectedRowId);
			$("#"+ selectedRow + "_curtailedSeats").focus();
			return UI_FLA_Optimize.blnValidated;
		}else{		
				var selectedObj = UI_FLA_Optimize.flightOptimizeDataList[UI_FLA_Optimize.selectedRowId-1];		
				var intLen  = selectedObj.bookingClassDetails.length;
				for(var x=0;x<intLen;x++){
					var index = x+1;
					var rowData = selectedObj.bookingClassDetails[x];
					var allocatedBkngCls = $("#"+ index + "_allocatedToBookingClass").val();
					if(!rowData.openReturnBc){
						if((allocatedBkngCls == null) || (allocatedBkngCls == '')){
							UI_FLA_Optimize.blnValidated = false;
							showERRMessage("Allocated to BC Amount is Empty.");
							$("#"+ index + "_allocatedToBookingClass").focus();
							return UI_FLA_Optimize.blnValidated;
						}	
					}
				}			
		}
		hideERRMessage();		
		return UI_FLA_Optimize.blnValidated;
	}	