function UI_searchRollForwad() {

	var selectedRowData = null;
	var isNew = true;
	var maxViaPoint = 3;
	this.ready = function() {
		
		var tLink = $("<a href='javascript:void(0)'><img src='../../images/tri-down1_no_cache.gif' alt=''>&nbsp;Search</a>");
		$("#divBlPaxSearch").decoratePanel(tLink);
		$('#btnDelete').decorateButton();
		$("#btnSearch").click(searchRollForward);
		$("#btnDelete").click(deleteRollForward);
		$(" #txtFromSearch,#txtFromDetails").datepicker({
			minDate : "",
			showOn : "button",
			buttonImage : "../../images/calendar_no_cache.gif",
			buttonImageOnly : true,
			dateFormat : 'dd/mm/yy'
		}).keyup(function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				$.datepicker._clearDate(this);
			}
		});
		$(" #txtToSearch,#txtToDetails").datepicker({
			minDate : "",
			showOn : "button",
			buttonImage : "../../images/calendar_no_cache.gif",
			buttonImageOnly : true,
			dateFormat : 'dd/mm/yy'
		}).keyup(function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				$.datepicker._clearDate(this);
			}
		});

	};

	searchRollForward = function() {
		if(validateForm()){
			constructRollForwardBatchGrid();
		}
	};

	constructRollForwardBatchGrid = function() {
		
		var newUrl = "searchRollForwardCriteria!search.action";
		$("#jqGridsearchRollForwardContainer").empty();
		var gridTable = $("<table></table>").attr("id",
				"jqGridsearchRollForwardData");
		var gridPager = $("<div></div>")
				.attr("id", "jqGridsearchRollForwardPages");
		$("#jqGridsearchRollForwardContainer").prepend(gridTable, gridPager);

		var reqData = [];
		reqData['searchRollForwardCriteriaSearchTO.fromDate'] = ($('#txtFromSearch').val() == "") ? null
				: $('#txtFromSearch').val();
		reqData['searchRollForwardCriteriaSearchTO.toDate'] = ($('#txtToSearch').val() == "") ? null
				: $('#txtToSearch').val();
		reqData['searchRollForwardCriteriaSearchTO.flightNumber'] = ($('#txtFlightNoSearch').val() == "") ? null
				: $('#txtFlightNoSearch').val();
		reqData['searchRollForwardCriteriaSearchTO.batchId'] = ($('#txtBatchIdSearch')
				.val() == "") ? null : $('#txtBatchIdSearch').val();
		reqData['searchRollForwardCriteriaSearchTO.status'] = ($('#statusSearch').val() == "-") ? null 
				: $('#statusSearch').val();
		
		var dateFomatter = function(cellVal, options, rowObject) {
			var treturn = "&nbsp;";
			if (cellVal != null) {
				treturn = cellVal.split("T")[0];
			}
			return treturn;
		}
		
		var buttonFmt = function(arr, options, rowObject) {
			var fmtStr = '<span><u:hasPrivilege privilegeId="plan.invn.alloc.bc.roll.advance.cancel.batch">';
	        var btnCancelSeg = "<button type='button' title='Cancel Job' style='width:110px;margin:1px;' id='btnDelete' onClick='deleteRollForward("+ rowObject.criteria.batchSegId + ");'>"+"Terminate"+"</button></u:hasPrivilege>";
	        var viewLog = "<button type='button' title='View Log' style='width:110px;margin:1px;' id='btnLog' onClick='showRollforwardLink("+rowObject.id+");' >View Log</button>";
	
			if(rowObject.criteria.status == 'WAITING'){
				if(hasRollCancleBatchPrivilege=='true'){
		                fmtStr += btnCancelSeg;
				}
			}
			if(rowObject.criteria.status == 'DONE'
				|| rowObject.criteria.status == 'ERROR'){
				  fmtStr += viewLog;
			}
			return fmtStr + "</span>";
		}
		var temGrid = $("#jqGridsearchRollForwardData").jqGrid(
				{
					datatype : function(postdata) {
						$.ajax({
							url : newUrl,
							beforeSend : ShowProgress(),
							data : $.extend(postdata, reqData),
							dataType : "json",
							complete : function(jsonData, stat) {
								
								if (stat == "success") {
									mydata = eval("(" + jsonData.responseText
											+ ")");
									if (mydata.msgType != "Error") {
										$.data(temGrid[0], "gridData",
												mydata.rows);
										temGrid[0].addJSONData(mydata);

									} else {
										console.log(mydata.message);
									}
									HideProgress();
								}
							}
						});
					},
					width : '100%',
					height : '350px',
					colNames : [ 'Batch Seg Id', 'Batch Id', 'Flight No',
							'From', 'To', 'Created Date',
							'User', 'Status',''],
					colModel : [ {
						name : 'batchSegId',
						index : 'batchSegId',
						width : 100,
						jsonmap : "criteria.batchSegId"
					},{
						name : 'batchId',
						index : 'batchId',
						width : 100,
						jsonmap : "criteria.batchId"
					}, {
						name : 'flightNo',
						index : 'flightNo',
						width : 100,
						jsonmap : "criteria.flightNo"
					}, {
						name : 'fromDate',
						index : 'fromDate',
						width : 100,
						jsonmap : "criteria.fromDate",
						formatter : dateFomatter
					}, {
						name : 'toDate',
						index : 'toDate',
						width : 90,
						jsonmap : "criteria.toDate",
						formatter : dateFomatter
					}, {
						name : 'createdDate',
						index : 'createdDate',
						width : 100,
						jsonmap : "criteria.createdDate",
						formatter : dateFomatter
					}, {
						name : 'createdUser',
						index : 'createdUser',
						width : 70,
						jsonmap : "criteria.createdBy"
					}, {
						name : 'status',
						index : 'status',
						width : 110,
						align : "center",
						jsonmap : "criteria.status"
					}, {
						name : 'canceljob',
						index : 'canceljob',
						width : 110,
						align : "center",
						formatter : buttonFmt
					}
					],
					imgpath : "",
					pager : jQuery('#jqGridsearchRollForwardPages'),
					multiselect : false,
					viewrecords : true,
					rowNum : 20,
					sortable : true,
					altRows : true,
					altclass : "GridAlternateRow",
					jsonReader : {
						root : "rows",
						page : "page",
						total : "total",
						records : "records",
						repeatitems : false,
						id : "0"
					},
					onSelectRow : function(rowid) {
						setSelectedRow(rowid - 1);
						setField("hdnSelectID", rowid - 1);
					}

				}).navGrid("#jqGridsearchRollForwardPages", {
			refresh : true,
			del : false,
			add : false,
			edit : false,
			search : false
		});
	};

	var submitData = {};

	setSelectedRow = function(id) {
		var selectID = id % 20;
		selectedRowData = $.data($("#jqGridsearchRollForwardData")[0], "gridData")[selectID];
		console.log(selectedRowData);
	};

	showRollforwardLink = function(id){

		var selectID = (id-1)%20;
		selectedRowData = $.data($("#jqGridsearchRollForwardData")[0], "gridData")[selectID];
		createRollforwardHTML = function(auditLog){
			var outerDiv='';
			var tepDiv = $("<div></div>").css("clear", "both");
			var newDiv = $("<div></div>").css({"float": "left", "border-left":"1px solid #999",
					"padding": "0 5px", "width":225});
			var t;
			if(auditLog != null){
				t = $("<div></div>").append(auditLog);
			}else{
				t = $("<div>No Audit Log</div>");
			}
			newDiv.append(t);
			tepDiv.append(newDiv);
			var tempWidth = 3 * 240;
			tepDiv.css("width",tempWidth);
			outerDiv = $("<div></div>").append(tepDiv);
			return outerDiv;
		};
		$("#popUp").openMyPopUp({
			"width":720,
			"height":480,
			"appendobj":false,
			"headerHTML":$("<lable>Rollforward Audit Logs</label>").css("color", "white"),
			"bodyHTML": createRollforwardHTML(selectedRowData.criteria.auditLog),
			"footerHTML":""
		});
	};

	deleteRollForward = function(batchsegid) {
			
		submitData["searchRollForwardCriteriaSearchTO.batchSegId"] = batchsegid;
		var targetURL;
		targetURL = 'searchRollForwardCriteria!delete.action';
		$.ajax({
			url : targetURL,
			beforeSend : ShowProgress(),
			data : submitData,
			type : "POST",
			async : false,
			complete : function(status,response) {
				HideProgress();
				if (response=='success') {
					SaveMsg("Roll Forward Batch terminated successfully!")
					constructRollForwardBatchGrid();
				} else {
					alert("System busy. Please try again later.");

				}
			}
		});
	}

	validateForm = function() {

		if ($("#txtFromSearch").val() == "") {
			showERRMessage(rollForwardFromDateRqrd);
			return false;
		}
		if ($("#txtToSearch").val() == "") {
			showERRMessage(rollForwardToDateRqrd);
			return false;
		}
		var toDateString = getValue("txtToSearch");
		var fromDateString = getValue("txtFromSearch");

		var toDateStrArr = toDateString.split('/');
		var fromDateSrtArr = fromDateString.split('/');
	
		var toDate = new Date(toDateStrArr[2],Number(toDateStrArr[1])-1,toDateStrArr[0]);
		var fromDate = new Date(fromDateSrtArr[2],Number(fromDateSrtArr[1])-1,fromDateSrtArr[0]);
		
		if (toDate < fromDate) {
			showERRMessage(rollForwardFromDateLessthanFromDate);
			return false;
		}
		return true;
	};
	
	function ShowProgress() {
		top[2].ShowProgress();
	}
	function HideProgress() {
		top[2].HideProgress();
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}
	
	function SaveMsg(msg) {
		top[2].objMsg.MessageText = msg;
		top[2].objMsg.MessageType = "Confirmation";
		top[2].ShowPageMessage();
	}

	function formatDate(dt, fmt1, fmt2) {
		var dd1, mm1, yy1;
		var dd2, mm2, yy2;
		var dt2, sep;

		if (fmt1 == "dd-mmm-yyyy") {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 3);
			yy1 = dt.substr(7, 4);
		} else if (fmt1 == "dd-mmm-yy") {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 3);
			yy1 = dt.substr(7, 2);
		} else if ((fmt1 == "dd/mm/yyyy") || (fmt1 == "dd-mm-yyyy")) {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 2);
			yy1 = dt.substr(6, 4);
		} else if ((fmt1 == "dd/mm/yy") || (fmt1 == "dd-mm-yy")) {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 2);
			yy1 = dt.substr(6, 2);
		} else if ((fmt1 == "yyyy/mm/dd") || (fmt1 == "yyyy-mm-dd")) {
			dd1 = dt.substr(8, 2);
			mm1 = dt.substr(5, 2);
			yy1 = dt.substr(0, 4);
		} else if ((fmt1 == "yy/mm/dd") || (fmt1 == "yy-mm-dd")) {
			dd1 = dt.substr(6, 2);
			mm1 = dt.substr(3, 2);
			yy1 = dt.substr(0, 2);
		} else {
			// alert("formatDate:Invalid format1:" + fmt1);
		}
		// ***********************

		// get seperator
		if (fmt2.indexOf("/") != -1) {
			sep = "/";
		} else {
			sep = "-";
		}

		// get year
		if (fmt2.indexOf("yyyy") != -1) {
			if (yy1.length == 2) {
				if (Number(yy1) < 30) {
					yy2 = 2000;
				} else {
					yy2 = 1900;
				}
				yy2 += Number(yy1);
			} else {
				yy2 = yy1;
			}
		} else {
			if (yy1.length == 4) {
				yy2 = yy1.substr(2, 2);
			} else {
				yy2 = yy1;
			}
		}

		// get month
		if (fmt2.indexOf("mmm") != -1) {
			if (mm1.length == 2) {
				mm2 = getStrMonth(mm1);
			} else {
				mm2 = mm1;
			}
		} else {
			if (mm1.length == 3) {
				mm2 = getNoMonth(mm1);
				return (yy1 + "/" + mm2 + "/" + dd1);
			} else {
				mm2 = mm1;
			}
		}

		// get day
		dd2 = dd1;

		// date

		if (fmt2.indexOf("yy") < 4) {
			dt2 = yy2 + sep + mm2 + sep + dd2;
		} else {
			dt2 = dd2 + sep + mm2 + sep + yy2;
		}

		return dt2;
	}
}
UI_searchRollForwad = new UI_searchRollForwad();

UI_searchRollForwad.ready();