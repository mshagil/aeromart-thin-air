var termsTemplate = null;

jQuery(document).ready(function(){
	
	termsTemplate = opener.termsTemplates[opener.currentlySelectedTemplateID];
	
	$("#enableEditor").html(termsTemplate.templateContent);
	
	$("#editingArea").show();
	
	//Initialize html editor
	tinyMCE.init({
        // General options
        mode : "textareas",
        editor_selector : "enableEditor",
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "spellchecker,|nonbreaking,blockquote,pagebreak,|insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",
        oninit : function() {  
        	// Once initialized, tell the editor to go full screen
	    	tinymce.get('enableEditor').execCommand('mceFullScreen');
	    },
	    save_onsavecallback: function() {
	    	var data = {};
	    	
	    	opener.termsTemplates[opener.currentlySelectedTemplateID].templateContent = tinyMCE.activeEditor.getContent();
	    	
	    	data["termsTemplateToSave.templateContent"] = tinyMCE.activeEditor.getContent();
	    	data["termsTemplateToSave.templateName"] = termsTemplate.templateName;
	    	data["termsTemplateToSave.templateID"] = termsTemplate.templateID;
	    	data["termsTemplateToSave.version"] = termsTemplate.version;
	    	data["termsTemplateToSave.carriers"] = termsTemplate.carriers;
	    	data["termsTemplateToSave.language"] = termsTemplate.language;
	    	
	    	$.post("showTermsTemplate!updateTermsAndConditions.action", data)
	    	.done(function(data) {
	    		opener.templateEditingCompleted(data);
	    	});
	    }
	});
});