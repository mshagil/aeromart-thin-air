function UI_setupFares(){
	this.destination = [];
	this.cosList = [];
	this.fareBasisCodeList = [];
	this.errorInfo = null;
	this.sysDate = "";
	this.fareGridRowChanged = false;
	this.screenId = "SC_INVN_020";
	this.fareSearchCriteria = {}
	var delimiter = "^";
	this.validityType = "VALID";
	this.chargeGridData = {};
	this.isChildVisible = false;
	this.posList = [];
	this.defaultSelectedCos = "";
	this.ready = function(){
		initPage();
		top[2].HideProgress();
	};
	function FareRuleFomatter(){
		this.dateFormat = function(date){
			if (date!=null){
				if (date.indexOf("T")>-1){
					date = date.split("T")[0].split("-");
					return date[2]+"/"+date[1]+"/"+date[0];
				}else{
					date = date.split("/");
					return date[2]+"-"+date[1]+"-"+date[0]+"T00:00:00";
				}
			}else{
				return "";
			}
			
		}
		this.currencyFromat = function(value){
			if (String(value).indexOf(".")==-1){
				value += ".00"; 
			}
			return value;
		}
	}
	
	function closeClick(){
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_setupFares.screenId))) {
			//setPageEdited(false);
			top[1].objTMenu.tabRemove(UI_setupFares.screenId);
		}
	}
	
	
	
	function initPage(){
		var newUrl = "fareSearch!createPageInitailData.action";
		$("#btnSearch").on("click", seachFares);
		$("#btnEdit").on("click", editSeletedGridRows);
		$("#btnEditRule").on("click", editSeletedFareRules);
		$("#btnEdit, #btnEditRule, #btnSave, #btnReset, #txtExtendFrom, #txtExtendTo").disable()
		$("#btnExtend").on("click",amendValidityDates);
		$(document).on("click",".selSelector",selectGridRow);
		$(document).on("change", ".gridEdit", updateRowEdit);
		//$(document).on("mouseup", ".gridEdit", enablehridButtons);
		$("#rasSF1").on("click", setEffectiveon);
		$("#radAllFares").on("click", setEffectiveon);
		$("#radAllFares").prop("checked", true).trigger("click")
		$("#txtFromDate, #txtToDate, #txtExtendFrom, #txtExtendTo").on("blur", function(){
			dateChk(this.id);
		});
		$("#btnClose").on("click",closeClick)
		$("#btnUpload").on("click",upload_click)
		$("#btnAdd").on("click", addNewFare);
		$("#btnSave").on("click", saveFares);
		$("#btnReset").on("click", reset_grid);
		
		$("#btnGetCharges").on("click", searchFareCharges);
		$.ajax({
		     url: newUrl,
		     type: 'POST',
		     beforeSend: top[2].ShowProgress(),
		     async:false,
		     dataType: 'json',
		})
		.done(function(response){
	    	 UI_setupFares.destination = response.fare.viaPointList;
	    	 UI_setupFares.cosList = response.fare.cosList;
	    	 UI_setupFares.fareBasisCodeList = response.fare.fareBasisCodeList;
	    	 UI_setupFares.sysDate = response.fare.systemDate;
	    	 UI_setupFares.errorInfo = response.errorInfo;
	    	 UI_setupFares.isChildVisible = response.fare.childVisible;
	    	 UI_setupFares.posList = response.fare.posList;
	    	 UI_setupFares.defaultSelectedCos = response.fare.defaultSelectedCos;
	    	 var searchCri = getTabValues(UI_setupFares.screenId, delimiter, "strSearchCriteria");
	    	 if (searchCri != ""){
	    		 UI_setupFares.fareSearchCriteria = eval("("+searchCri+")");
	    	 }
	     })
	     .fail(function(requestObj, textStatus, errorThrown){
		 	  showCommonError("Error", "System Error, Please contact system administrator");
		 	  top[2].HideProgress();
	     })
	     .always(function(jsonData,stat){
	    	 top[2].HideProgress();
	     });
		$("#selDeparture, #selArrival, #selVia1, #selVia2, #selVia3, #selVia4").fillDropDown({firstEmpty:true, dataArray:UI_setupFares.destination , keyIndex:0, valueIndex:1});
		$("#selCOS").fillDropDown({firstEmpty:false, dataArray:ObjListToArray(UI_setupFares.cosList) , keyIndex:0, valueIndex:1});
		$("#selBasisCode").fillDropDown({firstEmpty:true, dataArray:ObjListToArray(UI_setupFares.fareBasisCodeList) , keyIndex:1, valueIndex:1});
		$("#selPOS").fillDropDown({firstEmpty:true, dataArray:ObjListToArray(UI_setupFares.posList) , keyIndex:1, valueIndex:1});
		if (UI_setupFares.defaultSelectedCos != "") {
			$("#selCOS").val(UI_setupFares.defaultSelectedCos);
		}
		setSearchCriteria(UI_setupFares.fareSearchCriteria);
		$("#txtFromDate").val(UI_setupFares.sysDate);
	}

	function upload_click() {
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 500) / 2;	
		top[0].objWindow = window.open("showUploadFares.action","CWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=600,height=270,resizable=no,top=' + intTop + ',left=' + intLeft);

	}
	
	function reset_grid(){
		$("#fareGrid").trigger("reloadGrid");
	}
	
	function seachFares(){
		if ($("#selDeparture").val() == ""){
			showCommonError("Error", "Origin can not be blank");
			return false;
		}else if ($("#selArrival").val() == ""){
			showCommonError("Error", "Destination can not be blank");
		}else{
			constractGrid("", 'seach');
		}
	}
	
	function setSearchCriteria(reqData){
		var call = 'init';
		if (!$.isEmptyObject(reqData)){
			$("#selDeparture").val(reqData.originAirport);
			$("#selArrival").val(reqData.destinationAirport);
			$("#selCOS").val(reqData.cos);
			$("#selStatus").val(reqData.status);
			//optional search criteria
			$("#txtFromDate").val(reqData.fromDate);
			$("#txtToDate").val(reqData.toDate);
			$("#selVia1").val(reqData.via1);
			$("#selVia2").val(reqData.via2);
			$("#selVia3").val(reqData.via3);
			$("#selVia4").val(reqData.via4);
			$("#txtFareRule").val(reqData.fareRuleCode);
			$("#selBasisCode").val(reqData.fareBasisCode);
			if (reqData.effectiveDuring=="EffectiveAll"){
				$("#rasSF1")
				.prop("checked", true)
				.trigger("click");
				
			}else{
				$("#radAllFares")
				.prop("checked", true)
				.trigger("click");
			}
			$("#txtBkingClass").val(reqData.bookingClassCode);
			if (reqData.showAllOND=="selectedComb"){
				$("#radSelected").prop("checked", true).trigger("click");
			}else{
				$("#radSOD1").prop("checked", true).trigger("click");
			}
			if (reqData.validityType=="VALID"){
				$("#radValid").prop("checked", true).trigger("click");
			}else if(reqData.validityType=="SALES"){
				$("#radSales").prop("checked", true).trigger("click");
			}else if(reqData.validityType=="VALIDRT"){
				$("#radReturn").prop("checked", true).trigger("click");
			}
			call = 'search';
		}
		constractGrid("", call);
	}
	
	function setEffectiveon(e){
		var t = $(e.target).val();
		if (t=="From"){
			$("#txtFromDate, #txtToDate").enable()
			$("#txtFromDate, #txtToDate").datepicker({
				showOn: "button",
				buttonImage: "../../images/calendar_no_cache.gif",
				buttonImageOnly: true,
				dateFormat: 'dd/mm/yy'
			});
			$("#txtFromDate").val(UI_setupFares.sysDate);
		}else{
			$("#txtFromDate, #txtToDate").val("")
			$("#txtFromDate, #txtToDate").disable()
			$("#txtFromDate, #txtToDate").datepicker("destroy");
		}
		
	}
	
	function addNewFare(){
		var reqData = {};
		UI_setupFares.validityType = $("input[name='radDt']:checked").val();
		reqData.originAirport = $("#selDeparture").val();
		reqData.destinationAirport = $("#selArrival").val();
		reqData.cos = $("#selCOS").val();
		reqData.status = $("#selStatus").val();
		//optional search criteria
		reqData.fromDate = $("#txtFromDate").val();
		reqData.toDate = $("#txtToDate").val();
		reqData.via1 = $("#selVia1").val();
		reqData.via2 = $("#selVia2").val();
		reqData.via3 = $("#selVia3").val();
		reqData.via4 = $("#selVia4").val();
		reqData.fareRuleCode = $("#txtFareRule").val();
		reqData.fareBasisCode = $("#selBasisCode").val();
		reqData.effectiveDuring = $("input[name='rasSF1']:checked").val();
		reqData.bookingClassCode = $("#txtBkingClass").val();
		reqData.showAllOND = $("input[name='radSOD1']:checked").val();
		reqData.validityType = $("input[name='radDt']:checked").val();
		UI_setupFares.fareSearchCriteria = reqData;
		rule_click("add");
	}
	
	function enablehridButtons(){
		$("#btnSave, #btnReset, #btnExtend, #txtExtendFrom, #txtExtendTo").enable();
	}
	
	function updateRowEdit(e){
		UI_setupFares.fareGridRowChanged = true;
		var obj = e.target.id.split("_");
		var page = $("#fareGrid").jqGrid("getGridParam", "page")-1;
		var rowNum =$("#fareGrid").jqGrid("getGridParam", "rowNum");
		var gridRowData = $.data($("#fareGrid")[0], "gridData")[parseInt(obj[1]-(page*rowNum),10)-1];
		if ( (obj[0].indexOf("Date") > -1) || (obj[0].indexOf("date") > -1) ){
			var myFormater = new FareRuleFomatter();
			gridRowData[obj[0]] = myFormater.dateFormat($("#"+e.target.id).val());
		}else{
			gridRowData[obj[0]] = $("#"+e.target.id).val();
		}
		gridRowData['modified'] = true;
		
	}
	
	function amendValidityDates(){
		var validityType = UI_setupFares.validityType;
		var validStr = "valid";
		var myFormater = new FareRuleFomatter();
		if(validityType=="VALIDRT"){
			validStr = "rtValid";
		} else if(validityType=="SALES"){
			validStr = "salesValid";
		}
		if ($("#txtExtendFrom").val() != "" &&  $("#txtExtendTo").val()!=""){
			if (CheckDates($("#txtExtendFrom").val(), $("#txtExtendTo").val())){
				var selectedRow = $(".selSelector:checked");				
				selectedRow.each(function(){
					var rowid = this.id.split("_")[1];
					if (rowid > 10) {
						rowid = rowid % 10;
					}
					var gridRowData = $.data($("#fareGrid")[0], "gridData")[rowid -1];
					if(CheckDates(UI_setupFares.sysDate,  $("#txtExtendTo").val())){
						$("#" + validStr + "ToDate_"+rowid).val($("#txtExtendTo").val());
						$("#btnSave, #btnReset").enable();
						gridRowData[validStr + 'ToDate'] = myFormater.dateFormat($("#txtExtendTo").val());
						gridRowData['modified'] = true;
					}
					if(CheckDates(UI_setupFares.sysDate, $("#" + validStr + "FromDate_"+rowid).val()) 
							&& CheckDates(UI_setupFares.sysDate,  $("#txtExtendFrom").val())){
						$("#" + validStr + "FromDate_"+rowid).val($("#txtExtendFrom").val());
						$("#btnSave, #btnReset").enable();
						gridRowData[validStr + 'FromDate'] = myFormater.dateFormat($("#txtExtendFrom").val());
						gridRowData['modified'] = true;
					}
				});
			}else{
				showCommonError("Error", "Extend From date can not be grater than To date");
			}
		} else if ($("#txtExtendTo").val()!=""){
			var selectedRow = $(".selSelector:checked");
			selectedRow.each(function(){
				var rowid = this.id.split("_")[1];
				if (rowid > 10) {
					rowid = rowid % 10;
				}
				var gridRowData = $.data($("#fareGrid")[0], "gridData")[rowid-1];
				if(CheckDates(UI_setupFares.sysDate,  $("#txtExtendTo").val())){
					$("#" + validStr + "ToDate_"+rowid).val($("#txtExtendTo").val());
					$("#btnSave, #btnReset").enable();
					gridRowData[validStr + 'ToDate'] = myFormater.dateFormat($("#txtExtendTo").val());
					gridRowData['modified'] = true;
				}
			});
		}else{
			showCommonError("Error", "Extend dates can not be blank");
		}
		
	}
	
	function extendDate(flg){
		if(flg.toLowerCase()=='init'){
		$("#txtExtendFrom, #txtExtendTo").datepicker({
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			minDate: 0
		});
		}else{
			$("#txtExtendFrom, #txtExtendTo").val("")
			$("#txtExtendFrom, #txtExtendTo").datepicker( "destroy" );
		}
	} 
	
	function selectGridRow(e){
		var selectedRow = $(".selSelector:checked");
		var rowid = e.target.id.split("_")[1];
		var page = $("#fareGrid").jqGrid("getGridParam", "page")-1;
		var rowNum =$("#fareGrid").jqGrid("getGridParam", "rowNum");
		if (!$(e.traget).prop("checked")){
			var gridRowData = $.data($("#fareGrid")[0], "gridData")[parseInt(rowid-(page*rowNum),10)-1];
			if (gridRowData['modified']){
				var x = confirm("Unsaved date will be loss, do you want to continue?");
				if (x){
					gridRowData['modified'] = false;
				}else{
					return false;
				}
			}
			$("tr#"+rowid).find("input[type='text']").disable();
		}
		if (selectedRow.length>0){
			$("#btnEdit, #btnEditRule, #btnExtend, #txtExtendFrom, #txtExtendTo").enable();
			if (!$("#txtExtendFrom").hasClass("hasDatepicker")){
				extendDate('init');
			}
		}else{
			$("#btnEdit, #btnEditRule, #btnExtend, #txtExtendFrom, #txtExtendTo, #btnSave, #btnReset").disable();
			extendDate('distroy');
		}
	}
	
	function editSeletedGridRows(e){
		var selectedRow = $(".selSelector:checked");
		$("#btnSave, #btnReset").enable();
		var validityType = UI_setupFares.validityType;
		var validStr = "#valid";
		if(validityType=="VALIDRT"){
			validStr = "#rtValid";
		} else if(validityType=="SALES"){
			validStr = "#salesValid";
		}
		selectedRow.each(function(){
			var rowid = this.id.split("_")[1];
			if (CheckDates(UI_setupFares.sysDate, $(validStr + "FromDate_"+rowid).val())){
				$(validStr + "FromDate_"+rowid).enable();
			}
			if (CheckDates(UI_setupFares.sysDate, $(validStr + "ToDate_"+rowid).val())){
				$(validStr + "ToDate_"+rowid).enable();
			}
			$("#fareAmount_"+rowid).enable();
			$("#childFareAmount_"+rowid).enable();
			$("#infantFareAmount_"+rowid).enable();
		});
	}
	
	function editSeletedFareRules(e){
		var selectedRow = $(".selSelector:checked");
		if (selectedRow.length>1){
			showCommonError("Error", "Many fare rules can not be edit same time");
		}else{
			rule_click("edit");
		}
	}
	
	
	function rule_click(state) {
		var childScreenId = "SC_ADMN_019";
		if ((top[1].objTMenu.tabIsAvailable(childScreenId) && top[1].objTMenu.tabRemove(childScreenId))			
				|| (!top[1].objTMenu.tabIsAvailable(childScreenId))) {		
			if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_setupFares.screenId))) {
				if (state!="add"){
					var cos = $("#selCOS").val();
					var selectedRow = $(".selSelector:checked");
					var rowid = selectedRow.attr("id").split("_")[1];
					var page = $("#fareGrid").jqGrid("getGridParam", "page")-1;
					var rowNum =$("#fareGrid").jqGrid("getGridParam", "rowNum");
					var gridRowData = $.data($("#fareGrid")[0], "gridData")[parseInt(rowid-(page*rowNum),10)-1];
					var fareID = gridRowData['fareId'];
				}else{
					var fareID = "";
				}
					top[0].ruleStatus = "";
					top[1].objTMenu.tabSetTabInfo(UI_setupFares.screenId, childScreenId, "showLoadFareJsp!manageRule.action?mode=MANAGEFARE&fareId="+fareID, "Manage Fare", false);
					setTabValues(childScreenId, delimiter, "strInventoryFlightData",	"true");
					setTabValues(childScreenId, delimiter, "strSearchCriteria",	JSON.stringify(UI_setupFares.fareSearchCriteria, null));
					location.replace("showLoadFareJsp!manageRule.action?mode=MANAGEFARE&fareId="+fareID);
			}
		}
	}

	
	function constractGrid(e, flg){
		$("#btnEdit, #btnEditRule").disable();
		var setColumnsType = function(validityType){
			var arrColModel = []
			if (validityType=="VALIDRT"){
				arrColModel = ['&nbsp;', 'Fare ID' ,'Fare<br/>Rule', 'Basis<br/>Code', 'Return Valid<br>From <span>*</span>' ,'Return Valid<br>To <span>*</span>', 'Fare <span>*</span>',
			           'CH Fare<br/>Type<span>*</span>','CH<br/>Fare <span>*</span>', 'IN Fare<br/>Type<span>*</span>', 'IN<br/>Fare <span>*</span>', 'BC', 
			           'COS', "OW/RT", "Fix<br>All", "OND", "Visible<br/>Channels", "Stat", "Exp","Pax<br>Cat", "Sel"];
			}else if(validityType=="SALES"){
				arrColModel = ['&nbsp;', 'Fare ID' ,'Fare<br/>Rule', 'Basis<br/>Code', 'Sales Valid<br>From <span>*</span>' ,'Sales Valid<br>To <span>*</span>', 'Fare <span>*</span>',
			           'CH Fare<br/>Type<span>*</span>','CH<br/>Fare <span>*</span>', 'IN Fare<br/>Type<span>*</span>', 'IN<br/>Fare <span>*</span>', 'BC', 
			           'COS', "OW/RT", "Fix<br>All", "OND", "Visible<br/>Channels", "Stat", "Exp","Pax<br>Cat", "Sel"];
			}else if(validityType=="VALID"){
				arrColModel = ['&nbsp;', 'Fare ID' ,'Fare<br/>Rule', 'Basis<br/>Code', 'Valid<br>From <span>*</span>' ,'Valid To <span>*</span>', 'Fare <span>*</span>',
		           'CH Fare<br/>Type<span>*</span>','CH<br/>Fare <span>*</span>', 'IN Fare<br/>Type<span>*</span>', 'IN<br/>Fare <span>*</span>', 'BC', 
		           'COS', "OW/RT", "Fix<br>All", "OND", "Visible<br/>Channels", "Stat", "Exp","Pax<br>Cat", "Sel"];
			}
			return arrColModel;
		}
		
		var setColumnsModels = function(validityType){
			var returnFomatter = function(cellVal, options, rowObject){
				var treturn = "&nbsp;";
				if (cellVal=="Y"){
					treturn = "RT";
				}
				else if(cellVal=="N"){
					treturn = "OW";
				}
				return treturn;
			}
			var arrColModel = [
			   			    {name:'id',  index:'id', width:20},
						    {name:'fareId', index:'fareId', width:50},
							{name:'fareRuleCode',  index:'fareRuleCode', width:55},
							{name:'fareBasisCode', index:'fareBasisCode', width:50, align:"center"},
							{name:'validFromDate', index:'validFromDate', width:80, align:"left", formatter:createInputFormater, role:"date"},
							{name:'validToDate', index:'validToDate', width:80, align:"left", formatter:createInputFormater, role:"date"},
							{name:'fareAmount', index:'fareAmount', width:65, align:"right", formatter:createInputFormater, role:"curreny"},
							{name:'childFareType', width:25, align:"center"},
							{name:'childFareAmount', index:'childFareAmount', width:65, align:"right", formatter:createInputFormater, role:"curreny"},
							{name:'infantFareType', width:25, align:"center"},
							{name:'infantFareAmount', index:'infantFareAmount', width:65, align:"right", formatter:createInputFormater, role:"curreny"},
							{name:'bookingClass', index:'bookingClass', width:25, align:"right"},
							{name:'classOfService', index:'classOfService', width:30},
							{name:'returnFlag', width:25, align:"center",formatter:returnFomatter},
							{name:'fixedAllocation', index:'fixedAllocation', width:25, align:"center"},
							{name:'ondCode', index:'ondCode', width:70, align:"center"},
							{name:'visibilityChannels', index:'visibilityChannels', width:50, align:"center"},
							{name:'status', index:'status', width:30, align:"center"},
							{name:'refundableFlag', index:'refundableFlag', width:25, align:"center"},
							{name:'paxCategoryCode', width:25, align:"center",},
							{name:'sel', width:25, align:"center", formatter:function(cellVal, options, rowObject){
								var objIndex = options.rowId;
								return "<input type='checkbox' id='selFare_"+objIndex+"' name='selFare_"+objIndex+"' class='selSelector'>";
								}
							}
						];
			if (validityType=="VALIDRT"){
				arrColModel[4] = {name:'rtValidFromDate', index:'rtValidFromDate', width:80, align:"left", formatter:createInputFormater, role:"date"};
				arrColModel[5] = {name:'rtValidToDate', index:'rtValidToDate', width:80, align:"left", formatter:createInputFormater, role:"date"};
			}else if(validityType=="SALES"){
				arrColModel[4] = {name:'salesValidFromDate', index:'salesValidFromDate', width:80, align:"left", formatter:createInputFormater, role:"date"};
				arrColModel[5] = {name:'salesValidToDate', index:'salesValidToDate', width:80, align:"left", formatter:createInputFormater, role:"date"};
			}
			return arrColModel;
		}

		var createInputFormater = function(cellVal, options, rowObject){
			var myFormater = new FareRuleFomatter();
			var objIndex = options.rowId,colName = options.colModel.name,className="",value=cellVal;
			if (options.colModel.role=="date"){
				value = myFormater.dateFormat(cellVal);
			}else if(options.colModel.role=="curreny"){
				value = myFormater.currencyFromat(cellVal);
			}
			var tInput = "<input type='text' id='"+colName+"_"+objIndex+"' name='"+colName+"_"+objIndex+"'" +
					" style='width:97%;text-align:"+options.colModel.align+"' value='"+value+"' disabled='disabled' class='gridEdit "+options.colModel.role+"'/>";
			return tInput;
		}
		var tGrid = $('<table id="fareGrid"></table>'), tGPager = $('<div id="fareGridPager"></div>');
		$("#fareGridContainer")
		.empty()
		.append(tGrid,tGPager);
		var reqData = {};
		UI_setupFares.validityType = $("input[name='radDt']:checked").val();
		if (flg!='init'){
			reqData.originAirport = $("#selDeparture").val();
			reqData.destinationAirport = $("#selArrival").val();
			reqData.cos = $("#selCOS").val();
			reqData.status = $("#selStatus").val();
			//optional search criteria
			reqData.fromDate = $("#txtFromDate").val();
			reqData.toDate = $("#txtToDate").val();
			reqData.via1 = $("#selVia1").val();
			reqData.via2 = $("#selVia2").val();
			reqData.via3 = $("#selVia3").val();
			reqData.via4 = $("#selVia4").val();
			reqData.fareRuleCode = $("#txtFareRule").val();
			reqData.fareBasisCode = $("#selBasisCode").val();
			reqData.effectiveDuring = $("input[name='rasSF1']:checked").val();
			reqData.bookingClassCode = $("#txtBkingClass").val();
			reqData.showAllOND = $("input[name='radSOD1']:checked").val();
			reqData.validityType = UI_setupFares.validityType;
			UI_setupFares.fareSearchCriteria = reqData;
			var newUrl = "fareSearch.action";
			$("#btnSave, #btnReset, #btnExtend, #txtExtendFrom, #txtExtendTo").disable();
		}
		
		var tempGrid = $("#fareGrid").jqGrid({
			datatype: function(postdata){
		        $.ajax({
		           url: newUrl,
		           beforeSend:top[1].setVisible("divLoadMsg", true),
		           data: $.extend(postdata, reqData),
		           dataType:"json",
		           complete: function(jsonData,stat){	        	  
		              if(stat=="success") {
		            	  mydata = eval("("+jsonData.responseText+")");
		            	  if (mydata.msgType != "Error"){
			            	 $.data(tempGrid[0], "gridData", mydata.rows);
			            	 tempGrid[0].addJSONData(mydata);
		            	  }else{
		            		  showCommonError("Error",mydata.message)
		            	  }
		            	 top[2].HideProgress();
		              }
		           }
		        });
		    },
			height: 160,
			width: '100%',
			colNames: setColumnsType(UI_setupFares.validityType),
			colModel:setColumnsModels(UI_setupFares.validityType),
			imgpath: "",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			beforeSelectRow: function(rowid, e) {
			    return false;
			},
			pager: $('#fareGridPager'),
			multiselect: false,
			viewrecords: true,
			rowNum:10, 
			sortable:true,
			altRows:true,
			altclass:"GridAlternateRow",
			onSelectRow: function(rowid){
			},
			afterInsertRow: function myformat(rowid, rowData, rowelem) {
				if(rowelem.masterFareRefCode != "" && rowelem.masterFareRefCode != null){
					$(this).jqGrid('setRowData', rowid, false, { color: '#FF0000' });
				}	    	 	
			}
		}).navGrid("#fareGridPager",{refresh: true, edit: false, add: false, del: false, search: false});
		$(".curreny").numeric({allowDecimal:true});
		constractChargersGrid();
	};
	
	function saveFares(){
		var saveFareList = [];
		$.each($.data($("#fareGrid")[0], "gridData"), function(){
			if (this.modified){
				if(!validateFareData(this)){
					return;
				}				
				saveFareList[saveFareList.length] = this;
			}
		});
		
		if (saveFareList.length == 0){
			return;
		}
		
		var data = {};
		data['fareToJSONList'] = JSON.stringify(saveFareList, null);	
		
		//Call the save Action and pass the list to save
		var newUrl = "fare!saveFares.action";
		$.ajax({
		     url: newUrl,
		     type: 'POST',
		     beforeSend: top[2].ShowProgress(),
		     async:false,
		     data:data,
		     dataType: 'json',
		})
		.done(function(response){
			if (response.success){
				showCommonError("Confirmation", "All record(s) save successfully");
				constractGrid("", 'search');
			}else{
				showCommonError("Error", response.messageTxt);
			}
	     })
	     .fail(function(requestObj, textStatus, errorThrown){
		 	  showCommonError("Error", "System Error, Please contact system administrator");
	     })
	     .always(function(jsonData,stat){
	    	 top[2].HideProgress();
	     });
	}
	
	function ObjListToArray(list) {
		var t = []
		for (var i=0;i<list.length;i++){
			 var x = [];
			 for (var key in list[i]) {
				 var obj = list[i];
				 x[0] = key;
				 x[1] = obj[key];
			}
			t[t.length] = x;
		 }
		return t;
	}
	
	function constractChargersGrid(){
		var tempGrid = $("#fareChargerGrid").jqGrid({
			datatype: "local",
			height: 75,
			width: '100%',
			colNames: ["", "OND", "Group", "Category", "Charge<br>Code", "Charge Description", "Refund<br>Y/N", "Apply<br>To",			           
			           "Valid From", "Valid To", "V/P<br>Flag", "Amount"],
			colModel: [{name:'id',  index:'id', width:25},
					   {name:'ondCode', index:'ondCode', width:80, align:"left"},
					   {name:'chargeGroupCode',  index:'chargeGroupCode', width:95 , align:"left"},
					   {name:'chargeCategoryCode',  index:'chargeCategoryCode', width:75 ,align:"left"},
					   {name:'chargeCode',  index:'chargeCode', width:65 ,align:"center"},
					   {name:'chargeDescription',  index:'chargeDescription', width:180 ,align:"left"},
					   {name:'chargeRefFlag',  index:'chargeRefFlag', width:50 ,align:"center"},
					   {name:'chargeApplicability',  index:'chargeApplicability', width:70 ,align:"center"},
					   {name:'rateEffFromDate',  index:'rateEffFromDate', width:75 ,align:"center"},
					   {name:'rateEffToDate',  index:'rateEffToDate', width:75 ,align:"center"},
					   {name:'rateValuePerFlag',  index:'rateValuePerFlag', width:50 ,align:"center"},
					   {name:'rateValuePer',  index:'rateValuePer', width:60, align:"right"}],
			imgpath: "",
			beforeSelectRow: function(rowid, e) {
			    return false;
			},
			multiselect: false,
			viewrecords: true,
			rowNum:10, 
			sortable:true,
			altRows:true,
			altclass:"GridAlternateRow",
			onSelectRow: function(rowid){
			}
		}).navGrid("#fareGridPager",{refresh: false, edit: false, add: false, del: false, search: false});
	
	}
	
	function genarateGridList(rows){
		var myformat = new FareRuleFomatter();
		var listGen = []
		var count = 0;
		for (var i=0;i<rows.length;i++){
			var tond = "", tchCat = "", tchGr = "";
			var chGroup = rows[i].chargeGroupList;
			for (var j=0;j<chGroup.length;j++){
				count++;
				var temp = {};
				temp.id = count;
				if ( (tond!=rows[i].ondCode) ){
					temp.ondCode = rows[i].ondCode;
					tond = rows[i].ondCode;
				}else{
					temp.ondCode = "";
				}
				if ( (tchCat!=chGroup[j].chargeCategoryCode) ){
					temp.chargeCategoryCode = chGroup[j].chargeCategoryCode;
					tchCat = chGroup[j].chargeCategoryCode;
				}else{
					temp.chargeCategoryCode = "";
				}
				
				if ( (tchGr!=chGroup[j].chargeGroupCode) ){
					temp.chargeGroupCode = chGroup[j].chargeGroupCode;
					tchGr = chGroup[j].chargeGroupCode;
				}else{
					temp.chargeGroupCode = "";
				}
				temp.chargeDescription = chGroup[j].chargeDescription;
				temp.chargeRefFlag = chGroup[j].chargeRefFlag
				temp.chargeCode = chGroup[j].chargeCode;
				temp.chargeApplicability = chGroup[j].chargeApplicability;
				temp.rateEffFromDate = myformat.dateFormat( chGroup[j].chargeRateList[0].rateEffFromDate );
				temp.rateValuePerFlag = chGroup[j].chargeRateList[0].rateValuePerFlag;
				temp.rateEffToDate = myformat.dateFormat( chGroup[j].chargeRateList[0].rateEffToDate );
				temp.rateValuePer = chGroup[j].chargeRateList[0].rateValuePer;
				listGen[listGen.length] = temp;
			}
			
		}
		return listGen;
	}
	
	fillLocalGrid = function(p){
		var i = 0;
		p = $.extend({id:"",
					 data:null,
					 editable:false}, 
					p);
		$(p.id).clearGridData();
		if (p.data != null){
			$.each(p.data, function(){
				if ("D" != this.action){
					$(p.id).addRowData(i + 1, p.data[i]);
				}
				if ($(p.editable)){
					$(p.id).editRow(String(i+1));
				}
				i++;
			});
		}
	};
	
	
	function searchFareCharges(){
		var reqData = {};
		reqData.originAirport = $("#selDeparture").val();
		reqData.destinationAirport = $("#selArrival").val();
		reqData.via1 = $("#selVia1").val();
		reqData.via2 = $("#selVia2").val();
		reqData.via3 = $("#selVia3").val();
		reqData.via4 = $("#selVia4").val();
		reqData.posStation = $("#selPOS").val();
		
		reqData.effectiveDuring = $("input[name='rasSF1']").val();
		//optional search criteria
		reqData.fromDate = $("#txtFromDate").val();
		reqData.toDate = $("#txtToDate").val();
				
		//Call the fare charges search action
		var newUrl = "fareSearch!searchOndChargesData.action";
		$.ajax({
		     url: newUrl,
		     type: 'POST',
		     beforeSend: top[2].ShowProgress(),
		     async:false,
		     data:reqData,
		     dataType: 'json',
		})
		.done(function(response){
			UI_setupFares.chargeGridData = genarateGridList(response.rows);
			fillLocalGrid({"id":$("#fareChargerGrid"), "data":UI_setupFares.chargeGridData, "editable":false})
	     })
	     .fail(function(requestObj, textStatus, errorThrown){
		 	  showCommonError("Error", "System Error, Please contact system administrator");
	     })
	     .always(function(jsonData,stat){
	    	 top[2].HideProgress();
	     });
	}	
	
	function validateFareData(row){
		var validated = true;
		var myFormater = new FareRuleFomatter();	
		
		var validityType = UI_setupFares.validityType;
		var validFrmStr = row.validFromDate;
		var validToStr = row.validToDate;
		if(validityType=="SALES"){
			validFrmStr = row.salesValidFromDate;
			validToStr = row.salesValidToDate;
		}
		
		if (validFrmStr == "") {
			showCommonError("Error", UI_setupFares.errorInfo.fromdateRqrd + "in row " + row.id);
			validated = false;
			return false;
		}
		if (validFrmStr != "" && !dateValidDate(myFormater.dateFormat(validFrmStr))) {
			showCommonError("Error", UI_setupFares.errorInfo.fromDateIncorrect + " in row" + row.id);
			validated = false;
			return false;
		}		
		if (validFrmStr != "" && validToStr != ""
				&& !CheckDates(myFormater.dateFormat(validFrmStr), myFormater.dateFormat(validToStr))) {
			showCommonError("Error", UI_setupFares.errorInfo.datesInvalid + " in row " + row.id);
			validated = false;
			return false;
		}
		if (validToStr == "") {
			showCommonError("Error", UI_setupFares.errorInfo.todateRqrd + " in row " + row.id);
			validated = false;
			return false;
		}
		if (validToStr != "" && !dateValidDate(myFormater.dateFormat(validToStr))) {
			showCommonError("Error", UI_setupFares.errorInfo.toDateIncorrect + " in row " + row.id);
			validated = false;
			return false;
		}		
		if (row.fareAmount == "") {
			showCommonError("Error", UI_setupFares.errorInfo.AEDRqrd + " in row " + row.id);
			validated = false;
			return false;
		}
		if (row.fareAmount != "" && isNaN(row.fareAmount)) {
			showCommonError("Error", UI_setupFares.errorInfo.amountNan + " in row " + row.id);
			validated = false;
			return false;
		}
		if (String(row.fareAmount) != "" && parseFloat(row.fareAmount) <= 0) {			
			var confirmed = confirm("Fare Amount is Zero in row " + row.id
					+ ". Do you wish to continue?");		
			if (confirmed) {
				validated = true;
			} else {
				validated = false;
				return false;
			}

		}// validation for child and infant fares
		if (UI_setupFares.isChildVisible && row.childFareAmount == "") {
			showCommonError("Error", "Child fare cannot be empty" + " in row "
					+ row.id);
			validated = false;
			return false;
		}
		if (UI_setupFares.isChildVisible && row.childFareAmount != "" && isNaN(row.childFareAmount)) {
			showCommonError("Error", "Invalid child fare amount" + " in row "
					+ row.id);
			validated = false;
			return false;
		}
		
		if (UI_setupFares.isChildVisible && row.childFareAmount != "" && (row.childFareType=="P" && row.childFareAmount > 100)) {
			showCommonError("Error", "Child fare percentage cannot be lager than 100 in row "
					+ row.id);
			validated = false;
			return false;
		}
		
		if (UI_setupFares.isChildVisible && String(row.childFareAmount) != ""
				&& parseFloat(row.childFareAmount) <= 0) {			
			var confirmed = confirm("Child Fare Amount is Zero in row " + row.id
					+ ". Do you wish to continue?");
			if (confirmed) {
				validated = true;
			} else {
				validated = false;
				return false;
			}
		}
		if (UI_setupFares.isChildVisible && (row.infantFareAmount == "" && row.infantFareAmount != 0)) {
			showCommonError("Error", "Infant fare cannot be empty" + " in row "
					+ row.id);
			validated = false;
			return false;
		}
		if (UI_setupFares.isChildVisible && row.infantFareAmount != "" && isNaN(row.infantFareAmount)) {
			showCommonError("Error", "Invalid infant fare amount" + " in row "
					+ row.id);
			validated = false;
			return false;
		}
		
		if (UI_setupFares.isChildVisible && row.infantFareAmount != "" && (row.infantFareType=="P" && row.infantFareAmount > 100)) {
			showCommonError("Error", "Infant fare percentage cannot be lager than 100 in row "
					+ row.id);
			validated = false;
			return false;
		}
		
		if (UI_setupFares.isChildVisible && String(row.infantFareAmount) != ""
				&& parseFloat(row.infantFareAmount) <= 0) {
			var confirmed = confirm("Infant Fare Amount is Zero in row " + row.id
					+ ". Do you wish to continue?");
			if (confirmed) {
				validated = true;
			} else {
				validated = false;
				return false;
			}
		}
		
		return validated;
	}
}
var UI_setupFares = new UI_setupFares();
UI_setupFares.ready();
