function UI_OverRideOHD(){
	this.rowSelected = false;
	this.seletedID = -1;
	this.ready = function(){
		$("#fareRuleCode").text(fareRuleCode)
		$("#btnSearch").on("click", searchClick);
		$("#btnAdd").on("click", addConfigClick);
		$("#btnEdit").on("click", editConfigClick);
		$("#btnDelete").on("click", deleteConfigClick);
		$("#tblFormData").find("input, select").disable();
		$("#btnCancel").enable();
		$("#btnSave").on("click", saveConfigClick);
		$("#startCutOTimeDD, #startCutOTimeMM, #startCutOTimeHH, #endCutOTimeDD, #endCutOTimeMM, #endCutOTimeHH, #releaseTimeDD, #releaseTimeMM, #releaseTimeHH").numeric();
		createGrid();
		fillDataGrid();
	};
	
	this.fillGridData = function(p){
		var i = 0;
		p = $.extend({id:"",
					 data:null,
					 editable:false}, 
					p);
					
		$(p.id).clearGridData();
		
		if (p.data != null){
			$.each(p.data, function(){
				$(p.id).addRowData(i + 1, p.data[i]);
				i++;
			});
		}
	}
	
	function createGrid(){
		$("#wrapperGrid")
		.empty()
		.append('<table id="tblOverRideOHD"></table>','<div id="OverRidePager"></div>');
		$("#tblOverRideOHD").jqGrid({
			datatype: 'local',
			colNames:['&nbsp;','OHD Config ID','Start Cutover Time', 'End Cutover Time', 'Release Time',
			          'Time with Respect To','Module Code', '&nbsp;'],
		   	colModel:[  {name:'id', width:30, label:"id"},
		   	            {name:'ohdRelId',index:'ohdRelId', width:130 , align:'center'},
		   	            {name:'startCutOTime', index:'startCutOTime', width:150 ,align:'right'},
		   	            {name:'endCutOTime', index:'endCutOTime', width:150, align:'right'},
		   	            {name:'releaseTime', index:'releaseTime', width:150, align:'right'},
		   	            {name:'timeWithRespect', width:150, align:'right'},
		   	            {name:'moduleCode', width:120, align:'center'},
		   	            {name:'version', hidden:true}
		   	         ],
		 	height: 130,
			imgpath: '',
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
				$("#tblOverRideOHD").GridToForm( rowid, "#tblFormData" );
				setTimesValues( $("#tblOverRideOHD").jqGrid( 'getRowData', rowid ))
				UI_OverRideOHD.seletedID = rowid;
				UI_OverRideOHD.rowSelected = true;
			}
		});
	};
	
	function setTimesValues(rowData){
		var stCutOver = rowData.startCutOTime.split(":"),
		edCutOver = rowData.endCutOTime.split(":"),
		releaseTime = rowData.releaseTime.split(":");
		$("#startCutOTimeDD").val(stCutOver[0]);
		$("#startCutOTimeHH").val(stCutOver[1]);
		$("#startCutOTimeMM").val(stCutOver[2]);
		$("#endCutOTimeDD").val(edCutOver[0]);
		$("#endCutOTimeHH").val(edCutOver[1]);
		$("#endCutOTimeMM").val(edCutOver[2]);
		$("#releaseTimeDD").val(releaseTime[0]);
		$("#releaseTimeHH").val(releaseTime[1]);
		$("#releaseTimeMM").val(releaseTime[2]);
	}
	
	//to Fill the data
	function fillDataGrid(){
		var data = {};
		data['fareRuleCode'] = fareRuleCode;
		data['fareRuleId'] = fareRuleID;
		$.ajax({
		     url: 'fareOverrideOHD.action',
		     type: 'POST',
		     beforeSend: ShowProgress(),
		     async:false,
		     data:data,
		     dataType: 'json',
		})
		.done(function(response){
			if (response.success){
            	 $.data($("#tblOverRideOHD")[0], "gridData", response.rows);
            	 UI_OverRideOHD.fillGridData({id:"#tblOverRideOHD", data:response.rows});
            	 UI_OverRideOHD.rowSelected = false;
			}else{
				showWindowCommonError("Error", response.messageTxt);
			}
	     })
	     .fail(function(requestObj, textStatus, errorThrown){
		 	  showWindowCommonError("Error", "System Error, Please contact system administrator");
	     })
	     .always(function(jsonData,stat){
	    	 HideProgress();
	     });
	}
	
	
	//Search click function
	function searchClick(){
		createGrid();
	};
	
	//Add Configs click function
	function addConfigClick(){
		UI_OverRideOHD.seletedID = -1;
		$("#tblFormData").find("input, select").enable();
		clearData();
	};
	
	//Edit Configs click function
	function editConfigClick(){
		if (UI_OverRideOHD.rowSelected){
			$("#tblFormData").find("input, select").enable();
		}else{
			showWindowCommonError("Error", "Select a row before edit");
		}
	};
	
	//Delete Configs click function
	function deleteConfigClick(){
		if (UI_OverRideOHD.rowSelected){
			var data = {};
			data['relTimeId'] = $("#ohdRelId").val();	
			//Call the save Action and pass the list to save
			var newUrl = "fareOverrideOHD!deleteOnholdTimeConfig.action";
			$.ajax({
			     url: newUrl,
			     type: 'POST',
			     beforeSend: ShowProgress(),
			     async:false,
			     data:data,
			     dataType: 'json',
			})
			.done(function(response){
				if (response.success){
					showWindowCommonError("Confirmation", "Successfully deleted the row");
					createGrid();
					fillDataGrid();
					clearData();
				}else{
					showWindowCommonError("Error", response.messageTxt);
				}
		     })
		     .fail(function(requestObj, textStatus, errorThrown){
			 	  showWindowCommonError("Error", "System Error, Please contact system administrator");
		     })
		     .always(function(jsonData,stat){
		    	 HideProgress();
		     });
		}else{
			showWindowCommonError("Error", "Select a row before delete");
		}
	};
	
	function ShowProgress(){
		$("#divLoadMsg").show();
	}
	
	function HideProgress(){
		$("#divLoadMsg").hide();
	}
	
	function clearData(){
		$("#moduleCode").val("");
		$("#startCutOTimeDD").val("");
		$("#startCutOTimeMM").val("");
		$("#startCutOTimeHH").val("");
		$("#endCutOTimeDD").val("");
		$("#endCutOTimeMM").val("");
		$("#endCutOTimeHH").val("");	
		$("#releaseTimeDD").val("");
		$("#releaseTimeMM").val("");
		$("#releaseTimeHH").val("");
		$("#timeWithRespect").val("");
		$("#ohdRelId").val("");
		$("#version").val("");
	}
	
	//save Configs Click function
	function saveConfigClick(){
		if (validateForm()){
			var data = {};
			data['fareRuleId'] = fareRuleID;
			data['selModuleCode'] = $("#moduleCode").val();	
			data['startCutoverTime'] = $("#startCutOTimeDD").val() + ":" + $("#startCutOTimeHH").val() + ":" + $("#startCutOTimeMM").val();	
			data['endCutoverTime'] = $("#endCutOTimeDD").val() + ":" + $("#endCutOTimeHH").val() + ":" + $("#endCutOTimeMM").val();	
			data['releaseTime'] = $("#releaseTimeDD").val() + ":" + $("#releaseTimeHH").val() + ":" + $("#releaseTimeMM").val();
			data['selRelTimeRespectTo'] = $("#timeWithRespect").val();	
			data['relTimeId'] = $("#ohdRelId").val();
			data['version'] = $("#version").val();
			//Call the save Action and pass the list to save
			var newUrl = "fareOverrideOHD!saveOHDTime.action";
			$.ajax({
			     url: newUrl,
			     type: 'POST',
			     beforeSend: ShowProgress(),
			     async:false,
			     data:data,
			     dataType: 'json',
			})
			.done(function(response){
				if (response.success){
					showWindowCommonError("Confirmation", "All record(s) save successfully");
					createGrid();
					fillDataGrid();
					$("#tblFormData").find("input, select").disable();
					clearData();
				}else{
					showWindowCommonError("Error", response.messageTxt);
				}
		     })
		     .fail(function(requestObj, textStatus, errorThrown){
			 	  showWindowCommonError("Error", "System Error, Please contact system administrator");
		     })
		     .always(function(jsonData,stat){
		    	 HideProgress();
		     });
		}else{
			return false;
		}
	};
	
	function parseTimeStringToMin(timeString){
		var stCutOver = timeString.split(":");
		var timeInMin = 0;
		if(stCutOver.length == 3){
			timeInMin = stCutOver[0] * 24 * 60 + stCutOver[1] * 60 + stCutOver[2]*1;
		} else if(stCutOver.length == 2){
			timeInMin = stCutOver[0] * 60 + stCutOver[1]*1;
		} else if(stCutOver.length == 1){
			timeInMin = stCutOver[0]*1;
		}
		
		return timeInMin;
	};
	
	//validation 
	function validateForm(){
		var t_return = true;
		if ($("#startCutOTimeDD").val()=="" && $("#startCutOTimeHH").val()=="" && $("#startCutOTimeMM").val()==""){
			 showWindowCommonError("Error", "Enter valied start cutover time");
			 $("#startCutOTime").focus();
			 t_return=false;
		}else if($("#endCutOTimeDD").val()=="" && $("#endCutOTimeHH").val()=="" && $("#endCutOTimeMM").val()==""){
			 showWindowCommonError("Error", "Enter valied end cutover time");
			 $("#endCutOTime").focus();
			 t_return=false;
		}else if($("#releaseTimeDD").val()=="" && $("#releaseTimeHH").val()=="" && $("#releaseTimeMM").val()==""){
			showWindowCommonError("Error", "Enter valied release time");
			 $("#releaseTime").focus();
			 t_return=false;
		} else if($("#timeWithRespect").val()==""){
			showWindowCommonError("Error", "Select time with respect");
			 $("#timeWithRespect").focus();
			 t_return=false;
		} else {
			var startCutOStr = $("#startCutOTimeDD").val() + ":" + $("#startCutOTimeHH").val() + ":" + $("#startCutOTimeMM").val();
			var endCutOStr = $("#endCutOTimeDD").val() + ":" + $("#endCutOTimeHH").val() + ":" + $("#endCutOTimeMM").val();
			if(parseTimeStringToMin(endCutOStr) > parseTimeStringToMin(startCutOStr)){
				showWindowCommonError("Error", "End cutover time should not be greater than start cutover time");
				 $("#endCutOTimeDD").focus();
				 t_return=false;
			}
		}
			
		
		return t_return;
	};
}
var UI_OverRideOHD = new UI_OverRideOHD();
var erroImgPath = "";
UI_OverRideOHD.ready();