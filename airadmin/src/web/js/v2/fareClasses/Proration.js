
function UI_Proration(){
	this.rowSelected = false;
	this.seletedID = -1;
	this.prorationArry=[];
	this.dataChanged=false;
	
	this.ready = function(){	 		 
		$("#btnCancel").enable();		
		$("#btnApply").enable();
		$("#btnApply").click(function(){			
			saveData();
		});
		 
		if(opener.ManageFareClasses.isProDeleteEnable){
			$("#btnDelete").enable();
		}else{
			$("#btnDelete").disable();
		}
		$("#btnDelete").click(function(){			
			deleteData();
		});
		$("#btnCancel").on("click", closeClick);
		createGrid();
		
		$( "input[name^='proration_']" ).keypress(function(e){
			
		    var code = e.which || e.keyCode;
		    if(code == 46)
		    	return false;
		
		    return true;	  
		     
		});
		

	};

	function createGrid(){	 
		console.log(opener.ManageFareClasses.isProrationModify);
 	    console.log(opener.ManageFareClasses.prorationData);   			
		$("#wrapperGrid")
		.empty()
		.append('<table id="tblProration"></table>');
		$("#tblProration").jqGrid({
			 data: opener.ManageFareClasses.prorationData,
			 datatype: 'local',
			 colNames:['Segment','Proration %'],
			 colModel:[  
		   	            {name:'segmentCode',index:'segmentCode', width:200 , align:'center'},
		   	            {name:'proration', index:'proration', width:150 ,align:'center',formatter:function(cellVal, options, rowObject){
		   	            	var objIndex = options.rowId;
		   	            	var objName = options.colModel.name;
		   	            		   	            	 
		   	            	var value=cellVal;
		   	            	var tInput =null;

		   	            	tInput = "<input type='text' id='proration_"+objIndex+"' name='proration_"+objIndex+"'" +
							" style='width:97%;text-align:"+options.colModel.align+"' value='"+value+"'" +
							 "  />";
		   	                        	

		   	            	return tInput;
		   	            	 
		   	            	
		   	            }
		   	            }
		   	        
		   	         ],
		 	height: 130,
			imgpath: '',
			multiselect: false,
			viewrecords: true,
	 
			onSelectRow: function(rowid){

			}
		});
	};
	
	
	function saveData(){
 
		if(validate()){
					 
			for(var i=0;i<opener.ManageFareClasses.prorationData.length;i++){
				opener.ManageFareClasses.prorationData[i].proration=UI_Proration.prorationArry[i];
			}
			opener.ManageFareClasses.savingProrations=opener.ManageFareClasses.prorationData;
            opener.ManageFareClasses.isProrationSaved=true;			
            opener.ManageFareClasses.isProrationDeleted=false;	
			window.close();
		}
	};
	
	function deleteData(){
		opener.ManageFareClasses.savingProrations=[]; 
		opener.ManageFareClasses.isProrationDeleted=true;	
		opener.ManageFareClasses.isProDeleteEnable=false;
		opener.ManageFareClasses.prorationData=[];
		window.close();
	};

	function validateProration(rowData,rowId){
		var value= $.trim(rowData.proration);	
		if(value!=null && value !=""){			 
			var regEx = /^\d+$/;
			var pattern = new RegExp(regEx);
			if(pattern.test(value)){
				return true;
			}else{			
				 
				showWindowCommonError("Error",  opener.ManageFareClasses.errorInfo.valueInvalid+" "+rowId);			
		 
				return false;
			}
		}else{				
			showWindowCommonError("Error",  opener.ManageFareClasses.errorInfo.allRequired);	
			return false;
		}		
	}
	
	function validate(){	 
		var isValid=false;
		var ids = jQuery("#tblProration").jqGrid('getDataIDs');
		UI_Proration.prorationArry=[];	
		for (var i = 0; i < ids.length; i++){
		    var rowId = ids[i];
		    var rowData = jQuery('#tblProration').jqGrid ('getRowData', rowId);
		    rowData.proration = $('#proration_'+rowId).val();
		    
		    isValid=validateProration(rowData,rowId);
			if (!isValid) {
				return false;
			}
			UI_Proration.prorationArry.push(rowData.proration);
		}
 		if(isValid){			 
			var sum =0;
			for(var i=0;i<UI_Proration.prorationArry.length;i++){
				var value=parseInt(UI_Proration.prorationArry[i]);				 
				sum=sum+value;	
				if(opener.ManageFareClasses.isReturnFare && i == UI_Proration.prorationArry.length/2 -1){
					if(sum!=100){
						isValid=false;				
						showWindowCommonError("Error",  opener.ManageFareClasses.errorInfo.totalInvalid);					
					} else {
						sum = 0;
					}
				}
			}
			if(sum!=100){
				isValid=false;				
				showWindowCommonError("Error",  opener.ManageFareClasses.errorInfo.totalInvalid);	
		
			} 			
		} 
		return isValid;
	};
	
 	
	function closeClick(){
		if(isDataChanged()){
			var result=confirm("Closing the window will remove unsaved data. Do you wish to continue?");
			if (result){
				window.close();
			} 
		}else{
			window.close();
		}
	}
	
	function nonEditableOthers(){
		var ids = jQuery("#tblProration").jqGrid('getDataIDs');
		
		for (var i = 0; i < ids.length; i++){
		    var rowId = ids[i];
		    if(rowId!=UI_Proration.seletedID){
		    	$('#proration_'+rowId).attr('readonly', true);
		    }
		     
		}
	}
	
	function isDataChanged(){
		var isChanged=false;		
		for(var i=0;i<opener.ManageFareClasses.prorationData.length;i++){
			 
		    var proValue= $('#proration_'+(i+1)).val();
		    if(proValue != opener.ManageFareClasses.prorationData[i].proration){
		    	isChanged=true;
		    	break;
		    }
		}
        return isChanged;
	}

	

}
 
var UI_Proration = new UI_Proration();
var erroImgPath = "";
UI_Proration.ready();
 
