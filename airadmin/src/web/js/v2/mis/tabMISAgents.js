
	function UI_tabMISAgents(){}
	
	UI_tabMISAgents.countryList = null;
	UI_tabMISAgents.intLastClick = null;
	UI_tabMISAgents.intLastCity = null;
	UI_tabMISAgents.allAgents = null;
	UI_tabMISAgents.top10Agents = null;
	UI_tabMISAgents.nonPerfAgents = null;
	UI_tabMISAgents.roteWiseRevenue = null;
	UI_tabMISAgents.staffWiseRevenue = null;

	UI_tabMISAgents.onReady = function(){

		$("#selAgentBranch").change(function() {
		  	UI_tabMISAgents.retriveAndDisplayAgentDetail();
		});
	}
	
	UI_tabMISAgents.createCountryCities = function(){

		UI_tabMISAgents.intLastClick = null;
		var strHTMLText = "";
		if (UI_tabMISAgents.countryList != null && UI_tabMISAgents.countryList.length > 0){
			strHTMLText += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			for(var i=0 ; i< UI_tabMISAgents.countryList.length ; i++ ) {

					strHTMLText += "<tr>";
					strHTMLText += "	<td class='cursorPointer' valign='top' onclick='UI_tabMISAgents.tdCntryTreeOnClick(" + i + ")'>";
					strHTMLText += "		<img src='../../images/M030_no_cache.gif' border='0' id='img_" + i + "'>";
					strHTMLText += "	<\/td>";
					strHTMLText += "	<td valign='top' style='height:20px;'><font>&nbsp;&nbsp;<\/font><\/td>";
					strHTMLText += "	<td class='cursorPointer' valign='top' onclick='UI_tabMISAgents.tdCntryTreeOnClick(" + i + ")'>";
					strHTMLText += "		<font>" + UI_tabMISAgents.countryList[i].countryName + "<\/font>";
					strHTMLText += "	<\/td>";
					strHTMLText += "<\/tr>";
					strHTMLText += "<tr>";
					strHTMLText += "	<td colspan='3' valign='top'><div id='cntryTree_" + i + "' style='display:none;'>";
					strHTMLText += "		<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
					
					var gsaInfoList = UI_tabMISAgents.countryList[i].gsaList;
					if(gsaInfoList != null && gsaInfoList.length > 0) {
						for(var j=0 ; j < gsaInfoList.length; j++){
							var agentType = "GSA";
							if(gsaInfoList[j].independentAgent) agentType = "Agent";
							if(gsaInfoList[j].salesOffice) agentType = "SO";
							strHTMLText += "			<tr>";
							strHTMLText += "				<td width='11%'><font>&nbsp;<\/font><\/td>";
							strHTMLText += "				<td id='tdCity_" + i + "_" + j + "' width='89%' class='lgndBd cursorPointer'  style='background-Color:#B0C0C0' onclick='UI_tabMISAgents.tdCityOnClick(" + i + "," + j + ")'>";
							strHTMLText += "					<font id='fntCity_" + i + "_" + j + "'>&nbsp;" + gsaInfoList[j].stationCode + " [" + agentType + "]" + " - " + gsaInfoList[j].gsaName + "<\/font>";
							strHTMLText += "				<\/td>";
							strHTMLText += "			<\/tr>";
						}
					}
					strHTMLText += "		<\/table>";
					strHTMLText += "	<\/div></td><\/tr>";
			}
			strHTMLText += "		<\/table>";
			strHTMLText += "	<\/div></td><\/tr>"; 
			strHTMLText += "<\/table>";
		}
		
		DivWrite("divCntry", strHTMLText);
	}
	
	UI_tabMISAgents.tdCntryTreeOnClick = function(intRow){
		UI_tabMISAgents.reverseCityOnClick();
		if (UI_tabMISAgents.intLastClick != intRow){
			if (UI_tabMISAgents.intLastClick != null){
				setDisplay("cntryTree_" + UI_tabMISAgents.intLastClick, false);
				getFieldByID("img_" + UI_tabMISAgents.intLastClick).src = "../../images/M030_no_cache.gif";
			}
			UI_tabMISAgents.intLastClick = intRow;
			setDisplay("cntryTree_" + intRow, true);
			getFieldByID("img_" + intRow).src = "../../images/M031_no_cache.gif";
		}else{
			setDisplay("cntryTree_" + intRow, false);
			getFieldByID("img_" + intRow).src = "../../images/M030_no_cache.gif";
			UI_tabMISAgents.intLastClick = null;
		}
	}
	
	UI_tabMISAgents.tdCityOnClick = function(i, j){
		UI_tabMISAgents.reverseCityOnClick();
		getFieldByID("tdCity_" + i + "_" + j).style.backgroundColor = "#143F3E";
		getFieldByID("fntCity_" + i + "_" + j).style.color = "white";
		UI_tabMISAgents.intLastCity = j;
		UI_tabMISAgents.showAgentLocation();
	}
	
	UI_tabMISAgents.reverseCityOnClick = function(){
		if (getFieldByID("tdCity_" + UI_tabMISAgents.intLastClick + "_" + UI_tabMISAgents.intLastCity) != null){
			getFieldByID("tdCity_" + UI_tabMISAgents.intLastClick + "_" + UI_tabMISAgents.intLastCity).style.backgroundColor = "#B0C0C0";
			getFieldByID("fntCity_" + UI_tabMISAgents.intLastClick + "_" + UI_tabMISAgents.intLastCity).style.color = "black";
		}
	}
	
	UI_tabMISAgents.showAgentLocation = function(){

	    $("#trRouteAndStaffWiseInfo").hide();
	    $("#trRouteAnalysis").hide();		

	    var dtoCountryInfo = UI_tabMISAgents.countryList[UI_tabMISAgents.intLastClick];
	    var dtoGSAInfo = dtoCountryInfo.gsaList[UI_tabMISAgents.intLastCity];

  	    var address = dtoGSAInfo.stationDesc + "," + dtoCountryInfo.countryName;
	    UI_tabMISAgents.setMap(address);

	    //show gsa detail
	    if(dtoGSAInfo.gsaDetail != null){
		UI_tabMISAgents.displayGSADetail(dtoGSAInfo.gsaDetail);
	    } else {
		UI_tabMISAgents.retriveAndDisplayGSADetail(dtoGSAInfo.gsaCode);
	    }
	}

	UI_tabMISAgents.submitGsaListSuccess = function(response){
		top[2].HideProgress();
		if(response.success) {
			//create country cities
			UI_tabMISAgents.countryList = response.countryList;
			UI_tabMISAgents.allAgents = response.allAgents;
			UI_tabMISAgents.top10Agents = response.top10Agents;
			UI_tabMISAgents.nonPerfAgents = response.nonPerfAgents;
			UI_tabMISAgents.createCountryCities();
			UI_tabMISAgents.createAgentsList();		
		} else {
			//display the error message
		}		
	}

	UI_tabMISAgents.submitGsaDetailSuccess = function(response) {
		if(response.success) {
			//create country cities
			var dtoCountryInfo = UI_tabMISAgents.countryList[UI_tabMISAgents.intLastClick];
			var dtoGSAInfo = dtoCountryInfo.gsaList[UI_tabMISAgents.intLastCity];
			dtoGSAInfo.gsaDetail = 	response.gsaDetail;
			UI_tabMISAgents.displayGSADetail(dtoGSAInfo.gsaDetail);
		} else {
			//display the error message
		}		
	}
	
	UI_tabMISAgents.submitAgentDetailSuccess = function(response) {
		if(response.success) {
			//create country cities
			UI_tabMISAgents.roteWiseRevenue  = response.roteWiseRevenue;
			UI_tabMISAgents.staffWiseRevenue  = response.staffWiseRevenue;
			UI_tabMISAgents.displayRouteWiseRevenue();
			UI_tabMISAgents.displayStaffWiseRevenue();
			UI_tabMISAgents.createAgentRouteWiseAnalysis();
			$("#trRouteAndStaffWiseInfo").show();
			getFieldByID("divBranch").scrollTop = 9999; 
		} else {
			//display the error message
		}	
	}	
	
	UI_tabMISAgents.retriveAndDisplayGSAList = function() {

		UI_tabMISAgents.clearBranchSummary();
		UI_tabMISAgents.cleanRouteWiseRevenue();
		UI_tabMISAgents.cleanStaffWiseRevenue();
		DivWrite("divAgentInfo", "");
		DivWrite("divCntry", "");
		DivWrite("divAgentInfo", "");
		DivWrite("divPayHistory", "");
		$("#tblAgentsList").remove();
		$("#trRouteAndStaffWiseInfo").hide();
		$("#trRouteAnalysis").hide();

        UI_tabMISAgents.setMap(null);
            	
		$.ajax( {
			url : 'showMISAgentsGsaList.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISAgents.submitGsaListSuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});          		
	}

	UI_tabMISAgents.setMap = function(strAdress){
		  if(strAdress == null) {
			var myOptions = {zoom: 0, mapTypeId: google.maps.MapTypeId.ROADMAP };
		  	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		  	var mapPoint = new google.maps.LatLng(0 , 0 );
	    	 	map.setCenter(mapPoint);
		  } else {
			var geoCoader = new google.maps.Geocoder();
			geoCoader.geocode({address:strAdress}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					var myOptions = {zoom: 13, mapTypeId: google.maps.MapTypeId.ROADMAP };
					var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
					map.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
					    map: map, 
					    position: results[0].geometry.location
					});
				} else {
					var myOptions = {zoom: 0, mapTypeId: google.maps.MapTypeId.ROADMAP };
				  	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				  	var mapPoint = new google.maps.LatLng(0 , 0 );
			    	 	map.setCenter(mapPoint);
				}
			});     
		  }
	}

	UI_tabMISAgents.retriveAndDisplayGSADetail = function(gsaCode) {

		UI_tabMISAgents.clearBranchSummary();
		UI_tabMISAgents.cleanRouteWiseRevenue();
		UI_tabMISAgents.cleanStaffWiseRevenue();
		DivWrite("divAgentInfo", "");
		DivWrite("divPayHistory", "");	
		
		arrParamData['gsaCode'] = gsaCode;
		$.ajax( {
			url : 'showMISAgentsGsaDetail.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISAgents.submitGsaDetailSuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});
		

		/*var params = hdnData + "&gsaCode=" + gsaCode;
		$.post('showMISAgentsGsaDetail.action', params , function(response) {
			UI_tabMISAgents.submitGsaDetailSuccess(response);
		},'json');*/	
	}
	
	UI_tabMISAgents.retriveAndDisplayAgentDetail = function() {
		$("#trRouteAndStaffWiseInfo").hide();
		$("#trRouteAnalysis").hide();
		UI_tabMISAgents.cleanRouteWiseRevenue();
		UI_tabMISAgents.cleanStaffWiseRevenue();
		//clear the existing data
		if($("#selAgentBranch").val() != null && $("#selAgentBranch").val() != "") {
			
			arrParamData['agentCode'] = $("#selAgentBranch").val();
			
			$.ajax( {
				url : 'showMISAgentsAgentDetail.action',
				type : 'POST',
				data : arrParamData,
				dataType : 'json',
				success : function(response) {
					UI_tabMISAgents.submitAgentDetailSuccess(response);
				},
				error : function(requestObj, textStatus, errorThrown) {
					handleCommonAjaxInvocationErrors(requestObj);
				}
			});
			
			/*var params = hdnData + "&agentCode=" + $("#selAgentBranch").val();
			$.post('showMISAgentsAgentDetail.action', params , function(response) {
				UI_tabMISAgents.submitAgentDetailSuccess(response);
			},'json');*/
		}
	}
	
	UI_tabMISAgents.createAgentsList = function(){
		$("#tblAgentsList").remove();
		var strHTMLText = "<table id='tblAgentsList' width='100%' border='0' cellpadding='1' cellspacing='1' style='border:1px solid grey;'>";

		var objJSAgnt = "";
		var strType = $("#selAgentsFilter").val();
		switch (strType){
			case "AGENTS_A" : objJSAgnt = UI_tabMISAgents.allAgents ; break;
			case "AGENTS_T" : objJSAgnt = UI_tabMISAgents.top10Agents ; break;
			case "AGENTS_N" : objJSAgnt = UI_tabMISAgents.nonPerfAgents ; break;
		}
		strLastAgntType = strType;
		var tblRow = null;
		var strBGColor = "";
		if(objJSAgnt != null) {
			for(var i=0; i<objJSAgnt.length; i++) {
				strBGColor = getRowClass(i);
				strHTMLText += "<tr><td class='thinRight thinTop " + strBGColor + "'><font class=''>";
				strHTMLText +=  objJSAgnt[i].agentName + "<\/font><\/td><\/tr>";
			}
		}
		strHTMLText += "<\/table>";
		$("#divAgntsList").append(strHTMLText);
	}
	
	UI_tabMISAgents.displayGSADetail = function(gsaDetail){
		var strHTMLText = "";
		var dtoCountryInfo = UI_tabMISAgents.countryList[UI_tabMISAgents.intLastClick];
		var dtoGSASummary = dtoCountryInfo.gsaList[UI_tabMISAgents.intLastCity];
		var strTitle = ""
		if(dtoGSASummary.independentAgent) {
			strTitle += "Agent for " + dtoGSASummary.stationCode +  " - " + dtoCountryInfo.countryName;
		} else if(dtoGSASummary.salesOffice) {
			strTitle += "Sales Office at " + dtoGSASummary.stationCode +  " - " + dtoCountryInfo.countryName;
		} else {
			strTitle += "GSA for " + dtoGSASummary.stationCode +  " - " + dtoCountryInfo.countryName;
		}

		strHTMLText += "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td colspan='3'><font><b>" + strTitle + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td colspan='3'><hr><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td width='5%'><font><\/font><\/td>";
		strHTMLText += "		<td width='45%'><font>Name :<\/font><\/td>";
		strHTMLText += "		<td width='50%'><font><b>" + dtoGSASummary.gsaName + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font><\/font><\/td>";
		strHTMLText += "		<td><font>Period Start :<\/font><\/td>";
		strHTMLText += "		<td><font><b>" + gsaDetail.periodStart + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font><\/font><\/td>";
		strHTMLText += "		<td><font>Period End :<\/font><\/td>";
		strHTMLText += "		<td><font><b>" + gsaDetail.periodEnd + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";		
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font><\/font><\/td>";
		strHTMLText += "		<td><font>Turnover :<\/font><\/td>";
		strHTMLText += "		<td><font><b>" + gsaDetail.turnOver + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font><\/font><\/td>";
		strHTMLText += "		<td><font>Employees :<\/font><\/td>";
		strHTMLText += "		<td><font><b>" + gsaDetail.employees + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font><\/font><\/td>";
		strHTMLText += "		<td><font>Sub Agents :<\/font><\/td>";
		strHTMLText += "		<td><font><b>" + gsaDetail.branches + "<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";		
		strHTMLText += "	<tr>";
		strHTMLText += "		<td colspan='3'><font>&nbsp;<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td colspan='3'><font><b>Management :<b><\/font><\/td>";
		strHTMLText += "	<\/tr>";

		var intLen = gsaDetail.management.length;
		for(var i=0 ; i< intLen; i++){
			strHTMLText += "	<tr>";
			strHTMLText += "		<td><font><\/font><\/td>";
			strHTMLText += "		<td colspan='2'><font>" + gsaDetail.management[i] + "<\/font><\/td>";
			strHTMLText += "	<\/tr>";
		}
	
		strHTMLText += "<\/table>";

		
		DivWrite("divAgentInfo", strHTMLText);
		
		UI_tabMISAgents.createBranchSummary(gsaDetail);
		UI_tabMISAgents.fillBranchDropDown(gsaDetail);
		UI_tabMISAgents.createPaymentHistory(gsaDetail);
	}

	UI_tabMISAgents.clearBranchSummary = function(){
		var objTbl = getFieldByID("tblBranchSumm").tBodies[0];
		deleteTableRows(objTbl);
	}

	
	UI_tabMISAgents.createBranchSummary = function(gsaDetail){
		var objTbl = getFieldByID("tblBranchSumm").tBodies[0];
		deleteTableRows(objTbl);
		var tblRow = null;

		if(gsaDetail.agentSummaryList != null && gsaDetail.agentSummaryList.length != 0) {

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:""}));
			tblRow.appendChild(createCell({desc:"Sales [" + gsaDetail.currencyCode + "]", align:"center"}));
			objTbl.appendChild(tblRow);		

			var strBGColor = "";
			for(var i=0; i < gsaDetail.agentSummaryList.length ; i++) {
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i);				
				tblRow.appendChild(createCell({desc:gsaDetail.agentSummaryList[i].agentName, css:"thinRight thinTop " + strBGColor, width:"55%"}));
				tblRow.appendChild(createCell({desc:gsaDetail.agentSummaryList[i].turnover, align:"right", css:"thinTop " + strBGColor, width:"45%"}));
			
				objTbl.appendChild(tblRow);
			}

			$("#tblBranchSumm").css("border", "1px solid grey");
		} else {

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:""}));
			objTbl.appendChild(tblRow);

			var dtoCountryInfo = UI_tabMISAgents.countryList[UI_tabMISAgents.intLastClick];
			var dtoGSASummary = dtoCountryInfo.gsaList[UI_tabMISAgents.intLastCity];
			tblRow = document.createElement("TR");

			if(dtoGSASummary.independentAgent) {
				tblRow.appendChild(createCell({desc:"This agent does not have sub agents", align:"center"}));			
			} else if(dtoGSASummary.salesOffice) { 
				tblRow.appendChild(createCell({desc:"This sales office does not have sub agents", align:"center"}));
			}else {
				tblRow.appendChild(createCell({desc:"This GSA does not have sub agents", align:"center"}));
			}
			objTbl.appendChild(tblRow);
		
			$("#tblBranchSumm").css("border", "0px solid grey");
		}
	}
	
	UI_tabMISAgents.fillBranchDropDown = function(gsaDetail){

		var strOptionList = "<option value=''>-- Select a branch --</option>";
		if (gsaDetail.agentSummaryList != null) {
			for(var i=0; i < gsaDetail.agentSummaryList.length ; i++) {
				strOptionList += "<option value='" + gsaDetail.agentSummaryList[i].agentCode + "'>" + gsaDetail.agentSummaryList[i].agentName + "</option>";
			}
		}
		$("#selAgentBranch")
			.empty()
    			.append(strOptionList)
    			.find('option:first')
    			.attr("selected","selected");

	}
	

	UI_tabMISAgents.createPaymentHistory = function(gsaDetail){

		var strHTMLText = "";

		strHTMLText += "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td width='70%'>&nbsp<\/td>";
		strHTMLText += "		<td width='30%' align='right'><font>" + gsaDetail.currencyCode + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>Total Collection :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.totalCollection + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>Total Settlements :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.totalSettlement + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>Total Uninvoiced :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.totalNonInvoiced + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";		
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>Outstanding for 30 days :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.outstanding30 + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>Outstanding for 60 days :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.outstanding60 + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>Outstanding for 90 days :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.outstanding90 + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td><font>More than 90 days :<\/font><\/td>";
		strHTMLText += "		<td align='right'><font>" + gsaDetail.outstandingOther + "<\/font><\/td>";
		strHTMLText += "	<\/tr>";
		strHTMLText += "<\/table>";

		DivWrite("divPayHistory", strHTMLText);
	}

	UI_tabMISAgents.cleanRouteWiseRevenue = function() {
		var objTbl = getFieldByID("tblRoutePerf").tBodies[0];
		deleteTableRows(objTbl);
		UI_tabMISAgents.roteWiseRevenue = null;
	}
	
	UI_tabMISAgents.displayRouteWiseRevenue = function(){
		var objTbl = getFieldByID("tblRoutePerf").tBodies[0];
		deleteTableRows(objTbl);
		var tblRow = null;
		
		if (UI_tabMISAgents.roteWiseRevenue != null && UI_tabMISAgents.roteWiseRevenue.length > 0) {

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:""}));
			tblRow.appendChild(createCell({desc:"Sales", align:"center"}));
			objTbl.appendChild(tblRow);

			var strBGColor = "";
			for(var i=0; i< UI_tabMISAgents.roteWiseRevenue.length; i++){
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i-1);
				
				tblRow.appendChild(createCell({desc:UI_tabMISAgents.roteWiseRevenue[i].segmentCode, css:"thinRight thinTop " + strBGColor, width:"35%"}));
				tblRow.appendChild(createCell({desc:UI_tabMISAgents.roteWiseRevenue[i].amount, align:"right", css:"thinTop " + strBGColor, width:"65%"}));
				
				objTbl.appendChild(tblRow);
			}

			$("#tblRoutePerf").css("border", "1px solid grey");
		} else {

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:""}));
			objTbl.appendChild(tblRow);

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:"No Data to Display", align:"center"}));
			objTbl.appendChild(tblRow);

			$("#tblRoutePerf").css("border", "0px solid grey");
		}
	}
	
	UI_tabMISAgents.cleanStaffWiseRevenue = function() {
		var objTbl = getFieldByID("tblStaffPerf").tBodies[0];
		deleteTableRows(objTbl);
		UI_tabMISAgents.staffWiseRevenue = null;
	}
	
	
	UI_tabMISAgents.displayStaffWiseRevenue = function(){
		var objTbl = getFieldByID("tblStaffPerf").tBodies[0];
		deleteTableRows(objTbl);
		
		var tblRow = null;
		
		if(UI_tabMISAgents.staffWiseRevenue != null && UI_tabMISAgents.staffWiseRevenue.length > 0) {

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:""}));
			tblRow.appendChild(createCell({desc:"Sales", align:"center"}));
			//tblRow.appendChild(createCell({desc:"Sectors", align:"center"}));
			//tblRow.appendChild(createCell({desc:"BC", align:"center"}));
			//tblRow.appendChild(createCell({desc:"TKT", align:"center"}));
			objTbl.appendChild(tblRow);

			var strBGColor = "";
			for (var i=0; i < UI_tabMISAgents.staffWiseRevenue.length; i++){
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i-1);
				
				tblRow.appendChild(createCell({desc:UI_tabMISAgents.staffWiseRevenue[i].staffName, css:"thinRight thinTop " + strBGColor, width:"50%"}));
				tblRow.appendChild(createCell({desc:UI_tabMISAgents.staffWiseRevenue[i].amount, css:"thinRight thinTop " + strBGColor, width:"52%", align:"right"}));
				//tblRow.appendChild(createCell({desc:objJSAgnt[i].sectors, css:"thinRight thinTop " + strBGColor, width:"11%", align:"right"}));
				//tblRow.appendChild(createCell({desc:objJSAgnt[i].BC, css:"thinRight thinTop " + strBGColor, width:"11%", align:"right"}));
				//tblRow.appendChild(createCell({desc:objJSAgnt[i].TKT, align:"right", css:"thinTop " + strBGColor, width:"11%", align:"right"}));
				
				objTbl.appendChild(tblRow);
			}

			$("#tblStaffPerf").css("border", "1px solid grey");
		} else {

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:""}));
			objTbl.appendChild(tblRow);

			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:"No Data to Display", align:"center"}));
			objTbl.appendChild(tblRow);

			$("#tblStaffPerf").css("border", "0px solid grey");
		}
	}

	UI_tabMISAgents.createAgentRouteWiseAnalysis =	function(){
		var strParams = "";
		var strValues = "";
		var strMaxValue = "";
		var strLables = "";

		if (UI_tabMISAgents.roteWiseRevenue != null && UI_tabMISAgents.roteWiseRevenue.length > 0) {

			$("#trRouteAnalysis").show();

			var data = new google.visualization.DataTable();
		    	data.addColumn('string', 'Route');
		    	data.addColumn('number', 'Revenue');

			data.addRows(UI_tabMISAgents.roteWiseRevenue.length);

			for(var i=0; i< UI_tabMISAgents.roteWiseRevenue.length; i++){				
				data.setValue(i, 0, UI_tabMISAgents.roteWiseRevenue[i].segmentCode);
				data.setValue(i, 1, Number(UI_tabMISAgents.roteWiseRevenue[i].amountAsValue));
			}

			var colChart3d = new google.visualization.ColumnChart(document.getElementById('imgAgentFareWise'));
			colChart3d.draw(data, {width:450, height:260 ,
		        	is3D:true,
		        	colors:[{color:'#936997', darker:'#79567c'}],
				legend: 'none', vAxis: {baseline : 0}, backgroundColor:'#f6f6f6'
		        	} );

		} else {
			DivWrite("imgAgentFareWise", "");
			$("#trRouteAnalysis").hide();
		}
	}
