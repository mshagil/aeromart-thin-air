	/*
	*********************************************************
		Description		: Reports MIS
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created on		: 15th October 2009
		NOTE			: This code write only for the demo purpose 
						  DONT use it if you are palling to integrate with the back-end
	*********************************************************	
	*/
	var screenId = "MIS_SC_RPT_001";
	
	var intLastClick = -1
	var blnMISReportsLoaded = false;
	
	/*
	var arrReporMap = new Array();
	arrReporMap.push(new Array(0,"UC_REPM_023"));
	arrReporMap.push(new Array(1,"UC_REPM_015"));
	arrReporMap.push(new Array(2,"UC_REPM_020"));
	arrReporMap.push(new Array(3,"UC_REPM_001"));
	arrReporMap.push(new Array(4,"UC_REPM_002"));
	arrReporMap.push(new Array(5,"UC_REPM_003"));
	arrReporMap.push(new Array(6,"UC_REPM_004"));
	arrReporMap.push(new Array(7,"UC_REPM_005"));
	arrReporMap.push(new Array(8,"UC_REPM_006"));
	arrReporMap.push(new Array(9,"UC_REPM_030"));
	arrReporMap.push(new Array(10,"UC_REPM_032"));
	arrReporMap.push(new Array(11,"UC_REPM_034"));
	arrReporMap.push(new Array(12,"UC_REPM_035"));
	arrReporMap.push(new Array(13,"UC_REPM_057"));
	arrReporMap.push(new Array(14,"UC_REPM_055"));
	arrReporMap.push(new Array(15,"UC_REPM_051"));
	arrReporMap.push(new Array(16,"UC_REPM_058"));
	arrReporMap.push(new Array(17,"UC_REPM_061"));
	arrReporMap.push(new Array(18,"UC_REPM_065"));
	arrReporMap.push(new Array(19,"UC_REPM_066"));
	arrReporMap.push(new Array(20,"UC_REPM_067"));
	arrReporMap.push(new Array(21,"UC_REPM_068"));
	arrReporMap.push(new Array(22,"UC_REPM_069"));
	arrReporMap.push(new Array(23,"UC_REPM_007"));
	arrReporMap.push(new Array(24,"UC_REPM_008"));
	arrReporMap.push(new Array(25,"UC_REPM_023"));
	arrReporMap.push(new Array(26,"UC_REPM_009"));
	arrReporMap.push(new Array(27,"UC_REPM_025"));
	arrReporMap.push(new Array(28,"UC_REPM_031"));
	arrReporMap.push(new Array(29,"UC_REPM_033"));
	arrReporMap.push(new Array(30,"UC_REPM_056"));
	arrReporMap.push(new Array(31,"UC_REPM_059"));
	arrReporMap.push(new Array(32,"UC_REPM_062"));
	arrReporMap.push(new Array(33,"UC_REPM_063"));
	arrReporMap.push(new Array(34,"UC_REPM_010"));
	arrReporMap.push(new Array(35,"UC_REPM_011"));
	arrReporMap.push(new Array(36,"UC_REPM_012"));
	arrReporMap.push(new Array(37,"UC_REPM_013"));
	arrReporMap.push(new Array(38,"UC_REPM_014"));
	arrReporMap.push(new Array(39,"UC_REPM_049"));
	arrReporMap.push(new Array(40,"UC_REPM_015"));
	arrReporMap.push(new Array(41,"UC_REPM_016"));
	arrReporMap.push(new Array(42,"UC_REPM_017"));
	arrReporMap.push(new Array(43,"UC_REPM_018"));
	arrReporMap.push(new Array(44,"UC_REPM_019"));
	arrReporMap.push(new Array(45,"UC_REPM_027"));
	arrReporMap.push(new Array(46,"UC_REPM_022"));
	arrReporMap.push(new Array(47,"UC_REPM_021"));
	arrReporMap.push(new Array(48,"UC_REPM_026"));
	arrReporMap.push(new Array(49,"UC_REPM_050"));
	arrReporMap.push(new Array(50,"UC_REPM_064"));
	arrReporMap.push(new Array(51,"UC_REPM_028"));
	arrReporMap.push(new Array(52,"UC_REPM_029"));
	*/
	
	function pgNodeClick(strFID){
		var intLen = arrReporMap.length;
		var intMenu = top.arrMenu.length;
		if (intLen > 0){
			var i = 0;
			do{
				if (arrReporMap[i][0] == strFID){
					var intMLen = intMenu;
					var x = 0;
					do{
						if (arrReporMap[i][1] == top.arrMenu[x][4]){
							top.LoadPage(top.arrMenu[x][3], top.arrMenu[x][4] , top.arrMenu[x][6]);
							break;
						}
						x++;
					}while(--intMLen);
					break;
				}
				i++;
			}while(--intLen);
		}
	}
	
	function misReportsOnClick(intIndex){
		applyTabStyle(intIndex);
		if (!blnMISReportsLoaded){
			frames["frmReport"].location.replace("showLoadJsp!loadMISReportsCore.action");
			blnMISReportsLoaded = true;
		}else{
			frames["frmReport"].applyTabStyle(intIndex + 1);
		}
	}
	
	function applyTabStyle(intIndex){
		if (intLastClick != -1){
			getFieldByID("tdMis" + intLastClick).style.backgroundColor = "#B0C0C0";
			getFieldByID("fntClr" + intLastClick).style.color = "black";
		}
		
		getFieldByID("tdMis" + intIndex).style.backgroundColor = "#143F3E";
		getFieldByID("fntClr" + intIndex).style.color = "white";
		intLastClick = intIndex;
	}
	
	/*
	 *  On Load
	 */
	function onLoad(){
		top[2].HideProgress();
		misReportsOnClick(0);
		clickOnNodeObj(foldersTree)
		clickOnNodeObj(foldersTree)
	}
	
	onLoad();
	// ------------------------------------- end of File ---------------------------------