var isEditClick = false;
var isAddClick = false;
jQuery(document).ready(function(){
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnSave').decorateButton();

	$('#editQueue :input').attr('disabled', true);
	enableGridButtons(false);
	enbleFormButtons(false);    
	clearPanel('editQueue')

	top[2].HideProgress();

	$("#btnSearch").click(function(){searchRequestQueues();});

	$('#btnAdd').click(function() {
		isAddClick = true;
		isEditClick = false;
		clearPanel('editQueue');
		enbleFormButtons(true);
		$('#editQueue :input').attr('disabled', false);
	});  

	$('#btnEdit').click(function() {
		isEditClick = true;
		isAddClick = false;
		enbleFormButtons(true);
		$('#editQueue :input').attr('disabled', false);
	});

	$('#btnDelete').click(function() {
		deleteQueue(); 		
	});

});  // end of onload jquery function

function deleteQueue(){
	var rowId =$("#requestsGrid").getGridParam('selrow');
		var rowData = $("#requestsGrid").getRowData(rowId);
		var queueId=rowData.QueueId;
		var queueName=rowData.QueueName;

		if (confirm("Do you want to delete the request queue \""+queueName+"\" with queue id "+queueId) == true) {
       			var data = {};
			data['queueId'] = queueId;
			$.ajax(    // we can show and hide progess bar
			  {	
				type: "POST", 
				dataType: 'json', 
				data: data , 
				url:"showRequestTable!deleteRequestQueue.action",		
				cache : false,
				complete: function(resp){	    // check the string values 
				      resp = eval("("+resp.responseText+")");
					if (resp.success) {
						alert("succesfully deleted");
						afterDeletecallback();
					}else{
						alert("Delete Request Queue not succesfull");
				     	 }
				     top[2].HideProgress();
				}
			  });
    		}else{
			return ; 
		}
	clearPanel('editQueue');
	$('#editQueue :input').attr('disabled', true);
	enableGridButtons(false);
	enbleFormButtons(false);	
}

function enableGridButtons(cond) {
	if (cond) {
		$("#btnEdit").enableButton();
		$("#btnDelete").enableButton(); 
	} else {
		$("#btnEdit").disableButton();
		$("#btnDelete").disableButton();
	}

}

function enbleFormButtons(cond) {
	if (cond) {
		$("#btnReset").enableButton();
		$("#btnSave").enableButton();
	} else {
		$("#btnReset").disableButton();
		$("#btnSave").disableButton();
	}

}

searchRequestQueues = function(){
	
	createGridforQueueResults();
	enableGridButtons(false);
	$('#editQueue :input').attr('disabled', true);
	clearPanel('editQueue');
}

createGridforQueueResults = function(){	

	var listFomatterToString = function(cellVal, options, rowObject){
		var treturn = "";
		if (cellVal!=null){
			treturn= cellVal.toString();
		}
		return treturn;
	}

	var listFomatterStringify = function(cellVal, options, rowObject){
		var treturn = "";
		if (cellVal!=null){
			treturn= JSON.stringify(cellVal);
		}
		return treturn;
	}
	
	$("#requestsGridContainer").empty().append('<table id="requestsGrid"></table><div id="requestsPages"></div>')
	var data = {};
	data['queueName'] = $("#searchQueueType").val();
	data['applicablePageIDForSearchQueues'] = $("#applicablePage").val();
	var requestGrid = $("#requestsGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'showRequestTable!searchRequestQueue.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           type : "POST",		           
				   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.success){
			            		  $.data(requestGrid[0], "gridData", mydata.requestsList);
			            		  requestGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			              }
			           }
			 });
			},
		    height: 200,
		    colNames:['QueueName','Description','Applicable pages','Assigned Users','PageIdstringify','PageNamestringify','UserIdstringify','UserNamestringify','QueueId'], 
		    colModel:[ 	{name:'QueueName',width:40, align:"center", jsonmap:'queueRequestDTO.queueName'},   
			        {name:'Description' ,width:40, align:"center", jsonmap:'queueRequestDTO.description'},
                                {name:'Applicable pages', width:40, align:"center", jsonmap:'queueRequestDTO.pageNameList',formatter: listFomatterToString},
{name:'Assigned Users', width:40, align:"center",jsonmap:'queueRequestDTO.userNameList',formatter: listFomatterToString},
{name:'PageIdstringify',width:40, align:"center", jsonmap:'queueRequestDTO.pageIdList',hidden:false,formatter: listFomatterStringify},
{name:'PageNamestringify',width:40, align:"center", jsonmap:'queueRequestDTO.pageNameList',hidden:false,formatter: listFomatterStringify},
{name:'UserIdstringify',width:40, align:"center", jsonmap:'queueRequestDTO.userIdList',hidden:false,formatter: listFomatterStringify},
{name:'UserNamestringify',width:40, align:"center", jsonmap:'queueRequestDTO.userNameList',hidden:false,formatter: listFomatterStringify},
{name:'QueueId',width:40, align:"center", jsonmap:'queueRequestDTO.queueId',hidden:false}				   	 
                          ], 
		        caption: "Requests Submitted",
		        rowNum : 5,
			viewRecords : true,
			width : 1000,
			pager: jQuery('#requestsPages'),
			jsonReader: { 				
				root: "requestsList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				emptyrecords: "Nothing to display",
			},
			rownumbers: true
		    
	}).navGrid("#requestsPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});
	$("#requestsGrid").click(function(){ requestsGridOnClick(); });
}

requestsGridOnClick = function(){
	clearPanel('editQueue')
	var rowId =$("#requestsGrid").getGridParam('selrow');  
	var rowData = $("#requestsGrid").getRowData(rowId);
	$("#queueName").val(rowData.QueueName);
	$("#queueDescription").val(rowData.Description);
	fillSelectPagesUI(rowData.PageIdstringify,rowData.PageNamestringify); // for pages check for null, other wise the executio
	fillSelectUsersUI(rowData.UserIdstringify,rowData.UserNamestringify); // for users
	$("#btnDelete").enableButton();  
	$("#btnEdit").enableButton();
	$( "#selectedData" ).val(rowData.UserIdstringify);
		
}

function fillSelectPagesUI(val,Names) {		
	var jstring=jQuery.parseJSON(val);
	var namestring=jQuery.parseJSON(Names);
	var arrayLength = jstring.length;    
	for (var i = 0; i < arrayLength; i++) {
		var optVal=jstring[i];
		var optText=namestring[i];
    		if ($("select#selectedPages option[value='" + optVal + "']").length == 0) {
			$("#selectedPages").append("<option value=\"" + optVal + "\">" + optText + "</option>");
		}
	}
}

function clearPanel(panelId) {
	$("#" + panelId + " input[type=\"text\"]").val('');
	$("#" + panelId + " input[type=\"hidden\"]").val('');
	$("#" + panelId + " textarea").val('');
	$("#" + panelId + " select[id^=\"selectablePages\"]").html("");
	$("#" + panelId + " select[id=\"selectableUsers\"]").html("");
	$("#" + panelId + " select[id^=\"selectedPages\"]").html("");
	$("#" + panelId + " select[id=\"selectedUsers\"]").html("");
}

function fillSelectUsersUI(val,Names) {		
	var jstring=jQuery.parseJSON(val);
	var namestring=jQuery.parseJSON(Names);
	var arrayLength = jstring.length;
	for (var i = 0; i < arrayLength; i++) {
		var optVal=jstring[i];
		var optText=namestring[i];
    		if ($("select#selectedUsers option[value='" + optVal + "']").length == 0) {
			$("#selectedUsers").append("<option value=\"" + optVal + "\">" + optText + "</option>");
		}
	}
}

function addUsers() {
	var optVal;
	var optText;

	$("select#selectableUsers option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				if ($("select#selectedUsers option[value='" + optVal + "']").length == 0) {
					$("#selectedUsers").append("<option value=\"" + optVal + "\">" + optText + "</option>");
				}
				$("select#selectableUsers option").remove("[value='" + optVal + "']"); // I moved this down
			});
}

function addPage() {
	var optVal;
	var optText;

	$("select#selectablePages option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				if ($("select#selectedPages option[value='" + optVal + "']").length == 0) {
					$("#selectedPages").append("<option value=\"" + optVal + "\">" + optText + "</option>");
				}
				$("select#selectablePages option").remove("[value='" + optVal + "']"); 
			});
}

function removePage() {
	var optVal;
	var optText;

	$("select#selectedPages option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				$("#selectablePages").append("<option value=\"" + optVal + "\">" + optText + "</option>");
				$("select#selectedPages option").remove("[value='" + optVal + "']");
			});
}

function removeUsers() {
	var optVal;
	var optText;

	$("select#selectedUsers option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				$("#selectableUsers").append("<option value=\"" + optVal + "\">" + optText + "</option>");
				$("select#selectedUsers option").remove("[value='" + optVal + "']");
			});
}

function submitRequestQueue() {   // first lets do for the create new request queue
	if(validateCreateEditRequestQueue()){  
		var data1 = {};
		var strUrl;
		if(isEditClick){
			var rowId =$("#requestsGrid").getGridParam('selrow');
			var rowData = $("#requestsGrid").getRowData(rowId);
			var queueId=rowData.QueueId;
			data1['queueId'] = queueId;
			data1['stringifyUserIds'] = document.getElementById("selectedData").value; 
			strUrl = "showRequestTable!updateRequestQueue.action";
		}else{
			strUrl = "showRequestTable!saveRequestQueue.action";
		}
		
	
		data1['queueName'] = $("#queueName").val();     
		data1['description'] = $("#queueDescription").val();
		data1['status'] = "ACT";		
		
		var selectedpages=new Array();
		$("select#selectedPages option").each(
			function() {
				selectedpages.push($(this).val());
		});
		data1['pageIdlist'] = JSON.stringify(selectedpages);//pageIdlist

		var selectedusers=new Array();
		$("select#selectedUsers option").each(
			function() {
				selectedusers.push($(this).val());
		});
		data1['userIdlist'] = JSON.stringify(selectedusers);		

		var success;
		var message;
		$.ajax({ 
			url : strUrl,
			beforeSend : top[2].ShowProgress(),
			data : data1,
			type : "POST",
			dataType : "json",
			complete: function(jsonData,stat){	        	  
				if(stat=="success") {
			        	mydata = eval("("+jsonData.responseText+")");
					if (mydata.success) {
						if(isEditClick){
							alert("Request Queue succesfully updated");
							afterCallback();
					 	 }else{
							alert("Request Queue succesfully created");
					  	 }
					}	
			            	  
			         }
				top[2].HideProgress();
			}
		});				
		clearPanel('editQueue');
		$('#editQueue :input').attr('disabled', true);
		enableGridButtons(false);
		enbleFormButtons(false);   		
	}
	
}

function afterCallback() {
	if(isEditClick){
		createGridforQueueResults();
	} 
	isEditClick = false;
	isAddClick = false;
}

function afterDeletecallback() {
	createGridforQueueResults();
	isEditClick = false;
	isAddClick = false;
}

function searchForPages() {		
	var data3 = {};	
	data3['pageNamePart'] = $("#pageNameInput").val(); 		
	 $.ajax({
			           url : 'showRequestTable!searchForPages.action',
			           data: data3,
			           dataType:"json",
			           type : "POST",		           
				   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
							var arr=mydata.pagedResult;
							fillPagesFromSearch(arr);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			              }
			           }
		});
}

function searchForUsers() {
	var data2 = {};	
	data2['userName'] = $("#userNameInput").val(); 		
	 $.ajax({
			           url : 'showRequestTable!searchForUsers.action',
			           data: data2,
			           dataType:"json",
			           type : "POST",		           
				   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
							var arr=mydata.userSearchResult;
							fillUsersFromSearch(arr);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			              }
			           }
		});
}

function fillUsersFromSearch(arr) {		
	var arrayLength = arr.length;
	for (var i = 0; i < arrayLength; i++) {
		var optVal=arr[i].userId;
		var optText=arr[i].displayName;
    		if ($("select#selectableUsers option[value='" + optVal + "']").length == 0) {
			$("#selectableUsers").append("<option value=\"" + optVal + "\">" + optText + "</option>");
		}
	}
}

function fillPagesFromSearch(arr) {		
	var arrayLength = arr.length;
	for (var i = 0; i < arrayLength; i++) {
		var optVal=arr[i].pageId;
		var optText=arr[i].pageName;
    		if ($("select#selectablePages option[value='" + optVal + "']").length == 0) {
			$("#selectablePages").append("<option value=\"" + optVal + "\">" + optText + "</option>");
		}
	}
}

function validateCreateEditRequestQueue(){
	if($("#queueName").val()==""){
		showCommonError("Error",queuenameEmpty);
		return false;
	}
	if($("#queueDescription").val()==""){
		showCommonError("Error",descriptionEmpty);
		return false;
	}	
	if($('#selectedPages').has('option').length == 0 ){
		showCommonError("Error",applicablepagesEmpty);
		return false;
	}
	if($('#selectedUsers').has('option').length == 0){
		showCommonError("Error",assignedusersEmpty);
		return false;
	}
	return true;
}















