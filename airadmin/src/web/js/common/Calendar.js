	/*
	*********************************************************
		Description		: Load Calendar
		Author			: Rilwan A. Latiff
		Version			: 1.1
		Last Modified	: 10th June 2005
	*********************************************************	
	*/
	
	function Calendar(){
		var now		= new Date();
		var day		= now.getDate();
		var month	= (now.getMonth() + 1);
		var year	= now.getFullYear();
			
		// ----------------- Grid Properties
		this.ID = 0;					// Data Grid ID
		this.currentDate = day + "/" + month + "/" + year;
		this.noYears = 10;
		this.yearStart = year ;
		this.top = 0; 
		this.left = 0;
		if (arguments.length == 1){
			this.spanTagID = arguments[0];	
		}else{
			this.spanTagID = "spn_Cal_Layer" ;
		}
		this.borderColor = "Silver";
		this.headerTextColor = "White";
		this.onClick = "";
		this.blnDragCalendar = true;
		this.blnHeader = true;
		this.blnAutoHide = true;
		this.disableUpto = "";
		
		this.showCalendar = showCalendar;
		this.buildCalendar = buildCalendar;
		this.hideCalendar = hideCalendar;
		
		window._CalendarPage = "../../js/common/Calendar.jsp";
		window._CalendarImgPath = "../../images/";
		window._CalendarLayer = "spnCalLayer";
		
		if (!window._arrCalObj) window._arrCalObj = new Array();
		window._arrCalObj[this.spanTagID] = this;
		window._arrCalObj[window._arrCalObj.length] = this;
		
		// ---------------------- Identify the Browser;
		//this.delayCalendar = delayCalendar;
		this._calDelay;
		
		window._CalBrowser = true ;
		ua = navigator.userAgent;
		s = "MSIE";
		if ((i = ua.indexOf(s)) >= 0) {
			window._CalBrowser = true;
		}

		s = "Netscape6/";
		if ((i = ua.indexOf(s)) >= 0) {
			window._CalBrowser = false
		}
		
		s = "Gecko";
		if ((i = ua.indexOf(s)) >= 0) {
			window._CalBrowser = false;
		}
	}

	function delayCalendar(strID){
		var objContainer = document.getElementById(window._CalendarLayer);
		var strGridHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrCal.length ; i++){
			var objCal = objContainer.arrCal[i];
			if (objCal.spanTagID == strID){
				var strFrameName = "frm_Calendar_" + objCal.spanTagID ;
				if (frames[strFrameName].blnCalendarLoaded){
					clearTimeout(objCal._calDelay);
					
					var strDivCalID = 'div_Calendar_' + objCal.spanTagID ;
					var objControl = getFieldByID(strDivCalID);
					objControl.style.left = objCal.left;
					objControl.style.top = objCal.top; 
					
					setVisible(strDivCalID , true);
					
					var arrCurrDate = objCal.currentDate.split("/");
					var strFrameName = "frm_Calendar_" + objCal.spanTagID ;
					frames[strFrameName].SetDate(arrCurrDate[0], arrCurrDate[1], arrCurrDate[2], objCal.noYears, objCal.spanTagID, objCal.disableUpto);	
				}
				break;
			}
		}
	}

	function showCalendar(objEvent){
		var objContainer = document.getElementById(window._CalendarLayer);
		var strGridHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrCal.length ; i++){
			var objCal = objContainer.arrCal[i];
			if (objCal.spanTagID == this.spanTagID){
				var strDelayFunction = "delayCalendar('" + objCal.spanTagID + "')"
				
				var x = 0 ;
				var y = 0 ; 
				if (objEvent != null){
					if (window._CalBrowser) {
						x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
						y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
					}else{
						x = objEvent.clientX + window.scrollX;
						y = objEvent.clientY + window.scrollY;
					}	
				}
				if (objCal.top == 0){objCal.top = y + 20;}
				if (objCal.left == 0){objCal.left = x;}
				
				objCal._calDelay = setInterval(strDelayFunction, 300)
				break;
			}
		}
	}

	function buildCalendar(){
		document.writeln('<span id="' + window._CalendarLayer  + '" style="visibility:hidden;">?<\/span>');
		var objContainer = document.getElementById(window._CalendarLayer);
		objContainer.arrCal = new Array();
		for (var i = 0 ; i < window._arrCalObj.length ; i++){
			objContainer.arrCal[i] = window._arrCalObj[i];
			
			var strMoveCursor = "cursor:default;" ;
			if (objContainer.arrCal[i].blnDragCalendar){
				strMoveCursor = "cursor:move;";
			}
		
			var strCalHTML = "" ;
			var intBoxWidth = "260";
			var strBoxStyle = "font-family:verdana;font-size:11px;background-color: "+ objContainer.arrCal[i].borderColor + ";border: 2px groove " + objContainer.arrCal[i].borderColor + ";color: "+ objContainer.arrCal[i].borderColor + ";padding: 1px;position: absolute;cursor:"  + strMoveCursor + ";";
			if (!objContainer.arrCal[i].blnHeader){
				if (window._CalBrowser){
					strBoxStyle += "height:282px;" //"height:195px;"
				}else{
					strBoxStyle += "height:262px;" //"height:175px;"
				}
			}else{
				strBoxStyle += "height:282px;"// "height:195px;"
			}
			
			var strBarStyle = "color: white;font-weight: bold;padding: 1px 1px 1px 1px;" + strMoveCursor
			var strHDStyle = "font-family:verdana;font-size:11px;color:white;font-weight:bold;" + strMoveCursor
				
			var strMsgBoxHTML = "" ;
			var strSPNID = window._CalendarLayer + '_' + objContainer.arrCal[i].spanTagID;
			
			strMsgBoxHTML = '<span id="' + strSPNID + '">?<\/span>'
			document.writeln(strMsgBoxHTML);
			
			var strDivCalID = 'div_Calendar_' + objContainer.arrCal[i].spanTagID ;
			objContainer.arrCal[i].calDivID = strDivCalID;
			
			strMsgBoxHTML = ""
			strMsgBoxHTML += '<div id="' + strDivCalID + '" style="position:absolute;left:400px;top:150px;' + strBoxStyle + 'z-index:200;"';
			if (objContainer.arrCal[i].blnDragCalendar){
				strMsgBoxHTML += ' onmousedown="dragStart(event, '+ "'" + strDivCalID + "'" + ')"';
			}
			strMsgBoxHTML += ' >';
			if (objContainer.arrCal[i].blnHeader){
				strMsgBoxHTML += '	<span id="div_MsgBox_Header' + objContainer.arrCal[i].spanTagID+ '" style="height:20px;width:' + intBoxWidth + ';'+ strBarStyle +'"';
				if (objContainer.arrCal[i].blnDragCalendar){
						strMsgBoxHTML += ' onmousedown="dragStart(event, '+ "'" + strDivCalID + "'" + ')"'; 
				}
				strMsgBoxHTML += ' >';
				strMsgBoxHTML += '		<table width="' + intBoxWidth + '" border="0" cellpadding="2" cellspacing="0" align="left">'
				strMsgBoxHTML += '			<tr>'
				strMsgBoxHTML += '				<td><font style="color:' + objContainer.arrCal[i].headerTextColor + ';font-size:11px;"><b>:: Calendar<\/b><\/font><\/td>'
				strMsgBoxHTML += '				<td align="right"><a href="javascript:void(0)" onclick="HideCalendarClick(' + "'" + strDivCalID + "'" + ');return false;"><IMG SRC="' + window._CalendarImgPath + 'Close_no_cache.jpg" border="0" title="Close"><\/a><\/td>'
				strMsgBoxHTML += '			<\/tr>'
				strMsgBoxHTML += '		<\/table>'
				strMsgBoxHTML += '	<\/span>'
			}
			
			if (window._CalBrowser){
				strMsgBoxHTML += '	<div id="div_Calendar_Content_' + objContainer.arrCal[i].spanTagID + '' + '" style="height:285px;width:' + intBoxWidth + ';">'
			}else{
				strMsgBoxHTML += '	<div id="div_Calendar_Content_' + objContainer.arrCal[i].spanTagID + '' + '" style="height:265px;width:' + intBoxWidth + ';">'
			}
			strMsgBoxHTML += '	<iframe src="' + window._CalendarPage + '" id="frm_Calendar_' + objContainer.arrCal[i].spanTagID + '" name="frm_Calendar_' + objContainer.arrCal[i].spanTagID + '" height="100%" width="100%" frameborder="no" scrolling="no"><\/iframe>'
			strMsgBoxHTML += '<\/div>'
			strMsgBoxHTML += '<\/div>'
			
			DivWrite(strSPNID, strMsgBoxHTML);
			setVisible(strSPNID, false);
		}
	}


	function HideCalendarClick(strID){
		setVisible(strID, false);
	}
	
	function hideCalendar(){
		HideCalendarClick('div_Calendar_' + this.spanTagID);
	}

	function DateReturn(strDD, strMM, strYY, strObject){
		var objContainer = document.getElementById(window._CalendarLayer);
		var strGridHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrCal.length ; i++){
			var objCal = objContainer.arrCal[i];
			if (objCal.spanTagID == strObject){
				objCal.currentDate = strDD + "/" + strMM + "/" + strYY;
				if (objCal.blnAutoHide){
					var strSpanID = 'div_Calendar_'+ strObject
					HideCalendarClick(strSpanID);
				}
				if (objCal.onClick != ""){
					var strOnClickFunction = eval(objCal.onClick + "('" + objCal.currentDate + "','" + objCal.ID + "')");
				}
				strOnClickFunction;
				break;
			}
		}
	}
	// --------------------------------- End of Calendar 
