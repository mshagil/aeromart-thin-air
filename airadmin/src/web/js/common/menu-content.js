var strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
addMenu("AAMenu", "menu-top");
for (var i = 0 ; i < arrMenu.length ; i++){
	switch (arrMenu[i][0]){
		case "M" :
			addLink(arrMenu[i][1], arrMenu[i][2], "", "javascript:top.LoadPage('" + arrMenu[i][3] + "','" + arrMenu[i][4] + "','" + arrMenu[i][6] + "')", "");
			break ;
		case "S" :
			addSubMenu(arrMenu[i][1], arrMenu[i][2], "", "", arrMenu[i][5], "");
			break ;
		case "L" :
			addSeparator(arrMenu[i][1], "");
			break;
	}
}
endMenu();

function CloseWindow(){
	if (confirm("Are you sure you want to exit ? \n (Any unsaved data will be lost)")){
		top[0].CloseApplication();
		//	makePOST("../public/logout.do","");
	}
}


function processStateChange(){
	/*if (httpReq.readyState==4){
		top.location.replace("closeMenu");
	}*/
}
function LoadHome(){
	if (loadCheck()){
		strLastPage = "";
		top[2].MessageReset();
		top[1].location.replace("showMain");
	}
}

function loadCheck(){
	if (parent.pageEdited){
		return confirm("Changes will be lost! Do you wish to Continue?");
	}else{
		strLastPage = "";
		return true;
	}
}

function loadCheck(blnStat){
	if (blnStat)
	{
		return confirm("Changes will be lost! Do you wish to Continue?");
	}else{
		return true;
	}
}

function LoadXBE(){
	alert("load XBE");
}

function LoadPage(strPage, strFID, strDes){
	//if ((loadCheck())){
		top[0].initializeVar();
		if (strPage.indexOf("javascript:") != -1){
			eval(strPage.substr(11, strPage.length - 10));
		}else if (strPage.indexOf("http") != -1){
			window.open(strPage, 'winXBE', 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=1024,height=768,resizable=no,top=1,left=1');
		}else{
			if (top[1].objTMenu.tabLoad(strPage, strDes, strFID, "")){
				if(strFID != "SC_ADMN_022" && strFID != "SC_ADMN_002")
				top[2].ShowProgress();
			};	
			//top[1].location.replace(strPage);
		}
		strLastPage = strPage;
		setFID(strFID);
	//}
}

function defTabClick(strID){
	setFID(strID);
	top[2].HidePageMessage();
	return true;
}

function setFID(strFID){
	top[2].DivWrite("spnScreenID","<font class='fontPGInfo'>Screen ID : <b>" + strFID + "<\/b><\/font>");
}

function saveConfirmation(){
 alert("Successfully Saved");
}

// Quick Launch 

var strQLStart		= "AA148_no_cache.gif";
var strQLEnd		= "AA158_no_cache.gif";
var strQLSep		= "AA159_no_cache.gif";
var strQuickLaunch  = "";

function buildQuickLaunch(){
	var strHTMLText = "";
	var strHTMLData = "";
	var intWidth	= 0 ; 
	var strToolTip  = "";

	//arrQuickLaunch.push(new Array('M009_no_cache.gif',108,'showFlightLoad.action','SC_FLGT_002','admin.fla','Flt. Load Analysis', 'Flt. Load Analysis'));
	//arrQuickLaunch.push(new Array('M010_no_cache.gif',48,'showFlightLoad.action','DEMO_SC_SCHD_002','','Fares', 'Fares'));
	
	for (var i = 0 ; i < arrQuickLaunch.length ; i++){
		strToolTip = "";
		if (strHTMLData != ""){
			strHTMLData += '<td style="hight:55px;width:2px;"><img src="../../images/' + strQLSep + '"></td>';
			intWidth = intWidth + 2;
		}
		
		if (arrQuickLaunch[i][5] != ""){
			strToolTip = 'title = "' + arrQuickLaunch[i][5]  + '" ';
		}
		strHTMLData += '<td style="hight:55px;width:' + arrQuickLaunch[i][1] + 'px;"><a href="javascript:top.LoadPage(' +  "'" + arrQuickLaunch[i][2] + "','" + arrQuickLaunch[i][3] + "','" + arrQuickLaunch[i][6] + "'" + ');hideQuickLaunch();" ' + strToolTip + '><img border="0" src="../../images/' + arrQuickLaunch[i][0] + '"><\/a><a/td>';
		intWidth = intWidth + Number(arrQuickLaunch[i][1]);
	}
	
	if (intWidth > 0){
		intWidth = intWidth + 17 + 29;
		strHTMLText = '<table width="' + intWidth + '" border="0" cellpadding="0" cellspacing="0" align="right">';
		strHTMLText += '<tr>';
		strHTMLText += '<td style="hight:55px;width:17px;"><img src="../../images/' + strQLStart + '"></td>';
		strHTMLText	+= strHTMLData;
		strHTMLText += '<td style="hight:55px;width:29"><img USEMAP="#QL" border="0" src="../../images/' + strQLEnd + '"></td>';
		strHTMLText += '<\/tr>';
		strHTMLText += '<\/table>';
		strHTMLText += '<MAP NAME=QL>';
		strHTMLText += '<AREA HREF="javascript:void(0)" onclick=hideQuickLaunch(); return false;" title="Hide Quick Launch" COORDS="4,28,20,45" shape="rect">';
		strHTMLText += '</MAP>';
				
	}
	strQuickLaunch = strHTMLText;
	arrQuickLaunch = null;
}
buildQuickLaunch();