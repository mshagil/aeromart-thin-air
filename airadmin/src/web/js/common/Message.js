	/*
	*********************************************************
		Description		: Message Display using IFrame (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 1st June 2005
	*********************************************************	
	*/

	function MessageBox(){
		this.width = 200 ;
		this.top = 100;
		this.left = 0; 
		this.MessageType = "Error" //Error //Warning //Confirmation
		this.HeaderText = ""
		this.MessageText = "";
		this.Seconds = 0;
		this.blnDragMessageBox = true ;
		
		this.ShowMessage = ShowMessage;
		this.HideMessage = HideMessage;
		window._MsgPage = "../../js/common/ShowErrorsIframe.jsp";
		window._MsgImgPath = "../../images/";
		window._MsgImgPath2 = "../../images/";
		window._MsgStatus = false ;
		window._blnDragMessageBox = this.blnDragMessageBox ;
			
		function DrawMsgBox(){
			var strBoxStyle = "font-family:verdana;font-size:11px;background-color: red;border: 1px solid red;color: red;padding: 0px;position: absolute;cursor:move;"
			var strBarStyle = "color: white;font-weight: bold;padding: 1px 1px 1px 1px;cursor:move;"
			var strHDStyle = "font-family:verdana;font-size:11px;color:white;font-weight:bold;"
		
			var strMsgBoxHTML = "" ;
			strMsgBoxHTML = '<span id="spn_MsgBox" style="visibility:hidden;">?<\/span>'
			document.writeln(strMsgBoxHTML);
			
			strMsgBoxHTML = ""
			strMsgBoxHTML += '<div id="div_MsgBox" style="height:0px;position:absolute;left:400px;top:150px;' + strBoxStyle + '" onmousedown="dragMessageBox(event, '+ "'" + 'div_MsgBox' + "'" + ')">'
			strMsgBoxHTML += '	<span id="div_MsgBox_Header" style="height:0px;width:100%;'+ strBarStyle +'" onmousedown="dragMessageBox(event, '+ "'" + 'div_MsgBox' + "'" + ')">'
			strMsgBoxHTML += '	<\/span>'
			strMsgBoxHTML += '	<div id="div_MsgBox_Content" style="height:0px;width:200px;">'
			strMsgBoxHTML += '	<iframe src="' + window._MsgPage +'" id="frm_MsgBox" name="frm_MsgBox" height="100%" width="100%" frameborder="no" scrolling="no"><\/iframe>'
			strMsgBoxHTML += '<\/div>'
			strMsgBoxHTML += '<\/div>'
			
			DivWrite("spn_MsgBox", strMsgBoxHTML);
			setVisible("div_MsgBox", false);
		}
		DrawMsgBox();
	}
	
	function dragMessageBox(objEvent, strID){
		if (window._blnDragMessageBox){
			dragStart(objEvent, strID);
		}
	}

	function ShowMessage(){
		var strBGColor = ""
		var strMsgColor = "" ;
		var strImageURL = "";
		var strHDText = ""
		
		switch (this.MessageType.toUpperCase()){
			case "ERROR" : 
				strBGColor = "red"; 
				strMsgColor = "red";
				strImageURL = window._MsgImgPath2 + "Err_no_cache.gif"
				strHDText = "Error";
				break;
			case "WARNING" : 
				strBGColor = "#F4B100"; 
				strMsgColor = "black";
				strImageURL = window._MsgImgPath2 + "Warn_no_cache.gif"
				strHDText = "Warning";
				break;
			case "SUCCESS" :
			case "CONFIRMATION" : 
				strBGColor = "Green"; 
				strMsgColor = "Green";
				strImageURL = window._MsgImgPath2 + "Conf_no_cache.gif"
				strHDText = "Confirmation";
				break;
		}
		
		if (this.HeaderText == ""){this.HeaderText = strHDText;}
		if (this.left == ""){
			this.left = (window.screen.width - Number(this.width)) / 2;
		}
		
		var objMsgControl = getFieldByID("div_MsgBox");
		var strMsg = "" ;
		
		objMsgControl.style.backgroundColor = strBGColor ;
		objMsgControl.style.borderColor = strBGColor ;
		objMsgControl.style.color = strMsgColor;
		objMsgControl.style.left = this.left + "px";
		objMsgControl.style.top = this.top + "px";
		objMsgControl.style.width = this.width + "px";
		window._blnDragMessageBox = this.blnDragMessageBox;
		
		var objMsgHDControl = getFieldByID("div_MsgBox_Header");
		objMsgHDControl.style.borderColor = strBGColor ; 
		if (browser.isIE){
			objMsgHDControl.style.width = (this.width) + "px";
		}else{
			objMsgHDControl.style.width = (this.width - 22) + "px";
			strMsg = "?<br>"
		}

		var strMsgBoxHTML = "";
		strMsgBoxHTML += '		<table width="100%" border="0" cellpadding="2" cellspacing="0" align="right">'
		strMsgBoxHTML += '			<tr>'
		strMsgBoxHTML += '				<td><font style="color:white;font-size:11px;"><b>' + this.HeaderText + '<\/b><\/font><\/td>'
		strMsgBoxHTML += '				<td align="right"><a href="javascript:void(0)" onclick="HideMessage();return false;"><IMG SRC="' + window._MsgImgPath + 'MiniMize_no_cache.jpg" border="0" title="Minimize"><\/a><\/td>'
		strMsgBoxHTML += '			<\/tr>'
		strMsgBoxHTML += '		<\/table>'
				
		DivWrite("div_MsgBox_Header", strMsgBoxHTML);
		
		
		strMsg += ' <img src="'+ strImageURL + '" border="0"><br>' + this.MessageText;
		var objMsgContControl = getFieldByID("div_MsgBox_Content");
		objMsgContControl.style.width = this.width + "px";
		
		frames["frm_MsgBox"].WriteMsg(strMsg, strMsgColor);
		setVisible("div_MsgBox", true);
		
		if (this.Seconds > 1){
			if (window._MsgTimer != null){clearInterval(window._MsgTimer);}
			window._MsgTimer = setInterval("HideMessage()", (Number(this.Seconds) * 1000))
		}
		window._MsgStatus = true ;
	}

	function ReSizeMsgBox(intHeight){
		var intNHeight = intHeight ;
		if (intHeight > 50){intNHeight = intHeight - 30 ;}
		var objControl = getFieldByID("div_MsgBox");
		objControl.style.height = intHeight + 18   + "px"
		objControl.style.zIndex = 1001;
		
		var objControl = getFieldByID("div_MsgBox_Header");
		objControl.style.zIndex = 1002;
		
		var objControl = getFieldByID("frm_MsgBox");
		objControl.height = intHeight + "px";
	}

	function HideMessage(){
		if (window._MsgTimer != null){clearInterval(window._MsgTimer);}
		setVisible("div_MsgBox", false);
		window._MsgStatus = false;
	}
	
	// ---------------- Page level functions
	function ShowPageMessage(){
		if (objMsg.MessageText != ""){
			var strImageURL = "" ;
			var strColor = "black";
			var errorClass = "";
			//objMsg.ShowMessage();
			
			switch (objMsg.MessageType.toUpperCase()){
				case "ERROR" : 
					strImageURL = window._MsgImgPath + "Err_no_cache.gif"; 
					strColor = "red";
					errorClass = "Error";
					break;
				case "WARNING" : 
					strImageURL = window._MsgImgPath + "Warn_no_cache.gif"; 
					strColor = "black";
					errorClass = "Warning";
					break;
				case "SUCCESS" :
				case "CONFIRMATION" : 
					strImageURL = window._MsgImgPath + "Conf_no_cache.gif"; 
					strColor = "black";
					errorClass = "Confirm";
					break;
			}
			//setImage("imgError", strImageURL);
			//setVisible("imgError", true);
			//setStyleClass("tdSTBar", "StatsBar");
			//setDisplay("imgError", true);
			//DivWrite("spnDate", "<font style='Color:" + strColor + "' class='fntBold'>&nbsp;" + objMsg.MessageText + "</font>");
			dispalyError(objMsg.MessageText, errorClass);
		}
	}
	
	function ShowPageAlertMessage(){
		if (objMsg.MessageText != ""){
			alert(objMsg.MessageText);
		}
	}
	
	function ShowProgress(){
		HidePageMessage();
		/*
		var strImage = "../../images/Progress_no_cache.gif";
		setVisible("imgError", false);
		DivWrite("spnDate", "<img src='" + strImage +"'>");
		*/
		top[1].setVisible("divLoadMsg", true);
	}
	
	function HideProgress(){
		//setVisible("imgError", false);
		top[1].setVisible("divLoadMsg", false);
		//DivWrite("spnDate", "&nbsp;");
	}
	
	function HidePageMessage(){
		DivWrite("spnDate", "&nbsp;");
		setStyleClass("tdSTBar", "StatsBarDef");		
	}
	
	function MessageResetForStatusBar(){		
		objMsg.MessageText="";	
		setDisplay("imgError", false);
		MessageReset();
	}
	
	function MessageReset(){
		setVisible("imgError", false);	
		HidePageMessage();
	}
	
	function Body_onKeyDown(objEvent){
		var strKeyValue = objEvent.keyCode ;
		if (objEvent.altKey){
			if (strKeyValue == 190){
				if (window._MsgStatus){
					HideMessage();
				}else{
					ShowPageMessage();
				}
			}
		}
	}      
	
	function pageMode(strMode){
		setVisible("imgEditMode", false);	
		switch (strMode){
			case "E" : setImage("imgEditMode","../../images/AA128_no_cache.jpg") ;setVisible("imgEditMode", true); break;
			case "R" : setImage("imgEditMode","../../images/AA129_no_cache.jpg") ;setVisible("imgEditMode", true); break;
		}
	}
	
	var objMsg = new MessageBox();
	// --------------------------------- End of Message Box	
	

