var templatesData = {};
var generateChargesPane = false;
var isEditClick = false;
var isAddClick = false;
var nextSeatMinSeats=1; //max seats allowed through pnl is 2.
var nextSeatMaxSeats=1;
var selectedGridRowId = "";

jQuery(document).ready(function(){	


$('#btnClose').decorateButton();
$('#btnReset').decorateButton();
$('#btnSave').decorateButton();

//$('#editQ :input').attr('disabled', true);
$("#btnClose").attr('disabled', false);

enableGridButtons(false);
enbleFormButtons(false);

top[2].HideProgress();

$("#btnSearch").click(function(){searchRequestQueues();});

});

function enableGridButtons(cond) {
	if (cond) {
		$("#btnEdit").enableButton();
		$("#btnDelete").enableButton();
	} else {
		$("#btnEdit").disableButton();
		$("#btnDelete").disableButton();
	}

}

function enbleFormButtons(cond) {
	if (cond) {
		$("#btnReset").enableButton();
		$("#btnSave").enableButton();
	} else {
		$("#btnReset").disableButton();
		$("#btnSave").disableButton();
	}

}

searchRequestQueues = function(){
	
	createGridforQueueResults();
	
}

createGridforQueueResults = function(){	

	var listFomatter = function(cellVal, options, rowObject){
		var treturn = "";
		if (cellVal!=null){
			treturn=cellVal.toString()
		}
		return treturn;
	}
	
	$("#requestsGridContainer").empty().append('<table id="requestsGrid"></table><div id="requestsPages"></div>')
	var data = {};
	data['queueType'] = $("#searchQueueType").val();
	data['applicablePageID'] = $("#applicablePage").val();
	var requestGrid = $("#requestsGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'showRequestTable.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(requestGrid[0], "gridData", mydata.requestsList);
			            		  requestGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames:['Queue Name','Description','Applicable pages','Assigned Users','Hidden','Fomatter'], 
		    colModel:[ 	{name:'Queue Name',width:40, align:"center", jsonmap:'queueRequestDTO.queueName'},   
			        {name:'Description' ,width:40, align:"center", jsonmap:'queueRequestDTO.description'},
                                {name:'Applicable pages', width:40, align:"center", jsonmap:'queueRequestDTO.pageNameList',formatter: listFomatter},
{name:'Assigned Users', width:40, align:"center",jsonmap:'queueRequestDTO.userNameList',formatter: listFomatter},
{name:'Hidden',width:40, align:"center", jsonmap:'queueRequestDTO.queueName',hidden:true},
{name:'Fomatter',width:40, align:"center", jsonmap:'queueRequestDTO.pageIdList', formatter: listFomatter , hidden:false}
					   	 
                          ], 
		        caption: "Requests Submitted",
		        rowNum : 3,
			viewRecords : true,
			width : 1000,
			pager: jQuery('#requestsPages'),
			jsonReader: { 				
				root: "requestsList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
			},
			rownumbers: true
		    
	}).navGrid("#requestsPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});
	$("#requestsGrid").click(function(){ requestsGridOnClick(); });
}

requestsGridOnClick = function(){
	var rowId =$("#requestsGrid").getGridParam('selrow');  
	var rowData = $("#requestsGrid").getRowData(rowId);
		
	alert(rowId);	
}

function addRoutes() {
	var optVal;
	var optText;

	$("select#availableRoutes option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				if ($("select#applicableRoutes_1 option[value='" + optVal + "']").length == 0) {
					$("#applicableRoutes_1").append("<option value=\"" + optVal + "\">" + optText + "</option>");
					$("select#availableRoutes option").remove("[value='" + optVal + "']");
				}
			});
}

function removeRoutes() {
	var optVal;
	var optText;

	$("select#applicableRoutes_1 option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				$("#availableRoutes").append("<option value=\"" + optVal + "\">" + optText + "</option>");
				$("select#applicableRoutes_1 option").remove("[value='" + optVal + "']");
			});
}

