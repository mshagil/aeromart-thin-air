/*
srcName - a name to assign for source list box.
desName - a name to assign for destination list box.
strSpnID - Span id that contains the control.
strControlId - Control id given for the control when declairing 
			   This is optional and if ignored, control id assumed as 'ls'.
strSeperator - Seperator string to identiry string values seperately.

if data set is not grouped,
	"group1" property should be assigned an array of following "Array of Arrays" format
	Array Position  Internal Array Element 0   Internal Array Element 1
	--------------  ------------------------   ------------------------
	0				1st Data description		1st Data value
	1				2nd Data description		2nd Data value
	2				3rd Data description		3rd Data value
	:
	:

if data set is grouped,
	"group1" property should be assigned an single dimension array containing group descriptions or 
	an array of following "Array of Arrays" format
	Array Position  Internal Array Element 0   Internal Array Element 1
	--------------  ------------------------   ------------------------
	0				1st Group description		1st Group value
	1				2nd Group description		2nd Group value
	2				3rd Group description		3rd Group value
	:
	:

	"list1" property should be assigned an array of following "Array of Arrays" format
	Array Position  Internal Array Element 0   					Internal Array Element 1					...
	--------------  ------------------------------------------  ------------------------------------------
	0				Array of 2 elements containing 1st group's	Array of 2 elements containing 1st group's	
					1st element's description and value			2nd element's description and value
	1				Array of 2 elements containing 2nd group's	Array of 2 elements containing 2nd group's	
					1st element's description and value			2nd element's description and value
	2				Array of 2 elements containing 3rd group's	Array of 2 elements containing 3rd group's	
					1st element's description and value			2nd element's description and value
	:
	:

Set width, height and display names using following properties
	width 
	height
	headingLeft 
	headingRight

Finally call the "drawListBox()" method to draw the list box.

User "selectedData(strSelectedList)" function to initialize selection with a new set of items.

*/

function Listbox(srcName, desName, strSpnID, strControlId, strSeperator) {
	this.listBox1 = srcName;
	this.listBox2 = desName;	
	this.spnID = strSpnID ;
	this.group1=new Array();;
	this.group2=new Array();;
	this.list1=new Array();;
	this.list2=new Array();;
	this.sortBox1 = true;
	this.sortBox2 = true;
	this.width = "100px";
	this.height = "200px";
	this.headingLeft = "";
	this.headingRight = "";	
	this.onClick = "";
	this.drawListBox = drawListBox;
//	this.addRemoveFromListbox = addRemoveFromListbox;
	this.removeAllFromListbox = removeAllFromListbox;
	this.selectedData = selectedData;
	this.getselectedData = getselectedData;
	this.getSelectedDataWithGroup = getSelectedDataWithGroup;
	this.getNotSelectedDataWithGroup = getNotSelectedDataWithGroup;
	this.clear = clear;
	this.disable = mulitiDisable;
	this.getMarkedData = getMarkedData;
	this.blnClientValidate = false;
	this.onClickEvents = "";

	this.ancestorArray=new Array();;
	var blnGrouping = true;
	this.moveOptions = moveOptions;
	this.move = move;
//	this.reDrawListBoxContent = reDrawListBoxContent;
	this.init = init;
	//add filter to the left dropdown
	this.filter = false;
	this.filterLeft = filterLeft;
	var arrData;
	var SEC = 1; //Source element code position
	var SED = 0; //Source element description position
	var SGC = 1; //Source group code position
	var SGD = 0; //Source group description position
	var GCC = 0; // Groupe code column
	var GDC = 1; // Group description column
	var GSC = 2; // Group status column: -1 = Not decided yet, 0 = Not Selected, 1 = Selected, 2 = Exists in both Selected and unselected groups
	var GEC = 3; // Group element array column
	var GWC = 4; // Group working column: 0 = Idle, 1 = Pending action
	var ECC = 5; // Element code column	
	var EDC = 0; //Element description column
	var ESC = 2; //Element status column: 0 = Not Selected, 1 = Selected
	var EAC = 3; //Element ancestor array column
	var EWC = 4; //Element working column
	var EVC = 1; // Element value column
	var APC = 0; //Ancestor parent code position
	var ACC = 1; //Ancestor child code position
	var strSeperator1 = ":";  //Used to seperate ancestors with the parent.
	var strSeperator2 = ","; //Used to seperately identify user selection.

	function init(arrGroup, arrElement, arrAncestor) {		
		//Initialize seperator with user's choice.
		if (strSeperator != null && strSeperator != "")
		{
			strSeperator2= strSeperator;
		}
		//Initializes the controls		
		buildDataStructure(arrGroup, arrElement, arrAncestor);	
	}
	
	function clear(){
		this.group1 = new Array();
		this.list1  = new Array();
		this.drawListBox();
	}

	

	function buildDataStructure(arrGroup, arrElement, arrAncestor){
		var intDataCount = 0;
		var groupArrDiamentions = 0;
		if (arrElement == null || arrElement.length == 0)
		{
			//No grouping Done
			blnGrouping = false; 
		}

		if (arrGroup.length > 0 && typeof(arrGroup[0]) != "object")
		{
			//Single diamentional group array
			groupArrDiamentions=1;
		} else {
			//Two diamensional group array
			groupArrDiamentions = 2;
		}

		arrData = new Array();

		//Fill Data Structure
		for (var i=0; i<arrGroup.length; i++)
		{
			if (groupArrDiamentions==1)
			{
				arrData[intDataCount] = new Array(arrGroup[i], arrGroup[i], 0, new Array(), 0);
			}else {
				arrData[intDataCount] = new Array(arrGroup[i][SGC], arrGroup[i][SGD], 0, new Array(), 0);
			}
			
			if (blnGrouping)
			{
				//Find and sort elements
				arrData[intDataCount][GEC] = sort(arrElement[i],SED);
			}
			intDataCount++;
			
		}
		//Sort groups
		arrData = sort(arrData, GDC);
		if (blnGrouping)
		{
			//Elements grouping exists, add ancestors
			for (var i=0; i<arrData.length; i++)
			{
				//Find ancestors of each element
				for (var j=0; j<arrData[i][GEC].length; j++)
				{
					arrData[i][GEC][j][EAC] = getAncestorArray(arrData[i][GEC][j][EVC], arrAncestor);
					arrData[i][GEC][j][ESC] = 0;
					arrData[i][GEC][j][EWC] = 0;
					arrData[i][GEC][j][ECC] = arrData[i][GCC] + strSeperator1 + arrData[i][GEC][j][EVC];					
					//checkAncestorAnomilities();
				}
			}
		}

	}

	function getAncestorArray(elementVal, arrAncestor){
		//Find ancestor array for the given element value
		var arrTemp = new Array();
		for (var i=0; i<arrAncestor.length; i++)
		{
			if (arrAncestor[i][APC] == elementVal)
			{
				//Find Ancestor array
				for (var m=ACC; m<arrAncestor[i].length; m++)
				{
					var blnFound = false;
					for (var j=0; j<arrData.length && !blnFound; j++)
					{
						for (var k=0; k < arrData[j][GEC].length && !blnFound; k++)
						{
							//Get ancestor position
							if (arrData[j][GEC][k][EVC] == arrAncestor[i][m])
							{
								blnFound = true;
								arrTemp[arrTemp.length] = j + "," + k;
							}
						}
					}
				}
				
				return arrTemp;
			}
		}
		return arrTemp;
	}


	function move(strDirection, strSelectedDataSet) {	
		//Moves selection between list boxes
		var blnSelectAncestors = true;
		var prevStrDirection = "";
		if (strDirection==">>>")
		{
			//Initialize to not selected mode
			for (var i=0; i<arrData.length; i++)
			{
				arrData[i][GSC] = 0;
				for (var k=0; k<arrData[i][GEC].length; k++)
				{
					arrData[i][GEC][k][ESC] = 0; //Element status
				}
			}
			//Reset the direction to make it forward
			prevStrDirection = strDirection;
			strDirection=">";
			//Don't consider ancestors
			blnSelectAncestors = false;

		}
		var blnDir = (strDirection == ">" || strDirection == ">>" ? 1: 0);
		arrSelected = strSelectedDataSet.split(strSeperator2);
		
		for (var i=0; i<arrData.length; i++)
		{
			//Search for selected elements
			if (blnGrouping)
			{
				//Group - Element mode.  Re-initialize the group selection				
				if (arrData[i][GEC].length>0)
				{
					//Check group has elements, then initialize group.
					arrData[i][GSC] = -1;
				}
			}
			if (strDirection == ">" || strDirection == "<")
			{
				//Only Selectd moved
				for (var j=0; j<arrSelected.length; j++)
				{
					if (blnGrouping)
					{
						//Group elements mode
						if (arrData[i][GCC] == arrSelected[j])
						{
							//Set the group as selected
							arrData[i][GSC] = blnDir;
							//Whole group selected.  Select all the elements
							for (var k=0; k<arrData[i][GEC].length; k++)
							{
								arrData[i][GEC][k][ESC] = blnDir; //Element status
								arrData[i][GEC][k][EWC] = 1; //Action pending on element
								if (blnDir)
								{
									if (blnSelectAncestors) {
										//Element selected. Check for ancestors
										setAncestorSelection(arrData[i][GEC][k][EAC]);
									}
								}
							}
						} else {
							for (var k=0; k<arrData[i][GEC].length; k++)
							{
								if ( (prevStrDirection == ">>>" && arrData[i][GEC][k][EVC] == arrSelected[j]) ||
										(prevStrDirection != ">>>" && arrData[i][GEC][k][ECC] == arrSelected[j]) )
								{
									//Element found
									arrData[i][GEC][k][ESC] = blnDir; //Element status
									arrData[i][GEC][k][EWC] = 1; //Action pending on element
									if (blnDir)
									{
										if (blnSelectAncestors) {
											//Element selected. Check for ancestors
											setAncestorSelection(arrData[i][GEC][k][EAC]);
										}
									}
									break; //Search for next selected element
								}
							}
						}
					} else {
						//Only elements available
						if (arrData[i][GCC] == arrSelected[j])
						{
							//Element found
							arrData[i][GSC] = blnDir; //Element status
							arrData[i][GWC] = 1; //Action pending on element
							break; //Search for next selected element
						}
					}
				}
			} else {
				//All moved 
				arrData[i][GSC] = blnDir; //Element status
				if (blnGrouping)
				{
					//Group elements mode
					for (var k=0; k<arrData[i][GEC].length; k++)
					{
						//Set status in all group elements
						arrData[i][GEC][k][ESC] = blnDir; //Element status
						arrData[i][GEC][k][EWC] = 1; //Action pending on element
					}
				}
			}
		}
		//Recheck group selection
		if (blnGrouping)
		{
			//Group elements mode
			for (var i=0; i<arrData.length; i++)
			{
				for (var k=0; k<arrData[i][GEC].length; k++)
				{
					if (arrData[i][GEC][k][ESC] == 1)
					{
						//Element selected
						arrData[i][GSC] = (arrData[i][GSC] == 0?2:1); //Group status
					} else {
						//Element not selected
						arrData[i][GSC] = (arrData[i][GSC] == 1?2:0); //Group status
					}
					if (arrData[i][GSC] == 2)
					{
						break;
					}
				}
			}
		}
		reDrawListBoxContent();
		
	}


	function setAncestorSelection(arrAncestor) {
		/*
			Make elements selected for the given ancestors.
			Currently ancestors posible only for Group-Element mode.
		*/
		for (var i=0; i<arrAncestor.length; i++)
		{
			var arrAnsPos = arrAncestor[i].split(",");
			//arrData[arrAnsPos[0]][GSC] = 1;
			//arrData[arrAnsPos[0]][GWC] = 1;
			arrData[arrAnsPos[0]][GEC][arrAnsPos[1]][ESC] = 1;
			arrData[arrAnsPos[0]][GEC][arrAnsPos[1]][EWC] = 1;
			setAncestorSelection(arrData[arrAnsPos[0]][GEC][arrAnsPos[1]][EAC]);
		}

	}


	function reDrawListBoxContent(){
		//Re-Draw list boxes with new selection in the array
		var unSelCount = -1;
		var selCount = -1;
		this.listBox1 = srcName;
		this.listBox2 = desName;

		for (var i=0; i<arrData.length; i++)
		{
			if (arrData[i][GSC] == 1 || arrData[i][GSC] == 2)
			{
				//Make group option selected 
				selCount ++;
				updateSelectListBox(this.listBox2, selCount, arrData[i][GDC], arrData[i][GCC] );			
				if (arrData[i][GSC] == 2)
				{
					//Make the group available in unselected list too.
					unSelCount ++;
					updateSelectListBox(this.listBox1, unSelCount, arrData[i][GDC], arrData[i][GCC] );				
				}
				//Make elements selected.
				if (blnGrouping)
				{
					//Group elements available
					for (var j=0; j<arrData[i][GEC].length; j++)
					{
						if (arrData[i][GEC][j][ESC]==1)
						{
							//Make element selected
							selCount ++;
							updateSelectListBox(this.listBox2, selCount, "- " + arrData[i][GEC][j][EDC], arrData[i][GEC][j][ECC]);
						} else {
							//Make element unselected
							unSelCount ++;
							updateSelectListBox(this.listBox1, unSelCount, "- " + arrData[i][GEC][j][EDC], arrData[i][GEC][j][ECC]);
						}
					}
				}
			} else {
				//Make group option not selected
				unSelCount ++;
				updateSelectListBox(this.listBox1, unSelCount, arrData[i][GDC], arrData[i][GCC] );
				//Make elements selected.
				if (blnGrouping)
				{
					//Group elements available
					for (var j=0; j<arrData[i][GEC].length; j++)
					{						
						//Make element unselected
						unSelCount ++;
						updateSelectListBox(this.listBox1, unSelCount, "- " + arrData[i][GEC][j][EDC], arrData[i][GEC][j][ECC]);						
					}
				}
			}
		}
		
		//Delete remeining select options

		unSelCount++;
		var listBox = document.getElementById(this.listBox1);
		var intLength = listBox.options.length;
		if(0 == unSelCount){
            ClearOptionsFast(this.listBox1);                
		}
		else{
			for (var i=unSelCount; i<intLength; i++)
			{
				listBox.remove(listBox.options.length-1);
			}
		}

		selCount++;
		var listBox = document.getElementById(this.listBox2);
		var intLength = listBox.options.length;
		if(0 == selCount){
            ClearOptionsFast(this.listBox2);                
		}
		else{
			for (var i=selCount; i<intLength; i++)
			{				
				listBox.remove(listBox.options.length-1);
			}	
		}
		
		if (document.getElementById("filt_"+this.listBox1) != undefined){
			document.getElementById("filt_"+this.listBox1).value = "";
		}		
	}
	
	function ClearOptionsFast(id){
        var selectObj = document.getElementById(id);
        var selectParentNode = selectObj.parentNode;
        var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
        selectParentNode.replaceChild(newSelectObj, selectObj);
        return newSelectObj;
    }
    
	function updateSelectListBox(listBoxId, selCount, optText, optValue ){
		//Adds or renames the options in a text box
		//listBox - List box object 
		//selCount - Currnet element number 
		//optText - Option text 
		//optValue - Option value

		var listBox = document.getElementById(listBoxId);
		var selOptCount = listBox.options.length -1;

		if (selCount > selOptCount) 
		{
			//Need to add option
			var elOpt = document.createElement('option');
			elOpt.text = optText;
			elOpt.value = optValue;
			try
			{
				listBox.add(elOpt, null);	
			}
			catch (ex)
			{
				listBox.add(elOpt);
			}
			
		} else {
			//Rename existing option
			listBox.options[selCount].text = optText;
			listBox.options[selCount].value = optValue;
		}

	}

	function filterLeft(txt){
		var unSelCount = -1;
		var grpArr = [];
		if (arrData.length>0 && (txt.length > 2 || txt.length == 0)){
			//Make elements selected.
			var listBox = document.getElementById(this.listBox1);
			listBox.innerHTML='';
			if (blnGrouping){
				for (var i=0; i<arrData.length; i++){
					//Make group option not selected
						unSelCount ++;
						updateSelectListBox(this.listBox1, unSelCount, arrData[i][GDC], arrData[i][GCC] );
						grpArr[i] = arrData[i][0];
						//Group elements available
						for (var j=0; j<arrData[i][GEC].length; j++){						
							//Make element unselected
							unSelCount ++;
							var tempOri = arrData[i][GEC][j][EDC].toUpperCase();
							if ( tempOri.indexOf(txt.toUpperCase()) > -1 && arrData[i][GEC][j][ESC]!=1 ){
								updateSelectListBox(this.listBox1, unSelCount, "- " + arrData[i][GEC][j][EDC], arrData[i][GEC][j][ECC]);
							}
						}
				}
				// Remove inwanted groups
				for(var x=0;x<grpArr.length;x++){
					var removeGr = true;
					for (var t=0;t<listBox.options.length;t++){
						if (listBox.options[t].value.indexOf(grpArr[x]+":") > -1){
							removeGr = false;
							break;
						}
					}
					if (removeGr){
						for (var ts=0;ts<listBox.options.length;ts++){
							if ((listBox.options[ts].value.indexOf(grpArr[x]) > -1) && (listBox[ts].text.toUpperCase().indexOf(txt.toUpperCase())==-1)){
								listBox.remove(ts);
								break;
							}
						}
					}
				}
			}else{
				for (var i=0; i<arrData.length; i++){
					//Make element unselected
					unSelCount ++;
					var tempOri = arrData[i][1].toUpperCase();
					if ( tempOri.indexOf(txt.toUpperCase()) > -1 && arrData[i][2]!=1 ){
						updateSelectListBox(this.listBox1, unSelCount, arrData[i][1], arrData[i][0]);
					}
				}
			}
		}
	}
	
	function in_array(needle, haystack)
	{
	    for(var key in haystack)
	    {
	        if(needle === haystack[key])
	        {
	            return true;
	        }
	    }

	    return false;
	}
	
	function  moveOptions(strDirection) {
		//Calls move function with the selected data list
		var listBox;
		var strSelectedDataSet = "";
		if (strDirection == ">>" || strDirection == ">")
		{
			listBox = document.getElementById(this.listBox1);
		} else {
			listBox = document.getElementById(this.listBox2);
		}
		for (var i=0; i<listBox.options.length; i++)
		{

			if (listBox.options[i].selected)
			{
				//Call onclick function
				if (this.blnClientValidate){
					if (this.onClick != ""){
						if (this.onClickEvents != ""){
							blnClientCall	= false
							if (this.onClickEvents  == ((strDirection == ">" || strDirection == ">>") ? 1:2)){
								blnClientCall	= true;			
							}
						}
						
						if (blnClientCall){
							blnClientReturn = eval(this.onClick)();
						}
					}		
				}
				//Make selceted list
				strSelectedDataSet += listBox.options[i].value + strSeperator2;
			}
		}
		move(strDirection, strSelectedDataSet);
	}




	function drawListBox() {
		//Initializes the ancestor array		
		init(this.group1, this.list1, this.ancestorArray);

		if (strControlId==null || strControlId=="")
		{
			strControlId= 'ls';
		}
		var filTextBox ="";
		if (this.filter){
			filTextBox = "<input type='text' id='filt_"+this.listBox1+"' style='float:right;width:70%' onkeyup='javascript:" + strControlId + ".filterLeft(this.value)'/>"
		}
		var strHTMLMDropDown  = "" ;
		var strHTMLMDropDown  = "" ;
		strHTMLMDropDown +="<TABLE class='fntDefault' border='0' id='Table6' cellSpacing='0' cellPadding='0' width='300px' border='0'>";
		strHTMLMDropDown +="<tr>";
		strHTMLMDropDown +="	<td><font><b>" + this.headingLeft + "<\/b><\/font> " +filTextBox+ "<\/td>" ;
		strHTMLMDropDown +="	<td><font><b>&nbsp;<\/b><\/font><\/td>" ;
		strHTMLMDropDown +="	<td><font><b>" + this.headingRight + "<\/b><\/font><\/td><\/tr>" ;
		strHTMLMDropDown +="<TR>";
		strHTMLMDropDown +="	<TD vAlign='middle' align='center'>";
		strHTMLMDropDown +="		<SELECT id='" + this.listBox1 + "' class='ListBox' style='Width:" + this.width + ";height:" + this.height + "' multiple>";
		strHTMLMDropDown +="		</SELECT></TD>";
		strHTMLMDropDown +="	<TD style='HEIGHT: 55px' align='center' width='45'>";
		strHTMLMDropDown +="		<TABLE id='listboxTable' width='80px' cellSpacing='0' cellPadding='0' align='center' border='0'>";
		strHTMLMDropDown +="			<TR>";
		strHTMLMDropDown +="				<TD align='center'><INPUT style='WIDTH: 45px; HEIGHT: 20px' id='" + this.spnID + "_1' ";
		strHTMLMDropDown +="				onclick=\"javascript:" + strControlId + ".moveOptions('>>');\"";
		strHTMLMDropDown +="				type='button' value='>>' class='Button'></TD>";
		strHTMLMDropDown +="			</TR>";
		strHTMLMDropDown +="			<TR>";
		strHTMLMDropDown +="				<TD align='center'><INPUT style='WIDTH: 45px; HEIGHT: 20px' id='" + this.spnID + "_2' ";
		strHTMLMDropDown +="				onclick=\"javascript:" + strControlId + ".moveOptions('>');\"";
		strHTMLMDropDown +="				type='button' value='>' class='Button'></TD>";
		strHTMLMDropDown +="			</TR>";
		strHTMLMDropDown +="			<TR>";
		strHTMLMDropDown +="				<TD align='center'><INPUT style='WIDTH: 45px; HEIGHT: 20px' id='" + this.spnID + "_3' ";
		strHTMLMDropDown +="				onclick=\"javascript:" + strControlId + ".moveOptions('<');\"";
		strHTMLMDropDown +="				type='button' value='<' class='Button'></TD>";
		strHTMLMDropDown +="			</TR>";
		strHTMLMDropDown +="			<TR>";
		strHTMLMDropDown +="				<TD align='center'><INPUT style='WIDTH: 45px; HEIGHT: 20px' id='" + this.spnID + "_4' ";
		strHTMLMDropDown +="				onclick=\"javascript:" + strControlId + ".moveOptions('<<');\"";
		strHTMLMDropDown +="				type='button' value='<<' class='Button'></TD>";
		strHTMLMDropDown +="			</TR>";
		strHTMLMDropDown +="		</TABLE>";
		strHTMLMDropDown +="	</TD>";
		strHTMLMDropDown +="	<TD vAlign='middle' align='center'>";
		strHTMLMDropDown +="		<SELECT id='" + this.listBox2 + "' Class='ListBox' style='Width:" + this.width + ";height:" + this.height + "' multiple></SELECT></TD>";
		strHTMLMDropDown +="</TR>";
		strHTMLMDropDown +="</TABLE>";
		
		DivWrite(this.spnID, strHTMLMDropDown);
		
		reDrawListBoxContent();
	}


	function removeAllFromListbox(strSrcList, strDestList, srcID)
	{
		//This function is dipricated
		selectedData("");
	}


	function selectedData(strSelected){
	
		var objControl = document.getElementById(this.listBox1);
		//var arrSelected = strSelected.split(",");
		var intLength;

		move(">>>", strSelected);

	}
/*	
	function getselectedData(){
		var objControl = document.getElementById(this.listBox2);
		var intLength = objControl.length;
		var strReturn = "";
		for (var x = 0 ; x < intLength; x++){
			if (strReturn != ""){
				strReturn += "," ;
			}
			strReturn += objControl.options[x].value ;
		}
		return strReturn;
	}
*/

	function getselectedData(){		
		var strReturn = "";
		if (blnGrouping)
		{
			//Group - Element mode
			if(arrData != undefined){
				for (var p = 0; p < arrData.length ; p++)
				{
					if (arrData[p][GSC] == 1 || arrData[p][GSC] == 2)
					{
						for (var q = 0; q < arrData[p][GEC].length ; q++ )
						{
							if (arrData[p][GEC][q][ESC]==1){							
								if (strReturn != ""){
									strReturn += strSeperator2 ;
								}
								strReturn += arrData[p][GEC][q][EVC] ;
							}
						}
					}
				}
			}
		} else {
			//Only Element mode
			if(arrData != undefined){
				for (var p = 0; p < arrData.length ; p++)
				{
					if (arrData[p][GSC] == 1 )
					{
						if (strReturn != ""){
							strReturn += strSeperator2 ;
						}
						strReturn += arrData[p][GCC] ;
					}
				}
			}
		}
		return strReturn;
	}
	
	function mulitiDisable(blnStatus){
		document.getElementById(this.listBox1).disabled = blnStatus
		document.getElementById(this.listBox2).disabled = blnStatus		
		document.getElementById(this.spnID + '_1').disabled = blnStatus				
		document.getElementById(this.spnID + '_2').disabled = blnStatus				
		document.getElementById(this.spnID + '_3').disabled = blnStatus				
		document.getElementById(this.spnID + '_4').disabled = blnStatus										
	}

	function getSelectedDataWithGroup(){
		var strReturn = "";
		for (var p = 0; p < arrData.length ; p++)
		{
			if (arrData[p][GSC] == 1 || arrData[p][GSC] == 2)
			{
				if (strReturn != "") strReturn += "|";
				strReturn += arrData[p][GCC];
				var intCounter=0;
				for (var q = 0; q < arrData[p][GEC].length ; q++ )
				{
					if (arrData[p][GEC][q][ESC]==0)
						continue;
					
					if (intCounter == 0 )
						strReturn += ":" ;
					else
						strReturn += strSeperator2 ;
					if (arrData[p][GEC][q][ESC]==1)
					{
						strReturn += arrData[p][GEC][q][EVC];
						intCounter++;
					}						
				}
			}			
		}
		return strReturn;
	}

	function getNotSelectedDataWithGroup(){		
		var strReturn = "";
		for (var p = 0; p < arrData.length ; p++)
		{
			if (arrData[p][GSC] == 0 || arrData[p][GSC] == 2)
			{
				if (strReturn != "") strReturn += "|";
				strReturn += arrData[p][GCC];
				var intCounter=0;
				for (var q = 0; q < arrData[p][GEC].length ; q++ )
				{
					if (arrData[p][GEC][q][ESC]==1)
						continue;
					if (intCounter == 0 )
						strReturn += ":" ;
					else
						strReturn += strSeperator2 ;
					if (arrData[p][GEC][q][ESC]==0)
					{
						strReturn += arrData[p][GEC][q][EVC];
						intCounter++;
					}						
				}
			}			
		}
		return strReturn;
	}

	
	function getMarkedData(){
		var objControl = document.getElementById(this.listBox1);
		var intLength = objControl.length;
		var strReturn = "";
		for (var x = 0 ; x < intLength; x++){
			if (objControl.options[x].selected){
				if (strReturn != ""){
					strReturn += strSeperator2 ;
				}
				strReturn += objControl.options[x].value ;
			}
		}
		return strReturn;
	}
}