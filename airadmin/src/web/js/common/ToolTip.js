	/*
	*********************************************************
		Description		: Common Routings to display the Tool tips in a balloon
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 24th November 2006
	*********************************************************	
	*/

	var blnTT = false;
	var intTTHideDelay = 900;
	var intTTShowDelay = 600;
	var strTTImagePath = "../images/"
	var objTTHider = null;
	var objTTShow  = null;
		
	// show the tooltip
	function showTT(strMsg, strTop, strLeft, objEvent, intPadX, intPadY, intWidth){
		// strTop = TOP / LEFT / RIGHT / BOTTOM
		// strLeft = TOP / BOTTOM / LEFT / RIGHT / CENTER 
		
		strTop = strTop.toUpperCase();
		strLeft = strLeft.toUpperCase();
		
		if (objTTHider != null){
			clearTimeout(objTTHider);
		}
		
		if (!blnTT){
			var strHTMLText = "";
			var objC = getFieldByID("spnTT");
			var x = 0 ;
			var y = 0 ; 
			if (objEvent != null){
				if (browser.isIE) {
					x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
					y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
				}else{
					if ((navigator.userAgent.indexOf("Safari") != -1) ||
						(navigator.userAgent.indexOf("Opera") != -1)){
						x = objEvent.clientX;
						y = objEvent.clientY;
					}else{
						x = objEvent.clientX + window.scrollX;
						y = objEvent.clientY + window.scrollY;
					}
				}	
			}
			objC.style.top	= (y + intPadY) + "px"; 
			objC.style.left = (x + intPadX) + "px"; 
			
			//objC.style.top	= objEvent.clientY-60-20 + "px" //"500px;" //(y + intPadY) + "px"; 
			//objC.style.left = objEvent.clientX-60-20 + "px" //"500px;" //(x + intPadX) + "px"; 
			
			intTblWidth		= 234
			var intMWidth	= 200;
			var strImgPath	= strTTImagePath
			
			var intTHeight	= 17;
			var intTLWidth	= 17;
			var intTRWidth	= 17;
			var strTPointer	= ">";
			var strTImg		= "BL02_no_cache.gif";
			
			var intBHeight	= 17;
			var strBPointer	= ">";
			var strBImg		= "BL08_no_cache.gif";
			
			var strLTImg	= "BL01_no_cache.gif"
			var strLMImg	= "BL04_no_cache.gif"
			var strLBImg	= "BL07_no_cache.gif"
			var strLPointer	= ">";
			
			var strRTImg	= "BL03_no_cache.gif"
			var strRMImg	= "BL06_no_cache.gif"
			var strRBImg	= "BL09_no_cache.gif"
			var strRPointer	= ">";
			
			switch (strTop){
				case "TOP" : 
					intTHeight	= 31; 
					strTImg		= "BL14_no_cache.gif"
					strTPointer = " align='" + strLeft + "'><img src='" + strImgPath + "/BL13_no_cache.gif'>";
					break;
					
				case "BOTTOM" : 
					intBHeight	= 31; 
					strBImg		= "BL15_no_cache.gif"
					strBPointer = " align='" + strLeft + "'><img src='" + strImgPath + "/BL11_no_cache.gif'>";
					break;
					
				case "LEFT" :
					intTLWidth	= 29
					strLTImg	= "BL16_no_cache.gif";
					strLMImg	= "BL18_no_cache.gif";
					strLBImg	= "BL17_no_cache.gif";
					strLPointer = " valign='" + strLeft + "'><img src='" + strImgPath + "/BL12_no_cache.gif'>";
					break;
					
				case "RIGHT" :
					intTRWidth	= 29
					strRTImg	= "BL19_no_cache.gif";
					strRMImg	= "BL21_no_cache.gif";
					strRBImg	= "BL20_no_cache.gif";
					strRPointer = " valign='" + strLeft + "'><img src='" + strImgPath + "/BL10_no_cache.gif'>";
					break;
			}
			
			intTblWidth = intWidth + intTLWidth + intTRWidth
			
			strHTMLText = "<table width='" + intTblWidth + "px' border='0' cellpadding='0' cellspacing='0'>";
			strHTMLText += "	<tr>";
			strHTMLText += "		<td style='width:" + intTLWidth +"px;height:" + intTHeight + "px;background-image:url(" + strImgPath + "/" + strLTImg +");background-position: bottom;background-repeat: no-repeat;'><\/td>";
			strHTMLText += "		<td style='width:" + intWidth + "px;background-image:url(" + strImgPath + "/" + strTImg + ");background-position: bottom;background-repeat: repeat;' " + strTPointer + "<\/td>";
			strHTMLText += "		<td style='width:" + intTRWidth + "px;background-image:url(" + strImgPath + "/" + strRTImg + ");background-position: bottom;background-repeat: no-repeat;'><\/td>";
			strHTMLText += "	<\/tr>";
			strHTMLText += "	<tr>";
			strHTMLText += "		<td style='height:17px;background-image:url(" + strImgPath + "/" + strLMImg +");'" + strLPointer + "<\/td>";
			strHTMLText += "		<td style='background-image:url(" + strImgPath + "/BL05_no_cache.gif);'><span id='spnTTContents'>" + strMsg + "<\/span><\/td>";
			strHTMLText += "		<td style='background-image:url(" + strImgPath + "/" + strRMImg + ");'" + strRPointer + "<\/td>";
			strHTMLText += "	<\/tr>";
			strHTMLText += "	<tr>";
			strHTMLText += "		<td style='height:" + intBHeight + "px;background-image:url(" + strImgPath + "/" + strLBImg +");background-position: top;background-repeat: no-repeat;'><\/td>";
			strHTMLText += "		<td style='background-image:url(" + strImgPath + "/" + strBImg + ");background-position: bottom;background-repeat: repeat;' " + strBPointer + "<\/td>";
			strHTMLText += "		<td style='background-image:url(" + strImgPath + "/" + strRBImg + ");background-position: top;background-repeat: no-repeat;'><\/td>";
			strHTMLText += "	<\/tr>";
			strHTMLText += "<\/table>";
			
			setDisplay("spnTT", false);
			DivWrite("spnTT", strHTMLText);
			//setDisplay("spnTT", true);
			objTTShow = setTimeout("delayShowTT()", intTTShowDelay);
		}
	}

	// Refersh the tooltip contents
	function refreshTT(strMessage){
		if (blnTT){
			DivWrite("spnTTContents", strMessage);
		}
	}

	function getTTStatus(){
		return blnTT;
	}

	function hideTT(){
		blnTT = false;
		clearTimeout(objTTShow);
		objTTHider = setTimeout("delayHideTT()", intTTHideDelay);
	}

	function delayHideTT(){
		clearTimeout(objTTHider);
		setDisplay("spnTT", false);
	}
	
	function delayShowTT(){
		clearTimeout(objTTShow);
		setDisplay("spnTT", true);
		blnTT = true;
	}	
	/* --------------------------------------------------------- end of Page --------------------------------------------------------- */