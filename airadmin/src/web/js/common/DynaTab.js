/*
	*********************************************************
		Description		: Dynamic Tab generation 
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 28th January, 2006
	*********************************************************	
	*/

	function tabMenu(strSpanName){
		// properties
		this.id					= strSpanName;
		this.width				= "900";
		this.height				= "400";
		this.maxTabs			= 5;
		this.tabWidth			= 143;
		this.closeTooltip		= "Close this tab";
		this.top				= "25";
		this.left				= "50";
		this.closeOnClick		= "";
		this.tabOnClick			= "";
		this.maxTabMessage		= "Maximum allowed tabs to open " + this.maxTabs + ".";
		this.fontBold			= false;
		this.selectedTabBold	= false;
		this.closeOnValidate	= false;
		this.closeValidateMsg	= "Changes will be lost! Do you wish to Continue?";
		this.onUnload			= "";
		this.focusTab			= "";
		
		// methods 
		this.buildTabMenu		= buildTabMenu;			// build the menu Layout
		this.tabClick			= tabClick;				// tab Click;
		this.tabLoad			= tabLoad;				// load the tab with a page
		this.tabLock			= tabLock;				// Unlock / Lock the Tab
		this.tabFunCall			= tabFunCall			// Function Call 
		this.tabSetValue		= tabSetValue			// Set Values to a tab
		this.tabGetValue		= tabGetValue			// Get Values from a tab
		this.tabRemove			= tabRemove				// Remove tab
		this.tabSetTabInfo		= tabSetTabInfo			// Set Tab Information 
		this.tabPageEdited		= tabPageEdited			// Set Page Edited
		this.tabGetPageEidted	= tabGetPageEidted		// Get Page Edited Status
		this.tabIsAvailable		= tabIsAvailable		// check tab available
		
		//-------------------------------------------------------------------------------------------
		// Internal Use
		//-------------------------------------------------------------------------------------------
		this._arrTMData			= new Array();			// data Container
		this._menuTab			= false;
		this._menuTimer			= null;
		this._lastClickID		= "";
		
		this._arrTabNeedtoReload = new Array();
		this._arrTabNeedtoReload[1] = "";
		
		window._imagePath		= "../../images/" ;
		window._imgTM001		= "DT001_no_cache.jpg";
		window._imgTM002		= "DT002_no_cache.jpg";
		window._imgTM003		= "DT003_no_cache.jpg";
		window._imgTM004		= "DT004_no_cache.gif";
		window._imgTM005		= "DT005_no_cache.jpg";
		window._imgTM006		= "DT006_no_cache.gif";
		window._imgTM007		= "DT007_no_cache.gif";
		window._imgTM008		= "DT008_no_cache.gif";
		window._imgTM009		= "DT009_no_cache.jpg";
		window._imgTM010		= "DT010_no_cache.gif";
		window._imgTM011		= "DT011_no_cache.jpg";
		window._imgTM012		= "DT012_no_cache.gif";
		window._imgTM013		= "DT013_no_cache.gif";
		window._imgTM014		= "DT014_no_cache.gif";
		window._imgTM015		= "DT015_no_cache.gif";
		window._imgTM016		= "DT016_no_cache.gif";
		
		if (!window._arrTM) window._arrTM = new Array();
		window._arrTM[this.id] = this;
		window._arrTM[window._arrTM.length] = this;
		window._TMLayer		= "spnTabMenu";
		window._TMFrame		= "frmTabmenu";
		window._TMTabSpan	= "spnTabButtons";
	}

	function buildTabMenu(){
		document.writeln('<span id="' + window._TMLayer  + '" style="visibility:hidden;">�<\/span>');
		var objC = document.getElementById(window._TMLayer);
		objC.arrTM			= new Array();
		var strHTMLText		= "";
		var strHTMLFrame	= "";
		var objTM ;
		var intMiddleWidth = 0 ;
		for (var i = 0 ; i < window._arrTM.length ; i++){
			objC.arrTM[i]	= window._arrTM[i];
			objTM			= objC.arrTM[i];
			
			intMiddleWidth = objTM.width - (44);
			objTM._arrTMData.length = 0 ; 
			strHTMLFrame	= "";
			strHTMLText		= "";
			
			// Fill the array with the frame names
			for (var x = 0 ; x < objTM.maxTabs ; x++){
				var strFrameID = window._TMFrame + "_" + objTM.id + "_" + x ;
				objTM._arrTMData[x] = new Array();
				objTM._arrTMData[x][0] = strFrameID;
				objTM._arrTMData[x][1] = "";		// Tab Text
				objTM._arrTMData[x][2] = "";		// Frame Source
				objTM._arrTMData[x][3] = "";		// Function ID
				objTM._arrTMData[x][4] = "";		// Tool tip
				objTM._arrTMData[x][5] = "";		// Lock status
				objTM._arrTMData[x][6] = "";		// Tab values
				objTM._arrTMData[x][7] = "";		// Page Edited Status
				strHTMLFrame += "<layer><iframe src='about:blank' id='" + strFrameID + "' name='" + strFrameID + "' height='" + (objTM.height + 120) + "' width='" + (objTM.width - 10) + "' frameborder='0' scrolling='" + objTM.scrolling + "' style='position:absolute;style:visibility:hidden;'><\/iframe></layer>";
			}
			
			strHTMLText += "<table width='" + objTM.width + "' border='0' cellpadding='0' cellspacing='0' align='center'>"; 
			strHTMLText += "	<tr>"; 
			strHTMLText += "		<td colspan='2' rowspan='2' style='background-image: url(" + window._imagePath + window._imgTM008 + ");'>"; 
			strHTMLText += "			<span id='" + window._TMTabSpan + "_" + objTM.id + "'";  
			strHTMLText += "		</td>"; 
			strHTMLText += "		<td style='height:21;width:22px'></td>"; 
			strHTMLText += "	</tr>"; 
			strHTMLText += "	<tr>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM007 + ");height:21;' align='left' valign='top'>"; 
			strHTMLText += "		</td>"; 
			strHTMLText += "	</tr>"; 
			strHTMLText += "	<tr>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM001 + ");width:22px;height:" + objTM.height + "px;'></td>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM002 + ");width:" + intMiddleWidth + "px;'></td>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM003 + ");'></td>"; 
			strHTMLText += "	</tr>"; 
			strHTMLText += "	<tr>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM004 + ");width:22px;height:20px;'></td>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM005 + ");'></td>"; 
			strHTMLText += "		<td style='background-image: url(" + window._imagePath + window._imgTM006 + ");'></td>"; 
			strHTMLText += "	</tr>"; 
			strHTMLText += "</table>"; 
			strHTMLText += strHTMLFrame;
			
			objTM._menuTab		= true;
			DivWrite(objTM.id, strHTMLText);
			setVisible(objTM.id, false);
		}
	}

	function tabClick(strID){
		var objTM		= this
		var blnLocked	= false;
		setVisible(objTM.id, true);
		for (var i = 0 ; i < objTM._arrTMData.length ; i++){
			if (objTM._arrTMData[i][3] == strID){
				if (objTM._arrTMData[i][5] == true){
					blnLocked = true;
					break;
				}
			}
		}
		
		if (blnLocked){return false;}
		if (objTM.tabOnClick == ""){
			objTM._lastClickID = strID
			inTabClick(objTM);
		}else{
			if (eval(objTM.tabOnClick + "('" + strID + "')")){
				objTM._lastClickID = strID
				inTabClick(objTM);
			}
		}
		
		function inTabClick(objTM){
			var strHTMLText = "";
			var strTabsText		= "";
			var strLineText		= "";
			var intWidth		= 0;
			var blnFirst		= true;
			var strTabClick		= "";
			var strRemoveClick	= "";
			var objIF			= null;
			var strTT			= "";
			var blnSingleTab	= true;
			var intCount		= 0 ; 
			var strImage		= "";
			var strCloseTT		= "";
			var strPointer		= "";
			var strFontBoldS	= "";
			var strFontBoldE	= "";
			var strTFontBoldS	= "";
			var strTFontBoldE	= "";
			
			for (var i = 0 ; i < objTM._arrTMData.length ; i++){
				if (objTM._arrTMData[i][3] != ""){
					intCount++;
				}
			}
			if (intCount > 1){blnSingleTab = false;}
			
			
			if (objTM.fontBold){strFontBoldS = "<b>";strFontBoldE = "</b>";}
			if (objTM.selectedTabBold){strTFontBoldS = "<b>"; strTFontBoldE = "</b>";}
			for (var i = 0 ; i < objTM._arrTMData.length ; i++){
				objIF = document.getElementById(objTM._arrTMData[i][0]);
				objIF.style.visibility	= "hidden";
				objIF.style.width		= "0px";
				objIF.style.height		= "0px";
				objIF.style.top			= -0;
				objIF.style.left		= -0;
				if (objTM._arrTMData[i][4] != ""){
					strTT = " title='" + objTM._arrTMData[i][4] + "' ";
				}
				
				if (objTM._arrTMData[i][3] != ""){
					strTabClick = " onClick='tabUClick(" + '"' + objTM.id + '"' + "," + '"' + objTM._arrTMData[i][3] + '"' + ")'; "
					strRemoveClick = " onClick='tabURemoveClick(" + '"' + objTM.id + '"' + "," + '"' + objTM._arrTMData[i][3] + '"' + ")'; "
					if (objTM._arrTMData[i][3] == strID){
					
						if (!blnSingleTab){
							strImage		= window._imgTM012;
							strCloseTT		= " title='" + objTM.closeTooltip + "' ";
							strPointer		= " cursor:pointer; ";
						}else{
							strImage		= window._imgTM016;
							strTabClick		= "";
							strRemoveClick	= "";
							strCloseTT		= "";
							strPointer		= "";
						}
						strTabsText += "					<td " + strTabClick + " style='background-image: url(" + window._imagePath + window._imgTM010 + "); " + strPointer + " width:22px;height:21;'></td>";
						strTabsText += "					<td " + strTabClick + " style='background-image: url(" + window._imagePath + window._imgTM011 + "); " + strPointer + " width:" + objTM.tabWidth + "px;height:21;' align='center'><font " + strTT + ">" + strTFontBoldS + objTM._arrTMData[i][1] + strTFontBoldE + "</font></td>";
						strTabsText += "					<td " + strRemoveClick + " style='background-image: url(" + window._imagePath + strImage + "); " + strPointer + " width:22px;height:21;' " + strCloseTT + "></td>";
						
						if (blnFirst){
							strLineText += "					<td style='background-image: url(" + window._imagePath +  window._imgTM001 + ");width:22px;height:21;'></td>";
						}else{
							strLineText += "					<td style='background-image: url(" + window._imagePath +  window._imgTM002 + ");width:22px;height:21;'></td>";
						}
						strLineText += "					<td style='background-image: url(" + window._imagePath +  window._imgTM002 + ");width:" + objTM.tabWidth + "px;height:21;'></td>";
						strLineText += "					<td style='background-image: url(" + window._imagePath +  window._imgTM002 + ");width:22px;height:21;'></td>";
						objIF.style.visibility	= "visible";
						objIF.style.zIndex		= 0;
						objIF.style.top			= objTM.top;
						objIF.style.width		= Number(objTM.width) - 20; // "500px";
						objIF.style.height		= Number(objTM.height) + 30;
						if (objTM.left == ""){
							objIF.style.left		= ((window.screen.width - Number(objTM.width)) / 2) + 5		
						}else{
							objIF.style.left		= objTM.left;
						}	
					}else{
						strTabsText += "					<td " + strTabClick + " style='background-image: url(" + window._imagePath +  window._imgTM013 + "); cursor:pointer; width:22px;height:21;'></td>";
						strTabsText += "					<td " + strTabClick + " style='background-image: url(" + window._imagePath +  window._imgTM015 + "); cursor:pointer; width:" + objTM.tabWidth + "px;height:21;' align='center'><font " + strTT + ">" + strFontBoldS + objTM._arrTMData[i][1] + strFontBoldE  + "</font></td>";
						strTabsText += "					<td " + strRemoveClick + " style='background-image: url(" + window._imagePath +  window._imgTM014 + "); cursor:pointer; width:22px;height:21;' title='" + objTM.closeTooltip + "'></td>";
						
						if (blnFirst){
							strLineText += "					<td style='background-image: url(" + window._imagePath +  window._imgTM009 + ");width:22px;height:21;'></td>";
						}else{
							strLineText += "					<td style='width:22px;height:21;'></td>";
						}
						strLineText += "					<td style='width:" + objTM.tabWidth + "px;height:21;'></td>";
						strLineText += "					<td style='width:22px;height:21;'></td>";
						
					}
				
					intWidth = intWidth + (objTM.tabWidth + 44);
					blnFirst = false;
				}
			}
			
			
			strHTMLText += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='left'>";
			strHTMLText += "	<tr>";
			strHTMLText += "		<td style='height:21'>";
			strHTMLText += "			<table width='" + intWidth + "' border='0' cellpadding='0' cellspacing='0' align='left' ID='Table1'>";
			strHTMLText += "				<tr>";
			strHTMLText += strTabsText;
			strHTMLText += "				</tr>";
			strHTMLText += "			</table>";
			strHTMLText += "		</td>";
			strHTMLText += "	</tr>";
			strHTMLText += "	<tr>";
			strHTMLText += "		<td>";
			strHTMLText += "			<table width='" + +intWidth + "' border='0' cellpadding='0' cellspacing='0' align='left' ID='Table2'>";
			strHTMLText += "				<tr>";
			strHTMLText += strLineText;
			strHTMLText += "				</tr>";
			strHTMLText += "			</table>";
			strHTMLText += "		</td>";
			strHTMLText += "	</tr>";
			strHTMLText += "</table>";
			
			objTM.focusTab = strID
			DivWrite(window._TMTabSpan + "_" + objTM.id, strHTMLText)
		}
		top[2].HidePageMessage();
	}

	function tabUClick(strID, strFID){
		var objC = document.getElementById(window._TMLayer);
		for (var i = 0 ; i < objC.arrTM.length ; i++){
			var objTM = objC.arrTM[i];
			if (objTM.id == strID){
				objTM.tabClick(strFID);
				break;
			}
		}
	}

	function tabURemoveClick(strID, strFID){
		var objC = document.getElementById(window._TMLayer);
		var blnLocked	= false;
		var strReturn 	= false;
		for (var i = 0 ; i < objC.arrTM.length ; i++){
			var objTM = objC.arrTM[i];
			if (objTM.id == strID){
				if (objTM._arrTMData[i][5] == true){
					blnLocked = true;
					break;
				}
				if (objTM.closeOnClick == ""){
					if (objTM.closeOnValidate){
						if (confirm(objTM.closeValidateMsg)){
							strReturn = inTabURemoveClick(objTM);
						}
					}else{
						strReturn = inTabURemoveClick(objTM);
					}
				}else{
					if (eval(objTM.closeOnClick + "('" + strFID + "')")){
						if (objTM.closeOnValidate){
							if (confirm(objTM.closeValidateMsg)){
								strReturn = inTabURemoveClick(objTM);	
							}
						}else{
							strReturn = inTabURemoveClick(objTM);
						}
					}
				}
				
				break;
			}
		}
		if (blnLocked){
			return false;
		}else{
			return strReturn				
		}
		
		// remove tabs
		function inTabURemoveClick(objTM){
			var strReturn   = false;
			for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
				if (objTM._arrTMData[x][3] == strFID){
					if (removeValidate(objTM, objTM._arrTMData[x][7])){
						objTM._arrTMData[x][1] = "";
						objTM._arrTMData[x][2] = "";
						objTM._arrTMData[x][3] = "";
						objTM._arrTMData[x][4] = "";
						objTM._arrTMData[x][5] = "";
						objTM._arrTMData[x][6] = "";
						objTM._arrTMData[x][7] = "";
						strReturn = true;
						
						frames[objTM._arrTMData[x][0]].location.replace("about:blank");
						// re-arranging the array informaitons.
						var intRowNo = -1
						for (var y = 0 ;  y < objTM._arrTMData.length ; y++){
							if (objTM._arrTMData[y][3] == ""){
								intRowNo = -1	
								for (var m = (y+1) ; m < objTM._arrTMData.length ; m++){
									if (objTM._arrTMData[m][3] != ""){
										intRowNo = m;
										break;
									}
								}
								
								if (intRowNo != -1){
									var strFrameName	   = objTM._arrTMData[y][0];
									objTM._arrTMData[y][0] = objTM._arrTMData[intRowNo][0];
									objTM._arrTMData[y][1] = objTM._arrTMData[intRowNo][1];
									objTM._arrTMData[y][2] = objTM._arrTMData[intRowNo][2];
									objTM._arrTMData[y][3] = objTM._arrTMData[intRowNo][3];
									objTM._arrTMData[y][4] = objTM._arrTMData[intRowNo][4];
									objTM._arrTMData[y][5] = objTM._arrTMData[intRowNo][5];
									objTM._arrTMData[y][6] = objTM._arrTMData[intRowNo][6];
									objTM._arrTMData[y][7] = objTM._arrTMData[intRowNo][7];
									
									objTM._arrTMData[intRowNo][0] = strFrameName
									objTM._arrTMData[intRowNo][1] = "";
									objTM._arrTMData[intRowNo][2] = "";
									objTM._arrTMData[intRowNo][3] = "";
									objTM._arrTMData[intRowNo][4] = "";
									objTM._arrTMData[intRowNo][5] = "";
									objTM._arrTMData[intRowNo][6] = "";
									objTM._arrTMData[intRowNo][7] = "";
								}
							}
						}
					}
					break;
				}
			}
			
			if (strFID == objTM._lastClickID){
				strFID = "";
				var intCount = 0 ; 
				for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
					if (objTM._arrTMData[x][3] != ""){
						strFID = objTM._arrTMData[x][3];
						intCount++;
					}else{
						document.getElementById(objTM._arrTMData[i][0]).style.visibility	= "hidden";
					}
				}
				if (intCount > 0){
					objTM.tabClick(strFID);
				}else{
					if (objTM.onUnload != ""){
						eval(objTM.onUnload + "()");
					}
					setVisible(objTM.id, false);
				}
			}else{
				strFID = objTM._lastClickID;
				objTM.tabClick(strFID);
			}
			
			return strReturn
		}
		
		// Remove Validate
		function removeValidate(objTM, blnStatus){
			var strReturn = true
			if (blnStatus){
				if (!confirm(objTM.closeValidateMsg)){
					strReturn = false;
				}
			}
			return strReturn ; 
		}
	}

	function tabInitalLoad(strURL, strTabText, strFID, strID, strToolTip){
		var objC = document.getElementById(window._TMLayer);
		for (var i = 0 ; i < objC.arrTM.length ; i++){
			var objTM = objC.arrTM[i];
			if (objTM.id == strID){
				if (objTM._menuTab){
					clearInterval(objTM._menuTimer);
					var blnFound = false;
					for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
						if (objTM._arrTMData[x][3] == strFID){
							blnFound = true;	
							break;
						}
					}
					
					if (!blnFound){
						var blnNewTab = true;
						for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
							if (objTM._arrTMData[x][3] == ""){
								objTM._arrTMData[x][1] = strTabText;
								objTM._arrTMData[x][2] = strURL;
								objTM._arrTMData[x][3] = strFID;
								objTM._arrTMData[x][4] = strToolTip;
								objTM._arrTMData[x][5] = "";
								objTM._arrTMData[x][7] = "";
								frames[objTM._arrTMData[x][0]].location.replace(strURL);
								tabUClick(objTM.id, strFID);
								blnNewTab = false;
								break;
							}
						}
						if (blnNewTab){
							alert(objTM.maxTabMessage);
						}
					}else{
						tabUClick(objTM.id, strFID);
					}
				}
				break;
			}
		}
	}

	function tabLoad(strURL, strTabText, strFID, strToolTip){
		var objTM = this
		var strReturn = true;
		if (objTM._menuTab){
			clearInterval(objTM._menuTimer);
			var blnFound = false;
			for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
				if (objTM._arrTMData[x][3] == strFID){
					blnFound = true;	
					break;
				}
			}
			
			if (!blnFound){
				var blnNewTab = true;
				for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
					if (objTM._arrTMData[x][3] == ""){
						objTM._arrTMData[x][1] = strTabText;
						objTM._arrTMData[x][2] = strURL;
						objTM._arrTMData[x][3] = strFID;
						objTM._arrTMData[x][4] = strToolTip;
						objTM._arrTMData[x][5] = "";
						objTM._arrTMData[x][7] = "";
						frames[objTM._arrTMData[x][0]].location.replace(strURL);
						tabUClick(objTM.id, strFID);
						blnNewTab = false;
						break;
					}
				}
				if (blnNewTab){
					alert(objTM.maxTabMessage);
					strReturn = false;
				}
			}else{
				tabUClick(objTM.id, strFID);
				strReturn = false;
			}
		}
		return strReturn;
	}

	function tabLock(strFID, blnStatus){
		var objTM = this
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				objTM._arrTMData[x][5] = blnStatus
				break;
			}
		}
	}

	function tabFunCall(strFID, strFun){
		var objTM = this
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				frames[objTM._arrTMData[x][0]].eval(strFun);
				break;
			}
		}
	}
	
	function tabSetValue(strFID, strValue){
		var objTM = this
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				objTM._arrTMData[x][6] = strValue;
				break;
			}
		}
	}
	
	function tabGetValue(strFID){
		var objTM = this
		var strReturn = "";
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				strReturn = objTM._arrTMData[x][6];
				break;
			}
		}
		return strReturn;
	}
	
	function tabRemove(strFID){
		var objTM = this
		return  tabURemoveClick(objTM.id, strFID);
	}
	
	function tabSetTabInfo(strOldTabID, strFID, strURL, strTabText, blnLoad){
		var objTM = this
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strOldTabID){
				objTM._arrTMData[x][1] = strTabText;
				objTM._arrTMData[x][2] = strURL;
				objTM._arrTMData[x][3] = strFID;
				objTM._arrTMData[x][7] = "";
				tabUClick(objTM.id, strFID);
				
				if (blnLoad){
					frames[objTM._arrTMData[x][0]].location.replace(strURL);
				}
				break;
			}
		}
	}
	
	function tabPageEdited(strFID, blnStatus){
		var objTM = this
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				objTM._arrTMData[x][7] = blnStatus
				break;
			}
		}
	}
	
	function tabGetPageEidted(strFID){
		var objTM = this
		var strReturn = "";
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				strReturn = objTM._arrTMData[x][7];
				break;
			}
		}
		return strReturn;
	}
	
	function tabIsAvailable(strFID){
		var objTM = this
		var strReturn = false;
		for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
			if (objTM._arrTMData[x][3] == strFID){
				strReturn = true;
				break;
			}
		}
		return strReturn;
	}
	
	// -------------------------------- End of File -----------------------------------


