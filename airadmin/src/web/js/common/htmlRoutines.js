<!--
//***********************************************
//Sudheera Liyanage
//htmlRoutines.js
//Common JS routines
//***********************************************
function getText(objId,val,form){
	return getField(objId,form).value;
}

//*****************
function setText(objId,val,form) {
    getField(objId,form).value=val;
}

//*****************
function getCombo(objId,form){
    var sel;
    var idx;
    var val="";
	
    sel=getField(objId,form);
    idx=sel.selectedIndex;
    if (idx>=0){
        val=sel.options[idx].value;
    }
    return val;
}

//*****************
function setCombo(objId,val,form){
    var len;
    var sel;

    sel=getField(objId,form);
    len=sel.options.length;

    for (var i=0;i<len;i++) {
		//alert(sel.options[i].value + '==' + val);
        if (sel.options[i].value==val) {
			sel.options[i].selected=true;
            sel.options[i].defaultSelected=true;
            return true;
        }
    }

    if (len < 0) {
        sel.options[0].selected=true;
        sel.options[0].defaultSelected=true;
    }
}

//*****************
function getCheckbox(objId,form){
    var chk=getField(objId,form);
    var val=(chk.checked)?chk.value:'';
    return val;
}

//*****************
function getRadio(objId,form){
    var val='';

    var rad=getField(objId,form);
    var len=rad.length;

    for(var i=0;i<len;i++){
        if (rad[i].checked){
            val=rad[i].value;
        }
    }
    return val;
}

//*****************
function setCheckbox(objId,val,form){
    var chk=getField(objId,form);
    chk.checked=(val==chk.value)?true:false;
}

//*****************
function setRadio(objId,val,form){
    var rad = getField(objId,form);
    var len = rad.length;
    var idx;

   	for(var i=0;i<len;i++){
		rad[i].checked=(rad[i].value==val);
    }
}

//*****************
function getField(id,form) {
    form=(!form)?window.document.forms[0]:form;
    return form[id];
}
//***********************
function object(id,win){
	win=(!win)?window:win;
	return win.getElementById(id);
}
//Commented this since it clashes with setField() method on Common.js
/*function setField(id,val,form){
	//alert(id);
    var obj=getField(id,form);
    var cType=obj.type;
    if(!cType){
        cType="radio";
    }

    switch(cType){
        case "text":
            setText(id,val,form);
            break;
        case "textarea":
            setText(id,val,form);
            break;
        case "hidden":
            setText(id,val,form);
            break;
        case "button":
            setText(id,val,form);
            break;
        case "password":
            setText(id,val,form);
            break;
        case "select-one":
            setCombo(id,val,form);
            break;
        case "checkbox":
            setCheckbox(id,val,form);
            break;
        case "radio":
            setRadio(id,val,form);
            break;
    }
    //alert(cType);
}
*/
//*****************
function getVal(id){
    //alert(id);
    var obj=getField(id);
    var cType=obj.type;
    var val="";

    if(!cType){
        cType="radio";
    }

    switch(cType){
        case "text":
            val=getText(id);
            break;
        case "textarea":
            val=getText(id);
            break;
        case "hidden":
            val=getText(id);
            break;
        case "password":
            val=getText(id);
            break;
        case "button":
            val=getText(id);
            break;
        case "select-one":
            val=getCombo(id);
            break;
        case "checkbox":
            val=getCheckbox(id);
            break;
        case "radio":
            val=getRadio(id);
            break;
        default:
            alert("htmlRoutines.js:gatVal:Invalid cType:" + cType);
    }
    //alert(cType);

    return val;
}

//*****************
function getComboText(objId,val){

    var sel=getField(objId);
    var len=sel.length;
    var text="";
    for(var i=0;i<len;i++){
        if (sel.options[i].value==val){
            text=sel.options[i].text;
            break;
        }
    }

    return text;
}

//*****************
function disableMe(id,bln){
	//alert(id);
    var obj=getField(id);
    var cType=obj.type;
    var val="";

    if(!cType)cType="radio-array";
    //alert("id="+id+"\ncType="+cType);
    if(cType=="text"||cType=="textarea"||cType=='button'){
            obj.disabled=bln;
    }else if((cType=="select-one")||(cType=="select-multiple")||(cType=="radio")||(cType=="checkbox")){
            obj.disabled=bln;
    }else if(cType=="radio-array"){
        for(var i=0;i<obj.length;i++){
            obj[i].disabled=bln;
        }
    }
    if(obj.clear)setVal(id,"");
}

//*****************
function disable(id){
	disableMe(id,true);
}

//*****************
function enable(id){
	disableMe(id,false);
}

//*****************
function winDsp(id,bln){
    var obj=eval("window."+id);
    var str=(bln)?"":"none";
    //alert(id);
    obj.style.display=str;
}

//*****************
function winHide(id,bln){
    var obj=eval("window."+id);
    var str=(bln)?"hidden":"visible";
    //alert(id);
    obj.style.visibility=str;
}

//*****************
function display(id,bln){
    var obj=object(id);
    var str=(bln)?"":"none";
    //alert(id);
    obj.style.display=str;
}

//*****************
function visible(id,bln){
    var obj=eval("window.document.all['"+id+"']");
    var str=(bln)?"visible":"hidden";
    //alert(id);
    obj.style.visibility=str;
}

//*****************
function FrmDsp(id,bln){
    var obj=getField(id);
    var str=(bln)?"":"none";
    //alert(id);
    obj.style.display=str;
}

//*****************
function frmHide(id,bln){
    var obj=getField(id);
    var str=(bln)?"hidden":"visible";
    //alert(id);
    obj.style.visibility=str;
}

//*****************
function recreateCombo(id,arrV,arrT,defVal){
    var sel=getField(id);
    var len=arrV.length;

    sel.options.length=len;

    for(var i=0;i<len;i++){
        sel.options[i].value=arrV[i];
        sel.options[i].text=arrT[i];
        if(arrV[i]==defVal){
            sel.options[i].selected=true;
            sel.options[i].defaultSelected=true;
        }
        sel.disabled=false;
    }
}

//****************
function disableChilds(id,bln){
	if(!object(id).child)return;
	var child=object(id).child.split(",");
	var len=child.length;

	for(var i=0;i<len;i++){
		disableMe(child[i],bln);
	}
}

function clearErrors(){
	window.tdErrors.innerHTML="";
}
//-->
