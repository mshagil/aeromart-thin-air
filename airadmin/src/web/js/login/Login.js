var captchaTxt;
var captchaEnabled = false;

function validate() {
	var blnStatus = true;
	objMsg.MessageType = "Error";
	
	captchaTxt=trim(getValue("captcha_txt"));
	setField("captcha_txt",captchaTxt);
	
	if (getValue("j_username") == "") {
		// objMsg.MessageText = raiseError("XBE-ERR-01","User ID")
		objMsg.MessageText = userInvalidID;
		getFieldByID("j_username").focus();
		ShowPageMessage();
		return false;
	}

	if (getValue("j_password") == "") {
		// objMsg.MessageText = raiseError("XBE-ERR-01","Password") ;
		objMsg.MessageText = userInvalidPassword;
		getFieldByID("j_password").focus();
		ShowPageMessage();

		return false;
	}
	
	if (captchaEnabled && captchaTxt==""){
		objMsg.MessageText = "Enter value for Captcha" ;
		getFieldByID("captcha_txt").focus();
		ShowPageMessage();
		refreshCaptcha();
		return false;
	}
	
	if (captchaEnabled && !validateCaptcha()){
		objMsg.MessageText = "Invalid Captcha" ;
		getFieldByID("captcha_txt").focus();
		getFieldByID("captcha_txt").value = "";
		ShowPageMessage();
		refreshCaptcha();
		return false;
	}
	
	setField("j_username", defaultAirlineId + getValue("j_username").toUpperCase())
	return true;
}

function login() {
	if (validate() && getValue("j_token") == "0") {
		setField("j_token", "1");
		Disable("btnLogin", true);
		getFieldByID("formLogin").target = "_self";
		getFieldByID("formLogin").submit();
	}
}

function closeLogin() {
	window.close();
}

function onLoad(logStatus) {
	
	captchaEnabled = isCaptchaEnabled();
	
	if (captchaEnabled){
		document.getElementById('spnWelcome').innerHTML = "To access the "
			+ carrier
			+ " online site, please enter your <b>User ID</b> , <b>Password</b> and <b>Captcha</b> and click <b>Login</b>.";
	} else {
		document.getElementById('spnWelcome').innerHTML = "To access the "
			+ carrier
			+ " online site, please enter your <b>User ID</b> and <b>Password</b> and click <b>Login</b>.";
	}

	getFieldByID("j_username").focus();
	if (serverErrorMsg != null && serverErrorMsg != "") {	
		objMsg.MessageText = serverErrorMsg;
		ShowPageMessage();	
	} else 	if (logStatus == "LOGINFAILED") {
		// objMsg.MessageText = raiseError("XBE-ERR-04","User ID / Password");
		// objMsg.MessageText = "Enter valid User ID & password ";
		if(getText("hdnUserStatus") == "true"){
			objMsg.MessageText = userInactiveStatus;
		} else {
			objMsg.MessageText = userInvalidUserIdPassword;
		}		
		ShowPageMessage();
	}
}

function fullScreen(strID) {
	var strFileName = 'showLogin.action';

	switch (strID) {
	case 0:
		window.open(strFileName, 'winAdmin', 'fullscreen=yes, scrollbars=no');
		break;
	default:
		window
				.open(
						strFileName,
						"winAdmin",
						'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=1024,height=768,resizable=no,top=1,left=1');
		break;
	}
}

function fullScreen2() {

	// --comenting the previous one ----------
	/*
	 * var strFileName = "showAirAdminMain.do"; window.open(strFileName,
 'winAdmin', 'fullscreen=yes, scrollbars=no');
	 * 
	 */

	// -------------------------------------------------
	var strFileName = request['secureLoginUrl'];
	var win;

	if (window.name != "winAdmin") {
		if(request['useSecureLogin']){
			if ((window.location.href.indexOf("https://") == -1)
					&& (strFileName.indexOf("https://") != -1)) {
				strFileName = window.location.href;
			}
		}

		reOpen = true;

		if (top.name == 'winAdmin') {
			top.location = strFileName;
		} else {
			if (window.opener && !window.opener.closed) {
				win = window.opener.top;
				if (!win.closed) {
					win.location = strFileName;
				} else {
					window.open(strFileName, 'winAdmin',
							'fullscreen=yes, scrollbars=no');
				}
				window.close();
			} else {
				window.open(strFileName, 'winAdmin',
						'fullscreen=yes, scrollbars=no');
			}
		}
	} else if ((window.location.href.indexOf("https://") == -1)
			&& (strFileName.indexOf("https://") != -1)) {
		window.location.replace(strFileName);
	}
	// ---------------------------------------------------------

}

function Login_onKeyDown(objEvent) {
	var strKeyValue = objEvent.keyCode;
	var strDisable = "N";
	if (strKeyValue == 13) {
		login();
	}
}

function writeWelcome() {
	var strHTMLText = "";

}

function isCaptchaEnabled(){
	
	var data = {};
	var isCaptchaEnabled = false;
	$.ajax({
		type : "POST",
		url : "../../public/master/showCaptcha.action",
		dataType : "json",
		data : data,
		async : false,
		success: function (response) {
			
			if (response.captchaEnabled){
				isCaptchaEnabled = true;
			}

		}
	});
	
	return isCaptchaEnabled;
	
}

function validateCaptcha(){
	
	var data = {};
	var isCaptchaValidated = false;
	data['captchaTxt'] = captchaTxt;
	$.ajax({
		type : "POST",
		url : "../../public/master/adminCaptchaValidation.action",
		dataType : "json",
		data : data,
		async : false,
		success: function (response) {
			isCaptchaValidated = response.captchaValidated;
		}
	});
	
	return isCaptchaValidated;
}

function refreshCaptcha() {
    $("#txtCaptcha").val("");
    document['imgCPT'].src = '../../private/jcaptcha_new?relversion=' + (new Date()).getTime();
}


// -->
