var selectedMeal = '';
var grdArray = new Array();

jQuery(document)
		.ready(
				function() { // the page is ready

					grdArray = new Array();

					$("#divSearchPanel").decoratePanel("Search Templates");
					$("#divDispMealTemp").decoratePanel(
							"Add/Modify Meal Template");
					$("#divResultsPanel").decoratePanel("Meal Templates");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();
					$('#btnChgAdd').decorateButton();
					$('#btnChgEdit').decorateButton();
					$('#btnRestrict').decorateButton();
					$('#btnEdtPop').decorateButton();
					enableGridButtons(false);
					enbleFormButtons(false);
					createPopUps('Meal Restrictions',300,350,1);
					createPopUps('Add Popularity',380,550,2);
					PopUpData();
					var clickedRowId = -1;
					var isEdit = false;
					var mealRestrictedSelectedRowData = [];

					jQuery("#listMealTemp")
							.jqGrid(
									{
										url : 'showMealTemplate!searchMealTemplate.action?selStatus='
												+ $("#selStatus").val(),
										datatype : "json",
										jsonReader : {
											root : "rows",
											page : "page",
											total : "total",
											records : "records",
											repeatitems : false,
											id : "0"
										},
										colNames : [ '&nbsp;', 'Template Id',
												'Template Code', 'Description',
												'Status', 'Version',
												'createdBy', 'createdDate',
												'mealCharges',
												'ChargeLocalCurrencyCode','catRestriction'],
										colModel : [
												{
													name : 'id',
													width : 40,
													jsonmap : 'id'
												},
												{
													name : 'templateId',
													index : 'templateId',
													width : 125,
													jsonmap : 'mealTemplate.templateId'
												},
												{
													name : 'templateCode',
													width : 175,
													jsonmap : 'mealTemplate.templateCode'
												},
												{
													name : 'description',
													width : 275,
													jsonmap : 'mealTemplate.description'
												},
												{
													name : 'status',
													width : 150,
													align : "center",
													jsonmap : 'mealTemplate.status'
												},
												{
													name : 'version',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'mealTemplate.version'
												},
												{
													name : 'createdBy',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'mealTemplate.createdBy'
												},
												{
													name : 'createdDate',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'mealTemplate.createdDate'
												},
												{
													name : 'mealCharge',
													width : 175,
													align : "center",
													hidden : true
												},
												{
													name : 'chargeLocalCurrencyCode',
													jsonmap : 'mealTemplate.chargeLocalCurrencyCode',
													width : 150,
													align : "center",
													hidden : true
												},
												{
													name : 'catRestriction',
													jsonmap : 'catRestriction',
													width : 150,
													align : "center",
													hidden : true
												}],
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										pager : jQuery('#temppager'),
										rowNum : 20,
										viewrecords : true,
										height : 175,
										width : 925,
										loadui : 'block',
										onSelectRow : function(rowid) {
											if (isPageEdited()) {
												$('#frmMeal').clearForm();
												clearRestrictions();
												hideMessage();
												setPageEdited(false);
												$("#rowNo").val(rowid);
												fillForm(rowid);
												disableControls(true);
												enableSearch();
												enableGridButtons(true);
												enbleFormButtons(false);
												$("#btnRestrict").enableButton();
												$("#btnPopupClose").attr("disabled",false);
											}
										}
									}).navGrid("#temppager", {
								refresh : true,
								edit : false,
								add : false,
								del : false,
								search : false
							});

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
													// this can be used to do
													// the validation part
						success : showResponse, // post-submit callback
						dataType : 'json' // 'xml', 'script', or 'json'
											// (expected server response type)

					};

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
													// this can be used to do
													// the validation part
						success : showResponse, // post-submit callback
						url : 'showMealTemplate!deleteTemplate.action', // override
																		// for
																		// form's
																		// 'action'
																		// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
											// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};

					function fillForm(rowid) {
						jQuery("#listMealTemp").GridToForm(rowid, "#frmMeal");
						if (jQuery("#listMealTemp").getCell(rowid, 'status') == 'ACT') {
							$("#status").attr('checked', true);
						} else {
							$("#status").attr('checked', false);
						}

						code = jQuery("#listMealTemp").getCell(rowid,
								'chargeLocalCurrencyCode');
						$("#localCurrCode").val(code);
						$("#lblLocalCurrencyCode").html(code);

						var charge = jQuery("#listMealTemp").getCell(rowid,
								'mealCharge');
						var arrCharge = charge.split("~");
						var mlcharge;
						grdArray = new Array();
						for (var i = 0; i < arrCharge.length - 1; i++) {
							grdArray[i] = new Array();
							mlcharge = arrCharge[i].split(",");
							grdArray[i]["mId"] = i + 1;
							grdArray[i]["meal"] = mlcharge[0];
							grdArray[i]["chargeId"] = mlcharge[1];
							grdArray[i]["templateId"] = mlcharge[2];
							grdArray[i]["mealId"] = mlcharge[3];
							grdArray[i]["price"] = mlcharge[4];
							grdArray[i]["allocatedMeal"] = mlcharge[5];
							grdArray[i]["mversion"] = mlcharge[6];
							grdArray[i]["mstatus"] = mlcharge[7];
							grdArray[i]["cosDescription"] = mlcharge[8];
							grdArray[i]["cabinAndLogicalCC"] = mlcharge[9];
							grdArray[i]["popularity"] = mlcharge[10];
						}
						populateMealGrid();
						
						clearRestrictions();
						var restrict = jQuery("#listMealTemp").getCell(rowid,'catRestriction');
						mealRestrictedSelectedRowData = restrict.split("~");
						buildMealRestrictedUI();
						isEdit = false;
						
						
						clickedRowId = rowid;
					}
					
					function buildMealRestrictedUI() {
						
						mealRestrictedSelectedRowData.forEach(function(entry) {
						    if(entry!=null && entry != ""){
						    	var arrCats = entry.split(",");
						    	$("#chkCat_"+arrCats[0]).prop('checked', true);
						    }
						});
						
					}

					// pre-submit callback
					function showRequest(formData, jqForm, options) {
						if (trim($("#mealCharges").val()) == "") {
							showCommonError("Error", mealRqrd);
							return false;
						}
						if (trim($("#templateCode").val()) == "") {
							showCommonError("Error", tempCodeRqrd);
							return false;
						}
						if (trim($("#description").val()) == "") {
							showCommonError("Error", descriptionRqrd);
							return false;
						}
						if ($("#localCurrCode").val() == null) {
							showCommonError("Error", localCurrencyCodeRqrd);
							return false;
						}
						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							alert(responseText['succesMsg']);
							setPageEdited(false);
							$('#frmMeal').clearForm();
							clearRestrictions();
							jQuery("#listMealChg").clearGridData();
							jQuery("#listMealTemp").trigger("reloadGrid");
							enableGridButtons(false);
							enbleFormButtons(false);
							$("select#selTemplate").html(
									responseText['templOption']);

						}

					}

					function disableForm(cond) {
						$("#templateCode").attr('disabled', cond);
						$("#description").attr('disabled', cond);
						$("#status").attr('disabled', cond);
						$("#listMealChg").attr('disabled', cond);
						$("#cos").attr('disabled', cond);
						$("#price").attr('disabled', cond);
						$("#allocatedMeal").attr('disabled', cond);
						$("#mstatus").attr('disabled', cond);
						$("#btnChgAdd").attr('disabled', cond);
						$("#btnChgEdit").attr('disabled', cond);
						$("#btnSave").attr('disabled', cond);
						$("#btnRestrict").attr('disabled', cond);
					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
					$('#btnSave').click(function() {
						hideMessage();
						disableControls(false);
						if ($("#status").attr('checked')) {
							$("#status").val('ACT');
						} else {
							$("#status").val('INA');
						}

						hidePopUp(1);
						alert("Saving Meal Template will take some time.....");

						$("#mealCharges").val(buildChargeStr());
						$("#multiRestrictions").val(buildRestrictionStr());
						$('#templateCode').attr('disabled', false);
						$('#frmMeal').ajaxSubmit(options);
						return false;
					});

					function buildChargeStr() {
						var chargStr = "";
						for (var j = 0; j < grdArray.length; j++) {
							chargStr += grdArray[j]["meal"] + ",";
							chargStr += grdArray[j]["chargeId"] + ",";
							chargStr += grdArray[j]["templateId"] + ",";
							chargStr += grdArray[j]["mealId"] + ",";
							chargStr += grdArray[j]["price"] + ",";
							chargStr += grdArray[j]["allocatedMeal"] + ",";
							chargStr += grdArray[j]["mversion"] + ",";
							chargStr += grdArray[j]["mstatus"] + ",";
							chargStr += grdArray[j]["cabinAndLogicalCC"] + ",";
							chargStr += grdArray[j]["popularity"] + ",";
							chargStr += "~";
						}
						return chargStr;
					}

					$('#price').keyup(function() {
						var str = $("#price").val();
						str = str.replace(/[^0-9.]+/g, '');
						str = str.replace("..", '');
						if (str.indexOf(".") != -1){
							if (str.indexOf(".")+2 < str.length-1){
								str=str.substr(0,(str.indexOf(".")+3)); 
							}
						}
						$("#price").attr("value", str);
					});
					
					$('#price').keypress(function(e) {
						var str = $("#price").val();
						str = str.replace(/[^0-9.]+/g, '');
						
						if ((str.indexOf(".") != -1) && e.keyCode == 110){
							str=str+".";
						}
						str = str.replace("..", '');
						$("#price").attr("value", str);
					});
					
					$('#price').change(function() {
						var str = $("#price").val();
						str = str.replace(/[^0-9.]+/g, '');
						str = str.replace("..", '');
						if (str.indexOf(".") != -1){
							if (str.indexOf(".")+2 < str.length-1){
								str=str.substr(0,(str.indexOf(".")+3)); 
							}
						}
						$("#price").attr("value", str);
					});

					$('#allocatedMeal').keyup(function() {
						var str = $("#allocatedMeal").val();
						str = str.replace(/[^0-9]+/g, '');
						$("#allocatedMeal").attr("value", str);
					});
					$('#allocatedMeal').change(function() {
						var str = $("#allocatedMeal").val();
						str = str.replace(/[^0-9]+/g, '');
						$("#allocatedMeal").attr("value", str);
					});
					
					$('#popularity').keyup(function() {
						var str = $("#popularity").val();
						str = str.replace(/[^0-9]+/g, '');
						$("#popularity").attr("value", str);
					});
					
					$('#popularity').change(function() {
						var str = $("#popularity").val();
						str = str.replace(/[^0-9]+/g, '');
						$("#popularity").attr("value", str);
					});

					$('#cos').change(function() {
						loadMealList('false', "");
					});

					function loadMealList(condition, mealValue) {
						var data = {};
						data["cabinClass"] = $("#cabinClassCode").val();
						data["isGridMeal"] = condition;
						data["mealValue"] = mealValue;

						if (condition == "true") {
							$("#selMeal").attr('disabled', true);
						} else {
							$("#selMeal").attr('disabled', false);
						}

						$("#frmMeal")
								.ajaxSubmit(
										{
											dataType : 'json',
											data : data,
											url : "showMealTemplate!getMealsForCabinClass.action",
											success : loadSuccess
										});
					}

					loadSuccess = function(response) {
						if (response.msgType != "Error"
								&& response.mealsForCC != null
								&& trim(response.mealsForCC) != "") {
							$("#selMeal").html(response.mealsForCC);
						} else {
							alert("No Meals for selected cabin class");
							$("#selMeal").html("<option value='-1,,0'>");
							$("#selMeal").attr('disabled', true);
						}
					}

					$('#btnSearch')
							.click(
									function() {
										hideMessage();
										var strUrl = "showMealTemplate!searchMealTemplate.action?selTemplate="
												+ $("#selTemplate").val()
												+ "&selStatus="
												+ $("#selStatus").val();
										jQuery("#listMealTemp").setGridParam({
											url : strUrl
										});
										jQuery("#listMealTemp").trigger(
												"reloadGrid");
										enbleFormButtons(false);
										enableGridButtons(false);
										$('#frmMeal').clearForm();
										clearRestrictions();
										$("#version").val('-1');
										jQuery("#listMealChg").clearGridData();
										grdArray = new Array();
										disableControls(true);
										enableSearch();
										$("#btnAdd").attr('disabled', false); // fixed
																				// JIRA:AARESAA-2977
																				// Lalanthi
									});

					$('#btnAdd').click(function() {
						hideMessage();
						disableControls(false);
						$('#frmMeal').clearForm();
						clearRestrictions();
						$("#version").val('-1');
						jQuery("#listMealChg").clearGridData();
						grdArray = new Array();
						enbleFormButtons(true);
						if($("#localCurrCode").val() == ""){
							$("#localCurrCode").val(localCurrency);
							$("#lblLocalCurrencyCode").html(localCurrency);
						}
						isEdit = true;
					});

					$('#btnEdit').click(function() {
						hideMessage();
						disableControls(false);
						var mealRow = $("#selMeal").val();
						if (mealRow != null) {
							$("#cabinClassCode").attr('disabled', true);
							$("#selMeal").attr('disabled', true);
							$("#cos").attr('disabled', true);
						}
						$('#templateCode').attr('disabled', true);
						enbleFormButtons(true);
						isEdit = true;
					});
					$('#btnReset').click(function() {
						hideMessage();

						var mealArr = trim($("#selMeal").val()).split(",");
						$("#selMeal").html("<option value='-1,,0'>");
						$("#selMeal").attr('disabled', false);
						$("#cos").attr('disabled', false);
						$("#allocatedMeal").val('');
						$("#cos").val('');
						$("#price").val('');
						$("#popularity").val('');
						$("#mstatus").val('');
						selectedMeal = '';
						if ($("#rowNo").val() == '') {
							grdArray = new Array();
							$("#templateCode").val('');
							$("#description").val('');
							$("#status").val('');

							// $('#frmMeal').resetForm();
							jQuery("#listMealChg").clearGridData();
							clearRestrictions();
						} else {
							fillForm($("#rowNo").val());
						}
						isEdit = true;
						$('#btnChgAdd').enableButton();
						$('#btnChgEdit').disableButton();

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmMeal').ajaxSubmit(delOptions);
						disableForm(true);
						return false;
					});

					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("#btnClose").attr('disabled', false);
						$("#btnAdd").attr('disabled', false);
						$('#btnChgAdd').enableButton();
						$('#btnChgEdit').disableButton();
						isEdit = false;
					}

					function enableGridButtons(cond) {
						if (cond) {
							$("#btnEdit").enableButton();
							$("#btnDelete").enableButton();
						} else {
							$("#btnEdit").disableButton();
							$("#btnDelete").disableButton();
						}

					}

					function enbleFormButtons(cond) {
						if (cond) {
							$("#btnReset").enableButton();
							$("#btnSave").enableButton();
							$("#btnRestrict").enableButton();
							$("#btnEdtPop").enableButton();
						} else {
							$("#btnReset").disableButton();
							$("#btnSave").disableButton();
							$("#btnRestrict").disableButton();
							$("#btnEdtPop").disableButton();
						}

					}

					function enableSearch() {
						$("#btnSearch").attr('disabled', false);
						$("#selTemplate").attr('disabled', false);
						$("#selStatus").attr('disabled', false);
					}

					disableControls(true);
					$("#btnAdd").attr('disabled', false);
					enableSearch();
					jQuery("#listMealChg")
							.jqGrid(
									{
										datatype : 'local',
										colNames : [ '&nbsp;', 'Meal',
												'chargeId', 'templateId',
												'mealId', 'Price', 'Quantity',
												'Popularity', 'COS', 'version',
												'cabinAndLogicalCC', 'Status' ],
										colModel : [ {
											name : 'mId',
											index : 'mId',
											width : 40,
											sorttype : 'int'
										}, {
											name : 'meal',
											index : 'meal',
											width : 180
										}, {
											name : 'chargeId',
											index : 'chargeId',
											hidden : true,
											width : 150
										}, {
											name : 'templateId',
											index : 'templateId',
											hidden : true,
											width : 180
										}, {
											name : 'mealId',
											index : 'mealId',
											hidden : true,
											width : 70
										}, {
											name : 'price',
											index : 'price',
											align : "right",
											width : 100,
											sortable : false
										}, {
											name : 'allocatedMeal',
											width : 100,
											align : "right"
										}, {
											name:'popularity', 
											width:100, 
											align:"center", 
											sorttype:'number'
										}, {
											name : 'cosDescription',
											width : 155,
											align : "center"
										}, {
											name : 'mversion',
											width : 225,
											align : "center",
											hidden : true
										}, {
											name : 'cabinAndLogicalCC',
											width : 225,
											align : "center",
											hidden : true
										}, {
											name : 'mstatus',
											width : 100,
											align : "center"
										} ],
										viewrecords : true,
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										loadui : 'block',
										height : 100,
										beforeSelectRow: function(rowid) {
											if(!isEdit){
												return false;
											}
											return true;
										},
										onSelectRow : function(rowid) {
											selectedMeal = rowid;
											hideMessage();
											$("#btnChgAdd").disableButton();
											$("#btnChgEdit").enableButton();
											$("#selMeal")
													.attr('disabled', true);
											$("#cos").attr('disabled', true);
											jQuery("#listMealChg").GridToForm(
													rowid, "#frmMeal");
											var classOfService = jQuery(
													"#listMealChg").getCell(
													rowid, 'cabinAndLogicalCC');
											$('#cos option')
													.each(
															function(i, option) {
																var selVal = $(
																		this)
																		.val();
																if (trim(selVal) == trim(classOfService)) {
																	$(
																			'#cos option[value$="'
																					+ selVal
																					+ '"]')
																			.attr(
																					'selected',
																					true);
																}
															});
											var sltdmeal = jQuery(
													"#listMealChg").getCell(
													rowid, 'mealId')
													+ ","
													+ jQuery("#listMealChg")
															.getCell(rowid,
																	'meal')
													+ ","
													+ jQuery("#listMealChg")
															.getCell(rowid,
																	'price');
											$("#selMeal").val(sltdmeal);
											loadMealList('true', sltdmeal);
											$("#selMeal")
													.attr('disabled', true);
											$("#btnChgEdit").attr('disabled',
													false);
										}

									});

					$('#btnChgAdd')
							.click(
									function() {
										var mealArr = null;
										if ($("#selMeal").val() != null) {
											mealArr = trim($("#selMeal").val())
													.split(",");
										}
										if (($("#selMeal").val() == null)
												|| (trim(mealArr[0]) == "")
												|| (trim(mealArr[0]) == "-1")) {
											showCommonError("Error",
													mealCodeRqrd);
											return false;
										} else if (($("#allocatedMeal").val() == null)
												|| (trim($("#allocatedMeal")
														.val()) == "")) {
											showCommonError("Error",
													mealQtyRqrd);
											return false;
										} else if (!isPositiveInt(trim($(
												"#allocatedMeal").val()))
												|| (parseInt($("#allocatedMeal")
														.val()) <= 0)) {
											showCommonError("Error",
													"Please Enter a valid Quantity");
											return false;
										} else if (($("#mstatus").val() == null)
												|| (trim($("#mstatus").val()) == "")) {
											showCommonError("Error",
													"Please Enter the Meal Status");
											return false;
										} else if (!checkForMeal()) {
											$("#cos").attr('disabled', false);
											$("#selMeal").attr('disabled',
													false);
											showCommonError("Error", mealExist);
											return false;
										} else if(($("#popularity").val() == null) 
												|| (trim($("#popularity").val()) == "")) {
									    	showCommonError("Error", popularityRqrd);
									    	return false; 
									    } else if(($("#price").val() == null) 
												|| (trim($("#price").val()) == "")) {
									    	showCommonError("Error", priceRqrd);
									    	return false; 
									    } else if(!validatePopularity(mealArr[0])){									    	
									    	showCommonError("Error", popularityExist);
									    	return false; 
									    } else {
											setPageEdited(true);
											var msize = grdArray.length;

											grdArray[msize] = new Array();
											grdArray[msize]["mId"] = msize + 1;
											grdArray[msize]["meal"] = trim(mealArr[1]);
											grdArray[msize]["chargeId"] = '';
											grdArray[msize]["templateId"] = trim($(
													"#templateId").val());
											grdArray[msize]["mealId"] = trim(mealArr[0]);
											grdArray[msize]["price"] = trim($(
													"#price").val());
											grdArray[msize]["allocatedMeal"] = trim($(
													"#allocatedMeal").val());
											grdArray[msize]["mversion"] = -1;
											grdArray[msize]["mstatus"] = trim($(
													"#mstatus").val());
											grdArray[msize]["cabinAndLogicalCC"] = trim($(
													"#cos").val());
											if (trim($("#cos").val().split("_")[0]) == trim($(
													"#cos").val().split("_")[1])) {
												grdArray[msize]["cosDescription"] = trim($(
														"#cos option:selected")
														.text());
											} else {
												grdArray[msize]["cosDescription"] = trim(
														$(
																"#cos option:selected")
																.text())
														.substring(2);
											}
											grdArray[msize]["popularity"] = trim($("#popularity").val());
											var arrObj = $.extend({},
													grdArray[msize]);// need
																		// js
																		// object
																		// to
																		// fill
																		// the
																		// grid
																		// other
																		// than
																		// the
																		// Array
											jQuery("#listMealChg").addRowData(
													msize + 1, arrObj);
											$("#selMeal").html(													
													"<option value='-1,,0'>");
											$("#cos").val('');											
											$("#selMeal")
													.attr('disabled', true);
											$("#price").attr('disabled', false);
											$("#allocatedMeal").attr(
													'disabled', false);
											$("#mstatus").attr('disabled',
													false);
											$("#allocatedMeal").val('');
											$("#price").val('');
											$("#popularity").val('');
										}

									});
					
					
					function validatePopularity(selMealId){
						var popularity=$("#popularity").val().replace(/^0+/, '');
						for(gl=0;gl< grdArray.length;gl++) {
							if(trim(popularity) === grdArray[gl]["popularity"] &&
									selMealId != grdArray[gl]["mId"] ){								
								return false
							}					
						}
						return true				
					}

					function checkForMeal() {
						var classOfService = $("#cos").val();
						var selCosArr = classOfService.split('_');
						var selectedMealLcc = selCosArr[0];
						var selectedMealCabin = selCosArr[1];
						var mealArr = trim($("#selMeal").val()).split(",");

						for (var i = 0; i < grdArray.length; i++) {
							var gridCabinAndLogicalCC = grdArray[i]["cabinAndLogicalCC"];
							var gridCosArr = gridCabinAndLogicalCC.split('_');
							var gridSSRLcc = gridCosArr[0];
							var gridSSRCabin = gridCosArr[1];

							if (grdArray[i]["mealId"] == mealArr[0]) {
								if (gridCabinAndLogicalCC == classOfService) {
									return false;
								} else if (selectedMealCabin == gridSSRCabin) {
									var selSSRLastChar = selectedMealLcc
											.substr(-1);
									var gridSSRLastChar = gridSSRLcc.substr(-1);
									if ((selSSRLastChar == '^' && gridSSRLastChar != '^')
											|| (selSSRLastChar != '^' && gridSSRLastChar == '^')) {
										return false;
									}
								}
							}

						}
						return true;
					}

					$('#btnChgEdit')
							.click(
									function() {
										if (selectedMeal == '') {
											return;
										} else if (selectedMeal != ''
												&& $("#allocatedMeal").val() != null
												&& (parseInt($("#allocatedMeal")
														.val()) <= 0 || !isPositiveInt(trim($(
														"#allocatedMeal").val())))) {
											showCommonError("Error",
													"Please Enter a valid Quantity");
											return;
										} else if ($("#popularity").val() != null
											&& (parseInt($("#popularity")
													.val()) <= 0 || !isPositiveInt(trim($(
													"#popularity").val())))) {
												showCommonError("Error",popularityValid);
												return;
										} else if(($("#price").val() == null) 
												|| (trim($("#price").val()) == "")) {
									    	showCommonError("Error", priceRqrd);
									    	return false; 
									    } else if(!validatePopularity($("#mId").val())){									    	
									    	showCommonError("Error", popularityExist);
									    	return false; 
									    }										
										$("#cos").attr('disabled', false);
										setPageEdited(true);
										var inId = $("#mId").val();
										var newPrice = trim($("#price").val());
										var quanty = trim($("#allocatedMeal")
												.val());
										var stat = trim($("#mstatus").val());
										var popu = trim($("#popularity").val());
										grdArray[inId - 1]["price"] = newPrice;
										grdArray[inId - 1]["allocatedMeal"] = quanty;
										grdArray[inId - 1]["mstatus"] = stat;
										grdArray[inId-1]["popularity"] = popu;
										
										// $("#btnChgEdit").attr('disabled',
										// true);
										jQuery("#listMealChg").setRowData(inId,
												{
													price : newPrice,
													allocatedMeal : quanty,
													mstatus : stat,
													popularity :popu
												})
										jQuery("#listMealChg").resetSelection();
										$("#btnChgAdd").enableButton();
										$("#btnChgEdit").disableButton();
										$("#selMeal").html("<option value='-1,,0'>");
										$("#cos").val('');
										$("#selMeal").attr('disabled', true);
										$("#price").attr('disabled', false);
										$("#allocatedMeal").attr('disabled', false);
										$("#mstatus").attr('disabled',false);
										$("#allocatedMeal").val('');
										$("#price").val('');
										$("#popularity").val('');
									});

					function populateMealGrid() {
						jQuery("#listMealChg").clearGridData();
						for (var i = 0; i < grdArray.length; i++) {
							var arrObj = $.extend({}, grdArray[i]);// need js
																	// object to
																	// fill the
																	// grid
																	// other
																	// than the
																	// Array
							jQuery("#listMealChg").addRowData(i + 1, arrObj);
						}
					}

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
						hideMessage();
					}

					function isPageEdited() {
						return top.loadCheck(top[1].objTMenu
								.tabGetPageEidted(screenId));
					}
					function hideMessage() {
						top[2].HidePageMessage();
					}

					function PopUpData() {
						var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">'; 
						html +=	'		<tr>';
						html +=	'			<td>';
						html +=	'				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">'; 
						html += '					<tr>';
						html += '						<td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
						html += '						<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif">';
						html += ' 							<font class="FormHeader"> Restrict Multiple Meal Selection </font>';
						html += ' 						</td>';
						html += ' 						<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
						html += ' 					</tr>';
						html += '					<tr>';
						html += '						<td class="FormBackGround"></td>';
						html += '						<td class="FormBackGround" valign="top">';
						html += ' 							<table width="98%" border="0" cellpadding="0" cellspacing="0">';
						html += ' 								<tr>';
						html += '									<td width="100%" align="left">';
						html += ' 										<table width="98%" border="0" cellpadding="0" cellspacing="0">';
						for(var i=0;i<arrMealCategory.length;i++){
									html += '								<tr align="left" valign="middle" class="fntMedium"> ';
									html += '									<td width="75%" class="fntMedium"><font>' + arrMealCategory[i][1] + '</font></td>';
									html += '									<td width="25%" align="right" style="padding-bottom:3px; ">';
									html += '										<input type="checkbox" name=chkCat_' + arrMealCategory[i][0] + ' id=chkCat_' + arrMealCategory[i][0] + '>';
									html += '									</td> ';
									html += '								</tr> ';
						}
						html += '										</table>';
						html += '									</td>';
						html += '								</tr>';
						html += ' 								<tr>';
						html += ' 									<td align="top"><span id="spnSta"></span></td>';
						html += ' 								</tr> ';
						html += '								<tr>';
						html += ' 									<td align="right">';
						html += '										<input type="button" id="btnConfirm" value= "Confirm" class="Button" onClick="hidePopUp(1);"> &nbsp;';
						html += ' 										<input type="button" id="btnPopupClose" value= "Close" class="Button" >';
						html += ' 									</td>';
						html += '								</tr>';
						html += '							</table>';
						html += '						</td>';
						html += '						<td class="FormBackGround"></td>';
						html += '					</tr>';
						html += ' 					<tr> ';
						html += '						<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
						html += '						<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> ';
						html += '						<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> ';
						html += '					</tr>';
						html += ' 				</table> ';
						html += '			</td>';
						html += '		</tr>';
						html += '	</table>';

						DivWrite("spnPopupData", html);
					}

					$('#btnRestrict').click(function() {						
						showPopUp(1);
						if(!isEdit){
							disableMealRestrictions(true);
						}else{
							disableMealRestrictions(false)
						}
					});
					
					function disableMealRestrictions(condition){
						for (var j = 0; j < arrMealCategory.length; j++){
							$("#chkCat_"+arrMealCategory[j][0]).attr('disabled',condition);
						}
					}
					
					function buildRestrictionStr() {
						var restStr = "";
						for (var j = 0; j < arrMealCategory.length; j++) {
							if ($("#chkCat_"+arrMealCategory[j][0]).attr('checked')) {
								restStr += arrMealCategory[j][0] + ",";
								restStr += getSelectedIdAndVersion(arrMealCategory[j][0]) + ",";
								restStr += "~";
							}
						}
						return restStr;
					}
					
					function clearRestrictions() {
						for (var j = 0; j < arrMealCategory.length; j++) {
							$("#chkCat_"+arrMealCategory[j][0]).prop('checked', false);
						}
						
						clickedRowId = -1;
					}
					
					function getSelectedIdAndVersion(categoryId){
						var strReturn = "-1,-1";
						
						if(clickedRowId !=-1){
							var selRestrict = jQuery("#listMealTemp").getCell(clickedRowId,'catRestriction');
							
							var arrSelRestrict = selRestrict.split("~");
							
							arrSelRestrict.some(function(entry) {
							    if(entry!=null && entry != ""){
							    	var arrSelCats = entry.split(",");
							    	if(categoryId == arrSelCats[0]){
							    		strReturn = arrSelCats[1] + "," + arrSelCats[2];
							    		return strReturn;
							    	}
							    }
							});
						}
						
						return strReturn;
					}
					
					$('#btnEdtPop').click(function(){						
						PopUpPopularData();
						showPopUp(2);
					});
					
					$('#btnPopupClose').click(function() {
						clearRestrictions();
						buildMealRestrictedUI();
						hidePopUp(1);
					});

				});

// pop up function did not work in side jquery ready

function PopUpPopularData(){
	var html = '<table border="0" width="99%" cellspacing="0" cellpadding="0" align="center"><tr><td>';
    html += '<table id="Table4" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">';
    html += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif" /></td>';
    html += '<td class="FormHeadBackGround" height="20"><img src="../../images/bullet_small_no_cache.gif" /> <span class="FormHeader"> Add Popularity </span></td>';
    html += '<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif" /></td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td class="FormBackGround">&nbsp;</td>';
    html += '<td class="FormBackGround" valign="top">';
    html += '<table><tr><td align="left" width="663">';
    html += '<table border="1" width="98%" cellspacing="0" cellpadding="0">';
    html += '<tr class="fntMedium" align="left" valign="middle">';
    html += '<td class="fntMedium" width="75%"><font><b>Meals</b></font></td>';
    html += '<td class="fntMedium" align="center"><font><b>Popularity</b></font></td>';
    html += '</tr>';
    grdArray = grdArray.sort(function(a,b) {return a['popularity'] - b['popularity'];});
    for(var pl =0;pl<grdArray.length;pl++){
    	html += '<tr>';
    	html += '<td class="fntMedium"><font>'+grdArray[pl]["meal"];
    	html += '</font></td>';
    	html += '<td align="center" style="padding-bottom:5px;"><input style="text-align:center;"  name="popPopularity_'+pl +'" type="text" id="popPopularity_'+pl +'" onkeyup="validateEditPopularity(this)"  size="5" maxlength="3" value="'+grdArray[pl]["popularity"]+'">';
    	html += '</td>';
    	html += '</tr>';			    	
    }
    html += '</table>';					    
    html += '</td>';
    html += '</tr>';
    html += '</table>';
    html += '</td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td align="top">&nbsp;</td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td>&nbsp;</td>';
    html += '<td align="right"><input id="btnPopConfirm" class="Button" type="button" value="Confirm" onClick="confirmPopUp();"> &nbsp;<input id="btnPopupClose" class="Button" type="button" value="Close" onClick="hidePopUp(2);" /></td>';
    html += '</tr>';
    html += '</table>';
    html += '</td>';
    html += '<td class="FormBackGround">&nbsp;</td>';
    html += '</tr>';
    html += '<tr>';
    html += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif" /></td>';
    html += '<td class="FormBackGround" height="12"><img src="../../images/spacer_no_cache.gif" width="100%" height="1" /></td>';
    html += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif" /></td>';
    html += '</tr>';
    html += '</table>';

    DivWrite("spnPopupData2",html);
}

function confirmPopUp(){
	
	var popularArray =  new Array();
	
	for(var pl =0;pl<grdArray.length;pl++){
		popularArray[pl] = new Array();	
		if ($('#popPopularity_'+pl).val() != null && (parseInt($('#popPopularity_'+pl).val()) <= 0 || 
				!isPositiveInt(trim($('#popPopularity_'+pl).val())))) {
					showCommonError("Error",popularityValid);
					return false;
		}
		popularArray[pl]['popularity'] = trim($('#popPopularity_'+pl).val()); 
	}						
	
	if(checkDuplicatePopularity('popularity', popularArray)){
		showCommonError("Error", popularityExist);
    	return false; 
	}
	for(var cl =0;cl<grdArray.length;cl++){
		grdArray[cl]['popularity'] = trim($('#popPopularity_'+cl).val());
	}						
	grdArray = grdArray.sort(function(a,b) {return a['popularity'] - b['popularity'];});
	populatePopMealGrid();
	hidePopUp(2);
}

function validateEditPopularity(objCon){
	objCon.value = objCon.value.replace(/[^0-9]+/g, '');
}

function populatePopMealGrid() {
	jQuery("#listMealChg").clearGridData();
	for (var i = 0; i < grdArray.length; i++) {
		var arrObj = $.extend({}, grdArray[i]);// need js object to fill the grid other than the Array
		jQuery("#listMealChg").addRowData(i + 1, arrObj);
	}
}

function checkDuplicatePopularity(popularity, popArry) {
	  var haveDuplicate = false,
	      testObject = {};
	  
	  popArry.map(function(item) {
	    var itemPropertyName = item[popularity]; 
	    if (itemPropertyName in testObject) {
	      testObject[itemPropertyName].duplicate = true;
	      item.duplicate = true;
	      haveDuplicate = true;
	    }
	    else {
	      testObject[itemPropertyName] = item;
	      delete item.duplicate;
	    }
	  });
	  
	  return haveDuplicate;
}
