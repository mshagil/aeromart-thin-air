jQuery(document).ready(function(){  //the page is ready				
			
			$("#divSearch").decoratePanel("Search Meals");
			$("#divResultsPanel").decoratePanel("Meals");
			$("#divDispMeal").decoratePanel("Add/Modify Meal");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSave').decorateButton();
			$('#btnUpload').decorateButton();
			$('#btnSearch').decorateButton();
			$('#btnMealFD').decorateButton();
			$('#btnUploadThumbnail').decorateButton();
			
			disableMealButton();
			createPopUps('Meal List For Display');
			PopUpData();
			var grdArray = new Array();	
			jQuery("#listMeal").jqGrid({ 
				url:'showMeal!searchMeal.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Meal','Description', 'Meal Code','logicalCabinClass', 'Cabin Class', 'IATA', 'Status','version','createdBy','createdDate','modifiedBy','modifiedDate', 'mealId','mealsForDisplay','mealCategory','hasMealTemp','attachedCOS','channels'], 
				colModel:[ 	{name:'Id', width:40, jsonmap:'id'},   
						 	{name:'mealName',index:'mealName', width:180, jsonmap:'meal.mealName'}, 
						   	{name:'mealDescription',index:'mealDescription', width:360,  jsonmap:'meal.mealDescription'}, 
						   	{name:'mealCode', width:90, align:"center", jsonmap:'meal.mealCode'},
						   	{name:'logicalCabinClass',width:50, align:"center",hidden:true,jsonmap:'lccList'},
						   	{name:'cabinClassCode', width:75, align:"center",hidden:true,jsonmap:'ccList'},
						   	{name:'iataCode', width:70, align:"center",  jsonmap:'meal.iataCode'},
							{name:'status', width:125, align:"center",  jsonmap:'meal.status'},
							{name:'version', width:225, align:"center",hidden:true,  jsonmap:'meal.version'},						   							   	
						   	{name:'createdBy', width:225, align:"center",hidden:true,  jsonmap:'meal.createdBy'},
						   	{name:'createdDate', width:225, align:"center",hidden:true,  jsonmap:'meal.createdDate'},
						   	{name:'modifiedBy', width:225, align:"center",hidden:true,  jsonmap:'meal.modifiedBy'},
						   	{name:'modifiedDate', width:225, align:"center",hidden:true,  jsonmap:'meal.modifiedDate'},
							{name:'mealId', width:225, align:"center",hidden:true,  jsonmap:'meal.mealId'},
						   	{name:'mealForDisplay', width:225, align:"center",hidden:true},
							{name:'mealCategory', width:225, align:"center",hidden:true, jsonmap:'meal.mealCategoryID'},
						   	{name:'hasMealTemp', width:70, align:"center",hidden:true, jsonmap:'hasMealTemp'},
						   	{name:'attachedCOS', width:30, align:"center", hidden:true, jsonmap:'attachedCOS'},
						   	{name:'channels', width:30, align:"center", hidden:true, jsonmap:'channels'}
						   
						  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#mealpager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				onSelectRow: function(rowid){
						$("#rowNo").val(rowid);
						$('#dvUpLoad').html($('#dvUpLoad').html());
						$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
						fillForm(rowid);
						disableControls(true);
						enableSearch();
						disableMealButton();			
						enableGridButtons();
						enablePopupControls();
						setMealForDisplayList();
			}}).navGrid("#mealpager",{refresh: true, edit: false, add: false, del: false, search: true});
			
			var selectedCOS = "";
			var mealAttachedCOS = "";
			var selectedCOSArr = null;
			
			var options = {
				cache: false,	//Fixed JIRA : 3042 - Lalanthi
				beforeSubmit:  showRequest,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback			 
				dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)  
			}; 

			var delOptions = {	
				cache: false, //Fixed JIRA : 3042 - Lalanthi
				beforeSubmit:  showDelete,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback 				 
				url: 'showMeal!deleteMeal.action',         // override for form's 'action' attribute 
				dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
				resetForm: true        // reset the form after successful submit 
				 
			 }; 

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 
			    top[2].ShowProgress();
			    return true; 
			} 
			
			function showResponse(responseText, statusText)  {
				top[2].HideProgress();
				//refresh meal code select list with newest data AARESAA-3399
				$("#selMealCode").html('<option value="">All</option>'+responseText['selectListData']);
				showCommonError(responseText['msgType'], responseText['succesMsg']);
			    if(responseText['msgType'] != "Error") {
			    	$('#frmMeal').clearForm();
			    	$("#imgMeal").attr('src', '');
			    	jQuery("#listMeal").trigger("reloadGrid");
			    		disableMealButton();
			    	 $("#btnAdd").attr('disabled', false); 
			    	$("#btnSave").disableButton();
			    	disableForm(true);
			    	$("#status").val('INA');
			    	disableMealButton();
			    	$("#dvUpLoad").attr("disabled", true);
			    	$("#dvUpLoadThum").attr("disabled", true);
			    	$("#mealImage").attr('disabled',true);
			    	$("#mealThumbnailImage").attr('disabled',true);
			    	alert(responseText['succesMsg']);
			    }
			    
			} 			

			function showDelete(formData, jqForm, options) {				
			    return confirm(deleteRecoredCfrm); 
			} 
			
			function disableForm(cond){
				$("#mealName").attr('disabled',cond);
				$("#mealDescription").attr('disabled',cond);
				$("#mealCode").attr('disabled',cond);
				$("#iataCode").attr('disabled',cond);
				$("#cos").attr('disabled',cond);
				$("#mealCategory").attr('disabled',cond); 	
				$("#btnSave").attr('disabled',cond); 	
			};
			
			function validateMeal(){
				
				$("#mealCos").val("");
				var isSelectCOS = false;
				if(	trim($("#mealName").val()) == "") {			    	
			    	showCommonError("Error", mealNameRqrd);
			    	return false; 
			    }
			    if(	trim($("#mealDescription").val()) == "") {			    
			    	showCommonError("Error", descriptionRqrd);
			    	return false; 
			    } 
			    if(	trim($("#mealCode").val()) == "") {			    	
			    	showCommonError("Error", mealCodeRqrd);
			    	return false; 
			    } 
			    if(	trim($("#iataCode").val()) == "-1" || trim($("#iataCode").val()) == "") {
			    	showCommonError("Error", "IATA Code Required");
			    	return false; 
			    } 
			    if(	trim($("#mealCategory").val()) == "-1" || trim($("#mealCategory").val()) == "") {
			    	showCommonError("Error", "Meal Category Code Required");
			    	return false; 
			    } 
			    $('#cos option').each(function(i, option){ 
					var selVal = $(this).val();
					if($('#cos option[value$="' + selVal + '"]').attr('selected')){
						isSelectCOS = true;
					}					
			    });
			    if(isSelectCOS == false){
			    	showCommonError("Error", "Class of Service Required");
			    	return false;
			    }
			    var cosTOSave = "";
			    var found = false;
			    $('#cos option').each(function(i, option){ 
					var selVal = $(this).val();
					if($('#cos option[value$="' + selVal + '"]').attr('selected') ){
						if(found){
							cosTOSave += ",";
						}
						cosTOSave += selVal;
						found = true;
					}					
			    });
			    $("#mealCos").val(cosTOSave);
			    
			    
			    return true;
			}
			$('#btnSave').click(function() {
				disableControls(false);
				$('#dvUpLoad').html($('#dvUpLoad').html());
				$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				if($("#status").attr('checked')){
					$("#status").val('ACT');
				}else {
					$("#status").val('INA');
				}
				$("#mealsForDisplay").val(buildMealForDisplayStr());
				if(getText("hdnModel") == 'ADD') {
					setField("hdnMode","ADD");
				}
				if(validateMeal()){
					alert("Saving meal changes will update meals on all flights. This will take some time");				
					$('#frmMeal').ajaxSubmit(options);
					return true;
				}
				return false;	
			});
			 
			$('#btnAdd').click(function() {
				disableControls(false);
				$('#frmMeal').clearForm();		
				setEnableCOSList();
				$("#imgMeal").attr('src', '');	
				$("#imgMealThamb").attr('src', '');
				$("#version").val('-1');
				$("#rowNo").val('');
				$('#dvUpLoad').html($('#dvUpLoad').html());
				$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				$('#btnEdit').disableButton();
				$('#btnDelete').disableButton();
				$('#btnUpload').enableButton();
				$('#btnUploadThumbnail').enableButton();
				$("#btnSave").enableButton();
				setField("hdnMode","ADD");
				setField("hdnModel","ADD");				
			}); 	

			$("#cos").change(function () {				
				$("#cos option:selected").each(function () {
					var selVal = $(this).val();
					var selArr = selVal.split("_");
					if (selArr[0] == selArr[1]){
						$('#cos option[value!="' + selVal + '"][value$="_' + selArr[0] + '"]').attr('selected', false);
				    }else{
				    	$('#cos option[value!="' + selVal + '"][value$="' + selArr[0] + '_"]').attr('selected', false);
				    }
				});
			});
			
			$('#btnEdit').click(function() {				
				disableControls(false);	
				var mealTemp = jQuery("#listMeal").getCell($("#rowNo").val(),'hasMealTemp');
				setSelectedCOSList($("#rowNo").val());
				disableReferencedCOS();				
				$("#uploadImage").val('');
				$('#btnDelete').disableButton();
				$('#btnUpload').enableButton();
				$('#btnUploadThumbnail').enableButton();
				$("#btnSave").enableButton();
				setField("hdnMode","EDIT");
				 $('#mealCode').attr('disabled', true); 
			}); 
			
			$('#btnReset').click(function() {
				if($("#rowNo").val() == '') {
					$('#frmMeal').resetForm();
					$("#imgMeal").attr('src', '');
					$('#dvUpLoad').html($('#dvUpLoad').html());
					$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				}else {
					
					fillForm($("#rowNo").val());
				}
					
			});
			$('#btnDelete').click(function() {
				disableControls(false);
				$('#dvUpLoad').html($('#dvUpLoad').html());	
				$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				$('#frmMeal').ajaxSubmit(delOptions);
				disableForm(true);
				return false;			
			}); 
			
			$('#btnSearch').click(function() {
				//var newUrl = "showMeal!searchMeal.action?srchMealName="+$("#srchMealName").val() +"&srchMealCode="+$("#selMealCode").val()+"&srchStatus="+$("#selStatus").val();
				var newUrl = 'showMeal!searchMeal.action?';
			 	newUrl += 'mealSrch.mealName=' + $("#srchMealName").val();
			 	newUrl += '&mealSrch.mealCode=' + $("#selMealCode").val();
			 	newUrl += '&mealSrch.status=' + $("#selStatus").val();
			 	newUrl += '&mealSrch.classOfService=' + $("#selCOS").val();
			 	newUrl += '&mealSrch.iataCode=' + $("#selIATACode").val();
			 	
			 	$("#listMeal").clearGridData();
			 	$.getJSON(newUrl, function(response){
					$("#listMeal")[0].addJSONData(response);
				
			 	});				
			});

			function fillForm(rowid) {
				setField("hdnMode","");
				$("#mealCos").val("");
				jQuery("#listMeal").GridToForm(rowid, '#frmMeal');					
				if(jQuery("#listMeal").getCell(rowid,'status') == 'ACT'){
					$("#status").attr('checked', true);
				}else {
					$("#status").attr('checked', false);
				}
				setSelectedCOSList(rowid);
				setChannelList(rowid);
			
				var dbImageID = jQuery("#listMeal").getCell(rowid,'mealId');
				//No need to add the no_cache for meals since they are uploaded on runtime
				$("#imgMeal").attr('src', mlImagePath+'meal_'+dbImageID+'.jpg?rel='+new Date());  
				$("#imgMealThamb").attr('src', mlImagePath+'meal_Thumbnail_'+dbImageID+'.jpg?rel='+new Date()); 						
				enableGridButtons();
			}
			
			function disableReferencedCOS(){				
				var selectedCOSArr = selectedCOS.split(",");
				$('#cos option').each(function(i, option){ 
					var selVal = $(this).val();
					for(var i = 0 ;i< selectedCOSArr.length;i++){
						if(selectedCOSArr[i] == selVal.split("_")[0]){
							$('#cos option[value$="' + selVal + '"]').attr('disabled', true);
							break;
						} else {
							$('#cos option[value$="' + selVal + '"]').attr('disabled', false);
						}
					}
				});
			}
			function setEnableCOSList(){
				$('#cos option').each(function(i, option){ 
					$(this).attr('disabled', false);
				});
			}
			
			function setChannelList(rowid){
				var channels = jQuery("#listMeal").getCell(rowid,'channels');
				var channelsArr = channels.split(",");
				var channel = "";
				$("#chkIBE").attr('checked', false);	
				$("#chkXBE").attr('checked', false);	
				$("#chkWS").attr('checked', false);
				$("#chkIOS").attr('checked', false);
				$("#chkAndriod").attr('checked', false);
				
				for(var cl=0;cl< channelsArr.length;cl++){
					channel = channelsArr[cl];
					if(channel ==='4') {
						$("#chkIBE").attr('checked', true);						
					}
					if(channel ==='3') {
						$("#chkXBE").attr('checked', true);						
					}
					if(channel ==='12') {
						$("#chkWS").attr('checked', true);						
					}
					if(channel ==='20') {
						$("#chkIOS").attr('checked', true);						
					}
					if(channel ==='21') {
						$("#chkAndriod").attr('checked', true);						
					}
					
				}
			}
			
			function setSelectedCOSList(rowid){
				$('#cos option').each(function(i, option){ 
					var selVal = $(this).val();
					$('#cos option[value$="' + selVal + '"]').attr('selected', false);
				});
				var cabinClass = "";
				var logocalCC = "";
				mealAttachedCOS = new Array();
				var cc = jQuery("#listMeal").getCell(rowid,'cabinClassCode');
				var lcc = jQuery("#listMeal").getCell(rowid,'logicalCabinClass');
				selectedCOS = jQuery("#listMeal").getCell(rowid,'attachedCOS');
				mealAttachedCOS = selectedCOS.split(",");
				
				if(cc != ""){
					cabinClass = cc.split(",");
				}
				if(lcc != ""){
					logocalCC = lcc.split(",");
				}
				var selectedCC = "";
				$('#cos option').each(function(i, option){ 
					var selVal = $(this).val();
					var selArr = selVal.split("_");
					if(lcc != ""){
						for(var j = 0;j < logocalCC.length;j++){
							if (trim(selArr[0]) == trim(logocalCC[j])){
								$('#cos option[value$="' + selVal + '"]').attr('selected', true);
								for(var i = 0;i < mealAttachedCOS.length;i++){
									if(mealAttachedCOS[i] == logocalCC[j]){
										selectedCC = selArr[1];
										selectedCOS += "," + selArr[1];
									}
								}								
						    }
						}
						if(selectedCC == selArr[1]){
							selectedCOS += "," + selArr[1];
						}
					}
					if(cc != ""){
						for(var j = 0;j < cabinClass.length;j++){
							if (trim(selArr[0]) == trim(cabinClass[j])){
								selectedCC = selArr[1];
								$('#cos option[value$="' + selVal + '"]').attr('selected', true);
								for(var i = 0;i < mealAttachedCOS.length;i++){
									if(mealAttachedCOS[i] == logocalCC[j]){
										selectedCOS += selArr[0] + ",";
									}
								}								
						    }
						}
					}
				});
			}
			
			function  disableControls(cond){
				$("input").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
				      $(this).attr('disabled', cond); 
				});
				$("file").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("#btnClose").enableButton();
				
				if (listForDisplayEnabled) {
					$("#btnMealFD").enableButton();
				} else {
					$('#btnMealFD').attr("disabled", true); 
				}
			}
			
			function enableSearch(){
				$('#selMealCode').attr("disabled", false); 
				$('#srchMealName').attr("disabled", false); 
				$('#selStatus').attr("disabled", false);
				$('#selCOS').attr("disabled", false); 
				$('#selIATACode').attr("disabled", false);
				$('#btnSearch').enableButton();
			}
			
			function disableMealButton(){
				$("#btnEdit").disableButton();
				$("#btnDelete").disableButton();
				$("#btnReset").disableButton();
				$("#btnSave").disableButton();
				$('#btnUpload').disableButton();
				$('#btnUploadThumbnail').disableButton();
			}

			function enableGridButtons(){
				$("#btnAdd").enableButton();
				$("#btnEdit").enableButton();
				$("#btnDelete").enableButton();
				$("#btnReset").enableButton();
				if (listForDisplayEnabled) {
					$("#btnMealFD").enableButton();
				} else {
					$('#btnMealFD').attr("disabled", true); 
				}
				
			}
			function enablePopupControls(){
				$("#language").enableButton();
				$("#add").enableButton();
				$("#remove").enableButton();
				$("#translation").enableButton();
				$("#txtMealForDisplayOL").enableButton();
				//For Jira 4640
				$("#txtMealTitleForDisplayOL").enableButton();				
				$("#btnUpdate").enableButton();
				$("#btnPopupClose").enableButton();
			}
			disableControls(true);
			$("#btnAdd").enableButton();
			$('#selMealCode').attr("disabled", false); 
			$('#selCOS').attr("disabled", false); 
			$('#selIATACode').attr("disabled", false); 
			$('#srchMealName').attr("disabled", false); 
			$('#selStatus').attr("disabled", false); 
			$('#btnSearch').enableButton();
			
			
			var upOptionsImage = {				 
				beforeSubmit:  showUpdate,  // pre-submit callback - this can be used to do the validation part 
				success: showUploadResponseImage,   // post-submit callback 				 
				url: 'showUpload.action?mode=mealImage',         // override for form's 'action' attribute 
				dataType:  'script'        // 'xml', 'script', or 'json' (expected server response type)				 
			 };
			
			var upOptionsThumbnailImage = {				 
					beforeSubmit:  showUpdate,  // pre-submit callback - this can be used to do the validation part 
					success: showUploadResponseThumbnail,   // post-submit callback 				 
					url: 'showUpload.action?mode=mealThumbnailImage',         // override for form's 'action' attribute 
					dataType:  'script'        // 'xml', 'script', or 'json' (expected server response type)				 
				 };

			function showUpdate(formData, jqForm, options) {			
				var newImg = new Image();
				newImg.src = $("#imgMeal").attr('src');				
				var height = newImg.height;
				var width = newImg.width;
				
				if(width == 1024 || height == 700){
					showCommonError("Error", "Image is too large ");
					return false;
				}
				top[2].ShowProgress();
			} 
		
		$('#btnUpload').click(function() {
			$('#frmMeal').ajaxSubmitWithFileInputs(upOptionsImage);
			return false;	
		});
		
		$('#btnUploadThumbnail').click(function() {
			$('#frmMeal').ajaxSubmitWithFileInputs(upOptionsThumbnailImage);
			return false;	
		});

		function showUploadResponseImage(responseText, statusText)  {
			top[2].HideProgress();
		    eval(responseText);		    		
		    showCommonError(responseText['msgType'], responseText['succesMsg']);
		    if(responseText['msgType'] != "Error"){		    	
		    	$('#dvUpLoad').html($('#dvUpLoad').html());
		    	$("#uploadImage").val('UPLOADED');	
		    	//No need to add the no_cache for meals since they are uploaded on runtime
		    	$("#imgMeal").attr('src', mlImagePath+'mealImage_test.jpg?rel='+new Date());		    	
		    }	         
		    
		} 
		
		function showUploadResponseThumbnail(responseText, statusText)  {
			top[2].HideProgress();
		    eval(responseText);		    		
		    showCommonError(responseText['msgType'], responseText['succesMsg']);
		    if(responseText['msgType'] != "Error"){		    	
		    	$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
		    	$("#uploadImageThumbnail").val('UPLOADED');	
		    	//No need to add the no_cache for meals since they are uploaded on runtime
		    	$("#imgMealThamb").attr('src', mlImagePath+'mealThumbnailImage_test.jpg?rel='+new Date());		    	
		    }		    
		    
		} 
			
		});	
		
		function setPageEdited(isEdited) {
			top[1].objTMenu.tabPageEdited(screenId, isEdited);
		}	
		
		// Dhanya
		//For Jira 4640 
		function PopUpData(){
			var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Meal Description For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
				html += '<select name="language" id="language"  style="width:150px; "> ';
				html += '<option value="-1"></option>'+languageHTML;				
				html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addMealForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeMealForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>Meal Description* </font></td> <td align="left" valign="top"><input name="txtMealForDisplayOL" type="text" id="txtMealForDisplayOL" size="32" maxlength="125" /></td> </tr> '; 
				html += '<tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>Meal Title* </font></td> <td valign="top"><input name="txtMealTitleForDisplayOL" type="text" id="txtMealTitleForDisplayOL" size="32" maxlength="125" /></td> </tr>'; 
				html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveMealForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

			DivWrite("spnPopupData",html);
		}

		function POSenable(){			
			if(!listForDisplayEnabled)
				return;
			if(getFieldByID("mealDescription").value=="" || getFieldByID("mealDescription").value==null){
					alert("No meal is selected, please select meal and try again");
					return;
				}
			
			if(getValue('hdnModel') == 'ADD'){
				var mealList = getFieldByID("translation");
				mealList.options.length = 0;
				setField("txtMealForDisplayOL", "");
				setField("txtMealTitleForDisplayOL", "");
				setField("language","-1");
			}
			
			setField("hdnMode", "SAVELISTFORDISP");
			//setField("hdnModel", "SAVELISTFORDISP");
			//setMealForDisplayList();
			showPopUp(1);
		}

		function POSdisable(bFromClose){			
			if(!listForDisplayEnabled)
				return;
			if(bFromClose){
				//setField("hdnModel","");
				setField("hdnMode","");
			}
			 hidePopUp(1);
		}


		function addMealForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			if(getValue('txtMealForDisplayOL').length ==0 || getValue('txtMealTitleForDisplayOL').length ==0){
				return false;
			}
			var selLang = getValue("language");
			if(selLang == '-1')
				return false;
			if(findValueInList(selLang, "translation") == true){
				alert("Meal description for the selected language already exist");
				return false;
			}

			var selLangName = getCurSelectedLabel("language");
			var mealDesc = getValue("txtMealForDisplayOL");
			var mealTitle = getValue("txtMealTitleForDisplayOL");
			addToList("translation", selLangName+"=>"+mealDesc+"=>"+mealTitle, selLang);	
			setField("txtMealForDisplayOL", "");
			setField("txtMealTitleForDisplayOL", "");
			setField("language","-1");

		}
		function removeMealForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			var mealList = getFieldByID("translation");
			var selected = mealList.selectedIndex;
			if(selected != -1){
				var langCode = mealList.options[selected].value;
				var label = mealList.options[selected].text.split("=>");
				setField("language", langCode);
				setField("txtMealForDisplayOL", label[1]);
				setField("txtMealTitleForDisplayOL", label[2]);
				mealList.remove(selected);
			}
		}
		function saveMealForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			if(getValue('txtMealForDisplayOL').length>0 && getValue('txtMealTitleForDisplayOL').length>0 && getValue("language")!='-1'){
				if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
					return false;
			}
			var strMode = getText("hdnMode");
			var mealList = getFieldByID("translation");
			var size = mealList.length;
			var langCode;
			var mealName;
			var mealTitle;
			var label;
			var str="";
			var size;
			var i=0;
			var j=0;
			var newData = new Array();
			var count = 0;
			//////////
			var mealTranslations = jQuery("#listMeal").getCell($("#rowNo").val(),'mealForDisplay');
			var arrMealTrl = new Array();
			if(mealTranslations != null && mealTranslations != "")
				arrMealTrl = mealTranslations.split("~");
			var mealTrl;
			grdArray = new Array();
			for(var i =0;i<arrMealTrl.length-1;i++){
				grdArray[i] = new Array(); 
				mealTrl = arrMealTrl[i].split(",");
				grdArray[i][0] = mealTrl[0];
				grdArray[i][1] = mealTrl[1];
				grdArray[i][2] = mealTrl[2];
				grdArray[i][3] = mealTrl[3];
				grdArray[i][4] = mealTrl[4];
			//	grdArray[i][5] = mealTrl[5];
			}
			////////////
			for(i=0;i<mealList.length;i++){
				langCode = mealList.options[i].value;
				label = mealList.options[i].text.split("=>");
				mealName = label[1];
				if (label.length > 2)
					mealTitle = label[2];
				else
					mealTitle = getFieldByID("mealName");;
				size = grdArray.length;
				var found = false;
				for(j=0;j<size;j++){
					if(grdArray[j][0] == langCode){
						newData[count++] = new Array(langCode, mealName, grdArray[j][2], grdArray[j][3],grdArray[j][4], mealTitle);
						found = true;
						break;
					}
				}
				if(!found){
					newData[count++] = new Array(langCode, mealName, "-1", "-1", "-1", mealTitle);//langCode,mealName,id,ver, mealtitle
					//arrData[strGridRow][16][size] = new Array(langCode,mealName,'-1','-1');//langCode,mealName,id,ver
				}
			}
			var langCode2;
			var size1 = grdArray.length;
			var size2 = newData.length;
			for(i=0;i<size1;i++){//original list
				found = false;
				for(j=0;j<size2;j++){//new data
					langCode = grdArray[i][0];
					langCode2 = newData[j][0];
					if(langCode == langCode2){//the lang code exist in the new data
						found = true;
					}
				}
				if(!found){
					newData[newData.length] = new Array( grdArray[i][0],"null", grdArray[i][2],grdArray[i][3],grdArray[i][4], mealTitle);
				}
			}
			grdArray = newData;
			if(validateMealForDisplay() == false){
				return false;
			}
			$('#btnSave').click();
			return POSdisable(false);
		}
		
		function setMealForDisplayList(){			
			if(!listForDisplayEnabled)
				return;

			if(jQuery("#listMeal").getCell($("#rowNo").val())>=0 ){
				setField("txtMealForDisplayOL", "");
				setField("language","-1");
				setField("txtMealTitleForDisplayOL", "");
			
				var mealList = getFieldByID("translation");				
				mealList.options.length = 0;
				var langCode ;
				var mealName ;
				var langName;
				var mealTitle;
				var i=0;
				
				var mealTranslations = jQuery("#listMeal").getCell($("#rowNo").val(),'mealForDisplay');
				var arrMealTrl = new Array();
				if(mealTranslations != null && mealTranslations != "")
					arrMealTrl = mealTranslations.split("~");
				var mealTrl;
				grdArray = new Array();
				for(var i =0;i<arrMealTrl.length-1;i++){
					grdArray[i] = new Array(); 
					mealTrl = arrMealTrl[i].split(",");
					grdArray[i][0] = mealTrl[0];
					grdArray[i][1] = mealTrl[1].replace("\^",",");
					grdArray[i][2] = mealTrl[2];
					grdArray[i][3] = mealTrl[3];
					grdArray[i][4] = mealTrl[4];
					grdArray[i][5] = mealTrl[5];
				}
				for(i=0;i<grdArray.length;i++){
					langCode = grdArray[i][0];
					mealName = grdArray[i][1];
					mealTitle = grdArray[i][5];
					if(mealName == "null")
						continue;
					langName = getListLabel("language", langCode);
					addToList("translation", langName+"=>"+mealName+"=>"+mealTitle, langCode);
				}
			}
		}
		function validateMealForDisplay(){
			if(!listForDisplayEnabled)
				return;
			var strMode = getText("hdnMode");
			var size2=getFieldByID("language").length;
			var valid = true;
			var size1;
			var i;
			var j;
			var langCode;
			var mealName;
			var found = false;
			if(strMode == "ADD")
				valid = false;
			else{
				size1 = grdArray.length;
				if(size1 != (size2-1))//-1 to ignore the first empty item
					valid = false;
				else{
					for(i=0;i<size1;i++){//original list
						mealName = grdArray[i][1];
						if(mealName == "null"){
							valid = false;
							break;
						}
					}
				}
			}
			if(!valid){
				alert("Meal description for display is missing for all or some languages, original meal description will copied to all languages") ;
				if(strMode != "ADD"){
						var descr = getValue("mealDescription");
						var title = getValue("mealName");
						for(i=1;i<size2;i++){//lang  list, start from 1 to ignore the first item (empty)
							found = false;
							langCode = getFieldByID("language").options[i].value;
							for(j=0;j<size1;j++){
								mealName = grdArray[j][1];
								if(mealName == "null"){
									grdArray[j][1] = descr;
									grdArray[j][5] = title;
								}
								if(langCode == grdArray[j][0]){
									found = true;
									continue;
								}
							}
							if(!found)
								grdArray[grdArray.length] = new Array(langCode,descr,"-1","-1","-1",title);
						}
				}
			}
			return true;
			}
		
		
		function buildMealForDisplayStr(){			
			var strMode = getText("hdnMode");
			var mealStr = "";
			
			if(strMode == "ADD"){					
				mealDesc = getUnicode(getValue("mealDescription"));
				mealTitle = getUnicode(getValue("mealName"));
				
				var size2=getFieldByID("language").length;
				for(i=1;i<size2;i++){
					langCode = getFieldByID("language").options[i].value;
					if(mealStr.length>0)
						mealStr += "~";
					mealStr=mealStr + langCode+","+mealDesc+",-1,-1,-1"+","+mealTitle;
				}
			}else {
				prepareMealToSave();
				if(grdArray != null){					
					for(var j=0;j<grdArray.length;j++){
						mealStr += grdArray[j][0]+",";
						mealStr += getUnicode(grdArray[j][1])+",";
						mealStr += grdArray[j][2]+",";
						mealStr += grdArray[j][3]+",";
						mealStr += grdArray[j][4]+",";
						mealStr += getUnicode(grdArray[j][5])+",";
						mealStr += "~";	
				   	}
				}	
			}			
			return mealStr;
		}
		
		function prepareMealToSave(){
			var strMode = getText("hdnMode");
			var mealList = getFieldByID("translation");
			var size = mealList.length;
			var langCode;
			var mealName;
			var mealTitle;
			var label;
			var str="";
			var size;
			var i=0;
			var j=0;
			var newData = new Array();
			var count = 0;
			//////////
			var mealTranslations = jQuery("#listMeal").getCell($("#rowNo").val(),'mealForDisplay');
			var arrMealTrl = new Array();
			if(mealTranslations != null && mealTranslations != "")
				arrMealTrl = mealTranslations.split("~");
			var mealTrl;
			grdArray = new Array();
			for(var i =0;i<arrMealTrl.length-1;i++){
				grdArray[i] = new Array(); 
				mealTrl = arrMealTrl[i].split(",");
				grdArray[i][0] = mealTrl[0];
				grdArray[i][1] = mealTrl[1];
				grdArray[i][2] = mealTrl[2];
				grdArray[i][3] = mealTrl[3];
				grdArray[i][4] = mealTrl[4];				
			}
			////////////
			for(i=0;i<mealList.length;i++){
				langCode = mealList.options[i].value;
				label = mealList.options[i].text.split("=>");
				mealName = label[1];
				if (label.length > 2 && label[2]!="")
					mealTitle = label[2];
				else
					mealTitle = getValue("mealName");
				size = grdArray.length;
				var found = false;
				for(j=0;j<size;j++){
					if(grdArray[j][0] == langCode){
						newData[count++] = new Array(langCode, mealName, grdArray[j][2], grdArray[j][3],grdArray[j][4], mealTitle);
						found = true;
						break;
					}
				}
				if(!found){
					newData[count++] = new Array(langCode, mealName,  "-1", "-1", "-1", mealTitle);//langCode,mealName,id,ver, meal title
					//arrData[strGridRow][16][size] = new Array(langCode,mealName,'-1','-1');//langCode,mealName,id,ver
				}
			}
			var langCode2;
			var size1 = grdArray.length;
			var size2 = newData.length;
			for(i=0;i<size1;i++){//original list
				found = false;
				for(j=0;j<size2;j++){//new data
					langCode = grdArray[i][0];
					langCode2 = newData[j][0];
					if(langCode == langCode2){//the lang code exist in the new data
						found = true;
					}
				}
				if(!found){
					newData[newData.length] = new Array( grdArray[i][0],"null", grdArray[i][2],grdArray[i][3],grdArray[i][4], "null");
				}
			}
			grdArray = newData;		
			
		}