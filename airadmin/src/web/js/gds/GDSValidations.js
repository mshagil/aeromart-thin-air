/*
 * Author : Haider 13Oct08
 */

var strRowData;
var strGridRow;
var intLastRec;

var screenId = "SC_ADMN_026";
var valueSeperator = "~";

function validateGDS() {
	if (validateTextField(getValue("txtGDSCode"), gdsIdRqrd)) {
		getFieldByID("txtGDSCode").focus();
		return false;
	}
	if (getFieldByID("hdnMode").value == "ADD") {
		var gdsCode = trim(getFieldByID("txtGDSCode").value).toUpperCase();
		for ( var i = 0; i < arrData.length; i++) {
			if (arrData[i][1] == gdsCode) {
				showCommonError("Error", gdsIdExist);
				getFieldByID("txtGDSCode").focus();
				return false;
			}
		}
	}

	if (validateTextField(getValue("txtDesc"), gdsDescRqrd)) {
		getFieldByID("txtDesc").focus();
		return false;
	}

	var pubMech = getValue("selPublishMechanism");
	if (pubMech == "-1") {
		showCommonError("Error", gdsPubMechRequired);
		getFieldByID("selPublishMechanism").focus();
		return false;

	}

	return true;
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	disableInputControls();
	clearGDS();
	setPageEdited(false);
	if (top[1].objTMenu.focusTab == screenId) {
		buttonSetFocus("btnAdd");
	}
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);

	Disable("btnEdit", true);
	Disable("btnSSMRecap", true);
	Disable("btnPublishRoutes", true);
	
	if (arrFormData != null && arrFormData.length > 0) {
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setPageEdited(true);
		setField("txtGDSCode", arrFormData[0][1]);
		setField("txtDesc", arrFormData[0][2]);
		setField("txtRemarks", arrFormData[0][3]);
		setField("selPublishMechanism", arrFormData[0][5]);
		setField("hdnVersion", arrFormData[0][6]);
		setField("hdnGDSId", arrFormData[0][7]);
		setField("txtAVSAvailThreshold", arrFormData[0][8]);
		setField("txtAVSClosureThreshold", arrFormData[0][9]);
		setField("typeAVersion", arrFormData[0][11]);
		setField("typeASenderId", arrFormData[0][12]);
		setField("typeASyntaxId", arrFormData[0][13]);
		setField("typeASyntaxVersionNumber", arrFormData[0][14]);
		setField("typeAControllingAgency", arrFormData[0][15]);
		
		
		if (arrFormData[0][4] == "on"){
			getFieldByID("chkStatus").checked = true;
		}
		else{
			getFieldByID("chkStatus").checked = false;
		}
					
		if (arrFormData[0][10] == "on"){
			getFieldByID("chkEnableNAVS").checked = true;
		}
		else {
			getFieldByID("chkEnableNAVS").checked = false;
		}		
		if (arrFormData[0][16] == "on"){
			getFieldByID("airlineResModifiable").checked = true;
		}
		else {
			getFieldByID("airlineResModifiable").checked = false;
		}

		enableInputControls();

		if (isAddMode) {
			setField("hdnMode", "ADD");
			Disable("txtGDSCode", false);
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtGDSCode").focus();
			}
		} else {
			setField("hdnMode", "EDIT");
			Disable("txtGDSCode", true);
		}
	}

	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");
		}
	}
}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;

		setField("txtGDSCode", strRowData[0]);
		setField("txtDesc", trim(strRowData[1]));
		setField("txtRemarks", trim(strRowData[2]));
		setField("selPublishMechanism", trim(strRowData[4]));
		setField("hdnVersion", arrData[strRowNo][6]);
		setField("hdnGDSId", arrData[strRowNo][7]);
		setField("txtAVSAvailThreshold", arrData[strRowNo][8]);
		setField("txtAVSClosureThreshold", arrData[strRowNo][9]);		
		setField("typeAVersion", arrData[strRowNo][11]);
		setField("typeASenderId", arrData[strRowNo][12]);
		setField("typeASyntaxId", arrData[strRowNo][13]);
		setField("typeASyntaxVersionNumber", arrData[strRowNo][14]);
		setField("typeAControllingAgency", arrData[strRowNo][15]);

		if (strRowData[3] == "Active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;
		
		if (arrData[strRowNo][10] == "Y") {
			getFieldByID("chkEnableNAVS").checked = true;
		}
		else{
			getFieldByID("chkEnableNAVS").checked = false;
		} 
		if (arrData[strRowNo][16] == "Y") {
			getFieldByID("airlineResModifiable").checked = true;
		}
		else{
			getFieldByID("airlineResModifiable").checked = false;
		} 
		
		setPageEdited(false);
		disableInputControls();
		Disable("btnEdit", false);
		var isSchedulingSystem = arrData[strRowNo][17];
		if(isSchedulingSystem === "false"){
			Disable("btnSSMRecap", false);
			Disable("btnPublishRoutes", false);
		}else{
			Disable("btnSSMRecap", true);
			Disable("btnPublishRoutes", true);
		}
		
	}
}

function enableInputControls() {
	Disable("txtGDSCode", false);
	Disable("txtDesc", false);
	Disable("txtRemarks", false);
	Disable("btnSave", false);
	Disable("btnReset", false);
	Disable("selPublishMechanism", false);
	Disable("chkStatus", false);
	Disable("chkEnableNAVS", false);
	Disable("typeAVersion", false);
	Disable("typeASenderId", false);
	Disable("typeASyntaxId", false);
	Disable("typeASyntaxVersionNumber", false);
	Disable("typeAControllingAgency", false);
	Disable("airlineResModifiable", false);
	
}selectedGdsIDForPublish

function disableInputControls() {
	Disable("txtGDSCode", true);
	Disable("txtDesc", true);
	Disable("txtRemarks", true);
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("selPublishMechanism", true);
	Disable("chkStatus", true);
    Disable("chkEnableNAVS", true);
	Disable("typeAVersion", true);
	Disable("typeASenderId", true);
	Disable("typeASyntaxId", true);
	Disable("typeASyntaxVersionNumber", true);
	Disable("typeAControllingAgency", true);
	Disable("airlineResModifiable", true);
}

function clearGDS() {
	setField("hdnVersion", "");
	setField("txtGDSCode", "");
	setField("txtDesc", "");
	setField("selPublishMechanism", "");
	setField("txtRemarks", "");
	setField("hdnGDSId", "");
	setField("txtAVSAvailThreshold", "");
	setField("txtAVSClosureThreshold", "");
	setField("typeAVersion", "");
	setField("typeASenderId", "");
	setField("typeASyntaxId", "");
	setField("typeASyntaxVersionNumber", "");
	setField("typeAControllingAgency", "");
	
	

}

function saveGDS() {
	if (!validateGDS())
		return;

	var tempName = trim(getText("txtRemarks"));
	tempName = replaceall(tempName, "'", "`");
	tempName = replaceEnter(tempName);
	setField("txtRemarks", tempName);
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	Disable("txtGDSCode", false);
	document.forms[0].submit();
}

function resetGDS() {
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearGDS();
		getFieldByID("txtGDSCode").focus();
		getFieldByID("chkStatus").checked = true;
	}
	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtGDSCode")) {
			getFieldByID("txtDesc").focus();
			setField("txtGDSCode", arrData[strGridRow][1]);
			setField("txtDesc", arrData[strGridRow][2]);
			setField("txtRemarks", arrData[strGridRow][3]);
			setField("selPublishMechanism", arrData[strGridRow][5]);
			setField("txtAVSAvailThreshold", arrData[strGridRow][8]);
			setField("txtAVSClosureThreshold", arrData[strGridRow][9]);
			setField("typeAVersion", arrData[strGridRow][11]);
			setField("typeASenderId", arrData[strGridRow][12]);
			setField("typeASyntaxId", arrData[strGridRow][13]);
			setField("typeASyntaxVersionNumber", arrData[strGridRow][14]);
			setField("typeAControllingAgency", arrData[strGridRow][15]);

			if (arrData[strGridRow][4] == "Active"){
				getFieldByID("chkStatus").checked = true;
			}
			else{
				getFieldByID("chkStatus").checked = false;
			}
			
			if (arrData[strGridRow][10] == "Y") {
				getFieldByID("chkEnableNAVS").checked = true;
			}
			else{
				getFieldByID("chkEnableNAVS").checked = false;
			} 
			if (arrData[strGridRow][16] == "Y") {
				getFieldByID("airlineResModifiable").checked = true;
			}
			else{
				getFieldByID("airlineResModifiable").checked = false;
			} 
				
			setField("hdnVersion", arrData[strGridRow][6]);
			setField("hdnGDSId", arrData[strGridRow][7]);
		} else {
			// Clear the data entry area
			disableInputControls();
			clearGDS();

			buttonSetFocus("btnAdd");
			Disable("btnEdit", true);
		}
	}
	setPageEdited(false);
	objOnFocus();
}

function updateGDS() {
	if (!validateGDS())
		return;

	document.forms[0].submit();

}

// on Add Button click
function addClick() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[2].HidePageMessage();
		setPageEdited(true);
		Disable("txtGDSCode", false);
		clearGDS();
		enableInputControls();
		setField("hdnMode", "ADD");
		getFieldByID("chkStatus").checked = "true";
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		getFieldByID("txtGDSCode").focus();
	}
}

function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

// on Edit Button CLick
function editClick() {
	objOnFocus();
	top[2].HidePageMessage();
	if (getFieldByID("txtGDSCode").value == ""
			|| getFieldByID("txtGDSCode").value == null) {
		showCommonError("Error", "Please select row to edit!");
		Disable("txtGDSCode", true);
		disableInputControls();
		return;

	}
	// setPageEdited(false);
	enableInputControls();
	setField("hdnMode", "EDIT");
	// Disable("txtGDSCode", true);
	getFieldByID("txtGDSCode").focus();
}

function CCPress() {
	var strCC = getText("txtGDSCode");
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField("txtGDSCode", strCC.substr(0, strLen - 1));
		getFieldByID("txtGDSCode").focus();
	}
}

function CDPress() {
	var strCD = getText("txtDesc");
	var strLen = strCD.length;
	var blnVal = isAlphaNumeric(strCD);
	if (!blnVal) {
		setField("txtDesc", strCD.substr(0, strLen - 1));
		getFieldByID("txtDesc").focus();
	}
}

function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

function valDescription(objTextBox) {
	setPageEdited(true);
	objOnFocus();
	commaValidate(objTextBox);
}

function Save(msg) {
	top[2].objMsg.MessageText = msg;
	top[2].objMsg.MessageType = "Confirmation";
	top[2].ShowPageMessage();
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		// top[0].intLastRec=intRecNo;
		if (intRecNo <= 0)
			intRecNo = 1;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
function pageOnChange() {
	top[2].HidePageMessage();
	setPageEdited(true);
}

function ssmRecapClick(){
	
	var selectedGdsCode = getFieldByID("txtGDSCode").value;
	var selectedGdsID = getFieldByID("hdnGDSId").value;
	var width = 600;
	var height = 400;
	var top = (window.screen.height - height) / 2 ;
	var left = (window.screen.width - width) / 2 ;	
	var properties = "toolbar=no, scrollbars=no, resizable=no, width="+ width + ", height=" + height + ", left=" + left + ", top=" + top;

	var ssmRecapPopUp = window.open("showSSMRecap.action?gdsCode=" + selectedGdsCode + "&gdsID="+selectedGdsID +"&hdnMode=VIEW", "_blank", properties);

}

function publishRoutesOnClick(){
	
	var selectedGdsCode = getFieldByID("txtGDSCode").value;
	var selectedGdsID = getFieldByID("hdnGDSId").value;
	selectedGdsIDForPublish = getFieldByID("hdnGDSId").value;
	var width = 600;
	var height = 350; //470
	var top = (window.screen.height - height) / 2 ;
	var left = (window.screen.width - width) / 2 ;	
	var properties = "toolbar=no, scrollbars=no, resizable=no, width="+ width + ", height=" + height + ", left=" + left + ", top=" + top;
	var gdsPublishRoutesPopUp = window.open("showPublishRoutes.action?gdsCode=" + selectedGdsCode + "&gdsID="+selectedGdsID +"&hdnMode=VIEW", "_blank", properties);

}


