/**
 * rajitha
 * 
 */

function UI_PublishRoutes() {
}

var dataMap = {
	ONDs : [ {
		"0" : "CMB",
		"1" : "Colombo"
	}, {
		"0" : "SHJ",
		"1" : "Sharjah"
	} ],
	Carriers : [ "G9" ],
	PublishedOndList : [ "SHJ/CMB" ],
	PublishedCarrierCodes : [ "G9" ],
	RouteWiseAutoPublish: [ "false" ],
}
var selOnds = '';
var ondArray = [];
var ccArray = [];
var selectedGdsId = '';

function HideProgress() {
	top[2].HideProgress();
}


function loadData(isReset) {

	objectToArray = function(obj) {
		rObj = [];
		$.each(obj, function(key, val) {
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}

	fillInitData = function() {
		
		var ondList = '';
		var carrierList = '';
		var carrierCodes ='';
		var soldCarriers = [];

		$("#selFrom, #selTo").fillDropDown({
			firstEmpty : false,
			dataArray : dataMap.ONDs,
			keyIndex : 0,
			valueIndex : 0
		});
		
		
		
		$.each(dataMap.PublishedCarrierCodes, function(key, value) {

			carrierList += '<option value="' + key + '">' + value + '</option>';
			soldCarriers.push(value);
		});

		$("#sel_Carrier").html(carrierList);
		
		
		
		$.each( dataMap.Carriers, function(key, value) {
			
			if(!($.inArray(value, soldCarriers) > -1)){
				
				carrierCodes += '<option value="' + key + '">' + value + '</option>';
			}

			
		});

		$("#carrierCodes").html(carrierCodes);
			
		
		
		$.each(dataMap.PublishedOndList, function(key, value) {
			
			if(value.indexOf("***") != -1){
				value = value.replace("***","All")
			}

			ondList += '<option value="' + key + '">' + value + '</option>';
		});

		$("#sel_Onds").html(ondList);
		
		
		
		if(dataMap.RouteWiseAutoPublish === "true"){
			$("#routeWiseAutoPublish")[0].checked = true;
		}
	}

	loadInitdata = function() {
		
		var data = {};
		selectedGdsId = window.opener.selectedGdsIDForPublish;
		data['gdsId'] = selectedGdsId;
		$.ajax({
			url : 'loadInitDataForPublishGds.action',
			// beforeSend : ShowProgress(),
			type : "GET",
			data :data,
			async : false,
			dataType : "json",
			complete : function(response) {
						var response = eval("(" + response.responseText + ")");
						dataMap.ONDs = objectToArray(response.airports);
						dataMap.Carriers = response.carriers;
						if (response.gdsPublishedOndList) {
							dataMap.PublishedOndList = response.gdsPublishedOndList;
						}
						if (response.gdsPublishedCarrierCodes) {
							dataMap.PublishedCarrierCodes = response.gdsPublishedCarrierCodes;
						}
						dataMap.RouteWiseAutoPublish = response.enableRouteWiseAutoPublish;
						fillInitData();
						// HideProgress();
					}
		});
	}
	
	if(isReset === 'true'){
		fillInitData();
	}else{
		loadInitdata();
	}

}

getOnds = function() {
	var ond = "";
	if (($("#selFrom option:selected").val() != '')
			&& ($("#selTo option:selected").val() != '')) {
		var origin = $("#selFrom option:selected").text();
		var destination = $("#selTo option:selected").text();
		
		var fromVal = $("#selFrom").val();
		var toVal = $("#selTo").val();

		if(origin === destination){
			alert("Origin Destination can not be same!");
		}
		else{
			ond += origin + "/" + destination;
			ondArray.push(ond);
		}
	}

	return ond;
}


getCarriers = function() {
	var str = "";
	if (($("#carrierCode option:selected").val() != '')) {
		var cc = $("#carrierCode option:selected").text();
		
		var carrierCodeVal = $("#carrierCode").val();

		str += cc;
		ccArray.push(cc);
	}

	return str;
}

addItemsToSelect = function(id) {

	var tID = id.split("_")[1];
	var boolAdd = true;
	switch (tID) {
	case "Onds":

		var ond = getOnds();
		if (ond != "") {
			var appendItem = $("<option>" + ond + "</option>").attr("value",
					ond);

			alloption = $("#sel_" + tID).find("option");
			for (var i = 0; i < alloption.length; i++) {
				if (alloption[i].label== ond) {
					alert("Already added!..");
					boolAdd = false;
					break;
				}
			}
			if (boolAdd) {
				if (alloption[0] == "") {
					$("#sel_" + tID).empty();
				}
				$("#sel_" + tID).append(appendItem);

				var ondData = {};

				var ondVal = '';
				for (var i = 0; i < ondArray.length; i++) {
					if (i == 0) {
						ondVal = ondArray[i];
					} else {
						ondVal = ondVal + "," + ondArray[i];
					}

				}

				ondData['selectedOnds'] = ondVal;

			}
		}
		break;

	default:

	}
};


removeItemsFromSelect = function(id){
	var tID = id.split("_")[1];
	
	switch(tID){
		case "Onds":
			alloption = $("#sel_"+tID).find("option");
			if(alloption[0].value!=""){ 
			
				var selItem=$("#sel_"+tID).find("option:selected")[0].value;
				$("#sel_"+tID).find("option:selected").remove();
				var splitItem=selItem.split("/");
				var arrayElement=[];
				for(var i=0;i<splitItem.length-1;i++){
					if(splitItem[i+1]!=""){
						arrayElement[i]="'"+splitItem[i]+"/"+splitItem[i+1]+"'";
					}
				}
				for(var a=0;a<arrayElement.length;a++){
					for(var i=0; i<ondArray.length;i++ )
					{ 
						if(ondArray[i]==arrayElement[a]){
							ondArray.splice(i,1);
							break;
						} 
					}
				}
				var ondData = {};
				var ondVal = '';
				for (var i = 0; i < ondArray.length; i++) {
			       if(i==0){
			    	   ondVal= ondArray[i];
			       }
			       else{
			    	   ondVal=ondVal+","+ ondArray[i];
			       }
			           		        
			    }
				
				ondData['selectedRoutes']=ondVal;
				break;
	}
	
	}
};


function getSelectedOnds(){
	var optionValues = [];
	$('#sel_Onds option').each(function() {
		
		var ondToSave =	$(this).text();
		
		if(ondToSave.indexOf("All") != -1){
			ondToSave = ondToSave.replace("All","***")
		}
		
		optionValues.push(ondToSave);
		
	    
	});

	return optionValues;
}

function getSelectedCarriers(){
	var optionValues = [];
	$('#sel_Carrier option').each(function() {
	    optionValues.push($(this).text());
	});

	return optionValues;
}


function onSuccessSaveSchedule() {

		showWindowCommonError("Confirmation", "Save/Update Succesful");
		loadData(false);
	
}

function onErrorSaveSchedule() {
	showWindowCommonError("Error", "error");
}

function updateOrSaveData(){
	
	var selectedOnds = [];
	var selectedCarriers = [];
	var isEnableAutoPublishForGds;
	
	selectedOnds = getSelectedOnds();
	selectedCarriers = getSelectedCarriers();
	isEnableAutoPublishForGds = $("#routeWiseAutoPublish").is(":checked");
	
	

		var params = [ {
		name : "selectedOnds",
		value : selectedOnds
	}, {
		name : "selectedCarriers",
		value : selectedCarriers
	}, {

		name : "enableAutoPublishForGds",
		value : isEnableAutoPublishForGds
	}, {

		name : "gdsID",
		value : selectedGdsId
	}, {

		name : "hdnMode",
		value : "UPDATE"
	} ];
		
		$.ajax({
			url :'showPublishRoutesSaveUpdate.action',
			type : "POST",
			data :params,
			async : false,
			dataType : "json",
			success : onSuccessSaveSchedule,
			cache : false
			
		});
	
}




var addValuesToSelect = function(optVal, optText, src, dest){	
	if ($("select#" + dest + " option[value='" + optVal + "']").length == 0) {
    	if ( $("#"+dest+" option[value='"+optVal+"']").length == 0 ){                                		
    		$("#" + dest).append("<option value=\"" + optVal + "\">" + optText + "</option>");
    	}
        $("select#" + src + " option").remove("[value='" + optVal + "']");
    }
}


var addValuesToSelect = function(optVal, optText, src, dest){	
	if ($("select#" + dest + " option[value='" + optVal + "']").length == 0) {
    	if ( $("#"+dest+" option[value='"+optVal+"']").length == 0 ){                                		
    		$("#" + dest).append("<option value=\"" + optVal + "\">" + optText + "</option>");
    	}
        $("select#" + src + " option").remove("[value='" + optVal + "']");
    }
}


var addToSelectPane = function (src, dest) {
    var optVal;
    var optText;

    $("select#" + src + " option:selected").each(
        function () {
            optVal = $(this).val();
            optText = $(this).text();
            addValuesToSelect(optVal, optText, src, dest);
        });

}

var removeFromSelectPane = function (dest, src) {
    var optVal;

    $("select#" + dest + " option:selected").each(
        function () {
            optVal = $(this).val();
            optText = $(this).text();
            removeValuesFromSelect(optVal, optText, dest, src);
        });
}


function resetWindow(reset){
	loadData(reset);
	
}
