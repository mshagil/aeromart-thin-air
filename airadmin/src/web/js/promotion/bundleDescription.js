var bundleTemplates = {};
var currentlySelectedTemplateID = null;
var newSearch = false;
var isInAddMode = false;
var isInEditMode = false;
var languages = {};

var BASE_LANGUAGE = {'langCode':'en', 'languageName': 'English' };

var SYNTAX_CONSTRAINTS = {
	 'BAGGAGE': {syntax: 'href="BAGGAGE"', max_count: 1}
};


jQuery(document)
    .ready(function () {
        getLanguages();

        top[2].HideProgress();

        jQuery('#bundleTemplatesTable').jqGrid({
            url: "showBundleDescription.action",
            postData: {
                'templateNameSearchText': function () {
                    return $("#templateNameSearchText").val();
                },
                'newSearch': function () {
                    return newSearch;
                }
            },
            datatype: 'json',
            jsonReader: {
                root: 'bundleTemplates',
                page: "page",
                total: "totalPages",
                records: "totalRecords",
                repeatitems: false,
                id: 'templateID'
            },
            colNames: ['ID', 'Template Name','Translations', 'Created Date', 'Version'],
            colModel: [
                { name: 'templateID', jsonmap: 'templateID', align: "center", width: 47 },                
                { name: 'templateName', jsonmap: "templateName", align: "left", width: 400 },
                { name: 'translations', jsonmap: "translationTemplates", align: "left", width: 257,formatter: traslationFomatterToString },
                { name: 'createdDate', jsonmap: 'createdDate' , align: "center", width: 200},
                { name: 'version', jsonmap: 'version', align: "center", hidden: true }
            ],
            rowNum: 10,
            viewRecords: true,
            height: 140,
            pager: '#bundleTemplatesTablePager',

            afterInsertRow: function (rowid, rowdata, rowelement) {
                bundleTemplates[rowelement.templateID] = rowelement;
            },
            onSelectRow: function (rowid, status, e) {
                var populateRowDetails = !isInAddMode || isPreferredToClearForm();

                if (populateRowDetails) {

                    isInAddMode = false;
                    isInEditMode = false;

                    $('#btnEdit').show();
                    $('#btnTranslate').show();

                    currentlySelectedTemplateID = rowid;
                    var currentRowData = bundleTemplates[currentlySelectedTemplateID];
                    clearFormContent();
                    $('#txtTemplateName').val(currentRowData.templateName);
                    tinyMCE.get('freeContext_en').setContent(currentRowData.defaultFreeServicesContent);
                    tinyMCE.get('paidContext_en').setContent(currentRowData.defaultPaidServicesContent);
                    if(!jQuery.isEmptyObject(currentRowData.translationTemplates)){
                    	$.each(currentRowData.translationTemplates, function (i, item) {
                            tinyMCE.get("freeContext_" + item.languageCode).setContent(item.freeServicesContent); //translationTemplates
                            tinyMCE.get("paidContext_" + item.languageCode).setContent(item.paidServicesContent);
                        
                    	});
                    }
                    disableFormEditing(true);
                }

            }
        });     

        $("button").decorateButton();

        $("#btnEdit").click(function (event) {
            event.preventDefault();
            editTemplate();
        });

        $("#btnSearch").click(function (event) {
            event.preventDefault();
            handleSearchClick();
        });


        $("#btnAdd").click(function (event) {
            event.preventDefault();
            handleAddTemplate();
        });

        $("#btnSave").click(function (event) {
            event.preventDefault();
            saveTemplate();
        });
        
        $("#btnReset").click(function (event) {
            event.preventDefault();
            clearFormContent();
        });
        
        $("#tabTemplatesGrid").decoratePanelWithAlign("Bundle Display Templates");
        $("#addEditDisctiptionPanel").decoratePanelWithAlign("Add/Edit Display Templates");
    });

$(function() {
    $("#descriptionTabs").tabs({
        activate: function(event, ui) {
        	
            var tabId = event.currentTarget.id;
            if (isContentManagementTab(tabId)) {
                var langCode = tabId.split("_").pop();
                if (isInAddMode || isInEditMode) {
                    tinymce.execCommand('mceAddControl', true, ('freeContext_' + langCode));
                    tinymce.execCommand('mceAddControl', true, ('paidContext_' + langCode));
                    tinyMCE.get("freeContext_" + langCode).getBody().setAttribute('contenteditable', true);
                    tinyMCE.get("paidContext_" + langCode).getBody().setAttribute('contenteditable', true);
                }
            }
        }
    });

    $("#descriptionTabs").tabs().css({
        'resize': 'none',
        'min-height': '253px'
    });
});

/**
 * Checks whether it is a Conent Managment Tab (tinyMce) or not
 */
var isContentManagementTab = function(tabId) {
    var nonContentManagementTabs = ['tab_settings'];
    if ($.inArray(tabId, nonContentManagementTabs) > -1) {
        return false;
    }
    return true;
}

traslationFomatterToString = function(cellVal, options, rowObject){
	var traslationString = "en";
	if (cellVal!=null){
		 $.each(cellVal, function (i, traslations) {
			 traslationString = traslationString +" , "+ traslations.languageCode;
		 });
	}
	return traslationString;
}

getLanguages = function () {

    $.ajax({
        url: "showBundleDescription!retrieveLanguages.action",
        type: "POST",
        data: {},
        success: function (response) {
            languages = response.languagelist;
            var baseLang = response.baseLanguage;
            if(baseLang){
            	BASE_LANGUAGE =  {'langCode': baseLang.languageCode, 'languageName': baseLang.languageName };
            }
            buildTranslationTabs();
        }
    });
};

templateEditingCompleted = function (data) {

    clearFormContent();

    isInAddMode = false;
    isInEditMode = false;

    if (data.success) {
        showMsg("Success", "Bundle Template Updated Successfully");
        $('#bundleTemplatesTable').trigger("reloadGrid");
    } else {
        showMsg("Error", data.messageTxt);
    }
};

clearFormContent = function () {
    $('#txtTemplateName').val('');
    tinyMCE.get('freeContext_en').setContent('');
    tinyMCE.get('paidContext_en').setContent('');
    $.each(languages, function (i, item) {
        tinyMCE.get("freeContext_" + item.languageCode).setContent('');
        tinyMCE.get("paidContext_" + item.languageCode).setContent('');
    
    });
    
};

handleSearchClick = function () {
    isInAddMode = false;
    isInEditMode = false;

    clearFormContent();
    disableFormEditing(true);
    newSearch = true;
    top[2].HidePageMessage();
    $('#bundleTemplatesTable').trigger("reloadGrid");
    newSearch = false;
};

handleAddTemplate = function () {
    if (!isInEditMode || isPreferredToClearForm()) {
        isInAddMode = true;
        isInEditMode = false;

        // In add mode if row selected is in yellow
        // Then user might be in confusion
        // To avoid it in add mode removed the highlighted color of the row
        $('#bundleTemplatesTable tr').removeClass('ui-state-highlight');
        $('#btnSave').prop('disabled', false);


        // In add mode hiding the edit, translate and delete buttons to the user
        $('#btnEdit').hide();
        $('#btnTranslate').hide();

        clearFormContent();
        disableFormEditing(false);
    }
};

saveTemplate = function () {
    if (validateFormContent()) {
        $('#btnSave').prop('disabled', true);
        var rowData = bundleTemplates[currentlySelectedTemplateID];

        var data = {};
        data["updatedBundleTemplate.templateName"] = $('#txtTemplateName').val().trim();
        data["updatedBundleTemplate.defaultFreeServicesContent"] = tinyMCE.get('freeContext_en').getContent();
        data["updatedBundleTemplate.defaultPaidServicesContent"] = tinyMCE.get('paidContext_en').getContent();

        var translations ={};
        if (isInEditMode) {
            data["updatedBundleTemplate.templateID"] = currentlySelectedTemplateID;
            data["updatedBundleTemplate.createdDate"] = rowData.createdDate;
            data["updatedBundleTemplate.version"] = rowData.version;
            translations = rowData.translationTemplates;
        }
        
        
       
        
        //set traslations from respective panel contents
        $.each(languages, function (i, item) {
        	var langCode =  item.languageCode;
        	var updatedfreeContent = getContent("freeContext_" + langCode);
        	var updatedpaidContent = getContent("paidContext_" + langCode);
        	
        	// 1. If there are no translations for BOTH paid AND free, it will be not stored in the DB to minimize unwanted data accumulation in DB.
        	// 2. Therefore, full cleaning of contents will be not allowed for a language
        	if( isNotEmpty(updatedfreeContent) || isNotEmpty(updatedpaidContent) ){
                data["updatedBundleTemplate.translationTemplates." + item.languageCode + ".freeServicesContent"] = updatedfreeContent;
                data["updatedBundleTemplate.translationTemplates." + item.languageCode + ".paidServicesContent"] = updatedpaidContent;
                data["updatedBundleTemplate.translationTemplates." + item.languageCode + ".languageCode"] = langCode;
                
                if (isInEditMode) {
                    if (translations) {
                        var selectedTranslation = translations[langCode];
                        if (selectedTranslation) {
                            var translationVersion = selectedTranslation.version;
                            var translationTemplateId = selectedTranslation.templateID;
                            data["updatedBundleTemplate.translationTemplates." + item.languageCode + ".version"] = translationVersion;
                            data["updatedBundleTemplate.translationTemplates." + item.languageCode + ".templateID"] = translationTemplateId;
                        }

                    }
                }
        	}
        });

        $.ajax({
            url: "showBundleDescription!updateBundleTemplate.action",
            type: "POST",
            data: data,
            complete: function (response) {
                templateEditingCompleted(response);
            }
        });
    }
};



/**
 * Once user click on the edit button enable the form content
 */
editTemplate = function () {
    top[2].HidePageMessage();
    var rowData;
    if (!currentlySelectedTemplateID) {
        alert("Please select a template to edit.");
        return;
    } else {
        isInEditMode = true;
        isInAddMode = false;
        disableFormEditing(false);
        $('#btnSave').prop('disabled', false);
        rowData = bundleTemplates[currentlySelectedTemplateID];
    }
};


/*
 * Show message in the AirAdmin interface.
 */
showMsg = function (strType, strMsg) {
    top[2].objMsg.MessageText = strMsg;
    top[2].objMsg.MessageType = strType;
    top[2].ShowPageMessage();

};

disableFormEditing = function (disbale) {
    $('#txtTemplateName').prop('disabled', disbale);
    tinyMCE.get('freeContext_en').getBody().setAttribute('contenteditable', !disbale);
    tinyMCE.get('paidContext_en').getBody().setAttribute('contenteditable', !disbale);
    
    $.each(languages, function (i, item) {
            tinyMCE.get("freeContext_" + item.languageCode).getBody().setAttribute('contenteditable', !disbale);
            tinyMCE.get("paidContext_" + item.languageCode).getBody().setAttribute('contenteditable', !disbale);
        
    });
};

isPreferredToClearForm = function () {
    return confirm("Your Changes will be lost, Do you need to proceed ?");
};

buildTranslationTabs = function () {

    $.each(languages, function (i, item) {
        var $li = $("#tabs_li_en").clone();
        var $a = $("#tab_en").clone();
        $a.text(item.languageName);
        $a.prop('id', 'tab_id_' + item.languageCode);
        $a.prop('href', '#tabs_' + item.languageCode);
        $li.prop('id', 'tab_li_' + item.languageCode);
        $a.removeClass("ui-state-active").removeClass("ui-tabs-selected");
        $li.html($a);
        $('#tabs_ul').append($li);
    });
    $(".tabTranslationDiv").tabs("refresh");

    initTinyMCE("#freeContext_en,#paidContext_en");
    
    $.each(languages, function (i, item) {

        var tabId = 'tabs_' + item.languageCode;
        var freeTextareaId = "freeContext_" + item.languageCode;
        var paidTextareaId = "paidContext_" + item.languageCode;
        var tanslationContxtTblId = 'TraslationTextContext_' + item.languageCode;
        var tinymcSelector = "#" + freeTextareaId + ",#" + paidTextareaId;

        var div = $("#tabs_en").clone().prop('id', tabId);
        div.find(".freeContext").attr('id', freeTextareaId);
        div.find(".paidContext").attr('id', paidTextareaId);
        div.find(".traslationContext_table").attr('id', tanslationContxtTblId);

        $('.tabTranslationDiv').append(div);
        $("#descriptionTabs").tabs("refresh");

        initTinyMCE(tinymcSelector);

        $("#descriptionTabs").tabs("refresh");
    });

    var index = $('#descriptionTabs').index($('#tab_en'));
    $(".ui-tabs-selected").removeClass("ui-state-active").removeClass("ui-tabs-selected");
    $("#descriptionTabs").tabs("refresh");

};

function initTinyMCE(tinymcSelector) {
	 tinymce.init({
         selector: tinymcSelector,
         height: 150,
         width: 430,
         menubar: false,
         style_formats: [
             { title: 'Name Text', inline: 'b' },
             {title : 'Description Text', inline : 'small'}
         ],
         plugins: "autolink,lists,pagebreak,style,layer,save,advhr," +
             "advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print" +
             ",paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,template",

         // Theme options
         theme_advanced_buttons1: "bullist,|,bold,italic,underline,strikethrough,|,link,unlink,|,styleselect,|,undo,redo,|,ltr,rtl",
         theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,code",
         theme_advanced_buttons3: false,
         theme_advanced_buttons4: false,
         theme_advanced_toolbar_location: "top",
         theme_advanced_toolbar_align: "left",
         theme_advanced_statusbar_location: "bottom",
         theme_advanced_resizing: true,
         external_link_list_url: "../../js/configurations/tinyMceLinkList.js",

         init_instance_callback: function (editor) {
             if (document.getElementById(editor.id).hasAttribute('disabled')) {
                 editor.getBody().setAttribute('contenteditable', false);
             }
         }
     });
};


/**
 * Validations ==> Form
 *
 */
validateFormContent = function() {

    var valid = true;

    if (!isNotEmptyTextbox('#txtTemplateName')) {
    	valid =false;
        showMsg("WARNING", "Template Name cannot be empty");
    }
    
    var regex = /^[0-9a-zA-Z\_]+$/;
    if(valid && !regex.test($('#txtTemplateName').val())){
    	valid =false;
        showMsg("WARNING", "Template Name cannot include special charactors.");
    }

    //Validations for base language
    valid = valid && validateDefaultContent(BASE_LANGUAGE.langCode, BASE_LANGUAGE.languageName);
    if (valid) {
        valid = validateContentGrammer(BASE_LANGUAGE.langCode, BASE_LANGUAGE.languageName);
    }

    //Validations for non-base languages
    if (valid) {
        $.each(languages, function(i, item) {
            if (valid && !validateContentGrammer(item.languageCode, item.languageName)) {
                valid = false;
            }
        });
    }
    return valid;
};


/**
 * Validations => Base Language
 */
validateDefaultContent = function(langCode, languageName) {
    var valid = true;
    var defaultPaidId = 'freeContext_' + langCode;
    var defaultPaidContent = getContent(defaultPaidId);

    var defaultFreeId = 'paidContext_' + langCode;
    var defaultFreeContent = getContent(defaultFreeId);

    if (!isNotEmpty(defaultPaidContent) && !isNotEmpty(defaultFreeContent)) {
        var errorMsg = fillPlaceHolder(
            "{0}- Free services template and Paid Services Template both cannot be empty",
            languageName);
        valid = false;
        showMsg("WARNING", errorMsg);
    }
    return valid;
}


/**
 * Validations => Content Grammer
 * 
 * conent should start with  <ul> element and should end with  </ul>
 */
validateContentGrammer = function(langCode, languageName) {

    var freeTranslationId = 'freeContext_' + langCode;
    var freeTranslationContent = getContent(freeTranslationId);

    if (isNotEmpty(freeTranslationContent)) {
        if (grammerViolated(freeTranslationContent)) {
            var errorMsg = fillPlaceHolder("{0}- Free services template translation should be in list format", languageName);
            showMsg("WARNING", errorMsg);
            return false;
        }

        if (!syntaxValid(freeTranslationContent,languageName, true)) {
            return false;
        }
    }

    var paidTranslationId = 'paidContext_' + langCode;
    var paidTranslationContent = getContent(paidTranslationId);

    if (isNotEmpty(paidTranslationContent)) {
        if (grammerViolated(paidTranslationContent)) {
            var errorMsg = fillPlaceHolder("{0}- Paid services template translation should be in list format", languageName);
            showMsg("WARNING", errorMsg);
            return false;
        }

        if (!syntaxValid(paidTranslationContent, languageName, false)) {
            return false;
        }

    }

    return true;
}

var grammerViolated = function (content){
	return !(content.match("^<ul>")) || (!content.match("</ul>$"));
}

/**
 * Rules:
 * 1. Free services ==> cannot have rates links
 * 2. Paid services==> cannot have more than one rates links for a given type
 */
var syntaxValid = function(content, languageName, restrict) {
    var valid = true;
    var errorMsg = "";
    Object.keys(SYNTAX_CONSTRAINTS).forEach(function(code) {
        var constraint = SYNTAX_CONSTRAINTS[code];
        var syntax = new RegExp(constraint.syntax, 'g');
        var maxCount = constraint.max_count;
        var count = (content.match(syntax) || []).length;

        if (valid && restrict && count >= 1) {
        	valid = false;
        	errorMsg = fillPlaceHolder("{0}- Cannot add {0} Rate links to Free services",languageName);
            errorMsg = fillPlaceHolder(errorMsg, capitalizeFirstLetter(code));
            showMsg("WARNING", errorMsg);  
        }

        if (valid && count > maxCount) {
        	valid = false;
        	errorMsg = fillPlaceHolder("{0}- Cannot add more than {0} {0} Rate links to Paid services", languageName);
            errorMsg = fillPlaceHolder(errorMsg, maxCount);
            errorMsg = fillPlaceHolder(errorMsg, capitalizeFirstLetter(code));
            showMsg("WARNING", errorMsg);
        }

    });
    return valid;
}

var getContent = function(id) {
    return tinyMCE.get(id).getContent();
}


var isNotEmpty = function(field) {
    if (field && field.trim() && field.trim() !== "") {
        return true;
    }
    return false;
}

var isNotEmptyTextbox = function(textboxId) {
    var textContent = $(textboxId).val();
    if (isNotEmpty(textContent)) {
        return true;
    }
    return false;
}

var fillPlaceHolder = function(placeHolder, value) {
    if (placeHolder) {
        placeHolder = placeHolder.replace("{0}", value);
    }
    return placeHolder;
}

var capitalizeFirstLetter = function (str){
	if(str){
		str = str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
	}
	return str;
}
