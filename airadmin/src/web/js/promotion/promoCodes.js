var promoCodes = new Array(); 
var codesGenerated = false;
var codesArray = new Array();
var promoCriteriaId = "";
var enableSending = false;
const SALE_CHANNEL_WEB = '4';

function KPValidateDecimelDup(objCon, s, f) {
		
		var strText =objCon.value;
		var length = strText.length;
		var blnVal = isLikeDecimal(strText);
		var blnVal2 = false;
		var type = "";
				
		if (!blnVal) {
			customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		} else if (!blnVal2) {
			customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
			objCon.focus();
		}
		if(($("#selpromoType :selected").val() == "Discount")&&($("#selApplyToDiscount :selected").val()=="2")){
			type=$("#selpromoTypeDiscount").val();
			blnVal2 = currencyValidate(strText, 2, 2);
		}
		else if(($("#selpromoType :selected").val() == "Discount")&&($("#selApplyToDiscount :selected").val()!="2")){
			type=$("#selpromoTypeDiscount").val();
			blnVal2 = currencyValidate(strText, 3, 2);
		}
		else if ($("#selpromoType :selected").val() == "Free Service"){
			type= $("#selpromoTypeAncillary").val();
			blnVal2 = currencyValidate(strText, 3, 2);
		}
		else if ($("#numOfCodes").val() != ""){
			type= "NUM";
		}
		else{
			type= $("#limitedLoadFactor").val();
			blnVal2 = currencyValidate(strText, 3, 2);
		}
		if(type == "VALUE"){
			blnVal2 = currencyValidate(strText, 10, 2);
			if (!blnVal2) {
				if (strText.indexOf(".") != -1) {
					wholeNumber = strText.substr(0, strText.indexOf("."));
					if (wholeNumber.length > 13) {
						objCon.value= strText.substr(0, wholeNumber.length - 1);
					} else {
						objCon.value= strText.substr(0, length - 1);
					}
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			}
		}else if(type == "NUM"){
			blnVal2 = currencyValidate(strText);
			if (!blnVal2) {
				if (strText.indexOf(".") != -1) {
					objCon.value = strText.substr(0, strText.indexOf("."));
				}	
				if (strText.length>5){
					objCon.value = strText.substr(0,5);
				}
			}
		}
		else{
			
			if (!blnVal2) {
				if (strText.indexOf(".") != -1) {
					wholeNumber = strText.substr(0, strText.indexOf("."));
					if (wholeNumber.length > 6) {
						objCon.value= strText.substr(0, wholeNumber.length - 1);
					} else {
						objCon.value= strText.substr(0, length - 1);
					}
				} else {
					objCon.value= strText.substr(0, length - 1);
				}			
			}
			if(strText > 100){
				objCon.value= strText.substr(0, length - 1);
			}
			
		}
	}
function promoCodePress(objCon) {
	
	var strCC = objCon.value;
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		objCon.value=strCC.substr(0, strLen - 1);
		objCon.focus();
	}
	} 

function UI_promoCode(){
	
	// Gayan todo : put the new messages in a language resource file
	var selRoute='';
	var routeArray = [];
	var dataMap = {
		ONDs: [{"0":"CMB", "1":"Colombo"},{"0":"SHJ", "1":"Sharjah"}],
		FlightNo: [ "G90507","G90506","G90505","G90501","G90503" ],
		Agents: [],
		Anicillaries: [],
		COS: ["Y","E"],
		Channels:[["Public"], ["Agents"], ["WebServise"]],
		LCC: ["Y1","T2"],
		BookinClasses: ["ABC","YOM"],
	}
	var promotionType = [["Buy And Get Free","Buy And Get Free"],["Discount","Discount"],["Free Service","Free Service"]];
	
	var discountApplyTo = [["0","Fare"],["1","Fare + Surcharges"],["2","Total"]];
	var discountApplyValue = [["3","Reservation"],["4","Passenger"]];
	
	var selectedRowData = null;
	var isNew = true;
	var maxViaPoint = 3;
	
	//multiple date periods variables section
	var maxDateCount = null;
	var registrationDateGridDetails = {
		gridId: "multipleRegisterDateGrid",
		tableId: "multipleRegisterDateTable",
		fromId: "registrationFrom",
		toId: "registrationTo",
		addBtnId: "registrationDateAdd",
		delBtnId: "registrationDateDel",
		clrBtnId: "registrationDateClear",
		disabledDays: [],
		dateCount: 1,
		isEdit: false,
		selectedData: null,
		alertMessages: {
			required:"Registration date period is required.",
			invalid:"Registration from date should be before the registration to date",
			maxlength:"You can add maximum 5 date ranges for registration periods",
			overlap:"Overlapping ranges for registration periods not allowed"
		}
	};

	var flightDateGridDetails = {
		gridId: "multipleFlightDateGrid",
		tableId: "multipleFlightDateTable",
		fromId: "flightPeriodFrom",
		toId: "flightPeriodTo",
		addBtnId: "flightDateAdd",
		delBtnId: "flightDateDel",
		clrBtnId: "flightDateClear",
		disabledDays: [],
		dateCount: 1,
		isEdit: false,
		selectedData: null,
		alertMessages: {
			required:"Flight date period is required.",
			invalid:"Flight from date should be before the flight to date",
			maxlength:"You can add maximum 5 date ranges for flight periods",
			overlap:"Overlapping ranges for flight periods not allowed"
		}
	};
	
	this.ready = function(){
		var tLink = $("<a href='javascript:void(0)'><img src='../../images/tri-down1_no_cache.gif' alt=''>&nbsp;Search Promo Codes</a>");
		tLink.click(hideAddEditPromo);
		loadInitdata();
		loadLinkPOS();
		$("#divPromeSearch").decoratePanel(tLink);
		$("#promoCodeDetails").decoratePanel("Add/Edit Promo Codes");
		$("#promoCodeDetails").parent().hide();
		$("#selpromoType").change(showPromoType);
		
		$("#btnAdd").click(function(){
			showAddEditPromo('add');
			isNew = true;
			$("#selpromoType").removeAttr('disabled');
		});
		
		$("#btnEdit").click(function(){
			showAddEditPromo('edit');
			isNew = false;
			$("#selpromoType").attr('disabled', 'disabled');
		});
		
		$("#btnCopy").click(function(){
			showAddEditPromo('copy');
			isNew = true;
			$("#selpromoType").removeAttr('disabled');
		});
		
		//applicability option enable disable function
		$("#selChanel").change(function(){
			
			if($.inArray(SALE_CHANNEL_WEB,$(this).val()) > -1){
				$("#applicability").css("display","");
				$("#webPOS").css('display','');
				if (getSelectBoxData("lstPOSAssigned")==null){
					$("#addAllRight_lstPOS").trigger('click');
				}
			}else{
				$("#applicability").css("display","none");
				$("#webPOS").css('display','none');
			}
		});
		
		//Multiple reservation and date periods section initialization

		constructDatePeriodGrid(registrationDateGridDetails);
		constructDatePeriodGrid(flightDateGridDetails);
		
		$("#registrationDateAdd").click(function(){
			addOrSaveDatePeriod(registrationDateGridDetails);
		});

		$("#registrationDateDel").click(function(){
			editOrDeleteDatePeriod(registrationDateGridDetails, 'delete')
		});

		$("#registrationDateClear").click(function(){
			clearDatePeriods(registrationDateGridDetails);
		});


		$("#flightDateAdd").click(function(){
			addOrSaveDatePeriod(flightDateGridDetails);
		});

		$("#flightDateDel").click(function(){
			editOrDeleteDatePeriod(flightDateGridDetails, 'delete');	
		});

		$("#flightDateClear").click(function(){
			clearDatePeriods(flightDateGridDetails);
		});		

		$("#registrationFrom, #registrationTo").datepicker({
			minDate: new Date(),
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			beforeShowDay: function(date){
				var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
				return [ registrationDateGridDetails.disabledDays.indexOf(string) == -1 ]
			}
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});

		$("#flightPeriodFrom, #flightPeriodTo").datepicker({
			minDate: new Date(),
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			beforeShowDay: function(date){
				var string = jQuery.datepicker.formatDate('dd/mm/yy', date);
				return [ flightDateGridDetails.disabledDays.indexOf(string) == -1 ]
			}
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});
		
		//End of Multiple reservation and date periods section initialization
		
		$("#btnStatus").click(function(){
			makePromoCodeStatusUpdate();
						
		});
	//	$("#btnEdit").disable();
		$(".addItem").click(function(e){
			addItemsToSelect(e.target.id);
		});
		$(".removeItem").click(function(e){
			removeItemsFromSelect(e.target.id);
		});
		$("#adultPCount, #childPCount, #infantPCount, #adultFCount, #childFCount, #infantFCount,#limitPerFlight, #limitTotal").numeric();
		populateBuyNGetPromoSequence();
		$("#enblePromoCode").click(enebleDisablePromoCode);
		$("#btnSearch").click(searchPromoCodes);
		$("#btnSave").click(savePromoCode);
		$("#btnDelete").click(deletePromoCode);
		$("#btnReset").click(resetBtnPromoCode);
		$(" #txtStartDateSearch, #txtStopDateSearch" ).datepicker({
			minDate: "",
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy'
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});
		
		$("#selpromoTypeAncillary").change(discountTypeChangeAncillary);
		$("#selpromoTypeDiscount").change(discountTypeChange);
		$("#selApplyToDiscount").change(selApplyToDiscountChange);
		
		//$("#txtStartDateSearch, #txtStopDateSearch" ).datepicker({dateFormat: 'dd/mm/yy',minDate: null});
	
		$('#applOneway').bind('change', function () {
			
			$("#appliciOutbound,#appliciInBound").attr("checked", "checked");
			 	  if (($('#applOneway').is(':checked'))&&!($('#applReturn').is(':checked')))
				   {
			 		 $("#appliciOutbound").attr("checked", "true");
			 			$("#appliciInBound").removeAttr("checked");
					 	
					 $("#appliciInbound,#appliciOutBound").attr("disabled", "disabled");
				 
				   
				   }
			 	  else  if (($('#applOneway').is(':checked'))&&($('#applReturn').is(':checked')))
				   {
			 		 $("#appliciOutbound").attr("checked", "true");
			 		$("#appliciInbound").removeAttr("checked");
					 	
						 $("#appliciOutBound").attr("disabled", "disabled");
						 $("#appliciInbound").removeAttr("disabled");
					   
				   }
			   else
				   {
				   	  $("#appliciInbound,#appliciOutBound").removeAttr("disabled");
				   }
			  
			  
			});
		$('#applReturn').bind('change', function () {
		
		
		 	  if (($('#applOneway').is(':checked'))&&!($('#applReturn').is(':checked')))
			   {
		 			
		 			$('input[name=appliciOutbound]').attr('checked', true);
		 			$('input[name=appliciInBound]').attr('checked', false);
				 	$("#appliciInbound,#appliciOutBound").attr("disabled", "disabled");
							   
			   }
		 	  else  if (($('#applOneway').is(':checked'))&&($('#applReturn').is(':checked')))
			   {
		 		 $('input[name=appliciOutbound]').attr('checked', true);;
		 		 $('input[name=appliciInBound]').attr('checked', false);
				 $("#appliciOutBound").attr("disabled", "disabled");
				 $("#appliciInBound").removeAttr("disabled");
				   
			   }
		   else
			   {
			   	  $("#appliciInBound,#appliciOutBound").removeAttr("disabled");
			   }
		  
		});
		
		$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom, #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo" ).datepicker({
			minDate:  new Date(),
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy'
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});
		
		$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").datepicker({
			minDate:  new Date(),
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy'
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});
		$("#selApplyToType").change(changeDiscountApplyToTypeValues);
		$("#selApplyAsAncillary").change(changeFSApplyToTypeValues);
		createPopUps();
		PopUpData();
		
		$("#beforeGenerate").show();
		$("#afterGenerate").hide();
		$("#btnGenerate").click(function(){
			generatePromoCodes();
		});
		
		$("#closeBtn").click(function(){
			$("#viewMultiPromoCodePopup").dialog('close');
		});
		
		$("#btnEnableCodes").click(function(){
			enablePromoCodes();
		});
		
		$("#btnView").click(function(){
			viewPromoCodes();
		});
		
		$("#btnExport").click(function(){
			exportPromoCodes();
		});
		
		$("#btnSend").click(function(){
			sendPromoCodes();
		});	
		
		$('#addOndCodes').click(function(){
			addToSelectPaneCustom('sel_GeneratedRoutes', 'sel_Routes', true); 
			removeDuplicationFromSelectBox('sel_Routes');
		});

		$('#removeOndCodes').click(function(){
			addToSelectPaneCustom('sel_Routes', 'sel_GeneratedRoutes', true);
			removeDuplicationFromSelectBox('sel_GeneratedRoutes');
		});

		$('#addAllOndCodes').click(function(){
			addToSelectPaneCustom('sel_GeneratedRoutes', 'sel_Routes', false);
			removeDuplicationFromSelectBox('sel_Routes');
		});

		$('#removeAllOndCodes').click(function(){
			addToSelectPaneCustom('sel_Routes', 'sel_GeneratedRoutes', false);
			removeDuplicationFromSelectBox('sel_GeneratedRoutes');
		});	
	};
		
	searchPromoCodes = function(){
		constructPromcodesGrid();
	};
	
	objectFormat = function(obj,bind){
		rObj = [];
		$.each(obj, function(key, val){
			var t = val
			if (bind){
				t = val + "|" + key;
			}
			rObj[rObj.length] = t;
		})
		return rObj
	}
	
	objectToArray = function(obj){
		rObj = [];
		$.each(obj, function(key, val){
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}
	
	fillInitData = function(){
		$("#selFrom, #selTo").fillDropDown({firstEmpty:false, dataArray:dataMap.ONDs , keyIndex:0, valueIndex:0});
		$("#selChanel").fillDropDown({firstEmpty:false, dataArray:dataMap.Channels , keyIndex:0, valueIndex:1});
		$( "#FlightNo" ).autocomplete({
            source: dataMap['FlightNo'],
            scroll: true
        });
	
		$( "#COS" ).autocomplete({
            source: dataMap['COS']
        })
		$( "#BookinClasses" ).autocomplete({
            source: dataMap['BookinClasses']
        })
		$( "#LCC" ).autocomplete({
            source: dataMap['LCC']
        })
		$( "#Agents" ).autocomplete({
			source:  function( request, response ) {
				    var matches = $.map( dataMap['Agents'], function(tag) {
				      if ( tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0 ) {
				        return tag;
				      }
				    });
				    response(matches);
				  },
            select: function( event, ui ) {
                $( "#Agents" ).val( ui.item.label.split("|")[0] );
                $( "#Agents" ).attr("rel", ui.item.label.split("|")[1] );
                return false;
            }
        })
        
		$("#selpromoType").fillDropDown({firstEmpty:false, dataArray:dataMap.promotionTypes , keyIndex:1, valueIndex:1});
		$("#selApplyToDiscount").fillDropDown({firstEmpty:true, dataArray:dataMap.applyToOptions , keyIndex:0, valueIndex:1});
		$("#selApplyToAncillary").fillDropDown({firstEmpty:true, dataArray:discountApplyValue , keyIndex:0, valueIndex:1});
		$("#selApplyToType,#selApplyAsAncillary").fillDropDown({firstEmpty:true, dataArray:dataMap.types , keyIndex:1, valueIndex:1});
		$("#selpromoTypeDiscount, #selpromoTypeAncillary").fillDropDown({firstEmpty:true, dataArray:dataMap.discountType , keyIndex:1, valueIndex:1});
		$("#sel_Anicillaries").fillDropDown({firstEmpty:false, dataArray:dataMap.Anicillaries , keyIndex:1, valueIndex:1});
		$("#selApplyToDiscountForBuyAndGetFree").fillDropDown({firstEmpty:true, dataArray:discountApplyTo , keyIndex:0, valueIndex:1});
		
	}
	
	fillFlightData=function(){
		$( "#FlightNo" ).autocomplete({
            source: dataMap['FlightNo']
        });
	
	}
	loadInitdata = function(){
		$.ajax({ 
			url : 'loadPromotionFormInitData.action',
			beforeSend : ShowProgress(),
			type : "GET",
			async:false,
			dataType : "json",
			complete:function(response){
				 var response = eval("(" + response.responseText + ")");
				 dataMap.ONDs = objectToArray(response.airports);
				 dataMap.FlightNo = response.flightNos;
				 dataMap.Agents = objectFormat(response.agents, true);
				 dataMap.Anicillaries = objectToArray(response.availableAnci, false);
				 dataMap.COS = objectFormat(response.availableCOS, false);
				 dataMap.Channels = objectToArray(response.availableChannels, false);
				 dataMap.LCC = objectFormat(response.availableLCC, false);
				 dataMap.BookinClasses = objectFormat(response.availableBC, false);
				 dataMap.promotionTypes = objectToArray(response.promotionTypes);
				 dataMap.status = objectToArray(response.status);
				 dataMap.applyToOptions = objectToArray(response.applyToOptions);
				 dataMap.types = objectToArray(response.types, false);
				 dataMap.discountType = objectToArray(response.discountType);
				 maxDateCount = parseInt(response.maxDatePeriodCount);
				 fillInitData();
				 HideProgress();
			}
		});
	}
	
	constructPromcodesGrid = function(){
		var newUrl = "managePromotionCriteria!search.action";
		$("#jqGridPromoCodesContainer").empty();
		var gridTable = $("<table></table>").attr("id", "jqGridPromoCodesData");
		var gridPager = $("<div></div>").attr("id", "jqGridPromoCodesPages");
		$("#jqGridPromoCodesContainer").prepend(gridTable, gridPager);
		
		var reqData=[];
		reqData['promoCriteriaSearch.OND'] = ($('#selFrom').val()+'/'+$('#selTo').val() == "/")?"null":$('#selFrom').val()+'%/%'+$('#selTo').val();
		reqData['searchReservationFromDate'] = ($('#txtStartDateSearch').val()=="")?null:$('#txtStartDateSearch').val();
		reqData['searchReservationToDate'] = ($('#txtStopDateSearch').val()=="")?null:$('#txtStopDateSearch').val();
		reqData['promoCriteriaSearch.promotionCode'] = ($('#txtPromotionCodes').val()=="")?null:$('#txtPromotionCodes').val();
		reqData['promoCriteriaSearch.status'] = ($('#selStatus').val()=="-")?null:$('#selStatus').val();

		var channelFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				for(var i=0;i<dataMap.Channels.length;i++){
					if ($.inArray(dataMap.Channels[i][0], cellVal)>-1){
						treturn += dataMap.Channels[i][1] +"<br>";
					}
				}
			}
			return treturn;
		}
		
		var dateFormatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";	
			if(cellVal!=null){
				var sortedCellVal=sortDatePeriods(cellVal);
				for(var i=0; i<sortedCellVal.length; i++){
					treturn += sortedCellVal[i] + "<br>&nbsp;";		
				}			
			}	
			return treturn;	
		}
		
		var temGrid = $("#jqGridPromoCodesData").jqGrid({
			datatype: function(postdata) {
		        $.ajax({
		           url: newUrl,
		           beforeSend : ShowProgress(),
		           data: $.extend(postdata, reqData),
		           dataType:"json",
		           complete: function(jsonData,stat){	        	  
		              if(stat=="success") {
		            	  selectedRowData = null;
		            	  mydata = eval("("+jsonData.responseText+")");
		            	  if (mydata.msgType != "Error"){
			            	 $.data(temGrid[0], "gridData", mydata.rows);
			            	 temGrid[0].addJSONData(mydata);
			            	
		            	  }else{
		            		  alert(mydata.message);
		            	  }
		            	  HideProgress();
		              }
		           }
		        });
		    },
			width: '100%',
			colNames:['&nbsp;', 'PromoCode' , 'Registration Period', 'Flight Period', 'Channels', "Type", "Status", '&nbsp;'],
			colModel:[
				{name:'id',  index:'id', width:25},
			    {name:'promoCode', index:'promoCode', width:130,jsonmap:"criteria.promoCode"},
				{name:'regPeriod', index:'regPeriod', width:190, align:"center",jsonmap:"criteria.registrationPeriods",formatter:dateFormatter},
				{name:'flightFrom', index:'fltPeriod', width:190, align:"center",jsonmap:"criteria.flightPeriods",formatter:dateFormatter},
				{name:'channels', width:120,jsonmap:"criteria.applicableSalesChannels" ,formatter:channelFomatter},
				{name:'type',  width:150, align:"left",jsonmap:"criteria.promotionCriteriaType"},
				{name:'status', index:'status', width:50, align:"center",jsonmap:"criteria.status"},
				{name:'action', index:'action', width:50, align:"center"}
			],	
			imgpath: "",
			pager: jQuery('#jqGridPromoCodesPages'),
			multiselect: false,
			viewrecords: true,
			rowNum:20, 
			sortable:true,
			altRows:true,
			altclass:"GridAlternateRow",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onSelectRow: function(rowid){
				setSelectedRow(rowid-1);
				setField("hdnSelectID", rowid-1);
				setPCForDisplayList();
			
			},
			gridComplete: function(){ 
				var rowIds = $("#jqGridPromoCodesData").getDataIDs();
				for(var i=0;i < rowIds.length;i++){
					var rowId = rowIds[i];
					if(checkOrShowMultiplePromotion(rowId-1)){
						view = "<input style='height:15px;width:30px;' type='button' class='btnDefault button' value='View' onclick=\"checkOrShowMultiplePromotion('"+(rowId-1)+"','show');\" />";
						multGenPromoCode = "Multi Generated";
						gridSet = {action:view, promoCode:multGenPromoCode};
						$("#jqGridPromoCodesData").setRowData(rowIds[i],gridSet);
					} 
				} 
			},
		
		}).navGrid("#jqGridPromoCodesPages",{refresh: true, edit: false, add: false, del: false, search: false});
	};
	
	checkOrShowMultiplePromotion=function(id, type){
		var selectID=id%20;
		var multiplePromoCodeData = $.data($("#jqGridPromoCodesData")[0], "gridData")[selectID];
		if(type=='show'){
			initMultiPromoCodesPopUP(multiplePromoCodeData.criteria.promoCodesTo);
		}else{
			if(multiplePromoCodeData.criteria.promoCodesTo!=null){
				return true;
			}
			return false;
		}
	}

	//multiple promo codes popup for search & edit ui
	initMultiPromoCodesPopUP = function(data){	
		var dynamicHtml = "<tr><th width='30%'><font>Code</font></th><th width='15%'><font>Utilized</font></th><th valign='top' width='10%'></th><th width='30%'><font>Code</font></th><th width='15%'><font>Utilized</font></th></tr>"
		var j = ((data.length%2)==0)? (data.length/2) : (~~(data.length/2)+1);
		console.log("J :",j);
		var heightx = 120 + (j*20);
		for (var i = 0 ; i < j; i++) {
			dynamicHtml += 	"<tr>" +
							"<td width='30%'><input type='text' id='promoCodes_"+i+"' style='width:95%' class='uppercase' maxlength='40' value='"+data[i].promoCode+"'disabled='disabled'/></td>"+
						 	"<td width='15%'><input type='text' id='promoUtilized_"+i+"' style='width:95%;text-align:center;' class='uppercase' maxlength='40' value='"+(data[i].fullyUtilized?"Y":"N")+"'disabled='disabled'/></td>"+
						 	"<td valign='top' width='10%'></td>"+
						 	(((i+j) < data.length)?("<td width='30%'><input type='text' id='promoCodes_"+(i + j)+"' style='width:95%' class='uppercase' maxlength='40' value='"+data[i+j].promoCode+"'disabled='disabled'/></td>"+
						 	"<td width='15%'><input type='text' id='promoUtilized_"+(i + j)+"' style='width:95%;text-align:center;' class='uppercase' maxlength='40' value='"+ (data[i+j].fullyUtilized?"Y":"N") +"'disabled='disabled'/></td>"):"") +
						 	"</tr>";
		}
		$("#codeDetails").empty();
		$("#codeDetails").append(dynamicHtml);
		$("#viewMultiPromoCodePopup").dialog({ height: heightx, width: 400, title: "Generated Promo Codes", modal: true });	
	}
	
	enebleDisablePromoCode = function(){
		if ($("#enblePromoCode").attr("checked")){
			$("#promoCode").attr("disabled", false);
			$("#numOfCodes").attr("disabled", true);
			$("#btnGenerate").attr("disabled", true);
		}else{
			$("#promoCode").val("");
			$("#promoCode").attr("disabled", true);
			$("#numOfCodes").attr("disabled", false);
			if ($("#numOfCodes").val()==null || $("#numOfCodes").val==""){
				$("#btnGenerate").attr("disabled", false);
			}
		}
	};
	
	
	addItemsFunction=function(tID, checkList){
		var boolAdd = true;
		var addValue = $("#"+tID).val();
		if (tID=="Agents"){
			addValue +=  "|" + $("#"+tID).attr("rel");
		}
		var appenItem = $("<option>"+addValue+"</option>").attr("value", addValue);
		if (tID=="Agents"){
			appenItem = $("<option>"+addValue.split("|")[0]+"</option>").attr("value", addValue.split("|")[1]);
		}
		alloption = $("#sel_"+tID).find("option");
		if (addValue==""){
			boolAdd = false;
		}else if(checkList && $.inArray(addValue ,dataMap[tID])<0){
			alert("Not a valied item!..");
			boolAdd = false;
		}
		
		for(var i = 0; i < alloption.length; i++){
			if (alloption[i].value == addValue){
				alert("Already added!..");
				boolAdd = false;
				break;
			}
		}
		
		
		if(boolAdd){
			if (tID!="BIN"){
				if(alloption[0].value==""){ 
					$("#sel_"+tID).empty();
				}
			}
			$("#sel_"+tID).append(appenItem);
			$("#"+tID).val("");
		}
	};
	
		
	addItemsToSelect = function(id){
	
		var tID = id.split("_")[1];
		var boolAdd = true;
		switch (tID){
		case "getFree":	
			var valid = true;
			var paxCount = ($("#adultPCount").val() == "" ? "ANY" : $("#adultPCount").val())+ " ,"+ ($("#childPCount").val() == "" ? "ANY" : $("#childPCount").val())
					+ " ,"+ ($("#infantPCount").val() == "" ? "ANY" : $("#infantPCount").val());

			if ( $("#adultPCount").val() !="" && (Number($("#adultPCount").val()) < Number($("#adultFCount").val()))) {
				valid = false;
			}
			if ($("#childPCount").val() !="" && (Number($("#childPCount").val()) < Number($("#childFCount").val()))) {
				valid = false;
			}
			if ($("#infantPCount").val() !="" && (Number($("#infantPCount").val()) < Number($("#infantFCount").val()))) {
				valid = false;
			}
//			if (($("#adultPCount").val() + $("#childPCount").val() + $(
//					"#infantPCount").val()) < ($("#adultFCount").val()
//					+ $("#childFCount").val() + $("#infantFCount")
//					.val())) {
//				valid = false;
//			} else {
//				if ($("#adultFCount").val() > $("#adultPCount").val()) {
//					valid = false;
//				}
//			}
			var paxFCount = ($("#adultFCount").val() == "" ? "ALL" : $(
					"#adultFCount").val())
					+ " ,"
					+ ($("#childFCount").val() == "" ? "ALL" : $(
							"#childFCount").val())
					+ " ,"
					+ ($("#infantFCount").val() == "" ? "ALL" : $(
							"#infantFCount").val());

			var pTd = $("<td>" + paxCount + "</td>").addClass(
					"thinBorderB thinBorderR paddingTD");
			var pfTd = $("<td>" + paxFCount + "</td>").addClass(
					"thinBorderB thinBorderR paddingTD");
			var tr = $("<tr></tr>").append(pTd, pfTd);
			tr.mouseenter(function() {
				$(this).addClass("ui-state-hover");
			});
			tr.mouseleave(function() {
				$(this).removeClass("ui-state-hover");
			});
			tr.click(function(e) {
				if (!e.ctrlKey) {
					$(this).parent().find("tr").removeClass(
							"ui-state-highlight");
				}
				$(this).addClass("ui-state-highlight");
			});

			if (valid) {
				if ($("#buyNGetBody>tr").html() == null) {
					$("#buyNGetBody").append(tr);
					hideMessage();
				} else {

					$("#buyNGetBody>tr").each(function() {
						var i = 0;
						var evalue = $(this).eq(i).html();
						if (evalue == tr.html()) {
							valid = false;

						}
						i++;
					});
					if (valid) {
						$("#buyNGetBody").append(tr);
						hideMessage();
					} else {
						showERRMessage("Combination already exist");
					}

				}
			} else {
				showERRMessage("Invalid buy and get free combination");
			}		
			break;
		case "BIN":
			addItemsFunction(tID, false)
			break;
		default:
			addItemsFunction(tID, true)
		}
	};
	
	removeItemsFromSelect = function(id){
		var tID = id.split("_")[1];
		if (tID=="getFree"){
			$("#buyNGetBody").find(".ui-state-highlight").remove();
		}else{
			alloption = $("#sel_"+tID).find("option");
			if(alloption[0].value!=""){ 
			
				var selItem=$("#sel_"+tID).find("option:selected")[0].value;
				$("#sel_"+tID).find("option:selected").remove();
				var splitItem=selItem.split("/");
				var arrayElement=[];
				for(var i=0;i<splitItem.length-1;i++){
					if(splitItem[i+1]!=""){
						arrayElement[i]="'"+splitItem[i]+"/"+splitItem[i+1]+"'";
					}
				}
				for(var a=0;a<arrayElement.length;a++){
					for(var i=0; i<routeArray.length;i++ )
					{ 
						if(routeArray[i]==arrayElement[a]){
							routeArray.splice(i,1);
							break;
						} 
					}
				}
			}
			if($("#sel_"+tID).find("option").length==0){
				var appenItem = $("<option>"+"All"+"</option>").attr("value", "");
				$("#sel_"+tID).append(appenItem);
			}
		}
		

	};
	
	hideAddEditPromo = function(){
		$(this).find("img").attr("src", "../../images/tri-down1_no_cache.gif");
		$("#promoCodeDetails").parent().hide();
		$("#divPromeSearch").show();
	};
	
	showAddEditPromo = function(type){
		if ($(".ui-state-highlight").length == 0 && (type=='edit' || type=='copy')){
			if (type=='edit'){
				showERRMessage("Select a row to edit");
			}else{
				showERRMessage("Select a row to copy");
			}
		}else{
			 resetPromoCode();
			 loadInitdata();
			 showPromoType();
		
			$("#promoCodeDetails").parent().show();
			$("#divPromeSearch").parent().find("a>img").attr("src", "../../images/tri-right1_no_cache.gif");
			$("#divPromeSearch").hide();
			if (type=='edit'){
				setValuesToForm();
				if($("#promoStatus").val()=='N'){
					disablePromoCode();
					validateDateFields();
					enableEditablePromocodeFields();
				
				}
				else{
					enablePromoCode();
					validateDateFields();
				}
			} else {
				enablePromoCode();
				if(type=='copy'){
					setValuesToForm();
					resetRelavantFieldsForCopy();
				}else{
					$("#addAllRight_lstPOS").trigger('click');
				}
			}
		}
	};
	
	setSelectedRow = function(id){
		var selectID=id%20;
		selectedRowData = $.data($("#jqGridPromoCodesData")[0], "gridData")[selectID];
		setField("hdnPromoCodeDesTrans", selectedRowData.criteria.pcDescForDisplay);
		if(selectedRowData.criteria.status=='INA'){
			
			var submitData = {};
			submitData["promotionCriteriaTO.promoCriteriaID"] = selectedRowData.criteria.promoCriteriaID;
						
			$.ajax({ 
				url : 'managePromotionCriteria!searchReservation.action',
				beforeSend : ShowProgress(),
				data : submitData ,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 HideProgress();
					 var responseData=$.parseJSON(response.responseText);
					 if(responseData.success){
						if(responseData.reservationAvailable=='N'){
							//$("#btnEdit").enable();
							$("#promoStatus").val("Y");
						}
						else{
						//	$("#btnEdit").disable();
							$("#promoStatus").val("N");
						}
					 } else{
							$("#promoStatus").val("N");
					//	 $("#btnEdit").disable();
						 alert("Please try again later");
					 }
				}
			});
			
			
		}
		else{
			$("#promoStatus").val("N");
		//	$("#btnEdit").disable();
		}
			
	};
	
	setValuesToForm = function(){
		var fillSelectBoxes = function(id,data,extraData){
			extratExtraData =  function(dataE,falg){
				treturn = "";
				if (dataE!=null){
					for(var i=0;i<dataE.length;i++){
						if (dataE[i].split("|")[1] == falg){
							treturn = dataE[i].split("|")[0]
							break;
						}
					}
				}else{
					treturn = falg;
				}
				return treturn;
			}
			$("#"+id).empty();
			if (data!=''){
				$.each(data, function(){
					var t = $("<option>"+extratExtraData(extraData, this)+"</option>").attr("value", this);
					$("#"+id).append(t);
				});
			}
			else{
				var t = $("<option>All</option>").attr("value", '');
				$("#"+id).append(t);
			}
		}
		fillSelectBoxes('sel_Routes', selectedRowData.criteria.applicableONDs, null);
		
		$("#lstPOS").multiSelect.loadRight(selectedRowData.criteria.posCountries, 'lstPOS');
		
		fillSelectBoxes('sel_FlightNo', selectedRowData.criteria.flightNOs, null);
		
		fillSelectBoxes('sel_Agents', selectedRowData.criteria.applicableAgents, dataMap.Agents);
				
		fillSelectBoxes('sel_COS', selectedRowData.criteria.applicableCOSs, null);
		fillSelectBoxes('sel_LCC', selectedRowData.criteria.applicableLCCs, null);
		fillSelectBoxes('sel_BookinClasses', selectedRowData.criteria.applicableBCs, null);
		if(selectedRowData.criteria.applicableBINs!=null){
			fillSelectBoxes('sel_BIN', selectedRowData.criteria.applicableBINs, null);
		}
	//	if(selectedRowData.criteria.applicableAncillaries!=null){
			//		fillSelectBoxes('sel_Anicillaries', selectedRowData.criteria.applicableAncillaries, null);
	//	}
		
		$("#sel_Anicillaries").val(selectedRowData.criteria.applicableAncillaries);
		
		$("#selpromoTypeAncillary, #selpromoTypeDiscount").val(selectedRowData.criteria.discountType);

		$("#selApplyToType").val(selectedRowData.criteria.applyAs);
		$("#selChanel").val(selectedRowData.criteria.applicableSalesChannels);
		
		//applicable member select
		if($.inArray(SALE_CHANNEL_WEB,selectedRowData.criteria.applicableSalesChannels) > -1){
			$("#applicability").css('display', '');
			$("#webPOS").css('display', '');
			
			if(!selectedRowData.criteria.applicableForLoyaltyMembers){
				$("#applLoyaltyMember").attr("checked", false);
			}
			
			if(!selectedRowData.criteria.applicableForRegisteredMembers){
				$("#applRegtMember").attr("checked", false);
			}
			
			if(!selectedRowData.criteria.applicableForGuests){
				$("#applGuest").attr("checked", false);
			}
		}
		
		if (selectedRowData.criteria.restrictAppliedResCancelation){
			$("#chkCancelation").attr("checked", true);
		}else{
			$("#chkCancelation").attr("checked", false);
		}
		if (selectedRowData.criteria.restrictAppliedResModification){
			$("#chkModification").attr("checked", true);
		}else{
			$("#chkModification").attr("checked", false);
		}
		if (selectedRowData.criteria.restrictAppliedResSplitting){
			$("#chkSplitting").attr("checked", true);
		}else{
			$("#chkSplitting").attr("checked", false);
		}
		if(selectedRowData.criteria.restrictAppliedResRemovePax){
			$("#chkRemovePax").attr("checked", true);
		}else{
			$("#chkRemovePax").attr("checked", false);
		}
		if (selectedRowData.criteria.applicableForOneway){
			$("#applOneway").attr("checked", true);
			$("#appliciOutBound").attr("checked", true);
		}else{
			$("#applOneway").attr("checked", false);
		}
		if (selectedRowData.criteria.applicableForReturn){
			$("#applReturn").attr("checked", true);
			if(selectedRowData.criteria.applicability=='RT'){
				$("#appliciInBound").attr("checked", true);
				$("#appliciOutBound").attr("checked", true);
			}
			else if(selectedRowData.criteria.applicability=='IB'){
			
				$("#appliciInBound").attr("checked", true);
			}
			else if(selectedRowData.criteria.applicability=='OB'){
				$("#appliciOutBound").attr("checked", true);
			}
			
		}else{
			$("#applReturn").attr("checked", false);
		}
		$("#limitPerFlight").val(selectedRowData.criteria.perFlightLimit);
		$("#limitTotal").val(selectedRowData.criteria.totalLimit);
		$("#numByAgent").val(selectedRowData.criteria.numByAgent);
		$("#numBySalesChannel").val(selectedRowData.criteria.numBySalesChannel);
		$("#selStatus_1").val(selectedRowData.criteria.status)
		$("#promoName").val(selectedRowData.criteria.promoName);
		$("#selApplyToType").val(selectedRowData.criteria.applyAs);
		if(selectedRowData.criteria.promoCodesTo==null && selectedRowData.criteria.promoCodeEnabled){
			$("#enblePromoCode").attr("checked", true);
			$("#promoCode").val(selectedRowData.criteria.promoCode);
		}else if(selectedRowData.criteria.promoCodesTo!=null){
			$("#beforeGenerate").hide();
			$("#afterGenerate").show();
			$("#numOfCodes").val(selectedRowData.criteria.promoCodesTo.length);
			$("#btnView").show();
			$("#btnGenerate").hide();
		}else{
			$("#enblePromoCode").attr("checked", false);
		}
		$("#disValue, #disValueAncillary").val(selectedRowData.criteria.appliedAmount);

		if(selectedRowData.criteria.promotionCriteriaType=='Buy And Get Free')
			{
			$("#limitedLoadFactor").val(selectedRowData.criteria.limitingLoadFactor);
			var tr =CreateGetAndBuyTbl(JSON.stringify(selectedRowData.criteria.freeCriteria));
			$("#buyNGetBody").append(tr);
			$("#chkSplitting").attr("checked", true);
			$("#chkSplitting").attr("disabled", true);
			$("#selApplyToDiscountForBuyAndGetFree").val(selectedRowData.criteria.applyTO);
			}
		else if(selectedRowData.criteria.promotionCriteriaType=='Discount')
			{
			$("#selApplyToType").val(selectedRowData.criteria.applyAs);
			$("#selApplyToDiscount").val(selectedRowData.criteria.applyTO);
		    $("#selpromoTypeDiscount").val(selectedRowData.criteria.discountType);
		    $("#disValue").val(selectedRowData.criteria.discountValue);
		    
		    if(selectedRowData.criteria.constPaxName==true){
		    	$("#constPaxName").attr("checked", true);
		    }
		    else{
		    	$("#constPaxName").attr("checked", false);
		    }
		    if(selectedRowData.criteria.constPaxSector==true){
		    	$("#constSector").attr("checked", true);
		    }
		    else{
		    	$("#constSector").attr("checked", false);
		    }
		    if(selectedRowData.criteria.constFlightFrom!=null){
		    	$("#constFlightPeriodFrom").val(formatDate(selectedRowData.criteria.constFlightFrom,"yyyy-mm-dd","mm/dd/yyyy"));
		    }
		    if(selectedRowData.criteria.constFlightTo!=null){
		    	$("#constFlightPeriodTo").val(formatDate(selectedRowData.criteria.constFlightTo,"yyyy-mm-dd","mm/dd/yyyy"));
		    }
		    if(selectedRowData.criteria.constValidFrom!=null){
		    	$("#constBookingPeriodFrom").val(formatDate(selectedRowData.criteria.constValidFrom,"yyyy-mm-dd","mm/dd/yyyy"));
		    }
		    if(selectedRowData.criteria.constValidTo!=null){
		    	$("#constBookingPeriodTo").val(formatDate(selectedRowData.criteria.constValidTo,"yyyy-mm-dd","mm/dd/yyyy"));
		    }
		    if(selectedRowData.criteria.restrictAppliedResSplitting==true){
		    	 $("#chkSplitting").attr("checked", true);
		    } 
		    else{
		    	 $("#chkSplitting").attr("checked", false);
		    }
		   
			$("#chkSplitting").attr("disabled", false);
			}
		else{
			$("#selApplyAsAncillary").val(selectedRowData.criteria.applyAs);
			$("#selpromoTypeAncillary").val(selectedRowData.criteria.discountType);
			if($("#selpromoTypeAncillary").val()=='VALUE'){
				$("#tdAplAnc").show();
				$("#tdAplAnc1").show();
				$("#selApplyToAncillary").val(selectedRowData.criteria.applyTO);
			}
			else{
				
				$("#tdAplAnc").hide();
				$("#tdAplAnc1").hide();
				//$("#selApplyToAncillary").fillDropDown({firstEmpty:true, dataArray:discountApplyTo , keyIndex:0, valueIndex:1});
				
			}
			  if(selectedRowData.criteria.constPaxName==true){
			    	$("#fsconstPaxName").attr("checked", true);
			    }
			    else{
			    	$("#fsconstPaxName").attr("checked", false);
			    }
			    if(selectedRowData.criteria.constPaxSector==true){
			    	$("#fsconstSector").attr("checked", true);
			    }
			    else{
			    	$("#fsconstSector").attr("checked", false);
			    }
			    
			    if(selectedRowData.criteria.constFlightFrom!=null){
			    	$("#fsconstFlightPeriodFrom").val(formatDate(selectedRowData.criteria.constFlightFrom,"yyyy-mm-dd","mm/dd/yyyy"));
			    }
			    if(selectedRowData.criteria.constFlightTo!=null){
			    	$("#fsconstFlightPeriodTo").val(formatDate(selectedRowData.criteria.constFlightTo,"yyyy-mm-dd","mm/dd/yyyy"));
			    }
			    if(selectedRowData.criteria.constValidFrom!=null){
			    	$("#fsconstBookingPeriodFrom").val(formatDate(selectedRowData.criteria.constValidFrom,"yyyy-mm-dd","mm/dd/yyyy"));
			    }
			    if(selectedRowData.criteria.constValidTo!=null){
			    	$("#fsconstBookingPeriodTo").val(formatDate(selectedRowData.criteria.constValidTo,"yyyy-mm-dd","mm/dd/yyyy"));
			    }
			    
			    $("#disValueAncillary").val(selectedRowData.criteria.discountValue);
			    if(selectedRowData.criteria.restrictAppliedResSplitting==true){
			    	 $("#chkSplitting").attr("checked", true);
			    } 
			    else{
			    	 $("#chkSplitting").attr("checked", false);
			    }
				$("#chkSplitting").attr("disabled", false);
		}
		
		for (var i=0;i<discountApplyValue.length;i++){
			if(discountApplyTo[i][1] == selectedRowData.criteria.applyTO){
				$("#selApplyToAncillary, #selApplyToDiscount").val(discountApplyValue[i][0]);
				break;
			}
		}
		
		for (var pp=0;pp<promotionType.length;pp++){
			if(promotionType[pp][1] == selectedRowData.criteria.promotionCriteriaType){
				$("#selpromoType").val(promotionType[pp][0]);
				
				var selected = $("#selpromoType>option:selected").val();
				$("#trPromoCodeType").find("fieldset").hide();
				$("[id='f_"+selected+"']").show();
				
				$("#trPromoCodePanel").show();  //for showing outside fieldset box
				
			}
		}
		setDatePeriodFormData(registrationDateGridDetails, selectedRowData.criteria.registrationPeriods);
		setDatePeriodFormData(flightDateGridDetails, selectedRowData.criteria.flightPeriods);
		
	}
	
	showPromoType = function(){
		var selected = $("#selpromoType>option:selected").val();
		$("#trPromoCodeType").find("fieldset").hide();
		$("[id='f_"+selected+"']").show();
		
		$("#trPromoCodePanel").show(); 	//for showing outside fieldset box
		
		if(selected=='Buy And Get Free'){//if buy and get one free selected
			$("#chkSplitting").attr("checked", true);
			$("#chkSplitting").attr("disabled", true);
		}
		else{
			$("#chkSplitting").attr("checked", false);
			$("#chkSplitting").attr("disabled", false);
		}
		
		
	};
	
	savePromoCode = function(){
		if (valadateForm()){
			var submitData = getDataFromPage();
			enableSending = false;
			var targetURL;
			if(isNew){
				targetURL = 'managePromotionCriteria!save.action';
			}else{
				targetURL = 'managePromotionCriteria!update.action';
			}
			
			
			$.ajax({ 
				url : targetURL,
				beforeSend : ShowProgress(),
				data : submitData,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 var responseData=$.parseJSON(response.responseText);
					 HideProgress();
					 if(responseData.success){
						 promoCriteriaId = responseData.promotionCriteriaTO.promoCriteriaID;
						 enableSending = responseData.multiplePromoCodeGenerated;
						 resetPromoCode();
						 loadInitdata();
						 showPromoType();
						 resetPromoCodeGrid();
						 SaveMsg("Promo code(s) saved successfully!")
						 alert("Promo code(s) saved successfully!");
					 } else{
						 if(responseData.messageTxt=="DPR"){
							 showERRMessage(promoCodeUniqueValueRequired);
						 }
						 else{
							 alert("System busy. Please try again later.");
						 }
					 }
				}
			});
		}
	}
	
	deletePromoCode = function(){
		var submitData = {};
		submitData["promotionCriteriaTO.version"] = selectedRowData.criteria.version;
		submitData["promotionCriteriaTO.promoCriteriaID"] = selectedRowData.criteria.promoCriteriaID;
		submitData["promotionCriteriaTO.promotionCriteriaType"] =selectedRowData.criteria.promotionCriteriaType;
		submitData["promotionCriteriaTO.promoName"] =selectedRowData.criteria.promoName;
		submitData["promotionCriteriaTO.promoCode"] =selectedRowData.criteria.promoCode;
		
		$.ajax({ 
			url : 'managePromotionCriteria!delete.action',
			beforeSend : ShowDelete,
			data : submitData ,
			type : "POST",
			async:false,
			dataType : "json",
			complete:function(response){
				 HideProgress();
				 var responseData=$.parseJSON(response.responseText);
				 if(responseData.success){
					 resetPromoCode();
					 loadInitdata();
					 showPromoType();
					 resetPromoCodeGrid();
					 searchPromoCodes();
					 alert("Promo code deleted successfully!");
				 } else{
					 alert("Cannot delete this promo code since there are reservations against this promo code!");
				 }
			}
		});
	}
	
	getDataFromPage = function(){
		var data={};
		
		if(!isNew){
			data["promotionCriteriaTO.version"] = selectedRowData.criteria.version;
			data["promotionCriteriaTO.promoCriteriaID"] = selectedRowData.criteria.promoCriteriaID;
		}else{
			data["promotionCriteriaTO.version"] = -1;
		}
		
		data["applicableONDs"] = getSelectBoxData("sel_Routes");
		data["flightNOs"] = getSelectBoxData("sel_FlightNo");
		data["applicableCOSs"] = getSelectBoxData("sel_COS");
		data["applicableLCCs"] = getSelectBoxData("sel_LCC");
		data["applicableBCs"] = getSelectBoxData("sel_BookinClasses");
		data["applicableSalesChannels"] = getSelectedSelectBoxData("selChanel");
		data["applicableAgents"] = getSelectBoxData("sel_Agents");
		if($('#enblePromoCode').is(':checked')) {
			data["promotionCriteriaTO.promoCodeEnabled"] = $("#enblePromoCode").is(":checked");
			data["promotionCriteriaTO.promoCode"] = $("#promoCode").val();
		} else if($("#numOfCodes").val()!=''){
			data["promotionCriteriaTO.promoCodeEnabled"] = true;
			data["promotionCriteriaTO.multiCodeGenerated"] = true;			
			if(isNew){
				for (var i=0 ; i < codesArray.length; i++) {
					data["promotionCriteriaTO.enablePromoCodes["+i+"]"] = codesArray[i];
				}
			}
		} else {
			data["promotionCriteriaTO.promoCode"] = $("#promoCode").val();
		}
		
		data["promotionCriteriaTO.perFlightLimit"] = $("#limitPerFlight").val();
		data["promotionCriteriaTO.totalLimit"] = $("#limitTotal").val();
		data["promotionCriteriaTO.numByAgent"] = $("#numByAgent").val();
		data["promotionCriteriaTO.numBySalesChannel"] = $("#numBySalesChannel").val();

		data["promotionCriteriaTO.restrictAppliedResModification"] = $("#chkModification").is(":checked");
		data["promotionCriteriaTO.restrictAppliedResCancelation"] = $("#chkCancelation").is(":checked");
		data["promotionCriteriaTO.restrictAppliedResSplitting"] = $("#chkSplitting").is(":checked");
		data["promotionCriteriaTO.restrictAppliedResRemovePax"] = $("#chkRemovePax").is(":checked"); 
		data["promotionCriteriaTO.status"] = $("#selStatus_1 :selected").val();
		
		data["promotionCriteriaTO.promotionCriteriaType"] = $("#selpromoType :selected").val();
	
		
		data["promotionCriteriaTO.limitingLoadFactor"] = $("#limitedLoadFactor").val();
		data["freeCriteria"] = getFreeCriteriaData();
		data["applicableAncillaries"] = getSelectedSelectBoxData("sel_Anicillaries");
		data["applicableBINs"] = getSelectBoxData("sel_BIN");
		data["promotionCriteriaTO.promoName"] = $("#promoName").val();
		
		
		data["promotionCriteriaTO.applicableForOneway"] = $("#applOneway").is(":checked");
		data["promotionCriteriaTO.applicableForReturn"] =$("#applReturn").is(":checked");

		var selector="#selChanel option:selected";
		var selectedChannels=[];
		$(selector).each(function () {
			selectedChannels.push(this.value);
		});
		if($.inArray(SALE_CHANNEL_WEB,selectedChannels) > -1){
			data["promotionCriteriaTO.applicableForLoyaltyMembers"] = $("#applLoyaltyMember").is(":checked");
			data["promotionCriteriaTO.applicableForRegisteredMembers"] =$("#applRegtMember").is(":checked");
			data["promotionCriteriaTO.applicableForGuests"] = $("#applGuest").is(":checked");
			data["posCountries"]=getSelectBoxData("lstPOSAssigned");
		}else{
			data["posCountries"]="[]";
		}

		data["registrationPeriods"] = getDatePeriodFormData(registrationDateGridDetails);
		data["flightPeriods"] = getDatePeriodFormData(flightDateGridDetails);

		if(($("#appliciInBound").is(":checked"))&&($("#appliciOutBound").is(":checked"))){
			data["promotionCriteriaTO.applicability"]="RT";
		}
		else if(($("#appliciInBound").is(":checked"))&&(!$("#appliciOutBound").is(":checked"))){
			data["promotionCriteriaTO.applicability"]="IB";
		}
		else{
			data["promotionCriteriaTO.applicability"]="OB";
		}
		
		if($("#selpromoType :selected").val() == "Discount"){
			data["promotionCriteriaTO.applyAs"] = $("#selApplyToType").val();
			data["promotionCriteriaTO.applyTO"] = $("#selApplyToDiscount").val();
			data["promotionCriteriaTO.discountType"] = $("#selpromoTypeDiscount").val();
			data["promotionCriteriaTO.discountValue"] = $("#disValue").val();
			
			data["promotionCriteriaTO.constPaxName"] = $("#constPaxName").is(":checked");
			data["promotionCriteriaTO.constPaxSector"] = $("#constSector").is(":checked");
			data["constFlightFromDate"] = $("#constFlightPeriodFrom").val();
			data["constFlightToDate"] = $("#constFlightPeriodTo").val();
			data["constValidFromDate"] = $("#constBookingPeriodFrom").val();
			data["constValidToDate"] = $("#constBookingPeriodTo").val();
		}
		else{
			if($("#selpromoType :selected").val() == "Buy And Get Free"){
				data["promotionCriteriaTO.applyTO"] = $("#selApplyToDiscountForBuyAndGetFree").val();
			}else{
				data["promotionCriteriaTO.applyTO"] = $("#selApplyToAncillary").val();
			}
			data["promotionCriteriaTO.discountType"] = $("#selpromoTypeAncillary").val();
			data["promotionCriteriaTO.discountValue"] = $("#disValueAncillary").val();
			data["promotionCriteriaTO.applyAs"] = $("#selApplyAsAncillary").val();
			
			data["promotionCriteriaTO.constPaxName"] = $("#fsconstPaxName").is(":checked");
			data["promotionCriteriaTO.constPaxSector"] = $("#fsconstSector").is(":checked");
			data["constFlightFromDate"] = $("#fsconstFlightPeriodFrom").val();
			data["constFlightToDate"] = $("#fsconstFlightPeriodTo").val();
			data["constValidFromDate"] = $("#fsconstBookingPeriodFrom").val();
			data["constValidToDate"] = $("#fsconstBookingPeriodTo").val();
		}
		
		return data;
	}
	
	loadLinkPOS = function(){
		var url = "managePromotionCriteria!loadPOSData.action";
		var data = {};
		$.ajax({type: "POST", 
			dataType: 'json',
			data: data ,
			async: false, url:url,
			beforeSend:top[2].ShowProgress(),
			complete:function(response){
				loadLinkPOSDataProcess(response);
			}
			
		});
	}

	loadLinkPOSDataProcess = function(response) {
		var responseData=$.parseJSON(response.responseText);
		if (response.success) {	
			$("#lstPOS").multiSelect({
				data:responseData.pos,
				leftListBox:"lstPOS",
				label:"Country",
				rightWidth:"200px",
				leftWidth:"200px"
			});			
		top[2].HideProgress();
		}
		$("#addAllRight_lstPOS").trigger('click');
	}
	
	function getMultiSelectBoxValues(values) {	
		var data = "";
		if (values != null && values != "") {
			for (var i = 0; i < values.length; i++) {
				data += values[i] + ",";
			}
		}
		return data;
	}
	
	getFreeCriteriaData = function(){
		var freeCriteria = {};
		$("#buyNGetBody>tr").each(function(){
			var key=$(this).find("td").eq(0).html();
			var value=$(this).find("td").eq(1).html();
			freeCriteria[key]=value;
		});
		return JSON.stringify(freeCriteria);
	}
	
	getSelectBoxData = function(selectBoxID){
		var selector="#"+selectBoxID+" option"
		var selectedItems=[];
		$(selector).each(function () {
			if(this.value!=""){
				selectedItems.push(this.value);
			}
		});
		return JSON.stringify(selectedItems);
	}
	
	getSelectedSelectBoxData = function(selectBoxID) {
		var selector="#"+selectBoxID+" option:selected"
		var selectedItems=[];
		$(selector).each(function () {
			selectedItems.push(this.value);
		});
		return JSON.stringify(selectedItems);
	}
	
	discountTypeChangeAncillary=function(){
		
		if($("#selpromoTypeAncillary").val()=='VALUE'){
			$("#tdAplAnc").show();
			$("#tdAplAnc1").show();
			$("#selApplyToAncillary").empty();
			$("#selApplyToAncillary").fillDropDown({firstEmpty:true, dataArray:discountApplyValue , keyIndex:0, valueIndex:1});
		//	$("#disValueAncillary").val('0');
		}
		else{
			
			$("#tdAplAnc").hide();
			$("#tdAplAnc1").hide();
			//$("#selApplyToAncillary").fillDropDown({firstEmpty:true, dataArray:discountApplyTo , keyIndex:0, valueIndex:1});
		//	$("#disValueAncillary").val('0.0');
		}
	
	}
	
	discountTypeChange=function(){
		
		if($("#selpromoTypeDiscount").val()=='VALUE'){
			$("#selApplyToDiscount").empty();
			$("#selApplyToDiscount").fillDropDown({firstEmpty:true, dataArray:discountApplyValue , keyIndex:0, valueIndex:1});
		//	$("#disValue").val('0');
		}
		else{
			$("#selApplyToDiscount").empty();
			$("#selApplyToDiscount").fillDropDown({firstEmpty:true, dataArray:discountApplyTo , keyIndex:0, valueIndex:1});
			//$("#disValue").val('0.0');
		}
		
	}
	selApplyToDiscountChange=function(){
		if(($("#selApplyToDiscount").val()==2) && ($("#disValue").val()>=100)){
			$("#disValue").val("");
		}
	}
	valadateForm = function(){
		top[2].HidePageMessage();
		var valied = true;
		if($("#promoName").val()==""){
			showERRMessage(promotionNameRequired);
			return false;
		}
		
		if ($('#enblePromoCode').is(':checked') &&  ($("#promoCode").val()=="") ) 
		{
			showERRMessage(promotionCodeRequired);
			return false;
		}
		
		if($("#numOfCodes").val()!="" && codesArray.length==0 && isNew){
            showERRMessage(noPromoCodesGenerated);                
            return false;
        }
		
		if(!$("#applOneway").is(":checked") && !$("#applReturn").is(":checked")){
			showERRMessage(promotionApplyRequired);
			return false;
		}
		
		if(!(($("#" + registrationDateGridDetails.tableId).getDataIDs().length > 0) || ($("#" + flightDateGridDetails.tableId).getDataIDs().length > 0))){
            showERRMessage(promotionDateRequired);
            return false;
		}
		
		if ($("#sel_Routes>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#sel_FlightNo>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#selChanel").val()==null){
			showERRMessage(promoCodeChlSelectionRequired);
			valied = false;
		}else if($("#sel_Agents>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#sel_COS>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#sel_LCC>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#sel_COS>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#sel_BookinClasses>option").length==0){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}else if($("#selpromoType").val()==""){
			showERRMessage(promoCodeRequiredValueRequired);
			valied = false;
		}
		var ChannelList = document.getElementById("selChanel");
	    for(var i=0; i<ChannelList.selectedOptions.length; i++)
	    {
	       if((ChannelList.selectedOptions[i].label == "Web")) {
	    	   if ($("#lstPOSAssigned>option").length==0){
	    		   showERRMessage(promoCodeRequiredValueRequired);
		    	   valied = false;
	    	   }
	    	   if(!($('#applLoyaltyMember').is(':checked') || $('#applRegtMember').is(':checked') || $('#applGuest').is(':checked') )){
	    		   showERRMessage(promoCodeRequiredValueRequired);
	    	   	   valied = false;
	    	   }	   
	       }
	    }
//		//Discount selected
		if (valied && $("#selpromoType").val()=="Discount"){
			if($("#selpromoTypeDiscount").val()==""){
				showERRMessage(ApplytoDiscountPromoTypeRequired);
				valied = false;
			}else if($("#selApplyToDiscount").val()==""){
				showERRMessage(applyToDiscountRequired);
				valied = false;
			}else if($("#disValue").val()==""){
				showERRMessage(discountValueRequired);
				valied = false;
				
			}
			else if($("#disValue").val()==0){
				showERRMessage(promoCodeDisValNonZeroRequired);
				valied = false;
			}
			else if($("#selApplyToType").val()==""){
				showERRMessage(applyDiscountTypeRequired);
				valied = false;
			}
			
			 
			if(($('#constFlightPeriodFrom').val()!="") ||  ($("#constFlightPeriodTo").val()!="")){
				if(($('#constFlightPeriodFrom').val()!="") &&  ($("#constFlightPeriodTo").val()!="")){
				
					if(new Date(formatDate($('#constFlightPeriodFrom').val(),"dd/mm/yyyy","yyyy/mm/dd"))>new Date(formatDate($("#constFlightPeriodTo").val(),"dd/mm/yyyy","yyyy/mm/dd"))){
						showERRMessage("Flight from date should be before date to flight To date");
						return false;
					}
				}
				else{
					showERRMessage("Please enter flight period for the discount");
					return false;
				}
			}
			if(($('#constBookingPeriodFrom').val()!="") ||  ($("#constBookingPeriodTo").val()!="")){
				
				if(($('#constBookingPeriodFrom').val()!="") &&  ($("#constBookingPeriodTo").val()!="")){
					
					if(new Date(formatDate($('#constBookingPeriodFrom').val(),"dd/mm/yyyy","yyyy/mm/dd"))>new Date(formatDate($("#constBookingPeriodTo").val(),"dd/mm/yyyy","yyyy/mm/dd"))){
						showERRMessage("Booking from date should be before date to Booking to date");
						return false;
					}
				}
				else{
					showERRMessage("Please enter flight period for the discount");
					return false;
				}
			}
		}
		//Free Servise selected
	if (valied && $("#selpromoType").val()=="Free Service"){
			if ($("#sel_Anicillaries").val()==""){
				showERRMessage(ancillariesRequired);
				valied = false;
			}else if($("#selpromoTypeAncillary").val()==""){
				showERRMessage(ApplytoDiscountPromoTypeRequired);
				valied = false;
			}
			else if($("#disValueAncillary").val()==""){
				showERRMessage(discountValueRequired);
				valied = false;
			}
			else if($("#disValueAncillary").val()==0){
				showERRMessage(promoCodeDisValNonZeroRequired);
				valied = false;
			}
			if($("#selpromoTypeAncillary").val()=="VALUE"){
			  if($("#selApplyToAncillary").val()==""){
				showERRMessage(applyToDiscountRequired);
				valied = false;
			  }
			}
			 
			if(($('#fsconstFlightPeriodFrom').val()!="") ||  ($("#fsconstFlightPeriodTo").val()!="")){
				if(($('#fsconstFlightPeriodFrom').val()!="") &&  ($("#fsconstFlightPeriodTo").val()!="")){
				
					if(new Date(formatDate($('#fsconstFlightPeriodFrom').val(),"dd/mm/yyyy","yyyy/mm/dd"))>new Date(formatDate($("#fsconstFlightPeriodTo").val(),"dd/mm/yyyy","yyyy/mm/dd"))){
						showERRMessage("Flight from date should be before date to flight to date");
						return false;
					}
				}
				else{
					showERRMessage("Please enter valid flight period for the free service");
					return false;
				}
			}
			if(($('#fsconstBookingPeriodFrom').val()!="") ||  ($("#fsconstBookingPeriodTo").val()!="")){
				
				if(($('#fsconstBookingPeriodFrom').val()!="") &&  ($("#fsconstBookingPeriodTo").val()!="")){
					
					if(new Date(formatDate($('#fsconstBookingPeriodFrom').val(),"dd/mm/yyyy","yyyy/mm/dd"))>new Date(formatDate($("#fsconstBookingPeriodTo").val(),"dd/mm/yyyy","yyyy/mm/dd"))){
						showERRMessage("Booking from date should be before date to Booking To date");
						return false;
					}
				}
				else{
					showERRMessage("Please enter valid booking period for the free service");
					return false;
				}
			}
		}
		//Buy & Get Free selected
		if (valied && $("#selpromoType").val()=="Buy And Get Free"){
			if ($("#buyNGetBody>tr").length==0){
				showERRMessage(promoCodeRequiredValueRequired);
				valied = false;
			}else 
				if($("#limitedLoadFactor").val()==""){
				showERRMessage(loadFactorRequired);
				valied = false;
			}
		}
		
		return valied;
	}
	
	function ShowProgress(){
		top[2].ShowProgress();
	}
	function HideProgress(){
		top[2].HideProgress();
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}
	function ShowDelete() {				
	    return confirm("Selected promo code will be deleted.Do you wish to continue?"); 
	}
	resetPromoCode=function(){
		$('#sel_Routes').empty();
		$('#sel_FlightNo').empty();
		$('#sel_COS').empty();
		$('#sel_LCC').empty();
		$('#sel_Agents').empty();
		$('#sel_BookinClasses').empty();
		var appenRouteItem = $("<option>All</option>").attr("value", '');
		$("#sel_Routes,#sel_FlightNo,#sel_COS,#sel_LCC,#sel_Agents,#sel_BookinClasses").append(appenRouteItem);
		$("#selChanel").val([]);
		
		$('#sel_BIN').empty();
		$("#registrationFrom").val("");
		$("#registrationTo").val('');
		$("#flightPeriodFrom").val('');
		$("#flightPeriodTo").val('');
		resetDatePeriodGrid();
		
		$("#chkCancelation").attr("checked", false);
		$("#chkModification").attr("checked", false);
		$("#chkSplitting").attr("checked", false);
		$("#applOneway,#applReturn").attr("checked", false);
		
		//applicable member selection
		$("#applLoyaltyMember,#applRegtMember,#applGuest").attr("checked", true);
		$("#applicability").css('display','none');
		$("#webPOS").css('display','none');
		
		$("#limitPerFlight").val("");
		$("#limitTotal").val("");
		$("#numByAgent").val("");
		$("#numBySalesChannel").val("");
		$("#promoName").val("");
		$("#selStatus_1").val("")
		
		$("#enblePromoCode").attr("checked", false);
		$("#promoCode").val("");
		$("#disValue, #disValueAncillary").val("");
		$("#limitedLoadFactor").val("");
		$("#selpromoType").val("-");
		$("#buyNGetBody").empty();
		$("#FlightNo").val("");
		$("#adultPCount,#childPCount,#infantPCount,#adultFCount,#childFCount,#infantFCount").val("");
		$("#selpromoType,#selApplyToDiscount,#selApplyToAncillary,#selApplyToType,#selApplyAsAncillary,#selpromoTypeDiscount, #selpromoTypeAncillary,#sel_Anicillaries,#selApplyToDiscountForBuyAndGetFree").empty();
		$("#selChanel").empty();
	//	$("#selApplyToDiscount,#selApplyToAncillary,#selpromoTypeDiscount,#selpromoTypeAncillary").empty();
		$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").val('');
		$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom,  #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo").val('');
		$("#fsconstPaxName,#fsconstSector").attr("checked", false);
		$("#constPaxName,#constSector").attr("checked", false);
		$("#appliciInBound").attr("checked", false);
		$("#appliciOutBound").attr("checked", false);
		//	$("#selApplyToType,#selApplyAsAncillary,#sel_Anicillaries").empty();
		
		$('#numOfCodes').val("");
		$("#beforeGenerate").show();
		$("#afterGenerate").hide();
		$("#txtOndSearch").val("");
		$("#sel_GeneratedRoutes").empty();
		$("#btnView").hide();
		$("#btnGenerate").show();
		$("#btnGenerate").attr("disabled", true);
		$("#enblePromoCode").attr("disabled", false);
		$("#promoCode").attr("disabled", true);
		$("#numOfCodes").attr("disabled",false);
		promoCodes = new Array(); 
		codesGenerated = false;
		codesArray = new Array();

	}
	function SaveMsg(msg) {
		top[2].objMsg.MessageText = msg;
		top[2].objMsg.MessageType = "Confirmation";
		top[2].ShowPageMessage();
	}
	
	function CreateGetAndBuyTbl(str){
		var tr="";
		var obj=JSON.parse(str);
		for(var key in obj) {
		    var value = obj[key];
		    tr=tr+"<tr><td>"+key+"</td><td>"+value+"</td></tr>";
		}
	
		return tr;
	}
	
	function formatDate(dt,fmt1,fmt2){
	    var dd1,mm1,yy1;
	    var dd2,mm2,yy2;
	    var dt2,sep;

	    if(fmt1=="dd-mmm-yyyy"){
	        dd1=dt.substr(0,2);
	        mm1=dt.substr(3,3);
	        yy1=dt.substr(7,4);
	    }
	    else if(fmt1=="dd-mmm-yy"){
	        dd1=dt.substr(0,2);
	        mm1=dt.substr(3,3);
	        yy1=dt.substr(7,2);
	    }
	    else if((fmt1=="dd/mm/yyyy")||(fmt1=="dd-mm-yyyy")){
	        dd1=dt.substr(0,2);
	        mm1=dt.substr(3,2);
	        yy1=dt.substr(6,4);
	    }
	    else if((fmt1=="dd/mm/yy")||(fmt1=="dd-mm-yy")){
	        dd1=dt.substr(0,2);
	        mm1=dt.substr(3,2);
	        yy1=dt.substr(6,2);
	    }
	    else if((fmt1=="yyyy/mm/dd")||(fmt1=="yyyy-mm-dd")){
	        dd1=dt.substr(8,2);
	        mm1=dt.substr(5,2);
	        yy1=dt.substr(0,4);
	    }
	    else if((fmt1=="yy/mm/dd")||(fmt1=="yy-mm-dd")){
	        dd1=dt.substr(6,2);
	        mm1=dt.substr(3,2);
	        yy1=dt.substr(0,2);
	    }
	    else{
	       // alert("formatDate:Invalid format1:" + fmt1);
	    }
	    //***********************

	    //get seperator
	    if(fmt2.indexOf("/")!=-1){
	        sep="/";
	    }else{
	        sep="-";
	    }

	    //get year
	    if(fmt2.indexOf("yyyy")!=-1){
	        if(yy1.length==2){
	            if(Number(yy1)<30){
	                yy2=2000;
	            }else{
	                yy2=1900;
	            }
	            yy2+=Number(yy1);
	        }else{
	            yy2=yy1;
	        }
	    }else{
	        if(yy1.length==4){
	            yy2=yy1.substr(2,2);
	        }else{
	            yy2=yy1;
	        }
	    }

	    //get month
	    if(fmt2.indexOf("mmm")!=-1){
	        if(mm1.length==2){
	            mm2=getStrMonth(mm1);
	        }else{
	            mm2=mm1;
	        }
	    }
	    else{
	        if(mm1.length==3){
	            mm2=getNoMonth(mm1);
	            return (yy1+"/"+mm2+"/"+dd1);
	        }else{
	            mm2=mm1;
	        }
	    }

	    //get day
	    dd2=dd1;

	    //date

	    if(fmt2.indexOf("yy")<4){
	        dt2=yy2+sep+mm2+sep+dd2;
	    }else{
	        dt2=dd2+sep+mm2+sep+yy2;
	    }

	    return dt2;
	}
	
	getStatusDataFromPage = function(){
		var data={};
		
		
		data["promotionCriteriaTO.version"] = selectedRowData.criteria.version;
		data["promotionCriteriaTO.promoCriteriaID"] = selectedRowData.criteria.promoCriteriaID;
		data["promotionCriteriaTO.promoCriteriaID"] = selectedRowData.criteria.promoCriteriaID;
		data["promotionCriteriaTO.promotionCriteriaType"] =selectedRowData.criteria.promotionCriteriaType;
		if(selectedRowData.criteria.status=='ACT'){
			data["promotionCriteriaTO.status"] ='INA';
		}
		else{
			data["promotionCriteriaTO.status"] ='ACT';
		}
		
		return data;
		
	}
	
	makePromoCodeStatusUpdate = function(){
		var submitData = getStatusDataFromPage();
		var targetURL= 'managePromotionCriteria!statusUpdate.action';
	
		
		
		$.ajax({ 
			url : targetURL,
			beforeSend : ShowProgress(),
			data : submitData,
			type : "POST",
			async:false,
			dataType : "json",
			complete:function(response){
				 var responseData=$.parseJSON(response.responseText);
				 HideProgress();
				 if(responseData.success){
					 searchPromoCodes();
					 SaveMsg("Promo code updated successfully!")
					 alert("Promo code  updated successfully!");
				 } else{
					 if(responseData.messageTxt=="DPR"){
						 showERRMessage(promoCodeUniqueValueRequired);
					 }
					 alert("Operation Failed!");
				 }
			}
		});
	
	}
	
	disablePromoCode=function(){
		$("#promoName").attr("disabled", true);	
		$("#enblePromoCode").attr("disabled", true);
		$("#promoCode").attr("disabled", true);
		$("#numOfCodes").attr("disabled", true);
		//$("#btnGenerate").hide();
		$("#limitedLoadFactor").attr("disabled", true);
		$("#selpromoType").attr("disabled", true);
		$(".addItem").attr("disabled", true);
		$(".removeItem").attr("disabled", true);
		$("#sel_Anicillaries").attr("disabled", true);
		
		$("#selApplyToType,#selApplyToDiscount,#selpromoTypeDiscount,#disValue").attr("disabled", true);
		$("#selpromoTypeAncillary,#selApplyToAncillary,#selApplyAsAncillary,#disValueAncillary").attr("disabled", true);
		$("#limitedLoadFactor").attr("disabled", true);
		$("#applOneway,#applReturn").attr("disabled", true);
		
		$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").datepicker("disable");
		$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom,  #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo").datepicker("disable");
		$("#fsconstPaxName,#fsconstSector").attr("disabled", true);
		$("#constPaxName,#constSector").attr("disabled", true);
		$("#appliciInBound").attr("disabled", true);
		$("#appliciOutBound").attr("disabled", true);
		$("#btnReset").attr("disabled", true);
		
	}
	
	enableEditablePromocodeFields= function(){		
		$("#add_Agents").attr("disabled", false);
		$("#del_Agents").attr("disabled", false);
		$("#add_COS").attr("disabled", false);
		$("#del_COS").attr("disabled", false);
		$("#add_BookinClasses").attr("disabled", false);
		$("#del_BookinClasses").attr("disabled", false);
		$("#add_FlightNo").attr("disabled", false);
		$("#del_FlightNo").attr("disabled", false);
		
	}
	enablePromoCode=function(){
		$("#promoName").attr("disabled", false);
		$("#btnGenerate").attr("disabled", true);
		$("#limitPerFlight").attr("disabled", false);
		$('#limitTotal').attr("disabled", false);
		$("#numByAgent").attr("disabled", false);
		$("#numBySalesChannel").attr("disabled", false);
		
		$("#enblePromoCode").attr("disabled", false);
		$("#promoCode").attr("disabled", true);
		$("#numOfCodes").attr("disabled", false);
		
		
		$("#limitedLoadFactor").attr("disabled", false);
		$("#selpromoType").attr("disabled", false);
		$(".addItem").attr("disabled", false);
		$(".removeItem").attr("disabled", false);
		$("#selChanel,#sel_Anicillaries").attr("disabled", false);
		
		$("#selApplyToType,#selApplyToDiscount,#selpromoTypeDiscount,#disValue").attr("disabled", false);
		$("#selApplyAsAncillary,#selApplyToAncillary,#selpromoTypeAncillary,#disValueAncillary").attr("disabled", false);
		$("#limitedLoadFactor").attr("disabled", false);
		$("#chkCancelation").attr("disabled", false);
		$("#chkModification").attr("disabled", false);
		var selected = $("#selpromoType>option:selected").val();
		if(selected=='Buy And Get Free'){//if buy and get one free selected
			$("#chkSplitting").attr("disabled", true);
		}
		else{
			$("#chkSplitting").attr("disabled", false);
		}
		$("#chkRemovePax").attr("disabled", false);
		$("#applOneway,#applReturn").attr("disabled", false);
		
		$("#registrationFrom, #registrationTo, #flightPeriodFrom, #flightPeriodTo").datepicker("enable");
		$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").datepicker("enable");
		$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom,  #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo").datepicker("enable");
		$("#fsconstPaxName,#fsconstSector").attr("disabled", false);
		$("#constPaxName,#constSector").attr("disabled", false);
		$("#appliciInBound").attr("disabled", false);
		$("#appliciOutBound").attr("disabled", false);
		$("#addOndCodes").attr("disabled", false);
		$("#removeOndCodes").attr("disabled", false);
		$("#addAllOndCodes").attr("disabled", false);
		$("#removeAllOndCodes").attr("disabled", false);
		$("#btnReset").attr("disabled", false);	
	}
	
	//validate and enable date fields editable
	validateDateFields=function(){
		var d = new Date();
		
		if(new Date(formatDate($("#registrationFrom").val(),"dd/mm/yyyy","yyyy/mm/dd")) < d){
			$("#registrationFrom").datepicker("disable");
		}else{
			$("#registrationFrom").datepicker("enable");
		}
		$("#registrationTo").datepicker("enable");
		if(new Date(formatDate($("#flightPeriodFrom").val(),"dd/mm/yyyy","yyyy/mm/dd")) < d){
			$("#flightPeriodFrom").datepicker("disable");
		}else{
			$("#flightPeriodFrom").datepicker("enable");
		}
		$("#flightPeriodTo").datepicker("enable");

	}
	
//Multiple reservation and flight period section
	
	constructDatePeriodGrid=function(dateGridIds){
		$("#" + dateGridIds.gridId).empty();
		var gridTable = $("<table></table>").attr("id", dateGridIds.tableId);
		$("#" + dateGridIds.gridId).prepend(gridTable);
		
		$("#" + dateGridIds.tableId).jqGrid({
			datatype: "local",			
			width: 400,
			height: 'auto',
			colNames: ['&nbsp;', 'From', 'To'],
			colModel: [
				{name:'id',index:'id',align:"center",hidden:true},
				{name:'from',index:'from',width:190,align:"center"},
				{name:'to',index:'to',width:190,align:"center"}
			],
			multiselect:false,
			viewrecords: true,
			rowNum:5,
			sortable:true,
			altRows:true,
			scrollOffset:0,
			altclass:"GridAlternateRow",			
			beforeSelectRow: function(rowid){
				if(dateGridIds.isEdit){
					alert("Please save/clear date periods before edit");
					return false;
				}
				return true;
			},
			onSelectRow: function(rowid){				
				dateGridIds.selectedData = $("#" + dateGridIds.tableId).getRowData(rowid);
				editOrDeleteDatePeriod(dateGridIds, 'edit');
				$("#" + dateGridIds.clrBtnId).attr("disabled",false);
			}
		});
		
		$("#" + dateGridIds.delBtnId).hide();
		$("#" + dateGridIds.clrBtnId).hide();
		$("#" + dateGridIds.delBtnId).attr("disabled",true);
		$("#" + dateGridIds.clrBtnId).attr("disabled",true);
	}

	addOrSaveDatePeriod=function(dateGridIds){
		if(($("#" + dateGridIds.fromId).val()=="") ||  ($("#" + dateGridIds.toId).val()=="")){
			alert(dateGridIds.alertMessages.required);
			return;
		}else{
			if(new Date(formatDate($("#" + dateGridIds.fromId).val(),"dd/mm/yyyy","yyyy/mm/dd"))>new Date(formatDate($("#" + dateGridIds.toId).val(),"dd/mm/yyyy","yyyy/mm/dd"))){
				alert(dateGridIds.alertMessages.invalid);
				return;
			}	
		}

		if($("#" + dateGridIds.tableId).getDataIDs().length >= maxDateCount){
			alert(dateGridIds.alertMessages.maxlength);
			$("#" + dateGridIds.fromId).val('');
			$("#" + dateGridIds.toId).val('');
			return;		
		}
					
		var datePeriod = {from: $("#" + dateGridIds.fromId).val(), to: $("#" + dateGridIds.toId).val()};
		var dateRanges = getAllDatesFromRange(datePeriod.from, datePeriod.to);				
		if(!checkOverlappingPeriod(dateGridIds.disabledDays, dateRanges)){
			dateRanges.forEach(function(element){
				dateGridIds.disabledDays.push(element);			
			});	
		}else{
			alert(dateGridIds.alertMessages.overlap);
			return;
		}
							
		if(!dateGridIds.isEdit){
			if($("#" + dateGridIds.tableId).getDataIDs().length == 0){
				$("#" + dateGridIds.delBtnId).show();
				$("#" + dateGridIds.clrBtnId).show();
			}
			datePeriod.id = dateGridIds.dateCount++;			
			$("#" + dateGridIds.tableId).addRowData(datePeriod.id, datePeriod, 'last');
		}else{
			$("#" + dateGridIds.tableId).setRowData(dateGridIds.selectedData.id, datePeriod);
			$("#" + dateGridIds.addBtnId).val('Add');
			dateGridIds.selectedData = null;
			dateGridIds.isEdit = false;
			$("#" + dateGridIds.tableId).resetSelection();
			validationForDatePeriod(dateGridIds);
			$("#" + dateGridIds.delBtnId).attr("disabled",true);
			$("#" + dateGridIds.clrBtnId).attr("disabled",true);
		}
		$("#" + dateGridIds.fromId).val('');
		$("#" + dateGridIds.toId).val('');
	}

	validationForDatePeriod=function(dateGridIds){
		if(dateGridIds.isEdit){
			if(!isNew){
				if(new Date(formatDate($("#" + dateGridIds.toId).val(),"dd/mm/yyyy","yyyy/mm/dd")) < new Date()){
					$("#" + dateGridIds.fromId).datepicker("disable");
					$("#" + dateGridIds.toId).datepicker("disable");
					$("#" + dateGridIds.delBtnId).attr("disabled", true);
				}else if(new Date(formatDate($("#" + dateGridIds.fromId).val(),"dd/mm/yyyy","yyyy/mm/dd")) < new Date()){
					$("#" + dateGridIds.fromId).datepicker("disable");
					$("#" + dateGridIds.delBtnId).attr("disabled", true);
				}else{
					$("#" + dateGridIds.delBtnId).attr("disabled", false);
				}
			}else{
				$("#" + dateGridIds.delBtnId).attr("disabled", false);
			}
		}else{
			$("#" + dateGridIds.fromId).datepicker("enable");
			$("#" + dateGridIds.toId).datepicker("enable");	
		}
	}
	
	editOrDeleteDatePeriod=function(dateGridIds, type){		
		if(type == 'edit'){
			dateGridIds.isEdit = true;
			$("#" + dateGridIds.addBtnId).val('Save');
			$("#" + dateGridIds.fromId).val(dateGridIds.selectedData.from);
			$("#" + dateGridIds.toId).val(dateGridIds.selectedData.to);
		}else{
			dateGridIds.isEdit = false;
			$("#" + dateGridIds.tableId).delRowData(dateGridIds.selectedData.id);
			$("#" + dateGridIds.addBtnId).val('Add');
			$("#" + dateGridIds.fromId).val('');
			$("#" + dateGridIds.toId).val('');
			$("#" + dateGridIds.delBtnId).attr("disabled",true);
			$("#" + dateGridIds.clrBtnId).attr("disabled",true);
			if($("#" + dateGridIds.tableId).getDataIDs().length == 0){
				$("#" + dateGridIds.delBtnId).hide();
				$("#" + dateGridIds.clrBtnId).hide();
			}
		}
		validationForDatePeriod(dateGridIds);
		removeAllDatesFromRange(dateGridIds.selectedData.from, dateGridIds.selectedData.to, dateGridIds.disabledDays);
	}

	getDatePeriodFormData=function(dateGridIds){
		var datePeriods = [];
		var rowIds = $("#" + dateGridIds.tableId).getDataIDs();
		for(var i = 0; i < rowIds.length; i++){
			rowData = $("#" + dateGridIds.tableId).getRowData(rowIds[i]);
			datePeriods.push(rowData.from + "-" + rowData.to);
		}
		return JSON.stringify(datePeriods);
	}
	
	sortDatePeriods=function(data){
		for(var i=0; i < data.length-1; i++){
			var min_id = i;
			var selDate = new Date(formatDate(data[i].split("-")[0],"dd/mm/yyyy","yyyy/mm/dd"));
			for(var j=i+1; j < data.length; j++){
				if(new Date(formatDate(data[j].split("-")[0],"dd/mm/yyyy","yyyy/mm/dd")) < selDate){
					min_id = j;
				}
			}
			var tmp_data = data[min_id];
			data[min_id] = data[i];
			data[i] = tmp_data;	
		}
		return data;
	}
	
	setDatePeriodFormData=function(dateGridIds, data){
		var sortedDates = sortDatePeriods(data);
		$.each(sortedDates, function(){
			var dates = this.split("-");
			var datePeriod = {id: dateGridIds.dateCount++,from: dates[0], to: dates[1]};		
			var dateRanges = getAllDatesFromRange(datePeriod.from, datePeriod.to);				
			dateRanges.forEach(function(element){
				dateGridIds.disabledDays.push(element);			
			});
			$("#" + dateGridIds.tableId).addRowData(datePeriod.id, datePeriod, 'last');
		});
		$("#" + dateGridIds.delBtnId).show();
		$("#" + dateGridIds.clrBtnId).show();
	}
	
	clearDatePeriods=function(dateGridIds){
		if(dateGridIds.isEdit){
			dateGridIds.isEdit = false;
			$("#" + dateGridIds.tableId).resetSelection();
			$("#" + dateGridIds.addBtnId).val('Add');
			var dateRanges = getAllDatesFromRange(dateGridIds.selectedData.from, dateGridIds.selectedData.to);				
			dateRanges.forEach(function(element){
				dateGridIds.disabledDays.push(element);			
			});
		}
		$("#" + dateGridIds.fromId).val('');
		$("#" + dateGridIds.toId).val('');
		validationForDatePeriod(dateGridIds);
		$("#" + dateGridIds.delBtnId).attr("disabled",true);
		$("#" + dateGridIds.clrBtnId).attr("disabled",true);
	}
	
	resetDatePeriodGrid=function(){
		$("#multipleRegisterDateTable ,#multipleFlightDateTable").clearGridData();
		registrationDateGridDetails.disabledDays = [];
		registrationDateGridDetails.dateCount = 1;
		registrationDateGridDetails.isEdit = false;
		registrationDateGridDetails.selectedData = null;
		flightDateGridDetails.disabledDays = [];
		flightDateGridDetails.dateCount = 1;		
		flightDateGridDetails.isEdit = false;
		flightDateGridDetails.selectedData = null;
		$("#" + registrationDateGridDetails.delBtnId).hide();
		$("#" + registrationDateGridDetails.clrBtnId).hide();
		$("#" + flightDateGridDetails.delBtnId).hide();
		$("#" + flightDateGridDetails.clrBtnId).hide();
		
	}
	
	getAllDatesFromRange=function(from, to){
		var datesRange = [];		
		var currentDate = from;
		var endDate = new Date(formatDate(to,"dd/mm/yyyy","yyyy/mm/dd"));
		while (new Date(formatDate(currentDate,"dd/mm/yyyy","yyyy/mm/dd")) <= endDate) {
			datesRange.push(currentDate);			
			var date = new Date(formatDate(currentDate,"dd/mm/yyyy","yyyy/mm/dd"));
	  		date.setDate(date.getDate() + 1);
			currentDate = convertDateToString(date);
		}
		return datesRange;
	}

	checkOverlappingPeriod=function(parentRange, childRange){
		for(var i = 0; i < childRange.length; i++){
			if($.inArray(childRange[i], parentRange) > -1){
				return true;			
			}
		}
		return false;
	}

	removeAllDatesFromRange=function(from, to, daysArray){
		var currentDate = from;
		var endDate = new Date(formatDate(to,"dd/mm/yyyy","yyyy/mm/dd"));
		while (new Date(formatDate(currentDate,"dd/mm/yyyy","yyyy/mm/dd")) <= endDate) {
			var index = $.inArray(currentDate, daysArray);
			if(index > -1){
				daysArray.splice(index, 1);			
			}
			var date = new Date(formatDate(currentDate,"dd/mm/yyyy","yyyy/mm/dd"));
	  		date.setDate(date.getDate() + 1);
			currentDate = convertDateToString(date);
		}
	}

	convertDateToString=function(date){
		var d = new Date(date);
		var days = (d.getDate() < 10) ? ("0" + d.getDate()): ("" + d.getDate());
		var month = (d.getMonth() < 9) ? ("0" + (d.getMonth() + 1)): ("" + (d.getMonth() + 1));
		return (days + "/" + month + "/" + d.getFullYear());	
	}
	
	//End of multiple reservation and flight period section
	
	resetPromoCodeGrid=function(){
		$("#jqGridPromoCodesContainer").empty();
		$("#selFrom,#selTo,#txtStartDateSearch,#txtStopDateSearch,#txtPromotionCodes").val('')
		$("#selStatus").val("-")
	
	}
	resetBtnPromoCode=function(){
		 resetPromoCode();
		 loadInitdata();
		 showPromoType();
	}
	
	viewPromoCodes = function () {
		if(!isNew){
			initMultiPromoCodesPopUP(selectedRowData.criteria.promoCodesTo);
		}else{
			initMultiPromoCodeDialog();
		}
	}
	
	changeDiscountApplyToTypeValues=function(){
		if($("#selApplyToType").val()=='Credit'){
			$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").datepicker("enable");
			$("#constPaxName,#constSector").attr("disabled", false);
		}
		else {
			$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").val('');
			$("#constFlightPeriodTo, #constFlightPeriodFrom,  #constBookingPeriodFrom, #constBookingPeriodTo").datepicker("disable");
			$("#constPaxName,#constSector").attr("checked", false);
			$("#constPaxName,#constSector").attr("disabled", "disabled");
		}
	}
	changeFSApplyToTypeValues=function(){
		if($("#selApplyAsAncillary").val()=='Credit'){
			$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom,  #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo").datepicker("enable");
			
			$("#fsconstPaxName,#fsconstSector").attr("disabled", false);
		}
		else {
			$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom, #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo").val('');
			$("#fsconstFlightPeriodTo, #fsconstFlightPeriodFrom, #fsconstBookingPeriodFrom, #fsconstBookingPeriodTo").datepicker("disable");
			$("#fsconstPaxName,#fsconstSector").attr("checked", false);
			$("#fsconstPaxName,#fsconstSector").attr("disabled", "disabled");
		}
	}
	function populateBuyNGetPromoSequence(){
		$('#adultPCount').watermark('ANY');
		$('#childPCount').watermark('ANY');
		$('#infantPCount').watermark('ANY');
		$('#adultFCount').watermark('ALL');
		$('#childFCount').watermark('ALL');
		$('#infantFCount').watermark('ALL');
	}

	
	exportPromoCodes = function () {
		if (selectedRowData != null) {
			var promoCriteriaId = selectedRowData.criteria.promoCriteriaID;
			var objForm = document.createElement("form");
			objForm.setAttribute('id',"exportForm");
			objForm.setAttribute('action',"exportPromotionCriteria.action");
			objForm.setAttribute('target',"CWindow");
			objForm.setAttribute('method',"post");
			var inputElement = document.createElement("input"); 
			inputElement.setAttribute('type',"text");
			inputElement.setAttribute('id',"promoCriteriaIdHdn");
			inputElement.setAttribute('name',"promoCriteriaIdHdn");
			inputElement.setAttribute('value',promoCriteriaId);

			objForm.appendChild(inputElement);
			$("#promoCriteriaIdHdn").val(promoCriteriaId);		
			objForm.style.display="none";
			document.getElementsByTagName('body')[0].appendChild(objForm);
			objForm.submit();
			
		} else {
			showERRMessage("Select a promotion criteria to export");
		}
	}
	
	function addToSelectPaneCustom(src, dst, isSelected) {
		$("#"+dst+" option[value='']").remove();
		var selected = '';
		if (isSelected) {
			selected = ':selected';
		}
		 var options = $('#'  + src + ' option' + selected).sort().clone();
		 $('#' + dst ).append(options);	
		 $('#' + src + ' option' + selected).remove();
		 
	}

	function removeDuplicationFromSelectBox(src) {
		$('#'+ src + ' option').each(function() {
			  $(this).prevAll('option[value="' + this.value + '"]').remove();
		});
		
		var routeData={};
		 routeData['selectedRoute']=$('#sel_Routes').val();
		 if (routeData['selectedRoute']!=null){
			 loadApplicableFlightNumbers(routeData);
		 }
	}
	
	loadApplicableFlightNumbers = function(routeData){		
		$.ajax({ 
			url : 'loadPromotionFormInitData!getSeletedFlights.action',
			beforeSend : ShowProgress(),
			data:routeData,
			type : "GET",
			async:false,
			dataType : "json",
			complete:function(response){
				 var response = eval("(" + response.responseText + ")");
				 dataMap.FlightNo = response.selectedFlights;
				 fillFlightData();
				 HideProgress();
			}
		});
	}
	
	function resetRelavantFieldsForCopy(){
		$("#promoCode").val("");
		$("#numOfCodes").val("");
		$("#constFlightPeriodFrom").val("");
		$("#constFlightPeriodTo").val("");
		$("#constBookingPeriodFrom").val("");
		$("#constBookingPeriodTo").val("");   
		$("#btnView").hide();
		$("#btnGenerate").show();
		$("#afterGenerate").hide();
		$("#beforeGenerate").show();
		$("#enblePromoCode").attr("checked", false);
		$("#registrationFrom").val("");
		$("#registrationTo").val("");
		$("#flightPeriodFrom").val("");
		$("#flightPeriodTo").val("");
		changeDiscountApplyToTypeValues();
		changeFSApplyToTypeValues();
		resetDatePeriodGrid();
	}	

}
$( function() {	
    $("#txtOndSearch")
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if (event.keyCode == 13) {        	
        	//$('#txtOndSearch').autocomplete('close');
        	generateOndCodes(event);        	
        }
      })
      /*.autocomplete({    	 
    	  source: dataMap.SearchTags
      })*/;
  } );


function generateOndCodes() {
	var searchTxt = $("#txtOndSearch").val();
	//showProgressBar();
	if (validateSearchText(searchTxt)) {
		$.ajax({ 
			url : 'smartSearchServices!search.action',
			data : {
				'searchTO.searchText' : function(){return  searchTxt;},
				'searchTO.searchType' : function(){return  "OND_CODE";}
			 },	
		    beforeSend : function() {
		    	clearSearchBox();
		    },
			dataType : "json",
			cache:false,
			success:function(response){
				createONDSelectBox(response.searchTO.searchResult);
			}
		});
	}
}

function clearSearchBox() {
	$('#sel_GeneratedRoutes option').remove();
}

function validateSearchText(searchText) {
	var valid = true;
	if (searchText.length == 0) {
		clearSearchBox();
		valid = false;
	}
	
	return valid;
}

function createONDSelectBox(listOfOndCodes) {
	if (validateSearchText) {
		addOptionsToSelect("sel_GeneratedRoutes", listOfOndCodes);
	}
}

function addOptionsToSelect(selectID,values){
	if(values != null && values.length > 0){
		$("#" + selectID +" option[value='']").remove();
	}
	$.each(values,function(index,value){
		var displayText = replaceall(value,"***","All");
		$("#"+selectID).append('<option value='+value+'>'+displayText+'</option>');
	});
}

UI_promoCode = new UI_promoCode();
UI_promoCode.ready();


function PopUpData(){
	var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Promo Code Description For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
		html += '<select name="language" id="language"  style="width:150px; "> ';
		html += '<option value="-1"></option>'+languageHTML;				
		html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addPCForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removePCForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="min-width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>Promo Code Description* </font></td> <td align="left" valign="top"><input name="txtPCForDisplayOL" type="text" id="txtPCForDisplayOL" size="32" maxlength="125" /></td> </tr> '; 

		html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="savePCForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

	DivWrite("spnPopupData",html);
}

function POSenable(){	
	
	var selectID=getValue("hdnSelectID");
	selectedRowData = $.data($("#jqGridPromoCodesData")[0], "gridData")[selectID];
	if(selectedRowData.criteria!=null){
	
	
	setField("hdnPromoCodeID", selectedRowData.criteria.promoCriteriaID);
	//setField("hdnModel", "SAVELISTFORDISP");
	//setMealForDisplayList();
	showPopUp(1);
	}
}

function POSdisable(bFromClose){	
	

	 hidePopUp(1);
}


function addPCForDisplay(){			
	
	if(getValue('txtPCForDisplayOL').length ==0){
		return false;
	}
	var selLang = getValue("language");
	if(selLang == '-1')
		return false;
	if(findValueInList(selLang, "translation") == true){
		alert("Promo code description for the selected language already exist");
		return false;
	}

	var selLangName = getCurSelectedLabel("language");
	var mealDesc = getValue("txtPCForDisplayOL");
	
	addToList("translation", selLangName+"=>"+mealDesc, selLang);	
	setField("txtPCForDisplayOL", "");

	setField("language","-1");

}
function removePCForDisplay(){			

	var pcList = getFieldByID("translation");
	var selected = pcList.selectedIndex;
	if(selected != -1){
		var langCode = pcList.options[selected].value;
		var label = pcList.options[selected].text.split("=>");
		setField("language", langCode);
		setField("txtPCForDisplayOL", label[1]);
		
		pcList.remove(selected);
	}
}
function savePCForDisplay(){			
	
	if(getValue('txtPCForDisplayOL').length>0 && getValue("language")!='-1'){
		if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
			return false;
	}
//	var strMode = getText("hdnMode");
	var pcList = getFieldByID("translation");
	var size = pcList.length;
	var langCode;
	var pcName;
	
	var label;
	var str="";
	var size;
	var i=0;
	var j=0;
	var newData = new Array();
	var count = 0;
	//////////
	var pcTranslations = jQuery("#listPC").getCell($("#rowNo").val(),'pcForDisplay');
	var arrMealTrl = new Array();
	if(pcTranslations != null && pcTranslations != "")
		arrMealTrl = pcTranslations.split("~");
	var mealTrl;
	grdArray = new Array();
	for(var i =0;i<arrMealTrl.length-1;i++){
		grdArray[i] = new Array(); 
		mealTrl = arrMealTrl[i].split(",");
		grdArray[i][0] = mealTrl[0];
		grdArray[i][1] = mealTrl[1];
		grdArray[i][2] = getValue('hdnPromoCodeID');
		grdArray[i][3] = mealTrl[3];
		
	//	grdArray[i][5] = mealTrl[5];
	}
	////////////
	for(i=0;i<pcList.length;i++){
		langCode = pcList.options[i].value;
		label = pcList.options[i].text.split("=>");
		pcName = label[1];
		
		size = grdArray.length;
		var found = false;
		for(j=0;j<size;j++){
			if(grdArray[j][0] == langCode){
				newData[count++] = new Array(langCode, pcName, grdArray[j][2], grdArray[j][3]);
				found = true;
				break;
			}
		}
		if(!found){
			newData[count++] = new Array(langCode, pcName, getValue('hdnPromoCodeID'), "-1");//langCode,pcName,id,ver
		
		}
	}
	var langCode2;
	var size1 = grdArray.length;
	var size2 = newData.length;
	for(i=0;i<size1;i++){//original list
		found = false;
		for(j=0;j<size2;j++){//new data
			langCode = grdArray[i][0];
			langCode2 = newData[j][0];
			if(langCode == langCode2){//the lang code exist in the new data
				found = true;
			}
		}
		if(!found){
			newData[newData.length] = new Array( grdArray[i][0],"null", grdArray[i][2],grdArray[i][3]);
		}
	}
	grdArray = newData;
//	if(validatePCForDisplay() == false){
	//	return false;
//	}
	//$('#btnSave').click();
	var pcSaveForDes=buildPCForDisplayStr(grdArray);
	var submitData = {};
	submitData["pcSaveForDes"]=pcSaveForDes;
	$.ajax({ 
		url : 'managePromotionCriteria!updatePCDesc.action',
		data:submitData,
		type : "POST",
		async:false,
		dataType : "json",
		complete:function(response){
			 var response = eval("(" + response.responseText + ")");
		//	 dataMap.FlightNo = response.selectedFlights;
		//	 fillFlightData();
			 
		}
	});
	
	 resetPromoCodeGrid();
	 searchPromoCodes();
	return POSdisable(false);
}

function setPCForDisplayList(){			
	
	
		setField("txtPCForDisplayOL", "");
		setField("language","-1");
	
	
		var pcList = getFieldByID("translation");				
		pcList.options.length = 0;
		var langCode ;
		var mealName ;
		var langName;
		var mealTitle;
		var i=0;
		
		var pcTranslations = getValue("hdnPromoCodeDesTrans");
		var arrPCTrl = new Array();
		if(pcTranslations != null && pcTranslations != "")
			arrPCTrl = pcTranslations.split("~");
		var pcTrl;
		grdArray = new Array();
		for(var i =0;i<arrPCTrl.length-1;i++){
			grdArray[i] = new Array(); 
			pcTrl = arrPCTrl[i].split(",");
			grdArray[i][0] = pcTrl[0];
			grdArray[i][1] = pcTrl[1].replace("\^",",");
			grdArray[i][2] = pcTrl[2];
			grdArray[i][3] = pcTrl[3];
			
		}
		for(i=0;i<grdArray.length;i++){
			langCode = grdArray[i][0];
			pcName = grdArray[i][1];
			
			if(pcName == "null")
				continue;
			langName = getListLabel("language", langCode);
			addToList("translation", langName+"=>"+pcName, langCode);
		}
	
}
function validatePCForDisplay(){
	
//	/var strMode = getText("hdnMode");
	var size2=getFieldByID("language").length;
	var valid = true;
	var size1;
	var i;
	var j;
	var langCode;
	var pcName;
	var found = false;
	
		size1 = grdArray.length;
		if(size1 != (size2-1))//-1 to ignore the first empty item
			valid = false;
		else{
			for(i=0;i<size1;i++){//original list
				pcName = grdArray[i][1];
				if(pcName == "null"){
					valid = false;
					break;
				}
			}
		}
	
	if(!valid){
		alert("Promo code description for display is missing for all or some languages, original promo code description will copied to all languages") ;
		
				var descr = getValue("pcDescription");
				
				for(i=1;i<size2;i++){//lang  list, start from 1 to ignore the first item (empty)
					found = false;
					langCode = getFieldByID("language").options[i].value;
					for(j=0;j<size1;j++){
						mealName = grdArray[j][1];
						if(mealName == "null"){
							grdArray[j][1] = descr;
						//	grdArray[j][5] = title;
						}
						if(langCode == grdArray[j][0]){
							found = true;
							continue;
						}
					}
					if(!found)
						grdArray[grdArray.length] = new Array(langCode,descr,"-1","-1");
				
				}
	}
	return true;
	}


function buildPCForDisplayStr(grdArray){			
	//var strMode = getText("hdnMode");
	var pcStr = "";
	
	
	
		if(grdArray != null){					
			for(var j=0;j<grdArray.length;j++){
				pcStr += grdArray[j][0]+",";
				pcStr += grdArray[j][1]+",";
				pcStr += grdArray[j][2]+",";
				pcStr += grdArray[j][3]+",";
				
				pcStr += "~";	
		   	}
		}	
			
	return pcStr;
}

function preparePCToSave(){
	var strMode = getText("hdnMode");
	var mealList = getFieldByID("translation");
	var size = pcList.length;
	var langCode;
	var pcName;
	
	var label;
	var str="";
	var size;
	var i=0;
	var j=0;
	var newData = new Array();
	var count = 0;
	//////////
	var mealTranslations = jQuery("#listPC").getCell($("#rowNo").val(),'pcForDisplay');
	var arrPCTrl = new Array();
	if(pcTranslations != null && pcTranslations != "")
		arrPCTrl = pcTranslations.split("~");
	var pcTrl;
	grdArray = new Array();
	for(var i =0;i<arrMealTrl.length-1;i++){
		grdArray[i] = new Array(); 
		mealTrl = arrMealTrl[i].split(",");
		grdArray[i][0] = pcTrl[0];
		grdArray[i][1] = pcTrl[1];
		grdArray[i][2] = pcTrl[2];
		grdArray[i][3] = pcTrl[3];
		grdArray[i][4] = pcTrl[4];				
	}
	////////////
	for(i=0;i<pcList.length;i++){
		langCode = pcList.options[i].value;
		label = pcList.options[i].text.split("=>");
		pcName = label[1];
		
		size = grdArray.length;
		var found = false;
		for(j=0;j<size;j++){
			if(grdArray[j][0] == langCode){
				newData[count++] = new Array(langCode, mealName, grdArray[j][2], grdArray[j][3]);
				found = true;
				break;
			}
		}
		if(!found){
			newData[count++] = new Array(langCode, mealName,  "-1", "-1");//langCode,pcName,id,version
			//arrData[strGridRow][16][size] = new Array(langCode,mealName,'-1','-1');//langCode,mealName,id,ver
		}
	}
	var langCode2;
	var size1 = grdArray.length;
	var size2 = newData.length;
	for(i=0;i<size1;i++){//original list
		found = false;
		for(j=0;j<size2;j++){//new data
			langCode = grdArray[i][0];
			langCode2 = newData[j][0];
			if(langCode == langCode2){//the lang code exist in the new data
				found = true;
			}
		}
		if(!found){
			newData[newData.length] = new Array( grdArray[i][0],"null", grdArray[i][2],grdArray[i][3], "null");
		}
	}
	grdArray = newData;		
	
}

function addPCDesc(){
	
}

generatePromoCodes = function() {
	if ($("#numOfCodes").val()>25000 ){
		showERRMessage("The number of promotions to be generated should be less than 25000");
	} else{
		var data = {};	
		numOfCodes = $("#numOfCodes").val();
		data["numOfCodes"] = numOfCodes;
		$.ajax({ 
			url : 'managePromotionCriteria!generateCodes.action',
			beforeSend : ShowProgress(),
			data : data,
			type : "POST",
			async : false,
			dataType : "json",
			complete:function(response){
				generateSuccess(response);
			}
		});	
	}	
}

generateSuccess = function (response) {
	var responseData=$.parseJSON(response.responseText);
	promoCodes = responseData.sysGenPromoCodes;
	codesGenerated = false;
	codesArray = new Array();
	for(var i=0; i<promoCodes.length; i++){
		var code = promoCodes[i];		
		codesArray.push(code);
	}
	$("#numOfCodesGen").val(promoCodes.length);
	HideProgress();
	$("#beforeGenerate").hide();
	$("#afterGenerate").show();
	$("#btnGenerate").hide();
	$("#btnView").show();
	$("#numOfCodes").attr("disabled", true);
	$("#enblePromoCode").attr("disabled", true);
	
	
}

initMultiPromoCodeDialog = function(){	
	var dynamicHtml = "";
	var heightx = 120 + (promoCodes.length*10);
	if (promoCodes.length>100){
		var heightx = 500;
	}
	if(!codesGenerated){
		for (var i = 0 ; i < promoCodes.length-1 ; i += 2) {
			dynamicHtml += 	"<tr><td width='40%'><input type='text' id='promoCode_"+i+"' style='width:95%' class='uppercase' maxlength='40' value='"+promoCodes[i]+"'disabled='disabled'/></td>"+
						 	"<td valign='top' width='20%'>"+
						 	"<td width='40%'><input type='text' id='promoCode_"+(i+1)+"' style='width:95%' class='uppercase' maxlength='40' value='"+promoCodes[i+1]+"'disabled='disabled'/></td></tr>"
		}
		if (promoCodes.length % 2==1){
			dynamicHtml += 	"<tr><td width='40%'><input type='text' id='promoCode_"+(promoCodes.length-1)+"' style='width:95%' class='uppercase' maxlength='40' value='"+promoCodes[promoCodes.length-1]+"'disabled='disabled'/></td></tr>"
		}
		$("#codeSelection").empty();
		$("#codeSelection").append(dynamicHtml);
		codesGenerated = true;
	}
	$("#multiPromoCodeDialog").dialog({ height: heightx, width: 400, title: "System generated Promo Codes", modal: true });	
}

enablePromoCodes = function(){
	$("#multiPromoCodeDialog").dialog('close');
	$("#afterGenerate").show();
	$("#beforeGenerate").hide();
	codesArray = new Array();
	for(var i=0; i<promoCodes.length; i++){
		var code = $("#promoCode_"+i).val();		
		codesArray.push(code);
	}
	console.log("Code generated: ", codesGenerated);
}

sendPromoCodes = function(){
	if (promoCriteriaId != "" && promoCriteriaId != null && enableSending) {
		var redirectUrl = "showLoadJsp!createCampaign.action?id="+promoCriteriaId;
		top.LoadPage(redirectUrl,"SC_PROM_07","Campaign Management");	
	} else {
		showERRMessage(noCodesToSend);
	}
}

handleSingleOrMultiPromocode = function(){
	if($('#numOfCodes').val()==null || $('#numOfCodes').val()==""){
		$("#btnGenerate").attr("disabled", true);
		$("#enblePromoCode").attr("disabled", false);
		
	} else if(!$('#numOfCodes').val()==null || !$('#numOfCodes').val()==""){
		$("#btnGenerate").attr("disabled", false);
		$("#enblePromoCode").attr("disabled", true);
	}
	
};


