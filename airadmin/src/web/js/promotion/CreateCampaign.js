
    var controller;
    var arrError = new Array();
    var emailIds = new Array();
    var promoCodes = new Array(); 
    var noOfPromoCodes = 0;
    
    $(document).ready(function(){
        controller = new CONTROLLER();
        controller.initCampaignTargetView();
        getListData();
    });

    var MODEL = {
        destinations : ["AAB", "ABD", "ABS", "AHB", "ALA", "ALP", "AMD", "AMM", "AQB", "ATZ", "AUB", "AUH", "AYT", "BAH", "BEY", "BGW", "BLR", "BOM", "BSR", "CCJ", "CGP", "CIT", "CJB", "CMB", "CMN", "COK", "DAC", "DAM", "DBS", "DEL", "DME", "DMM", "DOH", "DOK", "DXB", "EAM", "EBL", "ELQ", "EVN", "FJB", "GOI", "HAS", "HBE", "HMB", "HOF", "HRI", "HRK", "HYD", "IBS", "IKA", "JAI", "JED", "KBL", "KBP", "KHF", "KHI", "KRR", "KRT", "KTM", "KWI", "KZN", "LHE", "LRR", "LTK", "LXR", "MAA", "MCT", "MED", "MHD", "MNL", "MRV", "NAG", "NBO", "NJF", "ODS", "PEW", "PRN", "QBS", "RBS", "ROV", "RUH", "SAH", "SAW", "SHJ", "SJX", "SKT", "SLL", "SON", "SVX", "SYZ", "TBS", "TIF", "TRV", "TSE", "UFA", "URC", "WBS", "YNB"],
        locations : ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"],
        fares : ["Fare 1","Fare 2","Fare 3","Fare 4","Fare 5","Fare 6"],
        months : ["January","February","March","April","May","June","July","August","September","October","November","December"],
        days : ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday" ],
        hours : function(){
            var h = [];
            var s;
            for(var i=0;i<24;i++){
                s = i;
                if(s.length == 1){
                    s = "0"+s;
                }
                h.push(s+":00");
            }
            return h;
        }
    };


    var CONTROLLER = function(){
        var targetView = new SET_CAMPAIGN_TARGET_VIEW();
        var contentView = new SET_CAMPAIGN_CONTENT_VIEW();

        this.initCampaignTargetView = function(){
            targetView.init(MODEL.destinations,MODEL.locations,MODEL.fares);
        };

        this.saveCampaign = function(){
        	saveCampaignEmail();
        };

        this.createCampaignTarget = function(){
            filterCustomers();
        };

        this.cancelCampaign = function(){
            contentView.resetView();
            targetView.showMe();
        };
    };


    var SET_CAMPAIGN_TARGET_VIEW = function(){
        this.init = function(destinations,locations,fares){
            $("#campaignCriteria").decoratePanel("Set Campaign Target");
            $("#campaignContent").decoratePanel("Set Campaign Content");
            $("#bookingDateFrom, #bookingDateTo, #travelDateFrom, #travelDateTo").datepicker({
                showOn: "button",
                buttonImage: "../../images/calendar_no_cache.gif",
                buttonImageOnly: true,
                dateFormat: 'dd/mm/yy'
            });

            $("body").off("click", "#btnSetTargets").on("click", "#btnSetTargets", onCreateCampaignClick);
            $("body").off("click", "#btnAddOND").on("click", "#btnAddOND", onAddOndClick);
            $("body").off("click", "#btnRemoveOND").on("click", "#btnRemoveOND", onRemoveOndClick);
            $("body").off("click", "#btnAddLocation").on("click", "#btnAddLocation", onAddLocationClick);
            $("body").off("click", "#btnRemoveLocation").on("click", "#btnRemoveLocation", onRemoveLocationClick);
            $("body").off("click", "#btnAddFare").on("click", "#btnAddFare", onAddFareClick);
            $("body").off("click", "#btnRemoveFare").on("click", "#btnRemoveFare", onRemoveFareClick);
            $("body").off("blur", "#bookingDateFrom,#bookingDateTo,#travelDateFrom,#travelDateTo").on("blur", "#bookingDateFrom,#bookingDateTo,#travelDateFrom,#travelDateTo", onDateChange);

//            SERVICE.populateSelect("#selOrigin",destinations);
//            SERVICE.populateSelect("#selDestination",destinations);
//            SERVICE.populateSelect("#selVia1",destinations);
//            SERVICE.populateSelect("#selVia2",destinations);
//            SERVICE.populateSelect("#selVia3",destinations);
//            SERVICE.populateSelect("#selVia4",destinations);
//            SERVICE.populateSelect("#selLocation",locations);
//            SERVICE.populateSelect("#selFare",fares);
        };

        this.showMe = function(){
            $("#campaignCriteria").show();
        };

        this.resetView = function(){
            SERVICE.removeAllItemsFromMultiSelect("#selectedFares,#selectedLocations,#selONDs");
            SERVICE.clearFieldValue("#txtOfferName,#bookingDateFrom,#bookingDateTo,#txtCampaignDescription,#travelDateFrom,#travelDateTo,#ageFrom,#ageTo,#selOrigin,#selDestination,#selVia1,#selVia2,#selVia3,#selVia4,#selLocation,#selFare");

        };

        var onDateChange = function(event){
            SERVICE.fromDateValidation(event.target.id);
        };

        var onRemoveFareClick = function(){
            SERVICE.removeSelectedItemsFromMultiSelect("#selectedFares");
        };

        var onAddFareClick = function(){
            var fare = $("#selFare").val();
            var txtFare = $("#selFare").text();
            SERVICE.addItemToMultiSelect("#selectedFares",fare);
        };

        var onAddLocationClick = function(){
            var location = $("#selLocation").val();
            SERVICE.addItemToMultiSelect("#selectedLocations",location);
        };

        var onRemoveLocationClick = function(){
            SERVICE.removeSelectedItemsFromMultiSelect("#selectedLocations");
        };

        var onRemoveOndClick = function(){
            SERVICE.removeSelectedItemsFromMultiSelect("#selONDs");
        };

        var onAddOndClick = function(){
            var origin = $("#selOrigin").val();
            var destination = $("#selDestination").val();
            var via1 = $("#selVia1").val();
            var via2 = $("#selVia2").val();
            var via3 = $("#selVia3").val();
            var via4 = $("#selVia4").val();

            var viaPoints=[];
            viaPoints.push(via1);
            viaPoints.push(via2);
            viaPoints.push(via3);
            viaPoints.push(via4);

            if(origin == null || destination == null){
                alert(ondSelect);
                return false;
            }else if(origin != "ALL" && destination != null && origin == destination){
                alert(ondSame);
                return false;
            }

            var wasPrevViaPointEmpty = false;

            for(var i=0; i<viaPoints.length; i++) {
                var value = viaPoints[i];
                if(wasPrevViaPointEmpty && value != null && value != ""){
                    alert(viaPointsOrder);
                    return false;
                }
                if((value==null || value == "") && !wasPrevViaPointEmpty){
                    wasPrevViaPointEmpty = true;
                }
            }

            var sortedViaPoints = viaPoints.sort();
            var prevValue = null;
            for(var i=0; i<sortedViaPoints.length; i++) {
                var value = sortedViaPoints[i];
                if(prevValue == value && value != "ALL" && value != null && value != ""){
                    alert(viapointsDuplicate);
                    return false;
                }
                prevValue = value;
            }

            var concatVal = origin + '/'
            if(via1!=null && via1 != ""){ concatVal += via1 + '/'; }
            if(via2!=null && via2 != ""){ concatVal += via2 + '/'; }
            if(via3!=null && via3 != ""){ concatVal += via3 + '/'; }
            if(via4!=null && via4 != ""){ concatVal += via4 + '/'; }
            concatVal += destination;

            SERVICE.addItemToMultiSelect("#selONDs",concatVal);

        };

        var onCreateCampaignClick = function(){
            if(validate()){
                controller.createCampaignTarget();
            }
        };

        var isEmptyField = function(id){
            if($.trim($(id).val()).length == 0){
                return true;
            }
            return false;
        };

        var validate = function(){
            if(isEmptyField("#txtOfferName")){
                showERRMessage(campaignNameEmpty);
                return false;
            }
            if(isEmptyField("#bookingDateFrom")){
                showERRMessage(bookingDateFromEmpty);
                return false;
            }
            if(isEmptyField("#bookingDateTo")){
                showERRMessage(bookingDateToEmpty);
                return false;
            }
            if(isEmptyField("#travelDateFrom")){
                showERRMessage(travelDateFromEmpty);
                return false;
            }
            if(isEmptyField("#travelDateTo")){
                showERRMessage(travelDateToEmpty);
                return false;
            }
            if($("#selONDs option").length == 0){
                showERRMessage(routesEmpty);
                return false;
            }

            return true;

        };

    };


    var SET_CAMPAIGN_CONTENT_VIEW = function(){
        var isInitialized = false;
        this.init = function(months,days,hours){
            $("#campaignContent").show();
            $("body").off("click", "#btnSubmitCampaign").on("click", "#btnSubmitCampaign", onCampaignSave);
            $("body").off("click", "#btnCancelSubmit").on("click", "#btnCancelSubmit", onCancelSubmit);
            $("body").off("click", "#btnAddMonth").on("click", "#btnAddMonth", onAddMonthClick);
            $("body").off("click", "#btnRemoveMonth").on("click", "#btnRemoveMonth", onRemoveMonthClick);
            $("body").off("click", "#btnAddDay").on("click", "#btnAddDay", onAddDay);
            $("body").off("click", "#btnRemoveDay").on("click", "#btnRemoveDay", onRemoveDay);
            $("body").off("click", "#btnAddHour").on("click", "#btnAddHour", onAddHour);
            $("body").off("click", "#btnRemoveHour").on("click", "#btnRemoveHour", onRemoveHour);
            $("body").off("click", "#btnScheduleCampaign").on("click", "#btnScheduleCampaign", onCampaignSchedule);

            $("#campaignEditor").html("");

            if(!isInitialized){
                isInitialized = true;
                initializeCampaignEditor();
                SERVICE.populateSelect("#selMonth",months);
                SERVICE.populateSelect("#selDay",days);
                SERVICE.populateSelect("#selHour",hours);
            }
        };

        this.resetView = function(){
//            SERVICE.removeAllItemsFromMultiSelect("#selectedMonths,#selectedDays,#selectedHours");
            tinyMCE.activeEditor.setContent('');
//            $("#selMonth,#selDay,#selHour").val("");
        };

        var onAddMonthClick = function(){
            var m = $("#selMonth").val();
            SERVICE.addItemToMultiSelect("#selectedMonths",m);
        };

        var onRemoveMonthClick = function(){
            SERVICE.removeSelectedItemsFromMultiSelect("#selectedMonths");
        };

        var onAddDay = function(){
            var d = $("#selDay").val();
            SERVICE.addItemToMultiSelect("#selectedDays",d);
        };

        var onRemoveDay = function(){
            SERVICE.removeSelectedItemsFromMultiSelect("#selectedDays");
        };

        var onAddHour = function(){
            var h = $("#selHour").val();
            SERVICE.addItemToMultiSelect("#selectedHours",h);
        };

        var onRemoveHour = function(){
            SERVICE.removeSelectedItemsFromMultiSelect("#selectedHours");
        };

        var onCampaignSave = function(){
            $("#campaignContent").hide();
            controller.saveCampaign();
        };
        
        var onCampaignSchedule = function(){
           scheduleCampaign();
        };

        var onCancelSubmit = function(){
            $("#campaignContent").hide();
            controller.cancelCampaign();
        };

        var initializeCampaignEditor = function(){
            //Initialize html editor
            tinyMCE.init({
                // General options
                width : "100%",
                mode : "textareas",
                editor_selector : "campaignEditor",
                theme : "advanced",
                plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

                // Theme options
                theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,advhr,|,print,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4 : "spellchecker,|nonbreaking,blockquote,pagebreak,|insertimage",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,

                // Skin options
                skin : "o2k7",
                skin_variant : "silver",
                oninit : function() {
                    $("#campaignEditor_tbl").css("width","100%");
                    $("#campaignEditor_ifr").css("width","100%");
                    $("#campaignEditor_ifr").css("height","240px");
                },
                save_onsavecallback: function() {
      //              onCampaignSave();
                }
            });
        };
    };


    var SERVICE = {
        populateSelect : function(id,dataCol){
            var select = $(id);
            $.each(dataCol, function(i,data) {
                select.append($("<option />").val(data).text(data));
            });
        },
        addItemToMultiSelect : function(id,value){
            if($(id+" option[value='"+value+"']").length > 0){
            	alert(valueExist);
                return false;
            }
            $(id).append($("<option/>", {
                value: value,
                text: value
            }));
        },
        removeSelectedItemsFromMultiSelect : function(id){
            $('option:selected', $(id)).remove();
        },
        removeAllItemsFromMultiSelect : function(id){
            $('option', $(id)).remove();
        },
        fromDateValidation : function(field) {
            if (!dateChk(field)) {
                showCommonError("Error", invalidDate);
                if (top[1].objTMenu.focusTab == screenId)
                    getFieldByID(field).focus();
                return;
            }
        },
        clearFieldValue : function(id){
            $(id).val("");
        }

    };
    
    getListData = function (){
    	var data = {};	
    	if (promoCriteriaId!=null && promoCriteriaId!=""){
    		data['promoCriteriaId'] = promoCriteriaId;
    	}
    	$.ajax({ 
    		url : 'loadCampaignManagement.action',
    		data : data,
    		beforeSend : ShowProgress(),
    		type : "POST",
    		async : false,
    		dataType : "json",
    		complete: createLists
    	});	
    }
    
    createLists = function(response) {
    	HideProgress();
    	var response = eval("(" + response.responseText + ")");
		$("#selLocation").append(response.locationsList);
		promoCodes = new Array();
		if(response.promoCodeList != null){
			promoCodes = response.promoCodeList;
			noOfPromoCodes = promoCodes.length;
			$("#noOfPromoCodes").val(noOfPromoCodes);
		}
		$("#selOrigin, #selDestination,#selVia1,#selVia2,#selVia3,#selVia4")
				.fillDropDown({
					firstEmpty : false,
					dataArray : response.airportsList,
					keyIndex : 0,
					valueIndex : 0
				});		
	} 
    
    filterCustomers = function(){
    	if(noOfPromoCodes!=0){
	    	var reqData={};
	    	reqData['oNDs'] = getSelectBoxData("selONDs");
			reqData['locations'] = getSelectBoxData("selectedLocations");
			reqData['fareTypes'] = getSelectBoxData("selectedFares");	
			reqData['customerPromoSegRequset.ageTo'] = ($('#ageTo').val()=="")?null:$('#ageTo').val();
			reqData['customerPromoSegRequset.ageFrom'] = ($('#ageFrom').val()=="")?null:$('#ageFrom').val();
			reqData['customerPromoSegRequset.selFare'] = ($('#selFare').val()=="")?null:$('#selFare').val();		
			reqData['reservationFromDate'] = $('#bookingDateFrom').val();
			reqData['reservationToDate'] = $('#bookingDateTo').val();
			reqData['flightFromDate'] = $('#travelDateFrom').val();
			reqData['flightToDate'] = $('#travelDateTo').val();
			
			$.ajax({
				url : 'manageCampaign!search.action',
				data : reqData,
				dataType : "json",
				beforeSend : ShowProgress(),
				type : "POST",
				async : false,
				success : customerSearchSuccess
			});
    	} else {
    		showERRMessage(noPromoCodesToSend);
    	}
    }
    
    getSelectBoxData = function(selectBoxID){
		var selector="#"+selectBoxID+" option"
		var selectedItems=[];
		$(selector).each(function () {
			if(this.value!=""){
				selectedItems.push(this.value);
			}
		});
		return JSON.stringify(selectedItems);
	}
    
    customerSearchSuccess =  function(response) {
    	HideProgress();
    	if(response.success){
	    	
	    	emailIds = new Array();
	    	if(response.emailIDs.length>0){
		    	for(var i=0; i<response.emailIDs.length; i++){
		    		var email = response.emailIDs[i];		
		    		emailIds.push(email);
		    	}
		    	$("#campaignCriteria").hide();
		    	var contentView = new SET_CAMPAIGN_CONTENT_VIEW();
		        contentView.init(MODEL.months,MODEL.days,MODEL.hours());
	    	}
	    	showCommonError('success', emailIds.length +" "+ filterSuccess);  
    	} else {
    		showERRMessage(filterError);
    	}
    }
    
    saveCampaignEmail =  function(){
    	
	    var reqData={};
	//  reqData['months'] = getSelectBoxData("selectedMonths");
	//	reqData['days'] = getSelectBoxData("selectedDays");
	//	reqData['hours'] = getSelectBoxData("selectedHours");	
		reqData['campaignTO.campaignName'] = $('#txtOfferName').val();
		reqData['campaignTO.campaignDes'] = $('#txtCampaignDescription').val();
		reqData['campaignTO.emailContent'] = tinyMCE.activeEditor.getContent();
		reqData['campaignTO.noOfPromoCodes'] = noOfPromoCodes;
		reqData['campaignTO.noOfCustomers'] = emailIds.length;
		for (var i = 0; i < emailIds.length; i++) {
			reqData['campaignTO.customers['+i+']'] = emailIds[i];
		}
		for (var i = 0; i < promoCodes.length ; i++){
			reqData['campaignTO.promoCodes['+i+']'] = promoCodes[i];
		}
		
		reqData['interval'] = $('#selScheduleFor').val();
//		reqData['scheduledTime'] = $('#selScheduledTime').val();
		reqData['startDate'] = $('#startDateCal').val();
		reqData['endDate'] = $('#endDateCal').val();
		$.ajax({
			url : 'manageCampaign!save.action',
			data : reqData,
			dataType : "json",
			beforeSend : ShowProgress(),
			type : "POST",
			async : false,
			success : saveCampaignSuccess
		});
    	
    }
    
    saveCampaignSuccess = function(response){
    	HideProgress();
    	if(response.success){
    		var targetView = new SET_CAMPAIGN_TARGET_VIEW();
    	    var contentView = new SET_CAMPAIGN_CONTENT_VIEW();
	    	showCommonError('success', emailSent);  
            targetView.resetView();
            contentView.resetView();
            targetView.showMe();
    	} else {
    		showERRMessage(emailSentFailed);
    	}
    }
    
    scheduleCampaign = function() {
    	UI_SchedCamp.displayForm({divName:'divSchedFrom', formName:'frmPage', 
    		composerName: 'companyPayment',
    		errorCallback:function(message){
    			showERRMessage(message);
    		},
    		successCallback:function(message){
    			showCommonError("Confirmation", message);
    		}
    	});    	
    }






