function UI_SchedCamp(){}
UI_SchedCamp.errorCallback = null;
UI_SchedCamp.successCallback = null;
UI_SchedCamp.formName = null;
UI_SchedCamp.composerName = null;
UI_SchedCamp.isCreated = false;

UI_SchedCamp.displayForm = function(params){
	$('#spnSchedule').remove();
	if(params != null || params.formName == null || params.composerName == null){
		UI_SchedCamp.distroy();
		var strAppend = "<span id='spnSchedule' style='position:absolute;top:300px;left:200px;width:480px;height:120px;'>";
		strAppend +=	"<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td valign='top'>";
		strAppend +=	"<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' ID='Table4'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td width='10' height='20'><img src='../../images/form_top_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td colspan='2' height='20' class='FormHeadBackGround'><img src='../../images/bullet_small_no_cache.gif'> <font class='FormHeader'>";
		strAppend +=	"Schedule Campaign<\/font><\/td>";
		strAppend +=	"<td width='11' height='20'><img src='../../images/form_top_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td class='FormBackGround'><\/td>";
		strAppend +=	"<td class='FormBackGround' valign='top' style='height:100px;' colspan='2'>";
		strAppend +=	UI_SchedCamp.constructContent(params.formName);
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround'></td>";
		strAppend +=	"</tr>";
		strAppend +=	"<tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td class='FormBackGround'><\/td>";
		strAppend +=	"<td class='FormBackGround' align='left'>";
		strAppend +=	"<input name='btnCancel' type='button' class='button' id='btnCancel' value='Cancel' onClick='UI_SchedCamp.distroy();'>";
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround' align='right'>";
		strAppend +=	"&nbsp;<input name='btnDone' type='button' class='button' id='btnDone' value='Done' onClick='UI_SchedCamp.scheduleCampaign();'>";
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround'></td>";
		strAppend +=	"</tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td height='12'><img src='../../images/form_bottom_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td height='12' colspan='2' class='FormBackGround'><img src='../../images/spacer_no_cache.gif' width='100%' height='1'><\/td>";
		strAppend +=	"<td height='12'><img src='../../images/form_bottom_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<\/table>";
		strAppend +=	"<\/td>";
		strAppend +=	"<\/table>";
		strAppend +=	"<\/span>";
		$("#"+params.divName).append(strAppend);
		UI_SchedCamp.errorCallback = params.errorCallback;
		UI_SchedCamp.successCallback = params.successCallback;
		UI_SchedCamp.formName = params.formName;
		UI_SchedCamp.composerName = params.composerName;

		$("#startDateCal").datepicker({ minDate: new Date(), dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		$("#endDateCal").datepicker({ minDate: new Date(), dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		UI_SchedCamp.setDateAutoFormatter();
	} else {
		alert("incorrect parameters");
	}
}
UI_SchedCamp.setDateAutoFormatter = function (){
	$('#startDateCal').change(function (){
		var strTime = this.value;
		dateChk('startDateCal');	
	});	
	$('#endDateCal').change(function (){
		var strTime = this.value;
		dateChk('endDateCal');	
	});
}


UI_SchedCamp.constructContent = function(formName){
	
	var strAppend = "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";	
	strAppend +=	"	<tr>";
	strAppend +=	"		<td colspan='2' style='height:5px;'><\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td width='100px'>";
	strAppend +=	"			<font>Schedule for :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<select id='selScheduleFor' name='selScheduleFor' style='width:250px;'>";
	strAppend +=	"				<option value='HOURLY'>Hourly<\/option>";
	strAppend +=	"				<option value='TWICE_A_DAY'>Twice a Day<\/option>";
	strAppend +=	"				<option value='THRICE_A_DAY'>Thrice a Day<\/option>";
	strAppend +=	"				<option value='DAILY'>Daily<\/option>";
	strAppend +=	"				<option value='WEEKlY'>Weekly<\/option>";
	strAppend +=	"			<\/select> <font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<\/br>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td colspan='2'>";
	strAppend += 	"		<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
	strAppend +=	"		<td width='100px'>";
	strAppend +=	"			<font>Start Date :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td width='100px'>";
	strAppend +=	"			<input type='text' id='startDateCal' name='startDateCal' style='width:78px;'><\/input><font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td width='68px'>";
	strAppend +=	"			<font>End Date :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<input type='text' id='endDateCal' name='endDateCal' style='width:78px;'><\/input> <font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<\/table>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<\/table>";	
	
	return strAppend;
}

UI_SchedCamp.distroy = function(){
	$('#spnSchedule').hide();
}

UI_SchedCamp.scheduleCampaign = function() {
	var valiMessage = UI_SchedCamp.validateParams();
	if(valiMessage == null) {
		$('#btnDone').attr('disable',true);
		UI_SchedCamp.scheduleCampaignSuccess();
	} else {
		UI_SchedCamp.displayError(valiMessage);
	}
}

UI_SchedCamp.scheduleCampaignSuccess = function(response){
	UI_SchedCamp.displaySuccess("Press Submit button to activate campaign and send emails..");
	UI_SchedCamp.distroy();
	$('#btnDone').attr('disable',false);
}

UI_SchedCamp.displaySuccess = function(message){
	if(UI_SchedCamp.successCallback != null){
		UI_SchedCamp.successCallback.call(this, message);
	} else {
		alert(message);
	}	
}

UI_SchedCamp.displayError = function(message){
	top[2].HideProgress();
	if(UI_SchedCamp.errorCallback != null){
		UI_SchedCamp.errorCallback.call(this, message);
	} else {
		alert(message);
	}	
	$('#btnDone').attr('disable',false);
}

UI_SchedCamp.validateParams = function() {

	if($('#startDateCal').val() == null || $('#startDateCal').val() == "") {
		return "Please enter start date.";
	} else {
		var dateFilter = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		if(!dateFilter.test($('#startDateCal').val())) {
			return "Please enter valid start date.";
		}
	}

	if($('#endDateCal').val() == null || $('#endDateCal').val() == "") {
		return "Please enter end date.";
	} else {
		var dateFilter = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		if(!dateFilter.test($('#endDateCal').val())) {
			return "Please enter valid end date.";
		}
	}

	var startDate = $('#startDateCal').val();
	var endDate = $('#endDateCal').val();
	var intStartDate = parseInt(startDate.substring(6) + startDate.substring(3,5) + startDate.substring(0,2));
	var intEndDate = parseInt(endDate.substring(6) + endDate.substring(3,5) + endDate.substring(0,2));
	if(intStartDate > intEndDate){
		return "Start date cannot be grater than end date.";
	}
	return null;
}

