//$Id$

var screenId = "SC_ADMN_022";
var valueSeperator = "~";
var blnEdited = false;

function onLoad() {
	isReseted();	
	if (window.location.href.indexOf('https://') == -1
			&& request['useSecureLogin']) {
			window.location.replace(request['secureUrl']+"?reset="+isReset);
		
	} else {
		setField("txtUserId", loggedInUserId);
		getFieldByID("pwdOld").focus();
		if (reqMessage != null && reqMessage != "") {
			showPageERRMessage(reqMessage);
		}
	}
}

function save() {
	if (validate()) {
		getFieldByID("formChangePassword").target = "_self";
		getFieldByID("formChangePassword").submit();
	}
}

function validate() {
	var blnStatus = true;
	var val = ""
	val = trim(getValue("pwdOld"));
	setField("pwdOld", val);

	if (val == "") {
		showPageERRMessage(userOldPasswordRqrd);
		getFieldByID("pwdOld").focus();
		return false;
	}

	val = trim(getValue("pwdNew"));
	setField("pwdNew", val);

	if (val == "") {
		showPageERRMessage(userNewPasswordRqrd);
		getFieldByID("pwdNew").focus();
		return false;
	}

	if (val.length < 8) {
		showPageERRMessage(userNewPasswordlength);
		getFieldByID("pwdNew").focus();
		return false;
	}

	var strChar = "";
	var numCount = 0;
	for ( var i = 0; i < val.length; i++) {
		strChar = val.substr(i, 1);
		if (isPositiveInt(strChar)) {
			numCount++;
		}
	}
	if ((numCount < 3) || (numCount > (val.length - 3))) {
		showPageERRMessage(cmbinvalid);
		getFieldByID("pwdNew").focus();
		return false;
	}

	val = getValue("pwdConfirm");
	setField("pwdConfirm", val);

	if (val == "") {
		showPageERRMessage(userConfPasswordRqrd);
		getFieldByID("pwdConfirm").focus();
		return false;
	}

	if (val.length < 8) {
		showPageERRMessage(userConfPasswordlength);
		setField("pwdConfirm", val);
		getFieldByID("pwdConfirm").focus();
		return false;
	}

	if (getValue("pwdOld") == getValue("pwdNew")) {
		showPageERRMessage(userSamePassword);
		getFieldByID("pwdNew").focus();
		return false;
	}

	if (getValue("pwdNew") != getValue("pwdConfirm")) {
		showPageERRMessage(userPasswordNotConf);
		getFieldByID("pwdNew").focus();
		return false;
	}

	return true;
}

function pageOnChange() {
	blnEdited = true;
	resetPageERRMessage();
}

function disableButtons(bln) {
	Disable('btnSave', bln);
	Disable('btnReset', bln);
}

function resetMe() {
	setField("txtUserId", loggedInUserId);
	setField("pwdOld", "");
	setField("pwdNew", "");
	setField("pwdConfirm", "");
	getFieldByID("pwdOld").focus();
	blnEdited = false
}

function showPageConfMessage(strMsg) {
	DivWrite("spnError",
			"<font class='fntBold'><img src='../../images/Conf_no_cache.gif'> " + strMsg
					+ '<\/font>');
}

function showPageERRMessage(strMsg) {
	DivWrite("spnError",
			"<font class='mandatory fntBold'><img src='../../images/Err_no_cache.gif'> "
					+ strMsg + '<\/font>');
}

function resetPageERRMessage() {
	DivWrite("spnError", "");
}

function closeClick() {
	if (blnEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			blnEdited = false;
			document.forms[0].hdnMode.value = "CLOSE";
			getFieldByID("formChangePassword").target = "_self";
			getFieldByID("formChangePassword").submit();
		}
	} else {
		blnEdited = false;
		document.forms[0].hdnMode.value = "CLOSE";
		getFieldByID("formChangePassword").target = "_self";
		getFieldByID("formChangePassword").submit();
	}
}

$(function() {
	$('.password').pstrength();
});

function isReseted(){
	setVisible("pwdReset",false);
	if(isReset == "true"){
		setVisible("pwdReset",true);
	}			
}	

// -->

