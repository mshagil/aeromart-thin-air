
$(document).ready(function(){
	UI_mngBCAllo.ready();
});

function UI_mngBCAllo(){
}
UI_mngBCAllo.GridIndex;
UI_mngBCAllo.BCGridID;
UI_mngBCAllo.segment = "";
UI_mngBCAllo.bcDetailsList=null;
UI_mngBCAllo.Add = "A";
UI_mngBCAllo.Edit = "E";
UI_mngBCAllo.Delete = "D";
UI_mngBCAllo.Nonmodify = "N";
UI_mngBCAllo.DeleteList = [];
UI_mngBCAllo.temporaryID = 1;
UI_mngBCAllo.oldCabinClass = -1;
UI_mngBCAllo.currentInvTempCCID ;
UI_mngBCAllo.currentInvTempID ;
UI_mngBCAllo.isEdited =false;
UI_mngBCAllo.allModified =false;
UI_mngBCAllo.maxAdultTotal;
UI_mngBCAllo.maxInfantTotal;
UI_mngBCAllo.totalBCAllocs = 0;

UI_mngBCAllo.ready = function (){
	$("#divSearchAC").decoratePanel("Search AirCraft Model");
	$("#divCabinClsSel").decoratePanel("Cabin Class Selection");
	$("#divBCAlloc").decoratePanel("Booking Code Allocations");
	$("#btnSegment").decorateButton();
	$('#btnSplit').decorateButton();
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnDelete').decorateButton();
	document.getElementById("selAircraftModel").disabled=true;
	document.getElementById("txtSegment").disabled=true;
	$('#chkPrio').click(function() {UI_mngBCAllo.allPriority_Click();});
	$('#chkClosed').click(function() {UI_mngBCAllo.allClosed_Click();});
	$("#btnSplit").click(function(){UI_mngBCAllo.splitSegmentAlloc();});
	UI_mngBCAllo.currentInvTempID = $("#hdnTmplateID").val();	
	// In edit template mode ,Route change not allowed
	UI_mngBCAllo.restrictRoutChange();
	// Initialize set data to drop down lists
	UI_mngBCAllo.getLoadData();
	$("#btnSegment").unbind().click(function() {UI_mngBCAllo.setSegment();});
	$("#btnSave").unbind().click(function() {UI_mngBCAllo.saveInvTemp();});
	$("#btnBack").click(function(){UI_mngBCAllo.BackClicked();});
	// Booking class allocation is disabled until select the  CC Alloc row
	$("#divBCAlloc").hide();
	$("#divBCAllocOpts").hide();
	// Add new BC to grid when click add button
	$("#btnAdd").unbind().click(function() {UI_mngBCAllo.addBookingClassInvnentory();});
	$("#btnDelete").click(function() {UI_mngBCAllo.deleteBCAlloc();});
	$("#btnEdit").click(function() {UI_mngBCAllo.editClick();});
}
UI_mngBCAllo.restrictRoutChange = function(){
	if(UI_mngBCAllo.currentInvTempID != 0 && UI_mngBCAllo.currentInvTempID != null){
		document.getElementById("selOrigin").disabled = true;
		document.getElementById("selVia1").disabled = true;
		document.getElementById("selVia2").disabled = true;
		document.getElementById("selVia3").disabled = true;
		document.getElementById("selDestination").disabled = true;
		document.getElementById("btnSegment").disabled = true;	
		//document.getElementById("chkStatus").disabled = true;
	}
}
UI_mngBCAllo.DisableEdit = function (){	
	document.getElementById("chkClosed").disabled = true;
	document.getElementById("chkPrio").disabled = true;
	$( "input[type='checkbox']" ).prop({
		  disabled: true
	});
	$( "input[type='text']" ).prop({
		  disabled: true
	});
	document.getElementById("txtAlloc").disabled = false;
	document.getElementById("chkStatus").disabled = false;
}
UI_mngBCAllo.EnableEdit = function (){	
	document.getElementById("chkClosed").disabled = false;
	document.getElementById("chkPrio").disabled = false;
	$( "input[type='checkbox']" ).prop({
		  disabled: false
	});
	$( "input[type='text']" ).prop({
		  disabled: false
	});
	document.getElementById("txtAlloc").disabled = false;
}
UI_mngBCAllo.setSegment = function (){
	var origin = $("#selOrigin").val();
    var destination = $("#selDestination").val();
    var via1 = $("#selVia1").val();
    var via2 = $("#selVia2").val();
    var via3 = $("#selVia3").val();
    if(0!==origin.length && origin!=null){
    	UI_mngBCAllo.segment = origin+"/";
    }else{
    	showERRMessage("Select origin");
    	return;
    }
    if(0!==via1.length && via1!=null){
    	UI_mngBCAllo.segment+=via1+"/"; 	
    }
    if(0!==via2.length && via2!=null){
    	UI_mngBCAllo.segment+=via2+"/"; 	
    }
    if(0!==via3.length && via3!=null){
    	UI_mngBCAllo.segment+=via3+"/"; 	
    }
    if(0 !== destination.length && destination!=null){
    	UI_mngBCAllo.segment+=destination;
    }else{
    	showERRMessage("Select destination");
    	return;
    }
    if(destination === origin){
    	showERRMessage("Origin and destination can not be equal ");
    	return;
    }
    document.getElementById("txtSegment").value = UI_mngBCAllo.segment;
}
UI_mngBCAllo.getLoadData = function () {
	$.ajax({
        url : 'mngInvTemps!loadInitData.action',
        dataType:"json",
        type : "POST",		           
		success : UI_mngBCAllo.loadDataSuccess
     });
}
UI_mngBCAllo.loadDataSuccess = function (response) {
	UI_mngBCAllo.airCraftList = response.airCraftList;
	UI_mngBCAllo.airPortCodes = response.airportCodes;
	try { 
		eval(response.errorList);
		eval(response.airCraftList);
		eval(response.airportCodes);
	} catch (err){
		console.log("eval() error ==> loadDataSuccess");
	}
	$('#selAircraftModel').html("<option>Options</option>"+UI_mngBCAllo.airCraftList);
	var airCraftModel = $("#hdnAirCraftModel").val();
	$("#selAircraftModel").val(airCraftModel);
	if($("#hdnStatus").val()==='ACT'){
		$("#chkStatus").prop('checked', true);	
	}
	else if($("#hdnStatus").val()==='INA'){
		$("#chkStatus").prop('checked', false);	
	}
	var currentSegment =  $("#hdnSegment").val();
	if(0!==currentSegment.length && currentSegment!==null){
		UI_mngBCAllo.segment = currentSegment;
		document.getElementById("txtSegment").value = UI_mngBCAllo.segment;	
	}
	$('#selOrigin').html("<option value=''>Options</option>"+UI_mngBCAllo.airPortCodes);
	$('#selDestination').html("<option value= ''>Options</option>"+UI_mngBCAllo.airPortCodes);
	$('#selVia1').html("<option value = ''>Options</option>"+UI_mngBCAllo.airPortCodes);
	$('#selVia2').html("<option value = ''>Options</option>"+UI_mngBCAllo.airPortCodes);
	$('#selVia3').html("<option value = ''>Options</option>"+UI_mngBCAllo.airPortCodes);
	// Initialize Cabin class selection grid
	UI_mngBCAllo.createCCSelGrid();	
}
UI_mngBCAllo.createCCSelGrid =function(){
	$("#CCSelDetailsGridContainer").empty();
	$("#CCSelDetailsGridContainer").append('<table id="listCcSelId"></table><div id="CcSelPages"></div>');
	var data = {};
	data['airCraftModel'] = $("#selAircraftModel").val();
	if(UI_mngBCAllo.currentInvTempID !=null && UI_mngBCAllo.currentInvTempID > 0 ){
		data['invTempID'] = UI_mngBCAllo.currentInvTempID;
	}
	var invTempCCAllocGrid = $("#listCcSelId").jqGrid({
				datatype : function(postdata) {
					$.ajax({
						url : "searchInvTemplCCAlloc.action",
						data : $.extend(postdata, data),
						dataType : "json",
						type : "POST",
						complete : function(jsonData, stat) {
							if (stat == "success") {
								mydata = eval("("+ jsonData.responseText + ")");
								if (mydata.msgType != "Error") {
									$.data(invTempCCAllocGrid[0], "gridData", mydata.invTemplateCCAllocList);
									invTempCCAllocGrid[0].addJSONData(mydata);
								} else {
									alert("Error in retriving data");
								}
							}
						}
					});
				},		
				height : 80,
				colNames : ['Cabin Class Code', 'Logical Cabin Class Code', 'Adult Seat Count','Infant Seat Count','InvTempCabinAllocID'],
				colModel : [ {name : 'cabinClassCode',index : 'cabinClassCode',width : 80,jsonmap : 'invTempCCAllocDTO.cabinClassCode'},
				             {name : 'logicalCabinClass',index : 'logicalCabinClass',width : 120,jsonmap : 'invTempCCAllocDTO.logicalCabinClassCode'},
				             {name : 'adultSeatCount',index : 'adultSeatCount',width : 120,jsonmap : 'invTempCCAllocDTO.adultAllocation'},
				             {name : 'infantSeatCount',index : 'infantSeatCount',width : 120,jsonmap : 'invTempCCAllocDTO.infantAllocation'},
				             {name : 'invTempCabinAllocID',index : 'invTempCabinAllocID', jsonmap : 'invTempCCAllocDTO.invTempCabinAllocID', hidden : true}],
			    caption : "Cabin Class Selection",
				rowNum : 20,
				viewRecords : true,
				width : 920,
				pager : jQuery('#CcSelPages'),
				jsonReader : {
     			root : "invTemplateCCAllocList",
				    	page : "page",
				   		total : "total",
				 		records : "records",
				    	repeatitems : false,
				     	id : "0"
				  	},
				  	beforeSelectRow: function (rowid) {
				  		if ($(this).getGridParam("selrow") === rowid) {
				  			$(this).jqGrid("resetSelection");
				  	    } else {
				  	        return true;
				  	    }
				  	}
	}).navGrid("#CcSelPages", {refresh : true,edit : false,add : false,del : false,search : false});
	UI_mngBCAllo.changeBCGrid();
	$("#listCcSelId").click(function() {
		UI_mngBCAllo.ccSelGridOnClick();
	});
}
UI_mngBCAllo.splitSegmentAlloc = function() {
	if($("#txtSegment").val().length === 0 && $("#hdnShowRW").val() === "Y"){
		showERRMessage("Segment Code can not be empty");
		return;
	}
	var selRow = $("#listCcSelId").getGridParam('selrow');
	var ccRowData = $("#listCcSelId").getRowData(selRow);
	if (selRow != null && selRow.length != 0){
		var cbutton = $("<input/>").attr({"type":"button", "value":"Cancel", "id":"btnCancel"});
		var sbutton = $("<input/>").attr({"type":"button", "value":"Save", "id":"btnSplit"});
		sbutton.click(function(){
			UI_mngBCAllo.splitSaveAlloc();
		});
		cbutton.click(function(){
			popClose();
		});
		var  spn = $("<span></span>");
		spn.append(sbutton, cbutton);
		$("#popUp").openMyPopUp({
			"width":625,
			"height":300,
			"appendobj":false,
			"headerHTML":$("<lable>Split Segments</label>"),
			"bodyHTML":UI_mngBCAllo.loadSplitAllocation(),
			"footerHTML":spn
		});
		loadDataToSplitGrid = function(){
			var reqData = {};
			var selRow = $("#listCcSelId").getGridParam('selrow');
			var rowData = $("#listCcSelId").getRowData(selRow);
			reqData.cabinClassCode = rowData.cabinClassCode;
			// Check for already splitted cabin class
			var exCcIds = $("#listCcSelId").getDataIDs(); 
			var dupCount =0;
			for(var i =0; i<exCcIds.length;i++){
				var exCcData = $("#listCcSelId").getRowData(exCcIds[i]);
				if(exCcData.cabinClassCode === reqData.cabinClassCode){
					dupCount++;
				}
			}
			if(dupCount >=2 ){
				reqData.alreadySplited = "Y";
			}else{
				reqData.alreadySplited = "N";
			}
			$.ajax({
	           url: 'mngInvTemps!searchAllLccForCabinClass.action',
	           data:  reqData,
	           dataType:"json",
	           type : "POST",		           
	    	   success : UI_mngBCAllo.addDataToSplitGrid
			});	
		}
		loadDataToSplitGrid();
		$("#tempDiv").show();
	}else{
		alert("Split Error, Please select one row to split");
	}
}
UI_mngBCAllo.addDataToSplitGrid = function(response){
	var lccList = response.lccListForCabinClass;
	var rowId = $("#listCcSelId").getGridParam('selrow');
	var ccRowData = $("#listCcSelId").getRowData(rowId);
	for(var i=0;i<lccList.length;i++){
		if(lccList[i].logicalCCCode === ccRowData.logicalCabinClass){
			var adultAllocGridText = "<input  type="+'"text"'+" id="+'"txtAdultAlloc'+1+'"'+" class="+'"txtAdultAlloc"'+" size="+'10'+" maxlength="+'20'+" >";
			var infantAllocGridText = "<input  type="+'"text"'+" id="+'"txtInfantAlloc'+1+'"'+" class="+'"txtInfantAlloc"'+" size="+'10'+" maxlength="+'20'+" >";
			var temp = {
					cabinClassCode : ccRowData.cabinClassCode,
					logicalCabinClass : lccList[i].description,
    				rank : lccList[i].nestRank,
    				adultCount : adultAllocGridText, 
    				infantCount :infantAllocGridText,	
    				lccCode : ccRowData.logicalCabinClass,
    				currentAdultCount :ccRowData.adultSeatCount,
    				currentInfantCount :ccRowData.infantSeatCount
    		};
    		$("#tblSplitGrid").addRowData( 1, temp);
    		$("#txtAdultAlloc"+1).numeric();
    		$("#txtInfantAlloc"+1).numeric();
    		document.getElementById("txtAdultAlloc"+1).value = ccRowData.adultSeatCount;
    		document.getElementById("txtInfantAlloc"+1).value = ccRowData.infantSeatCount;
		}else{
			var splitId = i+2;
			var adultAllocGridText = "<input  type="+'"text"'+" id="+'"txtAdultAlloc'+splitId+'"'+" class="+'"txtAdultAlloc"'+" size="+'10'+" maxlength="+'20'+" >";
			var infantAllocGridText = "<input  type="+'"text"'+" id="+'"txtInfantAlloc'+splitId+'"'+" class="+'"txtInfantAlloc"'+" size="+'10'+" maxlength="+'20'+" >";
			var currentAdultCount = 0;
			var currentInfantCount = 0;	
			if(response.alreadySplited === "Y"){
				var bcIds = $("#listCcSelId").getDataIDs();
				for(var j=0;j<bcIds.length;j++){
					var ccData = $("#listCcSelId").getRowData(bcIds[j]);
					if(ccData.cabinClassCode === lccList[i].cabinClassCode && ccData.logicalCabinClass ===lccList[i].logicalCCCode){
						currentAdultCount = ccData.adultSeatCount;
						currentInfantCount = ccData.infantSeatCount;
					}
				}
			}
			var temp = {
					cabinClassCode : ccRowData.cabinClassCode,
					logicalCabinClass : lccList[i].description,
    				rank : lccList[i].nestRank,
    				adultCount : adultAllocGridText, 
    				infantCount :infantAllocGridText,
    				lccCode :lccList[i].logicalCCCode,
    				currentAdultCount :currentAdultCount,
    				currentInfantCount :currentInfantCount
    		};
    		$("#tblSplitGrid").addRowData( i+2, temp);
    		$("#txtAdultAlloc"+splitId).numeric();
    		$("#txtInfantAlloc"+splitId).numeric();
    		var spId =i+2;
    		document.getElementById("txtAdultAlloc"+spId).value = currentAdultCount;
    		document.getElementById("txtInfantAlloc"+spId).value = currentInfantCount;
		}	
	}
}
UI_mngBCAllo.loadSplitAllocation = function(){
	$("#tempDiv").remove();
	var tepDiv = $("<div></div>").attr({"id": "tempDiv", "style":"display:none"});
	$("body").append(tepDiv);
	var table = $("<table></table>").attr("id", "tblSplitGrid");
	var hint = $("<div></div>").attr({"id": "divHint", "style":"margin-top:20px"});
	tepDiv.append(table, hint);
	$("#tblSplitGrid").jqGrid({
		datatype: 'local',
		height: "auto",
		width: '100%',
		height:180,
		colNames:['Cabin Class Code','Logical Cabin Class', 'Logical Cabin<br> Class Code','Current Adult<br>Count','Adult Count' ,'Current Infant<br>Count','Infant Count','Rank'],
		colModel:[
			{name:'cabinClassCode', index:'cabinClassCode', hidden:true },
			{name:'logicalCabinClass', index: 'logicalCabinClass',width:140 },
			{name:'lccCode',index:'lccCode',width:90 },
			{name:'currentAdultCount',index:'currentAdultCount',width:80,align:'center'},
			{name:'adultCount', index:'adultCount', width:80, align:'center'},
			{name:'currentInfantCount',index:'currentInfantCount',width:80,align:'center'},
			{name:'infantCount', index:'infantCount', width:80, align:'center'},
			{name:'rank',index:'rank', width:60, align:"center"}],
		imgpath: "",
		multiselect: false,
		viewrecords: true,
		altRows:true,
		altclass:"GridAlternateRow",
		rowNum:10,
		jsonReader: {
			root: "rows",
			page: "page",
			total: "total",
			records: "records",
			repeatitems: false,
			id: "0" 
		},
	});
	$("#tblSplitGrid").find("input").numeric();
	return tepDiv;
}
UI_mngBCAllo.splitSaveAlloc = function(){
	var selRowIds = $("#listCcSelId").getDataIDs();
	var selRowId = $("#listCcSelId").getGridParam('selrow');
	var selRwData = $("#listCcSelId").getRowData(selRowId);
	var maxAdultTotal =0;
	var maxInfantTotal =0;
	for(var ccI = 0;ccI<selRowIds.length;ccI++){
		var ccRowData = $("#listCcSelId").getRowData(selRowIds[ccI]);
		if(ccRowData.cabinClassCode === selRwData.cabinClassCode){
		maxAdultTotal = maxAdultTotal+parseInt(ccRowData.adultSeatCount);
		maxInfantTotal = maxInfantTotal+parseInt(ccRowData.infantSeatCount);
		}
	}
	var adultTotal =0;
	var infantTotal = 0;
	var ids = jQuery("#tblSplitGrid").getDataIDs(); 
	// validate split data
	for(var i=0;i<ids.length;i++){ 
		var rowData = $("#tblSplitGrid").getRowData(ids[i]);
		adultTotal  =adultTotal+parseInt($("#txtAdultAlloc"+ids[i]).val());
		infantTotal = infantTotal+parseInt($("#txtInfantAlloc"+ids[i]).val());
	}
	if(maxAdultTotal != adultTotal){
		showERRMessage("Total adult allocation should be equal to actual adult capacity");
		return;
	}
	if(maxInfantTotal != infantTotal){
		showERRMessage("Total infant allocation should be equal to actual infant capacity");
		return;
	}
	// Save new splited logical cabin class allocations.
	var data = {};
	if(UI_mngBCAllo.currentInvTempID != 0 && UI_mngBCAllo.currentInvTempID != null){
		data["invTempDTO.invTempID"] = UI_mngBCAllo.currentInvTempID;	
	}
	data["invTempDTO.airCraftModel"] = $("#selAircraftModel").val();
	data["invTempDTO.segment"] = UI_mngBCAllo.segment;
	var status = "INA";
	if (document.getElementById("chkStatus").checked) {
		status = $("#chkStatus").val();
	}
	data["invTempDTO.status"] = status;	
	var ccIds = $("#listCcSelId").getDataIDs(); 
	//var exCCData = $("#listCcSelId").getRowData();
	for(var j = 0;j< ids.length;j++){
		var splitRowData = $("#tblSplitGrid").getRowData(ids[j]);
		var index = "invTempDTO.invTempCCAllocDTOList["+j+"]";
		data[index+".cabinClassCode"] = splitRowData.cabinClassCode;
		data[index+".logicalCabinClassCode"] = splitRowData.lccCode;
		data[index+".adultAllocation"] = parseInt($("#txtAdultAlloc"+ids[j]).val());
		data[index+".infantAllocation"] = parseInt($("#txtInfantAlloc"+ids[j]).val());
		// set invTempCCAllocIds if any
		for(var i = 0;i < ccIds.length;i++){
			var exCCData = $("#listCcSelId").getRowData(ccIds[i]);
			if((exCCData.cabinClassCode === splitRowData.cabinClassCode) && (exCCData.logicalCabinClass === splitRowData.lccCode) && (exCCData.invTempCabinAllocID != null) && (exCCData.invTempCabinAllocID.toString().length > 0)){
				data[index+".invTempCabinAllocID"] = exCCData.invTempCabinAllocID;
			}
		}	
	}
	// If create template mode,add  already unsaved cabin classess in ccGrid.
	var ccAllocId = ids.length;
	var count = 0;
	if((UI_mngBCAllo.currentInvTempID == null) || (UI_mngBCAllo.currentInvTempID.toString().length == 0) || (parseInt(UI_mngBCAllo.currentInvTempID)== 0)){
		for(var i = 0;i < ccIds.length;i++){
			var exCCData = $("#listCcSelId").getRowData(ccIds[i]);
			var selRowId = $("#listCcSelId").getGridParam('selrow');
			if(ccIds[i] !== selRowId ){
				var newCcId = ids.length + count;
				var indx = "invTempDTO.invTempCCAllocDTOList["+newCcId+"]";
				data[indx+".cabinClassCode"] = exCCData.cabinClassCode;
				data[indx+".logicalCabinClassCode"] = exCCData.logicalCabinClass;
				data[indx+".adultAllocation"] = exCCData.adultSeatCount;
				data[indx+".infantAllocation"] = exCCData.infantSeatCount;
				count++;
			}
		}
	}
	$.ajax({
		data: data, 
		url : 'mngInvTemps!saveSplitCCAllocs.action',
		dataType:"json",
		type : "POST",		           
		success : UI_mngBCAllo.splitInvTmpSuccess
	});		
}
UI_mngBCAllo.splitInvTmpSuccess = function(response) {	
	if(response.invTempDTO.invTempID !=null && response.invTempDTO.invTempID > 0 ){
		UI_mngBCAllo.currentInvTempID = response.invTempDTO.invTempID;
		UI_mngBCAllo.segment = response.invTempDTO.segment;
	}	
	if(response.saveSuccess){
		UI_mngBCAllo.createCCSelGrid();
		UI_mngBCAllo.DeleteList = [];
		UI_mngBCAllo.isEdited = false;
		UI_mngBCAllo.allModified = false;	
		UI_mngBCAllo.restrictRoutChange();
		alert("Save/update operation is successfull");
	}
	else{
		UI_mngBCAllo.setErrorStatus();
	}
}
UI_mngBCAllo.ccSelGridOnClick = function() {
	// Warning when change cabin class 
	var bcIds = $("#listCcSelId").getDataIDs();
	var newCabinClass = $("#listCcSelId").getGridParam('selrow');
	if((newCabinClass !== UI_mngBCAllo.oldCabinClass) && (UI_mngBCAllo.oldCabinClass !== -1) && (UI_mngBCAllo.isEdited)){
		var chngeCabinClass = confirm("You will loss the changes for selected cabin class ");
		if (chngeCabinClass) {
			var ccSelRowId = $("#listCcSelId").getGridParam('selrow');
			var ccRowData = $("#listCcSelId").getRowData(ccSelRowId);
	    	UI_mngBCAllo.currentInvTempCCID =ccRowData.invTempCabinAllocID;
			UI_mngBCAllo.changeBCGrid();
			UI_mngBCAllo.oldCabinClass = newCabinClass;
			UI_mngBCAllo.DeleteList = [];
			UI_mngBCAllo.isEdited = false;
		}
		else{
			return;
		}
	}else{
		var ccSelRowId = $("#listCcSelId").getGridParam('selrow');
		var ccRowData = $("#listCcSelId").getRowData(ccSelRowId);
    	UI_mngBCAllo.currentInvTempCCID =ccRowData.invTempCabinAllocID;
		UI_mngBCAllo.changeBCGrid();
		UI_mngBCAllo.oldCabinClass = newCabinClass;
	}
}
UI_mngBCAllo.changeBCGrid = function(){
	$("#divBCAlloc").show();
	$("#divBCAllocOpts").show();
	$("#txtAlloc").numeric();
	var rowId = $("#listCcSelId").getGridParam('selrow');
	var rowData = $("#listCcSelId").getRowData(rowId);	
	UI_mngBCAllo.createBCDropDownList(rowData.cabinClassCode);
	UI_mngBCAllo.GridIndex = rowData.cabinClassCode + rowData.logicalCabinClass;
	var data ={};
	data["cabinClassCode"] = rowData.cabinClassCode;
	data["logicalCabinClassCode"] = rowData.logicalCabinClass;
	data["invTempDTO.airCraftModel"] = $("#selAircraftModel").val();
	data["invTempDTO.segment"] = $("#txtSegment").val();
	data["invTempDTO.invTempID"] = UI_mngBCAllo.currentInvTempID;
	var status = "INA";
	if (document.getElementById("chkStatus").checked) {
		status = $("#chkStatus").val();
	}
	data["invTempDTO.status"] = status;	
	$.ajax({
		data: data, 
		url : 'mngInvTemps!loadBCAllocateGridData.action',
		dataType:"json",
		type : "POST",		           
		success : UI_mngBCAllo.loadDataToBCGrid
	});	
}
UI_mngBCAllo.loadDataToBCGrid = function (response) {
	UI_mngBCAllo.totalBCAllocs = 0;
	UI_mngBCAllo.BCGridID = "#BcAllocGrid"+UI_mngBCAllo.GridIndex;
	UI_mngBCAllo.createBCAllocGrid();
	var ids = jQuery(UI_mngBCAllo.BCGridID).getDataIDs(); 
    for(var i=0;i<ids.length;i++){ 
    	$(UI_mngBCAllo.BCGridID).delRowData(ids[i]);
    }
    objOnFocus();
    var id =0;
    var ccSelRowId = $("#listCcSelId").getGridParam('selrow'); 
    var updatetGridDataList = response.updatedBcAllocGridList;
    if(updatetGridDataList != null && updatetGridDataList.length > 0){
    	for(var it = 0;it<updatetGridDataList.length;it++){
    		if(updatetGridDataList[it].invTempCabinAllocID != null){
    			UI_mngBCAllo.currentInvTempCCID = updatetGridDataList[it].invTempCabinAllocID;
    		}
    		var newBookingClass = updatetGridDataList[it].bookingCode;
    	//	var newAllocation = updatetGridDataList[it].allocWaitListedSeats;
    		var newBCDetailDTO = null;
    		for (var i = 0; i < response.bcDetailsList.length; i++){
    			if(newBookingClass==response.bcDetailsList[i].bookingCode){
    				newBCDetailDTO = UI_mngBCAllo.bcDetailsList[i];
    				id = i;
    				break;
    			}	
    		}
    		var Priority = "<input type="+'"checkbox"'+ "value="+'"Y"'+ "id="+'"priorityFlag'+id+'"'+ "class="+'"priorityFlagChkBx"'+ "onchange = "+'"UI_mngBCAllo.IdentifyEditedPrio(this)"'+" >";
    		var AllocationGridText = "<input  type="+'"text"'+" id="+'"txtGridAlloc'+id+'"'+" class="+'"txtGridAlloc"'+" size="+'10'+" maxlength="+'20'+ " onkeydown=" + '"UI_mngBCAllo.IdentifyEditedAlloc()"'+" >";
    		var Closed   = "<input type="+'"checkbox"'+ "value="+'"CLS"'+ "id="+'"closedStatus'+id+'"'+ "class="+'"closedStatusChkBx"'+"onchange = "+'"UI_mngBCAllo.IdentifyEditedClos(this)"'+" >";
    		var temp = {
    				bookingCode : updatetGridDataList[it].bookingCode,
    				std : newBCDetailDTO.standardCode,
    				rank : newBCDetailDTO.nestRank,
    				fixed : newBCDetailDTO.fixFlag,
    				priorityFlag : Priority,
    				allocation : AllocationGridText, 
    				statuseClosed : Closed,
    				invTmpCabinBCAllocID : updatetGridDataList[it].invTmpCabinBCAllocID,
    				invTempCabinAllocID :updatetGridDataList[it].invTempCabinAllocID,
    				invTempID : updatetGridDataList[it].invTempID,
    				modifyFlag : UI_mngBCAllo.Nonmodify,
    				temporaryID : 0
    		};
    		$(UI_mngBCAllo.BCGridID).addRowData( id, temp);
    		var chkPrioId = "#priorityFlag"+id;
    		if(updatetGridDataList[it].priorityFlag=="Y"){
    			$(chkPrioId).prop('checked', true);  
    		}else{
    			$(chkPrioId).prop('checked', false); 
    		}
    		var chkClsId = "#closedStatus" +id;
    		if(updatetGridDataList[it].statusBcAlloc=="CLS"){
    			$(chkClsId).prop('checked', true);  
    		}else{
    			$(chkClsId).prop('checked', false); 
    		}
    		var allocID = "txtGridAlloc"+id;
    		document.getElementById(allocID).value = updatetGridDataList[it].allocatedSeats;
    		$("#"+allocID).numeric();
    		UI_mngBCAllo.totalBCAllocs = UI_mngBCAllo.totalBCAllocs + parseInt(updatetGridDataList[it].allocatedSeats);
    		document.getElementById('totalBCAllocs').innerHTML = "<font>"+UI_mngBCAllo.totalBCAllocs+"</font>";
    	}
    }else if(ccSelRowId != null && ccSelRowId.length > 0){
    	 /* selected cabin class has no saved data but inventory template ID exist.Then this cabin class has hidden invTempCabinClassID in grid  */
		var ccRowData = $("#listCcSelId").getRowData(ccSelRowId);
    	UI_mngBCAllo.currentInvTempCCID =ccRowData.invTempCabinAllocID; 
    }else{
    	/* selected cabin class belongs to no inventory template.this is create new template mod. */
    	UI_mngBCAllo.currentInvTempCCID = 0;
    }
    UI_mngBCAllo.DisableEdit();
}
UI_mngBCAllo.createBCDropDownList = function(selectedCCCode){
	var submitData = {};
	submitData["cabinClassCode"] = selectedCCCode;
	$.ajax({
		data:submitData, 
		url : 'mngInvTemps!viewBCDetailsforInvTmp.action',
		dataType:"json",
		type : "POST",		           
		success : UI_mngBCAllo.setBCDropDownList
	});
}
UI_mngBCAllo.setBCDropDownList = function (response) {
	//Method to set booking class drop down list data
	UI_mngBCAllo.bcDetailsList = response.bcDetailsList; 
	var bcDropdownListHTML ="";
	bcDropdownListHTML+= "<option value = '' ></option>";
	for (var i = 0; i < UI_mngBCAllo.bcDetailsList.length; i++){
		bcDropdownListHTML += "<option value = '" + UI_mngBCAllo.bcDetailsList[i].bookingCode + "' >" + UI_mngBCAllo.bcDetailsList[i].bookingCode + "</option>";
	}
	$('#selBC').html(bcDropdownListHTML);	
}
UI_mngBCAllo.addBookingClassInvnentory = function(){
	if($("#txtSegment").val().length === 0 && $("#hdnShowRW").val() === "Y"){
		showERRMessage("Segment Code can not be empty");
		return;
	}
    objOnFocus();
    var id =0;
	var newBookingClass = $("#selBC").val();
	var ids = jQuery(UI_mngBCAllo.BCGridID).getDataIDs(); 
    for(var i=0;i<ids.length;i++){ 
    	var bc = $(UI_mngBCAllo.BCGridID).getCell(ids[i],'bookingCode');
    	if(bc === newBookingClass){
    		showERRMessage("Booking Code is alredy exist");
    		return;
    	}
    }   
	var newAllocation = $("#txtAlloc").val();
	if(newAllocation == null || newAllocation == "" || newAllocation < 1 ){
		showERRMessage("BC Allocation Less than Zero or Null");
		return;
	}
	var newBCDetailDTO = null;
	for (var i = 0; i < UI_mngBCAllo.bcDetailsList.length; i++){
		if(newBookingClass==UI_mngBCAllo.bcDetailsList[i].bookingCode){
			newBCDetailDTO = UI_mngBCAllo.bcDetailsList[i];
			id = i;
			break;
		}	
	}
	if (newBookingClass == ""){
		showERRMessage("Please select booking code");
	}else{
		var rowId = $("#listCcSelId").getGridParam('selrow');
		var ccRowData = $("#listCcSelId").getRowData(rowId);
		if(ccRowData.invTempCabinAllocID ==null || parseInt(ccRowData.invTempCabinAllocID) < 0 || isNaN(parseInt(ccRowData.invTempCabinAllocID))){
			// Then this is new template creating flow.No ccAlloc Id
			var ccID = -1;
		}
		else{
			var ccID = ccRowData.invTempCabinAllocID;
		}
		var Priority = "<input type="+'"checkbox"'+ "value="+'"Y"'+ "id="+'"priorityFlag'+id+'"'+ "class="+'"priorityFlagChkBx"'+ "onchange = "+'"UI_mngBCAllo.IdentifyEditedPrio(this)"'+" >";
		var AllocationGridText = "<input  type="+'"text"'+ " id="+'"txtGridAlloc'+id+'"'+" class="+'"txtGridAlloc"'+" size="+'10'+" maxlength="+'20'+ " onkeydown=" + '"UI_mngBCAllo.IdentifyEditedAlloc()"'+" >"; 
		var Closed   = "<input type="+'"checkbox"'+ "value="+'"CLS"'+ "id="+'"closedStatus'+id+'"'+ "class="+'"closedStatusChkBx"'+"onchange = "+'"UI_mngBCAllo.IdentifyEditedClos(this)"'+" >";
		var temp = {					
				bookingCode : newBookingClass,
				std : newBCDetailDTO.standardCode,
				rank : newBCDetailDTO.nestRank,
				fixed : newBCDetailDTO.fixFlag,
				priorityFlag : Priority,
				allocation : AllocationGridText,
				statuseClosed : Closed,
				modifyFlag : UI_mngBCAllo.Add,
				temporaryID: UI_mngBCAllo.temporaryID,
				invTempCabinAllocID: ccID
		};
		UI_mngBCAllo.isEdited = true;
		UI_mngBCAllo.temporaryID += 1;
		$(UI_mngBCAllo.BCGridID).addRowData( id, temp); 
		var allocID = "txtGridAlloc"+id;	
		document.getElementById(allocID).value = newAllocation;	
		$("#"+allocID).numeric();
		UI_mngBCAllo.totalBCAllocs = UI_mngBCAllo.totalBCAllocs + parseInt(newAllocation);
		document.getElementById('totalBCAllocs').innerHTML = "<font>"+UI_mngBCAllo.totalBCAllocs+"</font>";
	}
	UI_mngBCAllo.DisableEdit();
}
UI_mngBCAllo.createBCAllocGrid =function(){
	$("#BCAllocGridContainer").empty().append('<table id='+UI_mngBCAllo.BCGridID.substring(1)+'></table><div id="BcAllocPages"></div>')
	var grdArray = new Array();
	var bcAllocGrid = $(UI_mngBCAllo.BCGridID).jqGrid(
			{
				//url : 'mngInvTemps.action', // use relevant action here
				datatype : "local",
				jsonReader : {root : "rows",page : "page",total : "total",records : "records",repeatitems : false,id : "0"},
				colNames : ['Booking Class','Std', 'Rank', 'Fixed','Priority','Allocation','Closed','invTmpCabinBCAllocID','invTempCabinAllocID','invTempID','modifyFlag','temporaryID'],
				colModel : [ {name :'bookingCode',  index:'bookingCode', width:60}, 
				             {name : 'std',index : 'std',width : 100},
				             {name : 'rank',width : 100,align : "rank"},
				             {name : 'fixed',width : 100,align : "fixed"},
				             {name : 'priorityFlag', index:'priorityFlag', width:40 ,align:"center",sortable:false}, 
				             {name : 'allocation', index:'allocation',width : 160,align:"center",sortable:false},
				             {name : 'statuseClosed', index:'statuseClosed', width:40 ,align:"center",sortable:false},
				             {name : 'invTmpCabinBCAllocID',index : 'invTmpCabinBCAllocID',hidden : true},
				             {name : 'invTempCabinAllocID',index : 'invTempCabinAllocID',hidden : true},
				             {name : 'invTempID',index : 'invTempID',hidden :true},
				             {name : 'modifyFlag',index:'modifyFlag',hidden:true},
				             {name : 'temporaryID', index: 'temporaryID',hidden:true}],
				             
				imgpath : "",
				multiselect : false,
				pager : $('#bcAllocPager'),
				rowNum : 20,
				viewrecords : true,
				height : 200,
				width : 900,
				loadui : 'block',
				beforeSelectRow: function (rowid) {
			  		if ($(this).getGridParam("selrow") === rowid) {
			  			$(this).jqGrid("resetSelection");
			  	    } else {
			  	        return true;
			  	    }
			  	}
			}).navGrid("#bcAllocPages", {refresh : true,edit : false,add : false,del : false,search : false});
}
/*
 * Save On Click
 */
UI_mngBCAllo.saveInvTemp = function() {	
	if(UI_mngBCAllo.isEdited){
		var data = {};
		var bcIds = jQuery(UI_mngBCAllo.BCGridID).getDataIDs();  
		var firstBCTempID = $(UI_mngBCAllo.BCGridID).getCell(bcIds[0],'invTempID');
//		if(bcIds.length === 0 && UI_mngBCAllo.DeleteList.length === 0 ){
//			showERRMessage("Booking Class Allocation list can not be empty");
//			return;
//		}
		if($("#txtSegment").val().length === 0 && $("#hdnShowRW").val() === "Y"){
			showERRMessage("Segment Code can not be empty");
			return;
		}
		// invTempDTO data
		if(UI_mngBCAllo.currentInvTempID != 0 && UI_mngBCAllo.currentInvTempID != null){
			data["invTempDTO.invTempID"] = UI_mngBCAllo.currentInvTempID;
		}
		data["invTempDTO.airCraftModel"] = $("#selAircraftModel").val();
		data["invTempDTO.segment"] = UI_mngBCAllo.segment;
		var status = "INA";
		if (document.getElementById("chkStatus").checked) {
			status = $("#chkStatus").val();
		}
		data["invTempDTO.status"] = status;
		// set edited or newly added invTempCCAlloc data 
		var rowId = $("#listCcSelId").getGridParam('selrow');
		var ccRowData = $("#listCcSelId").getRowData(rowId);
		var index = "invTempDTO.invTempCCAllocDTOList[0]";
		data[index+".cabinClassCode"] = ccRowData.cabinClassCode;
		data[index+".logicalCabinClassCode"] = ccRowData.logicalCabinClass;
		data[index+".adultAllocation"] = ccRowData.adultSeatCount;
		data[index+".infantAllocation"] = ccRowData.infantSeatCount;
		data[index+".invTempCabinAllocID"] = UI_mngBCAllo.currentInvTempCCID;
		//Get all BC alloc Data	 
		var ids = jQuery(UI_mngBCAllo.BCGridID).getDataIDs(); 
		for(var i=0;i<ids.length;i++){ 
			var rowData = $(UI_mngBCAllo.BCGridID).getRowData(ids[i]);
			if(UI_mngBCAllo.allModified && rowData.temporaryID == 0){  // all prio or all closed has clicked.So all rows are updated.
				rowData.modifyFlag = UI_mngBCAllo.Edit;
			}
			if(rowData.modifyFlag === UI_mngBCAllo.Add || rowData.modifyFlag === UI_mngBCAllo.Edit){
				var id = index+".invTempCCBCAllocDTOList["+i+"]";
				data[id+".bookingCode"] = rowData.bookingCode;
				data[id+".updateFlag"] = rowData.modifyFlag;
				data[id+".invTempCabinAllocID"] = rowData.invTempCabinAllocID;
				data[id+".invTmpCabinBCAllocID"] = rowData.invTmpCabinBCAllocID;
				var allocId = "txtGridAlloc"+ids[i];
				var bcAllocation = document.getElementById(allocId).value;
				data[id+".allocatedSeats"] = bcAllocation;
				var chkPrioId = "#priorityFlag"+ids[i];
				if($(chkPrioId).is(':checked')){  
					data[id+".priorityFlag"] = "Y"; 	
				}else{
					data[id+".priorityFlag"] = "N"; 
				}        
				var chkClsId = "#closedStatus" + ids[i];
				if($(chkClsId).is(':checked')){
					data[id+".statusBcAlloc"] = "CLS"; 	
				}else{
					data[id+".statusBcAlloc"] = "OPN"; 
				} 
			}	
		}	
		// Add deleted Data from deleted List if any
		if(UI_mngBCAllo.DeleteList.length > 0){
			for(var i = 0; i<UI_mngBCAllo.DeleteList.length; i++ ){
				var idDel = ids.length +i;
				var idD = index+".invTempCCBCAllocDTOList["+idDel+"]";
				data[idD+".bookingCode"] = UI_mngBCAllo.DeleteList[i].bookingCode;
				//data[idD+".allocatedSeats"] = UI_mngBCAllo.DeleteList[i].allocation;
				data[idD+".updateFlag"] = UI_mngBCAllo.Delete;
				data[idD+".invTempCabinAllocID"] = UI_mngBCAllo.DeleteList[i].invTempCabinAllocID;
				data[idD+".invTmpCabinBCAllocID"] = UI_mngBCAllo.DeleteList[i].invTmpCabinBCAllocID;		
			}		
		}		
		// If Creating new template mode,adding unsaved invTempCabinAllocs
		if(UI_mngBCAllo.currentInvTempID == 0 && UI_mngBCAllo.currentInvTempID != null){
			var selCCrowId = $("#listCcSelId").getGridParam('selrow');
			var ids = $("#listCcSelId").getDataIDs(); 
			for(var i=0;i<ids.length;i++){ 
				var rowData = $("#listCcSelId").getRowData(ids[i]);
				if(selCCrowId !== ids[i]){
					var indx = "invTempDTO.invTempCCAllocDTOList["+ids[i]+"]";
					
					data[indx+".cabinClassCode"] = rowData.cabinClassCode;
					data[indx+".logicalCabinClassCode"] = rowData.logicalCabinClass;
					data[indx+".adultAllocation"] = rowData.adultSeatCount;
					data[indx+".infantAllocation"] = rowData.infantSeatCount;
				}
			}
		}
		// Save template
		$("#frmInvTemp").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"mngInvTemps.action",success: UI_mngBCAllo.saveInvTmpSuccess, error:UI_mngBCAllo.setErrorStatus});
	}else{
		showERRMessage("Add or edited data list is empty");
		return;
	}
}

UI_mngBCAllo.allPriority_Click = function(){  
	if($('#chkPrio').is(':checked')){	
		$(".priorityFlagChkBx").prop('checked', true);
		UI_mngBCAllo.allModified = true;
	}
	else{
		$(".priorityFlagChkBx").prop('checked', false);
		UI_mngBCAllo.allModified = true;
	}
	UI_mngBCAllo.isEdited = true;
}

UI_mngBCAllo.allClosed_Click = function() {
	if($('#chkClosed').is(':checked')){	
		$(".closedStatusChkBx").prop('checked', true);
		UI_mngBCAllo.allModified = true;
	}
	else{
		$(".closedStatusChkBx").prop('checked', false);
		UI_mngBCAllo.allModified = true;
	}
	UI_mngBCAllo.isEdited = true;
}
UI_mngBCAllo.deleteBCAlloc = function(){
	var rowId = $(UI_mngBCAllo.BCGridID).getGridParam('selrow');
	var rowData = $(UI_mngBCAllo.BCGridID).getRowData(rowId);
	if((rowData.temporaryID == 0) && (rowData.temporaryID!=null)){
		// add delete flag to existing data and add to delete List
		rowData.modifyFlag = UI_mngBCAllo.Delete;		
		UI_mngBCAllo.DeleteList.push(rowData);
	}
	var allocID = "#txtGridAlloc"+rowId;	
	UI_mngBCAllo.totalBCAllocs = UI_mngBCAllo.totalBCAllocs - parseInt($(allocID).val());
	document.getElementById('totalBCAllocs').innerHTML = "<font>"+UI_mngBCAllo.totalBCAllocs+"</font>";
	$(UI_mngBCAllo.BCGridID).delRowData(rowId);	
	UI_mngBCAllo.isEdited = true;
}
UI_mngBCAllo.editClick = function(){
	var rowId = $(UI_mngBCAllo.BCGridID).getGridParam('selrow');
	if(rowId != null && rowId.length > 0){
		UI_mngBCAllo.EnableEdit();
	}else{
		showERRMessage("Please select row to edit");
		return;
	}
}
UI_mngBCAllo.saveInvTmpSuccess = function(response) {	
	if(response.invTempDTO.invTempID !=null && response.invTempDTO.invTempID > 0 ){
		UI_mngBCAllo.currentInvTempID = response.invTempDTO.invTempID;
		UI_mngBCAllo.segment = response.invTempDTO.segment;
	}	
	if(response.saveSuccess){
		UI_mngBCAllo.createCCSelGrid();
		UI_mngBCAllo.DeleteList = [];
		UI_mngBCAllo.isEdited = false;
		UI_mngBCAllo.allModified = false;	
		UI_mngBCAllo.restrictRoutChange();
		alert("Save/update operation is successfull");
	}
	else if(response.duplicate){
		alert("Template allready exists ");
		return;
	}else{
		UI_mngBCAllo.setErrorStatus();
	}
}

UI_mngBCAllo.setErrorStatus = function(){
	alert("save failed");
}
UI_mngBCAllo.BackClicked = function() {  
	objOnFocus();
	if(UI_mngBCAllo.isEdited){
		var backPage = confirm("You will loss the changes for selected cabin class ");
		if (backPage) {
			top.LoadPage("showInventoryTemplates.action","SC_INVN_005","Inventory Templates");
			top[1].objTMenu.tabSetValue("SC_INVN_005", "1^");
			top[1].objTMenu.tabRemove('SC_INVN_006');
		}
		else{
			return;
		}
	}else{
		top.LoadPage("showInventoryTemplates.action","SC_INVN_005","Inventory Templates");
		top[1].objTMenu.tabSetValue("SC_INVN_005", "1^");
		top[1].objTMenu.tabRemove('SC_INVN_006');
	}
}
UI_mngBCAllo.StatusChanged= function(element){
	UI_mngBCAllo.isEdited = true;
}
UI_mngBCAllo.IdentifyEditedPrio = function(element){ 
	var rowId = $(UI_mngBCAllo.BCGridID).getGridParam('selrow');
	var rowData = $(UI_mngBCAllo.BCGridID).getRowData(rowId);
	var allocId = "txtGridAlloc"+rowId;
	var bcAllocation = document.getElementById(allocId).value;
	var chkPrioId = "#priorityFlag"+rowId;
	var oldStatusP = false;
	if($(chkPrioId).is(':checked')){  
		oldStatusP =false;
	}else{
		oldStatusP = true;
	}
	var chkClos = "#closedStatus"+rowId;
	var oldStatusC = false;
	if($(chkClos).is(':checked')){  
		oldStatusC =true;
	}else{
		oldStatusC = false;
	}	
	//if(rowData.modifyFlag !== UI_mngBCAllo.Edit){
		if(rowData.temporaryID == 0) { // edited existing data
			rowData.modifyFlag = UI_mngBCAllo.Edit;
		}else if(rowData.modifyFlag === UI_mngBCAllo.Add && rowData.temporaryID > 0) { // edited newly added data 
			rowData.modifyFlag = UI_mngBCAllo.Add;
		}	
		$(UI_mngBCAllo.BCGridID).setRowData(rowId, rowData);
		if(!oldStatusP){  
			$(chkPrioId).prop('checked', true); 
		}
		if(oldStatusC){  
			$(chkClos).prop('checked', true); 
		}
		document.getElementById(allocId).value = bcAllocation;
	//}
	UI_mngBCAllo.isEdited = true;
}
UI_mngBCAllo.IdentifyEditedClos = function(element){   
	var rowId = $(UI_mngBCAllo.BCGridID).getGridParam('selrow');
	var rowData = $(UI_mngBCAllo.BCGridID).getRowData(rowId);
	var allocId = "txtGridAlloc"+rowId;
	var bcAllocation = document.getElementById(allocId).value;
	var chk = "#closedStatus"+rowId;
	var oldStatusC = false;
	if($(chk).is(':checked')){  
		oldStatusC =false;
	}else{
		oldStatusC = true;
	}
	var chkPrio = "#priorityFlag"+rowId;
	var oldStatusP = false;
	if($(chkPrio).is(':checked')){  
		oldStatusP =true;
	}else{
		oldStatusP = false;
	}
	if(rowData.modifyFlag !== UI_mngBCAllo.Edit){
		if(rowData.temporaryID == 0) { // edited existing data
			rowData.modifyFlag = UI_mngBCAllo.Edit;
		}else if(rowData.modifyFlag === UI_mngBCAllo.Add && rowData.temporaryID > 0) { // edited newly added data 
			rowData.modifyFlag = UI_mngBCAllo.Add;
		}	
		$(UI_mngBCAllo.BCGridID).setRowData(rowId, rowData);
		if(!oldStatusC){  
			$(chk).prop('checked', true); 
		}
		if(oldStatusP){  
			$(chkPrio).prop('checked', true); 
		}
		document.getElementById(allocId).value = bcAllocation;
	}
	UI_mngBCAllo.isEdited = true;
}
UI_mngBCAllo.IdentifyEditedAlloc = function(){   /// TO DO : validate allocation edit 
	var rowId = $(UI_mngBCAllo.BCGridID).getGridParam('selrow');
	var rowData = $(UI_mngBCAllo.BCGridID).getRowData(rowId);
	var allocId = "txtGridAlloc"+rowId;
	var chkPrio = "#priorityFlag"+rowId;
	var oldStatusP = false;
	if($(chkPrio).is(':checked')){  
		oldStatusP =true;
	}else{
		oldStatusP = false;
	}
	var chkClos = "#closedStatus"+rowId;
	var oldStatusC = false;
	if($(chkClos).is(':checked')){  
		oldStatusC =true;
	}else{
		oldStatusC = false;
	}
	if(rowData.modifyFlag !== UI_mngBCAllo.Edit && rowData.temporaryID == 0){
		rowData.modifyFlag = UI_mngBCAllo.Edit;
		var bcAllocation = document.getElementById(allocId).value;
		$(UI_mngBCAllo.BCGridID).setRowData(rowId, rowData);
		
		if(oldStatusP){  
			$(chkPrio).prop('checked', true); 
		}
		if(oldStatusC){  
			$(chkClos).prop('checked', true); 
		}
		$("#"+allocId).numeric();
		document.getElementById(allocId).value = bcAllocation;
		$("#"+allocId ).focus();
	}		
	UI_mngBCAllo.isEdited = true;
}
function objOnFocus() {
	top[2].HidePageMessage();	
}
