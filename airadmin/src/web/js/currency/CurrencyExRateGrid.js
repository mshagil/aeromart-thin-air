// ---------------- Exchange Rate Maintenance Grid Columns
var objDGColEXt1 = new DGColumn();
objDGColEXt1.columnType = "CUSTOM";
objDGColEXt1.width = "19%";
objDGColEXt1.arrayIndex = 1;
objDGColEXt1.toolTip = "Effective From (Zulu)";
objDGColEXt1.headerText = "Effect From (Zulu)";
objDGColEXt1.ID = 'txtEffectiveFrom';
objDGColEXt1.htmlTag = "<input type='text' id='txtEffectiveFrom' name='txtEffectiveFrom' :CUSTOM: size='15' maxlength='16' style='text-align:righ;'  onBlur='parent.checkExRateDate(:ROW:,1, this)' invalidText='true'>";
objDGColEXt1.itemAlign = "center"
objDGColEXt1.sort = "true";

var objDGColEXt2 = new DGColumn();
objDGColEXt2.columnType = "CUSTOM";
objDGColEXt2.width = "12%";
objDGColEXt2.arrayIndex = 2;
objDGColEXt2.toolTip = "Effective To (Zulu)";
objDGColEXt2.headerText = "Effect To (Zulu)";
objDGColEXt2.ID = 'txtEffectiveTo';
objDGColEXt2.htmlTag = "<input type='text' id='txtEffectiveTo' name='txtEffectiveTo' :CUSTOM: size='15' maxlength='16' onBlur='parent.checkExRateDate(:ROW:,2, this)' invalidText='true' >";
objDGColEXt2.itemAlign = "center"

var objDGColEXt3 = new DGColumn();
objDGColEXt3.columnType = "CUSTOM";
objDGColEXt3.width = "10%";
objDGColEXt3.arrayIndex = 3;
objDGColEXt3.toolTip = "Sales Rate";
objDGColEXt3.headerText = "Sales Rate";
objDGColEXt3.ID = 'txtExRate';
objDGColEXt3.htmlTag = "<input type='text' id='txtExRate' name='txtExRate'  size='10' onKeyUp='parent.exRateValidateDecimal(this,15,5)' onKeyPress='parent.exRateValidateDecimal(this,15,5)' class='rightText'  :CUSTOM: >";
objDGColEXt3.itemAlign = "center"

var objDGColEXt4 = new DGColumn();
objDGColEXt4.columnType = "CHECKBOX";
objDGColEXt4.width = "9%";
objDGColEXt4.arrayIndex = 4;
objDGColEXt4.toolTip = "Active";
objDGColEXt4.headerText = "Act";
objDGColEXt4.itemAlign = "center";

var objDGColEXt5 = new DGColumn();
objDGColEXt5.columnType = "CUSTOM";
objDGColEXt5.width = "9%";
objDGColEXt5.arrayIndex = 9;
objDGColEXt5.toolTip = "Purchase Rate";
objDGColEXt5.headerText = "Purchase Rate";
objDGColEXt5.ID = 'txtPurchaseRate';
objDGColEXt5.htmlTag = "<input type='text' id='txtPurchaseRate' name='txtPurchaseRate'  size='10' onKeyUp='parent.exRateValidateDecimal(this,15,5)' onKeyPress='parent.exRateValidateDecimal(this,15,5)' class='rightText'  :CUSTOM: >";
objDGColEXt5.itemAlign = "center"

// ---------------- Exchange Rate Maintenance Grid
var objDGExr = new DataGrid("spnExRate");
objDGExr.addColumn(objDGColEXt1);
objDGExr.addColumn(objDGColEXt2);
objDGExr.addColumn(objDGColEXt3);
objDGExr.addColumn(objDGColEXt5);
objDGExr.addColumn(objDGColEXt4);

objDGExr.width = "99%";
objDGExr.height = "80px";
objDGExr.headerBold = false;
objDGExr.rowSelect = true;
objDGExr.backGroundColor = "#ECECEC";
objDGExr.setColors = "setColors";
objDGExr.arrGridData = arrExRateData;
objDGExr.seqNo = true;
objDGExr.seqNoWidth = "5%";
objDGExr.rowClick = "exRateGridRowClick";
objDGExr.displayGrid();

function setColors(strRowData) {
	var strColor = "";
	if (strRowData[4] == false) {
		strColor = "#AAAAAA";
	}
	return strColor;
}
