var screenId = "SC_ADMN_007";
var valueSeperator = "~";

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "8%";
objCol1.arrayIndex = 1;
// objCol1.toolTip = "Currency Code" ;
objCol1.headerText = "Currency Code";
objCol1.itemAlign = "center";
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "9%";
objCol2.arrayIndex = 2;
// objCol2.toolTip = "Description" ;
objCol2.headerText = "Description";
objCol2.itemAlign = "left";
objCol2.sort = "true";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "9%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Base";
objCol4.headerText = "Base Currency";
objCol4.itemAlign = "center"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "12%";
objCol10.arrayIndex = 15;
objCol10.toolTip = "Current Rate";
objCol10.headerText = "Current Rate";
objCol10.itemAlign = "right"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "10%";
objCol7.arrayIndex = 10;
objCol7.headerText = "XBE Visibility";
objCol7.itemAlign = "center";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "10%";
objCol8.arrayIndex = 11;
objCol8.headerText = "IBE Visibility";
objCol8.itemAlign = "center";

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "10%";
objCol9.arrayIndex = 12;
objCol9.headerText = "Card Payment Visibility";
objCol9.itemAlign = "center";

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "8%";
objCol11.arrayIndex = 21;
objCol11.headerText = "IBE PG";
objCol11.itemAlign = "center";

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "8%";
objCol12.arrayIndex = 22;
objCol12.headerText = "XBE PG";
objCol12.itemAlign = "center";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "8%";
objCol5.arrayIndex = 5;
// objCol5.toolTip = "status" ;
objCol5.headerText = "Status";
objCol5.itemAlign = "center";

var objCol13 = null;
var bol=(showItineraryFareBreakDown === "true")
if(bol){
	objCol13 = new DGColumn();
	objCol13.columnType = "label";
	objCol13.width = "8%";
	objCol13.arrayIndex = 26;
	objCol13.headerText = "Show total payment in base currency";
	objCol13.itemAlign = "center";
}




// ---------------- Grid
var objDG = new DataGrid("spnCurrencies");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol4);
objDG.addColumn(objCol10);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol11);
objDG.addColumn(objCol12);
objDG.addColumn(objCol5);
if(bol){
	objDG.addColumn(objCol13);
}

objDG.width = "99%";
// objDG.height = "234px";
objDG.height = "180px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.backGroundColor = "#ECECEC";
objDG.setColors = "setColors";
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords;
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function myRecFunction(intRecNo, intErrMsg) {
	if (intErrMsg != "") {
		objMsg.MessageText = "<li> " + intErrMsg;
		objMsg.MessageType = "Error";
		ShowPageMessage();
	}
}

function setColors(strRowData) {
	var strColor = "";
	if (strRowData[5] == "In-Active") {
		strColor = "#AAAAAA";
	}
	return strColor;
}