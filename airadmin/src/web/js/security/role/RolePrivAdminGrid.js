var screenId = "SC_ADMN_001";
var valueSeperator = "~";
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Role ID";
objCol1.itemAlign = "right"
objCol1.sort = "true"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "27%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Role Name";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "10%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Status";
objCol3.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnRoles");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);

objDG.width = "99%";
objDG.height = "290px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = roleData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function myRecFunction(intRecNo, intErrMsg) {
	if (intErrMsg != "") {
		objMsg.MessageText = "<li> " + intErrMsg;
		objMsg.MessageType = "Error";
		ShowPageMessage();
	}
}
