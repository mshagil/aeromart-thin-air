var selectedSSR='';

jQuery(document).ready(function(){
	var selStatus = $("#selStatus").val();
	var grdArray = new Array();	

	$("#divSearchPanel").decoratePanel("Search Templates");
	$("#divDispSSRTemp").decoratePanel("Add/Modify SSR Template");
	$("#divResultsPanel").decoratePanel("SSR Templates");			
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnDelete').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnSave').decorateButton();
	$('#btnSearch').decorateButton();
	$('#btnChgAdd').decorateButton();
	$('#btnChgEdit').decorateButton();
	enableGridButtons(false);
	enableFormButtons(false);
	constructGrid();
	
	function constructGrid(){
		jQuery("#listSSRTemp").jqGrid({	
			datatype: function(postdata) {
		        $.ajax({
		           url: 'showSSRTemplate!searchSSRTemplates.action?selStatus='+selStatus,		    
		           datatype: "json",
		           complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
				            	 $("#listSSRTemp")[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message)
			            	  }
			            	  top[2].HideProgress();
			              }
			           }
		        });
			},		        	  
			jsonReader : {
				  root: "rows", 
				  page: "page",
				  total: "total",
				  records: "records",
				  repeatitems: false,					 
				  id: "0"				  
			},													
			colNames:['&nbsp;','Template Id', 'Template Code','Description', 'Status','Version','createdBy','createdDate', 'ssrTemplQty'], 
			colModel:[ 	{name:'id', width:40, jsonmap:'id'},
					   	{name:'templateId',index:'templateId', width:125, jsonmap:'ssrTemplate.templateId'}, 
					   	{name:'templateCode', width:175, jsonmap:'ssrTemplate.templateCode'},						   
					   	{name:'description', width:275,  jsonmap:'ssrTemplate.description'},
						{name:'status', width:150, align:"center", jsonmap:'ssrTemplate.status'},
					   	{name:'version', width:225, align:"center",hidden:true, jsonmap:'ssrTemplate.version'},
					   	{name:'createdBy', width:225, align:"center",hidden:true, jsonmap:'ssrTemplate.createdBy'},
					   	{name:'createdDate', width:225, align:"center",hidden:true, jsonmap:'ssrTemplate.createdDate'},
						{name:'ssrTemplQty', width:225, align:"center",hidden:true}		   
					  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
			pager: jQuery('#temppager'),
			rowNum:20,						
			viewrecords: true,
			height:175,
			width:925,
			loadui:'block',
			onSelectRow: function(rowid){
				if(isPageEdited()) {
					$('#frmSSR').clearForm();
					hideMessage();
					setPageEdited(false);
					$("#rowNo").val(rowid);
					fillForm(rowid);
					disableControls(true);
					enableSearch();
					enableGridButtons(true);
					enableFormButtons(false);
				}
			},
			gridComplete: function(){
				$("#btnAdd").attr('disabled', false);
			}
		}).navGrid("#temppager",{refresh: true, edit: false, add: false, del: false, search: false});			
	}
	
	var options = { 
		cache: false,
		beforeSubmit:  showRequest,  	// pre-submit callback - this can be used to do the validation part 
		success: showResponse,   		// post-submit callback 			 
		dataType:  'json'				// 'xml', 'script', or 'json' (expected server response type) 

	}; 

	var delOptions = {	
		cache: false,
		beforeSubmit:  showDelete,  							// pre-submit callback - this can be used to do the validation part 
		success: showResponse,   								// post-submit callback 				 
		url: 'showSSRTemplate!deleteTemplate.action',         	// override for form's 'action' attribute 
		dataType:  'json',        								// 'xml', 'script', or 'json' (expected server response type) 
		resetForm: true        									// reset the form after successful submit 
		 
	};
	
	function showRequest(formData, jqForm, options){
	    if(	trim($("#templateCode").val()) == "") {
	    	showCommonError("Error", templateCodeReq);
	    	return false; 
	    }
	    if(	trim($("#description").val()) == "") {
	    	showCommonError("Error", templateDescReq);
	    	return false; 
	    } 
		if(	trim($("#ssrCabinQtys").val()) == "") {
	    	showCommonError("Error", ssrRqrd);
	    	return false; 
	    }
	    top[2].ShowProgress();
	    return true; 
	}
	
	function showDelete(formData, jqForm, options){
		return confirm(deleteRecoredCfrm);
	}
	
	function showResponse(responseText, statusText){
		top[2].HideProgress();
		showCommonError(responseText['msgType'], responseText['succesMsg']);
	    if(responseText['msgType'] != "Error"){
		    alert(responseText['succesMsg']);
			setPageEdited(false);
		    $('#frmSSR').clearForm();
			jQuery("#listSSRQty").clearGridData();
			jQuery("#listSSRTemp").trigger("reloadGrid");
			enableGridButtons(false);
			enableFormButtons(false);
			$("select#selTemplate").html(responseText['templOption']);
	    }	
	}
	
	function loadSSRList(condition,ssrValue) {
		var data = {};
		data["isGridSSR"] = condition;
		data["ssrValue"] = ssrValue;
		
		if (condition == "true") {
			$("#selSSR").attr('disabled', true); 
			var ssrArr = ssrValue.split("#");
			$("#selSSR").html("<option value='" + ssrValue + "'>" + ssrArr[1] + "</option>");
		} else {
			$("#selSSR").attr('disabled', false); 
			$("#frmSSR").ajaxSubmit({
			dataType: 'json', 
			data:data, 
			url:"showSSRTemplate!getSSRsForCabinClass.action",							
			success: loadSuccess});
		}		
	}
	
	function checkForSSR() {
		var classOfService = $("#cos").val();
		var selCosArr = classOfService.split('_');
		var selectedSSRLcc = selCosArr[0];
		var selectedSSRCabin = selCosArr[1];
		var ssrArr = trim($("#selSSR").val()).split("#");	
		
		for(var i= 0; i < grdArray.length; i++) {
			var gridCabinAndLogicalCC = grdArray[i]["cabinAndLogicalCC"];
			var gridCosArr = gridCabinAndLogicalCC.split('_');
			var gridSSRLcc = gridCosArr[0];
			var gridSSRCabin = gridCosArr[1];
			
			if(grdArray[i]["ssrId"] == ssrArr[0]){
				if(gridCabinAndLogicalCC == classOfService){
					return false;
				} else if(selectedSSRCabin == gridSSRCabin){
					var selSSRLastChar = selectedSSRLcc.substr(-1);
					var gridSSRLastChar = gridSSRLcc.substr(-1);
					if((selSSRLastChar == '^' && gridSSRLastChar != '^')
							|| (selSSRLastChar != '^' && gridSSRLastChar == '^')){
						return false;
					}
				}
			}
			
		}
		return true;
	} 	
	
	var loadSuccess = function(response){
		if (response.ssrsForCC != null && trim(response.ssrsForCC) != "") {
			$("#selSSR").html(response.ssrsForCC);
		} else {
			alert("No SSRs for selected cabin class");
			$("#selSSR").html("<option value='-1# '>");
			$("#selSSR").attr('disabled', true); 
		}
	}
	 
	function fillForm(rowid){
	 	jQuery("#listSSRTemp").GridToForm(rowid,"#frmSSR");
		if(jQuery("#listSSRTemp").getCell(rowid,'status') == 'ACT'){
			$("#status").attr('checked', true);
		} else {
			$("#status").attr('checked', false);
		}
		
		var ssrTemplQty = jQuery("#listSSRTemp").getCell(rowid,'ssrTemplQty');
		var arrSSRTemplQty = ssrTemplQty.split("~");
		var ssrQty;
		grdArray = new Array();
		for(var i =0;i<arrSSRTemplQty.length-1;i++){
			grdArray[i] = new Array(); 
			ssrQty = arrSSRTemplQty[i].split("#");
			grdArray[i]["sId"] = i+1;
			grdArray[i]["ssr"] = ssrQty[0];
			grdArray[i]["templateId"] = ssrQty[1];
			grdArray[i]["ssrId"] = ssrQty[2];
			grdArray[i]["maxQty"] = ssrQty[3];
			grdArray[i]["ssrVersion"] = ssrQty[4];
			grdArray[i]["ssrStatus"] = ssrQty[5];
			grdArray[i]["templCabinQty"] = ssrQty[6];
			grdArray[i]["cosDescription"] = ssrQty[7];
			grdArray[i]["cabinAndLogicalCC"] = ssrQty[8];
		}
		populateSSRGrid(); 
	}
	
	$('#cos').change(function(){
		loadSSRList('false',"");
	});
	
	$('#btnSearch').click(function() {
		hideMessage();
		$("#listSSRTemp").clearGridData();
		var strUrl = "showSSRTemplate!searchSSRTemplates.action?selStatus="+$("#selStatus").val()
						+"&selTemplateId="+$("#selTemplate").val();
		selStatus = $("#selStatus").val();
		$.getJSON(strUrl, function(response){
			$("#listSSRTemp")[0].addJSONData(response);		
	 	});	
		enableFormButtons(false);
		enableGridButtons(false);				
		$('#frmSSR').clearForm();
		$("#version").val('-1');
		jQuery("#listSSRQty").clearGridData();
		grdArray = new Array();
		disableControls(true);
		enableSearch();
		$("#btnAdd").attr('disabled', false);
	});
	
	$('#btnAdd').click(function() {
		hideMessage();
		disableControls(false);
		$('#frmSSR').clearForm();
		$("#version").val('-1');
		jQuery("#listSSRQty").clearGridData();
		grdArray = new Array();
		enableFormButtons(true);	
	});
	
	$('#btnEdit').click(function() {
		hideMessage();
		disableControls(false);
		var ssrRow = $("#selSSR").val();
		if (ssrRow != null) {
			$("#cos").attr('disabled', true); 
			$("#selSSR").attr('disabled', true); 
		}
		$('#templateCode').attr('disabled', true); 
		enableFormButtons(true);			
	});
	
	$('#btnDelete').click(function() {
		disableControls(false);				
		$('#frmSSR').ajaxSubmit(delOptions);
		return false;			
	}); 
	
	$('#btnReset').click(function() {
		hideMessage();
		$("#selSSR").html("<option value='-1# '>");
		$("#selSSR").attr('disabled', false); 
		$("#cos").attr('disabled', false); 
		$("#maxQty").val('');
		$("#templCabinQty").val('');
		selectedMeal= '';
		if($("#rowNo").val() == '') {
			grdArray = new Array();
			$("#templateCode").val('');
			$("#description").val('');
			$("#status").val('');
			
			jQuery("#listSSRQty").clearGridData();
		} else {
			fillForm($("#rowNo").val());
		}
	});
	
	$('#btnSave').click(function() {
		hideMessage();
		disableControls(false);
		if($("#status").attr('checked')){
			$("#status").val('ACT');
		} else {
			$("#status").val('INA');
		}
		
		$("#ssrCabinQtys").val(buildSSRQtyStr());	
		$('#templateCode').attr('disabled', false); 
		$('#frmSSR').ajaxSubmit(options);
		return false;	
	});
	
	function buildSSRQtyStr(){
		var ssrStr = "";
		for(var j=0;j<grdArray.length;j++){
			ssrStr += grdArray[j]["ssr"]+"#";
			ssrStr += grdArray[j]["templateId"]+"#";
			ssrStr += grdArray[j]["ssrId"]+"#";
			ssrStr += grdArray[j]["maxQty"]+"#";
			ssrStr += grdArray[j]["ssrVersion"]+"#";
			ssrStr += grdArray[j]["ssrStatus"] + "#";
			ssrStr += grdArray[j]["templCabinQty"] + "#";	
			ssrStr += grdArray[j]["cabinAndLogicalCC"] + "#";	
			ssrStr += "~";	
	   	}
		return ssrStr;
	}
	
	function populateSSRGrid(){
		jQuery("#listSSRQty").clearGridData();				
		for(var i =0;i<grdArray.length;i++){
			jQuery("#listSSRQty").addRowData(i+1, grdArray[i]);
		}				
	}
	
	function enableGridButtons(cond){
		if(cond) {
			$("#btnEdit").enableButton();
			$("#btnDelete").enableButton();					
		} else {
			$("#btnEdit").disableButton();
			$("#btnDelete").disableButton();					
		}				
		
	}
	
	function enableFormButtons(cond){
		if(cond) {
			$("#btnReset").enableButton();
			$("#btnSave").enableButton();
		} else {
			$("#btnReset").disableButton();
			$("#btnSave").disableButton();
		}
		
	}
	
	function enableSearch(){
		$("#btnSearch").attr('disabled', false);
		$("#selTemplate").attr('disabled', false);		
		$("#selStatus").attr('disabled', false);
	}
	
	function disableControls(cond){
		$("input").each(function(){ 
		      $(this).attr('disabled', cond); 
		});	
		$("textarea").each(function(){ 
		      $(this).attr('disabled', cond); 
		});	
		$("select").each(function(){ 
		      $(this).attr('disabled', cond); 
		});	
		$("#btnClose").attr('disabled', false);
	}
	
	function setPageEdited(isEdited) {
		top[1].objTMenu.tabPageEdited(screenId, isEdited);
		hideMessage();
	}

	function isPageEdited() {
		return top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId));
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}	
	
	disableControls(true);
	$("#btnAdd").attr('disabled', false);
	enableSearch();
	
	jQuery("#listSSRQty").jqGrid({
	        datatype: 'clientSide',
	        colNames:['&nbsp;', 'SSR', 'templateId', 'ssrId', 'Max Quantity','COS', 'version','Status', 'cabinAndLogicalCC', 'templCabinQty'], 
	        colModel :[
	        {name:'sId',index:'sId', width:60, sorttype:'int'},
	        {name:'ssr',index:'ssr',  width:200},
	        {name:'templateId',index:'templateId', hidden:true, width:180},
	        {name:'ssrId',index:'ssrId', hidden:true, width:70},
	        {name:'maxQty', index:'maxQty', width:85, align:"right"},
	        {name:'cosDescription',width:175,align:"center"},
	        {name:'ssrVersion', width:225, align:"center",hidden:true},
	        {name:'ssrStatus', width:125, align:"center"},
	        {name:'cabinAndLogicalCC', width:225, align:"center",hidden:true},
			{name:'templCabinQty', width:100, align:"center",hidden:true}],
	        viewrecords: true,
	        imgpath: '../../themes/default/images/jquery', multiselect:false,
	        loadui:'block',
	        height:100,
	        onSelectRow: function(rowid){
	    		selectedSSR =  rowid;
				hideMessage();
				var classOfService = jQuery("#listSSRQty").getCell(rowid,'cabinAndLogicalCC');
	    		$('#cos option').each(function(i, option){ 
					var selVal = $(this).val();
					if (trim(selVal) == trim(classOfService)){
						$('#cos option[value$="' + selVal + '"]').attr('selected', true);
					}				
				});
	    		jQuery("#listSSRQty").GridToForm(rowid,frmSSR);
	    		var sltdSSR = jQuery("#listSSRQty").getCell(rowid,'ssrId') +"#"+jQuery("#listSSRQty").getCell(rowid,'ssr');	
				loadSSRList('true',sltdSSR);
	    		$("#selSSR").val(sltdSSR); 
	    		$("#selSSR").attr('disabled', true); 
				$("#cos").attr('disabled', true);
		    }
	});
	
	$('#btnChgAdd').click(function() {
		var ssrArr = null;
		if ($("#selSSR").val() != null) {
			ssrArr = trim($("#selSSR").val()).split("#");
		}
	
		$("#selSSR").attr('disabled', false);
		if (($("#selSSR").val() == null) || (trim(ssrArr[0]) == "") || (trim(ssrArr[0]) == "-1")) {
	    	showCommonError("Error", ssrRqrd);
	    	return false; 
		} else if(!isPositiveInt(trim($("#maxQty").val())) || (parseInt($("#maxQty").val())<=0)) {
	    	showCommonError("Error", qtyInvalid);
	    	return false; 
	    } else if(($("#ssrStatus").val() == null) || (trim($("#ssrStatus").val()) == "")) {
	    	showCommonError("Error", statusRequired);
	    	return false; 
	    } else if(!checkForSSR()) {			    	 
			$("#selSSR").attr('disabled', false);
	    	showCommonError("Error", ssrExst);
	    	return false;			    	
	    } else {
	    	setPageEdited(true);
			var arrSize = grdArray.length;
			
			grdArray[arrSize] = new Array();				
			grdArray[arrSize]["sId"] = arrSize+1;
			grdArray[arrSize]["ssr"] = trim(ssrArr[1]);
			grdArray[arrSize]["templateId"] = trim($("#templateId").val());
			grdArray[arrSize]["ssrId"] = trim(ssrArr[0]);
			grdArray[arrSize]["maxQty"] = trim($("#maxQty").val());
			grdArray[arrSize]["ssrVersion"] = -1;
			grdArray[arrSize]["ssrStatus"] = trim($("#ssrStatus").val());
			grdArray[arrSize]["templCabinQty"] = ' ';
			grdArray[arrSize]["cabinAndLogicalCC"] = trim($("#cos").val());
			if(trim($("#cos").val().split("_")[0]) == trim($("#cos").val().split("_")[1])){
				grdArray[arrSize]["cosDescription"] = trim($("#cos option:selected").text());
			}else{
				grdArray[arrSize]["cosDescription"] = trim($("#cos option:selected").text()).substring(2);
			}
			jQuery("#listSSRQty").addRowData(arrSize+1, grdArray[arrSize]);
			$("#selSSR").html("<option value='-1# '>");
			$("#selSSR").attr('disabled', true); 
			$("#maxQty").val('');	
			$("#templCabinQty").val('');
		}							
	});
	
	$('#btnChgEdit').click(function() {
		if(selectedSSR == ''){
			return;
		} else if(selectedSSR != '' &&
				$("#maxQty").val() != null &&
				(parseInt($("#maxQty").val())<=0  ||
				!isPositiveInt(trim($("#maxQty").val()))) ) {
			showCommonError("Error", qtyInvalid);
			return;
		}
		
		setPageEdited(true);
		var inId = $("#sId").val();
		
		var qty = trim($("#maxQty").val());
		var stat = trim($("#ssrStatus").val());
		grdArray[inId-1]["maxQty"] = qty;
		grdArray[inId-1]["ssrStatus"] = stat;
		jQuery("#listSSRQty").setRowData( inId, { maxQty: qty, ssrStatus : stat});
	}); 
});