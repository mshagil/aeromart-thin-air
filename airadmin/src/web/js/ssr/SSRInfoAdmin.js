var SSR_CATEGORY = {INFLIGHT_SERVICE:1, AIRPORT_SERVICE:2, AIRPORT_TRANSFER:3};

jQuery(document)
		.ready(
				function() { // the page is ready

					$("#divSearch").decoratePanel(
							"Search Special Service Requests");
					$("#divResultsPanel").decoratePanel(
							"Special Service Requests");
					$("#divDispSSRInfo").decoratePanel(
							"Add/Modify Special Service Requests");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();
					
					$('#btnUpload').decorateButton();
					$('#btnUploadThumbnail').decorateButton();
					$('#btnSSRInfoFD').decorateButton();
					
					if(ssrCutOffTimeEnabled == false){
						$('#trSsrCutoffTime').hide();
					}
					
					
					disableSSRInfoButton();
					createPopUps();
					PopUpData();
					var grdArray = new Array();
					jQuery("#listSSRInfo")
							.jqGrid(
									{
										url : 'showSSRInfo!searchSSRInfo.action',
										datatype : "json",
										jsonReader : {
											root : "rows",
											page : "page",
											total : "total",
											records : "records",
											repeatitems : false,
											id : "0"
										},
										colNames : [ '&nbsp;', 'ssrId',
												'SSR Code','Name', 'Description',
												'ssrCatId', 'ssrSubCatId',
												'Status', 'version',
												'createdBy', 'createdDate',
												'modifiedBy', 'modifiedDate',
												'shownInIBE', 'shownInXBE', 'shownInGds',
												'applyConstarints',
												'applicableAirports',
												'consMaxMinTime',  
												'applicableOnds',
												'ssrDescriptionForDisplay', 
												'Display Position',
												'ssrCutoffTime',
												'shownInWs',
												'valid for pal cal',
												'userDefinedSSR'],
										colModel : [
												{
													name : 'Id',
													width : 10,
													jsonmap : 'id'
												},
												{
													name : 'ssrId',
													index : 'ssrId',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.SSRId'
												},
												{
													name : 'ssrCode',
													index : 'ssrCode',
													width : 25,
													jsonmap : 'ssrInfo.SSRCode'
												},
												{
													name : 'ssrName',
													index : 'ssrName',
													width : 50,
													jsonmap : 'ssrInfo.SSRName'
												},
												{
													name : 'ssrDesc',
													index : 'ssrDesc',
													width : 110,
													jsonmap : 'ssrInfo.SSRDescription'
												},
												{
													name : 'ssrCatId',
													index : 'ssrCatId',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.SSRCategoryId'
												},
												{
													name : 'ssrSubCatId',
													index : 'ssrSubCatId',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.SSRSubCategoryId'
												},
												{
													name : 'status',
													width : 25,
													align : "center",
													jsonmap : 'ssrInfo.status'
												}, /*{
													name : 'maxQtyPerFlight',
													width : 30,
													align : "center",
													hidden : !ssrInvEnabled,
													jsonmap : 'ssrInfo.maxQtyPerFlight'
												},*/
												{
													name : 'version',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.version'
												},
												{
													name : 'createdBy',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.createdBy'
												},
												{
													name : 'createdDate',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.createdDate'
												},
												{
													name : 'modifiedBy',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.modifiedBy'
												},
												{
													name : 'modifiedDate',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.modifiedDate'
												},
												{
													name : 'shownInIBE',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.shownInIBE'
												},
												{
													name : 'shownInXBE',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.shownInXBE'
												},
                                                {
													name : 'shownInGDS',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.shownInGDS'
												},
												{
													name : 'applyConstarints',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'applyConstarints'
												},
												{
													name : 'applicableAirports',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'applicableAirports'
												}, {
													name : 'consMaxMinTime',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'consMaxMinTime'
												},{
													name : 'applicableOnds',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'applicableOnds'
												}, { 
												    name:'ssrDescriptionForDisplay', 
													width:225, 
													align:"center",
													hidden:true
												}, {
													name : 'sortOrder',
													width : 30,
													align : "center",
													jsonmap : 'ssrInfo.sortOrder'
												}, {
													name : 'ssrCutoffTime',
													width : 15,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.ssrCutoffTime'
												}, {
													name : 'shownInWs',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.shownInWs'
												},{
													name : 'validForPalCal',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.validForPALCAL'
												},{
													name : 'userDefinedSSR',
													width : 30,
													align : "center",
													hidden : true,
													jsonmap : 'ssrInfo.userDefinedSSR'
												}

										],
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										pager : jQuery('#ssrinfopager'),
										rowNum : 20,
										viewrecords : true,
										height : 210,
										width : 930,
										loadui : 'block',
										onSelectRow : function(rowid) {
											$("#rowNo").val(rowid);
											
											var ssrCatId = $("#listSSRInfo").getCell(rowid, 'ssrCatId');
											var orderCount = getSortOrderCount(ssrCatId);
																						
											showHideApplicability(ssrCatId);
											fillSortOrderList(orderCount);
											fillForm(rowid);
											disableControls(true);
											enableSearch();
											disableSSRInfoButton();
											enableGridButtons();											
											enablePopupControls();
											setSSRForDisplayList();
										}
									}).navGrid("#ssrinfopager", {
								refresh : true,
								edit : false,
								add : false,
								del : false,
								search : true
							});
					
					function formatMaxQuantity(cellValue, opts, rowObject){
						if(cellValue < 0){
							return "";
						}
					}

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						dataType : 'json' // 'xml', 'script', or 'json'
					// (expected server response type)
					};

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						url : 'showSSRInfo!deleteSSRInfo.action', // override
						// for
						// form's
						// 'action'
						// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
						// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};

					// pre-submit callback
					function showRequest(formData, jqForm, options) {

						if (trim($("#ssrCode").val()) == "") {
							showCommonError("Error", ssrCodeReq);
							return false;
						}

						if (trim($("#ssrCode").val()).length > 4) {
							showCommonError("Error", ssrLenErr);
							return false;
						}
						
						if (trim($("#ssrName").val()) == "") {
							showCommonError("Error", ssrNameReq);
							return false;
						}
						
						if (trim($("#ssrDesc").val()) == "") {
							showCommonError("Error", ssrDescReq);
							return false;
						}
						
						
						if (!isAlphaNumericSpclDOL(trim($("#ssrDesc").val()))) {
							showCommonError("Error", "SSR description contains invalid characters");
							return false;
						}						


						if (trim($("#ssrCatId").val()) == "-1"
								|| trim($("#ssrCatId").val()) == "") {
							showCommonError("Error", servCatReq);
							return false;
						}

						if (trim($("#ssrSubCatId").val()) == "-1"
								|| trim($("#ssrSubCatId").val()) == "") {
							showCommonError("Error", servSubCatReq);
							return false;
						}

						if(trim($("#ssrCatId").val()) == SSR_CATEGORY.INFLIGHT_SERVICE){
							if($("#hdnOndList").val() == ""){
								showCommonError("Error", ondListReq);
								return false;
							}
						} else if(trim($("#ssrCatId").val()) == SSR_CATEGORY.AIRPORT_SERVICE || trim($("#ssrCatId").val()) == SSR_CATEGORY.AIRPORT_TRANSFER){
							if (ls3.getselectedData().length == 0) {
								showCommonError("Error", airportListReq);
								return false;
							}							
						}									

						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							$('#frmSSRInfo').clearForm();
							$("#imgSSR").attr('src', '');
							$("#imgSSRThamb").attr('src', '');
							ls3.selectedData("");
							clearAllOnds();
							jQuery("#listSSRInfo").trigger("reloadGrid");
							disableSSRInfoButton();
							$("#btnAdd").attr('disabled', false);
							$("#btnSave").enableButton();
							$("#totalAirportServiceCount").val(responseText['airportServiceCount']);
							$("#totalInflightServiceCount").val(responseText['inflightServiceCount']);
							$("#totalAirportTranferCount").val(responseText['airportTranferCount']);
							alert(responseText['succesMsg']);
							$("#btnAdd").click();
						}

					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
					
					function validateSSRData() {
						var cutOffTime = $("#ssrCutoffTime").val();
						if(cutOffTime != ""){
							var cutOffHrsMins = cutOffTime.split(":");
							if (cutOffHrsMins[1] > 59) {
								showERRMessage(invalidTimeFormat);
								$("#ssrCutoffTime").focus();
								return false;
							}
							return true;
						}
						return true;
					}
					
					$('#btnSave').click(function() {
						if(validateSSRData()){
							disableControls(false);
							$('#dvUpLoad').html($('#dvUpLoad').html());
							$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
							if ($("#status").attr('checked')) {
								$("#status").val('ACT');
							} else {
								$("#status").val('INA');
							}
							if ($("#validForPalCal").attr('checked')) {
								$("#validForPalCal").val(true);
							} else {
								$("#validForPalCal").val(false);
							}
							
							if ($("#userDefinedSSR").attr('checked')) {
								$("#userDefinedSSR").val(true);
							} else {
								$("#userDefinedSSR").val(false);
							}
							
							if(trim($("#ssrCatId").val())==SSR_CATEGORY.INFLIGHT_SERVICE || 
									trim($("#ssrCatId").val())==SSR_CATEGORY.AIRPORT_SERVICE ||
									    trim($("#ssrCatId").val())==SSR_CATEGORY.AIRPORT_TRANSFER){
								if ($("#shownInIBECheck").attr('checked')) {
									$("#shownInIBE").val(true);
								} else {
									$("#shownInIBE").val(false);
								}
	
								if ($("#shownInXBECheck").attr('checked')) {
									$("#shownInXBE").val(true);
								} else {
									$("#shownInXBE").val(false);
								}
	
	                            if ($("#shownInGdsCheck").attr('checked')) {
									$("#shownInGds").val(true);
								} else {
									$("#shownInGds").val(false);
								}
	                            if ($("#shownInWsCheck").attr('checked')) {
									$("#shownInWs").val(true);
								} else {
									$("#shownInWs").val(false);
								}
							}else{
								$("#shownInIBE").val(true);
								$("#shownInXBE").val(true);
	                            $("#shownInGds").val(true);
	                            $("#shownInWs").val(true);
	                        }
							
							setField("hdnAirportList", ls3.getselectedData());
							$("#ssrDescForDisplay").val(buildSSRForDisplayStr());
							$("#hdnOndList").val(getAllOndList());
							
							$('#frmSSRInfo').ajaxSubmit(options);
							return false;
						}
					});			
					
				
					var upOptionsImage = {				 
							beforeSubmit:  showUpdate,  
							success: showUploadResponseImage,
							url: 'showUpload.action?mode=ssrImage',
							dataType:  'script'        
						 };
						
						var upOptionsThumbnailImage = {				 
							beforeSubmit:  showUpdate,  
							success: showUploadResponseThumbnail,   
							url: 'showUpload.action?mode=ssrThumbnailImage',  
							dataType:  'script'        
						};

						function showUpdate(formData, jqForm, options) {			
							var newImg = new Image();
							newImg.src = $("#imgSSR").attr('src');				
							var height = newImg.height;
							var width = newImg.width;
							
							if(width == 1024 || height == 700){
								showCommonError("Error", "Image is too large ");
								return false;
							}
							top[2].ShowProgress();
						} 
					
					$('#btnUpload').click(function() {
						$('#frmSSRInfo').ajaxSubmitWithFileInputs(upOptionsImage);
						return false;	
					});
					
					$('#btnUploadThumbnail').click(function() {
						$('#frmSSRInfo').ajaxSubmitWithFileInputs(upOptionsThumbnailImage);
						return false;	
					});

					function showUploadResponseImage(responseText, statusText)  {
						top[2].HideProgress();
					    eval(responseText);		    		
					    showCommonError(responseText['msgType'], responseText['succesMsg']);
					    if(responseText['msgType'] != "Error"){		    	
					    	$('#dvUpLoad').html($('#dvUpLoad').html());
					    	$("#uploadImage").val('UPLOADED');	
					    	// No need to add the no_cache for meals since they
							// are uploaded on runtime
					    	 $("#imgSSR").attr('src',ssrImagePath+'ssrImage_test.jpg?rel='+new Date());
					    }		    
					    
					} 
					
					function showUploadResponseThumbnail(responseText, statusText)  {
						top[2].HideProgress();
					    eval(responseText);		    		
					    showCommonError(responseText['msgType'], responseText['succesMsg']);
					    if(responseText['msgType'] != "Error"){		    	
					    	$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
					    	$("#uploadImageThumbnail").val('UPLOADED');	
					    	// No need to add the no_cache for meals since they
							// are uploaded on runtime
					    	 $("#imgSSRThamb").attr('src', ssrImagePath+'ssrThumbnailImage_test.jpg?rel='+new
							 Date());
					    }		    
					    
					}			
					

					$('#btnAdd').click(function() {
						disableControls(false);
						$('#frmSSRInfo')[0].reset();
						$("#selSelected").empty();
						$("#version").val('-1');
						$("#rowNo").val('');
						$("#appConsId").val('');
						$("#ssrId").val('');
						$("#hdnOldAirportList").val('');						
						$('#btnEdit').disableButton();
						$('#btnDelete').disableButton();
						$("#btnSave").enableButton();
						$("#btnReset").enableButton();						
						$('#dvUpLoad').html($('#dvUpLoad').html());
						$('#dvUpLoadThum').html($('#dvUpLoadThum').html());						
						$('#btnUpload').enableButton();
						$('#btnUploadThumbnail').enableButton();					
						
						$("#shownInXBECheck").prop('checked', false);
						$("#shownInIBECheck").prop('checked', false);
						$("#shownInGdsCheck").prop('checked', false);
						$("#shownInWsCheck").prop('checked', false);
						$("#applDeparture").prop('checked', false);
						$("#applArrival").prop('checked', false);	
						$("#applTransit").prop('checked', false);		
						$("#status").prop('checked', false);
						$("#validForPalCal").prop('checked', false);
						$("#userDefinedSSR").prop('checked', false);
						
						$("#imgSSR").attr('src', '');	
						$("#imgSSRThamb").attr('src', '');	
						setField("hdnMode", "ADD");
						ls3.selectedData("");
						enableDisableXBEnIBE(true);
					});

					$('#btnEdit').click(function() {
						disableControls(false);
						$('#btnDelete').disableButton();
						$('#btnUpload').enableButton();
						$('#btnUploadThumbnail').enableButton();
						$("#btnSave").enableButton();
						setField("hdnMode", "EDIT");
						
						$('#ssrCode').attr('disabled', true);
						enableDisableXBEnIBE(true);
					});
					$('#btnReset').click(function() {
						if ($("#rowNo").val() == '') {
							$('#frmSSRInfo').resetForm();							
							ls3.selectedData("");
						} else {

							fillForm($("#rowNo").val());
							$('#dvUpLoad').html($('#dvUpLoad').html());
							$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
						}

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmSSRInfo').ajaxSubmit(delOptions);
						return false;
					});
					
					$('#selCategory').change(function() {
						$("#selSubCategory").empty();
						var optionVal = '<option value="ALL" selected="selected">All</option>' + jsonSSRSubCatMap[$(this).val()];
						$("#selSubCategory").html(optionVal);
					});
					
					$('#ssrCatId').change(function() {
						var selectedVal = $(this).val();
						fillSSRSubCatList(selectedVal);	
						showHideApplicability(selectedVal);
						var orderVal = 	getSortOrderCount(selectedVal);
						fillSortOrderList(orderVal);		
						if($("#hdnMode").val() == "ADD"){
							var newVal =  (orderVal == null || orderVal == undefined || orderVal == "") ? 2 : (parseInt(orderVal)+ 1);
							$('#sortOrder').append(new Option(newVal, newVal));
						}		
						
					});

					$('#btnSearch')
							.click(
									function() {
										var newUrl = 'showSSRInfo!searchSSRInfo.action?';
										newUrl += 'bagSrch.ssrCode='
												+ $("#selSsrCode").val();
										newUrl += '&bagSrch.status='
												+ $("#selStatus").val();
										newUrl += '&bagSrch.ssrCategory='
												+ $("#selCategory").val();
										newUrl += '&bagSrch.ssrSubCategory='
												+ $("#selSubCategory").val();
										newUrl += '&page=1';

										$("#listSSRInfo").clearGridData();
										$.getJSON(newUrl, function(response) {
											$("#listSSRInfo")[0]
													.addJSONData(response);
											$('#frmSSRInfo').resetForm();
										});
									});

					function fillForm(rowid) {
						jQuery("#listSSRInfo").GridToForm(rowid, '#frmSSRInfo');
						
						var selectedVal = $("#listSSRInfo").getCell(rowid, 'ssrCatId');
												
						fillSSRSubCatList(selectedVal);
						$("#ssrSubCatId").val($("#listSSRInfo").getCell(rowid, 'ssrSubCatId'));
						
						if (jQuery("#listSSRInfo").getCell(rowid, 'status') == 'ACT') {
							$("#status").attr('checked', true);
						} else {
							$("#status").attr('checked', false);
						}
						if (jQuery("#listSSRInfo").getCell(rowid, 'validForPalCal') == 'true') {
							$("#validForPalCal").attr('checked', true);
						} else {
							$("#validForPalCal").attr('checked', false);
						}
						if (jQuery("#listSSRInfo").getCell(rowid, 'userDefinedSSR') == 'true') {
							$("#userDefinedSSR").attr('checked', true);
						} else {
							$("#userDefinedSSR").attr('checked', false);
						}

						if (jQuery("#listSSRInfo").getCell(rowid, 'shownInIBE') == 'true') {
							$("#shownInIBECheck").attr('checked', true);
						} else {
							$("#shownInIBECheck").attr('checked', false);
						}

						if (jQuery("#listSSRInfo").getCell(rowid, 'shownInXBE') == 'true') {
							$("#shownInXBECheck").attr('checked', true);
						} else {
							$("#shownInXBECheck").attr('checked', false);
						}

                        if (jQuery("#listSSRInfo").getCell(rowid, 'shownInGDS') == 'true') {
							$("#shownInGdsCheck").attr('checked', true);
						} else {
							$("#shownInGdsCheck").attr('checked', false);
						}
                        
                        if (jQuery("#listSSRInfo").getCell(rowid, 'shownInWs') == 'true') {
							$("#shownInWsCheck").attr('checked', true);
						} else {
							$("#shownInWsCheck").attr('checked', false);
						}
						var dbImageCode = jQuery("#listSSRInfo").getCell(rowid,'ssrCode');
						// No need to add the no_cache for meals since they are
						// uploaded on runtime
						$("#imgSSR").attr('src', ssrImagePath+'ssr_'+dbImageCode+'.jpg?rel='+new Date());  
						$("#imgSSRThamb").attr('src', ssrImagePath+'ssr_thumbnail_'+dbImageCode+'.jpg?rel='+new Date()); 
											

						enableGridButtons();
						enableDisableXBEnIBE(false);
						setApplicableAirports();
						setApplicableOnds();
						setApplyConstraints();
					}
					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("file").each(function(){ 
						      $(this).attr('disabled', cond); 
						});	
						$("#btnClose").enableButton();
						
						if (listForDisplayEnabled) {
							$("#btnSSRInfoFD").enableButton();
						} else {
							$('#btnSSRInfoFD').attr("disabled", true); 
						}

					}

					function enableSearch() {
						$('#selStatus').attr("disabled", false);
						$('#selCategory').attr("disabled", false);
						$('#selSubCategory').attr("disabled", false);
						$('#selSsrCode').attr("disabled", false);
						$('#btnSearch').enableButton();
					}

					function disableSSRInfoButton() {
						$("#btnEdit").disableButton();
						$("#btnDelete").disableButton();
						$("#btnReset").disableButton();
						$("#btnSave").disableButton();
						$('#btnUpload').disableButton();
						$('#btnUploadThumbnail').disableButton();
					}

					function enableGridButtons() {
						$("#btnAdd").enableButton();
						$("#btnEdit").enableButton();
						$("#btnDelete").enableButton();
						$("#btnReset").enableButton();
						if (listForDisplayEnabled) {
							$("#btnSSRInfoFD").enableButton();
						} else {
							$('#btnSSRInfoFD').attr("disabled", true); 
						}
					}

					disableControls(true);
					$("#btnAdd").enableButton();
					$('#selCategory').attr("disabled", false);
					$('#selSsrCode').attr("disabled", false);
					$('#selStatus').attr("disabled", false);
					$('#selSubCategory').attr("disabled", false);
					$('#btnSearch').enableButton();

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
					}

				});

function setApplyConstraints() {

	resetApplyConstraints();

	if (jQuery("#listSSRInfo").getCell($("#rowNo").val()) >= 0) {
		var applyConstarints = jQuery("#listSSRInfo").getCell(
				$("#rowNo").val(), 'applyConstarints');

		var arrConstraints = new Array();
		if (applyConstarints != null && applyConstarints != "")
			arrConstraints = applyConstarints.split(",");

		if (arrConstraints.length > 3) {
			// applDeparture,applArrival,applTransit
			if (arrConstraints[1] == "Y") {
				// Enable Departure
				$("#applDeparture").attr('checked', true);
			}
			if (arrConstraints[2] == "Y") {
				// Enable Arrival
				$("#applArrival").attr('checked', true);
			}
			if (arrConstraints[3] == "Y") {
				// Enable Transit
				$("#applTransit").attr('checked', true);
			}
			setField("appConsId", arrConstraints[0]);
		}

	}

}

function resetApplyConstraints() {
	$("#applTransit").attr('checked', false);
	$("#applArrival").attr('checked', false);
	$("#applDeparture").attr('checked', false);
}

function setApplicableAirports() {
	// //3,SHJ,3^
	var selectedAirports = "";
	if (jQuery("#listSSRInfo").getCell($("#rowNo").val()) >= 0) {
		var applicableAirports = jQuery("#listSSRInfo").getCell(
				$("#rowNo").val(), 'applicableAirports');
		var arrAirports = new Array();

		if (applicableAirports != null && applicableAirports != "")
			arrAirports = applicableAirports.split("^");
		var arrAirportsProp = new Array();
		for (i = 0; i < arrAirports.length; i++) {
			if (arrAirports[i] != null && arrAirports[i] != "") {
				arrAirportsProp = arrAirports[i].split(",");
				if (arrAirportsProp[1] != null && arrAirportsProp[1] != "") {
					selectedAirports += arrAirportsProp[1];
					selectedAirports += ",";
				}
			}
		}

	}
	setField("hdnOldAirportList",selectedAirports);
	ls3.selectedData(selectedAirports)
}

function setApplicableOnds(){
	$("#selSelected").empty();
	var selectedOnds = "";
	if (jQuery("#listSSRInfo").getCell($("#rowNo").val()) >= 0) {
		var applicableOnds = jQuery("#listSSRInfo").getCell(
				$("#rowNo").val(), 'applicableOnds');
		var arrOnds = new Array();

		if (applicableOnds != null && applicableOnds != "")
			arrOnds = applicableOnds.split("^");
		var arrOndsProp = new Array();
		for (i = 0; i < arrOnds.length; i++) {
			if (arrOnds[i] != null && arrOnds[i] != "") {
				arrOndsProp = arrOnds[i].split(",");
				if (arrOndsProp[1] != null && arrOndsProp[1] != "") {
					var ondCode = (arrOndsProp[1]).replace(/\*\*\*/g, "All");
					selectedOnds += arrOndsProp[1] + ",";
					var options = $("<option>"+arrOndsProp[1]+"</option>").attr("value", "ondCode");
					$('#selSelected').append(options);
				}
			}
		}

	}
	
	$("#hdnOldOndList").val(selectedOnds);
}

function checkValidAmount(fieldId) {
	var checkIntparam = trim($("#" + fieldId).val());

	if (!isPositiveInt(checkIntparam) || (parseInt(checkIntparam) < 0)) {
		return false;
	}

	return true;
}

function alphaCustomValidate(val){
	customValidate(val,/[0-9`!@#$%\s&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
}

function descriptionOLTransVal(val){
	customValidate(val,/[=>~,"']/g);
}

function isAlphaNumericSpclDOL(s){return RegExp("^[a-zA-Z0-9_.\& \w\s]+$").test(s);}

function enableDisableXBEnIBE(isEnabled){
	$("#shownInIBECheck").attr('disabled', !isEnabled);
	$("#shownInXBECheck").attr('disabled', !isEnabled);
	$("#shownInGdsCheck").attr('disabled', !isEnabled);
	$("#shownInWsCheck").attr('disabled', !isEnabled);;
}

function btnAddSegment_click() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = $("#selDepature option:selected").text();
	var arr = $("#selArrival option:selected").text();
	var via1 = $("#selVia1 option:selected").text();
	var via2 = $("#selVia2 option:selected").text();
	var via3 = $("#selVia3 option:selected").text();
	var via4 = $("#selVia4 option:selected").text();

	var deptVal = $("#selDepature").val();
	var arrVal = $("#selArrival").val();
	var via1Val = $("#selVia1").val();
	var via2Val = $("#selVia2").val();
	var via3Val = $("#selVia3").val();
	var via4Val = $("#selVia4").val();

	if (dept == "") {
		showERRMessage(depatureRqrd);
		$("#selDepature").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage(departureInactive);
		$("#selDepature").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRqrd);
		$("#selArrival").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage(arrivalInactive);
		$("#selArrival").focus();

	} else if (dept == arr && (via1 == "" && via2 == "" && via3 == "" && via4 == "" )) {		
		showERRMessage(depatureArriavlSame);
		$("#selArrival").focus();
	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		$("#selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		$("#selDepature").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage(viaEqual);
		$("#selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + viaInactive);
		$("#selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + viaInactive);
		$("#selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + viaInactive);
		$("#selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + viaInactive);
		$("#selVia4").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}
		str += "/" + arr;

		var control = document.getElementById("selSelected");
		for ( var r = 0; r < control.options.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(ondRequired);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
		}
	}
}

function btnRemoveSegment_click() {
	var control = document.getElementById("selSelected");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
		}
	}

}

function clearAllOnds() {
	var control = document.getElementById("selSelected");
	control.options.length = 0;
	for ( var t = 0; t <= (control.length); t++) {
		control.options[t] = null;
	}

}

function getAllOndList() {
	var control = document.getElementById("selSelected");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		var str = control.options[t].text;
		var strReplace = control.options[t].text;
		if (str.indexOf("All") != -1) {
			strReplace = str.replace(/All/g, "***");
		}
		values += strReplace + ",";
	}
	return values;
}

function fillSSRSubCatList(selectedVal){
	if(selectedVal != -1){
		$("#ssrSubCatId").empty();
		var optionVal = '<option value="-1" selected="selected"></option>' + jsonSSRSubCatMap[selectedVal];
		$("#ssrSubCatId").html(optionVal);
		
		if(selectedVal == SSR_CATEGORY.INFLIGHT_SERVICE){
			$("#tblSelApplicableOnd").show();
			$("#tblSelApplicableAirport").hide();
		} else if(selectedVal == SSR_CATEGORY.AIRPORT_SERVICE || selectedVal == SSR_CATEGORY.AIRPORT_TRANSFER){
			$("#tblSelApplicableAirport").show();
			$("#tblSelApplicableOnd").hide();
		}
	} else {
		$("#tblSelApplicableAirport").hide();
		$("#tblSelApplicableOnd").hide();
	}
}

function fillSortOrderList(val){
	if(val == null || val == undefined || val == ""){
		val = 1;
	} 
	$('#sortOrder').empty();
	for(var i=1; i <= val; i++){
		$('#sortOrder').append(new Option(i, i));
	}
}

// description language translations
function PopUpData(){
	var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> SSR Description For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
		html += '<select name="language" id="language"  style="width:150px; "> ';
		html += '<option value="-1"></option>'+languageHTML;				
		html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addSSRForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeSSRForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:200px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr>  ';		
		
		html += '<tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>SSR Description</font><font class="mandatory">&nbsp;*</font></td> <td valign="top"><input name="txtDescForDisplayOL" type="text" id="txtDescForDisplayOL" size="32" maxlength="1000" onkeyUp="descriptionOLTransVal(this)" onkeyPress="descriptionOLTransVal(this)" /></td> </tr>';		
		html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveSSRForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

	DivWrite("spnPopupData",html);
}

function POSenable(){			
	if(!listForDisplayEnabled)
		return;
	if(getFieldByID("ssrDesc").value=="" || getFieldByID("ssrDesc").value==null){
			alert("No SSR is selected, please select SSR and try again");
			return;
		}
	
	if(getValue('hdnMode') == 'ADD'){
		var translationList = getFieldByID("translation");
		translationList.options.length = 0;

		setField("txtDescForDisplayOL", "");
		setField("language","-1");
	}
	
	setField("hdnMode", "SAVELISTFORDISP");

	showPopUp(1);
}

function POSdisable(bFromClose){			
	if(!listForDisplayEnabled)
		return;
	if(bFromClose){
		setField("hdnMode","");
	}
	 hidePopUp(1);
}


function addSSRForDisplay(){			
	if(!listForDisplayEnabled)
		return;
	
	if(getValue('txtDescForDisplayOL').length ==0){
		return false;
	}
	
	var selLang = getValue("language");
	
	if(selLang == '-1')
		return false;
	
	if(findValueInList(selLang, "translation") == true){
		alert("SSR description for the selected language already exist");
		return false;
	}
	
	
	if(!isAlphaNumericSpclDOL(getValue('txtDescForDisplayOL'))){
		alert("SSR description contains invalid characters");
		return false;
	}

	var selLangName = getCurSelectedLabel("language");
	var ssrDescription = getValue("txtDescForDisplayOL");	
	
	addToList("translation", selLangName+"=>"+ssrDescription, selLang);
	
	setField("txtDescForDisplayOL", "");
	
	setField("language","-1");
}

function removeSSRForDisplay(){			
	if(!listForDisplayEnabled)
		return;
	var translationList = getFieldByID("translation");
	var selected = translationList.selectedIndex;
	if(selected != -1){
		var langCode = translationList.options[selected].value;
		var label = translationList.options[selected].text.split("=>");
		setField("language", langCode);
		setField("txtDescForDisplayOL", label[1]);
		
		translationList.remove(selected);
	}
}

function saveSSRForDisplay(){			
	if(!listForDisplayEnabled)
		return;
	if(getValue('ssrDesc').length > 0 && getValue("language")!='-1'){
		if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
			return false;
	}
	var strMode = getText("hdnMode");
	var translationList = getFieldByID("translation");
	var size = translationList.length;
	var langCode;						
	var ssrDescription;
	var label;
	var str="";
	var size;
	var i=0;
	var j=0;
	var newData = new Array();
	var count = 0;

	var ssrTranslations = jQuery("#listSSRInfo").getCell($("#rowNo").val(),'ssrDescriptionForDisplay');
	
	var arrSsrTrl = new Array();
	if(ssrTranslations != null && ssrTranslations != "")
		arrSsrTrl = ssrTranslations.split("~");
	
	var ssrTrl;
	grdArray = new Array();
	
	for(var i =0;i<arrSsrTrl.length-1;i++){
		grdArray[i] = new Array(); 
		ssrTrl = arrSsrTrl[i].split(",");
		grdArray[i][0] = ssrTrl[0];
		grdArray[i][1] = ssrTrl[1];
		grdArray[i][2] = ssrTrl[2];							
	}
	
	for(i=0;i<translationList.length;i++){
		langCode = translationList.options[i].value;
		label = translationList.options[i].text.split("=>");

		if (label.length > 1){
			ssrDescription = label[1];
		}else{
			ssrDescription = getFieldByID("ssrDesc");
		}		
		
			
		size = grdArray.length;
		var found = false;
		for(j=0;j<size;j++){
			if(grdArray[j][0] == langCode){									
				newData[count++] = new Array(grdArray[j][0],langCode,ssrDescription);
				found = true;
				break;
			}
		}
		if(!found){								
			newData[count++] = new Array("-1",langCode,ssrDescription);								
		}
	}
	var langCode2;
	var size1 = grdArray.length;
	var size2 = newData.length;
	for(i=0;i<size1;i++){// original list
		found = false;
		for(j=0;j<size2;j++){// new data
			langCode = grdArray[i][1];
			langCode2 = newData[j][1];
			if(langCode == langCode2){// the lang code exist in the new data
				found = true;
			}
		}
		if(!found){								
			newData[newData.length] = new Array( grdArray[i][0],grdArray[i][1],ssrDescription);
		}
	}
	grdArray = newData;

	if(validateSSRForDisplay() == false){
		return false;
	}
	$('#btnSave').click();
	return POSdisable(false);
}


function setSSRForDisplayList(){			
	if(!listForDisplayEnabled)
		return;

	if(jQuery("#listSSRInfo").getCell($("#rowNo").val())>=0 ){
		setField("txtDescForDisplayOL", "");
		setField("language","-1");

		var translationList = getFieldByID("translation");	
		translationList.options.length = 0;
		var langCode ;		
		var ssrDescription;
		var langName;
		
		var i=0;
		
		var ssrTranslations = jQuery("#listSSRInfo").getCell($("#rowNo").val(),'ssrDescriptionForDisplay');
		
		var arrSsrTrl = new Array();
		if(ssrTranslations != null && ssrTranslations != "")
			arrSsrTrl = ssrTranslations.split("~");
		var ssrTrl;
		grdArray = new Array();
		for(var i =0;i<arrSsrTrl.length-1;i++){
			grdArray[i] = new Array(); 
			ssrTrl = arrSsrTrl[i].split(",");
			grdArray[i][0] = ssrTrl[0]; // id
			grdArray[i][1] = ssrTrl[1]; // langcode
			grdArray[i][2] = ssrTrl[2]; // desc
		}
		for(i=0;i<grdArray.length;i++){
			langCode = grdArray[i][1];

			ssrDescription = grdArray[i][2];

			if(ssrDescription == "null")
				continue;
			langName = getListLabel("language", langCode);
			addToList("translation",langName+"=>"+ssrDescription,langCode);
		}
	}
}

function validateSSRForDisplay(){
	if(!listForDisplayEnabled)
		return;
	var strMode = getText("hdnMode");
	var size2=getFieldByID("language").length;
	var valid = true;
	var size1;
	var i;
	var j;
	var langCode;						
	var ssrDescription;
	var found = false;
	if(strMode == "ADD")
		valid = false;
	else{
		size1 = grdArray.length;
		if(size1 != (size2-1))// -1 to ignore the first empty item
			valid = false;
		else{
			for(i=0;i<size1;i++){
				ssrDescription = grdArray[i][1];
				if(ssrDescription == "null"){
					valid = false;
					break;
				}
			}
		}
	}
	if(!valid){
		alert("SSR description for display is missing for all or some languages, original SSR description will copied to all languages") ;
		if(strMode != "ADD"){

				var descr = getValue("ssrDesc");

				for(i=1;i<size2;i++){// lang list, start from 1 to ignore the
										// first item (empty)
					found = false;
					langCode = getFieldByID("language").options[i].value;
					for(j=0;j<size1;j++){

						ssrDescription = grdArray[j][2];
						if(ssrDescription == "null"){
							grdArray[j][2] = descr;
						}
						if(langCode == grdArray[j][1]){
							found = true;
							continue;
						}
					}
					
					if(!found){
						grdArray[grdArray.length] = new Array("-1",langCode,descr);
					}

						
				}
		}
	}
	return true;
}

function buildSSRForDisplayStr(){		
	// ltid,langCode,lt
	var strMode = getText("hdnMode");
	var ssrStr = "";
	
	if(strMode == "ADD"){												

		ssrDescription = getValue("ssrDesc");		
		var size2=getFieldByID("language").length;

		for(i=1;i<size2;i++){
			langCode = getFieldByID("language").options[i].value;
			
			if(ssrStr.length>0)
				ssrStr += "~";

			ssrStr=ssrStr +"-1,"+ langCode+","+ssrDescription;
		}
	}else {
		prepareSSRToSave();
		if(grdArray != null){					
			for(var j=0;j<grdArray.length;j++){
				// id,langcode,desc
				ssrStr += grdArray[j][0]+","; 
				ssrStr += grdArray[j][1]+",";				
				ssrStr += grdArray[j][2];									
				ssrStr += "~";	
		   	}
		}	
	}		

	return ssrStr;
}

function prepareSSRToSave(){
	var strMode = getText("hdnMode");
	var translationList = getFieldByID("translation");
	var size = translationList.length;
	var langCode;
	var ssrDescription;
	var label;
	var str="";
	var size;
	var i=0;
	var j=0;
	var newData = new Array();
	var count = 0;
	
	var ssrTranslations = jQuery("#listSSRInfo").getCell($("#rowNo").val(),'ssrDescriptionForDisplay');
	var arrSsrTrl = new Array();
	if(ssrTranslations != null && ssrTranslations != "")
		arrSsrTrl = ssrTranslations.split("~");
	var ssrTrl;
	grdArray = new Array();						
	for(var i =0;i<arrSsrTrl.length-1;i++){
		grdArray[i] = new Array(); 
		ssrTrl = arrSsrTrl[i].split(",");
		grdArray[i][0] = ssrTrl[0];  // id
		grdArray[i][1] = ssrTrl[1];  // langcode
		grdArray[i][2] = ssrTrl[2];  // desc
					
	}

	for(i=0;i<translationList.length;i++){
		langCode = translationList.options[i].value;
		label = translationList.options[i].text.split("=>");
		
		if (label.length > 1 && label[1]!=""){
			ssrDescription = label[1];
		}else{
			ssrDescription = getValue("ssrDesc");
		}
			
		size = grdArray.length;
		var found = false;
		for(j=0;j<size;j++){
			if(grdArray[j][1] == langCode){									
				newData[count++] = new Array(grdArray[j][0],langCode,ssrDescription);
				found = true;
				break;
			}
		}
		if(!found){								
			newData[count++] = new Array( "-1",langCode,ssrDescription);
			
		}
	}
	var langCode2;
	var size1 = grdArray.length;
	var size2 = newData.length;
	for(i=0;i<size1;i++){// original list
		found = false;
		for(j=0;j<size2;j++){// new data
			langCode = grdArray[i][1];
			langCode2 = newData[j][1];
			if(langCode == langCode2){// the lang code exist in the new data
				found = true;
			}
		}
		if(!found){								
			newData[newData.length] = new Array(grdArray[i][0],grdArray[i][1],getValue("ssrDesc"));
		}
	}
	grdArray = newData;		
	
}

function enablePopupControls(){
	$("#language").enableButton();
	$("#add").enableButton();
	$("#remove").enableButton();
	$("#translation").enableButton();
	$("#txtDescForDisplayOL").enableButton();
	$("#btnUpdate").enableButton();
	$("#btnPopupClose").enableButton();
}

function getSortOrderCount(ssrCatId){
	var orderCount = null;
	
	if(ssrCatId == SSR_CATEGORY.AIRPORT_SERVICE){
		orderCount = $("#totalAirportServiceCount").val();
	} else if(ssrCatId == SSR_CATEGORY.INFLIGHT_SERVICE){
		orderCount = $("#totalInflightServiceCount").val();
	}else if(ssrCatId == SSR_CATEGORY.AIRPORT_TRANSFER){
		orderCount = $("#totalAirportTranferCount").val();
	} 
	
	return orderCount;
}

function showHideApplicability(ssrCatId){
	if(ssrCatId == SSR_CATEGORY.AIRPORT_SERVICE){
		$("#trApplicability").show();
		$("#applTransit").show();
		$(".transit").show();
	} else if(ssrCatId == SSR_CATEGORY.AIRPORT_TRANSFER){
		$("#trApplicability").show();
		$(".transit").hide();
		$("#applTransit").attr('checked', false);
	}else if(ssrCatId == SSR_CATEGORY.INFLIGHT_SERVICE){
		$("#trApplicability").hide();
	}
}
