/*
 * Author : Dhanya
*/

var strRowData;
var strGridRow;
var intLastRec;
var screenId = "SC_ADMN_027";
var valueSeperator = "~";


function validateSSR(){
	
	if(getFieldByID("hdnMode").value == "ADD") {
		for(var i = 0; i < arrData.length; i++) {
			if(arrData[i][1].toString().toUpperCase() == getFieldByID("txtSSRCode").value.toString().toUpperCase()) {
				showCommonError("Error","SSR code already exists");
				getFieldByID("txtSSRCode").focus();
				return;
			}
		}
	}

	if(validateTextField(getValue("txtSSRCode"),ssrCodeRqrd)) {
		getFieldByID("txtSSRCode").focus();
		return;
	}
		
		
	if(validateTextField(getValue("txtSSRDesc"),ssrDescRqrd)){
		getFieldByID("txtSSRDesc").focus();
		return;
	}

		
	if(getFieldByID("selSSRCategory").value == "-1") {
		showCommonError("Error","SSR category is required");
		getFieldByID("selSSRCategory").focus();
		return;
	}
	
	if(getFieldByID("txtDefaultCharge").value == "") {
			getFieldByID("txtDefaultCharge").value = 0;
	}

	return true;
}

function searchSSR() {
	if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		//once search button was pressed set the hidden value to "SEARCH"
		//top[0].initializeVar();
		getFieldByID("frmSSR").hdnMode.value="SEARCH";
		getFieldByID("frmSSR").target="_self";
		getFieldByID("frmSSR").action = "showSsr.action"
		getFieldByID("frmSSR").submit();
	}
}	

//On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg,strMsgType) {
	clearSSR();	
	disableInputControls();	
	setPageEdited(false);

	Disable("btnEdit", "true");
	Disable("btnDelete", "true");

	setField("selSSRCat",selectedSsrCategory);
	
	if(strMsg != null && strMsgType != null)
		showCommonError(strMsgType,strMsg);
		//var arrFormData= new Array();

	if (top[1].objTMenu.focusTab == screenId){
		buttonSetFocus("btnAdd");
	}

	if(arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true);
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); //Get saved grid Row
		setField("txtSSRCode",arrFormData[0][1]);
		setField("txtSSRDesc",arrFormData[0][2]);
		setField("txtDefaultCharge",arrFormData[0][3]);	
		setField("selSSRCategory",arrFormData[0][4]);	

		Disable("btnSave", true);
		Disable("btnReset", true);
	
		if (arrFormData[0][5] == "Active" || arrFormData[0][5] =="active"
		|| arrFormData[0][5] =="on"){
			getFieldByID("chkStatus").checked = true;
			Disable("btnSave", false);
			Disable("btnReset", false);
		}
		else{
			getFieldByID("chkStatus").checked = false;	
			Disable("btnSave", false);
			Disable("btnReset", false);
		}
		setVisibilities(arrFormData[0][9]);
		
		enableInputControls();			
		
		if(!isAddMode){
			Disable("txtSSRCode", true);
			setField("hdnMode","EDIT");	
			setField("hdnVersion", arrFormData[0][6]);
			setField("hdnSSRId",arrData[0][7]);
			if (top[1].objTMenu.focusTab == screenId){
				getFieldByID("txtSSRDesc").focus();
			}
		} else {
			Disable("txtSSRCode", false);
			setField("hdnMode","ADD");
			if (top[1].objTMenu.focusTab == screenId){
				getFieldByID("txtSSRCode").focus();	
			} 
		}		
	}else{
		if(isSuccessfulSaveMessageDisplayEnabled){
			if(isSaveTransactionSuccessful){
				alert("Record Successfully saved!");				
			}
		} 	
	}
	
	
}

function disableInputControls(){
	Disable("txtSSRCode", true);
	Disable("txtSSRDesc", true);
	Disable("txtDefaultCharge",true);
	Disable("selSSRCategory",true);	
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("chkStatus", true);	
	Disable("selSSRVisibility", true);
}


//on Grid Row click
function RowClick(strRowNo){
	objOnFocus();
	if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow=strRowNo;
		
		setField("txtSSRCode",arrData[strRowNo][1]);
		setField("txtSSRDesc",arrData[strRowNo][2]);
		setField("txtDefaultCharge",arrData[strRowNo][3]);
		setField("selSSRCategory",arrData[strRowNo][4]);
				
		if (arrData[strRowNo][5]=="Active" || arrData[strRowNo][5]=="active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;	
		
		setField("hdnVersion",arrData[strRowNo][6]);
		setField("hdnSSRId",arrData[strRowNo][7]);
		
		setVisibilities(arrData[strRowNo][9]);
		
		setPageEdited(false);
		disableInputControls();

		Disable("btnSave", true);
		Disable("btnReset", true);

		Disable("btnEdit", false);
		Disable("btnDelete", false);
	}
}

function enableInputControls(){

	Disable("txtSSRCode", false);
	Disable("txtSSRDesc", false);
	Disable("txtDefaultCharge",false);
	Disable("selSSRCategory",false);
	Disable("btnSave", false);
	Disable("btnReset", false);
	Disable("chkStatus", false);
	Disable("selSSRVisibility", false);
}



function clearSSR() {
	setField("hdnVersion","");
	setField("txtSSRCode","");
	setField("txtSSRDesc","");
	setField("selSSRCategory","-1");	
	setField("txtDefaultCharge","");
	getFieldByID("chkStatus").checked = false;
	setField("hdnSSRId","");
	var vCnt = getFieldByID("selSSRVisibility").options.length;
	for(var i=0; i< vCnt; i++)
		getFieldByID("selSSRVisibility").options[i].selected = false;
}

function saveSSR() {
		enableInputControls();
		if(!validateSSR())
			return;
		setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
		Disable("txtSSRCode", false);	
		setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); //Save the grid Row
		document.forms[0].submit();
}


function resetSSR(){
	var strMode = getText("hdnMode");
	if(strMode=="ADD"){
		clearSSR();
		getFieldByID("txtSSRCode").focus();
		
	}
	if(strMode=="EDIT"){
		if (strGridRow<arrData.length && arrData[strGridRow][1] == getText("txtSSRCode")) {
			getFieldByID("txtSSRCode").focus();
			setField("txtSSRCode",arrData[strGridRow][1]);
			setField("txtSSRDesc",arrData[strGridRow][2]);
			setField("txtDefaultCharge",arrData[strGridRow][3]);
				
			if(arrData[strGridRow][4] == ""){
			setField("selSSRCategory","-1");			
			}else {	
				setField("selSSRCategory",arrData[strGridRow][4]);			
			}		
	
			if (arrData[strGridRow][5]=="Active" || arrData[strGridRow][5]=="active")
				getFieldByID("chkStatus").checked = true;
			else
				getFieldByID("chkStatus").checked = false;	
			setField("hdnVersion",arrData[strGridRow][6]);	
			setField("hdnSSRId",arrData[strGridRow][7]);
			setVisibilities(arrData[strGridRow][9]);
		} else {
			//Grid row not found.  Clear it off.
			clearSSR();	
			disableInputControls();	
			Disable("btnEdit", "true");
			Disable("btnDelete", "true");
//			getFieldByID("btnAdd").focus();
			buttonSetFocus("btnAdd");
		}
	}
	objOnFocus();
	setPageEdited(false);
}


function updateSSR() {
	if(!validateSSR())
		return;
	document.forms[0].submit();
}


//on Add Button click
function addClick(){
	
	objOnFocus();
	if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		clearSSR();
		setPageEdited(true);
		enableInputControls();
		setField("hdnMode","ADD");
		getFieldByID("chkStatus").checked = true;
		getFieldByID("txtSSRCode").focus();
		Disable("btnEdit", "true");
		Disable("btnDelete", "true");
	}
}

//on Edit Button CLick
function editClick(){
	objOnFocus();
	if(getFieldByID("txtSSRCode").value=="" || getFieldByID("txtSSRCode").value==null){
		showCommonError("Error","Please select row to edit!");
		Disable("btnDelete", "true");
	}
	else {
		Disable("btnDelete", "true");
		enableInputControls();
		setPageEdited(true);
		Disable("txtSSRCode", true);
		setField("hdnMode","EDIT");
		//Disable("txtSSRCode", true);
		getFieldByID("txtSSRDesc").focus();
	}
}

//on Delete Button click
function deleteClick(){
	objOnFocus();
	if(getFieldByID("txtSSRCode").value=="" || getFieldByID("txtSSRCode").value==null){
		showCommonError("Error","Please select row to delete!");
	}else {	
		
		var confirmStr = confirm(deleteRecoredCfrm);
		if(!confirmStr)
			return;

		enableInputControls();
		
//		setField("hdnRecNo",(top[0].intLastRec));		
		setField("hdnMode","DELETE");
		setTabValues(screenId, valueSeperator, "intLastRec", 1); //Delete
		setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
		document.forms[0].submit();
		setPageEdited(false);
		disableInputControls();
	}	
}

/**  sets the visibilities from an Array */
function setVisibilities(arrCode){
	if(getFieldByID("selSSRVisibility")) {
		var intCount = getFieldByID("selSSRVisibility").length ;
		
		for(var i=0; i< intCount; i++)
			getFieldByID("selSSRVisibility").options[i].selected = false;

		for(var vl=0; vl<arrCode.length;vl++) {		
			 for (var i = 0; i < intCount; i++){
			 	if (trim(getFieldByID("selSSRVisibility").options[i].value)==trim(arrCode[vl])){
			   	  getFieldByID("selSSRVisibility").options[i].selected=true;
			   	  break;
			   	}
				
			}	
		}		
	}	
}


// to show error messages
function RecMsgFunction(intRecNo, intErrMsg){
	top[2].HidePageMessage();
	if (intErrMsg != ""){
		top[2].objMsg.MessageText = intErrMsg ;
		top[2].objMsg.MessageType = "Error" ; 
		top[2].ShowPageMessage();
	}
}


//To Handle Paging
function gridNavigations(intRecNo, intErrMsg){
	if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		setField("hdnMode","PAGING");
		objOnFocus();
		if (intRecNo<=0)
			intRecNo=1;
		//top[0].intLastRec=intRecNo;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		//setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
		if (intErrMsg != ""){
			top[2].objMsg.MessageText = intErrMsg ;
			top[2].objMsg.MessageType = "Error" ; 
			top[2].ShowPageMessage();
		}else{
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus(){
	top[2].HidePageMessage();
}

function setPageEdited(isEdited){
	top.pageEdited=isEdited;	
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}


