var recNo = "1";
var strSearch = top[1].objTMenu.tabGetValue("SC_ADMN_010");
if (strSearch != null && strSearch != "") {
	var arrRecNo = strSearch.split("~");
	recNo = arrRecNo[1];
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "7%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Charge Code";
objCol1.headerText = "Charge<br>Code";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "20%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Description";
objCol2.headerText = "Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "5%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Group";
objCol3.headerText = "Group";
objCol3.itemAlign = "Center"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "10%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Category  Criteria";
objCol4.headerText = "Category <br> Criteria";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Category ";
objCol5.headerText = "Category";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "4%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Value";
objCol6.headerText = "Value";
objCol6.itemAlign = "center"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "6%";
objCol7.arrayIndex = 7;
objCol7.toolTip = "Apply To";
objCol7.headerText = "Apply To";
objCol7.itemAlign = "left"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "6%";
objCol8.arrayIndex = 8;
objCol8.toolTip = "No of Charges";
objCol8.headerText = "No. of <br>Charges";
objCol8.itemAlign = "right"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "5%";
objCol9.arrayIndex = 9;
objCol9.toolTip = "Refundable Y=Yes N=No";
objCol9.headerText = "Refund";
objCol9.itemAlign = "center"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "5%";
objCol10.arrayIndex = 13;
objCol10.toolTip = "Status";
objCol10.headerText = "Status";
objCol10.itemAlign = "left"

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 16;
objCol11.toolTip = "Dep/Arr Charge";
objCol11.headerText = "Dep/Arr Charge";
objCol11.itemAlign = "center"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "5%";
objCol12.arrayIndex = 18;
objCol12.toolTip = "Tax to be Applied to OND Segment";
objCol12.headerText = "OND Seg";
objCol12.itemAlign = "center"

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "6%";
objCol13.arrayIndex = 19;
objCol13.toolTip = "Journey Type";
objCol13.headerText = "Journey<br>Type";
objCol13.itemAlign = "left"

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "1%";
objCol14.arrayIndex = 38;
objCol14.toolTip = "Flight Search Day Gap(Min)";
objCol14.headerText = "Min<br>Gap";
objCol14.itemAlign = "left"
	
var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "1%";
objCol15.arrayIndex = 39;
objCol15.toolTip = "Flight Search Day Gap(Max)";
objCol15.headerText = "Max<br>Gap";
objCol15.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnMC");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol9);
objDG.addColumn(objCol8);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);
objDG.addColumn(objCol12);
objDG.addColumn(objCol13);

if(isAllowAssCookieCharges){
	objDG.addColumn(objCol14);
	objDG.addColumn(objCol15);	
}

objDG.width = "99%";
objDG.height = "108px";
objDG.headerBold = false;
objDG.rowSelect = true;
// objDG.rowMultiSelect = true;
objDG.arrGridData = arrChargeData;
objDG.seqNo = true;
objDG.seqStartNo = recNo;
objDG.seqNoWidth = "5%";
objDG.pgnumRecTotal = totalRes; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.rowOver = false;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}
