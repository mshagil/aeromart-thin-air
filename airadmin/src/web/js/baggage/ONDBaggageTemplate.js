var selectedBaggage = '';
var baggageMandatory = null;
var baggageCheck = new Array();

jQuery(document)
		.ready(
				function() { // the page is ready

					var grdArray = new Array();

					$("#divSearchPanel").decoratePanel("Search Templates");
					$("#divDispBaggageTemp").decoratePanel(
							"Add/Modify OND Baggage Template");
					$("#divResultsPanel").decoratePanel("OND Baggage Templates");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnCopy').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();
					$('#btnChgAdd').decorateButton();
					$('#btnChgEdit').decorateButton();
					$('#btnAddOND').decorateButton();

                    $('#amount').numeric({allowDecimal:true});
                    $('#seatFactor').numeric({allowDecimal:true});
                    $('#totalWeight').numeric({allowDecimal:true});
					
					$("#fromDateTmp").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', minDate: new Date(), buttonImageOnly: true});
					$("#toDateTmp").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', minDate: new Date(), buttonImageOnly: true});
					
					enableGridButtons(false);
					enbleFormButtons(false);
					$("#btnAdd").enableButton();
					
					jQuery("#listBaggageTemp")
							.jqGrid(
									{
										url : 'showONDBaggageTemplate!searchONDBaggageTemplate.action?selStatus='
												+ $("#selStatus").val(),
										datatype : "json",
										jsonReader : {
											root : "rows",
											page : "page",
											total : "total",
											records : "records",
											repeatitems : false,
											id : "0",
											userdata : "baggageMandatory"
										},
										colNames : [ '&nbsp;', 'Template Id',
												'Template Code', 'Description',
												'Status', 'baggageCharges',
												'Version', 'createdBy',
												'createdDate', 'From Date',
												'To Date',
												'ChargeLocalCurrencyCode' ],
										colModel : [
												{
													name : 'id',
													width : 40,
													jsonmap : 'id'
												},
												{
													name : 'templateId',
													width : 125,
													jsonmap : 'baggageTemplate.templateId'
												},
												{
													name : 'templateCode',
													width : 175,
													jsonmap : 'baggageTemplate.templateCode'
												},
												{
													name : 'description',
													width : 275,
													jsonmap : 'baggageTemplate.description'
												},
												{
													name : 'status',
													width : 150,
													align : "center",
													jsonmap : 'baggageTemplate.status'
												},
												{
													name : 'baggageCharge',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'version',
													jsonmap : 'baggageTemplate.version',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'createdBy',
													jsonmap : 'baggageTemplate.createdBy',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'createdDate',
													jsonmap : 'baggageTemplate.createdDate',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'fromDateTmp',
													jsonmap : 'fromDateTmp',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'toDateTmp',
													jsonmap : 'toDateTmp',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'chargeLocalCurrencyCode',
													jsonmap : 'baggageTemplate.chargeLocalCurrencyCode',
													width : 150,
													align : "center",
													hidden : true
												} ],
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										pager : jQuery('#temppager'),
										rowNum : 20,
										viewrecords : true,
										height : 175,
										width : 925,
										loadui : 'block',
										onSelectRow : function(rowid) {
											if (isPageEdited()) {
												$('#frmBaggage').clearForm();
												hideMessage();
												setPageEdited(false);
												$("#rowNo").val(rowid);
												fillForm(rowid);
												disableControls(true);
												enableSearch();
												enableGridButtons(true);
												enbleFormButtons(false);
											}
											baggageMandatory = jQuery("#listBaggageTemp").getGridParam('userdata');
										}
									}).navGrid("#temppager", {
								refresh : true,
								edit : false,
								add : false,
								del : false,
								search : false
							});

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						dataType : 'json' // 'xml', 'script', or 'json'
					// (expected server response type)

					};

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						url : 'showONDBaggageTemplate!deleteTemplate.action', // override
						// for
						// form's
						// 'action'
						// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
						// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};

					function fillForm(rowid) {
						jQuery("#listBaggageTemp").GridToForm(rowid,
								"#frmBaggage");
						if (jQuery("#listBaggageTemp").getCell(rowid, 'status') == 'ACT') {
							$("#status").attr('checked', true);
						} else {
							$("#status").attr('checked', false);
						}
						code = jQuery("#listBaggageTemp").getCell(rowid,'chargeLocalCurrencyCode');
						$("#localCurrCode").val(code);
						$("#lblLocalCurrencyCode").html(code);

						var charge = jQuery("#listBaggageTemp").getCell(rowid,
								'baggageCharge');
						var arrCharge = charge.split("~");
						var mlcharge;
						grdArray = new Array();

						for ( var i = 0; i < arrCharge.length - 1; i++) {
							grdArray[i] = new Array();
							mlcharge = arrCharge[i].split(",");
							grdArray[i]["recId"] = i + 1;
							grdArray[i]["baggage"] = mlcharge[0];
							grdArray[i]["chargeId"] = mlcharge[1];
							grdArray[i]["templateId"] = mlcharge[2];
							grdArray[i]["baggageId"] = mlcharge[3];
							grdArray[i]["bagAmount"] = mlcharge[4];
							grdArray[i]["allocPieces"] = mlcharge[5];
							grdArray[i]["bagVersion"] = mlcharge[6];
							grdArray[i]["bagStatus"] = mlcharge[7];
							grdArray[i]["cosDescription"] = mlcharge[8];
							grdArray[i]["cabinAndLogicalCC"] = mlcharge[9];
							grdArray[i]["defaultBaggage"] = mlcharge[10];
							grdArray[i]["seatFactor"] = mlcharge[11];
							grdArray[i]["totalWeight"] = mlcharge[12];
							grdArray[i]["seatFactorApplyFor"] = mlcharge[13];
						}
						populateBaggageGrid();

					}

					function validateBaggage(){
						if (trim($("#templateCode").val()) == "") {
							showCommonError("Error", tempCodeRqrd);
							return false;
						}
						
						if (trim($("#description").val()) == "") {
							showCommonError("Error", descriptionRqrd);
							return false;
						}
						
						if (trim($("#fromDateTmp").val()) == "") {
							showCommonError("Error", fromDateRqrd);
							return false;
						}
						
						if (trim($("#toDateTmp").val()) == "") {
							showCommonError("Error", toDateRqrd);
							return false;
						}
						
						if (trim($("#fromDateTmp").val()) != "") {
							if (trim($("#toDateTmp").val()) == "") {
								showCommonError("Error", ondBaggTempFromToBothExistsOrNot);
								return false;
							}
						}
						
						if (trim($("#toDateTmp").val()) != "") {
							if (trim($("#fromDateTmp").val()) == "") {
								showCommonError("Error", ondBaggTempFromToBothExistsOrNot);
								return false;
							}
						}

                        if (trim($("#templateId").val()) != "") {
                            if (!CheckDates(getCurrentDate(), $("#fromDateTmp").val())){
                                showCommonError("Error", fromDateLessThanCurrent);
                                return false;
                            }
                        }
                        if (!CheckDates($("#fromDateTmp").val(), $("#toDateTmp").val())){
                            showCommonError("Error", toDateGreaterThanFromDate);
                            return false;
                        }

                        if($("#localCurrCode").val() == null || trim($("#localCurrCode").val()) == ""){
							showCommonError("Error", localCurrencyCodeRqrd);
							return false;
						}
						
						if(!isDefaultSet()){
							showCommonError("Error",defBaggRqrd);
							return false;
						}
						if(!isDefaultBaggageActive()){
							showCommonError("Error",defBaggAct);
							return false;
						}
						return true;
					}
					
					// pre-submit callback
					function showRequest(formData, jqForm, options) {
						if (trim($("#baggageCharges").val()) == "") {
							showCommonError("Error", baggageRqrd);
							return false;
						}				

						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {						
						var objTM = top[1].objTMenu;
						var strReturn = true;
						var isONDForBaggageOpen =  false;
						if (objTM._menuTab){
							
							var blnFound = false;
							for (var x = 0 ;  x < objTM._arrTMData.length ; x++){
								if ('SC_ADMN_041' == objTM._arrTMData[x][3] ){
									isONDForBaggageOpen = true;
									break;
								}
								
								
							}
						}
						if(isONDForBaggageOpen){
							top[1].objTMenu._arrTabNeedtoReload[1] =  'SC_ADMN_041';
							top[1].objTMenu._arrTabNeedtoReload[2] =  responseText['templOption'].replace("All",'');
						}
						
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							alert(responseText['succesMsg']);
							setPageEdited(false);
							$('#frmBaggage').clearForm();
							jQuery("#listBaggageChg").clearGridData();
							jQuery("#listBaggageTemp").trigger("reloadGrid");
							enableGridButtons(false);
							enbleFormButtons(false);
							$("select#selTemplate").html(
									responseText['templOption']);

						}

					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
					$('#btnSave').click(function() {
						hideMessage();
						disableControls(false);
						if ($("#status").attr('checked')) {
							$("#status").val('ACT');
						} else {
							$("#status").val('INA');
						}
						if(validateBaggage()){
							alert("Saving OND Baggage Template will take some time.....");						

							$("#baggageCharges").val(buildChargeStr());
							$('#templateCode').attr('disabled', false);
							$('#frmBaggage').ajaxSubmit(options);
							return true;
						}						
						return false;
					});

					function buildChargeStr() {
						var chargStr = "";
						for ( var j = 0; j < grdArray.length; j++) {
							chargStr += grdArray[j]["recId"] + ",";
							chargStr += grdArray[j]["baggage"] + ",";
							chargStr += grdArray[j]["chargeId"] + ",";
							chargStr += grdArray[j]["templateId"] + ",";
							chargStr += grdArray[j]["baggageId"] + ",";
							chargStr += grdArray[j]["bagAmount"] + ",";
							chargStr += grdArray[j]["allocPieces"] + ",";
							chargStr += grdArray[j]["bagVersion"] + ",";
							chargStr += grdArray[j]["bagStatus"] + ",";
							chargStr += grdArray[j]["cabinAndLogicalCC"] + ",";
							chargStr += grdArray[j]["defaultBaggage"] +",";
							chargStr += grdArray[j]["seatFactor"] + ",";
							chargStr += grdArray[j]["totalWeight"] + ",";
							chargStr += grdArray[j]["seatFactorApplyFor"];
							chargStr += "~";
						}
						return chargStr;
					}
					
					$('#cos').change(function(){
						loadBaggageList('false',"","");
					});
					
					function loadBaggageList(condition,baggageId,baggageValue) {
						var data = {};				
						data["isGridBaggage"] = condition;
						data["selBaggageId"] = baggageId;
						data["baggageValue"] = baggageValue;
						
						if (condition == "true") {
							$("#selBaggage").attr('disabled', true); 
						} else {
							$("#selBaggage").attr('disabled', false); 
						}
						
						$("#frmBaggage").ajaxSubmit({
						dataType: 'json', 
						data:data, 
						url:"showBaggageTemplate!getBaggagesForCabinClass.action",							
						success: loadSuccess});			
					}
					
					loadSuccess = function(response){
						if(response.baggageMandatory != null){
							baggageMandatory = response.baggageMandatory;
						}
						if (response.baggageForCC != null && trim(response.baggageForCC) != "") {
							$("#selBaggage").html(response.baggageForCC);
						}else {
							alert("No Baggages for selected cabin class");
							$("#selBaggage").html("<option value=''>");
							$("#selBaggage").attr('disabled', true); 
						}
					}

					$('#btnSearch')
							.click(
									function() {
										hideMessage();
										var strUrl = "showONDBaggageTemplate!searchONDBaggageTemplate.action?selTemplate="
												+ $("#selTemplate").val()
												+ "&selStatus="
												+ $("#selStatus").val();
										jQuery("#listBaggageTemp")
												.setGridParam({
													url : strUrl
												});
										jQuery("#listBaggageTemp").setGridParam({
										    page: 1
										});
										jQuery("#listBaggageTemp").trigger(
												"reloadGrid");
										enbleFormButtons(false);
										enableGridButtons(false);
										$('#frmBaggage').clearForm();
										$("#version").val('-1');
										jQuery("#listBaggageChg")
												.clearGridData();
										grdArray = new Array();
										disableControls(true);
										enableSearch();
										$("#btnAdd").attr('disabled', false);
									});

					$('#btnAdd').click(function() {
						hideMessage();
						disableControls(false);
						$('#frmBaggage').clearForm();
						$("#version").val('-1');
						jQuery("#listBaggageChg").clearGridData();
						resetCurrencyFields();
						grdArray = new Array();
						enbleFormButtons(true);
					});

					$('#btnEdit').click(function() {
						hideMessage();
						disableControls(false);

						var baggageRow = $("#selBaggage").val();

						if (baggageRow != null) {
							$("#cos").attr('disabled', true);
							$("#selBaggage").attr('disabled', true);
						}

						$('#templateCode').attr('disabled', true);
						enbleFormButtons(true);
					});
					
					$('#btnCopy').click(function() {		
						hideMessage();
						var selectedId = $("#rowNo").val();
						if (selectedId != null && selectedId != '') {
							disableControls(false);
							$('#frmBaggage').clearForm();
						    jQuery("#listBaggageChg").clearGridData();
							grdArray = new Array();
							enbleFormButtons(true);
							fillForm(selectedId);
							resetDataOnCopy();
							setPageEdited(true);
						}
					});
					
					$('#btnReset').click(function() {
						hideMessage();

						$("#selBaggage").val('');
						$("#amount").val('');
						$("#mdefault").val('');
						$("#seatFactor").val('');
						$("#totalWeight").val('');

						selectedBaggage = '';
						if ($("#rowNo").val() == '') {
							grdArray = new Array();
							$("#templateCode").val('');
							$("#description").val('');
							$("#status").val('');

							// $('#frmBaggage').resetForm();
							jQuery("#listBaggageChg").clearGridData();
						} else {
							fillForm($("#rowNo").val());
						}

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmBaggage').ajaxSubmit(delOptions);
						return false;
					});

					$('#btnAddOND').click(function(){
						top.LoadPage("showLoadJsp!loadONDTemplate.action","SC_ADMN_041","ONDs For Baggage")
					});
					
					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("#btnClose").attr('disabled', false);
					}

					function enableGridButtons(cond) {
						if (cond) {
							$("#btnEdit").enableButton();
							$("#btnDelete").enableButton();
							$("#btnAdd").enableButton();
							$("#btnCopy").enableButton();
						} else {
							$("#btnEdit").disableButton();
							$("#btnDelete").disableButton();
							$("#btnAdd").disableButton();
							$("#btnCopy").disableButton();
						}

					}

					function enbleFormButtons(cond) {
						if (cond) {
							$("#btnReset").enableButton();
							$("#btnSave").enableButton();
						} else {
							$("#btnReset").disableButton();
							$("#btnSave").disableButton();
						}

					}

					function enableSearch() {
						$("#btnSearch").attr('disabled', false);
						$("#selTemplate").attr('disabled', false);
						$("#selStatus").attr('disabled', false);
						$("#btnAdd").enableButton();
					}

					disableControls(true);
					$("#btnAdd").attr('disabled', false);
					enableSearch();
					jQuery("#listBaggageChg")
							.jqGrid(
									{
										datatype : 'clientSide',
										// bag-name,charge-id,template-id,bag-id,amount,alloc-pieces,vers,status,cabin-class,seat-factor
										colNames : [ '&nbsp;', 'Baggage',
												'chargeId', 'templateId',
												'baggageId', 'Amount',
												'Pieces', 'version', 'Status',
												'COS', 'cabinAndLogicalCC',
												'Default', 'Seat Factor',
												'Total Weight',
												'Seat Factor Apply For' ],
										colModel : [ {
											name : 'recId',
											index : 'recId',
											width : 40,
											sorttype : 'int'
										}, {
											name : 'baggage',
											index : 'baggage',
											width : 100
										}, {
											name : 'chargeId',
											index : 'chargeId',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'templateId',
											index : 'templateId',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'baggageId',
											index : 'baggageId',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'bagAmount',
											index : 'bagAmount',
											width : 90
										}, {
											name : 'allocPieces',
											index : 'allocPieces',
											width : 90,
											hidden : true
										}, {
											name : 'bagVersion',
											index : 'bagVersion',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'bagStatus',
											index : 'bagStatus',
											width : 60
										}, {
											name : 'cosDescription',
											index : 'cosDescription',
											width : 180
										} ,{
											name:'cabinAndLogicalCC', 
											width: 200,
											align:"center",
											hidden:true
										}, {
											name : 'defaultBaggage',
											index : 'defaultBaggage',
											width : 80
										} ,{
											name : 'seatFactor',
											index : 'seatFactor',
											width : 90
										}, {
											name : 'totalWeight',
											index : 'totalWeight',
											width : 90
										}, {
											name : 'seatFactorApplyFor',
											index : 'seatFactorApplyFor',
											width : 90
										}],
										viewrecords : true,
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										loadui : 'block',
										height : 100,
										onSelectRow : function(rowid) {

											selectedBaggage = rowid;
											hideMessage();
											
											$("#selBaggage").attr('disabled',
													false);

											$("#selBaggage")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'baggageId'));
											
											var sltdbaggage = jQuery("#listBaggageChg").getCell(rowid,'baggageId')
											$("#selBaggage").val(sltdbaggage);
											loadBaggageList('true',sltdbaggage,jQuery("#listBaggageChg").getCell(rowid,'baggage'));
											
											
											$("#amount")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'bagAmount'));
											$("#cabinClassCode")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'cabinClass'));
										
											$("#seatFactor")
											.val(
													jQuery(
															"#listBaggageChg")
															.getCell(
																	rowid,
																	'seatFactor'));

                                            $("#totalWeight")
											.val(
													jQuery(
															"#listBaggageChg")
															.getCell(
																	rowid,
																	'totalWeight'));
                                            
									
											var seatFactorApplyFor = jQuery("#listBaggageChg").getCell(rowid,'seatFactorApplyFor');
											$('#seatFactorApplyFor option').each(function(i, option){ 
												var selVal = $(this).text();
												if (trim(selVal) == trim(seatFactorApplyFor)){
													$('#seatFactorApplyFor option[value$="' + $(this).val() + '"]').attr('selected', true);
												}				
											});
											
											var classOfService = jQuery("#listBaggageChg").getCell(rowid,'cabinAndLogicalCC');
								    		$('#cos option').each(function(i, option){ 
												var selVal = $(this).val();
												if (trim(selVal) == trim(classOfService)){
													$('#cos option[value$="' + selVal + '"]').attr('selected', true);
												}				
											});
											if (jQuery("#listBaggageChg").getCell(rowid, 'defaultBaggage') == 'Y') {
												$("#mdefault").attr('checked', true);
											} else {
												$("#mdefault").attr('checked', false);
											}
											
											
											$("#selBaggage").attr('disabled',
													true);
											$("#cos").attr(
													'disabled', true);
										}

									});

					$('#btnChgAdd')
							.click(
									function() {

										var selBaggText = $(
												"#selBaggage option:selected")
												.text();
										var selBaggId = $("#selBaggage").val();

										var selCCText = $(
												"#cos option:selected")
												.text();
										var selCCId = $("#cos")
												.val();

										var bagAmount = $("#amount").val();
										var seatFactor = $("#seatFactor").val();
										var totalWeight = $("#totalWeight").val();
										
										var seatFactorApplyFor =$(
												"#seatFactorApplyFor option:selected")
												.text();
										

										$("#cos").attr('disabled',
												false);
										$("#selBaggage")
												.attr('disabled', false);
										$("#mdefault").attr('disabled', false);

										if ((selCCId == null)
												|| (trim(selCCId) == "")) {
											showCommonError("Error", ccRqrd);
											return false;
										} else if ((selBaggText == null)
												|| (trim(selBaggText) == "")) {
											showCommonError("Error",
													baggageRqrd);
											return false;
										} else if ((isNaN(parseFloat(trim(bagAmount))))
												|| (trim(bagAmount) < 0)) {
											showCommonError("Error", baggageAmt);
											return false;
										} else if (!checkForBaggage()) {
											if(baggageCheck[0] == "existBaggage"){
												showCommonError("Error",
														baggageExst);
											}											
											else if(baggageCheck[0] == "defaultBaggage"){
												showCommonError("Error","Deafault Baggage Already Defined");
											}
											return false;
										} else if ((isNaN(parseFloat(trim(seatFactor))))
												|| (trim(seatFactor) < 0)
												|| (trim(seatFactor) > 100)) {
											showCommonError("Error",
													ondBaggSeatFact);
											return false;
										} else if((trim(totalWeight) == '') || (isNaN(parseFloat(trim(totalWeight)))) || (trim(totalWeight) < 0)){
											showCommonError("Error", ondBaggTotalWeight);
											return false;
										} else if(seatFactorApplyFor == null || seatFactorApplyFor ==''){
											showCommonError("Error",seatFactorAppliedForReq);
											return false;
										}else {

											setPageEdited(true);
											var msize = grdArray.length;

											var defaultBaggage = 'N';

											if ($("#mdefault").attr('checked')) {
												defaultBaggage = 'Y';
											} 

											grdArray[msize] = new Array();
											grdArray[msize]["recId"] = msize + 1;
											grdArray[msize]["baggage"] = selBaggText;
											grdArray[msize]["chargeId"] = '';
											grdArray[msize]["templateId"] = trim($(
													"#templateId").val());
											grdArray[msize]["baggageId"] = trim(selBaggId);
											grdArray[msize]["bagAmount"] = bagAmount;
											grdArray[msize]["allocPieces"] = '100';
											grdArray[msize]["bagVersion"] = -1;
											grdArray[msize]["bagStatus"] = 'ACT';
											grdArray[msize]["defaultBaggage"] = defaultBaggage;
											grdArray[msize]["cabinAndLogicalCC"] = trim($("#cos").val());
											if(trim(selCCId.split("_")[0]) == trim(selCCId.split("_")[1])){
												grdArray[msize]["cosDescription"] = trim($("#cos option:selected").text());
											}else{
												grdArray[msize]["cosDescription"] = trim($("#cos option:selected").text()).substring(2);
											}
											grdArray[msize]["seatFactor"] = seatFactor;
											grdArray[msize]["totalWeight"] = totalWeight;
											grdArray[msize]["seatFactorApplyFor"] = seatFactorApplyFor;

											jQuery("#listBaggageChg")
													.addRowData(msize + 1,
															grdArray[msize]);

								
											$("#selBaggage").val('');
											$("#amount").val('');
											$("#mdefault").val('');
											$("#seatFactor").val('');
											$("#totalWeight").val('');
											$("#seatFactorApplyFor").val('');

										}
									});

					function checkForBaggage() {
						var isDefaultBaggage = "";
						//new flow
						var cabinAndLogicalCC = $("#cos").val();
						
						var classOfService = cabinAndLogicalCC.split("_");
						if($("#mdefault").attr('checked')){
							isDefaultBaggage = "Y";
						}
						
						var baggageArr = trim($("#selBaggage").val())
								.split(",");
						for ( var i = 0; i < grdArray.length; i++) {
							if (grdArray[i]["baggageId"] == baggageArr[0] && (grdArray[i]["cabinAndLogicalCC"] == cabinAndLogicalCC)) {
								baggageCheck[0] = "existBaggage";
								return false;
							}
							if(isDefaultBaggage == "Y"){
								var grdCos = grdArray[i]["cabinAndLogicalCC"].split("_");
								if(((grdCos[0] == classOfService[0]) || (grdCos[0] == classOfService[1]) ||
										(classOfService[0] == classOfService[1] && grdCos[1] == classOfService[1])) && 
										grdArray[i]["defaultBaggage"] == isDefaultBaggage){
									baggageCheck[0] = "defaultBaggage";
									return false;
								}
							}
						}
						return true;
					}

					$('#btnChgEdit').click(
							function() {

								var bagAmount = $("#amount").val();
								var seatFactor = $("#seatFactor").val();
								var totalWeight = $("#totalWeight").val();
								var seatFactorApplyFor =$("#seatFactorApplyFor option:selected").text();

										if (selectedBaggage == null
												|| selectedBaggage == '') {
											return;
										} else if ((isNaN(parseFloat(trim(bagAmount))))
												|| (trim(bagAmount) < 0)) {
											showCommonError("Error", baggageAmt);
											return false;
										} else if (!checkForBaggage()) {
											if (baggageCheck[0] == "defaultBaggage") {
												showCommonError("Error",
														"Deafault Baggage Already Defined");
												return false;
											}
										}
										if ((isNaN(parseFloat(trim(seatFactor))))
												|| (trim(seatFactor) < 0)
												|| (trim(seatFactor) > 100)) {
											showCommonError("Error",
													ondBaggSeatFact);
											return false;
										}
										// $("#btnChgEdit").attr('disabled',
										// true);
										$("#cos").attr('disabled', false);
										var inId = jQuery("#listBaggageChg")
												.getGridParam("selrow");

								
								if (inId == null || inId == '') {
									return;
								}								

								setPageEdited(true);
								
								var defaultBaggage = 'N';

								if ($("#mdefault").attr('checked')) {
									defaultBaggage = 'Y';
								} 

								grdArray[inId - 1]["bagAmount"] = parseFloat(bagAmount);
								grdArray[inId - 1]["bagStatus"] = 'ACT';
								grdArray[inId - 1]["defaultBaggage"] = defaultBaggage;
								grdArray[inId - 1]["seatFactor"] = seatFactor;
								grdArray[inId - 1]["totalWeight"] = totalWeight;
								grdArray[inId - 1]["seatFactorApplyFor"] = seatFactorApplyFor;

								jQuery("#listBaggageChg").setRowData(inId, {
									bagAmount : parseFloat(bagAmount),
									bagStatus : 'ACT',
									defaultBaggage : defaultBaggage,
									seatFactor :(parseFloat(seatFactor)).toFixed(2),
                                    totalWeight :totalWeight,
                                    seatFactorApplyFor :seatFactorApplyFor
								});
							});

					function populateBaggageGrid() {
						jQuery("#listBaggageChg").clearGridData();
						for ( var i = 0; i < grdArray.length; i++) {
							jQuery("#listBaggageChg").addRowData(i + 1,
									grdArray[i]);
						}
					}

					function isDefaultSet(){
						for ( var i = 0; i < grdArray.length; i++) {
							if(jQuery("#listBaggageChg").getCell((i+1), 'defaultBaggage')=='Y' || (baggageMandatory != null && baggageMandatory == false)){
								return true;
							}
						}				
						return false;	
					}

					function isDefaultBaggageActive(){
						for ( var i = 0; i < grdArray.length; i++) {
							if((jQuery("#listBaggageChg").getCell((i+1), 'defaultBaggage')=='Y' && jQuery("#listBaggageChg").getCell((i+1), 'bagStatus') == 'ACT') || (baggageMandatory != null && baggageMandatory == false)){
								return true;
							}
						}				
						return false;	
					}
					
					function resetCurrencyFields(){
						if(showInLocalCurrency == 'false'){
						    $("#localCurrCode").val(baseCurrency);
							$("#lblLocalCurrencyCode").html(baseCurrency);
						}
					}
					
					function resetDataOnCopy(){
						$("#version").val('-1');
						$("#templateCode").val('');
						$("#description").val('');
						
						if (!CheckDates(getCurrentDate(), $("#fromDateTmp").val())){
							$("#fromDateTmp").val('');
						}

						if (!CheckDates(getCurrentDate(), $("#toDateTmp").val())){
							$("#toDateTmp").val('');
						}	
					}
					
					function getCurrentDate(){
						var today = new Date();
						var dtNY = today.getFullYear();
						var dtND = Number(today.getDate());
						var dtNM = (today.getMonth() + 1);
						
						if (Number(dtND) < 10){dtND =  "0" + dtND;}
						if (Number(dtNM) < 10){dtNM =  "0" + dtNM;}
						
					    var dayBefore = dtND + "/" + dtNM + "/" + dtNY;
					    return dayBefore;
					}

					

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
						hideMessage();
					}

					function isPageEdited() {
						return top.loadCheck(top[1].objTMenu
								.tabGetPageEidted(screenId));
					}
					function hideMessage() {
						top[2].HidePageMessage();
					}
					
				});

function validateAndPopulate(cntfield){
	if (!dateChk(cntfield))	{
		showERRMessage(invalidDate);		
		getFieldByID(cntfield).focus();		
		return;
	}
}