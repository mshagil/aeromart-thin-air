package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;



public class AccessAnalyzer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
	        BufferedReader in = new BufferedReader(new FileReader("c:/ibe-access.log"));
	        HashMap accessByIP = new HashMap();
//	        HashMap accessByURI = new HashMap();
	        String str;
	        String IP;
	        String URI;
	        String [] strArr;
	        while ((str = in.readLine()) != null) {
	        	if (str.startsWith("2006-09")){
	        		str = str.substring(56);
	        		strArr = str.split(":");
	        		if (strArr != null && strArr.length >= 3){
		        		if (accessByIP.containsKey(strArr[0])){
		        			HashMap uriAccess = (HashMap) accessByIP.get(strArr[0]);
		        			if (uriAccess.containsKey(strArr[1])){
		        				Integer cnt = (Integer) uriAccess.get(strArr[1]);
		        				uriAccess.put(strArr[1], new Integer(cnt.intValue() + 1));
		        				accessByIP.put(strArr[0], uriAccess);
		        			} else {
		        				uriAccess.put(strArr[1], new Integer(1));
			        			accessByIP.put(strArr[0], uriAccess);
		        			}
		        			
		        		} else {
		        			HashMap accessByURI = new HashMap();
		        			accessByURI.put(strArr[1], new Integer(1));
		        			accessByIP.put(strArr[0], accessByURI);
		        		}
	        		}
	        	}
	        }
	        in.close();
	        
	        BufferedWriter out = new BufferedWriter(new FileWriter("c:/ibe-access.csv"));
	        Iterator accessByIPIterator = accessByIP.keySet().iterator();
	        String ip;
	        while (accessByIPIterator.hasNext()){
	        	ip = (String) accessByIPIterator.next();
	        	HashMap accessByURI = (HashMap) accessByIP.get(ip);
	        	Iterator accessByURIIt = accessByURI.keySet().iterator();
	        	while (accessByURIIt.hasNext()){
	        		String uri = (String) accessByURIIt.next();
	        		out.write(ip + "," + uri + "," + (Integer)accessByURI.get(uri) + "\n");
	        		System.out.println(ip + "," + uri + "," + (Integer)accessByURI.get(uri));
	        	}
	        }
	        out.close();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }

	}

}
