<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<script type="text/javascript">
var owaParams = {};
owaParams.product_id = '<c:out value="${searchParams.fromAirport}" escapeXml="false"/>-<c:out value="${searchParams.toAirport}" escapeXml="false"/>';
owaParams.departure = '<c:out value="${searchParams.departureDate}" escapeXml="false"/>';
owaParams.return = '<c:out value="${searchParams.returnDate}" escapeXml="false"/>';
</script>
<script type="text/javascript">
var owatagCid = 3673;
(function(){function d(){b.qo33OrmjMAEReady=1}
var a=document,b=window;b.addEventListener?b.addEventListener("load",d,false):b.attachEvent?b.attachEvent("onload",d):d();
var c=a.createElement("script");c.type="text/javascript";c.async=true;c.src="http"+("https:"==a.location.protocol?"s":"")+"://ssl.hurra.com/owatag.js";
a=a.getElementsByTagName("script")[0];a.parentNode.insertBefore(c,a)})();
</script>

<!-- Added as per Airarabia request on 08/04/2013 -->
<script type="text/javascript">
	try {var __scP=(document.location.protocol=="https:")?"https://":"http://";
	var __scS=document.createElement("script");__scS.type="text/javascript";
	__scS.async=true;__scS.src=__scP+"app.salecycle.com/capture/AIRARABIA.js";
	document.getElementsByTagName("head")[0].appendChild(__scS);}catch(e){}
	//for Production the URL should be chnage in the above script -------- app.salecycle.com/capture/AIRARABIA.js -------
</script>
<!-- Added as per Airarabia request on 08/04/2013 -->

<!-- Added as per Airarabia request on 05/05/2014 -->
<!-- Netaffiliation -->
<script type="text/javascript"
	src="http://action.metaffiliation.com/trk.php?mclic=S4A1ED1011&argmon=0&argtemp=<c:out value="${sessionScope.sessionDataDTO.requestSessionIdentifier}" escapeXml="false" />"></script>
<noscript>
	<img
		src="http://action.metaffiliation.com/trk.php?mclic=N4A1ED1011&argmon=0&argtemp=<c:out value="${sessionScope.sessionDataDTO.requestSessionIdentifier}" escapeXml="false" />"
		width="1" height="1" border="0" />
</noscript>
<!-- Netaffiliation -->
<!-- Affilinet -->
<iframe
	src="http://4006456.fls.doubleclick.net/activityi;src=4006456;type=abcde123;cat=fghij456;u1=<c:out value="${sessionScope.sessionDataDTO.customerId}" escapeXml="false" />;u2=<c:out value="${sessionScope.sessionDataDTO.customerEmail}" escapeXml="false" />;ord=<c:out value="${sessionScope.sessionDataDTO.requestSessionIdentifier}" escapeXml="false" />?"
	width="1" height="1" frameborder="0" style="display: none"></iframe>
<!-- Affilinet -->

<!-- criteo -->
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript"> 
window.criteo_q = window.criteo_q || []; 
window.criteo_q.push( 
 { event: "setAccount", account: 13722}, 
 { event: "setCustomerId", id: '<c:out value="${sessionScope.sessionDataDTO.customerId}" escapeXml="false" />' }, 
 { event: "setSiteType", type: "d" }, 
 { event: "viewSearch", checkin_date: '<c:out value="${searchParams.departureDate}" escapeXml="false"/>', checkout_date: '<c:out value="${searchParams.returnDate}" escapeXml="false"/>'}, 
 { event: "viewItem", item: "Flight ID" } 
); 
</script>
<!-- criteo -->

<!-- Added as per Airarabia request on 05/05/2013 -->