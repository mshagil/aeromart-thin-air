<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
	<tr>
		<td class="stepsItem">
			<div class="<fmt:message key="msg.pg.process1"/>">
				<!--<font class="fntWhite fntBold hdnavFont">1&nbsp;</font>
				<label id="lblPrecess1" class="fntWhite fntBold ">Search Flight</label>
			--></div>
		</td>
		<td class="gap">&nbsp;</td>
		<td class="stepsItem">
			<div class="<fmt:message key="msg.pg.process2"/>">
				<!--<font class="disableNavFont fntBold hdnavFont">2&nbsp;</font>
				<label id="lblPrecess2" class="disableNavFont fntBold ">Personalize</label>
			--></div>
		</td>
		<td class="gap">&nbsp;</td>
		<td class="stepsItem">
			<div class="<fmt:message key="msg.pg.process3"/>">
				<!--<font class="disableNavFont fntBold hdnavFont">3&nbsp;</font>
				<label id="lblPrecess3" class="disableNavFont fntBold ">Print & Fly</label>
			--></div>
		</td>
	</tr>
</table>

