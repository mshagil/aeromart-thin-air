<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<div class="outerSteps" style="background:#fff;height:74px">
<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="first">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text" style="width:150px;"><font class="fntBold"><fmt:message key="msg.pg.process1"/></font></td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text" style="width:180px;"><font class="fntBold"><fmt:message key="msg.pg.process2"/></font></td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="last">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text" style="width:130px;"><font class="fntBold"><fmt:message key="msg.pg.process3"/></font></td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td class="right-corner" style="padding-top:6px;"><div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
		</tr>
	</table>
</div>

