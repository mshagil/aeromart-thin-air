<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="first">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process1.v3"/></font></td>
						<td class="process-mid iconCorrect">
							<img src="../images/spacer_no_cache.gif" border="0"/> 
						</td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process2.v3"/></font></td>
						<td class="process-mid iconCorrect">
							<img src="../images/spacer_no_cache.gif" border="0"/> 
						</td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process3.v3"/></font></td>
						<td class="process-mid iconCorrect">
							<img src="../images/spacer_no_cache.gif" border="0"/> 
						</td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td class="stepsItem selected">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="last">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text"><font class="fntBold"><fmt:message key="msg.pg.process4.v3"/></font></td>
						<td class="process-mid iconCorrect">
							<img src="../images/spacer_no_cache.gif" border="0"/> 
						</td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
