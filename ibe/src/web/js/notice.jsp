<html>
<head>
<title>Notice</title>
</head>
<body>
<p>Dear Valued Customer,</p>
<br/>
<p>In order to improve your user experience we are currently updating our system. During this time you may receive this notice due to the effects of the updating
procedure. We kindly ask that if you receive this message more than once that you kindly make your reservation through one of our call centers.</p>
<br/>
<p>We hope to resolve this matter soon and we thank you for choosing Air Arabia.</p>
<br/>
<p>Best Regards,</p>
<p>Air Arabia Management.</p>
</body>
</html>