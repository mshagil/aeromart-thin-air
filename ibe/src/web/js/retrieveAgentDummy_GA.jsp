<%@ page language="java"%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<script type="text/javascript" src="http://airarabia.isaaviations.com/ibe/js/common/Common.js"></script>
<script type="text/javascript">
	<!--

function pageBtnOnClick(){
		var carriers = "G9";

		var strPostData = "EN^" +
							"RA^" +
							getValue("selCountry") + "^" + // Country selected
							getValue("selStation") + "^" + // Station selected
							carriers + "^" + // Comma seperated list of carriers to which selected station belongs to
											// set this to "" (empty string) if carriers to which it belongs to is not known
							getValue("selAgentType"); // Agent types to show
		setField("hdnParamData", strPostData);


		setField("hdnLanguage", "en"); // Language
		var url = getValue("hdnURL");

		setField("hdnCountryCode", getValue("selCountry"));
		setField("hdnCityCode", getValue("selStation"));
		setField("hdnAgentTypes", getValue("selAgentType"));
		setField("hdnMode", "SEARCH");
		
		//var url = "https://airarabia.isaaviations.com/ibe/public/" // Action URL
		//if (carriers = "3O") {
		//	url = "https://airarabiama.isaaviations.com/ibe/public/showAgentsInfo.action"
		//}

		getFieldByID("formRetrieveAgents").action =url+"showAgentsInfo.action"; //showReservation.action";
		getFieldByID("formRetrieveAgents").submit();
}

//--------- Country list to populate agent search --------------------------//
var arrCountry = new Array();
arrCountry[0] = new Array('', '-- SELECT COUNTRY --', '');
arrCountry[1] = new Array('AE', 'UAE', 'G9|3O');
arrCountry[2] = new Array('AF','Afghanistan','G9');
arrCountry[3] = new Array('AM','Armenia','G9');
arrCountry[4] = new Array('BD','Bangladesh','G9');
arrCountry[5] = new Array('BH','Bahrain','G9');
arrCountry[6] = new Array('DJ','Djibouti','G9');
arrCountry[7] = new Array('EG','Egypt','G9');
arrCountry[8] = new Array('FR','France','G9');
arrCountry[9] = new Array('GR','Greece','G9');
arrCountry[10] = new Array('IN','India','G9');
arrCountry[11] = new Array('IR','Iran','G9');
arrCountry[12] = new Array('JO','Jordan','G9');
arrCountry[13] = new Array('KE','Kenya','G9');
arrCountry[14] = new Array('KW','Kuwait','G9');
arrCountry[15] = new Array('KZ','Kazakhstan','G9');
arrCountry[16] = new Array('LB','Lebanon','G9');
arrCountry[17] = new Array('LK','Sri Lanka','G9');
arrCountry[18] = new Array('MA','Morocco','G9');
arrCountry[19] = new Array('MV','Maldives','G9');
arrCountry[20] = new Array('MY ','Malaysia','G9');
arrCountry[21] = new Array('NP','Nepal','G9');
arrCountry[22] = new Array('OM','Oman','G9');
arrCountry[23] = new Array('PK','Pakistan','G9');
arrCountry[24] = new Array('PN','Palestine','G9');
arrCountry[25] = new Array('QA','Qatar','G9');
arrCountry[26] = new Array('RU','Russia','G9');
arrCountry[27] = new Array('SA','Saudi Arabia','G9');
arrCountry[28] = new Array('SD','Sudan','G9');
arrCountry[29] = new Array('SG ','Singapore','G9');
arrCountry[30] = new Array('SP','Spain','G9');
arrCountry[31] = new Array('SY','Syria','G9');
arrCountry[32] = new Array('TR','Turkey','G9');
arrCountry[33] = new Array('UA','Ukraine','G9');
arrCountry[34] = new Array('YE','Yemen','G9');
//----------- End of country list -----------------------------//

//----------------- Station List to populate agent search screen -------------- //
var arrStation = new Array();
arrStation[0] = new Array ('XXX ', '---------------', 'AE', 'G9');
arrStation[1] = new Array ('OLA', 'STATION FOR OFFLINE AGENTS', 'OL', 'G9|3O');
arrStation[2] = new Array ('DXB', 'Dubai', 'AE', 'G9|3O');
arrStation[3] = new Array ('UAQ', 'Umm Al Qaiwan', 'AE', 'G9|3O');
arrStation[4] = new Array ('AAN', 'Al Ain', 'AE', 'G9|3O');
arrStation[5] = new Array ('AJM', 'Ajman', 'AE', 'G9|3O');
arrStation[6] = new Array ('FJR', 'Fujairah', 'AE', 'G9|3O');
arrStation[7] = new Array ('AUH', 'Abu Dhabi', 'AE', 'G9|3O');
arrStation[8] = new Array ('SHJ', 'Sharjah', 'AE', 'G9|3O');
arrStation[9] = new Array ('RKT', 'Ras Al Khaimah', 'AE', 'G9|3O');
arrStation[10] = new Array ('KBL', 'Kabul', 'AF', 'G9');
arrStation[11] = new Array ('EVN', 'Yerevan', 'AM', 'G9');
arrStation[12] = new Array ('DAC', 'DHAKA', 'BD', 'G9');
arrStation[13] = new Array ('CGP', 'Chittagong', 'BD', 'G9');
arrStation[14] = new Array ('BAH', 'Bahrain', 'BH', 'G9');
arrStation[15] = new Array ('JIB', 'DJibouti', 'DJ', 'G9');
arrStation[16] = new Array ('SSH', 'Sharm El Sheik', 'EG', 'G9');
arrStation[17] = new Array ('CAI', 'Cairo', 'EG', 'G9');
arrStation[18] = new Array ('ATZ', 'Assiut', 'EG', 'G9|3O');
arrStation[19] = new Array ('ALY', 'Alexandria', 'EG', 'G9|3O');
arrStation[20] = new Array ('CMB', 'Colombo', 'LK', 'G9');
arrStation[21] = new Array ('CMN', 'Casablanca', 'MA', '3O');
arrStation[22] = new Array ('MLE', 'Maldives', 'MV', 'G9');
arrStation[23] = new Array ('KUL', 'Kuala Lumpur', 'MY', 'G9|3O');
arrStation[24] = new Array ('KTM', 'Nepal', 'NP', 'G9|3O');
arrStation[25] = new Array ('MCT', 'Muscat', 'OM', 'G9|3O');
arrStation[26] = new Array ('KHI', 'Karachi', 'PK', 'G9|3O');
arrStation[27] = new Array ('PEW', 'Peshawar', 'PK', 'G9|3O');
arrStation[28] = new Array ('GAZ', 'GAZA', 'PN', 'G9|3O');
arrStation[29] = new Array ('NBL', 'NABOLUS', 'PN', 'G9|3O');
arrStation[30] = new Array ('DOH', 'Doha', 'QA', 'G9|3O');
arrStation[31] = new Array ('VKO', 'Moscow', 'RU', 'G9|3O');
arrStation[32] = new Array ('MED', 'Medinah', 'SA', 'G9|3O');
arrStation[33] = new Array ('KSA', 'Saudi Arabia', 'SA', 'G9|3O');
arrStation[34] = new Array ('RUH', 'Riyadh', 'SA', 'G9|3O');
arrStation[35] = new Array ('JED', 'Jeddah', 'SA', 'G9|3O');
arrStation[36] = new Array ('DMM', 'Dammam', 'SA', 'G9|3O');
arrStation[37] = new Array ('KSC', 'KSA Call Centre', 'SA', 'G9|3O');
arrStation[38] = new Array ('KRT', 'Kartoum', 'SD', 'G9|3O');
arrStation[39] = new Array ('SIN', 'SINGAPORE', 'SG', 'G9|3O');
arrStation[40] = new Array ('BCN', 'Barcelona', 'SP', 'G9|3O');
arrStation[41] = new Array ('ALP', 'Aleppo', 'SY', 'G9|3O');
arrStation[42] = new Array ('DAM', 'Damascus', 'SY', 'G9|3O');
arrStation[43] = new Array ('LTK', 'Latakia', 'SY', 'G9|3O');
arrStation[44] = new Array ('SAW', 'Istanbul', 'TR', 'G9|3O');
arrStation[45] = new Array ('IST', 'Istanbul', 'TR ', 'G9|3O');
arrStation[46] = new Array ('KBP', 'KIEV', 'UA ', 'G9|3O');
arrStation[47] = new Array ('SAH', 'Sanaa', 'YE ', 'G9|3O');

//----------------- End of Station List -------------- //


//**************** Related to Agents search ****************//
	/** Populates Country list*/
	function buildCountryDropDown(){
		var intCount = arrCountry.length ;
		var objCountry 	 = getFieldByID("selCountry");
		for (var i = 0 ; i < intCount ; i++){
			objCountry.length =  objCountry.length + 1
			objCountry.options[objCountry.length - 1].text = arrCountry[i][1];
			objCountry.options[objCountry.length - 1].value = arrCountry[i][0];
		}
		selCountryOnChange();
	}

	/**
		Builds station drop down according to the selected country
	*/
	function buildStationDropDown(country){
		var intCount = arrStation.length ;
		var objStation 	 = getFieldByID("selStation");
		objStation.length = 0; //Reset the lenght

		objStation.length =  objStation.length + 1;
		objStation.options[objStation.length - 1].text = 'All';
		objStation.options[objStation.length - 1].value = '';

		for (var i = 0 ; i < intCount ; i++){
			if(arrStation[i][2]==country){
			objStation.length =  objStation.length + 1;
				objStation.options[objStation.length - 1].text = arrStation[i][1];
				objStation.options[objStation.length - 1].value = arrStation[i][0];
			}
		}

	}

	function selCountryOnChange(){
		var country = getValue("selCountry");
		buildStationDropDown(country);
	}

//-->
</script>
</head>
<body>


<table cellspacing="0" cellpadding="2" border="0" align="center" width="60%">
		<tbody><tr>
			<td><font class="fntBold fntEnglish  aaFontColor hdFont">Agents Information</font></td>
		</tr>

		<tr>
			<td><font class="fntEnglish"> </font></td>
		</tr>
		<tr>
			<td valign="top" style="height: 200px;">
				<form method="post" name="formRetrieveAgents" id="formRetrieveAgents">
				<input type="hidden" value="<%=AppParamUtil.getSecureIBEUrl()%>" name="hdnURL" id="hdnURL"/>
				<input type="hidden" value="" name="hdnCPMode" id="hdnCPMode"/>
				<input type="hidden" value="" name="hdnParamData" id="hdnParamData"/>
				<input type="hidden" value="" name="hdnLanguage" id="hdnLanguage"/>
				<input type="hidden" value="" name="hdnCarrier" id="hdnCarrier"/>
				<input type="hidden" id="hdnCountryCode" name="hdnCountryCode"> 
				<input type="hidden" id="hdnCityCode" name="hdnCityCode"> 
				<input type="hidden" id="hdnCarrierCode" name="hdnCarrierCode"> 
				<input type="hidden" id="hdnAgentTypes" name="hdnAgentTypes">
				<input type="hidden" id="hdnMode" name="hdnMode">

				<table cellspacing="0" cellpadding="2" border="0" width="100%">
					<tbody>

					<tr>
						<td colspan="3"><span id="spnError"></span><br/></td>
					</tr>

					<tr>
						<td width="50px"> </td>
						<td width="150px"><font class="fntBold fntEnglish">Country Code :</font></td>
						<td><select id='selCountry' size='1' style='width:175px;' NAME='selCountry' onChange='selCountryOnChange()'>
							</select><font class="mandatory"> *</font></td>
					</tr>
					<tr>
						<td> </td>
						<td><font class="fntBold fntEnglish">Station :</font></td>
						<td><select id='selStation' size='1' style='width:175px;' NAME='selStation'>
							</select><font class="mandatory">
							<font class="mandatory"> *</font></td>
					</tr>
					<tr>
						<td> </td>
						<td><font class="fntBold fntEnglish">Type :</font></td>
						<td>
							<select id='selAgentType' size='1' style='width:175px;' NAME='selAgentType'>
															<option value="1" selected="true">All</option>
															<option value="2">Sales Shop or GSA</option>
															<option value="3">Travel Agents</option>
														</select><font class="mandatory">
							<font class="mandatory"> *</font>
						</td>
					</tr>

					<tr>
						<td align="right" colspan="3"><input type="button" class="Button" onclick="pageBtnOnClick()" value="Continue" title="Continue" tabindex="24" id="btnContinue"/></td>
					</tr>

				</tbody></table>
				</form>
			</td>
		</tr>

	</tbody></table>
</body>
<script type="text/javascript">
<!--
	buildCountryDropDown();
//-->
</script>
</html>