//Created by Haider 18/8/2008

	var html = "";

	var allDays = "MoTuWeThFrSaSu";	
	var newData = new Array();
	function dateConvertToDDMM(strDate){
		if (strDate != ""){
			var strDay=strDate.substring(0,2);
			var strMonth=strDate.substring(3,5);
			var strYear=strDate.substring(6,10); 	
			strDate = strMonth + "/" + strDay + "/" + strYear;
		}
		return strDate;
	}

    function getMonthYearText(){
        var monthArray = monthText.split(",");
        var dateArray = startDate.split("/");
        return monthArray[parseInt(dateArray[0], 10)-1] + " " + dateArray[2];
    }
	
	function getFlightDuration(deptTime, arrTime, departureTimeOffset ,arrivalTimeOffset){
		var flightDuration = "";
		var delimiter = ":";
		var departureTime = deptTime.split(delimiter);
		var arrivalTime = arrTime.split(delimiter);
		if(departureTime[0].charAt(0) == "0"){
			departureTime[0] = departureTime[0].substring(1);
		}
		if(departureTime[1].charAt(0) == "0"){
			departureTime[1] = departureTime[1].substring(1);
		}if(arrivalTime[0].charAt(0) == "0"){
			arrivalTime[0] = arrivalTime[0].substring(1);
		}
		if(arrivalTime[1].charAt(0) == "0"){
			arrivalTime[1] = arrivalTime[1].substring(1);
		}
		
		//FIXME until a proper fix is done
		//detecting a +1 day arrival flight
		if(arrivalTime[0] < departureTime[0]){
			arrivalTime[0] = parseInt(arrivalTime[0])+24;
		}		
		
		var departureTime  = parseInt(departureTime[0]) * 60 + parseInt(departureTime[1]);
		var arrivalTime = (24 * (arrivalTimeOffset - departureTimeOffset) + parseInt(arrivalTime[0])) * 60 + parseInt(arrivalTime[1]);
		var diffHours = parseInt((arrivalTime - departureTime) / 60);
		var diffMinutes = (arrivalTime - departureTime) - (diffHours * 60);
		if (diffHours > 24) {
			diffHours = diffHours - 24;
		}		
		flightDuration = diffHours + delimiter + diffMinutes;
		return flightDuration;
	}

	function editSchedule(index, data){
		var days = parseDays(data[4]);
		//newData[index] = new Array(data[1], data[28],days, data[30],data[31]);
		newData[index][0] = data[1];
		newData[index][1] = data[28];
		newData[index][2] = data[30];
		newData[index][3] = data[31];
	}
	function compareDate(d1,d2){
		var date1 = new Date(d1);
		var date2 = new Date(d2);
		if(date1 < date2 ) return -1;
		if(date1.toString() == date2.toString()) return 0;
		if(date1 > date2) return 1;
	}
	function getMinDate(d1,d2){
		if(compareDate(d1,d2) < 0)
			return d1;
		else
			return d2;
	}
	function getMaxDate(d1,d2){
		if(compareDate(d1,d2) < 0)
			return d2;
		else
			return d1;
	}
	function isDatesOverlap(d1s,d1e,d2s,d2e){
		if(compareDate(d2s,d1s)>=0 && compareDate(d2s,d1e)<=0)
			return true;
		if(compareDate(d2e,d1s)>=0 && compareDate(d2e,d1e)<=0)
			return true;
		return false;
		
	}
	function isNextDay(d1s,d1e,d2s,d2e){
		var date1s = new Date(d1s);
		var date2s = new Date(d2s);
		var date1e = new Date(d1e);
		var date2e = new Date(d2e);
		var a = date2e.getDate()+1;
		var b = date1s.getDate();
		var c = date1e.getDate()+1;
		var d = date2s.getDate();
		if(a == b || c == d)
			return true;
		else
			return false;
	}
	function addSchedule(data){
		var len = newData.length;
		//var days = parseDays(data[4]);
		var arrTime = getArrTime(data, data[15]);
							//	0FlightNo, 1depTime, 2arrTime, 3Freq, 4From,   5 To,	6	
		newData[len] = new Array(data[1], data[28],arrTime, data[4], data[30],data[31], data);
		return len;
	}
	function nextDay(d){
		var dt = new Date(d);
		dt.setDate(dt.getDate()+1);
		var day = dt.getDate();
		var mon = dt.getMonth()+1;
		var year = dt.getFullYear();
		if(day.length <2) day = "0"+day;
		if(mon.length <2) mon = "0"+mon;
		return mon+"/"+day+"/"+year;
	}
	
	function getUniqueSchedule(){
		var specialFlt = false;
		var p = 0;
		var arrData;
        var html = "";
		if (sheTitle!=""){
			html+='<br /><table class="'+pgDir+'" align="center" width="100%"><tr class="recordd thinBorderB thinBorderL thinBorderR"><td colspan="6" align="'+align+'" class="align header">'+sheTitle+'</td></tr></table>';
		}
		if(mainData == null || mainData.length == 0){
			html+= noFlightHTML;
		}

        var prev = 0;
		var headerName = '';
		for(p=0;p<mainData.length;p++){
                     if (prev == 0 || prev!=mainData[p][3]) {
			html += tableTagHeader;
                        html += row2+'@header'+'</td></tr>';
			html += '<tr><td>';
			prev = mainData[p][3];
			headerName = "<div style=\"display:none\"><font style=\"color:#f00\">Route:&nbsp;</font>"+mainData[p][1]+" - "+mainData[p][2]+"</div>";
		     }else {
			   headerName += " - " +mainData[p][2];
		     }
			newData = new Array();
			arrData = mainData[p][0];
			var len = arrData.length;
			var i=0,j=0,k=1;
			var selStartDate = new Date(startDate);
			var selStopDate = new Date(stopDate);
	
			for(i=0;i<len;i++){
				if(compareDate(arrData[i][30],selStartDate)<0)
					arrData[i][30] = startDate;
				if(compareDate(arrData[i][31],selStopDate)>0)
					arrData[i][31] = stopDate;
			}
			
			addSchedule(arrData[0]);
			for(i=1;i<len;i++){
				var bMatch = false;
				var depTime1 = arrData[i][28];
				var arrTime1 = getArrTime(arrData[i], arrData[i][15]);
				var flightNo1 = arrData[i][1];
				for(j=0;j<newData.length;j++){
					var depTime2=newData[j][1];
					var arrTime2 = newData[j][2];//arrData[arr[j]][32][0][5];
					var flightNo2 = newData[j][0];
					if(depTime1==depTime2 && arrTime1 == arrTime2 && flightNo1==flightNo2){
							var sd1 = arrData[i][30];
							var ed1 = arrData[i][31];
							var sd2 = newData[j][4];
							var ed2 = newData[j][5];
							//check if the two schedule's date are overlaped
							if(isDatesOverlap(sd1, ed1, sd2, ed2) ){//1
								bMatch = true;
								if(newData[j][3] == arrData[i][4]){//if the freq is same for both schedule
									newData[j][4] = getMinDate(sd1, sd2);
									newData[j][5] = getMaxDate(ed1, ed2);
									break
								}else{
									if((compareDate(sd1,sd2) == 0 && compareDate(ed1,ed2) == 0) ||compareDate(sd1,ed1)==0 || compareDate(sd2,ed2) == 0){ //two schedule with same dates
										newData[j][3] = merageFreq(newData[j][3], arrData[i][4]);
										newData[j][4] = getMinDate(sd1, sd2);
										newData[j][5] = getMaxDate(ed1, ed2);
										break;
									}else{
										if(compareDate(sd1,sd2) == 0){
											if(compareDate(ed1,ed2) < 0){
												var n = addSchedule(arrData[i]);
												newData[n][3] =  merageFreq(newData[j][3], arrData[i][4]);
												newData[j][4] = nextDay(ed1);
											}else{
												var n = addSchedule(newData[j][6]);
												newData[n][3] =  merageFreq(newData[j][3], arrData[i][4]);
												newData[j][4] = nextDay	(ed2);
												newData[j][5] = ed1;
												newData[j][3] = arrData[i][4];
												newData[j][6] = arrData[i];
											}	
										}else
										if(compareDate(sd1,sd2)<0){//2nd sch. start date less then 1st one
											if(compareDate(ed2,ed1)<0){//1st sch. date inside the second one
												addSchedule(newData[j][6]);
												newData[j][3] = merageFreq(newData[j][3], arrData[i][4]);
											}else{
												var n = addSchedule(arrData[i]);
												newData[n][5] = ed2; 
												n = addSchedule(arrData[i]);
												newData[n][4] = sd2;
												newData[n][3] =  merageFreq(newData[j][3], arrData[i][4]);
												newData[j][4] = ed1;
											}
										}else{
											if(compareDate(sd2,sd1)<0){//1nd sch. start date less then 2st one
												if(compareDate(ed1,ed2)<0){//2st sch. date inside the 1st one
													addSchedule(arrData[i]);
													newData[j][3] = merageFreq(newData[j][3], arrData[i][4]);
												}else{
													var n = addSchedule(newData[j][6]);
													newData[n][5] = sd1; 
													if(compareDate(ed1,ed2) == 0){
														newData[j][3] =  merageFreq(newData[j][3], arrData[i][4]);
														newData[j][4] = sd1;
													}else{
														n = addSchedule(arrData[i]);
														newData[n][4] = ed2;
														newData[j][3] =  merageFreq(newData[j][3], arrData[i][4]);
														newData[j][4] = sd1;
													}
												}
											}										
										}
									}
									
								}
								
								
								break;
							}//1
						}
				}//for j
				if(!bMatch){
					addSchedule(arrData[i]);
				}
					
			}//for i
			//pass2
			var newData2 = new Array();
			newData2[0] = newData[0];
			for(i=1;i<newData.length;i++){
				bMatch = false;
				for(j=0;j<newData2.length;j++){
					var sd1 = newData[i][4];
					var ed1 = newData[i][5];
					var sd2 = newData2[j][4];
					var ed2 = newData2[j][5];
					if(newData[i][0] == newData2[j][0] && newData[i][1] == newData2[j][1] &&
						newData[i][2] == newData2[j][2] && newData[i][3] == newData2[j][3] && isNextDay(sd1,ed1,sd2,ed2)){
							bMatch = true;	
							newData2[j][4] = getMinDate(newData[i][4], newData2[j][4]);
							newData2[j][5] = getMaxDate(newData[i][5], newData2[j][5]);
							break;
						}
				}
				if(!bMatch)
					newData2[newData2.length] = newData[i];
			}	
			newData = newData2;
			var s;
			
			html += tableTag;
                        //alert(html);
			//html += row1+mainData[p][1]+" - "+mainData[p][2]+'</td></tr>';
			html += row+mainData[p][1]+" - "+mainData[p][2]+'</td></tr><tr class="spacerGapRow" style="display:none"><td colspan="10" class="spacerGap"><br/></td></tr>';
			html +=table;
			var dateSp1= "<label class='date-disp'>";
			var dateSp2= "</label><label class='date-hid' style='display:none'>";
			for(i=0;i<newData.length;i++){
				var star = "";
				s = newData[i];
				var newFreq = getValidFrequencyWithinDate(s[3], s[4], s[5]);
				if (newFreq != null && newFreq.length>0){					
					if(isSpecialFlt(s)){
						star = "*";
						specialFlt = true;
					}
					html += tr[i%2]+td1+s[0]+star+td2;
					html += td1+s[1]+td2;
					html += td1+s[2]+td2;
					html += td1+getFrequency(parseDays(newFreq))+td2;
					if(compareDate(s[4], s[5])>0)
						s[5] = s[4]; 
					html += td1+ dateSp1 + dateConvertToDDMM(s[4]) + dateSp2 +  dateConvertToDDMM(s[4]) +"</label>"+td2;
					html += td1+ dateSp1 + dateConvertToDDMM(s[5]) + dateSp2 +  dateConvertToDDMM(s[5]) +"</label>"+td2;
					
					if(displayAdvancedScheduleDetails == "true"){						
						html += td1+s[6][13]+td2;						
						var departureTimeOffset = s[6][32][0][4];
						var arrivalTimeOffset = s[6][32][s[6][32].length - 1][6];
						//html += td1+ getFlightDuration(s[1],s[2],departureTimeOffset,arrivalTimeOffset) +td2;
						html += td1+ getFlightDuration(s[6][40][0][3],s[6][40][0][5],departureTimeOffset,arrivalTimeOffset) +td2;
					}
					html += tr2;
				}
			}//for i
                        
                        //alert('curr:' +mainData[p][3] + '' + (p+1));
			//alert(' curr:' +mainData[p][3] +  'len:' +mainData.length + ' p:' + p + ' 33333:::' + mainData[3][3]);
			if ( mainData.length == (p+1) || mainData[p+1][3] != mainData[p][3]){			
				html += "</table>"+"</td></tr></table>";
                                html = html.replace("@header",headerName);
                               // alert('headerName :'+headerName);
			} else
				html += "</table>";
		}
		
		if(specialFlt){
				html+='<br /><table width="100%"><tr class="recordd"><td colspan="6" align="'+align+'" class="noborder">'+specialNote+'</td></tr></table>';
		}
		
                ///alert('html\n\n'+html);
		DivWrite('scheduleTbl', html);
		dateChangerjobforLanguage(lang);
		$("body").attr("dir", pgDir.toLowerCase());
	}
	function isSpecialFlt(data){
		if(specialFltFromDate.length == 0 || specialFltToDate.length==0 || specialFltNo.length==0 || specialFltDest.length == 0)
			return false;
		var i;
		var n;
		var fltNo = specialFltNo.split(",");
		if(compareDate(data[4],specialFltFromDate)>=0 && compareDate(data[5],specialFltToDate)<=0 && (data[6][14]==specialFltDest || data[6][15]==specialFltDest)){
			for(i=0;i<fltNo.length;i++){
				n = "G9"+fltNo[i];
				if(n == data[0])
					return true;	
			}
		}
		return false;
	}
	function merageFreq(src, dst){
		var i;
		var newFreq = "";
		var day1,day2;
		for(i=0;i<14;i+=2){
			day1 = src.substr(i,2);
			day2 = dst.substr(i,2);
			if(day1 != '__') newFreq += day1;
			else if(day2 != '__') newFreq += day2;
			else newFreq += '__';
		}
		return newFreq;
	}
	function parseDays(s){
		if(s == allDays) return "Daily";
		var len = s.length;
		var i;
		var day;
		var days="";
		for(i=0;i<len;i+=2){
			day = s.substr(i,2)
			if(day == '__') continue;
			if(days.length > 0) days+=",";
			days += day;
		}
		return days;
	}
	function getFrequency(freq){
		if(freq == "Daily")
			return daily;
		var days = freq.split(",");
		var arDays = new Array();
		var arFreq="";
		var i,j=0;
		for(i=0; i<days.length; i++, j++){
			if(days[i] == "Sa")	arDays[j] = sa; else 
			if(days[i] == "Su")	arDays[j] = su; else
			if(days[i] == "Mo")	arDays[j] = mo; else
			if(days[i] == "Tu")	arDays[j] = tu; else
			if(days[i] == "We")	arDays[j] = we; else
			if(days[i] == "Th")	arDays[j] = th; else
			if(days[i] == "Fr")	arDays[j] = fr; 
		}
		for(i=0;i<arDays.length;i++){
			arFreq += arDays[i];
			if(i<arDays.length-1)
				arFreq +=comma;
		}
		return arFreq;
	}

	// Write to Divs / span
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	function getArrTime(data, dst){
		var len = data[32].length;
		if(len == 1)
			return data[32][0][5];
		var i;
		for(i=0;i<len;i++){
			if(data[32][i][2] == dst)
				return data[32][i][5]
		}
	}
	function getValidFrequencyWithinDate(freq, date1,date2){
		if(freq == allDays) return freq;
		var d1 = new Date(date1);
		var d2 = new Date(date2);
		var diff = (d2.getTime()-d1.getTime())/(1000*60*60*24)+1;
		if(diff>7)
			return freq;
		var days = new Array();
		var newFreq = "";
		var len = freq.length;
		var i;
		for(j=1,i=0;i<len;i+=2,j++){
			day = freq.substr(i,2)
			days[j] = day;
		}
		days[0] = days[7];
		//alert(days);
		var sunday = "" ;
		for(i=0;i<diff;i++){
			day = d1.getDay();
			if(day == 0)
				sunday = days[day];
			else{	
				if(days[day] != "__")
					newFreq += days[day];
				}
			d1.setDate(d1.getDate()+1);			
		}
		if(sunday != "__")
			newFreq += sunday;
		return newFreq;
		//alert(newFreq)						
	} 				