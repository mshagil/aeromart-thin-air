function UI_ExitPopup() {
};

;
(function(UI_ExitPopup) {

	UI_ExitPopup.isActive = false;
	UI_ExitPopup.exitStep = 2;
	UI_ExitPopup.emailTxt='';
	UI_ExitPopup.disable = false;
	UI_ExitPopup.exitPopupContent;
	UI_ExitPopup.isCustomerLoggedIn = false;
	UI_ExitPopup.maxPopupTimes=0;
	UI_ExitPopup.popupDisplayedTimes = 0;
	UI_ExitPopup.triggerTimeoutFunction = false;
	
	$(document).ready(function() {
		//based on the app parameter enable or disable exitpopup
		if(UI_Top.holder().GLOBALS.exitPopupEnable){
			appendToLog('Exit pop up is enabled by app parameter');
			UI_ExitPopup.ready();
		}
	});

	UI_ExitPopup.ready = function() {
		$(document).on('mousemove', function(e) {
			var isPageLoading = UI_ExitPopup.pageIsLoading();
			var scrolled = window.pageYOffset|document.body.scrollTop; //document.body.scrollTop uses for IE8
			if (e.pageY < (10 + scrolled) && !(UI_ExitPopup.isActive) && !(isPageLoading) ) {
				appendToLog('Detect mouse movment towards top level of the browser');
				UI_ExitPopup.enableExitPopup();
				if(!(UI_ExitPopup.disable)){
					appendToLog('exit pop is enabled');
					setTimeout(UI_ExitPopup.loadPopupContent,Number(UI_Top.holder().GLOBALS.exitPopupDisplayTimeGap));
					UI_ExitPopup.triggerTimeoutFunction = true;
				}
			}
		});
	}

	UI_ExitPopup.loadPopupContent = function(){
		UI_ExitPopup.isActive = true;
		UI_ExitPopup.triggerTimeoutFunction = false;
		if(typeof UI_ExitPopup.exitPopupContent != 'undefined'){
			UI_ExitPopup.openPopup();
		}else{
			var data = {};
			$.ajax({type: "POST", dataType: 'json',data: data , url: 'exitPopupContent.action', async:false,		
				success: UI_ExitPopup.setExitPopupLables, error:UI_ExitPopup.onError, complete:function(){}});
		}
		return false;
		
	}
	UI_ExitPopup.setExitPopupLables = function(response) {
		if (response.success) {			
			UI_ExitPopup.exitPopupContent = response.exitPopupContent;
			if(response.customerLoggedIn){
				UI_ExitPopup.isCustomerLoggedIn = true;
			}
			UI_ExitPopup.maxPopupTimes = response.maxPopupTimes;
			UI_ExitPopup.openPopup();
		} 
		
	}
	UI_ExitPopup.openPopup = function() {
		//prevent popup if the number of displayed times equal to the max display times parameter
		var exceedNuOfPopupTimes = Number(UI_ExitPopup.popupDisplayedTimes) >= Number(UI_ExitPopup.maxPopupTimes);
		var isDisplayedPopup = getExitPopupDisplayStatusFromSession();
		if(!exceedNuOfPopupTimes && !isDisplayedPopup){
			
			appendToLog('Displaying exit popup');
			$("#newPopItem").openMyPopUp({
				width:550,
				height:300,
				topPoint: ($(window).height()/2)-((300+70)/2),
				headerHTML:"<label class='fntBold'></label>",
				bodyHTML: function(){
					return getExitPopupHTMLText(UI_ExitPopup.exitPopupContent);
				},
				footerHTML: function(){
					var passHTML = "";
					return passHTML;
				}
			});
			
			$("#exitPopup").find(".isabutton").decoButton("redContinue");
			$(".modifySearchBtn").unbind("click").bind("click",UI_ExitPopup.modifySearch);
			$(".saveFlightSearchWithEmailBtn").unbind("click").bind("click",UI_ExitPopup.saveFlightSerach);
			$(".bestFaresBtn").unbind("click").bind("click",UI_ExitPopup.showBestOffers);
			$(".saveFlightSearchBtn").unbind("click").bind("click",UI_ExitPopup.saveFlightSerach);
			$(".errorInfoBtn").unbind("click").bind("click",UI_ExitPopup.openSurveyPage);
			$(".close").unbind("click").bind("click",function(){
				$("#newPopItem").closeMyPopUp();
				UI_ExitPopup.isActive = false;  
			});
			//increment displayed times by one
			UI_ExitPopup.popupDisplayedTimes = UI_ExitPopup.popupDisplayedTimes + 1;
			updateExitPopupSessionStatus();
			
		}else{
			return;
		}
	}
	
	UI_ExitPopup.openSurveyPage = function() {
		$("#newPopItem").closeMyPopUp();
		UI_ExitPopup.isActive = false;  
		window.open(UI_ExitPopup.exitPopupContent.surveyUrl);
	}
	
	UI_ExitPopup.modifySearch = function() {
		$("#newPopItem").closeMyPopUp();
		UI_ExitPopup.isActive = false;  
		if($("#btnModSeach").is(":visible") ){
			$("#btnModSeach").trigger('click');
		}	
	}
	UI_ExitPopup.showBestOffers = function(){
		$("#newPopItem").closeMyPopUp();
		UI_ExitPopup.isActive = false;  
		window.open(UI_ExitPopup.exitPopupContent.bestFaresUrl);
	}
	
	UI_ExitPopup.saveFlightSerach = function(){
		UI_ExitPopup.clearErrorLables();		
		//exit pop will show a text input for  email if the pax has not entered the email address
		if( !UI_ExitPopup.isCustomerLoggedIn && !(UI_ExitPopup.isEmailEnteredInPaxInfoPage()) && !($("#emailField").is(":visible"))){
			$('#emailField').show();
			$('#paxEmail').focus();
			return;
		}
		var data = UI_ExitPopup.populateFlightSearchDetails();
		data = $.extend(data,UI_ExitPopup.getContactDetails());

		var paxEmail = UI_ExitPopup.getPaxEmailAddress();
		//validate email and display an error message if it is invalid
		if(!UI_ExitPopup.isCustomerLoggedIn && !(UI_ExitPopup.isValidEmail(paxEmail))){
			$("#emailErrorField").show();
			$('#paxEmail').focus();
			return;
		}
		
		var url = 'exitPopup!sendSearchDetailsEmail.action';
		$.ajax({type: "POST", dataType: 'json',data: data , url:url, async:false,		
		success: UI_ExitPopup.onSuccess, error:UI_ExitPopup.onError, complete:function(){}});
	}
		
	UI_ExitPopup.onSuccess = function(){
		$("#newPopItem").closeMyPopUp();
		UI_ExitPopup.isActive = false;  
	}
	
	UI_ExitPopup.onError = function(){
		
	}
			
	UI_ExitPopup.populateFlightSearchDetails = function(){
		var obj = {};	   
		obj['searchParams.fromAirport'] = $("#resFromAirport").val();
		obj['searchParams.toAirport'] = $("#resToAirport").val();
		obj['searchParams.departureDate'] = $("#resDepartureDate").val();
		obj['searchParams.returnDate'] = $("#resReturnDate").val();
		obj['searchParams.adultCount'] = $("#resAdultCount").val();
		obj['searchParams.infantCount'] = $("#resInfantCount").val();
		obj['searchParams.childCount'] = $("#resChildCount").val();
		obj['searchParams.selectedCurrency'] = $("#resSelectedCurrency").val();
		obj['searchParams.promoCode'] = $("#resPromoCode").val();
		  
		obj['exitStep']= UI_ExitPopup.getExitStep();
		obj['loggedInUserName'] = $("#lblTxtName").val();
		
		var isReturn = $("#resReturnFlag").val();
		if (isReturn == "true" || isReturn == true) {
			obj['searchParams.returnFlag'] = true;
		} else {
			obj['searchParams.returnFlag'] = false;
		}
		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && typeof airports != 'undefined' ) {
				var airportsLength = airports.length;
				var selectedLang = UI_ExitPopup.exitPopupContent.selectedLang;
				for(var i=0;i<airportsLength;i++){ 
					 if($("#resFromAirport").val() == airports[i].code){
						 obj['searchParams.fromAirportName'] = airports[i][selectedLang];
					 }else if($("#resToAirport").val() == airports[i].code ){
						 obj['searchParams.toAirportName'] = airports[i][selectedLang]; 
					 }
				}
		}
		
		return obj;
	}
	
	UI_ExitPopup.validateEmail = function(){
		if(!(UI_ExitPopup.isisEmailEntered())){
			return false;
		}
	}
	
	UI_ExitPopup.isEmailEnteredInPaxInfoPage = function(){
		//check pax email in the second step (load container)
		if($("#txtEmail").is(":visible") ){
			var emailText = $('#txtEmail').val();
			if(emailText == ""){
				return false;
			}
			return true;
		}
		return false;
	}

	UI_ExitPopup.isValidEmail = function(email){
		var re = new RegExp("^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,7}$");
		var ve = re.exec(email);
		if (ve == null) {return false;}else{return true}
	}
	
	//txtEmail is visible on 2nd step
	UI_ExitPopup.getExitStep = function(){
		if($("#txtEmail").is(":visible") ){
			return 2;
		}
		return 1;
	}
	
	UI_ExitPopup.getUserNameIfLoggedIn = function(){
		return $('#lblTxtName').val();
	}
	
	
	//return pax paxinfo page or from exit popup
	UI_ExitPopup.getPaxEmailAddress  = function(){
		if(UI_ExitPopup.isEmailEnteredInPaxInfoPage()){
			return $('#txtEmail').val();
		}
		return $('#paxEmail').val();
	}
	
	//clear error messages on exit popup
	UI_ExitPopup.clearErrorLables  = function(){
		$('#emailInvalid').remove();
	}
	
	UI_ExitPopup.pageIsLoading = function(){
		//frmLoadMsg is closed once page is fully loaded
		return $("#frmLoadMsg").is(":visible");
	}
	
	UI_ExitPopup.enableExitPopup = function(){
		var exitPopupEnableStep = UI_Top.holder().GLOBALS.exitPopupEnableStep;
		var isInSecondStep = $("#passengerMainTable").is(":visible");
		var isInFirstStep = $(".AllFlight").is(":visible");
		var pnr = $("#resPNR").val();
		var isModifySegment;
		if(typeof pnr == 'undefined'){
			isModifySegment = false;
		}else{
			isModifySegment = true;
		}
		
		if( !(isInFirstStep || isInSecondStep ) || isModifySegment ){
			UI_ExitPopup.disable = true;
			appendToLog('exit pop up is disabled. Either out of first and second page or in modify segment stage');
		}else if((exitPopupEnableStep > 0) && (exitPopupEnableStep == 2) && isInFirstStep){
			UI_ExitPopup.disable = true;
		}else if((exitPopupEnableStep > 0) && (exitPopupEnableStep == 1) && isInSecondStep ){
			UI_ExitPopup.disable = true;
		}
	}
	
	UI_ExitPopup.enableExitpopupExplicitly = function(){
		UI_ExitPopup.disable = false;
	}
	

	function getExitPopupHTMLText(exitPopupContent) {
		
		var exitPopupHtml =	
			'<div id="exitPopup" class="exitPopup" >'
			+'<table style="width: 97%;height:180px;margin:10px auto">'
			+	'<tr><td colspan="2"><label id="exitpopupDialogTitle" class="fntBold">' + exitPopupContent.dialogTitle + '</label></td></tr>'
			+	'<tr id="modifySearch">'
			+		'<td><label for="modifySearchlabel" id="modifySearchlabel">'+ exitPopupContent.modifySearch +'</label></td>'
			+		'<td><button id="modifySearchBtn" class="modifySearchBtn isabutton" >' + exitPopupContent.modifySearchBtn +'</button></td>'
			+	'</tr>'
			+	'<tr id="bestFares">'
			+		'<td><label for="bestFareslabel" id="bestFareslabel">' +exitPopupContent.otherFares +'</label></td>'
			+		'<td><button id="bestFaresBtn" class="bestFaresBtn isabutton" >'+exitPopupContent.otherFaresBtn+'</button></td>'
			+   '</tr>'
			+	'<tr id="saveFlightSearch">'
			+		'<td><label for="saveFlightSearchlabel" id="saveFlightSearchlabel">' +exitPopupContent.sendSearchDetails +'</label></td>'
			+		'<td><button id="saveFlightSearchBtn" class="saveFlightSearchBtn isabutton" >' +exitPopupContent.sendSearchDetailsBtn +'</button></td>'
			+	'</tr>'			
			+	'<tr id="emailField" style="display: none">'
			+		'<td>'
			+			'<label for="emaillabel" id="emaillabel">' + exitPopupContent.paxEmailLabel+'</label>'
			+		'</td>'
			+		'<td>'
			+			'<table>'
			+				'<tr>'			
			+					'<td>'
			+						'<input type="text" id="paxEmail" style="width:125px" maxlength="100" />'
			+					'</td>'
			+					'<td id="emailErrorField" style="display: none"><label class="mandatory" >' + exitPopupContent.paxEmailInvaid +'</label></td>'
			+					'<td>'
			+						'<button id="saveFlightSearchWithEmailBtn" class="saveFlightSearchWithEmailBtn isabutton" >' +exitPopupContent.sendSearchWithEmail + '</button>'
			+					'</td>'
			+				'</tr>'
			+			'</table>'
			+  		'</td>'
			+	'</tr>'
			+	'<tr id="errorInfo">'
			+		'<td><label for="errorInfolabel" id="errorInfolabel">' +exitPopupContent.errorOnPage +'</label></td>'
			+		'<td><button id="errorInfoBtn" class="errorInfoBtn isabutton" >' +exitPopupContent.errorOnPageBtn +'</button></td>'
			+	'</tr>'
			+'</table>'
			+'</div>';
		return exitPopupHtml;
	}
	
	function appendToLog(logMsg){
		if (typeof console != "undefined" && typeof console.log != "undefined") {
			console.log(logMsg);
		}
	}
	
	function getExitPopupDisplayStatusFromSession() {
	    var displayStatus;
	    var url = 'exitPopup!lookupExitPopupDisplayStatus.action';
	    $.ajax({
	        type: "GET",
	        url: url,
	        async: false,
	        success : function(data) {
	        	displayStatus = data.exitPopupDisplayed;
	        }
	    });
	    return displayStatus;
	}
	
	function updateExitPopupSessionStatus() {
	    var url = 'exitPopup!updateExitPopupDisplayStatus.action';
	    $.ajax({
	        type: "GET",
	        url: url,
	        async: false,
	        success : function(data) {
	        }
	    });
	}
	
	UI_ExitPopup.getContactDetails = function(){
		var contactDetails = {};		
		contactDetails['contactInfo.title'] = $("#selTitle :selected").text();
		contactDetails['contactInfo.firstName'] = $("#txtFName").val();
		contactDetails['contactInfo.lastName'] = $("#txtLName").val();
		contactDetails['contactInfo.nationality'] = $("#selNationality :selected").text();
		contactDetails['contactInfo.country'] = $("#selCountry :selected").text();
		contactDetails['contactInfo.preferredLangauge'] = $("#selPrefLang :selected").text();
		contactDetails['contactInfo.mCountry'] = $("#txtMCountry").val();
		contactDetails['contactInfo.mNumber'] = $("#txtMobile").val();		
		contactDetails['contactInfo.lCountry'] = $("#txtPCountry").val();
		contactDetails['contactInfo.lNumber'] = $("#txtPhone").val();
		contactDetails['contactInfo.emailAddress'] = UI_ExitPopup.getPaxEmailAddress();					
		return contactDetails;
	}
	
})(UI_ExitPopup);