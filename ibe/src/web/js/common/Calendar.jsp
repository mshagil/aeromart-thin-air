<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Calendar</title>
		<LINK rel="stylesheet" type="text/css" href="../../css/CalendarStyle_no_cache.css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">		
			<script  type="text/javascript">
<!--
	/*
		Page Description : Calendar Control 
	*/
	var strinDate = ""; //opener.dateField;
	var clBgColor ="Silver";
	clBgColor = '<fmt:message key="msg.res.calendar.bordercolor"/>';	

	
	var arrMonth = new Array() //["January","February","March","April","May","June","July","August","September","October","November","December"];
	arrMonth[0] = new Array("January","?????");
	arrMonth[1] = new Array("February","??????");
	arrMonth[2] = new Array("March","????");
	arrMonth[3] = new Array("April","?????");
	arrMonth[4] = new Array("May","????");
	arrMonth[5] = new Array("June","?????");
	arrMonth[6] = new Array("July","?????");
	arrMonth[7] = new Array("August","?????");
	arrMonth[8] = new Array("September","??????");
	arrMonth[9] = new Array("October","??????");
	arrMonth[10] = new Array("November","??????");
	arrMonth[11] = new Array("December","??????");
	
	var arrWeeks = new Array();
	arrWeeks[0]	= new Array("Su","?","Sunday","?????");
	arrWeeks[1]	= new Array("Mo","?","Monday","??????");
	arrWeeks[2]	= new Array("Tu","?","Tuesday","?????????");
	arrWeeks[3]	= new Array("We","?","Wednesday","????????");
	arrWeeks[4]	= new Array("Th","?","Thursday","??????");
	arrWeeks[5]	= new Array("Fr","?","Friday","??????");
	arrWeeks[6]	= new Array("Sa","?","Saturday","??????");
							
	
	var arrDays = new Array(42);
	var now		= new Date();
	var day		= now.getDate();
	var month	= now.getMonth();
	var year	= now.getFullYear();
	// -----------------------------------------------
	var intSYear= year			// Years starting from
	var intNYear= 2				// Display no of years in the drop down 
	var intInDD = "";
	var intInMM = "";
	var intInYY = ""; 
	var objParent = "";
	var blnFirst = "";
	var intDisableUpto = "";
	var intDisableFrom = "";
	var strMonthStyle = 0 ;
	var strYRange = "RANGE";
	
	function selMonth_onChange(){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value

		strSelMonth = Number(strSelMonth) - 1;
		writeDiv("<b><font class='DefMonth'>" + arrMonth[strSelMonth][strMonthStyle] + "<\/font><\/b>", "divMonth");
		
		var objselYear = document.getElementById("selYear");
		var strSelYear = objselYear.options[objselYear.selectedIndex].value
		
		BuildCal(strSelMonth, strSelYear);
	}
	
	function SetDate(strDD, strMM, strYY, strYears, strObject, strDisableUpto, strTextStyle, strDisableFrom, strYearRange){
		blnFirst = true;
		strYRange = strYearRange;
		intSYear= year + 1;	
		objParent = strObject;
		strinDate = strDD + "/" + strMM + "/" + strYY + "/" + strYears;
		intNYear = strYears;
		strMonthStyle = strTextStyle;
		buildMonthsDropDown();
		
		if (strDisableUpto != "" && strDisableUpto !=  null){
			var arrDUpto = strDisableUpto.split("/");
			if (Number(arrDUpto[0]) < 10){arrDUpto[0] = "0" + Number(arrDUpto[0])}
			arrDUpto[1] = Number(arrDUpto[1]) - 1;
			if (Number(arrDUpto[1]) < 10){arrDUpto[1] = "0" + Number(arrDUpto[1])}
			intDisableUpto = arrDUpto[2] + arrDUpto[1] + arrDUpto[0];
			intSYear = arrDUpto[2];
		}
		
		if (strDisableFrom != "" && strDisableFrom !=  null){
			var arrDUpto = strDisableFrom.split("/");
			if (Number(arrDUpto[0]) < 10){arrDUpto[0] = "0" + Number(arrDUpto[0])}
			arrDUpto[1] = Number(arrDUpto[1]) - 1;
			if (Number(arrDUpto[1]) < 10){arrDUpto[1] = "0" + Number(arrDUpto[1])}
			intDisableFrom = arrDUpto[2] + arrDUpto[1] + arrDUpto[0];
		}

		
		var strPM = "";
		var strNM = "";
		switch (strMonthStyle){
			case 0 :
				strPM = "Previous Month";
				strNM = "Next Month";
				break;
			case 1: 
				strPM = " ?????? ???????";
				strNM = " ?????? ??????";
				break;
		}
		writeDiv('<a href="#" onclick="PrevClick(); return false;" title="' + strPM + '"><font class="NavMonth"><b>«<\/b><\/font><\/a>',"spnPMonth");
		writeDiv('<a href="#" onclick="NextClick(); return false;" title="' + strNM + '"><font class="NavMonth"><b>»<\/b><\/font><\/a>',"spnNMonth");				

		SetCurrDate();
	}
	
	function SetCurrDate(){
		if (strinDate != ""){
			var arrInDate = strinDate.split("/");
			intInDD = Number(arrInDate[0]);
			intInMM = Number(arrInDate[1]) - 1;
			intInYY = Number(arrInDate[2]);
			if (arrInDate.length > 3){
				//intNYear = arrInDate[3] * 2;
				if (intDisableUpto == "" || intDisableUpto == null){
					intSYear = intSYear - Number(arrInDate[3]);
				}
			}
		}

		var strYearData = '<select id="selYear" name="selYear" size="1" style="width:50px" class="clsDD" onchange="selMonth_onChange()">';
		var intYearBegin = intSYear;
		switch (strYRange){
			case "RANGE" :
				for (var i = 1; i <= (intNYear * 2); i ++){
					switch (strMonthStyle){
						case 0 : strYearData += '<option value="' + intYearBegin + '">' + intYearBegin + '<\/option>'; break;
						case 1 : strYearData += '<option value="' + intYearBegin + '">' + numberConvertToArabic(intYearBegin) + '<\/option>'; break;
					}
					intYearBegin++;
				}
				break;
			case "FIXED" :
				for (var i = 1; i <= intNYear; i ++){
					switch (strMonthStyle){
						case 0 : strYearData += '<option value="' + intYearBegin + '">' + intYearBegin + '<\/option>'; break;
						case 1 : strYearData += '<option value="' + intYearBegin + '">' + numberConvertToArabic(intYearBegin) + '<\/option>'; break;
					}
					intYearBegin++;
				}
				break;				
		}
		strYearData += '<\/select>';
		writeDiv(strYearData, "spnYear")
		var objselYear = document.getElementById("selYear");
		blnFirst = false;

		var objselMonth = document.getElementById("selMonth");
		objselMonth.options[month].selected = true;
		
		writeDiv("<b><font class='DefMonth'>" + arrMonth[month][strMonthStyle] + "<\/font><\/b>", "divMonth");
		switch (strMonthStyle){
			case 0 : writeDiv("<a href='javascript:void(0);' onclick='CDateClick(); return false;' title='Today'><b><font class='ToDate'>" + day + " " + arrMonth[month][strMonthStyle] + " " + year + "<\/font><\/b><\/a>", "spnCD"); break;
			case 1 : writeDiv("<a href='javascript:void(0);' onclick='CDateClick(); return false;' title='?????'><b><font class='ToDate'>" + numberConvertToArabic(year) + " " + arrMonth[month][strMonthStyle] + " " + numberConvertToArabic(day) + "<\/font><\/b><\/a>", "spnCD"); break;
		}
		
		BuildCal(month, year);
		if (strinDate != ""){
			objselMonth.options[intInMM].selected = true;
			for (var i = 0; i < objselYear.length; i++){
				if (objselYear.options[i].value == intInYY){
					objselYear.options[i].selected = true;
					break;
				}
			}
			selMonth_onChange();
		}
		
	}
	
	function BuildCal(strMonth, strYear){
	
		// Array Initialize 
		for (var i = 0; i <= 42; i++){
			arrDays[i] = "";
		}
		
		var intDays        = getDaysInMonth(strMonth + 1, strYear);
		var dtfirstDay     = new Date (strYear, strMonth, 1);
		var intArrStartPos = dtfirstDay.getDay();
		
		// Get preivous month Days 
		var intPMonth	= strMonth;
		var intPYear	= strYear;
		if (intPMonth <= 0){
			intPMonth = 12
			intPYear  = Number(intPYear) - 1
		}
		var intPDays    = getDaysInMonth(intPMonth, intPYear);
		var intPSDate   = (intPDays - intArrStartPos + 1)
		
		var intDCount = 1 
		var intNDCount = 1 
		for (var i = 0; i <= 42; i++){
			if (i >= intArrStartPos){
				if (intDCount <= intDays){
					arrDays[i] = intDCount; 
					intDCount++;
				}else{
					arrDays[i] = intNDCount; 
					intNDCount++;
				}
			}else{
				arrDays[i] = intPSDate
				intPSDate++;
			}
		}
		
		var intTDWidth = 26
		var strHTMLText  = "<table width='" + (intTDWidth * 7) + " cellpadding='0' cellspacing='2' border='0' class='BGColor' align='center'>"
		strHTMLText		+= "	<tr class='TblLineColor'>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='SUHDBGColor' style='height:25px;'><font class='SUHDColor' title='" + arrWeeks[0][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[0][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='MOHDBGColor'><font class='MOHDColor' title='" + arrWeeks[1][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[1][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='TUHDBGColor'><font class='TUHDColor' title='" + arrWeeks[2][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[2][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='WEHDBGColor'><font class='WEHDColor' title='" + arrWeeks[3][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[3][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='THHDBGColor'><font class='THHDColor' title='" + arrWeeks[4][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[4][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='FRHDBGColor'><font class='FRHDColor' title='" + arrWeeks[5][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[5][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='SAHDBGColor'><font class='SAHDColor' title='" + arrWeeks[6][Number(strMonthStyle) + 2] + "'><b>" + arrWeeks[6][strMonthStyle] + "<\/b><\/font><\/td>"
		strHTMLText		+= "	<\/tr>"
		
		var intCount = 0 
		var strLinkTag = "";
		var intDCount = 1 
		var strDisplayDay = "";
		for (var i = 0; i < 6; i++){
			strHTMLText		+= "	<tr class='TblLineColor'>"
			for (var x = 0; x < 7; x++){
				strHTMLText		+= " <td width='" + intTDWidth + "' align='center' " 
				switch (strMonthStyle){
					case 0 : strDisplayDay  = arrDays[intCount]; break;
					case 1 : strDisplayDay  = numberConvertToArabic(arrDays[intCount]); break;
				}

				var strDisableUpto = strYear;
				var strDisableMonth = strMonth
				var strDisableDay = arrDays[intCount] ;
				
				if (Number(strDisableMonth) < 10){strDisableMonth = "0" + strDisableMonth;}
				if (Number(strDisableDay) < 10){strDisableDay = "0" + strDisableDay;}

				strDisableUpto = strDisableUpto + String(strDisableMonth) + String(strDisableDay);

				if (intCount >= intArrStartPos){
					if (intDCount <= intDays){
						//strLinkTag = "<a href='#' onclick='Day_onClick(" + arrDays[intCount] + ")'>";
						strLinkTag = "<input type='button' class='button' value='" + arrDays[intCount] + "' onclick='Day_onClick(" + arrDays[intCount] + ")'>"
						
						if (intDisableUpto != "" && intDisableUpto != null && strDisableUpto < intDisableUpto){
							strHTMLText += "class='DisableBGUpto'><font class='DisableUpto'>" + strDisplayDay + "<\/font><\/td>";
						}else{
							if (intDisableFrom != "" && intDisableFrom != null && strDisableUpto > intDisableFrom){
								strHTMLText += "class='DisableBGUpto'><font class='DisableUpto'>" + strDisplayDay + "<\/font><\/td>";
							}else{
								// Check is it current date 
								if (strMonth == intInMM && strYear == intInYY && arrDays[intCount] == intInDD){
									//strHTMLText += "class='SelDateBGColor'>" + strLinkTag + "<font class='SelDate'><b>" + strDisplayDay + "<\/b>";
									strHTMLText += "class='SelDateBGColor'>" +  "<input type='button' class='SelectedButton' value='" + arrDays[intCount] + "' onclick='Day_onClick(" + arrDays[intCount] + ")'>" //+ "<font class='SelDate'><b>" + arrDays[intCount] + "<\/b>";
								}else{
									if (strMonth == month && strYear == year && arrDays[intCount] == day){
										//strHTMLText += "class='ToDateBGColor'>" + strLinkTag + "<font class='ToDate'><b>" + strDisplayDay + "<\/b>";
										strHTMLText += "class='ToDateBGColor'>" + "<input type='button' class='SelectedButton' value='" + arrDays[intCount] + "' onclick='Day_onClick(" + arrDays[intCount] + ")'>"  //+ "<font class='ToDate'><b>" + arrDays[intCount] + "<\/b>";
									}else{
										switch (x) {
											case 0 : strHTMLText += "class='SUBGColor'>" + strLinkTag ; break;
											case 1 : strHTMLText += "class='MOBGColor'>" + strLinkTag ; break;
											case 2 : strHTMLText += "class='TUBGColor'>" + strLinkTag ; break;
											case 3 : strHTMLText += "class='WEBGColor'>" + strLinkTag ; break;
											case 4 : strHTMLText += "class='THBGColor'>" + strLinkTag ; break;
											case 5 : strHTMLText += "class='FRBGColor'>" + strLinkTag ; break;
											case 6 : strHTMLText += "class='SABGColor'>" + strLinkTag ; break;
										}	
									}
								}
							}
						}
						strHTMLText	+= "<\/font><\/td>"
						intDCount++;
					}else{
						strHTMLText += "class='DisableDaysBGColor'><font class='DisableDays'>" + strDisplayDay + "<\/font><\/td>";
					}
				}else{
					strHTMLText += "class='DisableDaysBGColor'><font class='DisableDays'>" + strDisplayDay + "<\/font><\/td>";
				}
				intCount++
			}
			strHTMLText		+= "	<\/tr>"
		}
		
		strHTMLText		+= "<\/table>"
		writeDiv(strHTMLText, "divCal");
	}
	
	function Day_onClick(strDay){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value
		
		var objselYear = document.getElementById("selYear");
		var strSelYear = objselYear.options[objselYear.selectedIndex].value
		
		if (strDay < 10){strDay = "0" + strDay}
		if (strSelMonth < 10){strSelMonth = "0" + strSelMonth}
		//alert(strDay + "\n" + strSelMonth + "\n" + strSelYear + "\n" + objParent)
		parent.DateReturn(strDay, strSelMonth, strSelYear, objParent); 
		strinDate = strDay + "/" + strSelMonth + "/" + strSelYear;
		SetCurrDate();
		//window.close();
	}
	
	function PrevClick(){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value;
		strSelMonth = Number(strSelMonth) - 1; 
		var blnYear = true; 
		if (strSelMonth == 0){
			strSelMonth = 12;
			blnYear = false;
			var objselYear = document.getElementById("selYear");
			var strSelYear = objselYear.options[objselYear.selectedIndex].value;
			strSelYear = Number(strSelYear) - 1
			for (var i = 0; i < objselYear.length; i++){
				if (objselYear.options[i].value == strSelYear){
					objselYear.options[i].selected = true;
					blnYear = true; 
					break;
				}
			}
		}
		
		if (blnYear){
			strSelMonth = Number(strSelMonth) - 1; 
			objselMonth.options[strSelMonth].selected = true;
			selMonth_onChange();
		}
	}
	
	function NextClick(){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value;
		strSelMonth = Number(strSelMonth) + 1; 
		var blnYear = true; 
		if (strSelMonth == 13){
			strSelMonth = 1;
			
			blnYear = false;
			var objselYear = document.getElementById("selYear");
			var strSelYear = objselYear.options[objselYear.selectedIndex].value;
			strSelYear = Number(strSelYear) + 1
			for (var i = 0; i < objselYear.length; i++){
				if (objselYear.options[i].value == strSelYear){
					objselYear.options[i].selected = true;
					blnYear = true; 
					break;
				}
			}
		}
		if (blnYear){
			strSelMonth = Number(strSelMonth) - 1; 
			objselMonth.options[strSelMonth].selected = true;
			selMonth_onChange();
		}
	}
	
	function CDateClick(){
		var objselMonth = document.getElementById("selMonth");
		objselMonth.options[month].selected = true;
		
		var objselYear = document.getElementById("selYear");
		for (var i = 0; i < objselYear.length; i++){
			if (objselYear.options[i].value == year){
				objselYear.options[i].selected = true; 
				break;
			}
		}
		selMonth_onChange();
	}
	
	// GET NUMBER OF DAYS IN MONTH
	function getDaysInMonth(month,year)  {
		var days;
		if (month==1 || month==3 || month==5 || month==7 || month==8 ||
			month==10 || month==12)  days=31;
		else if (month==4 || month==6 || month==9 || month==11) days=30;
		else if (month==2)  {
			if (isLeapYear(year)) {
				days=29;
			}
			else {
				days=28;
			}
		}
		return (days);
	}


	// CHECK TO SEE IF YEAR IS A LEAP YEAR
	function isLeapYear (Year) {
		if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) {
			return (true);
		}
		else {
			return (false);
		}
	}

	// ----------------------
	function writeDiv(text,id){
		if (document.getElementById)
		{	x = document.getElementById(id);
			x.innerHTML = "";
			x.innerHTML = text;
		}
		else if (document.all)
		{
			x = document.all[id];
			x.innerHTML = text;
		}
		else if (document.layers)
		{
			x = document.layers[id];
			x.document.open();
			x.document.write(text);
			x.document.close();
		}
	}
	
	function numberConvertToArabic(intNumber){
		var arrNumbers = new Array("&#1632;","&#1633;","&#1634;","&#1635;","&#1636;","&#1637;","&#1638;","&#1639;","&#1640;","&#1641;")
		intNumber = String(intNumber);
		var intLength = intNumber.length;
		var strReturn = "" ;
		for (var i = 0 ; i < intLength ; i++){
			if (intNumber.substr(i,1) != " "){
				if (!isNaN(intNumber.substr(i,1))){
					strReturn += arrNumbers[intNumber.substr(i,1)];
				}else{
					strReturn += intNumber.substr(i,1);
				}
			}else{
				strReturn += intNumber.substr(i,1);
			}
		}
		
		return strReturn;
	}	

	function buildMonthsDropDown(){
		var objDL = document.getElementById("selMonth");
		objDL.length = 0 ; 
		for (var i = 0 ; i < 12 ; i++){
			objDL.length =  objDL.length + 1
			objDL.options[objDL.length - 1].text =  arrMonth[i][strMonthStyle];
			objDL.options[objDL.length - 1].value = (i+1);
		}	
	}	
//-->
			</script>
	</head>
	<body oncontextmenu='return false' ondrag='return false' class='context'>
	<table width="182" border="0" cellpadding="0" cellspacing="0" ID="Table4">
			<tr>
				<td width="14" height="14"><img src="../../images/NI016_no_cache.gif"></td>
				<td height="14" style="background-image:url(../../images/NI017_no_cache.gif);"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
				<td width="14" height="14"><img src="../../images/NI018_no_cache.gif"></td>
			</tr>
			<tr>
				<td width="14" height="14" style="background-image:url(../../images/NI019_no_cache.gif);"></td>
				<td height="14">

						<table width="100%" cellpadding="0" cellspacing="3" border="0" align="center" class='BGColor'>
							<tr class="TblLineColor">
								<td width="50" valign="middle" style="height:20px;">
									<select id="selMonth" name="selMonth" size="1" class="clsDD" style="width:50px" onchange="selMonth_onChange()">
										<option value="1">January</option>
										<option value="2">February</option>
										<option value="3">March</option>
										<option value="4">April</option>
										<option value="5">May</option>
										<option value="6">June</option>
										<option value="7">July</option>
										<option value="8">August</option>
										<option value="9">September</option>
										<option value="10">October</option>
										<option value="11">November</option>
										<option value="12">December</option>
									</select>
								</td>
								<td align="center" valign="middle" class="ToDateBGColor">
									<div id="divMonth"></div>
								</td>
								<td width="50" valign="middle" align="right">
									<span id="spnYear"></span>
									<!-- 
									<select id="selYear" name="selYear" size="1" class="clsDD" onchange="selMonth_onChange()">
										<option value='2005'>2005</option>
									</select>
									-->
								</td>
							</tr>
						</table>
						<div id="divCal"></div>
						<table width="100%" cellpadding="0" cellspacing="3" border="1" align="center"
							ID="Table1" class='BGColor'>
							<tr>
								<td width="32" height='19' align='center' class='NavMonthBGColor'><span id='spnPMonth'></span></td>
								<td class='ToDateBGColor' colspan="5" align="center"><span id="spnCD" class="ToDate"></span></td>
								<td width="32" align='center' class='NavMonthBGColor'><span id='spnNMonth'></span></td>
							</tr>
						</table>

				</td>
				<td width="14" height="14"  style="background-image:url(../../images/NI020_no_cache.gif);"></td>
			</tr>
			<tr>
				<td width="14" height="14"><img src="../../images/NI021_no_cache.gif"></td>
				<td height="14" style="background-image:url(../../images/NI022_no_cache.gif);"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
				<td width="14" height="14"><img src="../../images/NI023_no_cache.gif"></td>
			</tr>
		</table>
		<script  type="text/javascript">
	<!--
		SetCurrDate();
		var blnCalendarLoaded = true;
	//-->
		</script>
	</body>
</html>
