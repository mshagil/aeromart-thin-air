
	function pnrLinkClick(){
		var strPNR = "";
		var strCaller = '';

		if (arguments.length > 0){
			strPNR = arguments[0];
			
			if (arguments.length > 1){
				strCaller = arguments[1];
			}
		}else{
			strPNR = trim(getValue("txtPNR"));
		}
		
		if (strPageID == ""){
			if (strPNR == ""){
				alert(top[0].strPNRErrorMsg);
				getFieldByID("txtPNR").focus();
			}else{
				top[0].setField("hdnPNR", strPNR);
				top[0].strCaller =  strCaller;
				top[0].strPNRNo = strPNR;
				top[0].LoadingProgress(); 
				top[0].pnrSubmit();
			}
		}else{
		}
	}