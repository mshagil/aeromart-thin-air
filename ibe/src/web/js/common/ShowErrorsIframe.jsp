<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript">
</script>
</head>
<body bgcolor="#ECECEC">
<div id="divMsgDisplay" style="width:100%;position:absolute;color:red;font-family:verdana;font-size:11px;">
</div>
<script type="text/javascript">
<!--
function WriteMsg(strMessage, strFontColor){
	if (strMessage != null){
		DivWrite("divMsgDisplay", strMessage);
		var objControl = document.getElementById("divMsgDisplay")
		parent.ReSizeMsgBox(Number(objControl.clientHeight) + 30);
		objControl.style.color = strFontColor;
	}
}
//-->
</script>
</body>
</html>