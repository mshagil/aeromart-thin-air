/**
 * Functionalities
 * 1. Set Common parameters(nonSecurePath,securePath,homeURL,isCustomer,locale)
 * 2. Load Top banner according to locale
 * 3. Load Css file according to locale
 * 4. Set page direction(left/right)
 * 5. Load Home Page(Banner and Home link click)
 * 
 * Author : Pradeep Karunanayake
 */
function SYS_IBECommonParam(){}

SYS_IBECommonParam.nonSecurePath = "";
SYS_IBECommonParam.securePath = "";
SYS_IBECommonParam.homeURL = "";
SYS_IBECommonParam.regCustomer = false;
SYS_IBECommonParam.locale = "en";


$(document).ready(function(){
	SYS_IBECommonParam.ready();
});

/**
 * Page Ready
 */
SYS_IBECommonParam.ready = function() {
	// Get data from top frame for First page loading	
	if ($("#nonSecurePath").val() == "") {
		SYS_IBECommonParam.setPageDataFromTop();
	}
	SYS_IBECommonParam.getCommonParam();	
	SYS_IBECommonParam.loadCssFile({language:SYS_IBECommonParam.locale});
	SYS_IBECommonParam.setPageDirection({language:SYS_IBECommonParam.locale});
	SYS_IBECommonParam.loadFlashBanner({language:SYS_IBECommonParam.locale});
	$(document).on("click","#lnkHomeBanner",function(){ SYS_IBECommonParam.homeClick();});
	$(document).on("click","#linkHome",function(){ SYS_IBECommonParam.homeClick();});
	if (SYS_IBECommonParam.getPageID() != "SESSION_PAGE") {
		$("BODY").append('<div id="popup_container" style="display:none"><div class="drager"></div>'+
				'<iframe id="popup_container_frame" name="popup_container_frame" src="AlertMsg" '+
				'frameborder="0" scrolling="no" style="position:relative;top:-25px"></iframe></div>');
	}
}

SYS_IBECommonParam.getPageID = function() {
	return $("#pageID").val();
}
/**
 * Set Params from top 
 */
SYS_IBECommonParam.setPageDataFromTop = function() {
	//var arrParamsAA	= top.strReqParam.split('^');
	//$("#locale").val(arrParamsAA[0].toLowerCase());
	//$("#nonSecurePath").val(top.GLOBALS.nonsecureIBEUrl);
	//$("#securePath").val(top.GLOBALS.secureIBEUrl);
	//$("#homeURL").val(top.GLOBALS.airLineURL);
	//$("#kioskAccessPoint").val(top.GLOBALS.kioskAccessPoint);
	//To do : Set Common parameters
	if ($("#regCustomer").val() == "")
		$("#regCustomer").val(false);
}
/**
 * Populate parameters
 */
SYS_IBECommonParam.getCommonParam = function() {
	SYS_IBECommonParam.nonSecurePath = $("#nonSecurePath").val();
	SYS_IBECommonParam.securePath = $("#securePath").val();
	SYS_IBECommonParam.homeURL = $("#homeURL").val();
	SYS_IBECommonParam.regCustomer = $("#regCustomer").val();
	SYS_IBECommonParam.locale = $("#locale").val();
}

/**
 * Update Register Customer State 
 */
SYS_IBECommonParam.setCustomerState = function(blnStatus) {
	SYS_IBECommonParam.regCustomer =  blnStatus;
	$("#regCustomer").val(blnStatus);	
}


/**
 * Update Common Params
 */
SYS_IBECommonParam.updateCommonParam =  function(data) {
	$("#locale").val(data.locale);
	$("#nonSecurePath").val(data.nonSecurePath);
	$("#securePath").val(data.securePath);
	$("#homeURL").val(data.homeURL);
	//To do : Set Common parameters
	$("#regCustomer").val(data.regCustomer);
	
	SYS_IBECommonParam.getCommonParam();
}

/**
 *  Load flash banner
 */
SYS_IBECommonParam.loadFlashBanner = function(data) {
	var language = "";
	if (data.language != null && data.language != "") {
		language = data.language;
	} else {
		language = "en";
	}		
	/*$('#flashObj').flash({
	    src: 'http://www.airarabia.com/userfiles/header_banner/' + language + '/sharjah.swf',
	    width: 491,
	    height: 72
	});*/
}

/**
 * Load page css file
 */
SYS_IBECommonParam.loadCssFile = function(data) {	
	var language = data.language;
	var ts = "?ts="+new Date().getTime();
	var href = "../css/myStyle_no_cache.css"+ts;
	if (language == 'ar' || language == 'fa' || language == 'da' || language == 'pa') 
		href = "../css/myStyle_" + data.language + "_no_cache.css"+ts;
	
	$("head").append("<link>");
    css = $("head").children(":last");
    css.attr({
      rel:  "stylesheet",
      type: "text/css",
      href: href
    });	
}
/**
 * Page Direction Right/Left
 */
SYS_IBECommonParam.setPageDirection = function(data) {	
	var language = data.language;
	if(language == 'ar' || language == 'fa' || language == 'da' || language == 'pa') {
	   // Set body dir attribute
	   //$('#direction').attr("dir", "rtl");
		$('html').attr("dir", "rtl");
		$('html').attr("xml:lang","ar");
		$('html').attr("lang","ar");
		$('html').attr("xml:lang","ar");
		$('html').attr("lang","ar");
		// To Do : Remove Following code
	   $("#rightPanel").attr("align", "left");
	}
}
/**
 * Close Child Windows
 */
SYS_IBECommonParam.closeChildWindows = function(){
	// To Do : Close Child windows
}
/**
 * Home click
 */
SYS_IBECommonParam.homeClick = function(){
	SYS_IBECommonParam.loadProgressMsg();
	SYS_IBECommonParam.closeChildWindows();
	SYS_IBECommonParam.returnOnLoad();	
}
/**
 * Banner Home Link Click
 */
SYS_IBECommonParam.returnOnLoad = function() { 	
	var isKioskAccessPoint = false;
	if ($("#kioskAccessPoint").val() == "true") {
		isKioskAccessPoint = true;
	}
	var cusLoginStatus = SYS_IBECommonParam.regCustomer; 
	if ( cusLoginStatus != null && (cusLoginStatus == "true" || cusLoginStatus == true )) {		
		var formHtml =  "<form id='dynamicForm' method='post'> <input type='hidden' name='hdnParamData' value="+ SYS_IBECommonParam.locale +"^ASI></form>";
		$("body").append(formHtml);
		//$("#dynamicForm").attr("action", SYS_IBECommonParam.securePath + "showReservation.action");
		$("#dynamicForm").attr("action", SYS_IBECommonParam.securePath + "showCustomerLoadPage!loadCustomerHomePage.action");
		
		if(fromSocialSites != undefined && fromSocialSites != "" && fromSocialSites && fromSocialSites =='true'){
			$("#dynamicForm").attr("target", "_self");
		}else{
			$("#dynamicForm").attr("target", "_top");
		}
		$("#dynamicForm").submit();		
	} else if(isKioskAccessPoint == true) {
		var formHtml =  "<form id='dynamicForm' method='post'> <input type='hidden' name='hdnParamData' value='EN^KOS'></form>";
		$("body").append(formHtml);
		$("#dynamicForm").attr("action", SYS_IBECommonParam.securePath + "showReservation.action");
		$("#dynamicForm").attr("target", "_top");	
		$("#dynamicForm").submit();	
     }else {		
    	 UI_Top.holder().location.replace(SYS_IBECommonParam.homeURL);
	}
}

SYS_IBECommonParam.loadProgressMsg = function() {
	if ($("#divLoadMsg").length > 0) {
		$("#divLoadMsg").show();
	} else {
		try {
			if (parent.header !== undefined) {
				UI_commonSystem.loadingProgress();
			}
		} catch(ex) {
			if ($("#divContLoadMsg").length > 0) {
				$("#divContLoadMsg").show();
			}
		}
	}
}
