// jQuery Alert Dialogs Plugin
//
// Version 1.1
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 14 May 2009
//
// Visit http://abeautifulsite.net/notebook/87 for more information
//
// Usage:
//		jAlert( message, [title, callback] )
//		jConfirm( message, [title, callback] )
//		jPrompt( message, [value, title, callback] )
// 
// History:
//
//		1.00 - Released (29 December 2008)
//
//		1.01 - Fixed bug where unbinding would destroy all resize events
//
// License:
// 
// This plugin is dual-licensed under the GNU General Public License and the MIT License and
// is copyright 2008 A Beautiful Site, LLC. 


// ********* Modified and changed to fixed to IE6 ************* 
//********** Baladewa Welathanthri ************************

(function($) {
	
	$.alerts = {
		
		// These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time
		
		verticalOffset: -75,                // vertical offset of the dialog from center screen, in pixels
		horizontalOffset: 0,                // horizontal offset of the dialog from center screen, in pixels/
		repositionOnResize: true,           // re-centers the dialog on window resize
		overlayOpacity: .01,                // transparency level of overlay
		overlayColor: '#FFF',               // base color of overlay
		draggable: true,                    // make the dialogs draggable (requires UI Draggables plugin)
		okButton: '&nbsp;OK&nbsp;',         // text for the OK button
		cancelButton: '&nbsp;Cancel&nbsp;', // text for the Cancel button
		dialogClass: null,                  // if specified, this class will be applied to all dialogs
		
		// Public methods
		
		alert: function(message, title, callback, okLabel, cancelLabel) {
			if( title == null ) title = 'Alert';
			$.alerts._show(title, message, null, 'alert', function(result) {
				if( callback ) callback(result);
			}, okLabel, cancelLabel);
		},
		
		confirm: function(message, title, callback, okLabel, cancelLabel) {
			if( title == null ) title = 'Confirm';
			$.alerts._show(title, message, null, 'confirm', function(result) {
				if( callback ) callback(result);
			}, okLabel, cancelLabel);
		},
			
		prompt: function(message, value, title, callback, okLabel, cancelLabel) {
			if( title == null ) title = 'Prompt';
			$.alerts._show(title, message, value, 'prompt', function(result) {
				if( callback ) callback(result);
			}, okLabel, cancelLabel);
		},
		
		// Private methods
		
		_show: function(title, msg, value, type, callback, okLabel, cancelLabel) {
			
			$.alerts._hide();
			$.alerts._overlay('show');
			var _okLabel = (okLabel == undefined || okLabel == null) ? $.alerts.okButton : okLabel;
			var _cancelLabel = (cancelLabel == undefined || cancelLabel == null) ? $.alerts.cancelButton : cancelLabel;
			
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			$("#popup_container").css({
				position: pos,
				zIndex: 99999,
				padding: 0,
				margin: 0,
				height:127,
				display:"block"
			});
			$("#popup_container_frame").css({
				zIndex: 99997,
				padding: 0,
				margin: 0
			});
			var patt1=/\n/gi;
			var noBR = String(msg).match(patt1);

			$alertIFrame = $("#popup_container_frame");
			$alertIFrame.contents().find("html").attr("dir", $alertIFrame.parent().parent().parent().attr("dir"));
			if ($alertIFrame.parent().parent().parent().attr("dir") == "rtl"){
				$alertIFrame.contents().find("body").addClass("rtl");
			}
			$alertIFrame.contents().find("body").find("#popup_title").text(title);
			$alertIFrame.contents().find("body").find("#popup_content").addClass(type);
			$alertIFrame.contents().find("body").find("#popup_message").text(msg);
			$alertIFrame.contents().find("body").find("#popup_message").html($alertIFrame.contents().find("body").find("#popup_message").text().replace(/\n/g, '<br />') );
			

			
			switch( type ) {
				case 'alert':
					$alertIFrame.contents().find("body").find("#popup_panel").html('<input type="button" value="' + _okLabel + '" id="popup_ok" />');
					$alertIFrame.contents().find("body").find("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$alertIFrame.contents().find("body").find(".popuo_close").click( function() {
						$.alerts._hide();
						//if( callback ) callback(false);//fixed no action perfoms
					});
					$alertIFrame.contents().find("body").find("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
					$alertIFrame.contents().find("body").find(".popuo_close").css({ cursor: 'pointer' });
				break;
				case 'confirm':
					$alertIFrame.contents().find("body").find("#popup_panel").html('<input type="button" value="' + _okLabel + '" id="popup_ok" /> <input type="button" value="' + _cancelLabel + '" id="popup_cancel" />');
					$alertIFrame.contents().find("body").find("#popup_ok").click( function() {
						$.alerts._hide();
						if( callback ) callback(true);
					});
					$alertIFrame.contents().find("body").find(".popuo_close").click( function() {
						$.alerts._hide();
						//if( callback ) callback(false);//fixed no action perfoms
					});
					$alertIFrame.contents().find("body").find("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( false );
					});
					$alertIFrame.contents().find("body").find("#popup_ok").focus();
					$alertIFrame.contents().find("body").find("#popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					$alertIFrame.contents().find("body").find(".popuo_close").css({ cursor: 'pointer' });
				break;
				case 'prompt':
					$alertIFrame.contents().find("body").find("#popup_message").append('<br /><input type="text" size="30" id="popup_prompt" />');
					$alertIFrame.contents().find("body").find("#popup_panel").html('<input type="button" value="' + _okLabel + '" id="popup_ok" /> <input type="button" value="' + _cancelLabel + '" id="popup_cancel" />')
					$alertIFrame.contents().find("body").find("#popup_prompt").width( $("#popup_message").width() );
					$alertIFrame.contents().find("body").find("#popup_ok").click( function() {
						var val = $("#popup_prompt").val();
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$alertIFrame.contents().find("body").find(".popuo_close").click( function() {
						$.alerts._hide();
						if( callback ) callback(false);
					});
					$alertIFrame.contents().find("body").find("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$alertIFrame.contents().find("body").find("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$alertIFrame.contents().find("body").find("#popup_prompt, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( value ) $alertIFrame.contents().find("body").find("#popup_prompt").val(value);
					$alertIFrame.contents().find("body").find("#popup_prompt").focus().select();
					$alertIFrame.contents().find("body").find(".popuo_close").css({ cursor: 'pointer' });
				break;
			}
			
			var hightvar = Math.round(msg.length/42);
			if (noBR != null)
				hightvar = (hightvar < noBR.length) ? noBR.length : hightvar;
			
			var buttonHeight = $alertIFrame.contents().find("#popup_panel").outerHeight();
			var newHeight = $("#popup_container").outerHeight() + (buttonHeight / 2) + 16 * hightvar ;
			$("#popup_container").css({
				minWidth: $("#popup_container").scrollWidth,
				maxWidth: $("#popup_container").scrollWidth,
				height: newHeight
			});
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					$("#popup_container").draggable({ handle: $(".popuo_title_bg") });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
		},
		_hide: function() {
			$("#popup_container").hide();
			$.alerts._overlay('hide');
			$.alerts._maintainPosition(false);
		},
		
		_overlay: function(status) {
			switch( status ) {
				case 'show':
					$.alerts._overlay('hide');
					$("BODY").append('<div id="popup_overlay"></div>');
					$("#popup_overlay").css({
						position: 'absolute',
						zIndex: 99995,
						top: '0px',
						left: '0px',
						width: '100%',
						height: $(document).height(),
						background: $.alerts.overlayColor,
						opacity: $.alerts.overlayOpacity
					});
				break;
				case 'hide':
					$("#popup_overlay").remove();
				break;
			}
		},
		
		_reposition: function() {
			var top = (($(window).height() / 2) - ($("#popup_container").outerHeight() / 2)) + $.alerts.verticalOffset;
			var left = (($(window).width() / 2) - ($("#popup_container").outerWidth() / 2)) + $.alerts.horizontalOffset;
			if( top < 0 ) top = 0;
			if( left < 0 ) left = 0;
			
			// IE6 fix
			if( $.browser.msie && parseInt($.browser.version) <= 6 ) top = top + $(window).scrollTop();
			
			$("#popup_container").css({
				top: top + 'px',
				left: left + 'px'
			});
			$("#popup_overlay").height( $(document).height() );
		},
		
		_maintainPosition: function(status) {
			if( $.alerts.repositionOnResize ) {
				switch(status) {
					case true:
						$(window).bind('resize', $.alerts._reposition);
					break;
					case false:
						$(window).unbind('resize', $.alerts._reposition);
					break;
				}
			}
		}
		
	}
	
	// Shortuct functions
	jAlert = function(message, title, callback, okLabel, cancelLabel) {
		$.alerts.alert(message, title, callback, okLabel, cancelLabel);
	}
	
	jConfirm = function(message, title, callback, okLabel, cancelLabel) {
		$.alerts.confirm(message, title, callback, okLabel, cancelLabel);
	};
		
	jPrompt = function(message, value, title, callback, okLabel, cancelLabel) {
		$.alerts.prompt(message, value, title, callback, okLabel, cancelLabel);
	};
	
})(jQuery);