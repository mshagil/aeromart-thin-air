$.fn.iterateTempletePB = function(params) {
	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;
    var segmentCode = "";   
	var dtoRecord;	
	for (var f = 0; f < params.data.length; f++) {	
		segmentCode = params.data[f].segmentName;
		reSetPaxWiseData(params.data[f].paxWise);
		for(var i = 0; i < params.data[f].paxWise.length ; i++) {			
			clonedTemplateId = templeteName + "_" + f + "" + i;			
			var clone = $( "#" + templeteName + "").clone();			
			clone.attr("id", clonedTemplateId );			
			clone.appendTo($("#" + templeteName).parent());			
			dtoRecord =  params.data[f].paxWise[i];			
			elementIdAppend = params.dtoName + "[" + i + "].";
			
			if (i == 0) {
				$("#"+clonedTemplateId + " td:first").attr("rowspan", params.data[f].paxWise.length);
				$("#"+clonedTemplateId + " td:first").find("label").each(function( intIndex ){
					$(this).text(segmentCode);
					$(this).attr("id",  elementIdAppend +  this.id);
				});					
			} else {
				$("#"+clonedTemplateId + " td:first").remove();
			}			
			$("#"+clonedTemplateId).find("label").each(function( intIndex ){
				$(this).text(dtoRecord[this.id]);
				$(this).attr("id",  elementIdAppend +  this.id);
			});
			
			$( "#" + clonedTemplateId).show();			
		}
	}
	if (calDisplayConfig.layout.displayFlexiInCal && params.data.length > 1){
		$( "#" + templeteName).find("td").css({
			width: "49%"
		});
		$( "#" + templeteName).find("td:first").css({paddingRight:8})
	}
	$( "#" + templeteName).hide();
};

/**
 * Reset Paxwise data
 */

reSetPaxWiseData = function(paxWise) {
	var showPriceBDInSelCurr = false;
	var currencyCode = "";
	var selectedCurrency = "";
	if (UI_AvailabilitySearch != null){
		showPriceBDInSelCurr = UI_AvailabilitySearch.showPriceBDInSelCurr;
		currencyCode = UI_AvailabilitySearch.currencyCode;
		selectedCurrency = UI_AvailabilitySearch.selectedCurrency;
	}else{
		showPriceBDInSelCurr = UI_mcAvailabilitySearch.showPriceBDInSelCurr
		currencyCode = UI_mcAvailabilitySearch.currencyCode
		selectedCurrency = UI_mcAvailabilitySearch.selectedCurrency
	}
	
	var tempPax = "";
	for (var i = 0; i < paxWise.length; i++) {
		tempPax = paxWise[i];		
		tempPax.paxType = getPaxType({paxCode:tempPax.paxType});		
		
		var currCode = '';
		var fareObj = {sur:null, fare:null, total:null, totalPaxFare : null};
		if(showPriceBDInSelCurr && (currencyCode != selectedCurrency)){
			curCode = selectedCurrency;
			if (tempPax.sur) {
				fareObj.sur =  curCode + " " + tempPax.selCurSur;			
			}
			if (tempPax.fare) {
				fareObj.fare = curCode + " " + tempPax.selCurFare;
			}
			
			if (tempPax.selCurtotal) {
				fareObj.total = curCode + " " + tempPax.selCurtotal;
			}
			if (tempPax.totalPaxFare) {
				fareObj.totalPaxFare = curCode + " " + tempPax.selCurTotalPaxFare;
			}
			if (tempPax.selCurTotalPaxFare) {
				fareObj.selCurTotalPaxFare = curCode + " " + tempPax.selCurTotalPaxFare;
			}
		}else{
			curCode = currencyCode;
			
			if (tempPax.sur) {
				fareObj.sur = curCode + " " + tempPax.sur;
			}
			if (tempPax.fare) {
				fareObj.fare = curCode + " " + tempPax.fare;
			}
			
			if (tempPax.total) {
				fareObj.total = curCode + " " + tempPax.total;
			}
			if (tempPax.totalPaxFare) {
				fareObj.totalPaxFare = curCode + " " + tempPax.totalPaxFare;
			}
			if (tempPax.selCurTotalPaxFare) {
				fareObj.selCurTotalPaxFare = curCode + " " + tempPax.selCurTotalPaxFare;
			}
		}
		
		tempPax.fare = fareObj.fare;
		tempPax.sur = fareObj.sur;
		tempPax.total = fareObj.total;
		tempPax.totalPaxFare = fareObj.totalPaxFare;
		tempPax.selCurTotalPaxFare = fareObj.selCurTotalPaxFare;
		delete paxWise[i];
		paxWise[i] = tempPax;
	}
	return paxWise;
};



/**
 * Set the currency codes based on view options.
 */
reSetTaxBDData = function(taxBDData) {
	var showPriceBDInSelCurr = false;
	var currencyCode = "";
	var selectedCurrency = "";
	if (UI_AvailabilitySearch != null){
		showPriceBDInSelCurr = UI_AvailabilitySearch.showPriceBDInSelCurr;
		currencyCode = UI_AvailabilitySearch.currencyCode;
		selectedCurrency = UI_AvailabilitySearch.selectedCurrency;
	}else{
		showPriceBDInSelCurr = UI_mcAvailabilitySearch.showPriceBDInSelCurr;
		currencyCode = UI_mcAvailabilitySearch.currencyCode;
		selectedCurrency = UI_mcAvailabilitySearch.selectedCurrency;
	}
	
	var tempBD = "";	
	for (var i = 0; i < taxBDData.length; i++) {		
		tempBD = taxBDData[i];
		// we have only one type per row. so getting the first element.
		
 		var paxTypeString = getPaxType({paxCode:tempBD.applicablePassengerTypeCode[0]});
		if (paxTypeString != undefined && paxTypeString!=null && paxTypeString.length>0){
		    tempBD.applicableToDisplay = paxTypeString;
		}
		var currCode = '';
		var amount = null;
		
		if(showPriceBDInSelCurr && (currencyCode != selectedCurrency)){
			currCode = selectedCurrency;
			if (tempBD.amountInSelectedCurrency){
				amount = curCode + " " + tempBD.amountInSelectedCurrency;
			}						
		} else {
			currCode = currencyCode;
			if (tempBD.amount){
				amount = curCode + " " + tempBD.amount;
			}		
		}
		//set the formated amount value with currency code.
		tempBD.amount = amount;		
		delete taxBDData[i];
		taxBDData[i] =  tempBD;
	}
	return taxBDData;
};

getPaxType = function(params) {
	var lblAdult = "";
	var lblChild = "";
	var lblInfant = "";
	if (UI_AvailabilitySearch != null){
		lblAdult = UI_AvailabilitySearch.lblAdult;
		lblChild = UI_AvailabilitySearch.lblChild;
		lblInfant = UI_AvailabilitySearch.lblInfant;
	}else{
		lblAdult = UI_mcAvailabilitySearch.lblAdult;
		lblChild = UI_mcAvailabilitySearch.lblChild;
		lblInfant = UI_mcAvailabilitySearch.lblInfant;
	}
	switch(params.paxCode) {
		case "AD": return lblAdult;break;
		case "CH": return lblChild;break;
		case "IN": return lblInfant;break;		
	}
};


$.fn.iterateTempleteTaxBD = function(params) {
	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;  
	var dtoRecord;	
	reSetTaxBDData(params.data);
	for (var f = 0; f < params.data.length; f++) {	
		clonedTemplateId = templeteName + "_" + f;
		var clone = $( "#" + templeteName + "").clone();	
		clone.attr("id", clonedTemplateId );
		clone.appendTo($("#" + templeteName).parent());			
		dtoRecord =  params.data[f];	
		elementIdAppend = params.dtoName + "[" + f + "].";
		
		$("#"+clonedTemplateId).find("label").each(function( intIndex ){
			$(this).text(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});						
		$( "#" + clonedTemplateId).show();	
	}
	$( "#" + templeteName).hide();
};


function setDesignSpecLabels(){
	return true;
}

/**
 * Set Total Fare
 */
setTotalFare = function(fareInCurrency) {
	var totalPriceInBase = fareInCurrency.inBaseCurr.totalPrice;
	$("#totalAmount").text(fareInCurrency.inBaseCurr.currency + " " +totalPriceInBase);	
	
	if (fareInCurrency.inBaseCurr.currency != fareInCurrency.inSelectedCurr.currency) {
		var totalPriceInSelCur = fareInCurrency.inSelectedCurr.totalPrice;
		$("#totalAmountSel").text(fareInCurrency.inSelectedCurr.currency + " " + totalPriceInSelCur );
		$("#selectedTotalPanel").show();	
		$("#unselectedTotalPanel").removeClass('baseTotalPanel');
	}
}