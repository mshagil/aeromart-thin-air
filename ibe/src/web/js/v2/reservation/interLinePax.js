function UI_Passenger(){}

var arrPaxTitle = new Array();
var arrNationality = new Array();
var arrError = new Array();
var arrCountryPhone = new Array();
var arrAreaPhone  = new Array(); 
var paxAdults;
var paxChild;
var paxInfants;
var paxValidation;
var system = "";
var arrAdtTitle = new Array();
var arrChdTitle = new Array();
var arrInfTitle = new Array();


var strMsgAdult = "";
var strMsgChild = "";
var strMsgInfant = "";
var strMsgFN = "";
var strMsgLN = "";
var strMsgDOB = "";
var strMsgPSPT = "";
var strOneWayTrip = "";
var strRoundTrip = "";
var strMsgEmailConfirm = "";
var strLanguage = "";
var validateEmailDomain = false;
var foidLists = "";
var uniqfoid = "";
var uniqfoidCod = "";
var profEmailAddress = "";
var headFfidReq = false;
var lmsDetails = {};

//Pax & Contact info configs
var contactConfig = "";
var validationGroup = "";
var paxConfigAD = "";
var paxConfigCH = "";
var paxConfigIN = "";
var populateFirstPaxCheck = false;
UI_Passenger.displayLmsOnce=false;
UI_Passenger.securePath = "";
UI_Passenger.homeUrl = "";
UI_Passenger.loaded = false;
UI_Passenger.initialized = false;
UI_Passenger.isContactNationalityModified = false;
UI_Passenger.boolValidate = true;
UI_Passenger.passengerOptions = "guest"; //posible values guest, oldcustomer, newcustomer
UI_Passenger.isDomesticSegmentExists = false;
UI_Passenger.busCarrierCodes = {};


UI_Passenger.showSocial = {'FB':false, 'LN':false };

UI_Passenger.paxDetails = {};

//this will keep the mapping of validation group to
//client error code
var jsonGroupValidation = {"1":"ERR088"};

var jsonGroupElementMapping = {"1": ["#txtPhone","#txtPArea","#txtPCountry","#txtMobile","#txtMArea","#txtMCountry"]};

//this will keep the mapping of fieldname+groupid of t_contact_details_config
//to html form element
var jsonFieldToElementMapping = {"phoneNo1":"#txtPhone", "mobileNo1":"#txtMobile"};

UI_Passenger.relationshipList = null;
UI_Passenger.nationalityList = null;
UI_Passenger.paxDetailsRowId = null;
UI_Passenger.selectedFamilyMemberId = null;
UI_Passenger.relationshipTitleList = null;

UI_Passenger.ready = function(){
	globalConfig.currentPage = "PAX"
	UI_Container.showLoading();
	$("#divAnciPromoBanner").hide();
	$("#divSummaryPane").css("top","0px");
	// Page layout changes are effected
	UI_Passenger.setPagelayoutSettings();
	UI_Passenger.reSetAreCode();
	UI_commonSystem.showLmsInBar();
	
	var jsonFltDetails = $.parseJSON($('#resSelectedFlights').val()).flightSegments;
	// French translation issue fix for Arrival & Departure dates - Supports all languages
	if(jsonFltDetails.length > 0){
		for(var i = 0; i < jsonFltDetails.length;i++){
			if(typeof(jsonFltDetails[i]) !== "undefined"){
				jsonFltDetails[i].arrivalDate = unescape(jsonFltDetails[i].arrivalDate);
				jsonFltDetails[i].departureDate = unescape(jsonFltDetails[i].departureDate);
			}
		}
	}
	
	
	if(UI_Passenger.initialized == false){
		var data = new Array();
		data['paxType'] = paxCat;
		data['carriers'] = UI_Passenger.createCarrierList();
		data['system'] = system;
		data['origin'] = $("#resFromAirport").val();
		data['destination'] = $("#resToAirport").val();
		UI_commonSystem.getDummyFrm().ajaxSubmit({dataType: 'json', data:data, 
				url:'loadPaxContactConfig.action', async:false,
				success: UI_Passenger.processPaxContactConfig, 
				error:UI_commonSystem.setErrorStatus});
				
		// Show/Hide Contact Info & Emergency contact info based on the configurations
		UI_Passenger.showHideContactInfo();
		
		//Buttons
		if(contactConfig.country1.ibeVisibility){
			$("#selCountry").change(function(){UI_commonSystem.pageOnChange();UI_Passenger.setCountryPhone();UI_Passenger.setCountryName();});
			$("#txtMCountry").focusout(UI_Passenger.setCountryPhone);
		}	
		
		if(contactConfig.mobileNo1.ibeVisibility){
			$("#txtMCountry").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});	
			$("#txtMArea").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});	
			$("#txtMobile").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
		}
		
		if(contactConfig.phoneNo1.ibeVisibility){
			$("#txtPCountry").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});	
			$("#txtPArea").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
			$("#txtPhone").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
		}

		if(contactConfig.fax1.ibeVisibility){
			$("#txtFCountry").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});	
			$("#txtFArea").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
			$("#txtFax").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
		}
			
		if(contactConfig.phoneNo2.ibeVisibility){
			$("#txtEmgnPCountry").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});	
			$("#txtEmgnPArea").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
			$("#txtEmgnPhone").keyup(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.onlyNumericTxt(this);});
		}

		//$("#chkEmail").click(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.chkEmailChange(this);});
		$("#btnSignIn").click(function(event) {UI_commonSystem.pageOnChange();UI_Passenger.loginClick();UI_commonSystem.showLmsInBar();});
		$("#chkSameApplyNumber").click(function(event) {UI_Passenger.applyNumbeDuringTravel(this);})
		if(contactConfig.nationality1.ibeVisibility){
			$("#selNationality").change(function(event){UI_Passenger.isContactNationalityModified = true;});
		}
		
		$("#txtMobile, #txtMCountry, #txtMArea").change(function(){
			if ( $("#chkSameApplyNumber").attr("checked") == true ){
				$("#chkSameApplyNumber").attr("checked", false);
				UI_Passenger.applyNumbeDuringTravel($("#chkSameApplyNumber"));
			}
		});

		//$("#lblLogOut").click(function(){
			//UI_commonSystem.loadingProgress();
			//$.post("customerLogout.action","", UI_Passenger.logoutCustomer, "json");
		//});
		var voidPaste = function(event){
			if(event.ctrlKey && event.which == 86){
				event.preventDefault();
			}
			
			if(event.shiftKey && event.which == 45){
				event.preventDefault();
			}
			if(event.button == 2 || event.button == 1){
				event.preventDefault();
			}
		}
		
		if(contactConfig.email1.ibeVisibility){
			$("#txtVerifyEmail").keypress(voidPaste);
			$("#txtVerifyEmail").keydown(voidPaste);
			$("#txtVerifyEmail").mousedown(voidPaste);
			$("#txtVerifyEmail").click(voidPaste);
			$("#txtVerifyEmail").bind("contextmenu",function(e){return false});
		}
		
		
		UI_Passenger.initialized = true;
		$("#passengerMainTable").css("display","block");


	}
	if(UI_Passenger.loaded == false){
		UI_Passenger.loadPaxdata();
	}else{
		setTimeout('UI_Container.hideLoading()',1000);
	}
	if($("#resOndListStr").val()!=""){
		$("#trModSearch").hide();
	}
	if(UI_Top.holder().GLOBALS.displayNotificationForLastNameInIBE == false){
		$("#tblLastNameNotification").hide();
	}
	
	if(!UI_Top.holder().GLOBALS.loyaltyEnable){
		  $("#imgSeperater").hide();
          $("#imgAirewards").hide();
	}
	
	$("#isLMS").on("click", function(){
		if($("#isLMS").prop( "checked" )){
            $("#tblFFPField").show();
		}else{
            $("#tblFFPField").hide();
        }
	});
	
	// clears the pax info when "click" is clicked
	$(".clearAutoGenPax").click(UI_Passenger.clearPaxInfo);
	
	var now = new Date();
	var yearRange = (now.getFullYear() - 100) + ":" + now.getFullYear();
	
	$("#textDOBLMS").datepicker({
		regional: SYS_IBECommonParam.locale ,
		dateFormat: "dd/mm/yy",
		showOn: 'both',
		yearRange: yearRange,
		maxDate:'-2Y',
		minDate: "-99Y +1D",
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
	});
	
	$("#textDOBLMS").change(function() {
		var dob = $("#textDOBLMS").datepicker('getDate');
		var present = new Date();
		var age = (present - dob)/1000/60/60/24;
		if (age > (12*365+3)){
			$("#lblFamilHeadEmailLmsMand").hide();
			headFfidReq = false;
		}else{
			$("#lblFamilHeadEmailLmsMand").show();
			headFfidReq = true;
		}
	});
	
	$("#btnLmsJoin_sumPnl").click(function(){
		UI_commonSystem.joinLmsButtonClick();
	});
	
	UI_commonSystem.setCommonButtonSettings();
	UI_commonSystem.changeUserLmsStatus(UI_commonSystem.lmsDetails);
	UI_commonSystem.showLmsInBar();
	$("#textPPNumberLMS").alphaNumeric();
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
	
	$("#loadFamilyDetailsOK").click(function(){UI_Passenger.setFamilyMemberData();});
	$("#loadFamilyDetailsCancel").click(function(){$("#familyMemberDetails").dialog('close');});
	
	if(UI_Container.isRegisteredUser == true){
		var data = {};
		
		$.ajax({
			
	        url : 'showCustomerProfileDetail!setFamilyMemberDTOList.action',
	        data: data,
	        dataType:"json",
	        beforeSend :UI_commonSystem.loadingProgress,
	        type : "POST",		           
			success: UI_Passenger.getFamilyMemberSuccess,
			error:UI_commonSystem.setErrorStatus
		
	     });
		
	}
	
}

UI_Passenger.loadFamilyDetailsOnClick = function(event){
	
	//var id = event.currentTarget.id;
	UI_Passenger.paxDetailsRowId = event.currentTarget.id.split(".")[0];
	$("#familyMemberDetails").dialog('open');
	
}

UI_Passenger.getFamilyMemberSuccess = function(response){
	
	var id;
	var strHTMLText = '';
	UI_Passenger.relationshipList = response.relationshipList;
	UI_Passenger.nationalityList = response.nationalityInfo;
	UI_Passenger.relationshipTitleList = response.relationshipTitleList;
	var strColor = "SeperateorBGColor";
	$("#familyMembersList").find("#familyMembersTable").empty();
	
	$.each(response.familyMemberDTOList, function(i,familyMemberDTO){
		
		id = familyMemberDTO.familyMemberId;
		
		if (strColor == "SeperateorBGColor"){
			strColor = "";
		}else{
			strColor = "SeperateorBGColor";
		}
		
		strHTMLText = '';
		
		strHTMLText = '<tr style="cursor:pointer;" id="familyMemberId_'+id+'" class="GridItems "'+strColor+'">';
		
		strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
		strHTMLText += '		<label style="cursor:pointer;" id="'+id+'_FMFirstName">' + familyMemberDTO.firstName + '<\/label>';
		strHTMLText += '	<\/td>';

		strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
		strHTMLText += '		<label style="cursor:pointer;" id="'+id+'_FMLastName">' + familyMemberDTO.lastName + '<\/label>';
		strHTMLText += '	<\/td>';
			
		strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
		strHTMLText += '		<label style="cursor:pointer;" id="'+id+'_FMRelationshipId">' + getRelationshipDescription(familyMemberDTO.relationshipId) +'<\/label>';
		strHTMLText += '	<\/td>';
		
		strHTMLText += '	<td class="GridDblHighlight bdRight" align="center" style="padding-left:10px;">';
		strHTMLText += '		<label style="cursor:pointer;" id="'+id+'_FMVationalityCode">' + getNationalityName(familyMemberDTO.nationalityCode) +'<\/label>';
		strHTMLText += '	<\/td>';
		
		strHTMLText += '	<td class="GridDblHighlight " align="center" style="padding-left:10px;">';
		strHTMLText += '		<label style="cursor:pointer;" id="'+id+'_FMDateOfBirth">' + familyMemberDTO.dateOfBirth +'<\/label>';
		strHTMLText += '	<\/td>';
		
		strHTMLText += '<\/tr>';
			
		$("#familyMembersList").find("#familyMembersTable").append(strHTMLText);
	});	
	
	UI_Container.hideLoading();
	
	$("#familyMemberDetails").dialog({ 
		autoOpen: false,
		modal:true,
		height: 'auto',
		width: 'auto',
		title:'Family Member Details',
		close:function() {$("#familyMemberDetails").dialog('close');}
	});
	
	
	$("tbody#familyMembersTable tr").click(function(){
		   $(this).css('background-color' , '#ffb900').siblings().css('background-color' , '');
		   var value = $(this).attr('id');
		   UI_Passenger.selectedFamilyMemberId = value.split('_')[1];
	});
	
}

UI_Passenger.displayLmsMsg = function(){
	if(this.value!="" && this.value!=null){
	    var msg = "You will earn loyalty points with this booking";
	    if (!UI_Passenger.displayLmsOnce){
	        var lmsMsg = $("<div><label>"+msg+"</label></div>")
	            .css({
	            "background-color":"#ffffdd",
	            "padding":"10px",
	            "border":"1px solid #f8f8a9",
	            "position":"relative",
	            "text-align":"center",
	            "margin":"0 auto",
	            "width":"60%"
	        })
	            .addClass("ui-corner-all");
	        $("#divPassenger").prepend(lmsMsg);
	        var tm= setTimeout(function(){
	            lmsMsg
	                .slideUp()
	                .fadeOut(function(){
	                    lmsMsg.remove();
	                })
	        },8000);
	        UI_Passenger.displayLmsOnce=true;
	    }
	}
}

UI_Passenger.createCarrierList = function(){
	var jsonFltDetails = $.parseJSON($('#resSelectedFlights').val()).flightSegments;
	// French translation issue fix for Arrival & Departure dates - Supports all languages
	
	if(jsonFltDetails.length > 0){
		for(var i = 0; i < jsonFltDetails.length;i++){
			if(typeof(jsonFltDetails[i]) !== "undefined"){
				jsonFltDetails[i].arrivalDate = unescape(jsonFltDetails[i].arrivalDate);
				jsonFltDetails[i].departureDate = unescape(jsonFltDetails[i].departureDate);
			}
		}
	}
	
	var carrierList = '';
	if(jsonFltDetails != []){
		for(var i=0;i<jsonFltDetails.length;i++){
			if(i == 0){
				system = jsonFltDetails[i].system;
			}
			if(!UI_Passenger.isCarrierExists(carrierList, jsonFltDetails[i].carrierCode)){
				if(i == (jsonFltDetails.length - 1)){
					carrierList += jsonFltDetails[i].carrierCode;					
				} else {
					carrierList += jsonFltDetails[i].carrierCode + ",";
				}
			}
			if(jsonFltDetails[i].domesticFlight == true){
				UI_Passenger.isDomesticSegmentExists = true;
			}
		}
	}
	
	return carrierList;
};



UI_Passenger.isCarrierExists = function(arr, elem){
	var spltArr = arr.split(",");
	for(var i=0;i<spltArr.length;i++){
		if(spltArr[i] == elem){
			return true;		 
		}
	}
	
	return false;
}

UI_Passenger.processPaxContactConfig = function(response){
	if(response.success){
		contactConfig = response.contactConfig;
		validationGroup = response.validationGroup;
		paxConfigAD = response.paxConfigAD;
		paxConfigCH = response.paxConfigCH;
		paxConfigIN = response.paxConfigIN;
		isRequestNICForReservationsHavingDomesticSegments = response.requestNICForReservationsHavingDomesticSegments;
	}
}

UI_Passenger.showHideContactInfo = function(){
	
	//Reservation contact related configurations
	if(contactConfig.title1.ibeVisibility){
		if(!contactConfig.title1.mandatory){
			$("#lblTitleMand").hide();
		}
	} else {
		$("#trTitle").hide();
	}
	
	if(contactConfig.firstName1.ibeVisibility){
		if(!contactConfig.firstName1.mandatory){
			$("#lblFirstNameMand").hide();
		}
	} else {
		$("#tblFirstName").hide();
	}
	
	if(contactConfig.lastName1.ibeVisibility){
		if(!contactConfig.lastName1.mandatory){
			$("#lblLastNameMand").hide();
		}
	} else {
		$("#tblLastName").hide();
	}
	
	if(contactConfig.nationality1.ibeVisibility){
		if(!contactConfig.nationality1.mandatory){
			$("#lblNationalityMand").hide();
		}
	} else {
		$("#tblNationality").hide();
	}
	
	if(contactConfig.city1.ibeVisibility){
		if(!contactConfig.city1.mandatory){
			$("#lblCityMand").hide();
		}
	} else {
		$("#tblCity").hide();
	}
	
	if(contactConfig.address1.ibeVisibility){
		if(!contactConfig.address1.mandatory){
			$("#lblAddrMand").hide();
		}
	} else {
		$("#tblAddr1").hide();
		$("#tblAddr2").hide();
	}
	
	if(contactConfig.country1.ibeVisibility){
		if(!contactConfig.country1.mandatory){
			$("#lblCountryMand").hide();
		}
	} else {
		$("#tblCountry").hide();
	}
	
	if(contactConfig.preferredLang1.ibeVisibility){
		if(!contactConfig.preferredLang1.mandatory){
			$("#lblPreferredLangMand").hide();
		}
	} else {
		$("#tblPreferredLang").hide();
	}
	
	if(contactConfig.zipCode1.ibeVisibility){
		if(!contactConfig.zipCode1.mandatory){
			$("#lblZipCodeMand").hide();
		}
	} else {
		$("#tblZipCode").hide();
	}
	
	if((!contactConfig.mobileNo1.ibeVisibility) &&
			(!contactConfig.phoneNo1.ibeVisibility) && (!contactConfig.fax1.ibeVisibility)){
		$("#tblContactNo").hide();
	} else {
		if(contactConfig.mobileNo1.ibeVisibility){
			if(!contactConfig.mobileNo1.mandatory){
				$("#lblMobileNoMand").hide();
			}
			
			if(contactConfig.mobileNo1.maxLength && UI_Top.holder().GLOBALS.addMobileAreaCodePrefix){
				// add maximum text field lengths for mobile Number
				var maxLength = contactConfig.mobileNo1.maxLength.split('-');
				var areaCodeMaxLen = maxLength[0];
				var mobileMaxLen = maxLength[1];
				
				$("#txtMArea").attr('maxlength', areaCodeMaxLen);
				$("#txtMobile").attr('maxlength', mobileMaxLen);
			} 
			
		} else {
			$("#trMobileNo").hide();
		}
		
		if(contactConfig.phoneNo1.ibeVisibility){
			if(!contactConfig.phoneNo1.mandatory){
				$("#lblPhoneNoMand").hide();
			}
		} else {
			$("#trPhoneNo").hide();
		}
		
		if(contactConfig.fax1.ibeVisibility){
			if(!contactConfig.fax1.mandatory){
				$("#lblFaxMand").hide();
			}
		} else {
			$("#trFaxNo").hide();
		}
	}
	
	if(contactConfig.email1.ibeVisibility){
		if(!contactConfig.email1.mandatory){
			$("#lblEmailMand").hide();
			$("#lblVerifyEmailMand").hide();
		}
	} else {
		$("#tblEmail").hide();
	}
	
	// show/hide Contact Details Labels
	if (!contactConfig.contact_detail_label1.ibeVisibility){
		$("#trPhoneNoHeader").hide();
		$("#trPhoneNoLabel").hide();
	}
	// show/hide Contact Details Labels
	if (!contactConfig.areaCode1.ibeVisibility){
		$(".areaCode").hide();
	}
	
	//Emergency Contact related configurations
	if(contactConfig.firstName2.ibeVisibility ||
			contactConfig.lastName2.ibeVisibility ||
			contactConfig.phoneNo2.ibeVisibility){
		
		if(contactConfig.title2.ibeVisibility){
			if(!contactConfig.title2.mandatory){
				$("#lblEmgnTitleMand").hide();
			}
		} else {
			$("#trEmgnTitle").hide();
		}
		
		if(contactConfig.firstName2.ibeVisibility){
			if(!contactConfig.firstName2.mandatory){
				$("#lblEmgnFNameMand").hide();
			}
		} else {
			$("#tblEmgnFirstName").hide();
		}
		
		if(contactConfig.lastName2.ibeVisibility){
			if(!contactConfig.lastName2.mandatory){
				$("#lblEmgnLNameMand").hide();
			}
		} else {
			$("#tblEmgnLastName").hide();
		}
		
		if(contactConfig.phoneNo2.ibeVisibility){
			if(!contactConfig.phoneNo2.mandatory){
				$("#lblEmgnPhoneNoMand").hide();
			}
		} else {
			$("#trEmgnPhoneNo").hide();
		}
		
		if(contactConfig.email2.ibeVisibility){
			if(!contactConfig.email2.mandatory){
				$("#lblEmgnEmailMand").hide();
			}
		} else {
			$("#tblEmgnEmail").hide();
		}		
	} else {
		$("#trEmgnContactInfo").hide();
	}
	
}

UI_Passenger.startOverClick = function() {
	top[0].homeClick();
}

UI_Passenger.loadPaxdata = function(){
	 $("#frmReservation").attr('action', 'interlinePaxDetail.action');
	 $("#frmReservation").ajaxSubmit({dataType: 'json',			
			success: UI_Passenger.loadPassengerPage, error:UI_commonSystem.setErrorStatus});			
	return false;
}

UI_Passenger.loadAnciPage  = function(){
	if(UI_Passenger.paxValidat()) {
		$("#frmReservation").attr('action','showLoadPage!loadAnci.action');
		$("#frmReservation").submit();
	}
}

UI_Passenger.loadErrorPage = function(response){
	UI_Container.hideLoading();
	window.location.href="showLoadPage!loadError.action?messageTxt="+response.messageTxt;
}

//UI_Passenger.loadPaymentPage = function(){	
//	if(UI_Passenger.paxValidat()) {
//		UI_Passenger.setContactNumberValidData();
//		$("#frmPassenger").attr('action', UI_Passenger.securePath+'showLoadPage!loadPayment.action');
//		$("#frmPassenger").submit();
//	}	
//}

UI_Passenger.setParent = function (childObject) {
	var stInd = childObject.indexOf("[");
	var stIndout = childObject.indexOf("]");	
	var clId = childObject.substr(stInd+1, (stIndout- (stInd+1)));
	var adid = $("#infantList\\["+clId+"\\]\\.travelWith").val();
	adid = Number(adid) -1;
	$("#adultList\\["+adid+"\\]\\.parent").val(true);
	$("#adultList\\["+adid+"\\]\\.travelWith").val(Number(clId) +1);
}

UI_Passenger.loadPassengerPage = function(response){		
	if(response != null && response.success == true) {		
		$('input').change(function (){UI_commonSystem.pageOnChange();});
				
		UI_Passenger.securePath = response.urlInfo.securePath;
		UI_Passenger.homeUrl = response.urlInfo.homeURL;		
		UI_Passenger.paxDetails = response.paxDetail;
		paxValidation = response.paxValidation;		
		validateEmailDomain = response.validateEmailDomain;
		
		arrInfTitle[0] = ["MSTR","Mstr"];
		arrInfTitle[1] = ["MISS","Miss"];
		
		//TODO cleanup used things
		var label = UI_Container.getLabels();
		strMsgAdult = label["lblAdultHD"];
		strMsgChild = label["lblChildHD"];
		strMsgInfant = label["lblInfantHD"];
		strMsgFN = label["lblFirstName"];
		strMsgLN = label["lblLastName"];
		strMsgDOB = label["lblPaxDOB"];
		strMsgPSPT = label["lblPSPT"];
		//strOneWayTrip = label["lblOneWayTrip"];
		//strRoundTrip = label["lblRoundTrip"];
		strMsgEmailConfirm = label["MsgEmailConfirm"];		
		//eval(response.nationalityInfo);	
		eval(response.titleVisibilityInfo);
		eval(response.adtTitleVisibilityInfo);
		eval(response.chdTitleVisibilityInfo);
		//UI_Passenger.setItinaryLanguages(response.languageInfo);	
		UI_Passenger.setPreferredLanguages(response.languageInfo);
		//$("#selCountry").append(response.countryInfo);
		//$("#selNationality").fillDropDown({dataArray:arrNationality, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selCountry").fillDropDown({dataArray:response.countryInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selNationality").fillDropDown({dataArray:response.nationalityInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selTitle").fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selEmgnTitle").fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='nationality']").fillDropDown({dataArray:response.nationalityInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='foidPlace']").fillDropDown({dataArray:response.countryInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='title']").fillDropDown({dataArray:arrAdtTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='chdTitle']").fillDropDown({dataArray:arrChdTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='infTitle']").fillDropDown({dataArray:arrInfTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='visaApplicableCountry']").fillDropDown({dataArray:response.countryInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		$("select[name='travelDocumentType']").fillDropDown({dataArray:response.docTypeInfo, keyIndex:0, valueIndex:1, firstEmpty:true});
		UI_Passenger.busCarrierCodes = response.busCarrierCodes;
		
//		$("#airFareCurrCode").append(response.fareQuote.currency);
//		$("#airFare").append(response.fareQuote.totalFare);
//		$("#chargesCurrCode").append(response.fareQuote.currency);
//		$("#charges").append(response.fareQuote.totalTaxSurcharge);
//		$("#totalCurrCode").append(response.fareQuote.currency);
//		$("#total").append(response.fareQuote.totalPrice);	
		
//		if (response.fareQuote.currency != response.fareQuote.selectedCurrency ) {
//			$("#selectedCurr").show();
//			$("#totalSelectedCurrCode").append(response.fareQuote.selectedCurrency);
//			$("#selectedTotal").append(response.fareQuote.selectedtotalPrice);	
//		}
		var adLenArray = new Array();
		for(var i=0;i< response.paxDetail.adultList.length;i++){
			adLenArray[i] = new Array(i, i+1);
		}
		$("select[name='travelWith']").fillDropDown({dataArray:adLenArray, keyIndex:1, valueIndex:1, firstEmpty:true});
				
		//UI_Passenger.loadFlightdata(response.flightSegments);		
		UI_Passenger.loadPaxGrid(response.paxDetail, response.currentDate);
		UI_Passenger.setContactData(response.contactInfo, response.accessPoint, response.regUserAccessInKiosk, response.showRegUserLoginInPaxDetailPage);
		UI_Passenger.createFirstPaxFromContactInfo(response.contactInfo, response.lmsDetails);
//		if(response.blnReturn) {
//			$("#lblReturn").text(strRoundTrip);
//			$("#lblRetFlight").text(strRoundTrip);
//		}else {
//			$("#lblReturn").text(strOneWayTrip);
//			$("#lblRetFlight").text(strOneWayTrip);
//		}
		if(!response.showRegUserLoginInPaxDetailPage){
			$("#paxPageOptions").hide();
		}
		try{
			eval(response.errorMessageInfo);
		}catch(e){}
		UI_Passenger.buildTelephoneInfo(response.countryPhoneInfo, response.areaPhoneInfo);
		paxValidation = response.paxValidation;	
		UI_Passenger.createFirstPaxFromContactInfo(response.contactInfo, response.lmsDetails);
		UI_Passenger.hideMandatoryText();
		
		
		if(response.showSocialLoginInPaxDetailPage){
			UI_Passenger.loadThirdPartyScripts(response.socialParams);
		}
		
		UI_Passenger.loaded = true;
		UI_Container.hideLoading();
	} else {
		UI_Passenger.loadErrorPage(response);
	}
	
	
}

UI_Passenger.isBusCarrierCode = function(carrierCode){
	for(var i =0; i < UI_Passenger.busCarrierCodes.length; i++){
		if(carrierCode == UI_Passenger.busCarrierCodes[i] ){
			return true;
		}
	}
	return false;
}

UI_Passenger.loadFlightdata = function(segments){	
	$("#flightTemplate").iterateTemplete({templeteName:"flightTemplate", data:segments, dtoName:"flightSegments"});
}

UI_Passenger.getMinExpiryDate = function(){
	
	var minDate = new Date();	
	var inboundSeg = UI_Container.getInboundFlightSegs();
	var outBoundSeg = UI_Container.getOutboundFlightSegs();
	var fltRefNum = "";
	
	if(inboundSeg!= [] && inboundSeg.length!=0){
		fltRefNum = inboundSeg[inboundSeg.length-1].flightRefNumber;
	} else if(outBoundSeg != [] && outBoundSeg.length!=0){
		fltRefNum = outBoundSeg[outBoundSeg.length-1].flightRefNumber;
	}
	
	if(fltRefNum != ""){		
		var arrDateStr = fltRefNum.split("$");
		
		if(arrDateStr.length >= 5){
			var dateStr = trim(arrDateStr[4]);
			var arrDate = dateStr.split("#");
			
			if(arrDate.length >= 1){
				var dateNum = arrDate[0];
				var month = new Number(dateNum.slice(4,6)) - 1;
				minDate = new Date(dateNum.slice(0,4), month, dateNum.slice(6,8));
			}
		}
	}
	
	minDate.setDate(minDate.getDate()+1);
	return minDate;
}

UI_Passenger.loadPaxGrid = function(passengers, currentDate){
	var paxValidation = UI_Container.getPaxValidation();
	$("#paxAdTemplate").iterateTemplete({templeteName:"paxAdTemplate", data:passengers.adultList, dtoName:"adultList"});
	
	var psptExpiryMinDate = UI_Passenger.getMinExpiryDate();	
	var curDateArr = UI_Container.getSystemDate().split('/');
	 
	var depDate = UI_Passenger.extractFlightLastSegDate();
	var deptDateArr =  depDate.split('/');
	
	var curDay = (curDateArr[0] * curDateArr[0] ) / curDateArr[0];
    var curMon = (curDateArr[1] * curDateArr[1] ) / curDateArr[1];
    var curYer = curDateArr[2];
    
	var day = (deptDateArr[0] * deptDateArr[0] ) / deptDateArr[0];
	var mon = (deptDateArr[1] * deptDateArr[1] ) / deptDateArr[1];
	var yer =  deptDateArr[2];
	
	if(passengers.adultList.length == 0){
		$("#adlGrid").hide();
		$("#lblAdults").hide();
		$("#adultCount").hide();
		
	}else {
		paxAdults = passengers.adultList;		
		/*var range = '-'+paxValidation.adultAgeCutOverYears+':+'+paxValidation.adultAgeCutOverYears;
		var minDate = '-'+paxValidation.adultAgeCutOverYears+'y';
		var maxDate = '-'+(paxValidation.childAgeCutOverYears-1)+'y';*/
		
		var minDate = new Date((yer-paxValidation.adultAgeCutOverYears), mon-1, day);
		var maxDate = new Date((yer-paxValidation.childAgeCutOverYears), mon-1, day-1);
		var range = '-' + paxValidation.adultAgeCutOverYears + ':+' + paxValidation.adultAgeCutOverYears;		
		
		for(var pl=0;pl < passengers.adultList.length;pl++){
			
			if(!paxConfigAD.title.ibeVisibility){
				$("#tdADTitle").hide();
				$("#adultList\\["+pl+"\\]\\.title").parent().hide();
			} else {
				if(!paxConfigAD.title.ibeMandatory){
					$("#lblADTitleMand").hide();
				}
			}
			
			if(!paxConfigAD.firstName.ibeVisibility){
				$("#tdADFName").hide();
				$("#adultList\\["+pl+"\\]\\.firstName").parent().hide();
			} else {
				if(!paxConfigAD.firstName.ibeMandatory){
					$("#lblADFNameMand").hide();
				}
			}
			
			if(!paxConfigAD.lastName.ibeVisibility){
				$("#tdADLName").hide();
				$("#adultList\\["+pl+"\\]\\.lastName").parent().hide();
			} else {
				if(!paxConfigAD.lastName.ibeMandatory){
					$("#lblADLNameMand").hide();
				}
			}
			
			if(!paxConfigAD.nationality.ibeVisibility){
				$("#tdADNationality").hide();
				$("#adultList\\["+pl+"\\]\\.nationality").parent().hide();
			} else {
				if(!paxConfigAD.nationality.ibeMandatory){
					$("#lblADNationalityMand").hide();
				}
			}
			
			if(!paxConfigAD.dob.ibeVisibility){
				$("#tdADDOB").hide();
				$("#adultList\\["+pl+"\\]\\.dateOfBirth").parent().hide();
			} else {
				if(!paxConfigAD.dob.ibeMandatory){
					$("#lblADDOBMand").hide();
				}
				
				$("#adultList\\["+pl+"\\]\\.dateOfBirth").datepicker({regional: SYS_IBECommonParam.locale ,
					showOn: 'both',
					dateFormat: 'dd/mm/yy',
					changeMonth : true,
					changeYear : true,
					yearRange : range,
					minDate: minDate,
					maxDate: maxDate,
					buttonImage: globalConfig.calendaImagePath,
					showButtonPanel: true,
					buttonImageOnly: true,
					beforeShow: function(input, inst){
						 $.datepicker._pos = $.datepicker._findPos(input); //this is the default position
						 $.datepicker._pos[0] = $.datepicker._pos[0] - 50;
					}
				}).blur(function(event) {UI_Passenger.fotmatDateInfantOnBlur(this);});
			}
			
			
			
			$("#adultList\\["+pl+"\\]\\.foidExpiry").datepicker({regional: SYS_IBECommonParam.locale ,
				showOn: 'both',
				dateFormat: 'dd/mm/yy',
				changeMonth : true,
				changeYear : true,
				minDate: psptExpiryMinDate,
				buttonImage: globalConfig.calendaImagePath,
				showButtonPanel: true,
				buttonImageOnly: true
			});
			$("#paxAdTemplate_" + pl + "  .psptDetails").hide();
			
			$("#adultList\\["+pl+"\\]\\.visaDocIssueDate").datepicker({regional: SYS_IBECommonParam.locale ,
				showOn: 'both',
				dateFormat: 'dd/mm/yy',
				changeMonth : true,
				changeYear : true,
				//minDate: psptExpiryMinDate,
				buttonImage: globalConfig.calendaImagePath,
				showButtonPanel: true,
				buttonImageOnly: true
			});
			
			if(paxConfigAD.passportNo.ibeVisibility ||
					paxConfigAD.passportExpiry.ibeVisibility ||
					paxConfigAD.passportIssuedCntry.ibeVisibility||
                    true){//TODO add paxConfigAD.lmsID.ibeVisibility after adding to the pax configs
				$("#paxAdTemplate_"+pl+" [name^='adultList']").focus(
						function(event){
							if(event.target.type.indexOf("select") > -1 && event.target.id.indexOf("foidPlace") > -1){//IE fixed
								return false;
							}
							if(event.target.type.indexOf("button") > -1 && event.target.id.indexOf("loadFamilyDetails") > -1){//IE fixed
								return false;
							}
							$("[id^=paxChTemplate_] .psptDetails").hide();
							$("[id^=paxAdTemplate_] .psptDetails").hide();
							$("[id^=paxInTemplate_] .psptDetails").hide();
							
							if(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id != ""){
								$("#" + this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id +" .psptDetails").show();
							} else {
								$("#" + this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id +" .psptDetails").show();
							}
							if(!UI_commonSystem.loyaltyManagmentEnabled) {
								$("[id^=paxAdTemplate_] .paxffid").parent().hide();
								$("[id^=paxAdTemplate_] .adFFID").parent().hide();
							}
						}
				);
			}
			
			if(!paxConfigAD.ffid.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.ffidTxt").parent().hide();
				$("#adultList\\["+pl+"\\]\\.lblFFidAd").parent().hide();
			}
			
			if(paxConfigAD.passportNo.ibeVisibility &&
					!paxConfigAD.passportNo.ibeMandatory){	
				$(".lblADPSPTMand").hide();
			} else if(!paxConfigAD.passportNo.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblPSPT").parent().hide();
				$("#adultList\\["+pl+"\\]\\.foidNumberTxt").parent().hide();
				$(".lblADPSPTMand").hide();
				$(".adPSPT").hide();
			}
			
			if(paxConfigAD.nationalIDNo.ibeVisibility && isRequestNICForReservationsHavingDomesticSegments){
				if(UI_Passenger.isDomesticSegmentExists == false){	
					$(".lblADTNationalIDNoMand").hide();
				}
			} else {
				if(paxConfigAD.nationalIDNo.ibeVisibility && !paxConfigAD.nationalIDNo.ibeMandatory){	
					$(".lblADTNationalIDNoMand").hide();
				} else if(!paxConfigAD.nationalIDNo.ibeVisibility) {
					$("#adultList\\["+pl+"\\]\\.lblPaxNationalIDNo").parent().hide();
					$("#adultList\\["+pl+"\\]\\.nationalIDNoTxt").parent().hide();
					$(".lblADNationalIDNoMand").hide();				
				}
			}			
			
			if(paxConfigAD.passportExpiry.ibeVisibility &&
					!paxConfigAD.passportExpiry.ibeMandatory){
				$(".lblADExpiryMand").hide();
			} else if(!paxConfigAD.passportExpiry.ibeVisibility){
				$("#adultList\\["+pl+"\\]\\.lblPSPTExpiry").parent().hide();
				$("#adultList\\["+pl+"\\]\\.foidExpiry").parent().hide();
				$(".lblADExpiryMand").hide();
				$(".adPSPTExpiry").hide();
			}
			
			if(paxConfigAD.passportIssuedCntry.ibeVisibility &&
					!paxConfigAD.passportIssuedCntry.ibeMandatory){
				$(".lblADIssuedCntryMand").hide();
			} else if(!paxConfigAD.passportIssuedCntry.ibeVisibility){
				$("#adultList\\["+pl+"\\]\\.lblPSPTPlace").parent().hide();
				$("#adultList\\["+pl+"\\]\\.foidPlace").parent().hide();
				$(".lblADIssuedCntryMand").hide();
				$(".adPSPTPlace").hide();
			}
			
			if(paxConfigAD.placeOfBirth.ibeVisibility &&
					!paxConfigAD.placeOfBirth.ibeMandatory){	
				$(".lblADPlaceOfBirthMand").hide();
			} else if(!paxConfigAD.placeOfBirth.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblPlaceOfBirth").parent().hide();
				$("#adultList\\["+pl+"\\]\\.placeOfBirth").parent().hide();
				$(".lblADPlaceOfBirthMand").hide();				
			}
			
			if(paxConfigAD.travelDocumentType.ibeVisibility &&
					!paxConfigAD.travelDocumentType.ibeMandatory){	
				$(".lblADTravelDocumentTypeMand").hide();
			} else if(!paxConfigAD.travelDocumentType.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblTravelDocumentType").parent().hide();
				$("#adultList\\["+pl+"\\]\\.travelDocumentType").parent().hide();
				$(".lblADTravelDocumentTypeMand").hide();				
			}
			
			if(paxConfigAD.visaDocNumber.ibeVisibility &&
					!paxConfigAD.visaDocNumber.ibeMandatory){	
				$(".lblADVisaDocNumberMand").hide();
			} else if(!paxConfigAD.visaDocNumber.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblVisaDocNumber").parent().hide();
				$("#adultList\\["+pl+"\\]\\.visaDocNumber").parent().hide();
				$(".lblADVisaDocNumberMand").hide();				
			}
			
			if(paxConfigAD.visaDocPlaceOfIssue.ibeVisibility &&
					!paxConfigAD.visaDocPlaceOfIssue.ibeMandatory){	
				$(".lblADVisaDocPlaceOfIssueMand").hide();
			} else if(!paxConfigAD.visaDocPlaceOfIssue.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblVisaDocPlaceOfIssue").parent().hide();
				$("#adultList\\["+pl+"\\]\\.visaDocPlaceOfIssue").parent().hide();
				$(".lblADVisaDocPlaceOfIssueMand").hide();				
			}
			
			if(paxConfigAD.visaDocIssueDate.ibeVisibility &&
					!paxConfigAD.visaDocIssueDate.ibeMandatory){	
				$(".lblADVisaDocIssueDateMand").hide();
			} else if(!paxConfigAD.visaDocIssueDate.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblVisaDocIssueDate").parent().hide();
				$("#adultList\\["+pl+"\\]\\.visaDocIssueDate").parent().hide();
				$(".lblADVisaDocIssueDateMand").hide();				
			}
			
			if(paxConfigAD.visaApplicableCountry.ibeVisibility &&
					!paxConfigAD.visaApplicableCountry.ibeMandatory){	
				$(".lblADVisaApplicableCountryMand").hide();
			} else if(!paxConfigAD.visaApplicableCountry.ibeVisibility) {
				$("#adultList\\["+pl+"\\]\\.lblVisaApplicableCountry").parent().hide();
				$("#adultList\\["+pl+"\\]\\.visaApplicableCountry").parent().hide();
				$(".lblADVisaApplicableCountryMand").hide();				
			}
			if (paxConfigAD.placeOfBirth.ibeMandatory
					|| paxConfigAD.passportIssuedCntry.ibeMandatory
					|| paxConfigAD.passportExpiry.ibeMandatory
					|| paxConfigAD.nationalIDNo.ibeMandatory
					|| paxConfigAD.passportNo.ibeMandatory) {
				$("#paxAdTemplate_" + pl + "  .psptDetails").show();
			}	
			if(paxConfigAD.visaApplicableCountry.ibeMandatory
					|| paxConfigAD.visaDocIssueDate.ibeMandatory
					|| paxConfigAD.visaDocPlaceOfIssue.ibeMandatory
					|| paxConfigAD.visaDocNumber.ibeMandatory
					|| paxConfigAD.travelDocumentType.ibeMandatory){
				$("#paxAdTemplate_" + pl + "  .docoDetails").show();
			}
			
			$("#adlGrid").show();
			
			$("#paxAdTemplate_" + pl + " .ButtonLargeMedium").val("Load Famliy Details");
			
			if(UI_Container.isRegisteredUser != true){
				$("#paxAdTemplate_" + pl +" .alignRight").hide()
			}

		}
		$("#adultList\\["+0+"\\]\\.title").change(function(event) { UI_Passenger.loadTitle(this.value);});
		$("#adultList\\["+0+"\\]\\.firstName").change(function(event) { UI_Passenger.loadFirstName(this.value);});
		$("#adultList\\["+0+"\\]\\.lastName").change(function(event) { UI_Passenger.loadLastName(this.value);});
		$("#adultList\\["+0+"\\]\\.nationality").change(function(event) { UI_Passenger.loadNationality(this.value); UI_Passenger.addNationality(this.value)});
		
		
		
	}
	$("#paxChTemplate").iterateTemplete({templeteName:"paxChTemplate", data:passengers.childList, dtoName:"childList"});
	if(passengers.childList.length == 0) {		
		$("#chdGrid").hide();
		$("#lblChildren").hide();
		$("#childCount").hide();
		
	}else {
		paxChild = passengers.childList;		
		/*var range = '-'+paxValidation.childAgeCutOverYears+':+'+paxValidation.childAgeCutOverYears;
		var minDate = '-'+paxValidation.childAgeCutOverYears+'y';
		var maxDate = '-'+(paxValidation.infantAgeCutOverYears -1)+'y';*/
		
		var minDate = new Date((yer-paxValidation.childAgeCutOverYears), mon-1, day);
		var maxDate = new Date((yer-paxValidation.infantAgeCutOverYears), mon-1, day-1);
		var range = '-' + paxValidation.childAgeCutOverYears + ':+' + paxValidation.childAgeCutOverYears ;
		
		for(var cl=0;cl < passengers.childList.length;cl++){
			if(!paxConfigCH.title.ibeVisibility){
				$("#tdCHTitle").hide();
				$("#childList\\["+pl+"\\]\\.chdTitle").parent().hide();
			} else {
				if(!paxConfigCH.title.ibeMandatory){
					$("#lblCHTitleMand").hide();
				}
				//$("#childList\\["+pl+"\\]\\.title").parent().empty();
				//$("#selTitle").fillDropDown({dataArray:arrPaxTitle, keyIndex:0, valueIndex:1, firstEmpty:true});
			}
			
			if(!paxConfigCH.firstName.ibeVisibility){
				$("#tdCHFName").hide();
				$("#childList\\["+pl+"\\]\\.firstName").parent().hide();
			} else {
				if(!paxConfigCH.firstName.ibeMandatory){
					$("#lblCHFNameMand").hide();
				}
			}
			
			if(!paxConfigCH.lastName.ibeVisibility){
				$("#tdCHLName").hide();
				$("#childList\\["+pl+"\\]\\.lastName").parent().hide();
			} else {
				if(!paxConfigCH.lastName.ibeMandatory){
					$("#lblCHLNameMand").hide();
				}
			}
			
			if(!paxConfigCH.nationality.ibeVisibility){
				$("#tdCHNationality").hide();
				$("#childList\\["+pl+"\\]\\.nationality").parent().hide();
			} else {
				if(!paxConfigCH.nationality.ibeMandatory){
					$("#lblCHNationalityMand").hide();
				}
			}
			
			if(!paxConfigCH.dob.ibeVisibility){
				$("#tdCHDOB").hide();
				$("#childList\\["+pl+"\\]\\.dateOfBirth").parent().hide();
			} else {
				if(!paxConfigCH.dob.ibeMandatory){
					$("#lblCHDOBMand").hide();
				}
				
				$("#childList\\["+cl+"\\]\\.dateOfBirth").datepicker({regional: SYS_IBECommonParam.locale ,
					showOn: 'both',
					changeMonth: true,
					dateFormat: 'dd/mm/yy',
					changeYear: true,
					yearRange : range,
					minDate : minDate,
					maxDate : maxDate,
					buttonImage: globalConfig.calendaImagePath,
					showButtonPanel: true,
					buttonImageOnly: true,
					beforeShow: function(input, inst){
						 $.datepicker._pos = $.datepicker._findPos(input); //this is the default position
						 $.datepicker._pos[0] = $.datepicker._pos[0] - 50;
					}
				}).blur(function(event) {UI_Passenger.fotmatDateInfantOnBlur(this);});
			}
						
			$("#childList\\["+cl+"\\]\\.foidExpiry").datepicker({regional: SYS_IBECommonParam.locale ,
				showOn: 'both',
				dateFormat: 'dd/mm/yy',
				changeMonth : true,
				changeYear : true,
				minDate: psptExpiryMinDate,
				buttonImage: globalConfig.calendaImagePath,
				showButtonPanel: true,
				buttonImageOnly: true
			});
			
			$("#childList\\["+cl+"\\]\\.visaDocIssueDate").datepicker({regional: SYS_IBECommonParam.locale ,
				showOn: 'both',
				dateFormat: 'dd/mm/yy',
				changeMonth : true,
				changeYear : true,
				//minDate: psptExpiryMinDate,
				buttonImage: globalConfig.calendaImagePath,
				showButtonPanel: true,
				buttonImageOnly: true
			});
			
			$("#paxChTemplate_" + cl + "  .psptDetails").hide();
			
			if(paxConfigCH.passportNo.ibeVisibility ||
					paxConfigCH.passportExpiry.ibeVisibility ||
					paxConfigCH.passportIssuedCntry.ibeVisibility||
                    true){//TODO add paxConfigAD.lmsID.ibeVisibility after adding to the pax configs
				$("#paxChTemplate_"+cl+" [name^='childList']").focus(
						function(event){
							if(event.target.type.indexOf("select") > -1 && event.target.id.indexOf("foidPlace") > -1){//IE fixed
								return false;
							}
							$("[id^=paxAdTemplate_] .psptDetails").hide();
							$("[id^=paxChTemplate_] .psptDetails").hide();
							$("[id^=paxInTemplate_] .psptDetails").hide();
							
							if(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id != ""){
								$("#" + this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id +" .psptDetails").show();
							} else {
								$("#" + this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id +" .psptDetails").show();
							}			

							
							if(!UI_commonSystem.loyaltyManagmentEnabled) {
								$("[id^=paxChTemplate_] .paxffid").parent().hide();
								$("[id^=paxChTemplate_] .chFFID").parent().hide();
							}
						}
				);
			}
			
			if(!paxConfigCH.ffid.ibeVisibility) {
				$("#childList\\["+cl+"\\]\\.ffidTxt").parent().hide();
				$("#childList\\["+cl+"\\]\\.lblFFidCh").parent().hide();
			}
			
			if(paxConfigCH.passportNo.ibeVisibility &&
					!paxConfigCH.passportNo.ibeMandatory){	
				$(".lblCHPSPTMand").hide();
			} else if(!paxConfigCH.passportNo.ibeVisibility) {
				$("#childList\\["+cl+"\\]\\.lblPSPT").parent().hide();
				$("#childList\\["+cl+"\\]\\.foidNumberTxt").parent().hide();
				$(".lblCHPSPTMand").hide();
				$(".chPSPT").hide();
			}
			
			if(paxConfigCH.nationalIDNo.ibeVisibility && isRequestNICForReservationsHavingDomesticSegments){
				if(UI_Passenger.isDomesticSegmentExists == false){	
					$(".lblCHNationalIDNoMand").hide();
				}
			} else {
				if(paxConfigCH.nationalIDNo.ibeVisibility && !paxConfigCH.nationalIDNo.ibeMandatory){	
					$(".lblCHNationalIDNoMand").hide();
				} else if(!paxConfigCH.nationalIDNo.ibeVisibility) {
					$("#childList\\["+cl+"\\]\\.lblPaxNationalIDNo").parent().hide();
					$("#childList\\["+cl+"\\]\\.nationalIDNoTxt").parent().hide();
					$(".lblCHNationalIDNoMand").hide();				
				}
			}	
			
			if(paxConfigCH.passportExpiry.ibeVisibility &&
					!paxConfigCH.passportExpiry.ibeMandatory){
				$(".lblCHExpiryMand").hide();
			} else if(!paxConfigCH.passportExpiry.ibeVisibility){
				$("#childList\\["+cl+"\\]\\.lblPSPTExpiry").parent().hide();
				$("#childList\\["+cl+"\\]\\.foidExpiry").parent().hide();
				$(".lblCHExpiryMand").hide();
				$(".chPSPTExpiry").hide();
			}
			
			if(paxConfigCH.passportIssuedCntry.ibeVisibility &&
					!paxConfigCH.passportIssuedCntry.ibeMandatory){
				$(".lblCHIssuedCntryMand").hide();
			} else if(!paxConfigCH.passportIssuedCntry.ibeVisibility){
				$("#childList\\["+cl+"\\]\\.lblPSPTPlace").parent().hide();
				$("#childList\\["+cl+"\\]\\.foidPlace").parent().hide();
				$(".lblCHIssuedCntryMand").hide();
				$(".chPSPTPlace").hide();
			}
			
			if(paxConfigCH.placeOfBirth.ibeVisibility &&
					!paxConfigCH.placeOfBirth.ibeMandatory){
				$(".lblCHPlaceOfBirthMand").hide();
			} else if(!paxConfigCH.placeOfBirth.ibeVisibility) {
				$("#childList\\["+cl+"\\]\\.lblPlaceOfBirth").parent().hide();
				$("#childList\\["+cl+"\\]\\.placeOfBirth").parent().hide();
				$(".lblCHPlaceOfBirthMand").hide();
			}
			
			if(paxConfigCH.travelDocumentType.ibeVisibility &&
					!paxConfigCH.travelDocumentType.ibeMandatory){
				$(".lblCHTravelDocumentTypeMand").hide();
			} else if(!paxConfigCH.travelDocumentType.ibeVisibility){
				$("#childList\\["+cl+"\\]\\.lblTravelDocumentType").parent().hide();
				$("#childList\\["+cl+"\\]\\.travelDocumentType").parent().hide();
				$(".lblCHTravelDocumentTypeMand").hide();
			}
			
			if(paxConfigCH.visaDocNumber.ibeVisibility &&
					!paxConfigCH.visaDocNumber.ibeMandatory){
				$(".lblCHVisaDocNumberMand").hide();
			} else if(!paxConfigCH.visaDocNumber.ibeVisibility){
				$("#childList\\["+cl+"\\]\\.lblVisaDocNumber").parent().hide();
				$("#childList\\["+cl+"\\]\\.visaDocNumber").parent().hide();
				$(".lblCHVisaDocNumberMand").hide();
			}
			
			if(paxConfigCH.visaDocPlaceOfIssue.ibeVisibility &&
					!paxConfigCH.visaDocPlaceOfIssue.ibeMandatory){
				$(".lblCHVisaDocPlaceOfIssueMand").hide();
			} else if(!paxConfigCH.visaDocPlaceOfIssue.ibeVisibility) {
				$("#childList\\["+cl+"\\]\\.lblVisaDocPlaceOfIssue").parent().hide();
				$("#childList\\["+cl+"\\]\\.visaDocPlaceOfIssue").parent().hide();
				$(".lblCHVisaDocPlaceOfIssueMand").hide();
			}
			
			if(paxConfigCH.visaDocIssueDate.ibeVisibility &&
					!paxConfigCH.visaDocIssueDate.ibeMandatory){
				$(".lblCHVisaDocIssueDateMand").hide();
			} else if(!paxConfigCH.visaDocIssueDate.ibeVisibility){
				$("#childList\\["+cl+"\\]\\.lblVisaDocIssueDate").parent().hide();
				$("#childList\\["+cl+"\\]\\.visaDocIssueDate").parent().hide();
				$(".lblCHVisaDocIssueDateMand").hide();
			}
			
			if(paxConfigCH.visaApplicableCountry.ibeVisibility &&
					!paxConfigCH.visaApplicableCountry.ibeMandatory){
				$(".lblCHVisaApplicableCountryMand").hide();
			} else if(!paxConfigCH.visaApplicableCountry.ibeVisibility){
				$("#childList\\["+cl+"\\]\\.lblVisaApplicableCountry").parent().hide();
				$("#childList\\["+cl+"\\]\\.visaApplicableCountry").parent().hide();
				$(".lblCHVisaApplicableCountryMand").hide();
			}
			
$("#paxChTemplate_" + cl + " .ButtonLargeMedium").val("Load Famliy Details");
			if(UI_Container.isRegisteredUser != true){
				$("#paxChTemplate_" + cl +" .alignRight").hide()
			}

			if (paxConfigCH.placeOfBirth.ibeMandatory
					|| paxConfigCH.passportIssuedCntry.ibeMandatory
					|| paxConfigCH.passportExpiry.ibeMandatory
					|| paxConfigCH.nationalIDNo.ibeMandatory
					|| paxConfigCH.passportNo.ibeMandatory) {
				$("#paxChTemplate_" + cl + "  .psptDetails").show();
			}	
			if(paxConfigCH.visaApplicableCountry.ibeMandatory
					|| paxConfigCH.visaDocIssueDate.ibeMandatory
					|| paxConfigCH.visaDocPlaceOfIssue.ibeMandatory
					|| paxConfigCH.visaDocNumber.ibeMandatory
					|| paxConfigCH.travelDocumentType.ibeMandatory){
				$("#paxAChTemplate_" + cl + "  .docoDetails").show();
			}
			
		}
		if(passengers.adultList.length == 0){
			$("#childList\\["+0+"\\]\\.title").change(function(event) { UI_Passenger.loadTitle(this.value);});
			$("#childList\\["+0+"\\]\\.firstName").change(function(event) { UI_Passenger.loadFirstName(this.value);});
			$("#childList\\["+0+"\\]\\.lastName").change(function(event) { UI_Passenger.loadLastName(this.value);});
			$("#childList\\["+0+"\\]\\.nationality").change(function(event) { UI_Passenger.loadNationality(this.value);});
			
		}
		$("#chdGrid").show();
		
	}
	$("#paxInTemplate").iterateTemplete({templeteName:"paxInTemplate", data:passengers.infantList, dtoName:"infantList"});
	if(passengers.infantList.length == 0) {
		$("#infGrid").hide();
		$("#lblInfants").hide();
		$("#infantCount").hide();
		
	}else {
		paxInfants = passengers.infantList;		
		/*var range = '-'+paxValidation.infantAgeCutOverYears+':+'+paxValidation.infantAgeCutOverYears;
		var minDate = '-'+paxValidation.infantAgeCutOverYears+'y';
		var maxDate = '-'+paxValidation.infantAgeCutLowerBoundaryDays+'d';*/
		
		var minDate = new Date((yer-paxValidation.infantAgeCutOverYears), mon-1, day);
		var departureDate = UI_Passenger.extractFlightLastSegDateObj();
		var maxDate;
		var maxDateByDepartureDate= new Date(departureDate.getTime() - paxValidation.infantAgeCutLowerBoundaryDays * 24 * 60 * 60 * 1000);
		var currentSysDate = new Date(curYer,curMon-1,curDay);
		if(maxDateByDepartureDate < currentSysDate){
			maxDate = maxDateByDepartureDate;
		}else{
			maxDate = currentSysDate;
		}
		var range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;		
		
		for(var il=0;il < passengers.infantList.length;il++){
			if(!paxConfigIN.title.ibeVisibility){				
				$("#infTitleHeader").hide();
				$("#tdInfTitle").hide();
				$("#infantList\\["+il+"\\]\\.infTitle").parent().hide();
			} else {
				if(!paxConfigIN.title.ibeMandatory){
					$("#lblInfTitleMand").hide();
				}
			}
			
			if(!paxConfigIN.firstName.ibeVisibility){
				$("#tdINFName").hide();
				$("#infantList\\["+pl+"\\]\\.firstName").parent().hide();
			} else {
				if(!paxConfigIN.firstName.ibeMandatory){
					$("#lblINFNameMand").hide();
				}
			}
			
			if(!paxConfigIN.lastName.ibeVisibility){
				$("#tdINLName").hide();
				$("#infantList\\["+pl+"\\]\\.lastName").parent().hide();
			} else {
				if(!paxConfigIN.lastName.ibeMandatory){
					$("#lblINLNameMand").hide();
				}
			}
			
			if(!paxConfigIN.travelWith.ibeVisibility){
				$("#tdINTravelWith").hide();
				$("#infantList\\["+pl+"\\]\\.travelWith").parent().hide();
			} else {
				if(!paxConfigIN.travelWith.ibeMandatory){
					$("#lblINTravelWithMand").hide();
				}
			}
			
			if(!paxConfigIN.dob.ibeVisibility){
				$("#tdINDOB").hide();
				$("#infantList\\["+pl+"\\]\\.dateOfBirth").parent().hide();
			} else {
				if(!paxConfigIN.dob.ibeMandatory){
					$("#lblINDOBMand").hide();
				}
				
				$("#infantList\\["+il+"\\]\\.dateOfBirth").datepicker({regional: SYS_IBECommonParam.locale ,
					showOn: 'both',
					changeMonth: true,
					dateFormat: 'dd/mm/yy',
					maxDate: 0,
					changeYear: true,
					yearRange : range,
					minDate: minDate,
					maxDate: maxDate,
					buttonImage: globalConfig.calendaImagePath,
					showButtonPanel: true,
					buttonImageOnly: true
				}).blur(function(event) {UI_Passenger.fotmatDateInfantOnBlur(this);});
			}
			
			$("#infantList\\["+il+"\\]\\.foidExpiry").datepicker({regional: SYS_IBECommonParam.locale ,
				showOn: 'both',
				dateFormat: 'dd/mm/yy',
				changeMonth : true,
				changeYear : true,
				minDate: psptExpiryMinDate,
				buttonImage: globalConfig.calendaImagePath,
				showButtonPanel: true,
				buttonImageOnly: true
			});
			
			$("#infantList\\["+il+"\\]\\.visaDocIssueDate").datepicker({regional: SYS_IBECommonParam.locale ,
				showOn: 'both',
				dateFormat: 'dd/mm/yy',
				changeMonth : true,
				changeYear : true,
				//minDate: psptExpiryMinDate,
				buttonImage: globalConfig.calendaImagePath,
				showButtonPanel: true,
				buttonImageOnly: true
			});
			
			$("#paxInTemplate_" + il + "  .psptDetails").hide();
			
			if(paxConfigIN.passportNo.ibeVisibility ||
					paxConfigIN.passportExpiry.ibeVisibility ||
					paxConfigIN.passportIssuedCntry.ibeVisibility){
				$("#paxInTemplate_"+il+" [name^='infantList']").focus(
						function(event){
							$("[id^=paxAdTemplate_] .psptDetails").hide();
							$("[id^=paxChTemplate_] .psptDetails").hide();
							$("[id^=paxInTemplate_] .psptDetails").hide();
							
							if(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id != ""){
								$("#" + this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id +" .psptDetails").show();
							} else {
								$("#" + this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id +" .psptDetails").show();
							}

							if(!UI_commonSystem.loyaltyManagmentEnabled) {
								$("[id^=paxChTemplate_] .paxffid").parent().hide();
								$("[id^=paxChTemplate_] .chFFID").parent().hide();
							}
						}
				);
			}
			
			if(paxConfigIN.passportNo.ibeVisibility &&
					!paxConfigIN.passportNo.ibeMandatory){
				$(".lblINPSPTMand").hide();
			} else if(!paxConfigIN.passportNo.ibeVisibility) {
				$("#infantList\\["+il+"\\]\\.lblPSPT").parent().hide();
				$("#infantList\\["+il+"\\]\\.foidNumberTxt").parent().hide();
				$(".lblINPSPTMand").hide();
				$(".inPSPT").hide();
			}
			
			if(paxConfigIN.nationalIDNo.ibeVisibility &&
					(!paxConfigIN.nationalIDNo.ibeMandatory && 
					!(isRequestNICForReservationsHavingDomesticSegments && UI_Passenger.isDomesticSegmentExists == true))){	
				$(".lblINNationalIDNoMand").hide();
			} else if(!paxConfigIN.nationalIDNo.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblPaxNationalIDNo").parent().hide();
				$("#infantList\\["+il+"\\]\\.nationalIDNoTxt").parent().hide();
				$(".lblINNationalIDNoMand").hide();
			}
			
			if(paxConfigIN.passportExpiry.ibeVisibility &&
					!paxConfigIN.passportExpiry.ibeMandatory){
				$(".lblINExpiryMand").hide();
			} else if(!paxConfigIN.passportExpiry.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblPSPTExpiry").parent().hide();
				$("#infantList\\["+il+"\\]\\.foidExpiry").parent().hide();
				$(".lblINExpiryMand").hide();
				$(".inPSPTExpiry").hide();
			}
			
			if(paxConfigIN.passportIssuedCntry.ibeVisibility &&
					!paxConfigIN.passportIssuedCntry.ibeMandatory){
				$(".lblINIssuedCntryMand").hide();
			} else if(!paxConfigIN.passportIssuedCntry.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblPSPTPlace").parent().hide();
				$("#infantList\\["+il+"\\]\\.foidPlace").parent().hide();
				$(".lblINIssuedCntryMand").hide();
				$(".inPSPTPlace").hide();
			}
			
			if(paxConfigIN.placeOfBirth.ibeVisibility &&
					!paxConfigIN.placeOfBirth.ibeMandatory){
				$(".lblINPlaceOfBirthMand").hide();
			} else if(!paxConfigIN.placeOfBirth.ibeVisibility) {
				$("#infantList\\["+il+"\\]\\.lblPlaceOfBirth").parent().hide();
				$("#infantList\\["+il+"\\]\\.placeOfBirth").parent().hide();
				$(".lblINPlaceOfBirthMand").hide();
			}
			
			if(paxConfigIN.travelDocumentType.ibeVisibility &&
					!paxConfigIN.travelDocumentType.ibeMandatory){
				$(".lblINTravelDocumentTypeMand").hide();
			} else if(!paxConfigIN.travelDocumentType.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblTravelDocumentType").parent().hide();
				$("#infantList\\["+il+"\\]\\.travelDocumentType").parent().hide();
				$(".lblINTravelDocumentTypeMand").hide();
			}
			
			if(paxConfigIN.visaDocNumber.ibeVisibility &&
					!paxConfigIN.visaDocNumber.ibeMandatory){
				$(".lblINVisaDocNumberMand").hide();
			} else if(!paxConfigIN.visaDocNumber.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblVisaDocNumber").parent().hide();
				$("#infantList\\["+il+"\\]\\.visaDocNumber").parent().hide();
				$(".lblINVisaDocNumberMand").hide();
			}
			
			if(paxConfigIN.visaDocPlaceOfIssue.ibeVisibility &&
					!paxConfigIN.visaDocPlaceOfIssue.ibeMandatory){
				$(".lblINVisaDocPlaceOfIssueMand").hide();
			} else if(!paxConfigIN.visaDocPlaceOfIssue.ibeVisibility) {
				$("#infantList\\["+il+"\\]\\.lblVisaDocPlaceOfIssue").parent().hide();
				$("#infantList\\["+il+"\\]\\.visaDocPlaceOfIssue").parent().hide();
				$(".lblINVisaDocPlaceOfIssueMand").hide();
			}
			
			if(paxConfigIN.visaDocIssueDate.ibeVisibility &&
					!paxConfigIN.visaDocIssueDate.ibeMandatory){
				$(".lblINVisaDocIssueDateMand").hide();
			} else if(!paxConfigIN.visaDocIssueDate.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblVisaDocIssueDate").parent().hide();
				$("#infantList\\["+il+"\\]\\.visaDocIssueDate").parent().hide();
				$(".lblINVisaDocIssueDateMand").hide();
			}
			
			if(paxConfigIN.visaApplicableCountry.ibeVisibility &&
					!paxConfigIN.visaApplicableCountry.ibeMandatory){
				$(".lblINVisaApplicableCountryMand").hide();
			} else if(!paxConfigIN.visaApplicableCountry.ibeVisibility){
				$("#infantList\\["+il+"\\]\\.lblVisaApplicableCountry").parent().hide();
				$("#infantList\\["+il+"\\]\\.visaApplicableCountry").parent().hide();
				$(".lblINVisaApplicableCountryMand").hide();
			}

			// merge conflict from master_Vietjet
			$("#paxInTemplate_" + il + " .ButtonLargeMedium").val("Load Famliy Details");
			if(UI_Container.isRegisteredUser != true){
				$("#paxInTemplate_" +il +" .alignRight").hide()
			}

			if (paxConfigIN.placeOfBirth.ibeMandatory
					|| paxConfigIN.passportIssuedCntry.ibeMandatory
					|| paxConfigIN.passportExpiry.ibeMandatory
					|| paxConfigIN.nationalIDNo.ibeMandatory
					|| paxConfigIN.passportNo.ibeMandatory) {
				$("#paxInTemplate_" + il + "  .psptDetails").show();
			}	
			if(paxConfigIN.visaApplicableCountry.ibeMandatory
					|| paxConfigIN.visaDocIssueDate.ibeMandatory
					|| paxConfigIN.visaDocPlaceOfIssue.ibeMandatory
					|| paxConfigIN.visaDocNumber.ibeMandatory
					|| paxConfigIN.travelDocumentType.ibeMandatory){
				$("#paxInTemplate_" + il + "  .docoDetails").show();
			}
			
		}
			
		if(passengers.adultList.length == 0){
			$("#infantList\\["+0+"\\]\\.title").change(function(event) { UI_Passenger.loadTitle(this.value);});
			$("#infantList\\["+0+"\\]\\.firstName").change(function(event) { UI_Passenger.loadFirstName(this.value);});
			$("#infantList\\["+0+"\\]\\.lastName").change(function(event) { UI_Passenger.loadLastName(this.value);});
			$("#infantList\\["+0+"\\]\\.nationality").change(function(event) { UI_Passenger.loadNationality(this.value);});
			
		}
		$("#infGrid").show();
	}
    $(".paxffid").off("change").on("change", UI_Passenger.displayLmsMsg);
    $(".loadFamilyDetails").unbind().click(function(event){UI_Passenger.loadFamilyDetailsOnClick(event);});
}

UI_Passenger.showHidePspt = function(id, val){
	if(val){
		$(id).show();
	} else {
		$(id).hide();
	}
}

UI_Passenger.loadCalendar = function (strID, objEvent, strPaxType, currentDate){	
	UI_commonSystem.pageOnChange();
	var stInd = strID.indexOf("[");
	var stIndout = strID.indexOf("]");	
	var clId = strID.substr(stInd+1, (stIndout- (stInd+1)));
	if(strPaxType == "INF") {
		UI_Passenger.objCal1.ID = strID;
		UI_Passenger.objCal1.top =0;
		UI_Passenger.objCal1.left = 0;
		UI_Passenger.objCal1.onClick = "setDateForInf";
		UI_Passenger.objCal1.currentDate = currentDate;
		if ($("#infantList\\["+clId+"\\]\\.dateOfBirth").val() != ""){
			UI_Passenger.objCal3.currentDate = $("#infantList\\["+clId+"\\]\\.dateOfBirth").val();
		}
		
		UI_Passenger.objCal1.showCalendar(objEvent);	
	}
	if(strPaxType == "CHD") {	
		UI_Passenger.objCal2.ID = strID;
		UI_Passenger.objCal2.top = 0;
		UI_Passenger.objCal2.left = 0;
		UI_Passenger.objCal2.onClick = "setDateForCh";
		UI_Passenger.objCal2.currentDate = currentDate;
		if ($("#childList\\["+clId+"\\]\\.dateOfBirth").val() != ""){
			UI_Passenger.objCal2.currentDate = $("#childList\\["+clId+"\\]\\.dateOfBirth").val();
		}		
		UI_Passenger.objCal2.showCalendar(objEvent);	
	}
	if(strPaxType == "ADT") {
		UI_Passenger.objCal3.ID = strID;
		UI_Passenger.objCal3.top = 0;
		UI_Passenger.objCal3.left = 0;
		UI_Passenger.objCal3.onClick = "setDateForAd";
		UI_Passenger.objCal3.currentDate = currentDate;
		if ($("#adultList\\["+clId+"\\]\\.dateOfBirth").val() != ""){
			UI_Passenger.objCal1.currentDate = $("#adultList\\["+clId+"\\]\\.dateOfBirth").val();
		}	
		UI_Passenger.objCal3.showCalendar(objEvent);
	}
	
}

UI_Passenger.loadTitle = function(title) {
	UI_commonSystem.pageOnChange();
	if($("#selTitle").val() == ""){
		$("#selTitle").val(title);
	}
	
}

UI_Passenger.loadFirstName = function(name) {
	UI_commonSystem.pageOnChange();
	if($("#txtFName").val() == ""){
		$("#txtFName").val(name);
	}
	
}

UI_Passenger.loadLastName = function(name) {
	UI_commonSystem.pageOnChange();
	if($("#txtLName").val() == ""){
		$("#txtLName").val(name);
	}
}

UI_Passenger.loadNationality = function(nationality) {
	UI_commonSystem.pageOnChange();
	var isChildList = $("#childList\\["+0+"\\]\\.nationality option").length;
	var isAdultList = $("#adultList\\["+0+"\\]\\.nationality option").length;
	
	if (nationality != "" || (isAdultList == 0) || (isAdultList != 0  && isChildList== 0 )) {
		if (!UI_Passenger.isContactNationalityModified){
			$("#selNationality").val(nationality);
		}
	}
	
}

function setDateForInf (strDate, strID){
	document.getElementById(strID.replace("imageId","dateOfBirth")).value = strDate;	
}

function setDateForCh (strDate, strID){
	document.getElementById(strID.replace("imageId","dateOfBirth")).value = strDate;		
}
	
function setDateForAd(strDate, strID){	
	document.getElementById(strID.replace("imageId","dateOfBirth")).value = strDate;	
}

UI_Passenger.paxValidat = function (navCallBack) {	
	
	//Doing it in old style need to improve in 2nd phase
	 $("select").removeClass("errorControl");
	 $("input").removeClass("errorControl");	 
	 var strErrorHTML = "";
	 var isNavExc = true;
	 
	 foidLists = "";
	 
	//------------------------------------------------
	 
	 // Pax Grid Validate	 
	 if(paxAdults != null && paxAdults.length >0) {
		 strErrorHTML += UI_Passenger.adultValidate(paxAdults, "ADT");
	 }	 
	 if(paxChild != null && paxChild.length >0) {
		 strErrorHTML += UI_Passenger.adultValidate(paxChild, "CHD");
	 }
	 if(paxInfants != null && paxInfants.length >0) {
		 strErrorHTML += UI_Passenger.adultValidate(paxInfants, "INF");
	 }
	 
	 if(strErrorHTML.length > 0){
		 isNavExc = false;
	 }
	 
	 if ( foidLists!= "" ) {
		var isDuplicate = UI_Passenger.validatefoidIds(foidLists);
		if(isDuplicate !="" && isDuplicate == "true") {
			strErrorHTML += "<li> " + raiseError("ERR071");
			isNavExc = false;
		}
	 }	  
	
	// ---------------- Contact information validation -----------------------
	if(contactConfig.title1.ibeVisibility  && contactConfig.title1.mandatory){
		if (trim(getValue("selTitle")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR042");
			$("#selTitle").addClass("errorControl");	
			isNavExc = false;
		}
	}
	
	if(contactConfig.firstName1.ibeVisibility && contactConfig.firstName1.mandatory){
		if (trim(getValue("txtFName")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR028");
			$("#txtFName").addClass("errorControl");
			isNavExc = false;
		} else if(!isAlphaWhiteSpace(trim(getValue("txtFName")))){
			strErrorHTML += "<li> " + raiseError("VFNAME");
			$("#txtFName").addClass("errorControl");
			isNavExc = false;
		}
	} else if(contactConfig.firstName1.ibeVisibility &&
			!contactConfig.firstName1.mandatory
			&& trim(getValue("txtFName")) != ""){
		if(!isAlphaWhiteSpace(trim(getValue("txtFName")))){
			strErrorHTML += "<li> " + raiseError("VFNAME");
			$("#txtFName").addClass("errorControl");
			isNavExc = false;
		}
	}
	
	if(contactConfig.lastName1.ibeVisibility && contactConfig.lastName1.mandatory){
		if (trim(getValue("txtLName")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR029");
			$("#txtLName").addClass("errorControl");			
			isNavExc = false;
		} else if(!isAlphaWhiteSpace(trim(getValue("txtLName")))){
			strErrorHTML += "<li> " + raiseError("VLNAME");
			$("#txtLName").addClass("errorControl");
			isNavExc = false;
		}
	} else if(contactConfig.lastName1.ibeVisibility &&
			!contactConfig.lastName1.mandatory
			&& trim(getValue("txtLName")) != ""){
		if(!isAlphaWhiteSpace(trim(getValue("txtLName")))){
			strErrorHTML += "<li> " + raiseError("VLNAME");
			$("#txtLName").addClass("errorControl");	
			isNavExc = false;
		}
	}
	
	if(contactConfig.address1.ibeVisibility && contactConfig.address1.mandatory){
		if(trim(getValue("txtStreet")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR029");
			$("#txtStreet").addClass("errorControl");
			isNavExc = false;
		}
	}
	
	if(contactConfig.city1.ibeVisibility && contactConfig.city1.mandatory){
		if (trim(getValue("txtCity")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR049");
			$("#txtCity").addClass("errorControl");		
			isNavExc = false;
		//} else if(!isAlphaWhiteSpace(trim(getValue("txtCity")))){//Update user profile is allowing to enter Alpha Numeric  
			//strErrorHTML += "<li> " + raiseError("VCITY");
			//$("#txtCity").addClass("errorControl");			
		}
	}
	
	if(contactConfig.nationality1.ibeVisibility && contactConfig.nationality1.mandatory){
		if (trim(getValue("selNationality")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR048");
			$("#selNationality").addClass("errorControl");	
			isNavExc = false;
		} 
	}
	
	if(contactConfig.country1.ibeVisibility && contactConfig.country1.mandatory){
		if (trim(getValue("selCountry")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR041");
			$("#selCountry").addClass("errorControl");	
			isNavExc = false;
		}
	}
	
	if(contactConfig.zipCode1.ibeVisibility && contactConfig.zipCode1.mandatory){
		if (trim(getValue("txtZipCode")) == ""){
			strErrorHTML += "<li> " + raiseError("ERR063");
			$("#txtZipCode").addClass("errorControl");		
			isNavExc = false;
		} else if(!validateZipCode(trim(getValue("txtZipCode")))){
			strErrorHTML += "<li> " + raiseError("ERR064");
			$("#txtZipCode").addClass("errorControl");
			isNavExc = false;
		}
	} else if(contactConfig.zipCode1.ibeVisibility && !contactConfig.zipCode1.mandatory &&
			trim(getValue("txtZipCode"))!=""){
		if(!validateZipCode(trim(getValue("txtZipCode")))){
			strErrorHTML += "<li> " + raiseError("ERR064");
			$("#txtZipCode").addClass("errorControl");
			isNavExc = false;
		}
	}
	
	var phoneErr = UI_Passenger.TelePhoneValidate();
	
	if(phoneErr != ""){
		strErrorHTML += phoneErr;
	}else{
		if($("#hdnQiwiMobileNumber").length){
			$("#hdnQiwiMobileNumber").val("+" + $("#txtMCountry").val() + $("#txtMobile").val());
		}
		if($("#hdnPayFortMobileNumber").length){
			$("#hdnPayFortMobileNumber").val("+" + $("#txtMCountry").val() + $("#txtMobile").val());
		}
		if($("#hdnPayFortEmail").length){
			$("#hdnPayFortEmail").val($("#txtEmail").val());
		}
		if($("#hdnPGWPaymentMobileNumber").length){
			$("#hdnPGWPaymentMobileNumber").val("+" + $("#txtMCountry").val() + $("#txtMobile").val());
		}
		if($("#hdnPGWPaymentEmail").length){
			$("#hdnPGWPaymentEmail").val($("#txtEmail").val());
		}
		if($("#hdnPGWPaymentCustomerName").length){
			$("#hdnPGWPaymentCustomerName").val($('#txtFName').val() + " " + $('#txtLName').val());
		}
	}		
	
	if(!UI_Passenger.boolValidate) isNavExc = UI_Passenger.boolValidate;
	
	if(contactConfig.email1.ibeVisibility && contactConfig.email1.mandatory){
		if ($("#txtEmail").val() == ""){
			strErrorHTML += "<li> " + raiseError("ERR031");
			$("#txtEmail").addClass("errorControl");
			isNavExc = false;
		}
		
		
		if ($("#txtEmail").val() != ""){
			if (!checkEmail($.trim($("#txtEmail").val()))){
				strErrorHTML += "<li> " + raiseError("ERR032");
				$("#txtEmail").addClass("errorControl");	
				isNavExc = false;
			}else if ($("#txtEmail").val().toLowerCase() != $("#txtVerifyEmail").val().toLowerCase()){
				strErrorHTML += "<li> " + raiseError("ERR033");
				$("#txtEmail").addClass("errorControl");	
				$("#txtVerifyEmail").addClass("errorControl");
				isNavExc = false;
			} else if(validateEmailDomain){
				var emailErr = UI_Passenger.checkEmailDomain($("#txtEmail"), 1, strErrorHTML);
				if(emailErr.length > 0){
					isNavExc = false;
					strErrorHTML += emailErr;
				}
			} 
		}
	} else if(contactConfig.email1.ibeVisibility &&
			!contactConfig.email1.mandatory &&
			(trim($("#txtEmail").val()) != "" || trim($("#txtVerifyEmail").val()) != "")){
		if (!checkEmail($("#txtEmail").val())){
			strErrorHTML += "<li> " + raiseError("ERR032");
			$("#txtEmail").addClass("errorControl");
			isNavExc = false;
		}else if ($("#txtEmail").val().toLowerCase() != $("#txtVerifyEmail").val().toLowerCase()){
			strErrorHTML += "<li> " + raiseError("ERR033");
			$("#txtEmail").addClass("errorControl");	
			$("#txtVerifyEmail").addClass("errorControl");
			isNavExc = false;
		}else if(validateEmailDomain){
			var emailErr = UI_Passenger.checkEmailDomain($("#txtEmail"), 1, strErrorHTML);
			if(emailErr.length > 0){
				isNavExc = false;
				strErrorHTML += emailErr;
			}
		}
	} else if(trim($("#txtEmail").val()) != "" &&
			trim($("#txtVerifyEmail").val()) == "" &&
			!UI_Container.isRegisteredUser){
		$("#txtVerifyEmail").addClass("errorControl");
		strErrorHTML += "<li> " + raiseError("ERR033");
		isNavExc = false;
	}
	
	var groupErrHTML = groupFieldValidation(validationGroup, jsonFieldToElementMapping, jsonGroupValidation);
	
	if(groupErrHTML != ""){
		strErrorHTML += groupErrHTML;
		isNavExc = false;
	}
	
	if(phoneErr == "" && groupErrHTML == ""){
		if(trim($("#txtMobile").val()) == ""){
			$("#txtMCountry").val("");
			$("#txtMArea").val("");
		}
		
		if(trim($("#txtPhone").val()) == ""){
			$("#txtPCountry").val("");
			$("#txtPArea").val("");
		}
		
		if(trim($("#txtFax").val()) == ""){
			$("#txtFCountry").val("");
			$("#txtFArea").val("");
		}
	}
	
	//-------------------------Emergency Contact information validation -------------------------------
	
	if(contactConfig.firstName2.ibeVisibility ||
			contactConfig.lastName2.ibeVisibility ||
			contactConfig.phoneNo2.ibeVisibility){
		
		if(contactConfig.title2.ibeVisibility && contactConfig.title2.mandatory){
			if (trim(getValue("selEmgnTitle")) == ""){
				strErrorHTML += "<li> " + raiseError("ERR072");
				$("#selEmgnTitle").addClass("errorControl");	
				isNavExc = false;
			}
		}
		
		if(contactConfig.firstName2.ibeVisibility && contactConfig.firstName2.mandatory){
			if (trim(getValue("txtEmgnFName")) == ""){
				strErrorHTML += "<li> " + raiseError("ERR073");
				$("#txtEmgnFName").addClass("errorControl");
				isNavExc = false;
			} else if(!isAlphaWhiteSpace(trim(getValue("txtEmgnFName")))){
				strErrorHTML += "<li> " + raiseError("ERR074");
				$("#txtEmgnFName").addClass("errorControl");
				isNavExc = false;
			}
		} else if(contactConfig.firstName2.ibeVisibility &&
				!contactConfig.firstName2.mandatory
				&& trim(getValue("txtEmgnFName")) != ""){
			if(!isAlphaWhiteSpace(trim(getValue("txtEmgnFName")))){
				strErrorHTML += "<li> " + raiseError("ERR074");
				$("#txtEmgnFName").addClass("errorControl");
				isNavExc = false;
			}
		}
		
		if(contactConfig.lastName2.ibeVisibility && contactConfig.lastName2.mandatory){
			if (trim(getValue("txtEmgnLName")) == ""){
				strErrorHTML += "<li> " + raiseError("ERR075");
				$("#txtEmgnLName").addClass("errorControl");	
				isNavExc = false;
			} else if(!isAlphaWhiteSpace(trim(getValue("txtEmgnLName")))){
				strErrorHTML += "<li> " + raiseError("ERR076");
				$("#txtEmgnLName").addClass("errorControl");
				isNavExc = false;
			}
		} else if(contactConfig.lastName2.ibeVisibility &&
				!contactConfig.lastName2.mandatory
				&& trim(getValue("txtEmgnLName")) != ""){
			if(!isAlphaWhiteSpace(trim(getValue("txtEmgnLName")))){
				strErrorHTML += "<li> " + raiseError("ERR076");
				$("#txtEmgnLName").addClass("errorControl");
				isNavExc = false;
			}
		}
		
		if(contactConfig.phoneNo2.ibeVisibility && contactConfig.phoneNo2.mandatory){
	    	if(trim($("#txtEmgnPCountry").val()) != "") {
	    		if (validateInteger($("#txtEmgnPCountry").val())){				
	    			removeZero("#txtEmgnPCountry");
	    		}else {
	    			$("#txtEmgnPCountry").val("");
	    		}			
	    	}
	    	if ( contactConfig.areaCode1.ibeVisibility ){
		    	if(trim($("#txtEmgnPArea").val()) != "") {
	    			if (validateInteger($("#txtEmgnPArea").val())){				
	    				removeZero("#txtEmgnPArea");
	    			}else {
	    				$("#txtEmgnPArea").val("");
	    			}		
	    		}
	    	}
	    	if(trim($("#txtEmgnPhone").val()) != "") {
	    		if (validateInteger($("#txtEmgnPhone").val())){				
	    			removeZero("#txtEmgnPhone");
	    		}else {
	    			$("#txtEmgnPhone").val("");
	    		}		
	    	}
	    	
	    	if (trim($("#txtEmgnPhone").val()) == ""){
	    		strErrorHTML += "<li>" + raiseError("ERR079");
	    		$("#txtEmgnPhone").addClass("errorControl");	
	    		$("#txtEmgnPCountry").addClass("errorControl");
	    		$("#txtEmgnPArea").addClass("errorControl");
	    		isNavExc = false;
	    	}
	    	
	    	if (trim($("#txtEmgnPhone").val()) != ""){
	    		if (trim($("#txtEmgnPCountry").val()) == ""){
	    			strErrorHTML += "<li> " + raiseError("ERR077");
	    			$("#txtEmgnPCountry").addClass("errorControl");	
	    			isNavExc = false;
	    		}
	    		if ( contactConfig.areaCode1.ibeVisibility ){
		    		if (trim($("#txtEmgnPArea").val()) == ""){
		    			strErrorHTML += "<li> " + raiseError("ERR078");
		    			$("#txtEmgnPArea").addClass("errorControl");	
		    			isNavExc = false;
		    		}
	    		}else{
	    			if(trim($("#txtEmgnPhone").val()).length < 3) {
	    				strErrorHTML += "<li>" + raiseError("ERR051");
	    				$("#txtEmgnPhone").addClass("errorControl");	
	    			}
	    		}
	    	}
	    	
	    	//strErrorHTML += UI_Passenger.validatePhoneNoFormat("#txtEmgnPCountry","#txtEmgnPArea","#txtEmgnPhone");
	    	
	    //check phone no is not mandatory & user inserted a phone no
	    } else if(contactConfig.phoneNo2.ibeVisibility &&
	    		!contactConfig.phoneNo2.mandatory &&
				trim($("#txtEmgnPhone").val()) != ""){
	    	if (validateInteger($("#txtEmgnPCountry").val())){				
				removeZero("#txtEmgnPCountry");
			}else {
				strErrorHTML += "<li> " + raiseError("ERR077");
    			$("#txtEmgnPCountry").addClass("errorControl");	
				$("#txtEmgnPCountry").val("");
			}
	    	if ( contactConfig.areaCode1.ibeVisibility
					 && trim($("#txtEmgnPArea").val()) != ""){
		    	if (validateInteger($("#txtEmgnPArea").val())){				
					removeZero("#txtEmgnPArea");
				}else {
					strErrorHTML += "<li> " + raiseError("ERR078");
    				$("#txtEmgnPhone").addClass("errorControl");	
					$("#txtEmgnPhone").val("");
				}
	    	}
	    	if ( contactConfig.areaCode1.ibeVisibility ) {
			    	if (validateInteger($("#txtEmgnPhone").val())){				
						removeZero("#txtEmgnPhone");
					}else {
						$("#txtEmgnPhone").val("");
					}
	    	}else{
	    		if (trim($("#txtEmgnPhone").val()).length < 3){
	    			strErrorHTML += "<li>" + raiseError("ERR051");
	    			$("#txtEmgnPhone").addClass("errorControl");
	    		}
	    	}
	    	
	    	//strErrorHTML += UI_Passenger.validatePhoneNoFormat("#txtEmgnPCountry","#txtEmgnPArea","#txtEmgnPhone");
	    	
	    } else {
	    	$("#txtEmgnPCountry").val("");
	    	$("#txtEmgnPArea").val("");
	    	$("#txtEmgnPhone").val("");
	    }
		
		if(contactConfig.email2.ibeVisibility && contactConfig.email2.mandatory){
			if ($("#txtEmgnEmail").val() == ""){
				strErrorHTML += "<li> " + raiseError("ERR080");
				$("#txtEmgnEmail").addClass("errorControl");
				isNavExc = false;
			}
			
			
			if ($("#txtEmgnEmail").val() != ""){
				if (!checkEmail($("#txtEmgnEmail").val())){
					strErrorHTML += "<li> " + raiseError("ERR081");
					$("#txtEmgnEmail").addClass("errorControl");
					isNavExc = false;
				} else if ($("#txtEmgnEmail").val().toLowerCase() != $("#txtVerifyEmgnEmail").val().toLowerCase()){
					strErrorHTML += "<li> " + raiseError("ERR084");
					$("#txtEmgnEmail").addClass("errorControl");	
					$("#txtVerifyEmgnEmail").addClass("errorControl");
					isNavExc = false;
				} else if(validateEmailDomain){
					var emailErr = UI_Passenger.checkEmailDomain($("#txtEmgnEmail"), 2, strErrorHTML);
					if(emailErr.length > 0){
						isNavExc = false;
						strErrorHTML += emailErr;
					}			
				} 
			}
		} else if(contactConfig.email2.ibeVisibility && 
				!contactConfig.email2.mandatory &&
				trim($("#txtEmgnEmail").val()) != ""){
			if (!checkEmail($("#txtEmgnEmail").val())){
				strErrorHTML += "<li> " + raiseError("ERR081");
				$("#txtEmgnEmail").addClass("errorControl");	
				isNavExc = false;
			} else if ($("#txtEmgnEmail").val().toLowerCase() != $("#txtVerifyEmgnEmail").val().toLowerCase()){
				strErrorHTML += "<li> " + raiseError("ERR084");
				$("#txtEmgnEmail").addClass("errorControl");	
				$("#txtVerifyEmgnEmail").addClass("errorControl");
				isNavExc = false;
			}else if(validateEmailDomain){
				var emailErr = UI_Passenger.checkEmailDomain($("#txtEmgnEmail"), 2, strErrorHTML);
				if(emailErr.length > 0){
					isNavExc = false;
					strErrorHTML += emailErr;
				}			
			} 	
		} else {
			$("#txtEmgnEmail").val("");
		}
	}
	//if coutinue as new customer in new layout
	if (anciConfig.paxNewLayout!= undefined && anciConfig.paxNewLayout && 
			UI_Passenger.passengerOptions.toUpperCase() == "NEWCUSTOMER"){

		var strEmail = $("#txtLoginEmail").val();
		if(strEmail ==""){
			strErrorHTML += "<li> " + raiseError("ERR032");
			$("#txtLoginEmail").addClass("errorControl");
			isNavExc = false;
		}else if(checkEmail(strEmail) == false){
			strErrorHTML += "<li> " + raiseError("ERR032");
			$("#txtLoginEmail").addClass("errorControl");
			isNavExc = false;
		}else if(validateEmailDomain){
			var emailErr = UI_Passenger.checkEmailDomain($("#txtLoginEmail"), 1, strErrorHTML);
			if(emailErr.length > 0){
				isNavExc = false;
				strErrorHTML += emailErr;
			}
		}
		var strPassword = $("#txtPassword").val();
		var strConfpassword = $("#txtConPassword").val();
		if(strPassword ==""){
			strErrorHTML += "<li> " + raiseError("ERR057");
			$("#txtPassword").addClass("errorControl");
			isNavExc = false;
		}else if (trim(strPassword).length < 6){
			strErrorHTML += "<li> " + raiseError("ERR087");
			$("#txtPassword").addClass("errorControl");
			isNavExc = false;
		}	
		
		if(strConfpassword ==""){
			strErrorHTML += "<li> " + raiseError("ERR085");
			$("#txtConPassword").addClass("errorControl");
			isNavExc = false;
		}else if(strPassword != strConfpassword){
			strErrorHTML += "<li> " + raiseError("ERR086");
			$("#txtConPassword").addClass("errorControl");
			isNavExc = false;
		}

	}
	
	if($("#isLMS").prop( "checked" ) && UI_commonSystem.loyaltyManagmentEnabled){
		
		var lmsDOB = $("#textDOBLMS").val();
		var lmsEmailH = $("#textFamilHeadEmailLMS").val();
		var lmsRefEmail = $("#textRefferedEmailLMS").val();
		var isHeadOfFamiltyValidated = false;
		
		if(lmsDOB==""){
			strErrorHTML += "<li> " +  raiseError("userDateOfBirthRqrd");
			$("#textDOBLMS").addClass("errorControl");
			isNavExc = false;
		}
		else if(!dateValidDate(lmsDOB)){
			strErrorHTML += "<li> " +  raiseError("userDateOfBirtIncorrect");
			$("#textDOBLMS").addClass("errorControl");
			isNavExc = false;
		} else{
			$("#textDOBLMS").removeClass("errorControl");
			var dob = $("#textDOBLMS").datepicker('getDate');
			var present = new Date();
			var age = (present - dob)/1000/60/60/24;
            if (age < (12*365+3)){
                if(lmsEmailH ==""){
                	strErrorHTML += "<li> " + raiseError("headEmailReqrd");
                    $("#textFamilHeadEmailLMS").addClass("errorControl");
                    isNavExc = false;
                }else if(checkEmail(lmsEmailH) == false){
                	isHeadOfFamiltyValidated = true;
                	strErrorHTML += "<li> " + raiseError("headEmailIncorrect");
                    $("#textFamilHeadEmailLMS").addClass("errorControl");
                    isNavExc = false;
                }
                else{
                	$("#textFamilHeadEmailLMS").removeClass("errorControl");
                }
            }   
		}
		var loginEmail = $("#txtLoginEmail").val();
		if(!isHeadOfFamiltyValidated && (lmsEmailH!=null && lmsEmailH!="")){
			if(checkEmail(lmsEmailH) == false || lmsEmailH.toLowerCase() == loginEmail.toLowerCase()){
				strErrorHTML += "<li> " + raiseError("headEmailIncorrect");
                $("#textFamilHeadEmailLMS").addClass("errorControl");
                isNavExc = false;
            } else{
            	$("#textFamilHeadEmailLMS").removeClass("errorControl");
            }
		}
		
		if(lmsRefEmail!=null && lmsRefEmail!=""){
			if(checkEmail(lmsRefEmail) == false || lmsRefEmail.toLowerCase() == loginEmail.toLowerCase()){
				strErrorHTML += "<li> " + raiseError("refEmailIncorrect");
                $("#textRefferedEmailLMS").addClass("errorControl");
                isNavExc = false;
            } else{
				$("#textRefferedEmailLMS").removeClass("errorControl");
			}
		}
		
		var lmsPreConditionValidationError = UI_Passenger.lmsHeadRefferedValidate();
		if(lmsPreConditionValidationError != null && lmsPreConditionValidationError != ""){
			strErrorHTML += "<li> " + lmsPreConditionValidationError;
			isNavExc = false;
		}
		
		if(UI_commonSystem.lmsDetails.headOFEmailId=="N"){
			strErrorHTML += "<li> " + raiseError("headEmailDoesntExist");
			$("#textFamilHeadEmailLMS").addClass("errorControl");
			isNavExc = false;
		} else {
			$("#textFamilHeadEmailLMS").removeClass("errorControl");
		}
		if(UI_commonSystem.lmsDetails.refferedEmail=="N"){
			strErrorHTML += "<li> " + raiseError("refEmailDoesntExist");
			$("#textRefferedEmailLMS").addClass("errorControl");
			isNavExc = false;
		}else {
			$("#textRefferedEmailLMS").removeClass("errorControl");
		}
		
	}
	
	UI_Passenger.setLanguagePreferences();

	if(checkForDuplicates()){
		
		var label = UI_Container.getLabels(); 
		strErrorHTML +="<li> "+label.msgDuplicateNamesExist+"</li> ";
		isNavExc = false;
		//jAlert(label.msgDuplicateNamesExist);
	}
	if(UI_commonSystem.loyaltyManagmentEnabled){
		if(paxConfigCH.ffid.ibeVisibility || paxConfigAD.ffid.ibeVisibility){
			var ffidValidity = UI_Passenger.validateFFID();
			if(ffidValidity.length > 1){
				strErrorHTML += ffidValidity;
				isNavExc = false;
			}
		}
	}
	
	if(!isNavExc){
		//checkForDuplicates()
		$("#spnError").html("<font class='mandatory'> "+strErrorHTML+"</font>");
		if(strErrorHTML.length > 0){			
			navCallBack(false);
		}else {			
			$("#spnError").html("");
			if (!contactConfig.areaCode1.ibeVisibility) {
//			if (!contactConfig.ibeAreaCodeVisible){
				UI_Passenger.setAreCode();
			}
			navCallBack(true);
		}
	} else if(UI_commonSystem.loyaltyManagmentEnabled){
		$("#spnError").html("");
		if (UI_commonSystem.lmsDetails.emailId == 'Y'){
			var mergeMsg = "<div>The account with this Airewards ID already exists. If you want to merge please click Ok.</div>";
			var accountMege = "Mege Account";
			jConfirm(mergeMsg, accountMege, function(response) {
				if (response){					
					UI_Passenger.withOptionsSelection(navCallBack);	
				} else {
					
				}
			});
		}
		else{			
			UI_Passenger.withOptionsSelection(navCallBack);
		}		
	} else {			
		UI_Passenger.withOptionsSelection(navCallBack);
	}
	
}


UI_Passenger.lmsHeadRefferedValidate = function(){
	var data = {};
	var errorMsg = "";
	var url = "lmsAjaxRegister!validateHeadReffered.action"
	data["lmsDetails.headOFEmailId"] = $("#textFamilHeadEmailLMS").val();
	data["lmsDetails.refferedEmail"] = $("#textRefferedEmailLMS").val();
	data["lmsDetails.emailId"] = $("#txtLoginEmail").val();
	data["lmsDetails.firstName"] = $("#txtFName").val();
	data["lmsDetails.lastName"] = $("#txtLName").val();
	data["registration"] = true;
	var response = $.ajax({type: "POST", 
		dataType: 'json', 
		data:data,url:url,
		async: false,
		success: function(response){
			UI_commonSystem.lmsDetails = response.lmsDetails;
			if(!response.ibeAccountExists && response.lmsNameMismatch){				
				errorMsg = response.messageTxt;
			}
		},
		error:UI_commonSystem.setErrorStatus});
	
	return errorMsg;
}

// validate smart button membership
UI_Passenger.validateFFID = function(){
	
	var url = "lmsMemberConfirmation!validateFFID.action";
	var  data = new Object();
	
	data["paxDetailJson"] = $.toJSON(UI_Passenger.getFFIDPerPax());
	
	var response = $.ajax({ type: "POST", dataType: 'json', async: false, data:data, url:url});
	
	response = $.parseJSON(response.responseText);
	if(typeof response.paxDetail == undefined){
		return;
	}
	
	var paxDetails = response.paxDetail;
	var errorString = "";
	
	for(var pl=0;pl < paxDetails.adultList.length;pl++){
		if(paxDetails.adultList[pl].ffid != "" && !paxDetails.adultList[pl].validFFID){
			$("#adultList\\[" + pl + "\\]\\.ffidTxt").addClass("errorControl");
			return (errorString += ("<li> " + raiseError("ERR121")));
		}else{
			$("#adultList\\[" + pl + "\\]\\.ffid").val(trim($("#adultList\\[" + pl + "\\]\\.ffidTxt").val()));
		}
	}
	
	for(var pl=0;pl < paxDetails.childList.length;pl++){
		if(paxDetails.childList[pl].ffid != "" && !paxDetails.childList[pl].validFFID){
			$("#childList\\[" + pl + "\\]\\.ffidTxt").addClass("errorControl");
			return (errorString += ("<li> " + raiseError("ERR121")));
		}else{
			$("#childList\\[" + pl + "\\]\\.ffid").val(trim($("#childList\\[" + pl + "\\]\\.ffidTxt").val()));
		}
	}
	
	var details = UI_Passenger.getFFIDPerPax();
	
	for(var pl=0;pl < paxDetails.adultList.length;pl++){
		var firstName1 = paxDetails.adultList[pl].firstName.toLowerCase();
		var	lastName1 = paxDetails.adultList[pl].lastName.toLowerCase();
		var	firstName2 = details.adultList[pl].firstName.toLowerCase();		
		var	lastName2 = details.adultList[pl].lastName.toLowerCase();
		if(!compare(firstName1, lastName1 , firstName2, lastName2)){
			if(paxDetails.adultList[pl].ffid!=null && paxDetails.adultList[pl].ffid!=""){
				$("#adultList\\[" + pl + "\\]\\.firstName").addClass("errorControl");
				$("#adultList\\[" + pl + "\\]\\.lastName").addClass("errorControl");
				errorString += ("<li> " + raiseError("ERR122"));
			}
		}
		var temDateVal = paxDetails.adultList[pl].dateOfBirth;
		if((temDateVal != "") && (UI_commonSystem.getAge(temDateVal,UI_Passenger.extractFlightFirstSegOutDate()) < 12*365+3)){
				errorString += "<li> " + raiseError("ERR124", " # " + (pl + 1));
		}
	}
	
	for(var pl=0;pl < paxDetails.childList.length;pl++){
		var firstName1 = paxDetails.childList[pl].firstName.toLowerCase();
		var	lastName1 = paxDetails.childList[pl].lastName.toLowerCase();
		var	firstName2 = details.childList[pl].firstName.toLowerCase();		
		var	lastName2 = details.childList[pl].lastName.toLowerCase();
		if(!compare(firstName1, lastName1 , firstName2, lastName2)){
			if(paxDetails.childList[pl].ffid!=null && paxDetails.childList[pl].ffid!=""){
				$("#childList\\[" + pl + "\\]\\.firstName").addClass("errorControl");
				$("#childList\\[" + pl + "\\]\\.lastName").addClass("errorControl");
				errorString += ("<li> " + raiseError("ERR122"));
			}
		}
		var temDateVal = paxDetails.childList[pl].dateOfBirth;
		if((temDateVal != "") && (UI_commonSystem.getAge(temDateVal,UI_Passenger.extractFlightFirstSegOutDate()) > 12*365+3)){
				errorString += "<li> " + raiseError("ERR125", " # " + (pl + 1));
		}
	}

	return errorString;
}

UI_Passenger.getFFIDPerPax = function(){
	var paxDetails = $.airutil.dom.cloneObject(UI_Passenger.paxDetails);
		
	for(var pl=0;pl < paxDetails.adultList.length;pl++){
		paxDetails.adultList[pl].ffid = $("#adultList\\["+pl+"\\]\\.ffidTxt").val();
		paxDetails.adultList[pl].firstName = $("#adultList\\["+pl+"\\]\\.firstName").val();
		paxDetails.adultList[pl].lastName = $("#adultList\\["+pl+"\\]\\.lastName").val();
	}
	
	for(var pl=0;pl < paxDetails.childList.length;pl++){
		paxDetails.childList[pl].ffid = $("#childList\\["+pl+"\\]\\.ffidTxt").val();
		paxDetails.childList[pl].firstName = $("#childList\\["+pl+"\\]\\.firstName").val();
		paxDetails.childList[pl].lastName = $("#childList\\["+pl+"\\]\\.lastName").val();
	}	
	
	return paxDetails;
}



UI_Passenger.withOptionsSelection = function(navCallBack){
//after validation completed
var label = UI_Container.getLabels(); 
if (anciConfig.paxNewLayout!= undefined && anciConfig.paxNewLayout){
	if (UI_Passenger.passengerOptions.toUpperCase() == "OLDCUSTOMER"){//new customer
		if (UI_Container.isRegisteredUser){
			$("#spnError").html("");
			if (!contactConfig.areaCode1.ibeVisibility) {
//			if (!contactConfig.ibeAreaCodeVisible){
				UI_Passenger.setAreCode();
			}
			navCallBack(true);
		}else{
			jAlert(label["lblLoginContinuError"]);
		}
	}else if(UI_Passenger.passengerOptions.toUpperCase() == "NEWCUSTOMER"){//new customer
		UI_Container.showLoading();
		registerCustomer = function(){
			var url ="customerRegister.action";
			var  data = {};
			data["customer.emailId"]= $('#txtLoginEmail').val();
			data["customer.password"] = $('#txtPassword').val();
			data["customer.title"] = $('#selTitle').val();
			data["customer.firstName"] = $('#txtFName').val();
			data["customer.lastName"] = $('#txtLName').val();
			data["customer.nationalityCode"] = $('#selNationality').val();
			data["customer.countryCode"] = $('#selCountry').val();
			data["customer.telephone"] = $('#txtPCountry').val() +"-"+ $('#txtPArea').val() +"-"+ $('#txtPhone').val();
			data["customer.mobile"] = $('#txtMCountry').val() +"-"+ $('#txtMArea').val() +"-"+ $('#txtMobile').val();
			data["customer.fax"] = $('#txtFCountry').val() +"-"+ $('#txtFArea').val() +"-"+ $('#txtFax').val();
			data["customer.dateOfBirth"] = "";
			if($("#isLMS").attr( "checked" ) && UI_commonSystem.loyaltyManagmentEnabled){
				data["optLMS"]="Y";
				data["lmsDetails.mobileNumber"] = $('#txtMCountry').val() +"-"+ $('#txtMArea').val() +"-"+ $('#txtMobile').val();
				data["lmsDetails.headOFEmailId"] = $("#textFamilHeadEmailLMS").val();
				data["lmsDetails.refferedEmail"] = $("#textRefferedEmailLMS").val();
				data["lmsDetails.genderCode"] = $('#selTitle').val();
				data["lmsDetails.dateOfBirth"] = $("#textDOBLMS").val();
				data["lmsDetails.firstName"] = $("#txtFName").val();
				data["lmsDetails.lastName"] = $("#txtLName").val();
				data["lmsDetails.passportNum"] = $("#textPPNumberLMS").val();
				data["lmsDetails.phoneNumber"] = $('#txtPCountry').val() +"-"+ $('#txtPArea').val() +"-"+ $('#txtPhone').val();
				data["lmsDetails.nationalityCode"] = $("#selNationality").val();
				data["lmsDetails.residencyCode"] = $("#txtLName").val();
				data["lmsDetails.emailId"] = $("#txtLoginEmail").val();
				data["lmsDetails.language"] = $("#textComLangLMS").val();
				data["lmsDetails.appCode"] = "APP_IBE";
			}
			else{
				data["optLMS"]="N";
			}		
			
			$.ajax({type: "POST", dataType: 'json', data:data,url:url,		
				success: registerCustomerSuccess, error:UI_commonSystem.setErrorStatus});
			return false;
		};
		registerCustomerSuccess = function(response){
			
			if(response != null && response.success == true) {
				if (response.messageTxt != null && $.trim(response.messageTxt) != "") {
					UI_Container.hideLoading();
					var emailExistsError =label["lblEmailAlreadyExistError"].replace('{0}', UI_Container.carrierName);
					jConfirm(emailExistsError  , 'Confirm' ,
						function(response1){
							if (response1){
								navCallBack(false);
								UI_Passenger.cuntinueOptionClick([{id:"loginContinue", value:"on"}])
							}else{
								$("#txtLoginEmail").focus();
							}
						}
					, label["lblYesLogin"] , label["lblNoNewAccount"]);
				}else{
					//new prfile were created
					customerLoginSucess = function(response){
						if(response.loginSuccess) {
							UI_Container.buildCustomer(response.customer, response.totalCustomerCredit, response.baseCurr);
							eval(response.custProfile);
							profileLoad();
							if(top!=null & top[0]!=null){
								top[0].strReturnPage = "CUSTOMER";
								top[0].strUN = response.txtUname;
								UI_Container.isRegisteredUser = true;
							}
							// Update Register Customer Login status
							SYS_IBECommonParam.setCustomerState(true);
							if (!contactConfig.areaCode1.ibeVisibility){UI_Passenger.setAreCode();}
							navCallBack(true);
						} else {
							UI_Container.hideLoading();
							jAlert(UI_commonSystem.alertText({msg:arrError["ERR061"], language:strLanguage}));
						}
						if(response.lmsDetails.emailStatus!=null||response.lmsDetails.emailStatus!=""){
							UI_commonSystem.changeUserLmsStatus(response.lmsDetails);
							UI_commonSystem.showLmsInBar();
						}
						UI_commonSystem.showLmsInBar();
					}
					customerLogin = function(){
						//if(!loginValidation( $('#txtUID'), $('#txtPWD') )) return;
						var url = "ajaxLogin.action";
						var  data = new Object();
						data["txtUID"] = $('#txtLoginEmail').val();
						data["txtPWD"] = $('#txtPassword').val();
						$.ajax({ type: "POST", dataType: 'json', data:data, url:url,							
							success: customerLoginSucess, error:UI_commonSystem.setErrorStatus});
					}
					customerLogin();
				}
				
			}else{
				UI_Container.hideLoading();
				UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
			}
			
		};
		$("#spnError").html("");
		registerCustomer();
		}else{
			//guest
			customerCheckSucess = function (response){
				if(response.success) {
					var emailExistsError =label["lblEmailAlreadyExistError"].replace('{0}', UI_Container.carrierName);
					jConfirm(emailExistsError  , 'Confirm' ,
						function(response1){
							if (response1){
								navCallBack(false);
								UI_Passenger.cuntinueOptionClick([{id:"loginContinue",value:"on"}]);
							}else{
								$("#spnError").html("");
								if (!contactConfig.areaCode1.ibeVisibility){UI_Passenger.setAreCode();}
								navCallBack(true);
							}
						}
					, label["lblYesLogin"] , label["lblNoContinue"]);
				}else{
					$("#spnError").html("");
					if (!contactConfig.areaCode1.ibeVisibility){UI_Passenger.setAreCode();}
					navCallBack(true);
				}
			}
			customerCheck = function(){
				//if(!loginValidation( $('#txtUID'), $('#txtPWD') )) return;
				var url = "customerRegister!checkRegCustomer.action";
				var  data = new Object();
				data["customer.emailId"] = $('#txtEmail').val();
				$.ajax({ type: "POST", dataType: 'json', data:data, url:url,							
					success: customerCheckSucess, error:UI_commonSystem.setErrorStatus});
			}
			customerCheck();
		}
	}else{
		$("#spnError").html("");
		if (!contactConfig.areaCode1.ibeVisibility){UI_Passenger.setAreCode();}
		navCallBack(true);
	}
}

function groupFieldValidation(groupObj, jsonFieldToElementMapping, jsonGroupValidation) {
	var errorHtml = "";
	var rVal = true;
	$.each(groupObj, function(key, fieldList){
		var valid = false;
		$.each(fieldList, function(index, fVal){
			if(trim($(jsonFieldToElementMapping[fVal]).val()) != ""){
				valid = true;
			}
		});
		
		if(!valid){
			errorHtml += "<li> " + raiseError(jsonGroupValidation[key]);	
			$.each(jsonGroupElementMapping[key],function(index, elem){
				$(elem).addClass("errorControl");
			});
		}
	});
	
	return errorHtml;
}

;(function($){
	$.fn.serializeJson = function(){
		var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name]) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	}
})(jQuery);

function setDetailsForPax() {
    // added in order to load anci page details again
	UI_Anci.loaded = false;
}

UI_Passenger.getPaxDetails = function(){
	var formdata = {};
	if(UI_Passenger.loaded){	
		formdata = $('#frmReservation').serializeJson();
	}
	var paxTypes = UI_Container.paxTypeCounts;
	var dummyJsonPaxBuilder = function(formdata, paxType, count){
		var data = [];
		var paxPrefix = '';
		switch(paxType){
			case 'AD': paxPrefix='adultList';break;
			case 'CH': paxPrefix='childList';break;
			case 'INF': paxPrefix='infantList';break;
		}
		for(var i = 0; i < count;i++){
			var pax = {};
			var prefix = paxPrefix+'['+i+'].';
			pax['title'] = (formdata[prefix+'title']||'');
			pax['firstName'] = ($.trim(formdata[prefix+'firstName']));
			pax['lastName'] = ($.trim(formdata[prefix+'lastName']));
			pax['dateOfBirth'] =  dateToGregorian((formdata[prefix+'dateOfBirth']||''), SYS_IBECommonParam.locale);
			pax['foidNumber'] = (formdata[prefix+'foidNumber']||'');
			pax['foidExpiry'] = dateToGregorian((formdata[prefix+'foidExpiry']||''), SYS_IBECommonParam.locale);
			pax['foidPlace'] = (formdata[prefix+'foidPlace']||'');
			pax['foidType'] = (formdata[prefix+'foidNumber']||'');
			pax['placeOfBirth'] = (formdata[prefix+'placeOfBirth']||'');
			pax['travelDocumentType'] = (formdata[prefix+'travelDocumentType']||'');
			pax['visaDocNumber'] = (formdata[prefix+'visaDocNumber']||'');
			pax['visaDocPlaceOfIssue'] = (formdata[prefix+'visaDocPlaceOfIssue']||'');
			pax['visaDocIssueDate'] = dateToGregorian((formdata[prefix+'visaDocIssueDate']||''), SYS_IBECommonParam.locale);
			pax['visaApplicableCountry'] = (formdata[prefix+'visaApplicableCountry']||'');
			pax['nationality'] = (formdata[prefix+'nationality']||'');
			pax['displayNationalIDNo'] = (formdata[prefix+'displayNationalIDNo']||'');
			pax['paxCategory'] = paxCat;
			pax['parent']= false;			
			if(paxType == 'INF'){
				pax['travelWith'] = (formdata[prefix+'travelWith']||i);
				pax['title'] = (formdata[prefix+'infTitle']||'');
			} else if(paxType == 'CH'){
				pax['title'] = (formdata[prefix+'chdTitle']||''); 
				pax['ffid'] = (formdata[prefix+'ffid']||'');
			} else if(paxType == 'AD'){
				pax['ffid'] = (formdata[prefix+'ffid']||'');
			}		
			data[i] = pax;
		}
		
		return data;
	}
	var adults = dummyJsonPaxBuilder(formdata,'AD',paxTypes.adults);
	var infants = dummyJsonPaxBuilder(formdata,'INF',paxTypes.infants);
	for(var i = 0 ; i< infants.length ; i++){
		
		var paxId = parseInt(infants[i].travelWith,10)-1;
		if(paxId >= 0){
			adults[paxId].parent = true;
			adults[paxId].travelWith = (i+1) + paxTypes.adults + paxTypes.children;
		}
	}
	var paxDetails =  {
		adults:adults,
		children:dummyJsonPaxBuilder(formdata,'CH',paxTypes.children),
		infants:infants
		};
	
	return paxDetails;
}

UI_Passenger.getContactDetails = function(){
	var contactDetails = {};
	
	if(UI_Passenger.loaded){	
		formdata = $('#frmReservation').serializeJson();
	}
	
	contactDetails['title'] = formdata['contactInfo.title'];
	contactDetails['firstName'] = $.trim(formdata['contactInfo.firstName']);
	contactDetails['lastName'] = $.trim(formdata['contactInfo.lastName']);
	contactDetails['addresStreet'] = formdata['contactInfo.addresStreet'];
	contactDetails['addresline'] = formdata['contactInfo.addresline'];
	contactDetails['city'] = formdata['contactInfo.city'];
	contactDetails['country'] = formdata['contactInfo.country'];
	contactDetails['zipCode'] = formdata['contactInfo.zipCode'];
	contactDetails['nationality'] = formdata['contactInfo.nationality'];
	contactDetails['emailAddress'] = formdata['contactInfo.emailAddress'];
	contactDetails['preferredLangauge'] = formdata['contactInfo.preferredLangauge'];
	contactDetails['countryName'] = formdata['contactInfo.countryName'];
	
	return contactDetails;
}
/**
 * TODO: Remove code block
 */
UI_Passenger.getPaxSubmitData = function(paxTypeCounts){
	var output = {};
	if(!UI_Passenger.loaded){
		/*
		 * Passenger name should not change as T B A. T B A bookings will not reflect in PNL. As we have an issue for 
		 * IBE T B A bookings, we removed the below code based. in case of passenger details not available pls use correct methods to pass
		 * the details.
		 * 
		 */
//		var dummyPaxBuilder = function(paxType, count){
//			var data = {};
//			var paxPrefix = '';
//			switch(paxType){
//				case 'AD': paxPrefix='adultList';break;
//				case 'CH': paxPrefix='childList';break;
//				case 'INF': paxPrefix='infantList';break;
//			}
//			for(var i = 0; i < count;i++){
//				var prefix = paxPrefix+'['+i+'].';
//				data[prefix + 'firstName'] = 'T B A';
//				data[prefix + 'lastName'] = 'T B A';
//				data[prefix + 'dateOfBirth'] = '';
//				if(paxType == 'INF'){
//					data[prefix + 'travelWith'] = i;
//				}
//			}
//			
//			return data;
//		}
//		output = $.extend(output,dummyPaxBuilder('AD',paxTypeCounts.adults));
//		output = $.extend(output,dummyPaxBuilder('CH',paxTypeCounts.children));
//		output = $.extend(output,dummyPaxBuilder('INF',paxTypeCounts.infants));
	}
	return output;
}

UI_Passenger.adultValidate = function(paxList, type) {
	var strPaxHTML = "";
	var strPaxList = "#adultList\\[";
	var strPaxString = strMsgAdult;
	var fnCode = "ERR018";
	var lnCode = "ERR019";
	var agCode = "ERR053";
	var nationalityCode = "ERR089";
	var psptNullCode = "ERR069";
	var psptInvalidCode = "ERR070";
	var psptExpiryNullCode = "ERR091";
	var psptPlaceOfIssueNullCode = "ERR093";
	var docoPlaceOfBirthNullCode = "ERR100";
	var docoDocTypeNullCode = "ERR102";
	var docoDocNumNullCode = "ERR104";
	var docoPlaceOfIssueNullCode = "ERR106";
	var docoIssueDateNullCode = "ERR108";
	var docoCountryNullCode = "ERR110";
	var docoDocNumInvalidCode = "ERR112";
	var docoPlaceOfBirthInvalidCode = "ERR114";
	var docoPlaceOfIssueInvalidCode = "ERR116";
	var docoCountryCodeMismatch = "ERR119";
	var docoNationalIDNoNullCoe = "ERR128";
	var docoNICinvalid = "ERR129";
	var ageMandatory = false;
	var paxConfig = paxConfigAD;
	var docoDateOfBirthNullCode = "ERR0120";
		
	var lowestAge = Number(paxValidation.childAgeCutOverYears);
	var ageYears = Number(paxValidation.adultAgeCutOverYears);
	var ageLowerDays = paxValidation.infantAgeCutLowerBoundaryDays;
	var ageLowerMonths = paxValidation.infantAgeCutLowerBoundaryMonths;
	//var ageLowerYears = paxValidation.infantAgeLowerYears;
	var strLowerAge = ageLowerDays + "/" + ageLowerMonths + "/" + 0;
	var showPSPTDetails = false;
	var showDOCODetails = false;
		
	if(type == "CHD"){
		strPaxList = "#childList\\[";
		strPaxString = strMsgChild;
		fnCode = "ERR045";
		lnCode = "ERR046";
		agCode = "ERR025";
		psptNullCode = "ERR082";
		psptInvalidCode = "ERR083";
		nationalityCode = "ERR090";
		psptExpiryNullCode = "ERR092";
		psptPlaceOfIssueNullCode = "ERR094";
		docoPlaceOfBirthNullCode = "ERR101";
		docoDocTypeNullCode = "ERR103";
		docoDocNumNullCode = "ERR105";
		docoPlaceOfIssueNullCode = "ERR107";
		docoIssueDateNullCode = "ERR109";
		docoCountryNullCode = "ERR111";
		docoDocNumInvalidCode = "ERR113";
		docoPlaceOfBirthInvalidCode = "ERR115";
		docoPlaceOfIssueInvalidCode = "ERR117";
		docoNationalIDNoNullCoe = "ERR130";
		docoNICinvalid = "ERR131";
		ageYears = Number(paxValidation.childAgeCutOverYears);
		lowestAge = Number(paxValidation.infantAgeCutOverYears);
		paxConfig = paxConfigCH;
	}else if(type == "INF") {
		strPaxList = "#infantList\\[";
		strPaxString = strMsgInfant;
		fnCode = "ERR020";
		lnCode = "ERR021";
		agCode = "ERR025";
		psptNullCode = "ERR134";
		psptInvalidCode = "ERR135";
		psptPlaceOfIssueNullCode = "ERR095";
		psptExpiryNullCode = "ERR096";
		docoNationalIDNoNullCoe = "ERR124";
		docoNICinvalid = "ERR125";
		ageYears = Number(paxValidation.infantAgeCutOverYears);
		lowestAge = 0;
		paxConfig = paxConfigIN;
	}
	for(var vl=0; vl < paxList.length;vl++){
		$(strPaxList+vl+"\\]\\.firstName").addClass("fontCapitalize");
		$(strPaxList+vl+"\\]\\.lastName").addClass("fontCapitalize");
		$(strPaxList+vl+"\\]\\.ffidTxt").addClass("fontCapitalize");
		$(strPaxList+vl+"\\]\\.foidNumberTxt").removeClass("errorControl");
		showPSPTDetails = false;
		showDOCODetails = false;
		//Title
		if(type != "INF") {
			if(paxConfig.title.ibeVisibility && paxConfig.title.ibeMandatory){
				if(paxConfig.title.ibeVisibility && paxConfig.title.ibeMandatory){
					if (type == "ADT" && ($(strPaxList+vl+"\\]\\.title").val() == null || $(strPaxList+vl+"\\]\\.title").val() == "")){
							strPaxHTML += "<li> " +  raiseError("ERR017","#" + (vl + 1));
							$(strPaxList+vl+"\\]\\.title").addClass("errorControl");
					} else if (type == "CHD" && ($(strPaxList+vl+"\\]\\.chdTitle").val() == null || $(strPaxList+vl+"\\]\\.chdTitle").val() == "")) {
						strPaxHTML += "<li> " +  raiseError("ERR044","#" + (vl + 1));
						$(strPaxList+vl+"\\]\\.title").addClass("errorControl");
					}
				}
			}
			if(UI_commonSystem.loyaltyManagmentEnabled){
				if($(strPaxList+vl+"\\]\\.ffidTxt").val() != null && $(strPaxList+vl+"\\]\\.ffidTxt").val() != ""){
					if(!checkEmail($(strPaxList+vl+"\\]\\.ffidTxt").val())){
						strPaxHTML += "<li> " + raiseError("ERR120","#" + (vl + 1));
						$(strPaxList+vl+"\\]\\.ffidTxt").addClass("errorControl");
					}
				}
			}
		}		
		
		//First Name
		if(paxConfig.firstName.ibeVisibility && paxConfig.firstName.ibeMandatory){
			if (trim($(strPaxList+vl+"\\]\\.firstName").val()) == ""){
				strPaxHTML += "<li> " + raiseError(fnCode, " # " + (vl + 1));
				$(strPaxList+vl+"\\]\\.firstName").addClass("errorControl");			
			}
		}
		
		if(trim($(strPaxList+vl+"\\]\\.firstName").val()) != ""){
			strChkEmpty = checkInvalidChar(trim($(strPaxList+vl+"\\]\\.firstName").val()), arrError["ERR027"], strPaxString + " # " + (vl + 1) + " " + strMsgFN);
			if (strChkEmpty != ""){
				strPaxHTML += "<li> " + strChkEmpty;
				$(strPaxList+vl+"\\]\\.firstName").addClass("errorControl");
			}
			
			if (!isAlphaWhiteSpace(trim($(strPaxList+vl+"\\]\\.firstName").val()))){
				strPaxHTML += "<li> " + raiseError("ERR040", " "+strPaxString + " " + type + " " + (vl + 1) + " "+strMsgFN);
				$(strPaxList+vl+"\\]\\.firstName").addClass("errorControl");
			}
			
			if ($.trim($(strPaxList+vl+"\\]\\.firstName").val()).length < paxConfig.firstName.minLength) {
				strPaxHTML += "<li> " +  raiseError("ERR599", strPaxString, " " + type + " " + (vl + 1), " 2 ");
				$(strPaxList+vl+"\\]\\.firstName").addClass("errorControl");
			}
			
			//AARESAA-6541 - FIXED
			if(UI_Passenger.checkSpclCharOnly($(strPaxList+vl+"\\]\\.firstName").val())){
				strPaxHTML += "<li> " +  buildError(arrError["ERR027"],"--",strPaxString + " # " + (vl + 1) + " " + strMsgFN);
				$(strPaxList+vl+"\\]\\.firstName").addClass("errorControl");				
			}
		}
						
		//Last Name
		if(paxConfig.lastName.ibeVisibility && paxConfig.lastName.ibeMandatory){
			if (trim($(strPaxList+vl+"\\]\\.lastName").val()) == ""){
				strPaxHTML += "<li> " +  raiseError(lnCode, " # " + (vl + 1));
				$(strPaxList+vl+"\\]\\.lastName").addClass("errorControl");
			}
		}
		
		if (trim($(strPaxList+vl+"\\]\\.lastName").val()) != ""){
			strChkEmpty = checkInvalidChar($(strPaxList+vl+"\\]\\.lastName").val(), arrError["ERR027"], strPaxString + " # " + (vl + 1) + " " + strMsgLN);
			if (strChkEmpty != ""){
				strPaxHTML += "<li> " +  strChkEmpty;
				$(strPaxList+vl+"\\]\\.lastName").addClass("errorControl");
			}			
			
			if (!isAlphaWhiteSpace(trim($(strPaxList+vl+"\\]\\.lastName").val()))){
				strPaxHTML += "<li> " + raiseError("ERR040", " "+strPaxString+" " + type + " "+ (vl + 1) + " "+strMsgLN);
				$(strPaxList+vl+"\\]\\.lastName").addClass("errorControl");
			}
			
			if ($.trim($(strPaxList+vl+"\\]\\.lastName").val()).length < paxConfig.lastName.minLength) {
				strPaxHTML += "<li> " +  raiseError("ERR055", strPaxString, " " + type + " " + (vl + 1), " 2 ");
				$(strPaxList+vl+"\\]\\.lastName").addClass("errorControl");
			}
			
			//AARESAA-6541 - FIXED			
			if(UI_Passenger.checkSpclCharOnly($(strPaxList+vl+"\\]\\.lastName").val())){
				strPaxHTML += "<li> " +  buildError(arrError["ERR027"],"--",strPaxString + " # " + (vl + 1) + " " + strMsgLN);
				$(strPaxList+vl+"\\]\\.lastName").addClass("errorControl");				
			}
		}			
		
		if(type != 'INF'){
			if(paxConfig.nationality.ibeVisibility && paxConfig.nationality.ibeMandatory){
				if ($(strPaxList+vl+"\\]\\.nationality").val() == ""){
					strPaxHTML += "<li> " + raiseError(nationalityCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.nationality").addClass("errorControl");
				} 
			}
		}
		
		//Date of Birth 
		if(type == 'ADT'){
			if(paxConfig.dob.ibeVisibility && paxConfig.dob.ibeMandatory){
				if ($(strPaxList+vl+"\\]\\.dateOfBirth").val() == ""){
					strPaxHTML += "<li> " + raiseError("ERR126", " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
				} 
			}
		} else if(type == 'CHD'){
			if(paxConfig.dob.ibeVisibility && paxConfig.dob.ibeMandatory){
				if ($(strPaxList+vl+"\\]\\.dateOfBirth").val() == ""){
					strPaxHTML += "<li> " + raiseError("ERR0101", " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
				} 
			}
		} else if(type == 'INF'){
			if(paxConfig.dob.ibeVisibility && paxConfig.dob.ibeMandatory){
				if ($(strPaxList+vl+"\\]\\.dateOfBirth").val() == ""){
					strPaxHTML += "<li> " + raiseError("ERR022", strPaxString + " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
				} 
			}
		}
		
		if ($(strPaxList+vl+"\\]\\.dateOfBirth").val() != ""){
			var temDateVal = dateToGregorian($(strPaxList+vl+"\\]\\.dateOfBirth").val(), SYS_IBECommonParam.locale);
			if ((temDateVal != "") && (!dateValidDate(temDateVal))){
				 strPaxHTML += "<li> " + raiseError("ERR039", strPaxString + " # " + (vl + 1) + " " + strMsgDOB);
					$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
			} else{
				 if ((temDateVal != "") && (!CheckDates(temDateVal, UI_Container.getSystemDate()))){
					 strPaxHTML += "<li> " + raiseError("ERR023", strPaxString ," # " + (vl + 1),  UI_Container.getSystemDate());
					$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
				}
			}
			
			var strAgeDate = $('#resDepartureDate').val();
			var retD = $('#resReturnDate').val();
			if ( retD != null && retD != ''){
				strAgeDate = retD;
			}
			
			if ((temDateVal != "") && dateValidDate(temDateVal) && (!ageCompare(temDateVal,  strAgeDate, ageYears))){
				strPaxHTML += "<li> " + raiseError(agCode, strPaxString, " # " + (vl + 1), strPaxString, strPaxString, ageYears);
				$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
			}
			else if((temDateVal != "") && dateValidDate(temDateVal)
					&& (UI_commonSystem.getAge(temDateVal,UI_Passenger.extractFlightFirstSegOutDate()) < lowestAge*365+(lowestAge/4))){
				strPaxHTML += "<li> " + raiseError("ERR123", strPaxString, " # " + (vl + 1), strPaxString, strPaxString, ageYears, lowestAge);
			}
		}		
							
		if(type == "INF") {
			if(paxConfig.travelWith.ibeVisibility && paxConfig.travelWith.ibeMandatory){
				if($(strPaxList+vl+"\\]\\.travelWith").val() == null || $(strPaxList+vl+"\\]\\.travelWith").val() == ""){
					strPaxHTML += "<li> " + raiseError("ERR024", " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.travelWith").addClass("errorControl");	

				}
			}
			
			if($(strPaxList+vl+"\\]\\.travelWith").val() != null && 
					$(strPaxList+vl+"\\]\\.travelWith").val() != ""){
				for (var x = 0 ; x < paxList.length; x++){
					if (x != vl){
						if ($(strPaxList+vl+"\\]\\.travelWith").val() == $(strPaxList+x+"\\]\\.travelWith").val()){
							strPaxHTML += "<li> " + raiseError("ERR026", " # " + (vl + 1), " # " + $(strPaxList+vl+"\\]\\.travelWith").val());
							$(strPaxList+vl+"\\]\\.travelWith").addClass("errorControl");
							break;
						}			
					}
				}
			}	
			
			if(paxConfig.title.ibeVisibility && paxConfig.title.ibeMandatory){
				if($(strPaxList+vl+"\\]\\.infTitle").val() == null || $(strPaxList+vl+"\\]\\.infTitle").val() == ""){
					strPaxHTML += "<li> " + raiseError("ERR016", " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.infTitle").addClass("errorControl");	

				}
			}
			
			var temDateValINF = dateToGregorian($(strPaxList+vl+"\\]\\.dateOfBirth").val(), SYS_IBECommonParam.locale);			
			if ((temDateValINF != "") && 
					dateValidDate(temDateValINF) && 
					(!ageValidate(temDateValINF,  strAgeDate, strLowerAge, "Min"))){
				strPaxHTML += "<li> " + raiseError("ERR062", " # " + (vl + 1), ageLowerDays);
				$(strPaxList+vl+"\\]\\.dateOfBirth").addClass("errorControl");
			}
			
		}
		

			if(paxConfig.passportNo.ibeVisibility && paxConfig.passportNo.ibeMandatory){
				if(trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()) == ""){
					strPaxHTML += "<li> " + raiseError(psptNullCode, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").addClass("errorControl");
					showPSPTDetails = true;
				} else if(trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()) != "" && 
						!isAlphaNumeric(trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()))) {
					strPaxHTML += "<li> " + raiseError(psptInvalidCode, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").addClass("errorControl");
					showPSPTDetails = true;
				}
				$(strPaxList+vl+"\\]\\.foidNumber").val(trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()));
			}else{
				if($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val() != null && 
						trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()) != ""){
					if(!isAlphaNumeric(trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()))) {
						strPaxHTML += "<li> " + raiseError(psptInvalidCode, " # " + (vl + 1));
						$("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").addClass("errorControl");
					}
					
					if(paxConfig.passportNo.uniqueness != "NONE"){
						foidLists +=trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val())+"^" ;
					}
					
					$(strPaxList+vl+"\\]\\.foidNumber").val(trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val()));
				}
			}
			
			if(paxConfig.nationalIDNo.ibeVisibility && isRequestNICForReservationsHavingDomesticSegments){
				if(UI_Passenger.isDomesticSegmentExists == true 
					&& trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoNationalIDNoNullCoe, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.displayNationalIDNo").addClass("errorControl");
				} else if(trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()) != "" &&
						!isAlphaNumeric(trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()))) {
					strPaxHTML += "<li> " + raiseError(docoNICinvalid, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").addClass("errorControl");
				}
				$(strPaxList+vl+"\\]\\.displayNationalIDNo").val(trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()));					
			} else if(paxConfig.nationalIDNo.ibeVisibility && 
					(paxConfig.nationalIDNo.ibeMandatory)){
				if(trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoNationalIDNoNullCoe, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.displayNationalIDNo").addClass("errorControl");
					showPSPTDetails = true;
				} else{
					$(strPaxList+vl+"\\]\\.displayNationalIDNo").val(trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()));
				}				
			} else {
				$(strPaxList+vl+"\\]\\.displayNationalIDNo").val(trim($("textarea"+strPaxList+vl+"\\]\\.nationalIDNoTxt").val()));
			}				

			if(paxConfig.passportExpiry.ibeVisibility && paxConfig.passportExpiry.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.foidExpiry").val()) == ""){
					strPaxHTML += "<li> " + raiseError(psptExpiryNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.foidExpiry").addClass("errorControl");
					showPSPTDetails = true;
				}
			}

			if(paxConfig.passportIssuedCntry.ibeVisibility && paxConfig.passportIssuedCntry.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.foidPlace").val()) == ""){
					strPaxHTML += "<li> " + raiseError(psptPlaceOfIssueNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.foidPlace").addClass("errorControl");
					showPSPTDetails = true;
				}
			}
			
			if(paxConfig.visaDocNumber.ibeVisibility && paxConfig.visaDocNumber.ibeMandatory){
				if(trim($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoDocNumNullCode, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").addClass("errorControl");
					showDOCODetails = true;
				} else if(!isNumeric(trim($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val()))) {
					strPaxHTML += "<li> " + raiseError(docoDocNumInvalidCode, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").addClass("errorControl");
					showDOCODetails = true;
				}
				$(strPaxList+vl+"\\]\\.visaDocNumber").val(trim($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val()));
			}else{
				if($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val() != null && 
						trim($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val()) != ""){
					if(!isNumeric(trim($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val()))) {
						strPaxHTML += "<li> " + raiseError(docoDocNumInvalidCode, " # " + (vl + 1));
						$("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").addClass("errorControl");
					}
					
					//if(paxConfig.passportNo.uniqueness != "NONE"){
					//	foidLists +=trim($("textarea"+strPaxList+vl+"\\]\\.foidNumberTxt").val())+"^" ;
					//}
					
					$(strPaxList+vl+"\\]\\.visaDocNumber").val(trim($("textarea"+strPaxList+vl+"\\]\\.visaDocNumber").val()));
				}
			}

			if(paxConfig.placeOfBirth.ibeVisibility && paxConfig.placeOfBirth.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.placeOfBirth").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoPlaceOfBirthNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.placeOfBirth").addClass("errorControl");
				}
			} else if(paxConfig.placeOfBirth.ibeVisibility && trim($(strPaxList+vl+"\\]\\.placeOfBirth").val()) != "") {
				if(!isAlphaNumeric(trim($(strPaxList+vl+"\\]\\.placeOfBirth").val()))) {
					strPaxHTML += "<li> " + raiseError(docoPlaceOfBirthInvalidCode, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.placeOfBirth").addClass("errorControl");
				}
			}

			if(paxConfig.travelDocumentType.ibeVisibility && paxConfig.travelDocumentType.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.travelDocumentType").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoDocTypeNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.travelDocumentType").addClass("errorControl");
					showDOCODetails = true;
				}
			}
			
			if(paxConfig.visaDocPlaceOfIssue.ibeVisibility && paxConfig.visaDocPlaceOfIssue.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.visaDocPlaceOfIssue").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoPlaceOfIssueNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.visaDocPlaceOfIssue").addClass("errorControl");
					showDOCODetails = true;
				}
			} else if(paxConfig.placeOfBirth.ibeVisibility && trim($(strPaxList+vl+"\\]\\.visaDocPlaceOfIssue").val()) != "") {
				if(!isAlphaNumeric(trim($(strPaxList+vl+"\\]\\.visaDocPlaceOfIssue").val()))) {
					strPaxHTML += "<li> " + raiseError(docoPlaceOfIssueInvalidCode, " # " + (vl + 1));
					$("textarea"+strPaxList+vl+"\\]\\.visaDocPlaceOfIssue").addClass("errorControl");
				}
			}

			if(paxConfig.visaDocIssueDate.ibeVisibility && paxConfig.visaDocIssueDate.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.visaDocIssueDate").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoIssueDateNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.visaDocIssueDate").addClass("errorControl");
					showDOCODetails = true;
				}
			}
			
			if(paxConfig.visaApplicableCountry.ibeVisibility && paxConfig.visaApplicableCountry.ibeMandatory){
				if(trim($(strPaxList+vl+"\\]\\.visaApplicableCountry").val()) == ""){
					strPaxHTML += "<li> " + raiseError(docoCountryNullCode, " # " + (vl + 1));
					$(strPaxList+vl+"\\]\\.visaApplicableCountry").addClass("errorControl");
					showDOCODetails = true;
				}
			}
		if (showPSPTDetails) {
			if (type == "ADT") {
				$("#paxAdTemplate_" + vl + "  .psptDetails").show();

			} else if (type == "CHD") {
				$("#paxChTemplate_" + vl + "  .psptDetails").show();

			} else if (type == "INF") {
				$("#paxInTemplate_" + vl + "  .psptDetails").show();
			}
		}
		if (showDOCODetails) {
			if (type == "ADT") {
				$("#paxAdTemplate_" + vl + "  .docoDetails").show();

			} else if (type == "CHD") {
				$("#paxChTemplate_" + vl + "  .docoDetails").show();

			} else if (type == "INF") {
				$("#paxInTemplate_" + vl + "  .docoDetails").show();
			}
		}
	}
	
	return strPaxHTML;
	
}

UI_Passenger.TelePhoneValidate = function() {
	var telHtml = "";
	//############################## Mobile Number Mandatory Validation #############################
	//check mobile no is a mandatory field
    if(contactConfig.mobileNo1.ibeVisibility && contactConfig.mobileNo1.mandatory){
    	if(trim($("#txtMCountry").val()) != "") {
    		if (validateInteger(trim($("#txtMCountry").val()))){				
    			removeZero("#txtMCountry");
    		}else {
    			$("#txtMCountry").val("");
    		}			
    	}
    	
    	if ( contactConfig.areaCode1.ibeVisibility){
	    	if(trim($("#txtMArea").val()) != "") {
	    		if (validateInteger(trim($("#txtMArea").val()))){				
	    			removeZero("#txtMArea");
	    		}else {
	    			$("#txtMArea").val("");
	    		}		
	    	}
    	}
    	
    	if(trim($("#txtMobile").val()) != "") {
    		if (!validateInteger($("#txtMobile").val())){
    			$("#txtMobile").val("");
    		}		
    			
    	}
    	    	
    	if (trim($("#txtMobile").val()) == ""){
    		telHtml += "<li>" + raiseError("ERR030");	
    		$("#txtMobile").addClass("errorControl");	
    		$("#txtMCountry").addClass("errorControl");
    		$("#txtMArea").addClass("errorControl");
    	}
    	
    	if (trim($("#txtMobile").val()) != ""){		
    		if (trim($("#txtMCountry").val()) == ""){
    			telHtml += "<li> " + raiseError("ERR036");
    			$("#txtMCountry").addClass("errorControl");	
    		}
    		
    		if ( contactConfig.areaCode1.ibeVisibility  ){
	    		if (trim($("#txtMArea").val()) == ""){
	    			telHtml += "<li> " + raiseError("ERR037");
	    			$("#txtMArea").addClass("errorControl");	
	    		}
    		}else{
    			if ( trim($("#txtMobile").val()).length < 3 ){
    				telHtml += "<li>" + raiseError("ERR051");	
    	    		$("#txtMobile").addClass("errorControl");	
    			}
    		}
    	}
    	
    	telHtml += UI_Passenger.validateMobileNoFormat("#txtMCountry","#txtMArea","#txtMobile");
    	
    //check mobile no is not mandatory & user inserted a mobile no
	} else if(contactConfig.mobileNo1.ibeVisibility &&
			!contactConfig.mobileNo1.mandatory &&
			trim($("#txtMobile").val()) != ""){
		
		if (validateInteger(trim($("#txtMCountry").val()))){				
			removeZero("#txtMCountry");
		}else {
			telHtml += "<li> " + raiseError("ERR036");
			$("#txtMCountry").addClass("errorControl");
			$("#txtMCountry").val("");
		}
		
		if ( contactConfig.areaCode1.ibeVisibility 
				 && trim($("#txtMArea").val()) != ""){
			if (validateInteger(trim($("#txtMArea").val()))){				
				removeZero("#txtMArea");
			}else {
				telHtml += "<li> " + raiseError("ERR037");
    			$("#txtMArea").addClass("errorControl");	
				$("#txtMArea").val("");
			}
		}
		if ( contactConfig.areaCode1.ibeVisibility ){
			if (validateInteger($("#txtMobile").val())){				
				removeZero("#txtMobile");
			}else {
				$("#txtMobile").val("");
			}
		}else{
			if (trim($("#txtMobile").val()).length < 3){
				telHtml += "<li>" + raiseError("ERR051");	
    			$("#txtMobile").addClass("errorControl");
			}
		}
		telHtml += UI_Passenger.validateMobileNoFormat("#txtMCountry","#txtMArea","#txtMobile");
		
	}
     
    //############################ Phone Number Mandatory Validation ####################################
    //check phone no is a mandatory field
    if(contactConfig.phoneNo1.ibeVisibility && contactConfig.phoneNo1.mandatory){
    	if(trim($("#txtPCountry").val()) != "") {
    		if (validateInteger($("#txtPCountry").val())){				
    			removeZero("#txtPCountry");
    		}else {
    			$("#txtPCountry").val("");
    		}			
    	}
    	if ( contactConfig.areaCode1.ibeVisibility  ){
	    	if(trim($("#txtPArea").val()) != "") {
	    		if (validateInteger($("#txtPArea").val())){				
	    			removeZero("#txtPArea");
	    		}else {
	    			$("#txtPArea").val("");
	    		}		
	    	}
    	}
    	if(trim($("#txtPhone").val()) != "") {
    		if (!validateInteger($("#txtPhone").val())){
    			$("#txtPhone").val("");
    		}		
    	}
    	
    	if (trim($("#txtPhone").val()) == ""){
    		telHtml += "<li>" + raiseError("ERR030");
    		$("#txtPhone").addClass("errorControl");	
    		$("#txtPCountry").addClass("errorControl");
    		$("#txtPArea").addClass("errorControl");	
    	}
    	
    	if (trim($("#txtPhone").val()) != ""){
    		if (trim($("#txtPCountry").val()) == ""){
    			telHtml += "<li> " + raiseError("ERR034");
    			$("#txtPCountry").addClass("errorControl");	
    		}
    		
    		if ( contactConfig.areaCode1.ibeVisibility  ){
	    		if (trim($("#txtPArea").val()) == ""){
	    			telHtml += "<li> " + raiseError("ERR035");
	    			$("#txtPArea").addClass("errorControl");	
	    		}
    		}else{
    			if ( trim($("#txtPhone").val()).length < 3 ){
    				telHtml += "<li>" + raiseError("ERR051");	
    	    		$("#txtPhone").addClass("errorControl");	
    			}
    		}
    	}
    	
    	telHtml += UI_Passenger.validatePhoneNoFormat("#txtPCountry","#txtPArea","#txtPhone");
    	
    //check phone no is not mandatory & user inserted a phone no
    } else if(contactConfig.phoneNo1.ibeVisibility &&
    		!contactConfig.phoneNo1.mandatory &&
			trim($("#txtPhone").val()) != ""){
    	if (validateInteger($("#txtPCountry").val())){				
			removeZero("#txtPCountry");
		}else {
			telHtml += "<li> " + raiseError("ERR034");
			$("#txtPCountry").addClass("errorControl");	
			$("#txtPCountry").val("");
		}
    	
    	if ( contactConfig.areaCode1.ibeVisibility 
    			&& trim($("#txtPArea").val()) != "" ){
	    	if (validateInteger($("#txtPArea").val())){				
				removeZero("#txtPArea");
			}else {
				telHtml += "<li> " + raiseError("ERR035");
    			$("#txtPArea").addClass("errorControl");	
				$("#txtPArea").val("");
			}
    	}
    	if ( contactConfig.areaCode1.ibeVisibility ){
	    	if (!validateInteger($("#txtPhone").val())){				
	    		$("#txtPhone").val("");
			}
    	}else{
    		if (trim($("#txtPhone").val()).length < 3){
    			telHtml += "<li>" + raiseError("ERR051");	
    			$("#txtPhone").addClass("errorControl");
    		}
    	}
    	telHtml += UI_Passenger.validatePhoneNoFormat("#txtPCountry","#txtPArea","#txtPhone");
    	
    }
    
  //############################## Selected Country and Contry Code match ################################
    
    if($("#selCountry").val() != "" && !UI_Passenger.validateCountryAndAreaCodes()){
    	telHtml += "<li>" + raiseError("ERR119");
		$("#selCountry").addClass("errorControl");	
		$("#txtMCountry").addClass("errorControl");
		$("#txtMArea").addClass("errorControl");	
    }
    
   
    
  //############################ Fax Number Mandatory Validation ####################################
    //check fax no is a mandatory field
    if(contactConfig.fax1.ibeVisibility && contactConfig.fax1.mandatory){
    	if(trim($("#txtFCountry").val()) != "") {
    		if (validateInteger($("#txtFCountry").val())){				
    			removeZero("#txtFCountry");
    		}else {
    			$("#txtFCountry").val("");
    		}			
    	}
    	if (contactConfig.areaCode1.ibeVisibility){
	    	if(trim($("#txtFArea").val()) != "") {
	    		if (validateInteger($("#txtFArea").val())){				
	    			removeZero("#txtFArea");
	    		}else {
	    			$("#txtFArea").val("");
	    		}		
	    	}
    	}
    	if(trim($("#txtFax").val()) != "") {
    		if (validateInteger($("#txtFax").val())){				
    			removeZero("#txtFax");
    		}else {
    			$("#txtFax").val("");
    		}		
    	}
    	
    	if (trim($("#txtFax").val()) == ""){
    		telHtml += "<li>" + raiseError("ERR066");
    		$("#txtFax").addClass("errorControl");	
    		$("#txtFCountry").addClass("errorControl");
    		$("#txtFArea").addClass("errorControl");	
    	}
    	
    	if (trim($("#txtFax").val()) != ""){
    		if (trim($("#txtFCountry").val()) == ""){
    			telHtml += "<li> " + raiseError("ERR067");
    			$("#txtFCountry").addClass("errorControl");	
    		}
    		
    		if (contactConfig.areaCode1.ibeVisibility){
	    		if (trim($("#txtFArea").val()) == ""){
	    			telHtml += "<li> " + raiseError("ERR068");
	    			$("#txtFArea").addClass("errorControl");	
	    		}
    		}else{
    			if ( trim($("#txtFax").val()).length < 3 ){
    				telHtml += "<li>" + raiseError("ERR051");	
    	    		$("#txtFax").addClass("errorControl");	
    			}
    		}
    	}
    	
    	telHtml += UI_Passenger.validatePhoneNoFormat("#txtFCountry","#txtFArea","#txtFax");
    	
    //check phone no is not mandatory & user inserted a phone no
    } else if(contactConfig.fax1.ibeVisibility &&
    		!contactConfig.fax1.mandatory &&
			trim($("#txtFax").val()) != ""){
    	if (validateInteger($("#txtFCountry").val())){				
			removeZero("#txtFCountry");
		}else {
			telHtml += "<li> " + raiseError("ERR067");
			$("#txtFCountry").addClass("errorControl");	
			$("#txtFCountry").val("");
		}
    	
    	if (contactConfig.areaCode1.ibeVisibility
    			&& trim($("#txtFArea").val()) != ""){
	    	if (validateInteger($("#txtFArea").val())){				
				removeZero("#txtFArea");
			}else {
				telHtml += "<li> " + raiseError("ERR068");
    			$("#txtFArea").addClass("errorControl");
				$("#txtFArea").val("");
			}
    	}
    	
    	if ( contactConfig.areaCode1.ibeVisibility ){
	    	if (validateInteger($("#txtFax").val())){				
				removeZero("#txtFax");
			}else {
				$("#txtFax").val("");
			}
    	}else{
    		if (trim($("#txtFax").val()).length < 3){
    			telHtml += "<li>" + raiseError("ERR051");	
    			$("#txtFax").addClass("errorControl");
    		}
    	}
    	
    	telHtml += UI_Passenger.validatePhoneNoFormat("#txtFCountry","#txtFArea","#txtFax");
    	
    }
    if (telHtml != ""){
    	UI_Passenger.boolValidate = false;
    }else{
    	UI_Passenger.boolValidate = true;
    }
	return telHtml;
}

UI_Passenger.setAreCode = function(){
	if ( contactConfig.areaCode1 != undefined && !contactConfig.areaCode1.ibeVisibility ){
		if ( $("#txtPhone").val() != ""){
			$("#txtPArea").val(trim($("#txtPhone").val()).substr(0,2));
			$("#txtPhone").val(trim($("#txtPhone").val()).substr(2,trim($("#txtPhone").val()).length));
		}
		if ( $("#txtMobile").val() != ""){
			$("#txtMArea").val(trim($("#txtMobile").val()).substr(0,2));
			$("#txtMobile").val(trim($("#txtMobile").val()).substr(2,trim($("#txtMobile").val()).length));
		}
		if ( $("#txtFax").val() != ""){
			$("#txtFArea").val(trim($("#txtFax").val()).substr(0,2));
			$("#txtFax").val(trim($("#txtFax").val()).substr(2,trim($("#txtFax").val()).length));
		}
		if ( $("#txtEmgnTelephone").val() != ""){
			$("#txtEmgnPArea").val(trim($("#txtEmgnPhone").val()).substr(0,2));
			$("#txtEmgnPhone").val(trim($("#txtEmgnPhone").val()).substr(2,trim($("#txtEmgnPhone").val()).length))
		}
	}
}

UI_Passenger.reSetAreCode = function(){
	if ( contactConfig.areaCode1 != undefined && !contactConfig.areaCode1.ibeVisibility ){
		if ( $("#txtPhone").val() != ""){
			$("#txtPhone").val($("#txtPArea").val() + $("#txtPhone").val());
		}
		if ( $("#txtMobile").val() != ""){
			$("#txtMobile").val($("#txtMArea").val() + $("#txtMobile").val());
		}
		if ( $("#txtFax").val() != ""){
			$("#txtFax").val($("#txtFArea").val() + $("#txtFax").val());
		}
		if ( $("#txtEmgnTelephone").val() != ""){
			$("#txtEmgnPhone").val($("#txtEmgnPArea").val() + $("#txtEmgnPhone").val())
		}
	}
}
//check whether phone number format is correct
UI_Passenger.validatePhoneNoFormat = function(fieldCountryCode, fieldAreaCode, fieldPhone){
	var telHtml = "";
	var lancountryCode = trim($(fieldCountryCode).val());
	var lanareaCode =    trim($(fieldAreaCode).val());
	var lanno =    trim($(fieldPhone).val());
	var blnLanFound = true;
	var hsaLanNo = false;
	var selectedCountry = trim($("#selCountry").val());
	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && lanno != "" && lancountryCode != "") {
	
		for(var pl=0;pl < arrCountryPhone.length;pl++) {
			if(lancountryCode == arrCountryPhone[pl][0]) {
				hsaLanNo = true;
				if(arrCountryPhone[pl][1] == 'Y' && contactConfig.areaCode1.ibeVisibility){
					if(typeof(arrAreaPhone[lancountryCode]) != 'undefined') {
						for(var al=0;al < arrAreaPhone[lancountryCode].length; al++){
							if(arrAreaPhone[lancountryCode][al][0] == lanareaCode 
								&& arrAreaPhone[lancountryCode][al][1] == 'LAND'
								&& arrAreaPhone[lancountryCode][al][2] == 'ACT') {
								blnLanFound = true;
								break;									
							} else {
								blnLanFound = false;
							}
						}
						
						if(!blnLanFound) {
							telHtml += "<li> " + raiseError("ERR050");
							$(fieldCountryCode).addClass("errorControl");		
							$(fieldAreaCode).addClass("errorControl");	
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][2] != ''){
					if(lanno != ""  && contactConfig.areaCode1.ibeVisibility) {
						if(lanno.length < Number(arrCountryPhone[pl][2])) {
							telHtml += "<li> " + raiseError("ERR051");
							$(fieldPhone).addClass("errorControl");									
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][3] != ''){
					
					if(lanno != ""  && contactConfig.areaCode1.ibeVisibility) {
						if(lanno.length > Number(arrCountryPhone[pl][3])) {
							telHtml += "<li> " + raiseError("ERR052");
							$(fieldPhone).addClass("errorControl");									
						}
					
					}
				
				
				}
			
			} 
		
		}
		
		if(!hsaLanNo) {
			telHtml += "<li> " + raiseError("ERR050");
			$(fieldCountryCode).addClass("errorControl");		
			$(fieldAreaCode).addClass("errorControl");	
		}				
	
	}
	
	return telHtml;
}

//check whether mobile num format is correct
UI_Passenger.validateMobileNoFormat = function(fieldCountryCode, fieldAreaCode, fieldMobile){
	var telHtml = "";
	var countryCode = trim($(fieldCountryCode).val());
	var areaCode =    trim($(fieldAreaCode).val());
	var mobileno =    trim($(fieldMobile).val());
	var blnFound = true;
	var hsaNo = false;
	var selectedCountry = trim($("#selCountry").val());			
	
	if ((selectedCountry != "OT" && selectedCountry != "" ) && mobileno != "" && countryCode != "") {
	
		for(var pl=0;pl < arrCountryPhone.length;pl++) {					
			if(countryCode == arrCountryPhone[pl][0]) {
				hsaNo = true;
				if(arrCountryPhone[pl][1] == 'Y' && contactConfig.areaCode1.ibeVisibility){
					if(typeof(arrAreaPhone[countryCode]) != 'undefined') {
						for(var al=0;al < arrAreaPhone[countryCode].length; al++){
							if(arrAreaPhone[countryCode][al][0] == areaCode 
								&& arrAreaPhone[countryCode][al][1] == 'MOBILE'
								&& arrAreaPhone[countryCode][al][2] == 'ACT') {
								blnFound = true;
								break;									
							} else {
								blnFound = false;
							}
						}
						
						if(!blnFound) {
							telHtml += "<li> " + raiseError("ERR050");
							$(fieldCountryCode).addClass("errorControl");		
							$(fieldAreaCode).addClass("errorControl");	
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][2] != ''){
					if(mobileno != "") {
						if( contactConfig.areaCode1.ibeVisibility ){
							if(mobileno.length < Number(arrCountryPhone[pl][2])) {
								telHtml += "<li> " + raiseError("ERR051");
								$(fieldMobile).addClass("errorControl");									
							}
						}
					
					}
				
				}
				
				if(arrCountryPhone[pl][3] != ''){
					
					if(mobileno != "") {
						if( contactConfig.areaCode1.ibeVisibility ){
							if(mobileno.length > Number(arrCountryPhone[pl][3])) {
								telHtml += "<li> " + raiseError("ERR052");
								$(fieldMobile).addClass("errorControl");									
							}
						}
					}
				
				
				}
			
			} 
		
		}
		if(!hsaNo) {
			telHtml += "<li> " + raiseError("ERR050");
			$(fieldCountryCode).addClass("errorControl");		
			$(fieldAreaCode).addClass("errorControl");	
		}
						
	
	}
	
	return telHtml;
}

UI_Passenger.setCountryPhone = function () {
	$("#txtMCountry").val("");
	$("#txtPCountry").val("");
	$("#txtFCountry").val("");
	$("#txtMArea").val("");
	
	var choseCountry = $("#selCountry").val();
	for(var x=0;x<arrCountryPhone.length;x++) {
		if(choseCountry == arrCountryPhone[x][4]) {
			$("#txtMCountry").val(arrCountryPhone[x][0]);
			$("#txtFCountry").val(arrCountryPhone[x][0]);	
			
			/*
			 * The following piece of Code is Specific for W5.
			 * It prepends '9' for the area code of the Mobile Number.
			 * And Show Help Labels
			 * */
			if(choseCountry == 'IR' && UI_Top.holder().GLOBALS.addMobileAreaCodePrefix){
					
				$("#txtMArea").val('9');
				$("#txtMArea").on("keydown",function(){		
					var len = $(this).val().length;
					$(this)[0].setSelectionRange( len, len ); 
				});
				
				$("#txtMArea").on("keyup",function(){		
					var len = $(this).val().length;
					if(len == 0){
						$(this).val('9');
					} 
				});
			}
			break;
		}
	}

}

UI_Passenger.setCountryName = function(){
	$("#countryName").val($("#selCountry option:selected").text());	
}

UI_Passenger.validateCountryAndAreaCodes = function(){
	
	var choseCountry = $("#selCountry").val();
	for(var x=0;x<arrCountryPhone.length;x++) {
		
		if(choseCountry == arrCountryPhone[x][4]) {			
			if(	$("#txtMCountry").val() == arrCountryPhone[x][0]){
				if(choseCountry == 'IR' && UI_Top.holder().GLOBALS.addMobileAreaCodePrefix){
					/*
					 * The following piece of Code is Specific for W5.
					 * It prepends '9' for the area code of the Mobile Number.
					 * And Show Help Labels
					 * */	
					if($("#txtMArea").val().toString().charAt(0) != '9'){
						$("#txtMArea").val('9');
						return false;
					}
				}
				return true;
			}else{
				return false;
			}	
		}
	}
	
}

//TODO   move all the following methods to a common file

function isAlphaWhiteSpace(s){
	var re = new RegExp("^[a-zA-Z-/ \w\s]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
}

function isAlphaNumeric(s) {
	var re = new RegExp("^[a-zA-Z0-9]+$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp("^[a-zA-Z0-9]+$").test(s);
}

function validateInteger( strValue ) {
	var objRegExp  = /(^-?[0-9][0-9]*$)/;

  	//check for integer characters
  	return objRegExp.test(strValue);
}

function removeZero(field) {
	var val = trim($(field).val());
	$(field).val(Number(val) * 1);		
}

function checkEmail(s){
	var re = new RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$");
	var ve = re.exec(s);
	if (ve == null) {return false;}else{return true}
	//return RegExp( "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$" ).test(s);
}

function validateZipCode(field){
	var valid = "0123456789-";
	var hyphencount = 0;
	
	if (field.length!=5 && field.length!=10) {
		return false;
	}
	for (var i=0; i < field.length; i++) {
		var temp = "" + field.substring(i, i+1);
		if (temp == "-"){
			hyphencount++;
		}		
		if (valid.indexOf(temp) == "-1") {
			return false;
		}
		if ((hyphencount > 1) || ((field.length==10) && ""+field.charAt(5)!="-")) {
			return false;
		}
	}
	return true;
}

// Set empty data for if <empty> txtMobile OR txtPhone
UI_Passenger.setContactNumberValidData = function() {
	if(trim($("#txtMobile").val()) == "") {
		$("#txtMCountry").val("");
		$("#txtMArea").val("");
	}
	if(trim($("#txtPhone").val()) == "") {
		$("#txtPCountry").val("");
		$("#txtPArea").val("");
	}	
}
// Override calander method
// To do : find bug for new Lcc pasenger page
function showCalendar(objEvent){
	var objContainer = document.getElementById(window._CalendarLayer);
	var strGridHTMLText = "" ;
	for (var i = 0 ; i < objContainer.arrCal.length ; i++){
		var objCal = objContainer.arrCal[i];
		if (objCal.spanTagID == this.spanTagID){
			var strDelayFunction = "delayCalendar('" + objCal.spanTagID + "')"
			
			var x = 0 ;
			var y = 0 ; 
			if (objEvent != null){
				if (window._CalBrowser) {
					x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
					y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
				}else{
					x = objEvent.clientX + window.scrollX;
					y = objEvent.clientY + window.scrollY;
				}	
			}
			if (objCal.top == 0){objCal.top = y + 20;}
			if (objCal.left == 0){objCal.left = x;}
			
			objCal._calDelay = setInterval(strDelayFunction, 300);
			//### override code 
			var strDivCalID = 'div_Calendar_' + objCal.spanTagID ;
			$("#"+strDivCalID).css({top:objCal.top, left:objCal.left});
			//###			
			break;
		}
	}		
}

UI_Passenger.setContactData = function(contactInfo, accessPoint, regUserAccessInKiosk, showRegUserLoginInPaxDetailPage) {
	//IE6 Fixed, wait till the select box items are loaded
	loadAgain = function(){
		$("#selNationality").val(contactInfo.nationality);
		$("#selCountry").val(contactInfo.country);
		UI_Passenger.setCountryName();
		$("#selTitle").val(contactInfo.title);
	}
	if (contactInfo.title != null) {
		$("#txtFName").val(contactInfo.firstName);
		$("#txtLName").val(contactInfo.lastName);
		$("#txtPCountry").val(contactInfo.lCountry);
		$("#txtMCountry").val(contactInfo.mCountry);
		$("#txtFCountry").val(contactInfo.fCountry);
		
		if(contactConfig == "" || contactConfig == null){
			$("#txtPArea").val(contactInfo.lArea);
			$("#txtPhone").val(contactInfo.lNumber);
			$("#txtMArea").val(contactInfo.mArea);
			$("#txtMobile").val(contactInfo.mNumber);
			$("#txtFArea").val(contactInfo.fArea);
			$("#txtFax").val(contactInfo.fNumber);
		} else {			
			if(!contactConfig.areaCode1.ibeVisibility){
				$("#txtPhone").val(contactInfo.lArea+contactInfo.lNumber);
				$("#txtMobile").val(contactInfo.mArea+contactInfo.mNumber);
				$("#txtFax").val(contactInfo.fArea+contactInfo.fNumber);
			}else{
				$("#txtPArea").val(contactInfo.lArea);
				$("#txtPhone").val(contactInfo.lNumber);
				$("#txtMArea").val(contactInfo.mArea);
				$("#txtMobile").val(contactInfo.mNumber);
				$("#txtFArea").val(contactInfo.fArea);
				$("#txtFax").val(contactInfo.fNumber);
			}
		}
		
		$("#txtEmail").val(contactInfo.emailAddress);	
		$("#txtVerifyEmail").val(contactInfo.emailAddress);
		$("#txtCity").val(contactInfo.city);		
		$("#txtStreet").val(contactInfo.addresStreet);
		$("#txtAddress").val(contactInfo.addresline);
		$("#txtZipCode").val(contactInfo.zipCode);
		
		if (UI_Container.addModifyAncillary == true || UI_Container.modifySegment == true) {
			$("#selNationality").addOption(contactInfo.nationality);
			$("#selCountry").addOption(contactInfo.country);
			$("#selTitle").addOption(contactInfo.title);
		} else {
			try{
				$("#selNationality").val(contactInfo.nationality);
				$("#selCountry").val(contactInfo.country);
				UI_Passenger.setCountryName();
				$("#selTitle").val(contactInfo.title);
			}catch(e){
				setTimeout('loadAgain()',200);
			}
		}
		
		//set emergency contact details
		$("#selEmgnTitle").val(contactInfo.emgnTitle);
		$("#txtEmgnFName").val(contactInfo.emgnFirstName);
		$("#txtEmgnLName").val(contactInfo.emgnLastName);
		$("#txtEmgnPCountry").val(contactInfo.emgnLCountry);
		
		if(contactConfig == "" || contactConfig == null){
			$("#txtEmgnPArea").val(contactInfo.emgnLArea);
			$("#txtEmgnPhone").val(contactInfo.emgnLNumber);
		} else {			
			if(!contactConfig.areaCode1.ibeVisibility){
				$("#txtEmgnPhone").val(contactInfo.emgnLArea+contactInfo.emgnLNumber);
			}else{
				$("#txtEmgnPArea").val(contactInfo.emgnLArea);
				$("#txtEmgnPhone").val(contactInfo.emgnLNumber);
			}
		}		
		
		$("#txtEmgnEmail").val(contactInfo.emgnEmail);
	
		// Hide Registerd User Pannel
		if (anciConfig.paxNewLayout == undefined && !anciConfig.paxNewLayout){
			$("#trRegisterdUser").hide();
			$("#trUpdateCustProfile").show();
			profEmailAddress = contactInfo.emailAddress;
			$("#txtEmail").keyup(function(){UI_Passenger.validateProfileEmail(this)});
			$("#txtEmail").keypress(function(){UI_Passenger.validateProfileEmail(this)});
		}else{
			$("#trRegisterdUser").hide();
			$("#trUpdateCustProfile").hide();
		}
		
	} else {
		$("#trUpdateCustProfile").hide();
		// Show Registerd User Pannel
		if((accessPoint != null && accessPoint == "KSK" && regUserAccessInKiosk == "N")){
			$("#trRegisterdUser").hide();
		} else if(showRegUserLoginInPaxDetailPage) {
			$("#trRegisterdUser").hide();
            $(".clearAutoGenPax").hide();
		}
		else {
			//$("#trRegisterdUser").show();
		}
	}
	
}

UI_Passenger.createFirstPaxFromContactInfo = function(contactInfo, lmsDetails){
	var lowestAge = 12;
	var label = UI_Container.getLabels();
	if(UI_commonSystem.loyaltyManagmentEnabled){
		if (UI_commonSystem.getAge(lmsDetails.dateOfBirth,UI_Passenger.extractFlightFirstSegOutDate()) > lowestAge*365+(lowestAge/4)){
			populateAdult = function(){
				$("#adultList\\[0\\]\\.title").val(contactInfo.title);
				$("#adultList\\[0\\]\\.firstName").val(contactInfo.firstName);
				$("#adultList\\[0\\]\\.lastName").val(contactInfo.lastName);
				$("#adultList\\[0\\]\\.nationality").val($("#selNationality option:selected").val());
				$("#adultList\\[0\\]\\.title").focus();
				if((paxConfigCH.ffid.ibeVisibility || paxConfigAD.ffid.ibeVisibility)
						 && !UI_Passenger.interlineRes){
					$("#adultList\\[0\\]\\.ffidTxt").val(contactInfo.emailAddress);
				}
				if(lmsDetails != null){
					if(lmsDetails.dateOfBirth != null && lmsDetails.dateOfBirth != ''){
						$("#adultList\\[0\\]\\.dateOfBirth").val(lmsDetails.dateOfBirth);
					}
				}
				$("#adultList\\[0\\]\\.firstName").click();
			}
			var populateMsg = $("#hdnPopulateMsg").html();
			var populateTopic = $("#hdnPopulateTopic").html();
			if(($("#adultList\\[0\\]\\.firstName").val()==null || $("#adultList\\[0\\]\\.firstName").val()=="")
					&& ($("#adultList\\[0\\]\\.lastName").val()==null || $("#adultList\\[0\\]\\.lastName").val()=="")) {
				populateFirstPaxCheck = false;
			} else if ($("#adultList\\[0\\]\\.firstName").val().toLowerCase() == contactInfo.firstName.toLowerCase()
					&& $("#adultList\\[0\\]\\.lastName").val().toLowerCase() == contactInfo.lastName.toLowerCase()){
				populateFirstPaxCheck = false;
			} else {
				populateFirstPaxCheck = true;
			}
			if(populateFirstPaxCheck){
				jConfirm(populateMsg, populateTopic, function(response) {
					if (response){					
						populateAdult();
					} else {
						
					}
				}, label["lblYes"] , label["lblNo"]);
			} else {
				populateAdult();
			}
		}
		else if((UI_Passenger.paxDetails.childList.length > 0) 
				&& (UI_commonSystem.getAge(lmsDetails.dateOfBirth,UI_Passenger.extractFlightFirstSegOutDate()) > 2*365+(2/4))){			
			populateChild = function(){
				$("#childList\\[0\\]\\.chdTitle").val(contactInfo.title);
				$("#childList\\[0\\]\\.firstName").val(contactInfo.firstName);
				$("#childList\\[0\\]\\.lastName").val(contactInfo.lastName);
				$("#childList\\[0\\]\\.nationality").val($("#selNationality option:selected").val());
				$("#childList\\[0\\]\\.title").focus();
				if((paxConfigCH.ffid.ibeVisibility || paxConfigAD.ffid.ibeVisibility)
						 && !UI_Passenger.interlineRes){
					$("#childList\\[0\\]\\.ffidTxt").val(contactInfo.emailAddress);
				}
				if(lmsDetails != null){
					if(lmsDetails.dateOfBirth != null && lmsDetails.dateOfBirth != ''){
						$("#childList\\[0\\]\\.dateOfBirth").val(lmsDetails.dateOfBirth);
					}
				}
				$("#childList\\[0\\]\\.firstName").click();
			}
			var populateMsg = "<div>The account with this Airewards ID already exists. If you want to merge please click Ok.</div>";
			var populateTopic = "Mege Account";
						
			if (($("#childList\\[0\\]\\.firstName").val()==null || $("#childList\\[0\\]\\.firstName").val()=="")
					&& ($("#childList\\[0\\]\\.lastName").val()==null || $("#childList\\[0\\]\\.lastName").val()=="")) {
				populateFirstPaxCheck = false;
			} else if($("#childList\\[0\\]\\.firstName").val().toLowerCase() == contactInfo.firstName.toLowerCase()
					&& $("#childList\\[0\\]\\.lastName").val().toLowerCase() == contactInfo.lastName.toLowerCase()){
				populateFirstPaxCheck = false;
			} else {
				populateFirstPaxCheck = true;
			}	
			if(populateFirstPaxCheck){
				
				jConfirm(populateMsg, populateTopic, function(response) {
					if (response){					
						populateChild();
					} else {
						
					}
				});
			} else {
				populateChild();
			}
		}
	}
	else{
		$("#adultList\\[0\\]\\.title").val(contactInfo.title);
		$("#adultList\\[0\\]\\.firstName").val(contactInfo.firstName);
		$("#adultList\\[0\\]\\.lastName").val(contactInfo.lastName);
		$("#adultList\\[0\\]\\.nationality").val($("#selNationality option:selected").val());
		if(contactInfo.dateOfBirth != null && contactInfo.dateOfBirth != ''){
			$("#adultList\\[0\\]\\.dateOfBirth").val(contactInfo.dateOfBirth);
		}
	}
	
    if(contactInfo.firstName!="" && contactInfo.lastName!="") {
        $(".clearAutoGenPax").show();
    }
}

// clears the passenger information when "clear" is clicked
UI_Passenger.clearPaxInfo = function(){
	var paxDetails = UI_Passenger.paxDetails;
	
	for(var pl=0;pl < paxDetails.adultList.length;pl++){
		
		$("#adultList\\["+pl+"\\]\\.title").val("");
		$("#adultList\\["+pl+"\\]\\.firstName").val("");
		$("#adultList\\["+pl+"\\]\\.lastName").val("");
		$("#adultList\\["+pl+"\\]\\.nationality").val("");
		$("#adultList\\["+pl+"\\]\\.dateOfBirth").val("");
		$("#adultList\\["+pl+"\\]\\.ffidTxt").val("");
	}
	
	for(var pl=0;pl < paxDetails.childList.length;pl++){
		
		$("#childList\\["+pl+"\\]\\.title").val("");
		$("#childList\\["+pl+"\\]\\.firstName").val("");
		$("#childList\\["+pl+"\\]\\.lastName").val("");
		$("#childList\\["+pl+"\\]\\.nationality").val("");
		$("#childList\\["+pl+"\\]\\.dateOfBirth").val("");
		$("#childList\\["+pl+"\\]\\.ffidTxt").val("");
	}
	
	for(var pl=0;pl < paxDetails.infantList.length;pl++){
		
		$("#infantList\\["+pl+"\\]\\.title").val("");
		$("#infantList\\["+pl+"\\]\\.firstName").val("");
		$("#infantList\\["+pl+"\\]\\.lastName").val("");
		$("#infantList\\["+pl+"\\]\\.nationality").val("");
		$("#infantList\\["+pl+"\\]\\.dateOfBirth").val("");
		$("#infantList\\["+pl+"\\]\\.travelWith").val("")
	}
}

UI_Passenger.validateProfileEmail = function(field){
	if((field.value).toUpperCase() != profEmailAddress.toUpperCase()){
		$("#chkUpdateCustProf").attr("checked", false);
		$("#chkUpdateCustProf").disable();
	} else {
		$("#chkUpdateCustProf").enable();
	}
}

UI_Passenger.fotmatDateInfantOnBlur = function(objControl){
	if (objControl.value != ""){
		var strReturn = dateChk(objControl.value)
		if (strReturn){
			objControl.value = strReturn;
		}		
	}
}

// Validation for numeric
UI_Passenger.onlyNumericTxt = function(objC){
	if (!validateInteger(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}	
}
// Email Confirm alert
UI_Passenger.chkEmailChange = function (objC) {
	if (objC.checked) {
		if($.trim($("#txtEmail").val()) == "" &&
			$.trim($("#txtVerifyEmail").val()) == ""){
			$("#txtEmail").val("");
			$("#txtVerifyEmail").val("");			
			$("#txtEmail").removeClass("errorControl").disable();
			$("#txtVerifyEmail").removeClass("errorControl").disable();
		} else{
			//if(confirm(strMsgEmailConfirm)){
			jConfirm(strMsgEmailConfirm, 'Confirm', function(r){
				if (r == true){
					$("#txtEmail").val("");
					$("#txtVerifyEmail").val("");			
					$("#txtEmail").removeClass("errorControl").disable();
					$("#txtVerifyEmail").removeClass("errorControl").disable();
				}else{
					objC.checked = false;
				}
			});
				/*$("#txtEmail").val("");
				$("#txtVerifyEmail").val("");			
				$("#txtEmail").removeClass("errorControl").disable();
				$("#txtVerifyEmail").removeClass("errorControl").disable();
			} else {
				objC.checked = false;
			}		*/			
		}			
	} else {
		$("#txtEmail").enable()
		$("#txtVerifyEmail").enable();
	}
}

// Invoke user login action 
UI_Passenger.loginClick = function(){
	UI_commonSystem.pageOnChange();
	if(!loginValidation( $('#txtUID'), $('#txtPWD') )) return;	
	UI_Container.showLoading();
	var url = "ajaxLogin.action";
	var  data = new Object();
	data["txtUID"] = $('#txtUID').val().toUpperCase();
	data["txtPWD"] = $('#txtPWD').val();
	data["page"] = "interline";
	$("#btnSignIn").ajaxSubmit({ type: "POST", dataType: 'json', processData: false, data:data, url:url,							
		success: UI_Passenger.userLoginSucess, error:UI_commonSystem.setErrorStatus});
}	

UI_Passenger.autoLogin = function(retrivedEmail){
	if( (typeof retrivedEmail == 'undefined') || (retrivedEmail == 'undefined') ){
		jAlert(UI_commonSystem.alertText({msg:"Invaid login, please try again",language:strLanguage}));
		return false;
	}
	UI_commonSystem.pageOnChange();
	UI_Container.showLoading();
	var url = "ajaxLogin.action";
	var  data = new Object();
	data["socialLogin"] = true;
	data["txtUID"] = retrivedEmail;
	data["page"] = "interline";
	$("#btnSignIn").ajaxSubmit({ type: "POST", dataType: 'json', processData: false, data:data, url:url,							
		success: UI_Passenger.userLoginSucess, error:UI_commonSystem.setErrorStatus});
}	

UI_Passenger.populateRegister = function (response){
	if( (typeof (response.retrivedEmail) == 'undefined') || (response.retrivedEmail == 'undefined') ){
		jAlert(UI_commonSystem.alertText({msg:"Invaid login, please try again",language:strLanguage}));
		return false;
	}
	$("#txtLoginEmail").val(response.retrivedEmail);
	
	var randomString = Math.random().toString(36);
	if(randomString.length > 10){
		randomString = randomString.substr(0,10);
	}
	$("#txtPassword").val(randomString);
	$("#txtConPassword").val(randomString);
	
	if(typeof (response.gender) != 'undefined' && trim(response.gender) != ""){
		if (response.gender.toLowerCase().indexOf("f") > -1){
			$("#selTitle").val("MS");
		}else{
			$("#selTitle").val("MR");
		}
	}
	$("#txtFName").val(response.firstName);
	$("#txtLName").val(response.lastName);
}


// To Do:Old Common method==>New JqueryCommon method
// Login validation
function loginValidation(uidOBJ, pwOBJ){
	if (uidOBJ.val() == ""){
		jAlert(UI_commonSystem.alertText({msg:arrError["ERR056"],language:strLanguage}));
		uidOBJ.focus();
		return false;
	}
	
	if (pwOBJ.val() == ""){
		jAlert(UI_commonSystem.alertText({msg:arrError["ERR057"], language:strLanguage}));
		pwOBJ.focus();
		return false
	}
	
	var strChkEmpty = FindChar(uidOBJ.val())
	if (strChkEmpty != "0"){
		jAlert(UI_commonSystem.alertText({msg:buildError(arrError["ERR058"], strChkEmpty[0], arrError["ERR059"]), language:strLanguage}));
		uidOBJ.focus();
		return false
	}
	
	strChkEmpty = FindChar(pwOBJ.val())
	if (strChkEmpty != "0"){
		jAlert(UI_commonSystem.alertText({msg:buildError(arrError["ERR058"], strChkEmpty[0], arrError["ERR060"]), language:strLanguage}));
		pwOBJ.focus();
		return false
	}
	return true;
}	
// Procerss User login data
UI_Passenger.userLoginSucess = function(response) {
	if(response.loginSuccess) {
		if(response.lmsDetails !=null && (response.lmsDetails.emailStatus!=null || response.lmsDetails.emailStatus!="")){
			UI_commonSystem.changeUserLmsStatus(response.lmsDetails);
			UI_commonSystem.showLmsInBar();
		}
		UI_commonSystem.showLmsInBar();
		var lowestAge = 12;
		UI_Passenger.createFirstPaxFromContactInfo(response.contactInfo,UI_commonSystem.lmsDetails);
		$("#adultList\\[0\\]\\.nationality").val(response.contactInfo.nationality);
		UI_Container.hideLoading();
		$('#trRegisterdUser').hide();
		$('#trRegisterdUser').parent().append("<div class='sucess' style='background:#FFF980;padding:4px'><label id='lblSuccesLoginMsg'>"+UI_Container.getLabels().lblSuccesLoginMsg+"</label></div>");
		setTimeout("$('.sucess').fadeOut(1000, function(){$(this).remove();$('#trRegisterdUser').hide()})", 3000)
		UI_Container.buildCustomer(response.customer, response.totalCustomerCredit, response.baseCurr);
		eval(response.custProfile);
		profileLoad();
		$('#paxPageOptions').hide();
		if(top!=null & top[0]!=null){
			top[0].strReturnPage = "CUSTOMER";
			top[0].strUN = response.txtUname;
			top[0].blnMadereg = true;
		}
		// Update Register Customer Login status
		SYS_IBECommonParam.setCustomerState(true);
	} else {
		jAlert(UI_commonSystem.alertText({msg:arrError["ERR061"], language:strLanguage}));
		UI_Container.hideLoading();
	}		
}

//validate E-mail domain
//Change function as any email address to be handel
UI_Passenger.checkEmailDomain = function(emailAddObj, group, strErrorHTML){
	var data = {};
	data['domain'] = emailAddObj.val().substring(emailAddObj.val().indexOf("@")+1);	
	var emailErr = "";
	var retValue = null;
	$("#frmReservation").ajaxSubmit({dataType: 'json', url:'checkEMailDomain.action', async:false,
		data:data,
		success: function(response){ 
		retValue = response.exists;
		if(!retValue){
			if(group == 1){
				emailAddObj.addClass("errorControl");
				emailErr = "<li> " + raiseError("ERR032");
			} else if(group == 2){
				emailAddObj.addClass("errorControl");
				emailErr = "<li> " + raiseError("ERR081");
			}
		} else {
			emailErr = "";
		}
	},
	error:UI_commonSystem.setErrorStatus});	
	
	return emailErr;
}

// Fill Contact Information
function profileLoad(){
	if (arrProfile.length > 0){		
		$("#selTitle").val(arrProfile[0]);		
		$("#txtFName").val(arrProfile[1]);
		$("#txtLName").val(arrProfile[2]);
		$("#txtAdd1").val(arrProfile[3]);
		$("#txtAdd2").val(arrProfile[4]);
		$("#txtCity").val(arrProfile[7]);
		$("#txtState").val(arrProfile[9]);
		$("#txtEmail").val(arrProfile[10]);
		//alredy login
		$("#txtVerifyEmail").val(arrProfile[10]);
		
		var arrTel = arrProfile[5].split("-");
		if (arrTel.length > 0){$("#txtPCountry").val(arrTel[0]);}
		if(!contactConfig.areaCode1.ibeVisibility){
			if (arrTel.length > 2){$("#txtPhone").val(arrTel[1]+arrTel[2]);}
		}else{
			if (arrTel.length > 1){$("#txtPArea").val(arrTel[1]);}
			if (arrTel.length > 2){$("#txtPhone").val(arrTel[2]);}
		}
		
		
		var arrTel = arrProfile[6].split("-");
		if (arrTel.length > 0){$("#txtMCountry").val(arrTel[0]);}
		if(!contactConfig.areaCode1.ibeVisibility){
			if (arrTel.length > 2){$("#txtMobile").val(arrTel[1]+arrTel[2]);}
		}else{
			if (arrTel.length > 1){$("#txtMArea").val(arrTel[1]);}
			if (arrTel.length > 2){$("#txtMobile").val(arrTel[2]);}
		}

		
		var arrTel = arrProfile[8].split("-");
		if (arrTel.length > 0){$("#txtFCountry").val(arrTel[0]);}
		if(!contactConfig.areaCode1.ibeVisibility){
			if (arrTel.length > 2){$("#txtFax").val(arrTel[1]+arrTel[2]);}
		}else{
			if (arrTel.length > 1){$("#txtFArea").val(arrTel[1]);}
			if (arrTel.length > 2){$("#txtFax").val(arrTel[2]);}
		}
		
		$("#selCountry").val(arrProfile[11]);
		$("#selNationality").val(arrProfile[12]);	
		$("#txtCity").val(arrProfile[13]);
	}
	
}
$.fn.addOption = function(value) {
	this.append('<option value='+value +'> </option>');
}

UI_Passenger.addNationality = function(value) {
	$("select[id^= 'adultList']").each(function(){ 
		if ($("select[id$= 'nationality']")) {
			if ($(this).val() == "") {
				$(this).val(value);
			}
		}
	});
	
	$("select[id^= 'childList']").each(function(){ 
		if ($("select[id$= 'nationality']")) {
			if ($(this).val() == "") {
				$(this).val(value);	
			}
		}
	});
}
// Commented AARESAA-5454 - Issue 1 - As itinerary language is handled by preferred langauge
//UI_Passenger.setItinaryLanguages = function(languages) {
//	var loadAgain = function(){
//		if ($("#selLanguage").find("option").length <= 1) {
//			$("#trItinearyLans").hide();
//		} else {
//			$("#selLanguage").val(sysDefLang);
//		}
//	};
//	$("#selLanguage").find('option').remove().end().append(languages);	//IE6 Fix
//	try{//IE6 Fix
//		if ($("#selLanguage").find("option").length <= 1) {
//			$("#trItinearyLans").hide();
//		} else {
//			$("#selLanguage").val(sysDefLang);
//		}
//	}catch(e){
//		setTimeout('loadAgain',200);
//	}
//}

UI_Passenger.setPreferredLanguages = function(languages) {
	if (UI_Container.language != "")
		sysDefLang = UI_Container.language;
	var loadAgain = function(){
		if($("#selPrefLang").find("option").length > 0){
			$("#selPrefLang").val(sysDefLang);
		}
	};
	$("#selPrefLang").find('option').remove().end().append(languages);	//IE6 Fix
	try{//IE6 Fix
		if($("#selPrefLang").find("option").length > 0){
			$("#selPrefLang").val(sysDefLang);
		}
	}catch(e){
		setTimeout('loadAgain',200);
	}
}

UI_Passenger.hideMandatoryText = function() {
	if ($.trim($("#lblFillInEnglish").text()) == "") {
		$("#mandatoryText").hide();
	}
}

//Build Telephone data
UI_Passenger.buildTelephoneInfo = function(strCountryPhone, strAreaPhone) {	
	
	var tempArrAreaPhone = null;
	var tempCountryCode = null;
	var tempNewArray = null;
	
	if (strCountryPhone != null && strAreaPhone != null) {		
		var arrCountryPhoneLocal = strCountryPhone.split("^");
		for (var i = 0; i < arrCountryPhoneLocal.length; i++) {
			arrCountryPhone[i] = arrCountryPhoneLocal[i].split(",");
		}
		
		var arrAreaPhoneLocal = strAreaPhone.split("^");
		for (var i = 0; i < arrAreaPhoneLocal.length; i++) {
			tempArrAreaPhone = arrAreaPhoneLocal[i].split(",");	
			tempCountryCode = tempArrAreaPhone[0];
			tempNewArray = new Array();
			tempNewArray[0] = tempArrAreaPhone[1];
			tempNewArray[1]= tempArrAreaPhone[2];
			tempNewArray[2]= tempArrAreaPhone[3];
			
			if (arrAreaPhone[tempCountryCode] == undefined) {
				arrAreaPhone[tempCountryCode] = new Array();
			}			
			arrAreaPhone[tempCountryCode][i] = new Array();
			arrAreaPhone[tempCountryCode][i]  = tempNewArray;			
		}		
	}
}

//######################## FOID validation #####################################

//function will check foid duplicate foid ids
UI_Passenger.validatefoidIds = function(foidIds) {

	var strErrorHTML = "";
	var arr = new Array();
	arr = foidIds.split("^");
	if (arr!="") {
		var r = new Array();
		o:for(var i = 0, n = arr.length; i < n; i++)
			{
			for(var x = 0, y = r.length; x < y; x++)
			{
				if(r[x]==arr[i]) continue o;
				}
			   r[r.length] = arr[i];
			 }
	}
	if(arr!="" && r!="") {
		if(arr.length != r.length) {
			return "true";
		}		
	}
 	return "false";
}
UI_Passenger.setPagelayoutSettings = function (){
	if (anciConfig.paxNewLayout!= undefined && anciConfig.paxNewLayout){
		if (!UI_Container.isRegisteredUser){
            if (!GLOBALS.customerRegiterConfirmation){
                $(".usrReg").show();
            }
			$("#paxPageOptions").show();
			$("input[name='howContinue']").click(function (event){UI_commonSystem.pageOnChange();UI_Passenger.cuntinueOptionClick($(this));});
		}else{
			$("#paxPageOptions").hide();
			$("#trNewdUser").hide();
			UI_Passenger.passengerOptions = "oldcustomer";
		}
	}else{
		if (!UI_Container.isRegisteredUser){
			$("#trRegisterdUser").show();
            if (!GLOBALS.customerRegiterConfirmation){
                $(".usrReg").show();
            }
		}else{
			$("#trRegisterdUser").hide();
			UI_Passenger.passengerOptions = "oldcustomer";
		}
	}
	
	if(SYS_IBECommonParam.locale == "ar" || SYS_IBECommonParam.locale == "fa"){
		$(".div-Table").removeClass("div-Table").addClass("div-Table-rtl")
	}
	if(anciConfig.paxNewLayout!= undefined &&
		anciConfig.paxNewLayout){
		$("#txtLoginEmail").change(function(){
			$("#txtEmail").val(this.value);
		});
	}
}

UI_Passenger.cuntinueOptionClick = function (obj){
	if(obj[0].id == "registerContinue" && obj[0].value == "on"){
		UI_Passenger.passengerOptions = "newcustomer";
		$("#registerContinue").attr('checked', true);
		$("#trRegisterdUser").hide();
		$("#trNewdUser").show();
		UI_Passenger.showHidePanel(false);
		if(UI_commonSystem.loyaltyManagmentEnabled){
	        $("#isLMS").prop("checked", true);
	        $("#tblFFPField").show();
		}else{
			$("#isLMS").hide();
	        $("#isLMS").prop("checked", false);
	        $("#tblFFPField").hide();
		}
	}else if(obj[0].id == "registerContinue" && obj[0].value == "off"){
		UI_Passenger.passengerOptions = "guest";
		$("#trRegisterdUser").hide();
		$("#trNewdUser").hide();
	}else if(obj[0].id == "loginContinue" && obj[0].value == "on"){
		UI_Passenger.passengerOptions = "oldcustomer";
		$("#loginContinue").attr('checked', true);
		$("#txtPassword").val("");
		$("#txtConPassword").val("");
		if (trim($("#txtLoginEmail").val()) != ""){
			$("#txtUID").val($("#txtLoginEmail").val());
		}else if(trim($("#txtEmail").val()) != ""){
			$("#txtUID").val($("#txtEmail").val());
		}
        $("#tblFFPField").hide();
		$("#trNewdUser").hide();
		$("#trRegisterdUser").show();
		UI_Passenger.showHidePanel(true);
	}else if(obj[0].id == "loginContinue" && obj[0].value == "off"){
		UI_Passenger.passengerOptions = "guest";
		$("#trNewdUser").hide();
		$("#trRegisterdUser").hide();
	}else if(obj[0].id == "guestContinue"){
		UI_Passenger.passengerOptions = "guest";
		$("#trRegisterdUser").hide();
		$("#trNewdUser").hide();
		$("#tblFFPField").hide();
		$("#textDOBLMS").val('');
		$("#textPPNumberLMS").val('');
		$("#textRefferedEmailLMS").val('');
		$("#textFamilHeadEmailLMS").val('');
		$("#isLMS").prop('checked',false);
	}
	UI_Container.hideLoading();
}

UI_Passenger.applyNumbeDuringTravel = function(obj){
	if (obj.checked == false){
		$("#txtPCountry").val("");
		$("#txtPCountry").attr("readonly", false);
		$("#txtPhone").val("");
		$("#txtPhone").attr("readonly", false);
		$("#txtPArea").val("");
		$("#txtPArea").attr("readonly", false);
	}else{
		$("#txtPCountry").val($("#txtMCountry").val());
		$("#txtPCountry").attr("readonly", true);
		$("#txtPhone").val($("#txtMobile").val());
		$("#txtPhone").attr("readonly", true);
		$("#txtPArea").val($("#txtMArea").val());
		$("#txtPArea").attr("readonly", true);

	}

}

/**
 * Sets preferred language and itinerary language values as per configurations
 */
UI_Passenger.setLanguagePreferences = function(){
	var prefLang = $("#selPrefLang").val();
	
	if(prefLang != null){
		$("#hdnItiLanguage").val(prefLang);
	} else {
		$("#selPrefLang").val(sysDefLang);
		$("#hdnItiLanguage").val(sysDefLang);
	}
}

UI_Passenger.extractFlightLastSegOutDate = function() {
	// Selected flight date will be resDepartureDate, No need to take from resSelectedFlights
	//var jsonFltDetails = $.parseJSON($('#resSelectedFlights').val()).flightSegments
	//var objFlightData = jsonFltDetails;	
	//var segCount = objFlightData.length;	
	var departureDate = $('#resDepartureDate').val();
	var returnDate = $("#resReturnDate").val();
	if (returnDate != null && returnDate.length > 8 &&  $("#resReturnFlag").val() == "true") {
		departureDate = returnDate;
	}	
	//if(segCount != null && segCount > 0){
	//	departureDate = objFlightData[segCount-1].departureDate;
	//}	
	return departureDate;
}

UI_Passenger.extractFlightFirstSegOutDate = function() {
	var jsonFltDetails = $.parseJSON($('#resSelectedFlights').val()).flightSegments;
	// French translation issue fix for Arrival & Departure dates - Supports all languages
	
	if(jsonFltDetails.length > 0){
		for(var i = 0; i < jsonFltDetails.length;i++){
			if(typeof(jsonFltDetails[i]) !== "undefined"){
				jsonFltDetails[i].arrivalDate = unescape(jsonFltDetails[i].arrivalDate);
				jsonFltDetails[i].departureDate = unescape(jsonFltDetails[i].departureDate);
			}
		}
	}

	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	var departureDate = $('#resDepartureDate').val();

	 if(segCount != null && segCount > 0){
		 var d = new Date(objFlightData[0].departureTimeLong);
		 var curr_date = d.getDate();
		 var curr_month = d.getMonth() + 1; 
		 var curr_year = d.getFullYear();
		 departureDate = curr_date + "/" + curr_month + "/" + curr_year;
	} 
	
	return departureDate;
}

UI_Passenger.extractFlightLastSegDate = function() {
	var jsonFltDetails = $.parseJSON($('#resSelectedFlights').val()).flightSegments;
	// French translation issue fix for Arrival & Departure dates - Supports all languages
	
	if(jsonFltDetails.length > 0){
		for(var i = 0; i < jsonFltDetails.length;i++){
			if(typeof(jsonFltDetails[i]) !== "undefined"){
				jsonFltDetails[i].arrivalDate = unescape(jsonFltDetails[i].arrivalDate);
				jsonFltDetails[i].departureDate = unescape(jsonFltDetails[i].departureDate);
			}
		}
	}
	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	var departureDate = $('#resDepartureDate').val();

	 if(segCount != null && segCount > 0){
		 var d = new Date(objFlightData[segCount-1].departureTimeLong);
		 var curr_date = d.getDate();
		 var curr_month = d.getMonth() + 1; 
		 var curr_year = d.getFullYear();
		 departureDate = curr_date + "/" + curr_month + "/" + curr_year;
	} 
	
	return departureDate;
}

UI_Passenger.extractFlightLastSegDateObj = function() {
	var jsonFltDetails = $.parseJSON($('#resSelectedFlights').val()).flightSegments
	var objFlightData = jsonFltDetails;	
	var segCount = objFlightData.length;
	if(segCount != null && segCount > 0){
		return new Date(objFlightData[segCount-1].departureTimeLong);
	}
	return new Date();
}

UI_Passenger.convertDateFormat = function(strDate){
	var ddMMMyy_date = strDate.split(",");
	var day = ddMMMyy_date[1].substring(1,3);
	var m = ddMMMyy_date[1].substring(4,7);
	var y = ddMMMyy_date[1].substring(8,10);
	var year = '20'+y;
	
	var mon = (m == 'Jan' || m == 'jan') ? '01' :
				  (m == 'Feb' || m == 'feb') ? '02' :
					  (m == 'Mar' || m == 'mar') ? '03' :
						  (m == 'Apr' || m == 'apr') ? '04' :
							  (m == 'May' || m == 'may') ? '05' :
								  (m == 'Jun' || m == 'jun') ? '06' :
									  (m == 'Jul' || m == 'jul') ? '07' :
										  (m == 'Aug' || m == 'aug') ? '08' :
											  (m == 'Sep' || m == 'sep') ? '09' :
												  (m == 'Oct' || m == 'oct') ? '10' :
													  (m == 'Nov' || m == 'nov') ? '11' : '12';
	
	
	var formatedDate = day + '/' + mon + '/' + year;
	return formatedDate;
}

UI_Passenger.checkSpclCharOnly = function(fieldParam){
	fieldParam = $.trim(fieldParam);
	fieldParam = fieldParam.replace(/\s+/g, " ");

	if(fieldParam.indexOf('--') != -1 || fieldParam.indexOf('- -') != -1){
		return true;	
	}
	return false;
}

function checkForDuplicates(){
	var data = {};
	var paxDetails = UI_Passenger.getPaxDetails();	
	var paxArr = [];
	for(var i = 0 ; i < paxDetails.adults.length; i++){
		var index = paxArr.length;
		paxArr[index] = paxDetails.adults[i];
		paxArr[index].paxType= 'AD';			
	}
	for(var i = 0 ; i < paxDetails.children.length; i++){
		var index = paxArr.length;
		paxArr[index] = paxDetails.children[i];
		paxArr[index].paxType='CH';		
	}
	data['jsonPax']  = $.toJSON(paxArr);
	var hasDuplicates = false;
	$("#frmReservation").ajaxSubmit({dataType: 'json',url:'duplicateCheck.action', processData: false, async:false,	data:data,		
			success: function(response){
		if(response.success){
			if(response.hasDuplicates){
				hasDuplicates = true;
			}
		}
	}, error:UI_commonSystem.setErrorStatus});
	return hasDuplicates;
}

UI_Passenger.loadThirdPartyScripts = function (response) {
    UI_Passenger.showSocial.FB = response.showFacebook;
    UI_Passenger.showSocial.LN = response.showLinkedln;

    if (UI_Passenger.showSocial.FB) {
        try {
            (function (d) {
                var js, id = 'facebook-jssdk';
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                d.getElementsByTagName('head')[0].appendChild(js);
            }(document));

            window.fbAsyncInit = function () {
                FB.init({
                    appId: response.facebookAppId,
                    status: true,
                    cookie: true,
                    xfbml: true
                });
            }

            $("#btnFBSignInP").show();
            $("#btnFBSignInP").click(function () {
                UI_Passenger.facebookClick(true);
            });

            $("#btnFBRegisterP").show();
            $("#btnFBRegisterP").click(function () {
                UI_Passenger.facebookClick(false);
            }); //need to fix
        } catch (e) {}
    }

    if (UI_Passenger.showSocial.LN) {
        try {
            var e = document.createElement('script');
            e.type = 'text/javascript';
            e.async = true;
            e.src = document.location.protocol + '//platform.linkedin.com/in.js?async=true';
            e.onload = function () {
                IN.init({
                    api_key: response.linkedInAppId,
                    authorize: false
                });
            };

            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(e, s);
            $("#btnLNSignInP").show();
            $("#btnLNSignInP").click(function () {
                UI_Passenger.linkedInClick(true);
            });

            $("#btnLNRegisterP").show();
            $("#btnLNRegisterP").click(function () {
                UI_Passenger.linkedInClick(false);
            });

        } catch (e) {}
    }
}

UI_Passenger.facebookClick = function (login) {
    function updateButton(response) {
        if (response.authResponse) {
            //user is already logged in and connected
            FB.api('/me', function (response) {
                if (login) {
                    UI_Passenger.autoLogin(response.email);
                } else {
                	var dto = {'retrivedEmail':response.email , 'firstName':response.first_name, 'lastName':response.last_name, 'gender':response.gender};
                    UI_Passenger.populateRegister(dto);
                }
            });

        } else {
            //user is not connected to the app or logged out
            FB.login(function (response) {
                if (response.authResponse) {
                    // user is logged in but not connected
                    FB.api('/me', function (response) {
                        if (login) {
                            UI_Passenger.autoLogin(response.email);
                        } else {
                        	var dto = {'retrivedEmail':response.email , 'firstName':response.first_name, 'lastName':response.last_name, 'gender':response.gender};
                            UI_Passenger.populateRegister(dto);
                        }
                    });
                } else {
                    //user cancelled login or did not grant authorization
                }
            }, {
                scope: 'email'
            });
        }
    }
    FB.getLoginStatus(updateButton);
    FB.Event.subscribe('auth.statusChange', updateButton);
}

UI_Passenger.linkedInClick = function (login) {
    if ((typeof IN != 'undefined') && (typeof IN.User != 'undefined')) {
        IN.User.authorize(function () {
            if (login) {
                IN.API.Profile("me").fields("firstName", "lastName", "industry", "pictureUrl", "emailAddress", "id").result(UI_Passenger.LinkedInsignInSuccess);
            } else {
                IN.API.Profile("me").fields("firstName", "lastName", "industry", "pictureUrl", "emailAddress", "id").result(UI_Passenger.LinkedInRegisterSuccess);
            }

        });
    }
}

UI_Passenger.LinkedInsignInSuccess = function (profiles) {
    var profile = profiles.values[0];
    if (profile != null) {
        UI_Passenger.autoLogin(profile.emailAddress);
    }
}

UI_Passenger.LinkedInRegisterSuccess = function (profiles) {
    var profile = profiles.values[0];
    if (profile != null) {
    	var dto = {'retrivedEmail':profile.emailAddress , 'firstName':profile.firstName, 'lastName':profile.lastName, 'gender':''};
        UI_Passenger.populateRegister(dto);
    }
}


UI_Passenger.showHidePanel = function (isLogin) {
    if (isLogin) {
        if (UI_Passenger.showSocial.FB) {
            $("#tdFBSignInP").show();
        } else {
            $("#tdFBSignInP").hide();
        }

        if (UI_Passenger.showSocial.LN) {
            $("#tdLnSignInP").show();
        } else {
            $("#tdLnSignInP").hide();
        }
    } else {
        //register
        if (UI_Passenger.showSocial.FB) {
            $("#tdFBRegisterP").show();
        } else {
            $("#tdFBRegisterP").hide();
        }

        if (UI_Passenger.showSocial.LN) {
            $("#tdLnRegisterP").show();
        } else {
            $("#tdLnRegisterP").hide();
        }
    }
}

/* -------------------------------------S O U N D E X    N A M E   V A L I D A T I O N---------------------------------------*/
/* ---- Algorithm 1 Steps Start ---- */
/* Step 1 - Remove H if next char is a vowel and remove hypens and remove all single letters */
function removeH(source) {
	var vowels = [ "a", "e", "i", "o", "u" ], sourceLen = source.length;
	result = "";
	for (var idx = 0; idx < sourceLen; idx++) {
		if ((source[idx].toUpperCase() === "H" && idx < (sourceLen - 1) && vowels
				.indexOf(source[idx + 1]) == -1)
				|| (source[idx].toUpperCase() === "H" && idx === (sourceLen - 1))) {
			continue;
		}
		result += source[idx];
	}

	var chksingle = result.split(" ");
	result = "";
	for (var chk = 0; chk < chksingle.length; chk++) {
		if (chksingle[chk].length === 1) {

		} else {
			result += chksingle[chk] + " ";
		}
	}
	result = result.trim();

	return result.replace(/-/g, "");
}

/* Step 2 - Remove Vowels */
function removeVowels(sourceStrng) {
	var vowels = /[aeiou]/gi;
	replaceWith = ""
	return sourceStrng.replace(vowels, replaceWith);
}

/* Step 3 - Replace X With KS and TH with TY */
function rplceStrings(strngData) {
	return strngData.replace(/X/g, "KS").replace(/TH/g, "TY");
}

/* Step 4 - Remove repeated and spaces */
function removeDuplicates(replData) {
	return replData.replace(/([A-Z])\1+/ig, "$1").replace(/\s/g, "");
}

/* Step 5 - Compare lengths and search shorter names */
function algorithm1(fname1actual, fname2actual, sname1actual, sname2actual) {
	var fname1removeH = removeH(fname1actual).toUpperCase();
	var fname1removeV = removeVowels(fname1removeH);
	var fname1replace = rplceStrings(fname1removeV);
	var fname1removeD = removeDuplicates(fname1replace);

	var fname2removeH = removeH(fname2actual).toUpperCase();
	var fname2removeV = removeVowels(fname2removeH);
	var fname2replace = rplceStrings(fname2removeV);
	var fname2removeD = removeDuplicates(fname2replace);

	var sname1removeH = removeH(sname1actual).toUpperCase();
	var sname1removeV = removeVowels(sname1removeH);
	var sname1replace = rplceStrings(sname1removeV);
	var sname1removeD = removeDuplicates(sname1replace);

	var sname2removeH = removeH(sname2actual).toUpperCase();
	var sname2removeV = removeVowels(sname2removeH);
	var sname2replace = rplceStrings(sname2removeV);
	var sname2removeD = removeDuplicates(sname2replace);

	var fnameResult;
	if (fname1removeD.length > fname2removeD.length) {
		fnameResult = fname1removeD.indexOf(fname2removeD) >= 0 ? true : false;
	} else {
		fnameResult = fname2removeD.indexOf(fname1removeD) >= 0 ? true : false;
	}

	var snameResult;
	if (sname1removeD.length > sname2removeD.length) {
		snameResult = sname1removeD.indexOf(sname2removeD) >= 0 ? true : false;
	} else {
		snameResult = sname2removeD.indexOf(sname1removeD) >= 0 ? true : false;
	}

	if (fnameResult && snameResult) {
		return true;
	} else {
		/*
		 * Step 6 - IF mismatch replace incoming name with db surname and
		 * incoming surname with db name
		 */
		if (fname1removeD.length > sname2removeD.length) {
			fnameResult = fname1removeD.indexOf(sname2removeD) >= 0 ? true
					: false;
		} else {
			fnameResult = sname2removeD.indexOf(fname1removeD) >= 0 ? true
					: false;
		}

		if (sname1removeD.length > fname2removeD.length) {
			snameResult = sname1removeD.indexOf(fname2removeD) >= 0 ? true
					: false;
		} else {
			snameResult = fname2removeD.indexOf(sname1removeD) >= 0 ? true
					: false;
		}
		if (fnameResult && snameResult) {
			return true;
		} else {
			return false;
		}
	}
}
/* ---- Algorithm 1 Steps End ---- */

/* ---- Algorithm 3 Steps Start ---- */
var soundex = function(s) {
	var a = s.toLowerCase().split(''),
	f = a.shift(), r = '', codes = {
		a : '',
		e : '',
		i : '',
		o : '',
		u : '',
		b : 1,
		f : 1,
		p : 1,
		v : 1,
		c : 2,
		g : 2,
		j : 2,
		k : 2,
		q : 2,
		s : 2,
		x : 2,
		z : 2,
		d : 3,
		t : 3,
		l : 4,
		m : 5,
		n : 5,
		r : 6
	};

	r = f + a.map(function(v, i, a) {
		return codes[v]
	}).filter(function(v, i, a) {
		return ((i === 0) ? v !== codes[f] : v !== a[i - 1]);
	}).join('');

	return (r + '000').slice(0, 4).toUpperCase();
};

function getFormatString(name) {
	var param = name;
	var getData = param.split(" ");
	var length = 0;
	var position = 0;
	var res;

	return getData.sort(function(a, b) {
		return b.length - a.length;
	})[0].replace(/-/g, '');
}

function compare(firstName1, lastName1, firstName2, lastName2) {

	/* Step 1 Start */
	var fname1 = firstName1.trim();
	var fname2 = firstName2.trim();
	var sname1 = lastName1.trim();
	var sname2 = lastName2.trim();

	var regex = new RegExp("^[a-zA-Z]+[.-]?[a-zA-Z]*$");

	if ((!fname1 || !sname1)
			|| (!regex.test(fname1.replace(/ /g, "")) || !regex.test(sname1
					.replace(/ /g, "")))) {
		return false;
	}

	fname1 = getFormatString(fname1);
	fname2 = getFormatString(fname2);
	sname1 = getFormatString(sname1);
	sname2 = getFormatString(sname2);

	if ((fname1 === fname2) && (sname1 === sname2)) {
		return true;
	} else {
		/* Step 2 Start */
		var param1 = soundex(fname1);
		var param2 = soundex(fname2);
		var param3 = soundex(sname1);
		var param4 = soundex(sname2);

		if ((param1 === param2) && (param3 === param4)) {
			return true;
		} else {
			/* Step 3 Start */

			var fname1actual = firstName1;
			var fname1value = fname1actual.split(" ").join("");
			var fname2actual = firstName2;
			var fname2value = fname2actual.split(" ").join("");
			var sname1actual = lastName1;
			var sname1value = sname1actual.split(" ").join("");
			var sname2actual = lastName2;
			var sname2value = sname2actual.split(" ").join("");

			if ((fname1value === fname2value) && (sname1value === sname2value)) {
				return true;
			} else {
				/* Step 4 Start */
				var param1value = soundex(fname1value);
				var param2value = soundex(fname2value);
				var param3value = soundex(sname1value);
				var param4value = soundex(sname2value);

				if ((param1value === param2value)
						&& (param3value === param4value)) {
					return true;
				} else {
					/* Step 5 Start */
					if ((fname1actual === sname2actual)
							&& (fname2actual === sname1actual)) {
						return true;
					} else {
						if ((fname1 === sname2) && (fname2 === sname1)) {
							return true;
						} else {
							if ((param3 === param2) && (param1 === param4)) {
								return true;
							} else {
								if ((fname1value === sname2value)
										&& (fname2value === sname1value)) {
									return true;
								} else {
									if ((param1value === param4value)
											&& (param3value === param2value)) {
										return true;
									} else {
										return algorithm1(fname1actual,
												fname2actual, sname1actual,
												sname2actual);
									}
								}
							}
						}
					}
				}
				return false;
			}
		}
	}
}

getNationalityName = function(nationalityCode){
	var nationalityName;
	$.each(UI_Passenger.nationalityList, function(i,j){
		if(j[0] == nationalityCode){
			nationalityName =  j[1];
			return false;
		}
	});
	
	return nationalityName;
}

getNationalityCode = function(nationalityName){
	var nationalityCode;
	$.each(UI_Passenger.nationalityList, function(i,j){
		if(j[1] == nationalityName){
			nationalityCode = j[0];
			return false;
		}
	});
	return nationalityCode;
}

getRelationshipDescription = function(relationshipId){
	var relationshipDes;
	$.each(UI_Passenger.relationshipList, function(i,j){
		if(j[0] == relationshipId){
			relationshipDes =  j[1];
			return false;
		}
	});
	
	return relationshipDes;
}

getRelationshipId = function(relationshipDes){
	var relationshipId;
	$.each(UI_Passenger.relationshipList, function(i,j){
		if(j[1] == relationshipDes){
			relationshipId =  j[0];
			return false;
		}
	});
	
	return relationshipId;
}

getRelationshipTitle = function(relationshipId){
	var relationshipTitle;
	$.each(UI_Passenger.relationshipTitleList, function(i,j){
		if(j[0] == relationshipId){
			relationshipTitle =  j[1];
			return false;
		}
	});
	
	return relationshipTitle;
}

UI_Passenger.setFamilyMemberData = function(){
	
	var DOB = $("#"+UI_Passenger.selectedFamilyMemberId+"_FMDateOfBirth").text();
	
	if(UI_Passenger.paxDetailsRowId.indexOf("adultList") >= 0){
		if(ageCalculate(DOB) < top.GLOBALS.childCutOffAgeInYears){
			jAlert('Please select a Adult', 'Wrong Details');
			return false;
		}
	}else if(UI_Passenger.paxDetailsRowId.indexOf("childList") >= 0){
		if(ageCalculate(DOB) > top.GLOBALS.childCutOffAgeInYears | ageCalculate(DOB) < 2){
			jAlert('Please select a Child', 'Wrong Details');
			return false;
		}
	}else if(UI_Passenger.paxDetailsRowId.indexOf("infantList") >= 0){
		if(ageCalculate(DOB) > top.GLOBALS.infantCutOffAgeInYears){
			jAlert('Please select a Infant', 'Wrong Details');
			return false;
		}
	}
	
	var rowId = UI_Passenger.paxDetailsRowId.substr(UI_Passenger.paxDetailsRowId.indexOf("[") + 1 , 1);
	var paxType = UI_Passenger.paxDetailsRowId.split("[")[0];
	
	$("#"+paxType+"\\["+rowId+"\\]\\.firstName").val($("#"+UI_Passenger.selectedFamilyMemberId+"_FMFirstName").text());
	$("#"+paxType+"\\["+rowId+"\\]\\.lastName").val($("#"+UI_Passenger.selectedFamilyMemberId+"_FMLastName").text());
	
	var relationshipId = getRelationshipId($("#"+UI_Passenger.selectedFamilyMemberId+"_FMRelationshipId").text());
	
	if(ageCalculate(DOB) > top.GLOBALS.infantCutOffAgeInYears && ageCalculate(DOB) < top.GLOBALS.childCutOffAgeInYears){
		$("#"+paxType+"\\["+rowId+"\\]\\.chdTitle").val(getRelationshipTitle(relationshipId));
	}else if(ageCalculate(DOB) >= top.GLOBALS.childCutOffAgeInYears){
		$("#"+paxType+"\\["+rowId+"\\]\\.title").val(getRelationshipTitle(relationshipId));
	}

	var nationlityCode = getNationalityCode($("#"+UI_Passenger.selectedFamilyMemberId+"_FMVationalityCode").text());
	$("#"+paxType+"\\["+rowId+"\\]\\.nationality").val(nationlityCode);
	
	var dateOfBirth = DOB.split("/");
	$("#"+paxType+"\\["+rowId+"\\]\\.dateOfBirth").datepicker("setDate" , new Date(dateOfBirth[2] , dateOfBirth[1]-1 , dateOfBirth[0]));
	
	$("#familyMemberDetails").dialog('close');
}