/** IBE OHD Confirm FOR PAY AT HOME
 * 
 * @author Harshika
 *  
 */

function UI_OHDConfirmPayAtHome(){};
UI_OHDConfirmPayAtHome.pnr = null;
;(function(UI_OHDConfirmPayAtHome) {
	
	$(document).ready(function() {
		UI_OHDConfirmPayAtHome.displayPage();
		//$("#divLoadBg").hide();
		$("#pageLoading").hide();
		$("#divLoadMsg").show();
		UI_OHDConfirmPayAtHome.ready();		
	});
	
	UI_OHDConfirmPayAtHome.ready = function() {
		
		if(typeof $("#loadResLink") !== 'undefined'){
			 
			$("#hdnParamData").val(language + '^DIRECTLOARDPNR^');
			$("#hdnPnr").val(pnr);
			$("#form1").attr('action',loadResUrl);
			 
			$("#loadResLink").click(function(){
			$("#form1").submit(); 
				return false;
			});			 
		}
		 
		$("#linkHome","#btnFinish").click(function(){ SYS_IBECommonParam.homeClick() });
		$("#btnFinish").unbind("click").click(function(){ SYS_IBECommonParam.homeClick() });
		$("#btnPrint").click(function(){ UI_OHDConfirmPayAtHome.printPage() });
		UI_commonSystem.stepsSettings("last","interLineConfurmtion");
		UI_OHDConfirmPayAtHome.setPageData();		
	}
	
	UI_OHDConfirmPayAtHome.setPageData = function() {
		var onHoldData = strConfData;
		if(trackingNumber == null){
			$("#PgOHDconfirm").populateLanguage({messageList:strOHDLabels});
//			$("#resNoCancel").text(pnr);
			$("#resNo").text(onHoldData.pnr);
			$("#time").text(onHoldData.releaseTime);
//			$("#payFortHomeCancelText").text(payAtHomeCancelText);
			$("#lblPayATHomeTrackNum").hide();
			$("#trackNo").hide();
			$("#payFortBackToMerchant").show();
			$("#payFortHomeCancel").show();
		
		} else {
	
			$("#PgOHDconfirm").populateLanguage({messageList:strOHDLabels});
			$("#resNo").text(onHoldData.pnr);
			$("#time").text(onHoldData.releaseTime);
			$("#trackNo").text(trackingNumber);
			$("#payFortHomeCancel").hide();
			$("#lblPayATHomeTrackNum").show();
			$("#trackNo").show();
			$("#payFortBackToMerchant").show();
			
		}
		$("#divLoadMsg").hide();
		$("#divLoadBg").show();
		UI_OHDConfirmPayAtHome.loadTrackingPage();
		$('body').append('<div id="divSessContainer"></div>');
		UI_commonSystem.initSessionTimeout('divSessContainer',timeOut,function(){});
	}
	
	UI_OHDConfirmPayAtHome.printPage = function(){
		window.print();
		return false;
	}
	
	UI_OHDConfirmPayAtHome.displayPage = function() {
		UI_Top.holder().UI_commonSystem.loadingCompleted();
		if (ui_paymentGWManger.showConfirmIframe){ //TODO Need to add a js configuration or remove by check other clients
			$("#divLoadBg", parent.document).css("visibility", "hidden");
			$("#divLoadBg", parent.document).show();	
			var p = $(".rightColumn", parent.document).position();
			$(".rightColumn", parent.document).hide();
			$("#cardInputs", parent.document).attr("width", parseInt($(".rightColumn", parent.document).css("width")) + 10 );
			$("#cardInputs", parent.document).attr("height", "400");
			$("#cardInputs", parent.document).css("top", "0px");
			$("#cardInputs", parent.document).css("left", "0px");
			$("#cardInputPannel", parent.document).css("position", "absolute");
			$("#cardInputPannel", parent.document).css("top", p.top );
			$("#cardInputPannel", parent.document).css("left", p.left);
			$("#divLoadBg", parent.document).css("visibility", "visible");
		}else{
			$("#divLoadBg", parent.document).hide();	
			$("#cardInputs", parent.document).attr("height", "100%");
			$("#cardInputs", parent.document).attr("width", "100%");
			$("#cardInputs", parent.document).css("top", "0px");
			$("#cardInputs", parent.document).css("left", "0px");
		}
	}
	UI_OHDConfirmPayAtHome.loadTrackingPage = function(){
		var paramData = "pageID=OHDCONFIRM&rad="+UI_commonSystem.getRandomNumber()+"&pnr="+UI_OHDConfirmPayAtHome.pnr;
		$("#frmTracking").attr("src", "showLoadPage!loadTrackingPage.action?"+paramData);
	}
	
	
})(UI_OHDConfirmPayAtHome);