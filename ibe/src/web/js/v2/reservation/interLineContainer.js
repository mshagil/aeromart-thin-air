function UI_Container() {}

$.fn.disableNav = function(){
	this.each(function(index,item){
		$(item).prop('disabled',true).addClass("ButtonDefaultDisable");
	});
	return this;
}

$.fn.enableNav = function(){
	this.each(function(index,item){
		$(item).prop('disabled',false).removeClass("ButtonDefaultDisable");
	});
	return this;
}

;(function(UI_Container){
	UI_Container.loaded = false;
	UI_Container.focusedPageIndex = -1;
	UI_Container.pages = [];
	
	UI_Container.paxTypeCounts = {};
	UI_Container.selectedFlights = {
		isReturnFlight: false,
		flightSegments : []
	};

	UI_Container.labels = {};
	UI_Container.fareQuote = {};
	UI_Container.systemDate = "";
	UI_Container.segmentNameMap= {};
	UI_Container.airportNameMap= {};
	UI_Container.navInitialized = false;
	UI_Container.userMenuInitialized = false;
	UI_Container.addModifyAncillary = false;
	UI_Container.modifySegment = false;
	UI_Container.requoteFlow = false;
	UI_Container.modifyBalDisplayAS = false;
	UI_Container.carrier = "";
	UI_Container.carrierName = "";
	UI_Container.paxValidation = {};
	UI_Container.isRegisteredUser = false;
	UI_Container.isKiosk = false;
	UI_Container.isInterlineRes = false;
	UI_Container.outBoundDurations = "";
	UI_Container.inBoundDurations = "";
	UI_Container.sessionTimeOutDisplay = false;
	UI_Container.hasInsurance = false;
	UI_Container.isExternalPaymentGateway =  true;
	UI_Container.useIframeInPostingParams =  true;
	UI_Container.language = "en";
	UI_Container.isPaymentProcessing =  false;
	UI_Container.isFOCmealEnabled = false;
	UI_Container.isAllowInfantInsurance = false;
	UI_Container.bundleFarePopupEnabled = false;
	UI_Container.errorInfo={};
	UI_Container.addGroundSegment = false;
	UI_Container.makePayment = false;
	UI_Container.isOnholdPaymentCreateFlow = false;
	UI_Container.paymentRetry = false;
	UI_Container.actions = { PAYMENT_RETRY_POSTCARD: "interlineRetryPostCardDetails.action", 
							 PAYMENT_DEFAULT_POSTCARD: "interlinePostCardDetails.action"
						   };
	UI_Container.discountInfo = null;
	UI_Container.resFlexiUserSelectionCharge = 0 ;
	UI_Container.fareQuoteOndFlexiCharges = {};
	UI_Container.fareQuoteOndFlexiChargesTotal = 0;
	UI_Container.applyAncillaryPenalty = false;
	UI_Container.loyaltyManagmentEnabled = false;
	
		
	UI_Container.ready = function() {
		// UI_commonSystem.loadingCompleted();
		
		if($('#makePayment').val() == 'true'){
			UI_Container.makePayment = true;
		}
		if($('#paymentFlow').val() == 'PAYMENT_RETRY'){
			UI_Container.paymentRetry = true;
			UI_Container.makePayment = true;
		}
		if($('#modifyAncillary').val() == 'true'){
			UI_Container.addModifyAncillary = true;			
		}
		if($('#modifySegment').val() == 'true'){
			UI_Container.modifySegment = true;			
		}
		
		if($('#requoteFlightSearch').val() == 'true'){
			UI_Container.requoteFlow = true;
		}
		
		if($('#cancelSegmentRequote').val() == 'true' || $('#nameChangeRequote').val() == 'true'){
			UI_Container.makePayment = true;
		}
		
		if($('#mode').val() == 'true'){
			UI_Container.modifyBalDisplayAS = true;			
		}
		
		if($('#addGroundSegment').val() == 'true'){
			UI_Container.addGroundSegment = true;			
		}
		
		if($('#groupPNR').val() != "" && $('#groupPNR').val() == "true"){
			UI_Container.isInterlineRes = true;			
		}
		
		if (UI_Container.addModifyAncillary == true || UI_Container.modifySegment == true || UI_Container.requoteFlow) {
			UI_Passenger.setContactData($.parseJSON($('#contactInfoJson').val()));
		}
		
		if ($("#outBoundDurations").val() != "") {
			UI_Container.outBoundDurations = $("#outBoundDurations").val();
		}
		
		if ($("#inBoundDurations").val() != "") {
			UI_Container.inBoundDurations = $("#inBoundDurations").val();
		}
		
		if ($("#hasInsurance").val() != "" && $("#hasInsurance").val() == "true") {
			UI_Container.hasInsurance = true;
		}	
		
		if ($("#locale").val() != "") {
			UI_Container.language = $("#locale").val();
		}	
		
		if(!UI_Container.loaded){
			$('#frmReservation').ajaxSubmit({
				url:'iBEContainerLocale.action',
				dataType: 'json', data:{},		
				success: function(response){
					if(response == null) UI_commonSystem.setErrorStatus();
					
					UI_Container.loaded = true;
					UI_Container.labels = response.jsonLabel;
					UI_Container.systemDate = response.systemDate;
					UI_Container.carrier = response.carrier;
					UI_Container.carrierName = response.carrierName;
					UI_Container.specialMessages = response.specialMessages;
					UI_Container.airportMessage = response.airportMessage; 
					UI_Container.paxValidation = response.paxValidation;
					UI_Container.isKiosk = response.kiosk;
					UI_Container.isFOCmealEnabled = response.focMealEnabled;
					UI_Container.isAllowInfantInsurance = response.allowInfantInsurance;
					UI_Container.errorInfo = response.errorInfo;
					UI_Container.loyaltyManagmentEnabled = response.loyaltyManagmentEnabled;
					UI_commonSystem.loyaltyManagmentEnabled = response.loyaltyManagmentEnabled;
					UI_commonSystem.lmsDetails = response.lmsDetails;
					UI_commonSystem.showLmsInBar();
					buildTimeoutBanner(response.timeoutDTO);
					UI_Container.buildCustomer(response.customer, response.totalCustomerCredit, response.baseCurr);
					setDesignSpecLabels();
					layoutPage();
					buildLocale();
					UI_Container.sessionTimeOutDisplay = response.sessionTimeOutDisplay;
					UI_Container.bundleFarePopupEnabled = response.bundleFarePopupEnabled;
					
					if ( UI_Container.isKiosk || ((UI_Container.isRegisteredUser || UI_Container.modifySegment)
							&& UI_Container.sessionTimeOutDisplay )) {						
						$('body').append('<div id="divSessContainer"></div>');
						UI_commonSystem.initSessionTimeout('divSessContainer',response.timeoutDTO,function(){});
					}
				},
				error:UI_commonSystem.setErrorStatus
			});
		}
		UI_commonSystem.dragableLI();
        if (globalConfig.trackingLoadOnPaxPage){
            UI_Container.loadTrackingOnPax();
        }

	}
	
	UI_Container.isFOCmealEnabled = function(){
		return UI_Container.isFOCmealEnabled;
	}
	
	
	
	UI_Container.isModifyAncillary = function(){
		return UI_Container.addModifyAncillary;
	}
	
	UI_Container.isModifySegment = function(){
		return UI_Container.modifySegment;
	}
	
	UI_Container.isRequoteFlow = function(){
		return UI_Container.requoteFlow;
	}
	
	UI_Container.getPaxValidation = function(){
		return UI_Container.paxValidation;
	}
	
	UI_Container.isLoyaltyManagmentEnabled = function() {
		return UI_Container.loyaltyManagmentEnabled;
	}
	
	/*
	 * Check whether given ancillary is enabled in ancillary modify mode if
	 * ancillary modification is false return true
	 */
	UI_Container.isAnciModifyEnabled = function(anciShortCode){
		if(UI_Container.addModifyAncillary){
			var anciToMod = $.parseJSON($('#selectedAncillary').val());
			for(var i = 0 ; i < anciToMod.length ; i++){
				if(anciShortCode == anciToMod[i]){
					return true;
				}
			}
			return false;
		}else{
			true;
		}
    }
	
	UI_Container.buildCustomer = function(customer, credit, baseCurr){
		if(customer != null){			
			UI_Container.isRegisteredUser = true;
			var customerTitle = customer.title;
			if (customerTitle != "") {
				customerTitle = customerTitle.toLowerCase() + ".";
			}
			var customerName = customerTitle + " " + (customer.firstName).toLowerCase() + " " + (customer.lastName).toLowerCase();
			$("#lblTxtName").text(customerName);
			
			$("#spnCerdit").html(credit + " " + baseCurr);
			
			if(!UI_Container.userMenuInitialized){
				UI_Container.userMenuInitialized = true;
				$("#tdReservationList").click(function(){UI_Container.handleRegCusAction('tdReservationList');});
			    $("#tdReservationCredit").click(function(){UI_Container.handleRegCusAction('tdReservationCredit');});
			    $("#tdTravelHistory").click(function(){UI_Container.handleRegCusAction('tdTravelHistory');});
			    $("#tdUpdateProfile").click(function(){UI_Container.handleRegCusAction('tdUpdateProfile');});
			    $("#tdLogout, #lblLogOut").click(function(){UI_Container.handleRegCusAction('tdLogout');});
			}
		    
		    if (customer.loyaltyAccountExist == true) {
				$("#trActivateLoyalty").remove();
			}

			$('#regUser').show();

			if (UI_commonSystem.lmsDetails == null
					|| UI_commonSystem.lmsDetails.emailId == null
					|| UI_commonSystem.lmsDetails.emailId == "") {
				$('#tr-LMS-LoggedIn').hide();
			}

		}else{
			UI_Container.isRegisteredUser = false;
			$('#regUser').hide();
		}
	}
	
	UI_Container.handleRegCusAction = function(elementId){
		// TODO get text from lang files
		jConfirm(UI_Container.labels.lblLogOutMsg , UI_Container.labels.lblAlertConfirm, function(response){	
			if(response){
				submitToCustomerPage(elementId);
			}
		});
	}
	
	
	UI_Container.getSystemDate = function(){
		return UI_Container.systemDate;
	}
	
	UI_Container.loadErrorPage = function(errMsg){	
		window.location.href="showLoadPage!loadError.action?messageTxt="+errMsg;
	}

	UI_Container.getPaxSubmitData = function(){
		return UI_Passenger.getPaxSubmitData(UI_Container.paxTypeCounts);
	}
	
	UI_Container.getPaxDetails = function(){
		if(UI_Container.addModifyAncillary || UI_Container.isModifySegment() || UI_Container.addGroundSegment || UI_Container.makePayment){
			return $.parseJSON($('#paxJson').val());
		}else{			
			return UI_Passenger.getPaxDetails();
		}
		
	}

	UI_Container.getSelectedClassOfServiceDetails = function(flightRefNo){
        var cos = {'cc':null, 'lcc':null};
        var cabinClassCode = $('#resClassOfService').val();
        var logicalCCMap = $.parseJSON($('#resFareQuoteLogicalCCSelection').val());
        
        if(UI_Container.selectedFlights != null
                && UI_Container.selectedFlights.flightSegments != null){
            var selFlights = UI_Container.selectedFlights.flightSegments;
        
            function normalizeFltRef(fltRef){
                fltRef = fltRef.split('#')[0];
                if(fltRef.indexOf('-') > -1){
                    fltRef = fltRef.substring(0, fltRef.indexOf('-'));
                }
                return fltRef;
            }
            flightRefNo = normalizeFltRef(flightRefNo);
            for(var i=0;i<selFlights.length;i++){
                var selFlight = selFlights[i];
                var selFlightRefNo = normalizeFltRef(selFlight.flightRefNumber);
                
                if(selFlightRefNo == flightRefNo){
                    if(UI_Container.isModifyAncillary()){
                        cos.cc = selFlight.cabinClass;
                        cos.lcc = selFlight.logicalCC;
                    } else {
                        cos.cc = cabinClassCode;
                        cos.lcc = (logicalCCMap != null) ? logicalCCMap[selFlight.segmentShortCode] : null;
                    }
                    return cos;
                }
            }
            
        }
        
        return cos;
    }
	
	UI_Container.getContactDetails = function(){
		if (UI_Container.addModifyAncillary || UI_Container.isModifySegment() || UI_Container.addGroundSegment || UI_Container.makePayment) {
			return $.parseJSON($("#contactInfoJson").val());
		} else {
			return UI_Passenger.getContactDetails();
		}
	}
	
	UI_Container.compareFltRefNos = function(fltRefFirst,fltRefSecond){
		var a1 = fltRefFirst.split('$');
		var a2 = fltRefSecond.split('$');
		var no1, no2;
		if(a1.length >= 3){
			no1 = a1[2];
		}else{
			no1 = fltRefFirst;
		}
		if(a2.length >= 3){
			no2 = a2[2];
		}else{
			no2 = fltRefSecond;
		}
		return (no1 == no2)
	}
	UI_Container.isOneOfModifyingSeg = function(fltRefNo){
		var modifyingSegments = UI_Container.getModifingFlightInfo();
		function normalizeFltRef(fltRef){
            fltRef = fltRef.split('#')[0];
            if(fltRef.indexOf('-') > -1){
                fltRef = fltRef.substring(0, fltRef.indexOf('-'));
            }
            return fltRef;
        }
		
		for(var i = 0; i < modifyingSegments.length; i++){
			var normfltRefNo = normalizeFltRef(modifyingSegments[i].flightRefNumber);
			if(UI_Container.compareFltRefNos(fltRefNo, normfltRefNo )){
				return true;
			}
		}
		return false;
	}
	
	
	UI_Container.getModifingFlightInfo = function(){
		if(UI_Container.isModifySegment()){
			return $.parseJSON($('#modifingFlightInfo').val());
		}		
	}	
	
	UI_Container.getBaseCurrency = function() {
		var currencies = UI_Container.getCurrencies();
		if (currencies != null && currencies != "") {
			return currencies.base;
		} else {
			return "";
		}
		
	}
	
	UI_Container.getCurrencies = function(){
		var fareQuote = UI_Container.fareQuote;
		if (fareQuote != null && fareQuote != '') {
			var currencies = {
			        base:fareQuote.inBaseCurr.currency,
			        selected:fareQuote.inSelectedCurr.currency,
			        exchangeRate:fareQuote.inSelectedCurr.exchangeRate
			        };
			if(currencies.selected == null || currencies.selected == ''){
			    currencies.selected = currencies.base;
			}
		}
		return currencies;
	}
	
	UI_Container.getPaxWiseAnci = function(){
		return UI_Anci.generatePaxWiseAnci();
	}
	
	UI_Container.getPaxArray = function(includeInfants){
		return UI_Anci.getPaxArray(includeInfants);
	}
	
	UI_Container.getInsurance = function(){
		return UI_Anci.getInsurance();
	}
	
	UI_Container.getFreeServiceDiscount = function(){
		return UI_Anci.getFreeServiceDiscount();
	}
	
	UI_Container.onNext = function() {
		// UI_Container.disableNavigation();
		var currentElement = UI_Container.pages[UI_Container.focusedPageIndex][UI_Container.focusedElementIndex];
		var fnNav = function(valid){
			if(valid){
				var next = findNextPage();
				if ( next.exitContainer ) {
					exitNext();
				} else {
					focusPage({page:next.page,element:next.element, direction: 'next'});
					UI_Container.focusedPageIndex = next.page;
					UI_Container.focusedElementIndex = next.element;
				}
			}
			// UI_Container.enableNavigation();
		}
		var invNav = false;
		var navCallbak = false;
		if(currentElement.validate != null  ){
			if(currentElement.hasCallBack){
				currentElement.validate(fnNav);
				invNav = false;
				navCallbak = true;
			}else{
				invNav = currentElement.validate();
			}
		}else{
			invNav = true;
		}		
		if(!navCallbak){
			fnNav(invNav);
		}
		
	}

	
	UI_Container.onPrevious = function() {
		var currentElement = UI_Container.pages[UI_Container.focusedPageIndex][UI_Container.focusedElementIndex];
		if (currentElement.onPrevious != null) {
			currentElement.onPrevious();
		}
		var prev = findPrevPage();
		if (prev.exitContainer) {
			UI_Container.exitPrevious();
		} else {			
			focusPage({page:prev.page,element:prev.element, direction: 'previous'});
			UI_Container.focusedPageIndex = prev.page;
			UI_Container.focusedElementIndex = prev.element;
		}
	}
	
	UI_Container.getLabels = function (){
		return UI_Container.labels;
	}
	
	UI_Container.getSpecialMessages = function(){
		return UI_Container.specialMessages;
	}
	
	UI_Container.getAirportMessage = function(){
		return UI_Container.airportMessage;
	}
	
	UI_Container.getInboundFlightSegs = function(){
		var inArr = [];
		for(var i = 0 ; i < UI_Container.selectedFlights.flightSegments.length; i++ ){
			if(UI_Container.selectedFlights.flightSegments[i].returnFlag){
				inArr[inArr.length]=UI_Container.selectedFlights.flightSegments[i];
			}
		}
		return inArr;
	}
	
	UI_Container.getOutboundFlightSegs = function(){
		var outArr = [];
		for(var i = 0 ; i < UI_Container.selectedFlights.flightSegments.length; i++ ){
			if(!UI_Container.selectedFlights.flightSegments[i].returnFlag){
				outArr[outArr.length]=UI_Container.selectedFlights.flightSegments[i];
			}
		}
		return outArr;
	}
	
	UI_Container.updateFareQuote = function(fareQuote){	
		if(UI_Container.resFlexiUserSelectionCharge >0 && (fareQuote.flexiUCharge == null || fareQuote.flexiUCharge == 'undefined')){
			fareQuote.flexiUCharge =  UI_Container.resFlexiUserSelectionCharge;
			fareQuote.hasFlexiU = true;
		}
		UI_Container.fareQuote = fareQuote;
		showPaymentSummary();
	}

	UI_Container.updateAncillary = function(anciSum){

		var fareQuote=UI_Container.fareQuote;
		if(anciSum.seatMapCharge>0){
			fareQuote.hasSeatCharge = true;
			fareQuote.seatMapCharge = $.airutil.format.currency(anciSum.seatMapCharge);
		}else{
			fareQuote.hasSeatCharge = false;
			fareQuote.seatMapCharge = 0;
		}
		
		if(anciSum.mealCharge>0){
			fareQuote.hasMealCharge = true;
			fareQuote.mealCharge = $.airutil.format.currency(anciSum.mealCharge);
			var str = "<td colspan='5'><table width='100%' cellspacing='0'><tr><td width='75%' class='defaultRowGap totalCol alignRight'><label id='lblMealTotal'>"+UI_Container.labels.lblMealTotal+"</label></td>"+
			"<td width='75%' class='defaultRowGap totalCol alignRight' style='padding:0 8px'><label>"+UI_Anci.convertAmountToSelectedCurrency(fareQuote.mealCharge)+"</label>&nbsp;<label>"
			+UI_Anci.currency.selected+"</label></td></tr></table></td>";
			$("#mealTotalROW").html(str);
		}else{
			fareQuote.hasMealCharge = false;
			fareQuote.mealCharge = 0;
			$("#mealTotalROW").html('');
		}
		if(anciSum.baggageCharge>0){
			fareQuote.hasBaggageCharge = true;
			fareQuote.baggageCharge = $.airutil.format.currency(anciSum.baggageCharge);
		}else{
			fareQuote.hasBaggageCharge = false;
			fareQuote.baggageCharge = 0;
		}
		if(anciSum.insuranceCharge>0){
			fareQuote.hasInsurance = true;
			fareQuote.insuranceCharge = $.airutil.format.currency(anciSum.insuranceCharge);
		}else{
			fareQuote.hasInsurance = false;
			fareQuote.insuranceCharge = 0;
		}
		if(anciSum.flexiCharge>0){
			fareQuote.hasFlexiU = true;
			fareQuote.flexiUCharge = $.airutil.format.currency(anciSum.flexiCharge);
		}else{
			fareQuote.hasFlexiU = false;
			fareQuote.flexiUCharge = 0;
		}
		if(anciSum.halaCharge > 0){
			fareQuote.hasAirportServiceCharge = true;
			fareQuote.airportServiceCharge = $.airutil.format.currency(anciSum.halaCharge);
		} else {
			fareQuote.hasAirportServiceCharge = false;
			fareQuote.airportServiceCharge = 0;
		}
		
		if(anciSum.apTransferCharge > 0){
			fareQuote.hasAirportTransferCharge = true;
			fareQuote.airportTransferCharge = $.airutil.format.currency(anciSum.apTransferCharge);
		} else {
			fareQuote.hasAirportTransferCharge = false;
			fareQuote.airportTransferCharge = 0;
		}
		
		if(anciSum.ssrCharge > 0){
			fareQuote.hasSsrCharge = true;
			fareQuote.ssrCharge = $.airutil.format.currency(anciSum.ssrCharge);
		} else {
			fareQuote.hasSsrCharge = false;
			fareQuote.ssrCharge = 0;
		}
		
		var getValue = function(value){ if (value == null) return 0;  else return parseFloat(value)};		
		
		var currentDiscount = getValue(fareQuote.inBaseCurr.totalIbePromoDiscount);
		var promoDiscount = 0;
		if(currentDiscount > 0 && fareQuote.deductFromTotal) {
			promoDiscount = currentDiscount;
		}
		
		var total = getValue(fareQuote.inBaseCurr.totalFare)+ getValue(fareQuote.inBaseCurr.totalTaxSurcharge) + 
			/* getValue(fareQuote.flexiCharge) + */ getValue(fareQuote.reservationBalance) 
			- promoDiscount
			+ getValue(anciSum .seatMapCharge) + getValue(anciSum.mealCharge)+ getValue(anciSum.insuranceCharge) + 
			getValue(anciSum.baggageCharge) + getValue(anciSum.halaCharge) + getValue(anciSum.apTransferCharge) + 
			getValue(anciSum.flexiCharge) - fareQuoteFlexiTotalCharge();
		if(fareQuote.utilizedCredit !=null){
			total += getValue(fareQuote.utilizedCredit);
		}
		if(fareQuote.jnTaxApplicable){
			total += getValue(fareQuote.jnTaxAmount);
		}
		if(fareQuote.hasLoyaltyCredit){
			fareQuote.hasLoyaltyCredit = false;
		}		
		
		// Modify Segment Update Fare Quote
		if (UI_Container.modifySegment == true) {	
			total = total + getValue(fareQuote.inBaseCurr.modifyCharge);
			
			var balance = total - getValue(fareQuote.inBaseCurr.totalReservationCredit);
			
			if (balance > 0 ) {
				fareQuote.hasBalanceToPay = true;
				fareQuote.hasCreditBalance = false;
				fareQuote.inBaseCurr.balaceToPay = $.airutil.format.currency(Math.abs(balance));	
				fareQuote.inBaseCurr.totalPayable = $.airutil.format.currency(Math.abs(balance));
			} else {
				fareQuote.hasBalanceToPay = false;
				fareQuote.hasCreditBalance = true;
				fareQuote.inBaseCurr.creditBalnce = $.airutil.format.currency(Math.abs(balance));	
			}
			
		}
		
		fareQuote.hasFee = false;
		fareQuote.inBaseCurr.totalPrice = $.airutil.format.currency(total);
		var selCurrTotal =parseFloat(total * getValue(fareQuote.inSelectedCurr.exchangeRate));
		fareQuote.inSelectedCurr.totalPrice = $.airutil.format.currency(selCurrTotal);
		UI_Container.updateFareQuote(fareQuote);
	}
	
	UI_Container.updatePromoDiscount = function(DiscountInfo){
		if(DiscountInfo.discount != null){
			
			var getValue = function(value){ if (value == null) return 0;  else return parseFloat(value)};
			
			var fareQuote=UI_Container.fareQuote;
			var promoDiscount = 0;
			var selCurrPromoDiscount =parseFloat(DiscountInfo.discount * getValue(fareQuote.inSelectedCurr.exchangeRate));
			fareQuote.inBaseCurr.totalIbePromoDiscount = $.airutil.format.currency(DiscountInfo.discount);
			fareQuote.inSelectedCurr.totalIbePromoDiscount = $.airutil.format.currency(selCurrPromoDiscount);
			fareQuote.hasPromoDiscount = true;
			fareQuote.deductFromTotal = DiscountInfo.deductFromTot;
			if(DiscountInfo.deductFromTot){				fareQuote.inBaseCurr.balaceToPay
				promoDiscount = DiscountInfo.discount;
			}
			
			var currentTotal = getValue(fareQuote.inBaseCurr.totalFare)+ getValue(fareQuote.inBaseCurr.totalTaxSurcharge) + 
			getValue(fareQuote.reservationBalance) 
			+ getValue(fareQuote.seatMapCharge) + getValue(fareQuote.mealCharge)+ getValue(fareQuote.insuranceCharge) + 
			getValue(fareQuote.baggageCharge) + getValue(fareQuote.airportServiceCharge);
			if(fareQuote.utilizedCredit !=null){
				currentTotal += getValue(fareQuote.utilizedCredit);
			}
			
			var newTotal = currentTotal - promoDiscount;
			var selCurrTotal =parseFloat(newTotal * getValue(fareQuote.inSelectedCurr.exchangeRate));
			
			fareQuote.inBaseCurr.totalPrice = $.airutil.format.currency(newTotal);
			fareQuote.inSelectedCurr.totalPrice = $.airutil.format.currency(selCurrTotal);
						
			UI_Container.updateFareQuote(fareQuote);
		} else if(UI_Container.discountInfo != null && UI_Container.discountInfo.promotionType == UI_Anci.promoTypes.FREESERVICE) {
			var fareQuote=UI_Container.fareQuote;
			fareQuote.hasPromoDiscount = false;
			fareQuote.inBaseCurr.totalIbePromoDiscount = "0.00";
			UI_Container.updateFareQuote(fareQuote);
		}
	}
	
	UI_Container.updateAnciPenaltyAmount = function (anciPenalty){
		
		var getValue = function(value){ if (value == null) return 0;  else return parseFloat(value)};
		
		var fareQuote = UI_Container.fareQuote;
		var penaltyAmount = anciPenalty;
		
		fareQuote.anciPenaltyApplicable = true;
		fareQuote.anciPenaltyAmount = $.airutil.format.currency(penaltyAmount);
		
		
		var currentTotal = getValue(fareQuote.inBaseCurr.totalFare)+ getValue(fareQuote.inBaseCurr.totalTaxSurcharge) 
							+ getValue(fareQuote.reservationBalance) + getValue(fareQuote.seatMapCharge) 
							+ getValue(fareQuote.mealCharge)+ getValue(fareQuote.insuranceCharge) 
							+ getValue(fareQuote.baggageCharge) + getValue(fareQuote.airportServiceCharge)
							+ getValue(fareQuote.flexiCharge) + getValue(fareQuote.jnTaxAmount);
		
		if(fareQuote.utilizedCredit !=null){
			currentTotal += getValue(fareQuote.utilizedCredit);
		}

		var newTotal = currentTotal + penaltyAmount;	
		var selCurrTotal = parseFloat(newTotal * getValue(fareQuote.inSelectedCurr.exchangeRate));
		fareQuote.inBaseCurr.totalPrice = $.airutil.format.currency(newTotal);
		fareQuote.inSelectedCurr.totalPrice = $.airutil.format.currency(selCurrTotal);
		
		UI_Container.updateFareQuote(fareQuote);
		
	}
	
	
	UI_Container.updateAnciJnTaxAmount = function(anciJnTax){
		
		var getValue = function(value){ if (value == null) return 0;  else return parseFloat(value)};
		
		var fareQuote = UI_Container.fareQuote;
		var taxAmount = anciJnTax.taxAmount;
		
		if(anciJnTax.isApplicable){
			fareQuote.jnTaxApplicable = true;
			fareQuote.jnTaxAmount = anciJnTax.taxAmount;
		} else {
			fareQuote.jnTaxApplicable = false;
		}
		
		var currentTotal = getValue(fareQuote.inBaseCurr.totalFare)+ getValue(fareQuote.inBaseCurr.totalTaxSurcharge) 
				+ getValue(fareQuote.reservationBalance) + getValue(fareQuote.seatMapCharge) 
				+ getValue(fareQuote.mealCharge)+ getValue(fareQuote.insuranceCharge) 
				+ getValue(fareQuote.baggageCharge) + getValue(fareQuote.airportServiceCharge)+ getValue(fareQuote.flexiCharge);
		if(fareQuote.utilizedCredit !=null){
			currentTotal += getValue(fareQuote.utilizedCredit);
		}
		
		var newTotal = currentTotal + taxAmount;
		
		
		// Modify Segment Update Fare Quote
		if (UI_Container.modifySegment == true) {	
			newTotal += getValue(fareQuote.inBaseCurr.modifyCharge);
			
			var balance = newTotal - getValue(fareQuote.inBaseCurr.totalReservationCredit);
			
			if (balance > 0 ) {
				fareQuote.hasBalanceToPay = true;
				fareQuote.hasCreditBalance = false;
				fareQuote.inBaseCurr.balaceToPay = $.airutil.format.currency(Math.abs(balance));	
				fareQuote.inBaseCurr.totalPayable = $.airutil.format.currency(Math.abs(balance));	
			} else {
				fareQuote.hasBalanceToPay = false;
				fareQuote.hasCreditBalance = true;
				fareQuote.inBaseCurr.creditBalnce = $.airutil.format.currency(Math.abs(balance));	
			}
			
		}
		
		
		var selCurrTotal = parseFloat(newTotal * getValue(fareQuote.inSelectedCurr.exchangeRate));
		
		fareQuote.inBaseCurr.totalPrice = $.airutil.format.currency(newTotal);
		fareQuote.inSelectedCurr.totalPrice = $.airutil.format.currency(selCurrTotal);
					
		UI_Container.updateFareQuote(fareQuote);
	} 
	
	UI_Container.showLoading = function(){
		UI_Container.disableNavigation();
		UI_commonSystem.loadingProgress();	
	}
	
	UI_Container.hideLoading = function(){
		UI_commonSystem.loadingCompleted();
		UI_Container.enableNavigation();
	}
	
	UI_Container.getSegmentName = function(code){
		var name = UI_Container.segmentNameMap[code];
		return (name==null)?buildSegemntName(code):name;
	}
	
	function buildSegemntName(code) {
		var airportCodes = code.split("/");
		var lastIndex = airportCodes.length-1;
		var airportNameDep = $.trim(UI_Container.airportNameMap[airportCodes[0]]);
		var airportNameArr = $.trim(UI_Container.airportNameMap[airportCodes[lastIndex]]);
		return ((airportNameDep == null) ? airportCodes[0] :airportNameDep)  + " / " + ((airportNameArr == null) ? airportCodes[lastIndex] :airportNameArr);
	}
	
	function submitToCustomerPage(elementId){
		$('#frmReservation').attr('action','showCustomerLoadPage!loadCustomerHomePage.action?target='+elementId);
		$('#frmReservation').attr('target','_self');
		$('#frmReservation').submit();
	}
	
	function getPageEleFor(id){
		var pages = UI_Container.pages;
		for(var i = 0 ; i < pages.length; i++) {
			for( var j = 0 ; j < pages[i].length; j++){
				if(pages[i][j].id == id){
					return {page:pages[i][j].pageId-1,element:pages[i][j].elementOrder-1};
				}
			}
		}
		return {page: UI_Container.focusedPageIndex,element:UI_Container.focusedElementIndex};
	}
	
	function checkForDuplicates(){		
		var data = {};
		var paxDetails = UI_Container.getPaxDetails();
		var paxArr = [];
		for(var i = 0 ; i < paxDetails.adults.length; i++){
			var index = paxArr.length;
			paxArr[index] = paxDetails.adults[i];
			paxArr[index].paxType= 'AD';			
		}
		for(var i = 0 ; i < paxDetails.children.length; i++){
			var index = paxArr.length;
			paxArr[index] = paxDetails.children[i];
			paxArr[index].paxType='CH';		
		}
		data['jsonPax']  = $.toJSON(paxArr);
		var hasDuplicates = false;
		$("#frmReservation").ajaxSubmit({dataType: 'json',url:'duplicateCheck.action', processData: false, async:false,	data:data,		
				success: function(response){
			if(response.success){
				if(response.hasDuplicates){
					hasDuplicates = true;
				}
			}
		}, error:UI_commonSystem.setErrorStatus});
		return hasDuplicates;
	}
	
	function validateFfidWithName(firstName, lastName, ffid){
		
	}
	
	function exitNext (){
		UI_Container.showLoading();
		if (UI_Container.paymentRetry) {
			UI_Container.processPostCardSubmit();
		} else {
		
			if(!UI_Container.addModifyAncillary && !UI_Container.makePayment){
				$('input[name="loyaltyOption"]').attr("disabled","disabled");
				/* block seat map before the payment page */
				if(!checkForDuplicates()){
					if(UI_Container.isOnholdPaymentCreateFlow == false && !UI_Anci.blockSelectedSeats()){					
						UI_Container.hideLoading();
						UI_Anci.handleSeatBlockingError();
//						var page = getPageEleFor('ANCI');
//						UI_Container.focusedPageIndex = page.page;
//						UI_Container.focusedElementIndex = page.element;
//						page.direction = 'previous';
//						focusPage(page)
						$('input[name="loyaltyOption"]').attr("disabled","");
						UI_Container.onPrevious();
						return;
					}else{
						if (UI_Container.validatePaxName(UI_Container.getPaxWiseAnci())) {
							UI_Container.processPostCardSubmit();
						}
					}
				}else{
					jAlert(UI_Container.labels.msgDuplicateNamesExist);	
					//jAlert("Passenger Name(s) already exists in this flight segment.");	
					UI_Container.hideLoading();
					$('input[name="loyaltyOption"]').attr("disabled","");
				}	
			}else{ 
				/* Modify ancillary or Make Payment*/
				$('input[name="loyaltyOption"]').attr("disabled","disabled");			
				if (UI_Container.validatePaxName(UI_Container.getPaxWiseAnci())) {
					UI_Container.processPostCardSubmit();
				}
			}	
		}
	}
	
	
	/**
	 * This method has been implemented to avoid IBE pax name issue(pax name as null/ T B A)
	 * It will not be display error message for every bookings while the issue can not be reproduced.
	 * We assumed that the issue is happening due to browser issue
	 * As workaround we will validate pax name to break the reservation flow
	 *    
	 */
	UI_Container.validatePaxName = function(paxList) {
		var isValid = true;
		if (paxList != null && paxList != "") {
			var paxCount = paxList.length;
			for (var i = 0; i < paxCount; i++) {				
				if (paxList[i].firstName  == null || $.trim(paxList[i].firstName ).length == 0) {
					UI_Container.hideLoading();
					jAlert("The system couldn't process your request due to inaccuracy of data. Please start the process again.");					
					isValid = false;								
					break;					
				}	
			}			
		}
		return isValid;			
	}	
	UI_Container.processPostCardSubmit = function() {
		var action = UI_Container.actions.PAYMENT_DEFAULT_POSTCARD;
		if (UI_Container.paymentRetry) {
			action = UI_Container.actions.PAYMENT_RETRY_POSTCARD;
		} else {
			$('#frmFare').attr('target','_self');
			$('#jsonInsurance').val($.toJSON(UI_Container.getInsurance()));
			$('#jsonPaxWiseAnci').val($.toJSON(UI_Container.getPaxWiseAnci()));
			$("#totalSegmentCount").val(UI_commonSystem.totalSegCount);
			$("#jsonPaxInfo").val($.toJSON(UI_Container.getPaxArray()));
		}
		$('#frmReservation').attr('action', action);
		// Cancel session timeout - payment is processing in iframe
		if ( UI_Container.isKiosk || ((UI_Container.isRegisteredUser || UI_Container.modifySegment)
				&& UI_Container.sessionTimeOutDisplay )) {						
			UI_commonSystem.clearTimeoutSessionExpire();
		}
		
		if($("#viewPaymentInIframe").val() == "true" && $("#switchToExternalURL").val() == "false" && $("#brokerType").val() == "external"){
			if (UI_Payment.getPaymentType() != "ONHOLD"){
				var ifClone = $("#cardInputs").clone();
				ifClone.attr({"id": "cardInputs_"+UI_Payment.paymentGatewayID, "name":"cardInputs_"+UI_Payment.paymentGatewayID});
				var frmTarget = ifClone.attr("id");
				$("#paymentPannelPosition").empty().append(ifClone);
				ifClone.css({
					"position":"relative",
					"height": (ui_paymentGWManger.iframesHeight[UI_Payment.paymentGatewayID] + 13),
					"width": "100%"
				});
				$("#btnPurchase").hide();
				ifClone.show();
				$("#intExtProcessing").show();
				setTimeout(function(){$("#intExtProcessing").hide()},4000);// common timeout for loading external payment gateways
			}
	
		
			if (UI_Payment.getPaymentType() == "NORMAL") {
				UI_Container.isPaymentProcessing = true;	
				UI_Payment.timerPayment = setTimeout('UI_Payment.isTransationCompleted()', UI_Payment.payTimeGap);
			} else {
				UI_Container.isPaymentProcessing = false;
			}
			if ( UI_Payment.getPaymentType() == "ONHOLD" && UI_Payment.brokerType == "onHold" && UI_Payment.onHoldImageCapcha){
				UI_Container.enableNavigation();
				UI_Payment.isValideImageChapcha();
			}else{
				$("#cardInputPannel").show();
				if(UI_Container.useIframeInPostingParams) {
					UI_Container.hideLoading();
					$('#frmReservation').attr('target',frmTarget);
				} else {
					$('#frmReservation').attr('target','_self');
				}
				$('#frmReservation').submit();
				
			}
		}else {	
			if (UI_Container.isExternalPaymentGateway) {	
				if (UI_Payment.getPaymentType() != "ONHOLD"){
					$("#divLoadBg").hide();
					
					if ($.browser.msie && $.browser.version.substr(0,1)<7) {
						$("#cardInputs").attr("height", $(window).height());
						$("#cardInputs").attr("width", $(window).width());
					} else {
						$("#cardInputs").attr("height", "100%");
						$("#cardInputs").attr("width", "100%");
					}
					
					$("#cardInputs").css("top", "0px");
					$("#cardInputs").css("left", "0px");
				}
			} else {
				if (UI_Payment.getPaymentType() != "ONHOLD"){
					$("#cardInputs").attr("height", "400px");
					$("#cardInputs").attr("width", $("#paymentPannelPosition").width());
					$("#payByCreditCardPanel").hide();
					$("#paymentButtonset").hide();
					$("#intExtProcessing").show();
					var position = $("#paymentPannelPosition").position();						
					$("#cardInputs").css("top", position.top);
					if (UI_Container.language == 'ar') {
						$("#cardInputs").css("left", position.left - $("#paymentPannelPosition").width());
					} else {
						$("#cardInputs").css("left", position.left);
					}
					$("#cardInputs").show();	
				}
			}		
		
			if (UI_Payment.getPaymentType() == "NORMAL") {
				UI_Container.isPaymentProcessing = true;	
				UI_Payment.timerPayment = setTimeout('UI_Payment.isTransationCompleted()', UI_Payment.payTimeGap);
			} else {
				UI_Container.isPaymentProcessing = false;
			}
			if ( UI_Payment.getPaymentType() == "ONHOLD" && UI_Payment.brokerType == "onHold" && UI_Payment.onHoldImageCapcha){
				UI_Container.enableNavigation();
				UI_Payment.isValideImageChapcha();
			}else{
				$("#cardInputPannel").show();
				
				if(UI_Container.useIframeInPostingParams) {
					$('#frmReservation').attr('target','cardInputs');
				} else {
					$('#frmReservation').attr('target','_self');
				}
				$('#frmReservation').submit();
			}
		}
	}
	
	UI_Container.exitPrevious =  function(){
		UI_Container.showLoading();
		if(!UI_Container.addModifyAncillary && !UI_Container.makePayment){
			/* release blocked seats if any selected */
			UI_Anci.releaseBlockedSeats();		
			var url = 'showLoadPage!loadFlightSearch.action';
			if($('#fromSecure').val() == 'true' && !UI_Container.isKiosk){
				url = SYS_IBECommonParam.securePath+url;
			}else{
				url = SYS_IBECommonParam.nonSecurePath+url;
			}						
			$('#onPreviousClick').val("true");
			$('#frmReservation').attr('action',url);
			$('#frmReservation').attr('target','_self');
			$('#frmReservation').submit();
		}else{
			if(UI_Container.isRegisteredUser){
				submitToCustomerPage("tdReservationList");
			}else{
				SYS_IBECommonParam.homeClick();
			}
		}
	}
	
	function buildTimeoutBanner(timeoutDTO){
	// $('body').append('<div id="divSessContainer"></div>');
	// UI_commonSystem.initSessionTimeout('divSessContainer',timeoutDTO,function(){});
	}
	
	function buildLocale(){
		$("#divLoadBg").slideDown("slow");
		$("#divLoadBg").populateLanguage({messageList:UI_Container.labels});
	}
	
	
	function getBuiltPages(){
		var blocks = {};
		
		blocks['PAX'] = {
				id : 'PAX',
				onready : UI_Passenger.ready,
				div : 'divPassenger',
				anchor: 'ancPassenger',
				validate:UI_Passenger.paxValidat,
				hasCallBack: true,
				onPrevious: null,
				gAnalytics: {enabled: true, pageId: "/funnel_G1/Passenger/"}
			};
		blocks['ANCI'] = {
				id : 'ANCI',
				onready : UI_Anci.ready,
				div : 'divAncillary',
				anchor: 'ancAncillary',
				validate:UI_Anci.continueOnClick,
				hasCallBack: true,
				onPrevious: null,
				gAnalytics: {enabled: true, pageId: "/funnel_G1/SeatMap/"}
			};
		
		blocks['PAYMENT'] = {
				id : 'PAYMENT',
				onready : UI_Payment.ready,
				div : 'divPayment',
				anchor: 'ancPayment',
				validate:UI_Payment.validate,
				hasCallBack: false,
				onPrevious: UI_Payment.onPrevNavigation,
				gAnalytics: {enabled: true, pageId: "/funnel_G1/Payment/"}
			};
		
		blocks['MODIFYCONFIRM'] = {
				id : 'MODIFYCONFIRM',
				onready : UI_ModifyConfirm.ready,
				div : 'divModifyConfirm',
				anchor: 'ancModifyConfirm',
				validate:null,
				hasCallBack: false,
				onPrevious: null,
				gAnalytics: {enabled: false, pageId: ''}
			};
		
		var Page = function(id) {
			this.id = id;
			this.elementOrder = 1;
			this.elements = [];
			this.addElement = function(obj) {
				element = blocks[obj.id];
				if(element!=null){
					element.elementOrder = this.elementOrder++;
					element.pageId = this.id;
					this.elements[this.elements.length] = element;
				}else{
					throw 'Invalid element id '+obj.id;
				}
				return this;

			}

			this.getElements = function() {
				return this.elements;
			}
		}

		var PageBuilder = function() {
			this.pages = [];
			this.pageId = 1;
			this.newPage = function() {
				var p = new Page(this.pageId++);
				this.pages[this.pages.length] = p;
				return p;
			}
				this.getPages = function() {
				var arr = [];
				var globalOrder = 1;
				for ( var i = 0; i < this.pages.length; i++) {
					var page = this.pages[i];
					var elements = page.getElements();
					for ( var j = 0; j < elements.length; j++) {
						elements[j].globalOrder = globalOrder++;
					}
					arr[arr.length] = elements;
				}
				return arr;
			}
		}
	        
		var layout = [];
		/**
		 * Sample Layout structure [[{id:'PAX'}], [{id:'ANCI'}]] - only one
		 * element per page [[{id:'PAX'}, {id:'ANCI'}], [{id:'PAYMENT'}]] -
		 * multiple elements in the first page ( pax & anci)
		 */
		var extLayout = null;
		if(anciConfig!=null && anciConfig.layout!=null){
			extLayout = anciConfig.layout;
		}
		
		if (UI_Container.makePayment) {
			layout = (extLayout.makePayment==null)?[[{id:'PAYMENT'}]]:extLayout.makePayment;
		} else if(UI_Container.addModifyAncillary) {
			layout = (extLayout.addModifyAncillary==null)?[[{id:'ANCI'}], [{id:'PAYMENT'}]]:extLayout.addModifyAncillary;
		} else if(UI_Container.isRequoteFlow()) {
			layout = (extLayout.requoteFlow==null)? [[{id:'MODIFYCONFIRM'}],[{id:'PAYMENT'}]]:extLayout.requoteFlow;;
		} else if(UI_Container.isModifySegment() || UI_Container.addGroundSegment) {
			layout = [[{id:'ANCI'}], [{id:'PAYMENT'}]];
		} else {
			layout = [[{id:'PAX'}], [{id:'ANCI'}], [{id:'PAYMENT'}]];
		}		      
		var pages = [];
		var pb = new PageBuilder();
		for(var i=0; i<layout.length; i++){
			var page = pb.newPage();
			for(var j = 0 ; j < layout[i].length;j++){
				page.addElement(layout[i][j]);
			}
		}
		pages = pb.getPages();
		
		return pages;
	}
	
	
	UI_Container.buildSegmentNameMap = function(segments){
		var map = {};
		for(var i = 0 ; i < segments.length; i++){
			map[segments[i].segmentShortCode]= segments[i].segmentCode;
			buildAirportNameMap(segments[i]);
		}
		UI_Container.segmentNameMap = map;
	}
	
	function buildAirportNameMap(segment) {
		var map = UI_Container.airportNameMap;
		if (segment != null) {
			var airportShortCodes = segment.segmentShortCode.split("/");
			var airportNames = segment.segmentCode.split("/");
			if (airportShortCodes.length == airportNames.length) {
				for (var i = 0; i < airportShortCodes.length; i++) {
					if (map[airportShortCodes[i]] == null) {
						map[airportShortCodes[i]] = airportNames[i];
					}					
				}
			}
		}
	}
	
	function layoutPage() {
		UI_Container.paxTypeCounts.adults = parseInt($("#resAdultCount").val(), 10);
		UI_Container.paxTypeCounts.children = parseInt($("#resChildCount").val(), 10);
		UI_Container.paxTypeCounts.infants = parseInt($("#resInfantCount").val(), 10);
	
		// UI_Container.selectedFlights.isReturnFlight =
		// ($('#resReturnFlag').val() == 'true');
//		$('#balanceQueryData').val($('#balanceQueryData').val())
		
		UI_Container.fareQuote = $.parseJSON($('#resFareQuote').val());
		var selectedFlights = $.parseJSON($('#resSelectedFlights').val());
		
		// French translation issue fix for Arrival & Departure dates - Supports all languages
		
		if(typeof(selectedFlights.flightSegments) !== "undefined" && selectedFlights.flightSegments.length > 0){
			for(var i = 0; i < selectedFlights.flightSegments.length;i++){
				if(typeof(selectedFlights.flightSegments[i]) !== "undefined"){
					selectedFlights.flightSegments[i].arrivalDate = unescape(selectedFlights.flightSegments[i].arrivalDate);
					selectedFlights.flightSegments[i].departureDate = unescape(selectedFlights.flightSegments[i].departureDate);
				}
			}
		}
		
		UI_Container.selectedFlights = selectedFlights;
		UI_Container.discountInfo = $.parseJSON($('#respromotionInfo').val());
		
		if (!UI_Container.paymentRetry) {
			UI_Container.buildSegmentNameMap(selectedFlights.flightSegments);
			$(".bookingSummary").summaryPanel({id:"BookingSummary", data:UI_Container.selectedFlights, labels:UI_Container.labels });
			$(".paymentSummary").summaryPanel({id:"PaymentSummary", data:UI_Container.fareQuote, labels:UI_Container.labels });
		}		
		// showPaymentSummary();
		// showSelectedFlightsSummary(selectedFlights);
	
		// convert to persian date format(fa)
		dateChangerjobforLanguage(SYS_IBECommonParam.locale);
		var pages = getBuiltPages();		
		UI_Container.pages = pages;
		// FIXME order the pages according to page and element order and
		// assign'em global order
		for(var i = 0 ; i< pages.length; i ++){
			for(var j = 0 ; j < pages[i].length; j++){
				var tempE = $('#'+pages[i][j].div).detach();
				tempE.appendTo('#page_'+pages[i][j].globalOrder);
			}
		}
	
		var labels = UI_Container.labels;
		if(!UI_Container.navInitialized){
			UI_Container.navInitialized = true;
			if ($('button[name="btnNext"]').children().length == 0 ){
				$('button[name="btnNext"]').text(labels['btnNext']);
			}else{
				$('button[name="btnNext"]').find("td.buttonMiddle").text(labels['btnNext']);
			}
			$('button[name="btnNext"]').click(function() {
				// FIX ME- Temp Add For Modify Segment No Pay Functionality
				if (UI_Container.isModifySegment && UI_ModifyConfirm.blnNoPay == true) {
					UI_ModifyConfirm.nextClick();
				} else {
					UI_Container.onNext();
				}
			});
			if ($('button[name="btnPrevious"]').children().length == 0 ){
				$('button[name="btnPrevious"]').text(labels['btnPrevious']);
			}else{
				$('button[name="btnPrevious"]').find("td.buttonMiddle").text(labels['btnPrevious']);
			}
			$('button[name="btnPrevious"]').click(function() {
				UI_Container.onPrevious();
			});
			
			
			showPaymentOnAnci = function(res){
				if (res){
					$("#divAncillary").find(".buttonset").hide();
					// $("#divPayment").slideDown(100);
					$(".tabBody").slideDown(100);
					enableDisableAncillary(true)
					UI_Payment.ready();
					$('html, body').animate({scrollTop: $("#divPayment").offset().top}, 500);
					// $("#divPayment").focus();
				}
			}
			
			 hidePaymentOnAnci = function(){
				$("#divAncillary").find(".buttonset").slideDown(100);
				$("#divPayment").hide();
				enableDisableAncillary(false);
				// FIXME Generic way
				if (UI_Container.hasInsurance && UI_Anci.isRakInsurance) {
					$("#chkAnciIns").attr("disabled","disabled");
				}
				$('html, body').animate({scrollTop: 0}, 500);
			}
			 
			enableDisableAncillary = function(endi){
				if (endi){
					$("#divAncillary").find("input, select").attr("disabled","disabled");
					var dib = $(".disabledLinks").css({
						"position" : "absolute", "background":"#ffffff", "z-index" : "10000", "opacity":"0.0"
					});
					$.each(dib,function(i,k){
						$(k).css("height" , $(k).prev().height());
						$(k).css("width" , $(k).prev().width());
						$(k).css("top" , $(k).prev().position().top);
						$(k).css("left" , $(k).prev().position().left);
						$(k).show();
					});
				}else{
					$("#divAncillary").find("input, select").attr("disabled","");
					$(".disabledLinks").hide();
				}
			}
			
			
			if (anciConfig.paymentOnAnici){
				$('#anciBtnPayment').click(function() {
					UI_Anci.continueOnClick(showPaymentOnAnci);
				});
				$('#butEditAnci').click(function() {
					hidePaymentOnAnci();
				});
				$('#btnPurchaseOnAnci').click(function() {
					if (anciConfig.displayCCMsgAlert){
						jConfirm($('#specialMsg').children().html() , UI_Container.labels.lblAlertConfirm,
							function(response){
								if (response)
									exitNext();	
							}
						);
					}else{
						exitNext();
					}
				});
				
			}

			$('input[name="btnSOver"]').attr('value',labels['btnSOver']).click(function() {
				/* release ancillary if any selected */
				UI_Anci.releaseBlockedSeats();
			});
		}
		
		UI_Container.focusedPageIndex = 0;
		UI_Container.focusedElementIndex = 0;
		// Show the first page
		focusPage({page: UI_Container.focusedPageIndex,element:UI_Container.focusedElementIndex,direction:'next'});
	
	}
	
	UI_Container.disableNavigation = function(){
		$('button[name="btnPrevious"]').disableNav();
		$('button[name="btnNext"]').disableNav();
	}
	
	UI_Container.enableNavigation = function(){
		$('button[name="btnPrevious"]').enableNav();
		$('button[name="btnNext"]').enableNav();
	}
	
	UI_Container.showSelectedAncillary = function(segData){		
		for(var i = 0 ; i < segData.length; i++ ){
			var anci =$('#flightTemplate_'+i+' span[name="anciDetails"]');	
			anci.find('div').remove();
			if (segData[i].seats.length > 0 || segData[i].meals.length > 0 || 
					segData[i].ssrs.length > 0 || segData[i].baggages.length > 0 || segData[i].apss.length > 0 || segData[i].apts.length > 0){
			anci.append('<div><label class="fntBold" id="lblAditionalServices">'+UI_Container.labels.lblAditionalServices+'</label><div>');
			anci.append('<div id="anciDetailsSeat_'+i+'"><div>');
			anci.append('<div id="anciDetailsMeal_'+i+'"><div>');
			anci.append('<div id="anciDetailsBaggage_'+i+'"><div>');
			anci.append('<div id="anciDetailsHala_'+i+'"><div>');
			anci.append('<div id="anciDetailsSSR_'+i+'"><div>');
			anci.append('<div id="anciDetailsAPT_'+i+'"><div>');
			}
			for( var k = 0 ; k < segData[i].seats.length; k++){
				if (segData[i].seats.length == k+1)
					$('#anciDetailsSeat_'+i).append('<label>'+segData[i].seats[k]+'</label>');
				else
					$('#anciDetailsSeat_'+i).append('<label>'+segData[i].seats[k]+', </label>');
			}
			for( var k = 0 ; k < segData[i].meals.length; k++){
				if (segData[i].meals.length == k+1)
					$('#anciDetailsMeal_'+i).append('<label>'+segData[i].meals[k]+'</label>');
				else
					$('#anciDetailsMeal_'+i).append('<label>'+segData[i].meals[k]+', </label>');
			}
			for( var k = 0 ; k < segData[i].baggages.length; k++){
				if (segData[i].baggages.length == k+1)
					$('#anciDetailsBaggage_'+i).append('<label>'+segData[i].baggages[k]+'</label>');
				else
					$('#anciDetailsBaggage_'+i).append('<label>'+segData[i].baggages[k]+', </label>');
			}
			for( var k = 0 ; k < segData[i].ssrs.length; k++){
				if (segData[i].ssrs.length == k+1)
					$('#anciDetailsSSR_'+i).append('<label>'+segData[i].ssrs[k]+'</label>');
				else
					$('#anciDetailsSSR_'+i).append('<label>'+segData[i].ssrs[k]+', </label>');
			}
			for( var k = 0 ; k < segData[i].apss.length; k++){
				if (segData[i].apss.length == k+1)
					$('#anciDetailsHala_'+i).append('<label>'+segData[i].apss[k]+'</label>');
				else
					$('#anciDetailsHala_'+i).append('<label>'+segData[i].apss[k]+', </label>');
			}
			for( var k = 0 ; k < segData[i].apts.length; k++){
				if (segData[i].apts.length == k+1)
					$('#anciDetailsAPT_'+i).append('<label>'+segData[i].apts[k]+'</label>');
				else
					$('#anciDetailsAPT_'+i).append('<label>'+segData[i].apts[k]+', </label>');
			}
		}
	}
	
	UI_Container.setDiscountInfo = function(discountInfo){
		UI_Container.discountInfo = discountInfo;
	}
	
	UI_Container.getDiscountInfo = function(discountInfo){
		return UI_Container.discountInfo;
	}
	
	UI_Container.isBINPromotion = function(){
		var promotionApplicableBINs = UI_Container.getApplicableBINs();
		if(promotionApplicableBINs != null && promotionApplicableBINs.length > 0){
			return true;
		}
		
		return false;
	}

	UI_Container.getApplicableBINs = function(){
		var discountInfo = UI_Container.getDiscountInfo();
		if(discountInfo != null && discountInfo.promotionId != null){
			return discountInfo.applicableBINs;
		}
		
		return null;
	}
	
	
	function showPaymentSummary() {
		var fareQuote = UI_Container.fareQuote;
		if (fareQuote.inSelectedCurr.currency==null){
			fareQuote.inSelectedCurr.currency = fareQuote.inBaseCurr.currency;
		}
		$('label[name="valBaseCurrCode"]').text(fareQuote.inBaseCurr.currency);
		$('label[name="valSelectedCurrCode"]').text(
				fareQuote.inSelectedCurr.currency);
		if(parseFloat(fareQuote.inBaseCurr.totalFare)>0){
			$("#valAirFare").text(fareQuote.inBaseCurr.totalFare);
			$('#trAirFare').show();
		}else{
			$('#trAirFare').hide();
		}
		
		if(parseFloat(fareQuote.inBaseCurr.totalTaxSurcharge)>0){
			$("#valCharges").text(fareQuote.inBaseCurr.totalTaxSurcharge);
			$('#trCharges').show();
		}else{
			$('#trCharges').hide();
		}
		
		 if(fareQuote.hasFlexiU){
			 var flexiUAddedCharge = 0;
			 
			 if(UI_Anci.jsAnciFlexiUAdded[0]){
				 flexiUAddedCharge = +JSON.parse($("input#resOndwiseFlexiChargesForAnci").val())[0];
			 }
			 if(UI_Anci.jsAnciFlexiUAdded[1]){
				 flexiUAddedCharge += +JSON.parse($("input#resOndwiseFlexiChargesForAnci").val())[1];
			 }
			 if(UI_Anci.jsAnciFlexiUAdded[2]){
				 flexiUAddedCharge += +JSON.parse($("input#resOndwiseFlexiChargesForAnci").val())[2];
			 }
			 if(UI_Anci.jsAnciFlexiUAdded[3]){
				 flexiUAddedCharge += +JSON.parse($("input#resOndwiseFlexiChargesForAnci").val())[3];
			 }
			 
			 UI_Container.fareQuoteOndFlexiChargesTotal = parseFloat(UI_Container.fareQuoteOndFlexiChargesTotal);
			 if(flexiUAddedCharge>0 || UI_Container.fareQuoteOndFlexiChargesTotal>0){
				 //fare quote flexi charges + anci flexi charges
				 $("#valFlexiCharges").text(UI_Anci.convertAmountToSelectedCurrency(flexiUAddedCharge + UI_Container.fareQuoteOndFlexiChargesTotal));
				 $('#trFlexiCharges').show();
			 }else{
				 $('#trFlexiCharges').hide();
			 }
		 } else {
			 $('#trFlexiCharges').hide();
		 }
			
		if (fareQuote.hasFee && parseFloat(fareQuote.inBaseCurr.totalFee) > 0) {
			$('#trTxnFee').show();
			$("#valTxnFee").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.totalFee));
		} else {
			$('#trTxnFee').hide();
		}
		$("#valTotal").text(fareQuote.inBaseCurr.totalPrice);
	
		if (fareQuote.hasSeatCharge) {
			$("#valSeatCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.seatMapCharge));
			$('#trSeatCharges').show();
		} else {
			$('#trSeatCharges').hide();
		}
	
		if (fareQuote.hasMealCharge && (UI_Container.isFOCmealEnabled == false)) {
			$("#valMealCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.mealCharge));
			$('#trMealCharges').show();
		} else {
			$('#trMealCharges').hide();
		}
		
		if (fareQuote.hasBaggageCharge ) {
			$("#valBaggageCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.baggageCharge));
			$('#trBaggageCharges').show();
		} else {
			$('#trBaggageCharges').hide();
		}
		
		if (fareQuote.hasAirportServiceCharge ) {
			$("#valHalaCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.airportServiceCharge));
			$('#trHalaCharges').show();
		} else {
			$('#trHalaCharges').hide();
		}

        if (fareQuote.hasSsrCharge ) {
			$("#valSsrCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.ssrCharge));
			$('#trSsrCharges').show();
		} else {
			$('#trSsrCharges').hide();
		}
		
		if (fareQuote.hasAirportTransferCharge ) {
			$("#valAptCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.airportTransferCharge));
			$('#trAptCharges').show();
		} else {
			$('#trAptCharges').hide();
		}
		
		if (fareQuote.hasNextSeatPromoCharge ) {
			$("#valNextSeatPromoCharges").text(fareQuote.nextSeatPromoCharge);
			$('#trPromoNextSeat').show();
		} else {
			$('#trPromoNextSeat').hide();
		}
	
		if (fareQuote.hasInsurance) {
			$("#valInsuranceCharges").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.insuranceCharge));
			$('#trInsuranceCharges').show();
		} else {
			$('#trInsuranceCharges').hide();
		}
		
		if(fareQuote.jnTaxApplicable) {
			$("#valJnTaxForAnci").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.jnTaxAmount));
			$('#trJnTaxAnci').show();
		} else {
			$('#trJnTaxAnci').hide();
		}

		if (fareQuote.inSelectedCurr.currency != fareQuote.inBaseCurr.currency
				&& fareQuote.inSelectedCurr.currency != null  && 
				parseFloat(fareQuote.inSelectedCurr.totalPrice)>= 0) {
			$('label[name="valSelectedCurrCode"]').text(
					fareQuote.inSelectedCurr.currency);
			$("#valSelectedTotal").text(fareQuote.inSelectedCurr.totalPrice);
			$('#trSelectedCurr').show();
		} else {
			$('#trSelectedCurr').hide();
		}
		if(fareQuote.inSelectedCurr.currency != ""){
			$("#trResCredit").find('label[name="valBaseCurrCode"]').text(fareQuote.inSelectedCurr.currency);
			$("#trResBalanceToPay").find('label[name="valBaseCurrCode"]').text(fareQuote.inSelectedCurr.currency);
			$("#trCreditBalance").find('label[name="valBaseCurrCode"]').text(fareQuote.inSelectedCurr.currency);
			$("#trModificationCharge").find('label[name="valBaseCurrCode"]').text(fareQuote.inSelectedCurr.currency);
		}		
		
		
		if (UI_Container.modifySegment == true) {			
			if (fareQuote.hasTotalReservationCredit) {
				$("#trResCredit").show();
				if(fareQuote.inSelectedCurr.currency != ""){
					$("#valTotalReservationCredit").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.totalReservationCredit));
				} else {
					$("#valTotalReservationCredit").text(fareQuote.inBaseCurr.totalReservationCredit);
				}
			}
			
			if (fareQuote.hasBalanceToPay) {
				if(parseFloat(fareQuote.inBaseCurr.totalPayable)>0){
					if(fareQuote.inSelectedCurr.currency != ""){
						$("#valModifyBalanceToPay").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.totalPayable));
					} else {
						$("#valModifyBalanceToPay").text(fareQuote.inBaseCurr.totalPayable);						
					}
				}
				else{
					$("#valModifyBalanceToPay").text(fareQuote.inBaseCurr.balaceToPay);
				}
				$("#trResBalanceToPay").show();
			} else {
				$("#trResBalanceToPay").hide();
			}
			if (fareQuote.hasCreditBalance) {
				if(fareQuote.inSelectedCurr.currency != ""){
					$("#valResCreditBalance").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.creditBalnce));
				} else {
					$("#valResCreditBalance").text(fareQuote.inBaseCurr.creditBalnce);					
				}
				$("#trCreditBalance").show();
			} else {
				$("#trCreditBalance").hide();
			}
			if(fareQuote.inSelectedCurr.currency != ""){
				$("#valModificationCharge").text(UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.modifyCharge));
			} else {
				$("#valModificationCharge").text(fareQuote.inBaseCurr.modifyCharge);	
			}
			
			$("#trModificationCharge").show();
		} else {
			$("#trResCredit").hide();
			$("#trModificationCharge").hide();
			$("#trBalanceToPay").hide();
			$("#trCreditBalance").hide();			
		}
		
		if(UI_Container.addModifyAncillary == true && fareQuote.anciPenaltyApplicable && parseFloat(fareQuote.anciPenaltyAmount)>0 ){
			$("#valAnciDowngradePenalty").text($.airutil.format.currency(parseFloat(UI_Anci.convertAmountToSelectedCurrency(UI_Container.fareQuoteOndFlexiChargesTotal)) + parseFloat(UI_Anci.convertAmountToSelectedCurrency(fareQuote.anciPenaltyAmount))));
			$('#anciDowngradePenalty').show();
		}else{
			$('#anciDowngradePenalty').hide();
		}
		
		
		// Design version 1
		if (fareQuote.hasFee || fareQuote.hasSeatCharge || fareQuote.hasMealCharge || 
				fareQuote.hasInsurance || fareQuote.hasBaggageCharge || fareQuote.hasAirportServiceCharge || 
				fareQuote.hasFlexiU || fareQuote.hasModifySegment || fareQuote.anciPenaltyApplicable) {			
			if (!UI_Container.paymentRetry) {	
				$("#addServiceDiv").slideDown("slow");	
			}
		} else {
			$("#addServiceDiv").slideUp("slow");
		}
		
		var discountValue = parseFloat(fareQuote.inBaseCurr.totalIbeFareDiscount);
		if(discountValue > 0){
			$("#totalDiscountDiv").show();
			$("#valDiscount").text('-' + UI_Anci.convertAmountToSelectedCurrency(fareQuote.inBaseCurr.totalIbeFareDiscount));
		} else {
			$("#totalDiscountDiv").hide();
		}
		
		if(fareQuote.hasPromoDiscount){
			var dispDiscountVal = '';
			if (fareQuote.inSelectedCurr.currency != null && 
					fareQuote.inSelectedCurr.currency != fareQuote.inBaseCurr.currency){
				dispDiscountVal = fareQuote.inSelectedCurr.totalIbePromoDiscount;
			} else {				
				dispDiscountVal = fareQuote.inBaseCurr.totalIbePromoDiscount;
			}
			
			if(fareQuote.deductFromTotal){
				var isOutBoundPromo = UI_Container.discountInfo.applicability =="OB" ? true:false;
				$(".valPromoDiscount").text('-' + dispDiscountVal);
				if(isOutBoundPromo){
					$("#totalPromoDiscountDiv").hide();
					$("#totalPromoDiscount_OB_Div").show();
				}else{
					$("#totalPromoDiscountDiv").show();
					$("#totalPromoDiscount_OB_Div").hide();
				}
				$("#totalPromoDiscountDivCredit").hide();
			} else {
				$(".valPromoDiscount").text(dispDiscountVal);
				$("#totalPromoDiscountDivCredit").show();
				$("#totalPromoDiscountDiv").hide();
				$("#totalPromoDiscount_OB_Div").hide();
			}
		} else {
			$("#totalPromoDiscountDiv").hide();
			$("#totalPromoDiscount_OB_Div").hide();
			$("#totalPromoDiscountDivCredit").hide();
		}
	
	}
	
	function focusPage(current) {
		var pages = UI_Container.pages;
		for ( var i = 0; i < pages.length; i++) {
			if (i == current.page) {			
				showHideElements(true, pages[i], current);
			} else {
				showHideElements(false, pages[i], current);
			}
		}
		
	}
	
	function sendGoogleAnlData(gAnalytics) {
		var strAnalyticPage = gAnalytics.pageId;
		
		var mode = "CRE";
		if (UI_Container.addModifyAncillary == true || UI_Container.modifySegment == true) {
			mode = "MOD";
		} 
		
		var way = "OW";
		if(UI_Container.selectedFlights.isReturnFlight == "true") {
			way = "RT";
		}

		var path = "";
		
		// FIXME this might give surprises in the modify segment,
		// add/modify ancillary scenarios when flight segments not necessarily
		// in correct order
		// Wrap this up properly in a get origin destination method
		path = UI_Container.getOutboundFlightSegs()[0].segmentShortCode.split("/")[0];
        var originID = UI_Container.getOutboundFlightSegs()[0].segmentShortCode.split("/")[0];
        var departDate = UI_Container.getOutboundFlightSegs()[0].depLocalDateTimeJson.split("T")[0];
		path +=  UI_Container.getOutboundFlightSegs()[UI_Container.getOutboundFlightSegs().length - 1].segmentShortCode.split("/")[1];
        var destID = UI_Container.getOutboundFlightSegs()[UI_Container.getOutboundFlightSegs().length - 1].segmentShortCode.split("/")[1];
        var returnDate = "";
        if (UI_Container.getInboundFlightSegs().length>0){
            returnDate = UI_Container.getInboundFlightSegs()[0].depLocalDateTimeJson.split("T")[0];
            destID = UI_Container.getInboundFlightSegs()[0].segmentShortCode.split("/")[0];
        }

		strAnalyticPage += mode + "/" + UI_Container.carrier + "/" + path + "/" + way;		
		if(strAnalyticEnable == "true") {			
			//pageTracker._trackPageview(strAnalyticPage);
			_gaq.push(['_trackPageview', strAnalyticPage ]);
		}
        if (gAnalytics.pageId.indexOf("/Payment")>-1){
            //Set Dyn Ads for Flights on search results custom tag
            if(ga_conversion_id===undefined){var ga_conversion_id=00000000}
            if( window.google_trackConversion !== undefined) {
                window.google_trackConversion({
                    google_conversion_id: ga_conversion_id,
                    google_remarketing_only: true,
                    google_tag_params: {
                        flight_originid: originID,
                        flight_destid: destID,
                        flight_startdate: departDate,
                        flight_enddate: returnDate,
                        flight_pagetype: 'cart'
                    }
                });
            }
        }


	}

	function showHideElements(show, elements, current){
		for(var i = 0; i < elements.length; i ++){
			$('#' + elements[i].div+' .tabBody').hide();
			if(show){
					if(current.element == i){
						var tempObj = elements[i];
						$('#' + elements[i].div+' .tabBody').slideDown('slow')
							// $('#' + tempObj.anchor).focus();
							$('html,body').animate({scrollTop: 0},
							{duration: 500});
						if  (UI_Container.isRequoteFlow()){
							UI_commonSystem.stepsSettings(tempObj.globalOrder+2,"interLineContainer");
						}else if (UI_Container.addModifyAncillary || UI_Container.modifySegment || UI_Container.addGroundSegment){
							UI_commonSystem.stepsSettings(tempObj.globalOrder+2,"interLineContainer");
						} else if (UI_Container.makePayment){
							UI_commonSystem.stepsSettings(tempObj.globalOrder+3,"interLineContainer");
						} else {
							UI_commonSystem.stepsSettings(tempObj.globalOrder+1,"interLineContainer");
						}
						try {
							if(tempObj.gAnalytics.enabled){
								sendGoogleAnlData(tempObj.gAnalytics);
							}
						} catch(ex) {}
						tempObj.onready(current.direction);
						
					}
					$('#' + elements[i].div).slideDown('slow');
			}else{
				$('#' + elements[i].div).slideUp('slow');
			}
		}
	}
	
	function findNextPage(){
		var elements =UI_Container.pages[UI_Container.focusedPageIndex];
		var pageI = UI_Container.focusedPageIndex;
		var eleI = UI_Container.focusedElementIndex;
		var exitContainer = false;
		if(elements.length == eleI +1){
			if(UI_Container.pages.length == pageI+1){
				exitContainer = true;
				pageI = 0;
				eleI = 0;
			}else{
				pageI = pageI+1;
				eleI = 0;
			}
		}else{
			eleI = eleI+1;
		}
		return {exitContainer:exitContainer,page:pageI,element:eleI};
	}
	
	function findPrevPage(){
		var pageI = UI_Container.focusedPageIndex;
		var eleI = UI_Container.focusedElementIndex;
		var exitContainer = false;
		if(eleI == 0){
			if(pageI == 0){
				exitContainer = true;
				pageI = 0;
				eleI = 0;
			}else{
				pageI = pageI-1;
				eleI = UI_Container.pages[pageI].length-1;
			}
		}else{
			eleI = eleI-1;
		}
		return {exitContainer:exitContainer,page:pageI,element:eleI};
	}
	
	function fareQuoteFlexiTotalCharge(){
		var fqTotalFlexiCharge = 0;
		if(UI_Anci.flexiEnableInAnci){
			if(UI_Container.fareQuoteOndFlexiCharges != null){
				$.each(UI_Container.fareQuoteOndFlexiCharges, function(ondSeq, ondFlexiCharge){
					if(ondFlexiCharge != null && ondFlexiCharge != ""){					
						fqTotalFlexiCharge += parseFloat(ondFlexiCharge);
					}
				});
			}
		}
		return fqTotalFlexiCharge;
	}

    UI_Container.loadTrackingOnPax = function(){
        var from = $("#resFromAirport").val();
        var to= $("#resToAirport").val();
        var departureDate=$("#resDepartureDate").val();
        var returnDate=$("#resReturnDate").val();
        var fareQuoteDetails = JSON.parse($("#resFareQuote").val());
        var fareTotal= fareQuoteDetails.totalFare;

        var sendParam="pageID=PAX&rad="+UI_commonSystem.getRandomNumber()+"&src="+from+"&dest="+to+
            "&dt="+departureDate+"&lan="+SYS_IBECommonParam.locale.toUpperCase()+"&price="+fareTotal +"&returnDate="+returnDate;
        $("#frmTracking").attr("src", "showLoadPage!loadTrackingPage.action?"+sendParam);

    }
	
})(UI_Container);

$(document).ready(function() {
	UI_commonSystem.loadingProgress();	
	UI_Container.ready();

});