/** Kiosk Confirm
 * 
 * @author Pradeep Karunanayake
 *  
 */

function UI_KioskConfirm(){};
	
;(function(UI_KioskConfirm) {
	
	$(document).ready(function() {
		UI_KioskConfirm.displayPage();
		$("#divLoadBg").hide();
		$("#pageLoading").hide();
		$("#divLoadMsg").show();		
		UI_KioskConfirm.ready();		
	});
	
	UI_KioskConfirm.ready = function() {
		$("#linkHome").click(function(){ SYS_IBECommonParam.homeClick() });
		UI_KioskConfirm.setPageData();		
	}
	
	UI_KioskConfirm.setPageData = function() {
		var onHoldData = strConfData;
		$("#resNo").text(onHoldData.pnr);
		$("#time").text(onHoldData.releaseTime);
		$("#divLoadMsg").hide();
		$("#divLoadBg").show();
		$('body').append('<div id="divSessContainer"></div>');
		UI_commonSystem.initSessionTimeout('divSessContainer',timeOut,function(){});
	}
	
	UI_KioskConfirm.displayPage = function() {
		UI_Top.holder().UI_commonSystem.loadingCompleted();	
		$("#divLoadBg", parent.document).hide();	
		$("#cardInputs", parent.document).attr("height", "100%");
		$("#cardInputs", parent.document).attr("width", "100%");
		$("#cardInputs", parent.document).css("top", "0px");
		$("#cardInputs", parent.document).css("left", "0px");
	}
	
})(UI_KioskConfirm);