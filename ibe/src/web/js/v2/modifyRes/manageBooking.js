/**
 * Manage Booking functionality
 * Author : Pradeep Karunanayake
 * 
 */	
function UI_ManageBooking(){}

UI_ManageBooking.isElink = false;
UI_ManageBooking.errorCode = "";
UI_ManageBooking.isPageLoad = false;
UI_ManageBooking.isFromPromotionRegistration = false;

/**
 * Document Ready
 */
$(document).ready(function(){
	
	if(httpsEnabled == "true"){
	  UI_ManageBooking.reSubmitHttp(UI_commonSystem.checkHttps());	
	}
	$("#divLoadBg").hide();
	$("#btnPrevious").click(function(){SYS_IBECommonParam.homeClick();});
	$("#btnRefresh").click(function(){UI_ManageBooking.pageBtnOnClick(1) });
	$("#btnContinue").click(function(){UI_ManageBooking.pageBtnOnClick(0) });
	$("#imgCPT").click(function(){UI_ManageBooking.pageBtnOnClick(1) });
	$("#txtDateOfDep").blur(function(event){UI_ManageBooking.dateOfDepBlur(this) });	
	$("#imgCalendar").hide();
	$("#txtDateOfDep").datepicker({
		regional:SYS_IBECommonParam.locale,
		dateFormat: "dd/mm/yy",		
		showOn: 'both',
		buttonImage: globalConfig.calendaImagePath,
		buttonImageOnly: true,
		showButtonPanel: true
	});
	
	if(promoPnr != null && promoPnr != "" ){
		UI_ManageBooking.isFromPromotionRegistration = true;
		$("#pnr").val(promoPnr);
		$("#txtLastName").val(promoLastName);
		$("#txtDateOfDep").val(promoDepDate);
		UI_ManageBooking.loadPageData();
	}
	
	
	if (UI_ManageBooking.checkIsElink()) {
		UI_ManageBooking.isElink = true;
		UI_ManageBooking.loadExternalLink();
	} else {
		UI_ManageBooking.loadPageData();
	}
	try {
		if(strAnalyticEnable == "true") {	
			//pageTracker._trackPageview("/ManageBooking");
			_gaq.push(['_trackPageview', "/ManageBooking" ]);
		}
	} catch(ex) {}
});
/**
 * Validate Booking Details
 */
UI_ManageBooking.validateBooking = function() {
	$("#preDateOfDep").val($("#txtDateOfDep").val());
	 $("#dateOfDep").val(dateToGregorian($("#txtDateOfDep").val(),SYS_IBECommonParam.locale));
	 $("#txtDateOfDep").val($("#dateOfDep").val());
	 $("#frmManageBooking").attr('action', SYS_IBECommonParam.securePath + 'manageBookingV2.action');
	 $("#frmManageBooking").ajaxSubmit({dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress, success: UI_ManageBooking.validateBookingDataProcess, error:UI_commonSystem.setErrorStatus});	
	return false;
}
/**
 * Process Request data
 */
UI_ManageBooking.validateBookingDataProcess = function(response) {	
	if(response != null && response.success == true) {
		var message = response.messageTxt;
		if (message != null && message != "") {			
			if (UI_ManageBooking.isElink) {
				UI_ManageBooking.errorCode = message;
				UI_ManageBooking.loadPageData();				
			} else {
				UI_ManageBooking.setPageError(message);	
				if($("#preDateOfDep").val() != null || $("#preDateOfDep").val() != "") {
					$("#txtDateOfDep").val($("#preDateOfDep").val());
				}
			}			
		} else {
			UI_commonSystem.loadingProgress();
			$("#groupPNR").val(response.groupPNR);
			$("#airlineCode").val(response.airlineCode);
			$("#marketingAirlineCode").val(response.marketingAirlineCode);
			$("#frmManageBooking").attr('action','showCustomerLoadPage!loadCustomerHomePage.action');
			$("#frmManageBooking").submit();			
		}
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	
	UI_commonSystem.loadingCompleted();
}
/**
 * Load Page Data
 */
UI_ManageBooking.loadPageData = function() {
	if (UI_ManageBooking.isPageLoad == false) {
		$("#submitForm").ajaxSubmit({url:"manageBookingDetailV2.action", dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success:  UI_ManageBooking.loadPageDataProcess, error:UI_commonSystem.setErrorStatus});
	}
}
/**
 * Process Request Data
 */
UI_ManageBooking.loadPageDataProcess = function(response) {	 
	if(response != null && response.success == true) {
		if (UI_ManageBooking.isFromPromotionRegistration == false){
		$("#divLoadBg").show();
		}
		$("#PgManageBooking").populateLanguage({messageList:response.jsonLabel});
		UI_ManageBooking.clBgColor = response.jsonLabel.CalendarBorderColour;
		UI_ManageBooking.strMngPnr = response.jsonLabel.msgPnr;
		UI_ManageBooking.strMngLastname = response.jsonLabel.msgLastName;
		UI_ManageBooking.strMngDepDate= response.jsonLabel.msgDepatureDate;
		UI_ManageBooking.strMngDepDateValid= response.jsonLabel.msgValidDepatureDate;
		UI_ManageBooking.strMngImg= response.jsonLabel.msgWordInImage;
		UI_ManageBooking.strMngInvalidCombin= response.jsonLabel.msgInvalidCombination;
		UI_ManageBooking.isCaptchaEnabled = response.imageCapchaEnable;
		if (response.imageCapchaEnable == true) {
			$("#trPanelImageCaptcha").show();
			$("#btnRefresh").show();
		} else {
			$("#trPanelImageCaptcha").hide();
			$("#btnRefresh").hide();
		}
		if (response.allowLoadResUsingPaxLastName == true) {
			$("#contactOrPaxLastName").show();
			$("#contactLastName").hide();
		} else {
			$("#contactOrPaxLastName").hide();
			$("#contactLastName").show();
		}
		if (UI_ManageBooking.isElink) {
			UI_ManageBooking.setPageError(UI_ManageBooking.errorCode);
		}
		UI_ManageBooking.isPageLoad = true;
		
		if(UI_ManageBooking.isFromPromotionRegistration){
			UI_ManageBooking.pageBtnOnClick(0);
		}
		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

/**
 * Page Button Click
 */
UI_ManageBooking.pageBtnOnClick = function(intID){
	
	switch (intID){
		case 0 : 

			strErrorHTML = "";	
			if($("#pnr").val() == null || $("#pnr").val() == "") {
				strErrorHTML += "<li> " +  UI_ManageBooking.strMngPnr;
			}

			if($("#txtLastName").val() == null || $("#txtLastName").val() == "") {
				strErrorHTML += "<li> " +  UI_ManageBooking.strMngLastname;
			}

			if($("#txtDateOfDep").val() == null || $("#txtDateOfDep").val() == "") {
				strErrorHTML += "<li> " +  UI_ManageBooking.strMngDepDate;
			} else if(SYS_IBECommonParam.locale == "fa"){
				if(!persianDateValidDate($("#txtDateOfDep").val())){
					strErrorHTML += "<li> " +  UI_ManageBooking.strMngDepDateValid;
				}
			} else { 
				if(!dateValidDate($("#txtDateOfDep").val())){
					strErrorHTML += "<li> " +  UI_ManageBooking.strMngDepDateValid;
				}
			}

			if(UI_ManageBooking.isCaptchaEnabled && ($("#txtCaptcha").val() == null || $("#txtCaptcha").val() == "")) {
				strErrorHTML += "<li> " +  UI_ManageBooking.strMngImg;
			}
			
			if(strErrorHTML != ""){
				$("#spnError").html("<font class='mandatory'>" + strErrorHTML + "<\/font>");
			} else {
				$("#pnr").val($.trim($("#pnr").val()));
				$("#txtLastName").val($.trim($("#txtLastName").val()));
				UI_ManageBooking.validateBooking();				
			}

			break;
			
		case 1 : 	
			document ['imgCPT'].src = '../jcaptcha.jpg?relversion=' + (new Date()).getTime();

	}
}
/**
 * Date Of Departure Blur
 */
UI_ManageBooking.dateOfDepBlur = function(objControl){
		if (objControl.value != ""){
			var strReturn = dateChk(objControl.value)
			if (strReturn){
				objControl.value = strReturn;
			}			
		}
}


UI_ManageBooking.loadExternalLink = function() {	
		$('#pnr').val(pnr);
		$('#txtLastName').val(lname);
		var stringDateArr = (ddate).split("/");
		$("#txtDateOfDep").datepicker( "setDate" , new Date(stringDateArr[2], (stringDateArr[1] -1),stringDateArr[0]));		
		$("#frmManageBooking").attr('action', SYS_IBECommonParam.securePath + 'manageBookingV2.action?link='+linkType);
		$("#frmManageBooking").ajaxSubmit({dataType: 'json',beforeSubmit:UI_commonSystem.loadingProgress, success: UI_ManageBooking.validateBookingDataProcess});			
		return false;			
}

UI_ManageBooking.reSubmitHttp = function(isHttps) {
	if (isHttps == false) {		
		var strSubmitUrl = SYS_IBECommonParam.securePath + 'showReservation.action?hdnParamData='+SYS_IBECommonParam.locale +'^MAN';
		location.replace(strSubmitUrl);
		UI_commonSystem.loadingProgress();
	}
}

UI_ManageBooking.checkIsElink = function() {
	var isElink = false;
	if(linkType != null && linkType == 'ELINK') {
		isElink = true;
	}
	return isElink;		
}

UI_ManageBooking.setPageError = function(error) {
	var msg = "";
	if (error == "invalidCombination") {
		msg = UI_ManageBooking.strMngInvalidCombin;
	} else {
		msg = error;
	}
	if(msg != "") {
		$("#spnError").html(msg);
	}
	UI_ManageBooking.pageBtnOnClick(1);
}




