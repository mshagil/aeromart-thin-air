/**
 * 
 * Author : Pradeep Karunanayake
 */
function UI_CustomerModifySegment(){}

UI_CustomerModifySegment.details= {PNR:"",
								   isGroupPNR:false,
								   version:"",
								   segmentRefNos:"",
								   allResSegments:"",
								   modifingFlightInfo:"",
								   from:"",
								   to:"",
								   depDate:"",
								   adultCount:"",
								   childCount:"",
								   infantsCount:"",
								   classOfService:"",
								   paxJson: "",
								   fareDetails:[],
								   firstDepature: "",
								   lastArrival:"",
								   hasInsurance:false,
								   depGroundStation:false,
								   arrGroundStation:false};
//segment modification IBE
UI_CustomerModifySegment.allowModifyByDate = "";
UI_CustomerModifySegment.allowModifyByRoute = "";
/**
 * Page Ready 
 */
UI_CustomerModifySegment.ready = function() {
	$("#btnBackModifySegment").click(function(){ UI_FlightSearch.previousAirportData.isOnADataRefresh = true;UI_FlightSearch.resetSearhData(); UI_UserTabs.pnrClick();});
	if (UI_CustReservation.isRequoteFlow && !UI_CustReservation.isRequoteCancel){
		//$("#modifySegment").hide();
	}
	UI_CustomerModifySegment.pageBuild();
	UI_CustomerModifySegment.loadModifySegmentPageData();
}

/**
 * Set OlD Fare Details
 */
UI_CustomerModifySegment.setOldFareDetails = function() {
	UI_CustomerModifySegment.getSegmentFareDetails();
}

/**
 * Get Segment Fare Details
 */
UI_CustomerModifySegment.getSegmentFareDetails = function() {
	var  fareDetails = UI_CustReservation.fareDetails;
	var modifyDetails = UI_CustomerModifySegment.details;
	var segRefNumber = modifyDetails.segmentRefNos.split(":")[0];		
	if (fareDetails != null && segRefNumber != "") {		
		for (var i = 0; i < fareDetails.length; i++) {			
			if (segRefNumber == fareDetails[i].flightRefNumber) {
				modifyDetails.fareDetails = fareDetails[i];			
				break;
			}
		}
	}
	
}

/**
 * Modify Segment Page build Using Existing Data
 */
UI_CustomerModifySegment.pageBuild = function() {
	UI_CustomerModifySegment.setModifyInitData();	
	$("#spnPNR").text(UI_CustReservation.pnr); 
	var flightData = UI_CustomerModifySegment.getSelectedFlight(UI_CustReservation.flightRefNos);
	UI_CustomerModifySegment.details.modifingFlightInfo = flightData;
	UI_CustomerModifySegment.setSegmentDetails(flightData);
	UI_CustomerModifySegment.setGroundSegmentData();
	UI_FlightSearch.setModifySegmentSearchData(UI_CustomerModifySegment.details, UI_CustReservation.resFlexiAlerts,UI_CustomerModifySegment.allowModifyByDate,UI_CustomerModifySegment.allowModifyByRoute);
	$("#departueFlightMS").nextAll().remove();
	$("#departueFlightMS").iterateTemplete({templeteName:"departueFlightMS", data:flightData, dtoName:"flightDepDetails"});
	$("#btnSearchModifySegment").unbind('click').bind("click", function(){ UI_FlightSearch.flightSearchBtnOnClick(0);})
	UI_commonSystem.loadingCompleted();
	dateChangerjobforLanguage(SYS_IBECommonParam.locale);
	UI_FlightSearch.setDummyService();
	//alert("111")
}

UI_CustomerModifySegment.setModifyInitData = function() {
	var modifyData = UI_CustomerModifySegment.details;
	modifyData.PNR = UI_CustReservation.pnr;
	var isGruupPnr = false;
	if (UI_CustomerHome.groupPnr == "true") {
		isGruupPnr = true;
	} else if (UI_CustomerHome.groupPnr == "false") {
		isGruupPnr = false;
	} else {
		isGruupPnr = $.trim(UI_CustomerHome.groupPnr) != "" ?true:false;
	}
	modifyData.isGroupPNR = isGruupPnr;
	modifyData.version = UI_CustReservation.bookingInfo.version;
	modifyData.segmentRefNos = UI_CustReservation.flightRefNos;
	modifyData.allResSegments = UI_CustReservation.allReservationSegments;			  
	modifyData.adultCount = UI_CustReservation.bookingInfo.totalPaxAdultCount;
	modifyData.childCount = UI_CustReservation.bookingInfo.totalPaxChildCount;
	modifyData.infantsCount = UI_CustReservation.bookingInfo.totalPaxInfantCount;		 
	modifyData.paxJson = $.toJSON(UI_CustReservation.getPaxForAncillary());		   
  	modifyData.firstDepature = UI_CustReservation.firstDepature;
    modifyData.lastArrival= UI_CustReservation.lastArrival;
    modifyData.hasInsurance= UI_CustReservation.hasInsurance;
    modifyData.bookingStatus =UI_CustReservation.bookingInfo.status ;
    UI_CustomerModifySegment.setOldFareDetails();
}

/**
 * Set Segment Details
 */
UI_CustomerModifySegment.setSegmentDetails = function(segmentflightsInfo) {	
	if (segmentflightsInfo != null && segmentflightsInfo != "") {
		if (segmentflightsInfo.length >= 1) {
			UI_CustomerModifySegment.details.from = segmentflightsInfo[0].segmentShortCode.split('/')[0];
			// UI_CustomerModifySegment.details.depDate = new Date(segmentflightsInfo[0].departureDateLong);	
		 
			if(segmentflightsInfo[0].surfaceStation != null) {
				UI_CustomerModifySegment.details.depGroundStation = true;
			}else{
				UI_CustomerModifySegment.details.depGroundStation = false;
			}
			UI_CustomerModifySegment.details.depDate= segmentflightsInfo[0].departureDisplayDate;	
			if (segmentflightsInfo.length == 1) {
				// Multi Leg flight segments
				var segmentCodes = (segmentflightsInfo[0].segmentShortCode).split("/");
				UI_CustomerModifySegment.details.to = segmentCodes[segmentCodes.length -1];				
			} else {
				UI_CustomerModifySegment.details.to = segmentflightsInfo[segmentflightsInfo.length -1].segmentShortCode.split('/')[1];	
			}
			UI_CustomerModifySegment.details.classOfService = segmentflightsInfo[segmentflightsInfo.length -1].cabinClass;
			if(segmentflightsInfo[segmentflightsInfo.length -1].surfaceStation != null) {
				//UI_CustomerModifySegment.details.arrGroundStation = true;
			}
		}		
	} else {
		alert("Error in segment details")
	}
}

/**
 * Get Selected Segments
 */
UI_CustomerModifySegment.getSelectedFlight = function(flightRefNos) {
	var segments = [];	
	var flightSegmnts = UI_CustReservation.flightDepDetails.concat(UI_CustReservation.flightRetDetails);
	var segArray = flightRefNos.split(":");		
	for(var i = 0; i < segArray.length; i++) {		
			 if (flightSegmnts != null && flightSegmnts != "") {		 
				 for (var f = 0; f < flightSegmnts.length; f++) {							 
					 if(flightSegmnts[f].flightSegmentRefNumber == segArray[i]) {
						 segments[i] =  flightSegmnts[f];						 
						 UI_CustomerModifySegment.allowModifyByDate = flightSegmnts[f].isAllowModifyByDate;
						 UI_CustomerModifySegment.allowModifyByRoute = flightSegmnts[f].isAllowModifyByRoute;						 
					 }
				 }			 
		 }	 
	 }	 
	 return segments;	
}

/**
* Request Page Data
*/

UI_CustomerModifySegment.loadModifySegmentPageData = function() {
	var modifyData = UI_CustomerModifySegment.details;
	var data = {};	
	data['segmentRefNo'] = modifyData.segmentRefNos;
	data['oldAllSegments'] = $.toJSON(modifyData.allResSegments);		
	if(UI_CustReservation.isRequoteFlow && UI_CustReservation.isRequoteCancel){
		UI_CustomerModifySegment.loadCancelSegmentRequoteData();
	}else if(UI_CustReservation.isRequoteFlow && UI_CustReservation.isRequoteNameChange){
		UI_CustomerModifySegment.loadNameChangeRequoteData();
	}else{
		$("#submitForm").ajaxSubmit({url:"modifySegment.action", data:data, dataType: 'json', beforeSubmit:UI_commonSystem.loadingProgress, success: UI_CustomerModifySegment.loadModifySegmentPageDataProcess, error:UI_commonSystem.setErrorStatus});	
	}
	return false;
} 
/**
 * Load Data Success
 */

UI_CustomerModifySegment.loadModifySegmentPageDataProcess =  function(response) {	
	if(response != null && response.success == true) {
		$("#modifySegment").populateLanguage({messageList:response.jsonLabel});		
		if(response.fromAirportData != null && response.fromAirportData != "" &&
				response.toAirportData != null && response.toAirportData != ""){
			UI_FlightSearch.fillFromToAirportDD(response.fromAirportData, response.toAirportData);
		}
		
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

UI_CustomerModifySegment.setGroundSegmentData = function() {
	if(!UI_CustomerModifySegment.details.depGroundStation){
		UI_FlightSearch.previousAirportData.isOnADataRefresh = true;
		UI_FlightSearch.resetPreviousDataOnDropDowns();
	}else {
		if (UI_FlightSearch.previousAirportData.toAirportGroundOnlyData != null
				&& UI_FlightSearch.previousAirportData.fromAirportGroundOnlyData != null)
			UI_FlightSearch.fillFromToAirportDD(
					UI_FlightSearch.previousAirportData.fromAirportGroundOnlyData,
					UI_FlightSearch.previousAirportData.toAirportGroundOnlyData);
	}
}

UI_CustomerModifySegment.loadCancelSegmentRequoteData = function() {
	$('#selCurrency').val(UI_CustReservation.fareQuote.inSelectedCurr.currency);
	$("#resQuoteOutboundFlexi").val(false);
	$("#resQuoteInboundFlexi").val(false);
	$("#resFlexiSelected").val(false);
	$("#requoteFlightSearch").val(true);
	$("#cancelSegmentRequote").val(true);	
	$("#frmFlightSearch").attr('action','showLoadPage!loadFlightSearch.action');
	$("#frmFlightSearch").submit();	
}

UI_CustomerModifySegment.loadNameChangeRequoteData = function() {
	$('#selCurrency').val(UI_CustReservation.fareQuote.inSelectedCurr.currency);
	$("#resQuoteOutboundFlexi").val(false);
	$("#resQuoteInboundFlexi").val(false);
	$("#resFlexiSelected").val(false);
	$("#requoteFlightSearch").val(true);
	$("#nameChangeRequote").val(true);
	$("#cancelSegmentRequote").val(false);
	$("#frmFlightSearch").attr('action','showLoadPage!loadFlightSearch.action');
	$("#frmFlightSearch").submit();	
}

/**
 * Onload Page Ready
 */
UI_CustomerModifySegment.ready();