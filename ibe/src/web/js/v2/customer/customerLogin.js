/**
 * Customer Login functionality
 * Author : Pradeep Karunanayake
 * 
 */	

function UI_CustomerLogin(){}
// hold Error messages
UI_CustomerLogin.errorMsgs = "";
UI_CustomerLogin.strMsgUID = "";
UI_CustomerLogin.strMsgPWD = "";

$(document).ready(function(){
	UI_CustomerLogin.ready();
});

/**
 * Page Ready
 */
UI_CustomerLogin.ready = function() {
	
	if(httpsEnabled == "true"){
		UI_CustomerLogin.reSubmitHttp(UI_commonSystem.checkHttps());
	}	
	$("#divLoadBg").hide();	
	$("#btnSignIn").click(function(){UI_CustomerLogin.signInClick();});
	$("#btnforgetPwd").click(function(){UI_CustomerLogin.forgetPasswordClick();});

	UI_CustomerLogin.loadLoginData();

}

/**
 * Request login data
 * 
 */
UI_CustomerLogin.loadLoginData = function() {
	 UI_commonSystem.loadingProgress();
	 $("#frmLogin").attr('action', SYS_IBECommonParam.securePath + 'showCustomerLoginDetail.action');
	 $("#frmLogin").ajaxSubmit({dataType: 'json', success: UI_CustomerLogin.loadLoginDataSucess, error:UI_commonSystem.setErrorStatus});	
	return false;
}
/**
 * Request data process 
 *
 */
UI_CustomerLogin.loadLoginDataSucess = function(response) {
	if(response != null && response.success == true) {
		$("#divLoadBg").show();
		$("#frmLoadMsg").hide();
		UI_commonSystem.loadingCompleted();
		// Set Screen labels
		$("#PgLogin").populateLanguage({messageList:response.jsonLabel});
		if (!globalConfig.layOut.newRegUser){
			$("#sortable-ul .sortable-child:first").hide();
			$(".sortable-child:last").css("width","100%");
			$(".rightColumn").css("width","98%");
			$("#btnRegisgter").show();
			$("#btnRegisgter").click(function(){UI_CustomerLogin.registerClick();});
			$("#msgRegister").html(response.jsonLabel.msgRegister);
			$("#msgRegister1").html(response.jsonLabel.msgRegister1);
		}else{
			$("#msgRegister").html(response.jsonLabel.lblInfoAgree1);
			if(response.customer != null){
				$("#lblInfoAgree1").show();
			}
			$("#msgRegister1").html(response.jsonLabel.lblInfoAgree2);
			$("#btnRegisgter").hide();
		}
		$("#lblInfoAgree3").html(response.jsonLabel.lblInfoAgree3).append(response.jsonLabel.lblInfoAgree4);
		UI_CustomerLogin.policyLinkSetup();
		UI_CustomerLogin.strMsgUID  = response.jsonLabel.lblName;
		UI_CustomerLogin.strMsgPWD  = response.jsonLabel.lblPassword;
		UI_CustomerLogin.errorMsgs = response.errorInfo;
		
		if(!response.loyaltyManagmentEnabled){
			$("#lmsIntegrates").hide();
			$("#RewardsPanel-1").hide();
		}
		if(!response.loyaltyEnable){
			$("#RewardsPanel-2").hide();
		}
		
		UI_SocialCustomer.initLogin(response);
        $(".aiRewardsLink").off("click").on("click", function(){
            UI_commonSystem.openWindowPopup('11');
        });
		// Google Analytics
		try {
			if(strAnalyticEnable == "true") {	
				//pageTracker._trackPageview("/LoginCustomer");
				_gaq.push(['_trackPageview', "/LoginCustomer" ]);
			}
		} catch(ex) {}	
	} else {
		UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
	}
	UI_commonSystem.loadingCompleted();
}

/**
 * Policy Link Set up
 */
UI_CustomerLogin.policyLinkSetup = function() {
	$("#linkPolicy").attr("href", SYS_IBECommonParam.homeURL + "/privacy-policy.html");
}

/**
 * Sign In Click
 */
UI_CustomerLogin.signInClick = function() {		
	if(!UI_CustomerLogin.loginValidation()) 
		return;	
	UI_commonSystem.loadingProgress()
	$("#frmLogin").attr('action', SYS_IBECommonParam.securePath + 'customerLogin.action');
	$("#frmLogin").ajaxSubmit({dataType: 'json',beforeSubmit:UI_CustomerLogin.progressSetup, success: UI_CommonCustomer.loadCustomerLoginSucess, error:UI_commonSystem.setErrorStatus});	
	return false;	
}

/**
 * Register Click
 */
UI_CustomerLogin.registerClick = function() {
	var strSubmitUrl = SYS_IBECommonParam.securePath + 'showReservation.action?hdnParamData='+SYS_IBECommonParam.locale +'^RE';
	location.replace(strSubmitUrl);
	//UI_commonSystem.loadingProgress();
}

/**
 * Login Validation
 */	
UI_CustomerLogin.loginValidation = 	function (){
		if ($("#txtUID").val() == ""){
			jAlert(UI_CustomerLogin.errorMsgs.userIdRqrd);
			$("#txtUID").focus();
			return false;
		}
		
		if ($("#txtPWD").val() == ""){
			jAlert(UI_CustomerLogin.errorMsgs.userPasswordRqrd);
			$("#txtPWD").focus();
			return false
		}
		
		var strChkEmpty = FindChar($("#txtUID").val())
		if (strChkEmpty != "0"){
			jAlert(buildError(UI_CustomerLogin.errorMsgs.invalidChar, strChkEmpty[0], UI_CustomerLogin.strMsgUID));
			$("#txtUID").focus();
			return false
		}
		
		strChkEmpty = FindChar($("#txtPWD").val())
		if (strChkEmpty != "0"){
			jAlert(buildError(UI_CustomerLogin.errorMsgs.invalidChar, strChkEmpty[0], UI_CustomerLogin.strMsgPWD));
			$("#txtPWD").focus();
			return false
		}
		return true;
}
/**
 * Forgot Password Click
 */
UI_CustomerLogin.forgetPasswordClick = function() {	
		var url = "showCustomerLoadPage!loadCustomerForgotPWD.action";
		var inHeight = 300;		
		if ((UI_Top.holder().objCWindow) && (!UI_Top.holder().objCWindow.closed)){UI_Top.holder().objCWindow.close();}
		UI_Top.holder().objCWindow =  window.open(url, "popWindow", $("#popWindow").getPopWindowProp(300, 565));		
}

UI_CustomerLogin.reSubmitHttp = function(isHttps) {
	if (isHttps == false) {		
		var strSubmitUrl = SYS_IBECommonParam.securePath + 'showReservation.action?hdnParamData='+SYS_IBECommonParam.locale +'^SI';
		location.replace(strSubmitUrl);
	}
}

