function UI_PromotionRegister() {};

UI_PromotionRegister.colAdultCollection = "";
UI_PromotionRegister.colChildCollection = "";

UI_PromotionRegister.outboundSegmentIds = "";
UI_PromotionRegister.inboundSegmentIds = "";

UI_PromotionRegister.pnr = "";
UI_PromotionRegister.contactLastName = "";
UI_PromotionRegister.outboundDepartureDate = "";
UI_PromotionRegister.inboundDepartureDate = ""

UI_PromotionRegister.flexiTravelReferenceNumbers="";
UI_PromotionRegister.flexiLowerboundToPersist;
UI_PromotionRegister.flexiUpperboundToPersis;
UI_PromotionRegister.flexiSelectedSegmentId="";
UI_PromotionRegister.flexiSubscribedInCurrentSession = false;
UI_PromotionRegister.selectedInOutBoundCode="outbound";
UI_PromotionRegister.promotypeText = "";
UI_PromotionRegister.refundText = "";
UI_PromotionRegister.condetion2 = "";
UI_PromotionRegister.jsonLabel = null;

var tourRoute = [];
var paxCollection = [];


$(document).ready(function() {
	UI_PromotionRegister.loadReservationData();

	$("#btnContinue").click(function() {
		UI_PromotionRegister.savePromotionRequests();
	});
});

/**
 * Load promotion display data
 */
UI_PromotionRegister.loadReservationData = function() {

	$.ajax({
		url : "promotionRegisteration.action?promReqAttr=" + reqString,
		beforeSend : UI_commonSystem.loadingProgress(),
		data : $.extend({}, {}),
		dataType : "json",
		success : UI_PromotionRegister.loadReservationProcess,
		error : function(response) {
			UI_commonSystem.setErrorStatus(response);
		},
		complete : function(response) {
			// UI_commonSystem.loadingCompleted();
		}

	});

}

/**
 * Load promotion display data success
 */
UI_PromotionRegister.loadReservationProcess = function(response) {
	if (response != null && response.success == true && response.duplicates == false && response.unsubscribe == false) {
		UI_PromotionRegister.jsonLabel = response.jsonLabel;
		UI_PromotionRegister.promoTypeId = response.promotionRequestTo.promoTypeId;
		UI_PromotionRegister.promotionId = response.promotionRequestTo.promotionId;
		UI_PromotionRegister.colAdultCollection = response.colAdultCollection;
		UI_PromotionRegister.colChildCollection = response.colChildCollection;
		UI_PromotionRegister.promotionRequestTo = response.promotionRequestTo;
		UI_PromotionRegister.promoFares = response.promoFares;
		UI_PromotionRegister.inboundSegmentCode = response.inboundSegmentCode;
		UI_PromotionRegister.outboundSegmentCode = response.outboundSegmentCode;
		UI_PromotionRegister.outboundSegDisplayName = response.outboundSegDisplayName;
		UI_PromotionRegister.inboundSegDisplayName = response.inboundSegDisplayName;
		UI_PromotionRegister.outboundSegmentIds = response.outboundSegmentIds;
		UI_PromotionRegister.inboundSegmentIds = response.inboundSegmentIds;
		UI_PromotionRegister.flexiParams = response.promotionRequestTo.promotionRequestParams;
		UI_PromotionRegister.pnr = response.promotionRequestTo.pnr;
		UI_PromotionRegister.contactLastName = response.contactLastName;
		UI_PromotionRegister.outboundDepartureDate = response.outboundDepartureDate;
		UI_PromotionRegister.inboundDepartureDate = response.inboundDepartureDate;
		function segmentComparator(first ,second){
			var parser  = function(d){
				var a = d.split('T');
				var b = a[0].split('-');
				var c = a[1].split(':');
				return new Date(b[0],parseInt(b[1],10)-1,b[2],c[0],c[1],c[2]);
			}
			var d1 = parser(first.flightSegmentTO.departureDateTime);
			var d2 = parser(second.flightSegmentTO.departureDateTime);
			return (d1.getTime()-d2.getTime());
		}
		

		var composerFn = function(paxArr) {
			var retPaxArr = [];
			for ( var i = 0; i < paxArr.length; i++) {
				var pax = {};
				pax['title'] = '';
				pax['firstName'] = paxArr[i].itnPaxName;
				pax['lastName'] = '';
				pax['seqNumber'] = paxArr[i].actualSeqNo;
				pax['dateOfBirth'] = paxArr[i].dateOfBirth;
				pax['foidNumber'] = '';
				pax['foidType'] = '';
				pax['nationality'] = '';
				pax['travelerRefNumber'] = paxArr[i].travelerRefNumber;
				pax['itnSeat'] = paxArr[i].itnSeat;
				if( i>0 ){
					UI_PromotionRegister.flexiTravelReferenceNumbers += ",";	
				}
				UI_PromotionRegister.flexiTravelReferenceNumbers += paxArr[i].travelerRefNumber;
				
				var arr = pax['travelerRefNumber'].split('$')[0].split('/');
				if (arr.length > 1) {
					pax['travelWith'] = arr[1].split('')[1];
				}
			//	pax['currentAncillaries']=$.airutil.sort.quickSort(paxArr[i].selectedAncillaries, segmentComparator);
				retPaxArr[retPaxArr.length] = pax;
			}
			return retPaxArr;
		}
		
		if (response.promotionRequestTo.pnr != undefined) {
			$("#pnr").text(response.promotionRequestTo.pnr);
			$("#contactName").text(response.promotionRequestTo.resContactName);
		}

		
		/**
		 * Build Pax Collection
		 */
		if (UI_PromotionRegister.colAdultCollection.length > 0) {
			$.each(UI_PromotionRegister.colAdultCollection, function() {
				paxCollection[paxCollection.length] = this;
			});
		}

		if (UI_PromotionRegister.colChildCollection.length > 0) {
			$.each(UI_PromotionRegister.colChildCollection, function() {
				paxCollection[paxCollection.length] = this;
			});
		}
		composerFn(paxCollection);
				
		var setLabel = function(typ) {
			var txtTemp = ""
			if (typ == 1) {
				txtTemp =  UI_PromotionRegister.jsonLabel.lblDoubleSeaDescriptionText;
				UI_PromotionRegister.promotypeText = UI_PromotionRegister.jsonLabel.lblDoubleSeatHeading;
				UI_PromotionRegister.refundText = UI_PromotionRegister.jsonLabel.termsPromoDblSeat1;
				UI_PromotionRegister.condetion2 = UI_PromotionRegister.jsonLabel.termsPromoDblSeat2;
			} else {
				txtTemp = UI_PromotionRegister.jsonLabel.lblFlexiDescriptionText;
				UI_PromotionRegister.promotypeText = UI_PromotionRegister.jsonLabel.lblFlexiHeading;
				UI_PromotionRegister.refundText = UI_PromotionRegister.jsonLabel.termsPromoDblSeat1;
				UI_PromotionRegister.condetion2 = UI_PromotionRegister.jsonLabel.termsPromoDblSeat2;
			}
			$("#termsOFPromo").html(UI_PromotionRegister.refundText);
			$("#termsForMoveSeat").html(UI_PromotionRegister.condetion2);
			$("#lblPromoHeading").html(UI_PromotionRegister.promotypeText);
			$("#lblPromoText").html(txtTemp);
		}
		
		/**
		 * Promotion Main builder
		 */
		var promotionHTMLGEN = function(type, paxCollection) {
			
			/**
			 * Promotion Builder common Child process
			 */
			var buildPromoHtml = function(type, segInd, paxInd) {
				
				if (segInd == null)
					segInd = 0;
				if (paxInd == null)
					paxInd = 0;
				
				/**
				 * Promotion builder child process : Next Seat Free
				 */
				if (type ==1) {
					selectText = " Empty Seat";
					selPormo = $("<select><option value='-'>None</option></select>").attr("id", "selPromo_" + segInd + "_" + paxInd);
					
					if (UI_PromotionRegister.promotionRequestTo.promotionRequestParams != undefined && UI_PromotionRegister.promotionRequestTo.promotionRequestParams != null) {
						//key.split("_")
						var nonRefCharge =[];
						$.each(UI_PromotionRegister.promotionRequestTo.promotionRequestParams, function(key, val) {
							var t = {};
							if(key.split("_")[0] == 'NF'){
								t.nonRefkey = key.split("_")[1];
								t.nonRefval = val;
								nonRefCharge[nonRefCharge.length] = t;
							}
						});
						
						$.each(UI_PromotionRegister.promotionRequestTo.promotionRequestParams, function(key, val) {
							var nonRefund;
							for ( var k = 0; k < nonRefCharge.length; k++) {
								if (key == nonRefCharge[k].nonRefkey){
									nonRefund = nonRefCharge[k].nonRefval;
									break;
								}
							}
							var valueToDisplay;
							valueToDisplay = parseFloat(val) + parseFloat(nonRefund);
							if (key.indexOf("_") <= -1){
								var topt = $("<option>" + (key + selectText) + "</option>").attr("value", parseFloat(valueToDisplay));
								selPormo.append(topt);
								if (segInd == 0 && paxInd ==0) {
									UI_PromotionRegister.jsonLabel.termsPromoDblSeat1 = UI_PromotionRegister.jsonLabel.termsPromoDblSeat1.replace("{1}", nonRefund);
									//UI_PromotionRegister.refundText = UI_PromotionRegister.refundText + nonRefund + " AED for "+key+" Empty Seat(s), ";
								}
								
							}
							
						});
					   }
					
					selPormo.change(function() {
						var tm = this.id.split("_");
						if (type == 1) {
							$("#itnPaxPrice_" + tm[1] + "_" + tm[2]).text(
									$(this).val());
						}
					});
					
				} 
				/**
				 * Promotion builder child process : Flexi Date
				 */
				else if (type == 2){
					selectText = " Flexible day(s)";
					selFlexiPormo =$("<table></table");
					
					var buildSelectOptions = function(boundryValue, limit) { // segInd 
						trSelPromo =$("<tr></tr>");
						tdSelPromo = $("<td><label>Flexi Date Lower Limit</label></td>");
						
						if(limit == "upper"){
							tdSelPromo = $("<td><label>Flexi Date Upper Limit</label></td>");
						}
						 tdValContainer = $("<td></td>");
						
					//	 trSelPromo.append(tdSelPromo, tdValContainer);
						
						selPormo =$("<select><option value='-'>None</option></select>").attr("id", "selPromo_"+ segInd +'_' +limit);
						for ( var i = 1; i <= boundryValue; i++) {
							var topt = $("<option>" + (i + selectText) + "</option>").attr("value", i);
							selPormo.append(topt);
						}
						
						selPormo.change(function() {
							var tm = this.id.split("_");
								var selectedObj = document.getElementById('selPromo_' + tm[1] +'_' + tm[2]);
								var selIndx=selectedObj.selectedIndex;
								var selVal =selectedObj.options[selIndx].value;
								if( selVal=="-") selVal=0;
								var dateDiff =0;
								if (tm[2] == "upper"){
									
									if(tm[1] ==0){
										if (UI_PromotionRegister.outboundSegmentCode ==""){
											UI_PromotionRegister.inboundUpper = selVal;
										}
										UI_PromotionRegister.outboundUpper = selVal;
									}else{
										UI_PromotionRegister.inboundUpper = selVal;
									}
									
									UI_PromotionRegister.flexiUpperboundToPersist = selVal;
									if(UI_PromotionRegister.flexiLowerboundToPersist ==undefined) UI_PromotionRegister.flexiLowerboundToPersist=0;
									dateDiff = parseInt(UI_PromotionRegister.flexiUpperboundToPersist,10) + parseInt(UI_PromotionRegister.flexiLowerboundToPersist, 10);
									//$("#reward").text(UI_PromotionRegister.flexiParams["INBOUND_"+PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays']+parseInt(dateDiff, 10)]);
								//	if(dateDiff ==0)$("#reward").text("-");
								//	else{
									//  $("#reward").text(UI_PromotionRegister.flexiParams[PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays']+parseInt(dateDiff, 10)]); //FIXME
								//	}
							    } else {
							    	
							    	if(tm[1] ==0){
							    		if (UI_PromotionRegister.outboundSegmentCode ==""){
											UI_PromotionRegister.inboundLower = selVal;
										}
										UI_PromotionRegister.outboundLower = selVal;
									}else{
										UI_PromotionRegister.inboundLower = selVal;
									}
							    	
									UI_PromotionRegister.flexiLowerboundToPersist = selVal;
									if(UI_PromotionRegister.flexiUpperboundToPersist ==undefined) UI_PromotionRegister.flexiUpperboundToPersist=0;
									dateDiff = parseInt(UI_PromotionRegister.flexiUpperboundToPersist, 10) + parseInt(UI_PromotionRegister.flexiLowerboundToPersist, 10);
									//if(dateDiff ==0)$("#reward").text("-");
									//else{
									// $("#reward").text(UI_PromotionRegister.flexiParams[PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays']+parseInt(dateDiff, 10)]);
									//}
								}
								UI_PromotionRegister.calculateFlexiRewards();
						});
						
						tdValContainer.append(selPormo);
						
						trSelPromo.append(tdSelPromo,tdValContainer);
						return trSelPromo;
					}
					
				if(UI_PromotionRegister.inboundSegmentCode != null && UI_PromotionRegister.inboundSegmentCode !=""){
					if(UI_PromotionRegister.outboundSegmentCode != null && UI_PromotionRegister.outboundSegmentCode !=""){
						$("#FlexiFlight_0").show();
						$("#FlexiFlight_1").show();
						$("#trAccept").show();
						$("#tblContinue").show();
						$("#divReward").show();
						$("#lblFlexiSegments_0").text(UI_PromotionRegister.outboundSegDisplayName);
						$("#lblFlexiSegments_1").text(UI_PromotionRegister.inboundSegDisplayName);
						$("#lblFlexiDepartureDate_0").text(UI_PromotionRegister.outboundDepartureDate);
						$("#lblFlexiDepartureDate_1").text(UI_PromotionRegister.inboundDepartureDate);
						
					 }
				} else {
					if(UI_PromotionRegister.outboundSegmentCode != null && UI_PromotionRegister.outboundSegmentCode !=""){
						$("#FlexiFlight_0").show();
						$("#lblFlexiSegments_0").text(UI_PromotionRegister.outboundSegDisplayName);
						$("#lblFlexiDepartureDate_0").text(UI_PromotionRegister.outboundDepartureDate);
						UI_PromotionRegister.flexiSelectedSegmentId = UI_PromotionRegister.outboundSegmentIds;
					}else if(UI_PromotionRegister.inboundSegmentCode != null && UI_PromotionRegister.inboundSegmentCode !=""){
						$("#FlexiFlight_0").show();
						$("#lblFlexiSegments_0").text(UI_PromotionRegister.inboundSegDisplayName);
						$("#lblFlexiDepartureDate_0").text(UI_PromotionRegister.inboundDepartureDate);
						UI_PromotionRegister.flexiSelectedSegmentId = UI_PromotionRegister.inboundSegmentCode;
					}
						$("#divReward").show();
				}
					
					
					if (UI_PromotionRegister.promotionRequestTo != undefined && UI_PromotionRegister.promotionRequestTo != null) {
						var lowerBound;
						var upperBound;
					
					if (UI_PromotionRegister.promotionRequestTo.lowerBound != undefined && UI_PromotionRegister.promotionRequestTo.lowerBound != null) {
						  lowerBound = UI_PromotionRegister.promotionRequestTo.lowerBound;
						  selFlexiPormo.append(buildSelectOptions(lowerBound, "lower"));
					}
					if (UI_PromotionRegister.promotionRequestTo.upperBound != undefined && UI_PromotionRegister.promotionRequestTo.upperBound != null) {
						  upperBound = UI_PromotionRegister.promotionRequestTo.upperBound;
						  selFlexiPormo.append(buildSelectOptions(upperBound, "upper"));
					}
					
					
					
				 }
			 }
				
				if(type ==1)			
				return selPormo;
				
				else if(type ==2)
				return selFlexiPormo;
			} //end of buildPromoHtml(); 
			
			/**
			 * Promotion builder: common process
			 */
			$("#outBoundPaxBody").empty();
			$("#inBoundPaxBody").empty()
			$(".inBoundFlight").hide();
			$(".outBoundFlight").hide();
			var flexibleDateOffer = "";
			
			if (UI_PromotionRegister.outboundSegmentCode != "") {
				var t = {};
				t.outboundSegmentName = UI_PromotionRegister.outboundSegmentCode;
				t.segIds = UI_PromotionRegister.outboundSegmentIds;
				t.direction = 1;
				tourRoute[tourRoute.length] = t;
				$("#lbloutBoundSegs").text(UI_PromotionRegister.outboundSegDisplayName);
				flexibleDateOffer = UI_PromotionRegister.outboundSegDisplayName;
			}
			if (UI_PromotionRegister.inboundSegmentCode != "") {
				var t = {};
				t.inboundSegmentCode = UI_PromotionRegister.inboundSegmentCode;
				t.segIds = UI_PromotionRegister.inboundSegmentIds;
				t.direction = 2;
				tourRoute[tourRoute.length] = t;
				$("#lblinBoundSegs").text(UI_PromotionRegister.inboundSegDisplayName);
				
				if(type ==1 && UI_PromotionRegister.outboundSegmentCode == ""){
					$("#lblOutBound").text("Inbound");
					$("#lbloutBoundSegs").text(UI_PromotionRegister.inboundSegDisplayName);
				}
				
			}
			
			/**
			 * Promotion builder common process: Next seat pre-process
			 */
			if (UI_PromotionRegister.promoTypeId == 1) {
				$(".flexiDateSpecific").remove();
				var boundSegCount =0;
				var firstBoundSegCount =0;
				var lastBoundSegCount =0;
				
				$.each(tourRoute, function(x, y) {
					$("#tdMoveSeat_"+ x).show();
					
					var segs = y.segIds;
					if(x == 0) firstBoundSegCount= parseInt(segs.split(",").length, 10);
					if(x == 1) lastBoundSegCount= parseInt(segs.split(",").length, 10);
					var hideMoveSeatCol= true;
					$.each(paxCollection, function(i, j) {
						var pTDName = $("<td></td>").append(
								$("<label>" + j.itnPaxName + "</label>").attr(
										"id", "itnPaxName_" + x + "_" + i));
						pTDName.addClass("defaultRowGap rowColor bdBottom");
						
						var pTDTravelRefNo = $("<td style=\"display: none\"></td>").append(
								$("<label>" + j.travelerRefNumber+"</label>").attr(
										"id", "travelerRefNumber" + x + "_" + i));
						
						var pTDProm = $("<td></td>").append(
								buildPromoHtml(type, x, i));
						pTDProm.addClass("rowColor bdBottom");
						var pTDPrice = $("<td></td>").append(
								$("<label>-</label>").attr({
									"id" : "itnPaxPrice_" + x + "_" + i
								}));
						pTDPrice.addClass("rowColor bdBottom alignRight");
						
						var paxSegCount =1;
						if(x == 0) { paxSegCount = 1; boundSegCount = firstBoundSegCount };
						if(x == 1) { paxSegCount = firstBoundSegCount+1; boundSegCount = firstBoundSegCount + lastBoundSegCount; };
						    	
						
						j['currentAncillaries']=$.airutil.sort.quickSort(j.selectedAncillaries, segmentComparator);
						
						var pTR;
						var paxPreRequestedSeats = false;
						if ( j.currentAncillaries != null){
							for ( var anciCount = (paxSegCount - 1); anciCount < j.currentAncillaries.length; anciCount++) {
								if (paxSegCount <= boundSegCount && !paxPreRequestedSeats) {
									if (j.currentAncillaries[anciCount].airSeatDTO != null){
										if (j.currentAncillaries[anciCount].airSeatDTO.seatNumber != null && j.currentAncillaries[anciCount].airSeatDTO.seatNumber != ""){
											paxPreRequestedSeats = true;
											break;
										}
									}
									++paxSegCount;
								}
							}
						}
						
						
						if(paxPreRequestedSeats == true){
							hideMoveSeatCol = hideMoveSeatCol&&false;
							var pTDMoveSeat = $("<td class='move-seat'></td>").append(
									$("<input type='checkbox' checked='checked' />").attr({
										"id" : "itnMoveSeat_" + x + "_" + i
									}));
								
							pTDMoveSeat.addClass("rowColor bdBottom alignRight");
							
							 pTR = $("<tr></tr>").append(pTDName, pTDTravelRefNo, pTDProm,
									pTDPrice, pTDMoveSeat);
							
						} else{
							 var pTDMoveSeat = $("<td class='move-seat'></td>").append(
									$("<input type='checkbox' checked='checked' disabled='disabled'  />").attr({
										"id" : "itnMoveSeat_" + x + "_" + i
									}));
								
							pTDMoveSeat.addClass("rowColor bdBottom alignRight");
							
							 pTR = $("<tr></tr>").append(pTDName, pTDTravelRefNo, pTDProm,
									pTDPrice, pTDMoveSeat);
							
							
//							 pTR = $("<tr></tr>").append(pTDName, pTDTravelRefNo, pTDProm,
//									pTDPrice);
						}
						pTR.attr("id", "promoPax_" + x + "_" + i);
						$("#PaxBody_" + x).append(pTR);
					});
					if(hideMoveSeatCol){$(".move-seat").hide();}
					if ($("#PaxBody_" + x).children().length > 0) {
						$("#PaxBody_" + x).parent().parent().parent().show();
					}
				});
			} 
			/**
			 * Promotion Builder common process: Flexi date pre-process
			 */
			else if (UI_PromotionRegister.promoTypeId == 2) {
				$(".promoNextSeatOnly").remove();
				$.each(paxCollection, function(i, j) {
					var pTDName = $("<td></td>").append(
							$("<label>" + j.itnPaxName + "</label>").attr("id",
									"itnPaxName_" + i));
					pTDName.addClass("defaultRowGap rowColor bdBottom");
					var pTR = $("<tr></tr>").append(pTDName);
					pTR.attr("id", "promoPax_" + i);
					$("#PaxBody_0").append(pTR);
				});
				$(".outBoundFlight").show();
				$(".FlexiFlight_0").hide();
				$(".FlexiFlight_1").hide();
				$("#lblOutBound").find("#flexiPromo").remove();
				$("#lblOutBound").text("Passenger(s) in Reservation");
				$("#lbloutBoundSegs").text("");
				
				$.each(tourRoute, function(x, y) {
					var selectContainer = $(
						"<div><label>Available for : </label></div>").attr(
						"id", "flexiPromo");
					selectContainer.append(buildPromoHtml(type, x, null))
					$("#boundFexi_" + x).append(selectContainer);
					$(".FlexiFlight_" + x).show();
				
				});
				
			}
		}
	

		/**
		 * Entry point for promotion builder process
		 */
		if (paxCollection.length > 0) {
			promotionHTMLGEN(UI_PromotionRegister.promoTypeId, paxCollection);
			setLabel(UI_PromotionRegister.promoTypeId);
		} 

		$("#divLoadBg").show();

	} else if (response != null && response.success == true && response.duplicates == true && response.unsubscribe == false){
		UI_commonSystem.loadErrorPage({
			messageTxt : response.messageTxt
		});
		
	} else if (response != null && response.success == true && response.duplicates == true && response.unsubscribe == true){
		UI_PromotionRegister.jsonLabel = response.jsonLabel;
		UI_PromotionRegister.unsubscribe(response);
		
	} else {
		UI_commonSystem.loadErrorPage({
			messageTxt : response.messageTxt
		});
	}
	UI_commonSystem.loadingCompleted();
}


/**
 * Dynamically calculate Flexi Date rewards
 */
UI_PromotionRegister.calculateFlexiRewards = function() {
	var data1 = {};
	
	url="promotionRegisteration!calculateChargeAndRewards.action?";
	
	data1['promoChargeRewardTo.outboundSegCodes'] = UI_PromotionRegister.outboundSegmentCode;
	data1['promoChargeRewardTo.outboundLower'] = UI_PromotionRegister.outboundLower;
	data1['promoChargeRewardTo.outboundUpper'] = UI_PromotionRegister.outboundUpper;
	data1['promoChargeRewardTo.inboundSegCodes'] = UI_PromotionRegister.inboundSegmentCode;
	data1['promoChargeRewardTo.inboundLower'] = UI_PromotionRegister.inboundLower;
	data1['promoChargeRewardTo.inboundUpper'] = UI_PromotionRegister.inboundUpper;
	data1['promoChargeRewardTo.promotionId'] = UI_PromotionRegister.promotionId;
	$.ajax({
		url : url,
		beforeSend : UI_commonSystem.loadingProgress(),
		data : data1,
		type : "POST",
		dataType : "json",
		success : UI_PromotionRegister.calculateFlexiRewardsSuccess,
		error : function(response) {
			UI_commonSystem.setErrorStatus(response);
		},
		complete : function(response) {
		}

	});
	
}

UI_PromotionRegister.calculateFlexiRewardsSuccess = function(response) {
	if (response != null && response.success == true) {
		$("#reward").text(response.totalReward);		
	} else {
		UI_commonSystem.loadErrorPage({
			messageTxt : response.messageTxt
		});
	}
	UI_commonSystem.loadingCompleted();
}

/**
 *Process for saving of Promotion Requests
 */
UI_PromotionRegister.savePromotionRequests = function() {
	
	if (!($("#chkTerms").is(':checked'))) {
		alert(UI_PromotionRegister.jsonLabel.msgAgreeTerms);
		return false;
	}
	 
	if(UI_PromotionRegister.promoTypeId == 1){
		UI_PromotionRegister.PrepareDataToSaveNextSeatRequestsInSession();
	}
	else if(UI_PromotionRegister.promoTypeId == 2 ){
		UI_PromotionRegister.saveFlexiDateRequests();
	}
}

/**
 * Process for saving of Next Seat Requests
 */
UI_PromotionRegister.PrepareDataToSaveNextSeatRequestsInSession = function() {
	
	var promoReqs = new Array();
	var promoReq = {};
	var index = 0;
	var tot =0;
	
			$.each(tourRoute, function(x, y) {
				$.each(paxCollection, function(i, j) {
					
					var amountElement = document.getElementById("itnPaxPrice_" + x + "_" + i);
					if(amountElement.textContent == '-'){
						amountElement.textContent = 0;
					}
					tot = parseFloat(amountElement.textContent) + parseFloat(tot);
					var selectedObj = document.getElementById("selPromo_" + x + "_"+ i);
					var selIndx=selectedObj.selectedIndex;
					var selVal =selectedObj.options[selIndx].value;
					
					if( selVal !="-") {
						
						var agreedToMove = true;
						if ($('#' + "itnMoveSeat_" + x + "_" + i).is(':checked')) {
							agreedToMove = true;
						} else {
							agreedToMove =false;
						}
						
						var selVar=document.getElementById("selPromo_" + x + "_"+ i);
						
						promoReq = {
								'reservationSegId' : tourRoute[x].segIds,
								'promotionId' : UI_PromotionRegister.promotionId,
								'pnr' : UI_PromotionRegister.pnr,
								'lowerBound' :$("#selPromo_" + x + "_"+ i).find(":selected").text().split(" ")[0],
								'flexiTravelRefNumbers' : j.travelerRefNumber,
								'agreedToMove' : agreedToMove,
								'promoRequestCharge' : parseFloat(amountElement.textContent),
								'direction' : tourRoute[x].direction
						};
						promoReqs[ index ] = promoReq;
						index++;
						
					}
					
				});
			});
			
			if(index == 0){
				alert(UI_PromotionRegister.jsonLabel.msgSelectSeats);
				return false;
			}
			
	 data1 = {'promoRequestData' : JSON.stringify(promoReqs)};
	 UI_PromotionRegister.saveNextSeatRequestsInSession(JSON.stringify(promoReqs), tot);
 }

UI_PromotionRegister.saveNextSeatRequestsInSession = function (promoRequests, totalPayment) {
	var strSubmitUrl = SYS_IBECommonParam.securePath + 'showReservation.action?hdnParamData=en^MAN&fromPromoRegister=true&promoRequestData='+promoRequests+'&totalPayment='+totalPayment+'&promoPnr='+UI_PromotionRegister.pnr+'&promoLastName='+UI_PromotionRegister.contactLastName+'&promoDepDate='+UI_PromotionRegister.outboundDepartureDate;
	location.replace(strSubmitUrl);
	//TODO ajax post submit, Do-not pass the raw amount
	UI_commonSystem.loadingProgress();
}


/**
 * Process for saving of Flexi Date requests
 */
UI_PromotionRegister.saveFlexiDateRequests = function () {
	
	var data1 = {};
		
	var promoReqs = new Array();
	var promoReq = {};
	var index = 0;
	url="promotionRegisteration!saveFlexidateRequests.action?";
	
	$.each(tourRoute, function(x, y) {
		
		var lowerLimit = $("#selPromo_" + x + "_lower").find(":selected").text().split(" ")[0];
		var upperLimit = $("#selPromo_" + x + "_upper").find(":selected").text().split(" ")[0];
		
		if (lowerLimit != 'None' || upperLimit != 'None') {
			
			if(lowerLimit == 'None') lowerLimit = 0;
			if(upperLimit == 'None') upperLimit = 0;
		
			promoReq = {
					'reservationSegId' : tourRoute[x].segIds,
					'promotionId' : UI_PromotionRegister.promotionId,
					'pnr' : UI_PromotionRegister.pnr,
					'lowerBound' : lowerLimit,
					'upperBound' :upperLimit,
					'flexiTravelRefNumbers' : UI_PromotionRegister.flexiTravelReferenceNumbers,
					'direction' : tourRoute[x].direction
			};
			promoReqs[ index ] = promoReq;
			index++;
		}
	});
	
	if(index == 0){
		alert(UI_PromotionRegister.jsonLabel.msgSelectFlexiDates);
		return false;
	}
	
	data1 = {'flexiPromoRequestData' : JSON.stringify(promoReqs)};
		
	$.ajax({
		url : url,
		beforeSend : UI_commonSystem.loadingProgress(),
		data : data1,
		type : "POST",
		dataType : "json",
		success : UI_PromotionRegister.saveFlexiDateRequestsSuccess,
		error : function(response) {
			UI_commonSystem.setErrorStatus(response);
		},
		complete : function(response) {
		}

	});
}

UI_PromotionRegister.saveFlexiDateRequestsSuccess = function(response) {
	if (response != null && response.success == true) {
		UI_PromotionRegister.flexiSubscribedInCurrentSession = true;
		
		document.forms[0].action = "showLoadPage!loadPromotionRegisterConfimationPage.action";
		document.forms[0].target= "_top";
		document.forms[0].submit();

	} else {
		UI_commonSystem.loadErrorPage({
			messageTxt : response.messageTxt
		});
	}
	UI_commonSystem.loadingCompleted();
}


/**
 * Promotion Unsubcription Handler
 */
UI_PromotionRegister.unsubscribe = function(response){
	
	if(response != null && response.success == true) {
		
		if(response.messageTxt != "no_subscriptions" && response.messageTxt != "appr_reject"){
			jConfirm(response.messageTxt, UI_PromotionRegister.jsonLabel.lblUnsubcribeHeader, function(r) {
				if (r){
					var data1 = {};
					data1['promotionRequestTo.promotionId'] = response.promotionRequestTo.promotionId;
					data1['promotionRequestTo.pnr'] = response.promotionRequestTo.pnr;
				
				
					$.ajax({
						url : "promotionRegisteration!withdrawPromotionRequests.action",
						beforeSend : UI_commonSystem.loadingProgress(),
						data : data1,
						type : "POST",
						dataType : "json",
						success : UI_PromotionRegister.unsubscribeSuccess,
						error : function(response) {
							UI_commonSystem.setErrorStatus(response);
						},
						complete : function(response) {
							UI_commonSystem.loadingCompleted();
						}

					});
				
				} else {
					UI_commonSystem.loadErrorPage({
						messageTxt : UI_PromotionRegister.jsonLabel.msgStillEntitled
					});
				}
			});
		}else{
			var displayMessage = response.messageTxt;
			if(response.messageTxt == "no_subscriptions"){
				displayMessage = UI_PromotionRegister.jsonLabel.msgNotSubcribed;
			}else if(response.messageTxt == "appr_reject"){
				displayMessage = UI_PromotionRegister.jsonLabel.msgCannotSubcribe;
			}
			
			UI_commonSystem.loadErrorPage({
				messageTxt : displayMessage
		  });
		}
		
		
  } else{
	  UI_commonSystem.loadErrorPage({
			messageTxt : response.messageTxt
	  });
  }
	
}

UI_PromotionRegister.unsubscribeSuccess = function(response) {
	
	UI_commonSystem.loadErrorPage({
		messageTxt : response.messageTxt
	});
}

