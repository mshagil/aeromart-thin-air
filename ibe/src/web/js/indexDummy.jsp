<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>

</head>
<body>
<table width="775" cellpadding="2" cellspacing="0" border="0" align="center"  class='PageBackGround'>
	<tr>
		<td colspan="2">
			<table width="775" cellpadding="0" cellspacing="0" border="5" align="center">
				<tr>
					<td class="outLine">
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<tr>
								<td style='background-image:url(../images/AA_001_no_cache.jpg);height:74px;' valign='bottom'>
									<table width="95%" border="0" cellpadding="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td width="50%">
												<img src="../images/LogoAni_no_cache.gif">
											</td>
											<td align='right'>
												<a href='#' id="lnkHome" name="lnkHome"><font class="fntDefaultWhite fntBanner"><b>Home</b></font></a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="180">
			<!-- Flight Search Goes Here -->
			<table width='100%' border='0' cellpadding='0' cellspacing='2' ID='Table7' bgcolor="white">
				<tr>
					<td colspan='4'><font><b>Book Flights</b></font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selFromLoc' size='1' style='width:175px;' NAME='selFromLoc' onchange='selFromLocOnChange()'>

						</select>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selToLoc' size='1' style='width:175px;' NAME='selToLoc' onchange='selToLocOnChange()'>
						</select>
					</td>

				</tr>
				<tr>
					<td colspan='4'><font>Departing Date</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
							<tr>

								<td width="95%">
									<select id='selDtDept' size='1' style='width:50px;' NAME='selDtDept' onChange="selDtDeptOnChange()">
										<option></option>
									</select>
									<select id='selYrDept' size='1' style='width:100px;' NAME='selYrDept' onChange="selYrDeptOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>

									<a href='javascript:void(0)' onclick='LoadCalendar(0,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selDVariance' size='1' style='width:50px;' NAME='selDVariance' title='No of days before or after departure date'>

						</select>
						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td><font>Returning Date</font></td>
					<td colspan="3" align='right'>

						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td align='right'>
									<font>&nbsp;Return Trip&nbsp;</font>
								</td>
								<td align='right'>
									<input type="checkbox" id="chkReturnTrip" name="chkReturnTrip" onClick="chkReturnTrip_click()" class="noBorder">
								</td>

							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table3">
							<tr>
								<td width="95%">

									<select id='selDtRetu' size='1' style='width:50px;' NAME='selDtRetu' onChange="selDtRetuOnChange()">
										<option></option>
									</select>
									<select id='selYrRetu' size='1' style='width:100px;' NAME='selYrRetu' onChange="selYrRetuOnChange()">
										<option></option>
									</select>
								</td>
								<td  align='right'>
									<a href='javascript:void(0)' onclick='LoadCalendar(1,event)'><img src='../images/Calendar2_no_cache.gif' border='0' title='Click here to view calendar'></a>

								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selRVariance' size='1' style='width:50px;' NAME='selRVariance' title='No of days before or after return date'>
						</select>

						<font><b>+/-</b> days</font>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0' ID='Table8'>
							<tr>

								<td width='30%'><font>Adults</font></td>
								<td width='33%'><font>Children</font></td>
								<td width='37%'><font>Infants&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
							</tr>
							<tr>
								<td><font>&nbsp;</font></td>
								<td><font class='fntsmall'>(2-14 years)</font></td>

								<td><font class='fntsmall'>(under 2 years)</font></td>
							</tr>
							<tr>
								<td>
									<select id='selAdults' size='1' style='width:45px;' NAME='selAdults' onChange='selAdults_onChange()'>
									</select>
								</td>
								<td>
									<select id='selChild' size='1' style='width:50px;' NAME='selChild'>

									</select>
								</td>
								<td align='right'>
									<select id='selInfants' size='1' style='width:65px;' NAME='selInfants'>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan='4'>
						<a href="#" onClick="faqClick()"><font class='fntsmall'><u>Important information on Child and Infant bookings</u></font></a>
					</td>
				</tr>
				<tr>
					<td colspan='4' class="setHeight"></td>
				</tr>
				<tr id='trCOS1'>
					<td colspan='4'><font>Class</font></td>
				</tr>
				<tr id='trCOS2'>
					<td colspan='4'>
						<select id='selCOS' size='1' style='width:175px;' NAME='selCOS'>
						</select>
					</td>
				</tr>
				<tr id='trCOS3'>
					<td colspan='4' class="setHeight"></td>
				</tr>
				<tr>
					<td colspan='4'><font>Currency</font></td>
				</tr>
				<tr>
					<td colspan='4'>
						<select id='selCurrency' size='1' style='width:175px;' NAME='selCurrency'>
						</select>
					</td>
				</tr>

				<tr>
					<td colspan='4' align='right'>
						<input type='button' id='btnSearch' class='Button' value='Search' title='Click here to Search for flights' NAME='btnSearch' onclick='flightSearchBtnOnClick(0)'>
					</td>
				</tr>
			</table>
		</td>
		<td rowspan='5' valign='bottom'>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="btn1" value="Register" class='Button' onclick='flightSearchBtnOnClick(1)'>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" id="Button1" value="Sign In" class='Button' onclick='flightSearchBtnOnClick(2)'>
		</td>
	</tr>
	<tr>
		<td>
			<font>Language</font>
		</td>
	</tr>
	<tr>
		<td>
			<select id='selLangunage' size='1' style='width:175px;' NAME='selLangunage'>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan='2'>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan='2' style='background-image:url(../images/Bar1_no_cache.jpg);'>
			<font class='fntDefaultWhite'>2006 � Mihin Lanka</font>
		</td>
	</tr>
</table>
<form id="form1" method="post" action="">
	<input type="hidden" id="hdnParamData" name="hdnParamData">
</form>
</body>
<script>
var ibeConfig=new Array();
<c:out value="${applicationScope.appIBEConfigJS}" escapeXml="false" />
<c:out value='${applicationScope.appIBEUrlsJs}' escapeXml='false' />
</script>
<c:out value='${applicationScope.appIndexJSFile}' escapeXml='false' />
<script src="../js/indexCommon.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</html>
