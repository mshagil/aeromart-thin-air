function UI_giftVoucher() {
}

UI_giftVoucher.templateList = null;
UI_giftVoucher.templateListSalesPeriod = null;
UI_giftVoucher.showPaymentIcons = false;
UI_giftVoucher.paymentMethods = [];
UI_giftVoucher.objPGS = null;
UI_giftVoucher.isCardPanelBuild = false;
UI_giftVoucher.brokerType = null;
UI_giftVoucher.paymentGatewayID = null;
UI_giftVoucher.paymentGatewayTypeCode = null;
UI_giftVoucher.selectedPaymentMethodValStr = null;
UI_giftVoucher.systemDate = null;
UI_giftVoucher.errorInfo = "";
UI_giftVoucher.lables = "";
UI_giftVoucher.baseCurrency = "";
UI_giftVoucher.isFirstClick = true;
UI_giftVoucher.ccTnxFeeBase = "";
UI_giftVoucher.isBulkIssue = false;
UI_giftVoucher.voucherListToBuy = new Array();
UI_giftVoucher.globaltotal=0;
UI_giftVoucher.voucherTnxFeeMap = new Array();
UI_giftVoucher.voucherDTOList = new Array();
UI_giftVoucher.externalPayment = false;

var voucher = null;
var globalBtnId = 1;

var strSelectEmpty = "<option value=''> </option>";

UI_giftVoucher.paymentGatewayID = null;
UI_giftVoucher.paymentGatewayProviderCd = null;

$(document).ready(function() {
	UI_giftVoucher.ready();
});

UI_giftVoucher.ready = function() {
	$("#paxFirstName").alphaWhiteSpace();
	$("#paxLastName").alphaWhiteSpace();
	$("#divCardInfoPannel").hide();
	$("#btnPurchase").hide();
	$("#btnEmail").hide();
	$("#btnPrint").hide();
	$("#lblTotalDue").text("Total Due Amount");
	$("#voucherSummery").hide();
	$("#divPayment").hide();
	$("#divLoadBg").hide();
	$("#paymentButtonset").hide();
	$("#ccTaxVoucher").hide();
	$("#btnAdd").hide();
	$("#voucherListSummery").hide();

	UI_giftVoucher.getGiftVoucherScreenDetails();

	$("#voucherName").change(function() {
		UI_giftVoucher.populateVoucherTemplate();
	});
	$("#amountSrch").change(function() {
		UI_giftVoucher.populateCurrencyDropDown();
	});
	$("#currencySrch").change(function() {
		UI_giftVoucher.populateValidityDropDown();
	});
	$("#validitySrch").change(function() {
		UI_giftVoucher.populateVoucherNameDropDown();
	});

	$("#btnBuy").click(function() {
		if (UI_giftVoucher.validate()) {
			UI_giftVoucher.displayPaymentPannel();
			$("#btnEmail").hide();
			$("#btnPrint").hide();
			UI_giftVoucher.prepareForPayment();
			$("#btnPurchase").show();
		}
	});

	$("#btnReset").click(function() {
		UI_giftVoucher.resetPaxDetails();
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#emailSuccess").hide();
		$("#btnPurchase").hide();
	});
	
	$("#btnBack").click(function() {
		UI_giftVoucher.previousStep();
	});
	
	$("#btnAddToBuy").click(function() {
		UI_giftVoucher.addVouchersToCart();
	});
	
	$("#btnAdd").click(function() {
		UI_giftVoucher.clearVoucherDetails();
		$("#btnAdd").hide();
		$("#btnAddToBuy").show();
		UI_giftVoucher.enableVoucherDetails();		
	});

	$("#expiryMonth").change(function() {
		UI_giftVoucher.expiryDateChange();
	});
	$("#expiryYear").change(function() {
		UI_giftVoucher.expiryDateChange();
	});
	$("#whatisthis").click(function() {
		UI_giftVoucher.displayCCD($(this));
	});
	$("#btnPurchase").click(function() {
		UI_giftVoucher.ccPayment();
	});
}

UI_giftVoucher.displayPaymentPannel = function() {
	var data = {};
	data["amountInBase"] = parseFloat(UI_giftVoucher.globaltotal).toFixed(2);
	$.ajax({
		url : 'showVoucherPaymentDetails.action',
		dataType : "json",
		data : data,
		type : "POST",
		beforeSend : showProgress(),
		success : UI_giftVoucher.voucherPaymentDetailSuccess,
		error : UI_giftVoucher.loadErrorPage
	});
}

UI_giftVoucher.getGiftVoucherScreenDetails = function() {
	showProgress();
	$.ajax({
		url : 'showVoucherDetails.action',
		dataType : "json",
		type : "POST",
		success : UI_giftVoucher.voucherDetailSuccess,
		error : UI_giftVoucher.loadErrorPage
	});
}

UI_giftVoucher.voucherDetailSuccess = function(response) {
	hideProgress();
	if (response != null && response.success == true) {
		$("#divLoadBg").slideDown(1000);
		UI_giftVoucher.templateList = response.voucherTemplateList;
		UI_giftVoucher.populateDropDowns(UI_giftVoucher.templateList);
		// UI_giftVoucher.populateTemplateDropDown(UI_giftVoucher.templateList);
		UI_giftVoucher.paymentMethods = response.paymentMethods;
		UI_giftVoucher.showPaymentIcons = response.showPaymentIcons;
		UI_giftVoucher.systemDate = response.systemDate;
		UI_giftVoucher.errorInfo = response.errorInfo;
		UI_giftVoucher.lables = response.jsonLabel;
		UI_giftVoucher.setLables();
		UI_giftVoucher.baseCurrency = response.baseCurrency
		$("#lblYourIP").text(response.userIp);

	} else {
		wrapAndDebugError('001', response);
	}

}

UI_giftVoucher.setLables = function() {
	var lables = UI_giftVoucher.lables;
	for (var k in lables) {
		if (lables.hasOwnProperty(k)) {
			$("#" + k).html(lables[k]);
		}
	}
}

UI_giftVoucher.populateTemplateDropDown = function(templates) {
	var voucherTemplates = "";
	voucherTemplates += "<option value = '' ></option>";
	if (templates != null) {
		for (i = 0; i < templates.length; i++) {
			voucherTemplates += "<option value = '"
					+ JSON.stringify(templates[i]) + "' >"
					+ templates[i].voucherName + "</option>";
		}
	}
	$('#voucherName').empty();
	$('#voucherName').append(voucherTemplates);
}

UI_giftVoucher.populateVoucherTemplate = function() {
	var template = JSON.parse($("#voucherName").val());
	var validFrom = new Date();
	var validTo = new Date(validFrom.getTime() + template.voucherValidity * 24
			* 3600 * 1000);
	$("#amountInLocal").val(template.amountInLocal);
	$("#currencyCode").val(template.currencyCode);
	$("#validFrom").val(
			validFrom.getDate() + "/" + (validFrom.getMonth() + 1) + "/"
					+ validFrom.getFullYear());
	$("#validTo").val(
			validTo.getDate() + "/" + (validTo.getMonth() + 1) + "/"
					+ validTo.getFullYear());
	UI_giftVoucher.formDissable();
	// $("#divCardInfoPannel").hide();
	// $("#divCardInfoPannel").show();
}

UI_giftVoucher.voucherPaymentDetailSuccess = function(response) {
	hideProgress();
	UI_giftVoucher.paymentGatewayID = response.paymentGatewayID;
	UI_giftVoucher.objPGS = response.paymentGateways;
	UI_giftVoucher.buildCardInitPannel();
	if (UI_giftVoucher.objPGS != null) {
		// Build Payment Card List
		UI_giftVoucher.buildPaymentCardListPannel(response.cardsInfo);
	} else {
		$("#divCardInfoPannel").hide();
	}
}

UI_giftVoucher.formDissable = function() {
	$("#amountInLocal").prop('disabled', true);
	$("#currencyCode").prop('disabled', true);
	$("#validFrom").prop('disabled', true);
	$("#validTo").prop('disabled', true);
}

UI_giftVoucher.resetPaxDetails = function() {
	$("#paxFirstName").val("");
	$("#paxLastName").val("");
	$("#email").val("");
	$("#remarks").val("");
	$("#divCardInfoPannel").hide();
	$("#divTotalAmount").hide();
}

UI_giftVoucher.resetVoucherDetails = function() {
	$('#voucherName').val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
	$("#validFrom").val("");
	$("#validTo").val("");

}

UI_giftVoucher.validate = function() {
	var lengthArr = Object.keys(UI_giftVoucher.voucherListToBuy).length;
	if (lengthArr==0) {
		jAlert(UI_giftVoucher.errorInfo["selectVoucher"], 'Alert');
		return false;
	} else if (trim($("#paxFirstName").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["firstNameEmpty"], 'Alert');
		return false;
	} else if (trim($("#paxLastName").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["lastNameEmpty"], 'Alert');
	} else if (trim($("#email").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["emailEmpty"], 'Alert');
		return false;
	} else if (!checkEmail(trim($("#email").val()))) {
		jAlert(UI_giftVoucher.errorInfo["emailInvalid"], 'Alert');
		return false;
	} else {
		return true;
	}
}

UI_giftVoucher.formatDate = function(value) {
	var hipDate = value.substr(0, 10);
	return hipDate.replace("-", "/")
}

UI_giftVoucher.buildPaymentCardListPannel = function(cardsInfo) {

	if (cardsInfo == null || cardsInfo == "" || cardsInfo.length == 0) {
		jAlert(UI_giftVoucher.errorInfo["noPaymentMethod"], 'Alert');
		return;
	}
	setClassName = function(className) {
		var send = className.toUpperCase();
		// TODO Re-factor
		if (className.toUpperCase().indexOf("MASTER") > -1) {
			send = "MASTER";
		} else if (className.toUpperCase().indexOf("VISA") > -1) {
			send = "VISA";
		} else if (className.toUpperCase().indexOf("AMEX") > -1) {
			send = "AMEX";
		} else if (className.toUpperCase().indexOf("CMI") > -1) {
			send = "CMI";
		} else if (className.toUpperCase().indexOf("MTC") > -1) {
			send = "MTC";
		} else if (className.toUpperCase() === "CASH") {
			send = "CASH";
		} else if (className.toUpperCase().indexOf("PAYPAL") > -1) {
			send = "PAYPAL";
		} else if (className.toUpperCase().indexOf("BANCONTACT") > -1) {
			send = "BANCONTACT";
		} else if (className.toUpperCase().indexOf("BLEUE") > -1) {
			send = "BLEUE";
		} else if (className.toUpperCase().indexOf("GIROPAY") > -1) {
			send = "GIROPAY";
		} else if (className.toUpperCase().indexOf("IDEAL") > -1) {
			send = "IDEAL";
		} else if (className.toUpperCase().indexOf("SOFORT") > -1) {
			send = "SOFORT";
		} else if (className.toUpperCase() === "DEBIT") {
			send = "DEBIT";
		} else if (className.toUpperCase().indexOf("JCB") > -1) {
			send = "JCB";
		} else if (className.toUpperCase().indexOf("KNET") > -1) {
			send = "KNET";
		} else if (className.toUpperCase().indexOf("MOBICASH") > -1) {
			send = "MOBICASH";
		} else if (className.toUpperCase().indexOf("FATOURATI") > -1) {
			send = "FATOURATI";
		} else if (className.toUpperCase().indexOf("CANAUX1") > -1) {
			send = "CANAUX1";
		} else if (className.toUpperCase().indexOf("CANAUX2") > -1) {
			send = "CANAUX2";
		} else if (className.toUpperCase().indexOf("QIWI_OFFLINE") > -1) {
			send = "QIWI_OFFLINE";
		} else if (className.toUpperCase().indexOf("QIWI") > -1) {
			send = "QIWI";
		} else if (className.toUpperCase() === "CCAVENUE_CREDIT") {
			send = "CCAVENUE_CREDIT";
		} else if (className.toUpperCase() === "CCAVENUE_DEBIT") {
			send = "CCAVENUE_DEBIT";
		} else if (className.toUpperCase() === "CCAVENUE_NET") {
			send = "CCAVENUE_NET";
		}

		// TODO to implement all other card names to match with the card images
		return send;
	};

	if (UI_giftVoucher.isCardPanelBuild) {
		var cardImage = "";
		var cardCss = "";
		$("#listCard").html("")
		for (var y = 0; y < cardsInfo.length; y++) {
			cardCss = setClassName(cardsInfo[y].cssClassName);
			if (cardCss != "") {
				cardImage = "<span class='" + cardCss + "'></span>";
			} else {
				cardImage = "";
			}

			if (cardsInfo[y].modicifationAllowed) {

				$("#listCard")
						.append(
								"<li style='display: inline;'><div class='card-img'>"
										+ cardImage
										+ "</div><span class='card-desc'>"
										+ "<input type='RADIO' class='noBorder' name='cardRadio' id='cardRadio_"
										+ cardsInfo[y].cardType + "' value='"
										+ cardsInfo[y].cardType + ":"
										+ cardsInfo[y].paymentGateWayID + ":"
										+ cardsInfo[y].behaviour + ":"
										+ cardsInfo[y].cardDisplayName + "'>"
										+ "<label class='fntBold'> "
										+ cardsInfo[y].cardDisplayName
										+ "</label></span></li>");
			}

		}
		;

		$('input[name="cardRadio"]').click(function(e) {
			UI_giftVoucher.paymentMethodChange();
		});
	}
	UI_giftVoucher.buildCardDetailPanel();
}

UI_giftVoucher.expiryDateChange = function() {
	$("#expiryDate").val($("#expiryMonth").val() + $("#expiryYear").val());
}

UI_giftVoucher.paymentMethodChange = function() {

	var element = $('input:radio[name=cardRadio]:checked');
	var selectCardVal = element.val();
	UI_giftVoucher.selectedPaymentMethodValStr = selectCardVal;

	if (selectCardVal == null || selectCardVal == "") {
		alert("Can not process payment");
		return;
	}
	var selectCardValArr = selectCardVal.split(":");
	var cardTypeID = selectCardValArr[0];
	var paymentGatewayID = selectCardValArr[1];
	var paymentGatewayMode = selectCardValArr[2];
	var paymentMethod = selectCardValArr[3];

	UI_giftVoucher.brokerType = paymentGatewayMode;
	if (paymentGatewayID == "null") {
		UI_giftVoucher.paymentGatewayID = null;
	} else {
		$("#paymentType").val('NORMAL');
		UI_giftVoucher.paymentGatewayID = paymentGatewayID;
		UI_giftVoucher.paymentGatewayTypeCode = paymentMethod;
	}
	$("#cardType").val(cardTypeID);
	$("#paymentMethod").val(paymentMethod);

	UI_giftVoucher.updateAmountPGWise(paymentGatewayID);
}

UI_giftVoucher.updateAmountPGWise = function(paymentGatewayID) {
	var data = {};	
	var voucherList = new Array();
	var i=0;
	for(var key in UI_giftVoucher.voucherListToBuy){		
		var template = UI_giftVoucher.voucherListToBuy[key][1];
		var voucherTemplateTo;
		voucherName = template.voucherName;
		amountInLocal = template.amountInLocal;
		data["voucherList["+i+"].voucherName"] = voucherName;
		data["voucherList["+i+"].amountInLocal"] = amountInLocal;		
		i++;
	}
	
	data["amountInBase"] = parseFloat(UI_giftVoucher.globaltotal).toFixed(2);
	data["paymentGatewayID"] = paymentGatewayID;
	$.ajax({
		url : 'showVoucherPaymentDetails.action',
		dataType : "json",
		data : data,
		type : "POST",
		beforeSend : showProgress(),
		success : UI_giftVoucher.updateAmountPGWiseSuccess,
		error : UI_giftVoucher.loadErrorPage 
	});
}

UI_giftVoucher.updateAmountPGWiseSuccess = function(response) {
	hideProgress();
	UI_giftVoucher.paymentGatewayID = response.paymentGatewayID;
	UI_giftVoucher.objPGS = response.paymentGateways;
	var responseVList = response.voucherList;
	for(var i=0;i<responseVList.length;i++) {
		UI_giftVoucher.voucherTnxFeeMap[responseVList[i].voucherName] = responseVList[i].ccTnxFee;
	}
	$("#hdnTnxFee").val(response.ccTxnFee);
	UI_giftVoucher.ccTnxFeeBase = response.ccTxnFeeInBase;

	UI_giftVoucher.pgSelected();
}

UI_giftVoucher.pgSelected = function() {
	var pgId = UI_giftVoucher.paymentGatewayID;
	if (pgId != null) {
		for (var pl = 0; pl < UI_giftVoucher.objPGS.length; pl++) {
			if (UI_giftVoucher.objPGS[pl].paymentGateway == pgId) {
				UI_giftVoucher.fillPaymentGW(UI_giftVoucher.objPGS[pl]);
				break;
			}
		}
	}
}

UI_giftVoucher.fillPaymentGW = function(pg) {
	UI_giftVoucher.externalPayment = false;
	UI_giftVoucher.buildCardDetailPanel(pg);
	if (pg.payCurrency != null) {
		$("input[name=valBaseCurrCode]").text(pg.payCurrency);
		$("#lblTotalDueAmount").html(
				pg.payCurrencyAmount + " " + pg.payCurrency);
		// $("#valBaseCurrCode").text(pg.payCurrencyAmount);
		$("#ccTaxVoucher").show();
		$("#ccTaxAmount").html(UI_giftVoucher.ccTnxFeeBase);
		$("#ccTaxCurrency").html(UI_giftVoucher.baseCurrency);
		$("#baseTotalAmount").html(parseFloat(+UI_giftVoucher.ccTnxFeeBase + (+$("#baseAmount").text())).toFixed(2));
		$("#baseTotalCurrency").html(UI_giftVoucher.baseCurrency);
		$("#payCurrency").val(pg.payCurrency);
		$("#providerName").val(pg.providerName);
		$("#providerCode").val(pg.providerCode);
		$("#description").val(pg.description);
		$("#paymentGateway").val(pg.paymentGateway);
		$("#payCurrencyAmount").val(pg.payCurrencyAmount);
		$("#brokerType").val(UI_giftVoucher.brokerType);
		$("#switchToExternalURL").val(pg.switchToExternalURL);
		$("#viewPaymentInIframe").val(pg.viewPaymentInIframe);

		if (pg.switchToExternalURL) {
			$("#lblTotalDueAmountE").html(
					pg.payCurrencyAmount + " " + pg.payCurrency);
			UI_giftVoucher.externalPayment = true;
		}
	}
}

UI_giftVoucher.buildCardDetailPanel = function(paymentGatyeway) {
	$("#divCardInfoPannel").show();
	if (typeof paymentGatyeway === "undefined") {
	    return;
	} else {
		if (UI_giftVoucher.brokerType == "internal-external") {
			$("#cardDetailPannel").show();
			$("#paymentInfoPanel").hide();
			UI_giftVoucher.clearCardDetails();
			UI_giftVoucher.paymentGatewayProviderCd = paymentGatyeway.providerCode;
			$("#cardTypePics").empty();
			$("#cardType").focus();
			$("#btnPurchase").show();
			if ($("#cardType").val() != '6') {
				$("#extraCardDetailForPayPalPannel").hide();
			} else {
				$("#extraCardDetailForPayPalPannel").show();
			}
		} else if (UI_giftVoucher.brokerType == "external") {
			$("#btnPurchase").show();
			$("#paymentInfoPanel").show();
			$("#cardDetailPannel").hide();
		}
	}
}

UI_giftVoucher.buildCardInitPannel = function() {
	if (!UI_giftVoucher.isCardPanelBuild) {
		UI_giftVoucher.buildMonths();
		UI_giftVoucher.buildYears();
		UI_giftVoucher.isCardPanelBuild = true;
	}
	$("#cardDetailPannel").hide();
}

UI_giftVoucher.buildMonths = function() {
	$("#expiryMonth").empty();
	var strMonths = strSelectEmpty;

	for (var i = 1; i <= 12; i++) {
		if (i < 10) {
			strMonths += "<option value='0" + i + "'> 0" + i + " </option>";
		} else {
			strMonths += "<option value='" + i + "'> " + i + " </option>";
		}
	}

	$("#expiryMonth").append(strMonths);
}

UI_giftVoucher.buildYears = function() {
	$("#expiryYear").empty();
	var strYears = strSelectEmpty;
	var strSystemYear = UI_giftVoucher.systemDate.split("/")[2];

	if (strSystemYear == null || strSystemYear == "") {
		strSystemYear = (new Date()).getFullYear();
	}

	for (var i = Number(strSystemYear); i < Number(strSystemYear) + 15; i++) {
		strYears += "<option value='" + i.toString().substring(2) + "'> " + i
				+ " </option>";
	}

	$("#expiryYear").append(strYears);
}

UI_giftVoucher.displayCCD = function(obj) {
	colseThisPopup = function() {
		UI_commonSystem.readOnly(false);
		$(".popuploader").hide();
	};
	colseThisPopup();
	UI_commonSystem.readOnly(true);
	var posobj = obj.position();
	$(".popuploader").css({
		"position" : "absolute",
		"z-index" : "1900",
		"top" : posobj.top - 260,
		"left" : posobj.left + 60
	});
	$(".closethisp").click(function() {
		colseThisPopup();
	});
	if ($("#cardType").val() != 3)
		$(".popupbody").html('<img src="../images/allCards_no_cache.gif" />');
	else
		$(".popupbody").html('<img src="../images/AmexCards_no_cache.gif" />');
	$(".popuploader").show();
	$("#page-mask").click(function() {
		colseThisPopup();
	});
}

UI_giftVoucher.ccPayment = function() {
	showProgress();
	if (UI_giftVoucher.validateCcDetails()) {
		UI_giftVoucher.prepareVoucherList();
		var template = JSON.parse($("#voucherName").val());
		var cardHoldersName = trim($("#cardHoldersName").val());
		var cardNumber = trim($("#cardNumber").val());
		var expiryMonth = trim($("#expiryMonth").val());
		var expiryYear = trim($("#expiryYear").val());
		var cardCVV = trim($("#cardCVV").val());

	//	$("#camountInLocal").val(template.amountInLocal);
	//	$("#templateId").val(template.voucherId);
		$("#ccardNumber").val(cardNumber);
		$("#ccardHoldersName").val(cardHoldersName);
		$("#cexpiryDate").val(expiryMonth + expiryYear.slice(-2));
		$("#ccardCVV").val(cardCVV);
		$("#ccardTypedata").val(
				$('input:radio[name=cardRadio]:checked').val().split(':')[0]);
		$("#cpaymentMethod").val(
				$('input:radio[name=cardRadio]:checked').val().split(':')[3]);
		$("#paymentGateway").val(UI_giftVoucher.paymentGatewayID);
		$("#vcardType").val(
				$('input:radio[name=cardRadio]:checked').val().split(':')[0]);
		$("#vcardNumber").val(cardNumber);
		$("#vcardExpiry").val(expiryMonth + expiryYear.slice(-2));
		$("#vcardCVV").val(cardCVV);
		$("#vcardHolderName").val(cardHoldersName);
		$("#vpaymentMethod").val("CC");
	//	$("#vamountLocal").val(template.amountInLocal);
		$("#vcardTxnFeeLocal").val($("#hdnTnxFee").val());
	//	$("#vcurrencyCode").val(template.currencyCode);
		$("#vipgId").val(
				$('input:radio[name=cardRadio]:checked').val().split(':')[1]);
		$("#vvalidFrom").val($("#validFrom").val());
		$("#vvalidTo").val($("#validTo").val());
		$("#vpaymentMethod").val("CC");
		$("#vList").val(JSON.stringify(UI_giftVoucher.voucherDTOList));
		$("#frmVoucherPurchase").attr('action', 'issueGiftVoucher.action');
		$("#frmVoucherPurchase").attr('method', 'POST');
		$("#frmVoucherPurchase").submit();
	} else {
		hideProgress();
	}
}

UI_giftVoucher.prepareVoucherList = function() {
	var lengthArr = Object.keys(UI_giftVoucher.voucherListToBuy).length;
	var i = 0;
	for(var key in UI_giftVoucher.voucherListToBuy){
		var template = UI_giftVoucher.voucherListToBuy[key][1];
		var voucherName =template.voucherName;
		var txnFee = UI_giftVoucher.voucherTnxFeeMap[voucherName];
		var voucherPreDTO = {
			voucherName : voucherName,
			amountInLocal : template.amountInLocal,
			templateId : template.voucherId,
			currencyCode : template.currencyCode,
			validFrom : UI_giftVoucher.voucherListToBuy[key][2],
			validTo : UI_giftVoucher.voucherListToBuy[key][3],
			ccTxnFeeLocal : txnFee ,
			quantity : UI_giftVoucher.voucherListToBuy[key][0]
		};
		UI_giftVoucher.voucherDTOList[i] = voucherPreDTO;
		i++;
	}
}

UI_giftVoucher.voucherSaveSuccess = function(response) {

	if (response.redirectUrl != null) {
		window.location.href = "/ibe" + response.redirectUrl;
	}
	hideProgress();
	voucher = response.voucherDTO;
	$("#divTotalAmount").hide();
	$("#divCardInfoPannel").hide();
	$("#emailSuccess").hide();

	$("#btnEmail").show();
	$("#btnPrint").show();
	$("#btnPurchase").hide();
	UI_giftVoucher.resetPaxDetails();
	UI_giftVoucher.resetCardDetails();
	UI_giftVoucher.resetVoucherDetails();

}

UI_giftVoucher.validateCcDetails = function() {
	var cardType = $('input:radio[name=cardRadio]:checked').val().split(':')[0];
	var cardNumber = trim($("#cardNumber").val());
	if (UI_giftVoucher.externalPayment){
		return true;
	} else if (trim($("#cardHoldersName").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["noCardHolderName"], 'Alert');
		return false;
	} else if (cardNumber == "") {
		jAlert(UI_giftVoucher.errorInfo["emptyCard"], 'Alert');
		return false;
	} else if (!ValidateCardNos(cardType, cardNumber)) {
		jAlert(UI_giftVoucher.errorInfo["invalidCard"], 'Alert');
		return false;
	} else if (trim($("#expiryMonth").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["emptyMonth"], 'Alert');
		return false;
	} else if (trim($("#expiryYear").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["emptyYear"], 'Alert');
		return false;
	} else if (trim($("#cardCVV").val()) == "") {
		jAlert(UI_giftVoucher.errorInfo["emptyCvv"], 'Alert');
		return false;
	} else if (!UI_giftVoucher.validateCvvCode(cardType, $("#cardCVV").val())) {
		jAlert(UI_giftVoucher.errorInfo["invalidCvv"], 'Alert');
		return false;
	} else {
		return true;
	}
}

UI_giftVoucher.resetCardDetails = function() {
	$("#cardHoldersName").val("");
	$("#cardNumber").val("");
	$("#expiryMonth").val("");
	$("#expiryYear").val("");
	$("#cardCVV").val("");
}

UI_giftVoucher.validateCvvCode = function(cardType, cvvCode) {

	var digits = 0;
	// TODO Set Card Type
	switch (cardType) {
	case '1': // MASTERCARD
	case 'EUROCARD': // EUROCARD
	case 'EUROCARD/MASTERCARD':
	case '2': // VISA
	case '5': // CB
	case 'DISCOVER':
	case '6': // PAYPAL
		digits = 3;
		break;
	case '3':
	case 'AMERICANEXPRESS':
	case 'AMERICAN EXPRESS':
		digits = 4;
		break;
	default:
		return false;
	}

	var regExp = new RegExp('[0-9]{' + digits + '}');
	return (cvvCode.length == digits && regExp.test(cvvCode))
}

showProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").show();
}

hideProgress = function() {
	$("#divLoadMsg ,#divLoadMsg .mainPageLoaderFreame").hide();
}

function wrapAndDebugError(id, response) {
	var msg = (response == null) ? null : response.messageTxt;
	if (msg == null || msg == '') {
		if (msg == null)
			return; // no errors to be shown as browser back
		// create a bug in ajaxSubmit plugin
		if (console != null && console.log != null) {
			console.log(id);
			console.log(response);
		}
		msg = 'REF:' + id + ' Error occured. Please report with reference';
	}
	UI_commonSystem.loadErrorPage({
		messageTxt : msg
	});
}

UI_giftVoucher.prepareForPayment = function() {
	$("#tabFirst").removeClass('stepsItem select');
	$("#tabFirst").addClass('stepsItem selected last-setected');
	$("#tabSecond").removeClass('stepsItem');
	$("#tabSecond").addClass('stepsItem select');
	$("#tableModSearch").hide();
	$("#tableModDetails").hide();
	$("#btnSet1").hide();
	$("#divPayment").slideDown(1000);
	$("#voucherSummery").slideDown(500);
	$("#paymentButtonset").show();
	$("#voucherListSummery").hide();
 
	var headerHtml = "<tr><td width = '40%'><label class='fntBold' id='lblV'>Gift Voucher</label></td>"+
					 "<td width = '20%'><label class='fntBold' id='lblQ'>Quantity</label></td>"+
					 "<td width = '40%' align='center'><label class='fntBold' id='lblP'>Price</label></td></tr>"
	
	$("#paymentPgSummary").html("");
	$("#paymentPgSummary").append(headerHtml);
	
	for(var key in UI_giftVoucher.voucherListToBuy){
		var voucherName = UI_giftVoucher.voucherListToBuy[key][1].voucherName;
		var quantity =  UI_giftVoucher.voucherListToBuy[key][0];
		var amount = parseFloat(UI_giftVoucher.voucherListToBuy[key][1].amount*quantity).toFixed(2);
		UI_giftVoucher.globaltotal = parseFloat(amount) + parseFloat(UI_giftVoucher.globaltotal);
		
		var detailsHtml = "<tr><td width = '40%'><label>"+voucherName+"</label></td>"+
						  "<td width = '20%'><label>"+quantity+"</label></td>"+
						  "<td width = '40%' align='right'><label>"+amount+" "+UI_giftVoucher.baseCurrency+"</label></td></tr>"
		$("#paymentPgSummary").append(detailsHtml);				  
	}
	
	UI_giftVoucher.setLables();
	
	$("#cusName").html($("#paxFirstName").val() + " " + $("#paxLastName").val());
	$("#cusEmail").html($("#email").val());
	$("#baseAmount").html(parseFloat(UI_giftVoucher.globaltotal).toFixed(2));
	$("#baseCurrency").html(UI_giftVoucher.baseCurrency);
	$("#baseTotalAmount").html(parseFloat(UI_giftVoucher.globaltotal).toFixed(2));
	$("#baseTotalCurrency").html(UI_giftVoucher.baseCurrency);
}

UI_giftVoucher.populateDropDowns = function(templates) {
	var voucherAmounts = "";
	voucherAmounts += "<option value = '' ></option>";
	var amountPrevious = new Array();
	if (templates != null) {
		for (i = 0; i < templates.length; i++) {
			if($.inArray(templates[i].amountInLocal, amountPrevious)==-1){
				amountPrevious.push(templates[i].amountInLocal);
				voucherAmounts += "<option value = '"
					+ JSON.stringify(templates[i]) + "' >"
					+ templates[i].amountInLocal + '' + "</option>";
			}			
			
		}
	}

	$('#amountSrch').empty();
	$('#amountSrch').append(voucherAmounts);
	$("#voucherName").empty();
	$("#validitySrch").empty();
	$("#currencySrch").empty();
	$("#voucherName").val("");
	$("#validitySrch").val("");
	$("#currencySrch").val("");
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
}

UI_giftVoucher.populateCurrencyDropDown = function() {
	var amountSelected = $('#amountSrch option:selected').text();
	var templates = UI_giftVoucher.templateList;
	var voucherCurrencyList = "";
	var curPrevious = new Array();
	voucherCurrencyList += "<option value = '' ></option>";
	if (templates != null) {
		for (i = 0; i < templates.length; i++) {
			if (templates[i].amountInLocal == amountSelected) {
				if($.inArray(templates[i].currencyCode, curPrevious)==-1){
					curPrevious.push(templates[i].currencyCode);
					voucherCurrencyList += "<option value = '"
							+ JSON.stringify(templates[i]) + "' >"
							+ templates[i].currencyCode + "</option>";
				}
			}
		}
	}
	$('#currencySrch').empty();
	$('#currencySrch').append(voucherCurrencyList);
	$("#voucherName").empty();
	$("#validitySrch").empty();
	$("#voucherName").val("");
	$("#validitySrch").val("");
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
}

UI_giftVoucher.populateValidityDropDown = function() {
	var templates = UI_giftVoucher.templateList;
	var valdityPrevious = new Array();
	var validity;
	var selectedCurrency = $('#currencySrch option:selected').text();
	var amountSelected = $('#amountSrch option:selected').text();
	var voucherPeriods = "";
	voucherPeriods += "<option value = '' ></option>";
	if (templates != null) {
		for (i = 0; i < templates.length; i++) {
			
			if (templates[i].currencyCode == selectedCurrency && templates[i].amountInLocal == amountSelected) {
				if($.inArray(templates[i].voucherValidity, valdityPrevious)==-1){
					valdityPrevious.push(templates[i].voucherValidity);
					
					switch (templates[i].voucherValidity) {
						case 30:
							validity = "30 Days"
							break;
						case 180:
							validity = "180 Days"
							break;
						case 90:
							validity = "90 Days"
							break;
						case 365:
							validity = "1 Year"
							break;
						default:
							validity = "30 Days"
						}
					voucherPeriods += "<option value = '"
							+ JSON.stringify(templates[i]) + "' >" + validity
							+ "</option>";
				}
			}
		}
	}
	$('#validitySrch').empty();
	$('#validitySrch').append(voucherPeriods);
	$("#voucherName").empty();
	$("#voucherName").val("");
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
}
UI_giftVoucher.populateVoucherNameDropDown = function() {
	var templates = UI_giftVoucher.templateList;
	var validity;
	var namePrevious = new Array();
	var selectedCurrency = $('#currencySrch option:selected').text();
	var amountSelected = $('#amountSrch option:selected').text();
	var selectedValidity = $('#validitySrch option:selected').text();
	switch (selectedValidity) {
	case "30 Days":
		validity = 30
		break;
	case "180 Days":
		validity = 180
		break;
	case "90 Days":
		validity = 90
		break;
	case "1 Year":
		validity = 365
		break;
	default:
		validity = 30
	}
	var voucherNames = "";
	voucherNames += "<option value = '' ></option>";
	if (templates != null) {
		for (i = 0; i < templates.length; i++) {
			if (templates[i].voucherValidity == validity && templates[i].currencyCode == selectedCurrency && templates[i].amountInLocal == amountSelected) {
				if($.inArray(templates[i].voucherName, namePrevious)==-1){
					namePrevious.push(templates[i].voucherName);
					voucherNames += "<option value = '"
						+ JSON.stringify(templates[i]) + "' >"
						+ templates[i].voucherName + "</option>";
				}
			}
		}
	}
	$('#voucherName').empty();
	$('#voucherName').append(voucherNames);
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
}

UI_giftVoucher.previousStep = function() {
	$("#tabSecond").removeClass('stepsItem select');
	$("#tabSecond").addClass('stepsItem');
	$("#tabFirst").removeClass('stepsItem selected last-setected');
	$("#tabFirst").addClass('stepsItem select');
	$("#tableModSearch").slideDown(1000);
	$("#tableModDetails").slideDown(1000);
	$("#btnSet1").show();
	$("#divPayment").hide();
	$("#voucherSummery").hide();
	$("#paymentButtonset").hide();
	$("#cardDetailPannel").hide();
	$("#paymentInfoPanel").hide();
	UI_giftVoucher.isFirstClick = false;
	$("#ccTaxVoucher").hide();
	$("#voucherListSummery").slideDown(1000);
	$("#btnPurchase").hide();
	UI_giftVoucher.globaltotal=0;
	UI_giftVoucher.voucherTnxFeeMap = new Array();
}

UI_giftVoucher.clearCardDetails = function() {
	$("#payByCreditCardPanel").show();
	$("#cardHoldersName").val("");
	$("#cardNumber").val("");
	$("#expiryMonth").val("");
	$("#expiryYear").val("");
	$("#cardCVV").val("");
}

UI_giftVoucher.addVouchersToCart = function () {
	if(UI_giftVoucher.validateAdd()) {
		UI_giftVoucher.isBulkIssue = true;
		var number = $("#quantity").val();
		var template = JSON.parse($("#voucherName").val());
		var validFrom = $("#validFrom").val();		
		var validTo = $("#validTo").val();
		UI_giftVoucher.voucherListToBuy[globalBtnId] = [number,template,validFrom,validTo];	
		var tableId = "voucherListDetails_"+globalBtnId; 
		varbtnId = "btnRemove_"+globalBtnId;
		
		var genericHTML = "<tr><td class='pnlTop'></td></tr><tr><td style='height: auto;' class='pnlBottom add-padding alignLeft'><table id='"+tableId+"' cellspacing='1' cellpadding='1' border='0'><tr>"+
                          "<td width='50%'><label id='lblV'>Gift Voucher :</label>"+
                          "<label id='vName'>" +template.voucherName + "</label>"+
                          "</td><td width='50%'></td></tr><tr><td width='50%'><label id='lblVAmount'>Amount :</label>"+
                          "<label id='vAmountLocal'>"+ template.amountInLocal +" "+"</label>"+
                          "<label id='vCurrency'>"+template.currencyCode+"</label>"+
                          "</td><td width='50%'></td></tr><tr><td width='50%'><label id='lblVQuantity'>Quantity :</label>"+
                          "<label id='vQuantity'>"+number+"</label>"+
                          "</td><td width='50%'></td></tr><tr><td width='50%'><label id='lblVFrom'>Valid From :</label>"+
                          "<label id='vValidFrom'>"+$("#validFrom").val()+"</label>"+
                          "</td></tr><tr><td width='50%'><label id='lblVTo'>Valid To :</label>" +
                          "<label id='vValidTo'>"+$("#validTo").val()+"</label>"+
                          "</td><td align='right'>"+
                          "<input type='button' class='Button' id='"+varbtnId+"' value='Remove' autocomplete='off' onclick='removeVoucher(this.id)'></td></tr><tr><td width='100%' class='summarySperator'></td><td width='100%' class='summarySperator'></td></tr>"      
        
        $("#vDetails").append(genericHTML); 
		$("#btnAdd").show();
		$("#btnAddToBuy").hide();
		$("#voucherListSummery").show();
        globalBtnId++;
        UI_giftVoucher.disableVoucherDetails();
        UI_giftVoucher.setLables();
	}	
}

UI_giftVoucher.validateAdd = function() {
	var temp = $("#voucherName").text();
	if(temp==""){
		jAlert(UI_giftVoucher.errorInfo["selectVoucher"], 'Alert');
		return false;
	} else {
		return true;
	}
}

UI_giftVoucher.clearVoucherDetails = function(){
	$('#voucherName').val("");
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amountInLocal").val("");
	$("#currencyCode").val("");
	$('#amountSrch').val("");
	$("#validitySrch").val("");
	$("#currencySrch").val("");
	$("#quantity").val("1");
	$('#currencySrch').empty();
	$("#voucherName").empty();
	$("#validitySrch").empty();
}

UI_giftVoucher.disableVoucherDetails = function(){
	UI_giftVoucher.formDissable();
	$("#amountSrch").prop('disabled', true);
	$("#currencySrch").prop('disabled', true);
	$("#validitySrch").prop('disabled', true);
	$("#quantity").prop('disabled', true);
	$("#voucherName").prop('disabled', true);
}

UI_giftVoucher.enableVoucherDetails = function(){
	$("#amountSrch").prop('disabled', false);
	$("#currencySrch").prop('disabled', false);
	$("#validitySrch").prop('disabled', false);
	$("#quantity").prop('disabled', false);
	$("#voucherName").prop('disabled', false);
}

removeVoucher = function(id){
	var idNum = id.split("_")[1];
	var tableId = "voucherListDetails_"+idNum;
	$("#"+tableId).empty();
	delete UI_giftVoucher.voucherListToBuy[idNum];
	var lengthArr = Object.keys(UI_giftVoucher.voucherListToBuy).length;
	if(lengthArr==0){
		$("#voucherListSummery").hide();
	}
}

UI_giftVoucher.loadErrorPage = function(){
	hideProgress();
	UI_commonSystem.loadErrorPage({
		messageTxt : UI_giftVoucher.errorInfo["errorResponse"]
	});
}
