/** Modify Search
 * 
 * @author Pradeep Karunanayake
 *  
 */

function UI_ModifySearch(){};
 
;(function(UI_ModifySearch) {	

	UI_ModifySearch.errorInfo = "";	
	UI_ModifySearch.strFromHD = "";
	UI_ModifySearch.strToHD = "";
	UI_ModifySearch.strDash = "----------------------";
	UI_ModifySearch.isLoaded = false;	
	UI_ModifySearch.msgMaxAdultChildCount = 0;
	UI_ModifySearch.ondDataMap = [];
	UI_ModifySearch.originList = [];	
	UI_ModifySearch.hubDesc = "";
	UI_ModifySearch.currencyList = [];
	UI_ModifySearch.isModifySegment = false;
	UI_ModifySearch.isSearchLoaded = false;
	UI_ModifySearch.isPromoCodeEnabled = false;
	UI_ModifySearch.hubTimeDetailJSON = [];
	UI_ModifySearch.surfaceSegmentConfigs = {
			sufaceSegmentAppendStr : "--:",
			surfSegCodeSplitter : ":"	
	}
	
	$(document).ready(function(){
		UI_ModifySearch.ready();		
	});
	
	UI_ModifySearch.ready = function() {
		var noMonth = globalConfig.noOfMonthsinCalendaraPopup;
		$("#departureDateBox").datepicker({regional: SYS_IBECommonParam.locale ,
			numberOfMonths: noMonth,
			dateFormat: "d M yy",
			altFormat: "dd/mm/yy",
			altField: "#msDepartureDate",
			minDate: +0,
			//maxDate: '+1M +10D',
			showOn: 'both',
			buttonImage: globalConfig.calendaImagePath,
			buttonImageOnly: true,
			showButtonPanel: true,
			onSelect: function(dateStr) {
				var min = $(this).datepicker('getDate') || new Date(); // Selected date or today if none
				 $('#returnDateBox').datepicker('option', {minDate: min});
                 $("#msDepartureDate").val(dateToGregorian($("#msDepartureDate").val(),SYS_IBECommonParam.locale));
			}
		});

		$("#returnDateBox").datepicker({regional: SYS_IBECommonParam.locale ,
			numberOfMonths: noMonth,
			dateFormat: "d M yy",
			altFormat: "dd/mm/yy",
			altField: "#msReturnDate",
			minDate: +0,
			//maxDate: '+1M +10D',
			showOn: 'both',
			buttonImage: globalConfig.calendaImagePath,
			buttonImageOnly: true,
			showButtonPanel: true,
            onSelect: function(){
                $("#msReturnDate").val(dateToGregorian($("#msReturnDate").val(),SYS_IBECommonParam.locale));
            }
		});
		
		UI_ModifySearch.hub = UI_Top.holder().GLOBALS.hub;
		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_ModifySearch.ondListLoaded()){
			UI_ModifySearch.initDynamicOndList();
		}
		
		if (UI_ModifySearch.isLoaded != true) {
			$("#selFromLoc").change(function(){UI_ModifySearch.selFromLocOnChange();});
			$("#selToLoc").change(function(){UI_ModifySearch.selToLocOnChange();});
			$("input[name='tripDirection']").click(function(){UI_ModifySearch.chkReturnTripClick();});
			$("#selAdults").change(function(){UI_ModifySearch.selAdultsOnChange();});			
			$("#btnSearch").click(function(){UI_ModifySearch.flightSearchBtnOnClick();});			
			$("#btnModSeach").click(function(){
				$("#divModSeach").find("#lblChildren").show();
				$("#divModSeach").show();
				$("#tableModSearch").css("width", "100%");
				$(this).hide();
				if(typeof UI_AvailabilitySearch != 'undefined'){
					UI_AvailabilitySearch.rePositionBanner();
				}
				if(UI_Top.holder().GLOBALS.enableStopOverInIBE == false){
					$("#trCheckStopOver").hide();
				}
				if(UI_ModifySearch.isSearchLoaded == false)
					UI_ModifySearch.loadModifySearchData();
			});
		}
		
		UI_ModifySearch.isPromoCodeEnabled = UI_Top.holder().GLOBALS.promoCodeEnabled;
		if(!UI_ModifySearch.isModifySegment && UI_ModifySearch.isPromoCodeEnabled){		
			$("#divPromoCode").show();
			$("#promoCode").alphaNumeric();
			$("#bankIdNo").numeric({allowDecimal:false});
		} else {
			$("#divPromoCode").hide();
		}
		
		if(UI_Top.holder().GLOBALS.enableStopOverInIBE == false){
			$("#trCheckStopOver").hide();
		}
		UI_ModifySearch.isLoaded = true;
	}
	
	UI_ModifySearch.loadModifySearchData = function() {
		var url = "modifySearch.action";
		var data = {};
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: UI_ModifySearch.loadModifySearchDataProcess, error:UI_commonSystem.setErrorStatus, cache : false, beforeSubmit:UI_commonSystem.loadingProgress});
		return false;
	}
	
	UI_ModifySearch.loadModifySearchDataProcess = function(response) {
		if (response == null || response == "" || response == undefined) {
			//Load Network connection failure error
			UI_commonSystem.setErrorStatus();
			return false;
		}			
		if (response.success) {			
			UI_ModifySearch.pageInit(response);
			UI_ModifySearch.setSearchParameters();
			UI_ModifySearch.isSearchLoaded =  true;
		} else {
			UI_commonSystem.loadErrorPage({messageTxt:response.messageTxt});
		}
		UI_commonSystem.loadingCompleted();
	}
	
	UI_ModifySearch.setSearchParameters = function() {		
		$("#selFromLoc").val($("#resFromAirport").val());
		UI_ModifySearch.selFromLocOnChange();
		$("#selToLoc").val($("#resToAirport").val());		
		$("#selAdults").val($("#resAdultCount").val());
		$("#selChild").val($("#resChildCount").val());
		UI_ModifySearch.selAdultsOnChange();
		UI_ModifySearch.setDefaultSearchOption($("#selFromLoc").val(), $("#selToLoc").val());
		$("#selInfants").val($("#resInfantCount").val());		
		var isReturn = $("#resReturnFlag").val();
		var stringDateDepArr = $("#resDepartureDate").val().split("/");
		var stringDateRetpArr = $("#resReturnDate").val().split("/");
		if (isReturn == "true" || isReturn == true) {
			$("#roundTrip").attr("checked", true);
		} else {
			$("#onewayTrip").attr("checked", true);
			$("#returnDateBox").datepicker("disable");
			$("input[name='inboundStopOver']").val(0).disable();
		}
		if (stringDateDepArr.length > 2) {
			$("#departureDateBox").datepicker( "setDate" , new Date(stringDateDepArr[2], (stringDateDepArr[1] -1),stringDateDepArr[0]));				
		}
		
		if (stringDateRetpArr.length > 2) {
			$("#returnDateBox").datepicker( "setDate" , new Date(stringDateRetpArr[2], (stringDateRetpArr[1] -1),stringDateRetpArr[0]));	
		}
		$("#msDepartureDate").val($("#resDepartureDate").val());
		$("#msReturnDate").val($("#resReturnDate").val());	
		$("#selCOS").val($("#resClassOfService").val());	
		
		$("#promoCode").val($("#resPromoCode").val());
		$("#bankIdNo").val($("#resBankIdNo").val());
		
		if(UI_Top.holder().GLOBALS.enableStopOverInIBE == false){
			$("#trCheckStopOver").hide();
		}
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && UI_Top.holder().GLOBALS.enableStopOverInIBE == true &&
				UI_ModifySearch.ondListLoaded() && UI_Top.holder().GLOBALS.userDefineTransitTimeEnabled){
				UI_ModifySearch.setTransitTime();
			}
	}
	
	UI_ModifySearch.setTransitTime = function(){
		var habData = $.trim($("#resHubTimeDetails").val());
		if (habData!=""){
			$("#chkStopOver").prop("checked", true);
			var tHubData = habData.split(",");
			$.each(tHubData, function(){
				if (this!=""){
					var tempArra = this.split("_");
					var tempModel = {};
					tempModel.hubCode = tempArra[0];
					tempModel.outboundStopOver = tempArra[1];
					tempModel.inboundStopOver = tempArra[2];
					UI_ModifySearch.hubTimeDetailJSON[UI_ModifySearch.hubTimeDetailJSON.length - 1] = tempModel;
				}
			});
			chkStopOverAdd();
		}else{
			$("#chkStopOver").prop("checked", false);
		}
	}
	
	UI_ModifySearch.pageInit = function(response) {	
		if (response != null) {
			UI_ModifySearch.strFromHD = response.jsonLabel.lblWhereFrom;
			UI_ModifySearch.strToHD = response.jsonLabel.lblWhereTo;
			UI_ModifySearch.msgMaxAdultChildCount = response.jsonLabel.msgMaxAdultChildCount;
			UI_ModifySearch.errorInfo = response.errorInfo;			
			UI_ModifySearch.fillDropDown(response.airportList);
			//UI_ModifySearch.setDefaultFareType();
			UI_ModifySearch.flightSearchPopulateData();
			$("#selCOS").fillDropDown({dataArray:response.activeCos, keyIndex:0, valueIndex:1, firstEmpty:false});

		}
	}
	
	UI_ModifySearch.fillDropDown = function(airportList) {		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_ModifySearch.ondListLoaded()) {			
			var fromAirpotList = UI_ModifySearch.setAirportData(UI_ModifySearch.setAirportListHead('from') , UI_ModifySearch.originList);
			//to locations should be set only to selects
			UI_ModifySearch.fillFromToAirportDD(fromAirpotList,UI_ModifySearch.toRenameAirpots(fromAirpotList));			
		} else {			
			var fromAirpotList = UI_ModifySearch.setAirportData(UI_ModifySearch.setAirportListHead('from') , airportList);	
			UI_ModifySearch.fillFromToAirportDD(fromAirpotList,UI_ModifySearch.toRenameAirpots(fromAirpotList));			
		}
		
	}
	
	UI_ModifySearch.fillFromToAirportDD = function (fromAirpotList, toAirportList){
		if(UI_ModifySearch.previousAirportData.isEmpty()){
			UI_ModifySearch.previousAirportData.fromAirportsPrevious = fromAirpotList;
			UI_ModifySearch.previousAirportData.toAirportsPrevious = toAirportList;
		}else {
			//if not assuming modifysegment
			UI_ModifySearch.previousAirportData.fromAirportGroundOnlyData = fromAirpotList;
			UI_ModifySearch.previousAirportData.toAirportGroundOnlyData = toAirportList;
		}
		$("#selFromLoc").children().remove();
		$("#selToLoc").children().remove();
		$("#selFromLoc").fillDropDown({dataArray:fromAirpotList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selToLoc").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});		
	}
	
	UI_ModifySearch.mergeKeyValueData = function(data) {
		var temp = "";
		if (data != null) {
			for(var i = 0; i < data.length; i++) {
				temp = data[i][1];			
				delete data[i][1];
				data[i][1] = data[i][0] + " - " + temp;
			}
		}
		
		return data;
	}
	
	UI_ModifySearch.setAirportListHead = function(params) {
		var data = [];
		data[0] = [];
		data[0][0]= "";		
		
		if(params == 'from'){
			data[0][1] = UI_ModifySearch.strFromHD; 
		} else {
			data[0][1] = UI_ModifySearch.strToHD; 
		}
		
		return  data;
	}
	
	UI_ModifySearch.setAirportData =  function(head, data) {		
		return head.concat(data);
	}
	
	UI_ModifySearch.toRenameAirpots = function(data) {	
		var toArray = $.extend(true, [], data);
		toArray[0][1] = UI_ModifySearch.strToHD;
		return toArray;
	}
	
	UI_ModifySearch.createPaxCategoryList = function(paxList) {
		var data = [];
		for (var i = 0; i < paxList.length; i++) {			
			data[i] = [];
			data[i][0]= paxList[i].paxCategoryCode;		
			data[i][1] = paxList[i].paxCategoryDesc;	
		}
		return data;
	}
	
	UI_ModifySearch.createFareTypeList = function(fareList) {
		var data = [];
		for (var i = 0; i < fareList.length; i++) {			
			data[i] = [];
			data[i][0]= fareList[i].fareCategoryCode;		
			data[i][1] = fareList[i].fareCategoryDesc;	
		}
		return data;
	}
	
	/**
	 * Set Default Fare Type
	 */
	UI_ModifySearch.setDefaultFareType = function() {
		var arrFareCat = UI_ModifySearch.arrFareCat;
		for(var strFare = 0; strFare < arrFareCat.length; strFare++) {
			if((arrFareCat[strFare].status).toUpperCase() == 'ACT'){
				UI_ModifySearch.defaultFareType = arrFareCat[strFare].fareCategoryCode;
			}
		}
		if(UI_ModifySearch.defaultFareType ==""){
			UI_ModifySearch.defaultFareType ="A";
		}
	}
	
	UI_ModifySearch.flightSearchPopulateData = function() {
		
		var arrAdults = new Array();
		var arrChild  = new Array();
		
		arrChild[0] = new Array();
		arrChild[0][0] = "0";
		for (var i = 1 ; i <= UI_Top.holder().GLOBALS.flightSeachMaxAdult ; i++){
			arrAdults[i - 1] = new Array();
			arrAdults[i - 1][0] = i;
			
			arrChild[i] = new Array();
			arrChild[i][0] = i;
		}
		$("#selAdults").fillDropDown({dataArray:arrAdults, keyIndex:0, valueIndex:0, firstEmpty:false});
		$("#selChild").fillDropDown({dataArray:arrChild, keyIndex:0, valueIndex:0, firstEmpty:false});	
		
		//UI_ModifySearch.defaultDataLoad();		
	}
	
	UI_ModifySearch.defaultDataLoad = function() {
			var dtDepDate = addDays(UI_Top.holder().dtSysDate, Number(UI_Top.holder().GLOBALS.depDateDiffrence)); 
			var temDepDate = dtDepDate.getDate() + "/" + (dtDepDate.getMonth() + 1) + "/" + dtDepDate.getFullYear();
			var strDDate = dateChk(temDepDate);
			strSeleDDay = dtDepDate.getDate();	
	
			var dtRetuDate = "";
			var strRDate = "";
			strSeleRDay = "  /  /    ";
			if (String(UI_Top.holder().GLOBALS.retDateDiffrence) != ""){
				var dtRetuDate = addDays(UI_Top.holder().dtSysDate, Number(UI_Top.holder().GLOBALS.retDateDiffrence));
				var strRDate = dateChk(dtRetuDate.getDate() + "/" + (dtRetuDate.getMonth() + 1) + "/" + dtRetuDate.getFullYear());
				strSeleRDay = dtRetuDate.getDate();				
				//$("#roundTrip").attr("checked", true);				
			}
			$("#departureDateBox").datepicker( "setDate" , dtDepDate); 
			$("#returnDateBox").datepicker( "setDate" , dtDepDate); 			

			UI_ModifySearch.selAdultsOnChange();
			UI_ModifySearch.chkReturnTripClick();
	}
	
	
	UI_ModifySearch.chkReturnTripClick = function() { 
		if ($("#roundTrip").is(':checked')) {			
			//$("#resReturnFlag").val(true);
			$("#returnDateBox").datepicker("enable");	
			$("input[name='inboundStopOver']").enable();
		}else{			
			//$("#resReturnFlag").val(false);
			$("#returnDateBox").val("");
			$("#returnDateBox").datepicker("disable");
			$("#msReturnDate").val("");	
			$("input[name='inboundStopOver']").val(0).disable();
		}
	}
	
	UI_ModifySearch.selToLocOnChange = function() {
		if($.trim($("#selToLoc").val()).indexOf(UI_ModifySearch.surfaceSegmentConfigs.surfSegCodeSplitter) != -1){
			var splited = $("#selToLoc").val().split(UI_ModifySearch.surfaceSegmentConfigs.surfSegCodeSplitter);
			$("#hndToAirport").val(splited[0]);
			$("#hdnToAirportSubStation").val(splited[1]);
		}else {
			if ($("#selToLoc :selected").text() == UI_ModifySearch.strDash ||
					$("#selToLoc").val() == "") {
				$("#selToLoc option:first").attr("selected",true);
			} else{
				$("#hndToAirport").val($("#selToLoc :selected").text());
			}
		}
		UI_ModifySearch.setDefaultSearchOption($("#selFromLoc").val(), $("#selToLoc").val());
	}
	
	
	
	UI_ModifySearch.selFromLocOnChange = function() {
		
		
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled &&
				UI_ModifySearch.ondListLoaded() && UI_Top.holder().GLOBALS.enableStopOverInIBE == true){
			
			if ($("#selFromLoc :selected").text() == UI_ModifySearch.strDash ||
					$("#selFromLoc").val() == ""){
				$("#selFromLoc option:first").attr("selected",true);
			} else{			
				$("#hndFromAirport").val($("#selFromLoc :selected").text());
				//Set new list
				var currentTo = $("#selToLoc").val();
				var currentFrom = $("#selFromLoc").val();
				$("#selToLoc").children().remove();
				
				var toAirportList = [];				
				toAirportList = UI_ModifySearch.setAirportData(UI_ModifySearch.setAirportListHead('to') , UI_ModifySearch.ondDataMap[currentFrom]);	
				
				$("#selToLoc").fillDropDown({dataArray:toAirportList, keyIndex:0, valueIndex:1, firstEmpty:false});		
				
			}	

		} else {
			if($.trim($("#selFromLoc").val()).indexOf(UI_ModifySearch.surfaceSegmentConfigs.surfSegCodeSplitter) != -1){
				var splited = $("#selFromLoc").val().split(UI_ModifySearch.surfaceSegmentConfigs.surfSegCodeSplitter);
				$("#hndFromAirport").val(splited[0]);
				$("#hdnFromAirportSubStation").val(splited[1]);
			}else {
				if ($("#selFromLoc :selected").text() == UI_ModifySearch.strDash ||
						$("#selFromLoc").val() == ""){
					$("#selFromLoc option:first").attr("selected",true);
				} else{			
					$("#hndFromAirport").val($("#selFromLoc :selected").text());
				}	
			}
		}	
	
	}
	
	UI_ModifySearch.selAdultsOnChange = function(){
		var arrInfants = new Array();
		var arrChild  = new Array();
		var intAdults = Number($("#selAdults").val());
		var strCInfants = $("#selInfants").val();
		var intChilds = $("#selChild").val();
		
		for (var i = 0 ; i <= intAdults ; i++){
			arrInfants[i] = new Array();
			arrInfants[i][0] = i;
		}
		$("#selInfants").empty();
		$("#selInfants").fillDropDown({dataArray:arrInfants, keyIndex:0, valueIndex:0, firstEmpty:false});
		
		if (strCInfants > 0) {
			strCInfants = (intAdults >= strCInfants) ? strCInfants : intAdults;
			$("#selInfants").val(strCInfants);
		}
		
		if(UI_ModifySearch.isModifySegment == false) {
			// Child Count Change According to Adult Count.		
			var numOfChild = UI_Top.holder().GLOBALS.flightSeachMaxAdult - intAdults;
			for (var i = 0 ; i <= numOfChild ; i++){
				arrChild[i] = new Array();
				arrChild[i][0] = i;
			}
			$("#selChild").empty();
			$("#selChild").fillDropDown({dataArray:arrChild, keyIndex:0, valueIndex:0, firstEmpty:false});	
			
			if (intChilds > 0) {			
				if (numOfChild < intChilds) {	
					jAlert(UI_ModifySearch.msgMaxAdultChildCount + " " + UI_Top.holder().GLOBALS.flightSeachMaxAdult + ".");
					intChilds = numOfChild;
				} 
				$("#selChild").val(intChilds);
			}
		}
		
	}	
	
	UI_ModifySearch.flightSearchBtnOnClick = function(){
		//Enable Exit pop-up explicitly once user modify the search
        if (typeof UI_ExitPopup != 'undefined'){
            UI_ExitPopup.enableExitpopupExplicitly();
        }

		if (UI_ModifySearch.clientValidateFS()) {
			UI_ModifySearch.synchronizeSearchData();
			if ($("#PgContainer").length > 0) {
				UI_Container.exitPrevious();
			} else {
				UI_commonSystem.loadingProgress();
				$("#tempDiscountBanner").remove();
				$("#tempPromoBanner").remove();
				UI_AvailabilitySearch.buildCalendarTabs();
				UI_AvailabilitySearch.setCalendarDateVars(); 
				UI_AvailabilitySearch.loadAvailabilitySearchData();
			}
			setTimeout(function(){
				$("#divModSeach").hide();
				$("#btnModSeach").show();
				UI_AvailabilitySearch.rePositionBanner();
			},2000);

		}	
	}
	
	UI_ModifySearch.synchronizeSearchData = function() {
		//This is doing aftre OND selection completion time
		//UI_ModifySearch.setDefaultSearchOption($("#selFromLoc").val(), $("#selToLoc").val());
		var isReturn = false;
		$("#resFromAirport").val($("#selFromLoc").val());
		$("#resToAirport").val($("#selToLoc").val());
		$("#resAdultCount").val($("#selAdults").val());
		$("#resChildCount").val($("#selChild").val());		
		$("#resInfantCount").val($("#selInfants").val());	
		if ($("#roundTrip").is(":checked")) {
			isReturn = true;
		}
		$("#resReturnFlag").val(isReturn);
		$("#resDepartureDate").val($("#msDepartureDate").val());
		$("#resReturnDate").val($("#msReturnDate").val());
		$('#resFromAirportName').val($("#selFromLoc :selected").text());
		$('#resToAirportName').val($("#selToLoc :selected").text());
		$("#resPaxType").val($("#selPaxType").val());
		
		/**
		 * Set Date Variance parameters related
		 */
		strODate = $("#resDepartureDate").val();
		strRDate = $("#resReturnDate").val();
			
		$("#resFareQuoteLogicalCCSelection").val("");
		if(typeof UI_AvailabilitySearch != 'undefined'){			
			$.each(UI_AvailabilitySearch.ondWiseFlexiSelected, function(key, value){
				UI_AvailabilitySearch.ondWiseFlexiSelected[key] = false;
			});
			$("#resOndQuoteFlexi").val($.toJSON(UI_AvailabilitySearch.ondWiseFlexiSelected));
		}
		
		$("#resClassOfService").val($("#selCOS").val());
		
		if(!UI_ModifySearch.isModifySegment && UI_ModifySearch.isPromoCodeEnabled){			
			$("#resPromoCode").val($("#promoCode").val());
			$("#resBankIdNo").val($("#bankIdNo").val());
		}
		
		//update the transit time reservation param
		var strTransit = "";
		if ( UI_Top.holder().GLOBALS.userDefineTransitTimeEnabled && $("#chkStopOver").prop("checked")){
			$.each(UI_ModifySearch.hubTimeDetailJSON, function(i,j){
				if (UI_ModifySearch.hubTimeDetailJSON.length-1==i){
					var obStopOver=UI_ModifySearch.getStopOverValue($("#outboundStopOver_"+i).val());
					var ibStopOver=UI_ModifySearch.getStopOverValue($("#inboundStopOver_"+i).val());
					 
					strTransit+= $("#lblHubCode_"+i).attr("role") + "_" + obStopOver+ "_" + ibStopOver;
				}else{
					var obStopOver=UI_ModifySearch.getStopOverValue($("#outboundStopOver_"+i).val());
					var ibStopOver=UI_ModifySearch.getStopOverValue($("#inboundStopOver_"+i).val());
					strTransit+= $("#lblHubCode_"+i).attr("role") + "_" + obStopOver + "_" + ibStopOver+ ",";
				}
			});
		}
		$("#resHubTimeDetails").val(strTransit);
	}
	UI_ModifySearch.getStopOverValue = function (value){
		 
		if(value === ""){
			return 0;
		}else{
			return value;
		}
	} 
	UI_ModifySearch.setDefaultSearchOption = function (from, to){
		$("#trCheckStopOver").hide();//to hide after ond selection
		UI_ModifySearch.hubTimeDetailJSON = [];
		if(UI_Top.holder().GLOBALS.dynamicOndListEnabled && UI_Top.holder().GLOBALS.enableStopOverInIBE == true &&
				UI_ModifySearch.ondListLoaded()){	
			var fromIndex = -1;
			var toIndex = -1;
			//get from, to indexes
			var airportsLength = airports.length;
			for(var i=0;i<airportsLength;i++){
				if(from == airports[i].code){
					fromIndex = i;
				} else if(to == airports[i].code) {
					toIndex = i;
				}
				
				if(fromIndex != -1 && toIndex != -1){
					break;
				}
			}
			
			if(fromIndex != -1){			
				var availDestOptLength = origins[fromIndex].length;
				for(var i=0;i<availDestOptLength;i++){
					var destOptArr = origins[fromIndex][i];
					if(destOptArr[0] == toIndex){
						var isInt = false;
						var optCarriersArr = destOptArr[3].split(",");
						for(var j=0;j<optCarriersArr.length;j++){
							if(optCarriersArr[j] != GLOBALS.defaultCarrierCode){
								isInt = true;
								break;
							}
						}
						
						if(isInt){
							$('#resSearchSystem').val('INT');
						} else {
							$('#resSearchSystem').val('AA');
						}
						
						if(UI_Top.holder().GLOBALS.userDefineTransitTimeEnabled){
							//prepare HUB stopover time option 
							var hubCodeList = destOptArr[5];
							if(hubCodeList != null && hubCodeList.length > 0){
								for(var k = 0; k < hubCodeList.length; k++){
									var hubCodes = hubCodeList[k];
									if(hubCodes != null && hubCodes.length > 0){
										for(var l = 0; l < hubCodes.length ; l++){
											var tempModel = {};
											tempModel.hubCode = hubCodes[l];
											tempModel.outboundStopOver = 0;
											tempModel.inboundStopOver = 0;
											UI_ModifySearch.hubTimeDetailJSON[UI_ModifySearch.hubTimeDetailJSON.length] = tempModel;
											//Calling to the chkStopOverAdd(); to display the checkbox	
											chkStopOverAdd();
										}
										
									}
								}
							}
						}
						break;
					}
				}
			} else {			
				$('#resSearchSystem').val('INT');
			}
		} else {
			$('#resSearchSystem').val('AA');
		}
		
	}
	
	UI_ModifySearch.ondListLoaded = function(){
		if(typeof(airports)=="undefined" || typeof(origins)=="undefined"){
			return false;
		} else {
			return true;
		}
	}
	
	UI_ModifySearch.clientValidateFS = function() {
		var arrError = UI_ModifySearch.errorInfo;
		if ($("#selFromLoc").val() == "") {
			jAlert(arrError["ERR001"]);		
			$("#selFromLoc").focus();		
			return false;
		}
		
		if ($("#selToLoc").val() == ""){
			jAlert(arrError["ERR002"]);
			$("#selToLoc").focus();
			return false;
		}		
		
		if ($("#selFromLoc").val() == $("#selToLoc").val()){
			jAlert(arrError["ERR003"]);
			$("#selToLoc").focus();			
			return false;
		}
		
		if ($.trim($("#msDepartureDate").val()) == ""){
			jAlert(arrError["ERR007"]); 	
			$("#departureDateBox").focus();		
			return false;
		}else{
			if (!CheckDates(UI_Top.holder().strSysDate, dateToGregorian($.trim($("#msDepartureDate").val()),SYS_IBECommonParam.locale))){
				jAlert(arrError["ERR009"] + dateFromGregorian(UI_Top.holder().strSysDate,SYS_IBECommonParam.locale) + ".");
				$("#departureDateBox").focus();		
				return false;
			}			
		}
		
		if ($("#roundTrip").is(':checked')){
			if ($("#msReturnDate").val() == ""){
				jAlert(arrError["ERR008"]); 	
				$("#returnDateBox").focus();		
				return false;
			}
		
			if (!CheckDates($.trim($("#msDepartureDate").val()), $.trim($("#msReturnDate").val()))){
				jAlert(arrError["ERR004"]);
				$("#returnDateBox").focus();	
				return false;
			}
		}	
		
		if(!UI_ModifySearch.isModifySegment && UI_ModifySearch.isPromoCodeEnabled){			
			var bankIdNo = $("#bankIdNo").val();
			if(bankIdNo != '' && bankIdNo.length < 6){
				jAlert(arrError['ERR096']);
				$("#bankIdNo").focus();
				return false;
			}
		}
		
		return true;
	}
	
	
	
	/***
	 * Use fromAirportsPrevious and toAirportsPrevious to hold previous airports if airport drop down
	 * data changes and we want to restore the previous values in case of button click.
	 */
	UI_ModifySearch.previousAirportData = {
		fromAirportsPrevious : null,toAirportsPrevious : null,
		fromAirportGroundOnlyData: null, toAirportGroundOnlyData:null,
		isOnADataRefresh : false,
		clear : function (){
			this.fromAirportsPrevious = null;
			this.toAirportsPrevious = null;
			this.isOnADataRefresh = false;
		},
		isEmpty : function (){
			if(this.fromAirportsPrevious == null){
				return true;
			}
			return false;
		}
	}
		
	/**
	 * Data lists to be used when dynamic ond lists are enabled will be populated 
	 * by this method
	 */
	UI_ModifySearch.initDynamicOndList = function() {

		// Populate OnD data structure only for ond's which has current
		// operating carrier

		var defaultCarrier = UI_Top.holder().GLOBALS.defaultCarrierCode;
		var lang = UI_Top.holder().GLOBALS.sysLanguage;

		if ($("#locale").val() != null && $("#locale").val() != "") {
			lang = $("#locale").val();
		}

		for ( var originIndex in origins) {

			var tempDestArray = [];
			for ( var destIndex in origins[originIndex]) {
				// check whether operating carrier exists
				var curDest = origins[originIndex][destIndex];
				
				if(curDest[0] != null){							
					var currentSize = tempDestArray.length;
					tempDestArray[currentSize] = [];
					tempDestArray[currentSize][0] = airports[curDest[0]]['code'];
					tempDestArray[currentSize][1] = (airports[curDest[0]][lang] != null ? airports[curDest[0]][lang]
							: airports[curDest[0]][UI_Top.holder().GLOBALS.sysLanguage]);
					tempDestArray[currentSize][2] = currencies[curDest[2]]['code'];
				}			
				
			}

			// Add entry if at least one destination is there to final ond map
			if (tempDestArray.length > 0) {
		
				tempDestArray.sort(UI_ModifySearch.customSort);
				UI_ModifySearch.ondDataMap[airports[originIndex]['code']] = tempDestArray;

				var currentOriginSize = UI_ModifySearch.originList.length;
				UI_ModifySearch.originList[currentOriginSize] = [];
				UI_ModifySearch.originList[currentOriginSize][0] = airports[originIndex]['code'];
				UI_ModifySearch.originList[currentOriginSize][1] = (airports[originIndex][lang] != null ? airports[originIndex][lang]
						: airports[originIndex][UI_Top.holder().GLOBALS.sysLanguage]);

				if (UI_ModifySearch.hubDesc == ""
						&& UI_ModifySearch.hub == airports[originIndex]['code']) {
					UI_ModifySearch.hubDesc = (airports[originIndex][lang] != null ? airports[originIndex][lang]
							: airports[originIndex][UI_Top.holder().GLOBALS.sysLanguage]);
				}
				
				UI_ModifySearch.originList.sort(UI_ModifySearch.customSort);

			}
			
			
		}
		
		for ( var i = 0 ; i < currencies.length ; i++) {
				
			UI_ModifySearch.currencyList[i] = [];
			UI_ModifySearch.currencyList[i][0] = currencies[i]['code'];
			UI_ModifySearch.currencyList[i][1] = (currencies[i][lang] != null ? currencies[i][lang]
					: currencies[i][UI_Top.holder().GLOBALS.sysLanguage]);
		
		}
		
		UI_ModifySearch.currencyList.sort(UI_ModifySearch.customSort);
			
	}
	
	/**
	 * Custom sort to sort 2 dimensional on index 1
	 */
	
	UI_ModifySearch.customSort = function(a,b) {

		a = a[1];
		b = b[1];
		return a == b ? 0 : (a < b ? -1 : 1)

	}
	
	//TransitTime
	function chkStopOverAdd(){
		$("#trCheckStopOver").slideDown();
		UI_ModifySearch.showStopOver();
	}
	
	//return the hub name from airport list as necessary for the ibe
	getHubName = function(code){
		var hName = code;
		for (var i = 0;i<airports.length;i++){
			if (code == airports[i].code){
				hName = airports[i].en;
				break;
			}
		}
		return hName;
	}

	UI_ModifySearch.showStopOver = function(){
		$(".stopOverTimeInner").empty();
		$.each(UI_ModifySearch.hubTimeDetailJSON, function(i,j){
			var input = $("<div><label id='lblHubCode_"+i+"' role='"+j.hubCode+"'>" + getHubName(j.hubCode) + "</label> <input type='text' maxlength='2' class='aa-input' style='width:30px' id='outboundStopOver_"+i+"' name='outboundStopOver' value='"+j.outboundStopOver+"' maxlength='2' /> <label id='lblOutBoundDays'>Outbound Days</label> " +
					"<input type='text' maxlength='2' class='aa-input' style='width:30px' id='inboundStopOver_"+i+"' name='inboundStopOver' value='"+j.inboundStopOver+"' maxlength='2'/> <label id='lblInBoundDays'>Inbound Days</label>"+
					"</div>");

			$(".stopOverTimeInner").append(input);
		});
		
		$("input[name='outboundStopOver']").on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		});

		$("input[name='inboundStopOver']").on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		});
 
	};
	
		
})(UI_ModifySearch);