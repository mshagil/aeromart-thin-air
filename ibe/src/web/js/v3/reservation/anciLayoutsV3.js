/*
	 * Set Meal HTML Layout
	 */
	function setMealLayout() {
		var mealBanner = "";
		if (UI_Anci.mealViewType != "New-view"){
			if(UI_Anci.showMealMenu == "Y") {
				mealBanner +='<td align="center"><input type="button" id="btnViewSkyCafe"  value="View Sky Cafe Menu" class="ButtonMeal"/></td>';
			} 
		}else{
			mealBanner +='<td><div class="mealOfferDiv alignLeft"></div><div class="rowGap clear"></div></td>';
		}
		$("#mealMenuDisplay").html(mealBanner);
		
		var hrmlSrt = '<table border="0" cellpadding="1" cellspacing="0" width="100%"><tr><td>'+
		'<label id="lblMealHowTo">'+UI_Container.labels.lblMealHowTo+'</label>'+
	    '</td></tr><tr><td class="rowGap"></td></tr>';
		
		if (UI_Anci.mealViewType != "New-view"){
			hrmlSrt +='<tr><td class="tabBG noPadding" style="padding:0px 4px"><div class="newTabStyle"><table border="0" cellpadding="0" cellspacing="0" id="tbdyMealSeg" name="tbdyMealSeg"></div>';
		}else{
			hrmlSrt +='<tr><td class="noPadding"><table border="0" cellpadding="1" cellspacing="0" width="100%"><tr><td id="tbdyMealSeg" name="tbdyMealSeg">';
		}
		hrmlSrt +='</td></tr></table></td></tr>';
		if (UI_Anci.mealViewType != "New-view"){
			var mealPriceHeding = '';
			var mealChargeText = '';
			if(UI_Container.isFOCmealEnabled == false) {
				mealPriceHeding = '<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblMealPriceHD">'+UI_Container.labels.lblMealPriceHD+'</label></td>'
				mealChargeText = '<td class="rowColor bdRight bdBottom" align="center"><label name="mealCharge"></label></td>';
			}
			
			hrmlSrt +='<tr><td class="rowGap"></td></tr>'+
			'<tr><td align="left">'+
					'<table width="100%" border="0" cellpadding="1" cellspacing="1" align="center">'+
						'<thead><tr>'+
								'<td width="50%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblMealPaxHD">'+UI_Container.labels.lblMealPaxHD+'</label></td>'+
								'<td width="30%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblMealMenuHD">'+UI_Container.labels.lblMealMenuHD+'</label></td>'+
								mealPriceHeding +
						'</tr></thead>'+
						'<tr id="paxMealTemplate">'+
							'<td class="defaultRowGap rowColor bdLeft bdBottom bdRight"><label name="paxName" ></label></td>'+
							'<td class="rowColor bdRight bdBottom" align="center"><select name="mealList" size="1" style="width:150px;"></select></td>'+
							mealChargeText +
						'</tr></table>'+
			'</td></tr>';
		}
		hrmlSrt +='</table>';
		$('#tblMealDetails').html(hrmlSrt)
	}
	
	
	/*
	 * Baggage HTML Layout
	 */
	function setBaggageLayout() {	
		var hrmlSrt = '<table id="tblBaggageDetails" border="0" cellpadding="1" cellspacing="1" width="100%">'+
			'<tr><td class="rowGap"></td></tr>'+
			'<tr><td class="noPadding" style="padding: 0px 4px;">';
				if (UI_Anci.baggageViewType == "New-view"){
					hrmlSrt +='<div id="tbdyBaggageSeg"></div>';
				}else{
					hrmlSrt +='<div class="newTabStyle"><table cellspacing="2" cellpadding="0" border="0"><tbody name="tbdyBaggageSeg" id="tbdyBaggageSeg"></tbody></table></div>';
				}
			hrmlSrt +='</td></tr>';
			if (UI_Anci.baggageViewType != "New-view"){
				hrmlSrt +='<tr><td align="left">'+
						'<table width="95%" border="0" cellpadding="1" cellspacing="1" align="center"><tr>'+
								'<td width="5%" class="gridHD" align="center"><label class="gridHDFont fntBold">&nbsp;</label></td>'+
								'<td width="40%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblBaggagePaxNameHD">'+UI_Container.labels.lblSeatPaxNameHD+'</label></td>'+
								'<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblBaggageNumberHD">'+UI_Container.labels.lblBaggageTypeHD+'</label></td>'+
								'<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblBaggagePriceHD">'+UI_Container.labels.lblBaggagePriceHD+'</label></td>'+
								'<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold">&nbsp;</label></td>'+
							'</tr><tr id="baggagePaxTemplate">'+
								'<td class="defaultRowGap rowColor bdLeft bdBottom" align="center"><input type="radio" id="radBaggagePax" name="radBaggagePax"></td>'+
								'<td class="rowColor bdBottom"><label name="paxName"></label></td>'+
								'<td class="rowColor bdBottom" align="center"><label name="baggageNo"></label></td>'+
								'<td class="rowColor bdBottom" align="center"><label name="baggageCharge"></label></td>'+
								'<td class="rowColor bdBottom" align="center"><span name="clear"></span></td>'+
							'</tr></table>'+
				'</td></tr>';
			}
			hrmlSrt += '<tr><td class="rowGap"></td></tr>';
			hrmlSrt += '<tr id="baggageTermsRow">' +
				   '<td class="alignLeft">'+

				   '<table><tr>'+	
				   '<td width="2%"><label><input type="checkbox" title="Click here if you agree with baggage selection terms and conditions v1" name="chkBaggageTerms" id="chkBaggageTerms"/><label></td>'+
				   '<td width="98%"><label id="lblBaggageAcceptMsg">'+ UI_Container.labels.lblAnciSeatSelectionTermsAccept + '</label>&nbsp;&nbsp;</td>'	+													
				   '</tr></table>'+
				   
				   '</td></tr>';
				hrmlSrt +='</table>';
			$('.divBaggageAnci').html(hrmlSrt);
			$('#baggageTermsRow').hide();
	}
	
	/*
	 * Seat HTML Layout
	 */
	function setSeatMapLayout() {	
		var hrmlSrt = '<table id="tblSeatDetails" border="0" cellpadding="1" cellspacing="1" width="100%">';
		
		if(typeof UI_Top.holder().GLOBALS != "undefined" && UI_Top.holder().GLOBALS.autoSeatAssignmentEnabled){
			hrmlSrt += '<tr><td><label id="lblAutoSeatSelectWhy">'+UI_Container.labels.lblAutoSeatSelectWhy+'</label></td>';
		}else{
			hrmlSrt += '<tr><td><label id="lblSeatSelectWhy">'+UI_Container.labels.lblSeatSelectWhy+'</label></td>';
		}
		hrmlSrt += '<tr><td class="rowGap">&nbsp;</td></tr>'+
			'<tr><td class="noPadding" style="padding: 0px 4px;">';
				if (UI_Anci.seatViewType == "New-view"){
					hrmlSrt +='<div id="tbdySeatSeg"></div>';
				}else{
					hrmlSrt +='<div class="newTabStyle"><table cellspacing="2" cellpadding="0" border="0"><tbody name="tbdySeatSeg" id="tbdySeatSeg"></tbody></table></div>';
				}
			hrmlSrt +='</td></tr>'+
			'<tr><td class="rowGap"></td></tr>'+
			'<tr><td>'+
				'<table border="0" cellpadding="1" cellspacing="0"><tr>'+
						'<td class="availSeat matchCC"></td>'+
						'<td><label id="lblAvailableSeat">'+UI_Container.labels.lblAvailableSeat+'</label></td>'+
						'<td class="notAvailSeat matchCC"></td>'+
						'<td><label id="lblOccupiedSeat">'+UI_Container.labels.lblOccupiedSeat+'</label></td>'+
						'<td class="SltdSeat matchCC"></td>'+
						'<td><label id="lblSelectedSeat">'+UI_Container.labels.lblSelectedSeat+'</label></td>'+
						'<td class="oldSeat tdOldSeatLegend matchCC" style="display:none"></td>'+
						'<td class="tdOldSeatLegend matchCC" style="display:none"><label id="lblYouBookedSeat" >'+UI_Container.labels.lblYouBookedSeat+'</label></td>'+
						'<td class="friendsSeat friendsSeatLegend matchCC" style="display:none"></td>'+
						'<td class="friendsSeatLegend matchCC" style="display:none"><label id="lblFriendsSeat" >'+UI_Container.labels.lblFriendsSeat+'</label></td>'+
						
				'</tr></table>'+
				'</td></tr>'+
			'<tr><td class="rowGap"></td></tr>'+
			'<tr><td align="center" style="height:260px">'+
					'<div id="seatmapLoader" style="display: none"><img scr="../images/loading-cal_no_cache.gif"></div>'+
					'<table border="0" cellpadding="0" cellspacing="0" id="tblSeatMap" class="tblSeatMapContainer">'+
						'<tr><td class="wingRight" valign="bottom" align="center">'+
							'<table border="0" cellpadding="0" cellspacing="0"><tbody id="tbdyWingRight"></tbody></table>'+
						'</td></tr>'+
						'<tr><td align="center">'+
							'<table border="0" cellpadding="0" cellspacing="0"><tbody id="tbdySeatMap"></tbody></table>'+
						'</td></tr>'+
						'<tr><td class="wingLeft" valign="top" align="center"><table border="0" cellpadding="0" cellspacing="0"><tbody id="tbdyWingLeft"></tbody></table>'+
						'</td></tr>'+
					'</table><span id="spnAnciNoSeat"></span>'+
				'</td></tr>'+
			'<tr><td class="rowGap"></td></tr>';
			if (UI_Anci.seatViewType != "New-view"){
				hrmlSrt +='<tr><td align="left">'+
						'<table width="95%" border="0" cellpadding="1" cellspacing="1" align="center"><tr>'+
								'<td width="5%" class="gridHD" align="center"><label class="gridHDFont fntBold">&nbsp;</label></td>'+
								'<td width="40%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblSeatPaxNameHD">'+UI_Container.labels.lblSeatPaxNameHD+'</label></td>'+
								'<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblSeatNumberHD">'+UI_Container.labels.lblSeatNumberHD+'</label></td>'+
								'<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold" id="lblSeatPriceHD">'+UI_Container.labels.lblSeatPriceHD+'</label></td>'+
								'<td width="20%" class="gridHD" align="center"><label class="gridHDFont fntBold">&nbsp;</label></td>'+
							'</tr><tr id="seatPaxTemplate">'+
								'<td class="defaultRowGap rowColor bdLeft bdBottom bdRight" align="center"><input type="radio" id="radSeatPax" name="radSeatPax"></td>'+
								'<td class="rowColor bdRight bdBottom"><label name="paxName"></label></td>'+
								'<td class="rowColor bdRight bdBottom" align="center"><label name="seatNo"></label></td>'+
								'<td class="rowColor bdRight bdBottom" align="center"><label name="seatCharge"></label></td>'+
								'<td class="rowColor bdRight bdBottom" align="center"><span name="clear"></span></td>'+
							'</tr></table>'+
				'</td></tr>';
			}
			
			hrmlSrt +='<tr><td align="left">'+
	          '<table width="95%" border="0" cellpadding="1" cellspacing="1" align="center">' +
	          		'<tr><td>'+
	          		'<div>'+
			          	'<div id="socialSeatingFB" class="soclaiButton soCommonBtn" style="display:none">'+
							'<a href="javascript:void(0)" title="To find friends login with Facebook" id="btnSocialSeatingFacebook" name="btnSocialSeatingFacebook" class="buttonFB">'+
								'<span class="logo"></span>'+
								'<span>' + UI_Container.labels.findFriendsFacebook + '</span>'+
							'</a>'+
						'</div>'+
						'<div id="socialSeatingLN" class="soclaiButton soCommonBtn" style="display:none">'+
							'<a href="javascript:void(0)" title="To find friends login with LinkedIn" id="btnSeatLinkedIn" name="btnSeatLinkedIn" class="buttonLN">'+
								'<span class="logo"></span>'+
								'<span>' + UI_Container.labels.findFriendsLinkedIn + '</span>'+
							'</a>'+
						'</div>'+
						'</div><div class="clear"></div>'+
	          		'</div>'+
	          		'</td></tr>'+
	          		'<tr id="socialSeatingUserInfo">'+
	          			'<td> <div id="user-info"></div> </td>'+
	          		'</tr>' +
	          		'<tr id="socialSeatingFriends">'+
	          			'<td> <div id="mutualFriends" ></div> </td>'+
	          		'</tr>'+
	          		'</table></td></tr>';
			
			hrmlSrt += '<tr><td class="rowGap"></td></tr>';
			hrmlSrt += '<tr id="seatTermsRow">' +
				   '<td class="alignLeft">'+

				   '<table><tr>'+	
				   '<td width="2%"><label><input type="checkbox" title="Click here if you agree with seat selection terms and conditions v1" name="chkSeatTerms" id="chkSeatTerms"/><label></td>'+
				   '<td width="98%"><label id="lblSeatAcceptMsg">'+ UI_Container.labels.lblAnciSeatSelectionTermsAccept + '</label>&nbsp;&nbsp;</td>'	+													
				   '</tr></table>'+
				   
				   '</td></tr>';
			hrmlSrt += '<tr><td class="rowGap"></td></tr>';
			hrmlSrt +='</table>';
			$('.divSearAnici').html(hrmlSrt);
			$('#seatTermsRow').hide();
	}
	
	/*
	 * Insurance Layout
	 */
	function setInsuranceLayout() {
		var hrmlSrt = "<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>"+
			"<tr><td class='rowGap'></td></tr><tr>"+
			"<td class='alignLeft'><label id='lblInsuranceHD' class='fntBold hdFontColor'>"+UI_Container.labels.lblInsuranceHD+"</label>" +
			"</td></tr>"+
			"<tr><td><div class='insPromoOffer alignLeft'></div><div class='rowGap'></div></td></tr>";
			hrmlSrt +="<tr>"+
			"<td class='alignLeft'><label id='lblInsuranceDesc'>"+UI_Container.labels.lblInsuranceDesc+"</label></td>"+
			"</tr>";		
			hrmlSrt +="<tr class='AAStyle'><td class='rowGap'></td></tr>"+
			"</tr><tr class='AAStyle'><td class='alignLeft'><label id='lblInsuranceChDesc'></label>&nbsp;<label id='spnInsCost'></label></td></tr>";
		if (UI_Anci.insViewType == "New-view"){
			hrmlSrt +="<tr class='AAStyle'><td class='rowGap'></td></tr>"+
					"<td width='100%' valign='top' class='alignLeft'><table border='0' cellpadding='5' cellspacing='0' class='insCoverInforTable'>" +
					"<tr><td class='alignLeft'><label id='insCoverInfo1'>"+UI_Container.labels.insCoverInfo1+"</label></td></tr>" +
					"<tr><td class='withIcon'><span>&nbsp;</span><label id='insCoverInfo2'>"+UI_Container.labels.insCoverInfo2+"</label></td></tr>" +
					"<tr><td class='withIcon'><span>&nbsp;</span><label id='insCoverInfo3'>"+UI_Container.labels.insCoverInfo3+"</label></td></tr>" +
					"<tr><td class='withIcon'><span>&nbsp;</span><label id='insCoverInfo4'>"+UI_Container.labels.insCoverInfo4+"</label></td></tr>" +
					//"<tr><td align='right'><label id='insCoverInfo5'>More info</label></td></tr>" +
					"</table></td></tr>";
		}
		
		
		hrmlSrt += "<tr><td class='rowGap' colspan='2'></td></tr>" + "<tr><td class='alignLeft'>";
		
		if (UI_Anci.insViewType == "New-view"){
			hrmlSrt +="<table border='0' cellpadding='1' cellspacing='1' width='100%'>"+
					"<tr>"+
						"<td width='2%' ><input type='radio' name='radAnciIns' id='radAnciIns_Y'/></td>"+
						"<td width='98%' class='alignLeft'><label id='lblInsuranceYes'>"+UI_Container.labels.lblInsuranceYes+"</label><br>" +
						"<label class='Z2Style ftnGray'>"+UI_Container.labels.insCoverInfo2+"</label></td>"+
					"</tr>"+
					"<tr>"+
						"<td width='2%'><input type='radio' name='radAnciIns' id='radAnciIns_N'/></td>"+
						"<td width='98%' class='alignLeft'><label id='lblInsuranceNo'>"+UI_Container.labels.lblInsuranceNo+"</label></td>"+
					"</tr>"+
					"</table>";
		} else if(UI_Anci.insViewType == 'New-multiOption-view') {
			hrmlSrt +="<table border='0' cellpadding='1' cellspacing='1' width='100%'>"+
			"<tr>"+
				"<td width='2%' ><input type='radio' name='radAnciIns' id='radAnciIns_C'/></td>"+
				"<td width='98%' class='alignLeft'><label id='lblInsuranceCancel'>"+UI_Container.labels.lblInsuranceCancel+"</label></td>"+
			"</tr>"+
			"<tr>"+
				"<td width='2%' ></td>"+
				"<td width='98%' class='alignLeft'><span id='lblInsuranceTNC_cancelation'>"+UI_Container.labels.lblInsuranceTNC_cancelation+"</span></td>"+
			"</tr>"+
			"<tr>"+
				"<td width='2%' ><input type='radio' name='radAnciIns' id='radAnciIns_M'/></td>"+
				"<td width='98%' class='alignLeft'><label id='lblInsuranceMiltiRisk'>"+UI_Container.labels.lblInsuranceMiltiRisk+"</label></td>"+
			"</tr>"+
			"<tr>"+
				"<td width='2%' ></td>"+
				"<td width='98%' class='alignLeft'><span id='lblInsuranceTNC_multirisk'>"+UI_Container.labels.lblInsuranceTNC_multirisk+"</span></td>"+
			"</tr>"+
			"<tr>"+
				"<td width='2%'><input type='radio' name='radAnciIns' id='radAnciIns_N'/></td>"+
				"<td width='98%' class='alignLeft'><label id='lblInsuranceNo'>"+UI_Container.labels.lblInsuranceNo+"</label></td>"+
			"</tr>"+			
			"</table>";
		} else {
			hrmlSrt +="<table border='0' cellpadding='1' cellspacing='1' width='100%'>"+
			"<tr>"+
				"<td class='alignLeft' width='2%'><input type='checkbox' id='chkAnciIns' name='chkIns' /></td>"+
				"<td width='98%' class='alignLeft'><label id='lblInsuranceYes' >"+UI_Container.labels.lblInsuranceYes+"</label></td>"+
			"</tr>"+
			"<tr><td></td>"+
				"<td class='alignLeft'><label  id='lblInsuranceUncheckNotice' class='fntSmall'>"+UI_Container.labels.lblInsuranceUncheckNotice+"</label></td>"+
			"</tr></table>";
		}
		hrmlSrt +="</td>"+
			"</tr>"+
			"<tr><td colspan='2' class='rowGap'></td></tr>"+
			"<tr class='Z2Style'><td align='center'>" +
			"<div>"+UI_Container.labels.insCoverInfo1+"</div>"+
			"</td></tr>"+
			"<tr><td width='2%'>";
			if (anciConfig.insTCCheck){
				hrmlSrt +="<input type='checkbox' name='chkTC' id='chkTC' />";
			}
			hrmlSrt +="</td><td class='alignLeft'>"+
					"<label id='lblInsuranceTNC' class='alignLeft'>"+UI_Container.labels.lblInsuranceTNC+"</label>"+
			"</td></tr>"+
			"<tr><td class='rowGap'></td></tr>"+
				"<tr style='display:none'>"+
				"<td width='2%'><input type='hidden' name='insType' id='insType' value='1'/></td>"+
				"<td width='98%' class='alignLeft'></td>"+
			"</tr>"+
			"</table>";
		hrmlSrt = "";
		$('#insuranceLoad').html(hrmlSrt)
	}
	/*
	 * Baggage HTML Layout
	 */
	function setHalaLayout() {	
		var hrmlSrt = '<table id="tblHalaDetails" border="0" cellpadding="1" cellspacing="1" width="100%">'+
			'<tr><td class="rowGap"></td></tr>'+
			'<tr><td class="noPadding" style="padding: 0px 4px;">'+
			'<div id="tbdyHalaSeg"></div></td></tr>'+
			'<tr><td class="rowGap"></td></tr>'+
			'</table>';
			$('.divHalaAnci').html(hrmlSrt);
	}
	
	function setAirportTransferLayout() {	
		var hrmlSrt = '<table id="tblApTransferDetails" border="0" cellpadding="1" cellspacing="1" width="100%">'+
			'<tr><td class="rowGap"></td></tr>'+
			'<tr><td class="noPadding" style="padding: 0px 4px;">'+
			'<div id="tbdyApTransferSeg"></div></td></tr>'+
			'<tr><td class="rowGap"></td></tr>'+
			'</table>';
			$('.divApTransferAnci').html(hrmlSrt);
	}

	/*
	 * Flexi HTML Layout
	 */
	function setFlexiLayout() {	
		var hrmlSrt = '<table border="0" cellpadding="1" cellspacing="1" width="100%">'+
			'<tr><td class="rowGap"></td></tr>'+
			'<tr><td class="noPadding" style="padding: 0px 4px;">'+
			'<div id="tbdyFlexiSeg"></div></td></tr>'+
			'<tr><td class="rowGap"></td></tr>'+
			'</table>';
			$('#tblFlexiDetails').html(hrmlSrt);
	}
	
	/*function callMyPopUp(w, h, t, l, heder, body, navCallBack){
		var str = '';
		var count = 0;
		var atLeastOneSelected = false;
		var leastOne = null;
		var all = null;
		
		if(anciConfig.anciContAlertType == 'ALL'){
			all = UI_Anci.allSelectedCheck();
			count = all.count;
			str = all.str;
			atLeastOneSelected = all.atLeastOneSelected;
		} else {
			leastOne = UI_Anci.leastOneSelectedCheck();
			count = leastOne.count;
			str = leastOne.str;
			atLeastOneSelected = leastOne.atLeastOneSelected;
		}
		var cntd = false;
		if(count >0){			
			$('#divAnciContConfirm').show();
			$('#spnNotSelectedAnci').html(str);
			var youHaveNotSelected = UI_Anci.jsonLabel['lblHaveNotSelectedFollowings'];
			var doYouWantToContinue = UI_Anci.jsonLabel['lblDoYouWantToContinue'];
			var yesLbl = UI_Anci.jsonLabel['lblComYes'];
			var noLbl = UI_Anci.jsonLabel['lblComNo'];
			var response = null;
			if(UI_Container.isModifyAncillary() && !atLeastOneSelected){
				jAlert(UI_Container.labels.lblAnciSaveSelectAlert);	
				cntd = false;
				navCallBack(cntd);
			}else {
				if(all==null){
					var anciArray = str.split(",")
					$("#newPopItem").openMyPopUp({
						width:anciArray.length * 230,
						headerHTML:"",
						bodyHTML:createAnciPopupHTML(str),
						footerHTML: '<div class="alignRight"><input type="button" id="btnNoThanks" name="btnNoThanks" class="Button ButtonMediumDisable fntWhite" value="NO Thanks"/></div>'
					});
					$(".close").unbind("click").bind("click",function(){
						$("#newPopItem").closeMyPopUp();
						navCallBack(false);
					});

					$("#btnNoThanks").unbind("click").bind("click",function(){
						$("#newPopItem").closeMyPopUp();
						navCallBack(true);
					});
					$("input[name='btnChAnci']").unbind("click").bind("click",function(){
						$("#newPopItem").closeMyPopUp();
						navCallBack(false);
						if (this.id == "btnChSeat"){
							$('html,body').animate({"scrollTop": $("#seatTr").position().top });
						}
						if (this.id == "btnChMeal"){
							$('html,body').animate({"scrollTop": $("#mealTr").position().top });
						}
						if (this.id == "btnChBaggage"){
							$('html,body').animate({"scrollTop": $("#baggageTr").position().top });
						}
					});
					$($(".popup-column").get(0)).addClass("popup-column-first");
					//jConfirm(youHaveNotSelected +'\n'+str +'\n'+doYouWantToContinue, 'Confirm', function(res){
						//navCallBack(res);
					//},yesLbl,noLbl);
				}else{
					jConfirm(str +'\n'+doYouWantToContinue, 'Confirm', function(res){
						if(!res){
							UI_Anci.focusNotSelected(all);
						}
						navCallBack(res);
					},yesLbl,noLbl);
				}
				
			}
			cntd = false;
			
		}else{
			if(UI_Container.isModifyAncillary() && !atLeastOneSelected){
				jAlert(UI_Container.labels.lblAnciSaveSelectAlert);	
				cntd = false;
				navCallBack(cntd);
			}else{
				cntd = true;
			}
		}
		if(cntd){
			navCallBack(cntd);
		}
		
	};*/
	
	
	function callMyPopUp(w, h, t, l, heder, body, navCallBack){
		var str = '';
		var count = 0;
		var atLeastOneSelected = false;
		var leastOne = null;
		var all = null;
		var cheperBundlefareCount = 0 ;
		var bundleFareSTR = null ;  
		
		
		
		var cheaperBundleFare = UI_Anci.CheckForCheaperBundleFare();
		if(cheaperBundleFare.length > 0){
			for ( var i = 0; i < cheaperBundleFare.length; i++) {
				cheperBundlefareCount += cheaperBundleFare[i].length;
				
			}
			if(cheperBundlefareCount> 0 ){
				bundleFareSTR = UI_Anci.getCheaperBundleFareAvailableMessage(cheaperBundleFare);
			}
		}		
		
		if(anciConfig.anciContAlertType == 'ALL'){
			all = UI_Anci.allSelectedCheck();
			count = all.count;
			str = all.str;
			atLeastOneSelected = all.atLeastOneSelected;
		} else {
			leastOne = UI_Anci.leastOneSelectedCheck();
			count = leastOne.count;
			str = leastOne.str;
			atLeastOneSelected = leastOne.atLeastOneSelected;
		}
		
		var cntd = false;
		if(count >0 || cheperBundlefareCount > 0 ){			
			$('#divAnciContConfirm').show();
			$('#spnNotSelectedAnci').html(str);
			var youHaveNotSelected = UI_Anci.jsonLabel['lblHaveNotSelectedFollowings'];
			var doYouWantToContinue = UI_Anci.jsonLabel['lblDoYouWantToContinue'];
			var yesLbl = UI_Anci.jsonLabel['lblComYes'];
			var noLbl = UI_Anci.jsonLabel['lblComNo'];
			var response = null;
			if(count >0 && cheperBundlefareCount > 0){
			if(UI_Container.isModifyAncillary() && !atLeastOneSelected){
				jAlert(UI_Container.labels.lblAnciSaveSelectAlert);	
				cntd = false;
				navCallBack(cntd);
			}else {
				if(all==null){
					jConfirm(bundleFareSTR +'\n \n' + youHaveNotSelected +'\n'+str +'\n'+doYouWantToContinue, UI_Container.labels.lblAlertConfirm, function(res){
						navCallBack(res);
					},yesLbl,noLbl);
				}else{
					jConfirm(bundleFareSTR +'\n \n'+ str +'\n'+doYouWantToContinue, UI_Container.labels.lblAlertConfirm, function(res){
						if(!res){
							UI_Anci.focusNotSelected(all);
						}
						navCallBack(res);
					},yesLbl,noLbl);
				}
				
			}
		}else if(count >0 ){
			if(UI_Container.isModifyAncillary() && !atLeastOneSelected){
				jAlert(UI_Container.labels.lblAnciSaveSelectAlert);	
				cntd = false;
				navCallBack(cntd);
			}else {
				if(all==null){
					jConfirm(  youHaveNotSelected +'\n'+str +'\n'+doYouWantToContinue, UI_Container.labels.lblAlertConfirm, function(res){
						navCallBack(res);
					},yesLbl,noLbl);
				}else{
					jConfirm( str +'\n'+doYouWantToContinue, UI_Container.labels.lblAlertConfirm, function(res){
						if(!res){
							UI_Anci.focusNotSelected(all);
						}
						navCallBack(res);
					},yesLbl,noLbl);
				}
				
			}
		}else{
			jConfirm(bundleFareSTR +'\n \n'+doYouWantToContinue, UI_Container.labels.lblAlertConfirm, function(res){
				navCallBack(res);
			},yesLbl,noLbl);
		}
			cntd = false;
			
		}else{
			if(UI_Container.isModifyAncillary() && !atLeastOneSelected){
				jAlert(UI_Container.labels.lblAnciSaveSelectAlert);	
				cntd = false;
				navCallBack(cntd);
			}else{
				cntd = true;
			}
		}
		if(cntd){
			navCallBack(cntd);
		}
	};
	
	function ssrSegmentValidationPopUp(message){
		if (message == "C"){
			jAlert(UI_Container.labels.lblSsrSegmentValidationAlert);
		}else if (message = "M"){
			jAlert(UI_Container.labels.lblSsrSegmentValidationAlertModify);
		}
	}
	
	
	function createAnciPopupHTML(items){
		
		var t = items.toLowerCase();
		var strHTML = '<table border="0" cellpadding="2" cellspacing="0" height="100%"><tr>';
		if ( t.indexOf("seats") > -1 ){
			strHTML +=  '<td class="popup-column"><div><label id="lblbSeatlabelheader" class="hdFontColor fntBold">'+UI_Container.labels.lblChooseSeatHead+'</label></div>'+
						'<div class="item-body"><label id="lblpopSeatBody">'+UI_Container.labels.lblChooseSeatBody+'</label></div>'+
						'<div class="item-button rowSeat"><span></span><input type="button" id="btnChSeat" name="btnChAnci" value="'+UI_Container.labels.btnSelectSeat+'" class="ButtonLargeMedium Button"/></div>'+
						'</td>';
		}
		if ( t.indexOf("meals") > -1 ){
			strHTML +=  '<td class="popup-column"><div><label id="lblbMeallabelheader" class="hdFontColor fntBold">'+UI_Container.labels.lblChooseMealHead+'</label></div>'+
						'<div class="item-body"><label id="lblpopMealBody">body text</label></div>'+
						'<div class="item-button rowMeal"><span></span><input type="button" id="btnChMeal" name="btnChAnci" value="'+UI_Container.labels.btnSelectMeal+'" class="ButtonLargeMedium Button"/></div>'+
						'</td>';
		}
		if ( t.indexOf("insurance") > -1 ){
			strHTML +=  '<td class="popup-column"><div><label id="lblbInslabelheader" class="hdFontColor fntBold">'+UI_Container.labels.lblChooseInsHead+'</label></div>'+
						'<div class="item-body"><label id="lblpopInsBody">'+UI_Container.labels.lblChooseInsBody+'</label></div>'+
						'<div class="item-button rowIns"><span></span><input type="button" id="btnChIns" name="btnChAnci" value="'+UI_Container.labels.btnSelectIns+'" class="ButtonLargeMedium Button"/></div>'+
						'</td>';
		}
		if ( t.indexOf("baggages") > -1 ){
			strHTML +=  '<td class="popup-column"><div><label id="lblbBaglabelheader" class="hdFontColor fntBold">'+UI_Container.labels.lblChooseBaggageHead+'</label></div>'+
						'<div class="item-body"><label id="lblpopBagBody">'+UI_Container.labels.lblChooseBaggageBody+'</label></div>'+
						'<div class="item-button rowBag"><span></span><input type="button" id="btnChBaggage" name="btnChAnci" value="'+UI_Container.labels.btnSelectBaggage+'" class="ButtonLargeMedium Button"/></div>'+
						'</td>';
		}
		strHTML += '</tr></table>';
		return strHTML;
	}
	
	function setDesignSpecLabels(){
		UI_Container.labels['btnNext'] = btnContinuelbl;
		UI_Container.labels['btnPrevious'] = btnPreviouslbl;
		UI_Container.labels['btnPurchase'] = btnPaymentlbl;
	}