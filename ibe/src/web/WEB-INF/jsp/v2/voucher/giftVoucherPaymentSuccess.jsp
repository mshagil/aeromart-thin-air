<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="-1"/>
		<title><fmt:message key="msg.Page.Title" /></title>		
		<%@ include file='../common/iBECommonParam.jsp'%>
		<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css"/>
		<script type="text/javascript" src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/v2/voucher/giftVoucherPostPay.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'/>" type="text/javascript"></script>
	</head>
	<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <!-- Top Banner -->
        <c:import url="../../../../ext_html/header.jsp" />
        <tr>
            <td align='center' class="outerPageBackGround">
                <table style='width: 940px;' border='0' cellpadding='0'
                       cellspacing='0' align='center' class='PageBackGround'>
                    <tr>
                        <td colspan="2" class="mainbody">
                            <table width='95%' border='0' cellpadding='2' cellspacing='0'>
                                <tr>
                                    <td valign="top" class="spMsg">
                                        <img src="../images/n058_no_cache.gif" id="success" vspace="3" hspace="3" align="absmiddle" />
                                        <label id="lblVoucherMessage" class="fntBold"><c:out value="${requestScope.message}"/></label>
                                    </td>
                                </tr>
                                <tr><td><br/><br/></td> </tr>
                                
                            </table>
                     	</td>
                     </tr>
                     <tr>
                     	<td align="center">
	                     	<div class="alignRight" id="paymentButtons">
								<table width='100%' border='0' cellpadding='10' cellspacing='10'>
									<tr>
										<td width="100" class="alignRight">
											<input type="button" id="btnPrintConf" name="btnPrint" class="Button" value="Print"/>
										</td>
										<td width="100" class="alignLeft">
											<input type="button" id="btnEmailConf" name="btnEmail" class="Button" value="Email"/>
										</td>
									</tr>
								</table>
							</div>
                     	</td>
                     </tr>
                     <tr>
                     	<td>
	                     	<div id='divLoadMsg' class="mainPageLoader">
								<%@ include file='../common/includeLoadingMsg.jsp'%>
							</div>
                     	</td>
                     </tr>
                    
                     <%-- Bottom AA Logo --%>
                    <tr>
                        <td class='appLogo' colspan="2"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <c:import url="../../../../ext_html/cfooter.jsp" />
    </table>
	</body>
</html>