<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../../common/pageHD.jsp'%>
<html>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <META HTTP-EQUIV="Expires" CONTENT="-1">
    <link rel='stylesheet' type='text/css' href='../css/<fmt:message key="msg.pg.css.name"/>'>
    <%@ include file='../common/interlinePgHD.jsp' %>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>       
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	  	
	<script language="JavaScript" src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script type="text/javascript">

        var strReqParam = '<c:out value="${requestScope.sysReqParamAA}" escapeXml="false" />';
		var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;
		var objCWindow = "";
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}

		var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
		var strToDate	= strSysDate;
		if (UI_Top.holder().GLOBALS.currentDate != null && UI_Top.holder().GLOBALS.currentDate == "") {
			var date = UI_Top.holder().GLOBALS.currentDate;
			var dtTempSysDate = new Date(date.substr(6,4), (Number(date.substr(3,2)) - 1), date.substr(0,2));
			strSysDate  	= DateToString(addDays(dtTempSysDate, 0));
			dtSysDate		= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
			strToDate       = dtSysDate; 
		}	
		var bookingStepsArray = ["Select a Flight", "Passenger details", "Personalise ", "Payment", "Print & Fly"];
				
		//var url = {header:'<fmt:message key="msg.pg.header.banner"/>', footer:'<fmt:message key="msg.pg.header.banner"/>'};


	</script>
  </head>
  <body onkeydown='return Body_onKeyDown(event)'> 
	<iframe src='showBlank'	id='frmMain' name='frmMain'	frameborder='0' scrolling='auto' width='100%' height='100%'></iframe>
	<div id='divLoadMsg' class="mainPageLoader">
		<%@ include file='../common/includeLoadingMsg.jsp' %>
	</div>
<script
	src="../js/v2/reservation/reservation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type='text/javascript'></script>
<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />
  </body>	
</html>
			