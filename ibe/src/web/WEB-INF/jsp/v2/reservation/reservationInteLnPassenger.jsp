<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="-1"/>
<title></title>
	<%@ include file='../common/interlinePgHD.jsp' %>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
	<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
</head>

<body>
<div id="divLoadBg" style="display: none;">
<form action="interlinePaxDetail.action" id="frmPassenger" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgPassenger">
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<!-- Top Banner -->
					<c:import url="../../../../ext_html/header.jsp" />
					<!-- Content holder -->
					<tr>
						<!-- Left Column -->
						<%@ include file='../common/flightSummary.jsp'%>				
						
						<!-- Right Column -->
						<td valign='top' id="rightPanel"  class='alignRight'>
							<table width='98%' border='0' cellpadding='0' cellspacing='0' class='alignRight'>
								<tr>
									<td class="alignLeft">
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='pnlTop pnlWidth'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td colspan='2' class='pnlBottomTall' valign='top' align='center'>
													<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<!--  navigation  -->
														<%@ include file='../common/navigation.jsp'%>	
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class="alignLeft">
																<label id="lblPaxInfomation" class='fntBold hdFont hdFontColor'>Passenger Information</label>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td class="alignLeft">
																<img src="../images/AA045_no_cache.jpg"/>&nbsp;<label id="lblFillInEnglish">Please complete all information in English.&nbsp;</label><label id="lblHighlightWith">Field(s) highlighted with<label class='mandatory'>&nbsp;*&nbsp;</label><label id="lblAreMandatory"> are mandatory.</label><br>
																<img src="../images/AA045_no_cache.jpg"/>&nbsp;<label id="lblPaxEmailContactIsCorrect">Please ensure that your email and contact details are correct.</label><br/>
																<img src="../images/AA045_no_cache.jpg"/>&nbsp;<label id="lblPaxBookingNameIsCorrect" class='lineHeight fntEnglish'>Please ensure that you make the booking in the same name as shown in your passport.</label>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<!-- Grid -->
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottom' valign='top'>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 5px;">
																	<tr>
																		<td>
																			<table width="100%" border="0" cellspacing="1" cellpadding="1">
																				<tr>
																					<td style='width:25px;' class="alignLeft"></td>
																					<td class="alignLeft"><span id="spnError"></span></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<!-- Adult Grid -->
																	<tr id="adlGrid">
																		<td>
																			<table width="100%" border="0" cellspacing="1" cellpadding="1">
																				<thead>
																					<tr>
																						<td colspan='10' class="alignLeft"><label id="lblAdultHD"  class='fntBold hdFontColor'>Adult(s)</label></td>
																					</tr>
																					<tr>
																						<td width="7%" align="center" class='gridHD'><label id="lblPaxNo" class='gridHDFont fntBold'>No.</label></td>
																						<td width="9%" align="center" class='gridHD'><label id="lblPaxTitle" class='gridHDFont fntBold'>Title</label> <label class="MandatoryGrid">*</label></td>
																						<td width="28%" align="center" class='gridHD'><label id="lblPaxFirstName" class='gridHDFont fntBold'>First Name</label> <label class="MandatoryGrid">*</label></td>
																						<td width="26%" align="center" class='gridHD'><label id="lblPaxLastName" class='gridHDFont fntBold'>Last Name</label> <label class="MandatoryGrid">*</label></td>
																						<td width="16%" align="center" class='gridHD'><label id="lblPaxNationality" class='gridHDFont fntBold'>Nationality</label></td>
																						<td colspan='5' width="14%" align="center" class='gridHD'><label id="lblPaxDOB" class='gridHDFont fntBold'>Date of Birth</label></td>
																					</tr>
																				</thead>        
																				<tr id="paxAdTemplate">
																					<td align='right'><label id="paxSequence"></label></td>
																					<td align='center'><select id='title' style="width: 45px;" size='1' name='title'></select></td>
																					<td align='center'><input type='text' id='firstName' name="firstName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
																					<td align='center'><input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
																					<td align='center'><select id='nationality' size='1' style='width:150px;' name='nationality'></select></td>
																					<td align='center'><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:65px;' maxlength='50'/></td>
            																		<td align='center'><a href="javascript:void(0)"><img src="../images/Calendar2_no_cache.gif" border="0" title="Date Of Birth" id="imageId"/></a></td>
																					<td align='center'><input type='text' id='foidType' name="foidType" style='display:none' /></td>
																					<td align='center'><input type='text' id='foidNumber' name="foidNumber" style='display:none' /></td>
																					<td align='center'><input type='text' id='ssrCode' name="ssrCode" style='display:none' /></td>
																					<td align='center'><input type='text' id='travelWith' name="travelWith" style='display:none' /></td>
																					<td align='center'><input type='text' id='parent' name="parent" style='display:none' /></td>
																					<td align='center'><input type='text' id='nationalIDNo' name="nationalIDNo" style='display:none' /></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	
																	<!--  Child Grid -->
																	<tr id="chdGrid">
																		<td>
																			<table width="100%" border="0" cellspacing="1" cellpadding="1">
																				<thead>
																					<tr>
																						<td colspan='10' class="alignLeft"><label id="lblChildHD"  class='fntBold hdFontColor'>Children</label></td>
																					</tr>
																					<tr>
																						<td width="7%" align="center" class='gridHD'><label id="lblPaxNo" class='gridHDFont fntBold'>No.</label></td>
																						<td width="9%" align="center" class='gridHD'><label id="lblPaxTitle" class='gridHDFont fntBold'>Title</label> <label class="MandatoryGrid">*</label> </td>
																						<td width="28%" align="center" class='gridHD'><label id="lblPaxFirstName" class='gridHDFont fntBold'>First Name</label> <label class="MandatoryGrid">*</label> </td>
																						<td width="26%" align="center" class='gridHD'><label id="lblPaxLastName" class='gridHDFont fntBold'>Last Name</label> <label class="MandatoryGrid">*</label> </td>
																						<td width="16%" align="center" class='gridHD'><label id="lblPaxNationality" class='gridHDFont fntBold'>Nationality</label></td>
																						<td colspan='5' width="14%" align="center" class='gridHD'><label id="lblPaxDOB" class='gridHDFont fntBold'>Date of Birth</label></td>
																					</tr>
																				</thead>        
																				<tr id="paxChTemplate">
																					<td align='right'><label id="paxSequence"></label></td>
																					<td align='center'><select id='title' style="width: 45px;" size='1' name='title'></select></td>
																					<td align='center'><input type='text' id='firstName' name="firstName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
																					<td align='center'><input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize" /></td>
																					<td align='center'><select id='nationality' size='1' style='width:150px;' name='nationality'></select></td>
																					<td align='center'><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:65px;' maxlength='50'/></td>
            																		<td align='center'><a href="javascript:void(0)"><img src="../images/Calendar2_no_cache.gif" border="0" title="Date Of Birth" id="imageId"/></a></td>
																					<td align='center'><input type='text' id='foidType' name="foidType" style='display:none' /></td>
																					<td align='center'><input type='text' id='foidNumber' name="foidNumber" style='display:none' /></td>
																					<td align='center'><input type='text' id='ssrCode' name="ssrCode" style='display:none' /></td>
																					<td align='center'><input type='text' id='travelWith' name="travelWith" style='display:none' /></td>
																					<td align='center'><input type='text' id='parent' name="parent" style='display:none' /></td>
																					<td align='center'><input type='text' id='nationalIDNo' name="nationalIDNo" style='display:none' /></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	
																	<!--  Infant Grid -->
																	<tr id="infGrid">
																		<td>
																			<table width="100%" border="0" cellspacing="1" cellpadding="1">
																				<thead>
																					<tr>
																						<td colspan='8' class="alignLeft"><label id="lblInfantHD"  class='fntBold hdFontColor'>Infant(s)</label></td>
																					</tr>
																					<tr>
																						<td width="7%" align="center" class='gridHD'><label id="lblPaxNo" class='gridHDFont fntBold'>No.</label></td>
																						<td width="18%" align="center" class='gridHD' colspan="2"><label id="lblPaxDOB" class='gridHDFont fntBold'>Date Of Birth</label> <label class="MandatoryGrid">*</label></td>																						
																						<td width="28%" align="center" class='gridHD'><label id="lblPaxFirstName" class='gridHDFont fntBold'>First Name</label> <label class="MandatoryGrid">*</label> </td>
																						<td width="26%" align="center" class='gridHD'><label id="lblPaxLastName" class='gridHDFont fntBold'>Last Name</label> <label class="MandatoryGrid">*</label> </td>
																						<td colspan='4' width="20%" align="center" class='gridHD'><label id="lblPaxTravlingWith" class='gridHDFont fntBold'>Traveling With Adult</label> <label class="MandatoryGrid">*</label></td>
																					</tr>
																				</thead>        
																				<tr id="paxInTemplate">
																					<td align='right'><label id="paxSequence"></label></td>
																					<td align='center'><input type='text' id='dateOfBirth' name="dateOfBirth" style='width:65px;' maxlength='50'/></td>
            																		<td align='center'><a href="javascript:void(0)"><img src="../images/Calendar2_no_cache.gif" border="0" title="Date Of Birth" id="imageId"/></a></td>
																					<td align='center'><input type='text' id='firstName' name="firstName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
																					<td align='center'><input type='text' id='lastName' name="lastName" style='width:150px;' maxlength='50' class="fontCapitalize"/></td>
																					<td align='center'><select id='travelWith' size='1' style='width:50px;' name='travelWith'></select></td>
																					<td align='center'><input type='text' id='foidType' name="foidType" style='display:none' /></td>
																					<td align='center'><input type='text' id='foidNumber' name="foidNumber" style='display:none' /></td>
																					<td align='center'><input type='text' id='ssrCode' name="ssrCode" style='display:none' /></td>
																					<td align='center'><input type='text' id='nationalIDNo' name="nationalIDNo" style='display:none' /></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class='rowGap'></td>
														</tr>
													<%--<tr>
															<td class="alignLeft">
																<i><label id="lblPaxYemenMassage">Passengers travelling from Yemen are required to enter their nationality. </label></i>
															</td>
														</tr>--%>
													</table>
												</td>
											</tr>
											<tr>
												<td class='rowGap'></td>
											</tr>
											<!--  Registerd User login -->
											 <tr id="trRegisterdUser" style="display: none;">																								                                
						                           <td class="alignLeft">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBg' valign='top'>
																<table width="99%" border="0" cellspacing="0" cellpadding="1" align='center' style="padding-left:12px">
																	<tr>
																	   <td colspan="2" class="alignLeft">
														                  <label id="lblMsgForRegisteredUser">If you are already registered with Air Arabia, you can login to fill your contact information using your profile.</label>
																        </td>
														            </tr>
												                    <tr>
												                        <td class="alignLeft"><label id="lblLoginID"> Login ID </label></td>
												                        <td class="alignLeft">:&nbsp;<input type="text" id="txtUID" name="txtUID" maxlength="100" style="width:120px"/></td>
												                    </tr>
												                    <tr>
												                        <td class="alignLeft"><label id="lblPassword">Password</label></td>
												                        <td class="alignLeft">:&nbsp;<input type="password" id="txtPWD" name="txtPWD" maxlength="12" style="width:120px"/></td>
												                    </tr>
												                    <tr>
												                        <td colspan="2" >
												                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-top: 2px">
												                                <tr>
												                                    <td align='center'>
												                                        <input type="button" id="btnSignIn" title="Click here to login" class="Button" value="Sign In"/>
												                                    </td>
												                                </tr>
												                            </table>
												                        </td>
												                    </tr>
												                     <tr>
																		<td height="5px;"></td>
																	</tr>    
																</table>
															</td>
														 </tr>													
														</table>	
					                               </td>
					                        </tr> 					                        
					                        <tr>
												<td class='rowGap'></td>
											</tr>                       
											
											<!-- Contact informaitons -->
											<tr>
												<td class="alignLeft">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>
														<tr>
															<td colspan='2' class='pnlBottomTall' valign='top'>
																<table width="99%" border="0" cellspacing="0" cellpadding="1" align='center' style="padding-left:12px">
																	<tr>
																		<td colspan='6' class="alignLeft"><label id="lblcntInformation"  class='fntBold hdFontColor'>Contact Information</label></td>
																	</tr>
																	<tr>
																		<td colspan='6' class='rowGap'></td>
																	</tr>
																	<tr>
																		<td width="21%"><label id="lblTitle">Title</label></td>
																		<td width='1%'><label>:</label></td>
																		<!--  Bug fix for IE6: set width for title  -->
																		<td width="31%" class="alignLeft"><select id='selTitle' size='1' style="width: 45px;" tabindex="1" name='contactInfo.title'></select><label class='mandatory'>&nbsp;*</label></td>
																		<td width="17%">&nbsp;</td>
																		<td width='1%'>&nbsp;</td>
																		<td width="29%">&nbsp;</td>
																	</tr>
																	<tr>
																		<td><label id="lblFirstName">First Name</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<input type='text' id='txtFName' name="contactInfo.firstName" style='width:150px;' maxlength='50'  class="fontCapitalize"  tabindex="2" /><label class='mandatory'>&nbsp;*</label>
																		</td>
																		<td><label id="lblLastName">Last Name</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<input type='text' id='txtLName' name="contactInfo.lastName" style='width:150px;' maxlength='50'  class="fontCapitalize"  tabindex="3" /><label class='mandatory'>&nbsp;*</label>
																		</td>
																	</tr>
																	<tr>
																		<td><label id="lblCity">City</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<input type='text' id='txtCity'  name="contactInfo.city" style='width:150px;'  maxlength='20'  tabindex="6" /><label class='mandatory'>&nbsp;*</label>
																		</td>
																		<td><label id="lblNationality">Nationality</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<select id='selNationality' size='1' style='width:154px;' tabindex="18" name='contactInfo.nationality'></select><label class='mandatory'>&nbsp;*</label>
																		</td>
																	</tr>
																	<tr>
																		<td><label id="lblCountry">Country Of Residence</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<select id='selCountry' size='1' style='width:154px;'  tabindex="8" name='contactInfo.country'>	
																				<option value=""></option>						
																			</select><label class='mandatory'>&nbsp;*</label>
																		</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>
																	<%--
																	<tr>
																		<td><label id="lblAddress">Address</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<input type='text' id='txtAdd1' name="contactInfo.addresline" style='width:150px;' maxlength='100'  tabindex="4" />
																		</td>
																		<td><label id="lblNationality">Nationality</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<select id='selNationality' size='1' style='width:154px;' tabindex="18" name='contactInfo.nationality'></select><label class='mandatory'>&nbsp;*</label>
																		</td>
																	</tr>
																	<tr>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td class="alignLeft">
																			<input type='text' id='txtAdd2' name="contactInfo.addresStreet" style='width:150px;' maxlength='100'  tabindex="5" />
																		</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td><label id="lblCity">City</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<input type='text' id='txtCity'  name="contactInfo.city" style='width:150px;'  maxlength='20'  tabindex="6" /><label class='mandatory'>&nbsp;*</label>
																		</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td><label id="lblState">State/Province</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<input type='text' id='txtState'  name="contactInfo.state" style='width:150px;'  maxlength='20'  tabindex="7"/>
																		</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td><label id="lblCountry">Country Of Residence</label></td>
																		<td><label>:</label></td>
																		<td class="alignLeft">
																			<select id='selCountry' size='1' style='width:154px;'  tabindex="8" name='contactInfo.country'>	
																				<option value=""></option>						
																			</select><label class='mandatory'>&nbsp;*</label>
																		</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>
																	 --%>
																	<tr>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																		<td>&nbsp;</td>
																	</tr>
																	<tr>
																		<td colspan="6">
																			<table width="100%" border="0" cellspacing="0" cellpadding="1">
																				<tr>
																					<td width='21%'>&nbsp;</td>
																					<td width='1%'>&nbsp;</td>
																					<td width='11%' class="alignLeft"><label id="lblCountryCode" class='fntSmall'>Country Code</label></td>
																					<td width='9%' class="alignLeft"><label id="lblAreaCode" class='fntSmall'>Area Code</label></td>
																					<td class="alignLeft"><label id="lblNumber" class='fntSmall'>Number</label></td>
																				</tr>
																				<tr>
																					<td><label id="lblMobileNo">Mobile No</label></td>
																					<td><label>:</label></td>
																					<td class="alignLeft">
																						<input type='text' id='txtMCountry' name="contactInfo.mCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="9"/>
																					</td>
																					<td class="alignLeft">
																						<input type='text' id='txtMArea' name="contactInfo.mArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="10"/>
																					</td>
																					<td class="alignLeft">
																						<input type='text' id='txtMobile' name="contactInfo.mNumber" style='width:100px;text-align:right;' maxlength='10' tabindex="11" />
																						<label class='mandatory'>&nbsp;*</label>
																					</td>
																				</tr>
																				<tr>
																					<td><label id="lblPhoneNo">Phone No</label></td>
																					<td><label>:</label></td>
																					<td class="alignLeft">
																						<input type='text' id='txtPCountry' name="contactInfo.lCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="12" />
																					</td>
																					<td class="alignLeft">
																						<input type='text' id='txtPArea' name="contactInfo.lArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="13"/>
																					</td>
																					<td class="alignLeft">
																						<input type='text' id='txtPhone' name="contactInfo.lNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="14" />
																						<label class='mandatory'>&nbsp;*</label>
																					</td>
																				</tr>
																				<%--
																				<tr>
																					<td><label id="lblFaxNo">Fax No</label></td>
																					<td><label>:</label></td>
																					<td class="alignLeft">
																						<input type='text' id='txtFCountry' name='contactInfo.fCountry' style='width:50px;text-align:right;' maxlength='4' tabindex="15" />
																					</td>
																					<td class="alignLeft">
																						<input type='text' id='txtFArea' name='contactInfo.fArea' style='width:50px;text-align:right;' maxlength='4' tabindex="16"/>
																					</td>
																					<td class="alignLeft">
																						<input type='text' id='txtFax'  name='contactInfo.fNumber' style='width:100px;text-align:right;' maxlength='10'  tabindex="17" />
																					</td>
																				</tr>
																				 --%>
																				<tr>
																					<td colspan='5' class='rowGap'></td>
																				</tr>
																				<tr>
																					<td></td>
																					<td colspan='4' class="alignLeft">
																						<i><label id="lblMsgContactNumber" class='fntSmall'>Please provide at least one contact number</label></i>
																					</td>
																				</tr>
																				<tr>
																					<td colspan='5' class='rowGap'></td>
																				</tr>
																				<tr>
																					<td colspan='5' class='rowGap'></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td class='pnlTop pnlWidth'></td>
																		<td class='pnlTopR'></td>
																	</tr>
																	<tr>																	
																		<td colspan='2' class='pnlBottomTall' valign='top'>
																			<table width="99%" border="0" cellspacing="0" cellpadding="1"  style="padding-left:12px" align='center'>
																				<tr>
																					<td class="alignLeft">
																						<label id="lblMsgValideEmail">Your Reservation Confirmation will be sent to you by email. Please ensure that you provide a valid email address. </label>
																					</td>
																				</tr>
																				<tr>
																					<td class='rowSingleGap'></td>
																				</tr>
																				<tr>
																					<td class="alignLeft">
																						<table border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td>
																									<input type="checkbox" id="chkEmail" name="chkEmail" style='border:0px; background-color:transparent;' tabindex="20"/>
																								</td>
																								<td class="alignLeft"><label id="lblNoEmail"> Sorry I don't have an E-mail address </label></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<table width='100%' border="0" cellspacing="0" cellpadding="1">
																							<tr>
																								<td colspan='2' class='rowSingleGap'></td>
																							</tr>
																							<tr>
																								<td width='21%'><label id="lblEmailAddress">Email Address</label></td>
																								<td width='1%'><label>:</label></td>
																								<td class="alignLeft">
																									<input type='text' id='txtEmail' name="contactInfo.emailAddress" style='width:165px;' maxlength='100'  tabindex="18" />
																								</td>
																							</tr>
																							<tr>
																								<td><label id="lblVerifyEmail">verify Email</label></td>
																								<td><label>:</label></td>
																								<td class="alignLeft">
																									<input type='text' id='txtVerifyEmail' name="txtVerifyEmail" style='width:165px;' maxlength='100' tabindex="19" />
																								</td>
																							</tr>
																							<tr>
																								<td><label id="lblLanguage">Itinerary Language</label></td>
																								<td><label>:</label></td>
																								<td class="alignLeft">
																								   <!--  Bug fix :IE 6 Issues -->
																									<select id="selLanguage" name="contactInfo.emailLanguage"  style="width: 90px;" size="1" tabindex="20"></select>
																								</td>
																							</tr>
																						</table>
																					</td>
																				</tr> 
																				<tr>
																					<td height="30px">
																					</td>
																				</tr>
																				<tr>
																					<td class="alignLeft">
																					<div class="buttonset">
																						<!-- Button -->
																						<table border='0' cellpadding='0' cellspacing='0'>
																							<tr>
																								<td><input type="button" id="btnSOver"  tabindex="23"  value="Start Over" class="Button ButtonMedium ui-state-default ui-corner-all"/></td>					
																								<td><input type="button" id="btnContinue"  tabindex="24" value="Continue" class="Button ui-state-default ui-corner-all" /></td>
																							</tr>
																						</table>
																					</div>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr> 
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Bottom AA Logo -->
					<tr>
					<td class='appLogo' colspan="2"></td>
					</tr>
					<c:import url="../../../../ext_html/cfooter.jsp" />
				</table>
			</td>
		</tr>
	</table>
	<script src="../js/v2/reservation/interLinePax.js" type="text/javascript"></script>	
	<%@ include file='../common/iBECommonParam.jsp'%>
	<%@ include file='../common/reservationParam.jsp'%>	
</form>
</div>
</body>
</html>