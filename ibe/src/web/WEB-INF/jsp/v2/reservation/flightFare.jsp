<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<title></title>	
	<%@ include file='../common/interlinePgHD.jsp' %>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
	<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</head>

<body>
<div id="divLoadBg" style="display: none;">
<form action="" id="frmFare" method="post">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgFares">
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<!-- Top Banner -->
					<c:import url="../../../../ext_html/header.jsp" />
					<!-- Content holder -->
					<tr>
						<!-- Left Column -->
						<%@ include file='../common/flightSummary.jsp'%>
						
						<!-- Right Column -->
						<td valign='top' id="rightPanel" rowspan='2' align="right">
							<table width='98%' border='0' cellpadding='0' cellspacing='0' class="alignRight">
								<tr>
									<td align='left'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class='pnlTop pnlWidth'></td>
												<td class='pnlTopR'></td>
											</tr>
											<tr>
												<td colspan='2' class='pnlBottomTall' valign='top' align='center'>
													<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>
														<!--  navigation  -->
														<%@ include file='../common/navigation.jsp'%>														
														<tr>
															<td class='rowGap'></td>
														</tr>
														<tr>
															<td>
															<table cellspacing="0" cellpadding="0" border="0" align="center" width="98%">
																<tr>
																	<td class="rowSingleGap">
																	</td>
																</tr>
																<tr>
																	<td>
																		<label id="lblSelectFlight" class="hdFontColor hdFont fntBold"></label>
																	</td>
																</tr>
																<tr>
																	<td class="rowSingleGap">
																	</td>
																</tr>
																	<tr>
																		<td>
																			<label id="lblSelectFlightMsg"></label>
																		</td>
																	</tr>
																	<tr>
																	<td class="rowGap">
																	</td>
																</tr>															
															</table>
														</td>
													</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>									
										<!-- Grid -->
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>														
													</table>
												</td>
											</tr>										
											<tr>
												<td class='pnlBottomTall'>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">	
														<tr>
															<td valign="bottom" align="left">	
																<label class="fntBold hdFontColor paddingL5" id="outDesc"></label>
															</td>								
															<td align="right" valign="top" width="50%">
																<a title="Click here to search previous day flights"  id="linkPreviousOutgoing" href="#">
																	<img border="0" src="../images/NP_no_cache.gif"/>&nbsp;&nbsp;																		
																		<label class="fntBold fntRed cursorPointer" id="lblPreviousDay"></label>&nbsp;&nbsp;
																</a>
																<label class="fntBold fntEnglish" id="dateOB"></label>&nbsp;&nbsp;
																<a title="Click here to search next day flights" id="linkNextOutgoing" href="#">
																	<label class="fntBold fntRed cursorPointer" id="lblNextDay"></label>&nbsp;&nbsp;
																	<img border="0" src="../images/NA_no_cache.gif"/>
																</a>
													  		 </td>															
														</tr>													
														<tr>
															<td colspan='2'  valign='top'>													
																<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable" id="tableOB">																		
																	<tr>
																		<td width="5%" rowspan="2" class="gridHD"></td>
																		<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
																		<td colspan="2" align="center" class='gridHD'><label id="lblDepature" class='gridHDFont fntBold'></label></td>
																		<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
																		<td rowspan="2" align='center' class='gridHD'><label id="lblFlightNo" class='gridHDFont fntBold'></label></td>
																		<td rowspan="2" align='center' class='gridHD'><label id="lblCarrier" class='gridHDFont fntBold'></label></td>
																	</tr> 
																	<tr>            
																		<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'></label></td>            
																	</tr>
																	<tr id="departueFlightTemplate">
																		<td align="center" rowspan="1" class="defaultRowGap rowColor bdLeft bdBottom bdRight">	
																			<input type="checkbox" value=""  class="NoBorder"  id="radOut"/>
																		</td>
																		<td width='35%' class='rowColor bdRight bdBottom alignLeft'><label  id="segmentCode"></label></td>
																		<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="depatureDate"></label></td>
																		<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="depatureTime"></label></td>
																		<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
																		<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
																		<td width='10%' class='rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
																		<td width='9%' class='rowColor bdRight bdBottom' align='center'><img id="carrierImagePath" src=""/></td>
																	</tr>               
																</table>
																<table  width="100%" border="0" cellspacing="1" cellpadding="1" id="tableOBNoFlight">
																	<tr>
																		<td align="center">
																			<br/><br/>
																			<label id="" class="fntBold">No flight is available on the selected date. Please &nbsp;Select alternative dates. &nbsp; </label>
																			<br/>
																			    <a title="Click here to search previous day flights"  id="linkCenterPreviousOutgoing" href="#">																																				
																					<u><label class="fntBold fntRed cursorPointer" id="lblPreviousDay"></label></u>
																				</a>
																				&nbsp;&nbsp;
																				<a title="Click here to search next day flights" id="linkCenterNextOutgoing" href="#">
																					<u><label class="fntBold fntRed cursorPointer" id="lblNextDay"></label></u>																	
																				</a>
																		</td>
																	</tr>
																</table>																																
															</td>
														</tr>
														<tr id="trLocalTimeAirportMsgOB">
															<td colspan='2' class="alignLeft">
																<label class="fntItalic" id="lblLocalTimeAirport"></label>
															</td>
														</tr>
														<tr>
															<td colspan='7' class='rowGap'></td>
														</tr>
														<tr>
															<td colspan='7' class='rowGap'></td>
														</tr>
													</table>
												</td>
											</tr>	
										</table>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' id="tableReturnPanel">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>														
													</table>
												</td>
											</tr>																	
											<tr>
												<td class='pnlBottomTall'>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">	
														<tr>
															<td valign="bottom" align="left">	
																<label class="fntBold hdFontColor paddingL5" id="inDesc"></label>
															</td>								
															<td align="right" valign="top" width="50%">
																<a title="Click here to search previous day flights" id="linkPreviousReturn" href="#">
																	<img border="0" src="../images/NP_no_cache.gif"/>&nbsp;&nbsp;																		
																		<label class="fntBold fntRed cursorPointer" id="lblPreviousDay"></label>&nbsp;&nbsp;
																</a>
																<label class="fntBold fntEnglish" id="dateIB"></label>&nbsp;&nbsp;
																<a title="Click here to search next day flights" id="linkNextReturn" href="#">
																	<label class="fntBold fntRed cursorPointer" id="lblNextDay"></label>&nbsp;&nbsp;
																	<img border="0" src="../images/NA_no_cache.gif"/>
																</a>
													  		 </td>															
														</tr>															 
														<tr id="trRetrunGrid">
															<td colspan='2'  valign='top'>
																<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable" id="tableIB">																															
																	<tr>
																		<td width="5%" rowspan="2" class="gridHD"></td>
																		<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
																		<td colspan="2" align='center' class='gridHD'><label id="lblDepature" class='gridHDFont fntBold'></label></td>
																		<td colspan="2" align='center' class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
																		<td rowspan="2" align='center' class='gridHD'><label id="lblFlightNo" class='gridHDFont fntBold'></label></td>
																		<td rowspan="2" align='center' class='gridHD'><label id="lblCarrier" class='gridHDFont fntBold'></label></td>
																	</tr>
																	<tr>           
																		<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'></label></td>
																		<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'></label></td>
																	</tr> 
																	<tr id="arrivalFlightTemplate">
																		<td align="center" rowspan="1" class="defaultRowGap rowColor bdLeft bdBottom bdRight">	
																			<input type="checkbox" value=""  class="NoBorder"  id="radIn"/>
																		</td>
																		<td width='35%' class='defaultRowGap rowColor bdRight bdBottom alignLeft'><label id="segmentCode"></label></td>
																		<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="depatureDate"></label></td>
																		<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="depatureTime"></label></td>
																		<td width='15%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
																		<td width='8%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
																		<td width='10%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
																		<td width='9%' class='defaultRowGap rowColor bdRight bdBottom' align='center'><img id="carrierImagePath" src=""/></td>
																	</tr>                
																</table>
																<table  width="100%" border="0" cellspacing="1" cellpadding="1" id="tableIBNoFlight">
																	<tr>
																		<td align="center">
																			<br/><br/>
																			<label id="" class="fntBold">No flight is available on the selected date. Please &nbsp;Select alternative dates. &nbsp; </label>
																			<br/>
																			    <a title="Click here to search previous day flights" id="linkCenterPreviousReturn" href="#">																																				
																					<u><label class="fntBold fntRed cursorPointer" id="lblPreviousDay"></label></u>
																				</a>
																				&nbsp;&nbsp;
																				<a title="Click here to search next day flights" id="linkCenterNextReturn" href="#">
																					<u><label class="fntBold fntRed cursorPointer" id="lblNextDay"></label></u>																	
																				</a>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr id="trLocalTimeAirportMsgIB">
															<td colspan='2' class="alignLeft">
																<label class="fntItalic" id="lblLocalTimeAirport"></label>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan='2' class='rowGap'></td>
								</tr>
								<%-- Fare Quote Button --%>
								<tr>	
									<td align="center" colspan="2">		
										<input type="button"  class="Button ButtonMedium" value="Find Fare" title="Click here to Find fare" id="btnReCalculate"/>	
									</td>
								</tr>
								<tr>
									<td colspan='2' class='rowGap'></td>
								</tr>
								<tr id="trPriceBDPannel">
									<td>									
										<!-- Price Break Down -->
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>	
														<tr>
															<td class='pnlBottomTall'  colspan='2'>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
																	<tr>
																		<td class="alignLeft">																		
																			<label id="lblPriceBreakDown" class="fntBold hdFontColor paddingL5"></label>&nbsp;<label id="lblCurrencySupportMessage"></label>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
																				<tr>		
																					<td align="center" class="gridHD">
																						<label id="lblOnd" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" class="gridHD">
																						<label id="lblPassengerType" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblFare" class='gridHDFont fntBold'></label>																							
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblCharges" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="50" class="gridHD">
																						<label id="lblNoOfPax" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblTotal" class='gridHDFont fntBold'></label>																						
																					</td>	
																				</tr>	
																				<tr id="priceBreakDownTemplate">		
																					<td rowspan="1" align="left" class="GridItems">
																						<label id="ond">	</label>
																					</td>
																					<td class="GridItems" align="left">
																						<label id="paxType"></label>
																					</td>		
																					<td align="right" class="GridItems">
																						<label id="fare"></label>
																					</td>		
																					<td align="right" class="GridItems">
																						<label id="tax"></label>
																					</td>		
																					<td align="center" class="GridItems">
																						<label id="noPax"></label>
																					</td>		
																					<td align="right" class="GridItems">
																						<label id="total"></label>
																					</td>	
																				</tr>																																					
																		</table>
																		<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">
																			<tr>		
																					<td align="left" class="GridHighlight">
																						<a href="#">
																							<label class="fntLink" id="lblFareRules"></label>
																						</a>																						
																					</td>
																					<td>
																					</td>
																					<td width="100">
																					</td>	
																					<td width="100">
																					</td>	
																					<td align="right" width="50" class="GridHighlight">
																						<label id="lblTotal" class="fntBold uppercase"></label>
																					</td>		
																					<td align="right" width="100" class="GridHighlight">
																						<label class="fntBold" id="totalAmount"></label>
																					</td>	
																				</tr>	
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='2' class='rowGap'></td>
																</tr>	
																<tr>
																	<td class="alignLeft">																		
																			<label id="lblReCalMsg" class="fntItalic"></label>
																	</td>
																</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td colspan='2' class='rowGap'></td>
														</tr>													
													</table>
												</td>
											</tr>										
												
										</table>
									</td>
								</tr>
								<%-- Importance Notice --%>
								<tr id="trTermsNCond">
									<td>						
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>	
														<tr>
															<td class='pnlBottomTall'  colspan='2'>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
																	<tr>
																		<td class="alignLeft">																																					
																			<label  class="fntBold hdFontColor paddingL5"  id="lblImportantMsg"></label>
																		</td>
																	</tr>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>	
																	<tr>
																		<td class="paddingR5">
																			<label  id="termsNCond"></label>
																		</td>
																	</tr>
																</table>																	
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>					
								</tr>
								<tr id="trAccept">
									<td>
										<table>
											<tr>
												<td align="right">												
													<label id="lblAcceptMsg1"></label>&nbsp;
														<a href="#" id="linkTerms"><u>
															<label class="hdFontColor" id="lblAcceptMsg2"></label></u>
														</a>											
												</td>
												<td width="2%">												
													<input type="checkbox" title="Click here if you agree with terms and conditions"  class="noBorder" name="chkTerms" id="chkTerms"/>
												</td>	
											</tr>
										</table>
									</td>																																			
								</tr>
								<tr>
									<td  class='rowGap'></td>
								</tr>
								<tr>
									<td align='center'>
										<!-- Button -->
										<table border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td><input type="button" id="btnSOver"  class="Button"/></td>					
												<td><input type="button" id="btnContinue"  class="Button" /></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<!-- Bottom AA Logo -->
					<tr>
					<td class='appLogo' colspan="2"></td>
					</tr>
					<c:import url="../../../../ext_html/cfooter.jsp" />
				</table>
			</td>
		</tr>
	</table>
	<%@ include file='../common/iBECommonParam.jsp'%>
	<%@ include file='../common/reservationParam.jsp'%>
	<%--Set Next/Previous/Return Status --%>	
	<input type="hidden" name="blnNextPrevious"  id="blnNextPrevious" value="false"/>
	<input type="hidden" name="blnReturn"  id="blnReturn" value="false"/>
 </form>
<script src="../js/v2/reservation/flightFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
</div>
 </body>
</html>