<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
	<%@ page pageEncoding="UTF-8" %> 
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<head>  
<%@ include file='../common/interlinePgHD.jsp' %>
</head>
    <body>
    <table width="100%" ><tbody>
	<tr>
		<td valign="top">
			<table width="100%"><tbody><tr><td>
				<iframe id="wirecardPaymentInput" class="paymentCardInputs" src="showBlank" style="position: relative;top: 0px;left: 0px" frameborder="0" marginwidth="0" marginheight="0"  width="100%">
				</iframe>
				</td></tr></tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>
  	</body>  
  	<script type="text/javascript">
  	var responseText = "<c:out value='${requestScope.postInputDataFormHtml}' escapeXml='false' />";
  	if (responseText.indexOf("http:") == 0 || responseText.indexOf("https:") == 0){
  		$("#wirecardPaymentInput").attr("src",responseText);
  	}else{
   		var ifrm = document.getElementById('wirecardPaymentInput');
 		ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
 		ifrm.document.open();
 		ifrm.document.write(responseText);
 		ifrm.document.close();
  	}
 		$("#wirecardPaymentInput").attr("height",parent.ui_paymentGWManger.iframesHeight[parent.UI_Payment.paymentGatewayID]);
  	</script>
</html>