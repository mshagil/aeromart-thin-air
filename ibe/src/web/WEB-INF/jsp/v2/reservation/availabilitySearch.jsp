<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!--
	<c:if test='${sessionScope.sessionDataDTO.carrier == "G9" }'>
		<c:if test='${param.version=="1"}'>
			 Google Website Optimizer Tracking Script 
			<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['gwo._setAccount', 'UA-19350552-2']);
			  _gaq.push(['gwo._trackPageview', '/1698405054/test']);
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
			</script>
			 End of Google Website Optimizer Tracking Script 
		</c:if>
		<c:if test='${param.version!="1"}'>
			 Google Website Optimizer Control Script 
			<script>
			function utmx_section(){}function utmx(){}
			(function(){var k='1698405054',d=document,l=d.location,c=d.cookie;function f(n){
			if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return escape(c.substring(i+n.
			length+1,j<0?c.length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;
			d.write('<sc'+'ript src="'+
			'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com'
			+'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='
			+new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
			'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
			</script><script>utmx("url",'A/B');</script>
			 End of Google Website Optimizer Control Script 
			 Google Website Optimizer Tracking Script 
			<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['gwo._setAccount', 'UA-19350552-2']);
			  _gaq.push(['gwo._trackPageview', '/1698405054/test']);
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
			</script>
			 End of Google Website Optimizer Tracking Script 
		</c:if>
	</c:if>
	-->
	<%@ include file='../common/interlinePgHD.jsp' %>
	<link rel="stylesheet" type="text/css" href="../css/jquery.calendarview_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel="stylesheet" type="text/css" href="../css/remove-jquery_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<link rel="stylesheet" type="text/css" href="../css/jquery.bt_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<script src="../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script type="text/javascript" src="../js/v2/common/jquery.combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/v3/common/jquery.calendarview.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jquery.summaryPanel.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" type="text/javascript"></script>	
	<script type="text/javascript" src="../js/v2/common/jquery.stiky.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<script type="text/javascript">
	    var strReqParam = '<c:out value="${requestScope.sysReqParamAA}" escapeXml="false" />';
	    var fareType_IMG_PATH = '<c:out value="${requestScope.sysImagePath}" escapeXml="false" />';
		var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;
		var paxJason = null;
			
		//TODO Refactor-don't add more
		var objCWindow = "";
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}

		var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
		var strToDate	= strSysDate;
		if (UI_Top.holder().GLOBALS.currentDate != null && UI_Top.holder().GLOBALS.currentDate == "") {
			var date = UI_Top.holder().GLOBALS.currentDate;
			var dtTempSysDate = new Date(date.substr(6,4), (Number(date.substr(3,2)) - 1), date.substr(0,2));
			strSysDate  	= DateToString(addDays(dtTempSysDate, 0));
			dtSysDate		= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
			strToDate       = dtSysDate; 
		}	
		var bookingStepsArray = ["Select a Flight", "Passenger details", "Personalise ", "Payment", "Print & Fly"];
		
		var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		
		var strPGWPaymentMobileNumber= '<c:out value="${requestScope.fltPGWPaymentMobileNumber}" escapeXml="false" />';
		var strPGWPaymentEmail = '<c:out value="${requestScope.fltPGWPaymentEmail}" escapeXml="false" />';
		var strPGWPaymentCustomerName = '<c:out value="${requestScope.fltPGWPaymentCustomerName}" escapeXml="false" />';
		
	</script>
	
	<c:if test="${not empty(requestScope.paxJason)}">
	    <script type="text/javascript">
	       paxJason = <c:out value="${requestScope.paxJason}" escapeXml="false" />;
	    </script>	
    </c:if>	
    
</head>
<body>
<div id="divLoadBg" style="display: none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgFares">
		<tr>
			<td align='center' class="outerPageBackGround">
				<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
					<!-- Top Banner -->
					<c:import url="../../../../ext_html/header.jsp" />
					<!-- Content holder -->
					<tr>
					<td colspan="2" class="mainbody">
					<div id="sortable-ul">
					<div class="sortable-child">
					<!-- Left Column -->
					<div style="background: #fff" class="floater">
						<div id="divSummaryPane">
							<div class="bookingSummary"></div>
							<div class="paymentSummary"></div>
						</div> 
							<%-- <%@ include file="inc_tabSummaryPane.jsp" %> --%>
						
						</div>
					&nbsp;
					</div>
					<div class="sortable-child">
					<!-- Right Column -->
					<div class="rightColumn">
						<div class="page-body" style="width: 100%;">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
									<tr>
										<td class='pnlTop pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>
									<tr>
										<td class='pnlBottomTall'  colspan='2' valign="top" align="center">
										 <table width="100%" border="0" cellspacing="0" cellpadding="0">
										 	<tr>
											<td colspan="2" class="alignLeft">
											<div class="" style="width: 100%;" class="stepsContainer">
												<c:import url="../../../../ext_html/booking_process.jsp" />
											</div>
											</td>
											</tr>
											<tr>
											<td class="rowSingleGap">
													</td>
											</tr>
											<tr>
											<td class="pageName alignLeft paddingCalss" colspan="2">
												<label id="lblSelectFlight">Select a Flight</label>
											</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											<tr id="browserMsgTr">
												<td colspan='2' class="alignLeft paddingCalss">
													<div class="spMsg" id="browserMsgTxt"></div>
													<br/>
												</td>
											</tr>
										 	<tr>
										 		<td class="alignLeft paddingCalss" ><font>
										 		<label id="lblSelectFlightMsg">
										 		
										 		</label>
										 		</font>
										 		</td>
										 	</tr>
										 	<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											<tr>
												<td class="alignLeft paddingCalss" colspan="2">
													<div class="spMsg" id="searchPageAirportMsg">
														
													</div>
												</td>
											</tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											<tr><td class="alignLeft">
											<%-- Calendar View --%>
											<div id="tabs1">
												
											</div>
											</td></tr>
											<tr>
												<td colspan='2' class='rowGap'></td>
											</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
							<div id="FlightDetailsPanel">
							<%-- Flight Details --%>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class='pnlTop pnlWidth'></td>
										<td class='pnlTopR'></td>
									</tr>	
									<tr>
										<td class='pnlBottomTall'  colspan='2' valign="top">
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td class="alignLeft">																		
													<label id="lblFlightDetails" class="fntBold hdFontColor paddingL5">Flight Details</label>
												</td>
											</tr>	
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable" >
														<tr>
															<!-- <td width="5%" rowspan="2" class="gridHD"></td>-->
															<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
															<td colspan="2" align="center" class='gridHD'><label id="lblDepature" class='gridHDFont fntBold'></label></td>
															<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
															<td rowspan="2" align='center' class='gridHD'><label id="lblFlightNo" class='gridHDFont fntBold'></label></td>
															<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														</tr> 
														<tr>            
															<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align='center'><label id="lblDate" class='gridHDFont fntBold'></label></td>
															<td class='gridHDDark' align='center'><label id="lblTime" class='gridHDFont fntBold'></label></td>            
														</tr>
														<tr id="tableOBIB">
															<!--<td align="center" rowspan="1" class="defaultRowGap rowColor bdLeft bdBottom bdRight">	
																<input type="checkbox" value=""  class="NoBorder"  id="radOut"/>
															</td>-->
															<td width='35%' class='rowColor bdRight bdBottom alignLeft'><label  id="segmentCode"></label></td>
															<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="departureDate"></label></td>
															<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="departureTime"></label></td>
															<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
															<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
															<td width='10%' class='rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
															<!--<td width='9%' class='rowColor bdRight bdBottom' align='center'><img id="carrierImagePath" src=""/></td>
														--><td width='9%' class='rowColor bdRight bdBottom' align='center'><label id="duration"></label></td>
														</tr>               
													</table>
												</td>												
											</tr>
											<tr><td> </td></tr>
											<tr>
												<td>
													<label id="lblLocalTimeAirport"></label>
												</td>
											</tr>
										</table>
									</td>
									</tr>
									<tr>
										<td colspan="2" class='rowGap'></td>
									</tr>
								</table>
							</div>
							<div id="trPriceBDPannel">
										<%-- Price Break Down --%>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>	
														<tr>
															<td class='pnlBottomTall'  colspan='2' valign="top">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
																	<tr>
																		<td class="alignLeft">																		
																			<label id="lblPriceBreakDown" class="fntBold hdFontColor paddingL5"></label>&nbsp;<label id="lblCurrencySupportMessage"></label>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
																				<tr>		
																					<td align="center" width="200px" class="gridHD">
																						<label id="lblOnd" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" class="gridHD">
																						<label id="lblPassengerType" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblFare" class='gridHDFont fntBold'></label>																							
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblCharges" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="50" class="gridHD">
																						<label id="lblNoOfPax" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblTotal" class='gridHDFont fntBold'></label>																						
																					</td>	
																				</tr>	
																				<tr id="priceBreakDownTemplate">		
																					<td rowspan="1" width="200" class="GridItems alignLeft">
																						<label id="ond">	</label>
																					</td>
																					<td class="GridItems" align='center'>
																						<label id="paxType"></label>
																					</td>		
																					<td class="GridItems alignRight">
																						<label id="fare"></label>
																					</td>		
																					<td class="GridItems alignRight">
																						<label id="sur"></label>
																					</td>		
																					<td align="center" class="GridItems">
																						<label id="noPax"></label>
																					</td>		
																					<td class="GridItems alignRight">
																						<label id="total"></label>
																					</td>	
																				</tr>																		
																																																																										
																		</table>
																		<table cellspacing="0" cellpadding="2" border="0" width="100%" class="GridTable">
																			<tr style="display: none;" id="promoDiscountPanel">
																				<td colspan="2" class="alignRight" class="GridHighlight alignRight" style="border-right:1px solid #e4e4e4">
																					<label id="lblPromoDiscount" class="fntBold"></label>
																				</td>
																				<td width="100" class="GridHighlight alignRight"><label id="promoDiscountAmount" class="fntBold"></label></td>
																			</tr>
																			<tr>		
																					<td width="125px" class="GridHighlight alignLeft">
																						<a href="javascript:void(0);">
																							<u><label class="fntLink hdFontColor" id="lblFareRules"></label></u>
																						</a>																						
																					</td>																																										
																					<td  class="GridHighlight alignRight" style="border-right:1px solid #e4e4e4">
																						<label id="lblTotal" class="fntBold uppercase"></label>
																					</td>		
																					<td  width="100" class="GridHighlight alignRight">
																						<label class="fntBold" id="totalAmount"></label>
																					</td>	
																			</tr>
																			<tr style="display: none;" id="selectedTotalPanel">		
																					<td width="125px" class="GridHighlight alignLeft">																																												
																					</td>			
																					
																					<td   class="GridHighlight alignRight" style="border-right:1px solid #e4e4e4">
																						<label id="lblTotal" class="fntBold uppercase"></label>
																					</td>		
																					<td  width="100" class="GridHighlight alignRight">
																						<label class="fntBold" id="totalAmountSel"></label>
																					</td>	
																			</tr>	
																			<tr style="display: none;" id="promoDiscountPanelCredit">
																				<td colspan="2" class="alignRight" class="GridHighlight alignRight" style="border-right:1px solid #e4e4e4">
																					<label id="lblPromoDiscount" class="fntBold lblPromoDiscount"></label>
																				</td>
																				<td width="100" class="GridHighlight alignRight"><label id="promoDiscountAmount" class="fntBold promoDiscountAmount"></label></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan='2' class='rowGap'></td>
																</tr>	
																
																
																<%--tax and surcharge bd data --%>

																
																<tr id="taxBDDisplayLinkRow" class="taxBDDisplayLinkRows">
																		<td class="alignLeft">	
																			<a id="taxBDDisplayLink" href="javascript:void(0);">																	
																				<u><label id="lblTaxBreakDown" class="fntLink hdFontColor"></label></u>
																			</a>
																		</td>
																</tr>
																
																<tr class="taxBDDisplayRows">
																		<td>
																			<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
																				<tr>		
																					<td align="center" class="gridHD">
																						<label id="lblTaxBDApp" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" class="gridHD">
																						<label id="lblTaxBDOpCarrier" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="150" class="gridHD">
																						<label id="lblTaxBDCode" class='gridHDFont fntBold'></label>																							
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblTaxBDCharge" class='gridHDFont fntBold'></label>																						
																					</td>	
																				</tr>	
																				<tr id="taxBreakDownTemplate">		
																					<td rowspan="1" align="center" class="GridItems">
																						<label id="applicableToDisplay">	</label>
																					</td>
																					<td class="GridItems" align="center">
																						<label id="carrierCode"></label>
																					</td>		
																					<td align="center" class="GridItems">
																						<label id="taxCode"></label>
																					</td>		
																					<td align="right" class="GridItems">
																						<label id="amount"></label>
																					</td>	
																				</tr>																																					
																			</table>																		
																		</td>
																</tr>
																
																
																
																<tr class="surchargeBDDisplayRows">
																	<td colspan='2' class='rowGap'></td>
																</tr>
																
																<tr id="surchargeBDDisplayLinkRow" class="surchargeBDDisplayLinkRows">
																		<td class="alignLeft">																							
																			<a id="surchargeBDDisplayLink" href="javascript:void(0);">																	
																				<u><label id="lblSurchargeBreakDown" class="fntLink hdFontColor"></label></u>
																			</a>	
																		</td>
																</tr>
																
																<tr class="surchargeBDDisplayRows">
																		<td>
																			<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable">	
																				<tr>		
																					<td align="center" class="gridHD">
																						<label id="lblSurchargeBDApp" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" class="gridHD">
																						<label id="lblSurchargeBDOpCarrier" class='gridHDFont fntBold'></label>																						
																					</td>		
																					<td align="center" width="150" class="gridHD">
																						<label id="lblSurchargeBDCode" class='gridHDFont fntBold'></label>																							
																					</td>		
																					<td align="center" width="100" class="gridHD">
																						<label id="lblSurchargeBDCharge" class='gridHDFont fntBold'></label>																						
																					</td>	
																				</tr>	
																				<tr id="surchargeBreakDownTemplate">		
																					<td rowspan="1" align="center" class="GridItems">
																						<label id="applicableToDisplay"></label>
																					</td>
																					<td class="GridItems" align="center">
																						<label id="carrierCode"></label>
																					</td>		
																					<td align="center" class="GridItems">
																						<label id="surchargeCode"></label>
																					</td>		
																					<td class="alignRight GridItems">
																						<label id="amount"></label>
																					</td>	
																				</tr>																																						
																		</table>																		
																	</td>
																</tr>

																
																<tr>
																	<td class="alignLeft spMsg">																		
																			<label id="lblReCalMsg"></label>
																	</td>
																</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td colspan='2' class='rowGap'></td>
														</tr>													
													</table>
												</td>
											</tr>										
												
										</table>
								</div>
								
								<%--Modify Balance Summary  --%>
								<c:if test="${param.modifySegment == true}">
									<c:import url="../modifyRes/modifySegmentBalance.jsp"></c:import>
								</c:if>
								
								<%-- Flexi Message --%>
								<div id="divFlexiMessage" style="display: none;">
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td class='rowGap'></td>
										</tr>
										<tr>
											<td class="alignLeft paddingCalss" >
												<font class="fntEnglish fntBold">Remaining Flexibilities: <span id="spnFlexibilities" style="color: red;">No more flexibilities available for the cancelled segment</span></font>
											</td>
										</tr>	
										<tr>
											<td class='rowGap'></td>
										</tr>
									</table>
									
								</div>
								<%-- Onhold Booking State Message --%>
				
				               <div id="onholdBookingStateMessage" style="display: none;">
				                 <table width='100%' border='0' cellpadding='0' cellspacing='0'>
					               <tr>
						            <td class='rowGap'></td>
					                </tr>
					                <tr>
						              <td class="alignLeft spMsg">
						                <div>
							             <label id="lblOnHoldState">
								          
							             </label>
						               </div>
						              </td>
					                </tr>
					               <tr>
						             <td class='rowGap'></td>
					               </tr>
				                </table>
				              </div>								
								
								<%-- Importance Notice --%>
								<div id="trTermsNCond">
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class='pnlTop pnlWidth'></td>
															<td class='pnlTopR'></td>
														</tr>	
														<tr>
															<td class='pnlBottomTall'  colspan='2'>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">													 
																	<tr>
																		<td class="alignLeft paddingCalss">																																					
																			<label  class="fntBold hdFontColor paddingL5"  id="lblImportantMsg"></label>
																		</td>
																	</tr>
																	<tr>
																		<td colspan='2' class='rowGap'></td>
																	</tr>	
																	<tr>
																		<td class="paddingR5">
																			<label  id="termsNCond"></label>
																		</td>
																	</tr>
																</table>																	
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class='rowGap'></td>
											</tr>
										</table>
								</div>
									<div id="trAccept">
											<table>
												<tr>
													<td class="alignRight">												
														<label id="lblAcceptMsg1"></label>&nbsp;
															<a href="#" id="linkTerms"><u>
																<label class="hdFontColor" id="lblAcceptMsg2"></label></u>
															</a>
														<label id="lblAcceptMsg3"></label>										
													</td>
													<td width="2%">												
														<label><input type="checkbox" title="Click here if you agree with terms and conditions" name="chkTerms" id="chkTerms"/></label>
													</td>	
												</tr>
											</table>
									</div>
									
									
									<!-- Image Captcha imblude -->
								<%@include file="../../v3/common/captchaValidation.jsp" %>

									
									<div  class='rowGap'></div>
									<div class="buttonset">
										<%-- Button --%>
										<table border='0' cellpadding='0' cellspacing='0' width="100%" >
											<tr>
												<td class="alignLeft"><input type="button" id="btnSOver"  class="Button <fmt:message key='msg.res.startover.class'/>"/></td>
												<td style="width: 10px;">&nbsp;</td>					
												<td class="alignRight"><input type="button" id="btnContinue"  class="Button" /></td>
											</tr>
										</table>

								</div>
								<div  class='rowGap'></div>
								<div id="trPaymentMethods" style="display: none;">
								    <label id="lblPayingoptions"> You can pay by: </label>
								    <table width='100%' border='0' cellpadding='0' cellspacing='0'>
								
								        <tr>
								            <td class="paddingCalss" align="center"><span id="payMethods" class="card-img"></span>
								
								            </td>
								        </tr>
								
								    </table>
								    <div class='rowGap'></div>
								</div>
							</div>
							</div>
							</div>
						</td>
					</tr>
					
					<%-- Bottom AA Logo --%>
					<tr>
						<td class='appLogo' colspan="2"></td>
					</tr>
					<c:import url="../../../../ext_html/cfooter.jsp" />
				</table>
			</td>
		</tr>
	</table>
	<div class="loadingContainer">
	<div class="newPogressLoading">
		<table width="100%" border="0" height="100%" style="background: #fff">
			<tr>
				<td valign="middle" align="center">
					<img src="../images/Loading_no_cache.gif" alt="loading.." />
				</td>
			</tr>
		</table>
	</div>
	</div>
 <form action="" id="frmFare" method="post">
	<%@ include file='../common/iBECommonParam.jsp'%>
	<div id="searchSubmitParams" name="searchSubmitParams">
		<%@ include file='../common/reservationParam.jsp'%>
		<%--Modify Reservation Parameters --%>
		<c:if test="${(param.modifySegment == true) || (param.addGroundSegment == true)}">	
			<%@ include file="../common/modifyReservationParam.jsp"%>
			<input type="hidden" id="mode" name="mode" value="false"/>		
		</c:if>		
	</div>
	<div id="pgwDetails" name="pgwDetails">
		<input type="hidden" name="pgwPaymentMobileNumber"  id="pgwPaymentMobileNumber" escapeXml="false"/>
		<input type="hidden" name="pgwPaymentEmail"  id="pgwPaymentEmail" escapeXml="false"/>
		<input type="hidden" name="pgwPaymentCustomerName"  id="pgwPaymentCustomerName" escapeXml="false"/>
	</div>
	<input type="hidden" name="blnNextPrevious"  id="blnNextPrevious" value="false"/>
	<input type="hidden" id="resFlexibleDates" name="flexibleDates" value='<c:out value="${param.flexibleDates}" escapeXml="false"/>' />
	<input type="hidden" id="fromSecure" name="fromSecure" value='<c:out value="${param.fromSecure}" escapeXml="false"/>'/>	
	<input type="hidden" id="resFlexiAlerts" name="resFlexiAlerts" value='<c:out value="${param.resFlexiAlerts}" escapeXml="false"/>'/> 
	<input type="hidden" id="requestSessionIdentifier" name="requestSessionIdentifier" value='<c:out value="${param.requestSessionIdentifier}" escapeXml="false"/>'/> 			
 </form>
 
 <form id="frmTemp" method="post"> 
 </form>
<script src="../js/calDisplayConfig.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
<c:if test="${not empty(requestScope.sysOndSource)}">
<script src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>" type="text/javascript"></script>		
</c:if>	
<script src="../js/v2/reservation/availabilitySearchSupportV2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/common/captchaValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/v2/reservation/availabilitySearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

</div>
<div id='divLoadMsg' class="mainPageLoader" style="display: none">
	<%@ include file='../common/includeLoadingMsg.jsp' %>
</div>
<%-- Used to load third party tools and tracking codes--%>
<div id="accelAeroIBETrack" style="display: none">
	<iframe name="frmTracking" id="frmTracking" src="showBlank" frameborder="0">
	</iframe>	
</div>
</body>
<%@ include file='../common/inline_Tracking.jsp' %>
</html>