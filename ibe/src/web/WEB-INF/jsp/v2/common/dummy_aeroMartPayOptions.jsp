<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>aeroMart Pay Payment: Test Page
</head>

<body>
	<br />
	<br />
	<br />
	<br />

	<script type="text/javascript">
    function submitDirectPaymentSuccess() {
      document.getElementById("paymentgatewayId").value = "4";
      document.getElementById("cardType").value = "1";
      document.getElementById("cardNumber").value = "5399999999999999";
      document.getElementById("cardHolderName").value = "Kana";
      document.getElementById("cvv").value = "123";
      document.getElementById("expiryDate").value = "0823";
      document.forms["aeroMartPayHandlePayment"].submit();
    }

    function submitRedirectPaymentSuccess() {
      document.getElementById("paymentgatewayId").value = "4";
      document.getElementById("cardType").value = "2";
      document.getElementById("cardNumber").value = "4000000000000002";
      document.getElementById("cardHolderName").value = "Kana";
      document.getElementById("cvv").value = "123";
      document.getElementById("expiryDate").value = "0823";
      document.forms["aeroMartPayHandlePayment"].submit();
    }
    
    function submitPaymentFailure() {
      document.forms["aeroMartPayHandlePayment"].submit();
    }
  </script>


	<button type="button" onclick="submitDirectPaymentSuccess();">Simulate Direct
		Payment</button>
	<br />
	<br />
	
	<button type="button" onclick="submitRedirectPaymentSuccess();">Simulate Redirect
		Payment</button>
	<br />
	<br />

	<button type="button" onclick="submitPaymentFailure();">Simulate
		Payment Failure</button>


	<form id="aeroMartPayHandlePayment" name="aeroMartPayHandlePayment"
		method="post"
		action='<%=request.getAttribute("handleIPGPaymentURL")%>'>
		<input type="hidden" name="paymentgatewayId" id="paymentgatewayId"
			value="" /> <input type="hidden" name="cardType" id="cardType"
			value="" /> <input type="hidden" name="cardNumber" id="cardNumber"
			value="" /> <input type="hidden" name="cardHolderName"
			id="cardHolderName" value="" /> <input type="hidden" name="cvv"
			id="cvv" value="" /> <input type="hidden" name="expiryDate"
			id="expiryDate" value="" /> <input type="hidden" name="mobileNumber"
			id="mobileNumber" value="" /> <input type="hidden" name="email"
			id="email" value="" />
	</form>


</body>
</html>