<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<%@ include file='../common/cacheClear.jsp'%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<%@ include file='../common/interlinePgHD.jsp' %>	
	<link rel='stylesheet' type='text/css' href='../css/myStyle_no_cache.css'/>	
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v3/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</head>
<body>

	<script type="text/javascript">
	  
	  var loadResUrl = "<%=AppParamUtil.getSecureIBEUrl()%>" + "showReservation.action";
	  var voucherUrl = "<%=AppParamUtil.getSecureIBEUrl()%>" + "handleInterlinePayFort!showVoucher.action?"
	  var language = "<c:out value='${requestScope.language}' escapeXml='false' />";
	  var pnr = "<c:out value='${requestScope.pnr}' escapeXml='false' />";
	  var voucher_number = "<c:out value='${requestScope.voucherID}' escapeXml='false' />";
	  var request_id = "<c:out value='${requestScope.requestID}' escapeXml='false' />";
	  var lang = 'en';
	  var _rnd = Math.random() ;
	  var _proto = (("https:" == document.location.protocol) ? "https:" : "http:");
	  
	  $(document).ready(function(){
		 document.getElementById("voucherDisplay").src = voucherUrl +"voucherID="+"<c:out value='${requestScope.voucherID}' escapeXml='false' />"+"&requestID="+"<c:out value='${requestScope.requestID}' escapeXml='false' />"+"&payAtstoreScriptUrl="+"<c:out value='${requestScope.payAtstoreScriptUrl}' escapeXml='false' />";
		 if(typeof $("#loadResLink") !== 'undefined'){
			 
			 $("#hdnParamData").val(language + '^DIRECTLOARDPNR^');
			 $("#hdnPnr").val(pnr);
			 $("#form1").attr('action',loadResUrl);
			 
			 $("#loadResLink").click(function(){
				 $("#form1").submit(); 
				 return false;
			 });			 
		 }
		 
			$("#linkHome","#btnFinish").click(function(){ SYS_IBECommonParam.homeClick() });
			$("#btnFinish").unbind("click").click(function(){ SYS_IBECommonParam.homeClick() });
			var hideProgressTimeRef =  setTimeout('hideprogress()', 5000);
			
	  });
	  
	  function hideprogress(){
		 parent.UI_commonSystem.loadingCompleted();
		 clearTimeout(hideProgressTimeRef);
		
	  }
	  
	  
	</script>
	
	<form id="form1" target="_blank" method="post" action="">
		<input type="hidden" id="hdnParamData" name="hdnParamData" value="">
		<input type="hidden" id="hdnPnr" name="pnr" value="">
	</form>
	
    <div id="mainContainer">
    <%-- Re-enter card details --%>
    
    <style type="text/css">
		table, a {
			font-size: 10pt;
		}
	</style>
	
    <div id="divLoadBg">  			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<!-- Top Banner -->
		<c:if test="${requestScope.displayBanner != true}">
			<c:import url="../../../../ext_html/header.jsp" />
		</c:if>
			<tr>
				<td align='center' class="outerPageBackGround">
					<table style='width:940px; background-color: #EFFFF0; border: 1px solid #AAA;' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
						<tr><td class="mainbody">
									<div class="differError" style="display: block;margin:20px auto;width:94%;-moz-border-radius:6px 6px 6px 6px;">
												<table width="100%" border="0" cellpadding="2" cellspacing="2">
													<tr>
														<td style="height:100px;padding-left: 8%;" class="alignLeft" width="85%" valign="middle" >
															<c:out value="${requestScope.offlinePaymentInformation}" escapeXml="false"/>	
														</td>
													</tr>
													<tr>
														<td align="center" width="100%"  height="100%" valign="middle">
															<div>
																<iframe align="middle" id="voucherDisplay" src="" width="95%" height="750" scrolling="no" seamless="seamless">
																</iframe>
															</div>	
														</td>
													</tr>
												</table>
									</div>
									
								</td>
						</tr>						
					</table>									
				</td>
			</tr>
			<tr id="trButtonPannel">
							<td>
								<div class="buttonset">
									<!-- Button -->
										<table border='0' cellpadding='0' cellspacing='0' width="100%">
											<tr>
												<td>&nbsp;&nbsp;</td>
											</tr>
											<tr>
												<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td class="alignLeft">
													<u:hButton name="btnFinish" id="btnFinish" value="Finish"  cssClass="backPrevious"/>
													<!--<input type="button" id="linkHome"  value="Finish" class="Button"/>-->
												</td>
												<td class="alignRight">
													<!--<u:hButton name="btnPrint" id="btnPrint" value="Print"  cssClass="backPrevious"/>
													<input type="button" id="btnPrint"  value="Print" class="Button" />-->
												</td>
											</tr>
										</table>
									</div>
							</td>
						</tr>	
		<c:if test="${requestScope.displayBanner != true}">
			<tr>
				<td class='appLogo' colspan="2"></td>
			</tr>
		<c:import url="../../../../ext_html/cfooter.jsp" />
		</c:if>	
		</table>	
		<%@ include file='../common/iBECommonParam.jsp'%>
		</div>	
	</div>	
</body>
</html>