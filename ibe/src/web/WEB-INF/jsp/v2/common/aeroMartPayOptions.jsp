<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="LTR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>aeroMart Payment Page</title>
    <!-- Vendor libraries -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/aeroMartPaymentPage.css">
	<script	src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
	<script	src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
	<script	src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
	<script	src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
	<script	src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
	<script src="../js/common/cardValidator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script	src="../js/v3/aeroMARTpay/aeroMARTpay.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>


</head>

<body class="bg-gray">
	<script type="text/javascript">  
		var paymentOptions = <c:out value="${requestScope.paymentOptions}" escapeXml="false" />;
	  	var messages = <c:out value="${requestScope.messages}" escapeXml="false" />;
	</script>
	<div id="overLayingMask">
		<div id="loadImage">	
		</div>
	</div>
    <div class="topBanner">
        <div style="margin: 0 auto" class="alignLeft col-sm-12">
            <img src="../images/aeroMARTPay/LIbe00501092015.png" alt="logo" id="lnkHomeBanner" class="cursorPointer" style="margin-top:12px;">
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 bg-white margin-top-40 padding-15">
                <div class="select-payment-method waypoint_PaymentOption" ng-hide="paymentCtrl.payOnlyFromLMS">
                    <div class="col-md-8 col-sm-12 col-xs-12 pd-l-0">
                        <h2 translate="msg_payment_paymentMethod" class="ng-scope">Payment Details</h2>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pd-r-0">
                        <div class="secure-certificate">
                            <div class="data">
                                <!-- <div class="pull-right">
                              <span translate="lbl_payment_secure128" class="secure-title">Secure 128-bit encrypted payment</span>
                              <span class="ico"></span>
                              </div> -->
                                <!-- Cerificates -->
                                <div class="certificates">
                                    <span><img src="../images/aeroMARTPay/payment-logos/verisign.png"></span>
                                    <span><img id="master-card" src="../images/aeroMARTPay/payment-logos/master-card.png"></span>
                                    <span><img id="visa-card" src="../images/aeroMARTPay/payment-logos/visa-verified.png"></span>
                                </div>
                                <!-- // Cerificates -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
				<div class="panel-heading purchase-details display-table">              
					<div class="display-tr">
						<h3 class="panel-title display-td">Merchant Name : <c:out value="${sessionScope.aeroMartSessionInfo.merchantName}" escapeXml="false" /></h3>
						<h3 class="panel-title order-num display-td">Order ID : <c:out value="${sessionScope.aeroMartSessionInfo.orderID}" escapeXml="false" /></h3>
					</div>                    
		        </div>
                <div class="clearfix"></div>
                <div class="payment-options">
                    <!-- ngRepeat: payOption in paymentCtrl.paymentOption -->                    
                </div>
				 <div  class="col-md-12" id="divError">
					<ul id="ulError">
					</ul>
				</div>
                <div class="clearfix"></div>
                <div class="payment-details margin-top-20" style="min-height: 500px">
                    <div class="left-content">
                        <section class="master-card-details ng-scope">
            				<form role="form" id="payment-form" method="POST" action='<c:out value="${requestScope.handleIPGPaymentURL}" escapeXml="false" />'>
                                <div class="row">
                                </div>
                                <div class="payment-details" style="min-height: 500px">
                                    <!-- Left content -->
                                    <div class="left-content col-md-7 col-sm-12 col-xs-12">
                                        <!-- ngIf: paymentCtrl.paymentRetry -->
                                        <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
                                        <div style="display: none;">
                                            <input style="display:none" type="text" name="fakeusernameremembered">
                                            <input style="display:none" type="password" name="fakepasswordremembered">
                                        </div>

                                        <section class="master-card-details ng-scope" ng-if="paymentCtrl.selectedPaymentOption.paymentGateway.brokerType !== 'external' &&
                                    (paymentCtrl.selectedPaymentOption.cssClass === 'VISA' || paymentCtrl.selectedPaymentOption.cssClass === 'MASTER') ">
                                            <form novalidate="" isa-validator-type="creditCard"  autocomplete="off" name="paymentCtrl.paymentCard" class="ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-valid-minlength ng-valid-maxlength">
                                                <div class="row">
                                                    <!-- ngIf: paymentCtrl.hasPromoOption -->
                                                    <div class="col-md-12 col-sm-12 col-xs-12 ng-scope" style="margin-bottom: 15px;" ng-if="paymentCtrl.hasPromoOption">
                                                        <span id="pg_description" class="ng-scope"></span>
                                                    </div>
                                                    <!-- end ngIf: paymentCtrl.hasPromoOption -->
                                                    <!-- ngIf: paymentCtrl.showBinPromoText -->
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <label id="div_card_no">
                                                            <span>
                        										<span translate="lbl_payment_cardNum" class="ng-scope">Card Number</span>
                                                            	<i class="red"> *</i>
                                                            </span>
                                                            <div class="row">
                                                                <div class="ng-scope col-xs-12" ng-class="{'col-xs-9': paymentCtrl.showBinPromoText, 'col-xs-12': !paymentCtrl.showBinPromoText}">

                                                                    <input type="text" placeholder="The digits on front of your card"
							                                            class="form-control"
							                                            id="cardNumber"
							                                            name="cardNumber"
							                                            placeholder="Valid Card Number"
							                                            autocomplete="cc-number"
							                                            required="required"
							                                            maxlength="16"
							                                            autofocus 
							                                        >
                                                                    <!-- end ngIf: paymentCtrl.selectedPaymentOption.cssClass === 'MASTER' -->
                                                                    <div class="clearfix"></div>
                                                                    <div id="cardRequiredError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required">Credit card number is required</p>

                                                                    </div>
                                                                    <div id="cardInvalidError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required">Credit card number is invalid</p>

                                                                    </div>
                                                                </div>
                                                                <div ng-class="{'col-xs-3': paymentCtrl.showBinPromoText}">

                                                                    <button id="verify-btn" ng-disabled="!paymentCtrl.paymentCard.cardNumber.$valid" ng-if="paymentCtrl.hasPromoOption" ng-show="paymentCtrl.showBinPromoText" class="button red-btn ng-hide ng-scope" ng-click="paymentCtrl.checkBinPromo()" translate="" disabled="disabled">Verify</button>

                                                                    <div class="modal-box ng-hide" ng-show="paymentCtrl.isPopupBinPromoModalOpen">

                                                                        <div class="overlay"></div>

                                                                        <div class="modal-content" style="min-height: 265px;top:50%">

                                                                            <div class="modal-header">
                                                                                <span class="text ng-scope" translate="lbl_payment_getBinPromotion">Get Bin Promotion</span>
                                                                                <span class="close-btn" ng-click="paymentCtrl.binPromoPopupClose()">
                        <i class="fa fa-times"></i>
                        </span>
                                                                            </div>
                                                                            <!-- // Modal Header -->
                                                                            <div class="modal-relative-wrapper" style="position: relative;width: 100%">
                                                                                
                                                                                <div class="modal-footer">
                                                                                    <div class="row">
                                                                                        
                                                                                        <div ng-if="!paymentCtrl.binPromoModalStatus.promotionApplied && !paymentCtrl.binPromoModalStatus.hasExistingPromotion" class="ng-scope">
                                                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <button class="button light-gray-btn ng-scope" type="button" ng-click="paymentCtrl.binPromoPopupClose()" translate="btn_payment_cancel">Cancel</button>
                                                                                            </div>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                <button class="button red-btn ng-scope" type="button" ng-click="paymentCtrl.applyBinPromo()" translate="btn_payment_applyPromotion">Apply Promotion</button>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <!-- // Modal Footer -->
                                                                                <isa-busy-loader ng-hide="!paymentCtrl.binPromoModalStatus.promotionLoading" class="ng-isolate-scope ng-hide">
                                                                                    <div class="cg-busy cg-busy-animation ng-scope">
                                                                                        <div class="cg-busy cg-busy-backdrop"></div>
                                                                                        <div class="cg-busy-default-wrapper">
                                                                                            <div class="cg-busy-default-sign" ng-style="positionFixed">
                                                                                                <div class="cg-busy-default-spinner">
                                                                                                    <div class="bar1"></div>
                                                                                                    <div class="bar2"></div>
                                                                                                    <div class="bar3"></div>
                                                                                                    <div class="bar4"></div>
                                                                                                    <div class="bar5"></div>
                                                                                                    <div class="bar6"></div>
                                                                                                    <div class="bar7"></div>
                                                                                                    <div class="bar8"></div>
                                                                                                    <div class="bar9"></div>
                                                                                                    <div class="bar10"></div>
                                                                                                    <div class="bar11"></div>
                                                                                                    <div class="bar12"></div>
                                                                                                </div>
                                                                                                <div class="cg-busy-default-text ng-scope" translate="Please Wait...">Please Wait...</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </isa-busy-loader>
                                                                            </div>
                                                                            <!--// Modal Wrapper -->
                                                                        </div>
                                                                        <!-- // Modal Content -->
                                                                    </div>
                                                                    <!-- // Modal Box -->
                                                                </div>
                                                            </div>
                                                        </label>
                                                        <div class="row expire-row" id="div_expiry_date">
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <label>
                                                                    <span>
                        											<span translate="lbl_payment_expirationDate" class="ng-scope">Expiration date</span>
                                                                    <i class="red"> *</i>
                                                                    </span>
                                                                </label>
                                                                <div class="expire-group ng-scope">
                                                                    <select class="form-control ng-pristine ng-untouched ng-valid" ng-click="paymentCtrl.setExpireDate()" ng-model="paymentCtrl.expDateMonth" ng-options="month as month for month in paymentCtrl.expDate.month" name="expDateMonth" id="expDateMonth">
                                                                        <option value="?" selected="selected"></option>
                                                                        <option label="1" value="01">1</option>
                                                                        <option label="2" value="02">2</option>
                                                                        <option label="3" value="03">3</option>
                                                                        <option label="4" value="04">4</option>
                                                                        <option label="5" value="05">5</option>
                                                                        <option label="6" value="06">6</option>
                                                                        <option label="7" value="07">7</option>
                                                                        <option label="8" value="08">8</option>
                                                                        <option label="9" value="09">9</option>
                                                                        <option label="10" value="10">10</option>
                                                                        <option label="11" value="11">11</option>
                                                                        <option label="12" value="12">12</option>
                                                                    </select>
                                                                    <span>/</span>
                                                                    <select class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required" ng-click="paymentCtrl.setExpireDate()" ng-model="paymentCtrl.expDateYear" ng-options="year as year for year in paymentCtrl.expDate.year" name="expDateYear" id="expDateYear" ng-required="true" required="required">
                                                                        <option value="?" selected="selected"></option>
                                                                        <option label="2016" value="16">2016</option>
                                                                        <option label="2017" value="17">2017</option>
                                                                        <option label="2018" value="18">2018</option>
                                                                        <option label="2019" value="19">2019</option>
                                                                        <option label="2020" value="20">2020</option>
                                                                        <option label="2021" value="21">2021</option>
                                                                        <option label="2022" value="22">2022</option>
                                                                        <option label="2023" value="23">2023</option>
                                                                        <option label="2024" value="24">2024</option>
                                                                        <option label="2025" value="25">2025</option>
                                                                        <option label="2026" value="26">2026</option>
                                                                        <option label="2027" value="27">2027</option>
                                                                        <option label="2028" value="28">2028</option>
                                                                        <option label="2029" value="29">2029</option>
                                                                        <option label="2030" value="30">2030</option>
                                                                    </select>
                                                                    <div class="clearfix"></div>
                                                                    <div id="expiryRequiredError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required"><label>Expire Date is required</label></p>
                                                                    </div>
                                                                    <div id="expiryInvalidError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required"><label>Expire Date is invalid</label></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6" id="div_cvv">
                                                                <label>
                                                                    <span>
                        <span translate="lbl_payment_secCode" class="ng-scope">Security Code</span>
                                                                    <i class="red"> *</i>
                                                                    </span>
                                                                </label>
                                                                <div class="cvv ng-scope">
                                                                    <input type="password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required"  
                                                                    id="cvv"
							                                        name="cvv"
							                                        placeholder="CVV"
							                                        autocomplete="cc-cvv"
							                                        required>
                                                                    <span>
                        <h5 translate="title_payment_cv" class="ng-scope">CVV2 / CVC2</h5>
                        <p translate="msg_payment_cardDetails" class="ng-scope">The last 3 digits displayed on the back of your card</p>
                        </span>
                        											<div class="clearfix"></div>
                                                                    <div id="cvvRequiredError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required"><label>Credit CVV2 / CVC2 is required</label></p>
                                                                    </div>
                                                                    <div id="cvvInvalidError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required"><label>Credit CVV2 / CVC2 is Invalid</label></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label id="div_card_holder">
                                                            <span>
                        <span translate="lbl_payment_holderName" class="ng-scope">Cardholder Name</span>
                                                            <i class="red"> *</i>
                                                            </span>
                                                            <div class="row">
                                                                <div class="col-xs-12 ng-scope">
                                                                    <input type="text" placeholder="Full name as on the card" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required"  name="cardHolderName" id="cardHolderName"  ng-required="true" required="required">
                                                                    <div class="clearfix"></div>
                                                                    <div id="cardHolderRequiredError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required">Card holder name is required</p>
                                                                    </div>
                                                                    <div id="cardHolderInvalidError">
                                                                        <!-- ngMessage: required -->
                                                                        <p class="valdr-message ng-scope" ng-message="required">Card holder name is invalid</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </label>

                                                    </div>
                                                </div>
                                            </form>
                                        </section>
                                       
                                    </div>
                                    <input type="hidden" name="expiryDate" id="expiryDate"/>
                                   	<input type="hidden" name="paymentgatewayId" id="paymentgatewayId"/>
	                        		<input type="hidden" name="cardBehavior" id="cardBehavior"/>  
	                        		<input type="hidden" name="expiryDate" id="expiryDate"/>
	                        		<input type="hidden" name="cardType" id="cardType"/>
                                    <!-- // Left content -->
                                    <!-- Right Content -->
                                    <div class="right-content col-md-4 col-sm-12 col-xs-12 pull-right">
                                        <div>
                                            <!-- Amount -->
                                            <div class="amount" ng-show="paymentCtrl.selectedPaymentOption!==null" ng-style="!paymentCtrl.hasPromoOption ? { 'margin-top' : '0px' } : {'margin-top' : '94px'}" style="margin-top: 94px;">
                                                <div>

                                                    <span class="small-txt ng-scope" translate="lbl_payment_totalAmount">Total Amount Due</span>
                                                    <span class="big-txt ng-scope" ng-repeat="displayAmount in paymentCtrl.paymentDisplayAmounts">
                        <span ng-if="displayAmount.isPaymentCurrency === undefined" class="ng-scope">
                        <i class="ng-binding"><c:out value="${sessionScope.aeroMartSessionInfo.currencyCode}" escapeXml="false" /></i>
                        <i class="ng-binding"><c:out value="${sessionScope.aeroMartSessionInfo.totalAmount}" escapeXml="false" /></i>
                        <br>
                        <i class="ng-binding" id="pg_curr"></i>
                        <i class="ng-binding" id="pg_amount"></i>
                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                           
                                            <div class="submit">

                                                <button id="submitPayment" class="button red-btn ng-scope" type="button" translate="lbl_payment_confirmPay">Confirm Payment</button>

                                            </div>

                                        </div>
                                    </div>

                                    <div id="dummyForm">
                                    </div>
                                </div>
                    </div>
                </div>
                </form>
                </section>
            </div>
            <!-- // Left content -->
        </div>
    </div>
    </div>
    </div>
</body>

</html>