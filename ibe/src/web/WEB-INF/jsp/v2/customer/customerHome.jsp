<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="-1"/>	
		<title><fmt:message key="msg.Page.Title"/></title>			
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 		
		<%@ include file='../common/interlinePgHD.jsp' %>		
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 	
		<script src="../js/common/jquery.resizecrop-1.0.3.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<c:if test="${not empty(requestScope.sysOndSource)}">
	 	<script src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>" type="text/javascript"></script>		
		</c:if>	
		<script type="text/javascript"> fromPromoPage = '<c:out value="${requestScope.fromPromoPage}" escapeXml="false" />';</script>
		<script src="../js/v2/common/flightSearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">
			var GLOBALS = <c:out value="${requestScope.systemDefaultParam}" escapeXml="false" />;
			var contactConfig = <c:out value="${requestScope.contactConfig}" escapeXml="false" />;
			var userRegConfig = <c:out value="${requestScope.userRegConfig}" escapeXml="false" />;
			var validationGroup = <c:out value="${requestScope.validationGroup}" escapeXml="false" />;
			var fareType_IMG_PATH = '<c:out value="${requestScope.sysImagePath}" escapeXml="false" />';
			//TODO Refactor-don't add more
			var objCWindow = "";
			var dtC = new Date();
			var dtCM = dtC.getMonth() + 1;
			var dtCD = dtC.getDate();
			if (dtCM < 10){dtCM = "0" + dtCM}
			if (dtCD < 10){dtCD = "0" + dtCD}

			var strSysDate  = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
			var dtSysDate	= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
			var strToDate	= strSysDate;
			if (UI_Top.holder().GLOBALS.currentDate != null && UI_Top.holder().GLOBALS.currentDate == "") {
				var date = UI_Top.holder().GLOBALS.currentDate;
				var dtTempSysDate = new Date(date.substr(6,4), (Number(date.substr(3,2)) - 1), date.substr(0,2));
				strSysDate  	= DateToString(addDays(dtTempSysDate, 0));
				dtSysDate		= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));
				strToDate       = dtSysDate; 
			}	
		</script>
		<script type="text/javascript">
		var showFacebookLogin = '<c:out value="${requestScope.enableFacebookLogin}" escapeXml="false" />';
		var socialLoginFBAppId = '<c:out value="${requestScope.fbAppId}" escapeXml="false" />';
		var showLinkedInLogin = '<c:out value="${requestScope.enableLinkedInLogin}" escapeXml="false" />';
		var socialLoginLinkedInAppId = '<c:out value="${requestScope.linkedInAPIKey}" escapeXml="false" />';
		var socialSiteType = '<c:out value="${requestScope.socialSiteType}" escapeXml="false" />';
		var socialPictureUrl = '<c:out value="${requestScope.socialPictureUrl}" escapeXml="false" />';
		var isInDevMode = '<c:out value="${applicationScope.isDevModeOn}" escapeXml="false" />';
	  </script>
	
	</head>
	<body>
		<c:import url="../common/pageLoading.jsp"/>
		<div id="divLoadBg" style="display:none">
		 <a href="#"  id="linkFocus"> </a>
		 <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' >	   
			<tr>
			<td align='center' class="outerPageBackGround">
				<table border='0' style='width:940px;' cellpadding='0' cellspacing='0' class='PageBackGround'>	
					<!-- Top Banner -->
					<c:import url="../../../../ext_html/header.jsp" />
					<!-- Content holder -->				
					<tr>
						<td colspan="2" class="mainbody alignLeft">
							<div id="sortable-ul">
								<div class="sortable-child">
									<div style="width:250px;" class="fareSelSidePanel" id="sidePannelAll">
										<%@ include file='../../v2/common/includeFlightSearch.jsp' %>
										<br/>
										<div id="regUserHomeNewPanel">
										<%@ include file='../../v2/common/includeLeftPanel.jsp' %>
										</div>
									</div>
								</div>
								<div class="sortable-child">
									<div class="rightColumn">
										<table border='0' cellpadding='0' cellspacing='0' align='center' width='99%'>
											<tr id="trTabPannel">
												<td><form id="frmCustomerTab" name="frmCustomerTab" method="post">
													<%@ include file='../../v2/common/includeUserTabs.html' %>
													</form>
												</td>
											</tr>									
											<tr>
												<td>									
												<div id="reservationDetail" style="display:none">									  	
												</div>
												<div id="reservationList" style="display:none">
												</div>
												<div id="reservationCredit" style="display:none">
												</div>
												<div id="travelHistory" style="display:none">
												</div>
												<div id="updateProfile" style="display:none">
												</div>	
												<div id="modifySegment" style="display:none">
												</div>
												<div id="addBusSegment" style="display:none">
												</div>
												<div id="balanceSummary" style="display:none">
												</div>										
												</td>
											</tr>
											<tr>
												<td colspan="2" class="rowGap">
													&nbsp;
												</td>
											</tr>
											<tr id="aggrementInfo">
											<td>
												<table border="0" cellpadding="2" cellspacing="0" width="100%" >
													<tr><td>														
													<label id="lblInfoAgree1" hidden> </label>
													<br/><br/>
													<label id="lblInfoAgree2"> </label>	
													<br/>
													<div class="divfont" id="lblInfoAgree3"> </div>	
													</td></tr>
												</table>
											</td>
											</tr>
										 </table>	
									</div>
								</div>
							</div>
							</td>
						</tr>
						<!-- Bottom AA Logo -->
						<tr>
						<td class='appLogo' colspan="2"></td>
						</tr>
						<c:import url="../../../../ext_html/cfooter.jsp" />
					</table>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		</script>
		<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />
		<c:choose>
			<c:when test="${not empty param.pnr}">
				<input type="hidden" name="hdnPNR" id="hdnPNR" value="<c:out value="${param.pnr}" escapeXml="false"/>"/>
			</c:when>
			<c:otherwise>
				<input type="hidden" name="hdnPNR" id="hdnPNR" value="<c:out value="${pnr}" escapeXml="false"/>"/>
			</c:otherwise>
		</c:choose>	
		
		<c:choose>
            <c:when test="${not empty param.groupPNR}">
                 <input type="hidden" name="groupPNR" id="groupPNR" value="<c:out value="${param.groupPNR}" escapeXml="false"/>"/>
            </c:when>
            <c:otherwise>
                 <input type="hidden" name="groupPNR" id="groupPNR" value="<c:out value="${groupPNR}" escapeXml="false"/>"/>
            </c:otherwise>
        </c:choose>        
		<input type="hidden" name="airlineCode" id="airlineCode" value="<c:out value="${param.airlineCode}" escapeXml="false"/>"/>
		<input type="hidden" name="marketingAirlineCode" id="marketingAirlineCode" value="<c:out value="${param.marketingAirlineCode}" escapeXml="false"/>"/>
		<input type="hidden" name="tabTarget" id="tabTarget" value="<c:out value="${param.target}" escapeXml="false"/>"/>		
		<%-- Handle common parameters --%>
		<%@ include file='../common/iBECommonParam.jsp'%>
		<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 		
		</div>
		<script src="../js/v2/customer/includeUserTabs.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<script src="../js/v2/customer/customerHome.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
		<form id="submitForm" method="post">
		</form>
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp' %>
		</div>
	</body>
	<%@ include file='../common/inline_Tracking.jsp' %>
</html>