<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="-1"/>
		<title><fmt:message key="msg.Page.Title" /></title>
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/v2/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
		<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
		<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
		<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<script type="text/javascript">
			<c:out value="${requestScope.reqClientProfMessages}" escapeXml="false" />
		</script>
	</head>
  <body>
  <c:import url="../common/pageLoading.jsp"/>
  <!--fmt:setLocale value="${sessionScope.Language}" scope="session"/-->
	<div id="divLoadBg" style="display: none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
	<td align='center' class="outerPageBackGround">
		<table style='width:940px;' border='0' bgcolor="#ffffff" cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
		<!-- Top Banner -->
			<c:import url="../../../../ext_html/header.jsp" />
		<!-- Content holder -->
		<tr id="trForm">
			<td class="alignLeft mainbody">
						<div id="sortable-ul">
										 	<form id="frmRegister" name="frmRegister" method="post" action="registerAgent.action">
												<table width="98%" border="0" cellpadding="2" cellspacing="0" align='center' class="rightColumFix">
													<tr>
														<td colspan="4">
															<span id="spnError"></span>
															<font class="mandatory"><b><c:out value="${requestScope.customerExists}" /></b></font>
														</td>
													</tr>
													<tr>
														<td colspan="4" valign="top">
															<font class="fntBold hdFontColor hdFont"><fmt:message key="msg.register.title"/></font>
														</td>
													</tr>
													<tr>
														<td class="setHeight" colspan="4">
														</td>
													</tr>
													<tr>
														<td colspan="4" valign="top">
															<font><fmt:message key="msg.register.agent.hd"/></font>
														</td>
													</tr>
													<tr>
														<td class="rowGap" colspan="4">
														</td>
													</tr>
													<tr>
														<td colspan="4">
															<font class="fntBold hdFontColor"><fmt:message key="msg.register.agent.info"/></font>
														</td>
													</tr>	
													
													<tr id = taCoAgencyName>
														<td width='15%'><font id="agentTypeName"><fmt:message key="msg.regup.agencyName"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtAgencyName" name="agent.agencyName" size="50" maxlength="50" class="fontCapitalize"/> <font class="mandatory">*</font>
														</td>
													</tr>
													<tr id = taLicenseNo>
														<td><font><fmt:message key="msg.regup.licenseNo"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtLicenseNo" name="agent.licenseNo" size="50" maxlength="50" class="fontCapitalize"/> 
														</td>
													</tr>
													<tr id = taIATACode>
														<td><font><fmt:message key="msg.regup.IATACode"/> </font><font>:</font></td>
														<td colspan="3"><input type="text" id="txtIATACode" name="agent.iATACode" size="50" maxlength="50" class="fontCapitalize"/> 
														</td>
													</tr>
													<tr id = taCoStation>
														<td><font id="stationName"><fmt:message key="msg.regup.station"/> </font><font>:</font></td>
														<td colspan="3">
															<select style="width: 140px;"  size="1" name="agent.station" id="selStation">
																<option value=""><fmt:message key="msg.dropdown.default.text"/></option>												
															</select>
															<font class="mandatory">*</font>
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.paymentMethod"/> </font><font>:</font></td>
													</tr>
													<tr id = taPayMethBS>
														<td><font><fmt:message key="msg.regup.bsp"/> </font></td>
														<td colspan="3">
															<input class="NoBorder" type="radio" id="radPayMethBS" name="agent.payMeth" value="BS">
														</td>
													</tr>
													<tr id = taPayMethAC>
														<td><font><fmt:message key="msg.regup.agencyCredit"/> </font></td>
														<td colspan="3">
															<input class="NoBorder" type="radio" id="radPayMethAC" name="agent.payMeth" value="AC" checked="checked">
														</td>
													</tr>
													<tr id = coPayMeth>	
														<td><font><fmt:message key="msg.regup.agencyCredit"/> </font></td>
														<td colspan="3">
															<input class="NoBorder" type="checkbox" id="chkCoPayMeth" name="agent.chkCoPayMeth" value="AC">
														</td>
													</tr>
													<tr>	
														<td><font><fmt:message key="msg.regup.creditCard"/> </font></td>
														<td colspan="3">
															<input class="NoBorder" type="checkbox" id="chkPayMeth" name="agent.chkPayMeth" value="CC" checked="checked"><font>&nbsp;<fmt:message key="msg.regup.creditcard"/></font>
														</td>
													</tr>
													
													<tr>
														<td><font><fmt:message key="msg.regup.address"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtAddress" name="agent.address" size="50" maxlength="50" class="fontCapitalize"> <font class="mandatory">*</font>
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.city"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtCity" name="agent.city" size="50" maxlength="50" class="fontCapitalize"> <font class="mandatory">*</font>
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.country"/> </font><font>:</font></td>
														<td colspan="3">
															<select style="width: 140px;"  size="1" name="agent.country" id="selCountry">
																<option value=""><fmt:message key="msg.dropdown.default.text"/></option>											
															</select>
															<font class="mandatory">*</font>
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.state"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtState" name="agent.state" size="50" maxlength="50" class="fontCapitalize"> 
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.postalCode"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtPostalCode" name="agent.postalCode" size="50" maxlength="50" class="fontCapitalize"> 
														</td>
													</tr>
													
													<tr>
														<td><font><fmt:message key="msg.regup.email"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtEmail" name="agent.email" size="50" maxlength="50"> <font class="mandatory">*</font>
														</td>
													</tr>														
													
													<tr>
														<td colspan="4"><font>&nbsp;</font></td>
													</tr>
													<tr>
														<td colspan="4">
															<font class="fntBold hdFontColor"><fmt:message key="msg.regup.contact.info"/></font>
														</td>
													</tr>
													<tr id="coCompanyContactName">
														<td><font><fmt:message key="msg.regup.contactName"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtCompanyContactName" name="agent.companyContactName" size="50" maxlength="50" class="fontCapitalize">
														</td>
													</tr>
													<tr id="coContactDesignation">
														<td><font><fmt:message key="msg.regup.contactDesignation"/> </font>:</td>
														<td colspan="3"><input type="text" id="txtContactDesignation" name="agent.contactDesignation" size="50" maxlength="50" class="fontCapitalize">
														</td>
													</tr>
													<tr id="coCompanySize">
														<td><font><fmt:message key="msg.regup.companySize"/> </font></font>:</td>
														<td colspan="3">
															<select id="selSize" style='width:140px' name='agent.companySize'>
																<option value="">Please select</option>
																<option value="0 to 50">0-50</option>
																<option value="50 to 100">50-100</option>
																<option value="100 to 150">100-150</option>
																<option value="150 to 200">150-200</option>
																<option value="200 to 250">200-250</option>
																<option value="250 to 300">250-300</option>
																<option value="300 to 350">300-350</option>
																<option value="350 to 400">350-400</option>
																<option value="400 to 450">400-450</option>
																<option value="450 to 500">450-500</option>
																<option value="more than 500">500+</option>
															</select>
														</td>
													</tr>
													<tr id="coConvenientTime">
														<td><font><fmt:message key="msg.regup.convenientTimeToContact"/> </font></font>:</td>
														<td colspan="3">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="40"><font>AM : </font></td>
																	<td width="70"><input class="NoBorder" type="radio" id="chkam" name="agent.convenientTime" value="AM"></td>
																	<td width="40"><font>PM : </font></td>
																	<td><input class="NoBorder" type="radio" id="chkpm" name="agent.convenientTime" value="PM"></td>
																</tr>
															</table>														
														</td>
													</tr>
													<tr>
														<td>
														</td>
														<td colspan="3">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="70"><font class="fntSmall"><fmt:message key="msg.res.psg.Phone.CountryCode"/></font></td>
																	<td width="70"><font class="fntSmall"><fmt:message key="msg.res.psg.Phone.AreaCode"/></font></td>
																	<td><font class="fntSmall"><fmt:message key="msg.res.psg.Phone.Number"/></font></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.telephone" /> </font><font>:</font></td>
														<td colspan="3">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="70"><input type='text' id='txtPCountry' name="txtPCountry" style='width:50px;' maxlength='4'></td>
																	<td width="70"><input type='text' id='txtPArea' style='width:50px;' maxlength='4'></td>
																	<td><input type='text' id='txtTelephone' name="txtTelephone" style='width:100px;' maxlength='10'><font class='Mandatory'>&nbsp;*</font></td>
																</tr>
															</table>
														</td>
													</tr>
													<tr id="coMobile">
														<td><font><fmt:message key="msg.regup.mobile" /> </font> </font>:</td>
														<td colspan="3">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="70"><input type='text' id='txtMCountry' name="txtMCountry" style='width:50px;' maxlength='4'></td>
																	<td width="70"><input type='text' id='txtMArea' name='txtMArea' style='width:50px;' maxlength='4'></td>
																	<td><input type='text' id='txtMobile' name="txtMobile" style='width:100px;' maxlength='10'></td>
																</tr>
															</table>
														</td>
														</td>
													</tr>
													<tr id = TaFax>
														<td><font><fmt:message key="msg.regup.fax"/> :</font></td>
														<td colspan="3">
															<table width="100%" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td width="70"><input type='text' id='txtFCountry' style='width:50px;' maxlength='4'></td>
																	<td width="70"><input type='text' id='txtFArea' style='width:50px;' maxlength='4'></td>
																	<td><input type="text" id="txFax" name="txFax" style='width:100px;' maxlength="10"></td>
																</tr>
															</table>														
															
														</td>
													</tr>
													<tr>
														<td><font><fmt:message key="msg.regup.comments"/> </font>:</td>
															<td colspan="3"><textarea id="txtComments" name="agent.comments"  cols=30 rows=3 class="fontCapitalize" maxlength="500" ></textarea>
														</td>
													</tr>
													<tr>
														<td colspan="4"><font>&nbsp;</font></td>
													</tr>											
																										<tr>
														<td colspan="4" align="center">
															<input type="button"  value="<fmt:message key="msg.register.button.previous" />" class="Button" name="btnPrevious" id="btnPrevious"/>
															<input type="button" id="btnReset" name="btnReset" class="Button" value="<fmt:message key="msg.register.button.reset" />">
															<input type="button" id="btnRegister" name="btnRegister" class="Button"  value="<fmt:message key="msg.register.button.register" />"/>	
														</td>
													</tr>		
												</table>
												<input type="hidden" name="hdnPaymentMode" id="hdnPaymentMode">
												<input type="hidden" name="hdnVersion" value="">
												<input type="hidden" name="hdnMode" value="Register">																					
												<input class="NoBorder" type="checkbox" id="chkQMailList" value="Y" style="visibility:hidden;">
												<input type="hidden" id="hdnAnswers" name="hdnAnswers">
											 
											 <%@ include file='../common/iBECommonParam.jsp'%>
											 <input type="hidden" name="agent.telephone" id="hdnTelNo"/>
											 <input type="hidden" name="agent.mobile" id="hdnMobile"/>
											 <input type="hidden" name="agent.fax" id="hdnFax"/>	
											 </form>
 										</div>
									</td>
								</tr>
								<tr id = trMsg>
									<td class="pageBody">
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td valign="top">
													<%@ include file='../../common/includeFrameTop.jsp' %>						
														<table width='100%' border='0' cellpadding='2' cellspacing='0'>
															<tr>
																<td valign='top'>
																	<img src='<fmt:message key="msg.pg.img.folder"/>n058_no_cache.gif'>&nbsp;<br>
																	<font class="fntBold <fmt:message key="msg.pg.font.Page"/>">
																	<fmt:message key="msg.regup.agent.success"/></font>	
																</td>
															</tr>
														</table>
													<%@ include file='../../common/includeFrameBottom.jsp'%>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
					<td class='appLogo' colspan="2"></td>
				</tr>
					<c:import url="../../../../ext_html/cfooter.jsp" />
							</table>
						</td>
					</tr>
							<%-- Bottom AA Logo --%>
				
				</table>
	</div>
	<%@ include file='../../common/includeBottom.jsp'%>
	<form id="submitForm" method="post">
	</form>
  </body>
	<script type="text/javascript">	
		var strPageID	= "REGISTER_AGENT";
		var agentType = {cooparate:'<fmt:message key="msg.regup.cooperateName"/>', normal:'<fmt:message key="msg.regup.agencyName"/>' }
		var stationLbl = {cooparate:'<fmt:message key="msg.regup.location"/>', normal:'<fmt:message key="msg.regup.station"/>' }
		var taCo = '<c:out value='${sessionScope.taCo}' escapeXml='false' />';
	</script>	
	<script src="../js/v2/agent/registerAgent.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
	<script src="../js/v2/agent/agentValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
</html>
