<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../../../jsp/common/topHolder.jsp' %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="-1"/>    
</head>
<body>	
	<form name="redirectForm">
		<input type="hidden" name="pnr" value="<c:out value='${sessionScope.sesPNRNO}' escapeXml='false' />" />
	</form>
	<script type="text/javascript">
		document.forms[0].action = "showLoadPage!loadConfimationPage.action?id=" + Math.floor(Math.random()*10000);
		
		if(fromSocialSites != undefined && fromSocialSites != "" && fromSocialSites =='true'){
			document.forms[0].target= "_self";
		}else{
			document.forms[0].target= "_top";
		}
		document.forms[0].submit();
	</script>	
</body>
</html>