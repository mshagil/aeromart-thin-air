﻿<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/directives.jsp" %>
<%@ page language="java"%>
<%@ page import="com.isa.thinair.ibe.core.web.util.AppParamUtil"%>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <META HTTP-EQUIV="Expires" CONTENT="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	 <%
	 String agentsURL =  AppParamUtil.getNonsecureIBEUrl()+"showAgentsInfo.action";
	 //FIXME : direct to actual home url
	 String homeURL = "http://localhost:8080/ibe/js/indexDummy_GA.jsp";
	 %>	  	
	<script language="JavaScript" src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    	
<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
<script src="../js/common/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
<script src="../js/common/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
<link rel="stylesheet" type="text/css" href="./../css/myStyle_no_cache.css?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"/>		

<script type="text/javascript">
function Show_Popup(url) {
$('#popup').fadeIn('fast');
$('#google_Map').attr("src",url); 
$('#window').fadeIn('fast');
}
function Close_Popup() {
$('#popup').fadeOut('fast');
$('#window').fadeOut('fast');
}

function View_Large_Map(){
	//window.location = mapURL;
	window.open(mapURL,'Agent Location Map');
}
</script>
<style type="text/css">
#popup {
height: 100%;
width: 100%;
background: #000000;
position: absolute;
top: 0;
-moz-opacity:0.75;
-khtml-opacity: 0.75;
opacity: 0.75;
filter:alpha(opacity=75);
}

#window {
width: 600px;
height: 500px;
margin: 0 auto;
border: 1px solid #000000;
background: #ffffff;
position: absolute;
top: 100px;
left: 25%;
}


</style>
<style type="text/css">
BODY
{
    FONT-SIZE: 10px;
    MARGIN: 0px;
    COLOR: #000000;
    FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif;
	display:inline;
	width:98%;
}
html{
overflow: auto;
}
.firstCol{
	border-left:1px solid #d1d1d1;
}
.recordl td.GridItems {
	background-color:#EBEBEB;
	font-size:11px;
	height:20px;
	border-right: 1px solid #d1d1d1;
	padding: 0 3px;
}
.recordd td.GridItems {
	background-color:#FFFFFF;
	font-size:11px;
	height:20px;
	border-right: 1px solid #d1d1d1;
	padding: 0 3px;
}
</style>	

<script type="text/javascript">	
var agentName= '<fmt:message key="msg.agent.results.name"/>';
var map = '<fmt:message key="msg.agent.results.map"/>';
var viewmap = '<fmt:message key="msg.agent.results.viewmap"/>';
var nodata = '<fmt:message key="msg.agent.results.nodata"/>';
var agentHeading = '<fmt:message key="msg.agent.results.agentheading"/>';
var address = '<fmt:message key="msg.res.psg.address"/>';
var telephone = '<fmt:message key="msg.regup.telephone"/>';
var fax = '<fmt:message key="msg.regup.fax"/>';
var email = '<fmt:message key="msg.regup.email"/>';

var table = '<table cellpadding="0" cellspacing="0" width="100%">';
var tablebody = '<tbody><tr class="thinBorderT thinBorderB recordtop fntBold fntRed">';
var tableHeader = '<td align="left" style="width:60px;border-bottom:1 solid black;">'+agentHeading+'</td>';
var trow = '</tr> <tr><td colspan="6" align="left"></td></tr></tbody></table>';

var htmlNoData='<table width="100%"><tr class="recordtop fntRed fntBold"><td align="center" >'+nodata+'</td></tr></table>';
var mapURL;
</script>


</head>
  
<body  onLoad="onLoadPage();"scroll='yes' onkeydown='return Body_onKeyDown(event)' >
	<span id="tblAgentsInfo"></span>

<div id="popup" style="display: none;"></div>
	<div id="window" style="display: none;">
	<div id="popup_content">
	
	<a href="#" onclick="Close_Popup();" class="thinBorderT thinBorderB recordtop fntBold fntRed">Close</a>
	<a href="#" onclick="View_Large_Map();" class="thinBorderT thinBorderB recordtop fntBold fntRed">View Large</a>
	<iframe  id="google_Map" src="" name="google_Map" width="98%" height="90%" src="" scrolling="auto" frameborder="1" >
	</iframe>
	
</div>

</body>
<script type="text/javascript">	
		<!--
		//var arrAgents = new Array();
		<c:out value='${requestScope.agents}' escapeXml='false' />;
		<c:out value='${requestScope.agentType}' escapeXml='false' />;
		//-->	
</script>
<script src="../js/agent/agentsInformation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
</html>
			