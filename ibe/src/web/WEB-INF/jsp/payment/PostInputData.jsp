<%@ page language="java"%>
<%@ page import="com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD" %>
<%@ page import="com.isa.thinair.ibe.core.web.constants.RequestAttribute" %>
<%@ page import="com.isa.thinair.commons.core.util.StringUtil" %>
<%@ include file='../v2/common/cacheClear.jsp'%>
<%@ include file="../common/directives.jsp" %>
<%@ include file='../common/topHolder.jsp' %>
<% 
   	// FIXME - TO BE MOVED TO JAVA CODE
   	String requestMethod = (String)request.getAttribute(RequestAttribute.REQUEST_METHOD);
	String brokerType = (String)request.getAttribute(RequestAttribute.PAY_GATEWAY_TYPE);   	
   
   if (PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_EXTERNAL.equals(brokerType)) {
	   if (requestMethod.equals(PaymentBrokerBD.PaymentBrokerProperties.REQUEST_METHODS.POST.toString())) {
%>
		   	<html>
			<head>
			<title></title>
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="-1">
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
			<link rel='shortcut icon' href='../images/AA.ico'> 
		    <link rel='stylesheet' type='text/css' href='../css/<fmt:message key="msg.pg.css.name"/>'>
			<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
			<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>			
			</head>
			   <body>
		
				<c:out value='${requestScope.postInputDataFormHtml}' escapeXml='false' /> 
			   
			   </body>
				<script>					
					
					function onLoad(){							
							document.forms[0].submit();						
					}
					
					var isSwitchToExternalURL = "<c:out value='${requestScope.reqSwitchToExternalURL}' escapeXml='false' />";
					
					if (isSwitchToExternalURL != "true" || !isSwitchToExternalURL) {
						UI_Top.holder().UI_commonSystem.loadingCompleted();
					} else {
						document.forms[0].target = '_top';
					}	
					onLoad();
				</script>	   
			</html>
<%  
		} else { 					
			%>      		
      		<html>
				<body>
					<script>	
						var isSwitchToExternalURL = "<c:out value='${requestScope.reqSwitchToExternalURL}' escapeXml='false' />";
					    var url = "<c:out value='${requestScope.postInputDataFormHtml}' escapeXml='false' />";
						
 					    if (isSwitchToExternalURL != "true" || !isSwitchToExternalURL) {
					    	UI_Top.holder().UI_commonSystem.loadingCompleted();	
					    	window.UI_Top.holder().location.href = url;
					    } else {
					    	window.UI_Top.holder().location.href = url;
					    }				     
					</script>
				</body>
			</html>
		<%
      		
		}
	} else {		
   		if (requestMethod.equals(PaymentBrokerBD.PaymentBrokerProperties.REQUEST_METHODS.GET.toString())) {
   		%>
   			<html>
				<body>
					<script>
						var isSwitchToExternalURL = "<c:out value='${requestScope.reqSwitchToExternalURL}' escapeXml='false' />";
					    var url =  "<c:out value='${requestScope.postInputDataFormHtml}' escapeXml='false' />"; 
						 
					    if (isSwitchToExternalURL != "true" || !isSwitchToExternalURL) {
							UI_Top.holder().UI_commonSystem.loadingCompleted();
	  						 window.UI_Top.holder().location.href = url;
						} else {						   
							 window.UI_Top.holder().parent.location = url;
						}					    
					</script>
				</body>
			</html>
   			
   		<%      	
   		} else { 
   		%>			
			<html>
			<head>	
				<meta http-equiv="pragma" content="no-cache">
				<meta http-equiv="cache-control" content="no-cache">
				<meta http-equiv="expires" content="-1">			
			</head>
			<body style="display: none;">
			
				<c:out value='${requestScope.postInputDataFormHtml}' escapeXml='false' /> 
				
				<script>

				var track_count = 0;
				var isSwitchToExternalURL = "<c:out value='${requestScope.reqSwitchToExternalURL}' escapeXml='false' />";
				
				function formSubmit() {	 
					track_count++;
					if (track_count > 1) {						
						top.window.location.href = "/";
					} else {             
		                if (document.forms[0]) {
			                // Check submit attribute exist
		                    if (document.getElementsByName("submit").length <= 0) {
		                        document.forms[0].submit();
		                    } else {
		                         addBtn();
		                         document.getElementById("submitBtn").click();
		                    }
		                } 
					}        
				}

				function addBtn() {
                    var element = document.createElement("input");
                    element.setAttribute("type", "submit");
                    element.setAttribute("id", "submitBtn");
                    document.forms[0].appendChild(element);
                }                	
				
				if (isSwitchToExternalURL != "true" || !isSwitchToExternalURL) {
					UI_Top.holder().UI_commonSystem.loadingCompleted();	
				} else {
					document.forms[0].target = '_top';
				}
				formSubmit();
			</script>
			</body>
			</html>
		<% 
		} 
   } 
%>