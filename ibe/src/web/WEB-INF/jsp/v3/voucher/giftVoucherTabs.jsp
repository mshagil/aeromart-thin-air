<!-- ************ Steps ID Incremental NOTE: Do not remove Class name stepsItem ********* -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>  
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align="center" class="StepsTable">
		<tr>
			<td id="tabFirst" class="stepsItem select">
				<table id="firstTab" width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="first">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text"><lable id = "lblTab1"><font class="fntBold">Voucher and Customer Details</font></lable></td>
						<td class="process-mid iconCorrect">
							<img src="../images/spacer_no_cache.gif" border="0"/> 
						</td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
			<td id="tabSecond" class="stepsItem">
				<table width='auto' border='0' cellpadding='0' cellspacing='0' align="center" class="last">
					<tr>
						<td class="process-left"></td>
						<td class="process-mid peocess-text"><lable id ="lblTab2"><font class="fntBold">Pay and Confirm</font></lable></td>
						<td class="process-mid iconCorrect">
							<img src="../images/spacer_no_cache.gif" border="0"/> 
						</td>
						<td class="process-right"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>