<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<title><fmt:message key="msg.Page.Title"/></title>
		<link rel="stylesheet" type="text/css" href="../themes/default/css/jquery.ui_no_cache.css"></link>
		<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
		<%@ include file='../common/interlinePgHD.jsp' %>
		<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>   	
		<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/v3/common/iBECommonParam.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		
		<script type="text/javascript">
	    var promoPnr = null;								
		var promoLastName = null;
		var promoDepDate = null;
		var GLOBALS = {};
		GLOBALS.skipPersianDateChange = <c:out value="${requestScope.commonParams.skipPersianCalendarChange}" escapeXml="false" />;
       </script> 	
	</head>
	<body>
	<c:import url="../common/pageLoading.jsp"/>
		<div id="divLoadBg" style="display: none;">		
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="PgManageBooking">
				<!-- Top Banner -->
				<c:import url="../../../../ext_html/header.jsp" />
				<tr>
					<td align='center' class="outerPageBackGround">
						<table style='width:940px;' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
							<!-- Content holder -->
							<tr id="trForm">
							  <td>
								<form id="frmManageBooking" name="frmManageBooking" method="post" action="">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td class="alignLeft mainbody">
												<div id="sortable-ul">
													<table width="100%" border="0" cellpadding="2" cellspacing="0" align='center'>
															<tr>
																<td class="alignLeft paddingCalss" class="alignLeft"><label class="fntBold hdFontColor hdFont" id="lblHdManageRes"></label></td>
															</tr>
															<tr>
																<td class="alignLeft paddingCalss" class="alignLeft"><label id="lblMsgEnterInfo"></label></td>
															</tr>
															<tr>
																<td></td>
															</tr>
															<tr>
																<td valign="top"  class="alignLeft paddingCalss">
																<table cellspacing="0" cellpadding="0" border="0" width="100%">	
																<%@ include file="../common/includeFrameTop.html"%>																
																	<table cellspacing="0" cellpadding="2" border="0" width="100%">																		
																		<tr>
																			<td colspan="3" class="alignLeft" class="alignLeft">
																				<span id="spnError" class="mandatory fntBold">
																				
																				</span>
																				<br/>
																			</td>
																		</tr>											
																		<tr>
																			<td width="140px" class="alignLeft" class="alignLeft">
																				<label class="fntBold" id="lblPnrNo"></label>
																			</td>
																			<td width="5">:</td>
																			<td width="250" class="alignLeft" class="alignLeft">
																				<input type="text" name="pnr" id="pnr"/>
																				<font class="mandatory"> *</font>
																			</td>
																		</tr>
																		<tr>
																			<td id="contactLastName" width="140" class="alignLeft" align="left"><label class="fntBold" id="lblContactLastName"></label></td>
																			<td id="contactOrPaxLastName" width="140" class="alignLeft" align="left"><label class="fntBold" id="lblPaxOrContactLastName"></label></td>
																			<td width="5">:</td>
																			<td class="alignLeft" width="250" class="alignLeft">
																				<input type="text" style="width: 220px;" name="lastName" id="txtLastName"/>
																				<font class="mandatory"> *</font></td>
																		</tr>
																		<tr>
																			<td width="140"class="alignLeft" class="alignLeft"><label class="fntBold" id="lblDepDate"></label></td>
																			<td width="5">:</td>
																			<td class="alignLeft" width="250" class="alignLeft">
																				<input type="text" style="width: 80px;" name="txtDateOfDep" id="txtDateOfDep"/>
																				<input type="hidden" name="dateOfDep" id="dateOfDep"/>
																				<input type="hidden" name="preDateOfDep" id="preDateOfDep"/>
																				<img border="0" src="../images/Calendar2_no_cache.gif" id="imgCalendar" align="top" class="cursorPointer" title="Click here to view calendar" />
																				<label class="mandatory"> *</label>																				
																			</td>
																		</tr>
																		
																		<tr id="trPanelImageCaptcha">
																			<td width="140"class="alignLeft" class="alignLeft">
																				<img src="../jcaptcha.jpg" align="top" style="cursor: pointer;" name="imgCPT" id="imgCPT"/>
																				<br/>
																				<label id="lblTextImagecaptura">Please Click on the image to change</label>
																			</td>
																			<td width="5">:</td>
																			<td class="alignLeft" width="250" class="alignLeft">														
																				<input type="text" style="width: 220px;" name="captcha" id="txtCaptcha"/>
																				<font class="mandatory"> *</font>
																			</td>
																		</tr>
																	</table>
																	<%@ include file="../common/includeFrameBottom.html"%>		
																	</table>																	
																</td>
															</tr>
															<tr>
															<td>
																<table cellspacing="0" cellpadding="0" border="0" width="100%">
																<tr>
																	<td class="alignLeft">
																		<!-- <input type="button" class="Button"  title="Refresh the Word" tabindex="23" id="btnRefresh" style="display: none;"/> -->
																		<u:hButton name="btnPrevious" id="btnPrevious" value="Sign In" tabIndex="20" cssClass="backPrevious"/>
																		 <%--<input type="button"  value="Previous" class="Button" name="btnPrevious" id="btnPrevious"/> --%>
																	</td>
																	<td class="alignRight">
																		<u:hButton name="btnContinue" id="btnContinue" value="Sign In" tabIndex="20" cssClass="redContinue"/>
																		 <%--<input type="button" class="Button"  title="Continue" tabindex="24" id="btnContinue"/> --%>
																	</td>
																</tr>
																</table>	 
															</td>
															</tr>
														</table>
														</div>
													</td>
												</tr>
											</table>									
										<%@ include file='../common/iBECommonParam.jsp'%>	
										<input type="hidden" id="groupPNR" name="groupPNR" />	
										<input type="hidden" id="airlineCode" name="airlineCode" />
										<input type="hidden" id="marketingAirlineCode" name="marketingAirlineCode" />
										<script type="text/javascript">
													var pnr = "<c:out value="${requestScope.pnr}" escapeXml="false"/>";													
													var lname = "<c:out value="${requestScope.lname}" escapeXml="false"/>";
													var ddate = "<c:out value="${requestScope.ddate}" escapeXml="false"/>";
													var linkType = "<c:out value="${requestScope.link}" escapeXml="false"/>";
													var httpsEnabled = "<c:out value="${requestScope.httpsEnable}" escapeXml="false"/>";
										</script>	
										</form>
								  </td>
							 </tr>
							 <tr>
								<td class='appLogo' colspan="2"></td>
							</tr>
						</table>
					</td>
				</tr>
				<c:import url="../../../../ext_html/cfooter.jsp" />
			</table>
		</div>
		<script type="text/javascript">
				promoPnr = "<c:out value="${requestScope.promoPnr}" escapeXml="false"/>";									
				promoLastName = "<c:out value="${requestScope.promoLastName}" escapeXml="false"/>";
				promoDepDate = "<c:out value="${requestScope.promoDepDate}" escapeXml="false"/>";
		</script>
		
		
		<script type="text/javascript">
			var strAnalyticEnable = '<c:out value="${applicationScope.appAnalyticEnable}" escapeXml="false" />';
		</script>
		<c:out value='${applicationScope.appAnalyticJs}' escapeXml='false' />					
		<script src="../js/v2/modifyRes/manageBooking.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
		<form id="submitForm" method="post">
		</form>
		<div id='divLoadMsg' class="mainPageLoader">
			<%@ include file='../common/includeLoadingMsg.jsp' %>
		</div>
	</body>
	<%@ include file='../common/inline_Tracking.jsp' %>
</html>