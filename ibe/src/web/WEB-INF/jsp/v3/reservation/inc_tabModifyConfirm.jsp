<table width='100%' border='0' cellpadding='0' cellspacing='0'>
<tr>
	<td colspan="2">
			<div>
		 		<table cellspacing="0" cellpadding="2" border="0" width="100%">
						<tbody>
							<tr>
								<td class="alignLeft"><label class="fntBold hdFontColor hdFont">Confirm - Modification</label></td>
							</tr>
							<tr>
								<td valign="bottom"  class="alignLeft">
								<br>
								<label class="fntEnglish aaFontColor">Reservation Number </label>
								<span id="spnPNR"></span>
								</td>
							</tr>
							<tr>
								<td class='rowGap'></td>
							</tr>
						</tbody>
					</table>				 		
			</div>
		</td>
	</tr>
	<tr>
		<td class="rowGap"></td>
	</tr>
	<tr>
		<td>
			<div>
				 <%@ include file='../../v2/common/includeFrameTop.html' %>	
				 	<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
							<tr>
								<td colspan="7" class="Gridwt bdTop bdRight bdLeft alignLeft">
								<label id="lblModifing"  class='fntBold hdFontColor'>Modifying	Flight(s)</label></td>
							</tr>	
							<tr>
								<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'>Origin / Destination</label></td>
								<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'>Departure</label></td>
								<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'>Arrival</label></td>
								<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'>Flight No</label></td>
								<td rowspan="2" align='center' class='gridHD'><label id="lblCarrier" class='gridHDFont fntBold'>Carrier</label></td>
							</tr> 
							<tr>            
								<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'>Date</label></td>
								<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'>Time</label></td>
								<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'>Date</label></td>
								<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'>Time</label></td>            
							</tr>
							<tr id="departueFlightOld">
								<td width='35%' class='alignLeft  defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="orignNDest"></label></td>
								<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="departureDate"></label></td>
								<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="departureTime"></label></td>
								<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
								<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
								<td width='10%' class='rowColor bdRight bdBottom' align='center'><label id="flightNo"></label></td>
								<td width='9%' class='rowColor bdRight bdBottom' align='center'><img id="carrierImagePath" src=""/></td>
							</tr>							              
					</table>	
				 <%@ include file='../../v2/common/includeFrameBottom.html'%>		
			</div>	
		</td>
	</tr>
	<tr>
		<td class='rowGap' colspan='2'></td>
	</tr>	
	<tr>
		<td>
			<div>
				 <%@ include file='../../v2/common/includeFrameTop.html' %>	
				 	<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
							<tr>
								<td colspan="7" class="Gridwt bdTop bdRight bdLeft alignLeft">
								<label id="lblDeparting"  class='fntBold hdFontColor'>Departing Flight(s)</label></td>
							</tr>	
							<tr>
								<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'>Origin / Destination</label></td>
								<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'>Departure</label></td>
								<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'>Arrival</label></td>
								<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'>Flight No</label></td>
								<td rowspan="2" align='center' class='gridHD'><label id="lblCarrier" class='gridHDFont fntBold'>Carrier</label></td>
							</tr> 
							<tr>            
								<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'>Date</label></td>
								<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'>Time</label></td>
								<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'>Date</label></td>
								<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'>Time</label></td>            
							</tr>
							<tr id="departueFlightNew">
								<td width='35%' class='alignLeft defaultRowGap rowColor bdLeft bdBottom bdRight'><label id="segmentCode"></label></td>
								<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="depatureDate"></label></td>
								<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="depatureTime"></label></td>
								<td width='15%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalDate"></label></td>
								<td width='8%' class='rowColor bdRight bdBottom' align='center'><label id="arrivalTime"></label></td>
								<td width='10%' class='rowColor bdRight bdBottom' align='center'><label id="flightNumber"></label></td>
								<td width='9%' class='rowColor bdRight bdBottom' align='center'><img id="carrierImagePath" src=""/></td>
							</tr> 							             
					</table>	
				 <%@ include file='../../v2/common/includeFrameBottom.html'%>		
			</div>	
		</td>
	</tr>
	<tr>
		<td class='rowGap' colspan='2'></td>
	</tr>
	<tr>
		<td>
			<div>
				 <%@ include file='../../v2/common/includeFrameTop.html' %>	
				 	<table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridTable" id="modifyBal">										
						<tr>
							<td colspan="3" class="alignLeft">
							<label id="lblBalancePayment" class="fntBold fntRed">Payment Details</label></td>
						</tr>
						<tr>
							<td
								class="gridHD"><font>&nbsp;</font></td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblModifingFlight" class="fntBold fntWhite">Modifying Flight(s)&nbsp;</label>
							</td>
							<td align="center" width="25%"
								class="gridHD">
								<label id="lblNewFlight" class="fntBold fntWhite">New Flight(s)&nbsp;</label></td>
						</tr>						
						<tr id="balanceSummaryTemplate">
							<td
								class="SeperateorBGAlt rowDataHeight GridItems alignLeft">
								<label id="displayDesc"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayOldCharges" class="fntRed"></label></td>
							<td 
								class=" SeperateorBGAlt rowDataHeight GridItems alignRight">
								<label id="displayNewCharges" class="fntRed"></label>
							</td>
						</tr>
					 </table>
				 <%@ include file='../../v2/common/includeFrameBottom.html'%>		
			</div>	
		</td>
	</tr>	
	<tr>
		<td height="30px" colspan="2">
		</td>
	</tr>
	<%--<tr>
		<td align='left'>
		<div class="buttonset">
			<!-- Button -->
			<table border='0' cellpadding='0' cellspacing='0' id="modifyConfirm">
				<tr>
					<td><input type="button" name="btnPrevious"  tabindex="23"  value="Previous" class="Button"/></td>
					<td style="width:10px;">&nbsp;</td>		
					<td></td>	
					<td style="width:10px;">&nbsp;</td>				
					<td><input type="button" name="btnNext"  tabindex="24" value="Next" class="Button" /></td>
				</tr>
			</table>
		</div>
		</td>
	</tr> --%>
</table>