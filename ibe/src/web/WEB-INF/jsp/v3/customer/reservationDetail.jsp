<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ taglib prefix="u" uri="/WEB-INF/tld/wf-util.tld"%>
<%@ include file='../common/thirdPartyTracking.jsp' %>
<link rel="stylesheet" href="../css/table_layoutV3_EN.css" type="text/css" /> 	
<script src="../js/common/validations.js? <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
<c:if test='${applicationScope.isDevModeOn == "false"}'>
<script src="../js/v2/customer/reservationDetail.js? <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</c:if>
<script src="../js/v2/isalibs/isa.jquery.airutil.js" type="text/javascript"></script>
<script type="text/javascript">
	var fromPromoPage = null;
</script>	
<form action="" id="frmCustReservation" method="post" class="">
	<div id="reservationPage" style="display: none;">
		<a href="#"  id="linkFocus"> </a>
		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
			<tr>
		    	<td class='' colspan="2">
					<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">				
					  <tr>
							<td class="rowSingleGap">
							</td>
					  </tr>
					  <tr>
							<td class="alignLeft paddingCalss">
								<label id="lblSummary" class="fntBold  hdFont hdFontColor"></label>
							</td>
					  </tr>
				      <tr>
					      <td valign="top" id="tdSuccessMsg" style="display: none;">
					          <table cellspacing="0" cellpadding="2" border="0" width="100%">
							  	<tr>
								   <td width="38">
											<img src="../images/n058_no_cache.gif" id="imgConfirm">
								   </td>
								   <td class="alignLeft paddingCalss">
									  <label class="fntBold" id="lblMsgSuccess"></label>
								   </td>
							    </tr>						    
						     </table>
					      </td>
			         </tr>      
		      		   <tr id="anciButtonHdPannel">
					      <td class="alignLeft paddingCalss">
								<span style="display: block;" id="spnPernMsg">
									<label id="lblMsgAncilary" class="fntBold"></label>
									<label id="lblMsgOnhold" class="fntBold hdFont hdFontColor">	Pay for your booking</label>	
								</span>
								<br/>
					      </td>
				       </tr>
				       
				       <tr id="onHoldInfoPannel">
					      <td class="alignLeft paddingCalss">
								<span style="display: block;padding-top: 5px;padding-bottom: 5px;">
									<label id="lblMsgOnholdInfo1" class="fntBold hdFontColor">Your booking is onhold, please press the "Make payment" button to finish the payment before it expires.	</label>
									<br/>
									<label id="lblMsgOnholdInfo2" class="fntBold hdFontColor">Your booking is not confirmed until you do a successful payment and receive a confirmation.</label>									
								</span>
															
					      </td>
				       </tr>
				       
				        <tr id="forcedCFMInfoPannel">
					      <td class="alignLeft paddingCalss">
								<span style="display: block;padding-top: 5px;padding-bottom: 5px;">
									<label id="lblMsgForcedCFMInfo1" class="fntBold hdFontColor">Your booking is Forced-Confirm, please press the "Make payment" button to finish the payment.	</label>									
								</span>															
					      </td>
				       </tr>
				       
				       <tr>
					          <td class="rowHeight">
					          </td>
				       </tr>
				       <tr>
				       		<td colspan="2" style="padding-bottom: 5px;">
				       			<u:hButton name="btnSeatUpdate" id="btnSeatUpdate" value="Select Your Seat"  
												cssClass="whitebutton" inLineStyles="float:left"/>
				       			
				       			<u:hButton name="btnInsUpdate" id="btnInsUpdate" value="Travel Insurance"  
												cssClass="whitebutton" inLineStyles="float:left"/>
												
								<u:hButton name="btnmealUpdate" id="btnmealUpdate" value="Select Your Meal"  
												cssClass="whitebutton" inLineStyles="float:left"/>
												
								<u:hButton name="btnBaggageUpdate" id="btnBaggageUpdate" value="Select Your Baggage"  
												cssClass="whitebutton" inLineStyles="float:left" />
												
								<u:hButton name="btnHalaUpdate" id="btnHalaUpdate" value="Select Your Airport Service"  
												cssClass="whitebutton" inLineStyles="float:left" />
												
								<u:hButton name="btnAPTUpdate" id="btnAPTUpdate" value="Select Your Airport Transfer"  
												cssClass="whitebutton" inLineStyles="float:left" />
												
								<u:hButton name="btnAddSrv4" id="btnAddSrv4" value="Select Your SSR"  
												cssClass="whitebutton"  inLineStyles="float:left"/>
												
								<u:hButton name="btnMakePayment" id="btnMakePayment" value="Make Payment"  
												cssClass="whitebutton"  inLineStyles="float:left"/>
									 
								<%-- <table border='0' align='center' id="anciButtonPannel" style="display: none" width="650" style="width:650px">
									 <tr>
										    <td align='center' width="15%">
										    	
										    	
										    </td>
											<td align='center' width="15%">
												
												
											</td>
											<td align='center' width="15%">
												
												
											</td>
											<td align='center' width="15%">
												
												
											</td>
											<td align='center' width="15%">
												
												
											</td>
									 </tr>
								 </table>  --%>
							</td>
						</tr>
						<tr>
							<td class="rowHeight">
							</td>
						</tr>								       
			     </table>
			</td>
		</tr>
		<tr>
		<td class='rowGapNone' colspan="2"></td>
	</tr>
	<tr>
		<td class="rowSingleGap">
		</td>
  </tr>
	<tr id="trDataPanel">
		<td colspan="2">
		<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
				<tr>
					<td valign='bottom' class="alignLeft paddingCalss" width='55%'>
						<label id="lblPnr" class="fntBold"></label> 
						<label class="fntBold padding3"> : </label> 
						<label id="lblPnrNo" class=""></label>
					</td>
					<td valign='bottom' width='45%' class="alignLeft paddingCalss" > 
						<div id="policyPannel"> 
							<label id="lblPolicy" class="fntBold"></label> 
							<label class="fntBold padding3"> : </label>
							<label id="policyCode" class=""></label> 
						</div>
					</td>
				</tr>
				<tr>
					<td valign='bottom' width='55%' class="alignLeft paddingCalss">
						<label id="lblStatus" class="fntBold"></label> 
						<label class="fntBold padding3"> : </label>
						<label id="lblStrStatus" class="">&nbsp;</label>
						<label id="lblStrZuluReleaseDate" class="fntBold"></label>
					</td>
					<td width='45%' class="alignLeft paddingCalss" id="tdInsTypeDisplay"  style="display: none;">
						<label id="lblInsuranceType"  class="fntBold"></label>  
						<label class="fntBold padding3"> : </label>
						<label id="lblInsuranceTypeValue" class=""></label>
					</td>
				</tr>
				<tr>
					<td valign='bottom' width='55%' class="alignLeft paddingCalss">
						<label id="lblBkgDate"  class="fntBold"></label>  
						<label class="fntBold padding3"> : </label>
						<label id="lblStrBkgDate" class=""></label>
					</td>
					<td width='45%' class="alignLeft paddingCalss">						
					</td>
				</tr>
				<tr>
					<td class='rowGap' colspan='2'></td>
				</tr>
				<tr>
						 <td colspan='2'>											
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>	
								<%@ include file="../common/includeFrameTop.html"%>
								<table width='100%' border='0' cellpadding='0' cellspacing='0'>	
									<tr><td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">	
										<tr>
											<td><label id="lblSummary" class="fntBold hdFontColor"></label><!-- <label id="lblDeparting"  class='fntBold hdFontColor'></label> --></td>
										</tr>													
										<tr>
											<td valign='top'>													
												<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">	
													<tr>
														<td rowspan="2" align='center' class='gridHD'><label id="lblSegment" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align="center" class='gridHD'><label id="lblDeparture" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align="center" class='gridHD'><label id="lblArrival" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDFlightNo" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														<td align="center" width="5%" rowspan="2" class="gridHD" id="tdDepModifyImage" style="display: none;" id="tdHdDepatureResCheck">		
															<img title="Select for changes" src="../images/AA174_no_cache.gif" name="imgHD1" id="imgHD1">		
														</td>
													</tr> 
													<tr>            
														<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>            
													</tr>
													<tr id="departueFlight">
														<td width='35%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
														<td width='15%' class='rowColor' align='center'>
                                                            <label id="departureDate"  class="date-disp"></label>
                                                            <label id="departureDateValue" class="date-hid" style="display:none"></label>
                                                        </td>
														<td width='8%' class='rowColor' align='center'><label id="departureTime"></label></td>
														<td width='15%' class='rowColor' align='center'>
                                                            <label id="arrivalDate"  class="date-disp"></label>
                                                            <label id="arrivalDateValue" class="date-hid" style="display:none"></label>
                                                        </td>
														<td width='8%' class='rowColor' align='center'><label id="arrivalTime"></label></td>
														<td width='10%' class='rowColor' align='center'><label id="flightNo"></label></td>
														<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>														
														<td align="center" class="rowColor" id="tdDepatureResCheck" style="display: none;">
															<input type="checkbox"  value="" class="noBorder" name="chkSegment0" disabled="disabled"  id="flightSegmentRefNumber">
															<img title="Cancelled" src="../images/AA163_no_cache.gif">
													    </td>
													</tr>               
												</table>																																
											</td>
										</tr>
										<tr>
											<td colspan='1' class='rowGap'></td>
										</tr>									
									</table>
								</td>
							</tr>				
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">	
										<tr>
											<td><!-- <label id="lblReturning"  class='fntBold hdFontColor'></label> --></td>
										</tr>											 
										<tr id="trRetrunGrid">
											<td valign='top'>
												<table width="100%" border="0" cellspacing="1" cellpadding="1" class="GridTable">
													<tr>
														<td rowspan="2" align='center' class='gridHD'><label id="lblRSegment" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align='center' class='gridHD'><label id="lblRDeparture" class='gridHDFont fntBold'></label></td>
														<td colspan="2" align='center' class='gridHD'><label id="lblRArrival" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblRFlightNo" class='gridHDFont fntBold'></label></td>
														<td rowspan="2" align='center' class='gridHD'><label id="lblDuration" class='gridHDFont fntBold'></label></td>
														<td align="center" width="5%" rowspan="2" class="gridHD"  id="tdHdReturnResCheck" style="display: none;">		
															<img title="Select for changes" src="../images/AA174_no_cache.gif" name="imgHD1" id="imgHD1">		
														</td>
													</tr>
													<tr>           
														<td class='gridHDDark' align='center'><label id="lblDepartureDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblDepartureTime" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalDate" class='gridHDFont fntBold'></label></td>
														<td class='gridHDDark' align='center'><label id="lblArrivalTime" class='gridHDFont fntBold'></label></td>
													</tr> 
													<tr id="returnFlight">
														<td width='35%' class='defaultRowGap rowColor'><label id="orignNDest"></label></td>
														<td width='15%' class='defaultRowGap rowColor' align='center'><label id="departureDate"></label></td>
														<td width='8%' class='defaultRowGap rowColor' align='center'><label id="departureTime"></label></td>
														<td width='15%' class='defaultRowGap rowColor' align='center'><label id="arrivalDate"></label></td>
														<td width='8%' class='defaultRowGap rowColor' align='center'><label id="arrivalTime"></label></td>
														<td width='10%' class='defaultRowGap rowColor' align='center'><label id="flightNo"></label></td>
														<td width='9%' class='rowColor'  align='center'><label id="duration"></label></td>
														<td align="center" class="GridItems" id="tdReturnResCheck" style="display: none;">
															<input type="checkbox"  value="" class="noBorder" name="chkSegment0" disabled="disabled" id="flightSegmentRefNumber">
															<img title="Cancelled" src="../images/AA163_no_cache.gif">
													    </td>
													</tr>                
												</table>
											</td>
										</tr>
								</table>
							</td>
						</tr>
						</table>
						<%@ include file="../common/includeFrameBottom.html"%>	
						</table></td></tr>	
						<tr>
							<td colspan='2' class='rowGap'></td>
						</tr>	
						<tr id="departureButtonsPannel">
							<td align="center" colspan="2">
									<u:hButton name="btnCancelRes" id="btnCancelRes" value="Cancel Reservation"  title="Click here to cancel the reservation" cssClass="redNormal"/>
									<u:hButton name="btnCancelSegment" id="btnCancelSegment" value="Cancel Segemnt" title="Click here to cancel a segment" cssClass="redNormal"/>
									<u:hButton name="btnModify" id="btnModify" value="Modify Segment" title="Click here to modify a segment" cssClass="redNormal"/>
									<u:hButton name="btnAddBusSegment" id="btnAddBusSegment" value="Add Bus Segment" title="Click here to add a bus segment" cssClass="redNormal"/>		
									<u:hButton name="btnTransfer" id="btnTransfer" value="Transfer Segment" cssClass="redNormal" title="Click here to transfer a segment"/>								
									<%--<input type="button" class="Button ButtonLarge" title="Click here to cancel the reservation" id="btnCancelRes"> 
									<input type="button"  class="Button ButtonLargeDisable" title="Click here to cancel a segment" id="btnCancelSegment"> 
									<input type="button"  class="Button ButtonLargeDisable" title="Click here to modify a segment" id="btnModify">		 --%>										
								</td>
							</tr>
						<tr>
							<td class='rowGap'></td>
						</tr>
						<tr>
						<td colspan="2" class="alignLeft">
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<%@ include file="../common/includeFrameTop.html"%>
										<div>
											<label id="lblPaxInfo" class='hdFontColor fntBold'></label>
										</div>	
										<table width="100%" border="0" cellspacing="0" cellpadding="1" class="GridTable">
												<tr id="tradultfGrid">
													<td class='noPadding'>
														<table border='0' cellspacing='0' cellpadding='1' width='100%'>
															<tr>
																<td class='gridHDDark' width="3%">&nbsp;</td>																
																<td class='gridHDDark alignLeft'  width='59%'><label id="lblAdultHD" class='gridHDFont fntBold'></label></td>
																<td class='gridHDDark alignLeft'  width='38%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
															</tr>
															<tr id="adultGrid">
																<td class='defaultRowGap rowColor bdLeft bdBottom' width="3%"><label id="itnSeqNo"></label></td>
																<td class='defaultRowGap rowColor bdLeft bdBottom bdRight' width='59%'><label id="itnPaxName"></label></td>
																<td  class='defaultRowGap rowColor bdBottom bdRight' width='38%'><span id="additionalInfo" class="fntDefault"></span></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="trChildGrid">
													<td class='noPadding'>
														<table border='0' cellspacing='0' cellpadding='1' width='100%'>
															<tr>
																<td class='gridHDDark' width="3%">&nbsp;</td>																		
																<td class='gridHDDark alignLeft' width='59%'><label id="lblChildHD" class='gridHDFont fntBold'></label></td>
																<td class='gridHDDark alignLeft'  width='38%'><label id="lblServices" class='gridHDFont fntBold'></label></td>
															</tr>
															<tr id="childGrid">
																<td class='defaultRowGap rowColor bdLeft bdBottom' width="3%"><label id="itnSeqNo"></label></td>
																<td class='defaultRowGap rowColor bdLeft bdBottom bdRight' width='59%'><label id="itnPaxName"></label></td>
																<td  class='defaultRowGap rowColor bdBottom bdRight' width='38%'><span id="additionalInfo" class="fntDefault"></span></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr id="trInfGrid">
													<td class='noPadding'>
														<table border='0' cellspacing='0' cellpadding='1' width='100%'>
															<tr>
																<td class='gridHDDark' width="3%">&nbsp;</td>																		
																<td class='gridHDDark alignLeft'  width='59%'><label id="lblInfantHD" class='gridHDFont fntBold'></label></td>
																<td class='gridHDDark alignLeft'  width='38%'><label id="lbltravelwith" class='gridHDFont fntBold'></label></td>
															</tr>
															<tr id="infantGrid">
																<td class='defaultRowGap rowColor bdLeft bdBottom' width="3%"><label id="itnSeqNo"></label></td>
																<td class='defaultRowGap rowColor bdLeft bdBottom bdRight' width='59%'><label id="itnPaxName"></label></td>
																<td class='defaultRowGap rowColor bdBottom bdRight' width='38%'><label id="itnTravellingWith"></label></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td align="center" colspan="2">
														<u:hButton name="btnNameChange" id="btnNameChange" value="Change Passenger Details"  
														cssClass="redNormal" title="Click here to change passenger details."/>
													</td>
												</tr>																
											</table>
										<%@ include file="../common/includeFrameBottom.html"%>
										</table>
											<br/>
										<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
										<%@ include file="../common/includeFrameTop.html"%>
											<table width="100%" border="0" cellspacing="0" cellpadding="1" style="display: none;" id="flexiDetails">
												<tr>
													<td class='noPadding'>
														<div>
															<label id="lblFlexiHeader" class='hdFontColor fntBold'>Reservation Flexibilities</label>
														</div>	
														<table border='0' cellspacing='0' cellpadding='1' width='100%' class="GridTable">
															<tr>
																<td class='gridHDDark alignLeft'  width='30%'><label id="lblOriginDestination" class='gridHDFont fntBold'>Origin / Destination</label></td>
																<td class='gridHDDark alignLeft'  width='70%'><label id="lblFlexiInfo" class='gridHDFont fntBold'>Flexibilities</label></td>
															</tr>
															<tr id="flexiInfo">
																<td class='defaultRowGap rowColor bdLeft bdBottom'><label id="originDestination"></label></td>
																<td class='rowColor bdLeft bdBottom bdRight'><label id="flexiDetails"></label></td>
															</tr>
														</table>
														<div  class='rowGap'></div>	
													</td>
												</tr>
											</table>
											<table width="100%" border="0" cellspacing="0" cellpadding="1" style="display: none;" id="promotionDetails">
												<tr>
													<td class='noPadding'>
														<div>
															<label id="lblPromoHeader" class='hdFontColor fntBold'>Promotion Information</label>
														</div>	
														<table border='0' cellspacing='0' cellpadding='1' width='100%' class="GridTable">
															<tr id="trPromoInfo">
																<td class='rowColor bdLeft bdBottom bdRight'><label id="content"></label></td>
															</tr>
														</table>
														<div  class='rowGap'></div>	
													</td>
												</tr>
											</table>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" id="priceBreakDown">
												<tr>
													<td class='noPadding'>
													<div ><label id="lblPaymentInfo" class='hdFontColor fntBold'></label></div>
													<table border='0' cellspacing='0' cellpadding='1' width='100%' id="payDetail" class="GridTable">	
															<tr id="paymentDetails">
																<td class='defaultRowGap alignLeft rowColor paddingL5'><label id="labelName" ></label></td>
																<td align='right' class='rowColor'><label id="value"></label></td>
															</tr>																					     			
														</table>
													</td>
												</tr>
											</table>
												
											<div id="cancelBalanceSummary">
												<div  class='rowGap'></div>
												<div id="divRedeemReservation" class="divfont">															
												</div>
											</div>									
								
								<%@ include file="../common/includeFrameBottom.html"%>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan='2' class='rowGap'></td>
						</tr>
						<tr>
							<td align='left' id="contactInfomation" colspan="2">
								<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
								<%@ include file="../common/includeFrameTop.html"%>
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>		
									<tr>
										<td class='alignLeft paddingL5' colspan="6"><label id="lblCotactInfo" class='hdFontColor fntBold'></label></td>
									</tr>
										<tr>
										<td valign='top' colspan="3">
											<div id="spnErrorRes" class="mandatory"> </div>
											<div  class='rowGap'></div>
											<div class="div-Table" id="tblContactDetails">
												<div class="table-Row three-Col" id="trTitle">
													<div class="table-Data left-TD">
														<label id="lblTitle"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selCTitleRes' size='1' tabindex="1" name='contactInfo.title'></select>
														<label id="lblTitleMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblFirstName">
													<div class="table-Data left-TD">
														<label id="lblFirstname"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtFNameRes' name="contactInfo.firstName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="2" />
														<label id="lblFnameMand" class='mandatory'>*</label> 
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblLastName">
													<div class="table-Data left-TD">
														<label id="lblLasttname"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtLNameRes' name="contactInfo.lastName" style='width:130px;' maxlength='50'  class="fontCapitalize"  tabindex="3" />
														<label id="lblLNameMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblAddr1">
													<div class="table-Data left-TD">
														<label id="lblAddress"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtStreetRes' name='contactInfo.addresStreet' style="width:130px;" maxlength="100" tabindex="4"/>
														<label id="lblAddrMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblNationality">
													<div class="table-Data left-TD">
														<label id="lblNationality"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selNationalityRes' size='1' style='width:135px;' tabindex="18" name='contactInfo.nationality'></select>
														<label id="lblNationalityMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblAddr2">
													<div class="table-Data left-TD">
														<label>&nbsp;</label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtAddressRes' name='contactInfo.addresline' style="width:130px;" maxlength="100" tabindex="5"/>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblCity">
													<div class="table-Data left-TD">
														<label id="lblcity"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtCityRes'  name="contactInfo.city" style='width:130px;'  maxlength='20'  tabindex="6" />
														<label id="lblCityMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblZipCode">
													<div class="table-Data left-TD">
														<label id="lblZipCode"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtZipCodeRes' name="contactInfo.zipCode" style='width:130px;' maxlength='10'  tabindex="8" />
														<label id="lblZipCodeMand" class='mandatory'>*</label>
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblCountry">
													<div class="table-Data left-TD">
														<label id="lblCountry"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selCountryRes' size='1' style='width:135px;'  tabindex="9" name='contactInfo.country'>	
															<option value=""></option>						
														</select><label id="lblCountryMand" class='mandatory'>&nbsp;*</label>
														<input id='countryName' type="hidden" name='contactInfo.countryName' value="" />
													</div>
												</div>
												
												<div class="table-Row three-Col" id="tblPreferredLang">
													<div class="table-Data left-TD">
														<label id="lblPreferredLanguage"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<select id='selPrefLang' size='1' style='width:135px;'  tabindex="10" name='contactInfo.preferredLangauge'>	
															<option value=""></option>						
														</select><label id="lblPreferredLangMand" class='mandatory'>&nbsp;*</label>
													</div>
												</div>
												<div class="table-Row one-Col" style="height: 10px"><div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div></div>
												
												<div class="table-Row one-Col thin-row trPhoneNoHeader">
													<div class="table-Data one-Col">
														<label class="fntBold hdFontColor" id="lblContactDetails"></label>
													</div>
												</div>
												
												<div id="tblContactNo">
													<!-- Contact Details -->
													
												
													<div class="table-Row two-Col" id="trMobileNo">
														<div class="table-Data left-TD">
															<label id="lblMobileNo" class="hdFontColor"></label>
														</div>
														<div class="table-Row one-Col thin-row trPhoneNoLabel">
															<div class="table-Data left-TD">
																<span id="atLeasetOne"><font class="mandatory" >*&nbsp;</font> <label class="fntSmall" id="lblAtleaseOneText">Please provide at least one phone Number</label></span>
															</div>
															<div class="table-Data right-TD">
																<label id="lblCountryCode" class='fntSmall'></label>
																<label id="lblAreaCode" class='fntSmall areaCode'></label>
																<label id="lblNumber" class='fntSmall'></label>
															</div>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtMCountryRes' name="contactInfo.mCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="11" class="cunCode"/>
															<input type='text' id='txtMAreaRes' name="contactInfo.mArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="12" class="areaCode"/>
															<input type='text' id='txtMobileRes' name="contactInfo.mNumber" style='width:100px;text-align:right;' maxlength='10' tabindex="13" class="pNumber"/>
															<label id="lblMobileNoMand" class='mandatory'>*</label>
														</div>
													</div>
													
													<div class="table-Row two-Col" id="trphoneNo">
														<div class="table-Data left-TD">
															<label id="lblPhoneNo" class="hdFontColor"></label>
														</div>
														<div class="table-Row one-Col thin-row trPhoneNoLabel">
															<div class="table-Data left-TD">
																<label>&nbsp;</label>
															</div>
															<div class="table-Data right-TD">
																<label id="lblCountryCode" class='fntSmall'></label>
																<label id="lblAreaCode" class='fntSmall areaCode'></label>
																<label id="lblNumber" class='fntSmall'></label>
															</div>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtPCountryRes' name="contactInfo.lCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="14" class="cunCode"/>
															<input type='text' id='txtPAreaRes' name="contactInfo.lArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="15" class="areaCode"/>
															<input type='text' id='txtPhoneRes' name="contactInfo.lNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="16" class="pNumber" />
															<label id="lblPhoneNoMand" class='mandatory'>*</label>
														</div>
													</div>
													
													<div class="table-Row two-Col" id="trFaxNo">
														<div class="table-Data left-TD">
															<label id="lblFaxNo"></label>
														</div>
														<div class="table-Row one-Col thin-row trPhoneNoLabel">
															<div class="table-Data left-TD">
																<label>&nbsp;</label>
															</div>
															<div class="table-Data right-TD">
																<label id="lblCountryCode" class='fntSmall'></label>
																<label id="lblAreaCode" class='fntSmall areaCode'></label>
																<label id="lblNumber" class='fntSmall'></label>
															</div>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtFCountryRes' name="contactInfo.fCountry" maxlength='4'  tabindex="17" class="cunCode" />
															<input type='text' id='txtFAreaRes' name="contactInfo.fArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="18" class="areaCode"/>
															<input type='text' id='txtFaxRes' name="contactInfo.fNo" maxlength='10'  tabindex="19"  class="pNumber"/>
															<label id="lblFaxMand" class='mandatory'>&nbsp;*</label>
														</div>
													</div>
												</div>
												<div class="table-Row one-Col" style="height: 10px"><div class="dottedLine"><img src="../images/spacer_no_cache.gif"/> </div></div>
												<div id="tblEmail">
													<div class="table-Row one-Col auto-height">
														<div class="table-Data one-Col">
															<label id="lblEmailAddress" class="hdFontColor"></label>
														</div>
													</div>
													<div class="table-Row one-Col auto-height">
														<div class="table-Data one-Col">
															<label id="lblMsgValideEmail"></label>
														</div>
													</div>

													<div class="table-Row two-Col" >
														<div class="table-Data left-TD">
															<label id="lblEmailAddress"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtEmailRes' name="contactInfo.emailAddress" style='width:165px;' maxlength='100'  tabindex="20" />
															<label id="lblEmailMand" class='mandatory'>*</label>
														</div>
													</div>
													
													<div class="table-Row two-Col">
														<div class="table-Data left-TD">
															<label id="lblVerifyEmail"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<input type='text' id='txtVerifyEmailRes' name="txtVerifyEmail" style='width:165px;' maxlength='100' tabindex="21" />
														</div>
													</div>
													<!--  AARESAA-5454 - Issue 1 
													<div class="table-Row one-Col" id="trItinearyLans">
														<div class="table-Data left-TD">
															<label id="lblLanguage"></label>
														</div>
														<div class="table-Data right-TD">
															<font>:</font>
															<select id="selLanguageRes" name="contactInfo.emailLanguage"  style="width: 90px;" size="1" tabindex="22"></select>
														</div>
													</div>
													-->
												</div>
												<div class="table-Row  one-Col">
													<div class="table-Data one-Col">
														<label class="fntSmall">&nbsp;</label>
													</div>
												</div>
												<div class="end-Table"></div>
											</div>
										</td>
									</tr>
							  </table>
						<%@ include file="../common/includeFrameDivideTop.html" %>
					</table></td>
					</tr>
					<%-- Emergency Contact Information --%>
					<tr id="trEmgnContactInfo">
						<td class="alignLeft">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align='center'>
								<%@ include file="../common/includeFrameDivideBottom.html" %>
										<div class="div-Table">
											<div class="table-Row  one-Col">
												<div class="table-Data one-Col">
													<label id="lblEmgnCntInformation"  class='fntBold hdFontColor'></label>
												</div>
											</div>
											
											<div class="table-Row one-Col" id="trEmgnTitle">
												<div class="table-Data left-TD">
													<label id="lblTitle"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<select id='selEmgnTitleRes' size='1' style="width: 45px;" tabindex="23" name='contactInfo.emgnTitle'></select>
													<label id="lblEmgnTitleMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div class="table-Row two-Col" id="tblEmgnFirstName">
												<div class="table-Data left-TD">
													<label id="lblFirstname"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<input type='text' id='txtEmgnFNameRes' name="contactInfo.emgnFirstName" style='width:140px;' maxlength='50'  class="fontCapitalize"  tabindex="24" />
													<label id="lblEmgnFNameMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div class="table-Row two-Col" id="tblEmgnLastName">
												<div class="table-Data left-TD">
													<label id="lblLasttname"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<input type='text' id='txtEmgnLNameRes' name="contactInfo.emgnLastName" style='width:150px;' maxlength='50'  class="fontCapitalize"  tabindex="25" />
													<label id="lblEmgnLNameMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div id="trEmgnPhoneNo">
												<!-- Contact Details -->
												<div class="table-Row one-Col thin-row" id="trPhoneNoHeader">
													<div class="table-Data one-Col">
														<label class="fntBold hdFontColor" id="lblContactDetails"></label>
													</div>
												</div>
												
												<div class="table-Row one-Col thin-row trPhoneNoLabel">
													<div class="table-Data left-TD">
														<label>&nbsp;</label>
													</div>
													<div class="table-Data right-TD">
														<label id="lblCountryCode" class='fntSmall'></label>
														<label id="lblAreaCode" class='fntSmall areaCode'></label>
														<label id="lblNumber" class='fntSmall'></label>
													</div>
												</div>
												
												<div class="table-Row one-Col" id="trMobileNo">
													<div class="table-Data left-TD">
														<label id="lblPhoneNo"></label>
													</div>
													<div class="table-Data right-TD">
														<font>:</font>
														<input type='text' id='txtEmgnPCountryRes' name="contactInfo.emgnLCountry" style='width:50px;text-align:right;' maxlength='4'  tabindex="26" />
														<input type='text' id='txtEmgnPAreaRes' name="contactInfo.emgnLArea" style='width:50px;text-align:right;' maxlength='4'  tabindex="27" class="areaCode"/>
														<input type='text' id='txtEmgnPhoneRes' name="contactInfo.emgnLNumber" style='width:100px;text-align:right;' maxlength='10'  tabindex="28" />
														<label id="lblEmgnPhoneNoMand" class='mandatory'>*</label>
													</div>
												</div>
											</div>
											
											<div class="table-Row two-Col" id="tblEmgnEmail">
												<div class="table-Data left-TD">
													<label id="lblEmailAddress"></label>
												</div>
												<div class="table-Data right-TD">
													<font>:</font>
													<input type='text' id='txtEmgnEmailRes' name="contactInfo.emgnEmail" style='width:165px;' maxlength='100'  tabindex="29" />
													<label id="lblEmgnEmailMand" class='mandatory'>*</label>
												</div>
											</div>
											
											<div class="table-Row  one-Col">
												<div class="table-Data one-Col">
													<label class="fntSmall">&nbsp;</label>
												</div>
											</div>
											<div class="end-Table"></div>
										</div>
									</td>
									</tr>
									</table><tr><td colspan="2">
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td class='pnlMidL'></td>
											<td class='pnlMid'><label class='mandatory'>&nbsp;*&nbsp;</label> <label id="lblAreMandatory"></label></td>
											<td class='pnlMidR'></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
							<td colspan="2">
								<%@ include file="../common/includeFrameGroupBottom.html" %>
							</td>
							</tr>
							</table></td>
					</tr>
					<tr>
						<td colspan='2' class='rowGap'></td>
					</tr>
					<tr>
						<td colspan='2'>
							<table width='100%' border="0" cellspacing="0" cellpadding="1">
								<tr id="pageBottomBtnPannel">
									<td colspan='6' align='center'>
										<u:hButton name="btnUpdateRes" id="btnUpdateRes" value="Update Contact Details" tabIndex="23" 
										cssClass="redNormal" title="Click here to update contact details"/>
										<u:hButton name="btnEmailRes" id="btnEmailRes" value="Email Itinerary" tabIndex="24" 
										cssClass="redNormal" title="Click here to email itinerary"/>
										<u:hButton name="btnItineraryRes" id="btnItineraryRes" value="View Itinerary" tabIndex="25" 
										cssClass="redNormal" title="Click here to view itinerary"/>
										<%--
										<input type='button' id='btnUpdateRes' class='Button ButtonLarge' title='Click here to update contact details'  tabindex="23">
										<input type='button' id='btnEmailRes' class='Button ButtonLarge' title='Click here to email itinerary'  tabindex="24">
										<input type='button' id='btnItineraryRes' class='Button ButtonLarge' title='Click here to view itinerary'   tabindex="25"> --%>
									</td>																				
								</tr>
							</table>
						</td>
					</tr>
					<tr>
					   <td>
							<div id="divRentCar">																			
										<iframe name="frmrentcar" id="frmrentcar" src="showBlank" frameborder="0" width="500px" height="300px">
										</iframe>		
							</div>
						</td>
					</tr>		
					</table>																					
		<script type="text/javascript">
			fromPromoPage = '<c:out value="${requestScope.fromPromoPage}" escapeXml="false" />';
		</script>					
		<c:if test='${applicationScope.isDevModeOn == "false"}'>
		<script src="../js/v2/customer/reservationDetail.js? <c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		</c:if>
	
	
	<input type="hidden" id="versionRes" name="version" />
	<input type="hidden" id="selectedAncillary" name="selectedAncillary" />
	<input type="hidden" id="modifyAncillary" name="modifyAncillary" />
	<input type="hidden" id="paxJson" name="paxJson" />
	<input type="hidden" id="contactInfoJson" name="contactInfoJson" />
	<input type="hidden" id="pnr" name="pnr" />
	<input type="hidden" id="groupPNR" name="groupPNR" />
	<input type="hidden" id="oldAllSegments" name="oldAllSegments" />	
	<input type="hidden" id="makePayment" name="makePayment" />
	<input type="hidden" id="payFortOfflinePayment" name="payFortOfflinePayment" />
	<input type="hidden" id="payFortPayATHomeOfflinePayment" name="payFortPayATHomeOfflinePayment" />
	<input type="hidden" id="payFortMobileNumber" name="payFortMobileNumber" />
	<input type="hidden" id="payFortEmail" name="payFortEmail" />
	<input type="hidden" id="pgwPaymentMobileNumber" name="pgwPaymentMobileNumber" />
	<input type="hidden" id="pgwPaymentEmail" name="pgwPaymentEmail" />
	<input type="hidden" id="pgwPaymentCustomerName" name="pgwPaymentCustomerName" />
	<input type="hidden" 	id="resRegCustomer" name="commonParams.regCustomer" value='<c:out value="${commonParams.regCustomer}" escapeXml="false"/>' />
	<input type="hidden" id="jsonOnds" name="jsonOnds"/>
	<input type="hidden" id="insuranceBooked" name="insuranceBooked"/>
	<%@ include file='../../v2/common/reservationParam.jsp'%>	
	
	<!-- This is kept as a reference for the HTML code placed in the reservationDetails.js file. -->
	<!-- <div id="nameChangeTemplateContainer" style="display: none;" >
		<table id="nameChangeAdultContainer" width="100%">
			<tr>
				<td class="gridHDDark alignLeft"  width="100%" colspan="3">
					<label id="lblAdultHD" class="gridHDFont fntBold">Adults</label>
				</td>
			</tr>
			<tr>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFNameHD" class="gridHDFont fntBold">First Name</label>
				</td>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultLName" class="gridHDFont fntBold">Last Name</label>
				</td>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFFID" class="gridHDFont fntBold">AiRewards ID</label>
				</td>
			</tr>
			<tr id="nameChangeTemplateAdult">
				<td>
					<input id="itnPaxFirstName" name="itnPaxFirstName" type="text"  style="width:98%"/>
				</td>
				<td>
					<input id="itnPaxLastName" name="itnPaxLastName" type="text"  style="width:98%"/>
					<input id="actualSeqNo" type="hidden"/>
				</td>
				<td>
					<input id="itnPaxFFID" name="itnPaxFFID" type="text"  style="width:98%"/>
				</td>
			</tr>
		</table>
		<table id="nameChangeChildContainer" width="100%">
			<tr>														
				<td class="gridHDDark alignLeft"  width="100%" colspan="3">
					<label id="lblChildHD" class="gridHDFont fntBold">Children</label>
				</td>
			</tr>
			<tr>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFNameHD" class="gridHDFont fntBold">First Name</label>
				</td>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultLName" class="gridHDFont fntBold">Last Name</label>
				</td>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFFID" class="gridHDFont fntBold">AiRewards ID</label>
				</td>
			</tr>
			<tr id="nameChangeTemplateChild">
				<td>
					<input id="itnPaxFirstName" name="itnPaxFirstName" type="text" style="width:98%"/>
				</td>
				<td>
					<input id="itnPaxLastName" name="itnPaxLastName" type="text" style="width:98%"/>
					<input id="actualSeqNo" type="hidden"/>
				</td>
				<td>
					<input id="itnPaxFFID" name="itnPaxFFID" type="text"  style="width:98%"/>
				</td>
			</tr>
		</table>
		<table id="nameChangeInfantContainer" width="100%">
			<tr>
				<td class="gridHDDark alignLeft"  width="100%" colspan="3">
					<label id="lblInfantHD" class="gridHDFont fntBold">Infants</label>
				</td>
			</tr>
			<tr>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFNameHD" class="gridHDFont fntBold">First Name</label>
				</td>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultLName" class="gridHDFont fntBold">Last Name</label>
				</td>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFFID" class="gridHDFont fntBold">AiRewards ID</label>
				</td>
			</tr>
			<tr id="nameChangeTemplateInfant">
				<td>
					<input id="itnPaxFirstName" name="itnPaxFirstName" type="text"  style="width:98%"/>
				</td>
				<td>
					<input id="itnPaxLastName" name="itnPaxLastName" type="text" style="width:98%"/>
					<input id="actualSeqNo" type="hidden"/>
				</td>
			</tr>
		</table>
		<input type="button" id="btnCNXNameChange" class="Button ButtonLarge" title="Click here to cancel name change"  tabindex="23" value="Cancel"/>
		<input type="button" id="btnCNFNameChange" class="Button ButtonLarge" title="Click here to confirm name change"  tabindex="24" value="Confirm"/>
	</div> -->
	<!-- <div id="nameChangeTemplateContainer" style="display: none;" >
		<table id="ffidChangeAdultContainer" width="100%">
			<tr>
				<td class="gridHDDark alignLeft"  width="100%" colspan="3">
					<label id="lblAdultHD" class="gridHDFont fntBold">Adults</label>
				</td>
			</tr>
			<tr>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFFIDHD" class="gridHDFont fntBold">Airewards ID</label>
				</td>
			</tr>
			<tr id="ffidChangeTemplateAdult">
				<td>
					<input id="itnPaxFFID" name="itnPaxFFID" type="text"  style="width:98%"/>
					<input id="actualSeqNo" type="hidden"/>
				</td>
			</tr>
		</table>
		<table id="ffidChangeChildContainer" width="100%">
			<tr>
				<td class="gridHDDark alignLeft"  width="100%" colspan="3">
					<label id="lblChildHD" class="gridHDFont fntBold"> Children</label>
				</td>
			</tr>
			<tr>
				<td class="gridHDDark alignLeft">
					<label id="lblAdultFFIDHD" class="gridHDFont fntBold"> Airewards ID</label>
				</td>
			</tr>
			<tr id="ffidChangeTemplateChild">
				<td>
					<input id="itnPaxFFID" name="itnPaxFFID" type="text" style="width:98%"/>
					<input id="actualSeqNo" type="hidden"/>
				</td>
			</tr>
		</table>
	<input type="button" id="btnCNXFFIDChange" class="Button ButtonLarge" title="Click here to cancel Airewards ID change"  tabindex="25" value="Cancel"/>
	<input type="button" id="btnCNFFFIDChange" class="Button ButtonLarge" title="Click here to confirm Airewards ID change"  tabindex="26" value="Confirm"/>
</div> -->
	
	
	<div id="nameChangePopup" style="display: none;" >
		<%@ include file='../../common/includePopup.jsp' %>
	</div>
	<div id="ffidChangePopup" style="display: none;" >
		<%@ include file='../../common/includePopup.jsp' %>
	</div>
	<div id="accelAeroIBETrackYandex" style="display: none;"></div>
		
	</div>
</form>