<div id="divCardInfoPannel">	
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="customCCPanel">
						<%@ include file="../common/includeFrameTop.html"%>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" >													 
											<tr height="40px;">
												<td class="alignLeft paddingCalss">		
													<span class="secure-image"></span>
													<div style="float: left;">
														<span class="pageName">
															<label  class="fntBold hdFontColor"  id="lblCardInfoSecure"> Secure Credit Card Payment </label>
														</span>
														<br/>
														<label  class="fntBold"  id="lblCardSecureEncrypted"> This is secure 128-bit SSL encrypted payment </label>
													</div>
													<div style="float: right;" class="floatright secure_card_img">
														<img  src="../images/cards_secure_no_cache.gif">
														<img  src="../images/safekey_no_cache.gif">
													</div>
													<div class="clear"></div>		
												</td>									
											</tr>
										</table>
								<%@ include file="../common/includeFrameDivideTop.html" %>
								<%@ include file="../common/includeFrameDivideBottom.html" %>
										<label  class="fntBold fntMedium"  id="lblCardPaymentMethod"> Please select your payment method </label>
										<br/>																																					
										<div style="padding-top: 5px">		
											<ul class="ul-cards" id="listCard"> </ul>
											<div class="clear"></div>									
										</div>
										<div class='rowGap'></div>
								<%@ include file="../common/includeFrameDivideTop.html" %>
							</table>
				</td>
				</tr>
					<tr>
						<td colspan="3">			
																			
										<div id="cardDetailPannel" style="display: none">									
																				
											   <table width="100%" border="0" cellspacing="0" cellpadding="0">
												<%@ include file="../common/includeFrameDivideBottom.html" %>											
													<div id="lblError" class="mandatory paddingL5 alignLeft"></div>													
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td width="50%">
																	<table>																															
																		<tr>
																			<td class="alignLeft" colspan="2">
																				<label id="lblCardHolderName" class="fntBold"> Cardholder Name </label>
																			</td>														
																		</tr>
																		<tr>
																			<td class="alignLeft" colspan="2">																
																				<input type='text' id='cardHoldersName' name="card.cardHoldersName" style='width:200px;' maxlength='250'  class="fontCapitalize"/>
																				<label class='mandatory'>&nbsp;*</label>														
																			</td>
																		</tr>
																		<tr>
																			<td class="alignLeft" colspan="2">
																				<label id="lblCardNumber" class="fntBold"> Card Number </label>
																			</td>
																		</tr>
																		<tr>
																			<td class="alignLeft">
																				<!-- ClickTaleSensitive is clicktale's sensitive date secure class -->																
																				<input type='text' id='cardNumber' name="card.cardNo" style='width:140px;' maxlength='16' class="ClickTaleSensitive"/><label class='mandatory'>&nbsp;*</label>														
																			</td>
																			<td class="alignRight">
																				<button type ="button" id="checkPromotionsForBIN" class="isabutton" style="display: none">Check for Promotions</button>
																			</td>
																		</tr>
																		
																		<tr>
																			<td class="alignLeft" colspan="2">
																				<label id="lblCardExpiryDate" class="fntBold"> Card Expiry Date(MM/YYYY) </label>
																			</td>
																		</tr>
																		<tr>
																			<td class="alignLeft" colspan="2">																
																				<select id='expiryMonth' name="expiryMonth" size='1'>																	
																				</select>																
																				&nbsp;	<label>/</label>&nbsp;	
																				<select id="expiryYear" name="expiryYear" style="width:55px">																	
																				</select>														
																				<label class='mandatory'>&nbsp;*</label>
																				<!-- ClickTaleSensitive is clicktale's sensitive date secure class -->
																				<input type="hidden" id="expiryDate" name="card.expiryDate"	value="" class="ClickTaleSensitive"/>												
																			</td>
																		</tr>
																		
																		<tr>
																			<td class="alignLeft" colspan="2">
																				<label id="lblSecurityCode" class="fntBold"> Security Code </label>
																			</td>
																		</tr>
																		<tr>
																			<td class="alignLeft" colspan="2">
																				<!-- ClickTaleSensitive is clicktale's sensitive date secure class -->
																				<input type="text" id="cardCVV" name="card.cardCVV"  maxlength="4" size="4" class="ClickTaleSensitive"/><label class='mandatory'>&nbsp;*</label>
																				<a href="javascript:void(0)" style="text-decoration: underline"><label id='whatisthis' class="fntSmallSm">What is this?</label></a>	
																			</td>
																		</tr>
																		<tr>
																			<td class='rowGap' colspan="2"></td>
																		</tr>	
																	</table>
																	<div id="extraCardDetailForPayPalPannel" style="display: none">
																		  <table>
																		   <tr>
																			  <td class="alignLeft">
																				 <label id="lblBillingAddress1" class="fntBold"> Billing Address1 </label>
																			  </td>														
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">																
																				  <input type='text' id='billingAddress1' name="card.billingAddress1" style='width:200px;' maxlength='250'  class="fontCapitalize"/>
																				  <label class='mandatory'>&nbsp;*</label>														
																			  </td>
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">
																				 <label id="lblBillingAddress2" class="fntBold"> Billing Address2 </label>
																			  </td>														
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">																
																				  <input type='text' id='billingAddress2' name="card.billingAddress2" style='width:200px;' maxlength='250'  class="fontCapitalize"/>
																				  														
																			  </td>
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">
																				 <label id="lblCity" class="fntBold"> City </label>
																			  </td>														
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">																
																				  <input type='text' id='city' name="card.city" style='width:200px;' maxlength='250'  class="fontCapitalize"/>
																				  <label class='mandatory'>&nbsp;*</label>														
																			  </td>
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">
																				 <label id="lblPostalCode" class="fntBold"> Postal Code </label>
																			  </td>														
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">																
																				  <input type='text' id='postalCode' name="card.postalCode" style='width:200px;' maxlength='250'  class="fontCapitalize"/>
																				  <label class='mandatory'>&nbsp;*</label>														
																			  </td>
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">
																				 <label id="lblState" class="fntBold"> State </label>
																			  </td>														
																		   </tr>
																		   <tr>
																			  <td class="alignLeft">																
																				  <input type='text' id='state' name="card.state" style='width:200px;' maxlength='250'  class="fontCapitalize"/>
																				  <label class='mandatory'>&nbsp;*</label>														
																			  </td>
																		   </tr>
																		   </table>
																		</div>
																	
																</td>
																<td valign="top">
																	<table width="94%" border="0" cellspacing="0" cellpadding="6" height="100%" style="margin-top: 10px">
																		<tr>
																			<td class='defaultRowGap totalCol paddingL5 alightLeft'><label id="lblTotalDue" class='fntBold fntWhite'></label></td>
																			<td  class='totalCol alignRight paddingR5'>
																				<label id="lblTotalDueAmount" name='lblTotalDueAmount' class='fntBold fntWhite'></label>
																			 	<label name='valBaseCurrCode' class='fntBold fntWhite'></label>
																			</td>
																		</tr>
																		<tr><td colspan="2" ></td></tr>
																		<tr>
																			<td colspan="2" style="background: #ffffcf; border: 1px solid #aaa;">
																					<label id="lblPaymentGeneralInfo3">																					
																					</label>
																			</td>
																		</tr>
																		<tr>
																			<td valign="bottom"" class="alignRight" colspan="2">
																				<img  src="../images/VSign_no_cache.gif">
																			</td>
																		</tr>													
																	</table>
																</td>
															</tr>
																																																							
														</table>
													<%@ include file="../common/includeFrameDivideTop.html" %>
													</table>
											</div>
											<div id="paymentInfoPanel" style="display: none">	
											
												<div id="qiwiDetailsPanel" style="display: none">									
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="pnlMid" width="50%"><div id="lblQiwiError" class="mandatory paddingL5 alignLeft"></div>
																</td>
															</tr>
															<tr>																
																<td class="pnlMid" width="50%">
																	<table>																															
																		<tr>
																			<td class="pnlMidL">
																				<img src="../images/spacer_no_cache.gif">
																			</td>																																				
																			<td class="alignLeft">
																				<label id="lblQiwiMobileNumber" class="fntBold">QIWI Registered Mobile Number </label>
																			</td>														
																		</tr>
																		<tr>
																			<td class="pnlMidL">
																				<img src="../images/spacer_no_cache.gif">
																			</td>
																			<td class="alignLeft">
																				<label class="fntBold">&nbsp;+</label>																	
																				<input type='text' title="Enter the Mobile Number for Your Qiwi Account"  id='qiwiMobileNumber' name="qiwiMobileNumber" style='width:200px;' maxlength='250'  class="fontCapitalize pNumber"/>
																				<label class='mandatory'>&nbsp;*</label>														
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
												</div>
												<div id="payfortDetailsPanel" style="display: none">									
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="pnlMid" width="50%"><div id="lblPayFortError" class="mandatory paddingL5 alignLeft"></div>
																</td>
															</tr>
															<tr>
																<td class="pnlMid" width="50%">
																<label id="lblPayFortMessage" class="fntBold">Please Enter the Mobile and E mail Address to Receive the Voucher Details </label>
																</td>
															</tr>
															<tr>																
																<td class="pnlMid" width="50%">
																	<table>																															
																		<tr>
																			<td class="pnlMidL">
																				<img src="../images/spacer_no_cache.gif">
																			</td>																																				
																			<td class="alignLeft">
																				<label id="lblPayFortMobileNumber" class="fntBold">PayFort Mobile Number </label>
																			</td>		
																			<td class="alignLeft">
																				<label class="fntBold">&nbsp;+</label>																	
																				<input type='text' title="Enter the Mobile Number for Your PayFort Transcation"  id='payFortMobileNumber' name="payFortMobileNumber" style='width:200px;' maxlength='250'  class="fontCapitalize pNumber"/>
																				<label class='mandatory'>&nbsp;*</label>														
																			</td>												
																		</tr>
																		<tr>
																			<td class="pnlMidL">
																				<img src="../images/spacer_no_cache.gif">
																			</td>																																				
																			<td class="alignLeft">
																				<label id="lblPayFortEmail" class="fntBold">PayFort Email Address </label>
																			</td>			
																			<td class="alignLeft">
																				<label class="fntBold">&nbsp;&nbsp;&nbsp;</label>																	
																				<input type='text' title="Enter the Email for Your PayFort Transcation"  id='payFortEmail' name="'payFortEmail'" style='width:200px;' maxlength='250' />
																				<label class='mandatory'>&nbsp;*</label>														
																			</td>											
																		</tr>	
																	</table>
																</td>
															</tr>
														</table>
												</div>
												<div id="pgwCommonDetailsPanel" style="display: none">									
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="pnlMidL"><img
																	src="../images/spacer_no_cache.gif"></td>
																<td class="pnlMid" width="100%" colspan="3"><div id="lblPaymentCommonError" class="mandatory paddingL5 alignLeft"></div>
																</td>
																<td class="pnlMidR"><img src="../images/spacer_no_cache.gif"></td>
															</tr>
															<tr>
																<td class="pnlMidL"><img
																		src="../images/spacer_no_cache.gif"></td>
																<td class="pnlMid" width="100%" colspan="3">
																	<label id="lblPaymentCommonMessage" class="fntBold">Please Enter Required Fields </label>
																</td>
																<td class="pnlMidR"><img src="../images/spacer_no_cache.gif"></td>
															</tr>
															<tr>
																<td class="pnlMidL"><img
																	src="../images/spacer_no_cache.gif"></td>
																<td class="pnlMid"></td>
																<td class="pnlMid" width="25%">
																	<label
																	id="lblPaymentCommonMobileNumber" >Payment
																		Mobile Number </label></td>
																<td class="pnlMid"><label class="fntBold">&nbsp;+</label>
																	<input type='text'
																	title="Enter the Mobile Number for Your Payment Transcation"
																	id='cdPGWPaymentMobileNumber' name="cdPGWPaymentMobileNumber"
																	style='width: 200px;' maxlength='250'
																	class="fontCapitalize pNumber" /> <label class='mandatory'>&nbsp;*</label>
																</td>
																<td class="pnlMidR"><img src="../images/spacer_no_cache.gif"></td>
															</tr>
															<tr>
																<td class="pnlMidL"><img
																	src="../images/spacer_no_cache.gif"></td>
																<td class="pnlMid"></td>
																<td class="pnlMid" width="25%">
																	<label id="lblPaymentCommonEmail" >Payment Email Address </label></td>
																<td class="pnlMid">
																	<input type='text'
																	title="Enter the Email for Your Payment Transcation"
																	id='cdPGWPaymentEmail' name="cdPGWPaymentEmail"
																	style='width: 200px;' maxlength='250' /> <label
																	class='mandatory'>&nbsp;*</label>
																</td>
																<td class="pnlMidR"><img src="../images/spacer_no_cache.gif"></td>
															</tr>
															<tr>
																<td class="pnlMidL"><img
																	src="../images/spacer_no_cache.gif"></td>
																<td class="pnlMid"></td>
																<td class="pnlMid" width="25%">
																	<label id="lblPaymentUsername" >Payment UserName </label></td>
																<td class="pnlMid">
																	<input type='text'
																	title="Enter the Username for Your Payment Transcation"
																	id='cdPGWPaymentCustomerName' name="cdPGWPaymentCustomerName"
																	style='width: 200px;' maxlength='250'
																	class="fontCapitalize pNumber" /> <label class='mandatory'>&nbsp;*</label>
																</td>
																<td class="pnlMidR"><img src="../images/spacer_no_cache.gif"></td>
															</tr>
														</table>
												</div>
											
											
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<%@ include file="../common/includeFrameDivideBottom.html" %>
														<div id="lblErrorExternalIPG" class="mandatory paddingL5 alignLeft"></div>	
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class='rowGap'></td>
															</tr>
															<tr>
																<td width="50%">																	
																	<label  class="fntBold fntMedium"  id="lblPaymentInfoHeading"> You will be redirect to the payment gateway website to complete the transaction. </label>
																	<br/>
																	<br/>
																	<label  class="fntBold"  id="lblPaymentGeneralInfo1"> 
																		After you confirm the payment, you will return to this page and will be provided with
																		your unique flight reservation number.
																	</label>
																	<br/>																	
																	<label  class="fntBold"  id="lblPaymentGeneralInfo2"> 
																		The transaction process may take a few minutes depending on various factors like the time of day and  your internet connection 
																		speed.Please be patient during the process. 
																	</label>																	
																</td>
																<td valign="top">
																	<table width="94%" border="0" cellspacing="0" cellpadding="6" height="100%">
																		<tr>
																			<td class='defaultRowGap totalCol paddingL5 alightLeft'><label id="lblTotalDue" class='fntBold fntWhite'></label></td>
																			<td  class='totalCol alignRight paddingR5'>
																				<label id="lblTotalDueAmount" name='lblTotalDueAmount' class='fntBold fntWhite'></label>
																				<label name='valBaseCurrCode' class='fntBold fntWhite'></label>
																			</td>
																		</tr>
																		<tr><td colspan="2"></td></tr>
																		<tr>
																			<td colspan="2" style="background: #ffffcf; border: 1px solid #aaa;">
																					<label id="lblPaymentGeneralInfo3">																					
																					</label>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class='rowGap'></td>
															</tr>
														</table>
												<%@ include file="../common/includeFrameDivideTop.html" %>
												</table>
											</div>	
											
											<div id="onHoldInfoPanel" style="display: none">	
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<%@ include file="../common/includeFrameDivideBottom.html" %>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class='rowGap'></td>
															</tr>
															<tr>
																<td width="50%" style="padding:0 5px">																	
																	<label  class=""  id="lblPaymentInfoonHold">If you select "Cash payment" you will have</label>&nbsp;
																	<label class="fntBold" id="onHoldDisplayTime1">xx</label>&nbsp;
																	<label  class=""  id="lblPaymentInfoonHold1">
																		 to pay for your reservation through one of Air Arabia's selected payment partners. 
																	</label>&nbsp;
																	<a href="javascript:void(0)" id="onhold_Partners"><u><label id="lblPaymentInfoonHold2" class="hdFontColor">Click here</label></u></a>&nbsp;
																	<label id="lblPaymentInfoonHold3">to see the list of Air Arabia payment partners.</label>
																	<br/>																	
																</td>
																<td valign="top">
																	<table width="94%" border="0" cellspacing="0" cellpadding="6" height="100%">
																		<tr>
																			<td class='defaultRowGap totalCol paddingL5 alightLeft'><label id="lblTotalDue" class='fntBold fntWhite'></label></td>
																			<td  class='totalCol alignRight paddingR5'>
																				<label id="lblTotalDueAmount" name='lblTotalDueAmount' class='fntBold fntWhite'></label>
																				<label name='valBaseCurrCode' class='fntBold fntWhite'></label>
																			</td>
																		</tr>
																		<tr><td colspan="2"></td></tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class='rowGap' colspan="2"></td>
															</tr>
															<tr>
																<td colspan="2">
																	<div id="imageCap">
																		<table cellpadding="2" cellspacing="0" width="100%">
																			<tr>
																				<td>&nbsp;</td>
																				<td><img src="../jcaptcha.jpg" align="top"
																					style="cursor: pointer;" name="imgCPT" id="imgCPT" /> <br />
																				<label id="lblChangeImage">Please Click on the image to
																				change</label></td>
																				<td>&nbsp; : &nbsp; <input type="text"
																					style="width: 220px;" name="captcha" id="txtCaptcha" /> <font
																					class="mandatory"> *</font></td>
																			</tr>
																		</table>
																	</div>
																</td>
															</tr>
															<tr>
																<td class='rowGap' colspan="2"></td>
															</tr>
														</table>
												<%@ include file="../common/includeFrameDivideTop.html" %>
												</table>
											</div>
											<div id="externalEmbaded" style="display: none">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<%@ include file="../common/includeFrameDivideBottom.html" %>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr id="intExtProcessing" style="display: none">
															<td>
																<div style="text-align: center;" class="fntBold, fntRed">
																	<img  src="../images/tr-processing_en_no_cache.gif">
																</div>
															</td>
														</tr>																
														<tr>
															<td id="paymentPannelPosition">

															</td>
														</tr>
														<tr><td colspan="3">
															<div class="spMsg alignLeft">
																<label id="lblPaymentGeneralInfo4"></label>
															</div>
														</td></tr>
														<tr>
															<td class='rowGap' colspan="2"></td>
														</tr>
													</table>
													<%@ include file="../common/includeFrameDivideTop.html" %>
												</table>
											</div>																			
										</td>										
									</tr>
						</table>
		<div class="popuploader" style="display:none">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr><td class="headerBG hLeft"><img border='0' src="../images/spacer_no_cache.gif"/></td><td class="headerBG hCenter" style="width:350px">
			<label class="fntBold gridHDFont"></label>
			<a class="closethisp" href="javascript:void(0)">
			<img  src="../images/popupClose_no_cache.gif" />
			</a><div style="clear:both"></div>
			</td><td class="headerBG hRight"><img border='0' src="../images/spacer_no_cache.gif"/></td></tr><tr><td colspan="3" class="alignLeft">
			<div class="popupbody"></div></td></tr></table>
		</div>	
		
		<input type="hidden" name="card.cardType" id="cardType" />	
		<input type="hidden" name="card.paymentMethod" id="paymentMethod" />
</div>
		