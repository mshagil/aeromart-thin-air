<input type="hidden" 	id="resFromAirport" 		name="searchParams.fromAirport" value="<c:out value="${searchParams.fromAirport}" escapeXml="false"/>" >
<input type="hidden" 	id="resToAirport" 			name="searchParams.toAirport"  value="<c:out value="${searchParams.toAirport}" escapeXml="false"/>" >
<input type="hidden" 	id="resDepartureDate" 		name="searchParams.departureDate"  value="<c:out value="${searchParams.departureDate}" escapeXml="false"/>"  >
<input type="hidden" 	id="resReturnDate" 			name="searchParams.returnDate" value="<c:out value="${searchParams.returnDate}" escapeXml="false"/>" >
<input type="hidden" 	id="resDepartureVariance" 	name="searchParams.departureVariance" value="<c:out value="${searchParams.departureVariance}" escapeXml="false"/>" >
<input type="hidden" 	id="resReturnVariance" 		name="searchParams.returnVariance" value="<c:out value="${searchParams.returnVariance}" escapeXml="false"/>" >
<input type="hidden" 	id="resSelectedCurrency" 	name="searchParams.selectedCurrency"  value="<c:out value="${searchParams.selectedCurrency}" escapeXml="false"/>" >
<input type="hidden" 	id="resAdultCount" 			name="searchParams.adultCount"  value="<c:out value="${searchParams.adultCount}" escapeXml="false"/>" >
<input type="hidden" 	id="resChildCount" 			name="searchParams.childCount" value="<c:out value="${searchParams.childCount}" escapeXml="false"/>">
<input type="hidden" 	id="resInfantCount" 		name="searchParams.infantCount" value="<c:out value="${searchParams.infantCount}" escapeXml="false"/>">
<input type="hidden" 	id="resFromAirportName" 	name="searchParams.fromAirportName" value="<c:out value="${searchParams.fromAirportName}" escapeXml="false"/>">
<input type="hidden" 	id="resToAirportName" 		name="searchParams.toAirportName" value="<c:out value="${searchParams.toAirportName}" escapeXml="false"/>">
<input type="hidden" 	id="resReturnFlag" 			name="searchParams.returnFlag" value="<c:out value="${searchParams.returnFlag}" escapeXml="false"/>">
<input type="hidden" 	id="resFareType" 			name="searchParams.fareType" value="<c:out value="${searchParams.fareType}" escapeXml="false"/>">
<input type="hidden" 	id="resSearchSystem" 		name="searchParams.searchSystem" value="<c:out value="${searchParams.searchSystem}" escapeXml="false"/>">
<input type="hidden" 	id="resClassOfService" 	    name="searchParams.classOfService" value='<c:out value="${searchParams.classOfService}" escapeXml="false"/>'>
<input type="hidden" 	id="resFirstDeparture" 	    name="searchParams.firstDeparture" value='<c:out value="${searchParams.firstDeparture}" escapeXml="false"/>'>
<input type="hidden" 	id="resLastArrival" 	    name="searchParams.lastArrival" value='<c:out value="${searchParams.lastArrival}" escapeXml="false"/>'>
<input type="hidden" 	id="resFareQuote" 			name="fareQuoteJson" value='<c:out value="${fareQuoteJson}" escapeXml="false"/>'>
<input type="hidden" 	id="resSelectedFlights" 	name="selectedFlightJson" value='<c:out value="${selectedFlightJson}" escapeXml="false"/>'>
<input type="hidden" 	id="resHalfReturnFareQuote"	name="searchParams.halfReturnFareQuote" value="<c:out value="${searchParams.halfReturnFareQuote}" escapeXml="false"/>">
<input type="hidden" 	id="resStayOverTimeInMillis"  name="searchParams.stayOverTimeInMillis" value="<c:out value="${searchParams.stayOverTimeInMillis}" escapeXml="false"/>">
<input type="hidden" 	id="resFlexiSelection"  name="flexiSelection" value='<c:out value="${flexiSelection}" escapeXml="false"/>'>
<input type="hidden" 	id="resFromAirportSubStation"  	name="searchParams.fromAirportSubStation" value='<c:out value="${searchParams.fromAirportSubStation}" escapeXml="false"/>'>
<input type="hidden" 	id="resToAirportSubStation" 	name="searchParams.toAirportSubStation" value='<c:out value="${searchParams.toAirportSubStation}" escapeXml="false"/>'>
<input type="hidden" 	id="resFareQuoteLogicalCCSelection" 	name="searchParams.fareQuoteLogicalCCSelection" value='<c:out value="${searchParams.fareQuoteLogicalCCSelection}" escapeXml="false"/>'>
<input type="hidden" 	id="resFareQuoteBookingCCSelection" 	name="searchParams.fareQuoteOndSegBookingClassSelection" value='<c:out value="${searchParams.fareQuoteOndSegBookingClassSelection}" escapeXml="false"/>'>
<input type="hidden" 	id="resQuoteOutboundFlexi" 			name="searchParams.quoteOutboundFlexi" value='<c:out value="${searchParams.quoteOutboundFlexi}" escapeXml="false"/>'/>
<input type="hidden" 	id="resQuoteInboundFlexi" 			name="searchParams.quoteInboundFlexi" value='<c:out value="${searchParams.quoteInboundFlexi}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndQuoteFlexi" 			name="searchParams.ondQuoteFlexiStr" value='<c:out value="${searchParams.ondQuoteFlexiStr}" escapeXml="false"/>'/>
<input type="hidden" 	id="onPreviousClick" 			name="onPreviousClick" />
<input type="hidden" 	id="resFlexiSelected" 			name="searchParams.flexiSelected" value='<c:out value="${searchParams.flexiSelected}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndAvailbleFlexiStr" 			name="searchParams.ondAvailbleFlexiStr" value='<c:out value="${searchParams.ondAvailbleFlexiStr}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndSelectedFlexiStr" 			name="searchParams.ondSelectedFlexiStr" value='<c:out value="${searchParams.ondSelectedFlexiStr}" escapeXml="false"/>'/>
<input type="hidden" 	id="resflightSearchOndFlexiSelection" 			name="searchParams.flightSearchOndFlexiSelectionStr" value='<c:out value="${searchParams.flightSearchOndFlexiSelectionStr}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndwiseFlexiAvilableForAnci" 			name="searchParams.resOndwiseFlexiAvilableForAnci" value='<c:out value="${searchParams.resOndwiseFlexiAvilableForAnci}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndwiseFlexiChargesForAnci" 			name="searchParams.resOndwiseFlexiChargesForAnci" value='<c:out value="${searchParams.resOndwiseFlexiChargesForAnci}" escapeXml="false"/>'/>
<input type="hidden" 	id="resBookingType" 			name="searchParams.bookingType" value='<c:out value="${searchParams.bookingType}" escapeXml="false"/>'/>
<input type="hidden" 	id="resPromoCode" 			name="searchParams.promoCode" value='<c:out value="${searchParams.promoCode}" escapeXml="false"/>'/>
<input type="hidden" 	id="resBankIdNo" 			name="searchParams.bankIdentificationNo" value='<c:out value="${searchParams.bankIdentificationNo}" escapeXml="false"/>'/>
<input type="hidden" 	id="resPreviousSearch" 		name="searchParams.isPreviousSearch" value='<c:out value="${searchParams.isPreviousSearch}" escapeXml="false"/>'/>
<input type="hidden" 	id="resFlightSearchDates" 	name="searchParams.flightSearchDates" value='<c:out value="${searchParam.flightSearchDates}" escapeXml="false"/>'/>
<input type="hidden" 	id="resGMTOffset" 	name="searchParams.gmtOffset" value='<c:out value="${searchParams.gmtOffset}" escapeXml="false"/>'/> 
<input type="hidden" 	id="resHubTimeDetails" 	name="searchParams.hubTimeDetails" value='<c:out value="${searchParams.hubTimeDetails}" escapeXml="false"/>'/>
<input type="hidden" 	id="resHubTimeDetailJSON" 	name="searchParams.hubTimeDetailJSON" value='<c:out value="${searchParams.hubTimeDetailJSON}" escapeXml="false"/>'/> 
<input type="hidden" 	id="resOndListStr" name="searchParams.ondListString" value="<c:out value="${searchParams.ondListString}" escapeXml="true"/>"/>
<input type="hidden" 	id="respromotionInfo" 			name="promoInfoJson" value='<c:out value="${promoInfoJson}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndBundleFareStr" 			name="searchParams.preferredBundledFares" value='<c:out value="${searchParams.preferredBundledFares}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndBookingClassStr" 			name="searchParams.preferredBookingCodes" value='<c:out value="${searchParams.preferredBookingCodes}" escapeXml="false"/>'/>
<input type="hidden" 	id="resOndSegBookingClassStr" 		name="searchParams.ondSegBookingClassStr" value='<c:out value="${searchParams.ondSegBookingClassStr}" escapeXml="false"/>'/>
<input type="hidden" 	id="resAvailableBundleFareLCClassStr" 			name="searchParams.availableBundleFareLCClass" value='<c:out value="${searchParams.availableBundleFareLCClass}" escapeXml="false"/>'/>
<input type="hidden" 	id="resModFlexiSelected" 			name="searchParams.modifyFlexiSelected" value='<c:out value="${searchParams.modifyFlexiSelected}" escapeXml="false"/>'/>
<input type="hidden" 	id="resPointOfSale" 			name="searchParams.pointOfSale" value='<c:out value="${commonParams.pointOfSale}" escapeXml="false"/>'/>
<input type="hidden" 	id="resPaxType" 			name="searchParams.paxType" value='<c:out value="${searchParams.paxType}" escapeXml="false"/>'/>