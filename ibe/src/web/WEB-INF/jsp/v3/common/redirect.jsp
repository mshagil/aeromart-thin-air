<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<form id='frmRedirect' action='<c:out value="${redirectUrl}"/>' method="post">
		<input type="hidden" name="hdnParamData" value='<c:out value="${hdnParamData}"/>'/>
	</form>
	
	<script type="text/javascript">
		document.getElementById("frmRedirect").submit();
	</script>
</body>
</html>