<tr>
    <td colspan="2">
        <div id="regUser" style="display: none">
            <%-- Logged User --%>
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                <tr>
                    <td class="alignLeft paddingL5" width="100%">
						<span id="spnUN">
							<label id="lblWelcome">  </label>&nbsp;
							<label id="lblTxtName" class="fntBold hdFontColor fontCapitalize"></label>&nbsp;
                            <label id="lblLogOut" style="cursor: pointer;white-space: nowrap">Sign Out</label>
						</span>
                    </td>
                </tr>
                <tr id="tr-LMS-LoggedIn">
                    <td colspan="2" class="paddingL5">
                        <label id="lblAirewardsID"	>Airewards ID</label><label class="">:</label>
                        <label id="lblAirewardsIDValue" class="hdFontColor trimMe" data-len="25"></label><br/>
                        <label id="lblAirewardsPointAvaialble">Available Airewards Points</label><label>:</label>
                        <label id="lblAirewardsPointAvaialbleValue" class="hdFontColor fontCapitalize"></label><br/>
                    </td>
                </tr>
                <tr id="tr-LMS-NotConf">
                	<td class="paddingL5">
                		<label id="lblAirRewardsMailConf" class="hdFontColor fontCapitalize">Confirmation Email Sent<br/>
                        									Click the Confirmation Link to Confirm</label><br/>
                	</td>
                </tr>
                </tbody>
            </table>

            <div style="padding-bottom: 10px"></div>
        </div>
        <%-- Logged User --%>
    </td>
</tr>