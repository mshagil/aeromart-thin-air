<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
   	<META HTTP-EQUIV="Expires" CONTENT="-1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/<fmt:message key="msg.pg.css.name"/>'>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>    
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	  
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		
	<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
    <style type="text/css">     
       .hoverbox {
		cursor: default;
		list-style: none;
	 }

	.hoverbox a	{
		cursor: default;
	}
	
	.hoverbox a .preview {
		display: none;
	}
	
	.hoverbox a:hover .preview {
		display: block;
		position: absolute;
		top: -33px;
		left: -45px;
		z-index: 1;
	}
	
	.hoverbox img {
		background: #fff;
		border-color: #ff0000 #ff0000 #ff0000 #ff0000;
		border-style: solid;		
		color: inherit;
		float:left;		
		vertical-align: top;
		width: 50px;
		height: 50px;
	}
	
	.hoverbox li {
		background: #eee;
		border-color: #ff0000 #ff0000 #ff0000 #ff0000;
		color: inherit;
		display: inline;
		float: left;		
		position: relative;
	}
	
	.hoverbox .preview {
		border-color: #ff0000;
		width: 300px;
		height: 300px;
	}
    </style>
  </head>
  <body>  
	<table width="900" cellpadding="0" cellspacing="0" border="5" align="center" class="outLine">
		<tr>
			<td class="outLine">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>
		<tr>
			<td style='background-color:'red'>
				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td width="50%">
							<img border=0; src="<fmt:message key="msg.pg.img.folder"/>LogoAni<c:out value="${sessionScope.sysCarrier}" escapeXml="false" />_no_cache.gif">
						</td>
						
					</tr>
				</table>
			</td>
		</tr>
	</table>
			</td>
		</tr>
		<tr><td></td></tr>
		<tr>
			<td class="outLine">
	<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' class='PageBackGround'>			
		<tr><td class="sperateorBar outline"><img src="../images/spacer_no_cache.gif" alt=""></td></tr>
		<tr>
			<td class="pageBody">
				<br>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td valign='top' align="center"><span id="spnMeals"></span></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<%@ include file='../common/includeBottom.jsp'%>
	<script type="text/javascript">	
	var selMeals = opener.UI_Anci.getDisplayMeals();	
	var count = Math.floor(selMeals.length/3);		
	var mMod = selMeals.length%3;
	var strCurrency = opener.UI_Anci.getDefaultCurrency();
	var mealCount = 0;
	var strNotMessage = 'Image Is Not Available';
	var halaMessage  = "All Meals served on board Air Arabia is Halal";
	var strHtml = "<table width='80%' border='0' cellpadding='0' cellspacing='0' style='font-size:1em;'>"
	strHtml += "<tr>";
	strHtml += "<td colspan='3' align='left'><font style='color:red;font-size:17;weight:bold'>"+halaMessage;
	strHtml += "</font></td>";
		strHtml += "</tr>";	
	
	
	for(var i=0;i< count +1;i++) {
		if(count == i && mMod == 0) break;
		strHtml += "<tr>";
		strHtml += "<td>&nbsp;</td>";
		strHtml += "</tr>";
		strHtml += "<tr>";
		strHtml += "<td  bgcolor='#53504A' style='width:251'>";
		strHtml += "<table border='0'>";
		strHtml += "<tr>";
		strHtml += "<td><img height='220' width='251'  src='"+selMeals[mealCount]['mealImagePath'] +"' alt='"+strNotMessage+"'/>";
		strHtml += "</td>";
		strHtml += "</tr>";
		strHtml += "</table>";
		strHtml += "</td>";
		if(count != i || (count == i && mMod != 1)) {
			strHtml += "<td width='15%'>&nbsp;</td>";
			strHtml += "<td  bgcolor='#53504A' style='width:251'>";
			strHtml += "<table border='0'>";
			strHtml += "<tr>";
			strHtml += "<td><img height='220' width='251' alt='"+strNotMessage+"' src='"+selMeals[mealCount +1]['mealImagePath'] +"'/>";
			strHtml += "</td>";
			strHtml += "</tr>";
			strHtml += "</table>";
			strHtml += "</td>";
		}
		if(count != i) {
			strHtml += "<td width='15%'>&nbsp;</td>";
			strHtml += "<td  bgcolor='#53504A' style='width:251'>";
			strHtml += "<table border='0'>";
			strHtml += "<tr>";
			strHtml += "<td><img height='220' width='251' alt='"+strNotMessage+"' src='"+selMeals[mealCount +2]['mealImagePath'] +"'/>";
			strHtml += "</td>";
			strHtml += "</tr>";
			strHtml += "</table>";
			strHtml += "</td>";
		}
		
		
		strHtml += "</tr>";
		strHtml += "<tr>";
		strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount]['mealCharge']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount]['mealDescription'];
		strHtml += "</font></td>";
		if(count != i || (count == i && mMod != 1)) {
			strHtml += "<td>&nbsp;</td>";
			strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount +1]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount + 1]['mealCharge']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount + 1]['mealDescription'];
			strHtml += "</font></td>";
		}
		if(count != i) {
			strHtml += "<td>&nbsp;</td>";
			strHtml += "<td style='background-color:#53504A;padding:0.2em 2ex 0.2em 0ex;vertical-align:top'><font style='color:red;font-size:15;font-weight:bold;'>"+selMeals[mealCount +2]['mealName']+ "&nbsp;&nbsp;" +selMeals[mealCount + 2]['mealCharge']+ "&nbsp;" + strCurrency +"</font><br><font style='color:white;font-size:12;'>"+selMeals[mealCount + 2]['mealDescription'];
			strHtml += "</font></td>";
		}
		
		strHtml += "</tr>";
		mealCount = mealCount +3;
	}		
	strHtml += "</table>"
	document.getElementById("spnMeals").innerHTML=strHtml;
	</script>		
	</body>
</html>