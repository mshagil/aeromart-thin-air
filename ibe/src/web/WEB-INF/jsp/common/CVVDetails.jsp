<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
<%@ include file='../common/pageHD.jsp'%>
  <head>
    <title><fmt:message key="msg.Page.Title"/></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='-1'>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel='shortcut icon' href='../images/AA.ico'> 
    <link rel='stylesheet' type='text/css' href='../css/<fmt:message key="msg.pg.css.name"/>'>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/suppressMouse.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
  </head>
    <body class="pageBody" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' scroll="no" oncontextmenu="return showContextMenu()"  ondrag='return false'  onUnload="childWindowClosed()">
		<table width="500" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td valign="top">
				<%@ include file="../common/includeFrameTop.jsp" %>
					<table width='100%' border='0' cellpadding='2' cellspacing='0'>
				  		<tr>
				  			<td align='center'>
				  				<font class="fntLarge fntBold <fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.cvv.HD"/></font>
				  			</td>
				  		</tr>
						<tr>
				  			<td>
				  				<font class='fntCVV'>
									<p align="justify"><fmt:message key="msg.cvv.desc1"/></p>
									<br><br>
									<b><fmt:message key="msg.cvv.desc2"/></b>
									<br><br>
									<b><fmt:message key="msg.cvv.cards.master"/></b>
									<br><br>
									<p align="justify"><fmt:message key="msg.cvv.master"/></p>
									<br><br>
									<center><img src="../images/AA141_no_cache.gif"></center>
									<br><br>
									<b><fmt:message key="msg.cvv.cards.amex"/></b>
									<br><br>
									<p align="justify"><fmt:message key="msg.cvv.amex"/></p>
									<center><img src="../images/AA142_no_cache.gif"></center>
								</font>				
				  			</td>
				  		</tr>
				  		<tr>
				  			<td align='center'>
				  			  	<br><br>
								<input type="button" id="btnClose" class="Button" value='<fmt:message key="msg.cvv.Close"/>' NAME="btnClose" onclick="javascript:window.close()">
				  			</td>
				  		</tr>
				  	</table>
				  <%@ include file="../common/includeFrameBottom.jsp" %>
			</td>
		</tr>
	</table>
</body>
<script type="text/javascript">
<!--
opener.top[0].blnChildOpen = true;
//-->
</script>
</html>
</html>