	<%@ include file="/WEB-INF/jsp/common/directives.jsp" %>
	<span id='spnPNRSearch' style='visibility:hidden'>
	<%@ include file="../common/includeFrameTop.jsp" %>
		<table width="100%" border="0" cellpadding="2" cellspacing="0">
			<tr>
				<td colspan="2">
					<font class="<fmt:message key="msg.pg.font.Page"/>"><b><fmt:message key="msg.res.PNR.HD"/></b></font>
				</td>
			</tr>
			<tr>
				<td><font class="<fmt:message key="msg.pg.font.Page"/>"><fmt:message key="msg.res.PNR.BkgRef"/></font>
				<td><input type="text" id="txtPNR" maxlength="12" style="width:60px"></td>
			</tr>
			<tr>
				<td colspan="2" align="<fmt:message key='msg.pg.align.right'/>">
					<input type="button" id="btnPNRSearch" class="Button" title='<fmt:message key="msg.res.PNR.tt.Search"/>' value='<fmt:message key="msg.res.PNR.Search"/>' NAME="btnPNRSearch" onclick="pnrLinkClick()">
				</td>
			</tr>
		</table>
	<%@ include file="../common/includeFrameBottom.jsp" %>
	</span>
	<script language="JavaScript" src="../js/common/includePNRSearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>		