package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxContactConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PaxValildationTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LoadPaxContactConfigAction extends BaseRequestAwareAction {
	private static final Log log = LogFactory.getLog(LoadPaxContactConfigAction.class);

	private String system;

	private String carriers;

	private String paxType;

	private Map<String, IBEContactConfigDTO> contactConfig = new HashMap<String, IBEContactConfigDTO>();

	private Map<String, List<String>> validationGroup = new HashMap<String, List<String>>();

	private Map<String, IBEPaxConfigDTO> paxConfigAD = new HashMap<String, IBEPaxConfigDTO>();

	private Map<String, IBEPaxConfigDTO> paxConfigCH = new HashMap<String, IBEPaxConfigDTO>();

	private Map<String, IBEPaxConfigDTO> paxConfigIN = new HashMap<String, IBEPaxConfigDTO>();

	private String origin;

	private String destination;

	private PaxValildationTO paxValidation;

	private boolean success;

	private boolean isRequestNICForReservationsHavingDomesticSegments;

	public String execute() {

		try {
			List<String> carrierList = createCarrierList(carriers);

			PaxContactConfigDTO paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig(system,
					AppIndicatorEnum.APP_IBE.toString(), carrierList, TrackInfoUtil.getBasicTrackInfo(request));

			PaxUtil.populateContactConfig(paxContactDTO.getContactConfigList(), contactConfig, validationGroup);
			PaxUtil.populatePaxCatConfig(paxContactDTO.getPaxConfigList(), paxType, paxConfigAD, paxConfigCH, paxConfigIN);

			if (AppSysParamsUtil.isCountryPaxConfigEnabled() && BeanUtils.nullHandler(origin).length() > 0
					&& BeanUtils.nullHandler(destination).length() > 0) {

				List<String> originDestinations = new ArrayList<String>();
				originDestinations.add(origin);
				originDestinations.add(destination);

				// loading country wise configs for above origin and destination
				Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs = ModuleServiceLocator.getAirproxyPassengerBD()
						.loadPaxCountryConfig(system, AppIndicatorEnum.APP_IBE.toString(), originDestinations, getTrackInfo());

				if (countryWiseConfigs != null && countryWiseConfigs.size() > 0) {
					Map<String, Boolean> countryPaxConfigAD = new HashMap<String, Boolean>();
					Map<String, Boolean> countryPaxConfigCH = new HashMap<String, Boolean>();
					Map<String, Boolean> countryPaxConfigIN = new HashMap<String, Boolean>();

					// generating pax category type configs according to the most restricted rule
					PaxUtil.populateCountryWisePaxConfigs(countryWiseConfigs, countryPaxConfigAD, countryPaxConfigCH,
							countryPaxConfigIN, paxType);
					// overriding the configs in action level in IBE since for each reservation this action is calling
					if (countryPaxConfigAD.size() > 0 && countryPaxConfigCH.size() > 0 && countryPaxConfigIN.size() > 0) {
						overrideWithCountryConfigs(countryPaxConfigAD, ReservationInternalConstants.PassengerType.ADULT);
						overrideWithCountryConfigs(countryPaxConfigCH, ReservationInternalConstants.PassengerType.CHILD);
						overrideWithCountryConfigs(countryPaxConfigIN, ReservationInternalConstants.PassengerType.INFANT);
					}
				}
			}
			this.isRequestNICForReservationsHavingDomesticSegments = AppSysParamsUtil
					.isRequestNationalIDForReservationsHavingDomesticSegments();
			success = true;
		} catch (ModuleException e) {
			success = false;
			log.error("ERROR in loading pax & contact config", e);
		}
		return StrutsConstants.Result.SUCCESS;
	}

	private List<String> createCarrierList(String carriers) {
		List<String> cList = new ArrayList<String>();

		String[] cArr = carriers.split(",");

		for (String c : cArr) {
			cList.add(c);
		}

		return cList;
	}

	public String loadPaxValidation() {

		List<String> carrierList = createCarrierList(carriers);
		try {
			PaxContactConfigDTO paxContactDTO = ModuleServiceLocator.getAirproxyPassengerBD().loadPaxContactConfig(system,
					AppIndicatorEnum.APP_IBE.toString(), carrierList, TrackInfoUtil.getBasicTrackInfo(request));

			this.paxValidation = paxContactDTO.getPaxValidation();
		} catch (ModuleException e) {
			success = false;
			log.error("ERROR in loading pax validation", e);
		}

		return StrutsConstants.Result.SUCCESS;
	}

	private void overrideWithCountryConfigs(Map<String, Boolean> countryPaxConfig, String paxCategoryType) {
		for (String fieldName : countryPaxConfig.keySet()) {
			if (fieldName.equals(BasePaxConfigDTO.FIELD_TRAVELWITH)
					&& (paxCategoryType.equals(ReservationInternalConstants.PassengerType.ADULT) || paxCategoryType
							.equals(ReservationInternalConstants.PassengerType.CHILD))) {
				// travel with field is not available with adults and children
				continue;
			} else if (fieldName.equals(BasePaxConfigDTO.FIELD_TITLE)
					&& paxCategoryType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
				// title is not available with infants
				continue;
			} else {
				boolean globalValue = false;
				if (paxCategoryType.equals(ReservationInternalConstants.PassengerType.ADULT)) {
					globalValue = paxConfigAD.get(fieldName).isIbeMandatory();
					paxConfigAD.get(fieldName).setIbeMandatory(globalValue || countryPaxConfig.get(fieldName));
				} else if (paxCategoryType.equals(ReservationInternalConstants.PassengerType.CHILD)) {
					globalValue = paxConfigCH.get(fieldName).isIbeMandatory();
					paxConfigCH.get(fieldName).setIbeMandatory(globalValue || countryPaxConfig.get(fieldName));
				} else if (paxCategoryType.equals(ReservationInternalConstants.PassengerType.INFANT)) {
					globalValue = paxConfigIN.get(fieldName).isIbeMandatory();
					paxConfigIN.get(fieldName).setIbeMandatory(globalValue || countryPaxConfig.get(fieldName));
				}
			}
		}
	}

	/**
	 * @return the system
	 */
	@JSON(serialize = false)
	public String getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * @return the carriers
	 */
	@JSON(serialize = false)
	public String getCarriers() {
		return carriers;
	}

	/**
	 * @param carriers
	 *            the carriers to set
	 */
	public void setCarriers(String carriers) {
		this.carriers = carriers;
	}

	/**
	 * @return the paxType
	 */
	@JSON(serialize = false)
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the paxConfigAD
	 */
	public Map<String, IBEPaxConfigDTO> getPaxConfigAD() {
		return paxConfigAD;
	}

	/**
	 * @return the contactConfig
	 */
	public Map<String, IBEContactConfigDTO> getContactConfig() {
		return contactConfig;
	}

	/**
	 * @param contactConfig
	 *            the contactConfig to set
	 */
	public void setContactConfig(Map<String, IBEContactConfigDTO> contactConfig) {
		this.contactConfig = contactConfig;
	}

	/**
	 * @return the validationGroup
	 */
	public Map<String, List<String>> getValidationGroup() {
		return validationGroup;
	}

	/**
	 * @param validationGroup
	 *            the validationGroup to set
	 */
	public void setValidationGroup(Map<String, List<String>> validationGroup) {
		this.validationGroup = validationGroup;
	}

	/**
	 * @param paxConfigAD
	 *            the paxConfigAD to set
	 */
	public void setPaxConfigAD(Map<String, IBEPaxConfigDTO> paxConfigAD) {
		this.paxConfigAD = paxConfigAD;
	}

	/**
	 * @return the paxConfigCH
	 */
	public Map<String, IBEPaxConfigDTO> getPaxConfigCH() {
		return paxConfigCH;
	}

	/**
	 * @param paxConfigCH
	 *            the paxConfigCH to set
	 */
	public void setPaxConfigCH(Map<String, IBEPaxConfigDTO> paxConfigCH) {
		this.paxConfigCH = paxConfigCH;
	}

	/**
	 * @return the paxConfigIN
	 */
	public Map<String, IBEPaxConfigDTO> getPaxConfigIN() {
		return paxConfigIN;
	}

	/**
	 * @param paxConfigIN
	 *            the paxConfigIN to set
	 */
	public void setPaxConfigIN(Map<String, IBEPaxConfigDTO> paxConfigIN) {
		this.paxConfigIN = paxConfigIN;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public PaxValildationTO getPaxValidation() {
		return paxValidation;
	}

	public void setPaxValidation(PaxValildationTO paxValidation) {
		this.paxValidation = paxValidation;
	}

	public boolean isRequestNICForReservationsHavingDomesticSegments() {
		return isRequestNICForReservationsHavingDomesticSegments;
	}

	public void setRequestNICForReservationsHavingDomesticSegments(boolean isRequestNICForReservationsHavingDomesticSegments) {
		this.isRequestNICForReservationsHavingDomesticSegments = isRequestNICForReservationsHavingDomesticSegments;
	}
}
