package com.isa.thinair.ibe.core.web.v2.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;

public class DateUtil {

	/**
	 * Convert Local date to a Zulu date
	 * 
	 * @param airportCode
	 * @param zuluDate
	 * @return
	 * @throws ModuleException
	 */
	public static Date getZuluDateTime(Date localDate, String airportCode) throws ModuleException {
		ZuluLocalTimeConversionHelper helper = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportBD());

		return helper.getZuluDateTime(airportCode, localDate);
	}

	public static Date getZuluTime() {
		Calendar c = Calendar.getInstance();
		TimeZone z = c.getTimeZone();
		c.add(Calendar.MILLISECOND, -(z.getRawOffset() + z.getDSTSavings()));
		return c.getTime();
	}
	
	public static String dateToStringFormat(Date dtDate) {
		String strDate = "";
		if (dtDate != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			strDate = simpleDateFormat.format(dtDate);
		}
		return strDate;
	}

}
