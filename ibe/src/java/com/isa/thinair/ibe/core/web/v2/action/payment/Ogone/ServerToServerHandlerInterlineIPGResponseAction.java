package com.isa.thinair.ibe.core.web.v2.action.payment.Ogone;

import java.util.Calendar;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * 
 * @author Pradeep Karunanayake
 *
 */
/**
 * This is specific servlet related to Ogone payment gateway
 * The action will handle ogone post payment response
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
public class ServerToServerHandlerInterlineIPGResponseAction extends  BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(ServerToServerHandlerInterlineIPGResponseAction.class);
	
	public String execute() {
		
		if (log.isDebugEnabled())
			log.debug("### Start.." + request.getRequestedSessionId());
		
		Map<String, String> receiptyMap = RequestParameterUtil.getReceiptMap(request);
		log.info(receiptyMap);
		
		if (AppSysParamsUtil.enableServerToServerMessages()) {
			String trakingData = request.getParameter("COMPLUS"); /* trakingData = paymentBrokerRef_IPGID_currency */
			String orderID = request.getParameter("orderID");
			String status =  request.getParameter("STATUS"); /* We are processing only payment success response from the gateway. No need more load on back end */
			
			try {
				if (acceptStatus(status) && !StringUtil.isNullOrEmpty(trakingData)) {
					String[] trakingDataArr = trakingData.split("_");
					if (trakingDataArr.length == 3) {
						IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(new 
																					Integer(trakingDataArr[1]), trakingDataArr[2]);
						IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
						int paymentBrokerRefNo = Integer.parseInt(trakingDataArr[0]);
						int tempPayId = Integer.parseInt(getTempID(orderID));					
						
						ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
						ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
						ipgResponseDTO.setTemporyPaymentId(tempPayId);		
						ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);
						
					} else {
						// No need to throw error
					}
				}
			} catch(Exception ex) {
				log.error("ServerToServerHandlerInterlineIPGResponseAction==>", ex);
			}
		}
		
		return null;
	}	
	
	private boolean acceptStatus(String statusStr) {
		boolean status = false;
		int statusVal = Integer.parseInt(statusStr.trim());
		if (statusVal == 9 || statusVal == 5) {
			status = true;
		}
		return status;
	}
	
	// Move to util
	private String getTempID(String trackID) {
		String tempPayID = "";
		if (trackID != null) {
			tempPayID = trackID.substring(1);
		}
		return tempPayID;
	}

}
