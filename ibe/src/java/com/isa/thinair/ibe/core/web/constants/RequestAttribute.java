package com.isa.thinair.ibe.core.web.constants;

public interface RequestAttribute {

	public static final String POST_INPUT_DATA_FORM_HTML = "postInputDataFormHtml";

	public static final String REQUEST_METHOD = "reqRequestMethod";

	public static final String PAY_GATEWAY_TYPE = "reqPayGatewayType";

	public static final String PAY_GATEWAY_SWITCH_TOEXTERNAL_URL = "reqSwitchToExternalURL";

	
}
