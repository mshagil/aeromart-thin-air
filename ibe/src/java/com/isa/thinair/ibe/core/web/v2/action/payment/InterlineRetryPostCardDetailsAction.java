package com.isa.thinair.ibe.core.web.v2.action.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.LoyaltyPaymentOption;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.ibe.api.dto.BalanceTo;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.PaymentGateWayInfoDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.RequestAttribute;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.SessionAttribute;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.AppParamUtil;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.util.ancilarary.SeatMapUtil;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.StringUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard.FieldName;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.Pay_AT_StoreResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.util.CreditCardUtil;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.payFort.PayFortPaymentUtils;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyAsTypes;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.opensymphony.xwork2.ActionChainResult;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Payment.POST_INPUT_DATA),
		@Result(name = StrutsConstants.Result.BYPASS, type = ActionChainResult.class, value = StrutsConstants.Action.HANDLE_INTERLINE_IPG_RESPONSE),
		@Result(name = StrutsConstants.Action.SAVE_INTERLINE_RESERVATION, type = ActionChainResult.class, value = StrutsConstants.Action.SAVE_INTERLINE_RESERVATION),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, value = StrutsConstants.Jsp.Common.SESSION_EXPIRED),
		@Result(name = StrutsConstants.Result.LANDING_3DSECURE, value = StrutsConstants.Jsp.Payment.LANDING3DSECUREPAGE),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR),
		@Result(name = StrutsConstants.Result.CARD_PAYMENT_ERROR, value = StrutsConstants.Jsp.Payment.CARD_PAYMENT_ERROR) })
public class InterlineRetryPostCardDetailsAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(InterlineRetryPostCardDetailsAction.class);

	private PaymentGateWayInfoDTO pgw = new PaymentGateWayInfoDTO();

	private CreditCardTO card;

	public String execute() throws ModuleException {

		if (log.isDebugEnabled())
			log.debug("Start Executing Payment Retry Action");
		/* Validate the payment flow */
		isValidPaymentFlowData();

		IBEReservationInfoDTO resInfo;
		IBEReservationPostPaymentDTO resPostPay;
		synchronized (request) {
			resInfo = SessionUtil.getIBEreservationInfo(request);
			resPostPay = SessionUtil.getIBEReservationPostPaymentDTO(request);
			removeCardPaymentDataInSession(resInfo, resPostPay);
		}

		if ((resInfo == null || resPostPay == null) || (!resInfo.isFromPaymentPage() && !resInfo.isNoPay())) {
			releaseBlockedSeats(request);
			SessionUtil.expireSession(request);
			return StrutsConstants.Result.SESSION_EXPIRED;
		} else {
			resInfo.setFromPaymentPage(false);
		}
		resInfo.setFromPostCardDetails(true);

		try {
			PaymentBrokerBD paymentBrokerBD = ModuleServiceLocator.getPaymentBrokerBD();
			String pnr = getPNR(resPostPay);
			BigDecimal totalExcludingExtChgs = calculateTotalExcludingExtChgs(resInfo, resPostPay, pnr);
			BigDecimal lmsPaymentInfo = CustomerUtil.getLMSPaymentAmount(resInfo);

			// If reservation is fully paid by LMS points, redirect to confirmation page
			if (isFullyPaidByLMSPoints(resInfo, resPostPay, totalExcludingExtChgs)) {
				return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
			}

			/* If payment is loyalty full payment, System will redirect to confirmation page */
			BigDecimal loyaltyCredit = getLoyaltyCredit(resInfo);
			/*
			 * If full loyalty payment
			 */
			if (makeLoyaltyPayment(resInfo, resPostPay, totalExcludingExtChgs, loyaltyCredit)) {
				return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
			}

			String baseCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
			PayCurrencyDTO payCurrencyDTO = createPayCurrencyDTO(baseCurrencyCode);

			IPGIdentificationParamsDTO ipgIdentificationParamsDTO = WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(
					new Integer(pgw.getPaymentGateway()), payCurrencyDTO.getPayCurrencyCode());

			Properties ipgProps = paymentBrokerBD.getProperties(ipgIdentificationParamsDTO);
			String brokerType = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_BROKER_TYPE);
			String requestMethod = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_REQUEST_METHOD);
			String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);
			boolean isSwitchToExternalURL = Boolean.parseBoolean(ipgProps
					.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_SWITCH_TO_EXTERNAL_URL));

			IPGRequestDTO ipgRequestDTO = createIPGRequestDTO(brokerType, ipgIdentificationParamsDTO);

			/* Make payment assembler */
			BigDecimal totalBaseCardPayAmount = AccelAeroCalculator.subtract(totalExcludingExtChgs, lmsPaymentInfo);
			totalBaseCardPayAmount = AccelAeroCalculator.subtract(totalBaseCardPayAmount, loyaltyCredit);
			LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
			lccClientPaymentAssembler.addExternalCharges(getAllExternalCharges(resPostPay));
			boolean isIntExtPaymentGateway = isInternalExternalPaymentGatway(brokerType);

			List<CardDetailConfigDTO> cardDetailConfigData = null;
			String txtCardNo = "";
			String txtName = "";
			String strExpiry = "";
			String txtSCode = "";

			if (isIntExtPaymentGateway) {
				// Get payment gateway configuration data, avoid getting for 'external'
				cardDetailConfigData = paymentBrokerBD.getPaymentGatewayCardConfigData(ipgIdentificationParamsDTO.getIpgId()
						.toString());
				Map<String, String> cardStoreDataMap = CreditCardUtil.getDBStroreData(cardDetailConfigData,
						ipgRequestDTO.getCardNo(), ipgRequestDTO.getHolderName(), ipgRequestDTO.getExpiryDate(),
						ipgRequestDTO.getSecureCode());
				txtCardNo = cardStoreDataMap.get(FieldName.CARDNUMBER.code());
				txtName = cardStoreDataMap.get(FieldName.CARDHOLDERNAME.code());
				strExpiry = cardStoreDataMap.get(FieldName.EXPIRYDATE.code());
				txtSCode = cardStoreDataMap.get(FieldName.CVV.code());
			}

			if (isIntExtPaymentGateway) {
				lccClientPaymentAssembler.addInternalCardPayment(Integer.parseInt(ipgRequestDTO.getCardType()), strExpiry,
						txtCardNo, txtName, null, txtSCode, totalBaseCardPayAmount, AppIndicatorEnum.APP_IBE,
						TnxModeEnum.SECURE_3D,
						ipgIdentificationParamsDTO, payCurrencyDTO, new Date(), ipgRequestDTO.getHolderName(), "", null, null,
						null);
			} else {
				lccClientPaymentAssembler.addExternalCardPayment(Integer.parseInt(ipgRequestDTO.getCardType()), "",
						totalBaseCardPayAmount, AppIndicatorEnum.APP_IBE, TnxModeEnum.SECURE_3D, null,
						ipgIdentificationParamsDTO, payCurrencyDTO, new Date(), null, null, false, null);
			}
			/* Make tempory payment entry */
			boolean isCsOcFlightAvailable = ReservationUtil.isCsOcFlightAvailable(resPostPay.getSelectedFlightSegments());
			String originatorCarrierCode = getOriginatorCarrierCode(resPostPay.getSelectedFlightSegments());
			TrackInfoDTO trackInfoDTO = getTrackInfo();
			trackInfoDTO.setCarrierCode(SessionUtil.getCarrier(request));
			trackInfoDTO.setOriginChannelId(TrackInfoUtil.getWebOriginSalesChannel(request));
			
			String marketingUserId = "";
			
			if (trackInfoDTO.getOriginChannelId().equals(SalesChannelsUtil.SALES_CHANNEL_WEB)) {
				marketingUserId = "WEB-USER";
			} else if (trackInfoDTO.getOriginChannelId().equals(SalesChannelsUtil.SALES_CHANNEL_WEB)) {
				marketingUserId = "IOS-USER";
			} else if (trackInfoDTO.getOriginChannelId().equals(SalesChannelsUtil.SALES_CHANNEL_WEB)) {
				marketingUserId = "ANDROID-USER";
			} 
			CommonReservationContactInfo reservationContactInfo = retrieveContactInfo(resPostPay.getContactInfo());
			Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap = ModuleServiceLocator.getAirproxyReservationBD()
					.makeTemporyPaymentEntry(pnr, reservationContactInfo, lccClientPaymentAssembler, true,
							resInfo.getSelectedSystem(), originatorCarrierCode, trackInfoDTO, isCsOcFlightAvailable);
			int temporyPayId = LCCClientApiUtils.getTmpPayIdFromTmpPayMap(mapTempPayMap);
			/* Get Payment Gateway PNR */
			pnr = getIPGPNR(pnr, mapTempPayMap);
			log.info("Temp Pay ID:" + temporyPayId + "Session:" + request.getRequestedSessionId());
			/* Update IPG Request data */
			ipgRequestDTO.setAmount(AccelAeroRounderPolicy.convertAndRound(lccClientPaymentAssembler.getTotalPayAmount(),
					payCurrencyDTO).toString());
			/* Update ipgRequestDTO */
			updateIPGRequestDTO(ipgRequestDTO, temporyPayId, pnr, resPostPay.getContactInfo());

			/* TODO:Handle Paypal */
			IPGRequestResultsDTO ipgRequestResultsDTO = null;
			ReservationUtil.addTravelData(ipgRequestDTO, resPostPay.getSelectedFlightSegments());

			// for qiwi payment gateway
			if(request.getParameter("qiwiMobileNumber") != null 
					&& !"".equals(request.getParameter("qiwiMobileNumber"))){
				ipgRequestDTO.setContactMobileNumber(request.getParameter("qiwiMobileNumber"));
			}
			// for payfort pay@store setting the e mail and mobile number
			if (pgw.getProviderName().equalsIgnoreCase(PayFortPaymentUtils.PAY_FORT_STORE)) {
				ipgRequestDTO.setContactMobileNumber(request.getParameter("hdnPayFortMobileNumber"));
				ipgRequestDTO.setContactEmail(request.getParameter("hdnPayFortEmail"));
			}
			
			ipgRequestDTO.setDefaultCarrierName(SystemUtil.getCarrierName(request));
			ipgRequestDTO.setTimeToSpare(resPostPay.getTimeToSpare());

			ipgRequestResultsDTO = paymentBrokerBD.getRequestData(ipgRequestDTO);

			QiwiPResponse invoiceCreationResponse = ipgRequestResultsDTO.getQiwiResponse();

			if (invoiceCreationResponse != null) {

				isSwitchToExternalURL = !invoiceCreationResponse.isOfflineMode();
				resPostPay.setTimeToSpare(invoiceCreationResponse.getTimeToSpare());
				resPostPay.setSwitchToExternalURL(isSwitchToExternalURL);
				resPostPay.setInvoiceStatus(invoiceCreationResponse.getStatus());
				resPostPay.setBillId(invoiceCreationResponse.getBillId());

				// Qiwi invoice has been creaded.
				if (invoiceCreationResponse != null && invoiceCreationResponse.getStatus().equals(QiwiPRequest.FAIL)) {

					resPostPay.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
							ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));
					resPostPay.setPaymentGateWay(pgw);
					resPostPay.setResponseReceived(false);
					resPostPay.setErrorCode(invoiceCreationResponse.getError());
					return StrutsConstants.Result.BYPASS;
				}

				// offline payment mode
				if (!isSwitchToExternalURL && invoiceCreationResponse.getStatus().equals(QiwiPRequest.WAITING)) {

					pgw.setSwitchToExternalURL(isSwitchToExternalURL);
					resPostPay.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
							ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));
					resPostPay.setPaymentGateWay(pgw);

					return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
				}

			}

			if (ipgRequestResultsDTO.isPayFort()) {

				if (ipgRequestResultsDTO.getPostDataMap().containsKey(Pay_AT_StoreResponseDTO.VOUCHER_ID)
						&& ipgRequestResultsDTO.getPostDataMap().containsKey(Pay_AT_StoreResponseDTO.REQUEST_ID)) {

					int releaseTimeInMinutes = pgw.getOnholdReleaseTime();
					
					Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
					long longZuluLocalTime = zuluLocalTime.getTime();
					Date afterAddingPGWonholdTime = new Date(longZuluLocalTime + (releaseTimeInMinutes * 60000));
					String versionNo = "1";
					String version = "";
					if(resPostPay.isGroupPnr()){
						LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, resPostPay.isGroupPnr(), false, "","", true);
						LCCClientReservation commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(
								pnrModesDTO, null, getTrackInfo());
						version = String.valueOf(commonReservation.getVersion());
					} else {
						Reservation res = ReservationModuleUtils.getReservationBD().getReservation(pnr, false);
						version = String.valueOf(res.getVersion());
					}
					
					if (version != null && !"".equals(version)) {
						versionNo = version;
					}
					if (releaseTimeInMinutes > 0) {
						trackInfoDTO.setMarketingUserId(marketingUserId);
						ModuleServiceLocator.getAirproxyReservationBD().extendOnHold(pnr, afterAddingPGWonholdTime, versionNo,
								resPostPay.isGroupPnr(), getClientInfoDTO(),trackInfoDTO);
					}

					resPostPay.setPayFortStatus(true);
					resPostPay.setPayFortStatusVoucher(true);
					request.setAttribute(Pay_AT_StoreResponseDTO.REQUEST_ID,
							ipgRequestResultsDTO.getPostDataMap().get(Pay_AT_StoreResponseDTO.REQUEST_ID));
					request.setAttribute(Pay_AT_StoreResponseDTO.VOUCHER_ID,
							ipgRequestResultsDTO.getPostDataMap().get(Pay_AT_StoreResponseDTO.VOUCHER_ID));
					request.setAttribute(Pay_AT_StoreResponseDTO.PAYFORT, StrutsConstants.Result.SUCCESS);
					request.setAttribute(Pay_AT_StoreResponseDTO.PAY_AT_STORE_VOUCHER, StrutsConstants.Result.SUCCESS);

					log.debug("[InterlinePostCardDetailAction::PayFort Offline]");
					pgw.setSwitchToExternalURL(isSwitchToExternalURL);
					return StrutsConstants.Action.SAVE_INTERLINE_RESERVATION;
				} else {
					log.debug("[InterlinePostCardDetailAction::PayFort Voucher Creation Failed]");
					return StrutsConstants.Result.CARD_PAYMENT_ERROR;
				}
			}

			resPostPay.setPnr(pnr);
			resPostPay.setIpgRefenceNo(ipgRequestResultsDTO.getAccelAeroTransactionRef());
			resPostPay.setTemporyPaymentMap(LCCClientApiUtils.updateLccTemporyPaymentReferences(temporyPayId,
					ipgRequestResultsDTO.getPaymentBrokerRefNo(), mapTempPayMap));
			resPostPay.setPaymentGateWay(pgw);
			resPostPay.setReservationCredit(resInfo.getReservationCredit());
			resPostPay.setNoPay(resInfo.isNoPay());
			resPostPay.setCreditCardFee(resInfo.getCreditCardFee());
			resPostPay.setLoyaltyPayOption(resInfo.getLoyaltyPayOption());
			resPostPay.setBalanceToPay(resInfo.hasBalanceToPay());
			resPostPay.setLoyaltyCredit(getLoyaltyCredit(resInfo));
			resPostPay.setResponseReceived(false);
			resPostPay.setCarrierWiseLoyaltyPaymentInfo(resInfo.getCarrierWiseLoyaltyPaymentInfo());

			request.setAttribute(RequestAttribute.REQUEST_METHOD, requestMethod);
			request.setAttribute(RequestAttribute.PAY_GATEWAY_TYPE, brokerType);
			request.setAttribute(RequestAttribute.POST_INPUT_DATA_FORM_HTML, ipgRequestResultsDTO.getRequestData());
			request.setAttribute(RequestAttribute.PAY_GATEWAY_SWITCH_TOEXTERNAL_URL, isSwitchToExternalURL);
			/* Handle Specific gateway forward/Redirection */
			/* Ogone Direct link */
			String forwardPage = handleResponseTypeXML(responseTypeXML, ipgRequestDTO, ipgRequestResultsDTO, resInfo, resPostPay);
			if (!"".equals(forwardPage)) {
				return forwardPage;
			}

			if (pgw.getProviderCode().equalsIgnoreCase("PAYFORT")) {

				int releaseTimeInMinutes = pgw.getOnholdReleaseTime();
				Date zuluLocalTime = CalendarUtil.getCurrentZuluDateTime();
				long longZuluLocalTime = zuluLocalTime.getTime();
				Date afterAddingPGWonholdTime = new Date(longZuluLocalTime + (releaseTimeInMinutes * 60000));
				String versionNo = "1";
				String version = "";
				if(resPostPay.isGroupPnr()){
					LCCClientPnrModesDTO pnrModesDTO = ReservationUtil.getPnrModesDTO(pnr, resPostPay.isGroupPnr(), false, "","", true);
					LCCClientReservation commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(
							pnrModesDTO, null, getTrackInfo());
					version = String.valueOf(commonReservation.getVersion());
				} else {
					Reservation res = ReservationModuleUtils.getReservationBD().getReservation(pnr, false);
					version = String.valueOf(res.getVersion());
				}
				if (version != null && !"".equals(version)) {
					versionNo = version;
				}
				if (releaseTimeInMinutes > 0) {
					trackInfoDTO.setMarketingUserId(marketingUserId);
					ModuleServiceLocator.getAirproxyReservationBD().extendOnHold(pnr, afterAddingPGWonholdTime, versionNo,
							resPostPay.isGroupPnr(), getClientInfoDTO(), trackInfoDTO);
				}
			}

		} catch (ModuleException ex) {
			CustomerUtil.revertLoyaltyRedemption(request);
			errorTrack(ex);
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
					I18NUtil.getMessage(ex.getExceptionCode(), SessionUtil.getLanguage(request)));
		} catch (Exception ex) {
			CustomerUtil.revertLoyaltyRedemption(request);
			errorTrack(ex);
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE,
					I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request)));
		}
		if (log.isDebugEnabled())
			log.debug("Exit the hadling retry payments");

		return StrutsConstants.Result.SUCCESS;
	}

	private void removeCardPaymentDataInSession(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO postPayDTO) {
		postPayDTO.setTemporyPaymentMap(null);
		postPayDTO.setPaymentGateWay(null);
		postPayDTO.setIpgRefenceNo(null);
		postPayDTO.setIpgResponseDTO(null);
	}

	private void errorTrack(Exception ex) {
		log.error("InterlineRetryPostCardDetailsAction==>", ex);
		releaseBlockedSeats(request);
		SessionUtil.resetSesionCardDataInError(request);
	}

	private void isValidPaymentFlowData() throws ModuleException {
		if (!RequestParameterUtil.isPaymentRetry(request)) {
			throw new ModuleException(ExceptionConstants.SERVER_OPERATION_FAIL);
		}
	}

	/**
	 * 
	 * @param resPostPay
	 * @return
	 */
	private String getPNR(IBEReservationPostPaymentDTO resPostPay) {
		String pnr = null;
		if (isMakePayment() || resPostPay.isModifySegment() || resPostPay.isAddModifyAncillary()
				|| resPostPay.isAddGroundSegment()) {
			pnr = resPostPay.getPnr();
		}
		return pnr;
	}

	/**
	 * 
	 * @param pnr
	 * @param mapTempPayMap
	 * @return
	 * @throws ModuleException
	 */
	private String getIPGPNR(String pnr, Map<Integer, CommonCreditCardPaymentInfo> mapTempPayMap) throws ModuleException {
		String ipgPNR = null;
		if (pnr == null || pnr.isEmpty()) {
			ipgPNR = LCCClientApiUtils.getPNRFromLccTmpPayMap(mapTempPayMap);
		} else {
			ipgPNR = pnr;
		}
		return ipgPNR;
	}

	private String handleResponseTypeXML(String responseTypeXML, IPGRequestDTO ipgRequestDTO,
			IPGRequestResultsDTO ipgRequestResultsDTO, IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO resPostPay)
			throws ModuleException {
		/* If the PGW response is an XML, need to process the XML to handle the response(eg : OGONE PGW) */
		if (responseTypeXML != null && responseTypeXML.equals("true")) {
			if (log.isDebugEnabled())
				log.debug("Execute the payment and get the response for XML response type PGWs");
			XMLResponseDTO response = ModuleServiceLocator.getPaymentBrokerBD().getXMLResponse(
					ipgRequestDTO.getIpgIdentificationParamsDTO(), ipgRequestResultsDTO.getPostDataMap());
			if (log.isDebugEnabled())
				log.debug("Redirecting depending on the response");
			resPostPay.setSecurePayment3D(false);
			if (response != null && Integer.valueOf(response.getStatus()) == 46) {
				resInfo.setBank3DSecureHTML(response.getHtml());
				resPostPay.setSwitchToExternalURL(true);
				resPostPay.setSecurePayment3D(true);
				return StrutsConstants.Result.LANDING_3DSECURE;
			} else if (response != null && !response.getStatus().equals("")) {
				request.setAttribute(WebConstants.XML_RESPONSE_PARAMETERS, response);
				return StrutsConstants.Result.BYPASS;
			} else if (SessionUtil.getPutOnHoldBeforePaymentDTO(request).isPutOnHoldBeforePayment()) {
				return StrutsConstants.Result.CARD_PAYMENT_ERROR;
			} else {
				return StrutsConstants.Result.ERROR;
			}
		}

		return "";
	}

	// TODO:Move to utility
	private String getOriginatorCarrierCode(List<FlightSegmentTO> flightSegmentTOs) {
		String originatorCarrierCode = null;
		if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
			// sort by dept time zulu
			Collections.sort(flightSegmentTOs);
			FlightSegmentTO flightSegment = flightSegmentTOs.get(0);
			if (flightSegment != null && flightSegment.getFlightNumber() != null && flightSegment.getFlightNumber().length() > 2) {
				originatorCarrierCode = flightSegment.getFlightNumber().substring(0, 2);
			}
		}

		return originatorCarrierCode;
	}

	// Move to utility
	private CommonReservationContactInfo retrieveContactInfo(ContactInfoDTO contactInfo) {
		CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();

		lccClientReservationContactInfo.setTitle(contactInfo.getTitle());
		lccClientReservationContactInfo.setFirstName(contactInfo.getFirstName());
		lccClientReservationContactInfo.setLastName(contactInfo.getLastName());
		lccClientReservationContactInfo.setStreetAddress1(contactInfo.getAddresline());
		lccClientReservationContactInfo.setStreetAddress2(contactInfo.getAddresStreet());
		lccClientReservationContactInfo.setCity(contactInfo.getCity());
		lccClientReservationContactInfo.setCountryCode(contactInfo.getCountry());
		lccClientReservationContactInfo.setMobileNo(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-"
				+ contactInfo.getmNumber());
		lccClientReservationContactInfo.setPhoneNo(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-"
				+ contactInfo.getlNumber());
		lccClientReservationContactInfo.setFax("--");
		lccClientReservationContactInfo.setEmail(contactInfo.getEmailAddress());
		if (contactInfo.getNationality() != null && !contactInfo.getNationality().trim().equals("")) {
			lccClientReservationContactInfo.setNationalityCode(Integer.parseInt(contactInfo.getNationality()));
		}
		return lccClientReservationContactInfo;
	}

	private Collection<ExternalChgDTO> getAllExternalCharges(IBEReservationPostPaymentDTO resPostPay) {
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(resPostPay.getPaxList(),
				resPostPay.getSelectedExternalCharges(), true, true);
		Collection<ExternalChgDTO> externalCharges = externalChargesMediator.getAllExternalCharges();
		/*
		 * Add per pax flexi charges I think this can be done inside externalChargesMediator.getAllExternalCharges()
		 * instead having it here.
		 */
		/* TODO:Re-factor */
		for (ReservationPaxTO reservationPax : resPostPay.getPaxList()) {
			Collection<ExternalChgDTO> paxExternalCharges = reservationPax.getExternalCharges(EXTERNAL_CHARGES.FLEXI_CHARGES);
			if (paxExternalCharges != null) {
				if (externalCharges == null) {
					externalCharges = new LinkedList<ExternalChgDTO>();
				}
				externalCharges.addAll(paxExternalCharges);
			}
		}

		return externalCharges;
	}

	/**
	 * Check payment gateway type We have three types of payment gateways 1. External --> We are not capturing card
	 * details. All payment related process are doing in payment gateway side(with 3D secure) 2. Internal-external
	 * gateway --> We are capturing card details and forward to payment gateway. i) System will redirect for only 3D
	 * secure for Ogone IBE payments 3. Internal gateway - Process payment in our server or server top server
	 * communication
	 * 
	 * @param brokerType
	 * @return
	 */
	private boolean isInternalExternalPaymentGatway(String brokerType) {
		boolean isIntExtPaymentGateway = false;
		if (brokerType != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)) {
			isIntExtPaymentGateway = true;
		}
		return isIntExtPaymentGateway;
	}

	private String getCardType() {
		String selCardType = "5";
		if (card != null && card.getCardType() != null && !card.getCardType().trim().isEmpty()) {
			selCardType = card.getCardType();
		}
		return selCardType;
	}

	private IPGRequestDTO createIPGRequestDTO(String brokerType, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		String selCardType = getCardType();
		String txtCardNo = "";
		String txtName = "";
		String txtSCode = "";
		String strExpiry = "";

		if (isInternalExternalPaymentGatway(brokerType)) {
			selCardType = card.getCardType().trim();
			txtCardNo = card.getCardNo().trim();
			txtName = card.getCardHoldersName().trim();
			txtSCode = card.getCardCVV().trim();
			strExpiry = card.getExpiryDateYYMMFormat().trim();

			if (StringUtil.isEmpty(selCardType) || StringUtil.isEmpty(txtCardNo) || StringUtil.isEmpty(txtName)
					|| StringUtil.isEmpty(txtSCode) || StringUtil.isEmpty(strExpiry) || strExpiry.length() != 4) {
				throw new ModuleException("paymentbroker.card.invalid");
			}
		}

		IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
		ipgRequestDTO.setCardType(selCardType);
		ipgRequestDTO.setCardNo(txtCardNo);
		ipgRequestDTO.setSecureCode(txtSCode);
		ipgRequestDTO.setHolderName(txtName);
		ipgRequestDTO.setExpiryDate(strExpiry);
		ipgRequestDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);
		ipgRequestDTO.setPaymentMethod(card.getPaymentMethod());

		return ipgRequestDTO;
	}

	private void updateIPGRequestDTO(IPGRequestDTO ipgRequestDTO, int temporyPayId, String ipgPNR, ContactInfoDTO contactInfo)
			throws ModuleException {
		String carrierCode = SessionUtil.getCarrier(request);
		final String RETURN_URL = AppParamUtil.getSecureIBEUrl() + "handleInterlineIPGResponse.action";
		final String STATUS_URL = AppParamUtil.getSecureIBEUrl() + "handleInterlineIPGResponse!executeStatusRequest.action";
		ipgRequestDTO.setApplicationTransactionId(temporyPayId);

		ipgRequestDTO.setReturnUrl(RETURN_URL);
		ipgRequestDTO.setStatusUrl(STATUS_URL);

		ipgRequestDTO.setOfferUrl(IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode));
		ipgRequestDTO.setApplicationIndicator(AppIndicatorEnum.APP_IBE);
		ipgRequestDTO.setPnr(ipgPNR);
		ipgRequestDTO.setNoOfDecimals(2);

		ipgRequestDTO.setRequestedCarrierCode(carrierCode);
		ipgRequestDTO.setSessionID(request.getSession().getId());

		ipgRequestDTO.setEmail(StringUtils.trimToEmpty(contactInfo.getEmailAddress()));
		ipgRequestDTO.setContactEmail(StringUtils.trimToEmpty(contactInfo.getEmailAddress()));
		ipgRequestDTO.setContactFirstName(StringUtils.trimToEmpty(contactInfo.getFirstName()));
		ipgRequestDTO.setContactLastName(StringUtils.trimToEmpty(contactInfo.getLastName()));
		ipgRequestDTO.setContactAddressLine1(StringUtils.trimToEmpty(contactInfo.getAddresline()));
		ipgRequestDTO.setContactAddressLine2(StringUtils.trimToEmpty(contactInfo.getAddresStreet()));
		ipgRequestDTO.setContactCity(StringUtils.trimToEmpty(contactInfo.getCity()));
		ipgRequestDTO.setContactState(StringUtils.trimToEmpty(contactInfo.getState()));
		ipgRequestDTO.setContactCountryCode(StringUtils.trimToEmpty(contactInfo.getCountry()));
		ipgRequestDTO.setContactCountryName(StringUtils.trimToEmpty(contactInfo.getCountryName()));
		ipgRequestDTO.setContactMobileNumber(StringUtils.trimToEmpty(contactInfo.getmCountry() + "-" + contactInfo.getmArea()
				+ "-" + contactInfo.getmNumber()));
		ipgRequestDTO.setContactPhoneNumber(StringUtils.trimToEmpty(contactInfo.getlCountry() + "-" + contactInfo.getlArea()
				+ "-" + contactInfo.getlNumber()));
		ipgRequestDTO.setSessionLanguageCode(StringUtils.trimToEmpty(SessionUtil.getLanguage(request)));
		ipgRequestDTO.setUserIPAddress(getClientInfoDTO().getIpAddress());
		ipgRequestDTO.setUserAgent(getClientInfoDTO().getBrowser());
		if (AppSysParamsUtil.isTestStstem()) {
			String testSysOriginCountry = (String) request.getSession().getAttribute("originCountry");
			if (testSysOriginCountry == null || testSysOriginCountry.trim().length() == 0) {
				testSysOriginCountry = "OT"; // Other country
				//testSysOriginCountry = "AE"; // Other country
			}
			ipgRequestDTO.setIpCountryCode(testSysOriginCountry);
		} else {
			ipgRequestDTO.setIpCountryCode(ModuleServiceLocator.getCommoMasterBD().getCountryByIpAddress(
					getClientInfoDTO().getIpAddress()));
		}
		
		ipgRequestDTO.setPaymentGateWayName(pgw.getProviderName());
	}

	/**
	 * Create PayCurrencyDTO
	 * 
	 * @param baseCurrencyCode
	 * @return
	 * @throws ModuleException
	 */
	private PayCurrencyDTO createPayCurrencyDTO(String baseCurrencyCode) throws ModuleException {
		CommonMasterBD commonMasterBD = ModuleServiceLocator.getCommoMasterBD();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		Currency currency = null;
		PayCurrencyDTO payCurrencyDTO = null;
		String hdnSelCurrency = pgw.getPayCurrency();
		CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(hdnSelCurrency, ApplicationEngine.IBE);

		if (exchangeRate != null) {
			currency = commonMasterBD.getCurrency(hdnSelCurrency);
			if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultIbePGId() != null) {
				baseCurrencyCode = hdnSelCurrency;
				payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, exchangeRate.getMultiplyingExchangeRate(),
						currency.getBoundryValue(), currency.getBreakPoint());
			}
		}

		if (payCurrencyDTO == null) {
			if (log.isDebugEnabled())
				log.debug("Interline paymentgateway checking for selected currency does not have pg getting the default ");

			currency = commonMasterBD.getCurrency(baseCurrencyCode);
			exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(baseCurrencyCode, ApplicationEngine.IBE);
			payCurrencyDTO = new PayCurrencyDTO(baseCurrencyCode, new BigDecimal(1), currency.getBoundryValue(),
					currency.getBreakPoint());
		}
		pgw.setPayCurrency(baseCurrencyCode);
		return payCurrencyDTO;
	}

	private BigDecimal calculateTotalExcludingExtChgs(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO resPostPay,
			String pnr) throws ModuleException {
		BigDecimal totalExcludingExtChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
		PriceInfoTO priceInfoTO = resPostPay.getPriceInfoTO();
		if (resInfo.getTotalPriceWithoutExtChg() != null && priceInfoTO != null) {
			String totalExcludingExtChgsStr = BeanUtil.getTotalPriceWithoutExtCharges(priceInfoTO);
			totalExcludingExtChgs = new BigDecimal(totalExcludingExtChgsStr);
			/* Check whether data has been changed */
			if (!resInfo.getTotalPriceWithoutExtChg().equals(totalExcludingExtChgsStr)) {
				throw new ModuleException("msg.multiple.invalid.flight");
			}

			BigDecimal promoDiscountTotal = resInfo.getDiscountAmount(true);
			// Add promo discount for post pay DTO. If not user can change the promo amount using multiple browser tabs
			resPostPay.setPromoDiscountTotal(promoDiscountTotal);
			totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, promoDiscountTotal.negate());
		}
		if (resPostPay.isModifySegment()) {
			if (resPostPay.getlCCClientReservationBalance().getVersion() != null) {
				resPostPay.setVersion(resPostPay.getlCCClientReservationBalance().getVersion());
			}
			BalanceTo balanceTo = BalanceSummaryUtil
					.getTotalBalanceToPayAmount(resPostPay.getlCCClientReservationBalance(), null, resInfo.isInfantPaymentSeparated());
			totalExcludingExtChgs = AccelAeroCalculator.subtract(
					balanceTo.getBalanceToPay(),
					BalanceSummaryUtil.getUtilizeCreditBalance(resPostPay.getlCCClientReservationBalance(),
							resPostPay.getPaxList(), resInfo.isInfantPaymentSeparated()));

			if (resPostPay.isRequoteFlightSearch()) {
				// deduct external charge amount from amount due
				List<EXTERNAL_CHARGES> skipCharges = new ArrayList<ReservationInternalConstants.EXTERNAL_CHARGES>();
				skipCharges.add(EXTERNAL_CHARGES.CREDIT_CARD);
				skipCharges.add(EXTERNAL_CHARGES.JN_OTHER);
				for (ReservationPaxTO reservationPaxTO : resPostPay.getPaxList()) {
					if (!PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType())) {
						totalExcludingExtChgs = AccelAeroCalculator.subtract(totalExcludingExtChgs,
								BookingUtil.getTotalExtCharge(reservationPaxTO.getExternalCharges(), skipCharges));
					}
				}
			}
		}

		if (resPostPay.isAddModifyAncillary() || resPostPay.isMakePayment()) {
			if (resPostPay.getReservationCredit() != null
					&& resPostPay.getReservationCredit().negate().compareTo(BigDecimal.ZERO) > 0) {
				totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, resPostPay.getReservationCredit());
			}
			Map<String, BigDecimal> paxCreditMap = resInfo.getPaxCreditMap(pnr);
			BigDecimal reservationBalance = (BigDecimal) request.getSession().getAttribute(SessionAttribute.RESERVATION_BALANCE);
			if (reservationBalance == null) {
				reservationBalance = BigDecimal.ZERO;
			}

			BigDecimal removeAnciTotal = BigDecimal.ZERO;
			for (ReservationPaxTO pax : resPostPay.getPaxList()) {
				BigDecimal paxCredit = paxCreditMap.get(pax.getTravelerRefNumber());
				if (paxCredit != null) {
					if (paxCredit.compareTo(BigDecimal.ZERO) > 0) {
						reservationBalance = AccelAeroCalculator.add(reservationBalance, paxCredit);
					}
				}
				removeAnciTotal = AccelAeroCalculator.add(removeAnciTotal, pax.getToRemoveAncillaryTotal());
			}
			if (reservationBalance.compareTo(BigDecimal.ZERO) > 0) {
				totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, reservationBalance);
			}

			if (removeAnciTotal.compareTo(BigDecimal.ZERO) > 0) {
				totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, removeAnciTotal.negate());
			}
		}

		return totalExcludingExtChgs;
	}

	private boolean isMakePayment() {
		return SessionUtil.isMakePayment(request);
	}

	private boolean isFullyPaidByLMSPoints(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO resPostPay,
			BigDecimal totalExcludingExtChgs) throws ModuleException {
		if (AppSysParamsUtil.isLMSEnabled()) {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			BigDecimal reservationBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

			boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
					.getDiscountInfo().getDiscountAs()) : true;

			boolean promoExists = (resInfo.getDiscountInfo() != null) && (resInfo.getDiscountInfo().getPromotionId() != null);
			BigDecimal promoDiscountTotal = resInfo.getDiscountAmount(true);
			totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, promoDiscountTotal);
			FareQuoteTO fareQuote = ReservationUtil.fillFareQuote(resPostPay.getPaxList(), null, totalExcludingExtChgs,
					resInfo.getCreditCardFee(), resInfo.getTotalFare(), resInfo.getTotalTax(), resInfo.getTotalTaxSurchages(),
					LoyaltyPaymentOption.NONE, BigDecimal.ZERO, false, null, null, false, reservationBalance, false,
					exchangeRateProxy, promoExists, resInfo.getDiscountAmount(false), creditDiscount,
					resInfo.getIbeReturnFareDiscountAmount(), resPostPay.isRequoteFlightSearch(),
					CustomerUtil.getLMSPaymentAmount(resInfo), null, resInfo.isInfantPaymentSeparated());

			BigDecimal totalPayable = AccelAeroCalculator.getTwoScaledBigDecimalFromString(fareQuote.getTotalPayable());
			if (AccelAeroCalculator.isEqual(totalPayable, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				return true;
			}
		}

		return false;
	}

	private boolean makeLoyaltyPayment(IBEReservationInfoDTO resInfo, IBEReservationPostPaymentDTO resPostPay,
			BigDecimal totalExcludingExtChgs, BigDecimal loyaltyCredit) throws ModuleException {
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		resPostPay.setLoyaltyPayOption(resInfo.getLoyaltyPayOption());
		resPostPay.setLoyaltyCredit(loyaltyCredit);
		resPostPay.setBalanceToPay(resInfo.hasBalanceToPay());
		boolean isLoyaltyFullPayment = false;

		if (AppSysParamsUtil.isLoyalityEnabled()) {
			LoyaltyPaymentOption loyaltyPayOpt = resInfo.getLoyaltyPayOption();
			String loyaltyAcc = SessionUtil.getLoyaltyAccountNo(request);

			if (log.isDebugEnabled())
				log.debug("### Loyalty Option : " + resInfo.getLoyaltyPayOption() + " [" + loyaltyCredit + "]");

			if (loyaltyAcc != null) {
				BigDecimal totCredit = ModuleServiceLocator.getLoyaltyCreditBD().getTotalNonExpireCredit(loyaltyAcc);

				if (loyaltyPayOpt == LoyaltyPaymentOption.PART) {
					if (loyaltyCredit.compareTo(totCredit) > 0) {
						throw new ModuleException("errors.loyalty.invalid.credit");
					}
				}
				if (loyaltyPayOpt == LoyaltyPaymentOption.TOTAL) {
					resInfo.removeExternalCharge(EXTERNAL_CHARGES.CREDIT_CARD);
					resInfo.removeExternalCharge(EXTERNAL_CHARGES.JN_OTHER);

					boolean creditDiscount = (resInfo.getDiscountInfo() != null) ? DiscountApplyAsTypes.CREDIT.equals(resInfo
							.getDiscountInfo().getDiscountAs()) : true;

					boolean promoExists = (resInfo.getDiscountInfo() != null)
							&& (resInfo.getDiscountInfo().getPromotionId() != null);
					BigDecimal promoDiscountTotal = resInfo.getDiscountAmount(true);
					totalExcludingExtChgs = AccelAeroCalculator.add(totalExcludingExtChgs, promoDiscountTotal);

					BigDecimal voucherRedeemdAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

					if (resInfo.getPayByVoucherInfo() != null) {
						voucherRedeemdAmount = resInfo.getPayByVoucherInfo().getRedeemedTotal();
					}

					FareQuoteTO fareQuote = ReservationUtil.fillFareQuote(resPostPay.getPaxList(), null, totalExcludingExtChgs,
							resInfo.getCreditCardFee(), resInfo.getTotalFare(), resInfo.getTotalTax(),
							resInfo.getTotalTaxSurchages(), loyaltyPayOpt, loyaltyCredit, false, null, null, false,
							BigDecimal.ZERO, false, exchangeRateProxy, promoExists, resInfo.getDiscountAmount(false),
							creditDiscount, resInfo.getIbeReturnFareDiscountAmount(), resPostPay.isRequoteFlightSearch(),
							CustomerUtil.getLMSPaymentAmount(resInfo), voucherRedeemdAmount, resInfo.isInfantPaymentSeparated());
					BigDecimal totalPrice = new BigDecimal(fareQuote.getTotalPrice());
					if (totalPrice.compareTo(totCredit) > 0) {
						throw new ModuleException("errors.loyalty.invalid.credit");
					}
					resInfo.setLoyaltyCredit(totalPrice);
					resPostPay.setLoyaltyCredit(totalPrice);
					isLoyaltyFullPayment = true;
				}
			}
		}

		return isLoyaltyFullPayment;
	}

	private BigDecimal getLoyaltyCredit(IBEReservationInfoDTO resInfo) {
		BigDecimal loyaltyCredit = resInfo.getLoyaltyCredit();
		if (loyaltyCredit == null) {
			loyaltyCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		}
		return loyaltyCredit;
	}

	private static void releaseBlockedSeats(HttpServletRequest request) {
		try {
			SeatMapUtil.releaseBlockedSeats(request);
		} catch (Exception ex) {
			log.error("Seat release failed", ex);
		}
	}

	public PaymentGateWayInfoDTO getPgw() {
		return pgw;
	}

	public void setPgw(PaymentGateWayInfoDTO pgw) {
		this.pgw = pgw;
	}

	public CreditCardTO getCard() {
		return card;
	}

	public void setCard(CreditCardTO card) {
		this.card = card;
	}

}
