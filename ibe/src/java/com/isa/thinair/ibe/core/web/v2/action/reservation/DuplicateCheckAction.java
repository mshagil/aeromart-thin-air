package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;

/**
 * Action class for Duplicate name checks
 * 
 * @author Dilan Anuruddha
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class DuplicateCheckAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private boolean hasDuplicates = false;

	private String jsonPax;

	private boolean requoteFlightSearch;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();
			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					requoteFlightSearch);
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}
			List<LCCClientReservationPax> paxList = AncillaryJSONUtil.extractPaxOnly(jsonPax, ApplicationEngine.IBE);
			DuplicateValidatorAssembler dva = new DuplicateValidatorAssembler();
			dva.setTargetSystem(ibeResInfo.getSelectedSystem());
			dva.setFlightSegments(flightSegmentTOs);
			dva.setPaxList(paxList);
			this.hasDuplicates = ModuleServiceLocator.getAirproxyReservationBD().checkForDuplicates(dva , null , getTrackInfo());

		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("duplicate check ERROR", e);
		}
		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isHasDuplicates() {
		return hasDuplicates;
	}

	public void setJsonPax(String jsonPax) {
		this.jsonPax = jsonPax;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

}
