package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.ModifySegmentTO;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class showCustomerModifySegmentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(showCustomerModifySegmentAction.class);

	private boolean success = true;

	private String messageTxt;

	private Collection<FlightInfoTO> flightDepDetails;

	private Collection<FlightInfoTO> flightRetDetails;

	private String pnr;

	private String groupPNR;

	private String segmentRefNo;

	private ModifySegmentTO modifySegment = new ModifySegmentTO();

	@SuppressWarnings("unchecked")
	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			Customer cust = CustomerUtil.getCustomer(SessionUtil.getCustomerId(request));
			LCCClientReservation reservation = ReservationUtil.loadProxyReservation(pnr, (groupPNR != null && !groupPNR.trim()
					.isEmpty()) ? true : false, getTrackInfo(), false, (cust != null) ? true : false, null, null, false);
			Object[] flightInfo = ReservationUtil.loadFlightSegments(reservation, false, null);
			if (flightInfo != null) {
				if (flightInfo[0] != null) {
					flightDepDetails = (Collection<FlightInfoTO>) flightInfo[0];
				}

				if (flightInfo[1] != null) {
					flightRetDetails = (Collection<FlightInfoTO>) flightInfo[1];
				}
			}
			Collection<LCCClientReservationSegment> colModSegs = reservation.getSegments();
			String depSegment = null;
			String arrSegment = null;
			Date departureDate = null;
			Date arrivalDate = null;
			String strCabinclass = null;
			for (LCCClientReservationSegment seg : colModSegs) {
				if (departureDate == null || departureDate.after(seg.getDepartureDate())) {
					departureDate = seg.getDepartureDate();
					depSegment = seg.getSegmentCode();
					strCabinclass = seg.getCabinClassCode();
				}
				if (arrivalDate == null || arrivalDate.before(seg.getDepartureDate())) {
					arrivalDate = seg.getDepartureDate();
					arrSegment = seg.getSegmentCode();
				}
			}
			String depAirport = depSegment.split("/")[0];
			String arrAirport = arrSegment.split("/")[1];
			String strDepdate = DateFormatUtils.format(departureDate, "dd/MM/yyyy");

			modifySegment.setDepartureDate(strDepdate);
			modifySegment.setFrom(depAirport);
			modifySegment.setTo(arrAirport);
			modifySegment.setModifyingSegments(request.getParameter("fltSegment"));
			modifySegment.setMode(request.getParameter("pgMode"));
			modifySegment.setPNR(request.getParameter("pnr"));
			modifySegment.setBookingclass(strCabinclass);
			modifySegment.setNoOfAdults(reservation.getTotalPaxAdultCount());
			modifySegment.setNoOfInfants(reservation.getTotalPaxInfantCount());
			modifySegment.setNoOfChildren(reservation.getTotalPaxChildCount());

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("showCustomerModifySegmentAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("showCustomerModifySegmentAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Collection<FlightInfoTO> getFlightDepDetails() {
		return flightDepDetails;
	}

	public String getPnr() {
		return pnr;
	}

	public String getSegmentRefNo() {
		return segmentRefNo;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setSegmentRefNo(String segmentRefNo) {
		this.segmentRefNo = segmentRefNo;
	}

	public ModifySegmentTO getModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(ModifySegmentTO modifySegment) {
		this.modifySegment = modifySegment;
	}

	public Collection<FlightInfoTO> getFlightRetDetails() {
		return flightRetDetails;
	}

	public void setFlightRetDetails(Collection<FlightInfoTO> flightRetDetails) {
		this.flightRetDetails = flightRetDetails;
	}

}
