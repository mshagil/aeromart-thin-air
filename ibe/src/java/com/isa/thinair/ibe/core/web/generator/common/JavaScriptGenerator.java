package com.isa.thinair.ibe.core.web.generator.common;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class JavaScriptGenerator {
	private static GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();

	public static String createClientErrorsArray(Properties moduleErrs, String strLanguage) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		Set keySet = moduleErrs.keySet();

		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			strbData.append("arrError['" + moduleErrs.getProperty(key) + "'] = '" + I18NUtil.getMessage(key, strLanguage)
					+ "';");
		}
		return strbData.toString();
	}

	public static String getCountryHtml() throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createCountryList());
		return strbData.toString();
	}

	public static String getTitleHtml() throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleArray());
		return strbData.toString();
	}

	public static String getTiteVisibilityHtml() throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleVisibleArray());
		return strbData.toString();
	}
	
	public static String getTitleForChildVisibilityHtml() throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleForChildVisibleArray());
		return strbData.toString();
	} 
	
	public static String getTitleForAdultVisibilityHtml() throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createPaxTitleForAdultVisibleArray());
		return strbData.toString();
	} 

	public static String nullConvertToString(Object obj) throws ModuleException {
		String strReturn = "";

		if (obj == null) {
			strReturn = "";
		} else {
			strReturn = obj.toString();
		}
		return strReturn;
	}

	public static String createIBEConfigJS() {
		StringBuffer sb = new StringBuffer();

		sb.append("ibeConfig[0]='");
		sb.append(System.getProperty(ReservationWebConstnts.SYS_PROP_DEBUG_FRONTEND));
		sb.append("';");

		sb.append("ibeConfig[1]='");
		sb.append(System.getProperty(ReservationWebConstnts.SYS_PROP_DEBUG_FRONTEND));
		sb.append("';");

		sb.append("ibeConfig[2]='");
		sb.append(AppSysParamsUtil.isCreditCardPaymentsEnabled());
		sb.append("';");

		sb.append("ibeConfig[3]='");
		sb.append(AppSysParamsUtil.isReservationOnholdEnabled());
		sb.append("';");

		sb.append("ibeConfig[4]='");
		sb.append(AppSysParamsUtil.isCOSSelectionEnabled());
		sb.append("';");

		sb.append("ibeConfig[5]='");
		sb.append(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER));
		sb.append("';");

		sb.append("ibeConfig[6]='");
		sb.append(AppSysParamsUtil.isShowAlertsInIBEEnabled());
		sb.append("';");

		sb.append("ibeConfig[7]='");
		sb.append("Y".equals(globalConfig.getBizParam(SystemParamKeys.SHOW_OPERATING_CARRIER_LEGEND)) ? "true" : "false");
		sb.append("';");

		return sb.toString();
	}

}
