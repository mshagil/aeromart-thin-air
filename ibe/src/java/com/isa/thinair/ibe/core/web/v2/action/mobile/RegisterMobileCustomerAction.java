package com.isa.thinair.ibe.core.web.v2.action.mobile;


import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LmsMemberServiceBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.CustomerDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;
import com.isa.thinair.webplatform.api.util.LmsCommonUtil;
import com.isa.thinair.wsclient.api.util.LMSConstants;

/**
 * @author Chethiya Palliyaguruge
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class RegisterMobileCustomerAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(RegisterMobileCustomerAction.class);

	private boolean success = true;

	private String messageTxt;

	private CustomerDTO customer;

	private LmsMemberDTO lmsDetails;

	private boolean autoLogin = false;

	private boolean lmsNameMatch = true;

	private boolean joinLMS;

	private boolean registeredCustomer = false;

	private boolean skipNameMatching;

	private boolean lmsAvailable = false;

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		if (checkRegCustomer()) {
			return forward;
		}

		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		try {
			Customer modelCustomer = CustomerUtilV2.getModelCustomer(customer);
			customerDelegate.registerCustomer(modelCustomer, SessionUtil.getCarrier(request), null, false);

			int customerId = customerDelegate.getCustomer(modelCustomer.getEmailId()).getCustomerId();

			if (AppSysParamsUtil.isLMSEnabled() && joinLMS) {
				if (joinLMS) {
					lmsDetails.setAppCode("APP_IBE");
					lmsDetails.setLanguage("en");
					LmsMember lmsMember = CustomerUtilV2.getModelLmsMember(lmsDetails, customerId);
					LmsMemberServiceBD lmsMemberDelegate = ModuleServiceLocator.getLmsMemberServiceBD();
					LoyaltyManagementBD loyaltyManagementBD = ModuleServiceLocator.getLoyaltyManagementBD();
					LmsCommonUtil.lmsEnroll(lmsMember, SessionUtil.getCarrier(request), loyaltyManagementBD, lmsMemberDelegate,
							new Locale(lmsDetails.getLanguage()), false);
					lmsDetails.setEmailStatus("N");
				}
			}

		} catch (ModuleException ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("RegisterMobileCustomerAction==>", ex);
		}

		return forward;

	}

	public String validateHeadReffered() throws ModuleException {

		if (AppSysParamsUtil.isLMSEnabled() && joinLMS) {

			if (lmsDetails.getEmailId() != null && !lmsDetails.getEmailId().equals("")) {

				Customer customerModel = ModuleServiceLocator.getCustomerBD().getCustomer(lmsDetails.getEmailId());

				if (customerModel != null) {
					setRegisteredCustomer(true);
				}

				if (lmsDetails.getFirstName() == null || "".equals(lmsDetails.getFirstName())) {
					lmsDetails.setFirstName(customerModel.getFirstName());
				}

				if (lmsDetails.getLastName() == null || "".equals(lmsDetails.getLastName())) {
					lmsDetails.setLastName(customerModel.getLastName());
				}

				if (!skipNameMatching) {
					validateLMSName();
				}

			}
		} else {

			Customer customer = ModuleServiceLocator.getCustomerBD().getCustomer(lmsDetails.getEmailId());

			if (customer != null) {
				setRegisteredCustomer(true);
				return StrutsConstants.Result.SUCCESS;
			}

		}

		return StrutsConstants.Result.SUCCESS;

	}

	private void validateLMSName() throws ModuleException {
		LoyaltyMemberCoreDTO existingMember = ModuleServiceLocator.getLoyaltyManagementBD().getLoyaltyMemberCoreDetails(
				lmsDetails.getEmailId());

		if (existingMember != null && existingMember.getReturnCode() == LMSConstants.WSReponse.RESPONSE_CODE_OK) {
			lmsAvailable = true;
			if ((!existingMember.getMemberFirstName().equalsIgnoreCase(lmsDetails.getFirstName()) || !existingMember
					.getMemberLastName().equalsIgnoreCase(lmsDetails.getLastName()))) {
				setLmsNameMatch(false);
			} else {
				setLmsNameMatch(true);
			}
		}

	}

	private boolean checkRegCustomer() {
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
			Customer customergot = customerDelegate.getCustomer(customer.getEmailId());

			if (customergot != null && customergot.getCustomerId() != 0) {
				registeredCustomer = true;
			} else {
				registeredCustomer = false;
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("RegisterMobileCustomerAction==>", ex);
		}
		return registeredCustomer;
	}

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public boolean isAutoLogin() {
		return autoLogin;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

	public boolean getJoinLMS() {
		return joinLMS;
	}

	public void setJoinLMS(boolean joinLMS) {
		this.joinLMS = joinLMS;
	}

	public boolean isRegisteredCustomer() {
		return registeredCustomer;
	}

	public void setRegisteredCustomer(boolean registeredCustomer) {
		this.registeredCustomer = registeredCustomer;
	}

	public boolean isLmsAvailable() {
		return lmsAvailable;
	}

	public void setLmsAvailable(boolean lmsAvailable) {
		this.lmsAvailable = lmsAvailable;
	}

	public boolean isLmsNameMatch() {
		return lmsNameMatch;
	}

	public void setLmsNameMatch(boolean lmsNameMatch) {
		this.lmsNameMatch = lmsNameMatch;
	}

}
