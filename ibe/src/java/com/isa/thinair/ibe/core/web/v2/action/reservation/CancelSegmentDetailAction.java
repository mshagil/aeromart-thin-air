package com.isa.thinair.ibe.core.web.v2.action.reservation;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.FareTypeCodes;

/**
 * @author CG
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "")

})
public class CancelSegmentDetailAction extends IBEBaseAction {

	private String pnrSegId;

	private String fareId;

	private boolean cancelSegmentWithoutRequote = false;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		if (AppSysParamsUtil.isSkipNonModifiedSegFareONDs()) {
			try {
				cancelSegmentWithoutRequote = FareTypeCodes.SEGMENT.equalsIgnoreCase(SelectListGenerator.getFareType(pnrSegId,
						fareId));
			} catch (ModuleException e) {
				// Continue and return BYPASS
			}
		}
		return result;
	}

	public String getPnrSegId() {
		return pnrSegId;
	}

	public void setPnrSegId(String pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	public String getFareId() {
		return fareId;
	}

	public void setFareId(String fareId) {
		this.fareId = fareId;
	}

	public boolean isCancelSegmentWithoutRequote() {
		return cancelSegmentWithoutRequote;
	}

	public void setCancelSegmentWithoutRequote(boolean cancelSegmentWithoutRequote) {
		this.cancelSegmentWithoutRequote = cancelSegmentWithoutRequote;
	}

}
