package com.isa.thinair.ibe.core.web.util.ancilarary;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.parser.ParseException;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

public class SeatMapUtil {
	public static boolean releaseBlockedSeats(HttpServletRequest request) throws ModuleException, ParseException {
		boolean success = false;
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		if (resInfo == null) {
			return success;
		}
		String blockedSeats = resInfo.getBlockedSeatJson();

		if (blockedSeats != null) {
			List<LCCSegmentSeatDTO> blockedSeatDTOs = AncillaryJSONUtil.getBlockSeatDTOs(blockedSeats);
			success = ModuleServiceLocator.getAirproxyAncillaryBD().releaseSeats(blockedSeatDTOs, resInfo.getTransactionId(),
					resInfo.getSelectedSystem(), TrackInfoUtil.getBasicTrackInfo(request));
			if (success) {
				resInfo.setBlockedSeatJson(null);
			}
		}
		return success;
	}
}
