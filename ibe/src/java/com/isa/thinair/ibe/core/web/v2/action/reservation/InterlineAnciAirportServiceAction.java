package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.util.SSRUtils.SSRServicesUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciAirportServiceAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciAirportServiceAction.class);

	private boolean success = true;

	private String messageTxt;

	private String resPaxInfo;

	private String oldAllSegments;

	private String modifySegmentRefNos;

	private List<LCCFlightSegmentAirportServiceDTO> airportServicesList;

	private List<FlightSegmentTO> flightSegmentTOs;

	private String jsonOnds;

	private String pnr;

	private boolean modifySegment;

	private boolean modifyAncillary;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;

		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}

			SYSTEM system = ibeResInfo.getSelectedSystem();

			String selectedLanguage = SessionUtil.getLanguage(request);

			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					(modifySegment && AppSysParamsUtil.isRequoteEnabled()));

			List<FlightSegmentTO> selFlightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

			List<FlightSegmentTO> flightSegmentTOs = selFlightSegmentTOs;
			flightSegmentTOs = mergeFlightSegmentList(selFlightSegmentTOs);

			SegmentUtil.setFltSegSequence(flightSegmentTOs, null);

			if (ibeResInfo.getPriceInfoTO() != null) {
				SelectedFltSegBuilder totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(ibeResInfo.getPriceInfoTO(),
						flightSegmentTOs, totalFltSegBuilder.isReturn(), totalFltSegBuilder.getSelectedFlightSegments());
			}

			WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);

			Map<String, Integer> serviceCount = null;
			if (resPaxInfo != null && !"".equals(resPaxInfo)) {
				Collection<LCCClientReservationPax> colpaxs = ReservationUtil.transformJsonPassengers(resPaxInfo);
				serviceCount = SSRServicesUtil.extractAiportServiceCountFromJsonObj(colpaxs);
			} else {
				serviceCount = new HashMap<String, Integer>();
			}

			List<BundledFareDTO> bundledFareDTOs = ibeResInfo.getSelectedBundledFares();

			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability = ModuleServiceLocator
					.getAirproxyAncillaryBD().getAvailableAiportServices(flightSegmentTOs, AppIndicatorEnum.APP_IBE,
							SSRCategory.ID_AIRPORT_SERVICE.intValue(), system, selectedLanguage, strTxnIdntifier, getTrackInfo(),
							serviceCount, false, bundledFareDTOs, pnr);

			List<LCCFlightSegmentAirportServiceDTO> apsList = populateLCCAiportServicesList(aiportWiseAvailability);

			filterServicesForSelectedSegments(apsList, selFlightSegmentTOs);
			populateResponseFlightList();
		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("Airport Service REQ ERROR", e);
		}

		return result;
	}

	private void filterServicesForSelectedSegments(List<LCCFlightSegmentAirportServiceDTO> apsList,
			List<FlightSegmentTO> selflightSegmentTOs) {
		airportServicesList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();

		if (apsList != null) {
			for (LCCFlightSegmentAirportServiceDTO lccFlightSegmentAirportServiceDTO : apsList) {
				for (FlightSegmentTO flightSegmentTO : selflightSegmentTOs) {
					String[] fltRefArr = flightSegmentTO.getFlightRefNumber().split("-");
					String[] apFltRefArr = lccFlightSegmentAirportServiceDTO.getFlightSegmentTO().getFlightRefNumber().split("-");
					if (fltRefArr != null && fltRefArr.length > 0 && apFltRefArr != null && apFltRefArr.length > 0
							&& fltRefArr[0].equals(apFltRefArr[0])) {
						airportServicesList.add(lccFlightSegmentAirportServiceDTO);
					}
				}
			}
		}
	}

	private List<FlightSegmentTO> mergeFlightSegmentList(List<FlightSegmentTO> selectedFltSegList) throws Exception {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		flightSegmentTOs.addAll(selectedFltSegList);

		if (oldAllSegments != null && !"".equals(oldAllSegments)) {

			if (modifySegment && AppSysParamsUtil.isRequoteEnabled()) {
				SelectedFltSegBuilder totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);
				List<FlightSegmentTO> oldFlightSegmentTOs = totalFltSegBuilder.getSelectedFlightSegments();
				List<FlightSegmentTO> existingFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
				// remove oldflight details from selected flight details
				if (oldFlightSegmentTOs != null && oldFlightSegmentTOs.size() > 0) {
					for (FlightSegmentTO oldFlightSegmentTO : oldFlightSegmentTOs) {
						for (FlightSegmentTO selectedFlightSegmentTO : flightSegmentTOs) {
							if (oldFlightSegmentTO.getSegmentCode().equals(selectedFlightSegmentTO.getSegmentCode())
									&& oldFlightSegmentTO.getFlightRefNumber().equals(
											selectedFlightSegmentTO.getFlightRefNumber())
									&& ReservationUtil.isSameFlightMofication(getSearchParams(),
											oldFlightSegmentTO.getFlightRefNumber())
									&& selectedFlightSegmentTO.getCabinClassCode().equals(oldFlightSegmentTO.getCabinClassCode())) {
								existingFlightSegmentTOs.add(selectedFlightSegmentTO);
								break;
							}
						}
					}
					flightSegmentTOs.removeAll(existingFlightSegmentTOs);
				}
			} else if (modifyAncillary) {
				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);

				for (FlightSegmentTO selectedFlightSegmentTO : flightSegmentTOs) {
					for (LCCClientReservationSegment lccClientReservationSegment : colsegs) {

						String trimmedFltSegRefNum = FlightRefNumberUtil.filterFlightRPH(
								selectedFlightSegmentTO.getFlightRefNumber(), "-");
						String trimmedLccFliSegRefNum = FlightRefNumberUtil.filterFlightRPH(
								lccClientReservationSegment.getFlightSegmentRefNumber(), "-");

						if (trimmedFltSegRefNum.equals(trimmedLccFliSegRefNum)) {
							selectedFlightSegmentTO.setOndSequence(lccClientReservationSegment.getJourneySequence() - 1);
							selectedFlightSegmentTO.setReturnFlag(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE
									.equals(lccClientReservationSegment.getReturnFlag()));
						}
					}
				}

			} else {

				Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);

				if (colsegs != null) {

					if (modifySegmentRefNos != null && !"".equals(modifySegmentRefNos)) {
						String[] arrModifingAllSegs = modifySegmentRefNos.split(":");
						Collection<LCCClientReservationSegment> removableCol = new ArrayList<LCCClientReservationSegment>();

						for (int i = 0; i < arrModifingAllSegs.length; i++) {
							for (LCCClientReservationSegment resSeg : colsegs) {
								if (resSeg.getBookingFlightSegmentRefNumber().equals(arrModifingAllSegs[i])) {
									removableCol.add(resSeg);
									break;
								}
							}
						}

						if (removableCol.size() > 0) {
							colsegs.removeAll(removableCol);
						}
					}

					flightSegmentTOs.addAll(ReservationBeanUtil.convertReservationSegmentsToFlightSegmentTOs(colsegs));
				}
			}

		}

		return flightSegmentTOs;
	}

	private List<LCCFlightSegmentAirportServiceDTO> populateLCCAiportServicesList(
			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability) {
		List<LCCFlightSegmentAirportServiceDTO> lccAvailabilityList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();

		for (Entry<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> entry : aiportWiseAvailability.entrySet()) {
			AirportServiceKeyTO keyTO = entry.getKey();

			LCCFlightSegmentAirportServiceDTO airportServiceDTO = entry.getValue();
			airportServiceDTO.setFlightSegmentTO(airportServiceDTO.getFlightSegmentTO().clone());
			airportServiceDTO.getFlightSegmentTO().setAirportCode(keyTO.getAirport());
			airportServiceDTO.getFlightSegmentTO().setAirportType(keyTO.getAirportType());
			lccAvailabilityList.add(airportServiceDTO);
		}

		SortUtil.sortAirportServicesByFltDeparture(lccAvailabilityList);

		return lccAvailabilityList;
	}

	private void populateResponseFlightList() {
		flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		for (LCCFlightSegmentAirportServiceDTO aps : airportServicesList) {
			flightSegmentTOs.add(aps.getFlightSegmentTO());
		}
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the resPaxInfo
	 */
	public String getResPaxInfo() {
		return resPaxInfo;
	}

	/**
	 * @param resPaxInfo
	 *            the resPaxInfo to set
	 */
	public void setResPaxInfo(String resPaxInfo) {
		this.resPaxInfo = resPaxInfo;
	}

	/**
	 * @return the airportServicesList
	 */
	public List<LCCFlightSegmentAirportServiceDTO> getAirportServicesList() {
		return airportServicesList;
	}

	/**
	 * @param airportServicesList
	 *            the airportServicesList to set
	 */
	public void setAirportServicesList(List<LCCFlightSegmentAirportServiceDTO> airportServicesList) {
		this.airportServicesList = airportServicesList;
	}

	/**
	 * @return the flightSegmentTOs
	 */
	public List<FlightSegmentTO> getFlightSegmentTOs() {
		return flightSegmentTOs;
	}

	/**
	 * @param flightSegmentTOs
	 *            the flightSegmentTOs to set
	 */
	public void setFlightSegmentTOs(List<FlightSegmentTO> flightSegmentTOs) {
		this.flightSegmentTOs = flightSegmentTOs;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}
}
