package com.isa.thinair.ibe.core.web.v2.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;

public class ErrorMessageUtil {

	public static Map<String, String> getFlightNFareSearchErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.resv.fare.Dept.Flight.null", "ERR010");
		moduleErrs.setProperty("errors.resv.fare.Dept.Date.null", "ERR011");
		moduleErrs.setProperty("errors.resv.fltSrch.FromTo.date.compare", "ERR004");
		moduleErrs.setProperty("errors.resv.fare.Retu.Flight.null", "ERR012");
		moduleErrs.setProperty("errors.resv.fare.Retu.Date.null", "ERR013");
		moduleErrs.setProperty("errors.resv.fare.Retu.Date.compare", "ERR014");
		moduleErrs.setProperty("errors.resv.flt.Dept.Date.null", "ERR015");
		moduleErrs.setProperty("errors.resv.flt.Retu.Date.null", "ERR016");
		moduleErrs.setProperty("errors.resv.flt.fare.null", "ERR039");
		moduleErrs.setProperty("errors.resv.fltSrch.select", "ERR040");
		moduleErrs.setProperty("errors.resv.fltSrch.terms", "ERR042");
		moduleErrs.setProperty("errors.resv.flt.from.date", "ERR043");
		moduleErrs.setProperty("errors.resv.flt.to.date", "ERR044");
		moduleErrs.setProperty("errors.resv.fltSrch.bin.invalid", "ERR096");
		moduleErrs.setProperty("errors.resv.lms.ffid", "ERR120");
		moduleErrs.setProperty("errors.resv.lms.ffid.unregistered", "ERR121");
		moduleErrs.setProperty("errors.resv.lms.ffid.not.matched", "ERR122");


		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getCustomerProfileErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("msg.form.first.name.required", "userFirstNameRqrd");
		moduleErrs.setProperty("msg.form.last.name.required", "userLastNameRqrd");
		moduleErrs.setProperty("msg.form.dateOfBirth.required", "userDateOfBirthRqrd");
		moduleErrs.setProperty("msg.form.dateOfBirth.format.incorrect", "userDateOfBirtIncorrect");
		moduleErrs.setProperty("msg.form.nationality.required", "userNationalityRqrd");
		moduleErrs.setProperty("msg.form.country.required", "userCountryRqrd");
		moduleErrs.setProperty("msg.form.address.required", "userAddressRqrd");
		moduleErrs.setProperty("msg.form.zip.code.required", "userZipCodeRqrd");
		moduleErrs.setProperty("msg.form.zip.code.incorrect", "userZipCodeIncorrect");
		moduleErrs.setProperty("msg.form.gender.required", "userGenderRqrd");
		moduleErrs.setProperty("msg.form.telephone.required", "userTelephoneRqrd");
		moduleErrs.setProperty("msg.form.mobile.required", "userMobileRqrd");
		moduleErrs.setProperty("msg.form.home.office.required", "userHomeOfficeRqrd");
		moduleErrs.setProperty("msg.form.fax.required", "userFaxRqrd");
		moduleErrs.setProperty("msg.form.telephone.incorrect", "userTelephoneIncorrect");
		moduleErrs.setProperty("msg.form.mobile.incorrect", "userMobileIncorrect");
		moduleErrs.setProperty("msg.form.office.tel.format.incorrect", "userOfficeTelIncorrect");
		moduleErrs.setProperty("msg.form.fax.format.incorrect", "userFaxIncorrect");
		moduleErrs.setProperty("msg.form.email.required", "userEmailRqrd");
		moduleErrs.setProperty("msg.form.email.incorrect", "userEmailIncorrect");
		moduleErrs.setProperty("msg.form.password.required", "userPasswordRqrd");
		moduleErrs.setProperty("msg.form.confirm.password.required", "userConfPasswordRqrd");

		moduleErrs.setProperty("msg.form.password.notmatch", "userPasswordsNotMatch");
		moduleErrs.setProperty("msg.form.secrete.qs.required", "userSecreteQsRqrd");
		moduleErrs.setProperty("msg.form.answer.required", "userAnswerRqrd");
		moduleErrs.setProperty("msg.form.alternate.email.required", "UserAltEmailRqrd");
		moduleErrs.setProperty("msg.form.alternate.format.incorrect", "userAltEmailIncorrect");

		moduleErrs.setProperty("msg.form.login.userId.required", "userIDRqrd");
		moduleErrs.setProperty("msg.form.dateOfBirth.wrong", "DOBWrong");
		moduleErrs.setProperty("msg.form.password.invalid", "passwordIncorrect");
		moduleErrs.setProperty("msg.form.telephone.area.required", "userTeleAreaphoneRqrd");
		moduleErrs.setProperty("msg.form.telephone.country.required", "userTeleCountryphoneRqrd");
		moduleErrs.setProperty("msg.form.mobile.area.required", "userMobileAreaphoneRqrd");
		moduleErrs.setProperty("msg.form.mobile.country.required", "userMobileCountryphoneRqrd");
		moduleErrs.setProperty("msg.form.home.office.area.required", "userHomeOfficeAreaphoneRqrd");
		moduleErrs.setProperty("msg.form.home.office.country.required", "userHomeOfficeCountryphoneRqrd");
		moduleErrs.setProperty("msg.form.fax.area.required", "userFaxAreaphoneRqrd");
		moduleErrs.setProperty("msg.form.fax.country.required", "userfaxCountryphoneRqrd");
		moduleErrs.setProperty("msg.form.password.max", "passwordMax");
		moduleErrs.setProperty("msg.form.password.invcmb", "invalidCombine");
		moduleErrs.setProperty("errors.resv.psng.Invalid", "errInvalidChar");
		moduleErrs.setProperty("msg.form.first.title.required", "titleRequired");
		moduleErrs.setProperty("msg.form.age.limit", "ageLimit");

		moduleErrs.setProperty("msg.form.first.name.alphanumaric", "userFirstNameAlpha");
		moduleErrs.setProperty("msg.form.last.name.alphanumaric", "userLastNameAlpha");

		moduleErrs.setProperty("msg.form.city.required", "userCityRqrd");
		moduleErrs.setProperty("msg.form.phone.wrongcomb", "wrongComb");
		moduleErrs.setProperty("msg.form.phone.lenless", "lenSmall");
		moduleErrs.setProperty("msg.form.phone.lengreat", "lenLong");

		moduleErrs.setProperty("msg.form.emgn.title.required", "userEmgnTitleRqrd");
		moduleErrs.setProperty("msg.form.emgn.first.name.required", "userEmgnFirstNameRqrd");
		moduleErrs.setProperty("msg.form.emgn.first.name.alphanumaric", "userEmgnFirstNameAlpha");
		moduleErrs.setProperty("msg.form.emgn.last.name.required", "userEmgnLastNameRqrd");
		moduleErrs.setProperty("msg.form.emgn.last.name.alphanumaric", "userEmgnLastNameAlpha");
		moduleErrs.setProperty("msg.form.emgn.phone.country.required", "userEmgnCountryPhoneRqrd");
		moduleErrs.setProperty("msg.form.emgn.phone.area.required", "userEmgnAreaPhoneRqrd");
		moduleErrs.setProperty("msg.form.emgn.phone.required", "userEmgnPhoneRqrd");
		moduleErrs.setProperty("msg.form.emgn.email.required", "userEmgnEmailRqrd");
		moduleErrs.setProperty("msg.form.emgn.email.incorrect", "userEmgnEmailIncorrect");

		moduleErrs.setProperty("msg.form.lms.dob.required","userDateOfBirthRqrd");
		moduleErrs.setProperty("msg.form.lms.dob.incorrect", "userDateOfBirtIncorrect");
		moduleErrs.setProperty("msg.form.lms.hemail.required","headEmailReqrd");
		moduleErrs.setProperty("msg.form.lms.hemail.incorrect", "headEmailIncorrect");
		moduleErrs.setProperty("msg.form.lms.refmail.incorrect", "refEmailIncorrect");
		moduleErrs.setProperty("msg.form.lms.refmail.doesnt.exist", "refEmailDoesntExist");
		moduleErrs.setProperty("msg.form.lms.hemail.doesnt.exist", "headEmailDoesntExist");

		moduleErrs.setProperty("msg.form.loyalty.account", "userMashreqAccountNo");

		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getMobileCustomerProfileErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("msg.form.first.name.required", "userFirstNameRqrd");
		moduleErrs.setProperty("msg.form.last.name.required", "userLastNameRqrd");
		moduleErrs.setProperty("msg.form.dateOfBirth.required", "userDateOfBirthRqrd");
		moduleErrs.setProperty("msg.form.dateOfBirth.format.incorrect", "userDateOfBirtIncorrect");
		moduleErrs.setProperty("msg.form.nationality.required", "userNationalityRqrd");
		moduleErrs.setProperty("msg.form.country.required", "userCountryRqrd");
		moduleErrs.setProperty("msg.form.mobile.required", "userMobileRqrd");
		moduleErrs.setProperty("msg.form.mobile.incorrect", "userMobileIncorrect");
		moduleErrs.setProperty("msg.form.mobile.format.incorrect", "userMobileFormatIncorrect");
		moduleErrs.setProperty("msg.form.email.required", "userEmailRqrd");
		moduleErrs.setProperty("msg.form.email.incorrect", "userEmailIncorrect");
		moduleErrs.setProperty("msg.form.password.required", "userPasswordRqrd");
		moduleErrs.setProperty("msg.form.confirm.password.required", "userConfPasswordRqrd");

		moduleErrs.setProperty("msg.form.password.notmatch", "userPasswordsNotMatch");

		moduleErrs.setProperty("msg.form.dateOfBirth.wrong", "DOBWrong");
		moduleErrs.setProperty("msg.form.password.invalid", "passwordIncorrect");

		moduleErrs.setProperty("msg.form.password.max", "passwordMax");
		moduleErrs.setProperty("msg.form.password.invcmb", "invalidCombine");
		moduleErrs.setProperty("msg.form.first.title.required", "titleRequired");

		moduleErrs.setProperty("msg.form.first.name.alphanumaric", "userFirstNameAlpha");
		moduleErrs.setProperty("msg.form.last.name.alphanumaric", "userLastNameAlpha");

		moduleErrs.setProperty("msg.form.lms.dob.required", "userDateOfBirthRqrd");
		moduleErrs.setProperty("msg.form.lms.dob.incorrect", "userDateOfBirtIncorrect");

		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getAgentProfileErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("msg.form.agency.name.required", "userAgencyNameRqrd");
		moduleErrs.setProperty("msg.form.license.no.required", "userLicenceNoRqrd");
		moduleErrs.setProperty("msg.form.IATACode.required", "userIATACodeRqrd");
		moduleErrs.setProperty("msg.form.station.required", "userStationRqrd");
		moduleErrs.setProperty("msg.form.Cty.required", "userCityRqrd");
		moduleErrs.setProperty("msg.form.addr.required", "userAddrRqrd");
		moduleErrs.setProperty("msg.form.country.required", "userCountryRqrd");
		moduleErrs.setProperty("msg.form.telephone.required", "userTelephoneRqrd");
		moduleErrs.setProperty("msg.form.telephone.incorrect", "userTelephoneIncorrect");
		moduleErrs.setProperty("msg.form.telephone.area.required", "userTeleAreaphoneRqrd");
		moduleErrs.setProperty("msg.form.telephone.country.required", "userTeleCountryphoneRqrd");
		moduleErrs.setProperty("msg.form.mobile.incorrect", "userMobileIncorrect");
		moduleErrs.setProperty("msg.form.fax.format.incorrect", "userFaxIncorrect");
		moduleErrs.setProperty("msg.form.email.required", "userEmailRqrd");
		moduleErrs.setProperty("msg.form.email.incorrect", "userEmailIncorrect");
		moduleErrs.setProperty("msg.form.phone.wrongcomb", "wrongComb");
		moduleErrs.setProperty("msg.form.phone.lenless", "lenSmall");
		moduleErrs.setProperty("msg.form.phone.lengreat", "lenLong");
		moduleErrs.setProperty("msg.form.station.country.wrongcomb", "stationContryWrongComb");

		return createAgentErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static String getClientErrors(String language) throws ModuleException {

		if (language != null)
			language = language.toLowerCase();

		String strClientErrors = "";
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.resv.psng.Inft.Title.null", "ERR016");
		moduleErrs.setProperty("errors.resv.psng.Adlt.Title.null", "ERR017");
		moduleErrs.setProperty("errors.resv.psng.Adlt.FName.null", "ERR018");
		moduleErrs.setProperty("errors.resv.psng.Adlt.LName.null", "ERR019");
		moduleErrs.setProperty("errors.resv.psng.Inft.FName.null", "ERR020");
		moduleErrs.setProperty("errors.resv.psng.Inft.LName.null", "ERR021");
		moduleErrs.setProperty("errors.resv.psng.Inft.DOB.null", "ERR022");
		moduleErrs.setProperty("errors.resv.psng.child.DOB.null", "ERR0101");
		moduleErrs.setProperty("errors.resv.psng.Inft.DOB.compare", "ERR023");
		moduleErrs.setProperty("errors.resv.psng.Inft.TrvlWidth", "ERR024");
		moduleErrs.setProperty("errors.resv.psng.Inft.Age", "ERR025");
		moduleErrs.setProperty("errors.resv.psng.Inft.TrvlWidth.compare", "ERR026");
		moduleErrs.setProperty("errors.resv.psng.Invalid", "ERR027");
		moduleErrs.setProperty("errors.resv.psng.FName.null", "ERR028");
		moduleErrs.setProperty("errors.resv.psng.LName.null", "ERR029");
		moduleErrs.setProperty("errors.resv.psng.Mobile.null", "ERR030");
		moduleErrs.setProperty("errors.resv.psng.Email.null", "ERR031");
		moduleErrs.setProperty("errors.resv.psng.Email.Invalid", "ERR032");
		moduleErrs.setProperty("errors.resv.psng.Email.Check", "ERR033");
		moduleErrs.setProperty("errors.resv.psng.Phone.Country", "ERR034");
		moduleErrs.setProperty("errors.resv.psng.Phone.Area", "ERR035");
		moduleErrs.setProperty("errors.resv.psng.Mobile.Country", "ERR036");
		moduleErrs.setProperty("errors.resv.psng.Mobile.Area", "ERR037");
		moduleErrs.setProperty("errors.resv.psng.Inft.Child", "ERR038");
		moduleErrs.setProperty("errors.resv.psng.Invalid.date", "ERR039");
		moduleErrs.setProperty("errors.resv.psng.name.number", "ERR040");
		moduleErrs.setProperty("errors.resv.psng.Country", "ERR041");
		moduleErrs.setProperty("errors.resv.psng.Title", "ERR042");
		moduleErrs.setProperty("errors.resv.psng.Adlt.Type.null", "ERR043");
		moduleErrs.setProperty("errors.resv.psng.chld.Title.null", "ERR044");
		moduleErrs.setProperty("errors.resv.psng.chld.FName.null", "ERR045");
		moduleErrs.setProperty("errors.resv.psng.chld.LName.null", "ERR046");
		moduleErrs.setProperty("errors.resv.psng.Nationality", "ERR048");
		moduleErrs.setProperty("errors.resv.psng.City", "ERR049");
		moduleErrs.setProperty("errors.resv.psng.invalidComb", "ERR050");
		moduleErrs.setProperty("errors.resv.psng.lesminlength", "ERR051");
		moduleErrs.setProperty("errors.resv.psng.greaterMaxlen", "ERR052");
		moduleErrs.setProperty("errors.resv.psng.Adlt.Age", "ERR053");
		moduleErrs.setProperty("errors.resv.psng.duplucateFoids", "ERR054");
		moduleErrs.setProperty("errors.resv.psng.LName.lesminlength", "ERR055");
		moduleErrs.setProperty("msg.form.login.userId.required", "ERR056");
		moduleErrs.setProperty("msg.form.password.required", "ERR057");
		moduleErrs.setProperty("errors.sign.invalid", "ERR058");
		moduleErrs.setProperty("msg.login.user.name", "ERR059");
		moduleErrs.setProperty("msg.login.user.password", "ERR060");
		moduleErrs.setProperty("msg.user.pwd.forgot.Error", "ERR061");
		moduleErrs.setProperty("errors.resv.psng.Inft.OffLowAge", "ERR062");
		moduleErrs.setProperty("errors.resv.psng.zipCode", "ERR063");
		moduleErrs.setProperty("errors.resv.psng.zipCode.Invalid", "ERR064");
		moduleErrs.setProperty("errors.resv.psng.Address.null", "ERR065");
		moduleErrs.setProperty("errors.resv.psng.Fax.null", "ERR066");
		moduleErrs.setProperty("errors.resv.psng.Fax.Country", "ERR067");
		moduleErrs.setProperty("errors.resv.psng.Fax.Area", "ERR068");
		moduleErrs.setProperty("errors.resv.psng.Adlt.pspt.null", "ERR069");
		moduleErrs.setProperty("errors.resv.psng.Adlt.pspt.invalid", "ERR070");
		moduleErrs.setProperty("errors.resv.psng.pspt.duplicate", "ERR071");
		moduleErrs.setProperty("errors.resv.psng.FName.valid", "VFNAME");
		moduleErrs.setProperty("errors.resv.psng.LName.valid", "VLNAME");
		moduleErrs.setProperty("errors.resv.psng.City.valid", "VCITY");
		moduleErrs.setProperty("errors.resv.psng.EmgnTitle", "ERR072");
		moduleErrs.setProperty("errors.resv.psng.EmgnFName.null", "ERR073");
		moduleErrs.setProperty("errors.resv.psng.EmgnFName.valid", "ERR074");
		moduleErrs.setProperty("errors.resv.psng.EmgnLName.null", "ERR075");
		moduleErrs.setProperty("errors.resv.psng.EmgnLName.valid", "ERR076");
		moduleErrs.setProperty("errors.resv.psng.EmgnPhone.Country", "ERR077");
		moduleErrs.setProperty("errors.resv.psng.EmgnPhone.Area", "ERR078");
		moduleErrs.setProperty("errors.resv.psng.EmgnPhone.null", "ERR079");
		moduleErrs.setProperty("errors.resv.psng.EmgnEmail.null", "ERR080");
		moduleErrs.setProperty("errors.resv.psng.EmgnEmail.Invalid", "ERR081");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.null", "ERR082");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.invalid", "ERR083");
		moduleErrs.setProperty("errors.resv.psng.EmgnEmail.Check", "ERR084");
		moduleErrs.setProperty("msg.form.confirm.password.required", "ERR085");
		moduleErrs.setProperty("msg.form.password.notmatch", "ERR086");
		moduleErrs.setProperty("msg.form.password.max", "ERR087");
		moduleErrs.setProperty("errors.resv.psgn.phoneMobile.null", "ERR088");
		moduleErrs.setProperty("errors.resv.psgn.phoneMobile.null", "ERR088");
		moduleErrs.setProperty("errors.resv.psng.Adlt.Nationality.null", "ERR089");
		moduleErrs.setProperty("errors.resv.psng.chld.Nationality.null", "ERR090");
		moduleErrs.setProperty("errors.resv.psng.Adlt.pspt.expiry.null", "ERR091");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.expiry.null", "ERR092");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.expiry.null", "ERR096");
		moduleErrs.setProperty("errors.resv.psng.Adlt.pspt.issued.place.null", "ERR093");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.issued.place.null", "ERR094");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.issued.place.null", "ERR095");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfBirth.null", "ERR100");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfBirth.null", "ERR101");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.docType.null", "ERR102");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.docType.null", "ERR103");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.docNum.null", "ERR104");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.docNum.null", "ERR105");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfIssue.null", "ERR106");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfIssue.null", "ERR107");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.issueDate.null", "ERR108");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.issueDate.null", "ERR109");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.country.null", "ERR110");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.country.null", "ERR111");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.docNum.invalid", "ERR112");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.docNum.invalid", "ERR113");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfBirth.invalid", "ERR114");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfBirth.invalid", "ERR115");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfIssue.invalid", "ERR116");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfIssue.invalid", "ERR117");
		moduleErrs.setProperty("errors.resv.psng.FName.lesminlength", "ERR599");
		moduleErrs.setProperty("errors.resv.psng.CountryCode.mismatch", "ERR119");
		moduleErrs.setProperty("errors.resv.lms.ffid", "ERR120");
		moduleErrs.setProperty("errors.resv.lms.ffid.unregistered", "ERR121");
		moduleErrs.setProperty("errors.resv.lms.ffid.not.matched", "ERR122");
		moduleErrs.setProperty("errors.resv.psng.pax.dob", "ERR123");
		moduleErrs.setProperty("errors.resv.psng.Adlt.lms.dob", "ERR124");
		moduleErrs.setProperty("errors.resv.psng.Chld.lms.dob", "ERR125");
		moduleErrs.setProperty("errors.resv.psng.Adlt.DOB.null","ERR126");
		moduleErrs.setProperty("msg.pax.name.change.proceed.confirm","ERR127");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.nationalIDNo.null", "ERR128");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.nationalIDNo.invalid","ERR129");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.nationalIDNo.null", "ERR130");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.nationalIDNo.invalid","ERR131");
		moduleErrs.setProperty("errors.resv.psng.Inft.doco.nationalIDNo.null", "ERR132");
		moduleErrs.setProperty("errors.resv.psng.Inft.doco.nationalIDNo.invalid","ERR133");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.null", "ERR134");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.invalid", "ERR135");
		
		moduleErrs.setProperty("msg.form.lms.dob.required","userDateOfBirthRqrd");
		moduleErrs.setProperty("msg.form.lms.dob.incorrect", "userDateOfBirtIncorrect");
		moduleErrs.setProperty("msg.form.lms.hemail.required","headEmailReqrd");
		moduleErrs.setProperty("msg.form.lms.hemail.incorrect", "headEmailIncorrect");
		moduleErrs.setProperty("msg.form.lms.refmail.incorrect", "refEmailIncorrect");
		moduleErrs.setProperty("msg.form.lms.refmail.doesnt.exist", "refEmailDoesntExist");
		moduleErrs.setProperty("msg.form.lms.hemail.doesnt.exist", "headEmailDoesntExist");
		
		strClientErrors = JavaScriptGenerator.createClientErrorsArray(moduleErrs, language);
		return strClientErrors;
	}

	public static Map<String, String> getReservationContactDetailsErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("errors.resv.psng.Invalid", "ERR027");
		moduleErrs.setProperty("errors.resv.psng.FName.null", "ERR028");
		moduleErrs.setProperty("errors.resv.psng.LName.null", "ERR029");
		moduleErrs.setProperty("errors.resv.psng.Mobile.null", "ERR030");
		moduleErrs.setProperty("errors.resv.psng.Email.null", "ERR031");
		moduleErrs.setProperty("errors.resv.psng.Email.Invalid", "ERR032");
		moduleErrs.setProperty("errors.resv.psng.Email.Check", "ERR033");
		moduleErrs.setProperty("errors.resv.psng.Phone.Country", "ERR034");
		moduleErrs.setProperty("errors.resv.psng.Phone.Area", "ERR035");
		moduleErrs.setProperty("errors.resv.psng.Mobile.Country", "ERR036");
		moduleErrs.setProperty("errors.resv.psng.Mobile.Area", "ERR037");
		moduleErrs.setProperty("errors.resv.psng.name.number", "ERR040");
		moduleErrs.setProperty("errors.resv.psng.Country", "ERR041");
		moduleErrs.setProperty("errors.resv.psng.Title", "ERR042");
		moduleErrs.setProperty("errors.resv.psng.Nationality", "ERR048");
		moduleErrs.setProperty("errors.resv.psng.City", "ERR049");
		moduleErrs.setProperty("errors.resv.psng.invalidComb", "ERR050");
		moduleErrs.setProperty("errors.resv.psng.lesminlength", "ERR051");
		moduleErrs.setProperty("errors.resv.psng.greaterMaxlen", "ERR052");
		moduleErrs.setProperty("errors.resv.psng.zipCode", "ERR063");
		moduleErrs.setProperty("errors.resv.psng.zipCode.Invalid", "ERR064");
		moduleErrs.setProperty("errors.resv.psng.Address.null", "ERR065");
		moduleErrs.setProperty("errors.resv.psng.Fax.null", "ERR066");
		moduleErrs.setProperty("errors.resv.psng.Fax.Country", "ERR067");
		moduleErrs.setProperty("errors.resv.psng.Fax.Area", "ERR068");
		moduleErrs.setProperty("errors.resv.psng.pspt.null", "ERR069");
		moduleErrs.setProperty("errors.resv.psng.pspt.invalid", "ERR070");
		moduleErrs.setProperty("errors.resv.psng.pspt.duplicate", "ERR071");
		moduleErrs.setProperty("errors.resv.psng.FName.valid", "VFNAME");
		moduleErrs.setProperty("errors.resv.psng.LName.valid", "VLNAME");
		moduleErrs.setProperty("errors.resv.psng.City.valid", "VCITY");
		moduleErrs.setProperty("errors.resv.psng.EmgnTitle", "ERR072");
		moduleErrs.setProperty("errors.resv.psng.EmgnFName.null", "ERR073");
		moduleErrs.setProperty("errors.resv.psng.EmgnFName.valid", "ERR074");
		moduleErrs.setProperty("errors.resv.psng.EmgnLName.null", "ERR075");
		moduleErrs.setProperty("errors.resv.psng.EmgnLName.valid", "ERR076");
		moduleErrs.setProperty("errors.resv.psng.EmgnPhone.Country", "ERR077");
		moduleErrs.setProperty("errors.resv.psng.EmgnPhone.Area", "ERR078");
		moduleErrs.setProperty("errors.resv.psng.EmgnPhone.null", "ERR079");
		moduleErrs.setProperty("errors.resv.psng.EmgnEmail.null", "ERR080");
		moduleErrs.setProperty("errors.resv.psng.EmgnEmail.Invalid", "ERR081");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.null", "ERR082");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.invalid", "ERR083");
		moduleErrs.setProperty("errors.resv.psng.EmgnEmail.Check", "ERR084");
		moduleErrs.setProperty("errors.resv.psgn.phoneMobile.null", "ERR088");
		moduleErrs.setProperty("errors.resv.psng.Adlt.pspt.expiry.null", "ERR091");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.expiry.null", "ERR092");
		moduleErrs.setProperty("errors.resv.psng.Adlt.pspt.issued.place.null", "ERR093");
		moduleErrs.setProperty("errors.resv.psng.Chld.pspt.issued.place.null", "ERR094");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.issued.place.null", "ERR095");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfBirth.null", "ERR100");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfBirth.null", "ERR101");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.docType.null", "ERR102");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.docType.null", "ERR103");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.docNum.null", "ERR104");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.docNum.null", "ERR105");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfIssue.null", "ERR106");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfIssue.null", "ERR107");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.issueDate.null", "ERR108");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.issueDate.null", "ERR109");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.country.null", "ERR110");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.country.null", "ERR111");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.docNum.invalid", "ERR112");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.docNum.invalid", "ERR113");
		moduleErrs.setProperty("errors.resv.psng.Adlt.doco.placeOfBirth.invalid", "ERR114");
		moduleErrs.setProperty("errors.resv.psng.Chld.doco.placeOfBirth.invalid", "ERR115");
		moduleErrs.setProperty("errors.resv.psng.CountryCode.mismatch", "ERR119");
		moduleErrs.setProperty("msg.form.phone.wrongcomb", "wrongComb");
		moduleErrs.setProperty("msg.form.phone.lenless", "lenSmall");
		moduleErrs.setProperty("msg.form.phone.lengreat", "lenLong");
		moduleErrs.setProperty("msg.form.station.country.wrongcomb", "stationContryWrongComb");
		moduleErrs.setProperty("errors.resv.lms.ffid", "ERR120");
		moduleErrs.setProperty("errors.resv.lms.ffid.unregistered", "ERR121");
		moduleErrs.setProperty("errors.resv.lms.ffid.not.matched", "ERR122");
		moduleErrs.setProperty("errors.resv.psng.pax.dob", "ERR123");
		moduleErrs.setProperty("errors.resv.psng.Adlt.lms.dob", "ERR124");
		moduleErrs.setProperty("errors.resv.psng.Chld.lms.dob", "ERR125");
		moduleErrs.setProperty("errors.resv.psng.Adlt.DOB.null","ERR126");
		moduleErrs.setProperty("msg.pax.name.change.proceed.confirm","ERR127");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.null", "ERR134");
		moduleErrs.setProperty("errors.resv.psng.Inft.pspt.invalid", "ERR135");
		
		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getFlightSearchErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.resv.fltSrch.From.Location.null", "ERR001");
		moduleErrs.setProperty("errors.resv.fltSrch.To.Location.null", "ERR002");
		moduleErrs.setProperty("errors.resv.fltSrch.FromTo.location.compare", "ERR003");
		moduleErrs.setProperty("errors.resv.fltSrch.FromTo.date.compare", "ERR004");
		moduleErrs.setProperty("errors.errors.resv.fltSrch.Date.invalid", "ERR005");
		moduleErrs.setProperty("errors.resv.fltSrch.Currency.null", "ERR006");
		moduleErrs.setProperty("errors.resv.fltSrch.FromDate.null", "ERR007");
		moduleErrs.setProperty("errors.resv.fltSrch.To.Date.null", "ERR008");
		moduleErrs.setProperty("errors.resv.fltSrch.From.Date.compare", "ERR009");
		moduleErrs.setProperty("errors.resv.fltSrch.cos.null", "ERR010");
		moduleErrs.setProperty("errors.resv.fltSrch.paxCat.null", "ERR011");
		moduleErrs.setProperty("errors.resv.fltSrch.fare.null", "ERR012");
		moduleErrs.setProperty("errors.resv.fltSrch.cos.downgrade", "ERR095");
		moduleErrs.setProperty("errors.resv.fltSrch.bin.invalid", "ERR096");

		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getLoginClientErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.sign.loginId", "userIdRqrd");
		moduleErrs.setProperty("errors.sign.password", "userPasswordRqrd");
		moduleErrs.setProperty("errors.sign.invalid", "invalidChar");

		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getForgotPwdErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.cus.forgotPWD.ID.null", "ERR043");
		moduleErrs.setProperty("errors.cus.forgotPWD.seq.null", "ERR044");
		moduleErrs.setProperty("errors.cus.forgotPWD.ans.null", "ERR045");
		moduleErrs.setProperty("errors.cus.forgotPWD.ID.invalid", "ERR046");
		moduleErrs.setProperty("errors.resv.psng.Invalid", "ERR047");

		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getKioskLoginErrors(HttpServletRequest request) throws ModuleException {

		Map<String, String> errorMap = new HashMap<String, String>();
		errorMap.put("userInvalidID", "Please Enter User Name");
		errorMap.put("userInvalidPassword", "Please Enter Password");
		errorMap.put("userInvalidUserIdPassword", "Enter valid User ID & password");
		errorMap.put("userNoPrivilege", "Insufficient Privileges");

		return errorMap;

	}

	public static Map<String, String> getPromotionMessages(HttpServletRequest request) throws ModuleException {
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("promotion.subcribed.flexi", "flexiSubcribed");
		moduleErrs.setProperty("promotion.temrs.flexi", "flexiTerms");
		moduleErrs.setProperty("promotion.subcribed.next", "nextSubcribed");
		moduleErrs.setProperty("promotion.terms.next", "nextTerms");
		moduleErrs.setProperty("promotion.success.header", "promoHeading");

		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getCardErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.sign.invalid", "invalidChr");
		moduleErrs.setProperty("pay.gateway.error.common.blankCardType", "reqCardTYpe");
		moduleErrs.setProperty("pay.gateway.error.common.blankCardHolderName", "reqCardHolName");
		moduleErrs.setProperty("pay.gateway.error.common.blankCardNumber", "reqCardNo");
		moduleErrs.setProperty("pay.gateway.error.common.blankExpiryMonth", "reqExpiryMonth");
		moduleErrs.setProperty("pay.gateway.error.common.blankExpiryYear", "reqExpiryYear");
		moduleErrs.setProperty("pay.gateway.error.common.blankCVV", "reqCVV");
		moduleErrs.setProperty("pay.gateway.error.common.blankBillingAddress1", "reqBillingAddress1");
		moduleErrs.setProperty("pay.gateway.error.common.blankCity", "reqCity");
		moduleErrs.setProperty("pay.gateway.error.common.blankPostalCode", "reqPostalCode");
		moduleErrs.setProperty("pay.gateway.error.common.blankState", "reqState");
		moduleErrs.setProperty("pay.gateway.error.common.invalidCardNumber", "invalidCardNo");
		moduleErrs.setProperty("pay.gateway.error.common.invalidExpiryDateLessThanCurrentMonth", "invalidExpiryDate");
		moduleErrs.setProperty("pay.gateway.error.common.invalidCVV", "invalidCVV");
		moduleErrs.setProperty("pay.gateway.error.common.invalidCVV", "invalidCVV");
		moduleErrs.setProperty("errors.resv.pay.cc.bin.not.match", "ccBinNotMatch");
		moduleErrs.setProperty("pay.gateway.error.common.invalidQiwiMobileNo", "invalidQiwiMobileNo");
		moduleErrs.setProperty("pay.gateway.error.common.invalidPayFortMobileNo", "invalidPayFortMobileNo");
		moduleErrs.setProperty("pay.gateway.error.common.invalidPayFortEmail", "invalidPayFortEmail");
		moduleErrs.setProperty("pay.gateway.error.common.invalidPaymentMobileNo", "invalidPaymentMobileNo");
		moduleErrs.setProperty("pay.gateway.error.common.invalidPaymentEmail", "invalidPaymentEmail");
		moduleErrs.setProperty("pay.gateway.error.common.invalidUserName", "invalidUserName");
		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	public static Map<String, String> getContainerErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.resv.fltSrch.terms", "ERR042");
		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}
	
	public static Map<String, String> getVoucherErrors(HttpServletRequest request) throws ModuleException {
		//jAlert("Can not find payment method. Invalid payment gateway configuration. Please contact call center or administrator");

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("errors.voucher.voucherdetails.select", "selectVoucher");
		moduleErrs.setProperty("errors.voucher.firstname.empty", "firstNameEmpty");
		moduleErrs.setProperty("errors.voucher.lastname.empty", "lastNameEmpty");
		moduleErrs.setProperty("errors.voucher.email.empty", "emailEmpty");
		moduleErrs.setProperty("errors.voucher.email.invalid", "emailInvalid");
		moduleErrs.setProperty("errors.voucher.paymentmethod.invalid", "noPaymentMethod");
		moduleErrs.setProperty("errors.voucher.cardholdername.empty", "noCardHolderName");
		moduleErrs.setProperty("errors.voucher.card.invalid", "invalidCard");
		moduleErrs.setProperty("errors.voucher.cardmonth.empty", "emptyMonth");
		moduleErrs.setProperty("errors.voucher.card.empty", "emptyCard");
		moduleErrs.setProperty("errors.voucher.cardyear.empty", "emptyYear");
		moduleErrs.setProperty("errors.voucher.cardcvv.empty", "emptyCvv");
		moduleErrs.setProperty("errors.voucher.cardcvv.invalid", "invalidCvv");
		moduleErrs.setProperty("errors.voucher.request.incomplete", "errorResponse");
		moduleErrs.setProperty("errors.voucher.voucher.selectemail", "selectToEmail");
		moduleErrs.setProperty("errors.voucher.voucher.selectprint", "selectToPrint");
		moduleErrs.setProperty("errors.voucher.voucher.selectone", "selectOne");
		return createClientErrors(moduleErrs, SessionUtil.getLanguage(request));
	}

	@SuppressWarnings("unchecked")
	private static Map<String, String> createClientErrors(Properties moduleErrs, String strLanguage) throws ModuleException {
		Map<String, String> errorMap = new HashMap<String, String>();
		Set keySet = moduleErrs.keySet();
		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			errorMap.put(moduleErrs.getProperty(key), I18NUtil.getMessage(key, strLanguage));
		}
		return errorMap;
	}

	@SuppressWarnings("unchecked")
	private static Map<String, String> createAgentErrors(Properties moduleErrs, String strLanguage) throws ModuleException {
		Map<String, String> errorMap = new HashMap<String, String>();
		Set keySet = moduleErrs.keySet();
		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			errorMap.put(moduleErrs.getProperty(key), I18NUtil.getMessage(key, strLanguage));
		}
		return errorMap;
	}

}
