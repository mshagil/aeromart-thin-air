package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.AirportDDListGenerator;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ModifySearchAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifySearchAction.class);

	private boolean success = true;

	private String messageTxt;

	private List airportList;

	private Map<String, String> errorInfo;

	private HashMap<String, String> jsonLabel;

	private List<String[]> activeCos;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "PgSearch" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			airportList = AirportDDListGenerator.createIBEAirportDDList(null);
			activeCos = SelectListGenerator.getCOSActiveCodeDescList();
			errorInfo = ErrorMessageUtil.getFlightSearchErrors(request);
		} catch (ModuleException me) {
			log.error(me.getModuleCode(), me);
			success = false;
			if (!me.getExceptionCode().isEmpty()) {
				messageTxt = I18NUtil.getMessage(me.getExceptionCode());
			} else {
				messageTxt = me.getMessage();
			}
		} catch (Exception ex) {
			log.error("ModifySearchAction==>", ex);
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
		}
		return result;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List getAirportList() {
		return airportList;
	}

	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List<String[]> getActiveCos() {
		return activeCos;
	}
}
