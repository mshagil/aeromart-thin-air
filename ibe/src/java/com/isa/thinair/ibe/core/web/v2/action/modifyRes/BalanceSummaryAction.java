package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.ibe.api.dto.CancelBalanceSummaryDTO;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.ModifyBalanceDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.BalanceSummaryUtil;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class BalanceSummaryAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(BalanceSummaryAction.class);

	private boolean success = true;

	private String messageTxt;

	private ConfirmUpdateChargesTO updateCharge;

	private ArrayList<String> outFlightRPHList = new ArrayList<String>();

	private ArrayList<String> retFlightRPHList = new ArrayList<String>();

	private String version;

	// private String mode;

	private String oldAllSegments;

	private String pnr;

	private boolean groupPNR;

	private FareQuoteTO fareQuote;

	private String modifySegmentRefNos;

	private CancelBalanceSummaryDTO balanceSummary;

	private String jsonPaxWiseAnci = null;

	private String jsonInsurance = null;

	private boolean blnNoPay;

	private boolean modifySegment;

	private boolean cancelReservation;

	private boolean cancelSegment;

	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount;

	private HashMap<String, String> jsonLabel;

	// Fare Quote Update Details
	private ModifyBalanceDTO modifyBalanceDTO;

	private boolean addGroundSegment;

	public String execute() {
		LCCClientResAlterQueryModesTO lCCClientResAlterQueryModesTO = null;
		String forward = StrutsConstants.Result.SUCCESS;
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		ReservationBalanceTO lCCClientReservationBalanceTO = null;
		List<ReservationPaxTO> paxList = null;
		String strLanguage = SessionUtil.getLanguage(request);

		try {
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
			BigDecimal totalAnciCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

			if (resInfo == null) {
				// TODO
				throw new ModuleException("session.error");
			}
			Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getvalidateModifingSegment(colsegs,
					resInfo.getSegmentStatus(), modifySegmentRefNos);
			if (cancelReservation) {
				lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeCancelReservationQueryRequest(pnr, version,
						groupPNR);

			} else if (cancelSegment) {
				lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeCancelSegmentQueryRequest(pnr, colModSegs,
						version, groupPNR);
			} else if (addGroundSegment) {

				SYSTEM system = SYSTEM.getEnum(getSearchParams().getSearchSystem());
				if (system == SYSTEM.INT || groupPNR) {
					version = ReservationCommonUtil.getCorrectInterlineVersion(version);
				}
				SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
				boolean isReturn = fltSegBuilder.isReturn();

				LCCInsuredJourneyDTO insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(
						fltSegBuilder.getSelectedFlightSegments(), isReturn, getTrackInfo().getOriginChannelId());
				List<LCCInsuranceQuotationDTO> insuranceQuotations = AncillaryJSONUtil.getInsuranceQuotation(jsonInsurance,
						insJrnyDto);
				paxList = AncillaryJSONUtil.extractReservationPax(jsonPaxWiseAnci, insuranceQuotations,
						getApplicationEngine(getTrackInfo().getOriginChannelId()), fltSegBuilder.getSelectedFlightSegments(),
						null, resInfo.getAnciOfferTemplates());
				totalAnciCharges = ReservationUtil.getTotalAncillaryCharges(paxList);

				FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
				// FIXME avoid creating AvailableFlightSearchDTOBuilder and directly create the flightPriceRQ
				// Current implementation leads unnecessary DTO transformation step
				AvailableFlightSearchDTOBuilder fareQuteParams = SearchUtil.getSearchBuilder(getSearchParams());
				if (pnr != null && !"".equals(pnr)) {
					SearchUtil.setExistingSegInfo(fareQuteParams.getSearchDTO(), oldAllSegments, modifySegmentRefNos);
				}

				flightPriceRQ = BeanUtil.createFareQuoteRQ(fareQuteParams, outFlightRPHList, retFlightRPHList, getSearchParams());
				flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());

				ExternalChargeUtil.injectOndBaggageGroupId(paxList, fltSegBuilder.getSelectedFlightSegments());
				LCCClientSegmentAssembler lccClientSegmentAssembler = BookingUtil.getSegmentAssembler(
						fltSegBuilder.getSelectedFlightSegments(), BookingUtil.getNextSegmentSequnce(colModSegs));
				lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
				lccClientSegmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());

				lccClientSegmentAssembler.setSelectedFareSegChargeTO(resInfo.getPriceInfoTO().getFareSegChargeTO());
				lccClientSegmentAssembler.setFlexiSelected(getSearchParams().isFlexiSelected());
				lccClientSegmentAssembler.setPassengerExtChargeMap(getPaxExtChargesMap(paxList));

				BigDecimal customAdultCancelCharge = null;
				BigDecimal customInfantCancelCharge = null;
				BigDecimal customChildCancelCharge = null;
				Collection<LCCClientReservationSegment> colOldSegs = null;

				lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeAddSegmentRequest(pnr, colOldSegs, colsegs,
						lccClientSegmentAssembler, customAdultCancelCharge, customChildCancelCharge, customInfantCancelCharge,
						version, modifySegmentRefNos, resInfo.getReservationStatus());

			} else if (modifySegment) {
				SYSTEM system = SYSTEM.getEnum(getSearchParams().getSearchSystem());
				if (system == SYSTEM.INT || groupPNR) {
					version = ReservationCommonUtil.getCorrectInterlineVersion(version);
				}
				SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
				boolean isReturn = fltSegBuilder.isReturn();

				LCCInsuredJourneyDTO insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(
						fltSegBuilder.getSelectedFlightSegments(), isReturn, getTrackInfo().getOriginChannelId());
				List<LCCInsuranceQuotationDTO> insuranceQuotation = AncillaryJSONUtil.getInsuranceQuotation(jsonInsurance,
						insJrnyDto);
				paxList = AncillaryJSONUtil.extractReservationPax(jsonPaxWiseAnci, insuranceQuotation,
						getApplicationEngine(getTrackInfo().getOriginChannelId()), fltSegBuilder.getSelectedFlightSegments(),
						null, null);
				totalAnciCharges = ReservationUtil.getTotalAncillaryCharges(paxList);

				FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
				// FIXME avoid creating AvailableFlightSearchDTOBuilder and directly create the flightPriceRQ
				// Current implementation leads unnecessary DTO transformation step
				AvailableFlightSearchDTOBuilder fareQuteParams = SearchUtil.getSearchBuilder(getSearchParams());
				if (pnr != null && !"".equals(pnr)) {
					SearchUtil.setExistingSegInfo(fareQuteParams.getSearchDTO(), oldAllSegments, modifySegmentRefNos);
				}
				// if (modifySegment) {
				SearchUtil.setModifingSegmentSearchData(fareQuteParams, oldFareID, oldFareType, oldFareAmount);
				// }
				flightPriceRQ = BeanUtil.createFareQuoteRQ(fareQuteParams, outFlightRPHList, retFlightRPHList, getSearchParams());
				flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());

				ExternalChargeUtil.injectOndBaggageGroupId(paxList, fltSegBuilder.getSelectedFlightSegments());
				LCCClientSegmentAssembler lccClientSegmentAssembler = BookingUtil.getSegmentAssembler(
						fltSegBuilder.getSelectedFlightSegments(), BookingUtil.getNextSegmentSequnce(colModSegs));
				lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
				lccClientSegmentAssembler.setLccTransactionIdentifier(resInfo.getTransactionId());

				lccClientSegmentAssembler.setSelectedFareSegChargeTO(resInfo.getPriceInfoTO().getFareSegChargeTO());
				// lccClientSegmentAssembler.setFlexiSelected(ReservationUtil.isFlexiSelected(getFlexiSelection()));
				lccClientSegmentAssembler.setFlexiSelected(getSearchParams().getOndQuoteFlexi().get(0));
				lccClientSegmentAssembler.setPassengerExtChargeMap(getPaxExtChargesMap(paxList));

				lCCClientResAlterQueryModesTO = LCCClientResAlterModesTO.composeModifySegmentQueryRequest(pnr, colModSegs,
						lccClientSegmentAssembler, version, groupPNR);

			}

			if (resInfo == null) {
				resInfo = new IBEReservationInfoDTO();
				SessionUtil.setIBEreservationInfo(request, resInfo);
			}

			lCCClientReservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD().getResAlterationBalanceSummary(
					lCCClientResAlterQueryModesTO, getTrackInfo());

			version = lCCClientReservationBalanceTO.getVersion();

			if (lCCClientReservationBalanceTO.getVersion() != null) {
				lCCClientResAlterQueryModesTO.setVersion(lCCClientReservationBalanceTO.getVersion());
			}

			// TODO Remove
			resInfo.setCreditableAmount(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getSegmentSummary()
					.getCurrentRefunds()));
			resInfo.setModifyCharge(AccelAeroCalculator.formatAsDecimal(lCCClientReservationBalanceTO.getSegmentSummary()
					.getNewModAmount()));

			if (cancelReservation || cancelSegment) {
				// Local for Cancel Reservation/Segment
				String[] pagesIDs = { "PgCancel", "Common", "PgPriceBreakDown", "PgInterlineConfirm" };
				jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
				balanceSummary = BalanceSummaryUtil.getCancelBalanceSummary(lCCClientReservationBalanceTO,
						resInfo.getPaxCreditMap(pnr));
			} else {
				resInfo.setlCCClientReservationBalance(lCCClientReservationBalanceTO);
				String[] pagesIDs = { "PgModifySement", "PgPriceBreakDown" };
				jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
				// Clear Display Modify Data if not display in calendar page
				if (!AppSysParamsUtil.isModifySegmentBalanceDisplayAvailabilitySearch()) {
					updateCharge = null;
				} else {
					resInfo.setCreditCardFee(AccelAeroCalculator.getDefaultBigDecimalZero());
					updateCharge = BalanceSummaryUtil.getChargesDetails(lCCClientReservationBalanceTO, paxList, resInfo,
							getSearchParams().getSelectedCurrency(), strLanguage, pnr, getSearchParams().isQuoteOutboundFlexi(),
							getSearchParams().isQuoteInboundFlexi(), false);
					blnNoPay = resInfo.isNoPay();
				}
				modifyBalanceDTO = BalanceSummaryUtil.getFareQuoteUpdateDetails(lCCClientReservationBalanceTO,
						resInfo.getPaxCreditMap(pnr), getSearchParams().getSelectedCurrency(), resInfo.isInfantPaymentSeparated());
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("BalanceSummaryAction==>" + ex, ex);
		}

		return forward;
	}

	private Map<Integer, List<LCCClientExternalChgDTO>> getPaxExtChargesMap(Collection<ReservationPaxTO> paxList) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		for (ReservationPaxTO pax : paxList) {
			paxExtChgMap.put(pax.getSeqNumber(), pax.getExternalCharges());
		}
		return paxExtChgMap;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public ConfirmUpdateChargesTO getUpdateCharge() {
		return updateCharge;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public static void setLog(Log log) {
		BalanceSummaryAction.log = log;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public void setUpdateCharge(ConfirmUpdateChargesTO updateCharge) {
		this.updateCharge = updateCharge;
	}

	public void setCancelReservation(boolean cancelReservation) {
		this.cancelReservation = cancelReservation;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isGroupPNR() {
		return groupPNR;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public void setFareQuote(FareQuoteTO fareQuote) {
		this.fareQuote = fareQuote;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public CancelBalanceSummaryDTO getBalanceSummary() {
		return balanceSummary;
	}

	public boolean isBlnNoPay() {
		return blnNoPay;
	}

	public void setJsonPaxWiseAnci(String jsonPaxWiseAnci) {
		this.jsonPaxWiseAnci = jsonPaxWiseAnci;
	}

	public void setJsonInsurance(String jsonInsurance) {
		this.jsonInsurance = jsonInsurance;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public ModifyBalanceDTO getModifyBalanceDTO() {
		return modifyBalanceDTO;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public boolean isAddGroundSegment() {
		return addGroundSegment;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

}
