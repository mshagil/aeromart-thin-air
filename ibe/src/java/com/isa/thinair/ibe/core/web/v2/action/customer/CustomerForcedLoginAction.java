package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.impl.StrutsActionProxyFactory;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.DefaultActionProxyFactory;


public abstract class CustomerForcedLoginAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(CustomerForcedLoginAction.class);

	private boolean success = true;

	protected String messageCode = "";
	protected int customerId;

	protected abstract boolean accessGranted();

	public String execute() {

		AirCustomerServiceBD airCustomerServiceBD = ModuleServiceLocator.getCustomerBD();

		String forward = StrutsConstants.Result.ERROR;
		Customer customer;

		try {
			SessionUtil.expireSession(request);
			ForceLoginInvoker.defaultLogin();
			SessionUtil.initialize(request);
			SessionUtil.setLanguage(request);
			SessionUtil.setLocale(request);
			SessionUtil.setCarrier(request, AppSysParamsUtil.getDefaultCarrierCode());
			SessionUtil.setDesignVersion(request, AppSysParamsUtil.getDefaultDesign());

			if (accessGranted()) {

				HttpSession session = request.getSession();
				String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
				GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();
				session.setAttribute(CustomerWebConstants.APP_AIRLINE_NAME_STR,
						globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME, carrierCode));
				session.setAttribute(CustomerWebConstants.APP_AIRLINE_HOME_TEXT_STR,
						globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode));
				session.setAttribute(CustomerWebConstants.APP_AIRLINE_HOME_URL_STR,
						globalConfig.getBizParam(SystemParamKeys.AIRLINE_URL, carrierCode));

				customer = airCustomerServiceBD.getCustomer(customerId);
				SessionUtil.setCustomerId(request, customer.getCustomerId());
				SessionUtil.setCustomerEMail(request, customer.getEmailId());
				SessionUtil.setCustomerPassword(request, customer.getPassword());

				if (customer.getLMSMemberDetails() != null) {
					SessionUtil.setLoyaltyFFID(request, customer.getLMSMemberDetails().getFfid());
				}

				IBECommonDTO commonDTO = new IBECommonDTO();
				commonDTO.setRegCustomer(true);
				SystemUtil.setCommonParameters(request, commonDTO);
				request.setAttribute("commonParams", commonDTO);

				forward = delegates();
			} else {
				if (SessionUtil.isNewDesignVersion(request)) {
					forward = StrutsConstants.Jsp.Customer.LOGIN_V3;
				} else {
					forward = StrutsConstants.Jsp.Customer.LOGIN;
				}
			}

		} catch (Exception e) {
			success = false;
			String msg = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, msg);
			log.error("CustomerForcedLoginAction ---- ", e);
		}

		return forward;
	}


	public String delegates() throws Exception {
		String forward = StrutsConstants.Result.ERROR;
		String result;

		Map<String, Object> map = new HashMap<String, Object>();

		DefaultActionProxyFactory actionProxyFactory = new StrutsActionProxyFactory();
		actionProxyFactory.setContainer(ActionContext.getContext().getContainer());


		ActionProxy actionProxy = actionProxyFactory.createActionProxy(StrutsConstants.Namespace.PUBLIC,
				StrutsConstants.Action.REDIRECT_TO_CUSTOMER_LOGIN, "execute", map);

		CustomerLoginAction customerLoginAction = (CustomerLoginAction) actionProxy.getAction();
		customerLoginAction.setServletRequest(request);
		customerLoginAction.setCommonParams((IBECommonDTO) request.getAttribute("commonParams"));
		customerLoginAction.setEmailId(String.valueOf(SessionUtil.getCustomerEmail(request)));
		customerLoginAction.setPassword(SessionUtil.getCustomerPassword(request));

		result = actionProxy.getInvocation().getInvocationContext().getActionInvocation().invokeActionOnly();

		if (result.equals(StrutsConstants.Result.SUCCESS)) {
			actionProxy = actionProxyFactory.createActionProxy(StrutsConstants.Namespace.PUBLIC,
					StrutsConstants.Action.SHOW_CUSTOMER_LOAD_PAGE, "loadCustomerHomePage", map);

			ShowCustomerLoadPageAction customerLoadPageAction = (ShowCustomerLoadPageAction) actionProxy.getAction();
			customerLoadPageAction.setCommonParams((IBECommonDTO) request.getAttribute("commonParams"));
			customerLoadPageAction.setServletRequest(request);

			result = actionProxy.getInvocation().getInvocationContext().getActionInvocation().invokeActionOnly();
		}

		forward = result;

		return forward;
	}


	public boolean isSuccess() {
		return success;
	}

}