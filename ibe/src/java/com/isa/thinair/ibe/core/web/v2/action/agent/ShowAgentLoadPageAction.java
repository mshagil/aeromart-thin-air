package com.isa.thinair.ibe.core.web.v2.action.agent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;

/**
 * Load Registerd agent pages
 * 
 * @author Gavinda Nanayakkara
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Jsp.Agent.REGISTER, value = StrutsConstants.Jsp.Agent.REGISTER),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class ShowAgentLoadPageAction extends IBEBaseAction {

	public String loadAgentRegisterPage() {
		request.getSession().setAttribute("taCo", "TA");
		return StrutsConstants.Jsp.Agent.REGISTER;
	}

	public String loadCorporateRegisterPage() {
		request.getSession().setAttribute("taCo", "CO");
		return StrutsConstants.Jsp.Agent.REGISTER;
	}

}
