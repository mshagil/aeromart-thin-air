package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.commons.api.dto.IBEContactConfigDTO;
import com.isa.thinair.commons.api.dto.UserRegConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.CustomerUtilV2;
import com.isa.thinair.ibe.core.web.v2.util.PaxUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.webplatform.api.dto.LmsMemberDTO;

/**
 * Load Registered user pages
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Jsp.Customer.LOGIN, value = StrutsConstants.Jsp.Customer.LOGIN),
		@Result(name = StrutsConstants.Jsp.Customer.LOGIN_V3, value = StrutsConstants.Jsp.Customer.LOGIN_V3),
		@Result(name = StrutsConstants.Jsp.Customer.FORGOTPWD, value = StrutsConstants.Jsp.Customer.FORGOTPWD),
		@Result(name = StrutsConstants.Jsp.Customer.FORGOTPWD_V3, value = StrutsConstants.Jsp.Customer.FORGOTPWD_V3),
		@Result(name = StrutsConstants.Jsp.Customer.REGISTER, value = StrutsConstants.Jsp.Customer.REGISTER),
		@Result(name = StrutsConstants.Jsp.Customer.REGISTER_V3, value = StrutsConstants.Jsp.Customer.REGISTER_V3),
		@Result(name = StrutsConstants.Jsp.Customer.HOME, value = StrutsConstants.Jsp.Customer.HOME),
		@Result(name = StrutsConstants.Jsp.Customer.HOME_V3, value = StrutsConstants.Jsp.Customer.HOME_V3),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_DETAIL, value = StrutsConstants.Jsp.Customer.RESERVATION_DETAIL),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_DETAIL_V3, value = StrutsConstants.Jsp.Customer.RESERVATION_DETAIL_V3),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_LIST, value = StrutsConstants.Jsp.Customer.RESERVATION_LIST),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_LIST_V3, value = StrutsConstants.Jsp.Customer.RESERVATION_LIST_V3),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_CREDIT, value = StrutsConstants.Jsp.Customer.RESERVATION_CREDIT),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_CREDIT_V3, value = StrutsConstants.Jsp.Customer.RESERVATION_CREDIT_V3),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_HISTORY, value = StrutsConstants.Jsp.Customer.RESERVATION_HISTORY),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_HISTORY_V3, value = StrutsConstants.Jsp.Customer.RESERVATION_HISTORY_V3),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_CUSTOMERPROFILE, value = StrutsConstants.Jsp.Customer.RESERVATION_CUSTOMERPROFILE),
		@Result(name = StrutsConstants.Jsp.Customer.RESERVATION_CUSTOMERPROFILE_V3, value = StrutsConstants.Jsp.Customer.RESERVATION_CUSTOMERPROFILE_V3),
		@Result(name = StrutsConstants.Jsp.Customer.ACTIVATE_LOYALTY_ACCOUNT, value = StrutsConstants.Jsp.Customer.ACTIVATE_LOYALTY_ACCOUNT),
		@Result(name = StrutsConstants.Jsp.Customer.ACTIVATE_LOYALTY_ACCOUNT_V3, value = StrutsConstants.Jsp.Customer.ACTIVATE_LOYALTY_ACCOUNT_V3),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.MODIFY_SEGMENT, value = StrutsConstants.Jsp.ModifyReservation.MODIFY_SEGMENT),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.MODIFY_SEGMENT_V3, value = StrutsConstants.Jsp.ModifyReservation.MODIFY_SEGMENT_V3),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.ADD_BUS_SEGMENT, value = StrutsConstants.Jsp.ModifyReservation.ADD_BUS_SEGMENT),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.ADD_BUS_SEGMENT_V3, value = StrutsConstants.Jsp.ModifyReservation.ADD_BUS_SEGMENT_V3),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.BALANCE_SUMMARY, value = StrutsConstants.Jsp.ModifyReservation.BALANCE_SUMMARY),
		@Result(name = StrutsConstants.Jsp.ModifyReservation.BALANCE_SUMMARY_V3, value = StrutsConstants.Jsp.ModifyReservation.BALANCE_SUMMARY_V3),
        @Result(name = StrutsConstants.Jsp.Customer.LMSDETAILS_V3, value = StrutsConstants.Jsp.Customer.LMSDETAILS_V3),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class ShowCustomerLoadPageAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(ShowCustomerLoadPageAction.class);
	
	LmsMemberDTO lmsDetails;
	
	public String execute() throws JSONException {
		return loadCustomerHomePage();
	}

	public String loadReservationList() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_LIST_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_LIST;
		}
		return forward;
	}

	public String loadReservationCredit() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_CREDIT_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_CREDIT;
		}
		return forward;
	}

	public String loadReservationHistory() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_HISTORY_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_HISTORY;
		}
		return forward;
	}

	public String loadCustomerProfile() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_CUSTOMERPROFILE_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_CUSTOMERPROFILE;
		}
		return forward;
	}

	public String loadCustomerLoginPage() {
		String forward = null;
		request.setAttribute(ReservationWebConstnts.HTTPS_COMPATIBILITY, AppSysParamsUtil.isHttpsEnabledForIBE());
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.LOGIN_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.LOGIN;
		}
		return forward;
	}

	public String loadCustomerForgotPWD() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.FORGOTPWD_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.FORGOTPWD;
		}
		return forward;
	}

    public String loadCustomerLms() {
        String forward = null;
        if (SessionUtil.isNewDesignVersion(request)) {
            forward = StrutsConstants.Jsp.Customer.LMSDETAILS_V3;
        } else {
            forward = StrutsConstants.Jsp.Customer.LMSDETAILS_V3;
        }
        return forward;
    }

	public String loadCustomerHomePage() throws JSONException {
		boolean isRegUser = isValidateRegisterUser();
		String pnr = request.getParameter("pnr");
		if (pnr == null) {
			pnr = (String) request.getAttribute("pnr");
		}

		String socialSiteType = request.getParameter("externalSiteResponse.siteType");
		String socialProfileUrl = request.getParameter("externalSiteResponse.link");
		request.setAttribute(CustomerWebConstants.SOCIAL_SITE_TYPE, socialSiteType);
		request.setAttribute(CustomerWebConstants.SOCIAL_PIC_URL, socialProfileUrl);
		request.setAttribute(ReservationWebConstnts.PARAM_IMAGE_HOST_PATH, AppSysParamsUtil.getImageUploadPath());
		getCommonParams().setSkipPersianCalendarChange(AppSysParamsUtil.skipPersianDateChangeInIBE());
		if (pnr != null && !pnr.trim().isEmpty() && isRegUser == false && SessionUtil.getManageBookingPNR(request).equals(pnr)) {
			getCommonParams().setRegCustomer(false);
		} else if (isRegUser) {
			getCommonParams().setRegCustomer(true);
			// Handle Request Come from confirmation Page
			getCommonParams().setLocale(SessionUtil.getLanguage(request));
		} else {
			String forward = null;
			request.setAttribute(ReservationWebConstnts.HTTPS_COMPATIBILITY, AppSysParamsUtil.isHttpsEnabledForIBE());
			if (SessionUtil.isNewDesignVersion(request)) {
				forward = StrutsConstants.Jsp.Customer.LOGIN_V3;
			} else {
				forward = StrutsConstants.Jsp.Customer.LOGIN;
			}
			return forward;
		}
		// TODO Refactor
		request.setAttribute(ReservationWebConstnts.PARAM_SYSTEM_DEFAULT,
				CommonUtil.convertToJSON((SystemUtil.getSystemDefaultParams(request))));

		if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
			request.setAttribute(ReservationWebConstnts.PARAM_OND_SOURCE, AppSysParamsUtil.getDynamicOndListUrl());
		}
		Map<String, UserRegConfigDTO> userRegMap = new HashMap<String, UserRegConfigDTO>();
		Map<String, List<String>> validationGroup = new HashMap<String, List<String>>();
		Map<String, IBEContactConfigDTO> contactConfigMap = new HashMap<String, IBEContactConfigDTO>();

		List<UserRegConfigDTO> configList = IBEModuleUtils.getGlobalConfig().getUserRegConfig();
		PaxUtil.populateUserRegConfig(configList, userRegMap, validationGroup);

		List contactConfigList = IBEModuleUtils.getGlobalConfig().getContactDetailsConfig(AppIndicatorEnum.APP_IBE.toString());
		PaxUtil.populateContactConfig(contactConfigList, contactConfigMap, validationGroup);

		request.setAttribute(ReservationWebConstnts.REQ_USER_REG_CONFIG, JSONUtil.serialize(userRegMap));
		request.setAttribute(ReservationWebConstnts.REQ_CONTACT_CONFIG, JSONUtil.serialize(contactConfigMap));
		request.setAttribute(ReservationWebConstnts.REQ_VALIDATION_GROUP, JSONUtil.serialize(validationGroup));
		request.setAttribute(CustomerWebConstants.ENABLE_FACEBOOK_LOGIN, AppSysParamsUtil.isSocialLoginFromFacebookEnabled());
		request.setAttribute(CustomerWebConstants.FACEBOOK_APP_ID, AppSysParamsUtil.getFacebookAppIdForSocialLogin());
		request.setAttribute(CustomerWebConstants.ENABLE_LINKEDIN_LOGIN, AppSysParamsUtil.isSocialLoginFromLinkedInEnabled());
		request.setAttribute(CustomerWebConstants.LINKEDIN_APP_ID, AppSysParamsUtil.getLinkedInAPIKeyForSocialLogin());
		request.setAttribute(CustomerWebConstants.ENABLE_MULTICITY_SEARCH, AppSysParamsUtil.isOpenJawMultiCitySearchEnabled());
		
		int customerID = SessionUtil.getCustomerId(request);
		
		AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();
		try {
			Customer customer = customerDelegate.getCustomer(customerID);
			if(customer != null && !customer.getCustomerLmsDetails().isEmpty()){
				LmsMember lmsMember = customer.getCustomerLmsDetails().iterator().next();
				SessionUtil.setLoyaltyFFID(request, lmsMember.getFfid());
				lmsDetails = CustomerUtilV2.getLmsMemberDTO(lmsMember);
				request.setAttribute(CustomerWebConstants.LMS_DETAILS, lmsDetails);
			}
		} catch (ModuleException me) {
			log.error("ShowCustomerLoadPageAction==>", me);
		}
		
		String forward = null;

		if (SessionUtil.getIbePromotionPaymentPostDTO(request) != null) {
			request.setAttribute(ReservationWebConstnts.REQ_FROM_PROMOTION_PAGE, true);
		}

		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.HOME_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.HOME;
		}
		return forward;
	}

	public String loadCustomerRegisterPage() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.REGISTER_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.REGISTER;
		}
		return forward;
	}

	public String loadCustomerReservationDetail() {
		String forward = null;

		if (SessionUtil.getIbePromotionPaymentPostDTO(request) != null) {
			if (SessionUtil.getIbePromotionPaymentPostDTO(request).getTotalPayment() != null) {
				request.setAttribute(ReservationWebConstnts.REQ_FROM_PROMOTION_PAGE, true);
			}

		}
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_DETAIL_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.RESERVATION_DETAIL;
		}
		return forward;
	}

	public String loadCustomerModifySegment() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.ModifyReservation.MODIFY_SEGMENT_V3;
		} else {
			forward = StrutsConstants.Jsp.ModifyReservation.MODIFY_SEGMENT;
		}
		return forward;
	}

	public String loadCustomerAddBusSegment() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.ModifyReservation.ADD_BUS_SEGMENT_V3;
		} else {
			forward = StrutsConstants.Jsp.ModifyReservation.ADD_BUS_SEGMENT;
		}
		return forward;
	}

	public String loadActivateLoyaltyAccount() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.Customer.ACTIVATE_LOYALTY_ACCOUNT_V3;
		} else {
			forward = StrutsConstants.Jsp.Customer.ACTIVATE_LOYALTY_ACCOUNT;
		}
		return forward;
	}

	public String loadBalanceSummary() {
		String forward = null;
		if (SessionUtil.isNewDesignVersion(request)) {
			forward = StrutsConstants.Jsp.ModifyReservation.BALANCE_SUMMARY_V3;
		} else {
			forward = StrutsConstants.Jsp.ModifyReservation.BALANCE_SUMMARY;
		}
		return forward;
	}

	/**
	 * Validate User
	 * 
	 * @return
	 */
	private boolean isValidateRegisterUser() {
		boolean isRegUser = false;
		int customerID = SessionUtil.getCustomerId(request);

		if (customerID >= 0) {
			isRegUser = true;
		}
		return isRegUser;
	}

	public LmsMemberDTO getLmsDetails() {
		return lmsDetails;
	}

	public void setLmsDetails(LmsMemberDTO lmsDetails) {
		this.lmsDetails = lmsDetails;
	}

}
