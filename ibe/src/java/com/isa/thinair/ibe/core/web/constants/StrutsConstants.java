package com.isa.thinair.ibe.core.web.constants;

public class StrutsConstants {
	public interface Namespace {
		public String PUBLIC = "/public";
		public static final String PRIVATE = "/private";
	}

	public interface Result {
		public String WIRECARD_SUCCESS = "wirecardsuccess";
		public String SUCCESS = "success";
		public String PAYMENT = "payment";
		public String ERROR = "error";
		public String SESSION_EXPIRED = "sessionExpired";
		public String INTERNAL_EXTERNAL_3DSECURE = "banks3DSPage";
		public String LANDING_3DSECURE = "landingBanks3DSPage";
		public String LANDING_VOUCHER_3DSECURE = "landingBanksVoucher3DSPage";
		public String LANDING_BACKBUTTON_HANDLE = "landingBackButtonHandle";
		public String FARE_EXPIRED = "fareExpired";
		public String BYPASS = "bypass";
		public String CARD_PAYMENT_ERROR = "cardPaymentError";
		public String OFFLINE_PAYMENT_INFO = "offlinePaymentInformation";
		public String OFFLINE_PAYMENT_PAYFORT = "offlinePaymentPayFort";
		public String OFFLINE_PAYMENT_PAYFORT_VOUCHER = "offlinePaymentPayFortVoucher";
		public String VOUCHER_AVAILABLE = "voucherAvailable";
		public String REDIRECTSECURE3DJSON = "redirectSecure3Djson";
		public String AEROMART_PAY_RESPONSE = "aeroMartPayResponse";
		public String AEROMART_PAY_OPTIONS = "aeroMartPayOptions";
		public String AEROMART_PAY_IPG_REDIRECTION = "aeroMartIPGRedirection";
		public String LOAD_AEROMARTPAY_PAYMENT_OPTIONS = "loadPaymentOptions";
		public String AEROMART_PAY_EXT_IPG_REDIRECTION = "aeroMartExternalIPGRedirection";

	}

	public interface Jsp {
		public interface Common {
			public String ERROR = "/WEB-INF/jsp/v2/common/Error.jsp";
			public String SESSION_EXPIRED = "/WEB-INF/jsp/v2/common/sessionExpired.jsp";
			public String CREDITCARD_DETAILS = "/WEB-INF/jsp/v2/common/creditCardDetail.jsp";
			public String INTERNAL_EXTERNAL_3DSECURE = "/WEB-INF/jsp/v2/common/3Dsecure.jsp";
			public String BACKBUTTON_HANDLE_ERRORPAGE = "/WEB-INF/jsp/v2/common/landingBackButtonErrorPage.jsp";
			public String AEROMART_PAY_RESPONSE = "/WEB-INF/jsp/v2/common/aeroMartPayResponse.jsp";
			public String AEROMART_PAY_OPTIONS = "/WEB-INF/jsp/v2/common/aeroMartPayOptions.jsp";
			public String AEROMART_PAY_IPG_REDIRECTION = "/WEB-INF/jsp/v2/common/aeroMartPayIPGRedirection.jsp";
			public String LOAD_AEROMARTPAY_PAYMENT_OPTIONS = "/WEB-INF/jsp/v2/common/loadAeroMartPayPaymentOptions.jsp";
			public String AEROMART_PAY_EXT_IPG_REDIRECTION = "/WEB-INF/jsp/v2/common/aeroMartExternalIPGRedirection.jsp";

			public String PAYMENT_SUCEESS = "/WEB-INF/jsp/common/PaymentSuccess.jsp";
			public String TERMS = "/WEB-INF/jsp/common/Terms.jsp";
			public String LOADING_MESSAGE = "/WEB-INF/jsp/v2/common/pageLoading.jsp";
			public String TRACKING = "/WEB-INF/jsp/v2/common/tracking.jsp";
			public String NO_TRACKING = "/WEB-INF/jsp/v2/common/no_tracking.jsp";
			public String REDIRECT = "/WEB-INF/jsp/v2/common/redirect.jsp";
			public String PRINT_VOUCHER = "/WEB-INF/jsp/v3/voucher/printVoucher.jsp";
			public String VOUCHER_PAYMENT_SUCCESS = "/WEB-INF/jsp/v3/voucher/giftVoucherPaymentSuccess.jsp";

		}

		public interface Customer {
			// V2
			public String LOGIN = "/WEB-INF/jsp/v2/customer/customerLogin.jsp";
			public String FORGOTPWD = "/WEB-INF/jsp/v2/customer/customerForgotPWD.jsp";
			public String HOME = "/WEB-INF/jsp/v2/customer/customerHome.jsp";
			public String REGISTER = "/WEB-INF/jsp/v2/customer/registerCustomer.jsp";
			public String CONFIRM_CUSTOMERV2 = "/WEB-INF/jsp/v2/customer/confirmCustomer.jsp";
			public String RESERVATION_DETAIL = "/WEB-INF/jsp/v2/customer/reservationDetail.jsp";
			public String RESERVATION_LIST = "/WEB-INF/jsp/v2/customer/reservationList.jsp";
			public String RESERVATION_CREDIT = "/WEB-INF/jsp/v2/customer/customerCredit.jsp";
			public String RESERVATION_HISTORY = "/WEB-INF/jsp/v2/customer/customerReservationHistory.jsp";
			public String RESERVATION_CUSTOMERPROFILE = "/WEB-INF/jsp/v2/customer/customerProfileUpdate.jsp";
			public String ACTIVATE_LOYALTY_ACCOUNT = "/WEB-INF/jsp/v2/customer/activateLoyaltyAccount.jsp";
			// V3
			public String LOGIN_V3 = "/WEB-INF/jsp/v3/customer/customerLogin.jsp";
			public String HOME_V3 = "/WEB-INF/jsp/v3/customer/customerHome.jsp";
			public String REGISTER_V3 = "/WEB-INF/jsp/v3/customer/registerCustomer.jsp";
			public String ACTIVATE_LOYALTY_ACCOUNT_V3 = "/WEB-INF/jsp/v3/customer/activateLoyaltyAccount.jsp";
			public String RESERVATION_CUSTOMERPROFILE_V3 = "/WEB-INF/jsp/v3/customer/customerProfileUpdate.jsp";
			public String RESERVATION_HISTORY_V3 = "/WEB-INF/jsp/v3/customer/customerReservationHistory.jsp";
			public String RESERVATION_CREDIT_V3 = "/WEB-INF/jsp/v3/customer/customerCredit.jsp";
			public String RESERVATION_LIST_V3 = "/WEB-INF/jsp/v3/customer/reservationList.jsp";
			public String RESERVATION_DETAIL_V3 = "/WEB-INF/jsp/v3/customer/reservationDetail.jsp";
			public String FORGOTPWD_V3 = "/WEB-INF/jsp/v3/customer/customerForgotPWD.jsp";
			public String LMSDETAILS_V3 = "/WEB-INF/jsp/v3/customer/lmsDetails.jsp";
			public String LMS_CROSS_PORTAL_V3 = "/WEB-INF/jsp/v3/customer/LMSCrossProtal.jsp";
			public String MOBILE_REGISTER_V3 = "/WEB-INF/jsp/v3/customer/registerMobileCustomer.jsp";
			public String SELF_REPROTECTION = "/WEB-INF/jsp/v2/customer/selfReprotect.jsp";
			public String SELF_REPROTECTION_ERROR = "/WEB-INF/jsp/v2/customer/SelfReprotectError.jsp";
		}

		/**
		 * IBE - new screen to register travel agents. AARESGDS-128
		 * 
		 * @author sanjeewaf
		 * 
		 */
		public interface Agent {

			public String AGENTS_INFO = "/WEB-INF/jsp/agent/agentsInformation.jsp";

			// V2
			public String REGISTER = "/WEB-INF/jsp/v2/agent/registerAgent.jsp";
			// V3
			public String REGISTER_V3 = "/WEB-INF/jsp/v3/agent/registerAgent.jsp";

		}

		public interface Reservation {
			public String FLIGHT_SCHEDULE = "/WEB-INF/jsp/reservation/flightSchedule.jsp";
			public String V2_PRINTITINERARY = "/WEB-INF/jsp/v2/reservation/printItinerary.jsp";
		}

		public interface ModifyReservation {
			public String MODIFY_SEGMENT = "/WEB-INF/jsp/v2/modifyRes/modifySegment.jsp";
			public String BALANCE_SUMMARY = "/WEB-INF/jsp/v2/modifyRes/balanceSummary.jsp";
			public String LOAD_REDIRECTPAGE = "/WEB-INF/jsp/v2/modifyRes/redirectPage.jsp";
			public String ADD_BUS_SEGMENT = "/WEB-INF/jsp/v2/modifyRes/addBusSegment.jsp";

			// V3
			public String BALANCE_SUMMARY_V3 = "/WEB-INF/jsp/v3/modifyRes/balanceSummary.jsp";
			public String MODIFY_SEGMENT_V3 = "/WEB-INF/jsp/v3/modifyRes/modifySegment.jsp";
			public String ADD_BUS_SEGMENT_V3 = "/WEB-INF/jsp/v3/modifyRes/addBusSegment.jsp";
		}

		public interface Respro {
			public String INTERLINE_CONTAINER = "/WEB-INF/jsp/v2/reservation/reservationLnContainer.jsp";
			public String INTERLINE_CONTAINER_V3 = "/WEB-INF/jsp/v3/reservation/reservationLnContainer.jsp";
			public String INTERLINE_PAX = "/WEB-INF/jsp/v2/reservation/reservationInteLnPassenger.jsp";
			public String INTERLINE_ANCI = "/WEB-INF/jsp/v2/reservation/reservationInteLnAnci.jsp";
			public String INTERLINE_PAYMENT = "/WEB-INF/jsp/v2/reservation/reservationInteLnPayment.jsp";
			public String INTERLINE_CONFIRM = "/WEB-INF/jsp/v2/reservation/reservationInteLnConfirm.jsp";
			public String DISPLAY_MEALS = "/WEB-INF/jsp/respro/displayMeals.jsp";
			public String INTERLINE_LOAD_RES = "/WEB-INF/jsp/v2/customer/customerReservation.jsp";
			public String COMPARE_FARE = "/WEB-INF/jsp/v2/common/compareFare.jsp";
			// page switcher
			public String MIDDEL_PAGE_LOADER = "/WEB-INF/jsp/v2/reservation/preLoader.jsp";

			public String INTERLINE_LOAD_FARE = "/WEB-INF/jsp/v2/reservation/availabilitySearch.jsp";
			public String INTERLINE_LOAD_FARE_V3 = "/WEB-INF/jsp/v3/reservation/availabilitySearch.jsp";
			public String MANAGE_BOOKING = "/WEB-INF/jsp/v2/modifyRes/manageBooking.jsp";
			public String KIOSK_ONHOLD_CONFIRMATION = "/WEB-INF/jsp/v2/kiosk/kioskOHDConfirm.jsp";
			public String IBE_ONHOLD_CONFIRMATION = "/WEB-INF/jsp/v2/reservation/OHDConfirm.jsp";

			public String INTERLINE_LOAD_FLIGHTS_REQUOTE = "/WEB-INF/jsp/v2/reservation/availabilitySearchForRequote.jsp";
			public String INTERLINE_LOAD_FLIGHTS_REQUOTE_V3 = "/WEB-INF/jsp/v3/reservation/availabilitySearchForRequote.jsp";
			// V3
			public String MANAGE_BOOKING_V3 = "/WEB-INF/jsp/v3/modifyRes/manageBooking.jsp";
			public String IBE_ONHOLD_CONFIRMATION_V3 = "/WEB-INF/jsp/v3/reservation/OHDConfirm.jsp";
			public String INTERLINE_CONFIRM_V3 = "/WEB-INF/jsp/v3/reservation/reservationInteLnConfirm.jsp";
			public String INTERLINE_MULTICITY = "/WEB-INF/jsp/v2/reservation/mcAvailabilitySearch.jsp";
			public String INTERLINE_MULTICITY_V3 = "/WEB-INF/jsp/v3/reservation/mcAvailabilitySearch.jsp";
			public String IBE_ONHOLD_CONFIRMATION_PAY_AT_HOME = "/WEB-INF/jsp/v3/reservation/OHDConfirmPayAtHome.jsp";
		}

		public interface Payment {
			public String WIRECARD_INTERLINE_CONTAINER = "/WEB-INF/jsp/v2/reservation/wirecardReservationLnContainer.jsp";
			public String POST_INPUT_DATA = "/WEB-INF/jsp/payment/PostInputData.jsp";
			public String CARD_PAYMENT_ERROR = "/WEB-INF/jsp/v2/common/CardPaymentError.jsp";
			public String OFFLINE_PAYMENT_INFO = "/WEB-INF/jsp/v2/common/offlinePaymentInfo.jsp";
			public String OFFLINE_PAYMENT_PAYFORT = "/WEB-INF/jsp/v2/common/offlinePaymentPayFort.jsp";
			public String OFFLINE_PAYMENT_PAYFORT_VOUCHER = "/WEB-INF/jsp/v2/common/PayFortVoucherDisplay.jsp";
			// V2
			public String LANDING3DSECUREPAGE = "/WEB-INF/jsp/v2/common/landing3Dsecure.jsp";
			public String LANDING_VOUCHER_3D_SECURE_PAGE = "/WEB-INF/jsp/v2/common/landingVoucher3DSecure.jsp";
		}

		public interface Kiosk {
			public String LOGIN = "/WEB-INF/jsp/v2/kiosk/login.jsp";
			public String WELCOME_KIOSK = "/WEB-INF/jsp/v2/kiosk/welcomeUser.jsp";
			// V3
			public String LOGIN_V3 = "/WEB-INF/jsp/v3/kiosk/login.jsp";
			public String WELCOME_KIOSK_V3 = "/WEB-INF/jsp/v3/kiosk/welcomeUser.jsp";
		}

		public interface Insurance {
			public String CANCELATION = "/ext_html/insurance/ANNULATION_WEB_fr.pdf";
			public String MULTIRISK = "/ext_html/insurance/MULTIRISQUE_WEB_fr.pdf";
			public String CANCELATION_EN = "/ext_html/insurance/ANNULATION_WEB_en.pdf";
			public String MULTIRISK_EN = "/ext_html/insurance/MULTIRISQUE_WEB_en.pdf";
		}

		public interface Promotion {
			public String PROMOTION_REGISTRATION = "/WEB-INF/jsp/v3/promotion/promotionRegistration.jsp";
			public String PROMOTION_REGISTRATION_REDIRECT_PAGE = "/WEB-INF/jsp/v3/promotion/promotionRedirectPage.jsp";
			public String PROMOTION_REGISTRATION_CONFIRM = "/WEB-INF/jsp/v3/promotion/promotionRegistrationConfirm.jsp";
			public String LMS_CONFIRMATION_PAGE = "/WEB-INF/jsp/v3/promotion/lmsEmailConfirm.jsp";
		}

		/**
		 * IBE - new screen to sell gift vouchers AARESAA-21284
		 * 
		 * @author chanaka
		 * 
		 */
		public interface Voucher {
			String GIFT_VOUCHER = "/WEB-INF/jsp/v3/voucher/giftVoucher.jsp";

		}
	}

	public interface Action {
		String IBE_PAYMENT = "showPayment";
		String SAVE_INTERLINE_RESERVATION = "interLineConfirmation";
		String HANDLE_INTERLINE_IPG_RESPONSE = "handleInterlineIPGResponse";
		String HANDLE_VOUCHER_IPG_RESPONSE = "handleVoucherIPGResponse";
		String QIWI_INVOICE_NOTIFICATION_HANDLER = "qiwiInvoiceNotificationHandler";
		String SESSION_EXPIRED = "sessionExpiredNonExistanceAction";
		String SAVE_PROMOTION_REQUESTS = "promotionRegisterConfirmation";
		String LOAD_CONTAINER = "loadContainer";
		String REDIRECT_TO_CUSTOMER_LOGIN = "customerLogin";
		String MULTICITY_ON_PREVIOUS_CLK = "showReservation";
		String SHOWCUSTOMERLOADPAGE = "showCustomerLoadPage";
		String SHOWRESERVATION = "showReservation";
		String SHOW_CUSTOMER_LOAD_PAGE = "showCustomerLoadPage";
		String SAVE_GIFT_VOUCHER = "issueVoucherConfirmation";
		String SELF_REPROTECTION = "showSelfReprotection";		
		String INTERLINE_PAYMENT = "interlinePayment";
		String PAY_AT_STORE_BACK_TO_MERCHANT = "handleInterlinePayFort";
		String AEROMART_PAY_BYPASS = "handleAeroMartPayIPGResponse";
		String LOAD_AEROMARTPAY_OPTIONS = "loadAeroMartPayOptions";

	}

	public interface StrutsAction {
		String LOAD_CONTAINER = "loadContainer.action";
	}
}