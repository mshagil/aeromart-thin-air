package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.json.simple.JSONObject;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO.JourneyType;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.AirportMessageDisplayUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightFareSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.converters.AncillaryConverterUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.FareCategoryTO.FareCategoryStatusCodes;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.AvailableFlightInfo;
import com.isa.thinair.ibe.api.dto.FareQuoteTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.v2.FareRuleDisplayTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.CustomerWebConstants;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.CommonUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;

/**
 * @author M.Rikaz
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "")

})
public class FareRequoteAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(FareRequoteAction.class);

	private boolean success = true;

	private String messageTxt;

	private ArrayList<String> outFlightRPHList = new ArrayList<String>();

	private ArrayList<String> retFlightRPHList = new ArrayList<String>();

	private FareQuoteTO fareQuote;

	private String pnr;

	private boolean groupPNR;

	private String oldAllSegments;

	private String modifySegmentRefNos;

	private String airportMessage;

	private boolean isOutBoundFlexiAvailable = false;

	private boolean isInBoundFlexiAvailable = false;

	private boolean modifySegment;

	private boolean cancelSegment;

	private boolean nameChange;
	
	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount;

	private String oldFareCarrierCode;

	private String outBoundFlexiMessage;

	private String inBoundFlexiMessage;

	private boolean forceRefresh = false;

	private BigDecimal outboundCalFare;

	private BigDecimal inboundCalFare;

	private List<FareRuleDisplayTO> fareRules;

	private boolean showFareRules;

	private boolean fromPostCardPage;

	private String msgFromPostCardPage;

	private String requestSessionIdentifier;

	private boolean allowOnhold;

	private boolean requoteFlightSearch;

	private Collection<AvailableFlightInfo> flightInfo;

	private List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList = new ArrayList<List<OndClassOfServiceSummeryTO>>();

	private List<String> ondWiseTotalFlexiCharge = new ArrayList<String>();

	private List<Boolean> ondWiseTotalFlexiAvailable = new ArrayList<Boolean>();
	
	private JSONObject searchParamsCookieDetails = new JSONObject();

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		boolean isCabinClassOnholdEnable = false;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			String tempPaymentPNR = SessionUtil.getTempPaymentPNR(request);
			Locale locale = new Locale(SessionUtil.getLanguage(request));
			if (SessionUtil.isResponseReceivedFromPaymentGateway(request) && tempPaymentPNR != null
					&& !tempPaymentPNR.trim().isEmpty()) {
				messageTxt = CommonUtil.getExistingReservationMsg(request, tempPaymentPNR);
				success = false;
				return result;
			}

			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (resInfo != null && resInfo.isFromPostCardDetails()) {
				fromPostCardPage = resInfo.isFromPostCardDetails();
				msgFromPostCardPage = I18NUtil.getMessage("msg.postCardPage", strLanguage);
			}

			String searchSystem = getSearchParams().getSearchSystem();
			SYSTEM system = SYSTEM.getEnum(searchSystem);

			if ((system == null || system == SYSTEM.NONE) && resInfo != null && resInfo.getSelectedSystem() != null) {
				system = resInfo.getSelectedSystem();
			}

			if (system == SYSTEM.NONE) {
				throw new ModuleRuntimeException("Invalid Seach System :" + searchSystem);
			}

			FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
			// FIXME avoid creating AvailableFlightSearchDTOBuilder and directly create the flightPriceRQ
			// Current implementation leads unnecessary DTO transformation step
			AvailableFlightSearchDTOBuilder fareQuteParams = SearchUtil.getSearchBuilder(getSearchParams());
			if (modifySegment && !groupPNR) {
				SearchUtil.setModifingSegmentSearchData(fareQuteParams, oldFareID, oldFareType, oldFareAmount);
			}

			flightPriceRQ.getAvailPreferences().setRequoteFlightSearch(true);

			// Set Half Return Fare
			String fromAirport = fareQuteParams.getSearchDTO().getFromAirport();
			String toAirport = fareQuteParams.getSearchDTO().getToAirport();
			boolean isFromOpByOwn = ModuleServiceLocator.getAirportBD().isAirportOptByOwn(fromAirport);
			boolean isToOptByOwn = ModuleServiceLocator.getAirportBD().isAirportOptByOwn(toAirport);
			if (isFromOpByOwn && isToOptByOwn) {
				if (pnr != null && !"".equals(pnr) && !groupPNR) {
					SearchUtil.setExistingSegInfo(fareQuteParams.getSearchDTO(), oldAllSegments, modifySegmentRefNos);
				} else if ((fareQuteParams.getSearchDTO().isReturnFlag() && !fareQuteParams.getSearchDTO().isOpenReturnSearch())
						|| (fareQuteParams.getSearchDTO().isOpenReturnSearch() && fareQuteParams.getSearchDTO().getJourneyType() == JourneyType.OPEN_JAW)) {
					boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
					fareQuteParams.getSearchDTO().setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
				}
			}

			flightPriceRQ = BeanUtil.createFareReQuoteRQ(getSearchParams(), nameChange);

			if (pnr != null) {
				flightPriceRQ.getAvailPreferences().setPnr(pnr);
			}

			resInfo.setExcludedSegFarePnrSegIds(getSearchParams().getExcludedSegFarePnrSegIds());

			if (pnr != null && !"".equals(pnr)) {
				flightPriceRQ.getAvailPreferences().setModifyBooking(true);
				flightPriceRQ.getAvailPreferences().setOldFareID(oldFareID);
				flightPriceRQ.getAvailPreferences().setOldFareType(oldFareType);
				flightPriceRQ.getAvailPreferences().setOldFareAmount(new BigDecimal(oldFareAmount));
				flightPriceRQ.getAvailPreferences().setModifiedSegmentsList(
						fareQuteParams.getSearchDTO().getModifiedFlightSegments());
			}
			flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());

			flightPriceRQ.getAvailPreferences().setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
			flightPriceRQ.getAvailPreferences().setOldFareCarrierCode(oldFareCarrierCode);

			if (groupPNR) {
				system = SYSTEM.INT;
			}

			flightPriceRQ.getAvailPreferences().setSearchSystem(system);

			AnalyticsLogger.logRequoteSearch(AnalyticSource.IBE_REQUOTE, flightPriceRQ, request, pnr, getTrackInfo());

			FlightPriceRS flightPriceRS = ModuleServiceLocator.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ,
					getTrackInfo());

			String accessPoint = (String) request.getSession().getAttribute(CustomerWebConstants.SYS_ACCESS_POINT);

			resInfo.addNewServiceTaxes(flightPriceRS.getApplicableServiceTaxes());

			// TODO Re-factor
			if ((!CustomerWebConstants.SYS_ACCESS_POINT_KSK.equals(accessPoint))) {
				isCabinClassOnholdEnable = ReservationUtil.isReservationCabinClassOnholdable(flightPriceRS);
				resInfo.setAllowOnhold(isCabinClassOnholdEnable);

				if (isCabinClassOnholdEnable && AppSysParamsUtil.isOnHoldEnable(OnHold.IBE)) {
					boolean reservationOtherValidationOnholdable = ReservationUtil.isReservationOtherValidationOnholdable(
							flightPriceRS, system, getTrackInfo(), getClientInfoDTO().getIpAddress());
					resInfo.setReservationOnholdableOtherValidation(reservationOtherValidationOnholdable);

					allowOnhold = isCabinClassOnholdEnable && reservationOtherValidationOnholdable;

					if (allowOnhold) {
						for (OriginDestinationInformationTO ondInfo : flightPriceRS.getOriginDestinationInformationList()) {
							Date depTimeZulu = BeanUtil.getSelectedFlightDepartureDate(ondInfo.getOrignDestinationOptions());
							boolean ohdEnabled = (depTimeZulu != null) ? ReservationUtil.isOnHoldPaymentEnable(OnHold.IBE,
									depTimeZulu) : false;
							if (!(allowOnhold && ohdEnabled)) {
								allowOnhold = allowOnhold && ohdEnabled;
								break;
							}
							allowOnhold = allowOnhold && ohdEnabled;
						}
					}
				}
			}

			FlightFareSummaryTO outboundFlightFareSummaryTO = new FlightFareSummaryTO();
			FlightFareSummaryTO inboundFlightFareSummaryTO = new FlightFareSummaryTO();
			boolean machingResponceFound = true;
			if (outFlightRPHList != null && (!cancelSegment || fareQuote != null)) {
				for (String outFlt : outFlightRPHList) {
					if (!outFlt.trim().isEmpty()) {
						if (outFlt.indexOf("#") != -1) {
							outFlt = outFlt.split("#")[0];
						}

						if (outFlt.indexOf("-") != -1) {
							outFlt = outFlt.split("-")[0];
						}

						boolean ondMatched = false;

						for (OriginDestinationInformationTO priceRSOnDInfo : flightPriceRS.getOriginDestinationInformationList()) {
							for (OriginDestinationOptionTO originDestOpt : priceRSOnDInfo.getOrignDestinationOptions()) {
								for (FlightSegmentTO flightsegs : originDestOpt.getFlightSegmentList()) {
									if (outFlt.equalsIgnoreCase(flightsegs.getFlightRefNumber())) {
										ondMatched = true;
										outboundFlightFareSummaryTO = originDestOpt.getSelectedFlightFareSummary();
										break;
									}
								}
							}
						}
						machingResponceFound = ondMatched && machingResponceFound;
						if (!machingResponceFound) {
							forceRefresh = true;
							// TODO refactor this to a constant file
							throw new ModuleException("ibe.fare.quote.flights.not.matching");
						}
					}
				}
			}

			// else if (cancelSegment && fareQuote == null) {
			// fareQuote = new FareQuoteTO();
			// }

			// if (retFlightRPHList != null) {
			// for (String inFlt : retFlightRPHList) {
			// if (!inFlt.trim().isEmpty()) {
			// if (inFlt.indexOf("#") != -1) {
			// inFlt = inFlt.split("#")[0];
			// }
			// boolean ondMatched = false;
			//
			// for (OriginDestinationInformationTO priceRSOnDInfo : flightPriceRS.getOriginDestinationInformationList())
			// {
			// for (OriginDestinationOptionTO originDestOpt : priceRSOnDInfo.getOrignDestinationOptions()) {
			// for (FlightSegmentTO flightsegs : originDestOpt.getFlightSegmentList()) {
			// if (inFlt.equalsIgnoreCase(flightsegs.getFlightRefNumber())) {
			// ondMatched = true;
			// inboundFlightFareSummaryTO = originDestOpt.getSelectedFlightFareSummary();
			// break;
			// }
			// }
			// }
			// }
			// machingResponceFound = ondMatched && machingResponceFound;
			// if (!machingResponceFound) {
			// forceRefresh = true;
			// // TODO refactor this to a constant file
			// throw new ModuleException("ibe.fare.quote.flights.not.matching");
			// }
			// }
			// }
			// }

			flightInfo = BeanUtil.createAvailFlightInfoList(flightPriceRS, false, locale);

			PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();
			if (priceInfoTO != null) {
				String selCurr = getSearchParams().getSelectedCurrency();
				if (!AppSysParamsUtil.showCalFareInSelectedCurrency()) {
					selCurr = AppSysParamsUtil.getBaseCurrency();
				}

				ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
				CurrencyExchangeRate exchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurr, ApplicationEngine.IBE);
				// Total price - Fare + SUR + TAX for one adult
				if (AppSysParamsUtil.showTotalFareInCalendar()) {
					outboundCalFare = (outboundFlightFareSummaryTO != null) ? outboundFlightFareSummaryTO.getTotalPrice() : null;
					inboundCalFare = (inboundFlightFareSummaryTO != null) ? inboundFlightFareSummaryTO.getTotalPrice() : null;
				} else {
					outboundCalFare = (outboundFlightFareSummaryTO != null) ? outboundFlightFareSummaryTO.getBaseFareAmount()
							: null;
					inboundCalFare = (inboundFlightFareSummaryTO != null) ? inboundFlightFareSummaryTO.getBaseFareAmount() : null;
				}

				Currency currency = exchangeRate.getCurrency();
				boolean isCurrencyRoundUpEnabled = AppSysParamsUtil.isCurrencyRoundupEnabled();
				BigDecimal boundry = BigDecimal.ZERO;
				BigDecimal breakpoint = BigDecimal.ZERO;
				if (isCurrencyRoundUpEnabled) {
					boundry = (currency.getBoundryValue() != null) ? currency.getBoundryValue() : AppSysParamsUtil
							.getIBECalBoundry();
					breakpoint = (currency.getBreakPoint() != null) ? currency.getBreakPoint() : AppSysParamsUtil
							.getIBECalBreakpoint();
				}

				if (outboundCalFare != null) {
					// outboundCalFare = new
					// BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
					// outboundCalFare, exchangeRate.getMultiplyingExchangeRate())));
					// outboundCalFare = AccelAeroRounderPolicy.getRoundedValue(outboundCalFare,
					// AppSysParamsUtil.getIBECalBoundry(), AppSysParamsUtil.getIBECalBreakpoint());

					if (isCurrencyRoundUpEnabled) {
						outboundCalFare = AccelAeroRounderPolicy.convertAndRound(outboundCalFare,
								exchangeRate.getMultiplyingExchangeRate(), boundry, breakpoint);
					} else {
						outboundCalFare = AccelAeroCalculator
								.multiply(outboundCalFare, exchangeRate.getMultiplyingExchangeRate());
						outboundCalFare = AccelAeroRounderPolicy.getRoundedValue(outboundCalFare,
								AppSysParamsUtil.getIBECalBoundry(), AppSysParamsUtil.getIBECalBreakpoint());
					}
				}
				if (inboundCalFare != null) {
					// inboundCalFare = new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
					// inboundCalFare, exchangeRate.getMultiplyingExchangeRate())));
					// inboundCalFare = AccelAeroRounderPolicy.getRoundedValue(inboundCalFare,
					// AppSysParamsUtil.getIBECalBoundry(),
					// AppSysParamsUtil.getIBECalBreakpoint());

					if (isCurrencyRoundUpEnabled) {
						inboundCalFare = AccelAeroRounderPolicy.convertAndRound(inboundCalFare,
								exchangeRate.getMultiplyingExchangeRate(), boundry, breakpoint);
					} else {
						inboundCalFare = AccelAeroCalculator.multiply(inboundCalFare, exchangeRate.getMultiplyingExchangeRate());
						inboundCalFare = AccelAeroRounderPolicy.getRoundedValue(inboundCalFare,
								AppSysParamsUtil.getIBECalBoundry(), AppSysParamsUtil.getIBECalBreakpoint());
					}
				}

				// re-quote
				fareQuote = BeanUtil.buildFareTypeInfo(priceInfoTO.getFareTypeTO(),
						flightPriceRS.getOriginDestinationInformationList(), BigDecimal.ZERO, getSearchParams(), BigDecimal.ZERO,
						exchangeRateProxy, flightInfo);

				resInfo.setTotalPriceWithoutExtChg(BeanUtil.getTotalPriceWithoutExtCharges(priceInfoTO));
				resInfo.setTotalFare(fareQuote.getTotalFare());
				resInfo.setTotalTax(fareQuote.getTotalTax());
				resInfo.setTotalTaxSurchages(fareQuote.getTotalTaxSurcharge());
				resInfo.setPriceInfoTO(priceInfoTO);
				resInfo.setTransactionId(flightPriceRS.getTransactionIdentifier());

				// Request Session Identifier
				requestSessionIdentifier = (new Date().getTime()) + "";
				SessionUtil.setRequestSessionIdentifier(request, requestSessionIdentifier);

				List<FareRuleDTO> ondFareRules = BeanUtil.getFareRules(flightPriceRS.getSelectedPriceFlightInfo(),
						flightPriceRS.getOriginDestinationInformationList());
				GlobalConfig globalConfig = new GlobalConfig();
				Map<String, FareCategoryTO> sortedFareCategoryTOMap = globalConfig.getFareCategories();
				fareRules = new ArrayList<FareRuleDisplayTO>();

				for (FareRuleDTO fareRule : ondFareRules) {
					FareCategoryTO fareCategoryTO = sortedFareCategoryTOMap.get(fareRule.getFareCategoryCode());
					if (fareCategoryTO != null && fareCategoryTO.getStatus().equals(FareCategoryStatusCodes.ACT)) {
						showFareRules = (showFareRules || fareCategoryTO.getShowComments());
						if (fareCategoryTO.getShowComments()) {
							FareRuleDisplayTO frDis = new FareRuleDisplayTO();
							frDis.setOndCode(fareRule.getOrignNDest());
							frDis.setComment(fareRule.getComments());
							frDis.setBookingClass(fareRule.getBookingClassCode());
							fareRules.add(frDis);
						}
					}
				}

				List<OndClassOfServiceSummeryTO> allLogicalCCList = priceInfoTO.getAvailableLogicalCCList();
				ReservationBeanUtil.populateOndLogicalCCAvailability(allLogicalCCList, ondLogicalCCList);

				ondWiseTotalFlexiCharge = ReservationBeanUtil.getONDWiseFlexiCharges(priceInfoTO.getFareTypeTO());
				ondWiseTotalFlexiAvailable = ReservationBeanUtil
						.getONDWiseFlexiAvailable(priceInfoTO.getAvailableLogicalCCList());

				resInfo.setBaggageSummaryTo(AncillaryConverterUtil.toLccReservationBaggageSummaryTo(flightPriceRS));

			} else if (!cancelSegment) {
				forceRefresh = true;
			}

			// Load Airport Messages
			this.airportMessage = AirportMessageDisplayUtil.getAirportMessages(
					flightPriceRS.getOriginDestinationInformationList(),
					ReservationInternalConstants.AirportMessageSalesChannel.IBE,
					ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT, SessionUtil.getLanguage(request));
			
//			if (AppSysParamsUtil.isCookieSaveFunctionalityEnabled() && !cancelSegment) {
//				this.searchParamsCookieDetails = ReservationUtil.getSearchParamCookie(getSearchParams(), fareQuote, flightInfo);
//			}

		} catch (ModuleException me) {
			log.error(me.getModuleCode(), me);
			success = false;
			log.error("FareRequoteAction ==>", me);
			if (me.getExceptionCode().equals("ibe.fare.quote.flights.not.matching")) {
				messageTxt = "Returned flights not matching with fare quoted flights";
			} else if (me.getExceptionCode().equals("err.58-maxico.exposed.cannot.mix.agreements.in.onefare.quote")) {
				messageTxt = MessagesUtil.getMessage(me.getExceptionCode());
			} else if (me.getExceptionCode().equals("err.60-maxico.exposed.no.fares.detected")) {
				messageTxt = "Fares no longer available for the selected flight(s).";
				forceRefresh = true;
			} else if (me.getExceptionCode().equals("airinventory.arg.farequote.flightclosed")) {
				forceRefresh = true;
				messageTxt = "flightsHaveChanged";
			} else {
				messageTxt = me.getMessage();
			}

		} catch (Exception ex) {
			log.error("FareRequoteAction ==>", ex);
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
		}
		return result;
	}

	public boolean isForceRefresh() {
		return forceRefresh;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public FareQuoteTO getFareQuote() {
		return fareQuote;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setModifySegmentRefNos(String modifySegmentRefNos) {
		this.modifySegmentRefNos = modifySegmentRefNos;
	}

	public String getAirportMessage() {
		return airportMessage;
	}

	public void setAirportMessage(String airportMessage) {
		this.airportMessage = airportMessage;
	}

	public boolean isOutBoundFlexiAvailable() {
		return isOutBoundFlexiAvailable;
	}

	public void setOutBoundFlexiAvailable(boolean isOutBoundFlexiAvailable) {
		this.isOutBoundFlexiAvailable = isOutBoundFlexiAvailable;
	}

	public boolean isInBoundFlexiAvailable() {
		return isInBoundFlexiAvailable;
	}

	public void setInBoundFlexiAvailable(boolean isInBoundFlexiAvailable) {
		this.isInBoundFlexiAvailable = isInBoundFlexiAvailable;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public String getOutBoundFlexiMessage() {
		return outBoundFlexiMessage;
	}

	public void setOutBoundFlexiMessage(String outBoundFlexiMessage) {
		this.outBoundFlexiMessage = outBoundFlexiMessage;
	}

	public String getInBoundFlexiMessage() {
		return inBoundFlexiMessage;
	}

	public void setInBoundFlexiMessage(String inBoundFlexiMessage) {
		this.inBoundFlexiMessage = inBoundFlexiMessage;
	}

	public boolean isShowFareRules() {
		return showFareRules;
	}

	public List<FareRuleDisplayTO> getFareRules() {
		return fareRules;
	}

	public BigDecimal getInboundCalFare() {
		return inboundCalFare;
	}

	public BigDecimal getOutboundCalFare() {
		return outboundCalFare;
	}

	public boolean isFromPostCardPage() {
		return fromPostCardPage;
	}

	public String getMsgFromPostCardPage() {
		return msgFromPostCardPage;
	}

	public String getRequestSessionIdentifier() {
		return requestSessionIdentifier;
	}

	public void setOldFareCarrierCode(String oldFareCarrierCode) {
		this.oldFareCarrierCode = oldFareCarrierCode;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAllowOnhold() {
		return allowOnhold;
	}

	public boolean isRequoteFlightSearch() {
		return requoteFlightSearch;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public Collection<AvailableFlightInfo> getFlightInfo() {
		return flightInfo;
	}

	public void setFlightInfo(Collection<AvailableFlightInfo> flightInfo) {
		this.flightInfo = flightInfo;
	}

	public boolean isCancelSegment() {
		return cancelSegment;
	}

	public void setCancelSegment(boolean cancelSegment) {
		this.cancelSegment = cancelSegment;
	}

	public List<List<OndClassOfServiceSummeryTO>> getOndLogicalCCList() {
		return ondLogicalCCList;
	}

	public List<String> getOndWiseTotalFlexiCharge() {
		return ondWiseTotalFlexiCharge;
	}

	public List<Boolean> getOndWiseTotalFlexiAvailable() {
		return ondWiseTotalFlexiAvailable;
	}

	public JSONObject getSearchParamsCookieDetails() {
		return searchParamsCookieDetails;
	}

	public void setSearchParamsCookieDetails(JSONObject searchParamsCookieDetails) {
		this.searchParamsCookieDetails = searchParamsCookieDetails;
	}

    public boolean isNameChange() {
        return nameChange;
    }

    public void setNameChange(boolean nameChange) {
        this.nameChange = nameChange;
    }

}
