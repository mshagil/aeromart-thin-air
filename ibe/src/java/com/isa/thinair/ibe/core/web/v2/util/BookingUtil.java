package com.isa.thinair.ibe.core.web.v2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationPreferrenceInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.api.dto.ContactInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.api.dto.URLTO;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.PaxPaymentUtil;
import com.isa.thinair.webplatform.api.v2.util.PaymentAssemblerComposer;
import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

public class BookingUtil {

	public static CommonReservationAssembler createBookingDetails(IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO,
			PayCurrencyDTO payCurrencyDTO, Collection<ReservationPaxTO> paxList, PriceInfoTO priceInfoTO, boolean onHoldBooking,
			Date paymentTimestamp, Map<EXTERNAL_CHARGES, ExternalChgDTO> selectedExtChgs, boolean  isCCPayment,
			BigDecimal loyaltyPayAmount, String loyaltyAccount, String loyaltyAgentCode,
			Map<String, LoyaltyPaymentInfo> carrierWiseLoyaltyPayments, CommonCreditCardPaymentInfo cardPaymentInfo,
			String brokerType, boolean isPromotionResWithZeroPayment,
			ReservationDiscountDTO resDiscountDTO, PayByVoucherInfo payByVoucherInfo) throws ModuleException {

		CommonReservationAssembler lcclientReservationAssembler = new CommonReservationAssembler();
		lcclientReservationAssembler.setOnHoldBooking(onHoldBooking);

		Map<Integer, BigDecimal> paxWiseRedeemedAmounts = AirProxyReservationUtil
				.getPaxRedeemedTotalMap(carrierWiseLoyaltyPayments);	
		
		String loyaltyMemberAccountId = null;
		String[] rewardIDs = null;

		// Collection<PassengerDTO> infantList = paxDetailDTO.getInfantList();
		int adultChildCount = 0;
		for (ReservationPaxTO pax : paxList) {
			if (PaxTypeTO.ADULT.equals(pax.getPaxType()) || PaxTypeTO.CHILD.equals(pax.getPaxType())
					|| PaxTypeTO.PARENT.equals(pax.getPaxType())) {
				adultChildCount++;
			}
		}
		List<ReservationPaxTO> infantList = new ArrayList<ReservationPaxTO>();
		if (onHoldBooking) {
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.IBE, null)) {
				isCCPayment = true;
			} else {
				isCCPayment = false;
			}
		}
		ExternalChargesMediator externalChargesMediator = new ExternalChargesMediator(paxList, selectedExtChgs, isCCPayment,
				false);
		externalChargesMediator.setCalculateJNTaxForCCCharge(isCCPayment);
		LinkedList perPaxExternalCharges = externalChargesMediator.getExternalChargesForAFreshPayment(adultChildCount, 0);

		BigDecimal voucherPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (payByVoucherInfo != null && payByVoucherInfo.getRedeemedTotal() != null) {
			voucherPaymentAmount = payByVoucherInfo.getRedeemedTotal();
		}

		int intSeq = 1;
		Date currentDatetime = new Date();
		boolean isExtCardPay = true;
		if (brokerType != null
				&& PaymentBrokerBD.PaymentBrokerProperties.BrokerTypes.BROKER_TYPE_INTERNAL_EXTERNAL.equals(brokerType)
				&& cardPaymentInfo != null) {
			isExtCardPay = false;
		}
		Map<Integer, LCCClientPaymentAssembler> adultInfantPaymentAssemblerMap = new HashMap<Integer, LCCClientPaymentAssembler>();
		
		for (ReservationPaxTO reservationPax : paxList) {
			Integer paxSequence = reservationPax.getSeqNumber();
			if (PaxTypeTO.INFANT.equals(reservationPax.getPaxType())) {
				infantList.add(reservationPax);
				continue;
			} else {

				BigDecimal paxPromoDiscountTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (resDiscountDTO != null) {
					PaxDiscountDetailTO paxDiscountDetailTO = resDiscountDTO.getPaxDiscountDetail(reservationPax.getSeqNumber());
					if (paxDiscountDetailTO != null) {
						paxPromoDiscountTotal = paxDiscountDetailTO.getTotalDiscount();
					}

				}		
				
				boolean isParent = reservationPax.getIsParent();

				BigDecimal lmsMemberPayment = paxWiseRedeemedAmounts.get(paxSequence);

				List<LCCClientExternalChgDTO> extCharges = converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxExternalCharges
						.pop());
				extCharges.addAll(reservationPax.getExternalCharges());
				PaymentAssemblerComposer paymentComposer = new PaymentAssemblerComposer(extCharges);
				PaymentAssemblerComposer infantPaymentComposer = new PaymentAssemblerComposer(new ArrayList<LCCClientExternalChgDTO>());

				LCCClientPaymentAssembler lccClientPaymentAssembler = null;
				LCCClientPaymentAssembler infantPaymentAssembler = null;

				if (isPromotionResWithZeroPayment) {
					lccClientPaymentAssembler = new LCCClientPaymentAssembler();
					lccClientPaymentAssembler.addCashPayment(BigDecimal.ZERO, null, new Date(), null, null, null, null);
				} else {
					if (!onHoldBooking) {
						String paxType = reservationPax.getPaxType();
						BigDecimal infantAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (isParent) {
							paxType = PaxTypeTO.ADULT;
							infantAmount = priceInfoTO.getPerPaxPriceInfoTO(PaxTypeTO.INFANT).getPassengerPrice().getTotalPrice();
							infantPaymentAssembler = new LCCClientPaymentAssembler();
							infantPaymentAssembler.setPaxType(PaxTypeTO.INFANT);
						}
						BigDecimal passengerAmount = priceInfoTO.getPerPaxPriceInfoTO(paxType).getPassengerPrice()
								.getTotalPrice();

						BigDecimal totalPaymentAmount = AccelAeroCalculator.add(passengerAmount, infantAmount);

						BigDecimal totalWithAncillary = AccelAeroCalculator
								.add(totalPaymentAmount, getTotalExtCharge(extCharges));

						if (totalWithAncillary.compareTo(BigDecimal.ZERO) > 0) {

							BigDecimal totalExtChgs = BookingUtil.getTotalExtCharge(extCharges);
							if (paxPromoDiscountTotal != null && paxPromoDiscountTotal.compareTo(BigDecimal.ZERO) > 0
									&& totalWithAncillary.compareTo(paxPromoDiscountTotal) >= 0) {
								totalWithAncillary = AccelAeroCalculator.subtract(totalWithAncillary, paxPromoDiscountTotal);
							}
							PaxPaymentUtil paxPayUtil = new PaxPaymentUtil(totalWithAncillary, totalExtChgs, loyaltyPayAmount,
									lmsMemberPayment, voucherPaymentAmount, infantAmount);

							loyaltyPayAmount = paxPayUtil.getRemainingLoyalty();
							voucherPaymentAmount = paxPayUtil.getRemainingVoucherAmount();
							while (paxPayUtil.hasConsumablePayments()) {
								PaymentMethod payMethod = paxPayUtil.getNextPaymentMethod();
								BigDecimal paxPayAmt = paxPayUtil.getPaymentAmount(payMethod);
								BigDecimal infantPayAmt = paxPayUtil.getInfantPaymentAmount(payMethod);
								if (payMethod == PaymentMethod.LMS) {
									for (String operatingCarrierCode : carrierWiseLoyaltyPayments.keySet()) {
										LoyaltyPaymentInfo opCarrierLoyaltyPaymentInfo = carrierWiseLoyaltyPayments
												.get(operatingCarrierCode);
										Map<Integer, Map<String, BigDecimal>> carrierPaxProductRedeemed = opCarrierLoyaltyPaymentInfo
												.getPaxProductPaymentBreakdown();
										BigDecimal carrierLmsMemberPayment = AirProxyReservationUtil.getPaxRedeemedTotal(
												carrierPaxProductRedeemed, paxSequence);

										loyaltyMemberAccountId = opCarrierLoyaltyPaymentInfo.getMemberAccountId();
										rewardIDs = opCarrierLoyaltyPaymentInfo.getLoyaltyRewardIds();
										if (carrierLmsMemberPayment.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
											paymentComposer.addLMSMemberPayment(loyaltyMemberAccountId, rewardIDs,
												carrierPaxProductRedeemed, carrierLmsMemberPayment, payCurrencyDTO,
												currentDatetime, operatingCarrierCode);
										}
										/** TODO Add infant payment */
									}
								} else if (payMethod == PaymentMethod.MASHREQ_LOYALTY) {
									/** TODO check this */
									if (!paxPayAmt.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
										paymentComposer.addLoyaltyCreditPayment(loyaltyAgentCode, paxPayAmt, loyaltyAccount,
											payCurrencyDTO, currentDatetime);
									}
									if (infantPayAmt != null && !infantPayAmt.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
										infantPaymentComposer.addLoyaltyCreditPayment(loyaltyAgentCode, infantPayAmt, loyaltyAccount,
											payCurrencyDTO, currentDatetime);
									}
								} else if (payMethod == PaymentMethod.VOUCHER) {
									paymentComposer.addVoucherPayment(payByVoucherInfo, paxPayAmt, payCurrencyDTO,
											currentDatetime);
									if (infantPayAmt != null && !infantPayAmt.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
										infantPaymentComposer.addVoucherPayment(payByVoucherInfo, infantPayAmt, payCurrencyDTO,
												currentDatetime);										
									}
								} else {
									if (isExtCardPay) {
										paymentComposer.addCreditCardPayment(paxPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
												paymentTimestamp, null);
										if (infantPayAmt != null && !infantPayAmt.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
											infantPaymentComposer.addCreditCardPayment(infantPayAmt, creditInfo, ipgDTO, payCurrencyDTO,
													paymentTimestamp, null);											
										}
									} else {
										paymentComposer.addCreditCardPaymentInternal(paxPayAmt, creditInfo, ipgDTO,
												payCurrencyDTO, paymentTimestamp, cardPaymentInfo.getNo(),
												cardPaymentInfo.geteDate(), cardPaymentInfo.getSecurityCode(),
												cardPaymentInfo.getName(), null);
										if (infantPayAmt != null && !infantPayAmt.equals(AccelAeroCalculator.getDefaultBigDecimalZero())) {
											infantPaymentComposer.addCreditCardPaymentInternal(infantPayAmt, creditInfo, ipgDTO,
													payCurrencyDTO, paymentTimestamp, cardPaymentInfo.getNo(),
													cardPaymentInfo.geteDate(), cardPaymentInfo.getSecurityCode(),
													cardPaymentInfo.getName(), null);										
										}
									}
								}

							}

						}

					}
					lccClientPaymentAssembler = paymentComposer.getPaymentAssembler();
					infantPaymentAssembler = infantPaymentComposer.getPaymentAssembler();
					adultInfantPaymentAssemblerMap.put(intSeq, infantPaymentAssembler);
				}

				lccClientPaymentAssembler.setPaxType(CommonUtil.getActualPaxType(reservationPax));

				if (isParent) {
					// Passenger with a Infant - Parent
					lcclientReservationAssembler.addParent(reservationPax.getFirstName(), reservationPax.getLastName(),
							reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationality(), intSeq,
							reservationPax.getInfantWith(), null, reservationPax.getPaxAdditionalInfo(),
							reservationPax.getPaxCategory(), lccClientPaymentAssembler, null, null, null, null,
							reservationPax.getPnrPaxArrivalIntlFltNumber(), reservationPax.getPnrPaxIntlFltArrivalDate(),
							reservationPax.getPnrPaxDepartureIntlFltNumber(), reservationPax.getPnrPaxIntlFltDepartureDate(),
							reservationPax.getPnrPaxGroupId());
				} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
					lcclientReservationAssembler.addChild(reservationPax.getFirstName(), reservationPax.getLastName(),
							reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationality(), intSeq,
							null, reservationPax.getPaxAdditionalInfo(), reservationPax.getPaxCategory(),
							lccClientPaymentAssembler, null, null, null, null, null,
							reservationPax.getPnrPaxArrivalIntlFltNumber(), reservationPax.getPnrPaxIntlFltArrivalDate(),
							reservationPax.getPnrPaxDepartureIntlFltNumber(), reservationPax.getPnrPaxIntlFltDepartureDate(),
							reservationPax.getPnrPaxGroupId());
				} else {
					// Passenger without a Infant - Single
					lcclientReservationAssembler.addSingle(reservationPax.getFirstName(), reservationPax.getLastName(),
							reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationality(), intSeq,
							null, reservationPax.getPaxAdditionalInfo(), reservationPax.getPaxCategory(),
							lccClientPaymentAssembler, null, null, null, null, null,
							reservationPax.getPnrPaxArrivalIntlFltNumber(), reservationPax.getPnrPaxIntlFltArrivalDate(),
							reservationPax.getPnrPaxDepartureIntlFltNumber(), reservationPax.getPnrPaxIntlFltDepartureDate(),
							reservationPax.getPnrPaxGroupId());
				}

				intSeq++;
			}
		}
		for (ReservationPaxTO infant : infantList) {
			LCCClientPaymentAssembler infantPaymentAssembler = adultInfantPaymentAssemblerMap.get(infant.getInfantWith());
			lcclientReservationAssembler.addInfant(infant.getFirstName(), infant.getLastName(), infant.getTitle(),
					infant.getDateOfBirth(), infant.getNationality(), intSeq, infant.getInfantWith(), null, infant.getPaxAdditionalInfo(),
					infant.getPaxCategory(), null, null, null, null, infant.getPnrPaxArrivalIntlFltNumber(),
					infant.getPnrPaxIntlFltArrivalDate(), infant.getPnrPaxDepartureIntlFltNumber(),
					infant.getPnrPaxIntlFltDepartureDate(), infant.getPnrPaxGroupId(), infantPaymentAssembler);
			intSeq++;
		}

		return lcclientReservationAssembler;
	}

	public static void addPreferenceDetails(CommonReservationAssembler lcclientReservationAssembler, ContactInfoDTO contactInfo) {
		CommonReservationPreferrenceInfo lccClientReservationPreferenceInfo = new CommonReservationPreferrenceInfo();
		lccClientReservationPreferenceInfo.setPreferredLanguage(contactInfo.getEmailLanguage());
		lcclientReservationAssembler.addPreferenceInfo(lccClientReservationPreferenceInfo);
	}

	public static CommonReservationContactInfo getContactDetails(ContactInfoDTO contactInfo) {
		CommonReservationContactInfo lccClientReservationContactInfo = new CommonReservationContactInfo();
		lccClientReservationContactInfo.setTitle(contactInfo.getTitle());
		lccClientReservationContactInfo.setFirstName(contactInfo.getFirstName());
		lccClientReservationContactInfo.setLastName(contactInfo.getLastName());
		lccClientReservationContactInfo.setStreetAddress2(contactInfo.getAddresline());
		lccClientReservationContactInfo.setStreetAddress1(contactInfo.getAddresStreet());
		lccClientReservationContactInfo.setCity(contactInfo.getCity());
		lccClientReservationContactInfo.setCountryCode(contactInfo.getCountry());
		lccClientReservationContactInfo.setZipCode(contactInfo.getZipCode());
		lccClientReservationContactInfo.setMobileNo(contactInfo.getmCountry() + "-" + contactInfo.getmArea() + "-"
				+ contactInfo.getmNumber());
		lccClientReservationContactInfo.setPhoneNo(contactInfo.getlCountry() + "-" + contactInfo.getlArea() + "-"
				+ contactInfo.getlNumber());
		lccClientReservationContactInfo.setFax(contactInfo.getfCountry() + "-" + contactInfo.getfArea() + "-"
				+ contactInfo.getfNumber());
		lccClientReservationContactInfo.setEmail(contactInfo.getEmailAddress());
		// Fix Me, When loading XBE booking for IBE Modification
		if (contactInfo.getNationality() != null && !contactInfo.getNationality().isEmpty()) {
			lccClientReservationContactInfo.setNationalityCode(new Integer(contactInfo.getNationality()));
		}
		lccClientReservationContactInfo.setState(contactInfo.getState());
		lccClientReservationContactInfo.setCustomerId(contactInfo.getCustomerId());
		lccClientReservationContactInfo.setPreferredLanguage(contactInfo.getPreferredLangauge());

		// set emergency contact info details
		lccClientReservationContactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		lccClientReservationContactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		lccClientReservationContactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		lccClientReservationContactInfo.setEmgnPhoneNo(contactInfo.getEmgnLCountry() + "-" + contactInfo.getEmgnLArea() + "-"
				+ contactInfo.getEmgnLNumber());
		lccClientReservationContactInfo.setEmgnEmail(contactInfo.getEmgnEmail());

		return lccClientReservationContactInfo;

	}

	public static void addSegments(CommonReservationAssembler lcclientReservationAssembler, Collection<FlightSegmentTO> fltSegs) {
		int segemntSequnce = 1;
		// sort segments by departure date
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>(fltSegs);
		SortUtil.sortFlightSegByDepDate(flightSegments);
		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			if (!flightSegmentTO.isReturnFlag()) {
				lcclientReservationAssembler.addOutgoingSegment(segemntSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightRefNumber(),
						flightSegmentTO.getFlightNumber(), flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getBaggageONDGroupId(), false, flightSegmentTO.getRouteRefNumber());
			} else {
				lcclientReservationAssembler.addReturnSegment(segemntSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightRefNumber(),
						flightSegmentTO.getFlightNumber(), flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getBaggageONDGroupId(), false, flightSegmentTO.getRouteRefNumber());

			}

		}
	}

	private static String getFlightRefNumber(String flightRefNumber) {
		flightRefNumber = flightRefNumber.substring(flightRefNumber.indexOf("$") + 1, flightRefNumber.length());
		flightRefNumber = flightRefNumber.substring(flightRefNumber.indexOf("$") + 1, flightRefNumber.length());
		return flightRefNumber.substring(0, flightRefNumber.indexOf("$"));
	}

	public static URLTO getURLData(HttpServletRequest request) {
		URLTO urlTo = new URLTO();
		urlTo.setSecurePath(IBEModuleUtils.getConfig().getSecureIBEUrl());
		urlTo.setNonSecurePath(IBEModuleUtils.getConfig().getNonsecureIBEUrl());
		String strCarrier = SessionUtil.getCarrier(request);
		urlTo.setHomeURL(IBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.AIRLINE_URL, strCarrier));
		String strIp = request.getHeader("x-forwarded-for");
		Integer customerId = SessionUtil.getCustomerId(request);
		if (strIp == null || strIp.equals(""))
			strIp = request.getRemoteAddr();
		urlTo.setUserIp(strIp);
		return urlTo;

	}

	public static Date stringToDate(String strDate) {
		Date dtReturn = null;
		if (!strDate.equals("")) {
			int date = Integer.parseInt(strDate.substring(0, 2));
			int month = Integer.parseInt(strDate.substring(3, 5));
			int year = Integer.parseInt(strDate.substring(6, 10));

			Calendar validDate = new GregorianCalendar(year, month - 1, date);
			dtReturn = new Date(validDate.getTime().getTime());
		}
		return dtReturn;
	}

	public static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static BigDecimal getTotalExtChargeExcludingCCJN(List<LCCClientExternalChgDTO> extChgList) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			if (EXTERNAL_CHARGES.CREDIT_CARD.equals(chg.getCode()) || EXTERNAL_CHARGES.JN_OTHER.equals(chg.getCode())) {

			} else {
				total = AccelAeroCalculator.add(total, chg.getAmount());
			}
		}
		return total;
	}

	public static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList, List<EXTERNAL_CHARGES> skipCharges) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			if (skipCharges == null || (skipCharges != null && !skipCharges.contains(chg.getExternalCharges()))) {
				total = AccelAeroCalculator.add(total, chg.getAmount());
			}
		}
		return total;
	}

	/**
	 * Build LCCClientSegmentAssembler Using Selected List<FlightSegmentTO>
	 * 
	 * @param segmentTOs
	 * @return
	 */
	public static LCCClientSegmentAssembler getSegmentAssembler(List<FlightSegmentTO> segmentTOs, int segmentSequnce) {

		LCCClientSegmentAssembler lcclientSegmentAssembler = new LCCClientSegmentAssembler();
		// sort the flight segments by departure date
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>(segmentTOs);
		SortUtil.sortFlightSegByDepDate(flightSegments);

		for (FlightSegmentTO flightSegmentTO : flightSegments) {
			if (!flightSegmentTO.isReturnFlag()) {
				lcclientSegmentAssembler.addOutgoingSegment(segmentSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightNumber(),
						flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getOperatingAirline(), flightSegmentTO.getBaggageONDGroupId(),
						flightSegmentTO.getFlightRefNumber(), flightSegmentTO.getRouteRefNumber());

			} else {
				lcclientSegmentAssembler.addReturnSegment(segmentSequnce++,
						getFlightRefNumber(flightSegmentTO.getFlightRefNumber()), flightSegmentTO.getFlightNumber(),
						flightSegmentTO.getSegmentCode(), flightSegmentTO.getCabinClassCode(),
						flightSegmentTO.getLogicalCabinClassCode(), flightSegmentTO.getDepartureDateTime(),
						flightSegmentTO.getArrivalDateTime(), flightSegmentTO.getSubStationShortName(),
						flightSegmentTO.getDepartureDateTimeZulu(), flightSegmentTO.getArrivalDateTimeZulu(),
						flightSegmentTO.getOperatingAirline(), flightSegmentTO.getBaggageONDGroupId(),
						flightSegmentTO.getFlightRefNumber(), flightSegmentTO.getRouteRefNumber());
			}
		}

		return lcclientSegmentAssembler;
	}

	/**
	 * get last segment sequence
	 * 
	 * @param pnrSegments
	 * @return
	 */
	public static int getNextSegmentSequnce(Collection<LCCClientReservationSegment> pnrSegments) {
		int segmentSequnce = 0;
		if (pnrSegments != null) {
			for (LCCClientReservationSegment segemnt : pnrSegments) {
				if (segemnt.getSegmentSeq() > segmentSequnce) {
					segmentSequnce = segemnt.getSegmentSeq().intValue();
				}
			}
		}
		return ++segmentSequnce;
	}

	/**
	 * 
	 * @param postPayDTO
	 * @param lcclientSegmentAssembler
	 */
	public static void populateInsurance(IBEReservationPostPaymentDTO postPayDTO,
			LCCClientSegmentAssembler lcclientSegmentAssembler, int adultCount) {

		List<LCCInsuranceQuotationDTO> lccInsQuoteDTOs = postPayDTO.getInsuranceQuotes();
		if (lccInsQuoteDTOs != null && !lccInsQuoteDTOs.isEmpty()) {

			for (LCCInsuranceQuotationDTO lccInsQuoteDTO : lccInsQuoteDTOs) {

				LCCClientReservationInsurance insurance = new LCCClientReservationInsurance();

				LCCInsuredJourneyDTO insuredJourneyDTO = new LCCInsuredJourneyDTO();
				insuredJourneyDTO.setJourneyEndDate(lccInsQuoteDTO.getInsuredJourney().getJourneyEndDate());
				insuredJourneyDTO.setJourneyStartDate(lccInsQuoteDTO.getInsuredJourney().getJourneyStartDate());
				insuredJourneyDTO.setJourneyStartAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyStartAirportCode());
				insuredJourneyDTO.setJourneyEndAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyEndAirportCode());
				insuredJourneyDTO.setRoundTrip(lccInsQuoteDTO.getInsuredJourney().isRoundTrip());
				insurance.setInsuranceQuoteRefNumber(lccInsQuoteDTO.getInsuranceRefNumber());
				insurance.setInsuredJourneyDTO(insuredJourneyDTO);
				insurance.setQuotedTotalPremium(lccInsQuoteDTO.getQuotedTotalPremiumAmount());
				insurance.setInsuranceType(lccInsQuoteDTO.getInsuranceType());
				insurance.setApplicablePaxCount(adultCount);
				insurance.setOperatingAirline(lccInsQuoteDTO.getOperatingAirline());
				insurance.setPlanCode(lccInsQuoteDTO.getPlanCode());
				insurance.setSsrFeeCode(lccInsQuoteDTO.getSsrFeeCode());
				lcclientSegmentAssembler.addResInsurance(insurance);
			}

		}
	}

	public static List<LCCClientExternalChgDTO> converToProxyExtCharges(Collection<ExternalChgDTO> extChgs) {
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		if (extChgs != null) {
			for (ExternalChgDTO chg : extChgs) {
				extChgList.add(new LCCClientExternalChgDTO(chg));
			}
		}
		return extChgList;
	}

	public static void setSubStationData(CommonReservationAssembler lcclientReservationAssembler, IBEReservationInfoDTO resInfo) {
		Set<LCCClientReservationSegment> segSet = lcclientReservationAssembler.getLccreservation().getSegments();
		GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();

		// for (LCCClientReservationSegment clientReservationSegment : segSet) {
		// for (ReservationSegmentStatusDTO segStatus : resInfo.getSegmentStatus()) {
		// //if(clientReservationSegment.getFlightSegmentRefNumber().equals(segStatus.getFlightSegmentRefNo())){
		// //TODO finish implementation
		// //break;
		// //}
		// }
		// }

	}

	/**
	 * Get Credit card details
	 * 
	 * @param tempPayMap
	 * @return
	 */
	public static CommonCreditCardPaymentInfo getCommonCreditCardPaymentInfo(
			Map<Integer, CommonCreditCardPaymentInfo> tempPayMap, boolean isPaymentSuccess) {
		CommonCreditCardPaymentInfo cardPaymentInfo = null;

		if (tempPayMap != null && !tempPayMap.isEmpty()) {
			Set<Integer> tmpKeySet = tempPayMap.keySet();

			for (Integer key : tmpKeySet) {
				if (key != null) {
					cardPaymentInfo = tempPayMap.get(key);
					cardPaymentInfo.setPaymentSuccess(isPaymentSuccess);
					if (cardPaymentInfo != null) {
						break;
					}
				}
			}
		}
		return cardPaymentInfo;
	}

	public static void populateInsurance(List<LCCInsuranceQuotationDTO> lccInsQuoteDTOs, RequoteModifyRQ requoteRQ,
			int adultCount, TrackInfoDTO trackDTO) throws ModuleException {

		List<LCCClientReservationInsurance> lccInsurances = new ArrayList<LCCClientReservationInsurance>();

		if (lccInsQuoteDTOs != null && !lccInsQuoteDTOs.isEmpty()) {
			for (LCCInsuranceQuotationDTO lccInsQuoteDTO : lccInsQuoteDTOs) {
				LCCClientReservationInsurance lccInsurance = new LCCClientReservationInsurance();
				LCCInsuredJourneyDTO insuredJourneyDTO = new LCCInsuredJourneyDTO();
				insuredJourneyDTO.setJourneyEndDate(lccInsQuoteDTO.getInsuredJourney().getJourneyEndDate());
				insuredJourneyDTO.setJourneyStartDate(lccInsQuoteDTO.getInsuredJourney().getJourneyStartDate());
				insuredJourneyDTO.setJourneyStartAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyStartAirportCode());
				insuredJourneyDTO.setJourneyEndAirportCode(lccInsQuoteDTO.getInsuredJourney().getJourneyEndAirportCode());
				insuredJourneyDTO.setRoundTrip(lccInsQuoteDTO.getInsuredJourney().isRoundTrip());
				lccInsurance.setInsuranceQuoteRefNumber(lccInsQuoteDTO.getInsuranceRefNumber());
				lccInsurance.setInsuredJourneyDTO(insuredJourneyDTO);
				lccInsurance.setQuotedTotalPremium(lccInsQuoteDTO.getQuotedTotalPremiumAmount());
				lccInsurance.setApplicablePaxCount(adultCount);
				lccInsurance.setOperatingAirline(lccInsQuoteDTO.getOperatingAirline());
				lccInsurance.setSsrFeeCode(lccInsQuoteDTO.getSsrFeeCode());
				lccInsurance.setPlanCode(lccInsQuoteDTO.getPlanCode());

				lccInsurances.add(lccInsurance);
			}
		}

		List<IInsuranceRequest> insuranceRequests = new ArrayList<IInsuranceRequest>();

		IInsuranceRequest insurance = null;
		Reservation reservation = null;

		if (lccInsurances != null && !lccInsurances.isEmpty()) {
			LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
			modes.setPnr(requoteRQ.getPnr());
			reservation = ModuleServiceLocator.getReservationBD().getReservation(modes, trackDTO);

			for (LCCClientReservationInsurance lccInsurance : lccInsurances) {

				insurance = new InsuranceRequestAssembler();

				InsureSegment insSegment = new InsureSegment();
				LCCInsuredJourneyDTO insuredJourneyDTO = lccInsurance.getInsuredJourneyDTO();
				insSegment.setArrivalDate(insuredJourneyDTO.getJourneyEndDate());
				insSegment.setDepartureDate(insuredJourneyDTO.getJourneyStartDate());
				insSegment.setFromAirportCode(insuredJourneyDTO.getJourneyStartAirportCode());
				insSegment.setToAirportCode(insuredJourneyDTO.getJourneyEndAirportCode());
				insSegment.setRoundTrip(insuredJourneyDTO.isRoundTrip());
				insSegment.setSalesChanelCode(insuredJourneyDTO.getSalesChannel());

				insurance.setInsuranceId(new Integer(lccInsurance.getInsuranceQuoteRefNumber()));
				insurance.addFlightSegment(insSegment);
				insurance.setQuotedTotalPremiumAmount(lccInsurance.getQuotedTotalPremium());
				insurance.setInsuranceType(lccInsurance.getInsuranceType());
				insurance.setAllDomastic(lccInsQuoteDTOs.get(0).isAllDomastic()); // TODO
				insurance.setSsrFeeCode(lccInsurance.getSsrFeeCode());
				insurance.setPlanCode(lccInsurance.getPlanCode());
				insurance.setOperatingAirline(lccInsurance.getOperatingAirline());

				int paxCount = 0;
				if (AppSysParamsUtil.allowAddInsurnaceForInfants()) {
					paxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount()
							+ reservation.getTotalPaxInfantCount();
				} else {
					paxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
				}

				BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(lccInsurance.getQuotedTotalPremium(), paxCount);
				int i = 0;
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())
							|| (PaxTypeTO.INFANT.equals(pax.getPaxType()) && AppSysParamsUtil.allowAddInsurnaceForInfants())) {
						InsurePassenger insuredPassenger = new InsurePassenger();

						// adding AIG Pax info
						insuredPassenger.setFirstName(pax.getFirstName());
						insuredPassenger.setLastName(pax.getLastName());
						insuredPassenger.setDateOfBirth(pax.getDateOfBirth());

						ReservationContactInfo contactInfo = reservation.getContactInfo();
						// Adding AIG Contact Info
						insuredPassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insuredPassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insuredPassenger.setCity(contactInfo.getCity());
						insuredPassenger.setEmail(contactInfo.getEmail());
						insuredPassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insuredPassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

						insurance.addPassenger(insuredPassenger);
						if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
							insurance.addInsuranceCharge(pax.getPaxSequence(), BigDecimal.ZERO);
						} else {
							insurance.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
						}
					}
				}

				insuranceRequests.add(insurance);
			}

		       requoteRQ.setInsurances(insuranceRequests);
		}

	}
}
