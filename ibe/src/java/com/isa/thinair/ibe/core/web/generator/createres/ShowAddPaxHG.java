package com.isa.thinair.ibe.core.web.generator.createres;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.aircustomer.api.model.Customer;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;

public class ShowAddPaxHG {
	

	public static String createProfileInfo(Customer cust) throws ModuleException {
		StringBuffer strData = new StringBuffer();
		strData.append("arrProfile = new Array('" + JavaScriptGenerator.nullConvertToString(cust.getTitle()) + "'," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getFirstName()) + "'," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getLastName()) + "'," + "''," + "''," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getTelephone()) + "'," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getMobile()) + "'," + "''," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getFax()) + "'," + "''," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getEmailId()) + "'," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getCountryCode()) + "'," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getNationalityCode()) + "'," + "'"
				+ JavaScriptGenerator.nullConvertToString(cust.getCity()) + "');");

		return strData.toString();
	}

	// Selected language for Itinerary
	public static String createItineryLanguagesOptionSelected(String userLang) {

		if (userLang != null)
			userLang = userLang.toLowerCase();
		StringBuffer sb = new StringBuffer();
		Map<String, String> mapLan = AppSysParamsUtil.getIBESupportedLanguages();
		Set colKey = mapLan.keySet();
		for (Iterator iter = colKey.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			String lanName = I18NUtil.getMessage("msg.res.psg.Itn.Lng." + key, userLang);
			sb.append("<option value='");
			sb.append(key);
			sb.append("'");
			if (key.equalsIgnoreCase(userLang)) {
				sb.append(" selected");
			}
			sb.append(">");
			sb.append(lanName);
			sb.append("</option>");
		}
		return sb.toString();
	}

}
