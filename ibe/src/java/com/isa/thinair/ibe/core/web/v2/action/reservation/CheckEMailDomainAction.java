package com.isa.thinair.ibe.core.web.v2.action.reservation;

import javax.naming.NameNotFoundException;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.EmailDomainUtil;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * POJO to validate the email domain name
 * 
 * @author Jagath
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CheckEMailDomainAction extends BaseRequestAwareAction {

	private boolean exists = true;

	private boolean success = true;

	private String emailDomain;

	public String execute() throws ModuleException {
		String result = StrutsConstants.Result.SUCCESS;
		exists = true;

		try {

			emailDomain = request.getParameter("domain");
			if (EmailDomainUtil.isDomainWhiteListed(emailDomain)) {//TODO : CP email domain validation using DNS lookup
				exists = true;
			} else {
				exists = false;               
			}
		} catch (NameNotFoundException exp) {
			exists = false;
		} catch (Exception e) {
			success = true;
		}

		return result;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}

	public boolean getExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}