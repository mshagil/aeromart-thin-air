package com.isa.thinair.ibe.core.web.v2.action.rnd;

import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(StrutsConstants.Namespace.PUBLIC) 
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CalendarSearchResearchAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(CalendarSearchResearchAction.class);

	private boolean success = true;

	private String messageTxt;

	private CalendarSearchRS calendarRS;

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		CalendarSearchRQ calendarSearchRQ = new CalendarSearchRQ();
		try {
			OriginDestinationInformationTO ond = calendarSearchRQ.addNewOriginDestinationInformation();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			ond.setOrigin(request.getParameter("FROM"));
			ond.setDestination(request.getParameter("TO"));
			ond.setDepartureDateTimeStart(sdf.parse(request.getParameter("FROMDATE")));
			ond.setDepartureDateTimeEnd(sdf.parse(request.getParameter("TODATE")));
			ond.setPreferredClassOfService(request.getParameter("CABINCLASS"));

			int adt = Integer.parseInt(request.getParameter("AD"));
			int chd = Integer.parseInt(request.getParameter("CH"));
			int inf = Integer.parseInt(request.getParameter("IN"));

			AvailPreferencesTO availPref = calendarSearchRQ.getAvailPreferences();
			availPref.setAppIndicator(ApplicationEngine.IBE);
			availPref.setPreferredCurrency(request.getParameter("CUR"));

			TravelerInfoSummaryTO travellerInfo = calendarSearchRQ.getTravelerInfoSummary();
			if (adt > 0) {
				PassengerTypeQuantityTO paxType = travellerInfo.addNewPassengerTypeQuantityTO();
				paxType.setPassengerType(PaxTypeTO.ADULT);
				paxType.setQuantity(adt);
			}
			if (chd > 0) {
				PassengerTypeQuantityTO paxType = travellerInfo.addNewPassengerTypeQuantityTO();
				paxType.setPassengerType(PaxTypeTO.CHILD);
				paxType.setQuantity(chd);
			}
			if (inf > 0) {
				PassengerTypeQuantityTO paxType = travellerInfo.addNewPassengerTypeQuantityTO();
				paxType.setPassengerType(PaxTypeTO.INFANT);
				paxType.setQuantity(inf);
			}
			TravelPreferencesTO travelPref = calendarSearchRQ.getTravelPreferences();

			this.calendarRS = ModuleServiceLocator.getFlightInventoryReservationBD().searchCalendarFares(calendarSearchRQ);
		} catch (ModuleException e) {
			success = false;
			log.error(e);
			messageTxt = e.getMessage();
		} catch (Exception e) {
			success = false;
			log.error(e);
			messageTxt = "Unknown error";
		}

		return result;
	}

	public CalendarSearchRS getCalendarRS() {
		return calendarRS;
	}

	public void setCalendarRS(CalendarSearchRS calendarRS) {
		this.calendarRS = calendarRS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

}
