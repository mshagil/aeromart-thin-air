package com.isa.thinair.ibe.core.web.v2.util;

import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.AeroMartPayPaymentGatewayInfo;
import com.isa.thinair.ibe.api.dto.AeroMartPaySessionInfo;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;

public interface AeroMartPayValidator {

	public AeroMartPayResponse validatePaymentRequest(Map<String, String> requestParams);

	public IPGIdentificationParamsDTO validateAndPrepareIPGConfigurationParamsDTO(Integer ipgId, String paymentCurrencyCode)
			throws ModuleException;

	public List<String> validateUserInput(String cardBehavior, String paymentgatewayId, String cardNumber, String cardType,
			String cardHolderName, String cvv, String expiryDate);

	public boolean validateReferenceID(String referenceID) throws ModuleException;

	public boolean validateSessionDetails(AeroMartPaySessionInfo sessionInfo);

	public void validatePaymentGatewaysInfo(List<AeroMartPayPaymentGatewayInfo> paymentGatewayInfo) throws ModuleException;

	public AeroMartPayResponse validateQueryRequest(Map<String, String> requestParams);

}