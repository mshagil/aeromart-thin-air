package com.isa.thinair.ibe.core.web.v2.action.payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.ibe.api.dto.AeroMartPaySessionInfo;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants;
import com.isa.thinair.ibe.core.web.v2.constants.AeroMartPayConstants.AeroMartPayResponse;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayUtil;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidator;
import com.isa.thinair.ibe.core.web.v2.util.AeroMartPayValidatorImpl;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.LOAD_AEROMARTPAY_PAYMENT_OPTIONS, value = StrutsConstants.Jsp.Common.LOAD_AEROMARTPAY_PAYMENT_OPTIONS),
		@Result(name = StrutsConstants.Result.AEROMART_PAY_RESPONSE, value = StrutsConstants.Jsp.Common.AEROMART_PAY_RESPONSE) })
public class HandleAeroMartPayIPGResponseAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(HandleAeroMartPayIPGResponseAction.class);

	public String execute() {

		// TODO impl as service
		AeroMartPayValidator aeroMartPayValidator = new AeroMartPayValidatorImpl();

		boolean isPaymentSuccess;
		AeroMartPayResponse aeroMartPayResponse = null;
		String result = null;

		HttpSession session = request.getSession(false);
		AeroMartPaySessionInfo sessionInfo = (AeroMartPaySessionInfo) session.getAttribute(AeroMartPayConstants.SESSION_INFO);

		Collection<Integer> colTptIDs = null;
		if (!StringUtil.isNullOrEmpty(sessionInfo.getReferenceID())) {
			colTptIDs = new ArrayList<Integer>();
			colTptIDs.add(Integer.valueOf(sessionInfo.getReferenceID()));
		} else {
			sessionInfo.setReferenceID("");
		}

		try {

			if (aeroMartPayValidator.validateSessionDetails(sessionInfo)) {
				
				if (!sessionInfo.getAgentType().equals(AeroMartPayConstants.AGENT)){
					IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
					int paymentBrokerRefNo = (int) session.getAttribute(AeroMartPayConstants.PAYMENT_BROKER_REF_NO);
					String pgID = (String) session.getAttribute(AeroMartPayConstants.PAYMENT_GATEWAY_ID);

					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = aeroMartPayValidator
							.validateAndPrepareIPGConfigurationParamsDTO(Integer.valueOf(pgID), sessionInfo.getPaymentCurr());

					Properties ipgProps = ModuleServiceLocator.getPaymentBrokerBD().getProperties(ipgIdentificationParamsDTO);
					String responseTypeXML = ipgProps.getProperty(PaymentBrokerBD.PaymentBrokerProperties.KEY_RESPONSE_TYPE_XML);

					Map<String, String> receiptyMap = AeroMartPayUtil.getReceiptMap(request);

					if (responseTypeXML != null && !responseTypeXML.equals("") && responseTypeXML.equals("true")) {

						XMLResponseDTO xmlResponse = (XMLResponseDTO) request.getAttribute(WebConstants.XML_RESPONSE_PARAMETERS);
						if (xmlResponse != null) {
							receiptyMap = AeroMartPayUtil.getReceiptMap((XMLResponseDTO) request
									.getAttribute(WebConstants.XML_RESPONSE_PARAMETERS), request.getSession().getId());
						}
					}

					if (receiptyMap == null || receiptyMap.size() == 0) {
						aeroMartPayResponse = AeroMartPayResponse.ERR_101;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());
					}

					Date requestTime = null;

					requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(
							Integer.valueOf(sessionInfo.getReferenceID()));

					ipgResponseDTO.setRequestTimsStamp(requestTime);
					ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
					ipgResponseDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
					ipgResponseDTO.setTemporyPaymentId(Integer.valueOf(sessionInfo.getReferenceID()));

					log.debug("### Getting response data... ###");
					ipgResponseDTO = ModuleServiceLocator.getPaymentBrokerBD().getReponseData(receiptyMap, ipgResponseDTO,
							ipgIdentificationParamsDTO);

					isPaymentSuccess = ipgResponseDTO.isSuccess();
					
					synchronized (request.getSession()) {

						if ((boolean) request.getSession().getAttribute(AeroMartPayConstants.RESPONSE_RECEIVED)) {
							log.error("Multiple Payment responses, GoQUO orderID " + sessionInfo.getCurrencyCode());
							aeroMartPayResponse = AeroMartPayResponse.ERR_106;
							throw new ModuleException(aeroMartPayResponse.getRespMessage());

						} else {

							request.getSession().setAttribute(AeroMartPayConstants.RESPONSE_RECEIVED, true);

							try {
								if (isPaymentSuccess) {
									ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTptIDs,
											ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS,
											"GOQUO Payment successful", "", null, null);
								} else {
									ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTptIDs,
											ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE,
											"GOQUO Payment failed", "", null, null);
								}
							} catch (ModuleException e) {
								log.error("GOQUO PAYMENT SUCESS STATUS (RS) UPDATE FAILED FOR orderID: " + sessionInfo.getOrderID(),
										e);
							}

						}
					}
				}else{
					
					ServiceResponce serviceResponse = (ServiceResponce) request.getAttribute(WebConstants.AGENT_SERVICE_RESPONSE);
					
					isPaymentSuccess = serviceResponse.isSuccess();
					
					synchronized (request.getSession()) {
						try {
							if (isPaymentSuccess) {
								ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTptIDs,
										ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS,
										"GOQUO Payment successful", "", null, null);
							} else {
								ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTptIDs,
										ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE, "GOQUO Payment failed",
										"", null, null);
							}
						} catch (ModuleException e) {
							log.error("GOQUO PAYMENT SUCESS STATUS (RS) UPDATE FAILED FOR orderID: " + sessionInfo.getOrderID(),
									e);
						}
					}
				}

				

				if (isPaymentSuccess) {

					AeroMartPayUtil.setPaymentResponseAttributes(request, sessionInfo.getOrderID(), sessionInfo.getReturnURL(),
							AeroMartPayResponse.SUCCESS.getRespCode(), AeroMartPayResponse.SUCCESS.getRespMessage(),
							sessionInfo.getReferenceID(), sessionInfo.getTotalAmount());

					AeroMartPayUtil.invalidateSession(request);

					result = StrutsConstants.Result.AEROMART_PAY_RESPONSE;

					AeroMartPayUtil.invalidateSession(request);

				} else {

					log.error(AeroMartPayResponse.ERR_102.getRespMessage());

					if (AeroMartPayUtil.incrementRetrails(session)) {

						log.info("Recreate Payment Page, with new Reference ID");

						BigDecimal baseCurrencyValue;
						try {

							baseCurrencyValue = AeroMartPayUtil.getBaseCurrencyValue(sessionInfo.getCurrencyCode(),
									new BigDecimal(sessionInfo.getTotalAmount()));

							if (baseCurrencyValue == null || baseCurrencyValue.doubleValue() <= 0) {
								throw new ModuleException("");
							}
						} catch (Exception e) {
							aeroMartPayResponse = AeroMartPayResponse.ERR_12;
							throw new ModuleException(aeroMartPayResponse.getRespMessage());

						}

						String newReferenceID = AeroMartPayUtil.generateReferenceID(baseCurrencyValue, "", true, "", "",
								sessionInfo.getOrderID(), "", "", new BigDecimal(sessionInfo.getTotalAmount()),
								sessionInfo.getCurrencyCode(), "", AeroMartPayConstants.PRODUCT_TYPE);

						if (!aeroMartPayValidator.validateReferenceID(newReferenceID)) {
							// couldn't generate valid referenceID then throw payment failure
							aeroMartPayResponse = AeroMartPayResponse.ERR_102;
							throw new ModuleException(aeroMartPayResponse.getRespMessage());
						}

						sessionInfo.setReferenceID(newReferenceID);

						if (!AeroMartPayUtil.sessionHandlePaymentLock(session, false)) {
							log.error("Multiple Payment Details Submission, Concurernt LOCK Access, orderID: "
									+ sessionInfo.getOrderID());
							aeroMartPayResponse = AeroMartPayResponse.ERR_108;
							throw new ModuleException(aeroMartPayResponse.getRespMessage());
						}

						request.getSession().setAttribute(AeroMartPayConstants.MESSAGE,
								AeroMartPayResponse.ERR_102.getRespMessage() + ": Retry");

						request.setAttribute(AeroMartPayConstants.LOAD_PAYMENT_OPTIONS,
								AeroMartPayConstants.LOAD_PAYMENT_OPTIONS_URL);

						result = StrutsConstants.Result.LOAD_AEROMARTPAY_PAYMENT_OPTIONS;

					} else {
						aeroMartPayResponse = AeroMartPayResponse.ERR_201;
						throw new ModuleException(aeroMartPayResponse.getRespMessage());
					}
				}
			}

			else {
				aeroMartPayResponse = AeroMartPayResponse.ERR_105;
				throw new ModuleException(aeroMartPayResponse.getRespMessage());
			}

		} catch (Exception e) {
			if (aeroMartPayResponse == null) {
				aeroMartPayResponse = AeroMartPayResponse.ERR_103;
			}
			log.error(aeroMartPayResponse.getRespMessage(), e);

			if (colTptIDs != null) {
				try {
					ModuleServiceLocator.getReservationBD().updateTempPaymentEntry(colTptIDs,
							ReservationInternalConstants.TempPaymentTnxTypes.PAYMENT_FAILURE,
							aeroMartPayResponse.getRespCode() + ":" + aeroMartPayResponse.getRespMessage(), "", null, null);
				} catch (ModuleException ex) {
					log.error("Payment failure update failed. GoQUO orderID " + sessionInfo.getOrderID(), ex);

				}
			}

			AeroMartPayUtil.setPaymentResponseAttributes(request, sessionInfo.getOrderID(), sessionInfo.getErrorURL(),
					aeroMartPayResponse.getRespCode(), aeroMartPayResponse.getRespMessage(), sessionInfo.getReferenceID(),
					sessionInfo.getTotalAmount());

			result = StrutsConstants.Result.AEROMART_PAY_RESPONSE;

			AeroMartPayUtil.invalidateSession(request);

		}

		return result;

	}
}
