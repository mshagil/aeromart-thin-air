package com.isa.thinair.ibe.core.web.handler.common;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;

/**
 * @author Srikantha BasicRequestHandler is the base class for all other request handlers
 * 
 */

public class BasicRequestHandler {

	/**
	 * returns server error message
	 * 
	 * @param request
	 * @param key
	 * @return
	 */
	protected static String getServerErrorMessage(HttpServletRequest request, String key) {
		return I18NUtil.getMessage(key, SessionUtil.getLanguage(request));
	}

}
