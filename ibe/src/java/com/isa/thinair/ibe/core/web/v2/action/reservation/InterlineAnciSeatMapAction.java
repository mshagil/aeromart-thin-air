package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciSeatMapAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciSeatMapAction.class);

	private boolean success = true;

	private String messageTxt;

	private LCCSeatMapDTO seatMapDTO;

	private String oldAllSegments;

	private boolean modifySegment;

	private boolean isAnciOffer;

	private String anciOfferName;

	private String anciOfferDescription;

	private String jsonOnds;

	private String pnr;

	private boolean modifyAncillary;
	
	private String seatType = "";

	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();

			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					(modifySegment && AppSysParamsUtil.isRequoteEnabled()));
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

			SelectedFltSegBuilder totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);

			if (ibeResInfo.getPriceInfoTO() != null) {
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(ibeResInfo.getPriceInfoTO(),
						flightSegmentTOs, totalFltSegBuilder.isReturn(), totalFltSegBuilder.getSelectedFlightSegments());
			} else {
				List<FlightSegmentTO> oldFlightSegmentTOs = totalFltSegBuilder.getSelectedFlightSegments();
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(oldFlightSegmentTOs, flightSegmentTOs,
						searchParams.getAdultCount(), searchParams.getChildCount(), searchParams.getInfantCount(),
						totalFltSegBuilder.isReturn());
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
			}

			List<BundledFareDTO> bundledFareDTOs = ibeResInfo.getSelectedBundledFares();
			SYSTEM system = ibeResInfo.getSelectedSystem();

			LCCSeatMapDTO inSeatMapDTO = new LCCSeatMapDTO();
			inSeatMapDTO.setFlightDetails(flightSegmentTOs);
			inSeatMapDTO.setTransactionIdentifier(strTxnIdntifier);
			inSeatMapDTO.setQueryingSystem(system);
			inSeatMapDTO.setBundledFareDTOs(bundledFareDTOs);
			inSeatMapDTO.setPnr(pnr);

			String selectedLanguage = SessionUtil.getLanguage(request);

			seatMapDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getSeatMap(inSeatMapDTO, selectedLanguage,
					TrackInfoUtil.getBasicTrackInfo(request), ApplicationEngine.IBE);

			setAnciOfferDataToSession(seatMapDTO, ibeResInfo);
			setAnciOfferDetailsForDisplay(seatMapDTO);

			setSeatMapDTO(seatMapDTO);
			
			seatType = CustomerUtil.getCustomerPreferredSeatType(SessionUtil.getCustomerId(request));

		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("messageTxt", e);
		}
		return result;
	}

	private void setAnciOfferDataToSession(LCCSeatMapDTO seatMapDetails, IBEReservationInfoDTO ibeResInfo) {
		for (LCCSegmentSeatMapDTO segmentSeats : seatMapDetails.getLccSegmentSeatMapDTOs()) {

			if (!segmentSeats.isAnciOffer()) {
				continue;
			}

			String flightRef = AnciOfferUtil.getFlightRefWithoutLCCTxnRefDetails(segmentSeats.getFlightSegmentDTO()
					.getFlightRefNumber());

			if (ibeResInfo.getAnciOfferTemplates().get(flightRef) == null) {
				ibeResInfo.getAnciOfferTemplates().put(flightRef, new HashMap<EXTERNAL_CHARGES, Integer>());
			}
			ibeResInfo.getAnciOfferTemplates().get(flightRef)
					.put(EXTERNAL_CHARGES.SEAT_MAP, segmentSeats.getOfferedAnciTemplateID());
		}
		SessionUtil.setIBEreservationInfo(request, ibeResInfo);
	}

	private void setAnciOfferDetailsForDisplay(LCCSeatMapDTO seatMapDetails) {
		Collections.sort(seatMapDetails.getLccSegmentSeatMapDTOs());
		for (LCCSegmentSeatMapDTO segmentSeats : seatMapDetails.getLccSegmentSeatMapDTOs()) {

			if (segmentSeats.isAnciOffer()) {
				isAnciOffer = true;
				anciOfferName = segmentSeats.getAnciOfferName();
				anciOfferDescription = segmentSeats.getAnciOfferDescription();
				break;
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public LCCSeatMapDTO getSeatMapDTO() {
		return seatMapDTO;
	}

	public void setSeatMapDTO(LCCSeatMapDTO seatMapDTO) {
		this.seatMapDTO = seatMapDTO;
	}

	public String getOldAllSegments() {
		return oldAllSegments;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public boolean isModifySegment() {
		return modifySegment;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the anciOfferName
	 */
	public String getAnciOfferName() {
		return anciOfferName;
	}

	/**
	 * @param anciOfferName
	 *            the anciOfferName to set
	 */
	public void setAnciOfferName(String anciOfferName) {
		this.anciOfferName = anciOfferName;
	}

	/**
	 * @return the anciOfferDescription
	 */
	public String getAnciOfferDescription() {
		return anciOfferDescription;
	}

	/**
	 * @param anciOfferDescription
	 *            the anciOfferDescription to set
	 */
	public void setAnciOfferDescription(String anciOfferDescription) {
		this.anciOfferDescription = anciOfferDescription;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public String getSeatType() {
		return seatType;
	}
	
}
