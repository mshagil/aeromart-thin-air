package com.isa.thinair.ibe.core.web.listener;

import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;

public class AAHttpSessionListener implements HttpSessionListener {

	private static Log log = LogFactory.getLog(AAHttpSessionListener.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
	private static Hashtable<String, HttpSession> sessionMap = new Hashtable<String, HttpSession>();

	@Override
	public void sessionCreated(HttpSessionEvent hse) {
		try {
			if (hse != null) {
				HttpSession session = hse.getSession();
				if (session != null) {
					sessionMap.put(session.getId(), session);
					log.info("SessionCreated : " + session.getId() + ":" + dateFormat.format(session.getCreationTime()) + ":"
							+ dateFormat.format(session.getLastAccessedTime()) + ":" + session.getMaxInactiveInterval());
				} else {
					log.error("SessionCreated event :: session is null");
				}
			}
		} catch (Exception ex) {
			log.error("SessionCreated event :: Error in logging session created event", ex);
		}
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent hse) {
		try {
			if (hse != null) {
				HttpSession session = hse.getSession();
				if (session != null) {
					sessionMap.remove(session.getId());
					log.info("SessionDestroyed : " + session.getId() + ":" + dateFormat.format(session.getCreationTime()) + ":"
							+ dateFormat.format(session.getLastAccessedTime()) + ":" + session.getMaxInactiveInterval());
				} else {
					log.error("SessionDestroyed event :: session is null");
				}
			}
		} catch (Exception ex) {
			log.error("SessionDestroyed event :: Error in logging session destroyed event", ex);
		}

	}
	
	private static boolean swapSessionValues(String oldSessionId, String newSessionId) {
		oldSessionId = BeanUtils.nullHandler(oldSessionId);
		newSessionId = BeanUtils.nullHandler(newSessionId);

		if (oldSessionId.length() > 0 && newSessionId.length() > 0 && (!oldSessionId.equals(newSessionId))) {
			HttpSession oldSession = sessionMap.get(oldSessionId);
			HttpSession newSession = sessionMap.get(newSessionId);

			Enumeration<String> oldEnumeration = oldSession.getAttributeNames();
			while (oldEnumeration.hasMoreElements()) {
				String key = oldEnumeration.nextElement();
				Object value = oldSession.getAttribute(key);

				newSession.setAttribute(key, value);
			}

			return true;
		} else {
			return false;
		}
	}

	public static HttpSession getSession(String sessionId) {
		return sessionMap.get(sessionId);
	}

	public static void handleIPGPost(String oldSessionId, String newSessionId) {
		oldSessionId = BeanUtils.nullHandler(oldSessionId);
		newSessionId = BeanUtils.nullHandler(newSessionId);

		boolean status = swapSessionValues(oldSessionId, newSessionId);

		if (status) {
			HttpSession oldSession = sessionMap.get(oldSessionId);
			if (oldSession.getAttribute(WebConstants.IBE_NEW_SESSION_ID) == null) {
				oldSession.setAttribute(WebConstants.IBE_NEW_SESSION_ID, newSessionId);

				HttpSession newSession = sessionMap.get(newSessionId);
				if (newSession != null) {
					newSession.setAttribute(WebConstants.WAIT_TILL_RECEIPT, true);
				}
			}
		}
	}

	public static void handleCustomerReceipt(String oldSessionId, String newSessionId) {
		oldSessionId = BeanUtils.nullHandler(oldSessionId);
		newSessionId = BeanUtils.nullHandler(newSessionId);

		if (oldSessionId.length() > 0) {
			HttpSession oldSession = sessionMap.get(oldSessionId);
			if (oldSession != null) {
				String lastNewSessionId = BeanUtils.nullHandler(oldSession.getAttribute(WebConstants.IBE_NEW_SESSION_ID));

				if (lastNewSessionId.length() > 0) {
					swapSessionValues(lastNewSessionId, newSessionId);

					HttpSession lastNewSession = sessionMap.get(lastNewSessionId);
					if (lastNewSession != null) {
						lastNewSession.invalidate();
					}
					
					oldSession.setAttribute(WebConstants.IBE_NEW_SESSION_ID, null);
				}

				if (!oldSessionId.equals(newSessionId)) {
					oldSession.invalidate();
				}
			}
		}

		if (newSessionId.length() > 0) {
			HttpSession newSession = sessionMap.get(newSessionId);

			if (newSession != null) {
				newSession.setAttribute(WebConstants.WAIT_TILL_RECEIPT, null);
			}
		}
	}

}
