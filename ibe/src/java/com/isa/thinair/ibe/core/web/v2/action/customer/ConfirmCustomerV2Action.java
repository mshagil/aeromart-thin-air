package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.net.URLDecoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * Confirm Customer Functionality
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Customer.CONFIRM_CUSTOMERV2),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class ConfirmCustomerV2Action extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ConfirmCustomerV2Action.class);

	private String crypt;

	private String emailId;

	private String originCarrier;

	private IBECommonDTO commonParams = new IBECommonDTO();

	public String execute() {
		String forward = StrutsConstants.Result.ERROR;
		try {
			AirCustomerServiceBD customerDelegate = ModuleServiceLocator.getCustomerBD();

			if (originCarrier != null) {
				SessionUtil.initializeIfNot(request);
				SessionUtil.setCarrier(request, originCarrier);
			}
			boolean confirmed = customerDelegate.confirmCustomer(emailId, URLDecoder.decode(crypt, "UTF-8"));
			if (confirmed == false) {
				confirmed = customerDelegate.confirmCustomer(emailId, crypt);
			}
			// Temp fix
			if (confirmed == false) {
				confirmed = customerDelegate.confirmCustomer(emailId, crypt.replace(" ", "+"));
			}

			if (confirmed == true) {
				forward = StrutsConstants.Result.SUCCESS;
			} else {
				forward = StrutsConstants.Result.ERROR;
			}
			SystemUtil.setCommonParameters(request, commonParams);
		} catch (Exception ex) {
			log.error(ex);
			SystemUtil.setCommonParameters(request, commonParams);
			forward = StrutsConstants.Result.ERROR;
		}

		return forward;
	}

	public void setCrypt(String crypt) {
		this.crypt = crypt;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setOriginCarrier(String originCarrier) {
		this.originCarrier = originCarrier;
	}

	public IBECommonDTO getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(IBECommonDTO commonParams) {
		this.commonParams = commonParams;
	}

}
