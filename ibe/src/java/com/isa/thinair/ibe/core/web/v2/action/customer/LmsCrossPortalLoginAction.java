package com.isa.thinair.ibe.core.web.v2.action.customer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.ibe.api.dto.IBECommonDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ReservationWebConstnts;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.constants.WebConstants;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;

/**
 * 
 * @author rumesh
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
 @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Customer.LMS_CROSS_PORTAL_V3),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class LmsCrossPortalLoginAction extends IBEBaseAction {

	private static Log log = LogFactory.getLog(LmsCrossPortalLoginAction.class);

	private IBECommonDTO commonParams = new IBECommonDTO();

	private String menuIntRef;

	public String execute() {
		String forward = "";
		String ffid = SessionUtil.getLoyaltyFFID(request);
		boolean isSuccess = false;

		if (ffid != null && !"".equals(ffid)) {
			try {
				String url = ModuleServiceLocator.getLmsMemberServiceBD().getCrossProtalLoginUrl(ffid, menuIntRef);
				if (url != null && !"".equals(url)) {
					isSuccess = true;
					request.setAttribute(WebConstants.LMS_URL, url);
					request.setAttribute(WebConstants.LOGIN_SUCCESS, isSuccess);
				}
			} catch (ModuleException e) {
				log.error("Error occurred while getting LMS cross portal login url", e);
			}
		}


		if (isSuccess) {
			forward = StrutsConstants.Result.SUCCESS;
		} else {
			forward = StrutsConstants.Result.ERROR;
		}

		composeCommonError();
		SystemUtil.setCommonParameters(request, commonParams);

		return forward;
	}

	public void setMenuIntRef(String menuIntRef) {
		this.menuIntRef = menuIntRef;
	}

	private void composeCommonError() {
		StringBuffer messageBuilder = new StringBuffer();
		messageBuilder.append(I18NUtil.getMessage("lms.cross.protal.login.error"));
		request.setAttribute(ReservationWebConstnts.MSG_ERROR_MESSAGE, messageBuilder.toString());
	}
}
