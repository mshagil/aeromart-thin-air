package com.isa.thinair.ibe.core.web.constants;

public interface SessionAttribute {

	public static final String SESSION_DATA_DTO = "sessionDataDTO";

	public static final String HDN_MODE = "sesHdnMode";

	public static final String KSK_CONF_DATA = "sesKioskConfData";

	public static final String RESERVATION_BALANCE = "reservationBalance";
}
