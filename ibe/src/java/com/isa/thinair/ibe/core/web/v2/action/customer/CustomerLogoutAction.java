package com.isa.thinair.ibe.core.web.v2.action.customer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.SystemUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class CustomerLogoutAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(CustomerLogoutAction.class);

	private boolean success = true;

	private String messageTxt = "";

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			if (!SystemUtil.isKioskUser(request)) {
				CustomerUtil.revertLoyaltyRedemption(request);
				request.getSession().invalidate();
				ForceLoginInvoker.close();
			} else {
				SessionUtil.setCustomerId(request, -1);
				SessionUtil.setLoyaltyAccountNo(request, null);
				SessionUtil.setCustomerEMail(request, null);
				SessionUtil.setCustomerPassword(request, null);
				SessionUtil.setLoyaltyFFID(request, null);
			}
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("CustomerLogoutAction==>", ex);
		}
		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}
}
