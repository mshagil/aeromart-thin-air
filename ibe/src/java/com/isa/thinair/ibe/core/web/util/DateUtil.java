package com.isa.thinair.ibe.core.web.util;

import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;

public final class DateUtil {

	public static Date getCurrentZuluDateTime() {
		return CalendarUtil.getCurrentSystemTimeInZulu();
	}

}
