package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.aircustomer.api.dto.CustomerPreferredMealDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;

/**
 * Action class for IBE Meals
 * 
 * @author Dilan Anuruddha
 * 
 */

@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class InterlineAnciMealAction extends IBEBaseAction {
	private static Log log = LogFactory.getLog(InterlineAnciMealAction.class);

	private boolean success = true;

	private String messageTxt;

	private LCCMealResponceDTO mealResponceDTO;

	private boolean mulipleMealsSelectionEnabled;

	private boolean mealCategoryEnabled;

	private String oldAllSegments;

	private boolean isAnciOffer;

	private String anciOfferName;

	private String anciOfferDescription;

	private String jsonOnds;

	private String pnr;

	private boolean modifySegment;

	private boolean modifyAncillary;
	
	private List<CustomerPreferredMealDTO> preferredMealList;
	
	public String execute() {
		String result = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO ibeResInfo = SessionUtil.getIBEreservationInfo(request);
			String strTxnIdntifier = ibeResInfo.getTransactionId();

			if (log.isDebugEnabled()) {
				log.debug("Transaction identifier : " + strTxnIdntifier);
			}

			SYSTEM system = ibeResInfo.getSelectedSystem();
			String selectedLanguage = SessionUtil.getLanguage(request);
			SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams(),
					(modifySegment && AppSysParamsUtil.isRequoteEnabled()));
			List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();

			SelectedFltSegBuilder totalFltSegBuilder = new SelectedFltSegBuilder(oldAllSegments);

			if (ibeResInfo.getPriceInfoTO() != null) {
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(ibeResInfo.getPriceInfoTO(),
						flightSegmentTOs, totalFltSegBuilder.isReturn(), totalFltSegBuilder.getSelectedFlightSegments());
			} else {

				List<FlightSegmentTO> oldFlightSegmentTOs = totalFltSegBuilder.getSelectedFlightSegments();
				flightSegmentTOs = AnciOfferUtil.populateMissingDataForAnciOfferSearch(oldFlightSegmentTOs, flightSegmentTOs,
						searchParams.getAdultCount(), searchParams.getChildCount(), searchParams.getInfantCount(),
						totalFltSegBuilder.isReturn());
				WebplatformUtil.updateOndSequence(jsonOnds, flightSegmentTOs);
			}

			List<LCCMealRequestDTO> lccMealRequestDTOs = composeMealRequest(flightSegmentTOs, getSearchParams()
					.getClassOfService(), strTxnIdntifier);

			List<BundledFareDTO> bundledFareDTOs = ibeResInfo.getSelectedBundledFares();

			mealResponceDTO = ModuleServiceLocator.getAirproxyAncillaryBD().getAvailableMealsWithTranslations(lccMealRequestDTOs,
					strTxnIdntifier, bundledFareDTOs, system, selectedLanguage, TrackInfoUtil.getBasicTrackInfo(request),
					ApplicationEngine.IBE, pnr);

			setAnciOfferDataToSession(mealResponceDTO, ibeResInfo);
			setAnciOfferDetailsForDisplay(mealResponceDTO);

			mulipleMealsSelectionEnabled = mealResponceDTO.isMultipleMealSelectionEnabled();
			mealCategoryEnabled = mulipleMealsSelectionEnabled;
			
			preferredMealList = CustomerUtil.getCustomerPreferredMeals(SessionUtil.getCustomerId(request));
			
		} catch (Exception e) {
			success = false;
			messageTxt = e.getMessage();
			log.error("MEAL REQ ERROR", e);
		}
		return result;
	}

	/**
	 * Compose the meal request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCMealRequestDTO>
	 */
	private List<LCCMealRequestDTO> composeMealRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCMealRequestDTO> mL = new ArrayList<LCCMealRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCMealRequestDTO mrd = new LCCMealRequestDTO();
			mrd.setTransactionIdentifier(txnId);
			mrd.setCabinClass(cabinClass);
			mrd.getFlightSegment().add(fst);
			mL.add(mrd);
		}
		return mL;
	}

	private void setAnciOfferDataToSession(LCCMealResponceDTO mealDetails, IBEReservationInfoDTO ibeResInfo) {
		for (LCCFlightSegmentMealsDTO segmentMeals : mealDetails.getFlightSegmentMeals()) {

			if (!segmentMeals.isAnciOffer()) {
				continue;
			}

			String flightRef = AnciOfferUtil.getFlightRefWithoutLCCTxnRefDetails(segmentMeals.getFlightSegmentTO()
					.getFlightRefNumber());

			if (ibeResInfo.getAnciOfferTemplates().get(flightRef) == null) {
				ibeResInfo.getAnciOfferTemplates().put(flightRef, new HashMap<EXTERNAL_CHARGES, Integer>());
			}
			ibeResInfo.getAnciOfferTemplates().get(flightRef).put(EXTERNAL_CHARGES.MEAL, segmentMeals.getOfferedTemplateID());
		}
		SessionUtil.setIBEreservationInfo(request, ibeResInfo);
	}

	private void setAnciOfferDetailsForDisplay(LCCMealResponceDTO mealDetails) {
		Collections.sort(mealDetails.getFlightSegmentMeals());
		for (LCCFlightSegmentMealsDTO segmentMeals : mealDetails.getFlightSegmentMeals()) {

			if (segmentMeals.isAnciOffer()) {
				isAnciOffer = true;
				anciOfferName = segmentMeals.getAnciOfferName();
				anciOfferDescription = segmentMeals.getAnciOfferDescription();
				break;
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public LCCMealResponceDTO getMealResponceDTO() {
		return mealResponceDTO;
	}

	public void setMealResponceDTO(LCCMealResponceDTO mealResponceDTO) {
		this.mealResponceDTO = mealResponceDTO;
	}

	public boolean isMulipleMealsSelectionEnabled() {
		return mulipleMealsSelectionEnabled;
	}

	public void setMulipleMealsSelectionEnabled(boolean mulipleMealsSelectionEnabled) {
		this.mulipleMealsSelectionEnabled = mulipleMealsSelectionEnabled;
	}

	public boolean isMealCategoryEnabled() {
		return mealCategoryEnabled;
	}

	public void setMealCategoryEnabled(boolean mealCategoryEnabled) {
		this.mealCategoryEnabled = mealCategoryEnabled;
	}

	/**
	 * @return the oldAllSegments
	 */
	public String getOldAllSegments() {
		return oldAllSegments;
	}

	/**
	 * @param oldAllSegments
	 *            the oldAllSegments to set
	 */
	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	/**
	 * @return the isAnciOffer
	 */
	public boolean isAnciOffer() {
		return isAnciOffer;
	}

	/**
	 * @param isAnciOffer
	 *            the isAnciOffer to set
	 */
	public void setAnciOffer(boolean isAnciOffer) {
		this.isAnciOffer = isAnciOffer;
	}

	/**
	 * @return the anciOfferName
	 */
	public String getAnciOfferName() {
		return anciOfferName;
	}

	/**
	 * @param anciOfferName
	 *            the anciOfferName to set
	 */
	public void setAnciOfferName(String anciOfferName) {
		this.anciOfferName = anciOfferName;
	}

	/**
	 * @return the anciOfferDescription
	 */
	public String getAnciOfferDescription() {
		return anciOfferDescription;
	}

	/**
	 * @param anciOfferDescription
	 *            the anciOfferDescription to set
	 */
	public void setAnciOfferDescription(String anciOfferDescription) {
		this.anciOfferDescription = anciOfferDescription;
	}

	public void setJsonOnds(String jsonOnds) {
		this.jsonOnds = jsonOnds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setModifySegment(boolean modifySegment) {
		this.modifySegment = modifySegment;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public List<CustomerPreferredMealDTO> getPreferredMealList() {
		return preferredMealList;
	}
	
}
