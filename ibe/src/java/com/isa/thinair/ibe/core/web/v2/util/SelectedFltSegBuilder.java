package com.isa.thinair.ibe.core.web.v2.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;

public class SelectedFltSegBuilder {
	private boolean isReturn = false;
	private List<FlightSegmentTO> selectedFlightSegments = null;
	private SYSTEM system = null;
	private List<FlightSegmentTO> allFlightSegments = null;

	public SelectedFltSegBuilder(String jsonSegmentStr, FlightSearchDTO searchParams) throws ParseException {
		this(jsonSegmentStr, searchParams, false);
	}

	public SelectedFltSegBuilder(String jsonSegmentStr, FlightSearchDTO searchParams, boolean isRequote) throws ParseException {
		List<FlightSegmentTO> fsTOList = new ArrayList<FlightSegmentTO>();
		List<FlightSegmentTO> allFlightTOList = new ArrayList<FlightSegmentTO>();
		Boolean isReturn = new Boolean(false);
		SYSTEM tmpSystem = null;
		List<String> modifyingSegments = new ArrayList<String>();
		Map<String, Integer> fltRefOndSeq = new HashMap<String, Integer>();
		if (isRequote) {
			int ondSeq = 0;
			for (ONDSearchDTO ond : searchParams.getOndList()) {
				if ("NEW".equals(ond.getStatus())) {
					modifyingSegments.addAll(ond.getFlightRPHList());
					for (String fltRefNo : ond.getFlightRPHList()) {
						fltRefOndSeq.put(fltRefNo, ondSeq);
					}
				}
				ondSeq++;
			}
		}

		if (jsonSegmentStr != null && !"".equals(jsonSegmentStr)) {
			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(jsonSegmentStr);
			JSONArray jSegments = (JSONArray) jsonObject.get("flightSegments");
			isReturn = new Boolean(jsonObject.get("isReturnFlight").toString());
			Iterator<?> iterator = jSegments.iterator();
			boolean isDomestic = false;

			while (iterator.hasNext()) {
				JSONObject jSeg = (JSONObject) iterator.next();
				if (jSeg.isEmpty())
					continue;
				String flightRefNo = jSeg.get("flightRefNumber").toString();
				String modifyingSegRefNo = flightRefNo;

				Long dtime = new Long(jSeg.get("departureTimeLong").toString());
				Long atime = new Long(jSeg.get("arrivalTimeLong").toString());
				Long dZulu = new Long(jSeg.get("departureTimeZuluLong").toString());
				Long aZulu = new Long(jSeg.get("arrivalTimeZuluLong").toString());
				String segCode = jSeg.get("segmentShortCode").toString();
				String flightNo = jSeg.get("flightNumber").toString();
				String carrierCode = jSeg.get("carrierCode").toString();
				Boolean returnFlag = new Boolean(jSeg.get("returnFlag").toString());
				String pnrSegId = null;
				String csOcCarrierCode = null;
				if(jSeg.get("pnrSegID") != null){
					pnrSegId = jSeg.get("pnrSegID").toString();
				}
				if (jSeg.get("domesticFlight") != null && !"".equals(jSeg.get("domesticFlight"))) {
					isDomestic = new Boolean(jSeg.get("domesticFlight").toString());
				}
				String strSystem = jSeg.get("system").toString();
				if (jSeg.get("csOcCarrierCode") != null && !"".equals(jSeg.get("csOcCarrierCode"))) {
					csOcCarrierCode = jSeg.get("csOcCarrierCode").toString();
				}
				if (tmpSystem == null) {
					tmpSystem = SYSTEM.getEnum(strSystem);
				} else if (tmpSystem != SYSTEM.getEnum(strSystem)) {
					tmpSystem = SYSTEM.INT;
				}
				Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNo);

				FlightSegmentTO fltSeg = new FlightSegmentTO();
				fltSeg.setArrivalDateTime(new Date(atime));
				fltSeg.setDepartureDateTime(new Date(dtime));
				fltSeg.setArrivalDateTimeZulu(new Date(aZulu));
				fltSeg.setDepartureDateTimeZulu(new Date(dZulu));
				fltSeg.setFlightSegId(fltSegId);
				if (fltRefOndSeq.containsKey(flightRefNo)) {
					fltSeg.setOndSequence(fltRefOndSeq.get(flightRefNo));
				}

				// Set cabin class & Logical Cabin Class code
				ReservationBeanUtil.setClassOfServiceInfo(fltSeg, searchParams);
				ReservationBeanUtil.setBookingClassInfo(fltSeg, searchParams);

				fltSeg.setFlightNumber(flightNo);
				if (flightRefNo != null && flightRefNo.indexOf("#") != -1) {
					String[] flightRouteRef = flightRefNo.split("#");
					flightRefNo = flightRouteRef[0];
					fltSeg.setRouteRefNumber(flightRouteRef[1]);
				}
				fltSeg.setFlightRefNumber(flightRefNo);
				fltSeg.setOperatingAirline(carrierCode);
				fltSeg.setSegmentCode(segCode);
				fltSeg.setReturnFlag(returnFlag);
				fltSeg.setDomesticFlight(isDomestic);
				fltSeg.setPnrSegId(pnrSegId);
				fltSeg.setCsOcCarrierCode(csOcCarrierCode);

				allFlightTOList.add(fltSeg);
				if (isRequote && !modifyingSegments.contains(modifyingSegRefNo)) {
					continue;
				}

				if (jSeg.get("baggageOndGroupId") != null) {
					fltSeg.setBaggageONDGroupId(jSeg.get("baggageOndGroupId").toString());
				}

				fsTOList.add(fltSeg);
			}
		}
		this.selectedFlightSegments = fsTOList;
		this.allFlightSegments = allFlightTOList;
		this.isReturn = isReturn;
		this.system = tmpSystem;
	}

	public SelectedFltSegBuilder(String jsonSegmentStr) throws ParseException {
		List<FlightSegmentTO> fsTOList = new ArrayList<FlightSegmentTO>();
		Boolean isReturn = new Boolean(false);
		String strSystem = null;

		if (jsonSegmentStr != null && !"".equals(jsonSegmentStr) && !"[]".equals(jsonSegmentStr)) {
			JSONParser parser = new JSONParser();
			JSONArray jSegments = (JSONArray) parser.parse(jsonSegmentStr);
			Iterator<?> iterator = jSegments.iterator();
			while (iterator.hasNext()) {
				JSONObject jSeg = (JSONObject) iterator.next();
				if (jSeg.isEmpty())
					continue;
				Date dtime = parseJsonDate(jSeg.get("departureDate").toString());
				Date atime = parseJsonDate(jSeg.get("arrivalDate").toString());
				Date dZulu = parseJsonDate(jSeg.get("departureDateZulu").toString());
				Date aZulu = parseJsonDate(jSeg.get("arrivalDateZulu").toString());
				String segCode = jSeg.get("segmentCode").toString();
				String flightRefNo = jSeg.get("flightSegmentRefNumber").toString();
				if (flightRefNo != null && flightRefNo.indexOf("#") != -1) {
					flightRefNo = flightRefNo.split("#")[0];
				}
				String flightNo = jSeg.get("flightNo").toString();
				String carrierCode = jSeg.get("carrierCode").toString();
				String cabinClass = jSeg.get("cabinClassCode").toString();
				strSystem = jSeg.get("system").toString();
				String strStaus = jSeg.get("status").toString();
				String strRetFlag = jSeg.get("returnFlag").toString();
				int journeySequence = Integer.valueOf(jSeg.get("journeySequence") == null ? "0" : jSeg.get("journeySequence")
						.toString());
				int segmentSequence = Integer.valueOf(jSeg.get("segmentSeq") == null ? "0" : jSeg.get("segmentSeq").toString());

				Boolean returnFlag = false;
				if ("Y".equals(strRetFlag)) {
					returnFlag = true;
				}
				FlightSegmentTO fltSeg = new FlightSegmentTO();
				fltSeg.setArrivalDateTime(atime);
				fltSeg.setDepartureDateTime(dtime);
				fltSeg.setArrivalDateTimeZulu(aZulu);
				fltSeg.setDepartureDateTimeZulu(dZulu);

				fltSeg.setCabinClassCode(cabinClass);
				fltSeg.setFlightNumber(flightNo);
				fltSeg.setFlightRefNumber(flightRefNo);
				fltSeg.setOperatingAirline(carrierCode);
				fltSeg.setSegmentCode(segCode);
				fltSeg.setReturnFlag(returnFlag);
				fltSeg.setOndSequence(journeySequence - 1);
				fltSeg.setSegmentSequence(segmentSequence);
				fltSeg.setFlexiID(jSeg.get("flexiRuleID") == null || jSeg.get("flexiRuleID").toString().isEmpty()
						? null
						: Integer.parseInt(jSeg.get("flexiRuleID").toString()));

				if (jSeg.get("fareTO") != null) {
					JSONObject jsonFareTO = (JSONObject) jSeg.get("fareTO");
					fltSeg.setFareBasisCode(jsonFareTO.get("fareBasisCode") == null ? null : jsonFareTO.get("fareBasisCode")
							.toString());
					fltSeg.setBookingClass(jsonFareTO.get("bookingClassCode") == null ? null : jsonFareTO.get("bookingClassCode")
							.toString());
				}

				if (strStaus == null || strStaus.equals("CNF")) {
					fsTOList.add(fltSeg);
				}

			}
			Object[] outboundInboundFlifghts = AncillaryDTOUtil.getOutboundInboundList(fsTOList);
			List<FlightSegmentTO> inboundFlightSegmentTOs = (List<FlightSegmentTO>) outboundInboundFlifghts[1];

			IFlightSegment inboundFlightSegmentTO = inboundFlightSegmentTOs.size() == 0 ? null : inboundFlightSegmentTOs
					.get(inboundFlightSegmentTOs.size() - 1);
			if (inboundFlightSegmentTO != null) {
				isReturn = true;
			}
		}

		this.selectedFlightSegments = fsTOList;
		this.allFlightSegments = fsTOList;
		this.isReturn = isReturn;
		this.system = SYSTEM.getEnum(strSystem);
	}

	public boolean isReturn() {
		return this.isReturn;
	}

	public List<FlightSegmentTO> getSelectedFlightSegments() {
		return this.selectedFlightSegments;
	}

	public SYSTEM getSystem() {
		return this.system;
	}

	public List<FlightSegmentTO> getAllFlightSegments() {
		return allFlightSegments;
	}

	private Date parseJsonDate(String jsonDate) {
		Date d = null;
		if (jsonDate == null || "".equals(jsonDate)) {
			return d;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			d = sdf.parse(jsonDate);
		} catch (java.text.ParseException e) {
			d = null;
		}
		return d;
	}
}
