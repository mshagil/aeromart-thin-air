package com.isa.thinair.ibe.core.web.v2.action.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airproxy.api.dto.RedeemCalculateRQ;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.api.dto.IBEReservationPostPaymentDTO;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.BeanUtil;
import com.isa.thinair.ibe.core.web.util.CustomerUtil;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.action.system.IBEBaseAction;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.ibe.core.web.v2.util.SearchUtil;
import com.isa.thinair.ibe.core.web.v2.util.SelectedFltSegBuilder;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * Action class for redeeming loyalty points
 * 
 * @author rumesh
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LoyaltyPaymentHandleAction extends IBEBaseAction {

	private static final Log log = LogFactory.getLog(LoyaltyPaymentHandleAction.class);

	private boolean success = true;

	private String paxWiseAnci;

	private boolean makePayment = false;

	private boolean modifyAncillary;

	private boolean addGroundSegment;

	private boolean modifySegment;

	private boolean requoteFlightSearch = false;

	private boolean groupPNR;

	private String pnr = null;

	private String oldAllSegments;

	private String modifySegmentRefNos;

	private String insurance;

	private int oldFareID;

	private int oldFareType;

	private String oldFareAmount;

	private BigDecimal maxRedeemAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private double memberAvailablePoints = 0;

	private BigDecimal memberAvailableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private boolean lmsEnabled = true;

	@SuppressWarnings("unchecked")
	public String getRedeemBreakdown() {

		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		RedeemCalculateRQ redeemCalculateRQ = new RedeemCalculateRQ();

		try {

			String memberAccountId = SessionUtil.getLoyaltyFFID(request);

			if (!AppSysParamsUtil.isLMSEnabled() || BeanUtils.nullHandler(memberAccountId).equals("")) {
				lmsEnabled = false;
				success = false;
				return StrutsConstants.Result.SUCCESS;
			}

			FlightPriceRQ flightPriceRQ = null;
			Collection<ReservationPaxTO> paxList = null;
			List<FlightSegmentTO> flightSegmentTOs = null;

			if (isPaymentRetry()) {
				IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
				validateData(postPayDTO);
				flightPriceRQ = postPayDTO.getFlightPriceRQ();
				pnr = postPayDTO.getPnr();
				paxList = postPayDTO.getPaxList();
				flightSegmentTOs = postPayDTO.getSelectedFlightSegments();
				
			} else {

				SelectedFltSegBuilder fltSegBuilder = new SelectedFltSegBuilder(getSelectedFlightJson(), getSearchParams());
				flightSegmentTOs = getFlightSegments(fltSegBuilder);
				flightPriceRQ = getFlightPriceRQ(flightSegmentTOs);

				paxList = getPaxList(flightSegmentTOs, fltSegBuilder.isReturn(),
						resInfo.getAnciOfferTemplates());
			}

			Map<String, List<String>> carrierWiseFlightRPH = new HashMap<String, List<String>>();
			Map<String, String> busAirCarrierCodes = ModuleServiceLocator.getFlightBD().getBusAirCarrierCodes();
			if (flightSegmentTOs != null) {
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					String operatingCarrier = FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber());
					if (busAirCarrierCodes.containsKey(operatingCarrier)) {
						operatingCarrier = busAirCarrierCodes.get(operatingCarrier);
					}

					if (operatingCarrier != null && carrierWiseFlightRPH.containsKey(operatingCarrier)) {
						carrierWiseFlightRPH.get(operatingCarrier).add(flightSegmentTO.getFlightRefNumber());
					} else {
						List<String> flightRefNumList = new ArrayList<String>();
						flightRefNumList.add(flightSegmentTO.getFlightRefNumber());
						carrierWiseFlightRPH.put(operatingCarrier, flightRefNumList);
					}
				}

			}

			redeemCalculateRQ.setMemberAccountID(memberAccountId);
			redeemCalculateRQ.setSystem(resInfo.getSelectedSystem());

			String strTxnIdntifier = resInfo.getTransactionId();
			redeemCalculateRQ.setTxnIdntifier(strTxnIdntifier);
			redeemCalculateRQ.setCarrierWiseFlightRPH(carrierWiseFlightRPH);
			redeemCalculateRQ.setFlightPriceRQ(flightPriceRQ);
			redeemCalculateRQ.setPnr(pnr);

			Double remainingLoyaltyPoints = CustomerUtil.getMemberAvailablePoints(request);

			redeemCalculateRQ.setRemainingPoint(remainingLoyaltyPoints);

			if (requoteFlightSearch) {
				redeemCalculateRQ.setPaxCarrierProductDueAmount(WebplatformUtil.populatePaxProductDueAmounts(resInfo
						.getlCCClientReservationBalance()));

			} else if (makePayment) {
				redeemCalculateRQ.setPaxCarrierProductDueAmount(ModuleServiceLocator.getAirproxyReservationBD()
						.getPaxProduDueAmount(pnr, groupPNR, false, remainingLoyaltyPoints));
				redeemCalculateRQ.setPayForOHD(true);
			} else {

				FareSegChargeTO fareSegChargeTO = null;
				if (resInfo.getPriceInfoTO() != null) {
					fareSegChargeTO = resInfo.getPriceInfoTO().getFareSegChargeTO();
				}

				boolean isFlexiQuote = getSearchParams().isFlexiSelected();

				if (getSearchParams().isFlexiSelected()) {
					ReservationUtil.applyFlexiCharges(paxList, getSearchParams().getOndSelectedFlexi(), resInfo.getPriceInfoTO(),
							flightSegmentTOs);
				}

				Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = WebplatformUtil
						.populatePaxExternalCharges(paxList);
				redeemCalculateRQ.setPaxCarrierExternalCharges(paxCarrierExternalCharges);
				redeemCalculateRQ.setFareSegChargeTo(fareSegChargeTO);
				redeemCalculateRQ.setFlexiQuote(isFlexiQuote);
			}

			ReservationUtil.setDiscountCalculatorRQ(paxList,resInfo,  redeemCalculateRQ);
			
			TrackInfoDTO trackInfoDTO = getTrackInfo();
			trackInfoDTO.setAppIndicator(getTrackInfo().getAppIndicator());

			ServiceResponce serviceResponce = ModuleServiceLocator.getAirproxyReservationBD().calculateLoyaltyPointRedeemables(
					redeemCalculateRQ, trackInfoDTO);

			resInfo.setCarrierWiseLoyaltyPaymentInfo(null);

			if (serviceResponce.isSuccess()) {
				Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts = (Map<Integer, Map<String, BigDecimal>>) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT);

				if (paxWiseProductAmounts != null) {
					for (Entry<Integer, Map<String, BigDecimal>> paxEntry : paxWiseProductAmounts.entrySet()) {
						Map<String, BigDecimal> productAmounts = paxEntry.getValue();
						for (BigDecimal amount : productAmounts.values()) {
							if (amount != null) {
								maxRedeemAmount = AccelAeroCalculator.add(maxRedeemAmount, amount);
							}
						}
					}

				}

				memberAvailablePoints = (Double) serviceResponce.getResponseParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS);
				memberAvailableAmount = (BigDecimal) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS_AMOUNT);
			} else {
				log.error("Error in calculating redeemable amount for member: " + memberAccountId);
			}

		} catch (Exception e) {
			success = false;
		}

		return StrutsConstants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public void setMakePayment(boolean makePayment) {
		this.makePayment = makePayment;
	}

	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	public void setAddGroundSegment(boolean addGroundSegment) {
		this.addGroundSegment = addGroundSegment;
	}

	public void setRequoteFlightSearch(boolean requoteFlightSearch) {
		this.requoteFlightSearch = requoteFlightSearch;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public BigDecimal getMaxRedeemAmount() {
		return maxRedeemAmount;
	}

	public double getMemberAvailablePoints() {
		return memberAvailablePoints;
	}

	public BigDecimal getMemberAvailableAmount() {
		return memberAvailableAmount;
	}

	public void setGroupPNR(boolean groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public void setOldFareID(int oldFareID) {
		this.oldFareID = oldFareID;
	}

	public void setOldFareType(int oldFareType) {
		this.oldFareType = oldFareType;
	}

	public void setOldFareAmount(String oldFareAmount) {
		this.oldFareAmount = oldFareAmount;
	}

	public boolean isLmsEnabled() {
		return lmsEnabled;
	}

	private Collection<ReservationPaxTO> getPaxList(List<FlightSegmentTO> flightSegmentTOs, boolean isReturn,
			Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplates) throws ModuleException, ParseException,
			java.text.ParseException {

		List<LCCInsuranceQuotationDTO> insuranceQuotations = null;
		LCCInsuredJourneyDTO insJrnyDto = null;
		boolean isRakInsurance = AppSysParamsUtil.isRakEnabled();

		if (SessionUtil.getIbePromotionPaymentPostDTO(request) == null) {
			if (!addGroundSegment) {
				insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, isReturn, getTrackInfo().getOriginChannelId());
			} else {
				insJrnyDto = AncillaryDTOUtil.getInsuranceJourneyDetails(flightSegmentTOs, isReturn, getTrackInfo().getOriginChannelId());
			}
		}

		if (!makePayment) {
			insuranceQuotations = AncillaryJSONUtil.getInsuranceQuotation(insurance, insJrnyDto);
		}

		Collection<ReservationPaxTO> paxList = AncillaryJSONUtil.extractReservationPax(paxWiseAnci, insuranceQuotations,
				getApplicationEngine(getTrackInfo().getOriginChannelId()), flightSegmentTOs, null, anciOfferTemplates);

		return paxList;
	}

	private FlightPriceRQ getFlightPriceRQ(List<FlightSegmentTO> flightSegmentTOs) throws Exception {
		FlightPriceRQ flightPriceRQ = null;
		if (!modifyAncillary && !makePayment) {
			AvailableFlightSearchDTOBuilder avilBuilder = SearchUtil.getSearchBuilder(getSearchParams());
			if (modifySegment && !groupPNR) {
				SearchUtil.setModifingSegmentSearchData(avilBuilder, oldFareID, oldFareType, oldFareAmount);
			}
			if (pnr != null && !"".equals(pnr)) {
				SearchUtil.setExistingSegInfo(avilBuilder.getSearchDTO(), oldAllSegments, modifySegmentRefNos);
			}
			flightPriceRQ = BeanUtil.createFareQuoteRQ(avilBuilder.getSearchDTO(), flightSegmentTOs, getSearchParams()
					.getClassOfService(), getSearchParams().getLogicalCabinClass(), getSearchParams()
					.getFareQuoteLogicalCCSelection());
		}

		return flightPriceRQ;
	}

	private List<FlightSegmentTO> getFlightSegments(SelectedFltSegBuilder fltSegBuilder) {
		IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
		List<FlightSegmentTO> flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
		SortUtil.sortFlightSegByDepDate(flightSegmentTOs);

		if (resInfo != null && resInfo.getPriceInfoTO() != null) {
			for (FlightSegmentTO flightSegment : flightSegmentTOs) {
				for (FareRuleDTO fareRule : resInfo.getPriceInfoTO().getFareTypeTO().getApplicableFareRules()) {
					if (fareRule.getOrignNDest().contains(flightSegment.getSegmentCode())) {
						flightSegment.setCabinClassCode(fareRule.getCabinClassCode());
						flightSegment.setOndSequence(fareRule.getOndSequence());
						break;
					}
				}
			}
		}

		return flightSegmentTOs;
	}

	private boolean isOwnBooking() {
		if (isPaymentRetry()) {
			IBEReservationPostPaymentDTO postPayDTO = SessionUtil.getIBEReservationPostPaymentDTO(request);
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			if (postPayDTO != null && resInfo != null) {
				if ((postPayDTO.isMakePayment() || postPayDTO.isAddModifyAncillary() || postPayDTO.isRequoteFlightSearch())
						&& !groupPNR) {
					return true;
				} else if (!(postPayDTO.isMakePayment() || postPayDTO.isAddModifyAncillary() || postPayDTO
						.isRequoteFlightSearch()) && resInfo.getSelectedSystem().equals(SYSTEM.AA)) {
					return true;
				}
			}
		} else {
			if ((makePayment || modifyAncillary || requoteFlightSearch) && !groupPNR) {
				return true;
			} else if (!(makePayment || modifyAncillary || requoteFlightSearch)
					&& getSearchParams().getSearchSystem().equals(SYSTEM.AA.toString())) {
				return true;
			}
		}

		return false;
	}

	private boolean isPaymentRetry() {
		return RequestParameterUtil.isPaymentRetry(request);
	}

	private void validateData(Object data) throws ModuleException {
		if (data == null) {
			throw new ModuleException("server.default.operation.fail");
		}
	}
}
