package com.isa.thinair.ibe.core.web.v2.action.modifyRes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.ibe.api.dto.IBEReservationInfoDTO;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.AirportDDListGenerator;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;

/**
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ModifySegmentAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ModifySegmentAction.class);

	private boolean success = true;

	private String messageTxt;

	private String segmentRefNo;

	private String oldAllSegments;

	private HashMap<String, String> jsonLabel;

	private List<String[]> fromAirportData;

	private List<String[]> toAirportData;

	@SuppressWarnings("unchecked")
	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			IBEReservationInfoDTO resInfo = SessionUtil.getIBEreservationInfo(request);
			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
			Collection<LCCClientReservationSegment> sentSegs = ReservationUtil.getvalidateModifingSegment(colsegs,
					resInfo.getSegmentStatus(), segmentRefNo);
			String strLanguage = SessionUtil.getLanguage(request);
			String[] pagesIDs = { "Form", "PgModifySement", "Common" };
			jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
			this.prepareGroundSegmentData((ArrayList<LCCClientReservationSegment>) sentSegs);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("showCustomerModifySegmentAction==>" + ex);
		}
		return forward;
	}

	private void prepareGroundSegmentData(List<LCCClientReservationSegment> sentSegs) throws JSONException, ModuleException {

		StringBuilder newGroundStations = new StringBuilder();
		boolean isSubStationsAllowed = AppSysParamsUtil.isGroundServiceEnabled();
		if (isSubStationsAllowed) {
			String strOnd = null;
			if (sentSegs == null || sentSegs.size() != 1 || sentSegs.get(0).getFareTO() != null) {
				LCCClientReservationSegment sentSeg = sentSegs.get(0);
				if (sentSeg.getSubStationShortName() != null) {
					strOnd = sentSeg.getFareTO().getSegmentCode();
					List<String[]>[] arrList = SubStationUtil.getGroundSegmentModifyCombo(strOnd);
					fromAirportData = AirportDDListGenerator.createIBEAirportDDList(arrList[0]);
					toAirportData = AirportDDListGenerator.createIBEAirportDDList(arrList[1]);
				}
			}
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setSegmentRefNo(String segmentRefNo) {
		this.segmentRefNo = segmentRefNo;
	}

	public void setOldAllSegments(String oldAllSegments) {
		this.oldAllSegments = oldAllSegments;
	}

	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	public List<String[]> getFromAirportData() {
		return fromAirportData;
	}

	public void setFromAirportData(List<String[]> fromAirportData) {
		this.fromAirportData = fromAirportData;
	}

	public List<String[]> getToAirportData() {
		return toAirportData;
	}

	public void setToAirportData(List<String[]> toAirportData) {
		this.toAirportData = toAirportData;
	}

}
