package com.isa.thinair.ibe.core.web.v2.action.voucher;

import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.BookingUtil;
import com.isa.thinair.ibe.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.ibe.core.web.v2.util.ReservationUtil;
import com.isa.thinair.promotion.api.service.VoucherTemplateBD;
import com.isa.thinair.promotion.api.to.voucherTemplate.VoucherTemplateTo;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author chanaka
 *
 */
@SuppressWarnings("unchecked")
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR)

})
public class ShowVoucherDetailsAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(ShowVoucherDetailsAction.class);

	private boolean showPaymentIcons;

	private Set<String> paymentMethods;

	private ArrayList<VoucherTemplateTo> voucherTemplateList;

	private String systemDate;

	private String userIp;
	
	private static final String VOUCHERPAGEID = "Voucher";
	
	private HashMap<String, String> jsonLabel;

	private Map<String, String> errorInfo;
	
	private boolean success = true;
	
	private String messageTxt;
	
	private String baseCurrency;

	public String execute() {

		if (AppSysParamsUtil.isVoucherEnabled()) {
			VoucherTemplateBD voucherTemplateDelegate = ModuleServiceLocator.getVoucherTemplateBD();
			String strLanguage = SessionUtil.getLanguage(request);
			try {
				setVoucherTemplateList(voucherTemplateDelegate.getVoucherInSalesPeriod());
				String[] pagesIDs = {VOUCHERPAGEID};
				jsonLabel = I18NUtil.getMessagesInJSON(strLanguage, pagesIDs);
				errorInfo = ErrorMessageUtil.getVoucherErrors(request);
				baseCurrency = AppSysParamsUtil.getBaseCurrency();
			} catch (Exception e) {
				success = false;
				messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
				log.error("ShowGiftVoucherDetailAction ==> execute()", e);
				
			}

			showPaymentIcons = AppSysParamsUtil.showPaymentMethodsOnIBE();

			if (showPaymentIcons) {
				try {
					String countryCode = ReservationUtil.getCountryCode(request, getClientInfoDTO().getIpAddress());

					// Get applicable payments methods
					boolean enableOfflinePaymentImages = false;
					PaymentMethodDetailsDTO paymentMethodDetailsDTO =	(new PaymentMethodDetailsDTO())
							.setCountryCode(countryCode)
							.setEnableOffline(enableOfflinePaymentImages);

					paymentMethods = ModuleServiceLocator.getPaymentBrokerBD().getPaymentMethodNames(paymentMethodDetailsDTO);

				} catch (ModuleException e) {
					success = false;
					messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
					log.error("ShowVoucherDetailAction ==> execute()", e);
				}

				// Add System default icons
				paymentMethods.addAll(AppSysParamsUtil.getDefaultPaymentMethods());
				userIp = BookingUtil.getURLData(request).getUserIp();

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				systemDate = sdf.format(Calendar.getInstance().getTime());

			}
		}

		return StrutsConstants.Result.SUCCESS;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public ArrayList<VoucherTemplateTo> getVoucherTemplateList() {
		return voucherTemplateList;
	}

	public void setVoucherTemplateList(ArrayList<VoucherTemplateTo> voucherTemplateList) {
		this.voucherTemplateList = voucherTemplateList;
	}

	public boolean isShowPaymentIcons() {
		return showPaymentIcons;
	}

	public void setShowPaymentIcons(boolean showPaymentIcons) {
		this.showPaymentIcons = showPaymentIcons;
	}

	public Set<String> getPaymentMethods() {
		return paymentMethods;
	}

	public String getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}

	public void setPaymentMethods(Set<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	/**
	 * @return the jsonLabel
	 */
	public HashMap<String, String> getJsonLabel() {
		return jsonLabel;
	}

	/**
	 * @param jsonLabel the jsonLabel to set
	 */
	public void setJsonLabel(HashMap<String, String> jsonLabel) {
		this.jsonLabel = jsonLabel;
	}

	/**
	 * @return the errorInfo
	 */
	public Map<String, String> getErrorInfo() {
		return errorInfo;
	}

	/**
	 * @param errorInfo the errorInfo to set
	 */
	public void setErrorInfo(Map<String, String> errorInfo) {
		this.errorInfo = errorInfo;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the baseCurrency
	 */
	public String getBaseCurrency() {
		return baseCurrency;
	}

	/**
	 * @param baseCurrency the baseCurrency to set
	 */
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

}
