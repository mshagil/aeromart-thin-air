package com.isa.thinair.ibe.core.web.v2.action.customer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.dispatcher.ServletActionRedirectResult;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.ibe.core.service.IBEModuleUtils;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.ExceptionConstants;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.util.ScheduleUtils;
import com.isa.thinair.ibe.core.web.util.SessionUtil;
import com.isa.thinair.ibe.core.web.v2.util.I18NUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.dto.FlightLegTO;
import com.isa.thinair.webplatform.api.dto.FlightScheduleTO;
import com.isa.thinair.webplatform.api.dto.FlightSegmentTO;

/**
 * @author Jagath Kumara
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
		@Result(name = StrutsConstants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = StrutsConstants.Result.SESSION_EXPIRED, type = ServletActionRedirectResult.class, value = StrutsConstants.Action.SESSION_EXPIRED) })
public class ShowFlightScheduleRowAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowReservationListAction.class);

	private boolean success = true;

	private static GlobalConfig globalConfig = IBEModuleUtils.getGlobalConfig();

	private static final String defaultFormat // get the default date format
	= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);

	private Collection<FlightScheduleTO> obFlightScheduleTOList;
	private Collection<FlightScheduleTO> ibFlightScheduleTOList;

	private HashMap<String, String> jsonLabel;

	private String dispatchMode;

	private String messageTxt;

	private String searchFromStation;

	private String searchToStation;

	private String isRoundTrip;

	private String language;

	private static final String strSMDateFormat = "dd/MM/yyyy";

	private static final String strSMDateYearFormat = "MM/dd/yyyy HH:mm:ss";

	private static String dayOffSet = "1";

	public String execute() {
		String forward = StrutsConstants.Result.SUCCESS;
		try {
			String strLanguage = SessionUtil.getLanguage(request);
			Collection colSchedules = null;

			// fields relavant to both searchs (default/non-default)
			String strStartDate = "";
			String strStopDate = "";

			Date dStartDate = null;
			Date dStopDate = null;

			String arFrom = "";
			String arTo = "";
			String arSHJ = "";
			String tmp = null;
			tmp = request.getParameter("arFrom");
			if (tmp != null)
				arFrom = getUnicode(tmp);
			tmp = request.getParameter("arTo");
			if (tmp != null)
				arTo = getUnicode(tmp);
			tmp = request.getParameter("arSHJ");
			if (tmp != null)
				arSHJ = getUnicode(tmp);

			String specialFltFromDate = request.getParameter("sffd");
			String specialFltToDate = request.getParameter("sftd");
			String specialFltNo = request.getParameter("sfno");
			String specialFltDest = request.getParameter("sfd");
			// request.setAttribute("sffd", specialFltFromDate);
			// request.setAttribute("sftd", specialFltToDate);
			// request.setAttribute("sfno", specialFltNo);
			// request.setAttribute("sfd", specialFltDest);

			String startDate = request.getParameter("startDate");
			String strFrom = request.getParameter("fromDst");
			this.setSearchFromStation(strFrom);
			String strTo = request.getParameter("toDst");
			this.setSearchToStation(strTo);
			String roundTrip = request.getParameter("roundTrip");
			this.setIsRoundTrip(roundTrip);
			String lang = request.getParameter("lang");
			this.setLanguage(lang);
			Locale locale = new Locale(lang);
			Config.set(request.getSession(), Config.FMT_LOCALE, locale);
			if (startDate == null || strFrom == null || strTo == null || lang == null)
				return "";
			if (startDate.length() == 0 || strFrom.length() == 0 || strTo.length() == 0 || lang.length() == 0)
				return "";
			if (roundTrip == null)
				roundTrip = "false";

			Hashtable<String, String> hash = new Hashtable<String, String>();
			hash.put("01", "31");
			hash.put("02", "28");
			hash.put("03", "31");
			hash.put("04", "30");
			hash.put("05", "31");
			hash.put("06", "30");
			hash.put("07", "31");
			hash.put("08", "31");
			hash.put("09", "30");
			hash.put("10", "31");
			hash.put("11", "30");
			hash.put("12", "31");

			String[] mon_year = StringUtils.split(startDate, "/");
			strStartDate = "01/" + startDate; // the date format recivied as mm/yyyy
			int year = Integer.valueOf(mon_year[1]);
			String mon = hash.get(mon_year[0]);
			if (year % 4 == 0 && mon_year[1].equalsIgnoreCase("02"))
				mon = "29";
			strStopDate = mon + "/" + startDate;
			DateFormat formatter = new SimpleDateFormat(strSMDateFormat);
			try {
				dStartDate = formatter.parse(strStartDate);
				dStopDate = formatter.parse(strStopDate);
			} catch (Exception e) {
				log.error(e);
			}

			int nData = 0;
			ArrayList<String> dstFrom = new ArrayList<String>();
			ArrayList<String> dstTo = new ArrayList<String>();
			ArrayList<String> dstFromAr = new ArrayList<String>();
			ArrayList<String> dstToAr = new ArrayList<String>();
			dstFrom.add(strFrom);
			dstFromAr.add(arFrom);
			String hub = globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
			if (!strFrom.equalsIgnoreCase(hub)) {
				dstTo.add(hub);
				dstToAr.add(arSHJ);
				if (!strTo.equalsIgnoreCase(hub)) {
					dstFrom.add(hub);
					dstFromAr.add(arSHJ);
					dstTo.add(strTo);
					dstToAr.add(arTo);
				}
			} else {
				dstTo.add(strTo);
				dstToAr.add(arTo);
			}

			int len = dstFrom.size();
			for (int j = 0; j < len; j++) {
				colSchedules = getScheduleList(dStartDate, dStopDate, dstFrom.get(j), dstTo.get(j));
				if (colSchedules.isEmpty())
					return "";
				obFlightScheduleTOList = composeFlightScheduleListTO(colSchedules, nData++, lang, dstFromAr.get(j),
						dstToAr.get(j));

			}
			if (roundTrip.equalsIgnoreCase("true")) {
				for (int j = len - 1; j >= 0; j--) {
					colSchedules = getScheduleList(dStartDate, dStopDate, dstTo.get(j), dstFrom.get(j));
					if (colSchedules.isEmpty())
						return "";
					ibFlightScheduleTOList = composeFlightScheduleListTO(colSchedules, nData++, lang, dstToAr.get(j),
							dstFromAr.get(j));

				}
			}

		} catch (ModuleException me) {
			success = false;
			messageTxt = me.getMessageString();
			log.error("ShowReservationListAction==>", me);
		} catch (Exception ex) {
			success = false;
			messageTxt = I18NUtil.getMessage(ExceptionConstants.SERVER_OPERATION_FAIL, SessionUtil.getLanguage(request));
			log.error("ShowReservationListAction==>", ex);
		}

		return forward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Collection<FlightScheduleTO> getObFlightScheduleTOList() {
		return obFlightScheduleTOList;
	}

	public void setObFlightScheduleTOList(Collection<FlightScheduleTO> obFlightScheduleTOList) {
		this.obFlightScheduleTOList = obFlightScheduleTOList;
	}

	public Collection<FlightScheduleTO> getIbFlightScheduleTOList() {
		return ibFlightScheduleTOList;
	}

	public void setIbFlightScheduleTOList(Collection<FlightScheduleTO> ibFlightScheduleTOList) {
		this.ibFlightScheduleTOList = ibFlightScheduleTOList;
	}

	/*
	 * public String getDispatchMode() { return dispatchMode; }
	 * 
	 * public void setDispatchMode(String dispatchMode) { this.dispatchMode = dispatchMode; }
	 * 
	 * public HashMap<String, String> getJsonLabel() { return jsonLabel; }
	 */

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getSearchFromStation() {
		return searchFromStation;
	}

	public void setSearchFromStation(String searchFromStation) {
		this.searchFromStation = searchFromStation;
	}

	public String getSearchToStation() {
		return searchToStation;
	}

	public void setSearchToStation(String searchToStation) {
		this.searchToStation = searchToStation;
	}

	public String getIsRoundTrip() {
		return isRoundTrip;
	}

	public void setIsRoundTrip(String isRoundTrip) {
		this.isRoundTrip = isRoundTrip;
	}

	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	public static String getUnicode(String in) {
		String out = "";
		if (in != null && in.length() > 0) {
			char c4;
			StringBuffer output = new StringBuffer();
			String subStr = null;
			for (int i = 0; i < in.length(); i = i + 4) {
				subStr = in.substring(i, i + 4);
				c4 = (char) Integer.parseInt(subStr, 10);
				output.append(c4);
			}
			out = output.toString();
		}
		return out;
	}

	private static Collection getScheduleList(Date dStartDate, Date dStopDate, String strFrom, String strTo)
			throws ModuleException {
		Page page = null;
		Collection colSchedules = null;
		String strOperationtype = "";
		String strStatus = "";
		String strBuildStatus = "";

		// fields not relevant to the default search
		strOperationtype = "2";
		strStatus = "ACT";
		strBuildStatus = "BLT";

		// setting the criteria list to search
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		// setting the start date as criteria
		if (dStartDate != null) {

			ModuleCriterion moduleCriterionStartD = new ModuleCriterion();

			moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.START_DATE); // start
																												// date
																												// of
																												// the
																												// schedule

			List<Date> valueStartDate = new ArrayList<Date>();
			valueStartDate.add(dStartDate);

			moduleCriterionStartD.setValue(valueStartDate);

			critrian.add(moduleCriterionStartD);
		}
		// setting the stop date as criteria
		if (dStopDate != null) {

			ModuleCriterion moduleCriterionStopD = new ModuleCriterion();

			moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STOP_DATE); // stop date
																											// of the
																											// schedule

			List<Date> valueStopDate = new ArrayList<Date>();
			valueStopDate.add(dStopDate);

			moduleCriterionStopD.setValue(valueStopDate);

			critrian.add(moduleCriterionStopD);
		}
		// setting the operation type as criteria
		if (ScheduleUtils.isNotEmptyOrNull(strOperationtype)) {

			ModuleCriterion moduleCriterionOptType = new ModuleCriterion();

			moduleCriterionOptType.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionOptType.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.OPERATION_TYPE_ID);// operation
																														// type
																														// of
																														// the
																														// schedule

			List<Integer> valueOptType = new ArrayList<Integer>();
			valueOptType.add(new Integer(strOperationtype));

			moduleCriterionOptType.setValue(valueOptType);

			critrian.add(moduleCriterionOptType);
		}
		// setting the schedule status as criteria
		if (ScheduleUtils.isNotEmptyOrNull(strStatus)) {

			ModuleCriterion moduleCriterionStatus = new ModuleCriterion();

			moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STATUS_CODE); // status
																												// code
																												// of
																												// the
																												// schedule

			List<String> valueStatus = new ArrayList<String>();
			valueStatus.add(strStatus);

			moduleCriterionStatus.setValue(valueStatus);

			critrian.add(moduleCriterionStatus);
		}
		// setting the schedule build status as criteria
		if (ScheduleUtils.isNotEmptyOrNull(strBuildStatus)) {

			ModuleCriterion moduleCriterionBuildStatus = new ModuleCriterion();

			moduleCriterionBuildStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionBuildStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.BUILD_STATUS_CODE); // build
																															// status
																															// of
																															// the
																															// schedule

			List<String> valueBuildStatus = new ArrayList<String>();
			valueBuildStatus.add(strBuildStatus);

			moduleCriterionBuildStatus.setValue(valueBuildStatus);

			critrian.add(moduleCriterionBuildStatus);
		}
		// setting the flight number as criteria
		/*
		 * if (ScheduleUtils.isNotEmptyOrNull(strFlightNo) || ScheduleUtils.isNotEmptyOrNull(strFlightStartNo)) {
		 * //CommonUtil.checkCarrierCodeAccessibility(AiradminUtils.getUserCarrierCodes(request), strFlightStartNo);
		 * ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
		 * 
		 * moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_LIKE);
		 * moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER);
		 * //flight number of the schedule
		 * 
		 * List<String> valueFlightNo = new ArrayList<String>(); String flightNumber = strFlightStartNo + strFlightNo;
		 * valueFlightNo.add(flightNumber != null ? (flightNumber.toUpperCase()+ "%") : "");
		 * 
		 * moduleCriterionFlightNo.setValue(valueFlightNo);
		 * 
		 * critrian.add(moduleCriterionFlightNo); }
		 */
		// setting the departure airport code as criteria
		if (ScheduleUtils.isNotEmptyOrNull(strFrom)) {

			ModuleCriterion moduleCriterionFromStn = new ModuleCriterion();

			moduleCriterionFromStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionFromStn.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.DEPARTURE_APT_CODE);// departure
																														// airport
																														// code
																														// of
																														// the
																														// schedule

			List<String> valueFromStn = new ArrayList<String>();
			valueFromStn.add(strFrom);

			moduleCriterionFromStn.setValue(valueFromStn);

			critrian.add(moduleCriterionFromStn);
		}
		// setting the arrival airport code as criteria
		if (ScheduleUtils.isNotEmptyOrNull(strTo)) {

			ModuleCriterion moduleCriterionToStn = new ModuleCriterion();

			moduleCriterionToStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionToStn.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.ARRIVAL_APT_CODE);// arrival
																													// airport
																													// code
																													// of
																													// the
																													// schedule

			List<String> valueToStn = new ArrayList<String>();
			valueToStn.add(strTo);

			moduleCriterionToStn.setValue(valueToStn);

			critrian.add(moduleCriterionToStn);
		}

		// localtime = strTimeMode.equals(WebConstant.LOCALTIME); //if need to change with time mode uncomment this
		int totRec = 0;
		boolean localtime = true;

		page = ModuleServiceLocator.getScheduleServiceBD().searchAndReassembleSchedules(critrian, 0, 20, localtime, false)[0];
		if (page != null) {
			colSchedules = page.getPageData();
			totRec = page.getTotalNoOfRecords();

		}
		return colSchedules;
	}

	private static Collection composeFlightScheduleListTO(Collection flightSchedules, int nData, String lang, String arFrom,
			String arTo) throws ModuleException {

		Collection<FlightScheduleTO> flightScheduleList = new ArrayList<FlightScheduleTO>();

		if (flightSchedules != null) {
			List list = (List) flightSchedules;
			Object[] listArr = list.toArray();
			String dstFrom = null;
			String dstTo = null;
			FlightSchedule flightSchedule = null;

			// sb.append("var arrData = new Array();");
			for (int i = 0; i < listArr.length; i++) {
				flightSchedule = (FlightSchedule) listArr[i];

				String strStart = "";
				String strStop = "";
				String strStartZulu = "";
				String strStopZulu = "";
				Frequency frq = null;
				Frequency frqLocal = null;

				frq = flightSchedule.getFrequencyLocal();
				String strFrequency = createSegmentArray(frq);

				dstFrom = flightSchedule.getDepartureStnCode();
				dstTo = flightSchedule.getArrivalStnCode();

				strStart = ScheduleUtils.formatDate(flightSchedule.getStartDateLocal(), strSMDateYearFormat);
				strStop = ScheduleUtils.formatDate(flightSchedule.getStopDateLocal(), strSMDateYearFormat);
				frqLocal = flightSchedule.getFrequencyLocal();

				strStartZulu = ScheduleUtils.formatDate(flightSchedule.getStartDate(), strSMDateYearFormat);
				strStopZulu = ScheduleUtils.formatDate(flightSchedule.getStopDate(), strSMDateYearFormat);

				Collection segCol = flightSchedule.getFlightScheduleSegments();

				FlightScheduleTO flightScheduleTO = new FlightScheduleTO();

				flightScheduleTO.setFlightNumber(flightSchedule.getFlightNumber());
				flightScheduleTO.setFlightStopTime(strStop);
				flightScheduleTO.setFlightStartTime(strStart);
				flightScheduleTO.setFlightStartZulu(strStartZulu);
				flightScheduleTO.setFlightStopZulu(strStopZulu);
				flightScheduleTO.setFlightFrequency(strFrequency);
				flightScheduleTO.setDestinationFrom(dstFrom);
				flightScheduleTO.setDestinationTo(dstTo);
				flightScheduleTO.setFlightModelNumber(flightSchedule.getModelNumber());
				setLegArr(flightSchedule, flightScheduleTO);

				setSegmentArr(segCol, flightScheduleTO);

				// flightScheduleTO.setSegmentListArray(setSegmentArr(segCol));
				// flightScheduleTO.setLegListArray(setLegArr(flightSchedule));

				flightScheduleList.add(flightScheduleTO);

			}

		}
		return flightScheduleList;

	}

	/**
	 * Method to get the Frequency segment
	 * 
	 * @param freq
	 *            the Frequency
	 * @return String the string requency Su_TuWd__Sa
	 */
	static public String createSegmentArray(Frequency freq) {

		HashMap daysMap = createDayOfWeekMap(freq);

		// create frequency string using daysMap
		String strFrq = "";
		String[] dayweek = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", };

		int intoffset = Integer.parseInt(dayOffSet);
		intoffset = intoffset % 7;
		for (int i = 0; i < 7; i++) {
			if (intoffset == 7)
				intoffset = 0;
			strFrq += (((Boolean) daysMap.get(CalendarUtil.getDayFromDayNumber(i))).booleanValue()) ? dayweek[intoffset] : "__";
			intoffset++;
		}
		return strFrq;
	}

	/**
	 * Creates a Day of Week Map
	 * 
	 * @param freq
	 *            the Frequency
	 * @return HashMap contating Day of Week
	 */
	static private HashMap createDayOfWeekMap(Frequency freq) {

		// initialize hashmap
		HashMap<DayOfWeek, Boolean> daysMap = new HashMap<DayOfWeek, Boolean>();
		daysMap.put(DayOfWeek.MONDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));
		daysMap.put(DayOfWeek.TUESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.WEDNESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.THURSDAY, new Boolean(false));
		daysMap.put(DayOfWeek.FRIDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SATURDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));

		Collection days = CalendarUtil.getDaysFromFrequency(freq);
		Iterator itDays = days.iterator();

		while (itDays.hasNext()) {
			DayOfWeek day = (DayOfWeek) itDays.next();

			// reset map with selected days
			daysMap.put(day, new Boolean(true));
		}

		return daysMap;
	}

	static private void setLegArr(FlightSchedule flightSchedule, FlightScheduleTO flightScheduleTO) {

		Collection segDetails = flightSchedule.getFlightScheduleLegs();

		List<FlightLegTO> lstFlightlegTO = new ArrayList<FlightLegTO>();

		for (int legnum = 1; legnum < segDetails.size() + 1; legnum++) {

			Iterator segDtls = segDetails.iterator();
			while (segDtls.hasNext()) {
				FlightScheduleLeg legs = (FlightScheduleLeg) segDtls.next();

				if (legs.getLegNumber() == legnum) {

					if (legs.getLegNumber() == 1) {
						FlightLegTO flightlegTO = new FlightLegTO();
						flightlegTO.setLegNumber(legs.getLegNumber());
						flightlegTO.setLegOrigin(legs.getOrigin());
						flightlegTO.setLegDestination(legs.getDestination());
						flightlegTO.setLegDepTimeLocal(ScheduleUtils.formatDate(legs.getEstDepartureTimeLocal(), defaultFormat));
						flightlegTO.setLegDepDayOffsetLocal(legs.getEstDepartureDayOffsetLocal());
						flightlegTO.setLegDuration(legs.getDuration());
						flightlegTO.setLegId(legs.getId());
						flightlegTO.setOverlappingLeg(SegmentUtil.isOverlappingLeg(legs, flightSchedule));

						flightlegTO.setLegDepTimeZulu(ScheduleUtils.formatDate(legs.getEstDepartureTimeZulu(), defaultFormat));
						flightlegTO.setLegDepDayOffsetZulu(legs.getEstDepartureDayOffset());
						flightlegTO.setLegArrivalTimeZulu(ScheduleUtils.formatDate(legs.getEstArrivalTimeZulu(), defaultFormat));
						flightlegTO.setLegArrivalOffsetZulu(legs.getEstArrivalDayOffset());

						lstFlightlegTO.add(flightlegTO);
					}

					flightScheduleTO.setFlightLegTO(lstFlightlegTO);
					break;
				}
			}
		}

	}

	static private void setSegmentArr(Collection segs, FlightScheduleTO flightScheduleTO) {

		List<FlightSegmentTO> lstFlightSegTO = new ArrayList<FlightSegmentTO>();

		int j = 0;
		if (segs != null) {
			Iterator iter = segs.iterator();
			while (iter.hasNext()) {

				FlightScheduleSegment schSeg = (FlightScheduleSegment) iter.next();

				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

				flightSegmentTO.setSegmentId(schSeg.getFlSchSegId());
				flightSegmentTO.setSegmentCode(schSeg.getSegmentCode());
				flightSegmentTO.setValidFlag(schSeg.getValidFlag());
				if (schSeg.getOverlapSegmentId() != null) {
					flightSegmentTO.setOverlapSegmentId(schSeg.getOverlapSegmentId());
				}
				lstFlightSegTO.add(flightSegmentTO);

				j++;
			}
		}
		flightScheduleTO.setFlilghtSegmentTO(lstFlightSegTO);
	}

}
