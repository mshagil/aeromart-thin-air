package com.isa.thinair.ibe.core.web.action.createres;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.handler.createres.ShowTermsNCondRH;

/**
 * @author Latiff
 * 
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({ @Result(name = StrutsConstants.Result.SUCCESS, value = StrutsConstants.Jsp.Common.TERMS),
		@Result(name = StrutsConstants.Result.ERROR, value = StrutsConstants.Jsp.Common.ERROR) })
public class ShowTermsNCondAction {
	public String execute() {
		return ShowTermsNCondRH.execute(ServletActionContext.getRequest());
	}
}
