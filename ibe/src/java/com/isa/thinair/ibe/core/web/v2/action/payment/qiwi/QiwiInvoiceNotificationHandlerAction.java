package com.isa.thinair.ibe.core.web.v2.action.payment.qiwi;

import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.ibe.core.web.constants.StrutsConstants;
import com.isa.thinair.ibe.core.web.v2.util.RequestParameterUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.core.bl.qiwi.QiwiPaymentUtils;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * This is specific servlet related to Qiwi payment gateway
 * The action will handle Qiwi post payment response
 * 
 */
@Namespace(StrutsConstants.Namespace.PUBLIC)
@Results({
	@Result(name = StrutsConstants.Result.SUCCESS, value = "") })
public class QiwiInvoiceNotificationHandlerAction extends  BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(QiwiInvoiceNotificationHandlerAction.class);
	
	private static String SUCCESS_CODE = "0";
	private static String ERROR_CODE = "1";
	
	public String execute() throws MalformedURLException {
		log.info("QiwiInvoiceNotificationHandlerAction : Start");
		
	if (log.isDebugEnabled())
		log.debug("### Start.. QiwiInvoiceNotificationHandlerAction " + request.getRequestedSessionId());
	
		PrintWriter out = null;
		Map<String, String> receiptyMap = RequestParameterUtil.getReceiptMap(request);
		receiptyMap.put("qiwiInvoiceStatusChangeNotification", "true");
		log.info(receiptyMap);		

		if (AppSysParamsUtil.enableServerToServerMessages()) {
	
			try {
				
				//This is to avoid colliding the processes of "server to server response" and "handleIPGResponseAction"
				//This Prioritizes the "handleIPGResponseAction"
				String tempPaymentStatus = ModuleServiceLocator.getReservationBD().loadTempPayment(Integer.parseInt(receiptyMap.get(QiwiPRequest.TNX_ID))).getStatus();
				if(tempPaymentStatus.equals(ReservationInternalConstants.TempPaymentTnxTypes.INITIATED)){
					log.info("QiwiInvoiceNotificationHandlerAction : sleeping to avoid collision... : invoice id : " + receiptyMap.get(QiwiPRequest.TNX_ID) );
					Thread.sleep(20*1000);
					log.info("QiwiInvoiceNotificationHandlerAction : waking up ..... invoice id : " + receiptyMap.get(QiwiPRequest.TNX_ID) );
				}
				
				IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
				IPGPaymentOptionDTO qiwiPgwConfigs = null;
				out = this.response.getWriter();  
				IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
				List<IPGPaymentOptionDTO> pgwList = ModuleServiceLocator.getPaymentBrokerBD().getActivePaymentGatewayByProviderName(QiwiPRequest.QIWI_PROVIDER_CODE);
				qiwiPgwConfigs = pgwList.get(0);
				
				ipgIdentificationParamsDTO =  WebplatformUtil.validateAndPrepareIPGConfigurationParamsDTO(qiwiPgwConfigs.getPaymentGateway(), qiwiPgwConfigs.getBaseCurrency());
				Date requestTime = ModuleServiceLocator.getReservationBD().getPaymentRequestTime(Integer.parseInt(receiptyMap.get(QiwiPRequest.TNX_ID)));
	
				ipgResponseDTO.setRequestTimsStamp(requestTime);
				ipgResponseDTO.setResponseTimeStamp(Calendar.getInstance().getTime());
				ipgResponseDTO.setPaymentBrokerRefNo(ipgIdentificationParamsDTO.getIpgId());
				ipgResponseDTO.setTemporyPaymentId(Integer.parseInt(receiptyMap.get(QiwiPRequest.TNX_ID)));	
				receiptyMap.put("responseType", "PAYMENTSUCCESS");
				
				ModuleServiceLocator.getPaymentBrokerBD().handleDeferredResponse(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);		
				
				String pnr = ModuleServiceLocator.getReservationBD().loadTempPayment(Integer.parseInt(receiptyMap.get(QiwiPRequest.TNX_ID))).getPnr();
				
				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(pnr);
				modes.setRecordAudit(false);
				Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(modes, null);
				
				if(ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())){
					log.debug("QiwiInvoiceNotificationHandlerAction : PNR " + pnr + ": Success" );
					out.println(QiwiPaymentUtils.composeServerToServerResponse(SUCCESS_CODE));  
				}else{
					log.debug("QiwiInvoiceNotificationHandlerAction : PNR " + pnr + ": Failure" );
					out.println(QiwiPaymentUtils.composeServerToServerResponse(ERROR_CODE));  
				}
				
				out.flush(); 
				out.close();
				
			} catch(Exception ex) {
				
				log.error("Qiwi QiwiInvoiceNotificationHandlerAction==>", ex);
				out.println(QiwiPaymentUtils.composeServerToServerResponse(ERROR_CODE));  
				out.flush(); 
				out.close();				
			}
		}
		
		log.info("QiwiInvoiceNotificationHandlerAction : End");
		return StrutsConstants.Result.SUCCESS;
	}	
}
