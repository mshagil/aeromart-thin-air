package com.isa.thinair.ibe.api.dto;

import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;

public class BookingTO {
	private String PNR;

	private String status;

	private String bookingDate;

	private String releaseDate;

	private String agent;

	private String totalPaxAdultCount;

	private String totalPaxChildCount;

	private String totalPaxInfantCount;

	private String policyNo = "";

	private String prefferedLanguage;

	private String version;

	private String insuranceType;

	private boolean hasInsurance;

	private Collection<LCCClientCarrierOndGroup> pnrOndGroups;

	public boolean isHasInsurance() {
		return hasInsurance;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	/**
	 * @return the pNR
	 */
	public String getPNR() {
		return PNR;
	}

	/**
	 * @param pNR
	 *            the pNR to set
	 */
	public void setPNR(String pNR) {
		PNR = pNR;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the bookingDate
	 */
	public String getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate
	 *            the bookingDate to set
	 */
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent
	 *            the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the totalPaxAdultCount
	 */
	public String getTotalPaxAdultCount() {
		return totalPaxAdultCount;
	}

	/**
	 * @param totalPaxAdultCount
	 *            the totalPaxAdultCount to set
	 */
	public void setTotalPaxAdultCount(String totalPaxAdultCount) {
		this.totalPaxAdultCount = totalPaxAdultCount;
	}

	/**
	 * @return the totalPaxChildCount
	 */
	public String getTotalPaxChildCount() {
		return totalPaxChildCount;
	}

	/**
	 * @param totalPaxChildCount
	 *            the totalPaxChildCount to set
	 */
	public void setTotalPaxChildCount(String totalPaxChildCount) {
		this.totalPaxChildCount = totalPaxChildCount;
	}

	/**
	 * @return the totalPaxInfantCount
	 */
	public String getTotalPaxInfantCount() {
		return totalPaxInfantCount;
	}

	/**
	 * @param totalPaxInfantCount
	 *            the totalPaxInfantCount to set
	 */
	public void setTotalPaxInfantCount(String totalPaxInfantCount) {
		this.totalPaxInfantCount = totalPaxInfantCount;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getPrefferedLanguage() {
		return prefferedLanguage;
	}

	public void setPrefferedLanguage(String prefferedLanguage) {
		this.prefferedLanguage = prefferedLanguage;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Collection<LCCClientCarrierOndGroup> getPnrOndGroups() {
		return pnrOndGroups;
	}

	public void setPnrOndGroups(Collection<LCCClientCarrierOndGroup> pnrOndGroups) {
		this.pnrOndGroups = pnrOndGroups;
	}

}
