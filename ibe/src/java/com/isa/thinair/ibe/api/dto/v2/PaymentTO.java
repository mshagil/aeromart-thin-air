package com.isa.thinair.ibe.api.dto.v2;

import com.isa.thinair.webplatform.api.v2.util.PaymentMethod;


public class PaymentTO {
	private String amount;

	private String type;

	private String cardType;

	private String agent;

	private String mode;

	private String paymentReference;

	private Integer actualPaymentMethod;

	private String agentOneAmount;

	private String agentTwoAmount;

	private String agentTwo;

	private String ccAmount;

	public PaymentTO clone() {
		PaymentTO clone = new PaymentTO();
		clone.setActualPaymentMethod(this.getActualPaymentMethod());
		clone.setAgent(this.getAgent());
		clone.setAgentOneAmount(this.getAgentOneAmount());
		clone.setAgentTwo(this.getAgentTwo());
		clone.setAgentTwoAmount(this.getAgentTwoAmount());
		clone.setAmount(this.getAmount());
		clone.setCardType(this.getCardType());
		clone.setCcAmount(this.getCcAmount());
		clone.setMode(this.getMode());
		clone.setPaymentReference(this.getPaymentReference());
		clone.setType(this.getType());
		return clone;

	}

	/**
	 * @return
	 */
	public PaymentMethod getPaymentMethod() {
		return PaymentMethod.getPaymentType(this.type);
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent
	 *            the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return paymentReference
	 */
	public String getPaymentReference() {
		return paymentReference;
	}

	/**
	 * @param paymentReference
	 *            to set
	 */
	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public Integer getActualPaymentMethod() {
		return actualPaymentMethod;
	}

	public void setActualPaymentMethod(Integer actualPaymentMethod) {
		this.actualPaymentMethod = actualPaymentMethod;
	}

	public String getAgentOneAmount() {
		return agentOneAmount;
	}

	public void setAgentOneAmount(String agentOneAmount) {
		this.agentOneAmount = agentOneAmount;
	}

	public String getAgentTwoAmount() {
		return agentTwoAmount;
	}

	public void setAgentTwoAmount(String agentTwoAmount) {
		this.agentTwoAmount = agentTwoAmount;
	}

	public String getAgentTwo() {
		return agentTwo;
	}

	public void setAgentTwo(String agentTwo) {
		this.agentTwo = agentTwo;
	}

	public String getCcAmount() {
		return ccAmount;
	}

	public void setCcAmount(String ccAmount) {
		this.ccAmount = ccAmount;
	}

}
