package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

public class SessionTimeoutDataDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7996094693164206856L;
	private String lblSeconds;
	private String lblMsgTimeout;
	private String lblMsgCancel;
	private int timeout;
	private int displayThreshold;

	public String getLblSeconds() {
		return lblSeconds;
	}

	public void setLblSeconds(String lblSeconds) {
		this.lblSeconds = lblSeconds;
	}

	public String getLblMsgTimeout() {
		return lblMsgTimeout;
	}

	public void setLblMsgTimeout(String lblMsgTimeout) {
		this.lblMsgTimeout = lblMsgTimeout;
	}

	public String getLblMsgCancel() {
		return lblMsgCancel;
	}

	public void setLblMsgCancel(String lblMsgCancel) {
		this.lblMsgCancel = lblMsgCancel;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getDisplayThreshold() {
		return displayThreshold;
	}

	public void setDisplayThreshold(int displayThreshold) {
		this.displayThreshold = displayThreshold;
	}

}
