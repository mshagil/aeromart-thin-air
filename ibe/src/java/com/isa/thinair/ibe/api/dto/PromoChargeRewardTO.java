package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

public class PromoChargeRewardTO implements Serializable {

	private static final long serialVersionUID = 7988548530416334032L;

	private String outboundSegCodes;
	private String outboundLower;
	private String outboundUpper;
	private String inboundSegCodes;
	private String inboundLower;
	private String inboundUpper;
	private String promotionId;

	public String getOutboundSegCodes() {
		return outboundSegCodes;
	}

	public void setOutboundSegCodes(String outboundSegCodes) {
		this.outboundSegCodes = outboundSegCodes;
	}

	public String getOutboundLower() {
		return outboundLower;
	}

	public void setOutboundLower(String outboundLower) {
		this.outboundLower = outboundLower;
	}

	public String getOutboundUpper() {
		return outboundUpper;
	}

	public void setOutboundUpper(String outboundUpper) {
		this.outboundUpper = outboundUpper;
	}

	public String getInboundSegCodes() {
		return inboundSegCodes;
	}

	public void setInboundSegCodes(String inboundSegCodes) {
		this.inboundSegCodes = inboundSegCodes;
	}

	public String getInboundLower() {
		return inboundLower;
	}

	public void setInboundLower(String inboundLower) {
		this.inboundLower = inboundLower;
	}

	public String getInboundUpper() {
		return inboundUpper;
	}

	public void setInboundUpper(String inboundUpper) {
		this.inboundUpper = inboundUpper;
	}

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

}
