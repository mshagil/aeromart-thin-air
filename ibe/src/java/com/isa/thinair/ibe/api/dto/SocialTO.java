package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

public class SocialTO implements Serializable {

	private static final long serialVersionUID = -5895924176990598028L;

	private boolean showFacebook;
	private boolean showTwitter;
	private boolean showGooglePlus;
	private boolean showLinkedln;
	private boolean register;

	private boolean showInlinePanel;

	private String marketingPopupDelay;

	private String facebookAppId;
	private String linkedInAppId;

	private JourneyInfo outbound;
	private JourneyInfo inbound;

	public SocialTO() {
		outbound = new JourneyInfo();
		inbound = new JourneyInfo();
	}

	public boolean isShowFacebook() {
		return showFacebook;
	}

	public void setShowFacebook(boolean showFacebook) {
		this.showFacebook = showFacebook;
	}

	public boolean isShowTwitter() {
		return showTwitter;
	}

	public void setShowTwitter(boolean showTwitter) {
		this.showTwitter = showTwitter;
	}

	public boolean isShowGooglePlus() {
		return showGooglePlus;
	}

	public void setShowGooglePlus(boolean showGooglePlus) {
		this.showGooglePlus = showGooglePlus;
	}

	public boolean isShowLinkedln() {
		return showLinkedln;
	}

	public void setShowLinkedln(boolean showLinkedln) {
		this.showLinkedln = showLinkedln;
	}

	public boolean isRegister() {
		return register;
	}

	public void setRegister(boolean register) {
		this.register = register;
	}

	public String getFacebookAppId() {
		return facebookAppId;
	}

	public void setFacebookAppId(String facebookAppId) {
		this.facebookAppId = facebookAppId;
	}

	public String getMarketingPopupDelay() {
		return marketingPopupDelay;
	}

	public void setMarketingPopupDelay(String marketingPopupDelay) {
		this.marketingPopupDelay = marketingPopupDelay;
	}

	public String getLinkedInAppId() {
		return linkedInAppId;
	}

	public void setLinkedInAppId(String linkedInAppId) {
		this.linkedInAppId = linkedInAppId;
	}

	public JourneyInfo getOutbound() {
		return outbound;
	}

	public void setOutbound(JourneyInfo outbound) {
		this.outbound = outbound;
	}

	public JourneyInfo getInbound() {
		return inbound;
	}

	public void setInbound(JourneyInfo inbound) {
		this.inbound = inbound;
	}

	public boolean isShowInlinePanel() {
		return showInlinePanel;
	}

	public void setShowInlinePanel(boolean showInlinePanel) {
		this.showInlinePanel = showInlinePanel;
	}

}
