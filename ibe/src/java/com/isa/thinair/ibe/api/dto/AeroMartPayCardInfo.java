package com.isa.thinair.ibe.api.dto;

import java.util.List;

public class AeroMartPayCardInfo {

	private int cardType;
	private String displayName;
	private String cssClassName;
	private String cardBehavior;
	private List<String> requiredUserInputs;

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getCssClassName() {
		return cssClassName;
	}

	public void setCssClassName(String cssClassName) {
		this.cssClassName = cssClassName;
	}

	public List<String> getRequiredUserInputs() {
		return requiredUserInputs;
	}

	public void setRequiredUserInputs(List<String> requiredUserInputs) {
		this.requiredUserInputs = requiredUserInputs;
	}

	public String getCardBehavior() {
		return cardBehavior;
	}

	public void setCardBehavior(String cardBehavior) {
		this.cardBehavior = cardBehavior;
	}

}
