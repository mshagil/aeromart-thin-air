package com.isa.thinair.ibe.api.dto;

import java.io.Serializable;

public class Kiosk implements Serializable {

	/**
	 * Generated serieal id
	 */
	private static final long serialVersionUID = -8170813433472098075L;

	/*
	 * kiosk user name
	 */
	private String userName;

	/*
	 * kiosk pasword
	 */
	private String password;

	/**
	 * 
	 * @ruturn kiosk password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 
	 * @param kiosk
	 *            password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @retutn Kiosk user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 
	 * @param kiosk
	 *            user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
