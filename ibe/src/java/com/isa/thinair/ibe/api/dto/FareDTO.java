package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class FareDTO {

	private String flightRefNumber;

	private int fareId;

	private int fareRuleID;

	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String carrierCode = null;

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public void setFareRuleID(int fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public int getFareId() {
		return fareId;
	}

	public int getFareRuleID() {
		return fareRuleID;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

}
