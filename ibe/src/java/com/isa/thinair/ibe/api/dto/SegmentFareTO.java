package com.isa.thinair.ibe.api.dto;

import java.util.Collection;

import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;

public class SegmentFareTO {

	private Collection<PaxFareTO> paxWise;

	private String segmentCode;

	private String segmentName;

	private String totalPrice;

	private String totalPriceInSelectedCurr;

	private String totalTaxSurcharges;

	private String totalTaxSurchargesInSelectedCurr;
	
	private boolean hasInbound = false;
	
	public boolean isHasInbound() {
		return hasInbound;
	}

	public void setHasInbound(boolean hasInbound) {
		this.hasInbound = hasInbound;
	}


	public Collection<PaxFareTO> getPaxWise() {
		return paxWise;
	}

	public void setPaxWise(Collection<PaxFareTO> paxWise) {
		this.paxWise = paxWise;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getTotalTaxSurcharges() {
		return totalTaxSurcharges;
	}

	public void setTotalTaxSurcharges(String totalTaxSurcharges) {
		this.totalTaxSurcharges = totalTaxSurcharges;
	}

	public String getTotalPriceInSelectedCurr() {
		return totalPriceInSelectedCurr;
	}

	public void setTotalPriceInSelectedCurr(String totalPriceInSelectedCurr) {
		this.totalPriceInSelectedCurr = totalPriceInSelectedCurr;
	}

	public String getTotalTaxSurchargesInSelectedCurr() {
		return totalTaxSurchargesInSelectedCurr;
	}

	public void setTotalTaxSurchargesInSelectedCurr(String totalTaxSurchargesInSelectedCurr) {
		this.totalTaxSurchargesInSelectedCurr = totalTaxSurchargesInSelectedCurr;
	}

}
