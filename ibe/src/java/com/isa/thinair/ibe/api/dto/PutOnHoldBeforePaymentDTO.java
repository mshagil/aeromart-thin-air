package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.dto.CreditCardTO;

public class PutOnHoldBeforePaymentDTO {

	private boolean putOnHoldBeforePayment;

	private String putOnHoldBeforePaymentPnr;

	private String lastName;

	private Date departureDate;

	private CreditCardTO card;
	
	private BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

	public CreditCardTO getCard() {
		return card;
	}

	public void setCard(CreditCardTO card) {
		this.card = card;
	}

	public String getPutOnHoldBeforePaymentPnr() {
		return putOnHoldBeforePaymentPnr;
	}

	public void setPutOnHoldBeforePaymentPnr(String putOnHoldBeforePaymentPnr) {
		this.putOnHoldBeforePaymentPnr = putOnHoldBeforePaymentPnr;
	}

	public boolean isPutOnHoldBeforePayment() {
		return putOnHoldBeforePayment;
	}

	public void setPutOnHoldBeforePayment(boolean putOnHoldBeforePayment) {
		this.putOnHoldBeforePayment = putOnHoldBeforePayment;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public BigDecimal getTotalTicketPrice() {
		return totalTicketPrice;
	}

	public void setTotalTicketPrice(BigDecimal totalTicketPrice) {
		this.totalTicketPrice = totalTicketPrice;
	}	

}
