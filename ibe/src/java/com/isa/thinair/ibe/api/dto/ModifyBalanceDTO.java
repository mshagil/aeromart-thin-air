package com.isa.thinair.ibe.api.dto;

public class ModifyBalanceDTO {

	private String totalReservationCredit;

	private String totalCreditBalace;

	private String balanceToPay;

	private String modificationCharge;

	public String getTotalReservationCredit() {
		return totalReservationCredit;
	}

	public void setTotalReservationCredit(String totalReservationCredit) {
		this.totalReservationCredit = totalReservationCredit;
	}

	public String getTotalCreditBalace() {
		return totalCreditBalace;
	}

	public void setTotalCreditBalace(String totalCreditBalace) {
		this.totalCreditBalace = totalCreditBalace;
	}

	public String getBalanceToPay() {
		return balanceToPay;
	}

	public void setBalanceToPay(String balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

	public String getModificationCharge() {
		return modificationCharge;
	}

	public void setModificationCharge(String modificationCharge) {
		this.modificationCharge = modificationCharge;
	}

}
