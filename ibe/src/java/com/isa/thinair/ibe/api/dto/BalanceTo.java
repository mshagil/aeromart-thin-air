package com.isa.thinair.ibe.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class BalanceTo {

	private BigDecimal totalCreditBalace = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal balanceToPay = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getTotalCreditBalace() {
		return totalCreditBalace;
	}

	public void setTotalCreditBalace(BigDecimal totalCreditBalace) {
		this.totalCreditBalace = totalCreditBalace;
	}

	public BigDecimal getBalanceToPay() {
		return balanceToPay;
	}

	public void setBalanceToPay(BigDecimal balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

}
