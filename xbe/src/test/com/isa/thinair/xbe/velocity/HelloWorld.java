package com.isa.thinair.xbe.velocity;

import java.io.StringWriter;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

public class HelloWorld {

    public static void main(String[] args) throws Exception {
        /* first, get and initialize an engine */
        Properties props = new Properties();
        props.setProperty( "file.resource.loader.path", "D:/testing/resources/templates" );
       
        VelocityEngine ve = new VelocityEngine();
        ve.init(props);

        /* next, get the Template */

        Template t = ve.getTemplate("helloworld.vm");

        /* create a context and add data */

        VelocityContext context = new VelocityContext();

        context.put("name", "World");

        /* now render the template into a StringWriter */

        StringWriter writer = new StringWriter();

        t.merge(context, writer);

        /* show the World */

        System.out.println(writer.toString());
    }
}
