package com.isa.thinair.xbe.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.isa.thinair.commons.core.util.CalanderUtil;
import com.isa.thinair.xbe.core.util.DateUtil;

public class DateTest {
	
	public static void main(String[] args) throws ParseException {
		//addDate();
		dateOfWeek();
	}

	private static void dateOfWeek() {
		Date toDay = new Date();
		//Calendar cal = new GregorianCalendar(2003, Calendar.JANUARY, 1);
		Calendar cal = GregorianCalendar.getInstance(); 
		
	    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK); 
	    
	  System.out.println("dayOfWeek:" + dayOfWeek);
		
	}

	private static void addDate() throws ParseException {
		Date newDate = null;
		String dateString = "24072009";
		String format = "ddMMyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		TimeZone timeZone = TimeZone.getDefault();

		sdf.setTimeZone(timeZone);
		newDate = sdf.parse(dateString);

		Calendar cal = Calendar.getInstance();
		int years = 0;
		int months = 6;
		int days = 5;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		
		cal.setTime(newDate);
		
		cal.add(Calendar.YEAR, years);
		cal.add(Calendar.MONTH, months);
		cal.add(Calendar.DAY_OF_MONTH, days);
		cal.add(Calendar.HOUR_OF_DAY, hours);
		cal.add(Calendar.MINUTE, minutes);
		cal.add(Calendar.SECOND, seconds);

		System.out.println(cal.getTime());
		
	}
}
