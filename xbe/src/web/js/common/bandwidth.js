   var http_request = false;
   function makePOSTRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
	 try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
	 } catch (e) {}
	 
	 http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }

      if (!http_request) {
         //alert('Cannot create XMLHTTP instance');
         return false;
      }

      ST = CurrentTime();
      http_request.onreadystatechange = alertContents;
      http_request.open('POST', url, true);
      http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_request.setRequestHeader("Content-length", parameters.length);
      http_request.setRequestHeader("Connection", "close");
      http_request.send(parameters);
   }
   
   function getMessage(speed) {
	var color;
	if (speed > 512) {
	   color = " style='color: 66FF33' ";
	} else if (speed > 128) {
	   color = " style='color: FFFF00' ";
	} else if (speed > 64) {
	   color = " style='color: FF9933' ";
	} else {
	   color = " style='color: FF3300' ";
	}	
	return "<font" + color + "><b>" + speed + " [Kbps]<\/b><\/font>";
   }
   
   function alertContents() {
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {
            //alert(http_request.responseText);
            var  result;
          try {
			 result = http_request.responseText;
          }catch (e){
			  //should not break of this
			  //not used not sure wt the idea is
          }
            speed = GetKBPS(ST);
            speedMsg = getMessage(speed);
            top[2].DivWrite("spnBandwidth", speedMsg);
         } else {
            top[2].DivWrite("spnBandwidth", "<font style='color: white'><b>Processing...<\/b><\/font>");
         }
      }
   }

    function CurrentTime() {
	time = new Date();
	return time.getTime();
    }

    function returnKBPS(kbps){
	return kbps;
    }

    function GetKBPS(ST) {
    	ET	= CurrentTime();
    	Factor	= 1000;
    	DT = ((ET - ST)/Factor);
    	DataSize = 50;
    	kbps1    = (DataSize/DT);
		kbpsA    = ((kbps1*8)*10*1.02);
    	kbyteA   = ((DataSize*10)/DT);
    	kbps     = (Math.round(kbpsA)/10);
     	kbyte    = (Math.round(kbyteA)/10);
		return kbps;
    }

   function LoadBandwidthInfo() {
      var poststr = "";
      makePOSTRequest('../images/image50_no_cache.jpg', poststr);
   }

    function pause(numberMillis)
    {
		var now = new Date();
		var exitTime = now.getTime() + numberMillis;
		while (true)
		{
		now = new Date();
		if (now.getTime() > exitTime)
		return;
		}
    } 

