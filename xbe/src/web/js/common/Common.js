	/*
	*********************************************************
		Description		: Common Routings to get the Control information
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 1st June 2005
	*********************************************************	
	*/
	var url=window.location.href;
	if(window.name=='winCC'){
		if(url.indexOf('j_security_check')==-1&&url.indexOf('showLogin')==-1&&url.indexOf('showXBEMain.action')==-1&&url.indexOf('showPageTimedOut')==-1){
			window.location='../private/showXBEMain.action';
		}
	}
	// Set the Background Color
	function setBGColor(strControlID, strColor){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.backgroundColor = strColor;
	}	
	
	// Fill the controls with passed array element
	function setModel(model){
	    for(var key in model){
			if(model[key] != null){
			     setField(key,model[key]);
			}
	  	}
  	}

	// Set the Navigate URL for a Anchor Tag
	function setLinkUrl(strControlID, strNavigateURL){
		var objControl = getFieldByID(strControlID) ;
		objControl.href= strNavigateURL;
	}
	
	// Set Style Sheet
	function setStyleClass(strControlID, strClassName){
		var objControl = getFieldByID(strControlID) ;
		objControl.className = strClassName
	}
	
	// get Style Sheet
	function getStyleClass(strControlID){
		var objControl = getFieldByID(strControlID) ;
		return objControl.className;
	}
		
	// Write to Divs / span
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			if(objControl){
				objControl.innerHTML = "";
				objControl.innerHTML = strText;
			}
			
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			if(objControl){
				objControl.innerHTML = strText;
			}
			
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			if(objControl){
				objControl.document.open();
				objControl.document.write(strText);
				objControl.document.close();
			}
			
		}
	}
	
	// Read the div information
	function DivRead(strDivID){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			return objControl.innerHTML
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			return objControl.innerHTML;
		}
		else if (document.layers)
		{
			return "";
		}
	}
	
	// Set Image 	
	function setImage(strImageID, strImagePath){
		var objControl = getFieldByID(strImageID);
		objControl.src = strImagePath;
	}

	// set Background Image
	function setBGImage(strID, strImagePath){
		var objControl = getFieldByID(strID);
		objControl.style.backgroundImage = "url(" + strImagePath + ")";
	}
	

	// Set Visibility
	function setVisible(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if(objControl) {
			if (!blnVisible){
				objControl.style.visibility = "hidden";
			}else{
				objControl.style.visibility = "visible";
			}
		}
	}
	
	// Set Display
	function setDisplay(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if(objControl) {
			if (!blnVisible){
				objControl.style.display = "none";
			}else{
				objControl.style.display = "block";
			}
		}
	}
	
	// Get Display
	function getDisplay(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.display == "block"){
			return true;
		}else{
			return false;
		}
	}
	
	// Set Checked
	function setChecked(strControlID, blnStatus){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].checked = blnStatus;
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].checked = blnStatus;
				}
				break;
		}	
	}
	
	// Get Checked
	function getChecked(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var blnStatus = false;
		switch (strType){
			case "CHECKBOX" : 
				if (objControl.checked){
					blnStatus = true;
				}
				break;
		}	
		return blnStatus
	}

	// Set Visibility
	function display(strControlID, blnDisplay){
		var objControl = getFieldByID(strControlID);
		if(objControl) {
			if (!blnDisplay){
				objControl.style.display="none";
			}else{
				objControl.style.display="";
			}
		}
	}
	
	// Get Visibility
	function getVisible(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.visibility == "visible"){
			return true;
		}else{
			return false;
		}
	}
	
	// Get the Field Type
	function getFieldType(objControl){
		if(objControl!= null && objControl.type !=null){
			return objControl.type.toUpperCase();
		}else {
			return "";
		}
	}
	
	// Get the control as Object
	function getFieldByID(strControlID){
		return document.getElementById(strControlID) ;
	}
	
	function getFieldByName(strControlName){
		return document.getElementsByName(strControlName) ;
	}
	
	
	// Set ReadOnly	
	function ReadOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable ; break ;
			case "PASSWORD" : objControl.readOnly = blnEnable ; break ;
			case "TEXTAREA" : objControl.readOnly = blnEnable ; break ;
		}	
	}

	// Read Only
	function readOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable; break;
			case "PASSWORD" : objControl.readOnly = blnEnable; break;
			case "TEXTAREA" : objControl.readOnly = blnEnable; break;
		}
	}

	// Enable / Disable a Conrol
	function Disable(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		
		if(objControl) {		
			var strType = getFieldType(objControl);			
			switch (strType){
				case "CHECKBOX" : 
					objControl = getFieldByName(strControlID);
					var intLengrh = objControl.length ;
					for (var i = 0 ; i < intLengrh ; i++){
						objControl[i].disabled = blnEnable;
					}
					break;
				
				case "RADIO" : 
					objControl = getFieldByName(strControlID);
					var intLengrh = objControl.length ;
					for (var i = 0 ; i < intLengrh ; i++){
						objControl[i].disabled = blnEnable;
					}
					break;
				default :
					objControl.disabled = blnEnable ; break ;
			}
		}	
	}
	
	// Read Only
	function readOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable; break;
			case "PASSWORD" : objControl.readOnly = blnEnable; break;
			case "TEXTAREA" : objControl.readOnly = blnEnable; break;
		}
	}
	
	// Set values to a control 
	function getText(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.options[objControl.selectedIndex].text;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].text;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	// Set values to a control 
	function getValue(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (intLength > 0){
					for (var i = 0 ; i < intLength ; i++){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].checked;
					}				
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				for (var i = 0 ; i < intLength ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.value ;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].value;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	// Set values to a control 
	function setField(strControlID, strControlValue){
		var objControl = getFieldByID(strControlID) ;
		if(objControl){
			var strType = getFieldType(objControl);
			switch (strType){
				case "TEXT" : objControl.value = strControlValue ; break ;
				case "PASSWORD" : objControl.value = strControlValue ; break ;
				case "HIDDEN" : objControl.value = strControlValue ; break ;
				case "TEXTAREA" : objControl.value = strControlValue ; break ;
				case "CHECKBOX" : 
					objControl = getFieldByName(strControlID);
					var intLength = objControl.length ;
					if (strControlValue != true && strControlValue != false){
						var arrConValue = strControlValue.split(",");
						var intArrLength = arrConValue.length ;
						if (strControlValue == ""){
							for (var i = 0 ; i < intLength ; i++){
								objControl[i].checked = false;
							}				
						}
						
						for (var x = 0; x < intArrLength ; x++){
							for (var i = 0 ; i < intLength ; i++){
								if (objControl[i].value == arrConValue[x]){
									if (!objControl[i].disabled){
										objControl[i].checked = true;
									}
									break;
								}
							}
						}
					}else{
						for (var i = 0 ; i < intLength ; i++){
							if (!objControl[i].disabled){
								objControl[i].checked = strControlValue;
							}
							
						}
					}
					break;
				
				case "RADIO" : 
					objControl = getFieldByName(strControlID);
					var intLengrh = objControl.length ;
					if (strControlValue == ""){
						for (var i = 0 ; i < intLengrh ; i++){
							objControl[i].checked = false;
						}				
					}
					for (var i = 0 ; i < intLengrh ; i++){
						if (objControl[i].value == strControlValue){
							objControl[i].checked = true;
							break;
						}
					}				
					break;
					
				case "SELECT-ONE" :
					var intLengrh = objControl.length ;
					for (var i = 0 ; i < intLengrh ; i++){
						if (objControl.options[i].value == strControlValue){
							objControl.options[i].selected = true;
							break;
						}
					}
					break;
						
				case "SELECT-MULTIPLE" :
					var intLengrh = objControl.length ;
					var arrConValue = strControlValue.split(",");
					var intArrLength = arrConValue.length ;
					if (strControlValue == ""){
						for (var i = 0 ; i < intLengrh ; i++){
							objControl.options[i].selected = false;
						}				
					}
					for (var x = 0; x < intArrLength ; x++){
						for (var i = 0 ; i < intLengrh ; i++){
							if (objControl.options[i].value == arrConValue[x]){
								objControl.options[i].selected = true;
								break;
							}
						}				
					}
					break;
			}			
			
		}		
		
	}
	
	// Set the Maxlength
	function MaxLength(objControl, objEvent, intLen){
		if ((objEvent.keyCode == 9) || (objEvent.keyCode == 46) || (objEvent.keyCode == 8) || (objEvent.keyCode == 37) || (objEvent.keyCode == 38) || (objEvent.keyCode == 39) || (objEvent.keyCode == 40)){
			return true
		}else{
			if (objControl.value.length >= intLen){
				var strValue = objControl.value
				objControl.value = strValue.substr(0, intLen);
				return false;
			}
		}
		return true;
	}
	
	// Number convert to Arabic
	function numberConvertToArabic(intNumber){
		var arrNumbers = new Array("&#1632;","&#1633;","&#1634;","&#1635;","&#1636;","&#1637;","&#1638;","&#1639;","&#1640;","&#1641;")
		intNumber = String(intNumber);
		var intLength = intNumber.length;
		var strReturn = "" ;
		for (var i = 0 ; i < intLength ; i++){
			if (intNumber.substr(i,1) != " "){
				if (!isNaN(intNumber.substr(i,1))){
					strReturn += arrNumbers[intNumber.substr(i,1)];
				}else{
					strReturn += intNumber.substr(i,1);
				}
			}else{
				strReturn += intNumber.substr(i,1);
			}
		}
		
		return strReturn;
	}
	// Generate the Error
	function raiseError(strErrNo){
		var strMsg = top.arrError[strErrNo];
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}	

	function raiseErrorWindow(strErrNo){
		var strMsg = opener.top.arrError[strErrNo];
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}	
	
	function showCommonError(msgType,msg) {
		top[2].objMsg.MessageText = msg ;
		top[2].objMsg.MessageType = msgType; 
		top[2].ShowPageMessage();
		return true;
	}
	
	// to show the error messages
	function showERRMessage(strErrMessage) {
		top[2].objMsg.MessageText = strErrMessage;
		top[2].objMsg.MessageType = "Error" ; 
		top[2].ShowPageMessage();
		return true;
	}

	// to show the confirmation messages 
	function showConfirmation(strMessage) {
		top[2].objMsg.MessageText=strMessage;
		top[2].objMsg.MessageType="CONFIRMATION" ; 
		top[2].ShowPageMessage();
		return true;
	}
	
	// ---------------- Remote scripting
	var reqObj;
  	function retrieveURL(url) {
		if (window.XMLHttpRequest) { // Non-IE browsers
      		reqObj = new XMLHttpRequest();
      		reqObj.onreadystatechange = processStateChange;
      		try {
        		reqObj.open("GET", url, true);
      		}catch (e) {
        		showERRMessage(e);
      		}
      		reqObj.send(null);
    	}else if (window.ActiveXObject) { // IE
      		reqObj = new ActiveXObject("Microsoft.XMLHTTP");
      		if (reqObj) {
        		reqObj.onreadystatechange = processStateChange;
        		reqObj.open("GET", url, true);
        		reqObj.send();
      		}
    	}
  	}
  	
	var httpReq=false;
	function makePOST(url,params) {
		httpReq=false;
	    if(window.XMLHttpRequest){ // Mozilla, Safari,...
			httpReq=new XMLHttpRequest();
	    }else if(window.ActiveXObject){ // IE
			try{
	              httpReq=new ActiveXObject("Microsoft.XMLHTTP");
	        } catch (e) {}
	    }
	
	    if (!httpReq) {
	        alert('Cannot create XMLHTTP instance');
	        return false;
	    }
	     
	    httpReq.onreadystatechange=processStateChange;
	    httpReq.open('POST', url, true);
	    httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    httpReq.setRequestHeader("Content-length",params.length);
	    httpReq.setRequestHeader("Connection","close");
	    httpReq.send(params);
	    
	    try{
		    if (top.strTopPage){
			    ResetTimeOut();
		   	}
		}catch(e){}
	}

	function makeGet(url) {
		if (window.XMLHttpRequest) { // Non-IE browsers
	   		httpReq=new XMLHttpRequest();
	  	}else if (window.ActiveXObject) { // IE
	   		httpReq=new ActiveXObject("Microsoft.XMLHTTP");
	 	}
	
		if (httpReq) {
	   		httpReq.onreadystatechange=processStateChange;
	   		httpReq.open("GET",url,true);
	   		httpReq.setRequestHeader( "If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT" );
	   		httpReq.send(null);
	   	}
	   	/*
	   	var arrData = url.split("?");
	   	makePOST(arrData[0], arrData[1]);
	   	*/
	}

  	// --------------------- Array
	function showArray(strArrName){
		var objArray = eval(strArrName);
		var strR = "";
		for (var i = 0 ; i < objArray.length ; i++){
			strR += objArray[i] + "\n" 
		}
		alert(strR);
	}
	
	// Currency Format
	function CurrencyFormat(num, decimals){
		/*
		if ((String(amount) == "") || (amount == ".00")){amount = 0;}
		amount = addDecimals(amount, decimals);
		var Num = amount.toString().replace(/\$|\,/g,'');
		var dec = Num.indexOf(".");
		var intCents = ((dec > -1) ? "" + Num.substring(dec,Num.length) : ".00");
		Num = "" + parseInt(Num);
		var intValue = "";
		var intReturnValue = "";
		if (intCents.length == 2) intCents += "0";
		if (intCents.length == 1) intCents += "00";
		if (intCents == "") intCents += ".00";
		var count = 0;
		for (var k = Num.length-1; k >= 0; k--) {
			var oneChar = Num.charAt(k);
			if (count == 3){
				intValue += ",";
				intValue += oneChar;
				count = 1;
				continue;
			}else {
				intValue += oneChar;
				count ++;
			}
		}
		for (var k = intValue.length-1; k >= 0; k--) {
			var oneChar = intValue.charAt(k);
			intReturnValue += oneChar;
		}
		intReturnValue = intReturnValue + intCents;
		if (intReturnValue.indexOf("-,") != -1)
		{
			intReturnValue = intReturnValue.replace("-,","-");
		}
		*/
		
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = String(Math.floor(num/100));
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + num + '.' + cents);
	}

	// Add Decimals
	function addDecimals(strValue, intDecimals){
		var strReturn = "";
		var intDevide = "1"
		for (var i = 0 ; i < Number(intDecimals) ; i++){
			intDevide += "0";
		}

		strValue = String(Math.round(Number(strValue) * Number(intDevide))/Number(intDevide))
		var intIndex = strValue.indexOf(".")
		var strDecimals = "";
		if (intIndex !=  -1){
			var arrData = strValue.split(".");
			if (arrData.length > 1){
				strDecimals = arrData[1]
				for (var i = strDecimals.length ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
				strDecimals = strDecimals.substr(0, intDecimals);
			}else{
				for (var i = 0 ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
			}
			strReturn = arrData[0] + "." + strDecimals ;		
		}else{
			for (var i = 0 ; i < Number(intDecimals) ; i++){
				strDecimals += "0" ;
			}	
			strReturn = strValue + "." + strDecimals ;
		}
		return strReturn;
	}
	
	/*
	*
	*/
	function checkInvalidChar(strValue, strMessage, strControlText){
		var strChkEmpty = FindChar(strValue)
		if (strChkEmpty != "0"){
			return buildError(strMessage, strChkEmpty[0], strControlText);
		}else{
			return "";
		}
	}
	
	function checkInvalidCharForName(strValue, strMessage, strControlText){
		var strChkEmpty = FindCharNameField(strValue)
		if (strChkEmpty != "0"){
			return buildError(strMessage, strChkEmpty[0], strControlText);
		}else{
			return "";
		}
	}
	
	function buildError(strMessage){
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMessage = strMessage.replace("#" + (i+1), arguments[i+1]);
			}
			strMessage = strMessage
		}
		return strMessage;
	}
	
	// Find invalid characters
	function FindChar(StringIn){
		// ------------- Check the standards characters
		/*
		\b 	Backspace
		\f 	Form feed
		\n 	New line
		\r 	Carriage return
		\t 	tab
		\" 	quotation mark
		\'  MS Office single quote 6
		\'  Ms Office singel Quote 9
		\\  Back Slash
		*/

		var CharInArray = new Array("'","<",">","^",'"',"~");
		var CharOutArray=new Array();
		for (var i=0;i<StringIn.length;i++){
			switch (StringIn.charCodeAt(i)){
				case 92 :
					CharOutArray[0]="\\ "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				case 8216 :
				case 8217 :
					CharOutArray[0]="' "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				default :
					for (var j=0;j<CharInArray.length;j++){
						if (StringIn.charAt(i)==CharInArray[j]){
							CharOutArray[0]=CharInArray[j]
							CharOutArray[1]=eval(i+1);
							return (CharOutArray);
						}
					}
					break;
			}
		}
		return "0";
	}
	
	
		// Find invalid characters in a Name Field
	function FindCharNameField(StringIn){
		// ------------- Check the standards characters
		/*
		\b 	Backspace
		\f 	Form feed
		\n 	New line
		\r 	Carriage return
		\t 	tab
		\" 	quotation mark
		\'  MS Office single quote 6
		\'  Ms Office singel Quote 9
		\\  Back Slash
		*/

		var CharInArray = new Array("'","<",">","^",'"',"~","/");
		var CharOutArray=new Array();
		for (var i=0;i<StringIn.length;i++){
			switch (StringIn.charCodeAt(i)){
				case 92 :
					CharOutArray[0]="\\ "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				case 8216 :
				case 8217 :
					CharOutArray[0]="' "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				default :
					for (var j=0;j<CharInArray.length;j++){
						if (StringIn.charAt(i)==CharInArray[j]){
							CharOutArray[0]=CharInArray[j]
							CharOutArray[1]=eval(i+1);
							return (CharOutArray);
						}
					}
					break;
			}
		}
		return "0";
	}
	
	
	// context
	function disablePage(){
		return true;
	}
	
	// Right Trim
	function rightTrim(strValue){
		var objRegExp = /^([\w\W]*)(\b\s*)$/;
	
	      if(objRegExp.test(strValue)) {
	       //remove trailing a whitespace characters
	       strValue = strValue.replace(objRegExp, '$1');
	    }
	  	return strValue;
}
	
	// left trim
	function leftTrim(strValue){
		var objRegExp = /^(\s*)(\b[\w\W]*)$/;
	
	  	if(objRegExp.test(strValue)) {
	    	//remove leading a whitespace characters
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	  	return strValue;
	}
	
	// trim
	function trim(strValue){
		var objRegExp = /^(\s*)$/;
	
	    //check for all spaces
	    if(objRegExp.test(strValue)) {
	       strValue = strValue.replace(objRegExp, '');
	       if( strValue.length == 0)
	          return strValue;
	    }
	
	   //check for leading & trailing spaces
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
	       //remove leading and trailing whitespace characters
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	
	  	return strValue;
	}
	
	// clear cache
	function ClearCache(){
		try{
			window.clipboardData.clearData()
		}catch(e){}
	}
	
	// close child windows
	function CloseChildWindow(){
		try{
			if ((top.objCW) && (!top.objCW.closed)){
				top.objCW.close();
			}
		}catch (ex){}
	}

	// copy key array to a another array
	function keyArrayClone(objArray, objTarget){
		objTarget = new Array();
		for(var key in objArray){
			if(objArray[key] != null){
				objTarget[key] = objArray[key];
			}
		}
		return objTarget; 
	}

function checkPageEdited(){
	var winCC=(top.name=='winCC')?top:top.opener.top;
	if (top.pageEdited){
		return !confirm(winCC.arrError['XBE-WARN-01']);
	}
	return false;
}				

function CloneObject(what) {
	for (i in what) {
		if (typeof(what[i])=='object') {
			this[i]=new CloneObject(what[i]);
		}else{
			this[i]=what[i];
		}
	}
}

function refillArray(arrTo,arrFrom){
		var len=arrFrom.length;
		var consT="";
		var bSimpleType=true;

		for (var i=0;i<len;i++){
			bSimpleType=isSimpleType(arrFrom[i]);

			if(bSimpleType){
				arrTo[i]=arrFrom[i];				
			}else if((arrFrom[i].constructor+'').indexOf('function Array()')!=-1){
				//alert(arrFrom[i].constructor);
				//alert(arrFrom[i].constructor==Array);
				if(!arrTo[i]||arrTo[i]==null){
					arrTo[i]=new Array();
				}else if((arrTo[i].constructor+'').indexOf('function Array()')==-1){
					arrTo[i]=new Array();
				}
				refillArray(arrTo[i],arrFrom[i])
			}else{
				arrTo[i]=new CloneObject(arrFrom[i]);
			}
		}
}

function isSimpleType(val){
	var bSimpleType=!val||val==null;
	var cons;
	
	if(!bSimpleType){
		bSimpleType=bSimpleType||typeof(val)=='string'||typeof(val)=='number';
		bSimpleType=bSimpleType||typeof(val)=='boolean'||typeof(val)=='function';
	}

	if(!bSimpleType){
		cons=val.constructor;
		bSimpleType=bSimpleType||cons.constructor==Date;
	}

	return bSimpleType;
}
function addNumbers(){
		var len=arguments.length;
		var total=0;
		for(var i=0;i<len;i++){
			if(Number(arguments[i]))total+=Number(arguments[i]);
		}
		return total;
}

function showWindow(height,width,url,winName){
	var y=(window.screen.height-height)/2;
	var x=(window.screen.width-width)/2;
	var props='toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width='+width+',height='+height+',resizable=no,top='+y+',left='+x;
	var win=window[winName];

	if (win) {
        if(!win.closed) {
            win.focus();
            return;
        }
    }

	window[winName]=window.open(url,winName,props);
}
function showModalWindow(height,width,url,obj){
	var y=(window.screen.height-height)/2;
	var x=(window.screen.width-width)/2;
	var props='toolbar:yes;menubar:yes;scrollbars:yes;status:no;help:no';
		props+=';dialogwidth:'+width+'px;dialogheight:'+height+'px';
		props+=';dialogtop:'+y+';dialogleft:'+x;
	window.showModalDialog(url,obj,props);
}
	
function showContextMenu(){
	var bln=enableContextMenu();
	return bln;
}

function getNotNull(str){
	return (str==null||str=="")?VAL_XBE_NULL:str;
}

	// Replace all 
	function replaceall(strValue, strRepValue, strNValue){
		var i = strValue.indexOf(strRepValue);
		while(i > -1){
			strValue = strValue.replace(strRepValue, strNValue);
			i = strValue.indexOf(strRepValue);
		}
		return strValue
	}

function replaceEnter(inputValue) {
	
	var output = "";
	for (var i = 0; i < inputValue.length; i++) {
		if (inputValue.charCodeAt(i) == 13) {
			output += " ";				
		}else if (inputValue.charCodeAt(i) == 10) {	
			output +=  " ";				
		} else {
			output += inputValue.charAt(i);
   		}
	}	
	return output;	
}
function hideEnter(inputValue) {
	
	var output = "";
	for (var i = 0; i < inputValue.length; i++) {
		if (inputValue.charCodeAt(i) == 13) {
			output += "<br>";				
		}else if (inputValue.charCodeAt(i) == 10) {	
			output +=  "<br>";				
		} else {
			output += inputValue.charAt(i);
   		}
	}	
	return output;	
}

function addEnter(instr) {
	var output = "";
	var str = String.fromCharCode(13);	
	var strarr = instr.split("<br>");				 
	for (var k= 0; k < strarr.length; k++) {
		if(k == strarr.length - 1) {
			output = output +strarr[k];
		}else {
			output = output +strarr[k] + str  ;
		}					
	}
	return output;	

}

function setTopMessage(aMessage){
	top.aMessage=new CloneObject(aMessage);
}


function KeyPress(objC, objValType){
	var blnReturn = true;
	switch (objValType){
		case "ANW" : blnReturn = isAlphaNumericWhiteSpace(objC.value); break;
		case "AN" : blnReturn = isAlphaNumeric(objC.value); break;		
		case "A" : blnReturn = isAlpha(objC.value); break;				
		case "N" : blnReturn = validateInteger(objC.value); break;			
	}
	if (!blnReturn){
		objC.value = objC.value.substr(0,objC.value.length -1); 
		objC.focus();
	}
}

function setPageEdited(bln){
	top.pageEdited=bln;
}	

function isPageEdited(){
	return top.pageEdited;
}

	// focus to a control
	function setFocus(id){
		var objControl = getFieldByName(id);
		var intLength = objControl.length ;
		if (intLength > 1){
			objControl[0].focus();
		}else{
			try{
				document.getElementById(id).focus();
			}catch(e){
				//if not focusable continue
			}
		}
	}

	// Copy array to a another array
	function arrayClone(arrSource, arrTarget){
		arrTarget.length = 0 ; 
		for (var i = 0 ; i < arrSource.length ; i++){
			arrTarget[i] = new Array();
			for (var m = 0 ; m < arrSource[i].length ; m++){
				arrTarget[i][m] = arrSource[i][m];
			}
		}
	}

	// conver the text to Title Case
	function converToTitleCase(strValue){
		var arrValues = strValue.split(" ");
		strValue = "";
		var strFC = ""
		var strOC = ""
		for (var i = 0; i < arrValues.length ; i++){
			strOC = "";
			if (strValue != ""){strValue += " ";}
			arrValues[i] = trim(arrValues[i]);
			arrValues[i] = arrValues[i].toLowerCase();
			strFC = arrValues[i].substr(0,1).toUpperCase();
			if (arrValues[i].length > 1){
				strOC = arrValues[i].substr(1,arrValues[i].length - 1).toLowerCase()
			}
			strValue += strFC + strOC;
		}
		return strValue
	}
	
	// set the Child window Properties 
	function getChildWindowProp(intHeigh, intWidth){
		var intTop 	= (window.screen.height - intHeigh) / 2;
		var intLeft = (window.screen.width -  intWidth) / 2;
		var strScroll = "no";
		if (arguments.length > 2){
			if (arguments[2] == true){
				strScroll = "yes";
			}
		}
		var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=' + strScroll + ',width=' + intWidth + ',height=' + intHeigh + ',resizable=no,top='+ intTop +',left=' + intLeft
		return strProp
	}
	
	// Fill characters
	function fillChar(strValue, intLength, strChar){
		strValue = trim(String(strValue));
		var intVLength = strValue.length; 
		var strZero = "";
		for (var i = intVLength ; i < intLength ; i++){
			strZero += strChar;
		}
		return String(strZero) + String(strValue);
	}
	
	// Array Short Client side
	function sortArray(intColumn, strOrder, strArrayName){
		strArrayName = eval(strArrayName);
		var intCount = strArrayName.length
		for (var a = 1 ; a = 1;){
		//while (blnFound == false){
			var blnFound = false
			for (var i = 0 ; i < intCount ; i++){
				if ((i+1) < intCount){
					var strCompareValue = String(strArrayName[i][intColumn]);
					var strCompareTo = String(strArrayName[i+1][intColumn]);
					
					// Date Check 
					var arrMTemp;
					var arrTemp;
					var strLastValues = ""
					if (String(strCompareValue).indexOf('/') != -1){
						arrMTemp = strCompareValue.split(" ");
						arrTemp = arrMTemp[0].split("/");
						if (arrTemp.length == 3){
							if (arrTemp[2].length == 4 && arrTemp[1].length == 2 && arrTemp[0].length == 2){
								strLastValues = ""
								if (arrMTemp.length == 2){
									strLastValues = arrMTemp[1].replace(":","");
								}
								strCompareValue = String(arrTemp[2]) + String(arrTemp[1]) + String(arrTemp[0]) + strLastValues
							}
						}
					}
					
					if (strCompareValue.indexOf(',') != -1){
						strCompareValue = strCompareValue.replace(',',"");
					}
					
					if (strCompareTo.indexOf('/') != -1){
						arrMTemp = strCompareTo.split(" ");
						arrTemp = arrMTemp[0].split("/");
						if (arrTemp.length == 3){
							strLastValues = ""
								if (arrMTemp.length == 2){
									strLastValues = arrMTemp[1].replace(":","");
								}
								strCompareTo = String(arrTemp[2]) + String(arrTemp[1]) + String(arrTemp[0]) + strLastValues
						}
					}
					
					if (strCompareTo.indexOf(',') != -1){
						strCompareTo = strCompareTo.replace(',',"")
					}
					if (!isNaN(Number(strCompareValue))){
						strCompareValue = Number(strCompareValue);
					}else{
						strCompareValue = strArrayName[i][intColumn];
					}
					if (!isNaN(Number(strCompareTo))){
						strCompareTo = Number(strCompareTo);
					}else{
						strCompareTo = strArrayName[i+1][intColumn];
					}
					if (strOrder.toUpperCase() == "ASC"){
						if (strCompareValue > strCompareTo){
							var arrCurrData = new Array();
							var intarrColumns = strArrayName[i].length ;
							for (var x = 0 ; x < intarrColumns ; x++){
								arrCurrData[x] = strArrayName[i][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i][x] = strArrayName[i+1][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i+1][x] = arrCurrData[x];
							}
							blnFound = true
						}
					}else{
						if (strCompareValue < strCompareTo){
							var arrCurrData = new Array();
							var intarrColumns = strArrayName[i].length ;
							for (var x = 0 ; x < intarrColumns ; x++){
								arrCurrData[x] = strArrayName[i][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i][x] = strArrayName[i+1][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i+1][x] = arrCurrData[x];
							}
							blnFound = true
						}				
					}
				}
			}
			if (blnFound == false){
				break;
			}
		}
	}		
	
	/*
	 * Check Key Array is available
	 */
	 function checkKeyArrayAvailable(objArray){
		var blnReturn = false;
		for(var key in objArray){
			blnReturn = true;
		}
		return blnReturn;
	}
	
	function getArrayCodes(objArray, intIndex,  strID, intElement){
		var strReturn = "";
		for (var i = 0 ; i < objArray.length ; i++){
			if (objArray[i][intIndex] == strID){
				strReturn = objArray[i][intElement];
				break;
			}
		}
		return strReturn;
	}
	
	// get The Disabled Status
	function getDisabled(strID){
		return getFieldByID(strID).disabled;
	}
	
	function getMouseX(objEvent){
		var x = 0;
		if (browser.isIE) {
			x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				x = objEvent.clientX;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				x = objEvent.clientX;
			}else{
				x = objEvent.clientX + window.scrollX;
			}
		}	
		return x;
	}
	
	function getMouseY(objEvent){
		var y = 0;
		if (browser.isIE) {
			y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				y = objEvent.clientY;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				y = objEvent.clientY + document.documentElement.scrollTop;
			}else{
				y = objEvent.clientY + window.scrollY;
			}
		}	
		return y;
	}

	/*
	 * Select All in the Text
	 */
	function selectAll(strID){
		getFieldByID(strID).select()
	}
	
	/*
	 * Window Screen Height
	 */
	function getPageHeight(){
		var y;
		if (self.innerHeight){
			// all except Explorer{
			y = self.innerHeight;
		}else if (document.documentElement && document.documentElement.clientHeight){
			// Explorer 6 Strict
			y = document.documentElement.clientHeight;
		}else if (document.body){
		 	// other Explorers
			y = document.body.clientHeight;
		}
		return y;
	}
	
	/*
	 * Window Screen Width
	 */
	function getPageWidth(){
		var x
		if (self.innerHeight){
		 	// all except Explorer{
			x = self.innerWidth;
		}else if (document.documentElement && document.documentElement.clientWidth){
			// Explorer 6 Strict
			x = document.documentElement.clientWidth;
		}else if (document.body){
			// other Explorers{
			x = document.body.clientWidth;
		}
		return x;
	}
	
	/*
	 * set Z-index 
	 */
	function setZIndex(strControlID, intIndex){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.zIndex = intIndex;
	}
	
	// Set the Cursor
	function setCursor(strControlID, strCursor){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.cursor = strCursor;
	}
	
	/*
	 * Mouse Button Click
	 */
	function getMouseButtonClick(objEvent){
		var intBtn = null;
		if (browser.isIE){
			objEvent = window.event;
		}
		intBtn = objEvent.button;
		return intBtn;
	}

function contain(arr,val){
	var len=arr.length;
	for(var i=0;i<len;i++){
		if(arr[i]==val){
			return true;
		}
	}
	return false;
}

function debug(){
	var args=debug.arguments;
	var str='';
	for(var i=0;i<args.length;i++){
		str+=args[i]+'\n';
	}
	alert(str);
}

//Sort an array
	function sort(arr, compPos){
		//Insertion Sort
		//For normal sort "compPos" should be -1, otherwise the sort position the internal array should be given.
		//Ex. Consider following array format.
		//		ParentArrayElement_1 -> InternalArrPos_1, InternalArrPos_2, InternalArrPos_3
		//		ParentArrayElement_2 -> InternalArrPos_1, InternalArrPos_2
		//		ParentArrayElement_2 -> InternalArrPos_1, InternalArrPos_2, InternalArrPos_3, InternalArrPos_4
		//
		//    In this case if we call as sort(ParentArray, 1), it will sort the ParentArray according to content 
		//    of "InternalArrPos_2" possition of the internal array.
		var temp;
		var i;
		var j;
		for (i=1; i<arr.length; i++)
		{
			temp = arr[i];
			if (compPos >=0)
			{
				//Need to sort depend on an internal array content
				for (j=i; (j>0) && (temp[compPos] < arr[j-1][compPos]); j--)
				{
					arr[j]=arr[j-1];
				}
			} else {
				//Normal sort
				for (j=i; (j>0) && (temp < arr[j-1]); j--)
				{
					arr[j]=arr[j-1];
				}
			}
			
			arr[j]=temp;
		}
		return arr;
	}
	
	function getScrolling(){
		var scroll=(screen.width=='800')?'auto':'no';
		return scroll;
	}

	function enableContextMenu(){
		if(window.location.href.indexOf('https')!=-1){
			if(request&&request['debugFrontend']=='true'){
				return true;
			}else{
				return false;
			}
		}
		var winCC=(top.name=='winCC')?top:top.opener.top;
		if(!winCC.request){
			return false;
		}
		var bln=(winCC.request['debugFrontend']=='true')?true:false;
		return bln;
	}
	
//accellaero currency roundup
function getRoundedValue(input, boundry, breakPoint) {
	var output  = 0;
	if(boundry == 0) return output;
	var divValue = Math.floor(input / boundry);			
	var modValue = input % boundry;			
	var difference = 0;
	if(modValue != 0) {
		if(breakPoint == 0) {
			divValue = divValue +1;
		}else if(breakPoint != boundry) {
			difference = input - (divValue * boundry);					
			if(difference == breakPoint || difference > breakPoint) {
				divValue = divValue +1;
			}
		}		
	}

	output = addDecimals(divValue * boundry, getDecimalCount(boundry));
	return output;
}

//function to search an array for the existence of a value
function isValueExist(arr,value){
	var x;
	for (x in arr){
		if(value==arr[x]){
			return true;
		}
	}
	return false;
}

//number rounding
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

function getDecimalCount(no){
    var str=''+no;
    return str.substr(str.indexOf('.')+1).length;
}


/* --------------------------------------------------------- end of Page --------------------------------------------------------- */