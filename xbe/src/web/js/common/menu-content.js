var strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
var objMenuLoad	= null;
var strServerErrorMsg = "";
var strServerErrorRedirect = "";
var blnSeatBlocked = false;
var strSeatBlockFun = "";
var strClickedPage = "";
var strClickedFID = ""
var blnProcessCompleted = false
var blnPageTimeOut = false;
var objPageLoadedTimer = null;
var blnExit = false;
var blnInterLine = false;

addMenu("AAMenu", "menu-top");
var strMImagePath = "<img src=../images/AALogo_no_cache.jpg>"
for (var i = 0 ; i < arrMenu.length ; i++){
	arrMenu[i][2] = replaceall(arrMenu[i][2], "imgPath", strMImagePath); // this line added

	switch (arrMenu[i][0]){
		case "M" :
			addLink(arrMenu[i][1], arrMenu[i][2], "", "javascript:top.LoadPage('" + arrMenu[i][3] + "','" + arrMenu[i][4] + "','" + arrMenu[i][2] + "')", "");
			break ;
		case "S" :
			addSubMenu(arrMenu[i][1], arrMenu[i][2], "", "", arrMenu[i][5], "");
			break ;
		case "L" :
			addSeparator(arrMenu[i][1], "");
			break;
	}
}
endMenu();

function switchAirLine(intID){
	alert("TODO\n" + intID);
}

function LoadAdmin(){
	alert("Load Admin");
}

//TODO remove this after V1 is removed
function loadMakeBkgV2(){
	if (top.arrPrivi["xbe.res.make"] == 1){
		top[1].location.replace("makeReservation.action");
	}
}

function loadModifyBkgV2(){
	if (top.arrPrivi["xbe.res.alt"] == 1){
		top[1].location.replace("showNewFile!searchRes.action");
	}
}


function loadAlerts(){
	if (top.arrPrivi["xbe.alert"] == 1){
		LoadPage("showAlert.action", "SC_RESV_CC_012");
	}
}

function loadFlightManifest(){
	if (top.arrPrivi["xbe.res.stat"] == 1){
		LoadPage("showFlight.action", "SC_RESV_CC_016");
	}
}

function loadChangePassword(){
	LoadPage("showPasswordChange.action", "SC_CHANGE_PASSWORD");
}

function loadNewModifyBooking(strPNR, loadNeverLoadedRes){
	if (top.arrPrivi["xbe.res.alt"] == 1){
		blnSearchBooking = true
		strSearchBookingPNR = strPNR;
		var redirectUrl = "showNewFile!searchRes.action?load=";
		if(loadNeverLoadedRes){
		  // will loads own reservation which is never loaded via LCC.
			redirectUrl += "ALL";
		    blnAutoSearch = true;
		}else{
			redirectUrl += "LOAD";
		}
		top[1].location.replace(redirectUrl);
	}
}


function closeChildWindows(){
	if ((top.objCWindow)&& (!top.objCWindow.closed)){top.objCWindow.close();}
}

function CloseWindow(){
	if (!confirm(arrError['XBE-CONF-04'])){
		return;
	}
	closeChildWindows();
	blnExit = true;
	top[1].location.replace("showMain");
	
	proceedOK=true;
	makePOST("../public/logout.action","");
}

function processStateChange(){
	if (httpReq.readyState==4){
		top.location.replace("closeMenu");
		//top.close();
	}
}

function LoadHome(){
	if (loadCheck()){
		strLastPage = "";
		top[0].initializeVar();
		top[2].MessageReset();
		top[1].location.replace("showMain");
		setFID("");
	}
}

function loadCheck(){
	if (parent.pageEdited){
		if (confirm(arrError['XBE-WARN-01'])){
			parent.pageEdited = false;
			strLastPage = "";
			return true;
		}else{
			return false;
		}
	}else{
		strLastPage = "";
		parent.pageEdited = false;
		return true;
	}
}

function LoadPage(strPage, strFID, strDes){
	if (!blnProcessCompleted){
		if (strPage.indexOf("http") != -1){
			window.open(strPage,'winAdmin','toolbar=no,location=no,status=Yes,menubar=no,scrollbars=yes,width=1024,height=768,resizable=no,top=1,left=1');
		}else {
			if (loadCheck()){
				if (seatBlockCheck()){
					strClickedPage = strPage;
					strClickedFID = strFID
					if (objMenuLoad != null){
						clearTimeout(objMenuLoad);
					}
					if (top.arrPrivi['xbe.res.make.seat.block'] == 1) {
						if((strFID == "SC_FACM_001") || (strFID == "SC_FACM_005") || (strFID == "SC_ADMN_015") || (strFID == "SC_ADMN_002") || (strFID == "SC_FACM_006") || (strFID == "SC_FACM_008")){
							if (top[1].strPageID != "MENU") {
								top[1].location.replace("showMain");		
							} else if (top[1].objTMenu.tabLoad(strPage, strDes, strFID, "")){
								top[2].ShowProgress();
							}
						} else {
							objMenuLoad = setInterval("menuLinkPageLoad()", 300);
						}
					} else {
						if((strFID == "SC_FACM_001") || (strFID == "SC_FACM_005") || (strFID == "SC_ADMN_015") || (strFID == "SC_ADMN_002") || (strFID == "SC_FACM_006")){
							if (top[1].strPageID != "MENU"){
								top[1].location.replace("showMain");		
							} else if (top[1].objTMenu.tabLoad(strPage, strDes, strFID, "")){
								top[2].ShowProgress();
							} else {
								menuLinkPageLoad();
							}
						} else {
							objMenuLoad = setInterval("menuLinkPageLoad()", 300);
						}
					}
				}
			}
		}
	}else{
		alert("System Loading in Progress. Please wait.");
		return false;
	}
}



function menuLinkPageLoad(){
	if (!blnSeatBlocked){
		clearTimeout(objMenuLoad);
		ResetTimeOut();
		top[2].MessageReset();
		top[0].initializeVar();
		top[2].ShowProgress();
		if (strClickedPage.indexOf("javascript:") != -1){
			eval(strClickedPage.substr(11, strClickedPage.length - 10));
		}else{
			closeChildWindows()
			top[1].location.replace(strClickedPage);
		}
		strLastPage = strClickedPage;
		setFID(strClickedFID);
		top[2].LoadBandwidthInfo();
		if (objPageLoadedTimer != null){
			clearTimeout(objPageLoadedTimer);
		}
		blnProcessCompleted = true;
		objPageLoadedTimer = setInterval("ClearPageLoaded()", 3000);
	}
}

function ClearPageLoaded(){
	clearTimeout(objPageLoadedTimer);
	blnProcessCompleted = false;
}

function setFID(strFID){
	top[2].DivWrite("spnScreenID","<font class='fontPGInfo'>Screen ID : <b>" + strFID + "<\/b><\/font>");
}


function showServerErrors(strErrorMsg, strRedirect){
	strServerErrorMsg = strErrorMsg;
	strServerErrorRedirect = strRedirect;
	top[1].location.replace("showErrorMessage");
}


function varInitialize(){
	strLastPage = "";
	blnProcessCompleted = false
	blnSeatBlocked = false;
	parent.pageEdited = false;
	strPageFrom		= "";
	strPageReturn	= "";
	blnSearchBooking = false
	strSearchBookingPNR = "";
	blnInterLine = false;
	strSearchCriteria = "";
	blnAutoSearch = false;
}

function seatBlockCheck(){
	if (top.arrPrivi['xbe.res.make.seat.block'] == 1){
		if (blnSeatBlocked){
			if (confirm(arrError['XBE-WARN-02'])){
				if (strSeatBlockFun != ""){
					eval(strSeatBlockFun);
				}
				return true;
			}else{
				return false;
			}
		}
	}
	return true;
}


// Timeout page 
// Time out setting
function Timeout() {
	
	//props
	this.timerOn		= false;
	this.warningOn		= false;
	this.timerId;
	this.warningId;
	this.timePeriod		= 10000;
	this.loginScr		= "about:blank";

	//methods
	this.setTime = setTime;
	this.getTime = getTime;
	this.startTimer = startTimer;
	this.stopTimer = stopTimer;
	
	//
	function setTime(tPeriod) {
		this.timePeriod = tPeriod;
	}
	function getTime() {
		getTime = this.timePeriod;
	}
	function setLoginScreen(scrName) {
		this.loginScr = scrName;
	}
	function startTimer() {
		if (this.timerOn) {
			clearTimeout(this.timerId);
		}
		//
				
		top.loginScr = this.loginScr;
		
		this.timerId = setTimeout('logoff()',this.timePeriod);
		this.timerOn = true;
		//global var to check whether session end.
		top.sessionEnd = false;
		
	}
	function stopTimer() {
		if (this.timerOn) {
			clearTimeout(this.timerId);
		}
		this.timerOn = false;
	}
}
function logoff() {
	if (top[1].strPageID != "MENU"){
		window.open("showTimeOut","AlertWin",'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width=600,height=170,resizable=no,top=250,left=280')	
	}else{
		ResetTimeOut();
	}
}

var objT = new Timeout();
function ResetTimeOut(){
	objT.setTime(intTimerMiliSeconds);
	objT.startTimer();
}

function remoteSubmit() {
 	var url = "showLoadData.action?id=TIMEOUTRESET"; 
 	if (window.XMLHttpRequest) { // Non-IE browsers
    	reqObj = new XMLHttpRequest();
      	reqObj.onreadystatechange = processStateChange2;
      	try {
        	reqObj.open("POST", url, true);
      	}catch (e) {
      	}
      	reqObj.send(null);
	}else if (window.ActiveXObject) { // IE
  		reqObj = new ActiveXObject("Microsoft.XMLHTTP");
  		if (reqObj) {
	   		reqObj.onreadystatechange = processStateChange2;
    		reqObj.open("POST", url, true);
    		reqObj.send();
  		}
	}
}

function processStateChange2() {
	if (reqObj.readyState == 4){
    	if (reqObj.status == 200){
	  		ResetTimeOut();
	  	}
	}
}

function defTabClick(strID){
	setFID(strID);
	top[2].HidePageMessage();
	return true;
}

function PageTimedOut(){
	location.replace("showPageTimedOut");
}

function callMulticitySearch(){
		top[1].location.replace("demo!loadMulticiti.action");
}

function callUtilityPanel(srt){
	if (srt == null){srt = 'dv=true&exCur=true&wc=true';}
	var t = window.open("showNewFile!loadUtilPopup.action?"+srt,"Utility",'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width=658,height=400,resizable=no,top=250,left=280')
}

ResetTimeOut();