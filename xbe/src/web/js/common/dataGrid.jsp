<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/ToolTip.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<style type="text/css">
.pgScroll{
	overflow-x:hide;
	overflow-y:scroll;
}
.pgNoScroll{
	overflow-x:hide;
	overflow-y:hide;
}
.posAbs{
	position:absolute;
}
.posRelative{
	position:relative;
}
</style>
</head>
<body id="bdID" class="GridBody pgScroll" onkeydown="return shortCutKeyPress(event)"  oncontextmenu="return false" ondrag='return false'>
<span id="spnDG"></span>
<div id="spnTT" name="spnTT" style="position: absolute; z-index:1000;"></div>
<script  type="text/javascript">
<!--
var strGridID = "";
var _objDG = null;

function writeGrid(strText, strID, strBGColor){
	DivWrite("spnDG", strText);
	var objControl = document.getElementById("spnDG")
	var intWD = getPageWidth();
	var blnScroll = true;
	_objDG = parent._getDGObject(strID);
	strTTImagePath = _objDG.toolTipImagePath;
	
	if (!browser.isIE){
		intWD = getPageWidth() - _objDG.scrollWidth;
		if (_objDG.gridScroll.toUpperCase() == "NO"){
			setStyleClass("bdID", "GridBody pgNoScroll");
			intWD = getPageWidth();
		}
	}
	parent.gridHDWrite(strID, objControl.clientHeight, intWD);
	strGridID = strID;
	document.getElementById("bdID").style.backgroundColor = strBGColor;
}

var blnGridLoad = true;
function shortCutKeyPress(objEvent){
	if (Body_onKeyDown(objEvent)){
		parent.gridShortCutKey(objEvent, strGridID)
	}else{
		return false;
	}
}

function gridRowMouseDown(objEvent, intRowID, strID){
	parent._gridRowMouseDown(getMouseButtonClick(objEvent), intRowID, strID);
}

function _gridMouseOver(objEvent, strRowID, strID, strRowNo, intColumnCount, intColumn){
	if (_objDG.rowOver){
		parent.gridMouseOver(strRowID, strID, strRowNo, intColumnCount);
	}
	
	if (_objDG._arrDGBlnTooltip[intColumn]){
		if (_objDG.toolTipOnMouseOver != ""){
			var arrTTInfo = eval("parent." + _objDG.toolTipOnMouseOver + "(" + strRowNo + "," + intColumn + ")");
			if (typeof(arrTTInfo) == "object"){
				showTT(arrTTInfo[0], arrTTInfo[1], arrTTInfo[2], objEvent, arrTTInfo[3], arrTTInfo[4], arrTTInfo[5]);
			}
		}
	}
}

function _gridMouseOut(objEvent, strRowID, strID, strRowNo, intColumnCount, intColumn){
	if (_objDG.rowOver){
		parent.gridMouseOut(strRowID, strID, strRowNo, intColumnCount);
	}
	
	if (_objDG._arrDGBlnTooltip[intColumn]){
		hideTT();
	}
}

//-->
</script>
</body>
</html>
