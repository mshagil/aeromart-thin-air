//-- Values
var PRIVI_OVERRIDE_CHARGES='xbe.res.alt.charge';
var PRIVI_PAX_REFUND='xbe.res.alt.acc.ref';
var PRIVI_PAX_REFUND_CASH="xbe.res.alt.acc.ref.cash";
var PRIVI_PAX_REFUND_ONACC="xbe.res.alt.acc.ref.acc";
var PRIVI_PAX_REFUND_ONACC_ANY="xbe.res.alt.acc.ref.acc.any";
var PRIVI_PAX_REFUND_OPTCARRIER_ONACC_ANY="xbe.res.alt.acc.ref.opcarrier.acc.any";
var PRIVI_PAX_REFUND_CCARD="xbe.res.alt.acc.ref.card";
var PRIVI_PAX_REFUND_CCARD_ANY="xbe.res.alt.acc.ref.card.any";
var PRIVI_PAX_ADJUST_CREDIT='xbe.res.alt.acc.adj';
var PRIVI_PAX_ADJUST_CREDIT_ANY="xbe.res.alt.acc.adj.any";
var PRIVI_PAX_ADJUST_CREDIT_REFUND="xbe.res.alt.acc.adj.refund";
var PRIVI_PAX_ADJUST_CREDIT_NONREFUND="xbe.res.alt.acc.adj.nonrefund";
var PRIVI_PAX_VIEW_CREDIT_INFO_TAB='xbe.res.acc.view.pax.credit';
var PRIVI_PAX_CREDIT_EXPIRY_CHANGE='xbe.res.alt.acc.adj.expirychange';
var PRIVI_PAX_CREDIT_REINSTATE='xbe.res.alt.acc.adj.reinstate';
var PRIVI_BLOCK_SEATS='xbe.res.make.seat.block';
var PRIVI_CHANGE_COS='xbe.res.make.cos';
var PRIVI_GROUP_BOOKING='xbe.res.make.group';
var PRIVI_CHANGE_FARE='xbe.res.make.alterfare';
var PRIVI_VIEW_ALL_FLIGHTS='xbe.res.make.allflights'
var PRIVI_ON_HOLD='xbe.res.make.onhold';
var PRIVI_LOAD_PROFILE='xbe.res.make.profile';
var PRIVI_CASH_PAYMENT='xbe.res.make.pay.cash';
var PRIVI_CARD_PAYMENT='xbe.res.make.pay.card';
var PRIVI_VOUCHER_PAYMENT='xbe.res.make.pay.voucher';
var PRIVI_ONACC_PAYMENT='xbe.res.make.pay.acc';
var PRIVI_ANY_ONACC_PAYMENT='xbe.res.make.pay.acc.any'
var PRIVI_RPT_ONACC_PAYMENT='xbe.res.make.pay.acc.rpt'
var PRIVI_NO_PAY='xbe.res.make.confirm.nopay';
var PRIVI_CHECK_ON_HOLD='xbe.res.make.pay.pax';
var PRIVI_APPLY_CREDIT='xbe.res.make.pay.credit';
var PRIVI_APPLY_CREDIT_ANY='xbe.res.make.pay.credit.any';
var PRIVI_UTILIZE_ANYCARRIER_CREDIT='xbe.res.use.anycarrier.pax.credit';
var PRIVI_VIEW_ACCOUNT='xbe.res.alt.acc';
var PRIVI_CHANGE_CURRENCY='xbe.res.make.currency';
var PRIVI_TRANSFEROWNERSHIP='xbe.res.make.owner';
var PRIVI_ADVANCE_SEARCH = 'xbe.res.alt.find.adv';
var PRIVI_ADD_SSR='xbe.res.add.ssr';
var PRIVI_PAX_REFUND_REP = 'xbe.res.alt.acc.ref.acc.rpt';

var PRIVI_TBA_BKG = 'xbe.res.make.tba';

var PRIVI_ADD_SEGMENT='xbe.res.alt.seg.add';
var PRIVI_MODIFY_SEGMENT='xbe.res.alt.seg.mod';
var PRIVI_CANCEL_SEGMENT='xbe.res.alt.seg.cnx';
var PRIVI_TRANSFER_SEGMENT = 'xbe.res.alt.seg.tnx';

var PRIVI_CANCEL_RES = 'xbe.res.alt.cnx';
var PRIVI_MODIFY_CONTACT = 'xbe.res.alt.contact';
var PRIVI_EXTEND_OHD = 'xbe.res.alt.extend';
var PRIVI_NAME_CHANGE = 'xbe.res.alt.name';
var PRIVI_SSR_CHANGE = 'xbe.res.alt.ssr';
var PRIVI_EDIT_USER_NOTE = 'xbe.res.alt.edit';
var PRIVI_REMOVE_PAX = 'xbe.res.alt.pax.del';
var PRIVI_ADD_INFANT = 'xbe.res.alt.pax.inf';
var PRIVI_SPLIT_RES = 'xbe.res.alt.pax.split';
var PRIVI_VIEW_AGENT_CREDIT = 'xbe.res.creditview';
var PRIVI_CLEAR_ALERT = 'xbe.res.alt.alert';
var PRIVI_TBA_CHANGE = 'xbe.res.alt.tba';

var PRIVI_MODIFY_OLD_RESREVATION='xbe.res.alt.old';
var PRIVI_ALTER_OLD_RES = 'xbe.res.alt.old';
var PRIVI_ALTER_BUFFERED_RES = 'xbe.res.alt.modify.buffer';
var PRIVI_MAKE = 'xbe.res.make';

var PRIVI_MODIFY_CNX_RESREVATION='xbe.res.alt.modify.cnx';
var PRIVI_HIDE_CHARGES = 'xbe.res.itn.chg';
var PRIVI_ALLOW_ON_HOLD_PRINT = 'xbe.res.itn.onhold';

var PRIVI_ALTER_BUFFERED_RES_NAME = 'xbe.res.alt.modify.buffer.name';
var PRIVI_ALTER_BUFFERED_SSR_CHANGE = 'xbe.res.alt.modify.buffer.ssr';
var PRIVI_VIEW_ACCOUNT_BUFFERED = 'xbe.res.alt.modify.buffer.charges';

var PRIVI_CHANGE_FARE_ANY = 'xbe.res.make.alterfare.any';
var PRIVI_PAX_REFUND_NO_CREDIT = 'xbe.res.alt.acc.ref.nocred';
var PRIVI_MAKE_STANDBY_BOOKING =  'xbe.res.make.standby.booking';

var PRIVI_ITN_PAX = 'xbe.res.itn.chg';
var PRIVI_ITN_PAY = 'xbe.res.itn.withpay';
var PRIVI_ITN_CHG = 'xbe.res.itn.withcharges';
var PRIVI_ITN_SELECT_PAX = 'xbe.res.itn.selectpax';

//fare display Privileges
var PRIVI_SHOW_SEATS = 'xbe.res.make.alterfare.show.seats';
var PRIVI_SHOW_AGENTS = 'xbe.res.make.alterfare.show.agents';
var PRIVI_OVERRIDE_AGENT_FARE = 'xbe.res.make.alterfare.agfare';

//ext pay Privileges
var PRIVI_EXT_GRID = 'xbe.res.alt.ext.pay';
var PRIVI_EXT_SYNC = 'xbe.res.alt.ext.pay.sync';
var PRIVI_EXT_UPDATE = 'xbe.res.alt.ext.pay.update';
//-->

//view cancelled itenarary
var PRIVI_VIEW_CNX_ITEN = 'xbe.res.alt.view.cnx.itn';

//privileges for access reservations
var PRIVI_ACC_GDS_RES = 'xbe.res.allow.gds';

//Allow Select Pax to print/Email Itinerary
var PRIVI_ITN_SELECTPAX = 'xbe.res.itn.selectpax';

//segment modification
var PRIVI_MODIFY_SEGMENT_DATE = "xbe.res.alt.seg.mod.date";
var PRIVI_MODIFY_SEGMENT_ROUTE = "xbe.res.alt.seg.mod.route";

//Itinerary Fare Mask 
var PRIVI_ITINERARY_FARE_MASK = "xbe.res.make.itin.fare.mask";
var PRIVI_ITINERARY_FARE_MASK_MODIFY = "xbe.res.alt.itin.fare.mask";

var PRIVI_ALLOW_CREATE_USER_ALERT = "xbe.res.create.alert";
//to add fare discount for reservation 
var PRIVI_ADD_FARE_DISCOUNT ="xbe.res.make.addFareDisc";

//to override fare rule discount percentage for fare
var PRIVI_OVERRIDE_FARE_DISCOUNT = "xbe.res.make.override.fare.disc";

var PRIVI_ADD_CALL_ORIGIN_COUNTRY = "xbe.res.make.callorigin.country";
var PRIVI_MODIFY_CALL_ORIGIN_COUNTRY = "xbe.res.alt.callorigin.country";

