//$Id$

var top=this;
var aMessage=new Array();
var showXBEMain="showXBEMain.action"
var aMessage=new Array();
var languageDataObj = {};

function onLoad() {
  var objects = languageData.substring(1, languageData.length - 1).split(",");
  objects.forEach(function(element) {
      var obj = element.split("=");
      languageDataObj[obj[0].trim()] = obj[1].trim();
  });
  $("#divBookingTabs").seti18nDataMsg();
}
$(document).ready(function() {
  isReseted();
  if (request['useSecureLogin'] && window.location.href.indexOf('https://') == -1) {
      window.location.replace(request['secureUrl'] + "?reset=" + isReset);
  } else {
      setField("txtUserId", request['userId']);
      getFieldByID("pwdOld").focus();

      if (reqMessage != null && reqMessage != "") {
          showPageERRMessage(reqMessage);
      }
  }
});

function changePassword(){
	var url;
	if (validate()){
		resetPageERRMessage();
		disableButtons(true);
		//ShowProgress();
		showPageProgressMessage();	
		url="saveXBEData.action"

		var params="hdnCommands=CHANGEPASSWORD&pwdOld="+getValue('pwdOld')+"&pwdNew="+getValue('pwdNew')+"&pwdReset=true";
		makePOST(url,params);
		//alert("Your password is successfully changed");
		
		
	}
}


function reqListener(){
	this.responseText
}

function processStateChange() {
	var js;
    if (httpReq.readyState==4){
		//showPageProgressMessage();
		resetMe();

   		if (httpReq.status==200){
			//js=refineJS(httpReq.responseText);	
			js=httpReq.responseText;						   			
			if(js.indexOf('../js/login/Login.js')!=-1){				
				js="aMessage['code']='ERROR';aMessage['desc']='';";		
			}									
					
			js=js.replace(/\r\n|\r|\n/g, '');					
	   		eval(js);								
			if(aMessage['code']=="ERROR"){
				disableButtons(false);
				showPageERRMessage(aMessage['desc'],"pwdOld");
			}else{
				var url=request['contextPath']+'/public/logout.action';
				showPageConfMessage(aMessage['desc'],"");
				alert(raiseError('XBE-ERR-35'));
				window.top.location.href ="../private/showXBEMain.action"
					
			}
		}else{
			disableButtons(false);
       		showPageERRMessage(raiseError('XBE-ERR-33'),"pwdOld");
     	}
    }
}


function validate(){
	var blnStatus = true;
	var val="";
	
	val=trim(getValue("pwdOld"));
	setField("pwdOld",val);

	if (val==""){
		showPageERRMessage(raiseError("XBE-ERR-01","Old Password"));
		getFieldByID("pwdOld").focus();
		return false;
	}

	val=trim(getValue("pwdNew"));
	setField("pwdNew",val);

	if (val==""){
		showPageERRMessage(raiseError("XBE-ERR-01","New Password"));
		getFieldByID("pwdNew").focus();
		return false;
	}

	if (val.length<8){
		showPageERRMessage(raiseError("XBE-ERR-13","New Password", "8"));
		getFieldByID("pwdNew").focus();
		return false;
	}
		
	var  strChar = "";
	var numCount = 0;
	for(var i= 0;i < val.length; i++) {
		strChar = val.substr(i,1); 
		if(isPositiveInt(strChar)){
			numCount++;
		}	
	}
	if((numCount < 3) || (numCount > (val.length - 3))){
		showPageERRMessage(raiseError("XBE-ERR-50"));
		getFieldByID("pwdNew").focus();
		return false;
	} 
	

	val=getValue("pwdConfirm");
	setField("pwdConfirm",val);

	if (val==""){
		showPageERRMessage(raiseError("XBE-ERR-01","Confirm Password"));
		getFieldByID("pwdConfirm").focus();
		return false;
	}

	if (val.length<8){
		showPageERRMessage(raiseError("XBE-ERR-13","Confirm Password", "6"));
		setField("pwdConfirm",val);
		getFieldByID("pwdConfirm").focus();
		return false;
	}

	//By Charith - avoid more than 12 characters for the password
	if (val.length>12){
		showPageERRMessage(raiseError("XBE-ERR-70","Confirm Password", "12"));
		setField("pwdConfirm",val);
		getFieldByID("pwdConfirm").focus();
		return false;
	}
	
	if (getValue("pwdOld")==getValue("pwdNew")){
		showPageERRMessage(raiseError("XBE-ERR-02","Old Password","New Password"));
		getFieldByID("pwdNew").focus();
		ShowPageMessage();
		return false;
	}

	if (getValue("pwdNew")!=getValue("pwdConfirm")){
		showPageERRMessage(raiseError("XBE-ERR-14","New Password","Confirm Password"));
		getFieldByID("pwdNew").focus();
		ShowPageMessage();
		return false;
	}

	return true;
}

function disableButtons(bln){
	Disable('btnSave',bln);
	Disable('btnReset',bln);
}

function pageOnChange(){
	resetPageERRMessage();
	top.pageEdited=true;
}

function resetMe(){
	setField("txtUserId",request['userId']);
	setField("pwdOld","");
	setField("pwdNew","");
	setField("pwdConfirm","");
	
	$("#pwdNew_bar").remove();
	$("#pwdNew_text").remove();
	$("#pwdNew_minchar").remove();
	
	$(function() {
		$('.password').pstrength();
	}); 
	resetPageERRMessage();
	//top.pageEdited=false;	
}

function showPageError(msg,fld){ 
	showPageERRMessage(msg); 
	if(fld&&fld!="")getFieldByID(fld).focus();  
}

function showPageConfirmation(msg,fld){
	showConfirmation(msg);
	if(fld&&fld!="")getFieldByID(fld).focus();
}

function showPageConfMessage(strMsg){
	resetPageERRMessage();
	DivWrite("spnError", "<font class='fntBold'><img src='../images/Conf_no_cache.gif'> " + strMsg + '<\/font>');
}
	
function showPageERRMessage(strMsg){
	resetPageERRMessage();
	DivWrite("spnError", "<font class='mandatory fntBold'><img src='../images/Err_no_cache.gif'> " + strMsg + '<\/font>');
}
	
function showPageProgressMessage(strMsg){
	resetPageERRMessage();
	DivWrite("spnError", "<font class='fntBold'><blink>Please wait..... <br>The server is processing.....<\/blink><\/font>");
}

function resetPageERRMessage(){
	DivWrite("spnError", "");
}

function raiseError(strErrNo){
	var strMsg = arrError[strErrNo];
	if (arguments.length >1){
		for (var i = 0 ; i < arguments.length - 1 ; i++){
			strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
		}
	}
	return strMsg;
}

$(function() {
	$('.password').pstrength();
});


function closeClick() {
		 window.close(); 
}
	
function isReseted(){
	var labelMsg = '';
	setVisible("pageInfo",false);	
	if (isPasswordExpired == 'true') {
		labelMsg = arrError['TAIR-90180'];
	} else {
		labelMsg = arrError['TAIR-90179'];
	}
	if(isReset == 'true' || isPasswordExpired == 'true'){
		setVisible('pageInfo',true);
		getFieldByID('lblInfoMgs').innerHTML = labelMsg;
	}
	
}	

$.fn.seti18nDataMsg = function() {
  
  $(this).find("label, span, div, a, font, button, td, option, p, b, input").each(function( intIndex ){
    var messageId = $(this).attr("i18n_key");
    
    if(messageId){
      var i18Message = geti18nMsg(messageId);
      
      if(i18Message != null){
        if ($(this).attr("type") == "button" && $(this).attr("name") != null){ 
          $(this).val(i18Message);
        }else{
          $(this).text(i18Message);
        }
        
      }
      
    }

  });
}

var geti18nMsg = function(key){
  try {
    return languageDataObj[key];
  } catch (e) {
    console.log(e);
  }
  return null;
}
