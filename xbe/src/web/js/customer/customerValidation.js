function UI_customerValidation(){}
var jsonCustomerObj = [];
var jsonAirewardsObj = [];
var phoneObj = top.arrCountryPhoneJson;

UI_customerValidation.lmsValidationData = null;
UI_customerValidation.accountExists = false;
UI_customerValidation.lmsNameMismatch = false;
UI_customerValidation.lmsJoinNotAllowed = false;

$( document ).ready(function(){
	
	jsonCustomerObj = [{
		id : "#editEmailAddress",
		desc : "Email Address",
		required : true 
	}, {
		id : "#displayTitle",
		desc : "Title",
		required : true
	}, {
		id : "#firstName",
		desc : "First Name",
		required : true
	}, {
		id : "#lastName",
		desc : "Last Name",
		required : true
	}, {
		id : "#nationality",
		desc : "Nationaliy Of The Customer",
		required : true
	}, {
		id : "#residence",
		desc : "Residence Of The Customer",
		required : true
	}, {
		id : "#txtMobiCntry",
		desc : "Mobile Country Code",
		required : true
	}, {
		id : "#txtMobiNo",
		desc : "Mobile No",
		required : true
	}];
	
	
	jsonAirewardsObj = [{
		id : "#birthDate",
		desc : "Birth Day",
		required : true 
	}, {
		id : "#selPrefLang",
		desc : "Communication Language",
		required : true
	}];
	
});

UI_customerValidation.onlyNumeric = function(objC){
	
	if (!validateInteger(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
	
}

UI_customerValidation.setCountryPhone = function(){
	
	$("#txtMobiCntry").val("");
	$("#txtFaxCntry").val("");
	$("#txtTravCntry").val("");
	
	var choseCountry = $("#residence").val();
	
	for ( var cl = 0; cl < phoneObj.length; cl++) {
		counObj = phoneObj[cl];
		if (counObj.countryCode == choseCountry) {
			$("#txtMobiCntry").val(counObj.countryPhoneCode);
			$("#txtFaxCntry").val(counObj.countryPhoneCode);	
			$("#txtTravCntry").val(counObj.countryPhoneCode);
			break;
		}
	}
	
}

UI_customerValidation.onlyAlphaWhiteSpace = function(objC){
	
	if (!isAlphaWhiteSpace(objC.value)){
		var strValue = objC.value 
		objC.value = strValue.substr(0, strValue.length -1);
		objC.value = replaceall(objC.value, "-", "");
	}
	
}

UI_customerValidation.validate = function(){
	
	/* Mandatory Validations */
	if (!UI_commonSystem.validateMandatory(jsonCustomerObj)) {
		return false;
	}
	
	if($("#joinAirewards").is(":checked")){
	
		var toDate = new Date();
		
		if (!UI_commonSystem.validateMandatory(jsonAirewardsObj)) {
			return false;
		}
		
		if(!dateValidDate($("#birthDate").val())){
			
			showERRMessage(arrError.dobInvalid);
			$("#birthDate").focus();
			return false;
			
		}
		
		if(ageCalculate($("#birthDate").val()) < 2){
			
			showERRMessage(raiseError("XBE-ERR-04", "Birthday"));
			$("#birthDate").focus();
			return false;
			
		}else if(ageCalculate($("#birthDate").val()) < 12){
			
			if (!UI_commonSystem.validateMandatory([{
				id : "#familyHeadEmail",
				desc : "Family head email should be given for the customers below 12 years",
				required : true 
			}])) {
				$("#familyHeadEmail").focus();
				return false;
			}else{
				
				if ($.trim($("#familyHeadEmail").val()) != "") {
					if (!checkEmail($("#familyHeadEmail").val())) {
						showERRMessage(arrError.familyHeadInvalid);
						$("#familyHeadEmail").focus();
						return false;
					} 
				}
				
			}
			
		}
		
		if ($.trim($("#refferEmail").val()) != "") {
			if (!checkEmail($("#refferEmail").val())) {
				showERRMessage(arrError.refemailInvalid);
				$("#refferEmail").focus();
				return false;
			} 
		}
		
	}
	
	if(!UI_customerValidation.validatePhoneNumbers()){
		
		return false;
		
	}
	
	if ($.trim($("#editEmailAddress").val()) != "") {
		if (!checkEmail($("#editEmailAddress").val())) {
			showERRMessage(arrError.emailInvalid);
			$("#editEmailAddress").focus();
			return false;
		} 
	}
	
	var proceed = true;
	var lmsPreConditionValidationError = UI_customerValidation.lmsHeadRefferedValidate(true);
	if(UI_customerValidation.accountExists){
		if(UI_customerValidation.accountExists){
			showERRMessage(arrError.emailExists);
			return;
		}
		proceed = false;
	} else if(lmsPreConditionValidationError != ""){
		proceed = false;
	}
				
	if(UI_customerValidation.lmsValidationData != null){
		
		if(UI_customerValidation.lmsValidationData.headOFEmailId=="N"){
			showERRMessage(arrError.familyHeadDoesntExist);
			proceed = false;
		}
		if(UI_customerValidation.lmsValidationData.refferedEmail=="N"){
			showERRMessage(arrError.refEmailDoesntExist);
			proceed = false;
		}
		
	}
	
	if(!proceed){
		
		return false;
		
	}
	

	
	return true;
	
}

UI_customerValidation.validateCountryPhone = function(countryCode, phoneObj, phCountyCode, phNo, type){
	var hasNo = false;
	if(phoneObj.length > 0) {
		var counObj;
		for(var cl=0;cl< phoneObj.length;cl++){
			counObj = phoneObj[cl];
			if(counObj.countryPhoneCode == phCountyCode){
				var hasNo = true;
				if(phNo.length < Number(counObj.minLength)){
					showERRMessage(raiseError("XBE-ERR-03", type + " NO. " ,Number(counObj.minLength)));
					return false;
				}

				if(phNo.length > Number(counObj.maxlength)){
					showERRMessage(raiseError("XBE-ERR-05",type + " NO. " ,Number(counObj.maxlength)));
					return false;
				}
			
				break;
			} 
		}
		if(!hasNo) {
			showERRMessage(raiseError("XBE-ERR-61"));
			return false;				
		}
	}
	return true;
}

UI_customerValidation.lmsHeadRefferedValidate = function(isRegisterUser){
	
	var data = {};
	var url = "registerCustomer!validateHeadReffered.action";
	var errorMsg = "";
	data["lmsDetails.headOFEmailId"] = $("#familyHeadEmail").val();
	data["lmsDetails.refferedEmail"] = $("#refferEmail").val();
	data["lmsDetails.emailId"] = $("#editEmailAddress").val();
	data["lmsDetails.firstName"] = $("#firstName").val();
	data["lmsDetails.lastName"] = $("#lastName").val();
	data["registration"] = isRegisterUser;
	data["skipNameMatching"] = false;
	if($("#joinAirewards").is(":checked")){
		
		data['optLMS'] = 'Y';
		
	}else{

		data['optLMS'] = 'N';
		
	}
	var response = $.ajax({
		type: "POST", 
		dataType: 'json', 
		beforeSend :ShowPopProgress(),
		data:data,url:url,
		async:false,
		success: function(response){
			UI_customerValidation.lmsValidationData = response.lmsDetails;
			UI_customerValidation.accountExists = response.accountExists;
			UI_customerValidation.lmsNameMismatch = response.lmsNameMismatch;
			UI_customerValidation.lmsJoinNotAllowed = response.lmsJoinNotAllowed;
			HidePopProgress();
		},
		error: function(response){
			UI_customerValidation.lmsValidationData = null;
			HidePopProgress();
		}
	});
	
	return errorMsg;
}

UI_customerValidation.validateOnUpdateCustomer = function(isRegisterUser){
	
	if (!UI_commonSystem.validateMandatory(jsonCustomerObj)) {
		return false;
	}
	
	if($("#joinAirewards").is(":checked")){
		
		if (!UI_commonSystem.validateMandatory(jsonAirewardsObj)) {
			return false;
		}
		
		if(!dateValidDate($("#birthDate").val())){
			
			showERRMessage(arrError.dobInvalid);
			$("#birthDate").focus();
			return false;
			
		}
		
		if(ageCalculate($("#birthDate").val()) < 12){
			
			if (!UI_commonSystem.validateMandatory([{
				id : "#familyHeadEmail",
				desc : "Family head email should be given for the customers below 12 years",
				required : true 
			}])) {
				$("#familyHeadEmail").focus();
				return false;
			}else{
				
				if ($.trim($("#familyHeadEmail").val()) != "") {
					if (!checkEmail($("#familyHeadEmail").val())) {
						showERRMessage(arrError.familyHeadInvalid);
						$("#familyHeadEmail").focus();
						return false;
					} 
				}
				
			}
			
		}
		
		if ($.trim($("#refferEmail").val()) != "") {
			if (!checkEmail($("#refferEmail").val())) {
				showERRMessage(arrError.refemailInvalid);
				$("#refferEmail").focus();
				return false;
			} 
		}
		
	}
	
	
	if(!UI_customerValidation.validatePhoneNumbers()){
		
		return false;
		
	}
	
	var proceed = true;
	var lmsPreConditionValidationError = UI_customerValidation.lmsHeadRefferedValidate(isRegisterUser);
		
	if(UI_customerValidation.lmsValidationData != null){
		
		if(UI_customerValidation.lmsValidationData.headOFEmailId=="N"){
			showERRMessage(arrError.familyHeadDoesntExist);
			proceed = false;
		}
		if(UI_customerValidation.lmsValidationData.refferedEmail=="N"){
			showERRMessage(arrError.refEmailDoesntExist);
			proceed = false;
		}
		
	}
	
	if(UI_customerValidation.lmsJoinNotAllowed){
		
		showERRMessage(arrError.lmsJoinNotAllowed);
		proceed = false;
		
	}
	
	if(!proceed){
		
		return false;
		
	}
	

	
	return true;
	
	
}

UI_customerValidation.validatePhoneNumbers = function(){
	
	if ($.trim($("#txtMobiNo").val()) != "") {

		$("#txtMobiCntry").val($.trim($("#txtMobiCntry").val()));
		$("#txtMobiNo").val($.trim($("#txtMobiNo").val()));

		if (!UI_commonSystem.numberValidate( {
			id : "#txtMobiCntry",
			desc : "Mobile Country Code",
			minValue : 1
		})) {
			return false;
		}

		if (!UI_commonSystem.numberValidateWithStartingZero( {
			id : "#txtMobiNo",
			desc : "Mobile No",
			minValue : 1
		})) {
			return false;
		}
		if (!UI_customerValidation.validateCountryPhone($("#residence").val(),
				phoneObj, $("#txtMobiCntry").val(), $(
						"#txtMobiNo").val(), "MOBILE")) {
			return false;
			
		}

	} else {

		$("#txtMobiCntry").val("");
		
	}
	
	if ($.trim($("#txtFaxNo").val()) != "") {

		$("#txtFaxCntry").val($.trim($("#txtFaxCntry").val()));
		$("#txtFaxNo").val($.trim($("#txtFaxNo").val()));

		if (!UI_commonSystem.numberValidate( {
			id : "#txtFaxCntry",
			desc : "Fax Country Code",
			minValue : 1
		})) {
			return false;
		}

		if (!UI_commonSystem.numberValidateWithStartingZero( {
			id : "#txtFaxNo",
			desc : "Fax No",
			minValue : 1
		})) {
			return false;
		}
		
	} else {
		
		$("#txtFaxCntry").val("");

	}
	
	if ($.trim($("#txtTravNo").val()) != "") {

		$("#txtTravCntry").val($.trim($("#txtTravCntry").val()));
		$("#txtTravNo").val($.trim($("#txtTravNo").val()));

		if (!UI_commonSystem.numberValidate( {
			id : "#txtTravCntry",
			desc : "Number during travel Country Code",
			minValue : 1
		})) {
			return false;
		}

		if (!UI_commonSystem.numberValidateWithStartingZero( {
			id : "#txtTravNo",
			desc : "Number during travel No",
			minValue : 1
		})) {
			return false;
		}
		
	} else {
		
		$("#txtTravCntry").val("");

	}
	
	return true;
	
}