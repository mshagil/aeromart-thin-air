var flightNoONDCodeMap = null
jQuery(document).ready(function(){
	var data = {};
	enableDisableControls(true);
	$(function() {
	    $( "#txtFlightDate" ).datepicker({
	      showOn: "button",
	      buttonImage: "../images/Calendar_no_cache.gif",
	      minDate: "0",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      changeMonth: true,
	      changeYear: true
	    });
	    
	    $( "#txtFlightDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
	    
	 });
	$("#trMco").hide();
	temGrid=$("#listMCO").jqGrid({
		datatype: function(postdata) {
	        $.ajax({
		           url: 'manageMCO!searchMCO.action',
		           data: $.extend(postdata, null),
		           dataType:"json",
		           complete: function(jsonData,stat){	        	  
		              if(stat=="success") {
		            	  mydata = eval("("+jsonData.responseText+")");
		            	  if (mydata.msgType != "Error"){
			            	 $.data(temGrid[0], "gridData", mydata.rows);
			            	 temGrid[0].addJSONData(mydata);
		            	  }else{
		            		  alert(mydata.message)
		            	  }
		              }
		           }
		        });
		    },
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},
		colNames:['Issue Date','Pnr', 'Pax Name','MCO Number','MCO Service','Status', 'Flight No','Flight Date', 'Amount', 'Currency', 'Remarks', 'Email', 'mcoServiceId'], 
		colModel:[ 	{name:'issueDate', width:75, jsonmap:'issueDate'},  
				   	{name:'pnr', width:50,  jsonmap:'pnr'},
					{name:'paxName', width:125, align:"center", jsonmap:'paxName'},
				   	{name:'mcoNumber', width:90, align:"center", jsonmap:'mcoNumber'},
				   	{name:'mcoService', width:100, align:"center", jsonmap:'mcoService'},
				   	{name:'status', width:75, align:"center", jsonmap:'status'},
				   	{name:'flightNo', width:75, align:"center", jsonmap:'flightNo'},
				   	{name:'flightDate', width:75, align:"center", jsonmap:'flightDate'},
					{name:'amount', width:60, align:"center", jsonmap:'amount'},
					{name:'currency', width:60, align:"center", jsonmap:'currency'},
					{name:'remarks', width:75, align:"center", jsonmap:'remarks'},
					{name:'email', width:125, align:"center", jsonmap:'email'},
					{name:'mcoServiceId', width:1, align:"center", hidden: true, jsonmap:'mcoServiceId'}
				   	],
		imgpath: '../../themes/default/images/jquery', multiselect:false,
		pager: jQuery('#temppager'),
		rowNum:20,						
		viewrecords: true,
		height:175,
		width:975,
		loadui:'block',
		onSelectRow: function(rowid){
			selectedRowId = rowid;
			$("#rowNo").val(rowid);
			var statusVal = $('#listMCO').jqGrid ('getCell', selectedRowId, 'status');
			$('#mcoNumber').val(statusVal);
			fillMCO(); 
			var rowData = jQuery("#listMCO").getRowData(rowid);
			if (rowData["status"] == "V") {
				disableMCOOperation(true);
			} else {
				disableMCOOperation(false);
			}
			disableMCO();
		}
	}).navGrid("#temppager",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$("#btnSearch").click(function(){
		submitMCOSearch();
	});	
	
	$("#frmSearchMCO").on('submit', function(){
		submitMCOSearch();
	});
	
	$("#frmSearchMCO").keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			submitMCOSearch();
			return false;
	    }
	});
	
	$("#btnIssue").click(function(){
		enableDisableControls(false);
		disableMCOOperation(true);
		resetOnClick();
		$("#trMco").hide();
		enableMCO();
	});
	
	$("#btnLoadPassengers").click(function(){
		var pnr = $("#txtPnr").val();
		if(pnr == "") {
			showCommonError("Error", "Please Enter PNR");
			$('#txtPnr').focus();
			return false;
		} else {
			$("#selPaxNames option").remove();
			$("#frmManageMCO").ajaxSubmit({
				url : 'manageMCO!loadPassengers.action',
				dataType: 'json', 
				success: displayData
			});			
		}
	});
	
	$("#btnVoid").click(function(){		
		var rowData = jQuery("#listMCO").getRowData(selectedRowId);
		var mcoStatus = rowData['status'];
		if(mcoStatus == "V"){
			showCommonError("Error", "MCO is already VOID");
			return false;
		}
		data['mcoNumber'] = $('#txtMCO').val();
		$("#frmManageMCO").ajaxSubmit({
			url : 'manageMCO!voidMCO.action',
			dataType: 'json', 
			data : data,
			success: showVOIDResponse
		});
	});
	
	$("#btnClose").click(function(){
		top.LoadHome();
	});

	$("#btnPay").click(function(){
		if(validate()){
			data["mco.ond"] = flightNoONDCodeMap[$("#txtFlightNo").val().toUpperCase()];
			$("#frmManageMCO").ajaxSubmit({
				url : 'manageMCO.action',
				data : data,
				dataType: 'json', 
				success: showResponse,
				error : showResponse
			});			
		}
	});
	
	$("#btnReset").click(function(){
		resetOnClick();
	});

	$("#btnPrint").click(function() { 
		var mcoNum = $("#txtMcoNumber").val();
		if (mcoNum == null || mcoNum == "") {
			var rowData = jQuery("#listMCO").getRowData(selectedRowId);
			mcoNum = rowData['mcoNumber'];
		}

		var strUrl = "printMCO.action?mcoNumber="+mcoNum+"";		
		top.objCW = window.open(strUrl, "myWindow", $("#popWindow").getPopWindowProp(630, 900, true));
		if (top.objCW) {
			top.objCW.focus()
		}
	});
	
	$("#txtAmount").keyup(function () { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});

	function submitMCOSearch(){

		if($('#txtMCO').val() != "") {
			var data = {};
			data['mcoNumber'] = $('#txtMCO').val();
			$("#frmSearchMCO").ajaxSubmit({
				url : 'manageMCO!searchMCO.action',
				dataType: 'json', 
				data : data,
				success: function(response){
					$("#listMCO")[0].addJSONData(response);
					showCommonError(response['msgType'], response['succesMsg']);
				}
			});
		} else {
			showCommonError("Error", "Please Enter MCO Number");
			$('#txtMCO').focus();
			clearMCO();
			return false;
		}
	
	}
	
	function resetOnClick(){
		$('#frmManageMCO')[0].reset();
	}
	
	function validate(){
		var emailText = $('#txtEmail').val();
		var flightNo = $('#txtFlightNo').val();
		var departureDate = $('#txtFlightDate').val();
		var amount = $('#txtAmount').val();
		
		if(emailText != "" && !checkEmail(emailText)){
			showCommonError("Error", "Invalid Email Address");
			$('#txtEmail').focus();
			return false;
		} else if(flightNo == "") {
			showCommonError("Error", "Please Enter Flight Number");
			$('#txtFlightNo').focus();
			return false;
		} else if(departureDate == "") {
			showCommonError("Error", "Please Enter Departure Date");
			$('#txtFlightDate').focus();
			return false;
		} else if(amount == "") {
			showCommonError("Error", "Please Enter Pay Amount");
			$('#txtAmount').focus();
			return false;
		} else if($('#selPaxNames').val() == null) {
			showCommonError("Error", "Please Load Passengers");
			$('#btnLoadPassengers').focus();
			return false;
		}
		return true;
	}
	
	function displayData(response){
		if(response['paxIds'] == null){
			showCommonError("Error" , response['succesMsg']);
			resetOnClick();
		} else { 
			flightNoONDCodeMap = response.flightNoONDCodeMap;
			$("#selPaxNames").append(response['paxIds']);
		}		
	}
	
	function showResponse(response, statusText) { 
		if(response.success){
			showCommonError("SUCCESS", "Payment Success");			
			enableDisableControls(true);
			disableMCOOperation(false);
			$("#trMco").show();
			$("#txtMcoNumber").val(response['mcoNumber']).prop("disabled", true);
		} else {
			showCommonError("ERROR",  response['succesMsg']);
			resetOnClick();
		}
	}
	
	function disableMCOOperation(disable) {
		$("#btnPrint").prop("disabled", disable);
		$("#btnVoid").prop("disabled", disable);	
	}
	
	
	function showVOIDResponse(response) { 
		if(response['msgType'] != "error"){
			showCommonError("SUCCESS", "MCO status changed to VOID");			
			enableDisableControls(true);
			disableMCOOperation(true);
			$("#trMco").show();
			$("#txtMcoNumber").val(response['mcoNumber']).prop("disabled", true);
			jQuery("#listMCO").trigger("reloadGrid");
		} else {
			showCommonError("ERROR",  response['succesMsg']);
			resetOnClick();
		}
	}
	
	function enableDisableControls(status){
		$("#frmManageMCO input").prop("disabled", status);
		$("#btnLoadPassengers").prop("disabled", status);
		$("#btnPay").prop("disabled", status);
		$("#selPaxNames option").remove();
		$("#btnClose").prop("disabled", false);
	}

	function checkValidAmount(fieldId) {
		var checkIntparam = trim($("#" + fieldId).val());
		if (!isPositiveInt(checkIntparam) || (parseInt(checkIntparam) < 0)) {
			return false;
		}
		return true;
	}
	
	function fillMCO(){
		clearMCO();
		var rowData = jQuery("#listMCO").getRowData(selectedRowId);
		$('#txtPnr').val(rowData['pnr']);
		$('#txtFlightNo').val(rowData['flightNo']);
		$('#txtFlightDate').val(rowData['flightDate']);
		$('#txtRemarks').val(rowData['remarks']);
		$('#txtAmount').val(rowData['amount']);
		$('#txtEmail').val(rowData['email']);
		$('#txtMcoNumber').val(rowData['mcoNumber']);

		
		setValueToSelect('#selPaxNames', rowData['paxName']);
		var element = document.getElementById('selMCOService');
	    element.value = rowData['mcoServiceId'];
	}
	
	function clearMCO(){
		$('#txtPnr').val('');
		$('#txtFlightNo').val('');
		$('#txtFlightDate').val('');
		$('#txtRemarks').val('');
		$('#txtAmount').val('');
		$('#txtEmail').val('');
	}
	
	function disableMCO(){
		document.getElementById('txtPnr').disabled = true;
		document.getElementById('txtFlightNo').disabled = true;
		document.getElementById('txtFlightDate').disabled = true;
		document.getElementById('txtRemarks').disabled = true;
		document.getElementById('txtAmount').disabled = true;
		document.getElementById('txtEmail').disabled = true;
		document.getElementById('selPaxNames').disabled = true;
		document.getElementById('selMCOService').disabled = true;
		document.getElementById('selCurrency').disabled = true;
	}
	
	function enableMCO(){
		document.getElementById('txtPnr').disabled = false;
		document.getElementById('txtFlightNo').disabled = false;
		document.getElementById('txtFlightDate').disabled = false;
		document.getElementById('txtRemarks').disabled = false;
		document.getElementById('txtAmount').disabled = false;
		document.getElementById('txtEmail').disabled = false;
		document.getElementById('selPaxNames').disabled = false;
		document.getElementById('selMCOService').disabled = false;
		document.getElementById('selCurrency').disabled = false;
	}
	
	function setValueToSelect(selID, value){
		$(selID)
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="'+value+'">'+value+'</option>')
	    .val(value)
	;
	}
	
});