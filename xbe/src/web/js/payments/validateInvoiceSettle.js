//Javascript Document
//Author - Shakir

var strTabIDs;
var strRowData;
var setData;
var blnLoaded = false;

function invoiceLoad(strMsg,strMsgType) {

	objOnFocus();
	if (strMsg != null && strMsgType == "Error") {
			showWindowCommonError(strMsgType, strMsg);
	}
	if(parent.intStat==1){
		DivWrite("spnAgentId",parent.strRowData[1]);
	}else {
		DivWrite("spnAgentId",parent.parnagentName);
	}
	setPageEdited(false);
	Disable("btnReset","false");	
	if (arrInvoice.length > 0){
		parent.frames["frmTabs"].arrInvoice = arrInvoice;
		objDG.arrGridData = arrInvoice;
		objDG.refresh();
	}
	
	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");				
		}
	}

	blnLoaded =  true;
	
	  if (parent.falgGSA) { //AARESAA-1830
     setVisible("btnSave", false);	
    }
	

}

function showGrid(){
	try{
		objDG.arrGridData = parent.frames["frmTabs"].arrInvoice;
		objDG.refresh();
	}catch(Exception){}
}

function validateDecimal(objCon){
	setPageEdited(true);
	objOnFocus();
	var strLen = objCon.value.length;
	var strText = objCon.value;
	var blnVal = currencyValidate(strText, 10,5);
	if(!blnVal){
		objCon.value =  strText.substr(0,strLen-1); 
		objCon.focus();
	}
}

function validateDecimalAmount(objCon){
	setPageEdited(true);
	objOnFocus();
    if (parent.falgGSA) { //AARESAA-1830
     Disable("btnSave", true);	
    }
	var strLen = objCon.value.length;
	var strText = objCon.value;
	var blnVal = anyCurrencyValidate(strText, 15,2);
	if(!blnVal){
		objCon.value =  strText.substr(0,strLen-1); 
		objCon.focus();
	}
}

function txtChange(){
	Disable("btnReset","")
}

function saveClick(){
	setPageEdited(true);
	var fltCell;
	var fltTemp;
	var fltTot="0.0";
	var advTot="0.0";
	var invTot="0.0";
	var blnErr=true;
	setData="";

	var avlblForInv = parseFloat(parent.availableForInvSet);
//	var avlblAsAdv = parseFloat(parent.availableAsAdv);

	var objTemp = frames["frmTabs"]; 
	var strLen = objDG.arrGridData.length;
	var countZeroFound = 0;
	for(var i=0 ; i<strLen ; i++){
		strRowData = objDG.getRowValue(i);
		if (parseFloat(strRowData[5]) == 0) {
			countZeroFound++;		
		} else {		
			if (parseFloat(strRowData[5]) < 0) {
				showCommonError("Error", parent.arrError["ISAmountNegative"]);
				blnErr=false;
				break;
			}else {
				if (parseFloat(strRowData[5]) > parseFloat(strRowData[2])) {
					showCommonError("Error", parent.arrError["AmountExceedTotal"]);
					blnErr=false;
					break;
				}else {
					if (isNaN(parseFloat(strRowData[5])) || strRowData[5]=="") {
						if (strRowData[5]=="") {
							showCommonError("Error", parent.arrError["ISAmountNull"]);
							blnErr=false;
							break;
						}else{
							showCommonError("Error", parent.arrError["AmountNotValid"]);
							blnErr=false;
							break;
						}
						blnErr=false;
						break;
					}else {
						if(setData=="") {
							setData = strRowData + "||";
						}else{
							setData = setData + strRowData + "||";
						}
					}
				}
			}
		}
	}
	if (countZeroFound == strLen) {
		showCommonError("Error", parent.arrError["InvoiceAmountZero"]);	
		blnErr=false;
	}
		
	if (blnErr) {
		var strLen = objDG.arrGridData.length;		
		for(var i=0 ; i<strLen; i++){
			fltCell = objDG.getCellValue(i,5);
			advTot = parseFloat(advTot) + parseFloat(fltCell);
			invTot = parseFloat(invTot) + parseFloat(fltCell);			
		}
		if (parseFloat(avlblForInv) < parseFloat(invTot)){
			showCommonError("Error",parent.arrError["ISAmountExceed"]);
			blnErr=false;
		}		
	}
	
	
	if (blnErr)	{
		setField("hdnData", setData);
		if(parent.intStat==1){
			setField("hdnAgentCode", parent.strRowData[0]);
			setField("hdnAgentName", parent.strRowData[1]);
			parent.setField("hdnAgentCode", parent.strRowData[0]);
			parent.setField("hdnAgentName", parent.strRowData[1]);
		}else {
			setField("hdnAgentCode", parent.parnagentCode);
			setField("hdnAgentName", parent.parnagentName);
			parent.setField("hdnAgentCode", parent.parnagentCode);
			parent.setField("hdnAgentName", parent.parnagentName);
		}		
		setField("hdnMode", "SAVE");
		parent.setField("hdnData", setData);		
		parent.setField("hdnMode", "SAVE");		
		parent.saveInvSettle();		
	}
}

function resetClick(){
	for (var intCount=0 ; intCount < objDG.arrGridData.length ; intCount++)	{
		objDG.setCellValue(intCount, 5, "00.00", "");		
	}
}

function objOnFocus(){
	top[2].HidePageMessage();
}

function setPageEdited(isEdited){
	top.pageEdited = isEdited;
	if(isEdited){
		Disable("btnSave", "");	
		Disable("btnReset","");
	}else{
		Disable("btnSave", "false");
		Disable("btnReset","false");
	}
}

function closeClick(){
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		//top[0].manifestSearchValue = "";
		top[1].objTMenu.tabSetValue("SC_FACM_001", "");
		top[1].objTMenu.tabRemove("SC_FACM_001");
	}
}