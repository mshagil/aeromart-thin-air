	//var recNo = top[0].intLastRec;
	
	var tabValue =top[1].objTMenu.tabGetValue("SC_FACM_001"); // top[0].manifestSearchValue;
	var recNo =1;	
	if ( tabValue != null && tabValue != "" ) {
		recNo = tabValue.split(",")[0];
		}
	
	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "8%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "" ;
	objCol1.headerText = "Agent Id";
	objCol1.itemAlign = "left"
	objCol1.sort = "true";
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "18%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "" ;
	objCol2.headerText = "Agent Name";
	objCol2.itemAlign = "left"
		
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "12%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "" ;
	objCol3.headerText = "Credit Limit " + strBaseCurrency;
	objCol3.itemAlign = "right"
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "12%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "" ;
	objCol4.headerText = "Available Credit";
	objCol4.itemAlign = "right"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "12%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "" ;
	objCol5.headerText = "Amount Due";
	objCol5.itemAlign = "right"
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "9%";
	objCol6.arrayIndex = 6;
	objCol6.toolTip = "" ;
	objCol6.headerText = "Available For<br> Invoice Settlement";
	objCol6.itemAlign = "right"
	
	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "9%";
	objCol7.arrayIndex = 7;
	objCol7.toolTip = "" ;
	objCol7.headerText = "Available For<br> Advance Payment";
	objCol7.itemAlign = "right"
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "15%";
	objCol8.arrayIndex = 9;
	objCol8.toolTip = "" ;
	objCol8.headerText = "Credit Limit Specified";
	objCol8.itemAlign = "right"
	
	var objCol9 = new DGColumn();
	objCol9.columnType = "label";
	objCol9.width = "10%";
	objCol9.arrayIndex = 10;
	objCol9.toolTip = "" ;
	objCol9.headerText = "Agent IATA <br> Code";
	objCol9.itemAlign = "right"
		
	var objCol10 = new DGColumn();
	objCol10.columnType = "label";
	objCol10.width = "12%";
	objCol10.arrayIndex = 11;
	objCol10.toolTip = "" ;
	objCol10.headerText = "BSP Available <br> Credit";
	objCol10.itemAlign = "right"
	
	var objCol11 = new DGColumn();
	objCol9.columnType = "label";
	objCol9.width = "10%";
	objCol9.arrayIndex = 12;
	objCol9.toolTip = "" ;
	objCol9.headerText = "Amount Due by Reporting <br/> SGSAs/TAs/STAs";
	objCol9.itemAlign = "right"
	
	// ---------------- Grid	
	var objDG = new DataGrid("spnPayments");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
//	objDG.addColumn(objCol9);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol8);
	objDG.addColumn(objCol10);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol11);
	objDG.addColumn(objCol6);
//	objDG.addColumn(objCol7);
	objDG.width = "100%";
	objDG.height = "192px";
	objDG.headerBold = false;
	objDG.rowSelect = true;	
	objDG.arrGridData = arrData;
	objDG.seqNo = true;
	objDG.seqStartNo = recNo;
	objDG.pgnumRecTotal = totalRecords;
	objDG.paging = true
	objDG.pgnumRecPage = 10;
	objDG.pgonClick = "gridNavigations";	
	objDG.rowClick = "rowClick";
	var noDataFound=objDG.noDataFoundMsg;
	top[0].status=noDataFound;
	objDG.noDataFoundMsg="";
	objDG.displayGrid();
	
	function RecMsgFunction(intRecNo, intErrMsg){
		top[2].HidePageMessage();
		if (intErrMsg != ""){
			top[2].objMsg.MessageText = intErrMsg ;
			top[2].objMsg.MessageType = "Error" ; 
			top[2].ShowPageMessage();
		}
	}	
	
	
	
	