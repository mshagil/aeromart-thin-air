var screenId = "SC_ADMN_002";
var valueSeperator = "~";
var recstart = 1;

/*if (getTabValues(screenId, valueSeperator, "intLastRec")==""){
		setTabValues(screenId, valueSeperator, "intLastRec", 1);
	} */
if(trim(parent.initialrec) != "") {
	recstart = parent.initialrec;
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Login ID";
objCol1.itemAlign = "left"
objCol1.sort = "true"
	
var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "26%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Display Name";
objCol2.itemAlign = "left"
objCol2.sort = "true"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "19%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Full email Id" ;
objCol5.headerText = "Email ID";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "24%";
objCol6.arrayIndex = 6;
objCol6.headerText = "Travel Agent / GSA";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "7%";
objCol7.arrayIndex = 7;
objCol7.headerText = "Status";
objCol7.itemAlign = "left"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "7%";
objCol8.arrayIndex = 16;
objCol8.headerText = "Carrier Code";
objCol8.itemAlign = "left"

// ---------------- Grid	
var objDG = new DataGrid("spnUsers");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.width = "99%";
objDG.height = "125px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = parent.userData;
objDG.seqNo = true;
objDG.seqStartNo =   recstart //Grid
objDG.pgnumRecTotal = parent.totalNoOfRecords ; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "parent.gridNavigations";	
objDG.rowClick = "parent.rowClick";
objDG.displayGrid();

