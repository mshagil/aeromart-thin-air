if (typeof(arrSearchData) != "object")
	{
		var arrSearchData = new Array();
	} ;

	var arrSmsPnrList = new Array();
	var arrEmailPnrList = new Array();
	var arrSentNotices = new Array();
		
	var objTimer = null;
	setVisible("spnFlight", true);
	setVisible("spnFltSelectAll", true);
	
	var recstNo = top[0].intLastRec;

	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "7%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "Flight Number" ;
	objCol1.headerText = "Flight Number";
	objCol1.itemAlign = "left";
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "7%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "Flight Dates" ;
	objCol2.headerText = "Flight Dates";
	objCol2.itemAlign = "left";
	objCol2.sort = true;

	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "10%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "Route" ;
	objCol3.headerText = "Route";
	objCol3.itemAlign = "center";

	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "3%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "Adults";
	objCol4.headerText = "Ad";
	objCol4.itemAlign = "right";
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "3%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "Children";
	objCol5.headerText = "Ch";
	objCol5.itemAlign = "right";
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "3%";
	objCol6.arrayIndex = 6;
	objCol6.toolTip = "Infants";
	objCol6.headerText = "In";
	objCol6.itemAlign = "right";

	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "25%";
	objCol7.arrayIndex = 7;
	objCol7.toolTip = " Connection Details";
	objCol7.headerText = "Connection Details";
	objCol7.itemAlign = "left";

	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "25%";
	objCol8.arrayIndex = 8;
	objCol8.toolTip = "Alert Created Date";
	objCol8.headerText = "Alert Date";
	objCol8.itemAlign = "right";
	
	var objCol10 = new DGColumn();
	objCol10.columnType = "checkbox";
	objCol10.width = "3%";
	objCol10.toolTip = "Select";
	objCol10.headerText = "Select";
	objCol10.itemAlign = "center";
	
	var objCol11 = new DGColumn();
	objCol11.columnType = "label";
	objCol11.width = "5%";
	objCol11.arrayIndex = 9;
	objCol11.toolTip = "PNR count";
	objCol11.headerText = "PNR count";
	objCol11.itemAlign = "right";
		
	var objCol12 = new DGColumn();
	objCol12.columnType = "label";
	objCol12.width = "25%";
	objCol12.arrayIndex = 10;
	objCol12.toolTip = "Flight original time";
	objCol12.headerText = "Original Flight Time";
	objCol12.itemAlign = "right";
	
	var flightDG = new DataGrid("spnFlight");
	flightDG.addColumn(objCol10);
	flightDG.addColumn(objCol1); //flight numbers
	flightDG.addColumn(objCol2); //flight dates
	
	flightDG.addColumn(objCol3); //Route
	flightDG.addColumn(objCol4); //Number of Adults
	flightDG.addColumn(objCol5); //Number of Children
	flightDG.addColumn(objCol6); //Number of Infants
	flightDG.addColumn(objCol7); //InBound Connection Details
	flightDG.addColumn(objCol8); //Alert Created Date
	flightDG.addColumn(objCol11); //Total number of pnrs for the flight id
	flightDG.addColumn(objCol12); //Original Flight Time
	
	flightDG.width = "126%";
	flightDG.height = "120px";
	flightDG.headerBold = false;
	flightDG.arrGridData = arrSearchData;
	flightDG.seqNo = true;
	flightDG.seqStartNo = recstNo;
	//flightDG.pgnumRecTotal = totNoOfAlerts ;
	flightDG.paging = true;
	flightDG.pgnumRecPage = 20;
	flightDG.pgonClick = "gridNavigations"
	//alert("gridNavigation");
	//flightDG.rowClick = "parent.RowClick";
	//ShowProgress();
	flightDG.displayGrid();
	//HideProgress();
	
	function selectAllRecord() {
		var selectedPNRs = flightDG.getSelectedColumn("0");	
		if (getFieldByID("chkSelectAllRecord").checked) {
			for (var i = 0; i < selectedPNRs.length; i++) {
				flightDG.setCellValue(i, 0, "true");
			}
		} else {
			for (var i = 0; i < selectedPNRs.length; i++) {
				flightDG.setCellValue(i, 0, "false"); 	
			}			
		}
	}
	
	
