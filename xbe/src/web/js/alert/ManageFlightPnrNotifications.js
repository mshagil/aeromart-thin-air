	var selectedPnrArr = new Array();
	var selectedFlightData = new Array();
	var daysOfOperation = [];
	var intSelRow	= -1;
	var strRowData;
	var strGridRow;
	var intLastRec;
	var screenId = "SC_RESV_CC_027";
	var valueSeperator = "~";
	var totalRec;
	var defaultMsgLength = [
		{key:"title",value:3},
		{key:"firstName",value:20},
		{key:"pnr",value:9},
		{key:"flightNumber",value:5},
		{key:"origin",value:3},
		{key:"destination",value:3},
		{key:"departureDate",value:22}
	];
	var arrData = new Array();
	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.buildCalendar();

	
	function setDate(strDate, strID) {
		switch (strID) {
			case "1" : setField("txtNewDepDate", strDate); break;
			case "2" : setField("txtDepDateFrom", strDate); break;
			case "5" : setField("txtDepDateTo", strDate); break;
			case "3" : setField("txtAlertFromDate", strDate); break;
			case "4" : setField("txtAlertToDate", strDate); break;	
		}
	}
	
	function loadCalendar(strID, objEvent) {
		var displayCal = true;
		objCal1.ID = strID;
		if (strID == 1) {
			if (getFieldByID("txtNewDepDate").disabled) {
				displayCal = false;
			}
			objCal1.top = 250; 
			objCal1.left = 400;
		} else if (strID == 2) {
			if (getFieldByID("txtDepDateFrom").disabled) {
				displayCal = false;
			}
			objCal1.top = 40; 
			objCal1.left = 500;
		} else if(strID ==3){
			if (getFieldByID("txtAlertFromDate").disabled) {
				displayCal = false;
			}
			objCal1.top = 120; 
			objCal1.left = 500;
		} else if (strID ==4){
			if (getFieldByID("txtAlertToDate").disabled) {
				displayCal = false;
			}
			objCal1.top = 120; 
			objCal1.left = 630;
		} else if (strID == 5) {
			if (getFieldByID("txtDepDateTo").disabled) {
				displayCal = false;
			}
			objCal1.top = 40; 
			objCal1.left = 630;
		} 		
		if (displayCal) {	
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
	}
	
	function closeNotification(){
		top.strSearchCriteria = "";
		top.strFlightSearchCriteria = "";
		top.LoadHome();
	}
	
	function pageBtnClick(intID) {
		
		var objForm  = document.getElementById("frmNotify");
		objForm.target = "_self";
		objForm.action = "manageFlightPnrNotification.action";
		
		switch (intID){
			case 0 : 
				if (validate(0)){
					top.blnProcessCompleted=true;
					top.isFlight = true;
					setField("hdnAction", "SEARCH");
					setField("hdnSearchFlightCriteria", "");												
					setField("hdnSelectedPnr", "");	
					setField("hdnRecNo", 1);
					top.isPnr = true;
					buildFlightLoadCriteria();
					ShowProgress();
					objForm.submit();
				}
				break;
			case 1 :
					if (validate(1)) {
							setField("hdnAction", "GENERATE");
							setField("hdnMode", getValue("selNotifyPurpose"));
							setField("hdnRecNo", top[0].intLastRec);
							//setSelectedRows();
							setHiddenParameters(); // From SendFlightPnrNotificationGrid.js
							buildPnrLoadCriteria();
							selectedPnrArr = objDG.getSelectedColumn(5);
							var url="manageFlightPnrNotification.action";
							var params="hdnAction=GENERATE&hdnMode="+getValue("selNotifyPurpose")+"&txtFlightNo="+getValue("hdnFlightNo")+"&selNotifyChannel="+getValue("selNotifyChannel")
										+"&hdnSmsPnrSegList="+getValue("hdnSmsPnrSegList")+"&hdnEmailPnrSegList="+getValue("hdnEmailPnrSegList")
										+"&txtNewDepDate="+getValue("txtNewDepDate")+"&txtDepTime1="+getValue("txtDepTime1")+"&hdnDepDate="+getValue("hdnDepDate")
										+"&hdnSelectedPnr="+selectedPnrArr+"&selGenLanguage="+getValue("selGenLanguage");
							makePOST(url,params);
					}
				break;
			case 2 :
						if(getValue("selExistingCodes") == "-1") {
							var url="manageFlightPnrNotification.action";
							var params="hdnAction=EDITMSG";
							makePOST(url,params);
						}
							//Disable("txtMessage", false);

				break;
			case 3 :
				var cfrm = confirm("Notification will be sent to the selected passengers! Do you wish to Continue?");
				if (cfrm) {
					if (validate(3)) {
							selectedPnrArr = objDG.getSelectedColumn(5);
							setField("hdnSelectedPnr", selectedPnrArr);	
							setField("hdnMode", getValue("selNotifyPurpose"));			
							setField("hdnAction", "SEND");
							setField("hdnRecNo", top[0].intLastRec);
							setSelectedRows();
							setHiddenParameters(); // From SendFlightPnrNotificationGrid.js
							if(!validatePnrSelection()) {
								showERRMessage("No passengers selected for sending notification");
								return false;
							}
							buildPnrLoadCriteria();
							if(getValue("txtMessage") != null && getValue("txtMessage") != ""){
								var message = replaceEnter(getValue("txtMessage"));
								setField("hdnNtMessage", message);
								var strNotify = getValue("selNotifyChannel");
								if((getMessageDefaultCount(message) > 160) && (strNotify == "1" || strNotify == "3")){
									var smsConfirm = confirm("Message length is greater than 160 characters! Do you wish to send as multiple SMS?");
									setField("hdnOverrideMsgLength",smsConfirm)
								}
							}
							buildFieldInfo();
							objForm.submit();
					}
				}
				break;
			case 4 :
				if (validate(3)) {
						selectedPnrArr = objDG.getSelectedColumn(5);
						setField("hdnSelectedPnr", selectedPnrArr);
						setField("hdnAction", "VIEWMSG");
						setField("hdnMode", getValue("selNotifyPurpose"));
						setField("hdnRecNo", top[0].intLastRec);
						arrEmailPnrList = new Array();
						setSelectedRows();
						setHiddenParameters(); // From SendFlightPnrNotificationGrid.js
						buildPnrLoadCriteria();
						
						if(getValue("txtMessage") != null && getValue("txtMessage") != ""){
							setField("hdnNtMessage", getValue("txtMessage"));
						}							
						
						var intWidth = 500;
						var intHeight = 250; 
						var	strProp = 'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,width=' + intWidth + ',height=' 
						+ intHeight + ',resizable=yes,top=' + ((window.screen.height - intHeight) / 2) + ',left=' 
						+ (window.screen.width - intWidth) / 2;
						
						top[0].objWindow = window.open("about:blank","CWindow",strProp);
						var objForm1  = document.getElementById("frmNotify");
						objForm1.target = "CWindow";
						objForm1.action = "showFlightPnrNotificationMessage.action";
						objForm1.submit();
						
				}
				break;
			case 5 : 
						top.isFlight = false;
	                	setField("hdnAction", "LOAD");
						setField("hdnLoadPnrCriteria", "");						
						setField("hdnSelectedPnr", "");	
						setField("hdnRecNo", 1);
						setSearchRecNo(1);
						buildPnrLoadCriteria();
						setField("hdnPnrLoad","true");
						if(getValue("hdnLoadPnrCriteria")==""){
							showERRMessage("Please select a flight record");
						}else{
							var url="manageFlightPnrNotification.action";
							var params="hdnAction=LOAD&hdnLoadPnrCriteria="+getValue("hdnLoadPnrCriteria");
							makePOST(url,params);
						}   
				break;
		}
	}
	
	function processStateChange() {
		var msg;

		if (httpReq.readyState == 4){
			if (httpReq.status==200){
	   			if(getValue("hdnPnrLoad") ==  'true') {
	   				arrayString = httpReq.responseText;
	                arrayString = arrayString.replace(/^\s+|\s+$/g, '');
	                eval(arrData = arrayString);
	                objDG.arrGridData = arrData;
	                if (arrData.length > 0) {
	                    setVisible("spnPNR", true);
	                    setVisible("spnPnrSelectAll", true);
		            	setVisible("spnPNR", true);
		            	updateRecap();
		            	DivWrite("spnTotlaRecs", 'Total number of PNRs :&nbsp; <b>' + arrData.length + '</b>');
		            	Disable("btnViewMsg", false);
		        		Disable("btnEdit", false);
		        		Disable("btnSend", false);
		        		Disable("btnGenerate", false);
	                }else {
	                	Disable("btnViewMsg", true);
	            		Disable("btnEdit", true);
	            		Disable("btnSend", true);
	            		Disable("btnGenerate", true);
	                }
	                objDG.refresh();
	                setField("hdnPnrLoad","false");
	                onLoad();
	   			}else {
	   				msg= httpReq.responseText;
					msg= msg.replace(/^\s+|\s+$/g, '') ;
					if((msg != null) && (msg == "EDITMSG"))  {
						Disable("txtMessage", false);
					} else {
						var arrResponse = new Array();
						arrResponse = msg.split("###");
						if(arrResponse[0] == "ERR-DATETIME") {
							setField("txtCode", "");
							setField("txtMessage", "");
							setField("hdnNtCode", "");
							setField("hdnNtMessage", "");
							showERRMessage("New Dep.Date/Time same as original Dep.Date/Time");
						} else if(arrResponse[0] == "ERROR") {
							showPageERRMessage(raiseError('XBE-ERR-33'),"txtMessage");
							Disable("txtMessage", true);
						} else {						
							
								setVisible("btnEdit", true);
								Disable("txtMessage", false);
								setField("txtCode", arrResponse[0]);
								setField("txtMessage", arrResponse[1]);
								document.getElementById('smsCharLength').innerHTML = getMessageDefaultCount(arrResponse[1]);
								setField("hdnNtCode", arrResponse[0]);
								setField("hdnNtMessage", arrResponse[1]);
						}
					}
	   			}
		   		
			}else {
				showPageERRMessage(raiseError('XBE-ERR-33'),"txtMessage");
	     	}
		}
	    
	}
	
	function validate(btnNo) {
		top[2].MessageReset();
		
		if(btnNo == 0)
		{
			if ((getValue("txtPnr") == "" || getValue("txtPnr") ==null)
					&& (getValue("txtFlightNo") == "" || getValue("txtFlightNo") ==null)){
				showERRMessage(raiseError("XBE-ERR-01", "Flight No OR PNR"));
				return false;
			}

			if (getValue("txtDepDateFrom") == "" && getValue("txtDepDateTo") != "") {
				showERRMessage(raiseError("XBE-ERR-01", "Departure From Date"));
				getFieldByID("txtDepDateFrom").focus();
				return false;
			}
			
		
			
			if (getValue("txtAlertFromDate") == "" && getValue("txtAlertToDate") != "") {
				showERRMessage(raiseError("XBE-ERR-01", "Alert From Date"));
				getFieldByID("txtAlertFromDate").focus();
				return false;
			}
			
			if((getText("txtDepDateFrom")!= "") && dateValidDate(getText("txtDepDateFrom"))==false){
				showERRMessage(raiseError("XBE-ERR-04", "From Date"));
				getFieldByID("txtDepDateFrom").focus();
				return false;
			}
			if((getText("txtDepDateTo")!= "") && dateValidDate(getText("txtDepDateTo"))==false){
				showERRMessage(raiseError("XBE-ERR-04", "To Date"));
				getFieldByID("txtDepDateTo").focus();
				return false;
			}			
		
			
			if((getText("txtAlertFromDate")!= "") && dateValidDate(getText("txtAlertFromDate"))==false){
				showERRMessage(raiseError("XBE-ERR-04", "From Date"));
				getFieldByID("txtAlertFromDate").focus();
				return false;
			}
			if((getText("txtAlertToDate")!= "") && dateValidDate(getText("txtAlertToDate"))==false){
				showERRMessage(raiseError("XBE-ERR-04", "To Date"));
				getFieldByID("txtAlertToDate").focus();
				return false;
			}
			if(getValue("specificDelay")!=""){
				timeDelay= getValue("specificDelay").split(":");
				if (timeDelay.length==3){
					for (var i=0;i<timeDelay.length;i++){
						if (timeDelay[i].length!=2){
							showERRMessage(raiseError("XBE-ERR-04", "Specific Delay time"));
							return false;
						}
					}
				} else{
					showERRMessage(raiseError("XBE-ERR-04", "Specific Delay time"));
					return false;
				}
				
			}
			if (getValue("txtDepDateFrom") != "" && getValue("txtDepDateTo") != "") {
				if(!validateDepDate()){
					showERRMessage(raiseError("XBE-ERR-03", "Departure To Date","Departure From Date"));
					getFieldByID("txtDepDateFrom").focus();
					return false;
				}		
				
			}

			if (getValue("txtDepDateFrom") == "") {
		        var fromDate = new Date();
		        var dd = fromDate.getDate();
		        var mm = fromDate.getMonth() + 1;
		        var yyyy = fromDate.getFullYear();

		        if (dd < 10) {
		            dd = '0' + dd
		        }

		        if (mm < 10) {
		            mm = '0' + mm
		        }

		        fromDate = dd + '/' + mm + '/' + yyyy;
		        getFieldByID("txtDepDateFrom").value = fromDate;
		    }
		    if (getValue("txtDepDateTo") == "" || compareDate(getText("txtDepDateFrom"), getText("txtDepDateTo")) == 1) {

		        var td = getValue("txtDepDateFrom").split('/');

		        var toDate = new Date(td[2], td[1] - 1, td[0]);
		        var dd = toDate.getDate();
		        var mm = toDate.getMonth() + 2;
		        var yyyy = toDate.getFullYear();

		        if (dd < 10) {
		            dd = '0' + dd
		        }

		        if (mm < 10) {
		            mm = '0' + mm
		        } else if (mm > 12) {
		            mm = '01';
		            yyyy = yyyy + 1;
		        }

		        toDate = dd + '/' + mm + '/' + yyyy;
		        getFieldByID("txtDepDateTo").value = toDate;

		    }
			
		}
		else if(btnNo == 1) {
			if (getValue("selNotifyPurpose") == "" || getValue("selNotifyPurpose") == "-1") {
				showERRMessage(raiseError("XBE-ERR-01", "Notification Purpose"));
				getFieldByID("selNotifyPurpose").focus();		
				return false;
			}
			if (getValue("selNotifyChannel") == "" || getValue("selNotifyChannel") == "-1") {
				showERRMessage(raiseError("XBE-ERR-01", "Notification Channel"));
				getFieldByID("selNotifyChannel").focus();		
				return false;
			}
			if (getValue("selNotifyPurpose") == "UPDATE")
			{
				if (getValue("txtNewDepDate") == "" || getValue("txtDepTime1") == "") {
					showERRMessage(raiseError("XBE-ERR-01", "New Departure Date/Time"));
					getFieldByID("txtDepTime1").focus();		
					return false;
				}
				else if (!CheckDates(top.strSysDate, getValue("txtNewDepDate"))){
					showERRMessage(raiseError("XBE-ERR-03","New Departure Date", top.strSysDate));
					getFieldByID("txtNewDepDate").focus();	
					return false;
				}
				else if(top.strSysDate == getValue("txtNewDepDate")) {
					var currentTime = new Date();
					var colonIndex = getValue("txtDepTime1").indexOf(":");
					var newDepHrs = getValue("txtDepTime1").substring(0,colonIndex);
					var newDepMins = getValue("txtDepTime1").substring(colonIndex+1,5);
					if(newDepHrs < currentTime.getHours() || 
							(newDepHrs == currentTime.getHours() && newDepMins < currentTime.getMinutes()))
					{
						var mins = currentTime.getMinutes();
						if(currentTime.getMinutes() >= 0 && currentTime.getMinutes() <= 9)
							mins = "0" +currentTime.getMinutes();
						showERRMessage(raiseError("XBE-ERR-03","New Departure Time", currentTime.getHours()+":"+mins));
						getFieldByID("txtDepTime1").focus();	
						return false;
					}
				}
				
			}
			
			if(getValue("selGenLanguage") == ""){
				showERRMessage(raiseError("XBE-ERR-01", "Language"));
				getFieldByID("selGenLanguage").focus();		
				return false;
			}
			
		}
		else if(btnNo == 3) {
//			if (getValue("txtFlightNo") == "" || getValue("txtDepDateFrom") == "") {
//				showERRMessage(raiseError("XBE-ERR-01", "Flight No/Dep.Date"));
//				getFieldByID("txtFlightNo").focus();		
//				return false;
//			}
			
			if (getValue("selNotifyPurpose") == "" || getValue("selNotifyPurpose") == "-1") {
				showERRMessage(raiseError("XBE-ERR-01", "Notification Purpose"));
				getFieldByID("selNotifyPurpose").focus();		
				return false;
			}
			if (getValue("selNotifyChannel") == "" || getValue("selNotifyChannel") == "-1") {
				showERRMessage(raiseError("XBE-ERR-01", "Notification Channel"));
				getFieldByID("selNotifyChannel").focus();		
				return false;
			}
			if(getValue("hdnNtMessage") == "")
			{
				if (getValue("selNotifyPurpose") == "UPDATE")
				{
					if (getValue("txtNewDepDate") == "" || getValue("txtDepTime1") == "") {
						showERRMessage(raiseError("XBE-ERR-01", "New Departure Date/Time"));
						getFieldByID("txtDepTime1").focus();		
						return false;
					}
					else if (!CheckDates(top.strSysDate, getValue("txtNewDepDate"))){
						showERRMessage(raiseError("XBE-ERR-03","New Departure Date", top.strSysDate));
						getFieldByID("txtNewDepDate").focus();	
						return false;
					}
					else if(top.strSysDate == getValue("txtNewDepDate")) {
						var currentTime = new Date();
						var colonIndex = getValue("txtDepTime1").indexOf(":");
						var newDepHrs = getValue("txtDepTime1").substring(0,colonIndex);
						var newDepMins = getValue("txtDepTime1").substring(colonIndex+1,5);
						if(newDepHrs < currentTime.getHours() || 
								(newDepHrs == currentTime.getHours() && newDepMins < currentTime.getMinutes()))
						{
							var mins = currentTime.getMinutes();
							if(currentTime.getMinutes() >= 0 && currentTime.getMinutes() <= 9)
								mins = "0" +currentTime.getMinutes();
							showERRMessage(raiseError("XBE-ERR-03","New Departure Time", currentTime.getHours()+":"+mins));
							getFieldByID("txtDepTime1").focus();	
							return false;
						}
					}
					
				}
				
				showERRMessage(raiseError("XBE-ERR-01", "Generated Message"));
				return false;
			}
			if(objDG.getSelectedColumn(5) == null || objDG.getSelectedColumn(5).length == 0)
				return false;
		} 
		return true;
	}
	
	function validateDate(){	
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
			
		var dateFrom = getText("txtAlertFromDate");
		var dateTo = getText("txtAlertToDate");
		
		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
					
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
			
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			
		
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM;}
		if (dtCD < 10){dtCD = "0" + dtCD;}
		 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
			
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
			
		strSysODate=(tempYear+tempMonth+tempDay); 
		if(tempOStartDate > tempOEndDate){
			validate = false;
		} else {
			validate = true;				
		}
		return validate;	
	}
		
	function buildPnrLoadCriteria() {
			var pnrLoadInfo = new Array();
			var resultsLimit;
			if (getValue("hdnLoadPnrCriteria") != "") {
				
				pnrLoadInfo = getValue("hdnLoadPnrCriteria").split(",");
				pnrLoadInfo[0] = getValue("hdnAction");	
				/*if (getValue("hdnAction") == "VIEWPNR") {		
					alertInfo[6] = getValue("hdnPnr");
				} else {
					alertInfo[6] = "";
				}*/
				setField("hdnLoadPnrCriteria", pnrLoadInfo);
				top.strSearchCriteria = getText("hdnLoadPnrCriteria");
			} else {
				var selectedRows=flightDG.getSelectedColumn("0");
				var pnrLoad = "";
                for (var i=0; i<selectedRows.length; i++){
                	if(selectedRows[i][1]==true){
                		pnrLoadInfo = new Array();
                        pnrLoadInfo[0] = getValue("hdnAction");        
                        pnrLoadInfo[1] =  arrSearchData[i][1];        
                        pnrLoadInfo[2] =  arrSearchData[i][2];   
						pnrLoadInfo[3] = "";//getValue("txtPnr");
						pnrLoadInfo[4] = "-1";//getValue("selSrchMobile");
						pnrLoadInfo[5] = "-1";//getValue("selSrchEmail");	
						pnrLoadInfo[6] = "-1";//getValue("selSrchPhone");
						pnrLoadInfo[7] = "";//getValue("txtMobilePattern").replace(",",";");
						pnrLoadInfo[8] = "-1";//getValue("selExistingCodes");
						pnrLoadInfo[9] = getText("hdnSearchFlightCriteria").split(",")[9];
						pnrLoadInfo[10] = "+";//getValue("selPrefLanguage")!=""?getValue("selPrefLanguage"):" ";
//						var objControl = getFieldByName("radLanguage");
//						var strReturn = " ";
//						var intLength = objControl.length ;
//						for (var i = 0 ; i < intLength ; i++){
//							if (objControl[i].checked){
//								strReturn = objControl[i].value;
//							}
//						}			
						pnrLoadInfo[11] = "+";//strReturn;
						pnrLoadInfo[12] = getValue("selAnciReprotectStatus")!=""?getValue("selAnciReprotectStatus"):" ";
						
						pnrLoadInfo[13] = "";//getValue("txtAdults");
						pnrLoadInfo[14] = "";//getValue("txtChildren");
						pnrLoadInfo[15] = "";//getValue("txtInfants");
						pnrLoadInfo[16]	= "";//getValue("txtAlertFromDate");
						pnrLoadInfo[17]	= "";//getValue("txtAlertToDate");
						pnrLoadInfo[18] = "PNR";//getValue("selSortBy");
						pnrLoadInfo[19] = "ASC";//getValue("selSortByOrder");
						pnrLoadInfo[20] = "";//getValue("selSegFrom");
						pnrLoadInfo[21] = "";//getValue("selSegTo");
						pnrLoadInfo[22] = "true";//getFieldByID("chkAdults").checked;
						pnrLoadInfo[23] = "true";//getFieldByID("chkChildren").checked;
						pnrLoadInfo[24] = "true";//getFieldByID("chkInfants").checked;
						
						if(pnrLoad == ""){
							pnrLoad = pnrLoadInfo;
						}else{
							pnrLoad += "==" + pnrLoadInfo;
						}
                    }
                }
                setField("hdnLoadPnrCriteria", pnrLoad);
        		top.strSearchCriteria = getText("hdnLoadPnrCriteria");
			}
	}
	
	function buildFlightLoadCriteria() {
		var flightLoadInfo = new Array();
		var resultsLimit;
		flightLoadInfo[0] = getValue("hdnAction");		
		flightLoadInfo[1] = getValue("txtPnr"); //	
		flightLoadInfo[2] = getValue("txtFlightNo");
		flightLoadInfo[3] = getValue("txtDepDateFrom");
		flightLoadInfo[4] = getValue("txtDepDateTo");	
			//customise search
		flightLoadInfo[5] = getValue("selSegFrom");
		flightLoadInfo[6] = getValue("selSegTo");
		flightLoadInfo[7] = getValue("specificDelay");
		flightLoadInfo[8] = getValue("selExistingCodes");
		flightLoadInfo[9] = getValue("selAlert");						
		flightLoadInfo[10] = getValue("txtAlertFromDate");
		flightLoadInfo[11] = getValue("txtAlertToDate");
		if (daysOfOperation.length>0){
			var days="";
			for (var i=0; i< daysOfOperation.length ; i++){
				if(days == ""){
					days = daysOfOperation[i];
				}else{
					days += "/" + daysOfOperation[i];
				}
			}
			flightLoadInfo[12] =days;
		} else {
			flightLoadInfo[12] = "";
		}
		flightLoadInfo[13] = getValue("selSortBy");
		flightLoadInfo[14] = getValue("selSortByOrder");
		flightLoadInfo[15] = getValue("selAnciReprotectStatus")!=""?getValue("selAnciReprotectStatus"):" ";
		flightLoadInfo[16] = getValue("selConnection");
		
		setField("hdnSearchFlightCriteria", flightLoadInfo);
		top.strFlightSearchCriteria = getText("hdnSearchFlightCriteria");
			
	}
	
	function pageOnChange() {
		top[2].HidePageMessage();
	}

	function keyValidate(strID, objControl) {
		pageOnChange();
		switch (strID) {
			case 0 : 
				var strValue = objControl.value ;
				if (!validateInteger(strValue)) {
					objControl.value = strValue.substr(0, strValue.length - 1);
					objControl.focus();
				}
				break;
		}
	}
	
	
	function initialize() {
		setVisible("spnSelectAll", false);
		//setVisible("spnPNR", false);
		if(getValue("txtPnr") != "")
			Disable("chkSelectAll", true);
		else
			Disable("chkSelectAll", false);
		//Disable("btnSend", true);
		//setField("txtResultLimit", getValue("hdnResultsLimit"));
		top.strPageFrom		= "";
		top.strPageReturn	= "";
		//checkRadio();
		//top[2].HideProgress();
		Disable("txtMessage", true);
		Disable("txtCode", true);
		setEmailVisibilty();
		if (!top.isPnr){
			top.strFlightSearchCriteria = "";	
		}
	}
	
	function enableDisableLanguageRad(optVal){
		if(optVal != ""){
			getFieldByID("radInclude").disabled = false;
			getFieldByID("radExclude").disabled = false;
			getFieldByID("radInclude").checked = true;
		} else {
			getFieldByID("radInclude").checked = false;
			getFieldByID("radExclude").checked = false;
			getFieldByID("radInclude").disabled = true;
			getFieldByID("radExclude").disabled = true;
		}
	}
	
	/**
	 * function to make Attach email visible
	 */
	function setEmailVisibilty(){
		setVisible("spnEmail", false);
		setVisible("emailSub", false);
		var strChange = getValue("selNotifyPurpose");
		var strNotify = getValue("selNotifyChannel");
		if(strChange == "REPROTECT" || strChange== "CHANGE" || strChange== "UPDATE"){
			if(strNotify == "2" || strNotify == "3") {
				setVisible("spnEmail", true);
			}
		}
		if(strNotify == "2" || strNotify == "3") {
			setVisible("emailSub", true);
		}
		
	}
	
	function populateFlightLoadCriteria(StrFlightInfo){
		if ((StrFlightInfo != null) && (StrFlightInfo != "")) {			
			setField("hdnSearchFlightCriteria", StrFlightInfo);
			var flightLoadInfo = StrFlightInfo.split(",");
			setField("hdnAction",flightLoadInfo[0]);
			setField("hdnPnr",flightLoadInfo[1]);
			setField("selExistingCodes",flightLoadInfo[8]);
			setField("txtPnr",flightLoadInfo[1]);
			setField("txtFlightNo",flightLoadInfo[2]);			
			setField("txtDepDateFrom",flightLoadInfo[3]);	
			setField("txtDepDateTo",flightLoadInfo[4]);
			setField("selSegFrom",flightLoadInfo[5]);
			setField("selSegTo",flightLoadInfo[6]);
			setField("specificDelay",flightLoadInfo[7])
			setField("selExistingCodes",flightLoadInfo[8]);
			setField("selAlert",flightLoadInfo[9]);
			setField("txtAlertFromDate",flightLoadInfo[10]);
			setField("txtAlertToDate",flightLoadInfo[11]);
			setField("selSortBy",flightLoadInfo[13]);
			setField("selSortByOrder",flightLoadInfo[14]);
			setField("selAnciReprotectStatus",flightLoadInfo[15]);	
			setField("selConnection",flightLoadInfo[16]);
			daysOfOperation = flightLoadInfo[12].split("/");
			if (daysOfOperation != ""){
				for (var i = 0; i < daysOfOperation.length; i++) {
					var id = "chk_" + daysOfOperation[i];
					document.getElementById(id).checked = true;

				}
			}
			
		}
	}
	
	function populatePnrLoadCriteria(strPnrInfor) {		
//		alert("aaa populateAlertInfo(strAlertInfor) : " + strAlertInfor);
		var flighNoDepDate = "";
		if ((strPnrInfor != null) && (strPnrInfor != "")) {			
			setField("hdnLoadPnrCriteria", strPnrInfor);
			var pnrLoadInfo = strPnrInfor.split(",");
			setField("hdnAction",pnrLoadInfo[0]);		
			setField("hdnFlightNo",pnrLoadInfo[1]);		
			setField("hdnDepDate",pnrLoadInfo[2]);
			var multiPnrLoadInfo = strPnrInfor.split("==");
			for(var i = 0; i < multiPnrLoadInfo.length; i++){
				var pnrloadDetails = multiPnrLoadInfo[i].split(",");
				if(flighNoDepDate == ""){
					flighNoDepDate = pnrloadDetails[1] + ":" + pnrloadDetails[2];
				}else {
					flighNoDepDate += "," + pnrloadDetails[1] + ":" + pnrloadDetails[2];
				}
			}
			setField("hdnFlightNoDepDate",flighNoDepDate);
		}
	}
	
	
	
	function populateFieldInfo(str) {		
//		alert("aaa populateAlertInfo(strAlertInfor) : " + strAlertInfor);
		if ((str != null) && (str != "")) {			
			setField("hdnFields", str);
			var fieldInfo = str.split(",");
			setField("selNotifyPurpose",fieldInfo[0]);		
			setField("txtNewDepDate",fieldInfo[1]);		
			setField("txtDepDateTo",fieldInfo[2]);	
			setField("txtCode",fieldInfo[3]);	
			setField("txtMessage",fieldInfo[4]);
			document.getElementById('smsCharLength').innerHTML = getMessageDefaultCount(fieldInfo[4]);
			setField("selNotifyChannel",fieldInfo[5]);
			setField("selExistingCodes", fieldInfo[6]);
			setField("txtDepDateFrom",fieldInfo[7]);
			// Setting the notification
			if(getValue("selExistingCodes") == "-1" && getValue("txtCode") != null && getValue("txtCode") != "")
				for(var k=0; k < document.getElementById("selExistingCodes").options.length; k++)
				{
					var selText = document.getElementById("selExistingCodes").options[k].text;
					if(selText == getValue("txtCode"))
						document.getElementById("selExistingCodes").options[k].selected = true;
				}


		}
	}
	
	function buildFieldInfo() {
		var fieldInfo = new Array();
		fieldInfo[0] = getValue("selNotifyPurpose");		
		fieldInfo[1] = getValue("txtNewDepDate");	
		fieldInfo[2] = getValue("txtDepDateTo");
		fieldInfo[3] = getValue("hdnNtCode");
		if(getValue("selNotifyChannel") == "2"){
			fieldInfo[4] = "";
		} else {
			fieldInfo[4] = getValue("hdnNtMessage");
		}		
		fieldInfo[5] = getValue("selNotifyChannel");
		fieldInfo[6] = getValue("selExistingCodes");
		fieldInfo[7] = getValue("txtDepDateFrom");	
		setField("hdnFields", fieldInfo);
				
	}
	
	
	function onLoad() {
		top.ResetTimeOut();
		initialize();
		//top[2].ShowProgress();
		if (top.strSearchCriteria != "" && !top.isFlight) {
			populatePnrLoadCriteria(top.strSearchCriteria);	
			updateRecap();
		} 
		if (top.strFlightSearchCriteria != "" && top.isPnr) {
			populateFlightLoadCriteria(top.strFlightSearchCriteria);
			top.isPnr = false ;
		}
	    populateFieldInfo(strFields); // strFields set in jsp
		//checkPassengers();		
	}
	

	function showNotificationResponseMessage(){
				  			
		if ((reqMessage != null) && (reqMessage != "")) {
			if ((reqMessageType != null) && (reqMessageType == "confirmation")) {			
				showConfirmation(reqMessage);
			} else {
				showERRMessage(reqMessage);
			}
		} else {
			top[2].HidePageMessage();
		}
	}
	
	onLoad();
	
		
	//To Handle Paging
function gridNavigations(intRecNo, intErrMsg){

	//-----------------------------
	if (intRecNo <= 0) {
			intRecNo = 1;
		}
		//setSearchRecNo(intRecNo);	
		//setSearchCriteria()
		setField("hdnRecNo", intRecNo);
		setSearchRecNo(intRecNo);
		//setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		if (intErrMsg != ""){		
			showERRMessage(intErrMsg);
		}else{				
			setPageEdited(false);
			//document.forms[0].hdnMode.value="PAGE";
			setField("hdnMode", "PAGE");
			document.forms[0].submit();
			ShowProgress();
		
		}
}

function setSearchRecNo(recNo) {
	top[0].intLastRec = recNo;
}

function setTotalRecords(totRec){
	top[0].totalRec=totRec;
}


function winOnLoad() {
	buildFlightLoadCriteria();
	buildPnrLoadCriteria();
	setField("hdnFlightId", strFlightId);
}

	
function setTimeWithColon(field, strTime) {
	//objOnFocus();
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			setField(field, strTime);
		} else {
			var mn = strTime.substr(index,2);
			var hr = 0;
			if (strTime.length == 2) {
				hr = 00;
			}else if(strTime.length == 3) {
				hr =  strTime.substr(0,1); 
			
			}else {
				hr = strTime.substr(0,2); 
			}
			var timecolon = hr +":" + mn ;
			setField(field, timecolon);
			strTime = timecolon;
		}
	}
	
	return IsValidTime(strTime);
	
}

/*
function validateNotifyPurpose()
{
	var nPurpose = getValue("selNotifyPurpose");
	
	if(nPurpose == "UPDATE" && getValue("selExistingCodes") == "-1")
	{
		show("table1");
		
	}
	else 
		hide("table1");
	
}


function show(elem){
	document.getElementById(elem).style.display="block";
}

function hide(elem){
	document.getElementById(elem).style.display="none";
}


function validateCodeDetails() {
	var selectedCode = getValue("selExistingCodes");
	if(selectedCode != -1) {
		Disable("btnGenerate",true);
		Disable("btnEdit",true);
		Disable("txtMessage",true);
		var selText = document.getElementById("selExistingCodes").options[document.getElementById("selExistingCodes").selectedIndex].text;
		setField("txtCode", selText);
		setField("hdnNtCode", selText);
		for (var k=0; k < arrNCodes.length; k++)
		{
			arrNCodes[k][1] = arrNCodes[k][1].replace(/^\s+|\s+$/g, '') ;
			if(arrNCodes[k][1] == selectedCode) {
				setField("txtMessage", arrNCodes[k][2]);
				setField("hdnNtMessage", arrNCodes[k][2]);
				break;
			}
		}
		hide("table1");
		
		if(selText.indexOf('CNX') != -1)
			setField("selNotifyPurpose", 'CANCEL');
		else if((selText.indexOf('CHG') != -1))
			setField("selNotifyPurpose", 'UPDATE');
		else
			setField("selNotifyPurpose", '-1');
	}
	else {
		Disable("btnGenerate",false);
		Disable("btnEdit",false);
		setField("txtCode", "");
		setField("txtMessage", "");
		setField("hdnNtCode", "");
		setField("hdnNtMessage", "");
		setField("selNotifyPurpose","-1");
		//validateNotifyPurpose();
	}
	selectPNRs();
}

*/

function validatePnrSelection() {
	if(arrSmsPnrList.length == 0 && arrEmailPnrList.length == 0) { // From SendFlightPnrNotificationGrid.js
		return false;
	}
	return true;
}

/*function checkPassengers(){
	if(getFieldByID("chkAdults").checked){
		getFieldByID("txtAdults").disabled = true;
		getFieldByID("txtAdults").value = "";
	} else {
		getFieldByID("txtAdults").disabled = false;
	}
	
	if (getFieldByID("chkChildren").checked){
		getFieldByID("txtChildren").disabled = true;
		getFieldByID("txtChildren").value = "";
	}else{
		getFieldByID("txtChildren").disabled = false;
		
	}
	
	if (getFieldByID("chkInfants").checked){
		getFieldByID("txtInfants").disabled = true;
		getFieldByID("txtInfants").value = "";
	}else{
		getFieldByID("txtInfants").disabled = false;
	}	
}*/

function clearAlertDates(){
	if(document.getElementById("selAlert").value == "WOA"){
		getFieldByID("txtAlertFromDate").value = "";
		getFieldByID("txtAlertToDate").value = "";
		getFieldByID("txtAlertFromDate").disabled = true;
		getFieldByID("txtAlertToDate").disabled = true;
	} else if (document.getElementById("selAlert").value == "ANY"){
		getFieldByID("txtAlertFromDate").disabled = false;
		getFieldByID("txtAlertToDate").disabled = false;
	} else if (document.getElementById("selAlert").value == "WA"){
		getFieldByID("txtAlertFromDate").disabled = false;
		getFieldByID("txtAlertToDate").disabled = false;
	}
}

function updateDayOfoperationChange(id){
		var index = daysOfOperation.indexOf(getFieldByID(id).value);
		if (getFieldByID(id).checked) {
			if (index == -1) {
				daysOfOperation.push(getFieldByID(id).value);
			}
		} else if (daysOfOperation.indexOf(getFieldByID(id).value) != -1) {
			daysOfOperation.splice(index, 1);
		}
	
}

function updateRecap(){
	document.getElementById('spnPaxContactsDetails').innerHTML = '';
	setVisible("lblSelLoc", true);
    var countryList = objDG.arrGridData[(objDG.arrGridData.length - 1)][21];
    var res = countryList.split(",");
    var tableWidth = 100;
    if(res.length > 5) {
    	tableWidth = ((res.length*150)/750)*100;
    }
    
    document.getElementById('tablePaxContact').style.width = tableWidth + "%";
    
    for (var x = 0; x < res.length; x++) {
        var dat = res[x].split('-');
        if (dat[1]!=null){
            document.getElementById('spnPaxContactsDetails').insertAdjacentHTML('beforeend', '<td width="150px" align="left" class="GridItem"> <input onclick="resetDetailList(this)" class="GridItem" value="' + dat[1] + '" id="' + res[x] + '" type="checkbox">' + dat[0] + " (" + dat[2] + ")" + '</td>');           
        }
    }   
    setTimeout( function (){
		var countryList = objDG.arrGridData[(objDG.arrGridData.length - 1)][21];
	    var res = countryList.split(",");
	    for (var x = 0; x < res.length; x++) {
	    	document.getElementById(res[x]).checked=true;
	    	selectByCountry(document.getElementById(res[x]).value, 'true');
	    } 
	},750);
}

function resetDetailList(code) {
    if (code.checked) {
    	selectByCountry( code.value, 'true');
    } else {
    	selectByCountry( code.value, 'false');
    }
}

function selectByCountry( cCode, action) {
    var selectedPNRs = objDG.getSelectedColumn("0");
    var mob = objDG.getSelectedColumn(2);
    for (var i = 0; i < mob.length; i++) {
    	if (mob[i][1].split('-')[0] == cCode || mob[i][1].substring(0, cCode.length) == cCode) {
             objDG.setCellValue(i, 0, action);
        }
    }  
}


function validateSpecificTime(){
	var str = getValue("specificDelay");	
	str = str.replace(/[^0-9:]+/g, '');
	setField("specificDelay", str);	 
}

function getMessageLength(msgId){
	var message = msgId.value;
	var charLength = getMessageDefaultCount(msgId.value);
	document.getElementById('smsCharLength').innerHTML = charLength;
}

function getMessageDefaultCount(message){
	var varChar = '$';
	var count = (message!=null && message!="")?message.length:0;
	for(var i = 0; i < defaultMsgLength.length; i++){
		var replaceVal = varChar + defaultMsgLength[i].key;
		if(message.includes(replaceVal)){
			count += (defaultMsgLength[i].value - replaceVal.length);
		}
	}
	return count;
}

function validateDepDate(){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
		
	var dateFrom = getText("txtDepDateFrom");
	var dateTo = getText("txtDepDateTo");
	
	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
				
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM;}
	if (dtCD < 10){dtCD = "0" + dtCD;}
	 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
		
	strSysODate=(tempYear+tempMonth+tempDay); 
	if(tempOStartDate > tempOEndDate){
		validate = false;
	} else {
		validate = true;				
	}
	return validate;	
}