	/*
	*********************************************************
		Description		: XBE Make Reservation - Credit
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
	
	var jsonSCObj = [
					{id:"#pnr", desc:"PNR", required:false},
					{id:"#firstName", desc:"First Name", required:false},
					{id:"#lastName", desc:"Last Name", required:false},
					{id:"#fromAirport", desc:"Departure location", required:false},
					{id:"#depatureDate", desc:"Departure Date", required:false},
					{id:"#phoneNo", desc:"Phone No", required:false},
					{id:"#toAirport", desc:"Arrival location", required:false},
					{id:"#returnDate", desc:"Return Date", required:false},
					{id:"#flightNo", desc:"Flight No", required:false},
					{id:"#creditCardNo", desc:"Credit Card No", required:false},
					{id:"#cardExpiry", desc:"Expiry Date", required:false}
					];
					
	var jsPaxCredits = null;
	var selectedCredit= [];
					
	/*
	 * Change Fare
	 */
	function UI_useCredit(){}
	
	UI_useCredit.blnAdvSearch = false;
	UI_useCredit.creditApplied = 0;
	UI_useCredit.totBlance = "";
	UI_useCredit.openingDueAmount = 0;
	UI_useCredit.selPaxDisplayName="";		
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		if(lccEnabled === true){
			$('#spnChkSearchOther').show();
			$('#chkSearchOther').show();
		} else {
			$('#spnChkSearchOther').hide();
			$('#chkSearchOther').hide();
		}
		
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#depatureDate").datepicker({ dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		$("#returnDate").datepicker({ dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		
		$("#depatureDate").blur(function() { UI_useCredit.dateOnBlur({id:0});});
		$("#returnDate").blur(function() { UI_useCredit.dateOnBlur({id:1});});
		$("#cardExpiry").blur(function() { UI_useCredit.expiryDateChk();});
		
		$("#btnSearch").decorateButton();
		$("#btnConfirm").decorateButton();
		$("#btnCancel").decorateButton();
		
		$("#btnConfirm").disable();
		
		$("#btnSearch").click(function() { UI_useCredit.searchOnClick();});
		$("#btnConfirm").click(function() { UI_useCredit.confirmOnClick();});
		$("#btnCancel").click(function() { UI_useCredit.cancelOnClick();});
		$("#lnkAdvSrch").click(function() { UI_useCredit.advanceSearchClick();});
		
		UI_useCredit.fillDropDownData();
		
		$("#creditCardNo").keypress(function(e){return UI_commonSystem.numberOnly({event:e})});
		//find the credit using pax using pax sequence
		for(var i=0; i < opener.jsonPaxPayObj.length; i++){
			if(opener.jsonPaxPayObj[i].displayPaxID == opener.UI_tabPayment.selectedPax){
				UI_useCredit.selPaxDisplayName = opener.jsonPaxPayObj[i].displayPassengerName;
				$("#firstName").val(opener.jsonPaxPayObj[i].displayFirstName);
				$("#lastName").val(opener.jsonPaxPayObj[i].displayLastName);
				$("#paxID").val(opener.jsonPaxPayObj[i].displayPaxID);		
				$("#divAmtDue").html(opener.jsonPaxPayObj[i].displayOriginalTobePaid);
				UI_useCredit.totBlance = opener.jsonPaxPayObj[i].displayToPay;
				UI_useCredit.openingDueAmount = opener.jsonPaxPayObj[i].displayOriginalTobePaid;
				if(opener.jsonPaxPayObj[i].paxCreditInfo != null && opener.jsonPaxPayObj[i].paxCreditInfo.length > 0){
					for(var j=0;j < opener.jsonPaxPayObj[i].paxCreditInfo.length;j++){
						selectedCredit[selectedCredit.length] = {
							"paxID" : opener.jsonPaxPayObj[i].paxCreditInfo[j].paxCreditId,
							"carrier" : opener.jsonPaxPayObj[i].paxCreditInfo[j].carrier,
							"pnr" : opener.jsonPaxPayObj[i].paxCreditInfo[j].pnr,
							"utilizedCredit" : opener.jsonPaxPayObj[i].paxCreditInfo[j].usedAmount,
							"residingCarrierAmount" : opener.jsonPaxPayObj[i].paxCreditInfo[j].residingCarrierAmount};
					}
				}
				break;
			}			
		}	
		
		$("#firstName").disable();
		$("#lastName").disable();
		
		if (opener.top.arrPrivi[PRIVI_APPLY_CREDIT_ANY] == 1){
			$("#firstName").enable();
			$("#lastName").enable();
		}
		
		$("#chkSearchOther").disable();
		if (opener.top.arrPrivi[PRIVI_UTILIZE_ANYCARRIER_CREDIT] == 1){
			$("#chkSearchOther").enable();
		}
		
		
		$("#divAdvSearch").hide();
		if (opener.top.arrPrivi[PRIVI_ADVANCE_SEARCH] == 1){
		}else{
			$("#divAdvSearch").hide();
			$("#lnkAdvSrch").hide();
		}
	});
	
	/*
	 * TODO
	 * Search Click
	 */
	UI_useCredit.validateSearchClick = function (){
		UI_message.initializeMessage();
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidCharWindow(jsonSCObj)){return false;}
		
		if (($("#pnr").val() == "") && ($("#firstName").val() == "") && ($("#lastName").val() == "") && ($("#creditCardNo").val() == "")){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-11","")});
			$("#pnr").focus();	
			return false;
		}
		
		
		if ($("#depatureDate").val() != "" && $("#returnDate").val() != ""){
			if (!CheckDates($("#depatureDate").val(), $("#returnDate").val())){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-03","Return date", "from date")});
				$("#returnDate").focus();
				return false;
			}	
		}
		
		if ($("#fromAirport").val() != "" && $("#toAirport").val() != ""){
			if ($("#fromAirport").val() == $("#toAirport").val()){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-02","From location","To location")});
				$("#toAirport").focus();	
				return false;
			}
		}
		
		if ($("#chkExact").attr('checked')){
		//	if ($("#firstName").val() != ""){
		//		if ($("#firstName").val().length < 2){
		//			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-16","2","First Name")});
		//			$("#firstName").focus();	
		//			return false;
		//		}
		//	}
			
			if ($("#lastName").val() != ""){
				if ($("#lastName").val().length < 2){
					UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-16","2","Last Name")});
					$("#lastName").focus();						
					return false;
				}
			}
		}
		
		if ($("#creditCardNo").val() != ""){
			if ($("#cardExpiry").val() == ""){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-01","Expiry Date")});
				$("#cardExpiry").focus();		
				return false;
			}
			
			if ($("#creditCardNo").val().length !== 4){
				UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-16", "4", "Credit Card No")});
				$("#creditCardNo").focus();		
				return false;
			}
		}
		jsPaxCredits = null;
		UI_useCredit.creditApplied = 0;
		$("#divAmtPaying").html(UI_useCredit.creditApplied );
		$("#btnConfirm").disable();
		UI_commonSystem.showProgress();
		return true;
	}
	
	UI_useCredit.searchOnClick = function (){
		$("#frmSrchCredit").action("loadPaxCredit.action");
		$("#firstName").enable();
		$("#lastName").enable();
		var data = {};
		data['paxPayments'] = opener.UI_tabPayment.creditPaxPayObj;
		data['selPaxCredit'] = $.toJSON(selectedCredit);
		data['balanceToPay'] = UI_useCredit.openingDueAmount;
		data['operatingCarriers'] = opener.UI_tabPayment.operatingCarriers;
		data['pnr']= opener.UI_tabPayment.pnr;
		data['paxFullName']=UI_useCredit.selPaxDisplayName;		
		if(UI_useCredit.validateSearchClick()) {
			$("#frmSrchCredit").ajaxSubmit({ dataType: 'json', data:data, success: UI_useCredit.buildPaxCreditDetails});
		}else {
			if (!opener.top.arrPrivi[PRIVI_APPLY_CREDIT_ANY] == 1){
				$("#firstName").disable();
				$("#lastName").disable();
			}
		}
		
		return false;
	}

	/*
	 * TODO
	 * Confirm Click
	 */
	UI_useCredit.confirmOnClick = function (){
		if(!(opener.vouchersRedeemed)){
			UI_commonSystem.showProgress();
			opener.UI_tabPayment.applyPaxCredit($.toJSON(selectedCredit));
		} else {
			alert("Please remove voucher redemption and come back. You have to do credit payment first!");
		}
	}
	
	/*
	 * Cancel Click
	 */
	UI_useCredit.cancelOnClick = function (){
		window.close();
	}
	
	
	UI_useCredit.creditSelOnchange = function() {
		$("#btnConfirm").disable();
		if($("input[id*='chk_']:checked").length >0) {
			$("#btnConfirm").enable();
		}
	}
	
	/*
	 * Build Available Credit Grid
	 */
	UI_useCredit.buildPaxCreditDetails = function(response){
		UI_commonSystem.hideProgress();
		/* Initialize the Details */
		$("#tblCredit").children().remove();
		
		$("#firstName").disable();
		$("#lastName").disable();
		if (opener.top.arrPrivi[PRIVI_APPLY_CREDIT_ANY] == 1){
			$("#firstName").enable();
			$("#lastName").enable();
		}
		
		
		if(response.success){
			
			var seltdPaxId = response.paxID;
			for(var i=0; i < opener.jsonPaxPayObj.length; i++){
				if(opener.jsonPaxPayObj[i].displayPaxID == response.paxID){
					seltdPaxId = i;
				}
			}
			var  blnCrSelected = false;
			var opnpayObj = opener.jsonPaxPayObj;
			var  alreadySelected = false;
			UI_useCredit.totBlance  = response.balanceToPay;
			UI_useCredit.creditApplied = response.paxAppliedCredit;
			$("#divAmtPaying").html(UI_useCredit.creditApplied );			
			if(opnpayObj[seltdPaxId].paxCreditInfo.length > 0){
				blnCrSelected = true;
			}
			
			var objCredit = response.paxCreditList;
			jsPaxCredits = response.paxCreditList;
			if (objCredit != null){
				var intLen = objCredit.length;
				var i = 0;
				var tblRow = null;
				var intRowSpan = intLen;
				var strClass = "";
				var intRowSpan = 0;
				var objFlt = null;
				var intFltLen = 0;
				var x = 0;
				if (intLen > 0){
					do{
						strClass = "";
						tblRow = document.createElement("TR");
						
						objFlt	= objCredit[i].flightInfo;
						intRowSpan = objFlt.length;
						intFltLen = intRowSpan;
						
						tblRow.appendChild(UI_commonSystem.createCell({desc:objCredit[i].pnrNo, css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", rowSpan:intRowSpan, align:"Center", width:	"10%;", vAlign:"top"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:objCredit[i].paxName, css:UI_commonSystem.strDefClass + " rowHeight", rowSpan:intRowSpan, width:"18%;", vAlign:"top"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:objCredit[i].carrier, css:UI_commonSystem.strDefClass + " rowHeight", rowSpan:intRowSpan, width:"6%;", vAlign:"top"}));
		
						// Flight Details
						x = 0;
						do{
							if (x != 0){
								tblRow = document.createElement("TR");
							}
							
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFlt[x].flightNo + " " + objFlt[x].airLine, css:UI_commonSystem.strDefClass + " rowHeight", align:"Center", width:"10%;"}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFlt[x].departureDate + " " + objFlt[x].departureTime, css:UI_commonSystem.strDefClass + " rowHeight", align:"Center", width:"15%;"}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFlt[x].arrivalDate + " " + objFlt[x].arrivalTime, css:UI_commonSystem.strDefClass + " rowHeight", align:"Center", width:"15%;"}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFlt[x].orignNDest, css:UI_commonSystem.strDefClass + " rowHeight", align:"Center", width:"10%;"}));
							
							if (x == 0){
								tblRow.appendChild(UI_commonSystem.createCell({desc:objCredit[i].paxCount, css:UI_commonSystem.strDefClass + " rowHeight", rowSpan:intRowSpan, align:"Center", width:"8%;"}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:objCredit[i].pnrStatus, css:UI_commonSystem.strDefClass + " rowHeight", rowSpan:intRowSpan, align:"Center", width:"8%;"}));
							}
							
							$("#tblCredit").append(tblRow);
							x++;
						}while(--intFltLen);
						
						// Passenger Details
						x = 0;
						objFlt	= objCredit[i].paxCredits;
						intFltLen = objFlt.length;
						strClass = " alternateRow ";
						do{
							alreadySelected = false;
							tblRow = document.createElement("TR");
							if(blnCrSelected){
								for(var al=0;al < opnpayObj[seltdPaxId].paxCreditInfo.length;al++){
									if(opnpayObj[seltdPaxId].paxCreditInfo[al].paxCreditId == objFlt[x].paxID){
										alreadySelected = true;
										break;
									}
								}
							}
							if(alreadySelected){
								tblRow.appendChild(UI_commonSystem.createCell({desc:"<input type='checkbox' id='chk_" + objFlt[x].paxID + "' name='chk_" + objFlt[x].paxID + "' value='" +  i + "_" + x + "' class='noBorder' checked onclick='UI_useCredit.addCreditOnClick(this)'>", css:UI_commonSystem.strDefClass + " rowHeight " + strClass, align:"Center"}));
								objFlt[x].selected = true;																
							}else {
								tblRow.appendChild(UI_commonSystem.createCell({desc:"<input type='checkbox' id='chk_" + objFlt[x].paxID + "' name='chk_" + objFlt[x].paxID + "' value='" +  i + "_" + x + "' class='noBorder' onclick='UI_useCredit.addCreditOnClick(this)'>", css:UI_commonSystem.strDefClass + " rowHeight " + strClass, align:"Center"}));
								objFlt[x].selected = false;
							}
							
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFlt[x].paxName, css:UI_commonSystem.strDefClass + " rowHeight " + strClass, colSpan:2}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objFlt[x].availableCredit, css:UI_commonSystem.strDefClass + " rowHeight " + strClass, align:"right"}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:"<div id='div_" + objFlt[x].paxID + "' name='div_" + objFlt[x].paxID + "'><\/div>", css:UI_commonSystem.strDefClass + " rowHeight " + strClass, colSpan:4}));
							
							$("#tblCredit").append(tblRow);
							x++;
						}while(--intFltLen);
					
						i++;
					}while(--intLen);
				}
			}		
			
		}else {
			UI_message.showErrorMessage({messageText:response.messageTxt});
			return false;
		}
		
	}
	
	/*
	 *  Add Credit 
	 */
	UI_useCredit.addCreditOnClick = function(objC){
		var blnProceed = true;
		if (Number(UI_useCredit.creditApplied) > Number(UI_useCredit.totBlance)){
			if (objC.checked){
				objC.checked = false;
				return false;
			}
		}
		
		if (blnProceed){
			var arrValue = objC.value.split("_");
			var objCredit = jsPaxCredits;
			if (objCredit != null){
				if (objC.checked){
					if(Number(jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].availableCredit) > 0){
						UI_useCredit.creditApplied = (Number(UI_useCredit.creditApplied)*100 + Number(jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].availableCredit) * 100)/100;
						jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].utilizedCredit = jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].availableCredit;
						jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].selected = true;
						selectedCredit[selectedCredit.length] = jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]];
					}else {
						objC.checked = false;
						return false;
					}
					
				}else{
					UI_useCredit.creditApplied = (Number(UI_useCredit.creditApplied)* 100 - Number(jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].availableCredit) * 100)/100;
					//to set same pax click the credit button & remove the credit. putting a string check 
					if(Number(jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].utilizedCredit) > 0  
							&& jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].utilizedCredit != jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].availableCredit){
						jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].availableCredit = jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].originalCredit;
					}
					jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].utilizedCredit = 0;										
					jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].selected = false;
					for(var sl=0;sl< selectedCredit.length;sl++){
						if(selectedCredit[sl] != null && selectedCredit[sl].paxID == jsPaxCredits[arrValue[0]].paxCredits[arrValue[1]].paxID){
							selectedCredit[sl] = null;
							break;
						}
					}
					
				}
			}
			
			$("#divAmtPaying").html($.airutil.format.currency(UI_useCredit.creditApplied));
		}
		UI_useCredit.creditSelOnchange();
	}
	
	/*
	 * Advance Search Click
	 */
	UI_useCredit.advanceSearchClick = function (){
		if (!UI_useCredit.blnAdvSearch){
			$("#divAdvSearch").show();
			UI_useCredit.blnAdvSearch = true;
		}else{
			UI_useCredit.blnAdvSearch = false;
			$("#divAdvSearch").hide();
		}
	}
	
	/*
	 * Auto Date generation
	 */
	UI_useCredit.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 : dateChk("depatureDate"); break;
			case 1 : dateChk("returnDate"); break;
		}
	}
	
	/*
	 * Fill Drop Down Data for the Page
	 */
	UI_useCredit.fillDropDownData = function(){
		$("#fromAirport").fillDropDown( { dataArray:opener.top.arrAirports , keyIndex:0 , valueIndex:2, firstEmpty:true });
		$("#toAirport").fillDropDown( { dataArray:opener.top.arrAirports , keyIndex:0 , valueIndex:2, firstEmpty:true });
	
		$("#fromAirport").sexyCombo();
		$("#toAirport").sexyCombo();
	}
	
	/*
	 * Expiry Date check
	 */ 
	UI_useCredit.expiryDateChk = function(){
		var strSep = "";
		if ($("#cardExpiry").val() != ""){
			var strVal = $("#cardExpiry").val();
			if (strVal.indexOf("/") != -1){strSep = "/";}
			if (strVal.indexOf("-") != -1){strSep = "-";}
			if (strVal.indexOf(".") != -1){strSep = ".";}
			$("#cardExpiry").val(dateChk("01" + strSep + strVal, "MM/YY"));
		}
	}
	/* ------------------------------------------------ end of File ---------------------------------------------- */