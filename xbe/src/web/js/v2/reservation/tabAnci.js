/*
 *********************************************************
    Description     : XBE Make Reservation - Ancillary Tab
    Authors         : Rilwan A. Latiff, Dilan Anuruddha
    Version         : 1.0
    Created         : 19th January 2010
    Last Modified   : 25th February 2010
 *********************************************************   
 */
/* Available Ancillary */

var strSsrNeeds =  geti18nData('ancillary_SSRNeeds','SSR Needs');
var strMeal = geti18nData('ancillary_MealRequest','Meal Request');
var strSeat = geti18nData('ancillary_SeatRequest','Seat Request');
var strAirportServices = geti18nData('ancillary_AirportServices','Airport Services');
var strTravelInsurance = geti18nData('ancillary_TravelInsurance','Travel Insurance');
var strBaggageRequest =	geti18nData('ancillary_BaggageRequest','Baggage Request');
var strAirportTransfer = geti18nData('ancillary_AirportTransfer','Airport Transfer');
var strFlexi = 	geti18nData('ancillary_FlexiRequest','Flexi Request');
var strAutomaticCheckin = geti18nData('ancillary_AutomaticCheckin','Automatic Checkin');
	
var strMealQty = geti18nData('ancillary_Qty','Qty');
var strPassengerName = geti18nData('ancillary_PassengerName','Passenger Name');
var strTicketPrice = geti18nData('ancillary_TicketPrice','Ticket Price');
var strFlexiCharge =  geti18nData('ancillary_FlexiCharge','Flexi Charge');
var strMealCharge = geti18nData('ancillary_MealCharge','Meal Charge');
var strPerReservation = geti18nData('ancillary_PerReservation','per Reservation');


var jsAnciSrv = [{
    id: 0,
    code: "SSR",
    desc: strSsrNeeds,
    imgOff: "LCAnsi00_A_no_cache.jpg",
    imgOn: "LCAnsi00_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_0",
    containerPaxInfo: "spnAnciPaxWise_0",
    onClick: "UI_tabAnci.onClickSSR();"
}, {
    id: 1,
    code: "MEAL",
    desc: strMeal,
    imgOff: "LCAnsi01_A_no_cache.jpg",
    imgOn: "LCAnsi01_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_1",
    containerPaxInfo: "spnAnciPaxWise_1",
    onClick: "UI_tabAnci.onClickMeal();"
}, {
    id: 2,
    code: "SEAT",
    desc: strSeat,
    imgOff: "LCAnsi02_A_no_cache.jpg",
    imgOn: "LCAnsi02_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_2",
    containerPaxInfo: "spnAnciPaxWise_2",
    onClick: "UI_tabAnci.onClickSeat();"
}, {
    id: 3,
    code: "APSERVICES",
    desc: strAirportServices,
    imgOff: "LCAnsi03_A_no_cache.jpg",
    imgOn: "LCAnsi03_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_3",
    containerPaxInfo: "spnAnciPaxWise_3",
    onClick: "UI_tabAnci.onClickApService();"
}, {
    id: 4,
    code: "INS",
    desc: strTravelInsurance,
    imgOff: "LCAnsi04_A_no_cache.jpg",
    imgOn: "LCAnsi04_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_4",
    containerPaxInfo: "spnAnciPaxWise_4",
    onClick: "UI_tabAnci.onClickInsurance();"
}, {
    id: 5,
    code: "BAGGAGE",
    desc: strBaggageRequest,
    imgOff: "LCAnsibag2_no_cache.jpg",
    imgOn: "LCAnsibag1_no_cache.jpg",
    containerInfo: "spnAnciContainer_5",
    containerPaxInfo: "spnAnciPaxWise_5",
    onClick: "UI_tabAnci.onClickBaggage();"
}, {
    id: 6,
    code: "APTRANSFER",
    desc: strAirportTransfer,
    imgOff: "LCAnsi06_A_no_cache.jpg",
    imgOn: "LCAnsi06_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_6",
    containerPaxInfo: "spnAnciPaxWise_6",
    onClick: "UI_tabAnci.onClickAirportTransfer();"
}, {
    id: 7,
    code: "FLEXI",
    desc: strFlexi,
    imgOff: "LCAnsi07_A_no_cache.jpg",
    imgOn: "LCAnsi07_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_7",
    containerPaxInfo: "spnAnciPaxWise_7",
    onClick: "UI_tabAnci.onClickFlexi();"
}, {
    id: 8,
    code: "AUTOMATICCHECKIN",
    desc: strAutomaticCheckin,
    imgOff: "LCAnsi07_A_no_cache.jpg",
    imgOn: "LCAnsi07_B_no_cache.jpg",
    containerInfo: "spnAnciContainer_8",
    containerPaxInfo: "spnAnciPaxWise_8",
    onClick: "UI_tabAnci.onClickAutoCheckin();"
}];

/* Segments */
var jsAnciSeg = [];
var jsCurrAnciSeg = [];

var jsAnciSegRefMap = {};

/* SSR Model */
var jsSSRModel = [];

/* SSR List */
var jsSSRList = [];

/* Auto Checkin Model */
var jsAutoCheckinModel = [];

/* Auto Checkin List */
var jsACIList = [];

/* Meal List */
var jsMealList = [];

/* Baggage List */
var jsBaggageList = [];

/* Airport Services List */
var jsAPserviceList = [];

/* Airport Transfer List */
var jsAPTransferList = [];

/* Meal per segment Model */
var jsMealModel = [];

/* Seat Map Segment Model */
var jsSMModel = null;

/* Occupied Seats */
var occupiedSeats = {};

/* Ancillary availability data */
var jsonSegAnciAvail = null;

/* Adult, Child PAX data */
var jsonPaxAdults = null;

/* Infant PAX data */
var jsonPaxInfants = null;

/* Baggage per segment Model */
var jsBaggageModel = [];

/* Autocheckin per segment Model */
var jsAutoCheckinList = [];

/* Calendar data */
var commonCalendarData = [];

var jsInfantAutoCheckinList = {};

var isONDBaggage = "N";
// to Load the logical cabin class map
UI_tabAnci.lccMap = {};

UI_tabAnci.fltWiseFreeSeatMap = {};
UI_tabAnci.fltWiseFreeMealMap = {};

UI_tabAnci.selectDefaultSeatsForPax = true;
UI_tabAnci.isBaggagesSelectedForCurrentBookingVal = false;

var ondBaggageObj = {};

var jsFlexiSeg = [];

var isFromFlexiTab = false;


/*
 * Ancillary
 */
function UI_tabAnci() {}
UI_tabAnci.blOnLoad = true;
UI_tabAnci.blInitialized = false;
UI_tabAnci.blPrefIniatilized = false;
UI_tabAnci.currFltRefNo = null;
UI_tabAnci.currSegRow = null;
UI_tabAnci.currAnciSrv = null;
UI_tabAnci.currPaxIDRow = null;
UI_tabAnci.jsonFareQuoteSummary = null;
UI_tabAnci.anciTotal = 0;
UI_tabAnci.anciTotalPrev = 0;
//UI_tabAnci.anciTotalChanged = false;
UI_tabAnci.modifiedSegemntCount = 0;
UI_tabAnci.anciOldTotal = 0;
UI_tabAnci.anciInsurances = null;
UI_tabAnci.insurableFltRefNumbers = null;
UI_tabAnci.multiMealEnabled = false;
UI_tabAnci.multiBaggageEnabled = false;
UI_tabAnci.focMealEnabled = false;
UI_tabAnci.isSsrRefundableInModification = false;
UI_tabAnci.flexiSelectionEnabledInAnciPage = false;
UI_tabAnci.winO = null;
UI_tabAnci.onHold = false;
UI_tabAnci.waitListing = false;
UI_tabAnci.anciNSFirst = null;
UI_tabAnci.seatsBlocked = false;
UI_tabAnci.blockSeats = false;
UI_tabAnci.tabStatus = false;
UI_tabAnci.seatMapAvailable = false;
UI_tabAnci.anciPreferenceMsgEnabled = false;
UI_tabAnci.baggageAvailable = false;
UI_tabAnci.baggageMandatory = false;
UI_tabAnci.baggageChargeOverride = false;
UI_tabAnci.modifyBaggageLoaded = false;
UI_tabAnci.allowEditAirportTransfers = true;
UI_tabAnci.freeServiceDecorator = "Free";
UI_tabAnci.continueToServiceTaxCalc = true;
UI_tabAnci.salesTaxForFare = 0;
UI_tabAnci.salesTaxForServices = 0;
UI_tabAnci.adminFee = 0;
UI_tabAnci.selectedCurrencyAdminFee = 0;

UI_tabAnci.paxConfigResponse = null;
UI_tabAnci.paxresPaxInfo = null;
UI_tabAnci.flightSegmentInfo = null;
UI_tabAnci.isAutoCheckinEnabled=false;
UI_tabAnci.arrError = [];

UI_tabAnci.anciTabID = {
    SSR: 0,
    MEAL: 1,
    SEAT: 2,
    APSERVICES: 3,
    INS: 4,
    BAGGAGE: 5,
    APTRANSFER: 6,
    FLEXI: 7,
    AUTOMATICCHECKIN: 8
};
UI_tabAnci.anciType = {
    SEAT: 'SEAT_MAP',
    MEAL: 'MEALS',
    SSR: 'SSR',
    INS: 'INSURANCE',
    APSERVICES: 'AIRPORT_SERVICE',
    BAGGAGE: 'BAGGAGES',
    APTRANSFER: 'AIRPORT_TRANSFER',
    FLEXI: 'FLEXI',
    AUTOMATICCHECKIN: 'AUTOMATIC_CHECKIN'
};
UI_tabAnci.promoAnciType = {
    SEAT_MAP: 'SEAT_MAP',
    MEALS: 'MEAL',
    SSR: 'INFLIGHT_SERVICES',
    INSURANCE: 'INSURANCE',
    AIRPORT_SERVICE: 'AIRPORT_SERVICE',
    BAGGAGES: 'BAGGAGE',
    AUTOMATICCHECKIN:'AUTOMATIC_CHECKIN'
}
UI_tabAnci.promoTypes = {
    SYSGEN: "SYSGEN",
    BUYNGET: "BUYNGET",
    DISCOUNT: "DISCOUNT",
    FREESERVICE: "FREESERVICE"
}
UI_tabAnci.PromoAppicablity = {
    RESERVATION: 3,
    PER_PAX: 4
}
UI_tabAnci.promoDiscountType = {
    PERCENTAGE: "PERCENTAGE",
    VALUE: "VALUE"
}
UI_tabAnci.promoDiscountAs = {
    MONEY: "Money",
    CREDIT: "Credit"
}
UI_tabAnci.ChargeType = {
    PER_PAX: 'P',
    PER_RESERVATION: 'R'
};

UI_tabAnci.AirportType = {
    DEPARTURE: 'D',
    ARRIVAL: 'A',
    TRANSIT: 'T'
};

UI_tabAnci.FltSegTitle = {
    SEGMENT: 'Segment',
    AIRPORT: 'Airport'
};

UI_tabAnci.anciSummary = [];

UI_tabAnci.dataStatus = {
    meal: false,
    seat: false,
    insurance: false,
    ssr: false,
    baggage: false,
    flexi: false,
    aptransfer: false,
	automaticCheckin: false
};
UI_tabAnci.formatDate = UI_commonSystem.dateFormatter('accelaero');
UI_tabAnci.isAnciModify = false;
UI_tabAnci.isSegModify = false;
UI_tabAnci.isRequote = false;
UI_tabAnci.atLeastOneSelected = false;
UI_tabAnci.contactInfo = null;
UI_tabAnci.flightSegmentList = [];
UI_tabAnci.seatRefMap = {};
UI_tabAnci.mealRefMap = {};
UI_tabAnci.baggageRefMap = {};
UI_tabAnci.ssrRefMap = {};
UI_tabAnci.apServiceRefMap = {};
UI_tabAnci.apTransferRefMap = {};
UI_tabAnci.modifySeatLoaded = false;
UI_tabAnci.modifyMealLoaded = false;
UI_tabAnci.modifySSRLoaded = false;
UI_tabAnci.modifyAPServiceLoaded = false;
UI_tabAnci.modifyAPTransferLoaded = false;
UI_tabAnci.isAddSegment = false;
UI_tabAnci.pnr = "";
UI_tabAnci.airportMessage = "";
UI_tabAnci.allowInfantInsurance = false;
UI_tabAnci.insContent = "";
UI_tabAnci.reprotect = {
    meal: false,
    seat: false,
    ssr: false,
    baggage: false,
    apService: false,
    aptransfer: false,
	automaticCheckin: false
};

UI_tabAnci.ondwiseFelexiSelected = {};
UI_tabAnci.isFlexiEnableInAnci = false;
UI_tabAnci.selectedAnciTypeGlobal = "";
UI_tabAnci.carrierWiseShowSSRInv = {};

UI_tabAnci.OND_SEQUENCE = {
    OUT_BOUND: 0,
    IN_BOUND: 1
};

UI_tabAnci.mealCategoryMap = {};

UI_tabAnci.resetAnciData = function () {

    UI_tabAnci.releaseBlockedSeats({
        reset: false,
        clearSavedAnci: false
    });
    UI_tabAnci.currFltRefNo = null;
    UI_tabAnci.currSegRow = null;
    UI_tabAnci.currAnciSrv = null;
    UI_tabAnci.currPaxIDRow = null;
    UI_tabAnci.jsonFareQuoteSummary = null;
    UI_tabAnci.anciTotal = 0;
    UI_tabAnci.anciTotalPrev = 0;
    //UI_tabAnci.anciTotalChanged = false;
    UI_tabAnci.anciOldTotal = 0;
    UI_tabAnci.anciInsurances = null;
    UI_tabAnci.insurableFltRefNumbers = null;
    UI_tabAnci.anciSummary = [];
    UI_tabAnci.dataStatus = {
        meal: false,
        seat: false,
        insurance: false,
        ssr: false,
        init: false,
        baggage: false,
        apservice: false,
        aptransfer: false,
		automaticCheckin: false
    };
    UI_tabAnci.anciNSFirst = null;
    UI_tabAnci.atLeastOneSelected = false;
    UI_tabAnci.seatRefMap = {};
    UI_tabAnci.mealRefMap = {};
    UI_tabAnci.baggageRefMap = {};
    UI_tabAnci.ssrRefMap = {};
	UI_tabAnci.autoCheckinFeildMap = {};
    UI_tabAnci.apServiceRefMap = {};
    UI_tabAnci.apTransferRefMap = {};
    UI_tabAnci.autoCheckinRefMap = {};
    UI_tabAnci.modifySeatLoaded = false;
    UI_tabAnci.modifyMealLoaded = false;
    UI_tabAnci.modifySSRLoaded = false;
    UI_tabAnci.modifyAPServiceLoaded = false;
    UI_tabAnci.modifyAPTransferLoaded = false;
    UI_tabAnci.modifyAutoCheckinLoaded = false;
    UI_tabAnci.reprotect = {
        meal: false,
        seat: false,
        ssr: false,
        baggage: false,
        apService: false,
        aptransfer: false,
		automaticCheckin: false
    };
    UI_tabAnci.carrierWiseShowSSRInv = {};

    jsAnciSeg = [];
    jsCurrAnciSeg = [];
    jsSSRModel = [];
    jsSSRList = [];
    jsMealList = [];
    jsBaggageList = [];
    jsMealModel = [];
    jsBaggageModel = []; // baggage
    jsAPServicesModel = []; // Airport service
    jsAPTransferModel = [];
    jsAutoCheckinList = [];
    commonCalendarData = [];
    jsInfantAutoCheckinList = {};
    occupiedSeats = {};
    jsSMModel = null;
    jsonSegAnciAvail = null;
    jsonPaxAdults = null;
    jsonPaxInfants = null;
    UI_tabAnci.continueToServiceTaxCalc = true;
    UI_tabAnci.salesTaxForFare = 0;
    UI_tabAnci.salesTaxForServices = 0;
    UI_tabAnci.mealCategoryMap = {};

}

/*
 * Get default currency
 */
UI_tabAnci.getDefaultCurrency = function () {
    if (UI_tabAnci.jsonFareQuoteSummary.currency != null) {
        return UI_tabAnci.jsonFareQuoteSummary.currency
    } else {
        return DATA_ResPro.initialParams.baseCurrencyCode;
    }
}

/*
 * Reset data on the block error
 */
UI_tabAnci.resetDataOnBlockError = function () {
    /* For Seats */
    UI_tabAnci.resetSeatData();

}

/*
 * Reset ancillary data structures
 */
UI_tabAnci.resetAnciStructures = function () {
    var resetFn = function (dataArr) {
        for (var i = 0; i < dataArr.length; i++) {
            for (var j = 0; j < dataArr[i].pax.length; j++) {
                var pax = dataArr[i].pax[j];
                pax.seat = {
                    seatNumber: '',
                    seatCharge: 0
                };
                pax.meal = {
                    meals: [],
                    mealChargeTotal: 0,
                    mealQtyTotal: 0
                };
                pax.baggage = {
                    baggages: [],
                    baggageChargeTotal: 0,
                    baggageQtyTotal: 0
                };
                pax.ssr = {
                    ssrs: [],
                    ssrChargeTotal: 0
                };
                pax.apt = {
                    apts: [],
                    aptChargeTotal: 0
                };
            }
        }
        return dataArr;
    }
    jsCurrAnciSeg = resetFn(jsCurrAnciSeg);
    jsAnciSeg = resetFn(jsAnciSeg);
    jsFlexiSeg = UI_tabAnci.createONDSegmentForFlexi(jsAnciSeg);
}
/*
 * Reset seat data
 */
UI_tabAnci.resetSeatData = function () {
    $.each(occupiedSeats, function (fltRefNo, occSeatObj) {
        $.each(occSeatObj.seats, function (seatNumber, seatObject) {
            if (seatObject != null) {
                UI_tabAnci.clearSeat(seatObject); // TODO check this
            }
        });
    });

}

/*
 * return SSR inventory check is enabled for the given carrier
 */
UI_tabAnci.isSSRInvCheckEnabled = function (carrier) {
    return UI_tabAnci.carrierWiseShowSSRInv[carrier];
}

/*
 * prepare ancillary according to the response received from the server
 */
UI_tabAnci.prepareAncillary = function (response, fareQuoteSummary) {
    if (response == null || !response.success) {
        showERRMessage(response.messageTxt);
        return;
    }
    UI_tabAnci.onHold = response.onHoldBooking;
    UI_tabAnci.waitListing = response.waitListingBooking;
    
    UI_tabAnci.jsonFareQuoteSummary = fareQuoteSummary;
    UI_tabAnci.multiMealEnabled = response.mulipleMealsSelectionEnabled;
	UI_tabAnci.adminFee = fareQuoteSummary.adminFee;
	UI_tabAnci.selectedCurrencyAdminFee =fareQuoteSummary.selectCurrAdminFee;
	
    // UI_tabAnci.multiBaggageEnabled =
    // response.mulipleBaggagesSelectionEnabled;
    UI_tabAnci.multiBaggageEnabled = true

    UI_tabAnci.focMealEnabled = response.focMealEnabled;
    UI_tabAnci.isSsrRefundableInModification = response.ssrRefundableInModification;
    UI_tabAnci.flexiSelectionEnabledInAnciPage = response.flexiSelectionEnabledInAnciPage;
    UI_tabAnci.carrierWiseShowSSRInv = response.carrierWiseSSRInvCheck;
    UI_tabAnci.lccMap = response.lccMap;
    UI_tabAnci.anciPreferenceMsgEnabled = response.anciPreferenceMsgEnabled;
    UI_tabAnci.isBundleFarePopupEnabled = response.bundleFarePopupEnabled;

	//Added Toggle Feature for AutoCheckin
	UI_tabAnci.isAutoCheckinEnabled=response.autoCheckinAvailable;
	UI_tabAnci.paxConfigResponse = response.autoCheckinPaxConfig;
	UI_tabAnci.paxresPaxInfo = response.resPaxInfo;
	UI_tabAnci.flightSegmentInfo = response.availabilityOutDTO;
	UI_tabAnci.arrError = top.arrError;
    var showTab;

    if (!UI_tabAnci.dataStatus.init) {

        UI_tabAnci.dataStatus.init = true;

        jsonSegAnciAvail = response.availabilityOutDTO;

        UI_tabAnci.fltWiseFreeSeatMap = {};
        UI_tabAnci.fltWiseFreeMealMap = {};
        UI_tabAnci.ondwiseFelexiSelected = {};
        
        $
            .each(
                jsonSegAnciAvail,
                function (i, obj) {
                    var fltSeg = obj.flightSegmentTO;
                    var isSeatAvailable = UI_tabAnci.isAnciAvailable(
                        obj.ancillaryStatusList,
                        UI_tabAnci.anciType.SEAT);
                    var isMealAvailable = UI_tabAnci.isAnciAvailable(
                        obj.ancillaryStatusList,
                        UI_tabAnci.anciType.MEAL);
                    if (!UI_tabAnci.isFlexiEnableInAnci) {
                        UI_tabAnci.isFlexiEnableInAnci = UI_tabAnci.isAnciAvailable(
                            obj.ancillaryStatusList,
                            UI_tabAnci.anciType.FLEXI);
                    }
                    UI_tabAnci.fltWiseFreeSeatMap[fltSeg.flightRefNumber] = isSeatAvailable && fltSeg.freeSeatEnabled;
                    UI_tabAnci.fltWiseFreeMealMap[fltSeg.flightRefNumber] = isMealAvailable && fltSeg.freeMealEnabled;
                });

        jsonSegAnciAvail = $.airutil.sort.quickSort(jsonSegAnciAvail,
            UI_tabAnci.segmentComparator);
        jsonPaxAdults = eval(response.paxAdults);
        jsonPaxInfants = eval(response.paxInfants);

        if (jsonPaxAdults == null) {
            jsonPaxAdults = [];
        }
        if (jsonPaxInfants == null) {
            jsonPaxInfants = [];
        }
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            jsonPaxAdults[i].paxId = i;
            jsonPaxAdults[i].name = jsonPaxAdults[i].displayAdultFirstName + ' ' + jsonPaxAdults[i].displayAdultLastName;
            var paxType = jsonPaxAdults[i].displayAdultType;
            if (jsonPaxAdults[i].displayInfantTravelling == 'Y') {
                jsonPaxAdults[i].name += ' /INF';
                paxType = 'PA';
            }
            jsonPaxAdults[i].fn = jsonPaxAdults[i].displayAdultFirstName;
            jsonPaxAdults[i].ln = jsonPaxAdults[i].displayAdultLastName;
            jsonPaxAdults[i].dob = jsonPaxAdults[i].displayAdultDOB;
            jsonPaxAdults[i].type = paxType;
            jsonPaxAdults[i].anci = [];
            jsonPaxAdults[i].removeAnci = [];
            jsonPaxAdults[i].total = {
                credit: 0,
                charges: 0
            };
            jsonPaxAdults[i].ins = {
                ins: false,
                charge: 0
            };
        }

        
        if (UI_tabAnci.anciInsurances == null) {
        	UI_tabAnci.insurableFltRefNumbers = null;
        	UI_tabAnci.anciInsurances = new Array();
        	
              var anciInsurance = {
                total: 0,
                selected: false,
                insuranceRefNumber: null,
                operatingAirline: null,
                insuredJourney: null,
                quotedTotalPremiumAmount: 0
            };
              UI_tabAnci.anciInsurances.push(anciInsurance); 
        }
        
        
        UI_tabAnci.anciSummary.length = 0;
        jsAnciSeg.length = 0;
        jsCurrAnciSeg.length = 0;
        occupiedSeats = {};
        $('#chkAnciIns').attr('checked', false);
        /*
         * populate segment structure and seat occupied structure
         */
		jsAutoCheckinList = {
			passportNo: {
				visibility : false
			},
			passportExpiry: {
				visibility : false
			},
			passportIssuedCntry: {
				visibility : false
			},
			placeOfBirth:{
				visibility : false
			},
			dob: {
				visibility : false
			},
			travelDocumentType: {
				visibility : false
			},
			visaDocNumber: {
				visibility : false
			},
			visaDocIssueDate: {
				visibility : false
			},
			visaDocPlaceOfIssue: {
				visibility : false
			},
			visaApplicableCountry: {
				visibility : false
			}
		}
		
		jsInfantAutoCheckinList.recent = jsAutoCheckinList;
		jsInfantAutoCheckinList.global = $.airutil.dom.cloneObject(jsAutoCheckinList);
		
        for (var i = 0; i < jsonSegAnciAvail.length; i++) {
            var seg = jsonSegAnciAvail[i].flightSegmentTO;
            var hasAnciSeat = UI_tabAnci.isAnciAvailable(
                jsonSegAnciAvail[i].ancillaryStatusList,
                UI_tabAnci.anciType.SEAT);
            var fltRefNo = seg.flightRefNumber;
            var ddt = null;
            if (fltRefNo.indexOf('$') > 0) {
                ddt = UI_tabAnci._getFltRefDate(fltRefNo.split('$')[3])
                    .getTime();
            } else {
                ddt = UI_tabAnci._getDate(seg.depatureDateTime).getTime();
            }
            jsAnciSeg.push({
                segID: seg.segmentCode,
                orignNDest: seg.segmentCode,
                ddt: ddt,
                flightNumber: seg.flightNumber,
                fltRefNo: fltRefNo,
                flightRefNumber: seg.flightRefNumber,
                cabinClassCode: seg.cabinClassCode,
                logicalCabinClass: seg.logicalCabinClassCode,
                freeMealEnabled: seg.freeMealEnabled,
                freeSeatEnabled: seg.freeSeatEnabled,
                returnFlag: seg.returnFlag,
                pnrSegId: seg.pnrSegId,
                ondSequence: seg.ondSequence,
                pax: []
            });
            jsCurrAnciSeg.push({
                segID: seg.segmentCode,
                orignNDest: seg.segmentCode,
                ddt: ddt,
                flightNumber: seg.flightNumber,
                fltRefNo: fltRefNo,
                flightRefNumber: seg.flightRefNumber,
                cabinClassCode: seg.cabinClassCode,
                logicalCabinClass: seg.logicalCabinClassCode,
                freeMealEnabled: seg.freeMealEnabled,
                freeSeatEnabled: seg.freeSeatEnabled,
                ondSequence: seg.ondSequence,
                pax: []
            });
            /* add adults and childs into pax array */
            for (var j = 0; j < jsonPaxAdults.length; j++) {
                jsAnciSeg[jsAnciSeg.length - 1].pax[j] = {
                    paxType: jsonPaxAdults[j].type,
                    refI: j,
                    seat: {
                        seatNumber: '',
                        seatCharge: 0
                    },
                    meal: {
                        meals: [],
                        mealChargeTotal: 0,
                        mealQtyTotal: 0
                    },
                    baggage: {
                        baggages: [],
                        baggageChargeTotal: 0,
                        baggageQtyTotal: 0
                    },
                    ssr: {
                        ssrs: [],
                        ssrChargeTotal: 0
                    },
                    aps: {
                        apss: [],
                        apsChargeTotal: 0
                    },
                    apt: {
                        apts: [],
                        aptChargeTotal: 0
                    },
                    extraSeats: {
                        extraSeats: []
                    },
                    automaticCheckin: {
                        autoCheckinTemplateId: 0,
                        seatPreference: '',
                        seatCode: '',
                        autoCheckinCharge:0,
                        email:''
                    }
                };
                jsCurrAnciSeg[jsCurrAnciSeg.length - 1].pax[j] = {
                    paxType: jsonPaxAdults[j].type,
                    refI: j,
                    seat: {
                        seatNumber: '',
                        seatCharge: 0
                    },
                    meal: {
                        meals: [],
                        mealChargeTotal: 0,
                        mealQtyTotal: 0
                    },
                    baggage: {
                        baggages: [],
                        baggageChargeTotal: 0,
                        baggageQtyTotal: 0
                    },
                    returnFlag: seg.returnFlag,
                    ssr: {
                        ssrs: [],
                        ssrChargeTotal: 0
                    },
                    aps: {
                        apss: [],
                        apsChargeTotal: 0
                    },
                    apt: {
                        apts: [],
                        aptChargeTotal: 0
                    },
                    extraSeats: {
                        extraSeats: []
                    },
                    automaticCheckin: {
                        autoCheckinTemplateId: 0,
                        seatPreference: '',
                        seatCode: '',
                        autoCheckinCharge:0,
                        email:''
                    }
                };
            }

            jsAnciSegRefMap[fltRefNo] = {
                index: i
            };
            /*
             * add infants into pax array Available for SSR only for the moment
             */
            /*
             * for(var j=jsonPaxAdults.length; j <
             * (jsonPaxInfants.length+jsonPaxAdults.length); j++){ var ind = j -
             * jsonPaxAdults.length;
             * jsAnciSeg[jsAnciSeg.length-1].pax[j]={paxType:'PA',refI : ind,
             * seat:{seatNumber:'',seatCharge:0},meal:{meals:[],mealChargeTotal:0,mealQtyTotal:0},ssr:{ssrs:[],ssrChargeTotal:0}}; }
             */
            occupiedSeats[fltRefNo] = {
                seats: {},
                available: hasAnciSeat
            };
        }

        jsFlexiSeg = UI_tabAnci.createONDSegmentForFlexi(jsAnciSeg);
        
        var anciSummary = UI_tabAnci.anciSummary;

        var servStat = 'INA';

        UI_tabAnci.seatMapAvailable = response.seatMapAvailable;

        if (!response.seatMapAvailable) {
            $('#imgTab_2').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_2').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.SEAT,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 2,
            name: strSeat
        };

        if (!response.mealsAvailable) {
            $('#imgTab_1').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_1').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.MEAL,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 1,
            name: strMeal
        };

		if (!response.autoCheckinAvailable) {
            $('#imgTab_8').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_8').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.AUTOMATICCHECKIN,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 8,
            name: 'Autocheckin Request'
        };

        /**
         * For bagggage
         */

        UI_tabAnci.baggageAvailable = response.baggageAvailable;
        UI_tabAnci.baggageMandatory = response.baggageMandatory;
        UI_tabAnci.baggageChargeOverride = response.baggageChargeOverride;
        if (!response.baggageAvailable) {
            $('#imgTab_5').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_5').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.BAGGAGE,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 5,
            name: strBaggageRequest
        };

        // end baggage

        if (!response.insuranceAvailable) {
            $('#imgTab_4').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_4').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.INS,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 4,
            name: strTravelInsurance
        };

        if (!response.ssrAvailable) {
            $('#imgTab_0').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_0').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.SSR,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 0,
            name: strSsrNeeds
        };

        if (!response.airportServiceAvailable) {
            $('#imgTab_3').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_3').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.APSERVICES,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 3,
            name: strAirportServices
        };

        if (UI_tabAnci.onHold || !response.airportTransferAvailable) {
            $('#imgTab_6').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        } else {
            $('#imgTab_6').show();
            servStat = 'ACT';
        }
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.APTRANSFER,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 6,
            name: 'Airport Transfer'
        };
        
        if (!response.flexiSelectionEnabledInAnciPage) {
            $('#imgTab_7').hide();
            $('#spnAnciContainer_0').hide();
            servStat = 'INA';
        }        
        
        anciSummary[anciSummary.length] = {
            type: UI_tabAnci.anciType.FLEXI,
            subTotal: 0,
            qty: 0,
            old: {
                subTotal: 0,
                qty: 0
            },
            stat: servStat,
            tab: 7,
            name: 'Flexi'
        };
    } else {
        var paxAdultNew = eval(response.paxAdults);
        var resetSeatSelection = false;
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            jsonPaxAdults[i].name = paxAdultNew[i].displayAdultFirstName + ' ' + paxAdultNew[i].displayAdultLastName;

            if (paxAdultNew[i].displayInfantTravelling != jsonPaxAdults[i].displayInfantTravelling) {
                resetSeatSelection = true;
            }
            var paxType = jsonPaxAdults[i].displayAdultType;
            if (paxAdultNew[i].displayInfantTravelling == 'Y') {
                jsonPaxAdults[i].name += ' /INF';
                paxType = 'PA';
            }
            jsonPaxAdults[i].type = paxType;
            jsonPaxAdults[i].fn = paxAdultNew[i].displayAdultFirstName;
            jsonPaxAdults[i].ln = paxAdultNew[i].displayAdultLastName;
            jsonPaxAdults[i].dob = paxAdultNew[i].displayAdultDOB;

            jsonPaxAdults[i].displayInfantTravelling = paxAdultNew[i].displayInfantTravelling;
            jsonPaxAdults[i].displayAdultFirstName = paxAdultNew[i].displayAdultFirstName;
            jsonPaxAdults[i].displayAdultLastName = paxAdultNew[i].displayAdultLastName;
            jsonPaxAdults[i].displayAdultType = paxAdultNew[i].displayAdultType;
            jsonPaxAdults[i].displayETicket = paxAdultNew[i].displayETicket;
            jsonPaxAdults[i].displayPnrPaxCatFOIDNumber = paxAdultNew[i].displayPnrPaxCatFOIDNumber;
            jsonPaxAdults[i].displayNationalIDNo = paxAdultNew[i].displayNationalIDNo;
            jsonPaxAdults[i].displayAdultTitle = paxAdultNew[i].displayAdultTitle;
            jsonPaxAdults[i].displayAdultNationality = paxAdultNew[i].displayAdultNationality;
            jsonPaxAdults[i].displayInfantWith = paxAdultNew[i].displayInfantWith;
            jsonPaxAdults[i].displayAdultDOB = paxAdultNew[i].displayAdultDOB;
            jsonPaxAdults[i].displayFFID = paxAdultNew[i].displayFFID;

        }
        if (resetSeatSelection) {
            UI_tabAnci.releaseBlockedAnci();
        }
    }

    if (!UI_tabAnci.blPrefIniatilized) {
        UI_tabAnci.blPrefIniatilized = true;
        $('#btnAnciPrefToAnci').decorateButton();
        $('#btnAnciPrefToPayment').decorateButton();
        $('#btnAnciPrefCancel').decorateButton();

        $('#btnAnciPrefCancel').click(function () {
            UI_commonSystem.hideProgress();
            $('#divAnciPref').hide();
        });

        $('#btnAnciPrefToAnci').click(function () {
            var t = [];
            $('#divAnciPref input:checked').each(function () {
                t[t.length] = parseInt($(this).val(), 10);
            });
            showTab = t[0];
            UI_tabAnci.navigateToAnciTab(showTab);
        });

        $('#btnAnciPrefToPayment').click(function () {
            UI_commonSystem.hideProgress();
            UI_tabAnci.tabStatus = false;
            UI_tabAnci.resetAnciStructures();
            UI_tabAnci.releaseBlockedSeats({
                reset: true,
                clearSavedAnci: true
            });
            $('#divAnciPref').hide();
            UI_tabAnci.submitSeatsForBlocking(false);
        });
    }

    if (UI_tabAnci.isAnciModify) {
        UI_tabAnci.tabStatus = true;
        UI_tabAnci.flightSegmentList = response.flightSegmentList;
        UI_tabAnci.contactInfo = response.contactInfo;
    } else if (UI_tabAnci.isAddSegment || UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
        UI_tabAnci.contactInfo = response.contactInfo;
        UI_tabAnci.isAnciModify = false;
    } else {
        UI_tabAnci.isAnciModify = false;
    }

    if (UI_tabAnci.showFirstEnabledAnci()) {
        $('#divAnciPref').hide();
        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.loadDataForReprotectAnci();
            var page = "";

            if (UI_tabAnci.isSegModify) {
                page = UI_modifySegment;
            } else if (UI_tabAnci.isRequote) {
                page = UI_modifyResRequote;
            }

            page.tabDataRetrive.tab1 = true;
            page.tabStatus.tab1 = true;
            page.openTab(1);
        } else if (UI_tabAnci.isAnciModify) {
            UI_modifyAncillary.tabDataRetrive.tab0 = true;
            UI_modifyAncillary.tabStatus.tab0 = true;
            UI_modifyAncillary.openTab(0);
        }
        UI_tabAnci.selectFirstActiveAncillaryTab();
    } else {
        UI_commonSystem.showProgress();

        if (UI_tabAnci.baggageAvailable && !UI_tabAnci.anciPreferenceMsgEnabled) {
            UI_tabAnci.navigateToAnciTab(UI_tabAnci.anciTabID.BAGGAGE);
        } else if (UI_tabAnci.seatMapAvailable && !UI_tabAnci.anciPreferenceMsgEnabled) {
            UI_tabAnci.navigateToAnciTab(UI_tabAnci.anciTabID.SEAT);
        } else {
            // To keep ancillary selection for waitlisting in future we are
            // calling anci actions also
            if (!response.waitListingBooking) {
                $('#divAnciPref').show();
                UI_tabAnci.anciPrefPopulate();
            } else {
                UI_commonSystem.hideProgress();
                UI_tabAnci.tabStatus = false;
                UI_tabAnci.resetAnciStructures();
                UI_tabAnci.releaseBlockedSeats({
                    reset: true,
                    clearSavedAnci: true
                });
                UI_tabAnci.submitSeatsForBlocking(false);
            }
        }
    }

}

UI_tabAnci.navigateToAnciTab = function (anciTabID) {
    UI_commonSystem.hideProgress();
    UI_tabAnci.tabStatus = true;
    $('#divAnciPref').hide();
    if (UI_tabAnci.isSegModify) {
        UI_modifySegment.tabDataRetrive.tab1 = true;
        UI_modifySegment.tabStatus.tab1 = true;
        UI_modifySegment.openTab(1);
    } else if (UI_tabAnci.isAddSegment) {
        UI_addSegment.tabStatus.tab1 = true;
        UI_addSegment.openTab(1);
    } else if (UI_tabAnci.isAnciModify) {
        // skip tab selection
    } else if (UI_tabAnci.isRequote) {
        UI_modifyResRequote.tabDataRetrive.tab1 = true;
        UI_modifyResRequote.tabStatus.tab1 = true;
        UI_modifyResRequote.openTab(1);
    } else {
        UI_makeReservation.tabDataRetrive.tab3 = false;
        UI_makeReservation.tabStatus.tab2 = true;
        UI_makeReservation.openTab(2);
    }
    UI_tabAnci.ready(anciTabID);
}

UI_tabAnci.loadDataForReprotectAnci = function () {

    var anciSum = UI_tabAnci.anciSummary;
    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].stat == 'ACT') {
            if (anciSum[i].type == UI_tabAnci.anciType.MEAL && !UI_tabAnci.reprotect.meal) {
                UI_tabAnci.onClickMeal({
                    initLayout: false
                });
            } else if (anciSum[i].type == UI_tabAnci.anciType.SEAT && !UI_tabAnci.reprotect.seat) {
                UI_tabAnci.onClickSeat({
                    initLayout: false
                });
            } else if (anciSum[i].type == UI_tabAnci.anciType.SSR && !UI_tabAnci.reprotect.ssr) {
                UI_tabAnci.onClickSSR({
                    initLayout: false
                });
            } else if (anciSum[i].type == UI_tabAnci.anciType.BAGGAGE && !UI_tabAnci.reprotect.baggage) {
                UI_tabAnci.onClickBaggage({
                    initLayout: false
                });
            } else if (anciSum[i].type == UI_tabAnci.anciType.APSERVICES && !UI_tabAnci.reprotect.apservice) {
                UI_tabAnci.onClickApService({
                    initLayout: false
                });
            }else if (anciSum[i].type == UI_tabAnci.anciType.AUTOMATICCHECKIN && !UI_tabAnci.reprotect.automaticCheckin) {
                UI_tabAnci.onClickAutoCheckin({
                    initLayout: false
                });
            }
        }
    }
}

UI_tabAnci.showFirstEnabledAnci = function () {
    if (UI_tabAnci.isAnciModify) {
        return true;
    } else if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
        var anciSummary = UI_tabAnci.anciSummary;
        var leastOneActive = false;
        for (var i = 0; i < anciSummary.length; i++) {
            if (anciSummary[i].stat == 'ACT') {
                leastOneActive = true;
                break;
            }
        }
        if (!leastOneActive) {
            return false;
        }
        var paxList = jsonPaxAdults;
        for (var i = 0; i < paxList.length; i++) {
            var pax = paxList[i];
            if (pax.currentAncillaries != null) {
                for (var k = 0; k < pax.currentAncillaries.length; k++) {

                    var seat = pax.currentAncillaries[k].airSeatDTO;
                    var meal = pax.currentAncillaries[k].mealDTOs;
                    var ssr = pax.currentAncillaries[k].specialServiceRequestDTOs;
					var autoCheckin = pax.currentAncillaries[k].automaticCheckinDTOs;
                    if (seat != null && seat.seatNumber != null && seat.seatNumber != '') {
                        return true;
                    }
                    if (meal != null && meal.length > 0) {
                        return true;
                    }
					if(autoCheckin !=null && autoCheckin[0] != null && autoCheckin[0].seatPref != null
							&& autoCheckin[0].seatPref != ''){
						return true;
					}
                    if (ssr != null && ssr.length > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    return false;
}

UI_tabAnci.selectFirstActiveAncillaryTab = function () {
    var anciSum = UI_tabAnci.anciSummary;
    var sel = null;
    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].stat == 'ACT') {
            sel = anciSum[i];
            break;
        }
    }

    var showTab = null;

    if (UI_tabAnci.baggageAvailable) {
        showTab = UI_tabAnci.anciTabID.BAGGAGE;
    } else if (UI_tabAnci.seatMapAvailable) {
        showTab = UI_tabAnci.anciTabID.SEAT;
    } else if (sel != null) {
        showTab = sel.tab;
    }

    UI_tabAnci.ready(showTab);
}
/*
 * Populate modify ancillary data
 */
UI_tabAnci.populateModifySeatData = function () {
    for (var j = 0; j < jsonPaxAdults.length; j++) {
        var anciSegs = jsonPaxAdults[j].currentAncillaries;
        var dup = {};
        for (var k = 0; k < anciSegs.length; k++) {
            var anciSeg = anciSegs[k];
            var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
            if (anciSeg.airSeatDTO != null && anciSeg.airSeatDTO.seatNumber != null && anciSeg.airSeatDTO.seatNumber != '' && dup[fltRefNo] == null) {
                dup[fltRefNo] = fltRefNo;
                if (occupiedSeats[fltRefNo] != null && occupiedSeats[fltRefNo].available) {
                    var seats = occupiedSeats[fltRefNo].seats;
                    var seatNumber = anciSeg.airSeatDTO.seatNumber;
                    var seatRefObj = {
                        seatNumber: seatNumber,
                        fltRefNo: fltRefNo
                    };
                    var seatObj = UI_tabAnci.getSeatObj(seatRefObj);

                    var paxSeatObj = {
                        paxId: j,
                        paxType: jsonPaxAdults[j].paxType,
                        refObj: seatRefObj,
                        old: true
                    };
                    if (!UI_tabAnci.modifySeatLoaded) {
                        UI_tabAnci.updateSeatAvailRow(paxSeatObj, 'INC');
                        UI_tabAnci.markSeat(paxSeatObj);
                    }

                    if (UI_tabAnci.compareFlightRefNo(UI_tabAnci.currFltRefNo,
                        fltRefNo)) {
                        UI_tabAnci.reBuildPaxSelectedSeat(seatRefObj);
                    }
                }
            }
        }
    }
    UI_tabAnci.modifySeatLoaded = true;
}

UI_tabAnci.reBuildPaxSelectedSeat = function (seatRefObj) {
    var fltRefNo = seatRefObj.fltRefNo;
    var seatNumber = seatRefObj.seatNumber;

    var refObj = '{fltRefNo:\"' + fltRefNo + '\",seatNumber:\"' + seatNumber + '\"}';
    var seatHtml = '<p class="" style="font-size: 9px;"><a href="#"' + "onmouseover='UI_tabAnci.seatOnMouseOver(event," + refObj + ")'" + "onmouseout='UI_tabAnci.seatOnMouseOut(" + refObj + ")' onclick='UI_tabAnci.seatOnClick(" + refObj + ")'" + ' class="txtSmall noTextDec">&nbsp;&nbsp;&nbsp;&nbsp;</a></p>';
    $("#tdSeat_" + seatNumber).html(seatHtml);
    $("#tdSeat_" + seatNumber).removeClass("notAvailSeat");
    $("#tdSeat_" + seatNumber).addClass("availSeat");
}

/*
 * Ancillary selection preference options population
 */
UI_tabAnci.anciPrefPopulate = function () {
    var anciSum = UI_tabAnci.anciSummary;
    var firstElement = null;
    $('#trAnciPrefTemp').parent().find('tr').filter(function () {
        var regex = new RegExp('trAnciPrefd_*');
        return regex.test(this.id);
    }).remove();
    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].stat == 'ACT') {
            var tmpId = 'trAnciPrefd_' + anciSum[i].tab;
            var chId = 'chkAnciPref_' + anciSum[i].tab;

            var clone = $('#trAnciPrefTemp').clone().show();
            clone.attr('id', tmpId);
            clone.appendTo($('#trAnciPrefTemp').parent());

            var chBox = $('#' + tmpId + ' input');
            chBox.attr('id', chId);
            chBox.attr('tabIndex', 9000 + i);
            chBox.val(anciSum[i].tab);
            chBox.click(function () {
                var t = $('#divAnciPref input:checked');
                if (t != null && t.length > 0)
                    $('#btnAnciPrefToAnci').enable();
                else
                    $('#btnAnciPrefToAnci').disable();

            });

            if (firstElement == null) {
                firstElement = chId;
                chBox.attr('checked', true);
            }

            var label = $('#' + tmpId + ' label');
            label.attr('id', 'lblAnciPref_' + anciSum[i].tab);
            label.text(anciSum[i].name)
            label.click(function (e) {
                var a = e.target.id.split('_');
                $('#chkAnciPref_' + a[1]).click()
            });

        }
    }
    $('#btnAnciPrefToAnci').decorateButton();
    $('#btnAnciPrefToPayment').decorateButton();
    $('#btnAnciPrefToAnci').disable();
    if (firstElement != null) {
        $('#' + firstElement).focus();
        $('#trAncillaryOpt').show();
        $('#trNoAncillary').hide();
        $('#btnAnciPrefToAnci').show();
        $('#trAncillaryCntMsg').show();
        $('#btnAnciPrefToPayment').text('No');
        var t = $('#divAnciPref input:checked');
        if (t != null && t.length > 0)
            $('#btnAnciPrefToAnci').enable();
        else
            $('#btnAnciPrefToAnci').disable();
    } else {
        $('#btnAnciPrefToAnci').hide();
        $('#trAncillaryOpt').hide();
        $('#trAncillaryCntMsg').hide();
        $('#trNoAncillary').hide();
        // $('#btnAnciPrefToPayment').text('Ok');
        // $('#trNoAncillary').show();
        $('#divAnciPref').hide();
        UI_commonSystem.hideProgress();
        UI_tabAnci.tabStatus = false;
        UI_tabAnci.resetAnciStructures();
        UI_tabAnci.releaseBlockedSeats({
            reset: true,
            clearSavedAnci: true
        });
        UI_tabAnci.submitSeatsForBlocking(false);
    }
    $('#trAnciPrefTemp').hide();
}

/*
 * ANCI service On Load Method
 */
UI_tabAnci.ready = function (showTab) {
    // RAK release last minute changes, to change air airarabia text.
    if (carrierCode == "G9" || carrierCode == "3O" || carrierCode == "EG") {
        $("#lblInsCost")
            .html("Travel insurance by Air Arabia for this trip cost ");
        $("#lblTermsNCond")
            .html("Click here to view Air Arabia Travel Insurance ");
    } else if (carrierCode == "RT") {
        $("#lblInsCost").html("Travel Insurance cost ");
        $("#lblAddInsConf").html(
            "Yes, Traveler like to purchase Travel Insurance.");
        $("#lblTermsNCond").html(
            "Click here to view the Rak Airways Terms and Conditions ");
    } else if (carrierCode == "Z2") {
        $("#lblInsCost")
            .html(
                "Sir / Ma’am, I would like to inform you about Zest Air Essential Travel Insurance that you may include in your itinerary. <br /> Zest Air Essential Travel Insurance is a group travel insurance policy entered into by Zest Air with Insurance Company of North America (ACE Insurance), which is especially designed for Zest Air passengers. <br /> The premium price is only ");
        $("#lblTermsNCond").html("Click here to view Travel Secure ");
    } else {
        $("#lblInsCost").html("Travel insurance for this trip cost ");
        $("#lblTermsNCond").html("Click here to view Travel Insurance ");
    }
    if (!UI_tabAnci.blInitialized) {
        UI_tabAnci.blInitialized = true;
        $("#btnAnciCancel").decorateButton();
        // if(!UI_tabAnci.isAnciModify){ // && !UI_tabAnci.isSegModify &&
        // !UI_tabAnci.isAddSegment) {
        $("#btnAnciView").show();
        $('#divAnciContConfirm').hide();
        $('#divAnciGSTConfirm').hide();
        $("#btnAnciView").decorateButton();
        $("#btnAnciContinue").decorateButton();
        // $('#btnAnciSummaryClose').decorateButton();
        $("#btnAnciContOk").decorateButton();
        $('#btnAnciContNo').decorateButton();
        $('#btnAnciGSTContOk').decorateButton();
        
        $('#btnAnciContOk').click(
            function () {
            	
            	UI_commonSystem.hideProgress();
            	$('#divAnciContConfirm').hide();
            	UI_tabAnci.continueToServiceTaxCalc = true;
            	/*if((jsonFareQuoteSummary.totalServiceTax != "")&& UI_tabAnci.anciTotalChanged){
            		$('#divAnciContConfirm').hide();
            		UI_tabAnci.anciTotalChanged = false;
            		UI_tabAnci.anciContinueConfirm();//////////*********
            	}*/
            	
                 if (UI_tabAnci.isAnciModify) {
                	 $('#divAnciContConfirm').hide();
                     if (UI_tabAnci.atLeastOneSelected || UI_tabAnci.totalBalanceToPay > 0) {
                         UI_tabAnci.confirmAncillaryModification();
                     } else {
                         UI_modifyAncillary.loadHomePage();
                     }
                 } else {
                     UI_tabAnci.submitSeatsForBlocking(UI_tabAnci.blockSeats && (!UI_tabAnci.seatsBlocked));
                 }
            });
        
        $('#btnAnciContNo').click(function () {
            UI_commonSystem.hideProgress();
            $('#divAnciContConfirm').hide();
            if (UI_tabAnci.anciNSFirst != null) {
                UI_tabAnci.tabClick({
                    id: UI_tabAnci.anciNSFirst.tab
                });
            }
        });

        
        $('#btnAnciGSTContOk').click(function () {
        	$('#divAnciGSTConfirm').hide();
        });
        
        $('#btnFreeAnciOk').decorateButton();
        $('#btnFreeAnciOk').click(function () {
            UI_commonSystem.hideProgress();
            $('#divFreeAnciNotification').hide();
        });

        $("#btnAnciCancel").click(function () {
            $("#btnAnciCancel").blur();
            UI_tabAnci.anciCancelOnClick();
        });
        $("#btnAnciView").click(function () {
            UI_tabAnci.anciViewOnClick();
        });
        
        $("#btnAnciContinue").click(function () {
        	if(UI_tabAnci.isAutoCheckinEnabled && !UI_tabAnci.isAnciModify){
        		UI_tabAnci.checkSeatCodeForAutoCheckin();
        	}
        	if((jsonFareQuoteSummary.totalServiceTax != "" || top.gstAppliedForReservation) && UI_tabAnci.continueToServiceTaxCalc){
        		UI_tabAnci.anciContinueOnClick();
        	}else{
        		UI_tabAnci.anciContinueConfirm();
        	}
        });
        
        $("#btnAnciResetSeat").click(function () {
            UI_tabAnci.resetSeatPax();
        });

		$("#btnAnciAutoCheckinApply").decorateButton();
		$("#btnAnciAutoCheckinReset").decorateButton();

		$("#btnAnciAutoCheckinApply").click(function () {
			$("#btnAnciAutoCheckinApply").blur();
			UI_tabAnci.anciAutoCheckinApplyOnClick();
		});
        
		$("#btnAnciAutoCheckinReset").click(function () {
			$("#btnAnciAutoCheckinReset").blur();
			UI_tabAnci.anciAutoCheckinResetOnClick();
		});
 
        $("#btnAnciSSRApply").decorateButton();
        $("#btnAnciSSRApplyToAll").decorateButton();
        $("#btnAnciSSRApply").click(function () {
            $("#btnAnciSSRApply").blur();
            UI_tabAnci.anciSSRApplyOnClick();
        });
        $("#btnAnciSSRApplyToAll").click(function () {
            $("#btnAnciSSRApplyToAll").blur();
            UI_tabAnci.anciSSRApplyToAllOnClick();
        });

        // Airport service apply & apply all
        $("#btnAnciAPServiceApply").decorateButton();
        $("#btnAnciAPServiceApplyToAll").decorateButton();
        $("#btnAnciAPServiceApply").click(function () {
            $("#btnAnciAPServiceApply").blur();
            UI_tabAnci.anciAPServiceApplyOnClick();
        });
        $("#btnAnciAPServiceApplyToAll").click(function () {
            $("#btnAnciAPServiceApplyToAll").blur();
            UI_tabAnci.anciAPServiceApplyToAllOnClick();
        });

        $("#btnAnciAPTransferApply").decorateButton();
        $("#btnAnciAPTransferApplyToAll").decorateButton();
        $("#btnAnciAPTransferApply").click(function () {
            $("#btnAnciAPTransferApply").blur();
            UI_tabAnci.anciAirportTransferApplyOnClick();
        });
        $("#btnAnciAPTransferApplyToAll").click(function () {
            $("#btnAnciAPTransferApplyToAll").blur();
            UI_tabAnci.anciAirportTransferApplyToAllOnClick();
        });

        $("#btnAnciMealApply").decorateButton();
        $("#btnAnciMealApplyToAll").decorateButton();
        //if (!UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo)) {
        //    $("#btnAnciMealApply").hide();
        //    $("#btnAnciMealApplyToAll").hide();
        //}
        $("#btnAnciMealApply").click(function () {
            $("#btnAnciMealApply").blur();
            UI_tabAnci.anciMealApplyOnClick();
        });
        $("#btnAnciMealApplyToAll").click(function () {
            $("#btnAnciMealApplyToAll").blur();
            UI_tabAnci.anciMealApplyToAllOnClick();
        });
        $('#insTermsNCond').click(
            function () {
                UI_tabAnci.showPopupWindow("Terms and Conditions",
                    UI_tabAnci.insuranceTermsNCondLink);
            });

        if (UI_tabAnci.focMealEnabled == true) {
            $('#btnAnciMealMenu').hide();
        } else {
            $('#btnAnciMealMenu').decorateButton();
            $('#btnAnciMealMenu').click(
                function () {
                    UI_tabAnci.showPopupWindow("Sky Cafe",
                        "showFile!displayMealsV2.action", 600, 800,
                        true);
                });
        }

        $("#btnAnciBaggageApply").decorateButton();
        $("#btnAnciBaggageApplyToAll").decorateButton();
        $("#btnAnciBaggageApply").click(function () {
            $("#btnAnciBaggageApply").blur();
            UI_tabAnci.anciBaggageApplyOnClick(false);
        });
        $("#btnAnciBaggageApplyToAll").click(function () {
            $("#btnAnciBaggageApplyToAll").blur();
            UI_tabAnci.anciBaggageApplyToAllOnClick();
        });

        $("#btnAnciBaggageChargeOverride").decorateButton();
        if (!UI_tabAnci.baggageChargeOverride) {
            $("#btnAnciBaggageChargeOverride").hide();
        }
        $("#btnAnciBaggageChargeOverride").click(function () {
            $("#btnAnciBaggageChargeOverride").blur();
            UI_tabAnci.anciBaggageApplyOnClick(true);
        });

    }
    $("#spnSeatInfo").hide();
    UI_tabAnciSummary.hide();

    $("#tblSeatMap").hide();
    $("#spnAnciNoSeat").hide();

    UI_tabAnci.constructSSRGrid({
        id: "#tblAnciSSR"
    });
	
    UI_tabAnci.constructBaggageGrid({
        id: "#tblAnciBaggage"
    });
    UI_tabAnci.constructAPServiceGrid({
        id: "#tblAnciAPService"
    });

    UI_tabAnci.constructAPTransferGrid({
        id: "#tblAnciAPTransfer"
    });


    if (UI_tabAnci.focMealEnabled == true) {
        $(".paxMealCharge").hide();
    }

    UI_tabAnci.onLoadCall(showTab);

    if(UI_tabAnci.shouldFlexiTabVisible()){

        $("#imgTab_7").show();

    } else {
        $("#imgTab_7").hide();
    }
}

UI_tabAnci.shouldFlexiTabVisible = function() {

    var isONDFlexiAvailable = false;

    if(UI_tabAnci.isFlexiEnableInAnci) {
        for ( var i = 0; i < jsFlexiSeg.length; i++) {
            var ondSequence = jsFlexiSeg[i].ondSeq;
            var isAvailable = UI_tabSearchFlights.ondWiseFlexiAvailable[ondSequence];
            if(isAvailable){
                isONDFlexiAvailable = true;
                break;
            }
        }
    }

    return isONDFlexiAvailable;
}

/*
 * On Load Call
 */
UI_tabAnci.onLoadCall = function (showTab) {
    UI_tabAnci.tabClick({
        id: showTab
    });
    if(UI_tabAnci.salesTaxForFare != 0){
    	UI_tabAnci.continueToServiceTaxCalc = true;
    }
   UI_tabAnci.buildPriceSummary();
}

/*
 * Image On Click
 */
UI_tabAnci.tabClick = function (p) {
    UI_commonSystem.initializeMessage();
    var intLen = jsAnciSrv.length;
    var i = 0;
    var strImg = "";
    var strHD = "";
    var strFunName = "";
    UI_tabAnci.currAnciSrv = null;
    do {
        if (jsAnciSrv[i].id == p.id) {
            strImg = jsAnciSrv[i].imgOn;
            strHD = jsAnciSrv[i].desc;
            strFunName = jsAnciSrv[i].onClick;
            $("#" + jsAnciSrv[i].containerInfo).show();
            $("#" + jsAnciSrv[i].containerPaxInfo).show();
            UI_tabAnci.currAnciSrv = jsAnciSrv[i].code;
        } else {
            strImg = jsAnciSrv[i].imgOff;
            $("#" + jsAnciSrv[i].containerInfo).hide();
            $("#" + jsAnciSrv[i].containerPaxInfo).hide();
        }
        $("#imgTab_" + i).find("img").attr("src", UI_commonSystem.strImagePathAA + strImg);
        if (UI_tabAnci.blOnLoad) {
            $("#imgTab_" + i).click(function () {
                UI_tabAnci.tabClick({
                    id: this.id.split("_")[1]
                })
            });
        }

        i++;
    } while (--intLen);
    $("#divAnciHD").html(strHD);
    UI_tabAnci.blOnLoad = false;

    if (strFunName != "") {
        eval(strFunName);
    }
}

/*
 * Show / Hide Segments
 */
UI_tabAnci.showHideSegments = function (p) {
    if (p.show) {
        $("#tdAnciSeg").show();
    } else {
        $("#tdAnciSeg").hide();
    }
}

/*
 * Validate Apply
 */
UI_tabAnci.validateApply = function () {
    UI_commonSystem.initializeMessage();

    if (UI_tabAnci.currPaxIDRow == null) {
        showERRMessage(raiseError("XBE-ERR-21", "Passenger"));
        return false;
    }

    return true;
}

UI_tabAnci.isOneOfModifyingSeg = function (flightRefNum) {
    var page = null;
    if (UI_tabAnci.isSegModify) {
        page = UI_modifySegment;
    } else if (UI_tabAnci.isRequote) {
        page = UI_requote;
    }
    return (page != null) ? (page.modifyingFltSegHash[flightRefNum] != null) : false;
}

UI_tabAnci.getModifyingSegIndex = function (flightRefNum) {
    var page = null;
    if (UI_tabAnci.isSegModify) {
        page = UI_modifySegment;
    } else if (UI_tabAnci.isRequote) {
        page = UI_requote;
    }

    var count = 0;
    if (page != null) {
        $.each(page.modifyingFltSegHash, function (i, val) {
            if (i == flightRefNum) {
                return false;
            }

            count++;
        });
    }

    return count;
}

UI_tabAnci.transformPax = function (paxArr) {
    var outArr = [];
    for (var i = 0; i < paxArr.length; i++) {
        var inPax = paxArr[i];
        var pax = {};
        if (inPax.infants != null && inPax.infants.length > 0) {
            pax.displayInfantTravelling = 'Y';
            pax.displayInfantWith = inPax.infants[0].paxSequence - 1;
        } else {
            pax.displayInfantTravelling = 'N';
            pax.displayInfantWith = '';
        }
        pax.displayAdultTitle = inPax.title;
        pax.displayAdultFirstName = inPax.firstName;
        pax.displayAdultLastName = inPax.lastName;
        pax.displayAdultType = inPax.paxType;
        pax.displayAdultNationality = inPax.nationalityCode;
        pax.currentAncillaries = inPax.selectedAncillaries;
        pax.displayAdultDOB = inPax.dateOfBirth;
        pax.travelerRefNumber = inPax.travelerRefNumber;
        pax.displayPnrPaxCatFOIDNumber = inPax.pnrPaxCatFOIDNumber;
        pax.displayNationalIDNo = inPax.nationalIDNo;
        pax.displayETicket = inPax.eTicket;
        pax.paxId = inPax.paxSequence - 1;
        pax.seqNumber = inPax.paxSequence;
        pax.paxType = inPax.paxType;
        pax.totalPaidAmount = inPax.totalPaidAmount;
        pax.totalAvailableBalance = inPax.totalAvailableBalance;
        pax.totalPrice = inPax.totalPrice;
		if(inPax.lccClientAdditionPax != null && inPax.lccClientAdditionPax != undefined && inPax.lccClientAdditionPax != '' ){
			pax.displayVisaApplicableCountry = inPax.lccClientAdditionPax.visaApplicableCountry;
			pax.displayTravelDocType = inPax.lccClientAdditionPax.travelDocumentType;
			pax.displayVisaDocIssueDate = inPax.lccClientAdditionPax.visaDocIssueDate;
			pax.displayVisaDocNumber = inPax.lccClientAdditionPax.visaDocNumber;
			pax.displayVisaDocPlaceOfIssue = inPax.lccClientAdditionPax.visaDocPlaceOfIssue;
			pax.displayPnrPaxCatFOIDNumber = inPax.lccClientAdditionPax.passportNo;
			pax.displayPnrPaxCatFOIDExpiry = inPax.lccClientAdditionPax.passportExpiry;
			pax.displayPnrPaxCatFOIDPlace = inPax.lccClientAdditionPax.passportIssuedCntry;
			pax.displayPnrPaxPlaceOfBirth = inPax.lccClientAdditionPax.placeOfBirth;
		}

        pax.currentAncillaries = UI_tabAnci
            .addMissingAnciSegments(pax.currentAncillaries);
        outArr[i] = pax;
    }
    return outArr;
}

UI_tabAnci.paxSorter = function (first, second) {
    return (first.paxId - second.paxId);
}
/**
 * yyyyMMddHHmmss
 */
UI_tabAnci._getFltRefDate = function (dstr) {
    var i = function (str) {
        return parseInt(str, 10);
    };
    return new Date(i(dstr.substr(0, 4)), i(dstr.substr(4, 2)) - 1, i(dstr
        .substr(6, 2)), i(dstr.substr(8, 2)), i(dstr.substr(10, 2)), i(dstr
        .substr(12, 2)));
}

UI_tabAnci.segmentComparator = function (first, second) {
    var fArr = first.flightSegmentTO.flightRefNumber.split('$');
    var sArr = second.flightSegmentTO.flightRefNumber.split('$');
    var fTime = UI_tabAnci._getFltRefDate(fArr[3]);
    var sTime = UI_tabAnci._getFltRefDate(sArr[3]);
    return (fTime.getTime() - sTime.getTime());
}

UI_tabAnci.addMissingAnciSegments = function (currentAncillaries) {
    var page = null;
    if (UI_tabAnci.isSegModify) {
        page = UI_modifySegment;
    } else if (UI_tabAnci.isRequote) {
        page = UI_requote;
    }

    if (page != null) {
        $
            .each(
                page.modifyingFltSegHash,
                function (fltRefNo, segInfo) {
                    var found = false;
                    for (var i = 0; i < currentAncillaries.length; i++) {
                        if (currentAncillaries[i].flightSegmentTO.flightRefNumber == fltRefNo) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        var anciObj = {};
                        anciObj.flightSegmentTO = {};
                        anciObj.flightSegmentTO.flightRefNumber = fltRefNo;
                        anciObj.airSeatDTO = null;
                        anciObj.mealDTOs = [];
                        anciObj.specialServiceRequestDTOs = [];
                        anciObj.airportServiceDTOs = [];
                        anciObj.airportTransferDTOs = [];
                        anciObj.airportTransferDTOs = [];
                        anciObj.baggageDTOs = [];
                        currentAncillaries[currentAncillaries.length] = anciObj;
                    }
                });
    }

    return $.airutil.sort.quickSort(currentAncillaries,
        UI_tabAnci.segmentComparator);
}

/*
 * SSR On Click
 */
UI_tabAnci.onClickSSR = function (conf) {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.SSR;
    $("#spanFltSegTitle").text(UI_tabAnci.FltSegTitle.SEGMENT);
    var options = {
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (options.initLayout) {
        UI_tabAnci.showHideSegments({
            show: true
        });
        UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.SSR);
    }
    if (!UI_tabAnci.dataStatus.ssr) {
        var data = {};
        if (!UI_tabAnci.isAnciModify) {
        	if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        		data = UI_MinimumFare.createSearchParams('searchParams');
        	} else {
        		data = UI_tabSearchFlights.createSearchParams('searchParams');
        	}
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote
                    .populateFlightSegmentList());
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
        } else {
            data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['modifyAncillary'] = true;
        }

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "anciSSR.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.returnCallSSR(response, options.initLayout);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        UI_tabAnci.currFltRefNo = null;
        if (options.initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.SSR);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
        UI_commonSystem.hideProgress();
    }
}

/*
 * SSR data return call
 */
UI_tabAnci.returnCallSSR = function (response, initLayout) {
    UI_commonSystem.hideProgress();
    jsSSRModel = response.flightSegmentSSRs;

    if (jsSSRModel.length == 0) {
        showERRMessage("Error occured while fetching SSR data");
    } else {
        jsSSRModel = $.airutil.sort.quickSort(jsSSRModel,
            UI_tabAnci.departureDateComparator);

        for (var i = 0; i < jsSSRModel.length; i++) {
            jsSSRModel[i].specialServiceRequest = $.airutil.sort.quickSort(
                jsSSRModel[i].specialServiceRequest,
                UI_tabAnci.SSRCodeComparator);
        }

        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.reprotectSSR(jsSSRModel);
        }

        UI_tabAnci.dataStatus.ssr = true;
        UI_tabAnci.currFltRefNo = null;
        if (initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.SSR);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
    }
}

UI_tabAnci.reprotectSSR = function (jsSSRModel) {

    if (!UI_tabAnci.reprotect.ssr) {
        UI_tabAnci.reprotect.ssr = true;

        for (var j = 0; j < jsonPaxAdults.length; j++) {
            var currAnci = jsonPaxAdults[j].currentAncillaries;
            var segId = 0;

            if (currAnci != null && currAnci.length > 0) {
                for (var k = 0; k < currAnci.length; k++) {

                    if (!UI_tabAnci.isOneOfModifyingSeg(currAnci[k].flightSegmentTO.flightRefNumber) || jsSSRModel[segId] == null) {
                        continue;
                    }

                    if (currAnci[segId] != null || !UI_Container
                        .isOneOfModifyingSeg(currAnci[segId].flightSegmentTO.flightRefNumber)) {
                        if (currAnci[k].specialServiceRequestDTOs != undefined) {
                            for (var l = 0; l < currAnci[k].specialServiceRequestDTOs.length; l++) {
                                var ssrMapObj = UI_tabAnci
                                    .getSSRRefObj(
                                        currAnci[k].specialServiceRequestDTOs[l].ssrCode,
                                        jsSSRModel[segId].flightSegmentTO.flightRefNumber);

                                if ((ssrMapObj != null && ssrMapObj.availableQty > 0) || (ssrMapObj != null && ssrMapObj.availableQty == -1)) {
                                    var ssrObj = {
                                        ssrCode: ssrMapObj.ssrCode,
                                        ssrName: ssrMapObj.ssrName,
                                        description: ssrMapObj.description,
                                        charge: currAnci[k].specialServiceRequestDTOs[l].charge,
                                        text: currAnci[k].specialServiceRequestDTOs[l].text,
                                        ssrIndex: ssrMapObj.smI,
                                        availableQty: ssrMapObj.availableQty,
                                        serviceQuantity: ssrMapObj.serviceQuantity,
                                        old: false
                                    };
                                    if(ssrMapObj.availableQty > 0){
                                    	ssrMapObj.availableQty =  ssrMapObj.availableQty - 1;
                                    }
                                    UI_tabAnci.currSegRow = jsAnciSegRefMap[jsSSRModel[segId].flightSegmentTO.flightRefNumber].index;
                                    UI_tabAnci.paxSSRAdd(j, ssrObj,
                                        ssrObj.text, ssrObj.ssrIndex);
                                }
                            }
                        }
                    }

                    segId++;
                }
            }
        }
    }

}

UI_tabAnci.validateSegmentCode = function (currentSegmentCode,jsSSRModel){
	var isSegmentCodeAvailable = false;
	for(var j=0;j<jsSSRModel.length;j++){
		if(jsSSRModel[j].flightSegmentTO.segmentCode == currentSegmentCode ){
			isSegmentCodeAvailable = true;
		}
	}
	return isSegmentCodeAvailable;
}

UI_tabAnci.getValiedSegmentIdForSsr = function(currentSegmentCode){
	var segmentId=-1;
	for (var j = 0; j < jsSSRModel.length; j++) {
		if(jsSSRModel[j].flightSegmentTO.segmentCode==currentSegmentCode){
			segmentId = j;
			break;
		}
	}
	return segmentId;
}

/*
 * Meal On Click
 */
UI_tabAnci.onClickMeal = function (conf) {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.MEAL;
    $("#spanFltSegTitle").text(UI_tabAnci.FltSegTitle.SEGMENT);
    var options = {
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (options.initLayout) {
        UI_tabAnci.showHideSegments({
            show: true
        });
        UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.MEAL);
    }
    if (!UI_tabAnci.dataStatus.meal) {

        var data = {};
        if (!UI_tabAnci.isAnciModify) {
        	if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        		data = UI_MinimumFare.createSearchParams('searchParams');
    		} else {
    			 data = UI_tabSearchFlights.createSearchParams('searchParams');
    		}          
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote
                    .populateFlightSegmentList());
                data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
                data['jsonOnds'] = DATA_ResPro["jsonOnds"];
                data['pnr'] = DATA_ResPro["pnrNo"];
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
        } else {
            data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['jsonOnds'] = DATA_ResPro["jsonOnds"];
            data['modifyAncillary'] = true;
            data['pnr'] = DATA_ResPro["pnrNo"];
        }

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            async: false,
            data: data,
            url: "anciMeal.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.returnCallMeal(response, options.initLayout);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        if (options.initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.MEAL);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
        UI_commonSystem.hideProgress();
    }

}
/*
 * Meal data return call
 */
UI_tabAnci.returnCallMeal = function (response, initLayout) {
    /*
     * createMealCatModal = function(arraList){ var objectList = []; if
     * (arraList != null){ for (var i = 0; i < arraList.length; i++) { var temp =
     * {"catCode":arraList[i][0],"catName":arraList[i][1]}
     * objectList.push(temp); } } return objectList; };
     */

    UI_commonSystem.hideProgress();
    jsMealModel = response.mealResponceDTO.flightSegmentMeals;
    jsMealModel = $.airutil.sort.quickSort(jsMealModel,
        UI_tabAnci.segmentComparator);
    if (jsMealModel.length == 0) {
        showERRMessage("Error occured while fetching meal data");
    } else {
        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.reprotectMeal();
        }
        UI_tabAnci.dataStatus.meal = true;
        UI_tabAnci.currFltRefNo = null;
        if (initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.MEAL);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
    }

}

UI_tabAnci.reprotectMeal = function () {
    if (!UI_tabAnci.reprotect.meal) {
        UI_tabAnci.reprotect.meal = true;
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            var dupliHash = {};
            var mealReprotected = {};
            for (var k = 0; k < currAnci.length; k++) {
                if (dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] || !UI_tabAnci
                    .isOneOfModifyingSeg(currAnci[k].flightSegmentTO.flightRefNumber) || 
                     jsMealModel[UI_tabAnci.getValiedSegmentId(currAnci[k].flightSegmentTO.segmentCode)] == null) {
                    continue;
                }
                dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] = true;
                var usedCategory = {};
                for (var j = 0; j < currAnci[k].mealDTOs.length; j++) {
                    var mealMapObj = UI_tabAnci.getMealRefObj(
                        currAnci[k].mealDTOs[j].mealCode,
                        jsMealModel[UI_tabAnci.getValiedSegmentId(currAnci[k].flightSegmentTO.segmentCode)].flightSegmentTO.flightRefNumber);
                    var qty = currAnci[k].mealDTOs[j].soldMeals;
                    if (mealMapObj == null || parseInt(mealMapObj.availableMeals, 10) < qty) {
                        // UI_Anci.notAllAnciReprotect = true;
                    } else {
                    	var mealQty = qty;
                    	var multiMealEnabled = UI_tabAnci.setMultiMealEnabled(jsMealModel[UI_tabAnci.getValiedSegmentId(currAnci[k].flightSegmentTO.segmentCode)].flightSegmentTO.flightRefNumber);
                    	var skipReprotect = false;
                    	
                    	if(!multiMealEnabled && mealReprotected[currAnci[k].flightSegmentTO.flightRefNumber]){
                    		skipReprotect = true;
                    	} else if(!multiMealEnabled){
                    		mealQty = 1;                    		
                    	}
                    	
                    	if(mealMapObj.categoryRestricted){
                    		if(usedCategory[mealMapObj.mealCategoryCode]){
                    			skipReprotect = true;
                    		} else {
                    			usedCategory[mealMapObj.mealCategoryCode] = true;
                    			mealQty = 1; 
                    		}
                    	}
                    	
                    	if(!skipReprotect){                    		
                    		var mealObj = {
                    				mealCode: mealMapObj.mealCode,
                    				mealName: mealMapObj.mealName,
                    				mealQty: mealQty,
                    				mealCharge: mealMapObj.mealCharge,
                    				subTotal: parseFloat(mealMapObj.mealCharge) * (mealQty),
                    				mealIndex: mealMapObj.mmI,
                    				old: false,
                    				mealCategoryCode : mealMapObj.mealCategoryCode,
                    				categoryRestricted : mealMapObj.categoryRestricted
                    		};
                    		mealMapObj.availableMeals =  mealMapObj.availableMeals - mealQty;
                    		var segIndex = jsAnciSegRefMap[jsMealModel[UI_tabAnci.getValiedSegmentId(currAnci[k].flightSegmentTO.segmentCode)].flightSegmentTO.flightRefNumber].index;
                    		var paxMealObj = {
                    				paxId: i,
                    				segId: segIndex,
                    				mealObjRef: mealObj
                    		};
                    		UI_tabAnci.paxMealAdd(paxMealObj);
                    		mealReprotected[currAnci[k].flightSegmentTO.flightRefNumber] = true;
                    	}
                    }
                }
            }
        }
    }
}

UI_tabAnci.getValiedSegmentId = function(currentFlightSegment){
	var segmentId=-1;
	for (var j = 0; j < jsMealModel.length; j++) {
		if(jsMealModel[j].flightSegmentTO.segmentCode==currentFlightSegment){
			segmentId = j;
			break;
		}
	}
	return segmentId;
}

/*
 * Flexi On Click
 */
UI_tabAnci.onClickFlexi = function (conf) {
	isFromFlexiTab = true;
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.FLEXI;
    var options = {
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (options.initLayout) {
        UI_tabAnci.showHideSegments({
            show: true
        });
        UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.FLEXI);
    }

    var segObj = UI_tabAnci.getSegForActiveAnci(UI_tabAnci.anciType.FLEXI);
    UI_tabAnci.tdSegOnClickHighlight(segObj);

    UI_commonSystem.hideProgress();
};

UI_tabAnci.onClickAutoCheckin = function (conf) {
	UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.AUTOMATICCHECKIN;

    var options = {
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (options.initLayout) {
        UI_tabAnci.showHideSegments({
            show: true
        });
        UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.AUTOMATICCHECKIN);
    }
    if (!UI_tabAnci.dataStatus.automaticCheckin) {
        var data = {};
       	if (!UI_tabAnci.isAnciModify) {
        	if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        		data = UI_MinimumFare.createSearchParams('searchParams');
    		} else {
    			data = UI_tabSearchFlights.createSearchParams('searchParams');
    		}            
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote
                    .populateFlightSegmentList());
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
        } else {
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['modifyAncillary'] = true;
        }

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "anciAutomaticCheckin.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.returnCallAUTOMATICCHECKIN(response, options.initLayout);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        UI_tabAnci.currFltRefNo = null;
        if (options.initLayout) {
            var segObj = UI_tabAnci.getSegForActiveAnci(UI_tabAnci.anciType.AUTOMATICCHECKIN);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
        UI_commonSystem.hideProgress();
    }
}

/*
 * return call automatic Checkin
 */

UI_tabAnci.returnCallAUTOMATICCHECKIN = function (response, initLayout) {
    UI_commonSystem.hideProgress();
    jsAutoCheckinModel = response.automaticCheckinDTOs;

    if (jsAutoCheckinModel.length == 0) {
        showERRMessage("Error occured while fetching SSR data");
    } else {

        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
			UI_tabAnci.reprotectAutomaticCheckin(jsAutoCheckinModel);
        }

        UI_tabAnci.dataStatus.automaticCheckin = true;
        UI_tabAnci.currFltRefNo = null;
        if (initLayout) {
            var segObj = UI_tabAnci.getSegForActiveAnci(UI_tabAnci.anciType.AUTOMATICCHECKIN);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
    }
}

UI_tabAnci.reprotectAutomaticCheckin = function (jsAutoCheckinModel) {
    if (!UI_tabAnci.reprotect.automaticCheckin) {
        UI_tabAnci.reprotect.automaticCheckin = true;
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            var segId = 0;
            var dupliHash = {};
            for (var k = 0; k < currAnci.length; k++) {
                if (dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] || !UI_tabAnci
                    .isOneOfModifyingSeg(currAnci[k].flightSegmentTO.flightRefNumber)) {
                    continue;
                }
                dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] = true;
                var automaticCheckinDTO = currAnci[k].automaticCheckinDTOs;
                if (automaticCheckinDTO != null && automaticCheckinDTO.length > 0) {
                	var segObj = UI_tabAnci.getSegForActiveAnci(UI_tabAnci.anciType.AUTOMATICCHECKIN);
                    var fltRefNo = segObj.fltRefNo;
                    var paxType = jsonPaxAdults[i].paxType;
                    var refObj = {
                        fltRefNo: fltRefNo,
                    };

                    if(refObj !== undefined) {
                        var mObj = {
                            paxId: i,
                            paxType: paxType,
                            refObj: refObj,
							automaticCheckinDTO: automaticCheckinDTO,
                            subTotal: parseFloat(automaticCheckinDTO.automaticCheckinCharge),
                            old: false
                        };
						if(UI_tabAnci.routeModifyCheck(segObj.segID) && jsAnciSeg.length < 2){
							UI_tabAnci.markAutomaticCheckinSeat(mObj);
						}
                    }

                }
            }
        }
    } 
}

/*
 * Baggage On Click
 */
UI_tabAnci.onClickBaggage = function (conf) {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.BAGGAGE;
    $("#spanFltSegTitle").text(UI_tabAnci.FltSegTitle.SEGMENT);
    var options = {
        initLayout: true
    };
	options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (options.initLayout) {
        UI_tabAnci.showHideSegments({
            show: true
        });
        UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.BAGGAGE);
    }
    if (!UI_tabAnci.dataStatus.baggage) {
        var data = {};
        if (!UI_tabAnci.isAnciModify) {
			if (UI_tabSearchFlights.isCalenderMinimumFare()) {
				data = UI_MinimumFare.createSearchParams('searchParams');
			} else {
				data = UI_tabSearchFlights.createSearchParams('searchParams');
			}
           
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote
                    .populateFlightSegmentList());
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
        } else {
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['jsonOnds'] = DATA_ResPro["jsonOnds"];
            data['modifyAncillary'] = true;
            data['pnr'] = DATA_ResPro["pnrNo"];
        }

        // var i = 0;
        // $.each(gbl_ReservationSegments, function(index, val) {
        // data['resBaggageSummary.bookingClasses[' + index + ']'] =
        // val.bookingClassCode;
        // });

        if (options.initLayout) {
            UI_commonSystem.getDummyFrm().ajaxSubmit({
                dataType: 'json',
                processData: false,
                data: data,
                url: "anciBaggage.action",
                success: function (response) {
                    if (response == null || !response.success) {
                        UI_commonSystem.hideProgress();
                        showERRMessage(response.messageTxt);
                    } else {
                        UI_tabAnci.dataStatus.baggage = true;
                        UI_tabAnci.returnCallBaggage(response, true);
                    }
                },
                error: UI_commonSystem.setErrorStatus
            });
        }
    } else {
        if (options.initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.BAGGAGE);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
        UI_commonSystem.hideProgress();
    }
}

function getBaggageONDGrpIdWiseCount(jsBaggageModel, currentBaggageGrpONDId) {
    var count = 0;
    for (var i = 0; i < jsBaggageModel.length; i++) {
        var grpId = jsBaggageModel[i].flightSegmentTO.baggageONDGroupId;
        if (grpId != "" && grpId == currentBaggageGrpONDId) {
            count++;
        }
    }

    return count;
}

UI_tabAnci.getSelectedBaggageIndex = function (baggageChargeId) {
    for (var j = 0; j < jsAnciSeg.length; j++) {
        for (var i = 0; i < jsBaggageModel[j].baggages.length; i++) {
            if (baggageChargeId == jsBaggageModel[j].baggages[i].ondBaggageChargeId) {
                return i;
            }
        }
    }
    return null;
}

/*
 * Baggage data return call
 */
UI_tabAnci.returnCallBaggage = function (response, initLayout) {
    UI_commonSystem.hideProgress();
    jsBaggageModel = response.baggageResponseDTO.flightSegmentBaggages;

    if (jsBaggageModel.length > 0) {
        jsBaggageModel = $.airutil.sort.quickSort(jsBaggageModel,
            UI_tabAnci.departureDateComparator);
    }

    UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.BAGGAGE);

    if (jsBaggageModel.length == 0) {
        showERRMessage("Error occured while fetching baggage data");
    } else {

        if (UI_tabAnci.isSegModify || UI_tabAnci.isAnciModify) {
            UI_tabAnci.isBaggagesSelectedForCurrentBooking();
        }

        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.reprotectBaggage();
        }
        UI_tabAnci.currFltRefNo = null;
        var shouldLayoutInitialize = initLayout && UI_tabAnci.isBaggagesSelectedForCurrentBookingVal;

        if ((!UI_tabAnci.isAnciModify && !UI_tabAnci.isSegModify && !UI_tabAnci.isRequote) || !UI_tabAnci.isBaggagesSelectedForCurrentBookingVal) {
            for (var j = 0; j < jsAnciSeg.length; j++) {

                // Busses will not have baggages
                if (jsBaggageModel[j] != undefined) {
                    for (var i = 0; i < jsBaggageModel[j].baggages.length; i++) {
                        var qty = 1;
                        if (jsBaggageModel[j].baggages[i].defaultBaggage == 'Y') {
                            var grpId = jsBaggageModel[j].flightSegmentTO.baggageONDGroupId;
                            var grpCount = 1;
                            if (grpId != null && grpId != "") {
                                grpCount = getBaggageONDGrpIdWiseCount(
                                    jsBaggageModel, grpId);
                            }
                            var baggageObj = {
                                baggageName: jsBaggageModel[j].baggages[i].baggageName,
                                baggageQty: qty,
                                baggageCharge: jsBaggageModel[j].baggages[i].baggageCharge,
                                subTotal: parseFloat(jsBaggageModel[j].baggages[i].baggageCharge) * (qty) / grpCount,
                                baggageIndex: i,
                                ondBaggageChargeId: jsBaggageModel[j].baggages[i].ondBaggageChargeId,
                                old: false
                            };
                            var correctSegId = UI_tabAnci
                                .getCorrectSegmentId(jsBaggageModel[j].flightSegmentTO.flightRefNumber);

                            for (var k = 0; k < jsonPaxAdults.length; k++) {
                                if (jsonPaxAdults[k].currentAncillaries != undefined && jsonPaxAdults[k].currentAncillaries.length > 0) {
                                    for (var index = 0; index < jsonPaxAdults[k].currentAncillaries.length; index++) {
                                        if (!UI_tabAnci.isRequote || UI_tabAnci.validateSegments(jsBaggageModel
                                        		,jsonPaxAdults[k].currentAncillaries[index].flightSegmentTO.segmentCode)) {
                                            var baggage = jsonPaxAdults[k].currentAncillaries[index].baggageDTOs[0];
                                            var groupId = jsBaggageModel[j].flightSegmentTO.baggageONDGroupId;
                                            var groupCount = 1;
                                            if (groupId != null && groupId != "") {
                                                groupCount = getBaggageONDGrpIdWiseCount(
                                                    jsBaggageModel, groupId);
                                            }
                                            for (var m = 0; m < jsBaggageModel[j].baggages.length; m++) {
                                                if (baggage != undefined && baggage.baggageName == jsBaggageModel[j].baggages[m].baggageName) {
                                                    baggageObj = {
                                                        baggageName: jsBaggageModel[j].baggages[m].baggageName,
                                                        baggageQty: qty,
                                                        baggageCharge: jsBaggageModel[j].baggages[m].baggageCharge,
                                                        subTotal: parseFloat(jsBaggageModel[j].baggages[m].baggageCharge) * (qty) / grpCount,
                                                        baggageIndex: m,
                                                        ondBaggageChargeId: jsBaggageModel[j].baggages[m].ondBaggageChargeId,
                                                        old: false
                                                    };
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                paxBaggageObjTmp = {
                                    paxId: k,
                                    segId: correctSegId,
                                    baggageObjRef: baggageObj
                                };

                                var paxBaggageObjTmp = {
                                    paxId: k,
                                    segId: correctSegId,
                                    baggageObjRef: baggageObj
                                };
                                UI_tabAnci.paxBaggageAdd(paxBaggageObjTmp);
                            }
                        }
                    }
                }
            }
            shouldLayoutInitialize = initLayout;
        }
        if (shouldLayoutInitialize) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.BAGGAGE);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
    }
}

UI_tabAnci.validateSegments = function(jsBaggageModel,currentSegmentCode) {
	var isSegmentAvailable=false;
	
	for(var j=0;j<jsBaggageModel.length;j++){
		if(jsBaggageModel[j].flightSegmentTO.segmentCode == currentSegmentCode){
			isSegmentAvailable = true;
			break;
		}
	}
	
	return isSegmentAvailable;
}

UI_tabAnci.reprotectBaggage = function () {
    if (!UI_tabAnci.reprotect.baggage) {
        UI_tabAnci.reprotect.baggage = true;
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            var dupliHash = {};
            for (var k = 0; k < currAnci.length; k++) {
                var correctBaggageSegId = UI_tabAnci
                    .getCorrectBaggageSegmentId(currAnci[k].flightSegmentTO.flightRefNumber);
                if (correctBaggageSegId == undefined) {
                    continue;
                } else {
                    if (!UI_tabAnci.isBaggagesSelectedForCurrentBookingVal) {
                        continue;
                    }
                }
                if (dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] || !UI_tabAnci
                    .isOneOfModifyingSeg(currAnci[k].flightSegmentTO.flightRefNumber) || jsBaggageModel[correctBaggageSegId] == null) {
                    continue;
                }
                dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] = true;

                // if(currAnci[segId]!= null ||
                // !UI_Container.isOneOfModifyingSeg(currAnci[segId].flightSegmentTO.flightRefNumber)){
                if (currAnci[k].baggageDTOs != undefined) {
                    for (var j = 0; j < currAnci[k].baggageDTOs.length; j++) {
                        var baggageMapObj = UI_tabAnci
                            .getBaggageRefObj(
                                currAnci[k].baggageDTOs[j].baggageName,
                                jsBaggageModel[correctBaggageSegId].flightSegmentTO.flightRefNumber);
                        // var qty = currAnci[k].baggageDTOs[j].soldPieces;
                        if (baggageMapObj == null || baggageMapObj == undefined) {
                            // UI_Anci.notAllAnciReprotect = true;
                        } else {
                            var grpId = getBaggageONDGrpId(currAnci[k].flightSegmentTO.flightRefNumber);
                            var grpCount = 1;
                            if (grpId != null && grpId != "") {
                                grpCount = getBaggageONDGrpIdWiseCount(
                                    jsBaggageModel, grpId);
                            }
                            var baggageObj = {
                                baggageName: baggageMapObj.baggageName,
                                baggageQty: 1,
                                baggageCharge: baggageMapObj.baggageCharge,
                                subTotal: parseFloat(baggageMapObj.baggageCharge) / grpCount,
                                baggageIndex: baggageMapObj.mmI,
                                ondBaggageChargeId: baggageMapObj.ondBaggageChargeId,
                                old: false
                            };

                            var segIndex = jsAnciSegRefMap[jsBaggageModel[correctBaggageSegId].flightSegmentTO.flightRefNumber].index;
                            var paxBaggageObj = {
                                paxId: i,
                                segId: segIndex,
                                baggageObjRef: baggageObj
                            };
                            UI_tabAnci.paxBaggageAdd(paxBaggageObj);
                        }
                    }

                }

            }
        }
    }
}

/*
 * Seat On Click
 */
UI_tabAnci.onClickSeat = function (conf) {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.SEAT;
    $("#spanFltSegTitle").text(UI_tabAnci.FltSegTitle.SEGMENT);
    var options = {
        override: false,
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (options.initLayout) {
        UI_tabAnci.showHideSegments({
            show: true
        });
        UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.SEAT);
    }
    if (options.override || !UI_tabAnci.dataStatus.seat) {
        var data = {};
        if (!UI_tabAnci.isAnciModify) {
        	if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        		data = UI_MinimumFare.createSearchParams('searchParams');
    		} else {
    			data = UI_tabSearchFlights.createSearchParams('searchParams');
    		}            
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote
                    .populateFlightSegmentList());
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
        } else {
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['jsonOnds'] = DATA_ResPro["jsonOnds"];
            data['modifyAncillary'] = true;
            data['pnr'] = DATA_ResPro["pnrNo"];
        }

        // data['paxAdults'] = jsonPaxAdults;
        // data['paxInfants'] = jsonPaxInfants;
        UI_tabAnci.selectDefaultSeatsForPax = true;
        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            async: false,
            data: data,
            url: "seatMap.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.returnCallSeatMap(response, options.initLayout);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });

    } else {
        UI_tabAnci.currFltRefNo = null;
        if (options.initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.SEAT);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
            UI_commonSystem.hideProgress();
        }
    }
}

/*
 * Seat Map return Data
 */
UI_tabAnci.returnCallSeatMap = function (response, initLayout) {
    UI_commonSystem.hideProgress();
    jsSMModel = response.seatMapDTO.lccSegmentSeatMapDTOs;

    if (jsSMModel.length == 0) {
        showERRMessage("Error occured while fetching seat data");
    } else {
        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.reprotectSeat();
        }
        UI_tabAnci.dataStatus.seat = true;
        UI_tabAnci.currFltRefNo = null;
        if (initLayout) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.SEAT);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
            if (top.isAutoSeatAssignmentEnabled) {
                if (!UI_tabAnci.isAnciModify && !UI_tabAnci.isSegModify && !UI_tabAnci.isRequote) {
                    UI_tabAnci.setDefaultSeatSelection();
                }
            }
        }
    }
}

UI_tabAnci.setDefaultSeatSelection = function () {
    if (UI_tabAnci.selectDefaultSeatsForPax) {
        $.each(jsSMModel, function (index, smObj) {
            var cabinCls = smObj.lccSeatMapDetailDTO.cabinClass;
            var fltSegment = smObj.flightSegmentDTO;
            var setSeatSelection = false;
            if (UI_tabAnci.currFltRefNo == fltSegment.flightRefNumber) {
                setSeatSelection = true;
            }

            for (var ccI = 0; ccI < cabinCls.length; ccI++) {
                var cc = cabinCls[ccI];
                if (fltSegment.cabinClassCode == cc.cabinType) {
                    var rgDtos = cc.airRowGroupDTOs;
                    rgDtos = $.airutil.sort.quickSort(rgDtos,
                        UI_tabAnci.rowGroupComparator);
                    for (var rgI = 0; rgI < rgDtos.length; rgI++) {
                        var cgDtos = rgDtos[rgI].airColumnGroups;
                        cgDtos = $.airutil.sort.quickSort(cgDtos,
                            UI_tabAnci.columnGroupComparator);
                        for (var cgI = 0; cgI < cgDtos.length; cgI++) {
                            var rows = cgDtos[cgI].airRows;
                            rows = $.airutil.sort.quickSort(rows,
                                UI_tabAnci.rowComparator);
                            UI_tabAnci.setSeatForPax(rows, fltSegment,
                                setSeatSelection);
                        }
                    }
                }
            }
        });
        UI_tabAnci.selectDefaultSeatsForPax = false;
    }
}

UI_tabAnci.setSeatForPax = function (rows, fltSegment, setSeatSelection) {
    OUTER: for (var paxIndex = 0; paxIndex < jsonPaxAdults.length; paxIndex++) {
        var segIndex = jsAnciSegRefMap[fltSegment.flightRefNumber].index;
        if (jsAnciSeg[segIndex].pax[paxIndex].seat.seatNumber == undefined || jsAnciSeg[segIndex].pax[paxIndex].seat.seatNumber == "") {
            var paxObj = jsonPaxAdults[paxIndex];
            for (var rowId = 0; rowId < rows.length; rowId++) {
                var seats = rows[rowId].airSeats;

                var letters = [];
                for (var si = 0; si < seats.length; si++) {
                    letters[letters.length] = ((new String(seats[si].seatNumber))
                        .replace(/[\d]+/g, '')).toUpperCase();
                }
                letters = $.airutil.sort.quickSort(letters,
                    $.airutil.string.comparator);
                seats = $.airutil.sort.quickSort(seats,
                    function (first, second) {
                        var seatLetterF = ((new String(first.seatNumber))
                            .replace(/[\d]+/g, '')).toUpperCase();
                        var numF = parseInt(letters.indexOf(seatLetterF),
                            10);

                        var seatLetterS = ((new String(second.seatNumber))
                            .replace(/[\d]+/g, '')).toUpperCase();
                        var numS = parseInt(letters.indexOf(seatLetterS),
                            10);
                        return (numF - numS);
                    });

                var rowNumber = rows[rowId].rowNumber;
                for (var seatIndex = 0; seatIndex < seats.length; seatIndex++) {
                    var seat = seats[seatIndex];
                    var strSeatNo = seat.seatNumber;
                    if (seat.logicalCabinClassCode == fltSegment.logicalCabinClassCode && seat.seatAvailability == "VAC") {
                        var seatRefObj = {
                            fltRefNo: fltSegment.flightRefNumber,
                            seatNumber: strSeatNo,
                            rowNumber: rowNumber
                        };
                        if (!UI_tabAnci.isSeatOccupied(seatRefObj) && UI_tabAnci
                            .isSatisfySeatAssignementConstrains(seatRefObj)) {
                            var paxType = paxObj.type;
                            if (paxObj.displayInfantTravelling == "Y" && (!UI_tabAnci.canSeatInfant(seatRefObj,
                                paxIndex) || seat.seatType == "EXIT")) {
                                continue;
                            }

                            var mObj = {
                                paxId: paxIndex,
                                paxType: paxType,
                                refObj: seatRefObj,
                                old: false
                            };
                            UI_tabAnci.markSeat(mObj);
                            $("#tdPaxSeat_1_" + paxIndex).html(strSeatNo);
                            $("#tdPaxSeat_2_" + paxIndex).html(
                                $.airutil.format.currency(seat.seatCharge));
                            if (setSeatSelection) {
                                $("#tdSeat_" + strSeatNo).removeClass(
                                    "availSeat");
                                $("#tdSeat_" + strSeatNo).addClass("SltdSeat");
                            }
                            continue OUTER;
                        }
                    }
                }
            }
        }

    }
}

UI_tabAnci.reprotectSeat = function () {
    if (!UI_tabAnci.reprotect.seat) {
        UI_tabAnci.reprotect.seat = true;
        var notAllocSeats = [];
        jsSMModel = $.airutil.sort.quickSort(jsSMModel,
            UI_tabAnci.seatMapDepartureDateComparator);
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            var segId = 0;
            var dupliHash = {};
            for (var k = 0; k < currAnci.length; k++) {
                if (dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] || !UI_tabAnci
                    .isOneOfModifyingSeg(currAnci[k].flightSegmentTO.flightRefNumber) || jsSMModel[segId] == null) {
                    continue;
                }
                dupliHash[currAnci[k].flightSegmentTO.flightRefNumber] = true;
                var seatDTO = currAnci[k].airSeatDTO;
                var selFltSeg = jsSMModel[segId].flightSegmentDTO;
                if (seatDTO != null && selFltSeg.logicalCabinClassCode == seatDTO.logicalCabinClassCode) {
                    var fltRefNo = selFltSeg.flightRefNumber;
                    var paxType = jsonPaxAdults[i].paxType;
                    var refObj = {
                        fltRefNo: fltRefNo,
                        seatNumber: seatDTO.seatNumber
                    };
                    var seatMapObj = UI_tabAnci.getSeatObj(refObj);

                    if (!UI_tabAnci.isSeatOccupied(refObj) && seatMapObj.avail == 'VAC' && seatDTO.logicalCabinClassCode == seatMapObj.lccCode) {
                        if (UI_tabAnci.paxHasInfant(i) && !UI_tabAnci.canSeatInfant(refObj, i)) {
                            notAllocSeats[notAllocSeats.length] = {
                                paxId: i,
                                seat: seatDTO.seatNumber
                            };
                        } else {
                            var mObj = {
                                paxId: i,
                                paxType: paxType,
                                refObj: refObj,
                                subTotal: parseFloat(seatMapObj.seatCharge),
                                old: false
                            };
                            UI_tabAnci.markSeat(mObj);
                        }
                    } else if(refObj !== undefined && refObj.seatNumber != null) {
                        notAllocSeats[notAllocSeats.length] = {
                            paxId: i,
                            seat: seatDTO.seatNumber
                        };
                        var mObj = {
                                paxId: i,
                                paxType: paxType,
                                refObj: refObj,
                                subTotal: parseFloat(seatMapObj.seatCharge),
                                old: false
                            };
                            UI_tabAnci.markSeat(mObj);
                    }

                }
                segId++;
            }
        }
    }
}
/*
 * create Flight segment list JSON
 */
UI_tabAnci.createFlightSegmentTOList = function () {
    var fltSegArr = [];

    for (var i = 0; i < jsonSegAnciAvail.length; i++) {
        fltSegArr[fltSegArr.length] = jsonSegAnciAvail[i].flightSegmentTO;
    }

    return $.toJSON(fltSegArr);
}

/*
 * return Segment object for ancillary type @param Ancillary Type @param
 * rSegCode (optional)
 */
UI_tabAnci.getSegForActiveAnci = function (anciType, rFltRefNo) {

	if (anciType == UI_tabAnci.anciType.APSERVICES) {
   	 var segObj = null;
        for (var i = 0; i < jsAPServicesModel.length; i++) {
            var fltRefNo = jsAPServicesModel[i].flightSegmentTO.flightRefNumber;
            if (rFltRefNo == null || UI_tabAnci.compareFlightRefNo(rFltRefNo, fltRefNo)) {
                if (jsAPServicesModel[i].airportServices.length > 0) {
                    return {
                        segID: jsAPServicesModel[i].flightSegmentTO.segmentCode,
                        fltRefNo: fltRefNo,
                        segRow: i
                    };
                }
            }
        }
        return segObj;
   } else if (anciType == UI_tabAnci.anciType.APTRANSFER) {
       var segObj = null;
       for (var i = 0; i < jsAPTransferModel.length; i++) {
           var fltRefNo = jsAPTransferModel[i].flightSegmentTO.flightRefNumber;
           if (rFltRefNo == null || UI_tabAnci.compareFlightRefNo(rFltRefNo, fltRefNo)) {
               if (jsAPTransferModel[i].airportServices.length > 0) {
                   return {
                       segID: jsAPTransferModel[i].flightSegmentTO.segmentCode,
                       fltRefNo: fltRefNo,
                       segRow: i
                   };
               }
           }
       }
       return segObj;

   } else if(anciType == UI_tabAnci.anciType.FLEXI){
	   var intLen = jsonSegAnciAvail.length;
       var i = 0;
       var segObj = null;
       do {
           var segID = jsonSegAnciAvail[i].flightSegmentTO.segmentCode;
           var fltRefNo = jsonSegAnciAvail[i].flightSegmentTO.flightRefNumber;
           var ondSequence = jsonSegAnciAvail[i].flightSegmentTO.ondSequence;

           var anciList = jsonSegAnciAvail[i].ancillaryStatusList;
           if (rFltRefNo == null || UI_tabAnci.compareFlightRefNo(rFltRefNo, fltRefNo)) {
               if (UI_tabAnci.isAnciAvailable(anciList, anciType)
            		   && UI_tabSearchFlights.ondWiseFlexiAvailable[ondSequence]) {
                   return {
                       segID: segID,
                       fltRefNo: fltRefNo,
                       segRow: i
                   };
               } else {
	        	   for ( var c = 0; c < jsonSegAnciAvail.length; c++) {
						var tempJsonObj = jsonSegAnciAvail[c];
						if(ondSequence == tempJsonObj.flightSegmentTO.ondSequence){
							var innerAnciList = tempJsonObj.ancillaryStatusList;
							if (UI_tabAnci.isAnciAvailable(innerAnciList, anciType)
				            		   && UI_tabSearchFlights.ondWiseFlexiAvailable[ondSequence]) {
								return {
			                       segID: tempJsonObj.flightSegmentTO.segmentCode,
			                       fltRefNo: tempJsonObj.flightSegmentTO.flightRefNumber,
			                       segRow: i
								};
							}
						}					
	        	   }
               }
           }
           i++;
       } while (--intLen);

       return segObj;
   } else {
   	 	var intLen = jsonSegAnciAvail.length;
        var i = 0;
        var segObj = null;
        do {
            var segID = jsonSegAnciAvail[i].flightSegmentTO.segmentCode;
            var fltRefNo = jsonSegAnciAvail[i].flightSegmentTO.flightRefNumber;

            var anciList = jsonSegAnciAvail[i].ancillaryStatusList;
            if (rFltRefNo == null || UI_tabAnci.compareFlightRefNo(rFltRefNo, fltRefNo)) {
                if (UI_tabAnci.isAnciAvailable(anciList, anciType)) {
                    return {
                        segID: segID,
                        fltRefNo: fltRefNo,
                        segRow: i
                    };
                }
            }
            i++;
        } while (--intLen);

        return segObj;
   }
}

/*
 * Is Ancillary available for given segment
 */
UI_tabAnci.isAnciAvailForSeg = function (anciType, fltRefNo) {
    if (UI_tabAnci.getSegForActiveAnci(anciType, fltRefNo) == null) {
        return false;
    } else {
        return true;
    }
}

/*
 * Ancillary availability for given segment list
 */
UI_tabAnci.isAnciAvailable = function (anciList, anciType) {
    //	if(anciType ==UI_tabAnci.anciType.FLEXI){
    //		return true;
    //	}

    for (var j = 0; j < anciList.length; j++) {
        if (anciList[j].available) {
            if (anciList[j].ancillaryType == null) {
                continue;
            }
            var tmpAnciType = anciList[j].ancillaryType.ancillaryType;
            if (anciType == UI_tabAnci.anciType.SEAT && tmpAnciType == UI_tabAnci.anciType.SEAT) {
                return true;
            }
            if (anciType == UI_tabAnci.anciType.MEAL && tmpAnciType == UI_tabAnci.anciType.MEAL) {
                return true;
            }
            if (anciType == UI_tabAnci.anciType.FLEXI && tmpAnciType == UI_tabAnci.anciType.FLEXI) {
                return anciList[j].available;
            }
            if (anciType == UI_tabAnci.anciType.SSR && tmpAnciType == UI_tabAnci.anciType.SSR) {
                return true;
            }
            if (anciType == UI_tabAnci.anciType.INS && tmpAnciType == UI_tabAnci.anciType.INS) {
                return true;
            }
            if (anciType == UI_tabAnci.anciType.APSERVICES && tmpAnciType == UI_tabAnci.anciType.APSERVICES) {
                return true;
            }

            if (anciType == UI_tabAnci.anciType.APTRANSFER && tmpAnciType == UI_tabAnci.anciType.APTRANSFER) {
                return true;
            }

            if (anciType == UI_tabAnci.anciType.BAGGAGE && tmpAnciType == UI_tabAnci.anciType.BAGGAGE) {
                return true;
            }
			
			if (anciType == UI_tabAnci.anciType.AUTOMATICCHECKIN && tmpAnciType == UI_tabAnci.anciType.AUTOMATICCHECKIN) {
                return true;
            }
        }
    }
    return false;
}

function getModifyAnciContactInfo(prefix) {
    var data = {};
    $.each(UI_tabAnci.contactInfo, function (key, value) {
        data[prefix + '.' + key] = value;
    });
    return data;
}

/*
 * Insurance On Click
 */
UI_tabAnci.onClickInsurance = function () {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.INS;
    UI_tabAnci.showHideSegments({
        show: false
    });
    if (!UI_tabAnci.dataStatus.insurance) {
        var data = {};
		if (UI_tabAnci.isAnciModify) {
			data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
			data['modifyAncillary'] = true;
			var contactInfo = getModifyAnciContactInfo('contactInfo');
			data = $.airutil.dom.concatObjects(data, contactInfo);
		} else {
			if (UI_tabSearchFlights.isCalenderMinimumFare()) {
				data = UI_MinimumFare.createSearchParams('searchParams');
			} else {
				data = UI_tabSearchFlights.createSearchParams('searchParams');
			}
			if (UI_tabAnci.isSegModify) {				
				data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
				data['returnFlag'] = UI_tabSearchFlights.isReturn();
				data['insFltSegments'] = $.toJSON(UI_modifySegment
						.getInsuranceFlightSegments());
				var contactInfo = getModifyAnciContactInfo('contactInfo');
				data = $.airutil.dom.concatObjects(data, contactInfo);
			} else if (UI_tabAnci.isAddSegment) {
				data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
				data['returnFlag'] = UI_tabSearchFlights.isReturn();
				data['insFltSegments'] = $.toJSON(UI_addSegment
						.getInsuranceFlightSegments());
				var contactInfo = getModifyAnciContactInfo('contactInfo');
				data = $.airutil.dom.concatObjects(data, contactInfo);
			} else if (UI_tabAnci.isRequote) {
				data['selectedFlightList'] = $.toJSON(UI_requote
						.populateFlightSegmentList());
				data['insFltSegments'] = $.toJSON(UI_requote
						.getConfirmedExistingFlightSegments());
				data['requote'] = true;
				var contactInfo = getModifyAnciContactInfo('contactInfo');
				data = $.airutil.dom.concatObjects(data, contactInfo);
			} else {
				data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
				data['returnFlag'] = UI_tabSearchFlights.isReturn();
				var contactInfo = $('input,select').filterData('name',
						'contactInfo');
				data = $.airutil.dom.concatObjects(data, contactInfo);
			}
		}

        $('#frmMakeBkg :input').each(function () {
            var type = this.type;
            if (type == 'checkbox' || type == 'radio') {
                if (this.checked)
                    data[this.name] = $(this).val();
            } else {
                data[this.name] = $(this).val();
            }
        });

        data['paxAdults'] = $.toJSON(jsonPaxAdults);
        data['paxInfants'] = $.toJSON(jsonPaxInfants);
        data['pnr'] = UI_tabAnci.pnr;
        UI_commonSystem.showProgress();

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "anciInsurance.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.onClickInsuranceSuccess(response);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        UI_tabAnci.buildInsurancePax();
        UI_tabAnci.populateInsuranceData();
    }
}

/*
 * on Click insurance
 */
UI_tabAnci.onClickInsuranceSuccess = function(response) {
	UI_commonSystem.hideProgress();
	if ( !hasValidQuotes(response.insuranceDTO) ) {
		if (response.messageTxt != null && response.messageTxt != '') {
			showERRMessage(response.messageTxt);
		} else {
			showERRMessage("Insurance service is temporary unavaialble. Please try again later");
		}
		$('#tblInsuranceDetails').hide();
	} else {
		UI_tabAnci.dataStatus.insurance = true;
		UI_tabAnci.insContent = response.insContent;
		UI_tabAnci.buildInsurance(response.insuranceDTO);
		$('#tblInsuranceDetails').show();
		UI_tabAnci.insuranceTermsNCondLink = response.termsNCondLink;
		UI_tabAnci.insurableFltRefNumbers = response.insurableFltRefNumbers;
		UI_tabAnci.allowInfantInsurance = response.allowInfantInsurance;
		
		UI_tabAnci.buildInsurancePax();
		UI_tabAnci.populateInsuranceData();
	}
}

function hasValidQuotes(insuranceDTO){
	var valid = false;
	if(insuranceDTO != null &&  insuranceDTO.length > 0 ){
		 $.each(insuranceDTO, function (pid, p) {
			 if(p.insuranceRefNumber != null && p.insuranceRefNumber != ""){
				 valid =true;
				 return;
			 }
		 });
	}
	return valid;
}

function getOndBaggageInfoObj() {
    var obj = {
        rphOndMap: {},
        skipRphs: [],
        ondCodes: []
    };

    for (var i = 0; i < jsBaggageModel.length; i++) {
        var baggageONDGroupId = jsBaggageModel[i].flightSegmentTO.baggageONDGroupId;

        if (baggageONDGroupId != null && baggageONDGroupId != "") {
            var selectedSegCode = jsBaggageModel[i].flightSegmentTO.segmentCode;
            var arr = selectedSegCode.split("/");

            obj.rphOndMap[jsBaggageModel[i].flightSegmentTO.flightRefNumber] = baggageONDGroupId;
            if (obj.ondCodes[baggageONDGroupId] != null) {
                obj.skipRphs[obj.skipRphs.length] = jsBaggageModel[i].flightSegmentTO.flightRefNumber;
            }

            if (obj.ondCodes[baggageONDGroupId] == null) {
                obj.ondCodes[baggageONDGroupId] = jsBaggageModel[i].flightSegmentTO.segmentCode;
            } else {
                var tmpArr = obj.ondCodes[baggageONDGroupId].split("/");

                if (tmpArr[tmpArr.length - 1] == arr[0]) {
                    obj.ondCodes[baggageONDGroupId] = obj.ondCodes[baggageONDGroupId] + "/" + arr[arr.length - 1];
                } else {
                    obj.ondCodes[baggageONDGroupId] = obj.ondCodes[baggageONDGroupId] + "/" + selectedSegCode;
                }
            }
        }
    }

    return obj;
}

function getBaggageONDGrpId(rph) {
    for (var i = 0; i < jsBaggageModel.length; i++) {
        var flightSegmentTOObj = jsBaggageModel[i].flightSegmentTO;
        if (flightSegmentTOObj.flightRefNumber == rph) {
            return flightSegmentTOObj.baggageONDGroupId;
        }
    }

    return null;
}

/*
 * Flight Segments
 *
 */
UI_tabAnci.buildFlightSegments = function (anciType) {
    /* Initialize the Details */
    $("#tbdyAnciSeg").find("tr").remove();
    var objSegDt = null;
    if (anciType == UI_tabAnci.anciType.APSERVICES) {
        objSegDt = jsAPServicesModel;
    } else if (anciType == UI_tabAnci.anciType.APTRANSFER) {
        objSegDt = jsAPTransferModel;
    } else {
        objSegDt = jsAnciSeg;
    }
    
    var intLen = objSegDt.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    var ondFlexiObj = [];

    if (anciType == 'BAGGAGES') {
        ondBaggageObj = getOndBaggageInfoObj();
    }
    if (anciType == 'FLEXI') {
        ondFlexiObj = jsFlexiSeg;
    }
    if (intLen > 0) {
        do {
            if (anciType == 'BAGGAGES') {
                // skip seg wise baggage display for OND
                if ($.inArray(objSegDt[i].fltRefNo, ondBaggageObj.skipRphs) >= 0) {
                    i++;
                    continue;
                }
            }
            if (anciType == 'FLEXI') {
                // skip seg wise flexi and  display for OND
                if (ondFlexiObj.length <= i) {
                    i++;
                    continue;
                }
            }

            strClass = "whiteBackGround";
            strTxtStyle = "";
            strTDClick = UI_tabAnci.tdSegOnClick;

            if( anciType == UI_tabAnci.anciType.APSERVICES || anciType == UI_tabAnci.anciType.APTRANSFER){
                var fltSegRec = objSegDt[i].flightSegmentTO;
                var strDateTime;
                var strMsg;

                if (fltSegRec.airportType == UI_tabAnci.AirportType.ARRIVAL) {
                    strDateTime = UI_tabAnci.formatDateForAirportService(
                        fltSegRec, 4);
                    strMsg = "from";
                } else if (fltSegRec.airportType == UI_tabAnci.AirportType.DEPARTURE || fltSegRec.airportType == UI_tabAnci.AirportType.TRANSIT) {
                    strDateTime = UI_tabAnci.formatDateForAirportService(
                        fltSegRec, 3);
                    strMsg = "up to";
                }
                // var departuretDateNTime =
                // UI_tabAnci.formatDateForAirportService(fltSegRec, 3);
                // var arrivalDateNTime =
                // UI_tabAnci.formatDateForAirportService(fltSegRec, 4);
                strDesc = fltSegRec.airportCode + "<br />" + strMsg + "<br/>" + strDateTime;

                if (objSegDt[i].airportServices.length > 0) {
                    var avail = true;
                } else {
                    var avail = false;
                }

            } else {
                var freeStuf = ""
                    if (anciType == 'MEALS' && objSegDt[i].freeMealEnabled) {
                        freeStuf += "<br/> Free Meal";
                    } else if (anciType == "SEAT_MAP" && objSegDt[i].freeSeatEnabled) {
                        freeStuf += "<br/> Free Seat";
                    }

                    var dateNTime = UI_tabAnci
                        .formatDate(new Date(objSegDt[i].ddt));

                    var avail = UI_tabAnci.isAnciAvailForSeg(anciType, objSegDt[i].fltRefNo);
                    var currentBaggageGroupId = getBaggageONDGrpId(objSegDt[i].fltRefNo);
                    
                    if (anciType == 'BAGGAGES' && currentBaggageGroupId != null && currentBaggageGroupId != "") {
                        strDesc = ondBaggageObj.ondCodes[ondBaggageObj.rphOndMap[objSegDt[i].fltRefNo]] + "<br />" + dateNTime + "" + freeStuf;
                    } else if (anciType == 'FLEXI' && ondFlexiObj.length > 0) {
                        strDesc = ondFlexiObj[i].ondCode + "<br />" + ondFlexiObj[i].dateNTime + "" + freeStuf;
                        if(ondFlexiObj[i] != undefined){
                        	var flexiOndSeq = ondFlexiObj[i].ondSeq;
                        	avail = UI_tabSearchFlights.ondWiseFlexiAvailable[flexiOndSeq];
                        }
                    } else {
                        strDesc = objSegDt[i].orignNDest + "<br />" + objSegDt[i].flightNumber + '<br />' + dateNTime + "" + freeStuf;
                    }
                } 


            if (avail) {
                if (i % 2 != 0) {
                    strClass = " alternateRow";
                }
                if (anciType == UI_tabAnci.anciType.SEAT) {
                    strClass += " anciSeatAvail";
                }
                strClass += " cursorPointer";
            } else {
                strTDClick = "";
                strTxtStyle = "color:#BFBFBF;";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: strDesc,
                id: "tdAnciSeg_" + i,
                click: strTDClick,
                textCss: strTxtStyle,
                css: UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight thinBorderT",
                align: "center"
            }));

            $("#tbdyAnciSeg").append(tblRow,
                $("<tr><td style='height:25px'>&nbsp;</td></tr>"));
            i++;
        } while (--intLen);
    }
}

/*
 * Segment Cell Click
 */
UI_tabAnci.tdSegOnClick = function () {
    var fltRefNo = '';
    // 0 - tdAnciSeg
    // 1 - Row index of Seg;
    var arrData = this.id.split("_");
    var intRow = parseInt(arrData[1],10);
    if (UI_tabAnci.selectedAnciTypeGlobal == UI_tabAnci.anciType.APSERVICES) {
    	fltRefNo = jsAPServicesModel[intRow].flightSegmentTO.flightRefNumber;
    } else if (UI_tabAnci.selectedAnciTypeGlobal == UI_tabAnci.anciType.APTRANSFER) {
        fltRefNo = jsAPTransferModel[intRow].flightSegmentTO.flightRefNumber;
    } else {
    	fltRefNo = jsAnciSeg[intRow].fltRefNo;    	
    }
    
    if (UI_tabAnci.selectedAnciTypeGlobal == UI_tabAnci.anciType.FLEXI) {
    	isFromFlexiTab = false;
    }

    UI_tabAnci.tdSegOnClickHighlight({
        fltRefNo: fltRefNo,
        segRow: intRow
    });
}

/*
 * Highlight Selected Segment Click
 */
UI_tabAnci.tdSegOnClickHighlight = function (p) {
    /* This should not happen! srvice shd b blked from backend! Anyways.. */
    if (p == null) {
        showERRMessage('Selected service not available');
        return;
    }

    var strSegID = p.segID;
    var fltRefNo = p.fltRefNo;
    var segRow = p.segRow;
    var sbgColor = "";
    if (UI_tabAnci.currSegRow != null) {
        sbgColor = (UI_tabAnci.currSegRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdAnciSeg_" + UI_tabAnci.currSegRow).removeClass("rowSelectedAnci");
        $("#tdAnciSeg_" + UI_tabAnci.currSegRow).addClass(sbgColor);
    }

    UI_tabAnci.currFltRefNo = fltRefNo;
    UI_tabAnci.currSegRow = segRow;

    sbgColor = (UI_tabAnci.currSegRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdAnciSeg_" + UI_tabAnci.currSegRow).removeClass(sbgColor);
    $("#tdAnciSeg_" + UI_tabAnci.currSegRow).addClass("rowSelectedAnci");

    if (UI_tabAnci.currAnciSrv != "APSERVICES" && UI_tabAnci.currAnciSrv != "APTRANSFER"
    			&& UI_tabAnci.currAnciSrv != "FLEXI") {
        var dateNTime = UI_tabAnci.formatDate(new Date(jsAnciSeg[UI_tabAnci.currSegRow].ddt));
    }

    switch (UI_tabAnci.currAnciSrv) {
    case "SSR":
        $("#spnSSRPaxSeg").html(
            jsAnciSeg[UI_tabAnci.currSegRow].orignNDest + " " + dateNTime);
        UI_tabAnci.buildSSRPax({
            fltRefNo: fltRefNo
        });
        UI_tabAnci.buildSSR({
            fltRefNo: fltRefNo
        });
        if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.populateModifySSRData({
                fltRefNo: fltRefNo
            });
        }
        UI_tabAnci.populateSSRData({
            segID: segRow
        });
        UI_tabAnci.selectPax(UI_tabAnci.anciType.SSR);
        var carrier = fltRefNo.split("$")[0];
        if (UI_tabAnci.isSSRInvCheckEnabled(carrier)) {
            $("#tblAnciSSR").showCol("avSSRDsp");
        } else {
            $("#tblAnciSSR").hideCol("avSSRDsp");
        }
        break;

	case "AUTOMATICCHECKIN":
    	$("#spnACIPaxSeg").html(jsAnciSeg[UI_tabAnci.currSegRow].orignNDest + " " + dateNTime);
    	UI_tabAnci.setAutoCheckinPassenger();
		UI_tabAnci.constructACIGrid({
			id: "#tblAnciACI"
		});
		UI_tabAnci.buildACI({
            fltRefNo: fltRefNo
        });
		UI_tabAnci.buildACIPax({
            fltRefNo: fltRefNo
        });
		
		if(jsonPaxInfants.length > 0){
			UI_tabAnci.setAutoCheckinPassengerInfant();
			UI_tabAnci.constructACIInfantGrid({
				id: "#tblAnciACIInfant"
			});
			UI_tabAnci.buildACIInfant({
	            fltRefNo: fltRefNo
	        });
		}
		UI_tabAnci.mapCalendar();
		
		if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.populateModifyAutoCheckinData({
                fltRefNo: fltRefNo
            });
        }
		UI_tabAnci.populateAutoCheckinData({
            segID: segRow
        });
		if (UI_tabAnci.isAnciModify || UI_tabAnci.isRequote) {
			UI_tabAnci.modifyReteriveAutoCheckinPaxDetail();
		}
		UI_tabAnci.selectPax(UI_tabAnci.anciType.AUTOMATICCHECKIN);
		UI_tabAnci.autoCheckinFeildMap=UI_tabAnci.getAutoCheckinAdultDetails();
    	break;

    case "MEAL":
        $("#spnMealPaxSeg").html(
            jsAnciSeg[UI_tabAnci.currSegRow].orignNDest + " " + dateNTime);
        var isMultiMealEnabled = UI_tabAnci.setMultiMealEnabled(fltRefNo);
        $("#tblAnciMeal").GridUnload();
        UI_tabAnci.constructMealGrid({
            id: "#tblAnciMeal",
            isMultiMealEnabled: isMultiMealEnabled
        });
        UI_tabAnci.buildMealPax({
            fltRefNo: fltRefNo
        });
        UI_tabAnci.buildMeal({
            fltRefNo: fltRefNo
        });
        if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.populateModifyMealData({
                fltRefNo: fltRefNo
            });
        }
        UI_tabAnci.buildPaxMealHeaders();
        UI_tabAnci.populateMealData({
            segID: segRow
        });
        UI_tabAnci.selectPax(UI_tabAnci.anciType.MEAL);
        break;

    case "BAGGAGE":
        var currentBaggageGroupId = getBaggageONDGrpId(jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo);
        if (currentBaggageGroupId != null && currentBaggageGroupId != '') {
            $("#spnBaggagePaxSeg")
                .html(
                    ondBaggageObj.ondCodes[ondBaggageObj.rphOndMap[jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo]] + " " + dateNTime);
        } else {
            $("#spnBaggagePaxSeg").html(
                jsAnciSeg[UI_tabAnci.currSegRow].orignNDest + " " + dateNTime);
        }
        UI_tabAnci.buildBaggagePax({
            fltRefNo: fltRefNo
        });
        UI_tabAnci.buildBaggage({
            fltRefNo: fltRefNo
        });
        if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.populateModifyBaggageData({
                fltRefNo: fltRefNo
            });
        }
        UI_tabAnci.populateBaggageData({
            segID: segRow
        });
        UI_tabAnci.selectPax(UI_tabAnci.anciType.BAGGAGE);
        break;

    case "SEAT":
        $("#spnSeatPaxSeg").html(
            jsAnciSeg[UI_tabAnci.currSegRow].orignNDest + " " + dateNTime);
        UI_tabAnci.buildSeatPax({
            fltRefNo: fltRefNo
        });
        UI_tabAnci.buildSeatMap({
            fltRefNo: fltRefNo
        });
        $(".autoCrop").autoCrop({
            w: "auto",
            h: "246"
        });
        if (UI_tabAnci.isAnciModify || UI_tabAnci.isRequote) {
            UI_tabAnci.populateModifySeatData();
        }
        UI_tabAnci.populateSMData({
            fltRefNo: fltRefNo
        });
        UI_tabAnci.selectPax(UI_tabAnci.anciType.SEAT);
        break;

    case "APSERVICES":
    	
    	$('#tblAnciAPService').find("input[type='checkbox']:checked").each(function(){
    		$(this).trigger("click");
    	});
    	
        var currRec = jsAPServicesModel[UI_tabAnci.currSegRow];
        dateNTime = UI_tabAnci.formatDateForAirportService(
            currRec.flightSegmentTO, 3);
        $("#spnSeatPaxSeg").html(currRec.airportCode + " " + dateNTime);
        UI_tabAnci.buildAPServicePax({
            fltRefNo: currRec.flightSegmentTO.flightRefNumber,
            airportCode: currRec.flightSegmentTO.airportCode
        });
        UI_tabAnci.buildAPServices({
            fltRefNo: currRec.flightSegmentTO.flightRefNumber,
            airportCode: currRec.flightSegmentTO.airportCode
        });
        if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.populateModifyAPServiceData({
                fltRefNo: fltRefNo,
                airportCode: currRec.flightSegmentTO.airportCode
            });
        }
        UI_tabAnci.populateAPServiceData({
            segID: segRow,
            airportCode: currRec.flightSegmentTO.airportCode
        });
        UI_tabAnci.selectPax(UI_tabAnci.anciType.APSERVICES);
        break;

    case "APTRANSFER":
        var currRec = jsAPTransferModel[UI_tabAnci.currSegRow];
        var fltSeg = currRec.flightSegmentTO;
        if (fltSeg.airportType == UI_tabAnci.AirportType.ARRIVAL) {
            dateNTime = UI_tabAnci.formatDateForAirportService(fltSeg, 4);
        } else if (fltSeg.airportType == UI_tabAnci.AirportType.DEPARTURE || fltSeg.airportType == UI_tabAnci.AirportType.TRANSIT) {
            dateNTime = UI_tabAnci.formatDateForAirportService(fltSeg, 3);
        }
        $("#spnAPTransferPaxSeg").html(currRec.flightSegmentTO.airportCode + " " + dateNTime);
        UI_tabAnci.buildAPTransferPax({
            fltRefNo: currRec.flightSegmentTO.flightRefNumber,
            airportCode: currRec.flightSegmentTO.airportCode
        });
        UI_tabAnci.buildAPTransfers({
            fltRefNo: currRec.flightSegmentTO.flightRefNumber,
            airportCode: currRec.flightSegmentTO.airportCode
        });
        if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.populateModifyAPTransferData({
                fltRefNo: fltRefNo,
                airportCode: currRec.flightSegmentTO.airportCode
            });
        }
        UI_tabAnci.populateAPTransferData({
            segID: segRow,
            airportCode: currRec.flightSegmentTO.airportCode
        });
        UI_tabAnci.selectPax(UI_tabAnci.anciType.APTRANSFER);
        UI_tabAnci.applyAirportLevelControls();
        break;
    case "FLEXI":
    	var ondSequence = 0;
    	if(isFromFlexiTab){
    		ondSequence = jsAnciSeg[UI_tabAnci.currSegRow].ondSequence;
    	} else {
    		ondSequence = jsFlexiSeg[UI_tabAnci.currSegRow].ondSeq;
    	}
        UI_tabAnci.displayONDWiseAnciSelection(ondSequence);
        break;

    case "INS":
        /* NOTHING TO DO! Not dependent on the segments */
        break;
    }

}

UI_tabAnci.setMultiMealEnabled = function (flightRefNo) {
	var isMultiMealEnabled = false;
    if (jsMealModel != null) {
        $.each(jsMealModel, function (i, obj) {
            if (flightRefNo == obj.flightSegmentTO.flightRefNumber) {
            	isMultiMealEnabled = UI_tabAnci.multiMealEnabled && obj.multiMealEnabledForLogicalCabinClass;
            	
//            	if(!isMultiMealEnabled){
//            		$("#btnAnciMealApply").hide();
//                    $("#btnAnciMealApplyToAll").hide();
//            	} else {
//            		$("#btnAnciMealApply").show();
//                    $("#btnAnciMealApplyToAll").show();
//            	}
            	return;
            }
        });
    }
    
    return isMultiMealEnabled;
}

/*
 * Select the pax
 */
UI_tabAnci.selectPax = function (anciType) {
    if (anciType == UI_tabAnci.anciType.SSR) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxSSR_0_0').click();
    } else if (anciType == UI_tabAnci.anciType.MEAL) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxMeal_0_0').click();
    } else if (anciType == UI_tabAnci.anciType.BAGGAGE) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxBaggage_0_0').click();
    } else if (anciType == UI_tabAnci.anciType.SEAT) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxSeat_0_0').click();
    } else if (anciType == UI_tabAnci.anciType.APSERVICES) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxAPService_0_0').click();
    } else if (anciType == UI_tabAnci.anciType.APTRANSFER) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxAPTransfer_0_0').click();
    }else if (anciType == UI_tabAnci.anciType.AUTOMATICCHECKIN) {
        if (jsonPaxAdults.length > 0)
            $('#tdPaxACI_0_0').click();
    }
}

/*
 * Cancel On Click
 */
UI_tabAnci.anciCancelOnClick = function () {
    UI_tabAnci.resetAnciData();
    if (UI_tabAnci.isAnciModify) {
        UI_modifyAncillary.loadHomePage();
    } else if (UI_tabAnci.isSegModify) {
        UI_modifySegment.openTab(0);
    } else if (UI_tabAnci.isAddSegment) {
        UI_addSegment.openTab(0);
    } else if (UI_tabAnci.isRequote) {
        UI_modifyResRequote.openTab(0);
    } else {
        UI_makeReservation.openTab(1);
    }
}

/*
 * View On Click
 */
UI_tabAnci.anciViewOnClick = function () {

    var insurance = null;
    var insuranceAmount = 0;
    var insIndex = -1;
    
    for (var i=0; i < UI_tabAnci.anciInsurances.length; i++){
    	var ins = UI_tabAnci.anciInsurances[i];
    	if(ins.selected){
    		insuranceAmount += parseFloat(ins.quotedTotalPremiumAmount);
    		if(insIndex == -1 ){
    			 insurance = $.airutil.dom.cloneObject(UI_tabAnci.anciInsurances[i]);
    			 insIndex = i;
    		}
    	}
    }

    if (insurance != null) {
        insurance.origin = insurance.insuredJourney.journeyStartAirportCode;
        insurance.destination = insurance.insuredJourney.journeyEndAirportCode;
        insurance.dateOfTravel = UI_tabAnci._getDate(
            insurance.insuredJourney.journeyStartDate).getTime();
        insurance.dateOfReturn = UI_tabAnci._getDate(
            insurance.insuredJourney.journeyEndDate).getTime();
        insurance.total = insuranceAmount;
    }

    UI_tabAnciSummary.show(UI_tabAnci.generatePaxWiseAnci(), insurance,
        UI_tabAnci.focMealEnabled);
}

UI_tabAnci.checkForFreeSeatSelection = function () {
    var response = {
        result: true,
        message: '',
        segIndex: 0,
        fltRefNo: ''
    };
    for (var anciSegIndex = 0; anciSegIndex < jsAnciSeg.length; anciSegIndex++) {
        var anciSegObj = jsAnciSeg[anciSegIndex];
        var fltRefNo = anciSegObj.fltRefNo;
        if (UI_tabAnci.fltWiseFreeSeatMap[fltRefNo]) {
            for (var paxIndex = 0; paxIndex < anciSegObj.pax.length; paxIndex++) {
                var paxObj = anciSegObj.pax[paxIndex];
                if (paxObj.seat.seatNumber == null || paxObj.seat.seatNumber == '') {
                    response.result = false;
                    response.message = raiseError("XBE-ERR-84",
                        anciSegObj.segID, anciSegObj.flightNumber);
                    response.segIndex = anciSegIndex;
                    response.fltRefNo = fltRefNo;
                    return response;
                }
            }
        }
    }

    return response;
}

UI_tabAnci.checkForFreeMealSelection = function () {
    var response = {
        result: true,
        message: '',
        segIndex: 0,
        fltRefNo: ''
    };
    for (var anciSegIndex = 0; anciSegIndex < jsAnciSeg.length; anciSegIndex++) {
        var anciSegObj = jsAnciSeg[anciSegIndex];
        var fltRefNo = anciSegObj.fltRefNo;
        if (UI_tabAnci.fltWiseFreeMealMap[fltRefNo]) {
            for (var paxIndex = 0; paxIndex < anciSegObj.pax.length; paxIndex++) {
                var paxObj = anciSegObj.pax[paxIndex];
                if (paxObj.meal.mealQtyTotal == null || parseInt(paxObj.meal.mealQtyTotal) == 0) {
                    response.result = false;
                    response.message = raiseError("XBE-ERR-85",
                        anciSegObj.segID, anciSegObj.flightNumber);
                    response.segIndex = anciSegIndex;
                    response.fltRefNo = fltRefNo;
                    return response;
                }
            }
        }
    }

    return response;
}

UI_tabAnci.anciContinueOnClick = function(){
	
	if((jsonFareQuoteSummary.totalServiceTax != "" || jsonFareQuoteSummary.adminFee !=""|| top.gstAppliedForReservation) && !UI_tabAnci.isRequote){
		UI_tabAnci.continueToServiceTaxCalc = false;
		
		if (UI_tabAnci.isAnciModify) {
			UI_tabAnci.submitAnciModificationForGSTCalculation();
		}else{
			UI_tabAnci.submitAnciForGSTCalculation();
		}
	}else{
		UI_tabAnci.anciContinueConfirm();
	}
}


/*
 * Continue On Click
 */
UI_tabAnci.anciContinueConfirm = function () {
    if ((UI_tabAnci.anciInsuranceTCCheck != undefined && UI_tabAnci.anciInsuranceTCCheck) && !$("#chkAccept").attr("checked") && $('#chkAnciIns').attr("checked")) {
        alert("Can not continue without accepting the Insurance Trems & Conditions");
        return false;
    }

    if (carrierCode == "Z2" && (UI_tabAnci.anciInsuranceTCCheck != undefined && UI_tabAnci.anciInsuranceTCCheck) && $("#chkAccept").attr("checked") && $('#chkAnciIns').attr("checked") && !$('#chkAgentAgreement').attr("checked")) {
        alert("Accept agent terms to continue");
        return false;
    }
    
    if (UI_tabAnci.isBundleFarePopupEnabled) {
    	// return free anci list for selected fare bundles
        var map = new Object();
        map = UI_tabAnci.returnFareBundleFreeAnci(UI_tabSearchFlights.ondWiseBundleFareSelected, UI_tabSearchFlights.resAvailableBundleFareLCClassStr);
        var strTemp = '';
        var bundleFareNotSelectedFreeAnci = new Object();
        
        var ancillarySegments = [];
        ancillarySegments = jsAnciSeg;
        
        if(!(Object.keys(map).length === 0)){
        	var alertFareBundlePopup = false;
        	for (var i = 0; i < ancillarySegments.length; i++) {
            	
            	var paxList = [];
            	paxList = ancillarySegments[i].pax;
            	for (var j = 0; j < paxList.length; j++) {
            		var pax = paxList[j];
            		 Object.keys(pax).forEach(function(key,index) {
            			 switch(key) {
            			    case 'meal':
            			    	if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.MEAL) && map[ancillarySegments[i].ondSequence] != undefined && map[ancillarySegments[i].ondSequence].MEALS != undefined && map[ancillarySegments[i].ondSequence].MEALS=='MEALS' && pax.meal.meals.length == 0){
            	    				 alertFareBundlePopup = true;
            	    				 bundleFareNotSelectedFreeAnci['MEALS'] = 'Meals';
            	    			}
            			        break;
            			    case 'baggage':
            			    	if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.BAGGAGE) && map[ancillarySegments[i].ondSequence] != undefined && map[ancillarySegments[i].ondSequence].BAGGAGES != undefined && map[ancillarySegments[i].ondSequence].BAGGAGES=='BAGGAGES' && pax.baggage.baggages.length == 0){
            	    				 alertFareBundlePopup = true;
            	    				 bundleFareNotSelectedFreeAnci['BAGGAGES'] = 'Baggages';
            	    			}
            			        break;
            			    case 'seat':
            			    	if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.SEAT) && map[ancillarySegments[i].ondSequence] != undefined && map[ancillarySegments[i].ondSequence].SEAT_MAP != undefined && map[ancillarySegments[i].ondSequence].SEAT_MAP=='SEAT_MAP' && pax.seat.seatNumber == ""){
            	    				 alertFareBundlePopup = true;
            	    				 bundleFareNotSelectedFreeAnci['SEAT_MAP'] = 'Seats';
            	    			}
            			        break;
            			    case 'aps':
            			    	if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.APSERVICES) && map[ancillarySegments[i].ondSequence] != undefined && map[ancillarySegments[i].ondSequence].AIRPORT_SERVICE != undefined && map[ancillarySegments[i].ondSequence].AIRPORT_SERVICE=='AIRPORT_SERVICE' && pax.aps.apss.length == 0){
            	    				 alertFareBundlePopup = true;
            	    				 bundleFareNotSelectedFreeAnci['AIRPORT_SERVICE'] = 'Airport Services';
            	    			}
            			        break;
            			    case 'apt':
            			    	if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.APTRANSFER) && map[ancillarySegments[i].ondSequence] != undefined && map[ancillarySegments[i].ondSequence].AIRPORT_TRANSFER != undefined && map[ancillarySegments[i].ondSequence].AIRPORT_TRANSFER=='AIRPORT_TRANSFER' && pax.apt.apts.length == 0){
            	    				 alertFareBundlePopup = true;
            	    				 bundleFareNotSelectedFreeAnci['AIRPORT_TRANSFER'] = 'Airport Transfer';
            	    			}
            			        break;
            			    case 'ssr':
            			    	if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.SSR) && map[ancillarySegments[i].ondSequence] != undefined && map[ancillarySegments[i].ondSequence].SSR != undefined && map[ancillarySegments[i].ondSequence].SSR=='SSR' && pax.ssr.ssrs.length == 0){
            	    				 alertFareBundlePopup = true;
            	    				 bundleFareNotSelectedFreeAnci['SSR'] = 'SSR';
            	    			}
            			        break;
            			} 
            			 
            		 });
            		
            	}

                if (UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[i].ancillaryStatusList, UI_tabAnci.anciType.FLEXI)
                    && map[ancillarySegments[i].ondSequence] != undefined
                    && map[ancillarySegments[i].ondSequence].FLEXI != undefined
                    && map[ancillarySegments[i].ondSequence].FLEXI=='FLEXI'
                    && UI_tabSearchFlights.ondWiseFlexiAvailable[ancillarySegments[i].ondSequence]
                    && !UI_tabAnci.mergeFlexiSelection()[ancillarySegments[i].ondSequence]
                    && UI_tabAnci.shouldFlexiTabVisible()) {

                    alertFareBundlePopup = true;
                    bundleFareNotSelectedFreeAnci['FLEXI'] = 'Flexi';
                }
            }
            
            var bundleFareNotSelectedFreeAnciArray = $.map(bundleFareNotSelectedFreeAnci, function(value, index) {
                return [value];
            });
           
            strTemp = bundleFareNotSelectedFreeAnciArray.join(", ");
            
            bundleFareNotSelectedFreeAnci = {};
            
            if (alertFareBundlePopup){
            	alert('The fare you have selected includes the following extras: ' + strTemp.toString() + '. Please make your selection to continue.' );
            	return false;
            }
        	
        }
    }
    
    var seatSelectionResponse = UI_tabAnci.checkForFreeSeatSelection();
    if (!seatSelectionResponse.result) {
        $('#freeAnciTitle').text('Free Seat Selection Notification');
        $('#lblFreeAnciMessage').text(seatSelectionResponse.message);
        $('#divFreeAnciNotification').show();
        UI_tabAnci.navigateToAnciTab(UI_tabAnci.anciTabID.SEAT);
        UI_tabAnci.tdSegOnClickHighlight({
            fltRefNo: seatSelectionResponse.fltRefNo,
            segRow: seatSelectionResponse.segIndex
        });
        UI_commonSystem.showProgress();
        return;
    }

    var mealSelectionResponse = UI_tabAnci.checkForFreeMealSelection();
    if (!mealSelectionResponse.result) {
        $('#freeAnciTitle').text('Free Meal Selection Notification');
        $('#lblFreeAnciMessage').text(mealSelectionResponse.message);
        $('#divFreeAnciNotification').show();
        UI_tabAnci.navigateToAnciTab(UI_tabAnci.anciTabID.MEAL);
        UI_tabAnci.tdSegOnClickHighlight({
            fltRefNo: mealSelectionResponse.fltRefNo,
            segRow: mealSelectionResponse.segIndex
        });
        UI_commonSystem.showProgress();
        return;
    }

    var anciSum = UI_tabAnci.anciSummary;
    var str = '';
    var count = 0;

    var blockSeats = true;
    UI_tabAnci.atLeastOneSelected = false;
    var insuraceSelected = false;
    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].stat == 'ACT' && (anciSum[i].qty + anciSum[i].old.qty) == 0) {
            if (count > 0) {
                str += ', ';
            } else {
                UI_tabAnci.anciNSFirst = anciSum[i];
            }
            if (anciSum[i].type == UI_tabAnci.anciType.SEAT) {
                blockSeats = false;
            }
            str += anciSum[i].name;
            count++;
        }
        if ((anciSum[i].qty + anciSum[i].old.qty) > 0) {
            if (anciSum[i].type == UI_tabAnci.anciType.INS) {
                insuraceSelected = true;
            }
            UI_tabAnci.atLeastOneSelected = true;
        }
    }
    UI_tabAnci.blockSeats = blockSeats;
   
    var availableCheaperBundledfares = UI_tabAnci.CheckForCheaperBundleFare();
    
    //Adding OND wise support
    _getOndSegments = function (objSegDt) {    	
        var ondSegInfo = {};
        $.each(objSegDt, function (index, segmentInfo) {   
        	var ondSeq = segmentInfo.ondSequence;    	
        	
        	if(ondSegInfo[ondSeq] == undefined || ondSegInfo[ondSeq] == null){
        		ondSegInfo[ondSeq] = segmentInfo.orignNDest;
        	} else {
        		var currentOndCode = ondSegInfo[ondSeq];
        		currentOndCode += segmentInfo.orignNDest.substring(3, 7);
        		ondSegInfo[ondSeq] = currentOndCode;
        	}
        });
        
        return ondSegInfo;

    }
    
    var ondSegmentInfo = _getOndSegments(jsAnciSeg);
    
    var cheaperBundleFareAvailable = false;
    var strCheaperBundledFareNotifi = "";
    for ( var i = 0; i < availableCheaperBundledfares.length; i++) {
		var ondCheaperBunldesObj = availableCheaperBundledfares[i];
		if (ondCheaperBunldesObj != undefined && ondCheaperBunldesObj.length > 0
				&& ondSegmentInfo[i] != undefined && ondSegmentInfo[i] != null) {
	    	cheaperBundleFareAvailable = true;
	    	strCheaperBundledFareNotifi += "OnD - " + ondSegmentInfo[i] + ": ";
	        $.each(ondCheaperBunldesObj, function (index, ancillary) {
	            strCheaperBundledFareNotifi += ancillary + " , ";
	        });
	        strCheaperBundledFareNotifi += "<br/>";
	    }
	}
    
    if (cheaperBundleFareAvailable) {
        $('#spanCheapBundledFare').html(strCheaperBundledFareNotifi);
        if (count == 0) {
            $('#divAnciContConfirm').show();
        }

    } else {
    	$('#spanCheapBundledFare').html('');
        $('#lblCheapBundledFare').hide();
    }

    if (count > 0) {
        UI_commonSystem.showProgress();
        if (UI_tabAnci.isAnciModify) {
            if (UI_tabAnci.atLeastOneSelected) {
                if (UI_tabAnci
                    .isNothingToSave(UI_tabAnci.generatePaxWiseAnci()) && !insuraceSelected) {
                    UI_tabAnci.atLeastOneSelected = false;
                    $('#lblAnciCtdConfirm').text(
                        'None of the ancillaries were changed.');
                    str = '';
                    $('#divAnciContConfirm').show();
                    $('#spnNotSelectedAnci').html(str);
                } else {
                    UI_tabAnci.confirmAncillaryModification();
                }
            } else {
                $('#lblAnciCtdConfirm').text(
                    'None of the ancillaries were changed.');
                str = '';
                $('#divAnciContConfirm').show();
                $('#spnNotSelectedAnci').html(str);
            }
        } else {
        	$('#divAnciContConfirm').show();
            $('#spnNotSelectedAnci').html(str);
        }

    } else {
        UI_tabAnci.anciNSFirst = null;
        $('#divAnciContConfirm').hide();
        if (UI_tabAnci.isAnciModify) {
            UI_tabAnci.confirmAncillaryModification();
        } else {
            UI_tabAnci.submitSeatsForBlocking(blockSeats && (!UI_tabAnci.seatsBlocked));
        }
    }
}

UI_tabAnci.isNothingToSave = function (pax) {
    var count = 0;
    for (var i = 0; i < pax.length; i++) {
        for (var j = 0; j < pax[i].anci.length; j++) {
            if (pax[i].anci[j].seat.seatNumber != '') {
                count++;
            }
			if (pax[i].anci[j].automaticCheckin.seatPreference != '' || pax[i].anci[j].automaticCheckin.seatCode != '') {
                count++;
            }
            count += pax[i].anci[j].meal.meals.length;
            count += pax[i].anci[j].ssr.ssrs.length;
            count += pax[i].anci[j].baggage.baggages.length;
            count += pax[i].anci[j].aps.apss.length;
            count += pax[i].anci[j].apt.apts.length;
        }
        for (var j = 0; j < pax[i].removeAnci.length; j++) {
            if (pax[i].removeAnci[j].seat.seatNumber != '') {
                count++;
            }
			if (pax[i].removeAnci[j].automaticCheckin.seatPreference != '' || pax[i].anci[j].automaticCheckin.seatCode != '') {
                count++;
            }
            count += pax[i].removeAnci[j].meal.meals.length;
            count += pax[i].removeAnci[j].ssr.ssrs.length;
            count += pax[i].removeAnci[j].baggage.baggages.length;
            count += pax[i].removeAnci[j].aps.apss.length;
            count += pax[i].removeAnci[j].apt.apts.length;
        }
    }
    if (count == 0) {
        return true;
    } else {
        return false;
    }
}

/*
 * Return free anci for fare bundle
 */
UI_tabAnci.returnFareBundleFreeAnci = function (ondWiseBundleFareSelected, resAvailableBundleFareLCClassStr) {
	var map = new Object(); // or var map = {};
	
	Object.keys(ondWiseBundleFareSelected).forEach(function(key,index) {
		var farebundlePeriodID = ondWiseBundleFareSelected[key];
        
		if (farebundlePeriodID != null){
			var resBundleFares = {};
	        	resBundleFares = resAvailableBundleFareLCClassStr;
			
				var bundleFareList = {};
				var bundleFareList = resBundleFares[key];
				
				var fareBundleAnciList = new Object();
				
				if (bundleFareList != undefined){
					for (var j=0;j < bundleFareList.length; j++){
						var bundleFare = [];
						bundleFare = bundleFareList[j];
						for (var k=0;k<bundleFare.length;k++){
							if (bundleFare[k].bundledFarePeriodId == farebundlePeriodID){
								var selectedBundleFare = bundleFare[k];
								var bundleFareAnci = selectedBundleFare.bundleFareFreeServices;
								
								for (var l=0;l<bundleFareAnci.length;l++){
									var anci = bundleFareAnci[l];
									if (anci == 'MEAL'){
			            				fareBundleAnciList['MEALS'] = 'MEALS';
			            			}
			            			else if (anci == 'BAGGAGE'){
			            				fareBundleAnciList['BAGGAGES'] = 'BAGGAGES';
			            			}
			            			else if (anci == 'FLEXI_CHARGES'){
			            				fareBundleAnciList['FLEXI'] = 'FLEXI';
			            			}
			            			else{
			            				fareBundleAnciList[anci] = anci;
			            			}
								}
							}
						}
					}
				}
				
	            map[key] = fareBundleAnciList;
		}
	});

	return map;
}

/*
 * Release ancillary
 */
UI_tabAnci.releaseBlockedAnci = function () {
    UI_tabAnci.releaseBlockedSeats({
        reset: true,
        clearSavedAnci: true
    });
}
/*
 * Seat releasing call
 */
UI_tabAnci.releaseBlockedSeats = function (p) {
    if (UI_tabAnci.seatsBlocked) {
        var data = {};
        UI_tabAnci.seatsBlocked = false;

        data['selectedAncillaries'] = UI_tabAnci.seatsForBlocking();
        data['clearSavedAnci'] = p.clearSavedAnci;
        data['pnr'] = UI_tabAnci.pnr;
        data['searchSystem'] = UI_tabSearchFlights.getSelectedSystem();;
        if (p.reset != null && p.reset) {
            UI_commonSystem.getDummyFrm().ajaxSubmit({
                dataType: 'json',
                processData: false,
                data: data,
                url: "anciRelease.action",
                success: function (response) {
                    UI_tabAnci.resetSeatData();
                },
                error: UI_commonSystem.setErrorStatus
            });
        } else {
            UI_commonSystem.getDummyFrm().ajaxSubmit({
                dataType: 'json',
                processData: false,
                data: data,
                url: "anciRelease.action",
                success: function (response) {},
                error: UI_commonSystem.setErrorStatus
            });
        }

    }

}

UI_tabAnci.confirmAncillaryModification = function () {
    UI_modifyAncillary.confirmAncillaryModification();
}

/*
 * Seat blocking call
 */
UI_tabAnci.submitSeatsForBlocking = function (blockSeats) {
	$('#divAnciContConfirm').hide();
    var dataStat = false;
    if (UI_tabAnci.isSegModify) {
        dataStat = UI_modifySegment.tabDataRetrive.tab1;
    } else if (UI_tabAnci.isAddSegment) {
        dataStat = UI_addSegment.tabDataRetrive.tab1;
    } else if (UI_tabAnci.isRequote) {
        dataStat = UI_modifyResRequote.tabDataRetrive.tab1;
    } else {
        dataStat = UI_makeReservation.tabDataRetrive.tab2;
    }
    if (dataStat) {
    	var data = null;
    	if(UI_tabSearchFlights.isCalenderMinimumFare()){
    		data = UI_MinimumFare.createSearchParams('searchParams');
    	}else{
    		data = UI_tabSearchFlights.createSearchParams('searchParams');
    	}
        
        if (UI_tabAnci.isRequote) {
        	data['groupPnr'] = DATA_ResPro.reservationInfo.groupPNR;
            data['pnr'] = UI_tabAnci.pnr;
            data['requote'] = true;
            data['selectedFlightList'] = $.toJSON(UI_requote
                .populateFlightSegmentList());
            data['confExistingFlightSegs'] = $.toJSON(UI_requote.getConfirmedExistingFlightSegments());
        } else if (UI_tabSearchFlights.isMulticity()) {
            data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
        } else if(UI_tabSearchFlights.isCalenderMinimumFare()){
        	data['selectedFlightList'] = $.toJSON(UI_MinimumFare
                    .getFlightRPHList());
        }else {
            data['selectedFlightList'] = $.toJSON(UI_tabSearchFlights
                .getFlightRPHList(true));
        }
        
        data['paxWiseAnci'] = $.toJSON(UI_tabAnci.generatePaxWiseAnci(true));
        data['insurances'] = $.toJSON(UI_tabAnci.anciInsurances);
        data['insurableFltRefNumbers'] = $.toJSON(UI_tabAnci.insurableFltRefNumbers);
        data['blockSeats'] = blockSeats;
        data['selectedSeats'] = UI_tabAnci.seatsForBlocking();
        data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
        data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
        data['ondwiseFlexiSelected'] = $.toJSON(UI_tabAnci.mergeFlexiSelection());
        data['paxCountryCode'] = $("#selCountry").val();
        data['paxState'] = $("#selState").val();
        var taxRegNo = $("#txtTaxRegNo").val();
        data['paxTaxRegistered'] = (taxRegNo == null || taxRegNo == '') ? false:true;
        UI_tabSearchFlights.paxState = data['paxState'];
        UI_tabSearchFlights.paxCountryCode = data['paxCountryCode'];
        UI_tabSearchFlights.paxTaxRegistered = data['paxTaxRegistered'];
        UI_tabSearchFlights.paxRegNo = taxRegNo;

        if (UI_tabAnci.isSegModify) {
            data['confExistingFlightSegs'] = $.toJSON(UI_modifySegment
                .getInsuranceFlightSegments());

            if (UI_modifySegment.modifyingFltSegHash != null) {
                var modifyingFltRefNumbers = [];
                var objCount = 0;
                for (fltKey in UI_modifySegment.modifyingFltSegHash) {
                    modifyingFltRefNumbers[objCount] = fltKey;
                    objCount++;
                }
                data['modifyingFltRefNumbers'] = $
                    .toJSON(modifyingFltRefNumbers);
                data['resPaxInfo'] = $.toJSON(UI_modifySegment.resPaxInfo);
            }

            data['pnr'] = UI_tabAnci.pnr;
        } else if (UI_tabAnci.isAddSegment) {
            data['confExistingFlightSegs'] = $.toJSON(UI_addSegment
                .getInsuranceFlightSegments());
            data['pnr'] = UI_tabAnci.pnr;
        }

        data['isOnd'] = isONDBaggage;

        UI_commonSystem.showProgress();

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "anciBlocking.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_tabAnci.seatsBlocked = false;
                    UI_commonSystem.hideProgress();
                    /* SEAT BLOCKING FAILED */
                    UI_tabAnci.resetDataOnBlockError();
                    UI_tabAnci.onClickSeat({
                        override: true
                    });
                    UI_tabAnci.tabClick({
                        id: 2
                    });
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.seatsBlocked = true;
                    if (UI_tabAnci.onHold && DATA_ResPro.resPaxs == undefined) {
                    	/**
                    	 * 
                    	 */
                        UI_tabAnci.onHoldSubmit(UI_tabAnci.waitListing);
                    } else {
                    	/**
                    	 * 
                    	 */
                        UI_tabAnci.onReservationSubmit();
                    }
                }
            },
            error: UI_commonSystem.setErrorStatus
        });

        $("#trFareSection").slideDown('fast');
        $("#divFareQuotePane").slideDown('fast');

    } else {
        showERRMessage('Seat blocking submit failed');
    }
}

UI_tabAnci.submitAnciForGSTCalculation = function () {

        var data = UI_tabSearchFlights.createSearchParams('searchParams');
        if (UI_tabAnci.isRequote) {
        	data['groupPnr'] = DATA_ResPro.reservationInfo.groupPNR;
            data['pnr'] = UI_tabAnci.pnr;
            data['requote'] = true;
            data['selectedFlightList'] = $.toJSON(UI_requote
                .populateFlightSegmentList());
            data['confExistingFlightSegs'] = $.toJSON(UI_requote.getConfirmedExistingFlightSegments());
        } else if (UI_tabSearchFlights.isMulticity()) {
            data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
        } else {
            data['selectedFlightList'] = $.toJSON(UI_tabSearchFlights
                .getFlightRPHList(true));
        }
        
        data['paxWiseAnci'] = $.toJSON(UI_tabAnci.generatePaxWiseAnci(true));
        data['insurances'] = $.toJSON(UI_tabAnci.anciInsurances);
        data['insurableFltRefNumbers'] = $.toJSON(UI_tabAnci.insurableFltRefNumbers);
        data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
        data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
        data['ondwiseFlexiSelected'] = $.toJSON(UI_tabAnci.mergeFlexiSelection());
        data['paxCountryCode'] = $("#selCountry").val();
        data['paxState'] = $("#selState").val();
        var taxRegNo = $("#txtTaxRegNo").val();
        data['paxTaxRegistered'] = (taxRegNo == null || taxRegNo == '') ? false:true;
        UI_tabSearchFlights.paxState = data['paxState'];
        UI_tabSearchFlights.paxCountryCode = data['paxCountryCode'];
        UI_tabSearchFlights.paxTaxRegistered = data['paxTaxRegistered'];
        UI_tabSearchFlights.paxRegNo = taxRegNo;

        if (UI_tabAnci.isSegModify) {
            data['confExistingFlightSegs'] = $.toJSON(UI_modifySegment
                .getInsuranceFlightSegments());

            if (UI_modifySegment.modifyingFltSegHash != null) {
                var modifyingFltRefNumbers = [];
                var objCount = 0;
                for (fltKey in UI_modifySegment.modifyingFltSegHash) {
                    modifyingFltRefNumbers[objCount] = fltKey;
                    objCount++;
                }
                data['modifyingFltRefNumbers'] = $
                    .toJSON(modifyingFltRefNumbers);
                data['resPaxInfo'] = $.toJSON(UI_modifySegment.resPaxInfo);
            }

            data['pnr'] = UI_tabAnci.pnr;
        } else if (UI_tabAnci.isAddSegment) {
            data['confExistingFlightSegs'] = $.toJSON(UI_addSegment
                .getInsuranceFlightSegments());
            data['pnr'] = UI_tabAnci.pnr;
        }

        data['isOnd'] = isONDBaggage;

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "ancillaryGSTCalculation.action",
            success: function (response) {
                if (response != null && response.success!=null && response.success) {   
                	var gstConfStr = "";
                	if(jsonFareQuoteSummary.totalServiceTax != "" ||jsonFareQuoteSummary.adminFee !="" || top.gstAppliedForReservation){
                		UI_tabAnci.salesTaxForFare = response.totalServiceTaxAmountForFare;
                		UI_tabAnci.salesTaxForServices = response.totalServiceTaxAmountForAncillary;
						UI_tabAnci.adminFee = response.adminFee;
						UI_tabAnci.selectedCurrencyAdminFee= response.selectedCurrencyAdminFee;
						
						if(jsonFareQuoteSummary.totalServiceTax != "") {
							var totalGST = response.totalServiceTaxAmountForAncillary + response.totalServiceTaxAmountForFare;
	                		gstConfStr = totalGST.toFixed(2) + ' ' + jsonFareQuoteSummary.currency;
	                		$('#divAnciGSTConfirm').show();
	                        $('#spnGSTAnciMsg').html(gstConfStr);	
						}
						
                        UI_tabAnci.buildPriceSummary();
                }
              }
            },
            error: UI_commonSystem.setErrorStatus
        });
}

UI_tabAnci.submitAnciModificationForGSTCalculation = function () {

	var data = {};
	data['pnr'] = DATA_ResPro.pnrNo;
	data['groupPNR'] = DATA_ResPro.groupPNRNo;
	data['selCurrency'] = DATA_ResPro.selCurrency;		
	data['flightRPHList']= $.toJSON(DATA_ResPro.flightRPHList);
	data['resPaxs']= $.toJSON(DATA_ResPro.resPaxs);
	data['paxWiseAnci'] = $.toJSON(UI_tabAnci.generatePaxWiseAnci());
	data['insurances'] =  $.toJSON(UI_tabAnci.anciInsurances);
	data['insurableFltRefNumbers'] = $.toJSON(UI_tabAnci.insurableFltRefNumbers);
	data['resPaySummary'] =  $.toJSON(DATA_ResPro.resPaySummary);
	data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
	
	UI_commonSystem.getDummyFrm().ajaxSubmit({
        dataType: 'json',
        processData: false,
        data: data,
        url: "modifyAnciGSTCalculation.action",
        success: function (response) {
            if (response != null && response.success!=null && response.success) {   
            	var gstConfStr = "";
            	if(jsonFareQuoteSummary.totalServiceTax != "" ||jsonFareQuoteSummary.adminFee !="" || top.gstAppliedForReservation){
            		UI_tabAnci.salesTaxForFare = response.totalServiceTaxAmountForFare;
            		UI_tabAnci.salesTaxForServices = response.totalServiceTaxAmountForAncillary;
					UI_tabAnci.adminFee = response.adminFee;
					UI_tabAnci.selectedCurrencyAdminFee= response.selectedCurrencyAdminFee;
					
					if(jsonFareQuoteSummary.totalServiceTax != "") {
	            		var totalGST = response.totalServiceTaxAmountForAncillary + response.totalServiceTaxAmountForFare;
	            		gstConfStr = totalGST.toFixed(2) + ' ' + DATA_ResPro.resPaySummary.currency;
	            		$('#divAnciGSTConfirm').show();
	            		$('#spnGSTAnciMsg').html(gstConfStr);
	                    $('#spnGSTAnciModifyMsg').html("(For Difference)");						
					}
                    UI_tabAnci.buildPriceSummary();
            }
          }
        },
        error: UI_commonSystem.setErrorStatus
    });
 
}


/*
 * Submit reservation
 */
UI_tabAnci.onReservationSubmit = function () {
    if (UI_tabAnci.isSegModify && UI_modifySegment.tabDataRetrive.tab1) {
        UI_commonSystem.hideProgress();
        UI_modifySegment.showConfirmUpdate();
    } else if (UI_tabAnci.isAddSegment && UI_addSegment.tabDataRetrive.tab1) {
        UI_addSegment.capturePaymentFromAnci();
    } else if (UI_tabAnci.isRequote && UI_modifyResRequote.tabDataRetrive.tab1) {
        UI_commonSystem.hideProgress();
        UI_requote.balanceQuery();
    } else if (UI_makeReservation.tabDataRetrive.tab2) {
		if (UI_tabSearchFlights.isCalenderMinimumFare()) {
			data2 = UI_MinimumFare.createSearchParams('searchParams');
		} else {
			var data2 = UI_tabSearchFlights.createSearchParams('searchParams');
		}
        // data2["selectedAncillaries"]=$.toJSON(jsAnciSeg);
        // data2['paxWiseAnci'] = $.toJSON(UI_tabAnci.generatePaxWiseAnci());
        // data2['flightSearchDetails']=$.toJSON(UI_tabSearchFlights.objResSrchParams);
        // data2['paxAdults'] = UI_tabAnci.createAdultPaxInfo();
        // data2['paxInfants'] = UI_tabAnci.createInfantPaxInfo();
        data2['selCurrency'] = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
        data2['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
        data2['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
        UI_commonSystem.showProgress();
        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data2,
            url: "paymentTab.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.airportMessage = response.airportMessage;
                    UI_tabAnci.loadAirportMessage();
                    UI_tabAnci.continueToPaymentSuccess(response);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        UI_tabAnci.continueToPaymentSuccess(null);
    }
}
/*
 * Submit on hold
 */
UI_tabAnci.onHoldSubmit = function (isWaitListing) {
    $("#frmMakeBkg").attr('target', "_self");
    $("#txtPayAmt").enable();
    $('#txtPayAmt').val("0.00");
    $('#radPaymentTypeA').attr('checked', true);
    $('#txtPayMode').val("");
    var data = {};

    data['paxAdults'] = UI_tabPassenger.createAdultPaxInfo();
    data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();
    data['onHoldBooking'] = "true";
    data['waitListingBooking'] = isWaitListing;
    data['paxPayments'] = $.toJSON(jsonPaxPayObj);
    var $inputs = $('#frmPayment :input');
    $inputs.each(function () {
        if (this.name == "selLang") {
            data[this.name] = 'en';
        } else {
            data[this.name] = $(this).val();
        }

    });

    if (top.arrPrivi[PRIVI_LOAD_PROFILE] == 1) {
        if ($("#chkUpdateCustProf").attr("checked")) {
            data['customerProfileUpdate'] = true;
        } else {
            data['customerProfileUpdate'] = false;
        }
    } else {
        data['customerProfileUpdate'] = false;
    }

    // Overwrite payment values that were grabbed from form "frmMakeBkg"
    var selectedAgent = $('#selSalesPoint').val();
    if (!selectedAgent) {
        // If no agent is selected, or the drop down box is disabled, then
        // use the logged in agent
        selectedAgent = top.strAgentCode;
    }
    data['payment.agent'] = selectedAgent;
    data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
    // not getting set setting the value for agents who doesn't have on
    // account payments
    data['payment.type'] = "ACCO";
    data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
    data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
    data['selectedBookingCategory'] = $('#selBookingCategory').val();
    
   // data['blackListPnrpaxList'] = UI_tabPassenger.blackListedPaxList;
    data['reasonForAllowBlPax'] = UI_tabPassenger.reasonForAllowBlPax;
    
    if (top.arrPrivi[PRIVI_ITINERARY_FARE_MASK] == 1) {
        data['itineraryFareMaskOption'] = $('#selItineraryFareMask').val();
    } else {
        data['itineraryFareMaskOption'] = 'N';
    }

    var searchData = {};
    if (UI_tabSearchFlights.isCalenderMinimumFare()) {
    	searchData = UI_MinimumFare.createSearchParams('searchParams');
	} else {
		searchData = UI_tabSearchFlights.createSearchParams('searchParams');
	}
   
    if (top.arrPrivi[PRIVI_ADD_CALL_ORIGIN_COUNTRY] == 1) {
        data['selectedOriginCountry'] = $('#selOriginCountryOfCall').val();
    } else {
        data['selectedOriginCountry'] = '';
    }

    var searchData = UI_tabSearchFlights.createSearchParams('searchParams');
    data = $.airutil.dom.concatObjects(data, searchData);
    data = $.airutil.dom.concatObjects(data, UI_tabSearchFlights
        .inOutFlightRPHList())
    data['paxState'] = UI_tabSearchFlights.paxState;
	data['paxCountryCode'] = UI_tabSearchFlights.paxCountryCode;
	data['paxTaxRegistered'] = UI_tabSearchFlights.paxTaxRegistered;    

    UI_commonSystem.showProgress();
    $("#frmMakeBkg").ajaxSubmit({
        dataType: 'json',
        processData: false,
        data: data,
        url: "confirmationTab.action",
        success: UI_tabAnci.onHoldOnSuccess,
        error: UI_commonSystem.setErrorStatus
    });
}

/*
 * OnHold on success
 */
UI_tabAnci.onHoldOnSuccess = function (response) {

    UI_commonSystem.hideProgress();
    if (response != null) {
        if (!response.success) {
            showERRMessage(response.messageTxt);
            return false;
        }

    }
    UI_tabItinerary.itineraryDetailsOnLoadCall(response)

    UI_makeReservation.tabDataRetrive.tab1 = true;
    UI_makeReservation.tabDataRetrive.tab2 = true;
    UI_makeReservation.tabDataRetrive.tab3 = true;

    UI_makeReservation.tabStatus.tab1 = false;
    UI_makeReservation.tabStatus.tab2 = false;
    UI_makeReservation.tabStatus.tab3 = false;

    UI_makeReservation.tabStatus.tab4 = true;
    UI_makeReservation.openTab(4);
}

UI_tabAnci.continueToPaymentSuccess = function (response) {

    // prepare the next tab
    UI_tabPayment.isReservation = true;
    UI_tabPayment.ready();
    UI_commonSystem.hideProgress();

    UI_tabPayment.updatePayemntTabTO(response);

    UI_makeReservation.tabDataRetrive.tab3 = false;
    UI_makeReservation.tabStatus.tab3 = true;

    UI_makeReservation.tabStatus.tab0 = true;
    UI_makeReservation.tabStatus.tab1 = true;
    UI_makeReservation.tabStatus.tab2 = UI_tabAnci.tabStatus;

    UI_makeReservation.openTab(3);

    UI_tabPayment.paymentDetailsOnLoadCall();

    // prepare the next tab
    // UI_tabPayment.ready();
    // UI_tabPayment.isReservation = true;
}

/*
 * Create Adult Passenger Json Strings
 */
UI_tabAnci.createAdultPaxInfo = function () {
    var strJS = "[";

    var objAdt = jsonPaxAdults;
    var i = 0;
    $.each(objAdt, function () {
        if (i != 0) {
            strJS += ","
        }
        strJS += "{"
        strJS += "\"type\":\"" + objAdt[i].type + "\",";
        strJS += "\"title\":\"" + objAdt[i].title + "\",";
        strJS += "\"fn\":\"" + objAdt[i].fn + "\",";
        strJS += "\"ln\":\"" + objAdt[i].ln + "\",";
        strJS += "\"nat\":\"" + objAdt[i].nat + "\",";
        strJS += "\"dob\":\"" + objAdt[i].dob + "\",";
        strJS += "\"chrg\":\"" + objAdt[i].chrg + "\",";
        strJS += "\"ssr\":\"" + objAdt[i].ssr + "\",";
        strJS += "\"ssrDesc\":\"" + objAdt[i].ssrDesc + "\",";
        strJS += "\"parent\":\"" + objAdt[i].parent + "\",";
        strJS += "\"infantWith\":\"" + objAdt[i].infantWith + "\",";
        strJS += "\"chrgWithInf\":\"" + objAdt[i].chrgWithInf + "\"";
        strJS += "}"
        i++;
    });
    strJS += "]";
    return strJS;
}

UI_tabAnci.getAutoCheckinAdultDetails = function() {
	var paxAdults = jsonPaxAdults;
	var paxObj = [];

	for ( var jl = 0; jl < paxAdults.length; jl++) {
		var pax = {};
		pax = {
			emailAddress: $("#" + (jl + 1) + "_emailAddress").val(),
			displaySeatPref: $("#" + (jl + 1) + "_displaySeatPref").val(),
			displaySeatCode:$("#autoSeat_" + (jl + 1)).val()
		};
		paxObj.push(pax);
	}
	return paxObj;
}

UI_tabAnci.overrideAdultPaxConfig = function(paxConfigAd){

	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();
	var flightSegment = UI_tabAnci.flightSegmentInfo;
	
	for (var i=0;i<flightSegment.length;i++){
		var segcode = flightSegment[i].flightSegmentTO.segmentCode;
		var seg = segcode.split('/')[0];
		var paxConfigAdult = paxConfig[seg].AD;
		paxConfigAd.passportNo.visibility = paxConfigAd.passportNo.visibility || paxConfigAdult.passportNo.visibility;
		paxConfigAd.passportExpiry.visibility = paxConfigAd.passportExpiry.visibility || paxConfigAdult.passportExpiry.visibility;
		paxConfigAd.passportIssuedCntry.visibility = paxConfigAd.passportIssuedCntry.visibility || paxConfigAdult.passportIssuedCntry.visibility;
		paxConfigAd.placeOfBirth.visibility = paxConfigAd.placeOfBirth.visibility || paxConfigAdult.placeOfBirth.visibility;
		paxConfigAd.dob.visibility = paxConfigAd.dob.visibility || paxConfigAdult.dob.visibility;
		paxConfigAd.travelDocumentType.visibility = paxConfigAd.travelDocumentType.visibility || paxConfigAdult.travelDocumentType.visibility;
		paxConfigAd.visaDocNumber.visibility = paxConfigAd.visaDocNumber.visibility || paxConfigAdult.visaDocNumber.visibility;
		paxConfigAd.visaDocIssueDate.visibility = paxConfigAd.visaDocIssueDate.visibility || paxConfigAdult.visaDocIssueDate.visibility;
		paxConfigAd.visaDocPlaceOfIssue.visibility = paxConfigAd.visaDocPlaceOfIssue.visibility || paxConfigAdult.visaDocPlaceOfIssue.visibility;
		paxConfigAd.visaApplicableCountry.visibility = paxConfigAd.visaApplicableCountry.visibility || paxConfigAdult.visaApplicableCountry.visibility;
	}
	return paxConfigAd;
}

UI_tabAnci.overrideInfantPaxConfig = function(paxConfigAd){

	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();
	var flightSegment = UI_tabAnci.flightSegmentInfo;
	
	for (var i=0;i<flightSegment.length;i++){
		var segcode = flightSegment[i].flightSegmentTO.segmentCode;
		var seg = segcode.split('/')[0];
		var paxConfigInfant = paxConfig[seg].IN;
		paxConfigAd.passportNo.visibility = paxConfigAd.passportNo.visibility || paxConfigInfant.passportNo.visibility;
		paxConfigAd.passportExpiry.visibility = paxConfigAd.passportExpiry.visibility || paxConfigInfant.passportExpiry.visibility;
		paxConfigAd.passportIssuedCntry.visibility = paxConfigAd.passportIssuedCntry.visibility || paxConfigInfant.passportIssuedCntry.visibility;
		paxConfigAd.placeOfBirth.visibility = paxConfigAd.placeOfBirth.visibility || paxConfigInfant.placeOfBirth.visibility;
		paxConfigAd.dob.visibility = paxConfigAd.dob.visibility || paxConfigInfant.dob.visibility;
		paxConfigAd.travelDocumentType.visibility = paxConfigAd.travelDocumentType.visibility || paxConfigInfant.travelDocumentType.visibility;
		paxConfigAd.visaDocNumber.visibility = paxConfigAd.visaDocNumber.visibility || paxConfigInfant.visaDocNumber.visibility;
		paxConfigAd.visaDocIssueDate.visibility = paxConfigAd.visaDocIssueDate.visibility || paxConfigInfant.visaDocIssueDate.visibility;
		paxConfigAd.visaDocPlaceOfIssue.visibility = paxConfigAd.visaDocPlaceOfIssue.visibility || paxConfigInfant.visaDocPlaceOfIssue.visibility;
		paxConfigAd.visaApplicableCountry.visibility = paxConfigAd.visaApplicableCountry.visibility || paxConfigInfant.visaApplicableCountry.visibility;
	}
	return paxConfigAd;
}

UI_tabAnci.getPaxContactConfigDetail = function() {
	var paxConfigResponse = UI_tabAnci.paxConfigResponse;
	return JSON.parse(paxConfigResponse);
}

UI_tabAnci.getPaxresPaxInfoDetail = function() {
	var paxresPaxInfo = UI_tabAnci.paxresPaxInfo;
	return JSON.parse(paxresPaxInfo);
}

UI_tabAnci.routeModifyCheck = function(fltSegCode) {
	var paxresPaxInfo = UI_tabAnci.getPaxresPaxInfoDetail();
	var pastBookings = paxresPaxInfo[0].eTickets;
	for(var i=0; i<pastBookings.length; i++){
		if(pastBookings[i].segmentStatus == "CNF" && pastBookings[i].segmentCode == fltSegCode ){
			return true;
		}
	}
	return false;
}
/*
 * Create Adult Passenger Json Strings
 */
UI_tabAnci.createInfantPaxInfo = function () {
    var strJS = "";

    if (jsonPaxInfants != null) {
        strJS = "[";
        var objAdt = jsonPaxInfants;
        var i = 0;
        $.each(objAdt, function () {
            if (i != 0) {
                strJS += ","
            }
            strJS += "{"
            strJS += "\"type\":\"" + objAdt[i].type + "\",";
            strJS += "\"fn\":\"" + objAdt[i].fn + "\",";
            strJS += "\"ln\":\"" + objAdt[i].ln + "\",";
            strJS += "\"nat\":\"" + objAdt[i].nat + "\",";
            strJS += "\"dob\":\"" + objAdt[i].dob + "\",";
            strJS += "\"trvlWith\":\"" + objAdt[i].trvlWith + "\",";
            strJS += "\"ssr\":\"" + objAdt[i].ssr + "\",";
            strJS += "\"ssrDesc\":\"" + objAdt[i].ssrDesc + "\"";
            strJS += "}"
            i++;
        });
        strJS += "]";
    }
    return strJS;
}

// ------------------------------------------------------------ SSR
// ----------------------------------------------------------
/*
 * Grid Constructor
 */

UI_tabAnci.constructSSRGrid = function (inParams) {
    isPaxHasService = function (ssrObj) {
        var ssrData = jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].ssr;
        var paxSSRs = ssrData.ssrs;
        for (var i = 0; i < paxSSRs.length; i++) {
            if (paxSSRs[i].ssrCode == ssrObj.ssrCode) {
                return true;
            }

            return false;
        }
    }

    // construct grid
    $(inParams.id).jqGrid({
        datatype: "local",
        height: 200,
        width: 770,
        colNames: [geti18nData('ancillary_Code','Code'), geti18nData('testLbl','Service'),geti18nData('ancillary_Charge', 'Charge'), geti18nData('ancillary_Available', 'Available'),
                   geti18nData('ancillary_AdditionalInformation','Additional Information')
        ],
        colModel: [{
            name: 'ssrCode',
            index: 'ssrCode',
            width: 50,
            align: "left"
        }, {
            name: 'ssrName',
            index: 'ssrName',
            width: 200,
            align: "left"
        }, {
            name: 'charge',
            index: 'charge',
            width: 50,
            align: "center"
        }, {
            name: 'avSSRDsp',
            index: 'avSSRDsp',
            width: 75,
            align: "center",
            hidden: true
        }, {
            name: 'text',
            index: 'text',
            width: 125,
            align: "center",
            editable: true,
            editoptions: {
                className: "aa-input",
                maxlength: "200",
                style: "width:150px;",
                dataEvents: [{
                    type: 'keyup',
                    fn: function (e) {
                        var target = e.target;
                        var value = $.trim(target.value);
                        if (!/^[a-zA-Z0-9 ]*$/.test(value)) {
                            value = value.replace(
                                /[^a-zA-Z0-9 ]/g, "");
                            target.value = value;
                        }
                        if (value.length > 0) {
                            var id = target.id.split('_')[0];
                            UI_tabAnci.changeSSRInput(parseInt(
                                id, 10) - 1, 'SELECT');
                        }
                    }
                }]
            }
        }],
        imgpath: UI_commonSystem.strImagePath,
        multiselect: true,
        scrollrows: true,
        viewrecords: true,
        onSelectRow: function (id, status) {
            id = parseInt(id, 10);
            if (status)
                UI_tabAnci.changeSSRInput(parseInt(id, 10) - 1,
                    'SELECT');
            else
                UI_tabAnci.changeSSRInput(parseInt(id, 10) - 1,
                    'DESELECT');

            if (UI_tabAnci.isAnciModify) {
                var chkId = "#jqg_" + id;
                if (jsSSRList[id - 1].serviceStatus == "N") {
                    if (isPaxHasService(jsSSRList[id - 1])) {
                        $("#tblAnciSSR").find(chkId).attr('checked',
                            true);
                        if (!status) {
                            $('#tblAnciSSR').setSelection(id + '',
                                false);
                        }
                    } else {
                        $("#tblAnciSSR").find(chkId).attr('checked',
                            false);
                        $('#tblAnciSSR').setSelection(id + '', false);
                    }
                }
            }
        },
        onSelectAll: function (ids, status) {
            if (status) {
                for (var i = 0; i < jsSSRList.length; i++) {
                    if (UI_tabAnci.isAnciModify) {
                        var chkId = "#jqg_" + (i + 1);
                        if (jsSSRList[i].serviceStatus == "N") {
                            if (isPaxHasService(jsSSRList[i])) {
                                $("#tblAnciSSR").find(chkId).attr(
                                    'checked', true);
                            } else {
                                $("#tblAnciSSR").find(chkId).attr(
                                    'checked', false);
                                $('#tblAnciSSR').setSelection(
                                    (i + 1) + '', false);
                            }
                        }
                    }
                }
            }
        }
    });

}

UI_tabAnci.constructACIGrid = function (inParams) {

	var segments = jsAnciSeg[UI_tabAnci.currSegRow].orignNDest;
	var segment = segments.split("/")[0];
	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();

	var paxVisibility = jsAutoCheckinList;
	UI_tabAnci.overrideAdultPaxConfig(paxVisibility);
	var paxConfigAdult = paxConfig[segment].AD;
	var paxConfigChild = paxConfig[segment].CH;

	//var paxAdults = UI_tabAnci.getPaxContactConfigDetails();

	var fields = ['passportNo', 'passportExpiry','passportIssuedCntry','placeOfBirth','dob','email','visaDocPlaceOfIssue','visaDocIssueDate','travelDocumentType','visaDocNumber','visaApplicableCountry'];
	var fieldConfig = {};
	for (var i = 0; i < fields.length; i++) {
		var obj = $.extend({
			visible : true,
			mandatory : ""
		}, {
			visible : isVisible(paxVisibility, fields[i]),
			mandatory : isMandatory(paxConfigAdult, fields[i])
		});
		fieldConfig[fields[i]] = obj;
	}
	
	
	function isMandatory(configFirst, field){
		if(configFirst[field] == undefined)
			return UI_commonSystem.strTxtMandatory;
		if (configFirst[field].mandatory) 
			return UI_commonSystem.strTxtMandatory;
		return "";
	}
	
	function isVisible(configFirst, field){
		if(configFirst[field] == undefined)
			return true;
		return (configFirst[field].visibility) ;
	}


	// construct grid
	$(inParams.id).GridUnload();
	
	var columnModel = [ { 
		name: 'sequenceNo',
		width: 20,
    	index: 'sequenceNo',
    	align: "center"						            		
    },{
		name : 'displayAdultType',
		index : 'displayAdultType',
		width : 50,
		align : "center"
	},{
		name : 'passportNumber',
		index : 'passportNumber',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['passportNo'].visible
	}, {
		name : 'expiryDate',
		index : 'expiryDate',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "10",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabAnci.dateFormatter(this);
				}
			}]
		},
		hidden:!fieldConfig['passportExpiry'].visible
	}, {
		name : 'countryOfInsurance',
		index : 'countryOfInsurance',
		align : "center",
		editable : true,
		edittype : "select",
		editoptions : {
			className : "aa-input",
			value : UI_tabAnci.fillDropFromArray()
		},
		hidden:!fieldConfig['passportIssuedCntry'].visible
	}, {
		name : 'placeOfBirth',
		index : 'placeOfBirth',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['placeOfBirth'].visible
	}, {
		name : 'passengerDOB',
		index : 'passengerDOB',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "10",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabAnci.dateFormatter(this);
				}
			}]
		},
		hidden:!fieldConfig['dob'].visible
	},{
		name : 'emailAddress',
		index : 'emailAddress',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['email'].visible
	},{
		name : 'visaDocIssueDate',
		index : 'visaDocIssueDate',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "10",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabAnci.dateFormatter(this);
				}
			}]
		},
		hidden:!fieldConfig['visaDocIssueDate'].visible
	},{
		name : 'visaDocPlaceOfIssue',
		index : 'visaDocPlaceOfIssue',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['visaDocPlaceOfIssue'].visible
	},{
		name : 'travelDocumentType',
		index : 'travelDocumentType',
		align : "center",
		edittype : "select",
		editable : true,
		editoptions : {
			className : "aa-input",
			value : UI_tabAnci.fillDocTypeDropDown() 
		},
		hidden:!fieldConfig['travelDocumentType'].visible
	},{
		name : 'visaDocNumber',
		index : 'visaDocNumber',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['visaDocNumber'].visible
	},{
		name : 'visaApplicableCountry',
		index : 'visaApplicableCountry',
		align : "center",
		edittype : "select",
		editable : true,
		editoptions : {
			className : "aa-input",
			value : UI_tabAnci.fillDropFromArray()
		},
		hidden:!fieldConfig['visaApplicableCountry'].visible
	}];
	
	var columnNames = [ '',geti18nData('passenger_Type','Type'),
	                    geti18nData('passport_No','Passport No')+ fieldConfig['passportNo'].mandatory, 
	                    geti18nData('Expiry_Date','Expiry Date')+ fieldConfig['passportExpiry'].mandatory, 
	                    geti18nData('Country_Issurance','Country of Issurance')+ fieldConfig['passportIssuedCntry'].mandatory, 
	                    geti18nData('placeOfBirth','Place Of Birth')+ fieldConfig['placeOfBirth'].mandatory,
	                    geti18nData('passenger_DateOfBirth','Date of Birth')+ fieldConfig['dob'].mandatory, 
	                    geti18nData('Email_Address','Email Address')+ fieldConfig['email'].mandatory,
	                    
						geti18nData('visaDocIssueDate','visa Doc IssueDate')+ fieldConfig['visaDocIssueDate'].mandatory,
	                    geti18nData('visaDocPlaceOfIssue','Visa Doc PlaceOfIssue')+ fieldConfig['visaDocPlaceOfIssue'].mandatory,
	                    geti18nData('travelDocumentType','Travel DocumentType')+ fieldConfig['travelDocumentType'].mandatory,
						geti18nData('visaDocNumber','Visa Doc Number')+ fieldConfig['visaDocNumber'].mandatory,
	                    geti18nData('visaApplicableCountry','Visa ApplicableCountry')+ fieldConfig['visaApplicableCountry'].mandatory
	                  ];
	
	$(inParams.id).jqGrid(
		{
			height : 50,
			width : 770,
			colNames : columnNames,
			colModel : columnModel,

			imgpath : UI_commonSystem.strImagePath,
			multiselect : false,
			datatype : "local",
			viewrecords : true
		});

}

UI_tabAnci.dateFormatter = function(objC) {
	var strDT = dateChk(objC.id);
	strDT = strDT != true ? strDT : objC.value;
	objC.value = strDT;
	UI_commonSystem.pageOnChange();
}

UI_tabAnci.constructACIInfantGrid = function (inParams) {
	var segments = jsAnciSeg[UI_tabAnci.currSegRow].orignNDest;
	var segment = segments.split("/")[0];
	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();

	var paxVisibility = jsInfantAutoCheckinList.global;
	UI_tabAnci.overrideInfantPaxConfig(paxVisibility);
	var paxConfigInfant = paxConfig[segment].IN;


	var fields = ['passportNo','passportExpiry','passportIssuedCntry','placeOfBirth','dob','email','visaDocPlaceOfIssue','visaDocIssueDate','travelDocumentType','visaDocNumber','visaApplicableCountry'];
	var fieldConfig = {};
	for (var i = 0; i < fields.length; i++) {
		var obj = $.extend({
			visible : true,
			mandatory : ""
		}, {
			visible : isVisible(paxVisibility, fields[i]),
			mandatory : isMandatory(paxConfigInfant, fields[i])
		});
		fieldConfig[fields[i]] = obj;
	}
	
	
	function isMandatory(configFirst, field){
		if(configFirst[field] == undefined)
			return UI_commonSystem.strTxtMandatory;
		if (configFirst[field].mandatory) 
			return UI_commonSystem.strTxtMandatory;
		return "";
	}
	
	function isVisible(configFirst, field){
		if(configFirst[field] == undefined)
			return true;
		return (configFirst[field].visibility) ;
	}

	// construct grid
	$(inParams.id).GridUnload();
	
	var columnModel = [ { 
		name: 'sequenceNo',
		width: 20,
    	index: 'sequenceNo',
    	align: "center"						            		
    },{
    	name : 'displayType',
		index : 'displayType',
		width : 50,
		align : "center"
	}, {
		name : 'infantPassportNumber',
		index : 'infantPassportNumber',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['passportNo'].visible
	}, {
		name : 'infantExpiryDate',
		index : 'infantExpiryDate',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "10",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabAnci.dateFormatter(this);
				}
			}]
		},
		hidden:!fieldConfig['passportExpiry'].visible
	}, {
		name : 'infantCountryOfInsurance',
		index : 'infantCountryOfInsurance',
		align : "center",
		editable : true,
		edittype : "select",
		editoptions : {
			className : "aa-input",
			value : UI_tabAnci.fillDropFromArray()
		},
		hidden:!fieldConfig['passportIssuedCntry'].visible
	},{
		name : 'infantPlaceOfBirth',
		index : 'infantPlaceOfBirth',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['placeOfBirth'].visible
	}, {
		name : 'infantPassengerDOB',
		index : 'infantPassengerDOB',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "10",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabAnci.dateFormatter(this);
				}
			}]
		},
		hidden:!fieldConfig['dob'].visible
	},{
		name : 'infantEmailAddress',
		index : 'infantEmailAddress',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['email'].visible
	},{
		name : 'infantVisaDocIssueDate',
		index : 'infantVisaDocIssueDate',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "10",
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_tabAnci.dateFormatter(this);
				}
			}]
		},
		hidden:!fieldConfig['visaDocIssueDate'].visible
	},{
		name : 'infantVisaDocPlaceOfIssue',
		index : 'infantVisaDocPlaceOfIssue',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['visaDocPlaceOfIssue'].visible
	},{
		name : 'infantTravelDocumentType',
		index : 'infantTravelDocumentType',
		align : "center",
		edittype : "select",
		editable : true,
		editoptions : {
			className : "aa-input",
			value : UI_tabAnci.fillDocTypeDropDown() 
		},
		hidden:!fieldConfig['travelDocumentType'].visible
	},{
		name : 'infantVisaDocNumber',
		index : 'infantVisaDocNumber',
		align : "center",
		editable : true,
		editoptions : {
			className : "aa-input",
			maxlength : "30"
		},
		hidden:!fieldConfig['visaDocNumber'].visible
	},{
		name : 'infantVisaApplicableCountry',
		index : 'infantVisaApplicableCountry',
		align : "center",
		edittype : "select",
		editable : true,
		editoptions : {
			className : "aa-input",
			value : UI_tabAnci.fillDropFromArray()
		},
		hidden:!fieldConfig['visaApplicableCountry'].visible
	}];
	
	var columnNames = [ '',geti18nData('passenger_Type','Type'),
	                    geti18nData('passport_No','Passport No')+ fieldConfig['passportNo'].mandatory, 
	                    geti18nData('Expiry_Date','Expiry Date')+ fieldConfig['passportExpiry'].mandatory, 
	                    geti18nData('Country_Issurance','Country of Issurance')+ fieldConfig['passportIssuedCntry'].mandatory,
	                    geti18nData('placeOfBirth','Place Of Birth')+ fieldConfig['placeOfBirth'].mandatory,
	                    geti18nData('passenger_DateOfBirth','Date of Birth')+ fieldConfig['dob'].mandatory, 
	                    geti18nData('Email_Address','Email Address')+ fieldConfig['email'].mandatory,
	                    
						geti18nData('visaDocIssueDate','visa Doc IssueDate')+ fieldConfig['visaDocIssueDate'].mandatory,
	                    geti18nData('visaDocPlaceOfIssue','Visa Doc PlaceOfIssue')+ fieldConfig['visaDocPlaceOfIssue'].mandatory,
	                    geti18nData('travelDocumentType','Travel DocumentType')+ fieldConfig['travelDocumentType'].mandatory,
						geti18nData('visaDocNumber','Visa Doc Number')+ fieldConfig['visaDocNumber'].mandatory,
	                    geti18nData('visaApplicableCountry','Visa ApplicableCountry')+ fieldConfig['visaApplicableCountry'].mandatory
	                  ];
	
	$(inParams.id).jqGrid(
		{
			height : 50,
			width : 770,
			colNames : columnNames,
			colModel : columnModel,

			imgpath : UI_commonSystem.strImagePath,
			multiselect : false,
			datatype : "local",
			viewrecords : true
		});
}

/*
 * Change SSR Input
 */
UI_tabAnci.changeSSRInput = function (rowId, option) {
    if (option == 'SELECT') {
        // $('#btnSSRAdd_'+rowId).val("X");
        jsSSRList[rowId].status = "Y";
        if (!UI_tabAnci._isSSRRowSelected(rowId + 1)) {
            $('#tblAnciSSR').setSelection((rowId + 1) + '', false);
        }
    } else if (option == 'DESELECT') {
        // $('#btnSSRAdd_'+rowId).val("Add");
        jsSSRList[rowId].status = "N";
        // $("#"+ (rowId + 1) + "_text").val('');
        if (UI_tabAnci._isSSRRowSelected(rowId + 1)) {
            $('#tblAnciSSR').setSelection((rowId + 1) + '', false);
        }
    }
}
UI_tabAnci._isSSRRowSelected = function (id) {
    var selIds = $("#tblAnciSSR").getGridParam('selarrrow');
    for (var i = 0; i < selIds.length; i++) {
        if (selIds[i] == id) {
            return true;
        }
    }
    return false;
}

/*
 * Apply SSR Request
 */
UI_tabAnci.btnApplySSR = function (p, obj) {
    var strStatus = "";
    var intRow = p.row;
    if (obj.value == "Add") {
        obj.value = "X";
        strStatus = "Y";
    } else {
        obj.value = "Add";
        strStatus = "N";
        $("#" + (intRow + 1) + "_text").val('');
    }
    jsSSRList[intRow].status = strStatus;
}
UI_tabAnci.compareFlightRefNo = function (first, second) {
    var fArr = first.split('$');
    var sArr = second.split('$');
    if (fArr.length > 1) {
        first = fArr[2];
    }
    if (sArr.length > 1) {
        second = sArr[2];
    }
    return (first == second);
}
/*
 * Build SSR List
 */
UI_tabAnci.buildSSR = function (p) {

    var intSegRow = null;
    if (p.fltRefNo != null) {
        for (var i = 0; i < jsSSRModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsSSRModel[i].flightSegmentTO.flightRefNumber, p.fltRefNo)) {
                intSegRow = i;
                break;
            }
        }
    }

    if (intSegRow != null) {
        jsSSRList.length = 0;
        for (var i = 0; i < jsSSRModel[intSegRow].specialServiceRequest.length; i++) {
            var ts = jsSSRModel[intSegRow].specialServiceRequest[i];
            jsSSRList[i] = {
                ssrCode: ts.ssrCode,
                ssrName: ts.ssrName,
                serviceQuantity: ts.serviceQuantity,
                availableQty: ts.availableQty,
                charge: $.airutil.format.currency(ts.charge),
                description: ts.description,
                status: '',
                serviceStatus: ts.status,
                text: '',
                avSSRDsp: '<span id="avSSRDsp_' + i + '" >' + ts.availableQty + '</span>',
                smI: i,
                smSGI: intSegRow,
                apply: UI_tabAnci.ssrBtnStr(i)
            };
        }
        // jsSSRList =
        // $.airutil.sort.quickSort(jsSSRList,UI_tabAnci.SSRCodeComparator);
        // jsSSRList = jsSSRList.reverse();
        
        $('#tblAnciSSR').clearGridData();
        
        UI_commonSystem.fillGridData({
            id: "#tblAnciSSR",
            data: jsSSRList
        });

        if (UI_tabAnci.isAnciModify) {
            _disableSSRCheckbox();
        }

        $('#tblAnciSSR').resetSelection();
        if (jsSSRList.length > 0)
            $("#1_text").focus();
    }
}

_disableSSRCheckbox = function () {
    if (jsSSRList != null && jsSSRList.length > 0) {
        for (var i = 0; i < jsSSRList.length; i++) {
            var chkId = "#jqg_" + (i + 1);
            if (jsSSRList[i].serviceStatus == 'N') {
                $("#tblAnciSSR").find(chkId).attr('disabled', 'true');
            }
        }
    }
}

/*
 * Update SSR available count
 */
UI_tabAnci.updateSSRAvailCount = function (ssr, qty, op) {
    var index = ssr.ssrIndex;
    var ssr = jsSSRList[index];
    if (ssr == undefined)
        return;
    if (ssr.availableQty == undefined || ssr.availableQty <= 0)
        return;
    if (op == 'ADD') {
        ssr.availableQty += qty;
    } else if (op == 'DEDUCT') {
        ssr.availableQty -= qty;
    }

    $("#avSSRDsp_" + index).html(ssr.availableQty);
}

/*
 * Build SSR List
 */
UI_tabAnci.refreshSSR = function () {

    var ssrPax = jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].ssr;
    var paxSSRs = ssrPax.ssrs;
    var ssr = null;
    // {ssrCode:ssr.ssrCode,text:ssr.text,charge:ssr.charge,text:text};
    for (var i = 0; i < jsSSRList.length; i++) {
        UI_tabAnci.changeSSRInput(i, 'DESELECT');
        $("#" + (i + 1) + "_text").val('');
    }
    $('#tblAnciSSR').resetSelection();
    /* set the status to only pax selected values */
    for (var i = 0; i < paxSSRs.length; i++) {
        UI_tabAnci.changeSSRInput(paxSSRs[i].ssrIndex, 'SELECT');
        $("#" + (parseInt(paxSSRs[i].ssrIndex) + 1) + "_text").val(
            paxSSRs[i].text);
    }
}

/*
 * SSR apply btn
 */
UI_tabAnci.ssrBtnStr = function (rowId) {
    return "<input type='button' id='btnSSRAdd_" + rowId + "' name='btnAdd' value='Add' class='ui-state-default ui-corner-all' onclick='UI_tabAnci.btnApplySSR({row:" + rowId + "}, this)'>";
}

/*
 * SSR Pax
 */
UI_tabAnci.buildSSRPax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciSSRPax").find("tr").remove();
    UI_tabAnci.currPaxIDRow = null;
    var objPax = jsonPaxAdults; // .concat(jsonPaxInfants);
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    if (intLen > 0) {
        do {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxSSR_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
                click: UI_tabAnci.tdSSRPaxOnClick,
                align: "left"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxSSR_1_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: "",
                id: "tdPaxSSR_2_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));

            tblRow
                .appendChild(UI_commonSystem
                    .createCell({
                        desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxSSRRemove(" + i + ",true); return false;'><img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove SSR'>",
                        css: UI_commonSystem.strDefClass + strClass,
                        align: "center"
                    }));
            $("#tbdyAnciSSRPax").append(tblRow);
            i++;
        } while (--intLen);
    }
    var intLen = objPax.length;
}
UI_tabAnci.fillDropFromArray = function() {
	var intLen = top.arrCountry.length;
	// Eg : jsonCountry = ":;LK:Sri Lanka;AE:United Arab Emirates";
	var jsonCountry = ":";
	if (intLen > 0) {
		i = 0;
		do {
			jsonCountry += ";" + top.arrCountry[i][0] + ":"
					+ top.arrCountry[i][1];
			i++;
		} while (--intLen);
	}
	
	return jsonCountry;
}
UI_tabAnci.fillDocTypeDropDown = function() {
	// Eg : jsonCountry = ":;LK:Sri Lanka;AE:United Arab Emirates";
	return jsonDocType = ":;V:Visa";
}
UI_tabAnci.buildACI = function (p) {
    $('#tblAnciACI').clearGridData();
    var tempjsonPaxAdults = $.airutil.dom.cloneObject(jsonPaxAdults);
	var paxAdultLen = $.fn.objLen(tempjsonPaxAdults);
	var seqNo = 1;
	if(paxAdultLen > 0){
		var index = 0;
		do{
			tempjsonPaxAdults[index].sequenceNo = seqNo++;
			index++;
		} while(--paxAdultLen)
	}
    UI_commonSystem.fillGridData({
        id: "#tblAnciACI",
        data: tempjsonPaxAdults
    });
    
    UI_tabAnci.tableWidthAlign('#tblAnciACI');
}

UI_tabAnci.buildACIInfant = function (p) {
	
    $('#tblAnciACIInfant').clearGridData();
    var tempjsonPaxInfants = $.airutil.dom.cloneObject(jsonPaxInfants);
	var paxAdultLen = $.fn.objLen(tempjsonPaxInfants);
	var seqNo = 1;
	if(paxAdultLen > 0){
		var index = 0;
		do{
			tempjsonPaxInfants[index].sequenceNo = seqNo++;
			tempjsonPaxInfants[index].displayType = 'BABY';
			index++;
		} while(--paxAdultLen)
	}
    UI_commonSystem.fillGridData({
        id: "#tblAnciACIInfant",
        data: tempjsonPaxInfants
    });
    $("#gview_tblAnciACIInfant").prepend("<div class='paneHD'><p class='txtWhite txtBold' i18n_key='passenger_Infant'>Infant</p></div>");
    
    UI_tabAnci.tableWidthAlign('#tblAnciACIInfant');
}

UI_tabAnci.tableWidthAlign = function (tableId){
	$("table" + tableId + " tr:first td" ).each(function( i ) {
    	var x = $("table" + tableId + " tr:first td:nth-child("+i+")").clone().attr('style');
    	$("table" + tableId + ' tr td:nth-child('+i+')').attr('style', x);
    });
    var y = $("table" + tableId + " tr:first td:last-child").clone().attr('style');
	$("table" + tableId + ' tr td:last-child').attr('style', y);
}

UI_tabAnci.commonCalendarData = function() {
	var flightSegment = UI_tabAnci.flightSegmentInfo;
	var arrivalDate = UI_tabAnci.dateConverter(flightSegment[flightSegment.length-1].flightSegmentTO.arrivalDateTime);
	var depatureDate = UI_tabAnci.dateConverter(flightSegment[0].flightSegmentTO.departureDateTime);
	var curDate = top.arrParams[0];
	var arrivalDateArr = arrivalDate.split('/');
	var depatureDateArr = depatureDate.split('/');
	var curDateArr = curDate.split('/');
	commonCalendarData = ({
		arrivalDate : arrivalDate,
		depatureDate : depatureDate,
		curDate : curDate,
		arrivalDay : arrivalDateArr[0],
		arrivalMonth : arrivalDateArr[1],
		arrivalYear : arrivalDateArr[2],
		depatureDay : depatureDateArr[0],
		depatureMonth : depatureDateArr[1],
		depatureYear : depatureDateArr[2],
		curDay : curDateArr[0],
		curMonth : curDateArr[1],
		curYear : curDateArr[2]
	})
	return commonCalendarData;
}

UI_tabAnci.mapCalendar = function() {
	/* Adult */
	var commonCalendarData = UI_tabAnci.commonCalendarData();
	var intLen = jsonPaxAdults.length;
	
	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();
	var paxValidation = paxConfig.paxValidation;

	var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;
	
		if (intLen > 0) {
		var i = 1;
		$("#tblAnciACI").find("tr").each(function() {
			$(this).find("input").each(function() {
				if (this.name == "passengerDOB") {													
					var minDate;
					var maxDate;
					var range = '';														
					if(jsonPaxAdults[i - 1].displayAdultType == 'CH'){
						minDate = new Date((commonCalendarData.arrivalYear-paxValidation.childAgeCutOverYears), commonCalendarData.depatureMonth-1, commonCalendarData.depatureDay);
						maxDate = new Date((commonCalendarData.arrivalYear-paxValidation.infantAgeCutOverYears), commonCalendarData.depatureMonth-1, commonCalendarData.depatureDay-1);
						range = '-' + paxValidation.childAgeCutOverYears + ':+' + paxValidation.childAgeCutOverYears ;
					}else{
						minDate = new Date((commonCalendarData.arrivalYear-paxValidation.adultAgeCutOverYears), commonCalendarData.depatureMonth-1, commonCalendarData.depatureDay);
						maxDate = new Date((commonCalendarData.arrivalYear-paxValidation.childAgeCutOverYears), commonCalendarData.depatureMonth-1, commonCalendarData.depatureDay-1);
						range = '-' + paxValidation.adultAgeCutOverYears + ':+' + paxValidation.adultAgeCutOverYears;	
					}
					$("#" + this.id).datepicker({
						minDate : minDate,
						maxDate : maxDate,																		
						dateFormat : UI_commonSystem.strDTFormat,
						changeMonth : 'true',
						changeYear : 'true',
						yearRange: range,
						showButtonPanel : 'true'
					});
				}
				else if (this.name == "expiryDate") {
					var minDate;
					var maxDate;
					var range = '';
					minDate = new Date(parseInt(commonCalendarData.curYear),parseInt(commonCalendarData.arrivalMonth),parseInt(commonCalendarData.arrivalDay) + 1);
					maxDate = new Date((parseInt(commonCalendarData.curYear)+paxValidation.childAgeCutOverYears),parseInt(commonCalendarData.arrivalMonth),parseInt(commonCalendarData.arrivalDay));
					range = '-' + paxValidation.childAgeCutOverYears + ':+' + paxValidation.childAgeCutOverYears;
					$("#" + this.id).datepicker({
						minDate: minDate,
						maxDate: maxDate,
						dateFormat: UI_commonSystem.strDTFormat,
						changeMonth: 'true',
						changeYear: 'true',
						yearRange: range,
						showButtonPanel: 'true'
					});
				}
				else if (this.name == "visaDocIssueDate") {
					var minDate;
					var maxDate;
					var range = '';
					minDate = new Date(commonCalendarData.curYear- paxValidation.childAgeCutOverYears, commonCalendarData.curMonth-1 , commonCalendarData.curDay - 1);
					maxDate = new Date(commonCalendarData.curYear, commonCalendarData.curMonth-1 , commonCalendarData.curDay - 1);
					range = '-' + paxValidation.childAgeCutOverYears + ':+' + paxValidation.childAgeCutOverYears;
					$("#" + this.id).datepicker({
						minDate: minDate,
						maxDate: maxDate,
						dateFormat: UI_commonSystem.strDTFormat,
						changeMonth: 'true',
						changeYear: 'true',
						yearRange: range,
						showButtonPanel: 'true'
					});
				}
			});
			if(!UI_tabAnci.isAnciModify && !UI_tabAnci.isRequote){
				$("#"+ i + "_passportNumber").val(jsonPaxAdtObj.paxAdults[i - 1].displayPnrPaxCatFOIDNumber);
				$("#"+ i + "_expiryDate").val(jsonPaxAdtObj.paxAdults[i - 1].displayPnrPaxCatFOIDExpiry);
				$("#"+ i + "_countryOfInsurance").val(jsonPaxAdtObj.paxAdults[i - 1].displayPnrPaxCatFOIDPlace);
				$("#"+ i + "_placeOfBirth").val(jsonPaxAdtObj.paxAdults[i - 1].displayPnrPaxPlaceOfBirth);
				$("#"+ i + "_passengerDOB").val(jsonPaxAdtObj.paxAdults[i - 1].displayAdultDOB);

				$("#"+ i + "_visaDocIssueDate").val(jsonPaxAdtObj.paxAdults[i - 1].displayVisaDocIssueDate);
				$("#"+ i + "_travelDocumentType").val(jsonPaxAdtObj.paxAdults[i - 1].displayTravelDocType);
				$("#"+ i + "_visaDocNumber").val(jsonPaxAdtObj.paxAdults[i - 1].displayVisaDocNumber);
				$("#"+ i + "_visaApplicableCountry").val(jsonPaxAdtObj.paxAdults[i - 1].displayVisaApplicableCountry);
				$("#"+ i + "_visaDocPlaceOfIssue").val(jsonPaxAdtObj.paxAdults[i - 1].displayVisaDocPlaceOfIssue);
				
				var flightSegment = UI_tabAnci.flightSegmentInfo;
				for (var j=0;j<flightSegment.length;j++){
					if((jsAnciSeg[j].pax[i - 1].automaticCheckin.email) == ''){
						jsAnciSeg[j].pax[i - 1].automaticCheckin.email = $("#txtEmail").val();
					}
				}
			}
			i++;
		});
	}
	
	/* Infant */
	if (jsonPaxInfants != null) {
	var infLen = jsonPaxInfants.length;
	if (infLen > 0) {
		var i = 1;
		$("#tblAnciACIInfant").find("tr").each(function() {
			$(this).find("input").each(function() {
				if (this.name == "infantPassengerDOB") {													
					var minDate = new Date((commonCalendarData.arrivalYear-paxValidation.infantAgeCutOverYears), commonCalendarData.arrivalMonth-1, commonCalendarData.arrivalDay);
					var maxDate = getMaximumPossibleInfantDOB(commonCalendarData.curDate,commonCalendarData.depatureDate,infantAgeLowerBoundInDays);
					var range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;
					$("#" + this.id).datepicker({
						minDate : minDate,
						maxDate : maxDate,																		
						dateFormat : UI_commonSystem.strDTFormat,
						changeMonth : 'true',
						changeYear : 'true',
						yearRange: range,
						showButtonPanel : 'true'
					});
				}
				else if (this.name == "infantExpiryDate") {
					var minDate;
					var maxDate;
					var range = '';
					minDate = new Date(parseInt(commonCalendarData.curYear),parseInt(commonCalendarData.arrivalMonth),parseInt(commonCalendarData.arrivalDay) + 1);
					maxDate = new Date((parseInt(commonCalendarData.curYear)+paxValidation.infantAgeCutOverYears),parseInt(commonCalendarData.arrivalMonth),parseInt(commonCalendarData.arrivalDay));
					range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;
					$("#" + this.id).datepicker({
						minDate: minDate,
						maxDate: maxDate,
						dateFormat: UI_commonSystem.strDTFormat,
						changeMonth: 'true',
						changeYear: 'true',
						yearRange: range,
						showButtonPanel: 'true'
					});
				}
				else if (this.name == "infantVisaDocIssueDate") {
					var minDate;
					var maxDate;
					var range = '';
					minDate = new Date(commonCalendarData.curYear- paxValidation.infantAgeCutOverYears, commonCalendarData.curMonth-1 , commonCalendarData.curDay - 1);
					maxDate = new Date(commonCalendarData.curYear, commonCalendarData.curMonth-1 , commonCalendarData.curDay - 1);
					range = '-' + paxValidation.infantAgeCutOverYears + ':+' + paxValidation.infantAgeCutOverYears;
					$("#" + this.id).datepicker({
						minDate: minDate,
						maxDate: maxDate,
						dateFormat: UI_commonSystem.strDTFormat,
						changeMonth: 'true',
						changeYear: 'true',
						yearRange: range,
						showButtonPanel: 'true'
					});
				}
			});
			function getMaximumPossibleInfantDOB(curentDate, firstDepDateArr, infantAgeLowerDays){
				var firstDepDate = firstDepDateArr.split('/');
				var depDay = firstDepDate[0];
				var depMon = firstDepDate[1];
				var depYear = firstDepDate[2];
				
				var curentDateArr = curentDate.split('/');
				var currDay = curentDateArr[0];
				var currMon = curentDateArr[1];
				var currYear = curentDateArr[2];
				
				var maxDate = new Date(depYear,depMon-1,depDay-infantAgeLowerDays-1) ;
				var currentDate = new Date (currYear,currMon-1,currDay);
				 
				 if (maxDate < currentDate){			 
					 return maxDate;
				 } else {
				     return currentDate ;
				 }		
			}
			if(!UI_tabAnci.isAnciModify && !UI_tabAnci.isRequote){
				$("#"+ i + "_infantPassportNumber").val(jsonPaxAdtObj.paxInfants[i - 1].displayPnrPaxCatFOIDNumber);
				$("#"+ i + "_infantExpiryDate").val(jsonPaxAdtObj.paxInfants[i - 1].displayPnrPaxCatFOIDExpiry);
				$("#"+ i + "_infantCountryOfInsurance").val(jsonPaxAdtObj.paxInfants[i - 1].displayPnrPaxCatFOIDPlace);
				$("#"+ i + "_infantPlaceOfBirth").val(jsonPaxAdtObj.paxInfants[i - 1].displayPnrPaxPlaceOfBirth);
				$("#"+ i + "_infantPassengerDOB").val(jsonPaxAdtObj.paxInfants[i - 1].displayInfantDOB);

				$("#"+ i + "_infantVisaDocIssueDate").val(jsonPaxAdtObj.paxInfants[i - 1].displayVisaDocIssueDate);
				$("#"+ i + "_infantTravelDocumentType").val(jsonPaxAdtObj.paxInfants[i - 1].displayTravelDocType);
				$("#"+ i + "_infantVisaDocNumber").val(jsonPaxAdtObj.paxInfants[i - 1].displayVisaDocNumber);
				$("#"+ i + "_infantVisaApplicableCountry").val(jsonPaxAdtObj.paxInfants[i - 1].displayVisaApplicableCountry);
				$("#"+ i + "_infantVisaDocPlaceOfIssue").val(jsonPaxAdtObj.paxInfants[i - 1].displayVisaDocPlaceOfIssue);
			}
			i++;
		});
	}
	}
}

UI_tabAnci.modifyReteriveAutoCheckinPaxDetail = function (){
	var objPax;
	objPax = jsonPaxAdults;
	for(var i = 0; i < objPax.length;i++){
		$("#"+ (i+1) + "_passportNumber").val(objPax[i].displayPnrPaxCatFOIDNumber);
		$("#"+ (i+1) + "_expiryDate").val(UI_tabAnci.dateConverter(objPax[i].displayPnrPaxCatFOIDExpiry));
		$("#"+ (i+1) + "_countryOfInsurance").val(objPax[i].displayPnrPaxCatFOIDPlace);
		$("#"+ (i+1) + "_placeOfBirth").val(objPax[i].displayPnrPaxPlaceOfBirth);
		$("#"+ (i+1) + "_passengerDOB").val(UI_tabAnci.dateConverter(objPax[i].displayAdultDOB));
		
		$("#"+ (i+1) + "_visaDocIssueDate").val(UI_tabAnci.dateConverter(objPax[i].displayVisaDocIssueDate));
		$("#"+ (i+1) + "_travelDocumentType").val(objPax[i].displayTravelDocType);
		$("#"+ (i+1) + "_visaDocNumber").val(objPax[i].displayVisaDocNumber);
		$("#"+ (i+1) + "_visaApplicableCountry").val(objPax[i].displayVisaApplicableCountry);
		$("#"+ (i+1) + "_visaDocPlaceOfIssue").val(objPax[i].displayVisaDocPlaceOfIssue);
	}
	objPax = jsonPaxInfants;
	if(objPax != null && objPax.length > 0){
		for(var i = 0; i < objPax.length;i++){
			$("#"+ (i+1) + "_infantPassportNumber").val(objPax[i].displayPnrPaxCatFOIDNumber);
			$("#"+ (i+1) + "_infantExpiryDate").val(UI_tabAnci.dateConverter(objPax[i].displayPnrPaxCatFOIDExpiry));
			$("#"+ (i+1) + "_infantCountryOfInsurance").val(objPax[i].displayPnrPaxCatFOIDPlace);
			$("#"+ (i+1) + "_infantPlaceOfBirth").val(objPax[i].displayPnrPaxPlaceOfBirth);
			$("#"+ (i+1) + "_infantPassengerDOB").val(UI_tabAnci.dateConverter(objPax[i].displayAdultDOB));

			$("#"+ (i+1) + "_infantVisaDocIssueDate").val(UI_tabAnci.dateConverter(objPax[i].displayVisaDocIssueDate));
			$("#"+ (i+1) + "_infantTravelDocumentType").val(objPax[i].displayTravelDocType);
			$("#"+ (i+1) + "_infantVisaDocNumber").val(objPax[i].displayVisaDocNumber);
			$("#"+ (i+1) + "_infantVisaApplicableCountry").val(objPax[i].displayVisaApplicableCountry);
			$("#"+ (i+1) + "_infantVisaDocPlaceOfIssue").val(objPax[i].displayVisaDocPlaceOfIssue);
		}
	}
}
UI_tabAnci.dateConverter = function (p) {
	if(p == null || p == ''){
		return p;
	}else{
		var d = new Date(p);
		var date;
		var month;
		if (isNaN(d)){
			return p;
		}else{
			if(d.getDate() < 10){
		    	date = "0"+ d.getDate();
		    }else{
		    	date = d.getDate();
		    }
		    
			if(d.getMonth() < 9){
		    	month = "0"+ (d.getMonth()+1);
		    }else{
		    	month = d.getMonth()+1;
		    }
			var date = date + "/" + month + "/" + d.getFullYear();
			return date;
		}
	}
}
/*
 * AutoCheckin Pax
 */
UI_tabAnci.buildACIPax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciACIPax").find("tr").remove();

    UI_tabAnci.currPaxIDRow = null;
	var segmentNo = UI_tabAnci.currSegRow;
    var objPax = jsonPaxAdults; // .concat(jsonPaxInfants);
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    var seatCode;
    
    if (intLen > 0) {
        do {
        	if((UI_tabAnci.isAnciModify || UI_tabAnci.isRequote) && 
    	    	(jsonPaxAdults[i].currentAncillaries[segmentNo] != undefined) && 
    	    	(jsonPaxAdults[i].currentAncillaries[segmentNo].airSeatDTO != null) &&
    	    	(jsonPaxAdults[i].currentAncillaries[segmentNo].airSeatDTO.length > 0)){
        		
        		seatCode = jsonPaxAdults[i].currentAncillaries[segmentNo].airSeatDTO.seatNumber;
    	    }
    	    else{
    	    	seatCode = jsAnciSeg[segmentNo].pax[i].seat.seatNumber;
    	    }
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxACI_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
				click: UI_tabAnci.tdAutoCheckinPaxOnClick,
                align: "left"
            }));
			if (seatCode != "") {
				tblRow.appendChild(UI_commonSystem.createCell({
		            desc: ""+seatCode+"<input type='hidden'  id='autoSeat_" + (i+1) + "' value = '"+seatCode+"'>",
					 id: "tdPaxACI_1_" + i,
		            css: UI_commonSystem.strDefClass + strClass,
		            align: "center"
		        }));

			}else{
				tblRow.appendChild(UI_commonSystem.createCell({
		            desc: "<select style='width:100%;' id='" + (i+1) + "_displaySeatPref' name='displaySeatPref' size='1' class='editable aa-input'><option value='W'>Window Seat</option><option value='M'>Middle Seat</option><option value='A'>Aisle Seat</option></select> ",
		            id: "tdPaxACI_1_" + i,
		            css: UI_commonSystem.strDefClass + strClass,
		            align: "center"
		        }));

			}
            
			if ((i == 0) || (seatCode != "")) {
		        tblRow.appendChild(UI_commonSystem.createCell({
			        desc: "<input type='text' class='cbox' id='autoCheck_" + (i+1) + "' style='visibility:hidden;width:10px;'>",
			        id: "tdPaxACI_2_" + i,
			        css: UI_commonSystem.strDefClass + strClass,
			        align: "center"
		        }));
			}else{
				tblRow.appendChild(UI_commonSystem.createCell({
			        desc: "<input type='checkbox' class='cbox' id='autoCheck_" + (i+1) + "' onchange='UI_tabAnci.autoCheckinEnable(" + (i+1) + ");'>",
			        id: "tdPaxACI_2_" + i,
			        css: UI_commonSystem.strDefClass + strClass,
			        align: "center"
		        }));
			}
            $("#tbdyAnciACIPax").append(tblRow);
            i++;
        } while (--intLen);
    }
    var intLen = objPax.length;
}



UI_tabAnci.anciAutoCheckinDataReterive = function (){
	var index = UI_tabAnci.currSegRow;
	for (var i = 0; i < jsonPaxAdults.length; i++) {

		var selectId = "#" +(i+1)+ "_displaySeatPref";
		var chkId = "#autoCheck_" + (i+1);
		var emailId = "#"+ (i+1) + "_emailAddress";
		var infantEmailId = "#"+ (i+1) + "_infantEmailAddress";
		var seatPreference;
		if((UI_tabAnci.isAnciModify || UI_tabAnci.isRequote) && (jsonPaxAdults[i].currentAncillaries[index] != undefined) && (jsonPaxAdults[i].currentAncillaries[index].automaticCheckinDTOs.length > 0)){
			seatPreference = jsonPaxAdults[i].currentAncillaries[index].automaticCheckinDTOs[0].seatPref;
			$("#tblAnciACI").find(emailId).val(jsonPaxAdults[i].currentAncillaries[index].automaticCheckinDTOs[0].email);
			$("#tblAnciACIInfant").find(infantEmailId).val(jsonPaxAdults[i].currentAncillaries[index].automaticCheckinDTOs[0].email);
		}else{
			seatPreference = jsAnciSeg[index].pax[i].automaticCheckin.seatPreference;
			$("#tblAnciACI").find(emailId).val(jsAnciSeg[index].pax[i].automaticCheckin.email);
			$("#tblAnciACIInfant").find(infantEmailId).val(jsAnciSeg[index].pax[i].automaticCheckin.email);
		}

		if(seatPreference == 'S'){
			$("#tbdyAnciACIPax").find(selectId).prop('disabled', true);
			$("#tbdyAnciACIPax").find(selectId).css('color', '#BFBFBF');
			$("#tbdyAnciACIPax").find(chkId).attr('checked', true);

		}else{
			$("#tbdyAnciACIPax").find(selectId).val(seatPreference);
			$("#tbdyAnciACIPax").find(selectId).prop('disabled', false);
			$("#tbdyAnciACIPax").find(selectId).css('color', '#000000');
			$("#tbdyAnciACIPax").find(chkId).attr('checked', false);
		}
	}
}

UI_tabAnci.populateAutoCheckinData  = function (p) {
	var paxData = jsAnciSeg[parseInt(p.segID, 10)].pax;
    for (var i = 0; i < paxData.length; i++) {
    	var selectId = "#" +(i+1)+ "_displaySeatPref";
		var chkId = "#autoCheck_" + (i+1);
		var emailId = "#"+ (i+1) + "_emailAddress";
		var infantEmailId = "#"+ (i+1) + "_infantEmailAddress";
		var seatPreference;
		var email = paxData[i].automaticCheckin.email;
		var contactInfo = UI_tabAnci.contactInfo;
		
		seatPreference = paxData[i].automaticCheckin.seatPreference;
		
		if(UI_tabAnci.isAnciModify || UI_tabAnci.isRequote){
			if(email == ''){
				var currAncilaries = jsonPaxAdults[i].currentAncillaries;
				var currentAnciEmail;
				for (var j= 0; j < currAncilaries.length;j++){
					if(currAncilaries[j].automaticCheckinDTOs[0]=='undefined'){
						currentAnciEmail = currAncilaries[j].automaticCheckinDTOs[0].email;
					}else{
						currentAnciEmail = contactInfo.email;
					}
					if(currentAnciEmail != ''){
						$("#tblAnciACI").find(emailId).val(currentAnciEmail);
						$("#tblAnciACIInfant").find(infantEmailId).val(currentAnciEmail);
						break;
					}
				}
			}else{
				$("#tblAnciACI").find(emailId).val(email);
				$("#tblAnciACIInfant").find(infantEmailId).val(email);
			}
		}else{
			$("#tblAnciACI").find(emailId).val(email);
			$("#tblAnciACIInfant").find(infantEmailId).val(email);
		}
		
		if(seatPreference == 'S'){
			UI_tabAnci.setToGether(selectId,chkId);
		}else if(seatPreference == ''){
			if(jsAnciSeg[0].pax[i].automaticCheckin.seatPreference == 'S'){
				UI_tabAnci.setToGether(selectId,chkId);
			}else{
				UI_tabAnci.seatPrefrence(jsAnciSeg[0].pax[i].automaticCheckin.seatPreference,selectId,chkId);
			}
		}
		else{
			UI_tabAnci.seatPrefrence(seatPreference,selectId,chkId);
		}
    }
}

UI_tabAnci.setToGether = function(selectId,chkId){
	$("#tbdyAnciACIPax").find(selectId).prop('disabled', true);
	$("#tbdyAnciACIPax").find(selectId).css('color', '#BFBFBF');
	$("#tbdyAnciACIPax").find(chkId).attr('checked', true);
}
UI_tabAnci.seatPrefrence = function(seatVal,selectId,chkId){
	$("#tbdyAnciACIPax").find(selectId).val(seatVal);
	$("#tbdyAnciACIPax").find(selectId).prop('disabled', false);
	$("#tbdyAnciACIPax").find(selectId).css('color', '#000000');
	$("#tbdyAnciACIPax").find(chkId).attr('checked', false);
}

UI_tabAnci.autoCheckinEnable = function (paxId){
	if(jQuery("#autoCheck_" + paxId).is(":checked")){
		$('#tbdyAnciACIPax input:checkbox').prop('checked', true);
	}else{
		$('#tbdyAnciACIPax input:checkbox').prop('checked', false);
	}
	$("#tbdyAnciACIPax input").each(function(){
		if ($(this).is(":checked")) {
			$(this).parent().parent().parent().find('select').prop('disabled', true);
			$(this).parent().parent().parent().find('select').css('color', '#BFBFBF');
		}
		else{
			$(this).parent().parent().parent().find('select').prop('disabled', false);
			$(this).parent().parent().parent().find('select').css('color', '#000000');
		}
	});
}

/*
 * AutoCheckin Passenger Selection
 */
UI_tabAnci.tdAutoCheckinPaxOnClick = function () {
    // 0 - tdPaxSSR;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";
    var firstTime = false;
    var objPax = jsonPaxAdults;
    
    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxACI_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxACI_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    } else {
        firstTime = true;
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    $("#spnAutoCheckinPassenger").html(objPax[UI_tabAnci.currPaxIDRow].name);
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxACI_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxACI_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");
	
	var currentRow = parseInt(UI_tabAnci.currPaxIDRow);
	
	for(var i = 1; i <= jsonPaxAdults.length; i++){
		$("#tblAnciACI tr#"+i).css("display","none");
		$("#tblAnciACIInfant tr#"+i).css("display","none");
	}
	
	$("#tblAnciACI tr#"+ (currentRow+1)).css("display","table-row");
	
	if(jsonPaxAdults[currentRow].displayInfantTravelling == 'Y'){
		if(UI_tabAnci.isAnciModify || UI_tabAnci.isRequote){
			for(var i = 0; i < jsonPaxInfants.length; i++){
				if(parseInt(jsonPaxAdults[currentRow].displayInfantWith) == jsonPaxInfants[i].paxId){
					$("#tblAnciACIInfant tr#"+ (i+1)).css("display","table-row");
				}
			}
		}else{
			for(var i = 0; i < jsonPaxInfants.length; i++){
				if(parseInt(jsonPaxInfants[i].displayInfantTravellingWith) == (currentRow+1)){
					$("#tblAnciACIInfant tr#"+ (i+1)).css("display","table-row");
				}
			}
		}
		$('#gbox_tblAnciACIInfant').show();
	}else{
		$('#gbox_tblAnciACIInfant').hide();
	}
	
}

UI_tabAnci.anciAutoCheckinResetOnClick = function (refObj) {
	var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
	var fltRefNo = jsAnciSeg[UI_tabAnci.currSegRow].flightRefNumber;
	
	var refObj = {
        fltRefNo: fltRefNo,
    };

	
	var paxAutomaticCheckinSeatObj = {
        paxId: UI_tabAnci.currPaxIDRow,
        refObj: refObj,
        old: false
	};

	if(UI_tabAnci.isAnciModify){
		UI_tabAnci.clearModifyAutoCheck(paxAutoCheckinObj);
	}else{
		UI_tabAnci.clearAutomaticCheckin(paxAutomaticCheckinSeatObj);
	}
	
}

UI_tabAnci.anciAutoCheckinApplyOnClick = function () {
	var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
	var fltRefNo = jsAnciSeg[UI_tabAnci.currSegRow].flightRefNumber;
	
	for (var i = 0; i < jsonPaxAdults.length; i++) {
		var refObj = {
	        fltRefNo: fltRefNo,
	    };
	 	//var paxType = jsonPaxAdults[UI_tabAnci.currSegRow].type;
		var paxAutomaticCheckinSeatObj = {
	        paxId: i,
	        refObj: refObj,
	        old: false
		};
		if(UI_tabAnci.validate()){
			UI_tabAnci.markAutomaticCheckinSeat(paxAutomaticCheckinSeatObj);
		}
	}
}

UI_tabAnci.getCurrSegAutoCheckin = function(paxAutoCheckinObj, currAncillaries,fltRefNo){
	for (var i=0; i<currAncillaries.length; i++){
		if(currAncillaries[i].automaticCheckinDTOs.length > 0 && UI_tabAnci.compareFlightRefNo(
                currAncillaries[i].flightSegmentTO.flightRefNumber, fltRefNo)){
			paxAutoCheckinObj.automaticCheckin=currAncillaries[i].automaticCheckinDTOs[0];
			paxAutoCheckinObj.old=true;
			return paxAutoCheckinObj;
		}
	}
return paxAutoCheckinObj;
}

UI_tabAnci.autoCheckAnciOldObj =function (paxAutomaticCheckinSeatObj, paxId){
	var fltRfnNo=paxAutomaticCheckinSeatObj.refObj.fltRefNo;
	var paxAutoCheckinObj = {
		automaticCheckin : null,
        paxId: paxId,
        canClear: (paxAutomaticCheckinSeatObj== undefined)?true:false,
        old: false
	};
	if(paxAutomaticCheckinSeatObj != undefined && !paxAutomaticCheckinSeatObj.canClear){
		if(jsonPaxAdults !=null && jsonPaxAdults[paxAutoCheckinObj.paxId] && 
		jsonPaxAdults[paxAutoCheckinObj.paxId].currentAncillaries != null ){
		var currAncillaries = jsonPaxAdults[paxAutoCheckinObj.paxId].currentAncillaries;
		UI_tabAnci.getCurrSegAutoCheckin(paxAutoCheckinObj,currAncillaries,fltRfnNo);
		}
	}
		
   return paxAutoCheckinObj;
}

UI_tabAnci.validate = function () {
	var commonCalendarData = UI_tabAnci.commonCalendarData();
	var segments = jsAnciSeg[UI_tabAnci.currSegRow].orignNDest;
	var segment = segments.split("/")[0];
	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();
	var paxConfigAdult = paxConfig[segment].AD;
	var paxConfigInfant = paxConfig[segment].IN;
	var paxValidation = paxConfig.paxValidation;
	var selDateArr;
	var selDate;
	var compareDate;
	var paxVisibility;
	objJPax = jsonPaxAdults;
	if (objJPax != null) {
		paxVisibility = jsAutoCheckinList;
		UI_tabAnci.overrideAdultPaxConfig(paxVisibility);
		var intAdults = objJPax.length
		intLen = intAdults;
		var i = 0;
		if (intLen > 0) {
			do {
				paxrowId = "#tdPaxACI_0_" + i;
				selectorId = "#" + (i + 1) + "_expiryDate";
				compareDate = new Date(parseInt(commonCalendarData.curYear), parseInt(commonCalendarData.arrivalMonth) - 1,parseInt(commonCalendarData.arrivalDay) + 1);
				if(paxVisibility.passportExpiry.visibility && paxConfigAdult.passportExpiry.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Expiry Date",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						selDateArr = $(selectorId).val().split('/');
						selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
						if(selDate < compareDate){
							UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Passport Expiry Date")});
							$(paxrowId).click();
							$(selectorId).focus();
							return false;
						}
					}
				}else if(paxVisibility.passportExpiry.visibility && !paxConfigAdult.passportExpiry.mandatory){
					selDateArr = $(selectorId).val().split('/');
					selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					if((selDate < compareDate) && $(selectorId).val() != ''){
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Passport Expiry Date")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_passengerDOB";
				if(paxVisibility.dob.visibility && paxConfigAdult.dob.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Passenger Date of Birth",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						if (!UI_tabAnci.dateCompare(paxrowId,selectorId,objJPax[i].displayAdultType)) return false;
					}
				}else if(paxVisibility.dob.visibility && !paxConfigAdult.dob.mandatory){
					if (!UI_tabAnci.dateCompare(paxrowId,selectorId,objJPax[i].displayAdultType)) return false;
				}
				selectorId = "#" + (i + 1) + "_countryOfInsurance";
				if(paxVisibility.passportIssuedCntry.visibility && paxConfigAdult.passportIssuedCntry.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Country of Issurance",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_passportNumber";
				if(paxVisibility.passportNo.visibility && paxConfigAdult.passportNo.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Passport Number",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_placeOfBirth";
				if(paxVisibility.placeOfBirth.visibility && paxConfigAdult.placeOfBirth.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Place Of Birth",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_visaDocIssueDate";
				compareDate = new Date(parseInt(commonCalendarData.curYear), parseInt(commonCalendarData.curMonth)-1 , parseInt(commonCalendarData.curDay) - 1);
				
				if(paxVisibility.visaDocIssueDate.visibility && paxConfigAdult.visaDocIssueDate.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Visa Doc Issue Date",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						selDateArr = $(selectorId).val().split('/');
						selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
						if(selDate > compareDate){
							UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Document Issue Date")});
							$(paxrowId).click();
							$(selectorId).focus();
							return false;
						}
					}
				}else if(paxVisibility.visaDocIssueDate.visibility && !paxConfigAdult.visaDocIssueDate.mandatory){
					selDateArr = $(selectorId).val().split('/');
					selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					if((selDate > compareDate) && $(selectorId).val() != ''){
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Document Issue Date")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_travelDocumentType";
				if(paxVisibility.travelDocumentType.visibility && paxConfigAdult.travelDocumentType.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Travel Document Type",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_visaDocNumber";
				if(paxVisibility.visaDocNumber.visibility && paxConfigAdult.visaDocNumber.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "visa Doc Number",
						required : true
					}))
					{
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						if(!isNumeric(trim($("#"+(i + 1)+ "_visaDocNumber").val()))) {
							UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Doc Number")});
							$(paxrowId).click();
							$("#"+(i + 1)+ "_visaDocNumber").focus();
							return false;
						}
					}
				}else if(paxVisibility.visaDocNumber.visibility && !paxConfigAdult.visaDocNumber.mandatory){
					if(!isNumeric(trim($(selectorId).val())) && $(selectorId).val() != '') {
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Doc Number")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_visaDocPlaceOfIssue";
				if(paxVisibility.visaDocPlaceOfIssue.visibility && paxConfigAdult.visaDocPlaceOfIssue.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "visa Doc PlaceOfIssue",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_visaApplicableCountry";
				if(paxVisibility.visaApplicableCountry.visibility && paxConfigAdult.visaApplicableCountry.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "visa Applicable Country",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_emailAddress";
				
				if (!UI_commonSystem.controlValidate( {
					id : selectorId,
					desc : "Email Id",
					required : true
				})) {
					$(paxrowId).click();
					$(selectorId).focus();
					return false;
				}else{
					if (!checkEmail($(selectorId).val())) {
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Email Id")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}

				i++;
			} while (--intLen);
		}
		UI_tabAnci.setAutoCheckinPassenger();
	}
	var objInfPax = jsonPaxInfants;
	if (objInfPax.length > 0) {
		paxVisibility = jsInfantAutoCheckinList.global;
		UI_tabAnci.overrideInfantPaxConfig(paxVisibility);
		
		var intInfants = objInfPax.length
		intLen = intInfants;
		var i = 0;
		if (intLen > 0) {
			do {
				paxrowId = "#tdPaxACI_0_" + i;
				selectorId = "#" + (i + 1) + "_infantExpiryDate";
				compareDate = new Date(parseInt(commonCalendarData.curYear), parseInt(commonCalendarData.arrivalMonth) - 1,parseInt(commonCalendarData.arrivalDay) + 1);
				if(paxVisibility.passportExpiry.visibility && paxConfigInfant.passportExpiry.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Expiry Date",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						selDateArr = $(selectorId).val().split('/');
						selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
						if(selDate < compareDate){
							UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Passport Expiry Date")});
							$(paxrowId).click();
							$(selectorId).focus();
							return false;
						}
					}
				}else if(paxVisibility.passportExpiry.visibility && !paxConfigInfant.passportExpiry.mandatory){
					selDateArr = $(selectorId).val().split('/');
					selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					if((selDate < compareDate) && $(selectorId).val() != ''){
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Passport Expiry Date")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantPassengerDOB";
				if(paxVisibility.dob.visibility && paxConfigInfant.dob.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Passenger Date of Birth",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						if (!UI_tabAnci.infantDateCompare(paxrowId,selectorId)) return false;
					}
				}else if(paxVisibility.dob.visibility && !paxConfigInfant.dob.mandatory){
					if (!UI_tabAnci.infantDateCompare(paxrowId,selectorId)) return false;
				}
				selectorId = "#" + (i + 1) + "_infantCountryOfInsurance";
				if(paxVisibility.passportIssuedCntry.visibility && paxConfigInfant.passportIssuedCntry.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Country of Issurance",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantPassportNumber";
				if(paxVisibility.passportNo.visibility && paxConfigInfant.passportNo.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Passport Number",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantPlaceOfBirth";
				if(paxVisibility.placeOfBirth.visibility && paxConfigInfant.placeOfBirth.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Place Of Birth",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantVisaDocIssueDate";
				compareDate = new Date(parseInt(commonCalendarData.curYear), parseInt(commonCalendarData.curMonth)-1 , parseInt(commonCalendarData.curDay) - 1);
				if(paxVisibility.visaDocIssueDate.visibility && paxConfigInfant.visaDocIssueDate.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Visa Doc Issue Date",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						selDateArr = $(selectorId).val().split('/');
						selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
						if(selDate > compareDate){
							UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Document Issue Date")});
							$(selectorId).focus();
							$(paxrowId).click();
							return false;
						}
					}
				}else if(paxVisibility.visaDocIssueDate.visibility && !paxConfigInfant.visaDocIssueDate.mandatory){
					selDateArr = $(selectorId).val().split('/');
					selDate = new Date(selDateArr[2], selDateArr[1]-1, selDateArr[0]);
					if((selDate > compareDate) && $(selectorId).val() != ''){
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Document Issue Date")});
						$(selectorId).focus();
						$(paxrowId).click();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantTravelDocumentType";
				if(paxVisibility.travelDocumentType.visibility && paxConfigInfant.travelDocumentType.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "Travel Document Type",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantVisaDocNumber";
				if(paxVisibility.visaDocNumber.visibility && paxConfigInfant.visaDocNumber.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "visa Doc Number",
						required : true
					}))
					{
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}else{
						if(!isNumeric(trim($(selectorId).val()))) {
							UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Doc Number")});
							$(paxrowId).click();
							$(selectorId).focus();
							return false;
						}
					}
				}else if(paxVisibility.visaDocNumber.visibility && !paxConfigInfant.visaDocNumber.mandatory){
					if(!isNumeric(trim($(selectorId).val())) && $(selectorId).val() != '') {
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Visa Doc Number")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantVisaDocPlaceOfIssue";
				if(paxVisibility.visaDocPlaceOfIssue.visibility && paxConfigInfant.visaDocPlaceOfIssue.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "visa Doc PlaceOfIssue",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				selectorId = "#" + (i + 1) + "_infantVisaApplicableCountry";
				if(paxVisibility.visaApplicableCountry.visibility && paxConfigInfant.visaApplicableCountry.mandatory){
					if (!UI_commonSystem.controlValidate( {
						id : selectorId,
						desc : "visa Applicable Country",
						required : true
					})) {
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}
				
				selectorId = "#" + (i + 1) + "_infantEmailAddress";
				if (!UI_commonSystem.controlValidate( {
					id : selectorId,
					desc : "Email Id",
					required : true
				})) {
					$(paxrowId).click();
					$(selectorId).focus();
					return false;
				}else{
					if (!checkEmail($(selectorId).val())) {
						UI_message.showErrorMessage({messageText: buildError(UI_tabAnci.arrError["XBE-ERR-04"], "Email Id")});
						$(paxrowId).click();
						$(selectorId).focus();
						return false;
					}
				}

				i++;
			} while (--intLen);
		}
		UI_tabAnci.setAutoCheckinPassengerInfant();
	}
	return true;
}

function buildErrorMsg(strMessage){
	if (arguments.length >1){
		for (var i = 0 ; i < arguments.length - 1 ; i++){
			strMessage = strMessage.replace("#" + (i+1), arguments[i+1]);
		}
		strMessage = strMessage
	}
	return strMessage;
}

UI_tabAnci.dateCompare = function(paxrowId,selectorId,paxType) {
	var commonCalendarData = UI_tabAnci.commonCalendarData();
	var paxConfig = UI_tabAnci.getPaxContactConfigDetail();
	var paxValidation = paxConfig.paxValidation;
	
	var minAge;
	var maxAge;
	var strPaxDesc = "";
	
	if(paxType == 'AD'){
		minAge = paxConfig.paxValidation.childAgeCutOverYears;
		maxAge = paxConfig.paxValidation.adultAgeCutOverYears;
		strPaxDesc = "Adult";
	}else{
		minAge = paxConfig.paxValidation.infantAgeCutOverYears;
		maxAge = paxConfig.paxValidation.childAgeCutOverYears;
		strPaxDesc = "Child";
	}
	
	if (!ageCompare($(selectorId).val(),commonCalendarData.arrivalDate, maxAge)) {
		showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,strPaxDesc));
		$(paxrowId).click();
		$(selectorId).focus();
		return false;
	}
	if(ageCompare($(selectorId).val(),commonCalendarData.depatureDate, minAge)){
		showERRMessage(raiseError("XBE-ERR-64", strPaxDesc,strPaxDesc));
		$(paxrowId).click();
		$(selectorId).focus();
		return false;
	}
	return true;
}


UI_tabAnci.infantDateCompare = function(paxrowId,selectorId) {
	var commonCalendarData = UI_tabAnci.commonCalendarData();
	
	if (!CheckDates($(selectorId).val(), top.arrParams[0])) {							
		showERRMessage(raiseError("XBE-ERR-63", "Date of birth"));
		$(paxrowId).click();
		$(selectorId).focus();
		return false;
	}

	if (!ageCompare($(selectorId).val(), commonCalendarData.arrivalDate,top.strDefInfantAge)) {
		showERRMessage(raiseError("XBE-ERR-64", "Infant","Infant"));
		$(paxrowId).click();
		$(selectorId).focus();
		return false;
	}
	
	if (!ageCompare($(selectorId).val(), commonCalendarData.depatureDate,top.strDefInfantAge)) {
		showERRMessage(raiseError("XBE-ERR-64", "Infant","Infant"));
		$(paxrowId).click();
		$(selectorId).focus();
		return false;
	}
	
	var infantAgeLowerBoundInDays = top.infantAgeLowerBoundaryInDays;					
					
	if(!UI_tabPassenger.checkInfantMinAge(($(selectorId).val()),commonCalendarData.depatureDate,infantAgeLowerBoundInDays))  {
		showERRMessage(raiseError("XBE-ERR-70","Date of birth",infantAgeLowerBoundInDays));
		$(paxrowId).click();
		$(selectorId).focus();
		return false;
	}
	return true;
}



UI_tabAnci.setAutoCheckinPassenger = function() {
	
	var objJPax = jsonPaxAdults;
	for ( var jl = 0; jl < objJPax.length; jl++) {
		if($("#" + (jl + 1) + "_visaApplicableCountry").val() != null && $("#" + (jl + 1) + "_visaApplicableCountry").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayVisaApplicableCountry = $("#" + (jl + 1) + "_visaApplicableCountry").val();
			}
			jsonPaxAdults[jl].displayVisaApplicableCountry = $("#" + (jl + 1) + "_visaApplicableCountry").val();
		}
		if($("#" + (jl + 1) + "_travelDocumentType").val() != null && $("#" + (jl + 1) + "_travelDocumentType").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayTravelDocType = $("#" + (jl + 1) + "_travelDocumentType").val();
			}
			jsonPaxAdults[jl].displayTravelDocType = $("#" + (jl + 1) + "_travelDocumentType").val();
		}
		if($("#" + (jl + 1) + "_visaDocIssueDate").val() != null && $("#" + (jl + 1) + "_visaDocIssueDate").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayVisaDocIssueDate = $("#" + (jl + 1) + "_visaDocIssueDate").val();
			}
			jsonPaxAdults[jl].displayVisaDocIssueDate = $("#" + (jl + 1) + "_visaDocIssueDate").val();
		}
		if($("#" + (jl + 1) + "_visaDocNumber").val() != null && $("#" + (jl + 1) + "_visaDocNumber").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayVisaDocNumber = $("#" + (jl + 1) + "_visaDocNumber").val();
			}
			jsonPaxAdults[jl].displayVisaDocNumber = $("#" + (jl + 1) + "_visaDocNumber").val();
		}
		if($("#" + (jl + 1) + "_visaDocPlaceOfIssue").val() != null && $("#" + (jl + 1) + "_visaDocPlaceOfIssue").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayVisaDocPlaceOfIssue = $("#" + (jl + 1) + "_visaDocPlaceOfIssue").val();
			}
			jsonPaxAdults[jl].displayVisaDocPlaceOfIssue = $("#" + (jl + 1) + "_visaDocPlaceOfIssue").val();
		}
		if($("#" + (jl + 1) + "_passportNumber").val() != null && $("#" + (jl + 1) + "_passportNumber").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayPnrPaxCatFOIDNumber = $("#" + (jl + 1) + "_passportNumber").val();
			}
			jsonPaxAdults[jl].displayPnrPaxCatFOIDNumber = $("#" + (jl + 1) + "_passportNumber").val();
		}
		if($("#" + (jl + 1) + "_expiryDate").val() != null && $("#" + (jl + 1) + "_expiryDate").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayPnrPaxCatFOIDExpiry = $("#" + (jl + 1) + "_expiryDate").val();
			}
			jsonPaxAdults[jl].displayPnrPaxCatFOIDExpiry = $("#" + (jl + 1) + "_expiryDate").val();
		}
		if($("#" + (jl + 1) + "_countryOfInsurance").val() != null && $("#" + (jl + 1) + "_countryOfInsurance").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayPnrPaxCatFOIDPlace = $("#" + (jl + 1) + "_countryOfInsurance").val();
			}
			jsonPaxAdults[jl].displayPnrPaxCatFOIDPlace = $("#" + (jl + 1) + "_countryOfInsurance").val();
		}
		if($("#" + (jl + 1) + "_passengerDOB").val() != null && $("#" + (jl + 1) + "_passengerDOB").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayAdultDOB = $("#" + (jl + 1) + "_passengerDOB").val();
			}
			jsonPaxAdults[jl].displayAdultDOB = $("#" + (jl + 1) + "_passengerDOB").val();
			$("#" + (jl + 1) + "_displayAdultDOB").val($("#" + (jl + 1) + "_passengerDOB").val());
			
		}
		if($("#" + (jl + 1) + "_placeOfBirth").val() != null && $("#" + (jl + 1) + "_placeOfBirth").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxAdults[jl].displayPnrPaxPlaceOfBirth = $("#" + (jl + 1) + "_placeOfBirth").val();
			}
			jsonPaxAdults[jl].displayPnrPaxPlaceOfBirth = $("#" + (jl + 1) + "_placeOfBirth").val();
		}
		var flightSegment = UI_tabAnci.flightSegmentInfo;
		for (var j=0;j<flightSegment.length;j++){
			if($("#" + (jl + 1) + "_emailAddress").val() != null && $("#" + (jl + 1) + "_emailAddress").val() != "" ){
				jsAnciSeg[j].pax[jl].automaticCheckin.email = $("#" + (jl + 1) + "_emailAddress").val();
			}
		}
	}
}

UI_tabAnci.setAutoCheckinPassengerInfant = function() {
	
	var objJPaxInfant = jsonPaxInfants;
	for ( var jl = 0; jl < objJPaxInfant.length; jl++) {
		if($("#" + (jl + 1) + "_infantVisaApplicableCountry").val() != null && $("#" + (jl + 1) + "_infantVisaApplicableCountry").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayVisaApplicableCountry = $("#" + (jl + 1) + "_infantVisaApplicableCountry").val();
			}
			jsonPaxInfants[jl].displayVisaApplicableCountry = $("#" + (jl + 1) + "_infantVisaApplicableCountry").val();
		}
		if($("#" + (jl + 1) + "_infantTravelDocumentType").val() != null && $("#" + (jl + 1) + "_infantTravelDocumentType").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayTravelDocType = $("#" + (jl + 1) + "_infantTravelDocumentType").val();
			}
			jsonPaxInfants[jl].displayTravelDocType = $("#" + (jl + 1) + "_infantTravelDocumentType").val();
		}
		if($("#" + (jl + 1) + "_infantVisaDocIssueDate").val() != null && $("#" + (jl + 1) + "_infantVisaDocIssueDate").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayVisaDocIssueDate = $("#" + (jl + 1) + "_infantVisaDocIssueDate").val();
			}
			jsonPaxInfants[jl].displayVisaDocIssueDate = $("#" + (jl + 1) + "_infantVisaDocIssueDate").val();
		}
		if($("#" + (jl + 1) + "_infantVisaDocNumber").val() != null && $("#" + (jl + 1) + "_infantVisaDocNumber").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayVisaDocNumber = $("#" + (jl + 1) + "_infantVisaDocNumber").val();
			}
			jsonPaxInfants[jl].displayVisaDocNumber = $("#" + (jl + 1) + "_infantVisaDocNumber").val();
		}
		if($("#" + (jl + 1) + "_infantVisaDocPlaceOfIssue").val() != null && $("#" + (jl + 1) + "_infantVisaDocPlaceOfIssue").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayVisaDocPlaceOfIssue = $("#" + (jl + 1) + "_infantVisaDocPlaceOfIssue").val();
			}
			jsonPaxInfants[jl].displayVisaDocPlaceOfIssue = $("#" + (jl + 1) + "_infantVisaDocPlaceOfIssue").val();
		}
		if($("#" + (jl + 1) + "_infantPassportNumber").val() != null && $("#" + (jl + 1) + "_infantPassportNumber").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayPnrPaxCatFOIDNumber = $("#" + (jl + 1) + "_infantPassportNumber").val();
			}
			jsonPaxInfants[jl].displayPnrPaxCatFOIDNumber = $("#" + (jl + 1) + "_infantPassportNumber").val();
		}
		if($("#" + (jl + 1) + "_infantExpiryDate").val() != null && $("#" + (jl + 1) + "_infantExpiryDate").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayPnrPaxCatFOIDExpiry = $("#" + (jl + 1) + "_infantExpiryDate").val();
			}
			jsonPaxInfants[jl].displayPnrPaxCatFOIDExpiry = $("#" + (jl + 1) + "_infantExpiryDate").val();
		}
		if($("#" + (jl + 1) + "_infantCountryOfInsurance").val() != null && $("#" + (jl + 1) + "_infantCountryOfInsurance").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayPnrPaxCatFOIDPlace = $("#" + (jl + 1) + "_infantCountryOfInsurance").val();
			}
			jsonPaxInfants[jl].displayPnrPaxCatFOIDPlace = $("#" + (jl + 1) + "_infantCountryOfInsurance").val();
		}
		if($("#" + (jl + 1) + "_infantPassengerDOB").val() != null && $("#" + (jl + 1) + "_infantPassengerDOB").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayInfantDOB = $("#" + (jl + 1) + "_infantPassengerDOB").val();
			}
			jsonPaxInfants[jl].displayInfantDOB = $("#" + (jl + 1) + "_infantPassengerDOB").val();
			$("#" + (jl + 1) + "_displayInfantDOB").val($("#" + (jl + 1) + "_infantPassengerDOB").val());
		}
		if($("#" + (jl + 1) + "_infantPlaceOfBirth").val() != null && $("#" + (jl + 1) + "_infantPlaceOfBirth").val() != "" ){
			if(!UI_tabAnci.isRequote && !UI_tabAnci.isAnciModify){
				jsonPaxAdtObj.paxInfants[jl].displayPnrPaxPlaceOfBirth = $("#" + (jl + 1) + "_infantPlaceOfBirth").val();
			}
			jsonPaxInfants[jl].displayPnrPaxPlaceOfBirth = $("#" + (jl + 1) + "_infantPlaceOfBirth").val();
		}
		
	}
}
/*
 * SSR Apply On Click
 */
UI_tabAnci.anciSSRApplyOnClick = function () {
    if (!UI_tabAnci.isSSRSelected()) {
        showERRMessage('Please select a SSR to assign');
        return




    }
    if (UI_tabAnci.validateApply()) {
        var ssrNAv = UI_tabAnci.ssrNotAvailable(UI_tabAnci.currPaxIDRow);
        if (ssrNAv != null) {
            showERRMessage('Not enough SSRs from "' + ssrNAv.ssrName + '", to assign to passenger');
            return;
        }
        UI_tabAnci.paxSSRRemove(UI_tabAnci.currPaxIDRow, false);
        for (var i = 0; i < jsSSRList.length; i++) {
            var text = $("#" + (i + 1) + "_text").val();
            if (jsSSRList[i].status == 'Y') {
                UI_tabAnci.paxSSRAdd(UI_tabAnci.currPaxIDRow, jsSSRList[i],
                    text, i);
            }
        }
    }
}

/*
 * SSR ApplyToAll On Click
 */
UI_tabAnci.anciSSRApplyToAllOnClick = function () {
    UI_commonSystem.initializeMessage();
    if (!UI_tabAnci.isSSRSelected()) {
        showERRMessage('Please select a SSR to assign');
        return




    }

    var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;

    if (UI_tabAnci.isAnciModify && !_isAllPaxHasDisabledSSRService(paxList)) {
        showERRMessage('Some of the SSR(s) currently not available.');
        return;
    }

    var ssrNAv = UI_tabAnci.ssrNotAvailableForAll();
    if (ssrNAv != null) {
        showERRMessage('Not enough SSRs from "' + ssrNAv.ssrName + '", to assign to all');
        return;
    }
    if (confirm('This will replace existing SSRs if any!\nDo you want to continue?')) {
        var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
        for (var paxI = 0; paxI < paxList.length; paxI++) {
            UI_tabAnci.paxSSRRemove(paxI, false);
            var objList = [];
            for (var i = 0; i < jsSSRList.length; i++) {
                var text = $("#" + (i + 1) + "_text").val();
                if (jsSSRList[i].status == 'Y') {
                    objList[objList.length] = {
                        ssr: jsSSRList[i],
                        text: text,
                        ssrIndex: i
                    };
                    // UI_tabAnci.paxSSRAdd(paxI, jsSSRList[i],text,i);
                }

            }
            UI_tabAnci.paxSSRBulkAdd(paxI, objList)
        }
    }

}

_isAllPaxHasDisabledSSRService = function (paxList) {
    for (var paxI = 0; paxI < paxList.length; paxI++) {
        var ssrData = paxList[paxI].ssr;
        var paxSSRs = ssrData.ssrs;

        for (var j = 0; j < jsSSRList.length; j++) {
            if (jsSSRList[j].status == "Y" && jsSSRList[j].serviceStatus == "N") {
                if (!_paxHasService(paxSSRs, jsSSRList[j].ssrCode)) {
                    return false;
                }
            }
        }
    }

    return true;
}

_paxHasService = function (paxSSRs, ssrCode) {
    for (var i = 0; i < paxSSRs.length; i++) {
        if (paxSSRs[i].ssrCode == ssrCode) {
            return true;
        }
    }

    return false;
}

/*
 * In SSR selected?
 */
UI_tabAnci.isSSRSelected = function () {
    var retVal = false;
    var selArr = $('#tblAnciSSR').getGridParam('selarrrow');
    for (var i = 0; i < jsSSRList.length; i++) {
        jsSSRList[i].status = 'N';
    }
    for (var i = 0; i < selArr.length; i++) {
        jsSSRList[parseInt(selArr[i], 10) - 1].status = 'Y';
        retVal = true;
    }
    return retVal;
}

/*
 * Clear pax SSRs
 */
UI_tabAnci.paxSSRRemove = function (paxId, refresh) {
	var ssrPax = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].ssr;
    var removedSSRs = jsCurrAnciSeg[UI_tabAnci.currSegRow].pax[paxId].ssr.ssrs;

    UI_tabAnci.clearPaxSSRDetail(paxId);
    $("#tdPaxSSR_1_" + paxId).html(" ");
    $("#tdPaxSSR_2_" + paxId).html(" ");
    while (ssrPax.ssrs.length > 0) {
        var ssr = ssrPax.ssrs.pop();
        if (ssr.old) {
            removedSSRs[removedSSRs.length] = ssr;
        }
        
        if (!(UI_tabAnci.isPaxContainsSsr(paxId) && !UI_tabAnci.isSsrRefundableInModification && UI_tabAnci.isAnciModify)){
        	UI_tabAnci.updatepaxTotals(paxId, ssrPax.ssrChargeTotal, 'DEDUCT');
            UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.SSR, ssr.charge,
                    'DEDUCT', paxId, ssr.old);
        }
        UI_tabAnci.updateSSRAvailCount(ssr, 1, 'ADD');
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.SSR, 1, 'DEDUCT',
            ssr.old);
    }
    if (refresh && paxId == UI_tabAnci.currPaxIDRow) {
        UI_tabAnci.refreshSSR();
    }
    // ssrPax.srrs=[];
    ssrPax.ssrChargeTotal = 0;
}

UI_tabAnci.paxSSRBulkAdd = function (paxId, objList) {
    var ssrData = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].ssr;
    var paxSSRs = ssrData.ssrs;
    var ssrId = null;
    for (var i = 0; i < objList.length; i++) {
        var ssr = objList[i].ssr;
        var text = objList[i].text;
        var ssrIndex = objList[i].ssrIndex;

        ssrId = paxSSRs.length;
        paxSSRs[ssrId] = {
            ssrCode: ssr.ssrCode,
            ssrName: ssr.ssrName,
            description: ssr.description,
            charge: ssr.charge,
            text: text,
            ssrIndex: ssrIndex,
            old: false
        };
        ssrData.ssrChargeTotal += parseFloat(ssr.charge);
        if (!(UI_tabAnci.isPaxContainsSsr(paxId) && !UI_tabAnci.isSsrRefundableInModification && UI_tabAnci.isAnciModify)){
        	UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'ADD');
        	UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.SSR, ssr.charge, 'ADD',
                    paxId, false);
        }
        UI_tabAnci.updateSSRAvailCount(paxSSRs[ssrId], 1, 'DEDUCT');
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.SSR, 1, 'ADD', false);
    }
    UI_tabAnci.buildPaxSSRDetail(paxSSRs, paxId);

    $("#tdPaxSSR_1_" + paxId).html(" ");
    $("#tdPaxSSR_2_" + paxId).html(
        $.airutil.format.currency(ssrData.ssrChargeTotal));
}

UI_tabAnci.isPaxContainsSsr = function (paxId) {
	var isPaxContainsSsr = false;
	var pax = jsonPaxAdults[paxId];
	var anciSegs = [];
	anciSegs = pax.currentAncillaries;
	
        if (anciSegs != null && anciSegs.length != 0){
        	
            for (var k = 0; k < anciSegs.length; k++) {
                var anciSeg = anciSegs[k];
                if (anciSeg.specialServiceRequestDTOs != null && anciSeg.specialServiceRequestDTOs.length != null && anciSeg.specialServiceRequestDTOs.length > 0) {
                	isPaxContainsSsr = true;
                	break;
                }
            }
        }

	return isPaxContainsSsr;
}

/*
 * Add pax SSR
 */
UI_tabAnci.paxSSRAdd = function (paxId, ssr, text, ssrIndex) {
    var ssrData = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].ssr;
    var paxSSRs = ssrData.ssrs;
    var ssrId = null;

    ssrId = paxSSRs.length;
    paxSSRs[ssrId] = {
        ssrCode: ssr.ssrCode,
        ssrName: ssr.ssrName,
        description: ssr.description,
        charge: ssr.charge,
        text: text,
        ssrIndex: ssrIndex,
        old: false
    };
    ssrData.ssrChargeTotal += parseFloat(ssr.charge);

    if (!(UI_tabAnci.isPaxContainsSsr(paxId) && !UI_tabAnci.isSsrRefundableInModification && UI_tabAnci.isAnciModify)){
    	 UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'ADD');
    	 UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.SSR, ssr.charge, 'ADD',
    	        paxId, false);
    }
    UI_tabAnci.updateSSRAvailCount(paxSSRs[ssrId], 1, 'DEDUCT');
    UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.SSR, 1, 'ADD', false);

    UI_tabAnci.buildPaxSSRDetail(paxSSRs, paxId);

    $("#tdPaxSSR_1_" + paxId).html(" ");
    $("#tdPaxSSR_2_" + paxId).html(
        $.airutil.format.currency(ssrData.ssrChargeTotal));

}
/*
 * Pax Single SSR Remove
 */
UI_tabAnci.paxSingleSSRRemove = function (paxId, ssrCode) {
	var ssrPax = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].ssr;
    var paxSSRs = ssrPax.ssrs;
    var ssr = null;
    for (var i = 0; i < paxSSRs.length; i++) {
        if (paxSSRs[i].ssrCode == ssrCode) {
            ssr = paxSSRs.splice(i, 1)[0];
            break;
        }
    }
    if (ssr != null) {
        if (ssr.old) {
            var removedSSRs = jsCurrAnciSeg[UI_tabAnci.currSegRow].pax[paxId].ssr.ssrs;
            removedSSRs[removedSSRs.length] = ssr;
        }
        ssrPax.ssrChargeTotal -= ssr.charge;
        UI_tabAnci.updateSSRAvailCount(ssr, 1, 'ADD');
        if (!(UI_tabAnci.isPaxContainsSsr(paxId) && !UI_tabAnci.isSsrRefundableInModification && UI_tabAnci.isAnciModify)){
        	UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'DEDUCT');
            UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.SSR, ssr.charge,
                'DEDUCT', paxId, ssr.old);
        }
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.SSR, 1, 'DEDUCT',
            ssr.old);
        UI_tabAnci.clearPaxSSRDetail(paxId, ssrCode);

        $("#tdPaxSSR_1_" + paxId).html(" ");
        $("#tdPaxSSR_2_" + paxId).html(
            $.airutil.format.currency(ssrPax.ssrChargeTotal));

        if (paxId == UI_tabAnci.currPaxIDRow) {
            UI_tabAnci.refreshSSR();
        }
    }
}

UI_tabAnci.populateModifyAutoCheckinData = function (p) {
	for (var j = 0; j < jsonPaxAdults.length; j++) {
		 var anciSegs = jsonPaxAdults[j].currentAncillaries;
		for (var k = 0; k < anciSegs.length; k++) {
			var anciSeg = anciSegs[k];
			if (anciSeg.automaticCheckinDTOs != null && anciSeg.automaticCheckinDTOs.length != null && anciSeg.automaticCheckinDTOs.length > 0) {
				var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
				var autoCheckins = anciSeg.automaticCheckinDTOs;
				var currentSeg = UI_tabAnci.compareFlightRefNo(p.fltRefNo,fltRefNo);
				var segIndex = jsAnciSegRefMap[fltRefNo].index;
				var autoCheckinData = jsAnciSeg[segIndex].pax[j].automaticCheckin;
				var autoCheckinAvailable = UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[segIndex].ancillaryStatusList,UI_tabAnci.anciType.AUTOMATICCHECKIN);
				if(autoCheckinAvailable){
					for (var i = 0; i < autoCheckins.length; i++) {
						var autoCheckin = autoCheckins[i];
						
						var autoCheckinObj = {
                            autoCheckinId: autoCheckin.autoCheckinId,
                            automaticCheckinCharge: autoCheckin.automaticCheckinCharge,
                            email: autoCheckin.email,
                            seatPref: autoCheckin.seatPref,
                            status: autoCheckin.status,
                            old: true
                        };
	
						if (!UI_tabAnci.modifyautoCheckinLoaded) {

							autoCheckinData.autoCheckinTemplateId = autoCheckinObj.autoCheckinId;
							autoCheckinData.email = autoCheckinObj.email;
							autoCheckinData.seatPreference = autoCheckinObj.seatPref;
							autoCheckinData.status = autoCheckinObj.status;
							autoCheckinData.autoCheckinCharge += parseFloat(autoCheckinObj.automaticCheckinCharge);
                            
                            UI_tabAnci.updatepaxTotals(j, autoCheckinObj.automaticCheckinCharge, 'ADD');
                            UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.AUTOMATICCHECKIN, autoCheckinObj.automaticCheckinCharge,
									'ADD', j, autoCheckinObj.old);
							UI_tabAnci.updateAnciCounts(
                                UI_tabAnci.anciType.AUTOMATICCHECKIN,
                                1, 'ADD', true);
                        }

					}
				}
			}
		}
	}
	UI_tabAnci.modifyautoCheckinLoaded = true;
}

UI_tabAnci.populateModifySSRData = function (p) {
    for (var j = 0; j < jsonPaxAdults.length; j++) {
        var anciSegs = jsonPaxAdults[j].currentAncillaries;
        for (var k = 0; k < anciSegs.length; k++) {
            var anciSeg = anciSegs[k];
            if (anciSeg.specialServiceRequestDTOs != null && anciSeg.specialServiceRequestDTOs.length != null && anciSeg.specialServiceRequestDTOs.length > 0) {
                var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
                var ssrs = anciSeg.specialServiceRequestDTOs;
                var currentSeg = UI_tabAnci.compareFlightRefNo(p.fltRefNo,
                    fltRefNo);
                var segIndex = jsAnciSegRefMap[fltRefNo].index;
                var ssrData = jsAnciSeg[segIndex].pax[j].ssr;
                var ssrAvailable = UI_tabAnci.isAnciAvailable(
                    jsonSegAnciAvail[segIndex].ancillaryStatusList,
                    UI_tabAnci.anciType.SSR);
                if (ssrAvailable) {
                    for (var i = 0; i < ssrs.length; i++) {
                        var ssr = ssrs[i];
                        var ssrRefObj = UI_tabAnci.getSSRRefObj(ssr.ssrCode,
                            fltRefNo);
                        
                        //TEMP FIX: to over come existing PNR with SSR got saved against wrong Segment
                        //above issue has been fixed we need following null check for existing PNR only
                        if (typeof ssrRefObj == "undefined" || ssrRefObj == null) {
                            continue;
                        }
                        
                        var ssrObj = {
                            ssrCode: ssr.ssrCode,
                            ssrName: ssrRefObj.ssrName,
                            description: ssr.description,
                            charge: ssrRefObj.charge,
                            text: ssr.text,
                            ssrIndex: ssrRefObj.smI,
                            old: true
                        };

                        if (!UI_tabAnci.modifySSRLoaded) {
                            ssrData.ssrs[ssrData.ssrs.length] = ssrObj;

                            ssrData.ssrChargeTotal += parseFloat(ssrObj.charge);

                            UI_tabAnci.updatepaxTotals(j, ssr.charge, 'ADD');
                            UI_tabAnci.updateAnciTotals(
                                UI_tabAnci.anciType.SSR, ssrObj.charge,
                                'ADD', j, ssrObj.old);
                        }

                        if (currentSeg) {
                            UI_tabAnci.updateSSRAvailCount(ssrObj, 1, 'ADD');
                        }
                    }
                }
            }
        }
    }
    UI_tabAnci.modifySSRLoaded = true;
}

UI_tabAnci.getSSRRefObj = function (ssrCode, fltRefNo) {
    if (UI_tabAnci.ssrRefMap[fltRefNo] == null) {
        UI_tabAnci.ssrRefMap[fltRefNo] = {
            ssrs: {}
        };
        for (var i = 0; i < jsSSRModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsSSRModel[i].flightSegmentTO.flightRefNumber, fltRefNo)) {
                for (var j = 0; j < jsSSRModel[i].specialServiceRequest.length; j++) {
                    var ts = jsSSRModel[i].specialServiceRequest[j];

                    UI_tabAnci.ssrRefMap[fltRefNo].ssrs[ts.ssrCode] = {
                        ssrCode: ts.ssrCode,
                        ssrName: ts.ssrName,
                        serviceQuantity: ts.serviceQuantity,
                        availableQty: ts.availableQty,
                        charge: $.airutil.format.currency(ts.charge),
                        description: ts.description,
                        smI: j,
                        smSGI: i
                    };
                }
            }
        }
    }
    var ssrRef = UI_tabAnci.ssrRefMap[fltRefNo].ssrs[ssrCode];

    return ssrRef;
}

/*
 * Populate SSR Data
 */
UI_tabAnci.populateSSRData = function (p) {
    var ssrData = null;
    var paxId = null;

    var paxData = jsAnciSeg[parseInt(p.segID, 10)].pax;
    for (var i = 0; i < paxData.length; i++) {
        ssrData = paxData[i].ssr;

        UI_tabAnci.buildPaxSSRDetail(ssrData.ssrs, i);

        for (var j = 0; j < ssrData.ssrs.length; j++) {
            UI_tabAnci.updateSSRAvailCount(ssrData.ssrs[j], 1, 'DEDUCT');
        }

        $("#tdPaxSSR_1_" + i).html(" ");
        $("#tdPaxSSR_2_" + i).html(
            $.airutil.format.currency(ssrData.ssrChargeTotal));
    }
}

/*
 * Clear Pax SSR details
 */
UI_tabAnci.clearPaxSSRDetail = function (paxId, ssrCode) {
    $("#tbdyAnciSSRPax").find("tr").filter(function () {
        if (ssrCode == null) {
            ssrCode = '*';
        }
        var regex = new RegExp('trPaxSSRInfo_' + paxId + '_' + ssrCode);
        return regex.test(this.id);
    }).remove();
}
/*
 * Print SSR details per pax
 */
UI_tabAnci.buildPaxSSRDetail = function (ssrs, paxId) {
    UI_tabAnci.clearPaxSSRDetail(paxId);

    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";

    for (var i = ssrs.length - 1; i >= 0; i--) {
        strClass = " whiteBackGround";
        strTxtStyle = "";
        if (paxId % 2 != 0) {
            strClass = " alternateRow";
        }

        tblRow = document.createElement("TR");
        tblRow.id = "trPaxSSRInfo_" + paxId + '_' + ssrs[i].ssrCode;
        tblRow.appendChild(UI_commonSystem.createCell({
            desc: '&nbsp;&nbsp;&nbsp;&nbsp;' + ssrs[i].ssrName,
            css: UI_commonSystem.strDefClass + strClass + " rowHeight ",
            align: "left"
        }));

        var text = (ssrs[i].text == null || ssrs[i].text.length == 0) ? '&nbsp;' : ssrs[i].text;

        tblRow.appendChild(UI_commonSystem.createCell({
            desc: text,
            css: UI_commonSystem.strDefClass + strClass,
            align: "center"
        }));

        tblRow.appendChild(UI_commonSystem.createCell({
            desc: $.airutil.format.currency(ssrs[i].charge),
            css: UI_commonSystem.strDefClass + strClass,
            align: "center"
        }));

        tblRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxSingleSSRRemove(" + paxId + ",\"" + ssrs[i].ssrCode + "\"); return false;'>" + "<img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove SSR' >",
                    align: "center"
                }));

        $('#tdPaxSSR_0_' + paxId).parent().after(tblRow);
    }

}

/*
 * Passenger Selection
 */
UI_tabAnci.tdSSRPaxOnClick = function () {
    // 0 - tdPaxSSR;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";
    var firstTime = false;

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxSSR_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxSSR_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    } else {
        firstTime = true;
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxSSR_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxSSR_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");

    if (!(firstTime && jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].ssr.ssrs.length == 0)) {
        UI_tabAnci.refreshSSR();
    }
}

/*
 * Check SSR availability for all
 */
UI_tabAnci.ssrNotAvailableForAll = function () {
    var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
    for (var i = 0; i < jsSSRList.length; i++) {
        var smSSR = jsSSRModel[jsSSRList[i].smSGI].specialServiceRequest[jsSSRList[i].smI];
        if (jsSSRList[i].status != 'Y' || smSSR.availableQty == -1)
            continue;
        if ((smSSR.availableQty) < parseInt(paxList.length, 10)) {
            return jsSSRList[i];
        }
    }
    return null;
}

/*
 * Check SSR availability per pax
 */
UI_tabAnci.ssrNotAvailable = function (paxId) {
    var pax = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId];
    for (var i = 0; i < jsSSRList.length; i++) {
        if (jsSSRList[i].status != 'Y' || jsSSRList[i].availableQty == -1)
            continue;

        var availCount = jsSSRList[i].availableQty;
        var paxSSR = UI_tabAnci.getPaxSSRsPerCode(pax.ssr.ssrs,
            jsSSRList[i].ssrCode);
        if (paxSSR != null) {
            availCount += 1;
        }
        if (availCount < 1) {
            return jsSSRList[i];
        }
    }
    return null;
}

/*
 * Get SSR object from the pax ssr list based on ssr code
 */
UI_tabAnci.getPaxSSRsPerCode = function (ssrs, ssrCode) {
    for (var i = 0; i < ssrs.length; i++) {
        if (ssrs[i].ssrCode == ssrCode) {
            return ssrs[i];
        }
    }
    return null;
}

// ------------------------------------------------------------ Meal Request
// -------------------------------------------------
/*
 * Grid Constructor
 */
UI_tabAnci.constructMealGrid = function (inParams) {
    var gridHeadings = [];
    var gridColumns = [];
    
	var chargeFormatter = function(arr, options, rowObject) {		
		var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.MEALS,rowObject.mealCharge);
		if(applyFormatter){
			return UI_tabAnci.freeServiceDecorator;	
		}		
		return rowObject.mealCharge;
	}
    
    if (UI_tabAnci.focMealEnabled == false) {
        gridHeadings = [geti18nData('ancillary_Meal','Meal'), geti18nData('ancillary_Charge','Charge'),geti18nData('ancillary_Available','Available')];
        gridColumns = [{
            name: 'mealName',
            index: 'mealName',
            width: 250,
            align: "left"
        }, {
            name: 'mealCharge',
            index: 'mealCharge',
            width: 50,
            align: "center",
            formatter : chargeFormatter
        }, {
            name: 'avMealDsp',
            index: 'avMealDsp',
            width: 50,
            align: "center",
            hidden: true
        }];
        if (inParams.isMultiMealEnabled) {
            gridHeadings = [geti18nData('ancillary_Meal','Meal'),geti18nData('ancillary_Category','Category'), geti18nData('ancillary_Charge','Charge'), geti18nData('ancillary_Available','Available'), geti18nData('ancillary_Qty','Qty'),
                ''
            ];
            gridColumns = [{
                name: 'mealName',
                index: 'mealName',
                width: 250,
                align: "left"
            }, {
                name: 'mealCategoryCode',
                index: 'mealCategoryCode',
                width: 90,
                align: "center"
            }, {
                name: 'mealCharge',
                index: 'mealCharge',
                width: 50,
                align: "center",
                formatter : chargeFormatter
            }, {
                name: 'avMealDsp',
                index: 'avMealDsp',
                width: 50,
                align: "center"
            }, {
                name: 'mealQty',
                index: 'mealQty',
                width: 40,
                align: "center",
                editable: true,
                editoptions: {
                    className: "aa-input",
                    maxlength: "2",
                    style: "width:40px;",
                    dataEvents: [{
                        type: 'keyup',
                        fn: UI_tabAnci.processMealInputEvent
                    }, {
                        type: 'blur',
                        fn: UI_tabAnci.processMealInputEvent
                    }, {
                        type: 'focus',
                        fn: function (e) {
                            e.target.select();
                        }

                    }]
                }
            }, {
                name: 'apply',
                index: 'apply',
                width: 50,
                align: "center"
            }];
        }
    } else {
        gridHeadings = [ geti18nData('ancillary_Meal','Meal'),geti18nData('ancillary_Available','Available') ];
        gridColumns = [{
            name: 'mealName',
            index: 'mealName',
            width: 250,
            align: "left"
        }, {
            name: 'avMealDsp',
            index: 'avMealDsp',
            width: 50,
            align: "center",
            hidden: true
        }];
        if (inParams.isMultiMealEnabled) {
            gridHeadings = [ geti18nData('ancillary_Meal','Meal'), geti18nData('ancillary_Category','Category'), geti18nData('ancillary_Available','Available'), geti18nData('ancillary_Qty','Qty'), ''];
            gridColumns = [{
                name: 'mealName',
                index: 'mealName',
                width: 250,
                align: "left"
            }, {
                name: 'mealCategoryCode',
                index: 'mealCategoryCode',
                width: 90,
                align: "center"
            }, {
                name: 'avMealDsp',
                index: 'avMealDsp',
                width: 50,
                align: "center"
            }, {
                name: 'mealQty',
                index: 'mealQty',
                width: 40,
                align: "center",
                editable: true,
                editoptions: {
                    className: "aa-input",
                    maxlength: "2",
                    style: "width:40px;",
                    dataEvents: [{
                        type: 'keyup',
                        fn: UI_tabAnci.processMealInputEvent
                    }, {
                        type: 'blur',
                        fn: UI_tabAnci.processMealInputEvent
                    }, {
                        type: 'focus',
                        fn: function (e) {
                            e.target.select();
                        }

                    }]
                }
            }, {
                name: 'apply',
                index: 'apply',
                width: 50,
                align: "center"
            }];
        }
    }

    $(inParams.id).jqGrid({
        datatype: "local",
        height: 200,
        width: 770,
        colNames: gridHeadings,
        colModel: gridColumns,
        imgpath: UI_commonSystem.strImagePath,
        multiselect: false,
        scrollrows: true,
        cellsubmit: 'clientArray',
        viewrecords: true,
        onSelectRow: function (id) {
            id = parseInt(id, 10);
            UI_tabAnci.btnApplyMeal({
                row: id - 1
            }, null);
//            if (!inParams.isMultiMealEnabled) {
//                UI_tabAnci.anciMealApplyOnClick();
//            }

            // $('#tblAnciMeal').setSelection(id+'',false);
        }

    });
}

// ------------------------------------------------------------ Baggage Request
// -------------------------------------------------
/*
 * Grid Constructor
 */
UI_tabAnci.constructBaggageGrid = function (inParams) {
    var gridHeadings = [];
    var gridColumns = [];
    
	var chargeFormatter = function(arr, options, rowObject) {		
		var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.BAGGAGES,rowObject.baggageCharge);
		if(applyFormatter){
			return UI_tabAnci.freeServiceDecorator;	
		}	
		return rowObject.baggageCharge;
	}
    
    gridHeadings = [geti18nData('ancillary_Baggage','Baggage'), geti18nData('ancillary_Charge','Charge')];
    gridColumns = [{
        name: 'baggageDescription',
        index: 'baggageName',
        width: 250,
        align: "left"
    }, {
        name: 'baggageCharge',
        index: 'baggageCharge',
        width: 50,
        align: "center",
        formatter : chargeFormatter
    }];

    $(inParams.id).jqGrid({
        datatype: "local",
        height: 200,
        width: 770,
        colNames: gridHeadings,
        colModel: gridColumns,
        imgpath: UI_commonSystem.strImagePath,
        multiselect: false,
        scrollrows: true,
        cellsubmit: 'clientArray',
        viewrecords: true,
        onSelectRow: function (id) {
            id = parseInt(id, 10);
            UI_tabAnci.btnApplyBaggage({
                row: id - 1
            }, null);
            // UI_tabAnci.anciBaggageApplyOnClick();

        }

    });
}

/*
 * Process meal input events and select the meal
 */
UI_tabAnci.processMealInputEvent = function (e) {
    var target = e.target;
    var valueStr = $.trim(target.value);
    if (!/^\d*$/.test(valueStr)) {
        valueStr = valueStr.replace(/[^\d]/g, "");
        target.value = valueStr;
    }
    var value = parseInt(valueStr, 10);
    var id = parseInt(target.id.split('_')[0], 10);
    if (value != null && value > 0) {
        // if meal category is restricted set it as just one meal
        if(value > 1 && jsMealList[id-1].categoryRestricted) {
            target.value = 1;
        }
        UI_tabAnci.changeMealInput(id - 1, 'SELECT');
    } else {
        UI_tabAnci.changeMealInput(id - 1, 'DESELECT');
    }
}

UI_tabAnci.processBaggageInputEvent = function (e) {
    var target = e.target;
    var valueStr = $.trim(target.value);
    if (!/^\d*(\.\d*)$/.test(valueStr)) {
        valueStr = valueStr.replace(/[^\d(\.\d)]/g, "");
        target.value = valueStr;
    }
}

/*
 * Change Baggage Input
 */
UI_tabAnci.changeBaggageInput = function (rowId, option) {
    if (option == 'SELECT') {
        $('#btnBaggageAdd_' + rowId).show();
        $('#btnBaggageAdd_' + rowId).val("X");
        jsBaggageList[rowId].status = "Y";
    } else if (option == 'DESELECT') {

        $('#btnBaggageAdd_' + rowId).val("Add");
        jsBaggageList[rowId].status = "N";
    }
}

/*
 * Change Meal Input
 */
UI_tabAnci.changeMealInput = function (rowId, option) {
    if (option == 'SELECT') {
        $('#btnMealAdd_' + rowId).show();
        $('#btnMealAdd_' + rowId).val("X");
        jsMealList[rowId].status = "Y";
    } else if (option == 'DESELECT') {
        if (UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo)) {
            $('#btnMealAdd_' + rowId).hide();
        }
        $('#btnMealAdd_' + rowId).val("Add");
        jsMealList[rowId].status = "N";
    }
}
/*
 * Apply Meal Request
 */
UI_tabAnci.btnApplyMeal = function (p, obj) {
    if (obj == undefined || obj == null) {
        obj = $.extend({
            value: ''
        }, obj);
    }
    var intRow = parseInt(p.row, 10);
    var val = 1;
    var isMultiMealEnabled = UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo);
    
    if (isMultiMealEnabled) {
        val = parseInt($("#" + (intRow + 1) + "_mealQty").val(), 10);
    }
    var error = false;

    var strStatus = "";
    if (isMultiMealEnabled) {
        if (obj.value == "Add") {
            if (val != null && val > 0) {
                obj.value = "X";
                strStatus = "Y";
            } else {
                showERRMessage('Invalid meal Quantity');
                error = true;
            }
        } else if (obj.value == "X") {
            obj.value = "Add";
            strStatus = "N";
            if (isMultiMealEnabled) {
                $('#btnMealAdd_' + intRow).hide();
            }
            $("#" + (intRow + 1) + "_mealQty").val(0);
            
        }else{
        	
        	obj.value = "Add";
            strStatus = "N";
            if (isMultiMealEnabled && val == 0) {
                $('#btnMealAdd_' + intRow).hide();
            }
        }
    }
    if (!error) {
    	   for (var i = 0; i < jsMealList.length; i++) {
               if (i == intRow ||(isMultiMealEnabled && jsMealList[i].status=='Y')) {
                   jsMealList[i].status = 'Y';
                   if(isMultiMealEnabled){
                   	UI_tabAnci.updateSameCategoryStatus(jsMealList[i]);
                   }
               }else {
                   jsMealList[i].status = 'N';
               }
           }
    }
}

UI_tabAnci.updateSameCategoryStatus = function (meal){
	if(meal.categoryRestricted){
		for (var i = 0; i < jsMealList.length; i++) {
			if(meal.mealCode != jsMealList[i].mealCode 
					&& meal.mealCategoryCode==jsMealList[i].mealCategoryCode){
				jsMealList[i].status = 'N'; 
			}
		}
	}
}

/*
 * Apply Baggage Request
 */
UI_tabAnci.btnApplyBaggage = function (p, obj) {
    var intRow = parseInt(p.row, 10);
    var val = 1;

    var error = false;

    var strStatus = "";

    if (!error) {

        for (var i = 0; i < jsBaggageList.length; i++) {
            if (i == intRow) {
                jsBaggageList[i].status = 'Y';
            } else {
                jsBaggageList[i].status = 'N';
                $('#btnBaggageAdd_' + i).val("Add");
            }
        }

    }
}

/*
 * Build Meal List
 */
UI_tabAnci.buildMeal = function (p) {
    var intSegRow = null;
    var mealRefMap = null;
    var populateMealMap = (UI_tabAnci.mealRefMap[p.fltRefNo] == null)
    if (p.fltRefNo != null) {
        for (var i = 0; i < jsMealModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsMealModel[i].flightSegmentTO.flightRefNumber, p.fltRefNo)) {
                intSegRow = i;
                if (populateMealMap) {
                    UI_tabAnci.mealRefMap[p.fltRefNo] = {
                        meals: {}
                    };
                    mealRefMap = UI_tabAnci.mealRefMap[p.fltRefNo].meals;
                }
                break;
            }
        }
    }
    if (intSegRow != null) {
        jsMealList.length = 0;
        for (var i = 0; i < jsMealModel[intSegRow].meals.length; i++) {
            var tm = jsMealModel[intSegRow].meals[i];
            jsMealList[i] = {
                mealCode: tm.mealCode,
                mealName: tm.mealName,
                mealCategoryCode: tm.mealCategoryCode,
                mealDescription: tm.mealDescription,
                mealCharge: $.airutil.format.currency(tm.mealCharge),
                availableMeals: parseInt(tm.availableMeals, 10),
                mealQty: 0,
                mmI: i,
                mmSGI: intSegRow,
                apply: UI_tabAnci.mealBtnStr(i),
                avMealDsp: '<span id="avMealDsp_' + i + '" >' + tm.availableMeals + '</span>',
                status: 'N',
                categoryRestricted : tm.categoryRestricted
            };
            if (populateMealMap) {
                mealRefMap[tm.mealCode] = {
                    mmI: i,
                    mmSGI: intSegRow,
                    mealName: tm.mealName,
                    mealCategoryCode: tm.mealCategoryCode,
                    mealCode: tm.mealCode,
                    mealCharge: tm.mealCharge,
                    mealDescription: tm.mealDescription,
                    availableMeals: parseInt(tm.availableMeals, 10),
                    categoryRestricted : tm.categoryRestricted
                };
            }
        }
        UI_commonSystem.fillGridData({
            id: "#tblAnciMeal",
            data: jsMealList
        });
        if (UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo) && jsMealList.length > 0) {
        	$("#1_mealQty").click();
            $("#1_mealQty").focus();
        }
        if (populateMealMap) {
            UI_tabAnci.mealRefMap[p.fltRefNo].meals = mealRefMap;
        }
    } else {
        jsMealList.length = 0;
        UI_commonSystem.fillGridData({
            id: "#tblAnciMeal",
            data: []
        });
        showERRMessage("Error occured while fetching meal data");
    }
}

/*
 * Build Baggage List
 */
UI_tabAnci.buildBaggage = function (p) {
    var intSegRow = null;
    var baggageRefMap = null;
    var populateBaggageMap = (UI_tabAnci.baggageRefMap[p.fltRefNo] == null)
    if (p.fltRefNo != null) {
        for (var i = 0; i < jsBaggageModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsBaggageModel[i].flightSegmentTO.flightRefNumber,
                p.fltRefNo)) {
                intSegRow = i;
                if (populateBaggageMap) {
                    UI_tabAnci.baggageRefMap[p.fltRefNo] = {
                        baggages: {}
                    };
                    baggageRefMap = UI_tabAnci.baggageRefMap[p.fltRefNo].baggages;
                }
                break;
            }
        }
    }
    if (intSegRow != null) {
        jsBaggageList.length = 0;
        var index = 0;
        for (var i = 0; i < jsBaggageModel[intSegRow].baggages.length; i++) {
            var tm = jsBaggageModel[intSegRow].baggages[i];
            if (!tm.inactiveSelectedBaggage) {
                jsBaggageList[index++] = {
                    baggageName: tm.baggageName,
                    ondBaggageChargeId: tm.ondBaggageChargeId,
                    baggageDescription: tm.baggageDescription,
                    baggageCharge: $.airutil.format.currency(tm.baggageCharge),
                    overriddenCharge: $.airutil.format
                        .currency(tm.baggageCharge),
                    baggageQty: 0,
                    mmI: i,
                    mmSGI: intSegRow,
                    apply: UI_tabAnci.baggageBtnStr(i),
                    status: 'N'
                };
            }
            if (populateBaggageMap) {
                baggageRefMap[tm.baggageName] = {
                    mmI: i,
                    mmSGI: intSegRow,
                    baggageName: tm.baggageName,
                    baggageCharge: tm.baggageCharge,
                    baggageDescription: tm.baggageDescription

                };
            }
        }           
        
        UI_commonSystem.fillGridData({
            id: "#tblAnciBaggage",
            data: jsBaggageList
        });
        if (jsBaggageList.length > 0) {
            $("#1_baggageQty").focus();
        }
        if (populateBaggageMap) {
            UI_tabAnci.baggageRefMap[p.fltRefNo].baggages = baggageRefMap;
        }
    } else {
        jsBaggageList.length = 0;
        UI_commonSystem.fillGridData({
            id: "#tblAnciBaggage",
            data: []
        });
        showERRMessage("Error occured while fetching baggage data");
    }
}

/*******************************************************************************
 * Airport Services Related functions
 ******************************************************************************/

/*
 * Airport Services On Click
 */
UI_tabAnci.onClickApService = function (conf) {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.APSERVICES;
    $("#spanFltSegTitle").text(UI_tabAnci.FltSegTitle.AIRPORT);
    var options = {
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (!UI_tabAnci.dataStatus.apservice) {

        var data = {};
        if (!UI_tabAnci.isAnciModify) {
        	if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        		data = UI_MinimumFare.createSearchParams('searchParams');
        	} else {
        		data = UI_tabSearchFlights.createSearchParams('searchParams');
        	}            
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote
                    .populateFlightSegmentList());
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
            if (DATA_ResPro["resPaxs"] != null && DATA_ResPro["resPaxs"] != "") {
                data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
            }
        } else {
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['modifyAncillary'] = true;
            data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
            data['jsonOnds'] = DATA_ResPro["jsonOnds"];
            data['pnr'] = DATA_ResPro["pnrNo"];
        }

        if (UI_tabAnci.isSegModify) {
            data['modifySegment'] = true;
            data['modifyingSegments'] = DATA_ResPro.modifySegments.modifyingSegments;
            data['resSegments'] = $.toJSON(DATA_ResPro["resSegments"]);
        }

        if (UI_tabAnci.isAddSegment) {
            data['resSegments'] = $.toJSON(DATA_ResPro["resSegments"]);
        }

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "anciAirportService.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.returnCallApservice(response,
                        options.initLayout);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        if (options.initLayout) {
            UI_tabAnci.showHideSegments({
                show: true
            });
            UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.APSERVICES);

            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.APSERVICES);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
        UI_commonSystem.hideProgress();
    }
};

/*
 * Airport Service data return call
 */
UI_tabAnci.returnCallApservice = function (response, options) {
    UI_commonSystem.hideProgress();
    jsAPServicesModel = response.airportServicesList;
    if (jsAPServicesModel.length == 0) {
        showERRMessage("Error occured while fetching Airport service data");
    } else {
        if (options) {
            UI_tabAnci.showHideSegments({
                show: true
            });
            UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.APSERVICES);
        }

        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.reprotectAPservices();
        }
        UI_tabAnci.dataStatus.apservice = true;
        UI_tabAnci.currFltRefNo = null;
        if (options) {
            var segObj = UI_tabAnci
                .getSegForActiveAnci(UI_tabAnci.anciType.APSERVICES);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
    }
};

UI_tabAnci.reprotectAPservices = function () {

    if (!UI_tabAnci.reprotect.apService) {
        UI_tabAnci.reprotect.apService = true;
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            var dupliHash = {};
            var modifyingSegNum = 0;
            for (var k = 0; k < currAnci.length; k++) {

                var currFltSeg = currAnci[k].flightSegmentTO;

                if (currAnci[k].airportServiceDTOs != undefined) {
                    for (var j = 0; j < currAnci[k].airportServiceDTOs.length; j++) {

                        var currApServiceObj = currAnci[k].airportServiceDTOs[j];

                        if (dupliHash[currFltSeg.flightRefNumber + currApServiceObj.ssrCode + currApServiceObj.airportCode] || !UI_tabAnci
                            .isOneOfModifyingSeg(currFltSeg.flightRefNumber)) {
                            continue;
                        } else {
                            modifyingSegNum = UI_tabAnci
                                .getModifyingSegIndex(currFltSeg.flightRefNumber);
                        }

                        dupliHash[currFltSeg.flightRefNumber + currApServiceObj.ssrCode + currApServiceObj.airportCode] = true;

                        var resultJson = _getCorrespondingAPServiceObj(
                            modifyingSegNum, currApServiceObj.airportCode,
                            currFltSeg);

                        var applicableAPServiceObj = resultJson.applicableAirportObj;

                        if (applicableAPServiceObj != null) {
                            var apServiceMapObj = UI_tabAnci
                                .getAPServiceRefObj(
                                    currApServiceObj.ssrCode,
                                    applicableAPServiceObj.flightSegmentTO.flightRefNumber,
                                    applicableAPServiceObj.flightSegmentTO.airportCode);

                            if (apServiceMapObj != null) {
                                UI_tabAnci.currSegRow = resultJson.apServiceIndex;
                                UI_tabAnci.paxAPServiceAdd(i, apServiceMapObj,
                                    apServiceMapObj.mmI);
                            }
                        }

                    }
                }
            }
        }
    }

}

_getApplicableSegmentSeq = function () {
    var segSeq = 0;

    if (DATA_ResPro.resSegments != null) {
        for (var i = 0; i < DATA_ResPro.resSegments.length; i++) {
            if (DATA_ResPro.resSegments[i].status == "CNX") {
                segSeq++;
            }
        }
    }

    return segSeq;
}

_getCorrespondingAPServiceObj = function (segmentIndx, airport, flightSegmentTO) {

    var applicableAirportObj = null;
    var apServiceIndex = 0;

    var airportIndex = 0;
    var segmentArr = flightSegmentTO.segmentCode.split("/");
    for (var i = 0; i < segmentArr.length; i++) {
        if (airport == segmentArr[i]) {
            airportIndex = i;
            break;
        }
    }

    for (var i = 0; i < jsAPServicesModel.length; i++) {
        if (UI_tabAnci.getJsAnciSegIndex(i) != null) {
            var fltSeg = jsAPServicesModel[i].flightSegmentTO.segmentCode;
            var segAirportArr = fltSeg.split("/");

            if (segAirportArr.length > airportIndex) {
                var appAirport = segAirportArr[airportIndex];
                if (appAirport == jsAPServicesModel[i].flightSegmentTO.airportCode) {
                    applicableAirportObj = jsAPServicesModel[i];
                    apServiceIndex = i;
                    break;
                }
            }

        }
    }

    return {
        applicableAirportObj: applicableAirportObj,
        apServiceIndex: apServiceIndex
    };
}

/*
 * Airport Service Grid Constructor
 */
UI_tabAnci.constructAPServiceGrid = function (inParams) {
    var gridHeadings = [];
    var gridColumns = [];

	var chargeFormatter = function(arr, options, rowObject) {
		var serviceCharge = 0;
		if(rowObject.applicability == UI_tabAnci.ChargeType.PER_PAX){
			serviceCharge = parseFloat(rowObject.apServiceChargeAdult);
		} else {
			serviceCharge = parseFloat(rowObject.apServiceChargeRes);
		}
		var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.AIRPORT_SERVICE,serviceCharge);
		if(applyFormatter){
			return UI_tabAnci.freeServiceDecorator;	
		}	
		return rowObject.apServiceCharge;
	}
    
    gridHeadings = [geti18nData('ancillary_AirportService','Airport Service'),geti18nData('ancillary_Description','Description') , geti18nData('ancillary_Charge','Charge')];
    gridColumns = [{
        name: 'apServicNameText',
        index: 'apServicNameText',
        width: 80,
        align: "letf"
    }, {
        name: 'apServiceDescription',
        width: 170,
        align: "left"
    }, {
        name: 'apServiceCharge',
        index: 'apServiceCharge',
        width: 100,
        align: "left",
        formatter : chargeFormatter
    }];
    $(inParams.id).jqGrid({
        datatype: "local",
        height: 200,
        width: 770,
        colNames: gridHeadings,
        colModel: gridColumns,
        imgpath: UI_commonSystem.strImagePath,
        multiselect: true,
        scrollrows: true,
        cellsubmit: 'clientArray',
        viewrecords: true,
        onSelectRow: function (id, status) {
            id = parseInt(id, 10);
            UI_tabAnci.enableDisableApplyBtn(id - 1, status);
        },
        onSelectAll: function (ids, status) {
            if (!status) {
                $("#btnAnciAPServiceApply").enable();
            } else {
                var disable = false;
                for (var i = 0; i < jsAPserviceList.length; i++) {
                    if (jsAPserviceList[i].applicability == "R" && jsonPaxAdults.length > 1) {
                        disable = true;
                        break;
                    }
                }

                if (disable) {
                    $("#btnAnciAPServiceApply").disable();
                } else {
                    $("#btnAnciAPServiceApply").enable();
                }
            }
        }
    });
}

/*
 * Enable/Disable apply button based on selected service (per reservation or per
 * pax service)
 */
UI_tabAnci.enableDisableApplyBtn = function (id, status) {
    UI_tabAnci.isAPServiceSelected();
    var index = id;
    var applicability = jsAPserviceList[index].applicability;

    if (applicability == "R") {
        if (status && jsonPaxAdults.length > 1) {
            $("#btnAnciAPServiceApply").disable();
        } else if (!status && UI_tabAnci.isPerResServiceAdded() && jsonPaxAdults.length > 1) {
            $("#btnAnciAPServiceApply").disable();
        } else {
            $("#btnAnciAPServiceApply").enable();
        }
    } else if (applicability == "P") {
        if (UI_tabAnci.isPerResServiceSelected() && jsonPaxAdults.length > 1) {
            $("#btnAnciAPServiceApply").disable();
        } else if (UI_tabAnci.isPerResServiceAdded() && jsonPaxAdults.length > 1) {
            $("#btnAnciAPServiceApply").disable();
        } else {
            $("#btnAnciAPServiceApply").enable();
        }
    }

}

/*
 * Check whether user selected per reservation service to add
 */
UI_tabAnci.isPerResServiceSelected = function () {
    for (var i = 0; i < jsAPserviceList.length; i++) {
        if (jsAPserviceList[i].applicability == UI_tabAnci.ChargeType.PER_RESERVATION && jsAPserviceList[i].status == "Y") {
            return true;
        }
    }

    return false;
}

/*
 * Check whether user already added atleast one per reservation airport service
 * to the reservation
 */
UI_tabAnci.isPerResServiceAdded = function () {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);
    var apsData = jsAnciSeg[anciIndex].pax[0].aps;
    var paxAPSs = apsData.apss;

    for (var i = 0; i < paxAPSs.length; i++) {
        if (paxAPSs[i].applicability == UI_tabAnci.ChargeType.PER_RESERVATION && paxAPSs[i].airport == jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode) {
            return true;
        }
    }

    return false;
}

/*
 * Build Airport services List
 */
UI_tabAnci.buildAPServices = function (p) {

    var intSegRow = null;
    if (p.fltRefNo != null) {
        for (var i = 0; i < jsAPServicesModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsAPServicesModel[i].flightSegmentTO.flightRefNumber,
                p.fltRefNo) && p.airportCode == jsAPServicesModel[i].flightSegmentTO.airportCode) {
                intSegRow = i;
                break;
            }
        }
    }

    if (intSegRow != null) {
        jsAPserviceList.length = 0;
        for (var i = 0; i < jsAPServicesModel[intSegRow].airportServices.length; i++) {
            var tm = jsAPServicesModel[intSegRow].airportServices[i];
            var fltSeg = jsAPServicesModel[intSegRow].flightSegmentTO;

            // airport service charge for display in grid
            var chargeDisp;
            if (tm.applicabilityType == UI_tabAnci.ChargeType.PER_PAX) {
                chargeDisp = "Adult: " + $.airutil.format.currency(tm.adultAmount) + "<br/>" + "Child: " + $.airutil.format.currency(tm.childAmount) + "<br/>" + "Infant: " + $.airutil.format.currency(tm.infantAmount);
            } else if (tm.applicabilityType == UI_tabAnci.ChargeType.PER_RESERVATION) {
                chargeDisp = $.airutil.format.currency(tm.reservationAmount) + " " + strPerReservation;
            }

            jsAPserviceList[i] = {
                apServicName: tm.ssrCode,
                apServicNameText: tm.ssrName,
                apServiceDescription: tm.ssrDescription,
                apServiceCharge: chargeDisp,
                apServiceChargeAdult: $.airutil.format
                    .currency(tm.adultAmount),
                apServiceChargeChild: $.airutil.format
                    .currency(tm.childAmount),
                apServiceChargeInfant: $.airutil.format
                    .currency(tm.infantAmount),
                apServiceChargeRes: $.airutil.format
                    .currency(tm.reservationAmount),
                applicability: tm.applicabilityType,
                status: tm.status,
                airport: fltSeg.airportCode,
                airportType: fltSeg.airportType,
                mmI: i,
                mmSGI: intSegRow,
                apply: UI_tabAnci.apServiceBtnStr(i)
            };
        }       
        
        UI_commonSystem.fillGridData({
            id: "#tblAnciAPService",
            data: jsAPserviceList
        });
        $("#tblAnciAPService").parent(".ui-jqgrid-bdiv").css({
            "overflow-y": "scroll",
            "overflow-x": "auto"
        });
        $('#tblAnciAPService').resetSelection();
        if (jsSSRList.length > 0)
            $("#1_apServiceQty").focus();
    }
}

/*
 * APservices pax
 */
UI_tabAnci.buildAPServicePax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciAPServicePax").find("tr").remove();

    UI_tabAnci.currPaxIDRow = null;
    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    if (intLen > 0) {
        do {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxAPService_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
                click: UI_tabAnci.tdAPServicePaxOnClick,
                align: "left"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxAPService_1_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxAPService_2_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));
           
            tblRow
                .appendChild(UI_commonSystem
                    .createCell({
                        desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxAPServiceRemove(" + i + ",true, true, \"" + jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode + "\"); return false;'><img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Airport Service(s)'>",
                        css: UI_commonSystem.strDefClass + strClass,
                        align: "center"
                    }))
            $("#tbdyAnciAPServicePax").append(tblRow);
            i++;
        } while (--intLen);
    }
}

/*
 * Airport servie apply btn
 */
UI_tabAnci.apServiceBtnStr = function (rowId) {
    return "<input type='button' id='btnAPAdd_" + rowId + "' name='btnAdd' value='Add' class='ui-state-default ui-corner-all' onclick='UI_tabAnci.btnApplyAPService({row:" + rowId + "}, this)'>";
}

/*
 * Apply Airport Service Request
 */
UI_tabAnci.btnApplyAPService = function (p, obj) {
    var strStatus = "";
    var intRow = p.row;
    if (obj.value == "Add") {
        obj.value = "X";
        strStatus = "Y";
    } else {
        obj.value = "Add";
        strStatus = "N";
        $("#" + (intRow + 1) + "_text").val('');
    }
    jsAPserviceList[intRow].status = strStatus;
}

/*
 * Airport Service Apply On Click
 */
UI_tabAnci.anciAPServiceApplyOnClick = function () {
    if (!UI_tabAnci.isAPServiceSelected()) {
        showERRMessage('Please select a Airport Service to assign');
        return;
    }
    if (UI_tabAnci.validateApply()) {
        UI_tabAnci
            .paxAPServiceRemove(
                UI_tabAnci.currPaxIDRow,
                false,
                false,
                jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
        for (var i = 0; i < jsAPserviceList.length; i++) {
            if (jsAPserviceList[i].status == 'Y') {
                UI_tabAnci.paxAPServiceAdd(UI_tabAnci.currPaxIDRow,
                    jsAPserviceList[i], i);
            }
        }
    }
}

/*
 * Airport Service ApplyToAll On Click
 */
UI_tabAnci.anciAPServiceApplyToAllOnClick = function () {
    UI_commonSystem.initializeMessage();
    if (!UI_tabAnci.isAPServiceSelected()) {
        showERRMessage('Please select a airport service to assign');
        return;
    }

    if (confirm('This will replace existing airport services if any!\nDo you want to continue?')) {
        var paxList = jsAnciSeg[UI_tabAnci
            .getJsAnciSegIndex(UI_tabAnci.currSegRow)].pax;
        for (var paxI = 0; paxI < paxList.length; paxI++) {
            UI_tabAnci
                .paxAPServiceRemove(
                    paxI,
                    false,
                    false,
                    jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
            var objList = [];
            for (var i = 0; i < jsAPserviceList.length; i++) {
                var text = $("#txt_" + (i + 1)).val();
                if(text == undefined) {
                	text = "";
                }
                jsAPserviceList[i].apServiceNumber = text;
                if (jsAPserviceList[i].status == 'Y') {
                    objList[objList.length] = {
                        ssr: jsAPserviceList[i],
                        text: text,
                        ssrIndex: i
                    };
                }

            }
            UI_tabAnci.paxAPServiceBulkAdd(paxI, objList)
        }
    }

}

/*
 * Clear pax Airport Services
 */
UI_tabAnci.paxAPServiceRemove = function (paxId, refresh, isRemoveBtnClicked,
    airportCode) {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);
    var apsPax = jsAnciSeg[anciIndex].pax[paxId].aps;

    var deductCharge = parseFloat("0");
    for (var i = 0; i < apsPax.apss.length; i++) {
        if (apsPax.apss[i].airport == airportCode) {
            deductCharge += parseFloat(apsPax.apss[i].charge);
        }
    }

    UI_tabAnci.updatepaxTotals(paxId, deductCharge, 'DEDUCT');
    var removedAPSs = jsCurrAnciSeg[anciIndex].pax[paxId].aps.apss;

    UI_tabAnci.clearPaxAPServiceDetail(paxId);
    $("#tdPaxAPService_1_" + paxId).html(" ");
    $("#tdPaxAPService_2_" + paxId).html($.airutil.format.currency(0));

    var toRemoveArr = [];
    var count = 0;
    var indx = 0;
    var ispoped = false;
    var itr = _getAirportServiceIterateCounter(apsPax.apss, airportCode);

    while (apsPax.apss.length > itr) {

        if (apsPax.apss[indx].airport == airportCode) {
            ispoped = true;
            var aps = apsPax.apss.splice(indx, 1);
            if (aps[0].old) {
                removedAPSs[removedAPSs.length] = aps[0];
            }

            if (aps[0].applicability == "R") {
                toRemoveArr[count++] = {
                    "ssrCode": aps[0].ssrCode,
                    "paxId": paxId
                };
            }

            UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APSERVICES,
                aps[0].charge, 'DEDUCT', paxId, aps[0].old);
            UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APSERVICES, 1,
                'DEDUCT', aps[0].old);
        } else {
            ispoped = false;
        }

        if (!ispoped) {
            indx++;
        }
    }
    if (refresh && paxId == UI_tabAnci.currPaxIDRow) {
        UI_tabAnci.refreshAPService();
    }

    apsPax.apsChargeTotal = 0;

    if (toRemoveArr.length > 0 && isRemoveBtnClicked) {
        UI_tabAnci.removePerResAPService(toRemoveArr);
    }

}

_getAirportServiceIterateCounter = function (apss, airport) {
    var itrCounter = 0;
    for (var i = 0; i < apss.length; i++) {
        if (apss[i].airport != airport) {
            itrCounter++;
        }
    }

    return itrCounter;
}

UI_tabAnci.removePerResAPService = function (ssrArr) {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);
    for (var i = 0; i < jsAnciSeg[anciIndex].pax.length; i++) {
        for (var j = 0; j < ssrArr.length; j++) {
            if (i == ssrArr[j].paxId) {
                continue;
            }

            var apsList = jsAnciSeg[anciIndex].pax[i].aps.apss;
            for (var k = 0; k < apsList.length; k++) {
                if (apsList[k].ssrCode == ssrArr[j].ssrCode) {
                    UI_tabAnci
                        .paxSingleAPServiceRemove(
                            i,
                            ssrArr[j].ssrCode,
                            jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
                }
            }
        }
    }
}

/*
 * Clear Pax Airport Service details
 */
UI_tabAnci.clearPaxAPServiceDetail = function (paxId, ssrCode) {
    $("#tbdyAnciAPServicePax").find("tr").filter(function () {
        if (ssrCode == null) {
            ssrCode = '*';
        }
        var regex = new RegExp('trPaxAPServiceInfo_' + paxId + '_' + ssrCode);
        return regex.test(this.id);
    }).remove();
}

/*
 * Print airport service details per pax
 */
UI_tabAnci.buildPaxAPServiceDetail = function (apss, paxId, airportCode) {
    UI_tabAnci.clearPaxAPServiceDetail(paxId);

    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var anciMap = {};
    var serviceNo = "";
    
    var airportServices = null;
    if(objPax[paxId].currentAncillaries != undefined && objPax[paxId].currentAncillaries.length > 0 ){
    	airportServices = objPax[paxId].currentAncillaries[0].airportServiceDTOs;
    }
    
    if(airportServices != null){
    	for(var i = 0;i < airportServices.length;i++){
    		anciMap[airportServices[i].ssrCode + "_" + paxId] = airportServices[i].serviceNumber;
    	}    	
    }

    for (var i = apss.length - 1; i >= 0; i--) {

        if (apss[i].airport == airportCode) {
        	if(apss[i].serviceNumber == undefined){
        		serviceNo = anciMap[apss[i].ssrCode + "_" + paxId];
        		if(serviceNo == undefined){
        			serviceNo = "";
        		}
        		apss[i].serviceNumber = serviceNo;        		
        	} else {
        		serviceNo = apss[i].serviceNumber;
        	}
        	        	
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (paxId % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.id = "trPaxAPServiceInfo_" + paxId + '_' + apss[i].ssrCode;
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: '&nbsp;&nbsp;&nbsp;&nbsp;' + apss[i].text,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight ",
                align: "left"
            }));
            
            
            var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.AIRPORT_SERVICE,apss[i].charge);
            var appServChargeFrmt = apss[i].charge;            
    		if(applyFormatter){
    			appServChargeFrmt = UI_tabAnci.freeServiceDecorator;    			
    		}            
            

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: appServChargeFrmt,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: appServChargeFrmt,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));

            if (apss[i].applicability == "P") {
                tblRow
                    .appendChild(UI_commonSystem
                        .createCell({
                            desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxSingleAPServiceRemove(" + paxId + ",\"" + apss[i].ssrCode + "\", \"" + jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode + "\"); return false;'>" + "<img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Airport Service' >",
                            align: "center"
                        }));
            } else {
                tblRow.appendChild(UI_commonSystem.createCell({
                    desc: " ",
                    align: "center"
                }));
            }

            $('#tdPaxAPService_0_' + paxId).parent().after(tblRow);
        }

    }

}

/*
 * In Airport Service selected?
 */
UI_tabAnci.isAPServiceSelected = function () {
    var retVal = false;
    var selArr = $('#tblAnciAPService').getGridParam('selarrrow');
    for (var i = 0; i < jsAPserviceList.length; i++) {
        jsAPserviceList[i].status = 'N';
    }
    for (var i = 0; i < selArr.length; i++) {
        jsAPserviceList[parseInt(selArr[i], 10) - 1].status = 'Y';
        retVal = true;
    }
    return retVal;
}

/*
 * Add pax airport service as a bulk
 */
UI_tabAnci.paxAPServiceBulkAdd = function (paxId, objList) {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);
    var apsData = jsAnciSeg[anciIndex].pax[paxId].aps;
    var paxAPSs = apsData.apss;
    var ssrId = null;
    for (var i = 0; i < objList.length; i++) {
        var ssr = objList[i].ssr;
        var text = objList[i].text;
        var ssrIndex = objList[i].ssrIndex;

        ssrId = paxAPSs.length;

        var perPaxCharge = UI_tabAnci.getApplicableAirportServiceCharge(ssr,
            jsAnciSeg[anciIndex].pax, paxId, false);

        paxAPSs[ssrId] = {
            ssrCode: ssr.apServicName,
            description: ssr.apServiceDescription,
            charge: perPaxCharge,
            text: ssr.apServicNameText,
            ssrIndex: ssrIndex,
            airport: ssr.airport,
            airportType: ssr.airportType,
            applicability: ssr.applicability,
            old: false
        };
        apsData.apsChargeTotal += parseFloat(perPaxCharge);
        UI_tabAnci.updatepaxTotals(paxId, ssr.apServiceCharge, 'ADD');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APSERVICES,
            perPaxCharge, 'ADD', paxId, false);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APSERVICES, 1, 'ADD',
            false);
    }
    UI_tabAnci
        .buildPaxAPServiceDetail(
            paxAPSs,
            paxId,
            jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
    
    var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.AIRPORT_SERVICE,apsData.apsChargeTotal);
    var appServTotalFrmt = $.airutil.format.currency(apsData.apsChargeTotal);            
	if(applyFormatter){
		appServTotalFrmt = UI_tabAnci.freeServiceDecorator;    			
	}  

    $("#tdPaxAPService_1_" + paxId).html(" ");
    $("#tdPaxAPService_2_" + paxId).html(appServTotalFrmt);
}

/*
 * Add pax airport service
 */
UI_tabAnci.paxAPServiceAdd = function (paxId, ssr, ssrIndex) {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);

    var apsData = jsAnciSeg[anciIndex].pax[paxId].aps;
    var paxAPSs = apsData.apss;
    var ssrId = null;

    ssrId = paxAPSs.length;

    var perPaxCharge = UI_tabAnci.getApplicableAirportServiceCharge(ssr,
        jsAnciSeg[anciIndex].pax, paxId, false);

    paxAPSs[ssrId] = {
        ssrCode: ssr.apServicName,
        description: ssr.apServiceDescription,
        charge: perPaxCharge,
        text: ssr.apServicNameText,
        ssrIndex: ssrIndex,
        airport: ssr.airport,
        airportType: ssr.airportType,
        applicability: ssr.applicability,
        old: false
    };
    apsData.apsChargeTotal += parseFloat(perPaxCharge);

    UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'ADD');
    UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APSERVICES, perPaxCharge,
        'ADD', paxId, false);
    UI_tabAnci
        .updateAnciCounts(UI_tabAnci.anciType.APSERVICES, 1, 'ADD', false);

    UI_tabAnci
        .buildPaxAPServiceDetail(
            paxAPSs,
            paxId,
            jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
    
    var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.AIRPORT_SERVICE,apsData.apsChargeTotal);
    var appServTotalFrmt = $.airutil.format.currency(apsData.apsChargeTotal);            
	if(applyFormatter){
		appServTotalFrmt = UI_tabAnci.freeServiceDecorator;    			
	}  

    $("#tdPaxAPService_1_" + paxId).html(" ");
    $("#tdPaxAPService_2_" + paxId).html(appServTotalFrmt);

}

UI_tabAnci.getJsAnciSegIndex = function (index) {
    var apsFltRefNo = jsAPServicesModel[index].flightSegmentTO.flightRefNumber;

    for (var i = 0; i < jsAnciSeg.length; i++) {
        if (jsAnciSeg[i].fltRefNo == apsFltRefNo) {
            return i;
        }
    }
}

/*
 * Get Total pax count without infants
 */
UI_tabAnci.getPaxCountExceptInfants = function (paxList) {
    var paxCount = 0;
    for (var i = 0; i < paxList.length; i++) {
        if (paxList[i].paxType != "IN") {
            paxCount++;
        }
    }
    return paxCount;
}

UI_tabAnci.populateModifyAPServiceData = function (p) {
    for (var j = 0; j < jsonPaxAdults.length; j++) {
        var anciSegs = jsonPaxAdults[j].currentAncillaries;
        for (var k = 0; k < anciSegs.length; k++) {
            var anciSeg = anciSegs[k];
            if (anciSeg.airportServiceDTOs != null && anciSeg.airportServiceDTOs.length != null && anciSeg.airportServiceDTOs.length > 0) {
                var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
                var apss = anciSeg.airportServiceDTOs;
                var currentSeg = UI_tabAnci.compareFlightRefNo(p.fltRefNo,
                    fltRefNo);
                var segIndex = jsAnciSegRefMap[fltRefNo].index;
                var ssrData = jsAnciSeg[segIndex].pax[j].aps;
                var apServiceAvailable = UI_tabAnci.isAnciAvailable(
                    jsonSegAnciAvail[segIndex].ancillaryStatusList,
                    UI_tabAnci.anciType.APSERVICES);
                if (apServiceAvailable) {
                    for (var i = 0; i < apss.length; i++) {
                        var aps = apss[i];
                        var ssrRefObj = UI_tabAnci.getAPServiceRefObj(
                            aps.ssrCode, fltRefNo, aps.airportCode);
                        
                        if (typeof ssrRefObj == "undefined" || ssrRefObj == null) {
                            continue;
                        }
                        
                        var ssrObj = {
                            ssrCode: aps.ssrCode,
                            description: aps.ssrDescription,
                            charge: aps.serviceCharge,
                            text: aps.ssrName,
                            ssrIndex: ssrRefObj.mmI,
                            airport: aps.airportCode,
                            airportType: ssrRefObj.airportType,
                            applicability: ssrRefObj.applicability,
                            old: true
                        };

                        if (!UI_tabAnci.modifyAPServiceLoaded) {
                            ssrData.apss[ssrData.apss.length] = ssrObj;

                            ssrData.apsChargeTotal += parseFloat(ssrObj.charge);
                            UI_tabAnci.updatepaxTotals(j, ssrObj.charge, 'ADD');
                            UI_tabAnci.updateAnciTotals(
                                UI_tabAnci.anciType.APSERVICES,
                                ssrObj.charge, 'ADD', j, ssrObj.old);
                        }
                    }
                    ssrData.apsChargeTotal = $.airutil.format
                        .currency(ssrData.apsChargeTotal);
                }
            }
        }
    }
    UI_tabAnci.modifyAPServiceLoaded = true;
}

/*
 * Get aiport service charge based on configured criteria
 */
UI_tabAnci.getApplicableAirportServiceCharge = function (ssr, paxList, paxId,
    isModifyAnci) {
    var perPaxCharge;
    if (ssr.applicability == UI_tabAnci.ChargeType.PER_PAX) {
        var paxType = '';

        if (isModifyAnci) {
            paxType = paxList[paxId].type;
        } else {
            paxType = paxList[paxId].paxType;
        }

        if (paxType == "PA") {
            perPaxCharge = $.airutil.format
                .currency(parseFloat(ssr.apServiceChargeAdult) + parseFloat(ssr.apServiceChargeInfant));
        } else if (paxType == "AD") {
            perPaxCharge = ssr.apServiceChargeAdult;
        } else if (paxType == "CH") {
            perPaxCharge = ssr.apServiceChargeChild;
        }
    } else if (ssr.applicability == UI_tabAnci.ChargeType.PER_RESERVATION) {
        perPaxCharge = UI_tabAnci._splitWithoutLoss(ssr.apServiceChargeRes,
            jsonPaxAdults.length)[paxId];
    }

    return $.airutil.format.currency(perPaxCharge);
}

/*
 * Pax Single Airport Service Remove
 */
UI_tabAnci.paxSingleAPServiceRemove = function (paxId, ssrCode, airportCode) {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);
    var apsPax = jsAnciSeg[anciIndex].pax[paxId].aps;
    var paxAPSs = apsPax.apss;
    var ssr = null;
    for (var i = 0; i < paxAPSs.length; i++) {
        if (paxAPSs[i].ssrCode == ssrCode && paxAPSs[i].airport == airportCode) {
            ssr = paxAPSs.splice(i, 1)[0];
            break;
        }
    }
    if (ssr != null) {
        if (ssr.old) {
            var removedAPSs = jsCurrAnciSeg[anciIndex].pax[paxId].aps.apss;
            removedAPSs[removedAPSs.length] = ssr;
        }
        apsPax.apsChargeTotal -= ssr.charge;

        UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'DEDUCT');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APSERVICES, ssr.charge,
            'DEDUCT', paxId, ssr.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APSERVICES, 1,
            'DEDUCT', ssr.old);
        UI_tabAnci.clearPaxAPServiceDetail(paxId, ssrCode);
        var paxTotal = _getPaxWiseSubTotal(apsPax, airportCode);
        $("#tdPaxAPService_1_" + paxId).html(" ");
        
        
        var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.AIRPORT_SERVICE,paxTotal);
        var appServTotalFrmt = $.airutil.format.currency(paxTotal);            
    	if(applyFormatter){
    		appServTotalFrmt = UI_tabAnci.freeServiceDecorator;    			
    	}         
        
        $("#tdPaxAPService_2_" + paxId).html(appServTotalFrmt);

        if (paxId == UI_tabAnci.currPaxIDRow) {
            UI_tabAnci.refreshAPService();
        }
    }
}

UI_tabAnci.getAPServiceRefObj = function (ssrCode, fltRefNo, airportCode) {
    if (UI_tabAnci.apServiceRefMap[fltRefNo + airportCode] == null) {
        UI_tabAnci.apServiceRefMap[fltRefNo + airportCode] = {
            apss: {}
        };
        for (var i = 0; i < jsAPServicesModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsAPServicesModel[i].flightSegmentTO.flightRefNumber,
                fltRefNo) && jsAPServicesModel[i].flightSegmentTO.airportCode == airportCode) {
                for (var j = 0; j < jsAPServicesModel[i].airportServices.length; j++) {
                    var ts = jsAPServicesModel[i].airportServices[j];

                    UI_tabAnci.apServiceRefMap[fltRefNo + airportCode].apss[ts.ssrCode] = {
                        apServicName: ts.ssrCode,
                        apServicNameText: ts.ssrName,
                        apServiceDescription: ts.ssrDescription,
                        apServiceChargeAdult: $.airutil.format
                            .currency(ts.adultAmount),
                        apServiceChargeChild: $.airutil.format
                            .currency(ts.childAmount),
                        apServiceChargeInfant: $.airutil.format
                            .currency(ts.infantAmount),
                        apServiceChargeRes: $.airutil.format
                            .currency(ts.reservationAmount),
                        applicability: ts.applicabilityType,
                        status: ts.status,
                        airport: jsAPServicesModel[i].flightSegmentTO.airportCode,
                        airportType: jsAPServicesModel[i].flightSegmentTO.airportType,
                        mmI: j,
                        mmSGI: i
                    };
                }
            }
        }
    }
    var apsRef = UI_tabAnci.apServiceRefMap[fltRefNo + airportCode].apss[ssrCode];

    return apsRef;
}

/*
 * Populate Airport Service Data
 */
UI_tabAnci.populateAPServiceData = function (p) {
    var apsData = null;
    var paxId = null;

    var anciIndex = UI_tabAnci.getJsAnciSegIndex(parseInt(p.segID, 10));
    var paxData = jsAnciSeg[anciIndex].pax;
    for (var i = 0; i < paxData.length; i++) {

        apsData = paxData[i].aps;

        UI_tabAnci.buildPaxAPServiceDetail(apsData.apss, i, p.airportCode);
        var paxTotal = _getPaxWiseSubTotal(apsData, p.airportCode);
        var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.AIRPORT_SERVICE,paxTotal);
        var appServTotalFrmt = $.airutil.format.currency(paxTotal);            
    	if(applyFormatter){
    		appServTotalFrmt = UI_tabAnci.freeServiceDecorator;    			
    	}  

        $("#tdPaxAPService_1_" + i).html(" ");
        $("#tdPaxAPService_2_" + i).html(appServTotalFrmt);
    }
}

_getPaxWiseSubTotal = function (apsData, airportCode) {
    var subTotal = parseFloat("0");

    for (var i = 0; i < apsData.apss.length; i++) {
        if (apsData.apss[i].airport == airportCode) {
            subTotal += parseFloat(apsData.apss[i].charge);
        }
    }

    return $.airutil.format.currency(subTotal);
}

/*
 * Passenger Selection
 */
UI_tabAnci.tdAPServicePaxOnClick = function () {
    // 0 - tdPaxAPService;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";
    var firstTime = false;

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxAPService_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxAPService_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    } else {
        firstTime = true;
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxAPService_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxAPService_0_" + UI_tabAnci.currPaxIDRow).addClass(
        "rowSelectedAnci");

    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);

    if (!(firstTime && jsAnciSeg[anciIndex].pax[UI_tabAnci.currPaxIDRow].aps.apss.length == 0)) {
        UI_tabAnci.refreshAPService();
    }
}

/*
 * Refresh Airport Service List
 */
UI_tabAnci.refreshAPService = function () {
    var anciIndex = UI_tabAnci.getJsAnciSegIndex(UI_tabAnci.currSegRow);
    var apsPax = jsAnciSeg[anciIndex].pax[UI_tabAnci.currPaxIDRow].aps;
    var paxAPSs = apsPax.apss;
    var ssr = null;
    for (var i = 0; i < jsAPserviceList.length; i++) {
        UI_tabAnci.changeAPServiceInput(i, 'DESELECT');
        $("#" + (i + 1) + "_text").val('');
    }
    $('#tblAnciAPService').resetSelection();
    /* set the status to only pax selected values */
    for (var i = 0; i < paxAPSs.length; i++) {
        if (paxAPSs[i].airport == jsAPServicesModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode) {
            UI_tabAnci.changeAPServiceInput(paxAPSs[i].ssrIndex, 'SELECT');
            UI_tabAnci.enableDisableApplyBtn(paxAPSs[i].ssrIndex, true);
        }
    }
}

/*
 * Change Airport Service Input
 */
UI_tabAnci.changeAPServiceInput = function (rowId, option) {
    if (option == 'SELECT') {
        jsAPserviceList[rowId].status = "Y";
        if (!UI_tabAnci._isAPServiceRowSelected(rowId + 1)) {
            $('#tblAnciAPService').setSelection((rowId + 1) + '', false);
        }
    } else if (option == 'DESELECT') {
        jsAPserviceList[rowId].status = "N";
        if (UI_tabAnci._isAPServiceRowSelected(rowId + 1)) {
            $('#tblAnciAPService').setSelection((rowId + 1) + '', false);
        }
    }
}

UI_tabAnci._isAPServiceRowSelected = function (id) {
    var selIds = $("#tblAnciAPService").getGridParam('selarrrow');
    for (var i = 0; i < selIds.length; i++) {
        if (selIds[i] == id) {
            return true;
        }
    }
    return false;
}

/*******************************************************************************
 * End of Airport Services Related functions
 ******************************************************************************/

/****************************************************************************
 * Start- Airport Transfer Related functions
 ****************************************************************************/
UI_tabAnci.onClickAirportTransfer = function (conf) {
    UI_tabAnci.selectedAnciTypeGlobal = UI_tabAnci.anciType.APTRANSFER;
    $("#spanFltSegTitle").text(UI_tabAnci.FltSegTitle.AIRPORT);
    var options = {
        initLayout: true
    };
    options = $.extend(options, conf);
    UI_commonSystem.showProgress();
    if (!UI_tabAnci.dataStatus.aptransfer) {

        var data = {};
        if (!UI_tabAnci.isAnciModify) {
        	if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        		data = UI_MinimumFare.createSearchParams('searchParams');
        	} else {
        		data = UI_tabSearchFlights.createSearchParams('searchParams');
        	}            
            if (UI_tabAnci.isRequote) {
                data['requote'] = true;
                data['selectedFlightList'] = $.toJSON(UI_requote.populateFlightSegmentList());
            } else {
                data['selectedFlightList'] = UI_tabAnci.getFlightRPHList();
            }
            if (DATA_ResPro["resPaxs"] != null && DATA_ResPro["resPaxs"] != "") {
                data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
            }
        } else {
            data['selectedFlightList'] = $.toJSON(UI_tabAnci.flightSegmentList);
            data['modifyAncillary'] = true;
            data['resPaxInfo'] = $.toJSON(DATA_ResPro["resPaxs"]);
        }

        if (UI_tabAnci.isSegModify) {
            data['modifySegment'] = true;
            data['modifyingSegments'] = DATA_ResPro.modifySegments.modifyingSegments;
            data['resSegments'] = $.toJSON(DATA_ResPro["resSegments"]);
        }

        if (UI_tabAnci.isAddSegment) {
            data['resSegments'] = $.toJSON(DATA_ResPro["resSegments"]);
        }

        UI_commonSystem.getDummyFrm().ajaxSubmit({
            dataType: 'json',
            processData: false,
            data: data,
            url: "anciAirportTransfer.action",
            success: function (response) {
                if (response == null || !response.success) {
                    UI_commonSystem.hideProgress();
                    showERRMessage(response.messageTxt);
                } else {
                    UI_tabAnci.returnCallAirportTransfer(response, options.initLayout);
                }
            },
            error: UI_commonSystem.setErrorStatus
        });
    } else {
        if (options.initLayout) {
            UI_tabAnci.showHideSegments({
                show: true
            });
            UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.APTRANSFER);

            var segObj = UI_tabAnci.getSegForActiveAnci(UI_tabAnci.anciType.APTRANSFER);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }
        UI_commonSystem.hideProgress();
    }


}

UI_tabAnci.returnCallAirportTransfer = function (response, options) {
    UI_commonSystem.hideProgress();
    jsAPTransferModel = response.airportTransferList;
    if (jsAPTransferModel.length == 0) {
        showERRMessage("Error occured while fetching Airport service data");
    } else {
        UI_tabAnci.allowEditAirportTransfers = response.allowModify;
        if (options) {
            UI_tabAnci.showHideSegments({
                show: true
            });
            UI_tabAnci.buildFlightSegments(UI_tabAnci.anciType.APTRANSFER);
        }

        if (UI_tabAnci.isSegModify || UI_tabAnci.isRequote) {
            UI_tabAnci.reprotectAPTransfer();
        }
        UI_tabAnci.dataStatus.aptransfer = true;
        UI_tabAnci.currFltRefNo = null;
        if (options) {
            var segObj = UI_tabAnci.getSegForActiveAnci(UI_tabAnci.anciType.APTRANSFER);
            UI_tabAnci.tdSegOnClickHighlight(segObj);
        }

    }
};

UI_tabAnci.reprotectAPTransfer = function () {
    //At the moment, client does not request, will provide on demand
}

//apply logic
UI_tabAnci.anciAirportTransferApplyOnClick = function () {
    if (!UI_tabAnci.isAPTransferSelected()) {
        showERRMessage('Please select an Airport Trasfer to assign');
        return;
    }
    if (UI_tabAnci.validateApply() && UI_tabAnci.validateTransferData()) {
        UI_tabAnci.paxAPTransferRemove(UI_tabAnci.currPaxIDRow, false, false, jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
        for (var i = 0; i < jsAPTransferList.length; i++) {
            if (jsAPTransferList[i].status == 'Y') {
                UI_tabAnci.paxAPTransferAdd(UI_tabAnci.currPaxIDRow, jsAPTransferList[i], i);
            }
        }
    }
}

UI_tabAnci.validateTransferData = function () {
    var valid = false;
    var selIds = $("#tblAnciAPTransfer").getGridParam('selarrrow');
    for (var i = 0; i < selIds.length; i++) {
        var id = selIds[i];
        if (!UI_tabAnci.validateTransferRow(id)) {
            return false;
        } else {
            valid = true;
        }

    }
    return valid;
}

UI_tabAnci.validateTransferRow = function (id) {
    if (getFieldByID(id + "_pdDate").value == "") {
        showERRMessage("Transfer date cannot be empty");
        getFieldByID(id + "_pdDate").focus();
        return false;
    }
    if (!dateChk(id + "_pdDate")) {
        showERRMessage("Invalid transfer request date");
        getFieldByID(id + "_pdDate").focus();
        return false;
    }

    if (getFieldByID(id + "_pdHour").value == "") {
        showERRMessage("Request hour(HH) cannot be empty");
        getFieldByID(id + "_pdHour").focus();
        return false;
    }

    if (getFieldByID(id + "_pdMin").value == "") {
        showERRMessage("Request Minute(MM) cannot be empty");
        getFieldByID(id + "_pdMin").focus();
        return false;
    }
    
    if (getFieldByID(id + "_pdType").value == "") {
        showERRMessage("Request Type cannot be empty");
        getFieldByID(id + "_pdType").focus();
        return false;
    }

    var cuttOffTime = "";
    var currRec = jsAPTransferModel[UI_tabAnci.currSegRow];
    var fltSeg = currRec.flightSegmentTO;
    if (fltSeg.airportType == UI_tabAnci.AirportType.ARRIVAL) {
        cuttOffTime = fltSeg.arrivalDateTime;
    } else if (fltSeg.airportType == UI_tabAnci.AirportType.DEPARTURE || fltSeg.airportType == UI_tabAnci.AirportType.TRANSIT) {
        cuttOffTime = fltSeg.departureDateTime;
    }

    //"2013-12-27T15:55:00"
    var dateTime = cuttOffTime.split("T");
    var dateStr = dateTime[0];
    var dateArr = dateStr.split("-");

    var timeStr = dateTime[1];
    var timeArr = timeStr.split(":");
    var flightDate = dateArr[0] + dateArr[1] + dateArr[2] + timeArr[0] + timeArr[1];
    var formatedCutOff = timeArr[0] + ":" + timeArr[1] + " " + dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];

    var requestDateArr = getFieldByID(id + "_pdDate").value.split("/");
    var requestDate = requestDateArr[2] + requestDateArr[1] + requestDateArr[0] + getFieldByID(id + "_pdHour").value + getFieldByID(id + "_pdMin").value;

    if (fltSeg.airportType == UI_tabAnci.AirportType.ARRIVAL) {
        if (parseInt(requestDate, 10) <= parseInt(flightDate, 10)) {
            showERRMessage("Request Date-time should be after flight arrival (" + formatedCutOff + ")");
            return false;
        }
    } else if (fltSeg.airportType == UI_tabAnci.AirportType.DEPARTURE || fltSeg.airportType == UI_tabAnci.AirportType.TRANSIT) {
        if (parseInt(requestDate, 10) >= parseInt(flightDate, 10)) {
            showERRMessage("Request Date-time should be before flight departure (" + formatedCutOff + ")");
            return false;
        }
    }

    if (getFieldByID(id + "_pdAddress").value == "") {
        showERRMessage("Address cannot be empty");
        getFieldByID(id + "_pdAddress").focus();
        return false;
    }

    if (getFieldByID(id + "_pdContactNo").value == "") {
        showERRMessage("Contact number cannot be empty");
        getFieldByID(id + "_pdContactNo").focus();
        return false;
    }

    return true;
}

UI_tabAnci.clearTranferInputs = function (id) {
    $("#" + id + "_pdDate").val("");
    $("#" + id + "_pdHour").val("");
    $("#" + id + "_pdMin").val("");
    $("#" + id + "_pdType").val("");
    $("#" + id + "_pdAddress").val("");
    $("#" + id + "_pdContactNo").val("");
};

UI_tabAnci.populateTransferRequest = function (id, info) {
	var transferDate = info.pdDate;
    if(transferDate.indexOf("-") != -1){
    	var dateNewFormat = transferDate.split("-");
    	if(dateNewFormat[0].length == 2){
    		transferDate = dateNewFormat[0] + "/" + dateNewFormat[1] + "/" + dateNewFormat[2];
    	} else {
    		transferDate = dateNewFormat[2] + "/" + dateNewFormat[1] + "/" + dateNewFormat[0];	
    	}    	
    }
    $("#" + id + "_pdDate").val(transferDate);
    $("#" + id + "_pdHour").val(info.pdHour);
    $("#" + id + "_pdMin").val(info.pdMin);
    $("#" + id + "_pdType").val(info.pdType);
    $("#" + id + "_pdAddress").val(info.pdAddress);
    $("#" + id + "_pdContactNo").val(info.pdContactNo);
}

//removes all assgined transfer requests for a pax in given aiport
UI_tabAnci.paxAPTransferRemove = function (paxId, refresh, isRemoveBtnClicked, airportCode) {
    if (isRemoveBtnClicked) {
        $("#tdPaxAPTransfer_0_" + paxId).click();

        if (UI_tabAnci.isAnciModify && UI_tabAnci.allowEditAirportTransfers == false) {
            if ($("[id^=tdPaxAPTransferInfo_" + paxId + "]").length > 0) {
                showERRMessage('To cancel the reserved airport transfer, please contact the service provider');
                return false;
            }
        }

        for (var y = 1; y <= jsAPTransferList.length; y++) {
            UI_tabAnci.clearTranferInputs(y);
        }
    }
    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);
    var aptPax = jsAnciSeg[anciIndex].pax[paxId].apt;

    var deductCharge = parseFloat("0");
    for (var i = 0; i < aptPax.apts.length; i++) {
        if (aptPax.apts[i].airport == airportCode) {
            deductCharge += parseFloat(aptPax.apts[i].charge);
        }
    }

    UI_tabAnci.updatepaxTotals(paxId, deductCharge, 'DEDUCT');
    var removedAPTs = jsCurrAnciSeg[anciIndex].pax[paxId].apt.apts;

    UI_tabAnci.clearPaxAPTransferDetail(paxId);
    $("#tdPaxAPTransfer_1_" + paxId).html($.airutil.format.currency(0));

    var toRemoveArr = [];
    var count = 0;
    var indx = 0;
    var ispoped = false;
    var itr = _getAirportTransferIterateCounter(aptPax.apts, airportCode);

    while (aptPax.apts.length > itr) {

        if (aptPax.apts[indx].airport == airportCode) {
            ispoped = true;
            var apt = aptPax.apts.splice(indx, 1);
            if (apt[0].old) {
                removedAPTs[removedAPTs.length] = apt[0];
            }

            if (apt[0].applicability == "R") {
                toRemoveArr[count++] = {
                    "ssrCode": apt[0].ssrCode,
                    "paxId": paxId
                };
            }

            UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APTRANSFER, apt[0].charge, 'DEDUCT', paxId, apt[0].old);
            UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APTRANSFER, 1, 'DEDUCT', apt[0].old);
        } else {
            ispoped = false;
        }

        if (!ispoped) {
            indx++;
        }
    }
    if (refresh && paxId == UI_tabAnci.currPaxIDRow) {
        UI_tabAnci.refreshAPTransfer();
    }

    aptPax.aptChargeTotal = 0;

    if (toRemoveArr.length > 0 && isRemoveBtnClicked) {
        UI_tabAnci.removePerResAPTransfer(toRemoveArr);
    }
}

UI_tabAnci.refreshAPTransfer = function () {
    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);
    var aptPax = jsAnciSeg[anciIndex].pax[UI_tabAnci.currPaxIDRow].apt;
    var paxAPTs = aptPax.apts;
    for (var i = 0; i < jsAPTransferList.length; i++) {
        UI_tabAnci.changeAPTransferInput(i, 'DESELECT');
    }
    $('#tblAnciAPTransfer').resetSelection();

    for (var i = 0; i < paxAPTs.length; i++) {
        if (paxAPTs[i].airport == jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode) {
            var serviceId = paxAPTs[i].ssrIndex;
            UI_tabAnci.changeAPTransferInput(serviceId, 'SELECT');
            UI_tabAnci.populateTransferRequest(serviceId + 1, paxAPTs[i].info);
        }
    }
}

UI_tabAnci.removePerResAPTransfer = function (ssrArr) {
    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);
    for (var i = 0; i < jsAnciSeg[anciIndex].pax.length; i++) {
        for (var j = 0; j < ssrArr.length; j++) {
            if (i == ssrArr[j].paxId) {
                continue;
            }

            var aptList = jsAnciSeg[anciIndex].pax[i].apt.apts;
            for (var k = 0; k < aptList.length; k++) {
                if (aptList[k].ssrCode == ssrArr[j].ssrCode) {
                    UI_tabAnci.paxSingleAPTransferRemove(false, i, ssrArr[j].ssrCode, jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
                }
            }
        }
    }
}

UI_tabAnci.paxSingleAPTransferRemove = function (isRemoveBtnClicked, paxId, ssrCode, airportCode) {

    if (isRemoveBtnClicked) {
        $("#tdPaxAPTransfer_0_" + paxId).click();
        if (UI_tabAnci.isAnciModify && UI_tabAnci.allowEditAirportTransfers == false) {
            showERRMessage('To cancel the reserved airport transfer, please contact the service provider');
            return false;
        }

        for (var y = 1; y <= jsAPTransferList.length; y++) {
            UI_tabAnci.clearTranferInputs(y);
        }
    }

    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);
    var aptPax = jsAnciSeg[anciIndex].pax[paxId].apt;
    var paxAPTs = aptPax.apts;
    var ssr = null;
    for (var i = 0; i < paxAPTs.length; i++) {
        if (paxAPTs[i].ssrCode == ssrCode && paxAPTs[i].airport == airportCode) {
            ssr = paxAPTs.splice(i, 1)[0];
            break;
        }
    }
    if (ssr != null) {
        if (ssr.old) {
            var removedAPTs = jsCurrAnciSeg[anciIndex].pax[paxId].apt.apts;
            removedAPTs[removedAPTs.length] = ssr;
        }
        aptPax.aptChargeTotal -= ssr.charge;

        UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'DEDUCT');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APTRANSFER, ssr.charge, 'DEDUCT', paxId, ssr.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APTRANSFER, 1, 'DEDUCT', ssr.old);
        UI_tabAnci.clearPaxAPTransferDetail(paxId, ssrCode);
        var paxTotal = _getPaxWiseTransferSubTotal(aptPax, airportCode);
        $("#tdPaxAPTransfer_1_" + paxId).html($.airutil.format.currency(paxTotal));

        if (paxId == UI_tabAnci.currPaxIDRow) {
            UI_tabAnci.refreshAPTransfer();
        }
    }
}

UI_tabAnci.getAPTransferRefObj = function (ssrCode, fltRefNo, airportCode, airportTransferId) {
    if (UI_tabAnci.apTransferRefMap[fltRefNo + airportCode] == undefined || UI_tabAnci.apTransferRefMap[fltRefNo + airportCode] == null || 
    		(UI_tabAnci.apTransferRefMap[fltRefNo + airportCode].apts[ssrCode] == undefined)) {
    	if (UI_tabAnci.apTransferRefMap[fltRefNo + airportCode] == undefined || UI_tabAnci.apTransferRefMap[fltRefNo + airportCode] == null) {
	        UI_tabAnci.apTransferRefMap[fltRefNo + airportCode] = {
	            apts: {}
	        };
    	}
        for (var i = 0; i < jsAPTransferModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(jsAPTransferModel[i].flightSegmentTO.flightRefNumber, fltRefNo) /*&&
                jsAPTransferModel[i].flightSegmentTO.airportCode == airportCode*/) {
                for (var j = 0; j < jsAPTransferModel[i].airportServices.length; j++) {
                    var ts = jsAPTransferModel[i].airportServices[j];
                    if (airportTransferId == ts.airportTransferId) {
	                    var chargeDisp = "Adult: " + $.airutil.format.currency(ts.adultAmount) + "<br/>" + "Child: " + $.airutil.format.currency(ts.childAmount) + "<br/>" + "Infant: " + $.airutil.format.currency(ts.infantAmount);
	                    var providerDetails = '';
	                    var pr = ts.provider;
	                    if (pr != null) {
	                        providerDetails = pr.name + "<br>" + pr.address + "," + pr.number;
	                    }
	
	                    UI_tabAnci.apTransferRefMap[fltRefNo + airportCode].apts["Airport Transfer"] = {
	                        apSsrCode: ts.ssrCode,
	                        ssrName: ts.ssrName,
	                        apTransferDescription: ts.ssrDescription,
	                        apTransferChargeAdult: $.airutil.format.currency(ts.adultAmount),
	                        apTransferChargeChild: $.airutil.format.currency(ts.childAmount),
	                        apTransferChargeRes: $.airutil.format.currency(ts.infantAmount),
	                        apChargeDesc: chargeDisp,
	                        applicability: ts.applicabilityType,
	                        providerDetails: providerDetails,
	                        status: ts.status,
	                        airport: jsAPTransferModel[i].flightSegmentTO.airportCode,
	                        airportType: jsAPTransferModel[i].flightSegmentTO.airportType,
	                        mmI: j,
	                        mmSGI: i
	                    };
                	}
                }
            }
        }
    }
    var aptRef = UI_tabAnci.apTransferRefMap[fltRefNo + airportCode].apts[ssrCode];

    return aptRef;
}

UI_tabAnci.changeAPTransferInput = function (rowId, option) {
    if (option == 'SELECT') {
        jsAPTransferList[rowId].status = "Y";
        if (!UI_tabAnci._isAPTransferRowSelected(rowId + 1)) {
            $('#tblAnciAPTransfer').setSelection((rowId + 1) + '', false);
        }
    } else if (option == 'DESELECT') {
        jsAPTransferList[rowId].status = "N";
        if (UI_tabAnci._isAPTransferRowSelected(rowId + 1)) {
            $('#tblAnciAPTransfer').setSelection((rowId + 1) + '', false);
        }
    }
}

UI_tabAnci._isAPTransferRowSelected = function (id) {
    var selIds = $("#tblAnciAPTransfer").getGridParam('selarrrow');
    for (var i = 0; i < selIds.length; i++) {
        if (selIds[i] == id) {
            return true;
        }
    }
    return false;
}


_getAirportTransferIterateCounter = function (apts, airport) {
    var itrCounter = 0;
    for (var i = 0; i < apts.length; i++) {
        if (apts[i].airport != airport) {
            itrCounter++;
        }
    }

    return itrCounter;
}

_getPaxWiseTransferSubTotal = function (aptData, airportCode) {
    var subTotal = parseFloat("0");

    for (var i = 0; i < aptData.apts.length; i++) {
        if (aptData.apts[i].airport == airportCode) {
            subTotal += parseFloat(aptData.apts[i].charge);
        }
    }

    return $.airutil.format.currency(subTotal);
}
//add logic
UI_tabAnci.paxAPTransferAdd = function (paxId, ssr, ssrIndex) {
    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);

    var aptData = jsAnciSeg[anciIndex].pax[paxId].apt;
    var paxAPTs = aptData.apts;
    var ssrId = null;

    ssrId = paxAPTs.length;

    var perPaxCharge = UI_tabAnci.getApplicableAirportTrasnferCharge(ssr, jsAnciSeg[anciIndex].pax, paxId, false);

    var id = ssrIndex + 1;
    var existingDate = getFieldByID(id + "_pdDate").value;
    if(existingDate.indexOf("/") != -1){
    	var dateNewFormat = existingDate.split("/");
    	existingDate = dateNewFormat[0] + "-" + dateNewFormat[1] + "-" + dateNewFormat[2];
    }
    var paxInfo = {
        pdDate: existingDate,
        pdHour: getFieldByID(id + "_pdHour").value,
        pdMin: getFieldByID(id + "_pdMin").value,
        pdType: getFieldByID(id + "_pdType").value,
        pdAddress: getFieldByID(id + "_pdAddress").value,
        pdContactNo: getFieldByID(id + "_pdContactNo").value
    };

    paxAPTs[ssrId] = {
        ssrCode: ssr.apSsrCode,
        description: ssr.apChargeDesc,
        charge: perPaxCharge,
        text: ssr.ssrName,
        ssrIndex: ssrIndex,
        airport: ssr.airport,
        airportType: ssr.airportType,
        applicability: ssr.applicability,
        providerId : ssr.providerId,
        old: false,
        info: paxInfo
    };
    aptData.aptChargeTotal += parseFloat(perPaxCharge);

    UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'ADD');
    UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APTRANSFER, perPaxCharge, 'ADD', paxId, false);
    UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APTRANSFER, 1, 'ADD', false);

    UI_tabAnci.buildPaxAPTransferDetail(paxAPTs, paxId, jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);

    $("#tdPaxAPTransfer_1_" + paxId).html($.airutil.format.currency(aptData.aptChargeTotal));

}

UI_tabAnci.getApplicableAirportTrasnferCharge = function (ssr, paxList, paxId, isModifyAnci) {
    var perPaxCharge;
    if (ssr.applicability == UI_tabAnci.ChargeType.PER_PAX) {
        var paxType = '';

        if (isModifyAnci) {
            paxType = paxList[paxId].type;
        } else {
            paxType = paxList[paxId].paxType;
        }

        if (paxType == "PA") {
            perPaxCharge = $.airutil.format.currency(parseFloat(ssr.apTransferChargeAdult) + parseFloat(ssr.apTransferChargeInfant));
        } else if (paxType == "AD") {
            perPaxCharge = ssr.apTransferChargeAdult;
        } else if (paxType == "CH") {
            perPaxCharge = ssr.apTransferChargeChild;
        }
    } else if (ssr.applicability == UI_tabAnci.ChargeType.PER_RESERVATION) {
        perPaxCharge = UI_tabAnci._splitWithoutLoss(ssr.apTransferChargeRes, jsonPaxAdults.length)[paxId];
    }

    return $.airutil.format.currency(perPaxCharge);
}

/**
 * pax wise break down: 5-clear the child elements of pax headers and rebuild child records
 */
UI_tabAnci.buildPaxAPTransferDetail = function (apts, paxId, airportCode) {

    UI_tabAnci.clearPaxAPTransferDetail(paxId);

    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";


    for (var i = apts.length - 1; i >= 0; i--) {

        if (apts[i].airport == airportCode) {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (paxId % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.id = "trPaxAPTransferInfo_" + paxId + '_' + apts[i].ssrCode;
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: '&nbsp;&nbsp;&nbsp;&nbsp;' + (apts[i].text != null ? apts[i].text : apts[i].ssrCode),
                css: UI_commonSystem.strDefClass + strClass + " rowHeight ",
                align: "left"
            }));
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: apts[i].charge,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));

            if (apts[i].applicability == "P") {
                tblRow.appendChild(UI_commonSystem.createCell({
                    id: "tdPaxAPTransferInfo_" + paxId + '_' + apts[i].text,
                    desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxSingleAPTransferRemove(true, " + paxId + ",\"" +
                        apts[i].ssrCode + "\", \"" + jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode + "\"); return false;'>" +
                        "<img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Airport Transfer' >",
                    align: "center"
                }));
            } else {
                tblRow.appendChild(UI_commonSystem.createCell({
                    desc: " ",
                    align: "center"
                }));
            }

            $('#tdPaxAPTransfer_0_' + paxId).parent().after(tblRow);
        }

    }

}

/**
 * pax wise breakdown : 4-remove the child AT entries of given parent header
 */
UI_tabAnci.clearPaxAPTransferDetail = function (paxId, ssrCode) {
    $("#tbdyAnciAPTransferPax").find("tr").filter(function () {
        if (ssrCode == null) {
            ssrCode = '*';
        }
        var regex = new RegExp('trPaxAPTransferInfo_' + paxId + '_' + ssrCode);
        return regex.test(this.id);
    }).remove();
}


UI_tabAnci.anciAirportTransferApplyToAllOnClick = function () {
    UI_commonSystem.initializeMessage();
    if (!UI_tabAnci.isAPTransferSelected()) {
        showERRMessage('Please select an Airport Trasfer to assign');
        return;
    }

    if (UI_tabAnci.validateTransferData()) {
        if (confirm('This will replace existing airport transfers if any!\nDo you want to continue?')) {
            var paxList = jsAnciSeg[UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow)].pax;
            for (var paxI = 0; paxI < paxList.length; paxI++) {
                UI_tabAnci.paxAPTransferRemove(paxI, false, false, jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);
                var objList = [];
                for (var i = 0; i < jsAPTransferList.length; i++) {
                    var text = $("#" + (i + 1) + "_text").val();
                    if (jsAPTransferList[i].status == 'Y') {
                        var id = i + 1;
                        var existingDate = getFieldByID(id + "_pdDate").value;
                        if(existingDate.indexOf("/") != -1){
                        	var dateNewFormat = existingDate.split("/");
                        	existingDate = dateNewFormat[0] + "-" + dateNewFormat[1] + "-" + dateNewFormat[2];
                        }
                        var paxInfo = {
                            pdDate: existingDate,
                            pdHour: getFieldByID(id + "_pdHour").value,
                            pdMin: getFieldByID(id + "_pdMin").value,
                            pdType: getFieldByID(id + "_pdType").value,
                            pdAddress: getFieldByID(id + "_pdAddress").value,
                            pdContactNo: getFieldByID(id + "_pdContactNo").value
                        };

                        objList[objList.length] = {
                            ssr: jsAPTransferList[i],
                            text: text,
                            ssrIndex: i,
                            info: paxInfo
                        }; //revise me
                    }
                }
                UI_tabAnci.paxAPTransferBulkAdd(paxI, objList)
            }
        }
    }
}

UI_tabAnci.paxAPTransferBulkAdd = function (paxId, objList) {
    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);
    var aptData = jsAnciSeg[anciIndex].pax[paxId].apt;
    var paxAPTs = aptData.apts;
    var ssrId = null;
    for (var i = 0; i < objList.length; i++) {
        var ssr = objList[i].ssr;
        //var text = objList[i].text;
        var ssrIndex = objList[i].ssrIndex;

        ssrId = paxAPTs.length;
        var perPaxCharge = UI_tabAnci.getApplicableAirportTrasnferCharge(ssr, jsAnciSeg[anciIndex].pax, paxId, false);

        paxAPTs[ssrId] = {
            ssrCode: ssr.apSsrCode,
            description: ssr.apChargeDesc,
            charge: perPaxCharge,
            text: ssr.ssrName,
            ssrIndex: ssrIndex,
            airport: ssr.airport,
            airportType: ssr.airportType,
            applicability: ssr.applicability,
            old: false,
            info: objList[i].info,
            providerId : ssr.providerId
        };
        aptData.aptChargeTotal += parseFloat(perPaxCharge);

        UI_tabAnci.updatepaxTotals(paxId, ssr.charge, 'ADD');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APTRANSFER, perPaxCharge, 'ADD', paxId, false);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.APTRANSFER, 1, 'ADD', false);
    }
    UI_tabAnci.buildPaxAPTransferDetail(paxAPTs, paxId, jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode);

    $("#tdPaxAPTransfer_1_" + paxId).html($.airutil.format.currency(aptData.aptChargeTotal));
}

UI_tabAnci.isAPTransferSelected = function () {
    var retVal = false;
    var selArr = $("#tblAnciAPTransfer").getGridParam('selarrrow');
    for (var i = 0; i < jsAPTransferList.length; i++) {
        jsAPTransferList[i].status = 'N';
    }
    for (var i = 0; i < selArr.length; i++) {
        jsAPTransferList[parseInt(selArr[i], 10) - 1].status = 'Y';
        retVal = true;
    }
    return retVal;
}

UI_tabAnci.constructAPTransferGrid = function (inParams) {
    var gridHeadings = [];
    var gridColumns = [];

    var cellNumeric = {
        className: "aa-input",
        maxlength: "10",
        style: "width:125px;",
        dataEvents: UI_tabAnci.transferEvents().numeric
    };
    var cellAddress = {
        className: "aa-input",
        maxlength: "80",
        style: "width:125px;",
        dataEvents: UI_tabAnci.transferEvents().alphaNumeric
    };
    var cellContact = {
        className: "aa-input",
        maxlength: "16",
        style: "width:125px;",
        dataEvents: UI_tabAnci.transferEvents().numericWithSigns
    };

    var hourSelect = {
        className: "aa-input",
        value: UI_tabAnci.getTransferHourRange(),
        style: "width:40px;font-size:11px"
    };
    var minSelect = {
        className: "aa-input",
        value: ":;00:00;15:15;30:30;45:45",
        style: "width:40px;font-size:11px"
    };
    var typeSelect = {
            className: "aa-input",
            value: ":;HOME:HOME;AIRPORT:AIRPORT",
            style: "width:60px;font-size:11px"
        };
    var m = UI_commonSystem.strTxtMandatory;

    gridHeadings = ['ID', 'Airport', 'Provider Details', 'Charge', 'HH' + m, 'MM' + m, 'Request Date' + m, 'Type' + m, 'Address' + m, 'Contact No' + m];
    gridColumns = [{
        name: 'providerId',
        index: 'providerId',
        width: 20,
        align: "center"
    }, {
        name: 'airportCode',
        index: 'airportCode',
        width: 70,
        align: "center"
    }, {
        name: 'providerDetails',
        index: 'providerDetails',
        width: 90,
        align: "center"
    }, {
        name: 'apChargeDesc',
        index: 'apChargeDesc',
        width: 100,
        align: "center"
    }, {
        name: 'pdHour',
        index: 'pdHour',
        width: 50,
        align: "center",
        editable: true,
        edittype: "select",
        editoptions: hourSelect
    }, {
        name: 'pdMin',
        index: 'pdMin',
        width: 50,
        align: "center",
        editable: true,
        edittype: "select",
        editoptions: minSelect
    }, {
        name: 'pdDate',
        index: 'pdDate',
        width: 100,
        align: "center",
        editable: true,
        editoptions: cellNumeric
    },  {
        name: 'pdType',
        index: 'pdType',
        width: 70,
        align: "center",
        editable: true,
        edittype: "select",
        editoptions: typeSelect
    },  {
        name: 'pdAddress',
        index: 'pdAddress',
        width: 125,
        align: "center",
        editable: true,
        editoptions: cellAddress
    }, {
        name: 'pdContactNo',
        index: 'pdContactNo',
        width: 125,
        align: "center",
        editable: true,
        editoptions: cellContact
    }];


    $(inParams.id).jqGrid({
        datatype: "local",
        height: 200,
        width: 770,
        colNames: gridHeadings,
        colModel: gridColumns,
        imgpath: UI_commonSystem.strImagePath,
        multiselect: true,
        scrollrows: true,
        cellsubmit: 'clientArray',
        viewrecords: true,
        onSelectRow: function (id, status) {},
        onSelectAll: function (ids, status) {}

    });
}

UI_tabAnci.transferEvents = function () {
    var event = {};
    event.numeric = [{
        type: 'keyup',
        fn: function (e) {
            var target = e.target;
            var value = $.trim(target.value);
            if (!/^[0-9// ]*$/.test(value)) {
                value = value.replace(/[^0-9// ]/g, "");
                target.value = value;
            }
        }
    }, {
        type: 'blur',
        fn: function (e) {
            var target = e.target;
            var pdDateId = $.trim(target.id);
            if ($.trim($("#" + pdDateId).val()) != '') {
                dateChk(pdDateId);
            } else {
                $("#" + pdDateId).val('');
            }
        }
    }];

    event.alphaNumeric = [{
        type: 'keyup',
        fn: function (e) {
            var target = e.target;
            var value = $.trim(target.value);
            if (!/^[a-zA-Z0-9,.// ]*$/.test(value)) {
                value = value.replace(/[^a-zA-Z0-9,.// ]/g, "");
                target.value = value;
            }
        }
    }];

    event.numericWithSigns = [{
        type: 'keyup',
        fn: function (e) {
            var target = e.target;
            var value = $.trim(target.value);
            if (!/^[0-9-+]*$/.test(value)) {
                value = value.replace(/[^0-9-+]/g, "");
                target.value = value;
            }
        }
    }];
    return event;
}

UI_tabAnci.getTransferHourRange = function () {
    var jsonHours = ":;";
    for (var h = 0; h < 24; h++) {
        var temp = h;
        if (h < 10) {
            temp = '0' + temp;
        }
        jsonHours = jsonHours + temp + ":" + temp;
        if (h < 23) {
            jsonHours = jsonHours + ";"
        }
    }
    return jsonHours;
};

/**
 * pax wise breakdown:1- skeleton
 */
UI_tabAnci.buildAPTransferPax = function (p) {
    $("#tbdyAnciAPTransferPax").find("tr").remove();

    UI_tabAnci.currPaxIDRow = null;
    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    if (intLen > 0) {
        do {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxAPTransfer_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
                click: UI_tabAnci.tdAPTransferPaxOnClick,
                width: "40%",
                align: "left"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxAPTransfer_1_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                width: "15%",
                align: "center"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxAPTransferRemove(" + i + ",true, true, \"" + jsAPTransferModel[UI_tabAnci.currSegRow].flightSegmentTO.airportCode + "\"); return false;'><img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Airport Transfer(s)'>",
                id: "tdPaxAPTransfer_2_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                width: "7%",
                align: "center"
            }))

            $("#tbdyAnciAPTransferPax").append(tblRow);
            i++;
        } while (--intLen);
    }
}

/**
 * pax wise breakdown: 3- pax td click
 */
UI_tabAnci.tdAPTransferPaxOnClick = function () {
    for (var y = 1; y <= jsAPTransferList.length; y++) {
        UI_tabAnci.clearTranferInputs(y);
    }
    // 0 - tdPaxAPService;
    // 1 - Column;
    // 2 - Row(pax id)
    var arrData = this.id.split("_");
    var sbgColor = "";
    var firstTime = false;

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxAPTransfer_0_" + UI_tabAnci.currPaxIDRow).removeClass("rowSelectedAnci");
        $("#tdPaxAPTransfer_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    } else {
        firstTime = true;
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxAPTransfer_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxAPTransfer_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");

    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(UI_tabAnci.currSegRow);
    if (!(firstTime && jsAnciSeg[anciIndex].pax[UI_tabAnci.currPaxIDRow].apt.apts.length == 0)) {
        UI_tabAnci.refreshAPTransfer();
    }
    UI_tabAnci.applyPaxLevelControls();
}


UI_tabAnci.buildAPTransfers = function (p) {

    var intSegRow = null;
    if (p.fltRefNo != null) {
        for (var i = 0; i < jsAPTransferModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(jsAPTransferModel[i].flightSegmentTO.flightRefNumber, p.fltRefNo) &&
                p.airportCode == jsAPTransferModel[i].flightSegmentTO.airportCode) {
                intSegRow = i;
                break;
            }
        }
    }

    if (intSegRow != null) {
        jsAPTransferList.length = 0;
        for (var i = 0; i < jsAPTransferModel[intSegRow].airportServices.length; i++) {
            var tm = jsAPTransferModel[intSegRow].airportServices[i];
            var fltSeg = jsAPTransferModel[intSegRow].flightSegmentTO;
            var chargeDisp = "Adult: " + $.airutil.format.currency(tm.adultAmount) + "<br/>" + "Child: " + $.airutil.format.currency(tm.childAmount) + "<br/>" + "Infant: " + $.airutil.format.currency(tm.infantAmount);
            var providerDetails = '';
            var pr = tm.provider;
            if (pr != null) {
                providerDetails = pr.name + "<br>" + pr.address + "," + pr.number;
            }

            jsAPTransferList[i] = {
                apSsrCode: tm.ssrCode,
                ssrName: tm.ssrName,
                airportCode : tm.airportCode,
                providerId: pr.airportTransferId,
                apTransferChargeAdult: $.airutil.format.currency(tm.adultAmount),
                apTransferChargeChild: $.airutil.format.currency(tm.childAmount),
                apTransferChargeInfant: $.airutil.format.currency(tm.infantAmount),
                apTransferChargeRes: $.airutil.format.currency(tm.reservationAmount),
                apChargeDesc: chargeDisp,
                applicability: tm.applicabilityType,
                providerDetails: providerDetails,
                status: tm.status,
                airport: fltSeg.airportCode,
                airportType: fltSeg.airportType,
                pdDate: "",
                pdHour: "",
                pdMin: "",
                pdType: "",
                pdAddress: "",
                pdContactNo: "",
                mmI: i,
                mmSGI: intSegRow
            };
        }

        UI_commonSystem.fillGridData({
            id: "#tblAnciAPTransfer",
            data: jsAPTransferList
        });
        $("#tblAnciAPTransfer").parent(".ui-jqgrid-bdiv").css({
            "overflow-y": "scroll",
            "overflow-x": "auto"
        });
        $('#tblAnciAPTransfer').resetSelection();
        UI_tabAnci.mapAirportTransferDatePicker();
    }
}

UI_tabAnci.populateModifyAPTransferData = function (p) {
    for (var j = 0; j < jsonPaxAdults.length; j++) {
        var anciSegs = jsonPaxAdults[j].currentAncillaries;
        for (var k = 0; k < anciSegs.length; k++) {
            var anciSeg = anciSegs[k];
            if (anciSeg.airportTransferDTOs != null && anciSeg.airportTransferDTOs.length != null && anciSeg.airportTransferDTOs.length > 0) {
                var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
                var apts = anciSeg.airportTransferDTOs;
                var currentSeg = UI_tabAnci.compareFlightRefNo(p.fltRefNo, fltRefNo);
                var segIndex = jsAnciSegRefMap[fltRefNo].index;
                var aptData = jsAnciSeg[segIndex].pax[j].apt;
                var apTransferAvailable = UI_tabAnci.isAnciAvailable(jsonSegAnciAvail[segIndex].ancillaryStatusList, UI_tabAnci.anciType.APTRANSFER);
                if (apTransferAvailable) {
                    for (var i = 0; i < apts.length; i++) {
                        var apt = apts[i];
                        var ssrRefObj = UI_tabAnci.getAPTransferRefObj(apt.ssrCode, fltRefNo, apt.airportCode, apt.airportTransferId);
                        if (typeof ssrRefObj == "undefined" || ssrRefObj == null) {
                            continue;
                        }

                        var rDate = "",
                            rTime = "",
                            rHour = "",
                            rMin = "";
                        var reqDate = apt.tranferDate;
                        if (reqDate != null && reqDate.length > 0) {
                            rDate = reqDate.split(" ")[0];
                            rTime = reqDate.split(" ")[1];
                            rHour = rTime.split(":")[0];
                            rMin = rTime.split(":")[1];
                        }

                        var paxInfo = {
                            pdDate: rDate,
                            pdHour: rHour,
                            pdMin: rMin,
                            pdType: apt.transferType,
                            pdAddress: apt.transferAddress,
                            pdContactNo: apt.transferContact
                            
                        };
                        var ssrObj = {
                            ssrCode: apt.ssrCode,
                            description: apt.ssrDescription,
                            charge: apt.serviceCharge,
                            text: apt.ssrName,
                            ssrIndex: ssrRefObj.mmI,
                            airport: apt.airportCode,
                            airportType: ssrRefObj.airportType,
                            applicability: ssrRefObj.applicability,
                            providerId: apt.airportTransferId,
                            info: paxInfo,
                            old: true
                        };

                        if (!UI_tabAnci.modifyAPTransferLoaded) {
                            aptData.apts[aptData.apts.length] = ssrObj;

                            aptData.aptChargeTotal += parseFloat(ssrObj.charge);
                            UI_tabAnci.updatepaxTotals(j, ssrObj.charge, 'ADD');
                            UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.APTRANSFER, ssrObj.charge, 'ADD', j, ssrObj.old);
                        }
                    }
                    aptData.aptChargeTotal = $.airutil.format.currency(aptData.aptChargeTotal);
                }
            }
        }
    }
    UI_tabAnci.modifyAPTransferLoaded = true;
}

/**
 * Pax wise breakdown: 2-pax wise total headers
 */
UI_tabAnci.populateAPTransferData = function (p) {
    var aptData = null;
    var paxId = null;
    var anciIndex = UI_tabAnci.getTransferAnciSegIndex(parseInt(p.segID, 10));
    var paxData = jsAnciSeg[anciIndex].pax;

    for (var i = 0; i < paxData.length; i++) {
        aptData = paxData[i].apt;
        UI_tabAnci.buildPaxAPTransferDetail(aptData.apts, i, p.airportCode);
        $("#tdPaxAPTransfer_1_" + i).html(_getPaxWiseTransferSubTotal(aptData, p.airportCode));
    }
}

//TODO: extend the support to client side add/edit even in anci modifications
UI_tabAnci.applyAirportLevelControls = function () {
    $("#btnAnciAPTransferApplyToAll").show();
    if (UI_tabAnci.isAnciModify && UI_tabAnci.allowEditAirportTransfers == false) {
        if ($("[id^=tdPaxAPTransferInfo_]").length > 0) {
            $("#btnAnciAPTransferApplyToAll").hide();
        }
    }
}

UI_tabAnci.applyPaxLevelControls = function () {
    $("#btnAnciAPTransferApply").show();
    if (UI_tabAnci.isAnciModify && UI_tabAnci.allowEditAirportTransfers == false) {
        if ($("[id^=tdPaxAPTransferInfo_" + UI_tabAnci.currPaxIDRow + "]").length > 0) {
            $("#btnAnciAPTransferApply").hide();
        }
    }
}

UI_tabAnci.mapAirportTransferDatePicker = function () {
    var intLen = jsAPTransferList.length;
    if (intLen > 0) {
        var i = 1;
        $("#tblAnciAPTransfer").find("tr").each(function () {
            $(this).find("input").each(function () {
                if (this.name == "pdDate") {
                    $("#" + this.id).datepicker({
                        dateFormat: UI_commonSystem.strDTFormat,
                        minDate: UI_tabAnci.allowedFlightDates().previousDate,
                        maxDate: UI_tabAnci.allowedFlightDates().nextDate,
                        defaultDate: UI_tabAnci.allowedFlightDates().flightDate,
                        changeMonth: 'true',
                        changeYear: 'true',
                        yearRange: '-1:+1',
                        showButtonPanel: 'true'
                    });
                    i++;
                }
            });
        });
    }

}


UI_tabAnci.allowedFlightDates = function () {
    var cuttOffTime = "";
    var currRec = jsAPTransferModel[UI_tabAnci.currSegRow];
    var fltSeg = currRec.flightSegmentTO;
    if (fltSeg.airportType == UI_tabAnci.AirportType.ARRIVAL) {
        cuttOffTime = fltSeg.arrivalDateTime;
    } else if (fltSeg.airportType == UI_tabAnci.AirportType.DEPARTURE || fltSeg.airportType == UI_tabAnci.AirportType.TRANSIT) {
        cuttOffTime = fltSeg.departureDateTime;
    }
    //"2013-12-27T15:55:00"
    var dateTime = cuttOffTime.split("T");
    var dateStr = dateTime[0];
    var dateArr = dateStr.split("-");
    var formatedStrDate = dateArr[0] + "/" + dateArr[1] + "/" + dateArr[2];
    var flightDate = dateArr[2] + "/" + dateArr[1] + "/" + dateArr[0];

    var d1 = new Date(formatedStrDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1/$2/$3'));
    var d2 = new Date(formatedStrDate.replace(/(\d{4})(\d{2})(\d{2})/, '$1/$2/$3'));

    var dateArr = {
        minDate: '',
        maxDate: '',
        defaultDate: ''
    }
    dateArr.previousDate = new Date(d1.setDate(d1.getDate() - 1)); //one-day before
    dateArr.nextDate = new Date(d2.setDate(d2.getDate() + 1)); //one-daye after
    dateArr.flightDate = flightDate;
    return dateArr;
}

UI_tabAnci.getTransferAnciSegIndex = function (index) {
    var transferFltNo = jsAPTransferModel[index].flightSegmentTO.flightRefNumber;

    for (var i = 0; i < jsAnciSeg.length; i++) {
        if (jsAnciSeg[i].fltRefNo == transferFltNo) {
            return i;
        }
    }
}

/****************************************************************************
 * End- Airport Transfer Related functions
 ****************************************************************************/


/*
 * Update meal available count
 */
UI_tabAnci.updateMealAvailCount = function (meal, op) {
    var mlMeal = jsMealList[meal.mealIndex];
    if (mlMeal == null)
        return;
    // var mmMeal = jsMealModel[mlMeal.mmSGI].meals[mlMeal.mmI];
    if (op == 'ADD') {
        mlMeal.availableMeals += meal.mealQty;
        // mmMeal.availableMeals += qty;
    } else if (op == 'DEDUCT') {
        mlMeal.availableMeals -= meal.mealQty;
        // mmMeal.availableMeals -= qty;
    }
    $("#avMealDsp_" + meal.mealIndex).html(mlMeal.availableMeals);

}

/*
 * Update baggage available count
 */
UI_tabAnci.updateBaggageAvailCount = function (baggage, op) {
    var bgBaggage = jsBaggageList[baggage.baggageIndex];
    if (bgBaggage == null)
        return;

    if (op == 'ADD') {
        bgBaggage.availableBaggages += baggage.baggageQty;

    } else if (op == 'DEDUCT') {
        bgBaggage.availableBaggages -= baggage.baggageQty;

    }

}

/*
 * Refresh Baggage List
 */
UI_tabAnci.refreshBaggage = function () {

    var baggagePax = jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].baggage;
    var paxBaggages = baggagePax.baggages;
    var ssr = null;
    for (var i = 0; i < jsBaggageList.length; i++) {
        UI_tabAnci.changeBaggageInput(i, 'DESELECT');
        $("#" + (i + 1) + "_baggageQty").val(' ');
    }
    $('#tblAnciBaggage').resetSelection();
    /* set the status to only pax selected values */
    for (var i = 0; i < paxBaggages.length; i++) {
        UI_tabAnci.changeBaggageInput(paxBaggages[i].baggageIndex, 'SELECT');
        var id = parseInt(paxBaggages[i].baggageIndex) + 1;
        $("#" + id + "_baggageQty").val(paxBaggages[i].baggageQty);
        $('#tblAnciBaggage').setSelection(id + '', false);
    }
}

/*
 * Refresh Meal List
 */
UI_tabAnci.refreshMeal = function () {

    var mealPax = jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].meal;
    var paxMeals = mealPax.meals;
    var ssr = null;
    for (var i = 0; i < jsMealList.length; i++) {
        UI_tabAnci.changeMealInput(i, 'DESELECT');
        $("#" + (i + 1) + "_mealQty").val(0);
    }
    $('#tblAnciMeal').resetSelection();
    /* set the status to only pax selected values */
    for (var i = 0; i < paxMeals.length; i++) {
        UI_tabAnci.changeMealInput(paxMeals[i].mealIndex, 'SELECT');
        var id = parseInt(paxMeals[i].mealIndex) + 1;
        $("#" + id + "_mealQty").val(paxMeals[i].mealQty);
        $('#tblAnciMeal').setSelection(id + '', false);
    }
}
/*
 * Create button add btn string for meal
 */
UI_tabAnci.mealBtnStr = function (rowId) {
    return "<input type='button' id='btnMealAdd_" + rowId + "' value='Add' class='ui-state-default ui-corner-all'  onclick='UI_tabAnci.btnApplyMeal({row:" + rowId + "}, this)'>";
}

/*
 * Create button add btn string for baggage
 */
UI_tabAnci.baggageBtnStr = function (rowId) {
    return "<input type='button' id='btnBaggageAdd_" + rowId + "' value='Add' class='ui-state-default ui-corner-all'  onclick='UI_tabAnci.btnApplyBaggage({row:" + rowId + "}, this)'>";
}

/*
 * Baggage pax
 */
UI_tabAnci.buildBaggagePax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciBaggagePax").find("tr").remove();

    UI_tabAnci.currPaxIDRow = null;
    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    if (intLen > 0) {
        do {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxBaggage_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
                click: UI_tabAnci.tdBaggagePaxOnClick,
                align: "left"
            }));

            // tblRow.appendChild(UI_commonSystem.createCell({desc:" ",
            // id:"tdPaxBaggage_1_" + i,
            // css:UI_commonSystem.strDefClass + strClass,
            // align:"center"}))

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: "&nbsp;",
                id: "tdPaxBaggage_2_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }))

            // tblRow.appendChild(UI_commonSystem.createCell({desc:" ",
            // id:"tdPaxBaggage_3_" + i,
            // css:UI_commonSystem.strDefClass + strClass,
            // align:"center"}))

            if (UI_tabAnci.baggageMandatory) {
                tblRow.appendChild(UI_commonSystem.createCell({
                    desc: "&nbsp;",
                    css: UI_commonSystem.strDefClass + strClass,
                    align: "center"
                }))
            } else {
                tblRow
                    .appendChild(UI_commonSystem
                        .createCell({
                            desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxBaggageRemoveWrapper(" + i + ",true,null); return false;'><img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Baggage(s)' /></a>",
                            css: UI_commonSystem.strDefClass + strClass,
                            align: "center"
                        }))
            }

            $("#tbdyAnciBaggagePax").append(tblRow);
            i++;
        } while (--intLen);
    }
}

/*
 * Meal pax
 */
UI_tabAnci.buildMealPax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciMealPax").find("tr").remove();

    UI_tabAnci.currPaxIDRow = null;
    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    if (intLen > 0) {
        do {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxMeal_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
                click: UI_tabAnci.tdMealPaxOnClick,
                align: "left"
            }));

            if (UI_tabAnci.focMealEnabled == false) {
                tblRow.appendChild(UI_commonSystem.createCell({
                    desc: " ",
                    id: "tdPaxMeal_1_" + i,
                    css: UI_commonSystem.strDefClass + strClass,
                    align: "center"
                }))
            }

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxMeal_2_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }))

            if (UI_tabAnci.focMealEnabled == false) {
                tblRow.appendChild(UI_commonSystem.createCell({
                    desc: " ",
                    id: "tdPaxMeal_3_" + i,
                    css: UI_commonSystem.strDefClass + strClass,
                    align: "center"
                }))
            }

            tblRow
                .appendChild(UI_commonSystem
                    .createCell({
                        desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxMealRemove(" + i + ",true); return false;'><img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Meal(s)' /></a>",
                        css: UI_commonSystem.strDefClass + strClass,
                        align: "center"
                    }))
            $("#tbdyAnciMealPax").append(tblRow);
            i++;
        } while (--intLen);
    }
}
/*
 * Clear pax meal details
 */
UI_tabAnci.clearPaxMealDetail = function (paxId, mealCode) {
    $("#tbdyAnciMealPax").find("tr").filter(function () {
        if (mealCode == null) {
            mealCode = '*';
        }
        var regex = new RegExp('trPaxMealInfo_' + paxId + '_' + mealCode);
        return regex.test(this.id);
    }).remove();
}

/*
 * Clear pax baggage details
 */
UI_tabAnci.clearPaxBaggageDetail = function (paxId, baggageName) {
    $("#tbdyAnciBaggagePax").find("tr").filter(
        function () {
            if (baggageName == null) {
                baggageName = '*';
            }
            var regex = new RegExp('trPaxBaggageInfo_' + paxId + '_' + baggageName);
            return regex.test(this.id);
        }).remove();
}

/*
 * Print Baggages details per pax
 */
UI_tabAnci.buildPaxBaggageDetail = function (baggages, paxId) {

    UI_tabAnci.clearPaxBaggageDetail(paxId);

    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";

    for (var i = 0; i < baggages.length; i++) {
        strClass = " whiteBackGround";
        strTxtStyle = "";
        if (paxId % 2 != 0) {
            strClass = " alternateRow";
        }

        tblRow = document.createElement("TR");
        tblRow.id = "trPaxBaggageInfo_" + paxId + '_' + baggages[i].baggageName;
        tblRow.appendChild(UI_commonSystem.createCell({
            desc: '&nbsp;&nbsp;&nbsp;&nbsp;' + baggages[i].baggageName,
            css: UI_commonSystem.strDefClass + strClass + " rowHeight ",
            align: "left"
        }));

        // tblRow.appendChild(UI_commonSystem.createCell({desc:baggages[i].baggageCharge,
        // css:UI_commonSystem.strDefClass + strClass,
        // align:"center"}));

        tblRow.appendChild(UI_commonSystem.createCell({
            desc: baggages[i].baggageQty,
            css: UI_commonSystem.strDefClass + strClass,
            align: "center"
        }));

        // tblRow.appendChild(UI_commonSystem.createCell({desc:baggages[i].baggageCharge,
        // css:UI_commonSystem.strDefClass + strClass,
        // align:"center"}));

        tblRow.appendChild(UI_commonSystem.createCell({
            desc: "&nbsp;",
            css: "thinBorderB thinBorderR whiteBackGround" + strClass,
            align: "center"
        }));

        $('#tdPaxBaggage_0_' + paxId).parent().after(tblRow);
    }

}
UI_tabAnci.buildPaxMealHeaders = function () {
    tblHRow = document.createElement("TR");
    if (UI_tabAnci.focMealEnabled == true) {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: strPassengerName,
                    css: 'thinBorderB thinBorderR gridBG  thinBorderT ui-corner-TL rowHeight txtBold',
                    align: 'center',
                    width: '65%'
                }));
    } else {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: strPassengerName,
                    css: 'thinBorderB thinBorderR gridBG  thinBorderT ui-corner-TL rowHeight txtBold',
                    align: 'center',
                    width: '43%'
                }));
    }
    if (UI_tabAnci.focMealEnabled == false) {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: strMealCharge,
                    css: 'paxMealCharge thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold',
                    align: 'center',
                    width: '20%',
                    id: 'lblPaxMealCharge'
                }));
    }
    if (UI_tabAnci.focMealEnabled == true) {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: strMealQty,
                    css: 'thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold',
                    align: 'center',
                    width: '23%'
                }));
    } else {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: strMealQty,
                    css: 'thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold',
                    align: 'center',
                    width: '15%'
                }));
    }
    if (UI_tabAnci.focMealEnabled == false) {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: 'Amt',
                    css: 'paxMealCharge thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold',
                    align: 'center',
                    width: '15%',
                    id: 'lblPaxTotalMealAmount'
                }));
    }
    if (UI_tabAnci.focMealEnabled == true) {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: '&nbsp;',
                    css: 'thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR rowHeight txtBold',
                    align: 'center',
                    width: '12%'
                }));
    } else {
        tblHRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: '&nbsp;',
                    css: 'thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR rowHeight txtBold',
                    align: 'center',
                    width: '7%'
                }));
    }
    if ($("#HeaderMeal").children().length > 1) {
        $("#HeaderMeal").find("tr:last").remove();
    }
    $("#HeaderMeal").append(tblHRow);

    tblHSupRow = document.createElement("TR");
    tblHSupRow.appendChild(UI_commonSystem.createCell({
        desc: '',
        css: '',
        textCss: 'height: 1px',
        width: '44%'
    }));
    if (UI_tabAnci.focMealEnabled == false) {
        tblHSupRow.appendChild(UI_commonSystem.createCell({
            desc: '',
            css: 'paxMealCharge',
            textCss: 'height: 1px',
            width: '21%'
        }));
    }
    tblHSupRow.appendChild(UI_commonSystem.createCell({
        desc: '',
        css: '',
        textCss: 'height: 1px',
        width: '15%'
    }));
    if (UI_tabAnci.focMealEnabled == false) {
        tblHSupRow.appendChild(UI_commonSystem.createCell({
            desc: '',
            css: 'paxMealCharge',
            textCss: 'height: 1px',
            width: '15%'
        }));
    }
    tblHSupRow.appendChild(UI_commonSystem.createCell({
        desc: '',
        css: '',
        textCss: 'height: 1px',
        width: '7%'
    }));
    $("#HeaderMealSup").empty();
    $("#HeaderMealSup").append(tblHSupRow);

}

/*
 * Print Meals details per pax
 */
UI_tabAnci.buildPaxMealDetail = function (meals, paxId,isAnciModiFlow) {
    UI_tabAnci.clearPaxMealDetail(paxId);

    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    
    UI_tabAnci.mealCategoryMap = {};
    for (var i = 0; i < meals.length; i++) {
        strClass = " whiteBackGround";
        strTxtStyle = "";
        if (paxId % 2 != 0) {
            strClass = " alternateRow";
        }

        tblRow = document.createElement("TR");
        tblRow.id = "trPaxMealInfo_" + paxId + '_' + meals[i].mealCode;
        tblRow.appendChild(UI_commonSystem.createCell({
            desc: '&nbsp;&nbsp;&nbsp;&nbsp;' + meals[i].mealName,
            css: UI_commonSystem.strDefClass + strClass + " rowHeight ",
            align: "left"
        }));
        
        var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.MEALS,meals[i].mealCharge);
        var mealChargeFrmt = meals[i].mealCharge;
        var mealSubTotalFrmt =$.airutil.format.currency(meals[i].subTotal);
		if(applyFormatter){
			mealChargeFrmt = UI_tabAnci.freeServiceDecorator;	
			mealSubTotalFrmt= UI_tabAnci.freeServiceDecorator;
		}
        

        if (UI_tabAnci.focMealEnabled == false) {
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: mealChargeFrmt,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));
        }

        tblRow.appendChild(UI_commonSystem.createCell({
            desc: meals[i].mealQty,
            css: UI_commonSystem.strDefClass + strClass,
            align: "center"
        }));

        if (UI_tabAnci.focMealEnabled == false) {
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: mealSubTotalFrmt,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }));
        }

        tblRow
            .appendChild(UI_commonSystem
                .createCell({
                    desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.paxSingleMealRemove(" + paxId + ",\"" + meals[i].mealCode + "\"); return false;'>" + "<img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove Meal(s)' >",
                    align: "center"
                }));

        $('#tdPaxMeal_0_' + paxId).parent().after(tblRow);
        
        if(isAnciModiFlow){
	        var paxMealCategory = {
	        	mealCategoryCode : meals[i].mealCategoryCode,
	        	flightRefNo : UI_tabAnci.currFltRefNo,
	        	categoryRestricted : meals[i].categoryRestricted
	        };
	        
	        UI_tabAnci.paxMealCategoryCountUpdate(paxMealCategory);
    	}
    }

}

/*
 * Baggage Apply On Click
 */
UI_tabAnci.anciBaggageApplyOnClick = function (isOverrideClicked) {
    UI_commonSystem.initializeMessage();
    if (!UI_tabAnci.isBaggageSelected()) {
        showERRMessage('Please select a baggage to assign');
        return




    }

    if (UI_tabAnci.validateApply()) {

        UI_tabAnci.paxBaggageRemove(UI_tabAnci.currPaxIDRow, false, null);
        for (var i = 0; i < jsBaggageList.length; i++) {
            var qty = 1;
            if (qty > 0 & jsBaggageList[i].status == 'Y') {
                var charge = jsBaggageList[i].baggageCharge;
                if (UI_tabAnci.baggageChargeOverride && isOverrideClicked) {
                    charge = '0.00';
                }
                var baggageObj = {
                    baggageName: jsBaggageList[i].baggageName,
                    baggageQty: qty,
                    baggageCharge: charge,
                    subTotal: parseFloat(charge) * (qty),
                    baggageIndex: i,
                    ondBaggageChargeId: jsBaggageList[i].ondBaggageChargeId,
                    old: false
                };

                var correctBaggageSegId = UI_tabAnci
                    .getCorrectBaggageSegmentId(jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo);
                var grpId = jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId;
                var grpCount = "";
                if (grpId != null && grpId != "") {
                    grpCount = getBaggageONDGrpIdWiseCount(jsBaggageModel,
                        grpId);
                }

                if (grpCount != "") {
                    baggageObj.subTotal = parseFloat(baggageObj.subTotal) / grpCount;

                    var correctBagSegId = null;
                    for (var k = 0; k < jsAnciSeg.length; k++) {
                        if (getBaggageONDGrpId(jsAnciSeg[k].fltRefNo) == grpId) {
                            correctBagSegId = UI_tabAnci
                                .getCorrectBaggageSegmentId(jsAnciSeg[k].fltRefNo)
                            var correctSegId = UI_tabAnci
                                .getCorrectSegmentId(jsBaggageModel[correctBagSegId].flightSegmentTO.flightRefNumber);
                            UI_tabAnci.paxBaggageRemove(
                                UI_tabAnci.currPaxIDRow, false,
                                correctSegId);
                            var paxBaggageObjTmp = {
                                paxId: UI_tabAnci.currPaxIDRow,
                                segId: correctSegId,
                                baggageObjRef: baggageObj
                            };
                            UI_tabAnci.paxBaggageAdd(paxBaggageObjTmp);
                        }
                    }
                } else {
                    var paxBaggageObj = {
                        paxId: UI_tabAnci.currPaxIDRow,
                        segId: UI_tabAnci.currSegRow,
                        baggageObjRef: baggageObj
                    };

                    UI_tabAnci.paxBaggageAdd(paxBaggageObj);
                    var isConnectionFare = (jsonFareQuoteSummary.fareType == '2');
                    if (isConnectionFare) {
                        var correctBaggageSegId = UI_tabAnci
                            .getCorrectBaggageSegmentId(jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo);
                        var isReturn = jsBaggageModel[correctBaggageSegId].flightSegmentTO.returnFlag;
                        for (var k = 0; k < jsAnciSeg.length; k++) {
                            if (jsBaggageModel[k].baggages.length > 0 && isReturn == jsBaggageModel[k].flightSegmentTO.returnFlag && UI_tabAnci
                                .isBaggageAvailableForTheSegment(
                                    jsBaggageModel[k].baggages,
                                    baggageObj.baggageName)) {
                                var correctSegId = UI_tabAnci
                                    .getCorrectSegmentId(jsBaggageModel[k].flightSegmentTO.flightRefNumber);
                                UI_tabAnci.paxBaggageRemove(
                                    UI_tabAnci.currPaxIDRow, false,
                                    correctSegId);
                                var paxBaggageObjTmp = {
                                    paxId: UI_tabAnci.currPaxIDRow,
                                    segId: correctSegId,
                                    baggageObjRef: baggageObj
                                };
                                UI_tabAnci.paxBaggageAdd(paxBaggageObjTmp);

                            }
                        }
                    }
                }
            }
        }

    }

}
UI_tabAnci.isBaggageAvailableForTheSegment = function (segBaggages, baggageName) {
    for (var i = 0; i < segBaggages.length; i++) {
        if (segBaggages[i].baggageName == baggageName) {
            return true;
        }
    }
    return false;
}
// get correct segment Id
UI_tabAnci.getCorrectSegmentId = function (fltRefNo) {
    for (var k = 0; k < jsAnciSeg.length; k++) {
        if (jsAnciSeg[k].fltRefNo == fltRefNo) {
            return k;
        }
    }
}

// get correct segment Id
UI_tabAnci.getCorrectBaggageSegmentId = function (flightRefNumber) {
    for (var k = 0; k < jsBaggageModel.length; k++) {
        if (jsBaggageModel[k].flightSegmentTO.flightRefNumber == flightRefNumber) {
            return k;
        }
    }
}

/*
 * Meal Apply On Click
 */
UI_tabAnci.anciMealApplyOnClick = function () {
    UI_commonSystem.initializeMessage();
    
    if(!UI_tabAnci.isSelectedMealCountValid()){
    	return;
    }
    
    if (!UI_tabAnci.isMealSelected()) {
        showERRMessage('Please select a meal to assign');
        return;
    }

    if (UI_tabAnci.validateApply()) {
        var mealNAv = UI_tabAnci.mealNotAvailable(UI_tabAnci.currPaxIDRow);
        if (mealNAv != null) {
            showERRMessage('Not enough meals from ' + mealNAv.mealName + ', to assign to passenger');
            return;
        }
        UI_tabAnci.paxMealRemove(UI_tabAnci.currPaxIDRow, false);
        UI_tabAnci.mealCategoryMap = {};
        for (var i = 0; i < jsMealList.length; i++) {
        	 
        	var paxMealCategory = {
               	mealCategoryCode : jsMealList[i].mealCategoryCode,
               	flightRefNo : UI_tabAnci.currFltRefNo,
               	categoryRestricted : jsMealList[i].categoryRestricted
             };
        	 
            var qty = 1;
            if (UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo) && !jsMealList[i].categoryRestricted) {
                qty = parseInt($("#" + (i + 1) + "_mealQty").val(), 10);
            }
            
            if (qty > 0 & jsMealList[i].status == 'Y' && jsMealList[i].availableMeals >= qty 
            		&& !UI_tabAnci.paxMealCategoryCountRestricted(paxMealCategory)) {
                var mealObj = {
                    mealCode: jsMealList[i].mealCode,
                    mealName: jsMealList[i].mealName,
                    mealQty: qty,
                    mealCharge: jsMealList[i].mealCharge,
                    subTotal: parseFloat(jsMealList[i].mealCharge) * (qty),
                    mealIndex: i,
                    old: false
                };
                var paxMealObj = {
                    paxId: UI_tabAnci.currPaxIDRow,
                    segId: UI_tabAnci.currSegRow,
                    mealObjRef: mealObj
                };

                UI_tabAnci.paxMealAdd(paxMealObj);
                UI_tabAnci.paxMealCategoryCountUpdate(paxMealCategory);
                $("#" + (i + 1) + "_mealQty").val(qty);
            }else{
            	 $('#btnMealAdd_' + i).hide();
                 $("#" + (i + 1) + "_mealQty").val(0);
            }
        }
        // UI_tabAnci.buildMeal({segCode:UI_tabAnci.currFltRefNo});
    }

}

/*
 * Meal ApplyToAll On Click
 */
UI_tabAnci.anciMealApplyToAllOnClick = function () {
    UI_commonSystem.initializeMessage();
    
    if(!UI_tabAnci.isSelectedMealCountValid()){
    	return;
    }
    
    if (!UI_tabAnci.isMealSelected()) {
        showERRMessage('Please select a meal to assign');
        return;
    }
    var mealNAv = UI_tabAnci.mealNotAvailableForAll();
    if (mealNAv != null) {
        showERRMessage('Not enough meals from ' + mealNAv.mealName + ', to assign to all');
        return;
    }
    if (confirm('This will replace existing Meals if any!\nDo you want to continue?')) {
        var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
        var mealL = [];
        for (var paxI = 0; paxI < paxList.length; paxI++) {
            if (paxList[paxI].paxType != 'IN') {
                UI_tabAnci.paxMealRemove(paxI, false);
            }
        }
        
        UI_tabAnci.mealCategoryMap = {};
        for (var i = 0; i < jsMealList.length; i++) {
        	
        	var paxMealCategory = {
               	mealCategoryCode : jsMealList[i].mealCategoryCode,
              	flightRefNo : UI_tabAnci.currFltRefNo,
               	categoryRestricted : jsMealList[i].categoryRestricted
             };
        	
        	
            var qty = 1;
            if (UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo) && !jsMealList[i].categoryRestricted) {
                qty = parseInt($("#" + (i + 1) + "_mealQty").val(), 10);
            }
            if (qty >0 && jsMealList[i].status == 'Y' && jsMealList[i].availableMeals >= qty 
            		&& !UI_tabAnci.paxMealCategoryCountRestricted(paxMealCategory)) {
                var mealObj = {
                    mealCode: jsMealList[i].mealCode,
                    mealName: jsMealList[i].mealName,
                    mealQty: qty,
                    mealCharge: jsMealList[i].mealCharge,
                    subTotal: parseFloat(jsMealList[i].mealCharge) * (qty),
                    mealIndex: i,
                    old: false
                };
                mealL[mealL.length] = {
                    paxId: -1,
                    segId: UI_tabAnci.currSegRow,
                    mealObjRef: mealObj
                };
                
                $("#" + (i + 1) + "_mealQty").val(qty);
                UI_tabAnci.paxMealCategoryCountUpdate(paxMealCategory);
            }else{
            	 $('#btnMealAdd_' + i).hide();
                 $("#" + (i + 1) + "_mealQty").val(0);
            }
        }
        for (var paxI = 0; paxI < paxList.length; paxI++) {
            for (var i = 0; i < mealL.length; i++) {
                if (paxList[paxI].paxType != 'IN') {
                    mealL[i].paxId = paxI; // set the correct paxId
                    UI_tabAnci.paxMealAdd(mealL[i]);
                }
            }
        }
    }
}

UI_tabAnci.isSelectedMealCountValid = function(){
	
	for (var i = 0; i < jsMealList.length; i++) {
		var qty = parseInt($("#" + (i + 1) + "_mealQty").val(), 10);
        if (jsMealList[i].status == 'Y' && ( qty > 0 || isNaN(qty)))  {
            return true;
        }
    }
	return false;
}

UI_tabAnci.paxMealCategoryCountUpdate = function (paxMealCategory) {
	
	var mapKey = paxMealCategory.mealCategoryCode + paxMealCategory.flightRefNo;
	
	if(UI_tabAnci.mealCategoryMap [mapKey] !=null){
		UI_tabAnci.mealCategoryMap [mapKey]= {
	    	selectedCount : UI_tabAnci.mealCategoryMap [mapKey].selectedCount +1
	    };
	}else{
		UI_tabAnci.mealCategoryMap [mapKey]= {
			selectedCount : 1
		};
	}
}

UI_tabAnci.paxMealCategoryCountRestricted = function (paxMealSelction){
	var mapKey = paxMealSelction.mealCategoryCode + paxMealSelction.flightRefNo;
	
	if(paxMealSelction.categoryRestricted 
			&& UI_tabAnci.mealCategoryMap[mapKey] != null 
			&& UI_tabAnci.mealCategoryMap[mapKey].selectedCount >0){
		return true;
		
	}
	
	return false;
}

/*
 * Baggage ApplyToAll On Click
 */
UI_tabAnci.anciBaggageApplyToAllOnClick = function () {
    UI_commonSystem.initializeMessage();
    if (!UI_tabAnci.isBaggageSelected()) {
        showERRMessage('Please select a Baggage to assign');
        return




    }
    var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
    var baggageNAv = UI_tabAnci.baggageNotAvailableForAll();

    if (baggageNAv != null) {
        showERRMessage('Not enough Baggage from "' + baggageNAv.baggageName + '", to assign to all');
        return;
    }

    if (confirm('This will replace existing Baggages if any!\nDo you want to continue?')) {
        var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;

        var baggageL = [];
        correctSegId = null;
        for (var paxI = 0; paxI < paxList.length; paxI++) {
            if (paxList[paxI].paxType != 'IN') {
                UI_tabAnci.paxBaggageRemove(paxI, false, null);
            }
        }
        for (var i = 0; i < jsBaggageList.length; i++) {
            var qty = 1;

            if (jsBaggageList[i].status == 'Y') {
                var baggageObj = {
                    baggageCode: jsBaggageList[i].baggageCode,
                    baggageName: jsBaggageList[i].baggageName,
                    baggageQty: qty,
                    baggageCharge: jsBaggageList[i].baggageCharge,
                    subTotal: parseFloat(jsBaggageList[i].baggageCharge) * (qty),
                    baggageIndex: i,
                    ondBaggageChargeId: jsBaggageList[i].ondBaggageChargeId,
                    old: false
                };
                var correctBaggageSegId = UI_tabAnci
                    .getCorrectBaggageSegmentId(jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo);
                var grpId = jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId;
                var grpCount = "";
                if (grpId != null && grpId != "") {
                    grpCount = getBaggageONDGrpIdWiseCount(jsBaggageModel,
                        grpId);
                }

                if (grpCount != "") {
                    baggageObj.subTotal = parseFloat(baggageObj.subTotal) / grpCount;
                    var correctBagSegId = null;
                    for (var k = 0; k < jsAnciSeg.length; k++) {
                        if (getBaggageONDGrpId(jsAnciSeg[k].fltRefNo) == grpId) {
                            correctBagSegId = UI_tabAnci
                                .getCorrectBaggageSegmentId(jsAnciSeg[k].fltRefNo)
                            correctSegId = UI_tabAnci
                                .getCorrectSegmentId(jsBaggageModel[correctBagSegId].flightSegmentTO.flightRefNumber);
                        }
                    }
                } else {
                    var paxBaggageObj = {
                        paxId: UI_tabAnci.currPaxIDRow,
                        segId: UI_tabAnci.currSegRow,
                        baggageObjRef: baggageObj
                    };

                    UI_tabAnci.paxBaggageAdd(paxBaggageObj);
                    var isConnectionFare = (jsonFareQuoteSummary.fareType == '2');
                    if (isConnectionFare) {
                        var correctBaggageSegId = UI_tabAnci
                            .getCorrectBaggageSegmentId(jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo);
                        var isReturn = jsBaggageModel[correctBaggageSegId].flightSegmentTO.returnFlag;
                        for (var k = 0; k < jsAnciSeg.length; k++) {
                            if (jsBaggageModel[k].baggages.length > 0 && isReturn == jsBaggageModel[k].flightSegmentTO.returnFlag && UI_tabAnci
                                .isBaggageAvailableForTheSegment(
                                    jsBaggageModel[k].baggages,
                                    baggageObj.baggageName)) {
                                var correctSegId = UI_tabAnci
                                    .getCorrectSegmentId(jsBaggageModel[k].flightSegmentTO.flightRefNumber);
                            }
                        }
                    }
                    baggageL[baggageL.length] = paxBaggageObjTmp;
                }
            }
        }

        for (var paxI = 0; paxI < paxList.length; paxI++) {
            if (paxList[paxI].paxType != 'IN') {
                var correctBagSegId = null;
                for (var k = 0; k < jsAnciSeg.length; k++) {
                    if (getBaggageONDGrpId(jsAnciSeg[k].fltRefNo) == grpId) {
                        correctBagSegId = UI_tabAnci
                            .getCorrectBaggageSegmentId(jsAnciSeg[k].fltRefNo)
                        // if(correctBagSegId == correctBaggageSegId){
                        correctSegId = UI_tabAnci
                            .getCorrectSegmentId(jsBaggageModel[correctBagSegId].flightSegmentTO.flightRefNumber);
                        UI_tabAnci.paxBaggageRemove(paxI, false, correctSegId);
                        // }
                    }
                }
            }
        }

        for (var paxI = 0; paxI < paxList.length; paxI++) {
            if (paxList[paxI].paxType != 'IN') {
                var correctBagSegId = null;
                for (var k = 0; k < jsAnciSeg.length; k++) {
                    if (getBaggageONDGrpId(jsAnciSeg[k].fltRefNo) == grpId) {

                        correctBagSegId = UI_tabAnci
                            .getCorrectBaggageSegmentId(jsAnciSeg[k].fltRefNo)
                        // if(correctBagSegId == correctBaggageSegId){
                        correctSegId = UI_tabAnci
                            .getCorrectSegmentId(jsBaggageModel[correctBagSegId].flightSegmentTO.flightRefNumber);
                        var paxBaggageObjTmp = {
                            paxId: paxI,
                            segId: correctSegId,
                            baggageObjRef: baggageObj
                        };
                        UI_tabAnci.paxBaggageAdd(paxBaggageObjTmp);
                        // }
                    }
                }
            }
        }
    }

}

/* Check if at least one meal is selected */
UI_tabAnci.isMealSelected = function () {
    for (var i = 0; i < jsMealList.length; i++) {
        if (jsMealList[i].status == 'Y') {
            return true;
        }
    }
    return false;
}

/* Check if at least one baggage is selected */
UI_tabAnci.isBaggageSelected = function () {
    for (var i = 0; i < jsBaggageList.length; i++) {
        if (jsBaggageList[i].status == 'Y') {
            return true;
        }
    }
    return false;
}

/*
 * Check Meal availability for all
 */
UI_tabAnci.mealNotAvailableForAll = function () {
    var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
    for (var i = 0; i < jsMealList.length; i++) {
        var qty = 1;
        if (UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo)) {
            qty = parseInt($("#" + (i + 1) + "_mealQty").val(), 10);
        }
        var mmMeal = jsMealModel[jsMealList[i].mmSGI].meals[jsMealList[i].mmI];
        if (jsMealList[i].status == 'Y' && (mmMeal.availableMeals) < parseInt(qty * paxList.length, 10)) {
            return jsMealList[i];
        }
    }
    return null;
}

/*
 * Check Baggage availability for all
 */
UI_tabAnci.baggageNotAvailableForAll = function () {
    var paxList = jsAnciSeg[UI_tabAnci.currSegRow].pax;
    for (var i = 0; i < jsBaggageList.length; i++) {
        var smBaggage = jsBaggageModel[jsBaggageList[i].mmSGI].baggages[jsBaggageList[i].mmI];
        if (jsBaggageList[i].status != 'Y' || smBaggage.availableQty == -1)
            continue;
        if ((smBaggage.availableQty) < parseInt(paxList.length, 10)) {
            return jsBaggageList[i];
        }
    }
    return null;
}

/*
 * Check Meal availability per pax
 */
UI_tabAnci.mealNotAvailable = function (paxId) {
    var pax = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId];
    for (var i = 0; i < jsMealList.length; i++) {
        var qty = 1;
        if (UI_tabAnci.setMultiMealEnabled(UI_tabAnci.currFltRefNo)) {
            qty = parseInt($("#" + (i + 1) + "_mealQty").val(), 10);
        }
        var availCount = jsMealList[i].availableMeals;
        var paxMeal = UI_tabAnci.getPaxMealsPerCode(pax.meal.meals,
            jsMealList[i].mealCode);
        if (paxMeal != null) {
            availCount = jsMealList[i].availableMeals + paxMeal.mealQty;
        }
        if (jsMealList[i].status == 'Y' && availCount < parseInt(qty, 10)) {
            return jsMealList[i];
        }
    }
    return null;
}

UI_tabAnci.getPaxMealsPerCode = function (meals, mealCode) {
    for (var i = 0; i < meals.length; i++) {
        if (meals[i].mealCode == mealCode) {
            return meals[i];
        }
    }
    return null;
}

/*
 * Add pax meal
 */
UI_tabAnci.paxMealAdd = function (paxMealRefObj) {
    var paxId = paxMealRefObj.paxId;
    var segId = paxMealRefObj.segId;
    var mealObj = paxMealRefObj.mealObjRef;
    // UI_tabAnci.currSegRow
    var mealData = jsAnciSeg[segId].pax[paxId].meal;
    var paxMeals = mealData.meals;
    var mealId = null;

    mealId = paxMeals.length;
    paxMeals[mealId] = mealObj;

    mealData.mealChargeTotal += mealObj.subTotal;
    mealData.mealQtyTotal += mealObj.mealQty;

    UI_tabAnci.updateMealAvailCount(mealObj, 'DEDUCT');
    UI_tabAnci.updatepaxTotals(paxId, mealObj.subTotal, 'ADD');
    UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.MEAL, mealObj.subTotal,
        'ADD', paxId, mealObj.old);
    UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.MEAL, mealObj.mealQty,
        'ADD', mealObj.old);

    UI_tabAnci.buildPaxMealDetail(paxMeals, paxId);
    
    var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.MEALS,mealData.mealChargeTotal);
    var mealTotal = $.airutil.format.currency(mealData.mealChargeTotal);  
    
	if(applyFormatter){
		mealTotal = UI_tabAnci.freeServiceDecorator;		
	}

    $("#tdPaxMeal_1_" + paxId).html(" ");
    $("#tdPaxMeal_2_" + paxId).html(mealData.mealQtyTotal);
    $("#tdPaxMeal_3_" + paxId).html(mealTotal);

}

/*
 * Add pax baggage
 */
UI_tabAnci.paxBaggageAdd = function (paxBaggageRefObj) {
    var paxId = paxBaggageRefObj.paxId;
    var segId = paxBaggageRefObj.segId;
    var baggageObj = paxBaggageRefObj.baggageObjRef;
    // UI_tabAnci.currSegRow
    var baggageData = jsAnciSeg[segId].pax[paxId].baggage;
    var paxBaggages = baggageData.baggages;
    var baggageId = null;

    baggageId = paxBaggages.length;
    paxBaggages[baggageId] = baggageObj;

    // AARESAA-13460 Fix
    if (typeof (baggageData.baggageChargeTotal) == "undefined") {
        baggageData.baggageChargeTotal = 0;
    } else {
        tempBaggageChargeTotal = Number(baggageData.baggageChargeTotal) + Number(baggageObj.baggageCharge);
        baggageData.baggageChargeTotal = tempBaggageChargeTotal.toFixed(2);
    }
    baggageData.baggageQtyTotal += baggageObj.baggageQty;

    // UI_tabAnci.updateMealAvailCount(baggageObj,'DEDUCT');
    UI_tabAnci.updatepaxTotals(paxId, baggageObj.subTotal, 'ADD');
    UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.BAGGAGE,
        baggageObj.subTotal, 'ADD', paxId, baggageObj.old);
    UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.BAGGAGE,
        baggageObj.baggageQty, 'ADD', baggageObj.old);

    UI_tabAnci.buildPaxBaggageDetail(paxBaggages, paxId);

    $("#tdPaxBaggage_1_" + paxId).html(" ");
    $("#tdPaxBaggage_2_" + paxId).html(baggageData.baggageQtyTotal);
    $("#tdPaxBaggage_3_" + paxId).html(
        parseFloat(baggageData.baggageChargeTotal));

}

UI_tabAnci.paxBaggageRemoveWrapper = function (paxId, refresh, segId) {
    UI_tabAnci.paxBaggageRemove(paxId, refresh, segId);
    var correctBaggageSegId = UI_tabAnci
        .getCorrectBaggageSegmentId(jsAnciSeg[UI_tabAnci.currSegRow].fltRefNo);
    var grpId = jsBaggageModel[correctBaggageSegId].flightSegmentTO.baggageONDGroupId;

    if (grpId != null && grpId != "") {
        for (var i = 0; i < jsBaggageModel.length; i++) {
            var flightSegmentRef = jsBaggageModel[i].flightSegmentTO;
            if (flightSegmentRef.baggageONDGroupId == grpId) {
                var correctSegId = UI_tabAnci
                    .getCorrectBaggageSegmentId(flightSegmentRef.flightRefNumber);
                UI_tabAnci.paxBaggageRemove(paxId, false, correctSegId);
            }
        }
    }
}
/*
 * Remove pax's all baggages
 */
UI_tabAnci.paxBaggageRemove = function (paxId, refresh, segId) {
    if (paxId == null) {
        paxId = UI_tabAnci.currPaxIDRow;
    }
    if (paxId == null) {
        showERRMessage("Impossible has happend! pax has no id");
        return;
    }
    if (segId == null) {
        segId = UI_tabAnci.currSegRow;
    }

    var baggageData = jsAnciSeg[segId].pax[paxId].baggage;
    var amt = baggageData.baggageChargeTotal;

    UI_tabAnci.updatepaxTotals(paxId, amt, 'DEDUCT');

    UI_tabAnci.clearPaxBaggageDetail(paxId);

    $("#tdPaxBaggage_1_" + paxId).html(" ");
    $("#tdPaxBaggage_2_" + paxId).html(" ");
    $("#tdPaxBaggage_3_" + paxId).html(" ");

    while (baggageData.baggages.length > 0) {
        var baggage = baggageData.baggages.pop();
        if (baggage.old) {
            var removedBaggages = jsCurrAnciSeg[segId].pax[paxId].baggage.baggages;
            removedBaggages[removedBaggages.length] = baggage;
        }
        // UI_tabAnci.updateMealAvailCount(baggage,'ADD');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.BAGGAGE,
            baggage.subTotal, 'DEDUCT', paxId, baggage.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.BAGGAGE,
            baggage.baggageQty, 'DEDUCT', baggage.old);
    }

    baggageData.baggages = [];
    baggageData.baggageChargeTotal = 0;
    baggageData.baggageQtyTotal = 0;

    if (refresh && paxId == UI_tabAnci.currPaxIDRow) {
        UI_tabAnci.refreshBaggage();
    }
}

/*
 * Remove pax's all meals
 */
UI_tabAnci.paxMealRemove = function (paxId, refresh) {
    if (paxId == null) {
        paxId = UI_tabAnci.currPaxIDRow;
    }
    if (paxId == null) {
        showERRMessage("Impossible has happend! pax has no id");
        return;
    }

    var mealData = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].meal;
    var amt = mealData.mealChargeTotal;

    UI_tabAnci.updatepaxTotals(paxId, amt, 'DEDUCT');

    UI_tabAnci.clearPaxMealDetail(paxId);

    $("#tdPaxMeal_1_" + paxId).html(" ");
    $("#tdPaxMeal_2_" + paxId).html(" ");
    $("#tdPaxMeal_3_" + paxId).html(" ");

    while (mealData.meals.length > 0) {
        var meal = mealData.meals.pop();
        if (meal.old) {
            var removedMeals = jsCurrAnciSeg[UI_tabAnci.currSegRow].pax[paxId].meal.meals;
            removedMeals[removedMeals.length] = meal;
        }
        UI_tabAnci.updateMealAvailCount(meal, 'ADD');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.MEAL, meal.subTotal,
            'DEDUCT', paxId, meal.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.MEAL, meal.mealQty,
            'DEDUCT', meal.old);
    }

    mealData.meals = [];
    mealData.mealChargeTotal = 0;
    mealData.mealQtyTotal = 0;

    if (refresh && paxId == UI_tabAnci.currPaxIDRow) {
        UI_tabAnci.refreshMeal();
    }
}
/*
 * Remove single meal code from pax
 */
UI_tabAnci.paxSingleMealRemove = function (paxId, mealCode) {
    var mealData = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].meal;
    var paxMeals = mealData.meals;
    var meal = null;
    for (var i = 0; i < paxMeals.length; i++) {
        if (paxMeals[i].mealCode == mealCode) {
            meal = paxMeals.splice(i, 1)[0];
            break;
        }
    }
    if (meal != null) {
        if (meal.old) {
            var removedMeals = jsCurrAnciSeg[UI_tabAnci.currSegRow].pax[paxId].meal.meals;
            removedMeals[removedMeals.length] = meal;
        }
        mealData.mealChargeTotal -= meal.subTotal;
        mealData.mealQtyTotal -= meal.mealQty;
        UI_tabAnci.updateMealAvailCount(meal, 'ADD');
        UI_tabAnci.updatepaxTotals(paxId, meal.subTotal, 'DEDUCT');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.MEAL, meal.subTotal,
            'DEDUCT', paxId, meal.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.MEAL, meal.mealQty,
            'DEDUCT', meal.old);
        UI_tabAnci.clearPaxMealDetail(paxId, mealCode);
        $("#tdPaxMeal_2_" + paxId).html(mealData.mealQtyTotal);
        
        var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.MEALS,mealData.mealChargeTotal);
        var mealTotal =mealData.mealChargeTotal;    
    	if(applyFormatter){
    		mealTotal = UI_tabAnci.freeServiceDecorator;		
    	}
        
        $("#tdPaxMeal_3_" + paxId).html(mealTotal);

        if (paxId == UI_tabAnci.currPaxIDRow) {
            UI_tabAnci.refreshMeal();
        }
    }
}

/*
 * Remove single bagge from pax
 */
UI_tabAnci.paxSingleBaggageRemove = function (paxId, baggageName) {
    var baggageData = jsAnciSeg[UI_tabAnci.currSegRow].pax[paxId].baggage;
    var paxBaggages = baggageData.baggages;
    var baggage = null;
    for (var i = 0; i < paxBaggages.length; i++) {
        if (paxBaggages[i].baggageName == baggageName) {
            baggage = paxBaggages.splice(i, 1)[0];
            break;
        }
    }
    if (baggage != null) {
        if (baggage.old) {
            var removedBaggages = jsCurrAnciSeg[UI_tabAnci.currSegRow].pax[paxId].baggage.baggages;
            removedBaggages[removedBaggages.length] = baggage;
        }
        baggageData.baggageChargeTotal -= baggage.subTotal;
        baggageData.baggageQtyTotal -= baggage.baggageQty;
        // UI_tabAnci.updateMealAvailCount(baggage,'ADD');
        UI_tabAnci.updatepaxTotals(paxId, baggage.subTotal, 'DEDUCT');
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.BAGGAGE,
            baggage.subTotal, 'DEDUCT', paxId, baggage.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.BAGGAGE,
            baggage.baggageQty, 'DEDUCT', baggage.old);
        UI_tabAnci.clearPaxBaggageDetail(paxId, baggageName);
        $("#tdPaxBaggage_2_" + paxId).html(baggageData.baggageQtyTotal);
        $("#tdPaxBaggage_3_" + paxId).html(baggageData.baggageChargeTotal);

        if (paxId == UI_tabAnci.currPaxIDRow) {
            UI_tabAnci.refreshBaggage();
        }
    }
}

/*
 * Populate Modify Baggage Data
 */
UI_tabAnci.populateModifyBaggageData = function (p) {

    for (var j = 0; j < jsonPaxAdults.length; j++) {
        var anciSegs = jsonPaxAdults[j].currentAncillaries;
        for (var k = 0; k < anciSegs.length; k++) {
            var anciSeg = anciSegs[k];
            if (anciSeg.baggageDTOs != null && anciSeg.baggageDTOs.length != null && anciSeg.baggageDTOs.length > 0) {
                var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
                var baggages = anciSeg.baggageDTOs;
                var currentSeg = UI_tabAnci.compareFlightRefNo(p.fltRefNo,
                    fltRefNo);
                var segIndex = jsAnciSegRefMap[fltRefNo].index;
                var baggageData = jsAnciSeg[segIndex].pax[j].baggage; // TODO
                // get
                // the
                // proper
                // segId
                var baggageAvailable = UI_tabAnci.isAnciAvailable(
                    jsonSegAnciAvail[segIndex].ancillaryStatusList,
                    UI_tabAnci.anciType.BAGGAGE);
                if (baggageAvailable) {
                    for (var i = 0; i < baggages.length; i++) {
                        var baggage = baggages[i];
                        var baggageRefObj = UI_tabAnci.getBaggageRefObj(
                            baggage.baggageName, fltRefNo);
                        if (baggageRefObj == null) {
                            continue;
                        }

                        var baggageObj = {
                            baggageName: baggage.baggageName,
                            baggageQty: 1,
                            baggageCharge: baggage.baggageCharge,
                            subTotal: parseFloat(baggage.baggageCharge),
                            baggageIndex: baggageRefObj.mmI,
                            ondBaggageChargeId: baggage.ondBaggageChargeId,
                            old: true
                        };

                        var paxBaggages = baggageData.baggages;
                        if (!UI_tabAnci.modifyBaggageLoaded) {
                            paxBaggages[paxBaggages.length] = baggageObj;
                            baggageData.baggageChargeTotal += baggageObj.subTotal;
                            baggageData.baggageQtyTotal += baggageObj.baggageQty;
                            UI_tabAnci.updatepaxTotals(j, baggageObj.subTotal,
                                'ADD');
                            UI_tabAnci.updateAnciTotals(
                                UI_tabAnci.anciType.BAGGAGE,
                                baggageObj.subTotal, 'ADD', j, true);
                            UI_tabAnci.updateAnciCounts(
                                UI_tabAnci.anciType.BAGGAGE,
                                baggageObj.baggageQty, 'ADD', true);
                        }
                    }
                }
            }
        }
    }
    UI_tabAnci.modifyBaggageLoaded = true;
}

/*
 * Populate Modify Meal Data
 */
UI_tabAnci.populateModifyMealData = function (p) {

    for (var j = 0; j < jsonPaxAdults.length; j++) {
        var anciSegs = jsonPaxAdults[j].currentAncillaries;
        for (var k = 0; k < anciSegs.length; k++) {
            var anciSeg = anciSegs[k];
            if (anciSeg.mealDTOs != null && anciSeg.mealDTOs.length != null && anciSeg.mealDTOs.length > 0) {
                var fltRefNo = anciSeg.flightSegmentTO.flightRefNumber;
                var meals = anciSeg.mealDTOs;
                var currentSeg = UI_tabAnci.compareFlightRefNo(p.fltRefNo,
                    fltRefNo);
                var segIndex = jsAnciSegRefMap[fltRefNo].index;
                var mealData = jsAnciSeg[segIndex].pax[j].meal; // TODO get
                // the
                // proper
                // segId
                var mealAvailable = UI_tabAnci.isAnciAvailable(
                    jsonSegAnciAvail[segIndex].ancillaryStatusList,
                    UI_tabAnci.anciType.MEAL);
                if (mealAvailable) {
                    for (var i = 0; i < meals.length; i++) {
                        var meal = meals[i];
                        var mealRefObj = UI_tabAnci.getMealRefObj(
                            meal.mealCode, fltRefNo);
                        var mealInd = 0;
                        if (mealRefObj != null) { // when originally selected

                            mealInd = mealRefObj.mmI;
                        }
                        var mealObj = {
                            mealCode: meal.mealCode,
                            mealName: meal.mealName,
                            mealQty: meal.soldMeals,
                            mealCharge: meal.mealCharge,
                            subTotal: parseFloat(meal.mealCharge) * parseInt(meal.soldMeals, 10),
                            mealIndex: mealInd,
                            old: true,
                            categoryRestricted : meal.categoryRestricted,
                            mealCategoryCode : meal.mealCategoryCode
                        };
                        //

                        var paxMeals = mealData.meals;
                        if (!UI_tabAnci.modifyMealLoaded) {
                            paxMeals[paxMeals.length] = mealObj;
                            mealData.mealChargeTotal += mealObj.subTotal;
                            mealData.mealQtyTotal += mealObj.mealQty;
                            UI_tabAnci.updatepaxTotals(j, mealObj.subTotal,
                                'ADD');
                            UI_tabAnci.updateAnciTotals(
                                UI_tabAnci.anciType.MEAL, mealObj.subTotal,
                                'ADD', j, true);
                            UI_tabAnci.updateAnciCounts(
                                UI_tabAnci.anciType.MEAL, mealObj.mealQty,
                                'ADD', true);
                        }

                        if (currentSeg) {
                            UI_tabAnci.updateMealAvailCount(mealObj, 'ADD');
                        }
                    }
                }
            }
        }
    }
    UI_tabAnci.modifyMealLoaded = true;
}

UI_tabAnci.getMealRefObj = function (mealCode, fltRefNo) {
    if (UI_tabAnci.mealRefMap[fltRefNo] == null) {
        UI_tabAnci.mealRefMap[fltRefNo] = {
            meals: {}
        };
        for (var i = 0; i < jsMealModel.length; i++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsMealModel[i].flightSegmentTO.flightRefNumber, fltRefNo)) {
                for (var j = 0; j < jsMealModel[i].meals.length; j++) {
                    var tm = jsMealModel[i].meals[j];
                    UI_tabAnci.mealRefMap[fltRefNo].meals[tm.mealCode] = {
                        mmI: j,
                        mmSGI: i,
                        mealName: tm.mealName,
                        mealCode: tm.mealCode,
                        mealCharge: tm.mealCharge,
                        mealDescription: tm.mealDescription,
                        availableMeals: parseInt(tm.availableMeals, 10),
                        mealCategoryCode : tm.mealCategoryCode,
                        categoryRestricted : tm.categoryRestricted
                    };
                }
            }
        }
    }
    var mealRef = UI_tabAnci.mealRefMap[fltRefNo].meals[mealCode];

    return mealRef;
}

/**
 * getBaggageRefObj
 */

UI_tabAnci.getBaggageRefObj = function (baggageName, fltRefNo) {
    if (UI_tabAnci.baggageRefMap[fltRefNo] == null) {
        UI_tabAnci.baggageRefMap[fltRefNo] = {
            baggages: {}
        };
        for (var i = 0; i < jsBaggageModel.length; i++) {
            if (UI_tabAnci
                .compareFlightRefNo(
                    jsBaggageModel[i].flightSegmentTO.flightRefNumber,
                    fltRefNo)) {
                for (var j = 0; j < jsBaggageModel[i].baggages.length; j++) {
                    var tm = jsBaggageModel[i].baggages[j];
                    UI_tabAnci.baggageRefMap[fltRefNo].baggages[tm.baggageName] = {
                        mmI: j,
                        mmSGI: i,
                        baggageName: tm.baggageName,
                        baggageCharge: tm.baggageCharge,
                        baggageDescription: tm.baggageDescription,
                        ondBaggageChargeId: tm.ondBaggageChargeId
                    };
                }
            }
        }
    }
    var baggageRef = UI_tabAnci.baggageRefMap[fltRefNo].baggages[baggageName];

    return baggageRef;
}

/*
 * Populate Baggage Data
 */
UI_tabAnci.populateBaggageData = function (p) {
    var baggageData = null;
    var paxId = null;

    var paxData = jsAnciSeg[parseInt(p.segID, 10)].pax;
    for (var i = 0; i < paxData.length; i++) {
        if (paxData[i].paxType != 'IN') {
            baggageData = paxData[i].baggage;

            UI_tabAnci.buildPaxBaggageDetail(baggageData.baggages, i);
            // for(var j = 0 ; j < baggageData.baggages.length ; j++){
            // UI_tabAnci.updateBaggageAvailCount(baggageData.baggages[j],'DEDUCT');
            // }

            $("#tdPaxBaggage_1_" + i).html(" ");
            $("#tdPaxBaggage_2_" + i).html(baggageData.baggageQtyTotal);
            $("#tdPaxBaggage_3_" + i).html(baggageData.baggageChargeTotal);
        }
    }
}

/*
 * Populate Meal Data
 */
UI_tabAnci.populateMealData = function (p) {
    var mealData = null;
    var paxId = null;

    var paxData = jsAnciSeg[parseInt(p.segID, 10)].pax;
    for (var i = 0; i < paxData.length; i++) {
        if (paxData[i].paxType != 'IN') {
            mealData = paxData[i].meal;

            UI_tabAnci.buildPaxMealDetail(mealData.meals, i,(UI_tabAnci.isAnciModify || UI_tabAnci.reprotect.meal));
            for (var j = 0; j < mealData.meals.length; j++) {
                UI_tabAnci.updateMealAvailCount(mealData.meals[j], 'DEDUCT');
            }

            
            var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.MEALS,mealData.mealChargeTotal);
            var mealTotal =mealData.mealChargeTotal;    
        	if(applyFormatter){
        		mealTotal = UI_tabAnci.freeServiceDecorator;		
        	}
            
            $("#tdPaxMeal_1_" + i).html(" ");
            $("#tdPaxMeal_2_" + i).html(mealData.mealQtyTotal);
            $("#tdPaxMeal_3_" + i).html(mealTotal);
        }
    }
}

/*
 * Passenger Selection for baggage
 */
UI_tabAnci.tdBaggagePaxOnClick = function () {
    // 0 - tdPaxBaggage;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";
    var firstTime = false;

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxBaggage_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxBaggage_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    } else {
        firstTime = true;
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxBaggage_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxBaggage_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");

//    if (!(firstTime && jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].baggage.baggages.length == 0)) {
        UI_tabAnci.refreshBaggage();
//    }
}

/*
 * Passenger Selection
 */
UI_tabAnci.tdMealPaxOnClick = function () {
    // 0 - tdPaxMeal;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";
    var firstTime = false;

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxMeal_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxMeal_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    } else {
        firstTime = true;
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxMeal_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxMeal_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");

    if (!(firstTime && jsAnciSeg[UI_tabAnci.currSegRow].pax[UI_tabAnci.currPaxIDRow].meal.meals.length == 0)) {
        UI_tabAnci.refreshMeal();
    }
}

UI_tabAnci.getDisplayMeals = function () {
    var fltRefNo = UI_tabAnci.currFltRefNo;
    for (var i = 0; i < jsMealModel.length; i++) {
        if (UI_tabAnci.compareFlightRefNo(
            jsMealModel[i].flightSegmentTO.flightRefNumber, fltRefNo)) {
            return jsMealModel[i].meals;
        }
    }
    return [];
}

// ------------------------------------------------------------ Seat map
// Request -------------------------------------------------

UI_tabAnci.getSelectedCabinClass = function () {
    if (!UI_tabAnci.isAnciModify) {    	
    	return UI_tabSearchFlights.objResSrchParams.classOfService;
    } else {
        return UI_tabAnci.flightSegmentList[0].cabinClassCode;
    }
}

UI_tabAnci.getSelectedLogicalCabinClass = function (flightRefNum) {
    if (!UI_tabAnci.isAnciModify && !UI_tabAnci.isRequote) {
        if (UI_tabSearchFlights.isCalenderMinimumFare()) {
        	data = UI_MinimumFare.createSearchParams('searchParams');
        } else {
        	data = UI_tabSearchFlights.createSearchParams('searchParams');
        }
        if (!UI_tabSearchFlights.isMulticity()) {
        	var tempSearchParams = UI_tabSearchFlights.createSearchParams('searchParams');
            var logicalCCSelction = tempSearchParams['searchParams.fareQuoteLogicalCCSelection'];
        } else {
            var logicalCCSelction = $.toJSON(UI_Multicity
                .ondWiseLogicalCabinClassSelection());
        }

        if (logicalCCSelction != null && logicalCCSelction != '' && logicalCCSelction != 'null') {
            var logicalCCSelJson = $.parseJSON(logicalCCSelction);
            var flightList = [];
            if (UI_tabAnci.isRequote) {
                flightList = UI_requote.populateFlightSegmentList();
            } else if (UI_tabSearchFlights.isMulticity()) {
                flightList = UI_Multicity.getFlightRPHList();
            } else {
				if (UI_tabSearchFlights.isCalenderMinimumFare()) {
					flightList = UI_MinimumFare.getFlightRPHList();
				} else {
					flightList = UI_tabSearchFlights.getFlightRPHList();
				}
            }
            for (var i = 0; i < flightList.length; i++) {
                var fltObj = flightList[i];
                if (fltObj.flightRPH.split("#")[0] == flightRefNum) {
                    return logicalCCSelJson[fltObj.ondSequence];
                }
            }
        } else {
            return data['searchParams.logicalCabinClass'];
        }

    } else if (UI_tabAnci.isRequote) {
        data = UI_tabSearchFlights.createSearchParams('searchParams');
        var logicalCCSelction = data['searchParams.fareQuoteLogicalCCSelection'];
        if (logicalCCSelction != null && logicalCCSelction != '' && logicalCCSelction != 'null') {
            var logicalCCSelJson = $.parseJSON(logicalCCSelction);
            var ondList = UI_requote.getONDSearchDTOList();
            for (var i = 0; i < ondList.length; i++) {
            	var flightRPFList = ondList[i].flightRPHList;
            	for (var j = 0; j < flightRPFList.length; j++) {
            		var splitedFlightRph = flightRPFList[j].split("#");
            		if(splitedFlightRph[0] == flightRefNum){
                		return logicalCCSelJson[i];
                	}
            	}
            }
            return data['searchParams.logicalCabinClass'];
        } else {
            return data['searchParams.logicalCabinClass'];
        }
    } else {
    	for(var a = 0; a < UI_tabAnci.flightSegmentList.length; a++){
    		if(flightRefNum == UI_tabAnci.flightSegmentList[a].flightSegmentRefNumber){
    			return UI_tabAnci.flightSegmentList[a].logicalCabinClass;
    		}
    	}        
        return UI_tabAnci.flightSegmentList[0].logicalCabinClass;
    }
}

/* Seat Map */
UI_tabAnci.buildSeatMap = function (p) {
    $("#tblSeatMap").hide();
    $("#spnAnciNoSeat").hide();
    var blnCCsFound = false;
    var cabinCls = null;
    var intLen = null;
    var intSegRow = null;
    var populateRefMap = false;
    var seatRefMap = null;
    var fltRefNo = p.fltRefNo;
    var i = 0;
    var selectedLogicalCC = UI_tabAnci.getSelectedLogicalCabinClass(fltRefNo);
    var availLCCArray = [];
    var selectedCabinClass = null;
    
    if (p.fltRefNo != null) {
        intLen = jsSMModel.length;
        i = 0;
        do {
            if (UI_tabAnci.compareFlightRefNo(
                jsSMModel[i].flightSegmentDTO.flightRefNumber, p.fltRefNo)) {
                cabinCls = jsSMModel[i].lccSeatMapDetailDTO.cabinClass;
                seatRefMap = UI_tabAnci.seatRefMap[fltRefNo];
                selectedCabinClass = jsSMModel[i].flightSegmentDTO.cabinClassCode;
                if (seatRefMap == null) {
                    populateRefMap = true;
                    seatRefMap = {
                        seats: {}
                    };
                }
                intSegRow = i;
                blnCCsFound = true;
                break;
            }
            i++;
        } while (--intLen)
    }
    
    if(selectedCabinClass == null){
        selectedCabinClass = UI_tabAnci.getSelectedCabinClass();
    }

    if (!blnCCsFound) {
        $("#spnAnciNoSeat").show();
    } else {
        if (cabinCls != null) {
            var ccLen = cabinCls.length;

            if (ccLen > 0) {
                /* Initialize the Details */
                $("#tbdySeatMap").find("tr").remove();
                $("#tbdyWingRight").find("tr").remove();
                $("#tbdyWingLeft").find("tr").remove();

                $("#tblSeatMap").show();

                var tblRow = null;
                var tblWingRowLeft = null;
                var tblWingRowRight = null;
                var isleRow = null;
                var blankIsle = {
                    desc: "&nbsp;",
                    css: "aisle",
                    textCss: "font-size:9px;",
                    align: "right"
                };

                var columnLetters = new Array('A', 'B', 'C', 'D', 'E', 'F',
                    'G', 'H', 'I', 'J', 'K');
                var colLtrStr = "ABCDEFGHIJK";

                $('#tbdySeatMap').append("<tr id='trCabinClass'></tr>");
                $('#tbdyWingRight').append("<tr id='trRWCC'></tr>");
                $('#tbdyWingLeft').append("<tr id='trLWCC'></tr>");
                var buildLetters = true;
                for (var ccI = 0; ccI < ccLen; ccI++) {
                    buildLetters = true;
                    var cc = cabinCls[ccI];
                    var activeCC = false;

                    if (selectedCabinClass == cc.cabinType) {
                        activeCC = true;
                    }
                    $('#trCabinClass')
                        .append(
                            '<td><table cellspacing="0" cellpadding="0"><tbody class="cabinClass ' + cc.cabinType + '" style="margin-right:5px;"><tr id="trCC_' + ccI + '"></tr></tbody></table></td>');
                    $('#trRWCC').append(
                        '<td><table ><tbody  style="margin-right:5px;"><tr id="trCCRW_' + ccI + '"></tr></tbody></table></td>');
                    $('#trLWCC').append(
                        '<td><table ><tbody  style="margin-right:5px;"><tr id="trCCLW_' + ccI + '"></tr></tbody></table></td>');
                    tblWingRowRight = document.createElement("TR");
                    tblWingRowLeft = document.createElement("TR");

                    var rgDtos = cc.airRowGroupDTOs;
                    var winExitRightArr = new Array();
                    var winExitLeftArr = new Array();
                    for (var rgI = 0; rgI < rgDtos.length; rgI++) {
                        $('#trCC_' + ccI)
                            .append(
                                '<td><table cellspacing="0" cellpadding="0"><tbody class="tbSeatMap" id="tbodyRG_' + ccI + '_' + rgI + '"></tbody></table></td>');
                        var cgDtos = rgDtos[rgI].airColumnGroups;
                        var trArr = new Array();
                        /*
                         * array to contain td
                         * elements
                         */

                        /*
                         * additional windows required for letters rows - column
                         * grps
                         */
                        var winCell = {
                            desc: "&nbsp;",
                            css: 'window',
                            textCss: "font-size:9px;",
                            rowNum: 0,
                            align: "center"
                        };
                        winExitRightArr[winExitRightArr.length] = winCell;
                        winExitLeftArr[winExitLeftArr.length] = winCell;

                        isleRow = new Array();
                        var groupBreakLetters = {};
                        if (buildLetters) {
                            columnLetters = [];
                            colLtrStr = "";
                            var colGroupSizeArr = {};
                            for (var cgI = 0; cgI < cgDtos.length; cgI++) {
                                colGroupSizeArr[cgDtos[cgI].columnGroupId] = cgDtos[cgI].airRows[0].airSeats.length;
                                if (cgDtos[cgI].airRows.length > 0) {
                                    var seats = cgDtos[cgI].airRows[0].airSeats;
                                    var groupLetters = [];
                                    for (var sI = 0; sI < seats.length; sI++) {
                                        var colLetter = ((new String(
                                            seats[sI].seatNumber)).replace(
                                            /[\d]+/g, '')).toUpperCase();
                                        columnLetters[columnLetters.length] = colLetter;
                                        groupLetters[groupLetters.length] = colLetter;
                                    }
                                    groupLetters = $.airutil.sort.quickSort(
                                        groupLetters,
                                        $.airutil.string.comparator);
                                    groupBreakLetters[groupLetters[0]] = true;
                                }
                            }
                            columnLetters = $.airutil.sort.quickSort(
                                columnLetters, $.airutil.string.comparator);
                            colLtrStr = columnLetters.join("");
                            buildLetters = false;
                        }

                        var rowStatus = new Array();
                        for (var cgI = 0; cgI < cgDtos.length; cgI++) {
                            var rows = cgDtos[cgI].airRows;
                            /* initialize tr arrays */
                            if (cgI == 0) {
                                for (var k = 0; k < rows.length; k++) {
                                    trArr[k] = new Array();
                                }
                            }

                            for (var rI = 0; rI < rows.length; rI++) {
                                var lastSeatC = "matchCC";
                                var seats = rows[rI].airSeats;
                                var seatLogicalCC = "";
                                var rowIndex = rows[rI].rowNumber;
                                if (rowStatus[rowIndex] == undefined) {
                                    rowStatus[rowIndex] = true;
                                }

                                for (var sI = 0; sI < seats.length; sI++) {
                                    strDesc = "&nbsp;";
                                    seatLogicalCC = seats[sI].logicalCabinClassCode;
                                    if ($.inArray(
                                        seats[sI].logicalCabinClassCode,
                                        availLCCArray) == -1) {
                                        availLCCArray[availLCCArray.length] = seats[sI].logicalCabinClassCode;
                                    }
                                    if (!activeCC) {
                                        seats[sI].seatAvailability = "INALC";
                                        lastSeatC = "";
                                    } else {
                                        if (seatLogicalCC != selectedLogicalCC) {
                                            seats[sI].seatAvailability = "INALC";
                                            lastSeatC = "";
                                        }

                                    }
                                    if (seats[sI].seatVisibility == "N") {
                                        strSeatClass = "style=visibility:hidden;";
                                    } else {
                                        rowStatus[rowIndex] = false;
                                        switch (seats[sI].seatAvailability) {
                                        case "VAC":
                                            // var refObj =
                                            // '{segId:'+intSegRow+',ccId:'+ccI+',rgId:'+rgI+',cgId:'+cgI+',rId:'+rI+',sId:'+sI+'}';

                                            var refObj = '{fltRefNo:\"' + fltRefNo + '\",seatNumber:\"' + seats[sI].seatNumber + '\"}';
                                            strSeatClass = "td_seat matchCC availSeat cursorPointer";
                                            strDesc = "<a href='#' onmouseover='UI_tabAnci.seatOnMouseOver(event," + refObj + ")'  onmouseout='UI_tabAnci.seatOnMouseOut(" + refObj + ")' onclick='UI_tabAnci.seatOnClick(" + refObj + ")' class='txtSmall noTextDec'>&nbsp;&nbsp;&nbsp;&nbsp;<\/a>";
                                            break;
                                        case "INA":
                                            strSeatClass = "td_seat matchCC notAvailSeat";
                                            break;
                                        case "RES":
                                            strSeatClass = "td_seat matchCC notAvailSeat";
                                            break; // occupied
                                            // red
                                            // -
                                            // "SltdSeat"
                                        case "INALC": // occupied
                                            var refObj = '{fltRefNo:\"' + fltRefNo + '\",seatNumber:\"' + seats[sI].seatNumber + '\"}';
                                            // strDesc = "<a href='#'
                                            // onmouseover='UI_tabAnci.seatOnMouseOver(event,"+refObj+")'
                                            // onmouseout='UI_tabAnci.seatOnMouseOut("+refObj+")'
                                            // class='txtSmall
                                            // noTextDec'>&nbsp;&nbsp;&nbsp;&nbsp;<\/a>";
                                            strSeatClass = "td_seat notAvailSeat";
                                            break;
                                        default:
                                            strSeatClass = "td_seat matchCC notAvailSeat";
                                            break;
                                        }
                                    }

                                    var seatNo = seats[sI].seatNumber

                                    if (populateRefMap) {
                                        seatRefMap.seats[seatNo] = {
                                            indexRefObj: {
                                                segId: intSegRow,
                                                ccId: ccI,
                                                rgId: rgI,
                                                cgId: cgI,
                                                rId: rI,
                                                sId: sI
                                            },
                                            avail: seats[sI].seatAvailability,
                                            seatCharge: seats[sI].seatCharge,
                                            seatMessage: seats[sI].seatMessage,
                                            seatNumber: seatNo,
                                            fltRefNo: fltRefNo,
                                            lccCode: seats[sI].logicalCabinClassCode,
                                            seatType: seats[sI].seatType
                                        };
                                    }

                                    var cell = {
                                        desc: strDesc,
                                        id: "tdSeat_" + seatNo, // + "_"
                                        // +
                                        // objST[i].row[x].id,
                                        css: strSeatClass,
                                        rowNum: rows[rI].rowNumber,
                                        textCss: "font-size:9px;",
                                        align: "right",
                                        role: seatLogicalCC
                                    };
                                    var colLetter = ((new String(seatNo))
                                        .replace(/[\d]+/g, ''))
                                        .toUpperCase();
                                    var letterIndex = colLtrStr
                                        .indexOf(colLetter)
                                    var sdLtrIndex = letterIndex % seats.length;
                                    var tIndex = rI;
                                    var totCols = 0;
                                    for (var i = 0; i < cgDtos.length; i++) {
                                        totCols = totCols + cgDtos[i].airRows[0].airSeats.length;
                                    }
                                    var colIndex = (totCols - (letterIndex + 1));
                                    /*
                                     * F=0,
                                     * A=5
                                     */
                                    // row index and column index
                                    trArr[tIndex][colIndex] = cell;

                                    var blnFirstRow = (cgI == 0 && (colIndex == 0));
                                    var blnLastRow = (cgI == (cgDtos.length - 1)) && (colIndex == (totCols - 1));

                                    if (blnFirstRow || blnLastRow) {
                                        switch (seats[sI].seatType) {
                                        case "EXIT":
                                            strWindowClass = (blnFirstRow ? "exitRight" : "exitLeft");
                                            break;
                                        default:
                                            strWindowClass = "window";
                                            break;
                                        }
                                        var winExitCell = {
                                            desc: "&nbsp;",
                                            css: strWindowClass,
                                            textCss: "font-size:9px;",
                                            rowNum: rows[rI].rowNumber,
                                            align: "center"
                                        };
                                        if (blnFirstRow) {
                                            winExitRightArr[winExitRightArr.length] = winExitCell;
                                        } else {
                                            winExitLeftArr[winExitLeftArr.length] = winExitCell;
                                        }
                                    }

                                }

                                var rowNumber = rows[rI].rowNumber;
                                var isEmptyRow = rowStatus[rowNumber];
                                if (isEmptyRow) {
                                    rowNumber = '';
                                }
                                // creating the isle row
                                var cellIsle = {
                                    desc: (rowNumber),
                                    css: "aisle " + lastSeatC,
                                    textCss: "font-size:9px;",
                                    rowNum: rows[rI].rowNumber,
                                    align: "right"
                                };
                                isleRow[rI] = cellIsle;
                            }
                        } /* end of column grp loop */

                        /* populate table for rows per row group */
                        /* cols = = = */
                        /* rows | | | */

                        if (trArr.length > 0) {

                            trArr = $.airutil.sort.quickSort(trArr,
                                UI_tabAnci.seatRowComparator);
                            var colNos = trArr[0].length;
                            var rowNos = trArr.length;
                            var colsPerCGrp = parseInt(colNos / cgDtos.length,
                                10);
                            var letterIndex = colNos;
                            var colGrpCount = 1;
                            var colCount = 0;

                            /** adding seats and isles for row groups */
                            outer : for (var i = 0; i < colNos; i++) {
                                tblRow = document.createElement("TR");
                                if (i == colCount) {
                                    colsPerCGrp = colGroupSizeArr[colGrpCount];
                                    colCount = colCount + colsPerCGrp;
                                    colGrpCount++;
                                }
                                var lastLetter = '';

                                inner : for (var j = 0; j < rowNos; j++) {
                                    /*
                                     * booking class seat column letters print
                                     */
                                    if (j == 0 && letterIndex > 0) {
                                        lastLetter = columnLetters[--letterIndex];
                                        var letterCell = {
                                            desc: lastLetter,
                                            css: "aisle",
                                            textCss: "font-size:9px;font-weight:bold;color:red;",
                                            align: "right"
                                        };
                                        tblRow.appendChild(UI_commonSystem
                                            .createCell(letterCell));

                                    }
                                    /* adding seat columns to TR */
                                    if (trArr[j][i] != undefined) {
                                    	tdObj = UI_commonSystem.createCell(trArr[j][i]);
                                    } else {
                                    	continue inner;
                                    }
                                    tblRow.appendChild(tdObj);
                                }
                                $('#tbodyRG_' + ccI + '_' + rgI).append(tblRow);

                                /*
                                 * creating isles inbetweeen seat column grps
                                 */
                                if (groupBreakLetters[lastLetter] && i < colNos - 1) {
                                    isleRow = $.airutil.sort.quickSort(isleRow,
                                        UI_tabAnci.seatIsleComparator);
                                    isleRowtblRow = document
                                        .createElement("TR");
                                    isleRowtblRow.appendChild(UI_commonSystem
                                        .createCell(blankIsle)); // Row
                                    // Column
                                    // Letter
                                    for (var k = 0; k < rowNos; k++) {
                                        isleRowtblRow
                                            .appendChild(UI_commonSystem
                                                .createCell(isleRow[k]));
                                    }
                                    $('#tbodyRG_' + ccI + '_' + rgI).append(
                                        isleRowtblRow);
                                }
                            }
                        }

                    } /* end of row groups loop */

                    /* creating left wing windows */
                    winExitRightArr = $.airutil.sort.quickSort(winExitRightArr,
                        UI_tabAnci.seatIsleComparator);
                    for (var i = 0; i < winExitRightArr.length; i++) {
                        tblWingRowRight.appendChild(UI_commonSystem
                            .createCell(winExitRightArr[i]));
                    }
                    $('#trCCRW_' + ccI).append(tblWingRowRight);

                    /* creating right wing windows */
                    winExitLeftArr = $.airutil.sort.quickSort(winExitLeftArr,
                        UI_tabAnci.seatIsleComparator);
                    for (var i = 0; i < winExitLeftArr.length; i++) {
                        tblWingRowLeft.appendChild(UI_commonSystem
                            .createCell(winExitLeftArr[i]));
                    }
                    $('#trCCLW_' + ccI).append(tblWingRowLeft);

                } /* end of cabin class loop */

            } else { /* end of cabin classes len check */
                $("#spnAnciNoSeat").show();
            }

        } else { /* end of no segment */
            $("#spnAnciNoSeat").show();
        }
    }
    if (populateRefMap) {
        UI_tabAnci.seatRefMap[fltRefNo] = seatRefMap;
    }
    var setCabinCalssLabel = function () {
        var cabinClass = $(".cabinClass");
        var zIn = 9;
        for (var _cc = 0; _cc < cabinClass.length; _cc++) {
            var t_Cabin = $(cabinClass[_cc]);
            var firstRowSeats = t_Cabin.find(".tbSeatMap>tr:eq(1)").find(
                ".td_seat");
            // UI_tabAnci.lccMap[seatObj.lccCode].description

            for (var x = 0; x < availLCCArray.length; x++) {
                zIn++;
                var lebelLeft = 0;
                var showLable = false;
                for (var y = 0; y < firstRowSeats.length; y++) {
                    if ($(firstRowSeats[y]).attr("role") == availLCCArray[x]) {
                        lebelLeft = $(firstRowSeats[y]).position().left;
                        showLable = true;
                        break;
                    }
                }
                if (showLable) {

                    var clnam = UI_tabAnci.lccMap[availLCCArray[x]].description
                        .replace(/fare/gi, "Seat");
                    var divCC = $("<span class='labelLC' title='" + clnam + "'>" + clnam + "</span>");
                    divCC.css({
                        "position": "absolute",
                        "top": $("#tbdySeatMap").position().top - 16,
                        "padding": "2px 10px",
                        "border": "1px solid #777",
                        "border-bottom": "0px",
                        "background-color": "#fff",
                        "left": lebelLeft + 10,
                        "z-index": zIn
                    });
                    if (selectedLogicalCC == availLCCArray[x]) {
                        divCC.addClass("matchCC");
                        $(".wingRight").prepend(divCC);
                    }

                }
            }
        }

    };
    $(".labelLC").remove();
    if (availLCCArray.length > 1) {
        setCabinCalssLabel();;
    }
}

/*
 * Clear seat pax
 */
UI_tabAnci.clearSeatPax = function () {
    for (var i = 0; i < jsonPaxAdults.length; i++) {
        if (jsonPaxAdults[i].type != 'PA') {
            $("#tdPaxSeat_1_" + i).html(" ");
            $("#tdPaxSeat_2_" + i).html(" ");
        }
    }
}
/*
 * Populate seat map data
 */
UI_tabAnci.populateSMData = function (p) {
    if (occupiedSeats == null) {
        showERRMessage("Seat data is not accurate. Please reload!");
    }
    UI_tabAnci.clearSeatPax();
	    $.each(occupiedSeats[p.fltRefNo].seats, function(seatNumber, seatRefObj) {
		if (seatRefObj != null) {
			var seatObj = UI_tabAnci.getSeatObj(seatRefObj.refObj);
			if (seatObj != null) {
				if (seatObj.avail != 'RES' || UI_tabAnci.isAnciModify ) {

					var seatCharge = seatObj.seatCharge;
					var paxId = seatRefObj.paxId;
					$("#tdSeat_" + seatNumber).removeClass("availSeat");
					$("#tdSeat_" + seatNumber).addClass("SltdSeat");
					$("#tdPaxSeat_1_" + paxId).html(seatNumber);

					var formmattedAmount = $.airutil.format
							.currency(seatCharge);

					var applyFormatter = UI_tabAnci
							.isApplyFreeServiceDecorator(
									UI_tabAnci.promoAnciType.SEAT_MAP,
									formmattedAmount);
					var wrappedAmount = formmattedAmount;
					if (applyFormatter) {
						wrappedAmount = UI_tabAnci.freeServiceDecorator;
					}

					$("#tdPaxSeat_2_" + paxId).html(wrappedAmount);
				} else if(seatObj.avail == 'RES') {
					UI_tabAnci.clearSeat(seatRefObj);
				}
			}
		}
	});

    /*
	 * for(var i=0; i< occupiedSeats.length; i++){
	 * if(UI_tabAnci.compareFlightRefNo(occupiedSeats[i].fltRefNo, p.fltRefNo)){
	 * for(var j=0; j < occupiedSeats[i].seats.length; j++){ var strSeatNo =
	 * occupiedSeats[i].seats[j].seatNumber; var seatCharge =
	 * occupiedSeats[i].seats[j].seatCharge; var paxId =
	 * parseInt(occupiedSeats[i].seats[j].paxId,10); if(paxId >= 0){
	 * $("#tdSeat_" + strSeatNo ).removeClass("availSeat"); $("#tdSeat_" +
	 * strSeatNo ).addClass("SltdSeat"); $("#tdPaxSeat_1_" +
	 * paxId).html(strSeatNo); $("#tdPaxSeat_2_" + paxId).html(seatCharge); } } } }
	 */
}

function getFltSegMap(fltRefNo) {
    var seatRefMap = UI_tabAnci.seatRefMap[fltRefNo];
    if (seatRefMap == null) {
        // -------------------------------------------------------
        seatRefMap = {
            seats: {}
        };
        for (var intSegRow = 0; intSegRow < jsSMModel.length; intSegRow++) {
            if (UI_tabAnci.compareFlightRefNo(
                jsSMModel[intSegRow].flightSegmentDTO.flightRefNumber,
                fltRefNo)) {
                var cabinCls = jsSMModel[intSegRow].lccSeatMapDetailDTO.cabinClass;
                var ccLen = cabinCls.length;
                if (ccLen > 0) {
                    for (var ccI = 0; ccI < ccLen; ccI++) {
                        var cc = cabinCls[ccI];
                        var rgDtos = cc.airRowGroupDTOs;
                        for (var rgI = 0; rgI < rgDtos.length; rgI++) {
                            var cgDtos = rgDtos[rgI].airColumnGroups;
                            for (var cgI = 0; cgI < cgDtos.length; cgI++) {
                                var rows = cgDtos[cgI].airRows;
                                for (var rI = 0; rI < rows.length; rI++) {
                                    var seats = rows[rI].airSeats;
                                    for (var sI = 0; sI < seats.length; sI++) {

                                        var seatNo = seats[sI].seatNumber;

                                        seatRefMap.seats[seatNo] = {
                                            indexRefObj: {
                                                segId: intSegRow,
                                                ccId: ccI,
                                                rgId: rgI,
                                                cgId: cgI,
                                                rId: rI,
                                                sId: sI
                                            },
                                            avail: seats[sI].seatAvailability,
                                            seatCharge: seats[sI].seatCharge,
                                            seatMessage: seats[sI].seatMessage,
                                            seatNumber: seatNo,
                                            fltRefNo: fltRefNo,
                                            lccCode: seats[sI].logicalCabinClassCode,
                                            seatType: seats[sI].seatType
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        UI_tabAnci.seatRefMap[fltRefNo] = seatRefMap;
        // -------------------------------------------------
    }
    return seatRefMap;
}

/*
 * Get seat object for given reference
 */
UI_tabAnci.getSeatObj = function (refObj) {
    /*
     * var retSeatObj =
     * jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].airSeats[refObj.sId];
     * retSeatObj.segCode =
     * jsSMModel[refObj.segId].flightSegmentDTO.segmentCode; retSeatObj.fltRefNo =
     * jsSMModel[refObj.segId].flightSegmentDTO.flightRefNumber;
     */
    var seatRefMap = getFltSegMap(refObj.fltRefNo)
    var seatObj = seatRefMap.seats[refObj.seatNumber];
    if (seatObj == undefined || seatObj == null) {
        seatObj = {}
    } else {
        var currentSeatRef = UI_tabAnci.getSeatPriceFromPaxCurrentAnci(refObj);
        if (currentSeatRef.currentSeatExists) {
            seatObj.seatCharge = currentSeatRef.currentSeatCharge;
        }
    }
    return seatObj;
}

UI_tabAnci.getSeatPriceFromPaxCurrentAnci = function (seatRefObj) {
    if (UI_tabAnci.isAnciModify) {
        if (jsonPaxAdults != undefined && jsonPaxAdults != null) {
            for (var paxIndex = 0; paxIndex < jsonPaxAdults.length; paxIndex++) {
                var paxCurrentAnci = jsonPaxAdults[paxIndex].currentAncillaries
                if (paxCurrentAnci != undefined && paxCurrentAnci != null) {
                    for (var anciIndex = 0; anciIndex < paxCurrentAnci.length; anciIndex++) {
                        var anciObj = paxCurrentAnci[anciIndex];
                        if (anciObj.flightSegmentTO.flightRefNumber == seatRefObj.fltRefNo) {
                            var currentSeatObj = anciObj.airSeatDTO;
                            if (currentSeatObj != undefined && currentSeatObj != null && currentSeatObj.seatNumber == seatRefObj.seatNumber) {
                                return {
                                    currentSeatExists: true,
                                    currentSeatCharge: currentSeatObj.seatCharge
                                };
                            }
                        }
                    }
                }
            }
        }
    }

    return {
        currentSeatExists: false,
        currentSeatCharge: null
    };
}

/*
 * Get row object for given reference
 */
UI_tabAnci.getRowObj = function (seatRefObj) {
    var refObj = UI_tabAnci.getSeatObj(seatRefObj).indexRefObj;
    var retSeatObj = jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].airSeats;
    retSeatObj.fltRefNo = jsSMModel[refObj.segId].flightSegmentDTO.flightRefNumber;
    retSeatObj.segCode = jsSMModel[refObj.segId].flightSegmentDTO.segmentCode;
    return retSeatObj;
    /*
     * var retSeatObj =
     * jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].airSeats;
     * retSeatObj.segCode =
     * jsSMModel[refObj.segId].flightSegmentDTO.segmentCode; retSeatObj.fltRefNo =
     * jsSMModel[refObj.segId].flightSegmentDTO.flightRefNumber; return
     * retSeatObj;
     */
}

UI_tabAnci.getSeatRefObj = function (seatCode, fltRefNo) {
    var seatRef = null;
    outerMost: for (var i = 0; i < jsSMModel.length; i++) {
        if (UI_tabAnci.compareFlightRefNo(
            jsSMModel[i].flightSegmentDTO.flightRefNumber, fltRefNo)) {
            var cabinCls = jsSMModel[i].lccSeatMapDetailDTO.cabinClass;
            for (var ccI = 0; ccI < cabinCls.length; ccI++) {
                var cc = cabinCls[ccI];
                var rgDtos = cc.airRowGroupDTOs;
                for (var rgI = 0; rgI < rgDtos.length; rgI++) {
                    var cgDtos = rgDtos[rgI].airColumnGroups;
                    for (var cgI = 0; cgI < cgDtos.length; cgI++) {
                        var rows = cgDtos[cgI].airRows;
                        for (var rI = 0; rI < rows.length; rI++) {
                            var seats = rows[rI].airSeats;
                            for (var sI = 0; sI < seats.length; sI++) {
                                if (seats[sI].seatNumber == seatCode) {
                                    seatRef = {
                                        segId: i,
                                        ccId: ccI,
                                        rgId: rgI,
                                        cgId: cgI,
                                        rId: rI,
                                        sId: sI
                                    };
                                    break outerMost;
                                }

                            }
                        }
                    }
                }
            }
        }
    }
    return seatRef;
}
/*
 * Get segment code for given reference
 */
UI_tabAnci.getSeatFltRefNo = function (refObj) {
    return jsSMModel[refObj.segId].flightSegmentDTO.flightRefNumber;
}

/*
 * Get seat availability breakdown per row for given reference
 */
UI_tabAnci.getSeatAvailObjForRow = function (seatRefObj) {
    var refObj = UI_tabAnci.getSeatObj(seatRefObj).indexRefObj;
    var rowSeatAvailObj = jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].passengerTypeQuantity;
    rowSeatAvailObj.segCode = jsSMModel[refObj.segId].flightSegmentDTO.segmentCode;
    rowSeatAvailObj.fltRefNo = jsSMModel[refObj.segId].flightSegmentDTO.flightRefNumber;
    return rowSeatAvailObj;
}

/*
 * Update seat availability
 */
UI_tabAnci.updateSeatAvailRow = function (seatRefObj, updateType) {
    var refObj = UI_tabAnci.getSeatObj(seatRefObj.refObj).indexRefObj;
    var paxType = seatRefObj.paxType;
    var paxTypeHash = {};
    paxTypeHash[paxType] = true;
    if (paxType == 'PA') {
        paxTypeHash['IN'] = true;
    }
    var rowSeatAvailObj = jsSMModel[refObj.segId].lccSeatMapDetailDTO.cabinClass[refObj.ccId].airRowGroupDTOs[refObj.rgId].airColumnGroups[refObj.cgId].airRows[refObj.rId].passengerTypeQuantity;
    for (var i = 0; i < rowSeatAvailObj.length; i++) {
        if (paxTypeHash[rowSeatAvailObj[i].paxType] != null && paxTypeHash[rowSeatAvailObj[i].paxType]) {
            if (updateType == 'INC') {
                rowSeatAvailObj[i].quantity = parseInt(
                    rowSeatAvailObj[i].quantity, 10) + 1;
            } else if (updateType == 'DEC') {
                rowSeatAvailObj[i].quantity = parseInt(
                    rowSeatAvailObj[i].quantity, 10) - 1;
            }
            break;
        }
    }
}
/*
 * Seat availability check @return true if seat is occupied false otherwise
 */
UI_tabAnci.isSeatOccupied = function (seatObjRef) {
    if (occupiedSeats[seatObjRef.fltRefNo].seats[seatObjRef.seatNumber] != null) {
        return true;
    } else {
        return false;
    }
}

UI_tabAnci.isSatisfySeatAssignementConstrains = function (seatObjRef) {
    var rowNumber = seatObjRef.rowNumber;
    if (rowNumber >= top.autoSeatAssignmentStartingRowNumber) {
        return true;
    }
    return false;
}

UI_tabAnci.seatsForBlocking = function () {
    var occSeats = occupiedSeats;
    var outArr = [];
    $.each(occupiedSeats, function (fltRefNo, occObj) {
        var seatArr = [];
        $.each(occObj.seats, function (seatNumber, seatObj) {
            if (seatObj != null) {
                seatArr[seatArr.length] = {
                    seatNo: seatNumber,
                    paxType: seatObj.paxType
                };
            }
        });
        if (seatArr.length > 0) {
            outArr[outArr.length] = {
                fltRefNo: fltRefNo,
                seats: seatArr
            };
        }
    });
    return $.toJSON(outArr);
}
/*
 * Get occuppied seat reference object
 */
UI_tabAnci.getOccupiedSeat = function (fltPaxObj) {
    var seat = null;
    $.each(occupiedSeats[fltPaxObj.fltRefNo].seats,
        function (seatNumber, seatObj) {
            if (seatObj != null && parseInt(seatObj.paxId, 10) == parseInt(
                fltPaxObj.paxId, 10)) {
                seat = seatObj;
                return;
            }
        });
    return seat;
    /*
     * var fltRefNo = UI_tabAnci.currFltRefNo;
     *
     * for(var i=0; i< occupiedSeats.length; i++){
     * if(UI_tabAnci.compareFlightRefNo(occupiedSeats[i].fltRefNo, fltRefNo)){
     * for(var j=0; j< occupiedSeats[i].seats.length; j++){
     * if(parseInt(occupiedSeats[i].seats[j].paxId,10) ==
     * parseInt(seatObjRef.paxId,10) ){ return occupiedSeats[i].seats[j].refObj; } } } }
     * return null;
     */
}
UI_tabAnci.markAutomaticCheckinSeat = function (paxAutoCheckinObj) {
	var fltRefNo = paxAutoCheckinObj.refObj.fltRefNo;
	var index = jsAnciSegRefMap[fltRefNo].index;
	var paxId = paxAutoCheckinObj.paxId;
	var paxDetails = UI_tabAnci.getAutoCheckinAdultDetails();
	
	if((UI_tabAnci.isAnciModify || UI_tabAnci.isRequote) && (paxAutoCheckinObj.automaticCheckinDTO != undefined && paxAutoCheckinObj.automaticCheckinDTO.length > 0) ){
		jsAnciSeg[index].pax[paxId].automaticCheckin.seatPreference = paxAutoCheckinObj.automaticCheckinDTO[0].seatPref;
		jsAnciSeg[index].pax[paxId].automaticCheckin.email = paxAutoCheckinObj.automaticCheckinDTO[0].email;
	}else{
		if(jsAnciSeg[index].pax[paxId].seat.seatNumber != ""){
			jsAnciSeg[index].pax[paxId].automaticCheckin.seatCode = paxDetails[paxId].displaySeatCode;
		}else{
			if($("#autoCheck_"+(paxId+1)).is(":checked")){
				jsAnciSeg[index].pax[paxId].automaticCheckin.seatPreference = 'S';
			}else{
				jsAnciSeg[index].pax[paxId].automaticCheckin.seatPreference = paxDetails[paxId].displaySeatPref;
			}
		}
		jsAnciSeg[index].pax[paxId].automaticCheckin.email = paxDetails[paxId].emailAddress;
	}
	
	if(jsAnciSeg[index].pax[paxId].automaticCheckin.autoCheckinTemplateId != jsAutoCheckinModel[index].automaticCheckinTemplateId){
		jsAnciSeg[index].pax[paxId].automaticCheckin.autoCheckinTemplateId = jsAutoCheckinModel[index].automaticCheckinTemplateId;
		jsAnciSeg[index].pax[paxId].automaticCheckin.autoCheckinCharge = jsAutoCheckinModel[index].amount ;
		UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.AUTOMATICCHECKIN, jsAutoCheckinModel[index].amount,'ADD', paxId,paxAutoCheckinObj.old);
		UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.AUTOMATICCHECKIN, 1, 'ADD',paxAutoCheckinObj.old);
		UI_tabAnci.updatepaxTotals(paxId, jsAutoCheckinModel[index].amount, 'ADD');
	}
}
UI_tabAnci.checkSeatCodeForAutoCheckin = function () {
	for(var i=0;i < jsAnciSeg.length;i++){
		for(var j=0;j < jsAnciSeg[i].pax.length;j++){
			if((jsAnciSeg[i].pax[j].seat.seatNumber != '') && (jsCurrAnciSeg[i].pax[j].automaticCheckin.autoCheckinTemplateId != null && 
					jsCurrAnciSeg[i].pax[j].automaticCheckin.autoCheckinTemplateId != '' &&
					jsCurrAnciSeg[i].pax[j].automaticCheckin.autoCheckinTemplateId != 0)) {
				jsAnciSeg[i].pax[j].automaticCheckin.seatCode = jsAnciSeg[i].pax[j].seat.seatNumber;
				jsAnciSeg[i].pax[j].automaticCheckin.seatPreference = '';
			}
		}
	}
}
/*
 * Mark the seat occupied
 */
UI_tabAnci.markSeat = function (paxSeatObj) {
    // var fltRefNo = UI_tabAnci.getSeatFltRefNo(paxSeatObj);

    var fltRefNo = paxSeatObj.refObj.fltRefNo;
    var seatNumber = paxSeatObj.refObj.seatNumber;
    var seatObj = UI_tabAnci.getSeatObj(paxSeatObj.refObj);

    UI_tabAnci.updateSeatAvailRow(paxSeatObj, 'DEC');

    var index = jsAnciSegRefMap[fltRefNo].index;
    jsAnciSeg[index].pax[paxSeatObj.paxId].seat.seatNumber = seatObj.seatNumber;
    jsAnciSeg[index].pax[paxSeatObj.paxId].seat.seatCharge = seatObj.seatCharge;

    UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.SEAT, seatObj.seatCharge,
        'ADD', paxSeatObj.paxId, paxSeatObj.old);
    UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.SEAT, 1, 'ADD',
        paxSeatObj.old);
    UI_tabAnci.updatepaxTotals(paxSeatObj.paxId, seatObj.seatCharge, 'ADD');

    occupiedSeats[fltRefNo].seats[seatNumber] = paxSeatObj;

}

UI_tabAnci.clearModifyAutoCheck = function (paxAutomaticCheckinSeatObj) {
var fltRefNo = paxAutomaticCheckinSeatObj.refObj.fltRefNo;
	var index = jsAnciSegRefMap[fltRefNo].index;
	for (var i = 0; i < jsonPaxAdults.length; i++) {
		 var paxAutoCheckinObj = UI_tabAnci.autoCheckAnciOldObj(paxAutomaticCheckinSeatObj,i); 
			if (paxAutoCheckinObj.old) {
		        jsCurrAnciSeg[index].pax[i].automaticCheckin.autoCheckinCharge =paxAutoCheckinObj.automaticCheckin.automaticCheckinCharge;
		        jsCurrAnciSeg[index].pax[i].automaticCheckin.autoCheckinTemplateId = paxAutoCheckinObj.automaticCheckin.autoCheckinId;
				jsCurrAnciSeg[index].pax[i].automaticCheckin.email=paxAutoCheckinObj.automaticCheckin.email;
				jsCurrAnciSeg[index].pax[i].automaticCheckin.seatCode=paxAutoCheckinObj.automaticCheckin.seatCode;
				jsCurrAnciSeg[index].pax[i].automaticCheckin.seatPreference=paxAutoCheckinObj.automaticCheckin.seatPref;


				jsAnciSeg[index].pax[i].automaticCheckin.autoCheckinCharge = 0;
				jsAnciSeg[index].pax[i].automaticCheckin.autoCheckinTemplateId = 0;
				jsAnciSeg[index].pax[i].automaticCheckin.seatPreference = "";
				jsAnciSeg[index].pax[i].automaticCheckin.email = "";
				UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.AUTOMATICCHECKIN,jsAutoCheckinModel[index].amount, 'DEDUCT', paxAutomaticCheckinSeatObj.paxId, paxAutoCheckinObj.old);
				UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.AUTOMATICCHECKIN, 1, 'DEDUCT',paxAutoCheckinObj.old);
				UI_tabAnci.updatepaxTotals(paxAutomaticCheckinSeatObj.paxId, jsAutoCheckinModel[index].amount,'DEDUCT');
			
			}
		}
	}

UI_tabAnci.clearAutomaticCheckin = function (paxAutomaticCheckinSeatObj) {
	var fltRefNo = paxAutomaticCheckinSeatObj.refObj.fltRefNo;
	var index = jsAnciSegRefMap[fltRefNo].index;
	for (var i = 0; i < jsonPaxAdults.length; i++) {
		if(jsAnciSeg[index].pax[i].automaticCheckin.autoCheckinTemplateId != 0){
							jsAnciSeg[index].pax[i].automaticCheckin.autoCheckinCharge = 0;
				jsAnciSeg[index].pax[i].automaticCheckin.autoCheckinTemplateId = 0;
				jsAnciSeg[index].pax[i].automaticCheckin.seatPreference = "";
				jsAnciSeg[index].pax[i].automaticCheckin.email = "";
				UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.AUTOMATICCHECKIN,jsAutoCheckinModel[index].amount, 'DEDUCT', paxAutomaticCheckinSeatObj.paxId, paxAutomaticCheckinSeatObj.old);
				UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.AUTOMATICCHECKIN, 1, 'DEDUCT',paxAutomaticCheckinSeatObj.old);
				UI_tabAnci.updatepaxTotals(paxAutomaticCheckinSeatObj.paxId, jsAutoCheckinModel[index].amount,'DEDUCT');
			}	
		}
	}


/*
 * Clear the seat
 *
 * @return true on successful removal false otherwise
 *
 */
UI_tabAnci.clearSeat = function (seatObjRef) {
    // var fltRefNo = UI_tabAnci.getSeatFltRefNo(seatObjRef);
    var fltRefNo = seatObjRef.refObj.fltRefNo;
    var seatNumber = seatObjRef.refObj.seatNumber;
    var occSeat = occupiedSeats[fltRefNo].seats[seatNumber];
    if (occSeat != null) {
        var seatObj = UI_tabAnci.getSeatObj(seatObjRef.refObj);

        UI_tabAnci.updateSeatAvailRow(seatObjRef, 'INC');

        var index = jsAnciSegRefMap[fltRefNo].index;
        if (seatObjRef.old) {
            jsCurrAnciSeg[index].pax[seatObjRef.paxId].seat.seatNumber = seatObj.seatNumber;
            jsCurrAnciSeg[index].pax[seatObjRef.paxId].seat.seatCharge = jsAnciSeg[index].pax[seatObjRef.paxId].seat.seatCharge;
        }
        jsAnciSeg[index].pax[seatObjRef.paxId].seat.seatNumber = '';
        jsAnciSeg[index].pax[seatObjRef.paxId].seat.seatCharge = 0;

        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.SEAT,
            seatObj.seatCharge, 'DEDUCT', seatObjRef.paxId, seatObjRef.old);
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.SEAT, 1, 'DEDUCT',
            seatObjRef.old);
        UI_tabAnci.updatepaxTotals(seatObjRef.paxId, seatObj.seatCharge,
            'DEDUCT');

        occupiedSeats[fltRefNo].seats[seatNumber] = null;
        return true;
    } else {
        return false;
    }

}

/*
 * Pax has infant?
 *
 * @return true if passenger id has infant associated with it otherwise false
 */
UI_tabAnci.paxHasInfant = function (paxId) {
    if (jsonPaxAdults == null) {
        return false;
    }
    var pax = jsonPaxAdults[parseInt(paxId, 10)];
    if (pax.displayInfantTravelling == 'Y') {
        return true;
    } else {
        return false;
    }
}

/*
 * Check seat for infant seating
 */
UI_tabAnci.canSeatInfant = function (refObj, paxId) {
    var rowAvail = UI_tabAnci.getSeatAvailObjForRow(refObj);
    for (var i = 0; i < rowAvail.length; i++) {
        if (rowAvail[i].paxType == 'IN') {
            if (parseInt(rowAvail[i].quantity, 10) > 0) {
                return true;
            } else if (parseInt(rowAvail[i].quantity, 10) == 0) {
                var seatRef = UI_tabAnci.getOccupiedSeat({
                    paxId: paxId,
                    fltRefNo: refObj.fltRefNo
                });
                if (seatRef == null)
                    break;
                // var seatObj = UI_tabAnci.getSeatObj(refObj);
                var seats = UI_tabAnci.getRowObj(refObj);
                for (j = 0; j < seats.length; j++) {
                    if (seats[j].seatNumber == seatRef.refObj.seatNumber) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

/*
 * Seat Mouse Over
 *
 * refobj keys - segId,ccId,rgId,cgId,rid,sId
 */
UI_tabAnci.seatOnMouseOver = function (objE, refObj) {

    var x = 0
    var y = 0;
    if (objE != null) {
        if ($.browser.msie) {
            x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
            y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
        } else {
            if ((navigator.userAgent.indexOf("Safari") != -1) || (navigator.userAgent.indexOf("Opera") != -1)) {
                x = objE.clientX;
                y = objE.clientY;
            } else {
                x = objE.clientX + window.scrollX;
                y = objE.clientY + window.scrollY;
            }
        }
    }
    $("#spnSeatInfo").show();
    $("#spnSeatInfo").css({
        "left": (x + 10) + "px"
    });
    $("#spnSeatInfo").css({
        "top": (y + 10) + "px"
    });

    var seatObj = UI_tabAnci.getSeatObj(refObj);
    var selLogicalCC = UI_tabAnci.getSelectedLogicalCabinClass(refObj.fltRefNo);
    var strSeatNo = seatObj.seatNumber;
    var strPrice = seatObj.seatCharge;
    var strLccName = UI_tabAnci.lccMap[seatObj.lccCode].description;
    if (selLogicalCC == seatObj.lccCode) {
        // FIX : fix the currency code display for modifications
        var displayCurrency = UI_tabAnci.jsonFareQuoteSummary.currency;
        if (displayCurrency == null || displayCurrency == 'null') {
            displayCurrency = DATA_ResPro.initialParams.baseCurrencyCode;
        }
        $("#spnTTSeatNo").html(strSeatNo);       
                
		var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.SEAT_MAP,strPrice);
		if(applyFormatter){
			$("#spnTTSeatChg").html(UI_tabAnci.freeServiceDecorator);
		}else{
			$("#spnTTSeatChg").html(
		            $.airutil.format.currency(strPrice) + " " + displayCurrency);
		}		      
        
        if (!UI_tabAnci.isSeatOccupied(refObj)) {
            $("#tdSeat_" + strSeatNo).removeClass("availSeat");
            $("#tdSeat_" + strSeatNo).addClass("SltdSeat");
        }
    } else {
        $("#spnTTSeatNo").html(strSeatNo);
        $("#spnTTSeatChg").html(strLccName);
    }
}

/*
 * Seat On Mouse out
 *
 */
UI_tabAnci.seatOnMouseOut = function (refObj) {
    $("#spnSeatInfo").hide();

    var seatObj = UI_tabAnci.getSeatObj(refObj);

    var strSeatNo = seatObj.seatNumber;

    if (!UI_tabAnci.isSeatOccupied(refObj)) {
        $("#tdSeat_" + strSeatNo).removeClass("SltdSeat");
        $("#tdSeat_" + strSeatNo).addClass("availSeat");
    }
}

UI_tabAnci.seatOnClick = function (refObj) {
    if (UI_tabAnci.validateApply()) {

        if (!UI_tabAnci.isSeatOccupied(refObj)) {
            var seatObj = UI_tabAnci.getSeatObj(refObj);
            var seatMessage = seatObj.seatMessage;
            var strSeatNo = refObj.seatNumber;
            if (typeof seatMessage != 'undefined' && seatMessage != null && seatMessage.length > 0) {

                // append a '.' because in confirm dialog, the text is shown as
                // a one line string.
                seatMessage = $.trim(seatMessage);
                if (seatMessage.length > 0) {
                    if (seatMessage.charAt(seatMessage.length - 1) != '.') {
                        seatMessage += '.';
                    }
                }
                var seatSelected = confirm(seatMessage + ' \n\nDo you want to select this seat?');
                if (seatSelected) {
                    UI_tabAnci.seatOnClickSelection(refObj);
                } else {
                    return;
                }

            } else {
                UI_tabAnci.seatOnClickSelection(refObj);
            }
        }
        return false;
    }
}

/*
 * Seat On Click Selection
 *
 * refobj keys - segId,ccId,rgId,cgId,rid,sId
 */
UI_tabAnci.seatOnClickSelection = function (refObj) {
    if (UI_tabAnci.validateApply()) {

        if (!UI_tabAnci.isSeatOccupied(refObj)) {
            var seatObj = UI_tabAnci.getSeatObj(refObj);
            var strSeatNo = seatObj.seatNumber;
            var seatCharge = seatObj.seatCharge;
            var paxType = jsonPaxAdults[UI_tabAnci.currPaxIDRow].type;

            /* derive Infant pax */
            if (UI_tabAnci.paxHasInfant(UI_tabAnci.currPaxIDRow)) {
                if (!UI_tabAnci.canSeatInfant(refObj, UI_tabAnci.currPaxIDRow) || seatObj.seatType == "EXIT") {
                    /*
                     * TODO load error from error codes using raise Error
                     */
                    showERRMessage("This seat cannot be booked by a passenger traveling with infant");
                    return;
                }
            }

            var rObj = {
                paxId: UI_tabAnci.currPaxIDRow,
                fltRefNo: refObj.fltRefNo
            };
            var mObj = {
                paxId: UI_tabAnci.currPaxIDRow,
                paxType: paxType,
                refObj: refObj,
                old: false
            };
            UI_tabAnci.removeSeatPax(rObj);
            UI_tabAnci.markSeat(mObj);
            
            var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(UI_tabAnci.promoAnciType.SEAT_MAP,seatCharge);
            var seatChargeFrmt = $.airutil.format.currency(seatCharge);
    		if(applyFormatter){    			
    			seatChargeFrmt = UI_tabAnci.freeServiceDecorator;
    		}

            $("#tdPaxSeat_1_" + UI_tabAnci.currPaxIDRow).html(strSeatNo);
            $("#tdPaxSeat_2_" + UI_tabAnci.currPaxIDRow).html(seatChargeFrmt);
            $("#tdSeat_" + strSeatNo).removeClass("availSeat");
            $("#tdSeat_" + strSeatNo).addClass("SltdSeat");

        }
    }
}

/*
 * Seat Pax
 */
UI_tabAnci.buildSeatPax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciSeatPax").find("tr").remove();

    UI_tabAnci.currPaxIDRow = null;
    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    if (intLen > 0) {
        do {
            strClass = " whiteBackGround";
            strTxtStyle = "";
            if (i % 2 != 0) {
                strClass = " alternateRow";
            }

            tblRow = document.createElement("TR");
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: objPax[i].name,
                id: "tdPaxSeat_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight cursorPointer",
                click: UI_tabAnci.tdSeatPaxOnClick,
                align: "left"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxSeat_1_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }))
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxSeat_2_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }))

            var seatRef = '{paxId:' + objPax[i].paxId + ',fltRefNo:\"' + p.fltRefNo + '\"}';
            tblRow
                .appendChild(UI_commonSystem
                    .createCell({
                        desc: "<a href='#' class='noTextDec' onclick='UI_tabAnci.removeSeatPax(" + seatRef + "); return false;'><img src='" + UI_commonSystem.strImagePathAA + "Close_no_cache.jpg' border='0' title='Remove seat'>",
                        css: UI_commonSystem.strDefClass + strClass,
                        align: "center"
                    }))
            $("#tbdyAnciSeatPax").append(tblRow);
            i++;
        } while (--intLen);
    }      
}
/*
 * Reset Pax Seat
 */
UI_tabAnci.resetSeatPax = function () {
	 var objPax = jsonPaxAdults;
	 var intLen = objPax.length;
	 var fltRefNum = UI_tabAnci.currFltRefNo;
		
	 for(var i=0; i<intLen; i++){
		 var flightRefId = {
				 paxId: objPax[i].paxId,
				 fltRefNo:fltRefNum
	 	 };
	 	UI_tabAnci.removeSeatPax(flightRefId);
	 }
}
/*
 * Remove Pax Seat
 */
UI_tabAnci.removeSeatPax = function (refObj) {
    $("#tdPaxSeat_1_" + refObj.paxId).html(" ");
    $("#tdPaxSeat_2_" + refObj.paxId).html(" ");
    var seatRefObj = UI_tabAnci.getOccupiedSeat(refObj);
    if (seatRefObj == null) {
        return;
    }
    UI_tabAnci.clearSeat(seatRefObj);

    // var seatObj = UI_tabAnci.getSeatObj(seatRefObj.refObj);
    var strSeatNo = seatRefObj.refObj.seatNumber;

    $("#tdSeat_" + strSeatNo).removeClass("SltdSeat");
    $("#tdSeat_" + strSeatNo).addClass("availSeat");

}

/*
 * Passenger Selection
 */
UI_tabAnci.tdSeatPaxOnClick = function () {
    // 0 - tdPaxSeat;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxSeat_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxSeat_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxSeat_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxSeat_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");
}

// ------------------------------------------------------------ Hala
// -------------------------------------------------

/**
 * INSURANCE start
 */
UI_tabAnci.buildInsurancePax = function (p) {
    /* Initialize the Details */
    $("#tbdyAnciInsPax").find("tr").remove();

    var tblRow = null;
    var strClass = "";
    var strDesc = "";
    var strTDClick = "";
    var strTxtStyle = "";
    var objPax = jsonPaxAdults;
    var intLen = objPax.length;
    var i = 0;
    strClass = " whiteBackGround";
    if (intLen > 0) {
        do {
            tblRow = document.createElement("TR");
            var name = objPax[i].name;
            if (!UI_tabAnci.allowInfantInsurance) {
                name = name.split("/INF")[0];
            }
            tblRow.appendChild(UI_commonSystem.createCell({
                desc: name,
                id: "tdPaxIns_0_" + i,
                css: UI_commonSystem.strDefClass + strClass + " rowHeight ",
                align: "left"
            }));

            tblRow.appendChild(UI_commonSystem.createCell({
                desc: " ",
                id: "tdPaxIns_1_" + i,
                css: UI_commonSystem.strDefClass + strClass,
                align: "center"
            }))

            /*
             * tblRow.appendChild(UI_commonSystem.createCell({desc: '',
             * css:UI_commonSystem.strDefClass + strClass, align:"center"}))
             */
            $("#tbdyAnciInsPax").append(tblRow);

            i++;
        } while (--intLen);
    }
}
/*
 * Build Insurance
 */
UI_tabAnci.buildInsurance = function (insObj) {
	$('#spnInsCost').html("");
    UI_tabAnci.buildInsuranceProducts(insObj);
    
    if ((UI_tabAnci.isSegModify || UI_tabAnci.isRequote) && UI_tabAnci.dataStatus.insurance ) {
      //  UI_tabAnci.insOnClick();
    }
}

UI_tabAnci.buildInsuranceProducts = function(products) {
    UI_tabAnci.anciInsurances = new Array();
    $("#tblInsProducts").find("tr").remove();
    var rank = -1;
    var groupID = -1;
    var subGroupID = -1;

    var productCount = 1;

    var insHtml = "";
    
    $.each(products, function(pid, p) {

        var cRank = p.rank;
        var cGroupID = p.groupID;
        var cSubGroupID = p.subGroupID;
       // var cDisplayName = p.displayName;
        var cDisplayName = "";

        var validProduct = true;
        var dummyProduct = false;
        
        var insCurrency = UI_tabAnci.getDefaultCurrency();
        if (insCurrency == null) {
        	insCurrency = p.quotedCurrencyCode;
        }
        
        if (cRank > rank) {
            dummyProduct = true;
            insHtml = insHtml + "<tr id='insGroup_" + cRank + "_" + cSubGroupID + "'><td><b><label id='lblAddInsConf'>Travel Insurance</label></b></td></tr>";
            insHtml = insHtml + "<tr><td><input type='radio' id='insProduct_" + productCount + "' name='group_" + cRank + "_" + cGroupID + "' onClick='UI_tabAnci.updateInsurance()'>NONE<td></tr>";
        } else if (cRank == rank) {
            if (cGroupID > groupID) {
                dummyProduct = true;
                insHtml = insHtml + "<tr><td class='rowHeight'></td></tr>";
                insHtml = insHtml + "<tr id='insGroup_" + cRank + "_" + cSubGroupID + "'><td><b><label id='lblAddInsConf'>Missed Flight Insurance</label></b></td></tr>";
                
                if(UI_tabAnci.insContent != "undefined" && UI_tabAnci.insContent != "" ){
                	insHtml = insHtml + "<tr><td><p>" + UI_tabAnci.insContent + "</p></td></tr> ";
                }
                
                insHtml = insHtml + "<tr><td><input type='radio' id='insProduct_" + productCount + "' name='group_" + cRank + "_" + cGroupID + "' onClick='UI_tabAnci.updateInsurance()'>NONE<td></tr>";
            }
        } else {
            validProduct = false;
        }

        if (validProduct) {

            if (dummyProduct) {
                productCount++;
            }
            
            var content= cDisplayName;
            if(p.insuranceExternalContent.PLAN_TITEL != "undefined" && p.insuranceExternalContent.PLAN_TITEL != ""){
            	content += p.insuranceExternalContent.PLAN_TITEL.toUpperCase();
            }else{
            	content = p.displayName;
            }

            var insAmountStr = $.airutil.format.currency(p.quotedTotalPremiumAmount) + ' ' + insCurrency;
            content += " for  <b>" +  insAmountStr + "</b>";
            
            if(p.insuranceExternalContent.PLAN_DESC != "undefined" && p.insuranceExternalContent.PLAN_DESC != ""){
            	content += " <i>" + UI_tabAnci.removeHTMLTags(p.insuranceExternalContent.PLAN_DESC) + "</i>";
            }
            
            insHtml = insHtml + "<tr><td><input type='radio' id='insProduct_" + productCount + "' name='group_" + cRank + "_" + cGroupID + "' onClick='UI_tabAnci.updateInsurance()'>" + content + "<td></tr>";
            var anciInsurance = {
                productId: productCount,
                total: 0,
                selected: false,
                insuranceRefNumber: p.insuranceRefNumber,
                operatingAirline: p.operatingAirline,
                insuredJourney: p.insuredJourney,
                quotedTotalPremiumAmount: p.quotedTotalPremiumAmount
            };
            UI_tabAnci.anciInsurances.push(anciInsurance);
            productCount++;
        }

        rank = cRank;
        groupID = cGroupID;
        subGroupID = cSubGroupID;
    });

    $("#tblInsProducts").append(insHtml);

}

UI_tabAnci.removeHTMLTags = function (t){
	var tempStr = "";
	try{
		tempStr = t.replace(/<(?:.|\n)*?>/gm, '');
	}catch (e) {
		tempStr = "";
	}
  return  tempStr;
}

/**
 * Update Insurance
 */
UI_tabAnci.updateInsurance = function (){
	UI_tabAnci.removeInsurance();
	
	var selectedProducts = new Array();
	$("#tblInsProducts").find("input[type='radio']:checked").filter ( function () {
		   selectedProducts.push((this.id).split("_")[1]);
	});
	
	if (selectedProducts.length >0){
		UI_tabAnci.addInsurance(selectedProducts);
	}
}

/**
 * Remove Insurance
 */
UI_tabAnci.removeInsurance = function () {
	if ( UI_tabAnci.anciInsurances != null ){
		var removeInsurance = false;
		var insRemovalAmount = 0;
		
		$.each(UI_tabAnci.anciInsurances, function(k, p) {
			if(p.selected){
				p.selected =false;
				insRemovalAmount +=	p.quotedTotalPremiumAmount;
				removeInsurance = true;
			}
		});
		
		if(removeInsurance){
			UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.INS, insRemovalAmount, 'DEDUCT');
			UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.INS, 1, 'DEDUCT');
			UI_tabAnci.removeInsFromAll();
		}
	}
}

UI_tabAnci.removeInsPax = function (paxId) {
    var paxIns = jsonPaxAdults[paxId].ins;
    if (paxIns.ins) {
        UI_tabAnci.updatepaxTotals(paxId, paxIns.charge, 'DEDUCT');
        paxIns.ins = false;
        paxIns.charge = 0;
        if (UI_tabAnci.anciInsuranceTCCheck == undefined && !UI_tabAnci.anciInsuranceTCCheck) {
            $('#chkAnciIns').attr('checked', false);
        }

    }

    $("#tdPaxIns_1_" + paxId).html(" ");
}

UI_tabAnci.removeInsFromAll = function () {
    for (var i = 0; i < jsonPaxAdults.length; i++) {
        UI_tabAnci.removeInsPax(i);
    }
}

/**
 * Add Insurance
 */
UI_tabAnci.addInsurance = function (productIds) {
	var amountToAdd = 0;
	var addInsurance = false;
	
	$.each(productIds, function(sIndex, sProductId) {
		$.each(UI_tabAnci.anciInsurances, function(iPid, iP) {
			if( sProductId == iP.productId ){
				iP.selected = true;
				addInsurance = true;
				amountToAdd +=	iP.quotedTotalPremiumAmount;
			}
		});
	});

    if (addInsurance) {
        // UI_tabAnci.addInsPax(UI_tabAnci.currPaxIDRow,parseFloat(ins));
        var total = parseFloat(amountToAdd);
        UI_tabAnci.addInsToAll(parseFloat(total));
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.INS, total, 'ADD');
        UI_tabAnci.updateAnciCounts(UI_tabAnci.anciType.INS, 1, 'ADD');
    } 
    
    var insPaidCurrency = UI_tabAnci.getDefaultCurrency();
    if (insPaidCurrency == null) {
        insPaidCurrency = insObj.quotedCurrencyCode;
    }
    $('#spnInsCost').html( $.airutil.format.currency(amountToAdd) + ' ' + insPaidCurrency);
}

UI_tabAnci.addInsPax = function (paxId, ins) {
    var paxIns = jsonPaxAdults[paxId].ins;
    if (!paxIns.ins) {
        paxIns.ins = true;
        paxIns.charge = ins;
        UI_tabAnci.updatepaxTotals(paxId, ins, 'ADD');
    }

    $("#tdPaxIns_1_" + paxId).html($.airutil.format.currency(ins));
}


UI_tabAnci.addInsToAll = function (total) {
    if (jsonPaxAdults.length > 0) {
        if (!UI_tabAnci.allowInfantInsurance) {
            var slicedIns = UI_tabAnci._splitWithoutLoss(total,
                jsonPaxAdults.length);
            for (var i = 0; i < jsonPaxAdults.length; i++) {
                UI_tabAnci.addInsPax(i, slicedIns[i]);
            }
        } else {
            var slicedIns = UI_tabAnci._splitWithoutLoss(total,
                jsonPaxAdults.length + jsonPaxInfants.length);
            for (var i = 0; i < jsonPaxAdults.length; i++) {
                if (jsonPaxAdults[i].type == "PA") {
                    UI_tabAnci.addInsPax(i, slicedIns[i] * 2);
                } else {
                    UI_tabAnci.addInsPax(i, slicedIns[i]);
                }
            }
        }
    }
}
/*
 * Insurance Passenger Selection
 */
UI_tabAnci.tdInsPaxOnClick = function () {
    // 0 - tdPaxMeal;
    // 1 - Column;
    // 2 - Row
    var arrData = this.id.split("_");
    var sbgColor = "";

    if (UI_tabAnci.currPaxIDRow != null) {
        sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
        $("#tdPaxIns_0_" + UI_tabAnci.currPaxIDRow).removeClass(
            "rowSelectedAnci");
        $("#tdPaxIns_0_" + UI_tabAnci.currPaxIDRow).addClass(sbgColor);
    }

    UI_tabAnci.currPaxIDRow = arrData[2];
    sbgColor = (UI_tabAnci.currPaxIDRow % 2 == 0) ? "whiteBackGround" : "alternateRow";
    $("#tdPaxIns_0_" + UI_tabAnci.currPaxIDRow).removeClass(sbgColor);
    $("#tdPaxIns_0_" + UI_tabAnci.currPaxIDRow).addClass("rowSelectedAnci");

    var paxIns = jsonPaxAdults[UI_tabAnci.currPaxIDRow].ins;
    if (paxIns.ins) {
        $('#chkAnciIns').attr('checked', true);
    } else {
        $('#chkAnciIns').attr('checked', false);
    }
}


UI_tabAnci.populateInsuranceData = function (p) {
	//in future if insurance is editable, handle it here
	UI_tabAnci.updateInsurance();
}

/*
 * Show pop up window
 */
UI_tabAnci.showPopupWindow = function (title, url, width, height, scroll) {
    if (UI_tabAnci.winO != null && !UI_tabAnci.winO.closed)
        UI_tabAnci.winO.close();
    if (width == null)
        width = 630;
    if (height == null)
        height = 900;
    if (scroll == null)
        scroll = false;
    if ($.browser.msie)
        UI_tabAnci.winO = window.open(url, "", $("#popWindow")
            .getPopWindowProp(width, height, scroll));
    else
        UI_tabAnci.winO = window.open(url, title, $("#popWindow")
            .getPopWindowProp(width, height, scroll));
    UI_tabAnci.winO.focus();
}

/*
 * CREATE PAX STRUCTURE
 */
UI_tabAnci.generatePaxWiseAnci = function (forSubmit) {
    if (forSubmit == null) {
        forSubmit = false
    }
    var paxList = $.airutil.dom.cloneObject(jsonPaxAdults);
    var infList = $.airutil.dom.cloneObject(jsonPaxInfants);
    var anciSeg = $.airutil.dom.cloneObject(jsAnciSeg);
    var removedAnciSeg = $.airutil.dom.cloneObject(jsCurrAnciSeg);
    var paxId = null;

    if (forSubmit) {
        var tmpPaxList = [];
        for (var i = 0; i < paxList.length; i++) {
            var pax = paxList[i];
            var tmpPax = {
                displayInfantWith: pax.displayInfantWith,
                displayAdultType: pax.displayAdultType,
                displayETicket: (pax.displayETicket == undefined) ? null : pax.displayETicket,
                displayPnrPaxCatFOIDNumber: (pax.displayPnrPaxCatFOIDNumber == undefined) ? null : pax.displayPnrPaxCatFOIDNumber,
                displayNationalIDNo: (pax.displayNationalIDNo == undefined) ? null : pax.displayNationalIDNo,
                displayPnrPaxCatFOIDExpiry: (pax.displayPnrPaxCatFOIDExpiry == undefined) ? null : pax.displayPnrPaxCatFOIDExpiry,
                displayPnrPaxCatFOIDPlace: (pax.displayPnrPaxCatFOIDPlace == undefined) ? null : pax.displayPnrPaxCatFOIDPlace,
                displayPnrPaxPlaceOfBirth: (pax.displayPnrPaxPlaceOfBirth == undefined) ? null:pax.displayPnrPaxPlaceOfBirth,
                displayTravelDocType: (pax.displayTravelDocType == undefined) ? null:pax.displayTravelDocType,
                displayVisaDocNumber: (pax.displayVisaDocNumber == undefined) ? null:pax.displayVisaDocNumber,
                displayVisaDocPlaceOfIssue: (pax.displayVisaDocPlaceOfIssue == undefined) ? null:pax.displayVisaDocPlaceOfIssue,
                displayVisaDocIssueDate: (pax.displayVisaDocIssueDate == undefined) ? null:pax.displayVisaDocIssueDate,
                displayVisaApplicableCountry: (pax.displayVisaApplicableCountry == undefined) ? null:pax.displayVisaApplicableCountry,
						displaypnrPaxArrivalFlightNumber: (pax.displaypnrPaxArrivalFlightNumber == undefined) ? null:pax.displaypnrPaxArrivalFlightNumber,
						displaypnrPaxFltArrivalDate: (pax.displaypnrPaxFltArrivalDate == undefined) ? null:pax.displaypnrPaxFltArrivalDate,
						displaypnrPaxArrivalTime: (pax.displaypnrPaxArrivalTime == undefined) ? null:pax.displaypnrPaxArrivalTime,
						displaypnrPaxDepartureFlightNumber: (pax.displaypnrPaxDepartureFlightNumber == undefined) ? null:pax.displaypnrPaxDepartureFlightNumber,
						displaypnrPaxFltDepartureDate: (pax.displaypnrPaxFltDepartureDate == undefined) ? null:pax.displaypnrPaxFltDepartureDate,
						displaypnrPaxDepartureTime: (pax.displaypnrPaxDepartureTime == undefined) ? null:pax.displaypnrPaxDepartureTime,
						displayPnrPaxGroupId: (pax.displayPnrPaxGroupId == undefined) ? null:pax.displayPnrPaxGroupId,
                displayAdultTitle: pax.displayAdultTitle,
                displayAdultFirstName: pax.displayAdultFirstName,
                displayAdultLastName: pax.displayAdultLastName,
                displayAdultNationality: pax.displayAdultNationality,
                // Note : pax.seqNumber is undifined here for create booking.
                seqNumber: UI_tabAnci.getPaxSequenceNo(i + 1, pax),
                paxRefNum: pax.travelerRefNumber,
                paxId: pax.paxId,
                displayAdultDOB: pax.displayAdultDOB,
                displayPaxCategory: pax.displayPaxCategory,
                anci: [],
                removeAnci: [],
                displayAdultFirstNameOl: pax.displayAdultFirstNameOl,
                displayAdultLastNameOl: pax.displayAdultLastNameOl,
                displayNameTranslationLanguage: pax.displayNameTranslationLanguage,
                displayAdultTitleOl: pax.displayAdultTitleOl,
                displayFFID: pax.displayFFID
            };
            tmpPaxList[tmpPaxList.length] = tmpPax;
        }
        paxList = tmpPaxList;
    }

    for (var i = 0; i < anciSeg.length; i++) {
        var paxAnci = anciSeg[i].pax;
        var removeAnci = removedAnciSeg[i].pax;
        var segInfo = {
            segCode: anciSeg[i].segID,
            ddt: anciSeg[i].ddt,
            flightNumber: anciSeg[i].flightNumber,
            flightRefNumber: anciSeg[i].flightRefNumber,
            cabinClassCode: anciSeg[i].cabinClassCode,
            logicalCabinClass: anciSeg[i].logicalCabinClass,
            baggageONDGroupId: getBaggageONDGrpId(anciSeg[i].flightRefNumber),
            returnFlag: anciSeg[i].returnFlag,
            pnrSegId: anciSeg[i].pnrSegId
        };

        if (forSubmit) {
            segInfo = {
                segCode: anciSeg[i].segID,
                flightNumber: anciSeg[i].flightNumber,
                flightRefNumber: anciSeg[i].flightRefNumber,
                cabinClassCode: anciSeg[i].cabinClassCode,
                logicalCabinClass: anciSeg[i].logicalCabinClass,
                baggageONDGroupId: getBaggageONDGrpId(anciSeg[i].flightRefNumber),
                returnFlag: anciSeg[i].returnFlag,
                pnrSegId: anciSeg[i].pnrSegId
            };
        }

        for (var j = 0; j < paxAnci.length; j++) {
            paxId = j;
            // if(paxAnci[j].paxType == 'PA'){
            // paxId =
            // parseInt(infList[paxAnci[j].refI].displayInfantTravellingWith,10)-1;
            // UI_tabAnci.addInfantToPax(paxList[paxId].anci[i],paxAnci[j])
            // }else{
            paxAnci[j].segInfo = segInfo;
            removeAnci[j].segInfo = segInfo;
            if (forSubmit) {
                paxList[paxId].anci[i] = UI_tabAnci.removeEmpty(paxAnci[j]);
                paxList[paxId].removeAnci[i] = UI_tabAnci
                    .removeEmpty(removeAnci[j]);
            } else {
                paxList[paxId].anci[i] = paxAnci[j];
                paxList[paxId].removeAnci[i] = removeAnci[j];
            }
            // delete paxList[paxId].currentAncillaries;
            // }
        }
    }
    paxList = UI_tabAnci.checkAndRemoveRetainedAnci(paxList);
    return paxList;

}

UI_tabAnci.removeEmpty = function (paxSegAnci) {
    var retObj = {};
    var hasAnci = false;
    if (paxSegAnci.seat.seatNumber != '') {
        retObj.seat = paxSegAnci.seat;
        hasAnci = true;
    }

    if (paxSegAnci.meal.meals.length > 0) {
        retObj.meal = paxSegAnci.meal;
        hasAnci = true;
    }

    if (paxSegAnci.baggage.baggages.length > 0) {
        retObj.baggage = paxSegAnci.baggage;
        hasAnci = true;
    }

    if (paxSegAnci.ssr.ssrs.length > 0) {
        retObj.ssr = paxSegAnci.ssr;
        hasAnci = true;
    }

    if (paxSegAnci.aps.apss.length > 0) {
        retObj.aps = paxSegAnci.aps;
        hasAnci = true;
    }

    if (paxSegAnci.apt.apts.length > 0) {
        retObj.apt = paxSegAnci.apt;
        hasAnci = true;
    }

    if (paxSegAnci.extraSeats.extraSeats.length > 0) {
        retObj.extraSeats = paxSegAnci.extraSeats;
        hasAnci = true;
    }
    
    if (paxSegAnci.automaticCheckin != null && ((paxSegAnci.automaticCheckin.seatPreference != null 
    		&& paxSegAnci.automaticCheckin.seatPreference != '') || (paxSegAnci.automaticCheckin.seatCode != null &&
			paxSegAnci.automaticCheckin.seatCode != ''))) {
        retObj.automaticCheckin = paxSegAnci.automaticCheckin;
        hasAnci = true;
    }

    if (hasAnci) {
        retObj.segInfo = paxSegAnci.segInfo;
    }

    return retObj;
}

UI_tabAnci.checkAndRemoveRetainedAnci = function (paxList) {
    for (var i = 0; i < paxList.length; i++) {
        var pax = paxList[i];
        if (pax.currentAncillaries != null) {
            for (var j = 0; j < pax.anci.length; j++) {
                for (var k = 0; k < pax.currentAncillaries.length; k++) {
                    if (UI_tabAnci
                        .compareFlightRefNo(
                            pax.anci[j].segInfo.flightRefNumber,
                            pax.currentAncillaries[k].flightSegmentTO.flightRefNumber)) {
                        var seat = removeRetainedSeat(
                            pax.currentAncillaries[k].airSeatDTO,
                            pax.anci[j].seat, pax.removeAnci[j].seat);
                        var extraSeats = removeRetainedExtraSeats(
                            pax.currentAncillaries[k].extraSeatDTOs, seat);
                        var meal = removeRetainedMeal(
                            pax.currentAncillaries[k].mealDTOs,
                            pax.anci[j].meal.meals,
                            pax.removeAnci[j].meal.meals);
                        var ssr = removeRetainSSR(
                            pax.currentAncillaries[k].specialServiceRequestDTOs,
                            pax.anci[j].ssr.ssrs,
                            pax.removeAnci[j].ssr.ssrs);
                        var baggage = removeRetainedBaggage(
                            pax.currentAncillaries[k].baggageDTOs,
                            pax.anci[j].baggage.baggages,
                            pax.removeAnci[j].baggage.baggages);
                        var apService = removeRetainAPService(
                            pax.currentAncillaries[k].airportServiceDTOs,
                            pax.anci[j].aps.apss,
                            pax.removeAnci[j].aps.apss);
                        var apTransfer = removeRetainAPTransfer(
                            pax.currentAncillaries[k].airportTransferDTOs,
                            pax.anci[j].apt.apts,
                            pax.removeAnci[j].apt.apts);
                        pax.anci[j].seat = seat.add;
                        pax.anci[j].meal.meals = meal.add;
                        pax.anci[j].baggage.baggages = baggage.add;
                        pax.anci[j].ssr.ssrs = ssr.add;
                        pax.anci[j].aps.apss = apService.add;
                        pax.anci[j].extraSeats.extraSeats = extraSeats.add;
                        pax.removeAnci[j].seat = seat.remove;
                        pax.removeAnci[j].meal.meals = meal.remove;
                        pax.removeAnci[j].ssr.ssrs = ssr.remove;
                        pax.removeAnci[j].baggage.baggages = baggage.remove;
                        pax.removeAnci[j].aps.apss = apService.remove;
                        pax.removeAnci[j].extraSeats.extraSeats = extraSeats.remove;
                        break;
                    }
                }

            }
            delete pax.currentAncillaries;
        }
    }
    return paxList;
}

function removeRetainSSR(current, ssrs, remSsrs) {
    if (current != null && current.length != null && ssrs != null && ssrs.length != null) {
        var removed = {};
        var ssrHash = {};
        for (var i = 0; i < current.length; i++) {
            for (var j = 0; j < ssrs.length; j++) {
                if ((ssrs[j].ssrCode == current[i].ssrCode) && ((nullHandler(ssrs[j].text) == '' && nullHandler(current[i].text) == '') || (ssrs[j].text == current[i].text))) {
                    removed[ssrs[j].ssrCode] = true;
                    break;
                }
            }

        }
        var out = [];
        for (var j = 0; j < ssrs.length; j++) {
            if (removed[ssrs[j].ssrCode] == null || !removed[ssrs[j].ssrCode]) {
                out[out.length] = ssrs[j];
            }
        }
        ssrs = out;

        if (remSsrs != null && remSsrs.length != null) {
            var rem = [];
            for (var j = 0; j < remSsrs.length; j++) {
                if (removed[remSsrs[j].ssrCode] == null || !removed[remSsrs[j].ssrCode]) {
                    rem[rem.length] = remSsrs[j];
                }
            }
            remSsrs = rem;
        }

    }
    return {
        add: ssrs,
        remove: remSsrs
    };
}

function removeRetainAPService(current, apss, remApss) {
    if (current != null && current.length != null && apss != null && apss.length != null) {
        var removed = {};
        var ssrHash = {};
        for (var i = 0; i < current.length; i++) {
            for (var j = 0; j < apss.length; j++) {
                if (apss[j].ssrCode == current[i].ssrCode && apss[j].airport == current[i].airportCode) {
                    removed[apss[j].ssrCode + apss[j].airport] = true;
                    break;
                }
            }

        }
        var out = [];
        for (var j = 0; j < apss.length; j++) {
            if (removed[apss[j].ssrCode + apss[j].airport] == null || !removed[apss[j].ssrCode + apss[j].airport]) {
                out[out.length] = apss[j];
            }
        }
        apss = out;

        if (remApss != null && remApss.length != null) {
            var rem = [];
            for (var j = 0; j < remApss.length; j++) {
                if (removed[remApss[j].ssrCode + remApss[j].airport] == null || !removed[remApss[j].ssrCode + remApss[j].airport]) {
                    rem[rem.length] = remApss[j];
                }
            }
            remApss = rem;
        }

    }
    return {
        add: apss,
        remove: remApss
    };
}

function removeRetainAPTransfer(current, apts, remApts) {
    if (current != null && current.length != null && apts != null && apts.length != null) {
        var removed = {};
        var ssrHash = {};
        for (var i = 0; i < current.length; i++) {
            for (var j = 0; j < apts.length; j++) {
                if (apts[j].ssrCode == current[i].ssrCode &&
                    apts[j].airport == current[i].airportCode) {
                    removed[apts[j].ssrCode + apts[j].airport] = true;
                    break;
                }
            }

        }
        var out = [];
        for (var j = 0; j < apts.length; j++) {
            if (removed[apts[j].ssrCode + apts[j].airport] == null || !removed[apts[j].ssrCode + apts[j].airport]) {
                out[out.length] = apts[j];
            }
        }
        apts = out;

        if (remApts != null && remApts.length != null) {
            var rem = [];
            for (var j = 0; j < remApts.length; j++) {
                if (removed[remApts[j].ssrCode + remApts[j].airport] == null || !removed[remApts[j].ssrCode + remApts[j].airport]) {
                    rem[rem.length] = remApts[j];
                }
            }
            remApts = rem;
        }

    }
    return {
        add: apts,
        remove: remApts
    };
}

function removeRetainedMeal(current, meals, remMeals) {
    if (current != null && current.length != null && meals != null && meals.length != null) {
        var remove = {};
        for (var i = 0; i < current.length; i++) {
            for (var j = 0; j < meals.length; j++) {
                if ((meals[j].mealCode == current[i].mealCode && meals[j].mealQty == current[i].soldMeals)) {
                    remove[meals[j].mealCode] = true;
                }
            }

        }
        var out = [];
        for (var i = 0; i < meals.length; i++) {
            if (remove[meals[i].mealCode] == null || !remove[meals[i].mealCode]) {
                out[out.length] = meals[i];
            }
        }
        meals = out;

        if (remMeals != null && remMeals.length != null) {
            var rem = [];
            for (var i = 0; i < remMeals.length; i++) {
                if (remove[remMeals[i].mealCode] == null || !remove[remMeals[i].mealCode]) {
                    rem[rem.length] = remMeals[i];
                }
            }
            remMeals = rem;
        }
    }
    return {
        add: meals,
        remove: remMeals
    };
}

function removeRetainedBaggage(current, baggages, remBaggages) {
    if (current != null && current.length != null && baggages != null && baggages.length != null) {
        var remove = {};
        for (var i = 0; i < current.length; i++) {
            for (var j = 0; j < baggages.length; j++) {
                if ((baggages[j].baggageName == current[i].baggageName)) {
                    remove[baggages[j].baggageName] = true;
                }
            }

        }
        var out = [];
        for (var i = 0; i < baggages.length; i++) {
            if (remove[baggages[i].baggageName] == null || !remove[baggages[i].baggageName]) {
                out[out.length] = baggages[i];
            }
        }
        baggages = out;

        if (remBaggages != null && remBaggages.length != null) {
            var rem = [];
            for (var i = 0; i < remBaggages.length; i++) {
                if (remove[remBaggages[i].baggageName] == null || !remove[remBaggages[i].baggageName]) {
                    rem[rem.length] = remBaggages[i];
                }
            }
            remBaggages = rem;
        }
    }
    return {
        add: baggages,
        remove: remBaggages
    };
}

function removeRetainedSeat(current, seat, remSeat) {
    if (current != null && seat != null) {
        if (current.seatNumber != null && seat.seatNumber != null && current.seatNumber == seat.seatNumber) {
            seat = {
                seatNumber: '',
                seatCharge: 0
            };
            if (remSeat != null && remSeat.seatNumber != null && current.seatNumber == remSeat.seatNumber) {
                remSeat = {
                    seatNumber: '',
                    seatCharge: 0
                };
            }
        }

    }
    return {
        add: seat,
        remove: remSeat
    };
}

function removeRetainedExtraSeats(current, seat) {
    if (seat.add.seatNumber != null && seat.add.seatNumber != '') {
        return {
            add: [],
            remove: current
        };
    } else {
        return {
            add: [],
            remove: []
        };
    }
}

/*
 * Add infant anci to pax
 */
UI_tabAnci.addInfantToPax = function (pax, inf) {
    for (var i = 0; i < inf.ssr.ssrs.length; i++) {
        var found = false;
        for (var j = 0; j < pax.ssr.ssrs.length; j++) {
            if (pax.ssr.ssrs[j].ssrCode == inf.ssr.ssrs[j].ssrCode) {
                found = true;
                pax.ssr.ssrs[j].charge += inf.ssr.ssrs[j].charge;
                break;
            }
        }
        if (!found) {
            pax.ssr.ssrs[pax.ssr.ssrs.length] = inf.ssr.ssrs[j];
        }
    }
}

// ------------------------------------ UPDATE GLOBAL STRUCTURES
// -----------------------------------------

/*
 * Update ancillary total
 */
UI_tabAnci.updateAnciTotals = function (anciType, amount, action, paxId, old) {
    old = (old | false);
    amount = parseFloat(amount);
    var anciSum = UI_tabAnci.anciSummary;
    // jsonPaxAdults[i].total = {credit: 0, charges:0};
    var pax = {};

    var perPaxAmt = [];
    var applyToAllPax = false;
    if (paxId == null) {
        applyToAllPax = true;
        perPaxAmt = UI_tabAnci._splitWithoutLoss(amount, jsonPaxAdults.length);
    } else {
        pax = jsonPaxAdults[paxId].total;
    }

    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].type == anciType) {
            if (action == 'ADD') {
                if (old) {
                    anciSum[i].old.subTotal += amount;
                    // UI_tabAnci.anciOldTotal += amount;
                } else {
                    if (!applyToAllPax) {
                        pax.charges += amount;
                    } else {
                        for (var j = 0; j < perPaxAmt.length; j++) {
                            jsonPaxAdults[j].total.charges += perPaxAmt[j];
                        }
                    }
                    anciSum[i].subTotal += amount;
                    UI_tabAnci.anciTotal += amount;
                }
            } else if (action == 'DEDUCT') {
                if (old) {
                    anciSum[i].subTotal -= amount;
                    if (!applyToAllPax) {
                        pax.credit -= amount;
                    } else {
                        for (var j = 0; j < perPaxAmt.length; j++) {
                            jsonPaxAdults[j].total.credit -= perPaxAmt[j];
                        }
                    }
                    UI_tabAnci.anciOldTotal -= amount;
                } else {
                    if (!applyToAllPax) {
                        pax.credit -= amount;
                    } else {
                        for (var j = 0; j < perPaxAmt.length; j++) {
                            jsonPaxAdults[j].total.credit -= perPaxAmt[j];
                        }
                    }
                    anciSum[i].subTotal -= amount;
                    UI_tabAnci.anciTotal -= amount;
                }
            }
            break;
        }
    }
    if(UI_tabAnci.anciTotalPrev != UI_tabAnci.anciTotal){
    	//UI_tabAnci.anciTotalChanged = true;
    	UI_tabAnci.continueToServiceTaxCalc = true;
    	UI_tabAnci.anciTotalPrev = UI_tabAnci.anciTotal;
    }else{
    //	UI_tabAnci.continueToServiceTaxCalc = false; add if pop up wants only when add anci.
    }
    UI_tabAnci.buildPriceSummary();
}

/*
 * Update ancillary total
 */
UI_tabAnci.updateAnciCounts = function (anciType, qty, action, old) {
    old = (old | false)
    var anciSum = UI_tabAnci.anciSummary;
    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].type == anciType) {
            if (action == 'ADD') {
                if (!old) {
                    anciSum[i].qty += qty;
                }
            } else if (action == 'DEDUCT') {
                if (old) {
                    anciSum[i].old.qty += qty;
                } else {
                    anciSum[i].qty -= qty;
                }
            }
        }
        /*
         * if user comes back and change the seat selection release the
         * originally selected seats
         */
        if (UI_tabAnci.seatsBlocked) {
            /* Enable tabs before ancillary and disable others */
            if (UI_tabAnci.isSegModify) {
                UI_modifySegment.tabStatus.tab0 = true;
                UI_modifySegment.tabStatus.tab1 = true;
                UI_modifySegment.tabStatus.tab2 = false;
            } else if (UI_tabAnci.isAddSegment) {
                UI_addSegment.tabStatus.tab0 = true;
                UI_addSegment.tabStatus.tab1 = true;
                UI_addSegment.tabStatus.tab2 = false;
            } else if (UI_tabAnci.isRequote) {
                UI_modifyResRequote.tabStatus.tab0 = true;
                UI_modifyResRequote.tabStatus.tab1 = true;
                UI_modifyResRequote.tabStatus.tab2 = false;
            } else {
                UI_makeReservation.tabStatus.tab0 = true;
                UI_makeReservation.tabStatus.tab1 = true;
                UI_makeReservation.tabStatus.tab2 = true;
                UI_makeReservation.tabStatus.tab3 = false;
                UI_makeReservation.tabStatus.tab4 = false;
            }
            if (anciSum[i].type == UI_tabAnci.anciType.SEAT) {
                UI_tabAnci.releaseBlockedSeats({
                    reset: false,
                    clearSavedAnci: true
                });
            }
        }
        if (UI_tabAnci.isAnciModify && UI_modifyAncillary.tabStatus.tab1) {
            UI_modifyAncillary.tabStatus.tab0 = true;
            UI_modifyAncillary.tabStatus.tab1 = false;
        }
    }
}

/*
 * Update pax totals
 */
UI_tabAnci.updatepaxTotals = function (paxId, amount, action) {
    /*
     * NO IMPLMENTATION FOR THE MOMENT - BACKEND HANDLE PAXWISE CALCULATIONS
     */
    return;
}

// -------------------------------------------SUMMARY
// ----------------------------------------------------------------------
/*
 * Build price summary
 */
UI_tabAnci.buildPriceSummary = function () {
    if (UI_tabAnci.isAnciModify) {
        UI_tabAnci.buildModifyAnciPriceBreakdown();
    } else {
    	
    	var salesTaxFareSelCurrency = 0;
		var salesTaxSercicesSelCurrency = 0;
    	
        var isBaseCur = (UI_tabAnci.jsonFareQuoteSummary.selectedCurrency == UI_tabAnci.jsonFareQuoteSummary.currency);
        var exchangeRate = 1;
        var totalFare = parseFloat(UI_tabAnci.jsonFareQuoteSummary.totalPrice);

        var selCurTotalFare = parseFloat(UI_tabAnci.jsonFareQuoteSummary.selectedtotalPrice);
        
        if(jsonFareQuoteSummary.totalServiceTax != "" && !UI_tabAnci.isRequote && UI_tabAnci.salesTaxForFare != 0 ){
        	
    		salesTaxFareSelCurrency = (parseFloat(UI_tabAnci.salesTaxForFare) / exchangeRate);

    		var salseTaxAdjustmentFareBaseCur = parseFloat(UI_tabAnci.salesTaxForFare)-(parseFloat(jsonFareQuoteSummary.totalServiceTax));
    		var salseTaxAdjustmentFareSelCur = (parseFloat(salseTaxAdjustmentFareBaseCur) / exchangeRate);
    		
    		totalFare += salseTaxAdjustmentFareBaseCur;
    		selCurTotalFare += salseTaxAdjustmentFareSelCur;	
        }
        
        var baseCurrency = UI_tabAnci.jsonFareQuoteSummary.currency;
        var selCurrency = UI_tabAnci.jsonFareQuoteSummary.selectedCurrency;
        if (!isBaseCur)
            exchangeRate = parseFloat(UI_tabAnci.jsonFareQuoteSummary.selectedEXRate);

        if (UI_tabAnci.onHold || UI_tabAnci.waitListing) {
            totalFare += parseFloat(UI_tabAnci.jsonFareQuoteSummary.totalFareDiscount);
            selCurTotalFare += (parseFloat(UI_tabAnci.jsonFareQuoteSummary.totalFareDiscount) / exchangeRate);
        }

        var totalFlexiCharge = UI_tabAnci.calculateTotalFlexiCharge();
        var selCurTotalFlexiCharge = (parseFloat(totalFlexiCharge) / exchangeRate);

		var totalAutoCheckinCharge = UI_tabAnci.calculateTotalAutoCheckInCharge();
		var selCurTotalAutoCheckinCharge = (parseFloat(totalAutoCheckinCharge) / exchangeRate);
        
        var flexiChgRemoveFromTotalFare = UI_tabAnci.totalFlexiChargeFromFareQuote();
        var selCurFlexiChgRemoveFromTotalFare = (parseFloat(flexiChgRemoveFromTotalFare) / exchangeRate);
        
        var administrationFee = parseFloat(UI_tabAnci.adminFee);
        var selectCurrAdminFee = parseFloat(UI_tabAnci.selectedCurrencyAdminFee);
        

        var totalFareWithOutAdminFee = totalFare - (isNaN(parseFloat(jsonFareQuoteSummary.adminFee)) ? 0
						: parseFloat(jsonFareQuoteSummary.adminFee));
		var selCurTotalFareWithOutAdminFee = (parseFloat(totalFareWithOutAdminFee)/exchangeRate);
		
		$("#tbdyAnciPriceSum").find("tr").remove();
		$("#tbdyAnciDiscount").find("tr").remove();

		var tblRow = document.createElement("TR");
		tblRow.appendChild(UI_commonSystem.createCell({
			desc : "&nbsp;",
			textCss : "height:6px;",
			colSpan : (isBaseCur ? "2" : "3")
		}));
		$("#tbdyAnciPriceSum").append(tblRow);

        	tblRow = document.createElement("TR");
        	tblRow.appendChild(UI_commonSystem.createCell({
            		desc: strTicketPrice,
            		align: "left"
        	}));
		
		if (!isBaseCur)
			tblRow.appendChild(UI_commonSystem.createCell({
				desc : '<font color="red" >'
						+ $.airutil.format.currency(selCurTotalFareWithOutAdminFee
								- selCurFlexiChgRemoveFromTotalFare)
						+ '</font>',
				align : "right"
			}));
			
		tblRow.appendChild(UI_commonSystem.createCell({
			desc : $.airutil.format.currency(totalFareWithOutAdminFee
					- flexiChgRemoveFromTotalFare),
			align : "right"
		}));


        $('#tbdyAnciPriceSum').append(tblRow);

        tblRow = document.createElement("TR");
        tblRow.appendChild(UI_commonSystem.createCell({
            desc: '&nbsp;',
            textCss: "height:6px;",
            colspan: (isBaseCur ? 2 : 3)
        }));
        $('#tbdyAnciPriceSum').append(tblRow);

        var anciSum = UI_tabAnci.anciSummary;
        for (var i = 0; i < anciSum.length; i++) {
            if (anciSum[i].stat == 'ACT') {
                if (anciSum[i].type == 'MEALS' && (UI_tabAnci.focMealEnabled == true)) {
                    // hide meal details if free of charge meal selection is
                    // enabled.
                } else {
                    tblRow = document.createElement("TR");
                    tblRow.appendChild(UI_commonSystem.createCell({
                        desc: anciSum[i].name,
                        align: "left"
                    }));

                    var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(anciSum[i].type,anciSum[i].subTotal);
                    
                    if (!isBaseCur) {
                    	var selsubT = '';
                    	if(applyFormatter){
                    		selsubT = UI_tabAnci.freeServiceDecorator;
                    	}else{
                    		selsubT = $.airutil.format
                            .currency(anciSum[i].subTotal / exchangeRate);
                    	}                        
                        tblRow.appendChild(UI_commonSystem.createCell({
                            desc: "<font color='red'>" + selsubT + "</font>",
                            align: "right"
                        }));
                    }
                    
                 
                    var subTotalFrmt = $.airutil.format.currency(anciSum[i].subTotal);
            		if(applyFormatter){
            			subTotalFrmt = UI_tabAnci.freeServiceDecorator;
            		}
                    

                    tblRow.appendChild(UI_commonSystem.createCell({
                        desc: subTotalFrmt,
                        align: "right"
                    }));

                    $('#tbdyAnciPriceSum').append(tblRow);

                    tblRow = document.createElement("TR");
                    tblRow.appendChild(UI_commonSystem.createCell({
                        desc: '&nbsp;',
                        textCss: "height:6px;",
                        colspan: (isBaseCur ? 2 : 3)
                    }));
                    $('#tbdyAnciPriceSum').append(tblRow);
                }
            }
        }

        var formtFlexi = UI_tabAnci.isApplyFreeServiceDecorator("FLEXI_CHARGES",totalFlexiCharge);
        
        //Add Flexi details to Charge break down
		if(UI_tabAnci.flexiSelectionEnabledInAnciPage == true){        
			tblRow = document.createElement("TR");
		    tblRow.appendChild(UI_commonSystem.createCell({
		        desc: strFlexiCharge,
		        align: "left"
		    }));
		    if (!isBaseCur) {
		        var selsubT = $.airutil.format.currency(selCurTotalFlexiCharge);
		        if(formtFlexi){
		        	selsubT= UI_tabAnci.freeServiceDecorator;
		        }
		        tblRow.appendChild(UI_commonSystem.createCell({
		            desc: "<font color='red'>" + selsubT + "</font>",
		            align: "right"
		        }));
		    }

		    var frmtTotFlexiCharge = $.airutil.format.currency(totalFlexiCharge);
		    if(formtFlexi){
		    	frmtTotFlexiCharge = UI_tabAnci.freeServiceDecorator;
		    }
		    
		    tblRow.appendChild(UI_commonSystem.createCell({
		        desc: frmtTotFlexiCharge,
		        align: "right"
		    }));

		    $('#tbdyAnciPriceSum').append(tblRow);
		}
       
 
        var freeServiceDiscount = UI_tabAnci.getPromoDiscountObject(
            UI_tabAnci.promoTypes.FREESERVICE, jsonSegAnciAvail);
        var paxWiseDiscountableAnciTotal = UI_tabAnci
            .getDiscountableAnciTotal(freeServiceDiscount);

        var anciDiscountAmount = UI_tabAnci.calculateAnciDiscount(
            paxWiseDiscountableAnciTotal, freeServiceDiscount,
            jsonPaxAdults);

        var grandTotal = totalFare + UI_tabAnci.anciTotal;
        
        if(jsonFareQuoteSummary.totalServiceTax != "" && !UI_tabAnci.isRequote){
        	
        	var salseTaxServicesBaseCurrency = UI_tabAnci.salesTaxForServices;
        	salesTaxSercicesSelCurrency = (parseFloat(UI_tabAnci.salesTaxForServices) / exchangeRate);
        	
        	grandTotal += salseTaxServicesBaseCurrency;
        	
        	var tblRowServiceTax = document.createElement("TR");
        	tblRowServiceTax.appendChild(UI_commonSystem.createCell({
                desc: "&nbsp;",
                textCss: "height:6px;",
                colSpan: (isBaseCur ? "2" : "3")
            }));
            $("#tbdyAnciPriceSum").append(tblRowServiceTax);

            tblRowServiceTax = document.createElement("TR");
            tblRowServiceTax.appendChild(UI_commonSystem.createCell({
                desc: 'Service Tax for Ancillary',
                align: "left"
            }));

            if (!isBaseCur)
            	tblRowServiceTax.appendChild(UI_commonSystem.createCell({
                    desc: '<font color="red" >' + $.airutil.format.currency(salesTaxSercicesSelCurrency) + '</font>',
                    align: "right"
                }));

            tblRowServiceTax.appendChild(UI_commonSystem.createCell({
                desc: $.airutil.format.currency(salseTaxServicesBaseCurrency),
                align: "right"
            }));

            $('#tbdyAnciPriceSum').append(tblRowServiceTax);
        }
       
        if(jsonFareQuoteSummary.adminFee != null){
            var adminDiff = administrationFee - parseFloat(jsonFareQuoteSummary.adminFee);
            grandTotal += adminDiff;
        }
		var tblAdminFee = document.createElement("TR");
		tblAdminFee.appendChild(UI_commonSystem.createCell({
			desc : "&nbsp;",
			textCss : "height:6px;",
			colSpan : (isBaseCur ? "2" : "3")
		}));
		$("#tbdyAnciPriceSum").append(tblAdminFee);

		tblRow = document.createElement("TR");
		tblRow.appendChild(UI_commonSystem.createCell({
			desc : 'Admin Fee',
			align : "left"
		}));
		if (!isBaseCur)
			tblRow.appendChild(UI_commonSystem.createCell({
				desc : '<font color="red" >'+ $.airutil.format.currency(selectCurrAdminFee)+ '</font>',
				align : "right"
				}));
		
		 	tblRow.appendChild(UI_commonSystem.createCell({
			desc : $.airutil.format.currency(administrationFee),
			align : "right"
		}));
		 	

		$('#tbdyAnciPriceSum').append(tblRow);
		
        tblRow = document.createElement("TR");
        tblRow.appendChild(UI_commonSystem.createCell({
            desc: '&nbsp;',
            textCss: "height:6px;",
            colspan: (isBaseCur ? 2 : 3)
        }));
        $('#tbdyAnciPriceSum').append(tblRow);

        var tblFreeServiceDiscountRow1 = null;
        var tblFreeServiceDiscountRow2 = null;
        var isCreditDiscount = false;
        if (freeServiceDiscount != null) {
            var discountLabel = top.arrError['XBE-LABEL-04'];
            if (freeServiceDiscount.discountAs == UI_tabAnci.promoDiscountAs.MONEY) {
                grandTotal -= anciDiscountAmount;
                discountLabel = top.arrError['XBE-LABEL-03'];
            } else {
                isCreditDiscount = true;
            }

            tblFreeServiceDiscountRow1 = document.createElement("TR");
            tblFreeServiceDiscountRow1.appendChild(UI_commonSystem.createCell({
                desc: discountLabel,
                align: "left"
            }));
            if (!isBaseCur) {
                var selsubT = '-' + $.airutil.format.currency(anciDiscountAmount / exchangeRate);
                tblFreeServiceDiscountRow1.appendChild(UI_commonSystem
                    .createCell({
                        desc: "<font color='red'>" + selsubT + "</font>",
                        align: "right"
                    }));
            }
            var discountToShow = '-' + $.airutil.format.currency(anciDiscountAmount);
            if (freeServiceDiscount.discountAs == UI_tabAnci.promoDiscountAs.CREDIT) {
                discountToShow = $.airutil.format.currency(anciDiscountAmount);
            }
            tblFreeServiceDiscountRow1.appendChild(UI_commonSystem.createCell({
                desc: discountToShow,
                align: "right"
            }));

            tblFreeServiceDiscountRow2 = document.createElement("TR");
            tblFreeServiceDiscountRow2.appendChild(UI_commonSystem.createCell({
                desc: '&nbsp;',
                textCss: "height:6px;",
                colspan: (isBaseCur ? 2 : 3)
            }));

        }

        if (tblFreeServiceDiscountRow1 != null && tblFreeServiceDiscountRow2 != null && !isCreditDiscount) {
            $('#tbdyAnciPriceSum').append(tblFreeServiceDiscountRow1);
            $('#tbdyAnciPriceSum').append(tblFreeServiceDiscountRow2);
        }

        if (!isBaseCur) {
            var selTotal = $.airutil.format.currency(grandTotal / exchangeRate);
            $('#divAnciTotal').html($.airutil.format.currency(grandTotal));
            $('#spnBaseCur').html(baseCurrency);
            $('#divAnciTotalinSelCur').html(
                "<font color='yellow'>" + selTotal + "</font>");
            $('#spnSelCur')
                .html("<font color='red'>" + selCurrency + "</font>");
        } else {
            $('#divAnciTotal').html($.airutil.format.currency(grandTotal));
            $('#spnBaseCur').html(baseCurrency);
            $('#divAnciTotalinSelCur').html("");
            $('#spnSelCur').html("");
        }

        if (tblFreeServiceDiscountRow1 != null && tblFreeServiceDiscountRow2 != null && isCreditDiscount) {
            $('#tbdyAnciDiscount').append(tblFreeServiceDiscountRow1);
            $('#tbdyAnciDiscount').append(tblFreeServiceDiscountRow2);
        }

        if (UI_tabSearchFlights.appliedFareDiscountInfo != null) {

            if (UI_tabSearchFlights.appliedFareDiscountInfo.description != null) {
                var disAmount = UI_tabSearchFlights.appliedFareDiscountInfo.farePercentage;
                if (UI_tabSearchFlights.appliedFareDiscountInfo.discountType == "VALUE") {
                    if (!isBaseCur) {
                        disAmount = selCurrency + ' ' + $.airutil.format
                            .currency(UI_tabSearchFlights.appliedFareDiscountInfo.farePercentage / exchangeRate);
                    } else {
                        disAmount = baseCurrency + ' ' + $.airutil.format
                            .currency(UI_tabSearchFlights.appliedFareDiscountInfo.farePercentage);
                    }
                }
                var msgText = UI_tabSearchFlights.appliedFareDiscountInfo.description
                    .replace('{amount}', disAmount);
                msgText = msgText.replace('{Amount}', disAmount);
                if (msgText != '') {
                    $('#divAnciPromotionBanner').html(msgText);
                    $("#anciPromoIcon").show();
                    $("#ansiPromoSet").show();
                }

            } else {
                $('#divAnciPromotionBanner').html('');
                $("#anciPromoIcon").hide();
                $("#ansiPromoSet").hide();
            }
        } else {
            $('#divAnciPromotionBanner').html('');
            $("#anciPromoIcon").hide();
            $("#ansiPromoSet").hide();
        }
    }

    $("#anciPromoIcon").on('click', function () {
        $("#ansiPromoSet").show()
    });
    $("#closePromoIcon").on('click', function () {
        $(this).parent().hide();
    });
}

UI_tabAnci.getPromoDiscountObject = function (discountType, anciAvailable) {
    var promoObj = null;
    if (UI_tabSearchFlights.isPromotionApplied() && !UI_tabSearchFlights.isSkipDiscount && UI_tabSearchFlights.appliedFareDiscountInfo.promotionType == discountType) {
        for (var k = 0; k < anciAvailable.length; k++) {
            for (var i = 0; i < anciAvailable[k].ancillaryStatusList.length; i++) {
                for (var j = 0; j < UI_tabSearchFlights.appliedFareDiscountInfo.applicableAncillaries.length; j++) {
                    var availAnciType = anciAvailable[k].ancillaryStatusList[i].ancillaryType.ancillaryType;
                    if (UI_tabSearchFlights.appliedFareDiscountInfo.applicableAncillaries[j] == UI_tabAnci.promoAnciType[availAnciType]) {
                        if (anciAvailable[k].ancillaryStatusList[i].available) {
                            promoObj = UI_tabSearchFlights.appliedFareDiscountInfo;
                        }
                    }
                }

            }
        }
    }
    return promoObj;
}

UI_tabAnci.getDiscountableAnciTotal = function (freeServiceDiscount) {

    var discountedAnciList = [];
    var applicableOnds = [];
    var paxWiseDisCountableTotal = {};
    if (freeServiceDiscount != null) {
        discountedAnciList = freeServiceDiscount.applicableAncillaries;
        applicableOnds = freeServiceDiscount.applicableOnds;
    }

    if (discountedAnciList != null && discountedAnciList.length > 0) {
        var anciSum = UI_tabAnci.anciSummary;
        var totalPaxCount = jsonPaxAdults.length;
        var anciSegLength = jsAnciSeg.length;
        var totInsCharge = 0;

        for (var i = 0; i < anciSum.length; i++) {
            if (UI_tabAnci.anciType.INS == anciSum[i].type) {
                totInsCharge = anciSum[i].subTotal;
                break;
            }
        }

        var paxSegInsurance = 0;
        if (totInsCharge > 0) {
            paxSegInsurance = totInsCharge / (totalPaxCount * anciSegLength);
        }

        for (var i = 0; i < anciSegLength; i++) {
            var jsAnciSegObj = jsAnciSeg[i];
            var segCode = jsAnciSegObj.flightRefNumber.split("$")[1];
            if (UI_tabAnci.isMatchOndFound(applicableOnds, segCode)) {
                for (var j = 0; j < jsAnciSegObj.pax.length; j++) {

                    if (paxWiseDisCountableTotal[j] == undefined || paxWiseDisCountableTotal[j] == null) {
                        paxWiseDisCountableTotal[j] = 0;
                    }

                    var segPaxObj = jsAnciSegObj.pax[j];
                    if (UI_tabAnci.isAnciDiscountable(UI_tabAnci.anciType.SEAT,
                        discountedAnciList)) {
                        if (segPaxObj.seat.seatCharge != null && segPaxObj.seat.seatCharge != '') {
                            paxWiseDisCountableTotal[j] += parseFloat(segPaxObj.seat.seatCharge);
                        }
                    }
                    if (UI_tabAnci.isAnciDiscountable(UI_tabAnci.anciType.MEAL,
                        discountedAnciList)) {
                        for (var k = 0; k < segPaxObj.meal.meals.length; k++) {
                            paxWiseDisCountableTotal[j] += parseFloat(segPaxObj.meal.meals[k].subTotal);
                        }
                    }
                    if (UI_tabAnci.isAnciDiscountable(
                        UI_tabAnci.anciType.BAGGAGE, discountedAnciList)) {
                        for (var k = 0; k < segPaxObj.baggage.baggages.length; k++) {
                            paxWiseDisCountableTotal[j] += parseFloat(segPaxObj.baggage.baggages[k].subTotal);
                        }
                    }
                    if (UI_tabAnci.isAnciDiscountable(UI_tabAnci.anciType.SSR,
                        discountedAnciList)) {
                        for (var k = 0; k < segPaxObj.ssr.ssrs.length; k++) {
                            paxWiseDisCountableTotal[j] += parseFloat(segPaxObj.ssr.ssrs[k].charge);
                        }
                    }
                    if (UI_tabAnci.isAnciDiscountable(
                        UI_tabAnci.anciType.APSERVICES, discountedAnciList)) {
                        for (var k = 0; k < segPaxObj.aps.apss.length; k++) {
                            paxWiseDisCountableTotal[j] += parseFloat(segPaxObj.aps.apss[k].charge);
                        }
                    }
                    if (UI_tabAnci.isAnciDiscountable(UI_tabAnci.anciType.INS,
                        discountedAnciList)) {
                        paxWiseDisCountableTotal[j] += paxSegInsurance;
                    }
                }
            }
        }
    }

    return paxWiseDisCountableTotal;
}

UI_tabAnci.isMatchOndFound = function (ondList, segmentCode) {
    if (ondList != null && ondList.length > 0) {
        for (var i = 0; i < ondList.length; i++) {
            if ((ondList[i]).indexOf(segmentCode) != -1) {
                return true;
            }
        }
    } else {
        return true;
    }

    return false;
}

UI_tabAnci.isAnciDiscountable = function (anciType, discountedAnciList) {
    for (var i = 0; i < discountedAnciList.length; i++) {
        if (UI_tabAnci.promoAnciType[anciType] == discountedAnciList[i]) {
            return true;
        }
    }

    return false;
}

UI_tabAnci.calculateAnciDiscount = function (paxWiseDiscountableAnciTotal,
    freeServiceDiscount, jsonPaxAdults) {
    var anciDiscountAmount = 0;
    if (freeServiceDiscount != null) {
        var totalPaxCount = jsonPaxAdults.length;
        var discountType = freeServiceDiscount.discountType;
        var applyTo = freeServiceDiscount.discountApplyTo;
        var discountValue = freeServiceDiscount.farePercentage;

        if (discountType == UI_tabAnci.promoDiscountType.PERCENTAGE) {
            var discountableAnciTotal = 0;
            $.each(paxWiseDiscountableAnciTotal, function (key, value) {
                discountableAnciTotal += value;
            });
            anciDiscountAmount = (discountableAnciTotal * discountValue) / 100;
        } else if (discountType == UI_tabAnci.promoDiscountType.VALUE) {
            var paxEligibalDiscountAmount = 0;
            var paxAnciDiscount = 0;
            if (applyTo == UI_tabAnci.PromoAppicablity.RESERVATION) {
                paxEligibalDiscountAmount = discountValue / totalPaxCount;
            } else if (applyTo == UI_tabAnci.PromoAppicablity.PER_PAX) {
                paxEligibalDiscountAmount = discountValue;
            }

            for (var i = 0; i < totalPaxCount; i++) {
                var paxAnciTotal = paxWiseDiscountableAnciTotal[i];
                if (paxAnciTotal > 0) {
                    if (paxAnciTotal > paxEligibalDiscountAmount) {
                        anciDiscountAmount += paxEligibalDiscountAmount;
                    } else {
                        anciDiscountAmount += paxAnciTotal;
                    }
                }
            }

        }

    }
    return anciDiscountAmount;
}

UI_tabAnci.priceBreakDownInit = false;
UI_tabAnci.totalBalanceToPay = 0;
UI_tabAnci.buildModifyAnciPriceBreakdown = function () {
    if (!UI_tabAnci.priceBreakDownInit) {
        $('#tblPriceSummaryContainer').html('');
        var ele = $('#tblAnciPriceBreakDown').detach();
        $('#tblPriceSummaryContainer').append(ele);
        ele.show();
        UI_tabAnci.priceBreakDownInit = true;
    }
    
  //  var totalNewAnciValue = 0.00;
    var salseTaxServicesBaseCurrency = 0.00;
    var salseTaxServicesBaseCurrencyFrmt = 0.00;
    
    var templateName = '#trPriceBreakDownTmpl';
    // $(templateName).parent().find('tr:last').not().remove();
    var anciSum = UI_tabAnci.anciSummary;
    for (var i = 0; i < anciSum.length; i++) {
        if (anciSum[i].stat == 'ACT') {
            if (anciSum[i].type == 'MEALS' && (UI_tabAnci.focMealEnabled == true)) {
                // hide meal details if free of charge meal selection is
                // enabled.
            } else {
                var tmpltId = 'trPBD_' + i;
                $('#' + tmpltId).remove();
                var clone = $(templateName).clone();
                clone.attr('id', tmpltId);
                clone.appendTo($(templateName).parent());
                
                var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator(anciSum[i].type,anciSum[i].subTotal);
                var applyFormOld = UI_tabAnci.isApplyFreeServiceDecorator(anciSum[i].type,anciSum[i].old.subTotal);
                var subTotalFrmt = $.airutil.format.currency(anciSum[i].subTotal);
                var oldSubTotalFrmt =  $.airutil.format.currency(anciSum[i].old.subTotal);
        		if(applyFormatter){
        			subTotalFrmt = UI_tabAnci.freeServiceDecorator;
        		}
        		if(applyFormOld){
        			oldSubTotalFrmt = UI_tabAnci.freeServiceDecorator;
        		}                
        		
      //  		totalNewAnciValue += anciSum[i].subTotal;
        		
                $('#' + tmpltId + ' td[name="name"]').html(anciSum[i].name);
                $('#' + tmpltId + ' td[name="old"]').html(oldSubTotalFrmt);
                $('#' + tmpltId + ' td[name="new"]').html(subTotalFrmt);
                clone.show();
            }
        }
    }

    // var grandTotal = UI_tabAnci.anciTotal + UI_tabAnci.anciOldTotal;
    var grandTotal = 0;
    // jsonPaxAdults[i].total = {credit: 0, charges:0};
    
    if(top.gstAppliedForReservation){
    	
    	var tmpltIdGst = 'trPBD_' + anciSum.length;
        $('#' + tmpltIdGst).remove();
        var clone = $(templateName).clone();
        clone.attr('id', tmpltIdGst);
        clone.appendTo($(templateName).parent());
        
        salseTaxServicesBaseCurrency = UI_tabAnci.salesTaxForServices;
        salseTaxServicesBaseCurrencyFrmt = $.airutil.format.currency(salseTaxServicesBaseCurrency);

        $('#' + tmpltIdGst + ' td[name="name"]').html('Service tax for ancillary');
        $('#' + tmpltIdGst + ' td[name="old"]').html('-');
        $('#' + tmpltIdGst + ' td[name="new"]').html(salseTaxServicesBaseCurrencyFrmt);
      
        clone.show();
    }
    
    var previousBalance = 0;
    for (var i = 0; i < jsonPaxAdults.length; i++) {
        var paxCredit = jsonPaxAdults[i].totalAvailableBalance;
        paxCredit = (paxCredit == null ? 0 : paxCredit);
        if (paxCredit > 0)
            previousBalance += parseFloat(paxCredit);
        if ((paxCredit + jsonPaxAdults[i].total.credit + jsonPaxAdults[i].total.charges) > 0) {
            grandTotal += (paxCredit + jsonPaxAdults[i].total.credit + jsonPaxAdults[i].total.charges);
        }
    }
    
    for (var i = 0; i < jsonPaxInfants.length; i++) {
        var paxCredit = jsonPaxInfants[i].totalAvailableBalance;
        paxCredit = (paxCredit == null ? 0 : paxCredit);
        if (paxCredit > 0)
            previousBalance += parseFloat(paxCredit);
        if (paxCredit  > 0) {
            grandTotal += parseFloat(paxCredit);
        }
    }
    
    if (previousBalance > 0) {
    	if(top.gstAppliedForReservation){
    		 var tmpltId = 'trPBD_' + anciSum.length + 1;
    	}else{
    		 var tmpltId = 'trPBD_' + anciSum.length;
    	}
        $('#' + tmpltId).remove();
        var clone = $(templateName).clone();
        clone.attr('id', tmpltId);
        clone.appendTo($(templateName).parent());

        $('#' + tmpltId + ' td[name="name"]').html('Current Balance');
        $('#' + tmpltId + ' td[name="old"]').html(
            $.airutil.format.currency(previousBalance));
        $('#' + tmpltId + ' td[name="new"]').html('-');
        clone.show();
    }
    
 //   var totalModificationBalance = (salseTaxServicesBaseCurrency + totalNewAnciValue);
    
    $(templateName).hide();
    UI_tabAnci.totalBalanceToPay = grandTotal;
    grandTotal = grandTotal + salseTaxServicesBaseCurrency;
    if (grandTotal < 0)
        grandTotal = 0;
  /*  if(totalModificationBalance > 0){
    	 $('#divAnciBalance').html($.airutil.format.currency(totalModificationBalance));
    }else{
    	totalModificationBalance = 0.00;
    	$('#divAnciBalance').html($.airutil.format.currency(totalModificationBalance));
    }*/
    $('#divAnciBalance').html($.airutil.format.currency(grandTotal));
}
/*
 * AccelAero split without loss
 */
UI_tabAnci._splitWithoutLoss = function (amt, count) {
    var sliced = new Array();
    amt = parseFloat(amt);
    if (amt != null) {
        var piece = $.airutil.format.currency(amt / count);
        piece = parseFloat(piece);
        var allExceptOneTotal = 0;
        for (var i = 0; i < count - 1; i++) {
            allExceptOneTotal += piece;
            sliced[i] = piece;
        }
        sliced[sliced.length] = (amt - allExceptOneTotal);
    }
    return sliced;
}

/*
 * Row group comparator
 */
UI_tabAnci.rowGroupComparator = function (first, second) {
	var firstRowGroupId = 0;
	var secondRowGroupId = 0;
	var firstRowGroupId = undefined;
	var secondTopElement = undefined;

	if (first.length != undefined) {
		var index = 0;
		do {
			firstTopElement = first[index];
			index ++;
		} while (firstTopElement == undefined);
		firstRowGroupId = firstTopElement.rowGroupId;
	} else {
		firstRowGroupId = first.rowGroupId;
	}

	if (second.length != undefined) {
		var index = 0;
		do {
			secondTopElement = second[index];
			index ++;
		} while (secondTopElement == undefined);
		secondRowGroupId = secondTopElement.rowGroupId;
	} else {
		secondRowGroupId = second.rowGroupId;
	}

	var numF = parseInt(first.rowGroupId,10);
	var numS = parseInt(second.rowGroupId,10);
   return (numF - numS);
}

/*
 * Row comparator
 */
UI_tabAnci.rowComparator = function (first, second) {
    var numF = parseInt(first.rowNumber, 10);
    var numS = parseInt(second.rowNumber, 10);
    return (numF - numS);
}

/*
 * Column group comparator
 */
UI_tabAnci.columnGroupComparator = function (first, second) {
    var numF = parseInt(first.columnGroupId, 10);
    var numS = parseInt(second.columnGroupId, 10);
    return (numF - numS);
}

/*
 * Isle comparator
 */
UI_tabAnci.seatIsleComparator = function (first, second) {
    var numF = parseInt(first.rowNum, 10);
    var numS = parseInt(second.rowNum, 10);
    return (numF - numS);
}
/*
 * Row comparator
 */
UI_tabAnci.seatRowComparator = function (first, second) {
	var index = 0;
	var firstTopElement = undefined;
	do {
		firstTopElement = first[index];
		index ++;
	} while (firstTopElement == undefined);
	
	index = 0;
	var secondTopElement = undefined;
	do {
		secondTopElement = second[index];
		index ++;
	} while (secondTopElement == undefined);
		
    var numF = parseInt(firstTopElement.rowNum, 10);
    var numS = parseInt(secondTopElement.rowNum, 10);
    return (numF - numS);
}
/*
 * private FN get Date for string of type 2010-03-08T05:15:00
 */
UI_tabAnci._getDate = function (dstr) {
    var a = dstr.split('T');
    var d = a[0].split('-');
    var t = a[1].split(':');
    return new Date(d[0], parseInt(d[1], 10) - 1, d[2], t[0], t[1], t[2]);
}

UI_tabAnci.departureDateComparator = function (first, second) {
    var dF = UI_tabAnci._getDate(first.flightSegmentTO.departureDateTime);
    var dS = UI_tabAnci._getDate(second.flightSegmentTO.departureDateTime);
    return (dF.getTime() - dS.getTime());
}

UI_tabAnci.seatMapDepartureDateComparator = function (first, second) {
    var dF = UI_tabAnci._getDate(first.flightSegmentDTO.departureDateTime);
    var dS = UI_tabAnci._getDate(second.flightSegmentDTO.departureDateTime);
    return (dF.getTime() - dS.getTime());
}

/*
 * SSR Comparator
 */
UI_tabAnci.SSRCodeComparator = function (objOne, objTwo) {
    return $.airutil.string.comparator(objOne.ssrCode, objTwo.ssrCode);
}

UI_tabAnci.loadAirportMessage = function () {
    var html = '<a href="javascript:void(0)" onclick="UI_tabAnci.hideAirportMessage()"><img border="0" src="../images/A150_no_cache.gif" title="Close" align="right"><\/a> ';
    var apMsg = UI_tabAnci.airportMessage;
    $('#airportMsg').html('');
    $('#airportMsg').append(html + apMsg);

    if (apMsg != null && apMsg != '') {
        $('#airportMsg').slideDown('slow');
    } else {
        $('#airportMsg').hide();
    }

}

UI_tabAnci.hideAirportMessage = function () {
    $("#airportMsg").hide();
}

UI_tabAnci.getPaxSequenceNo = function (i, pax) {
    if (pax.seqNumber == undefined || pax.seqNumber == null) {
        return i;
    } else {
        return pax.seqNumber
    }

}

UI_tabAnci.formatDateForAirportService = function (fltSegRec, index) {
    if (fltSegRec.flightRefNumber.indexOf('$') > 0) {
        ddt = UI_tabAnci._getFltRefDate(
            fltSegRec.flightRefNumber.split('$')[index]).getTime();
    } else {
        if (index == 3) {
            ddt = UI_tabAnci._getDate(fltSegRec.depatureDateTime).getTime();
        } else if (index == 4) {
            ddt = UI_tabAnci._getDate(fltSegRec.arrivalDateTime).getTime();
        }
    }
    return UI_tabAnci.formatDate(new Date(ddt));
}

UI_tabAnci.getFlightRPHList = function () {

    var rphArray = [];
    if (UI_tabSearchFlights.isMulticity()) {
        rphArray = UI_Multicity.getFlightRPHList();
    }else if(UI_tabSearchFlights.isCalenderMinimumFare()){
    	rphArray = UI_MinimumFare.getFlightRPHList();
    }else {
        rphArray = UI_tabSearchFlights.getFlightRPHList();
    }

    return $.toJSON(rphArray);
}

UI_tabAnci.isBaggagesSelectedForCurrentBooking = function () {
    if (UI_tabAnci.isSegModify) {
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            var noOfSegmentNotHaveCurrentAncillaryBaggages = 0;
            var noOfSegmentHaveCurrentAncillaryBaggages = 0;
            for (var k = 0; k < currAnci.length; k++) {
               	if (UI_tabAnci.doesNotHaveCurrentAncillaryBaggages(currAnci[k].flightSegmentTO.flightRefNumber)) {
					noOfSegmentNotHaveCurrentAncillaryBaggages++;
				} else {
					noOfSegmentHaveCurrentAncillaryBaggages++;
				}
            }
            if (noOfSegmentHaveCurrentAncillaryBaggages == jsBaggageModel.length) {
                UI_tabAnci.isBaggagesSelectedForCurrentBookingVal = true;
            } else if (noOfSegmentNotHaveCurrentAncillaryBaggages > 0) {
                UI_tabAnci.isBaggagesSelectedForCurrentBookingVal = false;
            }
        }
    } else if (UI_tabAnci.isAnciModify) {
        for (var i = 0; i < jsonPaxAdults.length; i++) {
            var currAnci = jsonPaxAdults[i].currentAncillaries;
            for (var k = 0; k < currAnci.length; k++) {
                if (currAnci[k].baggageDTOs != undefined && currAnci[k].baggageDTOs.length > 0) {
                    UI_tabAnci.isBaggagesSelectedForCurrentBookingVal = true;
                    break;
                } else {
                    UI_tabAnci.isBaggagesSelectedForCurrentBookingVal = false;
                }
            }
        }
    }
}

UI_tabAnci.doesNotHaveCurrentAncillaryBaggages = function (flightRefNumber){
	var flag = false;
	var correctBaggageSegId = UI_tabAnci.getCorrectBaggageSegmentId(flightRefNumber);
	if(correctBaggageSegId == undefined){
		flag = true;
	}else if (UI_tabAnci.isSegModify) {
		flag = (typeof UI_modifySegment.modifyingFltSegHash[flightRefNumber] == "undefined")
		                  || UI_modifySegment.modifyingFltSegHash[flightRefNumber] ==null;
	}
	return flag;
}

function getBaggageONDGrpId(rph) {
    for (var i = 0; i < jsBaggageModel.length; i++) {
        var flightSegmentTOObj = jsBaggageModel[i].flightSegmentTO;
        if (flightSegmentTOObj.flightRefNumber == rph) {
            return flightSegmentTOObj.baggageONDGroupId;
        }
    }

    return null;
}

UI_tabAnci.CheckForCheaperBundleFare = function () {
    var cheaperBundleFares = new Array();
    if (UI_tabAnci.isAnciModify) {
        cheaperBundleFares[UI_tabAnci.OND_SEQUENCE.OUT_BOUND] = new Array();
        cheaperBundleFares[UI_tabAnci.OND_SEQUENCE.IN_BOUND] = new Array();
    } else {
        var bundleFareAnciMatchWithSelectedAnci = {
            "BAGGAGE": "BAGGAGES",
            "MEAL": "MEALS",
            "SEAT_MAP": "SEAT_MAP"
        };

        var avilableBundleFares = UI_tabSearchFlights.resAvailableBundleFareLCClassStr;
        var ondViceAnciSummary = UI_tabAnci.createOndViseAnciSummary();
        
        //Adding ond wise support
        if(avilableBundleFares != null){
        	$.each(avilableBundleFares, function(ondSeq, bundledFares){
				var ondAnciSummary = ondViceAnciSummary[ondSeq];
				if(bundledFares != null){					
					var ondCheaperBundledFares = UI_tabAnci.calculateExtrachargeForAvailableBundledFare(
							ondAnciSummary, bundledFares, ondSeq);
					cheaperBundleFares[ondSeq] = ondCheaperBundledFares;
				}
        	});
        }
    }

    return cheaperBundleFares;

}

UI_tabAnci.createOndViseAnciSummary = function () {
    var ondAnciSummaryMap = new Array();
    var ondViseAnciSummaryMap = new Array();
    ondViseAnciSummaryMap[UI_tabAnci.OND_SEQUENCE.OUT_BOUND] = {};
    ondViseAnciSummaryMap[UI_tabAnci.OND_SEQUENCE.IN_BOUND] = {};
    var ondSegPaxApsCharge = 0;
    var ondSegPaxBaggageCharge = 0;
    var ondSegPaxMealCharge = 0;
    var ondSegPaxSeatCharge = 0;
    var ondSegPaxSsrCharge = 0;
    $.each(jsAnciSeg, function (index, segmentAnciModle) {
		var segmentAnciModle = jsAnciSeg[index];
		var segmentPax = segmentAnciModle.pax;
        var ondSeq = segmentAnciModle.ondSequence;
        
        var segPaxApsCharge = 0;
        var segPaxBaggageCharge = 0;
        var segPaxMealCharge = 0;
        var segPaxSeatCharge = 0;
        var segPaxSsrCharge = 0;
        $.each(segmentPax, function (paxindex, pax) {
            segPaxApsCharge += parseFloat(pax.aps.apsChargeTotal);
            segPaxBaggageCharge += parseFloat(pax.baggage.baggageChargeTotal);
            segPaxMealCharge += parseFloat(pax.meal.mealChargeTotal);
            segPaxSeatCharge += parseFloat(pax.seat.seatCharge);
            segPaxSsrCharge += parseFloat(pax.ssr.ssrChargeTotal);
        });
        
        if(ondAnciSummaryMap[ondSeq] == undefined || ondAnciSummaryMap[ondSeq] == null){
        		ondAnciSummaryMap[ondSeq] = {
        	        apsTotal: 0,
        	        baggageTotal: 0,
        	        mealTotal: 0,
        	        seatTotal: 0,
        	        ssrTotal: 0
        	    };
        }

        segPaxApsCharge = ondAnciSummaryMap[ondSeq].apsTotal + segPaxApsCharge;
        segPaxBaggageCharge = ondAnciSummaryMap[ondSeq].baggageTotal + segPaxBaggageCharge;
        segPaxMealCharge = ondAnciSummaryMap[ondSeq].mealTotal + segPaxMealCharge;
        segPaxSeatCharge = ondAnciSummaryMap[ondSeq].seatTotal + segPaxSeatCharge;
        segPaxSsrCharge = ondAnciSummaryMap[ondSeq].ssrTotal + segPaxSsrCharge;
        ondAnciSummaryMap[ondSeq] = {
            apsTotal: segPaxApsCharge,
            baggageTotal: segPaxBaggageCharge,
            mealTotal: segPaxMealCharge,
            seatTotal: segPaxSeatCharge,
            ssrTotal: segPaxSsrCharge
        };        
    });
    
    $.each(ondAnciSummaryMap, function(ondSeq, anciObj){
    	
    	if(anciObj == undefined || anciObj == null){
    		ondViseAnciSummaryMap[ondSeq] = {
	    		0: {
	                type: 'APS',
	                subTotal: 0
	            },
	            1: {
	                type: 'BAGGAGES',
	                subTotal: 0
	            },
	            2: {
	                type: 'MEALS',
	                subTotal: 0
	            },
	            3: {
	                type: 'SEAT_MAP',
	                subTotal: 0
	            },
	            4: {
	                type: 'SSR',
	                subTotal: 0
	            }
    		}
    	} else {    		
    		ondViseAnciSummaryMap[ondSeq] = {
    				0: {
    					type: 'APS',
    					subTotal: anciObj.apsTotal
    				},
    				1: {
    					type: 'BAGGAGES',
    					subTotal: anciObj.baggageTotal
    				},
    				2: {
    					type: 'MEALS',
    					subTotal: anciObj.mealTotal
    				},
    				3: {
    					type: 'SEAT_MAP',
    					subTotal: anciObj.seatTotal
    				},
    				4: {
    					type: 'SSR',
    					subTotal: anciObj.ssrTotal
    				}
    		};
    	}
    	
    })
    
    

    return ondViseAnciSummaryMap;
}

UI_tabAnci.calculateExtrachargeForAvailableBundledFare = function (
    ondAnciSummary, ondBundledFare, ondSequence) {
	
	 // holds the cheaper Bundlefare than sub total of the selected anci
    var cheaperBundleFares = new Array();
	
	if(UI_tabSearchFlights.ondWiseBundleFareSelected != null &&
    		UI_tabSearchFlights.ondWiseBundleFareSelected[ondSequence] != undefined &&
    		UI_tabSearchFlights.ondWiseBundleFareSelected[ondSequence] != null){
    	return cheaperBundleFares;
    }
	
    // matching Selected anci types with bundlefare freeservices names
    var bundleFareAnciMatchWithSelectedAnci = {
        "BAGGAGE": "BAGGAGES",
        "MEAL": "MEALS",
        "SEAT_MAP": "SEAT_MAP"
    };
    // summary of selected Anci
    var anciSummary = ondAnciSummary;
    // convert available bundlefare searchParam to a JS object
    var avilableBundleFares = UI_tabSearchFlights.resAvailableBundleFareLCClassStr;
    var ondAvailableBundleFares = ondBundledFare;
    // for holds the Types of selected Anci types
    var selectedAnciTypeSummary = new Array();
    // Sub total of selected Anci
    var selectedAnciTotal = 0;
    var bundleFareExtraCost = new Array();
    var bundleFareExtaraCost = new Array();
    if (ondBundledFare != undefined && ondBundledFare.length > 0 && typeof ondAnciSummary != 'undefined') {
        $.each(ondAnciSummary, function (index, summaryAnci) {
            if (summaryAnci.subTotal > 0) {
                var anciTypeName = summaryAnci.type;
                selectedAnciTypeSummary[selectedAnciTypeSummary.length] = anciTypeName;
                selectedAnciTotal += parseFloat(summaryAnci.subTotal);

            }
        });
        
        var isSelectedFlexiFare = UI_tabSearchFlights.ondWiseFlexiSelected[ondSequence];
        var isSelctedFlexiASAnci = UI_tabAnci.ondwiseFelexiSelected[ondSequence];
        if(isSelectedFlexiFare || isSelctedFlexiASAnci){        	
        	var flexiCharge = UI_tabSearchFlights.ondwiseFlexiFare[ondSequence];
        	selectedAnciTotal += parseFloat(flexiCharge);
        }

        var cheaperBundleFares = new Array();
        $.each(
        	ondAvailableBundleFares,
            function (index, availableBundleFare) {
                var bundleFare = availableBundleFare[0];
                
                var bundleFareFeeForSelectedAnci = bundleFare.bundleFareFee * jsonPaxAdults.length;
            	if (selectedAnciTotal < bundleFareFeeForSelectedAnci) {
            		var bundleFare = availableBundleFare[0];
            		$.each(
            				ondAnciSummary,
            				function (index, summaryAnci) {            					
        						var isSelectedAnciAvalibaleInBundleFare = false;
        						$.each(bundleFare.bundleFareFreeServices,
        								function (index, freeService) {
        							var feeBundlefareService = freeService;
        							if (bundleFareAnciMatchWithSelectedAnci[feeBundlefareService] == summaryAnci.type) {
        								isSelectedAnciAvalibaleInBundleFare = true;
        							}
        						});
        						if (isSelectedAnciAvalibaleInBundleFare != true) {
        							bundleFareFeeForSelectedAnci += summaryAnci.subTotal;
        						}
            				});
            		if (bundleFareFeeForSelectedAnci != 0 && bundleFareFeeForSelectedAnci > selectedAnciTotal) {
            			cheaperBundleFares[cheaperBundleFares.length] = bundleFare.bundleFareName;
            		}
            	}                
            });
    }
    return cheaperBundleFares;
}


UI_tabAnci.displayONDWiseAnciSelection = function (ondSequence) {    
    var isONDFlexiAvailable = UI_tabSearchFlights.ondWiseFlexiAvailable[ondSequence];
    if (!isONDFlexiAvailable) {
        $('#anciOndFlexi').hide();
        return;
    }
    var isSelectedFlexiFare = UI_tabSearchFlights.ondWiseFlexiSelected[ondSequence];

    var isSelctedFlexiASAnci = UI_tabAnci.ondwiseFelexiSelected[ondSequence];
    
    var flexiCharge = UI_tabSearchFlights.ondwiseFlexiFare[ondSequence];
    
    if (isSelectedFlexiFare) {
        $('#chkFlexiAdd').attr('checked', true);
        $('#chkFlexiAdd').attr("disabled", true);        
    } else if (isSelctedFlexiASAnci) {
        $('#chkFlexiAdd').attr('checked', true);
        $('#chkFlexiAdd').attr("disabled", false);
    } else {
        $('#chkFlexiAdd').attr('checked', false);
        $('#chkFlexiAdd').attr("disabled", false);
    }
    
    $('#anciOndFlexi').show();
    $("#chkFlexiAdd").unbind("click").click(function() { UI_tabAnci.selectFlexiAsAnci(ondSequence);});
    
    UI_tabAnci.setOndFlexiCharge(ondSequence);

    UI_tabAnci.buildPriceSummary();
}

UI_tabAnci.selectFlexiAsAnci = function (ondSequence) {
	var flexiCharge = UI_tabSearchFlights.ondwiseFlexiFare[ondSequence];
    if ($('#chkFlexiAdd').is(":checked")) {
        UI_tabAnci.ondwiseFelexiSelected[ondSequence] = true;
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.FLEXI, flexiCharge,'ADD');
    } else {
        UI_tabAnci.ondwiseFelexiSelected[ondSequence] = false;
        UI_tabAnci.updateAnciTotals(UI_tabAnci.anciType.FLEXI, flexiCharge,'DEDUCT');
    }
    UI_tabAnci.buildPriceSummary();
}

UI_tabAnci.setOndFlexiCharge = function (ondSequence) {
    var ondWiseFlexiCharge = UI_tabSearchFlights.ondwiseFlexiFare[ondSequence];
    
    var applyFormatter = UI_tabAnci.isApplyFreeServiceDecorator("FLEXI_CHARGES",ondWiseFlexiCharge);
	if(applyFormatter){
		ondWiseFlexiCharge = UI_tabAnci.freeServiceDecorator;
	}
    
    $('#ondFlextCost').text(ondWiseFlexiCharge + " " + UI_tabAnci.getDefaultCurrency());

}

UI_tabAnci.calculateTotalAutoCheckInCharge = function () {

    var toalAutoCheckinChargeForTrip = UI_tabAnci.totalAutoCheckInChargeFromFareQuote();
    return toalAutoCheckinChargeForTrip;

}
UI_tabAnci.totalAutoCheckInChargeFromFareQuote = function(){
	
	var totalAutoCheckinChg = 0;
    return totalAutoCheckinChg;

}

UI_tabAnci.calculateTotalFlexiCharge = function () {

    var toalFlexiChargeForTrip = UI_tabAnci.totalFlexiChargeFromFareQuote();
    
    $.each(UI_tabAnci.ondwiseFelexiSelected, function(ondSeq, selection){
    	if(selection && UI_tabSearchFlights.ondwiseFlexiFare[ondSeq] != undefined && 
    			UI_tabSearchFlights.ondwiseFlexiFare[ondSeq] != null){
    		var ondFlexiChg = parseFloat(UI_tabSearchFlights.ondwiseFlexiFare[ondSeq]);
    		toalFlexiChargeForTrip += ondFlexiChg;
    	}
    });
    
    
    return toalFlexiChargeForTrip;

}
UI_tabAnci.totalFlexiChargeFromFareQuote = function(){
	var totalFlexiChg = 0;
    
    $.each(UI_tabSearchFlights.ondWiseFlexiSelected, function(ondSeq, selection){
    	if(selection && UI_tabSearchFlights.ondwiseFlexiFare[ondSeq] != undefined && 
    			UI_tabSearchFlights.ondwiseFlexiFare[ondSeq] != null){
    		var ondFlexiChg = parseFloat(UI_tabSearchFlights.ondwiseFlexiFare[ondSeq]);
    		totalFlexiChg += ondFlexiChg;
    	}
    });
    
    return totalFlexiChg;
}

UI_tabAnci.createONDSegmentForFlexi = function (objSegDt) {
	
	_getOndSegObj = function(ondSegArr, ondSeq){
		for ( var i = 0; i < ondSegArr.length; i++) {
			var obj = ondSegArr[i];
			if(obj.ondSeq == ondSeq){
				return obj;
			}
			
		}		
		return null;
	}
	
    var ondSegInfo = new Array();
    $.each(objSegDt, function (index, segmentInfo) {   
    	var ondSeq = segmentInfo.ondSequence;    	
    	var segObj = _getOndSegObj(ondSegInfo, ondSeq);
    	
    	if(segObj == null){
    		segObj = {};
    		segObj["ondCode"] = segmentInfo.orignNDest;
    		segObj["fltNum"] = segmentInfo.flightNumber;
    		segObj["dateNTime"] = UI_tabAnci.formatDate(new Date(segmentInfo.ddt));
    		segObj["ondSeq"] = segmentInfo.ondSequence;
    		ondSegInfo.push(segObj);
    	} else {
    		var currentOndCode = segObj["ondCode"];
    		currentOndCode += segmentInfo.orignNDest.substring(3, 7);
    		segObj["ondCode"] = currentOndCode;
    	}
    });
    
    return ondSegInfo;

}

UI_tabAnci.mergeFlexiSelection = function(){
	var mergedSelection = {}
	$.each(UI_tabSearchFlights.ondWiseFlexiSelected, function(ondSeq, selection){
		if(UI_tabAnci.ondwiseFelexiSelected[ondSeq] != undefined){			
			mergedSelection[ondSeq] = (UI_tabAnci.ondwiseFelexiSelected[ondSeq] || selection);
		} else {
			mergedSelection[ondSeq] = selection;
		}
    });
	
	return mergedSelection;
}

UI_tabAnci.isFreeServiceIncludedWithBundle = function(type){
	var currAnciSeg =  jsAnciSeg[UI_tabAnci.currSegRow];
	var ondSequence = UI_tabAnci.currSegRow;
	if(currAnciSeg != undefined && currAnciSeg != null){
		ondSequence = currAnciSeg.ondSequence;
	}
	if(ondSequence==undefined || ondSequence==null){
		ondSequence = 0;
	}
    var bundledFarePeriodId = UI_tabSearchFlights.ondWiseBundleFareSelected[ondSequence];
    var isFound = false;
	if(bundledFarePeriodId!=undefined && bundledFarePeriodId!=null){		
		var freeServices = UI_tabAnci.getAvailableFreeServices(ondSequence,bundledFarePeriodId);		
		if(freeServices!=undefined && freeServices!=null){
			$.each(freeServices, function (q, freeService) {        						
				if(type == freeService){   
					isFound = true;
				}        						
			 });
		}		
	}
	return isFound;
}


UI_tabAnci.getAvailableFreeServices = function(ondSequence,bundledFarePeriodId){
	var servicesList = [];
	if(bundledFarePeriodId!=undefined && bundledFarePeriodId!=null){
		var availbundleFares = UI_tabSearchFlights.resAvailableBundleFareLCClassStr[ondSequence];
		if(availbundleFares!=undefined && availbundleFares!=null){
			$.each(availbundleFares, function (key, availbundleFare) {			
				var selectedBundleFare = availbundleFare[0];
				if(selectedBundleFare.bundledFarePeriodId==bundledFarePeriodId){
					servicesList = selectedBundleFare.bundleFareFreeServices;				
				}
			 });        			
		}
	} 
	
	return servicesList;
}

UI_tabAnci.isApplyFreeServiceDecorator = function(type,chargeAmount){	
	
	switch(type){
		case "SSR":
			type=UI_tabAnci.promoAnciType.SSR;
			break;
		case "MEALS":
			type=UI_tabAnci.promoAnciType.MEALS;
			break;	
		case "BAGGAGES":
			type=UI_tabAnci.promoAnciType.BAGGAGES;
			break;
	}
	
	if(UI_tabAnci.isAnciModify){
		var bundleFareList = DATA_ResPro["bundleFareList"];
		var currAnciSeg =  jsAnciSeg[UI_tabAnci.currSegRow];
		var ondSequence = UI_tabAnci.currSegRow;
		if(currAnciSeg != undefined && currAnciSeg != null){
			ondSequence = currAnciSeg.ondSequence;
		}
		if(ondSequence==undefined || ondSequence==null){
			ondSequence = 0;
		}
		var isFreeService = false;
		if(bundleFareList != null){			
			$.each(bundleFareList, function (i, obj) {
				if(obj!=undefined && obj!=null && ondSequence==i){
					UI_tabSearchFlights.ondWiseBundleFareSelected[i] = obj.bundledFarePeriodId;
					$.each(obj.applicableServiceNames, function (j, services) {
						if(type == services){   
							isFreeService = true;
						}        			
					});				
				}			
			});	
		}
	    
	} else {
		 isFreeService = UI_tabAnci.isFreeServiceIncludedWithBundle(type);	
	}	
		
	if(!parseFloat(chargeAmount) > 0 && isFreeService){
		return true;	
	}		
    return false;	
}