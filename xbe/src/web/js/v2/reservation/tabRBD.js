/**
 * @author Ruwan Baladewa
 * @author Dilan Anuruddha
 */
(function($){
	$.fn.buildFlightPanels = function(params) {

		var templateName = params.templateName;
		
		$('#'+templateName).parent().find('div').filter(function() {			
			var regex = new RegExp(templateName+'*');
	        return (regex.test(this.id) && this.id != templateName);
	    }).remove();

		for(var i=0; i < params.data.length ; i++) {
			var dtoRecord = params.data[i];		
			var clonedTemplateId = templateName + "_" + i;		
			var clone = $( "#" + templateName + "").clone();		
			clone.attr("id", clonedTemplateId );		
			clone.appendTo($("#" + templateName).parent());		
			
			// setting the value and id of labels
			$("#"+clonedTemplateId).find("label").each(function( intIndex ){
				$(this).text(dtoRecord[this.id]);
				$(this).attr("id",  this.id + '_' + i);
			});		
			
			$("#"+clonedTemplateId).find(".classNav").each(function( intIndex ){
				$(this).attr("id",  this.id + '_' + i);
			});		
			$("#"+clonedTemplateId).find("#flightTable").each(function( intIndex ){
				$(this).attr("id",  this.id + '_' + i);
			});	
			
			$("#"+clonedTemplateId).find("#fareTable").each(function( intIndex ){
				$(this).attr("id",  this.id + '_' + i);
			});	
			$("#" + clonedTemplateId).show();		
		}
		$('#'+templateName).hide();
	}
})(jQuery);

UI_tabRBDSearch = function(){};
UI_tabRBDSearch.initialized = false;
UI_tabRBDSearch.busCarrierCodes = [];

UI_tabRBDSearch.ready = function(){
	if(!UI_tabRBDSearch.initialized){
		var n = 0;
		var count = 0;
		$(".showHide").click(function(){
			  if (this.id == "searc-shedule"){
				  var tem = $("#spnAdvancedSearchPane").css("display");
				  if (tem == "none")
					$("#spnAdvancedSearchPane").slideDown();
				  else
					$("#spnAdvancedSearchPane").slideUp();
					
			  }else if (this.id == "searc-flight"){
				  var temp = $("#spnSearchPane").css("display");
				  if (temp == "none")
					$("#spnSearchPane").slideDown();
					else
					$("#spnSearchPane").slideUp();
			  }else if(this.id == "backto-search"){
				  $("#trSearchParams").slideDown();
				  $("#trRbdResults").slideUp();
			  }else{
				  var temp1 = $("#multicity").css("display");
				  if (temp1 == "none")
					$("#multicity").slideDown();
					else
					$("#multicity").slideUp();
			  }
		});
		
		$("#divBookingTabs").tabs();
		
		
		$("#btnSearch").click(function(){
			// $("#trSearchParams").slideUp();
			  $("#trRbdResults").slideDown();
			  UI_tabRBDSearch.searchOnClick();
		});
		
		
		$(document).on("click","#btnReset", function(){
			window.location.href = "demo!loadMulticiti.action";
		});
		
		$(document).on("click",'.classNav', function(){
			alert(this.id);
		})
		
		$(document).on("click","#quote-search", function(){
			window.location.href = "demo!loadavailability.action?hdnAction=ADVANCEDSEARCH3";
		});
		
		$(".editFare").click(function(){
			$(this).next().find("input").val($(this).find("font").html());
			$(this).hide();
			$(this).next().show();
		});
		
		$(".faretext").keypress(function(event){
			var code = (event.keyCode ? event.keyCode : event.which);
	 			if(code == 13) { 
					setTotal(this)
					$(this).parent().prev().find("font").html($(this).val());
					$(this).parent().hide();
					$(this).parent().prev().show();
				}
	 	 });
		
		function setTotal(obj){
			var lastval = parseFloat($(obj).parent().prev().find("font").html());
			var newVal = parseFloat($(obj).val());
			var lastTotal = parseFloat($(obj).parent().parent().parent().find(".row").html());
			var newTotal = (lastTotal - lastval) + newVal;
			var lastGTot = parseFloat($(".tot").html());
			var newGTot = (lastGTot - lastTotal) + newTotal;
			$(obj).parent().parent().parent().find(".row").html(addDecimal(newTotal));
			$(".tot").html(addDecimal(newGTot));
		};
		
		function addDecimal(val){
			
			var temp = String(val).split(".")
			if (temp[1] == undefined){
				val = val + ".00";
			}else if (temp[1].length == 1){
				val = val + "0";
			}
			return val;
		};
		
		
		
		
		$(document).on("click",".edit", function(){
			var tk = $(this).parent().parent().prev().find(".dataTable").css("display");
			if (tk == "none"){
				//$("multicity-search-"+n).hide();
				$(this).parent().parent().prev().find(".dataTable").slideDown();
				$(this).parent().parent().prev().prev().slideDown()
				$(this).val("Done");
			}else{
				//$("multicity-search-"+n).show();
				$(this).parent().parent().prev().find(".dataTable").slideUp();
				$(this).parent().parent().prev().prev().slideUp()
				$(this).val("Edit");
			}
		});
		
		//RBD GRID
		 $("#btnFareQuote").click(function(){
			 showLoader();
			 $(".resultContainer").slideUp(); 
		 	$("#fareQuoteData").slideDown();
		 	$("#btnFareQuote").hide()
		 	$("#btBackresults").show()
		 });


		$("#btBackresults").click(function(){
			 $(".resultContainer").slideDown(); 
		 	$("#fareQuoteData").slideUp();
		 	$("#btnFareQuote").show()
		 	$("#btBackresults").hide()
		 });
		 
		//if (event.keyCode == 18){
		var clicked = true;
		$(".sectionZoomerHandler").click(function(){
			if (clicked){
				$(".sectionZoomer").css({height:160,zIndex:0,opacity:0.5});
				$(".sectionZoomerouter").parent().css({
						position:"static",
						zIndex:1
				});
				var pos = $(this).prev().position();
				$(this).parent().css({
					position:"absolute",
					zIndex:1000,
					top:pos.top,
					left:pos.left
				 });
				$(this).prev().animate({height:160,opacity:1},{duration:200});
				$(this).addClass("upsideDown");
				clicked = false;
			}else{
				var pos =$(this).prev().position();
				$(this).prev().animate({height:160},{duration:50 ,
					complete:function(){
					$(this).parent().css({
						position:"static",
						zIndex:1,
						top:pos.top,
						left:pos.left
					 	});
					 	}
			 	});
				$(".sectionZoomer").css({opacity:1});
				$(this).removeClass("upsideDown");
				clicked = true;	
			}
		});

		$(".sectionZoomerouter").parent().bind("mouseenter", function(){
			//var t = $(this).find(".sectionZoomer").css("opacity")
			//if (t == 1)
				//$(this).find(".sectionZoomerHandler").fadeIn("slow");
		});

		$(".sectionZoomerouter").parent().bind("mouseleave", function(){
			//$(this).find(".sectionZoomerHandler").fadeOut("fast");
		});
		 
		 
		$("#btnContinue").click(function(){
			showLoader();
			alert("All feature is not available in Demo mode. Please do a new search");
			window.location.href = "makeReservation.action";// Demo availability page does not have good enough information to redirect to the Passenger page, it direct to the normal search page with an alert
		});
		
		hideLoader = function(){
			$("#divLoadMsg").hide();	
		}
		showLoader = function(){
			$("#divLoadMsg").show();
			setTimeout('hideLoader()',1000);
		}
		showLoader();
		
		UI_tabRBDSearch.initialized = true;
	}
};


/*
 * Submit form on enter key press
 */

UI_tabRBDSearch.checkEnterAndSubmit = function(keyEvent){
      if(keyEvent.which == 13){
          UI_tabRBDSearch.searchOnClick();
      }
 }

/*
 * Search Flights
 */
UI_tabRBDSearch.searchOnClick = function() {	
	$("#btnSearch").blur();
	//UI_tabRBDSearch.originDestinationOnBlur();
	//UI_tabRBDSearch.releaseBlockSeats(UI_tabRBDSearch.blockSeatsReleaseTypes[1]);
	//UI_tabRBDSearch.disableEnablePageControls(false);
	// Handling for transfer segment
	if (UI_commonSystem.strPGMode != "transferSegment") {			
	//	UI_tabAnci.resetAnciData();
	//	UI_tabPassenger.resetData();			
	}
	var isValidate = UI_tabRBDSearch.validateSearchFlight();
	if (isValidate) {
		$("#frmSrchFlt").action("flightSearch.action");
		var data = {}; // $("#frmMakeBkg").serializeArray();
		data['modifyBooking'] = false;
		if($('#selSearchOptions').attr('disabled')){
			data['searchParams.searchSystem'] = $('#selSearchOptions').val();
		}
		
		// set params on segment modification
		if($('#fromAirport').attr('disabled')){				
			data['searchParams.fromAirport'] = $('#fromAirport').val();
		}
		if($('#toAirport').attr('disabled')){				
			data['searchParams.toAirport'] = $('#toAirport').val();
		}
		if($('#departureDate').attr('disabled')){
			data['searchParams.departureDate'] = $('#departureDate').val();				
		}
		if($('#returnDate').attr('disabled')){
			data['searchParams.returnDate'] = $('#returnDate').val();				
		}						
		if($('#returnVariance').attr('disabled')){
			data['searchParams.returnVariance'] = $('#returnVariance').val();				
		}
		if($('#departureVariance').attr('disabled')){
			data['searchParams.departureVariance'] = $('#departureVariance').val();				
		}			
		
		if($('#bookingType').attr('disabled')){
			data['searchParams.bookingType'] = $('#bookingType').val();				
		}
		
		if (UI_commonSystem.strPGMode == "addSegment") {
			data['addSegment'] = true;				
		}
		if (UI_commonSystem.strPGMode == "confirmOprt") {
			data['searchParams.openReturnConfirm'] = true;
		}
		
//		data['fareDiscountPercentage'] = UI_tabRBDSearch.fareDiscount.percentage;
//		data['fareDiscountNotes'] = UI_tabRBDSearch.fareDiscount.notes;		

		data['searchParams.ticketValidityMax'] = $('#ticketExpiryDate').val();				
		data['searchParams.ticketValidityMin'] = $('#ticketValidFrom').val();				
		data['searchParams.ticketValidityExist'] = $('#ticketExpiryEnabled').val();
		
		if (UI_commonSystem.strPGMode == "addSegment" || UI_commonSystem.strPGMode == "transferSegment") {
			UI_tabRBDSearch.createSubmitData({data:data, mode:UI_commonSystem.strPGMode});							
		} else 	if (UI_commonSystem.strPGMode != "") {
			UI_tabRBDSearch.createSubmitData({data:data, mode:UI_commonSystem.strPGMode});	
			data['flexiAlertInfo']= DATA_ResPro.flexiAlertInfo;
			if(DATA_ResPro.modifyFlexiBooking){data['searchParams.quoteFlexi'] = false;};
		}
		var select = document.getElementById('selPaxTypeOptions');
		UI_tabRBDSearch.strPaxType = select.options[select.selectedIndex].value;// $("#selPaxTypeOptions").val();
		
		$("#frmSrchFlt").ajaxSubmit({ dataType: 'json',	data:data,										
										success: UI_tabRBDSearch.onFlightAvailSuccess,
										error:UI_commonSystem.setErrorStatus});	
	} else {			
		if(UI_commonSystem.strPGMode != "") {
			UI_tabRBDSearch.disableEnablePageControls(true);
		}	
	}		
	//UI_tabRBDSearch.hideAirportMessage();		
	return false;
}

UI_tabRBDSearch.createSubmitData = function(){
	//TODO
}

UI_tabRBDSearch.validateSearchFlight = function(){
	return true; //TODO
}


UI_tabRBDSearch.onFlightAvailSuccess = function(response){
	if(response.success) {
		UI_tabRBDSearch.addBusCarrierCodes(response.busCarrierCodes);
	//	var data = [{
	//		segmentCode : 'DAM/SAW',
	//		selectedData : '02Aug10',
	//		flightDetails: [
	//		{id:"1",rad:"<input type='radio' name='selthis1'/>",segment:"DAM/SAW", flight:"<p class='p'>6Q201</p>", op:"<p class='p'>6Q</p>", departure:"<p class='p'>02Aug10 01:10</p>",arrival:"<p class='p'>02Aug10 13:10</p>",avl:"<p class='p'>62/29</p>"},
	//		{id:"2",rad:"<input type='radio' name='selthis1'/>",segment:"DAM/SAW", flight:"<p class='p'>6Q945</p>", op:"<p class='p'>6Q</p>", departure:"<p class='p'>02Aug10 03:10</p>",arrival:"<p class='p'>02Aug10 13:10</p>",avl:"<p class='p'>62/29</p>"}
	//		]
	//			}, {
	//				segmentCode : 'SAW/DAM',
	//				selectedData : '06Aug10',
	//				flightDetails:[
	//		{id:"1",rad:"<input type='radio' name='selthis2'/>",segment:"SAW/DAM", flight:"<p class='p'>6Q945</p>", op:"<p class='p'>6Q</p>", departure:"<p class='p'>06Aug10 13:10</p>",arrival:"<p class='p'>06Aug10 13:10</p>",avl:"<p class='p'>62/29</p>"},
	//		{id:"2",rad:"<input type='radio' name='selthis2'/>",segment:"SAW/DAM", flight:"<p class='p'>6Q201</p>", op:"<p class='p'>6Q</p>", departure:"<p class='p'>06Aug10 05:10</p>",arrival:"<p class='p'>06Aug10 13:10</p>",avl:"<p class='p'>62/29</p>"}
	//		]
	//	}];
		$('#panelContainer').buildFlightPanels({templateName:'panel', data :response.ondFlights});
		UI_tabRBDSearch.buildFlightGrid({data:response.ondFlights})
	}else{
		showERRMessage(response.messageTxt);
	}
};
function hideFareQuote(){
 	$("#btnFareQuote").attr("disabled","");
 	$("#fareQuoteData").hide();
 };


UI_tabRBDSearch.buildFlightGrid = function(params){
	
	
	
	var arrBreakFmt = function (arr, options, rowObject){
		var outStr = '';
		var first = true;
		for(var i = 0; i < arr.length ; i++){
			if(!first) outStr  += '<br />';
			outStr +=arr[i];
			first = false;
		}
		return outStr ;
	}
	
	var carrierCodeFmt = function (arr, options, rowObject){
		for(var i = 0; i < arr.length; i++){				
			arr[i] = UI_tabSearchFlights.getBusCarrierCode(arr[i]);
		}
		return arrBreakFmt(arr, options, rowObject);
	}
	
	var segBreakFmt = function (objValue, options, rowObject){
		var arr = objValue.split("/");
		var outStr = '';
		for(var i = 0; i < arr.length ; i++){
			if( i == 0){					
				outStr +=arr[i];  
			}else if(i%3 == 0){
				outStr+= '/<br />' +arr[i];
			}else {
				outStr+= '/' +arr[i];
			}
			
		}
		return outStr ;
	}
	
	for(var i = 0 ; i < params.data.length; i++){
		var radName = "radFlt_"+i;
		var radioButtonFmt = function(index, options, rowObject){
			var prefix = 'radFlt';
			var checked = '';
			var disabled = '';
			var str = "<input type='radio' id='"+ prefix + "_" + index  + "' name='"+ radName + "'"	+ checked +  disabled +"  value='" + index + "'>";
			return str;
		}
		var dtoRecord = params.data[i];		
		$('#flightTable_' + i).jqGrid({
			datatype: 'local',
			colNames:['','Segment','Flight','Op.','Departure','Arrival','Avl'],
			colModel :[ 
			   {name:'index',width:25, label:'', formatter:radioButtonFmt },
			   {name:'ondCode',width:90, label:'Segment', formatter: segBreakFmt},
			   {name:'flightNoList',width:40, label:'Flight', formatter: arrBreakFmt},
			   {name:'carrierList',label:'Op.', width:20, formatter: carrierCodeFmt},
			   {name:'depatureList',label:'Departure', width:110, formatter: arrBreakFmt},
			   {name:'arrivalList',label:'Arrival', width:110, formatter: arrBreakFmt},
			   {name:'availabilityList',label:'Avl',width:40,align:'right', formatter: arrBreakFmt}
		   ],
			viewrecords: true,
			height: "auto",
			imgpath: 'themes/basic/images',
			hidegrid: false, 
			onSelectRow: function(rowid){
				var tableId = this.id;
//				var cont = $('#'+tableId).getCell(rowid, 'segment');
//				cont +='<span style="padding:0 10px"></span>' + $('#'+tableId ).getCell(rowid, 'flightNoList').replace(/<br>/gi, " - ");
//				cont +='<span style="padding:0 10px"></span>' + $('#'+tableId ).getCell(rowid, 'depatureList').replace(/<br>/gi, " - ");;
				$('#' + tableId + ' tbody:first-child').find("#"+rowid).find("input[type='radio']").attr("checked",true);
			//	createFareTable("#fareTable1","tableF1",cont); //TODO
				var id = tableId.split('_')[1];
				UI_tabRBDSearch.loadFareBCSuccess({id:id});
			},
			loadComplete: function(){
				//var tt = $('#table1 tbody:first-child tr:first').attr('id');
				//$('#table1 tbody:first-child tr:first').find("input[type='radio']").attr("checked",true);
				//jQuery("#table1").setSelection(tt, true);
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
		UI_commonSystem.fillGridData({id:'#flightTable_'+i, data:dtoRecord.flightInfo});
	}
	
};


UI_tabRBDSearch.addBusCarrierCodes = function(newCodes){
	for(var j=0; j < newCodes.length; j++){
		var found = false;
		for(var i =0; i < UI_tabRBDSearch.busCarrierCodes.length; i++){
			if(newCodes[j] == UI_tabRBDSearch.busCarrierCodes[i] ){
				found = true;
				break;
			}
		}
		if(!found){
			UI_tabRBDSearch.busCarrierCodes[UI_tabRBDSearch.busCarrierCodes.length] = newCodes[j];
		}
	}
}
UI_tabRBDSearch.getBusCarrierCode = function(carrierCode){
	if(UI_tabSearchFlights.isBusCarrierCode(carrierCode)){
		return '<img border="0" src="../images/AA169_B_no_cache.gif" id="img_0_12">';
	}
	return carrierCode;
}

UI_tabRBDSearch.isBusCarrierCode = function(carrierCode){
	for(var i =0; i < UI_tabRBDSearch.busCarrierCodes.length; i++){
		if(carrierCode == UI_tabRBDSearch.busCarrierCodes[i] ){
			return true;
		}
	}
	return false;
}

UI_tabRBDSearch.loadFareBCSuccess = function(params){
	var fareDetails =[ [{segCode:'SHJ/CMB', fares:[
		{id:"V2",avs:"<a class='selV'>V</a> 2",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE1",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"V6",avs:"<a class='selV'>V</a> 7",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE2",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"H8",avs:"<a class='selV'>H</a> 8",adl:"990",chd:"1090",inf:"470",owrt:"RT",basis:"XLE1AE4",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"H9",avs:"<a class='selV'>H</a> 9",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE3",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"V8",avs:"<a class='selV'>V</a> 8",adl:"990",chd:"1090",inf:"470",owrt:"RT",basis:"XLE1AE4",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"V9",avs:"<a class='selV'>V</a> 9",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE3",validity:"29/11-28/12",combine:"N L P I J"}
		]}], [{segCode:'SHJ/CMB', fares:[
		{id:"V2",avs:"<a class='selV'>V</a> 2",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE1",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"V6",avs:"<a class='selV'>V</a> 7",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE2",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"H8",avs:"<a class='selV'>H</a> 8",adl:"990",chd:"1090",inf:"470",owrt:"RT",basis:"XLE1AE4",validity:"29/11-28/12",combine:"N L P I J"},
		{id:"H9",avs:"<a class='selV'>H</a> 9",adl:"990",chd:"990",inf:"470",owrt:"OW",basis:"XLE1AE3",validity:"29/11-28/12",combine:"N L P I J"}
		]}]];
	params.data = fareDetails[params.id];
	UI_tabRBDSearch.buildFareGrid(params);
};

UI_tabRBDSearch.buildFareGrid = function(params){
	$('#fareTable_'+params.id).html('');
	for(var i = 0 ; i < params.data.length ; i++){
		
		$('#fareTable_'+params.id).append('<table id="fareBCTable_'+params.id + '_' + i + '" class="scroll" ></table>');
		$("#fareBCTable_"+params.id+ '_' + i ).jqGrid({
			datatype: 'local',
			colNames:['AVS','ADL','CHD','INF','OW/RT','Basis','Validity','Combine'],
			colModel :[ 
			           {name:'avs',width:50, label:'AVS',align:'center'},
			           {name:'adl',label:'ADL', width:40,align:'right'},
			           {name:'chd',label:'CHD', width:40,align:'right'},
			           {name:'inf',label:'INF', width:40,align:'right'},
			           {name:'owrt',label:'OW/RT', width:60,align:'center'},
			           {name:'basis',label:'Basis',width:60,align:'center'},
			           {name:'validity',label:'Validity',width:80,align:'center'},
			           {name:'combine',label:'Combine',width:90,align:'center'}
			           ],
			           viewrecords: true,
			           height: "100",
			           hidegrid: false, 
			           imgpath: 'themes/basic/images',
			           caption: params.data[i].segCode,
			           onSelectRow: function(rowid){
			        	   var tableId = this.id;
			        	   var slObj = {classItem:"HK", bookingClass:$('#'+tableId).find("#"+rowid).find("a.selV").html(),
			        			   adlfare:"AED " + $('#'+tableId).getCell(rowid, 'adl'),
			        			   inffare:"AED " + $('#'+tableId).getCell(rowid, 'adl')};
			        	   //		if (tabelID == "tableF1"){
			        	   //			updateSelectedTable(slObj,"outBound")
			        	   //		}else{
			        	   //			updateSelectedTable(slObj,"inBound")
			        	   //		}
			        	   
			           },
			           loadComplete: function(){ 
			        	   //var tt = $("#"+tabelID+" tbody:first-child tr:first").attr('id')
			        	   //$("#"+tabelID).setSelection(tt, true);
			        	   //enebleTooltip();
			           },
			           onCellSelect:function(rowID,iCol,cellcontent,e){
			        	   hideFareQuote();
			           }
		});
		UI_commonSystem.fillGridData({id:'#fareBCTable_'+params.id+ '_' + i , data:params.data[i].fares});
	}
};


//Initialize on load
$(function(){
	UI_tabRBDSearch.ready();
});
 