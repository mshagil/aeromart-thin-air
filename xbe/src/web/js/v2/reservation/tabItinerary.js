	/*
	*********************************************************
		Description		: XBE Make Reservation - Itinerary Tab
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
	
	/* Booking Details */
	var jsonITN = ({PNR:"",
					status : "",
					bookingDate : ""
					});
	
	var jsonITNFlexiDetails = [];

	/* Flight Details */
	var jsonITNDepFltDetails = [];
	var jsonITNRetFltDetails = [];
	
	/* Ancillary data */
	var jsonInsurances = null;
							
	/* Payment Details */
	var jsonITNPayDetails =  ({payDetails : [
									],
								currency : "",
								totalAmount : 0
								});
								
	/* Contact info */
	var jsonITNPaxProfile = ({title:"",
							firstName:"",
							lastName:"",
							street:"",
							address:"",
							city:"",
							country:"",
							mobileCountry:"",
							mobileArea:"",
							mobileNo:"",
							phoneCountry:"",
							phoneArea:"",
							phoneNo:"",
							faxCountry:"",
							faxArea:"",
							faxNo:"",
							email:"",
							emgnTitle:"",
							emgnFirstName:"",
							emgnLastName:"",
							emgnPhoneCountry:"",
							emgnPhoneArea:"",
							emgnPhoneNo:"",
							emgnEmail:""
							});

	/* Pax Detailso */							
	var jsonITNPax = [
					{type : "",
					 itnPaxDetails : [
								]}
					];		
	var jsonITNAnciAvail = {};
	
	var jsonEnableReceptPrint = null;

	var paymentGatewayBillNumber;
	var paymentGatewayBillNumberLabel;

	var jsonEnableItineryPrint = null;
	
	var jsonTaxInvoices = [];
	
	UI_tabItinerary.grpBookingPax = null;
	UI_tabItinerary.printIndItinerary = (top.printIndItinerary == true || top.printIndItinerary == 'true');
	UI_tabItinerary.grpPrintByIndItinerary = false; 
	UI_tabItinerary.isCreditDiscount = false;
						
	/*
	 * Passenger Tab
	 */
	function UI_tabItinerary(){}
	
	/*
	 * passenger Informations Tab Page On Load
	 */
	UI_tabItinerary.ready = function(){		
		UI_tabItinerary.checkPrintPrivilige();
		$("#btnPrint").decorateButton();
		$("#btnPrintReciept").decorateButton();		
		$("#btnPaxItiPrint").decorateButton();
		$("#btnPaxItiEmail").decorateButton();
		$("#btnPaxItiIndPrint").decorateButton();
		
		$("#btnPrintInvoice").decorateButton();
		$("#btnPrintInvoice").attr('style', 'width:auto;');
		$("#btnPrintInvoice").enable();
		
		$("#btnPrint").click(function(){UI_tabItinerary.viewItineraryOnClick();});
		$("#btnPrintInvoice").click(function(){UI_tabItinerary.viewInvoicesOnClick();});
		$("#btnPaxItiPrint").click(function() {UI_tabItinerary.printGroupItineraryOnClick();});
		$("#btnPaxItiIndPrint").click(function() {UI_tabItinerary.printIndItineraryOnClick();});
		$("#btnPrintReciept").click(function(){UI_tabItinerary.printRecieptOnClick()});
		$("#btnInvoicePrint").click(function() {UI_tabItinerary.printTaxInvoiceOnClick();});
		
		UI_tabItinerary.fillDropDownData();
		
		
		/* TODO remove this */
		UI_tabItinerary.itineraryDetailsOnLoadCall();
		UI_commonSystem.pageOnChange();
		
		if(contactConfig != null && contactConfig != ""){
			if(!(contactConfig.firstName2.xbeVisibility ||
					contactConfig.lastName2.xbeVisibility ||
					contactConfig.phoneNo2.xbeVisibility)){
				$("#trItnrEmgnContactDetails").hide();
			}
		}		
		
		$("#itineraryPNRLink").focus();
	}
	
	/*
	 * Print On Click
	 */
	UI_tabItinerary.printClick = function(){
		
		var includePaxFinancials = $("#chkPax").prop("checked");
		var itineraryLanguage = $("#selITNLangIte").val();
		
		if (itineraryLanguage == null)
			itineraryLanguage = $("#selLang").val();
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}
		
		var strUrl = "printItinerary.action"
		+ "?hdnPNRNo=" + jsonITN.PNR
		+ "&groupPNR=" + jsonITN.groupPNR
		+ "&includePaxFinancials=" + includePaxFinancials
		+ "&itineraryLanguage=" + itineraryLanguage
		+ "&includeTermsAndConditions=" +includeTCDetails
		+ "&operationType=" + "CREATE_BOOKING";
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (window.focus) {top.objCW.focus()}
	}
	
	/*
	 * Enable Print according to the privilege
	 */
	UI_tabItinerary.checkPrintPrivilige = function(){
		if(jsonITN.status == 'ONHOLD' && !top.arrPrivi[PRIVI_ALLOW_ON_HOLD_PRINT] == 1){			
			$("#btnPrint").disable();
			$("#selITNLangIte").disable();
			$("#chkPax").disable();
		} else {			
			$("#btnPrint").enable();
			$("#selITNLangIte").enable();
			$("#chkPax").enable();
		}
	}
	
	/*
	 * This method will call once booking successfully completion
	 */ 
	UI_tabItinerary.itineraryDetailsOnLoadCall = function(response){
		var splittedBooking = false;
		if (response != null){
			jsonITN = response.bookingTO;
			jsonITNDepFltDetails = response.flightDepDetails;
			jsonITNRetFltDetails = response.flightRetDetails;
			jsonITNPaxProfile = response.contactInfoTO;
			jsonITNPax = UI_tabItinerary.summarizeAnciData(response.passengerDetails);
			
			jsonTaxInvoices = response.taxInvoices;
			if (jsonTaxInvoices == undefined || jsonTaxInvoices == null || jsonTaxInvoices.length == 0){
				$("#btnPrintInvoice").hide();
				$("#lblTaxInvoice").hide();
			}
			
			jsonITNAnciAvail = response.ancillaryAvailability;
			jsonITNPayDetails = response.itineraryPaymentSummaryTO;
			jsonITNFlexiDetails = response.flexiRuleDetails;
			jsonInsurances = response.insurances;
			splittedBooking = response.blnSplitBooking;
			jsonEnableReceptPrint = response.enableRecieptGeneration;
			paymentGatewayBillNumber = response.paymentGatewayBillNumber;
			paymentGatewayBillNumberLabel = response.paymentGatewayBillNumberLabel;
			jsonEnableItineryPrint = response.blnPrintItinerary;
			UI_tabItinerary.isCreditDiscount = response.creditDiscount;
			UI_makeReservation.tabDataRetrive.tab4 = false;
			UI_makeReservation.tabStatus.tab4 = true;
			UI_makeReservation.openTab(4);
			
			if($("#selLang").val() != null && $("#selLang").val() != ''){
				$("#selITNLang").val($("#selLang").val());
				$("#selITNLangIte").val($("#selLang").val());
			} else {
				if(DATA_ResPro["systemParams"] != null){
					$("#selITNLang").val((DATA_ResPro["systemParams"]).defaultLanguage);
					$("#selITNLangIte").val((DATA_ResPro["systemParams"]).defaultLanguage);
				}				
			}
			if(response.offlinePayment == true){
				$("#offlinePaymentMsg").show();
				$("#lblREF").show();
				$("#divREF").show();
			} else {
				$("#offlinePaymentMsg").hide();
			}
			
			$("#trSplitBooking").hide();

			if($("#chkPrint").attr('checked')) {
				$("#selITNLangIte").val($("#hdnPrintItineraryLang").val());
				UI_tabItinerary.printClick();
			}
			UI_tabPayment.resetCCOnClick();
			UI_tabPassenger.resetOnClick();
			UI_tabPayment.resetOnClick();
			UI_tabSearchFlights.resetOnClick();
			
			if(jsonEnableReceptPrint == 'Y' ) {				
				$("#btnPrintReciept").show();
			} else {
				$("#btnPrintReciept").hide();
			}

			if (paymentGatewayBillNumber) {
				$("#paymentGatewayBillNumber").text(paymentGatewayBillNumber);
				if(paymentGatewayBillNumberLabel) {
					$("#paymentGatewayBillNumberLabel").text(geti18nData(paymentGatewayBillNumberLabel, 'Bill Number :'));
				}
				$("#trPaymentGatewayBillNumber").show();
			} else {
				$("#trPaymentGatewayBillNumber").hide();
			}
			
			//clearing the pax info
			jsonPaxAdtObj = ({"paxAdults":[], "paxInfants":[]});
			//Reset sales point value
			if ($("#selSalesPoint").length > 0){
				$("#selSalesPoint").val("");
			}
			
			if(response.fatouratiRef != null && response.fatouratiRef != ""){
				$("#divREF").html(response.fatouratiRef);
			}
			
			// Control print option for on-hold
			UI_tabItinerary.checkPrintPrivilige();
		}
		
		$("#divPNR").html("<a id='itineraryPNRLink' href='#' tabIndex='1'>" + jsonITN.PNR + "<\/a>");
		$("#itineraryPNRLink").click(function(){
			UI_tabItinerary.loadModifyBooking(jsonITN.PNR);
		});
		
		$("#divStatus").html("("+jsonITN.status +")");
		
		
				
		if(jsonITN.releaseTime != null && jsonITN.releaseTime != ""){
			$("#lblRelease").show();
			$("#divRelease").show();
			$("#divRelease").html(jsonITN.releaseTime);
		}else {
			$("#lblRelease").hide();
			$("#divRelease").hide();
		}
		
		UI_tabItinerary.buildFlightDetails("#itnFlightDetails", "#itnFlightHead");
		UI_tabItinerary.buildPaymentDetails();
		UI_tabItinerary.buildFlexiDetails(jsonITNFlexiDetails,"#flexiData");		
		UI_tabItinerary.fillContactInfo();
		UI_tabItinerary.fillPassengerDetails(jsonITNPax, "#itnPaxDet");
		UI_tabItinerary.fillAnciDetails();
		UI_tabSearchFlights.initialization();
		if(splittedBooking){
			$("#trSplitBooking").show();
			$("#divItnPane").css('height','350px');
			$("#trCNFPaX").css('height','355px');
			$("#divSplitPNR").html("<a id='itineraryPNRLink' href='#' onclick='UI_tabItinerary.loadModifyBooking(" + response.splitBookingTO.PNR + "); return false;' tabIndex='1'>" + response.splitBookingTO.PNR + "<\/a>");
			$("#divSplitStatus").html("("+response.splitBookingTO.status +")");
			
			
			
			if(response.splitBookingTO.releaseTime != null && response.splitBookingTO.releaseTime != ""){
				$("#lblSplitRelease").show();
				$("#divSplitRelease").show();
				$("#divSplitRelease").html(response.splitBookingTO.releaseTime);
			}else {
				$("#lblSplitRelease").hide();
				$("#lblSplitRelease").hide();
			}	
			if(!jsonEnableItineryPrint){
				$("#btnPrint").hide();
			}
			
			UI_tabItinerary.buildFlightDetails("#itnSplitFlightDetails", "#itnSplitFlightHead");
			UI_tabItinerary.fillPassengerDetails(response.splitPassengerDetails, "#itnSplitPaxDet");
		}
		UI_tabPayment.resetOnClick();
	}
	/*
	 * Fill insurance data 
	 */
	UI_tabItinerary.fillAnciDetails = function(){
		var policyStr = "";
		var tracker = {};
		
		if(jsonInsurances!=null && jsonInsurances.length > 0 ){
			$.each(jsonInsurances, function (key, insObject){
				var policyCode = insObject.policyCode;
				if(policyCode!= null && policyCode != ''){
					if (!tracker[policyCode]){
						tracker[policyCode] = true;
						if(policyStr.length > 0){
							policyStr += ",";
						}
						policyStr += policyCode;
					}
				 }
			});
		}
		
		if(policyStr.length > 0){
			$('#trInsurance').show();
			$('#divPolicyCode').html(policyStr);
		} else {
			$('#trInsurance').hide();
		}
			
	}
	
	/*
	 * Load Modify Booking
	 */
	UI_tabItinerary.loadModifyBooking = function(loadPnr){
		top.blnInterLine = jsonITN.groupPNR;
		top.loadNewModifyBooking(loadPnr, false);
	}
	
	/*
	 * Flight details
	 */
	UI_tabItinerary.buildFlightDetails = function(trFltdetail, trfltHead){
		$(trfltHead).hide();
		
		/* Initialize the Details */
		$(trFltdetail).find("tr").remove();
		
		var objFltDt = jsonITNDepFltDetails;
		var objRetFltDt = jsonITNRetFltDetails;
		var intLen = objFltDt.length;
		
		var strFltInfoDisplayMouseOver = "";
		var strFltInfoDisplayMouseOut = ""; 
		
		if(DATA_ResPro.initialParams.fltStopOverDurInfoToolTipEnabled){
			strFltInfoDisplayMouseOver =  UI_tabItinerary.displayFlightAddInfoToolTip;
			strFltInfoDisplayMouseOut  =  UI_tabItinerary.hideFlightAddInfoToolTip;
		}	
		
		
		var i = 0;
		var tblRow = null;
		if (intLen > 0){
			do{
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:objFltDt[i].type, css:UI_commonSystem.strDefClass + " thinBorderL rowHeight lightBG txtWhite", bold:true, colSpan:8}));
				$(trFltdetail).append(tblRow);
				
				var flights = objFltDt[i].flights;
				var flightsLen = objFltDt[i].flights.length;
				var j = 0;
				
				do {
					tblRow = document.createElement("TR");
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].orignNDest, width:"17%", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", align:"center"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].flightNo, width:"7%",  css:UI_commonSystem.strDefClass, align:"center",id:"td_OB_" + i + "_"+j,mouseOver:strFltInfoDisplayMouseOver,mouseOut:strFltInfoDisplayMouseOut}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:UI_tabSearchFlights.getBusCarrierCode(flights[j].airLine), width:"7%",  css:UI_commonSystem.strDefClass, align:"center", bold:true, textCss:"color:red;"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].departureText, width:"7%",  css:UI_commonSystem.strDefClass, align:"center", bold:true}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].departureDate + " " + flights[j].departureTime, width:"22%",  css:UI_commonSystem.strDefClass, align:"center"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].arrivalText, width:"7%",  css:UI_commonSystem.strDefClass, align:"center", bold:true}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].arrivalDate + " " + flights[j].arrivalTime, width:"22%",  css:UI_commonSystem.strDefClass, align:"center"}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].status, width:"13%",  css:UI_commonSystem.strDefClass, align:"center"}));
					$(trFltdetail).append(tblRow);
					
					tblRow = document.createElement("TR");
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", css:"singleGap", colSpan:8}));
					$(trFltdetail).append(tblRow);

					j++;
				} while (--flightsLen)
				
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:"", css:"singleGap", colSpan:8}));
				$(trFltdetail).append(tblRow);
				
				i++;
			}while(--intLen);
		}
		
		i = 0;
		tblRow = null;
		intLen = objRetFltDt.length;
		if (intLen > 0){
			do{
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:objRetFltDt[i].type, css:UI_commonSystem.strDefClass + " thinBorderL rowHeight lightBG txtWhite", bold:true, colSpan:8}));
				$(trFltdetail).append(tblRow);
				
				var flights = objRetFltDt[i].flights;
				var flightsLen = objRetFltDt[i].flights.length;
				var j = 0;
				
				do {
					tblRow = document.createElement("TR");
					
					if(flights[j].status == 'OPENRT' ){
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].orignNDest, width:"17%", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", align:"center"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:" " , width:"7%",colSpan:6,  css:UI_commonSystem.strDefClass, align:"center"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].status, width:"13%",  css:UI_commonSystem.strDefClass, align:"center"}));
					}else{
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].orignNDest, width:"17%", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", align:"center"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].flightNo, width:"7%",  css:UI_commonSystem.strDefClass, align:"center",id:"td_IB_" + i + "_"+j,mouseOver:strFltInfoDisplayMouseOver,mouseOut:strFltInfoDisplayMouseOut}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:UI_tabSearchFlights.getBusCarrierCode(flights[j].airLine), width:"7%",  css:UI_commonSystem.strDefClass, align:"center", bold:true, textCss:"color:red;"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].departureText, width:"7%",  css:UI_commonSystem.strDefClass, align:"center", bold:true}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].departureDate + " " + flights[j].departureTime, width:"22%",  css:UI_commonSystem.strDefClass, align:"center"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].arrivalText, width:"7%",  css:UI_commonSystem.strDefClass, align:"center", bold:true}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].arrivalDate + " " + flights[j].arrivalTime, width:"22%",  css:UI_commonSystem.strDefClass, align:"center"}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:flights[j].status, width:"13%",  css:UI_commonSystem.strDefClass, align:"center"}));
					}
					$(trFltdetail).append(tblRow);
					
					tblRow = document.createElement("TR");
					tblRow.appendChild(UI_commonSystem.createCell({desc:"", css:"singleGap ", colSpan:8}));
					$(trFltdetail).append(tblRow);

					j++;
				} while (--flightsLen)
				
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:"", css:"singleGap ", colSpan:8}));
				$(trFltdetail).append(tblRow);
				
				i++;
			}while(--intLen);
			
			if(UI_tabSearchFlights.isOpenReturn){
				// TODO Retrieve data using load reservation 
				var confirmExpiryDate = "";
				var travelExpiryDate = "";
				var openReturnInfo = UI_tabSearchFlights.openReturnInfo;
				if (openReturnInfo != null && openReturnInfo != "") {
					confirmExpiryDate = openReturnInfo.confirmExpiryDate;
					travelExpiryDate = openReturnInfo.travelExpiryDate;
				}
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:"", css:"singleGap thinBorderB", colSpan:8}));
				$(trFltdetail).append(tblRow);
				
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:'Confirm Before', width:"17%",  css:UI_commonSystem.strDefClass + " rowHeight", align:"left", bold:true}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:confirmExpiryDate, colSpan:7,  css:UI_commonSystem.strDefClass, align:"left"}));
				$(trFltdetail).append(tblRow);
				
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:'Travel Expiry', width:"17%",  css:UI_commonSystem.strDefClass + " rowHeight", align:"left", bold:true}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:travelExpiryDate, colSpan:7,  css:UI_commonSystem.strDefClass, align:"left"}));
				$(trFltdetail).append(tblRow);
				
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:"", css:"singleGap", colSpan:8}));
				$(trFltdetail).append(tblRow);
			}
		}
	}
	
	/*
	 * Payment Details
	 */
	UI_tabItinerary.buildPaymentDetails = function(){
		
		var discountSign = ''
		var lblTotDisc = top.arrError['XBE-LABEL-02'];
		if(!UI_tabItinerary.isCreditDiscount){
			if( jsonITNPayDetails.totalDiscount != "0.00"){
				discountSign = '-';
			}
			lblTotDisc = top.arrError['XBE-LABEL-01'];
			$("#trITNDiscount").show();
			$("#trITNDiscountCredit").hide();
		} else {
			$("#trITNDiscountCredit").show();
			$("#trITNDiscount").hide();
		}
		
		$(".divITNFareDiscount").html(discountSign + jsonITNPayDetails.totalDiscount)
		$(".lblTotalFareDiscount").html(lblTotDisc);
		
		$("#divITNTotalAmount").html(jsonITNPayDetails.totalAmount + " " + jsonITNPayDetails.currency)
		
		if(jsonITNPayDetails.selCurrency != null && jsonITNPayDetails.selCurrency != "" && jsonITNPayDetails.selCurrency !=  jsonITNPayDetails.currency) {
			$("#lblselTotalAmt").html("&nbsp;<b>("+jsonITNPayDetails.totalCurrencyAmount + " " + jsonITNPayDetails.selCurrency+")</b>");
		}
		
		/* Initialize the Details */
		$("#itnPriceDetails").find("tr").remove();
		
		var objPDt = jsonITNPayDetails.payDetails;
		var intLen = objPDt.length;
		var i = 0;
		var tblRow = null;
		if (intLen > 0){
			do{
				tblRow = document.createElement("TR");
				tblRow.appendChild(UI_commonSystem.createCell({desc:objPDt[i].description, css:UI_commonSystem.strDefClass + " thinBorderL rowHeight"}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:objPDt[i].total, css:UI_commonSystem.strDefClass, align:"right"}));
				$("#itnPriceDetails").append(tblRow);
				
				i++;
			}while(--intLen);
		}
	}
	
	/*
	 * Contact Details
	 */
	UI_tabItinerary.fillContactInfo = function(){
		if(contactConfig != null && contactConfig != ""){
			if(contactConfig.firstName1.xbeVisibility){				
				$("#divCDFirstName").html(jsonITNPaxProfile.firstName);
			} else {
				$("#tblItnrFName").hide();
			}
			
			if(contactConfig.lastName1.xbeVisibility){					
				$("#divCDLastName").html(jsonITNPaxProfile.lastName);
			} else {
				$("#tblItnrLName").hide();
			}
			
			if(contactConfig.address1.xbeVisibility){				
				$("#divCDStreet").html(jsonITNPaxProfile.street);
				$("#divCDAddress").html(jsonITNPaxProfile.address);
			} else {
				$("#tblItnrAddr1").hide();
				$("#tblItnrAddr2").hide();
			}
			
			if(contactConfig.city1.xbeVisibility){					
				$("#divCDCity").html(jsonITNPaxProfile.city);
			} else {
				$("#tblItnrCity").hide();
			}
			
			if(contactConfig.country1.xbeVisibility){					
				$("#divCDCountry").html(jsonITNPaxProfile.country);
			} else {
				$("#tblItnrCountry").hide();
			}
			
			if(contactConfig.email1.xbeVisibility){					
				$("#divCDEmail").html(jsonITNPaxProfile.email);
			} else {
				$("#tblItnrEmail").hide();
			}
			
			if(contactConfig.mobileNo1.xbeVisibility){					
				$("#divCDMobileNo").html(jsonITNPaxProfile.mobileNo);
			} else {
				$("#tblItnrMobile").hide();
			}
			
			if(contactConfig.phoneNo1.xbeVisibility){					
				$("#divCDPhoneNo").html(jsonITNPaxProfile.phoneNo);
			} else {
				$("#tblItnrPhone").hide();
			}
			
			if(contactConfig.fax1.xbeVisibility){					
				$("#divCDFaxNo").html(jsonITNPaxProfile.faxNo);
			} else {
				$("#tblItnrFax").hide();
			}		
			
			if(contactConfig.taxRegNo1.xbeVisibility){					
				$("#divTaxRegNo").html(jsonITNPaxProfile.taxRegNo);
			} else {
				$("#tblItnrTaxRegNo").hide();
			}
			
			if(contactConfig.state1.xbeVisibility){					
				$("#divState").html(jsonITNPaxProfile.state);
			} else {
				$("#divState").hide();
			}
		
			if(contactConfig.firstName2.xbeVisibility ||
					contactConfig.lastName2.xbeVisibility ||
					contactConfig.phoneNo2.xbeVisibility){
				
				if(contactConfig.firstName2.xbeVisibility){					
					$("#divEmgnDispFName").html(jsonITNPaxProfile.emgnFirstName);
				} else {
					$("#tblItnrEmgnFName").hide();
				}
				
				if(contactConfig.lastName2.xbeVisibility){
					$("#divEmgnDispLName").html(jsonITNPaxProfile.emgnLastName);
				} else {
					$("#tblItnrEmgnLName").hide();
				}
				
				if(contactConfig.phoneNo2.xbeVisibility){
					$("#divEmgnDispPhoneNo").html(jsonITNPaxProfile.emgnPhoneNo);
				} else {
					$("#tblItnrEmgnPhone").hide();
				}
				
				if(contactConfig.email2.xbeVisibility){
					$("#divEmgnDispEmail").html(jsonITNPaxProfile.emgnEmail);
				} else {
					$("#tblItnrEmgnEmail").hide();
				}			
			} else {
				$("#trItnrEmgnContactDetails").hide();
			}
		}		
	}
	
	/*
	 * Fill Drop down data for the Passenger tab
	 */
	UI_tabItinerary.fillDropDownData = function(){
		$("#selITNLangIte").fillDropDown({dataArray:top.jsonItnLang, keyIndex:"id", valueIndex:"desc", firstEmpty:false});
	}
	/*
	 * Summarize Ancillary data to display
	 */
	UI_tabItinerary.summarizeAnciData = function(objPDt){
		for(var x = 0 ; x < objPDt.length; x++){
			var hasSeat = false;
			var hasMeal = false;
			var hasSSR = false;
			var hasBaggage = false;
			var hasAirportService = false;
			var hasAirportTransfer = false;

			for(var i = 0 ; i < objPDt[x].paxCollection.length; i++){
				var pax = objPDt[x].paxCollection[i];
				for(var j =0 ; j < pax.selectedAncillaries.length; j++){
					var anci = pax.selectedAncillaries[j];
					anci.seat = '';
					if(anci.airSeatDTO!=null && anci.airSeatDTO.seatNumber!=null && anci.airSeatDTO.seatNumber!=''){
						anci.seat = anci.airSeatDTO.seatNumber;
						hasSeat = true;
					}
					var meals = '';
					for(var k = 0 ; k < anci.mealDTOs.length; k++){
						if(anci.mealDTOs[k].mealName!=null && anci.mealDTOs[k].mealName != ''){
							if(k>0){
								meals += ', ';
							}
							meals += anci.mealDTOs[k].soldMeals + 'x ' + anci.mealDTOs[k].mealName;
							hasMeal = true;
						}
					}
					anci.meals = meals;

					var baggages = '';
					for(var k = 0 ; k < anci.baggageDTOs.length; k++){
						if(anci.baggageDTOs[k].baggageName!=null && anci.baggageDTOs[k].baggageName != ''){
							if(k>0){
								baggages += ', ';
							}
							baggages += anci.baggageDTOs[k].baggageName;
							hasBaggage = true;
						}
					}
					anci.baggages = baggages;
					
					var ssrs = '';
					for(var k = 0 ; k < anci.specialServiceRequestDTOs.length; k++){
						if(anci.specialServiceRequestDTOs[k].description!=null && anci.specialServiceRequestDTOs[k].description != ''){
							if(k>0){
								ssrs += ', ';
							}
							ssrs += anci.specialServiceRequestDTOs[k].description;
							if( anci.specialServiceRequestDTOs[k].text != null &&  anci.specialServiceRequestDTOs[k].text != ''){
								ssrs +=  (' - ' + anci.specialServiceRequestDTOs[k].text) ;
							}
							hasSSR = true;
						}
					}
					anci.ssrs = ssrs;
					
					var apss = '';
					for(var k = 0; k < anci.airportServiceDTOs.length; k++){
						if(anci.airportServiceDTOs[k].ssrName!=null && anci.airportServiceDTOs[k].ssrName != ''){
							if(k>0){
								apss += ', ';
							}
							apss += anci.airportServiceDTOs[k].ssrName;
							if( anci.airportServiceDTOs[k].airportCode != null &&  anci.airportServiceDTOs[k].airportCode != ''){
								apss +=  (' - ' + anci.airportServiceDTOs[k].airportCode) ;
							}
							hasAirportService = true;
						}
					}
					anci.apss = apss;
					
				var apts = '';
					for(var k = 0; k < anci.airportTransferDTOs.length; k++){
						if(anci.airportTransferDTOs[k].ssrName!=null && anci.airportTransferDTOs[k].ssrName != ''){
							if(k>0){
								apts += ', ';
							}
							if( anci.airportTransferDTOs[k].airportCode != null &&  anci.airportTransferDTOs[k].airportCode != ''){
								apts +=  ( anci.airportTransferDTOs[k].airportCode + ' - ') ;
							}
							apts += anci.airportTransferDTOs[k].ssrName;
							hasAirportTransfer = true;
						}
					}
					anci.apts = apts;
					
					anci.segCode = anci.flightSegmentTO.segmentCode;
				}
			}
			objPDt[x].hasSeat = hasSeat;
			objPDt[x].hasMeal = hasMeal;
			objPDt[x].hasBaggage = hasBaggage;
			objPDt[x].hasSSR = hasSSR;	
			objPDt[x].hasAPS = hasAirportService;	
			objPDt[x].hasAPT = hasAirportTransfer;
		
		}
		return objPDt;
	}
	
	/*
	 * Fill Passenger Details
	 */
	UI_tabItinerary.fillPassengerDetails = function(paxDetailList, trPaxDetail){
	
		/* Initialize the Details */
		$(trPaxDetail).find("tr").remove();
		
		var objPDt = paxDetailList;
		var anciAvail = jsonITNAnciAvail;		
		var intLen = objPDt.length;
		var i = 0;
		var tblRow = null;
		var strClass = "";
		
		if (intLen > 0){
			do{
				strClass = "";
				if (i == 0){
					strClass = " thinBorderT ";
				}
				tblRow = document.createElement("TR");
				var colSpan = 2;
				if(!objPDt[i].hasSeat && !objPDt[i].hasMeal && !objPDt[i].hasBaggage && !objPDt[i].hasSSR && !objPDt[i].hasAPS && !objPDt[i].hasAPT){
					colSpan  = 7;
				}
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:objPDt[i].type, colSpan:colSpan, css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", bold:true}));
				if(objPDt[i].hasSeat || objPDt[i].hasMeal || objPDt[i].hasBaggage || objPDt[i].hasSSR || objPDt[i].hasAPS || objPDt[i].hasAPT){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'Segment',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}
				if(anciAvail.seatAvailable && objPDt[i].hasSeat){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'Seat',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}
				if(anciAvail.mealAvailable && objPDt[i].hasMeal){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'Meal(s)',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}

				if(anciAvail.baggageAvailable && objPDt[i].hasBaggage){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'Baggage(s)',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}
				if(anciAvail.ssrAvailable && objPDt[i].hasSSR){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'SSR(s)',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}
				if(anciAvail.airportServiceAvailable && objPDt[i].hasAPS){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'Airport Service(s)',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}
				if(anciAvail.airportTransferAvailable && objPDt[i].hasAPT){
					tblRow.appendChild(UI_commonSystem.createCell({desc:'Airport Transfer(s)',  css:UI_commonSystem.strDefClass + strClass + " thinBorderL rowHeight", align:"center", bold:true}));
				}
				
				$(trPaxDetail).append(tblRow);
				
				if (objPDt[i].paxCollection != null){
					var objPaxDt = objPDt[i].paxCollection 
					var intDet = objPaxDt.length;
					if (intDet > 0){
						var x = 0;
						do{
							var anciSegCount = objPaxDt[x].selectedAncillaries.length;														
							objPaxDt[x].selectedAncillaries = $.airutil.sort.quickSort(objPaxDt[x].selectedAncillaries,UI_tabItinerary.segmentByDateComparator);
														
							tblRow = document.createElement("TR");
							tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].itnSeqNo, rowSpan: anciSegCount, width:"3%", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", align:"right"}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].itnPaxName,rowSpan: anciSegCount, colSpan:colSpan-1,width:"25%",  css:UI_commonSystem.strDefClass}));
							
							for(var k=0; k < anciSegCount; k++){
								if(k>0){
									$(trPaxDetail).append(tblRow);
									tblRow = document.createElement("TR");
								}
								if(objPDt[i].hasSeat || objPDt[i].hasMeal || objPDt[i].hasBaggage || objPDt[i].hasSSR || objPDt[i].hasAPS || objPDt[i].hasAPT){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].segCode,width:"8%",   css:UI_commonSystem.strDefClass, align:"center"}));
								}
								if(anciAvail.seatAvailable && objPDt[i].hasSeat){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].seat, width:"6%", css:UI_commonSystem.strDefClass, align:"center"}));
								}
								if(anciAvail.mealAvailable && objPDt[i].hasMeal){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].meals, width:"20%", css:UI_commonSystem.strDefClass}));
								}

								if(anciAvail.baggageAvailable && objPDt[i].hasBaggage){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].baggages, width:"7%", css:UI_commonSystem.strDefClass}));
								}
								if(anciAvail.ssrAvailable && objPDt[i].hasSSR){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].ssrs,width:"15%",  css:UI_commonSystem.strDefClass}));
								}
								if(anciAvail.airportServiceAvailable && objPDt[i].hasAPS){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].apss,width:"25%",  css:UI_commonSystem.strDefClass}));
								}
								if(anciAvail.airportTransferAvailable && objPDt[i].hasAPT){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objPaxDt[x].selectedAncillaries[k].apts,width:"20%",  css:UI_commonSystem.strDefClass}));
								}
								
							}
							$(trPaxDetail).append(tblRow);
							x++;
							
						}while(--intDet);
					}
				}
				
				i++;
			}while(--intLen);
		}
	}
	
	UI_tabItinerary.buildFlexiDetails = function(flexiDetails,trFlexiDetail){
		var len = flexiDetails.length;
		var tblRow = null;
		$(trFlexiDetail).find("tr").remove();
		if(len == 0){
			$("#flexiDetails").hide();
		}
		while(len > 0){
			var flexiRuleDetails = flexiDetails[len-1];
			tblRow = document.createElement("TR");
			tblRow.appendChild(UI_commonSystem.createCell({desc:flexiRuleDetails.originDestination + " " + flexiRuleDetails.flexiDetails,  css:UI_commonSystem.strDefClass + " " + " thinBorderL rowHeight", align:"left", bold:true}));
			len--;
			$(trFlexiDetail).append(tblRow);
		}
	}
	
	/*
	 * Anci segment comparator for bubble sort
	 */
	UI_tabItinerary.segmentByDateComparator = function (first ,second){
		var parser  = function(d){
			var a = d.split('T');
			var b = a[0].split('-');
			var c = a[1].split(':');
			return new Date(b[0],parseInt(b[1],10)-1,b[2],c[0],c[1],c[2]);
		}
		var d1 = parser(first.flightSegmentTO.departureDateTime);
		var d2 = parser(second.flightSegmentTO.departureDateTime);
		return (d1.getTime()-d2.getTime());
	}
	
	UI_tabItinerary.viewGroupItineraryOnClick = function(iniOpt) {
		
		var chkBoxesArray =new Array();
		
		$('#btnPaxItiPrint').decorateButton();
		$('#btnPaxItiCancel').decorateButton();
		$('#btnPaxItiEmail').decorateButton();
		$('#btnPaxItiIndPrint').decorateButton();
		$('#btnPaxItiIndEmail').hide();
		
		
		$('#btnPaxItiEmail').disable();
		$('#btnPaxItiPrint').disable();
		$('#btnPaxItiIndPrint').disable();
		
		$('#divGroupItineraryDisplay').show();
		$('#chkPaxSelectAll').attr('checked',false);
		
		$('#trPaxListTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('xxx_*');
	        return regex.test(this.id);
	    }).remove();
		
		var firstElement = null;
		var paxObj = jsonITNPax;
		var paxSeq = 0;
		for(var x=0; x<paxObj.length; x++){
			if(paxObj[x].type == 'Infant(s)'){
				continue;
			}
			
			var paxList = paxObj[x].paxCollection;
			for(var i=0; i < paxList.length; i++){				
				var tempId = 'xxx_'+paxList[i].itnSeqNo;
				var chkId = 'chkPax_'+paxList[i].itnSeqNo;
				
				var clone = $('#trPaxListTemp').clone().show();
				clone.attr('id',tempId);
				clone.appendTo($('#trPaxListTemp').parent());  
				
				var chBox = $('#'+tempId+' input');
				chBox.attr('id',chkId);
				chBox.attr('tabIndex',9000+i);
				chBox.val(paxList[i].itnSeqNo);			
				chBox.attr('style','display:show');			
				chBox.click(function(){
					var paxSelected = false;
					if(this.checked){
						$('#btnPaxItiEmail').enable();
						$('#btnPaxItiPrint').enable();
						$('#btnPaxItiIndPrint').enable();
					}
					
					for(var j=0; j<chkBoxesArray.length; j++){			
						var checkbox = chkBoxesArray[j];
						if(checkbox.get(0).checked){						
							paxSelected = true;						
						}			
					}
					
					if(paxSelected){					
						$('#btnPaxItiEmail').enable();
						$('#btnPaxItiPrint').enable();
						$('#btnPaxItiIndPrint').enable();
					} else{					
						$('#btnPaxItiEmail').disable();
						$('#btnPaxItiPrint').disable();
						$('#btnPaxItiIndPrint').disable();
					}
				});
				
				chkBoxesArray[paxSeq] = chBox;
				paxSeq++;
				
				if(firstElement==null){
					firstElement = chkId;
					chBox.attr('checked',true);  
				}
				
				var label = $('#'+tempId+' label');
				label.attr('id','lblPax_'+paxList[i].itnSeqNo);
				label.text(paxList[i].itnPaxName);			
				label.click(function(e){ var a = e.target.id.split('_'); $('#chkPax_'+a[1]).click()});				
			}
		}
			
		if(iniOpt == 1){
			$("#btnPaxItiPrint").show();
			$("#btnPaxItiEmail").hide()
			$('#btnPaxItiIndPrint').show();
		} else {
			$("#btnPaxItiPrint").hide();
			$("#btnPaxItiEmail").hide()
			$('#btnPaxItiIndPrint').hide();
		}
		
		for(var j=0; j<chkBoxesArray.length; j++){			
			var checkbox = chkBoxesArray[j];		
			if(checkbox.get(0).checked){				
				$('#btnPaxItiPrint').enable();				
				$('#btnPaxItiIndPrint').enable();
			}	
		}	
		
		UI_tabItinerary.grpBookingPax = chkBoxesArray;		
	}
   
   var chkBoxesArrayForPrintInvoice = new Array();
   UI_tabItinerary.viewInvoicesOnClick = function(iniOpt) {
	   $('#trInvoiceListTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('iii_*');
	        return regex.test(this.id);
	    }).remove();
	   var chkBoxesArray =new Array();
	   
	   $('#divInvoicesDisplay').show();
	   var firstElement = null;
	   var invoiceObj = jsonTaxInvoices;
	   var invoiceSeq = 0;
	   for(var i=0; i < invoiceObj.length; i++){
			
			var tempId = 'iii_'+invoiceObj[i].taxInvoiceId;
			var chkId = 'chkInvoice_'+invoiceObj[i].taxInvoiceId;
			
			var clone = $('#trInvoiceListTemp').clone().show();
			clone.attr('id',tempId);
			clone.appendTo($('#trInvoiceListTemp').parent());  
			
			var chBox = $('#'+tempId+' input');
			chBox.attr('id',chkId);
			chBox.attr('tabIndex',9000+i);
			chBox.val(invoiceObj[i].taxInvoiceId);			
			chBox.attr('style','display:show');			
			chBox.click(function(){

			});
			
			chkBoxesArray[invoiceSeq] = chBox;
			chkBoxesArrayForPrintInvoice[invoiceSeq] = chBox;
			invoiceSeq++;
			
			if(firstElement==null){
				firstElement = chkId;
				chBox.attr('checked',true);  
			}
			
			var label = $('#'+tempId+' label');
			label.attr('id','lblInvoice_'+invoiceObj[i].taxInvoiceId);
			label.text(invoiceObj[i].taxInvoiceNumber);			
			label.click(function(e){ var a = e.target.id.split('_'); $('#chkInvoice_'+a[1]).click()});				
	   }
	   
   }
  
      
   UI_tabItinerary.printGroupItineraryOnClick = function(){
		var grpBookingPax = UI_tabItinerary.grpBookingPax;
		var selectedPax = "";		
		for(var j=0; j<grpBookingPax.length; j++){			
			var checkbox = grpBookingPax[j];			
			if(checkbox.get(0).checked){				
				selectedPax += checkbox.get(0).value + ',';
			}			
		}		
				
		var includePaxFinancials = $("#chkPax").prop("checked");
		var itineraryLanguage = $("#selITNLangIte").val();
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}
		
		$('#divGroupItineraryDisplay').hide();
		
		
		if (UI_tabItinerary.printIndItinerary == true) {
			UI_tabItinerary.grpPrintByIndItinerary = true;
			UI_tabItinerary.printIndItineraryOnClick();
		} else {
			var strUrl = "printItinerary.action"
			+ "?hdnPNRNo=" + jsonITN.PNR
			+ "&groupPNR=" + jsonITN.groupPNR
			+ "&includePaxFinancials=" + includePaxFinancials
			+ "&itineraryLanguage=" + itineraryLanguage
			+ "&selectedPax=" + selectedPax
			+ "&includeTermsAndConditions=" +includeTCDetails
			+ "&operationType=" + "CREATE_BOOKING";
			
			top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
			if (window.focus) {top.objCW.focus()}
		}
	}
   
   UI_tabItinerary.printTaxInvoiceOnClick = function(){
	   var atLeastOneCheckBoxSelected = false;
	   var chkBoxesArray =new Array();
	   var selectedInvoiceIds = "";
	   chkBoxesArray = chkBoxesArrayForPrintInvoice;
	   for (var i=0;i<chkBoxesArray.length;i++){
		   if (chkBoxesArray[i][0].checked){
			   selectedInvoiceIds +=  chkBoxesArray[i][0].value + ",";
			   atLeastOneCheckBoxSelected = true;
		   }
	   }
	   UI_commonSystem.pageOnChange();
	   
	   var strUrl = "printTaxInvoice.action"
			      + "?hdnPNRNo=" + jsonITN.PNR
			      + "&groupPNR=" + jsonITN.groupPNR
			      + "&selectedTaxInvoices=" + selectedInvoiceIds
			      + "&invoiceLanguage=" + "";
	   
	   if (atLeastOneCheckBoxSelected){
		   $('#divInvoicesDisplay').hide();
		   top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		   $('#popup-content').clone().appendTo( top.objCW.document.body );
		   top.objCW.print();
	   } else {
		   alert("Please select atleast one check box.")
	   }
   }
   
   UI_tabItinerary.printIndItineraryOnClick = function(){
		var grpBookingPax = UI_tabItinerary.grpBookingPax;
		var selectedPax = "";		
		for(var j=0; j<grpBookingPax.length; j++){			
			var checkbox = grpBookingPax[j];			
			if (UI_tabItinerary.grpPrintByIndItinerary == true && UI_tabItinerary.printIndItinerary == true) {
				selectedPax += checkbox.get(0).value + ',';
			} else if(checkbox.get(0).checked){				
				selectedPax += checkbox.get(0).value + ',';
			}			
		}		
				
		var includePaxFinancials = $("#chkPax").prop("checked");
		var itineraryLanguage = $("#selITNLangIte").val();
		
		var includeTCDetails = true; // Default to true
		if($("#chkTC").length > 0) {
			includeTCDetails = $("#chkTC").prop("checked");
		}
		
		$('#divGroupItineraryDisplay').hide();
	
		var strUrl = "printItinerary!printIndividualPaxItinerary.action"
		+ "?hdnPNRNo=" + jsonITN.PNR
		+ "&groupPNR=" + jsonITN.groupPNR
		+ "&includePaxFinancials=" + includePaxFinancials
		+ "&itineraryLanguage=" + itineraryLanguage
		+ "&selectedPax=" + selectedPax
		+ "&includeTermsAndConditions=" +includeTCDetails
		+ "&operationType=" + "CREATE_BOOKING";
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (window.focus) {top.objCW.focus()}	
	}
	
   UI_tabItinerary.printRecieptOnClick = function(){
	   var itineraryLanguage = $("#selITNLangIte").val();
		
		var strUrl = "printReciept.action"
		+ "?hdnPNRNo=" + jsonITN.PNR
		+ "&groupPNR=" + jsonITN.groupPNR
		+ "&recieptLanguage=" + itineraryLanguage;
		
		top.objCW= window.open(strUrl,"myWindow",$("#popWindow").getPopWindowProp(630, 900, true));
		if (window.focus) {top.objCW.focus()}	
	}
	
	UI_tabItinerary.viewItineraryOnClick = function(){	  
	   if (UI_tabItinerary.printIndItinerary == true) {
	      UI_tabItinerary.viewGroupItineraryOnClick(1);
	   } else if(top.arrPrivi[PRIVI_ITN_SELECTPAX] == 1){
		   UI_tabItinerary.viewGroupItineraryOnClick(1);		   
	   } else {
		   UI_tabItinerary.printClick();
	   }
   }
	
	selectAllPaxOnClick = function(obj){	  
	   var grpBookingPax = UI_tabItinerary.grpBookingPax;
	   for(var i=0; i<grpBookingPax.length; i++){		   
		   var checkBox = grpBookingPax[i];		   
		   if(obj.checked == true){
				   checkBox.get(0).checked = true;		   	   
		   } else {
			   checkBox.get(0).checked = false;
		   }	   
	   }
	   if(obj.checked == true){
		   $('#btnPaxItiPrint').enable();
		   $('#btnPaxItiIndPrint').enable();		   
	   } else {
		   $('#btnPaxItiPrint').disable();
		   $('#btnPaxItiIndPrint').disable();
	   }
	   
   }
   
	/** Flight Info tool tip display*/
	UI_tabItinerary.displayFlightAddInfoToolTip = function() {
		  var sHtm  ="";
		  if (id != "") {
			  //id:"td_OB_" + i + "_"+j
			  var arrID = this.id.split("_");
			  
			  //var id = (new  Number(arrID[1])) - 0;
			  var flg = arrID[1];
			  var rowId = (new  Number(arrID[2])) - 0;
			  var id = (new  Number(arrID[3])) - 0;
			  
			  var fltDuration = "";
			  var fltModelDesc = "";
			  var fltModel = "";
			  var segmentCode = "";
			  var flightNO = "";
  			  var specialNote = "";
  			  var strData = "";
  			  var stopOverDuration ="";  			  
  			  var noOfStops ="";
  			  var arrivalTerminalName="";
  			  var departureTerminalName="";
  			  var csOcCarrierCode="";
  			  
			if(flg=="OB"){
				fltDetails = jsonITNDepFltDetails[rowId].flights;
			}else if(flg=="IB"){
				fltDetails = jsonITNRetFltDetails[rowId].flights;
			} 				  

 				 
 				  fltDuration = fltDetails[id].flightDuration;
 				  fltModelDesc = fltDetails[id].flightModelDescription;
 				  fltModel = fltDetails[id].flightModelNumber;
 				  segmentCode = fltDetails[id].orignNDest;
 				  flightNO = fltDetails[id].flightNo;	
 				  specialNote = fltDetails[id].remarks;
 				  stopOverDuration = fltDetails[id].flightStopOverDuration;	
 				  noOfStops = fltDetails[id].noOfStops;
 				  arrivalTerminalName = fltDetails[id].arrivalTerminal;
 				  departureTerminalName = fltDetails[id].departureTerminal;
 				  csOcCarrierCode = fltDetails[id].csOcCarrierCode;
 				  var strData = "";
 						
 					strData += "<font>Flight No&nbsp;:&nbsp;"+flightNO+"</font><br>";
 					strData += "<font>Segment Code&nbsp;:&nbsp;"+segmentCode+"</font><br>";
 					strData += "<font>Arrival Terminal&nbsp;:&nbsp;"+arrivalTerminalName+"</font><br>";	
 					strData += "<font>Departure Terminal&nbsp;:&nbsp;"+departureTerminalName+"</font><br>";	
 					strData += "<font>Flight Duration&nbsp;:&nbsp;"+fltDuration+"</font><br>";
 					strData += "<font>No of Stops&nbsp;:&nbsp;"+noOfStops+"</font><br>";
 					strData += "<font>Transit Duration&nbsp;:&nbsp;"+stopOverDuration+"</font><br>";				
 					//strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModel+"</font><br>";
 					strData += "<font>Aircraft Model&nbsp;:&nbsp;"+fltModelDesc+"</font><br>";
 					strData += "<font>Additional Note&nbsp;:&nbsp;"+specialNote+"</font><br>";
 					if(typeof(csOcCarrierCode) != "undefined" && csOcCarrierCode != "" && csOcCarrierCode != null){
 						strData += "<font>Operated By&nbsp;:&nbsp;"+csOcCarrierCode+"</font><br>";						
 					}

  			sHtm += strData;
			  
			  				
		  } 
		  $("#flightAddInfoToolTipBody").html(sHtm);
		  var pos = $("#"+ this.id).offset();	   
	    
	    
	    $("#flightAddInfoToolTip").css({
	        left: (pos.left-100) + 'px',
	        top: pos.top + 20 + 'px'
	       
	    });      
	   $("#flightAddInfoToolTip").show(); 	  
		}

	UI_tabItinerary.hideFlightAddInfoToolTip = function() {
		  $("#flightAddInfoToolTip").hide();
	}
	
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */