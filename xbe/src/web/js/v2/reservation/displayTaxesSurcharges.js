	/*
	***************************************************************************************
		Description		: XBE Make Reservation - Display Taxes and Surcharges
		Author			: Charith
		Version			: 1.0
		Created			: 4th Aug 2010
	***************************************************************************************	
	*/

	/*
	 * Display Taxes Surcharges breakdown
	 */
	function UI_displayTaxesSurcharges(){}	
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divBreakDownPane").fadeIn("slow");
		$("#btnClose").decorateButton();
		$("#btnClose").click(function(){UI_displayTaxesSurcharges.closeOnClick();});		
/*		$("#btnTaxB").blur();
		$("#btnSurB").blur();		*/
		UI_commonSystem.showProgress();
		
		var selCurrPreProcess = function(dataArr){
			$.each(dataArr, function(index, element){
				element['localCurrValue'] = element['amountInLocalCurrency']+ ' '+ element['localCurrencyCode'];
				if(element['localCurrencyCode'] != window.opener.jsonFareQuoteSummary.selectedCurrency){
					element['localCurrValue'] = element['localCurrValue']+'<br /> <span class="txtBold">['+element['amountInSelectedCurrency']+ ' ' +
					window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
				}
			});
			return dataArr;
		}
		
		var jsonBDTax = selCurrPreProcess(window.opener.UI_tabSearchFlights.jsonBDTax);
		var jsonBDSurcharge = selCurrPreProcess(window.opener.UI_tabSearchFlights.jsonBDSurcharge);
		
		/* Tax & Search Breakdown Grids */
		UI_displayTaxesSurcharges.constructBreakDownGrid({id:"#tblTaxBreakDown"});
		UI_displayTaxesSurcharges.constructBreakDownGrid({id:"#tblSurBreakDown"});		
		UI_displayTaxesSurcharges.buildBreakDownInfo({id:"#divTaxBD", idGrid:"#tblTaxBreakDown", data:jsonBDTax});
		UI_displayTaxesSurcharges.buildBreakDownInfo({id:"#divSurBD", idGrid:"#tblSurBreakDown", data:jsonBDSurcharge});
		UI_commonSystem.hideProgress();
	});
	
	/*
	 * Close window
	 */
	UI_displayTaxesSurcharges.closeOnClick = function(){
		window.close();
	}

	/*
	 * Build the Break down Informations
	 */
	UI_displayTaxesSurcharges.buildBreakDownInfo = function(inParams){
		$(inParams.id).show();
		UI_commonSystem.fillGridData({id:inParams.idGrid, data:inParams.data});
	}

	/*
	 * Break Down Grid 
	 */
	UI_displayTaxesSurcharges.constructBreakDownGrid = function(inParams){
		//construct grid
		var strCode = "surchargeCode";
		var strCaption =  getOpeneri18nData('tax_SurchargeBreakeDown','Surcharge Break down');
		if (inParams.id == "#tblTaxBreakDown"){
			strCode = "taxCode";
			strCaption = "Tax Break down";
		}
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 120,
			width: 350,
			colNames:[ getOpeneri18nData('tax_Applicable','Applicable'), getOpeneri18nData('tax_OperatingCarrier','Op. Carrier'), getOpeneri18nData('tax_Code','Code'), getOpeneri18nData('tax_Charge','Charge')],
			colModel:[
				{name:'applicableToDisplay',  index:'applicableToDisplay', width:100, align:"left"},
				{name:'carrierCode', index:'carrierCode', width:50, align:"left"},
				{name:strCode, index:strCode, width:70, align:"left"},
				{name:'localCurrValue', index:'localCurrValue', width:120, align:"right"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			caption:strCaption
		});
	}