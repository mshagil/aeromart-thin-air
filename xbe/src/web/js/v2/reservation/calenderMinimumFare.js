/*
 * Calender minimum fare search related methods
 * Author			: Sashrika Waidyarathna
 * Version			: 1.0
 * Created			: 11th June 2015
 */
function UI_MinimumFare() {
	this.minFareCalendar = null;
	this.calendarRS = null;
	this.searchResponse = null;
	this.segmentData = [];  
	this.departureDate = null;
	this.rphArray=[];
	this.fareQuoteResultFlightInfo=[];

	this.ready = function() {
		UI_tabSearchFlights.objResSrchParams={bookingType:$('#bookingType').val()}; 
		UI_tabSearchFlights.strPaxType = "A";   
		UI_tabSearchFlights.calenderMinimumFare = true;	
		UI_tabSearchFlights.disableEnablePageControls(true); //  check
		UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = false;
		UI_tabSearchFlights.ondWiseFlexiSelected[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = false;
		UI_tabSearchFlights.ondWiseFlexiQuote[UI_tabSearchFlights.OND_SEQUENCE.OUT_BOUND] = true;
		UI_tabSearchFlights.ondWiseFlexiQuote[UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND] = true;
		clearAllUI();		
		onChangeMethods();
		initilizeHTMLContent();
		wireActions();
		getReadyCalender();
		initilizetData(); 
	};
	
	function resetOnClick() {
		$("#trCheckStopOver").hide();
		$("#btnReset").blur();
		UI_tabSearchFlights.releaseBlockSeats();
		UI_tabSearchFlights.pgObj.lockTabs();
		$("#oldPerPaxFare").val('0');
		$("#oldPerPaxFareId").val('0');
		UI_tabSearchFlights.initialization();
		$("#divResultsPane").hide();
		$("#divTaxBD").hide();
		$("#divSurBD").hide();
		$("#divBreakDownPane").hide();
		$("#btnDetailedPrice").hide();
 
		UI_tabSearchFlights.hideAirportMessage();
		isLogicalCCSelected = false;
		
		UI_tabSearchFlights.resetBundledServiceSelection();
		UI_tabSearchFlights.resetFlexiSelection();
	};
	
	this.getFlightRPHList = function(){
		var rphArray = [];
		
		for ( var i = 0; i < UI_MinimumFare.fareQuoteResultFlightInfo.length; i++) {
			var outSegWaitListed = false;
			rphArray[rphArray.length] = {
				flightRPH : UI_MinimumFare.fareQuoteResultFlightInfo[i].flightRefNumber,
				segmentCode : UI_MinimumFare.fareQuoteResultFlightInfo[i].orignNDest,
				flightNo : UI_MinimumFare.fareQuoteResultFlightInfo[i].flightNo,  
				arrivalTime : UI_MinimumFare.fareQuoteResultFlightInfo[i].arrivalDateValue,
				departureTime : UI_MinimumFare.fareQuoteResultFlightInfo[i].departureDateValue,
				carrierCode : UI_MinimumFare.fareQuoteResultFlightInfo[i].airLine,
				departureTimeZulu : UI_MinimumFare.fareQuoteResultFlightInfo[i].departureDateZuluValue,
				arrivalTimeZulu : UI_MinimumFare.fareQuoteResultFlightInfo[i].arrivalDateZuluValue,
				returnFlag : UI_MinimumFare.fareQuoteResultFlightInfo[i].returnFlag,
				domesticFlight : '', //outFlt.domesticFlightList[i]
				ondSequence: UI_MinimumFare.fareQuoteResultFlightInfo[i].ondSequence,
				waitListed:outSegWaitListed   // do we need this????????????????
			};
		}
		UI_tabSearchFlights.fltRPHInfo = $.toJSON(rphArray);
		return rphArray;
	}
	
	/*
	 * Fare Quote Params
	 */
	this.createFareQuoteParams = function(){
		var fData = UI_MinimumFare.createSearchParams('fareQuoteParams');  // above part was not needed, so removed
		
		fData = $.airutil.dom.concatObjects(fData, UI_MinimumFare.inOutFlightRPHList());
		UI_tabSearchFlights.createSubmitData({
			data: fData,
			mode: UI_commonSystem.strPGMode
		});
		
		if(DATA_ResPro["modifyFlexiBooking"] != undefined &&
				DATA_ResPro["modifyFlexiBooking"]){
			var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
			$.each(ondFlexiQuote, function(key, value){
				ondFlexiQuote[key] = false;
			});
			fData['fareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(ondFlexiQuote);
		}
		
		return fData;
	}

	this.inOutFlightRPHList = function(){
		var fData = {};		
    		for(var i=0; i<UI_MinimumFare.rphArray.length; i++) {
    			// this will generate params like outFlights[i]=123123231
    			fData["outFlightRPHList[" + i + "]"]= UI_MinimumFare.rphArray[i];
    			//Check wailisting stutus in at least one segment
    			UI_tabSearchFlights.waitListedSegmentAvailable = true; // is this correct??
    		}
    		var outDate = UI_MinimumFare.rphArray[0].split("$")[3].substr(0,8);
    	    UI_tabSearchFlights.selectedOutDate = outDate.substring(6, 8) + "/" + outDate.substring(4, 6) +  "/" + outDate.substring(0, 4);    		
    		
    		fData["fareQuoteParams.searchSystem"] = "AA";
		
		
		return fData;
	}
	
	this.createSearchParams = function(varName){
		var pData = {};
		pData[varName+".fromAirport"]	= UI_MinimumFare.searchResponse.fromAirport;
		pData[varName+".toAirport"]		= UI_MinimumFare.searchResponse.toAirport;
		var parts = UI_MinimumFare.searchResponse.departureDate.split("-");


		pData[varName+".departureDate"]	= parts[2]+"/"+parts[1]+"/"+parts[0]; // may need to change the frmt
		pData[varName+".returnDate"]	= null;
		pData[varName+".validity"]		= 0;
		pData[varName+".adultCount"]	= UI_MinimumFare.searchResponse.adultCount;
		pData[varName+".childCount"]	= UI_MinimumFare.searchResponse.childCount;
		pData[varName+".infantCount"]	= UI_MinimumFare.searchResponse.infantCount;
		pData[varName+".classOfService"]= UI_MinimumFare.searchResponse.classOfService;
		pData[varName+".logicalCabinClass"] = '';
		pData[varName+".selectedCurrency"]= UI_MinimumFare.searchResponse.selectedCurrency;
		pData[varName+".bookingType"]	= $('#bookingType').val();   // HARD CODED MOCK VLAUE
		pData[varName+".paxType"]		= "A";   /// hard coded
		pData[varName+".fareType"]		= "A";
		pData[varName+".openReturn"]	= false;
		pData[varName+".searchSystem"]	= "AA";
		pData[varName+".fromAirportSubStation"]	='';
		pData[varName+".toAirportSubStation"]	= '';
		pData[varName+".openReturnConfirm"]	= false;
		pData[varName+".travelAgentCode"]	= '';
		pData[varName+".bookingClassCode"]	= '';	  // hardcoded	
		pData[varName+'.fareQuoteLogicalCCSelection'] = '';
		pData[varName+'.fareQuoteOndSegLogicalCCSelection'] = '';
		pData[varName+'.fareQuoteSegWiseLogicalCCSelection'] = '';
		pData[varName+'.ondQuoteFlexiStr'] = '';
		pData[varName+'.ondAvailbleFlexiStr'] = '';
		pData[varName+'.ondSelectedFlexiStr'] ='';
		pData[varName+'.promoCode'] = '';
		pData[varName+'.bankIdentificationNo'] = '';
		pData[varName+'.preferredBundledFares'] = '';
		pData[varName+'.preferredBookingCodes'] = '';
		pData[varName+'.ondSegBookingClassStr'] ='';
		
		// Add exclude Charges
		UI_tabSearchFlights.composeExcludeChargesData(pData, varName);		
		return pData;
	}

	function onChangeMethods() {
		/* On change */
		$("#departureVarince").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
		$(document).on('change',"#fromAirport",function(){UI_commonSystem.pageOnChange();clearAllUI(); UI_tabSearchFlights.originChanged();});
		$(document).on('change',"#toAirport",function(){UI_commonSystem.pageOnChange();clearAllUI(); UI_tabSearchFlights.originDestinationChanged();});
		$("#fAirport").change(function(){UI_commonSystem.pageOnChange();clearAllUI(); UI_tabSearchFlights.originChanged();});
		$("#tAirport").change(function(){UI_commonSystem.pageOnChange();clearAllUI(); UI_tabSearchFlights.originDestinationChanged();});		
		$('#classOfService').change(function(){UI_tabSearchFlights.fillBCDropDown();});				
		$("#departureDate").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});		
		$("#adultCount").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
		$("#childCount").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
		$("#infantCount").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
		$("#classOfService").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
		$("#selectedCurrency").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
		$("#bookingType").change(function(){UI_commonSystem.pageOnChange();clearAllUI(); UI_tabSearchFlights.bookingTypeOnChange();});
		
		$("#selBookingClassCode").change(function(){UI_commonSystem.pageOnChange();clearAllUI();});
	}

	function initilizeHTMLContent() {
		UI_commonSystem.pageOnChange();
		UI_tabSearchFlights.initExcludeChargesView(true);
		// to be compatible with IE
		window.onbeforeunload = function (e) {
			UI_tabSearchFlights.releaseBlockSeats();
			return;
		};
		var setHeights = function(collapse, diff){
			var curHeight = parseInt($("#tdResultPane").css("height"), 10);
			diff = parseInt(diff,10);
			if (collapse){
				$("#tdResultPane").animate({height:curHeight+diff},500, function() {
					$("#tdResultPane").css({"overflow-y":"auto","overflow-x":"hidden"});
				});
			}else{
				$("#tdResultPane").css({height:curHeight-diff});
			}
			$("#tdResultPane").css({"overflow-y":"auto","overflow-x":"hidden"});
		};
		$("#trSearchParams").collapsePanel({
			"startLabel":"Hide search panel",
			"collapseLabel":"Show search panel",
			"eapandLabel":"Hide search panel",
			"extaFun":setHeights
		});
		$(".PBRKDownBody").css({
			"overflow-y":"hidden",
			"height":"auto"
		});	
		$("#departureDate").datepicker({ minDate: new Date(), showOn: "button", buttonImage: "../images/Calendar_no_cache.gif", buttonImageOnly: true, maxDate:'+1Y', yearRange:'-1:+1', dateFormat: UI_commonSystem.strDTFormat, changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
		$("#draggable, #stopOverTimePopup").draggable();
		$("#departureDate").blur(function() { UI_tabSearchFlights.dateOnBlur({id:0});});
		$("#btnMinFareSearch").decorateButton();
		$("#btnDetailedPrice").decorateButton();
		$("#btnTaxSurB").decorateButton();
		$("#btnFareRulesB").decorateButton();
		$("#btnTogglePBRKDown").decorateButton();
		$("#btnCancel").decorateButton();
		$("#btnReset").decorateButton();
		$("#btnBook").decorateButton();		

		$(".PBRKDown").hide();

		constructGridoutGoing({
			id : "#tblOutboundFlights"
		});
	}

	function clearAllUI() {		
		clearFareQuote();
		clearAvilabilityUI();		
	}
	

	function clearFareQuote() {	
		if ((top.objCWindow) && (!top.objCWindow.closed)){top.objCWindow.close();}	
		$("#divFareQuotePane").hide();
		$("#trFareSection").hide();
		$("#btnDetailedPrice").hide();	
		$("#btnBook").hide();		
	}

	function clearAvilabilityUI() {		
		$("#minFareCal").hide();
		$("#divResultsPane").hide();			
		$("#tblIBOBFlights").hide();
		$("#trAddOND").hide(); 

		$("#tblOutboundFlights").hide();
		$("#tblInboundFlights").hide();
		$('#tblOpenReturnInfo').hide();
		jsonOutFlts = null;
		jsonRetFlts = null;		

	}

	function initilizetData() {		
		UI_PageShortCuts.initilizeMetaDataForSearchFlight();
		UI_tabSearchFlights.fillDropDownData();
		UI_tabSearchFlights.initialization();	
		UI_tabSearchFlights.pgObj = UI_makeReservation;	
		UI_tabSearchFlights.initDynamicOndList();	
		UI_tabSearchFlights.hideAirportMessage();
	}

	function wireActions() {
		$("#btnMinFareSearch").click(function() {
			searchMinFareOnClick(null);
		});
		$("#btnDetailedPrice").click(function() {
			$("#btnDetailedPrice").blur();
			loadFareQuoteOnClick();
		});

		$("#btnBook").click(function() { UI_tabSearchFlights.bookOnClick();});
		$("#btnReset").click(function() { resetOnClick();}); 
		$("#btnCancel").click(function() { UI_tabSearchFlights.cancelOnClick();});
		$('#frmSrchFlt').bind("keypress", function(keyEvent){ checkEnterAndSubmit(keyEvent);});
		$("#btnTaxSurB").click(function() { UI_tabSearchFlights.taxesSurchargesOnClick();});
		$("#btnFareRulesB").click(function() { UI_tabSearchFlights.fareRulesOnClick();});
		$('#btnTogglePBRKDown').click(function(){UI_tabSearchFlights.togglePBRKDown()});
	}

	function checkEnterAndSubmit(keyEvent){
          	if(keyEvent.which == 13){
              		searchMinFareOnClick(null);
         	 }
     	};

	function loadFareQuoteOnClick(){
		UI_commonSystem.showProgress(); 
		var data = {}; 		
		data['fareQuoteParams.fromAirport'] = $('#fromAirport').val();
		data['fareQuoteParams.toAirport'] = $('#toAirport').val();
		data['fareQuoteParams.departureDate'] = UI_MinimumFare.departureDate;
		data['fareQuoteParams.adultCount'] = $('#adultCount').val();
		data['fareQuoteParams.childCount'] = $('#childCount').val();
		data['fareQuoteParams.infantCount'] = $('#infantCount').val();

		var cos = $("#classOfService").val();
		var cosArr = cos.split("-");
		
		if(cosArr.length > 1){
			UI_tabSearchFlights.objResSrchParams.logicalCabinClass = cosArr[0];
			UI_tabSearchFlights.objResSrchParams.classOfService = cosArr[1];
		} else if(cosArr.length == 1) {
			UI_tabSearchFlights.objResSrchParams.classOfService = cosArr[0];
		}
		
		data['fareQuoteParams.classOfService'] = UI_tabSearchFlights.objResSrchParams.classOfService;		
		data['fareQuoteParams.selectedCurrency'] = $('#selectedCurrency').val();
		data['fareQuoteParams.bookingType'] = $('#bookingType').val(); 
		data['fareQuoteParams.paxType'] = "A";   
		data['fareQuoteParams.fareType'] = "A";	
		data['fareQuoteParams.searchSystem'] = "AA";
		data['fareQuoteParams.ondQuoteFlexiStr'] = "";
		
		var ondFlexiQuote = $.airutil.dom.cloneObject(UI_tabSearchFlights.ondWiseFlexiSelected);
		
		$.each(ondFlexiQuote, function(key, value){
			ondFlexiQuote[key] = true;
		});
		data['fareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(ondFlexiQuote);
		data['fareQuoteParams.ondSelectedFlexiStr'] = "";
		data['fareQuoteParams.preferredBundledFares'] = "";
		data['fareQuoteParams.preferredBookingCodes'] = "";
		data['fareQuoteParams.ondSegBookingClassStr'] = {};

		for (var i = 0; i < UI_MinimumFare.rphArray.length; i++) {
			data["outFlightRPHList[" + i + "]"]=UI_MinimumFare.rphArray[i];
		}	

		UI_commonSystem.getDummyFrm().ajaxSubmit({
			dataType : 'json',
			async : false,
			data : data,
			url : "loadFareQuote.action",
			success : function(response) {
				if (response.success) {
					UI_MinimumFare.fareQuoteResultFlightInfo=response.flightInfo;
					UI_tabSearchFlights.returnCallShowFareQuoteInfo(response);				
				} else {
					showCommonError("ERROR", response.messageTxt);
					clearAllUI();
				}
				UI_tabSearchFlights.closeDiscuntPop(); 
			},
			error : UI_commonSystem.setErrorStatus 
		});

	}

	function getReadyCalender() {
		$.fn.minFareCalendar = function(settings) {
	        var jsDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat'];
	        var jsMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	        var current_Block = 0;
	        var config = {
	            displayFormat:"DDD DD MMM",
	            dataClearTimeOut:2000,
	            displayItems : {flightDate:"",fare:"",minFareFlag:true},
	            calData:[], // json Array to send
	            onSelectDate : function(event, selectedDate){},
	            onNextPrev: function(event, npObj){}
	        }

	        return this.each(function() {
	            //override configs
	            if (settings){
	                settings = $.extend(config, settings);
	            }

	            var dayCount = 7;
	            var nodPerWeek =dayCount;
	            var	el = $(this);
	            var displayCurrency = null, cabinClass=null;

	            el.on("update", {
	                calendar_Event: "update"
	            }, function( event, arg1 ) {
	                settings.calData = arg1.calJson;
	                buildCalBlock();
	            });

	            el.on("updateBlock", {
	                calendar_Event: "update-block"
	            }, function( event, arg1 ) {
	                settings.calData = arg1;
	                var blockId = current_Block[1]+"_"+ arg1.origin + "_" + arg1.destination;
	                buildDaysBody(blockId, arg1);
	            });

	            function buildCalBlock(){
	                el.empty();
	                var calData = $.extend(true, [], settings.calData);
	                for (var i=0;i<calData.length;i++){
	                    var calText = "", title = ""
	                    var noOfDays=calData[i].calendarData.length;
	                    displayCurrency = calData[i].currencyCode;
	                    cabinClass = calData[i].cabin;
	                    var nor = Math.ceil(noOfDays/nodPerWeek), calBlockID = i + "_" + calData[i].origin + "_" + calData[i].destination;
	                    calText = '<div class="calBlock" id="calBlock_' + calBlockID + '" >';
	                    title = "Lowest fares for " + calData[i].origin + "/" + calData[i].destination + " for " + cabinClass + " class";
	                    calText += '<div class="titleRow"><h2>' + title + '</h2></div>';
	                    calText += '<a href="javascript:;" class="navigte nextNav" title="Next"> > </a> <a href="javascript:;" class="navigte prevNav" title="Prev"> < </a>';
	                    calText += '<div class="calTable">';
	                    /*calText += '<div class="calTableRow headerRow">';
	                    for (var j=0;j<dayCount;j++){
	                        calText +='<div class="calTableCell"><label>'+jsDays[j]+'</label></div>';
	                    }
	                    calText +='</div>';*/
	                    calText +='</div></div>';
	                    el.append(calText);
	                    buildDaysBody(calBlockID, calData[i]);
	                }
	            }

	            function buildDaysBody(blockId, blkData){
	                var stDate = blkData.calendarData[0].date, enDate=blkData.calendarData[blkData.calendarData.length-1].date;
	                blkData.startEndDateFlag = stDate + "_" + enDate;
	                $.data(el.find('#calBlock_' + blockId)[0],"blockData", blkData);
	                blkData = $.extend(true, {}, blkData);
	                el.find('#calBlock_' + blockId + ' .calTable').find(".dayItem").fadeOut();
	                el.find('#calBlock_' + blockId + ' .calTable').find(".bodyRow").remove();
	                var bodyData = blkData.calendarData;
	                var nor = Math.ceil(bodyData.length/dayCount);
	                var htmlH = '';
	                for (var i=0; i<nor; i++) {
	                    var stIndex = 0;
	                    var len = (bodyData.length >= dayCount) ? dayCount : bodyData.length;
	                    var rowData = bodyData.splice(stIndex, len);
	                    htmlH += '<div class="calTableRow bodyRow">';
	                    htmlH += buildARow(i, rowData);
	                    htmlH += '</div>';
	                }
	                el.find('#calBlock_' + blockId + ' .calTable').append(htmlH);
	                el.find('#calBlock_' + blockId + ' .calTable').find(".dayItem").fadeIn();
	                initMethods(blockId);
	            }

	            function buildARow(rowIndex, rowData){
	                var htmlH='';
	                for (var i=0;i<dayCount;i++){
	                    var tempDay=null;
	                    if(rowData[i]!=undefined){
	                        tempDay=rowData[i];
	                    }
	                    htmlH+='<div class="calTableCell">' + buildADay(tempDay) + '</div>';
	                }
	                return htmlH;
	            }

	            function setDateFormat(date){
	                var temdate = date.split("-"),send=date;
	                var newDate = new Date(temdate[0]+"/"+temdate[1]+"/"+temdate[2]);
	                if (settings.displayFormat = "DDD DD MMM"){//can apply other date formats later
	                     send=jsDays[newDate.getDay()] + " " + newDate.getDate() + " " + jsMonths[newDate.getMonth()];
	                }
	                return send
	            };

	            function buildADay(dayData){
	                var htmlH = "", clickCLass="";
	                if (dayData!=null) {
	                    if (dayData.fareDetails != null) {
	                        clickCLass="fareClick"
	                    }
	                    htmlH = '<div class="dayItem ' + clickCLass + '">';
	                    htmlH += '<div class="day-header"><span class="date-hidden">'+ dayData.date + '</span>';
	                    htmlH += '<span class="date-shown">' + setDateFormat(dayData.date) + '</span></div>';
	                    if (dayData.fareDetails != null) {
	                        htmlH += '<div class="day-fare"><div class="showFare">';
	                        htmlH += Number(dayData.fareDetails.fareAmount).toFixed(2) + ' </br><span>' + displayCurrency + '</span>';
	                        htmlH += '</div></div>'
	                    } else {
	                        htmlH += '<div class="day-fare"><div class="showFare"><span>Not</br>Available</span></div></div>';
	                    }
	                    htmlH+='</div>';
	                }
	                return htmlH;
	            }

	            function getObjectByData(tdate, array){
	                var send=null;
	                $.each(array.calendarData, function(i, obj){
	                    if (obj.date===tdate){
	                        send = obj;
	                    }
	                })
	                return send;
	            }

	            function zeroBased(dig){
	                if (String(dig).length===1){
	                    dig = "0"+dig;
	                }
	                return dig
	            }

	            function addDays(dtDate,intDays) {
	                var dtArray = dtDate.split("-");
	                var newDate = new Date(dtArray[0]+"/"+dtArray[1]+"/"+dtArray[2]);
	                var sendDate = new Date(newDate.getTime() + Number(intDays) *24*60*60*1000);
	                return sendDate.getFullYear() +"-"+ zeroBased((sendDate.getMonth() + 1)) +"-"+ zeroBased(sendDate.getDate());
	            }

	            function initMethods(blockId){
	                el.find('#calBlock_' + blockId).find(".fareClick").off("click").on('click', function(event){
	                    var selectedDate = $.trim($(this).find('.date-hidden').text()),
	                        selectedBlock = $(this).parent().parent().parent().parent(),
	                        selectedBlockData = $.data(selectedBlock[0],"blockData");
	                    selectedBlock.find(".fareClick").removeClass("selected-day");
	                    $(this).addClass("selected-day");
	                    settings.onSelectDate(event, getObjectByData(selectedDate, selectedBlockData));
	                });

	                el.find('#calBlock_' + blockId).find(".navigte").off("click").on("click", function(event){
	                    nextPreviousAction(event, this);
	                });
	            }

	            function nextPreviousAction(event, obj){
	                var blockID = $(obj).parent().attr("id");
	                var direction = $(obj).attr("title");
	                var tem = {};
	                tem["way"]=$(obj).attr("title");
	                var bIndex= blockID.split("_");
	                current_Block = bIndex;
	                var currentDB = $.data(el.find('#' + blockID)[0],"blockData");
	                var oldDatArray=$.data(el.find('#' + blockID)[0],"blockOldDataArray")||[];
	                var startDate ="", length = currentDB.calendarData.length, endDate="";
	                var reCallTimeoutForBlock = currentDB.reCallTimeOut * 1000;
	                if (direction.toLowerCase()==='prev'){
	                    var tStDt = currentDB.startEndDateFlag.split("_")[0];
	                    startDate = addDays(tStDt, -length);
	                    endDate = addDays(startDate, (length-1));
	                }else{
	                    var tStDt = currentDB.startEndDateFlag.split("_")[1];
	                    startDate = addDays(tStDt, 1);
	                    endDate = addDays(tStDt, length);
	                }
	                tem["startDate"]=startDate;
	                tem["endDate"]=endDate;
	                tem["origin"]=bIndex[2];
	                tem["destination"]=bIndex[3];
	                dateFlg = startDate + "_" + endDate;
	                var dt = new Date();
	                var currentDate = dt.getFullYear() + "-" + zeroBased((dt.getMonth() + 1)) + "-" + zeroBased(dt.getDate());
	                if (compareDate(currentDate, startDate)>=0){

	                    //check the availability of oldData
	                    var dataBlock = getNextPrevDataBlock(dateFlg, oldDatArray);
	                    //Inner function for delete when the timeout comes
	                    var deleteArrayItem = function(ind,arr){
	                        arr.splice(ind, 1);
	                    }


	                    if (oldDatArray.length<5){
	                        oldDatArray[oldDatArray.length]=currentDB;
	                        setTimeout(function(){
	                            oldDatArray.splice(oldDatArray.length-1, 1);
	                            $.data(el.find('#' + blockID)[0],"blockOldDataArray", oldDatArray);
	                        }, reCallTimeoutForBlock);
	                    }
	                    $.data(el.find('#' + blockID)[0],"blockOldDataArray", oldDatArray);

	                    if (dataBlock==null){
	                        settings.onNextPrev(event, tem);
	                    }else{
	                        var blId = bIndex[1] + "_" + bIndex[2] + "_" +bIndex[3];
	                        buildDaysBody(blId, dataBlock);
	                    }
	                }else{
	                    alert ('can not go to previous');
	                }
	            }

	            function getNextPrevDataBlock(blockFlg, array){
	                var send = null;
	                for(var i=0;i<array.length;i++){
	                    if (array[i].startEndDateFlag === blockFlg){
	                        send= array[i];
	                        break;
	                    }
	                }
	                return send;
	            }


	            function compareDate(date1,date2){
	                var tempDay = date1.split("-");
	                var tmpDate1=(tempDay[0]+tempDay[1]+tempDay[2]);
	                tempDay = date2.split("-");
	                var tmpDate2=(tempDay[0]+tempDay[1]+tempDay[2]);
	                if (tmpDate1 < tmpDate2){
	                    return 1;
	                }else if (tmpDate1 == tmpDate2){
	                    return 0;
	                } else {
	                    return -1;
	                }

	            }

	        })
	    };
	    
		UI_MinimumFare.minFareCalendar = $("#minFareCal").minFareCalendar({
					displayItems : {
						flightDate : "",
						fare : "",
						minFareFlag : true
					},
					onSelectDate : function(event, obj) {
						generateSegmentTable(obj);
					},
					onNextPrev : function(event, obj) {						
						var fromDate=new Date(obj.startDate);
						searchMinFareOnClick(fromDate);
					}
				});
	}

	function generateSegmentTable(obj) {
		var segmentsToDisplay=obj.fareDetails.selectedSegments;
		for (var i = 0; i < segmentsToDisplay.length; i++) {
			var segment = {
				ondCode : obj.fareDetails.selectedSegments[i].segmentCode,
				flightNoList :  obj.fareDetails.selectedSegments[i].flightNumber,
				carrierList : segmentsToDisplay[i].flightNumber.substring(0, 2),
				depatureList :  obj.fareDetails.selectedSegments[i].departureDateTime,
				arrivalList :  obj.fareDetails.selectedSegments[i].arrivalDateTime,
				availabilityList : "Yes"
			};
			
			UI_MinimumFare.segmentData[i]=segment;
		}
		UI_commonSystem.fillGridData({
			id : "#tblOutboundFlights",
			data : UI_MinimumFare.segmentData
		});
		$("#tblOutboundFlights").show();
		$("#trFareSection").hide();
		$("#btnDetailedPrice").show();
		$("#tblIBOBFlights").show();

		var departureDateArray=UI_MinimumFare.segmentData[0].depatureList.split("T")[0].split("-");
		var depDate=new Date(departureDateArray[0]+"/"+departureDateArray[1]+"/"+departureDateArray[2]);		
		UI_MinimumFare.departureDate=depDate.getDate()+"/"+(depDate.getMonth()+1)+"/"+depDate.getFullYear(); // why need this
		
		var selectedInvSegments=obj.fareDetails.selectedSegments;
		UI_MinimumFare.rphArray=[];
		for (var i = 0; i < selectedInvSegments.length; i++) {
			UI_MinimumFare.rphArray[i]=selectedInvSegments[i].flightRPH;
		}
	}

	function constructGridoutGoing(inParams) {
		var arrBreakFmt = function(arr, options, rowObject) {
			var outStr = '';
			var first = true;
			for (var i = 0; i < arr.length; i++) {
				if (!first)
					outStr += '<br />';
				outStr += arr[i];
				first = false;
			}
			return outStr;
		}

		var carrierCodeFmt = function(arr, options, rowObject) {
			for (var i = 0; i < arr.length; i++) {
				arr[i] = UI_tabSearchFlights.getBusCarrierCode(arr[i]);
			}
			return arrBreakFmt(arr, options, rowObject);
		}

		$(inParams.id).jqGrid(
				{
					datatype : "local",
					height : 50,
					width : 920,
					colNames : [ 'Segment', 'Flight', 'Op', 'Departure',
							'Arrival', 'Avl.' ],
					colModel : [ {
						name : 'ondCode',
						index : 'ondCode',
						width : 110,
						align : "left"
					}, {
						name : 'flightNoList',
						index : 'flightNoList',
						width : 80,
						align : "center"
					}, {
						name : 'carrierList',
						index : 'carrierList',
						width : 40,
						align : "center"
					}, {
						name : 'depatureList',
						index : 'depatureList',
						width : 110,
						align : "center"
					}, {
						name : 'arrivalList',
						index : 'arrivalList',
						width : 110,
						align : "center"
					}, {
						name : 'availabilityList',
						index : 'availabilityList',
						width : 50,
						align : "right"
					} ],
					imgpath : UI_commonSystem.strImagePath,
					multiselect : false,
					viewrecords : true
				});
	}

	function searchMinFareOnClick(startDate) {
		// hide result pane hide // hide button // hide fare quote
				
		$("#divResultsPane").hide();
		$("#tblIBOBFlights").hide();
		$("#btnMinFareSearch").blur();
		$("#departureDate").blur();
		UI_tabSearchFlights.originDestinationOnBlur(); // double check whether
		// we need them
		UI_tabSearchFlights.releaseBlockSeats();
		UI_tabSearchFlights.updateTravelAgentCode();
		UI_tabSearchFlights.disableEnablePageControls(false); // have to
		// rewrite

		var isValidate = validateSearchFlight();

		if (isValidate) {

			var data = {}; 
			var cos = $("#classOfService").val();
			var cosArr = cos.split("-");

			// If user selects logical cabin class, value patter will be
			// 'XXX-XXX' (logical_cc-cabin_class)
			if (cosArr.length > 1) {
				data['classOfService'] = cosArr[1];
			} else if (cosArr.length == 1) {
				data['classOfService'] = cosArr[0];
			}
			
			var departureVarince=$("#departureVarince option:selected").val();
			departureVarince=parseInt(departureVarince);
			if(isNaN(departureVarince)) {
				departureVarince=28;
			}
			
			var str = $('#departureDate').val();
			var parts = str.split("/");
			data['departureDate'] = parts[2] + "-" + parts[1] + "-" + parts[0];

			var departureDateObject = new Date(parts[2] + "-" + parts[1] + "-"
					+ parts[0]);

			if(startDate!=null){
				departureDateObject=startDate.getFullYear() + "/"
					+ (startDate.getMonth() + 1) + "/"
					+ startDate.getDate(); // for firefox '2017/05/20' date format is compatible
				data['departureDate'] = startDate.getFullYear() + "-"
				+ (startDate.getMonth() + 1) + "-"
				+ startDate.getDate();
			}
			
			var toDateObject = new Date(departureDateObject);
			toDateObject.setDate(toDateObject.getDate() + (departureVarince-1));

			data['toDate'] = toDateObject.getFullYear() + "-"
					+ (toDateObject.getMonth() + 1) + "-"
					+ toDateObject.getDate();

			data['fromAirport'] = $('#fromAirport').val();
			data['toAirport'] = $('#toAirport').val();
			data['adultCount'] = $('#adultCount').val();
			data['childCount'] = $('#childCount').val();
			data['infantCount'] = $('#infantCount').val();
			data['selectedCurrency'] = $('#selectedCurrency').val();
			

			UI_commonSystem.getDummyFrm().ajaxSubmit({
				dataType : 'json',
				async : false,
				data : data,
				url : "minFareCalculation.action",
				success : function(response) {
					if (response.success) {
						UI_MinimumFare.calendarRS = response.calendarRS;
						UI_MinimumFare.searchResponse = response;
						if(startDate==null){
							returnCallSearchCalender();
						}else{
							showNext();
						}	
						startDate==null; 			
					} else {
						showCommonError("ERROR", response.messageTxt);
					}

					UI_tabSearchFlights.closeDiscuntPop();
				},
				error : UI_commonSystem.setErrorStatus
			});

		} else {
			if (UI_commonSystem.strPGMode != "") {
				UI_tabSearchFlights.disableEnablePageControls(true);
			}
		}
		UI_tabSearchFlights.hideAirportMessage();
		UI_tabSearchFlights.hideAirportMessage();

		return false;

	};
	function showNext() {
		dataToCalender = generateDataToCalender();

		UI_MinimumFare.minFareCalendar.trigger('updateBlock',dataToCalender[0]);
		UI_commonSystem.hideProgress();
		$("#divResultsPane").show();	
		$("#minFareCal").show();  
		clearFareQuote();
	}

	function returnCallSearchCalender() {  		
		dataToCalender = generateDataToCalender();
		var jsonObj = {
			"calJson" : dataToCalender
		};
		UI_MinimumFare.minFareCalendar.trigger('update', jsonObj);
		UI_commonSystem.hideProgress();
		clearFareQuote();
		$("#divResultsPane").show();	
		$("#minFareCal").show();		
	}

	function generateDataToCalender() {
		var dataToCalender = [];
		var sampleObj = {};
		sampleObj.origin = UI_MinimumFare.calendarRS.ondCalendars[0].origin;
		sampleObj.destination = UI_MinimumFare.calendarRS.ondCalendars[0].destination;
		sampleObj.cabin = UI_MinimumFare.calendarRS.ondCalendars[0].cabinClass;
		sampleObj.currencyCode = UI_MinimumFare.calendarRS.currencyCode;
		sampleObj.reCallTimeOut = 5;
		sampleObj.calendarData = [];

		var indexes = [];
		var lowestValue = Number.POSITIVE_INFINITY;
		var minIndex = 0;

		var count;
		var dateCalender;
		var len = UI_MinimumFare.calendarRS.ondCalendars[0].calendarDates.length;
		var dateCalender = UI_MinimumFare.calendarRS.ondCalendars[0].calendarDates;
		for (count = 0; count < len; count++) {
			var calendarDataToSet = {};
			calendarDataToSet.date = convertaDateTimeToDate(dateCalender[count].date);
			calendarDataToSet.dateOfWeek = "SUN";
			calendarDataToSet.status = dateCalender[count].status;

			if (dateCalender[count].fareDetails != null) {
				var fareDetailsToSet = {};
				fareDetailsToSet.fareAmount = dateCalender[count].fareDetails.totalAmount; 
				fareDetailsToSet.fareIndicator = null;
				fareDetailsToSet.selectedSegments = dateCalender[count].fareDetails.selectedInvSegments;
				calendarDataToSet.fareDetails = fareDetailsToSet;

				if (fareDetailsToSet.fareAmount < lowestValue) { // find the
					// minimum
					// fare
					// index
					// from the
					// array
					minIndex=0;
					indexes = [];
					lowestValue = fareDetailsToSet.fareAmount;
					indexes[minIndex] = count;
				} else if (lowestValue == fareDetailsToSet.fareAmount) {
					minIndex++;
					indexes[minIndex] = count;					
				}
				

			} else {
				calendarDataToSet.fareDetails = null;
			}
			sampleObj.calendarData[count] = calendarDataToSet;
		}

		// put min fare to object
		var lenIndexes = indexes.length;
		for (count = 0; count < lenIndexes; count++) {
			sampleObj.calendarData[indexes[count]].fareDetails.fareIndicator = 'LOWEST';
			var a=indexes[count];
		}

		dataToCalender[0] = sampleObj;
		return dataToCalender;

	}
	function convertaDateTimeToDate(date) {
		var parts = date.split("T");
		return parts[0];
	}

	function validateSearchFlight() {

		UI_commonSystem.initializeMessage();

		var selectedSegmentCabinclass = '';
		selectedSegmentCabinclass = $("#classOfService").val();

		/* Mandatory Validations */
		if (!UI_commonSystem.validateMandatory(jsonSCObj)) {
			return false;
		}

		if ($('#fromAirport').val().toUpperCase() == 'PLEASE SELECT A VALUE') {
			showERRMessage(raiseError("XBE-ERR-01", "Departure location"));
			$("#fromAirport").focus();
			return false;
		}

		if ($('#toAirport').val().toUpperCase() == 'PLEASE SELECT A VALUE') {
			showERRMessage(raiseError("XBE-ERR-01", "Arrival location"));
			$("#toAirport").focus();
			return false;
		}

		if ($("#fromAirport").val() == $("#toAirport").val()) {
			showERRMessage(raiseError("XBE-ERR-02", "Departure location",
					"Arrival location"));
			$("#fromAirport").focus();
			return false;
		}
		if (UI_commonSystem.strPGMode != "confirmOprt") { 
			if ($("#departureDate").val() == ""
					|| ($("#departureDate").val().indexOf("/") == -1)) {
				showERRMessage("Wrong date format of Departure Date");
				$("#departureDate").focus();
				return false;
			}

			if (!CheckDates(top.strSysDate, $("#departureDate").val())) {
				showERRMessage(raiseError("XBE-ERR-03", "From date",
						top.strSysDate));
				$("#departureDate").focus();
				return false;
			}
			if ($("#bookingType").val() == ""
					|| $("#bookingType").val() == null) {
				showERRMessage(raiseError("XBE-ERR-06", "Booking Type"));
				return false;
			}
		}

		if (UI_commonSystem.strPGMode == "confirmOprt") { 
			if ($("#returnDate").val() == "" || $("#returnDate").val() == null) {
				showERRMessage(raiseError("XBE-ERR-06", "Return Date"));
				$("#returnDate").focus();
				return false;
			}
			if ($("#returnVariance").val() != "") {
				var trvlExp = DATA_ResPro.modifySegments.lastArrival;
				var trvlExpArr = trvlExp.split("-");
				var retDateArr = $("#returnDate").val().split("/");

				var expiryDate = new Date(trvlExpArr[0], (trvlExpArr[1] - 1),
						trvlExpArr[2].substring(0, 2));
				var retDate = new Date(retDateArr[2], (retDateArr[1] - 1),
						retDateArr[0]);
				if (expiryDate.getTime() < retDate.getTime()) {
					showERRMessage(raiseError("XBE-ERR-05", "Return Date ",
							" travel expiry date"));
					$("#returnDate").focus();
					return false;
				}
			}

			var reservationPaxs = DATA_ResPro.resPaxs;
			var retDateArr = $("#returnDate").val().split("/");
			var retDate = new Date(retDateArr[2], (retDateArr[1] - 1),
					retDateArr[0]);

			for (var i = 0; i < reservationPaxs.length; i++) {
				var passportExpiryDateStr = reservationPaxs[i].lccClientAdditionPax.passportExpiry;
				if (passportExpiryDateStr != null
						&& passportExpiryDateStr != '') {
					var passportExpiryDate = decodeJsonDate(passportExpiryDateStr);
					if (passportExpiryDate < retDate) {
						var paxName = reservationPaxs[i].firstName + " "
								+ reservationPaxs[i].lastName;
						showERRMessage("Passport expiry date of passenger : "
								+ paxName + ", is less than the return date.");
						$("#returnDate").focus();
						return false;
					}
				}
			}

		}

		if (UI_commonSystem.strPGMode != "transferSegment"
				&& UI_commonSystem.strPGMode != "confirmOprt") { // useless
			if (false) { 
				var ticketExpiry = $("#ticketExpiryDate").val();
				var ticketExpiryDate;
				var ticketExpDateFrmtStr;
				if (ticketExpiry != null && ticketExpiry != "") {
					var ticketExpArr = ticketExpiry.split("-");
					ticketExpiryDate = new Date(
							ticketExpArr[2].substring(0, 4),
							(ticketExpArr[1] - 1), ticketExpArr[0]);
					ticketExpDateFrmtStr = ticketExpArr[0] + "/"
							+ ticketExpArr[1] + "/"
							+ ticketExpArr[2].substring(0, 4);
				}

				var validFrom = $("#ticketValidFrom").val();
				var validFromDate;
				var validFromDateFrmtStr;
				if (validFrom != null && validFrom != "") {
					var validFromArr = validFrom.split("-");
					validFromDate = new Date(validFromArr[2].substring(0, 4),
							(validFromArr[1] - 1), validFromArr[0]);
					validFromDateFrmtStr = validFromArr[0] + "/"
							+ validFromArr[1] + "/"
							+ validFromArr[2].substring(0, 4);
				}

				if (ticketExpiry != ""
						&& !CheckDates($("#departureDate").val(), $(
								"#ticketExpiryDate").val())) {
					showERRMessage(raiseError("XBE-ERR-05", "Departure Date",
							ticketExpDateFrmtStr));
					$("#departureDate").focus();
					return false;
				}

				if (validFrom != ""
						&& !CheckDates($("#ticketValidFrom").val(), $(
								"#departureDate").val())) {
					showERRMessage(raiseError("XBE-ERR-03", "Departure Date",
							validFromDateFrmtStr));
					$("#departureDate").focus();
					return false;
				}

			}

		}

		/* Number Validations */
		if ($("#adultCount").val() != "") {
			if (!UI_commonSystem.numberValidate({
				id : "#adultCount",
				desc : "Number of Adults",
				minValue : 0
			})) {
				return false;
			}
		} else {
			$("#adultCount").val(0);
		}

		if ($("#childCount").val() != "") {
			if (!UI_commonSystem.numberValidate({
				id : "#childCount",
				desc : "Number of Children",
				minValue : 0
			})) {
				return false;
			}
		} else {
			$("#childCount").val(0);
		}

		if ($("#infantCount").val() != "") {
			if (!UI_commonSystem.numberValidate({
				id : "#infantCount",
				desc : "Number of Infants",
				minValue : 0
			})) {
				return false;
			}
			if (Number($("#infantCount").val()) > Number($("#adultCount").val())) {
				showERRMessage(raiseError("XBE-ERR-05", "Infant count",
						"adult count"));
				$("#infantCount").focus();
				return false;
			}
		} else {
			$("#infantCount").val(0);
		}

		if (($("#adultCount").val() == "") || ($("#adultCount").val() == "0")) {
			if (top.childBookingStatus != "Y") {
				showERRMessage(raiseError("XBE-ERR-01", "Number of Adults"));
				$("#adultCount").focus();
				return false;
			}
		}

		if (top.childBookingStatus == "Y") {
			if ((String(trim($("#childCount").val())) == "" || String(trim($(
					"#childCount").val())) == "0")
					&& (String(trim($("#adultCount").val())) == "" || String(trim($(
							"#adultCount").val())) == "0")) {
				showERRMessage(raiseError("XBE-ERR-01",
						"No of Adult or Children"));
				$("#childCount").focus();
				return false;
			}
		}

		if (top.arrPrivi[PRIVI_GROUP_BOOKING] == 1) { 
			// execution doesn't
		} else {
			var intTotPax = Number($("#adultCount").val())
					+ Number($("#childCount").val())
					+ Number($("#infantCount").val());
			if (intTotPax > top.intMaxGroup) {
				showERRMessage(raiseError("XBE-ERR-15", String(top.intMaxGroup)));
				$("#adultCount").focus();
				return false;
			}
		}

		if (UI_commonSystem.strPGMode == "confirmOprt") {
			UI_tabSearchFlights.strOutDate = $("#returnDate").val();
		} else {
			UI_tabSearchFlights.strOutDate = $("#departureDate").val();
			UI_tabSearchFlights.strRetDate = $("#returnDate").val();
		}

		UI_tabSearchFlights.strOrigin = $("#fromAirport").val();
		UI_tabSearchFlights.strDestination = $("#toAirport").val();

		 UI_commonSystem.showProgress(); 

		return true;
	};
	
};

UI_MinimumFare = new UI_MinimumFare();