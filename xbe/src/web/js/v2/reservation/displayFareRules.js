	/*
	*********************************************************
		Description		: XBE Make Reservation - Display Fare Rule
		Author			: Abheek Das Gupta
		Version			: 1.0
		Created			: 8th April 2010
		Last Modified	: 12th April 2010
	*********************************************************	
	*/
	
	/* Fare Rule Details */
	var jsonFareRuleDetails = [];
	
	/*
	 * Display Fare Rule
	 */
	function UI_displayFareRule(){}
	
	/* Flexi Fare Rule Details */
	var jsonFlexiFareRuleDetails = [];
	
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		$("#btnClose").decorateButton();
		$("#btnClose").click(function(){UI_displayFareRule.closeOnClick();});
		
		/* construct grid */
		var data = {};
		UI_commonSystem.showProgress();
		var response = window.opener.UI_tabSearchFlights.fareRulesInfo;
		if(response != null && response.fareRules !=null && response.fareRules.length > 0){
			$("#trFareRuleDetails").show();
			response.fareRuleDTOs = response.fareRules;
			UI_displayFareRule.constructFareRuleDetails({id:"#tblFareRuleDetails"}, response);
			UI_displayFareRule.populateFareRuleDetails(response);
		} else {
			$("#trFareRuleDetails").hide();
		}
		//UI_displayFareRule.constructFareRuleDescription({id:"#tblFareRuleDescription"});
		response = window.opener.UI_tabSearchFlights.flexiFareRulesInfo;
		if(response != null && response.fareRules !=null && response.fareRules.length > 0){
			$("#trFlexiFareRuleDetails").show();
			UI_displayFareRule.constructFlexiFareRuleDetails({id:"#tblFlexiFareRuleDetails"}, response);		
			UI_displayFareRule.populateFlexiFareRuleDetails(response);
		} else {
			$("#trFlexiFareRuleDetails").hide();
		}
		//UI_displayFareRule.populateFareRuleDescription(response);
		$('#trFareRuleDescription').hide();
		$('nr').css('color','red');
		$('nr').css('font-weight','bold');		
		UI_commonSystem.hideProgress();	
		
	});
	
	/*
	 * Close window
	 */
	UI_displayFareRule.closeOnClick = function(){
		window.close();
	}
	
	/*
	 * Fare rule details
	 */
	UI_displayFareRule.constructFareRuleDetails = function(inParams, response){
		var adultApplicable = response.adultApplicable;
		var childApplicable = response.childApplicable;
		var infantApplicable = response.infantApplicable;
		var hasComment = response.hasComment;
		var hasAgentInstructions = response.hasAgentInstructions;
		var width = 120;
		if(hasComment){width=70};
			//construct grid
			$(inParams.id).jqGrid({
				width: 860,
				height: 125,
				colNames:[getOpeneri18nData('fareRule_Segment', 'Segment'), getOpeneri18nData('fareRule_AdultFare','Adult Fare'), getOpeneri18nData('fareRule_ChildFare','Child Fare'), getOpeneri18nData('fareRule_InfantFare','Infant Fare'), getOpeneri18nData('fareRule_BookingClass','Booking Class'),getOpeneri18nData('fareRule_ClassOfService','Class of Service') ,getOpeneri18nData('fareRule_FareBasisCode','Fare Basis Code'), getOpeneri18nData('fareRule_FareRule','Fare Rule'), getOpeneri18nData('fareRule_Comments','Comments'),getOpeneri18nData('fareRule_AgentInstructions','Agent Instructions')],
				colModel:[
					{name:'orignNDest',  index:'orignNDest',width:120, align:"center"},
					{name:'adultFare', index:'adultFare', align:"center", width:120, hidden: !adultApplicable},
					{name:'childFare', index:'childFare', align:"center", width:120, hidden: !childApplicable},
					{name:'infantFare', index:'infantFare', align:"center", width:120, hidden: !infantApplicable},
					{name:'bookingClassCode', index:'bookingClassCode',width:width, align:"center"},
					{name:'cabinClassCode', index:'cabinClassCode',width:width, align:"center"},
					{name:'fareBasisCode', index:'fareBasisCode', width:width, align:"center"},
					{name:'fareRuleCode', index:'fareRuleCode', width:width, align:"center"},
					{name:'comments', index:'comments', width:200, align:"center", hidden: !hasComment},
					{name:'agentInstructions', index:'agentInstructions', width:150, align:"center", hidden: !hasAgentInstructions}
				],
				imgpath: UI_commonSystem.strImagePath,
				dataType: 'local',
				multiselect: false,
				caption:getOpeneri18nData('fareRule_FareRuleDetails', 'FareRule Details'),
				viewrecords: true
			});	
	}
	
	/*
	 * Flexi Fare rule details
	 */
	UI_displayFareRule.constructFlexiFareRuleDetails = function(inParams, response){
		var adultApplicable = response.adultApplicable;
		var childApplicable = response.childApplicable;
		var infantApplicable = response.infantApplicable;
		var hasComment = response.hasComment;

			//construct grid
			$(inParams.id).jqGrid({
				width: 860,
				height: 125,
				colNames:[ 'Segment', 'Adult Fare', 'Child Fare', 'Infant Fare', 'Comments','Total Price', 'Flexibilities'],
				colModel:[
					{name:'orignNDest',  index:'orignNDest', width:150, align:"center"},
					{name:'adultFare', index:'adultFare', align:"center", hidden: !adultApplicable},
					{name:'childFare', index:'childFare', align:"center", hidden: !childApplicable},
					{name:'infantFare', index:'infantFare', align:"center", hidden: !infantApplicable},
					{name:'comments', index:'comments', align:"center", hidden: !hasComment},
					{name:'totalTicketPrice', index:'totalTicketPrice', width:90, align:"center"},
					{name:'description', index:'description', width:200,align:"center"}
				],
				imgpath: UI_commonSystem.strImagePath,
				dataType: 'local',
				multiselect: false,
				caption:'Flexi Fare Rule Details',
				viewrecords: true
			});	
	}
	
	/*
	 * Fare rule descriptions
	 */
	UI_displayFareRule.constructFareRuleDescription = function(inParams){
		
		//construct grid
		$(inParams.id).jqGrid({
			width: 860,
			height: 75,
			colNames:[ 'Fare Rule', 'Comments'],
			colModel:[
				{name:'fareRuleCode', index:'fareRuleCode', width:30, align:"left"},
				{name:'comments', index:'comments', width:70, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			dataType: 'local',
			multiselect: false,
			caption:'Comments',
			viewrecords: true
		});
	}	
	
	UI_displayFareRule.populateFareRuleDetails = function(response) {
		var fareRuleDTOs = response.fareRuleDTOs;		
		var adultApplicable = response.adultApplicable;
		var childApplicable = response.childApplicable;
		var infantApplicable = response.infantApplicable;
		
		for ( var i = 0; i < fareRuleDTOs.length; i++) {
			var tm = fareRuleDTOs[i];
			var adultFare = tm.adultFareAmount;
			var childFare = tm.childFareAmount;
			var infantFare = tm.infantFareAmount;
			
			if (tm.depAPFareDTO != null){
				adultFare = tm.depAPFareDTO.adultFare ;
				childFare = tm.depAPFareDTO.childFare ;
				infantFare = tm.depAPFareDTO.infantFare ;
			}
			
			if (tm.currencyCode != null ){
				adultFare = adultFare + ' ' + tm.currencyCode;
				childFare = childFare + ' ' + tm.currencyCode;
				infantFare = infantFare + ' ' + tm.currencyCode;
			}
			
			if (tm.selectedCurrFareDTO != null && (tm.currencyCode == null || (tm.currencyCode != null && 
					window.opener.jsonFareQuoteSummary.selectedCurrency != tm.currencyCode))){
				adultFare = adultFare + '<br /> <span class="txtBold">['+tm.selectedCurrFareDTO.adultFare + ' ' 
				+ window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
				childFare = childFare +'<br /> <span class="txtBold">['+tm.selectedCurrFareDTO.childFare + ' ' 
				+ window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
				infantFare = infantFare + '<br /> <span class="txtBold">['+tm.selectedCurrFareDTO.infantFare 
				+ ' ' + window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
			}
				
			if(tm.adultFareRefundable){
				adultFare = adultFare + '<br>(Refundable)';
			} else{
				adultFare = adultFare + '<br>(Non Refundable)';
				adultFare = '<nr>' + adultFare + '</nr>';
			}		

			if(tm.childFareRefundable){
				childFare = childFare + '<br>(Refundable)';
			} else{
				childFare = childFare + '<br>(Non Refundable)';
				childFare = '<nr>' + childFare + '</nr>';
			}

			if(tm.infantFareRefundable){
				infantFare = infantFare + '<br>(Refundable)';
			} else{
				infantFare = infantFare + '<br>(Non Refundable)';
				infantFare = '<nr>' + infantFare + '</nr>';
			}	
			jsonFareRuleDetails[i] = {
					orignNDest : tm.orignNDest, 
					adultFare : adultFare, 
					childFare : childFare,
					infantFare : infantFare,  
					bookingClassCode : tm.bookingClassCode,
					cabinClassCode : tm.cabinClassCode,
					fareBasisCode : tm.fareBasisCode,
					fareRuleCode : tm.fareRuleCode,
					comments : tm.comments,
					agentInstructions : tm.agentInstructions
				};				
		}
		jsonFareRuleDetails = $.airutil.sort.quickSort(jsonFareRuleDetails,UI_displayFareRule.fareRuleComparator);
		UI_commonSystem.fillGridData({id:"#tblFareRuleDetails", data:jsonFareRuleDetails});
	}
	
	UI_displayFareRule.populateFlexiFareRuleDetails = function(response) {
		var fareRuleDTOs = response.fareRules;
		var segCharges = response.totalSegmentTicketPrice;
		var segChargesInSelCurr = response.totalSegmentTicketPriceInSelCurr;
		var adultApplicable = response.adultApplicable;
		var childApplicable = response.childApplicable;
		var infantApplicable = response.infantApplicable;
		
		for ( var i = 0; i < fareRuleDTOs.length; i++) {
			var tm = fareRuleDTOs[i];
			
			var adultFare = '<nr>' + tm.adultFareAmount;
			var childFare = '<nr>' + tm.childFareAmount;
			var infantFare = '<nr>' + tm.infantFareAmount;
			
			if (tm.selectedCurrFareDTO != null &&  
					window.opener.jsonFareQuoteSummary.selectedCurrency != window.opener.jsonFareQuoteSummary.currency){
				adultFare = adultFare + '<br /> <span class="txtBold">['+tm.selectedCurrFareDTO.adultFare + ' ' 
				+ window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
				childFare = childFare +'<br /> <span class="txtBold">['+tm.selectedCurrFareDTO.childFare + ' ' 
				+ window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
				infantFare = infantFare + '<br /> <span class="txtBold">['+tm.selectedCurrFareDTO.infantFare 
				+ ' ' + window.opener.jsonFareQuoteSummary.selectedCurrency+']</span>';
			}
			
			adultFare += '<br>(Non Refundable)</nr>';
			childFare += '<br>(Non Refundable)</nr>';
			infantFare += '<br>(Non Refundable)</nr>';
			
			var totalTicketPrice = 0.00;
			for ( var j = 0; j < segCharges.length; j++) {
				var segPrice = segCharges[j];
				var segment = segPrice.split("-")[0];
				if(tm.orignNDest.match(segment) != null ){
					totalTicketPrice = segPrice.split("-")[1];
					break;
				}
			}
			
			if(segChargesInSelCurr != null && 
					window.opener.jsonFareQuoteSummary.selectedCurrency != window.opener.jsonFareQuoteSummary.currency){				
				for ( var j = 0; j < segChargesInSelCurr.length; j++) {
					var segPrice = segChargesInSelCurr[j];
					var segment = segPrice.split("-")[0];
					if(tm.orignNDest.match(segment) != null ){
						totalTicketPrice += '<br /><span class="txtBold">[' + segPrice.split("-")[1] + 
						' ' + window.opener.jsonFareQuoteSummary.selectedCurrency + ']</span>';
						break;
					}
				}
			}
			
			jsonFlexiFareRuleDetails[i] = {
					orignNDest : tm.orignNDest, 
					adultFare : adultFare, 
					childFare : childFare,
					infantFare : infantFare,  
					comments : tm.comments,
					totalTicketPrice : totalTicketPrice,
					description : tm.description
				};				
		}
		UI_commonSystem.fillGridData({id:"#tblFlexiFareRuleDetails", data:jsonFlexiFareRuleDetails});
	}

	
	UI_displayFareRule.fareRuleComparator = function(first,second){
		return (first.departureDateLong - second.departureDateLong);
	}
	UI_displayFareRule.populateFareRuleDescription = function(response) {
		var fareRuleDTOs = response.fareRuleDTOs;		
		/* Fare Rule Descriptions */
		var jsonFareRuleDescriptions = [];
		for ( var i = 0; i < fareRuleDTOs.length; i++) {
			var tm = fareRuleDTOs[i];
			if(tm.fareRuleCode == null || tm.fareRuleCode == ''){
				continue;
			}
			jsonFareRuleDescriptions[i] = 	{
					fareRuleCode : tm.fareRuleCode, 
					comments : tm.comments
				};
		}
		if(jsonFareRuleDescriptions.length>0){
			UI_commonSystem.fillGridData({id:"#tblFareRuleDescription", data:jsonFareRuleDescriptions});
			$('#trFareRuleDescription').show();
		}else{
			$('#trFareRuleDescription').hide();
		}
	}

	/*------------------------------------------------ end of File ----------------------------------------------*/