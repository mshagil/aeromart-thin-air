	/*
	*********************************************************
		Description		: XBE Make Reservation - Change Fare
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/
	
	/* Flight Details */
	var jsonFltDetails = [];
	var jsonTestFltDetails = [];
	var jsonFltDetailsOrg = [];
						
	/* Fares */				
	/* TODO Once Backend is ready make the array as empty eg [] */
	//var jsonFares = [];
	var jsonFares = [
					{segment : "",
					 typeAdult : "",
					 typeChild : "",
					 typeInfant : "",
					 fareDeatils : [{}]
					}];	
	
	var FARE_TYPE_NO_FARE = 0;
	var FARE_TYPE_RETURN_FARE = 1;
	var FARE_TYPE_OND_FARE = 2;
	var FARE_TYPE_OND_FARE_AND_PER_FLIGHT_FARE = 3;
	var FARE_TYPE_PER_FLIGHT_FARE_AND_OND_FARE = 4;
	var FARE_TYPE_PER_FLIGHT_FARE = 5;
	var FARE_TYPE_HALF_RETURN_FARE = 6;

	var jsonChangedFaresInfo = [];
	var ondWiseSegmentCount = {};
	var segCountAsOndSequence = {};
	var segSeqOndSeqmapping = {};
	var ondWiseFareType = {};
	var segmentWiseFareType = {};
	var prevFareTypeOfSelectedOndSequence = FARE_TYPE_NO_FARE;
	
	var rowid = null;
	var modifyBooking = false;
	var returnBooking = false;
	
	flightSegmentList = [];
	
	var newOndList = [];
	var selectedCabinClass = "";
	var fromViewBCFare = false;
	var showNestedAvailableSeats = false;
	var splitOperationForBusInvoked = false;
	var ondWiseFareId = {};	
	var allowHalfRetFareForOpenReturn = false;
	var fareQuotedJourneyList = [];
	var ondWiseFareQuoteFlag = {};
	var journeyWiseOnd = {};
	var ondWiseJourneyGroup = {};
	var cabinClassChangedOnds = {};
	var segSequenceOndSequenceMapping = {};
	var segmentsMapPerSegType = {};
	var isBusSegmentExists = false;
	/*
	 * Change Fare
	 */
	function UI_changeFare(){
	}
	
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		UI_changeFare.selectedSegmentCOS = "";
						
		if(!opener.UI_tabSearchFlights.isAllFareChange){
			$("#lblChangeFareHeading").text("Change Fare");
		} else {
			$("#lblChangeFareHeading").text("All Booking Class Fares");
		}
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#btnShowFares").decorateButton();
		$("#btnReCal").decorateButton();
		$("#btnConfim").decorateButton();
		$("#btnClose").decorateButton();
		
		$("#divAvailableFares").hide();
		$("#btnReCal").hide();
		$("#btnConfim").hide();
		
		$("#btnShowFares").click(function(){UI_changeFare.showFaresOnClick();});
		$("#btnClose").click(function(){UI_changeFare.closeOnClick();});
		$("#btnReCal").click(function(){UI_changeFare.recalculateFares();});
		$("#btnConfim").click(function(){UI_changeFare.confirmFareChange();});
		
		UI_changeFare.selectedOutboundFareType = 0;
		UI_changeFare.selectedInboundFareType = 0;

		var fareQuoteInfo = window.opener.UI_tabSearchFlights.getFareQuote();
		var fltSegIdByJourneyMap = window.opener.UI_tabSearchFlights.fltSegIdByJourneyMap;
		segmentsMapPerSegType = window.opener.UI_tabSearchFlights.segmentsMapPerSegType;
		isBusSegmentExists = UI_changeFare.isBusSegmentExists();
		flightSegmentList = UI_changeFare.getFlightInfoList(fareQuoteInfo,fltSegIdByJourneyMap,segmentsMapPerSegType);
		UI_changeFare.getSegSequenceOndSequenceMapping(flightSegmentList);
		UI_changeFare.faretype = window.opener.UI_tabSearchFlights.getSelectedFareType();		
		UI_changeFare.setOndwiseSegmentCount(flightSegmentList);
		UI_changeFare.setFareQuotedOndSequence(flightSegmentList);
		UI_changeFare.populateSegmentDetails(fareQuoteInfo, flightSegmentList,fltSegIdByJourneyMap,segmentsMapPerSegType);
		UI_changeFare.setOndWiseFareType(fareQuoteInfo,segmentsMapPerSegType);
		UI_changeFare.setDefaultAmountsForReturnFareType();
		jsonFltDetailsOrg = jsonFltDetails;		
		UI_changeFare.fillSegmentDetails(flightSegmentList);
		UI_changeFare.selectedSegment = null;
		UI_changeFare.allFaresInfo = null;
		UI_changeFare.selectedSegmentIndex = -1;
		UI_changeFare.allFaresMap = {};
		
	});
	
	UI_changeFare.getSegSequenceOndSequenceMapping = function(flightSegmentList){
		for(var i=0;i<flightSegmentList.length;i++){
			segSequenceOndSequenceMapping[flightSegmentList[i].segmentSequence] = flightSegmentList[i].ondSequence;		
		}
	}
	
	UI_changeFare.getFlightInfoList = function(fareQuoteInfo,fltSegIdByJourneyMap,segmentsMapPerSegType){
		if ((fareQuoteInfo == null || fareQuoteInfo.length == 0) && window.opener.UI_tabSearchFlights.isRequote()) {
			if (window.opener.UI_tabSearchFlights.getFlightToGetBCInfo() != null) {
				fromViewBCFare = true;
				$("#lblChangeFareHeading").text("View all Fares");
				return window.opener.UI_tabSearchFlights.getFlightToGetBCInfo();
			} else {
				return UI_changeFare.populateFlightInfoFromOndList(fltSegIdByJourneyMap);
			}
		} else if(segmentsMapPerSegType != null){
			var flights = window.opener.UI_tabSearchFlights.getFlightInfo();
			var sortedFlights = [];
			var count = 0;
			if(segmentsMapPerSegType != undefined && segmentsMapPerSegType != null 
					&& flights!= undefined && flights!=null) {
				$.each(segmentsMapPerSegType, function(index, val){	
						if(val!=undefined && val!=null){
							for(var k=0;k<val.length;k++){	
								for(var jind=0; jind<flights.length; jind++){
									if(val[k]!=undefined && val[k]!=null 
											&& flights[jind].flightRefNumber.indexOf(val[k]) == 0){
										sortedFlights[count] = flights[jind];
										count++;
									}
								}
							}
						}
				});
			}
			return sortedFlights;
		} else {
			return window.opener.UI_tabSearchFlights.getFlightInfo();
		}
	}
	
	UI_changeFare.populateFlightInfoFromOndList = function(fltSegIdByJourneyMap){
		var flightInfoList = [];
		var allOndList = opener.UI_requote.getAllsegments();
		var count = 0;
		for(var i=0;i<allOndList.length;i++){
			var ondObj = allOndList[i];
			
			if(ondObj.segmentRPH.length == 0){
				//If segment is OPENRT
				flightInfoList[count] = {
						orignNDest : ondObj.ond, 
						flightNo : '',
						airLine : '',
						departureDate : '',
						flightRefNumber : '',
						ondSequence: ondObj.index,
						openReturnSegment:true,
						returnFlag: true,
						cabinClass: ondObj.cabinClass
				};
				count++;
			} else {				
				for(var j=0;j<ondObj.segmentRPH.length;j++){			
					var segRPH = ondObj.segmentRPH[j];
					var fltRefArr = segRPH.split("$");
					var deptDate = (ondObj.departureDate[j]).split('T')[0];
					var subJourneyId = -1;
					if(fltSegIdByJourneyMap != undefined && fltSegIdByJourneyMap != null) {
						$.each(fltSegIdByJourneyMap, function(index, val){	
							if(val!=undefined && val!=null){
								for(var k=0;k<val.length;k++){				
									if(segRPH.indexOf(val[k]) == 0){
										subJourneyId = index;
									}
								}
							}
						});
					}
						
					flightInfoList[count] = {
							orignNDest : fltRefArr[1], 
							flightNo : ondObj.flightNo[j],
							airLine : ondObj.carrierCode[j],
							departureDate : deptDate,
							flightRefNumber : segRPH,
							ondSequence: ondObj.index,
							openReturnSegment:false,
							returnFlag: false,
							cabinClass: ondObj.cabinClass,
							subJourneyGroup: subJourneyId
					};
					count ++;
				}				
			}
			
		}		
		flightInfoList.sort(function(a, b){
			 return a.subJourneyGroup-b.subJourneyGroup
			});
		return flightInfoList;
	}
	
	// This method sets ond wise segment count
	UI_changeFare.setOndwiseSegmentCount= function(flightInfo){
		if(flightInfo != null && flightInfo != undefined){			
			$.each(flightInfo, function(index, obj){
				
				if(ondWiseSegmentCount[obj.subJourneyGroup] == undefined 
						|| ondWiseSegmentCount[obj.subJourneyGroup] == null){
					ondWiseSegmentCount[obj.subJourneyGroup] = {};
				}
				
				if(ondWiseSegmentCount[obj.subJourneyGroup][obj.ondSequence] == undefined 
						|| ondWiseSegmentCount[obj.subJourneyGroup][obj.ondSequence] == null){
					ondWiseSegmentCount[obj.subJourneyGroup][obj.ondSequence] = 1;
				} else {
					ondWiseSegmentCount[obj.subJourneyGroup][obj.ondSequence]++;
				}
//				if(obj.segmentSequence != undefined){
//					if(segCountAsOndSequence[obj.segmentSequence] == undefined 
//							|| segCountAsOndSequence[obj.segmentSequence] == null){
//						segCountAsOndSequence[obj.segmentSequence] = 1;
//					} else {
//						segCountAsOndSequence[obj.segmentSequence]++;
//					}
//					segSeqOndSeqmapping[obj.segmentSequence] = obj.ondSequence;
//				}
				
				
//				if(obj.segmentSequence != undefined){
//					segCountAsOndSequence[obj.segmentSequence] = obj.ondSequence;
//				}
			});
		}		
	}
	
	
//	UI_changeFare.setSegmentWiseFareType = function(){
//		if(segSequenceOndSequenceMapping != null && segSequenceOndSequenceMapping != undefined){
//			$.each(segSequenceOndSequenceMapping, function(index, obj){
//				segmentWiseFareType[index] = ondWiseFareType[segSequenceOndSequenceMapping[index]];				
//			});
//		}
//	}
	
	UI_changeFare.setOndWiseFareType = function(fareQuoteInfo,segmentsMapPerSegType){
		if(fareQuoteInfo != null && fareQuoteInfo != undefined){
				$.each(jsonFltDetails, function(index, obj){
					if(ondWiseFareType[obj.subJourneyGroup] == undefined 
							|| ondWiseFareType[obj.subJourneyGroup] == null){
						ondWiseFareType[obj.subJourneyGroup] = {};
					}

					if(ondWiseFareId[obj.subJourneyGroup] == undefined 
							|| ondWiseFareId[obj.subJourneyGroup] == null){
						ondWiseFareId[obj.subJourneyGroup] = {};
					}

					ondWiseFareType[obj.subJourneyGroup][obj.ondSequence] = obj.fareType;
					ondWiseFareId[obj.subJourneyGroup][obj.ondSequence] = obj.fareId;
				});
		}else if(flightSegmentList != null && flightSegmentList != undefined){
			$.each(flightSegmentList, function(index, obj){
				
				if(ondWiseFareType[obj.subJourneyGroup] == undefined 
						|| ondWiseFareType[obj.subJourneyGroup] == null){
					ondWiseFareType[obj.subJourneyGroup] = {};
				}
				
				if(ondWiseFareId[obj.subJourneyGroup] == undefined 
						|| ondWiseFareId[obj.subJourneyGroup] == null){
					ondWiseFareId[obj.subJourneyGroup] = {};
				}
				
				ondWiseFareType[obj.subJourneyGroup][obj.ondSequence] = 0;
				ondWiseFareId[obj.subJourneyGroup][obj.ondSequence] = 0;
			});			
		}
	}
	
	UI_changeFare.getAirlineCode = function(carrierCode){
		if(opener.top.carrierAirlineCodeMap != null && opener.top.carrierAirlineCodeMap != ''
				&& opener.top.carrierAirlineCodeMap != {}){
			return opener.top.carrierAirlineCodeMap[carrierCode];
		}
		
		return null;
	}
	
	UI_changeFare.overrideFlexiSelection = function(){
		var parentPage = opener.UI_tabSearchFlights;
		var ondFlexiSelection = {};
		var ondSequences = parentPage.OND_SEQUENCE;
		
		var ondBundledFarePeriodId = parentPage.ondWiseBundleFareSelected[ondSequences.OUT_BOUND];			
		
		if(ondBundledFarePeriodId != null && ondBundledFarePeriodId != ''){
			ondFlexiSelection[ondSequences.OUT_BOUND] = false;
		} else {
			ondFlexiSelection[ondSequences.OUT_BOUND] = parentPage.ondWiseFlexiSelected[ondSequences.OUT_BOUND]
		}
		
		ondBundledFarePeriodId = parentPage.ondWiseBundleFareSelected[ondSequences.IN_BOUND];			
		
		if(ondBundledFarePeriodId != null && ondBundledFarePeriodId != ''){
			ondFlexiSelection[ondSequences.IN_BOUND] = false;
		} else {
			ondFlexiSelection[ondSequences.IN_BOUND] = parentPage.ondWiseFlexiSelected[ondSequences.IN_BOUND]
		}
		
		opener.UI_tabSearchFlights.ondWiseFlexiSelected = ondFlexiSelection;
		
		return ondFlexiSelection;
	}
	
	UI_changeFare.overrideChangedCabinClass = function(){
		for(var jind=0; jind<jsonFltDetails.length; jind++){
			var sequence = jsonFltDetails[jind].ondSequence;
			var segmentCode = jsonFltDetails[jind].orignNDest;
			if(cabinClassChangedOnds[segmentCode] &&
					!opener.UI_tabSearchFlights.ondRebuildLogicalCCAvailability[sequence]){
					opener.UI_tabSearchFlights.ondRebuildLogicalCCAvailability[sequence] = true;
			}
		}
	}
	
	UI_changeFare.overridePartialLogicallCCRebuild = function(){
		var changedOnds = new Array();
		var changedFlightRefList = new Array();
		for ( var i = 0; i < jsonChangedFaresInfo.length; i++) {
			var changedFareObj = jsonChangedFaresInfo[i];
			changedFlightRefList.push(changedFareObj.flightSegRefNo);
		}
		
		for ( var j = 0; j < flightSegmentList.length; j++) {
			var flightSeg = flightSegmentList[j];
			var ondSeq = flightSeg.ondSequence;
			if(ondSeq != null){				
				if($.inArray(flightSeg.flightRefNumber, changedFlightRefList) != -1
						&& $.inArray(ondSeq, changedOnds) == -1){
					changedOnds.push(ondSeq);
				}			
			}
		}
		
		$.each(opener.UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability, function(key, value){
			var ondSeq = parseInt(key);
			if($.inArray(ondSeq, changedOnds) == -1){
				opener.UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability[ondSeq] = false;
			} else {
				opener.UI_tabSearchFlights.ondPartialRebuildLogicalCCAvailability[ondSeq] = true;
			}
		});
	}
	
	/*
	 * Show Fare Click
	 */
	UI_changeFare.showFaresOnClick = function(){
		if (opener.UI_tabSearchFlights.objResSrchParams.openReturn) {
			var selrow = UI_changeFare.selectedSegmentIndex;
			var flightInfo = flightSegmentList;
			if (typeof flightInfo[selrow].openReturnSegment != 'undefined' && flightInfo[selrow].openReturnSegment 
					&& flightInfo[selrow].returnFlag) {
				return false;
			}
		}
		if(rowid == null || rowid == ""){
			return false;
		}
		
		UI_commonSystem.showProgress();
		var data = UI_changeFare.createSearchParams('changeFareQuoteParams');	
		if(opener.UI_tabSearchFlights.isRequote()){			
			data['pnr'] = opener.DATA_ResPro.reservationInfo.PNR;
			if (fromViewBCFare == true) {
				data['changeFareQuoteParams.ondListString'] = $.toJSON(getNewONDSearchDTOList());
			} else {
				data['changeFareQuoteParams.ondListString'] = $.toJSON(opener.UI_requote.getONDSearchDTOList());
			}
			data['changeFareQuoteParams.ondQuoteFlexiStr'] = opener.UI_requote.getFlexiQuoteForRequote();
			data['changeFareQuoteParams.ticketValidTillStr'] = window.opener.UI_requote.getTicketValidTill();
			data['changeFareQuoteParams.lastFareQuoteDateStr'] = window.opener.UI_requote.getLastFareQuoteDate();
			data['requote'] = true;
			selectedCabinClass = opener.UI_requote.selectedSegmentCabinclass;
		} else if(opener.UI_tabSearchFlights.isMulticity()){
			data['changeFareQuoteParams.ondListString'] = $.toJSON(opener.UI_Multicity.getONDSearchDTOList());
			data['changeFareQuoteParams.bookingType'] = opener.UI_Multicity.ondBookType();
			data['changeFareQuoteParams.departureDate'] = null;
			data['changeFareQuoteParams.fromAirport'] = null;
			data['changeFareQuoteParams.toAirport'] = null;
			data['changeFareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(opener.UI_Multicity.ondWiseFlexiSelected());			
			data['multicitySearch'] = true;
		} else {
			//data['changeFareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(UI_changeFare.overrideFlexiSelection());
			data['changeFareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(window.opener.UI_tabSearchFlights.ondWiseFlexiSelected);
			data['changeFareQuoteParams.ondWiseCitySearchStr'] = $.toJSON(window.opener.UI_tabSearchFlights.ondWiseCitySearch);		
		}
		data['selectedFlightSegments'] = $.toJSON(flightSegmentList);
		//data['logicalCCSelection'] = $.toJSON(opener.UI_tabSearchFlights.logicalCabinClassSelection);
		data['selectedSegmentRefNumber'] = UI_changeFare.selectedSegment;
		UI_changeFare.createSubmitData({data:data});
		data['mode'] = "SHOWALLFARES";
		if(opener.UI_tabSearchFlights.isAllFareChange){
			data['showAllBC'] = true;
		} else {
			data['showAllBC'] = false;
		}
		data['airLine'] = jsonFltDetails[rowid].airLine;
		data['carrierWiseSeg'] = UI_changeFare.carrierWiseSegMap();		
		UI_changeFare.setSearchSystem(data);
		data['changeFareQuoteParams.ticketValidityExist'] = window.opener.UI_tabSearchFlights.ticketValidityExist;
		data['changeFareQuoteParams.ticketValidityMax'] = window.opener.UI_tabSearchFlights.ticketValidityMax;		
		data['allowOverrideVisibleFare'] = opener.DATA_ResPro.initialParams.allowOverrideVisibleFare;
		data['allowOverrideAnyFare'] = opener.DATA_ResPro.initialParams.allowOverrideAnyFare;
		data['allowOverrideAgentFareOnly'] = opener.DATA_ResPro.initialParams.allowOverrideAgentFareOnly;
		data['cabinClassRanking'] = $.toJSON(opener.top.arrCabinClassRanking);
		
		if(UI_changeFare.selectedSegment == null || UI_changeFare.selectedSegment == "null"){			
			UI_commonSystem.hideProgress();
			return false;
		}
		UI_changeFare.clearAllFares();
	    $("#frmChgFares").ajaxSubmit({ dataType: 'json', data:  data, success:UI_changeFare.returnCallShowAvailableFaresInfo,url:"changeFare.action",
								error:UI_commonSystem.setErrorStatus});
		return false;
	};
	
	this.getNewONDSearchDTOList = function() {
		var list = [];
		for (var x = 0; x < opener.UI_tabSearchFlights.getFlightRPHList().length; x++) {
			var fromTo = opener.UI_tabSearchFlights.getFlightRPHList()[x].segmentCode.split('/');
			var flightRPH = [];
			flightRPH[0] = opener.UI_tabSearchFlights.getFlightRPHList()[x].flightRPH.split('#')[0];
			var dateString = opener.UI_tabSearchFlights.getFlightRPHList()[x].departureTime;
			var date = "20" + dateString.substring(0,2) + "-" + dateString.substring(2,4) + "-" + dateString.substring(4,6) + "T" + dateString.substring(6,8) + ":" + dateString.substring(8) + ":00";
			var searchOND = {
				fromAirport : fromTo[0],
				toAirport : fromTo[fromTo.length - 1],
				flightRPHList : flightRPH,
				flownOnd : false,
				existingFlightRPHList : [],
				existingResSegRPHList : [],
				departureDate : date,
				classOfService : opener.UI_tabSearchFlights.jsonOndFQParams().cabinClass,
				logicalCabinClass : opener.UI_tabSearchFlights.jsonOndFQParams().logicalCabinClass,
				bookingType : opener.UI_tabSearchFlights.jsonOndFQParams().bookingType,
				bookingClass : opener.UI_tabSearchFlights.jsonOndFQParams().bookingClass,
				modifiedResSegList: [],
				oldPerPaxFare : 0
			};
			list[x] = searchOND;
		}
		return list;
	}

	UI_changeFare.setSearchSystem = function(data){
		data['changeFareQuoteParams.searchSystem'] = "AA";
		var systemAirlineCode = UI_changeFare.getAirlineCode(opener.carrierCode);
		for(var rowid=0;rowid<jsonFltDetails.length;rowid++){			
			var fltAirlineCode = UI_changeFare.getAirlineCode(jsonFltDetails[rowid].airLine);
			
			if(systemAirlineCode != null && (fltAirlineCode != undefined || fltAirlineCode != null)){
				if(systemAirlineCode != fltAirlineCode){
					data['changeFareQuoteParams.searchSystem'] = "INT";
					return;
				}
			} else {
				if(jsonFltDetails[rowid].airLine != '' && jsonFltDetails[rowid].airLine != opener.carrierCode){
					data['changeFareQuoteParams.searchSystem'] = "INT";
					return;
				}
			}
		}
	}
	
	UI_changeFare.returnCallShowAvailableFaresInfo = function(response){
		if(response.success){
			UI_changeFare.fareQuotedJourneyList = response.fareQuotedFltRefList;
			UI_changeFare.showNestedAvailableSeats = response.showNestedAvailableSeats;
			UI_changeFare.splitOperationForBusInvoked = response.splitOperationForBusInvoked;
			UI_changeFare.allowHalfRetFareForOpenReturn = response.allowHalfRetFareForOpenReturn;
			UI_changeFare.showAgentCommission = response.showAgentCommission;
			UI_changeFare.populateAllFaresInfo(response.allFaresInfo);
			UI_changeFare.setAllFareInfo (UI_changeFare.selectedSegment,response.allFaresInfo, response.allFaresInfoMap);
			UI_changeFare.fillFareDetails();
			UI_changeFare.modifyBooking = response.modifyBooking;
			UI_changeFare.setFareQuotedOndSequence(flightSegmentList);
			$("#divAvailableFares").show();
			if (fromViewBCFare) {
				$("#btnReCal").hide();
				$("#btnConfim").hide();
			} else {
				$("#btnReCal").show();
				$("#btnConfim").show();
			}
		}		
		UI_commonSystem.hideProgress();	
	};
	
	/*
	 * Close window
	 */
	UI_changeFare.closeOnClick = function(){
		window.close();
	};
	

	
	/*
	 * Fill Passenger Details
	 */
	UI_changeFare.fillFareDetails = function(){
		$("#tblFares").find("tr").remove();
				
		var priviShowSeats =(opener.top.arrPrivi[PRIVI_SHOW_SEATS] == 1);
		var priviShowAgents = (opener.top.arrPrivi[PRIVI_SHOW_AGENTS] == 1);
		var childApplicable = (opener.top.childVisibility == "Y");
		var infantApplicable = (opener.top.childVisibility == "Y");
		var showAgentCommission = ( UI_changeFare.showAgentCommission == "undefined") ? false: UI_changeFare.showAgentCommission;
		
		if (jsonFares != null){
			
			if(jsonFares.length > 0){
				var jsonFareObj = jsonFares[0];
				if(jsonFareObj.segment == "" && jsonFareObj.typeAdult == "" && jsonFareObj.typeChild == "" && jsonFareObj.typeInfant == ""){
					tblRow = document.createElement("TR");
					tblRow.appendChild(UI_commonSystem.createCell({desc:"No Different Fares Available", bold:true, align:"center", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight"}));
					$("#tblFares").append(tblRow);
					return;
				}
			}
			
			var i = 0;
			var x = 0;
			var tblRow = null;
			var objD = null;
			
			var colspanFareDet = 5;
			var colSpanMisc = 4;
			
			if(priviShowAgents){
				colspanFareDet = colspanFareDet +1;
				colSpanMisc = colSpanMisc + 1;
			}
			
			if(priviShowSeats){
				colSpanMisc = colSpanMisc + 3;
			}
			
			if(childApplicable) {
				colspanFareDet = colspanFareDet +1;
			}
			
			if(infantApplicable){
				colspanFareDet = colspanFareDet +1;
			}
			
			tblRow = document.createElement("TR");
				
			tblRow.appendChild(UI_commonSystem.createCell({desc:"BC", bold:true, align:"center", css:UI_commonSystem.strHDClass + " ui-corner-TL rowHeight"}));
			tblRow.appendChild(UI_commonSystem.createCell({desc:"Fare Details", bold:true, align:"center",  css:UI_commonSystem.strHDClass, colSpan:colspanFareDet}));
			
			if(showAgentCommission){
				tblRow.appendChild(UI_commonSystem.createCell({desc:"Entitled <br>Commission", bold:true, align:"center",  css:UI_commonSystem.strHDClass }));
			}
			
			if(priviShowSeats){
				tblRow.appendChild(UI_commonSystem.createCell({desc:"Alloc.", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:"Sold/<br>On Hold", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:"Act.Sold/<br>On Hold", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:"Avail", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				
				if(UI_changeFare.showNestedAvailableSeats){
					tblRow.appendChild(UI_commonSystem.createCell({desc:"Nest<br>Avail", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				}	
				
			}
			tblRow.appendChild(UI_commonSystem.createCell({desc:"Status", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
			tblRow.appendChild(UI_commonSystem.createCell({desc:"Type", bold:true, align:"center",  css:UI_commonSystem.strHDClass }));
			
			if (opener.top.isLogicalCCEnabled) {			
				tblRow.appendChild(UI_commonSystem.createCell({desc:"CoS", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:"Logical CC", bold:true, align:"center",  css:UI_commonSystem.strHDClass + " ui-corner-TR"}));
			} else {
				tblRow.appendChild(UI_commonSystem.createCell({desc:"CoS", bold:true, align:"center",  css:UI_commonSystem.strHDClass + " ui-corner-TR"}));
			}
			
			$("#tblFares").append(tblRow);
				
			$.each(jsonFares, function(){
				tblRow = document.createElement("TR");
				
			
				tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFares[i].segment, bold:true, align:"center", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", colSpan:2}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:"&nbsp;", align:"center", css:UI_commonSystem.strDefClass, colSpan:2}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFares[i].typeAdult, bold:true, align:"center", css:UI_commonSystem.strDefClass}));
				if(childApplicable){
					tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFares[i].typeChild, bold:true, align:"center", css:UI_commonSystem.strDefClass}));
				}
				if(infantApplicable){
					tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFares[i].typeInfant, bold:true, align:"center", css:UI_commonSystem.strDefClass}));
				}
				
				tblRow.appendChild(UI_commonSystem.createCell({desc:"&nbsp;", align:"center", css:UI_commonSystem.strDefClass,  colSpan:colSpanMisc}));
				
				$("#tblFares").append(tblRow);
				
				var len = 0;
				var prevBookingCode = '';
				x = 0;
				objD = jsonFares[i].fareDeatils;
				if(selectedCabinClass != "" && UI_changeFare.selectedSegmentCOS == undefined){
					UI_changeFare.selectedSegmentCOS = selectedCabinClass;
				}
				
				$.each(objD, function(){
					if(!$.isEmptyObject(objD[x])){
						if((UI_changeFare.selectedSegmentCOS != "" &&
								(!opener.UI_tabSearchFlights.isRequote()) ||
								(opener.top.arrPrivi['xbe.res.allow.modify.bookings.low.priority.cos'] == 1) ||
								((opener.top.arrCabinClassRanking[objD[x].ccCode] <= opener.top.arrCabinClassRanking[UI_changeFare.selectedSegmentCOS]) && 
								(opener.top.arrPrivi['xbe.res.allow.modify.bookings.low.priority.cos'] != 1))) ||
								(UI_changeFare.selectedSegmentCOS == "")){
						tblRow = document.createElement("TR");
					
						var requestedSeatCnt = Number(opener.UI_tabSearchFlights.objResSrchParams.adultCount)+ Number(opener.UI_tabSearchFlights.objResSrchParams.childCount);
						var disabled = '';
						
						if(!objD[x].skipAvailSeatCheck && objD[x].avail <  requestedSeatCnt){
							disabled = " disabled='disabled' ";
						}
						
						if(UI_changeFare.showNestedAvailableSeats && objD[x].nestAvail >= requestedSeatCnt){
							disabled = '';
						}
						if(!objD[x].isCommonAllSegments){
							disabled = " disabled='disabled' ";
						}
						
						len = (prevBookingCode != objD[x].bookingCode)?0:len;
						if(len == 0){
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].bookingCode, align:"center", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight", rowSpan:objD[x].rowSpan}));
							prevBookingCode = objD[x].bookingCode;
						}
						
						tblRow.appendChild(UI_commonSystem.createCell({desc:"<input type='radio' id='radFare'"+ disabled +" name='radFare' value='" +objD[x].faretype+"^"+ x + "'>", align:"center", css:UI_commonSystem.strDefClass}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].fareRuleCode, align:"center",  css:UI_commonSystem.strDefClass}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].fareBasisCode, css:UI_commonSystem.strDefClass}));
						tblRow.appendChild(UI_commonSystem.createCell({desc:new Number(objD[x].afare).toFixed(2), align:"right",  css:UI_commonSystem.strDefClass}));
						if(childApplicable) {
							tblRow.appendChild(UI_commonSystem.createCell({desc:new Number(objD[x].cfare).toFixed(2), align:"right",  css:UI_commonSystem.strDefClass}));
						}
						if(infantApplicable){
							tblRow.appendChild(UI_commonSystem.createCell({desc:new Number(objD[x].ifare).toFixed(2), align:"right",  css:UI_commonSystem.strDefClass}));
						}
						if(priviShowAgents){
							var arrAgents = objD[x].agent.split("<br>");
							var displayAgentStr = "";
							var agentStr = "";
							var showToolTip = false;
							if(arrAgents.length <= 5){
								displayAgentStr = objD[x].agent;
								showToolTip = false;
							}else{
								for(var ag=0; ag < 5; ag++)
									displayAgentStr = displayAgentStr+arrAgents[ag]+"<br>";
								displayAgentStr = displayAgentStr + "...";
	
								showToolTip = true;
							}
							displayAgentStr = (displayAgentStr == "") ? "&nbsp;" : displayAgentStr;
							if(showToolTip)
								tblRow.appendChild(UI_commonSystem.createCell({desc:displayAgentStr, align:"center", css:UI_commonSystem.strDefClass, id:"td_"+i+"_"+x, mouseOver:UI_changeFare.displayAllAgentsToolTip, mouseOut:UI_changeFare.hideToolTip}));
							else
								tblRow.appendChild(UI_commonSystem.createCell({desc:displayAgentStr, align:"center", css:UI_commonSystem.strDefClass, id:"td_"+i+"_"+x}));
							//tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].agent, align:"center", css:UI_commonSystem.strDefClass}));
						}
						tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].channel, align:"center", css:UI_commonSystem.strDefClass}));
						
						if(showAgentCommission){
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].agntCom, align:"center", css:UI_commonSystem.strDefClass}));
						}
						
						if(len == 0){
							if(priviShowSeats){
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].alloc, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].sold, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].actualSold, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].avail, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
	
								if(UI_changeFare.showNestedAvailableSeats){
									tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].nestAvail, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
								}
							}
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].status, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].bctype, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
							tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].ccCode, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
							
							if (opener.top.isLogicalCCEnabled) {
								tblRow.appendChild(UI_commonSystem.createCell({desc:objD[x].logicalCC, align:"center",  css:UI_commonSystem.strDefClass, rowSpan:objD[x].rowSpan}));
							}
						}
						len++;
						$("#tblFares").append(tblRow);
						}
						x++;
				}
				});
				if(len==0){					
					tblRow = document.createElement("TR");
					tblRow.appendChild(UI_commonSystem.createCell({desc:"No privilege to modify bookings to low priority cabin classes", bold:true, align:"center", css:UI_commonSystem.strDefClass,colSpan:15}));
					$("#tblFares").append(tblRow);
				}
				i++;
			});
		}
	};
	
	UI_changeFare.populateAllFaresInfo = function(allFaresInfo){
		UI_changeFare.allFaresInfo = allFaresInfo;
		if(allFaresInfo!=undefined && allFaresInfo!=null){
			for (var i = 0; i < allFaresInfo.length; i++)
			{
				var displayCodeForFareType = allFaresInfo[i].displayCodeForFareType;
				var fareType = allFaresInfo[i].fareType;
				var fareRuleDTOs = allFaresInfo[i].fareRuleInfo;
				var invAllocDTOs = allFaresInfo[i].invAllocationInfo;
				if(fareRuleDTOs != null && fareRuleDTOs.length > 0) {
					jsonFares[i] = {segment : "", typeAdult : "", typeChild : "", typeInfant : "", fareDeatils : [{}]};
					jsonFares[i].segment = displayCodeForFareType;
					jsonFares[i].typeAdult ="Adult";
					jsonFares[i].typeChild ="Child";
					jsonFares[i].typeInfant = "Infant";
					
					for ( var fr = 0; fr < fareRuleDTOs.length; fr++) {
						var frule = fareRuleDTOs[fr];
						var inv = invAllocDTOs[fr];
						var rowspan = UI_changeFare.getAssociatedFareCountForBC(frule,fareRuleDTOs);
						aFareAmt = (frule.adultFareApplicable)?frule.adultFareAmount:0;
						cFareAmt = (frule.childFareApplicable)?frule.childFareAmount:0;
						iFareAmt = (frule.infantFareApplicable)?frule.infantFareAmount:0;
						
						var soldAndOnholdSeats = inv.soldSeats + "/"+ inv.onholdSeats;
						var actualSoldOnholdSeats = inv.actualSeatsSold + "/"+ inv.actualSeatsOnHold;
						if(frule.bookingClassCode == inv.bookingCode) {
							jsonFares[i].fareDeatils[fr] = {
									bookingCode : frule.bookingClassCode, 
									fareRuleCode : (frule.fareRuleCode != null && frule.fareRuleCode != undefined)?frule.fareRuleCode:"", 
									fareBasisCode : (frule.fareBasisCode != null && frule.fareBasisCode != undefined)?frule.fareBasisCode:"",
									afare 	: aFareAmt,  
									cfare 	: cFareAmt,
									ifare 	: iFareAmt,
									alloc 	: inv.allocatedSeats,
									sold  	: soldAndOnholdSeats,
									avail	: inv.availableMaxSeats,
									agent	: frule.visibleAgents,
									channel : frule.visibleChannelNames,
									status	: inv.bcInvStatus,
									bctype	: inv.bcType,
									faretype: fareType,
									fareId  : frule.fareId,
									agntCom : (frule.applicableAgentCommission == null)? '-':frule.applicableAgentCommission,
									logicalCC: inv.logicalCabinClass,
									rowSpan : rowspan,
									ccCode : frule.cabinClassCode,
									isSameBookingClass: inv.sameBookingClassBeingSearch,
									skipAvailSeatCheck: inv.skipAvailSeatCheck,
									nestAvail: inv.availableNestedSeats,
									isCommonAllSegments: inv.commonAllSegments,
									actualSold: actualSoldOnholdSeats
								};	
						}
					}
				}
			}
		}else{
			UI_commonSystem.hideProgress();
		}
	};
	
	UI_changeFare.populateSegmentDetails = function(fareQuoteInfo, flightInfo,fltSegIdByJourneyMap,segmentsMapPerSegType) {
		var logicalCCSelection = window.opener.UI_tabSearchFlights.logicalCabinClassSelection;
		var adultFare = 0;
		var childFare = 0;
		var infantFare = 0;
		var segFareObject = null;		

		var ondGroup = 0;
		var isFirstSegOfOnd = true;
		if(fareQuoteInfo.length < flightInfo.length){
			var subJourneyGroup = 1;
			for ( var m=0; m < flightInfo.length; m++) {
				var flt = flightInfo[m];

				if(flt.returnFlag){
					returnBooking = true;
				}

				var logicalCC = ""; 

				if (opener.top.isLogicalCCEnabled && logicalCCSelection!=null) {
					if (flt.requestedOndSequence != null && flt.requestedOndSequence != -1) {
						logicalCC = logicalCCSelection[flt.requestedOndSequence];	
					} else if (flt.ondSequence != null) {	
						logicalCC = logicalCCSelection[flt.ondSequence];
					}					
				}

				if(fareQuoteInfo == null || fareQuoteInfo.length == 0){
					jsonTestFltDetails[m] = {
							orignNDest : flt.orignNDest, 
							adultFare : adultFare, 
							childFare : childFare,
							infantFare : infantFare,  
							flightNo : flt.flightNo,
							airLine : flt.airLine,
							departureDate : flt.departureDate,
							fltSegRefNo : flt.flightRefNumber,
							returnFlag : flt.returnFlag,
							ondCode : flt.orignNDest,
							ondSequence: flt.ondSequence,
							subJourneyGroup: flt.subJourneyGroup,
							requestedOndSequence: flt.requestedOndSequence,
							ondGroup : ondGroup++,
							logicalCC: logicalCC,
							bcAllocated :  false,
							cabinClass : flt.cabinClass,
							fareType : 0,
							fareId: 0
					};
				} else {
					for ( var i = 0; i < fareQuoteInfo.length; i++) {
						var segExists = false;
						var fq = fareQuoteInfo[i];
						//if(fq.ondSequence == flt.ondSequence){
						if(fq.fullOndCode.indexOf(flt.orignNDest) == 0){
							isFirstSegOfOnd = true;
							segExists =  true;
							ondGroup++;
						}else if(fq.fullOndCode.indexOf(flt.orignNDest) != -1){
							isFirstSegOfOnd = false;
							segExists = true;
						}
						if(segExists){
							var segFareExist = (fq.segmentWise.length > 0)?true:false;
							if(segFareExist){
								segFareObject = UI_changeFare.getSegmentFare(flt.orignNDest, fq.segmentWise, "AD");
							}
							if(!segFareExist || (segFareExist && !segFareObject.isFareFound)){

								adultFare = 0;
								childFare = 0;
								infantFare= 0;

								for(var p=0; p < fq.paxWise.length ;p++){
									var paxFare = fq.paxWise[p];

									if(paxFare != null && paxFare != undefined){
										var paxType = paxFare.paxType;
										if(paxType.indexOf('Chi') == 0){
											childFare = paxFare.fare;
										} else if(paxType.indexOf('Inf') == 0) {
											infantFare = paxFare.fare;
										} else {
											adultFare = paxFare.fare;
										}
									}
								}

							}else{
								if(segFareObject.isFareFound){
									adultFare = segFareObject.fare;
								}
								segFareObject = UI_changeFare.getSegmentFare(flt.orignNDest, fq.segmentWise, "CH");
								if(segFareObject.isFareFound){
									childFare = segFareObject.fare;
								}
								segFareObject = UI_changeFare.getSegmentFare(flt.orignNDest, fq.segmentWise, "IN");
								if(segFareObject.isFareFound){
									infantFare = segFareObject.fare;
								}

							}

							jsonTestFltDetails[m] = {
									orignNDest : flt.orignNDest, 
									adultFare : adultFare, 
									childFare : childFare,
									infantFare : infantFare,  
									flightNo : flt.flightNo,
									airLine : flt.airLine,
									departureDate : flt.departureDate,
									fltSegRefNo : flt.flightRefNumber,
									returnFlag : flt.returnFlag,
									ondCode : fq.ondCode,
									ondSequence: flt.ondSequence,
									subJourneyGroup: flt.subJourneyGroup,
									requestedOndSequence: flt.requestedOndSequence,
									ondGroup : ondGroup,
									logicalCC: logicalCC,
									bcAllocated :  true,
									cabinClass : flt.cabinClass,
									fareType :fq.fareType,
									fareId: fq.fareId,
									segmentTypeId : flt.segmentTypeId
							};

						}
					}
				}
			}
			sortBySegmentType(jsonTestFltDetails,segmentsMapPerSegType);

		}else {
			for ( var m=0; m < flightInfo.length; m++) {
				var flt = flightInfo[m];

				if(flt.returnFlag){
					returnBooking = true;
				}

				var logicalCC = ""; 

				if(opener.top.isLogicalCCEnabled && flt.requestedOndSequence != null && logicalCCSelection!=null){
					
					if(logicalCCSelection[flt.requestedOndSequence]){
						logicalCC = logicalCCSelection[flt.requestedOndSequence];
					}else if(logicalCCSelection[flt.ondSequence]){
						logicalCC = logicalCCSelection[flt.ondSequence];
					}else{
						logicalCC = logicalCCSelection[flt.departure + "/" + flt.arrival];
					}
					
				}

				if(fareQuoteInfo == null || fareQuoteInfo.length == 0){
					jsonTestFltDetails[m] = {
							orignNDest : flt.orignNDest, 
							adultFare : adultFare, 
							childFare : childFare,
							infantFare : infantFare,  
							flightNo : flt.flightNo,
							airLine : flt.airLine,
							departureDate : flt.departureDate,
							fltSegRefNo : flt.flightRefNumber,
							returnFlag : flt.returnFlag,
							ondCode : flt.orignNDest,
							ondSequence: flt.ondSequence,
							subJourneyGroup: flt.subJourneyGroup,
							requestedOndSequence: flt.requestedOndSequence,
							ondGroup : ondGroup++,
							logicalCC: logicalCC,
							bcAllocated :  false,
							cabinClass : flt.cabinClass,
							fareType : 0
					};
				} else {
					for ( var i = 0; i < fareQuoteInfo.length; i++) {
						var fq = fareQuoteInfo[i];
						if(fq.ondSequence == flt.ondSequence){
							if(fq.fullOndCode.indexOf(flt.orignNDest) == 0){
								isFirstSegOfOnd = true;
								ondGroup++;
							}else{
								isFirstSegOfOnd = false;
							}

							var segFareExist = (fq.segmentWise.length > 0)?true:false;
							if(segFareExist){
								segFareObject = UI_changeFare.getSegmentFare(flt.orignNDest, fq.segmentWise, "AD");
							}
							if(!segFareExist || (segFareExist && !segFareObject.isFareFound)){

								adultFare = 0;
								childFare = 0;
								infantFare= 0;

								for(var p=0; p < fq.paxWise.length ;p++){
									var paxFare = fq.paxWise[p];

									if(paxFare != null && paxFare != undefined){
										var paxType = paxFare.paxType;
										if(paxType.indexOf('Chi') == 0){
											childFare = paxFare.fare;
										} else if(paxType.indexOf('Inf') == 0) {
											infantFare = paxFare.fare;
										} else {
											adultFare = paxFare.fare;
										}
									}
								}

							}else{
								if(segFareObject.isFareFound){
									adultFare = segFareObject.fare;
								}
								segFareObject = UI_changeFare.getSegmentFare(flt.orignNDest, fq.segmentWise, "CH");
								if(segFareObject.isFareFound){
									childFare = segFareObject.fare;
								}
								segFareObject = UI_changeFare.getSegmentFare(flt.orignNDest, fq.segmentWise, "IN");
								if(segFareObject.isFareFound){
									infantFare = segFareObject.fare;
								}

							}

							jsonTestFltDetails[m] = {
									orignNDest : flt.orignNDest, 
									adultFare : adultFare, 
									childFare : childFare,
									infantFare : infantFare,  
									flightNo : flt.flightNo,
									airLine : flt.airLine,
									departureDate : flt.departureDate,
									fltSegRefNo : flt.flightRefNumber,
									returnFlag : flt.returnFlag,
									ondCode : fq.ondCode,
									ondSequence: flt.ondSequence,
									subJourneyGroup: flt.subJourneyGroup,
									requestedOndSequence: flt.requestedOndSequence,
									ondGroup : ondGroup,
									logicalCC: logicalCC,
									bcAllocated :  true,
									cabinClass : flt.cabinClass,
									fareType :fq.fareType
							};
						}
					}
				}
			}
			sortBySegmentType(jsonTestFltDetails,segmentsMapPerSegType);

			var count = 1;
			if(fltSegIdByJourneyMap != undefined && fltSegIdByJourneyMap != null 
					&& jsonFltDetails!= undefined && jsonFltDetails!=null) {
				var isUpdatedSubJourneyGroupId = false;
				$.each(fltSegIdByJourneyMap, function(index, val){	
					if(val!=undefined && val!=null){
						for(var k=0;k<val.length;k++){					
							for(var jind=0; jind<jsonFltDetails.length; jind++){				
								if(val[k]!=undefined && val[k]!=null 
										&& jsonFltDetails[jind].fltSegRefNo == (val[k])){
								//	jsonFltDetails[jind].subJourneyGroup = count;
									isUpdatedSubJourneyGroupId = true;
								}
							}
						}
						count++;
					}
				});			
				if(isUpdatedSubJourneyGroupId){
					quickSort(jsonFltDetails, 0, jsonFltDetails.length -1);
					sortByDate(jsonFltDetails);
				}
			}	

		}
		UI_changeFare.setJourneyWiseOnd();
	};
	
	function sortBySegmentType(jsonTestFltDetails,segmentsMapPerSegType) {
		var count = 0;
		if(segmentsMapPerSegType != undefined && segmentsMapPerSegType != null 
				&& jsonTestFltDetails!= undefined && jsonTestFltDetails!=null) {
			$.each(segmentsMapPerSegType, function(index, val){	
					if(val!=undefined && val!=null){
						for(var k=0;k<val.length;k++){	
							for(var jind=0; jind<jsonTestFltDetails.length; jind++){
								if(val[k]!=undefined && val[k]!=null 
										&& jsonTestFltDetails[jind].fltSegRefNo.indexOf(val[k]) == 0){
									jsonFltDetails[count] = jsonTestFltDetails[jind];
									count++;
								}
							}
						}
					}
			});
		}
	}
	
	function sortByDate(jsonFltDetails) {
	    var temp = jsonFltDetails[0];
	    for (var i = 1; i < jsonFltDetails.length; i++) {
	        if (temp.subJourneyGroup == jsonFltDetails[i].subJourneyGroup && temp.departureDate > jsonFltDetails[i].departureDate) {
	            jsonFltDetails[i - 1] = jsonFltDetails[i];
	            jsonFltDetails[i] = temp;
	        }
	        temp = jsonFltDetails[i];
	    }
	}

	function quickSort(jsonFltDetails, p, r) {
	    if (p < r) {
	        var q = partition(jsonFltDetails, p, r);
	        quickSort(jsonFltDetails, p, q);
	        quickSort(jsonFltDetails, q + 1, r);
	    }
	}

	function partition(jsonFltDetails, p, r) {

	    var x = jsonFltDetails[p].subJourneyGroup;
	    var i = p - 1;
	    var j = r + 1;

	    while (true) {
	        i++;
	        while (i < r && jsonFltDetails[i].subJourneyGroup < x)
	            i++;
	        j--;
	        while (j > p && jsonFltDetails[j].subJourneyGroup > x)
	            j--;

	        if (i < j)
	            swap(jsonFltDetails, i, j);
	        else
	            return j;
	    }
	}

	function swap(jsonFltDetails, i, j) {
	    var temp = jsonFltDetails[i];
	    jsonFltDetails[i] = jsonFltDetails[j];
	    jsonFltDetails[j] = temp;
	}
		
	/*
	 * Fill Passenger Details
	 */
	UI_changeFare.fillSegmentDetails = function(flightInfo){
		$("#tblSegemnts").find("tr").remove();

		var childApplicable = (opener.top.childVisibility == "Y");
		var infantApplicable = (opener.top.childVisibility == "Y");
		UI_changeFare.selectedSegmentCOS = "";
		
		if (jsonFltDetails != null){
			var i = 0;
			var tblRow = null;

			var rowSpanFare = 1;
			var prevRowSpan = -1;
			var intLen = 1;
			
			tblRow = document.createElement("TR");
				
			tblRow.appendChild(UI_commonSystem.createCell({desc: getOpeneri18nData('changeFare_Segment','Segment'), bold:true, align:"center", css:UI_commonSystem.strHDClass + " ui-corner-TL rowHeight"}));
			tblRow.appendChild(UI_commonSystem.createCell({desc:getOpeneri18nData('changeFare_AdultFare','Adult Fare') , bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
			if(childApplicable){
				tblRow.appendChild(UI_commonSystem.createCell({desc: getOpeneri18nData('changeFare_ChildFare','Child Fare') , bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
			}
			if(infantApplicable){
				tblRow.appendChild(UI_commonSystem.createCell({desc: getOpeneri18nData('changeFare_InfantFare','Infant Fare'), bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
			}
			tblRow.appendChild(UI_commonSystem.createCell({desc:getOpeneri18nData('changeFare_FlightNo','Flight No'), bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
			tblRow.appendChild(UI_commonSystem.createCell({desc:"&nbsp;", bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
			
			if (opener.top.isLogicalCCEnabled) {
				tblRow.appendChild(UI_commonSystem.createCell({desc:getOpeneri18nData('changeFare_DepartureDate','Departure Date'), bold:true, align:"center",  css:UI_commonSystem.strHDClass}));
				tblRow.appendChild(UI_commonSystem.createCell({desc:getOpeneri18nData('changeFare_LogicalCabinClass','Logical CC'), bold:true, align:"center",  css:UI_commonSystem.strHDClass + " ui-corner-TR"}));			
			} else {
				tblRow.appendChild(UI_commonSystem.createCell({desc:getOpeneri18nData('changeFare_InfantFare','Infant Fare'), bold:true, align:"center",  css:UI_commonSystem.strHDClass + " ui-corner-TR"}));
			}
			tblRow.appendChild(UI_commonSystem.createCell({desc:getOpeneri18nData('changeFare_COS','COS'), align:"center",textCss :"display:none"}));
			
			
			var ondWiseSegCount = {};
			
			
			
			$("#tblSegemnts").append(tblRow);
			
			$.each(jsonFltDetails, function(){
				var jsonFltInfo;
				if(ondWiseFareQuoteFlag[jsonFltDetails[i].subJourneyGroup][jsonFltDetails[i].ondSequence]){
					jsonFltInfo = jsonFltDetails[i];
				}else{
					jsonFltInfo = jsonFltDetailsOrg[i];
				}			

					tblRow = document.createElement("TR");
					rowSpanFare = UI_changeFare.calculateRowSpan(jsonFltInfo.ondSequence, ondWiseFareType, ondWiseSegmentCount,jsonFltInfo.subJourneyGroup,jsonFltInfo.fareType);
					
					
					intLen = (intLen>rowSpanFare)?1:intLen;
					
					if(prevRowSpan == -1)
						prevRowSpan = rowSpanFare;
					else if((prevRowSpan != rowSpanFare)){
						intLen = 1;
						prevRowSpan = rowSpanFare;
					}
					
					if (typeof flightInfo[i].openReturnSegment != 'undefined' && flightInfo[i].openReturnSegment 
							&& flightInfo[i].ondSequence == window.opener.UI_tabSearchFlights.OND_SEQUENCE.IN_BOUND) {
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.orignNDest,  align:"center", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight",
							id:"td_"+i+"_0"
						}));
					} else {
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.orignNDest,  align:"center", css:UI_commonSystem.strDefClass + " thinBorderL rowHeight",
							id:"td_"+i+"_0",click:UI_changeFare.selectSegment
						}));
					}
					
					if(intLen == 1){
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.adultFare, align:"center", css:UI_commonSystem.strDefClass, rowSpan:rowSpanFare }));
					}else if(rowSpanFare == 1){
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.adultFare, align:"center", css:UI_commonSystem.strDefClass}));
					}
					if(childApplicable && intLen == 1){
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.childFare, align:"center", css:UI_commonSystem.strDefClass, rowSpan:rowSpanFare}));
					}else if(childApplicable && rowSpanFare == 1){
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.childFare, align:"center", css:UI_commonSystem.strDefClass}));
					}
					if(infantApplicable && intLen == 1){
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.infantFare, align:"center", css:UI_commonSystem.strDefClass, rowSpan:rowSpanFare}));
					}else if(infantApplicable && rowSpanFare == 1){
						tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.infantFare, align:"center", css:UI_commonSystem.strDefClass}));
					}
					tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.flightNo, align:"center", css:UI_commonSystem.strDefClass}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.airLine, align:"center", css:UI_commonSystem.strDefClass}));
					tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.departureDate, align:"center", css:UI_commonSystem.strDefClass}));
					
					if (opener.top.isLogicalCCEnabled) {
						if (intLen == 1) {
							tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.logicalCC, align:"center", css:UI_commonSystem.strDefClass, rowSpan:rowSpanFare}));
						} else if (rowSpanFare == 1) {
							tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.logicalCC, align:"center", css:UI_commonSystem.strDefClass}));
						}
					}
					tblRow.appendChild(UI_commonSystem.createCell({desc:jsonFltInfo.cabinClass, align:"center", textCss :"display:none"}));
					
					intLen++;
					$("#tblSegemnts").append(tblRow);
					i++;
			});
		}
	};
	
	UI_changeFare.selectSegment = function(){
		var arrIDData = this.id.split("_");
		rowid = arrIDData[1];
		var colid = arrIDData[2];
		UI_changeFare.clearAllFares();
		for(var j =0 ;j < jsonFltDetails.length; j++) {
			if(j == rowid){
				$("#td_" + j + "_" + colid).removeClass('rowUnSelected');
				$("#td_" + j + "_" + colid).addClass('rowSelected');
			}else{
				$("#td_" + j + "_" + colid).addClass('rowUnSelected');
				$("#td_" + j + "_" + colid).removeClass('rowSelected');
			}
		}
		
		UI_changeFare.selectedSegment = jsonFltDetails[rowid].fltSegRefNo;
		UI_changeFare.selectedSegmentCOS = jsonFltDetails[rowid].cabinClass;
		
		if(!UI_changeFare.selectedSegment){
			UI_changeFare.selectedSegment = flightSegmentList[rowid].flightRefNumber;
		}
		
		if(!UI_changeFare.selectedSegmentCOS){
			UI_changeFare.selectedSegmentCOS = flightSegmentList[rowid].cabinClass;	
		}
		
		UI_changeFare.selectedSegmentIndex = rowid;
		
		$("#btnReCal").hide();
		$("#btnConfim").hide();
	};
	
	UI_changeFare.clearRowSelection = function() {
		$('#tblSegemnts tr').each(function() {
		       $(this).css('backgroundColor', '');
		   });
		UI_changeFare.selectedSegment = null;
	};
	
	UI_changeFare.clearAllFares = function(){
		$("#divAvailableFares").hide();
		jsonFares = [	{segment : "",
						 typeAdult : "",
						 typeChild : "",
						 typeInfant : "",
						 fareDeatils : [{}]
						}];	
	};
	
	UI_changeFare.isBusSegmentExists = function(){
		var isBusSeg = false;
		if(segmentsMapPerSegType != undefined && segmentsMapPerSegType != null){
			$.each(segmentsMapPerSegType, function(key, value){
				if(key == "1"){
					isBusSeg = true;
				}
			})
		}
		return isBusSeg;
	}
	
	UI_changeFare.calculateRowSpan = function(ondSequence, ondWiseFareType, ondWiseSegCount,journeyGroup,fltFareType){
		var fareType = ondWiseFareType[journeyGroup][ondSequence];
		if(fareType == undefined){
			fareType = fltFareType;
		}
		var rowSpan = 1;
		if(fareType== FARE_TYPE_RETURN_FARE){
			rowSpan = 0;
			var seq = 0;
			$.each(ondWiseSegCount, function(key, value){
				if(key == journeyGroup){
						$.each(value, function(ckey, cvalue){
							if(UI_changeFare.isSameJourneyGroup2(journeyGroup,ckey)){
								rowSpan += cvalue;
							}
						});
				}
			});
		}else if(fareType == FARE_TYPE_OND_FARE || fareType == FARE_TYPE_HALF_RETURN_FARE){
			rowSpan = ondWiseSegCount[journeyGroup][ondSequence];				
		}else if(fareType == FARE_TYPE_PER_FLIGHT_FARE){
			rowSpan = 1;
		}else{
			rowSpan = 1;
		}
		return rowSpan;
	};	
		
	UI_changeFare.recalculateFares = function(){
		var selectedFare = $('input[name=radFare]:checked').val();
		if(selectedFare != null && selectedFare != undefined) {
			var arr = selectedFare.split("^");
			var selectedFareType = arr[0];
			var fareDetailsIndex = arr[1];
			var selectedFareRule = null;
			var selectedSeatInfo = null;
			var selectedFltSegment = UI_changeFare.selectedSegment;
			var selectedFltInfo = null;
			var returnSegSelected = false;
			var prevFareTypeForSeg = 0;
			var selectedSubJourneyGroup = null;
	
			for (var i = 0; i < UI_changeFare.allFaresInfo.length; i++)
			{
				var fareType = UI_changeFare.allFaresInfo[i].fareType;
				if(selectedFareType == fareType) {
						var fareRuleDTOs = UI_changeFare.allFaresInfo[i].fareRuleInfo;
						var seatDTOs = UI_changeFare.allFaresInfo[i].invAllocationInfo;
						selectedFareRule = fareRuleDTOs[fareDetailsIndex];
						selectedSeatInfo = seatDTOs[fareDetailsIndex];
				}
			}
			
			var selectedOndSequence = 0;
			// Re-assigning the selected segment fare with new selected fare
			for(var jind=0; jind<jsonFltDetails.length; jind++){
				if(jsonFltDetails[jind].fltSegRefNo == selectedFltSegment){
					returnSegSelected = jsonFltDetails[jind].returnFlag;
					prevFareTypeForSeg = ondWiseFareType[jsonFltDetails[jind].subJourneyGroup][jsonFltDetails[jind].ondSequence];
					selectedFltInfo = jsonFltDetails[jind];
					jsonFltDetails[jind].bcAllocated = true;
					jsonFltDetails[jind].logicalCC = selectedSeatInfo.logicalCabinClass;
					selectedOndSequence = jsonFltDetails[jind].ondSequence;
					selectedSubJourneyGroup = jsonFltDetails[jind].subJourneyGroup;
				}
			}
			
			if((opener.UI_tabSearchFlights.isAllFareChange || 
					(!opener.UI_tabSearchFlights.isMulticity() && !opener.UI_tabSearchFlights.isRequote())) 
					&& selectedFareRule.cabinClassCode != opener.UI_tabSearchFlights.objResSrchParams.classOfService){
				cabinClassChangedOnds[selectedFareRule.orignNDest] = true;
				opener.UI_tabSearchFlights.ondRebuildLogicalCCAvailability[selectedOndSequence] = true;
			} else {
				cabinClassChangedOnds[selectedFareRule.orignNDest] = false;
				opener.UI_tabSearchFlights.ondRebuildLogicalCCAvailability[selectedOndSequence] = false;
			}
			
			UI_changeFare.setSelectedFareType(selectedFareType, selectedOndSequence, selectedSubJourneyGroup);					
			UI_changeFare.updateFaresOnSelection(selectedFltInfo, selectedFareType, selectedFareRule);	
			UI_changeFare.fillSegmentDetails(flightSegmentList);
			
			for(var fl=0; fl< jsonFltDetails.length; fl++){
				if(ondWiseFareQuoteFlag[jsonFltDetails[fl].subJourneyGroup][jsonFltDetails[fl].ondSequence]){
					if(selectedFareType == FARE_TYPE_RETURN_FARE){
						UI_changeFare.addToChangeFareDetails(jsonFltDetails[fl].fltSegRefNo, selectedFareRule, selectedSeatInfo, jsonFltDetails[fl], selectedFareType);
					} else if(selectedFareType == FARE_TYPE_OND_FARE || selectedFareType == FARE_TYPE_HALF_RETURN_FARE){
						if(jsonFltDetails[fl].ondSequence == selectedFltInfo.ondSequence){
							UI_changeFare.addToChangeFareDetails(jsonFltDetails[fl].fltSegRefNo, selectedFareRule, selectedSeatInfo, jsonFltDetails[fl], selectedFareType);
						}
						if(UI_changeFare.allowHalfRetFareForOpenReturn 
							&& jsonFltDetails[fl].ondSequence != selectedFltInfo.ondSequence 
							&& UI_changeFare.isOpenReturnSegment(jsonFltDetails[fl].ondSequence)){						
							
							UI_changeFare.addHalfReturnFareForOpenReturn(jsonFltDetails[fl]);						
						}
					} else if(selectedFareType == FARE_TYPE_PER_FLIGHT_FARE ){
						if(jsonFltDetails[fl].fltSegRefNo == selectedFltInfo.fltSegRefNo){  
							UI_changeFare.addToChangeFareDetails(jsonFltDetails[fl].fltSegRefNo, selectedFareRule, selectedSeatInfo, jsonFltDetails[fl], selectedFareType);
						}
					}
				}
			}
		}
	};
	
	
	UI_changeFare.addToChangeFareDetails = function(selectedFltSegment, selectedFareRule, selectedSeatInfo, selectedFltInfo, selectedFareType) {
		var blnFound = false;
		for(var i=0; i< jsonChangedFaresInfo.length; i++){
			if(jsonChangedFaresInfo[i].flightSegRefNo != null && jsonChangedFaresInfo[i].flightSegRefNo == selectedFltSegment) {
				jsonChangedFaresInfo[i].fareType = selectedFareType;
				jsonChangedFaresInfo[i].fareRule = selectedFareRule;
				jsonChangedFaresInfo[i].seatInfo = selectedSeatInfo;
				jsonChangedFaresInfo[i].flightInfo = selectedFltInfo;
				blnFound = true;
				break;
			}
		}
		
		if(!blnFound){
			var ind = jsonChangedFaresInfo.length ;
			jsonChangedFaresInfo[ind] = {flightSegRefNo:"", fareRule:"", seatInfo:"", flightInfo:""};
			jsonChangedFaresInfo[ind].flightSegRefNo = selectedFltSegment;
			jsonChangedFaresInfo[ind].fareType = selectedFareType;
			jsonChangedFaresInfo[ind].fareRule = selectedFareRule;
			jsonChangedFaresInfo[ind].seatInfo = selectedSeatInfo;
			jsonChangedFaresInfo[ind].flightInfo = selectedFltInfo;
		}
		
	};
	
	UI_changeFare.confirmFareChange = function() {
		
		if(!UI_changeFare.validateBCAllocations()){			
			return false;
		} else if(jsonChangedFaresInfo.length == 0){
			alert("Same fare selected.Please select a different fare.");
			return false;
		}

		if(UI_changeFare.validateChangeFareCombination());
		else {
			return false;
		}
		UI_commonSystem.showProgress();
		var data = UI_changeFare.createSearchParams('changeFareQuoteParams');
		var ondList = null;
		
		if(opener.UI_tabSearchFlights.isRequote()){		
			//data['changeFareQuoteParams.ondListString'] = $.toJSON(opener.UI_requote.getONDSearchDTOList());
			ondList = opener.UI_requote.getONDSearchDTOList();
			data['pnr'] = opener.DATA_ResPro.reservationInfo.PNR;	
			data['pnr'] = opener.DATA_ResPro.reservationInfo.PNR;
			data['changeFareQuoteParams.ticketValidTillStr'] = opener.UI_requote.getTicketValidTill();
			data['changeFareQuoteParams.lastFareQuoteDateStr'] = opener.UI_requote.getLastFareQuoteDate();
			data['changeFareQuoteParams.ondQuoteFlexiStr'] = opener.UI_requote.getFlexiQuoteForRequote();
			data['requote'] = true;
		} else if(opener.UI_tabSearchFlights.isMulticity()){
			//data['changeFareQuoteParams.ondListString'] = $.toJSON(opener.UI_Multicity.getONDSearchDTOList());
			ondList = opener.UI_Multicity.getONDSearchDTOList();
			data['changeFareQuoteParams.bookingType'] = opener.UI_Multicity.ondBookType();
			data['changeFareQuoteParams.departureDate'] = null;
			data['changeFareQuoteParams.fromAirport'] = null;
			data['changeFareQuoteParams.toAirport'] = null;
			data['changeFareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(opener.UI_Multicity.ondWiseFlexiSelected());			
			data['multicitySearch'] = true;	
		} else {
			//data['changeFareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(UI_changeFare.overrideFlexiSelection());
			data['changeFareQuoteParams.ondQuoteFlexiStr'] = $.toJSON(window.opener.UI_tabSearchFlights.ondWiseFlexiSelected);
			data['changeFareQuoteParams.ondWiseCitySearchStr'] = $.toJSON(window.opener.UI_tabSearchFlights.ondWiseCitySearch);	
		}
		UI_changeFare.overridePartialLogicallCCRebuild();
//		UI_changeFare.overrideChangedCabinClass();
		
		data = $.airutil.dom.concatObjects(data, window.opener.UI_tabSearchFlights.inOutFlightRPHList());
		data['selectedFlightSegments'] = $.toJSON(flightSegmentList);
		data['changedFaresInfo'] = $.toJSON(jsonChangedFaresInfo);
		data['carrierWiseOnd'] = opener.ondMap;
		data['carrierWiseSeg'] = UI_changeFare.carrierWiseSegMap();
		
		UI_changeFare.validateBundledServiceSelection(jsonChangedFaresInfo, ondList);
		data['changeFareQuoteParams.preferredBundledFares'] = $.toJSON(opener.UI_tabSearchFlights.ondWiseBundleFareSelected);
		data['changeFareQuoteParams.preferredBookingCodes'] = $.toJSON(opener.UI_tabSearchFlights.ondWiseBookingClassSelected);
		
		if(ondList != null){
			data['changeFareQuoteParams.ondListString'] = $.toJSON(ondList);
		}
		
		UI_changeFare.createSubmitData({data:data});
		if(opener.UI_tabSearchFlights.isAllFareChange){
			data['showAllBC'] = true;
		} else {
			data['showAllBC'] = false;
		}
		data['existingFareQuotaType'] = opener.UI_tabSearchFlights.selectedFareType;
		data['mode'] = "CHANGEFARES";
		data['allowOverrideVisibleFare'] = opener.DATA_ResPro.initialParams.allowOverrideVisibleFare;
		data['allowOverrideAnyFare'] = opener.DATA_ResPro.initialParams.allowOverrideAnyFare;
		data['allowOverrideAgentFareOnly'] = opener.DATA_ResPro.initialParams.allowOverrideAgentFareOnly;
		data['allFaresInfoStr'] =  $.toJSON(UI_changeFare.allFaresMap);		
		
		
		$("#frmChgFares").ajaxSubmit({ dataType: 'json', data:  data, success:UI_changeFare.returnCallChangeFareQuote,url:"changeFare.action",
			error:UI_commonSystem.setErrorStatus});
		return false;
	};
	
	//Check whether selected bundled fare for a particular OND is valid after new fare selection
	UI_changeFare.validateBundledServiceSelection = function(jsonChangedFaresInfo, ondList){
		var selectedFareOnFareQuote = opener.UI_tabSearchFlights.availableFare;
		if(selectedFareOnFareQuote != null && selectedFareOnFareQuote.fareRulesInfo != null
				&& selectedFareOnFareQuote.fareRulesInfo.fareRules != null){
			var fareQuoteFareRules = selectedFareOnFareQuote.fareRulesInfo.fareRules;
			for(var i = 0 ; i < jsonChangedFaresInfo.length; i++){
				var changedFareRule = jsonChangedFaresInfo[i].fareRule;
				var changedFltInfo = jsonChangedFaresInfo[i].flightInfo
				
				for ( var j = 0; j < fareQuoteFareRules.length; j++) {
					var fqFareRule = fareQuoteFareRules[j];
					
					if(fqFareRule.ondSequence == changedFltInfo.ondSequence
							&& changedFareRule.bookingClassCode != fqFareRule.bookingClassCode){
						opener.UI_tabSearchFlights.ondWiseBundleFareSelected[changedFltInfo.ondSequence] = null;
						opener.UI_tabSearchFlights.ondWiseBookingClassSelected[changedFltInfo.ondSequence] = null;
						
						if(ondList != null && ondList.length > changedFltInfo.ondSequence){
							ondList[changedFltInfo.ondSequence].bundledServicePeriodId = null;
						}
					} else if(ondList != null && ondList.length > changedFltInfo.ondSequence){
						ondList[changedFltInfo.ondSequence].bundledServicePeriodId = opener.UI_tabSearchFlights.ondWiseBundleFareSelected[changedFltInfo.ondSequence];
					}
				}				
			}
		}
	}
	
	UI_changeFare.carrierWiseSegMap = function() {
		var segMapStr = '[';
		if(jsonFltDetails != null){
			for(var jind=0; jind<jsonFltDetails.length; jind++){
				segMapStr += '{"' + jsonFltDetails[jind].airLine + '":"' + jsonFltDetails[jind].orignNDest + '"}';
				if(jind != (jsonFltDetails.length-1)){
					segMapStr += ',';
				}
			}
		}
		
		return segMapStr + ']';
	};
	
	UI_changeFare.updateCabinCodes = function(){
		var chgFareInfo = [];
		for(var i = 0 ; i < jsonChangedFaresInfo.length; i++){
			var fareInfo = jsonChangedFaresInfo[i];
			chgFareInfo[chgFareInfo.length]={
					fltRefNo: fareInfo.flightSegRefNo,
					ccCode: fareInfo.seatInfo.ccCode
			}
			
		}
		if(chgFareInfo.length  > 0){
			if(opener.UI_tabSearchFlights.isRequote()){
				opener.UI_requote.restructureOndsChgFare(chgFareInfo)
			}
			opener.UI_tabSearchFlights.updateFlightCabinFromChangeFare(chgFareInfo)
		}
	}
	
	UI_changeFare.returnCallChangeFareQuote = function(response){
		// reset wait listed segment availability
		window.opener.UI_tabSearchFlights.ignoreWaitListing = true;
		response["isChangeFare"] = true;
		UI_changeFare.updateCabinCodes();
		window.opener.UI_tabSearchFlights.fareDiscount = {percentage:null, notes:null, code:null};
		window.opener.UI_tabSearchFlights.returnCallShowFareQuoteInfo(response);
		window.opener.UI_tabSearchFlights.buildFareQuoteDetails();
		UI_commonSystem.hideProgress();	
		UI_changeFare.closeOnClick();
		return false;
	};

	UI_changeFare.setSelectedFareType = function(newFareTypeForSeg, ondSequence, subJourneyGroup) {
		prevFareTypeOfSelectedOndSequence = ondWiseFareType[subJourneyGroup][ondSequence];
		if(newFareTypeForSeg == FARE_TYPE_RETURN_FARE){
			UI_changeFare.resetOndFareType(FARE_TYPE_RETURN_FARE,ondSequence, subJourneyGroup);
		} else {
			ondWiseFareType[subJourneyGroup][ondSequence] = newFareTypeForSeg;
		}
	};
	
	UI_changeFare.validateChangeFareCombination = function(){
		if(UI_changeFare.isHalfReturnWithPerFlightOrOndFareExists()) {			
			var invalidFlg = false;
			$.each(ondWiseFareType, function(jkey, jvalue){
				$.each(jvalue, function(ondSequence, fareType){
					if((fareType == FARE_TYPE_PER_FLIGHT_FARE || fareType == FARE_TYPE_OND_FARE) 
							&& ondWiseSegmentCount[jkey][ondSequence] > 1){
						for(var jind=0; jind<jsonFltDetails.length; jind++){
							if(jsonFltDetails[jind].ondSequence == ondSequence && jsonFltDetails[jind].bcAllocated) {
								invalidFlg = true;
								ondWiseFareType[jkey][ondSequence] = FARE_TYPE_HALF_RETURN_FARE;
								break;
							}
						}
					}
				});
			});
			
			if(invalidFlg) {
				alert("Selected fare type combination not valid");
				return false;
			}
		}
			
		return true;
	}
	
	UI_changeFare.validateBCAllocations = function() {
		for(var jind=0; jind<jsonFltDetails.length; jind++){
			if(!jsonFltDetails[jind].bcAllocated) {
				//opener.showERRMessage("Please Re-Calculate the fares. There are segments without a fare.");
				alert("Please re-calculate the fares. Segment "+ jsonFltDetails[jind].orignNDest +" doesn't have a fare.");
				return false;
			}
		}
		return true;
	};
	
	UI_changeFare.getSegmentFare = function(segmentCode, segmentList, paxType){
		var fare = 0;
		var isFareFound = false;
		if(segmentList != null) {
			for(var i=0; i< segmentList.length; i++){
				if(segmentCode == segmentList[i].segmentCode){
					var paxWiseFareList = segmentList[i].paxWise;
					for(var p=0; p < paxWiseFareList.length; p++){
						var paxFareTO = paxWiseFareList[p];
						if(paxFareTO.paxType == paxType){
							fare = paxFareTO.fare;
							isFareFound = true;
							break;
						}
					}
				}
			}
		}
		return {'fare' : fare, 'isFareFound' : isFareFound};
	};

UI_changeFare.setDefaultAmountsForReturnFareType = function() {
	var aFareAmt = 0;
	var cFareAmt = 0;
	var iFareAmt = 0;
	
	var OndFareChecked = {};
		
	$.each(journeyWiseOnd, function(key, list) {			
		if(UI_changeFare.isAllOndFare(FARE_TYPE_RETURN_FARE,key)){
			for (var jind = 0; jind < jsonFltDetails.length; jind++) {
				if(!OndFareChecked[jsonFltDetails[jind].ondSequence] 
				&& UI_changeFare.isSameJourneyGroup(key,jsonFltDetails[jind].ondSequence)){
					aFareAmt = aFareAmt + Number(jsonFltDetails[jind].adultFare);
					cFareAmt = cFareAmt +  Number(jsonFltDetails[jind].childFare);
					iFareAmt = iFareAmt +  Number(jsonFltDetails[jind].infantFare);
					OndFareChecked[jsonFltDetails[jind].ondSequence] = true;
				}
			}	

			for(var jind=0; jind<jsonFltDetails.length; jind++){
				if(UI_changeFare.isSameJourneyGroup(key,jsonFltDetails[jind].ondSequence)){
					jsonFltDetails[jind].adultFare = aFareAmt;
					jsonFltDetails[jind].childFare = cFareAmt;
					jsonFltDetails[jind].infantFare = iFareAmt;
				}			

			}

			aFareAmt = 0;
			cFareAmt = 0;
			iFareAmt = 0;
		}
	});
};

UI_changeFare.updateFaresOnSelection = function(selectedFltInfo, selectedFareType, selectedFareRule){
	var adultApplicable = (opener.UI_tabSearchFlights.objResSrchParams.adultCount > 0)?true:false;
	var childApplicable	= (opener.UI_tabSearchFlights.objResSrchParams.childCount > 0)?true:false;
	var infantApplicable = (opener.UI_tabSearchFlights.objResSrchParams.infantCount > 0)?true:false;
	
	for(var jind=0; jind<jsonFltDetails.length; jind++){
		var currentOndSequence = jsonFltDetails[jind].ondSequence;
		
		var isHalfReturnFareExists = UI_changeFare.isFareExistsInJourneyGroup(FARE_TYPE_HALF_RETURN_FARE,currentOndSequence,jsonFltDetails[jind].subJourneyGroup);
		var isReturnFareExists = UI_changeFare.isFareExistsInJourneyGroup(FARE_TYPE_RETURN_FARE,currentOndSequence,jsonFltDetails[jind].subJourneyGroup);		
		
		if(ondWiseFareQuoteFlag[jsonFltDetails[jind].subJourneyGroup][currentOndSequence]){
			if(selectedFareType == FARE_TYPE_RETURN_FARE){
					jsonFltDetails[jind].adultFare = (adultApplicable)?selectedFareRule.adultFareAmount:0;
					jsonFltDetails[jind].childFare = (childApplicable)?selectedFareRule.childFareAmount:0;
					jsonFltDetails[jind].infantFare = (infantApplicable)?selectedFareRule.infantFareAmount:0;
					jsonFltDetails[jind].bcAllocated = true;
					jsonFltDetails[jind].fareType = FARE_TYPE_RETURN_FARE;
			} else if(selectedFareType == FARE_TYPE_OND_FARE){
				if(selectedFltInfo.ondSequence == currentOndSequence){
					jsonFltDetails[jind].adultFare = (adultApplicable)?selectedFareRule.adultFareAmount:0;
					jsonFltDetails[jind].childFare = (childApplicable)?selectedFareRule.childFareAmount:0;
					jsonFltDetails[jind].infantFare = (infantApplicable)?selectedFareRule.infantFareAmount:0;
					jsonFltDetails[jind].bcAllocated = true;
					jsonFltDetails[jind].fareType = FARE_TYPE_OND_FARE;
				}
				if(isReturnFareExists || isHalfReturnFareExists 
						|| UI_changeFare.isConnectionFlightWithRetOrHalfRetFare(currentOndSequence,jsonFltDetails[jind].fareType)){
					if(selectedFltInfo.ondSequence != currentOndSequence){
						jsonFltDetails[jind].adultFare = 0;
						jsonFltDetails[jind].childFare = 0;
						jsonFltDetails[jind].infantFare = 0;
						jsonFltDetails[jind].bcAllocated = false;
						jsonFltDetails[jind].logicalCC = '';
						jsonFltDetails[jind].fareType = FARE_TYPE_NO_FARE;
						ondWiseFareType[selectedFltInfo.subJourneyGroup][currentOndSequence] = FARE_TYPE_NO_FARE;
					}
				}
			} else if(selectedFareType == FARE_TYPE_HALF_RETURN_FARE){
				if(selectedFltInfo.ondSequence == currentOndSequence){
					jsonFltDetails[jind].adultFare = (adultApplicable)?selectedFareRule.adultFareAmount:0;
					jsonFltDetails[jind].childFare = (childApplicable)?selectedFareRule.childFareAmount:0;
					jsonFltDetails[jind].infantFare = (infantApplicable)?selectedFareRule.infantFareAmount:0;
					jsonFltDetails[jind].bcAllocated = true;
					jsonFltDetails[jind].fareType = FARE_TYPE_HALF_RETURN_FARE;
				}
				
				if(selectedFltInfo.ondSequence != currentOndSequence 
						&& ondWiseFareType[selectedFltInfo.subJourneyGroup][currentOndSequence] != FARE_TYPE_HALF_RETURN_FARE){
					
					var openRetFareUpdated = false;				
					if(UI_changeFare.allowHalfRetFareForOpenReturn 
							&& UI_changeFare.isOpenReturnSegment(currentOndSequence) 
							&& ondWiseFareType[selectedFltInfo.subJourneyGroup][currentOndSequence] == FARE_TYPE_RETURN_FARE){
						
						var openRetFareRule = UI_changeFare.getOpenReturnFareRule(jsonFltDetails[jind]);
						
						if(openRetFareRule!=null){
							jsonFltDetails[jind].adultFare = (adultApplicable)?openRetFareRule.adultFareAmount:0;
							jsonFltDetails[jind].childFare = (childApplicable)?openRetFareRule.childFareAmount:0;
							jsonFltDetails[jind].infantFare = (infantApplicable)?openRetFareRule.infantFareAmount:0;
							jsonFltDetails[jind].bcAllocated = true;
							openRetFareUpdated = true;
							jsonFltDetails[jind].fareType = FARE_TYPE_HALF_RETURN_FARE;
						}					
	
					}				
					
					if(!openRetFareUpdated){
						jsonFltDetails[jind].adultFare = 0;
						jsonFltDetails[jind].childFare = 0;
						jsonFltDetails[jind].infantFare = 0;
						jsonFltDetails[jind].bcAllocated = false;
						jsonFltDetails[jind].logicalCC = '';
						ondWiseFareType[selectedFltInfo.subJourneyGroup][currentOndSequence] = FARE_TYPE_NO_FARE;
						jsonFltDetails[jind].fareType = FARE_TYPE_NO_FARE;
					}
				}
			} else if(selectedFareType == FARE_TYPE_PER_FLIGHT_FARE){
				if(selectedFltInfo.fltSegRefNo == jsonFltDetails[jind].fltSegRefNo){
					jsonFltDetails[jind].adultFare = (adultApplicable)?selectedFareRule.adultFareAmount:0;
					jsonFltDetails[jind].childFare = (childApplicable)?selectedFareRule.childFareAmount:0;
					jsonFltDetails[jind].infantFare = (infantApplicable)?selectedFareRule.infantFareAmount:0;
					jsonFltDetails[jind].bcAllocated = true;
					jsonFltDetails[jind].fareType = FARE_TYPE_PER_FLIGHT_FARE;
				}
				if((prevFareTypeOfSelectedOndSequence == FARE_TYPE_OND_FARE && selectedFltInfo.ondSequence == currentOndSequence) 
						|| isReturnFareExists || (!UI_changeFare.modifyBooking && isHalfReturnFareExists) 
						|| UI_changeFare.isConnectionFlightWithRetOrHalfRetFare(currentOndSequence,jsonFltDetails[jind].fareType)){
					if(selectedFltInfo.fltSegRefNo != jsonFltDetails[jind].fltSegRefNo){
						jsonFltDetails[jind].adultFare = 0;
						jsonFltDetails[jind].childFare = 0;
						jsonFltDetails[jind].infantFare = 0;
						jsonFltDetails[jind].bcAllocated = false;
						jsonFltDetails[jind].logicalCC = '';
						ondWiseFareType[selectedFltInfo.subJourneyGroup][currentOndSequence] = FARE_TYPE_NO_FARE;
						jsonFltDetails[jind].fareType = FARE_TYPE_NO_FARE;
					}
				}
			}
			
		}

	}
};


/* Display tooltip - Agent Information
 * 
 */
UI_changeFare.displayAllAgentsToolTip = function() {	
	  var sHtm  ="";
	  if (this.id != "") {
			var arrID = this.id.split("_");
			var rowid = arrID[1];
			var subrowid = arrID[2];
			var strData = "";	
			//if (strData != ""){ strData += "<hr>";}
			strData += "<font>" + jsonFares[rowid].fareDeatils[subrowid].agent + "</font>";

		}
	 
	  var arrAgents = strData.split("<br>");
	  var agentStr = "";
		for(var ag=0; ag < arrAgents.length; ag++){
			agentStr = agentStr + arrAgents[ag]+", ";
		}
		agentStr = agentStr.substring(0,agentStr.length-2);
		sHtm += agentStr;
	  
	  $("#toolTipBody").html(sHtm);	 
	  var pos = $("#" + this.id).offset();
	  var width = $("#" +this.id).width();
    $("#toolTipAgents").css({
        left: (pos.left) - 20 + 'px',
        top: pos.top + 20 + 'px'
       
    });      
   $("#toolTipAgents").show(); 	  
};
/* Hide Alert data
 * 
 */
UI_changeFare.hideToolTip = function() {
	  $("#toolTipAgents").hide();
};

UI_changeFare.getAssociatedFareCountForBC = function(fare, faresList){
	var rowSpan = 0;
	for ( var fr = 0; fr < faresList.length; fr++) {
		var tempFare = faresList[fr];
		if(fare.bookingClassCode == tempFare.bookingClassCode){
			rowSpan++;
		}
	}
	if(rowSpan == 0)
		rowSpan = 1;
	return rowSpan;
};

UI_changeFare.createSearchParams = function(varName){
	var pData = {};
	pData[varName+".fromAirport"]	= opener.UI_tabSearchFlights.objResSrchParams.fromAirport;
	pData[varName+".toAirport"]		= opener.UI_tabSearchFlights.objResSrchParams.toAirport;
	pData[varName+".departureDate"]	= opener.UI_tabSearchFlights.objResSrchParams.departureDate;
	pData[varName+".returnDate"]	= (opener.UI_tabSearchFlights.objResSrchParams.returnDate==null?'':opener.UI_tabSearchFlights.objResSrchParams.returnDate);
	pData[varName+".validity"]		= opener.UI_tabSearchFlights.objResSrchParams.validity;
	pData[varName+".adultCount"]	= opener.UI_tabSearchFlights.objResSrchParams.adultCount;
	pData[varName+".childCount"]	= opener.UI_tabSearchFlights.objResSrchParams.childCount;
	pData[varName+".infantCount"]	= opener.UI_tabSearchFlights.objResSrchParams.infantCount;
	if(!opener.UI_tabSearchFlights.isAllFareChange){
		pData[varName+".classOfService"]= opener.UI_tabSearchFlights.objResSrchParams.classOfService;		
	}
	pData[varName+".selectedCurrency"]= opener.UI_tabSearchFlights.objResSrchParams.selectedCurrency;
	pData[varName+".bookingType"]	= opener.UI_tabSearchFlights.objResSrchParams.bookingType;
	pData[varName+".paxType"]		= opener.UI_tabSearchFlights.objResSrchParams.paxType;
	pData[varName+".fareType"]		= opener.UI_tabSearchFlights.objResSrchParams.fareType;
	pData[varName+".openReturn"]	= opener.UI_tabSearchFlights.objResSrchParams.openReturn;
	pData[varName+".searchSystem"]	= opener.UI_tabSearchFlights.objResSrchParams.searchSystem;
	pData[varName+".quoteFlexi"]	= opener.UI_tabSearchFlights.objResSrchParams.quoteFlexi;
	pData[varName+".travelAgentCode"]	= opener.UI_tabSearchFlights.objResSrchParams.travelAgentCode == null?'' : opener.UI_tabSearchFlights.objResSrchParams.travelAgentCode;
	
	var isReturnValid = UI_changeFare.isReturnValid();
	
	pData[varName+".returnFlag"] =  pData[varName+".returnDate"] != ''?isReturnValid:false;
	pData[varName+".halfReturnFareQuote"] = pData[varName+".returnDate"] != ''?isReturnValid:false;
	pData[varName+".stayOverTimeInMillis"] = opener.UI_tabSearchFlights.objResSrchParams.stayOverTimeInMillis;
	pData[varName+'.ondQuoteFlexiStr'] = $.toJSON(opener.UI_tabSearchFlights.ondWiseFlexiQuote);
	pData[varName+".fareQuoteLogicalCCSelection"] = opener.UI_tabSearchFlights.getJsonLogicalCabinClassSelection();	
	pData[varName+".fareQuoteOndSegLogicalCCSelection"] = opener.UI_tabSearchFlights.getondSegWiselogicalCabinClassSelection;	
	pData[varName+'.promoCode'] = opener.UI_tabSearchFlights.objResSrchParams.promoCode;
	pData[varName+'.bankIdentificationNo'] = opener.UI_tabSearchFlights.objResSrchParams.bankIdentificationNo;
	
	//Set ond wise flexi selection excluding auto flexi selection by bundled services
	var ondFlexiSelection = $.airutil.dom.cloneObject(opener.UI_tabSearchFlights.ondWiseFlexiSelected);
	var ondBundledSelection = opener.UI_tabSearchFlights.ondWiseBundleFareSelected;
	if(ondBundledSelection != undefined && ondBundledSelection != null){
		$.each(ondFlexiSelection, function(ondSeq, flexiSelected){
			if(ondBundledSelection[ondSeq] != undefined && ondBundledSelection[ondSeq] != null
					&& ondBundledSelection[ondSeq] > 0){
				ondFlexiSelection[ondSeq] = false;
			}
		});
	}
	
	pData[varName+'.ondSelectedFlexiStr'] = $.toJSON(ondFlexiSelection);
	
	if(opener.DATA_ResPro.initialParams.allowExcludeCharges){
		var excludedCharges = opener.UI_tabSearchFlights.objResSrchParams.excludeCharge;
		if((typeof(excludedCharges) != 'undefined') && excludedCharges != null){
			for ( var i = 0; i < excludedCharges.length; i++) {
				pData[varName+'.excludeCharge[' + i + ']'] = excludedCharges[i];
			}
		}
	}
	
	return pData;
};

UI_changeFare.isReturnValid = function(){
	var carrierList = UI_changeFare.createCarrierList("arr");
	if(carrierList.length > 1){
		return false;
	} else {
		return true;
	}
};

UI_changeFare.createCarrierList = function(type){
	if(type == "arr"){
		var carrierList = new Array();
		if(jsonFltDetails != null){
			for(var i=0;i<jsonFltDetails.length;i++){
				if(!UI_changeFare.isCarrierExists(type, carrierList, jsonFltDetails[i].airLine)){
					carrierList.push(jsonFltDetails[i].airLine);
				}
			}
		}
		
		return carrierList;
	} else if(type == "str"){
		var carrierList = '';
		if(jsonFltDetails != null){
			for(var i=0;i<jsonFltDetails.length;i++){
				if(!UI_changeFare.isCarrierExists(type, carrierList, jsonFltDetails[i].airLine)){
					if(i == (jsonFltDetails.length - 1)){
						carrierList += jsonFltDetails[i].airLine;
					} else {
						carrierList += jsonFltDetails[i].airLine + ",";
					}
				}
			}
		}
		
		return carrierList;
	}
	
	return null;
};

UI_changeFare.isCarrierExists = function(type, arr, elem){
	if(type == "arr"){		
		for(var i=0;i<arr.length;i++){
			if(arr[i] == elem){
				return true;		 
			}
		}
		
		return false;
	} else if(type == "str") {
		var spltArr = arr.split(",");
		for(var i=0;i<spltArr.length;i++){
			if(spltArr[i] == elem){
				return true;		 
			}
		}	
		return false;
	}
};

UI_changeFare.createSubmitData =  function(params) {
	if (opener.UI_commonSystem.strPGMode != "" ){
		var opType = opener.UI_tabSearchFlights.getOperationType();
		params.data['resSegments']= $.toJSON(opener.DATA_ResPro.resSegments);
		if (opType == "ADS_BLS") {
			params.data['groupPNR'] = opener.DATA_ResPro.reservationInfo.groupPNR;
		}
		if (opType == "MDS_RES") {
			params.data['modifyBooking'] = true;
			if(window.opener.UI_tabSearchFlights.isRequote()){				
				params.data['pnr'] = opener.DATA_ResPro.reservationInfo.PNR;
				params.data['groupPNR'] = opener.DATA_ResPro.reservationInfo.groupPNR;
				params.data['modifySegment'] = opener.DATA_ResPro.reservationInfo.modifyingSegments;
				params.data['selCurrency'] = opener.DATA_ResPro.reservationInfo.selectedCurrency;
			} else {
				params.data['pnr'] = opener.DATA_ResPro.modifySegments.PNR;
				params.data['groupPNR'] = opener.DATA_ResPro.modifySegments.groupPNR;
				params.data['modifySegment'] = opener.DATA_ResPro.modifySegments.modifyingSegments;
				params.data['selCurrency'] = opener.DATA_ResPro.modifySegments.selectedCurrency;
			}			
		}
	}
};

//validate whether hal-return fare with per flight or ond fare exisit within a sub-journey
UI_changeFare.isHalfReturnWithPerFlightOrOndFareExists = function(){
	var isHalfRetExists = false;
	var isPerFlightExists = false;
	var isOndExists = false;
	var isMixFareExist = false;

	$.each(ondWiseFareType, function(jkey, jvalue){
		isHalfRetExists = false;
		isPerFlightExists = false;
		isOndExists = false;
		$.each(jvalue, function(key, value){
			if(value == FARE_TYPE_HALF_RETURN_FARE){
				isHalfRetExists = true;
			} else if(value == FARE_TYPE_PER_FLIGHT_FARE){
				isPerFlightExists = true;
			} else if(value == FARE_TYPE_OND_FARE){
				isOndExists = true;
			}

		});

		if(!isMixFareExist){
			isMixFareExist = isHalfRetExists && (isPerFlightExists || (UI_changeFare.modifyBooking && isOndExists));	
		}

		if(isMixFareExist){
			return;
		}

	});	

	return isMixFareExist;	
}


UI_changeFare.resetOndFareType = function(fareType,ondSequence , subJourneyGroup){
	var journeyGroup = ondWiseJourneyGroup[ondSequence];	
	$.each(ondWiseFareType, function(jkey, jvalue){
		if(subJourneyGroup == jkey){
			$.each(jvalue, function(key, value){
				ondWiseFareType[jkey][key] = fareType;	
			});
		}		
	});
	
}

//this method checks whether all OnDs are with the given fareType
UI_changeFare.isAllOndFare = function(fareType,journeyGroup){
	var isWithFare = true;
	$.each(ondWiseFareType, function(jkey, jvalue){
		if(journeyGroup == jkey){
			$.each(jvalue, function(key, value){
				if(value != fareType){
					isWithFare = false;
					return;
				}
			});
			return;
		}
	});
	
	return isWithFare;
}


UI_changeFare.isFareExistsInJourneyGroup = function(fareType,ondSequence,journeyGroup){
	var isWithFare = false;	
	$.each(ondWiseFareType, function(jkey, jvalue){		
		if(jkey == journeyGroup){
			$.each(jvalue, function(key, value){	
				if(value == fareType){
					isWithFare = true;
					return;
				}
			});
		}
	});
	
	return isWithFare;
}

UI_changeFare.isSameJourneyGroup = function(journeyGroup,ondSequence){	
	var samegroup = false;	
	var ondlist = journeyWiseOnd[journeyGroup];		
	for(var i=0;i<ondlist.length;i++){
		if(!samegroup && parseInt(ondSequence)==ondlist[i]){
			samegroup = true;
		}
	}
	return samegroup;
}


UI_changeFare.isSameJourneyGroup2 = function(journeyGroup,ondSequence){	
	var samegroup = false;	
	var ondlist = journeyWiseOnd[journeyGroup];		
	for(var i=0;i<ondlist.length;i++){
		if(!samegroup && parseInt(ondSequence)==ondlist[i]){
			samegroup = true;
		}
	}
	return samegroup;
}

UI_changeFare.setAllFareInfo = function(selectedSegmentRef, allFares, allFaresInfoMap) {
	UI_changeFare.allFaresMap[selectedSegmentRef] = allFares;
	$.each(allFaresInfoMap, function(key, value) {
		if (UI_changeFare.allFaresMap[key] == null) {
			UI_changeFare.allFaresMap[key] = value;
		}
	});
}

UI_changeFare.isOpenReturnSegment = function(ondSeq){	
	var isOpenReturn = false;
	$.each(flightSegmentList, function(key, value){		
		if(value.ondSequence ==ondSeq ){
			isOpenReturn = value.openReturnSegment;
		}			
	});	
	return isOpenReturn;
}

UI_changeFare.getHalfReturnFare = function(ondSeg,fltSegRefNo){
	
	var fareId=null;
	if(jsonChangedFaresInfo!=null){		
		for(var i=0;i<jsonChangedFaresInfo.length;i++){			
			if(jsonChangedFaresInfo[i].flightSegRefNo ==fltSegRefNo ){
				fareId = jsonChangedFaresInfo[i].fareRule.fareId
				break;
			}
		}		
	}
	
	if(fareId==null && ondWiseFareId!=null){
		fareId = ondWiseFareId[ondSeg];
	}
	return fareId;	
}

UI_changeFare.addHalfReturnFareForOpenReturn = function(jsonFltDetails) {
	var selectedFareRule = null;
	var selectedSeatInfo = null;
	for (var i = 0; i < UI_changeFare.allFaresInfo.length; i++){
		
		var fareType = UI_changeFare.allFaresInfo[i].fareType;
		
		if(FARE_TYPE_HALF_RETURN_FARE == fareType) {
			
				var fareRuleDTOs = UI_changeFare.allFaresInfo[i].fareRuleInfo;
				var seatDTOs = UI_changeFare.allFaresInfo[i].invAllocationInfo;				
				
				var fareId = UI_changeFare.getHalfReturnFare(jsonFltDetails.ondSequence,jsonFltDetails.fltSegRefNo);
				var selIndex = -1;
				
				for(var j=0;j<fareRuleDTOs.length;j++){
					if(fareRuleDTOs[j].fareId==fareId){
						selIndex = j;
						break;
					}
				}				
				
				if(selIndex!=-1){
					selectedFareRule = fareRuleDTOs[selIndex];
					selectedSeatInfo = seatDTOs[selIndex];
					selectedSeatInfo.openRTSegment = true;
				}else{
					//throw exception half return is not enabled for the I/B fare
					selectedFareRule = null;
					selectedSeatInfo = null;
					alert("Selected fare type combination not valid");
					return false;					
				}
				
		}
	}	
	
	if(selectedFareRule!=null && selectedSeatInfo!=null){
		UI_changeFare.addToChangeFareDetails(jsonFltDetails.fltSegRefNo, selectedFareRule, selectedSeatInfo, jsonFltDetails, FARE_TYPE_HALF_RETURN_FARE);	
	}
};

//following function returns fare rule info if a half return fare exist
UI_changeFare.getOpenReturnFareRule = function(jsonFltDetails) {
	var selectedFareRule = null;
	for (var i = 0; i < UI_changeFare.allFaresInfo.length; i++){		
		var fareType = UI_changeFare.allFaresInfo[i].fareType;		
		if(FARE_TYPE_HALF_RETURN_FARE == fareType) {			
				var fareRuleDTOs = UI_changeFare.allFaresInfo[i].fareRuleInfo;			
				
				var fareId = UI_changeFare.getHalfReturnFare(jsonFltDetails.ondSequence,jsonFltDetails.fltSegRefNo);
				var selIndex = -1;
				
				for(var j=0;j<fareRuleDTOs.length;j++){
					if(fareRuleDTOs[j].fareId==fareId){
						selIndex = j;
						break;
					}
				}				
				
				if(selIndex==-1){		
					selectedFareRule = null;
					break;
				}
				
				selectedFareRule = fareRuleDTOs[selIndex];				
		}
	}	
	return selectedFareRule	
};


UI_changeFare.setFareQuotedOndSequence = function(flightInfo){	

	if(flightInfo != null && flightInfo != undefined){			
		var fareQuotedJourneyId = UI_changeFare.getFareQuotedJourney(flightInfo);
		$.each(flightInfo, function(index, obj){
			
			if(ondWiseFareQuoteFlag[obj.subJourneyGroup] == undefined || ondWiseFareQuoteFlag[obj.subJourneyGroup] == null){
				ondWiseFareQuoteFlag[obj.subJourneyGroup] = {};
			}
			
			if(!UI_changeFare.isOpenReturnSegment(obj.ondSequence) 
					&& UI_changeFare.fareQuotedJourneyList!=null 
					&& UI_changeFare.fareQuotedJourneyList!=undefined 
					&& UI_changeFare.fareQuotedJourneyList.length > 0){
				var flg = UI_changeFare.isFareQuotedOnd(obj.flightRefNumber);	
				
				if (UI_changeFare.isFareQuotedOnd(obj.flightRefNumber)) {
					ondWiseFareQuoteFlag[obj.subJourneyGroup][obj.ondSequence] = true;
				} else if (UI_changeFare.splitOperationForBusInvoked 
						|| (!UI_changeFare.isSameJourneyGroup(fareQuotedJourneyId,obj.ondSequence))) {
					ondWiseFareQuoteFlag[obj.subJourneyGroup][obj.ondSequence] = false;
				}
			} else {				
				ondWiseFareQuoteFlag[obj.subJourneyGroup][obj.ondSequence] = true;
			}
			
		});
	}		
};

//returns changed fare requested flight sub-journey sequence  
UI_changeFare.getFareQuotedJourney = function (flightInfo){
	var journeyId = -1;
	if(flightInfo != undefined && flightInfo != null 
			&& UI_changeFare.fareQuotedJourneyList!=undefined 
			&& UI_changeFare.fareQuotedJourneyList!=null 
			&& UI_changeFare.fareQuotedJourneyList.length > 0){
		$.each(UI_changeFare.fareQuotedJourneyList, function(key, val){
			$.each(flightInfo, function(index, obj){
				if(obj.flightRefNumber.indexOf(val)== 0){
					journeyId = obj.subJourneyGroup;
					if(journeyId == undefined){
						journeyId =ondWiseJourneyGroup[index];
					}
					return;
				}
			});
		});
	}
	return journeyId;
};

UI_changeFare.isFareQuotedOnd= function(flightRefNumber){	
	var flg = false;	
	$.each(UI_changeFare.fareQuotedJourneyList, function(index, val){
		if(val==UI_changeFare.trimFlighttRPH(flightRefNumber)){
			flg = true;			
		}
	});
	
	return flg
};

UI_changeFare.isInitialLoad = function(){
	var flg = true;	
	$.each(ondWiseFareQuoteFlag, function(jindex, subjrnyGrp){
		$.each(subjrnyGrp, function(index, val){
			if(flg && val==false){
				flg = false;			
			}
		});
	});
	
	return flg;
};

UI_changeFare.trimFlighttRPH= function(flightRefNumber){	
	if(flightRefNumber!=undefined && flightRefNumber!=null){		
		if(flightRefNumber.indexOf("#")!=-1){
			var fltRefArray = flightRefNumber.split("#");
			if(fltRefArray.length > 1){
				return fltRefArray[0];
			}
		}
	}
	return flightRefNumber;
};

UI_changeFare.setJourneyWiseOnd = function(){	
	if(jsonFltDetails != null && jsonFltDetails != undefined){
		$.each(jsonFltDetails, function(index, obj){
			var ondList = new Array();
			if(journeyWiseOnd[obj.subJourneyGroup]!=null && journeyWiseOnd[obj.subJourneyGroup]!=undefined){
				ondList = journeyWiseOnd[obj.subJourneyGroup];
			}
			ondList.push(obj.ondSequence);
			journeyWiseOnd[obj.subJourneyGroup] = ondList;
			ondWiseJourneyGroup[obj.ondSequence]= obj.subJourneyGroup;
		});		
	}
}

/**
 * this validation is required when updating fares for connection route flights.
 * */
UI_changeFare.isConnectionFlightWithRetOrHalfRetFare = function(currentOndSequence,flightFareType){
	var count=0;
	for(var jind=0; jind<jsonFltDetails.length; jind++){		
		if(currentOndSequence== jsonFltDetails[jind].ondSequence){
			count++;
		}
	}
	
	if((count > 1) && (flightFareType == FARE_TYPE_HALF_RETURN_FARE 
			|| flightFareType == FARE_TYPE_RETURN_FARE)) {
		return true;
	} else {
		return false;
	}
};

	/* ------------------------------------------------ end of File ---------------------------------------------- */