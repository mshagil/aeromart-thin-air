function UI_SchedRept(){}
UI_SchedRept.errorCallback = null;
UI_SchedRept.successCallback = null;
UI_SchedRept.formName = null;
UI_SchedRept.composerName = null;
UI_SchedRept.isCreated = false;

//On load funciton
$(function() {
	UI_SchedRept.checkButtonAppParam ();
});

UI_SchedRept.displayForm = function(params){
	
	if(params != null || params.formName == null || params.composerName == null){
		UI_SchedRept.distroy();
		var strAppend = "<span id='spnSchedule' style='position:absolute;top:300px;left:200px;width:480px;height:200px;'>";
		strAppend +=	"<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td valign='top'>";
		strAppend +=	"<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' ID='Table4'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td width='10' height='20'><img src='../images/form_top_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td colspan='2' height='20' class='FormHeadBackGround'><img src='../images/bullet_small_no_cache.gif'> <font class='FormHeader'>";
		strAppend +=	"Schedule Report<\/font><\/td>";
		strAppend +=	"<td width='11' height='20'><img src='../images/form_top_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td class='FormBackGround'><\/td>";
		strAppend +=	"<td class='FormBackGround' valign='top' style='height:160px;' colspan='2'>";
		strAppend +=	UI_SchedRept.constructContent(params.formName);
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround'></td>";
		strAppend +=	"</tr>";
		strAppend +=	"<tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td class='FormBackGround'><\/td>";
		strAppend +=	"<td class='FormBackGround' align='left'>";
		strAppend +=	"<input name='btnCancel' type='button' class='button' id='btnCancel' value='Cancel' onClick='UI_SchedRept.distroy();'>";
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround' align='right'>";
		//strAppend +=	"<input name='btnAdvanced' type='button' class='ButtonDisabled' style='width:130px;' disabled='true' id='btnAdvanced' value='Advanced Settings'>";
		strAppend +=	"&nbsp;<input name='btnDone' type='button' class='button' id='btnDone' value='Done' onClick='UI_SchedRept.scheduleReport();'>";
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround'></td>";
		strAppend +=	"</tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td height='12'><img src='../images/form_bottom_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td height='12' colspan='2' class='FormBackGround'><img src='../images/spacer_no_cache.gif' width='100%' height='1'><\/td>";
		strAppend +=	"<td height='12'><img src='../images/form_bottom_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<\/table>";
		strAppend +=	"<\/td>";
		strAppend +=	"<\/table>";
		strAppend +=	"<\/span>";
		$("#"+params.divName).append(strAppend);
		UI_SchedRept.errorCallback = params.errorCallback;
		UI_SchedRept.successCallback = params.successCallback;
		UI_SchedRept.formName = params.formName;
		UI_SchedRept.composerName = params.composerName;

		$("#startDateCal").datepicker({ minDate: new Date(), dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		$("#endDateCal").datepicker({ minDate: new Date(), dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		UI_SchedRept.setDateAutoFormatter();
	} else {
		alert("incorrect parameters");
	}
}
UI_SchedRept.setDateAutoFormatter = function (){
	$('#startDateCal').change(function (){
		var strTime = this.value;
		dateChk('startDateCal');	
	});	
	$('#endDateCal').change(function (){
		var strTime = this.value;
		dateChk('endDateCal');	
	});
}


UI_SchedRept.constructContent = function(formName){

	var strAppend = "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";	
	strAppend +=	"	<tr>";
	strAppend +=	"		<td colspan='2' style='height:5px;'><\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td width='100px'>";
	strAppend +=	"			<font>Schedule for :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<select id='selScheduleFor' name='selScheduleFor' style='width:250px;'>";
	strAppend +=	"				<option value='DAILY'>Daily<\/option>";
	strAppend +=	"				<option value='EVERY_WEEK_START'>Start of every week<\/option>";
	strAppend +=	"				<option value='EVERY_WEEK_END'>End of every week<\/option>";
	strAppend +=	"				<option value='EVERY_MONTH_END'>End of the month<\/option>";
	strAppend +=	"				<option value='EVERY_MONTH_MIDDLE_N_END'>Middle and end of the month<\/option>";
	strAppend +=	"			<\/select> <font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td>";
	strAppend +=	"			<font>Report Period :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<select id='selPeriod' name='selPeriod' style='width:150px;'>";
	
	switch(formName){
	
	case 'frmViewSeatInventoryCollections':
		strAppend +=	"				<option value='ADJUSTED_RANGE'>Adjusted range<\/option>";
		strAppend +=	"				<option value='TILL_END_OF_MONTH'>Till end of current month<\/option>";
		strAppend +=	"				<option value='NEXT_MONTH'>Next Month<\/option>";
		strAppend +=	"				<option value='NEXT_2_MONTHS'>Next 2 Months<\/option>";
		strAppend +=	"				<option value='NEXT_3_MONTHS'>Next 3 Months<\/option>";
		strAppend +=	"				<option value='NEXT_6_MONTHS'>Next 6 Months<\/option>";
		break;
	default:		
		strAppend +=	"				<option value='ADJUSTED_RANGE'>Adjusted range<\/option>";
		strAppend +=	"				<option value='LAST_WEEK'>Last week<\/option>";
		strAppend +=	"				<option value='LAST_HALF_MONTH'>Last half month<\/option>";
		strAppend +=	"				<option value='LAST_MONTH'>Last month<\/option>";
		break;
	
	}
	
	strAppend +=	"			<\/select> <font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td valign='top'>";
	strAppend +=	"			<font>Schedule Time :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<select id='selScheduledTime' name='selScheduledTime' style='width:150px;'>";
	strAppend +=	"				<option value='22:00'>10.00 PM<\/option>";
	strAppend +=	"				<option value='23:00'>11.00 PM<\/option>";
	strAppend +=	"				<option value='00:00' selected='true'>00.00 AM<\/option>";
	strAppend +=	"				<option value='1:00'>01.00 AM<\/option>";
	strAppend +=	"				<option value='2:00'>02.00 AM<\/option>";
	strAppend +=	"				<option value='3:00'>03.00 AM<\/option>";
	strAppend +=	"				<option value='4:00'>04.00 AM<\/option>";
	strAppend +=	"				<option value='5:00'>05.00 AM<\/option>";
	strAppend +=	"				<option value='6:00'>06.00 AM<\/option>";
	strAppend +=	"				<option value='7:00'>07.00 AM<\/option>";
	strAppend +=	"				<option value='8:00'>08.00 AM<\/option>";
	strAppend +=	"				<option value='9:00'>09.00 AM<\/option>";
	strAppend +=	"				<option value='10:00'>10.00 AM<\/option>";
	strAppend +=	"				<option value='11:00'>11.00 AM<\/option>";
	strAppend +=	"				<option value='12:00'>12.00 PM<\/option>";
	strAppend +=	"				<option value='13:00'>01.00 PM<\/option>";
	strAppend +=	"				<option value='14:00'>02.00 PM<\/option>";
	strAppend +=	"				<option value='15:00'>03.00 PM<\/option>";
	strAppend +=	"				<option value='16:00'>04.00 PM<\/option>";
	strAppend +=	"				<option value='17:00'>05.00 PM<\/option>";
	strAppend +=	"				<option value='18:00'>06.00 PM<\/option>";
	strAppend +=	"				<option value='19:00'>07.00 PM<\/option>";
	strAppend +=	"				<option value='20:00'>08.00 PM<\/option>";
	strAppend +=	"				<option value='21:00'>09.00 PM<\/option>";
	strAppend +=	"			<\/select> <font class='mandatory'><b>*</b></font>";
	strAppend +=	"				<font>(Time is in Zulu)<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td colspan='2'>";
	strAppend += 	"		<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
	strAppend +=	"		<td width='100px'>";
	strAppend +=	"			<font>Start Date :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td width='100px'>";
	strAppend +=	"			<input type='text' id='startDateCal' name='startDateCal' style='width:78px;'><\/input><font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td width='68px'>";
	strAppend +=	"			<font>End Date :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<input type='text' id='endDateCal' name='endDateCal' style='width:78px;'><\/input> <font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<\/table>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";	
	strAppend +=	"	<tr>";
	strAppend +=	"		<td valign='top'>";
	strAppend +=	"			<font>Send Report To :<\/font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<textarea id='txtSendTo' name='txtSendTo' style='height:80px;width:300px;'></textarea> <font class='mandatory'><b>*</b>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"<\/table>";
	
	return strAppend;
}

UI_SchedRept.distroy = function(){
	$('#spnSchedule').remove();
}

UI_SchedRept.scheduleReport = function() {
	var valiMessage = UI_SchedRept.validateParams();
	if(valiMessage == null) {
		var data = {};
		data['composerName'] = UI_SchedRept.composerName;	
		data['cronType'] = $('#selScheduleFor').val();
		data['reportPeriodCriteria'] = $('#selPeriod').val();
		data['scheduledTime'] = $('#selScheduledTime').val();
		data['sendReportTo'] = $('#txtSendTo').val();
		data['startDate'] = $('#startDateCal').val();
		data['endDate'] = $('#endDateCal').val();
		top[2].ShowProgress();
		$('#btnDone').attr('disable',true);
		$('#' + UI_SchedRept.formName).ajaxSubmit(
			{ dataType: 'json', 
			  data:data, 
			  url:"scheduleAReport.action",
			  success: UI_SchedRept.scheduleReportSuccess });
	} else {
		UI_SchedRept.displayError(valiMessage);
	}
}

UI_SchedRept.scheduleReportSuccess = function(response){
	if(response.success) {
		UI_SchedRept.displaySuccess("Report Successfully Scheduled.");
		UI_SchedRept.distroy();
		top[2].HideProgress();
	} else {
		UI_SchedRept.displayError(response.errorMessage);
		top[2].HideProgress();
	}
	$('#btnDone').attr('disable',false);
}

UI_SchedRept.displaySuccess = function(message){
	if(UI_SchedRept.successCallback != null){
		UI_SchedRept.successCallback.call(this, message);
	} else {
		alert(message);
	}	
}

UI_SchedRept.displayError = function(message){
	top[2].HideProgress();
	if(UI_SchedRept.errorCallback != null){
		UI_SchedRept.errorCallback.call(this, message);
	} else {
		alert(message);
	}	
	$('#btnDone').attr('disable',false);
}

UI_SchedRept.validateParams = function() {

	if($('#startDateCal').val() == null || $('#startDateCal').val() == "") {
		return "Please enter start date.";
	} else {
		var dateFilter = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		if(!dateFilter.test($('#startDateCal').val())) {
			return "Please enter valid start date.";
		}
	}

	if($('#endDateCal').val() == null || $('#endDateCal').val() == "") {
		return "Please enter end date.";
	} else {
		var dateFilter = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		if(!dateFilter.test($('#endDateCal').val())) {
			return "Please enter valid end date.";
		}
	}

	var startDate = $('#startDateCal').val();
	var endDate = $('#endDateCal').val();
	var intStartDate = parseInt(startDate.substring(6) + startDate.substring(3,5) + startDate.substring(0,2));
	var intEndDate = parseInt(endDate.substring(6) + endDate.substring(3,5) + endDate.substring(0,2));
	if(intStartDate > intEndDate){
		return "Start date cannot be grater than end date.";
	}
 
	if($("#txtSendTo").val() != null && $("#txtSendTo").val() != "" ) {
		var emailFilter=/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		if(!emailFilter.test($("#txtSendTo").val())){
			return "Please enter correct email adress. Email adresses should be comma seperated."
		}
	} else {
	    return "Please enter send report to email adress(es).";
	}
	return null;
}

UI_SchedRept.checkButtonAppParam  = function (){
	$('.schdReptButton').hide();
	if(top.strReportScheduleEnabled == "true"){
		$('.schdReptButton').show();
	}	
}

