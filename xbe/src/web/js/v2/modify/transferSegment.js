	/*
	*********************************************************
		Description		: XBE Transfer Segment 
		Author			: Pradeep Karunanayake
		Version			: 1.0
		Created			: 15th March 2009		
	*********************************************************	
	*/

	/*
	 * Make Reservation related Methods
	 */
	function UI_TransferSegment(){}
	UI_TransferSegment.tabStatus = ({tab0:true});		// tab Status
	UI_TransferSegment.tabDataRetrive = ({tab0:false});	// Data Retrieval Status
	UI_TransferSegment.selCurrency = "";
	UI_TransferSegment.ohdReleaseTime = DATA_ResPro.transferSegment.ohdReleaseTime;
	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divBookingTabs").getLanguage();
		$("#divLegacyRootWrapper").fadeIn("slow");
		$("#divBookingTabs").tabs();
		$('#divBookingTabs').bind('tabsbeforeactivate', function(event, ui) {
			return eval("UI_TransferSegment.tabStatus.tab" + ui.newTab.index());
		});
		
		UI_commonSystem.strPGMode = DATA_ResPro.transferSegment.mode;
		
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
		
		// Search Flight Tab -----------------------------------------------------
		UI_tabSearchFlights.ready();
		UI_tabSearchFlights.fillModifySegment(DATA_ResPro.transferSegment);
		
		UI_commonSystem.strPGID = "SC_RESV_CC_005";		
		
	});
	
	/*
	 * Open Tab from the Function Call
	 */
	UI_TransferSegment.openTab = function(intIndex){
		$('#divBookingTabs').tabs("option", "active", intIndex); 	
	}	
	
	/*
	 * Lock Tabs
	 */ 
	UI_TransferSegment.lockTabs = function(){
		UI_TransferSegment.tabStatus = ({tab0:true
										});
		
		UI_TransferSegment.tabDataRetrive = ({tab0:false, 
										});
	}
	/*
	 * Transfer segment
	 */
	UI_TransferSegment.saveTransferSegemnt = function() {
		var data = {};	
		data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();		
		var fareQuoteData = UI_tabSearchFlights.createFareQuoteParams();				
		data = $.airutil.dom.concatObjects(data,fareQuoteData);		
		data["cabinClass"] = $("#classOfService").val();
		data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
		data['pnr'] = DATA_ResPro.transferSegment.PNR;
		data['groupPNR'] = DATA_ResPro.transferSegment.groupPNR;
		data['version']= DATA_ResPro.transferSegment.version;
		data['flightRPHList'] = $.toJSON(UI_tabSearchFlights.getFlightRPHList());
		data['fltSegment'] = DATA_ResPro.transferSegment.modifyingSegments;
		data['alertSegmentTransfer'] = DATA_ResPro.transferSegment.alertSegmentTransfer;	
		data['alertSegmentID'] = DATA_ResPro.transferSegment.alertSegmentID;
		data['gdsPnr'] = DATA_ResPro.transferSegment.gdsPnr;
		var dT =UI_tabSearchFlights.getFlightRPHList();
		var departureTime=dT[0].departureTime;
		
		if(DATA_ResPro.transferSegment.status == 'OHD' && UI_TransferSegment.compareTime(UI_TransferSegment.ohdReleaseTime,departureTime)) {
			showCommonError("Error", "New departure time can not be earlier than release onhold time");
			UI_commonSystem.hideProgress();	
		} else {
		    $("#frmTabLoad").ajaxSubmit({dataType: 'json', processData: false, data:data,  url:"transferSegmentUpdate.action",							
				success: UI_TransferSegment.clearAlertByTransferSegment,error:UI_commonSystem.setErrorStatus});
		}
	}
	
        /*
	 * This method compares two dates with times.
	 * if date1 >= date2 this will returns true.
	 * else returns false.
	 * Time Format - date1="20-02-2015 13:53"  date2="1502191810"
	 */ 
	UI_TransferSegment.compareTime = function(date1,date2){

	    date1 = date1.split(" ");
	    
		var date2D = Number(date2.substring(4,6));
		var date2M = Number(date2.substring(2,4));
		var date2Y = Number(date2.substring(0,2));
		
		var date1D = Number(date1[0].substring(0,2));
		var date1M = Number(date1[0].substring(3,5));
		var date1Y = Number(date1[0].substring(8));
		
		var date2hh = Number(date2.substring(6,8));
		var date2mm = Number(date2[0].substring(8));

		var date1hh = Number(date1[1].substring(0,2));
		var date1mm = Number(date1[1].substring(3));
		
		if(date2Y != date1Y){
			if(date2Y < date1Y){return true;}
			else{return false;}
		}
		else if(date2M != date1M){
			if(date2M < date1M){return true;}
			else{return false;}
		}
		else if(date2D != date1D){
			if(date2D < date1D){return true;}
			else{return false;}
		}
		else if(date2hh != date1hh){
			if(date2hh < date1hh){return true;}
			else{return false;}
		}
		else if(date2mm != date1mm){
			if(date2mm < date1mm){return true;}
			else{return false;}
		}
		else{
			//Both times are equal
			return true;
		}
		
	}
	/*
	 * Transfer segment success
	 */
	UI_TransferSegment.transferSegmentSuccess = function(response) {
		if(response.success) {
			showCommonError("CONFIRMATION", "Reservation Transfered Successfully");			
			location.replace("showNewFile!loadRes.action?" +
					"pnrNO="+ DATA_ResPro.transferSegment.PNR +
					"&groupPNRNO="+DATA_ResPro.transferSegment.groupPNR +
					"&forwardmessage=transferSegmentSuccess");	
		} else {
			showERRMessage(response.messageTxt);
		}
		UI_commonSystem.hideProgress();
	}
	/*
	 * Load Modify Reservation Home page
	 */
	UI_TransferSegment.loadHomePage = function(){				
		location.replace("showNewFile!loadRes.action?" +
				"pnrNO="+ DATA_ResPro.transferSegment.PNR +
				"&groupPNRNO="+DATA_ResPro.transferSegment.groupPNR);
	}
	
	UI_TransferSegment.initialization = function(){
		UI_commonSystem.initializeMessage();
		$("#frmSrchFlt").reset();	
		UI_tabSearchFlights.fillModifySegment(DATA_ResPro.transferSegment);		
			
		$("#departureVariance").val(top.strDefDV);
		$("#returnVariance").val(top.strDefDV);		
		
		var exists = false;
		$('#selectedCurrency option').each(function(){
		    if (this.value == DATA_ResPro.initialParams.defaultCurrencyCode) {
		        exists = true;
		        return false;
		    }
		});
		
		if(exists){
			$("#selectedCurrency").val(DATA_ResPro.initialParams.defaultCurrencyCode);	
		}else{
			$("#selectedCurrency").val(DATA_ResPro.initialParams.baseCurrencyCode);
		}
		
		//$("#btnFQ").enable();
		
		$("#divResultsPane").hide();	
		
		$("#fromAirport").focus();
	}
	
	UI_TransferSegment.clearAlertByTransferSegment = function(response) {
		if( DATA_ResPro.initialParams.clearAlertAfterTransfer){
			var data = {};
		    data["clearAlert.PNR"] = response.pnr;
			data["clearAlert.groupPNR"] = response.groupPNR;
			data["segmentRefNo"] = response.alertSegmentID;
			data["carrirCode"] = DATA_ResPro.transferSegment.modifyingSegmentOperatingCarrier;
			data["clearAlert.actionText"] = "by System in Transfer Segment"
			data["clearAlert.actionTaken"] = "Alert Cleared Automatically  |";			
			data["resAlerts"] =  $.toJSON(DATA_ResPro.resAlerts);		
			$("#frmModiBkg").ajaxSubmit({dataType: 'json', processData: false, data:data, url:"clearAlert.action",							
				success: UI_TransferSegment.transferSegmentSuccess,error:UI_commonSystem.setErrorStatus});													
	  }else{
		  UI_TransferSegment.transferSegmentSuccess(response);
		}
	}
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	