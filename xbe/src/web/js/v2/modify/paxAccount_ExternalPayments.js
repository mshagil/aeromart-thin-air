	/*
	*********************************************************
		Description		: XBE - Pax Account Details - External Payments
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	var jsonEPayObj = [
					{id:"#selStatus", desc:"Status", required:true}
					];
	
	var jsEPayData = [
						{displayChannel:"", displayTimeStamp:"", displayStatus:"", displayAmount:""}
					];
	
	function UI_paxAccountEPay(){}
	UI_paxAccountEPay.intEPID = null;
	
	/*
	 * Payment ready method
	 */
	UI_paxAccountEPay.ready = function(){

		$("#btnEPClose").decorateButton();
		$("#btnEPClose").click(function() {UI_paxAccountEPay.closeOnclick();});
		
		$("#btnEPUpdate").decorateButton();
		$("#btnEPUpdate").click(function() {UI_paxAccountEPay.updateOnclick();});
		
		UI_paxAccountEPay.constructGrid({id:"#tblExternalPay"});
		UI_paxAccountEPay.fillData();
	}
	
	/*
	 * Payment Fill Data
	 */
	UI_paxAccountEPay.fillData = function(){
	//	UI_commonSystem.fillGridData({id:"#tblExternalPay", data:jsEPayData})
		UI_commonSystem.fillGridData({id:"#tblExternalPay", data:jsExternalPayments});
		
	}
	
	/*
	 * Charges Validate
	 */
	UI_paxAccountEPay.validateEPay = function(){
		UI_message.initializeMessage();
		
		if ($("#tblExternalPay").getGridParam('selrow') == null){
			UI_message.showErrorMessage({messageText:raiseErrorWindow("XBE-ERR-23"," a record")});
			return false;
		}
		
		/* Invalid Character Validations */
		if (!UI_commonSystem.validateInvalidCharWindow(jsonEPayObj)){return false;}
		
		if (!UI_commonSystem.validateMandatoryWindow(jsonEPayObj)){return false;}
		
		UI_paxAccountEPay.intEPID = $("#tblExternalPay").getGridParam('selrow');
		return true;
	}
	
	/*
	 * User Notes Close
	 */
	UI_paxAccountEPay.closeOnclick = function(){
		UI_paxAccountDetails.close();
	}
	
	/*
	 * Update On Click
	 */
	UI_paxAccountEPay.updateOnclick = function(inParams){
		if (UI_paxAccountEPay.validateEPay()){
			$("#frmPaxAccount").action("paxAccountExternalPayConfirm.action?hdnID=" + UI_paxAccountEPay.intEPID);
			$("#frmPaxAccount").ajaxSubmit({ dataType: 'json', 
											success: UI_paxAccountEPay.updateSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	/*
	 * Payment Confirm Success
	 */
	UI_paxAccountEPay.updateSuccess = function(response){
		UI_message.showConfirmationMessage({messageText:jsSuccessMsg.msgExternalPay})
		alert("TODO\n Reload the Parent Page data");
	}
	
	/*
	 * Construct Grid
	 */
	UI_paxAccountEPay.constructGrid = function(inParams){
		//construct grid
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 380,
			width: 800,
			colNames:[getOpeneri18nData('PaxAccDetails_Channel','Channel'),getOpeneri18nData('PaxAccDetails_TimeStamp','Time Stamp'), getOpeneri18nData('PaxAccDetails_Status','Status'),getOpeneri18nData('PaxAccDetails_Amount','Amount')],
			colModel:[
				{name:'displayChannel', index:'displayChannel', width:130, align:"center"},
				{name:'displayTimeStamp', index:'displayTimeStamp', width:100, align:"center"},
				{name:'displayStatus', index:'displayStatus', width:100, align:"center"},
				{name:'displayAmount',  index:'displayAmount', width:120, align:"right"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
			}
		});
	}
	/* ------------------------------------------------ end of File ---------------------------------------------- */