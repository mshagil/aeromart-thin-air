	/*
	*********************************************************
		Description		: XBE Modify Segment 
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	/*
	 * Make Reservation related Methods
	 */
	function UI_modifySegment(){}
	UI_modifySegment.tabStatus = ({tab0:true, tab1:false, tab2:false});		// tab Status
	UI_modifySegment.tabDataRetrive = ({tab0:false, tab1:false, tab2:false});	// Data Retrieval Status
	UI_modifySegment.selCurrency = "";
	UI_modifySegment.modifyingFltSegHash = {};
	UI_modifySegment.airportMessage = "";
	UI_modifySegment.allSegments = null;
	UI_modifySegment.resPaxInfo = "";
	UI_modifySegment.userNote = "";

	/*
	 * Onload Function
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapper").fadeIn("slow");
		$("#divBookingTabs").tabs();
		$('#divBookingTabs').bind('tabsbeforeactivate', function(event, ui) {
			return eval("UI_modifySegment.tabStatus.tab" + ui.newTab.index());
		});
		
		UI_commonSystem.strPGMode = DATA_ResPro.modifySegments.mode;
		
		if(DATA_ResPro.initialParams.viewAvailableCredit) {
			UI_commonSystem.showAgentCredit();
			UI_commonSystem.showAgentCreditInAgentCurrency();
		} else {
			$("#divCreditPnl").hide();
		}
		
		// Search Flight Tab -----------------------------------------------------
		UI_tabSearchFlights.ready();
		UI_tabSearchFlights.fillModifySegment(DATA_ResPro.modifySegments);
		
		UI_tabSearchFlights.disableEnablePageControls(true);
		UI_modifySegment.populateModifyingSegmentData();
		UI_commonSystem.strPGID = "SC_RESV_CC_005";
	});
	
	/*
	 * Open Tab from the Function Call
	 */
	UI_modifySegment.openTab = function(intIndex){
		$('#divBookingTabs').tabs("option", "active", intIndex); 	
	}
	
	/*
	 * Lock Tabs
	 */ 
	UI_modifySegment.lockTabs = function(){
		UI_modifySegment.tabStatus = ({tab0:true, 
										tab1:false, 
										tab2:false});
		
		UI_modifySegment.tabDataRetrive = ({tab0:false, 
										tab1:true,
										tab2:false});
	}
	
	/*
	 * Confirm Update
	 */
	UI_modifySegment.showConfirmUpdate = function(){
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}				
		top.objCW= window.open("showNewFile!loadConfirmUpdate.action?confirmUpdateMode=MODIFYSEGMENT","myWindow",$("#popWindow").getPopWindowProp(630, 900));		
	}
	
	UI_modifySegment.populateModifyingSegmentData = function(){
		DATA_ResPro.resSegments = $.airutil.sort.quickSort(DATA_ResPro.resSegments,UI_modifySegment.modifySegmentComparator);
		UI_modifySegment.allSegments = $.toJSON(DATA_ResPro.resSegments);
		var modifyingSegIds = DATA_ResPro.modifySegments.modifyingSegments.split(',');
		var segIdHash = {};
		for(var i = 0; i < modifyingSegIds.length; i++){
			segIdHash[modifyingSegIds[i]] = true;
		}
		for(var i = 0; i < DATA_ResPro.resSegments.length; i++){
			var key = DATA_ResPro.resSegments[i].bookingFlightSegmentRefNumber+'$'+DATA_ResPro.resSegments[i].interlineGroupKey;
			if(segIdHash[key]){
				UI_modifySegment.modifyingFltSegHash[DATA_ResPro.resSegments[i].flightSegmentRefNumber] = DATA_ResPro.resSegments[i];
			}
		}
	}
	
	/***
	 *  Calls for confirming open return segment (When searchFlight --> Book clicks)
	 */
	UI_modifySegment.confirmOpenReturn = function(){
		var data = {};
		data['pnr']               = DATA_ResPro.modifySegments.PNR;
		data['groupPNR']          = DATA_ResPro.modifySegments.groupPNR;
		data['fltSegment']        = DATA_ResPro.modifySegments.modifyingSegments;
		data['modifySegment']     = DATA_ResPro.modifySegments.modifyingSegments;
		data['selCurrency']       = UI_modifySegment.selCurrency;
		data['resSegments']       = $.toJSON(DATA_ResPro.resSegments);
		data['resPaxs']           = $.toJSON(UI_modifySegment.getModifyPaxInfo());
		data['version']           = DATA_ResPro.modifySegments.version;		
		data['openReturnConfirm'] = true;
		data['selectedFareType']  = UI_tabSearchFlights.selectedFareType;
		data['flightRPHList']     = $.toJSON(UI_tabSearchFlights.getFlightRPHList());
		data['flexiSelected']     = UI_tabSearchFlights.isFlexiSelected();
		data["cabinClass"]        = $("#classOfService").val(); 
		var fareQuoteData = UI_tabSearchFlights.createFareQuoteParams();					
		data = $.airutil.dom.concatObjects(data,fareQuoteData);	
		
		UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'confirmOpenReturn.action', dataType: 'json', processData: false, data:data,
										success: UI_modifySegment.confirmOpenReturnSuccess,
										error:UI_commonSystem.setErrorStatus});
		
	}
		
	/*
	 * Capture Payment 
	 */
	UI_modifySegment.capturePayment = function(data){		
		data['pnr'] = DATA_ResPro.modifySegments.PNR;		
		data['modifySegment'] = DATA_ResPro.modifySegments.modifyingSegments;
		data['selCurrency'] = UI_modifySegment.selCurrency;
		data['resSegments']= $.toJSON(DATA_ResPro.resSegments);
		data['resPaxs']= $.toJSON(UI_modifySegment.getModifyPaxInfo());
		data['version']= DATA_ResPro.modifySegments.version;				
		data['selectedFareType'] = UI_tabSearchFlights.selectedFareType;
		data['flightRPHList'] = $.toJSON(UI_tabSearchFlights.getFlightRPHList());
		data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
		data['resContactInfo'] = $.toJSON(DATA_ResPro.resContactInfo);
		data['ownerAgentCode'] =  DATA_ResPro.modifySegments.ownerAgentCode;
		UI_modifySegment.userNote = data['userNote'];		
		var fareQuoteData = UI_tabSearchFlights.createFareQuoteParams();					
		data = $.airutil.dom.concatObjects(data,fareQuoteData);	
		UI_commonSystem.getDummyFrm().ajaxSubmit({ url:'modifyConfirm.action', dataType: 'json', processData: false, data:data,
										success: UI_modifySegment.continueOnClickSuccess,
										error:UI_commonSystem.setErrorStatus});
	}
	
	UI_modifySegment.getSubmittableContactInfo = function (contactInfo){
		var data = {};
		for(key in contactInfo){
			data['contactInfo.'+key] = contactInfo[key];
		}
		return data;
	}
	
	//Fill the paxifo only needed for modifying
	UI_modifySegment.getModifyPaxInfo = function(){
		var jPassengers = DATA_ResPro.resPaxs;
		var jpData = [];
		var splitPax= {"firstName":"", "travelerRefNumber": "", "lastName":"", "infants":"", "paxType":"", "paxSequence":"", "parent": ""};
		var tmpSplit = {};
		for(var jpl=0;jpl < jPassengers.length;jpl++){
			tmpSplit = $.airutil.dom.cloneObject(splitPax);
			tmpSplit.firstName = jPassengers[jpl].firstName;
			tmpSplit.lastName=jPassengers[jpl].lastName;
			tmpSplit.travelerRefNumber=jPassengers[jpl].travelerRefNumber;
			tmpSplit.infants=jPassengers[jpl].infants;
			tmpSplit.paxType=jPassengers[jpl].paxType;
			tmpSplit.paxSequence = jPassengers[jpl].paxSequence;
			tmpSplit.parent = jPassengers[jpl].parent;
			jpData[jpl]= tmpSplit;
		}
		
		return jpData;
	}
	
	UI_modifySegment.showConfirmError = function(resMessage){
		showERRMessage(resMessage);
	}
	
	
	/*
	 * Confirm open return segment success
	 */
	UI_modifySegment.confirmOpenReturnSuccess = function(response) {
		if(response.success) {
			showCommonError("CONFIRMATION", "Reservation Segment Confirmed Successfully");			
			location.replace("showNewFile!loadRes.action?pnrNO=" + DATA_ResPro.modifySegments.PNR+"&groupPNRNO="+DATA_ResPro.modifySegments.groupPNR + "&forwardmessage=confirmSegmentSuccess");	
		} else {
			showERRMessage(response.messageTxt);
		}
		UI_commonSystem.hideProgress();
	}
	/*
	 * Continue On Click success
	 */
	UI_modifySegment.continueOnClickSuccess = function(response){
		if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		// prepare the next tab 
		UI_tabPayment.isSegModify = true;
		UI_tabPayment.ready();
		UI_commonSystem.hideProgress();
		
		if (response != null){
			if(!response.success){
				showERRMessage(response.messageTxt);
				
				UI_modifySegment.airportMessage = response.airportMessage;
				UI_modifySegment.loadAirportMessage();	
				
				return false;
			}
			if(response.blnNoPay){
				DATA_ResPro.modifySegments.groupPNR = response.groupPNR;
				UI_modifySegment.loadHomePage();
				return false;
			}
		
			UI_tabPayment.updatePayemntTabTO(response);
		}
		UI_modifySegment.tabDataRetrive.tab2 = false;
		UI_modifySegment.tabStatus.tab2 = true;
		UI_modifySegment.openTab(2);
		UI_tabPayment.paymentDetailsOnLoadCall();
	}
	
	UI_modifySegment.bookOnClickSuccess = function(){
//		if(DATA_ResPro.modifySegments.groupPNR != null && DATA_ResPro.modifySegments.groupPNR != 'null' &&
//				DATA_ResPro.modifySegments.groupPNR != ""){
//			UI_commonSystem.hideProgress();
//			UI_modifySegment.showConfirmUpdate();
//		}else {
			UI_modifySegment.continueToAnci();
//		}
		
	}
	UI_modifySegment.getInsuranceFlightSegments = function(){
		var allSegs = DATA_ResPro.resSegments;
		var out = [];
		for(var i = 0 ;i < allSegs.length; i++){
			var key = allSegs[i].flightSegmentRefNumber;				
			if(allSegs[i].status != 'CNX' && UI_modifySegment.modifyingFltSegHash[key] == null){ //skips cancelled and modifying segment
				out[out.length] = allSegs[i];
			}
		}
		return out;
	}
	
	UI_modifySegment.getModifyingFlightSegmentsIds = function(){
		var allSegs = DATA_ResPro.resSegments;
		var out = '';
		for(var i = 0 ;i < allSegs.length; i++){
			var key = allSegs[i].flightSegmentRefNumber;				
			if(UI_modifySegment.modifyingFltSegHash[key] != null){ 
				var arr = key.split('$');
				if(arr != null && arr[2] != null){
					if(out == ''){
						out = arr[2];
					}
					else{
						out = out + "," + arr[2];
					}
				}
			}
		}
		return out;
	}
	
	UI_modifySegment.continueToAnci = function(){
		//if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		UI_commonSystem.showProgress();
		UI_tabAnci.isSegModify = true;
		
		var system = null;
		var data = UI_tabSearchFlights.createSearchParams('searchParams');
		data['flightRPHList'] = $.toJSON(UI_tabSearchFlights.getFlightRPHList());
		//data['flexiSelected'] = UI_tabSearchFlights.isFlexiSelected();
		data['flexiAlertInfo'] = DATA_ResPro.jsonFlexiAlerts;
		data['insuranceAvailable'] = DATA_ResPro.resHasInsurance;
		data['insFltSegments'] = $.toJSON(UI_modifySegment.getInsuranceFlightSegments());
		data['modifyingSegmentsIds'] = UI_modifySegment.getModifyingFlightSegmentsIds();
		data['modifySegment'] = true;
		data['pnr'] = DATA_ResPro.modifySegments.PNR;
		//data['paxAdults'] = UI_tabPassenger.createAdultPaxInfo();
		//data['paxInfants'] = UI_tabPassenger.createInfantPaxInfo();
		//data['flightDetails'] = $.toJSON(jsonFltDetails);
		data['selectedSystem'] = UI_tabSearchFlights.getSelectedSystem();
		data['resPaxInfo']= $.toJSON(UI_modifySegment.resPaxInfo);
		data['mulipleMealsSelectionEnabled'] = DATA_ResPro.initialParams.multiMealEnabled;
		
		UI_commonSystem.getDummyFrm().ajaxSubmit({url:'ancillaryAvailability.action', dataType: 'json', processData: false, data:data, success: function (res){
			UI_commonSystem.hideProgress();
			if(res.success){
				var paxArr = UI_tabAnci.transformPax(DATA_ResPro.resPaxs);
				paxArr = $.airutil.sort.quickSort(paxArr, UI_tabAnci.paxSorter);
				var infArr = [];
				var ninfArr = [];
				for(var i=0 ; i < paxArr.length; i++){
					if(paxArr[i].paxType == 'IN'){
						infArr[infArr.length] = paxArr[i];
					}else{
						ninfArr[ninfArr.length] = paxArr[i];
					}
				}
				res.paxAdults = ninfArr;
				res.paxInfants = infArr;
				res.contactInfo = DATA_ResPro.resContactInfo;
				
				
				//UI_modifySegment.openTab(1);
				UI_tabAnci.pnr =  DATA_ResPro.modifySegments.PNR;
				UI_tabAnci.prepareAncillary(res, jsonFareQuoteSummary);
			}else{			
				showERRMessage(res.messageTxt);
			}
		}, error:UI_commonSystem.setErrorStatus
		});
		
		
	}
	
	/**
	 * yyyyMMddHHmmss
	 */
	UI_modifySegment._getFltRefDate = function(dstr){
		var i = function(str){ return parseInt(str,10);};
		return new Date(i(dstr.substr(0,4)), i(dstr.substr(4,2))-1, i(dstr.substr(6,2)), i(dstr.substr(8,2)), i(dstr.substr(10,2)), i(dstr.substr(12,2)));
	}
	
	
	UI_modifySegment.modifySegmentComparator = function (first ,second){
		var fArr = first.flightSegmentRefNumber.split('$');
		var sArr = second.flightSegmentRefNumber.split('$');
		var fTime = UI_modifySegment._getFltRefDate(fArr[3]);
		var sTime = UI_modifySegment._getFltRefDate(sArr[3]);
		return (fTime.getTime()-sTime.getTime());
	}
	
	/*
	 * Load modify segment source page
	 */
	UI_modifySegment.loadHomePage = function(){
		UI_modifySegment.selCurrency = "";
		location.replace("showNewFile!loadRes.action?pnrNO=" + DATA_ResPro.modifySegments.PNR+"&groupPNRNO="+DATA_ResPro.modifySegments.groupPNR);
	}
	

	UI_modifySegment.loadAirportMessage = function() {
			var html = '<a href="javascript:void(0)" onclick="UI_modifySegment.hideAirportMessage()"><img border="0" src="../images/A150_no_cache.gif" title="Close" align="right"><\/a> ';
			var apMsg = UI_modifySegment.airportMessage;		
			$('#airportMsg').html('');	
			$('#airportMsg').append(html + apMsg);
			
			if(apMsg != null && apMsg != ''){
				$('#airportMsg').slideDown('slow');
			} else {
				$('#airportMsg').hide();
			}	
			
		}
		
	UI_modifySegment.hideAirportMessage = function(){		
		$("#airportMsg").hide();
	}
		
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	