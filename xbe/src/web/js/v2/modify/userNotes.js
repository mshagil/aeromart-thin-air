	/*
	*********************************************************
		Description		: XBE - User Notes
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created			: 25th August 2008
		Last Modified	: 25th August 2008
	*********************************************************	
	*/

	/*
	 * Passenger Tab
	 */
	function UI_userNotes(){}
	
	/*
	 * Contact Info Tab Page On Load
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#btnClose").decorateButton();
		$("#btnClose").click(function() {UI_userNotes.closeOnclick();});
		
		/* construct grid */
		UI_userNotes.constructGrid({id:"#tblHistory"});
		
		UI_userNotes.onLoadCall();
	});
	
	/*
	 * On Load Call
	 */
	UI_userNotes.onLoadCall = function(){
		UI_commonSystem.fillGridData({id:"#tblHistory", data:jsHistoryData})
	}
	
	/*
	 * User Notes Close
	 */
	UI_userNotes.closeOnclick = function(){
		window.close();
	}
	
	/*
	 * Grid Constructor
	 */
	UI_userNotes.constructGrid = function(inParams){
		//construct grid
		$(inParams.id).jqGrid({
			datatype: "local",
			height: 500,
			width: 850,
			colNames:[ geti18nData('History_lbl_Date','Date'),  geti18nData('History_lbl_Action','Action'),  geti18nData('History_lbl_UserNote','User Note'), geti18nData('History_lbl_UserName','User Name'), geti18nData('History_lbl_CarrierCode','Carrier Code')],
			colModel:[
				{name:'displayModifyDate',  index:'displayModifyDate', width:80, align:"center"},
				{name:'displayAction',  index:'displayAction', width:125, align:"left"},
				{name:'displayUserNote', index:'displayUserNote', width:570, align:"left"},
				{name:'displayUserName', index:'displayUserName', width:75, align:"center"},
				{name:'displayCarrierCode', index:'displayCarrierCode', width:50, align:"center"}
			],
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true
		});
	}
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
	
	