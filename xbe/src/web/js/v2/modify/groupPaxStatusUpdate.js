	function UI_groupPaxStatusUpdate(){}
	
	UI_groupPaxStatusUpdate.reloadBooking = false;
	
	$(document).ready(function(){	
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		$("#btnUpdate").decorateButton();
		$("#btnClose").decorateButton();
		
		$("#btnClose").click(function() {UI_groupPaxStatusUpdate.closeOnclick();});
		$( "#selSegment" ).change(function() {
			UI_commonSystem.fillGridData({id:"#listPax", data:fltSegPaxMap[$( "#selSegment" ).val()].paxEticketArr});
			UI_groupPaxStatusUpdate.populateSegmentData(fltSegPaxMap[$( "#selSegment" ).val()]);
			UI_groupPaxStatusUpdate.reset();
		});
		
		if(!DATA_ResPro.initialParams.allowChangePassengerCouponStatus){
			$("#btnUpdate").hide();
			$("#trStatusInfo").hide();
		} else {			
			$("#btnUpdate").click(function() {UI_groupPaxStatusUpdate.updateGroupPaxStatus();});			
		}		
		UI_groupPaxStatusUpdate.populateSegmentSelections();
		UI_groupPaxStatusUpdate.constructGrid();		
		UI_groupPaxStatusUpdate.populateInitialData();		
	});
	
	/*
	 * populate segment selection data
	 */
	UI_groupPaxStatusUpdate.populateSegmentSelections = function(){
		
		var $select = $('#selSegment');                        
	    $select.find('option').remove(); 
		$.each(fltSegPaxMap, function( index, value ) {
			$select.append('<option value=' + index + '>' + value.segmentCode + "-" + value.depDate + '</option>');
		});
	}
	
	/*
	 * Construct Grid
	 */
	UI_groupPaxStatusUpdate.constructGrid = function(){
		
		//construct grid
		jQuery("#listPax").jqGrid({ 
			datatype: "local",	
			colNames:['Title' , 'First Name' , 'Last Name', 'Type' , 'PaxName' , 'E-TicketId' , 'E-Ticket', 'Pax Status', 'Ticket Status', 'travelerRefNumber', 'Coupon'], 
			colModel:[ 	
			           	{name:'title',index:'title', width:60, hidden:true},
			           	{name:'firstName',index:'firstName', width:60, hidden:true}, 
			           	{name:'lastName',index:'lastName', width:60, hidden:true},			 
			           	{name:'paxType',index : 'paxType', width:30},
			           	{name:'paxName',index:'PaxName', width:60, formatter:paxFormatter},
			           	{name:'eticketId',index : 'eticketId', width:30, hidden:true},
					   	{name:'eticketNum', index : 'eticketNum', width:30, align:"center"},					   	
					   	{name:'paxStatus',index : 'paxStatus', width:30, align:"center"},
					   	{name:'eticketStatus', index : 'eticketStatus', width:30, align:"center"},
					   	{name:'travelerRefNumber', index : 'travelerRefNumber', width:30, hidden:true},
					   	{name:'coupanNo', index : 'coupanNo', width:30 }					   	
					  ], 
			imgpath: '../../themes/default/images/jquery', multiselect:false,
			viewrecords: true,
			multiselect: true,
			height:300,
			width:800,
			onSelectRow: function(rowId) { 
				
				var selRowArr = jQuery("#listPax").getGridParam('selarrrow');
				var selRowData = $("#listPax").getRowData(rowId);
				if(jQuery.inArray( rowId, selRowArr ) !== -1){
					if(selRowArr.length > 1){
						var setEticketStatus = true;
						var setPaxStatus = true;
						$.each(selRowArr, function( index, rowIndex ) {
							if(rowId !== rowIndex){
								var preRowData = $("#listPax").getRowData(rowIndex);							
								if(preRowData.eticketStatus !== selRowData.eticketStatus){
									setEticketStatus = false;
								}	
								if(preRowData.paxStatus !== selRowData.paxStatus){
									setPaxStatus = false;
								} 
							}
						});
						
						if(setEticketStatus){
							$('[name=selETLStatus] option').filter(function() { 
							    return ($(this).text() == selRowData.eticketStatus); 
							}).prop('selected', true);
						} else {
							$('#selETLStatus').find("option").attr("selected", false);
						}
						if(setPaxStatus){
							$('[name=selPFSStatus] option').filter(function() { 
							    return ($(this).text() == selRowData.paxStatus); 
							}).prop('selected', true);
						} else {
							$('#selPFSStatus').find("option").attr("selected", false);
						}
					} else {
						$('[name=selETLStatus] option').filter(function() { 
						    return ($(this).text() == selRowData.eticketStatus); 
						}).prop('selected', true);
						$('[name=selPFSStatus] option').filter(function() { 
						    return ($(this).text() == selRowData.paxStatus); 
						}).prop('selected', true);
					}
				} else {
					if(selRowArr.length > 0){
						var setEticketStatus = true;
						var setPaxStatus = true;
						var paxEticketStatus = "";
						var paxPFSStatus  = "";
						$.each(selRowArr, function( index, rowIndex ) {
							var preRowData = $("#listPax").getRowData(rowIndex);
							if(paxEticketStatus === ""){
								paxEticketStatus = preRowData.eticketStatus;
							} else {
								if(paxEticketStatus !== preRowData.eticketStatus){
									setEticketStatus = false;
								}
							}
							if(paxPFSStatus === ""){
								paxPFSStatus = preRowData.paxStatus;
							} else {
								if(paxPFSStatus !== preRowData.paxStatus){
									setPaxStatus = false;
								}
							}
						});
						if(setEticketStatus){
							$('[name=selETLStatus] option').filter(function() { 
							    return ($(this).text() == paxEticketStatus); 
							}).prop('selected', true);
						} else {
							$('#selETLStatus').find("option").attr("selected", false);
						}
						if(setPaxStatus){
							$('[name=selPFSStatus] option').filter(function() { 
							    return ($(this).text() == paxPFSStatus); 
							}).prop('selected', true);
						} else {
							$('#selPFSStatus').find("option").attr("selected", false);
						}
					} else {
						$('#selETLStatus').find("option").attr("selected", false);
						$('#selPFSStatus').find("option").attr("selected", false);
					}
				}
			},
			loadui:'block'});

		
	}
	
	function paxFormatter ( cellvalue, options, rowObject )
	{
		var paxTitle = "";
		 if(rowObject.title !== undefined && rowObject.title !== null){
			 paxTitle = rowObject.title;
		 }
	     return  paxTitle + ' ' + rowObject.firstName + ' ' + rowObject.lastName;
	}
	
	/*
	 * Fill initial data
	 */
	UI_groupPaxStatusUpdate.populateInitialData = function(){
		for (first in fltSegPaxMap) break;
		UI_commonSystem.fillGridData({id:"#listPax", data:fltSegPaxMap[first].paxEticketArr});		
		$("#target").val($("#target option:first").val());
		UI_groupPaxStatusUpdate.populateSegmentData(fltSegPaxMap[first]);
		$("#selPFSStatus").fillDropDown({dataArray:opener.top.arrAllPaxStatus, keyIndex:0 , valueIndex:1, firstEmpty:true });
		$("#selETLStatus").fillDropDown({dataArray:opener.top.arrAllCouponStatus, keyIndex:0 , valueIndex:1, firstEmpty:true });
	}
	
	/*
	 * Fill segment details
	 */
	UI_groupPaxStatusUpdate.populateSegmentData = function(fltSeg){
		$("#txtSegmentCode").text(fltSeg.segmentCode);
		$("#txtDepDate").text(fltSeg.depDate);
		$("#txtFltNum").text(fltSeg.fltNumber);
		$("#txtCarrier").text(fltSeg.carrier);
		$("#txtSegmentStatus").text(fltSeg.segmentStatus);
	}
	
	/*
	 * close button
	 */
	UI_groupPaxStatusUpdate.closeOnclick = function(){
		window.close();
	}
	
	UI_groupPaxStatusUpdate.updateGroupPaxStatus = function(){	

		var updatedPassengerTicketCoupons = [];
		if(UI_groupPaxStatusUpdate.validateUpdate()){
			var data = {};
			data['pnr'] = opener.jsonPNR;
			data['remark'] = $("#txtRemarks").val();
			data['groupPnr'] = opener.jsonGroupPNR;
			
			var selRowArr = jQuery("#listPax").getGridParam('selarrrow');
			$.each(selRowArr, function( index, rowId ) {
				var rowData = $("#listPax").getRowData(rowId);
				
				data['modifyingPassengerTicketCoupons[' + index + '].travelerRefNumber'] = rowData.travelerRefNumber;
				data['modifyingPassengerTicketCoupons[' + index + '].eticketId'] = rowData.eticketId;
				data['modifyingPassengerTicketCoupons[' + index + '].segmentCode'] = fltSegPaxMap[$( "#selSegment" ).val()].segmentCode;
				data['modifyingPassengerTicketCoupons[' + index + '].couponNo'] = rowData.coupanNo;
				data['modifyingPassengerTicketCoupons[' + index + '].passengerName'] = rowData.paxName;
				data['modifyingPassengerTicketCoupons[' + index + '].carrierCode'] = fltSegPaxMap[$( "#selSegment" ).val()].carrier;
				data['modifyingPassengerTicketCoupons[' + index + '].flightNo'] = fltSegPaxMap[$( "#selSegment" ).val()].fltNumber;
				data['modifyingPassengerTicketCoupons[' + index + '].departureDate'] = fltSegPaxMap[$( "#selSegment" ).val()].depDate;
				data['modifyingPassengerTicketCoupons[' + index + '].departureDateZulu'] = fltSegPaxMap[$( "#selSegment" ).val()].depDateZulu;
				data['modifyingPassengerTicketCoupons[' + index + '].flightSegmentRef'] = $( "#selSegment" ).val();
				data['modifyingPassengerTicketCoupons[' + index + '].paxStatus'] = rowData.paxStatus;
			});
			UI_commonSystem.showProgress();
			data['modifiedCoupon.paxETicketStatus'] =$("#selETLStatus").val();
			data['modifiedCoupon.paxStatus'] =$("#selPFSStatus").val();
			$("#frmGroupPaxStatusUpdate").action("loadPaxETicketMask!saveGroupModifiedCoupon.action");
			$("#frmGroupPaxStatusUpdate").ajaxSubmit({ dataType: 'json', processData: false, data:data,
				success: UI_groupPaxStatusUpdate.saveGroupModifiedCouponSuccess, error:UI_commonSystem.setErrorStatus});
		}
	}
	
	/*
	 * Ajax Return Call for passenger coupon modify
	 */
	UI_groupPaxStatusUpdate.saveGroupModifiedCouponSuccess = function(response){
		UI_commonSystem.hideProgress();
		if(response.success) {
			UI_groupPaxStatusUpdate.reloadBooking = true;
			UI_message.showConfirmationMessage({messageText:response.messageTxt});
			UI_groupPaxStatusUpdate.updateStatusChanges(response.updatedPassengerTicketCoupons)			
		}else{
			UI_message.showErrorMessage({messageText:response.messageTxt});
		}		
	}	
	
	UI_groupPaxStatusUpdate.forceReloadReservation = function(){
		if(UI_groupPaxStatusUpdate.reloadBooking){
			UI_groupPaxStatusUpdate.reloadBooking = false;	
			if(opener.DATA_ResPro.initialParams.restrictModForCnxFlown) {
				if(opener.jsonGroupPNR != undefined && opener.jsonGroupPNR != ""){
					opener.top.blnInterLine = true;
				}
				opener.top.loadNewModifyBooking(opener.jsonPNR, false);
			} else {
				opener.UI_reservation.loadReservation("", null, false);	
			}	
		}
	}
	
	UI_groupPaxStatusUpdate.updateStatusChanges = function(updatedList){
		
		var selRowArr = jQuery("#listPax").getGridParam('selarrrow');
		
		$.each(fltSegPaxMap[$( "#selSegment" ).val()].paxEticketArr, function( index, paxData ) {
				$.each(updatedList, function( index, updatedPaxData ) {
					if(paxData.eticketId === updatedPaxData.eticketId){
						paxData.eticketStatus = updatedPaxData.paxETicketStatus;
						paxData.paxStatus = updatedPaxData.paxStatus;
					}
				});
		});
		UI_commonSystem.fillGridData({id:"#listPax", data:fltSegPaxMap[$( "#selSegment" ).val()].paxEticketArr});
		UI_groupPaxStatusUpdate.reset();
		
	}
	
	UI_groupPaxStatusUpdate.reset = function(){
		jQuery("#listPax").jqGrid('resetSelection');
		$("#txtRemarks").val('');
		$('select#selETLStatus option').removeAttr("selected");
		$('select#selPFSStatus option').removeAttr("selected");
	}
	
	UI_groupPaxStatusUpdate.validateUpdate = function(){
		var selRowArr = jQuery("#listPax").getGridParam('selarrrow');
		
		if(selRowArr.length === 0){
			UI_message.showErrorMessage({messageText:"Please Select Data"});
			return false;
		}
		if($("#txtRemarks").val() === ''){
			UI_message.showErrorMessage({messageText:"Please Enter Remarks"});
			return false;
		}
		
		if($("#selETLStatus").val() === "" && $("#selPFSStatus").val() === "" ){
			UI_message.showErrorMessage({messageText:"Please Select PFS or ETL data"});
			return false;
		}
		return true;
	}
	
	$(window).unload(function() {
		UI_groupPaxStatusUpdate.forceReloadReservation();	
	});
	
	
	/* ------------------------------------------------ end of File ---------------------------------------------- */
	
