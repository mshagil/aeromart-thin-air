	/*
	 * International Flight Details Window
	 */
	function UI_IntlFlightDetails(){}
	
	UI_IntlFlightDetails.jsonPaxAdtObj = {};
	UI_IntlFlightDetails.arrError = [];
	
	var paxAdults = {};
	var paxInfants = {};
	
	
	var adCount = 0;
	var chCount = 0;
	var inCount = 0;

	/*
	 * International Flight Details Pop up On Load
	 */
	$(document).ready(function(){
		$("#divLegacyRootWrapperPopUp").fadeIn("slow");
		
		$("#btnConfirm").decorateButton();
		$("#btnConfirm").disable();
		$("#btnConfirm").click(function() {UI_IntlFlightDetails.confirmOnclick();});
		
		$("#btnCancel").decorateButton();
		$("#btnCancel").click(function() {UI_IntlFlightDetails.cancelOnclick();});
		
		UI_IntlFlightDetails.onLoad();
		$("#1_pnrPaxArrivalFlightNumber").focus()
	});
	
	/*
	 * Pax details On Load
	 */
	UI_IntlFlightDetails.onLoad = function(){
		if (DATA_ResPro.mode == 'CREATE') {
			paxAdults = opener.UI_tabPassenger.getAdultDetails();
			paxInfants = opener.UI_tabPassenger.getInfantDetails();
		}
		/***** IE Memory leak FIXEs  load opener object in the loading time****/
		UI_IntlFlightDetails.jsonPaxAdtObj = opener.jsonPaxAdtObj;
		
		UI_IntlFlightDetails.arrError =  opener.top.arrError;
		
		$("#divHDPane1").html("International Flight Details");
		UI_IntlFlightDetails.fillPaneData();
		UI_IntlFlightDetails.mapCalendar();
		UI_IntlFlightDetails.disableEnableControls(false);
		$("#btnConfirm").enable();
	}	

	
	/*
	 * Fill Pane Data
	 */
	UI_IntlFlightDetails.fillPaneData = function(inParams){
		UI_IntlFlightDetails.constructGridPane({id:"#tablPane1"});
		var objPaxDt = new Array();
		var adultCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults.length;
		for(var i = 0; i < adultCount; i++) {
			objPaxDt[i] = {};
			objPaxDt[i].paxType 	= UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displayAdultType;
			objPaxDt[i].title 	= UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displayAdultTitle;
			objPaxDt[i].name 	= UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displayAdultFirstName + "&nbsp;" + UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displayAdultLastName;

			objPaxDt[i].pnrPaxArrivalFlightNumber = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalFlightNumber;
			objPaxDt[i].pnrPaxFltArrivalDate = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displaypnrPaxFltArrivalDate;
			objPaxDt[i].pnrPaxArrivalTime = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displaypnrPaxArrivalTime;
			objPaxDt[i].pnrPaxDepartureFlightNumber = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureFlightNumber;
			objPaxDt[i].pnrPaxFltDepartureDate = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displaypnrPaxFltDepartureDate;
			objPaxDt[i].pnrPaxDepartureTime = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displaypnrPaxDepartureTime;
			
		}
		if (UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants != null) {
			var infantCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants.length;
			for(var j = 0; j < infantCount; j++) {
				objPaxDt[j + adultCount] = {};
				objPaxDt[j + adultCount].paxType 	= "IN";
				objPaxDt[j + adultCount].title 		= "-";
				objPaxDt[j + adultCount].name 		= UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displayInfantFirstName + "&nbsp;" + UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displayInfantLastName;

				objPaxDt[j + adultCount].pnrPaxArrivalFlightNumber = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalFlightNumber;
				objPaxDt[j + adultCount].pnrPaxFltArrivalDate = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displaypnrPaxFltArrivalDate;
				objPaxDt[j + adultCount].pnrPaxArrivalTime = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displaypnrPaxArrivalTime;
				objPaxDt[j + adultCount].pnrPaxDepartureFlightNumber = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureFlightNumber;
				objPaxDt[j + adultCount].pnrPaxFltDepartureDate = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displaypnrPaxFltDepartureDate;
				objPaxDt[j + adultCount].pnrPaxDepartureTime = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[j].displaypnrPaxDepartureTime;
				
			}
		}
		UI_commonSystem.fillGridData({id:"#tablPane1", data:objPaxDt});		
	}

	
	UI_IntlFlightDetails.constructGridPane = function(inParams){
		//construct grid
		var columnNames = [];
		var columnModels = [];
		
		var arrivalDateHeading = "Arrival Date ";
		
		
		//get Adult, Child & infant count
		var adultCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults.length;
		for(var i = 0; i < adultCount; i++) {
			if(UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displayAdultType == "AD"){
				adCount++;
			} else if(UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[i].displayAdultType == "CH"){
				chCount++;
			}
		}
		
		if (UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants != null) {
			inCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants.length;
		}
		
		
			
		columnNames.push(geti18nData('BookingInfo_PassengerInfo_lbl_Type','Type'));
		columnNames.push(geti18nData('BookingInfo_PassengerInfo_lbl_Title','Title'));
		columnNames.push(geti18nData('BookingInfo_PassengerInfo_lbl_Name','Name'));
		columnNames.push(geti18nData('BookingInfo_PassengerInfo_lbl_ArrivalFlight',"Arrival Flt No"));
			
		columnModels.push({name:'paxType', index:'paxType', width:40, align:"center"});
		columnModels.push({name:'title', index:'title', width:40, align:"center"});
		columnModels.push({name:'name', index:'name', width:110, align:"left"});
		columnModels.push({name:'pnrPaxArrivalFlightNumber', index:'pnrPaxArrivalFlightNumber', 
			width:100, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"10", style:"width:60px;", onchange : 'UI_IntlFlightDetails.IntlFlightDetailsOnChange()'}});
			
		columnNames.push("Arrival Date");
		columnModels.push({name:'pnrPaxFltArrivalDate', index:'pnrPaxFltArrivalDate', 
			width:100, align:"center", editable:true, 
			editoptions:{className:"aa-input", maxlength:"10", style:"width:75px;",disabled:true, onchange : 'UI_IntlFlightDetails.IntlFlightDetailsOnChange()',
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_IntlFlightDetails.dobOnBlur(this);
				}
			}]}
		});
			
		columnNames.push(geti18nData('IntlFlight_ArrivalTime','Arrival Time'));
		columnModels.push({name:'pnrPaxArrivalTime', index:'pnrPaxArrivalTime', 
			width:100, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"5", style:"width:60px;", onchange : 'UI_IntlFlightDetails.IntlFlightDetailsOnChange()',
				dataEvents:[{
					type : 'change',
					fn: function(e){
						setTimeWithColon(this);
					}
				}]}
		});
		columnNames.push(geti18nData('IntlFlight_DepartureFlNo','Departure Flt No'));
		columnModels.push({name:'pnrPaxDepartureFlightNumber', index:'pnrPaxDepartureFlightNumber', 
				width:100, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"10", style:"width:60px;", onchange : 'UI_IntlFlightDetails.IntlFlightDetailsOnChange()'}});
		columnNames.push("Departure Date");
		columnModels.push({name:'pnrPaxFltDepartureDate', index:'pnrPaxFltDepartureDate', 
			width:100, align:"center", editable:true, 
			editoptions:{className:"aa-input", maxlength:"10", style:"width:75px;",disabled:false, onchange : 'UI_IntlFlightDetails.IntlFlightDetailsOnChange()',
			dataEvents:[{
				type : 'change',
				fn: function(e){
					UI_IntlFlightDetails.dobOnBlur(this);
				}
			}]}
		});
		columnNames.push(geti18nData('IntlFlight_DepartureTime','Departure Time'));
		columnModels.push({name:'pnrPaxDepartureTime', index:'pnrPaxDepartureTime', 
			width:100, align:"center", editable:true,editoptions:{className:"aa-input", maxlength:"5", style:"width:60px;", onchange : 'UI_IntlFlightDetails.IntlFlightDetailsOnChange()',
				dataEvents:[{
					type : 'change',
					fn: function(e){
						setTimeWithColon(this);
					}
				}]}
		});
			

		$(inParams.id).jqGrid({
			datatype: "local",
			height: 150,
			width: 730,
			colNames:columnNames,
			colModel:columnModels,
			imgpath: UI_commonSystem.strImagePath,
			multiselect: false,
			viewrecords: true,
			onSelectRow: function(rowid){
			}
		});
	}
	
	UI_IntlFlightDetails.dobOnBlur = function(objC) {
		var strDT = dateChk(objC.id);
		strDT = strDT != true ? strDT : objC.value;
		if (strDT == false) {
			objC.value = objC.value;
		} else {
			objC.value = strDT;	
		}
			
	}
	
	
	/*
	 * Disable Page Controls
	 */ 
	UI_IntlFlightDetails.disableEnableControls = function(blnStatus){
		var adultCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults.length;
		
			for(var i = 1; i <= adultCount; i++) {
				$.each(UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults, function(){
					$("#"+ i + "_pnrPaxArrivalFlightNumber").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxFltArrivalDate").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxArrivalTime").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxDepartureFlightNumber").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxFltDepartureDate").disableEnable(blnStatus);
					$("#"+ i + "_pnrPaxDepartureTime").disableEnable(blnStatus);
				});
			}
			
			if (UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants != null) {
				var infantCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants.length;
				var index = 0;
				for(var j = 1; j <= infantCount; j++) {
					$.each(UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants, function(){
						index = j + adultCount;
						$("#"+ index + "_pnrPaxArrivalFlightNumber").disableEnable(blnStatus);
						$("#"+ index + "_pnrPaxFltArrivalDate").disableEnable(blnStatus);
						$("#"+ index + "_pnrPaxArrivalTime").disableEnable(blnStatus);
						$("#"+ index + "_pnrPaxDepartureFlightNumber").disableEnable(blnStatus);
						$("#"+ index + "_pnrPaxFltDepartureDate").disableEnable(blnStatus);
						$("#"+ index + "_pnrPaxDepartureTime").disableEnable(blnStatus);
					});					
				}
			}
		
	}
	
	/*
	 * Save
	 */
	UI_IntlFlightDetails.confirmOnclick = function(){		
		if(!UI_IntlFlightDetails.validate()) {			
			return;
		}

		var data = {};
		
		UI_commonSystem.showProgress();		

		var adultCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults.length;
		var infantCount = 0;
		if(UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants != null){
			infantCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants.length;
		}
		var infantIndex = 0;
		var totalCount = adultCount+infantCount;
		for(var i = 1; i <= totalCount; i++) {
			var index = i - 1;
			var rowTR = $("#"+ i + "paxType").closest('tr');				
			
			if(i <= adultCount) {
				UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[index].displaypnrPaxArrivalFlightNumber = $.trim($("#"+ i + "_pnrPaxArrivalFlightNumber").val());
				UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[index].displaypnrPaxFltArrivalDate = $("#"+ i + "_pnrPaxFltArrivalDate").val();
				UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[index].displaypnrPaxArrivalTime = $("#"+ i + "_pnrPaxArrivalTime").val();
				UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[index].displaypnrPaxDepartureFlightNumber = $("#"+ i + "_pnrPaxDepartureFlightNumber").val();
				UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[index].displaypnrPaxFltDepartureDate = $("#"+ i + "_pnrPaxFltDepartureDate").val();
				UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults[index].displaypnrPaxDepartureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
			} else {		
				var count = 0;
				if(infantIndex < infantCount){
					UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[infantIndex].displaypnrPaxArrivalFlightNumber = trim($("#"+ i + "_pnrPaxArrivalFlightNumber").val());
					UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[infantIndex].displaypnrPaxFltArrivalDate = $("#"+ i + "_pnrPaxFltArrivalDate").val();
					UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[infantIndex].displaypnrPaxArrivalTime = $("#"+ i + "_pnrPaxArrivalTime").val();
					UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[infantIndex].displaypnrPaxDepartureFlightNumber = $("#"+ i + "_pnrPaxDepartureFlightNumber").val();
					UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[infantIndex].displaypnrPaxFltDepartureDate = $("#"+ i + "_pnrPaxFltDepartureDate").val();
					UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants[infantIndex].displaypnrPaxDepartureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
					infantIndex++;
				}				
				
			}
		}	
		UI_IntlFlightDetails.disableEnableControls(true);
		UI_commonSystem.hideProgress();
		
		//Value accessible from the tabPassenger.js. Specifies whether passport data is validated or not.
		opener.internationalFlightDetailsValidated=true;
		window.close();
	}
	
UI_IntlFlightDetails.validate = function() {
		

		var validate1 = "true";
		var selDateArr = "";
		var arrivalDate="";
		var arrivalTime = "";
		var selDateDept = "";
		var departureDate = "";
		var departureTime = "";
		var timeFormatValidationRegex = /^([0-9]{1,2})\:([0-9]{2})$/;
		var minDate = "";
		var maxDate="";
		var minTime = "";
		var maxTime = "";
		
		// Validate FOID Fields	for AD & CH
		var numPax = 0;
		numPax = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults.length;
		var strFlightLstSegmentDeptDate = "";
		if(DATA_ResPro.mode == 'MODIFY') {
			if (opener.UI_reservation != null) {
				var allFltDetails = opener.UI_reservation.allFlightDetails;
				minDate = UI_IntlFlightDetails.getMaximumDateValue(allFltDetails);
				
				var minTimeH = minDate.getHours();
				var minTimeM = minDate.getMinutes();
				if (minTimeH != null && minTimeH < 10) {
					minTimeH = "0" + minTimeH;
				}
				
				if (minTimeM != null && minTimeM < 10) {
					minTimeM = "0" + minTimeM;
				}
				minTime = minTimeH + ":" + minTimeM;
				minDate.setHours(0,0,0,0);
				
				maxDate = UI_IntlFlightDetails.getMinimumDateValue(allFltDetails);
				
				var maxTimeH = maxDate.getHours();
				var maxTimeM = maxDate.getMinutes();
				if (maxTimeH != null && maxTimeH < 10) {
					maxTimeH = "0" + maxTimeH;
				}
				if (maxTimeM != null && maxTimeM < 10) {
					maxTimeM = "0" + maxTimeM;
				}
				maxTime = maxTimeH + ":" + maxTimeM;
				maxDate.setHours(0,0,0,0);
			}			
		} else {
			if (opener.UI_tabSearchFlights!= null && opener.UI_tabSearchFlights != "") {
				if(opener.UI_tabSearchFlights.strRetDate != null && opener.UI_tabSearchFlights.strRetDate != ""){
					var arrFlightDetails = opener.UI_tabSearchFlights.flightInfo[opener.UI_tabSearchFlights.flightInfo.length-1];
					var tmpDateString = arrFlightDetails.arrivalDateValue;
					var tmpDateVal = null;
					if (tmpDateString != null) {
						var tmpYear = tmpDateString.substring(0,2);
						var tmpMonth = tmpDateString.substring(2,4);
						var tmpDate = tmpDateString.substring(4,6);
						var dateString = tmpDate + tmpMonth + tmpYear;
						tmpDateVal = dateChk(dateString);
					}
					minTime = arrFlightDetails.arrivalTime;
					if (tmpDateVal != null && tmpDateVal != "false") {
						var arrtDateArr = tmpDateVal.split('/');
						minDate = new Date(arrtDateArr[2], arrtDateArr[1]-1, arrtDateArr[0]);
					}
					
					if (minDate == null) {
						minDate = new Date();
					}
					
				} else if(opener.UI_tabSearchFlights.strOutDate != null && opener.UI_tabSearchFlights.strOutDate != ""){
					var arrFlightDetails = opener.UI_tabSearchFlights.flightInfo[0];
					var tmpDateString = arrFlightDetails.arrivalDateValue;
					var tmpDateVal = null;
					if (tmpDateString != null) {
						var tmpYear = tmpDateString.substring(0,2);
						var tmpMonth = tmpDateString.substring(2,4);
						var tmpDate = tmpDateString.substring(4,6);
						var dateString = tmpDate + tmpMonth + tmpYear;
						tmpDateVal = dateChk(dateString);
					}
					minTime = arrFlightDetails.arrivalTime;
					if (tmpDateVal != null && tmpDateVal != "false") {
						var arrtDateArr = tmpDateVal.split('/');
						minDate = new Date(arrtDateArr[2], arrtDateArr[1]-1, arrtDateArr[0]);
					}
					
					if (minDate == null) {
						minDate = new Date();
					}
				} 
				
				if (opener.UI_tabSearchFlights.strOutDate != null && opener.UI_tabSearchFlights.strOutDate != "") {
					var departureFlight = opener.UI_tabSearchFlights.flightInfo[0];
					var tmpArrDateString = departureFlight.departureDateValue;
					var tmpDateValArr = null;
					if (tmpArrDateString != null) {
						var tmpYear = tmpArrDateString.substring(0,2);
						var tmpMonth = tmpArrDateString.substring(2,4);
						var tmpDate = tmpArrDateString.substring(4,6);
						var dateString = tmpDate + tmpMonth + tmpYear;
						tmpDateValArr = dateChk(dateString);
					}
					if (tmpDateValArr != null && tmpDateValArr != "false") {
						var arrtDateArr = tmpDateValArr.split('/');
						maxDate = new Date(arrtDateArr[2], arrtDateArr[1]-1, arrtDateArr[0]);
					}
					if (maxDate == null) {
						maxDate = new Date();
					}
					maxTime = opener.UI_tabSearchFlights.flightInfo[0].departureTime;
				}
			}			
		}
				
		
		for(var i = 1; i <= numPax; i++) {
			
			if ($("#"+ i + "_pnrPaxArrivalFlightNumber").val() != "") {
				selDateArr = $("#"+ i + "_pnrPaxFltArrivalDate").val();
				arrivalTime =  $("#"+ i + "_pnrPaxArrivalTime").val();
				if (arrivalTime != null && arrivalTime.length == 4) {
					arrivalTime = "0" + arrivalTime;
				}
				
				if(selDateArr == null || selDateArr =="") {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
					$("#"+ i + "_pnrPaxFltArrivalDate").focus();
					return false;
				}
				if (arrivalTime == "" || arrivalTime == null) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time")});
					$("#"+ i + "_pnrPaxArrivalTime").focus();
					return false;
				}
				
			} else if ($("#"+ i + "_pnrPaxFltArrivalDate").val() != null && $("#"+ i + "_pnrPaxFltArrivalDate").val() != "") {
				selDateArr = $("#"+ i + "_pnrPaxFltArrivalDate").val();
				arrivalTime =  $("#"+ i + "_pnrPaxArrivalTime").val();
				if (arrivalTime != null && arrivalTime.length == 4) {
					arrivalTime = "0" + arrivalTime;
				}
				if (arrivalTime == "" || arrivalTime == null) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time")});
					$("#"+ i + "_pnrPaxArrivalTime").focus();
					return false;
				}
			} else if ($("#"+ i + "_pnrPaxArrivalTime").val() != null && $("#"+ i + "_pnrPaxArrivalTime").val() != "") {
				selDateArr = $("#"+ i + "_pnrPaxFltArrivalDate").val();
				arrivalTime =  $("#"+ i + "_pnrPaxArrivalTime").val();
				if (arrivalTime != null && arrivalTime.length == 4) {
					arrivalTime = "0" + arrivalTime;
				}
				if(selDateArr == null || selDateArr =="") {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
					$("#"+ i + "_pnrPaxFltArrivalDate").focus();
					return false;
				}
			}
			
			if ($("#"+ i + "_pnrPaxDepartureFlightNumber").val() != "") {
				selDateDept = $("#"+ i + "_pnrPaxFltDepartureDate").val();
				departureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
				if (departureTime != null && departureTime.length == 4) {
					departureTime = "0"+departureTime;
				}
				if(selDateDept == null || selDateDept =="") {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
					$("#"+ i + "_pnrPaxFltDepartureDate").focus();
					return false;
				}
				if (departureTime == "" || departureTime == null) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time")});
					$("#"+ i + "_pnrPaxDepartureTime").focus();
					return false;
				}
			} else if ($("#"+ i + "_pnrPaxFltDepartureDate").val() != null && $("#"+ i + "_pnrPaxFltDepartureDate").val() != "") {
				selDateDept = $("#"+ i + "_pnrPaxFltDepartureDate").val();
				departureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
				if (departureTime != null && departureTime.length == 4) {
					departureTime = "0"+departureTime;
				}
				if (departureTime == "" || departureTime == null) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time")});
					$("#"+ i + "_pnrPaxDepartureTime").focus();
					return false;
				}
			} else if ($("#"+ i + "_pnrPaxDepartureTime").val() != null && $("#"+ i + "_pnrPaxDepartureTime").val() != "") {
				selDateDept = $("#"+ i + "_pnrPaxFltDepartureDate").val();
				departureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
				if (departureTime != null && departureTime.length == 4) {
					departureTime = "0"+departureTime;
				}
				if(selDateDept == null || selDateDept =="") {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
					$("#"+ i + "_pnrPaxFltDepartureDate").focus();
					return false;
				}
			}
			
			if (selDateArr != null && selDateArr != "") {
				var arrDate = selDateArr.split('/');
				if (arrDate.length != 3) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
					$("#"+ i + "_pnrPaxFltArrivalDate").focus();
					return false;
				}
				arrivalDate = new Date(arrDate[2], arrDate[1]-1, arrDate[0]);
				if (maxDate != null && maxDate !="" && arrivalDate > maxDate) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
					$("#"+ i + "_pnrPaxFltArrivalDate").focus();
					return false;
				}
			}
			
			if (selDateDept != null && selDateDept != "") {
				var arrDeptDate = selDateDept.split('/');
				if (arrDeptDate.length != 3) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
					$("#"+ i + "_pnrPaxFltDepartureDate").focus();
					return false;
				}
				departureDate = new Date(arrDeptDate[2], arrDeptDate[1]-1, arrDeptDate[0]);
				if (minDate != null && minDate != "" && minDate > departureDate) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
					$("#"+ i + "_pnrPaxFltDepartureDate").focus();
					return false;
				}
			}
			
			
			
			if (arrivalTime != null && arrivalTime != "" && arrivalTime.length == 4) {
				if (!timeFormatValidationRegex.test(arrivalTime) || (arrivalTime.substr(2,2) >= 0 && arrivalTime.substr(2,2) > 59) || arrivalTime.substr(1,1) != ":") {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time Format")});
					$("#"+ i + "_pnrPaxArrivalTime").focus();
					return false;
				}
				
			} else {
				if ((arrivalTime != null && arrivalTime != "") && (!timeFormatValidationRegex.test(arrivalTime) || !(arrivalTime.length == 5) || (arrivalTime.substr(0,2) >= 0 && arrivalTime.substr(0,2) >= 24) ||
			            (arrivalTime.substr(3,2) >= 0 && arrivalTime.substr(3,2) > 59) || arrivalTime.substr(2,1) != ":")) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time Format")});
					$("#"+ i + "_pnrPaxArrivalTime").focus();
					return false;
				}
			}
			
			if (departureTime != null && departureTime != "" && departureTime.length == 4) {
				if (!timeFormatValidationRegex.test(departureTime) || (departureTime.substr(2,2) >= 0 && departureTime.substr(2,2) > 59) || departureTime.substr(1,1) != ":") {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time Format")});
					$("#"+ i + "_pnrPaxDepartureTime").focus();
					return false;
				}
			} else {
				if ((departureTime != null && departureTime != "") && (!timeFormatValidationRegex.test(departureTime) || !(departureTime.length == 5) || (departureTime.substr(0,2) >= 0 && departureTime.substr(0,2) >= 24) ||
			            (departureTime.substr(3,2) >= 0 && departureTime.substr(3,2) > 59) || departureTime.substr(2,1) != ":")) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time Format")});
					$("#"+ i + "_pnrPaxDepartureTime").focus();
					return false;
				}
			}
				
			

			if (arrivalDate != null && arrivalDate != "" && arrivalTime != null
				&& arrivalTime != "" && maxDate != null && maxDate != ""
				&& maxTime != null && maxTime != "" && maxDate.getTime() == arrivalDate.getTime() && arrivalTime>=maxTime) {
				UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time")});
				$("#"+ i + "_pnrPaxArrivalTime").focus();
				return false;
			}
			
			if (departureDate != null && departureDate != "" && departureTime != null
					&& departureTime != "" && minDate != null && minDate != ""
					&& minTime != null && minTime != "" && minDate.getTime() == departureDate.getTime() && departureTime<=minTime) {
					UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time")});
					$("#"+ i + "_pnrPaxDepartureTime").focus();
					return false;
			}
			
		}
		
		var numInPax = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants==null?0:UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants.length;
		
		if(numInPax != 0){ 
			var totPaxCount = numPax + numInPax;
			var infIntCount = 0;
			
			for(var i = 1; i <= totPaxCount; i++) {
				
				//Skip AD+CH
				if(i<=numPax){
					continue;
				}
				
				if ($("#"+ i + "_pnrPaxArrivalFlightNumber").val() != "") {
					selDateArr = $("#"+ i + "_pnrPaxFltArrivalDate").val();
					arrivalTime =  $("#"+ i + "_pnrPaxArrivalTime").val();
					if (arrivalTime != null && arrivalTime.length == 4) {
						arrivalTime = "0" + arrivalTime;
					}
					if(selDateArr == null || selDateArr =="") {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
						$("#"+ i + "_pnrPaxFltArrivalDate").focus();
						return false;
					}
					if (arrivalTime == "" || arrivalTime == null) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time")});
						$("#"+ i + "_pnrPaxArrivalTime").focus();
						return false;
					}
					
				} else if ($("#"+ i + "_pnrPaxFltArrivalDate").val() != null && $("#"+ i + "_pnrPaxFltArrivalDate").val() != "") {
					selDateArr = $("#"+ i + "_pnrPaxFltArrivalDate").val();
					arrivalTime =  $("#"+ i + "_pnrPaxArrivalTime").val();
					if (arrivalTime != null && arrivalTime.length == 4) {
						arrivalTime = "0" + arrivalTime;
					}
					if (arrivalTime == "" || arrivalTime == null) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time")});
						$("#"+ i + "_pnrPaxArrivalTime").focus();
						return false;
					}
				} else if ($("#"+ i + "_pnrPaxArrivalTime").val() != null && $("#"+ i + "_pnrPaxArrivalTime").val() != "") {
					selDateArr = $("#"+ i + "_pnrPaxFltArrivalDate").val();
					arrivalTime =  $("#"+ i + "_pnrPaxArrivalTime").val();
					if (arrivalTime != null && arrivalTime.length == 4) {
						arrivalTime = "0" + arrivalTime;
					}
					if(selDateArr == null || selDateArr =="") {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
						$("#"+ i + "_pnrPaxFltArrivalDate").focus();
						return false;
					}
				}
				
				if ($("#"+ i + "_pnrPaxDepartureFlightNumber").val() != "") {
					selDateDept = $("#"+ i + "_pnrPaxFltDepartureDate").val();
					departureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
					if (departureTime != null && departureTime.length == 4) {
						departureTime = "0"+departureTime;
					}
					if(selDateDept == null || selDateDept =="") {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
						$("#"+ i + "_pnrPaxFltDepartureDate").focus();
						return false;
					}
					if (departureTime == "" || departureTime == null) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time")});
						$("#"+ i + "_pnrPaxDepartureTime").focus();
						return false;
					}
				} else if ($("#"+ i + "_pnrPaxFltDepartureDate").val() != null && $("#"+ i + "_pnrPaxFltDepartureDate").val() != "") {
					selDateDept = $("#"+ i + "_pnrPaxFltDepartureDate").val();
					departureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
					if (departureTime != null && departureTime.length == 4) {
						departureTime = "0"+departureTime;
					}
					if (departureTime == "" || departureTime == null) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time")});
						$("#"+ i + "_pnrPaxDepartureTime").focus();
						return false;
					}
				} else if ($("#"+ i + "_pnrPaxDepartureTime").val() != null && $("#"+ i + "_pnrPaxDepartureTime").val() != "") {
					selDateDept = $("#"+ i + "_pnrPaxFltDepartureDate").val();
					departureTime = $("#"+ i + "_pnrPaxDepartureTime").val();
					if (departureTime != null && departureTime.length == 4) {
						departureTime = "0"+departureTime;
					}
					if(selDateDept == null || selDateDept =="") {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
						$("#"+ i + "_pnrPaxFltDepartureDate").focus();
						return false;
					}
				}
				
				if (selDateArr != null && selDateArr != "") {
					var arrDate = selDateArr.split('/');
					if (arrDate.length != 3) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
						$("#"+ i + "_pnrPaxFltArrivalDate").focus();
						return false;
					}
					arrivalDate = new Date(arrDate[2], arrDate[1]-1, arrDate[0]);
					if (maxDate != null && maxDate !="" && arrivalDate > maxDate) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Date")});
						$("#"+ i + "_pnrPaxFltArrivalDate").focus();
						return false;
					}
				}
				
				if (selDateDept != null && selDateDept != "") {
					var arrDeptDate = selDateDept.split('/');
					if (arrDeptDate.length != 3) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
						$("#"+ i + "_pnrPaxFltDepartureDate").focus();
						return false;
					}
					departureDate = new Date(arrDeptDate[2], arrDeptDate[1]-1, arrDeptDate[0]);
					if (minDate != null && minDate != "" && minDate > departureDate) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Date")});
						$("#"+ i + "_pnrPaxFltDepartureDate").focus();
						return false;
					}
				}
				
				if (arrivalTime != null && arrivalTime != "" && arrivalTime.length == 4) {
					if (!timeFormatValidationRegex.test(arrivalTime) || (arrivalTime.substr(2,2) >= 0 && arrivalTime.substr(2,2) > 59) || arrivalTime.substr(1,1) != ":") {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time Format")});
						$("#"+ i + "_pnrPaxArrivalTime").focus();
						return false;
					}
					
				} else {
					if ((arrivalTime != null && arrivalTime != "") && (!timeFormatValidationRegex.test(arrivalTime) || !(arrivalTime.length == 5) || (arrivalTime.substr(0,2) >= 0 && arrivalTime.substr(0,2) >= 24) ||
				            (arrivalTime.substr(3,2) >= 0 && arrivalTime.substr(3,2) > 59) || arrivalTime.substr(2,1) != ":")) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time Format")});
						$("#"+ i + "_pnrPaxArrivalTime").focus();
						return false;
					}
				}
				
				if (departureTime != null && departureTime != "" && departureTime.length == 4) {
					if (!timeFormatValidationRegex.test(departureTime) || (departureTime.substr(2,2) >= 0 && departureTime.substr(2,2) > 59) || departureTime.substr(1,1) != ":") {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time Format")});
						$("#"+ i + "_pnrPaxDepartureTime").focus();
						return false;
					}
				} else {
					if ((departureTime != null && departureTime != "") && (!timeFormatValidationRegex.test(departureTime) || !(departureTime.length == 5) || (departureTime.substr(0,2) >= 0 && departureTime.substr(0,2) >= 24) ||
				            (departureTime.substr(3,2) >= 0 && departureTime.substr(3,2) > 59) || departureTime.substr(2,1) != ":")) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time Format")});
						$("#"+ i + "_pnrPaxDepartureTime").focus();
						return false;
					}
				}
				
				if (arrivalDate != null && arrivalDate != "" && arrivalTime != null
						&& arrivalTime != "" && maxDate != null && maxDate != ""
						&& maxTime != null && maxTime != "" && maxDate.getTime() == arrivalDate.getTime() && arrivalTime>=maxTime) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Arrival Time")});
						$("#"+ i + "_pnrPaxArrivalTime").focus();
						return false;
				}
					
				if (departureDate != null && departureDate != "" && departureTime != null
						&& departureTime != "" && minDate != null && minDate != ""
						&& minTime != null && minTime != "" && minDate.getTime() == departureDate.getTime() && departureTime<=minTime) {
						UI_message.showErrorMessage({messageText: buildError(UI_IntlFlightDetails.arrError["XBE-ERR-04"], "Departure Time")});
						$("#"+ i + "_pnrPaxDepartureTime").focus();
						return false;
				}
				
				
				infIntCount++;
			}
		}
		
		return true;
	}

	
	/*
	 * Cancel
	 */
	UI_IntlFlightDetails.cancelOnclick = function(){
		window.close();
	}
	
	
	/*
	 * Date On Blur
	 */
//	UI_IntlFlightDetails.dobOnBlur = function(objC) {
//		var strDT = dateChk(objC.id);
//		strDT = strDT != true ? strDT : objC.value;
//		objC.value = strDT;
//	}
	
	UI_IntlFlightDetails.mapCalendar = function() {
		
		var adultCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxAdults.length;
		var infantCount = 0;
		if(UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants != null){
			infantCount = UI_IntlFlightDetails.jsonPaxAdtObj.paxInfants.length;
		}
		var intLen = adultCount+infantCount;
		
		if(opener.UI_tabSearchFlights != null){	
			
			if(opener.UI_tabSearchFlights.strRetDate != null && opener.UI_tabSearchFlights.strRetDate != ""){
				var arrFlightDetails = opener.UI_tabSearchFlights.flightInfo[opener.UI_tabSearchFlights.flightInfo.length-1];
				var tmpDateString = arrFlightDetails.arrivalDateValue;
				if (tmpDateString != null) {
					var tmpYear = tmpDateString.substring(0,2);
					var tmpMonth = tmpDateString.substring(2,4);
					var tmpDate = tmpDateString.substring(4,6);
					var dateString = tmpDate + tmpMonth + tmpYear;
					minDate = dateChk(dateString);
				}
				if (minDate == null || !minDate) {
					minDate = new Date();
				}
			} else if(opener.UI_tabSearchFlights.strOutDate != null && opener.UI_tabSearchFlights.strOutDate != ""){
				var arrFlightDetails = opener.UI_tabSearchFlights.flightInfo[0];
				var tmpDateString = arrFlightDetails.arrivalDateValue;
				if (tmpDateString != null) {
					var tmpYear = tmpDateString.substring(0,2);
					var tmpMonth = tmpDateString.substring(2,4);
					var tmpDate = tmpDateString.substring(4,6);
					var dateString = tmpDate + tmpMonth + tmpYear;
					minDate = dateChk(dateString);
				}
				if (minDate == null || !minDate) {
					minDate = new Date();
				}
			} 
			
			if (opener.UI_tabSearchFlights.strOutDate != null && opener.UI_tabSearchFlights.strOutDate != "") {
				var deptFlightDetails = opener.UI_tabSearchFlights.flightInfo[0];
				var tmpArrDateString = deptFlightDetails.departureDateValue;
				if (tmpArrDateString != null) {
					var tmpYear = tmpArrDateString.substring(0,2);
					var tmpMonth = tmpArrDateString.substring(2,4);
					var tmpDate = tmpArrDateString.substring(4,6);
					var dateString = tmpDate + tmpMonth + tmpYear;
					maxDate = dateChk(dateString);
				}
				if (maxDate == null || !maxDate) {
					maxDate = new Date();
				}
			}
		} else if(opener.UI_reservation != null) {
			var allFltDetails = opener.UI_reservation.allFlightDetails;
			minDate = UI_IntlFlightDetails.getMaximumDateValue(allFltDetails);
			
			maxDate = UI_IntlFlightDetails.getMinimumDateValue(allFltDetails);
			
			
		}
		
		if (intLen > 0) {
			var i = 1;
			$("#tablPane1")
				.find("tr")
					.each(
						function() {
							$(this)
								.find("input")
									.each(
										function() {
											if (this.name == "pnrPaxFltDepartureDate") {
												$("#" + this.id)
														.datepicker(
																{
																	minDate : minDate,
																	dateFormat : UI_commonSystem.strDTFormat,
																	changeMonth : 'true',
																	changeYear : 'true',
																	showButtonPanel : 'true'
																});
												i++;
											}
											if (this.name == "pnrPaxFltArrivalDate") {
												$("#" + this.id)
														.datepicker(
																{
																	maxDate : maxDate,
																	dateFormat : UI_commonSystem.strDTFormat,
																	changeMonth : 'true',
																	changeYear : 'true',
																	showButtonPanel : 'true'
																});
												i++;
											}
										}
									);
					   }
				  );
		}
	}
	
	UI_IntlFlightDetails.IntlFlightDetailsOnChange = function() {
		if (opener.UI_makeReservation != null) {
			opener.UI_makeReservation.tabStatus.tab2 = false;
			opener.UI_makeReservation.tabStatus.tab3 = false;
		}
		
	}
	
	UI_IntlFlightDetails.getMinimumDateValue = function(fltArray) {
		var tmpMinDate = null;
		var arrDeptDate = new Array();
		var arrLength = fltArray.length;
		var count = 0;
		var tmpDateString = null;
		var tmpTimeString = null;
		var minDate = null;
		
		for (var i=0; i < arrLength; i++) {
			var tmpFlight = fltArray[i];
			if (tmpFlight.status == "CNF" && tmpFlight.departureDateLong != null) {
				arrDeptDate[count] = tmpFlight.departureDateLong;
				count++;
			}
		}
		if (arrDeptDate.length > 0) {
			tmpMinDate = Math.min.apply (null, arrDeptDate);
			if (tmpMinDate != null) {
				for (var i=0; i < arrLength; i++) {
					var tmpFlight = fltArray[i];
					if (tmpFlight.status == "CNF" && tmpFlight.departureDateLong != null && tmpMinDate == tmpFlight.departureDateLong) {
						tmpDateString = tmpFlight.departureDate;
						tmpTimeString = tmpFlight.departureTime;
					}
				}
			}
			if (tmpDateString != null) {
				var arrtDateArr = tmpDateString.split('/');
				var tmpTimeArr = tmpTimeString.split(':');
				minDate = new Date(arrtDateArr[2], arrtDateArr[1]-1, arrtDateArr[0], tmpTimeArr[0], tmpTimeArr[1]);
			}
		}
		if (minDate == null) {
			minDate = new Date();
		}
		return minDate;
	}
	
	UI_IntlFlightDetails.getMaximumDateValue = function(fltArray) {
		var tmpMaxDate = null;
		var arrArrDate = new Array();
		var arrLength = fltArray.length;
		var count = 0;
		var tmpDateString = null;
		var tmpTimeString = null;
		var maxDate = null;
		
		for (var j=0; j<arrLength;j++) {
			var tmpFlight = fltArray[j];
			if (tmpFlight.status == "CNF" && tmpFlight.arrivalDateLong != null) {
				arrArrDate[count] = tmpFlight.arrivalDateLong;
				count++;
			}
		}
		if (arrArrDate.length > 0) {
			tmpMaxDate = Math.max.apply(null,arrArrDate);
			for (var j=0; j<arrLength;j++) {
				var tmpFlight = fltArray[j];
				if (tmpFlight.status == "CNF" && tmpFlight.arrivalDateLong != null && tmpMaxDate == tmpFlight.arrivalDateLong) {
					tmpDateString = tmpFlight.arrivalDate;
					tmpTimeString = tmpFlight.arrivalTime;
				}
			}
			if (tmpDateString != null) {
				var arrtDateArr = tmpDateString.split('/');
				var arrTimeArr = tmpTimeString.split(':');
				maxDate = new Date(arrtDateArr[2], arrtDateArr[1]-1, arrtDateArr[0], arrTimeArr[0], arrTimeArr[1]);
			}
		}
		if (maxDate == null) {
			maxDate = new Date();
		}
		return maxDate;
	}
	