//validations.js
//Sudheera Liyanage

function validateUSPhone( strValue ) {
	var objRegExp  = /^\([1-9][0-9]{2}\)\s?[0-9]{3}\-[0-9]{4}$/;

  	//check for valid us phone with or without space between
  	//area code
  	return objRegExp.test(strValue);
}

function  isNumeric( strValue ) {
	var objRegExp  =  /(^-?[0-9][0-9]*\.[0-9]*$)|(^-?[0-9][0-9]*$)|(^-?\.[0-9][0-9]*$)/;

  	//check for numeric characters
  	return objRegExp.test(strValue);
}

function validateInteger( strValue ) {
	var objRegExp  = /(^-?[0-9][0-9]*$)/;

  	//check for integer characters
  	return objRegExp.test(strValue);
}

function validatePositiveInteger( strValue ) {
	var objRegExp  = /(^[0-9][0-9]*$)/;

  	//check for positive integer characters
  	return objRegExp.test(strValue);
}


function validateValue(strValue,strMatchPattern ){
	var objRegExp = new RegExp( strMatchPattern);

 	//check if string matches pattern
 	return objRegExp.test(strValue);
}


function currencyValidate(strValue, nValue, nDecimal){
	var strNValue = "*"
	var strDValue = "*" ;
	var strPattern = "" ;
	
	if (nValue != ""){
		strNValue = "{0," + nValue + "}";
	}
	
	if (nDecimal != ""){
		strDValue = "{0," + nDecimal + "}";
	}
	strPattern = "(^-?[0-9]" + strNValue + "\\.[0-9]" + strDValue + "$)|(^-?[0-9]" + strNValue + "$)|(^-?\\.[0-9]" + strDValue + "$)";
	return validateValue(strValue, strPattern);
}

function isEmpty(s) {return (trim(s).length==0);}
function hasWSpace(s){return RegExp("^\s*$").test(s);}
function isAlpha(s){return RegExp("^[a-zA-Z]+$").test(s);}
function isInt(n){return RegExp("^[-+]?[0-9]+$").test(n);}
function isNumeric(n){return RegExp("^[0-9]+$").test(n);}
function isPositiveInt(n){return RegExp("^[+]?[0-9]+$").test( n );}
function isNegativeInt(n){return RegExp("^-[0-9]+$").test(n);}
function isAlphaNumeric(s) {return RegExp("^[a-zA-Z0-9-/]+$").test(s);}
function isAlphaNumericWithhyphen(s) {return RegExp("^[a-zA-Z0-9]+$").test(s);}  
//function isAlphaNumericWhiteSpace(s){return RegExp("^[a-zA-Z0-9-/ \w\s]+$").test(s);}
function isAlphaNumericWhiteSpace(s){return RegExp("^[a-zA-Z0-9-.;,/ \w\s]+$").test(s);}
function isAlphaNumericWhiteSpaceComma(s){return RegExp("^[a-zA-Z0-9-,/ \w\s]+$").test(s);}
function isAlphaWhiteSpace(s){return RegExp("^[a-zA-Z-/ \w\s]+$").test(s);}
function isDecimal(n){return RegExp("^[-+]?[0-9]+[.][0-9]+$").test(n);}
function isDecimalWithMaxDecimalPoints(n,d){
	if(isDecimal(n)){
		return RegExp("^[-+]?[0-9]+[.][0-9]{1," + d + "}$").test(n);
	}else{
		return false
	}
}

function isLikeDecimal(n){
	//window.status=isInt(n)+'||'+RegExp("^[-+]?[0-9]+[.]+$").test(n)+'||'+isDecimal(n);
	return isInt(n)||RegExp("^[-+]?[0-9]+[.]$").test(n)||isDecimal(n);
}
function isPositiveDecimal(n){return (RegExp("^[+]?[0-9]+[.][0-9]+$").test(n));}
function isNegativeDecimal(n){return RegExp("^-[0-9]+[.][0-9]+$").test(n);}

function checkTime(s){ return RegExp( "^[012][0-9]:[0-5][0-9]$" ).test( s );}
function checkDutyHours(s){ return RegExp( "^[0-9][0-9].[0-5][0-9]$" ).test( s );}
function checkEmail(s){return RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?([.][a-z0-9]([a-z0-9-]*[a-z0-9])?)+$").test(s);}

function attachEvents(){
    document.onkeypress=checkOnKeyPress;
    //document.oncontextmenu=stopRightMouse;
	document.body.onpaste=validatePaste;
}	


function checkOnKeyPress() {
    var e=event;
    var kCode=e.keyCode;
    var keys1=[34,39,60,62,94,126];
    //alert("validKeysOnly=" + kCode);

    if (kCode=="13"){
		onEnterKeyPressed();
		return;
	}
    keys1="_"+keys1.join("_")+"_";
    if(keys1.indexOf("_"+kCode+"_")!=-1)e.returnValue=false;
}


function validatePaste() {
	
    var val=window.clipboardData.getData("Text");
    var obj=event.srcElement
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
			window.clipboardData.setData("Text",removeInvalids(val));
        }
    }
}

function removeInvalids(val){
    var re1=/'|"/g;
    var re2=/<|>|~|\^/g;

    val=val.replace(re1,"");
    val=val.replace(re2,"-");
    return val;
}


function stopRightMouse(){
    var e=event;
    var obj=e.srcElement;
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
            return true;
        }
    }
    return false;
}

function doAfterLoad(){
	attachEvents();
}

function isError(e,msg,id){
    var blnErr=false;
    var val=getVal(id);
    var obj=getField(id);

    if(e=="NULL"){
        if (isEmpty(val))blnErr=true;
    }else if(e=="NOT_ALPHA"){
        blnErr=!isAlpha(val);
    }else if(e=="NOT_ALPHANUMERIC"){
        blnErr=!isAlphaNumeric(val);
    }else if(e=="NaN"){
            blnErr=(isNaN(val));
    }else if(e=="NOT_FLOAT"){
            blnErr=(!isNaN(val))&&(val.indexOf(".")!=-1);
    }else if(e.substr(0,6)=="MAXVAL"){
        blnErr=(Number(val)>Number(e.substr(6)));
	}else if(e.substr(0,6)=="MINVAL"){
        blnErr=(Number(val)<Number(e.substr(6)));
    }else if(e.substr(0,6)=="MAXLEN"){
        blnErr=(val.length>Number(e.substr(6)));
    }else if(e.substr(0,6)=="MINLEN"){
        blnErr=(val.length<Number(e.substr(6)));
    }else if(e.substr(0,6)=="WHITESPACE"){
        blnErr=hasWSpace();
    }else{
        alert("isErr:"+e+":"+"invalid error type" );
    }

    if (blnErr){
        alert(msg);
		if(obj[0]){
			if (obj.tagName=="SELECT"){
				obj.focus();
			}else{
				obj[0].focus()
			}
		}else if(!(obj.readOnly||obj.disabled||obj.type=='hidden'||obj.style.visibility=='hidden'||obj.style.display=='none')){
			obj.focus();
		}
    }else{
        setField(id,trim(val));
    }

    return blnErr;
}


function removeChars(str,chars){
	return str;	
	var strTmp=str;
	if(chars=='undefined'||chars==null||chars==""){
		chars="`!@#$%&*?[]{}()|\\/+=:.,;";
	}
	var arr=chars.split("");
	for(var i=0;i<arr.length;i++){
		strTmp=strTmp.replace(arr[i],"");
	}
	return strTmp;
}

function keyPressCheckEmail(){
	var obj=window.event.srcElement;
	if(!isEmpty(obj.value)&&!checkEmail(obj.value)){
		alert('Invalid Email Address');
		obj.focus();
	}
}
//TIME Validation............
function IsValidTime(timeStr) {
	// Checks if time is in HH:MM:SS AM/PM format.
	// The seconds and AM/PM are optional.
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
		//alert("Time is not in a valid format.");
		return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	
	if (second=="") { second = null; }
	//if (ampm=="") { ampm = null }
	if (hour < 0  || hour > 23) {
		//alert("Hour must be between 1 and 12. (or 0 and 23 for military time)");
		return false;
	}
	//if (hour <= 12 && ampm == null) {
	//if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time")) {
	//alert("You must specify AM or PM.");
	//return false;
	//   }
	//}
	//if  (hour > 12 && ampm != null) {
	//alert("You can't specify AM or PM for military time.");
	//return false;
	//}
	if (minute<0 || minute > 59) {
		//alert ("Minute must be between 0 and 59.");
		return false;
	}
	if (second != null && (second < 0 || second > 59)) {
		//alert ("Second must be between 0 and 59.");
		return false;
	}
	return false;
}

function numberKeyPress(e){
	var key,isCtrl;

	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}

	if(key==8||isCtrl){
		return true;
	}else{
		var keychar = String.fromCharCode(key);
	}

	return isInt(keychar);

}

function blockNonNumbers(obj, e, allowDecimal, allowNegative){
	var key;
	var isCtrl=false;
	var keychar;
	var reg;
		
	if(window.event){
		key=e.keyCode;
		isCtrl=window.event.ctrlKey
	}
	else if(e.which){
		key=e.which;
		isCtrl=e.ctrlKey;
	}
	
	if(isNaN(key))return true;
	
	keychar=String.fromCharCode(key);
	
	// check for backspace or delete, or if Ctrl was pressed
	if (key==8||isCtrl){
		return true;
	}
	
	var isInvalid=obj.value.length!=0&&(keychar=='-'||keychar=='+');
	if(isInvalid){
		return false;
	}

	reg=/\d/;
	var isFirstNeg=allowNegative?keychar=='-'&&obj.value.indexOf('-')==-1:false;
	var isFirstPos=keychar=='+'&&obj.value.indexOf('+')==-1;

	var isFirstD=allowDecimal?keychar=='.'&&obj.value.indexOf('.')==-1:false;
	
	return isFirstNeg||isFirstPos||isFirstD||reg.test(keychar);
}

function formatTime(strTime,forceFormat) {    
    var text = strTime.value;
    
    if(text.length == 0)
        return;
        
    var numericPart = '';
    var characterPart = '';
    var i;
    var current;
    
    // Break the text input into numeric and character parts for easier parsing			    
    for(i = 0; i < text.length; i++) {  
        current = text.charAt(i);			        
        
        if(isInt(current))
            numericPart = numericPart + current;
            
        if(isAlpha(current))
            characterPart = characterPart + current;
   }
   					   
   var formattedDate;
   var hour;
   var minute;
   var dayPart;
   
   // Handle AM/PM by looking for an a, otherwise treat as PM
    if(characterPart.indexOf('a') > -1 || characterPart.indexOf('A') > -1)
        dayPart = 'AM';
    else
        dayPart = 'PM';
     
    if(numericPart.length >= 4) {
		hour = numericPart.substring(0, 2);
		minute = numericPart.substring(2, 4);
	} else if(numericPart.length == 3) {	 
		hour = numericPart.substring(0, 1);
		minute = numericPart.substring(1, 3);
	} else if(numericPart.length == 2) {	 
		hour = numericPart.substring(0, 2);
		minute = '00';		
	} else if(numericPart.length == 1) {	 
		hour = numericPart.substring(0, 1);
		minute = '00';									
	} else {
		if(!forceFormat)return;
		// Just use the current hour
		var d = new Date();
		hour = d.getHours();	
		
		minute = '00';
	}	
	
	// Apply 24 hour logic
	if(hour > 12) {
		if(hour <= 24) {
			//hour -= 12;
			dayPart = 'PM';
		} else {
			// If the hour is still > 12 then the user has inputed something that doesn't exist
			if(!forceFormat)return;

			//else just use current hour
			hour = (new Date()).getHours();				
					
			if(hour > 12) {		
				//hour -= 12;
				dayPart = 'PM';
			} else {		
				dayPart = 'AM';
			}
		}
	}	
	
	if(hour == 0) {
		hour = 12;
		dayPart = 'AM';
	}
		
	 strTime.value = hour + ':' + minute + ' ' + dayPart;				 
}

function checkAlpha(obj,e,allowChars){
	var key;
	var isCtrl=false;
	var keychar;
	var reg;
		
	if(window.event){
		key=e.keyCode;
		isCtrl=window.event.ctrlKey
	}
	else if(e.which){
		key=e.which;
		isCtrl=e.ctrlKey;
	}
	
	if(isNaN(key))return true;
		
	// check for backspace or delete, or if Ctrl was pressed
	if (key==8||key==46||isCtrl){
		return true;
	}
	
	keychar=String.fromCharCode(key);

	if(allowChars&&allowChars!=null&&allowChars.indexOf(keychar)!=-1){
		return true;
	}

	return isAlpha(keychar);
}

function checkAlphaNumeric(obj,e,allowChars){
	try{
		var key;
		var isCtrl=false;
		var keychar;
		var reg;
			
		if(window.event){
			key=e.keyCode;
			isCtrl=window.event.ctrlKey
		}
		else if(e.which){
			key=e.which;
			isCtrl=e.ctrlKey;
		}
		
		if(isNaN(key))return true;
			
		// check for backspace or delete, or if Ctrl was pressed
		if (key==8||key==46||isCtrl){
			return true;
		}
		
		keychar=String.fromCharCode(key);
	
		if(allowChars&&allowChars!=null&&allowChars.indexOf(keychar)!=-1){
			return true;
		}
	
		return isAlpha(keychar)||isNumber(keychar);
	}catch(e){}
}

function checkName(obj,e){
	var allowChars="";
	return checkAlphaNumeric(obj,e,allowChars);
}

var showXBEMain="../private/showXBEMain.do";
var winParams=new Array();


function refineJS(strJS){
	var winCC=null;
	if(strJS&&strJS!=null){
		if(strJS.indexOf('../js/login/Login.js')!=-1){
			strJS="aMessage['code']='ERROR';aMessage['desc']='';";
		}
	}
	
	if(top.name=='winCC'){
		winCC=top;
	}else{
		if(top.opener&&!top.opener.closed){
			winCC=top.opener.top;
		}
	}
	
	if(winCC!=null){
		winCC.ResetTimeOut();
	}
	strJS=strJS.replace(/\r\n|\r|\n/g, '');
	return strJS;
}

function setTimeWithColon(txtField) {
	var strTime = txtField.value;
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			var mn = "00";
			if(strTime.length == 3 || strTime.length == 4)
				mn = strTime.substr(index,2);

			var hr = 0;
			if(strTime.length == 3) {
				hr =  strTime.substr(0,1); 
			}else {
				hr = strTime.substr(0,2); 
			}
			var timecolon = hr +":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}
	}
	return IsValidTime(strTime);
}


function positiveWholeNumberValidate(objTxt){
	var strValue = objTxt.value;
	if (!isPositiveInt(strValue)) {
		objTxt.value = strValue.substr(0,strValue.length - 1);
	}	
}

function KPValidateDecimel(objCon,s,f){
	var strLen = objCon.value.length;
	var strText = objCon.value;
	var blnVal = isLikeDecimal(strText);
	
	if(!blnVal){
		objCon.value=strText.substr(0,strLen-1);
	} 
}

function isValidPassword(pass) {
	var  charArray = new Array();
	var  strChar = "";
	var numCount = 0;
	var alphaCount = 0;

	var intMinLen = 8;
	var intMaxLen = 12;
	
	if(!isAlphaNumeric(pass)){
		return false;
	}
	if(pass.length < intMinLen  || pass.length > intMaxLen){
		return false;
	}
	for(var i= 0;i < pass.length; i++) {
		strChar = pass.substr(i,1); 
		if(isPositiveInt(strChar)){
			numCount++;
		}
	}
	if((numCount < 3) || (numCount > (pass.length - 3))){
		return false;
	}
	return true;
}

function isLessCurrentDate(StrDate){
	return (Date.parse(convertDFV(getServerCurrentDate())) > Date.parse(convertDFV(StrDate)));
}

function getClientCurrentDate() {
	var dateCurrentDate = new Date();
	return (dateCurrentDate.getMonth()+1) + "/" + dateCurrentDate.getDate() + "/" + dateCurrentDate.getFullYear();
}

function getServerCurrentDate() {
	return objTop.arrParams[0];
}

function compareDate(strFromDate,StrToDate){
	return (Date.parse(convertDFV(strFromDate)) > Date.parse(convertDFV(StrToDate)));
}

//Convert String to java script date format
function convertDFV(strDate) {
	if(strDate != null && strDate.length >= 10){
		return strDate.substr(3,2) + "/" + strDate.substr(0,2) + "/" + strDate.substr(6,4);
	}
	return "";
}

function groupFieldValidation(groupObj, jsonFieldToElementMapping, jsonGroupValidation) {
	var rVal = true;
	$.each(groupObj, function(key, fieldList){
		var valid = false;
		$.each(fieldList, function(index, fVal){
			if(trim($(jsonFieldToElementMapping[fVal]).val()) != ""){
				valid = true;
			}
		});
		
		if(!valid){
			showERRMessage(raiseError(jsonGroupValidation[key]));
			rVal = false;
		}
	});
	
	return rVal;
}

//functions related to get combinations from carrier codes

function createWordList(wordArray, strbase, remainingchars) {

	if(remainingchars.length == 1) { //base case: only one character remains, and can be arranged only one way
		wordArray[wordArray.length] = strbase + remainingchars[0]; //append one remaining character to string base
	} else {
		for(var j = 0; j < remainingchars.length; j++) {
			currchar = remainingchars[j]; //first character of remaining characters
			wordArray = createWordList(wordArray, strbase + remainingchars[j], remainingchars.slice(0, j).concat(remainingchars.slice(j+1, remainingchars.length))); //recursive call
		}
	}

	return wordArray; //returns the wordArray containing stbase plus all possible combinations of remainingchars
}

function getCombinations(inputText) {
	return createWordList(new Array(), "", inputText);
}

function getArrayFromStr(arr){
	var strArr = new Array();
	var spltArr = arr.split(",");
	for(var i=0;i<spltArr.length;i++){
		if(spltArr[i] != ''){			
			strArr.push(spltArr[i]);
		}
	}
	
	return strArr;
}

function nullHandler(val){
	if(val == null){
		return "";
	} else {
		return trim(val);
	}
}

function ValidateFlagText(textBoxObj,evt) {

    //skip events for space and control keys
   var keyID = evt.charCode;
   if ((keyID==8)||(keyID==13||keyID==46||keyID==0)||(keyID >47 && keyID <58)||(keyID >64 && keyID <91)||(keyID >96 && keyID <123)) {
	   return true;
   }
   else{
	   return false;
   }
}
