UI_CreditManagement.screenID = "SC_AXBE_032";
UI_CreditManagement.addEdit; 


/**
 * UI_CreditManagement for AgentTicket sUI related functionalities
 */
function UI_CreditManagement() {
}

/**
 * Onload function
 */
$( function() {

	$("#divDisplayAgentTktStock").decoratePanelWithAlign("Manage Credit");
	
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();	
	$('#btnSave').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();	
	
	$('#btnEdit').click(function() { UI_CreditManagement.editOnClick(); });
	$('#btnAdd').click(function() { UI_CreditManagement.AddOnClick(); });
	$('#btnSave').click(function() { UI_CreditManagement.saveClick(); });
	$('#btnReset').click(function() { UI_CreditManagement.resetonClick(); });
	$('#btnClose').click(function() { UI_CreditManagement.closeClick(); });	
	
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();
 
    $("#frmModify").disableForm(); 

});

/*
 * Close Window
 */
UI_CreditManagement.closeClick = function(){	
	window.close()
}

UI_CreditManagement.refreshGrid = function (){	
	$("#frmModify").disableForm();	
	UI_CreditManagement.clearEditArea();		
	$("#tblAgentTktStock").trigger("reloadGrid");//This is an  Event
}

UI_CreditManagement.resetonClick = function (){	
	if($("#tblAgentTktStock").getGridParam("selrow") != null){
		UI_CreditManagement.gridRowOnClick($("#tblAgentTktStock").getGridParam("selrow"));
	} else {
		$("#frmModify").clearForm();
		$('#frmModify').disableForm();		
		$("#tblAgentTktStock").setGridParam({page:1,postData:{}});		
		$("#tblAgentTktStock").trigger("reloadGrid");
	}	
}

UI_CreditManagement.editOnClick = function (){	
	$("#btnSave").enableButton();
	$("#frmModify .frm_editable").enableFormSelective();
	$("#tktStockLowerBound").attr("readonly", true);
	$("#tktStockSequenceName").attr("readonly", true);
	$("#tktStockIsCycle").attr("disabled", true);
	
}

UI_CreditManagement.gridRowOnClick = function (rowid){	
	$("#tblAgentTktStock").GridToForm(rowid,"#frmModify");
	
	var selectedRow = UI_CreditManagement.getSelectedRow();
	if(selectedRow['agentTicketStockTo.cycleSequence'] == 'Y'){
		$('#tktStockIsCycle').attr('checked',true);
	}else
	{
		$('#tktStockIsCycle').attr('checked',false);
	}
	
	$("#frmModify").disableForm();
	$("#btnEdit").enableButton();
}

UI_CreditManagement.AddOnClick = function (){	
	UI_CreditManagement.clearEditArea();
	$("#btnSave").enableButton();
	$("#frmModify").enableForm();
	$("#tktStockSequenceName").attr("readonly", true);
	$("#tktStockLowerBound").attr("readonly", false);	
	
	$("#tblAgentTktStock").resetSelection();	
}

UI_CreditManagement.saveClick = function (){	
	$('#frmModify').ajaxSubmit({ dataType: 'json', 
		beforeSubmit:UI_CreditManagement.beforeSaveFormSubmit , 
		success: UI_CreditManagement.afterSave		
	});
}

UI_CreditManagement.clearEditArea = function (){
	$("#frmModify").clearForm();
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();	
}

UI_CreditManagement.beforeSaveFormSubmit = function (formData, jqForm, options){
	if(!UI_CreditManagement.validateSave()){
		return false;
	}	
	var $jsonString = {};
	$jsonString[formData.length] = {};
	$jsonString[formData.length].name = 'agentTicketStockTo.agentCode';
	$jsonString[formData.length].value = strAgentCode;
	$.extend(formData, $jsonString);
	
	var $jsonString = {};
	$jsonString[formData.length] = {};
	$jsonString[formData.length].name = 'agentTicketStockTo.cycleSequence';
	$jsonString[formData.length].value =( ($('#tktStockIsCycle').is(':checked')) ? 'Y':'N');
	$.extend(formData, $jsonString);	
}

UI_CreditManagement.afterSave = function (response){
	if(response.msgType != 'Error'){
		var postData = {};		
		$("#frmModify").clearForm();		
		postData['agentCode']= strAgentCode;	
		
		$("#tblAgentTktStock").setGridParam({page:1,postData:postData});
	 	$("#tblAgentTktStock").trigger("reloadGrid");
	 	
	 	$("#frmModify").disableForm();
	 	$("#btnSave").disableButton();
	 	UI_message.showConfirmationMessage({messageText:arrError['saveRecoredSucces']});
	 	
	}else{
	    UI_message.showErrorMessage({messageText:response.message});
	}
}

UI_CreditManagement.validateSave = function (){	
	
	if($("#tktStockLowerBound").isFieldEmpty()){		
		UI_message.showErrorMessage({messageText:arrError['msgTStockLowerBoundEmpty']});
		return false;
	}
	if($("#tktStockUpperBound").isFieldEmpty()){
		UI_message.showErrorMessage({messageText:arrError['msgTStockUpperBoundEmpty']});		
		return false;
	}
	if($("#tktStockStatus").isFieldEmpty()){
		UI_message.showErrorMessage({messageText:arrError['msgTktStockStatusEmpty']});				
		return false;
	}
	
	var selectedRow = UI_CreditManagement.getSelectedRow();
	if(selectedRow != null){	
		if (parseInt(selectedRow['agentTicketStockTo.sequenceUpperBound'])> parseInt($("#tktStockUpperBound").val()) )
		{
			UI_message.showErrorMessage({messageText:arrError['msgTktStockUpperNewInvalid']});		
			return false;
		}
	}
	return true;
}

UI_CreditManagement.getSelectedRow = function (){
	if($("#tblAgentTktStock").getGridParam("selrow") != null){
		return $("#tblAgentTktStock").getRowData($("#tblAgentTktStock").getGridParam("selrow"));		
	}else
		return null;	
}


UI_CreditManagement.onAddEditDataPopulate = function (status){
	UI_CreditManagement.addEdit = status;
}

UI_CreditManagement.positiveInt = function (objTextBox){	
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isPositiveInt(strText);
	if(!blnVal){
		objTextBox.value =  strText.substr(0,strLen-1); 
		objTextBox.focus();
	}
}