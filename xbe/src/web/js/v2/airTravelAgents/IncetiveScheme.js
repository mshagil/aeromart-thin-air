	
	UI_Incentive.screenID = "SC_FACM_008";
	UI_Incentive.addedElement = false;
	UI_Incentive.addNewClicked = false;
	
	
		
	
	/**
	 * Onload function
	 */
	$(function() {
	
		var gridimgpath = '../../themes/default/images/jquery';
		var gridWidth = 880;
		
		//Scheme Name Title display and fade out
		$("#txtSchemeName").focus(function () {
	             $('#spnSchName').css('display','inline').fadeOut(1000);
	   	});
	   	
	   	//Scheme Name Title display and fade out
//		$("#schemePrdFrom").focus(function () {
//	             $('#spnPrdFrm').css('display','inline').fadeOut(1000);
//	   	});
	   	//Scheme Name Title display and fade out
//		$("#schemePrdTo").focus(function () {
//	             $('#spnSchPrdTo').css('display','inline').fadeOut(1000);
//	   	});
	   	//Scheme Name Title display and fade out
		$("#comments").focus(function () {
	             $('#spnComments').css('display','inline').fadeOut(1000);
	   	});
		
		//date picker calendar
		$("#searchFrom").datepicker({showOn: 'button', buttonImage: '../images/Calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
		$("#searchTo").datepicker({showOn: 'button', buttonImage: '../images/Calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
		$("#schemePrdFrom").datepicker({showOn: 'button', buttonImage: '../images/Calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
		$("#schemePrdTo").datepicker({showOn: 'button', buttonImage: '../images/Calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
				
		$("#searchFrom").blur(function() { UI_Incentive.dateOnBlur({id:0});});
		$("#searchTo").blur(function() { UI_Incentive.dateOnBlur({id:1});});
		$("#schemePrdFrom").blur(function() { UI_Incentive.dateOnBlur({id:2});});
		$("#schemePrdTo").blur(function() { UI_Incentive.dateOnBlur({id:3});});
		
		$("#divBasis").decoratePanelWithoutTitle();
		$("#divInclutions").decoratePanelWithoutTitle();
		$("#divDispIncentive").decoratePanelWithAlign("Add/Edit Incentives");
		$("#divResultsPanel").decoratePanelWithAlign("Results");
		$("#divSearchPanel").decoratePanelWithAlign("Search Incentive");
		$("#btnSearch").decorateButton();
		$("#btnAdd").decorateButton();
		$("#btnEdit").decorateButton();
		$("#btnDelete").decorateButton();
		$("#btnLinkAgent").decorateButton();
		$("#btnClose").decorateButton();
		$("#btnReset").decorateButton();
		$("#btnSave").decorateButton();
		createPopUps();
		
		//FIXME do the serach criteria check box click event
//		$('input#myId').change(function () {
//		    if($(this).attr("checked") === "true") {
//		        //do the stuff that you would do when 'checked'
//
//		        return;
//		    }
//		    //Here do the stuff you want to do when 'unchecked'
//		});
		/**********************************************
		* Incentive Slab Related 
		***********************************************/	
		//Incentive Slabs
		$("#tblIncSlabs").jqGrid({
			url:'showIncentiveSlab!searchIncSlabs.action', 
			datatype: "json",
			jsonReader : {
				  root: "rowsSlb", 
				  page: "pageSlb",
				  total: "totalSlb",
				  records: "recordsSlb",
				  repeatitems: false,					 
				  id: "0"				  
				}, 
			height:130,
			width: 370,
		   	colNames:['&nbsp;','&nbsp;','Start Value', 'End Value', '%','slabID','schemeID','version' ],
		   	colModel:[	   
		   	    {name:'id', width:20, align:"center" },
		   	    {name:'incentive.slabLable', width:60, align:"center", editable:true},
		   	    {name:'incentive.rateStartValue', 	editable:true, width:100, align:"right", editoptions:{maxlength:"10"}},
	   	        {name:'incentive.rateEndValue',  	editable:true, width:100, align:"right", editoptions:{maxlength:"10"}},
	   	        {name:'incentive.persentage', 	 	editable:true, width:50,  align:"right", editoptions:{maxlength:"2"}},
	   	        {name:'incentive.agentIncSlabID', 				   width:20,  align:"center",	hidden:true},
	   	        {name:'incentive.agentIncSchemeID',	 			   width:20,  align:"center",	hidden:true},
	   	        {name:'incentive.version', 						   width:20,  align:"center",	hidden:true}
	   	   			],
		   	imgpath: gridimgpath,
		   	multiselect: false,
			pager: jQuery('#divPager'),
			rowNum:10,						
			viewrecords: false,
			pginput:false,
			pgbuttons:false,
			recordtext:"",
		   	//caption: "Incentive Slabs",
			onSelectRow: function(rowid){ UI_Incentive.rowSlabClicked(rowid); }
		}).navGrid("#divPager",{refresh: true, edit: false, add: false, del: false, search: false})
		.navButtonAdd('#divPager',{
	        caption:"Add",  onClickButton: function(){ UI_Incentive.slabAddClicked();}, position:"left"})
	    .navButtonAdd('#divPager',{
	          caption:"Edit", onClickButton: function(id){ UI_Incentive.slabEditClicked();}, position:"center"})
	    .navButtonAdd('#divPager',{
	        caption:"Delete", onClickButton: function(id){ UI_Incentive.slabDeleteClicked();}, position:"right"});	
		
		$('#load_tblIncSlabs').hide();
		
		/**********************************************
		* Incentive Grid Related 
		***********************************************/
		//construct Incentive grid
		$("#tblIncentive").jqGrid({
			url:'showIncentive!searchIncentives.action', 
			datatype: "json",
			jsonReader : {
				  root: "rows", 
				  page: "page",
				  total: "total",
				  records: "records",
				  repeatitems: false,					 
				  id: "0"				  
				}, 
			height: 130,
			width: gridWidth,
			colNames:['&nbsp;', 'Scheme Name', 'Scheme Range', 'Status', 'Scheme Period', 'Date','Created by User',
				'ticketPriceComponent','incPromoBookings','incOwnTRFBookings','incRepAgentSales','incentiveBasis',
				'version','modifiedDate','incentiveID','schemePrdFrom','schemePrdTo','Comments'
				],
		   	colModel:[
		   	    {name:'Id', width:40, jsonmap:'id'},  
		   	    {name:'incentive.schemeName',index:'mealDescription',align:"left", width:60, jsonmap:'incentive.schemeName'}, 
		   	    {name:'incentive.range', align:"right",index:'mealName', width:50, jsonmap:'incentive.range'}, 
		   	    {name:'incentive.status', width:70, align:"center", jsonmap:'incentive.status'},
		   	    {name:'incentive.period', width:80, align:"center", jsonmap:'incentive.period'},
		   	    {name:'incentive.date', width:60, align:"center", jsonmap:'incentive.date',hidden:true},
		   	    {name:'incentive.createdBy', width:80, align:"center", jsonmap:'incentive.createdBy'},
		   	    {name:'incentive.ticketPriceComponent', width:225, align:"center",hidden:true, jsonmap:'incentive.ticketPriceComponent'},
		   	    {name:'incentive.incPromoBookings', width:225, align:"center",hidden:true, jsonmap:'incentive.incPromoBookings'},
		   	    {name:'incentive.incOwnTRFBookings', width:225, align:"center",hidden:true, jsonmap:'incentive.incOwnTRFBookings'},
		   	    {name:'incentive.incRepAgentSales', width:225, align:"center",hidden:true, jsonmap:'incentive.incRepAgentSales'},
		   	    {name:'incentive.incentiveBasis', width:225, align:"center",hidden:true, jsonmap:'incentive.incentiveBasis'},
		   	    {name:'incentive.version', width:225, align:"center",hidden:true, jsonmap:'incentive.version'},
		   	    {name:'incentive.modifiedDate', width:225, align:"center",hidden:true, jsonmap:'incentive.modifiedDate'},
		   	    {name:'incentive.agentIncSchemeID', width:225, align:"center",hidden:true, jsonmap:'incentive.agentIncSchemeID'},
		   	    {name:'incentive.schemePrdFrom', width:225, align:"center",hidden:true, jsonmap:'incentive.schemePrdFrom'},
		   	    {name:'incentive.schemePrdTo', width:225, align:"center",hidden:true, jsonmap:'incentive.schemePrdTo'},
		   	    {name:'incentive.comments', width:225, align:"center",hidden:true, jsonmap:'incentive.comments'}	   	         

		   	           ],
		   	imgpath: gridimgpath,
		   	multiselect: false,
			pager: jQuery('#incentvpager'),
			rowNum:10,						
			viewrecords: true,
		   	loadComplete: function (e){UI_Incentive.cancelClicked();},
			onSelectRow: function(rowid){ UI_Incentive.rowClicked(rowid); }
		}).navGrid("#incentvpager",{refresh: true, edit: false, add: false, del: false, search: false});
				
		// register event handler for buttons
		$('#btnAdd').click(function() {	UI_Incentive.addClicked(); });
		$('#btnEdit').click(function() { UI_Incentive.editClicked(); });
		$('#btnSave').click(function() { UI_Incentive.saveClicked(); }); 
		$('#btnReset').click(function() { UI_Incentive.resetClicked(); });
		$('#btnDelete').click(function() { UI_Incentive.deleteClicked(); });
		$('#btnSearch').click(function() { UI_Incentive.searchClicked(); });
		$('#btnLinkAgent').click(function() { UI_Incentive.linkAgnetClicked(); });
		$('#searchEffCheck').click(function () {UI_Incentive.searchChkboxClicked();});
		$('#chkTktPrice').click(function () {UI_Incentive.tktPriceChkboxClicked();});
		$('#chkToalFare').click(function () {UI_Incentive.toalFareChkboxClicked();});
		$('#chkBasisAll').click(function () {UI_Incentive.basisAllChkboxClicked();});
		$('#chkBasisFlwn').click(function () {UI_Incentive.basisFlwnChkboxClicked();});
		$('#chkPromoFare').click(function () {UI_Incentive.chkPromoFaresOnClick();})
		$('#chkTRFBkng').click(function () {UI_Incentive.chkTransferBookingOnClick();})
		$('#chkAgntRptSls').click(function () {UI_Incentive.chkAgentReportSalesOnClick();})

		$('#btnClose').click(function () {UI_Incentive.closeClicked();});
	});
	
	
	/**
	 * UI_Incentive for Incentive ui related functionalities
	 */
	function UI_Incentive(){}
	
	UI_Incentive.chkAgentReportSalesOnClick = function (){
		if($('#chkAgntRptSls').attr('checked')){
			$('#chkAgntRptSls').val('Y');
		}else {
			$('#chkAgntRptSls').val('N');
		}
	}
	
	UI_Incentive.chkTransferBookingOnClick = function (){
		if($('#chkTRFBkng').attr('checked')){
			$('#chkTRFBkng').val('Y');
		}else {
			$('#chkTRFBkng').val('N');
		}
	}
	
	UI_Incentive.chkPromoFaresOnClick = function (){
		if($('#chkPromoFare').attr('checked')){
			$('#chkPromoFare').val('Y');
		}else {
			$('#chkPromoFare').val('N');
		}
	}
	
	UI_Incentive.clearSlabs = function (){
		$("#tblIncSlabs").clearGridData();
	}
	UI_Incentive.rowSlabClicked = function(rowid){
		var selectedRow = $("#tblIncSlabs").getRowData(rowid);
		var bll = selectedRow['incentive.rateStartValue'];
	}
	
	/**
	 * row click event handler method. 
	 */
	UI_Incentive.rowClicked = function(rowid){
		
		var selectedRow = $("#tblIncentive").getRowData(rowid);
		var schemeId = selectedRow["incentive.agentIncSchemeID"];
		var tktPrice = selectedRow["incentive.ticketPriceComponent"];//F:Fare or T:Total
		var promoBkng = selectedRow["incentive.incPromoBookings"];
		var TRFBkng = selectedRow["incentive.incOwnTRFBookings"];
		var RptAgntSls = selectedRow["incentive.incRepAgentSales"];
		var incBasis = selectedRow["incentive.incentiveBasis"];//S:Slae or F:Flown
	
		$("#frmIncentive").clearForm();	
		
		if(tktPrice == 'F'){
			$('#chkTktPrice').attr('checked',true);
			$('#chkToalFare').attr('checked',false);
		}else{
			$('#chkTktPrice').attr('checked',false);
			$('#chkToalFare').attr('checked',true);
		}
		if(incBasis == 'S'){
			$('#chkBasisAll').attr('checked',true);
			$('#chkBasisFlwn').attr('checked',false);
		}else{
			$('#chkBasisAll').attr('checked',false);
			$('#chkBasisFlwn').attr('checked',true);
		}
		if(promoBkng == 'Y'){
			$('#chkPromoFare').attr('checked',true);			
		}else{
			$('#chkPromoFare').attr('checked',false);
		}
		if(TRFBkng == 'Y'){
			$('#chkTRFBkng').attr('checked',true);
		}else{
			$('#chkTRFBkng').attr('checked',false);
		}
		if(RptAgntSls == 'Y'){
			$('#chkAgntRptSls').attr('checked',true);
		}else{
			$('#chkAgntRptSls').attr('checked',false);
		}
		$("#frmIncentive").disableForm();
		$("#tblIncentive").GridToForm(rowid,"#frmIncentive");
			
		var strUrl  = "showIncentiveSlab!searchIncSlabs.action?";
	 	if(schemeId != "") {
	 		strUrl += "searchSchemeID=";  
	 		strUrl  += schemeId;
	 	}
	 	
	 	$("#tblIncSlabs").setGridParam({url:strUrl,page:1});
	 	$("#tblIncSlabs").trigger("reloadGrid");	 	
	 	
		$("#btnAdd").enableButton();
		$("#btnEdit").enableButton();
		$("#btnDelete").enableButton();
		$("#btnCancel").enableButton();
		$("#btnSave").disableButton();
		
	}
	
	/**
	 * new click event handler method.
	 */
	UI_Incentive.addClicked = function(){
		$("#btnAdd").blur();
		UI_Incentive.addNewClicked = true;
		$("#btnAdd").disableButton();
		$("#btnEdit").disableButton();
		$("#btnDelete").disableButton();
		$("#btnCancel").enableButton();
		$("#btnSave").enableButton();	
		
		$("#tblIncentive").resetSelection();
		$("#tblIncSlabs").clearGridData();
		var emptyData = new Array();
		$("#tblIncSlabs").addRowData(1,emptyData);
		$('#tblIncSlabs').setColProp('incentive.slabLable',{editable:true});
		$('#tblIncSlabs').setColProp('incentive.slabLable',{editoptions: {value: 'SLAB 1'}});  
		$("#tblIncSlabs").editRow(1);
		$("#tblIncSlabs").saveRow(1, false, 'clientArray');
		$('#tblIncSlabs').setColProp('incentive.slabLable',{editable:false}); 
		$("#tblIncSlabs").editRow(1);
		$("#frmIncentive").clearForm();
		$("#frmIncentive").enableForm();
		$("#txtSchemeId").attr("disabled", true);		
	}
	
	/**
	 * cancel click event handler method.
	 */
	UI_Incentive.cancelClicked = function(){
		$("#btnCancel").blur();
		UI_Incentive.clearSlabs();
		$("#btnAdd").enableButton();
		$("#btnEdit").disableButton();
		$("#btnDelete").disableButton();
		$("#btnSave").disableButton();
		
		$("#tblIncentive").resetSelection();
		$("#frmIncentive").clearForm();
		$("#frmIncentive").disableForm();
		UI_Incentive.addNewClicked = false;
	}
	
	/**
	 * edit click event handler method.
	 */
	UI_Incentive.editClicked = function() {
		UI_Incentive.addNewClicked = false;
		var id = $("#tblIncentive").getGridParam('selrow');	
		if (!id) {
			showCommonError("Error", arrError['recordNtSelectd']);
			UI_Incentive.cancelClicked();
			return false;
		}
		$("#btnEdit").blur();

		//$("#btnAdd").disableButton();
		//$("#btnEdit").disableButton();
		$("#btnDelete").enableButton();
		$("#btnReset").enableButton();
		$("#btnCancel").enableButton();
		$("#btnSave").enableButton();
		
		$("#frmIncentive").enableForm();
		
		var schemeFrom = $("#schemePrdFrom").val();
		var schemeTo = $("#schemePrdTo").val();
		var dtCurrDate = new Date();
		var dtFrom = new Date();
		dtFrom.setFullYear(schemeFrom.substring(6), schemeFrom.substring(3,5)-1, schemeFrom.substring(0,2));
		var dtTo = new Date();
		dtTo.setFullYear(schemeTo.substring(6), schemeTo.substring(3,5)-1, schemeTo.substring(0,2));
		if((dtFrom < dtCurrDate) && (dtTo < dtCurrDate)){
	 		$("#tblIncSlabs").disableForm();
	 		$("#schemePrdFrom").attr("disabled", true);
	 		$("#schemePrdTo").attr("disabled", true);
	 	}else if(dtFrom < dtCurrDate){
	 		$("#tblIncSlabs").disableForm();
	 		$("#schemePrdFrom").attr("disabled", true);
	 	}	
	}
	
	/**
	 * new click event handler method.
	 */
	UI_Incentive.saveClicked = function(){
		$("#btnSave").blur();
		
		$("#btnAdd").disableButton();
		$("#btnEdit").enableButton();
		$("#btnDelete").enableButton();
		$("#btnCancel").enableButton();
		UI_Incentive.linkAgnetClicked();
		hidePopUp(1);
		var slabRow = $("#tblIncSlabs").getGridParam("records");
		for(var i=0;i<slabRow;i++){		            
			$("#tblIncSlabs").saveRow(i, false, 'clientArray');
		}
		$('#frmIncentive').ajaxSubmit({ dataType: 'json', beforeSubmit:UI_Incentive.beforeSave ,  success: UI_Incentive.afterSave });
		return false;	
	}
	
	
	/**
	 * edit click event handler method.
	 */
	UI_Incentive.resetClicked = function(){
		$("#btnReset").blur();
		if($("#tblIncentive").getGridParam("selrow") != null){
			UI_Incentive.rowClicked($("#tblIncentive").getGridParam("selrow"));
			UI_Incentive.editClicked();
		} else {
			UI_Incentive.cancelClicked();
		}
	}
	 
	 /**
	  * search click event handler method.
	  */
	UI_Incentive.searchClicked = function(){
		
		$("#btnSearch").blur();
		
	 	var strUrl  = "showIncentive!searchIncentives.action?";
	 	if(!$("#searchScheme").isFieldEmpty()) {
	 		strUrl += "schemeName=" + $("#searchScheme").val();
	 	}
	 	if(!$("#searchStatus").isFieldEmpty()) {
	 		strUrl += "&status=" + $("#searchStatus").val();
	 	}
	 	if($('#searchEffCheck').attr('checked')){
	 		if(!$("#searchFrom").isFieldEmpty()){
	 			strUrl += "&searchFrom=" + $("#searchFrom").val();
	 		}
	 		if(!$("#searchTo").isFieldEmpty()){
	 			strUrl += "&searchTo=" + $("#searchTo").val();
	 		}	 		
	 	}
	 	$("#frmIncentive").clearForm();
	 	$("#tblIncSlabs").clearGridData();
	 	$("#tblIncentive").setGridParam({url:strUrl,page:1});
	 	$("#tblIncentive").trigger("reloadGrid");	
		
	} 	
	
	/**
	 * method to validate before save.
	 */
	UI_Incentive.beforeSave = function (formData, jqForm, options){
		if( $('#txtSchemeId').attr('disabled')){
			$jsonStr = {};
			$jsonStr.name = 'incentive.agentIncSchemeID'
			$jsonStr.value = $('#txtSchemeId').val();
			formData[formData.length] = $jsonStr;
		}
		var $jsonString = {};
		var formObjLength = formData.length;
		var currMaxRow = $("#tblIncSlabs").getGridParam("records");
			for(var i=0;i<currMaxRow;i++){
			var rowNum = i+1;
				$jsonString[1] = {};
				$jsonString[2] = {};
				$jsonString[3] = {};
				$jsonString[4] = {};
				$jsonString[5] = {};
				$jsonString[6] = {};            
				            
				$("#tblIncSlabs").saveRow(rowNum, false, 'clientArray');
				var gridData = $("#tblIncSlabs").getRowData(rowNum);
				$jsonString[1].name = 'incentive.slabLst['+ i +'].rateStartValue';
				$jsonString[1].value = gridData['incentive.rateStartValue'];
				$jsonString[2].name = 'incentive.slabLst['+ i +'].rateEndValue';
				$jsonString[2].value = gridData['incentive.rateEndValue'];
				$jsonString[3].name = 'incentive.slabLst['+ i +'].persentage';
				$jsonString[3].value = gridData['incentive.persentage'];
				$jsonString[4].name = 'incentive.slabLst['+ i +'].agentIncSlabID';
				$jsonString[4].value = gridData['incentive.agentIncSlabID'];
				$jsonString[5].name = 'incentive.slabLst['+ i +'].agentIncSchemeID';
				$jsonString[5].value = gridData['incentive.agentIncSchemeID'];
				$jsonString[6].name = 'incentive.slabLst['+ i +'].version';
				if (gridData['incentive.version']==""){$jsonString[6].value = -1;}else{$jsonString[6].value = gridData['incentive.version'];}
				formData[formData.length] = $jsonString[1];
				formData[formData.length] = $jsonString[2];
				formData[formData.length] = $jsonString[3];
				formData[formData.length] = $jsonString[4];
				formData[formData.length] = $jsonString[5];
				formData[formData.length] = $jsonString[6];				
			}
			var agents = ls.getselectedData();
			$jsonString[1] = {};
			$jsonString[1].name = 'incentive.agents';
			$jsonString[1].value = agents;
			formData[formData.length] = $jsonString[1];				 
			
		return UI_Incentive.validateSave(formData, jqForm, options);
		
	}
	  
	/**
	 * method to validate before save.
	 */
	UI_Incentive.validateSave = function(formData, jqForm, options){
		var currMaxRow = $("#tblIncSlabs").getGridParam("records");
		var minVal = 0;
		var maxVal = 0;
		var schemeFrom = $("#schemePrdFrom").val();
		var schemeTo = $("#schemePrdTo").val();
		
		
		if($("#txtSchemeName").val() == ""){
			showCommonError("Error", arrError['schmNameRqrd']);
			return false;
		}else if(!$('#chkTktPrice').attr('checked')&& !$('#chkToalFare').attr('checked')){
			showCommonError("Error", arrError['fareNtChekd']);
			return false;
		}else if($('#chkBasisAll').attr('checked')&& $('#chkBasisFlwn').attr('checked')){
			showCommonError("Error", arrError['basisNtChekd']);
			return false;
		}else if($("#schemePrdFrom").isFieldEmpty()){
			showCommonError("Error", arrError['fromDtEmpty']);			
			return false;
		}else if(!dateValidDate($("#schemePrdFrom").val()))	{
			showCommonError("Error", arrError['fromDtInvalid']);
			return false;
		}else if($("#schemePrdTo").isFieldEmpty()){
			showCommonError("Error", arrError['toDtEmpty']);
			return false;
		}else if(!dateValidDate($("#schemePrdTo").val())) {
			showCommonError("Error", arrError['toDtinvalid']);
			return false;
		}else if(currMaxRow < 1){
			return false;
		}else {
			for(var i=0;i<currMaxRow;i++){
				var rowNum = i+1;
				var tmpStrt = 0;
				var tmpEnd = 0;
				var gridData = $("#tblIncSlabs").getRowData(rowNum);
				if(gridData['incentive.rateStartValue']== ""){
					showCommonError("Error", arrError['slabStrtEmpty']);
					return false;
				}else{
					tmpStrt = gridData['incentive.rateStartValue'];
				}
				if(gridData['incentive.rateEndValue']== ""){
					showCommonError("Error", arrError['slabEndEmpty']);
					return false;
				}else{
					tmpEnd = gridData['incentive.rateEndValue'];
				}
				if(gridData['incentive.persentage']== ""){
					showCommonError("Error", arrError['slabPrcntgEmpty']);
					return false;
				}
				
				if(tmpEnd <= tmpStrt){
					showCommonError("Error", arrError['slabRngOvrLap']);
					return false;
				}
				if(minVal == 0){	
					minVal = tmpStrt;
				} else if(minVal >= tmpStrt){ 
					showCommonError("Error", arrError['slabRngOvrLap']);
					return false;
				}
				if(maxVal == 0){	
					maxVal = tmpEnd;
				} else if(maxVal >= tmpEnd){ 
					showCommonError("Error", arrError['slabRngOvrLap']);
						return false;
				}								
			}
			var dtCurrDate = new Date();
			var dtFrom = new Date();
			dtFrom.setFullYear(schemeFrom.substring(6), schemeFrom.substring(3,5)-1, schemeFrom.substring(0,2));
			var dtTo = new Date();
			dtTo.setFullYear(schemeTo.substring(6), schemeTo.substring(3,5)-1, schemeTo.substring(0,2));
			
			if(UI_Incentive.addNewClicked){
				if(dtFrom <= dtCurrDate ){
					showCommonError("Error", "From date should be a future date.");
					return false;
				}else if (dtTo <= dtCurrDate){
					showCommonError("Error", "To date should be a future date.");
					return false;
				}
			}
			
			return true;
		}	
	}
	
	/**
	 * method to execute after save.
	 */
	UI_Incentive.afterSave = function(response){
		if (response.success) {
			var rowId = $("#tblIncentive").getGridParam("selrow");
			$("#tblIncentive").FormToGrid(rowId,"#frmIncentive");
			$("#frmIncentive").clearForm();
			$("#frmIncentive").disableForm();		
			$("#btnSave").disableButton();
			$("#tblIncentive").trigger("reloadGrid");
			
			showCommonError(response.msgType,response.succesMsg);
			UI_Incentive.cancelClicked();			
		} else {			
			showCommonError(response.msgType,response.succesMsg);
		}
	}
	
	/**
	 * method to validate delete.
	 */
	UI_Incentive.deleteClicked = function (){
		$("#btnDelete").blur();
		$("#btnAdd").disableButton();
		var id = $("#tblIncentive").getGridParam('selrow');
		var schemeID = null;
		var version = null;
		
		if (id) { 
			var ret = $("#tblIncentive").getRowData(id); 
			schemeID = ret['incentive.agentIncSchemeID'];
			version = ret['incentive.version'];
		} else { 
			showCommonError("Error",arrError['recordNtSelectd']);
			return false;
		}
		jConfirm( deleteRecoredCfrm , 
			function (bln){
					if(bln){
						var url = 'showIncentive!deleteIncentive.action?incentive.agentIncSchemeID='+schemeID+'&incentive.version='+version;
						$.getJSON(url,function(response){
							UI_Incentive.afterDelete(response);  
						});
					}}
		);	
		
		return false;	 
	}
	
	/**
	 * method to validate after delete.
	 */
	UI_Incentive.afterDelete = function (response){
		if (response.success) {
			var rowId = $("#tblIncentive").trigger("reloadGrid");
			$("#btnDelete").disableButton();
			$("#frmIncentive").clearForm();
			UI_Incentive.cancelClicked();
			eval(succesMsg);
			eval(msgType);
			showCommonError(msgType,succesMsg);
		} else {
			eval(succesMsg);
			eval(msgType);
			showCommonError(msgType,succesMsg);	
		}
	}
	
	/**
	 * linkAgent click event handler method. 
	 */
	UI_Incentive.linkAgnetClicked = function(){
		showPopUp(1);
		PopUpData();
		ls.drawListBox();
		
	}
	
	/**
	 * addSlab click event handler method. 
	 */
	UI_Incentive.slabAddClicked = function(){
		var currMaxRow = $("#tblIncSlabs").getGridParam("records");
		if(currMaxRow == null || currMaxRow == ""){
			currMaxRow = 0;
		}	
		var emptyData = new Array();
		var colNames = $("#tblIncSlabs").getGridParam("colNames");
		for(var i=0;i<colNames.length;i++){
			emptyData[i] = "";
		}	
		if(UI_Incentive.addedElement || currMaxRow ==0) {	
			currMaxRow++;
			$("#tblIncSlabs").addRowData(currMaxRow,emptyData);
			$('#tblIncSlabs').setColProp('incentive.slabLable',{editable:true}); 
			var lable = 'SLAB '+ currMaxRow;
			$('#tblIncSlabs').setColProp('incentive.slabLable',{editoptions: {value: lable}}); 
			$("#tblIncSlabs").editRow(currMaxRow);
			$("#tblIncSlabs").saveRow(currMaxRow, false, 'clientArray');
			$('#tblIncSlabs').setColProp('incentive.slabLable',{editable:false}); 
			$("#tblIncSlabs").editRow(currMaxRow);
			$("#tblIncSlabs").setSelection(currMaxRow,true);
			
		} else {	
			$("#tblIncSlabs").addRowData(currMaxRow+1,emptyData);
			$('#tblIncSlabs').setColProp('incentive.slabLable',{editable:true}); 
			var lable = 'SLAB '+ (currMaxRow + 1);
			$('#tblIncSlabs').setColProp('incentive.slabLable',{editoptions: {value: lable}}); 
			$("#tblIncSlabs").editRow(currMaxRow+1);
			$("#tblIncSlabs").saveRow(currMaxRow+1, false, 'clientArray');
			$('#tblIncSlabs').setColProp('incentive.slabLable',{editable:false}); 
			$("#tblIncSlabs").editRow(currMaxRow+1);
			$("#tblIncSlabs").setSelection(currMaxRow+1,true);
			
			UI_Incentive.addedElement = true;
		}	
	}
	
	/**
	 * editSlab click event handler method. 
	 */
	UI_Incentive.slabEditClicked = function(){
		var selectedRow = $("#tblIncSlabs").getGridParam("selrow");
		if(selectedRow == null) {
			showCommonError("Error", arrError['selectSlab']);
			return false;
		}		
		$("#tblIncSlabs").editRow(selectedRow); 
	}
	
	/**
	 * deleteSlab click event handler method. 
	 */
	UI_Incentive.slabDeleteClicked = function(){
		var selectedRow = $("#tblIncSlabs").getGridParam("selrow");
		if(selectedRow == null){
			showCommonError("Error", arrError['recordNtSelectd']);
			return false;
		}
		if(selectedRow == 0){
			showCommonError("Error", "At least Scheme must has a Slab.");
			return false;
		}else{
			jConfirm( deleteRecoredCfrm , 
					function (bln){
							if(bln){
								$("#tblIncSlabs").delRowData(selectedRow);						
							}}
				);					
				return false;			
		}		
	}
	
	UI_Incentive.searchChkboxClicked = function () {	
	    if($('#searchEffCheck').attr("checked") != true) {
	    	$('#searchFrom').attr("disabled", true); 
	        $('#searchTo').attr("disabled", true);
	        return;
	    }
		$('#searchFrom').removeAttr("disabled");
		$('#searchTo').removeAttr("disabled");
	}
	
	UI_Incentive.tktPriceChkboxClicked = function () {	
		if($('#chkTktPrice').attr('checked')){
			$('#chkTktPrice').val('Y');
		}else {
			$('#chkTktPrice').val('N');
		}
	    if($('#chkTktPrice').attr("checked") == true) {
	    	$('#chkToalFare').removeAttr("disabled");
	        return;
	    }
	    $('#chkToalFare').attr("disabled", true);	
    	$('#chkToalFare').attr("checked", false);	
	}
	
	UI_Incentive.toalFareChkboxClicked = function () {		
	    if($('#chkToalFare').attr("checked") == true) {
	    	$('#chkTktPrice').removeAttr("disabled");
	        return;
	    }
	    $('#chkTktPrice').attr("disabled", true);
		if($('#chkToalFare').attr('checked')){
			$('#chkToalFare').val('Y');
		}else {
			$('#chkToalFare').val('N');
		}	
	}
	
	UI_Incentive.basisAllChkboxClicked = function () {		
	    if($('#chkBasisAll').attr("checked") == true) {
	    	$('#chkBasisAll').removeAttr("disabled");
	        return;
	    }
	    $('#chkBasisAll').attr("disabled", true);
		if($('#chkBasisAll').attr('checked')){
			$('#chkBasisAll').val('Y');
		}else {
			$('#chkBasisAll').val('N');
		}	
	}
	
	UI_Incentive.basisFlwnChkboxClicked = function () {		
	    if($('#chkBasisFlwn').attr("checked") == true) {
	    	$('#chkBasisAll').removeAttr("disabled");
	        return;
	    }
	    $('#chkBasisAll').attr("disabled", true);		
		if($('#chkBasisFlwn').attr('checked')){
			$('#chkBasisFlwn').val('Y');
		}else {
			$('#chkBasisFlwn').val('N');
		}
	}
	
	/**
	 * cancel click event handler method.
	 */
	UI_Incentive.closeClicked = function(){
		top.LoadHome();
	}
	
	/**
	 * create Pop Up window UI. 
	 */
	function PopUpData(){
		var html = '<table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">';
		
			html += '<tr><td colspan="2"><span id="spn1"></span></td></tr>';
			html += '<tr><td><input name="btnCancel" type="button" class="btnDefault" id="btnCancel" onClick="return POSdisable();" value="Cancel"></td>';
			html += '<td align="right"><input name="btnUpdate" type="button" class="btnDefault" id="btnUpdate" onClick="return POSdisable();" value="Update"></td>';
			html += '</tr></table>';
	
		DivWrite("spnPopupData",html);
		
	}
	
	/**
	 * onClick linkAgent. 
	 */
	function POSenable() {
		 
		showPopUp(1);
		PopUpData();
		ls.drawListBox();
	}
	
	/**
	 * close the Pop Up. 
	 */
	function POSdisable() {
		hidePopUp(1);
		
	}

	/**
	 * Auto Date generation
	 */
	UI_Incentive.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 : dateChk("searchFrom"); break;
			case 1 : dateChk("searchTo"); break;
			case 2 : dateChk("schemePrdFrom"); break;
			case 3 : dateChk("schemePrdTo"); break;
		}
	}
	
	