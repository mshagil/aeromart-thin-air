var isAllSelected = false;
var passengersList;
var selectedStatus = "";
var multipleStatusChecked = false;

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "PNR";
objCol1.headerText = "PNR";
objCol1.itemAlign = "left";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "22%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "PAX Name";
objCol2.headerText = "PAX Name";
objCol2.itemAlign = "left";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "6%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Origin";
objCol3.headerText = "Origin";
objCol3.itemAlign = "left";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "12%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Segment";
objCol4.headerText = "Segment";
objCol4.itemAlign = "left";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "6%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Status";
objCol5.headerText = "Status";
objCol5.itemAlign = "left";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "6%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "PAX type";
objCol6.headerText = "PAX type";
objCol6.itemAlign = "left";

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "6%";
objCol7.arrayIndex = 7;
objCol7.toolTip = "COS";
objCol7.headerText = "COS";
objCol7.itemAlign = "left";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "6%";
objCol8.arrayIndex = 8;
objCol8.toolTip = "BC";
objCol8.headerText = "BC";
objCol8.itemAlign = "left";

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "6%";
objCol9.arrayIndex = 9;
objCol9.toolTip = "Seat";
objCol9.headerText = "Seat";
objCol9.itemAlign = "left";

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "6%";
objCol10.arrayIndex = 10;
objCol10.toolTip = "SSR";
objCol10.headerText = "SSR";
objCol10.itemAlign = "left";

var objCol11 = new DGColumn();
objCol11.columnType = "CHECKBOX";
objCol11.width = "4%";
objCol11.arrayIndex = 11;
objCol11.toolTip = "Select";
objCol11.headerText = "Sel";
objCol11.itemAlign = "center";

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "6%";
objCol12.arrayIndex = 13;
objCol12.toolTip = "Paid";
objCol12.headerText = "Paid In<BR>Full";
objCol12.itemAlign = "center";
// ---------------- Grid

var objDG = new DataGrid("spnCheckinPassengers");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol12);
objDG.addColumn(objCol11);

objDG.width = "99%";
objDG.height = "380px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrCheckinPassengers;
objDG.seqNo = true;
objDG.seqNoWidth = "4%";
objDG.backGroundColor = "#ECECEC";
objDG.seqStartNo = 1;
objDG.pgnumRecTotal = totalNoOfRecords;
objDG.paging = false
objDG.rowClick = "gridClick";
objDG.displayGrid();


function selectAll(){
	var selectedPassengers = objDG.getSelectedColumn(10);
	if (!isAllSelected) {
		for (var i = 0; i < selectedPassengers.length; i++) {
				objDG.setCellValue(i, 10, "true");
		}
		isAllSelected = true;
	} else {
		for (var i = 0; i < selectedPassengers.length; i++) {
			objDG.setCellValue(i, 10, "false"); 	
		}
		isAllSelected = false
	}
}

 

function checkin(){
	selectedPassengersList();
	if(passengersList == ""){
		alert("Please select at least one record!");
		return;
	}
	if(selectedStatus == "CHK" || multipleStatusChecked){
		alert("Please select not checked in passengers only!");
		return;
	}
	
	var strAction ="showPassengerCheckin.action?hdnMode=CHECKIN&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();
}

function unCheckin(){
	selectedPassengersList();
	if(passengersList == ""){
		alert("Please select at least one record!");
		return;
	}
	if(selectedStatus == "NCK" || multipleStatusChecked){
		alert("Please select checked in passengers only!");
		return;
	}	
	var strAction ="showPassengerCheckin.action?hdnMode=UNCHECKIN&hdnFlightId="+fltId;
	getFieldByID("frmPassengerCheckin").action = strAction;
	getFieldByID("frmPassengerCheckin").submit();	
}

function selectedPassengersList(){	
	var selectedPassengers = objDG.getSelectedColumn(11);
	passengersList = "";
	selectedStatus = "";
	multipleStatusChecked = false;
	for (var i = 0; i < selectedPassengers.length; i++) {		
		if (selectedPassengers [i][1] == true) {
			passengersList += arrCheckinPassengers[i][12] + ",";
			if(selectedStatus == ""){
				selectedStatus = arrCheckinPassengers[i][5];
			} else if(selectedStatus != arrCheckinPassengers[i][5]){
				multipleStatusChecked = true;
			}
		}
	}
	setField("hdnPPFSIds", passengersList);
}
