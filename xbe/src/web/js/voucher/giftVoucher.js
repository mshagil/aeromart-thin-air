UI_giftVoucher.templateList = null;
UI_giftVoucher.templateListSalesPeriod = null;
UI_giftVoucher.agentList = null;
UI_giftVoucher.confirmedOA = false;
var arrError = new Array();
var voucherListToBuy = new Array();
var totalAmount = 0;
var totalQuantity = 0;

var voucherTemplates = "";
var voucherTemp = {};
var saveRequestSent = false;
var cancelRequestSent = false;
var emailRequestSent = false;
var printRequestSent = false;

$(document).ready(function(){
	UI_giftVoucher.ready();
});

function UI_giftVoucher(){
}

UI_giftVoucher.ready = function (){
	$("#btnClose").click(function(){top.LoadHome();});
	$("#voucherIDSrch").numeric();
	$("#issuedAgntSrch").alphaNumeric();
	$("#fnameSrch").alphaWhiteSpace();
	$("#lnameSrch").alphaWhiteSpace();
	$("#paxFirstName").alphaWhiteSpace();
	$("#paxLastName").alphaWhiteSpace();
	$("#quantity").numeric();
	$("#txtCardNumber").numeric();
	$("#txtCardHolderName").alphaWhiteSpace();
	$("#txtCardExpiry").numeric();
	$("#txtCardCVV").numeric();
	
	$("#btnAddToCart").hide();
	$("#btnAdd").hide();
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnReset").hide();
	$("#ccDetails").hide();
	$("#trOAPaymentProcess").hide();
	$("#trOAPayment").hide();
	$("#oaPyamentConfirm").hide();
	$("#trVoucherSearch").hide();
	$("#btnPay").hide();
	$("#selLanguage").val("en");
	$("#selLanguage").hide();
	$("#btnEmail").hide();
	$("#btnPrint").hide();
	$("#selLanguage").hide();
	$("#btnCancelConfirm").hide();
	$("tr.visible").hide();
			
	$("#btnReset").click(function(){
		UI_giftVoucher.formReset();
		$("#tabs").tabs({
		    disabled: [1]
		});
		$("#tabs").tabs({
		    active: 0
		});
		UI_giftVoucher.clearSearch();
		$("#btnAddToCart").show();
	});
	
	$("#btnAdd").click(function(){
		
		UI_giftVoucher.clearSearch();
		$("#btnAddToCart").show();
		$("#btnAdd").hide();
		
	});
	
	$("#btnAddToCart").click(function(){
		UI_giftVoucher.addToBuy();		
		
	});
	
	$("#btnIssue").click(function(){
		UI_giftVoucher.sellGiftVoucher();
		$("#btnCancel").hide();
		$("tr.visible").hide();
		$("#btnCancelConfirm").hide();
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#selLanguage").hide();
		$("#trVoucherSearch").show();
		$("#btnAddToCart").show();
	});
	
	$("#btnSave").click(function(){
		UI_giftVoucher.saveClick();
	});
	
	$("#btnPay").click(function(){
		UI_giftVoucher.payClick();
	});
	
	$("#btnCancel").click(function() {
		$("#btnCancelConfirm").show();
		$("tr.visible").show();
		$("#cancelRemarks").prop('disabled', false);
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#selLanguage").hide();

	});

	$("#btnCancelConfirm").click(function() {
		UI_giftVoucher.cancelVoucher();
	});

	$("#btnEmail").click(function() {
		UI_giftVoucher.emailVoucher();
	});

	$("#btnPrint").click(function() {
		UI_giftVoucher.printVoucher();
	});

	UI_giftVoucher.createGrid();
	
	$("#buttonSearch").click(function(){
		UI_giftVoucher.createGrid(); 
	});
	
	$("#validFrom").datepicker({
		showOn: "button",
		dateFormat: 'dd/mm/yy',
		buttonImage: "../images/Calendar_no_cache.gif",
		yearRange: "+0:+99",
		minDate: "0Y",
		buttonImageOnly: true,
		onSelect: function () {
            var selDate = $('#validFrom').datepicker('getDate');
            $('#validTo').datepicker('option', 'minDate', selDate);
        },
		buttonText: "Select date",
		changeMonth: true,
		changeYear: true,
		disabled: true
	});
	
	$("#validTo").datepicker({
		showOn: "button",
		dateFormat: 'dd/mm/yy',
		buttonImage: "../images/Calendar_no_cache.gif",
		yearRange: "+0:+99",
		minDate: "0Y",
		buttonImageOnly: true,
		buttonText: "Select date",
		changeMonth: true,
		changeYear: true,
		disabled: true
	});
//	
	UI_giftVoucher.getGiftVoucherScreenDetails();
	
	$("#voucherName").change(function (){UI_giftVoucher.populateVoucherTemplate();});
	$("#selVoucherAmount").change(function (){UI_giftVoucher.populateCurrencyDropDown();});
	$("#selVoucherCurrency").change(function (){UI_giftVoucher.populateValidityDropDown();});
	$("#selVoucherValidity").change(function (){UI_giftVoucher.populateVoucherNameDropDown();});
	$('input:radio[name=paymentType]').change(function () {UI_giftVoucher.onPaymentTypeChange();});
	$("#selCCType").fillDropDown( { dataArray:top.arrIPGs , keyIndex:1 , valueIndex:0, firstEmpty:false });
	$("#selCCType").change(function (){
		UI_giftVoucher.cardTypeOnChange();
	});
	$("#selAgent").change(function (){
		if (UI_giftVoucher.oaPaymentValidate()){
			UI_giftVoucher.getOAPayment(); 
		}		
	});	
	
	$("#tabs").tabs({
	    disabled: [1]
	});
	UI_giftVoucher.formDisable();
	
	$("#ccPyamentConfirm").click(function(){
		UI_giftVoucher.ccConfirmClick(); 
	});
	
	$("#ccPyamentReset").click(function(){
		UI_giftVoucher.ccResetClick(); 
	});
	
	$("#ccPyamentCancel").click(function(){
		UI_giftVoucher.ccCancelClick();
	});
	UI_giftVoucher.cardTypeOnChange();
	
}

UI_giftVoucher.getGiftVoucherScreenDetails = function () {
	$.ajax({
        url : 'showGiftVoucherDetail.action',
        dataType:"json",
        beforeSend :ShowPopProgress(),
        type : "POST",		           
		success : UI_giftVoucher.voucherDetailSuccess
     });
}

UI_giftVoucher.voucherDetailSuccess = function (response) {
	UI_giftVoucher.templateList = response.voucherTemplateList;
	UI_giftVoucher.templateListSalesPeriod = response.salesPeriodTemplateList;
	UI_giftVoucher.agentList = '<option value=""></option>' + response.reqPayAgents;
	UI_giftVoucher.populateTemplateDropDown(UI_giftVoucher.templateList);
	UI_giftVoucher.populateAgentDropDown();
	try { 
		eval(response.errorList);
	} catch (err){
		console.log("eval() error ==> errorList");
	}
	HidePopProgress();
}
//GiftVoucherCCPaymentAction
UI_giftVoucher.getCCPayment = function () {
	var data = {};
	data["paymentAmount"] = totalAmount;
	$.ajax({
        url : 'giftVoucherPayment!ccPayment.action',
        dataType:"json",
        data : data,
        beforeSend :ShowPopProgress(),
        type : "POST",		           
		success : UI_giftVoucher.getCCPaymentSuccess
     });
}

UI_giftVoucher.getCCPaymentSuccess = function (response) {
	$("#hdnFullAmount").val(response.fullPayment);
	$("#hdnTnxFee").val(response.ccTxnFee);
	$("#tnxFee").empty().append(response.ccTxnFee);
	$("#currencyCodeTxtTnxFee").empty().append(response.currencyCode);
	$("#fullAmount").empty().append(response.fullPayment);
	$("#currencyCodeFullAmount").empty().append(response.currencyCode);
	var strCCMsg = top.spMsgCC;					
	$("#divCCMsg").empty().append(strCCMsg);
	HidePopProgress();
}

UI_giftVoucher.getOAPayment = function () {
	var data = {};
	data["paymentAmount"] = totalAmount;
	data["agentCode"] = $("#selAgent").val();
	$.ajax({
        url : 'giftVoucherPayment!oaPayment.action',
        dataType:"json",
        data : data,
        beforeSend :ShowPopProgress(),
        type : "POST",		           
		success : UI_giftVoucher.getOAPaymentSuccess
     });
}

UI_giftVoucher.getOAPaymentSuccess = function (response) {
	$("#trOAPaymentProcess").show();
	$("#trOAPayment").show();
	$("#hdnFullAmount").val(response.paymentAmount);
	$("#oaAmount").empty().append(response.fullPayment);
	$("#oaCurrencyCode").empty().append(response.currencyCode);
	$("#oaCurrencyCodeDesc").empty().append(response.currencyCode);
	UI_giftVoucher.confirmedOA = true;
	HidePopProgress();
}

UI_giftVoucher.payClick = function (){
	if(UI_giftVoucher.formPaymentValidate()){
		if ( $('input:radio[name=paymentType]:checked').val()=="CC" ) {
			$("#trButtons").hide();
			$("#paymentTypes").hide();
			$("#ccDetails").show();
			UI_giftVoucher.getCCPayment();
		} else if ( $('input:radio[name=paymentType]:checked').val() == "OA" || $('input:radio[name=paymentType]:checked').val() == "CA") {
			$("#hdnFullAmount").val("");
			UI_giftVoucher.confirmAction();
		}
	}
}

UI_giftVoucher.saveClick = function (){
	if(UI_giftVoucher.formValidate()){
		$("#btnSave").hide();
		$("#btnPay").show();
		$("#tabs").tabs('enable', 1);
		$("#tabs").tabs('disable', 0); 
		$("#tabs").tabs({
		    active: 1
		});
		$("#trOAPaymentProcess").hide();
		$("#trOAPayment").hide();
		$("#oaAmount").empty();
		$("#oaCurrencyCode").empty();
		$("#oaCurrencyCodeDesc").empty();
		$("#selAgent").val("");
		$("#paymentType").attr("checked", false);
		
	}
}

UI_giftVoucher.confirmAction = function (){
	if (UI_giftVoucher.formValidate() && !saveRequestSent){
		saveRequestSent = true;	
		var template = JSON.parse($("#voucherName").val());
		var data = {};
		var index = 0;
		data["paymentMethod"] = $('input:radio[name=paymentType]:checked').val();	
		if($("#hdnFullAmount").val()=="" || $("#hdnFullAmount").val()==null){
			data["fulAmountInBase"] = totalAmount;
		} else {
			data["fulAmountInBase"] = $("#hdnFullAmount").val();
		}
		for(var i=0 ; i < voucherListToBuy.length ; i++) {	
			var entry = voucherListToBuy[i];
			var quantity = entry[0];
			var template = entry[1];
			var hdnTnxFeePerVoucher = $("#hdnTnxFee").val() / quantity;
			for (var j = 0 ; j < quantity ; j++) {				
				data["voucherList["+index+"].validFrom"] = entry[2];
				data["voucherList["+index+"].validTo"] = entry[3];
				data["voucherList["+index+"].amountInLocal"] = template.amountInLocal;
				data["voucherList["+index+"].amountInBase"] = template.amount;
				data["voucherList["+index+"].currencyCode"] = template.currencyCode;
				data["voucherList["+index+"].paxFirstName"] = $("#paxFirstName").val();
				data["voucherList["+index+"].paxLastName"] = $("#paxLastName").val();
				data["voucherList["+index+"].email"] = $("#email").val();
				data["voucherList["+index+"].mobileNumber"] = $("#mobileNumber").val();
				data["voucherList["+index+"].remarks"] = entry[4];
				data["voucherList["+index+"].templateId"] = template.voucherId;
				data["voucherList["+index+"].voucherPaymentDTO.paymentMethod"]=$('input:radio[name=paymentType]:checked').val();
				data["voucherList["+index+"].voucherPaymentDTO.cardType"] = $("#selCardType").val();
				data["voucherList["+index+"].voucherPaymentDTO.cardNumber"] = $("#txtCardNumber").val();
				data["voucherList["+index+"].voucherPaymentDTO.cardHolderName"] = $("#txtCardHolderName").val();
				data["voucherList["+index+"].voucherPaymentDTO.cardExpiry"] = $("#txtCardExpiry").val();
				data["voucherList["+index+"].voucherPaymentDTO.cardCVV"] = $("#txtCardCVV").val();
				data["voucherList["+index+"].voucherPaymentDTO.amountLocal"] = template.amountInLocal;
				data["voucherList["+index+"].voucherPaymentDTO.amount"] = template.amount;
				//data["voucherList["+index+"].voucherPaymentDTO.cardTxnFeeLocal"] = hdnTnxFeePerVoucher;
				data["voucherList["+index+"].voucherPaymentDTO.agentCode"] = $("#selAgent").val();
				data["voucherList["+index+"].voucherPaymentDTO.ipgId"] = $("#selCCType").val();
				index++;
			}			
		}
			
		$.ajax({
	        url : 'issueGiftVoucher.action',
	        dataType:"json",
	        beforeSend :ShowPopProgress(),
	        data : data,
	        type : "POST",
	        async : true,
			success : UI_giftVoucher.voucherSaveSuccess
		});		
	}
}

UI_giftVoucher.voucherSaveSuccess = function (response) {
	HidePopProgress();
	if (response.success){
		UI_giftVoucher.formReset();
		UI_giftVoucher.formDisable();
		UI_giftVoucher.ccResetClick();
		$("#tnxFee").empty();
		$("#currencyCodeTxtTnxFee").empty();
		$("#fullAmount").empty();
		$("#currencyCodeFullAmount").empty();
		$("#paymentTypes").show();
		$("#ccDetails").hide();
		$( "#tabs" ).tabs({ active: 0 });
		$("#trButtons").show();
		$("#trVoucherSearch").hide();
		showConfirmation(arrError["saveSuccess"]);
		UI_giftVoucher.createGrid();

	} else {
		showERRMessage(response.paymentError);
	}
	saveRequestSent = false;
}

UI_giftVoucher.sellGiftVoucher = function (){
	$("#btnSave").show();
	$("#btnCancel").show();
	$("#btnReset").show();
	UI_giftVoucher.formReset();
	UI_giftVoucher.formEnable();
}

UI_giftVoucher.formReset = function () {
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").html("");
	$("#quantity").val("1");
	$("#voucherName").val("");
	$("#selVoucherAmount").val("");
	$("#selVoucherCurrency").val("");
	$("#selVoucherValidity").val("");
	$("#currencyCode").html("");
	$("#paxFirstName").val("");
	$("#paxLastName").val("");
	$("#email").val("");
	$("#mobileNumber").val("");
	$("#remarks").val("");
	$("#selAgent").val("");
	$("#paymentType").attr("checked", false);
	$("#btnPay").hide();	
	$("#btnSave").show();
	resetSummaryPannel();
	var totalAmount = 0;
	var totalQuantity = 0;

}

UI_giftVoucher.formDisable = function () {
	$("#voucherName").prop('disabled', true);
	$("#quantity").prop('disabled', true);
	$("#selVoucherAmount").prop('disabled', true);
	$("#selVoucherCurrency").prop('disabled', true);	
	$("#selVoucherValidity").prop('disabled', true);
	$("#paxFirstName").prop('disabled', true);
	$("#paxLastName").prop('disabled', true);
	$("#email").prop('disabled', true);
	$("#mobileNumber").prop('disabled', true);
	$("#remarks").prop('disabled', true);
	$("#selCardType").prop('disabled', true);
	$("#txtCardNumber").prop('disabled', true);
	$("#txtCardHolderName").prop('disabled', true);
	$("#txtCardExpiry").prop('disabled', true);
	$("#txtCardCVV").prop('disabled', true);
	$("#selAgent").prop('disabled', true);
	UI_giftVoucher.populateTemplateDropDown(UI_giftVoucher.templateList);
	$("#btnPay").hide();	
	$("#tabs").tabs({
	    disabled: [1]
	});
	$("#tabs").tabs({
	    active: 0
	});
}

UI_giftVoucher.formEnable = function () {
	$("#voucherName").prop('disabled', false);	
	$("#quantity").prop('disabled', false);
	$("#selVoucherAmount").prop('disabled', false);
	$("#selVoucherCurrency").prop('disabled', false);	
	$("#selVoucherValidity").prop('disabled', false);
	$("#paxFirstName").prop('disabled', false);
	$("#paxLastName").prop('disabled', false);
	$("#email").prop('disabled', false);
	$("#mobileNumber").prop('disabled', false);
	$("#remarks").prop('disabled', false);
	$("#selCardType").prop('disabled', false);
	$("#txtCardNumber").prop('disabled', false);
	$("#txtCardHolderName").prop('disabled', false);
	$("#txtCardExpiry").prop('disabled', false);
	$("#txtCardCVV").prop('disabled', false);
	$("#selAgent").prop('disabled', false);
	$( "#tab" ).tabs( "enable", 1 );
	$("#tabs").tabs({
	    disabled: [1]
	});
	//UI_giftVoucher.populateTemplateDropDown(UI_giftVoucher.templateListSalesPeriod);
	UI_giftVoucher.populateDropDowns(UI_giftVoucher.templateListSalesPeriod);
	$("#btnPay").hide();	
}

UI_giftVoucher.populateDropDowns = function (templates) {
	var voucherAmounts = "";
	voucherAmounts += "<option value = '' ></option>";
	var amountPrevious = new Array();
	if(templates != null){
		for (i = 0; i < templates.length; i++){
			if($.inArray(templates[i].amountInLocal, amountPrevious)==-1){
				amountPrevious.push(templates[i].amountInLocal);
				voucherAmounts += "<option value = '" + JSON.stringify(templates[i]) + "' >" + templates[i].amountInLocal + '' + "</option>";
			}
		}
	}
	$('#selVoucherAmount').empty();
	$('#selVoucherAmount').append(voucherAmounts);
	$("#voucherName").empty();
	$("#selVoucherValidity").empty();
	$("#selVoucherCurrency").empty();
	$("#voucherName").val("");
	$("#selVoucherValidity").val("");
	$("#selVoucherCurrency").val("");	
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").html("");
	$("#currencyCode").html("");
}

UI_giftVoucher.populateCurrencyDropDown = function () {
	var amountSelected = $('#selVoucherAmount option:selected').text();
	var templates = UI_giftVoucher.templateListSalesPeriod;
	var voucherCurrencyList = "";
	var curPrevious = new Array();
	voucherCurrencyList += "<option value = '' ></option>";
	if(templates != null){
		for (i = 0; i < templates.length; i++){
			if (templates[i].amountInLocal == amountSelected){
				if($.inArray(templates[i].currencyCode, curPrevious)==-1){
					curPrevious.push(templates[i].currencyCode);
					voucherCurrencyList += "<option value = '" + JSON.stringify(templates[i]) + "' >" + templates[i].currencyCode + "</option>";
				}
			}
		}
	}
	$('#selVoucherCurrency').empty();
	$('#selVoucherCurrency').append(voucherCurrencyList);
	$("#voucherName").empty();
	$("#selVoucherValidity").empty();
	$("#voucherName").val("");
	$("#selVoucherValidity").val("");
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").html("");
	$("#currencyCode").html("");
}

UI_giftVoucher.populateValidityDropDown = function () {
	var templates =UI_giftVoucher.templateListSalesPeriod;
	var validity;
	var valdityPrevious = new Array();
	var amountSelected = $('#selVoucherAmount option:selected').text();
	var selectedCurrency =  $('#selVoucherCurrency option:selected').text();
	var voucherPeriods = "";
	voucherPeriods += "<option value = '' ></option>";
	if(templates != null){
		for (i = 0; i < templates.length; i++){
			if (templates[i].currencyCode == selectedCurrency && templates[i].amountInLocal == amountSelected){
				if($.inArray(templates[i].voucherValidity, valdityPrevious)==-1){
					valdityPrevious.push(templates[i].voucherValidity);
					switch(templates[i].voucherValidity) {
				    case 30:
				    	validity = "30 Days"
				        break;
				    case 180:
				    	validity = "180 Days"
				        break;
				    case 90:
				    	validity = "90 Days"
				        break;
				    case 365:
				    	validity = "1 Year"
				        break;
				    default:
				    	validity = "30 Days"
					} 
					voucherPeriods += "<option value = '" + JSON.stringify(templates[i]) + "' >" + validity + "</option>";
				}
			}
		}
	}
	$('#selVoucherValidity').empty();
	$('#selVoucherValidity').append(voucherPeriods);
	$("#voucherName").empty();
	$("#voucherName").val("");
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").html("");
	$("#currencyCode").html("");
}
UI_giftVoucher.populateVoucherNameDropDown = function() {
	var templates = UI_giftVoucher.templateListSalesPeriod;
	var validity;
	var namePrevious = new Array();
	var selectedValidity =  $('#selVoucherValidity option:selected').text();
	var amountSelected = $('#selVoucherAmount option:selected').text();
	var selectedCurrency =  $('#selVoucherCurrency option:selected').text();
	switch(selectedValidity) {
	    case "30 Days":
	    	validity = 30
	        break;
	    case "180 Days":
	    	validity = 180
	        break;
	    case "90 Days":
	    	validity = 90
	        break;
	    case "1 Year":
	    	validity = 365
	        break;
	    default:
	    	validity = 30
	} 
	var voucherNames = "";
	voucherNames += "<option value = '' ></option>";
	if(templates != null){
		for (i = 0; i < templates.length; i++){
			if (templates[i].voucherValidity == validity && templates[i].currencyCode == selectedCurrency && templates[i].amountInLocal == amountSelected){
				if($.inArray(templates[i].voucherName, namePrevious)==-1){
					namePrevious.push(templates[i].voucherName);
					voucherNames += "<option value = '" + JSON.stringify(templates[i]) + "' >" + templates[i].voucherName + "</option>";
				}
			}
		}
	}
	$('#voucherName').empty();
	$('#voucherName').append(voucherNames);
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").html("");
	$("#currencyCode").html("");
}

UI_giftVoucher.populateTemplateDropDown = function (templates) {
	var voucherTemplates = "";
	voucherTemplates += "<option value = '' ></option>";
	if(templates != null){
		for (i = 0; i < templates.length; i++){
			voucherTemplates += "<option value = '" + JSON.stringify(templates[i]) + "' >" + templates[i].voucherName + "</option>";
		}
	}
	$('#voucherName').empty();
	$('#voucherName').append(voucherTemplates);
}

UI_giftVoucher.populateAgentDropDown = function () {
	$("#selAgent").html(UI_giftVoucher.agentList);
}

UI_giftVoucher.populateVoucherTemplate = function () {
	var template = JSON.parse($("#voucherName").val());
	var validFrom = new Date();
	var validTo = validFrom.getTime() + template.voucherValidity*24*3600*1000;
	$("#amount").html(template.amountInLocal);
	$("#currencyCode").html(template.currencyCode);
	$("#validFrom").datepicker("setDate", validFrom);
	$("#validTo").datepicker("setDate", new Date(validTo));	
}

UI_giftVoucher.formatDate = function(value) {
	var hipDate = value.substr(0, 10);
	return hipDate.replace("-","/")	
}

UI_giftVoucher.onPaymentTypeChange = function() {
	if ( $('input:radio[name=paymentType]:checked').val()=="CC" ) {
		$("#selCCType").show();
		$("#selAgent").hide();
		$("#trOAPaymentProcess").hide();
		$("#trOAPayment").hide();
		$("#oaPyamentConfirm").hide();
		UI_giftVoucher.oaReset();
	} else if ( $('input:radio[name=paymentType]:checked').val() == "OA" ) {
		$("#selCCType").hide();
		$("#selAgent").show();
		$("#oaPyamentConfirm").show();
		UI_giftVoucher.ccResetClick();
	} else if ( $('input:radio[name=paymentType]:checked').val() == "CA" ) {
		$("#selCCType").hide();
		$("#selAgent").hide();
		$("#trOAPaymentProcess").hide();
		$("#trOAPayment").hide();
		$("#oaPyamentConfirm").hide();
		UI_giftVoucher.ccResetClick();
		UI_giftVoucher.oaReset();
	}
}

UI_giftVoucher.cardTypeOnChange = function() {
	var arr = top.mapCCTypeForPgw[$("#selCCType").val()];
	arr = (arr==null?[]:arr);
	$("#selCardType").fillDropDown( { dataArray: arr, keyIndex:0 , valueIndex:1, firstEmpty:true});
}


UI_giftVoucher.formValidate = function () {

	var paxFirstName = $("#paxFirstName").val();
	var paxLastName = $("#paxLastName").val();
	var email = $("#email").val();
	var mobileNumber = $("#mobileNumber").val();
	var quantity = $("#quantity").val();
	var remarks = $("#remarks").val();

	//var voucherTemplate = $("#voucherName").val();
	
	var length = voucherListToBuy.length;

	var valid = true;

	if (length == 0 || typeof length === "undefined") {
		showERRMessage(arrError["noSelect"]);
		valid = false;
	}

	if (trim(paxFirstName) == "" || trim(paxFirstName) == null) {
		showERRMessage(arrError["fnameEmpty"]);
		valid = false;
	} else if (!isAlphaWhiteSpace(trim(paxFirstName))) {
		showERRMessage(arrError["fnameInvalid"]);
		valid = false;
	}

	if (trim(paxLastName) == "" || trim(paxLastName) == null) {
		showERRMessage(arrError["lnameEmpty"]);
		valid = false;
	} else if (!isAlphaWhiteSpace(trim(paxLastName))) {
		showERRMessage(arrError["lnameInvalid"]);
		valid = false;
	}
	
	if (trim(email)=="" || trim(email)==null) {
		showERRMessage(arrError["emailEmpty"]);
		valid = false;
	} else if (!checkEmail(trim(email))) {
		showERRMessage(arrError["emailInvalid"]);
		valid = false;
	}
	if (trim(mobileNumber) == "" || trim(mobileNumber) == null) {
		showERRMessage(arrError["mobileNumberEmpty"]);
		valid = false;
	}
	if (remarks.length > 200){
		showERRMessage(arrError["remarksTooLong"]);
		valid = false;
	}
	return valid;
}

UI_giftVoucher.formPaymentValidate = function () {
	var valid = true;
	var paymentMethod =  $('input:radio[name=paymentType]:checked').val();
	if ( paymentMethod != "CA" && paymentMethod != "OA" && paymentMethod != "CC") {
		showERRMessage(arrError["paymentMethodEmpty"]);
		valid = false;
	}
	return valid;
}

UI_giftVoucher.ccDetailsValidate = function () {

	var selCardType = $("#selCardType").val();
	var txtCardNumber = $("#txtCardNumber").val();
	var txtCardHolderName = $("#txtCardHolderName").val();
	var txtCardExpiry = $("#txtCardExpiry").val();
	var txtCardCVV = $("#txtCardCVV").val();
	
	var valid = true;
	
	if (trim(selCardType)=="" || trim(selCardType)==null) {
		showERRMessage(arrError["ccTypeEmpty"]);
		valid = false;
	}
	
	if (trim(txtCardNumber)=="" || trim(txtCardNumber)==null){ 
		showERRMessage(arrError["ccNumberEmpty"]);
		valid = false;
	} else if (!$.validateInteger(trim(txtCardNumber)))  {
		showERRMessage(arrError["ccNumberInvalid"]);
		valid = false;
	}	

	if (trim(txtCardHolderName)=="" || trim(txtCardHolderName)==null){ 
		showERRMessage(arrError["ccNameEmpty"]);
		valid = false;
	} else if  (!isAlphaWhiteSpace(trim(txtCardHolderName))){
		showERRMessage(arrError["ccNameInvalid"]);
		valid = false;
	}
	
	if (trim(txtCardExpiry)=="" || trim(txtCardExpiry)==null){ 
		showERRMessage(arrError["ccExpiryEmpty"]);
		valid = false;
	} else if (!$.validateInteger(trim(txtCardExpiry)) || trim(txtCardExpiry).length != 4) {
		showERRMessage(arrError["ccExpiryInvalid"]);
		valid = false;
	}
	
	if (trim(txtCardCVV)=="" || trim(txtCardCVV)==null){ 
		showERRMessage(arrError["ccCvvEmpty"]);
		valid = false;
	} else if (!$.validateInteger(trim(txtCardCVV)) || (txtCardCVV.length != 3)) {
		showERRMessage(arrError["ccCvvInvalid"]);
		valid = false;
	}
	
	return valid;
}

UI_giftVoucher.createGrid = function(){
	
	var voucherDTOJsonToString = function(cellVal, options, rowObject){		
		return JSON.stringify(cellVal);		
	}
	UI_giftVoucher.formReset();
	var statusFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			if(cellVal=="I"){
				treturn = "ISSUED";
			} else if(cellVal=="B"){
				treturn = "BLOCKED";
			} else if(cellVal=="C"){
				treturn = "CANCELED";
			} else if(cellVal=="U"){
				treturn = "USED";
			} else if(cellVal=="E"){
				treturn = "EXPIRED";
			}
		}
		return treturn;
	}
	
	
	$("#GiftVoucherGridContainer").empty().append('<table id="giftVoucherGrid"></table><div id="giftVoucherPages"></div>')
	var data = {};
	data['giftVoucherSearchRequest.voucherId'] = $("#voucherIDSrch").val();
	data['giftVoucherSearchRequest.issuedAgent'] = $("#issuedAgntSrch").val();
	data['giftVoucherSearchRequest.email'] = $("#emailSrch").val();
	data['giftVoucherSearchRequest.firstName'] = $("#fnameSrch").val();
	data['giftVoucherSearchRequest.lastName'] = $("#lnameSrch").val();

	var voucherGrid = $("#giftVoucherGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchGiftVoucher.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :ShowPopProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(voucherGrid[0], "gridData", mydata.giftVoucherList);
			            		  voucherGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  HidePopProgress();
			              }
			           }
			        });
			    },
		    height: 200,
	
		    colNames: ['Voucher ID', 'Cus. First Name', 'Cus. Last Name', 'Cus. Email', 'Valid From', 'Valid To', 'Amount', 'Amount In Local', 'Currency Code', 'Issued User ID', 'Issued Date', 'Voucher Template ID', 'Voucher Status', 'Remarks', 'Cancel Remarks' , 'Redeemed Amount' , 'VoucherDTO'],
		    colModel: [
			    {name: 'voucherID',index: 'voucherID',jsonmap:'voucherDTO.voucherId', width: 70},
				{name: 'paxFirstName',index: 'paxFirstName', jsonmap:'voucherDTO.paxFirstName', sorttype: "string", width: 90},
			    {name: 'paxLastName',index: 'paxLastName', jsonmap:'voucherDTO.paxLastName', sorttype: "string", width: 90}, 
			    {name: 'email',index: 'email', jsonmap:'voucherDTO.email', sorttype: "string" , width: 120},
			    {name: 'validFrom', index: 'validFrom',jsonmap:'voucherDTO.validFrom', width: 70, hidden: true},
			    {name: 'validTo',index: 'validTo',jsonmap:'voucherDTO.validTo', width: 70},
			    {name: 'amountInBase', index: 'amountInBase',jsonmap:'voucherDTO.amountInBase', align:"right" ,width: 70, hidden: true},
			    {name: 'amountInLocal',index: 'amountInLocal', jsonmap:'voucherDTO.amountInLocal', align:"right", width: 100, sorttype: "string"},
			    {name: 'currencyCode',index: 'currencyCode', jsonmap:'voucherDTO.currencyCode', width: 100},
				{name: 'issuedUserId',index: 'issuedUserId', jsonmap:'voucherDTO.issuedUserId', width: 90},
				{name: 'issuedTime',index: 'issuedTime', jsonmap:'voucherDTO.issuedTime', hidden: true},
			    {name: 'templateId',index: 'templateId', jsonmap:'voucherDTO.templateId', hidden: true},
			    {name: 'status',index: 'status', jsonmap:'voucherDTO.status', width: 90, formatter:statusFomatter},
			    {name: 'remarks',index: 'remarks', jsonmap:'voucherDTO.remarks', hidden: true},
			    {name: 'cancelRemarks',index: 'cancelRemarks', jsonmap:'voucherDTO.cancelRemarks', hidden: true},
			    {name: 'redeemdAmount',index:'redeemdAmount', jsonmap:'voucherDTO.redeemdAmount', align:"right", hidden:true},
			    {name: 'voucherDTO',index:'voucherDTO', jsonmap:'voucherDTO' , formatter:voucherDTOJsonToString ,hidden:true},
		    ],
		    caption: "Voucher Details",
		    rowNum : 20,
			viewRecords : true,
			width : 920,
			pager: jQuery('#giftVoucherPages'),
			jsonReader: { 
				root: "giftVoucherList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#giftVoucherPages",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$("#giftVoucherGrid").click(function(){ UI_giftVoucher.giftVoucherGridOnClick(); });
}

UI_giftVoucher.giftVoucherGridOnClick = function(){
	
	UI_giftVoucher.formReset();
	$("tr.visible").show();
	$("#trVoucherSearch").hide();
	var rowId = $("#giftVoucherGrid").getGridParam('selrow');
	var rowData = $("#giftVoucherGrid").getRowData(rowId);
	$("#btnAddToCart").hide();
	UI_giftVoucher.populateVoucherDetails(rowData);
	
}

UI_giftVoucher.populateVoucherDetails = function(rowData){
	
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnReset").hide();
	$("#btnCancelConfirm").hide();
	UI_giftVoucher.formDisable();
	var voucherDTO = JSON.parse(rowData['voucherDTO']);
	if(voucherDTO.status !="C") {
		$("tr.visible").hide();
	}
	for (var i = 0; i < UI_giftVoucher.templateList.length; i++) {
		if (UI_giftVoucher.templateList[i].voucherId == voucherDTO.templateId) {
			$("#voucherName").val(JSON.stringify(UI_giftVoucher.templateList[i]));
		}
	}
	
	$("#validFrom").val(voucherDTO.validFrom);
	$("#validTo").val(voucherDTO.validTo);
	$("#amount").html(voucherDTO.amountInLocal);
	$("#paxFirstName").val(voucherDTO.paxFirstName);
	$("#paxLastName").val(voucherDTO.paxLastName);
	$("#email").val(voucherDTO.email);
	$("#mobileNumber").val(voucherDTO.mobileNumber);
	$("#currencyCode").html(voucherDTO.currencyCode);
	$("#remarks").val(voucherDTO.remarks);
	$("#cancelRemarks").val(voucherDTO.cancelRemarks);
	
	var status = voucherDTO.status;
	$("#btnCancel").show();
	$("#btnEmail").show();
	$("#btnPrint").show();
	$("#selLanguage").show();

	if (status != "I") {
		$("#btnCancel").hide();
		$("#btnEmail").hide();
		$("#btnPrint").hide();
		$("#selLanguage").hide();
	}
}

UI_giftVoucher.ccCancelClick = function() {
	$("#ccDetails").hide();
	$("#trButtons").show();
	UI_giftVoucher.ccResetClick();
	$("#paymentTypes").show();
}

UI_giftVoucher.ccResetClick = function() {
	$("#selCardType").val("");
	$("#txtCardNumber").val("");
	$("#txtCardHolderName").val("");
	$("#txtCardExpiry").val("");
	$("#txtCardCVV").val("");
	$("#txtCardAmount").val("");
}

UI_giftVoucher.oaReset = function() {
	$("#selAgent").val("");
	$("#trOAPaymentProcess").hide();
	$("#trOAPayment").hide();
	$("#oaPyamentConfirm").hide();
}

UI_giftVoucher.oaPaymentValidate = function (){
	var voucherTemplate = $("#voucherName").val();
	var selAgent = $("#selAgent").val();
	
	var valid = true;

	if (trim(voucherTemplate)=="" || trim(voucherTemplate)==null) {
		showERRMessage(arrError["nameEmpty"]);
		valid = false;
	}
	
	if (trim(selAgent)=="" || trim(selAgent)==null) {
		showERRMessage(arrError["oaAgentEmpty"]);
		valid = false;
	}
	return valid;
}

UI_giftVoucher.ccConfirmClick = function(){
	if(UI_giftVoucher.ccDetailsValidate){
		UI_giftVoucher.confirmAction();
	}
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}

UI_giftVoucher.cancelVoucher = function() {

	var rowId = $("#giftVoucherGrid").getGridParam('selrow');
	var rowData = $("#giftVoucherGrid").getRowData(rowId);

	var voucherObject = JSON.parse(rowData['voucherDTO']);
	var status = voucherObject.status;
	var voucherID = voucherObject.voucherId;
	var templateId = voucherObject.templateId;
	var cancelRemarks = $("#cancelRemarks").val();

	if (UI_giftVoucher.validateCancel(status, cancelRemarks) && !cancelRequestSent) {
		cancelRequestSent = true;
		var data = {};
		data["voucherDTO.voucherId"] = voucherID;
		data["voucherDTO.cancelRemarks"] = cancelRemarks;
		data["voucherDTO.templateId"] = templateId;
		$.ajax({
			url : 'cancelVoucher.action',
			dataType : "json",
			beforeSend :ShowPopProgress(),
			data : data,
			type : "POST",
			async : false,
			success : UI_giftVoucher.voucherUpdateSuccess(voucherID)
		});
		$("#voucherIDSrch").val(voucherID);
		UI_giftVoucher.createGrid();
		$("tr.visible").hide();
	}
}

UI_giftVoucher.voucherUpdateSuccess = function(voucherID) {
	UI_giftVoucher.formReset();
	UI_giftVoucher.formDisable();
	cancelRequestSent = false;	
	showConfirmation(arrError["cancelSuccess"]);
	$("#voucherId").hide();
	$("#lblVoucherId").hide();
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnCancelConfirm").hide();
	$("#btnReset").hide();
	$("#cancelRemarks").val("");
	HidePopProgress();
}

UI_giftVoucher.validateCancel = function(status, cancelRemarks) {

	if (status != "I") {
		showERRMessage(arrError["statusInvalid"]);
		return false;
	} else if (trim(cancelRemarks) == "" || trim(cancelRemarks) == null) {
		showERRMessage(arrError["emptyCancelRemarks"]);
		$("#cancelRemarks").focuss;
		return false;
	} else {
		return true;
	}
}

UI_giftVoucher.emailVoucher = function() {
	if (!emailRequestSent) {
		emailRequestSent = true;
		var rowId = $("#giftVoucherGrid").getGridParam('selrow');
		var rowData = $("#giftVoucherGrid").getRowData(rowId);
		var voucherObject = JSON.parse(rowData['voucherDTO']);
		var language = $("#selLanguage").val();
		var data = {};
		data["voucherDTO.validFrom"] =voucherObject.validFrom;
		data["voucherDTO.validTo"] = voucherObject.validTo;
		data["voucherDTO.amountInLocal"] = voucherObject.amountInLocal;
		data["voucherDTO.currencyCode"] = voucherObject.currencyCode;
		data["voucherDTO.paxFirstName"] = voucherObject.paxFirstName;
		data["voucherDTO.paxLastName"] = voucherObject.paxLastName;
		data["voucherDTO.email"] = voucherObject.email;
		data["voucherDTO.voucherId"] = voucherObject.voucherId;
		data["voucherDTO.templateId"] = voucherObject.templateId;
		data["voucherDTO.printLanguage"] = language;
		data["voucherDTO.remarks"] = voucherObject.remarks;
		
		$.ajax({
			url : 'emailVoucher.action',
			dataType : "json",
			beforeSend : ShowPopProgress(),
			data : data,
			type : "POST",
			async : true,
			success : UI_giftVoucher.voucherEmailSuccess
		});
	}
}

UI_giftVoucher.voucherEmailSuccess = function() {
	emailRequestSent = false;
	HidePopProgress();
	showConfirmation(arrError["emailSuccess"]);
}

UI_giftVoucher.printVoucher = function() {
	if (!printRequestSent) {
		printRequestSent = true;
		var rowId = $("#giftVoucherGrid").getGridParam('selrow');
		var rowData = $("#giftVoucherGrid").getRowData(rowId);
		var voucherObject = JSON.parse(rowData['voucherDTO']);
		var validFrom = voucherObject.validFrom;
		var validTo = voucherObject.validTo;
		var amountInLocal = voucherObject.amountInLocal;
		var currencyCode = voucherObject.currencyCode;
		var paxFirstName = voucherObject.paxFirstName;
		var paxLastName = voucherObject.paxLastName;
		var voucherId = voucherObject.voucherId;
		var templateId = voucherObject.templateId;
		var language = $("#selLanguage").val();

		var strUrl = "printVoucher.action" + "?validFrom=" + validFrom
				+ "&validTo=" + validTo + "&amountInLocal=" + amountInLocal
				+ "&currencyCode=" + currencyCode + "&paxFirstName="
				+ paxFirstName + "&paxLastName=" + paxLastName + "&voucherId="
				+ voucherId + "&templateId=" + templateId + "&languageCode="
				+ language;

		ShowPopProgress(),

		top.objCW = window.open(strUrl, "myWindow", $("#popWindow")
				.getPopWindowProp(630, 900, true));
		if (top.objCW) {
			top.objCW.focus()
		}
		UI_giftVoucher.voucherPrintSuccess();
	}
}

UI_giftVoucher.voucherPrintSuccess = function() {
	printRequestSent = false;
	HidePopProgress();
	showConfirmation(arrError["printSuccess"]);
}

UI_giftVoucher.clearDropDown = function(id) {
	var select = document.getElementById(id);
	var length = select.options.length;
	for (i = 1; i < length+1; i++) {
	  select.options[i] = null;
	}
}

UI_giftVoucher.clearSearch = function () {
	$("#voucherName").empty();
	$("#selVoucherValidity").empty();
	$("#selVoucherCurrency").empty();
	$("#voucherName").val("");
	$("#selVoucherValidity").val("");
	$("#selVoucherCurrency").val("");	
	$("#validFrom").val("");
	$("#validTo").val("");
	$("#amount").html("");
	$("#currencyCode").html("");
	$('#selVoucherAmount').val("");
	$("#remarks").val("");
	$("#quantity").val("1");
}

UI_giftVoucher.addToBuy = function() {
	
	if(UI_giftVoucher.validateAdd()){
		var template = JSON.parse($("#voucherName").val());
		var number = $("#quantity").val();
		var validFrom = $("#validFrom").val();
		var validTo = $("#validTo").val();
		var remarks = $("#remarks").val();
		var multi = parseFloat(template.amount*number).toFixed(2);
		totalAmount = parseFloat(parseFloat(totalAmount) +  parseFloat(multi)).toFixed(2);
		totalQuantity = parseInt(totalQuantity) + parseInt(number);
		voucherListToBuy.push([number,template,validFrom,validTo,remarks]);
		$("#totalQuantity").val(totalQuantity);
		$("#totalAmount").val(totalAmount);	
		$("#btnAddToCart").hide();
		$("#btnAdd").show();
		$("#summaryPannel").show();
	}	
}

UI_giftVoucher.validateAdd = function() {
	var temp = $("#voucherName").text();
	if(temp==""){
		showERRMessage(arrError["nameEmpty"]);
		return false;
	} else {
		return true;
	}
}

resetSummaryPannel = function () {
	$("#summaryPannel").hide();
	voucherListToBuy = new Array();
	totalAmount = 0;
	totalQuantity = 0;
	$("#totalQuantity").val("");
	$("#totalAmount").val("");	
	$("#btnAdd").hide();
	$("#quantity").val("1");
	
}
