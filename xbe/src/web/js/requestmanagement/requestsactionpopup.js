/**
 * @author suneth
 *
 */

var top_0 = document;
var top_1 = document;
var top_2 = document;
var requestId;
var statusCode;

if (window.top[2] != undefined && window.top[2].name.toUpperCase() == "BOTTOM"){
	errorLog = window.parent.top.errorLog;
	top_0 = window.top[0];
	top_1 = window.top[1];
	top_2 = window.top[2];
}else{
	top_0 = this.document;
	top_1 = this.document;
	top_2 = this.document;
	$("body").append(errHTML);
	var erroImgPath = "";
}		

$("#requestAction", top_0.document).live("click", function(){
	
	$("html, body", top_1.document).find("#inProgressRequests").remove();
	$("html, body", top_1.document).append($("#inProgressRequests").clone());
	
	$("#inProgressRequests", top_1.document).css({
		position:"absolute",
		top:"0px",
		right:"30px",
		zIndex:"1",
		display:"block",
		width:"650px",
		height:"30px"
	});
	
	createPopupGrid();
	
	$("#inProgressRequests", top_1.document).animate({height:"425px"},{duration:500});
	if ($("body", top_1.document).length>1)
		$("body", top_1.document).find("#inProgressRequests").remove();
	
	$(".inProgressRequestsClose", top_1.document).bind("click",function(){
		$(this).parent().parent().remove();
		$("html", top_1.document).find("#inProgressRequests").remove();
	});
	
	$("#btnToOpenActionPopup", top_1.document).bind("click",function(){

		openRequest();
		
	});
	
	$("#btnToCloseActionPopup", top_1.document).bind("click",function(){

		closeRequest();
		
	});
	
	$("#btnRejectActionPopup", top_1.document).bind("click",function(){

		rejectRequest();
		
	});
	
	disableFields();
	
});


createPopupGrid = function(){
	
	$("#requestsGridContainerPopup" , top_1.document).empty().append('<table id="requestsGridPopup"></table><div id="requestsPagesPopup"></div>')
	var data = {};
	data['queueId'] = '';
	data['status'] = 'INP';
	
	var requestGrid = $("#requestsGridPopup" , top_1.document).jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchRequests!searchRequestsForActioning.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :UI_commonSystem.showProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(requestGrid[0], "gridData", mydata.requestsList);
			            		  requestGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  UI_commonSystem.hideProgress();
			              }
			           }
			        });
			    },
		    height: 150,
		    colNames: ['Created By', 'Description', 'Queue', 'Status', 'createdDate', 'statusCode', 'requestId', 'queueId'],
		    colModel: [
		        
		        {name: 'createdBy',index: 'createdBy',jsonmap:'queueRequestDTO.createdBy', width: 100, align: "center"},
		        {name: 'description',index: 'description', jsonmap:'queueRequestDTO.description', width: 450, resizable: false, align: "left"},
		        {name: 'queueType', index: 'queueType',jsonmap:'queueRequestDTO.queueName', width: 150, resizable: false, align: "center"},
			    {name: 'status',index: 'status', jsonmap:'queueRequestDTO.status', width: 100, resizable: false, align: "center"},
			    {name: 'createdDate',index: 'createdDate', jsonmap:'queueRequestDTO.createdDate', sorttype: "date", hidden: true},
			    {name: 'statusCode',index: 'statusCode', jsonmap:'queueRequestDTO.statusCode', hidden: true},
			    {name: 'requestId',index: 'requestId', jsonmap:'queueRequestDTO.requestId', hidden: true},
			    {name: 'queueId', index: 'queueId',jsonmap:'queueRequestDTO.queueId', hidden: true}
			    
		    ],
		    onSelectRow: function(id){
		    	var rowData = $("#requestsGridPopup" , top_1.document).getRowData(id);
			    requestId = rowData.requestId;
			    enableFields();
			},
		    
		    caption: "Inprogress Requests",
		    rowNum : 20,
			viewRecords : true,
			width : 640,
			pager: jQuery('#requestsPagesPopup' , top_1.document),
			jsonReader: { 
				root: "requestsList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid(("#requestsPagesPopup" , top_1.document),{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});
	
}

updateRequest = function(){
	

	if($("#remarkInprogressRequests", top_1.document).val() == ''){

		alert("Remark Can't be blank!");
		return;

	}
	
	var data = {};
	
	data['requestId'] = requestId;
	data['status'] = statusCode;
	data['remark'] = $("#remarkInprogressRequests", top_1.document).val();

	$.ajax({
		
        url : 'requestManagement!actionRequest.action',
        data: data,
        dataType:"json",
        beforeSend :UI_commonSystem.showProgress(),
        type : "POST",		           
		   success: function(response){

			   createPopupGrid();
			   $("#remarkInprogressRequests", top_1.document).val("");
			   UI_commonSystem.hideProgress();
			   showConfirmation("Request Updated Successfully.");
			   disableFields();
			   
		   },
		   error: function(response){
			   
			   UI_commonSystem.hideProgress();
			   showERRMessage("TAIR-91002: Cannot process your request, please try again later");
			   
		   }
	
     });
	
	statusCode = "";
	
	return true;
	
}

openRequest = function(){
	statusCode = "OPEN";
	updateRequest();
}

closeRequest = function(){
	statusCode = "CLSE";
	updateRequest();
}

rejectRequest = function(){
	statusCode = "RJCT";
	updateRequest();
}

disableFields = function(){
	$("#remarkInprogressRequests", top_1.document).prop('disabled', true);
	$("#btnRejectActionPopup", top_1.document).prop('disabled', true);
	$("#btnToCloseActionPopup", top_1.document).prop('disabled', true);
	$("#btnToOpenActionPopup", top_1.document).prop('disabled', true);
}

enableFields = function(){
    $("#remarkInprogressRequests", top_1.document).prop('disabled', false);
	$("#btnRejectActionPopup", top_1.document).prop('disabled', false);
	$("#btnToCloseActionPopup", top_1.document).prop('disabled', false);
	$("#btnToOpenActionPopup", top_1.document).prop('disabled', false);
}