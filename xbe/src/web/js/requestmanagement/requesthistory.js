/**
 * @author suneth
 *
 */

$(document).ready(function(){

	$("#btnClose").click(function(){window.close();});	
	createRequestHistoryGrid(urlParam('requestId'));
	
});

createRequestHistoryGrid = function(requestId){
	
	var dateFormatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			treturn = $.datepicker.formatDate('dd-M-yy', new Date(cellVal.split('T')[0]));
		}
		return treturn;
	}	
	
	$("#requestHistoryGridContainer").empty().append('<table id="requestHistoryGrid"></table><div id="requestHistoryPages"></div>')
	var data = {};
	data['requestId'] = requestId;
	var requestGrid = $("#requestHistoryGrid").jqGrid({		
			datatype: function(postdata) {
		        $.ajax({
			           url : 'searchRequests!searchRequestHistory.action',
			           data: $.extend(postdata , data),
			           dataType:"json",
			           beforeSend :UI_commonSystem.showProgress(),
			           type : "POST",		           
					   complete: function(jsonData,stat){	        	  
			              if(stat=="success") {
			            	  mydata = eval("("+jsonData.responseText+")");
			            	  if (mydata.msgType != "Error"){
			            		  $.data(requestGrid[0], "gridData", mydata.requestsList);
			            		  requestGrid[0].addJSONData(mydata);
			            	  }else{
			            		  alert(mydata.message);
			            	  }
			            	  UI_commonSystem.hideProgress();
			              }
			           }
			        });
			    },
		    height: 200,
		    colNames: ['Modified Date', 'Modified By', 'Status From', 'Status To', 'Action', 'Remark'],
		    colModel: [
		               
			    {name: 'modifiedDate',index: 'modifiedDate',jsonmap:'requestHistoryDTO.timestamp', width: 100 , resizable: false, sorttype: 'date', sortorder: 'desc', align: "center", formatter:dateFormatter},
			    {name: 'modifiedBy', index: 'modifiedBy',jsonmap:'requestHistoryDTO.userId', width: 100, resizable: false, align: "center"},
			    {name: 'statusFrom',index: 'statusFrom',jsonmap:'requestHistoryDTO.statusFrom', width: 100, resizable: false, align: "center"},
			    {name: 'statusTo', index: 'statusTo',jsonmap:'requestHistoryDTO.statusTo' ,width: 100 , resizable: false, align: "center"},
			    {name: 'action',index: 'action', jsonmap:'requestHistoryDTO.action', width: 100 , resizable: false, align: "center"},
			    {name: 'remark',index: 'remark', jsonmap:'requestHistoryDTO.remark', width: 410, resizable: false, align: "left"}
			    
		    ],
		    caption: "Request History",
		    rowNum : 20,
			viewRecords : true,
			width : 890,
			pager: jQuery('#requestHistoryPages'),
			jsonReader: { 
				root: "requestHistoryList",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
		    
	}).navGrid("#requestHistoryPages",{refresh: true, edit: false, add: false, del: false, search: false , shrinkToFit:false , forceFit:true});

}	

urlParam = function(name){
    var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
    
    if(results != null){
    	
    	return results[1] || 0;
    	
    }else{
    	
    	return null;
    	
    }
    
}