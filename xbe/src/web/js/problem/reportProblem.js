		
	var screenId="SC_RESV_CC_025";

	function setPageEdited(isEdited){
		top.pageEdited = isEdited;
	}
	
	function closeClick() {
		if (top.loadCheck(top.pageEdited)){
			setPageEdited(false);
			top.strSearchCriteria = "";
			top.LoadHome();
		}
	}
	
	function beforeUnload() {
		if ((top.objWindow) && (!top.objWindow.closed)) {
			top.objWindow.close();
		}
	}	

	function winOnLoad(strMsg, msgType){		
		clearControls();
	}
	
	function reSetClick() {
		clearControls();
	}
	
	function reportClick() {
		if(validateReport()) {
			setField("hdnMode", "SAVE");
			document.forms[0].action = "showReportProblem.action";
			document.forms[0].submit();
		}		
	}
	
	function validateReport() {
		if(getValue("txtName") == "") {
			showERRMessage(arrError["namenull"]);
			getFieldByID("txtName").focus();
			return false;
		}
		if(getValue("txtEMail") == "") {
			showERRMessage(arrError["emailnull"]);
			getFieldByID("txtEMail").focus();
			return false;
		}
		if (!checkEmail(getValue("txtEMail"))){
			showERRMessage(arrError["invalidformat"]);
			getFieldByID("txtEMail").focus();
			return false;
		}
		if(getValue("txtSummary") == "") {
			showERRMessage(arrError["problemnull"]);
			getFieldByID("txtSummary").focus();
			return false;
		}
		if(getValue("txtProblem") == "") {
			showERRMessage(arrError["problemnull"]);
			getFieldByID("txtProblem").focus();
			return false;
		}
		return true;
	}
	
	function clearControls() {
		setField("txtName", "");
		setField("txtEMail", "");
		setField("txtProblem", "");
		setField("hdnMode", "");
	}
 
 function uploadClick() {
 	
 	var strFileName = "showFile!attach.action";
	var intHeigh = 160;
	var intWidth = 700;
	var intTop 	= (window.screen.height - intHeigh) / 2;
	var intLeft = (window.screen.width -  intWidth) / 2;
	var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeigh + ',resizable=no,top='+ intTop +',left=' + intLeft;
	top.objCWindow =  window.open(strFileName, "myWindow", strProp);	
	
 } 
 
