	var recNumPerPage = 20;
	var recstNo = top[0].intLastRec;
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "6%";
	objCol2.arrayIndex = 1;
	objCol2.toolTip = "Flight No" ;
	objCol2.headerText = "Flight<br>No";
	objCol2.itemAlign = "center";
	objCol2.sort = true;
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "9%";
	objCol3.arrayIndex = 2;
	objCol3.toolTip = "Depature Date" ;
	objCol3.headerText = "D.Date<br>local";
	objCol3.itemAlign = "center";
	objCol3.sort = true;
	
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "6%";
	objCol4.arrayIndex = 3;
	objCol4.toolTip = "Origin" ;
	objCol4.headerText = "Org.<br>";
	objCol4.itemAlign = "center";
	objCol4.sort = true;
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "6%";
	objCol5.arrayIndex = 4;
	objCol5.toolTip = "Destination" ;
	objCol5.headerText = "Dest.<br>";
	objCol5.itemAlign = "center";
	objCol5.sort = true;
		
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "16%";
	objCol6.arrayIndex = 5;
	//objCol6.toolTip = "Segments" ;
	objCol6.headerText = "Segments";
	objCol6.itemAlign = "left"	
	
	var objCol7 = new DGColumn();
	objCol7.columnType = "label";
	objCol7.width = "6%";
	objCol7.arrayIndex = 6;
	objCol7.toolTip = "Estimated Time Departure - Zulu" ;
	objCol7.headerText = "ETD<BR>&nbsp;&nbsp;&nbsp;zulu";
	objCol7.itemAlign = "center";
	objCol7.sort = true;	
	
	var objCol8 = new DGColumn();
	objCol8.columnType = "label";
	objCol8.width = "6%";
	objCol8.arrayIndex = 24;
	objCol8.toolTip = "Estimated Time Departure - Local" ;
	objCol8.headerText = "ETD<BR>local";
	objCol8.itemAlign = "center";
	
	var objCol10 = new DGColumn();
	objCol10.columnType = "label";
	objCol10.width = "6%";
	objCol10.arrayIndex =9;
	//objCol10.toolTip = "Overlap Flight" ;
	objCol10.headerText = "O.lap<br>Flight";
	objCol10.itemAlign = "center";
	
	var objCol15 = new DGColumn();
	objCol15.columnType = "label";
	objCol15.width = "9%";
	objCol15.arrayIndex = 13;
	//objCol15.toolTip = "Status" ;
	objCol15.headerText = "Status";
	objCol15.itemAlign = "center";
	
	
	// ---------------- Grid	
	
	var objDG = new DataGrid("spnInstances");	
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol7);
	objDG.addColumn(objCol8);
	objDG.addColumn(objCol10);
	objDG.addColumn(objCol15);
	
	objDG.width = "99%";
	objDG.height = "370px";
	objDG.headerBold = false;
	objDG.rowSelect = true;
	objDG.arrGridData = parent.flightData;
	objDG.seqNo = true;
	
	var seqSize = Number(recstNo) + (Number(recNumPerPage) - 1);
	if (new String(seqSize).length > 2) {
		objDG.seqNoWidth = "4%";
	} else {
		objDG.seqNoWidth = "3%";	
	}
	objDG.backGroundColor = "#ECECEC";
	objDG.seqStartNo = recstNo;
	objDG.pgnumRecTotal = parent.totalNoOfRecords; // remove as per return record size
	objDG.paging = true;
	objDG.pgnumRecPage = recNumPerPage;
	objDG.pgonClick = "gridNavigations";	
	objDG.rowClick = "gridClick";
	objDG.displayGrid();
	
	function setColors(strRowData){		
		var strColor = "";		
		var StartD = dateChk(strRowData[2]);		
		var Today = dateChk(getVal("hdnCurrentDate"));	
		if (CheckDates(StartD,Today)) { // passed date
			strColor = "#AAAAAA"; 
		} else if ((parseInt(strRowData[10])) < (parseInt(strRowData[11]) + parseInt(strRowData[12]))) { // overload
			strColor = "red";
		} else if ((strRowData[19] == "true") && (strRowData[15] != '&nbsp')) { // manually changed
			strColor = "blue"; 
		} else if (trim(strRowData[9]) != "&nbsp") { // overlap			
			strColor = "green"; 
		}
		return strColor;
	}	
	

	