var objWindow;
var screenId="UC_REPM_092";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtFromDate",strDate);break ;
		case "1" : setField("txtToDate",strDate);break ;
	}
}

function setPageEdited(isEdited){
	top.pageEdited = isEdited;
}

function beforeUnload(){
	if ((top.objCWindow) && (!top.objCWindow.closed))	{
		top.objCWindow.close();
	}
}
	
function closeClick() {
	if (top.loadCheck(top.pageEdited)){
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}



function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function winOnLoad() {
	top.ResetTimeOut();
	getFieldByID("txtFromDate").focus();
	
	if(top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate",strSearch[0]);
		setField("txtToDate",strSearch[1]);
		setField("selAgencies",strSearch[2]);


		if (strSearch[9] != null && strSearch[9] != "") {
			if(ls.getNotSelectedDataWithGroup() == strSearch[9]){
                setSelectedDataWithGroup(strSearch[9],ls,true);
			}
			else{
                setSelectedDataWithGroup(strSearch[9],ls,false);                        
			}
		}
		
		if (strSearch[10] != null && strSearch[10] != "") {
			setSelectedDataWithGroup(strSearch[10],ls2,false);
		}
		
	}	
}

function getAgents() {
	var strAgents;
	if(getText("selAgencies")!="All"){
		 strAgents=ls.getSelectedDataWithGroup();
	}else {			
		strAgents=ls.getselectedData();
	}
	var newAgents;
	if(strAgents.indexOf(":")!=-1){
		strAgents=replaceall(strAgents,":" , ",");
	}
	if(strAgents.indexOf("|")!=-1){
		newAgents=replaceall(strAgents,"|" , ",");
	}else{
		newAgents=strAgents;
	}
	return newAgents;
}

function getUsers() {
	var strUsers = ls2.getselectedData();
	var newUsers;
	if (strUsers.indexOf(":") != -1) {
		strUsers = replaceall(strUsers, ":", ",");
	}
	if (strUsers.indexOf("|") != -1) {
		newUsers = replaceall(strUsers, "|", ",");
	} else {
		newUsers = strUsers;
	}
	return newUsers;
}

function getAgentClick() {
	if(getValue("selAgencies") == "") {			
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	}else {
		var strSearchCriteria = getValue("txtFromDate")+"#"+getValue("txtToDate")+"#"+getValue("selAgencies")+"#"
		+"#"+"#"+"#"
		+"#" + "#" +"#"
		+" #"+"#"+"#";
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents","");
		setField("hdnMode","SEARCH");		
		var objForm  = document.getElementById("frmTax");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function loadUsersClick() {

	var agents = getAgents();

	if (displayAgencyMode == 1  && getValue("selAgencies") == "") {			
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	} else if (agents == undefined || agents == null || agents == "") {			
		showERRMessage(arrError["agentsRqrd"]);
		getFieldByID("lstRoles").focus();
	} else {
		ls2.removeAllFromListbox();		
		
		var strSearchCriteria = getValue("txtFromDate") + "#"
		+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
		+ "#"
		+ "#"
		+ "#" 
		+ "#"
		+ "#"
		+ "#"
		+ ls.getSelectedDataWithGroup() + "#"
		+ ls2.getSelectedDataWithGroup() + "#"
		+"#";
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents", getAgents());
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmTax");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function setSelectedDataWithGroup(strData,listBoxID,isAll) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();
	
	if(isAll){
        listBoxID.moveOptions('>>');
    }
    else{
    	for ( var x = 0; x < groups.length; x++) {

			if (groups[x].indexOf(":") != -1) {
				var dd = groups[x].split(":");
				groupList = dd[1].split(",");

				for ( var z = 0; z < groupList.length; z++) {
					if (str == "") {
						str += dd[0] + ":" + groupList[z];
					} else {
						str += "," + dd[0] + ":" + groupList[z];
					}
				}
			} else {
				if (str == "") {
					str += groups[x];
				} else {
					str += "," + groups[x];
				}
			}
		}
    	listBoxID.move('>', str);
    }
}

function changeAgencies(){
	ls.clear();		
}


function validateDate(){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
		
	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");
	
	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
				
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM;}
	if (dtCD < 10){dtCD = "0" + dtCD;}
	 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
		
	strSysODate=(tempYear+tempMonth+tempDay); 
	if(tempOStartDate > tempOEndDate){
//		showCommonError("Error",fromDtExceed);
		showERRMessage(arrError["fromDtExceed"]);
		getFieldByID("txtToDate").focus();
	} else {
		validate = true;				
	}
	return validate;	
}




function viewClick(){
	
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}
	setField("hdnUsers", getUsers());
	
	if(getText("txtFromDate")==""){
		showERRMessage(arrError["fromDtEmpty"]);
		
		getFieldByID("txtFromDate").focus();
		
	} else if(dateValidDate(getText("txtFromDate"))==false){
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
		
	}  else if(getText("txtToDate")==""){
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
		
	} else if(dateValidDate(getText("txtToDate"))==false){
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();
		
	} else if ((displayAgencyMode == 1 || displayAgencyMode == 2)&& getText("hdnAgents") == "") {
				showERRMessage(arrError["agentsRqrd"]);
		
	} else if(dateValidDate(getText("txtFromDate"))==true && dateValidDate(getText("txtToDate"))==true && validateDate()) {
			setField("hdnMode","VIEW");
			//setField("hdnRptType","SUMMARY");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
			top.objCWindow = window.open("showBlank","CWindow",strProp);			
			var objForm  = document.getElementById("frmTax");
			objForm.target = "CWindow";
			objForm.action = "fareDiscountsReport.action";
			objForm.submit();			
			top[2].HidePageMessage();		
	}
	
}


function objOnFocus(){
	top[2].HidePageMessage();
}