var screenId = "UC_REPM_082";
var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "2":
		setField("txtBookedFromDate", strDate);
		break;
	case "3":
		setField("txtBookedToDate", strDate);
		break;
    case "4":
		setField("txtDepFromDate", strDate);
		break;
    case "5":
		setField("txtDepToDate", strDate);
		break;

	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarTo(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	//getFieldByID("selCategory").focus();
}

function getDateEquvalantNumber(tempDate) {
	var tempDay = tempDate.substring(0, 2);
	var tempMonth = tempDate.substring(3, 5);
	var tempYear = tempDate.substring(6, 10);
	return parseInt((tempYear + tempMonth + tempDay), 10);
}

function viewClick() {
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;

	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");
	var depDateFrom = getText("txtDepFromDate");
	var depDateTo = getText("txtDepToDate");
	
	var tempOStartDate = (dateFrom != "") ? getDateEquvalantNumber(dateFrom) : "";
	var tempOEndDate =  (dateTo != "") ? getDateEquvalantNumber(dateTo) : "";
	var tempDepStartDate =  (depDateFrom != "") ? getDateEquvalantNumber(depDateFrom) : "";
	var tempDepEndDate =  (depDateTo != "") ? getDateEquvalantNumber(depDateTo) : "";

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);

	strSysODate = (tempYear + tempMonth + tempDay);

	if ((getText("txtFromDate") == "") && (getText("txtDepFromDate") == "")) {
		showERRMessage(arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
		return;
	}  else if ((getText("txtToDate") == "") && (getText("txtDepToDate") == "")) {
		showERRMessage(arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
		return;
	} else if (!((getText("txtToDate") != "") && (getText("txtFromDate") != "")) 
			&& !((getText("txtDepToDate") != "") && (getText("txtDepFromDate") != ""))) {
		showERRMessage(arrError["atleastOneDateSetRequired"]);
		getFieldByID("txtFromDate").focus();
		return;
	} else if (getText("txtDepToDate") != "" && dateValidDate(getText("txtDepToDate")) == false) {
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtDepToDate").focus();
		return;
	} else if (getText("txtDepFromDate") != "" && dateValidDate(getText("txtDepFromDate")) == false) {
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtDepFromDate").focus();
		return;
	} else if (getText("txtFromDate") != "" && dateValidDate(getText("txtFromDate")) == false) {
		showERRMessage(arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
		return;
	} else if (getText("txtToDate") != "" && dateValidDate(getText("txtToDate")) == false) {
		showERRMessage(arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();
		return;
	} else if (tempOStartDate != "" && tempOEndDate != "" && tempOStartDate > tempOEndDate) {
		showERRMessage(arrError["fromDtExceed"]);
		getFieldByID("txtToDate").focus();
		return;
	} else if (tempDepStartDate != "" && tempDepEndDate != "" && tempDepStartDate > tempDepEndDate) {
		showERRMessage(arrError["fromDtExceed"]);
		getFieldByID("txtDepToDate").focus();
		return;
	} else if (getText("selSegQuantifier") == "" && getText("txtSegmentCount") != ""){
		showERRMessage(arrError["segQuantifierReq"]);
		getFieldByID("selSegQuantifier").focus();
		return;
	} else if (getText("selSegQuantifier") != "" && getText("txtSegmentCount") == ""){
		showERRMessage(arrError["segCountReq"]);
		getFieldByID("txtSegmentCount").focus();
		return;
	} else if (getText("txtSegmentCount") != "" && !isPositiveInt(getText("txtSegmentCount"))) {
		showERRMessage(arrError["segValidCountReq"]);
		getFieldByID("txtSegmentCount").focus();
		return;
	}

	var seg = getAllSegments().substr(0, getAllSegments().length - 1);
	setField("hdnSegments", seg);
	setField("hdnBookingClasses",trim(bcls.getselectedData()) );
    setField("hdnPaxStatus",trim(listBoxPs.getselectedData()) );

	setField("hdnMode", "VIEW");

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	var objForm = document.getElementById("frmUserIncentiveRpt");
	objForm.target = "CWindow";
	objForm.action = "showUserIncentiveDetailsReport.action";
	objForm.submit();
	top[2].HidePageMessage();

}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");
	var arr = getValue("selArrival");
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(arrError["depatureRqrd"]);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrError["arrivalRqrd"]);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(arrError["sameSegment"]);
		getFieldByID("selArrival").focus();
	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arrError["sameViaArival"]);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {

		showERRMessage(arrError["sameViaDept"]);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(arrError["sameVia"]);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(arrError["sameVia"]);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(arrError["sameVia"]);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(arrError["OnDExists"]);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		values += control.options[t].text + ",";
	}
	return values;
}

function setAllSegments(segs) {
	var control = document.getElementById("selSegment");
	var selSegs = segs.split(",");

	for ( var i = 0; i < selSegs.length; i++) {
		if (trim(selSegs[i]) != "") {
			control.options[control.length] = new Option(selSegs[i], selSegs[i]);
		}
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function beforeUnload() {
	if ((top.objCWindow) && (!top.objCWindow.closed)) {
		top.objCWindow.close();
	}
}
function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}