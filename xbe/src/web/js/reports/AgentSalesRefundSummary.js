var objWindow;
var value;
var screenId = "UC_REPM_083";
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function AgentSalesRefundSummary(){};

AgentSalesRefundSummary.isPgEdited = false;
AgentSalesRefundSummary.strSearchCriteria = "";

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}



function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarTo(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setPageEdited(isEdited) {
	AgentSalesRefundSummary.isPgEdited = isEdited;
}


function winOnLoad() {
	
	if(agentWiseRouteSelection) {
		getFieldByID("flightNumberDetail").style.display = "none";
		populateDynamicAgentWiseOndList();
	}
	
	if (top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
	
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		
		setField("selReportType", strSearch[2]);
		setField("txtFlightNumber", strSearch[3]);
		setField("selFlightType", strSearch[4]);
		setField("selCOS", strSearch[5]);
		//setField("selJourneyType", strSearch[6]);
		setField("selDept", strSearch[7]);
		setField("selArival", strSearch[8]);
		
//		if(strSearch[9]!=null && strSearch[9]!=""){
//			setAllSegments(strSearch[9]);	
//		}		
		
		setField("selBasisCode", strSearch[10]);
		setField("selCountry", strSearch[11]);
		setField("selStations", strSearch[12]);
		setField("selAgencies", strSearch[13]);		
		setField("selAgentCity", strSearch[14]);
		setField("selTerritory", strSearch[15]);
		setField("selPaidCurrency", strSearch[16]);
		setField("selPaxType", strSearch[17]);
		
		setField("radAgentSelOption", strSearch[18]);		
		
	}else{
		agentSelectionFilter();
	}
}

function viewClick(isSchedule) {	

	var radOption = getText("radAgentSelOption");	
	var agentCount = 0;
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents",getAgents());
		agentCount = getAgents().split(",").length;
		
		if(radOption=="A"){
			setField("hdnAgents","");	
		}
		
	} else {
		setField("hdnAgents","");
	}	
	
	
	if (getText("txtFromDate") == ""){
		showCommonError("Error", arrError["fromDtEmpty"]);
		getFieldByID("txtFromDate").focus();
	}else if (getText("txtToDate") == ""){
		showCommonError("Error", arrError["toDtEmpty"]);
		getFieldByID("txtToDate").focus();
	}else if ((getText("txtFromDate") != "") && (dateValidDate(getText("txtFromDate")) == false)) {
		showCommonError("Error", arrError["fromDtInvalid"]);
		getFieldByID("txtFromDate").focus();
	}else if ((getText("txtToDate") != "") && (dateValidDate(getText("txtToDate")) == false)) {
		showCommonError("Error", arrError["toDtinvalid"]);
		getFieldByID("txtToDate").focus();	
	}else if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))){		
		showCommonError("Error", arrError["fromDtExceed"]);
		getFieldByID("txtToDate").focus();	
	}else if (trim(getValue("selReportType")) == "") {
		showERRMessage(arrError["rptTypeRqrd"]);
		getFieldByID("selReportType").focus();		
	} else if ((getText("txtFlightNumber") != "") &&  (!isFlightNoValid("txtFlightNumber"))) {			
		showERRMessage(arrError["flightNoInvalid"]);				
		getFieldByID("txtFlightNumber").focus();	
	} else if(radOption!="A" && displayAgencyMode == 1  && getText("selAgencies")==""){
		showERRMessage(arrError["agentTypeRqrd"]);
		getFieldByID("selAgencies").focus();
	} else if(radOption!="A" && (displayAgencyMode == 1 || displayAgencyMode == 2) && getText("hdnAgents")==""){
		showERRMessage(arrError["agentsRqrd"]);
	} else if(radOption!="A" && (displayAgencyMode == 1 || displayAgencyMode == 2) && (getValue("selAgencies")=='All' && agentCount > 2)){
		showERRMessage(arrError["oneAgentCode"]);	
	} else if(radOption!="A" && (displayAgencyMode == 1 || displayAgencyMode == 2) && (getValue("selAgencies")!='All' && agentCount > 1)){
		showERRMessage(arrError["oneAgentCode"]);
	}else if(getText("selDept")==getText("selArival") && getText("selDept") !="All" && getText("selDept") !="" && getText("selArival") !=""  ){
			showERRMessage(arrError["sameSegment"]);
			getFieldByID("selArival").focus();
	}else{		
		
		if(isSchedule){
			scheduleReport();
		}else{
		
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=600,height=330,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmAgentSalesRefundReport");
			objForm.target = "CWindow";
			objForm.action = "showAgentSalesRefundSummaryReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(arrError["invaliddate"]);
		getFieldByID(cntfield).focus();
		return;
	}
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	AgentSalesRefundSummary.strSearchCriteria = "";
	top.LoadHome();
}
function ckheckPageEdited() {
	if (AgentSalesRefundSummary.isPgEdited)
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function checkGreatSalesDate() {
	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", arrError["slEffecFrmDtGrt"]);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}


function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else if (getValue("selAgencies") == 'SGSA' || getValue("selAgencies") == 'TA'){
		Disable('chkTAs',false);
		Disable('chkCOs',true);
		getFieldByID("chkCOs").checked = false;		
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
	if (getText("selAgencies").toUpperCase().indexOf("GSA") != -1) {
		Disable("selTerritory", "");
	} else {
		Disable("selTerritory", true);
	}
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showERRMessage(arrError["agentTypeRqrd"]);	
		getFieldByID("selAgencies").focus();
	} else {
		ls.removeAllFromListbox();
		//txtFromDate,txtToDate,selReportType,txtFlightNumber,selFlightType,selCOS
		//selJourneyType,selDeparture,selArrival,chkSales,chkRefunds,
		//selBasisCode,selCountry,selStations,selAgencies
		
		//var selSegStr = getSelectedSegment();
		var selSegStr = '';
		
		var strSearchCriteria = getValue("txtFromDate") + "#"
		+ getValue("txtToDate") + "#" + getValue("selReportType")+ "#" 
		+ getValue("txtFlightNumber")+ "#" + getValue("selFlightType")+"#"
		+ getValue("selCOS")+ "##"
		+ getValue("selDept")+ "#" + getValue("selArival")+"#"+selSegStr+"#"
		+ getValue("selBasisCode")+"#"
		+ getValue("selCountry")+ "#" + getValue("selStations")+"#"
		+ getValue("selAgencies")+"#"+ getValue("selAgentCity")+"#"
		+ getValue("selTerritory")+"#"+ getValue("selPaidCurrency")+"#"
		+getValue("selPaxType")+"#"+getText("radAgentSelOption");
		
		
		top.strSearchCriteria = strSearchCriteria;
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmAgentSalesRefundReport");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");
	var arr = getValue("selArrival");
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(arrError["depatureRequired"]);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrError["arrivalRequired"]);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(arrError["depatureArriavlSame"]);
		getFieldByID("selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arrError["arriavlViaSame"]);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(arrError["depatureViaSame"]);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(arrError["vianotinSequence"]);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(arrError["viaSame"]);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(arrError["viaSame"]);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(arrError["viaSame"]);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(arrError["OnDExists"]);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}

}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		values += control.options[t].text + ",";
	}
	return values;
}

function setAllSegments(segs) {
	var control = document.getElementById("selSegment");
	var selSegs = segs.split(",");

	for ( var i = 0; i < selSegs.length; i++) {
		if (trim(selSegs[i]) != "") {
			control.options[control.length] = new Option(selSegs[i], selSegs[i]);
		}
	}
}

function getSelectedSegment(){
	var control = document.getElementById("selSegment");

	var segmentStr='';
	var intCount = control.length;
	for ( var i = 0; i < intCount; i++) {
		segmentStr += control.options[i].value + ",";		
	}	
	
	return segmentStr;
}

function isFlightNoValid(txtfield) {	
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}

function cancelClick() {
	window.close();
}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmAgentSalesRefundReport', 
		composerName: 'agentSalesModifyRefundReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function agentSelectionFilter(){
	
	var radOption = getText("radAgentSelOption");	
	
	if(radOption == "A"){
		document.getElementById('divAgencies').style.display= 'none';
		document.getElementById('divAgents').style.display= 'none';
	} else if(radOption == "S"){			
		if (displayAgencyMode == 1 ) {
			document.getElementById('divAgencies').style.display= 'block';
			document.getElementById('divAgents').style.display= 'block';
		} else if (displayAgencyMode == 2 ) {
			document.getElementById('divAgents').style.display= 'block';
			document.getElementById('divAgencies').style.display= 'none';
		} else {
			document.getElementById('divAgencies').style.display= 'none';
			document.getElementById('divAgents').style.display= 'none';
		}
	}	
}

function populateDynamicAgentWiseOndList() {
	var airports = top.arrAirportsV2;
	var allOnds = top.arrOndMap;
	
	var airportCodes = new Array();
	for(var key in airports) {		
		if(airports.hasOwnProperty(key)) {
			airportCodes.push(airports[key][0].replace(/^'+/i, ''));		
		}		
	}
	clearDropDownValues("selDept");
	clearDropDownValues("selArival");
	ondMap = new Object();
	
	if(typeof(allOnds['***']) != "undefined"){
		var option = document.createElement('option');
	    option.text = "All";
	    option.value = "";
	    document.getElementById("selDept").add(option, 0);
		for(var ond in allOnds) {
			if(allOnds.hasOwnProperty(ond)) {
				if(ond != "***") {
					var i=1;
					ondMap[ond] = allOnds[ond].replace(/^'+/i, '').split("|");
					option = document.createElement('option');
				    option.text = option.value = ond.replace(/^'+/i, '');
				    document.getElementById("selDept").add(option, i);
				    i++;
				} else {
					ondMap[""] = allOnds[ond].replace(/^'+/i, '').split("|");
				}
				
			}
		}

	} else {
		for(var ond in allOnds) {
			if(allOnds.hasOwnProperty(ond)) {
				var i=0;
				ondMap[ond] = allOnds[ond].replace(/^'+/i, '').split("|");
				var option = document.createElement('option');
			    option.text = option.value = ond.replace(/^'+/i, '');
			    document.getElementById("selDept").add(option, i);
			}				
		}
	}
	
	var from =  document.getElementById("selDept");
	var to = document.getElementById("selArival");
	
	from.onchange = function(){
		to.length = 0;
	        var _val = this.options[this.selectedIndex].value;
	        if(ondMap != undefined) {
	        	if(ondMap[_val].indexOf("***,")!=-1) {
	        		var op = document.createElement('option');
	        		op.value = "";
	        		op.text = "All";
	        		to.appendChild(op);
	        	}
	        	for (var dst in ondMap[_val]){	
	        		if(ondMap[_val].hasOwnProperty(dst)) {
	        			if(ondMap[_val][dst].split(",")[0]!="***") {		
			        		var op = document.createElement('option');
			        		op.value = ondMap[_val][dst].split(",")[0];
				            op.text = ondMap[_val][dst].split(",")[0];
				            to.appendChild(op);
			        	}			        	
	        		}
		        	
		        }
	        }
	        
	    };
	    from.onchange();	
}

function clearDropDownValues(id) {
	var select = document.getElementById(id);
	var length = select.options.length;
	var i;
    for(i = select.options.length - 1 ; i >= 0 ; i--) {
    	select.remove(i);
    }
}