//Javascript Document
//Author -Shakir

var strCarry;
var strLen=0;

var objInterval = setInterval("gridLoad()", 300);
 populateAgentCurr();

DivWrite("CurrFormat","");	




function gridLoad() {
	if (objDG.loaded == true) {
		clearInterval(objInterval);
		strLen = arrChildData.length;
		if (strLen != 0) {
			DivWrite("spnAgentName", arrChildData[0][7]);		
			
			if(arrFormData[10]=='L'){
				DivWrite("spnCreditLimit",arrChildData[0][9]);// no need of currency code
			}else{
				DivWrite("spnCreditLimit", arrChildData[0][4]+" "+arrFormData[9]);
			}
			setField("hdnCurrectCreditLimit", arrChildData[0][4]);
			setField("hdnCode", arrChildData[0][6]);
			setField("hdnBankGuarantee", arrChildData[0][8]);
			setField("hdnLocCurrectCreditLimit", arrChildData[0][9]);
			setField("hdnLocBankGuarantee", arrChildData[0][10]);			
		} else {
			DivWrite("spnAgentName", arrFormData[5]);				
			DivWrite("spnCreditLimit", arrFormData[1]);
			setField("hdnCode", arrFormData[0]);					
			setField("hdnCurrectCreditLimit", arrFormData[1]);	
			setField("hdnBankGuarantee", arrFormData[6]);
			setField("hdnLocCurrectCreditLimit", arrFormData[7]);
			setField("hdnLocBankGuarantee", arrFormData[8]);									
		}
	}
}


 

 
function CHOnLoad(strMsg, strMsgType) {
	if ((strMsg != null) && (strMsgType != null) && (strMsg != "")) {
		if (arrFormData != null && strMsgType == "Error") {
			if ((arrFormData[2] != null) && (arrFormData[2] != "undefined")) {
				setField("txtNLimit", arrFormData[2]);		
			}
			if ((arrFormData[3] != null) && (arrFormData[3] != "undefined")) {
				setField("txtRemarks", arrFormData[3]);		
			}
			if(arrFormData[9])
				setField("selCurrIn", arrFormData[9]);
		}	
		showWindowCommonError(strMsgType, strMsg);
	}
	
	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			opener.searchClick();
			resetClick();
			alert("Record Successfully saved!");
		}
	}
	if(disableCreditLimitChange(arrFormData[11])){
		disableInputControls();
	}else{
		getFieldByID("txtNLimit").focus();
	}
}

function disableCreditLimitChange(val) {
	var disableCredit = true;
	for (var i = 0 ; i < arrAgentCA.length; i++) { 
		if (arrAgentCA[i][0] == val) {
			if (arrAgentCA[i][1] == "false") { // if credit not applicable
				disableCredit = true;
				break;
			} else {
				disableCredit = false;
			}
		} 
	}
	return disableCredit;
}

function disableInputControls(){
	Disable("btnReset", true);
	Disable("btnSave", true);
	Disable("txtNLimit", true);
	Disable("txtRemarks", true);
}

function saveClick() {
	if (isEmpty(getText("txtNLimit"))) {
		showWindowCommonError("Error", arrError["CreditLimitisEmpty"]);		
		getFieldByID("txtNLimit").focus();
		return false;
	} else if (isEmpty(trim(getText("txtRemarks")))) {
		showWindowCommonError("Error", arrError["ReasonForChangeEmpty"]);		
		getFieldByID("txtRemarks").focus();		
		return false;
	}else if (isEmpty(getValue("agentPayCurrIn"))) {
		showWindowCommonError("Error", arrError["Amountblank"]);		
		getFieldByID("selCurrIn").focus();		
		return false;
	} else if (checkInvalidChar(getValue("txtRemarks"),arrError["invalidCharacter"], "") != ""){
		showWindowCommonError("Error", arrError["invalidCharacter"]);		
		getFieldByID("txtRemarks").focus();		
		return false;
	} else { 
		if ((getValue("agentPayCurrIn")== baseCurrency && parseFloat(getText("txtNLimit")) > parseFloat(getText("hdnBankGuarantee")))
				||(getValue("selCurrIn")!= baseCurrency && parseFloat(getText("txtNLimit")) > parseFloat(getText("hdnLocBankGuarantee")))) {
			if (!confirm("Credit Limit Exceeds Bank Guarantee. Do you wish to continue?")) {
				getFieldByID("txtNLimit").focus();				
				return false;
			}	
		}
		Disable("btnSave",true);
		setPageEdited(false);	
		setField("txtNLimit", trim(getText("txtNLimit")));
		setField("txtRemarks", trim(getText("txtRemarks")));
		var tempName = trim(getText("txtRemarks"));
		tempName=replaceall(tempName,"'" , "`");
		setField("txtRemarks", replaceEnter(tempName));		
		setField("hdnCurrectCreditLimit", removeInvalidChars(getText("hdnCurrectCreditLimit")));
		setField("hdnCMode", "SAVECREDIT");
		Disable("agentPayCurrIn",false);
		setField("selCurrCode",getValue("agentPayCurrIn"));
		Disable("agentPayCurrIn",true);
		setField("ForceCancel", "false");
		setField("hdnCode", arrFormData[0]);
		document.forms[0].action ="showAdjustCreditLimit.action";
		document.forms[0].submit();
	}
}

function resetClick(){
	setField("txtNLimit", "");
	setField("txtRemarks", "");
	setField("selCurrIn", "");
	getFieldByID("txtNLimit").focus();	
}

function validateDecimal(obj,e) {
	var str = getFieldByID(obj).value;
	if(str.indexOf('.')>=0){
		var keynum;
		var keychar;
		if(window.event) { // IE
			keynum = e.keyCode
		}else if(e.which) {// Netscape/Firefox/Opera
			keynum = e.which
		}
		keychar = String.fromCharCode(keynum);

		if(keychar == '.'){
			return false;
		}
	}
	
	setPageEdited(true);
	
	var regex=/[^\d\.]/g;
	
	var str = getFieldByID(obj).value;
	var strCR=str.replace(regex,"");
	setField(obj, strCR); 		
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 13,2);
	var wholeNumber;
	if(!blnVal){
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 13) {
				setField(obj, strCR.substr(0, wholeNumber.length - 1)); 	
			} else {
				setField(obj,strCR.substr(0, strLen-1)); 		
			}
		} else {
			setField(obj, strCR.substr(0, strLen - 1)); 		
		}	
		getFieldByID(obj).focus();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function closeClick() {
	if (loadCheck()) {
		setPageEdited(false);
		window.close();
	}
}

function loadCheck() {
	if (top.pageEdited) {
		return confirm("Changes will be lost! Do you wish to Continue?");
	} else {
		return true;
	}
}

function removeInvalidChars(val) {
	var temp = val;
	for (var i = 0; i < temp.length; i++) {
		if ((temp.charAt(i) != ".") && (!isInt(temp.charAt(i)))) {
			temp = temp.replace(temp.charAt(i), "");
		}
	}
	return temp;
}

function populateAgentCurr(){
		var htm  = '<select id="agentPayCurrIn" name="agentPayCurrIn" size="1" onChange="currencyChange()" tabindex="31">';
		 
			
			htm+='<option value = '+arrFormData[9]+'>'+arrFormData[9]+'</option>';
	 
		 htm+='</select>';
		DivWrite("spnPayIn", htm);
		Disable("agentPayCurrIn", true);
		 
	}