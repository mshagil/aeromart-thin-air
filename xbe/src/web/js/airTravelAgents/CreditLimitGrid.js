	var objCol1 = new DGColumn();
	objCol1.columnType = "label";
	objCol1.width = "13%";
	objCol1.arrayIndex = 1;
	objCol1.toolTip = "" ;
	objCol1.headerText = "Date";
	objCol1.itemAlign = "center"
	
	var objCol2 = new DGColumn();
	objCol2.columnType = "label";
	objCol2.width = "14%";
	objCol2.arrayIndex = 2;
	objCol2.toolTip = "" ;
	objCol2.headerText = "Login ID";
	objCol2.itemAlign = "left"
	
	var objCol3 = new DGColumn();
	objCol3.columnType = "label";
	objCol3.width = "14%";
	objCol3.arrayIndex = 3;
	objCol3.toolTip = "" ;
	objCol3.headerText = "User Name";
	objCol3.itemAlign = "left"
	
	var objCol4 = new DGColumn();
	objCol4.columnType = "label";
	objCol4.width = "10%";
	objCol4.arrayIndex = 4;
	objCol4.toolTip = "credit limit from base currency" ;
	objCol4.headerText = "Credit Limit <br> " + baseCurrency;
	objCol4.itemAlign = "right"
	
	var objCol5 = new DGColumn();
	objCol5.columnType = "label";
	objCol5.width = "35%";
	objCol5.arrayIndex = 5;
	objCol5.toolTip = "" ;
	objCol5.headerText = "Reason for Change";
	objCol5.itemAlign = "left"	
	
	var objCol6 = new DGColumn();
	objCol6.columnType = "label";
	objCol6.width = "10%";
	objCol6.arrayIndex = 9;
	objCol6.toolTip = "credit limit from specified currency" ;
	objCol6.headerText = "Credit Limit <br> Specified" ;
	objCol6.itemAlign = "right"

	
	// ---------------- Grid	
	var objDG = new DataGrid("spnCreditHistory");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.addColumn(objCol3);
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol6);
	objDG.addColumn(objCol5);
	
	objDG.width = "100%";
	objDG.height = "144px";
	objDG.headerBold = false;
	objDG.arrGridData = parent.arrChildData;
	objDG.seqNo = true;
	objDG.rowOver = true;
	objDG.backGroundColor = "#F4F4F4";
	objDG.rowClick = "parent.rowClick";
	objDG.displayGrid();
