<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
  </head>
	 <body  scroll="no" oncontextmenu="return showContextMenu()" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	    <%@ include file="../common/IncludeTop.jsp"%>
	    					<br><br><br><br>
							<table width="80%" border="0" cellpadding="2" cellspacing="5" align="center">
								<tr>
									<td align="center">
										<img src="../images/AA173_no_cache.jpg">
									</td>
								</tr>
								<tr>
									<td align="center">
										<font class="mandatory">
											System error has occurred.
										</font>
									</td>
								</tr>
								<tr>
									<td align="center">
										<font class="mandatory">
											Please contact the Administrator. Sorry for the inconvenience.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<font class="mandatory fntBold"><span id="spnErrorMsg"></span></font>
									</td>
								</tr>
							</table>
	    				</td>
						<td width="30"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
					</tr>
				</table>
			</td>
			<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
		</tr>
	</table>
   <script language="javascript">
   <!--
	   DivWrite("spnErrorMsg", "<font class='Mandatory fntMedium'>" + top.strServerErrorMsg + "<\/font>")
	   top.varInitialize();
	   top[2].HideProgress();
   //-->
   
   function CloseWindow(){
		//window.close();
	}
   </script>	
	</body>
</html>