<%@ page language="java" %>
<%@ include file="../common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Send Notification</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
    <link rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
    <script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false'  />"
            type="text/javascript"></script>
    <script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../js/alert/ManageFlightPnrNotifications.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
</head>

<body onkeypress='return Body_onKeyPress(event)' onload="winOnLoad();" onmousewheel="return false;"
      onkeydown="return Body_onKeyDown(event)" scroll="yes" oncontextmenu="return showContextMenu()"
      ondrag="return false">
<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
<form method="post" name="frmViewNotify" id="frmViewNotify" action="showFlightPnrViewNotification.action">
    <script type="text/javascript">
        <c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
        <c:out value="${requestScope.reqResponseString}" escapeXml="false" />
        var reqMessageType = "<c:out value='${requestScope.reqMsgType}' escapeXml='false' />";
        var reqMessage = "<c:out value='${requestScope.reqMessage}' escapeXml='false' />";
        var strFlightId = "<c:out value='${requestScope.hdnFlightId}' escapeXml='false' />";
        var reqChannel = "";
        var reqLoadPnrCriteria = "<c:out value='${requestScope.hdnLoadPnrCriteria}' escapeXml='false' />";
        var strFields = "<c:out value='${requestScope.hdnFields}' escapeXml='false' />";

        <c:out value="${requestScope.reAjax}" escapeXml="false" />
        //top[2].ShowProgress();
    </script>

    <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td><font class="Header">View Notifications</font></td>
        </tr>
        <tr>
            <td><font class="fntSmall">&nbsp;</font></td>
        </tr>
        <tr>
            <td>
                <table width="100%" cellpadding="2" cellspacing="0" border="0" align="center">
                    <tr>
                        <td>
                            <%@ include file="../common/IncludeFormTop.jsp" %>
                            Search
                            <%@ include file="../common/IncludeFormHD.jsp" %>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="6%" align=center><font>PNR</font></td>
                                    <td width="20" align=left><input type="text" name="txtPnr" id="txtPnr" size="16"
                                                                     maxlength="16" onchange="pageOnChange()"></td>
                                    <td width="6%" align="center"><font>OR</font></td>

                                    <td width="10%" align=left><font>Flight Number</font></td>
                                    <td><input type="text" name="txtFlightNo" id="txtFlightNo" size="10" maxlength="10"
                                               onchange="pageOnChange()"></td>
                                    <td width="2%"><font> </font></td>
                                    <td width="15%"><font>Date Range: From</font></td>
                                    <td width="10%"><input type="text" name="txtFromDate" id="txtFromDate" size="10"
                                                           maxlength="10" onblur="dateChk('txtFromDate')"
                                                           onchange="pageOnChange()" invalidText="true"/>
                                    </td>
                                    <td width="3%" align="left"><a href="javascript:void(0)"
                                                                   onclick="loadCalendar(4, event); return false;"
                                                                   title="View Calendar"><img
                                            src="../images/Calendar_no_cache.gif" border="0"></a></td>
                                    <td width="5%" align=center><font>To</font></td>
                                    <td width="10%" align=right><input type="text" name="txtToDate" id="txtToDate"
                                                                       size="10" maxlength="10"
                                                                       onblur="dateChk('txtToDate')"
                                                                       onchange="pageOnChange()" invalidText="true"/>
                                    </td>
                                    <td width="4%" align="left"><a href="javascript:void(0)"
                                                                   onclick="loadCalendar(5, event); return false;"
                                                                   title="View Calendar"><img
                                            src="../images/Calendar_no_cache.gif" border="0"></a></td>

                                    <td colspan=10 align="right"><input name="btnViewSummary" type="button"
                                                                        class="button" id="btnViewSummary"
                                                                        value="SEARCH" style="width:100px"
                                                                        onClick="showSummaryView()"></td>

                                    <td width="12%" align="right">
                                        <a onclick="showGridView()" href="javascript:void(0)">
                                            <u>
                                                <font id="changeView" title="Switch to Grid view">Switch to Grid
                                                    view</font>
                                            </u>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                            <%@ include file="../common/IncludeFormBottom.jsp" %>
                        </td>
                    </tr>
                    <tr id="trSpnViewFlightNotificationGrid" style="visibility: collapse">
  						<td valign="top">
							<%@ include file="../common/IncludeFormTop.jsp"%>
							Flight Notification Details
							<%@ include file="../common/IncludeFormHD.jsp"%>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
									<tr><td colspan="2" >
									<div style="width:865px;overflow-x:auto;">
										<span id="spnViewFlightNotificationGrid" style="visibility: hidden"></span>
									</div></td>
  									</tr>
								</table>
			  				<%@ include file="../common/IncludeFormBottom.jsp"%>
						</td>
					</tr>
                    <tr id="trCalendar" style="visibility: collapse">
                        <td id="tdCalendar" width="100%" height="100%" style="visibility: collapse">
                            <div id="gridViewNotifications" style="visibility: hidden"></div>
                        </td>
                    </tr>
                    <tr id="trSpnViewNotificationSummary">
                        <td style="height:100px;" valign="top">
                            <%@ include file="../common/IncludeFormTop.jsp" %>
                            Summary
                            <%@ include file="../common/IncludeFormHD.jsp" %>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td><span id="spnViewNotificationSummary"></span></td>
                                </tr>
                            </table>
                            <%@ include file="../common/IncludeFormBottom.jsp" %>
                        </td>
                    </tr>

                    <tr id="trSpnViewNotificationDetails">
                        <td style="height:100px;" valign="top">
                            <%@ include file="../common/IncludeFormTop.jsp" %>
                            Details
                            <%@ include file="../common/IncludeFormHD.jsp" %>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td><span id="spnViewNotificationDetails"></span></td>
                                </tr>
                            </table>
                            <%@ include file="../common/IncludeFormBottom.jsp" %>
                        </td>
                    </tr>
                    <tr id="trSpnViewPaxContactsDetails">
                        <td style="height:30px;" valign="top">
                            <%@ include file="../common/IncludeFormTop.jsp" %>
                            Re-Cap of Passenger Contacts
                            <%@ include file="../common/IncludeFormHD.jsp" %>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
                                <tr id="spnViewPaxContactsDetails">
                                </tr>
                            </table>
                            <%@ include file="../common/IncludeFormBottom.jsp" %>
                        </td>
                    </tr>
                    <tr id="rtTxtMessage">
                        <td style="height:80px;" valign="top">
                            <%@ include file="../common/IncludeFormTop.jsp" %>
                            Correct & Re-Send Message
                            <%@ include file="../common/IncludeFormHD.jsp" %>

                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td><font>Notification Code</font></td>
                                    <td><input type="text" name="txtCode" id="txtCode" size="20" maxlength="20"
                                               onchange="pageOnChange()"></td>
                                    <td width="1%"></td>
                                    <td width="10%"><font>Notification Channel</font></td>
                                    <td><select onchange="selectPNRs()" tabindex="1" style="width: 125px"
                                                name="selNotifyChannel" size="1" id="selNotifyChannel">
                                        <option selected="" value="-1"></option>
                                        <option value="1">Sms Only</option>
                                        <option value="2">Email Only</option>
                                        <option value="3">Sms + Email</option>
                                    </select></td>
                                    <td rowspan=3>
                                    <td rowspan=3>
                                        <table align="left" id="table2" height="30%" cellpadding="0"
                                               cellspacing="0" border="0" align="center">
                                            <tr>
                                                <td align="center"><font>Generated Message</font></td>
                                                <td valign="top" rowspan=3><textarea name="txtMessage" id="txtMessage"
                                                                                     cols=25 rows=3
                                                                                     onchange="pageOnChange()"><c:out
                                                        value='${requestScope.txtMessage}'
                                                        escapeXml='false'/></textarea></td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td colspan=2 width="10%" align="right"><input name="btnSend" id="btnSend"
                                                                                   type="button" class="Button"
                                                                                   value="Re-Send"
                                                                                   onclick="resendMessages()">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=9 width="10%" align="right"><input name="btnClose" id="btnClose"
                                                                                   type="button" class="Button"
                                                                                   value="Close"
                                                                                   onclick="closeNotification()"></td>
                                </tr>
                            </table>
                            <%@ include file="../common/IncludeFormBottom.jsp" %>
                        </td>
                    </tr>
                </table>

                <input type="hidden" name="hdnResultsLimit" id="hdnResultsLimit" value="100">
                <input type="hidden" name="hdnAction" id="hdnAction" value="">
                <input type="hidden" name="hdnViewPnrCriteria" id="hdnViewPnrCriteria"
                       value="<c:out value="${requestScope.hdnViewPnrCriteria}" escapeXml="false" />">
                <input type="hidden" name="hdnPnr" id="hdnPnr" value="">
                <input type="hidden" name="hdnRecNo" id="hdnRecNo">
                <input type="hidden" name="hdnCalendarLoad" id="hdnCalendarLoad">
                <input type="hidden" name="hdnMode" id="hdnMode"
                       value="<c:out value='${requestScope.hdnMode}' escapeXml='false' />">
                <input type="hidden" name="hdnFlightNo" id="hdnFlightNo" value="">
                <input type="hidden" name="hdnDepDate" id="hdnDepDate" value="">
                <input type="hidden" name="hdnFlightId" id="hdnFlightId" value="">
                <input type="hidden" name="hdnFlightNotificationId" id="hdnFlightNotificationId" value="">
                <input type="hidden" name="hdnSelectedRow" id="hdnSelectedRow"
                       value="<c:out value='${requestScope.hdnSelectedRow}' escapeXml='false' />">
            </td>
        </tr>
    </table>
</form>

<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
<%@ include file="../common/IncludeLoadMsg.jsp" %>

<c:out value="${requestScope.returnData}" escapeXml="false"/>

<script src="../js/alert/ViewFlightPnrNotifications.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
        type="text/javascript"></script>
<script src="../js/alert/ViewFlightPnrNotificationSummaryGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
        type="text/javascript"></script>
<script src="../js/alert/ViewFlightPnrNotificationDetailGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
        type="text/javascript"></script>
<script src="../js/alert/ViewNotificationCalender.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
        type="text/javascript"></script>
<script src="../js/alert/ViewFlightPnrNotificationGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
        type="text/javascript"></script>
<script type="text/javascript">

    <!--

    var objProgressCheck = setInterval("ClearProgressbar()", 300);
    function ClearProgressbar() {
        if (typeof(objDG) == "object") {
            if (objDG.loaded) {
                clearTimeout(objProgressCheck);
                //top[2].HideProgress();
                //showAlertResponceMessage();
                HideProgress();
                top.blnProcessCompleted = false;
            }
        }
    }

    //-->
</script>

</body>
</html>