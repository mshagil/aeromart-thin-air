<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View / Adjust Credit Limit</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
  	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	<script src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script type="text/javascript">
  		var arrError = new Array();
  		 
		<c:out value="${requestScope.reqCreditHistoryRow}" escapeXml="false" />
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />	
		<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
		<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
		<c:out value="${requestScope.reqFormData}" escapeXml="false" />		
		<c:out value="${requestScope.reqAgentTypeCreditApplicable}" escapeXml="false" />
		
		var baseCurrency = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
		 var agentAgentCurr ="<c:out value="${requestScope.reqAllowLocalAgent}" escapeXml="false" />";
  </script>
  
  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
		onLoad="CHOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
   <form method="post" id="frmCredit" name="frmCredit">
   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr><td><font class="Header">View / Adjust Credit Limit</font></td></tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr><td><%@ include file="../common/IncludeMandatoryText.jsp"%></td></tr>	
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>			
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Credit Details History<%@ include file="../common/IncludeFormHD.jsp"%>
		  			<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr><td colspan="2"><font class="fntSmall">&nbsp;</font></td></tr>
  						<tr>
  							<td colspan="2"><font class="fntBold">Agent / GSA Name :<span id="spnAgentName"></span></font></td>
  						</tr>	
						<tr><td colspan="2"><font class="fntSmall">&nbsp;</font></td></tr>					
  						<tr><td colspan="2"><span id="spnCreditHistory"></span></td></tr>
		  				<tr><td colspan="2"><font>&nbsp;</font></td></tr>
		  			</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td><%@ include file="../common/IncludeFormTop.jsp"%>Credit Details<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td>	
							<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
									<td width="20%"><font>Credit Limit</font></td>
									<td colspan="2"><font class="fntBold"><span id="spnCreditLimit"></span></font></td>
								</tr>	
								<tr><td><font>New Credit Limit</font></td>
									<td colspan="2">
										<input type="text" name="txtNLimit" id="txtNLimit" maxlength="16" onKeyUp="return validateDecimal('txtNLimit',event)" onKeyPress="return validateDecimal('txtNLimit',event)" class="rightText">
										<font class="fntBold"><span id="CurrFormat" name="CurrFormat"></span></font><font class="mandatory">&nbsp;*</font>
									</td>
								</tr>
								<tr><td><font>Currency Format</font></td>
									<td colspan="2">
										 <span id="spnPayIn" name="spnPayIn"></span><font class="mandatory">&nbsp;*</font>
									</td>
								</tr>
								<tr>
									<td valign="top" ><font>Reason for Change</font></td>
									<td colspan="2" valign="top">
										<textarea name="txtRemarks" id="txtRemarks" cols="40" rows="4" onkeyUp="validateTextArea(this)" onkeyPress="validateTextArea(this)"></textarea>
										<font class="mandatory">&nbsp;*</font>
									</td>
								</tr>
								<tr>
									<td colspan="3">	
										<font>&nbsp;</font>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onClick="closeClick()">
										<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()">
									</td>
									<td align="right">
										<u:hasPrivilege privilegeId="ta.maint.credit.update">
											<input name="btnSave"  type="button" class="Button" id="btnSave" value="Save" onClick="saveClick()">
										</u:hasPrivilege>
									</td>
								</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
			</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
  	<script src="../js/airTravelAgents/CreditLimitGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/airTravelAgents/creditHistoryValidate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<input type="hidden" name="hdnCode" id="hdnCode" value="">
	<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
	<input type="hidden" name="hdnBankGuarantee" id="hdnBankGuarantee" value="">
	<input type="hidden" name="hdnCMode" id="hdnCMode" value="">
	<input type="hidden" name="hdnCurrectCreditLimit" id="hdnCurrectCreditLimit" value="">
	<input type="hidden" name="hdnNLimit" id="hdnNLimit" value="">
	<input type="hidden" name="ForceCancel" id="ForceCancel" value="">
	<input type="hidden" name="hdnLocBankGuarantee" id="hdnLocBankGuarantee" value="">
	<input type="hidden" name="hdnLocCurrectCreditLimit" id="hdnLocCurrectCreditLimit" value="">
	<input type="hidden" id="selCurrCode" name="selCurrCode">
	<input type="hidden" id="selCurrIn" name="selCurrIn">
  </form>
  </body>
</html>