<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="-1"/>
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css"/>

	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script  src="../js/common/tooltipnew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
   <script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	
  	
  	<script type="text/javascript">
			var arrData = new Array();
			var arrFormData = new Array();
			var selectedCurrency = "";
			var arrStatTer = new Array();
			var arrGSAs = new Array();
			var arrTAs = new Array();
			var arrAirports = new Array();
			var arrTerritories = new Array();
			var arrOwnTerritories = new Array();
			var arrCurrency = new Array();
			var arrTerritoryCounrty = new Array();
			var arrStationCountry = new Array();
			var arrStationCurrency = new Array();
			var strAgentTypes="";
			var blnsearchall = false;
			var arrError = new Array();
			var isServCh = false;
			var isServChEditable = false;
			var isHandlingFeeInfoEnable = false;
			var isRouteSelectionEnable = false;
			var hasStatusChangePriv = false;
			var hideGDSOnholdOption = "<c:out value="${requestScope.hideGDSOnholdOption}" escapeXml="false" />";

			<c:out value="${requestScope.reqServChJS}" escapeXml="false" />
			<c:out value="${requestScope.currencyExchangeList}" escapeXml="false" />
			
			
			<c:out value='${requestScope.reqGSACombo}' escapeXml='false' />
			<c:out value='${requestScope.reqAgentCombo}' escapeXml='false' />
			<c:out value='${requestScope.reqAirportCombo}' escapeXml='false' />
			<c:out value='${requestScope.reqTerritoryCombo}' escapeXml='false' />
			<c:out value='${requestScope.reqOwnTerritoryCombo}' escapeXml='false' />
			<c:out value="${requestScope.reqCurrencyCombo}" escapeXml="false" />
		 
			
			<c:out value="${requestScope.reqStationCountry}" escapeXml="false" />
			<c:out value="${requestScope.reqStationCurrency}" escapeXml="false" />
			<c:out value="${requestScope.reqStatTerList}" escapeXml="false" />
			var handlingCharge = "<c:out value="${requestScope.reqHandlingCharge}" escapeXml="false" />";
			<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqAgentTypeCreditApplicable}" escapeXml="false" />
			var baseCurrency = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
			arrCurrency[arrCurrency.length] = new Array(selectedCurrency, selectedCurrency);
			
			var varAccountCode = "<c:out value="${requestScope.reqAccountCode}" escapeXml="false" />";
			var carrierAgent = 	"<c:out value="${requestScope.reqCarrierAgent}" escapeXml="false" />";			
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqAdjustedPayment}" escapeXml="false" />
			<c:out value="${requestScope.reqsetPrivi}" escapeXml="false" />
			var strLanguages="<c:out value='${requestScope.reqLanguageHtml}' escapeXml='false' />";
			var userAgentType = "<c:out value="${requestScope.reqUserAgentType}" escapeXml="false" />";	
			var actualUserAgentType = "<c:out value="${requestScope.reqActualUserAgentType}" escapeXml="false" />";	
			var statusList = "<c:out value="${requestScope.reqAgentStatusList}" escapeXml="false" />"; 
			var agentCommList = "<c:out value="${requestScope.reqCommTypeList}" escapeXml="false" />";
			var reqAgntLevelEtickets = "<c:out value="${requestScope.reqAgntLevelEtickets}" escapeXml="false" />";
			var agentAgentCurr ="<c:out value="${requestScope.reqAllowLocalAgent}" escapeXml="false" />";
			var allowLocalCR = "<c:out value="${requestScope.reqAllowLocalCr}" escapeXml="false" />";
			var userCurr = "<c:out value="${requestScope.reqUserCurr}" escapeXml="false" />";
			var agentsPayOpt = "<c:out value="${requestScope.reqAgentPaymentOptions}" escapeXml="false" />";
			var carrierCode = "<c:out value='${requestScope.carrier}' escapeXml='false' />";
			var isIncentiveEnabled = false;
			var payRefEnabled = false;
			var maintainAgentBspCredLmt = false;
			var airlineAgentType = "<c:out value="${requestScope.reqAirlineAgentType}" escapeXml="false" />";
			isIncentiveEnabled = "<c:out value="${requestScope.taIncentiveEnabled}" escapeXml='false' />";
			payRefEnabled = "<c:out value="${requestScope.payRefEnabled}" escapeXml='false' />";
			var defaultAirlineCode = "<c:out value="${requestScope.reqDefaultAirlineCode}" escapeXml="false" />";
			var isRemoteUser = 	"<c:out value="${requestScope.reqIsRemoteUser}" escapeXml="false" />";
			var isAllowAirlineCarrierPrefix= 	"<c:out value="${requestScope.reqIsAllowCarrierPrefix}" escapeXml="false" />";
			isHandlingFeeInfoEnable = "<c:out value="${requestScope.reqAgentHandlingFee}" escapeXml="false" />";
			isRouteSelectionEnable = "<c:out value="${requestScope.reqAgntRouteSelection}" escapeXml="false" />";
			maintainAgentBspCredLmt = "<c:out value="${requestScope.maintainAgentBspCredLmt}" escapeXml='false' />";
			hasStatusChangePriv = <c:out value="${requestScope.hasStatusEditPrivilege}" escapeXml='false' />;
			var enableMultipleGSAsForAStation = ("true" == "<c:out value="${requestScope.reqMultipleGSAsForAStationEnabled}" escapeXml="false" />");
			var enableGSAV2 = ("true" == "<c:out value="${requestScope.reqGSAStructureV2Enabled}" escapeXml="false" />");
			var sysDate = "<c:out value="${requestScope.reqSysDate}" escapeXml="false" />";
			var showCreditSummaryFlag = ("true" == "<c:out value="${requestScope.reqShowCreditSummary}" escapeXml="false" />");
			
			<c:out value="${requestScope.reqVisibleTerritoriesStations}" escapeXml="false" />
			<c:out value="${requestScope.reqAgentCreditSummary}" escapeXml="false" />
		</script>  
  </head>
<!--<body style="background: transparent;">-->
<body style="background: transparent;" oncontextmenu="return false" 
ondrag="return false" scroll="no">
	  <div style="width: 918px;margin: 0px">
	  <form action="showTravelAgent.action" method="post" id="frmTravelAgent" name="frmTravelAgent">
			<div><%@ include file="../common/IncludeMandatoryText.jsp"%></div>
			<div>
					<%@ include file="../common/IncludeFormTop.jsp"%>Search Agent<%@ include file="../common/IncludeFormHD.jsp"%>
					 <table width="100%" border="0" cellpadding="0" cellspacing="1" ID="tblHead">
						<tr>
							<td width=16%><input type="radio" name="chkGroup1" id="chkGroup1" value="TA" onclick="chkClick()" class="NoBorder" style="margin:0;height: auto" tabindex="1" checked /><font>&nbsp; Agent</font></td>
						  	<td width=10%><font>Station</font></td>
						    <td width=18%></td>
						     <td width=45% align="left">
						   <table width="100%" border="0" cellpadding="0" cellspacing="0">
						    <tr>
						    <td width=50% align="left">
						    <font> Name </font>&nbsp;
								<input type="text" id="txtSearchName" name="txtSearchName" value="" style="width:100px" tabindex="3" maxlength="50"/>
						   </td>
						   <td width=50% align="left">
						    <font> Code </font>&nbsp;
						    		<select title="Airline Code" name="selAirlineCode"  id="selAirlineCode" size="1" tabindex="4" style="display: none">	
										<option VALUE="ALL">ALL</option>
										<c:out value="${requestScope.reqAirlineList}" escapeXml="false" />								 
								    </select>
									<input type="text" id="txtAgentCode" name="txtAgentCode" value="" style="width:80px" tabindex="4" maxlength="15"/>
						   </td>
						    </tr>
						    </table>	
						    </td>					  
						    <td width=15% align=center rowspan=2 valign="bottom">
						   		<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search" align="right" onclick="searchClick()" tabindex="7" />
						   	</td>
						 </tr>						 
						 <tr>
						 	<td width=20%>
						 	<u:hasPrivilege privilegeId="ta.maint.add.any">
						 		<input type="radio" style="margin:0;height: auto" name="chkGroup1" id="chkGroup2" value="GSA" class="NoBorder" onclick="chkClick()" tabindex="1"/><font>&nbsp; GSA</font>
						 	</u:hasPrivilege>
						 	</td>
						  	<td width=10%>
						  	<u:hasPrivilege privilegeId="ta.maint.add.any">
						  		<font>Territory</font>
						  	</u:hasPrivilege>
						  	</td>
						  	<td width=10%></td>
						  	<td width=10%>
						  	 <table width="100%" border="0" cellpadding="0"><tr><td width=50% align="left">
							  	<font>Status</font>&nbsp; 
							  	<select id="selSearchStatus" name="selSearchStatus" size="1" tabindex="7">
							  		<option value="ALL">All</option>
							  		<option value="ACT">Active</option>
							  		<option value="NEW">New</option>
							  		<option value="BLK">Blacklisted</option>
							  		<option value="INA">In-Active</option>
							  	</select>
							  	</td>
							  	<td width="50%" align="left">
									<font>IATA Code</font><span id="spnIataCode" style="float:right;display:none"></span>
									<input type="text" name="txtAgentIataCode" id="txtAgentIataCode" style="width:110px" maxlength="10%" size="" tabindex="2">
								</td>
							  	</td>
							  	</tr>
							  	</table>
							  	</td>
						 </tr>
					</table>
				  	<%@ include file="../common/IncludeFormBottom.jsp"%>
				  	</div>
				<div>
				<%@ include file="../common/IncludeFormTop.jsp"%>Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">					  		
						<tr><td colspan="2"><span id="spnTravelAgents" style='height:160px;'></span></td></tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="ta.maint.add">
									<input name="btnAdd"  type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()" tabindex="8">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="ta.maint.edit">
									<input name="btnEdit"  type="button" class="Button" id="btnEdit" value="Edit" onClick="editClick()" tabindex="9">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="ta.maint.delete">
									<input name="btnDelete"  type="button" class="Button" id="btnDelete" value="Delete" onClick="deleteClick()" tabindex="10">
								</u:hasPrivilege>								  	
							</td>
							<td align="right">
								<u:hasPrivilege privilegeId="ta.maint.incentive">
									<input name="btnIncentiveScheme"  type="button" class="Button" id="btnIncentiveScheme" value="Agent Incentive Scheme" style="width:165px" onClick="incentiveSchemeClick()" tabindex="10"/>
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="ta.maint.stock">
									<input name="btnStockLimit"  type="button" class="Button" id="btnStockLimit" value="View/Adjust Ticket Stock" style="width:175px" onClick="tktStockClick()" tabindex="10"/>
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="ta.maint.ticket">
									<input name="btnTktRange"  type="button" class="Button" id="btnTktRange" value="View/Adjust Ticket Range" style="width:175px" onClick="tktStockClick()" tabindex="11"/>
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="ta.maint.credit">
									<input name="btnCreditLimit"  type="button" class="Button" id="btnCreditLimit" value="View/Adjust Credit limit" style="width:175px" onClick="creditLimitClick()" tabindex="11"/>
								</u:hasPrivilege>									
							</td>
						</tr>
					</table>
				  	<%@ include file="../common/IncludeFormBottom.jsp"%>
				  	</div>
				<div>
				<%@ include file="../common/IncludeFormTop.jsp"%>Manage Travel Agent<%@ include file="../common/IncludeFormHD.jsp"%>
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1" >Agent Master Data</a></li>
						<li><a href="#tabs-2">Credit Related Information</a></li>
						<li> <u:hasPrivilege privilegeId="ta.maint.handlingfee"> <a href="#tabs-3">Handling Fee Information</a> </u:hasPrivilege></li>
						<li id="tabVisibleTerSta"><a href="#tabs-4">Visible Territory/Station</a></li>
						<li id="tabRouteSelection"><a href="#tabs-5">Route Selection</a></li>
						<li><span id="spnError">&nbsp;</span></li>
					</ul>
					<div id="tabs-1" style="padding: 0.5em 1.4em;">
					<table width="98%" border="0" cellpadding="0" cellspacing="0" id="tblHead">
						<tr>
							<td width="11%"><font>Agent Type</font></td>   
							<td width="21%">
								<select id="selAgentType" name="selAgentType" size="1" onChange="selAgentType_onChange(true)" tabindex="12" style="margin-bottom: 1px">
									<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />
								</select>	
								<font class="mandatory">&nbsp;*</font>
							</td>
							<td width="12%"><font><span id="showTerrt"></span></font></td>
							<td colspan="2" width="28%">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="45%">&nbsp;</td>											
										<td width="45%"><font class="mandatory"><span id="spnMandGSA">*</span></font></td>
										<td width="10%"><font class="mandatory"><span id="spnMand">*</span></font></td>
									</tr>
								</table>
							</td>								
							<td width="11%"><font>Country</font></td>
							<td><font class="fntBold"><span id='spnCountry'></span></font></td>	
						</tr>
						<tr>
							<td><font>Name</font></td>
							<td>
								<input type="text" id="txtName" name="txtName" maxlength="50" onKeyUp="validateAlphaNumericAnper(this)" 
								onkeypress="validateAlphaNumericAnper(this)"  tabindex="13" style="margin: 0px;padding:0px;font-size: 11px"/>
								<font class="mandatory">&nbsp;*</font>
							</td>
							<td ><font>Agent / GSA ID</font></td>
							<td colspan="2"><font class="fntBold"><span id='spnAgentCode'></span></font></td>
							<!--<td width="10%"><font></font></td>-->
							<td><font>Currency</font></td>
							<td>
								<!--<table width="100%" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="65%"><font>&nbsp;</font></td>
										<td align="left"><font class="mandatory">&nbsp;*</font></td>
									</tr>
								</table>-->
							</td>
						</tr>
						<tr>
							<td><font>Address</font></td>
							<td>
								<input type="text" id="txtAdd1" name="txtAdd1" maxlength="50" size="20" onKeyUp="validateAlphaNumeric(this)" 
								onkeypress="validateAlphaNumeric(this)" invalidText="true" tabindex="14" style="margin: 0px;padding:0px;font-size: 11px"/>
								<font class="mandatory">&nbsp;*</font>
							</td>
							<td>
								<u:hasPrivilege privilegeId="sys.security.role.servicechannel.view">
								<font>Service Channel</font>
								</u:hasPrivilege>
							</td>
							<td colspan="2" valign="top">
								<u:hasPrivilege privilegeId="sys.security.role.servicechannel.view">
								<select id="selServiceChannel" name="selServiceChannel" size="1" tabindex="22">
									<c:out value="${requestScope.reqServiceChannels}" escapeXml="false" />
								</select>	
								<font class="mandatory">*</font>
								</u:hasPrivilege>
							</td>
							<td><font>IATA Number</font></td>
							<td>
									<input type="text" id="txtIATA" name="txtIATA" maxlength="8" onKeyUp="validateAlphaNumeric(this)" 
									onkeypress="validateAlphaNumeric(this)" tabindex="29" style="margin: 0px;padding:0px;font-size: 11px"/>
							</td>
						</tr>
						<tr>
							<td><font>&nbsp;</font></td>
							<td>
								<input type="text" id="txtAdd2" name="txtAdd2" maxlength="25" onKeyUp="validateAlphaNumeric(this)" 
								onkeypress="validateAlphaNumeric(this)" invalidText="true" tabindex="14" style="margin: 0px;padding:0px;font-size: 11px"/>
							</td>
							<td>
								<font>Contact Person</font>
							</td>
							<td colspan="2">
							 <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
							 	<tr>
								 	<td>
								 		<select name="selContactPersonDes" id="selContactPersonDes" size="1" style="width:40px;font-size:9px;" tabindex="23">
								 		<option value="">--</option>
								 		<option value="MR">Mr</option>
								 		<option value="MISS">Miss</option>
								 		<option value="MRS">Mrs</option>
								 		</select>
								 	</td>
								 	<td>
								 		<input type="text" id="txtContactPerson" name="txtContactPerson" maxlength="50" 
								 		onchange="setPageEdited(true)" tabindex="24" style="margin: 0px;padding:0px;font-size: 11px"/>
								 	</td>
							 	</tr>
								</table>
							</td>
							<td><font>Status</font></td>
							<td>
								<span id ="spnSelStatus"></span>
							</td>
						</tr>
						<tr>
							<td><font>City</font></td>
							<td >
									<input type="text" id="txtCity" name="txtCity" maxlength="25" onKeyUp="validateAlphaNumeric(this)" 
									onkeypress="validateAlphaNumeric(this)" tabindex="15" style="margin: 0px;padding:0px;font-size: 11px"/>
									<font class="mandatory">&nbsp;*</font>
							</td>
							<td>
								<font>License Number</font>
							</td>
							<td colspan="2">
								<input type="text" id="txtLicense" name="txtLicense" maxlength="14" onChange="setPageEdited(true)" 
								tabindex="25" style="margin: 0px;padding:0px;font-size: 11px"/>
							</td>
							<td valign="top"><font>Notes</font></td>
							<td  rowspan="2" valign="top">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td valign="top" align="right">
											<textarea name="txtNotes" id="txtNotes" cols="15" rows="2" onKeyUp="validateAgentNotesTextArea(this)" 
											style="margin: 0px;padding:0px;font-size: 11px;height: 30px" onKeyPress="validateAgentNotesTextArea(this)" 
											tabindex="31"></textarea>
										</td>
									</tr>
								</table>								
							</td>
						</tr>
						<tr>
							<td><font>State / Province</font></td>
							<td>
								<input type="text" id="txtState" name="txtState" maxlength="20" onKeyUp="alphaValidate(this)" 
								onkeypress="alphaValidate(this)" tabindex="16" style="margin: 0px;padding:0px;font-size: 11px"/>
							</td>
							<td> <font>Pref. Language</font> </td>	
							<td colspan="3"> <span id="spnLanguages"></span></td>
							</tr>
						<tr>
							<td><font>Postal Code</font></td>
							<td>
								<input type="text" id="txtPostal" name="txtPostal" maxlength="20" onKeyUp="validateAlphaNumeric(this)" 
								onkeypress="validateAlphaNumeric(this)"  tabindex="17" style="margin: 0px;padding:0px;font-size: 11px"/>
							</td>
							
							<td>
								<font>Pref. Report Format</font>
							</td>
							<td colspan="2">
								<select name="selPrefRprFormat" id="selPrefRprFormat" size="1" style="width:60px" onChange="setPageEdited(true)" title="Select Pref. Format" tabindex="26">
									<option value="US" selected="selected">US</option>
									<option value="FR">EURO</option>																								
								</select>								
							</td>
							<td>
								<font>Publish To Web</font>									
							</td>	
							<td>
								<input type="checkbox" name="chkPublish" id="chkPublish" value="on" class="NoBorder" onClick="" 
								tabindex="32" onChange="setPageEdited(true)" style="height: 16px;margin: 0" />
							</td>
						</tr>
						<tr>
							<td><font>Telephone</font></td>
							<td>
								<input type="text" id="txtTelNo" name="txtTelNo" maxlength="30" onKeyUp="validateAlphaNumeric(this)" 
								onkeypress="validateAlphaNumeric(this)"  tabindex="18" style="margin: 0px;padding:0px;font-size: 11px" />
								<font class="mandatory">&nbsp;*</font>
							</td>
							<td ><font>Google Map URL</font></td>
							<td colspan="2">								
								<input type="text" id="txtLocationURL" name="txtLocationURL" onChange="setPageEdited(true)" tabindex="27"
								style="margin: 0px;padding:0px;font-size: 11px" />
							</td>
							<td>
								<span id="showGDSOHD">
									<font>Allow GDS On Hold</font>
									</span></td>
                            <td><span id="showGDSOHD"><input type="checkbox" name="chkOnHold" id="chkOnHold" class="NoBorder" onClick="validateCheck()" 
									tabindex="33" style="height: 16px;margin: 0" />
								</span>
							</td>
						</tr>
						<tr>
							<td><font>Fax</font></td>
							<td>
								<input type="text" id="txtFaxNo" name="txtFaxNo" maxlength="20" 
								onkeyup="positiveInt(this)" onKeyPress="positiveInt(this)"  tabindex="19" style="margin: 0px;padding:0px;font-size: 11px" />
							</td>
							<td colspan="3">
								<span id="showAgentComm"></span> 
							</td>
							<td><font>Report to </font></td>
							<td>
								<table>
									<tr>
										<td>
											<input type="checkbox" id="chkGSA" name="chkGSA" onclick="setGSADropDown()" class="NoBorder" tabindex="34" style="height: 16px;margin: 0" />
											<font class="fntBold">&nbsp;&nbsp;<span id='spnGSA'></span></font>
										</td>
										<td id="selectGSA" style="width: 127px;">
						                    <select title="General Sales Agent" name="selGSA" id="selGSA"  style="width: 113px;">
						                        <option VALUE=""></option>
						                        <c:out value="${requestScope.reqGSAList}" escapeXml="false" />
						                    </select>
											<font class="mandatory"> &nbsp;* </font>									
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><font>Mobile </font></td>
							<td><input type="text" id="txtMobile" name="txtMobile" maxlength="20" style="margin: 0px;padding:0px;font-size: 11px" onKeyUp="positiveInt(this)" 
							onkeypress="positiveInt(this)"  tabindex="20" /></td>
							
								<c:choose>
									<c:when test="${requestScope.reqShowCharterAgentOption}">
										<td ><font>Charter Agent </font></td>
										<td colspan="2"><font><input type="checkbox" id="chkCharterAgent" name="chkCharterAgent" value="on" class="NoBorder" tabindex="27" style="height: 16px;margin: 0" /></font>
   										&nbsp;&nbsp;&nbsp;&nbsp;
   									</c:when>
   									<c:otherwise>
   										<td colspan="3">
   										<input type="checkbox" id="chkCharterAgent" name="chkCharterAgent" value="" class="NoBorder" tabindex="27" style="height: 16px;margin: 0; display: none" />
   										&nbsp;&nbsp;&nbsp;&nbsp;
   									</c:otherwise>
   								</c:choose>
   								
								<u:hasPrivilege privilegeId="ta.maint.status.edit">								
								<font title="Agent Inactive Date">Inactive Date</font>
								<input type="text" id="txtInactiveDate" name="txtInactiveDate" maxlength="10" onkeyup="" onkeypress="" onblur="dateChk('txtAdvnaceExpiryDate')"  
										size="10"/>
								<a href="javascript:void(1)" onclick="LoadCalendar(1,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0"></a>
								<input type="text" id="txtInactiveTime" name="txtInactiveTime" maxlength="5" onkeyup="" onkeypress=""  
										size="5"/>
								</u:hasPrivilege>
								</td>		
							<td id="agentLevelETicket" ><font>Enable Agent Level E-Tickets</font></td>
							<td><font><input type="checkbox" id="chkEnableAgentLevelEticket" name="chkEnableAgentLevelEticket" value="on" class="NoBorder" tabindex="34" style="height: 16px;margin: 0" /></font>&nbsp;&nbsp;&nbsp;&nbsp;
								<c:if test="${requestScope.reqShowFareMaskOption}">
									<font>Fare Mask</font>&nbsp;&nbsp;<font><input type="checkbox" id="chkEnableAgentLevelFareMask" name="chkEnableAgentLevelFareMask" value="on" class="NoBorder" tabindex="34" style="height: 16px;margin: 0" /></font>
								</c:if>
								<c:if test="${requestScope.reqShowFareMaskOption == false}">
									<input type="checkbox" id="chkEnableAgentLevelFareMask" name="chkEnableAgentLevelFareMask" value="" class="NoBorder" tabindex="34" style="height: 16px;margin: 0; display:none" />
								</c:if>
							</td>
						</tr>
						<tr>
							<!-- Email addresses to Send notifications about the creation of the agent and future agent update notifications  -->
							<td valign="top"><font>Emails to Notify</font></td>
							<td>
								<textarea name="txtEmailsToNotify" id="txtEmailsToNotify" cols="15" rows="1" onKeyUp="" 
								style="margin: 0px;padding:0px;font-size: 11px;height: 23px" onKeyPress="" 
								tabindex="21" title="Emails Separated by Commas"></textarea>			
							</td>
							<td>
								<font>Max Adults Allowed</font>
							</td>
							<td>
								<input type="text" id="maxAdultAllowed" name="maxAdultAllowed" maxlength="3" onkeyup="" onkeypress="positiveInt(this)" size="3"/>
							</td>
						</tr>						
					</table>
				</div>
				<div id="tabs-2" style="height:180px;overflow-y:auto">
					<table width="98%" border="0" cellpadding="0" cellspacing="1" ID="tblHead2">
							<tr>
								<td width="15%"><font>Payment Method(s)</font></td>	
								<td valign="top" colspan="1" width="35%">
									<table width="100%">
										<tr>
											<td valign="top" colspan="2" align="left">
												<span id="spnOA"><input type="checkbox" name="chkOnAcc" id="chkOnAcc" value="" class="NoBorder" tabindex="35" /><font>&nbsp;On Account&nbsp;</font></span>
												<span id="spnCH"><input type="checkbox" name="chkCash" id="chkCash" value="" class="NoBorder" tabindex="36" /><font>&nbsp;Cash&nbsp;</font></span>
												<span id="spnCC"><input type="checkbox" name="chkCC" id="chkCC" value="" class="NoBorder" tabindex="37" /><font>&nbsp;Credit Card&nbsp;</font></span>
												<span id="spnBSP"><input type="checkbox" name="chkBSP" id="chkBSP" value="" class="NoBorder" tabindex="37" /><font>&nbsp;BSP&nbsp; </font></span>
												<span id="spnVoucher"><input type="checkbox" name="chkVoucher" id="chkVoucher" value="" class="NoBorder" tabindex="37" /><font>&nbsp;Voucher&nbsp; </font></span>
												<span id="spnOffline"><input type="checkbox" name="chkOffline" id="chkOffline" value="" class="NoBorder" tabindex="38" /><font>&nbsp;Offline&nbsp; </font></span>
												<font class="mandatory">&nbsp;*</font>
											</td>											
										</tr>
									</table>
								</td>
								<td width="3%"></td>
								<td width="17%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							
							<tr>
								<td><font>Email ID</font></td>
								<td>
									<input type="text" id="txtEmail" name="txtEmail" maxlength="100" onchange="setPageEdited(true)" tabindex="38" />
									<font class="mandatory">&nbsp;*</font>
								</td>
								<td></td>
								<td  onmouseover="setAmountSpn(event)" onmouseout="hideTooltip()"><font>Amounts in </font></td>
								<td align="left">								
									<span id="spnPayIn"  onmouseover="setAmountSpn(event)" onmouseout="hideTooltip()"></span>	
									<span id="spnBSPText">(BSP amounts are recorded in station currency)</span>		
								</td>
							</tr>
							<tr>
								<td><font>Billing Email ID</font></td>
								<td>
									<input type="text" id="txtBillingEmail" name="txtBillingEmail" maxlength="100" onchange="setPageEdited(true)" tabindex="39"/>
								</td>
								<td></td>
								<td>
									<!--<u:hasPrivilege privilegeId="ta.maint.handlingfee">
										<font>Handling Charge </font>
									</u:hasPrivilege>
								--></td>
								<td>
									<!--<u:hasPrivilege privilegeId="ta.maint.handlingfee">
										<input type="text" id="txtHandlingCharge" name="txtHandlingCharge" maxlength="16" onChange="setPageEdited(true)" class="rightText" onKeyUp="validateHandlingChrg()" 
										onKeyPress="validateHandlingChrg()"  tabindex="44" size="16"/>
										<font>&nbsp;<span id="baseCurr2"></span></font>
									</u:hasPrivilege>
								--></td>
							</tr>
							<tr>
								<td><font>Account Code</font></td>
								<td>
									<input type="text" id="txtAccount" name="txtAccount" maxlength="20" onkeyup="validateAlphaNumeric(this)" onkeypress="validateAlphaNumeric(this)"  tabindex="40"/>
									<font class="mandatory">&nbsp;*</font>
								</td>
								<td></td>
								<td colspan="2">
									<span id="showAutoInv">
									<table  border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
										<tr>
											<td width="46%"><font>Auto invoice</font>&nbsp;&nbsp;&nbsp;</td>
											<td><input type="checkbox" id="chkAutoInvoice" name="chkAutoInvoice" class="NoBorder" tabindex="45" style="height: 16px;margin: 0" /></td>
										</tr>
									</table>
									</span>
								</td>							
							</tr>
							<tr id="trCreditSummary">	
								<td colspan="2">				
									<table border="0" cellpadding="0" cellspacing="0" align="left" width="50%" class="ui-widget ui-widget-content ui-corner-all">
										<tbody>
										<tr>
											<td class="paneHD" colspan="2" align="left"><font class="txtWhite txtBold">Credit Summary</font></td>
										</tr>
										<tr>
											<td width="60%" align="left"><font>Agent Credit Limit</font></td>
											<td align="left"><font><span id="spnAvailableCredit"></span></font></td>
										</tr>
										<tr>
											<td width="60%" align="left"><font>Agent Shared Credit</font></td>
											<td align="left"><font><span id="spnSharedCredit"></span></font></td>
										</tr>
										<tr>
											<td width="60%" align="left"><font>Agent Distributed Credit</font></td>
											<td align="left"><font><span id="spnDistributedCredit"></span></font></td>
										</tr>
										</tbody>
									</table>
								</td>								
							</tr>
							<tr>
								<td colspan="2">
									<table border="0" cellpadding="0" cellspacing="0" align="left" width="100%">
										<tr id="trFixedCredit">
											<td width="30%" align="left"><font>Has Fixed Credit Limit</font></td>
											<td align="left"><input type="checkbox" name="chkFixedCredit" id="chkFixedCredit" class="NoBorder" onclick="validateCheckFixedCredit()" tabindex="46" style="height: 16px;margin: 0"/></td>
										</tr>
										<tr id="trCreditLimit">
											<td width="30%"><font>Credit Limit</font></td>
											<td>
												<input type="text" id="txtCredit" name="txtCredit" maxlength="16" onkeyup="validateDecimal('txtCredit')" onkeypress="validateDecimal('txtCredit')" 
												onblur="updateLocCreditLimit()" class="rightText" size="16" tabindex="41"/>
												 &nbsp;<font><span id="baseCurr"></span></font>
												<input type="text" id="txtLocCredit" name="txtLocCredit" maxlength="19" onkeyup="validateDecimal('txtLocCredit')" onkeypress="validateDecimal('txtLocCredit')" 
												onblur="updateBaseCreditLimit()" class="rightText" tabindex="41" size="16" />
												<font>&nbsp;<span id="LocCurr"></span></font><font class="mandatory">&nbsp;*</font>
											</td>
										</tr>
										<tr  id="trBankGuarantee" >
											<td valign="top"><font>Bank Guarantee</font></td>
											<td valign="top">
												<input type="text" id="txtAdvnace" name="txtAdvnace" maxlength="13" onkeyup="positiveInt(this)" onkeypress="positiveInt(this)" onblur="updateLocBankGuarantee()"  
												class="rightText" tabindex="42" size="16"/>
												&nbsp;<font><span id="baseCurr1"></span></font>
												<input type="text" id="txtLocAdvnace" name="txtLocAdvnace" maxlength="13" onkeyup="positiveInt(this)" 
												onkeypress="positiveInt(this)" onblur="updateBaseBankGuarantee()"  class="rightText" tabindex="42" size="16"/>
												<font>&nbsp;<span id="LocCurr1"></span></font><font class="mandatory">&nbsp;*</font>
											</td>
										</tr>
									    <tr  id="trBankGuaranteeExpire" >							
								    		<td valign="top"><font title="Bank Guarantee Expiry Date">Bank Guarantee Exp.</font></td>
											<td valign="top">
												<input type="text" id="txtAdvnaceExpiryDate" name="txtAdvnaceExpiryDate" maxlength="10" onkeyup="" onkeypress="" onblur="dateChk('txtAdvnaceExpiryDate')"  
												tabindex="42" size="10" invalidText="true"/>
												<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0"></a>
												<%-- <font class="mandatory">&nbsp;*</font> --%>
											</td>								
							    			<td colspan="3"></td>
										</tr>
									</table>
								
								</td>
								<td></td>
								<td colspan="2">
									<table  border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
										<tr>
											<td width="46%"><font>Billing</font></td>
											<td><input type="checkbox" name="chkBilling" id="chkBilling" value="" class="NoBorder" onclick="validateCheck()" tabindex="46" style="height: 16px;margin: 0"/></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td rowspan="2" colspan="2">
									<span id="spnPerofacPay" style="">
									  <table  border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
										<tr>
											<td width="46%"><font>Capture Ext Pay Reference</font></td>
											<td>
												<input type="checkbox" name="chkCapExtPay" id="chkCapExtPay" value="on" class="NoBorder" 
												onclick="" tabindex="47" onchange="setPageEdited(true);payRefValidation()" style="margin: 0px;height: 16px"/>
											</td>
										</tr>
										<tr>
											<td><font>External Pay Ref Mandatory</font></td>
											<td><input type="checkbox" name="chkExtPayMand" id="chkExtPayMand" value="on" class="NoBorder" 
											onclick="" tabindex="48" onchange="setPageEdited(true)" style="margin: 0px;height: 16px"/></td>
										</tr>
									</table>
									</span>
								</td>
							</tr>
							<tr>							
								<td valign="top"><font>BSP Guarantee</font></td>
								<td valign="top">
									<input type="text" id="txtBSPGuarantee" name="txtBSPGuarantee" maxlength="13" onkeyup="validateDecimal('txtBSPGuarantee')" onkeypress="validateDecimal('txtBSPGuarantee')" onblur="updateLocBSPGuarantee()"  
									class="rightText" tabindex="42" size="16"/>
									&nbsp;<font><span id="baseCurrBSP"></span></font>
									<input type="text" id="txtLocBSPGuarantee" name="txtLocBSPGuarantee" maxlength="13" onkeyup="validateDecimal('txtLocBSPGuarantee')" 
									onkeypress="validateDecimal('txtLocBSPGuarantee')" onblur="updateBaseBSPGuarantee()"  class="rightText" tabindex="42" size="16"/>
									<font>&nbsp;<span id="LocCurrBSP"></span></font>
								</td>								
							    <td colspan="3"></td>
							</tr>	
							<tr>							
								<td colspan="2">
									<div id="divIncentive">
										<table  border="0" cellpadding="0" cellspacing="2" align="left" width="100%">
										<tr>
											<td width="31%"><font>Incentive Scheme</font></td>
											<td><input type="checkbox" name="chkIncentive" id="chkIncentive" value="" class="NoBorder"  tabindex="49"/></td>
										</tr>
										</table> 
							    	</div>
							    </td>
							    <td colspan="3"></td>
							</tr>
							</table>
						</div>
						<div id="tabs-3"class="ui-tabs">
						<ul>
							<li><a href="#tabs-31">Create Reservation</a></li>
							<li><a href="#tabs-32" >Modify Reservation</a></li>
						</ul>
						<div  id="tabs-31"">
						<table width="98%" border="0" cellpadding="0" cellspacing="1" ID="tblHead3">
							<tr>
								<td><font> Applies To </font></td>
								<td>
									<select onchange="setPageEdited(true);enableDisableAgentMinMaxMargin();" id="selAppliesto" name="selAppliesto" size="1" style="width:130px" tabindex="50">
								  		<c:out value="${requestScope.reqChargeAppliesToList}" escapeXml="false" />								  								  		
								  	</select>	
								</td>
								<td></td>
								<td ><font> Type </font></td>
								<td align="left">								
									<select id="selType" name="selType" size="1" style="width:130px" onchange="setPageEdited(true);enableDisableAgentMinMaxMargin();" tabindex="51">
									<c:out value="${requestScope.reqChargeFareBasisToList}" escapeXml="false" />								
									</select>			
								</td>
							</tr>
							
							<tr>
								<td><font>OneWay Amount</font></td>
								<td>
									<input type="text" id="txtOWAmount" name="txtOWAmount" maxlength="16" onchange="setPageEdited(true)" tabindex="52" class="rightText" onKeyUp="validateDecimal('txtOWAmount')" 
										onKeyPress="validateDecimal('txtOWAmount')" />
										
									
								</td>
								<td></td>
								<td><font>Min Amount</font></td>
								<td>
									<input type="text" id="txtMinAmount" name="txtMinAmount" maxlength="16" onchange="setPageEdited(true)" tabindex="53" class="rightText" onKeyUp="validateDecimal('txtMinAmount')" 
										onKeyPress="validateDecimal('txtMinAmount')" />
										&nbsp;<font><span id="baseCurrMin"></span></font>
								</td>
							</tr>
							<tr>
							<td><font>Return Amount</font></td>
								<td>
									<input type="text" id="txtRTAmount" name="txtRTAmount" maxlength="16" onchange="setPageEdited(true)" tabindex="54" class="rightText" onKeyUp="validateDecimal('txtRTAmount')" 
										onKeyPress="validateDecimal('txtRTAmount')" />									
								</td>
								<td></td>
								<td><font>Max Amount</font></td>
								<td>
									<input type="text" id="txtMaxAmount" name="txtMaxAmount" maxlength="16" onchange="setPageEdited(true)" tabindex="55" class="rightText" onKeyUp="validateDecimal('txtMaxAmount')" 
										onKeyPress="validateDecimal('txtMaxAmount')"/>
										&nbsp;<font><span id="baseCurrMax"></span></font>
								</td>					
							 </tr>
							 
							 <tr>
							<td><font>Active</font></td>
								<td>
									<input type="checkbox" name="chkHFActive" id="chkHFActive" value="on" class="NoBorder"  tabindex="56"/>									
								</td>
								<td></td>
								<td></td>
								<td></td>					
							 </tr>
							</table>
						</div>
						<div  id="tabs-32">
						<form>
							<table id="tblHead3" border="0" width="100%" cellspacing="1" cellpadding="0">
								<tbody>
									<tr>
									<td>&nbsp;</td>
									<td colspan="2">
                                    <table>
                                       		<tr>
                                              <td>Apply to</td>
                                              	<td>&nbsp;</td>
      											<td>
      											<select id="selModHandlingAppliesto" name="selModHandlingAppliesto" size="1" style="width:120px">
								  					<c:out value="${requestScope.reqChargeAppliesToList}" escapeXml="false" />							  								  		
								  				</select>
      											</td>
                                        	 </tr>
                                       </table>  
                                      </td>
                                    <td>&nbsp;</td>
									<td colspan="3">
                                      	<table>
                                       		<tr>
                                                <td>Active</td>
                                                <td>&nbsp;</td>
                                          		<td><input id="chkHFModActiveForAgent" class="NoBorder" tabindex="56" name="chkHFModActiveForAgent" type="checkbox" value="on" /></td>
                                          	</tr>
                                        </table>
                                      </td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td valign="top" colspan="5">
										<table id="handlingFeeList" class="scroll" cellpadding="0" cellspacing="0">	 
										</table> </td>
									</tr>
									<tr>
									<td>&nbsp;</td>
									<td colspan="2">
                                    <table>
                                       		<tr>
                                              <td>Operation </td>
                                              <td>&nbsp;</td>
      											<td ><select id="selHandlingFeeOp" style="width: 120px;" 
      											tabindex="50" name="selHandlingFeeOp" size="1">
      											<c:out value="${requestScope.reqHandlingFeeApplyOperations}" escapeXml="false" />	
      											</select></td>
                                          	</tr>
                                       </table>  
                                      </td>
                                      <td>&nbsp;</td>
                                      <td colspan="3">
                                        	<table>
                                         		<tr>
													<td>Amount</td>
													<td>&nbsp;</td>
													<td><input name="amountModHandlingFee" type="text" id="amountModHandlingFee" size="5" maxlength="3"><font>&nbsp;AED</font></td> <!--<font class="mandatory">&nbsp;*</font>  -->
													<td>&nbsp;</td>
													<td>Type</td>
													<td>&nbsp;</td>
													<td>
													<select id="selHandlingFeeModType" name="selHandlingFeeModType" size="1" style="width:130px">
														<c:out value="${requestScope.reqChargeFareBasisToList}" escapeXml="false" />								
													</select>
													</td>
													<td>&nbsp;</td>
													<td>Status</td>
													<td>&nbsp;</td>
													<td><select id="selHandlingFeeModStatus"  name="selHandlingFeeModStatus"  style="width: 60px;" size="1">
														<c:out value="${requestScope.reqHandlingFeeApplyOpStatus}" escapeXml="false" />
													</select></td>
                                              	</tr>
                                        	</table>
                                      </td>
                                    <td>		
									<input name="btnHFMAdd" type="button" class="Button" id="btnHFMAdd" value="Add" onClick="addHFMClick()"></input>
									</td>		
									<td><input name="btnHFMEdit" type="button" class="Button" id="btnHFMEdit" value="Change" onClick="editHFMClick()"></input>
									</td>
									</tr>
							</tbody>
						</table>
						</form>
						</div>
						</div>
							<div id="tabs-4">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
								 	<tr> 
										<td valign="top" width="100%" colspan="2">
												<span id="spnTerritoryStationMultiSelect" class="FormBackGround"></span>
										</td>
									 </tr>	
				                </table>
						</div>
						<div id="tabs-5">
							<table border="0" width="80%" id="tblSelApplicableOnd">
								<tr>
									<td width="17%">
										<font>Applicable OnDs</font> &nbsp; <font class="mandatory">&nbsp;*</font>
									</td>
									<td></td>
									<td width="16%"></td>
									<td></td>
								</tr>
								<tr>
									<td width="17%"><font>Departure</font></td>
									<td >
										<select id="selDepature" name="selDepature" size="1" style="width:60px;">
										<option value="All">All</option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td width="16%"><font>Arrival</font></td>
									<td>
										<select id="selArrival" name="selArrival" size="1" style="width:60px;">
											<option value="All">All</option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
								</tr>
								<tr>
									<td width="17%"><font>Via 1</font></td>
									<td width="17%">
										<select id="selVia1" name="selVia1" size="1" style="width:60px;">
											<option value=""></option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td></td>
									<td width="16%" rowspan="4">
										<select id="selSelected" size="3" name="selSelected" style="width:70px;height:100px" multiple>
										</select>
									</td>
								</tr>
								<tr>							
									<td width="17%"><font>Via 2</font></td>
									<td width="17%">
										<select id="selVia2" name="selVia2" size="1" style="width:60px;">
											<option value=""></option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td align="center">
										<input type="button" id="btnAddSeg" name="btnAddSeg" class="Button" value=">" style="width:40px;height:17px" onClick="btnAddSegment_click()">
									</td>
									<td></td>
								</tr>
								<tr>
									<td width="17%"><font>Via 3</font></td>
									<td width="17%">
										<select id="selVia3" name="selVia3" size="1" style="width:60px;">
											<option value=""></option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td align="center">
										<input type="button" id="btnRemSeg" name="btnRemSeg" class="Button" value="<" style="width:40px;height:17px" onClick="btnRemoveSegment_click()">
									</td>
									<td></td>
								</tr>
								<tr>
									<td width="17%"><font>Via 4</font></td>
									<td width="17%">
										<select id="selVia4" name="selVia4" size="1" style="width:60px;">
											<option value=""></option>
											<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
										</select>
									</td>
									<td></td>												
								</tr>
							</table>
						</div>
					</div>
					<table  border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
						<tr><td style="height: 2px"></td></tr>					
						<tr>
							<td>
								<input type="button" id="btnClose" value="Close" class="Button" onclick="closeClick()" tabindex="57" />							
								<input type="button" id="btnReset" value="Reset" class="Button" onClick="resetClick()" tabindex="58" />
							</td>
							<td align="right">
								<input type="button" id="btnSave" value="Save" class="Button" onClick="saveClick()" tabindex="59" />
							</td>
						</tr>		
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp" %>
					</div>							
	
		<input type="hidden" name="hdnMode" id="hdnMode" value="">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnCode" id="hdnCode" value="">
		<input type="hidden" name="hdnSearchCriteria" id="hdnSearchCriteria" value="">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
		<input type="hidden" name="hdnPaymentmode" id="hdnPaymentmode" value="">		
		<input type="hidden" name="hdnCredit" id="hdnCredit" value="">		
		<input type="hidden" name="hdnAgentCMB" id="hdnAgentCMB" value="">
		<input type="hidden" name="hdnStationCMB" id="hdnStationCMB" value="">
		<input type="hidden" name="hdnTerritoryCMB" id="hdnTerritoryCMB" value="">		
		<input type="hidden" name="selCurrency" id="selCurrency" value="">
		<input type="hidden" name="hdnTerritoryCode" id="hdnTerritoryCode" value="">
		<input type="hidden" name="hdnStationCode" id="hdnStationCode" value="">
		<input type="hidden" name="hdnAgentCommition" id="hdnAgentCommition" value="">
		<input type="hidden" name="hdnPrefLanguage" id="hdnPrefLanguage" value="">
		<input type="hidden" name="selCurrIn" id="selCurrIn" value="">
		<input type="hidden" name="hdnHandling" id="hdnHandling" value="">
		<input type="hidden" name="hdnCurr" id="hdnCurr" value="">
		<input type="hidden" name="hdnPrefRprFormat" id="hdnPrefRprFormat" value="">
		<input type="hidden" name="hdnSearchCode" id="hdnSearchCode" value="">		
		<input type="hidden" name="hdnSearchStatus" id="hdnSearchStatus" value="">		
		<input type="hidden" name="hdnSearchAirline" id="hdnSearchAirline"/>
		<input type="hidden" name="hdnSelStatus" id="hdnSelStatus"/>
		<input type="hidden" name="hdnInactiveDate" id="hdnInactiveDate"/>
		<input type="hidden" name="hdnCountry" id="hdnCountry"/>
		<input type="hidden" name="hdnAgentOrGsaId" id="hdnAgentOrGsaId"/>
		<input type="hidden" name="hdnVisibleTerritoryStationValues" id="hdnVisibleTerritoryStationValues"/>	
		<input type="hidden" name="hdnSearchIataCode" id="hdnSearchIataCode" value="">
		<input type="hidden" name="hdnOndList" id="hdnOndList" value="">
		<input type="hidden" name="hdnHandlingFeeModStr" id="hdnHandlingFeeModStr" value="">
		<input type="hidden" name="hdnHandlingFeeModAct" id="hdnHandlingFeeModAct" value="">
		<input type="hidden" name="hdnHandlingFeeModApp" id="hdnHandlingFeeModApp" value="">
	</form> 
	</div><!-- Page Background Bottom page -->
  </body>	
	<script src="../js/airTravelAgents/agentsValidate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/airTravelAgents/TravelAgentGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	 
  	<script type="text/javascript">
  		winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')
  	</script>
  	<script type="text/javascript">
  		<!--
  		
  		var agentFormMode = "VIEW";
  		
  		var objProgressCheck = setInterval("ClearProgressbar()", 300);
			function ClearProgressbar() {
		    	if (objDG.loaded) {
					clearTimeout(objProgressCheck);
					HideProgress();
				}
			}
			hideColbolayers = function (){
				$("#comboLayer_selTerritoryCmb1").hide();
				$("#comboLayer_selAirportCmb1").hide();
				$("#comboLayer_selCurrencyCmb").hide();
			}
			showColbolayers = function (){
				$("#comboLayer_selTerritoryCmb1").show();
				$("#comboLayer_selAirportCmb1").show();
				$("#comboLayer_selCurrencyCmb").show();
			}
			setVisibleItems = function (){
				if (agentFormMode == "VIEW") {
					var hiddin1 = $("#txtLocCredit").css("visibility");
					var hiddin2 = $("#txtLocAdvnace").css("visibility");
					var hiddin3 = $("#txtLocBSPGuarantee").css("visibility");
					if(!($("#txtLocCredit").is(":visible"))){$("#txtLocCredit").hide()}else{$("#txtLocCredit").show()}
					if(!($("#txtLocAdvnace").is(":visible"))){$("#txtLocAdvnace").hide()}else{$("#txtLocAdvnace").show()}
					if(!($("#txtLocBSPGuarantee").is(":visible"))){$("#txtLocBSPGuarantee").hide()}else{$("#txtLocBSPGuarantee").show()}
				}
			}
			$(function() {
				 $( "#tabs" ).tabs({
					activate: function(event, ui) {
						switch (ui.newPanel[0].id)
						{
							case "tabs-1":
								showColbolayers();
								setVisibleItems();
								
								break;
							case "tabs-2":
								hideColbolayers();
								setVisibleItems();
								break;
							case "tabs-4":
								hideColbolayers();
								break;
							case "tabs-5":
								hideColbolayers();
								break;
							default:
						}
					}
				}); 
				$("#selLanguage").attr("tabindex","25");
			});
			$(function() {
				 $( "#tabs-3" ).tabs({
					activate: function(event, ui) {
						switch (ui.newPanel[0].id)
						{
							case "tabs-31":
							hideColbolayers();
							
							break;
							case "tabs-32":
								hideColbolayers();
								
								break;
							default:
						}
					}
				}); 
			});
  		//-->
  	</script>
  	<%@ include file="../common/IncludeLoadMsg.jsp"%>
</html>