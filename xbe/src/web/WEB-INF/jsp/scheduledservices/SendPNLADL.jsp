<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View/Send - PNL/ADL</title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>			
  </head>
  
  <body oncontextmenu="return false" ondrag="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="no"
  		onload="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>' ,'<c:out value="${requestScope.warningmessage}"/>')" >
  <!--body class="tabBGColor" oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" onbeforeunload="beforeUnload()" -->
  <form method="post" action="saveSendPNLADL.action" id="frmPNLADL" name="frmPNLADL">
	 <%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  			<script type="text/javascript">
				var arrData = new Array();
				var arrPNLADLData = new Array();
				var arrError = new Array();	
				var isDCSWS;//dcs webservice
				var allowManuallySendPnl = false;
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
				<c:out value="${requestScope.reqDefaultMailServer}" escapeXml="false" />
				<c:out value="${requestScope.reqPNLADLTextData}" escapeXml="false" />
				<c:out value="${requestScope.reqIsExceptionOccured}" escapeXml="false" />
				<c:out value="${requestScope.reqParams}" escapeXml="false" />
				allowManuallySendPnl = <c:out value="${requestScope.reqHasManuallySendPnl}" escapeXml='false' />;
			</script>
			
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="Header">View/Send - PNL/ADL</font></td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>

				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>View/Send - PNL/ADL<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td align="left"><font>Date of Flight (Local Time)</font></td>
								<td align="left">
									<input tabindex="1" type="text" name="txtDate" id="txtDate" size="12" maxlength="10" invalidText="true" onBlur="dateValidation('txtDate')" onChange="pageOnChange()">
									<a href="javascript:void(0)"  onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp*</font>  
	                            </td>
								<!--td colspan="3">&nbsp;</td-->
							<!--/tr>
							<tr-->
								<td align="left"><font>Flight Number</font></td>
								<td align="left">
									<input tabindex="2" type="text" name="txtFlightNo" id="txtFlightNo" size="7" maxlength="7" invalidText="true" onkeyUp=" valFlightNumber(this)" onkeyPress=" valFlightNumber(this)" onChange="pageOnChange()"><font class="mandatory">&nbsp*</font>
	                            </td>
								<td align="left"><font>Airport</font></td>
								<td align="left">
									<select tabindex="3" name="selAirport" id="selAirport" onChange="pageOnChange()">
										<option value="-1"></option>
										<c:out value="${requestScope.airportList}" escapeXml="false" />
	                                </select><font class="mandatory">&nbsp*</font>
	                            </td>
								<td width="30%" align="right"><input tabindex="4" name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchData()" value="Search"></td>
							</tr>
					  	</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>PNL/ADLs<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
								<span id="spnReprintGenInv"></span>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Send New/ Resend or Print PNL/ADL
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<br>
							  <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
								<tr><td colspan="4" height="200px">
									<span id="spnInput">
										 <table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td width="30%"><span tabindex="5" id="spn1" class="FormBackGround" style="visibility: hidden;"></span></td>
												<td align="center" valign="Bottom"> <font>&nbsp;&nbsp;Mail&nbsp;Server</font> </td>
												<td align="left" valign="Bottom">
												<select tabindex="6" size="7" name="selMailServer" id="selMailServer" onChange="pageOnChange()">
													<option value="-1"></option>
													<c:out value="${requestScope.reqMailServerList}" escapeXml="false" />
													</select><font class="mandatory">&nbsp*</font>
												</td>
												
											</tr>
										</table>
										</span>

									</td></tr>
									<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
									<tr>
									  <td style="height:42px;" colspan="4" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="2">
													<input tabindex="7" type="button" id="btnClose" class="Button" value="Close"  onclick="top.LoadHome();">	
													<input tabindex="8" name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetData()" value="Reset">
												 </td>
												 <td align="right" colspan="2">
													<input tabindex="9" name="btnSendNew" type="button" class="Button" id="btnSendNew" onClick="sendNew()" style="width:100px" value="Send New">
													<input tabindex="10" name="btnResend" type="button" class="Button" id="btnResend" onClick="reSend()" style="width:100px" value="Resend">
													<!--input tabindex="11" name="btnPrint" type="button" class="Button" id="btnPrint" onclick="ctrl_print_click();CWindowOpen(1)" value="Print"-->
													<input tabindex="11" name="btnPrint" type="button" class="Button" id="btnPrint" onclick="ctrl_print_click()" value="Print">
												 </td>
											</tr>
										</table>
									   </td>
									</tr>
							  </table>
							
						  	<%@ include file="../common/IncludeFormBottom.jsp"%>  
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  		<%@ include file="../common/IncludeShortCuts.jsp"%>  	
		<%@ include file="../common/IncludeLoadMsg.jsp"%>
		<input type="hidden" name="hdnVersion" id="hdnVersion"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnADLTimestamp" id="hdnADLTimestamp"/>
		<input type="hidden" name="hdnPNLTimestamp" id="hdnPNLTimestamp"/>
		<input type="hidden" name="hdnmsgType" id="hdnmsgType"/>
		<input type="hidden" name="hdnSITAValues" id="hdnSITAValues"/>	
		<input type="hidden" name="hdnTransmitStatus" id="hdnTransmitStatus"/>	
		<input type="hidden" name="hdnFlightID" id="hdnFlightID"/>	
		<input type="hidden" name="hdnTS" id="hdnTS"/>
		<input type="hidden" name="hdnRowSITA" id="hdnRowSITA"/>	

	
	</form>
		<script type="text/javascript">
			<c:out value="${requestScope.reqSITAData}" escapeXml="false" />	
		</script>
				<script type="text/javascript">
	  	<!--
	
		  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
		  	function ClearProgressbar(){
		  			if (typeof(objDG) == "object"){
				  		if (objDG.loaded){				  			
				  			clearTimeout(objProgressCheck);
				  			//top[2].HideProgress();
				  			//showAlertResponceMessage();
							HideProgress();
				  		}
				  	}
		  	}
		  	
		  	//-->
	  </script>
  </body>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
	<script src="../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/scheduledservices/SendPNLADLValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/scheduledservices/SendPNLADLGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
  
</html>