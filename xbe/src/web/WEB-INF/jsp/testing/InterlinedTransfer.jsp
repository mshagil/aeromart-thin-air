<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Territory Admin</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
</head>

<body>
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<form method="post" action="javascript:reportClick();">
	<table width="99%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan=4><font class="Header">Transfer Interlined Segments</font></td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Transfer Interlined Segments<%@ include file = "../common/IncludeFormHD.jsp" %>
				 	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblrepProblem">
						<tr>
							<td width="25%"><font class="fntBold">Date and Time</font></td>
							<td><input name="txtDateTime" id="txtDateTime" type="text" value=""></td>				
						</tr>
						<tr><td><font class="fntSmall"><input type="hidden" name="hdnMode" id="hdnMode" value="Mode">&nbsp;</font></td></tr>
						<tr><td colspan="2"><font class="fntBold">Please use yyyy.MM.dd.HH.mm.ss format [e.g. (2002.01.29.08.36.33)] Leaving empty leads to pick the default time</font></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>

						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="mandatory"><b><c:out value="${requestScope.reqMessage}"/></b></font></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>

						<tr>
							<td colspan="2">
								<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblrepProblembtn">
									<tr>
										<td>
											<input name="btnTransfer" type="button" class="Button" id="idBtnTransfer" value="Transfer" 
											onclick="reportClick()">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
			  		<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
	</table>
	</form>	
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<%@ include file="../common/IncludeLoadMsg.jsp"%>  
<script type="text/javascript">
function reportClick() {
	setField("hdnMode", "TRANSFER");
	ShowProgress();
	document.forms[0].action = "showInterlinedTransferTest.action";
	document.forms[0].submit();
}
</script>
</html>