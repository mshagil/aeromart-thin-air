<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Test Scheduler Jobs</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
</head>

<body>
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<form name = "test" method="post" action="javascript:reportClick();">
	<table width="99%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan=4><font class="Header">Test Scheduler Jobs</font></td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Test Schedular Jobs<%@ include file = "../common/IncludeFormHD.jsp" %>
				 	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblrepProblem">
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="fntSmall">Command \ Invoke Method &nbsp;<input type="text" length="2" name="hdnMode" id="hdnMode" value="">&nbsp;</font></td></tr>
						<tr><td><font class="fntSmall">Input &nbsp;<input type="text" length="2" name="hdnVal" id="hdnVal" value="">&nbsp;</font></td></tr>
						
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="mandatory">&nbsp; This screen is only enabled in test environments</font></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr><td><font class="fntSmall">Input Message&nbsp;</font></td></tr>						
						<tr><td><b><textarea name="inMsgText" id="inMsgText" cols="50" rows="10"></textarea></b></td></tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>

						<tr>
							<td colspan="2">
								<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblrepProblembtn">
									<tr>
										<td>
											<input name="btnTransfer" type="button" class="Button" id="idBtnTransfer" value="Execute" 
											onclick="reportClick()">
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" 
											onclick="closeClick()">
											<input name="btnEmail" type="button" class="Button" id="btnEmail" value="email" 
											onclick="emailClick()">
											<input name="btnHala" type="button" class="Button" id="btnHala" value="HalaServices" 
											onclick="btnHalaClick()">
											<input name="btnRakInsurance" type="button" class="Button" id="btnRakInsurance" value="Rak Insurance Publishing" 
											onclick="btnRakInsuranceClick()">
											<input name="btnLccAirportPubl" type="button" class="Button" id="btnLccAirportPubl" value="LCC Airport Publishing" 
											onclick="btnLccAirportPublClick()">
											<input name="btnReconcileCSV" type="button" class="Button" id="btnReconcileCSV" value="Reconcile CSV Data" 
											onclick="btnReconcileCSVClick()" style="width: 125px;">
										</td>
									</tr>
									<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
									<tr><td><font class="mandatory"><b><c:out value="${requestScope.reqMessage}"/></b></font></td></tr>
									<tr><td><b><textarea name="outputtext" cols="50" rows="10"></textarea></b></font></td></tr>
									
									<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
								</table>
							</td>
						</tr>
					</table>
			  		<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
	</table>
	</form>	
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<%@ include file="../common/IncludeLoadMsg.jsp"%>  
<script type="text/javascript">
function reportClick() {
	 if(getText("hdnMode").length < 3){
	 alert("Invalid command")
	 }else{
	ShowProgress();
	document.forms[0].action = "commonSchedularJobsTest.action";
	document.forms[0].submit();
	}
}

function emailClick() {
	document.forms[0].action = "commonSchedularJobsTest.action";
	document.forms[0].submit();
	
}

function btnHalaClick(){
	setField("hdnMode", "HALA");
	document.forms[0].action = "commonSchedularJobsTest.action";
	document.forms[0].submit();
}

function btnReconcileCSVClick(){
	setField("hdnMode", "CSV");
	document.forms[0].action = "commonSchedularJobsTest.action";
	document.forms[0].submit();
}

function btnRakInsuranceClick(){
	setField("hdnMode", "INS");
	document.forms[0].action = "commonSchedularJobsTest.action";
	document.forms[0].submit();
}

function btnLccAirportPublClick(){
	setField("hdnMode", "ZAP");
	document.forms[0].action = "commonSchedularJobsTest.action";
	document.forms[0].submit();
}


document.test.outputtext.value = "";
var error = "No Errors";
error = "<c:out value="${requestScope.reqDetailException}" escapeXml="false"/>";
 
document.test.outputtext.value = error;
function closeClick() {
	 top.LoadHome();
	 
}
</script>
</html>