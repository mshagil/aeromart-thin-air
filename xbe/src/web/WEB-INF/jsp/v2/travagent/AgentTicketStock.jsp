<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
 
      <title>View / Adjust Ticket Stock</title>

      <%@ include file='../common/inc_PgHD.jsp' %>
      
      		
	  <script type="text/javascript" src="../js/v2/airTravelAgents/AgentTicketStock.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		
	  <script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
      <script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
      
      <script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

		<script type="text/javascript">		    
			var strAgentCode = "<c:out value="${requestScope.reqAgentCode}" escapeXml="false" />";
			
			var arrError = new Array();
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />	
			
	    </script>
	    
  </head>
  <body class='tabBGColor'>
   
   <div style="margin-top:5px; margin-bottom:5px;margin-left:5px;margin-right:5px;height:500px;">	
	
   <div id="divResultsPanel" style="height:200px;text-align:left;background-color:#ECECEC;">
			<table id="tblAgentTktStock" class="scroll" cellpadding="0" cellspacing="0"></table>
			<div id="divAgentTktStockPager" class="scroll" style="text-align:center;"></div>
						<u:hasPrivilege privilegeId="ta.ticket.seq.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault"/>	
						</u:hasPrivilege>
						<u:hasPrivilege privilegeId="ta.ticket.seq.edit">						
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault"/>					
						</u:hasPrivilege>			
	       </div>
		
	<form id = "frmModify" method="post" action="showAdjustAgentTicketStock!addEditTicketStock.action">
	
	<div id="divDisplayAgentTktStock" style="height:250px;text-align:left;background-color:#ECECEC;">
		<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
				<td width = '50%' valign="top">
					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
					
					    <tr><td>&nbsp;</td></tr>
							
						<tr>
							<td ><font  class="fntBold"> Ticket Sequence Lower bound </font></td>
							<td>
								<input name="agentTicketStockTo.sequenceLowerBound" type="text" class = "aa-input frm_editable" id="tktStockLowerBound" size="20"  maxlength="8"  onkeyup="UI_AgentTicketStock.positiveInt(this)" onkeypress="UI_AgentTicketStock.positiveInt(this)" />						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>
					
					
					     <tr>
							<td ><font  class="fntBold"> Ticket Sequence Upper bound </font></td>
							<td>
								<input name="agentTicketStockTo.sequenceUpperBound" type="text" class = "aa-input frm_editable" id="tktStockUpperBound" size="20"  maxlength="8" onkeyup="UI_AgentTicketStock.positiveInt(this)" onkeypress="UI_AgentTicketStock.positiveInt(this)"  />						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>						
						
					     <tr>
							<td ><font  class="fntBold"> Sequence Name </font></td>
							<td>
								<input name="agentTicketStockTo.sequenceName" type="text" class = "aa-input frm_editable" id="tktStockSequenceName" size="20"  maxlength="20"  />						
							
							</td>
						</tr>	
						
						
						<tr><td>&nbsp;</td></tr>
						
						 <tr>
							<td ><font  class="fntBold"> Recycle Sequence </font></td>
							<td>
								<input name="tktStockIsCycle" type="checkbox" class = "aa-input frm_editable" id="tktStockIsCycle" />
							</td>
						</tr>
							
						<tr><td>&nbsp;</td></tr>					
						
						<tr>
							<td width="20%"><font  class="fntBold">Status</font></td>
							<td  width = "20%">
								<select id="tktStockStatus" name="agentTicketStockTo.status" class="aa-input frm_editable" style="width:100px">
								<option value="ACT">ACT</option>
								<option value="INC">INC</option>
								<option value="CLS">CLS</option>
								</select>
								<font class="mandatory">*</font>							
							</td>							
						</tr>					
						
						<tr><td>&nbsp;</td></tr>					
						
					</table>				
				</td>				
			</tr>						
		</table>
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">
						
					<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" class="btnDefault" />
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault" />
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault" />
						</td>
					</tr>
		</table>
	</div>
			<input type="hidden" name="agentTicketStockTo.agentTicketSeqId" id="hdnTktStockID" value="">
			<input type="hidden" name="agentTicketStockTo.version" id="hdnTktStockVersion" value="">
	
	</form>
	</div>
  </body>
</html>
