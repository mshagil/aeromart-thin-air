	<div id='divAnciPref' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD' i18n_key="passenger_AncillarySelection">
								<p class='txtWhite txtBold'> &nbsp;Ancillary Selection</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trAncillaryOpt'>
							<td class='popBGColor'  align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td colspan="2" align='left' i18n_key="passenger_AncillaryAvailability">Following Ancillaries are available. You can start selection with </td>
									</tr>
									<tr id="trAnciPrefTemp">
										<td>
											<input type="radio" name="radAnciPref"/>
										</td>
										<td align="left">
											<label class='txtBold' ></label>
										</td>
									</tr>
									
								</table>
							</td>							
						</tr>
						<tr id="trAncillaryCntMsg">
							<td class='popBGColor '> &nbsp;&nbsp;Would you like to select ancillary now? </td>
						</tr>
						<tr id="trNoAncillary">
							<td class='popBGColor '> &nbsp;&nbsp;No ancillary available for the trip. </td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>	
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>						
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								<button id="btnAnciPrefToAnci" type="button"  class="btnMargin" i18n_key="btn_Yes">Yes </button>
								<button id="btnAnciPrefToPayment" type="button"   class="btnMargin" i18n_key="btn_No">No </button>
								<button id="btnAnciPrefCancel" type="button"  class="btnMargin" i18n_key="btn_Cancel">Cancel </button>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>