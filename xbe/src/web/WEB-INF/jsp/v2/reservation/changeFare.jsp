<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/reservation/changeFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body class="legacyBody">
<form method='post' action="changeFare.action" id="frmChgFares">
	<div id="divLegacyRootWrapperPopUp" style="display:none;">
		<table style='width:900px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:880px;">
						<table width='97%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td>
									<label class='txtLarge txtBold' id="lblChangeFareHeading" i18n_key="changeFare_ChangeFare">Change Fare</label>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr>
								<td class="ui-widget ui-widget-content ui-corner-all">
									<table width='100%' border='0' cellpadding='1' cellspacing='0'>
										<tr>
											<td class='paneHD'>
												<p class='txtWhite txtBold' i18n_key="changeFare_Fare">Fares</p>
											</td>
										</tr>
										<tr>
											<td  class='singleGap'></td>
										</tr>
										<tr>
											<td style='height:100px' valign='top'>
												<table id="tblSegemnts" class="scroll" cellpadding="0" cellspacing="0" width='100%'></table>
											</td>
										</tr>
										<tr>
											<td align='right'>
												<button id="btnShowFares" type="button" class="btnMargin btnMedium" i18n_key="btn_ShowFares">Show Fares</button>
											</td>
										</tr>
										<tr>
											<td  class='singleGap'></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr>
								<td style='height:375px' valign='top'><div id='divAvailableFares'>
									<table width='100%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td class="ui-widget ui-widget-content ui-corner-all" style='height:370px;' valign='top'>
												<table width='100%' border='0' cellpadding='1' cellspacing='0'>
													<tr>
														<td class='paneHD'>
															<p class='txtWhite txtBold' i18n_key="changeFare__AvailableFares">Available Fares</p>
														</td>
													</tr>
													<tr>
														<td  class='singleGap'></td>
													</tr>
													<tr>
														<td align='center' valign='top'>
															<div id='divFares' style='position:relative;width:860px;height:330px;overflow-X:hidden; overflow-Y:auto;'>
																<table id='tblFares' width='100%' border='0' cellpadding='1' cellspacing='0'>
																	<tbody>
																	</tbody>
																</table>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table></div>
								</td>
							</tr>
							<tr>
								<td  class='doubleGap'></td>
							</tr>
							<tr>
								<td align='right'>
									<button id="btnReCal" type="button" class="btnMargin btnMedium" i18n_key="btn_ReCalculate">Re-Calculate</button><button id="btnConfim" type="button" class="btnMargin" i18n_key="btn_Confirm">Confirm</button><button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
								</td>
							</tr>
							<tr>
								<td  class='doubleGap'></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='inc_pageError.jsp' %>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div id="toolTipAgents" style="display:none;position: absolute;width:290px;z-index:20000; background-color:#fffed4;-moz-border-radius: 10px;-webkit-border-radius: 10px;border: 1px solid #ECEC00;padding: 10px;opacity:.7;filter: alpha(opacity=80);" >
					<center>
						<table border="0px" cellpadding="1" cellpadding="1" width="250px;">
							<tr>
								<td style="border-bottom:2px solid red; background-color:#f6f5b9;" align="center">	
									<font size="2px" style="font-weight:bold" i18n_key="changeFare_Agents"> Agents </font>
								</td>
							</tr>
							<tr>
								<td style="padding-top:10px;" id="toolTipBody">
								</td>
							</tr> 
						</table>
					</center>
				</div>
<input type="hidden" name="allFaresInfoStr"  id="allFaresInfoStr" value=""/>				
</form>		
<%@ include file='../common/inc_LoadMsg.jsp'%>		
</body>
</html>
<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguageForOpener();

</script>