							<span id='spnAnciPaxWise_5' style='position:relative ;display:none;height:230px;width:385px;overflow:auto;'>
								<table width='385' border='0' cellpadding='1' cellspacing='0'>
									<thead>
										<tr>
											<td colspan='5' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL rowHeight'>
												<b i18n_key="ancillary_Paxwisebreakdownfor">Pax wise breakdown for <span id='spnBaggagePaxSeg'></span></b>
											</td>
										</tr>
										<tr>
											<td width='63%' align='center' class='thinBorderB thinBorderR gridBG  thinBorderT ui-corner-TL rowHeight txtBold' i18n_key="ancillary_PassengerName">Passenger Name</td>
											<%-- <td id='lblPaxBaggageCharge' width='20%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold'>Baggage Charge</td> --%>
											<td width='30%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold' i18n_key="ancillary_Qty">Qty</td>
											<%-- <td id='lblPaxTotalBaggageAmount' width='15%' align='center' class='thinBorderB gridBG thinBorderR thinBorderT rowHeight txtBold'>Amt</td> --%>
											<td width='7%' align='center' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR rowHeight txtBold'>&nbsp;</td>
										</tr>
									</thead>
									<tbody>
										<tr><td colspan='5'>
											<div style="height: 200px;overflow: auto;">
												<table width='99%' border='0' cellpadding='1' cellspacing='0'>
													<thead>
														<tr>
															<td width='63%' style="height: 1px"></td>
															<!-- <td width='21%' style="height: 1px"></td> -->
															<td width='30%' style="height: 1px"></td>
															<!-- <td width='15%' style="height: 1px"></td> -->
															<td width='7%' style="height: 1px"></td>
														</tr>
													</thead>
													<tbody id="tbdyAnciBaggagePax" ></tbody>
												</table>
											</div></td></tr>
									</tbody>
								</table>
							</span>