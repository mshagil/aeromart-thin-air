<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<table width='100%' border='0' cellpadding='0' cellspacing='0' id='tblPaxTopMost'>
		<tr>
			<td class="ui-widget ui-widget-content ui-corner-all" style='height:105px;' valign='top'>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td class='paneHD'>
							<p class='txtWhite txtBold' i18n_key="passenger_FlightDetails">Flight Details</p>
						</td>
					</tr>
					<tr>
						<td><div id='divFltSltd' style='position:absolute;width:915px;height:80px;overflow-X:hidden; overflow-Y:scroll;'>
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<thead id='flightTempleteHead'>
									<tr>
										<td width='12%' class='thinBorderB thinBorderR thinBorderL rowHeight'>orignNDest</td>
										<td width='9%' class='thinBorderB thinBorderR' i18n_key="testLbl">flightNo</td>
										<td width='8%' class='thinBorderB thinBorderR' align='center'>airLine</td>
										<td width='10%' class='thinBorderB thinBorderR' align='center'>departureDate</td>
										<td width='1%' class=' thinBorderB thinBorderR lightBG'>&nbsp;</td>
										<td width='10%' class='thinBorderB thinBorderR' align='center'>departureText</td>
										<td width='8%' class='thinBorderB thinBorderR' align='center'>departure</td>
										<td width='10%' class='thinBorderB thinBorderR' align='center'>departureTime</td>
										<td width='1%' class='thinBorderB thinBorderR lightBG '>&nbsp;</td>
										<td width='10%' class='thinBorderB thinBorderR' align='center'>arrivalText</td>
										<td width='8%' class='thinBorderB thinBorderR' align='center'>arrival</td>
										<td width='10%' class='thinBorderB thinBorderR' align='center'>arrivalTime</td>
									</tr>
								</thead>
								<tbody id="flightTemplete"></tbody>
							</table></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class='singleGap'></td>
		</tr>
		<tr>
			<td class="ui-widget ui-widget-content ui-corner-all" style='height:280px;' valign='top'>
				<table width='100%' border='0' cellpadding='1' cellspacing='0'>
					<tr>
						<td class='paneHD'>
							<table>
								<tr>
									<td>
										<p class='txtWhite txtBold' i18n_key="passenger_PassengerInformation">Passenger Information</p>
									</td>
									<td>
										<div id='divPaxHD' class='txtWhite txtBold'></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										<table id="tblPaxAdt" class="scroll" cellpadding="0" cellspacing="0"></table>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'></td>
								</tr>
								<tr>
									<td><div id='divInfantPane'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td i18n_key="passenger_Infant"><b>Infant(s)</b></td>
											</tr>
											<tr>
												<td>
													<table id="tblPaxInf" class="scroll" cellpadding="0" cellspacing="0"></table>
												</td>
											</tr>
										</table></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align='right' style='height:20px;'>
							<u:hasPrivilege  privilegeId="xbe.tool.frequentcustomer.view">
								<button id="btnLoadFQPax" type="button" class="btnLarge ui-state-default ui-corner-all">Load FQ Pax Details</button>
							</u:hasPrivilege>
							<%-- To be used in development mode --%>
							<% if (com.isa.thinair.commons.core.util.SystemPropertyUtil.isDevMode()) { %>
								<button id="btnSetData" type="button" class="btnMedium" i18n_key="btn_SetData">Set Data</button>
							<% } else { %>
								<u:hasPrivilege  privilegeId="test.xbe.testui">
									<button id="btnSetData" type="button" class="btnMedium" i18n_key="btn_SetData">Set Data</button>
								</u:hasPrivilege>
							<% } %>
							<u:hasPrivilege  privilegeId="xbe.res.view.paxnames">
							<button id="btnPaxNames" type="button" class="btnMedium" i18n_key="btn_PassengerNames">Pax Names</button>
							</u:hasPrivilege>
							<button id="btnPaxDetails" type="button" class="btnMedium" i18n_key="btn_Details">PaxDetails</button>
							<button id="btnInternationalFlight" type="button" class="btnMedium" i18n_key="Infant">Int'l Flights</button>

							<u:hasPrivilege  privilegeId="xbe.res.make.uplcsv">
							<button id="btnUploadFromCSV" type="button" class="btnMedium" style="display: none;" i18n_key="btn_UploadFromCSV">Upload From CSV</button>
							<button id="btnCSVUploadSampleFile" type="button" class="btnMedium" style="display: none;" i18n_key="btn_SampleFile">Sample File</button>
							</u:hasPrivilege>

							<u:hasPrivilege  privilegeId="xbe.res.make.tba">
								<button id="btnTBA" type="button" class="btnMargin" i18n_key="testLbl">TBA</button>
							</u:hasPrivilege>						
						</td>
					</tr>
				</table>
				<div id="sampleFileIFrameDiv"></div>
			</td>
		</tr>
	</table>
	<div id="saveFrequentCustomerPopUp" style="display: none; width:900px;">
					<%@ include file='../../customer/AgentFrequentCustomerSavePopup.jsp' %>
	</div>
	<div id="populateFrequentCustomerPopUp" style="display: none;">
					<%@ include file='../../customer/AgentFrequentCustomerPopulatePopup.jsp' %>
	</div>
	<div id='divTabPaxPane' 
				style='position:relative;width:937px;height:170px;overflow-X:hidden; overflow-Y:auto;' 
				class='thinBorderT thinBorderB thinBorderR thinBorderL'>
		<table width='923' border='0' cellpadding='0' cellspacing='0' id="tblPaxAction">
			<tr>
				<td class='singleGap'></td>
			</tr>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all" style='height:165px;' valign='top'>
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="passenger_ContactDetails">Contact Details</p>
							</td>
						</tr>
						<tr>
							<td  class='singleGap'></td>
						</tr>
						<tr>
							<td>
								<table width='100%' border='0' cellpadding='1' cellspacing='0' class="compressTable">
									<tr>
										<td colspan="2" width="30%">
											<table id="tblTitle" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_Title">Title :</td>
													<td>
														<select id='selTitle' name='contactInfo.title' class="aa-input" style="width:50px;"></select>
														<span id="spnTitleMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>										
										<td colspan='2'></td>
										<td i18n_key="passenger_ProfilePNR">
											<u:hasPrivilege  privilegeId="xbe.res.make.profile">
												Profile/PNR :
											</u:hasPrivilege>
										</td>
										<td>
											<u:hasPrivilege  privilegeId="xbe.res.make.profile">
												<input type='text' id='txtPNR' name='loadProfileSearch.profileID' class="aa-input" maxlength="100"/>
											</u:hasPrivilege>
										</td>
										<td>
											<u:hasPrivilege  privilegeId="xbe.res.make.profile">
												<button id="btnLoadProfile" type="button" class="btnMargin" i18n_key="btn_Load">Load</button>
											</u:hasPrivilege>
										</td>
									</tr>
									<tr>
										<td colspan="2" width="30%">
											<table id="tblFirstName" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_FirstName">First Name :</td>
													<td>
														<input type='text' id='txtFirstName' name='contactInfo.firstName' class="aa-input fontCapitalize" style="width:145px;" maxlength="50"/>
														<span id="spnFNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2" width="31%">
											<table id="tblLastName" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='39%' i18n_key="passenger_LastName">Last Name :</td>
													<td>
														<input type='text' id='txtLastName' name='contactInfo.lastName' class="aa-input fontCapitalize" style='width:145px;' maxlength="50"/>
														<span id="spnLNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td width='39%' colspan='3' rowspan='6' valign="top">
											<u:hasPrivilege  privilegeId="xbe.res.make.profile">
											<table width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width="10%"><input type="checkbox" name="updateCustomeProfile" id="chkUpdateCustProf"/>&nbsp;</td>
													<td align="left" i18n_key="passenger_UpdateCustomerProfile">Update Customer Profile</td>
												</tr>
												<tr>
													<td colspan="2" class='singleGap'></td>
												</tr>
											</table>		
											</u:hasPrivilege>									
											<table width='100%' border='0' cellpadding='0' cellspacing='1'>
												<thead class="ui-state-default">
													<tr>
														<td width='29%' class='thinBorderB thinBorderR gridBG thinBorderL thinBorderT ui-corner-TL' align='center'>
															<table>
																<tr>
																	<td i18n_key="passenger_Price">
																		<b>Price</b>
																	</td>
																	<td>
																		<div id='divPBTxnCurr' class='txtBold'></div>
																	</td>
																</tr>
															</table>
														</td>
														<td width='10%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="passenger_Passenger"><b>Pax</b></td>
														<td width='13%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="passenger_Fare"><b>Fare</b></td>
														<td width='14%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="passenger_Tax"><b>Tax</b></td>
														<td width='14%' class='thinBorderB thinBorderR gridBG thinBorderT' align='center' i18n_key="passenger_Surcharges"><b>Sur</b></td>
														<td width='20%' class='thinBorderB thinBorderR gridBG thinBorderT ui-corner-TR' align='center' i18n_key="passenger_Total"><b>Total</b></td>
													</tr>
												</thead>
												<tbody id="paxPriceTemplete">
												</tbody>
												<tfoot>													
													<tr id="trPBFareDisc">
														<td class='thinBorderB thinBorderR thinBorderL rowHeight' colspan='5' ><label id='lblTotDisc' class='lblTotDisc' i18n_key="passenger_TotalDiscount">Total Discount</label></td>
														<td class='thinBorderB thinBorderR' align='right'><div id='divPBFareDisc' class='divPBFareDisc'>0</div></td>
													</tr>
												<tr>
													<td class='thinBorderB thinBorderR thinBorderL rowHeight'
														i18n_key="passenger_ADMIN_FEE" colspan='5'>Admin Fee</td>
													<td class='thinBorderB thinBorderR' align='right'><div
															id='divPBADFee'></div></td>
												</tr>
												<tr>
														<td class='thinBorderB thinBorderR thinBorderL rowHeight' i18n_key="passenger_Total"><b>Total</b></td>
														<td class='thinBorderB thinBorderR' colspan='4' align='center'>
															<table border='0' cellpadding='0' cellspacing='0'>
																<tr>	
																	<td>(</td>
																	<td><div id='divPBFareAmt' class='txtBold'>0</div></td>
																	<td><b>&nbsp;+&nbsp;</b></td>
																	<td><div id='divPBTaxAmt' class='txtBold'>0</div></td>
																	<td>)</td>
																</tr>
															</table>
														</td>
														<td class='thinBorderB thinBorderR' align='right'><div id='divPBTotAmt' class='txtBold'>0</div></td>
													</tr>
													<tr id="trPBFareDiscCredit">
														<td class='thinBorderB thinBorderR thinBorderL rowHeight' colspan='5' ><label id='lblTotDisc' class='lblTotDisc' i18n_key="passenger_TotalDiscount">Total Discount</label></td>
														<td class='thinBorderB thinBorderR' align='right'><div id='divPBFareDisc' class='divPBFareDisc'>0</div></td>
													</tr>
												</tfoot>
											</table>
											<div id='divPaxPrice'></div>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<table id="tblAddr1" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_StreetAddress">Street Address :</td>
													<td>
														<input type='text' id='txtStreet' name='contactInfo.street' class="aa-input fontCapitalize" style="width:145px;" maxlength="100"/>
														<span id="spnAddrMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>	
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2">
											<table id="tblMobileNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<!-- Mobile No Help for W5 -->
											<% if (com.isa.thinair.commons.core.util.AppSysParamsUtil.addMobileAreaCodePrefix()) { %>
												<tr id="mobileNoHelpRow">
													<td width='39%'><label id="lblMobNoExample" class='fntSmall fntBold'>Example</label></td>
													<td>						
														<label id="lblCountryCodeHelp" class='fntSmall fntBold'>98</label>
									                  	<label id="lblAreaCodeHelp" class='fntSmall areaCode fntBold'>9XX</label>
									                  	<label id="lblNumberHelp" class='fntSmall fntBold'>XXXXXXX</label>
													</td>
												</tr>
											<% } %>
												<tr>
													<td width='39%' i18n_key="passenger_Mobile">Mobile :</td>
													<td>
														<input type='text' id='txtMobCntry' name='contactInfo.mobileCountry' title="Enter Your Country Code"  class="aa-input" style="width:30px;" maxlength="4"/>
														<input type='text' id='txtMobArea' name='contactInfo.mobileArea' title="Enter Your Area Code"  class="aa-input" style="width:30px;" maxlength="4"/>
														<input type='text' id='txtMobiNo' name='contactInfo.mobileNo' title="Enter Your Mobile Number"  class="aa-input" style="width:75px;" maxlength="10" />
														<span id="spnMobileNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>							
									</tr>
									<tr>
										<td colspan="2">
											<table id="tblAddr2" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%'></td>
													<td>
														<input type='text' id='txtAddress' name='contactInfo.address' class="aa-input" style="width:145px;" maxlength="100" />
														<span id="spnAddr1Mand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2">
											<table id="tblPhoneNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='39%' i18n_key="passenger_PhoneNo">Phone No :</td>
													<td>
														<input type='text' id='txtPhoneCntry' name='contactInfo.phoneCountry' class="aa-input" style="width:30px;" maxlength="4"/>
														<input type='text' id='txtPhoneArea' name='contactInfo.phoneArea' class="aa-input" style="width:30px;" maxlength="4""/>
														<input type='text' id='txtPhoneNo' name='contactInfo.phoneNo' class="aa-input" style="width:75px;" maxlength="10"/>
														<span id="spnPhoneNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>								
									</tr>
									<tr>
										<td colspan="2">
											<table id="tblCity" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_TownCity">Town / City :</td>
													<td>
														<input type='text' id='txtCity' name='contactInfo.city' class="aa-input fontCapitalize" style="width:145px;" maxlength="20"/>
														<span id="spnCityMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2">
											<table id="tblFax" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='39%' i18n_key="passenger_Fax">Fax :</td>
													<td>
														<input type='text' id='txtFaxCntry' name='contactInfo.faxCountry' class="aa-input" style="width:30px;" maxlength="4"/>
														<input type='text' id='txtFaxArea' name='contactInfo.faxArea' class="aa-input" style="width:30px;" maxlength="4"/>
														<input type='text' id='txtFaxNo' name='contactInfo.faxNo' class="aa-input" style="width:75px;" maxlength="10"/>
														<span id="spnFaxMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>				
									</tr>
									<tr>
										<td colspan="2">
											<table id="tblNationality" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_Nationality">Nationality :</td>
													<td>
														<select id='selNationality' size='1' class="aa-input" style='width:145px;' tabindex="5" name='contactInfo.nationality'></select>
														<span id="spnNationalityMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2">
											<table id="tblEmail" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='39%' i18n_key="passenger_EmailAddress">Email Address : </td>
													<td>
														<input type='text' id='txtEmail' name='contactInfo.email' class="aa-input" style="width:145px;" maxlength="100"/>
														<span id="spnEmailMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>										
									</tr>
									<tr>
										<td colspan="2">
											<table id="tblZipCode" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_ZipCode">Zip Code :</td>
													<td>
														<input type='text' id='txtZipCode' name="contactInfo.zipCode" class="aa-input" style='width:145px;' maxlength='10'/>
														<span id="spnZipCodeMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2">
											<table id="tblPreferredLang" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='39%' i18n_key="passenger_PreferredLanguage">Preferred Language :</td>
													<td>
														<select id="selPrefLang" name="contactInfo.preferredLang" class="aa-input" style="width:145px;"></select>														
														<span id="spnPreferredLangMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2">
										</td>
										<td colspan="2">
											<table id="tblUserNote" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr id="userNoteType">
													<td width='39%'>User Note Type :</td>
													<td>
														<select id="selClassification"  name='contactInfo.userNoteType' class="aa-input" style="width:145px;" tabindex="31">
															<option value="PUB">Public</option>
															<option value="PVT">Private</option>
														</select>
													</td>
												</tr>
												<tr>
													<td width='39%' i18n_key="passenger_UserNotes">User Notes :</td>
													<td>
														<textarea id='txtUserNotes' name='contactInfo.userNotes' class="aa-input" cols='10' rows='2' style="width:145px;height:18px;z-index:2;"></textarea>
														<span id="spnUserNotesMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>

									</tr>
									<tr>
										<td colspan="2" valign="top"  class="service-tax-group">
											<table id="tblCountry" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_Country">Country :</td>
													<td>
														<select id="selCountry" name="contactInfo.country" class="aa-input" style="width:145px;"></select>
														<span id="spnCountryMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2" valign="top" class="service-tax-group">
											<table id="tblState" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' i18n_key="passenger_State">State :</td>
													<td>
														<select id="selState" name="contactInfo.state" class="aa-input" style="width:145px;"></select>
														<span id="spnStateMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2" class="service-tax-group">
											<table id="tblTaxRegNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='37%' ><span i18n_key="<c:out value='${requestScope.taxRegistrationNumberLabel}' escapeXml='false'/>">Tax Registration Number</span></td>
													<td>
														<input type='text' id='txtTaxRegNo' name="contactInfo.taxRegNo" class="aa-input" style='width:145px;' maxlength='15'/>
														<span id="spnTaxRegNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td  class='singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class='singleGap'></td>
			</tr>
			<tr id="trEmgnContactInfo">
				<td class="ui-widget ui-widget-content ui-corner-all" style='height:100px;' valign='top'>
					<table width='923' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="passenger_EmergencyContactDetails">Emergency Contact Details</p>
							</td>
						</tr>
						<tr>
							<td  class='singleGap'></td>
						</tr>
						<tr>
							<td>
								<table width='100%' border='0' cellpadding='1' cellspacing='0' class="compressTable">
									<tr>
										<td colspan="2" width="30%">
											<table id="tblEmgnTitle" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='36.6%' i18n_key="passenger_Title">Title :</td>
													<td>
														<select id='selEmgnTitle' name='contactInfo.emgnTitle' class="aa-input" style="width:50px;"></select>
														<span id="spnEmgnTitleMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>										
										<td colspan='5'></td>
									</tr>
									<tr>
										<td colspan="2" width="30%">
											<table id="tblEmgnFirstName" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='36.6%' i18n_key="passenger_FirstName">First Name :</td>
													<td>
														<input type='text' id='txtEmgnFirstName' name='contactInfo.emgnFirstName' class="aa-input fontCapitalize" style="width:145px;" maxlength="50"/>
														<span id="spnEmgnFNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2" width="31%">
											<table id="tblEmgnLastName" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='38.7%' i18n_key="passenger_LastName">Last Name :</td>
													<td>
														<input type='text' id='txtEmgnLastName' name='contactInfo.emgnLastName' class="aa-input fontCapitalize" style='width:145px;' maxlength="50"/>
														<span id="spnEmgnLNameMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="3"></td>
									</tr>
									<tr>
										<td colspan="2" width="30%">
											<table id="tblEmgnPhoneNo" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='36.6%' i18n_key="passenger_PhoneNo">Phone No :</td>
													<td>
														<input type='text' id='txtEmgnPhoneCntry' name='contactInfo.emgnPhoneCountry' class="aa-input" style="width:30px;" maxlength="4"/>
														<input type='text' id='txtEmgnPhoneArea' name='contactInfo.emgnPhoneArea' class="aa-input" style="width:30px;" maxlength="4""/>
														<input type='text' id='txtEmgnPhoneNo' name='contactInfo.emgnPhoneNo' class="aa-input" style="width:75px;" maxlength="10"/>
														<span id="spnEmgnPhoneNoMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="2" width="31%">
											<table id="tblEmgnEmail" width='100%' border='0' cellpadding='0' cellspacing='1'>
												<tr>
													<td width='38.7%' i18n_key="passenger_Email">Email :</td>
													<td>
														<input type='text' id='txtEmgnEmail' name='contactInfo.emgnEmail' class="aa-input" style="width:145px;" maxlength="100"/>
														<span id="spnEmgnEmailMand"><%@ include file='../common/inc_MandatoryField.jsp' %></span>
													</td>
												</tr>
											</table>
										</td>
										<td colspan="3"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<table width='100%' border='0' cellpadding='0' cellspacing='0' id="tblPaxAction" style="position:'';">
		<tr>
			<td class='singleGap'></td>
		</tr>
		<tr>
			<td width="8%" align="center">
				<span id="salesPointlblSpn" i18n_key="passenger_SalesPoint">Sales Point</span>
			</td>
			<td width="21%">
				<select id="selSalesPoint" name="selSalesPoint" class="aa-input"  size="1" style="width:180px" ></select>
			</td>
			<td width="8%" align="center">
				<span id="bookingCategorylblSpn" i18n_key="passenger_BookingCategory">Booking Category</span>
			</td>
			<td width="9%" align="center">
				<select id="selBookingCategory" name="selBookingCategory" class="aa-input"  size="1" style="width:70px" ></select><%@ include file='../common/inc_MandatoryField.jsp'%>
			</td>
			<u:hasPrivilege  privilegeId="xbe.res.make.itin.fare.mask">
			<td width="12%" align="center">
				<span id="fareMasklblSpn" i18n_key="passenger_ItineraryFareMask">Itinerary Fare Mask</span>
			</td>
			<td width="5%">
				<select id="selItineraryFareMask" name="selItineraryFareMask" class="aa-input"  style="width:45px" ></select>
			</td>
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="xbe.res.make.callorigin.country">
				<td width="8%" align="center">
					<span id="originCountrylblSpn" i18n_key="passenger_OriginOfCall">Origin Of Call</span>
				</td>
				<td width="20%">
					<select id="selOriginCountryOfCall" name="selOriginCountryOfCall" class="aa-input" size="1" style="width:145px"></select><%@ include file='../common/inc_MandatoryField.jsp'%>
				</td>
			</u:hasPrivilege>
		</tr>
		<tr>
			<td class='singleGap'></td>
		</tr>
	</table>
	<table width='100%' border='0' cellpadding='0' cellspacing='0' id="tblPaxAction" style="position:'';">
		<tr>
		<td width="6%">
				<button id="btnPHold" type="button" class="btnMargin btnMedium" style="width:100px" i18n_key="btn_OnHold">On-Hold</button>
		</td>
		<td align='right'>
			<button id="btnContinue" type="button" class="btnMargin btnMedium" i18n_key="btn_Continue" style="width:114px">Continue</button><button id="btnPReset" type="button" class="btnMargin" i18n_key="btn_Reset">Reset</button><button id="btnPCancel" type="button" class="btnMargin" i18n_key="btn_Cancel">Cancel</button>
		</td>
		<td align='right'>
			<button id="btnWaitList" type="button" class="btnMargin btnMedium" i18n_key="btn_WaitList">Wait-List</button>
		</td>
		</tr>
	</table>
	<%@ include file='inc_SSR.jsp' %>
	<%@ include file='inc_AnciPref.jsp' %>
	<%@ include file='inc_PromoRemoveConfirm.jsp' %>
	<%@ include file='inc_BlacklistPaxResAlertPop.jsp' %>
	

<script type="text/javascript">
	$("#tblPaxTopMost").getLanguage();
	$("#divSSRPane").getLanguage();
	$("#divPromoRemoveConfirm").getLanguage();
</script>

