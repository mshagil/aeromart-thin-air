<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced Search</title>
<%@ include file='../common/inc_PgHD.jsp' %>	
<script src="../js/v2/demo/advancedsearch.js" type="text/javascript"> </script> 

<style>
.multicitiStart{display:none}
#spnAdvancedSearchPane{display:block}
.citytop{display:none;text-align:left}
.citytop .lbl{font-weight:bold}
.butnset{margin-left:224px}
.odd {border:1px solid #aaa;border-left:0;border-right:0}
.city {padding:2px;border-bottom:1px solid #aaa;}
.edit, .remove{height:20px}
.ui-state-highlight, .ui-widget-content .ui-state-highlight{background:#FFEF9C;border:1px solid #D5CB8E;}
</style>
<script type="text/javascript">
$(function () {
	hideLoader = function(){
		$("#divLoadMsg").hide();	
	}
	
	showLoader = function(){
		$("#divLoadMsg").show();
		setTimeout('hideLoader()',1000);
	}
	
	showLoader();
});
</script>
</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:block;">
		<div style="height:5px;"></div>
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
		<ul>
				<li><a href="#divSearchTab" style="width:100px">Search Flights</a></li>
		</ul>
		

<div
	style="display:none; background: none repeat scroll 0% 0% transparent;"
	id="divLoadMsg"><iframe scrolling="no" frameborder="0"
	style="position: absolute; top: 50%; left: 40%; width: 260px; height: 40px; z-index: 2;"
	name="frmLoadMsg" id="frmLoadMsg" src="showFile!loadMsg.action"></iframe>
</div>
<div id="divSearchTab" style="padding-top:5px">

<div style="width:914px;display: block;margin: 15px auto;">
<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
	<tbody><tr>
		<td height="20"  class="FormHeadBackGround">
			<a href="javascript:void(0)" class="showHide" id="searc-shedule"><font class="FormHeader">
                     <span id="spnFltSearchHD">Search Schedules</span></font>
			</a>
		</td>
	</tr>
</tbody></table>
            <span id="spnAdvancedSearchPane" class="ui-widget ui-widget-content ui-corner-all">
            <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="FormBackGround" id="Table1">
            <tr><td align="center">
            	<table cellspacing="0" cellpadding="2" border="0" align="center" width="98%">
		 	 	<tbody>
                <tr>
                	<td valign="top">
                    <span id="searchShedule" style="visibility:visible">
                    
                        <table cellspacing="0" cellpadding="1" border="0" align="center" width="100%">
                        	<tr>
                            <td>
                             <input type="checkbox" value="return" tabindex="8" class="rightText" name="chkreturn" id="chkreturn" 
                             style="visibility: visible;" checked="checked"> 
                             <span id="spnOpenLabel"><font>&nbsp; Return</font></span>
                            </td>
                            <td colspan="3"></td>
                            </tr>
                        	<tr>
                            	<td width="7%">
					 	 			<font class="Mandatory">*&nbsp;</font><font>From :&nbsp;</font></td>
			 	 		  <td width="14%">
									<select id="origin-sche" style="width:170px">
                                    	<option selected="selected"></option>
                                    	<option value="DAM">DAM - Damascus</option>
									</select>                               </td>
                          <td width="5%">
					 	 			<font class="Mandatory">*&nbsp;</font><font>To :&nbsp;</font>					 	 		</td>
			 	 		  <td width="14%">
							<select id="destination-sche" style="width:170px">
                            	<option selected="selected"></option>
                            	<option value="SAW">SAW - Sabiha Gokcen</option>
							</select>                                </td>
                                    
                           <td width="8%">
			 	 			 <font class="Mandatory">*&nbsp;</font><font>Month :&nbsp;</font>					 	 		</td>
			 	 		  <td width="14%">
							<select id="month" style="width:120px">
                            <option>JAN</option>
                            <option>FEB</option>
                            <option>MAR</option>
                            <option>APR</option>
                            <option>MAY</option>
                            <option>JUN</option>
                            <option>JUL</option>
                            <option>AUG</option>
                            <option selected="selected">SEP</option>
                            <option>OCT</option>
                            <option>NOV</option>
                            <option>DEC</option>
							</select>                                </td>
                                    
                              <td width="10%">
                                <input type="button" tabindex="14" name="shedule-btnSearch" class="Button" value="Search" id="shedule-btnSearch" />
                              </td>
			 	 		  </tr>
                        </table>
                 	 </span></td>
                     <tr><td style="height:4px"></td></tr>
                     <tr><td>
                     <div class="search-shedule">
                     	<table id="sheduleDatetableOut" class="scroll" ></table>
                        <div style="height:8px"></div>
                        <table id="sheduleDatetableIn" class="scroll" ></table>
                     </div>
	  				</td>
                </tr>
                <tr><td style="height:4px"></td></tr>
			 </tbody></table>
                
            </td></tr></table>
            </span>

			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
				<tbody><tr>
					<td height="20"  class="FormHeadBackGround">
						<a href="javascript:void(0)" class="showHide" id="searc-flight"><font class="FormHeader">
                        <span id="spnFltSearchHD">Search Flights</span></font>
						</a>
					</td>
				</tr>
			</tbody></table>

            <span id="spnSearchPane" style="display:block" class="ui-widget ui-widget-content ui-corner-all">
                   <table cellspacing="0" cellpadding="0" border="0" align="center" width="98%">
					 	 	<tbody><tr>
					 	 		<td style="height: 5px;" colspan="11"></td>
					 	 	</tr>
					 	 	<tr>
					 	 		<td width="7%" height="28">
					 	 			<font>From :&nbsp;</font>					 	 		</td>
								<td>
					 	 	<select id="origin" style="width:170px">
                            	<option selected="selected"></option>
                                <option value="JED">JED - King Abdul Aziz Int. Airport</option><option value="DAM">DAM - Damascus</option>
								<option value="IST">IST - Istanbul</option><option value="MMX">MMX - Sturup - Malmo Airport</option>
                                <option value="SAW">SAW - Sabiha Gokcen</option>
							</select>
                            <font class="Mandatory">*&nbsp;</font>
								</td>
					 	 		<td width="12%">
					 	 			<font>Departure Date :&nbsp;</font>
					 	 		</td>
					 	 		<td width="8%" colspan="2">
					 	 			<input type="text" invalidtext="true" tabindex="3" name="txtDepartDate" size="10" maxlength="10" id="txtDepartDate"><font class="Mandatory">&nbsp;*</font>
								</td>
								
								<td width="11%">
									<input type="text" tabindex="4" name="txtDVariance" size="4" maxlength="1" class="rightText" id="txtDVariance" value="0">		 	 			
									<font>+/- Days</font>
								</td>
					 	 		<td width="6%">
					 	 			<font>Adults :&nbsp;</font>
					 	 		</td>
					 	 		<td width="5%">
					 	 			<input type="text"  tabindex="9" name="txtAdults" size="4" maxlength="3" class="rightText" id="txtAdults" value="1"><span id="spnAdultMandatory"></span>
								</td>	
								<td width="6%">
					 	 			<font>Class :&nbsp;</font>
					 	 		</td>	 	 	
								<td width="8%">
									<select title="Select currency to view Total" onchange="pageOnChange()" style="width: 57px;" tabindex="12" size="1" name="selCos" id="selCos">
									<option value="Y">Y</option></select><font class="Mandatory">&nbsp;*</font>
					 	 		</td>
								<td align="right" width="9%">
                                <input type="button" tabindex="14" name="0-btnSearch" class="Button" value="Search" id="mainSearch">
					 	 		</td>
					 	 	</tr>
					 	 	<tr>
					 	 		<td height="26">
					 	 			<font>To :&nbsp;</font>					 	 		</td>
	 	 		  <td>
					 	 			<select id="destination" style="width:170px">
                                        <option selected="selected"></option>
                                        <option value="JED">JED - King Abdul Aziz Int. Airport</option><option value="DAM">DAM - Damascus</option>
                                        <option value="IST">IST - Istanbul</option><option value="MMX">MMX - Sturup - Malmo Airport</option>
                                        <option value="SAW">SAW - Sabiha Gokcen</option>
									</select>
                                    <font class="Mandatory">*&nbsp;</font>
								</td>
					 	 		<td>
					 	 			<font>Return Date :&nbsp;</font>
					 	 		</td>
					 	 		<td colspan="2">
					 	 			<input type="text" invalidtext="true" tabindex="5" name="txtReturnDate" size="10" maxlength="10" id="txtReturnDate" >
								</td>

					 	 		<td>
									<input type="text"  tabindex="6" name="txtRVariance" size="4" maxlength="1" class="rightText" id="txtRVariance" value="0">		 	 			
									<font>+/- Days</font>
					 	 		</td>
					 	 		<td>
					 	 			<font>Children :&nbsp;</font>
					 	 		</td>
					 	 		<td>
					 	 			<input type="text" tabindex="10" name="txtChild" size="4" maxlength="2" class="rightText" id="txtChild" value="0">
								</td>		 	 		
					 	 		<td>
									<font>Currency :</font>
					 	 		</td>
					 	 		<td>
									<select title="Select currency to view Total" onchange="selCurrencyOnChange()" style="width: 55px;" tabindex="13" size="1" name="selCurrency" id="selCurrency">
									<option value=""></option><option value="BHD">BHD</option><option value="BDT">BDT</option><option value="EGP">EGP</option><option value="EUR">EUR</option><option value="INR">INR</option><option value="JOD">JOD</option><option value="KZT">KZT</option><option value="KES">KES</option><option value="KWD">KWD</option><option value="MYR">MYR</option><option value="NPR">NPR</option><option value="OMR">OMR</option><option value="PKR">PKR</option><option value="QAR">QAR</option><option value="RUB">RUB</option><option value="SAR">SAR</option><option value="LKR">LKR</option><option value="SDG">SDG</option><option value="SYP">SYP</option><option value="TES">TES</option><option value="AED" >AED</option><option value="USD" selected="selected">USD</option><option value="UAH">UAH</option><option value="YER">YER</option><option value="JUN">JUN</option></select><font class="Mandatory">&nbsp;*</font>
					 	 		</td>
					 	 		<td>
									&nbsp;&nbsp;
					 	 		</td>
					 	 	</tr>
					 	 	<tr>
  								<td><span id="spnRetValidityLabel"><font>Options :&nbsp;</font></span></td>
								<td>
									<select title="Flight Search Options" style="width: 170px;display:block" tabindex="14" name="selSearchOptions" id="selSearchOptions"><option value="DEFAULT">Search 6Q </option><option value="V2">Search Interline Airline</option></select>
                                     
								</td>
								<td><span id="spnRetValidityLabel"><font>Validity :&nbsp;</font></span></td>
					 	 	
								<td id="tdRetValidity" colspan="2"><select tabindex="7" style="width: 64px; visibility: visible;" size="1" name="selRetValidity" id="selRetValidity" disabled=""><option value=""></option><option value="1">01 Month</option><option value="2">03 Month</option><option value="3">06 Month</option><option value="4">12 Month</option></select><font class="Mandatory">&nbsp;*</font>
                               
                                
                                </td>
								<td><input type="checkbox" value="OPEN" tabindex="8" class="rightText" name="chkOpenRet" id="chkOpenRet" style="visibility: visible;"><span id="spnOpenLabel"><font>&nbsp; Open</font></span>
                                
                                </td>							
					 	 		<td>
					 	 			<font>Infants :&nbsp;</font>
					 	 		</td>
					 	 		<td>
					 	 			<input type="text" tabindex="11" name="txtInfants" size="4" maxlength="2" class="rightText" id="txtInfants" value="0"/>
								</td>		 	 		
					 	 		<td colspan="2">
					 	 			<span id="spnStandby"><table cellspacing="0" cellpadding="2" border="0" width="100%">	<tbody><tr>		<td width="35%"><font>Type :</font>	</td>	
					 	 			<td width="65%"><select title="Select booking Type" onchange="selBookingTypeOnChange()" style="width: 67px;" tabindex="13" size="1" name="selBookingType" id="selBookingType">		<option value="NORMAL">NORMAL</option><option value="STANDBY">STANDBY</option></select><font class="Mandatory">&nbsp;*</font>	</td>	</tr></tbody></table></span>
					 	 		</td>	
					 	 		<td>
                                <input type="checkbox" value="multicity" tabindex="8" class="rightText" name="chkMultiCity" id="chkMultiCity" style="visibility: hidden;" />
                                <span id="spnOpenLabel" style="visibility: hidden;"><font>&nbsp; Multi City</font></span>
                                
                                </td>
					 	 	</tr>
					 	 	<tr>
					 	 		<td><span id="spnFareLabel"></span></td>
								<td><select tabindex="7" onchange="selFareTypeOnChange()" style="width: 100px; visibility: hidden;" size="1" name="selFareType" id="selFareType"><option value="A">All</option></select> <span id="spnFareMandatoryLabel"></span></td>
								<td><span id="spnPaxCategoryLabel"></span></td>
					 	 	
								<td colspan="2"><select tabindex="7" onchange="selCusTypeOnChange()" style="width: 100px; visibility: hidden;" size="1" name="selCusType" id="selCusType"><option value="A">All</option></select><span id="spnPaxCategoryMandatoryLabel"></span></td>		 
					 	 		<td colspan="6"></td>		
					 	 	</tr>
						</tbody></table>
  			</span>
           <div class="multicitiStart">  
            <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
				<tbody><tr>
					<td height="20" width="10"></td>
					<td height="20"  class="FormHeadBackGround">
						<a href="javascript:void(0)" id="muticitysearch" class="showHide"><font class="FormHeader">
                        <span id="spnFltSearchHD">MultiCity Search</span></font>
						</a>
					</td>

				  <td height="20" width="11"></td>
				</tr>
			</tbody></table>
            
            
             <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%"><tr><td align="center">
             <table cellspacing="0" cellpadding="0" border="0" align="center" width="98%"><tr><td align="center">
            <div id="multicity">
            <div style="padding:4px">
            Choose the multiple city to create itineray for more than 2 cities. You can choose upto 3 cities.
            </div>
            <div class="newcities">
            	 
            </div>
            <div style="text-align:right">
            <span id="addnother-container">
            	<input type="button" tabindex="14" class="Button" value="Add Another City" id="another-search" disabled="disabled" style="width:auto">
            </span>
            <span id="farequate-container">
            	<input type="button" tabindex="14" class="Button" value="Complete Search" id="quote-search" style="width:auto">
            </span>
            </div>
            </div>  
            </td></tr></table>
            </td></tr></table>
            </div>
           
</div>
</div></div></div>
	<div style="position:absolute;top:12px;left:660px;z-index:1;width:310px;" id="divCreditPnl">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
				<td align="center" style="height:21px;" class="ui-widget ui-corner-all darkBG">
					<table cellspacing="0" cellpadding="0" border="0">
						<tbody><tr>
							<td><p class="txtWhite">Agents Available Credit :&nbsp;</p></td>
							<td>
								<div id="divAvailCredit"><p class="txtWhite txtBold">AED 11.00</p></div>
							</td>
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
	</div>
</body>
</html>
