<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced Search</title>
<%@ include file='../common/inc_PgHD.jsp' %>
<script src="../js/v2/demo/advancedsearch.js" type="text/javascript"> </script>  

<style>
.multicitiStart{display:none}
#spnAdvancedSearchPane{display:none}
.FormHeader{color:#333333}
#spnFltSearchHD{color:#555}
#fareCoteData{display:block}
.ui-state-highlight, .ui-widget-content .ui-state-highlight{background:#FFEF9C;border:1px solid #D5CB8E;}
a.c{color:#0000FF! important;text-decoration:underline! important;}
</style>

</head>

<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display:block;">
		<div style="height:5px;"></div>
		<div id="divBookingTabs" style="margin-top:0px;margin-left:20px;margin-right:20px;height:632px;">
		<ul>
				<li><a href="#divSearchTab" style="width:100px">Search Flights</a></li>
		</ul>
		<div style="width:914px;display: block;margin: 15px auto;">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
				<tbody><tr>
					<td height="20" width="10"></td>
					<td height="20"  class="FormHeadBackGround">
						<a href="javascript:void(0)" class="showHide" id="backto-search"><font class="FormHeader">
                        <span id="spnFltSearchHD">Back to Search Flights</span></font>
						</a>
					</td>

				  <td height="20" width="11"></td>
				</tr>
			</tbody></table>

            <span id="spnavailabilityPane" style="visibility: visible;">
             <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%"><tr>
             <td align="center" style="background-color:#Fff;">
            	<table cellspacing="0" cellpadding="2" border="0" align="center" width="98%">
					 <tbody>
                     <tr>
                     <td width="175" valign="top">
                     <table width="100%">
                        </table>
                        <div style="overflow-x:scroll;overflow-y:hidden;width:340px">
            			<table id="table1" class="scroll" ></table>
                        </div>
                     </td>
                      <td width="700" valign="top" id="fareTable1">
            			
                     </td>
                     </tr>
                     <tr><td>&nbsp;</td></tr>
                     <tr>
                     <td width="175" valign="top">
                     <table width="100%">
   
                        </table>
                        <div style="overflow-x:scroll;overflow-y:hidden;width:340px">
            			<table id="table2" class="scroll" ></table>
                        </div>
                     </td>
                      <td width="700" valign="top" id="fareTable2">
            			
                     </td>
                     </tr>
                     <tr><td>&nbsp;</td></tr>
                     <tr>
                     <td width="175" valign="top">
                     <table width="100%">
                        </table>
                        <div style="overflow-x:scroll;overflow-y:hidden;width:340px">
            			<table id="table3" class="scroll" ></table>
                        </div>
                     </td>
                      <td width="700" valign="top" id="fareTable3">
            			
                     </td>
                     </tr>
                     <tr><td colspan="2" align="right">
                     <input type="button" name="btnFareQuote" style="width: 100px;" class="Button" value="Fare Quote" id="btnFareQuote" disabled="disabled">
                     </td></tr>
                     <tr><td colspan="2">&nbsp;</td></tr>
                     <tr>
                     <td colspan="2">
                     <div id="fareCoteData">
                     <table cellspacing="1" cellpadding="2" border="0" align="center" width="100%" class="GridRow GridHeader GridHeaderBand GridHDBorder"><tbody><tr><td align="left"><font class="fntBold">Fare quote - Outgoing 31/May/2010 - Return 02/Jun/2010  - Economy Class</font></td></tr><tr>	<td align="center" valign="top" style="height: 187px;">	<table cellspacing="1" cellpadding="2" border="0" width="100%" id="Table10">		<tbody><tr>			<td width="28%"><font class="fntBold">&nbsp;</font></td>			<td align="center" width="14%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Pax Type</font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Fare	&nbsp;			</font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Tax</font>			&nbsp;		</td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Sur.</font>			&nbsp;			</td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Per Pax</font>			</td>			<td align="center" width="8%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">No. pax</font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Total	    &nbsp;            </font></td>			<td align="center" width="10%" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Total Price</font></td>		</tr>		
                     <tr><td style="height: 2px;" colspan="9"></td></tr>	
                     
                     <tr>
						<td valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor">
                        <font title="Outgoing">King Abdul Aziz - Damascus | 6Q945</font></td>
                        <td class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font></td>
	                    <td align="right" class="GridItemRow GridItem">
    	                 <span class="editFare"><font>420.00</font>&nbsp;</span>
        	             <div style="display:none"><input type="text" class="faretext" size="5" style="text-align:right"/></div>
            	         </td>                     
	               		<td align="right" class="GridItemRow GridItem"><font>296.00&nbsp;</font></td>
                        <td align="right" class="GridItemRow GridItem"><font>205.00&nbsp;</font></td>
                        <td align="right" class="GridItemRow GridItem"><font class="row">921.00</font>&nbsp;</td>
                        <td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font></td>		
                        <td align="right" class="GridItemRow GridItem"><font class="row">921.00</font>&nbsp;</td>
                   		<td align="right" class="GridItemRow GridItem"><font class="row">921.00</font>&nbsp;</td>
                   	</tr>
                    <tr><td style="height: 2px;" colspan="9"></td></tr>
                    <tr>
						<td valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor">
                        <font title="Outgoing">Damascus - Istanbul | 6Q201</font></td>
                        <td class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font></td>
	                    <td align="right" class="GridItemRow GridItem">
    	                 <span class="editFare"><font>420.00</font>&nbsp;</span>
        	             <div style="display:none"><input type="text" class="faretext" size="5" style="text-align:right"/></div>
            	         </td>                     
	               		<td align="right" class="GridItemRow GridItem"><font>296.00&nbsp;</font></td>
                        <td align="right" class="GridItemRow GridItem"><font>205.00&nbsp;</font></td>
                        <td align="right" class="GridItemRow GridItem"><font class="row">921.00</font>&nbsp;</td>
                        <td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font></td>		
                        <td align="right" class="GridItemRow GridItem"><font class="row">921.00</font>&nbsp;</td>
                   		<td align="right" class="GridItemRow GridItem"><font class="row">921.00</font>&nbsp;</td>
                   	</tr>
                                        
                                        <tr><td style="height: 2px;" colspan="9"></td></tr>	<tr>		<td valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor"><font title="Return">Istanbul - Sturup Malmo Airport | 6Q253</font></td>		<td class="GridItemRow GridItem GridItemHover defaultCursor"><font>Adult(s)&nbsp;</font></td>		
                            <td align="right" class="GridItemRow GridItem">
                            <span class="editFare"><font>350.00</font>&nbsp;</span>
                     		<div style="display:none"><input type="text" class="faretext" size="5" style="text-align:right"/></div>
  
                            </td>
                            
                            		<td align="right" class="GridItemRow GridItem"><font>55.00&nbsp;</font></td>		<td align="right" class="GridItemRow GridItem"><font>245.00&nbsp;</font></td>		<td align="right" class="GridItemRow GridItem"><font class="row">650.00</font>&nbsp;</td>		<td align="right" class="GridItemRow GridItem"><font>1&nbsp;</font></td>		
                                    <td align="right" class="GridItemRow GridItem"><font class="row">650.00</font>&nbsp;</td>
                                    
                                    		<td align="right" class="GridItemRow GridItem"><font class="row">650.00</font>&nbsp;</td>
                                            
                                            				</tr><tr><td style="height: 2px;" colspan="9"></td></tr>				<tr>					<td align="left" valign="middle" colspan="6" class="GridItemRow GridItem GridItemHover defaultCursor"> 						<table cellspacing="0" cellpadding="2" border="0" width="100%"><tbody><tr>							<td align="left">							</td>							<td align="right"><font class="fntBold">AED</font>								
                                                            <font title="Per pax charge in AED for Adult" class="fntBold tot"> 2492.00</font>&nbsp;								</td>							</tr>						</tbody></table>					</td>					<td align="right" colspan="2" class="GridItemRow GridItem GridItemHover defaultCursor"><font class="fntBold">Total Price AED&nbsp;</font></td>					<td align="right" valign="middle" class="GridItemRow GridItem GridItemHover defaultCursor">
                                                            <font class="fntBold tot">2492.00</font>&nbsp;
                                                            
                                                            </td>				</tr>			</tbody></table>		</td>	</tr></tbody></table>
                     </div>
                     </td>
                     </tr>
                     <tr><td colspan="2" align="right">
							<input type="button" name="btnContinue" class="Button" value="Book" id="btnContinue" style="visibility: visible;">
							<input type="button" name="btnReset" class="Button" value="Reset" id="btnReset">
							<input type="button" name="btnExit" class="Button" value="Close" id="btnExit">
                     </td></tr>
                     </tbody>
                 </table>
              </td></tr></table>
            </span>
     </div>
</div>
<script type="text/javascript">
$(function () {
	$("#table1").jqGrid({
			url:'./../js/v2/demo/gridFlightData0.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['Flight','Departure','Arrival','Fare','FC','Duration'],
			colModel :[ 
			   {name:'flight',width:68, label:'Flight'},
			   {name:'departure',label:'Departure', width:100},
			   {name:'arrival',label:'Arrival', width:65},
			   {name:'fare',label:'Fare', width:40,align:'right'},
			   {name:'fc',label:'FC',width:30,align:'center'},
			   {name:'duration',label:'Duration',width:65,align:'center'}  
		   ],
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			imgpath: 'themes/basic/images',
			caption: 'King Abdul Aziz Int. Airport - Damascus',
			hidegrid: false, 
			onSelectRow: function(rowid){
				createFareTable("#fareTable1","tableF1","./../js/v2/demo/gridFareDetails.js",rowid);
			},
			loadComplete: function(){ 
				var tt = $('#table1 tbody:first-child tr:first').attr('id')
				$("#table1").setSelection(tt, true);
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });

		
		 
		 $("#table2").jqGrid({
			url:'./../js/v2/demo/gridFlightData1.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['Flight','Departure','Arrival','Fare','FC','Duration'],
			colModel :[ 
			   {name:'flight',width:68, label:'Flight'},
			   {name:'departure',label:'Departure', width:100},
			   {name:'arrival',label:'Arrival', width:65},
			   {name:'fare',label:'Fare', width:40,align:'right'},
			   {name:'fc',label:'FC',width:30,align:'center'},
			   {name:'duration',label:'Duration',width:65,align:'center'}  
		   ],
		    hidegrid: false, 
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			imgpath: 'themes/basic/images',
			caption: 'Damascus - Istanbul',
			onSelectRow: function(rowid){
				createFareTable("#fareTable2","tableF2","./../js/v2/demo/gridFareDetails.js",rowid);
			},
			loadComplete: function(){ 
				var tt = $('#table2 tbody:first-child tr:first').attr('id')
				$("#table2").setSelection(tt, true);
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
		 
		 
		 
		 
		 $("#table3").jqGrid({
			url:'./../js/v2/demo/gridFlightData2.js',
			datatype: 'json',
			mtype: 'GET',
			colNames:['Flight','Departure','Arrival','Fare','FC','Duration'],
			colModel :[ 
			   {name:'flight',width:68, label:'Flight'},
			   {name:'departure',label:'Departure', width:100},
			   {name:'arrival',label:'Arrival', width:65},
			   {name:'fare',label:'Fare', width:40,align:'right'},
			   {name:'fc',label:'FC',width:30,align:'center'},
			   {name:'duration',label:'Duration',width:65,align:'center'}  
		   ],
		    hidegrid: false, 
			viewrecords: true,
			height: "auto",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			imgpath: 'themes/basic/images',
			caption: 'Istanbul - Sturup Malmo Airport',
			onSelectRow: function(rowid){
				createFareTable("#fareTable3","tableF3","./../js/v2/demo/gridFareDetails.js",rowid);
			},
			loadComplete: function(){ 
				var tt = $('#table3 tbody:first-child tr:first').attr('id')
				$("#table3").setSelection(tt, true);
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
		 
		function createFareTable(pId,tabelID,url,capt){
		 	$(pId).html('<table id="'+tabelID+'" class="scroll" ></table>')
			
			jQuery("#"+tabelID).jqGrid({
			url:url,
			datatype: 'json',
			mtype: 'GET',
			colNames:['FC','Basis','F.ADL','F.CHI','F.INF','Avl.','Max/Min','Season','Permit. COMB','OW/RT','&nbsp;'],
		    colModel :[ 
			   {name:'fc',width:25, label:'FC',align:'center'},
			   {name:'basis',label:'Basis', width:55},
			   {name:'fareAD',label:'Fare', width:40,align:'right'},
			   {name:'fareCH',label:'Fare', width:40,align:'right'},
			   {name:'fareINF',label:'Fare', width:40,align:'right'},
			   {name:'aval',label:'Avl.',width:32,align:'center'},
			   {name:'maxmin',label:'Max/Min',width:65,align:'center'},
			   {name:'season',label:'Season',width:80},
			   {name:'PCOMB',label:'Permit. COMB',width:80,align:'center'},
			   {name:'OWoRT',label:'OW/RT',width:60,align:'center'},
			   {name:'link',label:'&nbsp;',width:35,align:'center'}  
		   ],
			viewrecords: true,
			height: "80",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			hidegrid: false, 
			imgpath: 'themes/basic/images',
			caption: capt,
			onSelectRow: function(rowid){
				if (tabelID == "tableF1"){
					if (rowid != "V"){
						createFareTable("#fareTable2","tableF2","./../js/v2/demo/gridFareDetailsH.js",capt);
					}else{
						createFareTable("#fareTable2","tableF2","./../js/v2/demo/gridFareDetails.js",capt);
					}
				}
			},
			loadComplete: function(){ 
				var tt = $("#"+tabelID+" tbody:first-child tr:first").attr('id')
				$("#"+tabelID).setSelection(tt, true);
				enebleTooltip()
			},
			onCellSelect:function(rowID,iCol,cellcontent,e){
				hideFareQuote();
			}
		 });
		}

		 $("#btnFareQuote").click(function(){
		 	$("#fareCoteData").slideDown()
		 });
		 
		 function hideFareQuote(){
		 	$("#btnFareQuote").attr("disabled","");
		 	$("#fareCoteData").hide();
		 };
		 function enebleTooltip(){
			$('a.c').tooltip({ 
					track: true, 
					delay: 0, 
					showURL: false, 
					showBody: " - ", 
					fade: 250 
			});
		};
		

});
</script>
</div>
<div style="position: absolute; left: 650px; top: 10px; width: 300px; visibility: visible;" id="divAgentBalance"><table cellspacing="1" cellpadding="2" border="0" width="100%" class="GridRow GridHeader GridHeaderBand GridHDBorder">	<tbody><tr>		<td>			<table cellspacing="1" cellpadding="2" border="0" width="100%">				<tbody><tr>					<td align="center" width="58%" class="GridItemRow GridItem GridItemHover">						<font class="fntBold">Agents Available Credit </font>					</td>					<td align="right" class="GridItemRow GridItem">						<font class="fntBold">USD 0.00</font>					</td>				</tr>			</tbody></table>		</td>	</tr></tbody></table></div>
</body>
</html>
