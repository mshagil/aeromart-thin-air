<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <%@ include file='../common/inc_PgHD.jsp' %>
    <style>
    	.timeblock{
    		height:290px;
    		float: left;
    		background: url("../images/line_no_cache.jpg") no-repeat scroll 3px 14px	;
    	}
    	.even{
    		background: #ecf1fd;
    	}
    	.flightItem{
    		background: #ed8100;
    		position: relative;
    		z-index: 30;color: #fff;
    	}
    	.flightRow{
    		height: 28px
    	}
    	.mOver{background:#980000;color: #fff;cursor: pointer;}
    	.btnClass{width:155px}
    	.btnClass input{padding: 0px 1px}
    	.tabiItems td{padding: 3px}
    	.testUl{list-style: none;}
    	.testUl li {list-style: none;margin: 0 5px;width: 130px;overflow: hidden;}
    	.testUl li a{color: #2f4de5;text-decoration: none;}
    	.testUl li a:hover{color: #980000}
    	.dataTable td{background: #fff}
    </style>
  </head>
   <body style="margin: 0;padding: 0">
   <div id="divLegacyRootWrapper">
   		<div id="tabs" style="width:950px;margin:0px auto;paddig-top:5px;">
			<ul>
				<li><a href="#tabs-1">Shop Screen</a></li>
				<li><a href="#tabs-2">Advance Booking</a></li>
				<!-- <li><a href="#tabs-3">Multicity</a></li> -->
			</ul>
			<div id="tabs-1" style="height: 572px">
				<div id="searchOpt">
				<div class="ui-widget ui-widget-content ui-corner-all">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="170" valign="top">
							<div style="font-size: 14px;font-weight: bold;margin: 10px 5px">Today</div>
							<div style="margin: 10px 5px"><img src="../images/departure.jpg" width="110"/></div>
							<div style="font-weight: bold;margin: 10px 5px 5px">Filter</div>
							<div style="padding: 3px 5px"><input type="text" style="width:100px"></div>
							
						</td>
						<td width="770">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<thead>
									<tr>
										<td>
											<table>
												<tr>
													<td>Hub</td>
													<td>
														<select name="selHub" id="selHub">
															<option value="0">----Select all----</option>
															<option value="0">SHJ</option>	
														</select>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="height: 25px">&nbsp;</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div style="overflow-y:auto;height: 290px;position:relative ;">
												<table cellpadding="0" cellspacing="0" border="0" id="flightRows">
													
												</table>
											</div>
											<div id="timeLine" style="height: 290px;width: 100%;position: absolute ;z-index: 5;">
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</table>
				</div>
				<div style="float: left;width:400px;margin: 5px 0 0 0;">
					<div class="ui-widget ui-widget-content ui-corner-top">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr><td class="ui-widget-header" colspan="2" style="padding: 4px;border-bottom: 0;border-top: 0">Make Booking</td></tr>
						</table>
					</div>
				
					<div class="ui-widget ui-widget-content ui-corner-bottom tabiItems">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td width="75px">From</td>
								<td><select  name="txtFrom" id="txtFrom" ></select></td>
							</tr>
							<tr>
								<td width="75px">To</td>
								<td><select  name="txtTo" id="txtTo" ></select></td>
							</tr>
							<tr>
								<td width="75px" class="alignLeft"> <label class="fntBold" id="lblDepatureOn"> Depart on: </label> </td>
								<td class="alignLeft"> 
									<input type="text"  name="departureDateBox" id="departureDateBox" />
								 </td>
							</tr>
							<tr>
							    <td width="75px" class="alignLeft"> <label class="fntBold" id="lblReturnOn">Return on</label> </td>
								<td class="alignLeft"> 
									<input type="text" name="returnDateBox" id="returnDateBox"  />
								 </td>
								 <td></td>											 
							</tr>
							<tr><td colspan="2">
								<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tbody><tr>
										<td class="alignLeft" style="width: 75px;"><label class="fntBold" id="lblAdults">Adults</label></td>
										<td style="width: 70px;" class="alignLeft">
											<select style="width: 45px;" size="1" id="selAdults" name="selAdults">
											<option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>
										</td>
										<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblChildren">Children</label></td>
										<td style="width: 70px;" class="alignLeft">
											<select style="width: 45px;" size="1" id="selChild" name="selChild"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option></select>
										</td>									
										<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblnfants">Infants</label></td>
										<td style="width: 65px;" class="alignLeft">
											<select style="width: 45px;" size="1" id="selInfants" name="selInfants"><option value="0">0</option><option value="1">1</option></select>
										</td>
										<td></td>
									</tr>							
								</tbody></table>
							</td></tr>
							<tr>
								<td colspan="2" align="right">
									<input type="button" value="Search"/>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div style="float: right;width:400px;margin: 5px 0 0 0;">
					<div class="ui-widget ui-widget-content ui-corner-top">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr><td class="ui-widget-header" colspan="2" style="padding: 4px;border-bottom: 0;border-top: 0">Find Booking</td></tr>
						</table>
					</div>
				
					<div class="ui-widget ui-widget-content ui-corner-bottom tabiItems">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td width="75px">PNR</td>
								<td><input type="text"></td>
							</tr>
							<tr>
								<td width="75px">E-Ticket</td>
								<td><input type="text"></td>
							</tr>
							<tr>
								<td width="75px">Last Name</td>
								<td><input type="text"></td>
							</tr>
							<tr>
								<td width="75px">Mobile</td>
								<td><input type="text"></td>
							</tr>
							<tr>
								<td colspan="2" align="right">
									<input type="button" value="Find"/>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div style='clear:both;height:1px'></div>
				</div>
				<!-- Bookmark -->
				<div id="bookMark" title="Click to view bookmarks">
					<div class="ui-widget-header bookmarkClick" style="padding: 4px">Bookmarks / Notes</div>
					<div style="display: none;" id="bookMarkBody">
						<div style="float: left;width:600px" class="ui-widget ui-widget-content">
							<table width="100%" cellspacing="0" border="0">
								<thead>
								<tr style="background: #dfe2f1">
								<td colspan="5" style="border-bottom: 1px solid #666;padding: 3px"><label style="font-size: 12px;font-weight: bold">Bookmarks</label></td></tr>
								<tr style="background: #dfe2f1">
									<td width="25">&nbsp;</td>
									<td><label style="font-size: 12px;font-weight: bold">PNR</label></td>
									<td><label style="font-size: 12px;font-weight: bold">Name</label></td>
									<td><label style="font-size: 12px;font-weight: bold">Status</label></td>
									<td><label style="font-size: 12px;font-weight: bold">Comments</label></td>
								</tr>
								</thead>
								<tbody id="books">
									
								</tbody>
							</table>
						</div>
						<div style="float: right;width:300px" class="ui-widget ui-widget-content">
							<table width="100%" cellspacing="0" border="0" id="notes" style="background: #fbf8b2">
								<tr>
									<td style="border-bottom: 1px solid #999"><label style="font-size: 14px;font-weight: bold">Notes</label></td>
								</tr>
								<tr><td style="height: 300px;overflow-y:auto" id="NoteItem" valign="top">
									
								</td></tr>
								<tr>
									<td align="right" style="border-top: 1px solid #999">
										<a href="javascript:void(0)" title="Add New Note" class="addnote"> + Add</a>&nbsp;
										<a href="javascript:void(0)" title="Delete Note"> - Delete</a>&nbsp;
										<a href="javascript:void(0)" title="Delete All Note" class="delAllNote"> - Delete All</a>
									</td>
								</tr>
							</table>
						</div>
						<div style='clear:both;height:1px'></div>
					</div>
				</div>
			</div>
			<div id="tabs-2" style="height: 572px">
				<div id="advaSearchOpt">
					<div class="ui-widget ui-widget-content ui-corner-all" style="padding:5px">
						<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td>
									<div class="fntBold">From</div>
									<input type="text"  name="txtFrom" id="txtFrom1" >
								</td>
								<td>
									<div class="fntBold" >To</div>
									<input type="text"  name="txtTo" id="txtTo1" >
								</td>
								<td class="alignLeft">
									<div class="fntBold" > Depart on: </div> 
									<input type="text"  name="departureDate" id="departureDate" />
								 </td>
	 							 <td class="alignLeft"> 
									<div class="fntBold" >Return on</div>
									<input type="text" name="returnDate" id="returnDate"  />
								 </td>
								 <td>
									 <div class="fntBold" >&nbsp;</div>
								 	<input type="button" value="Clear" id="btnClear"/>
								 </td>
							</tr>
							<tr>
								<td valign="top">
									<ul id="fromUL" class="testUl">
										<li><a href="javascript:void(0)">SHJ - Sharjah</a></li>
										<li><a href="javascript:void(0)">SAW - Istanbul Sabiha Int Airport</a></li>
										<li><a href="javascript:void(0)">CMB - Colombo</a></li>
										<li><a href="javascript:void(0)">BLR - Bangalore</a></li>
										<li><a href="javascript:void(0)">BOM - Mumbai</a></li>
									</ul>
								
								</td>
								<td valign="top">
									<ul id="toUL" class="testUl">
										<li><a href="javascript:void(0)">CMB - Colombo</a></li>
										<li><a href="javascript:void(0)">SAW - Istanbul Sabiha Int Airport</a></li>
										<li><a href="javascript:void(0)">BLR - Bangalore</a></li>
										<li><a href="javascript:void(0)">SHJ - Sharjah</a></li>
									</ul>
								
								</td>
								<td rowspan="2">
								<div id="datepicker1"></div>
								
								</td>
								<td rowspan="2">
								<div id="datepicker2"></div>
								
								</td>
								<td valign="top" align="right" rowspan="2">
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="height: 170px">
										<tbody>
											<tr>
												<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblAdults">Adults</label></td>
												<td style="width: 70px;" class="alignLeft">
													<select style="width: 45px;" size="1" id="selAdults" name="selAdults">
													<option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>
												</td>
											</tr>
											<tr>
											<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblChildren">Children</label></td>
											<td style="width: 70px;" class="alignLeft">
												<select style="width: 45px;" size="1" id="selChild" name="selChild"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option></select>
											</td>
											</tr>
											<tr>									
											<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblnfants">Infants</label></td>
											<td style="width: 65px;" class="alignLeft">
												<select style="width: 45px;" size="1" id="selInfants" name="selInfants"><option value="0">0</option><option value="1">1</option></select>
											</td>
											</tr>
											<tr>
											<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblnfants">Cabin</label></td>
											<td style="width: 65px;" class="alignLeft">
												<select style="width: 80px;" size="1" id="selCabin" name="selCabin"><option value="0">Econimy</option><option value="1">Business</option></select>
											</td>
											</tr>
											<tr>
											<td class="alignLeft" style="width: 60px;"><label class="fntBold" id="lblnfants">Currency</label></td>
											<td style="width: 65px;" class="alignLeft">
												<select style="width: 80px;" size="1" id="selCurrency" name="selCurrency"><option value="0">AED</option><option value="1">USD</option></select>
											</td>
											</tr>
											<tr>
												<td colspan="2"><input type="button" value="Search Fares"/></td>
											</tr>
									</tbody></table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td height="30px" colspan="" >
									<input type="button" id="btnSearcgSchedule" value="Search Schedule"/>
								</td>
							</tr>
						</table>
						
								<!-- Shedule -->
								<div id="divShedule" style="display: block;margin-top: 10px">
									<div style="padding: 4px; " class="ui-widget-header">Schedules</div>
									<div style="float:left;width:400px;margin-top: 5px">
									<table cellspacing="0" cellpadding="2" border="0" width="100%">
										<tr>
											<td colspan="6" height="25" id="fSchedule" style="background: #2e3649;color: #fff;padding: 5px">&nbsp;</td>
										</tr>
										<tr>
											<td style="border-left:1px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;height: 20px">G9306</td>
											<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">9:50</td>
											<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">13:50</td>
											<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">Daily</td>
											<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">01/05/2011</td>
											<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">01/08/2011</td>
										</tr>
									</table>
									</div>
									<div style="float:right;width:400px;margin-top: 5px">
										<table cellspacing="0" cellpadding="2" border="0" width="100%">
											<tr>
												<td colspan="6" height="25" id="tSchedule" style="background: #2e3649;color: #fff;padding: 5px">&nbsp;</td>
											</tr>
											<tr>
												<td style="border-left:1px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;height: 20px">G9306</td>
												<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">9:50</td>
												<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">13:50</td>
												<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">Daily</td>
												<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">01/05/2011</td>
												<td style="border-left:0px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;">01/08/2011</td>
											</tr>
										</table>
									</div>
									<div style='clear:both;height:1px'></div>
								</div>
								<!-- daksjhdash -->
								<div style="margin-top: 5px" id="pubFa">
									<table cellspacing="0" cellpadding="2" border="0" width="100%">
										<tr>
											<td height="25" style="background: #2e3649;padding: 5px">
												<label style="font-size: 12px;font-weight:bold;color: #fff;cursor: pointer" id="pubFare"><u>Published Fares</u></label>
											</td>
										</tr>
										<tr>
											<td style="background: #2e3649">
												<table cellspacing="1" cellpadding="2" border="0" width="100%" class="dataTable">
													<tr>
														<td>&nbsp;</td>
														<td>Fare Basis</td>
														<td>Booking Class</td>
														<td>Trip Tipe</td>
														<td>Fare</td>
														<td>Cabin</td>
														<td>Effective Date</td>
														<td>Expiration date</td>
														<td>Min/Max Stay</td>
														<td>Adv Purchase Req.</td>
														<td>Pre Booking</td>
														<td>Name Change</td>
														<td>Refund</td>
														<td>Cancel</td>
													</tr>
													<tr>
														<td><input type="radio" class="fareItem" id="fare1" name="fareRadio"/></td>
														<td>TX0NSSL</td>
														<td>T</td>
														<td>Round Trip</td>
														<td>360 (AED)</td>
														<td>E</td>
														<td>12/06/2011</td>
														<td>12/08/2011</td>
														<td>20 /30</td>
														<td>&nbsp;</td>
														<td>Yes (Free)</td>
														<td>Yes (1)</td>
														<td>TC</td>
														<td>Yes (100%)</td>
													</tr>
													<tr>
														<td><input type="radio" class="fareItem" id="fare1" name="fareRadio"/></td>
														<td>TX0NSSL</td>
														<td>T</td>
														<td>Round Trip</td>
														<td>360 (AED)</td>
														<td>E</td>
														<td>12/06/2011</td>
														<td>12/08/2011</td>
														<td>20 /30</td>
														<td>&nbsp;</td>
														<td>Yes (Free)</td>
														<td>Yes (1)</td>
														<td>TC</td>
														<td>Yes (100%)</td>
													</tr>
													<tr>
														<td><input type="radio" class="fareItem" id="fare1" name="fareRadio"/></td>
														<td>TX0NSLL</td>
														<td>T</td>
														<td>Round Trip</td>
														<td>360 (AED)</td>
														<td>E</td>
														<td>12/06/2011</td>
														<td>12/00/2011</td>
														<td>20 /30</td>
														<td>&nbsp;</td>
														<td>Yes (Free)</td>
														<td>Yes (1)</td>
														<td>TC</td>
														<td>Yes (100%)</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>
								
								
								<div style="margin-top: 5px" id="forcast">
									<table cellspacing="0" cellpadding="2" border="0" width="100%">
										<tr>
											<td height="25" style="background: #2e3649;padding: 5px">
												<label style="font-size: 12px;font-weight:bold;color: #fff;cursor: pointer" id="pubFare">Forecast</label>
											</td>
										</tr>
										<tr>
											<td style="border-left:1px solid #2e3649; border-bottom:1px solid #2e3649;border-right:1px solid #2e3649;height: 20px">
												Forecast not available for this mode
											</td>
										</tr>
									</table>
							
							</div>
					</div>
			</div>
			</div>
			<!-- 
			<div id="tabs-3" style="height: 572px">
				Not Functional in Demo mode
			</div>
			 -->
		</div>
	</div>
   </body>
   <script type="text/javascript"><!--
   
   $(function() {
	   var availableTags = ["Select",
	            			"SHJ - Sharjah",
	            			"SCO - Aktau Airport",
	            			"SAW - Istanbul Sabiha Int Airport",
	            			"SNN - Shannon airport",
	            			"STN - London - Stansted T1",
	            			"SYZ - Shiraz",
	            			"SSH - Sharm El Sheikh",
	            			"CAI - Cairo International Airport",
	            			"CDG - Paris - Charles De Gaulle T3",
	            			"CJB - Coimbatore",
	            			"CMB - Colombo",
	            			"CMN - Casablanca T2",
	            			"COK - Kochi",
	            			"CUF - Torino - Cuneo",
	            			"BAH - Bahrain",
	            			"BCN - Barcelona - Barcelona International T 2A",
	            			"BEN - Benghazi",
	            			"BEY - Beirut",
	            			"BLR - Bangalore",
	            			"BOM - Mumbai",
	            			"BRU - Brussels - Terminal M"
	            		];
	   var flightList = [{depTime:9, arrTime:13, duration:4, number:"G9501", fare:"AED275", route:"SHJ/CMB",pnr:43545653, name:"Mr. Peter Paker",status:"CNF",comment:"waiting forpayment"},
	                     {depTime:10, arrTime:14, duration:3, number:"G9507", fare:"AED303", route:"SHJ/DOH",pnr:43565353, name:"Mr. TBA TBA",status:"STB",comment:"payment pending"},
	                     {depTime:8, arrTime:11, duration:3, number:"G9503", fare:"AED620", route:"SHJ/CMN",pnr:43545353, name:"Mr. Samson de Silve",status:"STB",comment:"processing..."},
	                     {depTime:3, arrTime:6, duration:3, number:"G9504", fare:"AED310", route:"SHJ/COK",pnr:12545353, name:"Mr. M M Abdula",status:"STB",comment:"completed"},
	                     {depTime:12, arrTime:15, duration:3, number:"G9511", fare:"AED340", route:"SHJ/BAH",pnr:43545353, name:"Mr. TBA TBA",status:"CNF",comment:"waiting forpayment"},
	                     {depTime:4, arrTime:8, duration:4, number:"G9506", fare:"AED560", route:"SHJ/SYZ",pnr:43545353, name:"Mr. TBA TBA",status:"CNF",comment:"waiting forpayment"},
	                     {depTime:10, arrTime:14, duration:3, number:"G9507", fare:"AED455", route:"SHJ/CJB",pnr:43766353, name:"Mr. TBA TBA",status:"CNF",comment:"processing..."},
	                     {depTime:8, arrTime:11, duration:3, number:"G9503", fare:"AED450", route:"SHJ/CUF",pnr:435874553, name:"Mr. TEST NAME",status:"CNF",comment:"processing..."},
	                     {depTime:3, arrTime:6, duration:3, number:"G9504", fare:"AED340", route:"SHJ/BEN",pnr:43545353, name:"Mr. TBA TBA",status:"CNF",comment:"completed"},
	                     {depTime:12, arrTime:15, duration:3, number:"G9511", fare:"AED370", route:"SHJ/BOM",pnr:43222253, name:"Mr. TBA TBA",status:"STB",comment:"completed"},
	                     {depTime:4, arrTime:8, duration:4, number:"G9506", fare:"AED470", route:"SHJ/BCN",pnr:4300053, name:"Mr. TBA TBA",status:"STB",comment:"-------"},
	                     {depTime:12, arrTime:15, duration:3, number:"G9510", fare:"AED405", route:"SHJ/CMB",pnr:43111353, name:"Mr. TBA TBA",status:"STB",comment:"test comment"},
	                     {depTime:3, arrTime:5, duration:2, number:"G9509", fare:"AED210", route:"SHJ/BAH",pnr:43512153, name:"Mr. TBA TBA",status:"STB",comment:"wating"}]
		$( "#tabs" ).tabs({
			select: function(event, ui) {
				if (ui.index == 1){
					tab2();
				}
			}
		});
	   $( ".bookmarkClick" ).click(function(){
		   if ( $( "#bookMarkBody" ).css("display") == "none" ){
			   $( "#searchOpt" ).slideUp();
			   $( "#bookMarkBody" ).slideDown();  
			   addBookmarkList();
			   $(".ttRow").unbind("mouseenter").bind("mouseenter", function(){
					$(this).addClass("mOver");
				});
				$(".ttRow").unbind("mouseleave").bind("mouseleave", function(){
					$(this).removeClass("mOver");
				});
		   }else{
			   $( "#searchOpt" ).slideDown();
			   $( "#bookMarkBody" ).slideUp();
		   }
		   
	   });
		$( "#departureDateBox" ).datepicker({
			showOn: "button",
			buttonImage: "../images/Calendar_no_cache.gif",
			buttonImageOnly: true
		});
		$( "#returnDateBox" ).datepicker({
			showOn: "button",
			buttonImage: "../images/Calendar_no_cache.gif",
			buttonImageOnly: true
		});
		$(".addnote").click(function(){
			$("#NoteItem").append("<textarea cols='38' rows='3' id='newComment' style='border:0;'></textarea>");
			$("#newComment").unbind("blur").bind("blur", function(){
				$(this).parent().append("<div style='border-bottom:1px solid #999;padding:2px'>"+$(this).val()+"</div>");
				$(this).remove();
			});
		});
		$(".delAllNote").click(function(){
			$("#NoteItem").html("");
		});
		/*	
		$( "#txtFrom" ).autocomplete({
			source: availableTags
		});
		$( "#txtTo" ).autocomplete({
			source: availableTags
		});
		*/
		function drowTimeLine(){
			var p = $("#timeLine").prev();
			$("#timeLine").css({
				top:p.position().top - 20,
				left:p.position().left + 155,
				width:p.width() - 170
			});
			var timeblockWidth = parseInt((p.width() - 155)/12);
			var str = "";
			for (var i =0;i<12;i++){
				str +="<div class='timeblock' style='width:"+(timeblockWidth - 1)+"px'>"+addTimeFormat(i)+"</div>";
			}
			str +="<div style='clear:both;height:1px'></div>";
			$("#timeLine").html(str);
		};
		
		function addTimeFormat(i){
			var t = i * 2;
			var ret = "";
			if (t == 0){
				ret = "Midn"
			}else if (t == 12){
				ret = "Noon"
			}else{
				ret = t + ":00"
			}

			return ret;
		};
		function addFlightsTotable(){
			var str = "";
			var p = $("#timeLine").prev();
			$.each(flightList,function(i,j){
				if (i%2 == 0){
					str +="<tr><td class='flightRow btnClass even'><input type='button' value='Buy "+j.fare+"' class='bookFlight'/> "+j.route+"</td>"+
					"<td class='flightRow even' style='width:"+(p.width() - 155)+"px'>"+blockFlight(j)+"</td></tr>";
				}else{
					str +="<tr><td class='flightRow btnClass'><input type='button' value='Buy "+j.fare+"' class='bookFlight'/> "+j.route+"</td>"+
					"<td class='flightRow' style='width:"+(p.width() - 155)+"px'>"+blockFlight(j)+"</td></tr>";
				}
			});
			$("#flightRows").html(str);
		};
		
		function addBookmarkList(){
			var str = "";
			var p = $("#timeLine").prev();
			$.each(flightList,function(i,j){
				if (i%2 == 0){
					str +="<tr class='even ttRow'><td class='flightRow'>"+(i+1)+"</td>"+
					"<td class='flightRow btnClass'>"+j.pnr+"</td>"+
					"<td class='flightRow btnClass'>"+j.name+"</td>"+
					"<td class='flightRow btnClass'>"+j.status+"</td>"+
					"<td class='flightRow btnClass'>"+j.comment+"</td></tr>";
				}else{
					str +="<tr class='ttRow'><td class='flightRow'>"+(i+1)+"</td>"+
					"<td class='flightRow btnClass'>"+j.pnr+"</td>"+
					"<td class='flightRow btnClass'>"+j.name+"</td>"+
					"<td class='flightRow btnClass'>"+j.status+"</td>"+
					"<td class='flightRow btnClass'>"+j.comment+"</td></tr>";
				}
			});
			$("#books").html(str);
		};
		
		function blockFlight(flight){
			var str = "<div class='flightItem' title='Click to book this flight' style='margin-left:"+(flight.depTime*30)+"px;width:"+(flight.duration*30)+"px'>"+flight.number+"</div>";
			return str;
		};
		function fillDwopDown(){
			var str = null;
			$.each(availableTags, function(i,j){
				str +="<option value='"+i+"'>"+j+"</option>";
			});
			$("#txtFrom").html(str);
			$("#txtTo").html(str);
		}
			
		addFlightsTotable();
		drowTimeLine();
		fillDwopDown();
		$(".flightItem").unbind("mouseenter").bind("mouseenter", function(){
			$(this).addClass("mOver");
		});
		$(".flightItem").unbind("mouseleave").bind("mouseleave", function(){
			$(this).removeClass("mOver");
		});
		$(".bookFlight, .flightItem").unbind("click").bind("click", function(){
			var t = confirm("Do you want to book this flight?");
		});
		function tab2(){
			$(".dataTable").hide();
			$("#forcast").hide();
			$("#divShedule").hide();
			$("#pubFa").hide();
			$( "#datepicker1" ).datepicker({
				minDate: -0,
				onSelect: function(dateText, inst) { 
					$("#departureDate").val(dateText);
				}
				});
			$( "#datepicker2" ).datepicker({minDate: -0,
				onSelect: function(dateText, inst) { 
					$("#returnDate").val(dateText);
				}
			});
			$("#pubFare").click(function(){
				$(".dataTable").show();
			});
			$("input[name='fareRadio']").click(function (){
				$("#forcast").show()
			});
			
			$("#fromUL li a").click(function(){
				$("#txtFrom1").val($(this).text());
				$("#txtTo1").focus();
			});
			
			$("#toUL li a").click(function(){
				$("#txtTo1").val($(this).text());
				$("#txtTo1").focus();
			});
			
			$("#txtTo1, #txtFrom1").focus(function(){
				if ( $("#txtTo1").val() != "" && $("#txtFrom1").val() != ""){
					$("#fSchedule").html( $("#txtFrom1").val() + " to " +  $("#txtTo1").val());
					$("#tSchedule").html( $("#txtTo1").val() + " to " +  $("#txtFrom1").val());
					$("#divShedule").show();
					$("#pubFa").show();
				}
			});
			
			$("#btnClear").click(function(){
				$("#txtFrom1").val("");
				$("#txtTo1").val("");
				$("#departureDate").val("");
				$("#returnDate").val("");
				$(".dataTable").hide();
				$("#forcast").hide();
				$("#divShedule").hide();
				$("#pubFa").hide();
			});
		}
		
	});
   
   --></script>
</html>