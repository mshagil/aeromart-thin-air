


<div id='divInvoicesDisplay' style='position:absolute;top:200px;left:350px;height:306px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key=""> &nbsp;Invoices List View</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trPaxOpt'>
							<td class='popBGColor'  align='center'>
							    <div style="height:250px; overflow-y:auto;overflow-x:hidden">						
									<table width='95%' border='0' cellpadding='1' cellspacing='0'>
										<tr>
											<td colspan="2" align='left' i18n_key="">Following are the list of Invoices, Please select invoices to Print.</td>
										</tr>
										<tr>
											<td colspan="2" align="left">											
											------------------------------------------------------------- 
											</td>
										</tr>	
										<!-- <tr>
											<td>											
											<input type="checkbox" id="chkPaxSelectAll" name="chkPaxSelectAll" onclick="selectAllPaxOnClick(this);"/>	
											</td>
											<td align="left"  i18n_key="PassengerInfo_opt_SelectAll">	
												Select All 
											</td>
										</tr>	 -->	
										<tr>
											<td colspan="2" align="left">											
											------------------------------------------------------------- <BR>
											</td>
										</tr>										
										<tr id="trInvoiceListTemp">
											<td>										   
												<input type="checkbox" name="chkPax" style="display:none"/>																						    
											</td>
											<td align="left">
												<label class='txtBold' ></label>
											</td>
										</tr>										
										<tr>
											<td class='popBGColor singleGap'></td>
										</tr>
										<tr>
											<td class='popBGColor singleGap'></td>
										</tr>
									</table>
								</div>
							</td>							
						</tr>						
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>	
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>						
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								<button id="btnInvoicePrint" type="button"   class="btnMargin" i18n_key="" style="width:110px">Print Invoice(s)</button>
								<button id="btnInvoiceCancel" type="button"  class="btnMargin" onclick="$('#divInvoicesDisplay').hide();" i18n_key="btn_Cancel">Cancel </button>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>