<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="x-ua-compatible" content="IE=8"></meta>

<%@ include file='../common/inc_PgHD.jsp' %>
	<script type="text/javascript">
	
		var DATA_ResPro = new Array();
		DATA_ResPro["initialParams"] = eval('(' + '<c:out value="${requestScope.initialParams }" escapeXml="false" />' + ')');	
	
		var strPaxID = "<c:out value="${requestScope.reqPaxID}" escapeXml="false" />";		
		var jsPaxETicketMask = null;
	
	</script>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
<script type="text/javascript" src="../js/v2/common/commonErrors.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/modify/paxETckLoadData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/modify/paxETicketMask.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
</head>
<body style="background: white;">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:850px;overflow-x:hidden;height:465px;overflow-y:auto;">
		<table style='width:850px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:850px;">
						
							<table width='95%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>
								<tr>
									<td>
										<label class='txtLarge txtBold' i18n_key = "BookingInfo_Etick_PassengrEtickDetails">Passenger E-Ticket Mask Details</label>
									</td>
								</tr>
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>
								<tr>
									<td class="ui-widget ui-widget-content ui-corner-all" align='center'>
										<form method='post' action="" id="frmPaxETckMask">
											<table width='95%' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td colspan='8' class='singleGap'></td>
												</tr>
												<tr>
													<td width='5%'><label class='txtBold' i18n_key="BookingInfo_Etick_PassengrID">PAX Id :</label></td>
													<td width='8%'><div id='divPNRPaxId'></div></td>
													
													<td width='8%'>
														<label class='txtBold' i18n_key="BookingInfo_Etick_PassengerName">PAX Name :</label>
													</td>
													<td width='20%'>
														<select id='selPax' name='selPax' size='1' style="width:200px;"  onchange='UI_paxETckMask.selPaxOnChange()'>
														</select>
													</td>
													
													<td width='10%'>
														<label class='txtBold' i18n_key="BookingInfo_Etick_SegementStatus">Segment Status :</label>
													</td>
													<td width='8%'>
														<select id='selSegmentStatus' style="width:60px;" name='selSegmentStatus' size='1' onchange='UI_paxETckMask.selOnChange()'>
														<option value=""></option>
														<c:out value="${requestScope.reqSegmentStatusHtml }" escapeXml="false" />
														</select>
													</td>
													
													<td width='10%'>
														<label class='txtBold' i18n_key ="BookingInfo_Etick_EtickettStatus">E-ticket Status :</label>
													</td>
													<td width='8%'>
														<select id='selEticketStatus' style="width:50px;" name='selEticketStatus' size='1' onchange='UI_paxETckMask.selOnChange()'>													
														</select>
													</td>				
													
												</tr>				
												<tr>
													<td colspan='8' class='singleGap'></td>
												</tr>							
											</table>
										</form>
									</td>
								</tr>					
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>			
								<tr>
									<td style='height:110px;' valign="top" class="ui-widget ui-widget-content ui-corner-all" align='center'>
										
										<table width='97%' border='0' cellpadding='0' cellspacing='0'>											
											<tr>
												<td style='height:100px;' valign='top'>
													<table id="tblPaxETckMask" class="scroll" cellpadding="0" cellspacing="0" width="97%"></table>
												</td>
											</tr>																					
										</table>												
									</td>
								</tr>
								<tr>
									<td  class='singleGap'>&nbsp;</td>
								</tr>
								
								<tr id="passengerCouponInput">
									<td style="height:100px;" class="ui-widget ui-widget-content ui-corner-all" >
									<form method='post' action="" id="frmPassengerCoupon">
										<table  width='100%' border='0' cellpadding='1' cellspacing='0'>
											<tr>
												<td  class='singleGap'>&nbsp;</td>
											</tr>
											
											<tr>
												<td width='20%'> <label class='txtBold' i18n_key="BookingInfo_Etick_AirlineSegment"> Airline Segment : </label></td>
												<td width='80%'>
													<table border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td>
																<input id="txtSegmentCode" name="txtSegmentCode" type="text" class="aa-input" style="width:120px" maxlength="8" readonly="readonly" />
															</td>												
														</tr>
													</table>
												</td>
											</tr>
											
											<tr>
												<td width='20%'><label class='txtBold' i18n_key="BookingInfo_Etick_FlightNumber"> Flight Number : </label></td>
												<td width='80%'>
													<table border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td>
																<input id="txtFlightNum" name="txtFlightNum" type="text" class="aa-input" style="width:120px" maxlength="8" readonly="readonly"/>
															</td>												
														</tr>
													</table>
												</td>
											</tr>
											
											<tr>
												<td><label class='txtBold' i18n_key ="BookingInfo_Etick_TicketNumberAndCoupon">Ticket Number and Coupon :</label> </td>
												<td width='80%'>
													<table border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td>
																<input id="txtTktNumber" name="txtTktNumber" type="text" class="aa-input" style="width:120px" maxlength="13" readonly="readonly"/>
															</td>																											
														</tr>
													</table>
												</td>
											</tr>
											
											<tr>
												<td> <label class='txtBold' i18n_key="BookingInfo_Etick_PassengerStatus"> Pax Status : </label></td>
												<td>
													<select id='selPaxStatus' name='selPaxStatus' size='1' class="aa-input" style="width:50px"></select>
													<%@ include file='../common/inc_MandatoryField.jsp' %>		
												</td>
											</tr>
											
											<tr>
												<td> <label class='txtBold' i18n_key="BookingInfo_Etick_CouponStatus"> Coupon Status : </label></td>
												<td>
													<select id='selCouponStatus' name='selCouponStatus' size='1' class="aa-input" style="width:50px"></select>
													<%@ include file='../common/inc_MandatoryField.jsp' %>		
												</td>
											</tr>											
											<tr>
												<td valign='top'> <label class='txtBold' i18n_key="BookingInfo_Etick_UserNotes">User notes : </label> </td>
												<td valign='top'>
													<table cellpadding='0' cellspacing='0'>
														<tr>
															<td><textarea id='txtUN' name='txtUN' rows='2' cols='60' class="aa-input"></textarea></td>
															<td valign='top'>
																<%@ include file='../common/inc_MandatoryField.jsp' %>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td  class='singleGap'>&nbsp;</td>
											</tr>											
										</table>
										</form>
									</td>
								</tr>
								<tr>
									<td align="right">
										<button id="btnEdit" type="button" class="btnMargin" i18n_key="btn_Edit">Edit</button>
										<button id="btnSave" type="button" class="btnMargin" i18n_key="btn_Save">Save</button>
										<button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
									</td>									
								</tr>							
								<tr>
									<td  class='singleGap'></td>
								</tr>								
							</table>							
						
					</div>
				</td>
			</tr>
			<tr>
				<td>
				<%--
					<div id="divLegacyRootWrapperPopUpFooter">
						<%@ include file='../reservation/inc_pageError.jsp'%>
					</div>
				 --%>
				</td>
			</tr>
		</table>
	</div>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
</body>
</html>

<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguageForOpener();
</script>
