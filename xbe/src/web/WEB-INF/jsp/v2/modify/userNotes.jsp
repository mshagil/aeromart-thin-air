<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp' %>
<script type="text/javascript">
	<!--
	var jsHistoryData = eval('(' + '<c:out value="${requestScope.userNoteHistory}" escapeXml="false" />' + ')');
//	-->
</script>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
<script type="text/javascript" src="../js/v2/common/jQuery.message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript" src="../js/v2/modify/userNotes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapperPopUp" style="display:none;width:900px;">
		<table style='width:900px;' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class='ui-widget'>
					<div id="divPane" style="margin-top:0px;margin-left:20px;margin-right:20px;width:880px;">
						<table width='97%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td>
									<label class='txtLarge txtBold' i18n_key="">User Notes - History</label>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr>
								<td>
									<table id="tblHistory" class="scroll" cellpadding="0" cellspacing="0"></table>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
							<tr>
								<td align='right'>
									<button id="btnClose" type="button" class="btnMargin" i18n_key="btn_Close">Close</button>
								</td>
							</tr>
							<tr>
								<td  class='singleGap'></td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
<script type="text/javascript">
$("#divLegacyRootWrapperPopUp").getLanguage();
</script>
