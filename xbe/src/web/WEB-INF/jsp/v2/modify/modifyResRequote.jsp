<%@ page language="java"%>
<%@ include file="../../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file='../common/inc_PgHD.jsp'%>
<%@ include file='../common/inc_ShortCuts.jsp'%>
<script type="text/javascript">
<!--
	var DATA_ResPro = new Array();
    DATA_ResPro["initialParams"] = eval('('
            + '<c:out value="${requestScope.initialParams }" escapeXml="false" />'
            + ')');
    DATA_ResPro["reservationInfo"] = eval('('
            + '<c:out value="${requestScope.reservationInfo }" escapeXml="false" />'
            + ')');
    DATA_ResPro["resSegments"] = eval('('
            + '<c:out value="${requestScope.resSegments }" escapeXml="false" />'
            + ')');

    DATA_ResPro["reservationInfo"] = eval('('
            + '<c:out value="${requestScope.reservationInfo }" escapeXml="false" />'
            + ')');
    DATA_ResPro["resPaxs"] = eval('('
            + '<c:out value="${requestScope.resPaxs }" escapeXml="false" />'
            + ')');
    DATA_ResPro["resPaySummary"] = eval('('
            + '<c:out value="${requestScope.resPaySummary }" escapeXml="false" />'
            + ')');
    DATA_ResPro["resContactInfo"] = eval('('
            + '<c:out value="${requestScope.resContactInfo }" escapeXml="false" />'
            + ')');
    DATA_ResPro["resHasInsurance"] = '<c:out value="${requestScope.resHasInsurance }" escapeXml="false" />';
    DATA_ResPro["blnIsFromNameChange"] = '<c:out value="${requestScope.blnIsFromNameChange }" escapeXml="false" />';
    DATA_ResPro["blnIsFromNameChange"] = '<c:out value="${requestScope.blnIsFromNameChange }" escapeXml="false" />';
    DATA_ResPro["paxNameDetails"] = '<c:out value="${requestScope.paxNameDetails }" escapeXml="false" />';
    DATA_ResPro["reasonForAllowBlPax"] = '<c:out value="${requestScope.reasonForAllowBlPax }" escapeXml="false" />';
    DATA_ResPro["systemParams"] = eval('('
            + '<c:out value="${requestScope.systemParams }" escapeXml="false" />'
            + ')');
    DATA_ResPro["existingBusCarrierCodes"] = eval('('
            + '<c:out value="${requestScope.existingBusCarrierCodes }" escapeXml="false" />'
            + ')');

    DATA_ResPro["isAddSegment"] = '<c:out value="${requestScope.isAddSegment }" escapeXml="false" />';
    DATA_ResPro["addGroundSegment"] = '<c:out value="${requestScope.addGroundSegment }" escapeXml="false" />';
    DATA_ResPro["flightDetails"] = '<c:out value="${requestScope.flightDetails }" escapeXml="false" />';
    DATA_ResPro["selectedPnrSeg"] = '<c:out value="${requestScope.selectedPnrSeg }" escapeXml="false" />';
    DATA_ResPro["flexiAlertInfo"] = '<c:out value="${requestScope.flexiAlertInfo }" escapeXml="false" />';
    
    var arrGroundSegmentParentFrom = new Array();
    var arrGroundSegmentParentTo = new Array();
    <c:out value='${requestScope.allowedGroundStations}' escapeXml='false' />
    var carrierCode = '<c:out value="${requestScope.carrierCode}" escapeXml="false" />';
    var selectedFlightSegRefNumber = '<c:out value="${requestScope.selectedFlightSegRefNumber }" escapeXml="false" />';
    var excludableCharges = eval('(' + '<c:out value="${requestScope.excludableCharges }" escapeXml="false" />' + ')');
	var skipNonModifiedSegFareONDs = '<c:out value="${requestScope.skipNonModifiedSegFareONDs}" escapeXml="false" />';
	var modifyUNSegments = '<c:out value="${requestScope.modifyUNSegments}" escapeXml="false" />';
    var jsonResPaymentSummary = null;
    var jsonPaySumObj = null;
    var jsonBkgInfo = null;
    var isMinimumFare = false;
//-->

</script>
<script type="text/javascript"
	src="../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/common/pageShortCuts.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript"
	src="../js/v2/reservation/tabSearchFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/reservation/tabPayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/reservation/tabPassenger.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/reservation/tabAnci.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/reservation/tabAnciSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/modify/tabRequote.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript"
	src="../js/v2/modify/modifyResRequote.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript"
	src="../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<c:if test="${not empty(requestScope.sysOndSource)}">
	<script
		src="<c:out value='${requestScope.sysOndSource}' escapeXml='false'/>"
		type="text/javascript"></script>
</c:if>

</head>
<body class="legacyBody">
	<div id="divLegacyRootWrapper" style="display: none;">
		<div style="height: 5px;"></div>
		<div id="divBookingTabs"
			style="margin-top: 0px; margin-left: 20px; margin-right: 20px; height: 632px;">
			<ul>
				<li><a href="#divSearchTab" style="width: 100px" i18n_key="BookingInfo_FlightDetails_lbl_SearchFlights">Search
						Flights</a></li>
				<li><a href="#divAncillaryTab" style="width: 90px" i18n_key="BookingInfo_FlightDetails_lbl_Ancillary">Ancillary</a></li>
				<li><a href="#divRequoteTab" style="width: 90px" i18n_key="BookingInfo_FlightDetails_lbl_PriceQuote">Price
						Quote</a></li>
				<li><a href="#divPaymentTab" style="width: 90px"i18n_key="BookingInfo_FlightDetails_lbl_Payments">Payments</a></li>
			</ul>
			<div id="divSearchTab" style="padding-top: 5px">
				<%@ include file='../reservation/inc_TabSearchFlights.jsp'%>
			</div>

			<form method='post' action="" id="frmMakeBkg">
				<div id="divAncillaryTab" style="padding-top: 5px">
					<%@ include file='../reservation/inc_TabAnci.jsp' %>					
				</div>
			
				<div id="divRequoteTab" style="padding-top: 5px">
					<%@ include file='inc_TabPriceQuote.jsp'%>
				</div>

				<div id="divPaymentTab" style="padding-top: 5px">
					<%@ include file='../reservation/inc_TabPayment.jsp'%>
				</div>
				<input type="hidden" name="flexiAlertInfo" id="flexiAlertInfo" />

			</form>
		</div>
	</div>
	<form method='post' action="" id="frmTabLoad"></form>
	<%@ include file='../common/inc_Credit.jsp'%>
	<%@ include file='../common/inc_LoadMsg.jsp'%>
	<%@ include file='../reservation/inc_AnciPref.jsp'%>
	<%@ include file='../reservation/inc_FlightStopOverDetails.jsp'%>

</body>
</html>
<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript">
$("#divLegacyRootWrapper").getLanguage();
</script>
