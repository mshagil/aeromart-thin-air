


<div id='divGroupItineraryDisplay' style='position:absolute;top:200px;left:350px;height:306px;width:350px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold' i18n_key="lbl_GroupBookingItineraryView"> &nbsp;Group Booking Itinerary View</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trPaxOpt'>
							<td class='popBGColor'  align='center'>
							    <div style="height:250px; overflow-y:auto;overflow-x:hidden">						
									<table width='95%' border='0' cellpadding='1' cellspacing='0'>
										<tr>
											<td colspan="2" align='left' i18n_key="lbl_SelectPrintViewItinerary">Following are the list of passengers, Please select to Print/View Itinerary.</td>
										</tr>
										<tr>
											<td colspan="2" align="left">											
											--------------------------------------------------------------------------------- 
											</td>
										</tr>	
										<tr>
											<td>											
											<input type="checkbox" id="chkPaxSelectAll" name="chkPaxSelectAll" onclick="selectAllPaxOnClick(this);"/>	
											</td>
											<td align="left"  i18n_key="PassengerInfo_opt_SelectAll">	
												Select All 
											</td>
										</tr>	
										<tr>
											<td colspan="2" align="left">											
											--------------------------------------------------------------------------------- <BR>
											</td>
										</tr>										
										<tr id="trPaxListTemp">
											<td>										   
												<input type="checkbox" name="chkPax" style="display:none"/>																						    
											</td>
											<td align="left">
												<label class='txtBold' ></label>
											</td>
										</tr>										
										<tr>
											<td class='popBGColor singleGap'></td>
										</tr>
										<tr>
											<td class='popBGColor singleGap'></td>
										</tr>
									</table>
								</div>
							</td>							
						</tr>						
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>	
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>						
						<tr>
							<td class='popBGColor rowHeight' align='right'>
								
								<button id="btnPaxItiEmail" type="button"  class="btnMargin" i18n_key="btn_Email">Email </button>
								<button id="btnPaxItiPrint" type="button"   class="btnMargin" i18n_key="btn_Print">Print </button>
								<button id="btnPaxItiIndPrint" type="button" style="width:180px" class="btnMargin" i18n_key="btn_PrintIndividualItinerary">Print Individual Itinerary </button>
								<button id="btnPaxItiIndEmail" type="button" style="width:180px" class="btnMargin" i18n_key="btn_EmailIndividualItinerary">Email Individual Itinerary </button>
								<button id="btnPaxItiCancel" type="button"  class="btnMargin" onclick="$('#divGroupItineraryDisplay').hide();" i18n_key="btn_Cancel">Cancel </button>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>