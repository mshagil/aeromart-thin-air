<%@ page language="java" session="true"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv='lcc-xbe-login' content='adfsdfj123dfa893435#@!bnyufUYR345245!RirasdfnM#djf' />
	<LINK rel="stylesheet" type="text/css" href="../css/oldStyle_no_cache.css">
	<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	
	<script src="../js/login/I18nLogin.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/login/Login.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<style>
		input {
	 		border: 1px solid #AAAAAA;
		}
	</style>
	<script>
		var reOpen=false;
		var request=new Array();
		
		<c:out value='${requestScope.reqRequestData}' escapeXml='false' />
		fullScreen2();
		var erroImgPath = "../images/error_icon_no_cache.gif";
		
		getLoginLanguageData();

		
	</script>



  </head>
  <body onkeypress='return Body_onKeyPress(event)' style="visibility:hidden" scrolling="yes" onkeydown='return Body_onKeyDown(event)' ondrag='return false' oncontextmenu="return showContextMenu()">
		<table width='999' cellpadding='0' cellspacing='0' border='0' align="center" class="TBLBackGround" id="mainTableLogin">
			<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
			<td width='974px;' style='background-image:url(../images/spacer_no_cache.gif);background-repeat:repeat-y;background-color:white;'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td style="height:670px;background-image:url(../images/M037_no_cache.jpg);background-repeat:no-repeat;' class="PageBackGround" align='center'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td style="height:670px;background-image:url(../images/spacer_no_cache.gif);background-repeat:no-repeat;background-position:bottom right;" align='center'>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td style="height:683px;background-image:url(../images/spacer_no_cache.gif);background-repeat:no-repeat;background-position:bottom right;" align='center'>
													<div id="divLogin" >
													<table width='60%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td colspan='3' style='height:110px;'></td>
														</tr>
														<tr>
															<td style="background-image:url(../images/M039_no_cache.jpg);background-repeat:repeat-x;"><img id='imgSysName' src='../images/M047_no_cache.jpg'></td>
															<td style="background-image:url(../images/M039_no_cache.jpg);background-repeat:repeat-x;">
																<form id="formLogin" name="formLogin" method="post" action="../public/j_security_check" style="margin-bottom: 0em; margin-top: 1em;"> 
																	<table width="266" border="0" cellpadding="0" cellspacing="2">
																		<tr>
																			<td colspan="2">
																				<font class="loginfntBold" i18n_key="welcome">Welcome !</font>
																			</td>
																		</tr>
																		<tr>
																			<td colspan="2">
																				<font class="loginfnt"><span id="spnWelcome" i18n_key="welcometext"></span></font>
																			</td>
																		</tr>
																		<!-- <tr><td colspan="2"><font>&nbsp;</font></td></tr> -->
																		<tr>
																			<td width="53%" align="right">
																				<input type="hidden" id="j_token" name="j_token" value="0">
																				<font class="loginfnt" i18n_key="userId">User ID :</font>
																			</td>
																			<td align="right">
																				<input type="hidden" id="j_username" name="j_username" value="">
																				<input type="text" id="username_txt" name="username_txt" size="20" onChange="HidePageMessage()" onkeydown='onAutoSubmit(event)'>
																			</td>
																		</tr>
																		<tr>
																			<td align="right">
																				<font class="loginfnt" i18n_key="password">Password :</font>
																			</td>
																			<td align="right">
																				<input type="password" id="j_password" name="j_password" size="20" onChange="HidePageMessage()" onkeydown='onAutoSubmit(event)'  onkeypress="capsDetect(event)">
																			</td>
																		</tr>								
																		<tr class="captcha" style="display: none;">
																			<td align="right">
																				<img src="../jcaptcha_new" align="top" style="cursor: pointer;" name="imgCPT" id="imgCPT" width="148px" height="63px" onclick="refreshCaptcha()"/>
																			</td>
																			<td align="right">														
																				<input type="text" size="20" name="captcha_txt" id="captcha_txt" onChange="HidePageMessage()" onkeydown='onAutoSubmit(event)'/>
																			</td>
																		</tr>
																		<tr>																			
																			<td align="right">
																				<!--font class="loginfnt">Registered Carrier :</font-->
																			</td>
																			<td align="right">
																			<div style="display: none;">
																				<select id="carrierCode" name="carrierCode" STYLE="width: 120px">
																					<c:out value='${requestScope.reqCarrierOptions}' escapeXml='false' />
																				</select>
																			</div>
																			</td>
																																						
																		</tr>	
																		<tr>
																			<td align="right" colspan="2">
																				<input type="button" id="btnClose" value="Close" class="loginButton" onclick="window.close()" i18n_key="close">
																				<input type="button" id="btnLogin" value="Login" class="loginButton" onclick="login()" i18n_key="login">
																			</td>
																		</tr>
																		<tr>
																			<td align="right">
																			<font class="loginfnt" i18n_key="language">Language :</font>
																			</td>
																			<td align="right">
																				<select id='prefLangunage' name='prefLangunage'  STYLE="width: 120px" >
																				
																				</select>
																			</td>
																		</tr>
																	</table>
																	
																	<input type="hidden" id="hdnUserStatus" name="hdnUserStatus" value="<c:out value="${sessionScope.isInactiveUserLogin}" escapeXml="false" />" />
																</form>
															</td>
															<td><img src='../images/M040_no_cache.jpg'></td>
														</tr>
														<tr>
															<td colspan='3'>
																<font style="color:#E4E4E4">&nbsp;Powered by <b>ISA</b></font>
															</td>
														</tr>
													</table>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><img src="../images/bottom6_no_cache.jpg"></td>
					</tr>
					<tr>
						<td>	
							<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBottomColor" style="height:100%;" ID="Table5">
								<tr>
									<td width="30" class="PageBottomShadow"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
									<td width="914" align="left" height="34" valign="middle">
										<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table6">
											<tr>
												<td width="20" valign="bottom"><a href="javascript:void(0);" onclick="ShowPageMessage()" title="View Message(s)"><img id="imgError" name="imgError" src="../images/spacer_no_cache.gif" border="0"></a></td>
												<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
												<td class="StatsBarDef" id="tdSTBar"><span id="spnDate">&nbsp;</span></td>
												<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
												<td width="265" align="right">
													<!--font class="fontPGInfo">This site best view in IE 5+ and 1024 x 768 resolution.</font-->
												</td>
											</tr>
										</table>
									</td>
									<td width="30" class="PageBottomShadow"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>	
		</table>
		<div id="errorMSGouter" style="display: none;">
			<div class="errorBox">
				<span id="errorMSG" style=""></span>
				<span class="errorClose">&nbsp;</span>
			</div>
		</div>
		<div id="errorPopup" style="display: none;">
			<div class="errorPopupHeader">
				<span class="text">Error Console</span>
				<span class="errorClose">&nbsp;</span>
				<div style="clear:both"></div>
			</div>
			<div class="errorPopupButton" style="">
				<a href="javascript:void(0)" class="errorClear">Clear All</a>
			</div>
				<div class="errorPopupbody" style="">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<div>
									<table id="errorPopupTable" border="0" width="100%" cellspacing="0" cellpadding="4">
									
									</table>
								</div>
							</td>
						</tr>
					</table>
				</div>
		</div>
	<%@ include file="../common/includeBaloon.jsp"%>
	<script src="../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<div id="accelAeroXBETrack" style="display: none">
	</div>
	<script language="javascript">
	<!--
	var arrError		= new Array();
	var carrier = "<c:out value='${requestScope.reqCarrName}' escapeXml='false' />"; 
	document.title = ": : "+carrier;
	var logStatus='<%=request.getParameter("hdnMode")%>';
	var serverErrorMsg = "<c:out value='${requestScope.reqServerErrors}' escapeXml='false' />"; 
	  	
	<c:out value='${requestScope.reqSecureLoginUrl}' escapeXml='false' />  	
	<c:out value='${requestScope.reqClientErrors}' escapeXml='false' />
	var autologin = <c:out value='${requestScope.autologin}' escapeXml='false' default ='false'/>;
	
	var uname = "<c:out value='${requestScope.uname}' escapeXml='false' />";
	var password = "<c:out value='${requestScope.password}' escapeXml='false' />";	
	var remoteLoginCarrier = "<c:out value='${requestScope.carrier}' escapeXml='false' />";
	var defaultAirlineId = "<c:out value='${requestScope.defaultAirlineId}' escapeXml='false' />";
	var isRequestNICForReservationsHavingDomesticSegments = "<c:out value='${requestScope.reqIsRequestNICForReservationsHavingDomesticSegments}' escapeXml='false' />";
	//code merged to fixed a issue related to this
	var isTrackingEnabled = <c:out value='${requestScope.reqXBETrackingEnabled}' escapeXml='false' />;
	
	if(!reOpen){
		document.body.style.visibility="visible";
		onLoad(logStatus);
	}	
	if (url.indexOf('agents')==-1){
		setImage("imgSysName", "../images/M047_no_cache.jpg");
	}else{
		setImage("imgSysName", "../images/M048_no_cache.jpg");		
	}
	setBallonLocation("408", "500");
	
	if(autologin){
		setField("username_txt",uname);
		setField("j_password",password);
		setField("carrierCode",remoteLoginCarrier);
		login();
	}
	
	function reloadLoginAction(){
		return true;
	}
	
	function refreshCaptcha() {
	    $("#txtCaptcha").val("");
	    document['imgCPT'].src = '../jcaptcha_new?relversion=' + (new Date()).getTime();
	}
	
	function getXbeUserLanguages(){
		
		var data = {};
		$.ajax({
			type : "POST",
			url : "../public/xbeUserLanguages.action",
			dataType : "json",
			contentType : "application/json; charset=utf-8",
			data : data,
			async : false,
			success: function (response) {
				
				 var options = '';
								
				$.each(response.suppotedLanguages, function(key, value){
				    
				    options += '<option value="'+ key +'">'+ value +'</option>';
				});
				
				$("#prefLangunage").html(options);
 
			}
		});
		
	}
	
	function showCaptcha(){
		
		var data = {};
		$.ajax({
			type : "POST",
			url : "../public/showUserCaptcha.action",
			dataType : "json",
			data : data,
			async : false,
			success: function (response) {
				
				if (response.captchaEnabled){
					$("tr.captcha").show();
				}
 
			}
		});
		
	}
	
	$(document).ready(function() {
		getXbeUserLanguages();
		showCaptcha();
		//code merged to fixed a issue related to this
		if (isTrackingEnabled && window.location.pathname.indexOf("public") > -1){
			var trackerVar = "showTrackerPublic.action";
			//$("#frmTracking").attr("src", "showTracker");
			var trakIframe = $("<iframe></iframe>").attr({
				"id":"frmTracking",
				"name":"frmTracking",
				"src":trackerVar,
				"frameborder":0
			});
			trakIframe.css({
				"height":1,
				"width":1,
				"display":"none"
			});
			loadFrame = function(){
				$("#accelAeroXBETrack").append(trakIframe);
			};
			setTimeout(loadFrame,1000);
		}
	});
	

		$("#prefLangunage").change(function() {
			var lang = "";
			lang = $("#prefLangunage").val();
			if (lang) {
				loadLanguage(lang);
			}

		});
		

			
	//-->
	</script>
</body>
</html>
