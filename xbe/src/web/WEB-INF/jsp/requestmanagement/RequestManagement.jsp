<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Expires" content="-1"/>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>
	<script  src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
	<script  src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script  src="../js/customer/customerValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
	
	<style>
		#spnError div.errorBoxHttps{
			padding: 2px 9px 0px 8px;
			background: #F3F376;
			-moz-border-radius:4px;
		}
	</style>

  </head>
   <body style="background: rgb(255,255,255);" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)"> 
  <!--<body style="background: transparent;"> -->
   <div style="overflow: auto; height:650px;width: 970px;margin: 0 auto" id="requestManagementContainer">
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td> &nbsp; </td></tr>
				<tr><td> &nbsp; </td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Queues						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="8%"><font>Queue </font></td>
								<td width="25%">
									<select style="width:180px;font-size:12px" id="searchQueueType" name="requestManagement.searchQueueType" size="1" class="editable aa-input">
										<option value=""></option>
										<c:out value="${requestScope.reqQueues}" escapeXml="false" />
									</select>
								</td>
								<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
									<td width="90px"><font>Applicable Page</font></td>
									<td  width="25%">
										<select style="width:180px;font-size:12px" id="applicablePage" name="requestManagement.applicablePage" size="1" class="editable aa-input">
											<option value=""></option>
											<c:out value="${requestScope.reqQueuePages}" escapeXml="false" />
										</select>
									</td>
								</u:hasPrivilege>

									<td width="5%"> <font>Status</font> </td>
									<td>
										
										<select style="width:113px;font-size:12px" id="requestStatus" name="requestManagement.requestStatus" size="1" class="editable aa-input">
											<option value=""></option>
											<c:out value="${requestScope.requestStatus}" escapeXml="false" />
										</select>
										
									</td>
							        <td align="left" colspan="3">
									  	<input name="requestManagement.btnSearch" type="button" class="Button" id="buttonSearch" value="Search" /> 
									</td>

							</tr>
							
							<tr>
								
								<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
									<td width="90px"><font>PNR</font></td>
									<td  width="25%">
										<input name="requestManagement.pnrSearch" type="text" id="pnrSearch" size="15" maxlength="25" class="editable aa-input"> </input>
									</td>
									
									<td width="90px"><font>From</font></td>
									<td>
										<input  name="requestManagement.fromDate" type="text" id="txtFromDate" size="20" maxlength="60" class="editable aa-input"/>
									</td>
									
									<td width="90px"><font>To</font></td>
									<td>
										<input  name="requestManagement.toDate" type="text" id="txtToDate" size="20" maxlength="60" class="editable aa-input"/>
									</td>
								</u:hasPrivilege>
							
							</tr>
							
							<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
								<tr>
									<td colspan="4">
										<input type="checkbox" name="lastTenRequests" id="lastTenRequests"/> Last 10 Requests 
									</td>
								</tr>
							</u:hasPrivilege>
							
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Queues<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4">							
							<tr>
								<td id="requestsGridContainer">
								
								</td>
							</tr>
							<tr>
								<td>
									<input type="button"  name="requestManagement.viewHistoryButton" id="btnViewHistory" class="Button" value="View History" style="width:110px"/>
									<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
										<input type="button"  name="requestManagement.createRequestButton" id="btnCreateRequest" class="Button" value="Create Request" style="width:110px"/>
										<input type="button"  name="requestManagement.editButton" id="btnEdit" class="Button" value="Edit"/>
										<input type="button"  name="requestManagement.deleteButton" id="btnDelete" class="Button" value="Delete"/>
										<input type="button"  name="requestManagement.reopenButton" id="btnReopen" class="Button" value="Reopen"/>
									</u:hasPrivilege>
									
									<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.action">
										<input name="requestManagement.toOpen" type="button" class="Button" id="btnToOpen" value="Set To Open" style="width:110px"/>
										<input type="button"  name="requestManagement.inProgress" id="btnSetInProgress" class="Button" value="Set to In Progress" style="width:130px"/>
										<input type="button"  name="requestManagement.closeRequest" id="btnCloseRequest" class="Button" value="Close Request" style="width:110px"/>
										<input type="button"  name="requestManagement.reject" id="btnReject" class="Button" value="Reject Request" style="width:110px"/>
									</u:hasPrivilege>
									
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				 
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>
					Add/Edit Requests &nbsp;&nbsp;<%@ include file="../common/IncludeFormHD.jsp"%>		
						<table width="100%" border="0" cellpadding="0" cellspacing="4">
						
							<tr>
								<td width="80px"><font>Queue </font></td>
								<td width="350px">
									<select style="width:150px;font-size:12px" id="addEditQueueType" name="requestManagement.addEditQueueType" size="1" class="editable aa-input">
										<option value=""></option>
										<c:out value="${requestScope.reqQueues}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>

									<lable style="padding:20px;font-family:tahoma">PNR</lable>
	  								<input name="requestManagement.pnr" type="text" id="pnr" size="15" maxlength="25" class="editable aa-input"> </input>

								</td>
								
								<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.action">
									<td>
										<lable style="padding:5px;font-family:tahoma">Created Date : </lable>
		  								<lable id="createdDateLbl"></lable>
									</td>

								</u:hasPrivilege>
								
							</tr>
							
							<tr><td> &nbsp; </td></tr>
							
							<tr>
							
								<td><font>Description </font></td>
								<td> 
									<textarea maxlength="1000" rows="6" cols="60" id="description" name="requestManagement.description" class="editable aa-input"></textarea>	
									<font class="mandatory">&nbsp;*</font>
								</td>
							
							</tr>						
						</table> 
			          		<div id="remarkPopup" style="display: none;min-height: 260px">
			          			<table>
			          				<tr>
				          				<td> <font id="remarkLbl">Remark</font> </td>
										<td>
											<textarea maxlength="1000" rows="6" cols="60" id="remark" name="requestManagement.remark" ></textarea>
											<font class="mandatory" id="remarkMandatoryLbl">&nbsp;*</font>	
										</td>
									</tr>
									<tr>
										<td align="right" colspan="2">
											<input name="requestManagement.ReopenSubmit" type="button" class="Button" id="btnSubmit" value="Submit"/>
										</td>
									</tr>
								</table>
			          		</div>
				           <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				           <tr><td style='height: 4px'></td></tr>						
							<tr>
								<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
									<td width="40%" >&nbsp;</td>
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.action">
									<td width="80%" >&nbsp;</td>
								</u:hasPrivilege>
			                    <td style="height:15px;" valign="bottom">
									<u:hasPrivilege privilegeId="xbe.tool.requestmanagement.submit">
										<input name="requestManagement.btnSave" type="button" class="Button" id="btnSave" value="Submit"/>
									</u:hasPrivilege>
									<input name="requestManagement.btnClose"type="button" id="btnClose" class="Button" value="Close"/>				                    
				              				
				                </td>
				                
			                  </tr>
			                  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>		

			           </table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>  
			    </td>
		    </tr>			
			</table>		

	<script src="../js/requestmanagement/requestmanagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/requestmanagement/requesthistory.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<form method='post' action="showNewFile!loadRes.action" id="frmRes">
		<input type='hidden' id='pnrNO' name='pnrNO'/>
		<input type='hidden' id='groupPNRNO' name='groupPNRNO'/>
		<input type='hidden' id='marketingAirlineCode' name='marketingAirlineCode'/>
		<input type='hidden' id='interlineAgreementId' name='interlineAgreementId'/>
		<input type='hidden' id='airlineCode' name='airlineCode'/>
	</form>
</div>
</body>
</html>
