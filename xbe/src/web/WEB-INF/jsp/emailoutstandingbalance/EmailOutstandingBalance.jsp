<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;	
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date	
	
	String CurrentDate =  formatter.format(dStartDate);
	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Invoice Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
<script type="text/javascript">
		var arrError = new Array();	
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		<c:out value="${requestScope.reqinvoiceGenDetails}" escapeXml="false" />
		<c:out value="${requestScope.reqTotalInvoiceGenDetails}" escapeXml="false" />
		var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
		var isWeeklyInvoiceEnabled = "<c:out value="${requestScope.isWeeklyInvoiceEnabled}" escapeXml="false" />";
		// var totString = "<c:out value="${requestScope.totalString}" escapeXml="false" />";
		
</script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  	<%@ include file="../common/IncludeTop.jsp"%>
  	<form name="frmInvoiceEmail" id="frmInvoiceEmail" action="showEmailOutstandingBalance.action" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">								
			<tr>
				<td>
					<%@ include file="../common/IncludeMandatoryText.jsp"%>
				</td>
			</tr>	
			
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>Search Invoice<%@ include file="../common/IncludeFormHD.jsp"%>
				  	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">							
						<tr>
							<td colspan="5">
								<table width="80%" border="0" cellpadding="0" cellspacing="2">	
									<tr>
										<td width="11%" align="left"><font>From Year</td>
										<td><select name="selYear1" id="selYear1" size="1" style="width:100px" onChange= "validatePeriod()" maxlength="4" title="From Year">
											<OPTION value="" selected></OPTION>	
												<c:out value="${requestScope.reqBasePrevious}" escapeXml="false" />
											</select><font class="mandatory"><b>*</b></font>
										</td>
										<td><font>Month </font></td>
										<td align="left"><select name="selMonth1" id="selMonth1" size="1" style="width:100px" onChange="validatePeriod()" maxlength="9" title="From Month">
											<!-- <OPTION value=""></OPTION>	 -->
											<OPTION value=01>January</OPTION>
											<OPTION value=02>February</OPTION> 
                           					<OPTION value=03>March</OPTION> 
                           					<OPTION value=04>April</OPTION> 
                           					<OPTION value=05>May</OPTION> 
                           					<OPTION value=06>June</OPTION> 
                           					<OPTION value=07>July</OPTION>
                           					<OPTION value=08>August</OPTION> 
                           					<OPTION value=09>September</OPTION> 
                           					<OPTION value=10>October</OPTION> 
                           					<OPTION value=11>November</OPTION> 
                           					<OPTION value=12>December</OPTION>
											</select><font class="mandatory"><b>*</b></font>
										</td>
										<td></td>
										<td>
											<table width="100%" border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td><font>Invoice Period</font></td>
													<td>
														<select name="selBPStart" id="selBPStart" size="1" style="width:50px" onChange="validatePeriod()">
															<c:out value="${requestScope.reqInvPeriods}" escapeXml="false" />
														</select>
													</td>
												</tr>	
											</table>
										</td>
										<td>
											<input type="checkBox" name="chkZero1" id="chkZero1" value="ZERO" class="noBorder">
											<font>Exclude Zero Invoices</font>
										</td>											
									</tr>										
								</table>
							</td>
						</tr>
						
						<tr>
							<td colspan="5">
								<table width="80%" border="0" cellpadding="0" cellspacing="2">	
									<tr>
										<td width="11%" align="left"><font>To Year</td>
										<td><select name="selYear2" id="selYear2" size="1" style="width:100px" onChange= "validatePeriod()" maxlength="4" title="To Year">
											<OPTION value="" selected></OPTION>	
												<c:out value="${requestScope.reqBasePrevious}" escapeXml="false" />
											</select><font class="mandatory"><b>*</b></font>
										</td>
										<td><font>Month </font></td>
										<td align="left"><select name="selMonth2" id="selMonth2" size="1" style="width:100px" onChange="validatePeriod()" maxlength="9" title="To Month">
											<!-- <OPTION value=""></OPTION>	 -->
											<OPTION value=01>January</OPTION>
											<OPTION value=02>February</OPTION> 
                           					<OPTION value=03>March</OPTION> 
                           					<OPTION value=04>April</OPTION> 
                           					<OPTION value=05>May</OPTION> 
                           					<OPTION value=06>June</OPTION> 
                           					<OPTION value=07>July</OPTION>
                           					<OPTION value=08>August</OPTION> 
                           					<OPTION value=09>September</OPTION> 
                           					<OPTION value=10>October</OPTION> 
                           					<OPTION value=11>November</OPTION> 
                           					<OPTION value=12>December</OPTION>
											</select><font class="mandatory"><b>*</b></font>
										</td>
										<td></td>
										<td>
											<table width="100%" border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td><font>Invoice Period</font></td>
													<td>
														<select name="selBPEnd" id="selBPEnd" size="1" style="width:50px" onChange="validatePeriod()">
															<c:out value="${requestScope.reqInvPeriods}" escapeXml="false" />
														</select>
													</td>
												</tr>	
											</table>
										</td>
										<td>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											
											<!-- input type="checkBox" name="chkZero2" id="chkZero2" value="ZERO" class="noBorder">
											<font>Exclude Zero Invoices</font-->
										</td>											
									</tr>										
								</table>
							</td>
						</tr>
						
							
						<tr>
							<td width="9%"><font>Agencies</font></td>
							<td width="15%">
								<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:100px">
									<option value=""></option>
									<option value="All">All</option>
									<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
								</select>
								<font class="mandatory"><b>*</b></font>
							</td>
							<td width="18%"><input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font></td>
							<td width="18%"><input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font></td>
							<td><input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"></td>
						</tr>	
												
						<tr>
							<td>
								<font class="fntBold">Agents</font>
							</td>
						</tr>
						
						<tr>
							<td valign="top" colspan="5"><span id="spn1"></span>								
						</tr>
						
						<tr>
							<td colspan="4" align="right">
								<input name="btnSearch" type="button" class="Button" id="btnSearch" value="View" onclick="searchClick()">
							</td>
						</tr>						
					</table>
					
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>	
									
			<tr>	
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>Invoice Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  	<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
						<tr>
							<td colspan="5">									
								<span id="spnInvoices"></span>
								<span id="spnTotal"></span>									
							</td>
						</tr>
						
					    <tr>
							<td>
								<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
							</td>
							<td colspan="3" align="right">
								
								
								<input type="checkBox" name="chkPage" id="chkPage" value="Page" class="noBorder" onClick="clickPage()">
								<font>Agents in Page</font>									
								<input type="checkBox" name="chkAll" id="chkAll" value="ALL" class="noBorder" onClick="clickAll()">
								<font>For All Agents</font>	
																
								<input name="btnDetails" type="button" class="Button" id="btnDetails" value="Details" onClick="viewReport()">
											
								<input type="checkBox" name="chkAllAgent" id="chkAllAgent" value="All" class="noBorder" onclick="clickAllAgent()">
								<font>All Agent Report</font>	
									
								<input type="checkBox" name="chkIndiAgent" id="chkIndiAgent" value="INDIVIDUAL" class="noBorder" onclick="clickSingleAgent()">
								<font>Individual Agent</font>
									
								<input name="btnEmail" type="button" class="Button" id="btnEmail" value="Email" onClick="sendClick()" title="Email to Agent ID" >
									
								
								<input type="text" name="txtEmail" id="txtEmail" onclick="">
								
								<input name="btnSend" type="button" class="Button" id="btnSend" value="Send" onClick="sendEmailToGivenEmail()" title="Enter Email ID" >						
								
							</td>
							
						</tr>					
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			
			</table>
			
			<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />			
			</script>		
			
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnInvoice" id="hdnInvoice" value="">
		<input type="hidden" name="hdnSelectedAgents" id="hdnSelectedAgents" value="">
		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
		<input type="hidden" name="hdnFromDate" id="hdnFromDate">
		<input type="hidden" name="hdnToDate" id="hdnToDate">
		<input type="hidden" name="hdnBillingEmail" id="hdnBillingEmail">
		
	
			
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
  
 <script src="../js/emailoutstandingbalance/EmailOutstandingBalanceValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 <script src="../js/emailoutstandingbalance/EmailOutstandingBalanceGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      HideProgress();
   }   

   
   //-->
  </script>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>
</html>
