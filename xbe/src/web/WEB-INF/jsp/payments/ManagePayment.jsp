 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Tabs.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/payments/validateManagePayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return showContextMenu()" ondrag="return false" onkeydown="return Body_onKeyDown(event)" onbeforeunload="beforeUnload()" 
  scroll="no" style="background: transparent;">
	<%@ include file="../common/IncludeTabTop.jsp"%><!-- Page Background Top page -->
	<form action="showManagePayment.action" method="post" name="frmManagePayment" id="frmManagePayment">

		 <script type="text/javascript">
		 	var isSaveTransactionSuccessful = false;
			var isSuccessfulSaveMessageDisplayEnabled = false;
			var arrError = new Array();
			var strCurrency = "";			
			<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
			
			var arrInvoice = new Array();
			<c:out value="${requestScope.reqInvoiceArray}" escapeXml="false" />	

			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			
			<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			<c:out value="${requestScope.reqsearchcurr}" escapeXml="false" />
			<c:out value="${requestScope.hasPriviGSA}" escapeXml="false" />
			<c:out value="${requestScope.searchGSA}" escapeXml="false" />
			<c:out value="${requestScope.gNameNCode}" escapeXml="false" />
			<c:out value="${requestScope.hasPriviForViewPayOnly}" escapeXml="false" />
			
			<c:out value="${requestScope.reqGSAStructureV2Enabled}" escapeXml="false" />
			
			var defaultAirlineCode = "<c:out value="${requestScope.reqDefaultAirlineCode}" escapeXml="false" />"; 
			var parnagentCode ="<c:out value="${requestScope.reqAgentCode}" escapeXml="false" />";
			var parnagentName ="<c:out value="${requestScope.reqAgentName}" escapeXml="false" />";
			var strBaseCurrency = "<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
		</script>		
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>				
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Select Agent<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="10%">
									<font>Agent Type</font>
								</td>
								<td width="20%">									
									<div id="GSAV2">
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td width="50%">
													<select id="selAgentType" name="selAgentType" size="1" onChange="selAgentType_onChange(true)" tabindex="12" style="margin-bottom: 1px">
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />
													</select>
												</td>
											</tr>
										</table>
									</div>
								</td>
								<td width="8%">	<font> Name </font></td>
								<td width="6%">
										<input id="optGSA" name="optAgent" type="radio" class="NoBorder" value="GSA" onClick="chkClick1()" tabIndex="2">
										<font id="gsaText" class="fntBold"> GSA </font>
							    </td>
								<td width="4%">	<font> Name </font></td>
								<td width="15%">
									<input type="text" name="txtAgentName" id="txtAgentName" maxlength="30" size="20" value="" tabIndex="5">
								</td>
                                <td width="1%"></td>
								<td width= "13%">
									<font>Agent Code </font><span id="spnAirlineCode" style="float:right;display:none"></span>
								</td>
								<td width="12%">
									<input type="text" name="txtAgentCode" id="txtAgentCode" maxlength="12" size="15" tabIndex="6">
								</td>
                                <td width="1%"></td>
								<td width="10%">
									<font style="float:right">Agent IATA Code</font><span id = "spnIataCode"  style="float:right;display:none"></span>
								</td>
								<td width="12%">
									<input type="text" name="txtAgentIataCode" id="txtAgentIataCode" maxLength="11%" size="15" tabindex="7">
								</td>								
								<td align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" value="Search" onclick="searchClick()" tabIndex="8">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>								
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Payment Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">					  			
							<tr>
								<td>									
									<span id="spnPayments"></span>
								</td>
							</tr>
							<tr>
								<td align="right">
								  <u:hasPrivilege privilegeId="ta.pay.view.payment,ta.pay.settle.payment">
										<input name="btnAdjustPay"  type="button" class="Button" id="btnAdjustPay" value="View/Adjust Payment" style="width:140px" onClick="adjustPayClick()" tabindex="11">
								  </u:hasPrivilege>
								</td>
							</tr>							
							</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<iframe src="showInvoicesettlement.action" id="frmTabs" name="frmTabs" style="height:270px" width="100%" frameborder="0" scrolling="no"></iframe>
					</td>
				</tr>				
			</table>
		<script src="../js/payments/ManagePayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
		<input type="hidden" name="hdnAction" id="hdnAction" value="1">
		<input type="hidden" name="hdnMode" id="hdnMode" value="">
		<input type="hidden" name="AgentCode" id="AgentCode" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnData" id="hdnData" value=""> <!-- To hold data from invoice settlement tab-->
		<input type="hidden" name="hdnAgentCode" id="hdnAgentCode" value="">
		<input type="hidden" name="CreditLimit" id="CreditLimit" value=""> <!-- To hold data from post payment tab-->
		<input type="hidden" name="AvailableCredit" id="AvailableCredit" value="">
		<input type="hidden" name="hdntxtDStart" id="hdntxtDStart" value="">
		<input type="hidden" name="hdntxtAmount" id="hdntxtAmount" value="">
		<input type="hidden" name="hdnselColP" id="hdnselColP" value="">
		<input type="hidden" name="hdnselColT" id="hdnselColT" value="">
		<input type="hidden" name="hdntxtChqNo" id="hdntxtChqNo" value="">
		<input type="hidden" name="hdntxtRemarks" id="hdntxtRemarks" value="">
		<input type="hidden" name="hdnSearchCriteria" id="hdnSearchCriteria" value="">
		<input type="hidden" name="hdnTransferType" id="hdnTransferType" value="">
		<input type="hidden" name="hdnTransferAmount" id="hdnTransferAmount" value="">
		
		<input type="hidden" name="hdnAgentID" id="hdnAgentID" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnCreditLimit" id="hdnCreditLimit" value="">
		<input type="hidden" name="hdnFromPage" id="hdnFromPage" value="">
		<input type="hidden" name="hdnAgCode" id="hdnAgCode" value="">	
		<input type="hidden" name="hdnAgentIataCode" id="hdnAgentIataCode">	
		
	</form>
	<%@ include file="../common/IncludeTabBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
  <script src="../js/payments/ManagePaymentGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
  	
	winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')
  </script>
 <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			HideProgress();
	  		}	  	
	  	}
  	}
  	 
  	//-->
  </script>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>
</html>