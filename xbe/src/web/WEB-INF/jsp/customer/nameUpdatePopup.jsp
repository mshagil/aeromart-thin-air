	<div id='divCustNameUpdateConfirm' style='position:absolute;top:300px;left:350px;width:350px;z-index:1001; background:transparent; display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold'> &nbsp;Profile Name Update Confirm</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>	
						<tr >
							<td align="center" class='popBGColor'>
							<table width='95%' border='0' cellpadding='1' cellspacing='0'>
							<tr>
								<td class='popBGColor' align="left"><label id='lblCustNameUpdate'> You have changed the customer name.  </label></td>
							</tr>							
							<tr>
								<td class='popBGColor txtBold' align="left"><label id='lblCustNameUpdateCNF'> This will result in the removal of the customer AiRewards ID from all the unflown reservations the Customer will be flying.</label></td>
							</tr>					
							<tr >
								<td class='popBGColor ' align="left">Would you like to continue ?</td>
							</tr>	
							<tr>
								<td class='popBGColor singleGap'></td>
							</tr>						
							<tr>
								<td class='popBGColor rowHeight' align='right'>
									<button id="btnNameUpdateOk" type="button"  class="btnMargin">Yes </button>
									<button id="btnNameUpdateNo" type="button"   class="btnMargin">No </button>
								</td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>