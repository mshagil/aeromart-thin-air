<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script type="text/javascript" src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script language="javascript" type="text/javascript">
	<!-- 
		var errorLog = [];
		
		var pageEdited 		= false;
		var strLastPage 	= "";
		var objCWindow ;
		var strNOData		= "No Data Found";
		var strBabyTitle	= "Baby.";
		var aMessage		= null;
		var arrPrivi		= new Array();
		var airportOwnerList 	= new Array();
		var arrAirports 	= new Array();
		var arrAirportsV2 	= new Array();
		var allOnds;
		var arrSearchOptions= new Array();
		var arrReservationSearchOptions	= new Array();
		var arrCurrency 	= new Array();
		var arrTitle 		= new Array();
		var arrTitleChild 	= new Array();
		var arrInfantTitle 	= new Array();
		var arrCountry 		= new Array();
		var arrNationality 	= new Array();
		var arrCType 		= new Array();
		var arrError		= new Array();
		var arrTravelAgents	= new Array();
		var arrTravelAgentsWithDry 	= new Array(); // ['Agent namr - Carrier' , 'carrier-agentCode', 'agent currency code']
		var arrIPGs	= new Array();
		var arrOfflineIPGs	= new Array();		
		var arrOfflineIPGsModifyEnabled	= new Array();
		var arrAllTravelAgents	= new Array();
		var arrAllTransferTravelAgents	= new Array();
		var arrOwnTravelAgents = new Array();
		var arrCos			= new Array();
		var arrLogicalCC    = new Array();
		var arrYear			= new Array();
		var arrSSR			= new Array();
		var arrPaxTitle		= new Array();
		var arrBookingClassCode  = new Array();
		var strAgentCode 	= "";
		var strAgentHandlingCharge = 0;
		var tagsNationality = "";
		var tagsTitle 		= "";
		var strPageFrom		= "";
		var strPageReturn	= "";
		var strSearchCriteria	= "";
		var proceedOK		= false;
		var intTimerMiliSeconds = 2400000
		var strInfantDisplay = "";
		var blnProcessing=false;
		var childBookingStatus="";
		var arrCusType=new Array();
		var arrFareCat=new Array();
		var defaultPaxTypes = new Array();
		var pageProcessing = false;
		var arrFareCatAll = new Array();		
		var arrCountryPhone = new Array();
		var arrAreaPhone  = new Array(); 
		var enableSeatMap = false;
		var userTheme = "";
		var userPWDReseted = "N";
		 
		var enableInsurance = false;
		var enableAncillary = false;
		var arrRetValidity = new Array();
		var arrBCType = new Array();
		
		var enableMeals = false;
		var enableAirportServices = false;
		var insuranceOHD	= false;
		var blnVersion2 = false;
		
		var arrBookingCategories = new Array();
		var arrAllBookingCategoriesList = new Array();
		var arrItineraryFareMaskOptions = new Array();
		var arrAllCouponStatus = new Array();
		var arrAllPaxStatus = new Array();
		var countryStateArry = {};
		var gstMessageEnabledAirports = new Array();
		var hubCountryArry = new Array();
		
		var arrCabinClassRanking = <c:out value='${requestScope.reqCabinClassRankingMap}' escapeXml='false' />;		
		
		<c:out value='${requestScope.reqRequestData}' escapeXml='false' />
		<c:out value='${requestScope.reqPrivileges}' escapeXml='false' />
		<c:out value='${requestScope.airportOwnerList}' escapeXml='false' />
		<c:out value='${requestScope.airportList}' escapeXml='false' />
		<c:out value='${requestScope.airportListV2}' escapeXml='false' />
		<c:out value='${requestScope.agentWiseOndMap}' escapeXml='false' />
		<c:out value='${requestScope.currencyList}' escapeXml='false' />
		<c:out value='${requestScope.titleList}' escapeXml='false' />
		<c:out value='${requestScope.defaultPaxTypeList}' escapeXml='false' />
		<c:out value='${requestScope.intantTitleList}' escapeXml='false' />
		<c:out value='${requestScope.coutnryList}' escapeXml='false' />
		<c:out value='${requestScope.nationalityList}' escapeXml='false' />
		<c:out value='${requestScope.cardType}' escapeXml='false' />
		<c:out value='${requestScope.reqClientErrors}' escapeXml='false' />
		<c:out value='${requestScope.reqSalesPointList}' escapeXml='false' />
		<c:out value='${requestScope.reqSalesPointWithDryList}' escapeXml='false' />
		<c:out value='${requestScope.reqAllTravelAgentsHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqAllTransferTravelAgentsHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqOwnTravelAgentsHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqAllIpgsHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqAllOfflineIpgsHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqModificationEnabledOfflineIpgsHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqItnLangList}' escapeXml='false' />
		<c:out value="${requestScope.reqLogicalCCEnabled}" escapeXml="false" />
		<c:out value='${requestScope.sesHtmlCosData}' escapeXml='false' />
		<c:out value='${requestScope.reqHtmlCosWithLogicalData}' escapeXml='false' />
		<c:out value='${requestScope.sesHtmlYearData}' escapeXml='false' />
		<c:out value='${requestScope.ssrCodes}' escapeXml='false' />
		<c:out value='${requestScope.titleVisibleList}' escapeXml='false' />
		<c:out value='${requestScope.standByList}' escapeXml='false' />
		<c:out value='${requestScope.reqCusTypeHtml}' escapeXml='false' />
		<c:out value='${requestScope.reqFareCatList}' escapeXml='false' />
		<c:out value='${requestScope.titleListWithChild}' escapeXml='false' />
		<c:out value='${requestScope.reqFareCatListAll}' escapeXml='false' />
		<c:out value='${requestScope.countryPhoneList}' escapeXml='false' />
		<c:out value='${requestScope.reqRetValidity}' escapeXml='false' />
		<c:out value='${requestScope.reqBookingClassCodes}' escapeXml='false' />
		<c:out value='${requestScope.reqBCType}' escapeXml='false' />
		<c:out value='${requestScope.arrSearchOptions}' escapeXml='false' />
		<c:out value='${requestScope.arrReservationSearchOptions}' escapeXml='false' />		
		<c:out value='${requestScope.reqBookingCategoryList}' escapeXml='false' />
		<c:out value='${requestScope.reqHubCountries}' escapeXml='false' />
		<c:out value='${requestScope.reqAllBookingCategoryList}' escapeXml='false' />
		<c:out value='${requestScope.reqItineraryFareMaskOptions}' escapeXml='false' />
		<c:out value='${requestScope.reqAutoSeatAssignmentEnabled}' escapeXml='false' />
		<c:out value='${requestScope.reqAutoSeatAssignmentStartingRow}' escapeXml='false' />
		<c:out value='${requestScope.reqAgentFareMaskStatus}' escapeXml='false' />
		
		<c:out value='${requestScope.reqAllCouponStatusList}' escapeXml='false' />
		<c:out value='${requestScope.reqAllPaxStatusList}' escapeXml='false' />

		<c:out value='${requestScope.reqCountryStateMap}' escapeXml='false' />
		<c:out value='${requestScope.gstMessageEnabledAirports}' escapeXml='false' />
		var gstMessage = "<c:out value='${requestScope.gstMessage}' escapeXml='false' />";
		
		var screenWidth 	= "<c:out value='${sessionScope.sesScreenWidth}' escapeXml='false' />";
		var strULevel		= true;
		var intAgtBalance	= "<c:out value='${requestScope.sesAgentBalance}' escapeXml='false' />";
		var intRemoteAgtCurrency 	= "<c:out value='${requestScope.hdnRemoteAgentCurr}' escapeXml='false' />";
		var strUID			= "<c:out value='${requestScope.logUID}' escapeXml='false' />";
		var strSPoint		= "<c:out value='${requestScope.salesPoint}' escapeXml='false' />";
		var strTNC 			= "<c:out value='${requestScope.termsConditions}' escapeXml='false' />";
		var strUName		= "<c:out value='${requestScope.agentName}' escapeXml='false' />";
		var strAgentName	= "&nbsp;";
		var carrier = "<c:out value='${requestScope.reqCarrName}' escapeXml='false' />";
		var childVisibility = "<c:out value='${requestScope.reqChildVisibility}' escapeXml='false' />";
		var dupNameOn = "<c:out value='${requestScope.reqDupNameCheck}' escapeXml='false' />";	
		//var dashboardContent = filterDBMEssage( <c:out value='${requestScope.dashboardContent}' escapeXml='false' /> );
		var carrierCode = "<c:out value='${requestScope.reqCarrCode}' escapeXml='false' />";
		userPWDReseted = "<c:out value='${requestScope.userPwdReseted}' escapeXml='false' />";
		var carrierAirlineCodeMap = <c:out value='${requestScope.reqCarrierAirlineCodeMap}' escapeXml='false'/>;

		var showAgentCreditInAgentCurrency = "<c:out value='${requestScope.sesShowAgentBalanceInAgentCurrency}' escapeXml='false' />";
		var agentAvailableCreditInAgentCurrency = "<c:out value='${requestScope.sesAgentBalanceInAgentCurrency}' escapeXml='false' />";

		var gstAppliedForReservation = false;
			
		<c:out value='${requestScope.reqActPayCA}' escapeXml='false' />
		<c:out value='${requestScope.reqActPayOA}' escapeXml='false' />
		<c:out value='${requestScope.reqActPayCC}' escapeXml='false' />
		<c:out value='${requestScope.actPay}' escapeXml='false' />
		var strReportScheduleEnabled = "<c:out value='${requestScope.reqReportSchedularAppParam}' escapeXml='false' />";
		var strFFPNumber = "<c:out value='${requestScope.reqFFPNumber}' escapeXml='false' />";
		
		var selectedLangunage = "<c:out value='${requestScope.reqSelectedLangunage}' escapeXml='false' />";
		
		document.title = ": : "+carrier;
		var arrParams = new Array();
		<c:out value="${requestScope.sysParam}" escapeXml="false" />;
		
		if (strUName != ""){
			var strNameTitle = ""
			if (strUName.length  > 70){
				strNameTitle = " title = '" + strUName + "' ";
				strAgentName = strUName.substr(0,70) + "...";
			}else{
				strAgentName = strUName;
			}
			strAgentName	= "<font class='fontPGInfo' " + strNameTitle + ">&nbsp;Agent Name : </font><font class='fontPGInfo'  " + strNameTitle + "><b>" + strAgentName.toUpperCase() + "<\/b><\/font>";
			if(arrParams[45]){				
				strAgentName	+= "<font class='fontPGInfo' >&nbsp; FFP NO :<\/font><font class='fontPGInfo'  " + strNameTitle + "><b>" + strFFPNumber + "<\/b><\/font>";
			}
		}
		
		var arrParams = new Array();
		<c:out value="${requestScope.sysParam}" escapeXml="false" />;
		strAgentCode = '<c:out value="${requestScope.agentCode}" escapeXml="false" />'
		
		var intMaxAdults	= "";
		var strBaseAirport  = "";
		var strSysDate		= "";
		var strDefDV		= "";
		var strDefRV		= "";
		var strDefCurr		= "";
		var strAdultCode	= "";
		var strChildCode	= "";
		var strInfantCode	= "";
		var strInsuranceChg = "";
		var strDefDept		= "";
		var strDefRetu		= "";
		var strDefInfantAge = "";
		var dtSysDate		= new Date();
		var intDecimals		= "2";
		var strDefCos		= "";
		var strChildTitle   = "";
		var intMaxGroup		= 0;
		var agentCurrency	= '';
		//var basrExRate		= '';
		var agentExchgRate  = "";
		var agentCurrBound  = "";
		var agentCurrBreakPoint = "";
		var agentCurrRuoundEnable ="";
		<c:out value="${requestScope.reqAgCurrDet}" escapeXml="false" />;
		
		var strDefMaxV		= "";
		var strHttpsPath	= "";
		var strStandbyDef  = "NORMAL";

		var strDefAdultAge = "";
		var strDefChildAge = "";
		/*FIXME: Don't use.*/
		var isInfAgeMandXbe = false;
		/*FIXME: Don't use.*/
		var isChAgeMandXbe  = false;
		/*FIXME: Don't use.*/
		var isAdAgeMandXbe  = false;
		var multiCurrPay	= false;
		var restrictCurr	= true;
		var openRT	= false;
		var reservationWindow;
		/**JIRA -AARESAA:2715 (Lalanthi)*/
		var infantAgeLowerBoundaryInDays = "";
		var childAgeLowerBoundaryInMons = "";	
		var adultAgeLowerBoundaryInMons = "";
		var validateEmailDomain = false;		
		
		function setParamValues(arrData){
			strSysDate  	= arrData[0];
			intMaxAdults 	= Number(arrData[1]) ;
			strDefDV 		= Number(arrData[14]);
			strDefRV 		= Number(arrData[14]);
			strBaseAirport 	= arrData[3];
			strDefCurr 		= arrData[4];
			strAdultCode 	= arrData[5];
			strChildCode    = arrData[24];
			strInfantCode 	= arrData[6];
			strInsuranceChg = arrData[7];
			strDefDept 		= arrData[8];
			strDefRetu 		= arrData[9];
			strDefInfantAge = arrData[10];
			intDecimals  	= arrData[11];
			strDefCos  		= arrData[15];
			strChildTitle	= arrData[16];
			strDefMaxV		= Number(arrData[12]);
			intMaxGroup		= Number(arrData[17]);
			dtSysDate		= new Date(strSysDate.substr(6,4), (Number(strSysDate.substr(3,2)) - 1), strSysDate.substr(0,2));	
			agentCurrency	= arrData[18];
			//baseExRate	    = arrData[19];
			intTimerMiliSeconds = arrData[20];
			strInfantDisplay = arrData[21]; 
			childBookingStatus = arrData[22]; 
			strDecimalPoints = arrData[23];
			if(arrData[25] == 'Y'){
				enableSeatMap = true;
			}else {
				enableSeatMap = false;
			}
			enableInsurance = (arrData[34] == 'Y');
			enableMeals = (arrData[36] == 'Y');
			enableAirportServices = (arrData[38] == 'Y');
			enableAncillary = enableSeatMap || enableInsurance || enableMeals;
				 
			strDefChildAge = arrData[26]; 
			strDefAdultAge = arrData[27];
			multiCurrPay	= arrData[31];
			restrictCurr	= arrData[32];
			openRT			= arrData[35];

			infantAgeLowerBoundaryInDays = arrData[39];
			childAgeLowerBoundaryInMons = arrData[40];
			adultAgeLowerBoundaryInMons = arrData[41];
			blnVersion2 = arrData[42];
			validateEmailDomain = arrData[43];
		}
		
		setParamValues(arrParams);

		/*		
		alert(	" arrAirports - " + arrAirports.length + "\n" +
				" arrCurrency - " + arrCurrency.length + "\n" + 
				" arrTitle - " + arrTitle.length + "\n" + 
				" arrInfantTitle - " + arrInfantTitle.length + "\n" + 
				" arrCountry - " + arrCountry.length + "\n" + 
				" arrNationality - " + arrNationality.length + "\n" + 
				" arrAgents - " + arrAgents.length + "\n" +
				" strTNC - " + strTNC + "\n" + 
				" strSPoint - " + strSPoint + "\n" + 
				"<c:out value='${requestScope.menuList}' escapeXml='false' />" + "\n" +
				"System Date - " + strSysDate + "\n" + 
				"System Date - " + dtSysDate + "\n" + 
				"strDefDept Date - " + strDefDept + "\n" + 
				"strDefRetu Date - " + strDefRetu + "\n" + 
				" " );
				*/
				
		var arrMenu 		= new Array();
		<c:out value='${requestScope.menuList}' escapeXml='false' />
		
		// Passenger Foid
		var arrPsntTypeInfo = new Array();
		<c:out value='${requestScope.reqPaxCategoryFOIDList}' escapeXml='false' />
		var arrCountryPhoneJson = <c:out value='${requestScope.countryPhoneJson}' escapeXml='false' />;
		var mapCCTypeForPgw = <c:out value='${requestScope.ccTypeForPgw}' escapeXml='false' />;
		var mapActualPaymentMethodData = <c:out value='${requestScope.reqAcutalPaymentMethodData}' escapeXml='false' />;
		var spMsgCC = "<c:out value='${requestScope.spMsgCC}' escapeXml='false' />";		
		var printIndItinerary = "<c:out value='${requestScope.printIndItinerary}' escapeXml='false' />";
		
		// Mapping code used in the pax foid level
		var defaultPaxCategory = "A";
		var strPsngAdult = "AD";
		var strPsngChild = "CH";
		var strPsngInfant = "IN";
		var strUpdtPsngType = "";	
		
		// Holds pax & contact configurations for different carrier combinations
		var globalPaxContactConfig = [];
		
		// Holds the tempory previous pax configurations
		var tempGlobalPaxContactConfig = [];
				
		function onLoadMenuPage(){
			tagsNationality = "<option value=''><\/option>"
			for (var i = 0 ; i < arrNationality.length ; i++){
				tagsNationality += "<option value='" + arrNationality[i][0] + "'>" + arrNationality[i][1] + "<\/option>";
			}
			
			tagsTitle = "<option value=''><\/option>"
			for (var i = 0 ; i < arrTitle.length ; i++){
				tagsTitle += "<option value='" + arrTitle[i][0] + "'>" + arrTitle[i][1] + "<\/option>";
			}
			
			tagsTitleAll = "<option value=''><\/option>"
			for (var i = 0 ; i < arrTitleChild.length ; i++){
				tagsTitleAll += "<option value='" + arrTitleChild[i][0] + "'>" + arrTitleChild[i][1] + "<\/option>";
			}
			
			/*
			 *  TODO remove the empty and pass the full url for https enableing
			 */
			/*
			if ((window.location.href.indexOf("agents"))  == -1){
				if (request['secureLoginUrl'] != ""){
					if  (request['secureLoginUrl'].indexOf("https") != -1){
						strHttpsPath	= request['secureLoginUrl'] + "/secure/";
					}
				}
			}
			*/
			
			if (request['secureLoginUrl'] != ""){
				if  (request['secureLoginUrl'].indexOf("https") != -1){
					strHttpsPath	= request['secureLoginUrl'] + "/private/";
				}
			}
		}
		
		function doUnload(){
			if ((top.objCWindow)&& (!top.objCWindow.closed)){top.objCWindow.close();}
			if(!proceedOK){
				makePOST("../public/logout.action","");
			}
		}

		function processStateChange(){}
		onLoadMenuPage();

		function cliseChilds(){
			if ((top.objCWindow)&& (!top.objCWindow.closed)){top.objCWindow.close();}
			if ((top.objCW) && (!top.objCW.closed)){top.objCW.close();}
		}
		var db_toolbar = true;
		testAction = function(e){
			var srt = "";
			if (e.currentTarget.id == "currencyCalc"){
				srt = "dv=false&exCur=true&wc=false&bgAvl=false&mlAvl=false";
			}else if (e.currentTarget.id == "dateVal"){
				srt = "dv=true&exCur=false&wc=false&bgAvl=false&mlAvl=false";
			}else if (e.currentTarget.id == "wc"){
				srt = "dv=false&exCur=false&wc=true&bgAvl=false&mlAvl=false";
			}else if (e.currentTarget.id == "mealAvl"){
				srt = "dv=false&exCur=false&wc=false&bgAvl=false&mlAvl=true";
			}else if (e.currentTarget.id == "baggageAvl"){
				srt = "dv=false&exCur=false&wc=false&bgAvl=true&mlAvl=false";
			}
			callUtilityPanel(srt);
		}
		var db_Items_Collection = [	{"title":"Date Validity Checker", "name":"Date Val",id:"dateVal", "imgPath":"../images/datacheck_no_cache.png", "listner":testAction, "status":"act"},
									{"title":"Currency Exchange Calculator", "name":"Currency Calc","id":"currencyCalc", "imgPath":"../images/currencyico_no_cache.png", "listner":testAction, "status":"act"}];
		var db_Clocks = {"dbtoolbar":true, "clocktype":"digital", "useampm":false , "listner":testAction, "zoneclock":[{"tzone":"Asia/Tehran|UTC+03:30|UTC+04:30", "sumapplied":false}]};
		
		
		var imageUrlString = "../themes/default/images/accelaero"
		var db_Anci_Collection = [	{"title":"Meal", "name":"Meal","id":"mealAvl", "imgPath": imageUrlString + "/LCAnsi01_B_no_cache.jpg", "listner":testAction, "status":"act"},
									{"title":"Baggage", "name":"Baggage","id":"baggageAvl", "imgPath": imageUrlString + "/LCAnsibag1_no_cache.jpg", "listner":testAction, "status":"act"}];
		
		var jsonDashboardPref = <c:out value='${requestScope.jsonDashboardPref}' escapeXml='false' />;
		db_Clocks = $.extend(db_Clocks , jsonDashboardPref);
		function updateClocks(d){
			$.extend(db_Clocks , d);
		};
		
		var demoDispay = <c:out value='${requestScope.demoDispay}' escapeXml='false' />;
		var dbToolDispay = <c:out value='${requestScope.dbToolDispay}' escapeXml='false' />;
		var dbAnciDispay = <c:out value='${requestScope.dbAnciDispay}' escapeXml='false' />;
		
		var msgSyncEnable = <c:out value='${requestScope.msgSyncEnable}' escapeXml='false' />;
		var msgSyncTime = <c:out value='${requestScope.msgSyncTime}' escapeXml='false' />;
		var isRequestNICForReservationsHavingDomesticSegments = "<c:out value='${requestScope.reqIsRequestNICForReservationsHavingDomesticSegments}' escapeXml='false' />";
		//code merged to fixed a issue related to this
		var isTrackingEnabled = <c:out value='${requestScope.reqXBETrackingEnabled}' escapeXml='false' />;
		//function filterDBMEssage(messagelist){
		//	var myList = messagelist;
		//	return myList
		//}
		
		var agentTaxRegNo = "<c:out value='${requestScope.reqAgentTaxRegNo}' escapeXml='false' />";
		function pingSessionTimeout(){
		    $.ajax({
		        type: "GET",
		        url: "sessionPing"
		      })
		        .done(function( obj ) {
		        });
		}
		var sessionKeepAliveInMins = parseInt(<c:out value='${requestScope.reqSessionKeepAliveInMins}' escapeXml='false' />, 10);
		if(sessionKeepAliveInMins > 0){
		    setInterval(pingSessionTimeout, sessionKeepAliveInMins * 60 * 1000);
		}
		//-->
		
	
		function getLanguageData(){
			
				var data = {};
				$.ajax( {
					type: "POST",
					url: "../public/makeReservationLanguage.action", 
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					data: data,
					async:false,
					success: i18n_xbe.populateLabels, cache : false
				});


		}
		
		getLanguageData();
	
	</script>
	 	
	<script type="text/javascript" src="../js/common/menu-path.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../js/common/menuG5LoaderFSX.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
  </head>
   <body onUnload="doUnload()" onkeydown='return Body_onKeyDown(event)' style="overflow: hidden;">
	<%--<c:if test='${sessionScope.sesScreenWidth != "800"}'>
	<frameset rows="27, 648,*" border="0" frameborder="0" onUnload="doUnload()">
		</c:if>
	<c:if test='${sessionScope.sesScreenWidth == "800"}'>
	<frameset rows="27, 440,*" border="0" frameborder="0" onUnload="doUnload()">
	</c:if>--%>
		<iframe src="showTop" frameborder="0" scrolling="no" style="height: 30px;width: 1000px;border: 0;margin: 0 auto;display:block"></iframe>


		<c:if test='${requestScope.userPwdReseted == "true"}'> 
			<c:if test='${requestScope.ViewPortHeight lt 648}'> 
				<iframe src="../private/showPasswordChange.action?reset=true" name="main" frameborder="0" scrolling="yes"
				style="height: <c:out value='${ requestScope.ViewPortHeight}' escapeXml='false' />px;width: 999px;border: 0;margin: 0 auto;overflow-X:hidden;display:block;" 
				id="mainFrame" onload="cliseChilds()"></iframe>
			</c:if>
			<c:if test='${requestScope.ViewPortHeight ge 648 }'>
				<iframe src="../private/showPasswordChange.action?reset=true" name="main" frameborder="0"  scrolling="<u:scroll />" 
				style="height: <c:out value='${ requestScope.ViewPortHeight}' escapeXml='false' />px;width: 1000px;border: 0;margin: 0 auto;display:block;overflow-Y:hidden;" 
				id="mainFrame" onload="cliseChilds()"></iframe>
			</c:if> 
		</c:if>


		<c:if test='${requestScope.userPwdReseted != "true"}'>
			<c:if test='${requestScope.ViewPortHeight lt 648}'> 
				<iframe src="showMain" name="main" frameborder="0" scrolling="yes"
				style="height: <c:out value='${ requestScope.ViewPortHeight}' escapeXml='false' />px;width: 999px;border: 0;margin: 0 auto;overflow-X:hidden;display:block;" 
				id="mainFrame" onload="cliseChilds()"></iframe>
			</c:if>
			<c:if test='${requestScope.ViewPortHeight ge 648 }'>
				<iframe src="showMain" name="main" frameborder="0"  scrolling="<u:scroll />" 
				style="height: <c:out value='${ requestScope.ViewPortHeight}' escapeXml='false' />px;width: 1000px;border: 0;margin: 0 auto;display:block;overflow-Y:hidden;" 
				id="mainFrame" onload="cliseChilds()"></iframe>
			</c:if> 
		</c:if>



		<iframe src="showBottom" name="bottom" scrolling="no" frameborder="0"
		style="height: 50px;width: 1000px;margin:0 auto;display:block"></iframe>
	<!--</frameset>
	</body>
--></html>