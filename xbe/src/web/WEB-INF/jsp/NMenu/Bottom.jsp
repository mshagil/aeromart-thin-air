<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		<%-- Temporally Removed the theme changes in user wise --%>
	<%--<link href="../themes/<%=com.isa.thinair.xbe.core.web.util.ThemeUtil.getTheme(request) %>/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />--%>
	<%-- Added to Load css to be loaded client wise --%>
	<link href="../css/loadCSS_no_cache.css" rel="stylesheet" type="text/css" />
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/bandwidth.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
 	<script  src="../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/common/jQuery.commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<%-- 	<script src="../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> --%>
<%-- 	<script src="../js/v2/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
<%-- 	<script src="../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> --%>
  </head>
	 <body onmousewheel="return false;" scroll="no" onkeydown='return Body_onKeyDown(event)' oncontextmenu="return false" ondrag='return false'>
	 
	 <table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
	 	<tr>
				<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td width="974" height="14" align="left" valign="bottom" background="" class="bottomBackDesign">
				<img src="../images/spacer_no_cache.gif" width="100%" height="14"></td>
				<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		<tr>
			<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
			<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBottomColor" style="height:100%;" ID="Table5">
						<tr>
							<td width="30" class="PageBottomShadow"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td width="914" align="center" height="34" valign="middle">
								<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table6">
									<tr>
										<td width="3" valign="bottom"><!--<a href="javascript:void(0);" onclick="ShowPageMessage();" title="View Message(s)"><img id="imgError" name="imgError" src="../images/spacer_no_cache.gif" border="0"></a></td>-->
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td class="StatsBarDef" id="tdSTBar"><span id="spnDate">&nbsp;</span></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td width="100" align="center"><span id="spnBandwidth"></span></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td width="130" align="center"><span id="spnUID"></span></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td width="168" align="center"><span id="spnScreenID"></span></td>
									</tr>
								</table>
							</td>
							<td width="30" class="PageBottomShadow" align="center" valign="top"><img id="imgSSLLock" src="../images/lock_no_cache.jpg"></td>
						</tr>
					</table>
			</td>
			<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
	 	</tr>
	 </table>
		<!--<table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td width="974" height="24" align="left" valign="bottom"><img src="../images/bottom6_no_cache.jpg"></td>
				<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td height="20" align="left" valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBottomColor" style="height:100%;" ID="Table5">
						<tr>
							<td width="30" class="PageBottomShadow"><img src="../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td width="914" align="center" height="25" valign="middle">
								<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table6">
									<tr>
										<td width="20" valign="bottom"><a href="javascript:void(0);" onclick="ShowPageMessage();" title="View Message(s)"><img id="imgError" name="imgError" src="../images/spacer_no_cache.gif" border="0"></a></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td class="StatsBarDef" id="tdSTBar"><span id="spnDate">&nbsp;</span></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td width="130" align="center"><span id="spnBandwidth"></span></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td width="130" align="center"><span id="spnUID"></span></td>
										<td width="1"><img src="../images/StatusBar_no_cache.jpg"></td>
										<td width="200" align="center"><span id="spnScreenID"></span></td>
									</tr>
								</table>
							</td>
							<td width="30" class="PageBottomShadow" align="center" valign="top"><img id="imgSSLLock" src="../images/lock_no_cache.jpg"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
	--><script type="text/javascript"><!--
	var newMsgType = null;
	function getthedate(){
		var arrDay= new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
		var arrMonth = new Array("January","February","March","April","May","June","July","August","September","October","November","December")
		
		var dtCurrent = new Date()
		var intYear = dtCurrent.getYear()
		if (intYear < 1000)
			intYear+=1900
			var intDay = dtCurrent.getDay()
			var intMonth = dtCurrent.getMonth()
			var intDayM = dtCurrent.getDate()
			
		if (intDayM<10){intDayM="0"+intDayM;}
		var cdate="<b><font class='fontPGInfo'>"+arrDay[intDay]+", "+arrMonth[intMonth]+" "+intDayM+", "+intYear+"<\/font><\/b>"
		DivWrite("spnDate", cdate);	
	}	

	// Write to Divs / span
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	
	function SSLLock(blnStatus){
		//setVisible("imgSSLLock", blnStatus);
		setVisible("imgSSLLock", false);
	}
	SSLLock(false);
	HidePageMessage();
	top.setFID("");
	DivWrite("spnUID","<font class='fontPGInfo'>User ID : <b>" + top.strUID + "<\/b><\/font>");
	LoadBandwidthInfo();
	
	$(document).ready(function() {

		//code merged to fixed a issue related to this
		if (top.isTrackingEnabled && window.location.pathname.indexOf("private") > -1){
			var trackerVar = "showTrackerPrivate.action";
			//$("#frmTracking").attr("src", "showTracker");
			var trakIframe = $("<iframe></iframe>").attr({
				"id":"frmTracking",
				"name":"frmTracking",
				"src":trackerVar,
				"frameborder":0
			});
			trakIframe.css({
				"height":1,
				"width":1,
				"display":"none"
			});
			loadFrame = function(){
				$("#accelAeroXBETrack").append(trakIframe);
			}
			setTimeout(loadFrame,1000);
		}
	});
	var erroImgPath = "../images/error_icon_no_cache.gif";
//getthedate();	
//
--></script>
		<script src="../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/requestmanagement/requestsactionpopup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		<div id="errorMSGouter" style="display: none;">
			<div class="errorBox">
				<span id="errorMSG" style=""></span>
				<span class="errorClose">&nbsp;</span>
			</div>
		</div>
		<div id="errorPopup" style="display: none;">
			<div class="errorPopupHeader">
				<span class="text">Error Console</span>
				<span class="errorClose">&nbsp;</span>
				<div style="clear:both"></div>
			</div>
			<div class="errorPopupButton" style="">
				<a href="javascript:void(0)" class="errorBtn showAll errorSelected">All</a>
				<a href="javascript:void(0)" class="errorBtn showErrors"><span>&nbsp;</span>Errors</a>
				<a href="javascript:void(0)" class="errorBtn showWarnings"><span>&nbsp;</span>Warnings</a>
				<a href="javascript:void(0)" class="errorBtn showConfirm"><span>&nbsp;</span>Confirms</a> 
				<span class="errorSep">|</span>
				<a href="javascript:void(0)" class="errorBtn errorClear">Clear</a>
				<div style="clear:both"></div>
			</div>
				<div class="errorPopupbody" style="">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<div>
									<table id="errorPopupTable" border="0" width="100%" cellspacing="0" cellpadding="4">
									
									</table>
								</div>
							</td>
						</tr>
					</table>
				</div>
		</div>
		
		<div id="inProgressRequests" style="display: none;">
			
			<div class="inProgressRequestsHeader">
				<span class="text">My Inprogress</span>
				<span class="inProgressRequestsClose" id="inProgressRequestsClose">&nbsp;</span>
				<div style="clear:both"></div>
			</div>
			<div>
				<br>
				<font style="font-size:14px;padding-left:15px;"><b> My Inprogress Requests </b></font>
				<br>
				<hr style="margin: 10px;">
				
			</div>
			<div>
				<table width="100%" border="0" cellpadding="0" cellspacing="4">						
					<tr>
						<td id="requestsGridContainerPopup">
						</td>
					</tr>
					<tr>
						<td> <font> &nbsp; </font> </td>			
					</tr>
					<tr>
						<td width=40>
							<font> Remark </font>
						</td>
										
						<td>
							<textarea maxlength="1000" rows="3" cols="60" id="remarkInprogressRequests" name="remarkInprogressRequests" style="margin-left:-600px;width:310px"></textarea>
							<font class="mandatory">&nbsp;*</font>
						</td>
						
						<td>
							<table>		
							      <tbody>
							      	<tr>
								        <td style="padding: 5px">
								          <input name="btnToOpen" type="button" class="Button" id="btnToOpenActionPopup" value="Set To Open" style="margin-left: -260px;">
								        </td>
								    </tr>
							        
							      	<tr>
								        <td>
								          <input name="btnToClose" type="button" class="Button" id="btnToCloseActionPopup" value="Close Request" style="margin-left: -255px;">
								        </td>
							      	</tr>
							  		<tr>
									    <td style="padding: 5px">
									      <input name="btnReject" type="button" class="Button" id="btnRejectActionPopup" value="Reject Request" style="margin-left: -260px;">
									    </td>
							     	</tr>
							  
							  	</tbody>
							</table>
						</td>
					</tr>
				</table>
			</div>		
		</div>
		
		<div id="accelAeroXBETrack" style="display: none">
		</div>
  	</body>
</html>
