 <%-- 
	 @Author 	: 	Dhanushka
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  </head>
  <body onload="load()" onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
  	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmPaxStatus" id="frmPaxStatus" action="" method="post">
		<script type="text/javascript">
			var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			var arrError = new Array();
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			var displayAgencyMode = 0;
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
		</script>
		<table width="99%" border="0" cellpadding="0" cellspacing="0">				
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>							
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Passenger Status Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblAdminAudit">					  		
							<tr>
								<td width="60%"></td>					
							</tr>
							<tr>
								<td colspan="2">
									<table width="50%" border="0" cellpadding="0" cellspacing="2" >
										<tr><td colspan="4"><font class="fntBold">Departure Date&nbsp;&nbsp;&nbsp;</font></td>
											<tr>
												<td width="24%"><font>From</font></td>
												<td width="35%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="settingValidation('txtFromDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
												<td width="15%"><font>To</font>
												<td width="25%"><input name="txtToDate" type="text" id="txtToDate" size="10" maxlength="10" onBlur="settingValidation('txtToDate')">
												<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>							
							<tr>
								<td colspan="2">
							<table border="0" cellpadding="0" cellspacing="2" width="100%">
								<tr>
									<td width="20%">
									<font class="fntBold">Flight Number&nbsp;&nbsp;&nbsp;</font>
									<input name="txtFlightNumber" type="text" id="txtFlightNumber" size="10" style="width:75px;" maxlength="7" >
									</td>
								</tr>
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							</table>
							</td>
							</tr>
							
							<tr>					
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>				
										<td width="13%"><font class="fntBold">Departure&nbsp;&nbsp;&nbsp;</font></td>
										<td width="20%">
											<select id="selDept" size="1" name="selDept" style="width:75px" tabindex="1" maxlength="10">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     	</select>
									     </td>	
									     
										<td width="8%"><font class="fntBold">Arrival&nbsp;&nbsp;&nbsp;</font></td>
										<td>
											<select id="selArival" size="1" name="selArival" style="width:75px" tabindex="1" maxlength="10">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     	</select>
									    </td>		
									    
									  </tr>
								</table>
							</tr>		
											
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							
							<tr>
							<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>								
										<td align="left" width="13%"><font class="fntBold">Pax Status&nbsp;&nbsp;&nbsp; </font></td>
										<td>
											<select name="selPAXStatus" id="selPAXStatus" size="1" title="Status">
											<c:out value="${requestScope.reqPaxStatusList}" escapeXml="false" />
											</select>									
										</td>
									</tr>
							</table>
							<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td align="left" width="20%"><font class="fntBold">Based On </font></td>
													<td width="20%"><input type="radio" name="radBasedOn" id="radOriginAgent" value="ORIGIN_AGENT" class="noBorder" checked><font>Origin Agent</font></td>		
													<td width="20%"><input type="radio" name="radBasedOn" id="radOwnerAgent" value="OWNER_AGENT" class="noBorder"><font>Owner Agent</font></td>
													<td width="40%"></td>
												</tr>																	
							</table>
							</td>
							</tr>
							<tr>
							<td>
							 <table width="100%" border="0" cellpadding="0" cellspacing="2">
							 	<tr>
							 		<td align="left"><font class="fntBold">PFS Status&nbsp;&nbsp;&nbsp; </font> 
								<select name="selPFSStatus" id="selPFSStatus" size="1" title="PFSStatus">
									<c:out value="${requestScope.reqPfsStatusList}" escapeXml="false" />
									</select>
									
								</td>
							 	</tr>
							 </table>
							 </td>
							 </tr>
							<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
									<tr>
										<td>
										<div id="divAgencies">
											<table>
												<tr>
													<td width="40%">
													<font>Agencies</font>
													</td>
													<td>														
														<select id="selAgencies" size="1" name="selAgencies" onChange="changeAgencies()" style="width:76px">
															<option value=""></option>
															<option value="All">All</option>
															<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
														</select>
													</td>
													
													<td>
														<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
													</td>
												</tr>
											</table>
										</div>
									</td>
									</tr>
									<tr>
									<td>
										<div id="divAgents">
											<table>
												<tr>
													<td>
														<font class="fntBold">Agents</font>
													</td>
												</tr>
												<tr><td>
												<table width="62%" border="0" cellpadding="0" cellspacing="2">	
												<tr>
													<td valign="top" width="2%"><span id="spn1"></span></td>
													<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
													</table></td>
												</tr>						
											</table>
										</div>
									</td>
							</tr>
							</table>
							</td>	
							</tr>
							
							
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>								
								<td>
									<font class="fntBold">Output Option</font>
								</td>
							</tr>	
								
							<tr>								
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"	value="HTML" onClick="chkClick()" class="noBorder" checked><font>HTML</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF" onClick="chkClick()" class="noBorder"><font>PDF</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL" onClick="chkClick()" class="noBorder"><font>EXCEL</font></td>
											<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV" onClick="chkClick()" class="noBorder"><font>CSV</font></td>
										</tr>																	
									</table>
									<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>	
							</tr>	
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
								</td>
								<td >
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>	

			<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />				
				<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}				
			</script>	

		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  </body>
<script src="../js/reports/PAXStatusReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
