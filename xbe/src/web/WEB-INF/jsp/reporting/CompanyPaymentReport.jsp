<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ include file='../v2/common/inc_PgHD.jsp' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../js/v2/common/JQuery.xbe.i18n.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		var selectBox;
		var modDetailsEnabled = "<c:out value="${requestScope.reqModDetailsEnabled}" escapeXml="false" />";
		var systemDate = "<c:out value="${requestScope.systemDate}" escapeXml="false" />";
		var offlineReportParams = "<c:out value="${requestScope.offlineReportParams}" escapeXml="false" />";
		var displayAgencyMode = 0;
		<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
		var repShowpay = "<c:out value="${requestScope.reqSowPay}" escapeXml="false" />";	
		<c:out value="${requestScope.allSubAgentTypes}" escapeXml="false" />
		var stationFilterEnabled =  "<c:out value="${requestScope.reqStationsFilterForCompanyPayRpt}" escapeXml="false" />";
	</script>
  </head>
  <body onkeypress='return Body_onKeyPress(event)' onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<div style="overflow: auto;" id="lengthScreen">
  	<form name="frmPage" id="frmPage" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="Header" i18n_key="CompPayRpt_lbl_CompPayRpt">Company Payment Report</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%><font i18n_key="CompPayRpt_Company_Payments">Company Payments</font><%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
				  			<tr>
								<td width="60%"><font class="fntBold" i18n_key="CompPayRpt_lbl_DateRange">Date Range</font></td>
								<td width="20%"></td>
								<td></td>				
							</tr>				  		
							<tr>
								<td>
									<table width="auto" border="0" cellpadding="0" cellspacing="2">	
										<tr><td width="100"><font class="fntBold" ></font></td>
											<td width="110" align="left"><font i18n_key="CompPayRpt_lbl_From">From : </font><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true"></td>
											<td width="30"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											<td width="50" align="left">&nbsp;</td>
											<td width="100"><font i18n_key="CompPayRpt_lbl_To">To : </font><input	name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true"></td>
											<td width="30"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											<td width="50" align="left">&nbsp;</td>
											<td width="15%"><input type="checkbox" name="chkLocalTime" id="chkLocalTime" value="on" class="noBorder" align="left"><font i18n_key="CompPayRpt_opt_LocalTime">In Local Time</font></td>
											
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
						
						<tr>
							<td width='55%'>
								<font class="fntBold" i18n_key="CompPayRpt_lbl_ModeOfPay">Mode of Payment</font>
							</td>
						<u:hasPrivilege privilegeId="rpt.ta.comp.all,rpt.ta.comp.rpt">
						<td>
							<font class="fntBold">Agents Stations</font>
						</td>
						</u:hasPrivilege>
						</tr>
						<tr>
							<td>
								<table width="80%" border="0" cellpadding="0" cellspacing="0">
									<tr>	
										<td colspan ="3" rowspan="7" valign="top">
											<span id="spn2" class="FormBackGround"></span>
										</td>
									<td valign="bottom"><font class="mandatory"><b>*</b></font></td>
							  	</tr>
							</table>
							</td>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<u:hasPrivilege privilegeId="rpt.ta.comp.all,rpt.ta.comp.rpt">
										<td colspan ="3" rowspan="7" valign="top">
										<span id="spnSTC" class="FormBackGround"></span>
										</td>
										<td valign="bottom"><font class="mandatory"><b>*</b></font></td>
										</u:hasPrivilege>
							  		</tr>
							</table>
						</td>
						</tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>						
							<tr>
								<td colspan="2">
									<table width="auto" border="0" cellpadding="0" cellspacing="2">	
										<tr>
											<td><input type="checkbox" name="chkSales" id="chkSales" value=" Sales" checked="checked" class="noBorder" align="left"></td>
											<td width="100">
												<font i18n_key="CompPayRpt_opt_Sales">Sales</font>
											</td>
											<td><input type="checkbox" name="chkRefunds" id="chkRefunds" value=" Refunds" checked="checked" class="noBorder" align="left"></td>
											<td width="100">	
												<font i18n_key="CompPayRpt_opt_Refunds">Refunds</font>
											</td>
											<td><input type="checkbox" name="chkModifications" id="chkModifications" value=" Modifications" class="noBorder" align="left"></td>
											<td width="100">
												<div id="divModifications"><font i18n_key="">Modifications</font></div>
											</td>
											<td width="70"><font i18n_key="CompPayRpt_lbl_FlightTypes">Flight Type</font></td>
											<td width="110">
												<select name="selFlightType" id="selFlightType" size="1" title=" Flight Type">
													<option value="All" selected="selected">All</option>
													<c:out value="${requestScope.reqFlightTypes}" escapeXml="false" />
												</select><font class="mandatory"> &nbsp;* </font>
											</td>
											<u:hasPrivilege privilegeId="rpt.ta.comp.discount">
												<td width="120"><font i18n_key="">Fare Discount Type</font></td>
												<td width="110">
														<select name="selFareDiscountCode" id="selFareDiscountCode" size="1" title="Fare Discount Code">
															<option value="All" selected="selected">All</option>
															<c:out value="${requestScope.reqFareDiscountTypes}" escapeXml="false" />
														</select>
												</td>
											</u:hasPrivilege>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<div id="divAgencies">
										<table>
											<tr>
												<td width="100" valign="top"><font i18n_key="CompPayRpt_lbl_Agencies">Agencies</font></td>
												<td valign="top">
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All" i18n_key="CompPayRpt_list_All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td width="40">&nbsp;</td>
												<td width="80" valign="top">
													<font class="fntBold">Reporting agent types</font>			
												</td>
												<td valign="top">
													<select name="selRepotingAgentTypes" id="selRepotingAgentTypes" style="width:60px; height:76px;" multiple>
														
													</select>				
												</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td valign="top"><input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()" i18n_key="CompPayRpt_btn_List"></td>
											</tr>
											<tr>
											<td width="100"><font>Status</font></td>
											<td>
												<select id="agentStatusFilter" size="1" name="agentStatusFilter" onChange="changeAgencies()" style="width:76px">
														
														<option value="All">All</option>
														<c:out value="${requestScope.reqCmpPayAgentStatusList}" escapeXml="false" />					
												</select>
												
											</td>
											<td width="40">&nbsp;</td>								
											<td><input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"></td>
											<td>
												<font i18n_key="CompPayRpt_opt_WithRptCo">With reporting CO's</font>
											</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>	
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											</tr>
																						
										</table>
									</div>
								</td>
							</tr>							
							<tr>
								<td colspan="2">
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold" i18n_key="CompPayRpt_lbl_Agents">Agents</font>
												</td>
											</tr>
												
											<tr>
												<td>
												<table width="62%" border="0" cellpadding="0" cellspacing="2">	
												<tr>
													<td valign="top" width="2%"><span id="spn1"></span></td>
													<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
												</table>
												</td>
												</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>							
								<td>
								<div id="divPay">
									<table width="40%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td><font i18n_key="CompPayRpt_lbl_PaymentSource">Payment Source</font></td>
											<td><select id="selPaySource" name="selPaySource">
													<option value="INTERNAL" i18n_key="CompPayRpt_list_ResSys">Reservation System</option>
													<option value="EXTERNAL" i18n_key="CompPayRpt_list_External">External</option> 
												</select>
											</td>											
										</tr>
									</table>
									</div>	
								</td>
								<td>
									<u:hasPrivilege privilegeId="rpt.ta.comp.base">
										<font i18n_key="CompPayRpt_opt_InBaseCurrency">In Base Currency</font>
									</u:hasPrivilege>
								</td>
								<td>
									<u:hasPrivilege privilegeId="rpt.ta.comp.base">
									<input type="checkbox" name="chkBase" id="chkBase" value="on" class="NoBorder">
									</u:hasPrivilege>
								</td>
								<td colspan="2">
								<u:hasPrivilege privilegeId="rpt.rev.new.view">
								<font i18n_key="">New View</font> 
								</u:hasPrivilege>
								</td>
								<td colspan="2">
								<u:hasPrivilege privilegeId="rpt.rev.new.view">
								<input type="checkbox" name="chkNewview" id="chkNewview" value="on" class="NoBorder">
								</u:hasPrivilege>
								</td>							
							</tr>
							<tr>
								<td>
								<div id="divEntity">
									<table width="40%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td><font i18n_key="CompPayRpt_lbl_Entity">Entity</font>&nbsp;&nbsp;&nbsp;</td>
											<td><select id="selEntity" name="selEntity">
													<c:out value="${requestScope.reqEntities}" escapeXml="false" />
												</select>
											</td>											
										</tr>
									</table>
									</div>	
								</td>
								<td colspan="6">
								</td>
							</tr>
								
							<tr>
								<td colspan="7">
									<input type="checkbox" name="chkAddInfo" id="chkAddInfo" value="ADDINFO"  class="noBorder"><font i18n_key="CompPayRpt_opt_PrintAdditionalInfo">Print Additional Information</font>
								</td>
							</tr>
							
							<tr><td width="60%">
									<font class="fntBold" i18n_key="CompPayRpt_lbl_OutputOption">Output Option</font>
								</td></tr>

							<tr><td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><font i18n_key="CompPayRpt_opt_HTML">HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font i18n_key="CompPayRpt_opt_PDF">PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font i18n_key="CompPayRpt_opt_EXCEL">EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font i18n_key="CompPayRpt_opt_CSV">CSV</font></td>
							</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
							</td>							
							<tr>
								<td colspan="2">
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()" i18n_key="CompPayRpt_btn_Close">
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()" i18n_key="CompPayRpt_btn_View">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />
				<c:out value="${requestScope.reqStnHtmlData}" escapeXml="false" />
				
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}				
		</script>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnPayments" id="hdnPayments" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnPaymentDesc" id="hdnPaymentDesc" value="">
		<input type="hidden" name="hdnReportView" id="hdnReportView" value="">
		<input type="hidden" name="hdnSelectedAllAgents" id="hdnSelectedAllAgents" value="">
		<input type="hidden" name="hdnReportingAgentTypes" id="hdnReportingAgentTypes" value="">
		<input type="hidden" name="hdnStations" id="hdnStations" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnEntityText" id="hdnEntityText" value="">
	</form>
	
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
	</div>
  </body>
  <%@ include file="../common/IncludeLoadMsg.jsp"%>
  <script src="../js/reports/ValidateCompanyPayments.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  <script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height
  </script>
  <script type="text/javascript">
   <!--
//   var objProgressCheck = setInterval("ClearProgressbar()", 300);
//   function ClearProgressbar(){
//      clearTimeout(objProgressCheck);
//      HideProgress();
//   }
   //-->
   $("#lengthScreen").getLanguage();
  </script>
</html>
