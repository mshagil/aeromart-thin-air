<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
		
	<script src="../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type="text/javascript">
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		var arrError = new Array();
		var displayAgencyMode = 0;
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
		<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
	</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return showContextMenu()" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="yes">
  <%@ include file="../common/IncludeTop.jsp"%>
  <div style="overflow: auto;" id="lengthScreen">
  	<form name="frmForceConfrmdRpt" id="frmForceConfrmdRpt" action="" method="post">
  	
  			 <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="Header">Force Confirmed Booking Report</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
					
					<!-- START -->				
				<%@ include file="../common/IncludeFormTop.jsp"%>Force Confirmed Booking Report<%@ include file="../common/IncludeFormHD.jsp"%>
				  	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
						<tr>
							<td><font class="fntBold">Booking Date Range</font></td>				
						</tr>
						<tr>
							<td>
								<table width="62%" border="0" cellpadding="0" cellspacing="2">	
									<tr>
										<td width="15%" align="left"><font>From &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
										<td width="30%" align="left"><input name="txtBkngFromDate" type="text" id="txtBkngFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtBkngFromDate')" invalidText="true">
											<font>&nbsp;&nbsp;&nbsp;</font><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
										<td width="20%"><font>To </font><input	name="txtBkngToDate" type="text" id="txtBkngToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtBkngToDate')" invalidText="true"></td>
										<td width="35%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						</tr>
						<tr>
							<td><font class="fntBold">Departure Date Range</font></td>				
						</tr>
						<tr>
							<td>
								<table width="62%" border="0" cellpadding="0" cellspacing="2">	
									<tr>
										<td width="15%" align="left"><font>From &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
										<td width="30%" align="left"><input name="txtDeptFromDate" type="text" id="txtDeptFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtDeptFromDate')" invalidText="true">
											<font>&nbsp;&nbsp;&nbsp;</font><a href="javascript:void(0)" onclick="LoadCalendar(2,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a></td>
										<td width="20%"><font>To </font><input	name="txtDeptToDate" type="text" id="txtDeptToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtDeptToDate')" invalidText="true"></td>
										<td width="35%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a></td>
									</tr>
								</table>
							</td>
							<td>
							</td>
						</tr>	
						<tr>
						<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td><font class="fntBold">Origin &amp; Destination</font></td>
								<td><font>Departure</font></td>
								<td><select name="selDeparture" size="1" id="selDeparture"
									style="width: 55px;">
										<option value=""></option>
										<c:out value="${requestScope.airportList}" escapeXml="false" />
								</select></td>
								<td><font>&nbsp;&nbsp;Arrival</font></td>
								<td><select name="selArrival" size="1" id="selArrival"
									style="width: 55px;">
										<option value=""></option>
										<c:out value="${requestScope.airportList}" escapeXml="false" />
								</select></td>
								<td><font>&nbsp;</font></td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
							<td><font class="fntBold">Paid Status</font></td>
							<td><select name="payStatus" size="1" id="payStatus">
										<option value=""></option>
										<c:out value="${requestScope.reqForceConfirmedPayStatuType}" escapeXml="false" />
							</select></td>
							</tr>
							</table>
							</td>
						</tr>												
						<tr>
							<td width="60%"><font class="fntBold">Output Option</font></td>
						</tr>
						<tr>
							<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><font>HTML</font></td>
										<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
										<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
										<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font>CSV</font></td>
									</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
							</td>							
						</tr>
						<tr>
							<td  width = "90%">
								<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
							</td>
							<td>
							<div id="divSchedFrom"></div>
							<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched" value="Schedule" onClick="viewClick(true)">
							</td>
							<td align="right">
								<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick(false)">
							</td>
						</tr>
					</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>			
					
					<!--   END -->
				  	</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">		
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">		
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
	</form>
	</div>
  </body>  
  <script src="../js/reports/ForceConfirmedBookingReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   document.getElementById("lengthScreen").style.height = parent.document.getElementById('mainFrame').style.height
  </script>
</html>
