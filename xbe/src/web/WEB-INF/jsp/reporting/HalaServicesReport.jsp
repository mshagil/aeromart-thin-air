<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Hala Services Booked</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	

<script type="text/javascript">
		var arrError = new Array();
		<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
</script>
<!--For Calendar-->
</head>
<body onload="pageLoad()"  onbeforeunload="beforeUnload()" oncontextmenu="return false">
<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
<form action="" method="post" id="frmViewBookedHalaServices"
	name="frmViewBookedHalaServices">
	
	<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
		</tr>
		<tr>
			<td><%@ include file="../common/IncludeFormTop.jsp"%>Booked Hala Services<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="2"
					ID="tblHead">	
					<tr>
						<td width="100%">
							<table width="60%" border="0" cellpadding="0" cellspacing="2">	
								<tr><td width="20%" align="left"><font>Departure Date From (Local)</font></td>
								<td width="10%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')" tabindex="1"></td>
								<td width="12%" align="left" valign="bottom">&nbsp;<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From" tabindex="2"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
								<td width="20%"><font>Departure Date To (Local) </font></td>
								<td width="14%"><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')" tabindex="3"></td>
								<td width="24%" valign="bottom">&nbsp;<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0" tabindex="4"></a><font class="mandatory">&nbsp;*</font></td>
								
								</tr>
							</table>
						</td>
					</tr>									
					<tr>
					<td><table width="60%" border="0" cellpadding="0" cellspacing="2">
					</tr>				
					<tr>
					<td width="100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">	
					
         					 <td width="18%"><font>Origin</font></td>
         					 <td width="20%"><select id="selFromAirport" size="1" name="selFromAirport" style="width:75px" tabindex="5" maxlength="10">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     </select></td>					
					          <td width="26%" align="right"><font>Destination&nbsp;</font></td>
					          <td width="18%"><select id="selToAirport" size="1" name="selToAirport" style="width:75px" tabindex="6" maxlength="10">
											<option value=""></option>
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									     </select></td>
									     <td width="10%"></td>
									     <td width="8%"></td>
									     </table>
						</td>
					</tr>
					</table>
					</td>
				</tr>		
							
				<tr>			
					<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="2">				
						<tr><td width="52%"><font>Flight No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input name="txtFlightNo" type="text" id="txtFlightNo" size="10" style="width:75px;" maxlength="7" tabindex="7"></td>	
										     
						</tr>
					</table>
					</td>
					
														     						    			
				</tr>
				<tr>
				<td width="100%">
				<table width="80%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				<td width="14%">
					<font>Service Category </font>
				</td>
				<td width="35%"><select name="serviceCategory" id="serviceCategory" size="1" tabindex="8" style="width:120px" title="Service Category">																					
											<option value="Hala">Hala</option>
											<c:out value="${requestScope.reqServiceCategories}" escapeXml="false" />
											</select>
				</td> 
				<td width="51%"></td>
				</tr>	
				</table>
				</td>				
				</tr>
				<tr>				
				<tr>
					<td>
						<table id="carrierCode" style="display: none;">
							<tr>
								<td>
									<font class="fntBold"> Carrier Code </font>										
								</td>							
							</tr>
							
							<tr>
								<td>
									<table>
										<tr>
											<td valign="top"><span id="spn3"></span></td>
											<td valign="bottom"><font class="mandatory"> <b>*</b></font></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>	
					</td>				
				</tr>
				
				<tr>
				<td></td>				
				</tr>	
							
				<tr>				
					<td><font class="fntBold">Output Option</font></td>							
				</tr>
				<tr>
					<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="HTML" class="noBorder"  checked tabindex="9"><font>HTML</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
						value="PDF" class="noBorder"><font>PDF</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="EXCEL" class="noBorder"><font>EXCEL</font></td>
					<td><input type="radio" name="radReportOption" id="radReportOption" 
						value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=90%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()" tabindex="10"></td>
				<td width=10% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()" tabindex="11"></td>	
			</tr></table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnReportView" id="hdnReportView">
	<input type="hidden" name="hdnCarrierCode" id="hdnCarrierCode">
	<input type="hidden" name="fromDate" id="fromDate">
	<input type="hidden" name="toDate" id="toDate">
	<input type="hidden" name="origin" id="origin">
	<input type="hidden" name="chargeId" id="chargeId">
</form>
<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<%@ include file="../common/IncludeLoadMsg.jsp"%>
<script src="../js/reports/ViewBookedHalaServices.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">

<c:out value="${requestScope.carrierCodeList}" escapeXml="false" />
  
</script>
</html>