<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.DATE,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%>
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/PrivilegeConstants.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
  </head>
  
  <body onkeypress='return Body_onKeyPress(event)'  class="PageBackGround" oncontextmenu="return true" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="<u:scroll />"
  		onload="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>' ,'<c:out value="${requestScope.warningmessage}"/>')" >

	<div id="flightResultsWindow" style="display: none;position: absolute;top: 0%;left: 0%;width: 100%;height: 100%;background-color: white;z-index:1001;-moz-opacity: 0.5;opacity:.50;filter: alpha(opacity=50);"></div>
  <!--body class="tabBGColor" oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" onbeforeunload="beforeUnload()" -->
	<form method="post" name="frmFlight" action="showAncillaryReportFlightSearch.action">
	
	  
  		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0" class="PageBackGround">
				<tr>
					<td><font class="Header">Flight Ancillary Report</font></td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>		
		
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Flight Search<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table2">
							<tr>
								<td colspan="2"><font>Start Date - Local</font></td>								
								<td colspan="2"><font>Stop Date - Local</font></td>								
								<td><font>Flight No</font></td>
								<td><font>From</font></td>
								<td><font>To</font></td>
								<td><font>Operation Type</font></td>
								<td><font>Status</font></td>								
							</tr>
							<tr>
								<td style="width:75px;">									
									<input type="text" name="txtStartDateSearch" id="txtStartDateSearch"  invalidText="true"  onBlur="dateChk('txtStartDateSearch')" value=<%=StartDate %> style="width:70px;"  maxlength="10" tabIndex="1">
								</td>
								<td>
									<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar" tabIndex="2"><img src="../images/Calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
								</td>
								<td style="width:75px;">
									<input type="text" name="txtStopDateSearch" id="txtStopDateSearch" invalidText="true" onBlur="dateChk('txtStopDateSearch')" value=<%=StopDate %> style="width:70px;" maxlength="10" tabIndex="3">
								</td>
								<td>
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../images/Calendar_no_cache.gif" border="0" tabIndex="4"></a><font class="mandatory"> &nbsp;* </font>
								</td>								
								<td><!-- 
									<select id="txtFlightNoStartSearch" size="1" name="txtFlightNoStartSearch" style="width:35px" tabindex="1" maxlength="10">																				
																																			
								    </select>-->
									<font class="mandatory"> &nbsp;* </font><input type="text" id="txtFlightNoStartSearch" name="txtFlightNoStartSearch" maxlength="2" style="width:20px" value="" readonly="readonly" tabIndex="7">
									<!--input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="4" style="width:105px" onkeyUp="flightValid('txtFlightNoSearch')" onkeyPress="flightValid('txtFlightNoSearch')"-->									
									<input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="4" style="width:50px" tabIndex="8"> 									
								</td>
								<td>
									<select name="selFromStn6" id="selFromStn6" size="1" maxlength="30" style="width:60px" title="Origin" tabIndex="9">
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select name="selToStn6" id="selToStn6" size="1" maxlength="30" style="width:60px" title="Destination" tabIndex="10">										
										<option></option>
										<c:out value="${sessionScope.sesHtmlAirportData}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select name="selOperationTypeSearch" id="selOperationTypeSearch" size="1" maxlength="30" tabIndex="11">
										<c:out value="${sessionScope.sesOperationType}" escapeXml="false" />
										<option>All</option>
									</select><font class="mandatory"> &nbsp;* </font>
								</td>
								<td>
									<select name="selStatusSearch" id="selStatusSearch" size="1" maxlength="20" title="Flight Status" tabIndex="12">
										<c:out value="${sessionScope.sesStatus}" escapeXml="false" />	
										<option>All</option>										
									</select><font class="mandatory"> &nbsp;* </font>
								</td>
								<td></td>
								<td align="right">
									<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="button" onclick="SearchData()" tabIndex="13">
								</td>
							</tr>
							<tr>
								<td colspan="11"></td>								
							</tr>																																
						</table>
						
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr><br><tr>
					  		<td style="height:370px;" valign="top">
						  		<%@ include file="../common/IncludeFormTop.jsp"%>Flight Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table8">
										<tr>
										  <td style="height:5px">
										  </td>
										</tr>										
										<tr>
											<td valign="top"><span id="spnInstances"></span></td>
										</tr>
									</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
					  		</td>
					  	</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
						<table width="17%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table8">
								<tr>
									
									<td align="right">
										<input type="button" id="btnExit" value="Close" class="Button" NAME="btnExit" onclick="top.LoadHome()">										
										
									</td>
									
									<td align="right">
										<input type="button" id="btnPreview" value="Preview" class="Button" NAME="btnPreview" onclick="showAncillaryReportOptionsWindow(1)">										
										
									</td>
								
								</tr>
						</table>					
						<%@ include file="../common/IncludeFormBottom.jsp"%>  	  		
					  </td>
				</tr><tr><td>
				<div id="flightAncillaryOptions" style="position:absolute;top:210px;left:175px;width:550px;z-index:1002;display:none;overflow-y: auto;background-color: white;height: 135px">
				<div class="ui-widget ui-widget-content ui-corner-all addPaddingBottom" style="width:548px;height: 130px">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">
		<tr>
			<td colspan="2" height="20" class="FormHeadBackGround"><font class="FormHeader">Ancillary Report Options</font></td>
			<td width="23" height="20" class="FormHeadBackGround">
				<a class="winClose" title="Close Popup" href="javascript:showAncillaryReportOptionsWindow(0)">
										<img width="17" border="0" src="../images/spacer_no_cache.gif" id="imgMMCl">
				</a>
			</td>
		</tr>
		<tr>
			<td class="FormBackGround" width="10"><img width="10" border="0" src="../images/spacer_no_cache.gif"></td>
			<td class="FormBackGround" valign="top">
						<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table8" border="1">
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<%--</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table9" border="1">--%>
								<tr>
									<td>&nbsp;Ancillary Types</td>
									<td align="left" colspan="4">
										<input type="checkbox" id="chkSeatSelected" name="chkSeatSelected" checked>Seat</input>&nbsp;
										<input type="checkbox" id="chkMealSelected" name="chkMealSelected" checked>Meal</input>&nbsp;
										<input type="checkbox" id="chkBaggageSelected" name="chkBaggageSelected" checked>Baggage</input>&nbsp;
										<input type="checkbox" id="chkInsuranceSelected" name="chkInsuranceSelected" checked>Insurance</input>&nbsp;
										<input type="checkbox" id="chkHalaSelected" name="chkHalaSelected" checked>Hala</input>&nbsp;							
										<input type="checkbox" id="chkFlexiSelected" name="chkFlexiSelected" checked>Flexi</input>&nbsp;
										<input type="checkbox" id="chkBusSelected" name="chkBusSelected" checked>Bus</input>&nbsp;
									</td>

								</tr>
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<tr>
									<td>&nbsp;Output Options</td>
									<td align="left" colspan="4">
										<input type="radio" name="radViewOption" id="radViewOption"	value="Html" onClick="chkClick()" class="noBorder" checked>HTML</input>&nbsp;
										<input type="radio" name="radViewOption" id="radViewOption" value="Pdf" onClick="chkClick()" class="noBorder">PDF</input>&nbsp;
										<input type="radio" name="radViewOption" id="radViewOption" value="Excel" onClick="chkClick()" class="noBorder">EXCEL</input>&nbsp;
										<input type="radio" name="radViewOption" id="radViewOption" value="Csv" onClick="chkClick()" class="noBorder">CSV</input>&nbsp;
									</td>

								</tr>
								<tr><td colspan="5" style="height:10px;"></td></tr>
								
								<tr><td colspan="5" style="height:7px;"></td></tr>
	
								<tr><td colspan="5" style="height:10px;"></td></tr>
								<%--</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" align="right" ID="Table10" border="1">--%>
									<tr>
										<td align="left"><input type="button" id="btnClose" value="Close" class="Button" onclick="showAncillaryReportOptionsWindow(0)"/></td>
										<td colspan="4" align="right">
										<input type="button" id="btnShow" value="Show" class="Button" onclick="previewFlightAncillaryReport()"/>
										</td>	
									</tr>
								</table>
							</td>
							<td class="FormBackGround" width="10"><img width="10" border="0" src="../images/spacer_no_cache.gif"></td>
						</tr>	
				</table>
			</div></div>			
			</td>
		</tr>
	</table>
	
  	<%@ include file="../common/IncludeShortCuts.jsp"%>  	
   	<%@ include file="../common/IncludeLoadMsg.jsp"%>

	<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
	<input type="hidden" name="hdnRecNo" id="hdnRecNo"  value=""/>	
	<input type="hidden" name="hdnCosType" id="hdnCosType" value=""/>
			
	</form>
		<script src="../js/reports/AncillaryReportFlightInstance.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../js/reports/AncillaryReportFlightInstanceGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">	
	    
	   
		</script>	
		
		<form method="post" name="frmF" id ="frmF" action="showAncillaryReportFlightSearch.action">

		</form>

  </body>
</html>