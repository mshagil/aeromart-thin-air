<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Booked SSR Details Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
<script
	src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var arrError = new Array();
	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />

	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
<!--For Calendar-->
</head>
<body onload="winOnLoad()" onbeforeunload="beforeUnload()"
	oncontextmenu="return false">
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
	<form action="showBookedSSRDetailsReport.action" method="post"
		id="frmBookedSSR" name="frmBookedSSR">

		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Booked
					SSR Details Report<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="tblHead">
						<tr>
							<td>
								<table width="90%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="15%"><font class="fntBold">Service
												Category</font>
										</td>
										<td width="20%"><select name="selCategory" size="1"
											id="selCategory" onchange="filterSSRCodeByCategory()">
												<option value=""></option>
												<c:out value="${requestScope.reqServiceCategories}"
													escapeXml="false" />

										</select> <font class="mandatory"><b>*</b> </font>
										</td>

										<td width="15%"><font class="fntBold">Report Type</font>
										</td>
										<td width="15%"><select name="selReportType" size="1"
											id="selReportType">
												<option value=""></option>
												<option value="DETAIL">Details</option>
												<option value="SUMMARY">Summary</option>
										</select><font class="mandatory"><b>*</b> </font>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table width="100%" border="0" cellpadding="2" cellspacing="5">
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="25%"><font class="fntBold">Departure
															Date</font>
													</td>

													<td width="20%" align="left"><font>From </font> <input
														name="txtFromDate" type="text" id="txtFromDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtFromDate')" invalidText="true">
													</td>
													<td width="20%"><a href="javascript:void(0)"
														onclick="LoadCalendar(0,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
													<td width="18%"><font>To </font> <input
														name="txtToDate" type="text" id="txtToDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtToDate')" invalidText="true">
													</td>
													<td width="17%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(1,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="25%"><font class="fntBold">Booked
															Date</font>
													</td>

													<td width="20%" align="left"><font>From </font> <input
														name="txtBookedFromDate" type="text"
														id="txtBookedFromDate" size="10" style="width: 75px;"
														maxlength="10" onblur="dateChk('txtBookedFromDate')"
														invalidText="true">
													</td>
													<td width="20%"><a href="javascript:void(0)"
														onclick="LoadCalendar(2,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a></td>
													<td width="18%"><font>To </font> <input
														name="txtBookedToDate" type="text" id="txtBookedToDate"
														size="10" style="width: 75px;" maxlength="10"
														onblur="dateChk('txtBookedToDate')" invalidText="true">
													</td>
													<td width="17%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(3,event); return false;"
														title="Date From"><img
															SRC="../images/Calendar_no_cache.gif" border="0"
															border="0"> </a></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td><table width="100%" border="0" cellpadding="0"
												cellspacing="2">
												<tr>
													<td width="15%"><font>Flight Number </font>
													</td>
													<td width="55%"><input type="text"
														name="txtFlightNumber" id="txtFlightNumber" maxlength="10"
														onchange="" size="10">
													</td>
													<td width="10%">&nbsp;</td>
													<td width="20%">&nbsp;</td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td><font class="fntBold">O &amp; D</font></td>
													<td><font>Departure</font></td>
													<td><select name="selDeparture" size="1"
														id="selDeparture" style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Arrival</font></td>
													<td><select name="selArrival" size="1" id="selArrival"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>&nbsp;</font></td>
													<td colspan="2" rowspan="3"><select id="selSegment"
														name="selSegment" multiple size="1"
														style="width: 200px; height: 85px">
													</select></td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 1</font></td>
													<td><select name="selVia1" size="1" id="selVia1"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 2</font></td>
													<td><select name="selVia2" size="1" id="selVia2"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="addToList()" title="Add to list"><img
															src="../images/AA115_no_cache.gif" border="0"> </a></td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 3</font></td>
													<td><select name="selVia3" size="1" id="selVia3"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 4</font></td>
													<td><select name="selVia4" size="1" id="selVia4"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="removeFromList()" title="Remove from list"><img
															src="../images/AA114_no_cache.gif" border="0"> </a></td>
												</tr>
											</table>
										</td>
									</tr>














									<%--
									<tr>
										<td>
											<table width="100%" border="0" cellpadding="0"
												cellspacing="2">
												<tr>
													<td width="8%"><font>Departure</font></td>
													<td width="10%"><select id="selDept" size="1"
														name="selDept" style="width: 60px" tabindex="1"
														maxlength="10">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>

													<td width="20%"><font>Arrival&nbsp;</font> <select
														id="selArival" size="1" name="selArival"
														style="width: 60px" tabindex="1" maxlength="10">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td width="%15"><font>Via 1&nbsp;</font> <select
														id="selVia1" size="1" name="selVia1" style="width: 60px"
														tabindex="1" maxlength="5">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>

													<td width="15%"><font>Via 2&nbsp;</font><select
														id="selVia2" size="1" name="selVia2" style="width: 60px"
														tabindex="1" maxlength="5">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td width="15%"><font>Via 3&nbsp;</font> <select
														id="selVia3" size="1" name="selVia3" style="width: 60px"
														tabindex="1" maxlength="5">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>

													<td width="15%"><font>Via 4&nbsp;</font> <select
														id="selVia4" size="1" name="selVia4" style="width: 60px"
														tabindex="1" maxlength="5">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
												</tr>
											</table>
										</td>
									</tr>
									 --%>

									<tr>
										<td>
											<table width="55%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top"><span id="spnSSR"
														class="FormBackGround"></span>
													</td>
													<td valign="bottom"><font class="mandatory"><b>*</b>
													</font></td>
												</tr>
											</table></td>
										<td></td>
										<td>
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td colspan="3" rowspan="7" valign="top"></td>
												</tr>
											</table></td>
									</tr>
									
									<tr>
										<td>
											<table width="55%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top"><span id="spnPFSStatus"
														class="FormBackGround"></span></td>
													<td valign="bottom"><font class="mandatory">
													</font></td>
												</tr>
											</table>
										</td>
										<td></td>
										<td>
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td colspan="3" rowspan="7" valign="top"></td>
												</tr>
											</table>
										</td>
									</tr>

								</table>
							</td>
						</tr>

						<%--
												<tr>
							<td width="100%">
								<table width="60%" border="0" cellpadding="0" cellspacing="2">

									<tr>
										<td width="20%" align="left"><font>Departure Date
												From (Local)</font>
										</td>
										<td width="10%"><input name="txtFromDate" type="text"
											id="txtFromDate" size="10" style="width: 75px;"
											maxlength="10" onblur="dateChk('txtFromDate')" tabindex="1">
										</td>
										<td width="12%" align="left" valign="bottom">&nbsp;<a
											href="javascript:void(0)"
											onclick="LoadCalendar(0,event); return false;"
											title="Date From" tabindex="2"><img
												SRC="../images/Calendar_no_cache.gif" border="0" border="0">
										</a><font class="mandatory">&nbsp;*</font>
										</td>
										<td width="20%"><font>Departure Date To (Local) </font>
										</td>
										<td width="14%"><input name="txtToDate" type="text"
											id="txtToDate" size="10" style="width: 75px;" maxlength="10"
											onblur="dateChk('txtToDate')" tabindex="3">
										</td>
										<td width="24%" valign="bottom">&nbsp;<a
											href="javascript:void(0)"
											onclick="LoadCalendar(1,event); return false;"
											title="Date From"><img
												SRC="../images/Calendar_no_cache.gif" border="0" border="0"
												tabindex="4"> </a><font class="mandatory">&nbsp;*</font>
										</td>

									</tr>
								</table></td>
						</tr>
						<tr>
							<td><table width="60%" border="0" cellpadding="0"
									cellspacing="2">
									</tr>
									<tr>
										<td width="100%">
											<table width="100%" border="0" cellpadding="0"
												cellspacing="2">

												<td width="18%"><font>Origin</font>
												</td>
												<td width="20%"><select id="selFromAirport" size="1"
													name="selFromAirport" style="width: 75px" tabindex="5"
													maxlength="10">
														<option value=""></option>
														<c:out value="${requestScope.reqStationList}"
															escapeXml="false" />
												</select>
												</td>
												<td width="26%" align="right"><font>Destination&nbsp;</font>
												</td>
												<td width="18%"><select id="selToAirport" size="1"
													name="selToAirport" style="width: 75px" tabindex="6"
													maxlength="10">
														<option value=""></option>
														<c:out value="${requestScope.reqStationList}"
															escapeXml="false" />
												</select>
												</td>
												<td width="10%"></td>
												<td width="8%"></td>
											</table></td>
									</tr>
								</table></td>
						</tr>

						<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="52%"><font>Flight
												No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input
											name="txtFlightNo" type="text" id="txtFlightNo" size="10"
											style="width: 75px;" maxlength="7" tabindex="7">
										</td>

									</tr>
								</table></td>


						</tr>
						<tr>
							<td width="100%">
								<table width="80%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="14%"><font>Service Category </font></td>
										<td width="35%"><select name="serviceCategory"
											id="serviceCategory" size="1" tabindex="8"
											style="width: 120px" title="Service Category">
												<option value="Hala">Hala</option>
												<c:out value="${requestScope.reqServiceCategories}"
													escapeXml="false" />
										</select></td>
										<td width="51%"></td>
									</tr>
								</table></td>
						</tr>
						<tr>
						<tr>
							<td>
								<table id="carrierCode" style="display: none;">
									<tr>
										<td><font class="fntBold"> Carrier Code </font></td>
									</tr>

									<tr>
										<td>
											<table>
												<tr>
													<td valign="top"><span id="spn3"></span>
													</td>
													<td valign="bottom"><font class="mandatory"> <b>*</b>
													</font>
													</td>
												</tr>
											</table></td>
									</tr>
								</table></td>
						</tr>
						 --%>


						<tr>
							<td></td>
						</tr>

						<tr>
							<td><font class="fntBold">Output Option</font></td>
						</tr>
						<tr>
							<td><table width="100%" border="0" cellpadding="0"
									cellspacing="2">
									<tr>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="HTML"
											class="noBorder" checked tabindex="9"><font>HTML</font>
										</td>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="PDF"
											class="noBorder"><font>PDF</font></td>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="EXCEL"
											class="noBorder"><font>EXCEL</font></td>
										<td><input type="radio" name="radReportOption"
											id="radReportOption" value="CSV" class="noBorder"><font>CSV</font>
										</td>
									</tr>
								</table> <c:out value="${requestScope.rptFormatOption}"
									escapeXml="false" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width=90%><input type="button" id="btnClose"
								name="btnClose" value="Close" class="Button"
								onclick="closeClick()" tabindex="10"></td>
							<td width=10% align="right"><input type="button"
								id="btnView" name="btnView" value="View" class="Button"
								onclick="viewClick()" tabindex="11"></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode"> <input
			type="hidden" name="hdnReportView" id="hdnReportView"> <input
			type="hidden" name="fromDate" id="fromDate"> <input
			type="hidden" name="toDate" id="toDate"> <input type="hidden"
			name="origin" id="origin"> <input type="hidden"
			name="chargeId" id="chargeId"> <input type="hidden"
			name="hdnSsrCodes" id="hdnSsrCodes"> <input type="hidden"
			name="hdnAllSSRByCategory" id="hdnAllSSRByCategory"
			value="<c:out value='${requestScope.reqSsrAndCategoryList}' escapeXml='false' />">
		<input type="hidden" name="hdnSegments" id="hdnSegments">
		<input type="hidden" name="hdnPfsStatus" id="hdnPfsStatus"> 


	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<%@ include file="../common/IncludeLoadMsg.jsp"%>
<script
	src="../js/reports/BookedSSRDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
	<c:out value="${requestScope.reqSSRCodeList}" escapeXml="false" />
	if (ssrs) {
		ssrs.height = '125px';
		ssrs.width = '160px';
		ssrs.drawListBox();
	}
	<c:out value="${requestScope.reqPfsStatusList}" escapeXml="false" />
	   pfsStatuss.height = '125px';
	   pfsStatuss.width = '160px';				
	   pfsStatuss.drawListBox();
</script>
</html>