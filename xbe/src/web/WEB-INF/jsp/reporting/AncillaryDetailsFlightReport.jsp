<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/PrivilegeConstants.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>		
  </head>
  
  <body oncontextmenu="return true" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="auto">
					
		 <%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->	 
  		<table width="100%" height="100%" align="center" border="0" cellpadding="0" cellspacing="0">					
				
			<tr>
				<td align="center">
					<iframe src="showFile!ancillaryDetailsFlightGrid.action" id="grdFlights" name="grdFlights" height="100%" width="100%" frameborder="no" scrolling="no"></iframe> 
			</td>
			
		</tr>	
					
			
		</table>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->		
		<%@ include file="../common/IncludeLoadMsg.jsp"%>
		<script type="text/javascript">	
			var arrError = new Array();
			var flightData = new Array();
				
			var Maxcontime = "<c:out value="${requestScope.reqMaxConTime}" escapeXml="false" />";							
			var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
			var defaultOperationType = "<c:out value="${requestScope.reqDefaultOperationType}" escapeXml="false" />";
			var defaultStatus = "<c:out value="${requestScope.reqFlightDefStatus}" escapeXml="false" />";		
	    	<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			<c:out value="${requestScope.reqFlightData}" escapeXml="false" />
			var connLimit="";
			var minConnLimit="";
			<c:out value="${sessionScope.sesConnectionLimit}" escapeXml="false" />
			<c:out value="${sessionScope.sesMinConnectionLimit}" escapeXml="false" />
			var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";
			var strMsg = "<c:out value="${requestScope.reqMessage}"/>";
			var isPrintPaxDet = "<c:out value="${requestScope.reqPaxDet}" escapeXml="false" />";
			var carrierCodeOptions =  "<c:out value="${requestScope.carrierCodeListOptions}" escapeXml="false" />";
		</script>		
  </body>
</html>