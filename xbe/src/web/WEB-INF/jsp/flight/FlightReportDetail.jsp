<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/Directives.jsp" %>
<%@ include file='../v2/common/inc_PgHD.jsp' %>

<script type="text/javascript">

	function loadReservation(pnr, orginatedPnr) {
		if (window.opener) {
			window.close();
			var interline = false;
			if (orginatedPnr != "-1") {
				window.opener.top.blnInterLine = true;
				interline = true;
			}
			window.opener.top.loadNewModifyBooking(pnr, interline);
		} else {
			alert("Please login with XBE to load the reservation");
		}
	}
</script>
<c:out value='${requestScope.reqManifestHtml}' escapeXml='false' />
<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
<script src="../js/flight/FlightReportDetail.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	