<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View GDS Messages</title>
    <meta http-equiv='pragma' content='no-cache'>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta HTTP-EQUIV="expires" CONTENT="-1">
	<LINK rel="stylesheet" type="text/css" href="../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../css/Grid_no_cache.css">	
	<LINK rel="stylesheet" type="text/css" href="../css/Cbo_Style_no_cache.css">
	<script src="../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
	<script src="../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
	<script src="../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script  src="../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>			
  </head>
  
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no"
  		onload="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>' ,'<c:out value="${requestScope.warningmessage}"/>')" >
  <form method="post" action="showGdsMessages.action" id="frm111" name="frm111">
	 <%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  			<script type="text/javascript">
				var arrData = new Array();
				var arrGDSData = new Array();
				var arrError = new Array();	
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
				<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
				<c:out value="${requestScope.reqIsExceptionOccured}" escapeXml="false" />
				var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
			</script>
			
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="Header">View GDS Messages</font></td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>

				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search GDS Messages<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="2" cellspacing="2" cellpadding='2' ID="Table2">
										<tr valign="top">
											<td><font>GDS Name</font></td>
											<td>
												<select id='selGDSType' name='selGDSType' size='1' tabindex="1">
													<option value=""></option>
													<c:out value="${requestScope.gdsNamesList}" escapeXml="false" />
												</select>
											</td>
											<td><font>Message Type</font></td>
											<td>
												<select id='selMSGType' name='selMSGType' size='1' tabindex="2">
													<option value=""></option>
													<c:out value="${requestScope.gdsMsgTypeList}" escapeXml="false" />
												</select>
												
											</td>
											<td><font>Process Status</font></td>
											<td>
												<select id='selProcessStatus' name='selProcessStatus' size='1' tabindex="3">
													<option value=""></option>
													<c:out value="${requestScope.gdsProcessStatusList}" escapeXml="false" />
												</select>
											</td>
											<td><font>Communication Reference</font></td>
											<td>
												<input type="text" id="txtCommReference" name="txtCommReference" size="15" tabindex="4">
											</td>
											<td>&nbsp;</td>
										</tr>

										<tr valign="top">
											<td><font>From Date</font></td>
											<td>
												<table width="75%" cellpadding="0" cellspacing="0">
													<tr>
														<td><input name="txtFromDate" type="text" id="txtFromDate" size="10"  maxlength="10" onBlur="dateValidation('txtFromDate')" onchange="dataChanged()" tabindex="5"></td>
														<td width="8%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a></td>
													</tr>
												</table>
											</td>
											<td><font>To Date</font>
											</td>
											<td>
												<table width="75%" cellpadding="0" cellspacing="0">
													<tr>
														<td><input	name="txtToDate" type="text" id="txtToDate" size="10" maxlength="10" onBlur="dateValidation('txtToDate')" onchange="dataChanged()" tabindex="6"></td>
														<td align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../images/Calendar_no_cache.gif" border="0" border="0"></a></td>
													</tr>
												</table>
											</td>
											<td><font>Message Broker Reference</font></td>
											<td><input type="text" id="txtMsgBrokerRef" name="txtMsgBrokerRef" size="15" tabindex="7"></td>
											<td><font>GDS PNR</font></td>
											<td><input type="text" id="txtGDSPnr" name="txtGDSPnr" size="15" tabindex="8"></td>
											<td>&nbsp;</td>
										</tr>
										<tr valign="top">
											<td><font>Request XML</font></td>
											<td colspan="3">
												<input type="text" id="txtReqXml" name="txtReqXml" size="50" tabindex="9"></td>
											<td><font>Response XML</font></td>
											<td colspan="3">
												<input type="text" id="txtResXml" name="txtResXml" size="50" tabindex="10">
											</td>
											<td align="right" valign="bottom">
												<input name="btnSearch" type="button" class="Button" id="btnSearch" onClick="searchData()" value="Search" tabindex="11">
											</td>
										</tr>	
					  	</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>GDS Messages<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
								<span id="spnReprintGenInv"></span>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						GDS Message Details
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<br>
							  <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
								<tr><td colspan="4">
										 <table width="100%" border="0" cellpadding="2" cellpadding="0" cellspacing="0">
											<tr>
												<td><font>Message Type</font></td>
												<td><input type="text" id="frmMessageType" name="frmMessageType" size="15" readonly></td>
												<td><font>Received Date</td>
												<td><input type="text" id="frmReceivedDate" size="10" name="frmReceivedDate" maxlength="10" readonly></td>
												<td><font>GDS Name</font></td>
												<td><input type="text" id="frmGDSName" name="frmGDSName" size="15" readonly></td>
											</tr>
											<tr>
												<td><font>Status History</font></td>
												<td><input type="text" id="frmStatusHistory" name="frmStatusHistory" size="15" readonly></td>
												<td><font>AccelAero Object ID</font></td>
												<td colspan="3"><input type="text" id="frmAAObjectID" name="frmAAObjectID" size="75" readonly></td>
											</tr>
											<tr>
												<td><font>Process Status</font></td>
												<td><input type="text" id="frmProcessStatus" name="frmProcessStatus" size="15" readonly></td>
												<td><font>Process Description</font></td>
												<td colspan="3" rowspan="2"><textarea rows="2" id="frmProcessDes" name="frmProcessDes" cols="50" readonly></textarea></td>
											</tr>
											<tr>
												<td><font>Communication Reference</font></td>
												<td><input type="text" id="frmComRef" name="frmComRef" size="15" readonly></td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><font>Oreginator Address</font></td>
												<td><input type="text" id="frmOreginator" name="frmOreginator" size="15" readonly></td>
												<td><font>GDS PNR</font></td>
												<td><input type="text" id="frmGdsPnr" name="frmGdsPnr" size="15" readonly></td>
												<td><font>Message Broker Reference</font></td>
												<td><input type="text" id="frmMsgBrokerRef" name="frmMsgBrokerRef" size="15" readonly></td>
											</tr>
											<tr>
												<td><font>Responder Address</font></td>
												<td><input type="text" id="frmResponder" name="frmResponder" size="15" readonly></td>
												<td><font>Detail Messages</font></td>
												<td colspan="3">
													<input type="button" style="width:110px" id="btnReqXML" class="Button" value="Requeest XML"  onclick="showRequestXML();">
													<input type="button" style="width:110px" id="btnResXML" class="Button" value="Response XML"  onclick="showResponseXML();">
												</td>
											</tr>

										</table>
									</td>
								</tr>
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
									<tr>
									  <td style="height:10px;" colspan="4" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="2">
													<input tabindex="12" type="button" id="btnClose" class="Button" value="Close"  onclick="top.LoadHome();">	
												</td>
											</tr>
										</table>
									   </td>
									</tr>
							  </table>
							
						  	<%@ include file="../common/IncludeFormBottom.jsp"%>  
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
  		<%@ include file="../common/IncludeShortCuts.jsp"%>  	
		<%@ include file="../common/IncludeLoadMsg.jsp"%>
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnGdsMsgID" id="hdnGdsMsgID"/>

	
	</form>
		<script type="text/javascript">
			<c:out value="${requestScope.reqSITAData}" escapeXml="false" />	
		</script>
				<script type="text/javascript">
	  	<!--
	
		  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
		  	function ClearProgressbar(){
		  			if (typeof(objDG) == "object"){
				  		if (objDG.loaded){				  			
				  			clearTimeout(objProgressCheck);
				  			//top[2].HideProgress();
				  			//showAlertResponceMessage();
							HideProgress();
				  		}
				  	}
		  	}
		 		  	 
		  	//-->
	  </script>
  </body>
	<script src="../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>	
	<script src="../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/gdsMessages/GdsMessagesValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../js/gdsMessages/GdsMessagesGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  
  
</html>