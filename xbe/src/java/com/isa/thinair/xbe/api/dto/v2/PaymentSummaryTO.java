package com.isa.thinair.xbe.api.dto.v2;

public class PaymentSummaryTO {
	private String currency;

	private String totalFare;

	private String totalDiscount;

	private boolean creditDiscount = false;

	private String totalTaxSurCharges;

	private String modificationCharges;

	private String handlingCharges;

	private String ticketPrice;

	private String paid;

	private String balanceToPay;

	private String balanceToPaySelcur;

	private String selectedCurrency;

	private String totalAgentCommission;

	private String pendingAgentCommission;
	
	private String countryCurrencyCode;

	private String totalGOQUOAmount;
	
	private String totalServiceTax;

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the totalFare
	 */
	public String getTotalFare() {
		return totalFare;
	}

	/**
	 * @param totalFare
	 *            the totalFare to set
	 */
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	public String getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(String totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public boolean isCreditDiscount() {
		return creditDiscount;
	}

	public void setCreditDiscount(boolean creditDiscount) {
		this.creditDiscount = creditDiscount;
	}

	/**
	 * @return the totalTaxSurCharges
	 */
	public String getTotalTaxSurCharges() {
		return totalTaxSurCharges;
	}

	/**
	 * @param totalTaxSurCharges
	 *            the totalTaxSurCharges to set
	 */
	public void setTotalTaxSurCharges(String totalTaxSurCharges) {
		this.totalTaxSurCharges = totalTaxSurCharges;
	}

	/**
	 * @return the modificationCharges
	 */
	public String getModificationCharges() {
		return modificationCharges;
	}

	/**
	 * @param modificationCharges
	 *            the modificationCharges to set
	 */
	public void setModificationCharges(String modificationCharges) {
		this.modificationCharges = modificationCharges;
	}

	/**
	 * @return the handlingCharges
	 */
	public String getHandlingCharges() {
		return handlingCharges;
	}

	/**
	 * @param handlingCharges
	 *            the handlingCharges to set
	 */
	public void setHandlingCharges(String handlingCharges) {
		this.handlingCharges = handlingCharges;
	}

	/**
	 * @return the ticketPrice
	 */
	public String getTicketPrice() {
		return ticketPrice;
	}

	/**
	 * @param ticketPrice
	 *            the ticketPrice to set
	 */
	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	/**
	 * @return the paid
	 */
	public String getPaid() {
		return paid;
	}

	/**
	 * @param paid
	 *            the paid to set
	 */
	public void setPaid(String paid) {
		this.paid = paid;
	}

	/**
	 * @return the balanceToPay
	 */
	public String getBalanceToPay() {
		return balanceToPay;
	}

	/**
	 * @param balanceToPay
	 *            the balanceToPay to set
	 */
	public void setBalanceToPay(String balanceToPay) {
		this.balanceToPay = balanceToPay;
	}

	/**
	 * @return the balanceToPay
	 */
	public String getBalanceToPaySelcur() {
		return balanceToPaySelcur;
	}

	/**
	 * @param balanceToPaySelcur
	 *            the balanceToPaySelcur to set
	 */
	public void setBalanceToPaySelcur(String balanceToPaySelcur) {
		this.balanceToPaySelcur = balanceToPaySelcur;
	}

	/**
	 * @return the selectedCurrency
	 */
	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	/**
	 * @param selectedCurrency
	 *            the selectedCurrency to set
	 */
	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getTotalAgentCommission() {
		return totalAgentCommission;
	}

	public void setTotalAgentCommission(String totalAgentCommission) {
		this.totalAgentCommission = totalAgentCommission;
	}

	public String getPendingAgentCommission() {
		return pendingAgentCommission;
	}

	public void setPendingAgentCommission(String pendingAgentCommission) {
		this.pendingAgentCommission = pendingAgentCommission;
	}

	public String getCountryCurrencyCode() {
		return countryCurrencyCode;
	}

	public void setCountryCurrencyCode(String countryCurrencyCode) {
		this.countryCurrencyCode = countryCurrencyCode;
	}

	public String getTotalGOQUOAmount() {
		return totalGOQUOAmount;
	}

	public void setTotalGOQUOAmount(String totalGOQUOAmount) {
		this.totalGOQUOAmount = totalGOQUOAmount;
	}

	public String getTotalServiceTax() {
		return totalServiceTax;
	}

	public void setTotalServiceTax(String totalServiceTax) {
		this.totalServiceTax = totalServiceTax;
	}
	
}
