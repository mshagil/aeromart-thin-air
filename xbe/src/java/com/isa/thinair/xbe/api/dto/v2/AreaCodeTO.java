package com.isa.thinair.xbe.api.dto.v2;

/**
 * TO to keep phone area code details
 * 
 * @author Thushara
 * 
 */

public class AreaCodeTO {

	private String areaCode;
	private String type;
	private String status;

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
