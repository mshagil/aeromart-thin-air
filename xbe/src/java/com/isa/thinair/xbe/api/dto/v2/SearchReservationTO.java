package com.isa.thinair.xbe.api.dto.v2;

public class SearchReservationTO {
	private String pnr;

	private String firstName;

	private String lastName;

	private String fromAirport;

	private String depatureDate;

	private String phoneCntry;

	private String phoneArea;

	private String phoneNo;

	private String toAirport;

	private String returnDate;

	private String flightNo;

	private String creditCardNo;

	private String cardExpiry;

	private String authCode;

	private String bookingType;

	private String passport;

	private String exactMatch;

	private String loadZeroBalance;

	private String searchOtheCarrier;

	private String searchSystem;

	/** Holds whether to load interline reservations (in addition to normal reservations) */
	private boolean includeInterlineReservations;

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr.toUpperCase();
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the fromAirport
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            the fromAirport to set
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return the depatureDate
	 */
	public String getDepatureDate() {
		return depatureDate;
	}

	/**
	 * @param depatureDate
	 *            the depatureDate to set
	 */
	public void setDepatureDate(String depatureDate) {
		this.depatureDate = depatureDate;
	}

	/**
	 * @return the phoneCntry
	 */
	public String getPhoneCntry() {
		return phoneCntry;
	}

	/**
	 * @param phoneCntry
	 *            the phoneCntry to set
	 */
	public void setPhoneCntry(String phoneCntry) {
		this.phoneCntry = phoneCntry;
	}

	/**
	 * @return the phoneArea
	 */
	public String getPhoneArea() {
		return phoneArea;
	}

	/**
	 * @param phoneArea
	 *            the phoneArea to set
	 */
	public void setPhoneArea(String phoneArea) {
		this.phoneArea = phoneArea;
	}

	/**
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo
	 *            the phoneNo to set
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return the toAirport
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            the toAirport to set
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	/**
	 * @return the returnDate
	 */
	public String getReturnDate() {
		return returnDate;
	}

	/**
	 * @param returnDate
	 *            the returnDate to set
	 */
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	/**
	 * @return the flightNo
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 *            the flightNo to set
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return the creditCardNo
	 */
	public String getCreditCardNo() {
		return creditCardNo;
	}

	/**
	 * @param creditCardNo
	 *            the creditCardNo to set
	 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}

	/**
	 * @return the cardExpiry
	 */
	public String getCardExpiry() {
		return cardExpiry;
	}

	/**
	 * @param cardExpiry
	 *            the cardExpiry to set
	 */
	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	/**
	 * @return the authCode
	 */
	public String getAuthCode() {
		return authCode;
	}

	/**
	 * @param authCode
	 *            the authCode to set
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	/**
	 * @return the passport
	 */
	public String getPassport() {
		return passport;
	}

	/**
	 * @param passport
	 *            the passport to set
	 */
	public void setPassport(String passport) {
		this.passport = passport;
	}

	/**
	 * @return the exactMatch
	 */
	public String getExactMatch() {
		return exactMatch;
	}

	/**
	 * @param exactMatch
	 *            the exactMatch to set
	 */
	public void setExactMatch(String exactMatch) {
		this.exactMatch = exactMatch;
	}

	public boolean isIncludeInterlineReservations() {
		return includeInterlineReservations;
	}

	public void setIncludeInterlineReservations(boolean includeInterlineReservations) {
		this.includeInterlineReservations = includeInterlineReservations;
	}

	/**
	 * @return the loadZeroBalance
	 */
	public String getLoadZeroBalance() {
		return loadZeroBalance;
	}

	/**
	 * @param loadZeroBalance
	 *            the loadZeroBalance to set
	 */
	public void setLoadZeroBalance(String loadZeroBalance) {
		this.loadZeroBalance = loadZeroBalance;
	}

	/**
	 * @return the searchOtheCarrier
	 */
	public String getSearchOtheCarrier() {
		return searchOtheCarrier;
	}

	/**
	 * @param searchOtheCarrier
	 *            the searchOtheCarrier to set
	 */
	public void setSearchOtheCarrier(String searchOtheCarrier) {
		this.searchOtheCarrier = searchOtheCarrier;
	}

	/**
	 * @return the searchSystem
	 */
	public String getSearchSystem() {
		return searchSystem;
	}

	/**
	 * Method to set the searching system. Either own or all other systems
	 * 
	 * @param searchSystem
	 *            the searchSystem to set
	 */
	public void setSearchSystem(String searchSystem) {
		this.searchSystem = searchSystem;
	}

}
