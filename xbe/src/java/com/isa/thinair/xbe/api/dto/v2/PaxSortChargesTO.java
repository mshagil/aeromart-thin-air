package com.isa.thinair.xbe.api.dto.v2;

import java.util.ArrayList;

public class PaxSortChargesTO {

	private String segmentCode;

	private ArrayList<ChargesListTO> chargeList;

	private Integer sequence = 0;

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public ArrayList<ChargesListTO> getChargeList() {
		return chargeList;
	}

	public void setChargeList(ArrayList<ChargesListTO> chargeList) {
		this.chargeList = chargeList;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

}
