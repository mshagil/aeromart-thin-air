package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

public class ItineraryPaxTO {
	private String type;

	private Collection<PaxTO> paxCollection;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the paxCollection
	 */
	public Collection<PaxTO> getPaxCollection() {
		return paxCollection;
	}

	/**
	 * @param paxCollection
	 *            the paxCollection to set
	 */
	public void setPaxCollection(Collection<PaxTO> paxCollection) {
		this.paxCollection = paxCollection;
	}
}
