package com.isa.thinair.xbe.api.dto.v2;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class InfantPaxTO implements IDisplayPax {
	private String displayInfantTitle;

	private String displayInfantType;

	private String displayInfantFirstName;

	private String displayInfantLastName;

	private String displayInfantNationality;

	private String displayInfantDOB;

	private String displayInfantTravellingWith;

	private String displayInfantSSR;

	private String displayInfantSSRCode;

	private String displayInfantSSRInformation;

	private String displayInfantSelect;

	private String displayPaxTravelReference;

	private String displayPaxCategory;

	private String displayPnrPaxCatFOIDNumber;

	private String displayPnrPaxCatFOIDExpiry;

	private String displayPnrPaxCatFOIDPlace;

	private String displayPnrPaxCatFOIDType;

	private String displayPnrPaxPlaceOfBirth;

	private String displayTravelDocType;

	private String displayVisaDocNumber;

	private String displayVisaDocPlaceOfIssue;

	private String displayVisaDocIssueDate;

	private String displayVisaApplicableCountry;

	private boolean nameEditable;

	private String displayPaxSequence;

	private String displayETicketMask;

	private String displayInfantFirstNameOl;

	private String displayInfantLastNameOl;

	private String displayNameTranslationLanguage;

	private boolean alertAutoCancellation;
	private String displaypnrPaxArrivalFlightNumber;

	private String displaypnrPaxFltArrivalDate;

	private String displaypnrPaxArrivalTime;

	private String displaypnrPaxDepartureFlightNumber;

	private String displaypnrPaxFltDepartureDate;

	private String displaypnrPaxDepartureTime;

	private String displayPnrPaxGroupId;

	private String displayInfantTitleOl;

	private String displayNationalIDNo;

	private String displayInfantMCO;
	
	private String displayInfantCharg;
	
	private String displayInfantPay;

	private String displayInfantBal;

	private String displayInfantAcco;

	public InfantPaxTO() {
	}

	private InfantPaxTO(int zeroIndexPaxSequence, boolean showPaxEticket, boolean allowViewMCO, boolean hasMCO, boolean viewAccntInfo, LCCClientReservationPax lccClientReservationPax, boolean isInfantPaymentSeparated) {
		
		if(isInfantPaymentSeparated){
			String totalCharges = AccelAeroCalculator.formatAsDecimal(lccClientReservationPax.getTotalPrice());
			String availableBalance = AccelAeroCalculator.formatAsDecimal(lccClientReservationPax.getTotalAvailableBalance());
			String totalPaid = AccelAeroCalculator.formatAsDecimal(lccClientReservationPax.getTotalPaidAmount());
			
			setDisplayInfantCharg(totalCharges);
			setDisplayInfantPay(totalPaid);
			setDisplayInfantBal(availableBalance);	
		}else{
			setDisplayInfantCharg("-");
			setDisplayInfantPay("-");
			setDisplayInfantBal("-");	
		}
			
		setDisplayPaxType("BABY");
		setDisplayPaxSSR("<input type='button' id='btndisplayInfantSSRI" + zeroIndexPaxSequence
				+ "' name='btndisplayInfantSSR' value='SSR' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnSSROnClick({row:"
				+ zeroIndexPaxSequence + ", type:\"INF\"})'>");
		if (showPaxEticket) {
			setDisplayETicketMask("<input type='button' id='btndisplayETckMask" + zeroIndexPaxSequence
					+ "' name='btndisplayETckMask' value='E-TCK' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.loadGroupPaxStatusUpdate()'>");
		} else {
			setDisplayETicketMask("<input type='button' id='btndisplayETckMask" + zeroIndexPaxSequence
					+ "' name='btndisplayETckMask' value='E-TCK' class='ui-state-default ui-corner-all' disabled='disabled'>");
		}
		
		if (viewAccntInfo && isInfantPaymentSeparated) {
			setDisplayInfantAcco("<input type='button' id='btndisplayInfantAcco" + zeroIndexPaxSequence
					+ "' name='btndisplayInfantAcco' value='ACC' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnAccoOnClick({row:"
					+ zeroIndexPaxSequence + ", type:\"INF\"})'>");

		} else {
			setDisplayInfantAcco("<input type='button' id='btndisplayInfantAcco" + zeroIndexPaxSequence
					+ "' name='btndisplayInfantAcco' value='ACC' class='ui-state-default ui-corner-all' disabled='disabled'>");
		}

		if (allowViewMCO) {
			if (hasMCO) {
				setDisplayInfantMCO("<input type='button' id='btndisplayInfantMCO" + zeroIndexPaxSequence
					+ "' name='btndisplayInfantMCO' value='MCO' class='ui-state-default ui-corner-all' onclick='UI_tabBookingInfo.btnMCOOnClick({row:"
					+ zeroIndexPaxSequence + ", type:\"INF\"})'>");
			} else {
				setDisplayInfantMCO("<input type='button' id='btndisplayInfantMCO" + zeroIndexPaxSequence
						+ "' name='btndisplayInfantMCO' value='MCO' class='ui-state-disabled ui-corner-all' disabled>");
			}
		}
	}

	public static InfantPaxTO getInfantDisplayInstance(int zeroIndexPaxSequence, boolean showPaxEticket, boolean allowViewMCO,
			boolean hasMCO, boolean viewAccntInfo, LCCClientReservationPax lccClientReservationPax, boolean isInfantPaymentSeparated) {
		return new InfantPaxTO(zeroIndexPaxSequence, showPaxEticket, allowViewMCO, hasMCO, viewAccntInfo, lccClientReservationPax, isInfantPaymentSeparated);
	}

	public String getDisplayNationalIDNo() {
		return displayNationalIDNo;
	}

	@Override
	public void setDisplayNationalIDNo(String displayNationalIDNo) {
		this.displayNationalIDNo = displayNationalIDNo;
	}

	/**
	 * @return the displayInfantType
	 */
	public String getDisplayInfantType() {
		return displayInfantType;
	}

	@Override
	public void setDisplayPaxType(String displayInfantType) {
		setDisplayInfantType(displayInfantType);
	}

	/**
	 * @param displayInfantType
	 *            the displayInfantType to set
	 */
	public void setDisplayInfantType(String displayInfantType) {
		this.displayInfantType = displayInfantType;
	}

	/**
	 * @return the displayInfantFirstName
	 */
	public String getDisplayInfantFirstName() {
		return displayInfantFirstName;
	}

	@Override
	public void setDisplayPaxFirstName(String displayInfantFirstName) {
		setDisplayInfantFirstName(displayInfantFirstName);
	}

	/**
	 * @param displayInfantFirstName
	 *            the displayInfantFirstName to set
	 */
	public void setDisplayInfantFirstName(String displayInfantFirstName) {
		this.displayInfantFirstName = displayInfantFirstName;
	}

	/**
	 * @return the displayInfantLastName
	 */
	public String getDisplayInfantLastName() {
		return displayInfantLastName;
	}

	@Override
	public void setDisplayPaxLastName(String displayInfantLastName) {
		setDisplayInfantLastName(displayInfantLastName);
	}

	/**
	 * @param displayInfantLastName
	 *            the displayInfantLastName to set
	 */
	public void setDisplayInfantLastName(String displayInfantLastName) {
		this.displayInfantLastName = displayInfantLastName;
	}

	/**
	 * @return the displayInfantNationality
	 */
	public String getDisplayInfantNationality() {
		return displayInfantNationality;
	}

	@Override
	public void setDisplayPaxNationality(String displayInfantNationality) {
		setDisplayInfantNationality(displayInfantNationality);
	}

	/**
	 * @param displayInfantNationality
	 *            the displayInfantNationality to set
	 */
	public void setDisplayInfantNationality(String displayInfantNationality) {
		this.displayInfantNationality = displayInfantNationality;
	}

	/**
	 * @return the displayInfantDOB
	 */
	public String getDisplayInfantDOB() {
		return displayInfantDOB;
	}

	@Override
	public void setDisplayPaxDOB(String displayInfantDOB) {
		setDisplayInfantDOB(displayInfantDOB);
	}

	/**
	 * @param displayInfantDOB
	 *            the displayInfantDOB to set
	 */
	public void setDisplayInfantDOB(String displayInfantDOB) {
		try {
			if (displayInfantDOB != null && !"".equals(displayInfantDOB.trim())) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				displayInfantDOB = format.format(format.parse(displayInfantDOB));
			}
		} catch (ParseException e) {
			// do nothing
		}
		this.displayInfantDOB = displayInfantDOB;
	}

	/**
	 * @return the displayInfantTravellingWith
	 */
	public String getDisplayInfantTravellingWith() {
		return displayInfantTravellingWith;
	}

	@Override
	public void setDisplayPaxTravellingWith(String displayInfantTravellingWith) {
		setDisplayInfantTravellingWith(displayInfantTravellingWith);
	}

	/**
	 * @param displayInfantTravellingWith
	 *            the displayInfantTravellingWith to set
	 */
	public void setDisplayInfantTravellingWith(String displayInfantTravellingWith) {
		this.displayInfantTravellingWith = displayInfantTravellingWith;
	}

	/**
	 * @return the displayInfantSSR
	 */
	public String getDisplayInfantSSR() {
		return displayInfantSSR;
	}

	@Override
	public void setDisplayPaxSSR(String displayInfantSSR) {
		setDisplayInfantSSR(displayInfantSSR);
	}

	/**
	 * @param displayInfantSSR
	 *            the displayInfantSSR to set
	 */
	public void setDisplayInfantSSR(String displayInfantSSR) {
		this.displayInfantSSR = displayInfantSSR;
	}

	/**
	 * @return the displayInfantSSRCode
	 */
	public String getDisplayInfantSSRCode() {
		return displayInfantSSRCode;
	}

	@Override
	public void setDisplayPaxSSRCode(String displayInfantSSRCode) {
		setDisplayInfantSSRCode(displayInfantSSRCode);
	}

	/**
	 * @param displayInfantSSRCode
	 *            the displayInfantSSRCode to set
	 */
	public void setDisplayInfantSSRCode(String displayInfantSSRCode) {
		this.displayInfantSSRCode = displayInfantSSRCode;
	}

	/**
	 * @return the displayInfantSSRInformation
	 */
	public String getDisplayInfantSSRInformation() {
		return displayInfantSSRInformation;
	}

	@Override
	public void setDisplayPaxSSRInformation(String displayInfantSSRInformation) {
		setDisplayInfantSSRInformation(displayInfantSSRInformation);
	}

	/**
	 * @param displayInfantSSRInformation
	 *            the displayInfantSSRInformation to set
	 */
	public void setDisplayInfantSSRInformation(String displayInfantSSRInformation) {
		this.displayInfantSSRInformation = displayInfantSSRInformation;
	}

	/**
	 * @return the displayInfantSelect
	 */
	public String getDisplayInfantSelect() {
		return displayInfantSelect;
	}

	@Override
	public void setDisplayPaxSelect(String displayInfantSelect) {
		setDisplayInfantSelect(displayInfantSelect);
	}

	/**
	 * @param displayInfantSelect
	 *            the displayInfantSelect to set
	 */
	public void setDisplayInfantSelect(String displayInfantSelect) {
		this.displayInfantSelect = displayInfantSelect;
	}

	/**
	 * @return the displayPaxTravelReference
	 */
	public String getDisplayPaxTravelReference() {
		return displayPaxTravelReference;
	}

	/**
	 * @param displayPaxTravelReference
	 *            the displayPaxTravelReference to set
	 */
	@Override
	public void setDisplayPaxTravelReference(String displayPaxTravelReference) {
		this.displayPaxTravelReference = displayPaxTravelReference;
	}

	public boolean isNameEditable() {
		return nameEditable;
	}

	@Override
	public void setNameEditable(boolean nameEditable) {
		this.nameEditable = nameEditable;
	}

	public String getDisplayPaxCategory() {
		return displayPaxCategory;
	}

	@Override
	public void setDisplayPaxCategory(String displayPaxCategory) {
		this.displayPaxCategory = displayPaxCategory;
	}

	public String getDisplayPnrPaxCatFOIDNumber() {
		return displayPnrPaxCatFOIDNumber;
	}

	@Override
	public void setDisplayPnrPaxCatFOIDNumber(String displayPnrPaxCatFOIDNumber) {
		this.displayPnrPaxCatFOIDNumber = displayPnrPaxCatFOIDNumber;
	}

	public String getDisplayPnrPaxCatFOIDExpiry() {
		return displayPnrPaxCatFOIDExpiry;
	}

	@Override
	public void setDisplayPnrPaxCatFOIDExpiry(String displayPnrPaxCatFOIDExpiry) {
		try {
			if (displayPnrPaxCatFOIDExpiry != null && !"".equals(displayPnrPaxCatFOIDExpiry.trim())) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				displayPnrPaxCatFOIDExpiry = format.format(format.parse(displayPnrPaxCatFOIDExpiry));
			}
		} catch (ParseException e) {
			// do nothing
		}
		this.displayPnrPaxCatFOIDExpiry = displayPnrPaxCatFOIDExpiry;
	}

	public String getDisplayPnrPaxCatFOIDPlace() {
		return displayPnrPaxCatFOIDPlace;
	}

	@Override
	public void setDisplayPnrPaxCatFOIDPlace(String displayPnrPaxCatFOIDPlace) {
		this.displayPnrPaxCatFOIDPlace = displayPnrPaxCatFOIDPlace;
	}

	public String getDisplayPaxSequence() {
		return displayPaxSequence;
	}

	@Override
	public void setDisplayPaxSequence(String displayPaxSequence) {
		this.displayPaxSequence = displayPaxSequence;
	}

	public String getDisplayPnrPaxCatFOIDType() {
		return displayPnrPaxCatFOIDType;
	}

	@Override
	public void setDisplayPnrPaxCatFOIDType(String displayPnrPaxCatFOIDType) {
		this.displayPnrPaxCatFOIDType = displayPnrPaxCatFOIDType;
	}

	public String getDisplayETicketMask() {
		return displayETicketMask;
	}

	@Override
	public void setDisplayETicketMask(String displayETicketMask) {
		this.displayETicketMask = displayETicketMask;
	}

	public String getDisplayInfantFirstNameOl() {
		return displayInfantFirstNameOl;
	}

	@Override
	public void setDisplayPaxFirstNameOl(String displayInfantFirstNameOl) {
		setDisplayInfantFirstNameOl(displayInfantFirstNameOl);
	}

	public void setDisplayInfantFirstNameOl(String displayInfantFirstNameOl) {
		this.displayInfantFirstNameOl = displayInfantFirstNameOl;
	}

	public String getDisplayInfantLastNameOl() {
		return displayInfantLastNameOl;
	}

	@Override
	public void setDisplayPaxLastNameOl(String displayInfantLastNameOl) {
		setDisplayInfantLastNameOl(displayInfantLastNameOl);
	}

	public void setDisplayInfantLastNameOl(String displayInfantLastNameOl) {
		this.displayInfantLastNameOl = displayInfantLastNameOl;
	}

	public String getDisplayNameTranslationLanguage() {
		return displayNameTranslationLanguage;
	}

	@Override
	public void setDisplayNameTranslationLanguage(String displayNameTranslationLanguage) {
		this.displayNameTranslationLanguage = displayNameTranslationLanguage;
	}

	/**
	 * @return the alertAutoCancellation
	 */
	public boolean getAlertAutoCancellation() {
		return alertAutoCancellation;
	}

	/**
	 * @param alertAutoCancellation
	 *            the alertAutoCancellation to set
	 */
	@Override
	public void setAlertAutoCancellation(boolean alertAutoCancellation) {
		this.alertAutoCancellation = alertAutoCancellation;
	}

	public String getDisplayPnrPaxPlaceOfBirth() {
		return displayPnrPaxPlaceOfBirth;
	}

	@Override
	public void setDisplayPnrPaxPlaceOfBirth(String displayPnrPaxPlaceOfBirth) {
		this.displayPnrPaxPlaceOfBirth = displayPnrPaxPlaceOfBirth;
	}

	public String getDisplayTravelDocType() {
		return displayTravelDocType;
	}

	@Override
	public void setDisplayTravelDocType(String displayTravelDocType) {
		this.displayTravelDocType = displayTravelDocType;
	}

	public String getDisplayVisaDocNumber() {
		return displayVisaDocNumber;
	}

	@Override
	public void setDisplayVisaDocNumber(String displayVisaDocNumber) {
		this.displayVisaDocNumber = displayVisaDocNumber;
	}

	public String getDisplayVisaDocPlaceOfIssue() {
		return displayVisaDocPlaceOfIssue;
	}

	@Override
	public void setDisplayVisaDocPlaceOfIssue(String displayVisaDocPlaceOfIssue) {
		this.displayVisaDocPlaceOfIssue = displayVisaDocPlaceOfIssue;
	}

	public String getDisplayVisaDocIssueDate() {
		return displayVisaDocIssueDate;
	}

	@Override
	public void setDisplayVisaDocIssueDate(String displayVisaDocIssueDate) {
		try {
			if (displayVisaDocIssueDate != null && !"".equals(displayVisaDocIssueDate.trim())) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				displayVisaDocIssueDate = format.format(format.parse(displayVisaDocIssueDate));
			}
		} catch (ParseException e) {
			// do nothing
		}
		this.displayVisaDocIssueDate = displayVisaDocIssueDate;
	}

	public String getDisplayVisaApplicableCountry() {
		return displayVisaApplicableCountry;
	}

	@Override
	public void setDisplayVisaApplicableCountry(String displayVisaApplicableCountry) {
		this.displayVisaApplicableCountry = displayVisaApplicableCountry;
	}

	public String getDisplaypnrPaxArrivalFlightNumber() {
		return displaypnrPaxArrivalFlightNumber;
	}

	@Override
	public void setDisplaypnrPaxArrivalFlightNumber(String displaypnrPaxArrivalFlightNumber) {
		this.displaypnrPaxArrivalFlightNumber = displaypnrPaxArrivalFlightNumber;
	}

	public String getDisplaypnrPaxFltArrivalDate() {
		return displaypnrPaxFltArrivalDate;
	}

	@Override
	public void setDisplaypnrPaxFltArrivalDate(String displaypnrPaxFltArrivalDate) {
		this.displaypnrPaxFltArrivalDate = displaypnrPaxFltArrivalDate;
	}

	public String getDisplaypnrPaxArrivalTime() {
		return displaypnrPaxArrivalTime;
	}

	@Override
	public void setDisplaypnrPaxArrivalTime(String displaypnrPaxArrivalTime) {
		this.displaypnrPaxArrivalTime = displaypnrPaxArrivalTime;
	}

	public String getDisplaypnrPaxDepartureFlightNumber() {
		return displaypnrPaxDepartureFlightNumber;
	}

	@Override
	public void setDisplaypnrPaxDepartureFlightNumber(String displaypnrPaxDepartureFlightNumber) {
		this.displaypnrPaxDepartureFlightNumber = displaypnrPaxDepartureFlightNumber;
	}

	public String getDisplaypnrPaxFltDepartureDate() {
		return displaypnrPaxFltDepartureDate;
	}

	@Override
	public void setDisplaypnrPaxFltDepartureDate(String displaypnrPaxFltDepartureDate) {
		this.displaypnrPaxFltDepartureDate = displaypnrPaxFltDepartureDate;
	}

	public String getDisplaypnrPaxDepartureTime() {
		return displaypnrPaxDepartureTime;
	}

	@Override
	public void setDisplaypnrPaxDepartureTime(String displaypnrPaxDepartureTime) {
		this.displaypnrPaxDepartureTime = displaypnrPaxDepartureTime;
	}

	public String getDisplayPnrPaxGroupId() {
		return displayPnrPaxGroupId;
	}

	@Override
	public void setDisplayPnrPaxGroupId(String displayPnrPaxGroupId) {
		this.displayPnrPaxGroupId = displayPnrPaxGroupId;
	}

	public String getDisplayInfantTitle() {
		return displayInfantTitle;
	}

	@Override
	public void setDisplayPaxTitle(String displayInfantTitle) {
		setDisplayInfantTitle(displayInfantTitle);
	}

	public void setDisplayInfantTitle(String displayInfantTitle) {
		this.displayInfantTitle = displayInfantTitle;
	}

	public String getDisplayInfantTitleOl() {
		return displayInfantTitleOl;
	}

	@Override
	public void setDisplayPaxTitleOl(String displayInfantTitleOl) {
		setDisplayInfantTitleOl(displayInfantTitleOl);
	}

	public void setDisplayInfantTitleOl(String displayInfantTitleOl) {
		this.displayInfantTitleOl = displayInfantTitleOl;
	}

	@Override
	public void setDisplayFFID(String ffid) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setDisplayOrdePaxSequence(String string) {
		// TODO Auto-generated method stub
	}

	public String getDisplayInfantMCO() {
		return displayInfantMCO;
	}

	public void setDisplayInfantMCO(String displayInfantMCO) {
		this.displayInfantMCO = displayInfantMCO;
	}

	public String getDisplayInfantCharg() {
		return displayInfantCharg;
	}

	public void setDisplayInfantCharg(String displayInfantCharg) {
		this.displayInfantCharg = displayInfantCharg;
	}

	public String getDisplayInfantPay() {
		return displayInfantPay;
	}

	public void setDisplayInfantPay(String displayInfantPay) {
		this.displayInfantPay = displayInfantPay;
	}

	public String getDisplayInfantBal() {
		return displayInfantBal;
	}

	public void setDisplayInfantBal(String displayInfantBal) {
		this.displayInfantBal = displayInfantBal;
	}

	public String getDisplayInfantAcco() {
		return displayInfantAcco;
	}

	public void setDisplayInfantAcco(String displayInfantAcco) {
		this.displayInfantAcco = displayInfantAcco;
	}
	
}
