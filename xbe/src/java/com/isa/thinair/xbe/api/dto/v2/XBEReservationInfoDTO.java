package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCPromotionInfoTO;

/**
 * DTO to keep all the XBE reservation related Information
 * 
 * 
 * @author Thushara
 */
public class XBEReservationInfoDTO implements Serializable {

	private static final long serialVersionUID = 2337654111098L;

	/**
	 * VARIABLES USED THROUGHOUT THE FLOW
	 */

	private String transactionId;

	private SYSTEM selectedSystem;

	private String originAgentCode;

	private BigDecimal availableBalance;

	private String reservationStatus;

	private String bookingType;

	private Map<Integer, BigDecimal> paxCreditMap;

	private List<String> interlineModificationParams;

	private Date reservationLastModDate;
	
	private String marketingAirlineCode;

	/**
	 * The flag denoting whether the particular reservation is allowed to be modified in the modification flows.
	 */
	private boolean modifiableReservation = true;

	boolean gdsReservationModAllowed = false;

	private boolean hasDuplicates;

	private LCCReservationBaggageSummaryTo resBaggageSummary;

	private List<BundledFareDTO> bundledFareDTOs;

	private Date existingReleaseTimestamp;

	private LCCPromotionInfoTO lccPromotionInfoTO;
	
	private String ownerChannelId;

	private int originChannelId;
	
	private boolean isSegmentModifiableAsPerETicketStatus = false;
	
	private boolean infantPaymentSeparated = false;

	private XBEReservationContactInfoDTO contactInfoDTO;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public SYSTEM getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(SYSTEM selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public String getOriginAgentCode() {
		return originAgentCode;
	}

	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getReservationStatus() {
		return reservationStatus;
	}

	public void setReservationStatus(String reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public void setPaxCreditMap(Map<Integer, BigDecimal> paxCreditMap) {
		this.paxCreditMap = paxCreditMap;
	}

	public Map<Integer, BigDecimal> getPaxCreditMap() {
		return paxCreditMap;
	}

	public List<String> getInterlineModificationParams() {
		return interlineModificationParams;
	}

	public void setInterlineModificationParams(List<String> interlineModificationParams) {
		this.interlineModificationParams = interlineModificationParams;
	}

	/**
	 * @return the modifiableReservation
	 */
	public boolean isModifiableReservation() {
		return modifiableReservation;
	}

	/**
	 * @param modifiableReservation
	 *            the modifiableReservation to set
	 */
	public void setModifiableReservation(boolean modifiableReservation) {
		this.modifiableReservation = modifiableReservation;
	}

	/**
	 * @return the bookingType
	 */
	public String getBookingType() {
		return bookingType;
	}

	/**
	 * @param bookingType
	 *            the bookingType to set
	 */
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public boolean hasDuplicates() {
		return hasDuplicates;
	}

	public void setDuplicates(boolean hasDuplicates) {
		this.hasDuplicates = hasDuplicates;
	}

	/**
	 * @return the gdsReservationModAllowed
	 */
	public boolean isGdsReservationModAllowed() {
		return gdsReservationModAllowed;
	}

	/**
	 * @param gdsReservationModAllowed
	 *            the gdsReservationModAllowed to set
	 */
	public void setGdsReservationModAllowed(boolean gdsReservationModAllowed) {
		this.gdsReservationModAllowed = gdsReservationModAllowed;
	}

	public Date getReservationLastModDate() {
		return reservationLastModDate;
	}

	public void setReservationLastModDate(Date reservationLastModDate) {
		this.reservationLastModDate = reservationLastModDate;
	}

	public LCCReservationBaggageSummaryTo getResBaggageSummary() {
		return resBaggageSummary;
	}

	public void setResBaggageSummary(LCCReservationBaggageSummaryTo resBaggageSummary) {
		this.resBaggageSummary = resBaggageSummary;
	}

	public List<BundledFareDTO> getBundledFareDTOs() {
		return bundledFareDTOs;
	}

	public void setBundledFareDTOs(List<BundledFareDTO> bundledFareDTOs) {
		this.bundledFareDTOs = bundledFareDTOs;
	}

	public Date getExistingReleaseTimestamp() {
		return existingReleaseTimestamp;
	}

	public void setExistingReleaseTimestamp(Date existingReleaseTimestamp) {
		this.existingReleaseTimestamp = existingReleaseTimestamp;
	}

	public LCCPromotionInfoTO getLccPromotionInfoTO() {
		return lccPromotionInfoTO;
	}

	public void setLccPromotionInfoTO(LCCPromotionInfoTO lccPromotionInfoTO) {
		this.lccPromotionInfoTO = lccPromotionInfoTO;
	}

	public String getOwnerChannelId() {
		return ownerChannelId;
	}

	public void setOwnerChannelId(String ownerChannelId) {
		this.ownerChannelId = ownerChannelId;
	}

	public int getOriginChannelId() {
		return originChannelId;
	}

	public void setOriginChannelId(int originChannelId) {
		this.originChannelId = originChannelId;
	}	

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	public boolean isInfantPaymentSeparated() {
		return infantPaymentSeparated;
	}

	public boolean isSegmentModifiableAsPerETicketStatus() {
		return isSegmentModifiableAsPerETicketStatus;
	}

	public void setSegmentModifiableAsPerETicketStatus(boolean isSegmentModifiableAsPerETicketStatus) {
		this.isSegmentModifiableAsPerETicketStatus = isSegmentModifiableAsPerETicketStatus;
	}

	public void setInfantPaymentSeparated(boolean infantPaymentSeparated) {
		this.infantPaymentSeparated = infantPaymentSeparated;
	}

	public XBEReservationContactInfoDTO getContactInfoDTO() {
		return contactInfoDTO;
	}

	public void setContactInfoDTO(XBEReservationContactInfoDTO contactInfoDTO) {
		this.contactInfoDTO = contactInfoDTO;
	}
}
