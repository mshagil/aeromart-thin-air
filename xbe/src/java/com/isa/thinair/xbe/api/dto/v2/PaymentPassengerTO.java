package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

public class PaymentPassengerTO {
	private String displayPaxID;

	private String displayPassengerName;

	private String displayTotal;

	private String displayPaid;

	private String displayDiscount;

	private String displayToPay;

	private String displayUsedCredit;

	private String displayFirstName;

	private String displayLastName;

	private Collection<PaxUsedCreditInfoTO> paxCreditInfo;

	private String displayTravelerRefNo;

	private String displayOriginalPaid;

	private String displayOriginalTobePaid;

	private String paxSequence;

	private String paxType;

	/**
	 * @return the displayPaxID
	 */
	public String getDisplayPaxID() {
		return displayPaxID;
	}

	/**
	 * @param displayPaxID
	 *            the displayPaxID to set
	 */
	public void setDisplayPaxID(String displayPaxID) {
		this.displayPaxID = displayPaxID;
	}

	/**
	 * @return the displayPassengerName
	 */
	public String getDisplayPassengerName() {
		return displayPassengerName;
	}

	/**
	 * @param displayPassengerName
	 *            the displayPassengerName to set
	 */
	public void setDisplayPassengerName(String displayPassengerName) {
		this.displayPassengerName = displayPassengerName;
	}

	/**
	 * @return the displayTotal
	 */
	public String getDisplayTotal() {
		return displayTotal;
	}

	/**
	 * @param displayTotal
	 *            the displayTotal to set
	 */
	public void setDisplayTotal(String displayTotal) {
		this.displayTotal = displayTotal;
	}

	/**
	 * @return the displayPaid
	 */
	public String getDisplayPaid() {
		return displayPaid;
	}

	/**
	 * @param displayPaid
	 *            the displayPaid to set
	 */
	public void setDisplayPaid(String displayPaid) {
		this.displayPaid = displayPaid;
	}

	/**
	 * @return the displayToPay
	 */
	public String getDisplayToPay() {
		return displayToPay;
	}

	/**
	 * @param displayToPay
	 *            the displayToPay to set
	 */
	public void setDisplayToPay(String displayToPay) {
		this.displayToPay = displayToPay;
	}

	/**
	 * @return the displayUsedCredit
	 */
	public String getDisplayUsedCredit() {
		return displayUsedCredit;
	}

	/**
	 * @param displayUsedCredit
	 *            the displayUsedCredit to set
	 */
	public void setDisplayUsedCredit(String displayUsedCredit) {
		this.displayUsedCredit = displayUsedCredit;
	}

	/**
	 * @return the displayFirstName
	 */
	public String getDisplayFirstName() {
		return displayFirstName;
	}

	/**
	 * @param displayFirstName
	 *            the displayFirstName to set
	 */
	public void setDisplayFirstName(String displayFirstName) {
		this.displayFirstName = displayFirstName;
	}

	/**
	 * @return the displayLastName
	 */
	public String getDisplayLastName() {
		return displayLastName;
	}

	/**
	 * @param displayLastName
	 *            the displayLastName to set
	 */
	public void setDisplayLastName(String displayLastName) {
		this.displayLastName = displayLastName;
	}

	public Collection<PaxUsedCreditInfoTO> getPaxCreditInfo() {
		return paxCreditInfo;
	}

	public void setPaxCreditInfo(Collection<PaxUsedCreditInfoTO> paxCreditInfo) {
		this.paxCreditInfo = paxCreditInfo;
	}

	public String getDisplayTravelerRefNo() {
		return displayTravelerRefNo;
	}

	public void setDisplayTravelerRefNo(String displayTravelerRefNo) {
		this.displayTravelerRefNo = displayTravelerRefNo;
	}

	public String getDisplayOriginalPaid() {
		return displayOriginalPaid;
	}

	public void setDisplayOriginalPaid(String displayOriginalPaid) {
		this.displayOriginalPaid = displayOriginalPaid;
	}

	public String getDisplayOriginalTobePaid() {
		return displayOriginalTobePaid;
	}

	public void setDisplayOriginalTobePaid(String displayOriginalTobePaid) {
		this.displayOriginalTobePaid = displayOriginalTobePaid;
	}

	public String getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(String paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getDisplayDiscount() {
		return displayDiscount;
	}

	public void setDisplayDiscount(String displayDiscount) {
		this.displayDiscount = displayDiscount;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public String getPaxType() {
		return paxType;
	}

}
