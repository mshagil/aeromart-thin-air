package com.isa.thinair.xbe.api.dto.v2;

public interface IDisplayPax {

	void setDisplayNationalIDNo(String displayNationalIDNo);

	void setDisplayPaxType(String displayPaxType);

	void setDisplayPaxFirstName(String displayPaxFirstName);

	void setDisplayPaxLastName(String displayPaxLastName);

	void setDisplayPaxNationality(String displayPaxNationality);

	void setDisplayPaxDOB(String displayPaxDOB);

	void setDisplayPaxTravellingWith(String displayPaxTravellingWith);

	void setDisplayPaxSSR(String displayPaxSSR);

	void setDisplayPaxSSRCode(String displayPaxSSRCode);

	void setDisplayPaxSSRInformation(String displayPaxSSRInformation);

	void setDisplayPaxSelect(String displayPaxSelect);

	void setDisplayPaxTravelReference(String displayPaxTravelReference);

	void setNameEditable(boolean nameEditable);

	void setDisplayPaxCategory(String displayPaxCategory);

	void setDisplayPnrPaxCatFOIDNumber(String displayPnrPaxCatFOIDNumber);

	void setDisplayPnrPaxCatFOIDExpiry(String displayPnrPaxCatFOIDExpiry);

	void setDisplayPnrPaxCatFOIDPlace(String displayPnrPaxCatFOIDPlace);

	void setDisplayPaxSequence(String displayPaxSequence);

	void setDisplayPnrPaxCatFOIDType(String displayPnrPaxCatFOIDType);

	void setDisplayETicketMask(String displayETicketMask);

	void setDisplayPaxFirstNameOl(String displayPaxFirstNameOl);

	void setDisplayPaxLastNameOl(String displayPaxLastNameOl);

	void setDisplayNameTranslationLanguage(String displayNameTranslationLanguage);

	void setAlertAutoCancellation(boolean alertAutoCancellation);

	void setDisplayPnrPaxPlaceOfBirth(String displayPnrPaxPlaceOfBirth);

	void setDisplayTravelDocType(String displayTravelDocType);

	void setDisplayVisaDocNumber(String displayVisaDocNumber);

	void setDisplayVisaDocPlaceOfIssue(String displayVisaDocPlaceOfIssue);

	void setDisplayVisaDocIssueDate(String displayVisaDocIssueDate);

	void setDisplayVisaApplicableCountry(String displayVisaApplicableCountry);

	void setDisplaypnrPaxArrivalFlightNumber(String displaypnrPaxArrivalFlightNumber);

	void setDisplaypnrPaxFltArrivalDate(String displaypnrPaxFltArrivalDate);

	void setDisplaypnrPaxArrivalTime(String displaypnrPaxArrivalTime);

	void setDisplaypnrPaxDepartureFlightNumber(String displaypnrPaxDepartureFlightNumber);

	void setDisplaypnrPaxFltDepartureDate(String displaypnrPaxFltDepartureDate);

	void setDisplaypnrPaxDepartureTime(String displaypnrPaxDepartureTime);

	void setDisplayPnrPaxGroupId(String displayPnrPaxGroupId);

	void setDisplayPaxTitle(String displayPaxTitle);

	void setDisplayPaxTitleOl(String displayPaxTitleOl);

	void setDisplayFFID(String ffid);

	void setDisplayOrdePaxSequence(String string);

}