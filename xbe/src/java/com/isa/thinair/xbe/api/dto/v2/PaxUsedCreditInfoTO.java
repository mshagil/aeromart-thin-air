package com.isa.thinair.xbe.api.dto.v2;

import java.io.Serializable;

public class PaxUsedCreditInfoTO implements Serializable {

	private static final long serialVersionUID = 11643812543737812L;

	private String paxCreditId = null;

	private String usedAmount = null;

	private String paxSequence = null;

	private String carrier = null;

	private String pnr = null;

	private String residingCarrierAmount;

	public String getPaxCreditId() {
		return paxCreditId;
	}

	public void setPaxCreditId(String paxCreditId) {
		this.paxCreditId = paxCreditId;
	}

	public String getUsedAmount() {
		return usedAmount;
	}

	public void setUsedAmount(String usedAmount) {
		this.usedAmount = usedAmount;
	}

	public String getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(String paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier
	 *            the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getResidingCarrierAmount() {
		return residingCarrierAmount;
	}

	public void setResidingCarrierAmount(String residingCarrierAmount) {
		this.residingCarrierAmount = residingCarrierAmount;
	}

}
