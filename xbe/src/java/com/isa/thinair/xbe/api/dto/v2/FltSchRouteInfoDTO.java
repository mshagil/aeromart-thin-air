package com.isa.thinair.xbe.api.dto.v2;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author eric
 * 
 */
public class FltSchRouteInfoDTO {

	private String route;

	private Collection<FlightScheduleSegInfoDTO> fltScheSegInfo;

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Collection<FlightScheduleSegInfoDTO> getFltScheSegInfo() {
		if (fltScheSegInfo == null) {
			fltScheSegInfo = new ArrayList<FlightScheduleSegInfoDTO>();
		}
		return fltScheSegInfo;
	}
}
