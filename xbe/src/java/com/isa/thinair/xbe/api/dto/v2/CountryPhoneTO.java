package com.isa.thinair.xbe.api.dto.v2;

import java.util.Collection;

/**
 * TO to keep Phone details for validations
 * 
 * @author Thushara
 * 
 */
public class CountryPhoneTO {

	private String countryCode;

	private String countryPhoneCode;

	private String minLength;

	private String maxlength;

	private boolean validateAreaCode;

	private Collection<AreaCodeTO> areaCodes;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryPhoneCode() {
		return countryPhoneCode;
	}

	public void setCountryPhoneCode(String countryPhoneCode) {
		this.countryPhoneCode = countryPhoneCode;
	}

	public String getMinLength() {
		return minLength;
	}

	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}

	public String getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(String maxlength) {
		this.maxlength = maxlength;
	}

	public Collection<AreaCodeTO> getAreaCodes() {
		return areaCodes;
	}

	public void setAreaCodes(Collection<AreaCodeTO> areaCodes) {
		this.areaCodes = areaCodes;
	}

	public boolean getValidateAreaCode() {
		return validateAreaCode;
	}

	public void setValidateAreaCode(boolean validateAreaCode) {
		this.validateAreaCode = validateAreaCode;
	}

}
