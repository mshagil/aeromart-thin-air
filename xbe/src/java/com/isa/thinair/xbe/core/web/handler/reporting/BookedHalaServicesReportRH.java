package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * Request handler for Hala service reports.
 * 
 * @author lalanthi
 * @since 01/09/2009
 * 
 */
public class BookedHalaServicesReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(BookedHalaServicesReportRH.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("BookedHalaServicesReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			forward = S2Constants.Result.ERROR;
			log.error("Error in BookedHalaServicesReportRH execute()" + e.getMessage());
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setStationComboList(request);
		setReportingPeriod(request);
		setServiceCategoryComboList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets the values list to populate from and to airports lists
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createAirportsList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Sets the values list to populate Service Category List.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setServiceCategoryComboList(HttpServletRequest request) throws ModuleException {
		// String strServiceCategory = SelectListGenerator.createSsrCategoryList();
		request.setAttribute(WebConstants.REQ_SERVICE_CATEGORY_LIST, "Hala");
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the initial reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Gets the report data and sets to the corresponding view option.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ReportsSearchCriteria search = null;
		ResultSet resultSet = null;
		String reportView = request.getParameter("hdnReportView");
		boolean showFullColumns = BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_FULL_SEAT_INVENT_RPT);
		String id = "UC_REPM_026";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		String templateDetail = "HalaServicesDetailReport.jasper";
		String templateSummary = "HalaServicesSummaryReport.jasper";

		try {
			if (reportView.equals("SUMMARY")) {
				search = new ReportsSearchCriteria();
				String fromDate = request.getParameter("txtFromDate");
				String toDate = request.getParameter("txtToDate");
				String fromAirport = request.getParameter("selFromAirport");
				String toAirport = request.getParameter("selToAirport");
				String flightNo = request.getParameter("txtFlightNo");
				String serviceCategory = request.getParameter("serviceCategory");
				String value = request.getParameter("radReportOption");

				if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {

					String s_FromDate = "'" + fromDate + " 00:00:00'," + "'DD/MM/YYYY HH24:mi:ss'";
					String s_ToDate = "'" + toDate + " 23:59:00'," + "'DD/MM/YYYY HH24:mi:ss'";
					search.setDateRangeFrom(s_FromDate);
					search.setDateRangeTo(s_ToDate);

				}
				search.setFrom(fromAirport);
				search.setTo(toAirport);
				search.setFlightNumber(flightNo);
				search.setSsrCategory(serviceCategory);
				search.setSsrCategory(SSRCategory.ID_AIRPORT_SERVICE.toString());
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templateSummary));
				resultSet = ModuleServiceLocator.getDataExtractionBD().getBookedHalaServiceData(search);
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("SHOW_FULL_COLUMNS", String.valueOf(showFullColumns));
				parameters.put("FROM_DATE", fromDate);
				parameters.put("TO_DATE", toDate);
				parameters.put("ID", id);
				parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameters.put("AMT_1", "(" + strBase + ")");

				parameters.put("FROM_AIRPORT", fromAirport);
				parameters.put("TO_AIRPORT", toAirport);
				parameters.put("FLIGHT_NO", flightNo);
				parameters.put("SERVICE_CAT", serviceCategory);
				parameters.put("REPORT_MEDIUM", value);

				// To provide Report Format Options
				String reportNumFormat = request.getParameter("radRptNumFormat");
				ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

				if (value.trim().equals("HTML")) {
					parameters.put("IMG", image);
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							null, response);
				} else if (value.trim().equals("PDF")) {
					image = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG", image);
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
				} else if (value.trim().equals("EXCEL")) {
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
				} else if (value.trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=BookedHalaServicesSummaryReport.csv");
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
				}

			} else {
				search = new ReportsSearchCriteria();
				String detailFlightNo = request.getParameter("flightNo");
				String strDateFrom = request.getParameter("fromDate");
				String strDateTo = request.getParameter("toDate");
				String origin = request.getParameter("origin");
				String destination = request.getParameter("destination");
				String ssrChargeId = request.getParameter("chargeId");
				String serviceCat = request.getParameter("servCat");
				String param_fromDate = null;
				String param_toDate = null;

				if (isNotEmptyOrNull(strDateFrom) && isNotEmptyOrNull(strDateTo)) {
					param_fromDate = strDateFrom.substring(0, 11);
					param_toDate = strDateTo.substring(0, 11);
				}
				if (isNotEmptyOrNull(strDateFrom) && isNotEmptyOrNull(strDateTo)) {
					search.setDateRangeFrom("'" + strDateFrom + "'," + "'dd/mm/yy HH24:mi'");// date format dd/mm/yy
																								// HH24:mm
					search.setDateRangeTo("'" + strDateTo + "'," + "'dd/mm/yy HH24:mi'");
				}
				search.setFlightNumber(detailFlightNo);
				search.setFrom(origin);
				search.setSsrChargeId(ssrChargeId);
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templateDetail));
				resultSet = ModuleServiceLocator.getDataExtractionBD().getBookedHalaServiceDetails(search);
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("SHOW_FULL_COLUMNS", String.valueOf(showFullColumns));
				parameters.put("FROM_DATE", param_fromDate);
				parameters.put("TO_DATE", param_toDate);
				parameters.put("ID", id);
				parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
				parameters.put("TO_AIRPORT", destination);
				parameters.put("FROM_AIRPORT", origin);
				parameters.put("FLIGHT_NO", detailFlightNo);
				parameters.put("SERVICE_CAT", serviceCat);

				// To provide Report Format Options
				String reportNumFormat = request.getParameter("radRptNumFormat");
				ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BookedHalaServicesDetailReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
