package com.isa.thinair.xbe.core.web.handler.system;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.SessionClearUtill;

public class LogoutRH extends BasicRH {

	private static Log log = LogFactory.getLog(LogoutRH.class);

	public static String execute(HttpServletRequest request) {

		String forward = S2Constants.Result.GET_XBE_DATA;

		try {
			// LoadDataRequestHandler.setReleaseSeatsHtml(request);
			SessionClearUtill.releaseBlocSeat(request);
			BasicRH.audit(request, TasksUtil.SECURITY_LOG_OUT);

			// When a user logs out from XBE, we need to clear all the AASessions that are in other carriers
			if (isLCCAccessed(request) && AppSysParamsUtil.isLCCConnectivityEnabled()
					&& hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
				UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

				if (log.isDebugEnabled()) {
					log.debug("USER SESSION INVALIDATION CALLED " + userPrincipal.getUserId() + ", "
							+ userPrincipal.getSessionId());
				}

				ModuleServiceLocator.getLCCUserAgentSeviceBD().invalidateAAServiceSessionAcrossCarriers(
						userPrincipal.getUserId(), TrackInfoUtil.getBasicTrackInfo(request));
				request.getSession().setAttribute(S2Constants.Session_Data.LCC_ACCESSED, null);
			}
		} catch (ModuleException e) {
			log.error(e);
		}

		if (!request.getSession().isNew()) {
			request.getSession().invalidate();
			ForceLoginInvoker.close();
		}

		return forward;
	}

	private static boolean isLCCAccessed(HttpServletRequest request) {
		return request.getSession().getAttribute(S2Constants.Session_Data.LCC_ACCESSED) != null
				&& ((Boolean) request.getSession().getAttribute(S2Constants.Session_Data.LCC_ACCESSED)).booleanValue();
	}
}
