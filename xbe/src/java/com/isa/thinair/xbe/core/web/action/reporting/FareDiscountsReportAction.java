package com.isa.thinair.xbe.core.web.action.reporting;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.reporting.FareDiscountsReportRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.FAIR_DISCOUNTS_REPORT)
public class FareDiscountsReportAction extends BaseRequestResponseAwareAction {

	public String execute() throws Exception {
		return FareDiscountsReportRH.execute(request, response);
	}

}
