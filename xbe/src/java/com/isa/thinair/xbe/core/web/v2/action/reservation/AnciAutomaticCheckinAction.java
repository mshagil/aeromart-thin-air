package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.FltSegBuilder;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * 
 * @author Mohamed Rizwan
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciAutomaticCheckinAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AnciAutomaticCheckinAction.class);
	private boolean success = true;

	private String messageTxt;

	private String selectedFlightList;

	private FlightSearchDTO searchParams;

	private boolean modifyAncillary = false;

	private boolean isRequote = false;

	private List<AutomaticCheckinDTO> automaticCheckinDTOs;

	public String execute() {
		String result = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			List<FlightSegmentTO> flightSegmentTOs = null;

			if (modifyAncillary) {
				FltSegBuilder fltSegBuilder = new FltSegBuilder(selectedFlightList);
				flightSegmentTOs = fltSegBuilder.getSelectedFlightSegments();
			} else {
				if (isRequote) {
					flightSegmentTOs = ReservationUtil.populateFlightSegmentList(selectedFlightList, searchParams);
				} else {
					flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(selectedFlightList, searchParams);
				}
			}
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap = getAirportWiseSegMap(flightSegmentTOs);

			Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> autoCheckinDtoMap = ModuleServiceLocator.getAutomaticCheckinBD()
					.getAvailableAutomaticCheckins(airportWiseSegMap);

			automaticCheckinDTOs = transformAutoCheckin(autoCheckinDtoMap, flightSegmentTOs);

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Exception while getting Airport Template amount for auto checkin-" + e);
		}
		return result;
	}

	/**
	 * Transform DTO
	 * 
	 * @param autoCheckinDtoMap
	 * @param flightSegmentTOs
	 * @return
	 */
	private List<AutomaticCheckinDTO> transformAutoCheckin(Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> autoCheckinDtoMap,
			List<FlightSegmentTO> flightSegmentTOs) {

		List<AutomaticCheckinDTO> segmentAutoCheckinMaps = new ArrayList<AutomaticCheckinDTO>();

		Set<Entry<AirportServiceKeyTO, List<AutomaticCheckinDTO>>> flightSegAutoCheckinSet = autoCheckinDtoMap.entrySet();
		for (Entry<AirportServiceKeyTO, List<AutomaticCheckinDTO>> flightSegAutoCheckinEntry : flightSegAutoCheckinSet) {
			AirportServiceKeyTO airportKeyTo = flightSegAutoCheckinEntry.getKey();
			List<AutomaticCheckinDTO> flightSegmentautoCheckins = flightSegAutoCheckinEntry.getValue();
			AutomaticCheckinDTO autoCheckinDTO = new AutomaticCheckinDTO();
			autoCheckinDTO = getAutoCheckinSegmentWise(flightSegmentautoCheckins, airportKeyTo, autoCheckinDTO, flightSegmentTOs);
			segmentAutoCheckinMaps.add(autoCheckinDTO);
		}

		return segmentAutoCheckinMaps;
	}

	/**
	 * @param flightSegmentautoCheckins
	 * @param airportKeyTo
	 * @param autoCheckinDTO
	 * @param flightSegmentTOs
	 * @return
	 */
	private AutomaticCheckinDTO getAutoCheckinSegmentWise(List<AutomaticCheckinDTO> flightSegmentautoCheckins,
			AirportServiceKeyTO airportKeyTo, AutomaticCheckinDTO autoCheckinDTO, List<FlightSegmentTO> flightSegmentTOs) {
		if (flightSegmentautoCheckins != null && flightSegmentautoCheckins.size() > 0) {
			autoCheckinDTO = flightSegmentautoCheckins.get(0);
			Optional<FlightSegmentTO> fligthSegmentOpt = flightSegmentTOs.stream()
					.filter(fltSeg -> fltSeg.getSegmentCode().equals(airportKeyTo.getSegmentCode())).findFirst();
			if (fligthSegmentOpt.isPresent()) {
				autoCheckinDTO.setFlightSegmentTo(fligthSegmentOpt.get());
			}
		}
		return autoCheckinDTO;
	}

	/**
	 * @param flightSegmentTOs
	 * @return
	 */
	private Map<AirportServiceKeyTO, FlightSegmentTO> getAirportWiseSegMap(List<FlightSegmentTO> flightSegmentTOs) {
		AirportServiceKeyTO airportServiceKeyTO;

		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap = new LinkedHashMap<AirportServiceKeyTO, FlightSegmentTO>();
		for (FlightSegmentTO flightsegmentTo : flightSegmentTOs) {
			airportServiceKeyTO = new AirportServiceKeyTO(flightsegmentTo.getAirportCode(), flightsegmentTo.getAirportType(),
					flightsegmentTo.getDepartureDateTimeZulu().getTime(), flightsegmentTo.getSegmentCode());
			if (!airportWiseSegMap.containsKey(airportServiceKeyTO)) {
				airportWiseSegMap.put(airportServiceKeyTO, flightsegmentTo);
			}
		}

		return airportWiseSegMap;
	}

	/**
	 * @return the selectedFlightList
	 */
	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	/**
	 * @param selectedFlightList
	 *            the selectedFlightList to set
	 */
	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	/**
	 * @return the searchParams
	 */
	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	/**
	 * @param searchParams
	 *            the searchParams to set
	 */
	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	/**
	 * @return the modifyAncillary
	 */
	public boolean isModifyAncillary() {
		return modifyAncillary;
	}

	/**
	 * @param modifyAncillary
	 *            the modifyAncillary to set
	 */
	public void setModifyAncillary(boolean modifyAncillary) {
		this.modifyAncillary = modifyAncillary;
	}

	/**
	 * @return the isRequote
	 */
	public boolean isRequote() {
		return isRequote;
	}

	/**
	 * @param isRequote
	 *            the isRequote to set
	 */
	public void setRequote(boolean isRequote) {
		this.isRequote = isRequote;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success
	 *            the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param messageTxt
	 *            the messageTxt to set
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * @return the automaticCheckinDTOs
	 */
	public List<AutomaticCheckinDTO> getAutomaticCheckinDTOs() {
		return automaticCheckinDTOs;
	}

	/**
	 * @param automaticCheckinDTOs
	 *            the automaticCheckinDTOs to set
	 */
	public void setAutomaticCheckinDTOs(List<AutomaticCheckinDTO> automaticCheckinDTOs) {
		this.automaticCheckinDTOs = automaticCheckinDTOs;
	}

}