package com.isa.thinair.xbe.core.web.handler.user;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.config.XBEConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.generator.user.UserUNotesHG;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class UserUNotesRH extends BasicRH {

	private static Log log = LogFactory.getLog(UserUNotesRH.class);

	private static XBEConfig xbeConfig = new XBEConfig();

	private static final String PARAM_MODE = "hdnMode";

	/**
	 * Main execute method for User notes
	 * 
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;
		String strMode = request.getParameter(PARAM_MODE);
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		if (strMode != null && strMode.equals("VIEW")) {
			try {
				checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER);
				setNotes(request);
			} catch (ModuleException me) {
				if (log.isErrorEnabled()) {
					log.error("Error in geting user Notes ", me);
				}
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Error in geting user Notes ", e);
				}
				if (e instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
				} else {
					saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
				}
			}

			forward = S2Constants.Result.VIEW_USER_NOTE;
		}

		if (strMode != null && strMode.equals("SAVE")) {
			try {
				checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_USER_ADDNOTE);
				saveNotes(request);
				saveMessage(request, xbeConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS, userLanguage), WebConstants.MSG_SUCCESS);

			} catch (ModuleException me) {
				if (log.isErrorEnabled()) {
					log.error("Error in saving user Notes " + me.getMessage());
				}
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Error in saving user Notes " + e.getMessage());
				}
				if (e instanceof RuntimeException) {
					forward = S2Constants.Result.ERROR;
					JavaScriptGenerator.setServerError(request, e.getMessage(), "", "");
				} else {
					saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}
		return forward;
	}

	/**
	 * SETS THE USER NOTES TO THE REQUEST
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setNotes(HttpServletRequest request) throws ModuleException {
		String strHtml = UserUNotesHG.getUserNotesDetails(request);
		request.setAttribute(WebConstants.REQ_HTML_USR_NOTES, strHtml);
	}

	/**
	 * saves the user notes
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void saveNotes(HttpServletRequest request) throws ModuleException {

		String strUserId = request.getParameter("hdnUserId");
		String strUserNote = request.getParameter("txtUsetNotes");
		UserNotes uNotes = null;
		if (strUserId != null && !strUserId.equals("")) {
			uNotes = new UserNotes();
			uNotes.setUserNotesTypeCode(WebConstants.USER_NOTES_FOR_USER_MAINTENANCE);
			uNotes.setIdentificationCode(strUserId);
			uNotes.setContent(strUserNote);
			ModuleServiceLocator.getSecurityBD().saveUserNotes(uNotes);

		}

	}

}
