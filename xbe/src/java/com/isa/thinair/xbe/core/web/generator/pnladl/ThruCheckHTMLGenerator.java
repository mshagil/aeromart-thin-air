package com.isa.thinair.xbe.core.web.generator.pnladl;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.PnlAdlTiming;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

/**
 * @author Thushara
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */
public class ThruCheckHTMLGenerator {
	private static Log log = LogFactory.getLog(ThruCheckHTMLGenerator.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static String clientErrors;

	public final String getThruCheckRowHtml(HttpServletRequest request) throws ModuleException {

		Page page = null;
		int recNo = 1;
		int totRec = 0;
		Collection thruChkCol = null;

		// setting minimumtimes for PNL & ADLs
		String strPNLMinimum = globalConfig.getBizParam(SystemParamKeys.PNL_DEPARTURE_GAP);
		String strADLMinimum = globalConfig.getBizParam(SystemParamKeys.ADL_INTERVAL);
		String strADLMinimumAfterCutoff = globalConfig.getBizParam(SystemParamKeys.ADL_INTERVAL_AFTER_CUTOFF_TIME);

		if (strPNLMinimum != null) {
			request.setAttribute("min_pnl_time", strPNLMinimum);
		}

		if (strADLMinimum != null) {
			request.setAttribute("min_adl_time", strADLMinimum);
		}

		if (strADLMinimumAfterCutoff != null) {
			request.setAttribute("min_adl_time_after_cutoff", strADLMinimumAfterCutoff);
		}
		request.setAttribute("maintain_min_adl_time_after_cutoff", AppSysParamsUtil.isMaintainSeperateADLTransmissionIntervalAfterCutOffTime());
		if (request.getParameter("hdnRecNo") == null) {
			recNo = 1;
		} else if (!("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}
		recNo = recNo - 1;

		String strSrchFltNo = request.getParameter("txtFlightNoSearch");
		String strSrchAirport = request.getParameter("selFrom");

		if (strSrchFltNo != null && strSrchFltNo.trim().equals("")) {
			strSrchFltNo = null;
		} else if (strSrchFltNo != null) {
			strSrchFltNo = strSrchFltNo.trim();

		}
		if (strSrchAirport != null && !strSrchAirport.trim().equals("")) {
			page = ModuleServiceLocator.getReservationAuxilliaryBD().getAllTimings(strSrchFltNo, strSrchAirport, recNo, 20);
		}

		// get the page data
		if (page != null) {
			thruChkCol = page.getPageData();
			totRec = page.getTotalNoOfRecords();

		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		log.debug("inside ThruCheckHTMLGenerator.getScheduleRowHtml search values");

		return createThrucheckRowHTML(thruChkCol);

	}

	// private String create ThruCheck RowHTML(Collection territories)
	private String createThrucheckRowHTML(Collection thruChkcol) {
		Iterator iter = null;
		PnlAdlTiming pnlAdlTiming = null;
		StringBuffer sb = new StringBuffer();
		SimpleDateFormat smtd = new SimpleDateFormat("dd/MM/yyyy");
		// String strArr = "var thruchkData = new Array();";
		// sb.append(strArr);
		int i = 0;

		if (thruChkcol != null) {
			iter = thruChkcol.iterator();
		}

		if (iter != null) {
			while (iter.hasNext()) {
				pnlAdlTiming = (PnlAdlTiming) iter.next();

				sb.append("thruchkData[" + i + "] = new Array();");
				if (pnlAdlTiming.getFlightNumber() == null) {
					sb.append("thruchkData[" + i + "][1] = 'ALL FLIGHTS';");
				} else {
					sb.append("thruchkData[" + i + "][1] = '" + pnlAdlTiming.getFlightNumber() + "';");
				}

				sb.append("thruchkData[" + i + "][2] = '" + pnlAdlTiming.getAirportCode() + "';");
				sb.append("thruchkData[" + i + "][3] = '" + getTimeGapString(pnlAdlTiming.getPnlDepGap()) + "';");
				sb.append("thruchkData[" + i + "][4] = '" + getTimeGapString(pnlAdlTiming.getAdlRepIntv()) + "';");
				sb.append("thruchkData[" + i + "][5] = '" + smtd.format(pnlAdlTiming.getStartingZuluDate()) + "';");
				sb.append("thruchkData[" + i + "][6] = '" + smtd.format(pnlAdlTiming.getEndingZuluDate()) + "';");

				if (pnlAdlTiming.getStatus() != null && pnlAdlTiming.getStatus().trim().equals(PNLConstants.PnlAdlTimings.ACTIVE)) {
					sb.append("thruchkData[" + i + "][7] = 'Active';");
				} else {
					sb.append("thruchkData[" + i + "][7] = 'In-Active';");
				}

				sb.append("thruchkData[" + i + "][8] = '" + pnlAdlTiming.getApplyToAllFlights() + "';");
				sb.append("thruchkData[" + i + "][9] = '" + pnlAdlTiming.getVersion() + "';");
				if (pnlAdlTiming.getId() != null) {
					sb.append("thruchkData[" + i + "][10] = '" + pnlAdlTiming.getId() + "';");
				} else {
					sb.append("thruchkData[" + i + "][10] = '';");
				}
				sb.append("thruchkData[" + i + "][11] = '" + getTimeGapString(pnlAdlTiming.getLastAdlGap()) + "';");
				sb.append("thruchkData[" + i + "][12] = '" + getTimeGapString(pnlAdlTiming.getAdlRepIntvAfterCutoff()) + "';");

				i++;
			}
		}
		return sb.toString();

	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("cc_thrucheck_airport_null", "airportnull");
			moduleErrs.setProperty("cc_thrucheck_flightno_null", "noFlight");
			moduleErrs.setProperty("cc_thrucheck_adltime_null", "noadltime");
			moduleErrs.setProperty("cc_thrucheck_pnltime_null", "nopnltime");
			moduleErrs.setProperty("cc_thrucheck_startdate_null", "startdate");
			moduleErrs.setProperty("cc_thrucheck_stopdate_null", "enddate");
			moduleErrs.setProperty("cc_thrucheck_date_invalid", "invaliddate");
			moduleErrs.setProperty("cc_thrucheck_time_invalid_format", "invalidtimegap");
			moduleErrs.setProperty("cc_thrucheck_startdate_startdategreat", "startdategreat");
			moduleErrs.setProperty("cc_thrucheck_stopdate_stopdategreat", "stopdategreat");
			moduleErrs.setProperty("cc_thrucheck_pnl_lesspnl", "lessminpnl");
			moduleErrs.setProperty("cc_thrucheck_adl_lessadl", "lessminadl");
			moduleErrs.setProperty("cc_thrucheck_adl_greatpnl", "greatmaxpnl");
			moduleErrs.setProperty("cc_thrucheck_adl_greatadl", "greatmaxadl");

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	/**
	 * Converts time gap in minutes to DD:HH:MM string format
	 * 
	 * @param timeGap
	 * @return
	 */
	private static String getTimeGapString(int timeGap) {

		int days = timeGap / 1440;

		int hours = (timeGap % 1440) / 60;

		int mins = (timeGap % 1440) % 60;

		return String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", mins);
	}

}
