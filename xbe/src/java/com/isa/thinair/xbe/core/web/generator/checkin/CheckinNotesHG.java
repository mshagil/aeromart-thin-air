package com.isa.thinair.xbe.core.web.generator.checkin;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.UserNotes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

public class CheckinNotesHG {

	private static Log log = LogFactory.getLog(CheckinNotesHG.class);

	/**
	 * Gets the collection of Checkin Notes
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static String getCheckinNotesDetails(HttpServletRequest request) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Geting the User Checkin Notes");
		}
		String strFlightSegId = request.getParameter("hdnFlightSegId");// notes for flight segment
		Collection<UserNotes> colUserNotes = null;
		if (strFlightSegId != null && !strFlightSegId.equals("")) {
			colUserNotes = ModuleServiceLocator.getSecurityBD().getCheckinNotes(WebConstants.USER_NOTES_FOR_MANUAL_CHECKIN,
					strFlightSegId);
		}

		return createUserNotes(colUserNotes);
	}

	/**
	 * Creates the Grid Array
	 * 
	 * @param colNotes
	 * @return
	 * @throws ModuleException
	 */
	private static String createUserNotes(Collection<UserNotes> colNotes) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Iterator<UserNotes> iter = null;
		UserNotes userNotes = null;
		SimpleDateFormat smft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		int i = 0;
		if (colNotes != null) {
			iter = colNotes.iterator();
			while (iter.hasNext()) {
				userNotes = (UserNotes) iter.next();
				sb.append("arrCheckinNotes [" + i + "] = new Array();");
				sb.append("arrCheckinNotes [" + i + "][1] = '" + smft.format(userNotes.getCreatedDate()) + "';");
				sb.append("arrCheckinNotes [" + i + "][2] = '" + userNotes.getContent() + "';");
				sb.append("arrCheckinNotes [" + i + "][3] = '" + userNotes.getUserId() + "';");

				i++;
			}
		}
		return sb.toString();
	}

}
