package com.isa.thinair.xbe.core.web.generator.reservation;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class ReservationHG {

	public static String getUsersAvailableCredit(HttpServletRequest request) throws ModuleException {
		String strReturnData = "";
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		BigDecimal dblAvailableCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (userPrincipal.getAgentCode() != null) {
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getCreditSharingAgent(userPrincipal.getAgentCode());

			if (agent.getAgentSummary() != null) {
				dblAvailableCredit = agent.getAgentSummary().getAvailableCredit();
			}
		}
		strReturnData = AccelAeroCalculator.formatAsDecimal(dblAvailableCredit);
		return strReturnData;
	}

	public static String getUserAvailableCreditInUserCurrency(HttpServletRequest request, String baseCurency)
			throws ModuleException {

		String agentAvailableCredit = null;

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(new Date(System.currentTimeMillis()));

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		
		BigDecimal dblAvailableCredit = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal agentCreditToShow = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (userPrincipal.getAgentCode() != null) {
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());

			if (agent.getAgentSummary() != null) {

				dblAvailableCredit = agent.getAgentSummary().getAvailableCredit();

				if (!baseCurency.equals(agent.getCurrencyCode())) {
					agentCreditToShow = exchangeRateProxy.convert(agent.getCurrencyCode(), dblAvailableCredit);
				} else {
					agentCreditToShow = dblAvailableCredit;
				}
			}
		}

		agentAvailableCredit = AccelAeroCalculator.formatAsDecimal(agentCreditToShow);

		return agentAvailableCredit;
	}
	
	
	
}
