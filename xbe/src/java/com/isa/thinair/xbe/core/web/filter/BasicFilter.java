package com.isa.thinair.xbe.core.web.filter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

abstract class BasicFilter implements Filter {

	private FilterConfig filterConfig;
	String errorPage = "/public/showError";

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {

	}

}
