package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.util.ServiceTaxCalculatorUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdatePaxSummaryListTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ConfirmUpdateUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class RequoteBalanceAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ConfirmUpdateAction.class);
	private boolean success = true;

	private String messageTxt;

	private FlightSearchDTO fareQuoteParams;

	private String balanceQueryData;

	private ConfirmUpdateChargesTO updateCharge;

	private Collection<ConfirmUpdateOverrideChargesTO> overrideCharges;

	private Collection<ConfirmUpdatePaxSummaryListTO> paxSummaryList;

	private boolean isAddSegment;

	private String paxNameDetails;

	private String flightRPHList;

	private String resContactInfo;

	public String execute() {
		String strForward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		UserPrincipal up = (UserPrincipal) request.getUserPrincipal();

		try {

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
					.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession()
					.getAttribute(S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo.getReservationStatus(), null,
					resInfo.isModifiableReservation(), false, resInfo.getLccPromotionInfoTO(), null, false, false);

			CommonReservationContactInfo contactInfo = ReservationUtil.transformJsonContactInfo(resContactInfo);

			boolean isAllowOverbookAfterCutOffTime = BasicRH.hasPrivilege(request,
					PriviledgeConstants.ALLOW_MAKE_OVERBOOK_RESERVATIONS_AFTER_CUT_OFF_TIME);

			FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, AppIndicatorEnum.APP_XBE);
			flightPriceRQ.getAvailPreferences().setAllowDoOverbookAfterCutOffTime(isAllowOverbookAfterCutOffTime);

			RequoteBalanceQueryRQ balanceQueryRQ = JSONFETOParser.parse(RequoteBalanceQueryRQ.class, balanceQueryData);

			if (bookingShoppingCart.getPriceInfoTO() != null) {
				QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(
						bookingShoppingCart.getPriceInfoTO().getFareSegChargeTO(), flightPriceRQ,
						bookingShoppingCart.getFareDiscount());
				balanceQueryRQ.setFareInfo(fareInfo);
				balanceQueryRQ.setLastFareQuoteDate(bookingShoppingCart.getPriceInfoTO().getLastFareQuotedDate());
				balanceQueryRQ.setFQWithinValidity(bookingShoppingCart.getPriceInfoTO().isFQWithinValidity());

			}

			balanceQueryRQ.setRequoteSegmentMap(ReservationBeanUtil.getReQuoteResSegmentMap(fareQuoteParams.getOndList()));
			Map<Integer, NameDTO> paxNamesMap = null;
			if (paxNameDetails != null) {
				paxNamesMap = ReservationBeanUtil.getNameChangedPaxMap(paxNameDetails);
				balanceQueryRQ.setNameChangedPaxMap(paxNamesMap);
				boolean skipDuplicateNameCheck = BasicRH.hasPrivilege(request,
						PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP);
				paxNamesMap = ReservationBeanUtil.getNameChangedPaxMap(paxNameDetails);
				boolean dupNameCheck = AppSysParamsUtil.isDuplicateNameCheckEnabled() && !skipDuplicateNameCheck;
				balanceQueryRQ.setDuplicateNameCheck(dupNameCheck);
			}

			Map<Integer, Integer> oldFareIdByFltSegIdMap = new HashMap<Integer, Integer>();

			balanceQueryRQ.setOndFareTypeByFareIdMap(
					ReservationBeanUtil.getONDFareTypeByFareIdMap(fareQuoteParams.getOndList(), oldFareIdByFltSegIdMap));

			balanceQueryRQ.setOldFareIdByFltSegIdMap(oldFareIdByFltSegIdMap);

			ReservationBalanceTO reservationBalanceTO;

			if (flightPriceRQ.getAvailPreferences().getSearchSystem().equals(SYSTEM.INT)
					|| (balanceQueryRQ.getGroupPnr() != null && !balanceQueryRQ.getGroupPnr().isEmpty())) {
				balanceQueryRQ.setSystem(SYSTEM.INT);

				balanceQueryRQ.setCarrierWiseFlightRPHMap(ReservationCommonUtil.getCarrierWiseRPHs(fareQuoteParams.getOndList()));
				balanceQueryRQ.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(balanceQueryRQ.getVersion()));
				balanceQueryRQ.setTransactionIdentifier(resInfo.getTransactionId());
				if (balanceQueryRQ.getGroupPnr() == null || balanceQueryRQ.getGroupPnr().isEmpty()) {
					balanceQueryRQ.setGroupPnr(balanceQueryRQ.getPnr());
				}
			}

			if (bookingShoppingCart.getPaxList() == null || bookingShoppingCart.getPaxList().isEmpty()) {
				Collection<ReservationPax> resPaxList = ModuleServiceLocator.getResPassengerBD()
						.getPassengers(balanceQueryRQ.getPnr(), false);
				bookingShoppingCart.setPaxList(ReservationUtil.createPaxList(resPaxList));
			}

			calculateHandlingFeeForModification(bookingShoppingCart, balanceQueryRQ);

			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = ReservationUtil.getPassengerExtChgMap(bookingShoppingCart);
			balanceQueryRQ.setPaxExtChgMap(paxExtChgMap);
			balanceQueryRQ.setExcludedSegFarePnrSegIds(fareQuoteParams.getExcludedSegFarePnrSegIds());

			reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD().getRequoteBalanceSummary(balanceQueryRQ,
					getTrackInfo());

			// Apply cc charge + jn + service tax when adminfee is enabled
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, up.getAgentCode())) {

				BigDecimal totalPaymentAmountExcludingCCCharge = reservationBalanceTO.getTotalAmountDue();
				if (totalPaymentAmountExcludingCCCharge.compareTo(BigDecimal.ZERO) > 0) {
					bookingShoppingCart.setReservationBalance(reservationBalanceTO);
					if (bookingShoppingCart.getPaxList().size() != bookingShoppingCart.getReservationBalance()
							.getPassengerSummaryList().size()) {
						updatePaxList(bookingShoppingCart);
					}
					ExternalChgDTO externalCcChgDTO = bookingShoppingCart.getAllExternalChargesMap()
							.get(EXTERNAL_CHARGES.CREDIT_CARD);
					externalCcChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
					bookingShoppingCart.addSelectedExternalCharge(externalCcChgDTO);

					// Calculate CC & Handling charge JN tax
					BigDecimal jnTaxForCC = BigDecimal.ZERO;
					jnTaxForCC = com.isa.thinair.xbe.core.web.v2.util.ReservationUtil.getApplicableServiceTax(bookingShoppingCart,
							EXTERNAL_CHARGES.JN_OTHER);

					BigDecimal ccChargeAmountWithJN = AccelAeroCalculator.add(jnTaxForCC, externalCcChgDTO.getAmount());

					// apply service tax
					BigDecimal serviceTaxAmountForCC = BigDecimal.ZERO;
					ServiceTaxCCParamsDTO serviceTaxCCParamsDTO = new ServiceTaxCCParamsDTO(fareQuoteParams,
							contactInfo.getState(), contactInfo.getCountryCode(), isPaxTaxRegistered(contactInfo.getTaxRegNo()),
							flightRPHList, balanceQueryRQ.getGroupPnr());
					serviceTaxAmountForCC = ReservationUtil.calculateServiceTaxForTransactionFee(bookingShoppingCart,
							externalCcChgDTO.getAmount(), resInfo.getTransactionId(), serviceTaxCCParamsDTO, getTrackInfo(),
							EXTERNAL_CHARGES.CREDIT_CARD, true);

					BigDecimal totalCCWithServiceTaxes = AccelAeroCalculator.add(ccChargeAmountWithJN, serviceTaxAmountForCC);

					reservationBalanceTO.setTotalAmountDue(
							AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge, totalCCWithServiceTaxes));
					reservationBalanceTO.getSegmentSummary().setNewSurchargeAmount(AccelAeroCalculator
							.add(reservationBalanceTO.getSegmentSummary().getNewSurchargeAmount(), totalCCWithServiceTaxes));

					updatePaxWiseBalanceSummaryWithAdminFee(totalCCWithServiceTaxes, reservationBalanceTO);
				}

			}

			// Make sure this is executed first to identify Anci charges
			if (paxNamesMap != null) {
				paxSummaryList = ConfirmUpdateUtil.getPaxSummaryList(reservationBalanceTO, paxNamesMap.values(),
						resInfo.isInfantPaymentSeparated());
			} else {
				paxSummaryList = ConfirmUpdateUtil.getPaxSummaryList(reservationBalanceTO, null,
						resInfo.isInfantPaymentSeparated());
			}
			// paxSummaryList = (Collection<ConfirmUpdatePaxSummaryListTO>)summObj[0];
			updateCharge = ConfirmUpdateUtil.getChargesDetails(reservationBalanceTO, bookingShoppingCart);

			overrideCharges = ConfirmUpdateUtil.getOverrideList(reservationBalanceTO, balanceQueryRQ,
					resInfo.isModifiableReservation(), rpParams.isBlnShowChargeTypeOveride());

			isAddSegment = balanceQueryRQ.isAddSegment();
		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(getMessageTxt(), e);

		}
		return strForward;
	}

	private void updatePaxList(BookingShoppingCart bookingShoppingCart) {
		Set<Integer> paxSet = new HashSet<>();
		for (ReservationPaxTO x : bookingShoppingCart.getPaxList()) {
			paxSet.add(x.getSeqNumber());
		}

		for (LCCClientPassengerSummaryTO x : bookingShoppingCart.getReservationBalance().getPassengerSummaryList()) {
			Integer paxSeq = PaxTypeUtils.getPaxSeq(x.getTravelerRefNumber());
			if (!paxSet.contains(paxSeq)) {
				ReservationPaxTO pax = new ReservationPaxTO();
				pax.setSeqNumber(paxSeq);
				pax.setTravelerRefNumber(x.getTravelerRefNumber());
				pax.setPaxType(x.getPaxType());
				if (x.hasInfant()) {
					pax.setIsParent(true);
				} else {
					pax.setIsParent(false);
				}
				bookingShoppingCart.getPaxList().add(pax);
			}
		}
	}

	private void updatePaxWiseBalanceSummaryWithAdminFee(BigDecimal totalCCWithServiceTaxes,
			ReservationBalanceTO reservationBalanceTO) {

		int payableCount = 0;
		for (LCCClientPassengerSummaryTO paxSummary : reservationBalanceTO.getPassengerSummaryList()) {
			if (paxSummary.getBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				payableCount++;
			}
		}
		if (payableCount > 0) {
			BigDecimal[] paxWiseChgAmt = AccelAeroCalculator.roundAndSplit(totalCCWithServiceTaxes, payableCount);
			int i = 0;
			for (LCCClientPassengerSummaryTO paxSummary : reservationBalanceTO.getPassengerSummaryList()) {
				if (paxSummary.getBalance().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
					BigDecimal paxAmount = paxWiseChgAmt[i++];
					paxSummary.getSegmentSummary().setNewExtraFeeAmount(
							AccelAeroCalculator.add(paxSummary.getSegmentSummary().getNewExtraFeeAmount(), paxAmount));
					paxSummary.getSegmentSummary().setNewTotalPrice(
							AccelAeroCalculator.add(paxSummary.getSegmentSummary().getNewTotalPrice(), paxAmount));
					paxSummary.setTotalAmountDue(AccelAeroCalculator.add(paxSummary.getTotalAmountDue(), paxAmount));
				}
			}
		}
	}

	private boolean isPaxTaxRegistered(String taxRegNo) {
		boolean isPaxTaxRegistered = false;
		if (taxRegNo != null && !taxRegNo.equals("")) {
			isPaxTaxRegistered = true;
		}
		return isPaxTaxRegistered;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setBalanceQueryData(String balanceQueryData) {
		this.balanceQueryData = balanceQueryData;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public ConfirmUpdateChargesTO getUpdateCharge() {
		return updateCharge;
	}

	public Collection<ConfirmUpdateOverrideChargesTO> getOverrideCharges() {
		return overrideCharges;
	}

	public Collection<ConfirmUpdatePaxSummaryListTO> getPaxSummaryList() {
		return paxSummaryList;
	}

	public boolean isAddSegment() {
		return isAddSegment;
	}

	public void setAddSegment(boolean isAddSegment) {
		this.isAddSegment = isAddSegment;
	}

	public String getPaxNameDetails() {
		return paxNameDetails;
	}

	public void setPaxNameDetails(String paxNameDetails) {
		this.paxNameDetails = paxNameDetails;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public String getResContactInfo() {
		return resContactInfo;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	private String getFirstFlightReference() {
		String flightRef = null;
		if (fareQuoteParams.getOndList() != null && fareQuoteParams.getOndList().size() > 0) {
			Iterator<ONDSearchDTO> ondSearchItr = fareQuoteParams.getOndList().iterator();
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(ondSearchDTO.getStatus())
						&& !ondSearchDTO.isFlownOnd()) {
					List<String> flightRPHList = ondSearchDTO.getFlightRPHList();
					if (flightRPHList != null && flightRPHList.size() > 0) {
						for (String flightRPH : flightRPHList) {
							return flightRPH;
						}
					}
				}

			}

		}

		return flightRef;
	}

	private boolean isNewSegmentExist() {
		if (fareQuoteParams.getOndList() != null && fareQuoteParams.getOndList().size() > 0) {
			Iterator<ONDSearchDTO> ondSearchItr = fareQuoteParams.getOndList().iterator();
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();
				if (ReservationInternalConstants.ReservationSegmentStatus.NEW_SEGMENT.equals(ondSearchDTO.getStatus())) {
					return true;
				}

			}

		}
		return false;
	}

	private int getModificationOperationMode(RequoteBalanceQueryRQ balanceQueryRQ) {
		int operationMode = CommonsConstants.ModifyOperation.MODIFY_SEGMENT.getOperation();
		if (isNewSegmentExist() && !balanceQueryRQ.hasRemovedSegments()) {
			operationMode = CommonsConstants.ModifyOperation.ADD_SEGMENT.getOperation();
		} else if (!isNewSegmentExist() && balanceQueryRQ.hasRemovedSegments() && !balanceQueryRQ.isSameFlightModification()) {
			operationMode = CommonsConstants.ModifyOperation.CANCEL_SEGMENT.getOperation();
		} else if (fareQuoteParams.isFromNameChange() && !balanceQueryRQ.getNameChangedPaxMap().isEmpty()) {
			operationMode = CommonsConstants.ModifyOperation.NAME_CHANGE.getOperation();
		}
		return operationMode;
	}

	private void calculateHandlingFeeForModification(BookingShoppingCart bookingShoppingCart,
			RequoteBalanceQueryRQ balanceQueryRQ) throws ModuleException {
		// handling fee on modification

		if (AppSysParamsUtil.applyHandlingFeeOnModification()) {
			int operationMode = getModificationOperationMode(balanceQueryRQ);
			ExternalChgDTO handlingChgDTO = ModuleServiceLocator.getReservationBD().getHandlingFeeForModification(operationMode);

			if (handlingChgDTO != null && handlingChgDTO.getAmount() != null && handlingChgDTO.getAmount().doubleValue() > 0) {
				String flightRef = getFirstFlightReference();
				if (CommonsConstants.ModifyOperation.CANCEL_SEGMENT.getOperation().intValue() == operationMode) {
					Set<String> removedSegmentIds = balanceQueryRQ.getRemovedSegmentIds();
					if (removedSegmentIds != null && !removedSegmentIds.isEmpty()) {
						flightRef = removedSegmentIds.iterator().next();
					}
				}

				BigDecimal paxWiseHandlingFee[] = AccelAeroCalculator.roundAndSplit(handlingChgDTO.getAmount(),
						bookingShoppingCart.getPaxList().size());
				int i = 0;
				for (ReservationPaxTO pax : bookingShoppingCart.getPaxList()) {
					// Remove Handling charge, mod/cnx charges override & reset functionality will duplicates the
					// handling charge
					pax.removeSelectedExternalCharge(EXTERNAL_CHARGES.HANDLING_CHARGE);

					LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
					chg.setAmount(paxWiseHandlingFee[i]);
					chg.setExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
					chg.setCode(handlingChgDTO.getChargeCode());
					chg.setFlightRefNumber(flightRef);
					chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRef));
					chg.setOperationMode(operationMode);
					pax.addExternalCharges(chg);
					i++;
				}
			}

		}

	}

}
