package com.isa.thinair.xbe.core.web.action.travagent;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = S2Constants.Result.ERROR, type = JSONResult.class, value = "") })
public class ManageMCOAction extends BaseRequestAwareAction {

	private String pnr;

	private MCO mco;
	private String mcoID;
	private String mcoNumber;
	private String paxIds;
	private String msgType;
	private String succesMsg;
	private boolean success;

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private Map<String, String> flightNoONDCodeMap;

	public static Log log = LogFactory.getLog(ManageMCOAction.class);

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		setSuccess(true);
		try {
			MCO mco = getMCO();
			if (validateMCOInputdata(mco)) {
				ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
				chargeBD.issueMCO(mco, getPayCurrencyDTO(mco.getCurrency(), mco.getAmountInLocal()), getPaymentReferenceTO(mco));
				chargeBD.emailMCO(mco.getMcoNumber(), getLoggedInUserAgentCode());
				this.mcoNumber = mco.getMcoNumber();
			}
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
			setSuccess(false);
			succesMsg = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}
		return forward;
	}

	public String voidMCO() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		setSuccess(true);
		try {
			MCO mco = ModuleServiceLocator.getChargeBD().getMCO(mcoNumber);
			Reservation reservation = ModuleServiceLocator.getReservationBD().loadReservationByPnrPaxId(mco.getPnrPaxId());
			validateForDepartureDate(reservation, mco);
			getValidPassengers(reservation);
			if (mco != null) {
				mco.setStatus(MCO.VOID);
				User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
				mco.setModifiedBy(user.getUserId());
				mco.setModifiedDate(CalendarUtil.getCurrentSystemTimeInZulu());
				ModuleServiceLocator.getChargeBD().voidMCO(mco, getPayCurrencyDTO(mco.getCurrency(), mco.getAmountInLocal()),
						getPaymentReferenceTO(mco));
			}
		} catch (ModuleException e) {
			setSuccess(false);
			msgType = S2Constants.Result.ERROR;
			succesMsg = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}
		return forward;
	}

	public String searchMCO() {
		String forward = S2Constants.Result.SUCCESS;

		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {

			HashMap<String, Object> mcoRecordMap = new HashMap<String, Object>();

			Map<String, Object> counmap = new HashMap<String, Object>();

			User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);

			if (BasicRH.hasPrivilege(request, PrivilegesKeys.MCOPrivilegesKeys.ALLOW_SEARCH_ANY)) {
				mcoRecordMap = ModuleServiceLocator.getChargeBD().getMCORecord(mcoNumber, null);
			} else if (BasicRH.hasPrivilege(request, PrivilegesKeys.MCOPrivilegesKeys.ALLOW_SEARCH_OWN)) {
				mcoRecordMap = ModuleServiceLocator.getChargeBD().getMCORecord(mcoNumber, user.getAgentCode());
			} else {
				throw new ModuleException("airpricing.empty.mco.results");
			}

			if (mcoRecordMap != null && !mcoRecordMap.isEmpty()) {
				Object[] dataRow = new Object[1];
				int index = 1;
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				counmap.put("issueDate", dateFormat.format(mcoRecordMap.get("issuedDate")));
				counmap.put("pnr", mcoRecordMap.get("pnr"));
				counmap.put("paxName",
						"" + mcoRecordMap.get("title") + " " + mcoRecordMap.get("firstName") + " " + mcoRecordMap.get("lastName"));
				counmap.put("mcoNumber", mcoRecordMap.get("mcoNumber"));
				counmap.put("mcoService", mcoRecordMap.get("service"));
				counmap.put("flightNo", mcoRecordMap.get("fltNum"));
				counmap.put("flightDate", dateFormat.format(mcoRecordMap.get("fltDeparture")));
				counmap.put("amount", mcoRecordMap.get("amount"));
				counmap.put("currency", mcoRecordMap.get("currencyCode"));
				counmap.put("remarks", mcoRecordMap.get("remarks"));
				counmap.put("email", mcoRecordMap.get("email"));
				counmap.put("status", mcoRecordMap.get("status"));
				counmap.put("mcoServiceId", mcoRecordMap.get("serviceId"));
				dataRow[index - 1] = counmap;

				this.total = index;
				this.page = 1;

				this.rows = dataRow;
				succesMsg = "";
				msgType = S2Constants.Result.SUCCESS;
			} else {
				throw new ModuleException("airpricing.empty.mco.results");
			}

		} catch (Exception e) {
			msgType = S2Constants.Result.ERROR;
			succesMsg = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}

		return forward;
	}

	private Collection<Integer> getValidPassengers(Reservation reservation) throws ModuleException {
		Collection<Integer> validPassengers = new ArrayList<Integer>();
		if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
			throw new ModuleException("xbe.arg.invalidPnrCancel");
		}
		Map<Integer, Collection<Integer>> paxWiseFlownSegments = ModuleServiceLocator.getReservationBD().getPaxwiseFlownSegments(
				reservation);
		Collection<Integer> flownByDepDate = new ArrayList<Integer>();
		for (ReservationSegmentDTO resSegmentDTO : reservation.getSegmentsView()) {
			if (resSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				if (CalendarUtil.getCurrentSystemTimeInZulu().compareTo(resSegmentDTO.getZuluDepartureDate()) > 0) {
					flownByDepDate.add(resSegmentDTO.getPnrSegId());
				}
			}
		}
		Collection<ReservationPax> paxCol = AirproxyModuleUtils.getPassengerBD().getPassengers(reservation.getPnr(), true);
		Map<Integer, Collection<Integer>> paxWiseCNFSegments = new HashMap<Integer, Collection<Integer>>();
		for (ReservationPax pax : paxCol) {
			paxWiseCNFSegments.put(pax.getPnrPaxId(), new ArrayList<Integer>());

			for (ReservationPaxFare paxFare : pax.getPnrPaxFares()) {
				for (ReservationPaxFareSegment seg : paxFare.getPaxFareSegments()) {
					if (seg.getStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED)) {
						paxWiseCNFSegments.get(pax.getPnrPaxId()).add(seg.getPnrSegId());
						if (flownByDepDate.contains(seg.getPnrSegId().intValue())) {
							paxWiseFlownSegments.get(pax.getPnrPaxId()).add(seg.getPnrSegId());
						}
					}
				}
			}
		}
		for (Entry<Integer, Collection<Integer>> entry : paxWiseCNFSegments.entrySet()) {
			Integer paxId = entry.getKey();
			Collection<Integer> segs = paxWiseFlownSegments.get(paxId);
			entry.getValue().removeAll(segs);
			if (!entry.getValue().isEmpty()) {
				validPassengers.add(paxId);
			}
		}
		if (validPassengers.isEmpty()) {
			throw new ModuleException("xbe.arg.no.future.segments");
		}
		return validPassengers;
	}

	public String loadPassengers() {
		String forward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadSegView(true);

			Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, getTrackInfo());
			Collection<ReservationPax> passengers = reservation.getPassengers();
			Collection<Integer> paxIds = getValidPassengers(reservation);
			StringBuffer sb = new StringBuffer();
			if (!passengers.isEmpty()) {
				for (ReservationPax pax : passengers) {
					if (paxIds.contains(pax.getPnrPaxId().intValue())) {
						sb.append("<option value='" + pax.getPnrPaxId() + "'>" + pax.getFirstName() + " " + pax.getLastName()
								+ "</option>");
					}
				}
				this.paxIds = sb.toString();
				msgType = S2Constants.Result.SUCCESS;
			} else {
				msgType = S2Constants.Result.ERROR;
				succesMsg = S2Constants.Result.ERROR;
				forward = S2Constants.Result.ERROR;
			}

			flightNoONDCodeMap = new HashMap<String, String>();

			for (ReservationSegmentDTO segment : reservation.getSegmentsView()) {
				flightNoONDCodeMap.put(segment.getFlightNo(), segment.getSegmentCode());
			}

		} catch (ModuleException e) {
			succesMsg = BasicRH.getErrorMessage(e, userLanguage);
			log.error("Generic exception caught.", e);
		}
		return forward;
	}

	private boolean validateMCOInputdata(MCO mco) throws ModuleException {
		Date departureDate = mco.getFlightDate();
		String flightNumber = mco.getFlightNo();
		boolean validated = false;

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);

		Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, getTrackInfo());

		if (reservation.getSegmentsView() != null || !reservation.getSegmentsView().isEmpty()) {
			ReservationSegmentDTO segment = null;
			try {
				segment = reservation
						.getSegmentsView()
						.stream()
						.filter(seg -> seg.getFlightNo().equals(flightNumber)
								&& DateUtils.isSameDay(departureDate, seg.getDepartureDate())).findFirst().get();
			} catch (NoSuchElementException ex) {
				throw new ModuleException("xbe.arg.flight.not.valid");
			}
			if (segment != null) {
				mco.setFlightDate(segment.getDepartureDate());
				validated = true;
			}

		}
		return validated;

	}

	private void validateForDepartureDate(Reservation reservation, MCO mco) throws ModuleException {
		if (reservation.getSegmentsView() != null || !reservation.getSegmentsView().isEmpty()) {
			ReservationSegmentDTO segment = null;
			try {
				segment = reservation
						.getSegmentsView()
						.stream()
						.filter(seg -> DateUtils.isSameDay(mco.getFlightDate(), seg.getDepartureDate())
								&& mco.getFlightNo().equals(seg.getFlightNo())
								&& CalendarUtil.getCurrentSystemTimeInZulu().compareTo(seg.getZuluDepartureDate()) < 0)
						.findFirst().get();
			} catch (NoSuchElementException ex) {
				throw new ModuleException("xbe.arg.no.future.segments");
			}

		}
	}

	private MCO getMCO() throws ModuleException {
		mco.setMcoNumber(ModuleServiceLocator.getChargeBD().getNextMCOSQID());
		mco.setStatus(MCO.FLOWN);
		mco.setCreatedDate(CalendarUtil.getCurrentSystemTimeInZulu());
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		mco.setCreatedBy(user.getUserId());

		ExchangeRateProxy proxy = new ExchangeRateProxy(new Date());
		CurrencyExchangeRate exchangeRate = proxy.getCurrencyExchangeRate(mco.getCurrency());
		BigDecimal amountInBase = AccelAeroRounderPolicy.convertAndRound(mco.getAmountInLocal(),
				AccelAeroCalculator.divide(BigDecimal.valueOf(1), exchangeRate.getMultiplyingExchangeRate()), null, null);
		mco.setAmount(amountInBase);
		return mco;
	}

	public static PayCurrencyDTO getPayCurrencyDTO(String currencyCode, BigDecimal amount) {
		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currencyCode, null);
		payCurrencyDTO.setTotalPayCurrencyAmount(amount);
		return payCurrencyDTO;
	}

	public static PaymentReferenceTO getPaymentReferenceTO(MCO mco) {
		PaymentReferenceTO paymentReferenceTO = new PaymentReferenceTO();
		paymentReferenceTO.setPaymentRef(mco.getMcoNumber());
		return paymentReferenceTO;
	}

	private String getLoggedInUserAgentCode() {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURRENT_USER);
		return user.getAgentCode();
	}

	public String getPnr() {
		return pnr;
	}

	public String getPaxIds() {
		return paxIds;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setPaxIds(String paxIds) {
		this.paxIds = paxIds;
	}

	public void setMcoID(String mcoID) {
		this.mcoID = mcoID;
	}

	public String getMcoNumber() {
		return mcoNumber;
	}

	public void setMcoNumber(String mcoNumber) {
		this.mcoNumber = mcoNumber;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMcoID() {
		return mcoID;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public MCO getMco() {
		return mco;
	}

	public void setMco(MCO mco) {
		this.mco = mco;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Map<String, String> getFlightNoONDCodeMap() {
		return flightNoONDCodeMap;
	}

	public void setFlightNoONDCodeMap(Map<String, String> flightNoONDCodeMap) {
		this.flightNoONDCodeMap = flightNoONDCodeMap;
	}

}
