package com.isa.thinair.xbe.core.web.generator.alerts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrSearchNotificationDTO;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;

public class FlightPnrNotificationHTMLGenerator {

	private static Log log = LogFactory.getLog(FlightPnrNotificationHTMLGenerator.class);

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	private static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";

	private static final String PARAM_PNR_LOAD_CRITERIA = "hdnLoadPnrCriteria";

	private static final String PARAM_PNR_VIEW_CRITERIA = "hdnViewPnrCriteria";

	private static final String PARAM_FLIGHT_ID = "hdnFlightId";

	private static final String PARAM_FLIGHT_PNR_DTO = "flightPnrDetailDTO";

	private static final String PARAM_FLIGHT_SEARCH_CRITERIA = "hdnSearchFlightCriteria";

	private static final String WITH_CONNECTIONS = "WC";
	private static final String WITHOUT_CONNECTIONS = "WOC";

	/**
	 * returns the PNR detail data
	 * 
	 * @return String
	 */
	public final String getPnrRowHtml(HttpServletRequest request) throws ModuleException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		Iterator it = null;
		String[] pnrSearchInfo = null;
		String multiPnrSearchInfo = null;
		String[] arrPnrSearchInfo = null;

		String flightNo = null;
		String depDate = null;
		String pnr = null;
		String searchMobileOption = null;
		String searchEmailOption = null;
		String searchLandPhoneOption = null;
		String notificationCode = null;
		String mobileNumberPattern = null;
		String strAlert = null;
		Date fltDepDate = null;
		String prefLanguage = null;
		String langOpt = null;
		String anciReprotectStatus = null;
		String sortBy = null;
		String sortByOrder = null;
		String alertFromDateString = null;
		String alertToDateString = null;
		Date alertFromDate = null;
		Date alertToDate = null;
		int noOfAdults = -1;
		int noOfChildren = -1;
		int noOfInfants = -1;
		String segmentFrom = null;
		String segmentTo = null;
		HashMap<String, Integer> countyCount = new HashMap<>(1);
		
		StringBuffer sbData = new StringBuffer();
		String arrName = "arrData";

		int count = 0;
		Collection multiPnrResults = null;

		sbData.append("var " + arrName + " = new Array();");
		multiPnrSearchInfo = request.getParameter(PARAM_PNR_LOAD_CRITERIA);

		if(multiPnrSearchInfo != null) {
			pnrSearchInfo = multiPnrSearchInfo.split("==");
			if(pnrSearchInfo.length > 0) {
				for(String pnrInfo: pnrSearchInfo) {
					arrPnrSearchInfo = pnrInfo.split(",");
					if ((arrPnrSearchInfo != null) && (arrPnrSearchInfo.length > 0)) {
						flightNo = (arrPnrSearchInfo[1].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[1].trim().toUpperCase());
						depDate = (arrPnrSearchInfo[2].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[2].trim();
						try {
							if (depDate != null)
								fltDepDate = CalendarUtil.getParsedTime(depDate, DATE_FORMAT_DDMMYYYY);
						} catch (ParseException e) {
							log.error("getPnrRowHtml() Invalid Date - Parse Exception", e);
						}
						pnr = (arrPnrSearchInfo[3].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[3].trim());
						searchMobileOption = (arrPnrSearchInfo[4].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[4].trim()
								.toUpperCase());
						searchEmailOption = (arrPnrSearchInfo[5].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[5].trim()
								.toUpperCase());
						searchLandPhoneOption = (arrPnrSearchInfo[6].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[6].trim()
								.toUpperCase());
						notificationCode = (arrPnrSearchInfo[8].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[8].trim()
								.toUpperCase());
						mobileNumberPattern = (arrPnrSearchInfo[7].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[7].trim());
						strAlert = (arrPnrSearchInfo[9] != null) ? arrPnrSearchInfo[9].trim() : null;

						prefLanguage = (arrPnrSearchInfo[10] != null) ? arrPnrSearchInfo[10].trim() : "";
						langOpt = (arrPnrSearchInfo[11] != null) ? arrPnrSearchInfo[11].trim() : "";

						prefLanguage = (arrPnrSearchInfo[10] != null) ? arrPnrSearchInfo[10].trim() : "";
						langOpt = (arrPnrSearchInfo[11] != null) ? arrPnrSearchInfo[11].trim() : "";

						anciReprotectStatus = (arrPnrSearchInfo[12] != null) ? arrPnrSearchInfo[12].trim() : "";

						sortBy = (arrPnrSearchInfo[18] != null) ? arrPnrSearchInfo[18].trim() : "";
						sortByOrder = (arrPnrSearchInfo[19] != null) ? arrPnrSearchInfo[19].trim() : "";
						
						noOfAdults = (arrPnrSearchInfo[13].trim().equals("")) ? noOfAdults : Integer.valueOf(arrPnrSearchInfo[13].trim());
						noOfChildren = (arrPnrSearchInfo[14].trim().equals("")) ? noOfChildren : Integer.valueOf(arrPnrSearchInfo[14].trim());
						noOfInfants = (arrPnrSearchInfo[15].trim().equals("")) ? noOfInfants : Integer.valueOf(arrPnrSearchInfo[15].trim());
						
						alertFromDateString = (arrPnrSearchInfo[16].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[16].trim();
						try {
							if (alertFromDateString != null)
								alertFromDate = CalendarUtil.getParsedTime(alertFromDateString, DATE_FORMAT_DDMMYYYY);
						} catch (ParseException e) {
							log.error("getPnrRowHtml() Invalid Alert Created From Date - Parse Exception", e);
						}
						
						alertToDateString = (arrPnrSearchInfo[17].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[17].trim();
						try {
							if (alertToDateString != null)
								alertToDate = CalendarUtil.getParsedTime(alertToDateString, DATE_FORMAT_DDMMYYYY);
						} catch (ParseException e) {
							log.error("getPnrRowHtml() Invalid Alert Created To Date - Parse Exception", e);
						}
						
						segmentFrom = (arrPnrSearchInfo[20].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[20].trim());
						segmentTo = (arrPnrSearchInfo[21].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[21].trim());

						if (mobileNumberPattern != null) {
							if (mobileNumberPattern.indexOf(" ") != -1)
								mobileNumberPattern = mobileNumberPattern.replaceAll(" ", "");
							if (mobileNumberPattern.indexOf("-") != -1)
								mobileNumberPattern = mobileNumberPattern.replaceAll("-", "");
						}


						FlightPnrNotificationDTO searchCriteria = new FlightPnrNotificationDTO();
						searchCriteria.setFlightNumber(flightNo);
						searchCriteria.setDepDate(fltDepDate);
						searchCriteria.setPnr(pnr);
						searchCriteria.setSearchMobileOption(searchMobileOption);
						searchCriteria.setSearchEmailOption(searchEmailOption);
						searchCriteria.setSearchLandPhoneOption(searchLandPhoneOption);
						searchCriteria.setNotificationCode(notificationCode);
						searchCriteria.setMobileNumberPattern(mobileNumberPattern);
						searchCriteria.setAlert(strAlert);
						searchCriteria.setPreferredLanguage(prefLanguage);
						searchCriteria.setLanguageOption(langOpt);
						searchCriteria.setSearchReprotectAnciOption(anciReprotectStatus);
						searchCriteria.setSortBy(sortBy);
						searchCriteria.setSortByOrder(sortByOrder);
						searchCriteria.setNoOfAdults(noOfAdults);
						searchCriteria.setNoOfChildren(noOfChildren);
						searchCriteria.setNoOfInfants(noOfInfants);
						searchCriteria.setAlertCreatedFromDate(alertFromDate);
						searchCriteria.setAlertCreatedToDate(alertToDate);
						searchCriteria.setSegmentFrom(segmentFrom);
						searchCriteria.setSegmentTo(segmentTo);
						
						

						Page pnrDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveFlightPnrDetailsForSearchCriteria(
								searchCriteria);
						Collection pnrDetails = pnrDetailsPage.getPageData();
						
						if(multiPnrResults==null) {
							multiPnrResults = pnrDetails;
						}else {
							multiPnrResults.addAll(pnrDetails);
						}
					}
				}
			}
			
			HashMap<Integer, FlightPnrNotificationDTO> hmPnrDetails = new HashMap<Integer, FlightPnrNotificationDTO>();
			String flightIds = null;
			if ((multiPnrResults != null) && (!multiPnrResults.isEmpty())) {
				sbData.append("var totNoOfPnr = " + multiPnrResults.size() + ";");
				it = multiPnrResults.iterator();
				while (it.hasNext()) {
					int flightId = -1;
					FlightPnrNotificationDTO pnrDetailDTO = (FlightPnrNotificationDTO) it.next();
					getOutBoundConnectionForPnr(pnrDetailDTO);
					if (pnrDetailDTO != null) {
						String ph = StringUtil.getNotNullString(pnrDetailDTO.getMobile());
						if (ph.split("-").length > 0) {
							CountryDetailsDTO country = ModuleServiceLocator.getLocationServiceBD()
									.getCountryByPhoneCode(ph.split("-")[0]);
							if (country != null) {
								String key = country.getCountryName() + "-" + country.getPhoneCode();
								if (countyCount.containsKey(key)) {
									countyCount.put(key, countyCount.get(key) + 1);
								} else {
									countyCount.put(key, 1);
								}
							}
						}
						sbData.append(arrName + "[" + count + "] = new Array();");
						sbData.append(arrName + "[" + count + "][1] = '"
								+ ((pnrDetailDTO.getFlightNumber() == null) ? "" : pnrDetailDTO.getFlightNumber()) + "';");
						if (pnrDetailDTO.getDepDate() == null) {
							sbData.append(arrName + "[" + count + "][2] = '" + "" + "';");
						} else {
							sbData.append(arrName + "[" + count + "][2] = '" + dateFormat.format(pnrDetailDTO.getDepDate())
									+ "';");
						}
						if (pnrDetailDTO.getAlert().equals("1"))
							sbData.append(arrName + "[" + count + "][3] = '"
									+ ((pnrDetailDTO.getPnr() == null) ? "" : pnrDetailDTO.getPnr()) + "';");
						else
							sbData.append(arrName + "[" + count + "][3] = '"
									+ ((pnrDetailDTO.getPnr() == null) ? "" : pnrDetailDTO.getPnr()) + "';");
						if (pnr == null)
							sbData.append(arrName + "[" + count + "][4] = '"
									+ ((pnrDetailDTO.getRoute() == null) ? "" : pnrDetailDTO.getRoute()) + "';");
						else {
							sbData.append(arrName
									+ "["
									+ count
									+ "][4] = '"
									+ ((pnrDetailDTO.getRoute() == null) ? "" : pnrDetailDTO.getRoute())
									+ " ["
									+ ((pnrDetailDTO.getFlightNumber() == null) ? "" : pnrDetailDTO.getFlightNumber())
									+ "-"
									+ ((pnrDetailDTO.getDepDate() == null) ? ""
											: dateFormat.format(pnrDetailDTO.getDepDate())) + "]" + "';");
						}

						sbData.append(arrName + "[" + count + "][5] = '" + getMobNumber(pnrDetailDTO.getMobile()) + "';");
						sbData.append(arrName + "[" + count + "][6] = '"
								+ ((pnrDetailDTO.getEmail() == null) ? "" : pnrDetailDTO.getEmail()) + "';");
						sbData.append(arrName + "[" + count + "][7] = '" + getMobNumber(pnrDetailDTO.getLandPhone()) + "';");
						sbData.append(arrName + "[" + count + "][8] = '"
								+ ((pnrDetailDTO.getFlightId() <= 0) ? "" : pnrDetailDTO.getFlightId()) + "';");
						sbData.append(arrName + "[" + count + "][9] = '"
								+ ((pnrDetailDTO.getPnrSegmentId() <= 0) ? "" : pnrDetailDTO.getPnrSegmentId()) + "';");
						sbData.append(arrName + "[" + count + "][10] = new Array();");
						if (pnrDetailDTO.getNotifications() != null) {
							ArrayList aList = pnrDetailDTO.getNotifications();
							for (int k = 0; k < aList.size(); k++) {
								FlightPnrNotification fltNotification = (FlightPnrNotification) aList.get(k);
								sbData.append(arrName + "[" + count + "][10][" + k + "] = new Array();");
								sbData.append(arrName
										+ "["
										+ count
										+ "][10]["
										+ k
										+ "][1] = '"
										+ ((fltNotification.getFlightNotificationId() <= 0) ? "" : fltNotification
												.getFlightNotificationId()) + "';");
								sbData.append(arrName
										+ "["
										+ count
										+ "][10]["
										+ k
										+ "][2] = '"
										+ ((fltNotification.getNotificationChannel() == null) ? "" : fltNotification
												.getNotificationChannel()) + "';");
							}
						}
						sbData.append(arrName + "[" + count + "][11]='" + pnrDetailDTO.getTotalPax() + "';");
						sbData.append(arrName + "[" + count + "][12]='" + pnrDetailDTO.getNoOfAdults() + "';");
						sbData.append(arrName + "[" + count + "][13]='" + pnrDetailDTO.getNoOfChildren() + "';");
						sbData.append(arrName + "[" + count + "][14]='" + pnrDetailDTO.getNoOfInfants() + "';");
						if(pnrDetailDTO.getAlertCreatedActualDate()==null){
							sbData.append(arrName + "[" + count + "][15]='';");
						}else{
							sbData.append(arrName + "[" + count + "][15]='" + dateFormat.format(pnrDetailDTO.getAlertCreatedActualDate()) + "';");
						}
						if (pnrDetailDTO.getInBoundFlightNo() != null) {
							sbData.append(arrName
									+ "["
									+ count
									+ "][16]='"
									+ pnrDetailDTO.getInBoundFlightNo()
									+ ":"
									+ ((pnrDetailDTO.getInBoundDepartureStation() == null) ? "" : pnrDetailDTO
											.getInBoundDepartureStation())
									+ ":"
									+ ((pnrDetailDTO.getInBoundDepartureDate() == null) ? "" : pnrDetailDTO
											.getInBoundDepartureDate()) + "';");
						} else {
							sbData.append(arrName + "[" + count + "][16]='';");
						}

						if(pnrDetailDTO.getOutBoundFlightNo() != null) {
							sbData.append(arrName
									+ "["
									+ count
									+ "][17]='"
									+ pnrDetailDTO.getOutBoundFlightNo()
									+ ":"
									+ ((pnrDetailDTO.getOutBoundSegmentCode() == null) ? "" : pnrDetailDTO
											.getOutBoundSegmentCode())
									+ ":"
									+ ((pnrDetailDTO.getOutBoundDepartureDate() == null) ? "" : pnrDetailDTO
											.getOutBoundDepartureDate()) + "';");
						}else {
							sbData.append(arrName + "[" + count + "][17]='';");
						}
						String mobLandAppendString = ((getMobNumber(pnrDetailDTO.getMobile()) != "")
								&& (getMobNumber(pnrDetailDTO.getLandPhone()) != "")) ? ", " : "";
						String mobileLand = getMobNumber(pnrDetailDTO.getMobile()) + mobLandAppendString
								+ getMobNumber(pnrDetailDTO.getLandPhone());
						sbData.append(arrName + "[" + count + "][18] = '" + ((mobileLand.equals(""))?"":mobileLand.replaceAll("-", "")) +  "';");
						sbData.append(arrName + "[" + count + "][19] = '" + pnrDetailDTO.getFlightNumber() +  "';");
						sbData.append(arrName + "[" + count + "][20] = '" + pnrDetailDTO.getDepDate() +  "';");
						sbData.append(arrName + "[" + count + "][21] = '" + getCountryCountList(countyCount) + "';");

						count++;
						
						hmPnrDetails.put(new Integer(pnrDetailDTO.getPnrSegmentId()), pnrDetailDTO);
						flightId = pnrDetailDTO.getFlightId();
						if(flightIds == null) {
							flightIds = String.valueOf(flightId);
						}else {
							if(!flightIds.contains(String.valueOf(flightId)))
								flightIds += "," + String.valueOf(flightId);
						}
					}
				}
				
				// request.getSession().setAttribute("flightPnrDetailDTO",hmPnrDetails);
				request.setAttribute(PARAM_FLIGHT_PNR_DTO, hmPnrDetails);
				request.setAttribute(PARAM_FLIGHT_ID, flightIds);
			}
		}
		return sbData.toString();
	}

	/**
	 * returns the Notification Summary data
	 * 
	 * @return String
	 */
	public final String getNotificationSummaryRowHtml(HttpServletRequest request) throws ModuleException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		Iterator it = null;
		String pnrSearchInfo = null;
		String[] arrPnrSearchInfo = null;

		String flightNo = null;
		String depDate = null;
		String pnr = null;
		String strFromDate = null;
		String strToDate = null;
		String fltNotificationId = null;
		String ancriReprotectStatus = null;

		Date fltDepDate = null;
		Date fromDate = null;
		Date toDate = null;

		StringBuffer sbData = new StringBuffer();
		String arrName = "arrSummaryData";

		int count = 0;

		pnrSearchInfo = request.getParameter(PARAM_PNR_VIEW_CRITERIA);
		if (pnrSearchInfo != null) {
			arrPnrSearchInfo = pnrSearchInfo.split(",");
			if ((arrPnrSearchInfo != null) && (arrPnrSearchInfo.length > 0)) {
				flightNo = (arrPnrSearchInfo[1].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[1].trim().toUpperCase());
				depDate = (arrPnrSearchInfo[2].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[2].trim();
				strFromDate = (arrPnrSearchInfo[4].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[4].trim();
				strToDate = (arrPnrSearchInfo[5].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[5].trim();
				ancriReprotectStatus = (arrPnrSearchInfo[7].trim().equalsIgnoreCase("")) ? null : arrPnrSearchInfo[7].trim();
				try {
					if (depDate != null)
						fltDepDate = CalendarUtil.getParsedTime(depDate, DATE_FORMAT_DDMMYYYY);
					if (strFromDate != null)
						fromDate = CalendarUtil.getParsedTime(strFromDate, DATE_FORMAT_DDMMYYYY);
					if (strToDate != null)
						toDate = CalendarUtil.getParsedTime(strToDate, DATE_FORMAT_DDMMYYYY);

				} catch (ParseException e) {
					log.error("getPnrRowHtml() Invalid Date - Parse Exception", e);
				}
				pnr = (arrPnrSearchInfo[3].trim().equalsIgnoreCase("") ? null : arrPnrSearchInfo[3].trim());
				fltNotificationId = (arrPnrSearchInfo[6].trim().equalsIgnoreCase("-1") ? null : arrPnrSearchInfo[6].trim());

				FlightPnrNotificationDTO searchCriteria = new FlightPnrNotificationDTO();
				searchCriteria.setFlightNumber(flightNo);
				searchCriteria.setDepDate(fltDepDate);
				searchCriteria.setPnr(pnr);
				searchCriteria.setDateFrom(fromDate);
				searchCriteria.setDateTo(toDate);
				searchCriteria.setSearchReprotectAnciOption(ancriReprotectStatus);
				if (fltNotificationId != null)
					searchCriteria.setFlightNotificationId(Integer.parseInt(fltNotificationId));

				Page pnrDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveFlightPnrDetailsForViewSearchCriteria(
						searchCriteria);
				Collection pnrDetails = pnrDetailsPage.getPageData();

				int flightId = -1;
				String strPnr = searchCriteria.getPnr();

				if (flightNo != null && fltDepDate != null) {
					flightId = ModuleServiceLocator.getAlertingBD().getFlightID(flightNo, fltDepDate);
				}

				sbData.append("var " + arrName + " = new Array();");

				if ((pnrDetails != null) && (!pnrDetails.isEmpty())) {
					it = pnrDetails.iterator();
					while (it.hasNext()) {
						FlightPnrNotificationDTO pnrDetailDTO = (FlightPnrNotificationDTO) it.next();
						if (pnrDetailDTO != null) {
							sbData.append(arrName + "[" + count + "] = new Array();");
							sbData.append(arrName + "[" + count + "][1] = '"
									+ ((pnrDetailDTO.getFlightNumber() == null) ? "" : pnrDetailDTO.getFlightNumber()) + "';");
							if (pnrDetailDTO.getDepDate() == null) {
								sbData.append(arrName + "[" + count + "][2] = '" + "" + "';");
							} else {
								sbData.append(arrName + "[" + count + "][2] = '" + dateFormat.format(pnrDetailDTO.getDepDate())
										+ "';");
							}
							sbData.append(arrName + "[" + count + "][3] = '" + ((strPnr == null) ? "" : strPnr) + "';");
							if (pnrDetailDTO.getNotificationtDate() == null) {
								sbData.append(arrName + "[" + count + "][4] = '" + "" + "';");
							} else {
								sbData.append(arrName + "[" + count + "][4] = '"
										+ dateTimeFormat.format(pnrDetailDTO.getNotificationtDate()) + "';");
							}
							sbData.append(arrName + "[" + count + "][5] = '"
									+ ((pnrDetailDTO.getNotificationCode() == null) ? "" : pnrDetailDTO.getNotificationCode())
									+ "';");
							String msg = StringUtil.escapeStr(pnrDetailDTO.getNotificationMsg(), "'", "\\\\'");
							msg = StringUtil.escapeStr(msg, "\n|\r|\r\n", " ");
							sbData.append(arrName + "[" + count + "][6] = '"
									+ ((pnrDetailDTO.getNotificationMsg() == null) ? "" : msg) + "';");
							sbData.append(arrName + "[" + count + "][7] = '" + pnrDetailDTO.getTotalSms() + "';");
							sbData.append(arrName + "[" + count + "][8] = '" + pnrDetailDTO.getTotalSmsDelivered() + "/"
									+ pnrDetailDTO.getTotalSmsFailed() + "';");
							sbData.append(arrName + "[" + count + "][9] = '" + pnrDetailDTO.getTotalEmail() + "';");
							sbData.append(arrName + "[" + count + "][10] = '" + pnrDetailDTO.getTotalEmailDelivered() + "/"
									+ pnrDetailDTO.getTotalEmailFailed() + "';");
							sbData.append(arrName + "[" + count + "][11] = '" + pnrDetailDTO.getFlightNotificationId() + "';");

							count++;

						}
					}
				}
				request.setAttribute(PARAM_FLIGHT_ID, String.valueOf(flightId));

			}

		}
		return sbData.toString();
	}

	/**
	 * returns the Notification Details data
	 * 
	 * @return String
	 */
	public final String getNotificationDetailRowHtml(HttpServletRequest request) throws ModuleException {

		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		String strParam = request.getParameter("hdnFlightNotificationId");
		if (strParam == null || "".equalsIgnoreCase(strParam))
			return "";

		String[] arrParams = strParam.split(",");
		String hdnFlightNotificationId = (arrParams[0] != null && !"".equals(arrParams[0])) ? arrParams[0] : "-1";
		String hdnNotificationDate = (arrParams[1].trim().equalsIgnoreCase("")) ? null : arrParams[1].trim();
		String hdnPnr = StringUtil.isNullOrEmpty(request.getParameter("hdnPnr")) ? "" : request.getParameter("hdnPnr");
		Date notifyDate = null;

		try {
			if (hdnNotificationDate != null)
				notifyDate = CalendarUtil.getParsedTime(hdnNotificationDate, DATE_TIME_FORMAT);

		} catch (ParseException e) {
			log.error("getNotificationDetailRowHtml() Invalid Date - Parse Exception", e);
		}

		Page pnrDetailsPage = ModuleServiceLocator.getAlertingBD().retrieveDetailedNotificationList(
				Integer.parseInt(hdnFlightNotificationId), notifyDate, hdnPnr);
		Collection pnrDetails = pnrDetailsPage.getPageData();

		Iterator it = null;
		StringBuffer sbData = new StringBuffer();
		String arrName = "arrDetailData";
		String smsDeliveryStatus = "";
		String emailDeliveryStatus = "";

		int count = 0;

		sbData.append("var " + arrName + " = new Array();");

		if ((pnrDetails != null) && (!pnrDetails.isEmpty())) {

			it = pnrDetails.iterator();
			while (it.hasNext()) {
				FlightPnrNotificationDTO pnrDetailDTO = (FlightPnrNotificationDTO) it.next();
				if (pnrDetailDTO != null) {

					HashMap hmap = (HashMap) pnrDetailDTO.getDeliveryStatus();
					smsDeliveryStatus = (String) hmap.get("SMS");
					emailDeliveryStatus = (String) hmap.get("EMAIL");

					sbData.append(arrName + "[" + count + "] = new Array();");
					if (pnrDetailDTO.getNotificationtDate() == null) {
						sbData.append(arrName + "[" + count + "][1] = '" + "" + "';");
					} else {
						sbData.append(arrName + "[" + count + "][1] = '" + dateFormat.format(pnrDetailDTO.getNotificationtDate())
								+ "';");
					}
					sbData.append(arrName + "[" + count + "][2] = '"
							+ ((pnrDetailDTO.getNotificationCode() == null) ? "" : pnrDetailDTO.getNotificationCode()) + "';");
					sbData.append(arrName + "[" + count + "][3] = '"
							+ ((pnrDetailDTO.getPnr() == null) ? "" : pnrDetailDTO.getPnr()) + "';");
					sbData.append(arrName + "[" + count + "][4] = '"
							+ ((pnrDetailDTO.getMobile() == null) ? "" : pnrDetailDTO.getMobile()) + "';");
					sbData.append(arrName
							+ "["
							+ count
							+ "][5] = '"
							+ ((smsDeliveryStatus == null) ? "" : (smsDeliveryStatus.equals("D") ? "Delivered"
									: (smsDeliveryStatus.equals("F") ? "Failed" : "Sent"))) + "';");
					sbData.append(arrName + "[" + count + "][6] = '"
							+ ((pnrDetailDTO.getEmail() == null) ? "" : pnrDetailDTO.getEmail()) + "';");
					sbData.append(arrName
							+ "["
							+ count
							+ "][7] = '"
							+ ((emailDeliveryStatus == null) ? "" : (emailDeliveryStatus.equals("D") ? "Delivered"
									: (emailDeliveryStatus.equals("F") ? "Failed" : "Sent"))) + "';");
					sbData.append(arrName + "[" + count + "][8] = '"
							+ ((pnrDetailDTO.getLandPhone() == null) ? "" : pnrDetailDTO.getLandPhone()) + "';");
					sbData.append(arrName + "[" + count + "][9] = '"
							+ ((pnrDetailDTO.getRoute() == null) ? "" : pnrDetailDTO.getRoute()) + "';");
					sbData.append(arrName + "[" + count + "][10] = '" + pnrDetailDTO.getFlightNotificationId() + "';");

					count++;

				}
			}
		}

		return sbData.toString();
	}

	/**
	 * Return flight summary details for pnr notification search
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public final String getFlightPnrRowHtml(HttpServletRequest request) throws ModuleException {
		String flightSearchInfo = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		Iterator it = null;
		String[] arrFlightSearchInfo = null;

		Date fltDepFromDate = null;
		Date fltDepToDate = null;
		Date fltAlertFromDate = null;
		Date fltAlertToDate = null;

		int count = 0;

		StringBuffer sbData = new StringBuffer();
		String arrName = "arrSearchData";
		sbData.append("var " + arrName + " = new Array();");

		flightSearchInfo = request.getParameter(PARAM_FLIGHT_SEARCH_CRITERIA);

		if (flightSearchInfo != null) {
			arrFlightSearchInfo = flightSearchInfo.split(",");
			if ((arrFlightSearchInfo != null) && (arrFlightSearchInfo.length > 0)) {
				String pnr = (arrFlightSearchInfo[1].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[1].trim());
				String flightNo = (arrFlightSearchInfo[2].trim().equalsIgnoreCase("") ? null
						: arrFlightSearchInfo[2].trim().toUpperCase());
				String depFromDate = (arrFlightSearchInfo[3].trim().equalsIgnoreCase("")) ? null : arrFlightSearchInfo[3].trim();
				String depToDate = (arrFlightSearchInfo[4].trim().equalsIgnoreCase("")) ? null : arrFlightSearchInfo[4].trim();
				String segFrom = (arrFlightSearchInfo[5].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[5].trim().toUpperCase());
				String segTo = (arrFlightSearchInfo[6].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[6].trim().toUpperCase());
				String specifyDelay = (arrFlightSearchInfo[7].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[7].trim().toUpperCase());
				String notificationCode = (arrFlightSearchInfo[8].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[8].trim().toUpperCase());
				String alertFlag = (arrFlightSearchInfo[9].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[9].trim().toUpperCase());
				String alertFromdate = (arrFlightSearchInfo[10].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[10].trim());
				String alertToDate = (arrFlightSearchInfo[11].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[11].trim());
				String daysOfOperation = (arrFlightSearchInfo[12].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[12].trim());
				String sortBy = (arrFlightSearchInfo[13].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[13].trim().toUpperCase());
				String sortByOrder = (arrFlightSearchInfo[14].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[14].trim().toUpperCase());
				String anciReprotectStatus = arrFlightSearchInfo[15].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[15].trim();
				String connectionFilter = arrFlightSearchInfo[16].trim().equalsIgnoreCase("") ? null : arrFlightSearchInfo[16].trim();

				try {
					if (depFromDate != null)
						fltDepFromDate = CalendarUtil.getParsedTime(depFromDate, DATE_FORMAT_DDMMYYYY);
					if (depToDate != null)
						fltDepToDate = CalendarUtil.getParsedTime(depToDate, DATE_FORMAT_DDMMYYYY);
					if (alertFromdate != null)
						fltAlertFromDate = CalendarUtil.getParsedTime(alertFromdate, DATE_FORMAT_DDMMYYYY);
					if (alertToDate != null)
						fltAlertToDate = CalendarUtil.getParsedTime(alertToDate, DATE_FORMAT_DDMMYYYY);
				} catch (ParseException e) {
					log.error("getFlightPnrRowHtml() Invalid Date - Parse Exception", e);
				}
				
				if (daysOfOperation != null) {
					daysOfOperation = daysOfOperation.replaceAll("/", ",");
				}
				
				FlightPnrSearchNotificationDTO flightSearchCriteria = new FlightPnrSearchNotificationDTO();
				flightSearchCriteria.setFlightNumber(flightNo);
				flightSearchCriteria.setPnr(pnr);
				flightSearchCriteria.setFlightFromDate(fltDepFromDate);
				flightSearchCriteria.setFlightToDate(fltDepToDate);
				flightSearchCriteria.setSegmentFrom(segFrom);
				flightSearchCriteria.setSegmentTo(segTo);
				flightSearchCriteria.setSpecifyDelay(getSpecificDelayInMinutes(specifyDelay));
				flightSearchCriteria.setNotificationCode(notificationCode);
				flightSearchCriteria.setAlertFlag(alertFlag);
				flightSearchCriteria.setAlertCreatedFromDate(fltAlertFromDate);
				flightSearchCriteria.setAlertCreatedToDate(fltAlertToDate);
				flightSearchCriteria.setSortBy(sortBy);
				flightSearchCriteria.setSortByOrder(sortByOrder);
				flightSearchCriteria.setDaysOfOperation(daysOfOperation);
				flightSearchCriteria.setSearchReprotectAnciOption(anciReprotectStatus);

				Page flightPnrDetailsPage = ModuleServiceLocator.getAlertingBD()
						.retrieveFlightDetailsForSearchCriteria(flightSearchCriteria);
				Collection flightPnrDetails = flightPnrDetailsPage.getPageData();

				HashMap<Integer, FlightPnrSearchNotificationDTO> hmFlightDetails = new HashMap<Integer, FlightPnrSearchNotificationDTO>();
				int flightId = -1;

				sbData.append("var " + arrName + " = new Array();");

				sbData.append("var totNoOfFlights = " + flightPnrDetailsPage.getTotalNoOfRecords() + ";");

				if ((flightPnrDetails != null) && (!flightPnrDetails.isEmpty())) {
					it = flightPnrDetails.iterator();
					while (it.hasNext()) {
						FlightPnrSearchNotificationDTO flightSearchDetailsDTO = (FlightPnrSearchNotificationDTO) it.next();
						if (flightSearchDetailsDTO != null) {
							getOutBoundConnectionList(flightSearchDetailsDTO);
							if (isFilterByConnection(flightSearchDetailsDTO, connectionFilter)) {
								sbData.append(arrName + "[" + count + "] = new Array();");
								sbData.append(
										arrName + "[" + count + "][1] = '" + ((flightSearchDetailsDTO.getFlightNumber() == null) ? ""
												: flightSearchDetailsDTO.getFlightNumber()) + "';");
								if (flightSearchDetailsDTO.getDepDate() == null) {
									sbData.append(arrName + "[" + count + "][2] = '" + "" + "';");
									sbData.append(arrName + "[" + count + "][10] = '" + "" + "';");
								} else {
									sbData.append(arrName + "[" + count + "][2] = '"
											+ dateFormat.format(flightSearchDetailsDTO.getDepDate()) + "';");
									sbData.append(arrName + "[" + count + "][10] = '"
											+ flightSearchDetailsDTO.getOriginalFlightTime() + "';");
								}

								sbData.append(arrName + "[" + count + "][3] = '"
										+ ((flightSearchDetailsDTO.getRoute() == null) ? "" : flightSearchDetailsDTO.getRoute())
										+ "';");

								sbData.append(arrName + "[" + count + "][4]='" + flightSearchDetailsDTO.getNoOfAdults() + "';");
								sbData.append(arrName + "[" + count + "][5]='" + flightSearchDetailsDTO.getNoOfChildren() + "';");
								sbData.append(arrName + "[" + count + "][6]='" + flightSearchDetailsDTO.getNoOfInfants() + "';");

								if (flightSearchDetailsDTO.getInBoundConnections() != null
										|| flightSearchDetailsDTO.getOutBoundConnections() != null) {
									String[] inBoundRoutes = (flightSearchDetailsDTO.getInBoundConnections() != null)
											? flightSearchDetailsDTO.getInBoundConnections().split(",")
											: null;
									String[] outBoundRoutes = (flightSearchDetailsDTO.getOutBoundConnections() != null)
											? flightSearchDetailsDTO.getOutBoundConnections().split(",")
											: null;
									int totalCount = ((inBoundRoutes != null) ? inBoundRoutes.length : 0)
											+ ((outBoundRoutes != null) ? outBoundRoutes.length : 0);

									String inBoundString = getConnectionsString(inBoundRoutes, "I");
									String outBoundString = getConnectionsString(outBoundRoutes, "O");

									sbData.append(arrName + "[" + count + "][7]='" + totalCount + "  "
											+ ((inBoundString == null) ? "" : inBoundString) + " "
											+ ((outBoundString == null) ? "" : outBoundString) + "';");
								} else {
									sbData.append(arrName + "[" + count + "][7]='';");
								}

								if (flightSearchDetailsDTO.getAlertCreatedDate() == null) {
									sbData.append(arrName + "[" + count + "][8]='';");
								} else {
									sbData.append(
											arrName + "[" + count + "][8]='" + flightSearchDetailsDTO.getAlertCreatedDate() + "';");
								}
								sbData.append(arrName + "[" + count + "][9]='" + flightSearchDetailsDTO.getTotalPnrCount() + "';");

								count++;
							}

						}
					}
				}

			}

		}
		return sbData.toString();
	}

	public void updateReservationContactData(HttpServletRequest request) {
		String pnr = request.getParameter("pnr");
		String land = request.getParameter("land");
		String mobile = request.getParameter("mobile");
		String email = request.getParameter("email");
		Reservation reservation = ModuleServiceLocator.getReservationBD().getReservation(pnr, false);
		reservation.getContactInfo().setPhoneNo(land);
		reservation.getContactInfo().setMobileNo(mobile);
		reservation.getContactInfo().setEmail(email);
		reservation.getContactInfo().setPnr(pnr);
		ModuleServiceLocator.getReservationBD().updateReservationContactData(reservation.getContactInfo());
	}
	
	private String getMobNumber(String str) {
		String strMob = "";
		if (str != null) {
			if (str.trim().length() > 5) {
				strMob = str;
			}
		}
		for (int i = 0; i < strMob.length(); i++) {
			if (strMob.startsWith("0")) {
				strMob = strMob.substring(1, strMob.length());
			} else {
				return strMob;
			}
		}
		return strMob;
	}

	private void getOutBoundConnectionList(FlightPnrSearchNotificationDTO flpnDTO) throws ModuleException {
		Iterator it = null;
		int flightId = flpnDTO.getFlightId();
		String departureStation = flpnDTO.getRoute().substring(0, 3);
		String[] pnrArray = flpnDTO.getPnr().split(",");
		for (String pnr : pnrArray) {
			Collection outBound = ModuleServiceLocator.getReservationAuxilliaryBD().getAllOutBoundSequences(pnr, 0,
					departureStation, flightId);
			if (outBound != null) {
				it = outBound.iterator();
				while (it.hasNext()) {
					OnWardConnectionDTO onward = (OnWardConnectionDTO) it.next();
					if(flpnDTO.getOutBoundConnections() != null) {
						flpnDTO.setOutBoundConnections(flpnDTO.getOutBoundConnections() + "," + onward.getSegmentCode().substring(0, 3));
					}else {
						flpnDTO.setOutBoundConnections(onward.getSegmentCode().substring(0, 3));
					}
				}
			}

		}
	}

	private String getConnectionsString(String[] connectionRoutes, String code) {
		String connectionString = null;
		if (connectionRoutes != null) {
			connectionString = "";
			HashMap<String, Integer> routes = new HashMap<>();
			for (String route : connectionRoutes) {
				if (routes.containsKey(route)) {
					routes.put(route, routes.get(route) + 1);
				} else {
					routes.put(route, 1);
				}
			}
			for (String route : routes.keySet()) {
				connectionString += route + "-" + routes.get(route) + "(" + code + ") ";
			}
		}
		return connectionString;
	}
	
	private Long getSpecificDelayInMinutes(String specificDelay) {
		Long specDelay = 0l;
		if (specificDelay != null && !(specificDelay.trim().isEmpty()) ) {

			specDelay = (Long.parseLong(specificDelay.split(":")[0]) * 24 * 60 + Long.parseLong(specificDelay.split(":")[1]) * 60 + Long
					.parseLong(specificDelay.split(":")[2]));
		}
		return specDelay;
	}

	private void getOutBoundConnectionForPnr(FlightPnrNotificationDTO flpnDTO) throws ModuleException {
		Iterator it = null;
		int flightId = flpnDTO.getFlightId();
		String departureStation = flpnDTO.getRoute().substring(0, 3);
		String pnr = flpnDTO.getPnr();
		Collection outBound = ModuleServiceLocator.getReservationAuxilliaryBD().getAllOutBoundSequences(pnr, 0,
				departureStation, flightId);
		if (outBound != null) {
			it = outBound.iterator();
			while (it.hasNext()) {
				OnWardConnectionDTO onward = (OnWardConnectionDTO) it.next();
				if(onward != null) {
					flpnDTO.setOutBoundFlightNo(onward.getFlight());
					flpnDTO.setOutBoundDepartureDate(onward.getDepartureDate().toString());
					flpnDTO.setOutBoundSegmentCode(onward.getSegmentCode());
				}
			}
		}

	}

	private String getCountryCountList(HashMap<String, Integer> countyCount) {
        Set<String> keys = countyCount.keySet();
        String list = "";
        for (String key : keys) {
            if ("".equalsIgnoreCase(list)) {
                list = list + key + "-" + countyCount.get(key);
            } else {
                list = list + "," + key + "-" + countyCount.get(key);
            }
        }

        return list;
    }

	private boolean isFilterByConnection(FlightPnrSearchNotificationDTO fpsnDTO, String connFilterStr) {
		if (connFilterStr.equals(WITH_CONNECTIONS)) {
			return ((fpsnDTO.getInBoundConnections() != null) || (fpsnDTO.getOutBoundConnections() != null));
		} else if (connFilterStr.equals(WITHOUT_CONNECTIONS)) {
			return ((fpsnDTO.getInBoundConnections() == null) && (fpsnDTO.getOutBoundConnections() == null));
		} else {
			return true;
		}
	}
}
