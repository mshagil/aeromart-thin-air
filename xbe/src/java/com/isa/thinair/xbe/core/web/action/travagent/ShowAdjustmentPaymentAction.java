package com.isa.thinair.xbe.core.web.action.travagent;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.travagent.AgentCollectionRequestHandler;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.TravAgent.ADJUSTMENT_PAYMENT_GRID),
		@Result(name = S2Constants.Result.ADJUSTED_PAYMENT, value = S2Constants.Jsp.TravAgent.ADJUSTMENT_PAYMENT_GRID) })
public class ShowAdjustmentPaymentAction extends BaseRequestAwareAction {

	public String execute() throws ModuleException {
		return AgentCollectionRequestHandler.execute(request);
	}
}
