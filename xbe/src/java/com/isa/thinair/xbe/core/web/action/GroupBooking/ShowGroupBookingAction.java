package com.isa.thinair.xbe.core.web.action.GroupBooking;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.GroupBooking.GroupBookingRH;

@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.GroupBooking.GRP_BOOKING_URL),
		@Result(name = S2Constants.Result.ACTION_FORWARD_SUCCESS_AJAX, value = S2Constants.Jsp.Common.LOADAJAX_JSP),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowGroupBookingAction extends BaseRequestAwareAction {

	public String execute() {
		return GroupBookingRH.execute(request);
	}

}
