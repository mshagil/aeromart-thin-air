package com.isa.thinair.xbe.core.web.v2.action.voucher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.handler.voucher.VoucherRequestHandler;

/**
 * @author chanaka
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Voucher.ISSUE_VOUCHER_FOR_REPROTECTED_PAX),
		@Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowReprotectedPaxAction extends BaseRequestAwareAction {
	
	private static Log log = LogFactory.getLog(ShowReprotectedPaxAction.class);	
	
	public String execute() {
		return VoucherRequestHandler.execute(request);
	}	

}
