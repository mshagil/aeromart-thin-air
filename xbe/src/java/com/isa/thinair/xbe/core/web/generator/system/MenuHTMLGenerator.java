package com.isa.thinair.xbe.core.web.generator.system;

import java.io.File;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.v2.util.I18nUtil;

public class MenuHTMLGenerator {

	private static Log log = LogFactory.getLog(MenuHTMLGenerator.class);

	private static String clientErrors;

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	public static String createTermsConditionsHtml(HttpServletRequest request) throws ModuleException {
		/**
		 * also need to translate
		 */
		StringBuffer strData = new StringBuffer();

		strData.append("Terms & Conditions");

		return strData.toString();
	}

	// menu 1
	public static String createMenuHtml(String menuFile, HashMap hMPriviledgesIDs, String language) throws ModuleException {
		StringBuffer strData = new StringBuffer();
		strData.append(readXMLFile(menuFile, hMPriviledgesIDs,language));
		return strData.toString();
	}

	public static String getClientErrors(HttpServletRequest request) {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			// error messages
			moduleErrs.setProperty("cc_common", "XBE-ERR-00");
			moduleErrs.setProperty("cc_null", "XBE-ERR-01");
			moduleErrs.setProperty("cc_no_child_only_resrvations", "TAIR-90144");
			moduleErrs.setProperty("cc_compare", "XBE-ERR-02");
			moduleErrs.setProperty("cc_lessthan", "XBE-ERR-03");
			moduleErrs.setProperty("cc_invalid", "XBE-ERR-04");
			moduleErrs.setProperty("cc_higherthan", "XBE-ERR-05");
			moduleErrs.setProperty("cc_flight", "XBE-ERR-06");
			moduleErrs.setProperty("cc_max", "XBE-ERR-07");
			moduleErrs.setProperty("cc_nullfor", "XBE-ERR-08");
			moduleErrs.setProperty("cc_travellingwithadult.same", "XBE-ERR-09");
			moduleErrs.setProperty("cc_Infantdob.age", "XBE-ERR-10");
			moduleErrs.setProperty("cc_credit.search", "XBE-ERR-11");
			moduleErrs.setProperty("cc_invalid.char", "XBE-ERR-12");
			moduleErrs.setProperty("cc_min_length", "XBE-ERR-13");
			moduleErrs.setProperty("cc_not_equal", "XBE-ERR-14");
			moduleErrs.setProperty("cc_not_pax_count", "XBE-ERR-15");
			moduleErrs.setProperty("cc_serch_min_letters", "XBE-ERR-16");
			moduleErrs.setProperty("cc_serch_Child", "XBE-ERR-17");
			moduleErrs.setProperty("cc_max_textarea", "XBE-ERR-18");
			moduleErrs.setProperty("cc_payemnt_chk", "XBE-ERR-19");
			moduleErrs.setProperty("cc_email", "XBE-ERR-20");
			moduleErrs.setProperty("cc_DD", "XBE-ERR-21");
			moduleErrs.setProperty("cc_exceeded", "XBE-ERR-22");
			moduleErrs.setProperty("cc_not_selected", "XBE-ERR-23");
			moduleErrs.setProperty("cc_not_specified", "XBE-ERR-24");
			moduleErrs.setProperty("cc_not_tally", "XBE-ERR-25");
			moduleErrs.setProperty("cc_not_phone", "XBE-ERR-26");
			moduleErrs.setProperty("cc_not_tranfer_time", "XBE-ERR-27");
			moduleErrs.setProperty("cc_cannot_onhold", "XBE-ERR-28");
			moduleErrs.setProperty("cc_no_fares", "XBE-ERR-29");
			moduleErrs.setProperty("cc_card_pay_not_selected", "XBE-ERR-30");
			moduleErrs.setProperty("cc_card_no_not_matched", "XBE-ERR-31");
			moduleErrs.setProperty("cc_apply_credit_current_pnr", "XBE-ERR-32");
			moduleErrs.setProperty("cc_sys_down", "XBE-ERR-33");
			moduleErrs.setProperty("cc_name_numbers", "XBE-ERR-34");
			moduleErrs.setProperty("cc_paaaword_change_restart", "XBE-ERR-35");
			moduleErrs.setProperty("cc_resmod_not_enough_adults", "XBE-ERR-39");
			moduleErrs.setProperty("cc_resmod_select_pax_remove", "XBE-ERR-40");
			moduleErrs.setProperty("cc_resmod_select_pax_split", "XBE-ERR-41");
			moduleErrs.setProperty("cc_resmod_select_time_extend", "XBE-ERR-42");
			moduleErrs.setProperty("cc_resmod_select_date_extend", "XBE-ERR-43");
			moduleErrs.setProperty("cc_resmod_select_pax_extend", "XBE-ERR-44");
			moduleErrs.setProperty("cc_resmod_enter_atleast_one_phone", "XBE-ERR-45");
			moduleErrs.setProperty("cc_resmod_email_invalid", "XBE-ERR-46");
			moduleErrs.setProperty("cc_resmod_select_apply_credit", "XBE-ERR-47");
			moduleErrs.setProperty("cc_resmod_payment_not_tally", "XBE-ERR-48");
			moduleErrs.setProperty("cc_resmod_addinfant_nosplit", "XBE-ERR-49");
			moduleErrs.setProperty("cc_paaaword_invalidComb", "XBE-ERR-50");
			moduleErrs.setProperty("cc_negative", "XBE-ERR-51");
			moduleErrs.setProperty("cc_onhold_timestamp_cannot_extend", "XBE-ERR-52");
			moduleErrs.setProperty("cc_resmod_select_time_lower", "XBE-ERR-53");
			moduleErrs.setProperty("cc_payemnt_no_option", "XBE-ERR-54");
			moduleErrs.setProperty("cc_resmod_select_pax", "XBE-ERR-55");
			moduleErrs.setProperty("cc_decimal_points_exceeded", "XBE-ERR-56");
			moduleErrs.setProperty("cc_select_same_route", "XBE-ERR-57");
			moduleErrs.setProperty("cc_amount_exceed", "XBE-ERR-58");
			moduleErrs.setProperty("cc_amount_invalid", "XBE-ERR-59");
			moduleErrs.setProperty("cc_date_invalid", "XBE-ERR-60");
			moduleErrs.setProperty("cc_phone_wrongcomb", "XBE-ERR-61");
			moduleErrs.setProperty("cc_date_past_date", "XBE-ERR-62");

			moduleErrs.setProperty("cc_dob_exceed", "XBE-ERR-63");
			moduleErrs.setProperty("cc_dob_notmatch", "XBE-ERR-64");
			moduleErrs.setProperty("cc_date_past_date_null", "XBE-ERR-65");
			moduleErrs.setProperty("cc_exchange_rate_undefined_card", "XBE-ERR-66");
			moduleErrs.setProperty("cc_foid_id_exist", "XBE-ERR-67");
			moduleErrs.setProperty("cc_foid_duplicate_id", "XBE-ERR-68");
			moduleErrs.setProperty("cc_modify_inward_outward", "XBE-ERR-69");
			moduleErrs.setProperty("cc_dob_less", "XBE-ERR-70");
			moduleErrs.setProperty("cc_phone_cc_wrongcmb", "XBE-ERR-73");
			moduleErrs.setProperty("cc_flight_no_fares_available", "XBE-ERR-75");
			moduleErrs.setProperty("cc_flight_cannot_cross_select", "XBE-ERR-76");
			moduleErrs.setProperty("cc_flight_no_flight_on_selected_date", "XBE-ERR-77");
			moduleErrs.setProperty("cc_booking_category_empty", "XBE-ERR-78");
			moduleErrs.setProperty("cc_duplicate_names_in_flights", "XBE-ERR-79");
			moduleErrs.setProperty("cc_duplicate_names_in_flights_skip", "XBE-ERR-80");
			moduleErrs.setProperty("cc_duplicate_names_skip", "XBE-ERR-72");
			moduleErrs.setProperty("cc_resmod_refund_cannot_exceed_pay_amount", "XBE-ERR-81");
			moduleErrs.setProperty("cc_resmod_ohd_roll_forward_not_allow_cnx", "XBE-ERR-82");
			moduleErrs.setProperty("cc_resmod_ohd_roll_forward_not_allow_multi_cabin", "XBE-ERR-83");
			moduleErrs.setProperty("cc_free_seat_notification", "XBE-ERR-84");
			moduleErrs.setProperty("cc_free_meal_notification", "XBE-ERR-85");
			moduleErrs.setProperty("cc_fromdate_exeeeds_ToDate", "XBE-ERR-86");
			moduleErrs.setProperty("cc_resmod_select_priority_change", "XBE-ERR-87");
			moduleErrs.setProperty("cc_promo_bin_not_match", "XBE-ERR-88");
			moduleErrs.setProperty("cc_resmod_select_date_extends_dept_date", "XBE-ERR-89");
			moduleErrs.setProperty("cc_modified_travel_with_require_save", "XBE-ERR-90");
			moduleErrs.setProperty("cc_flight_no_flight_on_selected_date_for_ond", "XBE-ERR-91");
			moduleErrs.setProperty("cc_flight_fares_not_available_for_all_flights", "XBE-ERR-92");
			moduleErrs.setProperty("um.problem.countryCode.mismatch", "XBE-ERR-93");
			moduleErrs.setProperty("cc_names_doesnt_match", "XBE-ERR-95");
			moduleErrs.setProperty("cc_ffid_doesnt_exist", "XBE-ERR-96");
			moduleErrs.setProperty("cc_ffid_not_allow_for_tba", "XBE-ERR-97");			
			moduleErrs.setProperty("cc_resmod_refund_non_refundable_payment", "XBE-ERR-98");
			moduleErrs.setProperty("cc_flight_departure_before_other_arrive", "XBE-ERR-99");
			moduleErrs.setProperty("reservation.add.usernote.faild", "XBE-ERR-100");
			moduleErrs.setProperty("cc_ffid_enter_ffid", "XBE-ERR-101");
			moduleErrs.setProperty("cc_resmod_modify_contact_country_not_allowed", "XBE-ERR-102");
			moduleErrs.setProperty("cc_invalid_tba", "XBE-ERR-103");

			// confirmation messages
			moduleErrs.setProperty("cc_success_msg", "XBE-CONF-01");
			moduleErrs.setProperty("cc_confirm_onhold", "XBE-CONF-02");
			moduleErrs.setProperty("cc_confirm_split", "XBE-CONF-03");
			moduleErrs.setProperty("cc_warn_page_close", "XBE-CONF-04");
			moduleErrs.setProperty("cc_auto_cancel", "XBE-CONF-05");
			moduleErrs.setProperty("cc_confirm_remove_agent_commission", "XBE-CONF-06");
			moduleErrs.setProperty("cc_confirm_proceed_rename_multi_pax_reservation", "XBE-CONF-07");
			moduleErrs.setProperty("reservation.add.usernote.success", "XBE-CONF-08");

			// warning messages
			moduleErrs.setProperty("cc_warn_page_edited", "XBE-WARN-01");
			moduleErrs.setProperty("cc_warn_seat_blocked", "XBE-WARN-02");
			moduleErrs.setProperty("cc_warn_alert_too_many", "XBE-WARN-03");

			moduleErrs.setProperty("msg.fltSearch.spMsg", "XBE-MSG-01");
			moduleErrs.setProperty("msg.fltSearch.spMsgCC3", "XBE-MSG-02");
			moduleErrs.setProperty("msg.fltSearch.spMsgCCMB", "XBE-MSG-03");

			// Haider 28Apr09
			moduleErrs.setProperty("cc_warn_no_confirm_with_insurance", "XBE-WARN-04");
			moduleErrs.setProperty("cc_warn_no_force_with_insurance", "XBE-WARN-05");
			moduleErrs.setProperty("cc_warn_all_pax_with_insurance", "XBE-WARN-06");

			moduleErrs.setProperty("cc_warn_id_pending_for_verification", "XBE-WARN-07");
			moduleErrs.setProperty("cc_duplicate_Names", "XBE-ERR-71");
			moduleErrs.setProperty("cc.airadmin.user.notAuthorized", "XBE-ERR-74");

			// Auto cancellation alerts
			moduleErrs.setProperty("um.resmod.auto.cancel.segment", "XBE-ALERT-01");
			moduleErrs.setProperty("um.resmod.auto.cancel.infant", "XBE-ALERT-02");
			moduleErrs.setProperty("um.resmod.auto.cancel.seat", "XBE-ALERT-03");
			moduleErrs.setProperty("um.resmod.auto.cancel.meal", "XBE-ALERT-04");
			moduleErrs.setProperty("um.resmod.auto.cancel.baggage", "XBE-ALERT-05");
			moduleErrs.setProperty("um.resmod.auto.cancel.ssr", "XBE-ALERT-06");
			moduleErrs.setProperty("um.resmod.auto.cancel.aps", "XBE-ALERT-07");
			moduleErrs.setProperty("um.resmod.auto.cancel.insurance", "XBE-ALERT-08");
			moduleErrs.setProperty("um.resmod.auto.cancel.bal.to.pay", "XBE-ALERT-09");
			moduleErrs.setProperty("um.resmod.auto.cancel.bal.credit", "XBE-ALERT-10");

			moduleErrs.setProperty("lbl.discount.total", "XBE-LABEL-01");
			moduleErrs.setProperty("lbl.credit.discount.total", "XBE-LABEL-02");
			moduleErrs.setProperty("lbl.discount.free.service", "XBE-LABEL-03");
			moduleErrs.setProperty("lbl.credit.discount.free.service", "XBE-LABEL-04");

			// TF last used no : 80

			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	// menu 2
	/*
	 * Reading the Menu xml file
	 */
	private static String readXMLFile(String menuFile, HashMap hMPriviledgesIDs, String language) {

		StringBuffer strReturnDataMenu = new StringBuffer();

		try {

			String strFileName = menuFile;
			int intCount = 0;
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File(strFileName));

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("r");
			int intRecords = listOfRecords.getLength();
			String strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			
			
			HashMap<String, String> menuData = I18nUtil.getMenuData(language);

			String strMT = "";
			String strMID = "";
			String strDes = "";
			String strFID = "";
			String strSep = "";
			String strURL = "";
			String strSL = "";
			String strPrev = "";
			String appParamKey = "";
			String strDescriptionKey = "";

			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				NamedNodeMap attributes = menuNode.getAttributes();

				strMT = attributes.getNamedItem("C1").getNodeValue().toString();
				strMID = attributes.getNamedItem("C2").getNodeValue().toString();
				strDescriptionKey = attributes.getNamedItem("C11").getNodeValue().toString();	//Menu data key
				strDes = menuData.get(strDescriptionKey); 
				
				if(strDes == null || strDes.isEmpty()){
					
					strDes = attributes.getNamedItem("C3").getNodeValue().toString(); //default
				}
				
				strFID = attributes.getNamedItem("C4").getNodeValue().toString();
				strSep = attributes.getNamedItem("C5").getNodeValue().toString();
				strURL = attributes.getNamedItem("C6").getNodeValue().toString();
				appParamKey = attributes.getNamedItem("C10") == null ? null : attributes.getNamedItem("C10").getNodeValue()
						.toString();

				if (strURL != null && strURL.indexOf("#") != -1) {
					int start = strURL.indexOf("#", 0);
					int end = strURL.indexOf("#", start + 1);
					if (start != -1 && end != -1) {
						String replaceStr = strURL.substring(start + 1, end);
						if (replaceStr != null && replaceStr.equals("ADMIN_URL")) {
							strURL = strURL.substring(0, start) + XBEModuleUtils.getGlobalConfig().getAdminServerAddressAndPort()
									+ strURL.substring(end + 1);
						} else if (replaceStr != null && replaceStr.equals("EXTERNAL_AGENT_URL")) {
							strURL = AppSysParamsUtil.getExternalAgentURL();
						}
					}
				}
				strSL = attributes.getNamedItem("C7").getNodeValue().toString();
				strPrev = attributes.getNamedItem("C9").getNodeValue().toString();
				strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;";

				boolean blnFound = true;
				if (!strMT.equals("M")) {
					blnFound = checkAvailability(i, intRecords, strMID, listOfRecords, hMPriviledgesIDs);
				}

				if (blnFound) {
					if (strSL.equals("")) {
						strSL = "menu-top";
						strSpace = "";
					}

					// Sub Menu ----------------
					if (strMT.equals("S")) {
						strReturnDataMenu.append(buildMenuArray(intCount, "S", strSL, strDes + strSpace, strURL, "", strMID));
						intCount++;
						if (strSep.equals("1")) {
							if (checkSepAvailability(i, intRecords - 1, strSL, listOfRecords, hMPriviledgesIDs)) {
								strReturnDataMenu.append(buildMenuArray(intCount, "L", strSL, "", "", "", ""));
								intCount++;
							}
						}
					}

					// Menu -------------------
					if (strMT.equals("M")) {
						if (strSL.equals("menu-top")) {
							strMID = strSL;
						}
						if (checkFID(strPrev, hMPriviledgesIDs, appParamKey)) {
							strReturnDataMenu.append(buildMenuArray(intCount, "M", strMID, strDes, strURL, strFID, ""));
							intCount++;

							if (strSep.equals("1")) {
								if (checkSepAvailability(i, intRecords - 1, strSL, listOfRecords, hMPriviledgesIDs)) {
									strReturnDataMenu.append(buildMenuArray(intCount, "L", strMID, "", "", "", ""));
									intCount++;
								}
							}
						}
					}
				}
			}

		} catch (SAXParseException err) {
			log.error("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId(), err);
		} catch (Throwable t) {
			log.error(t);
		}
		return strReturnDataMenu.toString();
	}

	/*
	 * Check availability for the menu
	 */
	private static boolean checkAvailability(int intStart, int intLength, String strMenuID, NodeList nodeList,
			HashMap hMPriviledgesIDs) {
		boolean blnReturn = false;
		String[] arrVal = strMenuID.split("0");
		intStart = intStart + 1;
		for (int x = intStart; x < intLength; x++) {
			NamedNodeMap attributes = nodeList.item(x).getAttributes();
			String strMID = attributes.getNamedItem("C2").getNodeValue().toString();
			String strPrev = attributes.getNamedItem("C9").getNodeValue().toString();
			String appParamKey = attributes.getNamedItem("C10") == null ? null : attributes.getNamedItem("C10").getNodeValue()
					.toString();
			String[] arrCVal = strMID.split("0");
			if (arrCVal[0].length() >= arrVal[0].length()) {
				if (arrCVal[0].substring(0, arrVal[0].length()).equals(arrVal[0])
						&& attributes.getNamedItem("C1").getNodeValue().toString().equals("M")) {
					if (checkFID(strPrev, hMPriviledgesIDs, appParamKey)) {
						blnReturn = true;
						break;
					}
				}
			}
		}
		return blnReturn;
	}

	/*
	 * Check menu links availabel for the below areas
	 */
	private static boolean checkSepAvailability(int intStart, int intLength, String strMenuID, NodeList nodeList,
			HashMap hMPriviledgesIDs) {
		boolean blnReturn = false;

		intStart = intStart + 1;
		for (int x = intStart; x < intLength; x++) {
			String strSL = nodeList.item(x).getAttributes().getNamedItem("C7").getNodeValue().toString();
			String strMID = nodeList.item(x).getAttributes().getNamedItem("C2").getNodeValue().toString();
			String strMT = nodeList.item(x).getAttributes().getNamedItem("C1").getNodeValue().toString();
			if (strSL.equals(strMenuID)) {
				if (strMT.equals("M")) {
					blnReturn = true;
					break;
				} else {
					if (checkAvailability(x, intLength, strMID, nodeList, hMPriviledgesIDs)) {
						blnReturn = true;
						break;
					}
				}
			}
		}
		return blnReturn;
	}

	/*
	 * Add the menu array
	 */
	private static String buildMenuArray(int intCount, String strMenuType, String strMenuID, String strMenuDesc,
			String strCallPage, String strFID, String strMGroup) {
		StringBuffer strReturnData = new StringBuffer();
		strReturnData.append("arrMenu[" + intCount + "] = new Array();");
		strReturnData.append("arrMenu[" + intCount + "][0] = '" + strMenuType + "';");
		strReturnData.append("arrMenu[" + intCount + "][1] = '" + strMenuID + "';");
		strReturnData.append("arrMenu[" + intCount + "][2] = '" + strMenuDesc + "';");
		strReturnData.append("arrMenu[" + intCount + "][3] = '" + strCallPage + "';");
		strReturnData.append("arrMenu[" + intCount + "][4] = '" + strFID + "';");
		strReturnData.append("arrMenu[" + intCount + "][5] = '" + strMGroup + "';");
		return strReturnData.toString();
	}

	/*
	 * Check for Function access
	 */
	private static boolean checkFID(String strPrevID, HashMap hMPriviledgesIDs, String appParamKey) {
		boolean blnReturn = true;
		if (!StringUtil.isNullOrEmpty(strPrevID)) {
			if (hMPriviledgesIDs != null && hMPriviledgesIDs.containsKey(strPrevID)) {
				blnReturn = true;
			} else {
				blnReturn = false;
			}
		} else if (!StringUtil.isNullOrEmpty(appParamKey)) {
			if ("Y".equalsIgnoreCase(globalConfig.getBizParam(appParamKey))) {
				blnReturn = true;
			} else {
				blnReturn = false;
			}
		} else {
			blnReturn = true;
		}
		return blnReturn;
	}
}
