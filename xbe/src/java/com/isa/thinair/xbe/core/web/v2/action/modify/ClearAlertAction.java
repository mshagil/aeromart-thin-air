package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientClearAlertDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * Alert clear functionality
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ClearAlertAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ClearAlertAction.class);

	private boolean success = true;

	private String messageTxt;

	private LCCClientClearAlertDTO clearAlert;

	private String segmentRefNo;

	private String carrirCode;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			boolean isGroupPNR = ReservationUtil.isGroupPnr(clearAlert.getGroupPNR());
			if (isGroupPNR) {
				clearAlert.setPNR(clearAlert.getGroupPNR());
			}
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			ReservationProcessParams rparam = new ReservationProcessParams(request,
					resInfo != null ? resInfo.getReservationStatus() : null, null, resInfo.isModifiableReservation(), false, null, null, false, false);
			if (isGroupPNR) {
				rparam.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), null, resInfo.isModifiableReservation(), null);
			}
			if (rparam.isClearAlert()) {
				String strAlerts = request.getParameter("resAlerts");
				Map<Integer, String> segmentCarrier = new HashMap<Integer, String>();
				for (String segmentId : segmentRefNo.split(",")){
				segmentCarrier.put(new Integer(segmentId.trim()), carrirCode);
				}
				clearAlert.setPnrSegIds(segmentCarrier);
				clearAlert.setAlertContent(ReservationUtil.getSegmentAlertContent(segmentRefNo,
						ReservationUtil.transformJsonAlerts(strAlerts)));
				ModuleServiceLocator.getAirproxySegmentBD().clearSegmentAlerts(clearAlert,
						TrackInfoUtil.getBasicTrackInfo(request));
			} else {
				throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
			}
			clearAlert = null;
		} catch (Exception e) {
			clearAlert = null;
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * 
	 * @return the messageTxt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * @param clearAlert
	 *            the clearAlert to set
	 */
	public void setClearAlert(LCCClientClearAlertDTO clearAlert) {
		this.clearAlert = clearAlert;
	}

	public LCCClientClearAlertDTO getClearAlert() {
		return clearAlert;
	}

	/**
	 * @param segmentRefNo
	 *            the segmentRefNo to set
	 */
	public void setSegmentRefNo(String segmentRefNo) {
		this.segmentRefNo = segmentRefNo;
	}

	/**
	 * @param carrirCode
	 *            the carrirCode to set
	 */
	public void setCarrirCode(String carrirCode) {
		this.carrirCode = carrirCode;
	}

}
