package com.isa.thinair.xbe.core.web.v2.action.system;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.SystemParams;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.config.XBEModuleUtils;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.generator.AgentTicketStockHG;
import com.isa.thinair.xbe.core.web.v2.util.AirportDDListGenerator;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

/**
 * @author Thushara Fernando
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({
		@Result(name = S2Constants.Jsp.Reservation.V2_SEARCH_RESERVATION, value = S2Constants.Jsp.Reservation.V2_SEARCH_RESERVATION),
		@Result(name = S2Constants.Jsp.Reservation.V2_CONFIRMUPDATE, value = S2Constants.Jsp.Reservation.V2_CONFIRMUPDATE),
		@Result(name = S2Constants.Jsp.Reservation.V2_PAX_DETAILS, value = S2Constants.Jsp.Reservation.V2_PAX_DETAILS),
		@Result(name = S2Constants.Jsp.Reservation.V2_INTERNATIONAL_FLIGHT_DETAILS, value = S2Constants.Jsp.Reservation.V2_INTERNATIONAL_FLIGHT_DETAILS),
		@Result(name = S2Constants.Jsp.Reservation.V2_ADVANCED_PASSENGER_SELECTION_FOR_SPLIT, value = S2Constants.Jsp.Reservation.V2_ADVANCED_PASSENGER_SELECTION_FOR_SPLIT),
		@Result(name = S2Constants.Jsp.Reservation.V2_ACCOUNTS, value = S2Constants.Jsp.Reservation.V2_ACCOUNTS),
		@Result(name = S2Constants.Jsp.Reservation.V2_RESERVATION, value = S2Constants.Jsp.Reservation.V2_RESERVATION),
		@Result(name = S2Constants.Jsp.Reservation.V2_DISPLAY_FARE_RULES, value = S2Constants.Jsp.Reservation.V2_DISPLAY_FARE_RULES),
		@Result(name = S2Constants.Jsp.Reservation.V2_CREDIT, value = S2Constants.Jsp.Reservation.V2_CREDIT),
		@Result(name = S2Constants.Jsp.Reservation.V2_DISPLAY_TAXES_SURCHARGES, value = S2Constants.Jsp.Reservation.V2_DISPLAY_TAXES_SURCHARGES),
		@Result(name = S2Constants.Jsp.Reservation.V2_CHANGE_FARE, value = S2Constants.Jsp.Reservation.V2_CHANGE_FARE),
		@Result(name = S2Constants.Jsp.Reservation.V2_MULTYCITY_SEARCH, value = S2Constants.Jsp.Reservation.V2_MULTYCITY_SEARCH),
		@Result(name = S2Constants.Jsp.Reservation.V2_E_TICKET_MASK, value = S2Constants.Jsp.Reservation.V2_E_TICKET_MASK),
		@Result(name = S2Constants.Jsp.Reservation.V2_SEARCH_SHEDULES, value = S2Constants.Jsp.Reservation.V2_SEARCH_SHEDULES),
		@Result(name = S2Constants.Jsp.Common.DASHBOARD_UTIL_POPUP, value = S2Constants.Jsp.Common.DASHBOARD_UTIL_POPUP),
		@Result(name = S2Constants.Jsp.Reservation.V2_AGENT_TICKET_STOCK, value = S2Constants.Jsp.Reservation.V2_AGENT_TICKET_STOCK),
		@Result(name = S2Constants.Jsp.Reservation.V2_CREATE_ALERT, value = S2Constants.Jsp.Reservation.V2_CREATE_ALERT),
		@Result(name = S2Constants.Jsp.Reservation.V2_PAX_NAMES_IN_OTHER_LANGUAGES, value = S2Constants.Jsp.Reservation.V2_PAX_NAMES_IN_OTHER_LANGUAGES),
		@Result(name = S2Constants.Jsp.Reservation.V2_USERNOTES, value = S2Constants.Jsp.Reservation.V2_USERNOTES),
		@Result(name = S2Constants.Jsp.Reservation.V2_MCO_DETAILS, value = S2Constants.Jsp.Reservation.V2_MCO_DETAILS),
		@Result(name = S2Constants.Jsp.TypeB.TYPEB_MESSAGES, value = S2Constants.Jsp.TypeB.TYPEB_MESSAGES),
		@Result(name = S2Constants.Jsp.Reservation.V2_GROUP_PAX_STATUS_UPDATE, value = S2Constants.Jsp.Reservation.V2_GROUP_PAX_STATUS_UPDATE)})
public class ShowNewFileAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowNewFileAction.class);

	public String searchRes() throws JSONException {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		boolean isGdsEnabled = BasicRH.hasPrivilege(request,
				PriviledgeConstants.PRIVI_ACC_GDS_RES);
		request.setAttribute("load", request.getParameter("load"));
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("searchByETicket", AppSysParamsUtil.isShowPaxETKT());
		request.setAttribute("gdsEnabled", isGdsEnabled);
		return S2Constants.Jsp.Reservation.V2_SEARCH_RESERVATION;
	}

	public String loadRes() throws JSONException {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));

		SystemParams sysParams = new SystemParams();
		sysParams.setDefaultLanguage(XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
		request.setAttribute("systemParams", JSONUtil.serialize(sysParams));

		request.setAttribute("pnr", request.getParameter("pnrNO"));
		request.setAttribute("groupPNR", request.getParameter("groupPNRNO"));
		request.setAttribute("marketingAirlineCode", request.getParameter("marketingAirlineCode"));
		request.setAttribute("interlineAgreementId", request.getParameter("interlineAgreementId"));
		request.setAttribute("airlineCode", request.getParameter("airlineCode"));
		request.setAttribute("forwardmessage", request.getParameter("forwardmessage"));
		request.setAttribute("taxRegistrationNumberLabel", AppSysParamsUtil.getTaxRegistrationNumberLabel());

		request.setAttribute("focMealEnabled",
				XBEModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.FOC_MEAL_SELECTION_ENABLED));
		try {
			request.setAttribute("reqPayAgents", ReservationUtil.getPaymentAgentOptions(request));
		} catch (Exception e) {
			// don't fail
		}
		return S2Constants.Jsp.Reservation.V2_RESERVATION;
	}

	public String loadConfirmUpdate() throws JSONException {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, resInfo != null
				? resInfo.isGdsReservationModAllowed()
				: false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("confirmUpdateMode", request.getParameter("confirmUpdateMode"));
		return S2Constants.Jsp.Reservation.V2_CONFIRMUPDATE;
	}

	public String loadPaxDetails() throws JSONException {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("mode", request.getParameter("mode"));
		request.setAttribute("isDomesticFlightExist", request.getParameter("isDomesticFlightExist"));
		request.setAttribute("isGdsPnr", request.getParameter("isGdsPnr"));

		
		String param = "var isDuplicatePassportNumberCheckOnlyWithinAdult = "
				+ AppSysParamsUtil.isCheckDuplicatePassportNumberOnlyWithinAdults() + ";";
		request.setAttribute("reqDuplicatePassportNumberCheckOnlyWithinAdult", param);

		return S2Constants.Jsp.Reservation.V2_PAX_DETAILS;
	}
	
	public String loadExternalInternationalFlightDetails() throws JSONException {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("mode", request.getParameter("mode"));
		
		return S2Constants.Jsp.Reservation.V2_INTERNATIONAL_FLIGHT_DETAILS;
	}
	
	public String loadAdvancedPassengerSelectionForSplit() throws JSONException {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("mode", request.getParameter("mode"));

		return S2Constants.Jsp.Reservation.V2_ADVANCED_PASSENGER_SELECTION_FOR_SPLIT;
	}

	public String loadCreateAlert() throws JSONException {
		try {
			request.setAttribute("reqPnrSegIds", request.getParameter("pnrSegIds"));
			request.setAttribute("reqPnr", request.getParameter("pnr"));

		} catch (Exception e) {
			log.error("Error occured while loading Create Alert Page", e);
		}
		return S2Constants.Jsp.Reservation.V2_CREATE_ALERT;
	}

	public String loadPaxNamesInOtherLanguages() throws Exception {
		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("mode", request.getParameter("mode"));
		String strHtml = SelectListGenerator.createLanguageList();
		request.setAttribute("reqLanguageList", strHtml);

		return S2Constants.Jsp.Reservation.V2_PAX_NAMES_IN_OTHER_LANGUAGES;
	}

	public String loadPaxAccount() throws JSONException {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		request.setAttribute("reqPaxID", request.getParameter("paxID"));

		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		Set<String> agentcolMethod = null;
		try {
			request.setAttribute("reqPayAgents", ReservationUtil.getRefundAgentOptions(request));
			Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());
			agentcolMethod = agent.getPaymentMethod();
			request.setAttribute(WebConstants.REQ_HTML_ALL_IPGS_LIST, SelectListGenerator.createAllIPGsForXBE("arrIPGs"));
		} catch (Exception e) {
			// no need to fail the operation
		}

		request.setAttribute(
				"creditCardRefundAllowed",
				(agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_CREDITCARD))
						&& rpParams.isAllowCreditCardRefund());
		request.setAttribute("cashRefundAllowed", (agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_CASH))
				&& rpParams.isAllowCashRefund());
		request.setAttribute("offlineRefundAllowed", (agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_OFFLINE))
				&& rpParams.isAllowOfflineRefund());
		request.setAttribute(
				"onAccountRefundAllowed",
				agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_ONACCOUNT)
						&& rpParams.isAllowOnAccountRefund());
		request.setAttribute("bspRefundAllowed",
				(agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_BSP) && rpParams.isAllowBspRefund()));
		request.setAttribute("onAccountReportingRefundAllowed",
				(agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_ONACCOUNT) && rpParams
						.isAllowOnAccountRptRefund()));
		request.setAttribute("anyOnAccountRefundAllowed",
				(agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_ONACCOUNT) && rpParams
						.isAllowAnyOnAccountRefund()));
		request.setAttribute("anyOpCarrierOnAccountRefundAllowed",
				(agentcolMethod != null && agentcolMethod.contains(Agent.PAYMENT_MODE_ONACCOUNT) && rpParams
						.isAllowAnyOpCarrierOnAccountRefund()));
		request.setAttribute("noCreditRefundAllowed", rpParams.isAllowRefundNoCredit());
		request.setAttribute("anyCarrierRefundAllowed", rpParams.isAllowAnyCarrierRefund());

		request.setAttribute("currentCarrierCode", AppSysParamsUtil.getDefaultCarrierCode());
		request.setAttribute("allowRecieptPrinting", rpParams.isAllowRecieptPrinting());
		request.setAttribute("isVoidReservation",
				ReservationInternalConstants.ReservationStatus.VOID.equals(resInfo.getReservationStatus()));
		request.setAttribute("stopRefundTotalAmountWhenNoshowRulesApplied",
				AppSysParamsUtil.stopRefundingTotalAmountWhenNoshowRulesApplied());

		return S2Constants.Jsp.Reservation.V2_ACCOUNTS;
	}

	public String loadETicketMask() throws JSONException {
		request.setAttribute("reqPaxID", request.getParameter("paxID"));

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		// set segment status
		request.setAttribute(WebConstants.REQ_HTML_SEGMENT_STATUS, SelectListGenerator.createSegmentStatusList());

		return S2Constants.Jsp.Reservation.V2_E_TICKET_MASK;
	}

	public String loadPaxMCODetails() throws JSONException {
		request.setAttribute("reqPaxID", request.getParameter("paxID"));

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession()
				.getAttribute(S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request,
				resInfo != null ? resInfo.getReservationStatus() : null, null,
				resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));
		// set segment status
		request.setAttribute(WebConstants.REQ_HTML_SEGMENT_STATUS, SelectListGenerator.createSegmentStatusList());

		return S2Constants.Jsp.Reservation.V2_MCO_DETAILS;
	}
	
	public String loadGroupPaxStatusUpdate() throws JSONException {

		XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
				S2Constants.Session_Data.XBE_SES_RESDATA);
		ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo != null
				? resInfo.getReservationStatus()
				: null, null, resInfo != null ? resInfo.isModifiableReservation() : true, false, null, null, false, false);
		request.setAttribute("initialParams", JSONUtil.serialize(rpParams));

		return S2Constants.Jsp.Reservation.V2_GROUP_PAX_STATUS_UPDATE;
	}

	public String searchFlightShedules() throws JSONException {
		boolean allowRouteWiseAgentSelection = AppSysParamsUtil.isEnableRouteSelectionForAgents();
		boolean viewAvailableCredit = BasicRH.hasPrivilege(request, PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_VIEW_CREDIT);
		request.setAttribute("agentWiseRouteSelection", allowRouteWiseAgentSelection);
		request.setAttribute("viewAvailableCredit", viewAvailableCredit);
		return S2Constants.Jsp.Reservation.V2_SEARCH_SHEDULES;
	}

	public String loadAgentTicketStock() throws JSONException {
		request.setAttribute("reqAgentCode", request.getParameter("reqAgentCode"));
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, AgentTicketStockHG.getClientErrors(request));
		} catch (Exception e) {
			log.error(" Exception occured while loading error messages ");
		}
		// set e ticket status
		return S2Constants.Jsp.Reservation.V2_AGENT_TICKET_STOCK;
	}

	public String displayFareRules() {
		return S2Constants.Jsp.Reservation.V2_DISPLAY_FARE_RULES;
	}

	public String displayTaxesSurcharges() {
		return S2Constants.Jsp.Reservation.V2_DISPLAY_TAXES_SURCHARGES;
	}

	public String loadCredit() {
		request.setAttribute("lccEnabled", AppSysParamsUtil.isLCCConnectivityEnabled());
		return S2Constants.Jsp.Reservation.V2_CREDIT;
	}

	public String displayChangeFares() {
		return S2Constants.Jsp.Reservation.V2_CHANGE_FARE;
	}

	public String loadUtilPopup() {
		request.setAttribute("dateValidity", request.getParameter("dv"));
		request.setAttribute("extCurrency", request.getParameter("exCur"));
		request.setAttribute("workclock", request.getParameter("wc"));
		request.setAttribute("mealAvl", request.getParameter("mlAvl"));
		request.setAttribute("baggageAvl", request.getParameter("bgAvl"));

		if (request.getParameter("bgAvl") != null && !request.getParameter("bgAvl").isEmpty()
				&& request.getParameter("bgAvl").equals("true")) {

			if (AppSysParamsUtil.isDynamicOndListPopulationEnabled()) {
				request.setAttribute("isOndJsEnabled", "true");
				request.setAttribute("sysOndSource", AppSysParamsUtil.getDynamicOndListUrl());
			} else {
				request.setAttribute("isOndJsEnabled", "false");
			}
			request.setAttribute("defaultLanguage", XBEModuleUtils.getGlobalConfig()
					.getBizParam(SystemParamKeys.DEFAULT_LANGUAGE));
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_OWNER_LIST, JavaScriptGenerator.createAirportOwnerList(request));
			request.setAttribute("carrierCode", AppSysParamsUtil.getDefaultCarrierCode());

			boolean isSubStationsAllowed = AppSysParamsUtil.isGroundServiceEnabled();
			try {
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST_V2,
						AirportDDListGenerator.prepareAirportsHtml(request, null, "arrAirportsV2", isSubStationsAllowed));
				request.setAttribute(WebConstants.REQ_HTML_COS_WITH_LOGICAL_DATA,
						JavaScriptGenerator.createCosWithLogicalHtml(request));
				setSearchOptionhtml(request);
			} catch (Exception e) {
				log.error("Error occured while generating airports,search options", e);
			}
		} else if (request.getParameter("mlAvl") != null && !request.getParameter("mlAvl").isEmpty()
				&& request.getParameter("mlAvl").equals("true")) {
			try {
				request.setAttribute(WebConstants.REQ_HTML_COS_WITH_LOGICAL_DATA,
						JavaScriptGenerator.createCosWithLogicalHtml(request));
			} catch (Exception e) {
				log.error("Error occured while generating class of services", e);
			}
		}

		return S2Constants.Jsp.Common.DASHBOARD_UTIL_POPUP;
	}

	public String loadUserNote() {
		boolean isGroupPNR = false;
		boolean isClassifyUN = BasicRH.hasPrivilege(request, PrivilegesKeys.MakeResPrivilegesKeys.CLASSIFY_USER_NOTE);
		String pnr = request.getParameter("pnr");
		String groupPNR = request.getParameter("groupPNR");
		String originSalesChanel = request.getParameter("originSalesChanel");
		if (groupPNR != null && !groupPNR.trim().equals("") && !groupPNR.trim().equalsIgnoreCase("null")) {
			isGroupPNR = true;
		} else {
			groupPNR = pnr;
		}

		try {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			List<UserNoteTO> reservationHistoryList = ModuleServiceLocator.getAirproxyReservationBD().searchUserNoteHistory(
					groupPNR, carrierCode, isGroupPNR, originSalesChanel, getTrackInfo(), isClassifyUN);
			request.setAttribute("userNoteHistory",
					JSONUtil.serialize(ReservationUtil.getUserNoteHistory(reservationHistoryList)));

		} catch (Exception e) {
			log.error(e);
		}
		return S2Constants.Jsp.Reservation.V2_USERNOTES;
	}

	private void setSearchOptionhtml(HttpServletRequest request) throws Exception {
		StringBuffer sf = new StringBuffer();
		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		if (AppSysParamsUtil.isLCCConnectivityEnabled() && BasicRH.hasPrivilege(request, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
			sf.append("arrSearchOptions[0] = new Array('" + SYSTEM.AA + "','Search " + defaultCarrier + " ');");
			sf.append("arrSearchOptions[1] = new Array('" + SYSTEM.INT + "','Search All');");
		} else {
			sf.append("arrSearchOptions[0] = new Array('" + SYSTEM.AA + "','Search " + defaultCarrier + " ');");
		}
		request.setAttribute(WebConstants.REQ_HTML_SEARCH_OPTION_LIST, sf.toString());
	}

	public String loadTypeBMessages(){
		return S2Constants.Jsp.TypeB.TYPEB_MESSAGES;		
	}
}
