/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightMealDetailDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

/**
 * @author Harsha
 * 
 */
public class AgentMealDetailsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(AgentMealDetailsReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public AgentMealDetailsReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = S2Constants.Result.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		try {
			log.debug("Inside AgentMealDetailsRH execute()");
			setDisplayData(request);
			log.debug("AgentMealDetailsRH SetDispalyData() SUCCESS");
		} catch (Exception e) {
			log.error("AgentMealDetailsRH SetDisplayData() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("AgentMealDetailsReportRH setReportView Success");
				return null;
			} else {
				log.error("AgentMealDetailsReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ModeOfPaymentsRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String flightDate = request.getParameter("txtFlightDate");
		String flightNo = request.getParameter("txtFlightNo");
		String value = request.getParameter("radReportOption");
		String reportCat = request.getParameter("radReportType");
		String paxCategory = request.getParameter("radReportPaxCat");

		String id = "UC_REPM_052";
		String templateMealDetail = "FlightMealDetails.jasper";
		String templateMealSummary = "FlightMealSummary.jasper";
		String onHoldPax = "OHD";
		String confirmedPax = "CNF";

		try {
			ReportsSearchCriteria rscount = new ReportsSearchCriteria();
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (flightDate != null && !"".equals(flightDate.trim())) {
				rscount.setDepTimeLocal(ReportsHTMLGenerator.convertDate(flightDate));
				search.setDepTimeLocal(ReportsHTMLGenerator.convertDate(flightDate));
			}
			if (flightNo != null && !"".equals(flightNo.trim())) {
				rscount.setFlightNumber(flightNo.toUpperCase());
				search.setFlightNumber(flightNo.toUpperCase());
			}
			if (reportCat != null && reportCat.trim().equals("DET")) {
				if (paxCategory != null && paxCategory.trim().equals("ALLPAX")) {
					search.setPaxCategory("ALL");
				} else {
					if (paxCategory != null) {
						search.setPaxCategory("MEAL");
					}
				}
			}
			rscount.getPaxStatus().add(confirmedPax);
			rscount.getPaxStatus().add(onHoldPax);

			Map<String, Map<String, Integer>> noOfPassenger = ModuleServiceLocator.getDataExtractionBD().getCountOfPassengers(
					rscount);

			Map<String, Integer> noOfCnfPassenger = noOfPassenger.get(confirmedPax);
			if (noOfCnfPassenger != null && !noOfCnfPassenger.isEmpty()) {
				int i = 1;
				for (String segmentCode : noOfCnfPassenger.keySet()) {
					parameters.put("CNF_PAX_CNT" + i, noOfCnfPassenger.get(segmentCode).toString());
					parameters.put("CNF_SEG" + i, segmentCode);
					i++;
				}
			}

			Map<String, Integer> noOfOnholdPassenger = noOfPassenger.get(onHoldPax);
			if (noOfOnholdPassenger != null && !noOfOnholdPassenger.isEmpty()) {
				int i = 1;
				for (String segmentCode : noOfOnholdPassenger.keySet()) {
					parameters.put("OHD_PAX_CNT" + i, noOfOnholdPassenger.get(segmentCode).toString());
					parameters.put("OHD_SEG" + i, segmentCode);
					i++;
				}
			}

			// For the meal details report;
			List<FlightMealDetailDTO> reportItems = new ArrayList<FlightMealDetailDTO>();

			if (reportCat != null && reportCat.trim().equals("SUMM")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templateMealSummary));
			} else {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templateMealDetail));
			}
			if (reportCat != null && reportCat.trim().equals("SUMM")) {
				resultSet = ModuleServiceLocator.getDataExtractionBD().getMealSummaryReport(search);
			} else {
				resultSet = ModuleServiceLocator.getDataExtractionBD().getMealDetailReport(search);
				reportItems = createFlightMealDetailDTOList(resultSet);
			}

			parameters.put("DEPARTURE_DATE", flightDate);
			parameters.put("FLIGHT_NO", flightNo.toUpperCase());
			parameters.put("ID", id);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (value.trim().equals("HTML")) {
				response.reset();
				parameters.put("IMG", reportsRootDir);

				if (reportCat != null && reportCat.trim().equals("SUMM")) {
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							reportsRootDir, response);
				} else {
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, reportItems, null,
							reportsRootDir, response);
				}

			} else if (value.trim().equals("PDF")) {
				response.reset();
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.addHeader("Content-Disposition", "filename=MealDetailsReport.pdf");

				if (reportCat != null && reportCat.trim().equals("SUMM")) {
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
				} else {
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, reportItems,
							response);
				}

			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.setHeader("Content-Disposition", "attachment;filename=MealDetailsReport.xls");
				if (reportCat != null && reportCat.trim().equals("SUMM")) {
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
				} else {
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, reportItems,
							response);
				}

			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.setHeader("Content-Disposition", "attachment;filename=MealDetailsReport.csv");
				if (reportCat != null && reportCat.trim().equals("SUMM")) {
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
				} else {
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, reportItems,
							response);
				}

			}
		} catch (Exception e) {
			log.error("setReportView() method is failed:");
		}

	}

	private static List<FlightMealDetailDTO> createFlightMealDetailDTOList(ResultSet flightMealDetailResultSet) {
		List<FlightMealDetailDTO> flightMealDetailDTOs = new ArrayList<FlightMealDetailDTO>();

		try {
			while (flightMealDetailResultSet.next()) {
				FlightMealDetailDTO flightMealDetail = new FlightMealDetailDTO();

				flightMealDetail.setPnr(flightMealDetailResultSet.getString("PNR"));
				flightMealDetail.setPaxFirstName(flightMealDetailResultSet.getString("FIRST_NAME"));
				flightMealDetail.setPaxLastName(flightMealDetailResultSet.getString("LAST_NAME"));
				flightMealDetail.setStatus(flightMealDetailResultSet.getString("STATUS"));
				flightMealDetail.setSeatCode(flightMealDetailResultSet.getString("SEAT_CODE"));
				flightMealDetail.setDepTimeLocal(flightMealDetailResultSet.getString("DEP_TIME_LOCAL"));
				flightMealDetail.setMealCode(composeMealCodeString(flightMealDetailResultSet.getString("MEAL_CODE")));

				flightMealDetailDTOs.add(flightMealDetail);

			}
		} catch (SQLException sqlEx) {
			log.error("Error occurred while iterating the flight meal details list :" + sqlEx);
		}

		return flightMealDetailDTOs;
	}

	private static String composeMealCodeString(String mealCodeStr) {
		StringBuilder sb = new StringBuilder();
		if (StringUtils.isNotBlank(mealCodeStr)) {
			String[] mealCodes = mealCodeStr.split(",");
			if (mealCodes != null && mealCodes.length > 0) {
				Map<String, Integer> mealCodeMap = new HashMap<String, Integer>();

				for (String mealCode : mealCodes) {
					if (mealCodeMap.keySet().contains(mealCode.trim())) {
						Integer currentCount = mealCodeMap.get(mealCode);
						currentCount += 1;
						mealCodeMap.put(mealCode, currentCount);
					} else {
						mealCodeMap.put(mealCode, 1);
					}
				}

				int i = 0;
				for (Map.Entry<String, Integer> entry : mealCodeMap.entrySet()) {
					if (i != 0) {
						sb.append(", ");
					}
					sb.append(entry.getValue() + "x" + entry.getKey());
					i++;
				}
			}
		}

		return sb.toString();
	}
}