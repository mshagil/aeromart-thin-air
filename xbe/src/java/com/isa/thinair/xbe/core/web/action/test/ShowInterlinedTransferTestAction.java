package com.isa.thinair.xbe.core.web.action.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.scheduledservices.core.client.InterlinedSegmentTransfer;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;

/**
 * @author Nilindra Fernando
 */
@Namespace(S2Constants.Namespace.PRIVATE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Test.INTERLINED_TRANSFER) })
public class ShowInterlinedTransferTestAction extends BaseRequestAwareAction {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(ShowInterlinedTransferTestAction.class);

	/** Holds the execution lock */
	private static Object lock = new Object();

	/**
	 * Execute Process
	 */
	public String execute() {
		String token = PlatformUtiltiies.nullHandler(request.getParameter("hdnMode"));

		if (token.equals("TRANSFER")) {
			processRequest(request);
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * Process the request
	 * 
	 * @param request
	 */
	private void processRequest(HttpServletRequest request) {
		synchronized (lock) {
			try {
				String strDate = PlatformUtiltiies.nullHandler(request.getParameter("txtDateTime"));
				Date executionDate = null;

				if (!strDate.equals("")) {
					DateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
					executionDate = (Date) formatter.parse(strDate);
				}

				Calendar start = new GregorianCalendar();

				log.debug(" ###### Starting Test Action Interlined Transfer ..........");
				InterlinedSegmentTransfer codeShareSegmentTransfer = new InterlinedSegmentTransfer();
				codeShareSegmentTransfer.execute(executionDate);
				log.debug(" ###### Completed Test Action Interlined Transfer ..........");

				Calendar end = new GregorianCalendar();

				// Get difference in milliseconds
				long diffMillis = end.getTimeInMillis() - start.getTimeInMillis();

				// Get difference in seconds
				long diffSecs = diffMillis / 1000;

				request.setAttribute(WebConstants.REQ_MESSAGE, "Transfer Successful, processed in " + diffSecs + " seconds ");
			} catch (Exception e) {
				log.error(e);
				request.setAttribute(WebConstants.REQ_MESSAGE, e.getMessage());
			}
		}
	}
}
