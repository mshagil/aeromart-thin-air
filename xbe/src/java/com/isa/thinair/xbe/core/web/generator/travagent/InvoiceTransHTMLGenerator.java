package com.isa.thinair.xbe.core.web.generator.travagent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceGenerationStatusDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.common.JavaScriptGenerator;

public class InvoiceTransHTMLGenerator {

	private static Log log = LogFactory.getLog(InvoiceTransHTMLGenerator.class);

	private static String clientErrors;

	private static final String strSMDateFormat = "dd/MM/yyyy";
	private static final String strSMTimeFormat = "dd/MM/yyyy HH:mm:ss";

	/**
	 * Gets the Invoice Transfer Grid Row for the Search critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containg Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getInvoiceTransRowHtml(HttpServletRequest request) throws ModuleException {

		Collection<GeneratedInvoiceDTO> colInvoices = null;
		Page page = null;
		int recNo = 0;
		int totRec = 0;

		String strHdnMode = request.getParameter("hdnMode");
		String strYear = request.getParameter("selYear");
		String strMonth = request.getParameter("selMonth");
		String strBilingPeriod = request.getParameter("selBP");
		String strAgent = request.getParameter("hdnAgents");
		String strZero = request.getParameter("chkZero");
		String strAgentType = request.getParameter("selAgencies");
		// Transaction wise
		String strInvoiceFrm = request.getParameter("txtInvoiceFrom");
		String strInvoiceTo = request.getParameter("txtInvoiceTo");
		String strEntity = request.getParameter("selEntity");

		SearchInvoicesDTO srcinvDto = new SearchInvoicesDTO();
		InvoiceGenerationStatusDTO invGeneratedStsDto = new InvoiceGenerationStatusDTO();
		Collection<String> agentcol = new ArrayList<String>();
		String strTotalMessage = "";

		boolean isByTransaction = AppSysParamsUtil.isTransactInvoiceEnable();

		if (request.getParameter("hdnRecNo") != null && !("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		if (strHdnMode != null && !strHdnMode.equals("AGENTS")) {

			if (strYear != null && !strYear.trim().equals("")) {
				srcinvDto.setYear(Integer.parseInt(strYear));
			}
			if (strMonth != null && !strMonth.trim().equals("")) {
				srcinvDto.setMonth(Integer.parseInt(strMonth));
			}
			if (strBilingPeriod != null) {
				srcinvDto.setInvoicePeriod(Integer.parseInt(strBilingPeriod));
			}
			if (strAgent != null && !strAgent.equals("")) {
				String[] agentList = strAgent.split(",");
				for (int j = 0; j < agentList.length; j++) {
					agentcol.add(agentList[j]);
				}

				srcinvDto.setAgentCodes(agentcol);
			} else if (strAgentType != null && strAgentType.equals("All")) {
				srcinvDto.setAgentCodes(null);

			}

			if (strZero != null && strZero.equals("ZERO")) {
				srcinvDto.setFilterZeroInvoices(true);
			}
			if (strInvoiceFrm != null && !strInvoiceFrm.trim().equals("")) {
				String[] strDate = strInvoiceFrm.split("/");
				Date frmDate = new GregorianCalendar(new Integer(strDate[2]).intValue(), new Integer(strDate[1]).intValue() - 1,
						new Integer(strDate[0]).intValue(), 0, 0, 0).getTime();
				srcinvDto.setYear(Integer.parseInt(strDate[2]));
				srcinvDto.setMonth(Integer.parseInt(strDate[1]) - 1);
				srcinvDto.setInvoiceDateFrom(frmDate);
			}
			if (strInvoiceTo != null && !strInvoiceFrm.trim().equals("")) {
				String[] strDate = strInvoiceTo.split("/");
				Date toDate = new GregorianCalendar(new Integer(strDate[2]).intValue(), new Integer(strDate[1]).intValue() - 1,
						new Integer(strDate[0]).intValue(), 23, 59, 59).getTime();
				srcinvDto.setInvoiceDateTo(toDate);
			}
			srcinvDto.setByTrasaction(isByTransaction);
			
			if (strEntity != null && !strEntity.equals("")) {
				srcinvDto.setEntity(strEntity);
			}

			invGeneratedStsDto = ModuleServiceLocator.getInvoicingBD().getGeneratedInvoicesDetails(srcinvDto, recNo, 50);

			if (invGeneratedStsDto != null) {
				page = invGeneratedStsDto.getInvoices();
				strTotalMessage = invGeneratedStsDto.getAmountStatus();
			}
			if (strTotalMessage == null) {
				strTotalMessage = "";
			}
			if (page != null) {
				colInvoices = page.getPageData();
				totRec = page.getTotalNoOfRecords();

			}

		}

		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		request.setAttribute(WebConstants.REQ_TOTAL_STRING, strTotalMessage);
		log.debug("inside InoiceHTMLGenerator.getGenerated invoices search ");
		return createInvoiceRowHtml(colInvoices);

	}

	/**
	 * Create the Grid Array from a Collection of Invoices
	 * 
	 * @param invoices
	 *            the Collection of GeneratedInvoiceDTOs
	 * @return String the Created Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String createInvoiceRowHtml(Collection<GeneratedInvoiceDTO> invoices) throws ModuleException {
		StringBuffer sb = new StringBuffer("var invoiceData = new Array();");
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		GeneratedInvoiceDTO genInvDTO = null;
		if (invoices != null) {
			List<GeneratedInvoiceDTO> list = (List<GeneratedInvoiceDTO>) invoices;
			Object[] listArr = list.toArray();

			for (int i = 0; i < listArr.length; i++) {
				genInvDTO = (GeneratedInvoiceDTO) listArr[i];
				String totalAmount = null;
				String curCode = null;
				if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
						&& "Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))) {
					totalAmount = genInvDTO.getInvoiceAmountLocal() + "";
					curCode = genInvDTO.getCurrencyCode();
				} else {
					totalAmount = genInvDTO.getInvoiceAmount() + "";
					curCode = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
				}

				sb.append("invoiceData[" + i + "] = new Array();");
				sb.append("invoiceData[" + i + "][0] = '" + i + "';");
				sb.append("invoiceData[" + i + "][1] = '" + genInvDTO.getAgentCode() + "';");
				sb.append("invoiceData[" + i + "][2] = '" + genInvDTO.getAgentName() + "';");
				sb.append("invoiceData[" + i + "][3] = '" + genInvDTO.getAccountCode() + "';");
				sb.append("invoiceData[" + i + "][4] = '" + genInvDTO.getInvoiceNumber() + "';");
				sb.append("invoiceData[" + i + "][5] = '" + totalAmount + "';");
				if (AppSysParamsUtil.isTransactInvoiceEnable()) {
					sb.append("invoiceData[" + i + "][6] = '" + formatDate(genInvDTO.getInvoiceDate(), strSMTimeFormat) + "';");
				} else {
					sb.append("invoiceData[" + i + "][6] = '" + formatDate(genInvDTO.getInvoiceDate(), strSMDateFormat) + "';");
				}
				sb.append("invoiceData[" + i + "][7] = '" + getTransferStatus(genInvDTO.getTransferedStatus()) + "';");
				if (genInvDTO.getEmailsSent() != null) {
					sb.append("invoiceData[" + i + "][8] = '" + genInvDTO.getEmailsSent() + "';");
				} else {
					sb.append("invoiceData[" + i + "][8] = '0';");
				}
				sb.append("invoiceData[" + i + "][10] = '" + curCode + "';");
				if (genInvDTO.getPnr() != null) {
					sb.append("invoiceData[" + i + "][11] = '" + genInvDTO.getPnr() + "';");
				}
				if (genInvDTO.getEntityName() != null) {
					sb.append("invoiceData[" + i + "][12] = '" + genInvDTO.getEntityName() + "';");
				} else {
					sb.append("invoiceData[" + i + "][12] = '';");
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Sets the Client Validations for the Invoice Generate Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.invoicetrans.year.null", "yearEmpty");
			moduleErrs.setProperty("um.invoicetrans.month.null", "monthEmpty");
			moduleErrs.setProperty("um.invoicetrans.agentType.null", "agentTypeRqrd");
			moduleErrs.setProperty("um.invoicetrans.agent.null", "agentsRqrd");
			moduleErrs.setProperty("um.invoicetrans.month.greater", "monthGreater");
			moduleErrs.setProperty("um.invoicetrans.fromdate.null", "fromDateEmpty");
			moduleErrs.setProperty("um.invoicetrans.todate.null", "toDateEmpty");
			clientErrors = JavaScriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Formats the Date to a Given Format
	 * 
	 * @param utilDate
	 *            the Date
	 * @param fmt
	 *            the String Format
	 * @return String the Formatted date
	 */
	private static String formatDate(Date utilDate, String fmt) {
		String strDate = "";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			strDate = sdFmt.format(utilDate);
		}
		return strDate;
	}

	/**
	 * Gets the Trasfering Status
	 * 
	 * @param str
	 *            the string
	 * @return String the Status
	 */
	private static String getTransferStatus(String str) {
		if (str.equals("P")) {
			return "InPrgress";
		}
		return (str.equals("Y")) ? "Transfered" : "Not Transfered";
	}
}
