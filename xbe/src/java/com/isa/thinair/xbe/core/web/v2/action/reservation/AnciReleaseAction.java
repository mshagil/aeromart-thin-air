package com.isa.thinair.xbe.core.web.v2.action.reservation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryJSONUtil;
import com.isa.thinair.webplatform.api.v2.util.TrackInfoUtil;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class AnciReleaseAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(AnciReleaseAction.class);
	private boolean success = true;

	private String messageTxt;

	private String selectedAncillaries;

	private boolean releaseSeats = true;
	
	private String pnr;
	
	private String searchSystem;

	public String execute() {
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);

			String strTxnIdntifier = resInfo.getTransactionId();
			SYSTEM system = resInfo.getSelectedSystem();

			List<LCCSegmentSeatDTO> blockedSeatDTOs = AncillaryJSONUtil.getBlockSeatDTOs(selectedAncillaries);
			if (log.isDebugEnabled()) {
				log.debug("Calling LCC to release seats");
			}
			if (releaseSeats) {
				
				if (pnr != null && !pnr.isEmpty()) {
					blockedSeatDTOs = ReservationUtil.filteredBlockSeats(blockedSeatDTOs, pnr,
							searchSystem.equals(SYSTEM.AA.name()) ? false : true, request, getTrackInfo());
				}
				
				boolean blockSeatsStatus = ModuleServiceLocator.getAirproxyAncillaryBD().releaseSeats(blockedSeatDTOs,
						strTxnIdntifier, system, TrackInfoUtil.getBasicTrackInfo(request));
				if (!blockSeatsStatus) {
					log.error("Releasing release seats failed for transaction idf :" + strTxnIdntifier);
				}
			}

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e,userLanguage);
			log.error("Releasing blocked seats failed", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getSelectedAncillaries() {
		return selectedAncillaries;
	}

	public void setSelectedAncillaries(String selectedAncillaries) {
		this.selectedAncillaries = selectedAncillaries;
	}

	public boolean isReleaseSeats() {
		return releaseSeats;
	}

	public void setReleaseSeats(boolean releaseSeats) {
		this.releaseSeats = releaseSeats;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getSearchSystem() {
		return searchSystem;
	}

	public void setSearchSystem(String searchSystem) {
		this.searchSystem = searchSystem;
	}

}
