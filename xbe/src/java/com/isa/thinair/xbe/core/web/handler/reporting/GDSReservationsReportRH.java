package com.isa.thinair.xbe.core.web.handler.reporting;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.generator.reporting.ReportsHTMLGenerator;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;

public class GDSReservationsReportRH extends BasicRH {

	private static Log log = LogFactory.getLog(GDSReservationsReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = S2Constants.Result.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("GDSReservationsReportRH setReportView Success");
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("GDSReservationsReport Handler setreportview FAILED ", e);
		}

		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setGDSList(request);
		setClientErrors(request);
	}

	private static void setGDSList(HttpServletRequest request) throws ModuleException {
		String gdsList = SelectListGenerator.createGDSList();
		setAttribInRequest(request, WebConstants.REQ_HTML_GDS_NAMES_LIST, gdsList);

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		String id = "UC_REPM_093";
		String reportTemplate = "GDSReservationReport.jasper";

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String gdsID = request.getParameter("selGDS");
		String reportType = request.getParameter("radReportOption");

		try {
			ReportsSearchCriteria searchCriteria = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (fromDate != null && !fromDate.equals("")) {
				searchCriteria.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
			if (toDate != null && !toDate.equals("")) {
				searchCriteria.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + "  23:59:59");
			}
			searchCriteria.setGdsCode(gdsID);

			ResultSet resultSet = null;

			resultSet = ModuleServiceLocator.getDataExtractionBD().getGDSReservationsData(searchCriteria);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			if (gdsID != null && !gdsID.equals("")) {
				int gdsintID = Integer.parseInt(gdsID);
				Gds selectedGDS = ModuleServiceLocator.getGDSBD().getGds(gdsintID);
				parameters.put("GDS_ID", selectedGDS.getGdsCode());
			} else {
				List<Gds> gdsList = ModuleServiceLocator.getGDSBD().getGDSs();
				for (Gds gds : gdsList) {
					if (gdsID == null || gdsID.equals("")) {
						gdsID = gds.getGdsCode();
					} else {
						gdsID = gdsID + "," + gds.getGdsCode();
					}
				}
				parameters.put("GDS_ID", gdsID);
			}

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			if (reportType.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (reportType.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=GDSReservationsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			} else if (reportType.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=GDSReservationsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}

		} catch (ModuleException e) {
			log.error("setReportView() method is failed :" + e.getMessage());
		}
	}
}
