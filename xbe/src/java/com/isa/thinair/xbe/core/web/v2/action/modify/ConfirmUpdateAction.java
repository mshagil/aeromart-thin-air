package com.isa.thinair.xbe.core.web.v2.action.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airproxy.api.ModifyReservationUtil;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxCnxModPenChargeForServiceTaxDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForNonTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientResAlterQueryModesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PnrChargeDetailTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.HandlingFeeCalculationUtil;
import com.isa.thinair.webplatform.api.util.ReservationBeanUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ConfirmUpdateChargesTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ServiceTaxCCParamsDTO;
import com.isa.thinair.webplatform.core.util.ReservationCommonUtil;
import com.isa.thinair.xbe.api.dto.ReservationProcessParams;
import com.isa.thinair.xbe.api.dto.v2.BookingShoppingCart;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdateOverrideChargesTO;
import com.isa.thinair.xbe.api.dto.v2.ConfirmUpdatePaxSummaryListTO;
import com.isa.thinair.xbe.api.dto.v2.XBEReservationInfoDTO;
import com.isa.thinair.xbe.core.exception.XBERuntimeException;
import com.isa.thinair.xbe.core.service.ModuleServiceLocator;
import com.isa.thinair.xbe.core.web.constants.S2Constants;
import com.isa.thinair.xbe.core.web.constants.WebConstants;
import com.isa.thinair.xbe.core.web.handler.common.BasicRH;
import com.isa.thinair.xbe.core.web.v2.util.BookingUtil;
import com.isa.thinair.xbe.core.web.v2.util.ConfirmUpdateUtil;
import com.isa.thinair.xbe.core.web.v2.util.ReservationUtil;

@Namespace(S2Constants.Namespace.PRIVATE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ConfirmUpdateAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ConfirmUpdateAction.class);

	private boolean success = true;

	private String messageTxt;

	private String strMode;

	private String pnr;

	private String groupPNR;

	private ArrayList<String> outFlightRPHList;

	private ArrayList<String> retFlightRPHList;

	private String modifySegment;

	private String adultCnxCharge;

	private String childCnxCharge;

	private String infantCnxCharge;

	private String defaultAdultCnxCharge;

	private String defaultChildCnxCharge;

	private String defaultInfantCnxCharge;

	private ConfirmUpdateChargesTO updateCharge;

	Collection<ConfirmUpdateOverrideChargesTO> overRideCharges;

	Collection<ConfirmUpdatePaxSummaryListTO> paxSummaryList;

	private FlightSearchDTO fareQuoteParams;

	private String paxAdults;

	private String paxInfants;

	private String version;

	private String flightRPHList;

	private boolean blnOverride = Boolean.FALSE;

	private boolean isFlexiReservation = false;

	private String flexiInfo;

	private String flexiSelected;

	private Boolean blnShowChargeTypeOveride = Boolean.FALSE;

	private ConfirmUpdateOverrideChargesTO adultOverideCharges;

	private ConfirmUpdateOverrideChargesTO childOverideCharges;

	private ConfirmUpdateOverrideChargesTO infantOverideCharges;

	private String routeType;

	private CommonReservationContactInfo contactInfo;

	public String execute() {
		String strForward = S2Constants.Result.SUCCESS;
		String userLanguage = (String) request.getSession().getAttribute(WebConstants.REQ_LANGUAGE);
		try {
			boolean isGroupPNR = ReservationUtil.isGroupPnr(groupPNR);
			// FIXME fareQuoteParams is null in remove pax
			if (!isGroupPNR && fareQuoteParams != null && SYSTEM.getEnum(fareQuoteParams.getSearchSystem()) == SYSTEM.INT) {
				isGroupPNR = true;
				groupPNR = pnr;
			}

			boolean hasOverrideCnxModChargePrivilege = false;
			String strModeMode = null;

			if (!isGroupPNR) {
				groupPNR = pnr;
			}

			XBEReservationInfoDTO resInfo = (XBEReservationInfoDTO) request.getSession().getAttribute(
					S2Constants.Session_Data.XBE_SES_RESDATA);
			BookingShoppingCart bookingShoppingCart = (BookingShoppingCart) request.getSession().getAttribute(
					S2Constants.Session_Data.LCC_SES_BOOKING_CART);
			String strTxnIdntifier = resInfo.getTransactionId();

			LCCClientResAlterQueryModesTO resAlterQueryModesTO = null;

			Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(request
					.getParameter("resSegments"));

			ReservationProcessParams rpParams = new ReservationProcessParams(request, resInfo.getReservationStatus(), colsegs,
					resInfo.isModifiableReservation(), resInfo.isGdsReservationModAllowed(), resInfo.getLccPromotionInfoTO(),
					null, false, false);
			if (isGroupPNR) {
				rpParams.enableInterlineModificationParams(resInfo.getInterlineModificationParams(),
						resInfo.getReservationStatus(), colsegs, resInfo.isModifiableReservation(),
						resInfo.getLccPromotionInfoTO());
			}

			hasOverrideCnxModChargePrivilege = rpParams.isHasOverrideCnxModChargePrivilege();
			if (hasOverrideCnxModChargePrivilege) {
				this.blnOverride = true;
				this.blnShowChargeTypeOveride = rpParams.isBlnShowChargeTypeOveride();
			}

			boolean isModify = false;
			List<Integer> paxExclusionSeq = null;
			int operationMode = 0;
			if (strMode != null) {

				if (strMode.trim().equals(WebConstants.ACTION_RES_CANCEL)) {
					if (rpParams.isCancelReservation()) {
						resAlterQueryModesTO = LCCClientResAlterModesTO.composeCancelReservationQueryRequest(groupPNR, version,
								isGroupPNR);
					} else {
						throw new XBERuntimeException(WebConstants.KEY_UNAUTHORIZED_OPERATION);
					}
					strModeMode = ReservationConstants.AlterationType.ALT_CANCEL_RES;
					operationMode = CommonsConstants.ModifyOperation.CANCEL_PNR.getOperation();
				} else if (strMode.trim().equals(WebConstants.ACTION_RES_VOID)) {
					resAlterQueryModesTO = LCCClientResAlterModesTO.composeVoidReservationQueryRequest(groupPNR, version,
							isGroupPNR);
					strModeMode = ReservationConstants.AlterationType.ALT_VOID_RES;					
				} else if (strMode.trim().equals(WebConstants.ACTION_CANCEL_SEGMENT)) {
					Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
							modifySegment, rpParams, false);
					resAlterQueryModesTO = LCCClientResAlterModesTO.composeCancelSegmentQueryRequest(groupPNR, colModSegs,
							version, isGroupPNR);
					strModeMode = ReservationConstants.AlterationType.ALT_CANCEL_OND;
				} else if (strMode.trim().equals(WebConstants.ACTION_MODIFY_SEGMENT)) {

					List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getFlightSegmentFromRPHList(flightRPHList,
							fareQuoteParams);
					FlightPriceRQ flightPriceRQ = ReservationBeanUtil.createFareQuoteRQ(fareQuoteParams, outFlightRPHList,
							retFlightRPHList);
					flightPriceRQ.setTransactionIdentifier(resInfo.getTransactionId());
					boolean hasPrivMakeResFlights = BasicRH.hasPrivilege(request, PriviledgeConstants.MAKE_RES_ALLFLIGHTS);
					flightPriceRQ.getAvailPreferences().setRestrictionLevel(
							AvailabilityConvertUtil.getAvailabilityRestrictionLevel(hasPrivMakeResFlights,
									fareQuoteParams.getBookingType()));

					ReservationBeanUtil.setExistingSegInfo(flightPriceRQ, request.getParameter("resSegments"),
							request.getParameter("modifySegment"));
					isModify = true;
					LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
					lccClientSegmentAssembler.setLccTransactionIdentifier(strTxnIdntifier);

					ExternalChargeUtil.injectOndBaggageGroupId(bookingShoppingCart.getPaxList(), flightSegmentTOs);
					BookingUtil.addSegments(lccClientSegmentAssembler, flightSegmentTOs,
							BookingUtil.getNextSegmentSequnce(colsegs));
					lccClientSegmentAssembler.setFlightPriceRQ(flightPriceRQ);
					lccClientSegmentAssembler.setSelectedFareSegChargeTO(bookingShoppingCart.getPriceInfoTO()
							.getFareSegChargeTO());
					lccClientSegmentAssembler.setFlexiSelected(ReservationUtil.isFlexiSelected(flexiSelected));
					lccClientSegmentAssembler.setDiscountedFareDetails(bookingShoppingCart.getFareDiscount());
					lccClientSegmentAssembler
							.setServiceTaxRatio(bookingShoppingCart.getServiceTaxRatio(EXTERNAL_CHARGES.JN_ANCI));
					lccClientSegmentAssembler.setExternalChargesMap(null);
					lccClientSegmentAssembler
							.setPassengerExtChargeMap(ReservationUtil.getPassengerExtChgMap(bookingShoppingCart));

					Collection<LCCClientReservationSegment> colModSegs = ReservationUtil.getModifyingSegments(colsegs,
							modifySegment, rpParams, true);
					resAlterQueryModesTO = LCCClientResAlterModesTO.composeModifySegmentQueryRequest(groupPNR, colModSegs,
							lccClientSegmentAssembler, version, isGroupPNR);

					strModeMode = ReservationConstants.AlterationType.ALT_MODIFY_OND;
				} else if (strMode.trim().equals(WebConstants.ACTION_REMOVE_PAX)) {
					Collection<LCCClientReservationPax> colPaxes = ReservationUtil.transformJsonPassengers(request
							.getParameter("resPaxs"));
					Set<LCCClientReservationPax> paxSet = createPaxSet(colPaxes);
					List<String> paxInfantList = new ArrayList<String>();
					List<String> paxAdultsList = new ArrayList<String>();

					paxInfantList = extractJSonObject(paxInfants);
					paxAdultsList = extractJSonObject(paxAdults);
					
					/*
					 * TODO currently the pax status and e ticket status is validating from the front end (remove pax
					 * button) it is better to have the validation here also
					 */
					boolean isInfantRemovable = false;
					boolean onlyInfantAvailable = (paxInfantList.size() > 0 && paxAdultsList.isEmpty());
					isInfantRemovable = (onlyInfantAvailable && AppSysParamsUtil.allowRemoveInfantWhenFutureSegmentsNotThere());
					
					paxExclusionSeq = ModifyReservationUtil.getExclusionPaxSequenceFromHandlingFee(colPaxes, paxInfantList, paxAdultsList);

					for (LCCClientReservationPax reservationPax : colPaxes) {
						if (ModifyReservationUtil.isSelectedTraveler(reservationPax.getTravelerRefNumber(), paxAdultsList)) {
							if (reservationPax.getInfants().size() > 0) {
								Set<LCCClientReservationPax> infantPaxs = reservationPax.getInfants();
								for (LCCClientReservationPax infantPax : infantPaxs) {
									if (!ModifyReservationUtil
											.isSelectedTraveler(infantPax.getTravelerRefNumber(), paxInfantList)) {
										paxInfantList.add(infantPax.getTravelerRefNumber());
									}
								}
							}
						}
					}

					Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
					List<String> segmentsToDepart = new ArrayList<String>();
					for (LCCClientReservationSegment reservationSegment : colsegs) {
						if (currentDate.compareTo(reservationSegment.getDepartureDateZulu()) < 0) {
							segmentsToDepart.add(reservationSegment.getFlightSegmentRefNumber());
						}
					}

					if (isInfantRemovable || segmentsToDepart.size() > 0) {
						resAlterQueryModesTO = LCCClientResAlterModesTO.composeRemovePaxQueryRequest(groupPNR, version,
								paxAdultsList, paxInfantList, isGroupPNR, paxSet);
					} else {
						throw new XBERuntimeException(WebConstants.NO_OUTBOUND_FOUND);
					}
					strModeMode = ReservationConstants.AlterationType.ALT_CANCEL_RES;
					operationMode = CommonsConstants.ModifyOperation.REMOVE_PAX.getOperation();
				}
				// This only applys to Cancel reservation and Modify Rerservations flows ONLY
				if (this.adultOverideCharges != null) {
					resAlterQueryModesTO.setAdultCustomChargeTO(this.createPNRDetailTO(strMode, adultOverideCharges));
				}
				if (this.infantOverideCharges != null) {
					resAlterQueryModesTO.setInfantCustomChargeTO(this.createPNRDetailTO(strMode, infantOverideCharges));
				}
				if (this.childOverideCharges != null) {
					resAlterQueryModesTO.setChildCustomChargeTO(this.createPNRDetailTO(strMode, childOverideCharges));
				}

				if (this.defaultAdultCnxCharge != null && !this.defaultAdultCnxCharge.isEmpty()) {
					resAlterQueryModesTO.setDefaultCustomAdultCharge(new BigDecimal(defaultAdultCnxCharge));
				}
				if (this.defaultChildCnxCharge != null && !this.defaultChildCnxCharge.isEmpty()) {
					resAlterQueryModesTO.setDefaultCustomChildCharge(new BigDecimal(defaultChildCnxCharge));
				}
				if (this.defaultInfantCnxCharge != null) {
					resAlterQueryModesTO.setDefaultCustomInfantCharge(new BigDecimal(defaultInfantCnxCharge));
				}
			}
			BigDecimal customAdultCharge = null;
			BigDecimal customChildCharge = null;
			BigDecimal customInfantCharge = null;
			if (adultCnxCharge != null && !adultCnxCharge.equals("") && !adultCnxCharge.equals("undefined")) {
				customAdultCharge = new BigDecimal(adultCnxCharge);
				if (this.adultOverideCharges == null
						|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(this.adultOverideCharges)) {
					resAlterQueryModesTO.setSendingAbosoluteCharge(true);
				} // if not modifying using percentage
			}

			if (childCnxCharge != null && !childCnxCharge.equals("") && !childCnxCharge.equals("undefined")) {
				customChildCharge = new BigDecimal(childCnxCharge);
				if (this.childOverideCharges == null
						|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(this.childOverideCharges)) {
					resAlterQueryModesTO.setSendingAbosoluteCharge(true);
				}
			}

			if (infantCnxCharge != null && !infantCnxCharge.equals("") && !infantCnxCharge.equals("undefined")) {
				customInfantCharge = new BigDecimal(infantCnxCharge);
				if (this.infantOverideCharges == null
						|| AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes()
								.equals(this.infantOverideCharges)) {
					resAlterQueryModesTO.setSendingAbosoluteCharge(true);
				}
			}

			resAlterQueryModesTO.setCustomAdultCharge(customAdultCharge);
			resAlterQueryModesTO.setCustomChildCharge(customChildCharge);
			resAlterQueryModesTO.setCustomInfantCharge(customInfantCharge);

			resAlterQueryModesTO.setRouteType(this.routeType);
			resAlterQueryModesTO.setInfantPaymentSeparated(resInfo.isInfantPaymentSeparated());
			if (isGroupPNR) {
				resAlterQueryModesTO.setVersion(ReservationCommonUtil.getCorrectInterlineVersion(resAlterQueryModesTO
						.getVersion()));
			}
			
			
			Collection<LCCClientReservationSegment> lccSegments = ModifyReservationJSONUtil
					.transformJsonSegments(request.getParameter("resSegments"));

			LCCClientSegmentAssembler lccClientSegmentAssembler = new LCCClientSegmentAssembler();
			lccClientSegmentAssembler.setPassengerExtChargeMap(
					HandlingFeeCalculationUtil.composeExternalChargesByPaxSequence(pnr, lccSegments, paxExclusionSeq, operationMode));
			resAlterQueryModesTO.setLccClientSegmentAssembler(lccClientSegmentAssembler);	
			

			ReservationBalanceTO reservationBalanceTO = ModuleServiceLocator.getAirproxyReservationBD()
					.getResAlterationBalanceSummary(resAlterQueryModesTO, getTrackInfo());

			ServiceTaxQuoteForNonTicketingRevenueRS serviceTaxForNonTicketingRevenue = null;
			int paxSeq;
			if (strMode.trim().equals(WebConstants.ACTION_RES_CANCEL) || strMode.trim().equals(WebConstants.ACTION_REMOVE_PAX)) {
				ServiceTaxQuoteCriteriaForNonTktDTO serviceTaxQuoteCriteriaDTO = new ServiceTaxQuoteCriteriaForNonTktDTO();
				Map<Integer, PaxCnxModPenChargeForServiceTaxDTO> paxWiseCharges = new HashMap<>();

				contactInfo = ReservationUtil.transformJsonContactInfo(request.getParameter("contactInfo"));

				for (LCCClientPassengerSummaryTO paxSummary : reservationBalanceTO.getPassengerSummaryList()) {
					paxSeq = PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber());

					PaxCnxModPenChargeForServiceTaxDTO paxCnxModPenChargeForServiceTaxDTO = new PaxCnxModPenChargeForServiceTaxDTO();

					LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
					chgDTO.setCode(ReservationInternalConstants.ChargeGroup.CNX);
					chgDTO.setAmount(paxSummary.getTotalCreditAmount());

					List<LCCClientExternalChgDTO> chgs = new ArrayList<>();
					chgs.add(chgDTO);

					paxCnxModPenChargeForServiceTaxDTO.setCharges(chgs);
					paxWiseCharges.put(paxSeq, paxCnxModPenChargeForServiceTaxDTO);
					serviceTaxQuoteCriteriaDTO.setPrefSystem(SYSTEM.AA);
					serviceTaxQuoteCriteriaDTO.setPaxWiseCharges(paxWiseCharges);
					serviceTaxQuoteCriteriaDTO.setPaxCountryCode(contactInfo.getCountryCode());
					serviceTaxQuoteCriteriaDTO.setPaxState(contactInfo.getState());

				}

				serviceTaxForNonTicketingRevenue = ModuleServiceLocator.getAirproxyReservationBD()
						.quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteCriteriaDTO, getTrackInfo(),
								(UserPrincipal) request.getUserPrincipal());

			}

			// Apply cc charge + jn + service tax when adminfee is enabled

			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			String agentCode = userPrincipal.getAgentCode();
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.XBE, agentCode)) {


				BigDecimal totalPaymentAmountExcludingCCCharge = reservationBalanceTO.getTotalAmountDue();

				ExternalChgDTO externalCcChgDTO = bookingShoppingCart.getAllExternalChargesMap()
						.get(EXTERNAL_CHARGES.CREDIT_CARD);
				externalCcChgDTO.calculateAmount(totalPaymentAmountExcludingCCCharge);
				bookingShoppingCart.addSelectedExternalCharge(externalCcChgDTO);

				// Calculate CC & Handling charge JN tax
				com.isa.thinair.xbe.core.web.v2.util.ReservationUtil.getApplicableServiceTax(bookingShoppingCart,
						EXTERNAL_CHARGES.JN_OTHER);

				// apply service tax
				String grpPNR = "";
				if(isGroupPNR) {
					grpPNR = groupPNR;
				}
				ServiceTaxCCParamsDTO serviceTaxCCParamsDTO = new ServiceTaxCCParamsDTO(fareQuoteParams, contactInfo.getState(),
						contactInfo.getCountryCode(), isPaxTaxRegistered(contactInfo.getTaxRegNo()), flightRPHList, grpPNR);
				BigDecimal totalServiceTaxAmount = BigDecimal.ZERO;
				totalServiceTaxAmount = ReservationUtil.calculateServiceTaxForTransactionFee(bookingShoppingCart,
						externalCcChgDTO.getAmount(), resInfo.getTransactionId(), serviceTaxCCParamsDTO, getTrackInfo(),
						EXTERNAL_CHARGES.CREDIT_CARD, false);

				reservationBalanceTO.setTotalAmountDue(AccelAeroCalculator.add(totalPaymentAmountExcludingCCCharge,
						externalCcChgDTO.getAmount(), totalServiceTaxAmount));
				BigDecimal currentTotalPrice = reservationBalanceTO.getTotalPrice();
				reservationBalanceTO.setTotalPrice(AccelAeroCalculator.add(currentTotalPrice, externalCcChgDTO.getAmount(),
						totalServiceTaxAmount));

			}

			if (reservationBalanceTO.getVersion() != null) {
				this.version = reservationBalanceTO.getVersion();
			}

			StringBuffer flexiBuffer = new StringBuffer();
			if (!resInfo.getReservationStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				isFlexiReservation = ReservationUtil.setFlexiInformation(strMode, modifySegment, flexiBuffer,
						request.getParameter("flexiAlertInfo"));
			}
			// Make sure this is executed first to identify Anci charges

			paxSummaryList = ConfirmUpdateUtil.getPaxSummaryList(reservationBalanceTO, null,
					Boolean.parseBoolean(request.getParameter("isInfantPaymentSeparated")));
			// paxSummaryList = (Collection<ConfirmUpdatePaxSummaryListTO>)summObj[0];
			updateCharge = ConfirmUpdateUtil.getChargesDetails(reservationBalanceTO, bookingShoppingCart);

			// This is a temporary fix for the pax wise balance retrieved via interline
			// TODO implement the current pax wise payments and c/f credit via interline
			if (isGroupPNR) {
				updateBalanceSummaryForInterline(paxSummaryList);
			}

			// TODO fix charge detail for interline
			overRideCharges = ConfirmUpdateUtil.getOverrideList(reservationBalanceTO, resAlterQueryModesTO, isModify,
					blnShowChargeTypeOveride);

			flexiInfo = flexiBuffer.toString();

		} catch (Exception e) {
			success = false;
			messageTxt = BasicRH.getErrorMessage(e, userLanguage);
			log.error(messageTxt, e);

		}
		return strForward;
	}

	private boolean isPaxTaxRegistered(String taxRegNo) {
		boolean isPaxTaxRegistered = false;
		if (taxRegNo != null && !taxRegNo.equals("")) {
			isPaxTaxRegistered = true;
		}
		return isPaxTaxRegistered;
	}

	private void updateBalanceSummaryForInterline(Collection<ConfirmUpdatePaxSummaryListTO> paxSummaryList) {
		for (ConfirmUpdatePaxSummaryListTO pax : paxSummaryList) {
			pax.setDisplayTotalCFNew("0.00");
			pax.setDisplayTotalPaymentsCurrent("0.00");
		}
	}

	private List<String> extractJSonObject(String jSonString) throws ParseException {
		List<String> jsonList = new ArrayList<String>();
		if (jSonString != null && !jSonString.equals("")) {
			JSONArray jsonArray = (JSONArray) new JSONParser().parse(jSonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();
				String paxId = BeanUtils.nullHandler(jObject.get("paxID"));
				jsonList.add(paxId);
			}
		}
		return jsonList;
	}

	private Set<LCCClientReservationPax> createPaxSet(Collection<LCCClientReservationPax> colPaxes) {
		Set<LCCClientReservationPax> paxSet = new HashSet<LCCClientReservationPax>();
		for (LCCClientReservationPax reservationPax : colPaxes) {
			paxSet.add(reservationPax);
		}
		return paxSet;
	}

	private PnrChargeDetailTO createPNRDetailTO(String strMode, ConfirmUpdateOverrideChargesTO chargesTO) {
		PnrChargeDetailTO chargeDetailTO = new PnrChargeDetailTO();
		if (strMode.equals(WebConstants.ACTION_CANCEL_SEGMENT) || strMode.equals(WebConstants.ACTION_RES_CANCEL)
				|| strMode.equals(WebConstants.ACTION_REMOVE_PAX)) {
			chargeDetailTO.setCancellationChargeType(chargesTO.getChargeType());
			if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMin())) {
				chargeDetailTO.setMinCancellationAmount(new BigDecimal(chargesTO.getMin()));
			}
			if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMax())) {
				chargeDetailTO.setMaximumCancellationAmount(new BigDecimal(chargesTO.getMax()));
			}
		} else if (strMode.equals(WebConstants.ACTION_MODIFY_SEGMENT)) {
			chargeDetailTO.setModificationChargeType(chargesTO.getChargeType());
			if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMin())) {
				chargeDetailTO.setMinModificationAmount(new BigDecimal(chargesTO.getMin()));
			}
			if (PlatformUtiltiies.isNotEmptyString(chargesTO.getMax())) {
				chargeDetailTO.setMaximumModificationAmount(new BigDecimal(chargesTO.getMax()));
			}
		} else {
			return null;
		}
		if (PlatformUtiltiies.isNotEmptyString(chargesTO.getPrecentageVal())) {
			chargeDetailTO.setChargePercentage(new BigDecimal(chargesTO.getPrecentageVal()));
		}
		return chargeDetailTO;
	}

	// getters
	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public String getStrMode() {
		return strMode;
	}

	public void setStrMode(String strMode) {
		this.strMode = strMode;
	}

	public String getModifySegment() {
		return modifySegment;
	}

	public void setAdultCnxCharge(String adultCnxCharge) {
		this.adultCnxCharge = adultCnxCharge;
	}

	public void setChildCnxCharge(String childCnxCharge) {
		this.childCnxCharge = childCnxCharge;
	}

	public void setInfantCnxCharge(String infantCnxCharge) {
		this.infantCnxCharge = infantCnxCharge;
	}

	public String getAdultCnxCharge() {
		return adultCnxCharge;
	}

	public String getChildCnxCharge() {
		return childCnxCharge;
	}

	public String getInfantCnxCharge() {
		return infantCnxCharge;
	}

	public String getDefaultAdultCnxCharge() {
		return defaultAdultCnxCharge;
	}

	public void setDefaultAdultCnxCharge(String defaultAdultCnxCharge) {
		this.defaultAdultCnxCharge = defaultAdultCnxCharge;
	}

	public String getDefaultChildCnxCharge() {
		return defaultChildCnxCharge;
	}

	public void setDefaultChildCnxCharge(String defaultChildCnxCharge) {
		this.defaultChildCnxCharge = defaultChildCnxCharge;
	}

	public String getDefaultInfantCnxCharge() {
		return defaultInfantCnxCharge;
	}

	public void setDefaultInfantCnxCharge(String defaultInfantCnxCharge) {
		this.defaultInfantCnxCharge = defaultInfantCnxCharge;
	}

	public void setModifySegment(String modifySegment) {
		this.modifySegment = modifySegment;
	}

	public ConfirmUpdateChargesTO getUpdateCharge() {
		return updateCharge;
	}

	public void setUpdateCharge(ConfirmUpdateChargesTO updateCharge) {
		this.updateCharge = updateCharge;
	}

	public Collection<ConfirmUpdateOverrideChargesTO> getOverRideCharges() {
		return overRideCharges;
	}

	public void setOverRideCharges(Collection<ConfirmUpdateOverrideChargesTO> overRideCharges) {
		this.overRideCharges = overRideCharges;
	}

	public Collection<ConfirmUpdatePaxSummaryListTO> getPaxSummaryList() {
		return paxSummaryList;
	}

	public void setPaxSummaryList(Collection<ConfirmUpdatePaxSummaryListTO> paxSummaryList) {
		this.paxSummaryList = paxSummaryList;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public ArrayList<String> getOutFlightRPHList() {
		return outFlightRPHList;
	}

	public void setOutFlightRPHList(ArrayList<String> outFlightRPHList) {
		this.outFlightRPHList = outFlightRPHList;
	}

	public ArrayList<String> getRetFlightRPHList() {
		return retFlightRPHList;
	}

	public void setRetFlightRPHList(ArrayList<String> retFlightRPHList) {
		this.retFlightRPHList = retFlightRPHList;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	/**
	 * @param paxAdults
	 *            the paxAdults to set
	 */
	public void setPaxAdults(String paxAdults) {
		this.paxAdults = paxAdults;
	}

	/**
	 * @param paxInfants
	 *            the paxInfants to set
	 */
	public void setPaxInfants(String paxInfants) {
		this.paxInfants = paxInfants;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersion() {
		return version;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public boolean isBlnOverride() {
		return blnOverride;
	}

	public boolean isFlexiReservation() {
		return isFlexiReservation;
	}

	public void setFlexiReservation(boolean isFlexiReservation) {
		this.isFlexiReservation = isFlexiReservation;
	}

	public String getFlexiInfo() {
		return flexiInfo;
	}

	public void setFlexiInfo(String flexiInfo) {
		this.flexiInfo = flexiInfo;
	}

	public String getFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(String flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public boolean isBlnShowChargeTypeOveride() {
		return blnShowChargeTypeOveride;
	}

	public void setBlnShowChargeTypeOveride(boolean blnShowChargeTypeOveride) {
		this.blnShowChargeTypeOveride = blnShowChargeTypeOveride;
	}

	public ConfirmUpdateOverrideChargesTO getAdultOverideCharges() {
		return adultOverideCharges;
	}

	public void setAdultOverideCharges(ConfirmUpdateOverrideChargesTO adultOverideCharges) {
		this.adultOverideCharges = adultOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getChildOverideCharges() {
		return childOverideCharges;
	}

	public void setChildOverideCharges(ConfirmUpdateOverrideChargesTO childOverideCharges) {
		this.childOverideCharges = childOverideCharges;
	}

	public ConfirmUpdateOverrideChargesTO getInfantOverideCharges() {
		return infantOverideCharges;
	}

	public void setInfantOverideCharges(ConfirmUpdateOverrideChargesTO infantOverideCharges) {
		this.infantOverideCharges = infantOverideCharges;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public CommonReservationContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(CommonReservationContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}
}
